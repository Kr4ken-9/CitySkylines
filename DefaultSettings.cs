﻿using System;
using ColossalFramework;

public static class DefaultSettings
{
	public static readonly int dofMode = 3;

	public static readonly float tiltShiftAmount = 1f;

	public static readonly float filmGrainAmount = 0.3f;

	public static readonly float mainAudioVolume = 1f;

	public static readonly float musicAudioVolume = 1f;

	public static readonly float ambientAudioVolume = 1f;

	public static readonly float broadcastAudioVolume = 1f;

	public static readonly float effectAudioVolume = 1f;

	public static readonly float uiAudioVolume = 1f;

	public static readonly float chirperAudioVolume = 1f;

	public static readonly string localeID = "en";

	public static readonly bool tutorialMessages = true;

	public static readonly bool enableDayNight = true;

	public static readonly bool enableWeather = true;

	public static readonly bool randomDisastersEnabled = true;

	public static readonly float randomDisastersProbability = 0.5f;

	public static readonly bool roadNamesVisible = true;

	public static readonly bool fadeAwayNotifications = false;

	public static readonly int vsync = 1;

	public static readonly int shadowsDistance = 2;

	public static readonly int shadowsQuality = 3;

	public static readonly int texturesQuality = 2;

	public static readonly int antialiasing = 1;

	public static readonly int anisotropicFiltering = 1;

	public static readonly int levelOfDetail = 2;

	public static readonly bool edgeScrolling = true;

	public static readonly bool invertYMouse = false;

	public static readonly bool mouseWheelZoom = true;

	public static readonly float mouseSensitivity = 0.5f;

	public static readonly float edgeScrollSensitivity = 0.5f;

	public static readonly float mouseLightIntensity = 0.5f;

	public static readonly int builtinLUT = 0;

	public static readonly string userLUT = string.Empty;

	public static readonly bool autoExpandChirper = true;

	public static readonly bool autoSave = false;

	public static readonly int autoSaveInterval = 10;

	public static readonly bool editorAutoSave = false;

	public static readonly int editorAutoSaveInterval = 10;

	public static readonly string chirperImage = "Ordinary";

	public static readonly int temperatureUnit = 0;

	public static readonly InputKey debugOuput = SavedInputKey.Encode(288, false, false, false);

	public static readonly InputKey screenshot = SavedInputKey.Encode(293, false, false, false);

	public static readonly InputKey hiresScreenshot = SavedInputKey.Encode(293, false, true, false);

	public static readonly InputKey buildElevationUp = SavedInputKey.Encode(280, false, false, false);

	public static readonly InputKey buildElevationDown = SavedInputKey.Encode(281, false, false, false);

	public static readonly InputKey cameraMoveLeft = SavedInputKey.Encode(97, false, false, false);

	public static readonly InputKey cameraMoveRight = SavedInputKey.Encode(100, false, false, false);

	public static readonly InputKey cameraMoveForward = SavedInputKey.Encode(119, false, false, false);

	public static readonly InputKey cameraMoveBackward = SavedInputKey.Encode(115, false, false, false);

	public static readonly InputKey cameraMouseRotate = SavedInputKey.Encode(325, false, false, false);

	public static readonly InputKey cameraRotateLeft = SavedInputKey.Encode(113, false, false, false);

	public static readonly InputKey cameraRotateRight = SavedInputKey.Encode(101, false, false, false);

	public static readonly InputKey cameraRotateUp = SavedInputKey.Encode(114, false, false, false);

	public static readonly InputKey cameraRotateDown = SavedInputKey.Encode(102, false, false, false);

	public static readonly InputKey cameraZoomCloser = SavedInputKey.Encode(122, false, false, false);

	public static readonly InputKey cameraZoomAway = SavedInputKey.Encode(120, false, false, false);

	public static readonly InputKey inGameSimulationPause = SavedInputKey.Encode(32, false, false, false);

	public static readonly InputKey inGameSimulationSpeed1 = SavedInputKey.Encode(49, false, false, false);

	public static readonly InputKey inGameSimulationSpeed2 = SavedInputKey.Encode(50, false, false, false);

	public static readonly InputKey inGameSimulationSpeed3 = SavedInputKey.Encode(51, false, false, false);

	public static readonly InputKey inGameShortcutRoads = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutZoning = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutDistricts = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutElectricity = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutWater = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutGarbage = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutHealthcare = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutFireDepartment = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPoliceDepartment = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutEducation = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransport = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutDecoration = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonuments = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutWonders = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscaping = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutEconomy = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBudget = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutTaxes = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLoans = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPolicies = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBulldozer = SavedInputKey.Encode(98, false, false, false);

	public static readonly InputKey inGameShortcutBulldozerUnderground = SavedInputKey.Encode(118, false, false, false);

	public static readonly InputKey inGameShortcutRoadsSmallGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutRoadsMediumGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutRoadsLargeGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutRoadsHighwayGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutRoadsIntersectionGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutRoadsMaintenanceGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportBusGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportTramGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportMetroGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportTrainGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportShipGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportPlaneGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportTaxiGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportMonorailGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutPublicTransportCableCarGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutDistrictSpecializationPaintGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutDistrictSpecializationIndustrialGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutDistrictSpecializationCommercialGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutWaterServicesGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutWaterHeatingGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBeautificationParksGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBeautificationPlazasGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBeautificationOthersGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBeautificationExpansion1Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutBeautificationExpansion2Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingPathsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingTreesGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingRocksGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingToolsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingWaterStructuresGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutLandscapingDisastersGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentLandmarksGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentExpansion1Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentExpansion2Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory1Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory2Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory3Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory4Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory5Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutMonumentCategory6Group = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutAdvisor = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutFreeCameraMode = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutUnlockingPanel = SavedInputKey.Encode(117, false, false, false);

	public static readonly InputKey inGameShortcutGameAreas = SavedInputKey.Encode(121, false, false, false);

	public static readonly InputKey inGameShortcutCityInfo = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutQuickSave = SavedInputKey.Encode(282, false, false, false);

	public static readonly InputKey inGameShortcutQuickLoad = SavedInputKey.Encode(285, false, false, false);

	public static readonly InputKey inGameShortcutZoneResidentialLow = SavedInputKey.Encode(52, false, false, false);

	public static readonly InputKey inGameShortcutZoneResidentialHigh = SavedInputKey.Encode(53, false, false, false);

	public static readonly InputKey inGameShortcutZoneCommercialLow = SavedInputKey.Encode(54, false, false, false);

	public static readonly InputKey inGameShortcutZoneCommercialHigh = SavedInputKey.Encode(55, false, false, false);

	public static readonly InputKey inGameShortcutZoneIndustrial = SavedInputKey.Encode(56, false, false, false);

	public static readonly InputKey inGameShortcutZoneOffices = SavedInputKey.Encode(57, false, false, false);

	public static readonly InputKey inGameShortcutDezone = SavedInputKey.Encode(48, false, false, false);

	public static readonly InputKey inGameShortcutInfoviews = SavedInputKey.Encode(105, false, false, false);

	public static readonly InputKey inGameShortcutInfoElectricity = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoWater = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoCrime = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoHealth = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoHappiness = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoPopulation = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoNoisePollution = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoPublicTransport = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoPollution = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoResources = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoLandValue = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoDistricts = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoOutsideConnections = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoTrafficCongestion = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoWindSpeed = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoGarbage = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoLevel = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoFireSafety = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoEducation = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoHeating = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoMaintenance = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoSnow = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoEscapeRoutes = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoRadio = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoDestruction = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoDisasterDetection = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutInfoTrafficRoutes = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutSnapToAngle = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutSnapToLength = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutSnapToGrid = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutSnapToHelperLines = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey inGameShortcutSnapToAll = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutToggleSnappingMenu = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutElevationStep = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutOptionsButton1 = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutOptionsButton2 = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutOptionsButton3 = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey sharedShortcutOptionsButton4 = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorSimulationPause = SavedInputKey.Encode(32, false, false, false);

	public static readonly InputKey mapEditorSimulationSpeed1 = SavedInputKey.Encode(49, false, false, false);

	public static readonly InputKey mapEditorSimulationSpeed2 = SavedInputKey.Encode(50, false, false, false);

	public static readonly InputKey mapEditorSimulationSpeed3 = SavedInputKey.Encode(51, false, false, false);

	public static readonly InputKey mapEditorTerrainTool = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorImportHeightmap = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorExportHeightmap = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorWaterTool = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorNaturalResourcesTool = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorForestTool = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorOutsideConnections = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorEnvironment = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorIncreaseBrushSize = SavedInputKey.Encode(270, false, false, false);

	public static readonly InputKey mapEditorDecreaseBrushSize = SavedInputKey.Encode(269, false, false, false);

	public static readonly InputKey mapEditorIncreaseBrushStrength = SavedInputKey.Encode(270, false, true, false);

	public static readonly InputKey mapEditorDecreaseBrushStrength = SavedInputKey.Encode(269, false, true, false);

	public static readonly InputKey mapEditorRaiseSeaHeight = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorLowerSeaHeight = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorRaiseLevelHeight = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorLowerLevelHeight = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorResetSeaLevel = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorIncreaseCapacity = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorDecreaseCapacity = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey mapEditorShortcutBulldozer = SavedInputKey.Encode(98, false, false, false);

	public static readonly InputKey mapEditorShortcutBulldozerUnderground = SavedInputKey.Encode(118, false, false, false);

	public static readonly InputKey mapEditorTerrainUndo = SavedInputKey.Encode(122, true, false, false);

	public static readonly InputKey mapEditorSettings = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationSimulationPause = SavedInputKey.Encode(32, false, false, false);

	public static readonly InputKey decorationSimulationSpeed1 = SavedInputKey.Encode(49, false, false, false);

	public static readonly InputKey decorationSimulationSpeed2 = SavedInputKey.Encode(50, false, false, false);

	public static readonly InputKey decorationSimulationSpeed3 = SavedInputKey.Encode(51, false, false, false);

	public static readonly InputKey decorationRoads = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationBeautification = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboards = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboards = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsIndustrial = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsParks = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommon = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsResidential = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsMarkers = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationSurface = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationSettings = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationShortcutBulldozer = SavedInputKey.Encode(98, false, false, false);

	public static readonly InputKey decorationShortcutBulldozerUnderground = SavedInputKey.Encode(118, false, false, false);

	public static readonly InputKey decorationRoadsSmallGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationRoadsMediumGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationRoadsLargeGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationRoadsHighwayGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPublicTransportTramGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPublicTransportTrainGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationRoadsIntersectionGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationBeautificationPathsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationBeautificationPropsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboardsLogoGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboardsSmallBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboardsMediumBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboardsLargeBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsBillboardsRandomLogoGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboardsRandomSmallBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboardsRandomMediumBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboardsRandomLargeBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboards3DBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsSpecialBillboardsAnimatedBillboardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsIndustrialContainersGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsIndustrialConstructionMaterialsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsIndustrialStructuresGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsParksPlaygroundsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsParksFlowersAndPlantsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsParksParkEquipmentGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsParksFountainsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommonAccessoriesGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommonGarbageGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommonCommunicationsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommonStreetsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsCommonLightsGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsResidentialHomeYardGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsResidentialGroundTilesGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsResidentialRooftopAccessGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey decorationPropsResidentialRandomRooftopAccessGroup = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorSimulationPause = SavedInputKey.Encode(32, false, false, false);

	public static readonly InputKey themeEditorSimulationSpeed1 = SavedInputKey.Encode(49, false, false, false);

	public static readonly InputKey themeEditorSimulationSpeed2 = SavedInputKey.Encode(50, false, false, false);

	public static readonly InputKey themeEditorSimulationSpeed3 = SavedInputKey.Encode(51, false, false, false);

	public static readonly InputKey themeEditorTerrainProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorWaterProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorAtmosphereProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorStructuresProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorWorldProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorDisasterProperties = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey themeEditorSettings = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey scenarioEditorBulldozer = SavedInputKey.Encode(98, false, false, false);

	public static readonly InputKey scenarioEditorBulldozerUnderground = SavedInputKey.Encode(118, false, false, false);

	public static readonly InputKey scenarioEditorTerrainTool = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey scenarioEditorPlaceDisasters = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey scenarioEditorTriggerPanel = SavedInputKey.Encode(0, false, false, false);

	public static readonly InputKey scenarioEditorSettings = SavedInputKey.Encode(0, false, false, false);
}
