﻿using System;

public class HealthcareGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.HealthCare;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Building;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "HealthcareDefault")
			{
				return 0;
			}
			if (name == "MonumentModderPack")
			{
				return 1;
			}
		}
		return 2147483647;
	}
}
