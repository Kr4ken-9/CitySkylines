﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class EventTicketMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_targetTicketsSold;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)this.m_targetTicketsSold)
		{
			num = (long)this.m_targetTicketsSold;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_EVENT_TICKETS_DESC", this.m_localizationKey), this.m_targetTicketsSold);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_EVENT_TICKETS_PROG", this.m_localizationKey), new object[]
		{
			num,
			this.m_targetTicketsSold
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = (long)this.m_targetTicketsSold;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			int num = 0;
			for (int i = 0; i < instance.m_events.m_size; i++)
			{
				if ((instance.m_events.m_buffer[i].m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
				{
					EventInfo info = instance.m_events.m_buffer[i].Info;
					if ((this.m_supportEvents & info.m_type) != (EventManager.EventType)0 && (this.m_supportGroups & info.m_group) != (EventManager.EventGroup)0)
					{
						int num2;
						int num3;
						int num4;
						info.m_eventAI.CountVisitors((ushort)i, ref instance.m_events.m_buffer[i], out num2, out num3, out num4);
						num = Mathf.Max(num, ((num2 + num3) * 100 + (num4 >> 1)) / Mathf.Max(1, num4));
					}
				}
			}
			data.m_progress = (long)Mathf.Min(this.m_targetTicketsSold, num);
			data.m_passedCount = ((data.m_progress != (long)this.m_targetTicketsSold) ? 0 : 1);
		}
		return data;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		if (this.m_targetTicketsSold != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)this.m_targetTicketsSold));
		}
		return num;
	}

	public int m_targetTicketsSold = 100;

	[BitMask]
	public EventManager.EventType m_supportEvents;

	[BitMask]
	public EventManager.EventGroup m_supportGroups;

	public string m_localizationKey;
}
