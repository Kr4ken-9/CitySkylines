﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterTextureLoader
{
	public static Task LoadTextures(Task<GameObject> modelLoad, GameObject model, AssetImporterTextureLoader.ResultType[] results, string path, string modelName, bool lod, int anisoLevel = 1, bool generateDummy = false, bool generatePadding = false)
	{
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Creating Texture processing Thread  [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		Task result = ThreadHelper.get_taskDistributor().Dispatch(delegate
		{
			try
			{
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Loading Textures [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				string baseName = Path.Combine(path, Path.GetFileNameWithoutExtension(modelName)) + ((!lod) ? string.Empty : "_lod");
				bool[] array = new bool[8];
				for (int i = 0; i < results.Length; i++)
				{
					int num = (int)results[i];
					for (int j = 0; j < AssetImporterTextureLoader.NeededSources[num].Count; j++)
					{
						array[(int)AssetImporterTextureLoader.NeededSources[num][j]] = true;
					}
				}
				Task<Image>[] array2 = new Task<Image>[array.Length];
				for (int k = 0; k < array.Length; k++)
				{
					if (array[k])
					{
						array2[k] = AssetImporterTextureLoader.LoadTexture((AssetImporterTextureLoader.SourceType)k, baseName);
					}
				}
				TaskExtension.WaitAll(array2);
				if (modelLoad != null)
				{
					modelLoad.Wait();
					model = modelLoad.get_result();
				}
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Finished loading Textures  [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				Image[] array3 = AssetImporterTextureLoader.ExtractImages(array2);
				int width = 0;
				int height = 0;
				for (int l = 0; l < array3.Length; l++)
				{
					if (array3[l] != null && array3[l].get_width() > 0 && array3[l].get_height() > 0)
					{
						width = array3[l].get_width();
						height = array3[l].get_height();
						break;
					}
				}
				for (int m = 0; m < results.Length; m++)
				{
					Color[] texData = AssetImporterTextureLoader.BuildTexture(array3, results[m], width, height, !lod);
					AssetImporterTextureLoader.ResultType result = results[m];
					string sampler = AssetImporterTextureLoader.ResultSamplers[(int)result];
					Color def = AssetImporterTextureLoader.ResultDefaults[(int)result];
					bool resultLinear = AssetImporterTextureLoader.ResultLinear[sampler];
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						Texture2D texture2D;
						if (texData != null)
						{
							if (sampler.Equals("_XYCAMap"))
							{
								texture2D = new Texture2D(width, height, 4, false, resultLinear);
							}
							else
							{
								texture2D = new Texture2D(width, height, 3, false, resultLinear);
							}
							texture2D.SetPixels(texData);
							texture2D.set_anisoLevel(anisoLevel);
							texture2D.Apply();
						}
						else if (generateDummy && width > 0 && height > 0)
						{
							texture2D = new Texture2D(width, height, 3, false, AssetImporterTextureLoader.ResultLinear[sampler]);
							if (result == AssetImporterTextureLoader.ResultType.XYS)
							{
								def.b = 1f - def.b;
							}
							else if (result == AssetImporterTextureLoader.ResultType.APR)
							{
								def.r = 1f - def.r;
								def.g = 1f - def.g;
							}
							for (int n = 0; n < height; n++)
							{
								for (int num2 = 0; num2 < width; num2++)
								{
									texture2D.SetPixel(num2, n, def);
								}
							}
							texture2D.set_anisoLevel(anisoLevel);
							texture2D.Apply();
						}
						else
						{
							texture2D = null;
						}
						if (generatePadding && texture2D != null)
						{
							texture2D = AssetImporterTextureLoader.GeneratePaddedTexture(texture2D, 0.125f, resultLinear);
						}
						AssetImporterTextureLoader.ApplyTexture(model, sampler, texture2D);
					});
				}
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Finished applying Textures  [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
			}
			catch (Exception ex)
			{
				CODebugBase<LogChannel>.Error(LogChannel.AssetImporter, string.Concat(new object[]
				{
					ex.GetType(),
					" ",
					ex.Message,
					" ",
					ex.StackTrace
				}));
				UIView.ForwardException(ex);
			}
		});
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Created Texture processing Thread [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		return result;
	}

	private static Texture2D GeneratePaddedTexture(Texture2D src, float padding, bool linear)
	{
		Texture2D texture2D = new Texture2D(src.get_width(), src.get_height(), src.get_format(), false, linear);
		texture2D.set_anisoLevel(src.get_anisoLevel());
		int width = src.get_width();
		int height = src.get_height();
		float num = 1f / (1f - 2f * padding);
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				float num2 = Mathf.Clamp(((float)j / (float)width - padding) * num, 0f, 1f);
				float num3 = Mathf.Clamp(((float)i / (float)height - padding) * num, 0f, 1f);
				texture2D.SetPixel(j, i, src.GetPixelBilinear(num2, num3));
			}
		}
		texture2D.Apply(true);
		return texture2D;
	}

	protected static Task<Image> LoadTexture(AssetImporterTextureLoader.SourceType source, string baseName)
	{
		string textureFile = AssetImporterTextureLoader.FindTexture(baseName, source);
		if (textureFile == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "Texture missing");
			return null;
		}
		Task<Image> task = new Task<Image>(delegate
		{
			try
			{
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"*********Loading texture [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				Image image = new Image(textureFile);
				Image result;
				if (image != null && image.get_width() > 0 && image.get_height() > 0)
				{
					CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
					{
						"*********Finished loading texture [",
						Thread.CurrentThread.Name,
						Thread.CurrentThread.ManagedThreadId,
						"]"
					}));
					result = image;
					return result;
				}
				CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "Texture: resolution error or texture missing");
				result = null;
				return result;
			}
			catch (Exception ex)
			{
				CODebugBase<LogChannel>.Error(LogChannel.AssetImporter, string.Concat(new object[]
				{
					ex.GetType(),
					" ",
					ex.Message,
					" ",
					ex.StackTrace
				}));
				UIView.ForwardException(ex);
			}
			return null;
		});
		AssetImporterTextureLoader.sTaskDistributor.Dispatch(task);
		return task;
	}

	public static string FindTexture(string basePath, AssetImporterTextureLoader.SourceType type)
	{
		for (int i = 0; i < AssetImporterTextureLoader.SourceTextureExtensions.Length; i++)
		{
			string text = basePath + AssetImporterTextureLoader.SourceTypeSignatures[(int)type] + AssetImporterTextureLoader.SourceTextureExtensions[i];
			if (FileUtils.Exists(text))
			{
				return text;
			}
		}
		return null;
	}

	protected static bool CheckResolutions(int width, int height, params Image[] images)
	{
		for (int i = 0; i < images.Length; i++)
		{
			if (images[i] != null && (images[i].get_width() != width || images[i].get_height() != height))
			{
				return false;
			}
		}
		return true;
	}

	protected static Image[] ExtractImages(Task<Image>[] tasks)
	{
		Image[] array = new Image[tasks.Length];
		for (int i = 0; i < tasks.Length; i++)
		{
			if (tasks[i] != null)
			{
				array[i] = tasks[i].get_result();
			}
		}
		return array;
	}

	public static void ApplyTexture(GameObject target, string sampler, Texture texture)
	{
		if (target == null)
		{
			return;
		}
		Renderer[] componentsInChildren = target.GetComponentsInChildren<MeshRenderer>(true);
		if (componentsInChildren == null || componentsInChildren.Length == 0)
		{
			componentsInChildren = target.GetComponentsInChildren<SkinnedMeshRenderer>(true);
		}
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			for (int j = 0; j < componentsInChildren[i].get_sharedMaterials().Length; j++)
			{
				componentsInChildren[i].get_sharedMaterials()[j].SetTexture(sampler, texture);
			}
		}
	}

	protected static void CombineChannels(Color[] output, Color[] r, Color[] g, Color[] b, Color[] a, bool ralpha, bool galpha, bool balpha, bool rinvert, bool ginvert, bool binvert, bool ainvert, Color defaultColor)
	{
		for (int i = 0; i < output.Length; i++)
		{
			float num = (r == null) ? defaultColor.r : ((!ralpha) ? r[i].r : r[i].a);
			float num2 = (g == null) ? defaultColor.g : ((!galpha) ? g[i].g : g[i].a);
			float num3 = (b == null) ? defaultColor.b : ((!balpha) ? b[i].b : b[i].a);
			float num4 = (a == null) ? defaultColor.a : a[i].a;
			if (rinvert)
			{
				num = 1f - num;
			}
			if (ginvert)
			{
				num2 = 1f - num2;
			}
			if (ainvert)
			{
				num4 = 1f - num4;
			}
			if (binvert)
			{
				num3 = 1f - num3;
			}
			output[i] = new Color(num, num2, num3, num4);
		}
	}

	protected static Color[] BuildTexture(Image[] sources, AssetImporterTextureLoader.ResultType type, int width, int height, bool nullAllowed)
	{
		List<AssetImporterTextureLoader.SourceType> list = AssetImporterTextureLoader.NeededSources[(int)type];
		Image[] array = new Image[list.Count];
		for (int i = 0; i < list.Count; i++)
		{
			array[i] = sources[(int)list[i]];
		}
		if (nullAllowed)
		{
			bool flag = false;
			for (int j = 0; j < array.Length; j++)
			{
				if (array[j] != null)
				{
					flag = true;
				}
			}
			if (!flag)
			{
				return null;
			}
		}
		if (AssetImporterTextureLoader.CheckResolutions(width, height, array))
		{
			Color[] array2 = new Color[width * height];
			switch (type)
			{
			case AssetImporterTextureLoader.ResultType.RGB:
			{
				Color[] array3 = (array[0] == null) ? null : array[0].GetColors();
				AssetImporterTextureLoader.CombineChannels(array2, array3, array3, array3, null, false, false, false, false, false, false, false, AssetImporterTextureLoader.ResultDefaults[(int)type]);
				break;
			}
			case AssetImporterTextureLoader.ResultType.XYS:
			{
				Color[] array4 = (array[0] == null) ? null : array[0].GetColors();
				if (array[1] != null)
				{
					array[1].Convert(1);
				}
				Color[] b = (array[1] == null) ? null : array[1].GetColors();
				AssetImporterTextureLoader.CombineChannels(array2, array4, array4, b, null, false, false, true, false, false, true, false, AssetImporterTextureLoader.ResultDefaults[(int)type]);
				break;
			}
			case AssetImporterTextureLoader.ResultType.ACI:
			{
				if (array[0] != null)
				{
					array[0].Convert(1);
				}
				Color[] array5 = (array[0] == null) ? null : array[0].GetColors();
				if (array[1] != null)
				{
					array[1].Convert(1);
				}
				Color[] array6 = (array[1] == null) ? null : array[1].GetColors();
				if (array[2] != null)
				{
					array[2].Convert(1);
				}
				Color[] b2 = (array[2] == null) ? null : array[2].GetColors();
				AssetImporterTextureLoader.CombineChannels(array2, array5, array6, b2, null, true, true, true, true, true, false, false, AssetImporterTextureLoader.ResultDefaults[(int)type]);
				break;
			}
			case AssetImporterTextureLoader.ResultType.XYCA:
			{
				Color[] array4 = (array[0] == null) ? null : array[0].GetColors();
				if (array[1] != null)
				{
					array[1].Convert(1);
				}
				Color[] array6 = (array[1] == null) ? null : array[1].GetColors();
				if (array[2] != null)
				{
					array[2].Convert(1);
				}
				Color[] array5 = (array[2] == null) ? null : array[2].GetColors();
				AssetImporterTextureLoader.CombineChannels(array2, array4, array4, array6, array5, false, false, true, false, false, true, true, AssetImporterTextureLoader.ResultDefaults[(int)type]);
				break;
			}
			case AssetImporterTextureLoader.ResultType.APR:
			{
				if (array[0] != null)
				{
					array[0].Convert(1);
				}
				Color[] array5 = (array[0] == null) ? null : array[0].GetColors();
				if (array[1] != null)
				{
					array[1].Convert(1);
				}
				Color[] g = (array[1] == null) ? null : array[1].GetColors();
				if (array[2] != null)
				{
					array[2].Convert(1);
				}
				Color[] b3 = (array[2] == null) ? null : array[2].GetColors();
				AssetImporterTextureLoader.CombineChannels(array2, array5, g, b3, null, true, true, true, true, true, false, false, AssetImporterTextureLoader.ResultDefaults[(int)type]);
				break;
			}
			}
			CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
			{
				"******Finished loading & processing textures [",
				Thread.CurrentThread.Name,
				Thread.CurrentThread.ManagedThreadId,
				"]"
			}));
			return array2;
		}
		CODebugBase<LogChannel>.Error(LogChannel.AssetImporter, "Texture error: resolutions don't match or textures missing");
		return null;
	}

	public static Task CompressTexture(Material mat, string texName, bool linear)
	{
		Texture2D texture = mat.GetTexture(texName) as Texture2D;
		if (texture != null)
		{
			Image img = new Image(texture);
			if (!texName.Equals("_XYCAMap"))
			{
				img.Convert(3);
			}
			return ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				img.Compress();
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					Texture texture = img.CreateTexture(linear);
					texture.set_name(texName);
					texture.set_anisoLevel(texture.get_anisoLevel());
					mat.SetTexture(texName, texture);
					Object.Destroy(texture);
				}).Wait();
			});
		}
		return null;
	}

	private static void BuildDummyLODTextures(GameObject LODObject)
	{
		if (LODObject != null)
		{
			Renderer component = LODObject.GetComponent<Renderer>();
			if (component != null && component.get_sharedMaterial() != null)
			{
				Texture texture = component.get_sharedMaterial().GetTexture("_MainTex");
				Texture texture2 = component.get_sharedMaterial().GetTexture("_XYSMap");
				Texture texture3 = component.get_sharedMaterial().GetTexture("_ACIMap");
				int num = 0;
				int num2 = 0;
				if (texture != null)
				{
					num = texture.get_width();
					num2 = texture.get_height();
				}
				else if (texture2 != null)
				{
					num = texture2.get_width();
					num2 = texture2.get_height();
				}
				else if (texture3 != null)
				{
					num = texture3.get_width();
					num2 = texture3.get_height();
				}
				Color32[] array = new Color32[num * num2];
				if (texture == null)
				{
					for (int i = 0; i < num * num2; i++)
					{
						array[i] = new Color32(255, 255, 255, 255);
					}
					Texture2D texture2D = new Texture2D(num, num2, 3, false, false);
					texture2D.SetPixels32(array);
					texture2D.Apply();
					component.get_sharedMaterial().SetTexture("_MainTex", texture2D);
				}
				if (texture2 == null)
				{
					for (int j = 0; j < num * num2; j++)
					{
						array[j] = new Color32(128, 128, 255, 255);
					}
					Texture2D texture2D2 = new Texture2D(num, num2, 3, false, false);
					texture2D2.SetPixels32(array);
					texture2D2.Apply();
					component.get_sharedMaterial().SetTexture("_XYSMap", texture2D2);
				}
				if (texture3 == null)
				{
					for (int k = 0; k < num * num2; k++)
					{
						array[k] = new Color32(0, 0, 0, 255);
					}
					Texture2D texture2D3 = new Texture2D(num, num2, 3, false, false);
					texture2D3.SetPixels32(array);
					texture2D3.Apply();
					component.get_sharedMaterial().SetTexture("_ACIMap", texture2D3);
				}
			}
		}
	}

	protected static readonly string[] SourceTypeSignatures = new string[]
	{
		"_d",
		"_n",
		"_s",
		"_a",
		"_c",
		"_i",
		"_p",
		"_r"
	};

	protected static readonly string[] ResultSamplers = new string[]
	{
		"_MainTex",
		"_XYSMap",
		"_ACIMap",
		"_XYCAMap",
		"_APRMap"
	};

	protected static readonly Color[] ResultDefaults = new Color[]
	{
		new Color(1f, 1f, 1f, 1f),
		new Color(0.5f, 0.5f, 0f, 1f),
		new Color(1f, 1f, 0f, 1f),
		new Color(0.5f, 0.5f, 1f, 1f),
		new Color(1f, 0f, 0f, 0f)
	};

	protected static readonly Dictionary<string, bool> ResultLinear = new Dictionary<string, bool>
	{
		{
			"_MainTex",
			false
		},
		{
			"_XYSMap",
			true
		},
		{
			"_ACIMap",
			true
		},
		{
			"_XYCAMap",
			true
		},
		{
			"_APRMap",
			true
		}
	};

	protected static readonly List<AssetImporterTextureLoader.SourceType>[] NeededSources = new List<AssetImporterTextureLoader.SourceType>[]
	{
		new List<AssetImporterTextureLoader.SourceType>
		{
			AssetImporterTextureLoader.SourceType.DIFFUSE
		},
		new List<AssetImporterTextureLoader.SourceType>
		{
			AssetImporterTextureLoader.SourceType.NORMAL,
			AssetImporterTextureLoader.SourceType.SPECULAR
		},
		new List<AssetImporterTextureLoader.SourceType>
		{
			AssetImporterTextureLoader.SourceType.ALPHA,
			AssetImporterTextureLoader.SourceType.COLOR,
			AssetImporterTextureLoader.SourceType.ILLUMINATION
		},
		new List<AssetImporterTextureLoader.SourceType>
		{
			AssetImporterTextureLoader.SourceType.NORMAL,
			AssetImporterTextureLoader.SourceType.COLOR,
			AssetImporterTextureLoader.SourceType.ALPHA
		},
		new List<AssetImporterTextureLoader.SourceType>
		{
			AssetImporterTextureLoader.SourceType.ALPHA,
			AssetImporterTextureLoader.SourceType.PAVEMENT,
			AssetImporterTextureLoader.SourceType.ROAD
		}
	};

	protected static readonly string[] SourceTextureExtensions = Image.GetExtensions(94);

	protected static TaskDistributor sTaskDistributor = new TaskDistributor("SubTaskDistributor");

	public enum SourceType
	{
		DIFFUSE,
		NORMAL,
		SPECULAR,
		ALPHA,
		COLOR,
		ILLUMINATION,
		PAVEMENT,
		ROAD,
		COUNT
	}

	public enum ResultType
	{
		RGB,
		XYS,
		ACI,
		XYCA,
		APR
	}
}
