﻿using System;

[Serializable]
public class GuideInfo
{
	public string m_name;

	public string m_icon;

	public string m_tag;

	public GuideInfo.Delay m_delayType;

	public int m_displayDelay;

	public int m_repeatDelay;

	public int m_maxDisplayCount;

	public bool m_overrideOptions;

	public enum Delay
	{
		GameWeeks,
		RealSeconds,
		OccurrenceCount
	}
}
