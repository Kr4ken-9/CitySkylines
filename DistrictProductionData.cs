﻿using System;
using ColossalFramework.IO;

public struct DistrictProductionData
{
	public void Add(ref DistrictProductionData data)
	{
		this.m_tempElectricityCapacity += data.m_tempElectricityCapacity;
		this.m_tempWaterCapacity += data.m_tempWaterCapacity;
		this.m_tempSewageCapacity += data.m_tempSewageCapacity;
		this.m_tempIncinerationCapacity += data.m_tempIncinerationCapacity;
		this.m_tempGarbageAmount += data.m_tempGarbageAmount;
		this.m_tempGarbageCapacity += data.m_tempGarbageCapacity;
		this.m_tempHealCapacity += data.m_tempHealCapacity;
		this.m_tempCremateCapacity += data.m_tempCremateCapacity;
		this.m_tempDeadAmount += data.m_tempDeadAmount;
		this.m_tempDeadCapacity += data.m_tempDeadCapacity;
		this.m_tempEducation1Capacity += data.m_tempEducation1Capacity;
		this.m_tempEducation2Capacity += data.m_tempEducation2Capacity;
		this.m_tempEducation3Capacity += data.m_tempEducation3Capacity;
		this.m_tempCriminalAmount += data.m_tempCriminalAmount;
		this.m_tempCriminalCapacity += data.m_tempCriminalCapacity;
		this.m_tempCriminalExtra += data.m_tempCriminalExtra;
		this.m_tempTaxiCapacity += data.m_tempTaxiCapacity;
		this.m_tempSnowCover += data.m_tempSnowCover;
		this.m_tempSnowAmount += data.m_tempSnowAmount;
		this.m_tempSnowCapacity += data.m_tempSnowCapacity;
		this.m_tempHeatingCapacity += data.m_tempHeatingCapacity;
		this.m_tempWaterStorageAmount += data.m_tempWaterStorageAmount;
		this.m_tempWaterStorageCapacity += data.m_tempWaterStorageCapacity;
		this.m_tempShelterCitizenNumber += data.m_tempShelterCitizenNumber;
		this.m_tempShelterCitizenCapacity += data.m_tempShelterCitizenCapacity;
		this.m_tempRenewableElectricity += data.m_tempRenewableElectricity;
		this.m_tempPollutingElectricity += data.m_tempPollutingElectricity;
	}

	public void Update()
	{
		this.m_finalElectricityCapacity = this.m_tempElectricityCapacity;
		this.m_finalWaterCapacity = this.m_tempWaterCapacity;
		this.m_finalSewageCapacity = this.m_tempSewageCapacity;
		this.m_finalIncinerationCapacity = this.m_tempIncinerationCapacity;
		this.m_finalGarbageAmount = this.m_tempGarbageAmount;
		this.m_finalGarbageCapacity = this.m_tempGarbageCapacity;
		this.m_finalHealCapacity = this.m_tempHealCapacity;
		this.m_finalCremateCapacity = this.m_tempCremateCapacity;
		this.m_finalDeadAmount = this.m_tempDeadAmount;
		this.m_finalDeadCapacity = this.m_tempDeadCapacity;
		this.m_finalEducation1Capacity = this.m_tempEducation1Capacity;
		this.m_finalEducation2Capacity = this.m_tempEducation2Capacity;
		this.m_finalEducation3Capacity = this.m_tempEducation3Capacity;
		this.m_finalCriminalAmount = this.m_tempCriminalAmount;
		this.m_finalCriminalCapacity = this.m_tempCriminalCapacity;
		this.m_finalCriminalExtra = this.m_tempCriminalExtra;
		this.m_averageCriminalExtra = this.m_averageCriminalExtra * 239u / 240u + this.m_finalCriminalExtra * 100u;
		this.m_finalTaxiCapacity = this.m_tempTaxiCapacity;
		this.m_finalSnowCover = this.m_tempSnowCover;
		this.m_finalSnowAmount = this.m_tempSnowAmount;
		this.m_finalSnowCapacity = this.m_tempSnowCapacity;
		this.m_finalHeatingCapacity = this.m_tempHeatingCapacity;
		this.m_finalWaterStorageAmount = this.m_tempWaterStorageAmount;
		this.m_finalWaterStorageCapacity = this.m_tempWaterStorageCapacity;
		this.m_finalShelterCitizenNumber = this.m_tempShelterCitizenNumber;
		this.m_finalShelterCitizenCapacity = this.m_tempShelterCitizenCapacity;
		this.m_finalRenewableElectricity = this.m_tempRenewableElectricity;
		this.m_finalPollutingElectricity = this.m_tempPollutingElectricity;
	}

	public void Reset()
	{
		this.m_tempElectricityCapacity = 0u;
		this.m_tempWaterCapacity = 0u;
		this.m_tempSewageCapacity = 0u;
		this.m_tempIncinerationCapacity = 0u;
		this.m_tempGarbageAmount = 0u;
		this.m_tempGarbageCapacity = 0u;
		this.m_tempHealCapacity = 0u;
		this.m_tempCremateCapacity = 0u;
		this.m_tempDeadAmount = 0u;
		this.m_tempDeadCapacity = 0u;
		this.m_tempEducation1Capacity = 0u;
		this.m_tempEducation2Capacity = 0u;
		this.m_tempEducation3Capacity = 0u;
		this.m_tempCriminalAmount = 0u;
		this.m_tempCriminalCapacity = 0u;
		this.m_tempCriminalExtra = 0u;
		this.m_tempTaxiCapacity = 0u;
		this.m_tempSnowCover = 0u;
		this.m_tempSnowAmount = 0u;
		this.m_tempSnowCapacity = 0u;
		this.m_tempHeatingCapacity = 0u;
		this.m_tempWaterStorageAmount = 0u;
		this.m_tempWaterStorageCapacity = 0u;
		this.m_tempShelterCitizenNumber = 0u;
		this.m_tempShelterCitizenCapacity = 0u;
		this.m_tempRenewableElectricity = 0u;
		this.m_tempPollutingElectricity = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempElectricityCapacity);
		s.WriteUInt24(this.m_tempWaterCapacity);
		s.WriteUInt24(this.m_tempSewageCapacity);
		s.WriteUInt24(this.m_tempIncinerationCapacity);
		s.WriteUInt32(this.m_tempGarbageAmount);
		s.WriteUInt32(this.m_tempGarbageCapacity);
		s.WriteUInt24(this.m_finalElectricityCapacity);
		s.WriteUInt24(this.m_finalWaterCapacity);
		s.WriteUInt24(this.m_finalSewageCapacity);
		s.WriteUInt24(this.m_finalIncinerationCapacity);
		s.WriteUInt32(this.m_finalGarbageAmount);
		s.WriteUInt32(this.m_finalGarbageCapacity);
		s.WriteUInt24(this.m_tempHealCapacity);
		s.WriteUInt24(this.m_tempCremateCapacity);
		s.WriteUInt32(this.m_tempDeadAmount);
		s.WriteUInt32(this.m_tempDeadCapacity);
		s.WriteUInt24(this.m_finalHealCapacity);
		s.WriteUInt24(this.m_finalCremateCapacity);
		s.WriteUInt32(this.m_finalDeadAmount);
		s.WriteUInt32(this.m_finalDeadCapacity);
		s.WriteUInt24(this.m_tempEducation1Capacity);
		s.WriteUInt24(this.m_tempEducation2Capacity);
		s.WriteUInt24(this.m_tempEducation3Capacity);
		s.WriteUInt24(this.m_finalEducation1Capacity);
		s.WriteUInt24(this.m_finalEducation2Capacity);
		s.WriteUInt24(this.m_finalEducation3Capacity);
		s.WriteUInt24(this.m_tempCriminalAmount);
		s.WriteUInt24(this.m_tempCriminalCapacity);
		s.WriteUInt24(this.m_finalCriminalAmount);
		s.WriteUInt24(this.m_finalCriminalCapacity);
		s.WriteUInt16(this.m_tempCriminalExtra);
		s.WriteUInt16(this.m_finalCriminalExtra);
		s.WriteUInt32(this.m_averageCriminalExtra);
		s.WriteUInt16(this.m_tempTaxiCapacity);
		s.WriteUInt16(this.m_finalTaxiCapacity);
		s.WriteUInt32(this.m_tempSnowAmount);
		s.WriteUInt32(this.m_tempSnowCapacity);
		s.WriteUInt32(this.m_finalSnowAmount);
		s.WriteUInt32(this.m_finalSnowCapacity);
		s.WriteUInt24(this.m_tempHeatingCapacity);
		s.WriteUInt24(this.m_finalHeatingCapacity);
		s.WriteUInt32(this.m_tempSnowCover);
		s.WriteUInt32(this.m_finalSnowCover);
		s.WriteUInt32(this.m_tempWaterStorageAmount);
		s.WriteUInt32(this.m_tempWaterStorageCapacity);
		s.WriteUInt24(this.m_tempShelterCitizenNumber);
		s.WriteUInt24(this.m_tempShelterCitizenCapacity);
		s.WriteUInt32(this.m_finalWaterStorageAmount);
		s.WriteUInt32(this.m_finalWaterStorageCapacity);
		s.WriteUInt24(this.m_finalShelterCitizenNumber);
		s.WriteUInt24(this.m_finalShelterCitizenCapacity);
		s.WriteUInt24(this.m_tempRenewableElectricity);
		s.WriteUInt24(this.m_tempPollutingElectricity);
		s.WriteUInt24(this.m_finalRenewableElectricity);
		s.WriteUInt24(this.m_finalPollutingElectricity);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempElectricityCapacity = s.ReadUInt24();
		this.m_tempWaterCapacity = s.ReadUInt24();
		this.m_tempSewageCapacity = s.ReadUInt24();
		this.m_tempIncinerationCapacity = s.ReadUInt24();
		this.m_tempGarbageAmount = s.ReadUInt32();
		this.m_tempGarbageCapacity = s.ReadUInt32();
		this.m_finalElectricityCapacity = s.ReadUInt24();
		this.m_finalWaterCapacity = s.ReadUInt24();
		this.m_finalSewageCapacity = s.ReadUInt24();
		this.m_finalIncinerationCapacity = s.ReadUInt24();
		this.m_finalGarbageAmount = s.ReadUInt32();
		this.m_finalGarbageCapacity = s.ReadUInt32();
		if (s.get_version() >= 108u)
		{
			this.m_tempHealCapacity = s.ReadUInt24();
			this.m_tempCremateCapacity = s.ReadUInt24();
			this.m_tempDeadAmount = s.ReadUInt32();
			this.m_tempDeadCapacity = s.ReadUInt32();
			this.m_finalHealCapacity = s.ReadUInt24();
			this.m_finalCremateCapacity = s.ReadUInt24();
			this.m_finalDeadAmount = s.ReadUInt32();
			this.m_finalDeadCapacity = s.ReadUInt32();
		}
		else
		{
			this.m_tempHealCapacity = 0u;
			this.m_tempCremateCapacity = 0u;
			this.m_tempDeadAmount = 0u;
			this.m_tempDeadCapacity = 0u;
			this.m_finalHealCapacity = 0u;
			this.m_finalCremateCapacity = 0u;
			this.m_finalDeadAmount = 0u;
			this.m_finalDeadCapacity = 0u;
		}
		if (s.get_version() >= 109u)
		{
			this.m_tempEducation1Capacity = s.ReadUInt24();
			this.m_tempEducation2Capacity = s.ReadUInt24();
			this.m_tempEducation3Capacity = s.ReadUInt24();
			this.m_finalEducation1Capacity = s.ReadUInt24();
			this.m_finalEducation2Capacity = s.ReadUInt24();
			this.m_finalEducation3Capacity = s.ReadUInt24();
		}
		else
		{
			this.m_tempEducation1Capacity = 0u;
			this.m_tempEducation2Capacity = 0u;
			this.m_tempEducation3Capacity = 0u;
			this.m_finalEducation1Capacity = 0u;
			this.m_finalEducation2Capacity = 0u;
			this.m_finalEducation3Capacity = 0u;
		}
		if (s.get_version() >= 215u)
		{
			this.m_tempCriminalAmount = s.ReadUInt24();
			this.m_tempCriminalCapacity = s.ReadUInt24();
			this.m_finalCriminalAmount = s.ReadUInt24();
			this.m_finalCriminalCapacity = s.ReadUInt24();
		}
		else
		{
			this.m_tempCriminalAmount = 0u;
			this.m_tempCriminalCapacity = 0u;
			this.m_finalCriminalAmount = 0u;
			this.m_finalCriminalCapacity = 0u;
		}
		if (s.get_version() >= 216u)
		{
			this.m_tempCriminalExtra = s.ReadUInt16();
			this.m_finalCriminalExtra = s.ReadUInt16();
			this.m_averageCriminalExtra = s.ReadUInt32();
		}
		else
		{
			this.m_tempCriminalExtra = 0u;
			this.m_finalCriminalExtra = 0u;
			this.m_averageCriminalExtra = 0u;
		}
		if (s.get_version() >= 219u)
		{
			this.m_tempTaxiCapacity = s.ReadUInt16();
			this.m_finalTaxiCapacity = s.ReadUInt16();
		}
		else
		{
			this.m_tempTaxiCapacity = 0u;
			this.m_finalTaxiCapacity = 0u;
		}
		if (s.get_version() >= 226u)
		{
			this.m_tempSnowAmount = s.ReadUInt32();
			this.m_tempSnowCapacity = s.ReadUInt32();
			this.m_finalSnowAmount = s.ReadUInt32();
			this.m_finalSnowCapacity = s.ReadUInt32();
		}
		else
		{
			this.m_tempSnowAmount = 0u;
			this.m_tempSnowCapacity = 0u;
			this.m_finalSnowAmount = 0u;
			this.m_finalSnowCapacity = 0u;
		}
		if (s.get_version() >= 228u)
		{
			this.m_tempHeatingCapacity = s.ReadUInt24();
			this.m_finalHeatingCapacity = s.ReadUInt24();
		}
		else
		{
			this.m_tempHeatingCapacity = 0u;
			this.m_finalHeatingCapacity = 0u;
		}
		if (s.get_version() >= 240u)
		{
			this.m_tempSnowCover = s.ReadUInt32();
			this.m_finalSnowCover = s.ReadUInt32();
		}
		else
		{
			this.m_tempSnowCover = 0u;
			this.m_finalSnowCover = 0u;
		}
		if (s.get_version() >= 307u)
		{
			this.m_tempWaterStorageAmount = s.ReadUInt32();
			this.m_tempWaterStorageCapacity = s.ReadUInt32();
			this.m_tempShelterCitizenNumber = s.ReadUInt24();
			this.m_tempShelterCitizenCapacity = s.ReadUInt24();
			this.m_finalWaterStorageAmount = s.ReadUInt32();
			this.m_finalWaterStorageCapacity = s.ReadUInt32();
			this.m_finalShelterCitizenNumber = s.ReadUInt24();
			this.m_finalShelterCitizenCapacity = s.ReadUInt24();
		}
		else
		{
			this.m_tempWaterStorageAmount = 0u;
			this.m_tempWaterStorageCapacity = 0u;
			this.m_tempShelterCitizenNumber = 0u;
			this.m_tempShelterCitizenCapacity = 0u;
			this.m_finalWaterStorageAmount = 0u;
			this.m_finalWaterStorageCapacity = 0u;
			this.m_finalShelterCitizenNumber = 0u;
			this.m_finalShelterCitizenCapacity = 0u;
		}
		if (s.get_version() >= 109010u)
		{
			this.m_tempRenewableElectricity = s.ReadUInt24();
			this.m_tempPollutingElectricity = s.ReadUInt24();
			this.m_finalRenewableElectricity = s.ReadUInt24();
			this.m_finalPollutingElectricity = s.ReadUInt24();
		}
		else
		{
			this.m_tempRenewableElectricity = 0u;
			this.m_tempPollutingElectricity = 0u;
			this.m_finalRenewableElectricity = 0u;
			this.m_finalPollutingElectricity = 0u;
		}
	}

	public uint m_tempElectricityCapacity;

	public uint m_tempWaterCapacity;

	public uint m_tempSewageCapacity;

	public uint m_tempIncinerationCapacity;

	public uint m_tempGarbageAmount;

	public uint m_tempGarbageCapacity;

	public uint m_tempHealCapacity;

	public uint m_tempCremateCapacity;

	public uint m_tempDeadAmount;

	public uint m_tempDeadCapacity;

	public uint m_tempEducation1Capacity;

	public uint m_tempEducation2Capacity;

	public uint m_tempEducation3Capacity;

	public uint m_tempCriminalAmount;

	public uint m_tempCriminalCapacity;

	public uint m_tempCriminalExtra;

	public uint m_tempTaxiCapacity;

	public uint m_tempSnowCover;

	public uint m_tempSnowAmount;

	public uint m_tempSnowCapacity;

	public uint m_tempHeatingCapacity;

	public uint m_tempWaterStorageAmount;

	public uint m_tempWaterStorageCapacity;

	public uint m_tempShelterCitizenNumber;

	public uint m_tempShelterCitizenCapacity;

	public uint m_tempRenewableElectricity;

	public uint m_tempPollutingElectricity;

	public uint m_finalElectricityCapacity;

	public uint m_finalWaterCapacity;

	public uint m_finalSewageCapacity;

	public uint m_finalIncinerationCapacity;

	public uint m_finalGarbageAmount;

	public uint m_finalGarbageCapacity;

	public uint m_finalHealCapacity;

	public uint m_finalCremateCapacity;

	public uint m_finalDeadAmount;

	public uint m_finalDeadCapacity;

	public uint m_finalEducation1Capacity;

	public uint m_finalEducation2Capacity;

	public uint m_finalEducation3Capacity;

	public uint m_finalCriminalAmount;

	public uint m_finalCriminalCapacity;

	public uint m_finalCriminalExtra;

	public uint m_finalTaxiCapacity;

	public uint m_finalSnowCover;

	public uint m_finalSnowAmount;

	public uint m_finalSnowCapacity;

	public uint m_finalHeatingCapacity;

	public uint m_finalWaterStorageAmount;

	public uint m_finalWaterStorageCapacity;

	public uint m_finalShelterCitizenNumber;

	public uint m_finalShelterCitizenCapacity;

	public uint m_finalRenewableElectricity;

	public uint m_finalPollutingElectricity;

	public uint m_averageCriminalExtra;
}
