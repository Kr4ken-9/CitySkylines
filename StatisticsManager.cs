﻿using System;
using ColossalFramework;
using ColossalFramework.IO;

public class StatisticsManager : SimulationManagerBase<StatisticsManager, StatisticsProperties>, ISimulationManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_statistics = new StatisticArray();
	}

	public StatisticBase Acquire<T>(StatisticType type) where T : StatisticBase, new()
	{
		return this.m_statistics.Acquire<T>((int)type, 58);
	}

	public StatisticBase Acquire<T, U>(StatisticType type, int count) where T : StatisticBase, new() where U : StatisticBase, new()
	{
		StatisticBase statisticBase = this.m_statistics.Acquire<T>((int)type, 58);
		for (int i = 0; i < count; i++)
		{
			statisticBase.Acquire<U>(i, count);
		}
		return statisticBase;
	}

	public void ResetAll()
	{
		this.m_startFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		for (int i = 0; i < 58; i++)
		{
			StatisticBase statisticBase = this.m_statistics.Get(i);
			if (statisticBase != null)
			{
				statisticBase.Reset();
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u);
			int num2 = num * 58 >> 12;
			int num3 = ((num + 1) * 58 >> 12) - 1;
			for (int i = num2; i <= num3; i++)
			{
				StatisticBase statisticBase = this.m_statistics.Get(i);
				if (statisticBase != null)
				{
					statisticBase.Tick();
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("StatisticsManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || mode == SimulationManager.UpdateMode.NewMap)
		{
			this.ResetAll();
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.Unemployed, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.EligibleWorkers, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.WorkplaceCount, 4);
			this.Acquire<StatisticInt32>(StatisticType.ElectricityCapacity);
			this.Acquire<StatisticInt32>(StatisticType.WaterCapacity);
			this.Acquire<StatisticInt32>(StatisticType.SewageCapacity);
			this.Acquire<StatisticInt32>(StatisticType.IncinerationCapacity);
			this.Acquire<StatisticInt32>(StatisticType.GarbageAmount);
			this.Acquire<StatisticInt32>(StatisticType.GarbageCapacity);
			this.Acquire<StatisticInt32>(StatisticType.HealCapacity);
			this.Acquire<StatisticInt32>(StatisticType.CremateCapacity);
			this.Acquire<StatisticInt32>(StatisticType.DeadAmount);
			this.Acquire<StatisticInt32>(StatisticType.DeadCapacity);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.EducationCapacity, 3);
			this.Acquire<StatisticInt32>(StatisticType.GarbagePiles);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.CitizenHealth, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.CitizenHappiness, 4);
			this.Acquire<StatisticInt32>(StatisticType.CityHappiness);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.CrimeRate, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.StudentCount, 3);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.EducatedCount, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.AbandonedBuildings, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.BuildingArea, 5);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.SpecializationArea, 9);
			this.Acquire<StatisticInt32>(StatisticType.GroundPollution);
			this.Acquire<StatisticInt32>(StatisticType.WaterPollution);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.TouristVisits, 3);
			this.Acquire<StatisticInt32>(StatisticType.BirthRate);
			this.Acquire<StatisticInt32>(StatisticType.DeathRate);
			this.Acquire<StatisticInt32>(StatisticType.PrisonerAmount);
			this.Acquire<StatisticInt32>(StatisticType.HeatingCapacity);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.HeatingConsumption, 5);
			this.Acquire<StatisticInt32>(StatisticType.HeatingUsage);
			this.Acquire<StatisticInt32>(StatisticType.SnowCover);
			this.Acquire<StatisticInt32>(StatisticType.RenewableElectricity);
			this.Acquire<StatisticInt32>(StatisticType.PollutingElectricity);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.GoodsProduced, 5);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.IncomingTourists, 3);
			this.Acquire<StatisticInt64>(StatisticType.CityValue);
			this.Acquire<StatisticInt32>(StatisticType.MoveRate);
			this.Acquire<StatisticInt32>(StatisticType.FullLifespans);
			this.Acquire<StatisticArray, StatisticInt64>(StatisticType.ServiceIncome, 20);
			this.Acquire<StatisticArray, StatisticInt64>(StatisticType.ServiceExpenses, 20);
			this.Acquire<StatisticInt64>(StatisticType.PlayerMoney);
			this.Acquire<StatisticInt64>(StatisticType.PlayerDebt);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.ImmaterialResource, 24);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.NaturalResourcesExtracted, 4);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.AveragePassengers, 10);
			this.Acquire<StatisticArray, StatisticInt32>(StatisticType.PassengerCount, 10);
			this.Acquire<StatisticInt32>(StatisticType.CyclistAmount);
			this.Acquire<StatisticInt32>(StatisticType.SnowCollected);
			this.Acquire<StatisticInt32>(StatisticType.RoadBoosted);
			this.Acquire<StatisticInt32>(StatisticType.DisasterCount);
			this.Acquire<StatisticInt32>(StatisticType.CasualtyCount);
			this.Acquire<StatisticInt32>(StatisticType.CollapsedCount);
			this.Acquire<StatisticInt32>(StatisticType.DestroyedLength);
			this.Acquire<StatisticInt32>(StatisticType.TrafficFlow);
			this.Acquire<StatisticInt32>(StatisticType.ParkVisitCount);
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new StatisticsManager.Data());
	}

	public StatisticBase Get(StatisticType type)
	{
		if (type != StatisticType.None && type != StatisticType.Count)
		{
			return this.m_statistics.Get((int)type);
		}
		return null;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	private uint m_startFrame;

	private StatisticBase m_statistics;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "StatisticsManager");
			StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
			s.WriteUInt32(instance.m_startFrame);
			s.WriteObject<StatisticBase>(instance.m_statistics);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "StatisticsManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "StatisticsManager");
			StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
			instance.m_startFrame = s.ReadUInt32();
			instance.m_statistics = s.ReadObject<StatisticBase>();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "StatisticsManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "StatisticsManager");
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "StatisticsManager");
		}
	}
}
