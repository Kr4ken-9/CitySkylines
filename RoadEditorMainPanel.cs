﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class RoadEditorMainPanel : ToolsModifierControl
{
	private void Awake()
	{
		base.get_component().Hide();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.PositionComponentToRight();
		}
	}

	private void OnEnable()
	{
		ToolsModifierControl.toolController.eventEditPrefabChanged += new ToolController.EditPrefabChanged(this.OnEditPrefabChanged);
	}

	private void OnDisable()
	{
		ToolsModifierControl.toolController.eventEditPrefabChanged -= new ToolController.EditPrefabChanged(this.OnEditPrefabChanged);
	}

	public void Reset(NetInfo info)
	{
		this.m_Info = info;
		this.Clear();
		this.Initialize();
	}

	private void Clear()
	{
		while (this.m_ElevationsTabstrip.get_tabContainer().get_components().Count > 0)
		{
			UIComponent uIComponent = this.m_ElevationsTabstrip.get_tabContainer().get_components()[0];
			RoadEditorPanel component = uIComponent.GetComponent<RoadEditorPanel>();
			if (component != null)
			{
				component.Clear();
			}
			this.m_ElevationsTabstrip.get_tabContainer().RemoveUIComponent(uIComponent);
			Object.Destroy(uIComponent.get_gameObject());
		}
		while (this.m_ElevationsTabstrip.get_tabs().get_Count() > 0)
		{
			UIComponent uIComponent2 = this.m_ElevationsTabstrip.get_tabs().get_Item(0);
			this.m_ElevationsTabstrip.RemoveUIComponent(uIComponent2);
			Object.Destroy(uIComponent2.get_gameObject());
		}
		UITemplateManager.ClearInstances(RoadEditorPanel.kCollapsiblePanel);
		UITemplateManager.ClearInstances(RoadEditorPanel.kToggle);
		UITemplateManager.ClearInstances(RoadEditorPanel.kAddButton);
		UITemplateManager.ClearInstances(RoadEditorPanel.kBoolSet);
		UITemplateManager.ClearInstances(RoadEditorPanel.kFloatSet);
		UITemplateManager.ClearInstances(RoadEditorPanel.kEnumBitmaskSet);
		UITemplateManager.ClearInstances(RoadEditorPanel.kRefSet);
	}

	private void Initialize()
	{
		this.AddTab("Basic", this.m_Info);
		this.AddTab("Elevated", AssetEditorRoadUtils.TryGetElevated(this.m_Info));
		this.AddTab("Bridge", AssetEditorRoadUtils.TryGetBridge(this.m_Info));
		this.AddTab("Slope", AssetEditorRoadUtils.TryGetSlope(this.m_Info));
		this.AddTab("Tunnel", AssetEditorRoadUtils.TryGetTunnel(this.m_Info));
		this.m_ElevationsTabstrip.set_selectedIndex(-1);
		this.m_ElevationsTabstrip.set_selectedIndex(0);
	}

	private void Refresh()
	{
		foreach (UIComponent current in this.m_ElevationsTabstrip.get_tabs())
		{
			RoadEditorPanel component = current.GetComponent<RoadEditorPanel>();
			if (component != null)
			{
				component.Refresh();
			}
		}
	}

	private void AddTab(string name, NetInfo info)
	{
		if (info != null)
		{
			UIComponent uIComponent = this.m_ElevationsTabstrip.AddTab(name, UITemplateManager.GetAsGameObject(RoadEditorMainPanel.kTabstripButton), UITemplateManager.GetAsGameObject(RoadEditorMainPanel.kElevationPanel), new Type[0]);
			((UIButton)uIComponent).set_text(name);
			RoadEditorPanel component = this.m_ElevationsTabstrip.get_tabContainer().Find(name + "Panel").GetComponent<RoadEditorPanel>();
			component.Initialize(info);
			component.EventObjectModified += new RoadEditorPanel.ObjectModifiedHandler(this.OnObjectModified);
			component.owner = this;
		}
	}

	private void PositionComponentToRight()
	{
		Vector2 screenResolution = base.get_component().GetUIView().GetScreenResolution();
		Vector2 size = base.get_component().get_size();
		base.get_component().set_absolutePosition(new Vector3(screenResolution.x - size.x - 10f, screenResolution.y / 2f - size.y / 2f));
	}

	private void OnEditPrefabChanged(PrefabInfo info)
	{
		if (this.m_Info != null && this.m_Info != info)
		{
			AssetEditorRoadUtils.ReleaseRoad(this.m_Info);
		}
		if (info != null && info is NetInfo)
		{
			base.get_component().Show();
			this.Reset(info as NetInfo);
		}
		else
		{
			base.get_component().Hide();
		}
	}

	private void OnObjectModified()
	{
		base.StartCoroutine(AssetEditorRoadUtils.RefreshNetsCoroutine(this.m_Info));
		this.Refresh();
	}

	public static readonly string kTabstripButton = "RoadEditorTabstripButton";

	public static readonly string kElevationPanel = "RoadEditorElevationPanel";

	public UITabstrip m_ElevationsTabstrip;

	private NetInfo m_Info;
}
