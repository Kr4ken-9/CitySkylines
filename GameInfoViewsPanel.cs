﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class GameInfoViewsPanel : InfoViewsPanel
{
	public bool IgnoreInfoView(InfoManager.InfoMode mode)
	{
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
		bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		bool flag3 = Singleton<SimulationManager>.get_exists() && Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Winter";
		switch (mode)
		{
		case InfoManager.InfoMode.Heating:
		case InfoManager.InfoMode.Maintenance:
			return !flag;
		case InfoManager.InfoMode.Snow:
			return !flag3 || !flag;
		case InfoManager.InfoMode.EscapeRoutes:
		case InfoManager.InfoMode.Radio:
		case InfoManager.InfoMode.Destruction:
		case InfoManager.InfoMode.DisasterDetection:
			return !flag2;
		default:
			return false;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		UIButton uIButton = null;
		int num = 0;
		for (int i = 0; i < GameInfoViewsPanel.kResources.Length; i++)
		{
			if (!this.IgnoreInfoView(GameInfoViewsPanel.kResources[i].get_enumValue()))
			{
				uIButton = base.SpawnButtonEntry(GameInfoViewsPanel.kResources[i].get_enumName(), "InfoIcon", "INFOVIEWS", num, ToolsModifierControl.IsUnlocked(GameInfoViewsPanel.kResources[i].get_enumValue()));
				this.m_buttonToResource[num] = i;
				this.m_resourceToButton[i] = num;
				num++;
			}
		}
		if (uIButton != null)
		{
			UIPanel uIPanel = (UIPanel)this.m_ChildContainer;
			this.m_RootContainer.set_height((float)Mathf.CeilToInt((float)num / 2f) * (uIButton.get_height() + (float)uIPanel.get_autoLayoutPadding().get_top()) + (float)uIPanel.get_autoLayoutPadding().get_top());
		}
	}

	protected override void ShowSelectedIndex()
	{
		if (Singleton<InfoManager>.get_exists())
		{
			if (Singleton<InfoManager>.get_instance().NextMode != this.m_cachedMode)
			{
				this.m_cachedMode = Singleton<InfoManager>.get_instance().NextMode;
				this.m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(this.m_cachedMode, "Game");
			}
			int num = this.m_cachedIndex;
			if (num >= 0 && num < this.m_resourceToButton.Length)
			{
				num = this.m_resourceToButton[num];
			}
			if (num != base.selectedIndex)
			{
				base.SelectByIndex(num, false);
			}
		}
		base.ShowSelectedIndex();
	}

	protected override void OnButtonClicked(int index)
	{
		if (index >= 0 && index < this.m_buttonToResource.Length)
		{
			index = this.m_buttonToResource[index];
		}
		if (ToolsModifierControl.GetCurrentTool<TransportTool>() != null)
		{
			ToolsModifierControl.SetTool<DefaultTool>();
			base.CloseToolbar();
		}
		else if (ToolsModifierControl.GetCurrentTool<NetTool>() != null && !ToolBase.OverrideInfoMode && (Singleton<TransportManager>.get_instance().TunnelsVisible || Singleton<TransportManager>.get_instance().TunnelsVisibleInfo))
		{
			ToolsModifierControl.SetTool<DefaultTool>();
			base.CloseToolbar();
		}
		if (index >= 0 && index < GameInfoViewsPanel.kResources.Length)
		{
			BuildingTool currentTool = ToolsModifierControl.GetCurrentTool<BuildingTool>();
			if (currentTool != null && currentTool.m_relocate != 0)
			{
				currentTool.CancelRelocate();
			}
			ToolBase.OverrideInfoMode = true;
			WorldInfoPanel.HideAllWorldInfoPanels();
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(GameInfoViewsPanel.kResources[index].get_enumValue(), InfoManager.SubInfoMode.Default);
				Singleton<GuideManager>.get_instance().InfoViewUsed();
			}
		}
		else
		{
			ToolBase.OverrideInfoMode = true;
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
				Singleton<GuideManager>.get_instance().InfoViewUsed();
			}
		}
	}

	private static readonly PositionData<InfoManager.InfoMode>[] kResources = Utils.GetOrderedEnumData<InfoManager.InfoMode>("Game");

	private InfoManager.InfoMode m_cachedMode;

	private int m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(InfoManager.InfoMode.None, "Game");

	private int[] m_buttonToResource = new int[GameInfoViewsPanel.kResources.Length];

	private int[] m_resourceToButton = new int[GameInfoViewsPanel.kResources.Length];

	public UIComponent m_RootContainer;
}
