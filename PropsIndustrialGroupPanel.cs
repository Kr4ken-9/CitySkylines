﻿using System;

public sealed class PropsIndustrialGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Prop;
		}
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsIndustrialContainers", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsIndustrialConstructionMaterials", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsIndustrialStructures", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "PropsIndustrialContainers")
			{
				return 0;
			}
			if (name == "PropsIndustrialConstructionMaterials")
			{
				return 1;
			}
			if (name == "PropsIndustrialStructures")
			{
				return 2;
			}
		}
		return 2147483647;
	}
}
