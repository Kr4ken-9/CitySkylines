﻿using System;
using System.Collections.Generic;

public static class DLCIconUtils
{
	public static List<string> GetIcons(SteamHelper.DLC_BitMask dlcMask, int maxCount = -1)
	{
		List<string> list = new List<string>();
		foreach (SteamHelper.DLC_BitMask current in DLCIconUtils.dlcToIcon.Keys)
		{
			if ((dlcMask & current) != (SteamHelper.DLC_BitMask)0)
			{
				list.Add(DLCIconUtils.dlcToIcon[current]);
			}
			if (maxCount > 0 && list.Count >= maxCount)
			{
				break;
			}
		}
		return list;
	}

	public static List<string> GetBuyIcons(SteamHelper.DLC_BitMask dlcMask, int maxCount = -1)
	{
		List<string> list = new List<string>();
		foreach (SteamHelper.DLC_BitMask current in DLCIconUtils.dlcToBuyIcon.Keys)
		{
			if ((dlcMask & current) != (SteamHelper.DLC_BitMask)0)
			{
				list.Add(DLCIconUtils.dlcToBuyIcon[current]);
			}
			if (maxCount > 0 && list.Count >= maxCount)
			{
				break;
			}
		}
		return list;
	}

	private static readonly Dictionary<SteamHelper.DLC_BitMask, string> dlcToIcon = new Dictionary<SteamHelper.DLC_BitMask, string>
	{
		{
			SteamHelper.DLC_BitMask.DeluxeDLC,
			"DeluxeIcon"
		},
		{
			SteamHelper.DLC_BitMask.AfterDarkDLC,
			"ADIcon"
		},
		{
			SteamHelper.DLC_BitMask.SnowFallDLC,
			"WWIcon"
		},
		{
			SteamHelper.DLC_BitMask.NaturalDisastersDLC,
			"NaturalDisastersIcon"
		},
		{
			SteamHelper.DLC_BitMask.Football,
			"MDIcon"
		},
		{
			SteamHelper.DLC_BitMask.Football2345,
			"StadiumsDLCIcon"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack1,
			"ArtDecoIcon"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack2,
			"HighTechIcon"
		},
		{
			SteamHelper.DLC_BitMask.OrientalBuildings,
			"ChineseBuildingsIcon"
		},
		{
			SteamHelper.DLC_BitMask.InMotionDLC,
			"MassTransitIcon"
		},
		{
			SteamHelper.DLC_BitMask.RadioStation1,
			"RadioIconRelaxationStation"
		},
		{
			SteamHelper.DLC_BitMask.RadioStation2,
			"RadioIconRockCity"
		},
		{
			SteamHelper.DLC_BitMask.MusicFestival,
			"ConcertsIcon"
		},
		{
			SteamHelper.DLC_BitMask.GreenCitiesDLC,
			"GreenCitiesIcon"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack3,
			"Modderpack3Icon"
		}
	};

	private static readonly Dictionary<SteamHelper.DLC_BitMask, string> dlcToBuyIcon = new Dictionary<SteamHelper.DLC_BitMask, string>
	{
		{
			SteamHelper.DLC_BitMask.DeluxeDLC,
			"DeluxeBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.AfterDarkDLC,
			"ADBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.SnowFallDLC,
			"WWBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.NaturalDisastersDLC,
			"NaturalDisastersBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.Football,
			"MDBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.Football2345,
			"StadiumsDLCBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack1,
			"ArtDecoBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack2,
			"HighTechBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.OrientalBuildings,
			"ChineseBuildingsBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.InMotionDLC,
			"MassTransitBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.RadioStation1,
			"RadioIconRelaxationStation"
		},
		{
			SteamHelper.DLC_BitMask.RadioStation2,
			"RockCityRadioBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.MusicFestival,
			"ConcertsBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.GreenCitiesDLC,
			"GreenCitiesBuyButton"
		},
		{
			SteamHelper.DLC_BitMask.ModderPack3,
			"Modderpack3PackBuyButton"
		}
	};
}
