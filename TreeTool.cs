﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TreeTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
		this.m_randomizer = new Randomizer((int)DateTime.Now.Ticks);
	}

	protected override void OnToolGUI(Event e)
	{
		if (!this.m_toolController.IsInsideUI && e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = true;
				if (this.m_mode == TreeTool.Mode.Single)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.CreateTree());
				}
			}
			else if (e.get_button() == 1)
			{
				this.m_mouseRightDown = true;
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = false;
			}
			else if (e.get_button() == 1)
			{
				this.m_mouseRightDown = false;
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		base.ToolCursor = this.m_buildCursor;
		this.m_toolController.ClearColliding();
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		if (this.m_mode == TreeTool.Mode.Brush)
		{
			this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
		}
		else
		{
			this.m_toolController.SetBrush(null, Vector3.get_zero(), 1f);
		}
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		this.m_toolController.SetBrush(null, Vector3.get_zero(), 1f);
		this.m_mouseLeftDown = false;
		this.m_mouseRightDown = false;
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		this.m_mouseRayValid = false;
	}

	public override void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
		TreeInfo treeInfo = this.m_treeInfo;
		if (this.m_mode == TreeTool.Mode.Single && treeInfo != null && this.m_placementErrors == ToolBase.ToolErrors.None && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			Randomizer randomizer = this.m_randomizer;
			uint num = Singleton<TreeManager>.get_instance().m_trees.NextFreeItem(ref randomizer);
			Randomizer randomizer2;
			randomizer2..ctor(num);
			float scale = treeInfo.m_minScale + (float)randomizer2.Int32(10000u) * (treeInfo.m_maxScale - treeInfo.m_minScale) * 0.0001f;
			float brightness = treeInfo.m_minBrightness + (float)randomizer2.Int32(10000u) * (treeInfo.m_maxBrightness - treeInfo.m_minBrightness) * 0.0001f;
			TreeInstance.RenderInstance(null, treeInfo, this.m_cachedPosition, scale, brightness);
		}
		base.RenderGeometry(cameraInfo);
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		TreeInfo treeInfo = this.m_treeInfo;
		if (this.m_mode == TreeTool.Mode.Single && treeInfo != null && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			Color toolColor = base.GetToolColor(false, this.m_placementErrors != ToolBase.ToolErrors.None);
			this.m_toolController.RenderColliding(cameraInfo, toolColor, toolColor, toolColor, toolColor, 0, 0);
			Randomizer randomizer = this.m_randomizer;
			uint num = Singleton<TreeManager>.get_instance().m_trees.NextFreeItem(ref randomizer);
			Randomizer randomizer2;
			randomizer2..ctor(num);
			float scale = treeInfo.m_minScale + (float)randomizer2.Int32(10000u) * (treeInfo.m_maxScale - treeInfo.m_minScale) * 0.0001f;
			TreeTool.RenderOverlay(cameraInfo, treeInfo, this.m_cachedPosition, scale, toolColor);
		}
		base.RenderOverlay(cameraInfo);
	}

	public static void RenderOverlay(RenderManager.CameraInfo cameraInfo, TreeInfo info, Vector3 position, float scale, Color color)
	{
		if (info == null)
		{
			return;
		}
		float size = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * scale;
		ToolManager expr_3F_cp_0 = Singleton<ToolManager>.get_instance();
		expr_3F_cp_0.m_drawCallData.m_overlayCalls = expr_3F_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, size, position.y - 100f, position.y + 100f, false, true);
	}

	public static void CheckOverlayAlpha(TreeInfo info, float scale, ref float alpha)
	{
		if (info == null)
		{
			return;
		}
		float num = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * scale * 0.5f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num)));
	}

	protected override void OnToolUpdate()
	{
		TreeInfo treeInfo = this.m_treeInfo;
		if (treeInfo == null)
		{
			return;
		}
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			int constructionCost = treeInfo.GetConstructionCost();
			if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None && constructionCost != 0)
			{
				string text = StringUtils.SafeFormat(Locale.Get("TOOL_CONSTRUCTION_COST"), constructionCost / 100);
				base.ShowToolInfo(true, text, this.m_cachedPosition);
			}
			else
			{
				base.ShowToolInfo(true, null, this.m_cachedPosition);
			}
		}
		else
		{
			base.ShowToolInfo(false, null, this.m_cachedPosition);
		}
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		if (this.m_mode == TreeTool.Mode.Brush)
		{
			this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
		}
		else
		{
			this.m_toolController.SetBrush(null, Vector3.get_zero(), 1f);
		}
		this.m_cachedPosition = this.m_mousePosition;
		ToolBase.OverrideInfoMode = false;
	}

	[DebuggerHidden]
	private IEnumerator CreateTree()
	{
		TreeTool.<CreateTree>c__Iterator0 <CreateTree>c__Iterator = new TreeTool.<CreateTree>c__Iterator0();
		<CreateTree>c__Iterator.$this = this;
		return <CreateTree>c__Iterator;
	}

	public static void DispatchPlacementEffect(Vector3 pos, bool bulldozing)
	{
		EffectInfo effectInfo;
		if (bulldozing)
		{
			effectInfo = Singleton<TreeManager>.get_instance().m_properties.m_bulldozeEffect;
		}
		else
		{
			effectInfo = Singleton<TreeManager>.get_instance().m_properties.m_placementEffect;
		}
		if (effectInfo != null)
		{
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(pos, Vector3.get_up(), 1f);
			Singleton<EffectManager>.get_instance().DispatchEffect(effectInfo, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
		}
	}

	public override void SimulationStep()
	{
		if (this.m_prefab == null)
		{
			this.m_wasPrefab = null;
			this.m_treeInfo = null;
			return;
		}
		if (this.m_treeInfo == null || this.m_wasPrefab != this.m_prefab)
		{
			this.m_wasPrefab = this.m_prefab;
			if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
			{
				this.m_treeInfo = this.m_prefab;
			}
			else
			{
				this.m_treeInfo = this.m_prefab.GetVariation(ref this.m_randomizer);
			}
		}
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		if (this.m_mode == TreeTool.Mode.Single && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			input.m_currentEditObject = true;
		}
		ulong[] collidingSegmentBuffer;
		ulong[] collidingBuildingBuffer;
		this.m_toolController.BeginColliding(out collidingSegmentBuffer, out collidingBuildingBuffer);
		try
		{
			ToolBase.RaycastOutput raycastOutput;
			if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
			{
				if (this.m_mode == TreeTool.Mode.Brush)
				{
					this.m_mousePosition = raycastOutput.m_hitPos;
					if (Singleton<TreeManager>.get_instance().CheckLimits())
					{
						this.m_placementErrors = ToolBase.ToolErrors.Pending;
					}
					else
					{
						this.m_placementErrors = ToolBase.ToolErrors.TooManyObjects;
					}
					if (this.m_mouseLeftDown != this.m_mouseRightDown)
					{
						this.ApplyBrush();
					}
				}
				else if (this.m_mode == TreeTool.Mode.Single)
				{
					if (!raycastOutput.m_currentEditObject)
					{
						raycastOutput.m_hitPos.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(raycastOutput.m_hitPos);
					}
					Randomizer randomizer = this.m_randomizer;
					uint id = Singleton<TreeManager>.get_instance().m_trees.NextFreeItem(ref randomizer);
					ToolBase.ToolErrors toolErrors = TreeTool.CheckPlacementErrors(this.m_treeInfo, raycastOutput.m_hitPos, raycastOutput.m_currentEditObject, id, collidingSegmentBuffer, collidingBuildingBuffer);
					bool flag = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None;
					if (flag)
					{
						int constructionCost = this.m_treeInfo.GetConstructionCost();
						if (constructionCost != 0 && constructionCost != Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost))
						{
							toolErrors |= ToolBase.ToolErrors.NotEnoughMoney;
						}
					}
					if (!Singleton<TreeManager>.get_instance().CheckLimits())
					{
						toolErrors |= ToolBase.ToolErrors.TooManyObjects;
					}
					this.m_mousePosition = raycastOutput.m_hitPos;
					this.m_placementErrors = toolErrors;
					this.m_fixedHeight = raycastOutput.m_currentEditObject;
				}
			}
			else
			{
				this.m_placementErrors = ToolBase.ToolErrors.RaycastFailed;
			}
		}
		finally
		{
			this.m_toolController.EndColliding();
		}
	}

	public static ToolBase.ToolErrors CheckPlacementErrors(TreeInfo info, Vector3 position, bool fixedHeight, uint id, ulong[] collidingSegmentBuffer, ulong[] collidingBuildingBuffer)
	{
		Randomizer randomizer;
		randomizer..ctor(id);
		float num = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num2 = info.m_generatedInfo.m_size.y * num;
		float num3 = 0.3f;
		Vector2 vector = VectorUtils.XZ(position);
		Quad2 quad = default(Quad2);
		quad.a = vector + new Vector2(-num3, -num3);
		quad.b = vector + new Vector2(-num3, num3);
		quad.c = vector + new Vector2(num3, num3);
		quad.d = vector + new Vector2(num3, -num3);
		float y = position.y;
		float maxY = position.y + num2;
		ItemClass.CollisionType collisionType = ItemClass.CollisionType.Terrain;
		if (fixedHeight)
		{
			collisionType = ItemClass.CollisionType.Elevated;
		}
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if (!Singleton<ToolManager>.get_instance().m_properties.m_disablePropCollisions)
		{
			if (Singleton<PropManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, 0, 0))
			{
				toolErrors |= ToolBase.ToolErrors.ObjectCollision;
			}
			if (Singleton<TreeManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, 0, 0u))
			{
				toolErrors |= ToolBase.ToolErrors.ObjectCollision;
			}
		}
		if (Singleton<NetManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0, collidingSegmentBuffer))
		{
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) == ItemClass.Availability.None && Singleton<BuildingManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0, collidingBuildingBuffer))
		{
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if (Singleton<TerrainManager>.get_instance().HasWater(vector))
		{
			toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
		}
		if (Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad))
		{
			toolErrors |= ToolBase.ToolErrors.OutOfArea;
		}
		return toolErrors;
	}

	private void ApplyBrush()
	{
		float[] brushData = this.m_toolController.BrushData;
		float num = this.m_brushSize * 0.5f;
		float num2 = 32f;
		int num3 = 540;
		TreeInstance[] buffer = Singleton<TreeManager>.get_instance().m_trees.m_buffer;
		uint[] treeGrid = Singleton<TreeManager>.get_instance().m_treeGrid;
		float strength = this.m_strength;
		Vector3 mousePosition = this.m_mousePosition;
		int num4 = Mathf.Max((int)((mousePosition.x - num) / num2 + (float)num3 * 0.5f), 0);
		int num5 = Mathf.Max((int)((mousePosition.z - num) / num2 + (float)num3 * 0.5f), 0);
		int num6 = Mathf.Min((int)((mousePosition.x + num) / num2 + (float)num3 * 0.5f), num3 - 1);
		int num7 = Mathf.Min((int)((mousePosition.z + num) / num2 + (float)num3 * 0.5f), num3 - 1);
		for (int i = num5; i <= num7; i++)
		{
			float num8 = (((float)i - (float)num3 * 0.5f + 0.5f) * num2 - mousePosition.z + num) / this.m_brushSize * 64f - 0.5f;
			int num9 = Mathf.Clamp(Mathf.FloorToInt(num8), 0, 63);
			int num10 = Mathf.Clamp(Mathf.CeilToInt(num8), 0, 63);
			for (int j = num4; j <= num6; j++)
			{
				float num11 = (((float)j - (float)num3 * 0.5f + 0.5f) * num2 - mousePosition.x + num) / this.m_brushSize * 64f - 0.5f;
				int num12 = Mathf.Clamp(Mathf.FloorToInt(num11), 0, 63);
				int num13 = Mathf.Clamp(Mathf.CeilToInt(num11), 0, 63);
				float num14 = brushData[num9 * 64 + num12];
				float num15 = brushData[num9 * 64 + num13];
				float num16 = brushData[num10 * 64 + num12];
				float num17 = brushData[num10 * 64 + num13];
				float num18 = num14 + (num15 - num14) * (num11 - (float)num12);
				float num19 = num16 + (num17 - num16) * (num11 - (float)num12);
				float num20 = num18 + (num19 - num18) * (num8 - (float)num9);
				int num21 = (int)(strength * (num20 * 1.2f - 0.2f) * 10000f);
				if (this.m_mouseLeftDown && this.m_prefab != null)
				{
					if (this.m_randomizer.Int32(10000u) < num21)
					{
						TreeInfo treeInfo;
						if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
						{
							treeInfo = this.m_prefab;
						}
						else
						{
							treeInfo = this.m_prefab.GetVariation(ref this.m_randomizer);
						}
						Vector3 vector;
						vector.x = ((float)j - (float)num3 * 0.5f) * num2;
						vector.z = ((float)i - (float)num3 * 0.5f) * num2;
						vector.x += ((float)this.m_randomizer.Int32(10000u) + 0.5f) * (num2 / 10000f);
						vector.z += ((float)this.m_randomizer.Int32(10000u) + 0.5f) * (num2 / 10000f);
						vector.y = 0f;
						float num22;
						float num23;
						vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector, out num22, out num23);
						if (Mathf.Max(Mathf.Abs(num22), Mathf.Abs(num23)) < (float)this.m_randomizer.Int32(10000u) * 5E-05f)
						{
							Randomizer randomizer = this.m_randomizer;
							uint num24 = Singleton<TreeManager>.get_instance().m_trees.NextFreeItem(ref randomizer);
							Randomizer randomizer2;
							randomizer2..ctor(num24);
							float num25 = this.m_treeInfo.m_minScale + (float)randomizer2.Int32(10000u) * (this.m_treeInfo.m_maxScale - this.m_treeInfo.m_minScale) * 0.0001f;
							float num26 = this.m_treeInfo.m_generatedInfo.m_size.y * num25;
							float num27 = 4.5f;
							Vector2 vector2 = VectorUtils.XZ(vector);
							Quad2 quad = default(Quad2);
							quad.a = vector2 + new Vector2(-num27, -num27);
							quad.b = vector2 + new Vector2(-num27, num27);
							quad.c = vector2 + new Vector2(num27, num27);
							quad.d = vector2 + new Vector2(num27, -num27);
							Quad2 quad2 = default(Quad2);
							quad2.a = vector2 + new Vector2(-8f, -8f);
							quad2.b = vector2 + new Vector2(-8f, 8f);
							quad2.c = vector2 + new Vector2(8f, 8f);
							quad2.d = vector2 + new Vector2(8f, -8f);
							float y = mousePosition.y;
							float maxY = mousePosition.y + num26;
							ItemClass.CollisionType collisionType = ItemClass.CollisionType.Terrain;
							if (!Singleton<PropManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, 0, 0))
							{
								if (!Singleton<TreeManager>.get_instance().OverlapQuad(quad2, y, maxY, collisionType, 0, 0u))
								{
									if (!Singleton<NetManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, treeInfo.m_class.m_layer, 0, 0, 0))
									{
										if (!Singleton<BuildingManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, treeInfo.m_class.m_layer, 0, 0, 0))
										{
											if (!Singleton<TerrainManager>.get_instance().HasWater(vector2))
											{
												if (!Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad))
												{
													uint num28;
													if (Singleton<TreeManager>.get_instance().CreateTree(out num28, ref this.m_randomizer, treeInfo, vector, false))
													{
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else if (this.m_mouseRightDown || this.m_prefab == null)
				{
					uint num29 = treeGrid[i * num3 + j];
					int num30 = 0;
					while (num29 != 0u)
					{
						uint nextGridTree = buffer[(int)((UIntPtr)num29)].m_nextGridTree;
						if (this.m_randomizer.Int32(10000u) < num21)
						{
							Singleton<TreeManager>.get_instance().ReleaseTree(num29);
						}
						num29 = nextGridTree;
						if (++num30 >= 262144)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_placementErrors;
	}

	public TreeInfo m_prefab;

	public TreeTool.Mode m_mode;

	public float m_brushSize = 200f;

	public float m_strength = 0.1f;

	public Texture2D m_brush;

	public CursorInfo m_buildCursor;

	private TreeInfo m_wasPrefab;

	private TreeInfo m_treeInfo;

	private Vector3 m_mousePosition;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private bool m_mouseLeftDown;

	private bool m_mouseRightDown;

	private ToolBase.ToolErrors m_placementErrors;

	private bool m_fixedHeight;

	private Vector3 m_cachedPosition;

	private bool m_mouseRayValid;

	private Randomizer m_randomizer;

	public enum Mode
	{
		Brush,
		Single
	}
}
