﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class LoadScenarioPanel : LoadSavePanelBase<ScenarioMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_SaveList = base.Find<UIListBox>("SaveList");
		this.m_SaveList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnListingSelectionChanged));
		this.m_LoadButton = base.Find<UIButton>("Load");
		this.m_CityName = base.Find<UILabel>("CityName");
		this.m_MapTheme = base.Find<UILabel>("MapTheme");
		this.m_MapThemeLabel = base.Find<UILabel>("MapThemeLabel");
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_AchNope = base.Find<UISprite>("AchNope");
		this.m_Ach = base.Find("Ach");
		this.m_AchAvLabel = base.Find("AchAvLabel");
		this.m_Population = base.Find<UILabel>("Population");
		this.m_PopulationLabel = base.Find<UILabel>("PopulationLabel");
		this.m_Cash = base.Find<UILabel>("Money");
		this.m_CashLabel = base.Find<UILabel>("MoneyLabel");
		this.m_ThemeOverridePanel = base.Find<UIPanel>("OverridePanel");
		this.m_Themes = new Dictionary<string, List<MapThemeMetaData>>();
		this.m_ThemeOverrideDropDown = base.Find<UIDropDown>("OverrideMapTheme");
		this.ClearInfo();
	}

	private void ClearInfo()
	{
		if (this.m_SnapShotSprite.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShotSprite.get_texture());
		}
		this.m_CityName.set_text("-");
		this.m_MapTheme.set_text(string.Empty);
		this.m_Ach.set_tooltip(string.Empty);
		this.m_AchAvLabel.set_tooltip(string.Empty);
	}

	private void OnListingSelectionChanged(UIComponent comp, int sel)
	{
		if (sel > -1)
		{
			ScenarioMetaData listingMetaData = base.GetListingMetaData(sel);
			string cityName = listingMetaData.cityName;
			this.m_CityName.set_text((cityName == null) ? "Unknown" : cityName);
			string population = listingMetaData.population;
			if (!string.IsNullOrEmpty(population))
			{
				this.m_Population.Show();
				this.m_PopulationLabel.Show();
				this.m_Population.set_text(population);
			}
			else
			{
				this.m_Population.Hide();
				this.m_PopulationLabel.Hide();
			}
			string cash = listingMetaData.cash;
			if (!string.IsNullOrEmpty(cash))
			{
				this.m_Cash.Show();
				this.m_CashLabel.Show();
				this.m_Cash.set_text(cash);
			}
			else
			{
				this.m_Cash.Hide();
				this.m_CashLabel.Hide();
			}
			string themeString = LoadSavePanelBase<ScenarioMetaData>.GetThemeString(null, listingMetaData.environment, null);
			if (!string.IsNullOrEmpty(themeString))
			{
				this.m_MapTheme.Show();
				this.m_MapThemeLabel.Show();
				this.m_MapTheme.set_text(themeString);
				this.m_SelectedEnvironment = listingMetaData.environment;
				if (this.m_ThemeOverridePanel != null)
				{
					base.RefreshOverrideThemes();
					base.SelectThemeOverride(listingMetaData.mapThemeRef);
					this.m_ThemeOverridePanel.Show();
				}
			}
			else
			{
				this.m_MapThemeLabel.Hide();
				this.m_MapTheme.Hide();
				if (this.m_ThemeOverridePanel != null)
				{
					this.m_ThemeOverridePanel.Hide();
				}
			}
			if (this.m_SnapShotSprite.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShotSprite.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShotSprite.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShotSprite.get_texture() != null)
				{
					this.m_SnapShotSprite.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShotSprite.set_texture(null);
			}
			Package.Asset listingData = base.GetListingData(sel);
			ScenarioMetaData listingMetaData2 = base.GetListingMetaData(sel);
			bool flag = Singleton<PluginManager>.get_instance().get_enabledModCount() > 0 || listingData.get_isWorkshopAsset() || listingMetaData2.achievementsDisabled;
			this.m_AchNope.set_isVisible(flag);
			string text = string.Empty;
			if (flag)
			{
				text = Locale.Get("LOADPANEL_ACHSTATUS_DISABLED");
				text += "<color #50869a>";
				if (Singleton<PluginManager>.get_instance().get_enabledModCount() > 0)
				{
					text += Environment.NewLine;
					text += LocaleFormatter.FormatGeneric("LOADPANEL_ACHSTATUS_MODSACTIVE", new object[]
					{
						Singleton<PluginManager>.get_instance().get_enabledModCount()
					});
				}
				if (listingData.get_isWorkshopAsset())
				{
					text += Environment.NewLine;
					text += Locale.Get("LOADPANEL_ACHSTATUS_WORKSHOP");
				}
				if (listingMetaData2.achievementsDisabled)
				{
					text += Environment.NewLine;
					text += Locale.Get("LOADPANEL_ACHSTATUS_PREVMAP");
				}
				text += "</color>";
			}
			else
			{
				text = Locale.Get("LOADPANEL_ACHSTATUS_ENABLED");
			}
			this.m_AchAvLabel.set_tooltip(text);
			this.m_Ach.set_tooltip(text);
		}
		else
		{
			this.ClearInfo();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			MainMenu.SetFocus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveList.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnLoad();
			}
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_SaveList && sel != -1)
		{
			if (this.m_SaveList.get_selectedIndex() != sel)
			{
				this.m_SaveList.set_selectedIndex(sel);
			}
			this.OnLoad();
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("LoadPanel.Refresh()"))
		{
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.ScenarioMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
						if ((scenarioMetaData.environment != "Winter" || flag) && (!scenarioMetaData.IsBuiltinScenario(current.get_package().get_packagePath()) || this.m_showBuiltinScenarios))
						{
							base.AddToListing(current.get_name(), scenarioMetaData.timeStamp, current, scenarioMetaData, true);
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			this.m_SaveList.set_items(base.GetListingItems());
			if (this.m_SaveList.get_items().Length > 0)
			{
				int num = base.FindIndexOf(LoadScenarioPanel.m_LastSaveName);
				this.m_SaveList.set_selectedIndex((num == -1) ? 0 : num);
				this.m_LoadButton.set_isEnabled(true);
			}
			else
			{
				this.m_LoadButton.set_isEnabled(false);
			}
			base.RebuildThemeDictionary();
		}
	}

	public void OnLoad()
	{
		ScenarioMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
		if (this.m_ThemeOverrideDropDown != null && this.m_Themes != null)
		{
			if (this.m_ThemeOverrideDropDown.get_items().Length > 0 && this.m_ThemeOverrideDropDown.get_selectedIndex() > 0 && !string.IsNullOrEmpty(this.m_SelectedEnvironment) && this.m_Themes.ContainsKey(this.m_SelectedEnvironment) && this.m_Themes[this.m_SelectedEnvironment].Count >= this.m_ThemeOverrideDropDown.get_selectedIndex())
			{
				listingMetaData.mapThemeRef = this.m_Themes[this.m_SelectedEnvironment][this.m_ThemeOverrideDropDown.get_selectedIndex() - 1].mapThemeRef;
			}
			else if (this.m_ThemeOverrideDropDown.get_selectedIndex() == 0)
			{
				listingMetaData.mapThemeRef = null;
			}
		}
		if (base.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.LoadRoutine();
				}
			});
		}
		else
		{
			this.LoadRoutine();
		}
	}

	protected void LoadRoutine()
	{
		if (!SaveScenarioPanel.isSaving && Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			base.CloseEverything();
			LoadScenarioPanel.m_LastSaveName = base.GetListingName(this.m_SaveList.get_selectedIndex());
			SaveScenarioPanel.lastLoadedName = LoadScenarioPanel.m_LastSaveName;
			ScenarioMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
			base.PrintModsInfo(listingMetaData.mods);
			Package.Asset listingData = base.GetListingData(this.m_SaveList.get_selectedIndex());
			SimulationMetaData simulationMetaData = new SimulationMetaData
			{
				m_CityName = listingMetaData.cityName,
				m_updateMode = SimulationManager.UpdateMode.LoadGame,
				m_environment = this.m_forceEnvironment
			};
			if (Singleton<PluginManager>.get_instance().get_enabledModCount() > 0 || (listingData.get_package() != null && listingData.get_package().GetPublishedFileID() != PublishedFileId.invalid))
			{
				simulationMetaData.m_disableAchievements = SimulationMetaData.MetaBool.True;
			}
			if (listingMetaData.mapThemeRef != null)
			{
				Package.Asset asset = PackageManager.FindAssetByName(listingMetaData.mapThemeRef);
				if (asset != null)
				{
					simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
					simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
				}
			}
			simulationMetaData.m_updateMode = SimulationManager.UpdateMode.LoadScenario;
			Singleton<LoadingManager>.get_instance().LoadLevel(listingData, "ScenarioEditor", "InScenarioEditor", simulationMetaData);
			UIView.get_library().Hide(base.GetType().Name, 1);
		}
	}

	public string m_forceEnvironment;

	private static string m_LastSaveName;

	private UIListBox m_SaveList;

	private UIButton m_LoadButton;

	private UITextureSprite m_SnapShotSprite;

	private UILabel m_CityName;

	private UILabel m_MapTheme;

	private UILabel m_MapThemeLabel;

	private UISprite m_AchNope;

	private UIComponent m_Ach;

	private UIComponent m_AchAvLabel;

	private UILabel m_Population;

	private UILabel m_PopulationLabel;

	private UILabel m_Cash;

	private UILabel m_CashLabel;

	public bool m_showBuiltinScenarios;
}
