﻿using System;
using ColossalFramework;
using UnityEngine;

public class CitizenInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_skinRenderer != null)
		{
			Material sharedMaterial = this.m_skinRenderer.get_sharedMaterial();
			this.m_color0 = sharedMaterial.GetColor("_ColorV0");
			this.m_color1 = sharedMaterial.GetColor("_ColorV1");
			this.m_color2 = sharedMaterial.GetColor("_ColorV2");
			this.m_color3 = sharedMaterial.GetColor("_ColorV3");
		}
		else
		{
			this.m_color0 = Color.get_white();
			this.m_color1 = Color.get_white();
			this.m_color2 = Color.get_white();
			this.m_color3 = Color.get_white();
		}
		if (this.m_lodObject != null)
		{
			MeshFilter component = this.m_lodObject.GetComponent<MeshFilter>();
			this.m_lodMesh = component.get_sharedMesh();
			this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
			this.GenerateCombinedLodMesh();
		}
		else
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		this.RefreshLevelOfDetail();
		if (this.m_citizenAI == null)
		{
			this.m_citizenAI = base.GetComponent<CitizenAI>();
			this.m_citizenAI.m_info = this;
			this.m_citizenAI.InitializeAI();
		}
		this.m_lodLocations = new Matrix4x4[16];
		this.m_lodColors = new Vector4[16];
		this.m_lodCount = 0;
		this.m_lodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
		this.m_undergroundLodLocations = new Matrix4x4[16];
		this.m_undergroundLodColors = new Vector4[16];
		this.m_undergroundLodCount = 0;
		this.m_undergroundLodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_undergroundLodMax = new Vector3(-100000f, -100000f, -100000f);
	}

	public override void DestroyPrefab()
	{
		if (this.m_lodMeshCombined1 != null)
		{
			Object.Destroy(this.m_lodMeshCombined1);
			this.m_lodMeshCombined1 = null;
		}
		if (this.m_lodMeshCombined4 != null)
		{
			Object.Destroy(this.m_lodMeshCombined4);
			this.m_lodMeshCombined4 = null;
		}
		if (this.m_lodMeshCombined8 != null)
		{
			Object.Destroy(this.m_lodMeshCombined8);
			this.m_lodMeshCombined8 = null;
		}
		if (this.m_lodMeshCombined16 != null)
		{
			Object.Destroy(this.m_lodMeshCombined16);
			this.m_lodMeshCombined16 = null;
		}
		if (this.m_lodMaterialCombined != null)
		{
			Object.Destroy(this.m_lodMaterialCombined);
			this.m_lodMaterialCombined = null;
		}
		if (this.m_undergroundLodMaterial != null)
		{
			Object.Destroy(this.m_undergroundLodMaterial);
			this.m_undergroundLodMaterial = null;
		}
		if (this.m_citizenAI != null)
		{
			this.m_citizenAI.ReleaseAI();
			this.m_citizenAI = null;
		}
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		float levelOfDetailFactor = RenderManager.LevelOfDetailFactor;
		if (this.m_lodObject != null)
		{
			this.m_lodRenderDistance = levelOfDetailFactor * 150f;
		}
		else
		{
			this.m_lodRenderDistance = 10000f;
		}
		this.m_maxRenderDistance = levelOfDetailFactor * 400f;
	}

	public override void InitializePrefabInstance(PrefabInfo prefabInfo)
	{
		base.InitializePrefabInstance(prefabInfo);
		this.m_animator = base.get_gameObject().GetComponent<Animator>();
		this.m_citizenAI = base.get_gameObject().GetComponent<CitizenAI>();
		if (this.m_citizenAI != null)
		{
			this.m_citizenAI.m_info = this;
		}
		this.m_renderer = base.get_gameObject().GetComponent<SkinnedMeshRenderer>();
		if (this.m_renderer == null)
		{
			for (int i = 0; i < base.get_gameObject().get_transform().get_childCount(); i++)
			{
				Transform child = base.get_gameObject().get_transform().GetChild(i);
				this.m_renderer = child.GetComponent<Renderer>();
				if (this.m_renderer != null)
				{
					break;
				}
			}
		}
		if (this.m_renderer != null)
		{
			this.m_skinRenderer = (this.m_renderer as SkinnedMeshRenderer);
			this.m_material = new Material(this.m_renderer.get_sharedMaterial());
			this.m_renderer.set_sharedMaterial(this.m_material);
		}
	}

	public override void DestroyPrefabInstance()
	{
		if (this.m_material != null)
		{
			Object.Destroy(this.m_material);
			this.m_material = null;
		}
		if (this.m_undergroundMaterial != null)
		{
			Object.Destroy(this.m_undergroundMaterial);
			this.m_undergroundMaterial = null;
		}
		this.m_renderer = null;
		this.m_animator = null;
		base.DestroyPrefabInstance();
	}

	public override void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
		Vector3 position;
		position..ctor(0f, 60f, 0f);
		Animator component = base.GetComponent<Animator>();
		float @float = component.GetFloat(Singleton<CitizenManager>.get_instance().ID_Speed);
		if (@float < 0.1f)
		{
			component.set_speed((1f - @float * 9f) * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		else
		{
			component.set_speed(@float * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		CitizenInstance.RenderInstance(cameraInfo, this, position);
	}

	public void SetRenderParameters(Vector3 position, Quaternion rotation, Vector3 velocity, Color color, int state, bool underground)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		CitizenManager expr_0C_cp_0 = instance;
		expr_0C_cp_0.m_drawCallData.m_defaultCalls = expr_0C_cp_0.m_drawCallData.m_defaultCalls + 1;
		Transform transform = base.get_transform();
		transform.set_position(position);
		transform.set_rotation(rotation);
		float magnitude = velocity.get_magnitude();
		this.m_animator.SetFloat(instance.ID_Speed, magnitude);
		this.m_animator.SetInteger(instance.ID_State, state);
		if (this.m_instanceChanged)
		{
			this.m_instanceChanged = false;
			switch (state)
			{
			case 0:
				if (magnitude > 0.1f)
				{
					this.m_animator.Play("walk 0", -1, Random.get_value());
				}
				else
				{
					this.m_animator.Play("idle", -1, Random.get_value());
				}
				break;
			case 1:
				if (magnitude > 0.1f)
				{
					this.m_animator.Play("walk 0", -1, Random.get_value());
				}
				else
				{
					this.m_animator.Play("panic", -1, Random.get_value());
				}
				break;
			case 2:
				this.m_animator.Play("sitting idle", -1, Random.get_value());
				break;
			case 3:
				if (magnitude > 0.1f)
				{
					this.m_animator.Play("bike-ride", -1, Random.get_value());
				}
				else
				{
					this.m_animator.Play("bike-idle", -1, Random.get_value());
				}
				break;
			case 4:
				if (magnitude > 0.1f)
				{
					this.m_animator.Play("walk 0", -1, Random.get_value());
				}
				else
				{
					this.m_animator.Play("idle2", -1, Random.get_value());
				}
				break;
			case 5:
				if (magnitude > 0.1f)
				{
					this.m_animator.Play("walk 0", -1, Random.get_value());
				}
				else
				{
					this.m_animator.Play("concert", -1, Random.get_value());
				}
				break;
			}
		}
		if (magnitude < 0.1f)
		{
			this.m_animator.set_speed((1f - magnitude * 9f) * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		else
		{
			this.m_animator.set_speed(magnitude * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		if (underground != this.m_undergroundMode)
		{
			this.m_undergroundMode = underground;
			if (underground && this.m_undergroundMaterial == null && this.m_material != null)
			{
				CitizenProperties properties = Singleton<CitizenManager>.get_instance().m_properties;
				if (properties != null)
				{
					this.m_undergroundMaterial = new Material(properties.m_undergroundShader);
					this.m_undergroundMaterial.CopyPropertiesFromMaterial(this.m_material);
				}
			}
			if (underground)
			{
				if (this.m_renderer != null)
				{
					if (this.m_undergroundMaterial != null)
					{
						this.m_renderer.set_sharedMaterial(this.m_undergroundMaterial);
					}
					this.m_renderer.get_gameObject().set_layer(Singleton<CitizenManager>.get_instance().m_undergroundLayer);
				}
			}
			else if (this.m_renderer != null)
			{
				if (this.m_material != null)
				{
					this.m_renderer.set_sharedMaterial(this.m_material);
				}
				this.m_renderer.get_gameObject().set_layer(Singleton<CitizenManager>.get_instance().m_citizenLayer);
			}
		}
		if (underground)
		{
			if (this.m_undergroundMaterial != null)
			{
				this.m_undergroundMaterial.set_color(color);
			}
		}
		else if (this.m_material != null)
		{
			this.m_material.set_color(color);
		}
	}

	private void GenerateCombinedLodMesh()
	{
		if (this.m_lodMeshCombined1 == null)
		{
			this.m_lodMeshCombined1 = new Mesh();
		}
		if (this.m_lodMeshCombined4 == null)
		{
			this.m_lodMeshCombined4 = new Mesh();
		}
		if (this.m_lodMeshCombined8 == null)
		{
			this.m_lodMeshCombined8 = new Mesh();
		}
		if (this.m_lodMeshCombined16 == null)
		{
			this.m_lodMeshCombined16 = new Mesh();
		}
		if (this.m_lodMaterialCombined == null)
		{
			string[] shaderKeywords;
			if (this.m_lodMaterial != null)
			{
				this.m_lodMaterialCombined = new Material(this.m_lodMaterial);
				shaderKeywords = this.m_lodMaterial.get_shaderKeywords();
			}
			else
			{
				this.m_lodMaterialCombined = new Material(this.m_material);
				shaderKeywords = this.m_material.get_shaderKeywords();
			}
			for (int i = 0; i < shaderKeywords.Length; i++)
			{
				this.m_lodMaterialCombined.EnableKeyword(shaderKeywords[i]);
			}
			this.m_lodMaterialCombined.EnableKeyword("MULTI_INSTANCE");
		}
		Vector3[] vertices = this.m_lodMesh.get_vertices();
		Vector3[] normals = this.m_lodMesh.get_normals();
		Vector4[] tangents = this.m_lodMesh.get_tangents();
		Vector2[] uv = this.m_lodMesh.get_uv();
		Color32[] array = this.m_lodMesh.get_colors32();
		int[] triangles = this.m_lodMesh.get_triangles();
		if (array.Length != vertices.Length)
		{
			array = new Color32[vertices.Length];
			for (int j = 0; j < array.Length; j++)
			{
				array[j] = new Color32(255, 255, 255, 255);
			}
		}
		int num = vertices.Length;
		int num2 = triangles.Length;
		if (num * 16 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num, num2);
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 4, num2 * 4);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 8, num2 * 8);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 16, num2 * 16);
		int num3 = 0;
		int num4 = 0;
		for (int k = 0; k < 16; k++)
		{
			byte a = (byte)(k * 16);
			for (int l = 0; l < num2; l++)
			{
				if (k < 1)
				{
					meshData.m_triangles[num4] = triangles[l] + num3;
				}
				if (k < 4)
				{
					meshData2.m_triangles[num4] = triangles[l] + num3;
				}
				if (k < 8)
				{
					meshData3.m_triangles[num4] = triangles[l] + num3;
				}
				if (k < 16)
				{
					meshData4.m_triangles[num4] = triangles[l] + num3;
				}
				num4++;
			}
			for (int m = 0; m < num; m++)
			{
				Color32 color = array[m];
				color.a = a;
				if (k < 1)
				{
					meshData.m_vertices[num3] = vertices[m];
					meshData.m_normals[num3] = normals[m];
					meshData.m_tangents[num3] = tangents[m];
					meshData.m_uvs[num3] = uv[m];
					meshData.m_colors[num3] = color;
				}
				if (k < 4)
				{
					meshData2.m_vertices[num3] = vertices[m];
					meshData2.m_normals[num3] = normals[m];
					meshData2.m_tangents[num3] = tangents[m];
					meshData2.m_uvs[num3] = uv[m];
					meshData2.m_colors[num3] = color;
				}
				if (k < 8)
				{
					meshData3.m_vertices[num3] = vertices[m];
					meshData3.m_normals[num3] = normals[m];
					meshData3.m_tangents[num3] = tangents[m];
					meshData3.m_uvs[num3] = uv[m];
					meshData3.m_colors[num3] = color;
				}
				if (k < 16)
				{
					meshData4.m_vertices[num3] = vertices[m];
					meshData4.m_normals[num3] = normals[m];
					meshData4.m_tangents[num3] = tangents[m];
					meshData4.m_uvs[num3] = uv[m];
					meshData4.m_colors[num3] = color;
				}
				num3++;
			}
		}
		meshData.PopulateMesh(this.m_lodMeshCombined1);
		meshData2.PopulateMesh(this.m_lodMeshCombined4);
		meshData3.PopulateMesh(this.m_lodMeshCombined8);
		meshData4.PopulateMesh(this.m_lodMeshCombined16);
	}

	public override PrefabAI GetAI()
	{
		return this.m_citizenAI;
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public bool InitializeCustomPrefab(CustomAssetMetaData meta)
	{
		GameObject gameObject = meta.assetRef.get_package().Instantiate<GameObject>("SkinnedMeshRenderer");
		gameObject.get_transform().set_parent(base.get_transform());
		this.m_skinRenderer = gameObject.GetComponent<SkinnedMeshRenderer>();
		BoneTransformHierarchy boneTransformHierarchy = meta.assetRef.get_package().Instantiate<BoneTransformHierarchy>("BoneTransformHierarchy");
		boneTransformHierarchy.ExtractData(base.get_gameObject());
		if (meta.templateName == "Fireman Hose")
		{
			FiremanAI component = base.GetComponent<FiremanAI>();
			component.m_hoseBone = base.get_transform().Find("Hose");
			Transform transform = new GameObject().get_transform();
			transform.set_parent(base.get_transform());
			transform.set_localPosition(new Vector3(-0.1631431f, 0.1477886f, 0.1434195f));
			transform.set_eulerAngles(new Vector3(-23.036f, -7.04f, -11.19f));
			component.m_waterEffect = EffectCollection.FindEffect("Fireman Water");
		}
		else if (meta.templateName == "Cow")
		{
			LivestockAI component2 = base.GetComponent<LivestockAI>();
			component2.m_randomEffect = EffectCollection.FindEffect("Cow Random Effect");
		}
		else if (meta.templateName == "Pig")
		{
			LivestockAI component3 = base.GetComponent<LivestockAI>();
			component3.m_randomEffect = EffectCollection.FindEffect("Pig Random Effect");
		}
		else if (meta.templateName == "RescueDog")
		{
			RescueAnimalAI component4 = base.GetComponent<RescueAnimalAI>();
			component4.m_detectionEffect = EffectCollection.FindEffect("Rescue Dog");
		}
		else if (meta.templateName == "Seagull")
		{
			BirdAI component5 = base.GetComponent<BirdAI>();
			component5.m_randomEffect = EffectCollection.FindEffect("Seagull Random Effect");
		}
		int num = PrefabCollection<CitizenInfo>.LoadedCount();
		uint num2 = 0u;
		while ((ulong)num2 < (ulong)((long)num))
		{
			CitizenInfo loaded = PrefabCollection<CitizenInfo>.GetLoaded(num2);
			if (loaded != null && loaded.get_name() == meta.templateName)
			{
				Animator component6 = base.GetComponent<Animator>();
				Animator component7 = loaded.GetComponent<Animator>();
				component6.set_runtimeAnimatorController(component7.get_runtimeAnimatorController());
				component6.set_avatar(component7.get_avatar());
				component6.set_cullingMode(1);
				return true;
			}
			num2 += 1u;
		}
		return false;
	}

	public Citizen.Gender m_gender;

	public Citizen.SubCulture m_subCulture;

	public Citizen.AgePhase m_agePhase;

	public ItemClass m_class;

	public ItemClass.Placement m_placementStyle;

	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	public SkinnedMeshRenderer m_skinRenderer;

	public GameObject m_lodObject;

	public float m_walkSpeed = 3f;

	public float m_radius = 0.4f;

	public float m_height = 1.8f;

	[NonSerialized]
	public Mesh m_lodMesh;

	[NonSerialized]
	public Material m_lodMaterial;

	[NonSerialized]
	public Color m_color0;

	[NonSerialized]
	public Color m_color1;

	[NonSerialized]
	public Color m_color2;

	[NonSerialized]
	public Color m_color3;

	[NonSerialized]
	public float m_maxRenderDistance;

	[NonSerialized]
	public float m_lodRenderDistance;

	[NonSerialized]
	public CitizenAI m_citizenAI;

	[NonSerialized]
	private bool m_undergroundMode;

	[NonSerialized]
	private Material m_material;

	[NonSerialized]
	private Material m_undergroundMaterial;

	[NonSerialized]
	private Renderer m_renderer;

	[NonSerialized]
	private Animator m_animator;

	[NonSerialized]
	public Mesh m_lodMeshCombined1;

	[NonSerialized]
	public Mesh m_lodMeshCombined4;

	[NonSerialized]
	public Mesh m_lodMeshCombined8;

	[NonSerialized]
	public Mesh m_lodMeshCombined16;

	[NonSerialized]
	public Material m_lodMaterialCombined;

	[NonSerialized]
	public Matrix4x4[] m_lodLocations;

	[NonSerialized]
	public Vector4[] m_lodColors;

	[NonSerialized]
	public int m_lodCount;

	[NonSerialized]
	public Vector3 m_lodMin;

	[NonSerialized]
	public Vector3 m_lodMax;

	[NonSerialized]
	public Material m_undergroundLodMaterial;

	[NonSerialized]
	public Matrix4x4[] m_undergroundLodLocations;

	[NonSerialized]
	public Vector4[] m_undergroundLodColors;

	[NonSerialized]
	public int m_undergroundLodCount;

	[NonSerialized]
	public Vector3 m_undergroundLodMin;

	[NonSerialized]
	public Vector3 m_undergroundLodMax;
}
