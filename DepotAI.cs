﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class DepotAI : PlayerBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		float num = this.m_info.m_generatedInfo.m_max.z - 7f;
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				if (this.m_info.m_paths[i].m_netInfo != null && this.m_info.m_paths[i].m_netInfo.m_class.m_service == ItemClass.Service.Road && this.m_info.m_paths[i].m_netInfo.m_placementStyle == ItemClass.Placement.Manual && this.m_info.m_paths[i].m_nodes != null)
				{
					for (int j = 0; j < this.m_info.m_paths[i].m_nodes.Length; j++)
					{
						num = Mathf.Min(num, -16f - this.m_info.m_paths[i].m_netInfo.m_halfWidth - this.m_info.m_paths[i].m_nodes[j].z);
					}
				}
			}
		}
		this.m_quayOffset = num;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.NoisePollution;
		radius1 = this.m_noiseRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[(int)this.m_transportInfo.m_transportType];
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public int GetVehicleCount(ushort buildingID, ref Building data)
	{
		int result = 0;
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, this.m_transportInfo.m_vehicleReason, ref result, ref num, ref num2, ref num3);
		return result;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Transport;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
		if (this.m_transportInfo != null && this.m_maxVehicleCount != 0)
		{
			if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
			{
				Singleton<TransportManager>.get_instance().m_ferryDepot.Disable();
			}
			else if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Blimp)
			{
				Singleton<TransportManager>.get_instance().m_blimpDepot.Disable();
			}
		}
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == this.m_transportInfo.m_vehicleReason)
		{
			VehicleInfo randomVehicleInfo;
			if (this.m_overrideVehicleClass != null)
			{
				randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_overrideVehicleClass.m_service, this.m_overrideVehicleClass.m_subService, this.m_overrideVehicleClass.m_level);
			}
			else
			{
				randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportInfo.m_class.m_service, this.m_transportInfo.m_class.m_subService, this.m_transportInfo.m_class.m_level);
			}
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				Vector3 position;
				Vector3 vector;
				this.CalculateSpawnPosition(buildingID, ref data, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, out position, out vector);
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, position, reason, false, true))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], reason, offer);
				}
			}
		}
		else if (this.m_secondaryTransportInfo != null && reason == this.m_secondaryTransportInfo.m_vehicleReason)
		{
			VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_secondaryTransportInfo.m_class.m_service, this.m_secondaryTransportInfo.m_class.m_subService, this.m_secondaryTransportInfo.m_class.m_level);
			if (randomVehicleInfo2 != null)
			{
				Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
				Vector3 position2;
				Vector3 vector2;
				this.CalculateSpawnPosition(buildingID, ref data, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, out position2, out vector2);
				ushort num2;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, position2, reason, false, true))
				{
					randomVehicleInfo2.m_vehicleAI.SetSource(num2, ref vehicles2.m_buffer[(int)num2], buildingID);
					randomVehicleInfo2.m_vehicleAI.StartTransfer(num2, ref vehicles2.m_buffer[(int)num2], reason, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, reason, offer);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason vehicleReason = this.m_transportInfo.m_vehicleReason;
		if (vehicleReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(vehicleReason, offer);
		}
		if (this.m_secondaryTransportInfo != null)
		{
			vehicleReason = this.m_secondaryTransportInfo.m_vehicleReason;
			if (vehicleReason != TransferManager.TransferReason.None)
			{
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Building = buildingID;
				Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(vehicleReason, offer2);
			}
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void PathfindSuccess(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		base.PathfindSuccess(buildingID, ref data, type);
		if (type == BuildingAI.PathFindType.LeavingTransport)
		{
			data.m_outgoingProblemTimer = (byte)Mathf.Max(0, (int)(data.m_outgoingProblemTimer - 4));
		}
	}

	public override void PathfindFailure(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		base.PathfindFailure(buildingID, ref data, type);
		if (type == BuildingAI.PathFindType.LeavingTransport)
		{
			data.m_outgoingProblemTimer = (byte)Mathf.Min(8, (int)(data.m_outgoingProblemTimer + 2));
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if (buildingData.m_outgoingProblemTimer != 0)
		{
			byte outgoingProblemTimer;
			buildingData.m_outgoingProblemTimer = (outgoingProblemTimer = buildingData.m_outgoingProblemTimer) - 1;
			if (outgoingProblemTimer >= 4)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.DepotNotConnected);
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<BuildingManager>.get_instance().m_depotNotConnected.Activate(properties.m_depotNotConnected, buildingID);
				}
			}
			else
			{
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.DepotNotConnected);
			}
		}
		else
		{
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.DepotNotConnected);
		}
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num, buildingData.m_position, this.m_noiseRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
		TransferManager.TransferReason vehicleReason = this.m_transportInfo.m_vehicleReason;
		TransferManager.TransferReason transferReason = TransferManager.TransferReason.None;
		if (this.m_secondaryTransportInfo != null)
		{
			transferReason = this.m_secondaryTransportInfo.m_vehicleReason;
		}
		if (vehicleReason != TransferManager.TransferReason.None || transferReason != TransferManager.TransferReason.None)
		{
			int num2 = (finalProductionRate * this.m_maxVehicleCount + 99) / 100;
			int num3 = (finalProductionRate * this.m_maxVehicleCount2 + 99) / 100;
			if (this.m_transportInfo.m_transportType == TransportInfo.TransportType.Taxi)
			{
				DistrictManager instance = Singleton<DistrictManager>.get_instance();
				byte district = instance.GetDistrict(buildingData.m_position);
				District[] expr_FA_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_FA_cp_0_cp_1 = district;
				expr_FA_cp_0_cp_0[(int)expr_FA_cp_0_cp_1].m_productionData.m_tempTaxiCapacity = expr_FA_cp_0_cp_0[(int)expr_FA_cp_0_cp_1].m_productionData.m_tempTaxiCapacity + (uint)num2;
			}
			int num4 = 0;
			int num5 = 0;
			ushort num6 = 0;
			int num7 = 0;
			int num8 = 0;
			ushort num9 = 0;
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num10 = buildingData.m_ownVehicles;
			int num11 = 0;
			while (num10 != 0)
			{
				TransferManager.TransferReason transferType = (TransferManager.TransferReason)instance2.m_vehicles.m_buffer[(int)num10].m_transferType;
				if (vehicleReason != TransferManager.TransferReason.None && transferType == vehicleReason)
				{
					VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num10].Info;
					int num12;
					int num13;
					info.m_vehicleAI.GetSize(num10, ref instance2.m_vehicles.m_buffer[(int)num10], out num12, out num13);
					num4++;
					if ((instance2.m_vehicles.m_buffer[(int)num10].m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
					{
						num5++;
					}
					else if ((instance2.m_vehicles.m_buffer[(int)num10].m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
					{
						num6 = num10;
					}
					else if (instance2.m_vehicles.m_buffer[(int)num10].m_targetBuilding != 0)
					{
						num6 = num10;
					}
				}
				else if (transferReason != TransferManager.TransferReason.None && transferType == transferReason)
				{
					VehicleInfo info2 = instance2.m_vehicles.m_buffer[(int)num10].Info;
					int num14;
					int num15;
					info2.m_vehicleAI.GetSize(num10, ref instance2.m_vehicles.m_buffer[(int)num10], out num14, out num15);
					num7++;
					if ((instance2.m_vehicles.m_buffer[(int)num10].m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
					{
						num8++;
					}
					else if ((instance2.m_vehicles.m_buffer[(int)num10].m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
					{
						num9 = num10;
					}
					else if (instance2.m_vehicles.m_buffer[(int)num10].m_targetBuilding != 0)
					{
						num9 = num10;
					}
				}
				num10 = instance2.m_vehicles.m_buffer[(int)num10].m_nextOwnVehicle;
				if (++num11 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (this.m_maxVehicleCount < 16384 && num4 - num5 > num2 && num6 != 0)
			{
				VehicleInfo info3 = instance2.m_vehicles.m_buffer[(int)num6].Info;
				info3.m_vehicleAI.SetTarget(num6, ref instance2.m_vehicles.m_buffer[(int)num6], buildingID);
			}
			if (num3 < 16384 && num7 - num8 > num3 && num9 != 0)
			{
				VehicleInfo info4 = instance2.m_vehicles.m_buffer[(int)num9].Info;
				info4.m_vehicleAI.SetTarget(num9, ref instance2.m_vehicles.m_buffer[(int)num9], buildingID);
			}
			if (vehicleReason != TransferManager.TransferReason.None && num4 < num2)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 0;
				offer.Building = buildingID;
				offer.Position = buildingData.m_position;
				offer.Amount = Mathf.Min(2, num2 - num4);
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(vehicleReason, offer);
			}
			if (transferReason != TransferManager.TransferReason.None && num7 < num3)
			{
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Priority = 0;
				offer2.Building = buildingID;
				offer2.Position = buildingData.m_position;
				offer2.Amount = Mathf.Min(2, num3 - num7);
				offer2.Active = true;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(transferReason, offer2);
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			Vector3 vector;
			Vector3 vector2;
			if (this.m_spawnPoints != null && this.m_spawnPoints.Length != 0)
			{
				int num = ((int)randomizer.seed & 16777215) % this.m_spawnPoints.Length;
				vector = this.m_spawnPoints[num].m_position;
				vector2 = this.m_spawnPoints[num].m_target;
			}
			else
			{
				vector = this.m_spawnPosition;
				vector2 = this.m_spawnTarget;
			}
			position = data.CalculatePosition(vector);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(vector * 2f - vector2);
			}
			else
			{
				target = data.CalculatePosition(vector2);
			}
		}
		else if (this.m_secondaryTransportInfo != null && info.m_vehicleType == this.m_secondaryTransportInfo.m_vehicleType)
		{
			Vector3 vector3;
			Vector3 vector4;
			if (this.m_spawnPoints2 != null && this.m_spawnPoints2.Length != 0)
			{
				int num2 = ((int)randomizer.seed & 16777215) % this.m_spawnPoints2.Length;
				vector3 = this.m_spawnPoints2[num2].m_position;
				vector4 = this.m_spawnPoints2[num2].m_target;
			}
			else
			{
				vector3 = this.m_spawnPosition2;
				vector4 = this.m_spawnTarget2;
			}
			position = data.CalculatePosition(vector3);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(vector3 * 2f - vector4);
			}
			else
			{
				target = data.CalculatePosition(vector4);
			}
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			Vector3 vector;
			Vector3 vector2;
			if (this.m_spawnPoints != null && this.m_spawnPoints.Length != 0)
			{
				int num = ((int)randomizer.seed & 16777215) % this.m_spawnPoints.Length;
				vector = this.m_spawnPoints[num].m_position;
				vector2 = this.m_spawnPoints[num].m_target;
			}
			else
			{
				vector = this.m_spawnPosition;
				vector2 = this.m_spawnTarget;
			}
			position = data.CalculatePosition(vector);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic != SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(vector * 2f - vector2);
			}
			else
			{
				target = data.CalculatePosition(vector2);
			}
		}
		else if (this.m_secondaryTransportInfo != null && info.m_vehicleType == this.m_secondaryTransportInfo.m_vehicleType)
		{
			Vector3 vector3;
			Vector3 vector4;
			if (this.m_spawnPoints2 != null && this.m_spawnPoints2.Length != 0)
			{
				int num2 = ((int)randomizer.seed & 16777215) % this.m_spawnPoints2.Length;
				vector3 = this.m_spawnPoints2[num2].m_position;
				vector4 = this.m_spawnPoints2[num2].m_target;
			}
			else
			{
				vector3 = this.m_spawnPosition2;
				vector4 = this.m_spawnTarget2;
			}
			position = data.CalculatePosition(vector3);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic != SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(vector3 * 2f - vector4);
			}
			else
			{
				target = data.CalculatePosition(vector4);
			}
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if (this.m_transportInfo.m_transportType == TransportInfo.TransportType.Tram)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<BuildingManager>.get_instance().m_tramDepotPlacement.Activate(properties.m_tramDepotBuilt);
			}
		}
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
		{
			Vector3 vector;
			Vector3 vector2;
			bool flag;
			if (BuildingTool.SnapToCanal(position, out vector, out vector2, out flag, 40f, false))
			{
				if (flag || !this.m_supportCanals)
				{
					vector += vector2 * this.m_quayOffset;
				}
				else
				{
					vector -= vector2 * (this.m_spawnPosition.z - 16f);
				}
				angle = Mathf.Atan2(vector2.x, -vector2.z);
				position.x = vector.x;
				position.z = vector.z;
				if (!flag && !this.m_supportCanals)
				{
					toolErrors |= ToolBase.ToolErrors.ShoreNotFound;
				}
			}
			GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties2 != null)
			{
				Singleton<TransportManager>.get_instance().m_ferryDepot.Activate(properties2.m_ferryDepot);
			}
		}
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Blimp)
		{
			GuideController properties3 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties3 != null)
			{
				Singleton<TransportManager>.get_instance().m_blimpDepot.Activate(properties3.m_blimpDepot);
			}
		}
		return toolErrors | base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
		{
			float num = Mathf.Max(0f, (float)this.m_info.m_cellLength * 4f + this.m_spawnPosition.z - 31f);
			min = 20f / Mathf.Max(22f, (float)this.m_info.m_cellLength * 8f);
			max = num / Mathf.Max(num + 2f, (float)this.m_info.m_cellLength * 8f);
			return true;
		}
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	public override void PlacementSucceeded()
	{
		if (this.m_transportInfo.m_transportType == TransportInfo.TransportType.Tram)
		{
			Singleton<BuildingManager>.get_instance().m_tramDepotPlacement.Disable();
		}
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			string.Empty
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string text = string.Empty;
		if (this.m_transportInfo != null && this.m_maxVehicleCount != 0)
		{
			switch (this.m_transportInfo.m_transportType)
			{
			case TransportInfo.TransportType.Bus:
			{
				int vehicleCount = this.GetVehicleCount(buildingID, ref data);
				int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
				int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
				int num = (productionRate * this.m_maxVehicleCount + 99) / 100;
				text += LocaleFormatter.FormatGeneric("AIINFO_BUSDEPOT_BUSCOUNT", new object[]
				{
					vehicleCount,
					num
				});
				break;
			}
			case TransportInfo.TransportType.Ship:
				if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
				{
					int vehicleCount2 = this.GetVehicleCount(buildingID, ref data);
					int budget2 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
					int productionRate2 = PlayerBuildingAI.GetProductionRate(100, budget2);
					int num2 = (productionRate2 * this.m_maxVehicleCount + 99) / 100;
					text += LocaleFormatter.FormatGeneric("AIINFO_FERRYDEPOT_FERRYCOUNT", new object[]
					{
						vehicleCount2,
						num2
					});
				}
				break;
			case TransportInfo.TransportType.Airplane:
				if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Blimp)
				{
					int vehicleCount3 = this.GetVehicleCount(buildingID, ref data);
					int budget3 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
					int productionRate3 = PlayerBuildingAI.GetProductionRate(100, budget3);
					int num3 = (productionRate3 * this.m_maxVehicleCount + 99) / 100;
					text += LocaleFormatter.FormatGeneric("AIINFO_BLIMPDEPOT_BLIMPCOUNT", new object[]
					{
						vehicleCount3,
						num3
					});
				}
				break;
			case TransportInfo.TransportType.Taxi:
			{
				int vehicleCount4 = this.GetVehicleCount(buildingID, ref data);
				int budget4 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
				int productionRate4 = PlayerBuildingAI.GetProductionRate(100, budget4);
				int num4 = (productionRate4 * this.m_maxVehicleCount + 99) / 100;
				text += LocaleFormatter.FormatGeneric("AIINFO_TAXIDEPOT_VEHICLES", new object[]
				{
					vehicleCount4,
					num4
				});
				break;
			}
			case TransportInfo.TransportType.Tram:
			{
				int vehicleCount5 = this.GetVehicleCount(buildingID, ref data);
				int budget5 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
				int productionRate5 = PlayerBuildingAI.GetProductionRate(100, budget5);
				int num5 = (productionRate5 * this.m_maxVehicleCount + 99) / 100;
				text += LocaleFormatter.FormatGeneric("AIINFO_TRAMDEPOT_TRAMCOUNT", new object[]
				{
					vehicleCount5,
					num5
				});
				break;
			}
			}
		}
		return text;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Car || (this.m_secondaryTransportInfo != null && this.m_secondaryTransportInfo.m_vehicleType == VehicleInfo.VehicleType.Car);
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 3;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 18;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 21;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 18;

	public TransportInfo m_transportInfo;

	public TransportInfo m_secondaryTransportInfo;

	public ItemClass m_overrideVehicleClass;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 100f;

	public DepotAI.SpawnPoint[] m_spawnPoints;

	public DepotAI.SpawnPoint[] m_spawnPoints2;

	public Vector3 m_spawnPosition;

	public Vector3 m_spawnTarget;

	public Vector3 m_spawnPosition2;

	public Vector3 m_spawnTarget2;

	public bool m_canInvertTarget;

	public bool m_supportCanals;

	public int m_maxVehicleCount = 100000;

	public int m_maxVehicleCount2 = 100000;

	[NonSerialized]
	protected float m_quayOffset;

	[Serializable]
	public struct SpawnPoint
	{
		public Vector3 m_position;

		public Vector3 m_target;
	}
}
