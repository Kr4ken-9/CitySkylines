﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class HearseAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Health)
		{
			return base.GetColor(vehicleID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			if ((data.m_flags & (Vehicle.Flags.Stopped | Vehicle.Flags.WaitingTarget)) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_HEARSE_WAIT");
			}
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_HEARSE_RETURN");
			}
			if (data.m_targetBuilding != 0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_HEARSE_COLLECT");
			}
		}
		else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_HEARSE_RETURN");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_HEARSE_UNLOAD");
			}
			if (data.m_targetBuilding != 0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_HEARSE_TRANSFER");
			}
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Hearse";
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = data.m_citizenUnits;
			int num2 = 0;
			int num3 = 0;
			while (num != 0u)
			{
				uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Moving)
					{
						num2++;
					}
				}
				num = nextUnit;
				if (++num3 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			current = num2;
			max = this.m_corpseCapacity;
		}
		else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			current = (int)data.m_transferSize;
			max = this.m_corpseCapacity;
		}
		else
		{
			current = 0;
			max = this.m_corpseCapacity;
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_corpseCapacity + this.m_driverCount, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_transferType == 42)
		{
			if (data.m_sourceBuilding != 0 && data.m_transferSize != 0)
			{
				int transferSize = (int)data.m_transferSize;
				BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				if (info != null)
				{
					info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
					data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
				}
			}
		}
		else if (data.m_sourceBuilding != 0)
		{
			ushort buildingID = 0;
			BuildingInfo info2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			if (info2 != null)
			{
				buildingID = data.m_sourceBuilding;
			}
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = data.m_citizenUnits;
			int num2 = 0;
			while (num != 0u)
			{
				uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u)
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, buildingID, 0u);
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Visit;
					}
				}
				num = nextUnit;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				int num = Mathf.Min(0, (int)data.m_transferSize - this.m_corpseCapacity);
				info.m_buildingAI.ModifyMaterialBuffer(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
				num = Mathf.Max(0, -num);
				data.m_transferSize += (ushort)num;
			}
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			instance.m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				if (data.m_transferSize > 0)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Vehicle = vehicleID;
					if (data.m_sourceBuilding != 0)
					{
						offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
					}
					else
					{
						offer.Position = data.GetLastFramePosition();
					}
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
					data.m_flags |= Vehicle.Flags.WaitingTarget;
				}
				else
				{
					data.m_flags |= Vehicle.Flags.GoingBack;
				}
			}
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				int num = this.m_corpseCapacity;
				if (this.ShouldReturnToSource(vehicleID, ref data))
				{
					num = (int)data.m_transferSize;
				}
				else if (data.m_sourceBuilding != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
					if (info == null)
					{
						return;
					}
					int num2;
					int num3;
					info.m_buildingAI.GetMaterialAmount(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], TransferManager.TransferReason.Dead, out num2, out num3);
					num = Mathf.Min(num, num3 - num2);
				}
				if ((int)data.m_transferSize < num)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					offer2.Priority = 7;
					offer2.Vehicle = vehicleID;
					if (data.m_sourceBuilding != 0)
					{
						offer2.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
					}
					else
					{
						offer2.Position = data.GetLastFramePosition();
					}
					offer2.Amount = 1;
					offer2.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer2);
					data.m_flags |= Vehicle.Flags.WaitingTarget;
				}
				else
				{
					data.m_flags |= Vehicle.Flags.GoingBack;
				}
			}
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if (material == TransferManager.TransferReason.DeadMove)
			{
				if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
				{
					this.SetTarget(vehicleID, ref data, offer.Building);
				}
			}
			else if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				uint citizen = offer.Citizen;
				ushort building = offer.Building;
				if (citizen != 0u)
				{
					ushort buildingByLocation = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation();
					this.SetTarget(vehicleID, ref data, buildingByLocation);
					Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, vehicleID, 0u);
					data.m_transferSize += 1;
				}
				else if (building != 0)
				{
					this.SetTarget(vehicleID, ref data, building);
				}
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_corpseCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 && this.CanLeave(vehicleID, ref vehicleData))
		{
			vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
			vehicleData.m_flags |= Vehicle.Flags.Leaving;
		}
		if ((vehicleData.m_flags & (Vehicle.Flags.TransferToSource | Vehicle.Flags.GoingBack)) == Vehicle.Flags.TransferToSource && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & (Building.Flags.Active | Building.Flags.Downgrading)) != Building.Flags.Active && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
			else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		if (data.m_transferType == 42)
		{
			int num = 0;
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				num = (int)data.m_transferSize;
			}
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				num = Mathf.Min(0, (int)data.m_transferSize - this.m_corpseCapacity);
			}
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
			}
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				data.m_transferSize += (ushort)Mathf.Max(0, -num);
			}
			if (data.m_sourceBuilding != 0 && (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.Outgoing)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				ushort num2 = instance.FindBuilding(instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position, 200f, info2.m_class.m_service, info2.m_class.m_subService, Building.Flags.Incoming, Building.Flags.Outgoing);
				if (num2 != 0)
				{
					instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
					data.m_sourceBuilding = num2;
					instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
				}
			}
		}
		else
		{
			this.LoadDeadCitizens(vehicleID, ref data, data.m_targetBuilding);
			for (int i = 0; i < this.m_driverCount; i++)
			{
				this.CreateDriver(vehicleID, ref data, Citizen.AgePhase.Senior0);
			}
			data.m_flags |= Vehicle.Flags.Stopped;
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		bool result = true;
		bool flag = false;
		ushort num = 0;
		uint num2 = vehicleData.m_citizenUnits;
		int num3 = 0;
		while (num2 != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num2)].GetCitizen(i);
				if (citizen != 0u && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
				{
					ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance3 != 0)
					{
						CitizenInfo info = instance2.m_instances.m_buffer[(int)instance3].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service)
						{
							if ((instance2.m_instances.m_buffer[(int)instance3].m_flags & CitizenInstance.Flags.EnteringVehicle) == CitizenInstance.Flags.None)
							{
								if ((instance2.m_instances.m_buffer[(int)instance3].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
								{
									flag = true;
								}
								else if (num == 0)
								{
									num = instance3;
								}
								else
								{
									instance2.ReleaseCitizenInstance(instance3);
								}
							}
							result = false;
						}
					}
				}
			}
			num2 = nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!flag && num != 0)
		{
			CitizenInfo citizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Male, Citizen.SubCulture.Generic, Citizen.AgePhase.Senior1);
			if (citizenInfo != null)
			{
				instance2.m_instances.m_buffer[(int)num].Info = citizenInfo;
			}
			else
			{
				citizenInfo = instance2.m_instances.m_buffer[(int)num].Info;
			}
			citizenInfo.m_citizenAI.SetTarget(num, ref instance2.m_instances.m_buffer[(int)num], 0);
		}
		return result;
	}

	private void CreateDriver(ushort vehicleID, ref Vehicle data, Citizen.AgePhase agePhase)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		CitizenInfo groupCitizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Male, Citizen.SubCulture.Generic, agePhase);
		if (groupCitizenInfo != null)
		{
			int family = instance.m_randomizer.Int32(256u);
			uint num = 0u;
			if (instance2.CreateCitizen(out num, 90, family, ref instance.m_randomizer, groupCitizenInfo.m_gender))
			{
				ushort num2;
				if (instance2.CreateCitizenInstance(out num2, ref instance.m_randomizer, groupCitizenInfo, num))
				{
					Vector3 randomDoorPosition = data.GetRandomDoorPosition(ref instance.m_randomizer, VehicleInfo.DoorType.Exit);
					groupCitizenInfo.m_citizenAI.SetCurrentVehicle(num2, ref instance2.m_instances.m_buffer[(int)num2], 0, 0u, randomDoorPosition);
					groupCitizenInfo.m_citizenAI.SetTarget(num2, ref instance2.m_instances.m_buffer[(int)num2], data.m_targetBuilding);
					instance2.m_citizens.m_buffer[(int)((UIntPtr)num)].SetVehicle(num, vehicleID, 0u);
				}
				else
				{
					instance2.ReleaseCitizen(num);
				}
			}
		}
	}

	private void LoadDeadCitizens(ushort vehicleID, ref Vehicle data, ushort buildingID)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation != Citizen.Location.Moving)
				{
					ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance3 != 0)
					{
						instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance = 0;
						instance2.m_instances.m_buffer[(int)instance3].m_citizen = 0u;
						instance2.ReleaseCitizenInstance(instance3);
					}
					instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Moving;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int num3 = this.m_corpseCapacity;
		if (data.m_sourceBuilding == 0)
		{
			num3 = (int)data.m_transferSize;
		}
		else if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			num3 = (int)data.m_transferSize;
		}
		else
		{
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			int num4;
			int num5;
			info.m_buildingAI.GetMaterialAmount(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], TransferManager.TransferReason.Dead, out num4, out num5);
			num3 = Mathf.Min(num3, num5 - num4);
		}
		if ((int)data.m_transferSize < num3)
		{
			num = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)buildingID].m_citizenUnits;
			num2 = 0;
			while (num != 0u)
			{
				uint nextUnit2 = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				for (int j = 0; j < 5; j++)
				{
					uint citizen2 = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(j);
					if (citizen2 != 0u && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].GetBuildingByLocation() == buildingID)
					{
						ushort instance4 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].m_instance;
						if (instance4 != 0)
						{
							instance2.ReleaseCitizenInstance(instance4);
						}
						instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].SetVehicle(citizen2, vehicleID, 0u);
						instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].CurrentLocation = Citizen.Location.Moving;
						if ((int)(data.m_transferSize += 1) >= num3)
						{
							return;
						}
					}
				}
				num = nextUnit2;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		if (data.m_transferType == 42)
		{
			int num = 0;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				num = (int)data.m_transferSize;
				BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
			}
		}
		else
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num2 = data.m_citizenUnits;
			int num3 = 0;
			while (num2 != 0u)
			{
				uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num2)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, data.m_sourceBuilding, 0u);
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Visit;
					}
				}
				num2 = nextUnit;
				if (++num3 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 targetPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out targetPos2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, 2f));
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 endPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public int m_driverCount = 2;

	[CustomizableProperty("Capacity")]
	public int m_corpseCapacity = 5;
}
