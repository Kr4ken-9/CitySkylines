﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class EarthquakeAI : DisasterAI
{
	public override void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort disasterID, ref DisasterData data, ref Vector3 shake)
	{
		if ((data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			int num = (int)(instance.m_referenceFrameIndex - data.m_activationFrame + 128u);
			if (num <= 0)
			{
				return;
			}
			if ((long)num >= (long)((ulong)this.m_activeDuration))
			{
				return;
			}
			Vector3 vector = cameraInfo.m_camera.get_transform().InverseTransformPoint(data.m_targetPosition);
			vector.z *= 0.25f;
			float num2 = 0.3f / (1f + vector.get_magnitude() * 0.001f);
			float num3 = (float)num + instance.m_referenceTimer;
			num2 *= 0.5f - 0.5f * Mathf.Cos(num3 * 0.0245436933f);
			Vector2 vector2;
			vector2.x = -Mathf.Sin(data.m_angle);
			vector2.y = Mathf.Cos(data.m_angle);
			float num4 = Mathf.Sin(num3 * 0.63f) + Mathf.Sin(num3 * 0.17f);
			num4 *= num2;
			shake.x += num4 * vector2.y;
			shake.z -= num4 * vector2.x;
		}
	}

	public override void PlayInstance(AudioManager.ListenerInfo listenerInfo, ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None && this.m_loopSound != null)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			int num = (int)(instance.m_referenceFrameIndex - data.m_activationFrame);
			if (num <= 0)
			{
				return;
			}
			if ((long)num >= (long)((ulong)this.m_activeDuration))
			{
				return;
			}
			float num2 = (float)num + instance.m_referenceTimer;
			float num3 = (this.m_activeDuration - num2) / this.m_activeDuration;
			InstanceID empty = InstanceID.Empty;
			empty.Disaster = disasterID;
			Vector3 targetPosition = data.m_targetPosition;
			float volume = Mathf.Sqrt(Mathf.Clamp01(num3)) * (0.5f + (float)data.m_intensity * 0.005f);
			Singleton<AudioManager>.get_instance().EffectGroup.AddPlayer(listenerInfo, (int)empty.RawData, this.m_loopSound, targetPosition, Vector3.get_zero(), 10000f, volume, 1f);
		}
	}

	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector2 vector = VectorUtils.XZ(data.m_targetPosition);
			Vector2 vector2;
			vector2.x = -Mathf.Sin(data.m_angle);
			vector2.y = Mathf.Cos(data.m_angle);
			float num = this.m_crackWidth * (0.5f + (float)data.m_intensity * 0.005f);
			float num2 = this.m_crackLength * (0.5f + (float)data.m_intensity * 0.005f);
			Segment2 segment;
			segment.a = vector - vector2 * (num2 * 0.5f);
			segment.b = vector + vector2 * (num2 * 0.5f);
			float num3 = 200f;
			segment.a.x = segment.a.x + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.a.y = segment.a.y + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.b.x = segment.b.x + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.b.y = segment.b.y + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			float num4 = (float)randomizer.Int32(30, 100) * 0.1f;
			float num5 = (float)randomizer.Int32(-15, 15) * 0.1f;
			float num6 = 0f;
			float num7 = 2000f + (float)data.m_intensity * 20f + num3 * 2f;
			Vector2 vector3 = segment.Min() - new Vector2(num7 + num, num7 + num);
			Vector2 vector4 = segment.Max() + new Vector2(num7 + num, num7 + num);
			int num8 = Mathf.Max((int)(vector3.x / 38.4f + 128f), 2);
			int num9 = Mathf.Max((int)(vector3.y / 38.4f + 128f), 2);
			int num10 = Mathf.Min((int)(vector4.x / 38.4f + 128f), 253);
			int num11 = Mathf.Min((int)(vector4.y / 38.4f + 128f), 253);
			for (int i = num9; i <= num11; i++)
			{
				Vector2 vector5;
				vector5.y = ((float)i - 128f + 0.5f) * 38.4f;
				for (int j = num8; j <= num10; j++)
				{
					vector5.x = ((float)j - 128f + 0.5f) * 38.4f;
					float num13;
					float num12 = Mathf.Sqrt(segment.DistanceSqr(vector5, ref num13));
					float num14 = 2f * num13 - 1f;
					num12 -= num * Mathf.Sqrt(Mathf.Max(0f, 1f - num14 * num14));
					num12 += num * (0.5f + 0.5f * Mathf.Sin(num13 * num4 + num5));
					if (num12 < num7)
					{
						float num15 = Mathf.Min(1f, 1f - (num12 - num6) / (num7 - num6));
						num15 *= num15;
						int num16 = i * 256 + j;
						map[num16] = (byte)(255 - (int)(255 - map[num16]) * (255 - Mathf.RoundToInt(num15 * 255f)) / 255);
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			if (data.m_activationFrame != 0u)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				int num;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.EarthquakeCoverage, data.m_targetPosition, out num);
				num = Mathf.Min(num, 100);
				uint num2 = (uint)(num * 6437 / 100 + 1755);
				if (currentFrameIndex + num2 >= data.m_activationFrame)
				{
					Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, num != 0);
				}
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			if ((data.m_flags & DisasterData.Flags.Located) == DisasterData.Flags.None)
			{
				int num3;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.EarthquakeCoverage, data.m_targetPosition, out num3);
				if (num3 != 0)
				{
					Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, true);
				}
			}
			Vector2 vector = VectorUtils.XZ(data.m_targetPosition);
			Vector2 vector2;
			vector2.x = -Mathf.Sin(data.m_angle);
			vector2.y = Mathf.Cos(data.m_angle);
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			float num4 = (float)randomizer.Int32(70, 200) * 0.1f;
			float num5 = (float)randomizer.Int32(-15, 15) * 0.1f;
			uint currentFrameIndex2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			randomizer..ctor((long)randomizer.Bits32(16) ^ (long)((ulong)(currentFrameIndex2 >> 8)));
			InstanceID id = default(InstanceID);
			id.Disaster = disasterID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
			float num6 = this.m_crackWidth * (0.5f + (float)data.m_intensity * 0.005f);
			float num7 = this.m_crackLength * (0.5f + (float)data.m_intensity * 0.005f);
			for (int i = 0; i < 4; i++)
			{
				float num8 = (float)randomizer.Int32(-400, 400) * 0.001f;
				float num9 = Mathf.Sin(num8 * num4 + num5);
				float num10 = num6 * (1f - num8 * num8 * 4f);
				Vector2 vector3 = vector + vector2 * (num8 * num7);
				vector3.x += vector2.y * (num9 * num10 * 0.5f);
				vector3.y -= vector2.x * (num9 * num10 * 0.5f);
				DisasterHelpers.SplashWater(vector3, num10, num10 * 0.5f);
				Vector3 vector4 = VectorUtils.X_Y(vector3);
				vector4.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector4);
				DisasterHelpers.DestroyBuildings((int)disasterID, group, vector4, num10, num10 * 0.5f, num10, num10 * 2f, num10, num10 * 1.5f, 1f);
				DisasterHelpers.DestroyNetSegments((int)disasterID, group, vector4, num10, num10 * 0.5f, num10, num10 * 2f);
				Vector2 startPos = vector + vector2 * (num8 * num7 - num10);
				Vector2 endPos = vector + vector2 * (num8 * num7 + num10);
				float num11 = Mathf.Sin((num8 - num10 / num7) * num4 + num5);
				float num12 = Mathf.Sin((num8 + num10 / num7) * num4 + num5);
				startPos.x += vector2.y * (num11 * num10 * 0.5f);
				startPos.y -= vector2.x * (num11 * num10 * 0.5f);
				endPos.x += vector2.y * (num12 * num10 * 0.5f);
				endPos.y -= vector2.x * (num12 * num10 * 0.5f);
				DisasterHelpers.MakeCrack(startPos, endPos, num10 * 0.5f, num10 * 0.05f);
				DisasterHelpers.BurnGround(vector3, num10, 0.7f);
			}
			float num13 = 2000f + (float)data.m_intensity * 20f;
			DisasterHelpers.DestroyBuildings((int)disasterID, group, data.m_targetPosition, num13, 0f, 0f, num13, 0f, num13, 0.02f);
		}
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(data.m_targetPosition);
			data.m_activationFrame = data.m_startFrame + this.m_emergingDuration;
			data.m_flags |= DisasterData.Flags.Significant;
			Singleton<DisasterManager>.get_instance().m_randomDisasterCooldown = 0;
		}
	}

	protected override void ActivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.ActivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().FollowDisaster(disasterID);
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillEmerging(disasterID, ref data))
		{
			return true;
		}
		if (data.m_activationFrame != 0u)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex >= data.m_activationFrame)
			{
				return false;
			}
		}
		return true;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num < this.m_activeDuration;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return 1500 / (8 + ((int)num >> 10));
	}

	protected override bool IsStillClearing(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillClearing(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		float num2 = num * 0.125f - 1000f;
		float num3 = this.m_crackLength * (0.5f + (float)data.m_intensity * 0.005f);
		num2 -= num3;
		Vector2 vector;
		vector.x = Mathf.Abs(data.m_targetPosition.x) + 4800f;
		vector.y = Mathf.Abs(data.m_targetPosition.z) + 4800f;
		float num4 = VectorUtils.LengthXZ(vector);
		return num4 > num2;
	}

	public override bool CanAffectAt(ushort disasterID, ref DisasterData data, Vector3 position, out float priority)
	{
		if (!base.CanAffectAt(disasterID, ref data, position, out priority))
		{
			return false;
		}
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			return false;
		}
		float num = VectorUtils.LengthXZ(position - data.m_targetPosition);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		float num3 = num2 * 0.125f - 1000f;
		float num4 = num2 * 0.125f + 100f;
		float num5 = this.m_crackLength * (0.5f + (float)data.m_intensity * 0.005f);
		num3 -= num5;
		num4 += num5;
		priority = Mathf.Clamp01(Mathf.Min((num4 - num) * 0.02f, (num - num3) * 0.002f));
		return num >= num3 && num <= num4;
	}

	protected override float GetMinimumEdgeDistance()
	{
		return this.m_crackLength * 0.5f + 150f;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool HasDirection()
	{
		return true;
	}

	public override bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		position = data.m_targetPosition;
		rotation = Quaternion.get_identity();
		float num = this.m_crackLength * (0.5f + (float)data.m_intensity * 0.005f);
		size..ctor(num, num, num);
		return true;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.EarthquakeHazard;
		return true;
	}

	public float m_crackLength = 1000f;

	public float m_crackWidth = 100f;

	public uint m_emergingDuration = 4096u;

	public uint m_activeDuration = 1024u;

	public AudioInfo m_loopSound;
}
