﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class ParamedicAI : ServicePersonAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
	}

	public override void ReleaseAI()
	{
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Health)
		{
			return base.GetColor(instanceID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		target = InstanceID.Empty;
		return Locale.Get("PARAMEDIC_STATUS");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void ReleaseInstance(ushort instanceID, ref CitizenInstance data)
	{
		if (data.m_citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_instance = 0;
			instance.ReleaseCitizen(data.m_citizen);
			data.m_citizen = 0u;
		}
		base.ReleaseInstance(instanceID, ref data);
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_vehicle == 0)
		{
			instance.ReleaseCitizenInstance(instanceID);
			return;
		}
		if (data.m_waitCounter < 255)
		{
			data.m_waitCounter += 1;
		}
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
				data.m_waitCounter = 0;
			}
			else
			{
				if ((data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
				{
					Vector3 vector = this.GetVehicleEnterPosition(instanceID, ref data, 1f);
					Vector3 position = data.GetLastFrameData().m_position;
					Vector3 vector2 = vector - position;
					vector2.y = 0f;
					if (vector2.get_sqrMagnitude() > 0.01f)
					{
						CitizenInstance.Frame frame = default(CitizenInstance.Frame);
						frame.m_position = position;
						frame.m_rotation = Quaternion.LookRotation(vector2);
						data.m_frame0 = frame;
						data.m_frame1 = frame;
						data.m_frame2 = frame;
						data.m_frame3 = frame;
					}
				}
				data.m_flags |= CitizenInstance.Flags.EnteringVehicle;
				data.Spawn(instanceID);
			}
		}
	}

	protected override Vector4 GetVehicleEnterPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			ushort vehicle = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
			if (vehicle != 0)
			{
				Vector4 result = instance2.m_vehicles.m_buffer[(int)vehicle].GetClosestDoorPosition(citizenData.m_targetPos, (!this.m_hasStretcher) ? VehicleInfo.DoorType.Enter : VehicleInfo.DoorType.Load);
				result.w = citizenData.m_targetPos.w;
				return result;
			}
		}
		return citizenData.m_targetPos;
	}

	protected override void GetBuildingTargetPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		if (citizenData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)instanceID);
			Vector3 vector;
			Vector3 vector2;
			Vector2 targetDir;
			CitizenInstance.Flags flags;
			info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref randomizer, this.m_info, instanceID, out vector, out vector2, out targetDir, out flags);
			citizenData.m_targetPos = new Vector4(vector.x, vector.y, vector.z, 1f);
			citizenData.m_targetDir = targetDir;
			return;
		}
		citizenData.m_targetDir = Vector2.get_zero();
	}

	protected override bool EnterVehicle(ushort instanceID, ref CitizenInstance citizenData)
	{
		citizenData.Unspawn(instanceID);
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
		}
		return true;
	}

	protected override bool ArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData)
	{
		citizenData.Unspawn(instanceID);
		return false;
	}

	protected override void ArriveAtDestination(ushort instanceID, ref CitizenInstance citizenData, bool success)
	{
	}

	public bool m_hasStretcher;
}
