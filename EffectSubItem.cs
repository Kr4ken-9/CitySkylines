﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public abstract class EffectSubItem : UICustomControl
{
	protected TriggerMilestone currentTriggerMilestone
	{
		get
		{
			if (this.m_selectedTrigger == null)
			{
				this.m_selectedTrigger = base.get_component().get_gameObject().GetComponentInParent<SelectedTrigger>();
			}
			return this.m_selectedTrigger.m_triggerMilestone;
		}
	}

	public TriggerEffect currentTriggerEffect
	{
		get
		{
			if (this.m_effectItem == null)
			{
				this.m_effectItem = base.get_component().GetComponentInParent<EffectItem>();
			}
			return this.m_effectItem.m_currentTriggerEffect;
		}
		set
		{
			if (this.m_effectItem == null)
			{
				this.m_effectItem = base.get_component().GetComponentInParent<EffectItem>();
			}
			this.m_effectItem.m_currentTriggerEffect = value;
		}
	}

	public void SetDefaultValues(EffectItem.EffectType effectType)
	{
		this.m_currentEffectType = effectType;
		this.SetDefaultValuesImpl();
	}

	protected abstract void SetDefaultValuesImpl();

	public void Show(EffectItem.EffectType effectType)
	{
		base.get_component().set_isVisible(true);
		this.m_currentEffectType = effectType;
		this.ShowImpl();
	}

	protected abstract void ShowImpl();

	public void Hide()
	{
		base.get_component().set_isVisible(false);
		this.HideImpl();
	}

	protected abstract void HideImpl();

	private SelectedTrigger m_selectedTrigger;

	private EffectItem m_effectItem;

	[HideInInspector]
	public EffectItem.EffectType m_currentEffectType;
}
