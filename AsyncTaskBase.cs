﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public abstract class AsyncTaskBase
{
	public AsyncTaskBase(string taskName)
	{
		if (!string.IsNullOrEmpty(taskName))
		{
			this.m_Name = taskName;
		}
		else
		{
			this.m_Name = "Anonymous Task";
		}
		this.m_Progress = -1f;
	}

	public virtual string name
	{
		get
		{
			return this.m_Name;
		}
	}

	public float progress
	{
		get
		{
			return this.m_Progress;
		}
	}

	public int progressSteps
	{
		get
		{
			return this.m_ProgressSteps;
		}
		set
		{
			this.m_ProgressSteps = Mathf.Min(value, 1);
			this.m_CachedStepCount = 1f / (float)this.m_ProgressSteps;
		}
	}

	public virtual bool isExecuting
	{
		get
		{
			return this.progress >= 0f && this.progress < 1f;
		}
	}

	public bool completed
	{
		get
		{
			return this.progress == 1f;
		}
	}

	public virtual bool completedOrFailed
	{
		get
		{
			return this.progress >= 1f;
		}
	}

	public bool failed
	{
		get
		{
			return this.progress == 100f;
		}
	}

	[DebuggerHidden]
	private IEnumerator WaitCoroutine()
	{
		AsyncTaskBase.<WaitCoroutine>c__Iterator0 <WaitCoroutine>c__Iterator = new AsyncTaskBase.<WaitCoroutine>c__Iterator0();
		<WaitCoroutine>c__Iterator.$this = this;
		return <WaitCoroutine>c__Iterator;
	}

	public Coroutine WaitTaskCompleted(MonoBehaviour waitWithin)
	{
		return waitWithin.StartCoroutine(this.WaitCoroutine());
	}

	public abstract void Execute();

	protected string m_Name;

	protected volatile float m_Progress;

	protected int m_ProgressSteps;

	protected float m_CachedStepCount = 1f;
}
