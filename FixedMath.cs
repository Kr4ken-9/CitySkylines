﻿using System;
using UnityEngine;

public class FixedMath
{
	public static float FixedToFloat(int value)
	{
		return (float)value * 0.0009765625f;
	}

	public static int FloatToFixed(float value)
	{
		return (int)(value * 1024f);
	}

	public static uint ConvertColor(Color color)
	{
		return (uint)(color.a * 255f) << 24 | (uint)(color.r * 255f) << 16 | (uint)(color.g * 255f) << 8 | (uint)(color.b * 255f);
	}

	public static Color ConvertColor(uint color)
	{
		return new Color((color >> 16 & 255u) * 0.003921569f, (color >> 8 & 255u) * 0.003921569f, (color & 255u) * 0.003921569f, (color >> 24 & 255u) * 0.003921569f);
	}

	public static int Sqrt(long value)
	{
		long num = value >> 11;
		if (num == 0L)
		{
			return 0;
		}
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		return (int)num;
	}

	public static int Sqrt(int value)
	{
		int num = value >> 11;
		if (num == 0)
		{
			return 0;
		}
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		num = num + value / num >> 1;
		return num + value / num >> 1;
	}

	public static uint NumberOfSetBits(uint i)
	{
		i -= (i >> 1 & 1431655765u);
		i = (i & 858993459u) + (i >> 2 & 858993459u);
		return (i + (i >> 4) & 252645135u) * 16843009u >> 24;
	}

	public static uint NumberOfSetBits(ulong i)
	{
		i -= (i >> 1 & 6148914691236517205uL);
		i = (i & 3689348814741910323uL) + (i >> 2 & 3689348814741910323uL);
		return (uint)((i + (i >> 4) & 1085102592571150095uL) * 72340172838076673uL >> 56);
	}

	public static int Sin(int angle, int multiplier)
	{
		angle &= 65535;
		if (angle <= 16384)
		{
			return FixedMath.GetSinValue(angle, multiplier);
		}
		if (angle <= 32768)
		{
			return FixedMath.GetSinValue(32768 - angle, multiplier);
		}
		if (angle <= 49152)
		{
			return -FixedMath.GetSinValue(angle - 32768, multiplier);
		}
		return -FixedMath.GetSinValue(65536 - angle, multiplier);
	}

	public static int Cos(int angle, int multiplier)
	{
		angle &= 65535;
		if (angle <= 16384)
		{
			return FixedMath.GetSinValue(16384 - angle, multiplier);
		}
		if (angle <= 32768)
		{
			return -FixedMath.GetSinValue(angle - 16384, multiplier);
		}
		if (angle <= 49152)
		{
			return -FixedMath.GetSinValue(49152 - angle, multiplier);
		}
		return FixedMath.GetSinValue(angle - 49152, multiplier);
	}

	public static int Asin(int sin)
	{
		if (sin >= 0)
		{
			return FixedMath.GetAsinValue(sin);
		}
		return -FixedMath.GetAsinValue(-sin);
	}

	public static int Acos(int cos)
	{
		if (cos >= 0)
		{
			return 16384 - FixedMath.GetAsinValue(cos);
		}
		return 16384 + FixedMath.GetAsinValue(-cos);
	}

	private static int GetSinValue(int angle, int multiplier)
	{
		long num = FixedMath.sinTable[angle >> 8];
		long num2 = FixedMath.sinTable[(angle >> 8) + 1];
		return (int)(((num << 8) + (num2 - num) * (long)(angle & 255)) * (long)multiplier >> 32);
	}

	private static int GetAsinValue(int sin)
	{
		long num = FixedMath.arcsinTable[sin >> 8];
		long num2 = FixedMath.arcsinTable[(sin >> 8) + 1];
		return (int)((num << 8) + (num2 - num) * (long)(sin & 255) >> 16);
	}

	public static int Atan2(FixedVector2D v)
	{
		FixedMath.Normalize2D(ref v, 16384);
		if (v.x >= 0)
		{
			return FixedMath.Acos(Mathf.Clamp(v.z, -16384, 16384));
		}
		return -FixedMath.Acos(Mathf.Clamp(v.z, -16384, 16384));
	}

	public static int GEqualPowerOf2(int value)
	{
		if (value == 0)
		{
			return 0;
		}
		for (int i = 0; i < 30; i++)
		{
			if (1 << i >= value)
			{
				return 1 << i;
			}
		}
		return 0;
	}

	public static uint GEqualPowerOf2(uint value)
	{
		if (value == 0u)
		{
			return 0u;
		}
		for (int i = 0; i < 31; i++)
		{
			if (1u << i >= value)
			{
				return 1u << i;
			}
		}
		return 0u;
	}

	public static int Normalize2D(ref FixedVector2D v, int len)
	{
		int num = FixedMath.Sqrt((long)v.x * (long)v.x + (long)v.z * (long)v.z);
		if (num != 0)
		{
			long num2 = ((long)len << 30) / (long)num;
			v.x = (int)(num2 * (long)v.x >> 30);
			v.z = (int)(num2 * (long)v.z >> 30);
		}
		return num;
	}

	public static int Normalize3D(ref FixedVector3D v, int len)
	{
		int num = FixedMath.Sqrt((long)v.x * (long)v.x + (long)v.y * (long)v.y + (long)v.z * (long)v.z);
		if (num != 0)
		{
			long num2 = ((long)len << 30) / (long)num;
			v.x = (int)(num2 * (long)v.x >> 30);
			v.y = (int)(num2 * (long)v.y >> 30);
			v.z = (int)(num2 * (long)v.z >> 30);
		}
		return num;
	}

	public static int VectorLength2D(FixedVector2D v)
	{
		return FixedMath.Sqrt((long)v.x * (long)v.x + (long)v.z * (long)v.z);
	}

	public static long VectorLengthSq2D(FixedVector2D v)
	{
		return (long)v.x * (long)v.x + (long)v.z * (long)v.z;
	}

	public static int VectorLength3D(FixedVector3D v)
	{
		return FixedMath.Sqrt((long)v.x * (long)v.x + (long)v.y * (long)v.y + (long)v.z * (long)v.z);
	}

	public static long VectorLengthSq3D(FixedVector3D v)
	{
		return (long)v.x * (long)v.x + (long)v.y * (long)v.y + (long)v.z * (long)v.z;
	}

	public static long DistanceFromLineSq2D(FixedVector2D ab, FixedVector2D pa)
	{
		long num = (long)ab.x * (long)ab.x + (long)ab.z * (long)ab.z >> 10;
		if (num == 0L)
		{
			return 0L;
		}
		long num2 = (long)ab.x * (long)pa.z - (long)ab.z * (long)pa.x >> 10;
		return num2 * num2 / num << 10;
	}

	public static long DistanceFromLineSq3D(FixedVector3D ab, FixedVector3D pa)
	{
		long num = (long)ab.x * (long)ab.x + (long)ab.y * (long)ab.y + (long)ab.z * (long)ab.z >> 10;
		if (num == 0L)
		{
			return 0L;
		}
		long num2 = (long)ab.y * (long)pa.z - (long)ab.z * (long)pa.y >> 10;
		long num3 = (long)ab.z * (long)pa.x - (long)ab.x * (long)pa.z >> 10;
		long num4 = (long)ab.x * (long)pa.y - (long)ab.y * (long)pa.x >> 10;
		return (num2 * num2 + num3 * num3 + num4 * num4) / num << 10;
	}

	public static FixedVector2D OffsetToLine2D(FixedVector2D ab, FixedVector2D pa)
	{
		long num = (long)ab.x * (long)pa.x + (long)ab.z * (long)pa.z;
		long num2 = (long)ab.x * (long)ab.x + (long)ab.z * (long)ab.z >> 10;
		if (num2 != 0L)
		{
			num /= num2;
		}
		return new FixedVector2D(pa.x - (int)((long)ab.x * num >> 10), pa.z - (int)((long)ab.z * num >> 10));
	}

	public static long DistanceFromSegmentSq2D(FixedVector2D ab, FixedVector2D pa)
	{
		return FixedMath.DistanceFromSegmentSq2D(ab, pa, false, false);
	}

	public static long DistanceFromSegmentSq2D(FixedVector2D ab, FixedVector2D pa, bool capA)
	{
		return FixedMath.DistanceFromSegmentSq2D(ab, pa, capA, false);
	}

	public static long DistanceFromSegmentSq2D(FixedVector2D ab, FixedVector2D pa, bool capA, bool capB)
	{
		if ((long)ab.x * (long)pa.x + (long)ab.z * (long)pa.z >= 0L)
		{
			return (!capA) ? ((long)pa.x * (long)pa.x + (long)pa.z * (long)pa.z) : 1000000000000000L;
		}
		int num = pa.x + ab.x;
		int num2 = pa.z + ab.z;
		if ((long)ab.x * (long)num + (long)ab.z * (long)num2 <= 0L)
		{
			return (!capB) ? ((long)num * (long)num + (long)num2 * (long)num2) : 1000000000000000L;
		}
		return FixedMath.DistanceFromLineSq2D(ab, pa);
	}

	public static long DistanceFromSegmentSq3D(FixedVector3D ab, FixedVector3D pa)
	{
		if ((long)ab.x * (long)pa.x + (long)ab.y * (long)pa.y + (long)ab.z * (long)pa.z >= 0L)
		{
			return (long)pa.x * (long)pa.x + (long)pa.y * (long)pa.y + (long)pa.z * (long)pa.z;
		}
		FixedVector3D fixedVector3D = pa + ab;
		if ((long)ab.x * (long)fixedVector3D.x + (long)ab.y * (long)fixedVector3D.y + (long)ab.z * (long)fixedVector3D.z <= 0L)
		{
			return (long)fixedVector3D.x * (long)fixedVector3D.x + (long)fixedVector3D.y * (long)fixedVector3D.y + (long)fixedVector3D.z * (long)fixedVector3D.z;
		}
		return FixedMath.DistanceFromLineSq3D(ab, pa);
	}

	public static long SegmentDistanceSq2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa)
	{
		return FixedMath.SegmentDistanceSq2D(ab, pq, pa, false, false, false, false);
	}

	public static long SegmentDistanceSq2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa, bool capA)
	{
		return FixedMath.SegmentDistanceSq2D(ab, pq, pa, capA, false, false, false);
	}

	public static long SegmentDistanceSq2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa, bool capA, bool capB)
	{
		return FixedMath.SegmentDistanceSq2D(ab, pq, pa, capA, capB, false, false);
	}

	public static long SegmentDistanceSq2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa, bool capA, bool capB, bool capP)
	{
		return FixedMath.SegmentDistanceSq2D(ab, pq, pa, capA, capB, capP, false);
	}

	public static long SegmentDistanceSq2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa, bool capA, bool capB, bool capP, bool capQ)
	{
		long num = (long)ab.x * (long)pq.z - (long)ab.z * (long)pq.x >> 10;
		if (num == 0L)
		{
			long num2 = FixedMath.DistanceFromSegmentSq2D(pq, new FixedVector2D(-pa.x, -pa.z), capP, capQ);
			long num3 = FixedMath.DistanceFromSegmentSq2D(pq, new FixedVector2D(-pa.x - ab.x, -pa.z - ab.z), capP, capQ);
			long num4 = FixedMath.DistanceFromSegmentSq2D(ab, pa, capA, capB);
			long num5 = FixedMath.DistanceFromSegmentSq2D(ab, new FixedVector2D(pa.x - pq.x, pa.z - pq.z), capA, capB);
			if (num3 < num2)
			{
				num2 = num3;
			}
			if (num4 < num2)
			{
				num2 = num4;
			}
			if (num5 < num2)
			{
				num2 = num5;
			}
			return num2;
		}
		long num6 = ((long)pa.x * (long)pq.z - (long)pa.z * (long)pq.x) / num;
		if (num6 >= 0L)
		{
			return FixedMath.DistanceFromSegmentSq2D(pq, new FixedVector2D(-pa.x, -pa.z), capP, capQ);
		}
		if (num6 <= -1024L)
		{
			return FixedMath.DistanceFromSegmentSq2D(pq, new FixedVector2D(-pa.x - ab.x, -pa.z - ab.z), capP, capQ);
		}
		long num7 = ((long)ab.x * (long)pa.z - (long)ab.z * (long)pa.x) / num;
		if (num7 <= 0L)
		{
			return FixedMath.DistanceFromSegmentSq2D(ab, pa, capA, capB);
		}
		if (num7 >= 1024L)
		{
			return FixedMath.DistanceFromSegmentSq2D(ab, new FixedVector2D(pa.x - pq.x, pa.z - pq.z), capA, capB);
		}
		return 0L;
	}

	public static bool LineIntersect2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa, out int a, out int b)
	{
		long num = (long)ab.x * (long)pq.z - (long)ab.z * (long)pq.x >> 10;
		if (num == 0L)
		{
			a = 0;
			b = 0;
			return false;
		}
		a = (int)(((long)pa.z * (long)pq.x - (long)pa.x * (long)pq.z) / num);
		b = (int)(((long)ab.x * (long)pa.z - (long)ab.z * (long)pa.x) / num);
		return true;
	}

	public static int SegmentIntersect2D(FixedVector2D ab, FixedVector2D pq, FixedVector2D pa)
	{
		long num = (long)ab.x * (long)pq.z - (long)ab.z * (long)pq.x >> 10;
		if (num == 0L)
		{
			return -2000000000;
		}
		long num2 = ((long)pa.z * (long)pq.x - (long)pa.x * (long)pq.z) / num;
		if (num2 < 0L || num2 > 1024L)
		{
			return -2000000000;
		}
		long num3 = ((long)ab.x * (long)pa.z - (long)ab.z * (long)pa.x) / num;
		if (num3 < 0L || num3 > 1024L)
		{
			return -2000000000;
		}
		return (int)num2;
	}

	public static bool PointInsideQuad2D(FixedVector2D a, FixedVector2D b, FixedVector2D c, FixedVector2D d, FixedVector2D p)
	{
		return (long)(p.x - a.x) * (long)(b.z - a.z) - (long)(p.z - a.z) * (long)(b.x - a.x) >= 0L && (long)(p.x - a.x) * (long)(d.z - a.z) - (long)(p.z - a.z) * (long)(d.x - a.x) <= 0L && (long)(p.x - c.x) * (long)(b.z - c.z) - (long)(p.z - c.z) * (long)(b.x - c.x) <= 0L && (long)(p.x - c.x) * (long)(d.z - c.z) - (long)(p.z - c.z) * (long)(d.x - c.x) >= 0L;
	}

	public static bool PointInsideTriangle2D(FixedVector2D a, FixedVector2D b, FixedVector2D c, FixedVector2D p)
	{
		return (long)(p.x - a.x) * (long)(b.z - a.z) - (long)(p.z - a.z) * (long)(b.x - a.x) >= 0L && (long)(p.x - a.x) * (long)(c.z - a.z) - (long)(p.z - a.z) * (long)(c.x - a.x) <= 0L && (long)(p.x - c.x) * (long)(b.z - c.z) - (long)(p.z - c.z) * (long)(b.x - c.x) <= 0L;
	}

	public static bool QuadOverlap2D(FixedVector2D a, FixedVector2D b, FixedVector2D c, FixedVector2D d, FixedVector2D p, FixedVector2D q, FixedVector2D r, FixedVector2D s)
	{
		return FixedMath.SegmentIntersect2D(b - a, q - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(d - a, q - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(b - c, q - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(d - c, q - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(b - a, s - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(d - a, s - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(b - c, s - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(d - c, s - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(b - a, q - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(d - a, q - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(b - c, q - r, c - r) >= 0 || FixedMath.SegmentIntersect2D(d - c, q - r, c - r) >= 0 || FixedMath.SegmentIntersect2D(b - a, s - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(d - a, s - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(b - c, s - r, c - r) >= 0 || FixedMath.SegmentIntersect2D(d - c, s - r, c - r) >= 0 || FixedMath.PointInsideQuad2D(a, b, c, d, p) || FixedMath.PointInsideQuad2D(p, q, r, s, a);
	}

	public static bool QuadTriangleOverlap2D(FixedVector2D a, FixedVector2D b, FixedVector2D c, FixedVector2D d, FixedVector2D p, FixedVector2D q, FixedVector2D r)
	{
		return FixedMath.SegmentIntersect2D(b - a, q - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(d - a, q - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(b - c, q - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(d - c, q - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(b - a, r - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(d - a, r - p, a - p) >= 0 || FixedMath.SegmentIntersect2D(b - c, r - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(d - c, r - p, c - p) >= 0 || FixedMath.SegmentIntersect2D(b - a, q - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(d - a, q - r, a - r) >= 0 || FixedMath.SegmentIntersect2D(b - c, q - r, c - r) >= 0 || FixedMath.SegmentIntersect2D(d - c, q - r, c - r) >= 0 || FixedMath.PointInsideQuad2D(a, b, c, d, p) || FixedMath.PointInsideTriangle2D(p, q, r, a);
	}

	public static FixedVector3D BezierPosition3D(FixedVector3D v1, FixedVector3D v2, FixedVector3D v3, FixedVector3D v4, long t)
	{
		long num = 32768L - t;
		long num2 = t * t * t >> 15;
		long num3 = 3L * num * t * t >> 15;
		long num4 = 3L * t * num * num >> 15;
		long num5 = num * num * num >> 15;
		FixedVector3D result;
		result.x = (int)((long)v1.x * num5 + (long)v2.x * num4 + (long)v3.x * num3 + (long)v4.x * num2 >> 30);
		result.y = (int)((long)v1.y * num5 + (long)v2.y * num4 + (long)v3.y * num3 + (long)v4.y * num2 >> 30);
		result.z = (int)((long)v1.z * num5 + (long)v2.z * num4 + (long)v3.z * num3 + (long)v4.z * num2 >> 30);
		return result;
	}

	public static FixedVector3D BezierTangent3D(FixedVector3D v1, FixedVector3D v2, FixedVector3D v3, FixedVector3D v4, long t)
	{
		long num = t * t;
		long num2 = 3L * num;
		long num3 = (6L * t << 15) - 9L * num;
		long num4 = (long)((ulong)-1073741824 - (ulong)((ulong)(12L * t) << 15) + (ulong)(9L * num));
		long num5 = (6L * t << 15) - (long)((ulong)-1073741824) - 3L * num;
		FixedVector3D result;
		result.x = (int)((long)v1.x * num5 + (long)v2.x * num4 + (long)v3.x * num3 + (long)v4.x * num2 >> 30);
		result.y = (int)((long)v1.y * num5 + (long)v2.y * num4 + (long)v3.y * num3 + (long)v4.y * num2 >> 30);
		result.z = (int)((long)v1.z * num5 + (long)v2.z * num4 + (long)v3.z * num3 + (long)v4.z * num2 >> 30);
		return result;
	}

	private static readonly long[] sinTable = new long[]
	{
		0L,
		411733L,
		823219L,
		1234209L,
		1644455L,
		2053710L,
		2461729L,
		2868265L,
		3273072L,
		3675909L,
		4076531L,
		4474698L,
		4870169L,
		5262706L,
		5652074L,
		6038037L,
		6420363L,
		6798821L,
		7173184L,
		7543226L,
		7908725L,
		8269459L,
		8625213L,
		8975771L,
		9320922L,
		9660458L,
		9994176L,
		10321873L,
		10643353L,
		10958422L,
		11266890L,
		11568571L,
		11863283L,
		12150850L,
		12431097L,
		12703856L,
		12968963L,
		13226258L,
		13475586L,
		13716797L,
		13949745L,
		14174291L,
		14390298L,
		14597637L,
		14796184L,
		14985817L,
		15166424L,
		15337895L,
		15500126L,
		15653022L,
		15796488L,
		15930439L,
		16054795L,
		16169479L,
		16274424L,
		16369565L,
		16454846L,
		16530216L,
		16595628L,
		16651044L,
		16696429L,
		16731757L,
		16757007L,
		16772163L,
		16777216L,
		16777216L
	};

	private static readonly long[] arcsinTable = new long[]
	{
		0L,
		41723L,
		83457L,
		125210L,
		166995L,
		208820L,
		250697L,
		292636L,
		334647L,
		376742L,
		418932L,
		461227L,
		503639L,
		546180L,
		588863L,
		631699L,
		674701L,
		717883L,
		761258L,
		804841L,
		848645L,
		892688L,
		936985L,
		981552L,
		1026407L,
		1071570L,
		1117059L,
		1162895L,
		1209100L,
		1255698L,
		1302713L,
		1350171L,
		1398101L,
		1446533L,
		1495500L,
		1545037L,
		1595181L,
		1645974L,
		1697462L,
		1749692L,
		1802721L,
		1856606L,
		1911414L,
		1967219L,
		2024103L,
		2082158L,
		2141489L,
		2202216L,
		2264476L,
		2328427L,
		2394256L,
		2462183L,
		2532470L,
		2605437L,
		2681477L,
		2761086L,
		2844902L,
		2933771L,
		3028855L,
		3131829L,
		3245267L,
		3373505L,
		3525009L,
		3721662L,
		4194304L,
		4194304L
	};
}
