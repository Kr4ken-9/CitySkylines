﻿using System;

public struct DrawCallData
{
	public void Add(DrawCallData source)
	{
		this.m_defaultCalls += source.m_defaultCalls;
		this.m_lodCalls += source.m_lodCalls;
		this.m_batchedCalls += source.m_batchedCalls;
		this.m_overlayCalls += source.m_overlayCalls;
	}

	public int m_defaultCalls;

	public int m_lodCalls;

	public int m_batchedCalls;

	public int m_overlayCalls;
}
