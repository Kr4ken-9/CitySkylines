﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class ThemeSettingsPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		this.SpawnEntry("Snapshot", 0);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		if (comp.get_zOrder() == 0)
		{
			ToolsModifierControl.SetTool<SnapshotTool>();
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("THEMESETTINGS_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("THEMESETTINGS_DESC", name)
		});
		return base.CreateButton(name, tooltip, "ThemeSettings" + name, index, GeneratedPanel.tooltipBox);
	}
}
