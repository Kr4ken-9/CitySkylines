﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TransportPatch
{
	public TransportPatch(int x, int z, TransportInfo info)
	{
		this.m_x = x;
		this.m_z = z;
		this.m_info = info;
		float num = 1920f;
		Vector3 vector;
		vector..ctor(((float)x - 4.5f + 0.5f) * num, 512f, ((float)z - 4.5f + 0.5f) * num);
		Vector3 vector2;
		vector2..ctor(num, 1024f, num);
		this.m_bounds = new Bounds(vector, vector2);
	}

	public void Release()
	{
		if (this.m_mesh != null)
		{
			Object.Destroy(this.m_mesh);
			this.m_mesh = null;
		}
	}

	public void RenderOverlay(RenderManager.CameraInfo cameraInfo, int typeMask)
	{
		if ((1 << (int)this.m_info.m_transportType & typeMask) == 0)
		{
			return;
		}
		if (this.m_meshData != null)
		{
			this.UpdateMesh();
		}
		if (this.m_mesh != null)
		{
			Material pathMaterial = this.m_info.m_pathMaterial2;
			if (pathMaterial != null && cameraInfo.Intersect(this.m_bounds))
			{
				Singleton<TerrainManager>.get_instance().SetWaterMaterialProperties(this.m_bounds.get_center(), pathMaterial);
				if (pathMaterial.SetPass(0))
				{
					TransportManager expr_8F_cp_0 = Singleton<TransportManager>.get_instance();
					expr_8F_cp_0.m_drawCallData.m_overlayCalls = expr_8F_cp_0.m_drawCallData.m_overlayCalls + 1;
					Graphics.DrawMeshNow(this.m_mesh, Matrix4x4.get_identity());
				}
			}
		}
	}

	private void UpdateMesh()
	{
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		RenderGroup.MeshData meshData;
		try
		{
			meshData = this.m_meshData;
			this.m_meshData = null;
		}
		finally
		{
			Monitor.Exit(this);
		}
		if (meshData != null)
		{
			if (meshData.m_vertices != null)
			{
				if (this.m_mesh == null)
				{
					this.m_mesh = new Mesh();
				}
				meshData.PopulateMesh(this.m_mesh);
			}
			else if (this.m_mesh != null)
			{
				Object.Destroy(this.m_mesh);
				this.m_mesh = null;
			}
		}
	}

	public void UpdateMeshData()
	{
		this.m_isDirty = false;
		NetManager instance = Singleton<NetManager>.get_instance();
		FastList<ushort> serviceSegments = instance.GetServiceSegments(this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		FastList<ushort> fastList = null;
		if (this.m_info.m_secondaryNetService != ItemClass.Service.None)
		{
			fastList = instance.GetServiceSegments(this.m_info.m_secondaryNetService, this.m_info.m_secondaryNetSubService);
		}
		FastList<ushort> tempSegments = TransportPatch.m_tempSegments;
		tempSegments.Clear();
		FastList<ushort> tempNodes = TransportPatch.m_tempNodes;
		tempNodes.Clear();
		float num = 17280f;
		float num2 = 1920f;
		int num3 = 0;
		int num4 = 0;
		if (serviceSegments != null)
		{
			num3 = serviceSegments.m_size;
		}
		if (fastList != null)
		{
			num4 = fastList.m_size;
		}
		int num5 = num3 + num4;
		for (int i = 0; i < num5; i++)
		{
			ushort num6;
			if (i < num3)
			{
				num6 = serviceSegments.m_buffer[i];
			}
			else
			{
				num6 = fastList.m_buffer[i - num3];
			}
			NetInfo info = instance.m_segments.m_buffer[(int)num6].Info;
			if ((info.m_vehicleTypes & this.m_info.m_vehicleType) != VehicleInfo.VehicleType.None)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)num6].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)num6].m_endNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				Vector3 vector = (position + position2) * 0.5f;
				int num7 = Mathf.Clamp((int)((vector.x + num * 0.5f) / num2), 0, 8);
				int num8 = Mathf.Clamp((int)((vector.z + num * 0.5f) / num2), 0, 8);
				if (num7 == this.m_x && num8 == this.m_z)
				{
					tempSegments.Add(num6);
				}
				if (this.m_info.m_usePathNodes)
				{
					if ((instance.m_nodes.m_buffer[(int)startNode].m_flags & (NetNode.Flags.Middle | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.None)
					{
						int j = 0;
						while (j < 8)
						{
							ushort segment = instance.m_nodes.m_buffer[(int)startNode].GetSegment(j);
							if (segment == 0)
							{
								j++;
							}
							else
							{
								if (segment != num6)
								{
									break;
								}
								num7 = Mathf.Clamp((int)((position.x + num * 0.5f) / num2), 0, 8);
								num8 = Mathf.Clamp((int)((position.z + num * 0.5f) / num2), 0, 8);
								if (num7 == this.m_x && num8 == this.m_z)
								{
									tempNodes.Add(startNode);
								}
								break;
							}
						}
					}
					if ((instance.m_nodes.m_buffer[(int)endNode].m_flags & (NetNode.Flags.Middle | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.None)
					{
						int k = 0;
						while (k < 8)
						{
							ushort segment2 = instance.m_nodes.m_buffer[(int)endNode].GetSegment(k);
							if (segment2 == 0)
							{
								k++;
							}
							else
							{
								if (segment2 != num6)
								{
									break;
								}
								num7 = Mathf.Clamp((int)((position2.x + num * 0.5f) / num2), 0, 8);
								num8 = Mathf.Clamp((int)((position2.z + num * 0.5f) / num2), 0, 8);
								if (num7 == this.m_x && num8 == this.m_z)
								{
									tempNodes.Add(endNode);
								}
								break;
							}
						}
					}
				}
			}
		}
		int num9 = tempSegments.m_size + tempNodes.m_size;
		if (num9 != 0)
		{
			RenderGroup.MeshData meshData = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Colors, num9 * 8, num9 * 30);
			int num10 = 0;
			for (int l = 0; l < tempSegments.m_size; l++)
			{
				ushort num11 = tempSegments.m_buffer[l];
				ushort startNode2 = instance.m_segments.m_buffer[(int)num11].m_startNode;
				ushort endNode2 = instance.m_segments.m_buffer[(int)num11].m_endNode;
				Vector3 a = instance.m_nodes.m_buffer[(int)startNode2].m_position;
				Vector3 d = instance.m_nodes.m_buffer[(int)endNode2].m_position;
				Vector3 startDir = instance.m_segments.m_buffer[(int)num11].m_startDirection;
				Vector3 endDir = instance.m_segments.m_buffer[(int)num11].m_endDirection;
				Bezier3 bezier = default(Bezier3);
				bezier.a = a;
				bezier.d = d;
				NetSegment.CalculateMiddlePoints(bezier.a, startDir, bezier.d, endDir, true, true, out bezier.b, out bezier.c);
				if (this.m_info.m_usePathNodes)
				{
					bool flag = false;
					if ((instance.m_nodes.m_buffer[(int)startNode2].m_flags & (NetNode.Flags.Middle | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.None)
					{
						float num12 = bezier.Travel(0f, this.m_info.m_pathHalfWidth * 0.7f);
						a = bezier.Position(num12);
						startDir = VectorUtils.NormalizeXZ(bezier.Tangent(num12));
						flag = true;
					}
					if ((instance.m_nodes.m_buffer[(int)endNode2].m_flags & (NetNode.Flags.Middle | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.None)
					{
						float num13 = bezier.Travel(1f, -this.m_info.m_pathHalfWidth * 0.7f);
						d = bezier.Position(num13);
						endDir = -VectorUtils.NormalizeXZ(bezier.Tangent(num13));
						flag = true;
					}
					if (flag)
					{
						bezier.a = a;
						bezier.d = d;
						NetSegment.CalculateMiddlePoints(bezier.a, startDir, bezier.d, endDir, true, true, out bezier.b, out bezier.c);
					}
				}
				float num14 = Vector3.Distance(bezier.a, bezier.b) + Vector3.Distance(bezier.b, bezier.c) + Vector3.Distance(bezier.c, bezier.d);
				TransportLine.FillPathSegment(bezier, meshData, null, num10++, 0, 0f, Mathf.Max(1f, Mathf.Round(num14 / this.m_info.m_pathTiling)), this.m_info.m_pathHalfWidth, 30f, true);
			}
			for (int m = 0; m < tempNodes.m_size; m++)
			{
				ushort num15 = tempNodes.m_buffer[m];
				Vector3 position3 = instance.m_nodes.m_buffer[(int)num15].m_position;
				TransportLine.FillPathNode(position3, meshData, num10++, this.m_info.m_pathHalfWidth, 30f, true);
			}
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_meshData = meshData;
			}
			finally
			{
				Monitor.Exit(this);
			}
		}
		else
		{
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_meshData = new RenderGroup.MeshData();
			}
			finally
			{
				Monitor.Exit(this);
			}
		}
	}

	public TransportInfo m_info;

	public int m_x;

	public int m_z;

	public Mesh m_mesh;

	public RenderGroup.MeshData m_meshData;

	public TransportPatch m_nextPatch;

	public bool m_isDirty;

	public Bounds m_bounds;

	private static FastList<ushort> m_tempSegments = new FastList<ushort>();

	private static FastList<ushort> m_tempNodes = new FastList<ushort>();
}
