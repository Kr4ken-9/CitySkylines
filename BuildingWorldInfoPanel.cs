﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class BuildingWorldInfoPanel : WorldInfoPanel
{
	protected virtual string problemIconTemplate
	{
		get
		{
			return BuildingWorldInfoPanel.kProblemIconTemplate;
		}
	}

	public string buildingName
	{
		get
		{
			return this.m_NameField.get_text();
		}
	}

	protected override void Start()
	{
		base.Start();
		this.m_AllGood = base.Find<UILabel>("AllGood");
		this.m_NameField = base.Find<UITextField>("BuildingName");
		this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_IconsContainer = base.Find<UIPanel>("ProblemsPanel");
		this.m_IconsContainer.set_color(this.m_NoProblemoColor);
		this.InitializeIcons();
	}

	private void InitializeIcons()
	{
		for (int i = 0; i < BuildingWorldInfoPanel.kNotifications.Length; i++)
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(this.problemIconTemplate);
			asGameObject.set_name(BuildingWorldInfoPanel.kNotifications[i].get_enumName());
			UIComponent uIComponent = this.m_IconsContainer.AttachUIComponent(asGameObject);
			uIComponent.set_isVisible(false);
			if (i == 0)
			{
				this.m_IconWidth = uIComponent.get_width() + (float)this.m_IconsContainer.get_autoLayoutPadding().get_horizontal();
			}
		}
	}

	protected override void OnSetTarget()
	{
		this.m_NameField.set_text(this.GetName());
		this.m_Time = 0f;
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		BuildingWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new BuildingWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		if (this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			return Singleton<BuildingManager>.get_instance().GetBuildingName(this.m_InstanceID.Building, InstanceID.Empty);
		}
		return string.Empty;
	}

	protected string GetInfoString(string type, int value)
	{
		return StringUtils.SafeFormat(Locale.Get(type), value);
	}

	private void UpdateNotification(int i, string spriteName, string tooltip)
	{
		UISprite uISprite = this.m_IconsContainer.get_components()[i] as UISprite;
		if (spriteName == null || tooltip == null)
		{
			uISprite.set_isVisible(false);
		}
		else
		{
			uISprite.set_spriteName(spriteName);
			uISprite.set_tooltip(tooltip);
			uISprite.set_isVisible(true);
			uISprite.set_opacity(0.75f + Mathf.Cos(this.m_Time * 3.14159274f * 1.5f + (float)i * 123.1263f) * 0.25f);
		}
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			string text = string.Empty;
			int num = 0;
			Building building = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building];
			BuildingInfo info = building.Info;
			for (int i = 0; i < BuildingWorldInfoPanel.kNotifications.Length; i++)
			{
				Notification.Problem enumValue = BuildingWorldInfoPanel.kNotifications[i].get_enumValue();
				if ((building.m_problems & enumValue) != Notification.Problem.None)
				{
					if ((building.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
					{
						if (string.IsNullOrEmpty(text))
						{
							text = BuildingWorldInfoPanel.kNotifications[i].get_enumName();
						}
						this.UpdateNotification(i, BuildingWorldInfoPanel.kNotificationsFatal[i].get_enumName(), this.GetNotificationLocale("NOTIFICATION_FATAL", "NOTIFICATION_FATAL_PLAYER", "NOTIFICATION_FATAL_ND", BuildingWorldInfoPanel.kNotifications[i].get_enumName(), info));
						num++;
					}
					else if ((building.m_problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
					{
						if (string.IsNullOrEmpty(text))
						{
							text = BuildingWorldInfoPanel.kNotifications[i].get_enumName();
						}
						this.UpdateNotification(i, BuildingWorldInfoPanel.kNotificationsMajor[i].get_enumName(), this.GetNotificationLocale("NOTIFICATION_MAJOR", "NOTIFICATION_MAJOR_PLAYER", null, BuildingWorldInfoPanel.kNotifications[i].get_enumName(), info));
						num++;
					}
					else
					{
						if (string.IsNullOrEmpty(text))
						{
							text = BuildingWorldInfoPanel.kNotifications[i].get_enumName();
						}
						this.UpdateNotification(i, BuildingWorldInfoPanel.kNotificationsNormal[i].get_enumName(), this.GetNotificationLocale("NOTIFICATION_NORMAL", "NOTIFICATION_NORMAL_PLAYER", null, BuildingWorldInfoPanel.kNotifications[i].get_enumName(), info));
						num++;
					}
				}
				else
				{
					this.UpdateNotification(i, null, null);
				}
			}
			if (building.m_problems == Notification.Problem.None)
			{
				this.m_IconsContainer.set_color(this.m_NoProblemoColor);
			}
			else if ((building.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				this.m_IconsContainer.set_color(this.m_ProblemoBizarroFatalColor);
			}
			else if ((building.m_problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
			{
				this.m_IconsContainer.set_color(this.m_ProblemoBizarroMajorColor);
			}
			else
			{
				this.m_IconsContainer.set_color(this.m_ProblemoBizarroNormalColor);
			}
			this.m_AllGood.set_width(this.m_IconsContainer.get_width() - (float)this.m_IconsContainer.get_padding().get_horizontal() - this.m_IconWidth * (float)num);
			if (building.m_problems == Notification.Problem.None)
			{
				this.m_AllGood.set_text(string.Empty);
			}
			else if (num == 1)
			{
				if ((building.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
				{
					if (building.GetLastFrameData().m_fireDamage == 255)
					{
						this.m_AllGood.set_text(Locale.Get("NOTIFICATION_BURNED_DOWN"));
					}
					else if (building.m_levelUpProgress == 255)
					{
						this.m_AllGood.set_text(Locale.Get("NOTIFICATION_VISITED"));
					}
					else
					{
						this.m_AllGood.set_text(Locale.Get("NOTIFICATION_COLLAPSED"));
					}
				}
				else if ((building.m_flags & Building.Flags.Abandoned) != Building.Flags.None)
				{
					this.m_AllGood.set_text(Locale.Get("NOTIFICATION_ABANDONED"));
				}
				else
				{
					this.m_AllGood.set_text(this.GetNotificationLocale("NOTIFICATION_TITLE", "NOTIFICATION_TITLE_PLAYER", null, text, info));
				}
			}
			else
			{
				this.m_AllGood.set_text(Locale.Get("NOTIFICATION_MULTIPLE"));
			}
		}
		this.m_Time += Time.get_deltaTime();
	}

	private string GetNotificationLocale(string automaticID, string manualID, string disasterID, string key, BuildingInfo info)
	{
		string result;
		if (disasterID != null && Singleton<LoadingManager>.get_instance().SupportsExpansion(2) && Locale.GetUnchecked(disasterID, key, ref result))
		{
			return result;
		}
		string result2;
		if (info != null && info.m_placementStyle == ItemClass.Placement.Manual && Locale.GetUnchecked(manualID, key, ref result2))
		{
			return result2;
		}
		return Locale.Get(automaticID, key);
	}

	private static readonly string kProblemIconTemplate = "ProblemIconTemplate";

	private static readonly PositionData<Notification.Problem>[] kNotificationsNormal = Utils.GetOrderedEnumData<Notification.Problem>("Normal");

	private static readonly PositionData<Notification.Problem>[] kNotificationsMajor = Utils.GetOrderedEnumData<Notification.Problem>("Major");

	private static readonly PositionData<Notification.Problem>[] kNotificationsFatal = Utils.GetOrderedEnumData<Notification.Problem>("Fatal");

	private static readonly PositionData<Notification.Problem>[] kNotifications = Utils.GetOrderedEnumData<Notification.Problem>("Text");

	protected UITextField m_NameField;

	private UIPanel m_IconsContainer;

	private UILabel m_AllGood;

	public Color32 m_NoProblemoColor;

	public Color32 m_ProblemoBizarroNormalColor;

	public Color32 m_ProblemoBizarroMajorColor;

	public Color32 m_ProblemoBizarroFatalColor;

	private float m_Time;

	private float m_IconWidth;
}
