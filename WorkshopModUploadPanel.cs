﻿using System;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class WorkshopModUploadPanel : UICustomControl
{
	private void Awake()
	{
		this.m_ShareButton = base.Find<UIButton>("Share");
		this.m_ShareButton.add_eventClick(new MouseEventHandler(this.OnShare));
		this.m_ShareButton.set_isEnabled(false);
		this.m_LegalLink = base.Find<UIButton>("LegalLink");
		this.m_LegalLink.add_eventClick(new MouseEventHandler(this.OnLegalAgreement));
		this.m_FolderSelect = base.Find<UIButton>("FolderSelect");
		this.m_FolderSelect.add_eventClick(new MouseEventHandler(this.OnFolderSelect));
		this.m_IncludeSource = base.Find<UICheckBox>("IncludeSource");
		this.m_Title = base.Find<UITextField>("Title");
		this.m_Desc = base.Find<UITextField>("Desc");
		this.m_ChangeNote = base.Find<UITextField>("ChangeNote");
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_UpdateBar = base.Find<UIProgressBar>("UpdateBar");
		this.m_UpdateBar.Hide();
		this.m_UpdateText = base.Find<UILabel>("UpdateText");
		this.m_UpdateText.Hide();
	}

	private void OnEnable()
	{
		PlatformService.get_workshop().add_eventCreateItem(new Workshop.CreateItemResultHandler(this.OnItemCreated));
		PlatformService.get_workshop().add_eventSubmitItemUpdate(new Workshop.SubmitItemUpdateResultHandler(this.OnItemSubmitted));
	}

	private void OnDisable()
	{
		PlatformService.get_workshop().remove_eventCreateItem(new Workshop.CreateItemResultHandler(this.OnItemCreated));
		PlatformService.get_workshop().remove_eventSubmitItemUpdate(new Workshop.SubmitItemUpdateResultHandler(this.OnItemSubmitted));
	}

	private void Update()
	{
		if (base.get_component().get_isVisible() && this.m_CurrentHandle != UGCHandle.invalid)
		{
			ItemUpdateStatus updateStatus = PlatformService.get_workshop().GetUpdateStatus(this.m_CurrentHandle);
			this.m_UpdateText.set_isVisible(updateStatus != 0);
			this.m_UpdateText.set_text(Locale.Get("WORKSHOP_UPDATESTATUS", updateStatus.ToString()));
			float updateProgress = PlatformService.get_workshop().GetUpdateProgress(this.m_CurrentHandle);
			this.m_UpdateBar.set_isVisible(updateStatus == 3 || updateStatus == 4);
			this.m_UpdateBar.set_value(updateProgress);
			this.m_ShareButton.set_isEnabled(updateStatus == 0);
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		string fullPath = ((FileSystemEventArgs)e.get_arguments()).FullPath;
		if (string.Compare(Path.GetFileName(fullPath), "PreviewImage.png", StringComparison.OrdinalIgnoreCase) == 0)
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.ReloadPreviewImage();
			});
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
	}

	private void UpdateItem()
	{
		try
		{
			string text = this.m_Title.get_text();
			string text2 = this.m_Desc.get_text();
			string text3 = this.m_ChangeNote.get_text();
			string[] files = Directory.GetFiles(this.m_ContentPath, "*.*", SearchOption.AllDirectories);
			if (!this.m_IncludeSource.get_isChecked())
			{
				try
				{
					DirectoryUtils.DeleteDirectory(Path.Combine(this.m_ContentPath, "Source"));
				}
				catch (DirectoryNotFoundException ex)
				{
					ex.HelpLink = string.Empty;
				}
				catch (Exception ex2)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Failed to delete source for " + this.m_ContentPath + "\n" + ex2.ToString());
				}
			}
			WorkshopHelper.VerifyAndFinalizeFiles(files, this.m_PublishedFileId.ToString());
			this.m_CurrentHandle = PlatformService.get_workshop().UpdateItem(this.m_PublishedFileId, text, text2, text3, this.m_PreviewPath, this.m_ContentPath, new string[]
			{
				"Mod"
			});
		}
		catch (Exception ex3)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, ex3.GetType() + " " + ex3.Message);
			UIView.ForwardException(new StagingException("Workshop Staging Failed", ex3));
		}
	}

	public void OnLegalAgreement(UIComponent component, UIMouseEventParameter p)
	{
		PlatformService.ActivateGameOverlay(8);
	}

	public void OnFolderSelect(UIComponent component, UIMouseEventParameter p)
	{
		Utils.OpenInFileBrowser(this.m_StagingPath);
	}

	public void OnShare(UIComponent component, UIMouseEventParameter p)
	{
		if (this.m_PublishedFileId == PublishedFileId.invalid)
		{
			if (!PlatformService.get_workshop().CreateItem())
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "An error occurred while creating a new workshop item");
			}
		}
		else
		{
			this.UpdateItem();
		}
	}

	public void OnClosed()
	{
		this.StopWatchingPath();
		try
		{
			DirectoryUtils.DeleteDirectory(this.m_StagingPath);
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Failed to delete " + this.m_StagingPath + "\n" + ex.ToString());
		}
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	private void OnItemSubmitted(SubmitItemUpdateResult res, bool ioError)
	{
		if (base.get_component().get_isVisible() && this.m_CurrentHandle != UGCHandle.invalid)
		{
			if (res.result == 1)
			{
				if (this.m_PublishedFileId != PublishedFileId.invalid)
				{
					PlatformService.ActivateGameOverlayToWorkshopItem(this.m_PublishedFileId);
				}
				this.OnClosed();
			}
			else
			{
				UIView.ForwardException(new WorkshopException("Error submitting workshop item:" + res.result, null));
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Error submitting workshop item [" + res.result + "]");
			}
		}
	}

	private void OnItemCreated(CreateItemResult res, bool ioError)
	{
		if (base.get_component().get_isVisible() && res.result == 1)
		{
			if (res.userNeedsToAcceptWorkshopLegalAgreement)
			{
				PlatformService.ActivateGameOverlay(8);
			}
			this.m_PublishedFileId = res.publishedFileId;
			this.UpdateItem();
		}
	}

	private void StartWatchingPath()
	{
		for (int i = 0; i < WorkshopModUploadPanel.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + WorkshopModUploadPanel.m_Extensions[i], this.m_StagingPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Start();
			}
		}
	}

	private void StopWatchingPath()
	{
		for (int i = 0; i < WorkshopModUploadPanel.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Stop();
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void PrepareStagingArea(Texture previewTexture)
	{
		string path = Guid.NewGuid().ToString();
		this.m_StagingPath = Path.Combine(Path.Combine(DataLocation.get_localApplicationData(), "WorkshopStagingArea"), path);
		Directory.CreateDirectory(this.m_StagingPath);
		Texture2D texture2D = previewTexture as Texture2D;
		if (texture2D == null)
		{
			texture2D = (Object.Instantiate<Texture>(this.m_DefaultModPreviewTexture) as Texture2D);
		}
		this.m_PreviewPath = Path.Combine(this.m_StagingPath, "PreviewImage.png");
		File.WriteAllBytes(this.m_PreviewPath, texture2D.EncodeToPNG());
		this.ReloadPreviewImage();
		this.m_ContentPath = Path.Combine(this.m_StagingPath, "Content" + Path.DirectorySeparatorChar);
		Directory.CreateDirectory(this.m_ContentPath);
		WorkshopHelper.DirectoryCopy(this.m_TargetFolder, this.m_ContentPath, true);
		this.StartWatchingPath();
	}

	public void SetAsset(string folder, PublishedFileId id)
	{
		this.m_PublishedFileId = id;
		this.RequestDetails();
		this.m_ShareButton.set_localeID((!(id != PublishedFileId.invalid)) ? "WORKSHOP_SHARE" : "WORKSHOP_UPDATE");
		this.SetAssetInternal(folder);
	}

	public void SetAsset(string folder)
	{
		this.m_PublishedFileId = PublishedFileId.invalid;
		this.m_ShareButton.set_localeID("WORKSHOP_SHARE");
		this.SetAssetInternal(folder);
	}

	private void OnDetailsReceived(UGCDetails details, bool ioError)
	{
		if (this.m_PublishedFileId == details.publishedFileId)
		{
			PlatformService.get_workshop().remove_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
			this.m_Title.set_text(details.title);
			this.m_Desc.set_text(details.description);
		}
	}

	public void RequestDetails()
	{
		if (this.m_PublishedFileId != PublishedFileId.invalid)
		{
			PlatformService.get_workshop().remove_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
			PlatformService.get_workshop().add_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
			PlatformService.get_workshop().RequestItemDetails(this.m_PublishedFileId);
		}
	}

	private void OnDestroy()
	{
		PlatformService.get_workshop().remove_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
	}

	private void ReloadPreviewImage()
	{
		if (this.m_SnapShot.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShot.get_texture());
		}
		Image image = new Image(this.m_PreviewPath);
		this.m_SnapShot.set_texture(image.CreateTexture());
		this.m_SnapShot.get_texture().set_wrapMode(1);
	}

	private void SetAssetInternal(string folder)
	{
		this.m_StagingPath = null;
		this.m_PreviewPath = null;
		this.m_ContentPath = null;
		this.m_CurrentHandle = UGCHandle.invalid;
		this.m_ShareButton.set_isEnabled(false);
		this.m_TargetFolder = folder;
		this.m_UpdateText.set_isVisible(false);
		this.m_UpdateBar.set_isVisible(false);
		this.m_ShareButton.set_isEnabled(true);
		string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(folder);
		this.m_Title.set_text(fileNameWithoutExtension);
		this.m_Desc.set_text(fileNameWithoutExtension);
		this.m_ChangeNote.set_text(string.Empty);
		this.PrepareStagingArea(null);
	}

	private const string kPreviewImage = "PreviewImage.png";

	public Texture m_DefaultModPreviewTexture;

	private UITextField m_Title;

	private UITextField m_Desc;

	private UITextField m_ChangeNote;

	private UITextureSprite m_SnapShot;

	private UIProgressBar m_UpdateBar;

	private UILabel m_UpdateText;

	private UIButton m_ShareButton;

	private UIButton m_LegalLink;

	private UIButton m_FolderSelect;

	private UICheckBox m_IncludeSource;

	private string m_TargetFolder;

	private UGCHandle m_CurrentHandle = UGCHandle.invalid;

	private PublishedFileId m_PublishedFileId = PublishedFileId.invalid;

	private string m_StagingPath;

	private string m_PreviewPath;

	private string m_ContentPath;

	private static readonly string[] m_Extensions = Image.GetExtensions(4);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[WorkshopModUploadPanel.m_Extensions.Length];
}
