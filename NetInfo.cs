﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

[ExecuteInEditMode]
[Serializable]
public class NetInfo : PrefabInfo
{
	public static NetInfo.Direction InvertDirection(NetInfo.Direction direction)
	{
		if (direction == NetInfo.Direction.Forward)
		{
			return NetInfo.Direction.Backward;
		}
		if (direction == NetInfo.Direction.Backward)
		{
			return NetInfo.Direction.Forward;
		}
		if (direction == NetInfo.Direction.AvoidBackward)
		{
			return NetInfo.Direction.AvoidForward;
		}
		if (direction != NetInfo.Direction.AvoidForward)
		{
			return direction;
		}
		return NetInfo.Direction.AvoidBackward;
	}

	public float GetMinNodeDistance()
	{
		if (this.m_halfWidth < 3.5f)
		{
			return 7f;
		}
		if (this.m_halfWidth < 7.5f)
		{
			return 15f;
		}
		return 23f;
	}

	public ItemClass GetConnectionClass()
	{
		if (this.m_connectionClass == null)
		{
			return this.m_class;
		}
		return this.m_connectionClass;
	}

	public bool IsCombatible(NetInfo with)
	{
		return this.m_netAI.IsCombatible(with);
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		this.m_requireSurfaceMaps = false;
		this.m_maxBuildAngleCos = Mathf.Cos(this.m_maxBuildAngle * 0.0174532924f);
		this.m_maxTurnAngleCos = Mathf.Cos(this.m_maxTurnAngle * 0.0174532924f);
		this.m_hasForwardVehicleLanes = false;
		this.m_hasBackwardVehicleLanes = false;
		this.m_forwardVehicleLaneCount = 0;
		this.m_backwardVehicleLaneCount = 0;
		this.m_hasPedestrianLanes = false;
		this.m_hasTerrainHeightProps = false;
		float levelOfDetailFactor = RenderManager.LevelOfDetailFactor;
		float lodRenderDistance = Mathf.Clamp(100f + levelOfDetailFactor * Mathf.Max(this.m_halfWidth * 50f, (this.m_maxHeight - this.m_minHeight) * 80f), 100f, 1000f);
		bool flag = false;
		bool flag2 = false;
		this.m_color = Color.get_white();
		this.m_netLayers = 1 << this.m_prefabDataLayer;
		this.m_nodeConnectGroups = NetInfo.ConnectGroup.None;
		if (this.m_segments != null)
		{
			for (int i = 0; i < this.m_segments.Length; i++)
			{
				if (this.InitSegmentInfo(this.m_segments[i], ref this.m_requireSurfaceMaps))
				{
					this.m_segments[i].m_lodRenderDistance = lodRenderDistance;
				}
				else
				{
					this.m_segments[i].m_lodRenderDistance = 100000f;
					flag = true;
				}
				if (!flag2 && this.m_segments[i].m_material != null)
				{
					this.m_color = this.m_segments[i].m_material.get_color();
					flag2 = true;
				}
				this.m_netLayers |= 1 << this.m_segments[i].m_layer;
			}
		}
		if (this.m_nodes != null)
		{
			for (int j = 0; j < this.m_nodes.Length; j++)
			{
				if (this.InitNodeInfo(this.m_nodes[j], ref this.m_requireSurfaceMaps, ref this.m_requireSegmentRenderers, ref this.m_requireDirectRenderers))
				{
					this.m_nodes[j].m_lodRenderDistance = lodRenderDistance;
				}
				else
				{
					this.m_nodes[j].m_lodRenderDistance = 100000f;
					flag = true;
				}
				if (!flag2 && this.m_nodes[j].m_material != null)
				{
					this.m_color = this.m_nodes[j].m_material.get_color();
					flag2 = true;
				}
				this.m_netLayers |= 1 << this.m_nodes[j].m_layer;
				if (this.m_nodes[j].m_directConnect)
				{
					this.m_nodeConnectGroups |= (this.m_nodes[j].m_connectGroup & NetInfo.ConnectGroup.AllGroups);
				}
			}
		}
		if (flag)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		this.m_laneTypes = NetInfo.LaneType.None;
		this.m_vehicleTypes = VehicleInfo.VehicleType.None;
		this.m_hasParkingSpaces = false;
		if (this.m_lanes != null)
		{
			bool flag3 = Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True;
			this.m_sortedLanes = new int[this.m_lanes.Length];
			this.m_averageVehicleLaneSpeed = 0f;
			int num = 0;
			for (int k = 0; k < this.m_lanes.Length; k++)
			{
				this.m_laneTypes |= this.m_lanes[k].m_laneType;
				if (this.m_lanes[k].m_laneType == NetInfo.LaneType.Parking)
				{
					this.m_hasParkingSpaces = true;
				}
				else if (this.m_lanes[k].m_laneType == NetInfo.LaneType.Pedestrian)
				{
					this.m_hasPedestrianLanes = true;
				}
				else if ((byte)(this.m_lanes[k].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
				{
					this.m_vehicleTypes |= this.m_lanes[k].m_vehicleType;
					if (this.m_lanes[k].m_speedLimit == 0f)
					{
						CODebugBase<LogChannel>.Warn(LogChannel.Core, string.Concat(new object[]
						{
							base.get_gameObject().get_name(),
							" (lane ",
							k,
							"): Speed limit is zero!"
						}), base.get_gameObject());
					}
					if ((this.m_lanes[k].m_vehicleType & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None)
					{
						this.m_averageVehicleLaneSpeed += this.m_lanes[k].m_speedLimit;
						num++;
					}
				}
				if (flag3 && (byte)(this.m_lanes[k].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) != 0)
				{
					this.m_lanes[k].m_finalDirection = NetInfo.InvertDirection(this.m_lanes[k].m_direction);
				}
				else
				{
					this.m_lanes[k].m_finalDirection = this.m_lanes[k].m_direction;
				}
				if ((byte)(this.m_lanes[k].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
				{
					if ((byte)(this.m_lanes[k].m_finalDirection & NetInfo.Direction.Forward) != 0)
					{
						this.m_hasForwardVehicleLanes = true;
						this.m_forwardVehicleLaneCount++;
					}
					if ((byte)(this.m_lanes[k].m_finalDirection & NetInfo.Direction.Backward) != 0)
					{
						this.m_hasBackwardVehicleLanes = true;
						this.m_backwardVehicleLaneCount++;
					}
				}
				if (this.m_lanes[k].m_useTerrainHeight && this.m_lanes[k].m_laneProps != null)
				{
					this.m_hasTerrainHeightProps = true;
				}
				this.m_sortedLanes[k] = k;
				this.m_lanes[k].m_similarLaneIndex = -1;
			}
			if (num != 0)
			{
				this.m_averageVehicleLaneSpeed /= (float)num;
			}
			bool flag4;
			do
			{
				flag4 = true;
				for (int l = 1; l < this.m_sortedLanes.Length; l++)
				{
					if (this.m_lanes[this.m_sortedLanes[l]].m_position < this.m_lanes[this.m_sortedLanes[l - 1]].m_position)
					{
						int num2 = this.m_sortedLanes[l];
						this.m_sortedLanes[l] = this.m_sortedLanes[l - 1];
						this.m_sortedLanes[l - 1] = num2;
						flag4 = false;
					}
				}
			}
			while (!flag4);
			for (int m = 0; m < this.m_sortedLanes.Length; m++)
			{
				NetInfo.Lane lane = this.m_lanes[this.m_sortedLanes[m]];
				if (lane.m_similarLaneIndex == -1)
				{
					lane.m_similarLaneIndex = 0;
					NetInfo.LaneType laneType = lane.m_laneType;
					if ((byte)(laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
					{
						laneType = (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle);
					}
					int similarLaneCount = 1;
					for (int n = m + 1; n < this.m_sortedLanes.Length; n++)
					{
						NetInfo.Lane lane2 = this.m_lanes[this.m_sortedLanes[n]];
						if (lane2.m_direction == lane.m_direction && (byte)(lane2.m_laneType & laneType) != 0 && (lane2.m_vehicleType == lane.m_vehicleType || (lane2.m_vehicleType & lane.m_vehicleType) != VehicleInfo.VehicleType.None))
						{
							lane2.m_similarLaneIndex = similarLaneCount++;
						}
					}
					lane.m_similarLaneCount = similarLaneCount;
					for (int num3 = m + 1; num3 < this.m_sortedLanes.Length; num3++)
					{
						NetInfo.Lane lane3 = this.m_lanes[this.m_sortedLanes[num3]];
						if (lane3.m_direction == lane.m_direction && (byte)(lane3.m_laneType & laneType) != 0 && (lane3.m_vehicleType == lane.m_vehicleType || (lane3.m_vehicleType & lane.m_vehicleType) != VehicleInfo.VehicleType.None))
						{
							lane3.m_similarLaneCount = similarLaneCount;
						}
					}
				}
			}
		}
		if (this.m_netAI == null)
		{
			this.m_netAI = base.GetComponent<NetAI>();
			this.m_netAI.m_info = this;
			this.m_netAI.InitializePrefab();
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_netAI != null)
		{
			this.m_netAI.DestroyPrefab();
			this.m_netAI = null;
		}
		if (this.m_segments != null)
		{
			for (int i = 0; i < this.m_segments.Length; i++)
			{
				NetInfo.DestroySegmentInfo(this.m_segments[i]);
			}
		}
		if (this.m_nodes != null)
		{
			for (int j = 0; j < this.m_nodes.Length; j++)
			{
				NetInfo.DestroyNodeInfo(this.m_nodes[j]);
			}
		}
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		float levelOfDetailFactor = RenderManager.LevelOfDetailFactor;
		float lodRenderDistance = Mathf.Clamp(100f + levelOfDetailFactor * Mathf.Max(this.m_halfWidth * 50f, (this.m_maxHeight - this.m_minHeight) * 80f), 100f, 1000f);
		if (this.m_segments != null)
		{
			for (int i = 0; i < this.m_segments.Length; i++)
			{
				if (this.m_segments[i].m_lodMesh != null)
				{
					this.m_segments[i].m_lodRenderDistance = lodRenderDistance;
				}
				else
				{
					this.m_segments[i].m_lodRenderDistance = 100000f;
				}
			}
		}
		if (this.m_nodes != null)
		{
			for (int j = 0; j < this.m_nodes.Length; j++)
			{
				if (this.m_nodes[j].m_lodMesh != null)
				{
					this.m_nodes[j].m_lodRenderDistance = lodRenderDistance;
				}
				else
				{
					this.m_nodes[j].m_lodRenderDistance = 100000f;
				}
			}
		}
		this.m_maxPropDistance = 0f;
		if (this.m_lanes != null)
		{
			for (int k = 0; k < this.m_lanes.Length; k++)
			{
				NetLaneProps laneProps = this.m_lanes[k].m_laneProps;
				if (laneProps != null && laneProps.m_props != null)
				{
					for (int l = 0; l < laneProps.m_props.Length; l++)
					{
						if (laneProps.m_props[l].m_finalProp != null)
						{
							this.m_maxPropDistance = Mathf.Max(this.m_maxPropDistance, laneProps.m_props[l].m_finalProp.m_maxRenderDistance);
						}
					}
				}
			}
		}
	}

	public override void CheckReferences()
	{
		this.m_maxPropDistance = 0f;
		this.m_propLayers = 0;
		if (this.m_lanes != null)
		{
			for (int i = 0; i < this.m_lanes.Length; i++)
			{
				NetLaneProps laneProps = this.m_lanes[i].m_laneProps;
				if (laneProps != null && laneProps.m_props != null)
				{
					for (int j = 0; j < laneProps.m_props.Length; j++)
					{
						if (laneProps.m_props[j].m_prop != null)
						{
							if (laneProps.m_props[j].m_prop.m_prefabInitialized)
							{
								laneProps.m_props[j].m_finalProp = laneProps.m_props[j].m_prop;
							}
							else
							{
								laneProps.m_props[j].m_finalProp = PrefabCollection<PropInfo>.FindLoaded(laneProps.m_props[j].m_prop.get_gameObject().get_name());
							}
							if (laneProps.m_props[j].m_finalProp == null)
							{
								throw new PrefabException(this, "Referenced prop is not loaded (" + laneProps.m_props[j].m_prop.get_gameObject().get_name() + ")");
							}
							this.CheckProp(laneProps.m_props[j].m_finalProp, null);
							this.m_maxPropDistance = Mathf.Max(this.m_maxPropDistance, laneProps.m_props[j].m_finalProp.m_maxRenderDistance);
						}
						else if (laneProps.m_props[j].m_tree != null)
						{
							if (laneProps.m_props[j].m_tree.m_prefabInitialized)
							{
								laneProps.m_props[j].m_finalTree = laneProps.m_props[j].m_tree;
							}
							else
							{
								laneProps.m_props[j].m_finalTree = PrefabCollection<TreeInfo>.FindLoaded(laneProps.m_props[j].m_tree.get_gameObject().get_name());
							}
							if (laneProps.m_props[j].m_finalTree == null)
							{
								throw new PrefabException(this, "Referenced tree is not loaded (" + laneProps.m_props[j].m_tree.get_gameObject().get_name() + ")");
							}
							this.m_propLayers |= 1 << laneProps.m_props[j].m_finalTree.m_prefabDataLayer;
						}
					}
				}
			}
		}
	}

	private void CheckProp(PropInfo info, HashSet<PropInfo> traversed = null)
	{
		if (traversed == null)
		{
			traversed = new HashSet<PropInfo>();
		}
		if (traversed.Contains(info))
		{
			return;
		}
		traversed.Add(info);
		this.m_propLayers |= 1 << info.m_prefabDataLayer;
		if (info.m_requireHeightMap && !info.m_requireWaterMap)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
			{
				"Net prop cannot require heightmap (",
				base.get_gameObject().get_name(),
				"->",
				info.get_gameObject().get_name(),
				")"
			}), info.get_gameObject());
		}
		if (info.m_variations != null)
		{
			for (int i = 0; i < info.m_variations.Length; i++)
			{
				if (info.m_variations[i].m_finalProp != null)
				{
					this.CheckProp(info.m_variations[i].m_finalProp, traversed);
				}
			}
		}
	}

	private void OnDrawGizmosSelected()
	{
		if (this.m_instanceID.IsEmpty)
		{
			Gizmos.set_matrix(base.get_transform().get_localToWorldMatrix());
			int i = 0;
			while (i < this.m_lanes.Length)
			{
				NetInfo.LaneType laneType = this.m_lanes[i].m_laneType;
				switch (laneType)
				{
				case NetInfo.LaneType.Vehicle:
					goto IL_C0;
				case NetInfo.LaneType.Pedestrian:
					Gizmos.set_color(new Color(0f, 1f, 0f, 0.5f));
					goto IL_129;
				case NetInfo.LaneType.Vehicle | NetInfo.LaneType.Pedestrian:
				case NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking:
				case NetInfo.LaneType.Pedestrian | NetInfo.LaneType.Parking:
				case NetInfo.LaneType.Vehicle | NetInfo.LaneType.Pedestrian | NetInfo.LaneType.Parking:
					IL_5D:
					if (laneType == NetInfo.LaneType.CargoVehicle || laneType == NetInfo.LaneType.TransportVehicle)
					{
						goto IL_C0;
					}
					if (laneType != NetInfo.LaneType.EvacuationTransport)
					{
						Gizmos.set_color(new Color(1f, 1f, 1f, 0.75f));
						goto IL_129;
					}
					goto IL_E3;
				case NetInfo.LaneType.Parking:
					Gizmos.set_color(new Color(0f, 0f, 1f, 0.5f));
					goto IL_129;
				case NetInfo.LaneType.PublicTransport:
					goto IL_E3;
				}
				goto IL_5D;
				IL_129:
				Gizmos.DrawCube(new Vector3(this.m_lanes[i].m_position, this.m_lanes[i].m_verticalOffset - 0.05f, 0f), new Vector3(Mathf.Max(0.1f, this.m_lanes[i].m_width), 0.1f, this.m_segmentLength));
				i++;
				continue;
				IL_C0:
				Gizmos.set_color(new Color(1f, 0f, 0f, 0.5f));
				goto IL_129;
				IL_E3:
				Gizmos.set_color(new Color(1f, 1f, 0f, 0.5f));
				goto IL_129;
			}
		}
	}

	protected override void LateUpdate()
	{
		if (this.m_instanceID.IsEmpty)
		{
			Matrix4x4 localToWorldMatrix = base.get_transform().get_localToWorldMatrix();
			float vScale = 0.05f;
			Vector3 vector;
			vector..ctor(-this.m_halfWidth, 0f, -this.m_segmentLength * 0.5f);
			Vector3 vector2;
			vector2..ctor(this.m_halfWidth, 0f, -this.m_segmentLength * 0.5f);
			Vector3 vector3;
			vector3..ctor(-this.m_halfWidth, 0f, this.m_segmentLength * 0.5f);
			Vector3 vector4;
			vector4..ctor(this.m_halfWidth, 0f, this.m_segmentLength * 0.5f);
			Vector3 startDir;
			startDir..ctor(0f, 0f, 1f);
			Vector3 startDir2;
			startDir2..ctor(0f, 0f, 1f);
			Vector3 endDir;
			endDir..ctor(0f, 0f, -1f);
			Vector3 endDir2;
			endDir2..ctor(0f, 0f, -1f);
			bool smoothStart = false;
			bool smoothEnd = false;
			Vector3 vector5;
			Vector3 vector6;
			NetSegment.CalculateMiddlePoints(vector, startDir, vector3, endDir, smoothStart, smoothEnd, out vector5, out vector6);
			Vector3 vector7;
			Vector3 vector8;
			NetSegment.CalculateMiddlePoints(vector2, startDir2, vector4, endDir2, smoothStart, smoothEnd, out vector7, out vector8);
			Matrix4x4 matrix4x = NetSegment.CalculateControlMatrix(vector, vector5, vector6, vector3, vector2, vector7, vector8, vector4, Vector3.get_zero(), vScale);
			Matrix4x4 matrix4x2 = NetSegment.CalculateControlMatrix(vector2, vector7, vector8, vector4, vector, vector5, vector6, vector3, Vector3.get_zero(), vScale);
			Vector4 vector9;
			vector9..ctor(0.5f / this.m_halfWidth, 1f / this.m_segmentLength, 1f, 1f);
			if (NetInfo.m_materialPropertyBlock == null)
			{
				NetInfo.m_materialPropertyBlock = new MaterialPropertyBlock();
			}
			NetInfo.m_materialPropertyBlock.Clear();
			NetInfo.m_materialPropertyBlock.SetMatrix("_LeftMatrix", matrix4x);
			NetInfo.m_materialPropertyBlock.SetMatrix("_RightMatrix", matrix4x2);
			NetInfo.m_materialPropertyBlock.SetVector("_MeshScale", vector9);
			if (this.m_segments != null)
			{
				for (int i = 0; i < this.m_segments.Length; i++)
				{
					NetInfo.Segment segment = this.m_segments[i];
					bool flag;
					if (segment.CheckFlags(NetSegment.Flags.None, out flag))
					{
						Graphics.DrawMesh(segment.m_mesh, localToWorldMatrix, segment.m_material, base.get_gameObject().get_layer(), null, 0, NetInfo.m_materialPropertyBlock);
					}
				}
			}
			if (this.m_lanes != null)
			{
				for (int j = 0; j < this.m_lanes.Length; j++)
				{
					NetLaneProps laneProps = this.m_lanes[j].m_laneProps;
					if (laneProps != null && laneProps.m_props != null)
					{
						int num = laneProps.m_props.Length;
						for (int k = 0; k < num; k++)
						{
							NetLaneProps.Prop prop = laneProps.m_props[k];
							int num2 = 2;
							if (prop.m_repeatDistance > 1f)
							{
								num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_segmentLength / prop.m_repeatDistance));
							}
							float num3 = prop.m_segmentOffset * 0.5f;
							if (this.m_segmentLength != 0f)
							{
								num3 = Mathf.Clamp(num3 + prop.m_position.z / this.m_segmentLength, -0.5f, 0.5f);
							}
							if ((byte)(this.m_lanes[j].m_direction & NetInfo.Direction.Both) == 2)
							{
								num3 = -num3;
							}
							if (prop.m_prop != null)
							{
								if (prop.m_prop.m_mesh == null)
								{
									prop.m_prop.m_mesh = prop.m_prop.GetComponent<MeshFilter>().get_sharedMesh();
								}
								if (prop.m_prop.m_material == null)
								{
									prop.m_prop.m_material = prop.m_prop.GetComponent<Renderer>().get_sharedMaterial();
									prop.m_prop.m_isDecal = (prop.m_prop.m_material.GetTag("RenderType", false) == "Decal");
								}
								Material material = prop.m_prop.m_material;
								Vector3 one = Vector3.get_one();
								if (prop.m_prop.m_isDecal)
								{
									if (NetInfo.m_propDecalSceneMaterial == null)
									{
										NetInfo.m_propDecalSceneMaterial = new Material(Shader.Find("Custom/Props/Decal/Scene"));
									}
									NetInfo.m_propDecalSceneMaterial.CopyPropertiesFromMaterial(material);
									material = NetInfo.m_propDecalSceneMaterial;
									one..ctor(1f, 0.01f, 1f);
								}
								for (int l = 1; l <= num2; l += 2)
								{
									float num4 = num3 + (float)l / (float)num2 - 0.5f;
									Vector3 vector10;
									vector10..ctor(this.m_lanes[j].m_position, this.m_lanes[j].m_verticalOffset, this.m_segmentLength * num4);
									if ((byte)(this.m_lanes[j].m_direction & NetInfo.Direction.Both) == 2)
									{
										vector10.x -= prop.m_position.x;
									}
									else
									{
										vector10.x += prop.m_position.x;
									}
									vector10.y += prop.m_position.y;
									Quaternion quaternion = Quaternion.AngleAxis((((byte)(this.m_lanes[j].m_direction & NetInfo.Direction.Both) != 2) ? 180f : 0f) + prop.m_angle, Vector3.get_down());
									Matrix4x4 matrix4x3 = default(Matrix4x4);
									matrix4x3.SetTRS(vector10, quaternion, one);
									matrix4x3 = localToWorldMatrix * matrix4x3;
									Graphics.DrawMesh(prop.m_prop.m_mesh, matrix4x3, material, prop.m_prop.get_gameObject().get_layer());
								}
							}
							if (prop.m_tree != null)
							{
								if (prop.m_tree.m_mesh == null)
								{
									prop.m_tree.m_mesh = prop.m_tree.GetComponent<MeshFilter>().get_sharedMesh();
								}
								if (prop.m_tree.m_material == null)
								{
									prop.m_tree.m_material = prop.m_tree.GetComponent<Renderer>().get_sharedMaterial();
								}
								for (int m = 1; m <= num2; m += 2)
								{
									float num5 = num3 + (float)m / (float)num2 - 0.5f;
									Vector3 vector11;
									vector11..ctor(this.m_lanes[j].m_position, this.m_lanes[j].m_verticalOffset, this.m_segmentLength * num5);
									if ((byte)(this.m_lanes[j].m_direction & NetInfo.Direction.Both) == 2)
									{
										vector11.x -= prop.m_position.x;
									}
									else
									{
										vector11.x += prop.m_position.x;
									}
									vector11.y += prop.m_position.y;
									Matrix4x4 matrix4x4 = default(Matrix4x4);
									matrix4x4.SetTRS(vector11, Quaternion.get_identity(), Vector3.get_one());
									matrix4x4 = localToWorldMatrix * matrix4x4;
									Graphics.DrawMesh(prop.m_tree.m_mesh, matrix4x4, prop.m_tree.m_material, prop.m_tree.get_gameObject().get_layer());
								}
							}
						}
					}
				}
			}
		}
		else
		{
			base.LateUpdate();
		}
	}

	private bool InitSegmentInfo(NetInfo.Segment info, ref bool requireSurfaceMaps)
	{
		float num = this.m_minHeight - this.m_maxSlope * 64f - 10f;
		float num2 = this.m_maxHeight + this.m_maxSlope * 64f + 10f;
		info.m_mesh.set_bounds(new Bounds(new Vector3(0f, (num + num2) * 0.5f, 0f), new Vector3(128f, num2 - num, 128f)));
		info.m_segmentMesh = info.m_mesh;
		info.m_segmentMaterial = new Material(info.m_material);
		string tag = info.m_material.GetTag("NetType", false);
		if (tag == "TerrainSurface")
		{
			requireSurfaceMaps = true;
			info.m_requireSurfaceMaps = true;
		}
		else
		{
			info.m_requireSurfaceMaps = false;
		}
		if (tag == "PowerLine")
		{
			info.m_requireWindSpeed = true;
			info.m_preserveUVs = true;
			info.m_generateTangents = false;
			info.m_layer = LayerMask.NameToLayer("PowerLines");
		}
		else if (tag == "MetroTunnel")
		{
			info.m_requireWindSpeed = false;
			info.m_preserveUVs = false;
			info.m_generateTangents = false;
			info.m_layer = LayerMask.NameToLayer("MetroTunnels");
		}
		else
		{
			info.m_requireWindSpeed = false;
			info.m_preserveUVs = false;
			info.m_generateTangents = false;
			info.m_layer = this.m_prefabDataLayer;
		}
		info.m_segmentMaterial.EnableKeyword("NET_SEGMENT");
		Color color = info.m_material.get_color();
		color.a = 0f;
		info.m_segmentMaterial.set_color(color);
		Texture2D texture2D = info.m_material.get_mainTexture() as Texture2D;
		if (texture2D != null && texture2D.get_format() == 12)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "Segment diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		return info.m_lodMesh != null;
	}

	private static void DestroySegmentInfo(NetInfo.Segment info)
	{
		if (info.m_segmentMaterial != null)
		{
			Object.Destroy(info.m_segmentMaterial);
			info.m_segmentMaterial = null;
		}
		NetInfo.ReleaseLodValue(ref info.m_combinedLod);
	}

	private bool InitNodeInfo(NetInfo.Node info, ref bool requireSurfaceMaps, ref bool requireSegmentRenderers, ref bool requireDirectRenderers)
	{
		float num = this.m_minHeight - 10f;
		float num2 = this.m_maxHeight + 10f;
		info.m_mesh.set_bounds(new Bounds(new Vector3(0f, (num + num2) * 0.5f, 0f), new Vector3(128f, num2 - num, 128f)));
		info.m_nodeMesh = info.m_mesh;
		info.m_nodeMaterial = new Material(info.m_material);
		string tag = info.m_material.GetTag("NetType", false);
		if (tag == "TerrainSurface")
		{
			requireSurfaceMaps = true;
			info.m_requireSurfaceMaps = true;
		}
		else
		{
			info.m_requireSurfaceMaps = false;
		}
		if (tag == "PowerLine")
		{
			info.m_requireWindSpeed = true;
			info.m_preserveUVs = true;
			info.m_generateTangents = false;
			info.m_layer = LayerMask.NameToLayer("PowerLines");
		}
		else if (tag == "MetroTunnel")
		{
			info.m_requireWindSpeed = false;
			info.m_preserveUVs = false;
			info.m_generateTangents = false;
			info.m_layer = LayerMask.NameToLayer("MetroTunnels");
		}
		else
		{
			info.m_requireWindSpeed = false;
			info.m_preserveUVs = false;
			info.m_generateTangents = false;
			info.m_layer = this.m_prefabDataLayer;
		}
		Color color = info.m_material.get_color();
		color.a = 0f;
		info.m_nodeMaterial.set_color(color);
		if (info.m_directConnect)
		{
			requireDirectRenderers = true;
		}
		else
		{
			info.m_nodeMaterial.EnableKeyword("NET_NODE");
			requireSegmentRenderers = true;
		}
		Texture2D texture2D = info.m_material.get_mainTexture() as Texture2D;
		if (texture2D != null && texture2D.get_format() == 12)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "Node diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		return info.m_lodMesh != null;
	}

	private static void DestroyNodeInfo(NetInfo.Node info)
	{
		if (info.m_nodeMaterial != null)
		{
			Object.Destroy(info.m_nodeMaterial);
			info.m_nodeMaterial = null;
		}
		NetInfo.ReleaseLodValue(ref info.m_combinedLod);
	}

	public void InitMeshData(NetInfo.Segment info, Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aprAtlas)
	{
		if (info.m_lodMesh == null || info.m_lodMaterial == null)
		{
			return;
		}
		NetInfo.ReleaseLodValue(ref info.m_combinedLod);
		if (NetInfo.m_lodMeshes == null)
		{
			NetInfo.m_lodMeshes = new Dictionary<NetInfo.MeshKey, NetInfo.MeshValue>();
		}
		if (NetInfo.m_lodItems == null)
		{
			NetInfo.m_lodItems = new Dictionary<NetInfo.LodKey, NetInfo.LodValue>();
		}
		NetInfo.MeshKey key;
		key.m_srcMesh = info.m_lodMesh;
		key.m_atlasRect = atlasRect;
		key.m_preserveUVs = info.m_preserveUVs;
		key.m_generateTangents = info.m_generateTangents;
		NetInfo.MeshValue meshValue;
		if (!NetInfo.m_lodMeshes.TryGetValue(key, out meshValue))
		{
			meshValue = new NetInfo.MeshValue();
			meshValue.m_key = key;
			meshValue.m_mesh1 = new Mesh();
			meshValue.m_mesh4 = new Mesh();
			meshValue.m_mesh8 = new Mesh();
			meshValue.m_data = this.GenerateCombinedLodMesh(info.m_lodMesh, meshValue.m_mesh1, meshValue.m_mesh4, meshValue.m_mesh8, atlasRect, info.m_preserveUVs, info.m_generateTangents);
			NetInfo.m_lodMeshes.Add(key, meshValue);
		}
		NetInfo.LodKey key2;
		key2.m_mesh = meshValue;
		key2.m_srcMaterial = info.m_lodMaterial;
		key2.m_layer = info.m_layer;
		key2.m_node = false;
		NetInfo.LodValue lodValue;
		if (NetInfo.m_lodItems.TryGetValue(key2, out lodValue))
		{
			lodValue.m_refCount++;
		}
		else
		{
			meshValue.m_refCount++;
			lodValue = new NetInfo.LodValue();
			lodValue.m_key = key2;
			lodValue.m_material = new Material(info.m_lodMaterial);
			lodValue.m_material.EnableKeyword("NET_SEGMENT");
			lodValue.m_lodCount = 0;
			lodValue.m_refCount = 1;
			lodValue.m_leftMatrices = new Matrix4x4[8];
			lodValue.m_rightMatrices = new Matrix4x4[8];
			lodValue.m_meshScales = new Vector4[8];
			lodValue.m_objectIndices = new Vector4[8];
			lodValue.m_meshLocations = new Vector4[8];
			lodValue.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			lodValue.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			lodValue.m_material.EnableKeyword("MULTI_INSTANCE");
			Color color = info.m_lodMaterial.get_color();
			color.a = 0f;
			lodValue.m_material.set_color(color);
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_MainTex))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_MainTex, rgbAtlas);
			}
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_XYSMap))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_XYSMap, xysAtlas);
			}
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_APRMap))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_APRMap, aprAtlas);
			}
			NetInfo.m_lodItems.Add(key2, lodValue);
		}
		info.m_combinedLod = lodValue;
	}

	public void InitMeshData(NetInfo.Node info, Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aprAtlas)
	{
		if (info.m_lodMesh == null || info.m_lodMaterial == null)
		{
			return;
		}
		NetInfo.ReleaseLodValue(ref info.m_combinedLod);
		if (NetInfo.m_lodMeshes == null)
		{
			NetInfo.m_lodMeshes = new Dictionary<NetInfo.MeshKey, NetInfo.MeshValue>();
		}
		if (NetInfo.m_lodItems == null)
		{
			NetInfo.m_lodItems = new Dictionary<NetInfo.LodKey, NetInfo.LodValue>();
		}
		NetInfo.MeshKey key;
		key.m_srcMesh = info.m_lodMesh;
		key.m_atlasRect = atlasRect;
		key.m_preserveUVs = info.m_preserveUVs;
		key.m_generateTangents = info.m_generateTangents;
		NetInfo.MeshValue meshValue;
		if (!NetInfo.m_lodMeshes.TryGetValue(key, out meshValue))
		{
			meshValue = new NetInfo.MeshValue();
			meshValue.m_key = key;
			meshValue.m_mesh1 = new Mesh();
			meshValue.m_mesh4 = new Mesh();
			meshValue.m_mesh8 = new Mesh();
			meshValue.m_data = this.GenerateCombinedLodMesh(info.m_lodMesh, meshValue.m_mesh1, meshValue.m_mesh4, meshValue.m_mesh8, atlasRect, info.m_preserveUVs, info.m_generateTangents);
			NetInfo.m_lodMeshes.Add(key, meshValue);
		}
		NetInfo.LodKey key2;
		key2.m_mesh = meshValue;
		key2.m_srcMaterial = info.m_lodMaterial;
		key2.m_layer = info.m_layer;
		key2.m_node = !info.m_directConnect;
		NetInfo.LodValue lodValue;
		if (NetInfo.m_lodItems.TryGetValue(key2, out lodValue))
		{
			lodValue.m_refCount++;
		}
		else
		{
			meshValue.m_refCount++;
			lodValue = new NetInfo.LodValue();
			lodValue.m_key = key2;
			lodValue.m_material = new Material(info.m_lodMaterial);
			if (info.m_directConnect)
			{
				lodValue.m_material.EnableKeyword("NET_SEGMENT");
			}
			else
			{
				lodValue.m_material.EnableKeyword("NET_NODE");
				lodValue.m_leftMatricesB = new Matrix4x4[8];
				lodValue.m_rightMatricesB = new Matrix4x4[8];
				lodValue.m_centerPositions = new Vector4[8];
				lodValue.m_sideScales = new Vector4[8];
			}
			lodValue.m_lodCount = 0;
			lodValue.m_refCount = 1;
			lodValue.m_leftMatrices = new Matrix4x4[8];
			lodValue.m_rightMatrices = new Matrix4x4[8];
			lodValue.m_meshScales = new Vector4[8];
			lodValue.m_objectIndices = new Vector4[8];
			lodValue.m_meshLocations = new Vector4[8];
			lodValue.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			lodValue.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			lodValue.m_material.EnableKeyword("MULTI_INSTANCE");
			Color color = info.m_lodMaterial.get_color();
			color.a = 0f;
			lodValue.m_material.set_color(color);
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_MainTex))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_MainTex, rgbAtlas);
			}
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_XYSMap))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_XYSMap, xysAtlas);
			}
			if (lodValue.m_material.HasProperty(Singleton<NetManager>.get_instance().ID_APRMap))
			{
				lodValue.m_material.SetTexture(Singleton<NetManager>.get_instance().ID_APRMap, aprAtlas);
			}
			NetInfo.m_lodItems.Add(key2, lodValue);
		}
		info.m_combinedLod = lodValue;
	}

	private static void ReleaseLodValue(ref NetInfo.LodValue lodValue)
	{
		if (lodValue != null)
		{
			if (--lodValue.m_refCount == 0)
			{
				NetInfo.ReleaseMeshValue(ref lodValue.m_key.m_mesh);
				if (lodValue.m_material != null)
				{
					Object.Destroy(lodValue.m_material);
					lodValue.m_material = null;
				}
				lodValue.m_surfaceTexA = null;
				lodValue.m_surfaceTexB = null;
				lodValue.m_leftMatrices = null;
				lodValue.m_leftMatricesB = null;
				lodValue.m_rightMatrices = null;
				lodValue.m_rightMatricesB = null;
				lodValue.m_meshScales = null;
				lodValue.m_centerPositions = null;
				lodValue.m_sideScales = null;
				lodValue.m_meshLocations = null;
				lodValue.m_objectIndices = null;
				NetInfo.m_lodItems.Remove(lodValue.m_key);
			}
			lodValue = null;
		}
	}

	private static void ReleaseMeshValue(ref NetInfo.MeshValue meshValue)
	{
		if (meshValue != null)
		{
			if (--meshValue.m_refCount == 0)
			{
				if (meshValue.m_mesh1 != null)
				{
					Object.Destroy(meshValue.m_mesh1);
					meshValue.m_mesh1 = null;
				}
				if (meshValue.m_mesh4 != null)
				{
					Object.Destroy(meshValue.m_mesh4);
					meshValue.m_mesh4 = null;
				}
				if (meshValue.m_mesh8 != null)
				{
					Object.Destroy(meshValue.m_mesh8);
					meshValue.m_mesh8 = null;
				}
				NetInfo.m_lodMeshes.Remove(meshValue.m_key);
			}
			meshValue = null;
		}
	}

	public static void ClearLodValues()
	{
		int num = PrefabCollection<NetInfo>.LoadedCount();
		for (int i = 0; i < num; i++)
		{
			NetInfo loaded = PrefabCollection<NetInfo>.GetLoaded((uint)i);
			if (loaded != null)
			{
				if (loaded.m_segments != null)
				{
					for (int j = 0; j < loaded.m_segments.Length; j++)
					{
						NetInfo.ReleaseLodValue(ref loaded.m_segments[j].m_combinedLod);
					}
				}
				if (loaded.m_nodes != null)
				{
					for (int k = 0; k < loaded.m_nodes.Length; k++)
					{
						NetInfo.ReleaseLodValue(ref loaded.m_nodes[k].m_combinedLod);
					}
				}
			}
		}
	}

	private RenderGroup.MeshData GenerateCombinedLodMesh(Mesh srcMesh, Mesh destMesh1, Mesh destMesh4, Mesh destMesh8, Rect atlasRect, bool preserveUVs, bool generateTangents)
	{
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(srcMesh);
		int num = meshData.m_vertices.Length;
		int num2 = meshData.m_triangles.Length;
		if (num * 8 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		if (meshData.m_colors.Length != num)
		{
			meshData.m_colors = new Color32[num];
			for (int i = 0; i < meshData.m_colors.Length; i++)
			{
				meshData.m_colors[i] = new Color32(255, 255, 255, 255);
			}
		}
		if (!preserveUVs)
		{
			meshData.m_uvs3 = new Vector2[num];
			atlasRect.set_x(atlasRect.get_x() + atlasRect.get_width() * 0.125f);
			atlasRect.set_y(atlasRect.get_y() + atlasRect.get_height() * 0.125f);
			atlasRect.set_width(atlasRect.get_width() * 0.75f);
			atlasRect.set_height(atlasRect.get_height() * 0.75f);
			for (int j = 0; j < num; j++)
			{
				Vector2 vector = meshData.m_uvs[j];
				vector.x = atlasRect.get_x() + atlasRect.get_width() * vector.x;
				meshData.m_uvs[j] = vector;
				meshData.m_uvs3[j] = new Vector2(atlasRect.get_y(), atlasRect.get_height());
			}
		}
		if (generateTangents)
		{
			for (int k = 0; k < num; k++)
			{
				meshData.m_tangents[k] = new Vector4(0f, 0f, 1f, 1f);
			}
		}
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3, num, num2);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3, num * 4, num2 * 4);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3, num * 8, num2 * 8);
		int num3 = 0;
		int num4 = 0;
		for (int l = 0; l < 8; l++)
		{
			byte a = (byte)(l * 32);
			for (int m = 0; m < num2; m++)
			{
				if (l < 1)
				{
					meshData2.m_triangles[num4] = meshData.m_triangles[m] + num3;
				}
				if (l < 4)
				{
					meshData3.m_triangles[num4] = meshData.m_triangles[m] + num3;
				}
				meshData4.m_triangles[num4] = meshData.m_triangles[m] + num3;
				num4++;
			}
			for (int n = 0; n < num; n++)
			{
				Color32 color = meshData.m_colors[n];
				color.a = a;
				if (l < 1)
				{
					meshData2.m_vertices[num3] = meshData.m_vertices[n];
					meshData2.m_normals[num3] = meshData.m_normals[n];
					meshData2.m_tangents[num3] = meshData.m_tangents[n];
					meshData2.m_uvs[num3] = meshData.m_uvs[n];
					if (!preserveUVs)
					{
						meshData2.m_uvs3[num3] = meshData.m_uvs3[n];
					}
					meshData2.m_colors[num3] = color;
				}
				if (l < 4)
				{
					meshData3.m_vertices[num3] = meshData.m_vertices[n];
					meshData3.m_normals[num3] = meshData.m_normals[n];
					meshData3.m_tangents[num3] = meshData.m_tangents[n];
					meshData3.m_uvs[num3] = meshData.m_uvs[n];
					if (!preserveUVs)
					{
						meshData3.m_uvs3[num3] = meshData.m_uvs3[n];
					}
					meshData3.m_colors[num3] = color;
				}
				meshData4.m_vertices[num3] = meshData.m_vertices[n];
				meshData4.m_normals[num3] = meshData.m_normals[n];
				meshData4.m_tangents[num3] = meshData.m_tangents[n];
				meshData4.m_uvs[num3] = meshData.m_uvs[n];
				if (!preserveUVs)
				{
					meshData4.m_uvs3[num3] = meshData.m_uvs3[n];
				}
				meshData4.m_colors[num3] = color;
				num3++;
			}
		}
		meshData2.PopulateMesh(destMesh1);
		meshData3.PopulateMesh(destMesh4);
		meshData4.PopulateMesh(destMesh8);
		return meshData;
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("NET_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("NET_TITLE", base.get_gameObject().get_name());
	}

	public override string GetUncheckedLocalizedTitle()
	{
		if (Locale.Exists("NET_TITLE", base.get_gameObject().get_name()))
		{
			return Locale.Get("NET_TITLE", base.get_gameObject().get_name());
		}
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("NET_DESC", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescriptionShort()
	{
		return Locale.Get("NET_DESC", base.get_gameObject().get_name());
	}

	public override PrefabAI GetAI()
	{
		return this.m_netAI;
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public override MilestoneInfo GetUnlockMilestone()
	{
		return this.m_UnlockMilestone;
	}

	public override int GetConstructionCost()
	{
		return this.m_netAI.GetConstructionCost();
	}

	public override int GetMaintenanceCost()
	{
		return this.m_netAI.GetMaintenanceCost();
	}

	public override void GetDecorationArea(out int width, out int length, out float offset)
	{
		width = 64;
		length = 64;
		offset = 0f;
	}

	public CursorInfo m_placementCursor;

	public CursorInfo m_upgradeCursor;

	public ItemClass m_class;

	[CustomizableProperty("Unlock Milestone", "Properties")]
	public MilestoneInfo m_UnlockMilestone;

	public ItemClass.Placement m_placementStyle;

	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	public ItemClass m_connectionClass;

	public ItemClass m_intersectClass;

	[CustomizableProperty("Half Width", "Properties")]
	public float m_halfWidth = 8f;

	[CustomizableProperty("Pavement Width", "Properties")]
	public float m_pavementWidth = 3f;

	[CustomizableProperty("Segment Length", "Properties")]
	public float m_segmentLength = 64f;

	[CustomizableProperty("Min Height", "Properties")]
	public float m_minHeight;

	[CustomizableProperty("Max Height", "Properties")]
	public float m_maxHeight = 5f;

	[CustomizableProperty("Max Slope", "Properties")]
	public float m_maxSlope = 0.25f;

	[CustomizableProperty("Max Build Angle", "Properties")]
	public float m_maxBuildAngle = 180f;

	[CustomizableProperty("Max Turn Angle", "Properties")]
	public float m_maxTurnAngle = 180f;

	[CustomizableProperty("Min Corner Offset", "Properties")]
	public float m_minCornerOffset;

	[CustomizableProperty("Build Height", "Properties")]
	public float m_buildHeight;

	[CustomizableProperty("Surface Level", "Properties")]
	public float m_surfaceLevel;

	public float m_terrainStartOffset;

	public float m_terrainEndOffset;

	[CustomizableProperty("Create Pavement", "Properties")]
	public bool m_createPavement;

	[CustomizableProperty("Create Gravel", "Properties")]
	public bool m_createGravel;

	[CustomizableProperty("Create Ruining", "Properties")]
	public bool m_createRuining;

	[CustomizableProperty("Flatten Terrain", "Properties")]
	public bool m_flattenTerrain;

	[CustomizableProperty("Lower Terrain", "Properties")]
	public bool m_lowerTerrain;

	[CustomizableProperty("Clip Terrain", "Properties")]
	public bool m_clipTerrain;

	[CustomizableProperty("Follow Terrain", "Properties")]
	public bool m_followTerrain;

	[CustomizableProperty("Flat Junctions", "Properties")]
	public bool m_flatJunctions;

	[CustomizableProperty("Clip Segment Ends", "Properties")]
	public bool m_clipSegmentEnds;

	[CustomizableProperty("Twist Segment Ends", "Properties")]
	public bool m_twistSegmentEnds;

	[CustomizableProperty("Straight Segment Ends", "Properties")]
	public bool m_straightSegmentEnds;

	[CustomizableProperty("Enable Bending Segments", "Properties")]
	public bool m_enableBendingSegments;

	[CustomizableProperty("Enable Bending Nodes", "Properties")]
	public bool m_enableBendingNodes;

	public bool m_enableMiddleNodes;

	public bool m_requireContinuous;

	[CustomizableProperty("Can Cross Lanes", "Properties")]
	public bool m_canCrossLanes = true;

	[CustomizableProperty("Can Collide", "Properties")]
	public bool m_canCollide = true;

	[CustomizableProperty("Block Water", "Properties")]
	public bool m_blockWater;

	[CustomizableProperty("Auto Remove", "Properties")]
	public bool m_autoRemove;

	public bool m_overlayVisible = true;

	public bool m_useFixedHeight;

	public bool m_snapBuildingNodes;

	public bool m_canDisable;

	public Vehicle.Flags m_setVehicleFlags;

	public CitizenInstance.Flags m_setCitizenFlags;

	[BitMask, CustomizableProperty("Connect Group", "Properties")]
	public NetInfo.ConnectGroup m_connectGroup;

	[CustomizableProperty("Lanes")]
	public NetInfo.Lane[] m_lanes;

	[CustomizableProperty("Segments")]
	public NetInfo.Segment[] m_segments;

	[CustomizableProperty("Nodes")]
	public NetInfo.Node[] m_nodes;

	[NonSerialized]
	public int[] m_sortedLanes;

	[NonSerialized]
	public bool m_requireSurfaceMaps;

	[NonSerialized]
	public bool m_requireSegmentRenderers;

	[NonSerialized]
	public bool m_requireDirectRenderers;

	[NonSerialized]
	public float m_maxBuildAngleCos;

	[NonSerialized]
	public float m_maxTurnAngleCos;

	[NonSerialized]
	public NetAI m_netAI;

	[NonSerialized]
	public bool m_hasForwardVehicleLanes;

	[NonSerialized]
	public bool m_hasBackwardVehicleLanes;

	[NonSerialized]
	public int m_forwardVehicleLaneCount;

	[NonSerialized]
	public int m_backwardVehicleLaneCount;

	[NonSerialized]
	public float m_averageVehicleLaneSpeed;

	[NonSerialized]
	public NetTypeGuide m_notUsedGuide;

	[NonSerialized]
	public bool m_hasParkingSpaces;

	[NonSerialized]
	public bool m_hasPedestrianLanes;

	[NonSerialized]
	public bool m_hasTerrainHeightProps;

	[NonSerialized]
	public int m_propLayers;

	[NonSerialized]
	public int m_netLayers;

	[NonSerialized]
	public float m_maxPropDistance;

	[NonSerialized]
	public Color m_color;

	[NonSerialized]
	public NetInfo.ConnectGroup m_nodeConnectGroups;

	[NonSerialized]
	public NetInfo.LaneType m_laneTypes;

	[NonSerialized]
	public VehicleInfo.VehicleType m_vehicleTypes;

	[NonSerialized]
	private static MaterialPropertyBlock m_materialPropertyBlock;

	[NonSerialized]
	private static Material m_propDecalSceneMaterial;

	[NonSerialized]
	private static Dictionary<NetInfo.MeshKey, NetInfo.MeshValue> m_lodMeshes;

	[NonSerialized]
	private static Dictionary<NetInfo.LodKey, NetInfo.LodValue> m_lodItems;

	[Flags]
	public enum LaneType : byte
	{
		None = 0,
		Vehicle = 1,
		Pedestrian = 2,
		Parking = 4,
		PublicTransport = 8,
		CargoVehicle = 16,
		TransportVehicle = 32,
		EvacuationTransport = 64,
		All = 255
	}

	[Flags]
	public enum Direction : byte
	{
		None = 0,
		Forward = 1,
		Backward = 2,
		Both = 3,
		Avoid = 12,
		AvoidBackward = 7,
		AvoidForward = 11,
		AvoidBoth = 15
	}

	[Flags]
	public enum ConnectGroup
	{
		None = 0,
		NarrowTram = 1,
		WideTram = 2,
		SingleTram = 4,
		CenterTram = 8,
		DoubleTrain = 16,
		SingleTrain = 32,
		TrainStation = 64,
		DoubleMonorail = 256,
		SingleMonorail = 512,
		MonorailStation = 1024,
		AllGroups = 4095,
		OnewayStart = 4096,
		OnewayEnd = 8192,
		Oneway = 12288
	}

	[Serializable]
	public class Lane
	{
		public bool CheckType(NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes)
		{
			if (laneTypes == NetInfo.LaneType.None || (byte)(laneTypes & this.m_laneType) != 0)
			{
				if ((byte)(this.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) == 0)
				{
					return true;
				}
				if (vehicleTypes == VehicleInfo.VehicleType.None || (vehicleTypes & this.m_vehicleType) != VehicleInfo.VehicleType.None)
				{
					return true;
				}
			}
			return false;
		}

		public bool CheckType(NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, VehicleInfo.VehicleType stopTypes)
		{
			if (laneTypes == NetInfo.LaneType.None || (byte)(laneTypes & this.m_laneType) != 0)
			{
				if ((byte)(this.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) != 0)
				{
					if (vehicleTypes == VehicleInfo.VehicleType.None || (vehicleTypes & this.m_vehicleType) != VehicleInfo.VehicleType.None)
					{
						return true;
					}
				}
				else
				{
					if ((byte)(this.m_laneType & NetInfo.LaneType.Pedestrian) == 0)
					{
						return true;
					}
					if (stopTypes == VehicleInfo.VehicleType.None || (stopTypes & this.m_stopType) != VehicleInfo.VehicleType.None)
					{
						return true;
					}
				}
			}
			return false;
		}

		[CustomizableProperty("Position")]
		public float m_position;

		[CustomizableProperty("Width")]
		public float m_width = 3f;

		[CustomizableProperty("Vertical Offset")]
		public float m_verticalOffset;

		[CustomizableProperty("Stop Offset")]
		public float m_stopOffset;

		[CustomizableProperty("Speed Limit")]
		public float m_speedLimit = 1f;

		[CustomizableProperty("Direction")]
		public NetInfo.Direction m_direction = NetInfo.Direction.Forward;

		[CustomizableProperty("Lane Type")]
		public NetInfo.LaneType m_laneType;

		[BitMask, CustomizableProperty("Vehicle Type")]
		public VehicleInfo.VehicleType m_vehicleType;

		[BitMask, CustomizableProperty("Stop Type")]
		public VehicleInfo.VehicleType m_stopType;

		public NetLaneProps m_laneProps;

		[CustomizableProperty("Allow Connect")]
		public bool m_allowConnect = true;

		[CustomizableProperty("Use Terrain Height")]
		public bool m_useTerrainHeight;

		[CustomizableProperty("Center Platform")]
		public bool m_centerPlatform;

		[CustomizableProperty("Elevated")]
		public bool m_elevated;

		[NonSerialized]
		public NetInfo.Direction m_finalDirection;

		[NonSerialized]
		public int m_similarLaneIndex;

		[NonSerialized]
		public int m_similarLaneCount;
	}

	[Serializable]
	public class Segment
	{
		public bool CheckFlags(NetSegment.Flags flags, out bool turnAround)
		{
			if (((this.m_forwardRequired | this.m_forwardForbidden) & flags) == this.m_forwardRequired)
			{
				turnAround = false;
				return true;
			}
			if (((this.m_backwardRequired | this.m_backwardForbidden) & flags) == this.m_backwardRequired)
			{
				turnAround = true;
				return true;
			}
			turnAround = false;
			return false;
		}

		public Mesh m_mesh;

		public Mesh m_lodMesh;

		public Material m_material;

		public Material m_lodMaterial;

		[BitMask, CustomizableProperty("Forward Required")]
		public NetSegment.Flags m_forwardRequired;

		[BitMask, CustomizableProperty("Forward Forbidden")]
		public NetSegment.Flags m_forwardForbidden;

		[BitMask, CustomizableProperty("Backward Required")]
		public NetSegment.Flags m_backwardRequired;

		[BitMask, CustomizableProperty("Backward Forbidden")]
		public NetSegment.Flags m_backwardForbidden;

		public bool m_emptyTransparent;

		[CustomizableProperty("Disable Bend Nodes")]
		public bool m_disableBendNodes;

		[NonSerialized]
		public Mesh m_segmentMesh;

		[NonSerialized]
		public Material m_segmentMaterial;

		[NonSerialized]
		public NetInfo.LodValue m_combinedLod;

		[NonSerialized]
		public float m_lodRenderDistance;

		[NonSerialized]
		public bool m_requireSurfaceMaps;

		[NonSerialized]
		public bool m_requireWindSpeed;

		[NonSerialized]
		public bool m_preserveUVs;

		[NonSerialized]
		public bool m_generateTangents;

		[NonSerialized]
		public int m_layer;
	}

	[Serializable]
	public class Node
	{
		public bool CheckFlags(NetNode.Flags flags)
		{
			return ((this.m_flagsRequired | this.m_flagsForbidden) & flags) == this.m_flagsRequired;
		}

		public Mesh m_mesh;

		public Mesh m_lodMesh;

		public Material m_material;

		public Material m_lodMaterial;

		[BitMask, CustomizableProperty("Flags Required")]
		public NetNode.Flags m_flagsRequired;

		[BitMask, CustomizableProperty("Flags Forbidden")]
		public NetNode.Flags m_flagsForbidden;

		[BitMask, CustomizableProperty("Connect Group")]
		public NetInfo.ConnectGroup m_connectGroup;

		[CustomizableProperty("Direct Connect")]
		public bool m_directConnect;

		public bool m_emptyTransparent;

		[NonSerialized]
		public Mesh m_nodeMesh;

		[NonSerialized]
		public Material m_nodeMaterial;

		[NonSerialized]
		public NetInfo.LodValue m_combinedLod;

		[NonSerialized]
		public float m_lodRenderDistance;

		[NonSerialized]
		public bool m_requireSurfaceMaps;

		[NonSerialized]
		public bool m_requireWindSpeed;

		[NonSerialized]
		public bool m_preserveUVs;

		[NonSerialized]
		public bool m_generateTangents;

		[NonSerialized]
		public int m_layer;
	}

	public struct MeshKey
	{
		public Mesh m_srcMesh;

		public Rect m_atlasRect;

		public bool m_preserveUVs;

		public bool m_generateTangents;
	}

	public class MeshValue
	{
		public NetInfo.MeshKey m_key;

		public Mesh m_mesh1;

		public Mesh m_mesh4;

		public Mesh m_mesh8;

		public RenderGroup.MeshData m_data;

		public int m_refCount;
	}

	public struct LodKey
	{
		public NetInfo.MeshValue m_mesh;

		public Material m_srcMaterial;

		public int m_layer;

		public bool m_node;
	}

	public class LodValue
	{
		public NetInfo.LodKey m_key;

		public Material m_material;

		public Matrix4x4[] m_leftMatrices;

		public Matrix4x4[] m_leftMatricesB;

		public Matrix4x4[] m_rightMatrices;

		public Matrix4x4[] m_rightMatricesB;

		public Vector4[] m_meshScales;

		public Vector4[] m_centerPositions;

		public Vector4[] m_sideScales;

		public Vector4[] m_meshLocations;

		public Vector4[] m_objectIndices;

		public Vector3 m_lodMin;

		public Vector3 m_lodMax;

		public Texture m_surfaceTexA;

		public Texture m_surfaceTexB;

		public Vector4 m_surfaceMapping;

		public int m_lodCount;

		public int m_refCount;
	}
}
