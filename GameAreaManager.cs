﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class GameAreaManager : SimulationManagerBase<GameAreaManager, GameAreaProperties>, ISimulationManager, IRenderableManager
{
	public int MaxAreaCount
	{
		get
		{
			if (this.m_maxAreaCount == 0)
			{
				this.m_maxAreaCount = 9;
			}
			return this.m_maxAreaCount;
		}
	}

	public bool ThumbnailMode
	{
		get
		{
			return this.m_thumbnailMode;
		}
		set
		{
			this.m_thumbnailMode = value;
		}
	}

	public bool BordersVisible
	{
		get
		{
			return this.m_bordersVisible;
		}
		set
		{
			this.m_bordersVisible = value;
		}
	}

	public bool AreasVisible
	{
		get
		{
			return this.m_areasVisible;
		}
		set
		{
			if (this.m_areasVisible != value)
			{
				this.m_areasVisible = value;
				this.UpdateAreaMapping();
			}
		}
	}

	public int HighlightAreaIndex
	{
		get
		{
			return this.m_highlightAreaIndex;
		}
		set
		{
			if (this.m_highlightAreaIndex != value)
			{
				this.m_highlightAreaIndex = value;
				if (this.m_areasVisible)
				{
					this.UpdateAreaMapping();
				}
			}
		}
	}

	public void SkipFading()
	{
		if (this.m_areasVisible)
		{
			this.m_areaAlpha = 1f;
			this.m_borderAlpha = 0f;
		}
		else if (this.m_bordersVisible)
		{
			this.m_borderAlpha = 1f;
			this.m_areaAlpha = 0f;
		}
		else
		{
			this.m_areaAlpha = 0f;
			this.m_borderAlpha = 0f;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_areaGrid = new int[25];
		this.m_startTile = 12;
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_AreaMapping = Shader.PropertyToID("_AreaMapping");
		this.ID_DecorationArea = Shader.PropertyToID("_DecorationArea");
		this.ID_DecorationAlpha = Shader.PropertyToID("_DecorationAlpha");
		this.ID_ThumbshotAlpha = Shader.PropertyToID("_ThumbshotAlpha");
		this.m_bordersVisible = true;
		this.m_areaTex = new Texture2D(8, 8, 5, false, true);
		this.m_areaTex.set_filterMode(0);
		this.m_areaTex.set_wrapMode(1);
		this.m_areasUpdated = true;
		this.UpdateAreaMapping();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		if (this.m_areaTex != null)
		{
			Object.Destroy(this.m_areaTex);
			this.m_areaTex = null;
		}
		if (this.m_borderMaterial != null)
		{
			Object.Destroy(this.m_borderMaterial);
			this.m_borderMaterial = null;
		}
		if (this.m_areaMaterial != null)
		{
			Object.Destroy(this.m_areaMaterial);
			this.m_areaMaterial = null;
		}
		if (this.m_decorationMaterial != null)
		{
			Object.Destroy(this.m_decorationMaterial);
			this.m_decorationMaterial = null;
		}
	}

	private void CreateRelay()
	{
		if (this.m_AreasWrapper == null)
		{
			this.m_AreasWrapper = new AreasWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_AreasWrapper != null)
		{
			this.m_AreasWrapper.Release();
			this.m_AreasWrapper = null;
		}
	}

	public override void InitializeProperties(GameAreaProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_borderMesh == null)
		{
			this.GenerateBorderMesh();
		}
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
		if (this.m_areasVisible)
		{
			this.UpdateAreaMapping();
		}
		this.m_borderMaterial = new Material(this.m_properties.m_borderMaterial);
		this.m_areaMaterial = new Material(this.m_properties.m_areaMaterial);
		this.m_decorationMaterial = new Material(this.m_properties.m_decorationMaterial);
	}

	public override void DestroyProperties(GameAreaProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_cameraController = null;
			if (this.m_borderMaterial != null)
			{
				Object.Destroy(this.m_borderMaterial);
				this.m_borderMaterial = null;
			}
			if (this.m_areaMaterial != null)
			{
				Object.Destroy(this.m_areaMaterial);
				this.m_areaMaterial = null;
			}
			if (this.m_decorationMaterial != null)
			{
				Object.Destroy(this.m_decorationMaterial);
				this.m_decorationMaterial = null;
			}
		}
		base.DestroyProperties(properties);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		int num = 2;
		int num2;
		int num3;
		this.GetStartTile(out num2, out num3);
		Vector3 startOffset;
		startOffset..ctor((float)(num2 - num) * 1920f, 0f, (float)(num3 - num) * 1920f);
		this.m_cameraController.Reset(startOffset);
	}

	private void UpdateAreaMapping()
	{
		if (this.m_areasVisible)
		{
			this.UpdateAreaTexture();
		}
		if (this.m_cameraController != null)
		{
			if (this.m_areasVisible)
			{
				Vector3 vector = this.m_cameraController.get_transform().TransformDirection(Vector3.get_right());
				ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
				Bounds freeBounds;
				if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
				{
					freeBounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(9600f, 1024f, 9600f));
				}
				else
				{
					freeBounds = this.GetFreeBounds();
				}
				Vector3 center = freeBounds.get_center();
				if (Mathf.Abs(vector.x) >= Mathf.Abs(vector.z))
				{
					if (vector.x > 0f)
					{
						center.z -= freeBounds.get_size().z * 0.1f + 200f;
					}
					else
					{
						center.z += freeBounds.get_size().z * 0.1f + 200f;
					}
					this.m_cameraController.SetOverrideModeOn(center, new Vector2((vector.x <= 0f) ? 180f : 0f, 60f), freeBounds.get_size().z);
				}
				else
				{
					if (vector.z > 0f)
					{
						center.x += freeBounds.get_size().x * 0.1f + 200f;
					}
					else
					{
						center.x -= freeBounds.get_size().x * 0.1f + 200f;
					}
					this.m_cameraController.SetOverrideModeOn(center, new Vector2((vector.z <= 0f) ? 90f : -90f, 60f), freeBounds.get_size().x);
				}
			}
			else
			{
				this.m_cameraController.SetOverrideModeOff();
			}
		}
	}

	private void UpdateAreaTexture()
	{
		this.m_areasUpdated = false;
		int num = 1;
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			int num2;
			int num3;
			this.GetStartTile(out num2, out num3);
			for (int i = 0; i <= 8; i++)
			{
				for (int j = 0; j <= 8; j++)
				{
					int num4 = j - num;
					int num5 = i - num;
					bool flag = num4 == num2 && num5 == num3;
					bool flag2 = !flag && num4 >= 0 && num5 >= 0 && num4 < 5 && num5 < 5;
					Color color;
					color.r = ((!flag) ? 0f : 1f);
					color.g = ((!flag2) ? 0f : 1f);
					color.b = ((!flag2 || this.m_highlightAreaIndex != num5 * 5 + num4) ? 0f : 1f);
					color.a = 1f;
					this.m_areaTex.SetPixel(j, i, color);
				}
			}
		}
		else
		{
			for (int k = 0; k <= 8; k++)
			{
				for (int l = 0; l <= 8; l++)
				{
					int num6 = l - num;
					int num7 = k - num;
					bool flag3 = this.IsUnlocked(num6, num7);
					bool flag4 = this.CanUnlock(num6, num7);
					Color color2;
					color2.r = ((!flag3) ? 0f : 1f);
					color2.g = ((!flag4) ? 0f : 1f);
					if (this.m_highlightAreaIndex == num7 * 5 + num6)
					{
						if (flag4)
						{
							color2.b = 0.5f;
						}
						else if (flag3)
						{
							color2.b = 0.5f;
						}
						else
						{
							color2.b = 0f;
						}
					}
					else
					{
						color2.b = 0f;
					}
					color2.a = 1f;
					this.m_areaTex.SetPixel(l, k, color2);
				}
			}
		}
		this.m_areaTex.Apply(false);
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_areasVisible && this.m_areasUpdated && !this.m_unlocking)
		{
			this.UpdateAreaMapping();
		}
	}

	private void Update()
	{
		if (this.m_areasVisible)
		{
			this.m_areaAlpha = Mathf.Min(1f, this.m_areaAlpha + Time.get_deltaTime() * 2f);
			this.m_borderAlpha = Mathf.Min(this.m_borderAlpha, 1f - this.m_areaAlpha);
		}
		else if (this.m_bordersVisible)
		{
			this.m_borderAlpha = Mathf.Min(1f, this.m_borderAlpha + Time.get_deltaTime() * 2f);
			this.m_areaAlpha = Mathf.Min(this.m_areaAlpha, 1f - this.m_borderAlpha);
		}
		else
		{
			this.m_areaAlpha = Mathf.Max(0f, this.m_areaAlpha - Time.get_deltaTime() * 2f);
			this.m_borderAlpha = Mathf.Max(0f, this.m_borderAlpha - Time.get_deltaTime() * 2f);
		}
	}

	private Vector4 GetBuildingLimits(BuildingInfo bi)
	{
		Bounds bounds = default(Bounds);
		bounds.Encapsulate(this.GetSubBuildingBounds(bi, Vector3.get_zero(), 0f));
		if (bi.m_subBuildings != null)
		{
			BuildingInfo.SubInfo[] subBuildings = bi.m_subBuildings;
			for (int i = 0; i < subBuildings.Length; i++)
			{
				BuildingInfo.SubInfo subInfo = subBuildings[i];
				if (subInfo.m_buildingInfo != null)
				{
					bounds.Encapsulate(this.GetSubBuildingBounds(subInfo.m_buildingInfo, subInfo.m_position, subInfo.m_angle));
				}
			}
		}
		return new Vector4(bounds.get_center().x - bounds.get_extents().x, bounds.get_center().z - bounds.get_extents().z, bounds.get_center().x + bounds.get_extents().x, bounds.get_center().z + bounds.get_extents().z);
	}

	private Bounds GetSubBuildingBounds(BuildingInfo bi, Vector3 position, float angle)
	{
		Bounds result = default(Bounds);
		int num;
		int num2;
		float num3;
		bi.GetDecorationArea(out num, out num2, out num3);
		result.Encapsulate(position + Quaternion.AngleAxis(angle, Vector3.get_up()) * new Vector3((float)num * 4f, 0f, (float)num2 * 4f));
		result.Encapsulate(position + Quaternion.AngleAxis(angle, Vector3.get_up()) * new Vector3((float)num * 4f, 0f, (float)(-(float)num2) * 4f));
		result.Encapsulate(position + Quaternion.AngleAxis(angle, Vector3.get_up()) * new Vector3((float)(-(float)num) * 4f, 0f, (float)(-(float)num2) * 4f));
		result.Encapsulate(position + Quaternion.AngleAxis(angle, Vector3.get_up()) * new Vector3((float)(-(float)num) * 4f, 0f, (float)num2 * 4f));
		return result;
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_borderAlpha >= 0.001f || this.m_thumbnailMode)
			{
				PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
				if (editPrefabInfo != null)
				{
					if (this.m_thumbnailMode && editPrefabInfo is BuildingInfo)
					{
						Vector3 vector;
						vector..ctor(0f, 512f, 0f);
						Vector3 vector2;
						vector2..ctor(17280f, 1224f, 17280f);
						Bounds bounds;
						bounds..ctor(vector, vector2);
						Vector4 buildingLimits = this.GetBuildingLimits((BuildingInfo)editPrefabInfo);
						this.m_decorationMaterial.SetVector(this.ID_DecorationArea, buildingLimits);
						this.m_decorationMaterial.SetFloat(this.ID_DecorationAlpha, (!this.m_thumbnailMode) ? this.m_borderAlpha : 1f);
						this.m_decorationMaterial.SetFloat(this.ID_ThumbshotAlpha, (!this.m_thumbnailMode) ? 0.5f : (1f - 0.5f * this.m_borderAlpha));
						GameAreaManager expr_129_cp_0 = Singleton<GameAreaManager>.get_instance();
						expr_129_cp_0.m_drawCallData.m_overlayCalls = expr_129_cp_0.m_drawCallData.m_overlayCalls + 1;
						Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_decorationMaterial, 0, bounds);
					}
					else
					{
						Vector3 vector3;
						vector3..ctor(0f, 512f, 0f);
						Vector3 vector4;
						vector4..ctor(17280f, 1224f, 17280f);
						Bounds bounds2;
						bounds2..ctor(vector3, vector4);
						int num;
						int num2;
						float num3;
						editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
						bool flag;
						bool flag2;
						bool flag3;
						bool flag4;
						editPrefabInfo.GetDecorationDirections(out flag, out flag2, out flag3, out flag4);
						float num4 = (float)num * 4f;
						float num5 = (float)num2 * 4f;
						this.m_decorationMaterial.SetVector(this.ID_DecorationArea, new Vector4(-num4, num3 - num5, num4, num3 + num5));
						this.m_decorationMaterial.SetFloat(this.ID_DecorationAlpha, (!this.m_thumbnailMode) ? this.m_borderAlpha : 1f);
						this.m_decorationMaterial.SetFloat(this.ID_ThumbshotAlpha, (!this.m_thumbnailMode) ? 0.5f : (1f - 0.5f * this.m_borderAlpha));
						GameAreaManager expr_24E_cp_0 = Singleton<GameAreaManager>.get_instance();
						expr_24E_cp_0.m_drawCallData.m_overlayCalls = expr_24E_cp_0.m_drawCallData.m_overlayCalls + 1;
						Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_decorationMaterial, 0, bounds2);
						if (!this.m_thumbnailMode)
						{
							if (flag)
							{
								Color color;
								color..ctor(1f, 1f, 1f, 0.25f);
								Quad3 quad;
								quad.a = new Vector3(-num4 - 16f - 10f, 60f, -10f);
								quad.b = new Vector3(-num4 - 16f + 10f, 60f, -10f);
								quad.c = new Vector3(-num4 - 16f + 10f, 60f, 10f);
								quad.d = new Vector3(-num4 - 16f - 10f, 60f, 10f);
								GameAreaManager expr_33F_cp_0 = Singleton<GameAreaManager>.get_instance();
								expr_33F_cp_0.m_drawCallData.m_overlayCalls = expr_33F_cp_0.m_drawCallData.m_overlayCalls + 1;
								Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, this.m_properties.m_directionArrow, color, quad, 50f, 70f, false, true);
							}
							if (flag2)
							{
								Color color2;
								color2..ctor(1f, 1f, 1f, 0.25f);
								Quad3 quad2;
								quad2.a = new Vector3(num4 + 16f + 10f, 60f, 10f);
								quad2.b = new Vector3(num4 + 16f - 10f, 60f, 10f);
								quad2.c = new Vector3(num4 + 16f - 10f, 60f, -10f);
								quad2.d = new Vector3(num4 + 16f + 10f, 60f, -10f);
								GameAreaManager expr_433_cp_0 = Singleton<GameAreaManager>.get_instance();
								expr_433_cp_0.m_drawCallData.m_overlayCalls = expr_433_cp_0.m_drawCallData.m_overlayCalls + 1;
								Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, this.m_properties.m_directionArrow, color2, quad2, 50f, 70f, false, true);
							}
							if (flag4)
							{
								Color color3;
								color3..ctor(1f, 1f, 1f, 0.25f);
								Quad3 quad3;
								quad3.a = new Vector3(-10f, 60f, num5 + 16f + 10f);
								quad3.b = new Vector3(-10f, 60f, num5 + 16f - 10f);
								quad3.c = new Vector3(10f, 60f, num5 + 16f - 10f);
								quad3.d = new Vector3(10f, 60f, num5 + 16f + 10f);
								GameAreaManager expr_527_cp_0 = Singleton<GameAreaManager>.get_instance();
								expr_527_cp_0.m_drawCallData.m_overlayCalls = expr_527_cp_0.m_drawCallData.m_overlayCalls + 1;
								Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, this.m_properties.m_directionArrow, color3, quad3, 50f, 70f, false, true);
							}
						}
					}
				}
			}
		}
		else if ((mode & (ItemClass.Availability.MapEditor | ItemClass.Availability.ScenarioEditor)) != ItemClass.Availability.None)
		{
			if (this.m_borderAlpha >= 0.001f && this.m_borderMaterial != null)
			{
				Quaternion quaternion = Quaternion.AngleAxis(90f, Vector3.get_up());
				int num6;
				int num7;
				this.GetStartTile(out num6, out num7);
				Color color4;
				color4..ctor(1f, 1f, 1f, this.m_borderAlpha);
				Color color5;
				color5..ctor(1f, 1f, 1f, this.m_borderAlpha);
				Color color6;
				color6..ctor(1f, 1f, 1f, 0.25f * this.m_borderAlpha);
				for (int i = 0; i <= 5; i++)
				{
					for (int j = 0; j <= 5; j++)
					{
						bool flag5 = this.GetArea(j, i) > 0;
						bool flag6 = this.GetArea(j, i - 1) > 0;
						bool flag7 = this.GetArea(j - 1, i) > 0;
						if (j != 5)
						{
							Vector3 vector5;
							vector5..ctor(((float)j - 2.5f + 0.5f) * 1920f, 0f, ((float)i - 2.5f) * 1920f);
							Vector3 vector6;
							vector6..ctor(1920f, 1024f, 100f);
							Bounds bounds3;
							bounds3..ctor(vector5 + new Vector3(0f, vector6.y * 0.5f, 0f), vector6);
							if (cameraInfo.Intersect(bounds3))
							{
								Singleton<TerrainManager>.get_instance().SetWaterMaterialProperties(vector5, this.m_borderMaterial);
								if ((i >= num7 && i <= num7 + 1 && j == num6) || flag5 != flag6)
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color4);
								}
								else if (i == 0 || i == 5)
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color5);
								}
								else
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color6);
								}
								if (this.m_borderMaterial.SetPass(0))
								{
									GameAreaManager expr_770_cp_0 = Singleton<GameAreaManager>.get_instance();
									expr_770_cp_0.m_drawCallData.m_overlayCalls = expr_770_cp_0.m_drawCallData.m_overlayCalls + 1;
									Graphics.DrawMeshNow(this.m_borderMesh, vector5, quaternion);
								}
							}
						}
						if (i != 5)
						{
							Vector3 vector7;
							vector7..ctor(((float)j - 2.5f) * 1920f, 0f, ((float)i - 2.5f + 0.5f) * 1920f);
							Vector3 vector8;
							vector8..ctor(100f, 1024f, 1920f);
							Bounds bounds4;
							bounds4..ctor(vector7 + new Vector3(0f, vector8.y * 0.5f, 0f), vector8);
							if (cameraInfo.Intersect(bounds4))
							{
								Singleton<TerrainManager>.get_instance().SetWaterMaterialProperties(vector7, this.m_borderMaterial);
								if ((j >= num6 && j <= num6 + 1 && i == num7) || flag5 != flag7)
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color4);
								}
								else if (j == 0 || j == 5)
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color5);
								}
								else
								{
									this.m_borderMaterial.SetColor(this.ID_Color, color6);
								}
								if (this.m_borderMaterial.SetPass(0))
								{
									GameAreaManager expr_8B8_cp_0 = Singleton<GameAreaManager>.get_instance();
									expr_8B8_cp_0.m_drawCallData.m_overlayCalls = expr_8B8_cp_0.m_drawCallData.m_overlayCalls + 1;
									Graphics.DrawMeshNow(this.m_borderMesh, vector7, Quaternion.get_identity());
								}
							}
						}
					}
				}
			}
			if (this.m_areaAlpha >= 0.001f && this.m_areaMaterial != null)
			{
				Vector4 vector9;
				vector9.z = 6.510417E-05f;
				vector9.x = 0.4375f;
				vector9.y = 0.4375f;
				vector9.w = 0.125f;
				this.m_areaMaterial.set_mainTexture(this.m_areaTex);
				this.m_areaMaterial.SetColor(this.ID_Color, new Color(1f, 1f, 1f, this.m_areaAlpha));
				this.m_areaMaterial.SetVector(this.ID_AreaMapping, vector9);
				Bounds bounds5;
				bounds5..ctor(new Vector3(0f, 512f, 0f), new Vector3(9600f, 1024f, 9600f));
				bounds5.set_size(bounds5.get_size() + new Vector3(100f, 1f, 100f));
				GameAreaManager expr_9F3_cp_0 = Singleton<GameAreaManager>.get_instance();
				expr_9F3_cp_0.m_drawCallData.m_overlayCalls = expr_9F3_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_areaMaterial, 0, bounds5);
			}
		}
		else if ((mode & ItemClass.Availability.Editors) == ItemClass.Availability.None)
		{
			if (this.m_borderAlpha >= 0.001f && this.m_borderMaterial != null)
			{
				Quaternion quaternion2 = Quaternion.AngleAxis(90f, Vector3.get_up());
				Color color7 = Color.get_white();
				ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
				if (properties != null)
				{
					ToolBase currentTool = properties.CurrentTool;
					if ((currentTool.GetErrors() & ToolBase.ToolErrors.OutOfArea) != ToolBase.ToolErrors.None)
					{
						color7 = Color.get_red();
					}
				}
				color7.a = this.m_borderAlpha;
				for (int k = 0; k <= 5; k++)
				{
					for (int l = 0; l <= 5; l++)
					{
						bool flag8 = this.GetArea(l, k) > 0;
						bool flag9 = this.GetArea(l, k - 1) > 0;
						bool flag10 = this.GetArea(l - 1, k) > 0;
						if (flag8 != flag9)
						{
							Vector3 vector10;
							vector10..ctor(((float)l - 2.5f + 0.5f) * 1920f, 0f, ((float)k - 2.5f) * 1920f);
							Vector3 vector11;
							vector11..ctor(1920f, 1024f, 100f);
							Bounds bounds6;
							bounds6..ctor(vector10 + new Vector3(0f, vector11.y * 0.5f, 0f), vector11);
							if (cameraInfo.Intersect(bounds6))
							{
								Singleton<TerrainManager>.get_instance().SetWaterMaterialProperties(vector10, this.m_borderMaterial);
								this.m_borderMaterial.SetColor(this.ID_Color, color7);
								if (this.m_borderMaterial.SetPass(0))
								{
									GameAreaManager expr_BB1_cp_0 = Singleton<GameAreaManager>.get_instance();
									expr_BB1_cp_0.m_drawCallData.m_overlayCalls = expr_BB1_cp_0.m_drawCallData.m_overlayCalls + 1;
									Graphics.DrawMeshNow(this.m_borderMesh, vector10, quaternion2);
								}
							}
						}
						if (flag8 != flag10)
						{
							Vector3 vector12;
							vector12..ctor(((float)l - 2.5f) * 1920f, 0f, ((float)k - 2.5f + 0.5f) * 1920f);
							Vector3 vector13;
							vector13..ctor(100f, 1024f, 1920f);
							Bounds bounds7;
							bounds7..ctor(vector12 + new Vector3(0f, vector13.y * 0.5f, 0f), vector13);
							if (cameraInfo.Intersect(bounds7))
							{
								Singleton<TerrainManager>.get_instance().SetWaterMaterialProperties(vector12, this.m_borderMaterial);
								this.m_borderMaterial.SetColor(this.ID_Color, color7);
								if (this.m_borderMaterial.SetPass(0))
								{
									GameAreaManager expr_C95_cp_0 = Singleton<GameAreaManager>.get_instance();
									expr_C95_cp_0.m_drawCallData.m_overlayCalls = expr_C95_cp_0.m_drawCallData.m_overlayCalls + 1;
									Graphics.DrawMeshNow(this.m_borderMesh, vector12, Quaternion.get_identity());
								}
							}
						}
					}
				}
			}
			if (this.m_areaAlpha >= 0.001f && this.m_areaMaterial != null)
			{
				Vector4 vector14;
				vector14.z = 6.510417E-05f;
				vector14.x = 0.4375f;
				vector14.y = 0.4375f;
				vector14.w = 0.125f;
				this.m_areaMaterial.set_mainTexture(this.m_areaTex);
				this.m_areaMaterial.SetColor(this.ID_Color, new Color(1f, 1f, 1f, this.m_areaAlpha));
				this.m_areaMaterial.SetVector(this.ID_AreaMapping, vector14);
				Bounds freeBounds = this.GetFreeBounds();
				freeBounds.set_size(freeBounds.get_size() + new Vector3(100f, 1f, 100f));
				GameAreaManager expr_DA9_cp_0 = Singleton<GameAreaManager>.get_instance();
				expr_DA9_cp_0.m_drawCallData.m_overlayCalls = expr_DA9_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_areaMaterial, 0, freeBounds);
			}
		}
	}

	private void GenerateBorderMesh()
	{
		this.m_borderMesh = new Mesh();
		int num = 30;
		float num2 = 40f;
		float num3 = 16f;
		float num4 = 1920f / (float)num;
		int num5 = num * 8;
		int num6 = num * 30;
		Vector3[] array = new Vector3[num5];
		int[] array2 = new int[num6];
		int num7 = 0;
		int num8 = 0;
		for (int i = 0; i < num; i++)
		{
			float num9 = ((float)i - (float)num * 0.5f + 0.5f) * num4 - num2 * 0.5f;
			array[num7++] = new Vector3(-num3 * 0.5f, -8f, num9);
			array[num7++] = new Vector3(-num3 * 0.5f, 8f, num9);
			array[num7++] = new Vector3(num3 * 0.5f, 8f, num9);
			array[num7++] = new Vector3(num3 * 0.5f, -8f, num9);
			num9 += num2;
			array[num7++] = new Vector3(-num3 * 0.5f, -8f, num9);
			array[num7++] = new Vector3(-num3 * 0.5f, 8f, num9);
			array[num7++] = new Vector3(num3 * 0.5f, 8f, num9);
			array[num7++] = new Vector3(num3 * 0.5f, -8f, num9);
			array2[num8++] = num7 - 8;
			array2[num8++] = num7 - 7;
			array2[num8++] = num7 - 5;
			array2[num8++] = num7 - 5;
			array2[num8++] = num7 - 7;
			array2[num8++] = num7 - 6;
			array2[num8++] = num7 - 8;
			array2[num8++] = num7 - 4;
			array2[num8++] = num7 - 7;
			array2[num8++] = num7 - 7;
			array2[num8++] = num7 - 4;
			array2[num8++] = num7 - 3;
			array2[num8++] = num7 - 7;
			array2[num8++] = num7 - 3;
			array2[num8++] = num7 - 6;
			array2[num8++] = num7 - 6;
			array2[num8++] = num7 - 3;
			array2[num8++] = num7 - 2;
			array2[num8++] = num7 - 6;
			array2[num8++] = num7 - 2;
			array2[num8++] = num7 - 5;
			array2[num8++] = num7 - 5;
			array2[num8++] = num7 - 2;
			array2[num8++] = num7 - 1;
			array2[num8++] = num7 - 3;
			array2[num8++] = num7 - 4;
			array2[num8++] = num7 - 2;
			array2[num8++] = num7 - 2;
			array2[num8++] = num7 - 4;
			array2[num8++] = num7 - 1;
		}
		this.m_borderMesh = new Mesh();
		this.m_borderMesh.set_vertices(array);
		this.m_borderMesh.set_triangles(array2);
		this.m_borderMesh.set_bounds(new Bounds(new Vector3(0f, 512f, 0f), new Vector3(num3, 1040f, 1920f)));
	}

	public Vector3 GetAreaPositionSmooth(int x, int z)
	{
		if (x < 0 || z < 0 || x >= 5 || z >= 5)
		{
			return Vector3.get_zero();
		}
		Vector3 vector;
		vector.x = ((float)x - 2.5f + 0.5f) * 1920f;
		vector.y = 0f;
		vector.z = ((float)z - 2.5f + 0.5f) * 1920f;
		vector.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector, true, 0f);
		return vector;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep <= 1 && properties != null)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			if (num == 700 && this.m_areaCount < this.MaxAreaCount && Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.GameAreas) && Singleton<UnlockManager>.get_instance().Unlocked(this.m_areaCount))
			{
				this.m_areaNotUnlocked.Activate(properties.m_areaNotUnlocked);
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("GameAreaManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || mode == SimulationManager.UpdateMode.NewAsset || mode == SimulationManager.UpdateMode.LoadAsset)
		{
			this.m_areaCount = 0;
			int startTile = this.m_startTile;
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					int num = i * 5 + j;
					if (num == startTile)
					{
						this.m_areaGrid[num] = ++this.m_areaCount;
					}
					else
					{
						this.m_areaGrid[num] = 0;
					}
				}
			}
		}
		int num2;
		int num3;
		this.GetStartTile(out num2, out num3);
		if (mode != SimulationManager.UpdateMode.LoadGame || this.m_buildableArea0 < 0f)
		{
			this.m_buildableArea0 = 0f;
			this.m_buildableArea1 = 0f;
			this.m_buildableArea2 = 0f;
			this.m_buildableArea3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			float num6 = 0f;
			float num7 = 0f;
			for (int k = 0; k < 5; k++)
			{
				for (int l = 0; l < 5; l++)
				{
					switch (Mathf.Abs(l - num2) + Mathf.Abs(k - num3))
					{
					case 0:
						this.m_buildableArea0 += this.CalculateBuildableArea(l, k);
						num4 += 1f;
						break;
					case 1:
						this.m_buildableArea1 += this.CalculateBuildableArea(l, k);
						num5 += 1f;
						break;
					case 2:
						this.m_buildableArea2 += this.CalculateBuildableArea(l, k);
						num6 += 1f;
						break;
					case 3:
						this.m_buildableArea3 += this.CalculateBuildableArea(l, k);
						num7 += 1f;
						break;
					}
				}
			}
			if (num4 != 0f)
			{
				this.m_buildableArea0 /= num4;
			}
			if (num5 != 0f)
			{
				this.m_buildableArea1 /= num5;
			}
			if (num6 != 0f)
			{
				this.m_buildableArea2 /= num6;
			}
			if (num7 != 0f)
			{
				this.m_buildableArea3 /= num7;
			}
		}
		int num8 = 2;
		for (int m = 0; m < 5; m++)
		{
			for (int n = 0; n < 5; n++)
			{
				if (this.GetArea(n, m) > 0)
				{
					Singleton<TerrainManager>.get_instance().SetDetailedPatch(n + num8, m + num8);
				}
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_areaNotUnlocked == null)
		{
			this.m_areaNotUnlocked = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	private float CalculateBuildableArea(int tileX, int tileZ)
	{
		uint num;
		uint num2;
		uint num3;
		uint num4;
		uint num5;
		Singleton<NaturalResourceManager>.get_instance().GetTileResources(tileX, tileZ, out num, out num2, out num3, out num4, out num5);
		float tileFlatness = Singleton<TerrainManager>.get_instance().GetTileFlatness(tileX, tileZ);
		float num6 = 3686400f;
		float num7 = 1139.0625f;
		float num8 = num6 / num7 * 255f;
		return tileFlatness * (1f - num5 / num8);
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new GameAreaManager.Data());
	}

	public bool UnlockArea(int index)
	{
		this.m_unlocking = true;
		bool result;
		try
		{
			int num = index % 5;
			int num2 = index / 5;
			if (this.CanUnlock(num, num2))
			{
				this.m_areaNotUnlocked.Deactivate();
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Unlocking new area");
				this.m_areaGrid[index] = ++this.m_areaCount;
				this.m_areasUpdated = true;
				if (this.m_areaCount == 9 && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						if (!PlatformService.get_achievements().get_Item("SIMulatedCity").get_achieved())
						{
							PlatformService.get_achievements().get_Item("SIMulatedCity").Unlock();
						}
					});
				}
				float num3 = ((float)num - 2.5f) * 1920f;
				float num4 = ((float)(num + 1) - 2.5f) * 1920f;
				float num5 = ((float)num2 - 2.5f) * 1920f;
				float num6 = ((float)(num2 + 1) - 2.5f) * 1920f;
				Quad2 quad;
				quad..ctor(new Vector2(num3, num5), new Vector2(num3, num6), new Vector2(num4, num6), new Vector2(num4, num5));
				Singleton<ZoneManager>.get_instance().UpdateBlocks(quad);
				int num7 = 2;
				if (Singleton<TerrainManager>.get_instance().SetDetailedPatch(num + num7, num2 + num7))
				{
					Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_properties.m_unlockMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID());
					this.m_AreasWrapper.OnUnlockArea(num, num2);
					result = true;
					return result;
				}
				this.m_areaCount--;
				this.m_areaGrid[index] = 0;
				this.m_areasUpdated = true;
			}
			result = false;
		}
		finally
		{
			this.m_unlocking = false;
		}
		return result;
	}

	public bool ClampPoint(ref Vector3 position)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				int num;
				int num2;
				float num3;
				editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
				float num4 = (float)num * 4f + 128f;
				float num5 = (float)num2 * 4f + 128f;
				if (position.x < -num4)
				{
					position.x = -num4;
				}
				if (position.x > num4)
				{
					position.x = num4;
				}
				if (position.z < num3 - num5)
				{
					position.z = num3 - num5;
				}
				if (position.z > num3 + num5)
				{
					position.z = num3 + num5;
				}
				return true;
			}
			return false;
		}
		else
		{
			if ((mode & ItemClass.Availability.Editors) != ItemClass.Availability.None)
			{
				float num6 = 8640f;
				if (position.x < -num6)
				{
					position.x = -num6;
				}
				if (position.x > num6)
				{
					position.x = num6;
				}
				if (position.z < -num6)
				{
					position.z = -num6;
				}
				if (position.z > num6)
				{
					position.z = num6;
				}
				return true;
			}
			int x = Mathf.FloorToInt(position.x / 1920f + 2.5f);
			int z = Mathf.FloorToInt(position.z / 1920f + 2.5f);
			if (this.GetArea(x, z) > 0)
			{
				return true;
			}
			Rect rect = default(Rect);
			rect.set_xMin(-4800f);
			rect.set_yMin(-4800f);
			rect.set_xMax(4800f);
			rect.set_yMax(4800f);
			float num7 = 1000000f;
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					if (this.m_areaGrid[i * 5 + j] > 0)
					{
						Rect rect2 = default(Rect);
						rect2.set_xMin(((float)j - 2.5f) * 1920f);
						rect2.set_yMin(((float)i - 2.5f) * 1920f);
						rect2.set_xMax(rect2.get_xMin() + 1920f);
						rect2.set_yMax(rect2.get_yMin() + 1920f);
						float num8 = Mathf.Max(Mathf.Max(position.x - rect2.get_xMax(), rect2.get_xMin() - position.x), Mathf.Max(position.z - rect2.get_yMax(), rect2.get_yMin() - position.z));
						if (num8 < num7)
						{
							rect = rect2;
							num7 = num8;
						}
					}
				}
			}
			if (position.x < rect.get_xMin())
			{
				position.x = rect.get_xMin();
			}
			if (position.x > rect.get_xMax())
			{
				position.x = rect.get_xMax();
			}
			if (position.z < rect.get_yMin())
			{
				position.z = rect.get_yMin();
			}
			if (position.z > rect.get_yMax())
			{
				position.z = rect.get_yMax();
			}
			return num7 != 1000000f;
		}
	}

	public bool QuadOutOfArea(Quad2 quad)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				int num;
				int num2;
				float num3;
				editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
				float num4 = (float)num * 4f;
				float num5 = (float)num2 * 4f;
				if (quad.a.x < -num4 || quad.a.x > num4 || quad.a.y < num3 - num5 || quad.a.y > num3 + num5)
				{
					return true;
				}
				if (quad.b.x < -num4 || quad.b.x > num4 || quad.b.y < num3 - num5 || quad.b.y > num3 + num5)
				{
					return true;
				}
				if (quad.c.x < -num4 || quad.c.x > num4 || quad.c.y < num3 - num5 || quad.c.y > num3 + num5)
				{
					return true;
				}
				if (quad.d.x < -num4 || quad.d.x > num4 || quad.d.y < num3 - num5 || quad.d.y > num3 + num5)
				{
					return true;
				}
			}
		}
		else
		{
			bool flag = (mode & ItemClass.Availability.Editors) != ItemClass.Availability.None;
			Vector2 vector = quad.Min();
			Vector2 vector2 = quad.Max();
			int num6 = Mathf.FloorToInt((vector.x - 8f) / 1920f + 2.5f);
			int num7 = Mathf.FloorToInt((vector.y - 8f) / 1920f + 2.5f);
			int num8 = Mathf.FloorToInt((vector2.x + 8f) / 1920f + 2.5f);
			int num9 = Mathf.FloorToInt((vector2.y + 8f) / 1920f + 2.5f);
			for (int i = num7; i <= num9; i++)
			{
				for (int j = num6; j <= num8; j++)
				{
					int area = this.GetArea(j, i);
					if (area == -2 || (!flag && area <= 0))
					{
						Quad2 quad2 = default(Quad2);
						quad2.a = new Vector2(((float)j - 2.5f) * 1920f - 8f, ((float)i - 2.5f) * 1920f - 8f);
						quad2.b = new Vector2(((float)j - 2.5f) * 1920f - 8f, ((float)i - 2.5f + 1f) * 1920f + 8f);
						quad2.c = new Vector2(((float)j - 2.5f + 1f) * 1920f + 8f, ((float)i - 2.5f + 1f) * 1920f + 8f);
						quad2.d = new Vector2(((float)j - 2.5f + 1f) * 1920f + 8f, ((float)i - 2.5f) * 1920f - 8f);
						if (quad.Intersect(quad2))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public void GetAreaBounds(int x, int z, out float minX, out float minZ, out float maxX, out float maxZ)
	{
		minX = ((float)x - 2.5f) * 1920f;
		minZ = ((float)z - 2.5f) * 1920f;
		maxX = ((float)x - 2.5f + 1f) * 1920f;
		maxZ = ((float)z - 2.5f + 1f) * 1920f;
	}

	public bool PointOutOfArea(Vector3 p)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				int num;
				int num2;
				float num3;
				editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
				float num4 = (float)num * 4f;
				float num5 = (float)num2 * 4f;
				if (p.x < -num4 || p.x > num4 || p.z < num3 - num5 || p.z > num3 + num5)
				{
					return true;
				}
			}
		}
		else
		{
			bool flag = (mode & ItemClass.Availability.Editors) != ItemClass.Availability.None;
			int x = Mathf.FloorToInt(p.x / 1920f + 2.5f);
			int z = Mathf.FloorToInt(p.z / 1920f + 2.5f);
			int area = this.GetArea(x, z);
			if (area == -2 || (!flag && area <= 0))
			{
				return true;
			}
		}
		return false;
	}

	public bool PointOutOfArea(Vector3 p, float radius)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				int num;
				int num2;
				float num3;
				editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
				float num4 = (float)num * 4f;
				float num5 = (float)num2 * 4f;
				if (p.x - radius < -num4 || p.x + radius > num4 || p.z - radius < num3 - num5 || p.z + radius > num3 + num5)
				{
					return true;
				}
			}
		}
		else
		{
			bool flag = (mode & ItemClass.Availability.Editors) != ItemClass.Availability.None;
			int num6 = Mathf.FloorToInt((p.x - radius) / 1920f + 2.5f);
			int num7 = Mathf.FloorToInt((p.z - radius) / 1920f + 2.5f);
			int num8 = Mathf.FloorToInt((p.x + radius) / 1920f + 2.5f);
			int num9 = Mathf.FloorToInt((p.z + radius) / 1920f + 2.5f);
			for (int i = num7; i <= num9; i++)
			{
				for (int j = num6; j <= num8; j++)
				{
					int area = this.GetArea(j, i);
					if (area == -2 || (!flag && area <= 0))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public int GetAreaIndex(Vector3 p)
	{
		int num = Mathf.FloorToInt(p.x / 1920f + 2.5f);
		int num2 = Mathf.FloorToInt(p.z / 1920f + 2.5f);
		if (num < 0 || num2 < 0 || num >= 5 || num2 >= 5)
		{
			return -1;
		}
		return num2 * 5 + num;
	}

	public int GetArea(int x, int z)
	{
		if (x >= 0 && z >= 0 && x < 5 && z < 5)
		{
			return this.m_areaGrid[z * 5 + x];
		}
		int num = 2;
		if (x < -num || z < -num || x >= 5 + num || z >= 5 + num)
		{
			return -2;
		}
		return -1;
	}

	public bool IsUnlocked(int x, int z)
	{
		return x >= 0 && z >= 0 && x < 5 && z < 5 && this.m_areaGrid[z * 5 + x] != 0;
	}

	public void GetStartTile(out int x, out int z)
	{
		int startTile = this.m_startTile;
		x = startTile % 5;
		z = startTile / 5;
	}

	public void SetStartTile(int x, int z)
	{
		this.m_startTile = z * 5 + x;
		this.m_areasUpdated = true;
	}

	public void SetStartTile(int tile)
	{
		this.m_startTile = tile;
		this.m_areasUpdated = true;
	}

	public void GetTileXZ(int tile, out int x, out int z)
	{
		x = tile % 5;
		z = tile / 5;
	}

	public void GetTileXZ(Vector3 p, out int x, out int z)
	{
		x = Mathf.Clamp(Mathf.FloorToInt(p.x / 1920f + 2.5f), 0, 4);
		z = Mathf.Clamp(Mathf.FloorToInt(p.z / 1920f + 2.5f), 0, 4);
	}

	public int GetTileIndex(int x, int z)
	{
		return z * 5 + x;
	}

	public bool CanUnlock(int x, int z)
	{
		if (x < 0 || z < 0 || x >= 5 || z >= 5)
		{
			return false;
		}
		if (this.m_areaCount >= this.MaxAreaCount)
		{
			return false;
		}
		if (!Singleton<UnlockManager>.get_instance().Unlocked(this.m_areaCount))
		{
			return false;
		}
		if (this.m_areaGrid[z * 5 + x] != 0)
		{
			return false;
		}
		bool result = this.IsUnlocked(x, z - 1) || this.IsUnlocked(x - 1, z) || this.IsUnlocked(x + 1, z) || this.IsUnlocked(x, z + 1);
		this.m_AreasWrapper.OnCanUnlockArea(x, z, ref result);
		return result;
	}

	public int CalculateTilePrice(int tile)
	{
		int x = tile % 5;
		int z = tile / 5;
		if (this.CanUnlock(x, z))
		{
			int num;
			int num2;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.Road, ItemClass.SubService.None, out num, out num2);
			int tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.Road, ItemClass.SubService.None);
			bool road = (num != 0 || num2 != 0) && tileNodeCount != 0;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, out num, out num2);
			tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain);
			bool train = (num != 0 || num2 != 0) && tileNodeCount != 0;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, out num, out num2);
			tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip);
			bool ship = (num != 0 || num2 != 0) && tileNodeCount != 0;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, out num, out num2);
			bool plane = num != 0 || num2 != 0;
			uint ore;
			uint oil;
			uint forest;
			uint fertility;
			uint water;
			Singleton<NaturalResourceManager>.get_instance().GetTileResources(x, z, out ore, out oil, out forest, out fertility, out water);
			float tileFlatness = Singleton<TerrainManager>.get_instance().GetTileFlatness(x, z);
			return this.CalculateTilePrice(ore, oil, forest, fertility, water, road, train, ship, plane, tileFlatness);
		}
		return 0;
	}

	public int CalculateTilePrice(uint ore, uint oil, uint forest, uint fertility, uint water, bool road, bool train, bool ship, bool plane, float landFlatness)
	{
		long num = (long)this.m_properties.m_baseAreaPrices[Mathf.Clamp(this.m_areaCount, 0, this.m_properties.m_baseAreaPrices.Length - 1)];
		if (road)
		{
			num += (long)this.m_properties.m_roadConnectionPrice;
		}
		if (train)
		{
			num += (long)this.m_properties.m_trainConnectionPrice;
		}
		if (ship)
		{
			num += (long)this.m_properties.m_shipConnectionPrice;
		}
		if (plane)
		{
			num += (long)this.m_properties.m_planeConnectionPrice;
		}
		float num2 = 3686400f;
		float num3 = 1139.0625f;
		long num4 = (long)Mathf.RoundToInt(num2 / num3 * 255f);
		long num5 = (long)Mathf.RoundToInt(landFlatness * (float)(num4 - (long)((ulong)water)));
		num += (long)((ulong)forest * (ulong)((long)this.m_properties.m_forestResourcePrice) / (ulong)num4);
		num += (long)((ulong)fertility * (ulong)((long)this.m_properties.m_fertilityResourcePrice) / (ulong)num4);
		num += (long)((ulong)ore * (ulong)((long)this.m_properties.m_oreResourcePrice) / (ulong)num4);
		num += (long)((ulong)oil * (ulong)((long)this.m_properties.m_oilResourcePrice) / (ulong)num4);
		num += (long)((ulong)water * (ulong)((long)this.m_properties.m_waterAreaPrice) / (ulong)num4);
		num += num5 * (long)this.m_properties.m_buildableAreaPrice / num4;
		int result = (int)((num + 5000L) / 10000L * 10000L);
		this.m_AreasWrapper.OnGetAreaPrice(ore, oil, forest, fertility, water, road, train, ship, plane, landFlatness, ref result);
		return result;
	}

	public int ScalePopulationTarget(int originalTarget)
	{
		float num = Mathf.Max(0.1f, this.m_buildableArea0);
		float num2 = Mathf.Max(0.1f, this.m_buildableArea1);
		float num3 = Mathf.Max(0.1f, this.m_buildableArea2);
		float num4 = Mathf.Max(0.1f, this.m_buildableArea3);
		float num5 = 1f;
		float num6 = (float)originalTarget / (num * 10000f);
		float num7 = (float)originalTarget / (num * 10000f + num2 * 40000f);
		float num8 = (float)originalTarget / (num * 10000f + num2 * 40000f + num3 * 80000f);
		float num9 = num5 * num + num6 * num2 + num7 * num3 + num8 * num4;
		num9 /= num5 + num6 + num7 + num8;
		int i = Mathf.CeilToInt(num9 * (float)originalTarget);
		int num10 = 1;
		while (i > num10 * 100)
		{
			num10 *= 10;
		}
		i = (i + num10 - 1) / num10;
		if (i >= 50)
		{
			i = (i + 4) / 5 * 5;
		}
		else if (i >= 20)
		{
			i = (i + 1) / 2 * 2;
		}
		return i * num10;
	}

	private Bounds GetFreeBounds()
	{
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (this.IsUnlocked(j, i))
				{
					zero.x = Mathf.Min(zero.x, ((float)(j - 1) - 2.5f) * 1920f);
					zero2.x = Mathf.Max(zero2.x, ((float)(j + 2) - 2.5f) * 1920f);
					zero.z = Mathf.Min(zero.z, ((float)(i - 1) - 2.5f) * 1920f);
					zero2.z = Mathf.Max(zero2.z, ((float)(i + 2) - 2.5f) * 1920f);
					zero2.y = Mathf.Max(zero2.y, 1024f);
				}
			}
		}
		Bounds result = default(Bounds);
		result.SetMinMax(zero, zero2);
		return result;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float AREAGRID_CELL_SIZE = 1920f;

	public const int AREAGRID_RESOLUTION = 5;

	public const int TOTAL_AREA_RESOLUTION = 9;

	public const int AREA_MAP_RESOLUTION = 8;

	public const int DEFAULT_START_X = 2;

	public const int DEFAULT_START_Z = 2;

	public int m_areaCount;

	public int m_maxAreaCount;

	public int[] m_areaGrid;

	private Mesh m_borderMesh;

	private Texture2D m_areaTex;

	private Material m_borderMaterial;

	private Material m_areaMaterial;

	private Material m_decorationMaterial;

	private int ID_Color;

	private int ID_AreaMapping;

	private int ID_DecorationArea;

	private int ID_DecorationAlpha;

	private int ID_ThumbshotAlpha;

	private bool m_bordersVisible;

	private bool m_areasVisible;

	private bool m_thumbnailMode;

	private bool m_areasUpdated;

	private volatile bool m_unlocking;

	private int m_highlightAreaIndex;

	private CameraController m_cameraController;

	private float m_borderAlpha;

	private float m_areaAlpha;

	private int m_startTile;

	private float m_buildableArea0;

	private float m_buildableArea1;

	private float m_buildableArea2;

	private float m_buildableArea3;

	[NonSerialized]
	public GenericGuide m_areaNotUnlocked;

	[NonSerialized]
	public AreasWrapper m_AreasWrapper;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "GameAreaManager");
			GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
			int[] areaGrid = instance.m_areaGrid;
			int num = areaGrid.Length;
			s.WriteUInt8((uint)instance.m_areaCount);
			s.WriteUInt8((uint)instance.m_startTile);
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				@byte.Write((byte)areaGrid[i]);
			}
			@byte.EndWrite();
			s.WriteObject<GenericGuide>(instance.m_areaNotUnlocked);
			s.WriteFloat(instance.m_buildableArea0);
			s.WriteFloat(instance.m_buildableArea1);
			s.WriteFloat(instance.m_buildableArea2);
			s.WriteFloat(instance.m_buildableArea3);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "GameAreaManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "GameAreaManager");
			GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
			int[] areaGrid = instance.m_areaGrid;
			int num = areaGrid.Length;
			instance.m_areaCount = (int)s.ReadUInt8();
			instance.m_maxAreaCount = Mathf.Max(instance.MaxAreaCount, instance.m_areaCount);
			if (s.get_version() >= 137u)
			{
				instance.m_startTile = (int)s.ReadUInt8();
			}
			else
			{
				instance.m_startTile = 12;
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				areaGrid[i] = (int)@byte.Read();
			}
			@byte.EndRead();
			if (s.get_version() >= 87u)
			{
				instance.m_areaNotUnlocked = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_areaNotUnlocked = null;
			}
			if (s.get_version() >= 199u)
			{
				instance.m_buildableArea0 = s.ReadFloat();
				instance.m_buildableArea1 = s.ReadFloat();
				instance.m_buildableArea2 = s.ReadFloat();
				instance.m_buildableArea3 = s.ReadFloat();
			}
			else
			{
				instance.m_buildableArea0 = -1f;
				instance.m_buildableArea1 = -1f;
				instance.m_buildableArea2 = -1f;
				instance.m_buildableArea3 = -1f;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "GameAreaManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "GameAreaManager");
			GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
			instance.m_areasUpdated = true;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "GameAreaManager");
		}
	}
}
