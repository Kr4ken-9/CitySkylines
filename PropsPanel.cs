﻿using System;
using ColossalFramework.UI;

public sealed class PropsPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Prop, new Comparison<PrefabInfo>(base.ItemsGenericSort));
		base.RefreshDLCBadges();
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		PrefabInfo prefabInfo;
		if (base.GetInfo(comp, out prefabInfo) && prefabInfo is PropInfo)
		{
			PropTool propTool = ToolsModifierControl.SetTool<PropTool>();
			if (propTool != null)
			{
				propTool.m_prefab = (PropInfo)prefabInfo;
			}
		}
	}
}
