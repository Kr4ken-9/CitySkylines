﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class DecorationKeyShortcuts : KeyShortcuts
{
	private void Awake()
	{
		this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
		UIPanel uIPanel = UIView.Find<UIPanel>("InfoViewsPanel");
		if (uIPanel != null)
		{
			this.m_CloseInfoViews = uIPanel.Find<UIButton>("CloseButton");
		}
	}

	private void SteamEscape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
	}

	protected void Update()
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseMenu))
			{
				UIView.get_library().ShowModal("PauseMenu");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseSimulation))
			{
				base.SimulationPause();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.NormalSimSpeed))
			{
				base.SimulationSpeed1();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.MediumSimSpeed))
			{
				base.SimulationSpeed2();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FastSimSpeed))
			{
				base.SimulationSpeed3();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Escape))
			{
				this.SteamEscape();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FreeCamera))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Bulldoze))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.InfoViews))
			{
				base.SelectUIButton("InfoPanel");
			}
		}
	}

	protected override void OnProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (this.m_ShortcutSimulationPause.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationPause();
			}
			else if (this.m_ShortcutSimulationSpeed1.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed1();
			}
			else if (this.m_ShortcutSimulationSpeed2.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed2();
			}
			else if (this.m_ShortcutSimulationSpeed3.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed3();
			}
			else if (eventType == 4 && keyCode == 27)
			{
				this.Escape();
			}
			else if (this.m_ShortcutRoads.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Roads");
			}
			else if (this.m_ShortcutBeautification.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Beautification");
			}
			else if (this.m_ShortcutPropsBillboards.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboards");
			}
			else if (this.m_ShortcutPropsSpecialBillboards.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboards");
			}
			else if (this.m_ShortcutPropsIndustrial.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsIndustrial");
			}
			else if (this.m_ShortcutPropsParks.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsParks");
			}
			else if (this.m_ShortcutPropsCommon.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommon");
			}
			else if (this.m_ShortcutPropsResidential.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsResidential");
			}
			else if (this.m_ShortcutPropsMarkers.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsMarkers");
			}
			else if (this.m_ShortcutSurface.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Surface");
			}
			else if (this.m_ShortcutDecorationRoadsSmallGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsSmallGroup");
			}
			else if (this.m_ShortcutDecorationRoadsMediumGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsMediumGroup");
			}
			else if (this.m_ShortcutDecorationRoadsLargeGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsLargeGroup");
			}
			else if (this.m_ShortcutDecorationRoadsHighwayGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsHighwayGroup");
			}
			else if (this.m_ShortcutDecorationPublicTransportTramGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportTramGroup");
			}
			else if (this.m_ShortcutDecorationPublicTransportTrainGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportTrainGroup");
			}
			else if (this.m_ShortcutDecorationRoadsIntersectionGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsIntersectionGroup");
			}
			else if (this.m_ShortcutDecorationBeautificationPathsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationPathsGroup");
			}
			else if (this.m_ShortcutDecorationBeautificationPropsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationPropsGroup");
			}
			else if (this.m_ShortcutDecorationPropsBillboardsLogoGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboardsLogoGroup");
			}
			else if (this.m_ShortcutDecorationPropsBillboardsSmallBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboardsSmallBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsBillboardsMediumBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboardsMediumBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsBillboardsLargeBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboardsLargeBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsBillboardsRandomLogoGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsBillboardsRandomLogoGroup");
			}
			else if (this.m_ShortcutDecorationPropsSpecialBillboardsRandomSmallBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboardsRandomSmallBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsSpecialBillboardsRandomMediumBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboardsRandomMediumBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsSpecialBillboardsRandomLargeBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboardsRandomLargeBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsSpecialBillboards3DBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboards3DBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsSpecialBillboardsAnimatedBillboardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsSpecialBillboardsAnimatedBillboardGroup");
			}
			else if (this.m_ShortcutDecorationPropsIndustrialContainersGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsIndustrialContainersGroup");
			}
			else if (this.m_ShortcutDecorationPropsIndustrialConstructionMaterialsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsIndustrialConstructionMaterialsGroup");
			}
			else if (this.m_ShortcutDecorationPropsIndustrialStructuresGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsIndustrialStructuresGroup");
			}
			else if (this.m_ShortcutDecorationPropsParksPlaygroundsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsParksPlaygroundsGroup");
			}
			else if (this.m_ShortcutDecorationPropsParksFlowersAndPlantsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsParksFlowersAndPlantsGroup");
			}
			else if (this.m_ShortcutDecorationPropsParksParkEquipmentGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsParksParkEquipmentGroup");
			}
			else if (this.m_ShortcutDecorationPropsParksFountainsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsParksFountainsGroup");
			}
			else if (this.m_ShortcutDecorationPropsCommonAccessoriesGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommonAccessoriesGroup");
			}
			else if (this.m_ShortcutDecorationPropsCommonGarbageGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommonGarbageGroup");
			}
			else if (this.m_ShortcutDecorationPropsCommonCommunicationsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommonCommunicationsGroup");
			}
			else if (this.m_ShortcutDecorationPropsCommonStreetsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommonStreetsGroup");
			}
			else if (this.m_ShortcutDecorationPropsCommonLightsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsCommonLightsGroup");
			}
			else if (this.m_ShortcutDecorationPropsResidentialHomeYardGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsResidentialHomeYardGroup");
			}
			else if (this.m_ShortcutDecorationPropsResidentialGroundTilesGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsResidentialGroundTilesGroup");
			}
			else if (this.m_ShortcutDecorationPropsResidentialRooftopAccessGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsResidentialRooftopAccessGroup");
			}
			else if (this.m_ShortcutDecorationPropsResidentialRandomRooftopAccessGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PropsResidentialRandomRooftopAccessGroup");
			}
			else if (this.m_ShortcutSettings.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("AssetEditorSettings");
			}
			else if (this.m_ShortcutBulldozer.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (this.m_ShortcutBulldozerUnderground.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BulldozerUndergroundToggle");
			}
			else if (this.m_ShortcutInGameShortcutFreeCameraMode.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (this.m_ShortcutDecorationShortcutSnapToAngle.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAngles = !SnapSettingsPanel.snapToAngles;
			}
			else if (this.m_ShortcutDecorationShortcutSnapToLength.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToLength = !SnapSettingsPanel.snapToLength;
			}
			else if (this.m_ShortcutDecorationShortcutSnapToGrid.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToGrid = !SnapSettingsPanel.snapToGrid;
			}
			else if (this.m_ShortcutDecorationShortcutSnapToHelperLines.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToHelperLine = !SnapSettingsPanel.snapToHelperLine;
			}
			else if (this.m_ShortcutDecorationShortcutSnapToAll.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAll = !SnapSettingsPanel.snapToAll;
			}
			else if (this.m_ShortcutToggleSnappingMenu.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressSnappingMenuButton();
			}
			else if (this.m_ShortcutElevationStep.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressElevationStepButton();
			}
			else if (this.m_ShortcutOptionButton1.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(0);
			}
			else if (this.m_ShortcutOptionButton2.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(1);
			}
			else if (this.m_ShortcutOptionButton3.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(2);
			}
			else if (this.m_ShortcutOptionButton4.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(3);
			}
		}
	}

	private void Escape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else
		{
			UIView.get_library().ShowModal("PauseMenu");
		}
	}

	private SavedInputKey m_ShortcutSimulationPause = new SavedInputKey(Settings.decorationSimulationPause, Settings.inputSettingsFile, DefaultSettings.decorationSimulationPause, true);

	private SavedInputKey m_ShortcutSimulationSpeed1 = new SavedInputKey(Settings.decorationSimulationSpeed1, Settings.inputSettingsFile, DefaultSettings.decorationSimulationSpeed1, true);

	private SavedInputKey m_ShortcutSimulationSpeed2 = new SavedInputKey(Settings.decorationSimulationSpeed2, Settings.inputSettingsFile, DefaultSettings.decorationSimulationSpeed2, true);

	private SavedInputKey m_ShortcutSimulationSpeed3 = new SavedInputKey(Settings.decorationSimulationSpeed3, Settings.inputSettingsFile, DefaultSettings.decorationSimulationSpeed3, true);

	private SavedInputKey m_ShortcutInGameShortcutFreeCameraMode = new SavedInputKey(Settings.inGameShortcutFreeCameraMode, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFreeCameraMode, true);

	private SavedInputKey m_ShortcutRoads = new SavedInputKey(Settings.decorationRoads, Settings.inputSettingsFile, DefaultSettings.decorationRoads, true);

	private SavedInputKey m_ShortcutBeautification = new SavedInputKey(Settings.decorationBeautification, Settings.inputSettingsFile, DefaultSettings.decorationBeautification, true);

	private SavedInputKey m_ShortcutPropsBillboards = new SavedInputKey(Settings.decorationPropsBillboards, Settings.inputSettingsFile, DefaultSettings.decorationPropsBillboards, true);

	private SavedInputKey m_ShortcutPropsSpecialBillboards = new SavedInputKey(Settings.decorationPropsSpecialBillboards, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboards, true);

	private SavedInputKey m_ShortcutPropsIndustrial = new SavedInputKey(Settings.decorationPropsIndustrial, Settings.inputSettingsFile, DefaultSettings.decorationPropsIndustrial, true);

	private SavedInputKey m_ShortcutPropsParks = new SavedInputKey(Settings.decorationPropsParks, Settings.inputSettingsFile, DefaultSettings.decorationPropsParks, true);

	private SavedInputKey m_ShortcutPropsCommon = new SavedInputKey(Settings.decorationPropsCommon, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommon, true);

	private SavedInputKey m_ShortcutPropsResidential = new SavedInputKey(Settings.decorationPropsResidential, Settings.inputSettingsFile, DefaultSettings.decorationPropsResidential, true);

	private SavedInputKey m_ShortcutPropsMarkers = new SavedInputKey(Settings.decorationPropsMarkers, Settings.inputSettingsFile, DefaultSettings.decorationPropsMarkers, true);

	private SavedInputKey m_ShortcutSurface = new SavedInputKey(Settings.decorationSurface, Settings.inputSettingsFile, DefaultSettings.decorationSurface, true);

	private SavedInputKey m_ShortcutSettings = new SavedInputKey(Settings.decorationSettings, Settings.inputSettingsFile, DefaultSettings.decorationSettings, true);

	private SavedInputKey m_ShortcutDecorationRoadsSmallGroup = new SavedInputKey(Settings.decorationRoadsSmallGroup, Settings.inputSettingsFile, DefaultSettings.decorationRoadsSmallGroup, true);

	private SavedInputKey m_ShortcutDecorationRoadsMediumGroup = new SavedInputKey(Settings.decorationRoadsMediumGroup, Settings.inputSettingsFile, DefaultSettings.decorationRoadsMediumGroup, true);

	private SavedInputKey m_ShortcutDecorationRoadsLargeGroup = new SavedInputKey(Settings.decorationRoadsLargeGroup, Settings.inputSettingsFile, DefaultSettings.decorationRoadsLargeGroup, true);

	private SavedInputKey m_ShortcutDecorationRoadsHighwayGroup = new SavedInputKey(Settings.decorationRoadsHighwayGroup, Settings.inputSettingsFile, DefaultSettings.decorationRoadsHighwayGroup, true);

	private SavedInputKey m_ShortcutDecorationPublicTransportTramGroup = new SavedInputKey(Settings.decorationPublicTransportTramGroup, Settings.inputSettingsFile, DefaultSettings.decorationPublicTransportTramGroup, true);

	private SavedInputKey m_ShortcutDecorationPublicTransportTrainGroup = new SavedInputKey(Settings.decorationPublicTransportTrainGroup, Settings.inputSettingsFile, DefaultSettings.decorationPublicTransportTrainGroup, true);

	private SavedInputKey m_ShortcutDecorationRoadsIntersectionGroup = new SavedInputKey(Settings.decorationRoadsIntersectionGroup, Settings.inputSettingsFile, DefaultSettings.decorationRoadsIntersectionGroup, true);

	private SavedInputKey m_ShortcutDecorationBeautificationPathsGroup = new SavedInputKey(Settings.decorationBeautificationPathsGroup, Settings.inputSettingsFile, DefaultSettings.decorationBeautificationPathsGroup, true);

	private SavedInputKey m_ShortcutDecorationBeautificationPropsGroup = new SavedInputKey(Settings.decorationBeautificationPropsGroup, Settings.inputSettingsFile, DefaultSettings.decorationBeautificationPropsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsBillboardsLogoGroup = new SavedInputKey(Settings.decorationPropsBillboardsLogoGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsBillboardsLogoGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsBillboardsSmallBillboardGroup = new SavedInputKey(Settings.decorationPropsBillboardsSmallBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsBillboardsSmallBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsBillboardsMediumBillboardGroup = new SavedInputKey(Settings.decorationPropsBillboardsMediumBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsBillboardsMediumBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsBillboardsLargeBillboardGroup = new SavedInputKey(Settings.decorationPropsBillboardsLargeBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsBillboardsLargeBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsBillboardsRandomLogoGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboardsRandomSmallBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboardsRandomSmallBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsSpecialBillboardsRandomSmallBillboardGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboardsRandomSmallBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboardsRandomSmallBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsSpecialBillboardsRandomMediumBillboardGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboardsRandomMediumBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboardsRandomMediumBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsSpecialBillboardsRandomLargeBillboardGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboardsRandomLargeBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboardsRandomLargeBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsSpecialBillboards3DBillboardGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboards3DBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboards3DBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsSpecialBillboardsAnimatedBillboardGroup = new SavedInputKey(Settings.decorationPropsSpecialBillboardsAnimatedBillboardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsSpecialBillboardsAnimatedBillboardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsIndustrialContainersGroup = new SavedInputKey(Settings.decorationPropsIndustrialContainersGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsIndustrialContainersGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsIndustrialConstructionMaterialsGroup = new SavedInputKey(Settings.decorationPropsIndustrialConstructionMaterialsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsIndustrialConstructionMaterialsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsIndustrialStructuresGroup = new SavedInputKey(Settings.decorationPropsIndustrialStructuresGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsIndustrialStructuresGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsParksPlaygroundsGroup = new SavedInputKey(Settings.decorationPropsParksPlaygroundsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsParksPlaygroundsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsParksFlowersAndPlantsGroup = new SavedInputKey(Settings.decorationPropsParksFlowersAndPlantsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsParksFlowersAndPlantsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsParksParkEquipmentGroup = new SavedInputKey(Settings.decorationPropsParksParkEquipmentGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsParksParkEquipmentGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsParksFountainsGroup = new SavedInputKey(Settings.decorationPropsParksFountainsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsParksFountainsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsCommonAccessoriesGroup = new SavedInputKey(Settings.decorationPropsCommonAccessoriesGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommonAccessoriesGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsCommonGarbageGroup = new SavedInputKey(Settings.decorationPropsCommonGarbageGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommonGarbageGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsCommonCommunicationsGroup = new SavedInputKey(Settings.decorationPropsCommonCommunicationsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommonCommunicationsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsCommonStreetsGroup = new SavedInputKey(Settings.decorationPropsCommonStreetsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommonStreetsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsCommonLightsGroup = new SavedInputKey(Settings.decorationPropsCommonLightsGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsCommonLightsGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsResidentialHomeYardGroup = new SavedInputKey(Settings.decorationPropsResidentialHomeYardGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsResidentialHomeYardGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsResidentialGroundTilesGroup = new SavedInputKey(Settings.decorationPropsResidentialGroundTilesGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsResidentialGroundTilesGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsResidentialRooftopAccessGroup = new SavedInputKey(Settings.decorationPropsResidentialRooftopAccessGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsResidentialRooftopAccessGroup, true);

	private SavedInputKey m_ShortcutDecorationPropsResidentialRandomRooftopAccessGroup = new SavedInputKey(Settings.decorationPropsResidentialRandomRooftopAccessGroup, Settings.inputSettingsFile, DefaultSettings.decorationPropsResidentialRandomRooftopAccessGroup, true);

	private SavedInputKey m_ShortcutBulldozer = new SavedInputKey(Settings.decorationShortcutBulldozer, Settings.inputSettingsFile, DefaultSettings.decorationShortcutBulldozer, true);

	private SavedInputKey m_ShortcutBulldozerUnderground = new SavedInputKey(Settings.decorationShortcutBulldozerUnderground, Settings.inputSettingsFile, DefaultSettings.decorationShortcutBulldozerUnderground, true);

	private SavedInputKey m_ShortcutToggleSnappingMenu = new SavedInputKey(Settings.sharedShortcutToggleSnappingMenu, Settings.inputSettingsFile, DefaultSettings.sharedShortcutToggleSnappingMenu, true);

	private SavedInputKey m_ShortcutElevationStep = new SavedInputKey(Settings.sharedShortcutElevationStep, Settings.inputSettingsFile, DefaultSettings.sharedShortcutElevationStep, true);

	private SavedInputKey m_ShortcutOptionButton1 = new SavedInputKey(Settings.sharedShortcutOptionsButton1, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton1, true);

	private SavedInputKey m_ShortcutOptionButton2 = new SavedInputKey(Settings.sharedShortcutOptionsButton2, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton2, true);

	private SavedInputKey m_ShortcutOptionButton3 = new SavedInputKey(Settings.sharedShortcutOptionsButton3, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton3, true);

	private SavedInputKey m_ShortcutOptionButton4 = new SavedInputKey(Settings.sharedShortcutOptionsButton4, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton4, true);

	private SavedInputKey m_ShortcutDecorationShortcutSnapToAngle = new SavedInputKey(Settings.inGameShortcutSnapToAngle, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAngle, true);

	private SavedInputKey m_ShortcutDecorationShortcutSnapToLength = new SavedInputKey(Settings.inGameShortcutSnapToLength, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToLength, true);

	private SavedInputKey m_ShortcutDecorationShortcutSnapToGrid = new SavedInputKey(Settings.inGameShortcutSnapToGrid, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToGrid, true);

	private SavedInputKey m_ShortcutDecorationShortcutSnapToHelperLines = new SavedInputKey(Settings.inGameShortcutSnapToHelperLines, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToHelperLines, true);

	private SavedInputKey m_ShortcutDecorationShortcutSnapToAll = new SavedInputKey(Settings.inGameShortcutSnapToAll, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAll, true);

	private UIButton m_CloseToolbarButton;

	private UIButton m_CloseInfoViews;
}
