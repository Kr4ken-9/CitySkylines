﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public class WaterSimulation : MonoBehaviour
{
	public WaterSimulation.Cell[] BeginRead()
	{
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		WaterSimulation.Cell[] result;
		try
		{
			this.m_readLockCount++;
			result = this.m_waterBuffers[(int)((UIntPtr)(~(this.m_waterFrameIndex >> 6) & 1u))];
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
		return result;
	}

	public void EndRead()
	{
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		try
		{
			if (--this.m_readLockCount == 0)
			{
				Monitor.PulseAll(this.m_simulationBufferLock);
			}
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
	}

	private void Awake()
	{
		this.m_waterProfiler = new ThreadProfiler();
		int num = 1080;
		this.m_waterBuffers = new WaterSimulation.Cell[2][];
		for (int i = 0; i < this.m_waterBuffers.Length; i++)
		{
			this.m_waterBuffers[i] = new WaterSimulation.Cell[(num + 1) * (num + 1)];
		}
		this.m_heightMaps = new byte[81][];
		this.m_surfaceMapsA = new byte[81][];
		this.m_surfaceMapsB = new byte[81][];
		for (int j = 0; j < this.m_heightMaps.Length; j++)
		{
			this.m_heightMaps[j] = new byte[65536];
			this.m_surfaceMapsA[j] = new byte[12288];
			this.m_surfaceMapsB[j] = new byte[12288];
		}
		this.m_heightBounds = new Vector4[81];
		this.m_waterSources = new FastList<WaterSource>();
		this.m_waterWaves = new FastList<WaterWave>();
		this.m_tsunamiWaves = new FastList<WaterWave>();
		this.m_impactWaves = new FastList<WaterWave>();
		this.m_currentSeaLevel = 40f;
		this.m_nextSeaLevel = 40f;
		this.m_resetWater = false;
		this.m_tempPollutionDisposeRate = 1;
		this.m_finalPollutionDisposeRate = 1;
		this.m_bitCells = 17;
		this.m_waterExists1 = new ulong[64];
		this.m_waterExists2 = new ulong[64];
		this.m_waterExists3 = new ulong[64];
		this.m_simulationBufferLock = new object();
		this.m_simulationStatus = WaterSimulation.Status.Running;
		this.m_simulationThread = new Thread(new ThreadStart(this.WaterThread));
		this.m_simulationThread.Name = "Water";
		this.m_simulationThread.Priority = SimulationManager.SIMULATION_PRIORITY;
		this.m_simulationThread.Start();
		if (!this.m_simulationThread.IsAlive)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Water simulation thread failed to start!");
		}
	}

	public void Initialize(ushort[] heights)
	{
		this.m_heightBuffer = heights;
	}

	private void OnDestroy()
	{
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		try
		{
			this.m_simulationStatus = WaterSimulation.Status.Terminated;
			Monitor.PulseAll(this.m_simulationBufferLock);
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
	}

	public bool WaterExists(int minX, int minZ, int maxX, int maxZ)
	{
		minX = Mathf.Max((minX - 1) / this.m_bitCells, 0);
		minZ = Mathf.Max((minZ - 1) / this.m_bitCells, 0);
		maxX = Mathf.Min((maxX + 2) / this.m_bitCells, 63);
		maxZ = Mathf.Min((maxZ + 2) / this.m_bitCells, 63);
		ulong num = 18446744073709551615uL << minX & ~(18446744073709551614uL << maxX);
		for (int i = minZ; i <= maxZ; i++)
		{
			if (((this.m_waterExists1[i] | this.m_waterExists2[i] | this.m_waterExists3[i]) & num) != 0uL)
			{
				return true;
			}
		}
		return false;
	}

	public bool WaterExists(int x, int z)
	{
		x /= this.m_bitCells;
		z /= this.m_bitCells;
		return ((this.m_waterExists1[z] | this.m_waterExists2[z] | this.m_waterExists3[z]) & 1uL << x) != 0uL;
	}

	public void BeginTerrainUpdate(out uint renderingFrameIndex, out uint targetFrameIndex)
	{
		renderingFrameIndex = this.m_renderingFrameIndex;
		targetFrameIndex = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		if (targetFrameIndex < renderingFrameIndex)
		{
			targetFrameIndex = renderingFrameIndex;
		}
	}

	public void EndTerrainUpdate(uint renderingFrameIndex)
	{
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		try
		{
			this.m_renderingFrameIndex = renderingFrameIndex;
			Monitor.PulseAll(this.m_simulationBufferLock);
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
	}

	public WaterSource LockWaterSource(ushort source)
	{
		while (!Monitor.TryEnter(this.m_waterSources, 0))
		{
		}
		return this.m_waterSources.m_buffer[(int)(source - 1)];
	}

	public void UnlockWaterSource(ushort source, WaterSource sourceData)
	{
		this.m_waterSources.m_buffer[(int)(source - 1)] = sourceData;
		Monitor.Exit(this.m_waterSources);
	}

	public bool CreateWaterSource(out ushort source, WaterSource sourceData)
	{
		source = 0;
		while (!Monitor.TryEnter(this.m_waterSources, 0))
		{
		}
		try
		{
			for (int i = 0; i < this.m_waterSources.m_size; i++)
			{
				if (this.m_waterSources.m_buffer[i].m_type == 0)
				{
					source = (ushort)(i + 1);
					this.m_waterSources.m_buffer[i] = sourceData;
					bool result = true;
					return result;
				}
			}
			if (source == 0 && this.m_waterSources.m_size < 65535)
			{
				source = (ushort)(this.m_waterSources.m_size + 1);
				this.m_waterSources.Add(sourceData);
				bool result = true;
				return result;
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterSources);
		}
		return false;
	}

	public void ReleaseWaterSource(ushort source)
	{
		while (!Monitor.TryEnter(this.m_waterSources, 0))
		{
		}
		try
		{
			this.m_waterSources.m_buffer[(int)(source - 1)].m_type = 0;
			while (this.m_waterSources.m_size != 0)
			{
				if (this.m_waterSources.m_buffer[this.m_waterSources.m_size - 1].m_type != 0)
				{
					break;
				}
				this.m_waterSources.m_size--;
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterSources);
		}
	}

	public bool CreateWaterWave(out ushort wave, WaterWave waveData)
	{
		wave = 0;
		while (!Monitor.TryEnter(this.m_waterWaves, 0))
		{
		}
		try
		{
			for (int i = 0; i < this.m_waterWaves.m_size; i++)
			{
				if (this.m_waterWaves.m_buffer[i].m_type == 0)
				{
					wave = (ushort)(i + 1);
					this.m_waterWaves.m_buffer[i] = waveData;
					bool result = true;
					return result;
				}
			}
			if (wave == 0 && this.m_waterWaves.m_size < 65535)
			{
				wave = (ushort)(this.m_waterWaves.m_size + 1);
				this.m_waterWaves.Add(waveData);
				bool result = true;
				return result;
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterWaves);
		}
		return false;
	}

	public void ReleaseWaterWave(ushort wave)
	{
		while (!Monitor.TryEnter(this.m_waterWaves, 0))
		{
		}
		try
		{
			this.m_waterWaves.m_buffer[(int)(wave - 1)].m_type = 0;
			while (this.m_waterWaves.m_size != 0)
			{
				if (this.m_waterWaves.m_buffer[this.m_waterWaves.m_size - 1].m_type != 0)
				{
					break;
				}
				this.m_waterWaves.m_size--;
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterWaves);
		}
	}

	public void AddPollutionDisposeRate(int delta)
	{
		this.m_tempPollutionDisposeRate = Mathf.Clamp(this.m_tempPollutionDisposeRate + delta, 0, 127);
	}

	public int GetPollutionDisposeRate()
	{
		return this.m_finalPollutionDisposeRate;
	}

	public void SimulationStep(int subStep)
	{
		if (subStep != 1000)
		{
			while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
			{
			}
			try
			{
				this.m_simulationFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				if (subStep != 0 && (this.m_simulationFrameIndex & 255u) == 0u)
				{
					this.m_finalPollutionDisposeRate = this.m_tempPollutionDisposeRate;
					this.m_tempPollutionDisposeRate = 1;
				}
				Monitor.PulseAll(this.m_simulationBufferLock);
				while (this.m_simulationFrameIndex > this.m_waterFrameIndex + 1u && this.m_simulationStatus != WaterSimulation.Status.Terminated)
				{
					Monitor.Wait(this.m_simulationBufferLock, 1);
					if (Singleton<SimulationManager>.get_instance().Terminated || this.m_simulationStatus == WaterSimulation.Status.Terminated)
					{
						break;
					}
				}
			}
			finally
			{
				Monitor.Exit(this.m_simulationBufferLock);
			}
		}
	}

	public void UpdatePatch(int x, int z)
	{
		WaterSimulation.Cell[] buffer = this.m_waterBuffers[0];
		ulong[] waterExists = this.m_waterExists1;
		int num = 120;
		int num2 = z * 9 + x;
		int num3 = num * x;
		int num4 = num * (x + 1);
		int num5 = num * z;
		int num6 = num * (z + 1);
		int num7 = 128 - num >> 1;
		int xOffset = 64 - num * x - (num >> 1);
		int zOffset = 64 - num * z - (num >> 1);
		int minX = num3 - num7;
		int maxX = num4 + num7 - 1;
		int minZ = num5 - num7;
		int maxZ = num6 + num7 - 1;
		this.FillHeightMap(minX, minZ, maxX, maxZ, this.m_heightMaps[num2], ref this.m_heightBounds[num2], xOffset, zOffset, buffer, waterExists);
		this.FillHeightMap(minX, minZ, maxX, maxZ, this.m_heightMaps[num2], ref this.m_heightBounds[num2], xOffset, zOffset, buffer, waterExists);
		num7 = 64 - (num >> 1) >> 1;
		xOffset = 32 - (num >> 1) * x - (num >> 2);
		zOffset = 32 - (num >> 1) * z - (num >> 2);
		minX = Mathf.Max((num3 >> 1) - num7, 0);
		maxX = Mathf.Min((num4 >> 1) + num7 - 1, 540);
		minZ = Mathf.Max((num5 >> 1) - num7, 0);
		maxZ = Mathf.Min((num6 >> 1) + num7 - 1, 540);
		this.FillSurfaceMap(minX, minZ, maxX, maxZ, this.m_surfaceMapsA[num2], this.m_surfaceMapsB[num2], xOffset, zOffset, buffer, waterExists);
		Singleton<TerrainManager>.get_instance().m_patches[num2].m_waterInitialized = false;
		Singleton<TerrainManager>.get_instance().m_patches[num2].m_waterExists = 0;
	}

	public void EnableWaterFlow()
	{
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		try
		{
			if (this.m_simulationStatus == WaterSimulation.Status.ReadyForLoad)
			{
				this.m_simulationStatus = WaterSimulation.Status.Running;
				Monitor.PulseAll(this.m_simulationBufferLock);
			}
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
	}

	private void WaterThread()
	{
		while (true)
		{
			int pollutionDisposeRate = 0;
			while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
			{
			}
			try
			{
				while ((this.m_waterFrameIndex >= this.m_simulationFrameIndex || this.m_simulationStatus != WaterSimulation.Status.Running) && this.m_simulationStatus != WaterSimulation.Status.Terminated)
				{
					if (this.m_simulationStatus == WaterSimulation.Status.WaitingForLoad)
					{
						this.m_simulationStatus = WaterSimulation.Status.ReadyForLoad;
						Monitor.PulseAll(this.m_simulationBufferLock);
					}
					Monitor.Wait(this.m_simulationBufferLock, 1);
				}
				pollutionDisposeRate = this.m_finalPollutionDisposeRate;
				if (this.m_simulationStatus == WaterSimulation.Status.Terminated)
				{
					break;
				}
			}
			finally
			{
				Monitor.Exit(this.m_simulationBufferLock);
			}
			try
			{
				this.m_waterProfiler.BeginStep();
				try
				{
					this.SimulateWater(pollutionDisposeRate);
				}
				finally
				{
					this.m_waterProfiler.EndStep();
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	private void SimulateWater(int pollutionDisposeRate)
	{
		float currentSeaLevel = this.m_currentSeaLevel;
		float nextSeaLevel = this.m_nextSeaLevel;
		bool resetWater = this.m_resetWater;
		this.m_currentSeaLevel = nextSeaLevel;
		this.m_resetWater = false;
		int num = (int)(currentSeaLevel * 64f);
		int num2 = (int)(nextSeaLevel * 64f);
		bool flag = num2 != num || resetWater;
		float num3 = 16f;
		int num4 = 1080;
		int num5 = 120;
		float num6 = (float)num4 * num3;
		ushort[] heightBuffer = this.m_heightBuffer;
		uint num7 = this.m_waterFrameIndex & 4294967232u;
		uint waterFrameIndex = this.m_waterFrameIndex;
		uint num8 = (uint)(600 + (num4 + 1) * 4);
		uint maxProgress = 81u;
		uint num9 = 200u;
		uint num10 = 0u;
		uint num11 = ((num7 >> 6) + 2u) % 3u;
		uint num12 = (num7 >> 6) % 3u;
		uint num13 = ((num7 >> 6) + 1u) % 3u;
		ulong[] array;
		if (num11 == 0u)
		{
			array = this.m_waterExists1;
		}
		else if (num11 == 1u)
		{
			array = this.m_waterExists2;
		}
		else
		{
			array = this.m_waterExists3;
		}
		ulong[] array2;
		if (num12 == 0u)
		{
			array2 = this.m_waterExists1;
		}
		else if (num12 == 1u)
		{
			array2 = this.m_waterExists2;
		}
		else
		{
			array2 = this.m_waterExists3;
		}
		ulong[] array3;
		if (num13 == 0u)
		{
			array3 = this.m_waterExists1;
		}
		else if (num13 == 1u)
		{
			array3 = this.m_waterExists2;
		}
		else
		{
			array3 = this.m_waterExists3;
		}
		for (int i = 0; i < 64; i++)
		{
			if (array[i] != 0uL)
			{
				int num14 = Mathf.Min(this.m_bitCells, num4 + 1 - i * this.m_bitCells);
				num8 += FixedMath.NumberOfSetBits(array[i]) * (uint)num14 << 1;
			}
			array2[i] = 0uL;
		}
		for (int j = 0; j < 9; j++)
		{
			for (int k = 0; k < 9; k++)
			{
				int minX = num5 * k;
				int maxX = num5 * (k + 1);
				int minZ = num5 * j;
				int maxZ = num5 * (j + 1);
				num8 += this.CountWaterExists(minX, minZ, maxX, maxZ, array) * (uint)this.m_bitCells + 2u;
			}
		}
		WaterSimulation.Cell[] array4 = this.m_waterBuffers[(int)((UIntPtr)(~(num7 >> 6) & 1u))];
		WaterSimulation.Cell[] array5 = this.m_waterBuffers[(int)((UIntPtr)(num7 >> 6 & 1u))];
		int num15 = ((this.m_stepIndex & 3) != 0) ? 0 : 2147483647;
		int num16 = ((this.m_stepIndex & 3) != 1) ? 0 : 2147483647;
		int num17 = ((this.m_stepIndex & 3) != 2) ? 0 : 2147483647;
		int num18 = ((this.m_stepIndex & 3) != 3) ? 0 : 2147483647;
		int num19 = ((this.m_stepIndex & 3) > 1) ? 0 : 2147483647;
		ushort num20 = ((this.m_stepIndex & 3) != 0) ? 0 : 1;
		ushort num21 = ((this.m_stepIndex & 15) >= pollutionDisposeRate) ? 0 : 1;
		Randomizer randomizer;
		randomizer..ctor(this.m_stepIndex);
		this.m_stepIndex++;
		int num22 = 11;
		uint num23 = 1u << num22;
		int num24 = (int)(num23 - 1u);
		this.m_tsunamiWaves.Clear();
		this.m_impactWaves.Clear();
		while (!Monitor.TryEnter(this.m_waterWaves, 0))
		{
		}
		try
		{
			for (int l = 0; l < this.m_waterWaves.m_size; l++)
			{
				ushort type = this.m_waterWaves.m_buffer[l].m_type;
				if (type == 1)
				{
					this.m_tsunamiWaves.Add(this.m_waterWaves.m_buffer[l]);
					this.m_waterWaves.m_buffer[l].m_currentTime = (ushort)Mathf.Min((int)(this.m_waterWaves.m_buffer[l].m_currentTime + 64), 65535);
				}
				else if (type == 2)
				{
					this.m_impactWaves.Add(this.m_waterWaves.m_buffer[l]);
					this.m_waterWaves.m_buffer[l].m_currentTime = (ushort)Mathf.Min((int)(this.m_waterWaves.m_buffer[l].m_currentTime + 64), 65535);
					if (this.m_waterWaves.m_buffer[l].m_currentTime > this.m_waterWaves.m_buffer[l].m_duration)
					{
						this.ReleaseWaterWave((ushort)(l + 1));
					}
				}
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterWaves);
		}
		for (int m = -5; m <= num4; m++)
		{
			int num25 = (m + 5 + num5) / num5 - 1;
			if (num25 == (m + 4 + num5) / num5 && num25 < 9)
			{
				for (int n = 0; n < 9; n++)
				{
					if (this.SetCurrentWaterFrame(num7, num9, num10, num8, maxProgress, true))
					{
						return;
					}
					int num26 = num25 * 9 + n;
					int num27 = num5 * n;
					int num28 = num5 * (n + 1);
					int num29 = num5 * num25;
					int num30 = num5 * (num25 + 1);
					num9 += this.CountWaterExists(num27, num29, num28, num30, array) * (uint)this.m_bitCells + 2u;
					num10 += 1u;
					if (this.WaterExists(num27, num29, num28, num30))
					{
						int num31 = 128 - num5 >> 1;
						int xOffset = 64 - num5 * n - (num5 >> 1);
						int zOffset = 64 - num5 * num25 - (num5 >> 1);
						int minX2 = num27 - num31;
						int maxX2 = num28 + num31 - 1;
						int minZ2 = num29 - num31;
						int maxZ2 = num30 + num31 - 1;
						this.FillHeightMap(minX2, minZ2, maxX2, maxZ2, this.m_heightMaps[num26], ref this.m_heightBounds[num26], xOffset, zOffset, array4, array);
						num31 = 64 - (num5 >> 1) >> 1;
						xOffset = 32 - (num5 >> 1) * n - (num5 >> 2);
						zOffset = 32 - (num5 >> 1) * num25 - (num5 >> 2);
						minX2 = Mathf.Max((num27 >> 1) - num31, 0);
						maxX2 = Mathf.Min((num28 >> 1) + num31 - 1, 540);
						minZ2 = Mathf.Max((num29 >> 1) - num31, 0);
						maxZ2 = Mathf.Min((num30 >> 1) + num31 - 1, 540);
						this.FillSurfaceMap(minX2, minZ2, maxX2, maxZ2, this.m_surfaceMapsA[num26], this.m_surfaceMapsB[num26], xOffset, zOffset, array4, array);
					}
				}
			}
			num25 = m + 3;
			if (num25 >= 0 && num25 <= num4)
			{
				if (this.SetCurrentWaterFrame(num7, num9, num10, num8, maxProgress, false))
				{
					return;
				}
				num9 += FixedMath.NumberOfSetBits(array[num25 / this.m_bitCells]) + 2u;
				ulong num32 = array[Mathf.Max((num25 - 2) / this.m_bitCells, 0)] | array[Mathf.Min((num25 + 2) / this.m_bitCells, 63)] | array3[Mathf.Max((num25 - 2) / this.m_bitCells, 0)] | array3[Mathf.Min((num25 + 2) / this.m_bitCells, 63)];
				int num33 = 0;
				while (num33 < num4)
				{
					int num34 = num33;
					while (num34 <= num4 && (num32 & 1uL) == 0uL)
					{
						num34 += this.m_bitCells;
						num32 >>= 1;
					}
					num33 = num34;
					while (num33 <= num4 && (num32 & 1uL) != 0uL)
					{
						num33 += this.m_bitCells;
						num32 >>= 1;
					}
					int num35 = Mathf.Max(num34 - 2, 0);
					int num36 = Mathf.Min(num33 + 1, num4);
					if (num35 > num36)
					{
						break;
					}
					int num37 = num25 * (num4 + 1) + num35;
					WaterSimulation.Cell cell = default(WaterSimulation.Cell);
					WaterSimulation.Cell cell2 = array4[num37];
					int num38 = (int)heightBuffer[num37];
					if (cell2.m_height != 0)
					{
						cell2.m_height -= num20;
					}
					if (cell2.m_pollution != 0)
					{
						cell2.m_pollution -= num21;
					}
					if (cell2.m_pollution > cell2.m_height)
					{
						cell2.m_pollution = cell2.m_height;
					}
					for (int num39 = num35; num39 <= num36; num39++)
					{
						WaterSimulation.Cell cell3 = default(WaterSimulation.Cell);
						int num40 = 0;
						int num41 = 0;
						int num42 = 0;
						for (int num43 = 0; num43 < this.m_impactWaves.m_size; num43++)
						{
							if (num39 >= (int)this.m_impactWaves[num43].m_minX && num39 <= (int)(this.m_impactWaves[num43].m_maxX + 1) && num25 >= (int)this.m_impactWaves[num43].m_minZ && num25 <= (int)(this.m_impactWaves[num43].m_maxZ + 1))
							{
								int num44 = 1 + Mathf.Max((int)(this.m_impactWaves[num43].m_maxX - this.m_impactWaves[num43].m_origX), (int)(this.m_impactWaves[num43].m_origX - this.m_impactWaves[num43].m_minX));
								num44 *= num44;
								int num45 = num39 - (int)this.m_impactWaves[num43].m_origX;
								int num46 = num25 - (int)this.m_impactWaves[num43].m_origZ;
								int num47 = num45 + 1;
								int num48 = num46 + 1;
								int num49 = num45 * num45 + num46 * num46;
								int num50 = num47 * num47 + num46 * num46;
								int num51 = num45 * num45 + num48 * num48;
								if (num49 < num44)
								{
									int num52 = (int)this.m_impactWaves[num43].m_delta;
									num52 -= num52 * num49 / num44;
									num41 += num52;
									num42 += num52;
								}
								if (num50 < num44)
								{
									int num53 = (int)this.m_impactWaves[num43].m_delta;
									num53 -= num53 * num50 / num44;
									num41 -= num53;
								}
								if (num51 < num44)
								{
									int num54 = (int)this.m_impactWaves[num43].m_delta;
									num54 -= num54 * num51 / num44;
									num42 -= num54;
								}
							}
						}
						if (num39 != num4)
						{
							cell3 = array4[num37 + 1];
							num40 = (int)heightBuffer[num37 + 1];
							if (cell3.m_height != 0)
							{
								cell3.m_height -= num20;
							}
							if (cell3.m_pollution != 0)
							{
								cell3.m_pollution -= num21;
							}
							if (cell3.m_pollution > cell3.m_height)
							{
								cell3.m_pollution = cell3.m_height;
							}
							int num55 = num38 + num41 + (int)cell2.m_height - num40 - (int)cell3.m_height;
							int num56 = (int)cell2.m_velocityX;
							if (num56 > 0)
							{
								num56 = randomizer.Int32(num23) + num56 * num24 >> num22;
							}
							if (num56 < 0)
							{
								num56 = -(randomizer.Int32(num23) - num56 * num24 >> num22);
							}
							if (num55 > 0)
							{
								num56 += num55 + randomizer.Int32(4u) >> 2;
							}
							else if (num55 < 0)
							{
								num56 -= randomizer.Int32(4u) - num55 >> 2;
							}
							if (num56 > (int)cell2.m_height)
							{
								num56 = (int)cell2.m_height;
							}
							if (num56 < (int)(-(int)cell3.m_height))
							{
								num56 = (int)(-(int)cell3.m_height);
							}
							cell2.m_velocityX = (short)Mathf.Clamp(num56, -32766, 32766);
						}
						if (num25 != num4)
						{
							WaterSimulation.Cell cell4 = array4[num37 + num4 + 1];
							int num57 = (int)heightBuffer[num37 + num4 + 1];
							if (cell4.m_height != 0)
							{
								cell4.m_height -= num20;
							}
							int num58 = num38 + num42 + (int)cell2.m_height - num57 - (int)cell4.m_height;
							int num59 = (int)cell2.m_velocityZ;
							if (num59 > 0)
							{
								num59 = randomizer.Int32(num23) + num59 * num24 >> num22;
							}
							if (num59 < 0)
							{
								num59 = -(randomizer.Int32(num23) - num59 * num24 >> num22);
							}
							if (num58 > 0)
							{
								num59 += num58 + randomizer.Int32(4u) >> 2;
							}
							else if (num58 < 0)
							{
								num59 -= randomizer.Int32(4u) - num58 >> 2;
							}
							if (num59 > (int)cell2.m_height)
							{
								num59 = (int)cell2.m_height;
							}
							if (num59 < (int)(-(int)cell4.m_height))
							{
								num59 = (int)(-(int)cell4.m_height);
							}
							cell2.m_velocityZ = (short)Mathf.Clamp(num59, -32766, 32766);
						}
						WaterSimulation.Cell cell5 = default(WaterSimulation.Cell);
						if (num25 != 0)
						{
							cell5 = array5[num37 - num4 - 1];
						}
						int num60 = 0;
						int num61 = 0;
						if (cell.m_velocityX < 0)
						{
							num60 -= (int)cell.m_velocityX;
						}
						else
						{
							num61 += (int)cell.m_velocityX;
						}
						if (cell5.m_velocityZ < 0)
						{
							num60 -= (int)cell5.m_velocityZ;
						}
						else
						{
							num61 += (int)cell5.m_velocityZ;
						}
						if (cell2.m_velocityX > 0)
						{
							num60 += (int)cell2.m_velocityX;
						}
						else
						{
							num61 -= (int)cell2.m_velocityX;
						}
						if (cell2.m_velocityZ > 0)
						{
							num60 += (int)cell2.m_velocityZ;
						}
						else
						{
							num61 -= (int)cell2.m_velocityZ;
						}
						if (num60 > (int)cell2.m_height)
						{
							if (cell.m_velocityX < 0)
							{
								cell.m_velocityX = (short)(-(short)(((num60 - 1 & num15) - (int)(cell.m_velocityX * (short)cell2.m_height)) / num60));
								array5[num37 - 1] = cell;
							}
							if (cell5.m_velocityZ < 0)
							{
								cell5.m_velocityZ = (short)(-(short)(((num60 - 1 & num17) - (int)(cell5.m_velocityZ * (short)cell2.m_height)) / num60));
								array5[num37 - num4 - 1] = cell5;
							}
							if (cell2.m_velocityX > 0)
							{
								cell2.m_velocityX = (short)(((num60 - 1 & num16) + (int)(cell2.m_velocityX * (short)cell2.m_height)) / num60);
							}
							if (cell2.m_velocityZ > 0)
							{
								cell2.m_velocityZ = (short)(((num60 - 1 & num18) + (int)(cell2.m_velocityZ * (short)cell2.m_height)) / num60);
							}
						}
						if (num61 > (int)(65535 - cell2.m_height))
						{
							if (cell.m_velocityX > 0)
							{
								cell.m_velocityX = (short)(((num61 - 1 & num15) + (int)(cell.m_velocityX * (short)(65535 - cell2.m_height))) / num61);
								array5[num37 - 1] = cell;
							}
							if (cell5.m_velocityZ > 0)
							{
								cell5.m_velocityZ = (short)(((num61 - 1 & num17) + (int)(cell5.m_velocityZ * (short)(65535 - cell2.m_height))) / num61);
								array5[num37 - num4 - 1] = cell5;
							}
							if (cell2.m_velocityX < 0)
							{
								cell2.m_velocityX = (short)(-(short)(((num61 - 1 & num16) - (int)(cell2.m_velocityX * (short)(65535 - cell2.m_height))) / num61));
							}
							if (cell2.m_velocityZ < 0)
							{
								cell2.m_velocityZ = (short)(-(short)(((num61 - 1 & num18) - (int)(cell2.m_velocityZ * (short)(65535 - cell2.m_height))) / num61));
							}
						}
						array5[num37] = cell2;
						cell = cell2;
						cell2 = cell3;
						num38 = num40;
						num37++;
					}
				}
			}
			num25 = m;
			if (num25 >= 0 && num25 <= num4)
			{
				if (this.SetCurrentWaterFrame(num7, num9, num10, num8, maxProgress, false))
				{
					return;
				}
				num9 += FixedMath.NumberOfSetBits(array[num25 / this.m_bitCells]) + 2u;
				ulong num62 = array[Mathf.Max((num25 - 1) / this.m_bitCells, 0)] | array[Mathf.Min((num25 + 1) / this.m_bitCells, 63)];
				int num63 = 0;
				while (num63 < num4)
				{
					int num64 = num63;
					while (num64 <= num4 && (num62 & 1uL) == 0uL)
					{
						num64 += this.m_bitCells;
						num62 >>= 1;
					}
					num63 = num64;
					while (num63 <= num4 && (num62 & 1uL) != 0uL)
					{
						num63 += this.m_bitCells;
						num62 >>= 1;
					}
					int num65 = Mathf.Max(num64 - 1, 0);
					int num66 = Mathf.Min(num63, num4);
					if (num65 > num66)
					{
						break;
					}
					int num67 = num25 * (num4 + 1) + num65;
					for (int num68 = num65; num68 <= num66; num68++)
					{
						WaterSimulation.Cell cell6 = array5[num67];
						if (num68 != num4)
						{
							if (cell6.m_velocityX > 0)
							{
								WaterSimulation.Cell cell7 = array5[num67 + 1];
								int velocityX = (int)cell6.m_velocityX;
								if (cell6.m_pollution != 0 && cell6.m_height != 0)
								{
									int num69 = (velocityX * (int)cell6.m_pollution + ((int)(cell6.m_height - 1) & num19)) / (int)cell6.m_height;
									cell6.m_pollution = (ushort)((int)cell6.m_pollution - num69);
									cell7.m_pollution = (ushort)((int)cell7.m_pollution + num69);
								}
								cell6.m_height = (ushort)Mathf.Max((int)cell6.m_height - velocityX, 0);
								cell7.m_height = (ushort)Mathf.Min((int)cell7.m_height + velocityX, 65535);
								array5[num67] = cell6;
								array5[num67 + 1] = cell7;
								array2[num25 / this.m_bitCells] |= 1uL << (num68 + 1) / this.m_bitCells;
							}
							else if (cell6.m_velocityX < 0)
							{
								WaterSimulation.Cell cell8 = array5[num67 + 1];
								int num70 = (int)(-(int)cell6.m_velocityX);
								if (cell8.m_pollution != 0 && cell8.m_height != 0)
								{
									int num71 = (num70 * (int)cell8.m_pollution + ((int)(cell8.m_height - 1) & num19)) / (int)cell8.m_height;
									cell6.m_pollution = (ushort)((int)cell6.m_pollution + num71);
									cell8.m_pollution = (ushort)((int)cell8.m_pollution - num71);
								}
								cell6.m_height = (ushort)Mathf.Min((int)cell6.m_height + num70, 65535);
								cell8.m_height = (ushort)Mathf.Max((int)cell8.m_height - num70, 0);
								array5[num67] = cell6;
								array5[num67 + 1] = cell8;
							}
						}
						if (num25 != num4)
						{
							if (cell6.m_velocityZ > 0)
							{
								WaterSimulation.Cell cell9 = array5[num67 + num4 + 1];
								int velocityZ = (int)cell6.m_velocityZ;
								if (cell6.m_pollution != 0 && cell6.m_height != 0)
								{
									int num72 = (velocityZ * (int)cell6.m_pollution + ((int)(cell6.m_height - 1) & num19)) / (int)cell6.m_height;
									cell6.m_pollution = (ushort)((int)cell6.m_pollution - num72);
									cell9.m_pollution = (ushort)((int)cell9.m_pollution + num72);
								}
								cell6.m_height = (ushort)Mathf.Max((int)cell6.m_height - velocityZ, 0);
								cell9.m_height = (ushort)Mathf.Min((int)cell9.m_height + velocityZ, 65535);
								array5[num67] = cell6;
								array5[num67 + num4 + 1] = cell9;
								array2[(num25 + 1) / this.m_bitCells] |= 1uL << num68 / this.m_bitCells;
							}
							else if (cell6.m_velocityZ < 0)
							{
								WaterSimulation.Cell cell10 = array5[num67 + num4 + 1];
								int num73 = (int)(-(int)cell6.m_velocityZ);
								if (cell10.m_pollution != 0 && cell10.m_height != 0)
								{
									int num74 = (num73 * (int)cell10.m_pollution + ((int)(cell10.m_height - 1) & num19)) / (int)cell10.m_height;
									cell6.m_pollution = (ushort)((int)cell6.m_pollution + num74);
									cell10.m_pollution = (ushort)((int)cell10.m_pollution - num74);
								}
								cell6.m_height = (ushort)Mathf.Min((int)cell6.m_height + num73, 65535);
								cell10.m_height = (ushort)Mathf.Max((int)cell10.m_height - num73, 0);
								array5[num67] = cell6;
								array5[num67 + num4 + 1] = cell10;
							}
						}
						if (cell6.m_height != 0)
						{
							array2[num25 / this.m_bitCells] |= 1uL << num68 / this.m_bitCells;
						}
						num67++;
					}
				}
				if (flag)
				{
					int num75 = num25 * (num4 + 1);
					for (int num76 = 0; num76 <= num4; num76++)
					{
						WaterSimulation.Cell cell11 = array5[num75];
						int num77 = (int)heightBuffer[num75];
						int num78 = num77 + (int)cell11.m_height;
						if (cell11.m_height == 0 || resetWater)
						{
							int num79 = num2 - num77;
							if (num79 > 0)
							{
								cell11.m_height = (ushort)Mathf.Min(num79, 65535);
								array2[num25 / this.m_bitCells] |= 1uL << num76 / this.m_bitCells;
							}
							else
							{
								cell11 = default(WaterSimulation.Cell);
							}
						}
						else
						{
							int num80 = num2 - num;
							if (num78 > num + 128)
							{
								if (num80 < 0)
								{
									num80 += num78 - num - 128;
									if (num80 > 0)
									{
										num80 = 0;
									}
								}
								else
								{
									num80 -= num78 - num - 128;
									if (num80 < 0)
									{
										num80 = 0;
									}
								}
							}
							int num81 = (int)cell11.m_height + num80;
							if (num81 > 0)
							{
								cell11.m_height = (ushort)Mathf.Min(num81, 65535);
							}
							else
							{
								cell11 = default(WaterSimulation.Cell);
							}
						}
						array5[num75] = cell11;
						num75++;
					}
				}
				int num82 = num25 * (num4 + 1);
				int num83 = (num25 != 0 && num25 != num4) ? num4 : 1;
				for (int num84 = 0; num84 <= num4; num84 += num83)
				{
					int num85 = num2;
					for (int num86 = 0; num86 < this.m_tsunamiWaves.m_size; num86++)
					{
						num85 = this.m_tsunamiWaves.m_buffer[num86].GetSeaLevel(num85, num84, num25);
					}
					WaterSimulation.Cell cell12 = array5[num82];
					int num87 = (int)(heightBuffer[num82] + cell12.m_height) - num85;
					if (num87 > 0 && cell12.m_height != 0)
					{
						if (num87 > (int)cell12.m_height)
						{
							num87 = (int)cell12.m_height;
						}
						if (cell12.m_pollution != 0)
						{
							int num88 = (num87 * (int)cell12.m_pollution + ((int)(cell12.m_height - 1) & num19)) / (int)cell12.m_height;
							cell12.m_pollution = (ushort)((int)cell12.m_pollution - num88);
						}
						cell12.m_height = (ushort)((int)cell12.m_height - num87);
						array5[num82] = cell12;
					}
					else if (num87 < 0)
					{
						cell12.m_height = (ushort)((int)cell12.m_height - num87);
						array5[num82] = cell12;
						array2[num25 / this.m_bitCells] |= 1uL << num84 / this.m_bitCells;
					}
					num82 += num83;
				}
			}
		}
		if (this.SetCurrentWaterFrame(num7, num9, num10, num8, maxProgress, false))
		{
			return;
		}
		num9 += 400u;
		while (!Monitor.TryEnter(this.m_waterSources, 0))
		{
		}
		try
		{
			for (int num89 = 0; num89 < this.m_waterSources.m_size; num89++)
			{
				WaterSource value = this.m_waterSources[num89];
				if (value.m_type != 0)
				{
					bool flag2 = value.m_type == 1;
					value.m_flow = 0u;
					if (flag2)
					{
						value.m_water = 0u;
						value.m_pollution = 0u;
					}
					int num90 = (int)value.m_inputRate;
					if (num90 > 0)
					{
						float num91 = Mathf.Sqrt((float)num90) * 0.4f + 10f;
						if (value.m_type == 2 || value.m_type == 3)
						{
							num91 = Mathf.Min(50f, num91);
						}
						int num92 = Mathf.Max(Mathf.CeilToInt((value.m_inputPosition.x - num91 + num6 * 0.5f) / num3), 0);
						int num93 = Mathf.Max(Mathf.CeilToInt((value.m_inputPosition.z - num91 + num6 * 0.5f) / num3), 0);
						int num94 = Mathf.Min(Mathf.FloorToInt((value.m_inputPosition.x + num91 + num6 * 0.5f) / num3), num4);
						int num95 = Mathf.Min(Mathf.FloorToInt((value.m_inputPosition.z + num91 + num6 * 0.5f) / num3), num4);
						int num96 = 0;
						for (int num97 = num93; num97 <= num95; num97++)
						{
							float num98 = (float)num97 * num3 - num6 * 0.5f - value.m_inputPosition.z;
							for (int num99 = num92; num99 <= num94; num99++)
							{
								float num100 = (float)num99 * num3 - num6 * 0.5f - value.m_inputPosition.x;
								if (num98 * num98 + num100 * num100 < num91 * num91)
								{
									int num101 = num97 * (num4 + 1) + num99;
									WaterSimulation.Cell cell13 = array5[num101];
									int num102 = (int)heightBuffer[num101];
									int num103 = Mathf.Max((int)value.m_target, num102);
									num96 += Mathf.Min(num102 + (int)cell13.m_height - num103, (int)cell13.m_height);
								}
							}
						}
						if (flag2)
						{
							num90 = Mathf.Min(num90, num96 >> 1);
						}
						else
						{
							num90 = Mathf.Min(num90, num96);
						}
						if (num90 > 0)
						{
							for (int num104 = num93; num104 <= num95; num104++)
							{
								float num105 = (float)num104 * num3 - num6 * 0.5f - value.m_inputPosition.z;
								for (int num106 = num92; num106 <= num94; num106++)
								{
									float num107 = (float)num106 * num3 - num6 * 0.5f - value.m_inputPosition.x;
									if (num105 * num105 + num107 * num107 < num91 * num91)
									{
										int num108 = num104 * (num4 + 1) + num106;
										WaterSimulation.Cell cell14 = array5[num108];
										int num109 = (int)heightBuffer[num108];
										int num110 = Mathf.Max((int)value.m_target, num109);
										int num111 = Mathf.Min(num109 + (int)cell14.m_height - num110, (int)cell14.m_height);
										num111 = (num111 * num90 + num96 - 1) / num96;
										if (num111 > 0)
										{
											if (cell14.m_pollution != 0 && cell14.m_height != 0)
											{
												int num112 = (num111 * (int)cell14.m_pollution + ((int)(cell14.m_height - 1) & num19)) / (int)cell14.m_height;
												cell14.m_pollution = (ushort)((int)cell14.m_pollution - num112);
												value.m_pollution += (uint)num112;
											}
											value.m_water += (uint)num111;
											cell14.m_height = (ushort)((int)cell14.m_height - num111);
											array5[num108] = cell14;
										}
									}
								}
							}
						}
					}
					if (value.m_type == 3)
					{
						uint num113 = value.m_water >> 3;
						if (num113 > value.m_pollution)
						{
							num113 = value.m_pollution;
						}
						value.m_pollution -= num113;
					}
					if (value.m_pollution > value.m_water)
					{
						value.m_pollution = value.m_water;
					}
					int num114;
					int num115;
					if (flag2)
					{
						num114 = (int)value.m_outputRate;
						num115 = 0;
					}
					else
					{
						num114 = Mathf.Min((int)value.m_outputRate, (int)value.m_water);
						if (value.m_water != 0u)
						{
							num115 = (int)((ulong)value.m_pollution * (ulong)((long)num114) / (ulong)value.m_water);
						}
						else
						{
							num115 = 0;
						}
					}
					if (num114 > 0)
					{
						float num116;
						if (value.m_type == 2 || value.m_type == 3)
						{
							num116 = Mathf.Clamp(Mathf.Sqrt((float)num114) * 0.4f, 10f, 50f);
						}
						else
						{
							num116 = Mathf.Sqrt((float)num114) * 0.4f + 10f;
						}
						int num117 = Mathf.Max(Mathf.CeilToInt((value.m_outputPosition.x - num116 + num6 * 0.5f) / num3), 0);
						int num118 = Mathf.Max(Mathf.CeilToInt((value.m_outputPosition.z - num116 + num6 * 0.5f) / num3), 0);
						int num119 = Mathf.Min(Mathf.FloorToInt((value.m_outputPosition.x + num116 + num6 * 0.5f) / num3), num4);
						int num120 = Mathf.Min(Mathf.FloorToInt((value.m_outputPosition.z + num116 + num6 * 0.5f) / num3), num4);
						int num121 = 0;
						int num122 = 0;
						for (int num123 = num118; num123 <= num120; num123++)
						{
							float num124 = (float)num123 * num3 - num6 * 0.5f - value.m_outputPosition.z;
							for (int num125 = num117; num125 <= num119; num125++)
							{
								float num126 = (float)num125 * num3 - num6 * 0.5f - value.m_outputPosition.x;
								if (num124 * num124 + num126 * num126 < num116 * num116)
								{
									int num127 = num123 * (num4 + 1) + num125;
									WaterSimulation.Cell cell15 = array5[num127];
									int num128 = (int)heightBuffer[num127];
									if (!flag2 || num128 < (int)value.m_target)
									{
										int num129 = Mathf.Max((int)value.m_target, num128);
										num121 += Mathf.Min(num128 + (int)cell15.m_height - num129, (int)cell15.m_height);
										num122++;
									}
								}
							}
						}
						if (flag2)
						{
							num114 = Mathf.Min(num114, -(num121 >> 1));
						}
						if (num114 > 0 && num122 > 0)
						{
							for (int num130 = num118; num130 <= num120; num130++)
							{
								float num131 = (float)num130 * num3 - num6 * 0.5f - value.m_outputPosition.z;
								for (int num132 = num117; num132 <= num119; num132++)
								{
									float num133 = (float)num132 * num3 - num6 * 0.5f - value.m_outputPosition.x;
									if (num131 * num131 + num133 * num133 < num116 * num116)
									{
										int num134 = num130 * (num4 + 1) + num132;
										WaterSimulation.Cell cell16 = array5[num134];
										if (!flag2 || heightBuffer[num134] < value.m_target)
										{
											int num135 = (num114 + (num122 >> 1)) / num122;
											if (num135 > (int)(65535 - cell16.m_height))
											{
												num135 = (int)(65535 - cell16.m_height);
											}
											if (num135 > 0)
											{
												if (num115 != 0)
												{
													int num136 = (num115 + (num122 >> 1)) / num122;
													cell16.m_pollution = (ushort)Mathf.Min(65535, (int)cell16.m_pollution + num136);
													value.m_pollution = (uint)Mathf.Max(0, (int)(value.m_pollution - (uint)num136));
												}
												cell16.m_height = (ushort)Mathf.Min(65535, (int)cell16.m_height + num135);
												value.m_water = (uint)Mathf.Max(0, (int)(value.m_water - (uint)num135));
												value.m_flow += (uint)num135;
												array5[num134] = cell16;
												if (cell16.m_height != 0)
												{
													array2[num130 / this.m_bitCells] |= 1uL << num132 / this.m_bitCells;
												}
											}
										}
									}
								}
							}
						}
					}
					if (waterFrameIndex < this.m_waterFrameIndex)
					{
						this.m_waterSources[num89] = value;
					}
				}
			}
		}
		finally
		{
			Monitor.Exit(this.m_waterSources);
		}
		this.SetCurrentWaterFrame(num7, num9, num10, num8, maxProgress, false);
	}

	private bool SetCurrentWaterFrame(uint startFrame, uint waterProgress, uint waterProgress2, uint maxProgress, uint maxProgress2, bool waitRendering)
	{
		uint num = startFrame + (waterProgress << 6) / maxProgress;
		uint num2 = startFrame + (waterProgress2 << 6) / maxProgress2;
		if (num2 > num + 28u)
		{
			num = num2 - 28u;
		}
		while (!Monitor.TryEnter(this.m_simulationBufferLock, 0))
		{
		}
		bool result;
		try
		{
			if (num != this.m_waterFrameIndex)
			{
				if ((num & 63u) == 0u)
				{
					while (this.m_readLockCount != 0)
					{
						this.m_waterProfiler.PauseStep();
						Monitor.Wait(this.m_simulationBufferLock, 1);
						this.m_waterProfiler.ContinueStep();
					}
				}
				this.m_waterFrameIndex = num;
				Monitor.PulseAll(this.m_simulationBufferLock);
			}
			if (num2 != this.m_waterFrameIndex2)
			{
				this.m_waterFrameIndex2 = num2;
				Monitor.PulseAll(this.m_simulationBufferLock);
			}
			while (this.m_simulationStatus != WaterSimulation.Status.Terminated && this.m_simulationStatus != WaterSimulation.Status.ReadyForLoad)
			{
				if (this.m_simulationStatus == WaterSimulation.Status.WaitingForLoad)
				{
					this.m_simulationStatus = WaterSimulation.Status.ReadyForLoad;
					Monitor.PulseAll(this.m_simulationBufferLock);
					result = true;
					return result;
				}
				if (!waitRendering)
				{
					result = false;
					return result;
				}
				if (this.m_waterFrameIndex2 + 4u < this.m_renderingFrameIndex)
				{
					result = false;
					return result;
				}
				this.m_waterProfiler.PauseStep();
				Monitor.Wait(this.m_simulationBufferLock, 1);
				this.m_waterProfiler.ContinueStep();
			}
			result = true;
		}
		finally
		{
			Monitor.Exit(this.m_simulationBufferLock);
		}
		return result;
	}

	private uint CountWaterExists(int minX, int minZ, int maxX, int maxZ, ulong[] waterExists)
	{
		minX = Mathf.Max((minX - 1) / this.m_bitCells, 0);
		minZ = Mathf.Max((minZ - 1) / this.m_bitCells, 0);
		maxX = Mathf.Min((maxX + 2) / this.m_bitCells, 63);
		maxZ = Mathf.Min((maxZ + 2) / this.m_bitCells, 63);
		ulong num = 18446744073709551615uL << minX & ~(18446744073709551614uL << maxX);
		uint num2 = 0u;
		for (int i = minZ; i <= maxZ; i++)
		{
			ulong num3 = waterExists[i] & num;
			if (num3 != 0uL)
			{
				num2 += FixedMath.NumberOfSetBits(num3);
			}
		}
		return num2;
	}

	private void FillHeightMap(int minX, int minZ, int maxX, int maxZ, byte[] target, ref Vector4 heightBounds, int xOffset, int zOffset, WaterSimulation.Cell[] buffer, ulong[] waterExists)
	{
		int num = 1080;
		int num2 = 128;
		int num3 = 65535;
		int num4 = 0;
		for (int i = minZ; i <= maxZ; i++)
		{
			ulong num5 = waterExists[Mathf.Max((i - 2) / this.m_bitCells, 0)] | waterExists[Mathf.Min((i + 3) / this.m_bitCells, 63)];
			int num6 = Mathf.Clamp(minX, 0, num);
			int num7 = Mathf.Clamp(i, 0, num);
			int num8 = Mathf.Clamp(i - 1, 0, num);
			int num9 = Mathf.Clamp(i + 1, 0, num);
			int num10 = num7 * (num + 1) + num6;
			int num11 = (int)this.m_heightBuffer[num10];
			WaterSimulation.Cell cell = buffer[num10];
			int num12 = 0;
			int num13 = 0;
			if (cell.m_height != 0)
			{
				num12 = num11 + (int)cell.m_height;
				num13 = -1;
			}
			else
			{
				num10 = num8 * (num + 1) + num6;
				cell = buffer[num10];
				if (cell.m_height != 0)
				{
					num12 += (int)(this.m_heightBuffer[num10] + cell.m_height);
					num13++;
				}
				num10 = num9 * (num + 1) + num6;
				cell = buffer[num10];
				if (cell.m_height != 0)
				{
					num12 += (int)(this.m_heightBuffer[num10] + cell.m_height);
					num13++;
				}
				if (num13 != 0)
				{
					num12 = Mathf.Min(num11, num12 / num13);
				}
				else
				{
					num12 = num11;
				}
			}
			int num14 = num12;
			int num15 = num13;
			for (int j = minX; j <= maxX; j++)
			{
				ulong num16 = 18446744073709551615uL << Mathf.Max((j - 2) / this.m_bitCells, 0) & ~(18446744073709551614uL << Mathf.Min((j + 3) / this.m_bitCells, 63));
				if ((num16 & num5) != 0uL)
				{
					num6 = Mathf.Clamp(j + 1, 0, num);
					num10 = num7 * (num + 1) + num6;
					int num17 = (int)this.m_heightBuffer[num10];
					cell = buffer[num10];
					int num18 = 0;
					int num19 = 0;
					if (cell.m_height != 0)
					{
						num18 = num17 + (int)cell.m_height;
						num19 = -1;
					}
					else
					{
						num10 = num8 * (num + 1) + num6;
						cell = buffer[num10];
						if (cell.m_height != 0)
						{
							num18 += (int)(this.m_heightBuffer[num10] + cell.m_height);
							num19++;
						}
						num10 = num9 * (num + 1) + num6;
						cell = buffer[num10];
						if (cell.m_height != 0)
						{
							num18 += (int)(this.m_heightBuffer[num10] + cell.m_height);
							num19++;
						}
						if (num19 != 0)
						{
							num18 = Mathf.Min(num17, num18 / num19);
						}
						else
						{
							num18 = num17;
						}
					}
					int num20;
					bool flag;
					if (num13 == -1)
					{
						num20 = num12;
						flag = true;
					}
					else
					{
						num20 = 0;
						int num21 = 0;
						if (num15 != 0)
						{
							num20 += num14;
							num21++;
						}
						if (num13 != 0)
						{
							num20 += num12;
							num21++;
						}
						if (num19 != 0)
						{
							num20 += num18;
							num21++;
						}
						if (num21 != 0)
						{
							num20 = Mathf.Min(num11, num20 / num21);
							flag = true;
						}
						else
						{
							num20 = 0;
							flag = false;
						}
					}
					num14 = num12;
					num15 = num13;
					num12 = num18;
					num11 = num17;
					num13 = num19;
					num20 = Mathf.Clamp(num20 - 20, 0, 65535);
					if (flag)
					{
						if (num20 < num3)
						{
							num3 = num20;
						}
						if (num20 > num4)
						{
							num4 = num20;
						}
					}
					num10 = (i + zOffset) * num2 + j + xOffset << 2;
					target[num10 + 1] = target[num10 + 3];
					target[num10 + 2] = target[num10];
					target[num10 + 3] = (byte)(num20 >> 8);
					target[num10] = (byte)(num20 & 255);
				}
				else
				{
					num14 = num12;
					num15 = num13;
					num12 = 0;
					num13 = 0;
					num10 = (i + zOffset) * num2 + j + xOffset << 2;
					target[num10 + 1] = target[num10 + 3];
					target[num10 + 2] = target[num10];
					target[num10 + 3] = 0;
					target[num10] = 0;
				}
			}
		}
		for (int k = 2; k <= 8; k <<= 1)
		{
			int num22 = ~(k - 1);
			int num23 = minX & num22;
			if (num23 < -xOffset)
			{
				num23 += k;
			}
			int num24 = maxX + k - 1 & num22;
			if (num24 >= num2 - xOffset)
			{
				num24 -= k;
			}
			int num25 = minZ & num22;
			if (num25 < -zOffset)
			{
				num25 += k;
			}
			int num26 = maxZ + k - 1 & num22;
			if (num26 >= num2 - zOffset)
			{
				num26 -= k;
			}
			for (int l = num25; l <= num26; l += k)
			{
				for (int m = num23; m <= num24; m += k)
				{
					int num27 = (l + zOffset) * num2 + m + xOffset << 2;
					if (target[num27 + 3] == 0 && target[num27] == 0)
					{
						int num28 = 0;
						int num29 = 0;
						for (int n = l - k; n <= l + k; n += k)
						{
							if (n >= 0 && n <= num)
							{
								for (int num30 = m - k; num30 <= m + k; num30 += k)
								{
									if (num30 >= 0 && num30 <= num)
									{
										int num31 = n * (num + 1) + num30;
										int height = (int)buffer[num31].m_height;
										if (height != 0)
										{
											int num32 = (int)this.m_heightBuffer[num31];
											num28 += num32 + height;
											num29++;
										}
									}
								}
							}
						}
						if (num29 != 0)
						{
							num28 /= num29;
							if (m >= 0 && m <= num && l >= 0 && l <= num)
							{
								num28 = Mathf.Min(num28, (int)this.m_heightBuffer[l * (num + 1) + m]);
							}
							else
							{
								num28 = Mathf.Min(num28, 65535);
							}
							target[num27 + 3] = (byte)(num28 >> 8);
							target[num27] = (byte)(num28 & 255);
						}
					}
				}
			}
		}
		heightBounds.x = heightBounds.z;
		heightBounds.y = heightBounds.w;
		heightBounds.z = (float)num3 * 0.015625f;
		heightBounds.w = (float)num4 * 0.015625f;
	}

	private void FillSurfaceMap(int minX, int minZ, int maxX, int maxZ, byte[] targetA, byte[] targetB, int xOffset, int zOffset, WaterSimulation.Cell[] buffer, ulong[] waterExists)
	{
		int num = 1080;
		int num2 = 64;
		for (int i = minZ; i <= maxZ; i++)
		{
			ulong num3 = waterExists[Mathf.Max(((i << 1) - 2) / this.m_bitCells, 0)] | waterExists[Mathf.Min(((i << 1) + 3) / this.m_bitCells, 63)];
			int num4 = Mathf.Max(0, i << 1);
			int num5 = Mathf.Min(num, (i << 1) + 1);
			int num6 = Mathf.Min(num, (i << 1) + 2);
			for (int j = minX; j <= maxX; j++)
			{
				ulong num7 = 18446744073709551615uL << Mathf.Max(((j << 1) - 2) / this.m_bitCells, 0) & ~(18446744073709551614uL << Mathf.Min(((j << 1) + 3) / this.m_bitCells, 63));
				if ((num7 & num3) != 0uL)
				{
					int num8 = 0;
					int num9 = 0;
					int num10 = 0;
					int num11 = 0;
					int num12 = 0;
					int num13 = 0;
					int num14 = 0;
					int num15 = 0;
					int num16 = 0;
					int num17 = 0;
					int num18 = 0;
					int num19 = 0;
					int num20 = 0;
					int num21 = 0;
					int num22 = 0;
					int num23 = 0;
					int num24 = 0;
					int num25 = Mathf.Max(0, j << 1);
					int num26 = Mathf.Min(num, (j << 1) + 1);
					int num27 = Mathf.Min(num, (j << 1) + 2);
					int num28 = num4 * (num + 1) + num25;
					WaterSimulation.Cell cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height = (int)cell.m_height;
						int num29 = (int)this.m_heightBuffer[num28] + height;
						num8 += num29;
						num14 += num29;
						num11++;
						num17++;
						num20 += Mathf.Min(1024, height) * height;
						num21 += (int)cell.m_pollution;
						num22 += (int)cell.m_velocityX << 1;
						num23 += (int)cell.m_velocityZ << 1;
						num24 += height;
					}
					num28 = num4 * (num + 1) + num26;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height2 = (int)cell.m_height;
						int num30 = height2 << 1;
						int num31 = ((int)this.m_heightBuffer[num28] << 1) + num30;
						num9 += num31;
						num14 += num31;
						num12 += 2;
						num17 += 2;
						num20 += Mathf.Min(1024, height2) * num30;
						num21 += (int)cell.m_pollution << 1;
						num22 += (int)cell.m_velocityX << 1;
						num23 += (int)cell.m_velocityZ << 2;
						num24 += num30;
					}
					num28 = num4 * (num + 1) + num27;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height3 = (int)cell.m_height;
						int num32 = (int)this.m_heightBuffer[num28] + height3;
						num10 += num32;
						num14 += num32;
						num13++;
						num17++;
						num20 += Mathf.Min(1024, height3) * height3;
						num21 += (int)cell.m_pollution;
						num23 += (int)cell.m_velocityZ << 1;
						num24 += height3;
					}
					num28 = num5 * (num + 1) + num25;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height4 = (int)cell.m_height;
						int num33 = height4 << 1;
						int num34 = ((int)this.m_heightBuffer[num28] << 1) + num33;
						num8 += num34;
						num15 += num34;
						num11 += 2;
						num18 += 2;
						num20 += Mathf.Min(1024, height4) * num33;
						num21 += (int)cell.m_pollution << 1;
						num22 += (int)cell.m_velocityX << 2;
						num23 += (int)cell.m_velocityZ << 1;
						num24 += num33;
					}
					num28 = num5 * (num + 1) + num26;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height5 = (int)cell.m_height;
						int num35 = height5 << 2;
						int num36 = ((int)this.m_heightBuffer[num28] << 2) + num35;
						num9 += num36;
						num15 += num36;
						num12 += 4;
						num18 += 4;
						num20 += Mathf.Min(1024, height5) * num35;
						num21 += (int)cell.m_pollution << 2;
						num22 += (int)cell.m_velocityX << 2;
						num23 += (int)cell.m_velocityZ << 2;
						num24 += num35;
					}
					num28 = num5 * (num + 1) + num27;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height6 = (int)cell.m_height;
						int num37 = height6 << 1;
						int num38 = ((int)this.m_heightBuffer[num28] << 1) + num37;
						num10 += num38;
						num15 += num38;
						num13 += 2;
						num18 += 2;
						num20 += Mathf.Min(1024, height6) * num37;
						num21 += (int)cell.m_pollution << 1;
						num23 += (int)cell.m_velocityZ << 1;
						num24 += num37;
					}
					num28 = num6 * (num + 1) + num25;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height7 = (int)cell.m_height;
						int num39 = (int)this.m_heightBuffer[num28] + height7;
						num8 += num39;
						num16 += num39;
						num11++;
						num19++;
						num20 += Mathf.Min(1024, height7) * height7;
						num21 += (int)cell.m_pollution;
						num22 += (int)cell.m_velocityX << 1;
						num24 += height7;
					}
					num28 = num6 * (num + 1) + num26;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height8 = (int)cell.m_height;
						int num40 = height8 << 1;
						int num41 = ((int)this.m_heightBuffer[num28] << 1) + num40;
						num9 += num41;
						num16 += num41;
						num12 += 2;
						num19 += 2;
						num20 += Mathf.Min(1024, height8) * num40;
						num21 += (int)cell.m_pollution << 1;
						num22 += (int)cell.m_velocityX << 1;
						num24 += num40;
					}
					num28 = num6 * (num + 1) + num27;
					cell = buffer[num28];
					if (cell.m_height != 0)
					{
						int height9 = (int)cell.m_height;
						int num42 = (int)this.m_heightBuffer[num28] + height9;
						num10 += num42;
						num16 += num42;
						num13++;
						num19++;
						num20 += Mathf.Min(1024, height9) * height9;
						num21 += (int)cell.m_pollution;
						num24 += height9;
					}
					int num43 = 0;
					int num44 = 0;
					if (num11 != 0 && num13 != 0)
					{
						num43 = num8 / num11 - num10 / num13 >> 3;
					}
					else if (num11 != 0 && num12 != 0)
					{
						num43 = num8 / num11 - num9 / num12 >> 2;
					}
					else if (num12 != 0 && num13 != 0)
					{
						num43 = num9 / num12 - num10 / num13 >> 2;
					}
					if (num17 != 0 && num19 != 0)
					{
						num44 = num14 / num17 - num16 / num19 >> 3;
					}
					else if (num17 != 0 && num18 != 0)
					{
						num44 = num14 / num17 - num15 / num18 >> 2;
					}
					else if (num18 != 0 && num19 != 0)
					{
						num44 = num15 / num18 - num16 / num19 >> 2;
					}
					int num45 = (int)Mathf.Sqrt((float)(num43 * num43 + 65025 + num44 * num44));
					if (num45 != 0)
					{
						num43 = num43 * 127 / num45;
						num44 = num44 * 127 / num45;
					}
					if (num24 != 0)
					{
						num20 = Mathf.Clamp(num20 / (num24 << 2), 0, 255);
						num21 = Mathf.Clamp(num21 * 255 / num24, 0, 255);
						num22 = Mathf.Clamp(num22 * 255 / num24, -127, 127);
						num23 = Mathf.Clamp(num23 * 255 / num24, -127, 127);
					}
					int num46 = ((i + zOffset) * num2 + j + xOffset) * 3;
					targetA[num46] = (byte)(num43 + 128);
					targetA[num46 + 1] = (byte)num21;
					targetA[num46 + 2] = (byte)(num44 + 128);
					targetB[num46] = (byte)(num22 + 128);
					targetB[num46 + 1] = (byte)num20;
					targetB[num46 + 2] = (byte)(num23 + 128);
				}
				else
				{
					int num47 = ((i + zOffset) * num2 + j + xOffset) * 3;
					targetA[num47] = 128;
					targetA[num47 + 1] = 0;
					targetA[num47 + 2] = 128;
					targetB[num47] = 128;
					targetB[num47 + 1] = 0;
					targetB[num47 + 2] = 128;
				}
			}
		}
	}

	public const float DEFAULT_SEA_LEVEL = 40f;

	public const float MAX_SEA_LEVEL = 500f;

	public ThreadProfiler m_waterProfiler;

	private uint m_simulationFrameIndex;

	private uint m_renderingFrameIndex;

	private uint m_waterFrameIndex;

	private uint m_waterFrameIndex2;

	private WaterSimulation.Status m_simulationStatus;

	private Thread m_simulationThread;

	private object m_simulationBufferLock;

	private ushort[] m_heightBuffer;

	private ulong[] m_waterExists1;

	private ulong[] m_waterExists2;

	private ulong[] m_waterExists3;

	private int m_bitCells;

	private int m_stepIndex;

	private int m_readLockCount;

	private int m_tempPollutionDisposeRate;

	private int m_finalPollutionDisposeRate;

	[NonSerialized]
	private WaterSimulation.Cell[][] m_waterBuffers;

	[NonSerialized]
	public byte[][] m_heightMaps;

	[NonSerialized]
	public Vector4[] m_heightBounds;

	[NonSerialized]
	public byte[][] m_surfaceMapsA;

	[NonSerialized]
	public byte[][] m_surfaceMapsB;

	[NonSerialized]
	public FastList<WaterSource> m_waterSources;

	[NonSerialized]
	public FastList<WaterWave> m_waterWaves;

	[NonSerialized]
	private FastList<WaterWave> m_tsunamiWaves;

	[NonSerialized]
	private FastList<WaterWave> m_impactWaves;

	[NonSerialized]
	public volatile float m_currentSeaLevel;

	[NonSerialized]
	public volatile float m_nextSeaLevel;

	[NonSerialized]
	public volatile bool m_resetWater;

	public enum Status
	{
		None,
		Running,
		WaitingForLoad,
		ReadyForLoad,
		Terminated
	}

	public struct Cell
	{
		public ushort m_height;

		public ushort m_pollution;

		public short m_velocityX;

		public short m_velocityZ;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "WaterSimulation");
			WaterSimulation waterSimulation = Singleton<TerrainManager>.get_instance().WaterSimulation;
			while (!Monitor.TryEnter(waterSimulation.m_simulationBufferLock, 0))
			{
			}
			try
			{
				waterSimulation.m_simulationFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				Monitor.PulseAll(waterSimulation.m_simulationBufferLock);
				while (waterSimulation.m_simulationFrameIndex > waterSimulation.m_waterFrameIndex && waterSimulation.m_simulationStatus != WaterSimulation.Status.Terminated)
				{
					Monitor.Wait(waterSimulation.m_simulationBufferLock, 1);
					if (Singleton<SimulationManager>.get_instance().Terminated || waterSimulation.m_simulationStatus == WaterSimulation.Status.Terminated)
					{
						return;
					}
				}
			}
			finally
			{
				Monitor.Exit(waterSimulation.m_simulationBufferLock);
			}
			int num = 1080;
			WaterSimulation.Cell[] array = waterSimulation.BeginRead();
			try
			{
				int num2 = array.Length;
				EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
				for (int i = 0; i < num2; i++)
				{
					uShort.Write(array[i].m_height);
				}
				uShort.EndWrite();
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
				for (int j = 0; j < num2; j++)
				{
					WaterSimulation.Cell cell = array[j];
					if (cell.m_height != 0)
					{
						uShort2.Write(cell.m_pollution);
					}
				}
				uShort2.EndWrite();
				EncodedArray.Short @short = EncodedArray.Short.BeginWrite(s);
				for (int k = 0; k <= num; k++)
				{
					for (int l = 0; l <= num; l++)
					{
						int num3 = k * (num + 1) + l;
						WaterSimulation.Cell cell2 = array[num3];
						if (cell2.m_height != 0 || (l < num && array[num3 + 1].m_height != 0))
						{
							@short.Write(cell2.m_velocityX);
						}
					}
				}
				@short.EndWrite();
				EncodedArray.Short short2 = EncodedArray.Short.BeginWrite(s);
				for (int m = 0; m <= num; m++)
				{
					for (int n = 0; n <= num; n++)
					{
						int num4 = m * (num + 1) + n;
						WaterSimulation.Cell cell3 = array[num4];
						if (cell3.m_height != 0 || (m < num && array[num4 + num + 1].m_height != 0))
						{
							short2.Write(cell3.m_velocityZ);
						}
					}
				}
				short2.EndWrite();
				s.WriteUInt16((uint)waterSimulation.m_waterSources.m_size);
				for (int num5 = 0; num5 < waterSimulation.m_waterSources.m_size; num5++)
				{
					waterSimulation.m_waterSources.m_buffer[num5].Serialize(s);
				}
				s.WriteUInt16((uint)waterSimulation.m_waterWaves.m_size);
				for (int num6 = 0; num6 < waterSimulation.m_waterWaves.m_size; num6++)
				{
					waterSimulation.m_waterWaves.m_buffer[num6].Serialize(s);
				}
				s.WriteFloat(waterSimulation.m_currentSeaLevel);
				s.WriteInt8(waterSimulation.m_tempPollutionDisposeRate);
				s.WriteInt8(waterSimulation.m_finalPollutionDisposeRate);
				s.WriteUInt32(waterSimulation.m_waterFrameIndex);
			}
			finally
			{
				waterSimulation.EndRead();
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "WaterSimulation");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "WaterSimulation");
			WaterSimulation waterSimulation = Singleton<TerrainManager>.get_instance().WaterSimulation;
			while (!Monitor.TryEnter(waterSimulation.m_simulationBufferLock, 0))
			{
			}
			try
			{
				if (waterSimulation.m_simulationStatus == WaterSimulation.Status.Running)
				{
					waterSimulation.m_simulationStatus = WaterSimulation.Status.WaitingForLoad;
					Monitor.PulseAll(waterSimulation.m_simulationBufferLock);
				}
				else if (waterSimulation.m_simulationStatus != WaterSimulation.Status.Terminated)
				{
					waterSimulation.m_simulationStatus = WaterSimulation.Status.ReadyForLoad;
				}
				while (waterSimulation.m_simulationStatus != WaterSimulation.Status.ReadyForLoad && waterSimulation.m_simulationStatus != WaterSimulation.Status.Terminated)
				{
					Monitor.Wait(waterSimulation.m_simulationBufferLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(waterSimulation.m_simulationBufferLock);
			}
			int num = 1080;
			WaterSimulation.Cell[] array = waterSimulation.m_waterBuffers[0];
			int num2 = array.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int i = 0; i < num2; i++)
			{
				array[i].m_height = uShort.Read();
			}
			uShort.EndRead();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int j = 0; j < num2; j++)
			{
				if (array[j].m_height == 0)
				{
					array[j].m_pollution = 0;
				}
				else
				{
					array[j].m_pollution = uShort2.Read();
				}
			}
			uShort2.EndRead();
			if (s.get_version() >= 42u)
			{
				EncodedArray.Short @short = EncodedArray.Short.BeginRead(s);
				for (int k = 0; k <= num; k++)
				{
					for (int l = 0; l <= num; l++)
					{
						int num3 = k * (num + 1) + l;
						if (array[num3].m_height != 0 || (l < num && array[num3 + 1].m_height != 0))
						{
							array[num3].m_velocityX = @short.Read();
						}
						else
						{
							array[num3].m_velocityX = 0;
						}
					}
				}
				@short.EndRead();
			}
			else
			{
				EncodedArray.Short short2 = EncodedArray.Short.BeginRead(s);
				for (int m = 0; m < num2; m++)
				{
					if (array[m].m_height == 0)
					{
						array[m].m_velocityX = 0;
					}
					else
					{
						array[m].m_velocityX = short2.Read();
					}
				}
				short2.EndRead();
			}
			if (s.get_version() >= 42u)
			{
				EncodedArray.Short short3 = EncodedArray.Short.BeginRead(s);
				for (int n = 0; n <= num; n++)
				{
					for (int num4 = 0; num4 <= num; num4++)
					{
						int num5 = n * (num + 1) + num4;
						if (array[num5].m_height != 0 || (n < num && array[num5 + num + 1].m_height != 0))
						{
							array[num5].m_velocityZ = short3.Read();
						}
						else
						{
							array[num5].m_velocityZ = 0;
						}
					}
				}
				short3.EndRead();
			}
			else
			{
				EncodedArray.Short short4 = EncodedArray.Short.BeginRead(s);
				for (int num6 = 0; num6 < num2; num6++)
				{
					if (array[num6].m_height == 0)
					{
						array[num6].m_velocityZ = 0;
					}
					else
					{
						array[num6].m_velocityZ = short4.Read();
					}
				}
				short4.EndRead();
			}
			uint num7 = s.ReadUInt16();
			waterSimulation.m_waterSources.Clear();
			waterSimulation.m_waterSources.EnsureCapacity((int)num7);
			for (uint num8 = 0u; num8 < num7; num8 += 1u)
			{
				WaterSource item = default(WaterSource);
				item.Deserialize(s);
				waterSimulation.m_waterSources.Add(item);
			}
			waterSimulation.m_waterWaves.Clear();
			if (s.get_version() >= 259u)
			{
				num7 = s.ReadUInt16();
				waterSimulation.m_waterWaves.EnsureCapacity((int)num7);
				for (uint num9 = 0u; num9 < num7; num9 += 1u)
				{
					WaterWave item2 = default(WaterWave);
					item2.Deserialize(s);
					waterSimulation.m_waterWaves.Add(item2);
				}
			}
			if (s.get_version() >= 131u)
			{
				waterSimulation.m_currentSeaLevel = s.ReadFloat();
			}
			else
			{
				waterSimulation.m_currentSeaLevel = 40f;
			}
			if (s.get_version() >= 147u)
			{
				waterSimulation.m_tempPollutionDisposeRate = s.ReadInt8();
				waterSimulation.m_finalPollutionDisposeRate = s.ReadInt8();
			}
			else
			{
				waterSimulation.m_tempPollutionDisposeRate = 0;
				waterSimulation.m_finalPollutionDisposeRate = 0;
			}
			if (s.get_version() >= 173u)
			{
				waterSimulation.m_waterFrameIndex = s.ReadUInt32();
				waterSimulation.m_waterFrameIndex2 = waterSimulation.m_waterFrameIndex;
			}
			waterSimulation.m_nextSeaLevel = waterSimulation.m_currentSeaLevel;
			waterSimulation.m_resetWater = false;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "WaterSimulation");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "WaterSimulation");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			int num = 1080;
			WaterSimulation waterSimulation = Singleton<TerrainManager>.get_instance().WaterSimulation;
			WaterSimulation.Cell[] array = waterSimulation.m_waterBuffers[0];
			for (int i = 1; i < waterSimulation.m_waterBuffers.Length; i++)
			{
				Array.Copy(array, waterSimulation.m_waterBuffers[i], array.Length);
			}
			waterSimulation.m_simulationFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (s.get_version() < 173u)
			{
				waterSimulation.m_waterFrameIndex = (Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4294967232u);
				waterSimulation.m_waterFrameIndex2 = (Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4294967232u);
			}
			waterSimulation.m_renderingFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			ulong[] waterExists = waterSimulation.m_waterExists1;
			ulong[] waterExists2 = waterSimulation.m_waterExists2;
			ulong[] waterExists3 = waterSimulation.m_waterExists3;
			int num2 = num + 63 >> 6;
			int num3 = 0;
			int num4 = 0;
			ulong num5 = 0uL;
			for (int j = 0; j <= num; j++)
			{
				int num6 = j / num2;
				if (num6 != num4)
				{
					waterExists[num4] = num5;
					waterExists2[num4] = num5;
					waterExists3[num4] = num5;
					num5 = 0uL;
					num4 = num6;
				}
				for (int k = 0; k <= num; k++)
				{
					WaterSimulation.Cell cell = array[num3];
					if (cell.m_height != 0)
					{
						num5 |= 1uL << k / num2;
					}
					num3++;
				}
			}
			waterExists[num4] = num5;
			waterExists2[num4] = num5;
			waterExists3[num4] = num5;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "WaterSimulation");
		}
	}
}
