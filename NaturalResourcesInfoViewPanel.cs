﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class NaturalResourcesInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		UIPanel uIPanel = base.Find<UIPanel>("EntryOil");
		this.m_OilAmount = uIPanel.Find<UILabel>("Amount");
		this.m_OilUsed = uIPanel.Find<UILabel>("Used");
		UIPanel uIPanel2 = base.Find<UIPanel>("EntryOre");
		this.m_OreAmount = uIPanel2.Find<UILabel>("Amount");
		this.m_OreUsed = uIPanel2.Find<UILabel>("Used");
		UIPanel uIPanel3 = base.Find<UIPanel>("EntryForest");
		this.m_ForestAmount = uIPanel3.Find<UILabel>("Amount");
		this.m_ForestUsed = uIPanel3.Find<UILabel>("Used");
		UIPanel uIPanel4 = base.Find<UIPanel>("EntryFertileLand");
		this.m_FertileLandAmount = uIPanel4.Find<UILabel>("Amount");
		this.m_FertileLandUsed = uIPanel4.Find<UILabel>("Used");
	}

	private void SetSpriteColor(UIComponent entry, Color col)
	{
		UISlicedSprite uISlicedSprite = entry.Find<UISlicedSprite>("Sprite");
		col.a = uISlicedSprite.get_opacity();
		uISlicedSprite.set_color(col);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UIPanel entry = base.Find<UIPanel>("EntryOil");
				UIPanel entry2 = base.Find<UIPanel>("EntryOre");
				UIPanel entry3 = base.Find<UIPanel>("EntryForest");
				UIPanel entry4 = base.Find<UIPanel>("EntryFertileLand");
				UISprite uISprite = base.Find<UISprite>("OilColor");
				UISprite uISprite2 = base.Find<UISprite>("OreColor");
				UISprite uISprite3 = base.Find<UISprite>("ForestColor");
				UISprite uISprite4 = base.Find<UISprite>("FertileLandColor");
				if (Singleton<NaturalResourceManager>.get_exists())
				{
					uISprite.set_color(Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[2]);
					uISprite2.set_color(Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[0]);
					uISprite3.set_color(Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[4]);
					uISprite4.set_color(Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[3]);
					this.SetSpriteColor(entry, uISprite.get_color());
					this.SetSpriteColor(entry2, uISprite2.get_color());
					this.SetSpriteColor(entry3, uISprite3.get_color());
					this.SetSpriteColor(entry4, uISprite4.get_color());
				}
			}
			this.m_initialized = true;
		}
		uint num = 0u;
		uint num2 = 0u;
		uint num3 = 0u;
		uint num4 = 0u;
		uint num5 = 0u;
		Singleton<NaturalResourceManager>.get_instance().CalculateUnlockedResources(out num2, out num, out num3, out num4, out num5);
		uint num6 = 0u;
		uint num7 = 0u;
		uint num8 = 0u;
		uint num9 = 0u;
		Singleton<NaturalResourceManager>.get_instance().CalculateUsedResources(out num7, out num6, out num8, out num9);
		float num10 = 0.1116728f;
		float num11 = 0.000446691178f;
		float num12 = 0.00399999972f;
		num3 = (uint)Mathf.CeilToInt(num3 * num10);
		num8 = Math.Min(num3, num8);
		num4 = (uint)Mathf.CeilToInt(num4 * num11);
		num9 = (uint)Mathf.CeilToInt(num9 * num12);
		num9 = Math.Min(num4, num9);
		this.m_OilAmount.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_OILAMOUNT"), num));
		this.m_OreAmount.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_OREAMOUNT"), num2));
		this.m_ForestAmount.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_FORESTAMOUNT"), num3));
		this.m_FertileLandAmount.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_FERTILELANDAMOUNT"), num4));
		this.m_OilUsed.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_OILUSED"), num6));
		this.m_OreUsed.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_OREUSED"), num7));
		this.m_ForestUsed.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_FORESTUSED"), num8));
		this.m_FertileLandUsed.set_text(StringUtils.SafeFormat(Locale.Get("INFO_RES_FERTILELANDUSED"), num9));
	}

	private UILabel m_OilAmount;

	private UILabel m_OilUsed;

	private UILabel m_OreAmount;

	private UILabel m_OreUsed;

	private UILabel m_ForestAmount;

	private UILabel m_ForestUsed;

	private UILabel m_FertileLandAmount;

	private UILabel m_FertileLandUsed;

	private bool m_initialized;
}
