﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;

public class PolicyContainer : ToolsModifierControl
{
	private void Start()
	{
		this.m_Panel = (base.get_component().get_objectUserData() as PoliciesPanel);
		this.m_Policy = Utils.GetEnumByName<DistrictPolicies.Policies>(base.get_component().get_stringUserData());
		this.m_Button = base.Find<UIButton>("PolicyButton");
		this.m_Check = base.Find<UICheckBox>("Checkbox");
		if (EnumExtensions.Category<DistrictPolicies.Policies>(this.m_Policy) == "Industry")
		{
			base.Find<UISprite>("Unchecked").set_spriteName("check-unchecked");
			base.Find<UISprite>("Checked").set_spriteName("check-checked");
		}
		else
		{
			base.Find<UISprite>("Unchecked").set_spriteName("ToggleBase");
			base.Find<UISprite>("Checked").set_spriteName("ToggleBaseFocused");
		}
	}

	private bool IsPolicySet(byte district, DistrictPolicies.Policies policy)
	{
		if (district > 0)
		{
			return Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(policy, district);
		}
		return Singleton<DistrictManager>.get_instance().IsCityPolicySet(policy);
	}

	[DebuggerHidden]
	private IEnumerator SetPolicy(byte district, bool value, DistrictPolicies.Policies policy)
	{
		PolicyContainer.<SetPolicy>c__Iterator0 <SetPolicy>c__Iterator = new PolicyContainer.<SetPolicy>c__Iterator0();
		<SetPolicy>c__Iterator.value = value;
		<SetPolicy>c__Iterator.district = district;
		<SetPolicy>c__Iterator.policy = policy;
		return <SetPolicy>c__Iterator;
	}

	public bool isGlobal
	{
		get
		{
			return Singleton<DistrictManager>.get_exists() && this.m_Panel != null && this.IsPolicySet(this.m_Panel.targetDistrict, this.m_Policy);
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && Singleton<DistrictManager>.get_exists() && this.m_Panel != null && this.IsPolicySet(this.m_Panel.targetDistrict, this.m_Policy) != value)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.SetPolicy(this.m_Panel.targetDistrict, value, this.m_Policy));
			}
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_Button.set_state((!this.m_Check.get_isEnabled()) ? 4 : ((!this.m_Check.get_isChecked()) ? 0 : 1));
		}
	}

	private PoliciesPanel m_Panel;

	private DistrictPolicies.Policies m_Policy;

	private UIButton m_Button;

	private UICheckBox m_Check;
}
