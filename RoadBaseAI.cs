﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RoadBaseAI : PlayerNetAI
{
	public override void RenderNode(ushort nodeID, ref NetNode nodeData, RenderManager.CameraInfo cameraInfo)
	{
		if ((nodeData.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
		{
			float x = Vector3.Distance(cameraInfo.m_position, nodeData.m_position);
			float num = MathUtils.SmoothStep(1000f, 500f, x);
			if (num < 0.001f)
			{
				return;
			}
			bool flag = (nodeData.m_flags & (NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn)) == NetNode.Flags.None;
			bool flag2 = this.CanEnableTrafficLights(nodeID, ref nodeData);
			if (!flag && !flag2)
			{
				return;
			}
			InstanceID instanceID;
			int num2;
			Singleton<ToolManager>.get_instance().m_properties.GetComponent<DefaultTool>().GetHoverInstance(out instanceID, out num2);
			if (flag2)
			{
				float scale = (instanceID.NetNode != nodeID || num2 != -1) ? 0.75f : 1f;
				if ((nodeData.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
				{
					NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.TrafficLightsOn, nodeData.m_position, scale, num);
				}
				else
				{
					NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.TrafficLightsOff, nodeData.m_position, scale, num);
				}
				if (Singleton<SimulationManager>.get_instance().m_simulationView.Intersect(nodeData.m_position, -10f))
				{
					Singleton<NetManager>.get_instance().m_visibleTrafficLightNode = nodeID;
				}
			}
			if (flag)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				float num3 = this.GetCollisionHalfWidth() * 0.75f;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = nodeData.GetSegment(i);
					if (segment != 0)
					{
						NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
						if ((info.m_vehicleTypes & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None)
						{
							bool flag3 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
							bool flag4 = (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
							if ((flag3 != flag4) ? info.m_hasBackwardVehicleLanes : info.m_hasForwardVehicleLanes)
							{
								float scale2 = (instanceID.NetNode != nodeID || num2 != i + 1) ? 0.75f : 1f;
								Vector3 vector = (!flag3) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
								Vector3 position = nodeData.m_position + vector * num3;
								NetSegment.Flags flags = (!flag3) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
								if ((instance.m_segments.m_buffer[(int)segment].m_flags & flags) != NetSegment.Flags.None)
								{
									NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.YieldOn, position, scale2, num);
								}
								else
								{
									NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.YieldOff, position, scale2, num);
								}
							}
						}
					}
				}
			}
		}
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Traffic:
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)data.m_trafficDensity * 0.01f));
		case InfoManager.InfoMode.Wind:
		case InfoManager.InfoMode.BuildingLevel:
		case InfoManager.InfoMode.Entertainment:
		case InfoManager.InfoMode.TerrainHeight:
		case InfoManager.InfoMode.Heating:
		case InfoManager.InfoMode.Radio:
			IL_41:
			switch (infoMode)
			{
			case InfoManager.InfoMode.None:
			{
				Color color = this.m_info.m_color;
				if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
				{
					float num = 0.5f;
					color.r = color.r * (1f - num) + num * 0.75f;
					color.g = color.g * (1f - num) + num * 0.75f;
					color.b = color.b * (1f - num) + num * 0.75f;
				}
				color.a = (float)(255 - data.m_wetness) * 0.003921569f;
				return color;
			}
			case InfoManager.InfoMode.CrimeRate:
			case InfoManager.InfoMode.Health:
				goto IL_46A;
			case InfoManager.InfoMode.NoisePollution:
			{
				int num2 = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
				int num3 = this.m_noiseAccumulation * num2 / 100;
				return CommonBuildingAI.GetNoisePollutionColor((float)num3 * 1.25f);
			}
			case InfoManager.InfoMode.Transport:
				if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[6], 0.15f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(segmentID, ref data, infoMode);
		case InfoManager.InfoMode.Garbage:
		case InfoManager.InfoMode.FireSafety:
		case InfoManager.InfoMode.Education:
		case InfoManager.InfoMode.EscapeRoutes:
			goto IL_46A;
		case InfoManager.InfoMode.Maintenance:
			if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) == VehicleInfo.VehicleType.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
			{
				if (this.m_highwayRules)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Mathf.Clamp01((float)data.m_condition / 255f));
			}
			else
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				byte coverage = instance.m_nodes.m_buffer[(int)data.m_startNode].m_coverage;
				byte coverage2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_coverage;
				if (coverage == 255 || coverage2 == 255)
				{
					return Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage;
				}
				return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)(coverage + coverage2) * 0.001968504f));
			}
			break;
		case InfoManager.InfoMode.Snow:
			if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) == VehicleInfo.VehicleType.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
			{
				if (this.m_accumulateSnow)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)data.m_wetness / 255f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			else
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				byte coverage3 = instance2.m_nodes.m_buffer[(int)data.m_startNode].m_coverage;
				byte coverage4 = instance2.m_nodes.m_buffer[(int)data.m_endNode].m_coverage;
				if (coverage3 == 255 || coverage4 == 255)
				{
					return Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage;
				}
				return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)(coverage3 + coverage4) * 0.001968504f));
			}
			break;
		case InfoManager.InfoMode.Destruction:
			if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		goto IL_41;
		IL_46A:
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) == VehicleInfo.VehicleType.None || (infoMode == InfoManager.InfoMode.CrimeRate && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower))
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		NetManager instance3 = Singleton<NetManager>.get_instance();
		byte coverage5 = instance3.m_nodes.m_buffer[(int)data.m_startNode].m_coverage;
		byte coverage6 = instance3.m_nodes.m_buffer[(int)data.m_endNode].m_coverage;
		if (coverage5 == 255 || coverage6 == 255)
		{
			return Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage;
		}
		return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)(coverage5 + coverage6) * 0.001968504f));
	}

	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Traffic:
			return true;
		case InfoManager.InfoMode.Wind:
		case InfoManager.InfoMode.BuildingLevel:
		case InfoManager.InfoMode.Entertainment:
		case InfoManager.InfoMode.TerrainHeight:
		case InfoManager.InfoMode.Heating:
			IL_39:
			if (infoMode != InfoManager.InfoMode.CrimeRate && infoMode != InfoManager.InfoMode.Health)
			{
				return base.ColorizeProps(infoMode);
			}
			goto IL_4E;
		case InfoManager.InfoMode.Garbage:
		case InfoManager.InfoMode.FireSafety:
		case InfoManager.InfoMode.Education:
		case InfoManager.InfoMode.Maintenance:
		case InfoManager.InfoMode.Snow:
		case InfoManager.InfoMode.EscapeRoutes:
			goto IL_4E;
		}
		goto IL_39;
		IL_4E:
		return (this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None && (infoMode != InfoManager.InfoMode.CrimeRate || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.WaterPower);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Traffic:
		{
			int num = 0;
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_trafficDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= Mathf.Min(2, num2);
			}
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)num * 0.01f));
		}
		case InfoManager.InfoMode.Wind:
		case InfoManager.InfoMode.BuildingLevel:
		case InfoManager.InfoMode.Entertainment:
		case InfoManager.InfoMode.TerrainHeight:
		case InfoManager.InfoMode.Heating:
		case InfoManager.InfoMode.Radio:
			IL_41:
			switch (infoMode)
			{
			case InfoManager.InfoMode.None:
			{
				int num3 = 0;
				int num4 = 0;
				NetManager instance2 = Singleton<NetManager>.get_instance();
				int num5 = 0;
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = data.GetSegment(j);
					if (segment2 != 0)
					{
						if ((instance2.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
						{
							num3++;
						}
						num4 += (int)instance2.m_segments.m_buffer[(int)segment2].m_wetness;
						num5++;
					}
				}
				if (num5 != 0)
				{
					num4 /= num5;
				}
				Color color = this.m_info.m_color;
				if (num3 != 0)
				{
					float num6 = (float)num3 / (float)num5 * 0.5f;
					color.r = color.r * (1f - num6) + num6 * 0.75f;
					color.g = color.g * (1f - num6) + num6 * 0.75f;
					color.b = color.b * (1f - num6) + num6 * 0.75f;
				}
				color.a = (float)(255 - num4) * 0.003921569f;
				return color;
			}
			case InfoManager.InfoMode.CrimeRate:
			case InfoManager.InfoMode.Health:
				goto IL_7E8;
			case InfoManager.InfoMode.NoisePollution:
			{
				int num7 = 0;
				if (this.m_noiseAccumulation != 0)
				{
					NetManager instance3 = Singleton<NetManager>.get_instance();
					int num8 = 0;
					for (int k = 0; k < 8; k++)
					{
						ushort segment3 = data.GetSegment(k);
						if (segment3 != 0)
						{
							num7 += (int)instance3.m_segments.m_buffer[(int)segment3].m_noiseDensity;
							num8++;
						}
					}
					if (num8 != 0)
					{
						num7 /= num8;
					}
				}
				int num9 = 100 - (num7 - 100) * (num7 - 100) / 100;
				int num10 = this.m_noiseAccumulation * num9 / 100;
				return CommonBuildingAI.GetNoisePollutionColor((float)num10 * 1.25f);
			}
			case InfoManager.InfoMode.Transport:
			{
				NetManager instance4 = Singleton<NetManager>.get_instance();
				bool flag = false;
				for (int l = 0; l < 8; l++)
				{
					ushort segment4 = data.GetSegment(l);
					if (segment4 != 0)
					{
						NetInfo info = instance4.m_segments.m_buffer[(int)segment4].Info;
						if (info != null && (info.m_vehicleTypes & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None)
						{
							flag = true;
						}
					}
				}
				if (flag)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[6], 0.15f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			}
			return base.GetColor(nodeID, ref data, infoMode);
		case InfoManager.InfoMode.Garbage:
		case InfoManager.InfoMode.FireSafety:
		case InfoManager.InfoMode.Education:
		case InfoManager.InfoMode.EscapeRoutes:
			goto IL_7E8;
		case InfoManager.InfoMode.Maintenance:
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
			{
				int num11 = 0;
				NetManager instance5 = Singleton<NetManager>.get_instance();
				int num12 = 0;
				bool flag2 = false;
				bool flag3 = true;
				for (int m = 0; m < 8; m++)
				{
					ushort segment5 = data.GetSegment(m);
					if (segment5 != 0)
					{
						num11 += (int)instance5.m_segments.m_buffer[(int)segment5].m_condition;
						num12++;
						NetInfo info2 = instance5.m_segments.m_buffer[(int)segment5].Info;
						if (info2 != null)
						{
							if ((info2.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
							{
								flag2 = true;
							}
							RoadBaseAI roadBaseAI = info2.m_netAI as RoadBaseAI;
							if (roadBaseAI == null || !roadBaseAI.m_highwayRules)
							{
								flag3 = false;
							}
						}
					}
				}
				if (num12 != 0)
				{
					num11 /= num12;
				}
				if (flag2 && !flag3)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Mathf.Clamp01((float)num11 / 255f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			else
			{
				NetManager instance6 = Singleton<NetManager>.get_instance();
				bool flag4 = false;
				for (int n = 0; n < 8; n++)
				{
					ushort segment6 = data.GetSegment(n);
					if (segment6 != 0)
					{
						NetInfo info3 = instance6.m_segments.m_buffer[(int)segment6].Info;
						if (info3 != null && (info3.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
						{
							flag4 = true;
						}
					}
				}
				if (flag4)
				{
					return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)data.m_coverage * 0.003937008f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			break;
		case InfoManager.InfoMode.Snow:
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
			{
				int num13 = 0;
				NetManager instance7 = Singleton<NetManager>.get_instance();
				int num14 = 0;
				bool flag5 = false;
				bool flag6 = false;
				for (int num15 = 0; num15 < 8; num15++)
				{
					ushort segment7 = data.GetSegment(num15);
					if (segment7 != 0)
					{
						num13 += (int)instance7.m_segments.m_buffer[(int)segment7].m_wetness;
						num14++;
						NetInfo info4 = instance7.m_segments.m_buffer[(int)segment7].Info;
						if (info4 != null)
						{
							if ((info4.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
							{
								flag5 = true;
							}
							RoadBaseAI roadBaseAI2 = info4.m_netAI as RoadBaseAI;
							if (roadBaseAI2 != null && roadBaseAI2.m_accumulateSnow)
							{
								flag6 = true;
							}
						}
					}
				}
				if (num14 != 0)
				{
					num13 /= num14;
				}
				if (flag5 && flag6)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)num13 / 255f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			else
			{
				NetManager instance8 = Singleton<NetManager>.get_instance();
				bool flag7 = false;
				for (int num16 = 0; num16 < 8; num16++)
				{
					ushort segment8 = data.GetSegment(num16);
					if (segment8 != 0)
					{
						NetInfo info5 = instance8.m_segments.m_buffer[(int)segment8].Info;
						if (info5 != null && (info5.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
						{
							flag7 = true;
						}
					}
				}
				if (flag7)
				{
					return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)data.m_coverage * 0.003937008f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			break;
		case InfoManager.InfoMode.Destruction:
		{
			NetManager instance9 = Singleton<NetManager>.get_instance();
			bool flag8 = false;
			for (int num17 = 0; num17 < 8; num17++)
			{
				ushort segment9 = data.GetSegment(num17);
				if (segment9 != 0 && (instance9.m_segments.m_buffer[(int)segment9].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
				{
					flag8 = true;
				}
			}
			if (flag8)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		}
		goto IL_41;
		IL_7E8:
		NetManager instance10 = Singleton<NetManager>.get_instance();
		bool flag9 = false;
		for (int num18 = 0; num18 < 8; num18++)
		{
			ushort segment10 = data.GetSegment(num18);
			if (segment10 != 0)
			{
				NetInfo info6 = instance10.m_segments.m_buffer[(int)segment10].Info;
				if (info6 != null && (info6.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
				{
					flag9 = true;
				}
			}
		}
		if (!flag9 || (infoMode == InfoManager.InfoMode.CrimeRate && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower))
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		return Color.Lerp(Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage, Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage, Mathf.Clamp01((float)data.m_coverage * 0.003937008f));
	}

	public override void GetNodeState(ushort nodeID, ref NetNode nodeData, ushort segmentID, ref NetSegment segmentData, out NetNode.Flags flags, out Color color)
	{
		flags = nodeData.m_flags;
		color = Color.get_gray().get_gamma();
		if ((nodeData.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
		{
			if ((nodeData.m_flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
			{
				TrainTrackBaseAI.GetLevelCrossingNodeState(nodeID, ref nodeData, segmentID, ref segmentData, ref flags, ref color);
			}
			else
			{
				RoadBaseAI.GetTrafficLightNodeState(nodeID, ref nodeData, segmentID, ref segmentData, ref flags, ref color);
			}
		}
	}

	public static void GetTrafficLightNodeState(ushort nodeID, ref NetNode nodeData, ushort segmentID, ref NetSegment segmentData, ref NetNode.Flags flags, ref Color color)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - 15u;
		uint num2 = (uint)(((int)nodeID << 8) / 32768);
		uint num3 = num - num2 & 255u;
		RoadBaseAI.TrafficLightState trafficLightState;
		RoadBaseAI.TrafficLightState trafficLightState2;
		RoadBaseAI.GetTrafficLightState(nodeID, ref segmentData, num - num2, out trafficLightState, out trafficLightState2);
		color.a = 0.5f;
		switch (trafficLightState)
		{
		case RoadBaseAI.TrafficLightState.Green:
			color.g = 1f;
			break;
		case RoadBaseAI.TrafficLightState.RedToGreen:
			if (num3 < 45u)
			{
				color.g = 0f;
			}
			else if (num3 < 60u)
			{
				color.r = 1f;
			}
			else
			{
				color.g = 1f;
			}
			break;
		case RoadBaseAI.TrafficLightState.Red:
			color.g = 0f;
			break;
		case RoadBaseAI.TrafficLightState.GreenToRed:
			if (num3 < 45u)
			{
				color.r = 1f;
			}
			else
			{
				color.g = 0f;
			}
			break;
		}
		switch (trafficLightState2)
		{
		case RoadBaseAI.TrafficLightState.Green:
			color.b = 1f;
			break;
		case RoadBaseAI.TrafficLightState.RedToGreen:
			if (num3 < 45u)
			{
				color.b = 0f;
			}
			else
			{
				color.b = 1f;
			}
			break;
		case RoadBaseAI.TrafficLightState.Red:
			color.b = 0f;
			break;
		case RoadBaseAI.TrafficLightState.GreenToRed:
			if (num3 < 45u)
			{
				if ((num3 / 8u & 1u) == 1u)
				{
					color.b = 1f;
				}
			}
			else
			{
				color.b = 0f;
			}
			break;
		}
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
	}

	public override void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
		if (this.m_info.m_hasPedestrianLanes && (this.m_info.m_hasForwardVehicleLanes || this.m_info.m_hasBackwardVehicleLanes))
		{
			this.CheckBuildings(segmentID, ref data);
		}
		base.ReleaseSegment(segmentID, ref data);
	}

	public override void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
		base.NodeLoaded(nodeID, ref data, version);
		Singleton<NetManager>.get_instance().AddTileNode(data.m_position, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
	}

	public override int RayCastNodeButton(ushort nodeID, ref NetNode data, Segment3 ray)
	{
		if ((data.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
		{
			float num = Vector3.Distance(ray.a, data.m_position);
			if (num < 1000f)
			{
				bool flag = (data.m_flags & (NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn)) == NetNode.Flags.None;
				bool flag2 = this.CanEnableTrafficLights(nodeID, ref data);
				float num2 = 0.0675f * Mathf.Pow(num, 0.65f);
				if (flag2 && ray.DistanceSqr(data.m_position) < num2 * num2)
				{
					return -1;
				}
				if (flag)
				{
					NetManager instance = Singleton<NetManager>.get_instance();
					float num3 = this.GetCollisionHalfWidth() * 0.75f;
					for (int i = 0; i < 8; i++)
					{
						ushort segment = data.GetSegment(i);
						if (segment != 0)
						{
							NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
							if ((info.m_vehicleTypes & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None)
							{
								bool flag3 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
								bool flag4 = (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
								if ((flag3 != flag4) ? info.m_hasBackwardVehicleLanes : info.m_hasForwardVehicleLanes)
								{
									Vector3 vector = (!flag3) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
									Vector3 vector2 = data.m_position + vector * num3;
									if (ray.DistanceSqr(vector2) < num2 * num2)
									{
										return i + 1;
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}

	public override void ClickNodeButton(ushort nodeID, ref NetNode data, int index)
	{
		if ((data.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
		{
			if (index == -1)
			{
				data.m_flags ^= NetNode.Flags.TrafficLights;
				data.m_flags |= NetNode.Flags.CustomTrafficLights;
				this.UpdateNodeFlags(nodeID, ref data);
				Singleton<NetManager>.get_instance().m_yieldLights.Disable();
			}
			else if (index >= 1 && index <= 8 && (data.m_flags & (NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn)) == NetNode.Flags.None)
			{
				ushort segment = data.GetSegment(index - 1);
				if (segment != 0)
				{
					NetManager instance = Singleton<NetManager>.get_instance();
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if ((info.m_vehicleTypes & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None)
					{
						bool flag = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
						NetSegment.Flags flags = (!flag) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
						NetSegment[] expr_115_cp_0 = instance.m_segments.m_buffer;
						ushort expr_115_cp_1 = segment;
						expr_115_cp_0[(int)expr_115_cp_1].m_flags = (expr_115_cp_0[(int)expr_115_cp_1].m_flags ^ flags);
						instance.m_segments.m_buffer[(int)segment].UpdateLanes(segment, true);
						Singleton<NetManager>.get_instance().m_yieldLights.Disable();
					}
				}
			}
		}
	}

	public override void ManualActivation(ushort segmentID, ref NetSegment data, NetInfo oldInfo)
	{
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			Vector3 position3 = (position + position2) * 0.5f;
			float num = Vector3.Distance(position, position2);
			float num2 = (float)this.m_noiseAccumulation * num / this.m_noiseRadius;
			float num3 = Mathf.Max(num, this.m_noiseRadius);
			if (oldInfo != null)
			{
				int num4;
				float num5;
				oldInfo.m_netAI.GetNoiseAccumulation(out num4, out num5);
				if (num4 != 0)
				{
					num2 -= (float)num4 * num / num5;
					num3 = Mathf.Max(num3, num5);
				}
			}
			if (num2 != 0f)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position3, (num2 <= 0f) ? NotificationEvent.Type.Happy : NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, num2, num3);
			}
		}
	}

	public override void ManualDeactivation(ushort segmentID, ref NetSegment data)
	{
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			Vector3 position3 = (position + position2) * 0.5f;
			float num = Vector3.Distance(position, position2);
			float num2 = (float)this.m_noiseAccumulation * num / this.m_noiseRadius;
			float radius = Mathf.Max(num, this.m_noiseRadius);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(position3, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, -num2, radius);
		}
	}

	public override void GetNoiseAccumulation(out int noiseAccumulation, out float noiseRadius)
	{
		noiseAccumulation = this.m_noiseAccumulation;
		noiseRadius = this.m_noiseRadius;
	}

	public override void TrafficDirectionUpdated(ushort nodeID, ref NetNode data)
	{
		base.TrafficDirectionUpdated(nodeID, ref data);
		this.UpdateOutsideFlags(nodeID, ref data);
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			building = this.m_outsideConnection;
			heightOffset = -2f;
		}
		else
		{
			base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
		}
	}

	public override bool WantTrafficLights()
	{
		return this.m_trafficLights;
	}

	public override bool IsCombatible(NetInfo with)
	{
		if (this == with)
		{
			return true;
		}
		if (((this.m_info.m_vehicleTypes ^ with.m_vehicleTypes) & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None)
		{
			return false;
		}
		RoadBaseAI roadBaseAI = with.m_netAI as RoadBaseAI;
		return (roadBaseAI == null || this.m_centerAreaWidth == roadBaseAI.m_centerAreaWidth) && base.IsCombatible(with);
	}

	private bool CanEnableTrafficLights(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Junction) == NetNode.Flags.None)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info.m_class.m_service == ItemClass.Service.Road)
				{
					num++;
				}
				else if ((info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
				{
					num2++;
				}
				if (info.m_hasPedestrianLanes)
				{
					num3++;
				}
			}
		}
		return (num < 1 || num2 < 1) && ((data.m_flags & NetNode.Flags.OneWayIn) == NetNode.Flags.None || num3 != 0);
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetNode.Flags flags = data.m_flags;
		uint num = 0u;
		int num2 = 0;
		NetManager instance = Singleton<NetManager>.get_instance();
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		bool flag = this.WantTrafficLights();
		bool flag2 = false;
		int num6 = 0;
		int num7 = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null)
				{
					uint num8 = 1u << (int)info.m_class.m_level;
					if ((num & num8) == 0u)
					{
						num |= num8;
						num2++;
					}
					if (info.m_netAI.WantTrafficLights())
					{
						flag = true;
					}
					if ((info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None != ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None))
					{
						flag2 = true;
					}
					int num9 = 0;
					int num10 = 0;
					instance.m_segments.m_buffer[(int)segment].CountLanes(segment, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram, ref num9, ref num10);
					if (instance.m_segments.m_buffer[(int)segment].m_endNode == nodeID)
					{
						if (num9 != 0)
						{
							num3++;
							num4 += num9;
						}
					}
					else if (num10 != 0)
					{
						num3++;
						num4 += num10;
					}
					if (num9 != 0 || num10 != 0)
					{
						num5++;
					}
					if (info.m_class.m_service == ItemClass.Service.Road)
					{
						num6++;
					}
					else if ((info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
					{
						num7++;
					}
				}
			}
		}
		if (num6 >= 1 && num7 >= 1)
		{
			flags &= (NetNode.Flags.Created | NetNode.Flags.Deleted | NetNode.Flags.Original | NetNode.Flags.Disabled | NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Junction | NetNode.Flags.Moveable | NetNode.Flags.Untouchable | NetNode.Flags.Outside | NetNode.Flags.Temporary | NetNode.Flags.Double | NetNode.Flags.Fixed | NetNode.Flags.OnGround | NetNode.Flags.Ambiguous | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.ForbidLaneConnection | NetNode.Flags.Underground | NetNode.Flags.Transition | NetNode.Flags.LevelCrossing | NetNode.Flags.OneWayOut | NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn | NetNode.Flags.Heating | NetNode.Flags.Electricity | NetNode.Flags.Collapsed | NetNode.Flags.DisableOnlyMiddle | NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward);
			if (num6 >= 1 && num7 >= 2)
			{
				flags |= (NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights);
			}
			else
			{
				flags &= ~(NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights);
			}
			if (num2 >= 2 || flag2)
			{
				flags |= NetNode.Flags.Transition;
			}
			else
			{
				flags &= ~NetNode.Flags.Transition;
			}
		}
		else
		{
			flags &= ~NetNode.Flags.LevelCrossing;
			if (num2 >= 2 || flag2)
			{
				flags |= NetNode.Flags.Transition;
			}
			else
			{
				flags &= ~NetNode.Flags.Transition;
			}
			if (flag)
			{
				flag = ((num3 > 2 || (num3 >= 2 && num5 >= 3 && num4 > 6)) && (flags & NetNode.Flags.Junction) != NetNode.Flags.None);
			}
			if ((flags & NetNode.Flags.CustomTrafficLights) != NetNode.Flags.None)
			{
				if (!this.CanEnableTrafficLights(nodeID, ref data))
				{
					flags &= (NetNode.Flags.Created | NetNode.Flags.Deleted | NetNode.Flags.Original | NetNode.Flags.Disabled | NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Junction | NetNode.Flags.Moveable | NetNode.Flags.Untouchable | NetNode.Flags.Outside | NetNode.Flags.Temporary | NetNode.Flags.Double | NetNode.Flags.Fixed | NetNode.Flags.OnGround | NetNode.Flags.Ambiguous | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.ForbidLaneConnection | NetNode.Flags.Underground | NetNode.Flags.Transition | NetNode.Flags.LevelCrossing | NetNode.Flags.OneWayOut | NetNode.Flags.OneWayIn | NetNode.Flags.Heating | NetNode.Flags.Electricity | NetNode.Flags.Collapsed | NetNode.Flags.DisableOnlyMiddle | NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward);
				}
				else if (flag == ((data.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None))
				{
					flags &= (NetNode.Flags.Created | NetNode.Flags.Deleted | NetNode.Flags.Original | NetNode.Flags.Disabled | NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Junction | NetNode.Flags.Moveable | NetNode.Flags.Untouchable | NetNode.Flags.Outside | NetNode.Flags.Temporary | NetNode.Flags.Double | NetNode.Flags.Fixed | NetNode.Flags.OnGround | NetNode.Flags.Ambiguous | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.ForbidLaneConnection | NetNode.Flags.Underground | NetNode.Flags.Transition | NetNode.Flags.LevelCrossing | NetNode.Flags.OneWayOut | NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn | NetNode.Flags.Heating | NetNode.Flags.Electricity | NetNode.Flags.Collapsed | NetNode.Flags.DisableOnlyMiddle | NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward);
				}
			}
			else if (flag)
			{
				flags |= NetNode.Flags.TrafficLights;
			}
			else
			{
				flags &= ~NetNode.Flags.TrafficLights;
			}
		}
		data.m_flags = flags;
	}

	public static void GetTrafficLightState(ushort nodeID, ref NetSegment segmentData, uint frame, out RoadBaseAI.TrafficLightState vehicleLightState, out RoadBaseAI.TrafficLightState pedestrianLightState)
	{
		int num;
		if ((frame >> 8 & 1u) == 0u)
		{
			num = (int)segmentData.m_trafficLightState0;
		}
		else
		{
			num = (int)segmentData.m_trafficLightState1;
		}
		if (segmentData.m_startNode == nodeID)
		{
			num &= 15;
		}
		else
		{
			num >>= 4;
		}
		vehicleLightState = (RoadBaseAI.TrafficLightState)(num & 3);
		pedestrianLightState = (RoadBaseAI.TrafficLightState)(num >> 2);
	}

	public static void GetTrafficLightState(ushort nodeID, ref NetSegment segmentData, uint frame, out RoadBaseAI.TrafficLightState vehicleLightState, out RoadBaseAI.TrafficLightState pedestrianLightState, out bool vehicles, out bool pedestrians)
	{
		int num;
		if ((frame >> 8 & 1u) == 0u)
		{
			num = (int)segmentData.m_trafficLightState0;
		}
		else
		{
			num = (int)segmentData.m_trafficLightState1;
		}
		if (segmentData.m_startNode == nodeID)
		{
			num &= 15;
			vehicles = ((segmentData.m_flags & NetSegment.Flags.TrafficStart) != NetSegment.Flags.None);
			pedestrians = ((segmentData.m_flags & NetSegment.Flags.CrossingStart) != NetSegment.Flags.None);
		}
		else
		{
			num >>= 4;
			vehicles = ((segmentData.m_flags & NetSegment.Flags.TrafficEnd) != NetSegment.Flags.None);
			pedestrians = ((segmentData.m_flags & NetSegment.Flags.CrossingEnd) != NetSegment.Flags.None);
		}
		vehicleLightState = (RoadBaseAI.TrafficLightState)(num & 3);
		pedestrianLightState = (RoadBaseAI.TrafficLightState)(num >> 2);
	}

	public static void SetTrafficLightState(ushort nodeID, ref NetSegment segmentData, uint frame, RoadBaseAI.TrafficLightState vehicleLightState, RoadBaseAI.TrafficLightState pedestrianLightState, bool vehicles, bool pedestrians)
	{
		int num = (int)((int)pedestrianLightState << 2 | vehicleLightState);
		if (segmentData.m_startNode == nodeID)
		{
			if ((frame >> 8 & 1u) == 0u)
			{
				segmentData.m_trafficLightState0 = (byte)((int)(segmentData.m_trafficLightState0 & 240) | num);
			}
			else
			{
				segmentData.m_trafficLightState1 = (byte)((int)(segmentData.m_trafficLightState1 & 240) | num);
			}
			if (vehicles)
			{
				segmentData.m_flags |= NetSegment.Flags.TrafficStart;
			}
			else
			{
				segmentData.m_flags &= ~NetSegment.Flags.TrafficStart;
			}
			if (pedestrians)
			{
				segmentData.m_flags |= NetSegment.Flags.CrossingStart;
			}
			else
			{
				segmentData.m_flags &= ~NetSegment.Flags.CrossingStart;
			}
		}
		else
		{
			if ((frame >> 8 & 1u) == 0u)
			{
				segmentData.m_trafficLightState0 = (byte)((int)(segmentData.m_trafficLightState0 & 15) | num << 4);
			}
			else
			{
				segmentData.m_trafficLightState1 = (byte)((int)(segmentData.m_trafficLightState1 & 15) | num << 4);
			}
			if (vehicles)
			{
				segmentData.m_flags |= NetSegment.Flags.TrafficEnd;
			}
			else
			{
				segmentData.m_flags &= ~NetSegment.Flags.TrafficEnd;
			}
			if (pedestrians)
			{
				segmentData.m_flags |= NetSegment.Flags.CrossingEnd;
			}
			else
			{
				segmentData.m_flags &= ~NetSegment.Flags.CrossingEnd;
			}
		}
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		if (index == 0)
		{
			return base.NodeModifyMask(nodeID, ref data, segment1, segment2, index, ref surface, ref heights, ref edges, ref leftT, ref rightT, ref leftY, ref rightY);
		}
		if (index == 1 && !this.m_info.m_createPavement && this.m_highwayRules && (data.m_flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
		{
			surface = TerrainModify.Surface.PavementA;
			heights = TerrainModify.Heights.None;
			leftT = this.m_info.m_pavementWidth / (this.m_info.m_halfWidth * 2f);
			rightT = 1f - this.m_info.m_pavementWidth / (this.m_info.m_halfWidth * 2f);
			return true;
		}
		return false;
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Flood | Notification.Problem.Snow);
		if ((data.m_flags & NetSegment.Flags.AccessFailed) != NetSegment.Flags.None && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(16u) == 0)
		{
			data.m_flags &= ~NetSegment.Flags.AccessFailed;
		}
		float num = 0f;
		uint num2 = data.m_lanes;
		int num3 = 0;
		while (num3 < this.m_info.m_lanes.Length && num2 != 0u)
		{
			NetInfo.Lane lane = this.m_info.m_lanes[num3];
			if ((byte)(lane.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0 && (lane.m_vehicleType & ~VehicleInfo.VehicleType.Bicycle) != VehicleInfo.VehicleType.None)
			{
				num += instance2.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_length;
			}
			num2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
			num3++;
		}
		int num4 = 0;
		int num5 = 0;
		if (data.m_trafficBuffer == 65535)
		{
			if ((data.m_flags & NetSegment.Flags.Blocked) == NetSegment.Flags.None)
			{
				data.m_flags |= NetSegment.Flags.Blocked;
				data.m_modifiedIndex = instance.m_currentBuildIndex++;
			}
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.Blocked;
			int num6 = Mathf.RoundToInt(num) << 4;
			if (num6 != 0)
			{
				num4 = (int)((byte)Mathf.Min((int)(data.m_trafficBuffer * 100) / num6, 100));
				num5 = (int)((byte)Mathf.Min((int)(data.m_noiseBuffer * 250) / num6, 100));
			}
		}
		data.m_trafficBuffer = 0;
		data.m_noiseBuffer = 0;
		if (num4 > (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Min((int)(data.m_trafficDensity + 5), num4);
		}
		else if (num4 < (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Max((int)(data.m_trafficDensity - 5), num4);
		}
		if (num5 > (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Min((int)(data.m_noiseDensity + 2), num5);
		}
		else if (num5 < (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Max((int)(data.m_noiseDensity - 2), num5);
		}
		Vector3 position = instance2.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		bool flag = false;
		if ((this.m_info.m_setVehicleFlags & Vehicle.Flags.Underground) == (Vehicle.Flags)0)
		{
			float num7 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
			if (num7 > vector.y + 1f && num7 > 0f)
			{
				flag = true;
				data.m_flags |= NetSegment.Flags.Flooded;
				problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
				Vector3 min = data.m_bounds.get_min();
				Vector3 max = data.m_bounds.get_max();
				RoadBaseAI.FloodParkedCars(min.x, min.z, max.x, max.z);
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.Flooded;
				if (num7 > vector.y && num7 > 0f)
				{
					flag = true;
					problem = Notification.AddProblems(problem, Notification.Problem.Flood);
				}
			}
		}
		DistrictManager instance3 = Singleton<DistrictManager>.get_instance();
		byte district = instance3.GetDistrict(vector);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance3.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		int num8 = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
		{
			if ((this.m_info.m_setVehicleFlags & Vehicle.Flags.Underground) == (Vehicle.Flags)0)
			{
				if (flag && (data.m_flags & (NetSegment.Flags.AccessFailed | NetSegment.Flags.Blocked)) == NetSegment.Flags.None && instance.m_randomizer.Int32(10u) == 0)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 4;
					offer.NetSegment = segmentID;
					offer.Position = vector;
					offer.Amount = 1;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.FloodWater, offer);
				}
				int num9 = (int)data.m_wetness;
				if (!instance2.m_treatWetAsSnow)
				{
					if (flag)
					{
						num9 = 255;
					}
					else
					{
						int num10 = -(num9 + 63 >> 5);
						float num11 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
						if (num11 != 0f)
						{
							int num12 = Mathf.RoundToInt(Mathf.Min(num11 * 4000f, 1000f));
							num10 += instance.m_randomizer.Int32(num12, num12 + 99) / 100;
						}
						num9 = Mathf.Clamp(num9 + num10, 0, 255);
					}
				}
				else if (this.m_accumulateSnow)
				{
					if (flag)
					{
						num9 = 128;
					}
					else
					{
						float num13 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
						if (num13 != 0f)
						{
							int num14 = Mathf.RoundToInt(num13 * 400f);
							int num15 = instance.m_randomizer.Int32(num14, num14 + 99) / 100;
							if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Snowplow))
							{
								num9 = Mathf.Min(num9 + num15, 255);
							}
							else
							{
								num9 = Mathf.Min(num9 + num15, 128);
							}
						}
						else if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u) == 0)
						{
							num9 = Mathf.Max(num9 - 1, 0);
						}
						if (num9 >= 64 && (data.m_flags & (NetSegment.Flags.AccessFailed | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded)) == NetSegment.Flags.None && instance.m_randomizer.Int32(10u) == 0)
						{
							TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
							offer2.Priority = num9 / 50;
							offer2.NetSegment = segmentID;
							offer2.Position = vector;
							offer2.Amount = 1;
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Snow, offer2);
						}
						if (num9 >= 192)
						{
							problem = Notification.AddProblems(problem, Notification.Problem.Snow);
						}
						District[] expr_63D_cp_0_cp_0 = instance3.m_districts.m_buffer;
						byte expr_63D_cp_0_cp_1 = district;
						expr_63D_cp_0_cp_0[(int)expr_63D_cp_0_cp_1].m_productionData.m_tempSnowCover = expr_63D_cp_0_cp_0[(int)expr_63D_cp_0_cp_1].m_productionData.m_tempSnowCover + (uint)num9;
					}
				}
				if (num9 != (int)data.m_wetness)
				{
					if (Mathf.Abs((int)data.m_wetness - num9) > 10)
					{
						data.m_wetness = (byte)num9;
						InstanceID empty = InstanceID.Empty;
						empty.NetSegment = segmentID;
						instance2.AddSmoothColor(empty);
						empty.NetNode = data.m_startNode;
						instance2.AddSmoothColor(empty);
						empty.NetNode = data.m_endNode;
						instance2.AddSmoothColor(empty);
					}
					else
					{
						data.m_wetness = (byte)num9;
						instance2.m_wetnessChanged = 256;
					}
				}
			}
			int num16;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.StuddedTires) != DistrictPolicies.CityPlanning.None)
			{
				num8 = num8 * 3 + 1 >> 1;
				num16 = Mathf.Min(700, (int)(50 + data.m_trafficDensity * 6));
			}
			else
			{
				num16 = Mathf.Min(500, (int)(50 + data.m_trafficDensity * 4));
			}
			if (!this.m_highwayRules)
			{
				int num17 = instance.m_randomizer.Int32(num16, num16 + 99) / 100;
				data.m_condition = (byte)Mathf.Max((int)data.m_condition - num17, 0);
				if (data.m_condition < 192 && (data.m_flags & (NetSegment.Flags.AccessFailed | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded)) == NetSegment.Flags.None && instance.m_randomizer.Int32(20u) == 0)
				{
					TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
					offer3.Priority = (int)((255 - data.m_condition) / 50);
					offer3.NetSegment = segmentID;
					offer3.Position = vector;
					offer3.Amount = 1;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.RoadMaintenance, offer3);
				}
			}
		}
		if (!this.m_highwayRules)
		{
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.HeavyTrafficBan) != DistrictPolicies.CityPlanning.None)
			{
				data.m_flags |= NetSegment.Flags.HeavyBan;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.HeavyBan;
			}
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.BikeBan) != DistrictPolicies.CityPlanning.None)
			{
				data.m_flags |= NetSegment.Flags.BikeBan;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.BikeBan;
			}
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.OldTown) != DistrictPolicies.CityPlanning.None)
			{
				data.m_flags |= NetSegment.Flags.CarBan;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.CarBan;
			}
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.CombustionEngineBan) != DistrictPolicies.CityPlanning.None)
			{
				data.m_flags |= NetSegment.Flags.WaitingPath;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.WaitingPath;
			}
		}
		int num18 = this.m_noiseAccumulation * num8 / 100;
		if (num18 != 0)
		{
			float num19 = Vector3.Distance(position, position2);
			int num20 = Mathf.FloorToInt(num19 / this.m_noiseRadius);
			for (int i = 0; i < num20; i++)
			{
				Vector3 position3 = Vector3.Lerp(position, position2, (float)(i + 1) / (float)(num20 + 1));
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num18, position3, this.m_noiseRadius);
			}
		}
		if (data.m_trafficDensity >= 50 && data.m_averageLength < 25f && (instance2.m_nodes.m_buffer[(int)data.m_startNode].m_flags & (NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights)) == NetNode.Flags.TrafficLights && (instance2.m_nodes.m_buffer[(int)data.m_endNode].m_flags & (NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights)) == NetNode.Flags.TrafficLights)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<NetManager>.get_instance().m_shortRoadTraffic.Activate(properties.m_shortRoadTraffic, segmentID, false);
			}
		}
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties2 != null)
			{
				Singleton<NetManager>.get_instance().m_roadDestroyed.Activate(properties2.m_roadDestroyed, segmentID, false);
				Singleton<NetManager>.get_instance().m_roadDestroyed2.Activate(properties2.m_roadDestroyed2, this.m_info.m_class.m_service);
			}
			if ((ulong)(instance.m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(segmentID & 15)))
			{
				int delta = Mathf.RoundToInt(data.m_averageLength);
				StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.DestroyedLength);
				statisticBase.Add(delta);
			}
		}
		data.m_problems = problem;
	}

	public static void FloodParkedCars(float minX, float minZ, float maxX, float maxZ)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		int num = Mathf.Max((int)(minX / 32f + 270f), 0);
		int num2 = Mathf.Max((int)(minZ / 32f + 270f), 0);
		int num3 = Mathf.Min((int)(maxX / 32f + 270f), 539);
		int num4 = Mathf.Min((int)(maxZ / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_parkedGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					VehicleInfo info = instance.m_parkedVehicles.m_buffer[(int)num5].Info;
					if (info.m_vehicleType == VehicleInfo.VehicleType.Car)
					{
						Vector3 position = instance.m_parkedVehicles.m_buffer[(int)num5].m_position;
						ushort num7;
						if (Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(position)) && instance.CreateVehicle(out num7, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, position, TransferManager.TransferReason.None, false, false))
						{
							Vehicle.Frame frame = instance.m_vehicles.m_buffer[(int)num7].m_frame0;
							frame.m_rotation = instance.m_parkedVehicles.m_buffer[(int)num5].m_rotation;
							instance.m_vehicles.m_buffer[(int)num7].m_frame0 = frame;
							instance.m_vehicles.m_buffer[(int)num7].m_frame1 = frame;
							instance.m_vehicles.m_buffer[(int)num7].m_frame2 = frame;
							instance.m_vehicles.m_buffer[(int)num7].m_frame3 = frame;
							info.m_vehicleAI.FrameDataUpdated(num7, ref instance.m_vehicles.m_buffer[(int)num7], ref frame);
							Vehicle[] expr_1DC_cp_0 = instance.m_vehicles.m_buffer;
							ushort expr_1DC_cp_1 = num7;
							expr_1DC_cp_0[(int)expr_1DC_cp_1].m_flags2 = (expr_1DC_cp_0[(int)expr_1DC_cp_1].m_flags2 | Vehicle.Flags2.Floating);
							uint ownerCitizen = instance.m_parkedVehicles.m_buffer[(int)num5].m_ownerCitizen;
							instance.m_vehicles.m_buffer[(int)num7].m_transferSize = (ushort)(ownerCitizen & 65535u);
							info.m_vehicleAI.TrySpawn(num7, ref instance.m_vehicles.m_buffer[(int)num7]);
							InstanceID empty = InstanceID.Empty;
							empty.ParkedVehicle = num5;
							InstanceID empty2 = InstanceID.Empty;
							empty2.Vehicle = num7;
							Singleton<InstanceManager>.get_instance().ChangeInstance(empty, empty2);
							if (ownerCitizen != 0u)
							{
								instance2.m_citizens.m_buffer[(int)((UIntPtr)ownerCitizen)].SetParkedVehicle(ownerCitizen, 0);
								instance2.m_citizens.m_buffer[(int)((UIntPtr)ownerCitizen)].SetVehicle(ownerCitizen, num7, 0u);
							}
							else
							{
								instance.ReleaseParkedVehicle(num5);
							}
						}
					}
					num5 = instance.m_parkedVehicles.m_buffer[(int)num5].m_nextGridParked;
					if (++num6 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override int GetMaintenanceCost(Vector3 startPos, Vector3 endPos)
	{
		int num = base.GetMaintenanceCost(startPos, endPos);
		Vector3 worldPos = (startPos + endPos) * 0.5f;
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(worldPos);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.StuddedTires) != DistrictPolicies.CityPlanning.None)
		{
			num = num * 5 + 3 >> 2;
		}
		return num;
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		if ((data.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
		{
			if ((data.m_flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
			{
				TrainTrackBaseAI.LevelCrossingSimulationStep(nodeID, ref data);
			}
			else
			{
				RoadBaseAI.TrafficLightSimulationStep(nodeID, ref data);
			}
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = 0;
		if (this.m_noiseAccumulation != 0)
		{
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
		}
		int num3 = 100 - (num - 100) * (num - 100) / 100;
		int num4 = this.m_noiseAccumulation * num3 / 100;
		if (num4 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num4, data.m_position, this.m_noiseRadius);
		}
		if ((data.m_problems & Notification.Problem.RoadNotConnected) != Notification.Problem.None && (data.m_flags & NetNode.Flags.Original) != NetNode.Flags.None)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				instance.m_outsideNodeNotConnected.Activate(properties.m_outsideNotConnected, nodeID, Notification.Problem.RoadNotConnected, false);
			}
		}
	}

	public static void TrafficLightSimulationStep(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		int num = (int)(data.m_maxWaitTime & 3);
		int num2 = data.m_maxWaitTime >> 2 & 7;
		int num3 = data.m_maxWaitTime >> 5;
		int num4 = -1;
		int num5 = -1;
		int num6 = -1;
		int num7 = -1;
		int num8 = -1;
		int num9 = -1;
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		int num13 = 0;
		int num14 = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				int num15 = 0;
				int num16 = 0;
				instance.m_segments.m_buffer[(int)segment].CountLanes(segment, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram, ref num15, ref num16);
				bool flag = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
				bool flag2 = (!flag) ? (num15 != 0) : (num16 != 0);
				bool flag3 = (!flag) ? (num16 != 0) : (num15 != 0);
				if (flag2)
				{
					num10 |= 1 << i;
				}
				if (flag3)
				{
					num11 |= 1 << i;
					num13++;
				}
				RoadBaseAI.TrafficLightState trafficLightState;
				RoadBaseAI.TrafficLightState trafficLightState2;
				bool flag4;
				bool flag5;
				RoadBaseAI.GetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment], currentFrameIndex - 256u, out trafficLightState, out trafficLightState2, out flag4, out flag5);
				if ((trafficLightState2 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green && flag5)
				{
					if (num7 == -1)
					{
						num7 = i;
					}
					if (num9 == -1 && num14 >= num3)
					{
						num9 = i;
					}
				}
				num14++;
				if (flag2 || flag4)
				{
					if ((trafficLightState & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
					{
						num5 = i;
						if (flag4)
						{
							num4 = i;
						}
					}
					else if (flag4)
					{
						if (num6 == -1)
						{
							num6 = i;
						}
						if (num8 == -1 && num12 >= num2)
						{
							num8 = i;
						}
					}
					num12++;
				}
			}
		}
		if (num8 == -1)
		{
			num8 = num6;
		}
		if (num9 == -1)
		{
			num9 = num7;
		}
		if (num5 != -1 && num4 != -1 && num <= 1)
		{
			num8 = -1;
			num9 = -1;
			num++;
		}
		if (num9 != -1 && num8 != -1 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) != 0)
		{
			num9 = -1;
		}
		if (num8 != -1)
		{
			num5 = num8;
		}
		if (num9 == num5)
		{
			num5 = -1;
		}
		Vector3 vector = Vector3.get_zero();
		if (num9 != -1)
		{
			ushort segment2 = data.GetSegment(num9);
			vector = instance.m_segments.m_buffer[(int)segment2].GetDirection(nodeID);
			if (num5 != -1)
			{
				segment2 = data.GetSegment(num5);
				Vector3 direction = instance.m_segments.m_buffer[(int)segment2].GetDirection(nodeID);
				if (direction.x * vector.x + direction.z * vector.z < -0.5f)
				{
					num5 = -1;
				}
			}
			if (num5 == -1)
			{
				for (int j = 0; j < 8; j++)
				{
					if (j != num9 && (num10 & 1 << j) != 0)
					{
						segment2 = data.GetSegment(j);
						if (segment2 != 0)
						{
							Vector3 direction2 = instance.m_segments.m_buffer[(int)segment2].GetDirection(nodeID);
							if (direction2.x * vector.x + direction2.z * vector.z >= -0.5f)
							{
								num5 = j;
								break;
							}
						}
					}
				}
			}
		}
		int num17 = -1;
		Vector3 vector2 = Vector3.get_zero();
		Vector3 vector3 = Vector3.get_zero();
		if (num5 != -1)
		{
			ushort segment3 = data.GetSegment(num5);
			vector2 = instance.m_segments.m_buffer[(int)segment3].GetDirection(nodeID);
			if ((num10 & num11 & 1 << num5) != 0)
			{
				for (int k = 0; k < 8; k++)
				{
					if (k != num5 && k != num9 && (num10 & num11 & 1 << k) != 0)
					{
						segment3 = data.GetSegment(k);
						if (segment3 != 0)
						{
							vector3 = instance.m_segments.m_buffer[(int)segment3].GetDirection(nodeID);
							if (num9 == -1 || vector3.x * vector.x + vector3.z * vector.z >= -0.5f)
							{
								if (num13 == 2)
								{
									num17 = k;
									break;
								}
								if (vector3.x * vector2.x + vector3.z * vector2.z < -0.9396926f)
								{
									num17 = k;
									break;
								}
							}
						}
					}
				}
			}
		}
		for (int l = 0; l < 8; l++)
		{
			ushort segment4 = data.GetSegment(l);
			if (segment4 != 0)
			{
				RoadBaseAI.TrafficLightState trafficLightState3;
				RoadBaseAI.TrafficLightState trafficLightState4;
				RoadBaseAI.GetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment4], currentFrameIndex - 256u, out trafficLightState3, out trafficLightState4);
				trafficLightState3 &= ~RoadBaseAI.TrafficLightState.RedToGreen;
				trafficLightState4 &= ~RoadBaseAI.TrafficLightState.RedToGreen;
				if (num5 == l || num17 == l)
				{
					if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState3 = RoadBaseAI.TrafficLightState.RedToGreen;
						num = 0;
						if (++num2 >= num12)
						{
							num2 = 0;
						}
					}
					if ((trafficLightState4 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState4 = RoadBaseAI.TrafficLightState.GreenToRed;
					}
				}
				else
				{
					if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState3 = RoadBaseAI.TrafficLightState.GreenToRed;
					}
					Vector3 direction3 = instance.m_segments.m_buffer[(int)segment4].GetDirection(nodeID);
					if ((num11 & 1 << l) != 0 && num9 != l && ((num5 != -1 && direction3.x * vector2.x + direction3.z * vector2.z < -0.5f) || (num17 != -1 && direction3.x * vector3.x + direction3.z * vector3.z < -0.5f)))
					{
						if ((trafficLightState4 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
						{
							trafficLightState4 = RoadBaseAI.TrafficLightState.GreenToRed;
						}
					}
					else if ((trafficLightState4 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState4 = RoadBaseAI.TrafficLightState.RedToGreen;
						if (++num3 >= num14)
						{
							num3 = 0;
						}
					}
				}
				RoadBaseAI.SetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment4], currentFrameIndex, trafficLightState3, trafficLightState4, false, false);
			}
		}
		data.m_maxWaitTime = (byte)(num3 << 5 | num2 << 2 | num);
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.RoadNotConnected | Notification.Problem.TrackNotConnected);
		int num = 0;
		int num2 = 0;
		data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, true, ref num, ref num2);
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_6F_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_6F_cp_1 = data.m_building;
				expr_6F_cp_0[(int)expr_6F_cp_1].m_flags = (expr_6F_cp_0[(int)expr_6F_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_9B_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_9B_cp_1 = data.m_building;
				expr_9B_cp_0[(int)expr_9B_cp_1].m_flags = (expr_9B_cp_0[(int)expr_9B_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_C8_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_C8_cp_1 = data.m_building;
				expr_C8_cp_0[(int)expr_C8_cp_1].m_flags = (expr_C8_cp_0[(int)expr_C8_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_F1_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_F1_cp_1 = data.m_building;
				expr_F1_cp_0[(int)expr_F1_cp_1].m_flags = (expr_F1_cp_0[(int)expr_F1_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
		else if ((num != 0 && num2 == 0) || (num == 0 && num2 != 0))
		{
			problem = Notification.AddProblems(problem, Notification.Problem.RoadNotConnected);
		}
		if (num == 1)
		{
			data.m_flags |= NetNode.Flags.OneWayOut;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.OneWayOut;
		}
		if (num2 == 1)
		{
			data.m_flags |= NetNode.Flags.OneWayIn;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.OneWayIn;
		}
		if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
		{
			int num3 = 0;
			num = 0;
			num2 = 0;
			NetManager instance2 = Singleton<NetManager>.get_instance();
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					int num4 = 0;
					int num5 = 0;
					instance2.m_segments.m_buffer[(int)segment].CountLanes(segment, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Tram, ref num4, ref num5);
					if (num4 != 0 || num5 != 0)
					{
						num3++;
						if (num4 != 0)
						{
							num4 = 1;
						}
						if (num5 != 0)
						{
							num5 = 1;
						}
						if (instance2.m_segments.m_buffer[(int)segment].m_endNode == nodeID)
						{
							num += num5;
							num2 += num4;
						}
						else
						{
							num += num4;
							num2 += num5;
						}
					}
				}
			}
			if ((num != 0 && num2 == 0) || (num == 0 && num2 != 0) || num3 == 1)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.TrackNotConnected);
			}
		}
		data.m_problems = problem;
	}

	private void UpdateOutsideFlags(ushort nodeID, ref NetNode data)
	{
		int num = 0;
		int num2 = 0;
		data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram, true, ref num, ref num2);
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_53_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_53_cp_1 = data.m_building;
				expr_53_cp_0[(int)expr_53_cp_1].m_flags = (expr_53_cp_0[(int)expr_53_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_7F_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_7F_cp_1 = data.m_building;
				expr_7F_cp_0[(int)expr_7F_cp_1].m_flags = (expr_7F_cp_0[(int)expr_7F_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_AC_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_AC_cp_1 = data.m_building;
				expr_AC_cp_0[(int)expr_AC_cp_1].m_flags = (expr_AC_cp_0[(int)expr_AC_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_D5_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_D5_cp_1 = data.m_building;
				expr_D5_cp_0[(int)expr_D5_cp_1].m_flags = (expr_D5_cp_0[(int)expr_D5_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
		if (num == 1)
		{
			data.m_flags |= NetNode.Flags.OneWayOut;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.OneWayOut;
		}
		if (num2 == 1)
		{
			data.m_flags |= NetNode.Flags.OneWayIn;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.OneWayIn;
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True;
		Vector3 vector;
		Vector3 vector2;
		bool smoothStart;
		data.CalculateCorner(segmentID, true, true, true, out vector, out vector2, out smoothStart);
		Vector3 vector3;
		Vector3 vector4;
		bool smoothEnd;
		data.CalculateCorner(segmentID, true, false, true, out vector3, out vector4, out smoothEnd);
		Vector3 vector5;
		Vector3 vector6;
		data.CalculateCorner(segmentID, true, true, false, out vector5, out vector6, out smoothStart);
		Vector3 vector7;
		Vector3 vector8;
		data.CalculateCorner(segmentID, true, false, false, out vector7, out vector8, out smoothEnd);
		if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
		{
			data.m_cornerAngleStart = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector5.z - vector.z, vector5.x - vector.x) * 40.7436638f) & 255);
			data.m_cornerAngleEnd = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector3.z - vector7.z, vector3.x - vector7.x) * 40.7436638f) & 255);
		}
		else
		{
			data.m_cornerAngleStart = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector.z - vector5.z, vector.x - vector5.x) * 40.7436638f) & 255);
			data.m_cornerAngleEnd = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector7.z - vector3.z, vector7.x - vector3.x) * 40.7436638f) & 255);
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		bool flag2 = false;
		bool flag3 = false;
		instance.m_nodes.m_buffer[(int)data.m_endNode].CountLanes(data.m_endNode, segmentID, NetInfo.Direction.Forward, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, -data.m_endDirection, ref num, ref num2, ref num3, ref num4, ref num5, ref num6);
		if ((instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Outside)) != NetNode.Flags.None)
		{
			if (num + num2 + num3 == 0)
			{
				flag3 = true;
			}
			else
			{
				flag2 = true;
			}
		}
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		bool flag4 = false;
		bool flag5 = false;
		instance.m_nodes.m_buffer[(int)data.m_startNode].CountLanes(data.m_startNode, segmentID, NetInfo.Direction.Forward, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, -data.m_startDirection, ref num7, ref num8, ref num9, ref num10, ref num11, ref num12);
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Outside)) != NetNode.Flags.None)
		{
			if (num7 + num8 + num9 == 0)
			{
				flag5 = true;
			}
			else
			{
				flag4 = true;
			}
		}
		NetLane.Flags flags = NetLane.Flags.None;
		if (num4 != 0 && num == 0)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.EndOneWayLeft : NetLane.Flags.StartOneWayLeft);
		}
		if (num6 != 0 && num3 == 0)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.EndOneWayRight : NetLane.Flags.StartOneWayRight);
		}
		if (num10 != 0 && num7 == 0)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.StartOneWayLeft : NetLane.Flags.EndOneWayLeft);
		}
		if (num12 != 0 && num9 == 0)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.StartOneWayRight : NetLane.Flags.EndOneWayRight);
		}
		if ((data.m_flags & NetSegment.Flags.YieldStart) != NetSegment.Flags.None)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.YieldStart : NetLane.Flags.YieldEnd);
		}
		if ((data.m_flags & NetSegment.Flags.YieldEnd) != NetSegment.Flags.None)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.YieldEnd : NetLane.Flags.YieldStart);
		}
		float num13 = 0f;
		float num14 = 0f;
		uint num15 = 0u;
		uint num16 = data.m_lanes;
		for (int i = 0; i < this.m_info.m_lanes.Length; i++)
		{
			if (num16 == 0u)
			{
				if (!Singleton<NetManager>.get_instance().CreateLanes(out num16, ref Singleton<SimulationManager>.get_instance().m_randomizer, segmentID, 1))
				{
					break;
				}
				if (num15 != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)num15)].m_nextLane = num16;
				}
				else
				{
					data.m_lanes = num16;
				}
			}
			NetInfo.Lane lane = this.m_info.m_lanes[i];
			float num17 = lane.m_position / (this.m_info.m_halfWidth * 2f) + 0.5f;
			if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				num17 = 1f - num17;
			}
			Vector3 vector9 = vector + (vector5 - vector) * num17;
			Vector3 startDir = Vector3.Lerp(vector2, vector6, num17);
			Vector3 vector10 = vector7 + (vector3 - vector7) * num17;
			Vector3 endDir = Vector3.Lerp(vector8, vector4, num17);
			vector9.y += lane.m_verticalOffset;
			vector10.y += lane.m_verticalOffset;
			Vector3 vector11;
			Vector3 vector12;
			NetSegment.CalculateMiddlePoints(vector9, startDir, vector10, endDir, smoothStart, smoothEnd, out vector11, out vector12);
			NetLane.Flags flags2 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_flags;
			NetLane.Flags flags3 = flags;
			flags2 &= ~(NetLane.Flags.Forward | NetLane.Flags.Left | NetLane.Flags.Right | NetLane.Flags.YieldStart | NetLane.Flags.YieldEnd | NetLane.Flags.StartOneWayLeft | NetLane.Flags.StartOneWayRight | NetLane.Flags.EndOneWayLeft | NetLane.Flags.EndOneWayRight);
			if ((byte)(lane.m_finalDirection & NetInfo.Direction.Both) == 2)
			{
				flags3 &= ~NetLane.Flags.YieldEnd;
			}
			if ((byte)(lane.m_finalDirection & NetInfo.Direction.Both) == 1)
			{
				flags3 &= ~NetLane.Flags.YieldStart;
			}
			if ((lane.m_vehicleType & VehicleInfo.VehicleType.Monorail) != VehicleInfo.VehicleType.None)
			{
				flags3 &= ~(NetLane.Flags.YieldStart | NetLane.Flags.YieldEnd);
			}
			flags2 |= flags3;
			if (flag)
			{
				flags2 |= NetLane.Flags.Inverted;
			}
			else
			{
				flags2 &= ~NetLane.Flags.Inverted;
			}
			int num18 = 0;
			int num19 = 255;
			if ((byte)(lane.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
			{
				bool flag6 = (byte)(lane.m_finalDirection & NetInfo.Direction.Forward) != 0 == ((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
				int num20;
				int num21;
				int num22;
				if (flag6)
				{
					num20 = num;
					num21 = num2;
					num22 = num3;
				}
				else
				{
					num20 = num7;
					num21 = num8;
					num22 = num9;
				}
				int num23;
				int num24;
				if ((byte)(lane.m_finalDirection & NetInfo.Direction.Forward) != 0)
				{
					num23 = lane.m_similarLaneIndex;
					num24 = lane.m_similarLaneCount - lane.m_similarLaneIndex - 1;
				}
				else
				{
					num23 = lane.m_similarLaneCount - lane.m_similarLaneIndex - 1;
					num24 = lane.m_similarLaneIndex;
				}
				int num25 = num20 + num21 + num22;
				num18 = 255;
				num19 = 0;
				if (num25 != 0)
				{
					int num26;
					int num27;
					if (lane.m_similarLaneCount >= num25)
					{
						num26 = num20;
						num27 = num22;
					}
					else
					{
						num26 = num20 * lane.m_similarLaneCount / (num25 + (num21 >> 1));
						num27 = num22 * lane.m_similarLaneCount / (num25 + (num21 >> 1));
					}
					int num28 = num26;
					int num29 = lane.m_similarLaneCount - num26 - num27;
					int num30 = num27;
					if (num29 > 0)
					{
						if (num20 > num26)
						{
							num28++;
						}
						if (num22 > num27)
						{
							num30++;
						}
					}
					if (num23 < num28)
					{
						int num31 = (num23 * num20 + num28 - 1) / num28;
						int num32 = ((num23 + 1) * num20 + num28 - 1) / num28;
						if (num32 > num31)
						{
							flags2 |= NetLane.Flags.Left;
							num18 = Mathf.Min(num18, num31);
							num19 = Mathf.Max(num19, num32);
						}
					}
					if (num23 >= num26 && num24 >= num27 && num21 != 0)
					{
						if (lane.m_similarLaneCount > num25)
						{
							num26++;
						}
						int num33 = num20 + ((num23 - num26) * num21 + num29 - 1) / num29;
						int num34 = num20 + ((num23 + 1 - num26) * num21 + num29 - 1) / num29;
						if (num34 > num33)
						{
							flags2 |= NetLane.Flags.Forward;
							num18 = Mathf.Min(num18, num33);
							num19 = Mathf.Max(num19, num34);
						}
					}
					if (num24 < num30)
					{
						int num35 = num25 - ((num24 + 1) * num22 + num30 - 1) / num30;
						int num36 = num25 - (num24 * num22 + num30 - 1) / num30;
						if (num36 > num35)
						{
							flags2 |= NetLane.Flags.Right;
							num18 = Mathf.Min(num18, num35);
							num19 = Mathf.Max(num19, num36);
						}
					}
					if (this.m_highwayRules)
					{
						if ((flags2 & NetLane.Flags.LeftRight) == NetLane.Flags.Left)
						{
							if ((flags2 & NetLane.Flags.Forward) == NetLane.Flags.None || (num21 >= 2 && num20 == 1))
							{
								num19 = Mathf.Min(num19, num18 + 1);
							}
						}
						else if ((flags2 & NetLane.Flags.LeftRight) == NetLane.Flags.Right && ((flags2 & NetLane.Flags.Forward) == NetLane.Flags.None || (num21 >= 2 && num22 == 1)))
						{
							num18 = Mathf.Max(num18, num19 - 1);
						}
					}
				}
				if (flag6)
				{
					if (flag2)
					{
						flags2 &= ~(NetLane.Flags.Forward | NetLane.Flags.Left | NetLane.Flags.Right);
					}
					else if (flag3)
					{
						flags2 |= NetLane.Flags.Forward;
					}
				}
				else if (flag4)
				{
					flags2 &= ~(NetLane.Flags.Forward | NetLane.Flags.Left | NetLane.Flags.Right);
				}
				else if (flag5)
				{
					flags2 |= NetLane.Flags.Forward;
				}
			}
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_bezier = new Bezier3(vector9, vector11, vector12, vector10);
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_segment = segmentID;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_flags = (ushort)flags2;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_firstTarget = (byte)num18;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_lastTarget = (byte)num19;
			num13 += instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].UpdateLength();
			num14 += 1f;
			num15 = num16;
			num16 = instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_nextLane;
		}
		if (num14 != 0f)
		{
			data.m_averageLength = num13 / num14;
		}
		else
		{
			data.m_averageLength = 0f;
		}
		bool flag7 = false;
		if (data.m_averageLength < 11f && (instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && (instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
		{
			flag7 = true;
		}
		num16 = data.m_lanes;
		int num37 = 0;
		while (num37 < this.m_info.m_lanes.Length && num16 != 0u)
		{
			NetLane.Flags flags4 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_flags & ~NetLane.Flags.JoinedJunction;
			if (flag7)
			{
				flags4 |= NetLane.Flags.JoinedJunction;
			}
			instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_flags = (ushort)flags4;
			num16 = instance.m_lanes.m_buffer[(int)((UIntPtr)num16)].m_nextLane;
			num37++;
		}
		if (!loading)
		{
			int num38 = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num39 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num40 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num41 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int j = num39; j <= num41; j++)
			{
				for (int k = num38; k <= num40; k++)
				{
					ushort num42 = instance.m_nodeGrid[j * 270 + k];
					int num43 = 0;
					while (num42 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num42].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num42].m_position;
						float num44 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num44 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num42, ref instance.m_nodes.m_buffer[(int)num42]);
						}
						num42 = instance.m_nodes.m_buffer[(int)num42].m_nextGridNode;
						if (++num43 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
			if (this.m_info.m_hasPedestrianLanes && (this.m_info.m_hasForwardVehicleLanes || this.m_info.m_hasBackwardVehicleLanes))
			{
				this.CheckBuildings(segmentID, ref data);
			}
		}
	}

	private void CheckBuildings(ushort segmentID, ref NetSegment data)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num = Mathf.Max((int)((data.m_bounds.get_min().x - 80f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 80f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 80f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 80f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = instance.m_buildings.m_buffer[(int)num5].m_position;
					position.y = data.m_middlePosition.y;
					if (data.m_bounds.SqrDistance(position) < 6400f)
					{
						instance.RoadCheckNeeded(num5);
					}
					num5 = instance.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		return base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
	}

	public override bool CanUpgradeTo(NetInfo info)
	{
		return base.CanUpgradeTo(info);
	}

	public override void UpgradeFailed()
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<NetManager>.get_instance().m_upgradeExistingRoad.Activate(properties.m_upgradeExistingRoad);
		}
	}

	public override void UpgradeSucceeded()
	{
		Singleton<NetManager>.get_instance().m_upgradeExistingRoad.Deactivate();
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return false;
		}
		if (DefaultTool.IsOutOfCityArea(segmentID))
		{
			return false;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_7C_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_7C_cp_1 = disaster;
				expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength = expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		if (data.m_blockStartLeft != 0)
		{
			ZoneBlock[] expr_BA_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_BA_cp_1 = data.m_blockStartLeft;
			expr_BA_cp_0[(int)expr_BA_cp_1].m_flags = (expr_BA_cp_0[(int)expr_BA_cp_1].m_flags | 4u);
		}
		if (data.m_blockStartRight != 0)
		{
			ZoneBlock[] expr_E8_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_E8_cp_1 = data.m_blockStartRight;
			expr_E8_cp_0[(int)expr_E8_cp_1].m_flags = (expr_E8_cp_0[(int)expr_E8_cp_1].m_flags | 4u);
		}
		if (data.m_blockEndLeft != 0)
		{
			ZoneBlock[] expr_116_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_116_cp_1 = data.m_blockEndLeft;
			expr_116_cp_0[(int)expr_116_cp_1].m_flags = (expr_116_cp_0[(int)expr_116_cp_1].m_flags | 4u);
		}
		if (data.m_blockEndRight != 0)
		{
			ZoneBlock[] expr_144_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_144_cp_1 = data.m_blockEndRight;
			expr_144_cp_0[(int)expr_144_cp_1].m_flags = (expr_144_cp_0[(int)expr_144_cp_1].m_flags | 4u);
		}
		Notification.Problem problems = data.m_problems;
		data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
		if (data.m_problems != problems)
		{
			Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
		return true;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_4B_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_4B_cp_1 = disaster;
				expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength = expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		if (data.m_blockStartLeft != 0)
		{
			ZoneBlock[] expr_89_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_89_cp_1 = data.m_blockStartLeft;
			expr_89_cp_0[(int)expr_89_cp_1].m_flags = (expr_89_cp_0[(int)expr_89_cp_1].m_flags | 4u);
		}
		if (data.m_blockStartRight != 0)
		{
			ZoneBlock[] expr_B7_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_B7_cp_1 = data.m_blockStartRight;
			expr_B7_cp_0[(int)expr_B7_cp_1].m_flags = (expr_B7_cp_0[(int)expr_B7_cp_1].m_flags | 4u);
		}
		if (data.m_blockEndLeft != 0)
		{
			ZoneBlock[] expr_E5_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_E5_cp_1 = data.m_blockEndLeft;
			expr_E5_cp_0[(int)expr_E5_cp_1].m_flags = (expr_E5_cp_0[(int)expr_E5_cp_1].m_flags | 4u);
		}
		if (data.m_blockEndRight != 0)
		{
			ZoneBlock[] expr_113_cp_0 = instance.m_blocks.m_buffer;
			ushort expr_113_cp_1 = data.m_blockEndRight;
			expr_113_cp_0[(int)expr_113_cp_1].m_flags = (expr_113_cp_0[(int)expr_113_cp_1].m_flags | 4u);
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			Notification.Problem problems = data.m_problems;
			data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
			if (data.m_problems != problems)
			{
				Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
			}
		}
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (segmentInfo.m_requireSurfaceMaps)
		{
			if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 255;
		}
		else
		{
			if (vector2.y > -0.5f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 0;
		}
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (nodeInfo.m_requireSurfaceMaps)
		{
			if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 255;
		}
		else
		{
			if (vector2.y > -0.5f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 0;
		}
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	public override NetSegment.Flags GetBendFlags(ushort nodeID, ref NetNode nodeData)
	{
		NetSegment.Flags flags = NetSegment.Flags.Bend;
		if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
		{
			if ((nodeData.m_flags & NetNode.Flags.AsymForward) != NetNode.Flags.None)
			{
				flags |= NetSegment.Flags.AsymBackward;
			}
			if ((nodeData.m_flags & NetNode.Flags.AsymBackward) != NetNode.Flags.None)
			{
				flags |= NetSegment.Flags.AsymForward;
			}
		}
		else
		{
			if ((nodeData.m_flags & NetNode.Flags.AsymForward) != NetNode.Flags.None)
			{
				flags |= NetSegment.Flags.AsymForward;
			}
			if ((nodeData.m_flags & NetNode.Flags.AsymBackward) != NetNode.Flags.None)
			{
				flags |= NetSegment.Flags.AsymBackward;
			}
		}
		return flags;
	}

	public override string GetLocalizedTooltip()
	{
		string localizedTooltip = base.GetLocalizedTooltip();
		return TooltipHelper.Append(localizedTooltip, TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Speed,
			LocaleFormatter.FormatGeneric("AIINFO_SPEED", new object[]
			{
				this.m_info.m_averageVehicleLaneSpeed * 50f
			})
		}));
	}

	public override float GetEndRadius()
	{
		return Mathf.Min(8f, this.m_info.m_halfWidth);
	}

	protected string GenerateStreetName(ref Randomizer r)
	{
		switch (r.Int32(20u))
		{
		case 0:
		case 1:
		{
			uint num = Locale.Count("NAME_FEMALE_FIRST");
			return Locale.Get("NAME_FEMALE_FIRST", r.Int32(num));
		}
		case 2:
		case 3:
		{
			uint num2 = Locale.Count("NAME_MALE_FIRST");
			return Locale.Get("NAME_MALE_FIRST", r.Int32(num2));
		}
		case 4:
		case 5:
		case 6:
		case 7:
		{
			uint num3 = Locale.Count("NAME_FEMALE_LAST");
			string text = Locale.Get("NAME_FEMALE_LAST", r.Int32(num3));
			return text.Replace("{0}", string.Empty).Trim();
		}
		case 8:
		case 9:
		case 10:
		case 11:
		{
			uint num4 = Locale.Count("NAME_MALE_LAST");
			string text2 = Locale.Get("NAME_MALE_LAST", r.Int32(num4));
			return text2.Replace("{0}", string.Empty).Trim();
		}
		case 12:
		{
			uint num5 = Locale.Count("NAME_FEMALE_FIRST");
			uint num6 = Locale.Count("NAME_FEMALE_LAST");
			string arg = Locale.Get("NAME_FEMALE_FIRST", r.Int32(num5));
			string format = Locale.Get("NAME_FEMALE_LAST", r.Int32(num6));
			return StringUtils.SafeFormat(format, arg);
		}
		case 13:
		{
			uint num7 = Locale.Count("NAME_MALE_FIRST");
			uint num8 = Locale.Count("NAME_MALE_LAST");
			string arg2 = Locale.Get("NAME_MALE_FIRST", r.Int32(num7));
			string format2 = Locale.Get("NAME_MALE_LAST", r.Int32(num8));
			return StringUtils.SafeFormat(format2, arg2);
		}
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		{
			uint num9 = Locale.Count("DISTRICT_NAME");
			return Locale.Get("DISTRICT_NAME", r.Int32(num9));
		}
		default:
			return string.Empty;
		}
	}

	[CustomizableProperty("Traffic Lights", "Properties")]
	public bool m_trafficLights;

	[CustomizableProperty("Highway Rules", "Properties")]
	public bool m_highwayRules;

	[CustomizableProperty("Accumulate Snow", "Properties")]
	public bool m_accumulateSnow = true;

	[CustomizableProperty("Noise Accumulation", "Properties")]
	public int m_noiseAccumulation = 10;

	[CustomizableProperty("Noise Radius", "Properties")]
	public float m_noiseRadius = 40f;

	public BuildingInfo m_outsideConnection;

	[CustomizableProperty("Center Area Width", "Properties")]
	public float m_centerAreaWidth;

	[Flags]
	public enum TrafficLightState
	{
		Green = 0,
		RedToGreen = 1,
		Red = 2,
		GreenToRed = 3,
		IsChanging = 1
	}
}
