﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterAssetImport : AssetImporterPanelBase
{
	public bool IsLargePreviewOpen
	{
		get
		{
			return this.m_LargePreview != null && this.m_LargePreview.get_isVisible();
		}
	}

	private PrefabInfo selectedTemplate
	{
		get
		{
			if (base.owner != null && base.owner.assetTemplatePanel != null)
			{
				return base.owner.assetTemplatePanel.selectedPrefabInfo;
			}
			return null;
		}
	}

	private bool hasPreview
	{
		get
		{
			return base.owner != null && base.owner.assetTemplatePanel != null && base.owner.assetTemplatePanel.hasPreview;
		}
	}

	private bool loadExistingDecoration
	{
		get
		{
			return base.owner != null && base.owner.assetTemplatePanel != null && base.owner.assetTemplatePanel.loadExistingDecoration;
		}
	}

	public void Reset()
	{
		this.m_LoadingLabel.Hide();
		this.m_ContinueButton.set_isEnabled(false);
		this.m_LargePreview.Hide();
	}

	public static string assetImportPath
	{
		get
		{
			if (AssetImporterAssetImport.m_AssetsPath == null)
			{
				AssetImporterAssetImport.m_AssetsPath = Path.Combine(DataLocation.get_addonsPath(), "Import");
			}
			if (!Directory.Exists(AssetImporterAssetImport.m_AssetsPath))
			{
				Directory.CreateDirectory(AssetImporterAssetImport.m_AssetsPath);
			}
			return AssetImporterAssetImport.m_AssetsPath;
		}
	}

	private void Awake()
	{
		this.m_LargePreview = ((!this.m_StandaloneModelImporter) ? base.get_component().get_parent().get_parent().Find<UITextureSprite>("LargePreview") : base.Find<UITextureSprite>("LargePreview"));
		this.m_SmallPreview = base.Find<UITextureSprite>("Preview");
		this.m_LargePreview.Hide();
		this.m_Scale = base.Find<UITextField>("Scale");
		this.m_Scale.set_text(1f.ToString());
		this.m_BottomPivot = base.Find<UICheckBox>("BottomPivot");
		this.m_BottomPivot.set_isChecked(false);
		this.m_LoadingLabel = base.Find<UILabel>("LoadingLabel");
		this.m_LoadingProgress = this.m_LoadingLabel.Find<UISprite>("ProgressSprite").get_transform();
		this.m_FileList = base.Find<UIListBox>("FileList");
		this.m_FileList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnFileSelectionChanged));
		this.m_FileList.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnFileListVisibilityChanged));
		this.m_ContinueButton = base.Find<UIButton>("Continue");
		this.m_ContinueButton.add_eventClick(new MouseEventHandler(this.OnContinue));
		this.m_BackButton = base.Find<UIButton>("Back");
		this.m_BackButton.add_eventClick(new MouseEventHandler(this.OnBack));
		this.m_PreviewSettingsContainer = base.Find<UIPanel>("PreviewSettingsContainer");
		this.showGrid = 0;
		for (int i = 0; i < AssetImporterAssetImport.m_ModelExtensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + AssetImporterAssetImport.m_ModelExtensions[i], AssetImporterAssetImport.assetImportPath, new FileSystemReporter.ReporterEventHandler(this.RefreshAssetList));
		}
		this.Reset();
		this.m_HadModelLastTime = false;
		base.get_component().Hide();
	}

	public void OnEnter()
	{
		this.ShowTransformFields();
		BuildingInfo component = this.selectedTemplate.GetComponent<BuildingInfo>();
		if (component != null)
		{
			this.m_CurrentAsset = new ImportAssetBuilding(this.selectedTemplate.get_gameObject(), this.m_PreviewCamera, this.loadExistingDecoration);
			return;
		}
		TreeInfo component2 = this.selectedTemplate.GetComponent<TreeInfo>();
		if (component2 != null)
		{
			this.m_CurrentAsset = new ImportAssetTree(this.selectedTemplate.get_gameObject(), this.m_PreviewCamera);
			return;
		}
		PropInfo component3 = this.selectedTemplate.GetComponent<PropInfo>();
		if (component3 != null)
		{
			this.m_CurrentAsset = new ImportAssetProp(this.selectedTemplate.get_gameObject(), this.m_PreviewCamera);
			return;
		}
		VehicleInfo component4 = this.selectedTemplate.GetComponent<VehicleInfo>();
		if (component4 != null)
		{
			this.m_CurrentAsset = new ImportAssetVehicle(this.selectedTemplate.get_gameObject(), this.m_PreviewCamera);
			return;
		}
		CitizenInfo component5 = this.selectedTemplate.GetComponent<CitizenInfo>();
		if (component5 != null)
		{
			this.HideTransformFields();
			this.m_CurrentAsset = new ImportAssetCitizen(this.selectedTemplate.get_gameObject(), this.m_PreviewCamera);
			return;
		}
	}

	public void ImportModel(Shader shader, AssetImporterAssetImport.ModelImportCallbackHandler callback)
	{
		base.get_component().Show();
		base.get_component().BringToFront();
		this.Reset();
		this.m_ModelImportCallback = callback;
		this.m_CurrentAsset = new ImportAssetModel(null, this.m_PreviewCamera, shader);
	}

	private void ResetTransformFields()
	{
		this.m_Scale.set_text(1.ToString());
		this.m_Rotation = default(Vector3);
		this.m_BottomPivot.set_isChecked(false);
	}

	private void ShowTransformFields()
	{
		this.m_PreviewSettingsContainer.Show();
		this.SetLargePreviewSettingsVisibility(true);
	}

	private void HideTransformFields()
	{
		this.m_PreviewSettingsContainer.Hide();
		this.SetLargePreviewSettingsVisibility(false);
	}

	private void SetLargePreviewSettingsVisibility(bool visible)
	{
		if (this.m_LargePreviewSettingsContainer == null)
		{
			this.m_LargePreviewSettingsContainer = this.m_LargePreview.Find<UIPanel>("LargePreviewSettingsContainer");
		}
		if (this.m_LargePreviewSettingsContainer != null)
		{
			if (visible)
			{
				this.m_LargePreviewSettingsContainer.Show();
			}
			else
			{
				this.m_LargePreviewSettingsContainer.Hide();
			}
		}
	}

	private void OnDestroy()
	{
		this.Reset();
		PreviewCamera.InfoRenderer.FullShading();
		for (int i = 0; i < AssetImporterAssetImport.m_ModelExtensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	protected void Update()
	{
		if (this.m_CurrentAsset != null)
		{
			this.m_ContinueButton.set_isEnabled(true);
			if (this.m_CurrentAsset.IsLoadingModel)
			{
				this.m_LoadingLabel.Show();
				this.m_FileList.set_isEnabled(false);
				this.m_ContinueButton.set_isEnabled(false);
				this.m_HadModelLastTime = false;
			}
			else
			{
				this.m_LoadingLabel.Hide();
				this.m_FileList.set_isEnabled(true);
				if (!this.m_HadModelLastTime)
				{
					this.SetDefaultScale();
					this.ApplyTransform();
					this.m_HadModelLastTime = true;
				}
			}
			this.m_PreviewCamera.target = ((!this.m_CurrentAsset.ReadyForEditing) ? null : this.m_CurrentAsset.Object);
			if (this.m_CurrentAsset.IsLoadingTextures || !this.m_CurrentAsset.ReadyForEditing)
			{
				this.m_ContinueButton.set_isEnabled(false);
			}
			if (this.m_CurrentAsset is ImportAssetModel)
			{
				if (this.m_CurrentAsset.TextureLoadingFinished && this.m_CurrentAsset.Tasks == null)
				{
					this.m_CurrentAsset.FinalizeImport();
				}
				if (this.m_CurrentAsset.Tasks == null || this.m_CurrentAsset.Tasks.isExecuting)
				{
					this.m_ContinueButton.set_isEnabled(false);
				}
			}
		}
		if (this.m_LoadingLabel.get_isVisible())
		{
			this.m_LoadingProgress.Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
		}
	}

	private void OnContinue(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_ContinueButton.set_isEnabled(false);
		PreviewCamera.InfoRenderer.FullShading();
		this.m_CurrentAsset.ApplyTransform(this.GetScaleMesh(), this.m_Rotation, this.m_BottomPivot.get_isChecked());
		if (this.m_StandaloneModelImporter)
		{
			base.get_component().Hide();
			if (this.m_ModelImportCallback != null)
			{
				this.m_ModelImportCallback(this.m_CurrentAsset.mesh, this.m_CurrentAsset.material, this.m_CurrentAsset.lodMesh, this.m_CurrentAsset.lodMaterial);
			}
			this.m_CurrentAsset.DestroyAsset();
			this.m_CurrentAsset = null;
		}
		else
		{
			this.m_CurrentAsset.FinalizeImport();
			if (this.m_PreviousAsset != null)
			{
				this.m_PreviousAsset.DestroyAsset();
			}
			this.m_PreviousAsset = this.m_CurrentAsset;
			if (ToolsModifierControl.toolController.m_editPrefabInfo != null && !(ToolsModifierControl.toolController.m_editPrefabInfo is NetInfo))
			{
				PrefabInfo editPrefabInfo = ToolsModifierControl.toolController.m_editPrefabInfo;
				ToolsModifierControl.toolController.m_editPrefabInfo = null;
				Object.DestroyImmediate(editPrefabInfo.get_gameObject());
			}
			ToolsModifierControl.toolController.m_editPrefabInfo = this.m_CurrentAsset.Object.GetComponent<PrefabInfo>();
			ToolsModifierControl.toolController.m_templatePrefabInfo = this.m_CurrentAsset.TemplateObject.GetComponent<PrefabInfo>();
			Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier = Guid.NewGuid().ToString();
			Singleton<SimulationManager>.get_instance().m_metaData.m_WorkshopPublishedFileId = PublishedFileId.invalid;
			base.owner.Complete();
		}
	}

	private void OnBack(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_CurrentAsset.DestroyAsset();
		this.m_CurrentAsset = null;
		if (this.m_StandaloneModelImporter)
		{
			base.get_component().Hide();
		}
		else
		{
			base.owner.GoBack();
		}
	}

	public void OnFileSelectionChanged(UIComponent comp, int index)
	{
		this.ResetTransformFields();
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_ContinueButton.set_isEnabled(false);
			if (this.selectedTemplate != null && this.hasPreview && index == 0)
			{
				this.m_CurrentAsset.ImportDefault();
			}
			else
			{
				this.m_CurrentAsset.Import(AssetImporterAssetImport.m_AssetsPath, this.m_FileList.get_items()[index]);
				this.SetDefaultScale();
			}
		}
		else
		{
			this.Reset();
		}
	}

	private void OnFileListVisibilityChanged(UIComponent comp, bool visible)
	{
		if (comp == this.m_FileList)
		{
			if (visible)
			{
				this.RefreshAssetList(AssetImporterAssetImport.m_ModelExtensions);
				for (int i = 0; i < AssetImporterAssetImport.m_ModelExtensions.Length; i++)
				{
					if (this.m_FileSystemReporter[i] != null)
					{
						this.m_FileSystemReporter[i].Start();
					}
				}
			}
			else
			{
				for (int j = 0; j < AssetImporterAssetImport.m_ModelExtensions.Length; j++)
				{
					if (this.m_FileSystemReporter[j] != null)
					{
						this.m_FileSystemReporter[j].Stop();
					}
				}
			}
		}
	}

	public void OnOpenLargePreview()
	{
		this.m_SmallPreview.set_texture(null);
		this.m_PreviewCamera.SetPreview(this.m_LargePreview, this.m_LargePreview.GetUIView().GetScreenResolution() * 0.88f);
		this.m_PreviewCamera.alpha = false;
		this.m_LargePreview.Show();
		Vector2 startSize = this.m_LargePreview.GetUIView().GetScreenResolution() * 0.66f;
		Vector2 endSize = this.m_LargePreview.GetUIView().GetScreenResolution() * 0.88f;
		ValueAnimator.Animate("LargePreview", delegate(float val)
		{
			if (this.m_LargePreview != null)
			{
				this.m_LargePreview.set_opacity(val);
				this.m_LargePreview.set_size(Vector2.Lerp(startSize, endSize, val));
				this.m_LargePreview.CenterTo(null);
			}
		}, new AnimatedFloat(0f, 1f, 0.15f));
	}

	public void OnCloseLargePreview()
	{
		Vector2 startSize = this.m_LargePreview.GetUIView().GetScreenResolution() * 0.66f;
		Vector2 endSize = this.m_LargePreview.GetUIView().GetScreenResolution() * 0.88f;
		ValueAnimator.Animate("LargePreview", delegate(float val)
		{
			if (this.m_LargePreview != null)
			{
				this.m_LargePreview.set_opacity(val);
				this.m_LargePreview.set_size(Vector2.Lerp(startSize, endSize, val));
				this.m_LargePreview.CenterTo(null);
			}
		}, new AnimatedFloat(1f, 0f, 0.15f), delegate
		{
			if (this.m_LargePreview != null)
			{
				if (this.m_PreviewCamera != null)
				{
					this.m_PreviewCamera.burnedAmount = 0f;
					this.m_PreviewCamera.preview = this.m_SmallPreview;
					this.m_PreviewCamera.alpha = true;
				}
				this.m_LargePreview.Hide();
			}
		});
	}

	public void ApplyTransform()
	{
		if (this.m_CurrentAsset != null)
		{
			this.m_CurrentAsset.ApplyTransform(this.GetScaleMesh(), this.m_Rotation, this.m_BottomPivot.get_isChecked());
		}
	}

	public void OnScaleSubmitted()
	{
		this.ApplyTransform();
	}

	public void OnBottomPivotChanged()
	{
		this.ApplyTransform();
	}

	public void OnRotateX()
	{
		this.m_Rotation.x = this.m_Rotation.x + 90f;
		this.ApplyTransform();
	}

	public void OnRotateY()
	{
		this.m_Rotation.y = this.m_Rotation.y + 90f;
		this.ApplyTransform();
	}

	public void OnRotateZ()
	{
		this.m_Rotation.z = this.m_Rotation.z + 90f;
		this.ApplyTransform();
	}

	public void OnViewModeSelectionChanged(UIComponent comp, int index)
	{
		if (index != 0)
		{
			if (index == 1)
			{
				PreviewCamera.InfoRenderer.LightingOnly();
			}
		}
		else
		{
			PreviewCamera.InfoRenderer.FullShading();
		}
	}

	public int showGrid
	{
		get
		{
			return (!this.m_PreviewCamera.showGrid) ? -1 : 0;
		}
		set
		{
			this.m_PreviewCamera.showGrid = (value == 0);
		}
	}

	public void OpenFolder()
	{
		Utils.OpenInFileBrowser(AssetImporterAssetImport.assetImportPath);
	}

	private void RefreshAssetList(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.RefreshAssetList(AssetImporterAssetImport.m_ModelExtensions);
		});
	}

	private void RefreshAssetList(string[] extensions)
	{
		base.get_component().Focus();
		List<string> list = new List<string>();
		if (this.selectedTemplate != null && this.hasPreview)
		{
			list.Add(Locale.Get("ASSETIMPORTER_TEMPLATE"));
		}
		DirectoryInfo directoryInfo = new DirectoryInfo(AssetImporterAssetImport.assetImportPath);
		if (directoryInfo.Exists)
		{
			FileInfo[] array = null;
			try
			{
				array = directoryInfo.GetFiles();
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			if (array != null)
			{
				FileInfo[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					FileInfo fileInfo = array2[i];
					if (!Path.GetFileNameWithoutExtension(fileInfo.Name).EndsWith(AssetImporterAssetImport.sLODModelSignature, StringComparison.OrdinalIgnoreCase))
					{
						for (int j = 0; j < extensions.Length; j++)
						{
							if (string.Compare(Path.GetExtension(fileInfo.Name), extensions[j]) == 0)
							{
								list.Add(fileInfo.Name);
							}
						}
					}
				}
			}
			this.m_FileList.set_items(list.ToArray());
		}
		if (this.selectedTemplate != null && this.hasPreview)
		{
			this.m_FileList.set_selectedIndex(0);
		}
	}

	private Vector3 GetScaleMesh()
	{
		float num;
		if (float.TryParse(this.m_Scale.get_text(), out num))
		{
			return new Vector3(num, num, num);
		}
		return Vector3.get_one();
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.m_BackButton.SimulateClick();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.m_ContinueButton.SimulateClick();
				p.Use();
			}
		}
	}

	private void SetDefaultScale()
	{
		float num = this.m_CurrentAsset.CalculateDefaultScale();
		if (num > 1f)
		{
			this.m_Scale.set_text(Math.Floor((double)num).ToString());
		}
		else
		{
			this.m_Scale.set_text(1f.ToString());
		}
	}

	public PreviewCamera m_PreviewCamera;

	public float m_RotationSpeed = 280f;

	public bool m_StandaloneModelImporter;

	private UITextureSprite m_LargePreview;

	private UITextureSprite m_SmallPreview;

	private UIButton m_ContinueButton;

	private UIButton m_BackButton;

	private UILabel m_LoadingLabel;

	private Transform m_LoadingProgress;

	public static string m_AssetsPath;

	private UIListBox m_FileList;

	private ImportAsset m_CurrentAsset;

	private ImportAsset m_PreviousAsset;

	private UITextField m_Scale;

	private UICheckBox m_BottomPivot;

	private Vector3 m_Rotation;

	private UIPanel m_PreviewSettingsContainer;

	private UIPanel m_LargePreviewSettingsContainer;

	private bool m_HadModelLastTime;

	protected static readonly string[] m_ModelExtensions = new string[]
	{
		".fbx",
		".FBX",
		".obj",
		".OBJ",
		".3ds",
		".3DS",
		".dae",
		".DAE",
		".dxf",
		".DXF"
	};

	protected static readonly string sLODModelSignature = "_lod";

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[AssetImporterAssetImport.m_ModelExtensions.Length];

	private AssetImporterAssetImport.ModelImportCallbackHandler m_ModelImportCallback;

	public delegate void ModelImportCallbackHandler(Mesh mesh, Material material, Mesh lodMesh, Material lodMaterial);
}
