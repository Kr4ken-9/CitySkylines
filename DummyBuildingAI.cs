﻿using System;
using ColossalFramework;
using UnityEngine;

public class DummyBuildingAI : CommonBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		ushort num = Building.FindParentBuilding(buildingID);
		if (num != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
			return info.m_buildingAI.GetColor(num, ref instance.m_buildings.m_buffer[(int)num], infoMode);
		}
		return base.GetColor(buildingID, ref data, infoMode);
	}

	protected override bool ShowConsumption(ushort buildingID, ref Building data)
	{
		return false;
	}

	public override int GetGarbageAmount(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public override void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, Vector3 position, Quaternion rotation, Vector4 buildingState, Vector4 objectIndex, Color color)
	{
		ushort num = Building.FindParentBuilding(buildingID);
		if (num != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			float active = ((instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) == Building.Flags.None) ? 0f : 1f;
			int state = -1000000;
			bool playActiveState = true;
			Color color2 = color;
			ushort num2 = instance.m_buildings.m_buffer[(int)num].m_eventIndex;
			if (num2 != 0)
			{
				EventManager instance2 = Singleton<EventManager>.get_instance();
				if ((instance2.m_events.m_buffer[(int)num2].m_flags & (EventData.Flags.Preparing | EventData.Flags.Active | EventData.Flags.Completed | EventData.Flags.Cancelled)) == EventData.Flags.None)
				{
					ushort nextBuildingEvent = instance2.m_events.m_buffer[(int)num2].m_nextBuildingEvent;
					if (nextBuildingEvent != 0)
					{
						num2 = nextBuildingEvent;
					}
				}
				if (num2 != 0)
				{
					EventInfo info = instance2.m_events.m_buffer[(int)num2].Info;
					if (info != null)
					{
						info.m_eventAI.GetAnimationState(num2, ref instance2.m_events.m_buffer[(int)num2], ref active, ref state, ref playActiveState, ref color, ref color2);
					}
				}
			}
			this.m_info.SetRenderParameters(buildingID, position, rotation, buildingState, objectIndex, active, state, playActiveState, color, color2);
		}
		else
		{
			base.SetRenderParameters(cameraInfo, buildingID, ref data, position, rotation, buildingState, objectIndex, color);
		}
	}

	public override void RenderMeshes(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		if (this.m_info.m_subMeshes == null || this.m_info.m_subMeshes.Length == 0)
		{
			base.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
			return;
		}
		ushort num = Building.FindParentBuilding(buildingID);
		if (num == 0)
		{
			base.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
			return;
		}
		if (this.m_info.m_mesh != null)
		{
			BuildingAI.RenderMesh(cameraInfo, buildingID, ref data, this.m_info, ref instance);
		}
		Building.Flags flags = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num].m_flags;
		for (int i = 0; i < this.m_info.m_subMeshes.Length; i++)
		{
			BuildingInfo.MeshInfo meshInfo = this.m_info.m_subMeshes[i];
			if (((meshInfo.m_flagsRequired | meshInfo.m_flagsForbidden) & flags) == meshInfo.m_flagsRequired)
			{
				BuildingInfoBase subInfo = meshInfo.m_subInfo;
				BuildingAI.RenderMesh(cameraInfo, this.m_info, subInfo, meshInfo.m_matrix, ref instance);
			}
		}
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		ushort num = Building.FindParentBuilding(buildingID);
		if (num == 0)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data);
			return;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		ushort eventIndex = instance.m_buildings.m_buffer[(int)num].m_eventIndex;
		if (eventIndex != 0)
		{
			EventManager instance2 = Singleton<EventManager>.get_instance();
			EventInfo info = instance2.m_events.m_buffer[(int)eventIndex].Info;
			AudioInfo audioInfo = this.m_info.m_customLoopSound;
			if (audioInfo == null)
			{
				if (this.m_info.m_class.m_subService != ItemClass.SubService.None)
				{
					audioInfo = instance.m_properties.m_subServiceSounds[(int)this.m_info.m_class.m_subService];
				}
				else
				{
					audioInfo = instance.m_properties.m_serviceSounds[(int)this.m_info.m_class.m_service];
				}
			}
			if (audioInfo != null)
			{
				float maxDistance = 50f + Mathf.Min((float)(data.Width + data.Length) * 8f, 100f);
				float volume = 1f;
				if (info != null && info.m_eventAI.GetAudioState(eventIndex, ref instance2.m_events.m_buffer[(int)eventIndex], ref volume, ref maxDistance, ref audioInfo))
				{
					base.PlayAudio(listenerInfo, buildingID, ref data, volume, audioInfo, maxDistance);
				}
			}
			return;
		}
		if ((instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data, 1f);
		}
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		ushort num = Building.FindParentBuilding(buildingID);
		if (num != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
			return info.m_buildingAI.GetFireParameters(num, ref instance.m_buildings.m_buffer[(int)num], out fireHazard, out fireSize, out fireTolerance);
		}
		return base.GetFireParameters(buildingID, ref buildingData, out fireHazard, out fireSize, out fireTolerance);
	}

	public override bool ClearOccupiedZoning()
	{
		return true;
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		amountDelta = 0;
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		amount = 0;
		max = 0;
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline)
		{
			min = 20f / Mathf.Max(22f, (float)this.m_info.m_cellLength * 8f);
			max = 1f;
			return true;
		}
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return !this.m_info.m_anyMeshRequireWaterMap && base.CanUseGroupMesh(buildingID, ref buildingData);
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
	}

	public override void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		negX = false;
		posX = false;
		negZ = false;
		posZ = false;
	}

	public int m_dummyID;
}
