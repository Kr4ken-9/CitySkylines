﻿using System;
using ColossalFramework.IO;

public struct DistrictConsumptionData
{
	public void Add(ref DistrictConsumptionData data)
	{
		this.m_tempBuildingCount += data.m_tempBuildingCount;
		this.m_tempElectricityConsumption += data.m_tempElectricityConsumption;
		this.m_tempHeatingConsumption += data.m_tempHeatingConsumption;
		this.m_tempWaterConsumption += data.m_tempWaterConsumption;
		this.m_tempSewageAccumulation += data.m_tempSewageAccumulation;
		this.m_tempGarbageAccumulation += data.m_tempGarbageAccumulation;
		this.m_tempIncomeAccumulation += data.m_tempIncomeAccumulation;
		this.m_tempWaterPollution += data.m_tempWaterPollution;
		this.m_tempGarbagePiles += data.m_tempGarbagePiles;
		this.m_tempDeadCount += data.m_tempDeadCount;
		this.m_tempSickCount += data.m_tempSickCount;
		this.m_tempExportAmount += data.m_tempExportAmount;
		this.m_tempImportAmount += data.m_tempImportAmount;
	}

	public void Update()
	{
		this.m_finalBuildingCount = this.m_tempBuildingCount;
		if (this.m_tempBuildingCount != 0)
		{
			this.m_finalGarbagePiles = (byte)(((ulong)this.m_tempGarbagePiles + (ulong)((long)(this.m_tempBuildingCount >> 1))) / (ulong)((long)this.m_tempBuildingCount));
			this.m_finalWaterPollution = (byte)(((ulong)this.m_tempWaterPollution + (ulong)((long)(this.m_tempBuildingCount >> 1))) / (ulong)((long)this.m_tempBuildingCount));
		}
		else
		{
			this.m_finalGarbagePiles = 0;
			this.m_finalWaterPollution = 0;
		}
		this.m_finalElectricityConsumption = this.m_tempElectricityConsumption;
		this.m_finalHeatingConsumption = this.m_tempHeatingConsumption;
		this.m_finalWaterConsumption = this.m_tempWaterConsumption;
		this.m_finalSewageAccumulation = this.m_tempSewageAccumulation;
		this.m_finalGarbageAccumulation = this.m_tempGarbageAccumulation;
		this.m_finalIncomeAccumulation = this.m_tempIncomeAccumulation;
		this.m_finalDeadCount = this.m_tempDeadCount;
		this.m_finalSickCount = this.m_tempSickCount;
		this.m_finalExportAmount = this.m_tempExportAmount;
		this.m_finalImportAmount = this.m_tempImportAmount;
	}

	public void Reset()
	{
		this.m_tempBuildingCount = 0;
		this.m_tempElectricityConsumption = 0u;
		this.m_tempHeatingConsumption = 0u;
		this.m_tempWaterConsumption = 0u;
		this.m_tempSewageAccumulation = 0u;
		this.m_tempGarbageAccumulation = 0u;
		this.m_tempIncomeAccumulation = 0u;
		this.m_tempWaterPollution = 0u;
		this.m_tempGarbagePiles = 0u;
		this.m_tempDeadCount = 0u;
		this.m_tempSickCount = 0u;
		this.m_tempExportAmount = 0u;
		this.m_tempImportAmount = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempElectricityConsumption);
		s.WriteUInt24(this.m_tempWaterConsumption);
		s.WriteUInt24(this.m_tempSewageAccumulation);
		s.WriteUInt24(this.m_tempGarbageAccumulation);
		s.WriteUInt32(this.m_tempIncomeAccumulation);
		s.WriteUInt16((uint)this.m_tempBuildingCount);
		s.WriteUInt24(this.m_tempGarbagePiles);
		s.WriteUInt24(this.m_tempWaterPollution);
		s.WriteUInt24(this.m_tempDeadCount);
		s.WriteUInt24(this.m_tempSickCount);
		s.WriteUInt24(this.m_finalDeadCount);
		s.WriteUInt24(this.m_finalSickCount);
		s.WriteUInt32(this.m_tempExportAmount);
		s.WriteUInt32(this.m_tempImportAmount);
		s.WriteUInt32(this.m_finalExportAmount);
		s.WriteUInt32(this.m_finalImportAmount);
		s.WriteUInt24(this.m_finalElectricityConsumption);
		s.WriteUInt24(this.m_finalWaterConsumption);
		s.WriteUInt24(this.m_finalSewageAccumulation);
		s.WriteUInt24(this.m_finalGarbageAccumulation);
		s.WriteUInt32(this.m_finalIncomeAccumulation);
		s.WriteUInt16((uint)this.m_finalBuildingCount);
		s.WriteUInt8((uint)this.m_finalGarbagePiles);
		s.WriteUInt8((uint)this.m_finalWaterPollution);
		s.WriteUInt24(this.m_tempHeatingConsumption);
		s.WriteUInt24(this.m_finalHeatingConsumption);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempElectricityConsumption = s.ReadUInt24();
		this.m_tempWaterConsumption = s.ReadUInt24();
		this.m_tempSewageAccumulation = s.ReadUInt24();
		this.m_tempGarbageAccumulation = s.ReadUInt24();
		this.m_tempIncomeAccumulation = s.ReadUInt32();
		this.m_tempBuildingCount = (ushort)s.ReadUInt16();
		this.m_tempGarbagePiles = s.ReadUInt24();
		this.m_tempWaterPollution = s.ReadUInt24();
		if (s.get_version() >= 108u)
		{
			this.m_tempDeadCount = s.ReadUInt24();
			this.m_tempSickCount = s.ReadUInt24();
		}
		else
		{
			this.m_tempDeadCount = 0u;
			this.m_tempSickCount = 0u;
		}
		if (s.get_version() >= 110u)
		{
			this.m_finalDeadCount = s.ReadUInt24();
			this.m_finalSickCount = s.ReadUInt24();
			this.m_tempExportAmount = s.ReadUInt32();
			this.m_tempImportAmount = s.ReadUInt32();
			this.m_finalExportAmount = s.ReadUInt32();
			this.m_finalImportAmount = s.ReadUInt32();
		}
		else
		{
			this.m_finalDeadCount = 0u;
			this.m_finalSickCount = 0u;
			this.m_tempExportAmount = 0u;
			this.m_tempImportAmount = 0u;
			this.m_finalExportAmount = 0u;
			this.m_finalImportAmount = 0u;
		}
		this.m_finalElectricityConsumption = s.ReadUInt24();
		this.m_finalWaterConsumption = s.ReadUInt24();
		this.m_finalSewageAccumulation = s.ReadUInt24();
		this.m_finalGarbageAccumulation = s.ReadUInt24();
		this.m_finalIncomeAccumulation = s.ReadUInt32();
		this.m_finalBuildingCount = (ushort)s.ReadUInt16();
		this.m_finalGarbagePiles = (byte)s.ReadUInt8();
		this.m_finalWaterPollution = (byte)s.ReadUInt8();
		if (s.get_version() >= 234u)
		{
			this.m_tempHeatingConsumption = s.ReadUInt24();
			this.m_finalHeatingConsumption = s.ReadUInt24();
		}
		else
		{
			this.m_tempHeatingConsumption = 0u;
			this.m_finalHeatingConsumption = 0u;
		}
	}

	public uint m_tempElectricityConsumption;

	public uint m_tempHeatingConsumption;

	public uint m_tempWaterConsumption;

	public uint m_tempSewageAccumulation;

	public uint m_tempGarbageAccumulation;

	public uint m_tempIncomeAccumulation;

	public uint m_tempWaterPollution;

	public uint m_tempGarbagePiles;

	public uint m_tempDeadCount;

	public uint m_tempSickCount;

	public uint m_tempExportAmount;

	public uint m_tempImportAmount;

	public uint m_finalElectricityConsumption;

	public uint m_finalHeatingConsumption;

	public uint m_finalWaterConsumption;

	public uint m_finalSewageAccumulation;

	public uint m_finalGarbageAccumulation;

	public uint m_finalIncomeAccumulation;

	public uint m_finalDeadCount;

	public uint m_finalSickCount;

	public uint m_finalExportAmount;

	public uint m_finalImportAmount;

	public ushort m_tempBuildingCount;

	public ushort m_finalBuildingCount;

	public byte m_finalGarbagePiles;

	public byte m_finalWaterPollution;
}
