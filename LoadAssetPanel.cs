﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class LoadAssetPanel : LoadSavePanelBase<CustomAssetMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_SaveList = base.Find<UIListBox>("SaveList");
		this.m_SaveList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnListingSelectionChanged));
		this.m_AssetName = base.Find<UILabel>("AssetName");
		this.m_AssetDesc = base.Find<UILabel>("AssetDesc");
		this.m_AssetDescLabel = base.Find<UILabel>("AssetDescLabel");
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_LoadButton = base.Find<UIButton>("Load");
	}

	private string SafeGetAssetString(string localeID, string assetName, Package package)
	{
		if (package != null)
		{
			string text = package.get_packageName() + "." + assetName + "_Data";
			if (Locale.Exists(localeID, text))
			{
				return Locale.Get(localeID, text);
			}
		}
		return assetName;
	}

	private string SafeGetAssetName(CustomAssetMetaData metaData, Package package)
	{
		string localeID;
		if (metaData.type == CustomAssetMetaData.Type.Building)
		{
			localeID = "BUILDING_TITLE";
		}
		else if (metaData.type == CustomAssetMetaData.Type.Prop)
		{
			localeID = "PROPS_TITLE";
		}
		else if (metaData.type == CustomAssetMetaData.Type.Tree)
		{
			localeID = "TREE_TITLE";
		}
		else
		{
			if (metaData.type != CustomAssetMetaData.Type.Vehicle)
			{
				return metaData.name;
			}
			localeID = "VEHICLE_TITLE";
		}
		return this.SafeGetAssetString(localeID, metaData.name, package);
	}

	private string SafeGetAssetDesc(CustomAssetMetaData metaData, Package package)
	{
		string localeID;
		if (metaData.type == CustomAssetMetaData.Type.Building)
		{
			localeID = "BUILDING_DESC";
		}
		else if (metaData.type == CustomAssetMetaData.Type.Prop)
		{
			localeID = "PROPS_DESC";
		}
		else
		{
			if (metaData.type != CustomAssetMetaData.Type.Tree)
			{
				return metaData.name;
			}
			localeID = "TREE_DESC";
		}
		return this.SafeGetAssetString(localeID, metaData.name, package);
	}

	private bool IsDescRelevant(CustomAssetMetaData metaData, Package package)
	{
		return metaData.type == CustomAssetMetaData.Type.Building || metaData.type == CustomAssetMetaData.Type.Prop || metaData.type == CustomAssetMetaData.Type.Tree;
	}

	private void OnListingSelectionChanged(UIComponent comp, int sel)
	{
		if (sel > -1)
		{
			CustomAssetMetaData listingMetaData = base.GetListingMetaData(sel);
			this.m_AssetName.set_text(this.SafeGetAssetName(listingMetaData, listingMetaData.assetRef.get_package()));
			bool isVisible = this.IsDescRelevant(listingMetaData, listingMetaData.assetRef.get_package());
			this.m_AssetDesc.set_isVisible(isVisible);
			this.m_AssetDescLabel.set_isVisible(isVisible);
			this.m_AssetDesc.set_text(this.SafeGetAssetDesc(listingMetaData, listingMetaData.assetRef.get_package()));
			if (this.m_SnapShot.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShot.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShot.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShot.get_texture() != null)
				{
					this.m_SnapShot.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShot.set_texture(null);
			}
		}
	}

	private bool willClose
	{
		get
		{
			return ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo == null;
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			base.Find<UIButton>("Close").set_tooltipLocaleID((!this.willClose) ? string.Empty : "ASSETIMPORTER_BACKTOMAINMENU");
			this.Refresh();
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveList.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnLoad();
			}
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_SaveList && sel != -1)
		{
			if (this.m_SaveList.get_selectedIndex() != sel)
			{
				this.m_SaveList.set_selectedIndex(sel);
			}
			this.OnLoad();
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("LoadAssetPanel.Refresh()"))
		{
			base.ClearListing();
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.CustomAssetMetaData
			}))
			{
				if (current != null && current.get_isEnabled() && current.get_isMainAsset())
				{
					try
					{
						CustomAssetMetaData customAssetMetaData = current.Instantiate<CustomAssetMetaData>();
						base.AddToListing(current.get_name(), customAssetMetaData.timeStamp, current, customAssetMetaData, true);
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			this.m_SaveList.set_items(base.GetListingItems());
			if (base.GetListingCount() > 0)
			{
				int num = base.FindIndexOf(LoadAssetPanel.m_LastSaveName);
				this.m_SaveList.set_selectedIndex((num == -1) ? 0 : num);
				this.m_LoadButton.set_isEnabled(true);
			}
			else
			{
				this.m_LoadButton.set_isEnabled(false);
			}
		}
	}

	public override void OnClosed()
	{
		base.OnClosed();
		if (this.willClose)
		{
			Singleton<LoadingManager>.get_instance().UnloadLevel();
		}
	}

	public void OnLoad()
	{
		base.CloseEverything();
		LoadAssetPanel.m_LastSaveName = base.GetListingName(this.m_SaveList.get_selectedIndex());
		SaveAssetPanel.lastLoadedName = LoadAssetPanel.m_LastSaveName;
		CustomAssetMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
		base.PrintModsInfo(listingMetaData);
		SaveAssetPanel.lastLoadedAsset = this.SafeGetAssetName(listingMetaData, listingMetaData.assetRef.get_package());
		string listingPackageName = base.GetListingPackageName(this.m_SaveList.get_selectedIndex());
		PublishedFileId invalid = PublishedFileId.invalid;
		ulong num;
		if (ulong.TryParse(listingPackageName, out num))
		{
			invalid..ctor(num);
		}
		Singleton<SimulationManager>.get_instance().m_metaData.m_WorkshopPublishedFileId = invalid;
		Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier = listingMetaData.guid;
		UIView.get_library().Hide(base.GetType().Name, 1);
		GameObject gameObject = GameObject.FindGameObjectWithTag("GameController");
		if (gameObject != null)
		{
			ToolController component = gameObject.GetComponent<ToolController>();
			if (listingMetaData.type == CustomAssetMetaData.Type.Vehicle)
			{
				foreach (Package.Asset current in listingMetaData.assetRef.get_package().FilterAssets(new Package.AssetType[]
				{
					UserAssetType.CustomAssetMetaData
				}))
				{
					CustomAssetMetaData customAssetMetaData = current.Instantiate<CustomAssetMetaData>();
					if (customAssetMetaData.assetRef.get_checksum() != listingMetaData.assetRef.get_checksum())
					{
						GameObject gameObject2 = customAssetMetaData.assetRef.Instantiate<GameObject>();
						gameObject2.set_name(customAssetMetaData.assetRef.get_package().get_packageName() + "." + gameObject2.get_name());
						gameObject2.SetActive(false);
						VehicleInfo component2 = gameObject2.GetComponent<VehicleInfo>();
						if (component2 != null && PrefabCollection<VehicleInfo>.FindLoaded(gameObject2.get_name()) == null)
						{
							PrefabCollection<VehicleInfo>.InitializePrefabs("Custom Assets", component2, null);
							if (component2.m_lodObject != null)
							{
								component2.m_lodObject.SetActive(false);
							}
							PrefabCollection<VehicleInfo>.BindPrefabs();
						}
					}
				}
			}
			else if (listingMetaData.type == CustomAssetMetaData.Type.Building)
			{
				foreach (Package.Asset current2 in listingMetaData.assetRef.get_package().FilterAssets(new Package.AssetType[]
				{
					UserAssetType.CustomAssetMetaData
				}))
				{
					CustomAssetMetaData customAssetMetaData2 = current2.Instantiate<CustomAssetMetaData>();
					if (customAssetMetaData2.type == CustomAssetMetaData.Type.SubBuilding && customAssetMetaData2.assetRef != null)
					{
						GameObject gameObject3 = customAssetMetaData2.assetRef.Instantiate<GameObject>();
						gameObject3.set_name(customAssetMetaData2.assetRef.get_package().get_packageName() + "." + gameObject3.get_name());
						gameObject3.SetActive(false);
						BuildingInfo component3 = gameObject3.GetComponent<BuildingInfo>();
						if (component3 != null && PrefabCollection<BuildingInfo>.FindLoaded(gameObject3.get_name()) == null)
						{
							PrefabCollection<BuildingInfo>.InitializePrefabs("Custom Assets", component3, null);
							if (component3.m_lodObject != null)
							{
								component3.m_lodObject.SetActive(false);
							}
							PrefabCollection<BuildingInfo>.BindPrefabs();
						}
					}
				}
			}
			else if (listingMetaData.type == CustomAssetMetaData.Type.Road)
			{
				foreach (Package.Asset current3 in listingMetaData.assetRef.get_package().FilterAssets(new Package.AssetType[]
				{
					UserAssetType.CustomAssetMetaData
				}))
				{
					CustomAssetMetaData customAssetMetaData3 = current3.Instantiate<CustomAssetMetaData>();
					if (customAssetMetaData3.type == CustomAssetMetaData.Type.Pillar && customAssetMetaData3.assetRef != null)
					{
						GameObject gameObject4 = customAssetMetaData3.assetRef.Instantiate<GameObject>();
						gameObject4.set_name(customAssetMetaData3.assetRef.get_package().get_packageName() + "." + gameObject4.get_name());
						gameObject4.SetActive(false);
						BuildingInfo component4 = gameObject4.GetComponent<BuildingInfo>();
						if (component4 != null)
						{
							if (component4.m_lodObject != null)
							{
								component4.m_lodObject.SetActive(false);
							}
							string replace = (!PrefabCollection<BuildingInfo>.LoadedExists(gameObject4.get_name())) ? null : gameObject4.get_name();
							PrefabCollection<BuildingInfo>.InitializePrefabs("Custom Assets", component4, replace);
							component4.CheckReferences();
							PrefabCollection<BuildingInfo>.BindPrefabs();
						}
					}
				}
				Singleton<BuildingManager>.get_instance().InitRenderData();
				foreach (Package.Asset current4 in listingMetaData.assetRef.get_package().FilterAssets(new Package.AssetType[]
				{
					UserAssetType.CustomAssetMetaData
				}))
				{
					CustomAssetMetaData customAssetMetaData4 = current4.Instantiate<CustomAssetMetaData>();
					if (customAssetMetaData4.type == CustomAssetMetaData.Type.RoadElevation && customAssetMetaData4.assetRef != null)
					{
						GameObject gameObject5 = customAssetMetaData4.assetRef.Instantiate<GameObject>();
						gameObject5.set_name(customAssetMetaData4.assetRef.get_package().get_packageName() + "." + gameObject5.get_name());
						gameObject5.SetActive(false);
						NetInfo component5 = gameObject5.GetComponent<NetInfo>();
						if (component5 != null)
						{
							string replace2 = (!PrefabCollection<NetInfo>.LoadedExists(gameObject5.get_name())) ? null : gameObject5.get_name();
							PrefabCollection<NetInfo>.InitializePrefabs("Custom Assets", component5, replace2);
							component5.CheckReferences();
							PrefabCollection<NetInfo>.BindPrefabs();
						}
					}
				}
			}
			GameObject gameObject6 = listingMetaData.assetRef.Instantiate<GameObject>();
			if (gameObject6 != null)
			{
				PrefabInfo prefabInfo = gameObject6.GetComponent<PrefabInfo>();
				if (prefabInfo.m_Atlas != null && prefabInfo.m_InfoTooltipThumbnail != null && prefabInfo.m_InfoTooltipThumbnail != string.Empty && prefabInfo.m_Atlas.get_Item(prefabInfo.m_InfoTooltipThumbnail) != null)
				{
					prefabInfo.m_InfoTooltipAtlas = prefabInfo.m_Atlas;
				}
				if (prefabInfo is CitizenInfo)
				{
					CitizenInfo citizenInfo = prefabInfo as CitizenInfo;
					citizenInfo.InitializeCustomPrefab(listingMetaData);
					citizenInfo.GetComponent<Animator>().set_cullingMode(0);
				}
				if (prefabInfo is NetInfo)
				{
					gameObject6.set_name(listingMetaData.assetRef.get_package().get_packageName() + "." + gameObject6.get_name());
					string replace3 = (!PrefabCollection<NetInfo>.LoadedExists(gameObject6.get_name())) ? null : gameObject6.get_name();
					NetInfo netInfo = (NetInfo)prefabInfo;
					PrefabCollection<NetInfo>.InitializePrefabs("Custom Assets", netInfo, replace3);
					netInfo.CheckReferences();
					PrefabCollection<NetInfo>.BindPrefabs();
					prefabInfo = AssetEditorRoadUtils.InstantiatePrefab(netInfo);
				}
				else
				{
					prefabInfo.InitializePrefab();
					prefabInfo.CheckReferences();
					prefabInfo.m_prefabInitialized = true;
				}
				component.m_editPrefabInfo = prefabInfo;
				if (listingMetaData.templateName != null)
				{
					if (prefabInfo is BuildingInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<BuildingInfo>.FindLoaded(listingMetaData.templateName);
					}
					else if (prefabInfo is CitizenInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<CitizenInfo>.FindLoaded(listingMetaData.templateName);
					}
					else if (prefabInfo is VehicleInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<VehicleInfo>.FindLoaded(listingMetaData.templateName);
					}
					else if (prefabInfo is PropInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<PropInfo>.FindLoaded(listingMetaData.templateName);
					}
					else if (prefabInfo is TreeInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<TreeInfo>.FindLoaded(listingMetaData.templateName);
					}
					else if (prefabInfo is NetInfo)
					{
						component.m_templatePrefabInfo = PrefabCollection<NetInfo>.FindLoaded(listingMetaData.templateName);
					}
				}
				else
				{
					component.m_templatePrefabInfo = null;
				}
				SaveAssetPanel.lastAssetDescription = this.SafeGetAssetDesc(listingMetaData, listingMetaData.assetRef.get_package());
			}
		}
	}

	public string m_forceEnvironment;

	private static string m_LastSaveName;

	private UILabel m_AssetName;

	private UILabel m_AssetDesc;

	private UILabel m_AssetDescLabel;

	private UITextureSprite m_SnapShot;

	private UIListBox m_SaveList;

	private UIButton m_LoadButton;
}
