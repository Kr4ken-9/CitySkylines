﻿using System;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterThumbnails
{
	public static bool NeedThumbnails(PrefabInfo info)
	{
		VehicleInfo vehicleInfo = info as VehicleInfo;
		BuildingInfo buildingInfo = info as BuildingInfo;
		CitizenInfo citizenInfo = info as CitizenInfo;
		if (buildingInfo != null)
		{
			ItemClass.Service service = buildingInfo.GetService();
			if (service == ItemClass.Service.Residential || service == ItemClass.Service.Commercial || service == ItemClass.Service.Industrial || service == ItemClass.Service.Office)
			{
				return false;
			}
		}
		else
		{
			if (vehicleInfo != null)
			{
				return false;
			}
			if (citizenInfo != null)
			{
				return false;
			}
		}
		return true;
	}

	public static UITextureAtlas CreateThumbnailAtlas(Texture2D[] textures, string name)
	{
		UITextureAtlas uITextureAtlas = ScriptableObject.CreateInstance<UITextureAtlas>();
		uITextureAtlas.set_name("Generated Atlas (" + name + ")");
		Texture2D texture2D = new Texture2D(0, 0, 5, false, false);
		texture2D.set_name("Texture");
		texture2D.set_hideFlags(1);
		Material material = new Material(Shader.Find("UI/Default UI Shader"));
		material.set_name("Material");
		material.set_hideFlags(1);
		material.set_mainTexture(texture2D);
		uITextureAtlas.set_material(material);
		uITextureAtlas.AddTextures(textures);
		return uITextureAtlas;
	}

	public static void CreateThumbnails(GameObject asset, string fileName, PreviewCamera camera)
	{
		if (asset != null && camera != null)
		{
			PrefabInfo component = asset.GetComponent<PrefabInfo>();
			if (component != null)
			{
				string text = component.get_name() + "Thumb";
				Texture2D texture2D = AssetImporterThumbnails.LoadThumbnail(string.Empty, fileName);
				if (texture2D != null)
				{
					texture2D.set_name(text);
					Color32[] pixels = texture2D.GetPixels32();
				}
				else
				{
					Color32[] pixels = AssetImporterThumbnails.GenerateThumbnail(camera);
					texture2D = new Texture2D(AssetImporterThumbnails.thumbWidth, AssetImporterThumbnails.thumbHeight, 5, false, false);
					texture2D.set_name(text);
					texture2D.SetPixels32(pixels);
					texture2D.Apply(false);
				}
				Texture2D texture2D2 = AssetImporterThumbnails.LoadThumbnail("focused", fileName);
				Texture2D texture2D3 = AssetImporterThumbnails.LoadThumbnail("hovered", fileName);
				Texture2D texture2D4 = AssetImporterThumbnails.LoadThumbnail("pressed", fileName);
				Texture2D texture2D5 = AssetImporterThumbnails.LoadThumbnail("disabled", fileName);
				Texture2D[] textures = new Texture2D[]
				{
					texture2D,
					texture2D2,
					texture2D3,
					texture2D4,
					texture2D5
				};
				AssetImporterThumbnails.GenerateMissingThumbnailVariants(ref textures);
				UITextureAtlas atlas = AssetImporterThumbnails.CreateThumbnailAtlas(textures, component.get_name());
				component.m_Thumbnail = text;
				component.m_Atlas = atlas;
			}
		}
	}

	public static void GenerateMissingThumbnailVariants(ref Texture2D[] thumbs)
	{
		if (thumbs[0] == null)
		{
			return;
		}
		Texture2D texture2D = thumbs[0];
		Texture2D texture2D2 = thumbs[1];
		Texture2D texture2D3 = thumbs[2];
		Texture2D texture2D4 = thumbs[3];
		Texture2D texture2D5 = thumbs[4];
		Color32[] array = new Color32[AssetImporterThumbnails.thumbWidth * AssetImporterThumbnails.thumbHeight];
		Color32[] pixels = thumbs[0].GetPixels32();
		if (texture2D2 == null)
		{
			AssetImporterThumbnails.ApplyFilter(pixels, array, (Color32 c) => new Color32(0, 0, 128 + (c.r + c.g + c.b) / 6, c.a));
			texture2D2 = new Texture2D(texture2D.get_width(), texture2D.get_height(), 5, false, false);
			texture2D2.SetPixels32(array);
			texture2D2.Apply(false);
		}
		texture2D2.set_name(texture2D.get_name() + "Focused");
		if (texture2D3 == null)
		{
			AssetImporterThumbnails.ApplyFilter(pixels, array, (Color32 c) => new Color32(128 + c.r / 2, 128 + c.g / 2, 128 + c.b / 2, c.a));
			texture2D3 = new Texture2D(texture2D.get_width(), texture2D.get_height(), 5, false, false);
			texture2D3.SetPixels32(array);
			texture2D3.Apply(false);
		}
		texture2D3.set_name(texture2D.get_name() + "Hovered");
		if (texture2D4 == null)
		{
			AssetImporterThumbnails.ApplyFilter(pixels, array, (Color32 c) => new Color32(192 + c.r / 4, 192 + c.g / 4, 192 + c.b / 4, c.a));
			texture2D4 = new Texture2D(texture2D.get_width(), texture2D.get_height(), 5, false, false);
			texture2D4.SetPixels32(array);
			texture2D4.Apply(false);
		}
		texture2D4.set_name(texture2D.get_name() + "Pressed");
		if (texture2D5 == null)
		{
			AssetImporterThumbnails.ApplyFilter(pixels, array, (Color32 c) => new Color32(0, 0, 0, c.a));
			texture2D5 = new Texture2D(texture2D.get_width(), texture2D.get_height(), 5, false, false);
			texture2D5.SetPixels32(array);
			texture2D5.Apply(false);
		}
		texture2D5.set_name(texture2D.get_name() + "Disabled");
		thumbs[1] = texture2D2;
		thumbs[2] = texture2D3;
		thumbs[3] = texture2D4;
		thumbs[4] = texture2D5;
	}

	private static void ApplyFilter(Color32[] src, Color32[] dst, Func<Color32, Color32> filter)
	{
		for (int i = 0; i < src.Length; i++)
		{
			dst[i] = filter(src[i]);
		}
	}

	private static Color32[] GenerateThumbnail(PreviewCamera camera)
	{
		Texture2D texture2D = new Texture2D(camera.targetTexture.get_width(), camera.targetTexture.get_height(), 5, false, false);
		RenderTexture active = RenderTexture.get_active();
		RenderTexture.set_active(camera.targetTexture);
		camera.m_InfoRenderer.Render();
		texture2D.ReadPixels(new Rect(0f, 0f, (float)camera.targetTexture.get_width(), (float)camera.targetTexture.get_height()), 0, 0, false);
		texture2D.Apply(false);
		RenderTexture.set_active(active);
		Image image = new Image(texture2D.get_width(), texture2D.get_height(), 4, texture2D.GetPixels32());
		image.Resize(AssetImporterThumbnails.thumbWidth, AssetImporterThumbnails.thumbHeight);
		return image.GetColors32(0, 1);
	}

	public static Texture2D LoadThumbnail(string variant, string filename)
	{
		if (filename == null)
		{
			return null;
		}
		string str = "_thumb" + ((!variant.Equals(string.Empty)) ? "_" : string.Empty);
		for (int i = 0; i < AssetImporterThumbnails.SourceTextureExtensions.Length; i++)
		{
			string text = filename + str + variant + AssetImporterThumbnails.SourceTextureExtensions[i];
			if (FileUtils.Exists(text))
			{
				Image image = new Image(text);
				if (image != null && image.get_width() != 0 && image.get_height() != 0)
				{
					image.Resize(AssetImporterThumbnails.thumbWidth, AssetImporterThumbnails.thumbHeight);
					return image.CreateTexture();
				}
			}
		}
		return null;
	}

	public static readonly int thumbWidth = 109;

	public static readonly int thumbHeight = 100;

	protected static readonly string[] SourceTextureExtensions = Image.GetExtensions(94);
}
