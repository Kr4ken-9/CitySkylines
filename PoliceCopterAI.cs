﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PoliceCopterAI : HelicopterAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.CrimeRate)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_POLICE_COPTER_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_POLICE_COPTER_PATROL_WAIT");
		}
		if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_POLICE_COPTER_EMERGENCY");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_POLICE_COPTER_PATROL");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "PoliceCopter";
		current = (int)data.m_transferSize;
		max = this.m_crimeCapacity;
		if ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && current == max)
		{
			current = max - 1;
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~(Vehicle.Flags.Emergency2 | Vehicle.Flags.Landing | Vehicle.Flags.WaitingTarget);
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 0f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 0f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			data.m_flags &= ~Vehicle.Flags.Landing;
			if (PoliceCopterAI.CountCriminals(targetBuilding) != 0)
			{
				data.m_flags |= Vehicle.Flags.Emergency2;
			}
			else
			{
				data.m_flags &= ~Vehicle.Flags.Emergency2;
			}
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else
		{
			data.m_flags &= ~Vehicle.Flags.Emergency2;
			if ((int)data.m_transferSize < this.m_crimeCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 7;
				offer.Vehicle = vehicleID;
				offer.Position = data.GetLastFramePosition();
				offer.Amount = 1;
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
				data.m_flags |= Vehicle.Flags.WaitingTarget;
			}
			else
			{
				data.m_flags &= ~Vehicle.Flags.Landing;
				data.m_flags |= Vehicle.Flags.GoingBack;
			}
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public static int CountCriminals(ushort building)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		uint num = instance.m_buildings.m_buffer[(int)building].m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Criminal && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == building)
				{
					num3++;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num3;
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_crimeCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		frameData.m_blinkState = (((vehicleData.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0) ? 0f : 10f);
		this.TryCollectCrime(vehicleID, ref vehicleData, ref frameData);
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			if (this.CanLeave(vehicleID, ref vehicleData))
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
				vehicleData.m_flags |= Vehicle.Flags.Leaving;
			}
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && ((int)vehicleData.m_transferSize >= this.m_crimeCapacity || this.ShouldReturnToSource(vehicleID, ref vehicleData)))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		int num = -this.m_crimeCapacity;
		BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
		info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		data.m_transferSize = 0;
		this.RemoveSource(vehicleID, ref data);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 0f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 targetPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out targetPos2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, info2.m_size.y + 10f));
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 endPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
			return base.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2, info2.m_size.y + 10f);
		}
		return false;
	}

	private void TryCollectCrime(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) == Vehicle.Flags.Underground)
		{
			return;
		}
		Vector3 position = frameData.m_position;
		float num = position.x - 64f;
		float num2 = position.z - 64f;
		float num3 = position.x + 64f;
		float num4 = position.z + 64f;
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					this.TryCollectCrime(vehicleID, ref vehicleData, ref frameData, num9, ref instance.m_buildings.m_buffer[(int)num9]);
					num9 = instance.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private void TryCollectCrime(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort buildingID, ref Building building)
	{
		Vector3 vector = building.CalculateSidewalkPosition();
		if (VectorUtils.LengthSqrXZ(vector - frameData.m_position) < 4096f)
		{
			int num = -this.m_crimeCapacity;
			BuildingInfo info = building.Info;
			info.m_buildingAI.ModifyMaterialBuffer(buildingID, ref building, TransferManager.TransferReason.Crime, ref num);
			vehicleData.m_transferSize = (ushort)Mathf.Clamp((int)vehicleData.m_transferSize - num, 0, this.m_crimeCapacity);
		}
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	[CustomizableProperty("Crime capacity")]
	public int m_crimeCapacity = 50000;
}
