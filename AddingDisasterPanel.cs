﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class AddingDisasterPanel : ToolsModifierControl
{
	public static AddingDisasterPanel instance
	{
		get
		{
			if (AddingDisasterPanel.m_instance == null)
			{
				AddingDisasterPanel.m_instance = UIView.Find<UIPanel>("AddingDisasterPanel").GetComponent<AddingDisasterPanel>();
			}
			return AddingDisasterPanel.m_instance;
		}
	}

	private void Awake()
	{
		AddingDisasterPanel.m_instance = UIView.Find<UIPanel>("AddingDisasterPanel").GetComponent<AddingDisasterPanel>();
		this.m_label = base.Find<UILabel>("AddingDisasterLabel");
		this.Hide();
	}

	public void SelectDisaster(ushort disasterID)
	{
		this.m_currentEffectItem.OnDisasterSelected(disasterID);
		this.Hide();
	}

	public void Show(EffectItemDisaster effectItem)
	{
		this.m_label.set_text(LocaleFormatter.FormatGeneric("SCENARIOEDITOR_ADDINGDISASTERFOR", new object[]
		{
			SelectedTrigger.instance.m_triggerMilestone.GetLocalizedName()
		}));
		base.get_component().set_isVisible(true);
		this.m_currentEffectItem = effectItem;
	}

	public void Hide()
	{
		base.get_component().set_isVisible(false);
		this.m_currentEffectItem = null;
	}

	private void Update()
	{
		if (this.m_currentEffectItem != null)
		{
			DisasterTool currentTool = ToolsModifierControl.GetCurrentTool<DisasterTool>();
			if (currentTool == null)
			{
				this.Hide();
			}
		}
	}

	private static AddingDisasterPanel m_instance;

	private UILabel m_label;

	[HideInInspector]
	public EffectItemDisaster m_currentEffectItem;
}
