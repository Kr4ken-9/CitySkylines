﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class DistrictPolicyCollection : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(DistrictPolicyCollection.InitializePolicies(base.get_gameObject().get_name(), this));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading(base.get_gameObject().get_name());
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		for (int i = 0; i < this.m_policies.Length; i++)
		{
			instance.UnloadPolicy(this.m_policies[i]);
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private static IEnumerator InitializePolicies(string name, DistrictPolicyCollection collection)
	{
		DistrictPolicyCollection.<InitializePolicies>c__Iterator0 <InitializePolicies>c__Iterator = new DistrictPolicyCollection.<InitializePolicies>c__Iterator0();
		<InitializePolicies>c__Iterator.collection = collection;
		return <InitializePolicies>c__Iterator;
	}

	public DistrictPolicies.Policies[] m_policies;

	[NonSerialized]
	public BuildingInfoSub[] m_subInfos;
}
