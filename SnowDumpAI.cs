﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using UnityEngine;

public class SnowDumpAI : PlayerBuildingAI
{
	public override void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		if (this.m_pollutionAccumulation != 0)
		{
			resource1 = NaturalResourceManager.Resource.Pollution;
			position1 = data.m_position;
			radius1 = this.m_pollutionRadius;
		}
		else
		{
			resource1 = NaturalResourceManager.Resource.None;
			position1 = data.m_position;
			radius1 = 0f;
		}
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Pollution)
		{
			int pollutionAccumulation = this.m_pollutionAccumulation;
			return ColorUtils.LinearLerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor, Mathf.Clamp01((float)pollutionAccumulation * 0.04f));
		}
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int noiseAccumulation = this.m_noiseAccumulation;
			return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
		}
		if (infoMode != InfoManager.InfoMode.Snow)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		if (resource == NaturalResourceManager.Resource.Pollution)
		{
			int num = (int)(data.m_customBuffer1 / 100);
			return (num * this.m_pollutionAccumulation + 99) / 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public int GetSnowAmount(ushort buildingID, ref Building data)
	{
		return Mathf.Min(this.m_snowCapacity, (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer));
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Snow, ref num, ref num2, ref num3, ref num4);
		int num5 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		return StringUtils.SafeFormat("Snow: {0} (+{1})", new object[]
		{
			num5,
			num2
		});
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Snow;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.SetPrefab(this.m_info);
		}
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data))
		{
			return Locale.Get("BUILDING_STATUS_FULL");
		}
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EMPTYING");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data) && this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.Relock();
		}
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
		data.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
		this.CheckCapacity(buildingID, ref data);
		this.SetEmptying(buildingID, ref data, (data.m_flags & Building.Flags.Downgrading) != Building.Flags.None);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainGarbage, position, 1.5f);
		if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Sad, NaturalResourceManager.Resource.Pollution, (float)this.m_pollutionAccumulation, this.m_pollutionRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseGarbage, position, 1.5f);
			if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Happy, NaturalResourceManager.Resource.Pollution, (float)(-(float)this.m_pollutionAccumulation), this.m_pollutionRadius);
			}
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			if (this.IsFull(buildingID, ref buildingData))
			{
				Singleton<BuildingManager>.get_instance().m_snowDumpFull.Activate(properties.m_snowDumpFull, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_snowDumpFull.Deactivate(buildingID, false);
			}
		}
		if (buildingData.m_customBuffer1 >= 100)
		{
			int num = (int)(buildingData.m_customBuffer1 / 100);
			num = (num * this.m_pollutionAccumulation + 99) / 100;
			if (num != 0)
			{
				Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num, num, buildingData.m_position, this.m_pollutionRadius);
			}
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		int num2 = Mathf.Min(this.m_snowCapacity, (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer));
		District[] expr_E3_cp_0_cp_0 = instance.m_districts.m_buffer;
		byte expr_E3_cp_0_cp_1 = district;
		expr_E3_cp_0_cp_0[(int)expr_E3_cp_0_cp_1].m_productionData.m_tempSnowAmount = expr_E3_cp_0_cp_0[(int)expr_E3_cp_0_cp_1].m_productionData.m_tempSnowAmount + (uint)num2;
		District[] expr_107_cp_0_cp_0 = instance.m_districts.m_buffer;
		byte expr_107_cp_0_cp_1 = district;
		expr_107_cp_0_cp_0[(int)expr_107_cp_0_cp_1].m_productionData.m_tempSnowCapacity = expr_107_cp_0_cp_0[(int)expr_107_cp_0_cp_1].m_productionData.m_tempSnowCapacity + (uint)this.m_snowCapacity;
		this.CheckCapacity(buildingID, ref buildingData);
	}

	public override ToolBase.ToolErrors CheckBulldozing(ushort buildingID, ref Building data)
	{
		int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		if (num != 0 && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			return ToolBase.ToolErrors.NotEmpty;
		}
		return base.CheckBulldozing(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Snow)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else if (material == TransferManager.TransferReason.SnowMove)
		{
			VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo2 != null)
			{
				Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num2;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, data.m_position, material, false, true))
				{
					randomVehicleInfo2.m_vehicleAI.SetSource(num2, ref vehicles2.m_buffer[(int)num2], buildingID);
					randomVehicleInfo2.m_vehicleAI.StartTransfer(num2, ref vehicles2.m_buffer[(int)num2], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.Snow)
		{
			bool flag = this.IsFull(buildingID, ref data);
			int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
			amountDelta = Mathf.Max(amountDelta, 0);
			num += amountDelta;
			data.m_customBuffer1 = (ushort)(num / 1000);
			data.m_garbageBuffer = (ushort)(num - (int)(data.m_customBuffer1 * 1000));
			bool flag2 = this.IsFull(buildingID, ref data);
			if (flag != flag2)
			{
				if (flag2)
				{
					if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Unlock();
					}
				}
				else if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Relock();
				}
			}
		}
		else if (material == TransferManager.TransferReason.SnowMove)
		{
			bool flag3 = this.IsFull(buildingID, ref data);
			int num2 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
			amountDelta = Mathf.Max(amountDelta, -num2);
			num2 += amountDelta;
			data.m_customBuffer1 = (ushort)(num2 / 1000);
			data.m_garbageBuffer = (ushort)(num2 - (int)(data.m_customBuffer1 * 1000));
			bool flag4 = this.IsFull(buildingID, ref data);
			if (flag3 != flag4)
			{
				if (flag4)
				{
					if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Unlock();
					}
				}
				else if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Relock();
				}
			}
		}
		else if (material == TransferManager.TransferReason.Garbage)
		{
			amountDelta = 0;
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override int GetGarbageAmount(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Snow)
		{
			amount = 0;
			max = 0;
		}
		else if (material == TransferManager.TransferReason.Garbage)
		{
			amount = 0;
			max = 0;
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None || data.m_fireIntensity != 0)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_collectRadius * 0.01f;
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Snow, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.SnowMove, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.SnowMove, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Notification.Problem problem = buildingData.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			if (buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		buildingData.m_problems = problem;
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			bool flag = this.IsFull(buildingID, ref buildingData);
			int num = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer);
			int num2 = Mathf.Min(num, (finalProductionRate * this.m_snowConsumption + 99) / 100);
			if (num2 > 0)
			{
				num -= num2;
				buildingData.m_customBuffer1 = (ushort)(num / 1000);
				buildingData.m_garbageBuffer = (ushort)(num - (int)(buildingData.m_customBuffer1 * 1000));
			}
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
			int num3 = (finalProductionRate * this.m_snowTruckCount + 99) / 100;
			if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
			{
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				int num7 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.SnowMove, ref num4, ref num5, ref num6, ref num7);
				if (num > 0 && num4 < num3)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.SnowMove, offer);
				}
			}
			else
			{
				int num8 = 0;
				int num9 = 0;
				int num10 = 0;
				int num11 = 0;
				int num12 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Snow, ref num8, ref num10, ref num11, ref num12);
				base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.SnowMove, ref num9, ref num10, ref num11, ref num12);
				int num13 = this.m_snowCapacity - num;
				if (this.m_snowConsumption == 0)
				{
					num13 -= num11;
				}
				int num14 = 1;
				int num15 = num13 / 5000;
				bool flag2 = num13 >= num14 && (num15 > 1 || num8 >= num3 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0);
				bool flag3 = num13 >= num14 && num8 < num3 && (num15 > 1 || !flag2);
				if (flag2)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					if (this.m_snowConsumption != 0)
					{
						offer2.Priority = Mathf.Max(1, num13 * 6 / this.m_snowCapacity);
					}
					else
					{
						offer2.Priority = 0;
					}
					offer2.Building = buildingID;
					offer2.Position = buildingData.m_position;
					offer2.Amount = Mathf.Max(1, num15 - 1);
					offer2.Active = false;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.SnowMove, offer2);
				}
				if (flag3)
				{
					TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
					offer3.Priority = 2 - num8;
					offer3.Building = buildingID;
					offer3.Position = buildingData.m_position;
					offer3.Amount = 1;
					offer3.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Snow, offer3);
				}
			}
			bool flag4 = this.IsFull(buildingID, ref buildingData);
			if (flag != flag4)
			{
				if (flag4)
				{
					if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Unlock();
					}
				}
				else if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Relock();
				}
			}
			int num16 = finalProductionRate * this.m_noiseAccumulation / 100;
			if (num16 != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num16, buildingData.m_position, this.m_noiseRadius);
			}
		}
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	private void CheckCapacity(ushort buildingID, ref Building buildingData)
	{
		int num = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer);
		if (num >= this.m_snowCapacity)
		{
			if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityFull)
			{
				buildingData.m_flags |= Building.Flags.CapacityFull;
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
				Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			}
		}
		else if (num >= this.m_snowCapacity >> 1)
		{
			if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityStep1)
			{
				buildingData.m_flags = ((buildingData.m_flags & ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2)) | Building.Flags.CapacityStep1);
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
				Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			}
		}
		else if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.None)
		{
			buildingData.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
			Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		}
	}

	public override void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
		Notification.Problem problem = data.m_problems;
		Notification.Problem problems = data.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if (emptying)
		{
			data.m_flags |= Building.Flags.Downgrading;
			if (data.m_customBuffer1 * 1000 + data.m_garbageBuffer == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		else
		{
			data.m_flags &= ~Building.Flags.Downgrading;
		}
		data.m_problems = problem;
		if (problem != problems)
		{
			Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, problem);
			Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void UpdateGuide(GuideController guideController)
	{
		if (this.m_info.m_notUsedGuide != null && Singleton<NetManager>.get_instance().m_lastMaxWetness >= 128)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				this.m_info.m_notUsedGuide.Activate(guideController.m_snowDumpNotUsed, this.m_info);
			}
		}
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = this.m_pollutionAccumulation;
		noise = this.m_noiseAccumulation;
	}

	public override bool IsFull(ushort buildingID, ref Building data)
	{
		int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		return num >= this.m_snowCapacity;
	}

	public override bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		return num == 0;
	}

	public override bool CanBeEmptied()
	{
		return true;
	}

	public override bool CanBeEmptied(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			return true;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
		int size = serviceBuildings.m_size;
		ushort[] buffer = serviceBuildings.m_buffer;
		if (buffer != null && size <= buffer.Length)
		{
			for (int i = 0; i < size; i++)
			{
				ushort num = buffer[i];
				if (num != 0 && num != buildingID)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
					if (info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_level == this.m_info.m_class.m_level && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None && instance.m_buildings.m_buffer[(int)num].m_productionRate != 0 && !info.m_buildingAI.IsFull(num, ref instance.m_buildings.m_buffer[(int)num]))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		string text2 = LocaleFormatter.FormatGeneric("AIINFO_CAPACITY", new object[]
		{
			this.m_snowCapacity
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_SNOWTRUCK_CAPACITY", new object[]
		{
			this.m_snowTruckCount
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			text2
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num = (productionRate * this.m_snowTruckCount + 99) / 100;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.SnowMove, ref num2, ref num3, ref num4, ref num5);
		}
		else
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Snow, ref num2, ref num3, ref num4, ref num5);
		}
		string str = string.Empty;
		int snowAmount = this.GetSnowAmount(buildingID, ref data);
		int num6 = 0;
		if (snowAmount != 0)
		{
			num6 = Mathf.Max(1, snowAmount * 100 / this.m_snowCapacity);
		}
		str = str + LocaleFormatter.FormatGeneric("AIINFO_FULL", new object[]
		{
			num6
		}) + Environment.NewLine;
		return str + LocaleFormatter.FormatGeneric("AIINFO_SNOW_TRUCKS", new object[]
		{
			num2,
			num
		});
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 2;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 2;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 1;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3;

	[CustomizableProperty("Snow Plow Count")]
	public int m_snowTruckCount = 3;

	[CustomizableProperty("Snow Capacity")]
	public int m_snowCapacity = 10000000;

	[CustomizableProperty("Snow Consumption")]
	public int m_snowConsumption = 1000;

	[CustomizableProperty("Pollution Accumulation", "Pollution")]
	public int m_pollutionAccumulation = 100;

	[CustomizableProperty("Pollution Radius", "Pollution")]
	public float m_pollutionRadius = 150f;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation = 50;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;

	[CustomizableProperty("Collect Radius")]
	public float m_collectRadius = 2000f;

	public ManualMilestone m_fullPassMilestone;
}
