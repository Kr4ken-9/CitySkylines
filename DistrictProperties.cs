﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

[ExecuteInEditMode]
public class DistrictProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		DistrictProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new DistrictProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("DistrictProperties");
			Singleton<DistrictManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Material m_areaMaterial;

	public UIFont m_areaNameFont;

	public Shader m_areaNameShader;

	public Shader m_areaIconShader;

	public UITextureAtlas m_areaIconAtlas;

	public Color m_areaNameColor = Color.get_white();

	public Color m_areaNameColorInfo = Color.get_black();
}
