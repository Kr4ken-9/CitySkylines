﻿using System;
using ColossalFramework.UI;

public sealed class PublicTransportPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.PublicTransport;
		}
	}

	private bool IsRoadEligibleToPublicTransport(NetInfo info)
	{
		string category = base.category;
		switch (category)
		{
		case "PublicTransportTrain":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None;
		case "PublicTransportMetro":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Metro) != VehicleInfo.VehicleType.None;
		case "PublicTransportBus":
			return (byte)(info.m_laneTypes & NetInfo.LaneType.TransportVehicle) != 0;
		case "PublicTransportTram":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None;
		case "PublicTransportShip":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Ferry) != VehicleInfo.VehicleType.None;
		case "PublicTransportPlane":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Blimp) != VehicleInfo.VehicleType.None;
		case "PublicTransportMonorail":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.Monorail) != VehicleInfo.VehicleType.None;
		case "PublicTransportCableCar":
			return (info.m_vehicleTypes & VehicleInfo.VehicleType.CableCar) != VehicleInfo.VehicleType.None;
		}
		return false;
	}

	protected override bool IsCategoryValid(NetInfo info, bool ignoreCategories)
	{
		return this.IsRoadEligibleToPublicTransport(info);
	}

	protected override bool IsServiceValid(NetInfo info)
	{
		bool flag = info.GetService() == this.service || info.GetService() == ItemClass.Service.Road;
		return flag && this.IsRoadEligibleToPublicTransport(info);
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)7, new Comparison<PrefabInfo>(base.ItemsTypeReverseSort));
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		NetInfo netInfo = objectUserData as NetInfo;
		TransportInfo transportInfo = objectUserData as TransportInfo;
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				base.HideAllOptionPanels();
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
		if (transportInfo != null)
		{
			TransportTool transportTool = ToolsModifierControl.SetTool<TransportTool>();
			if (transportTool != null)
			{
				base.HideAllOptionPanels();
				transportTool.m_prefab = transportInfo;
				transportTool.m_building = 0;
			}
		}
		if (netInfo != null)
		{
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				ItemClass.SubService subService = netInfo.GetSubService();
				if (subService == ItemClass.SubService.PublicTransportMetro)
				{
					base.ShowTunnelsOptionPanel();
				}
				else if (subService == ItemClass.SubService.PublicTransportTrain || subService == ItemClass.SubService.PublicTransportTram || subService == ItemClass.SubService.PublicTransportMonorail)
				{
					base.ShowTracksOptionPanel();
				}
				else if (subService == ItemClass.SubService.PublicTransportShip || subService == ItemClass.SubService.PublicTransportPlane)
				{
					base.ShowQuaysOptionPanel();
				}
				else if (subService == ItemClass.SubService.PublicTransportCableCar)
				{
					base.ShowPowerLinesOptionPanel();
				}
				else
				{
					base.ShowRoadsOptionPanel();
				}
				netTool.Prefab = netInfo;
			}
		}
	}
}
