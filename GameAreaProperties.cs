﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class GameAreaProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		GameAreaProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new GameAreaProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("GameAreaProperties");
			Singleton<GameAreaManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Material m_borderMaterial;

	public Material m_areaMaterial;

	public Material m_decorationMaterial;

	public Texture2D m_directionArrow;

	public MessageInfo m_unlockMessage;

	public int[] m_baseAreaPrices;

	public int m_roadConnectionPrice;

	public int m_trainConnectionPrice;

	public int m_shipConnectionPrice;

	public int m_planeConnectionPrice;

	public int m_forestResourcePrice;

	public int m_fertilityResourcePrice;

	public int m_oreResourcePrice;

	public int m_oilResourcePrice;

	public int m_waterAreaPrice;

	public int m_buildableAreaPrice;
}
