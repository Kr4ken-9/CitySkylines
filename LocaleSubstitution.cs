﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;

public static class LocaleSubstitution
{
	private static Locale.Key GetKey(string id)
	{
		Locale.Key result = default(Locale.Key);
		result.m_Identifier = id;
		return result;
	}

	private static Locale.Key GetKey(string id, string key)
	{
		Locale.Key result = default(Locale.Key);
		result.m_Identifier = id;
		result.m_Key = key;
		return result;
	}

	private static Locale.Key GetKey(string id, int index)
	{
		Locale.Key result = default(Locale.Key);
		result.m_Identifier = id;
		result.m_Index = index;
		return result;
	}

	private static Locale.Key GetKey(string id, string key, int index)
	{
		Locale.Key result = default(Locale.Key);
		result.m_Identifier = id;
		result.m_Key = key;
		result.m_Index = index;
		return result;
	}

	public static LocaleManager.SubstitutionSetting[] SubstitutionRules(string codeString)
	{
		if (PlatformService.get_platformType() == 1)
		{
			return LocaleSubstitution.tgpSubstitution;
		}
		if (PlatformService.get_platformType() == 2)
		{
			return LocaleSubstitution.qqSubstitution;
		}
		return null;
	}

	static LocaleSubstitution()
	{
		// Note: this type is marked as 'beforefieldinit'.
		LocaleManager.SubstitutionSetting[] expr_07 = new LocaleManager.SubstitutionSetting[28];
		int arg_3A_0_cp_1 = 0;
		LocaleManager.SubstitutionSetting substitutionSetting = default(LocaleManager.SubstitutionSetting);
		substitutionSetting.source = LocaleSubstitution.GetKey("LOADING_TIP_TITLE_TGP");
		substitutionSetting.target = LocaleSubstitution.GetKey("LOADING_TIP_TITLE", 7);
		expr_07[arg_3A_0_cp_1] = substitutionSetting;
		int arg_72_0_cp_1 = 1;
		LocaleManager.SubstitutionSetting substitutionSetting2 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting2.source = LocaleSubstitution.GetKey("LOADING_TIP_TEXT_TGP");
		substitutionSetting2.target = LocaleSubstitution.GetKey("LOADING_TIP_TEXT", 7);
		expr_07[arg_72_0_cp_1] = substitutionSetting2;
		int arg_A9_0_cp_1 = 2;
		LocaleManager.SubstitutionSetting substitutionSetting3 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting3.source = LocaleSubstitution.GetKey("WORKSHOP_WORKSHOP_TGP");
		substitutionSetting3.target = LocaleSubstitution.GetKey("WORKSHOP_WORKSHOP");
		expr_07[arg_A9_0_cp_1] = substitutionSetting3;
		int arg_E0_0_cp_1 = 3;
		LocaleManager.SubstitutionSetting substitutionSetting4 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting4.source = LocaleSubstitution.GetKey("CONTENTMANAGER_ACHIEVEMENTS_TGP");
		substitutionSetting4.target = LocaleSubstitution.GetKey("CONTENTMANAGER_ACHIEVEMENTS");
		expr_07[arg_E0_0_cp_1] = substitutionSetting4;
		int arg_118_0_cp_1 = 4;
		LocaleManager.SubstitutionSetting substitutionSetting5 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting5.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMOD_TGP");
		substitutionSetting5.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMOD");
		expr_07[arg_118_0_cp_1] = substitutionSetting5;
		int arg_150_0_cp_1 = 5;
		LocaleManager.SubstitutionSetting substitutionSetting6 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting6.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSAVE_TGP");
		substitutionSetting6.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSAVE");
		expr_07[arg_150_0_cp_1] = substitutionSetting6;
		int arg_188_0_cp_1 = 6;
		LocaleManager.SubstitutionSetting substitutionSetting7 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting7.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMAP_TGP");
		substitutionSetting7.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMAP");
		expr_07[arg_188_0_cp_1] = substitutionSetting7;
		int arg_1C0_0_cp_1 = 7;
		LocaleManager.SubstitutionSetting substitutionSetting8 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting8.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHASSET_TGP");
		substitutionSetting8.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHASSET");
		expr_07[arg_1C0_0_cp_1] = substitutionSetting8;
		int arg_1F8_0_cp_1 = 8;
		LocaleManager.SubstitutionSetting substitutionSetting9 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting9.source = LocaleSubstitution.GetKey("CONTENTMANAGER_STEAMWORKSHOP_TGP");
		substitutionSetting9.target = LocaleSubstitution.GetKey("CONTENTMANAGER_STEAMWORKSHOP");
		expr_07[arg_1F8_0_cp_1] = substitutionSetting9;
		int arg_231_0_cp_1 = 9;
		LocaleManager.SubstitutionSetting substitutionSetting10 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting10.source = LocaleSubstitution.GetKey("SAVEMAP_USECLOUD_TGP");
		substitutionSetting10.target = LocaleSubstitution.GetKey("SAVEMAP_USECLOUD");
		expr_07[arg_231_0_cp_1] = substitutionSetting10;
		int arg_26A_0_cp_1 = 10;
		LocaleManager.SubstitutionSetting substitutionSetting11 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting11.source = LocaleSubstitution.GetKey("SAVEGAME_CLOUDUNSUPPORTED_TGP");
		substitutionSetting11.target = LocaleSubstitution.GetKey("SAVEGAME_CLOUDUNSUPPORTED");
		expr_07[arg_26A_0_cp_1] = substitutionSetting11;
		int arg_2A8_0_cp_1 = 11;
		LocaleManager.SubstitutionSetting substitutionSetting12 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting12.source = LocaleSubstitution.GetKey("CONTENT_CONFIRM_WORKSHOPDELETE_TGP");
		substitutionSetting12.target = LocaleSubstitution.GetKey("CONTENT_CONFIRM_WORKSHOPDELETE", "Message");
		expr_07[arg_2A8_0_cp_1] = substitutionSetting12;
		int arg_2E1_0_cp_1 = 12;
		LocaleManager.SubstitutionSetting substitutionSetting13 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting13.source = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_UPDATE_TGP");
		substitutionSetting13.target = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_UPDATE");
		expr_07[arg_2E1_0_cp_1] = substitutionSetting13;
		int arg_31A_0_cp_1 = 13;
		LocaleManager.SubstitutionSetting substitutionSetting14 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting14.source = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_TGP");
		substitutionSetting14.target = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED");
		expr_07[arg_31A_0_cp_1] = substitutionSetting14;
		int arg_353_0_cp_1 = 14;
		LocaleManager.SubstitutionSetting substitutionSetting15 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting15.source = LocaleSubstitution.GetKey("CONTENT_VIEW_TGP");
		substitutionSetting15.target = LocaleSubstitution.GetKey("CONTENT_VIEW");
		expr_07[arg_353_0_cp_1] = substitutionSetting15;
		int arg_38C_0_cp_1 = 15;
		LocaleManager.SubstitutionSetting substitutionSetting16 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting16.source = LocaleSubstitution.GetKey("WORKSHOP_TITLE_TGP");
		substitutionSetting16.target = LocaleSubstitution.GetKey("WORKSHOP_TITLE");
		expr_07[arg_38C_0_cp_1] = substitutionSetting16;
		int arg_3C5_0_cp_1 = 16;
		LocaleManager.SubstitutionSetting substitutionSetting17 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting17.source = LocaleSubstitution.GetKey("WORKSHOP_DESC_TGP");
		substitutionSetting17.target = LocaleSubstitution.GetKey("WORKSHOP_DESC");
		expr_07[arg_3C5_0_cp_1] = substitutionSetting17;
		int arg_3FE_0_cp_1 = 17;
		LocaleManager.SubstitutionSetting substitutionSetting18 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting18.source = LocaleSubstitution.GetKey("CONTENTMANAGER_USINGCLOUD_TGP");
		substitutionSetting18.target = LocaleSubstitution.GetKey("CONTENTMANAGER_USINGCLOUD");
		expr_07[arg_3FE_0_cp_1] = substitutionSetting18;
		int arg_437_0_cp_1 = 18;
		LocaleManager.SubstitutionSetting substitutionSetting19 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting19.source = LocaleSubstitution.GetKey("CONTENT_AD_BUY_TGP");
		substitutionSetting19.target = LocaleSubstitution.GetKey("CONTENT_AD_BUY");
		expr_07[arg_437_0_cp_1] = substitutionSetting19;
		int arg_470_0_cp_1 = 19;
		LocaleManager.SubstitutionSetting substitutionSetting20 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting20.source = LocaleSubstitution.GetKey("LOADPANEL_ACHSTATUS_WORKSHOP_TGP");
		substitutionSetting20.target = LocaleSubstitution.GetKey("LOADPANEL_ACHSTATUS_WORKSHOP");
		expr_07[arg_470_0_cp_1] = substitutionSetting20;
		int arg_4AE_0_cp_1 = 20;
		LocaleManager.SubstitutionSetting substitutionSetting21 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting21.source = LocaleSubstitution.GetKey("CONFIRM_UNSUBSCRIBEALL_TGP");
		substitutionSetting21.target = LocaleSubstitution.GetKey("CONFIRM_UNSUBSCRIBEALL", "Message");
		expr_07[arg_4AE_0_cp_1] = substitutionSetting21;
		int arg_4E7_0_cp_1 = 21;
		LocaleManager.SubstitutionSetting substitutionSetting22 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting22.source = LocaleSubstitution.GetKey("CONTENTMANAGER_SUBS_TGP");
		substitutionSetting22.target = LocaleSubstitution.GetKey("CONTENTMANAGER_SUBS");
		expr_07[arg_4E7_0_cp_1] = substitutionSetting22;
		int arg_520_0_cp_1 = 22;
		LocaleManager.SubstitutionSetting substitutionSetting23 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting23.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSTYLE_TGP");
		substitutionSetting23.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSTYLE");
		expr_07[arg_520_0_cp_1] = substitutionSetting23;
		int arg_55E_0_cp_1 = 23;
		LocaleManager.SubstitutionSetting substitutionSetting24 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting24.source = LocaleSubstitution.GetKey("DISCLAIMER_MESSAGE_TGP");
		substitutionSetting24.target = LocaleSubstitution.GetKey("DISCLAIMER_MESSAGE", "agreedModsDisclaimer");
		expr_07[arg_55E_0_cp_1] = substitutionSetting24;
		int arg_597_0_cp_1 = 24;
		LocaleManager.SubstitutionSetting substitutionSetting25 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting25.source = LocaleSubstitution.GetKey("CONTENT_SF_BUY_TGP");
		substitutionSetting25.target = LocaleSubstitution.GetKey("CONTENT_SF_BUY");
		expr_07[arg_597_0_cp_1] = substitutionSetting25;
		int arg_5D5_0_cp_1 = 25;
		LocaleManager.SubstitutionSetting substitutionSetting26 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting26.source = LocaleSubstitution.GetKey("NEWMAP_UNPUBLISHED_THEME_TGP");
		substitutionSetting26.target = LocaleSubstitution.GetKey("NEWMAP_UNPUBLISHED_THEME", "Message");
		expr_07[arg_5D5_0_cp_1] = substitutionSetting26;
		int arg_60E_0_cp_1 = 26;
		LocaleManager.SubstitutionSetting substitutionSetting27 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting27.source = LocaleSubstitution.GetKey("CONTENT_ND_BUY_TGP");
		substitutionSetting27.target = LocaleSubstitution.GetKey("CONTENT_ND_BUY");
		expr_07[arg_60E_0_cp_1] = substitutionSetting27;
		int arg_647_0_cp_1 = 27;
		LocaleManager.SubstitutionSetting substitutionSetting28 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting28.source = LocaleSubstitution.GetKey("NEWSFEED_ERROR_TGP");
		substitutionSetting28.target = LocaleSubstitution.GetKey("NEWSFEED_ERROR");
		expr_07[arg_647_0_cp_1] = substitutionSetting28;
		LocaleSubstitution.tgpSubstitution = expr_07;
		LocaleManager.SubstitutionSetting[] expr_658 = new LocaleManager.SubstitutionSetting[28];
		int arg_68C_0_cp_1 = 0;
		LocaleManager.SubstitutionSetting substitutionSetting29 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting29.source = LocaleSubstitution.GetKey("LOADING_TIP_TITLE_QQ");
		substitutionSetting29.target = LocaleSubstitution.GetKey("LOADING_TIP_TITLE", 7);
		expr_658[arg_68C_0_cp_1] = substitutionSetting29;
		int arg_6C5_0_cp_1 = 1;
		LocaleManager.SubstitutionSetting substitutionSetting30 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting30.source = LocaleSubstitution.GetKey("LOADING_TIP_TEXT_QQ");
		substitutionSetting30.target = LocaleSubstitution.GetKey("LOADING_TIP_TEXT", 7);
		expr_658[arg_6C5_0_cp_1] = substitutionSetting30;
		int arg_6FD_0_cp_1 = 2;
		LocaleManager.SubstitutionSetting substitutionSetting31 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting31.source = LocaleSubstitution.GetKey("WORKSHOP_WORKSHOP_QQ");
		substitutionSetting31.target = LocaleSubstitution.GetKey("WORKSHOP_WORKSHOP");
		expr_658[arg_6FD_0_cp_1] = substitutionSetting31;
		int arg_735_0_cp_1 = 3;
		LocaleManager.SubstitutionSetting substitutionSetting32 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting32.source = LocaleSubstitution.GetKey("CONTENTMANAGER_ACHIEVEMENTS_QQ");
		substitutionSetting32.target = LocaleSubstitution.GetKey("CONTENTMANAGER_ACHIEVEMENTS");
		expr_658[arg_735_0_cp_1] = substitutionSetting32;
		int arg_76D_0_cp_1 = 4;
		LocaleManager.SubstitutionSetting substitutionSetting33 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting33.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMOD_QQ");
		substitutionSetting33.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMOD");
		expr_658[arg_76D_0_cp_1] = substitutionSetting33;
		int arg_7A5_0_cp_1 = 5;
		LocaleManager.SubstitutionSetting substitutionSetting34 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting34.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSAVE_QQ");
		substitutionSetting34.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSAVE");
		expr_658[arg_7A5_0_cp_1] = substitutionSetting34;
		int arg_7DD_0_cp_1 = 6;
		LocaleManager.SubstitutionSetting substitutionSetting35 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting35.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMAP_QQ");
		substitutionSetting35.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHMAP");
		expr_658[arg_7DD_0_cp_1] = substitutionSetting35;
		int arg_815_0_cp_1 = 7;
		LocaleManager.SubstitutionSetting substitutionSetting36 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting36.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHASSET_QQ");
		substitutionSetting36.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHASSET");
		expr_658[arg_815_0_cp_1] = substitutionSetting36;
		int arg_84D_0_cp_1 = 8;
		LocaleManager.SubstitutionSetting substitutionSetting37 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting37.source = LocaleSubstitution.GetKey("CONTENTMANAGER_STEAMWORKSHOP_QQ");
		substitutionSetting37.target = LocaleSubstitution.GetKey("CONTENTMANAGER_STEAMWORKSHOP");
		expr_658[arg_84D_0_cp_1] = substitutionSetting37;
		int arg_886_0_cp_1 = 9;
		LocaleManager.SubstitutionSetting substitutionSetting38 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting38.source = LocaleSubstitution.GetKey("SAVEMAP_USECLOUD_QQ");
		substitutionSetting38.target = LocaleSubstitution.GetKey("SAVEMAP_USECLOUD");
		expr_658[arg_886_0_cp_1] = substitutionSetting38;
		int arg_8BF_0_cp_1 = 10;
		LocaleManager.SubstitutionSetting substitutionSetting39 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting39.source = LocaleSubstitution.GetKey("SAVEGAME_CLOUDUNSUPPORTED_QQ");
		substitutionSetting39.target = LocaleSubstitution.GetKey("SAVEGAME_CLOUDUNSUPPORTED");
		expr_658[arg_8BF_0_cp_1] = substitutionSetting39;
		int arg_8FD_0_cp_1 = 11;
		LocaleManager.SubstitutionSetting substitutionSetting40 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting40.source = LocaleSubstitution.GetKey("CONTENT_CONFIRM_WORKSHOPDELETE_QQ");
		substitutionSetting40.target = LocaleSubstitution.GetKey("CONTENT_CONFIRM_WORKSHOPDELETE", "Message");
		expr_658[arg_8FD_0_cp_1] = substitutionSetting40;
		int arg_936_0_cp_1 = 12;
		LocaleManager.SubstitutionSetting substitutionSetting41 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting41.source = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_UPDATE_QQ");
		substitutionSetting41.target = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_UPDATE");
		expr_658[arg_936_0_cp_1] = substitutionSetting41;
		int arg_96F_0_cp_1 = 13;
		LocaleManager.SubstitutionSetting substitutionSetting42 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting42.source = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED_QQ");
		substitutionSetting42.target = LocaleSubstitution.GetKey("CONTENT_MAPPUBLISHED");
		expr_658[arg_96F_0_cp_1] = substitutionSetting42;
		int arg_9A8_0_cp_1 = 14;
		LocaleManager.SubstitutionSetting substitutionSetting43 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting43.source = LocaleSubstitution.GetKey("CONTENT_VIEW_QQ");
		substitutionSetting43.target = LocaleSubstitution.GetKey("CONTENT_VIEW");
		expr_658[arg_9A8_0_cp_1] = substitutionSetting43;
		int arg_9E1_0_cp_1 = 15;
		LocaleManager.SubstitutionSetting substitutionSetting44 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting44.source = LocaleSubstitution.GetKey("WORKSHOP_TITLE_QQ");
		substitutionSetting44.target = LocaleSubstitution.GetKey("WORKSHOP_TITLE");
		expr_658[arg_9E1_0_cp_1] = substitutionSetting44;
		int arg_A1A_0_cp_1 = 16;
		LocaleManager.SubstitutionSetting substitutionSetting45 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting45.source = LocaleSubstitution.GetKey("WORKSHOP_DESC_QQ");
		substitutionSetting45.target = LocaleSubstitution.GetKey("WORKSHOP_DESC");
		expr_658[arg_A1A_0_cp_1] = substitutionSetting45;
		int arg_A53_0_cp_1 = 17;
		LocaleManager.SubstitutionSetting substitutionSetting46 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting46.source = LocaleSubstitution.GetKey("CONTENTMANAGER_USINGCLOUD_QQ");
		substitutionSetting46.target = LocaleSubstitution.GetKey("CONTENTMANAGER_USINGCLOUD");
		expr_658[arg_A53_0_cp_1] = substitutionSetting46;
		int arg_A8C_0_cp_1 = 18;
		LocaleManager.SubstitutionSetting substitutionSetting47 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting47.source = LocaleSubstitution.GetKey("CONTENT_AD_BUY_QQ");
		substitutionSetting47.target = LocaleSubstitution.GetKey("CONTENT_AD_BUY");
		expr_658[arg_A8C_0_cp_1] = substitutionSetting47;
		int arg_AC5_0_cp_1 = 19;
		LocaleManager.SubstitutionSetting substitutionSetting48 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting48.source = LocaleSubstitution.GetKey("LOADPANEL_ACHSTATUS_WORKSHOP_QQ");
		substitutionSetting48.target = LocaleSubstitution.GetKey("LOADPANEL_ACHSTATUS_WORKSHOP");
		expr_658[arg_AC5_0_cp_1] = substitutionSetting48;
		int arg_B03_0_cp_1 = 20;
		LocaleManager.SubstitutionSetting substitutionSetting49 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting49.source = LocaleSubstitution.GetKey("CONFIRM_UNSUBSCRIBEALL_QQ");
		substitutionSetting49.target = LocaleSubstitution.GetKey("CONFIRM_UNSUBSCRIBEALL", "Message");
		expr_658[arg_B03_0_cp_1] = substitutionSetting49;
		int arg_B3C_0_cp_1 = 21;
		LocaleManager.SubstitutionSetting substitutionSetting50 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting50.source = LocaleSubstitution.GetKey("CONTENTMANAGER_SUBS_QQ");
		substitutionSetting50.target = LocaleSubstitution.GetKey("CONTENTMANAGER_SUBS");
		expr_658[arg_B3C_0_cp_1] = substitutionSetting50;
		int arg_B75_0_cp_1 = 22;
		LocaleManager.SubstitutionSetting substitutionSetting51 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting51.source = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSTYLE_QQ");
		substitutionSetting51.target = LocaleSubstitution.GetKey("WORKSHOP_PUBLISHSTYLE");
		expr_658[arg_B75_0_cp_1] = substitutionSetting51;
		int arg_BB3_0_cp_1 = 23;
		LocaleManager.SubstitutionSetting substitutionSetting52 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting52.source = LocaleSubstitution.GetKey("DISCLAIMER_MESSAGE_QQ");
		substitutionSetting52.target = LocaleSubstitution.GetKey("DISCLAIMER_MESSAGE", "agreedModsDisclaimer");
		expr_658[arg_BB3_0_cp_1] = substitutionSetting52;
		int arg_BEC_0_cp_1 = 24;
		LocaleManager.SubstitutionSetting substitutionSetting53 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting53.source = LocaleSubstitution.GetKey("CONTENT_SF_BUY_QQ");
		substitutionSetting53.target = LocaleSubstitution.GetKey("CONTENT_SF_BUY");
		expr_658[arg_BEC_0_cp_1] = substitutionSetting53;
		int arg_C2A_0_cp_1 = 25;
		LocaleManager.SubstitutionSetting substitutionSetting54 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting54.source = LocaleSubstitution.GetKey("NEWMAP_UNPUBLISHED_THEME_QQ");
		substitutionSetting54.target = LocaleSubstitution.GetKey("NEWMAP_UNPUBLISHED_THEME", "Message");
		expr_658[arg_C2A_0_cp_1] = substitutionSetting54;
		int arg_C63_0_cp_1 = 26;
		LocaleManager.SubstitutionSetting substitutionSetting55 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting55.source = LocaleSubstitution.GetKey("CONTENT_ND_BUY_QQ");
		substitutionSetting55.target = LocaleSubstitution.GetKey("CONTENT_ND_BUY");
		expr_658[arg_C63_0_cp_1] = substitutionSetting55;
		int arg_C9C_0_cp_1 = 27;
		LocaleManager.SubstitutionSetting substitutionSetting56 = default(LocaleManager.SubstitutionSetting);
		substitutionSetting56.source = LocaleSubstitution.GetKey("NEWSFEED_ERROR_QQ");
		substitutionSetting56.target = LocaleSubstitution.GetKey("NEWSFEED_ERROR");
		expr_658[arg_C9C_0_cp_1] = substitutionSetting56;
		LocaleSubstitution.qqSubstitution = expr_658;
	}

	private static LocaleManager.SubstitutionSetting[] tgpSubstitution;

	private static LocaleManager.SubstitutionSetting[] qqSubstitution;
}
