﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class TriggerPanel : ToolsModifierControl
{
	public static TriggerPanel instance
	{
		get
		{
			if (TriggerPanel.m_instance == null)
			{
				TriggerPanel.m_instance = UIView.Find<UIPanel>("TriggerPanel").GetComponent<TriggerPanel>();
			}
			return TriggerPanel.m_instance;
		}
	}

	private void Awake()
	{
		this.m_triggerListBox = base.Find<UIListBox>("TriggerListBox");
		this.m_triggerListBox.add_eventItemClicked(new PropertyChangedEventHandler<int>(this.OnTriggerClicked));
		this.m_selectedTriggerScript = base.GetComponentInChildren<SelectedTrigger>();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.RefreshTriggerList();
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers.Length > 0)
		{
			if (this.m_triggerListBox.get_selectedIndex() != -1)
			{
				this.SelectTrigger(this.m_triggerListBox.get_selectedIndex());
			}
			else
			{
				this.SelectTrigger(0);
			}
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		if (updateMode == SimulationManager.UpdateMode.NewScenarioFromGame)
		{
			this.DeleteAllTriggersAndDisasters();
		}
		this.RefreshTriggerList();
		this.SelectTrigger(0);
	}

	private int triggerCount
	{
		get
		{
			if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
			{
				return Singleton<UnlockManager>.get_instance().m_scenarioTriggers.Length;
			}
			return 0;
		}
	}

	private void DeleteAllTriggersAndDisasters()
	{
		for (int i = this.triggerCount - 1; i >= 0; i--)
		{
			Singleton<UnlockManager>.get_instance().RemoveScenarioTrigger(i);
		}
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			int num = instance.m_disasters.m_size;
			if (num < 0)
			{
				num = 0;
			}
			for (ushort num2 = (ushort)num; num2 > 0; num2 -= 1)
			{
				if ((instance.m_disasters.m_buffer[(int)num2].m_flags & DisasterData.Flags.Created) != DisasterData.Flags.None)
				{
					instance.ReleaseDisaster(num2);
				}
			}
		});
	}

	public void RefreshTriggerList()
	{
		List<string> list = new List<string>();
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
		{
			TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
			for (int i = 0; i < scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = scenarioTriggers[i];
				list.Add(triggerMilestone.GetLocalizedName());
			}
		}
		if (list.Count == 0)
		{
			this.AddTrigger();
			return;
		}
		list.Add(Locale.Get("TRIGGERPANEL_ADDTRIGGER"));
		this.m_triggerListBox.set_items(list.ToArray());
	}

	private void OnTriggerClicked(UIComponent comp, int index)
	{
		if (index == this.triggerCount)
		{
			this.AddTrigger();
		}
		else
		{
			this.SelectTrigger(index);
		}
	}

	private void AddTrigger()
	{
		Singleton<UnlockManager>.get_instance().AddScenarioTrigger();
		this.RefreshTriggerList();
		this.SelectTrigger(this.triggerCount - 1);
	}

	public void SelectTrigger(int index)
	{
		this.m_triggerListBox.set_selectedIndex(index);
		this.m_selectedTriggerScript.SelectTrigger(Singleton<UnlockManager>.get_instance().m_scenarioTriggers[index]);
	}

	public void DeleteSelectedTrigger()
	{
		int num = this.m_triggerListBox.get_selectedIndex();
		Singleton<UnlockManager>.get_instance().RemoveScenarioTrigger(num);
		if (this.triggerCount == 0)
		{
			this.AddTrigger();
		}
		else
		{
			num--;
			if (num < 0)
			{
				num = 0;
			}
			this.RefreshTriggerList();
			this.SelectTrigger(num);
		}
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public void OnCloseButton()
	{
		base.CloseToolbar();
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private SelectedTrigger m_selectedTriggerScript;

	private UIListBox m_triggerListBox;

	private static TriggerPanel m_instance;
}
