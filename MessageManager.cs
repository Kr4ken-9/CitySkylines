﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ICities;
using UnityEngine;

public class MessageManager : SimulationManagerBase<MessageManager, MessageController>, ISimulationManager
{
	public event Action m_messagesUpdated
	{
		add
		{
			Action action = this.m_messagesUpdated;
			Action action2;
			do
			{
				action2 = action;
				action = Interlocked.CompareExchange<Action>(ref this.m_messagesUpdated, (Action)Delegate.Combine(action2, value), action);
			}
			while (action != action2);
		}
		remove
		{
			Action action = this.m_messagesUpdated;
			Action action2;
			do
			{
				action2 = action;
				action = Interlocked.CompareExchange<Action>(ref this.m_messagesUpdated, (Action)Delegate.Remove(action2, value), action);
			}
			while (action != action2);
		}
	}

	public event MessageManager.NewMessageHandler m_newMessages
	{
		add
		{
			MessageManager.NewMessageHandler newMessageHandler = this.m_newMessages;
			MessageManager.NewMessageHandler newMessageHandler2;
			do
			{
				newMessageHandler2 = newMessageHandler;
				newMessageHandler = Interlocked.CompareExchange<MessageManager.NewMessageHandler>(ref this.m_newMessages, (MessageManager.NewMessageHandler)Delegate.Combine(newMessageHandler2, value), newMessageHandler);
			}
			while (newMessageHandler != newMessageHandler2);
		}
		remove
		{
			MessageManager.NewMessageHandler newMessageHandler = this.m_newMessages;
			MessageManager.NewMessageHandler newMessageHandler2;
			do
			{
				newMessageHandler2 = newMessageHandler;
				newMessageHandler = Interlocked.CompareExchange<MessageManager.NewMessageHandler>(ref this.m_newMessages, (MessageManager.NewMessageHandler)Delegate.Remove(newMessageHandler2, value), newMessageHandler);
			}
			while (newMessageHandler != newMessageHandler2);
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_messageData = new Dictionary<string, MessageManager.MessageData>();
		this.m_messageQueue = new Queue<MessageBase>();
		this.m_recentMessages = new MessageBase[16];
		this.m_randomMessageTimers = new byte[26];
	}

	public override void InitializeProperties(MessageController properties)
	{
		base.InitializeProperties(properties);
	}

	public override void DestroyProperties(MessageController properties)
	{
		base.DestroyProperties(properties);
	}

	private void MessagesUpdated()
	{
		if (this.m_messagesUpdated != null)
		{
			this.m_messagesUpdated();
		}
	}

	private void Update()
	{
		if (this.m_messageTimeout > 0f)
		{
			this.m_messageTimeout -= Time.get_deltaTime();
		}
		else
		{
			this.m_canShowMessage = true;
		}
	}

	private void ShowMessagePanel(MessageBase message)
	{
		if (this.m_newMessages != null)
		{
			this.m_newMessages(message);
		}
		this.m_messageTimeout = this.m_properties.m_messageNextTimeout;
		this.m_canShowMessage = false;
		this.m_messageOpened = true;
	}

	public void TryCreateMessage(MessageInfo info, uint senderID)
	{
		if (info == null || this.m_properties == null)
		{
			return;
		}
		this.TryCreateMessage(info.m_firstID1, info.m_repeatID1, null, senderID, null);
		this.TryCreateMessage(info.m_firstID2, info.m_repeatID2, null, senderID, null);
	}

	public void TryCreateMessage(MessageInfo info, uint senderID, string tag)
	{
		if (info == null || this.m_properties == null)
		{
			return;
		}
		this.TryCreateMessage(info.m_firstID1, info.m_repeatID1, null, senderID, tag);
		this.TryCreateMessage(info.m_firstID2, info.m_repeatID2, null, senderID, tag);
	}

	public void TryCreateMessage(string id, string key, uint senderID)
	{
		if (this.m_properties == null)
		{
			return;
		}
		this.TryCreateMessage(null, id, key, senderID, null);
	}

	public void TryCreateMessage(string id, string key, uint senderID, string tag)
	{
		if (this.m_properties == null)
		{
			return;
		}
		this.TryCreateMessage(null, id, key, senderID, tag);
	}

	public void TryCreateDistrictMessage(string id, string key, byte districtID)
	{
		if (this.m_properties == null)
		{
			return;
		}
		this.TryCreateDistrictMessage(null, id, key, districtID);
	}

	private void TryCreateMessage(string firstID, string repeatID, string key, uint senderID, string tag)
	{
		string text = firstID;
		MessageManager.MessageData value;
		if (string.IsNullOrEmpty(text))
		{
			text = repeatID;
		}
		else if (this.m_messageData.TryGetValue(text, out value) && value.m_lastCreateFrame != 0u)
		{
			text = repeatID;
		}
		if (!string.IsNullOrEmpty(text))
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (this.m_messageData.TryGetValue(text, out value) && value.m_lastCreateFrame != 0u && value.m_lastCreateFrame >= currentFrameIndex - 4096u)
			{
				return;
			}
			value.m_lastCreateFrame = currentFrameIndex;
			this.m_messageData[text] = value;
			this.CreateMessage(text, key, senderID, tag);
		}
	}

	private void TryCreateDistrictMessage(string firstID, string repeatID, string key, byte districtID)
	{
		string text = firstID;
		MessageManager.MessageData value;
		if (string.IsNullOrEmpty(text))
		{
			text = repeatID;
		}
		else if (this.m_messageData.TryGetValue(text, out value) && value.m_lastCreateFrame != 0u)
		{
			text = repeatID;
		}
		if (!string.IsNullOrEmpty(text))
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (this.m_messageData.TryGetValue(text, out value) && value.m_lastCreateFrame != 0u && value.m_lastCreateFrame >= currentFrameIndex - 4096u)
			{
				return;
			}
			value.m_lastCreateFrame = currentFrameIndex;
			this.m_messageData[text] = value;
			uint randomCitizenID = this.GetRandomCitizenID(districtID);
			this.CreateMessage(text, key, randomCitizenID, null);
		}
	}

	private void CreateMessage(string id, string key, uint senderID, string tag)
	{
		this.QueueMessage(new CitizenMessage(senderID, id, key, tag));
	}

	private uint GetRandomCitizenID(Notification.Problem problem)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		int num = instance.m_randomizer.Int32(1, 49151);
		for (int i = 0; i < 49152; i++)
		{
			if (++num == 49152)
			{
				num = 1;
			}
			if (instance2.m_buildings.m_buffer[num].m_flags != Building.Flags.None && (instance2.m_buildings.m_buffer[num].m_problems & problem) != Notification.Problem.None)
			{
				uint citizenUnits = instance2.m_buildings.m_buffer[num].m_citizenUnits;
				uint randomCitizenID = this.GetRandomCitizenID(citizenUnits, CitizenUnit.Flags.Home | CitizenUnit.Flags.Work);
				if (randomCitizenID != 0u)
				{
					return randomCitizenID;
				}
			}
		}
		return this.GetRandomResidentID();
	}

	private uint GetRandomCitizenID(byte district)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		DistrictManager instance3 = Singleton<DistrictManager>.get_instance();
		int num = instance.m_randomizer.Int32(1, 49151);
		for (int i = 0; i < 49152; i++)
		{
			if (++num == 49152)
			{
				num = 1;
			}
			if (instance2.m_buildings.m_buffer[num].m_flags != Building.Flags.None)
			{
				Vector3 position = instance2.m_buildings.m_buffer[num].m_position;
				if (district == instance3.GetDistrict(position))
				{
					uint citizenUnits = instance2.m_buildings.m_buffer[num].m_citizenUnits;
					uint randomCitizenID = this.GetRandomCitizenID(citizenUnits, CitizenUnit.Flags.Home | CitizenUnit.Flags.Work);
					if (randomCitizenID != 0u)
					{
						return randomCitizenID;
					}
				}
			}
		}
		return this.GetRandomResidentID();
	}

	public uint GetRandomResidentID(ushort buildingID, CitizenUnit.Flags unitFlag)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (buildingID != 0 && instance.m_buildings.m_buffer[(int)buildingID].m_flags != Building.Flags.None)
		{
			BuildingInfo info = instance.m_buildings.m_buffer[(int)buildingID].Info;
			if (info != null)
			{
				uint citizenUnits = instance.m_buildings.m_buffer[(int)buildingID].m_citizenUnits;
				uint randomCitizenID = this.GetRandomCitizenID(citizenUnits, unitFlag);
				if (randomCitizenID != 0u)
				{
					return randomCitizenID;
				}
			}
		}
		return this.GetRandomResidentID();
	}

	public uint GetRandomResidentID()
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		int num = instance.m_randomizer.Int32(1, 49151);
		for (int i = 0; i < 49152; i++)
		{
			if (++num == 49152)
			{
				num = 1;
			}
			if (instance2.m_buildings.m_buffer[num].m_flags != Building.Flags.None)
			{
				BuildingInfo info = instance2.m_buildings.m_buffer[num].Info;
				if (info != null && info.m_class.m_service == ItemClass.Service.Residential)
				{
					uint citizenUnits = instance2.m_buildings.m_buffer[num].m_citizenUnits;
					uint randomCitizenID = this.GetRandomCitizenID(citizenUnits, CitizenUnit.Flags.Home);
					if (randomCitizenID != 0u)
					{
						return randomCitizenID;
					}
				}
			}
		}
		return instance.m_randomizer.UInt32(1u, 1048575u);
	}

	private uint GetRandomCitizenID(uint units, CitizenUnit.Flags flag)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = units;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					if (instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i) != 0u)
					{
						num2++;
					}
				}
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (num2 != 0)
		{
			num2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num2);
			num = units;
			num3 = 0;
			while (num != 0u)
			{
				if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0)
				{
					for (int j = 0; j < 5; j++)
					{
						uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(j);
						if (citizen != 0u && num2-- == 0)
						{
							return citizen;
						}
					}
				}
				num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				if (++num3 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return 0u;
	}

	public void QueueMessage(MessageBase message)
	{
		MessageController properties = this.m_properties;
		if (properties != null)
		{
			this.m_messageQueue.Enqueue(message);
		}
	}

	public void DeleteMessage(IChirperMessage message)
	{
		int num = -1;
		for (int i = 0; i < this.m_recentMessages.Length; i++)
		{
			int num2 = (this.m_recentMessageIndex + this.m_recentMessages.Length - i - 1) % this.m_recentMessages.Length;
			if (num != -1)
			{
				this.m_recentMessages[num] = this.m_recentMessages[num2];
				num = num2;
			}
			else if (this.m_recentMessages[num2] == message)
			{
				num = num2;
			}
		}
		if (num != -1)
		{
			this.m_recentMessages[num] = null;
		}
	}

	public MessageBase[] GetRecentMessages()
	{
		int num = 0;
		for (int i = 0; i < this.m_recentMessages.Length; i++)
		{
			if (this.m_recentMessages[i] != null)
			{
				num++;
			}
		}
		MessageBase[] array = new MessageBase[num];
		for (int j = 0; j < this.m_recentMessages.Length; j++)
		{
			int num2 = (this.m_recentMessageIndex + this.m_recentMessages.Length - j - 1) % this.m_recentMessages.Length;
			MessageBase messageBase = this.m_recentMessages[num2];
			if (messageBase != null)
			{
				array[--num] = messageBase;
			}
		}
		return array;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_currentMessage != null && this.m_messageOpened && this.m_canShowMessage)
		{
			this.m_currentMessage = null;
		}
		MessageController properties = this.m_properties;
		if (subStep != 0 && subStep != 1000 && properties != null)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			int num = (int)(currentFrameIndex & 4095u);
			int num2 = num * 26 >> 12;
			int num3 = ((num + 1) * 26 >> 12) - 1;
			for (int i = num2; i <= num3; i++)
			{
				string id;
				string key;
				int num4;
				Notification.Problem problem;
				if (this.CheckRandomMessage(i, out id, out key, out num4, out problem))
				{
					if (num4 == 0 || (int)this.m_randomMessageTimers[i] == num4 - 1)
					{
						if (currentFrameIndex - this.m_lastRandomMessageFrame >= 8192u)
						{
							int num5 = this.m_recentMessages.Length;
							MessageBase message5 = this.m_recentMessages[(this.m_recentMessageIndex + num5 - 1) % num5];
							MessageBase message2 = this.m_recentMessages[(this.m_recentMessageIndex + num5 - 2) % num5];
							if (!this.IsSimilarMessage(message5, id, key) && !this.IsSimilarMessage(message2, id, key))
							{
								if (problem != Notification.Problem.None)
								{
									this.CreateMessage(id, key, this.GetRandomCitizenID(problem), null);
								}
								else
								{
									this.CreateMessage(id, key, this.GetRandomResidentID(), null);
								}
								this.m_lastRandomMessageFrame = currentFrameIndex;
								this.m_randomMessageTimers[i] = 0;
							}
						}
					}
					else
					{
						byte[] expr_169_cp_0 = this.m_randomMessageTimers;
						int expr_169_cp_1 = i;
						expr_169_cp_0[expr_169_cp_1] += 1;
					}
				}
				else
				{
					this.m_randomMessageTimers[i] = 0;
				}
			}
		}
		if (subStep == 1 && properties != null && this.m_currentMessage == null && this.m_canShowMessage && this.m_messageQueue.Count != 0)
		{
			int num6 = this.m_recentMessages.Length;
			MessageBase message = this.m_messageQueue.Dequeue();
			MessageBase message3 = this.m_recentMessages[(this.m_recentMessageIndex + num6 - 1) % num6];
			MessageBase message4 = this.m_recentMessages[(this.m_recentMessageIndex + num6 - 2) % num6];
			if (!this.IsSimilarMessage(message, message3) && !this.IsSimilarMessage(message, message4))
			{
				this.m_currentMessage = message;
				this.m_recentMessages[this.m_recentMessageIndex] = message;
				this.m_recentMessageIndex = (this.m_recentMessageIndex + 1) % num6;
				this.m_messageOpened = false;
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.ShowMessagePanel(message);
				});
			}
		}
	}

	private bool IsSimilarMessage(MessageBase message1, MessageBase message2)
	{
		if (message1 == null || message2 == null)
		{
			return message1 == message2;
		}
		return message1.IsSimilarMessage(message2);
	}

	private bool IsSimilarMessage(MessageBase message, string id, string key)
	{
		if (message == null || id == null)
		{
			return message == null && id == null;
		}
		CitizenMessage citizenMessage = message as CitizenMessage;
		return citizenMessage != null && citizenMessage.m_messageID == id && citizenMessage.m_keyID == key;
	}

	private bool CheckRandomMessage(int index, out string id, out string key, out int delay, out Notification.Problem problem)
	{
		if (Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount == 0u)
		{
			id = null;
			key = null;
			delay = 0;
			problem = Notification.Problem.None;
			return false;
		}
		switch (index)
		{
		case 0:
			id = "CHIRP_LOW_CRIME";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.PoliceDepartment))
			{
				int finalCrimeRate = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalCrimeRate;
				return finalCrimeRate <= 10;
			}
			return false;
		case 1:
			id = "CHIRP_HIGH_CRIME";
			key = null;
			delay = 4;
			problem = Notification.Problem.Crime;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.PoliceDepartment))
			{
				int finalCrimeRate2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalCrimeRate;
				return finalCrimeRate2 >= 90;
			}
			return false;
		case 2:
			id = "CHIRP_LOW_HEALTH";
			key = null;
			delay = 4;
			problem = Notification.Problem.Noise;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare))
			{
				int finalHealth = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHealth;
				return finalHealth <= 25;
			}
			return false;
		case 3:
			id = "CHIRP_TRASH_PILING_UP";
			key = null;
			delay = 2;
			problem = Notification.Problem.Garbage;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Garbage) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Garbage) != Notification.Problem.None;
		case 4:
			id = "CHIRP_NO_WATER";
			key = null;
			delay = 2;
			problem = Notification.Problem.Water;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Water) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Water) != Notification.Problem.None;
		case 5:
			id = "CHIRP_SEWAGE";
			key = null;
			delay = 2;
			problem = Notification.Problem.Sewage;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Water) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Sewage) != Notification.Problem.None;
		case 6:
			id = "CHIRP_NO_ELECTRICITY";
			key = null;
			delay = 2;
			problem = Notification.Problem.Electricity;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Electricity) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Electricity) != Notification.Problem.None;
		case 7:
			id = "CHIRP_LOW_HAPPINESS";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_finalHappiness <= 30;
		case 8:
			id = "CHIRP_HAPPY_PEOPLE";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_finalHappiness >= 80;
		case 9:
		{
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.Attractiveness, out num);
			id = "CHIRP_ATTRACTIVE_CITY";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return num >= 30;
		}
		case 10:
			id = "CHIRP_INDUSTRIAL_DEMAND";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return Singleton<ZoneManager>.get_instance().m_workplaceDemand >= 75;
		case 11:
			id = "CHIRP_COMMERCIAL_DEMAND";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return Singleton<ZoneManager>.get_instance().m_commercialDemand >= 75;
		case 12:
			id = "CHIRP_RESIDENTIAL_DEMAND";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			return Singleton<ZoneManager>.get_instance().m_residentialDemand >= 75;
		case 13:
			id = "CHIRP_FIRE_HAZARD";
			key = null;
			delay = 4;
			problem = Notification.Problem.None;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.FireDepartment))
			{
				int num2 = 0;
				Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.FireHazard, out num2);
				return num2 >= 50;
			}
			return false;
		case 14:
			id = "CHIRP_DEAD_PILING_UP";
			key = null;
			delay = 2;
			problem = Notification.Problem.Death;
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.DeathCare) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Death) != Notification.Problem.None;
		case 15:
		{
			id = "CHIRP_HIGH_TECH_LEVEL";
			key = null;
			delay = 10;
			problem = Notification.Problem.None;
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			int finalCount = (int)instance.m_districts.m_buffer[0].m_educated3Data.m_finalCount;
			int finalCount2 = (int)instance.m_districts.m_buffer[0].m_populationData.m_finalCount;
			return finalCount > finalCount2 >> 1;
		}
		case 16:
		{
			id = "CHIRP_ABANDONED_BUILDINGS";
			key = null;
			delay = 2;
			problem = Notification.Problem.None;
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			int finalAbandonedCount = (int)instance2.m_districts.m_buffer[0].m_residentialData.m_finalAbandonedCount;
			int finalAbandonedCount2 = (int)instance2.m_districts.m_buffer[0].m_commercialData.m_finalAbandonedCount;
			int finalAbandonedCount3 = (int)instance2.m_districts.m_buffer[0].m_industrialData.m_finalAbandonedCount;
			int finalAbandonedCount4 = (int)instance2.m_districts.m_buffer[0].m_officeData.m_finalAbandonedCount;
			return finalAbandonedCount + finalAbandonedCount2 + finalAbandonedCount3 + finalAbandonedCount4 >= 50;
		}
		case 17:
			id = "CHIRP_NEED_MORE_PARKS";
			key = null;
			delay = 20;
			problem = Notification.Problem.None;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Beautification))
			{
				FastList<ushort> serviceBuildings = Singleton<BuildingManager>.get_instance().GetServiceBuildings(ItemClass.Service.Beautification);
				return serviceBuildings == null || serviceBuildings.m_size == 0;
			}
			return false;
		case 18:
			id = "CHIRP_POLLUTION";
			key = null;
			delay = 2;
			problem = Notification.Problem.Pollution;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Pollution) != Notification.Problem.None;
		case 19:
			id = "CHIRP_NOISEPOLLUTION";
			key = null;
			delay = 2;
			problem = Notification.Problem.Noise;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.Noise) != Notification.Problem.None;
		case 20:
			id = "CHIRP_POISONED";
			key = null;
			delay = 2;
			problem = Notification.Problem.DirtyWater;
			return Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare) && (Singleton<BuildingManager>.get_instance().m_lastBuildingProblems & Notification.Problem.DirtyWater) != Notification.Problem.None;
		case 21:
			id = "CHIRP_NO_HEALTHCARE";
			key = null;
			delay = 10;
			problem = Notification.Problem.None;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare))
			{
				FastList<ushort> serviceBuildings2 = Singleton<BuildingManager>.get_instance().GetServiceBuildings(ItemClass.Service.HealthCare);
				return serviceBuildings2 == null || serviceBuildings2.m_size == 0;
			}
			return false;
		case 22:
			id = "CHIRP_NO_SCHOOLS";
			key = null;
			delay = 10;
			problem = Notification.Problem.None;
			if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Education))
			{
				FastList<ushort> serviceBuildings3 = Singleton<BuildingManager>.get_instance().GetServiceBuildings(ItemClass.Service.Education);
				return serviceBuildings3 == null || serviceBuildings3.m_size == 0;
			}
			return false;
		case 23:
			id = "CHIRP_NO_PRISONS";
			key = null;
			delay = 3;
			problem = Notification.Problem.None;
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.DoubleSentences) && Singleton<UnlockManager>.get_instance().Unlocked(DistrictPolicies.Policies.DoubleSentences))
			{
				int criminalCapacity = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetCriminalCapacity();
				int criminalAmount = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetCriminalAmount();
				int extraCriminals = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetExtraCriminals();
				return criminalAmount + extraCriminals > criminalCapacity;
			}
			return false;
		case 24:
			id = "FOOTBALLCHIRP_GENERIC";
			key = null;
			delay = 9;
			problem = Notification.Problem.None;
			return (Singleton<EventManager>.get_instance().GetEventTypeMask() & EventManager.EventType.Football) != (EventManager.EventType)0;
		case 25:
		{
			uint num3 = Locale.CountUnchecked("CHIRP_RANDOM");
			string environment = Singleton<SimulationManager>.get_instance().m_metaData.m_environment;
			uint num4 = Locale.CountUnchecked("CHIRP_RANDOM_THEME", environment);
			uint num5 = 0u;
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.FastRecovery))
			{
				num5 = Locale.CountUnchecked("CHIRP_RANDOM_DISASTERS");
			}
			uint num6 = 0u;
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.HighTicketPrices))
			{
				num6 = Locale.CountUnchecked("CHIRP_RANDOM_INMOTION");
			}
			uint num7 = 0u;
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.FilterIndustrialWaste))
			{
				num7 = Locale.CountUnchecked("CHIRP_RANDOM_EXP5");
			}
			uint num8 = Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(num3 + num4 + num5 + num6 + num7);
			delay = 0;
			problem = Notification.Problem.None;
			if (num8 < num3)
			{
				id = "CHIRP_RANDOM";
				key = null;
			}
			else if (num8 < num3 + num4)
			{
				id = "CHIRP_RANDOM_THEME";
				key = environment;
			}
			else if (num8 < num3 + num4 + num5)
			{
				id = "CHIRP_RANDOM_DISASTERS";
				key = null;
			}
			else if (num8 < num3 + num4 + num5 + num6)
			{
				id = "CHIRP_RANDOM_INMOTION";
				key = null;
			}
			else
			{
				id = "CHIRP_RANDOM_EXP5";
				key = null;
			}
			return Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0;
		}
		default:
			id = null;
			key = null;
			delay = 0;
			problem = Notification.Problem.None;
			return false;
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new MessageManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("MessageManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.m_messageQueue = new Queue<MessageBase>();
			this.m_recentMessages = new MessageBase[16];
			this.m_recentMessages[0] = new GenericMessage("CHIRPER_NAME", "CHIRP_DEFAULT", Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(2147483648u));
			this.m_recentMessageIndex = 1;
		}
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.MessagesUpdated();
		});
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const int RECENT_MESSAGE_COUNT = 16;

	public const int RANDOM_MESSAGE_COUNT = 26;

	private MessageBase m_currentMessage;

	private float m_messageTimeout;

	private volatile bool m_canShowMessage;

	private volatile bool m_messageOpened;

	private Queue<MessageBase> m_messageQueue;

	private MessageBase[] m_recentMessages;

	private int m_recentMessageIndex;

	private byte[] m_randomMessageTimers;

	private uint m_lastRandomMessageFrame;

	private Dictionary<string, MessageManager.MessageData> m_messageData;

	public struct MessageData
	{
		public uint m_lastCreateFrame;
	}

	public delegate void NewMessageHandler(IChirperMessage message);

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "MessageManager");
			MessageManager instance = Singleton<MessageManager>.get_instance();
			MessageBase[] array = instance.m_messageQueue.ToArray();
			s.WriteObjectArray<MessageBase>(array);
			s.WriteObjectArray<MessageBase>(instance.m_recentMessages);
			s.WriteInt16(instance.m_recentMessageIndex);
			s.WriteUInt24((uint)instance.m_messageData.Count);
			foreach (KeyValuePair<string, MessageManager.MessageData> current in instance.m_messageData)
			{
				s.WriteUniqueString(current.Key);
				s.WriteUInt32(current.Value.m_lastCreateFrame);
			}
			s.WriteUInt32(instance.m_lastRandomMessageFrame);
			s.WriteInt16(26);
			for (int i = 0; i < 26; i++)
			{
				s.WriteUInt8((uint)instance.m_randomMessageTimers[i]);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "MessageManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "MessageManager");
			MessageManager instance = Singleton<MessageManager>.get_instance();
			MessageBase[] collection = s.ReadObjectArray<MessageBase>();
			instance.m_messageQueue = new Queue<MessageBase>(collection);
			instance.m_recentMessages = s.ReadObjectArray<MessageBase>();
			if (s.get_version() >= 85u)
			{
				instance.m_recentMessageIndex = s.ReadInt16();
			}
			else
			{
				instance.m_recentMessageIndex = 0;
			}
			if (s.get_version() < 149u)
			{
				for (int i = 0; i < instance.m_recentMessages.Length; i++)
				{
					instance.m_recentMessages[i] = null;
				}
				instance.m_recentMessageIndex = 0;
			}
			instance.m_messageData.Clear();
			if (s.get_version() >= 89u)
			{
				uint num = s.ReadUInt24();
				for (uint num2 = 0u; num2 < num; num2 += 1u)
				{
					string key = s.ReadUniqueString();
					MessageManager.MessageData value;
					value.m_lastCreateFrame = s.ReadUInt32();
					if (s.get_version() >= 149u)
					{
						instance.m_messageData[key] = value;
					}
				}
			}
			if (s.get_version() >= 155u)
			{
				instance.m_lastRandomMessageFrame = s.ReadUInt32();
				int num3 = s.ReadInt16();
				for (int j = 0; j < num3; j++)
				{
					instance.m_randomMessageTimers[j] = (byte)s.ReadUInt8();
				}
				for (int k = num3; k < 26; k++)
				{
					instance.m_randomMessageTimers[k] = 0;
				}
			}
			else
			{
				instance.m_lastRandomMessageFrame = 0u;
				for (int l = 0; l < 26; l++)
				{
					instance.m_randomMessageTimers[l] = 0;
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "MessageManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "MessageManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "MessageManager");
		}
	}
}
