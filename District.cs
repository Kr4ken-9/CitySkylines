﻿using System;
using ColossalFramework;
using UnityEngine;

public struct District
{
	public bool IsPolicySet(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			return (this.m_specializationPolicies & (DistrictPolicies.Specialization)(1 << (int)policy)) != DistrictPolicies.Specialization.None;
		case DistrictPolicies.Policies.Farming:
			return (this.m_servicePolicies & (DistrictPolicies.Services)(1 << (int)policy)) != DistrictPolicies.Services.None;
		case DistrictPolicies.Policies.Oil:
			return (this.m_taxationPolicies & (DistrictPolicies.Taxation)(1 << (int)policy)) != DistrictPolicies.Taxation.None;
		case DistrictPolicies.Policies.Ore:
			return (this.m_cityPlanningPolicies & (DistrictPolicies.CityPlanning)(1 << (int)policy)) != DistrictPolicies.CityPlanning.None;
		case DistrictPolicies.Policies.Leisure:
			return (this.m_specialPolicies & (DistrictPolicies.Special)(1 << (int)policy)) != DistrictPolicies.Special.None;
		case DistrictPolicies.Policies.Tourist:
			return (this.m_eventPolicies & (DistrictPolicies.Event)(1 << (int)policy)) != DistrictPolicies.Event.None;
		default:
			return false;
		}
	}

	public void SetPolicy(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
		{
			DistrictPolicies.Specialization specialization = (DistrictPolicies.Specialization)(1 << (int)policy);
			if ((specialization & (DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore)) != DistrictPolicies.Specialization.None)
			{
				this.m_specializationPolicies &= ~(DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore);
			}
			if ((specialization & (DistrictPolicies.Specialization.Leisure | DistrictPolicies.Specialization.Tourist | DistrictPolicies.Specialization.Organic)) != DistrictPolicies.Specialization.None)
			{
				this.m_specializationPolicies &= ~(DistrictPolicies.Specialization.Leisure | DistrictPolicies.Specialization.Tourist | DistrictPolicies.Specialization.Organic);
			}
			if ((specialization & DistrictPolicies.Specialization.Selfsufficient) != DistrictPolicies.Specialization.None)
			{
				this.m_specializationPolicies &= ~DistrictPolicies.Specialization.Selfsufficient;
			}
			if ((specialization & DistrictPolicies.Specialization.Hightech) != DistrictPolicies.Specialization.None)
			{
				this.m_specializationPolicies &= ~DistrictPolicies.Specialization.Hightech;
			}
			this.m_specializationPolicies |= specialization;
			return;
		}
		case DistrictPolicies.Policies.Farming:
			this.m_servicePolicies |= (DistrictPolicies.Services)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Oil:
			this.m_taxationPolicies |= (DistrictPolicies.Taxation)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Ore:
			this.m_cityPlanningPolicies |= (DistrictPolicies.CityPlanning)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Leisure:
			this.m_specialPolicies |= (DistrictPolicies.Special)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Tourist:
			this.m_eventPolicies |= (DistrictPolicies.Event)(1 << (int)policy);
			return;
		default:
			return;
		}
	}

	public void UnsetPolicy(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			this.m_specializationPolicies &= (DistrictPolicies.Specialization)(~(DistrictPolicies.Specialization)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Farming:
			this.m_servicePolicies &= (DistrictPolicies.Services)(~(DistrictPolicies.Services)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Oil:
			this.m_taxationPolicies &= (DistrictPolicies.Taxation)(~(DistrictPolicies.Taxation)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Ore:
			this.m_cityPlanningPolicies &= (DistrictPolicies.CityPlanning)(~(DistrictPolicies.CityPlanning)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Leisure:
			this.m_specialPolicies &= (DistrictPolicies.Special)(~(DistrictPolicies.Special)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Tourist:
			this.m_eventPolicies &= (DistrictPolicies.Event)(~(DistrictPolicies.Event)(1 << (int)policy));
			return;
		default:
			return;
		}
	}

	public void SimulationStep(byte districtID)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		bool flag = (instance.m_currentFrameIndex & 4095u) >= 3840u;
		if (this.m_populationData.m_tempCount < 10000000u)
		{
			this.m_populationData.Update();
			this.m_productionData.Update();
			this.m_groundData.Update();
			this.m_childData.Update();
			this.m_teenData.Update();
			this.m_youngData.Update();
			this.m_adultData.Update();
			this.m_seniorData.Update();
			this.m_educated0Data.Update();
			this.m_educated1Data.Update();
			this.m_educated2Data.Update();
			this.m_educated3Data.Update();
			this.m_education1Data.Update();
			this.m_education2Data.Update();
			this.m_education3Data.Update();
			this.m_student1Data.Update();
			this.m_student2Data.Update();
			this.m_student3Data.Update();
			this.m_residentialData.Update();
			this.m_commercialData.Update();
			this.m_visitorData.Update();
			this.m_industrialData.Update();
			this.m_officeData.Update();
			this.m_playerData.Update();
			this.m_farmingData.Update();
			this.m_forestryData.Update();
			this.m_oreData.Update();
			this.m_oilData.Update();
			this.m_leisureData.Update();
			this.m_touristData.Update();
			this.m_organicData.Update();
			this.m_selfsufficientData.Update();
			this.m_hightechData.Update();
			this.m_playerConsumption.Update();
			this.m_residentialConsumption.Update();
			this.m_commercialConsumption.Update();
			this.m_industrialConsumption.Update();
			this.m_officeConsumption.Update();
			this.m_usageData.Update();
			this.m_hippieData.Update();
			this.m_hipsterData.Update();
			this.m_redneckData.Update();
			this.m_gangstaData.Update();
			if (flag)
			{
				this.m_birthData.Update();
				this.m_deathData.Update();
				this.m_importData.Update();
				this.m_exportData.Update();
				this.m_tourist1Data.Update();
				this.m_tourist2Data.Update();
				this.m_tourist3Data.Update();
			}
			uint num = this.m_residentialData.m_tempHappiness + this.m_commercialData.m_tempHappiness + this.m_industrialData.m_tempHappiness + this.m_officeData.m_tempHappiness;
			uint num2 = this.m_residentialData.m_tempHomeOrWorkCount + this.m_commercialData.m_tempHomeOrWorkCount + this.m_industrialData.m_tempHomeOrWorkCount + this.m_officeData.m_tempHomeOrWorkCount;
			if (num2 != 0u)
			{
				this.m_finalHappiness = (byte)((num + (num2 >> 1)) / num2);
			}
			else
			{
				this.m_finalHappiness = 0;
			}
			uint num3 = this.m_playerData.m_tempCrimeRate + this.m_residentialData.m_tempCrimeRate + this.m_commercialData.m_tempCrimeRate + this.m_industrialData.m_tempCrimeRate + this.m_officeData.m_tempCrimeRate;
			num2 += this.m_playerData.m_tempHomeOrWorkCount;
			if (num2 != 0u)
			{
				this.m_finalCrimeRate = (byte)((num3 + (num2 >> 1)) / num2);
			}
			else
			{
				this.m_finalCrimeRate = 0;
			}
		}
		if (districtID != 0)
		{
			instance2.m_districts.m_buffer[0].AddDistrictData(ref this, flag);
		}
		this.m_populationData.Reset();
		this.m_productionData.Reset();
		this.m_groundData.Reset();
		this.m_childData.Reset();
		this.m_teenData.Reset();
		this.m_youngData.Reset();
		this.m_adultData.Reset();
		this.m_seniorData.Reset();
		this.m_educated0Data.Reset();
		this.m_educated1Data.Reset();
		this.m_educated2Data.Reset();
		this.m_educated3Data.Reset();
		this.m_education1Data.Reset();
		this.m_education2Data.Reset();
		this.m_education3Data.Reset();
		this.m_student1Data.Reset();
		this.m_student2Data.Reset();
		this.m_student3Data.Reset();
		this.m_residentialData.Reset();
		this.m_commercialData.Reset();
		this.m_visitorData.Reset();
		this.m_industrialData.Reset();
		this.m_officeData.Reset();
		this.m_playerData.Reset();
		this.m_farmingData.Reset();
		this.m_forestryData.Reset();
		this.m_oreData.Reset();
		this.m_oilData.Reset();
		this.m_leisureData.Reset();
		this.m_touristData.Reset();
		this.m_organicData.Reset();
		this.m_selfsufficientData.Reset();
		this.m_hightechData.Reset();
		this.m_playerConsumption.Reset();
		this.m_residentialConsumption.Reset();
		this.m_commercialConsumption.Reset();
		this.m_industrialConsumption.Reset();
		this.m_officeConsumption.Reset();
		this.m_usageData.Reset();
		this.m_hippieData.Reset();
		this.m_hipsterData.Reset();
		this.m_redneckData.Reset();
		this.m_gangstaData.Reset();
		if (flag)
		{
			this.m_birthData.Reset();
			this.m_deathData.Reset();
			this.m_importData.Reset();
			this.m_exportData.Reset();
			this.m_tourist1Data.Reset();
			this.m_tourist2Data.Reset();
			this.m_tourist3Data.Reset();
		}
		DistrictPolicies.Specialization specialization = this.m_specializationPolicies & this.m_specializationPoliciesEffect;
		this.m_specializationPoliciesEffect = DistrictPolicies.Specialization.None;
		if (specialization != DistrictPolicies.Specialization.None && (this.m_industrialData.m_finalBuildingCount != 0 || this.m_commercialData.m_finalBuildingCount != 0))
		{
			for (int i = 0; i < 9; i++)
			{
				DistrictPolicies.Specialization specialization2 = (DistrictPolicies.Specialization)(1 << i);
				if ((specialization & specialization2) != DistrictPolicies.Specialization.None)
				{
					bool flag2 = false;
					if ((specialization2 & (DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore)) != DistrictPolicies.Specialization.None && this.m_industrialData.m_finalBuildingCount != 0)
					{
						flag2 = true;
					}
					if ((specialization2 & (DistrictPolicies.Specialization.Leisure | DistrictPolicies.Specialization.Tourist | DistrictPolicies.Specialization.Organic)) != DistrictPolicies.Specialization.None && this.m_commercialData.m_finalBuildingCount != 0)
					{
						flag2 = true;
					}
					if ((specialization2 & DistrictPolicies.Specialization.Selfsufficient) != DistrictPolicies.Specialization.None && this.m_residentialData.m_finalBuildingCount != 0)
					{
						flag2 = true;
					}
					if ((specialization2 & DistrictPolicies.Specialization.Hightech) != DistrictPolicies.Specialization.None && this.m_officeData.m_finalBuildingCount != 0)
					{
						flag2 = true;
					}
					if (flag2)
					{
						uint currentFrameIndex = instance.m_currentFrameIndex;
						if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex)
						{
							instance2.m_nextPolicyMessageFrame1 = currentFrameIndex + instance.m_randomizer.UInt32(5000u, 10000u);
							Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", specialization2.ToString(), districtID);
						}
					}
				}
			}
		}
		DistrictPolicies.Services services = this.m_servicePolicies & this.m_servicePoliciesEffect;
		this.m_servicePoliciesEffect = DistrictPolicies.Services.None;
		if (services != DistrictPolicies.Services.None && (this.m_residentialData.m_finalBuildingCount != 0 || this.m_commercialData.m_finalBuildingCount != 0 || this.m_industrialData.m_finalBuildingCount != 0 || this.m_officeData.m_finalBuildingCount != 0))
		{
			for (int j = 0; j < 20; j++)
			{
				DistrictPolicies.Services services2 = (DistrictPolicies.Services)(1 << j);
				if ((services & services2) != DistrictPolicies.Services.None)
				{
					uint currentFrameIndex2 = instance.m_currentFrameIndex;
					if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex2)
					{
						instance2.m_nextPolicyMessageFrame1 = currentFrameIndex2 + instance.m_randomizer.UInt32(5000u, 10000u);
						Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", services2.ToString(), districtID);
					}
				}
			}
		}
		DistrictPolicies.Taxation taxation = this.m_taxationPolicies & this.m_taxationPoliciesEffect;
		this.m_taxationPoliciesEffect = DistrictPolicies.Taxation.None;
		if (taxation != DistrictPolicies.Taxation.None && (this.m_residentialData.m_finalBuildingCount != 0 || this.m_commercialData.m_finalBuildingCount != 0 || this.m_officeConsumption.m_finalBuildingCount != 0))
		{
			for (int k = 0; k < 11; k++)
			{
				DistrictPolicies.Taxation taxation2 = (DistrictPolicies.Taxation)(1 << k);
				if ((taxation & taxation2) != DistrictPolicies.Taxation.None)
				{
					uint currentFrameIndex3 = instance.m_currentFrameIndex;
					if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex3)
					{
						instance2.m_nextPolicyMessageFrame1 = currentFrameIndex3 + instance.m_randomizer.UInt32(5000u, 10000u);
						Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", taxation2.ToString(), districtID);
					}
				}
			}
		}
		DistrictPolicies.CityPlanning cityPlanning = this.m_cityPlanningPolicies & this.m_cityPlanningPoliciesEffect;
		this.m_cityPlanningPoliciesEffect = DistrictPolicies.CityPlanning.None;
		if (cityPlanning != DistrictPolicies.CityPlanning.None && (this.m_residentialData.m_finalBuildingCount != 0 || this.m_commercialData.m_finalBuildingCount != 0 || this.m_industrialData.m_finalBuildingCount != 0 || this.m_officeData.m_finalBuildingCount != 0))
		{
			for (int l = 0; l < 19; l++)
			{
				DistrictPolicies.CityPlanning cityPlanning2 = (DistrictPolicies.CityPlanning)(1 << l);
				if ((cityPlanning & cityPlanning2) != DistrictPolicies.CityPlanning.None)
				{
					uint currentFrameIndex4 = instance.m_currentFrameIndex;
					if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex4)
					{
						instance2.m_nextPolicyMessageFrame1 = currentFrameIndex4 + instance.m_randomizer.UInt32(5000u, 10000u);
						Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", cityPlanning2.ToString(), districtID);
					}
				}
			}
		}
		DistrictPolicies.Special special = this.m_specialPolicies & this.m_specialPoliciesEffect;
		this.m_specialPoliciesEffect = DistrictPolicies.Special.None;
		if (special != DistrictPolicies.Special.None && (this.m_residentialData.m_finalBuildingCount != 0 || this.m_commercialData.m_finalBuildingCount != 0))
		{
			for (int m = 0; m < 1; m++)
			{
				DistrictPolicies.Special special2 = (DistrictPolicies.Special)(1 << m);
				if ((special & special2) != DistrictPolicies.Special.None)
				{
					uint currentFrameIndex5 = instance.m_currentFrameIndex;
					if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex5)
					{
						instance2.m_nextPolicyMessageFrame1 = currentFrameIndex5 + instance.m_randomizer.UInt32(5000u, 10000u);
						Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", special2.ToString(), districtID);
					}
				}
			}
		}
		DistrictPolicies.Event @event = this.m_eventPolicies & this.m_eventPoliciesEffect;
		this.m_eventPoliciesEffect = DistrictPolicies.Event.None;
		if (@event != DistrictPolicies.Event.None && this.m_residentialData.m_finalBuildingCount != 0)
		{
			for (int n = 0; n < 5; n++)
			{
				DistrictPolicies.Event event2 = (DistrictPolicies.Event)(1 << n);
				if ((@event & event2) != DistrictPolicies.Event.None)
				{
					uint currentFrameIndex6 = instance.m_currentFrameIndex;
					if (instance2.m_nextPolicyMessageFrame1 <= currentFrameIndex6)
					{
						instance2.m_nextPolicyMessageFrame1 = currentFrameIndex6 + instance.m_randomizer.UInt32(5000u, 10000u);
						Singleton<MessageManager>.get_instance().TryCreateDistrictMessage("CHIRP_POLICY", event2.ToString(), districtID);
					}
				}
			}
		}
		if (districtID == 0)
		{
			StatisticsManager instance3 = Singleton<StatisticsManager>.get_instance();
			StatisticBase statisticBase = instance3.Acquire<StatisticArray>(StatisticType.Unemployed);
			statisticBase.Acquire<StatisticInt32>(0, 4).Set((int)this.m_educated0Data.m_finalUnemployed);
			statisticBase.Acquire<StatisticInt32>(1, 4).Set((int)this.m_educated1Data.m_finalUnemployed);
			statisticBase.Acquire<StatisticInt32>(2, 4).Set((int)this.m_educated2Data.m_finalUnemployed);
			statisticBase.Acquire<StatisticInt32>(3, 4).Set((int)this.m_educated3Data.m_finalUnemployed);
			StatisticBase statisticBase2 = instance3.Acquire<StatisticArray>(StatisticType.EligibleWorkers);
			statisticBase2.Acquire<StatisticInt32>(0, 4).Set((int)this.m_educated0Data.m_finalEligibleWorkers);
			statisticBase2.Acquire<StatisticInt32>(1, 4).Set((int)this.m_educated1Data.m_finalEligibleWorkers);
			statisticBase2.Acquire<StatisticInt32>(2, 4).Set((int)this.m_educated2Data.m_finalEligibleWorkers);
			statisticBase2.Acquire<StatisticInt32>(3, 4).Set((int)this.m_educated3Data.m_finalEligibleWorkers);
			StatisticBase statisticBase3 = instance3.Acquire<StatisticArray>(StatisticType.WorkplaceCount);
			statisticBase3.Acquire<StatisticInt32>(0, 4).Set((int)this.m_commercialData.m_finalHomeOrWorkCount);
			statisticBase3.Acquire<StatisticInt32>(1, 4).Set((int)this.m_industrialData.m_finalHomeOrWorkCount);
			statisticBase3.Acquire<StatisticInt32>(2, 4).Set((int)this.m_officeData.m_finalHomeOrWorkCount);
			statisticBase3.Acquire<StatisticInt32>(3, 4).Set((int)this.m_playerData.m_finalHomeOrWorkCount);
			StatisticBase statisticBase4 = instance3.Acquire<StatisticInt32>(StatisticType.ElectricityCapacity);
			statisticBase4.Set((int)this.m_productionData.m_finalElectricityCapacity);
			StatisticBase statisticBase5 = instance3.Acquire<StatisticInt32>(StatisticType.RenewableElectricity);
			statisticBase5.Set((int)this.m_productionData.m_finalRenewableElectricity);
			StatisticBase statisticBase6 = instance3.Acquire<StatisticInt32>(StatisticType.PollutingElectricity);
			statisticBase6.Set((int)this.m_productionData.m_finalPollutingElectricity);
			StatisticBase statisticBase7 = instance3.Acquire<StatisticInt32>(StatisticType.WaterCapacity);
			statisticBase7.Set((int)this.m_productionData.m_finalWaterCapacity);
			StatisticBase statisticBase8 = instance3.Acquire<StatisticInt32>(StatisticType.SewageCapacity);
			statisticBase8.Set((int)this.m_productionData.m_finalSewageCapacity);
			StatisticBase statisticBase9 = instance3.Acquire<StatisticInt32>(StatisticType.IncinerationCapacity);
			statisticBase9.Set((int)this.m_productionData.m_finalIncinerationCapacity);
			StatisticBase statisticBase10 = instance3.Acquire<StatisticInt32>(StatisticType.GarbageAmount);
			statisticBase10.Set((int)this.m_productionData.m_finalGarbageAmount);
			StatisticBase statisticBase11 = instance3.Acquire<StatisticInt32>(StatisticType.GarbageCapacity);
			statisticBase11.Set((int)this.m_productionData.m_finalGarbageCapacity);
			StatisticBase statisticBase12 = instance3.Acquire<StatisticInt32>(StatisticType.HealCapacity);
			statisticBase12.Set((int)this.m_productionData.m_finalHealCapacity);
			StatisticBase statisticBase13 = instance3.Acquire<StatisticInt32>(StatisticType.CremateCapacity);
			statisticBase13.Set((int)this.m_productionData.m_finalCremateCapacity);
			StatisticBase statisticBase14 = instance3.Acquire<StatisticInt32>(StatisticType.DeadAmount);
			statisticBase14.Set((int)this.m_productionData.m_finalDeadAmount);
			StatisticBase statisticBase15 = instance3.Acquire<StatisticInt32>(StatisticType.DeadCapacity);
			statisticBase15.Set((int)this.m_productionData.m_finalDeadCapacity);
			StatisticBase statisticBase16 = instance3.Acquire<StatisticInt32>(StatisticType.PrisonerAmount);
			statisticBase16.Set((int)this.m_productionData.m_finalCriminalAmount);
			StatisticBase statisticBase17 = instance3.Acquire<StatisticArray>(StatisticType.EducationCapacity);
			statisticBase17.Acquire<StatisticInt32>(0, 3).Set((int)this.m_productionData.m_finalEducation1Capacity);
			statisticBase17.Acquire<StatisticInt32>(1, 3).Set((int)this.m_productionData.m_finalEducation2Capacity);
			statisticBase17.Acquire<StatisticInt32>(2, 3).Set((int)this.m_productionData.m_finalEducation3Capacity);
			StatisticBase statisticBase18 = instance3.Acquire<StatisticInt32>(StatisticType.GarbagePiles);
			int num4 = 0;
			int num5 = 0;
			num4 += (int)((ushort)this.m_playerConsumption.m_finalGarbagePiles * this.m_playerConsumption.m_finalBuildingCount);
			num4 += (int)((ushort)this.m_residentialConsumption.m_finalGarbagePiles * this.m_residentialConsumption.m_finalBuildingCount);
			num4 += (int)((ushort)this.m_commercialConsumption.m_finalGarbagePiles * this.m_commercialConsumption.m_finalBuildingCount);
			num4 += (int)((ushort)this.m_industrialConsumption.m_finalGarbagePiles * this.m_industrialConsumption.m_finalBuildingCount);
			num4 += (int)((ushort)this.m_officeConsumption.m_finalGarbagePiles * this.m_officeConsumption.m_finalBuildingCount);
			num5 += (int)this.m_playerConsumption.m_finalBuildingCount;
			num5 += (int)this.m_residentialConsumption.m_finalBuildingCount;
			num5 += (int)this.m_commercialConsumption.m_finalBuildingCount;
			num5 += (int)this.m_industrialConsumption.m_finalBuildingCount;
			num5 += (int)this.m_officeConsumption.m_finalBuildingCount;
			if (num5 != 0)
			{
				num4 = (num4 + (num5 >> 1)) / num5;
			}
			statisticBase18.Set(num4);
			StatisticBase statisticBase19 = instance3.Acquire<StatisticArray>(StatisticType.CitizenHealth);
			statisticBase19.Acquire<StatisticInt32>(0, 4).Set((int)this.m_residentialData.m_finalHealth);
			statisticBase19.Acquire<StatisticInt32>(1, 4).Set((int)this.m_commercialData.m_finalHealth);
			statisticBase19.Acquire<StatisticInt32>(2, 4).Set((int)this.m_industrialData.m_finalHealth);
			statisticBase19.Acquire<StatisticInt32>(3, 4).Set((int)this.m_officeData.m_finalHealth);
			StatisticBase statisticBase20 = instance3.Acquire<StatisticArray>(StatisticType.CitizenHappiness);
			statisticBase20.Acquire<StatisticInt32>(0, 4).Set((int)this.m_residentialData.m_finalHappiness);
			statisticBase20.Acquire<StatisticInt32>(1, 4).Set((int)this.m_commercialData.m_finalHappiness);
			statisticBase20.Acquire<StatisticInt32>(2, 4).Set((int)this.m_industrialData.m_finalHappiness);
			statisticBase20.Acquire<StatisticInt32>(3, 4).Set((int)this.m_officeData.m_finalHappiness);
			StatisticBase statisticBase21 = instance3.Acquire<StatisticInt32>(StatisticType.CityHappiness);
			statisticBase21.Set((int)this.m_finalHappiness);
			StatisticBase statisticBase22 = instance3.Acquire<StatisticArray>(StatisticType.CrimeRate);
			statisticBase22.Acquire<StatisticInt32>(0, 4).Set((int)this.m_finalCrimeRate);
			statisticBase22.Acquire<StatisticInt32>(1, 4).Set((int)this.m_finalCrimeRate);
			statisticBase22.Acquire<StatisticInt32>(2, 4).Set((int)this.m_finalCrimeRate);
			statisticBase22.Acquire<StatisticInt32>(3, 4).Set((int)this.m_finalCrimeRate);
			StatisticBase statisticBase23 = instance3.Acquire<StatisticArray>(StatisticType.StudentCount);
			statisticBase23.Acquire<StatisticInt32>(0, 3).Set((int)this.m_student1Data.m_finalCount);
			statisticBase23.Acquire<StatisticInt32>(1, 3).Set((int)this.m_student2Data.m_finalCount);
			statisticBase23.Acquire<StatisticInt32>(2, 3).Set((int)this.m_student3Data.m_finalCount);
			StatisticBase statisticBase24 = instance3.Acquire<StatisticArray>(StatisticType.EducatedCount);
			statisticBase24.Acquire<StatisticInt32>(0, 4).Set((int)this.m_educated0Data.m_finalCount);
			statisticBase24.Acquire<StatisticInt32>(1, 4).Set((int)this.m_educated1Data.m_finalCount);
			statisticBase24.Acquire<StatisticInt32>(2, 4).Set((int)this.m_educated2Data.m_finalCount);
			statisticBase24.Acquire<StatisticInt32>(3, 4).Set((int)this.m_educated3Data.m_finalCount);
			StatisticBase statisticBase25 = instance3.Acquire<StatisticArray>(StatisticType.AbandonedBuildings);
			statisticBase25.Acquire<StatisticInt32>(0, 4).Set((int)this.m_residentialData.m_finalAbandonedCount);
			statisticBase25.Acquire<StatisticInt32>(1, 4).Set((int)this.m_commercialData.m_finalAbandonedCount);
			statisticBase25.Acquire<StatisticInt32>(2, 4).Set((int)this.m_industrialData.m_finalAbandonedCount);
			statisticBase25.Acquire<StatisticInt32>(3, 4).Set((int)this.m_officeData.m_finalAbandonedCount);
			StatisticBase statisticBase26 = instance3.Acquire<StatisticArray>(StatisticType.BuildingArea);
			statisticBase26.Acquire<StatisticInt32>(0, 5).Set((int)this.m_residentialData.m_finalBuildingArea);
			statisticBase26.Acquire<StatisticInt32>(1, 5).Set((int)this.m_commercialData.m_finalBuildingArea);
			statisticBase26.Acquire<StatisticInt32>(2, 5).Set((int)this.m_industrialData.m_finalBuildingArea);
			statisticBase26.Acquire<StatisticInt32>(3, 5).Set((int)this.m_officeData.m_finalBuildingArea);
			statisticBase26.Acquire<StatisticInt32>(4, 5).Set((int)this.m_playerData.m_finalBuildingArea);
			StatisticBase statisticBase27 = instance3.Acquire<StatisticArray>(StatisticType.SpecializationArea);
			statisticBase27.Acquire<StatisticInt32>(0, 9).Set((int)this.m_farmingData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(1, 9).Set((int)this.m_forestryData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(2, 9).Set((int)this.m_oreData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(3, 9).Set((int)this.m_oilData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(4, 9).Set((int)this.m_leisureData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(5, 9).Set((int)this.m_touristData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(6, 9).Set((int)this.m_organicData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(7, 9).Set((int)this.m_selfsufficientData.m_finalBuildingArea);
			statisticBase27.Acquire<StatisticInt32>(8, 9).Set((int)this.m_hightechData.m_finalBuildingArea);
			StatisticBase statisticBase28 = instance3.Acquire<StatisticInt32>(StatisticType.GroundPollution);
			statisticBase28.Set(this.GetGroundPollution());
			StatisticBase statisticBase29 = instance3.Acquire<StatisticInt32>(StatisticType.WaterPollution);
			statisticBase29.Set(this.GetWaterPollution());
			StatisticBase statisticBase30 = instance3.Acquire<StatisticArray>(StatisticType.TouristVisits);
			statisticBase30.Acquire<StatisticInt32>(0, 3).Set((int)this.m_tourist1Data.m_averageCount);
			statisticBase30.Acquire<StatisticInt32>(1, 3).Set((int)this.m_tourist2Data.m_averageCount);
			statisticBase30.Acquire<StatisticInt32>(2, 3).Set((int)this.m_tourist3Data.m_averageCount);
			StatisticBase statisticBase31 = instance3.Acquire<StatisticInt32>(StatisticType.BirthRate);
			statisticBase31.Set((int)this.m_birthData.m_finalCount);
			StatisticBase statisticBase32 = instance3.Acquire<StatisticInt32>(StatisticType.DeathRate);
			statisticBase32.Set((int)this.m_deathData.m_finalCount);
			StatisticBase statisticBase33 = instance3.Acquire<StatisticInt32>(StatisticType.HeatingCapacity);
			statisticBase33.Set((int)this.m_productionData.m_finalHeatingCapacity);
			StatisticBase statisticBase34 = instance3.Acquire<StatisticArray>(StatisticType.HeatingConsumption);
			statisticBase34.Acquire<StatisticInt32>(0, 5).Set((int)this.m_residentialConsumption.m_finalHeatingConsumption);
			statisticBase34.Acquire<StatisticInt32>(1, 5).Set((int)this.m_commercialConsumption.m_finalHeatingConsumption);
			statisticBase34.Acquire<StatisticInt32>(2, 5).Set((int)this.m_industrialConsumption.m_finalHeatingConsumption);
			statisticBase34.Acquire<StatisticInt32>(3, 5).Set((int)this.m_officeConsumption.m_finalHeatingConsumption);
			statisticBase34.Acquire<StatisticInt32>(4, 5).Set((int)this.m_playerConsumption.m_finalHeatingConsumption);
			StatisticBase statisticBase35 = instance3.Acquire<StatisticInt32>(StatisticType.HeatingUsage);
			statisticBase35.Set((int)this.m_usageData.m_finalHeatingUsage);
			StatisticBase statisticBase36 = instance3.Acquire<StatisticInt32>(StatisticType.SnowCover);
			statisticBase36.Set((int)this.m_productionData.m_finalSnowCover);
		}
	}

	private void AddDistrictData(ref District source, bool lastFrame)
	{
		this.m_populationData.Add(ref source.m_populationData);
		this.m_productionData.Add(ref source.m_productionData);
		this.m_groundData.Add(ref source.m_groundData);
		this.m_childData.Add(ref source.m_childData);
		this.m_teenData.Add(ref source.m_teenData);
		this.m_youngData.Add(ref source.m_youngData);
		this.m_adultData.Add(ref source.m_adultData);
		this.m_seniorData.Add(ref source.m_seniorData);
		this.m_educated0Data.Add(ref source.m_educated0Data);
		this.m_educated1Data.Add(ref source.m_educated1Data);
		this.m_educated2Data.Add(ref source.m_educated2Data);
		this.m_educated3Data.Add(ref source.m_educated3Data);
		this.m_education1Data.Add(ref source.m_education1Data);
		this.m_education2Data.Add(ref source.m_education2Data);
		this.m_education3Data.Add(ref source.m_education3Data);
		this.m_student1Data.Add(ref source.m_student1Data);
		this.m_student2Data.Add(ref source.m_student2Data);
		this.m_student3Data.Add(ref source.m_student3Data);
		this.m_residentialData.Add(ref source.m_residentialData);
		this.m_commercialData.Add(ref source.m_commercialData);
		this.m_visitorData.Add(ref source.m_visitorData);
		this.m_industrialData.Add(ref source.m_industrialData);
		this.m_officeData.Add(ref source.m_officeData);
		this.m_playerData.Add(ref source.m_playerData);
		this.m_farmingData.Add(ref source.m_farmingData);
		this.m_forestryData.Add(ref source.m_forestryData);
		this.m_oreData.Add(ref source.m_oreData);
		this.m_oilData.Add(ref source.m_oilData);
		this.m_leisureData.Add(ref source.m_leisureData);
		this.m_touristData.Add(ref source.m_touristData);
		this.m_organicData.Add(ref source.m_organicData);
		this.m_selfsufficientData.Add(ref source.m_selfsufficientData);
		this.m_hightechData.Add(ref source.m_hightechData);
		this.m_playerConsumption.Add(ref source.m_playerConsumption);
		this.m_residentialConsumption.Add(ref source.m_residentialConsumption);
		this.m_commercialConsumption.Add(ref source.m_commercialConsumption);
		this.m_industrialConsumption.Add(ref source.m_industrialConsumption);
		this.m_officeConsumption.Add(ref source.m_officeConsumption);
		this.m_usageData.Add(ref source.m_usageData);
		this.m_hippieData.Add(ref source.m_hippieData);
		this.m_hipsterData.Add(ref source.m_hipsterData);
		this.m_redneckData.Add(ref source.m_redneckData);
		this.m_gangstaData.Add(ref source.m_gangstaData);
		if (lastFrame)
		{
			this.m_birthData.Add(ref source.m_birthData);
			this.m_deathData.Add(ref source.m_deathData);
			this.m_importData.Add(ref source.m_importData);
			this.m_exportData.Add(ref source.m_exportData);
			this.m_tourist1Data.Add(ref source.m_tourist1Data);
			this.m_tourist2Data.Add(ref source.m_tourist2Data);
			this.m_tourist3Data.Add(ref source.m_tourist3Data);
		}
	}

	public void AddPlayerData(ref Citizen.BehaviourData behaviour, int crimeRate, int workCount, int aliveWorkCount, int emptyWorkCount, int visitCount, int aliveVisitCount, int emptyVisitCount, int electricityConsumption, int heatingConsumption, int waterConsumption, int sewageAccumulation, int garbageAccumulation, int incomeAccumulation, int garbagePiles, int waterPollution)
	{
		this.m_playerData.m_tempCrimeRate = this.m_playerData.m_tempCrimeRate + (uint)(crimeRate * workCount);
		this.m_playerData.m_tempHomeOrWorkCount = this.m_playerData.m_tempHomeOrWorkCount + (uint)workCount;
		this.m_playerData.m_tempAliveCount = this.m_playerData.m_tempAliveCount + (uint)aliveWorkCount;
		this.m_playerData.m_tempEmptyCount = this.m_playerData.m_tempEmptyCount + (uint)emptyWorkCount;
		this.m_playerConsumption.m_tempElectricityConsumption = this.m_playerConsumption.m_tempElectricityConsumption + (uint)electricityConsumption;
		this.m_playerConsumption.m_tempHeatingConsumption = this.m_playerConsumption.m_tempHeatingConsumption + (uint)heatingConsumption;
		this.m_playerConsumption.m_tempWaterConsumption = this.m_playerConsumption.m_tempWaterConsumption + (uint)waterConsumption;
		this.m_playerConsumption.m_tempSewageAccumulation = this.m_playerConsumption.m_tempSewageAccumulation + (uint)sewageAccumulation;
		this.m_playerConsumption.m_tempGarbageAccumulation = this.m_playerConsumption.m_tempGarbageAccumulation + (uint)garbageAccumulation;
		this.m_playerConsumption.m_tempBuildingCount = this.m_playerConsumption.m_tempBuildingCount + 1;
		this.m_playerConsumption.m_tempGarbagePiles = this.m_playerConsumption.m_tempGarbagePiles + (uint)garbagePiles;
		this.m_playerConsumption.m_tempWaterPollution = this.m_playerConsumption.m_tempWaterPollution + (uint)waterPollution;
		this.m_playerConsumption.m_tempDeadCount = this.m_playerConsumption.m_tempDeadCount + (uint)behaviour.m_deadCount;
		this.m_playerConsumption.m_tempSickCount = this.m_playerConsumption.m_tempSickCount + (uint)behaviour.m_sickCount;
	}

	public void AddGroundData(int landvalue, int pollution, int coverage)
	{
		this.m_groundData.m_tempLandvalue = this.m_groundData.m_tempLandvalue + (uint)(landvalue * coverage);
		this.m_groundData.m_tempPollution = this.m_groundData.m_tempPollution + (uint)(pollution * coverage);
		this.m_groundData.m_tempCoverage = this.m_groundData.m_tempCoverage + (uint)coverage;
	}

	public void AddResidentialData(ref Citizen.BehaviourData behaviour, int citizenCount, int health, int happiness, int crimeRate, int homeCount, int aliveHomeCount, int emptyHomeCount, int level, int electricityConsumption, int heatingConsumption, int waterConsumption, int sewageAccumulation, int garbageAccumulation, int incomeAccumulation, int garbagePiles, int waterPollution, ItemClass.SubService subService)
	{
		this.m_populationData.m_tempCount = this.m_populationData.m_tempCount + (uint)citizenCount;
		this.m_childData.m_tempCount = this.m_childData.m_tempCount + (uint)behaviour.m_childCount;
		this.m_teenData.m_tempCount = this.m_teenData.m_tempCount + (uint)behaviour.m_teenCount;
		this.m_youngData.m_tempCount = this.m_youngData.m_tempCount + (uint)behaviour.m_youngCount;
		this.m_adultData.m_tempCount = this.m_adultData.m_tempCount + (uint)behaviour.m_adultCount;
		this.m_seniorData.m_tempCount = this.m_seniorData.m_tempCount + (uint)behaviour.m_seniorCount;
		this.m_educated0Data.m_tempCount = this.m_educated0Data.m_tempCount + (uint)behaviour.m_educated0Count;
		this.m_educated0Data.m_tempUnemployed = this.m_educated0Data.m_tempUnemployed + (uint)behaviour.m_educated0Unemployed;
		this.m_educated0Data.m_tempEligibleWorkers = this.m_educated0Data.m_tempEligibleWorkers + (uint)behaviour.m_educated0EligibleWorkers;
		this.m_educated1Data.m_tempCount = this.m_educated1Data.m_tempCount + (uint)behaviour.m_educated1Count;
		this.m_educated1Data.m_tempUnemployed = this.m_educated1Data.m_tempUnemployed + (uint)behaviour.m_educated1Unemployed;
		this.m_educated1Data.m_tempEligibleWorkers = this.m_educated1Data.m_tempEligibleWorkers + (uint)behaviour.m_educated1EligibleWorkers;
		this.m_educated2Data.m_tempCount = this.m_educated2Data.m_tempCount + (uint)behaviour.m_educated2Count;
		this.m_educated2Data.m_tempUnemployed = this.m_educated2Data.m_tempUnemployed + (uint)behaviour.m_educated2Unemployed;
		this.m_educated2Data.m_tempEligibleWorkers = this.m_educated2Data.m_tempEligibleWorkers + (uint)behaviour.m_educated2EligibleWorkers;
		this.m_educated3Data.m_tempCount = this.m_educated3Data.m_tempCount + (uint)behaviour.m_educated3Count;
		this.m_educated3Data.m_tempUnemployed = this.m_educated3Data.m_tempUnemployed + (uint)behaviour.m_educated3Unemployed;
		this.m_educated3Data.m_tempEligibleWorkers = this.m_educated3Data.m_tempEligibleWorkers + (uint)behaviour.m_educated3EligibleWorkers;
		this.m_education1Data.m_tempCount = this.m_education1Data.m_tempCount + (uint)behaviour.m_education1Count;
		this.m_education2Data.m_tempCount = this.m_education2Data.m_tempCount + (uint)behaviour.m_education2Count;
		this.m_education3Data.m_tempCount = this.m_education3Data.m_tempCount + (uint)behaviour.m_education3Count;
		this.m_residentialData.m_tempHealth = this.m_residentialData.m_tempHealth + (uint)(health * homeCount);
		this.m_residentialData.m_tempHappiness = this.m_residentialData.m_tempHappiness + (uint)(happiness * homeCount);
		this.m_residentialData.m_tempCrimeRate = this.m_residentialData.m_tempCrimeRate + (uint)(crimeRate * homeCount);
		this.m_residentialData.m_tempHomeOrWorkCount = this.m_residentialData.m_tempHomeOrWorkCount + (uint)homeCount;
		this.m_residentialData.m_tempAliveCount = this.m_residentialData.m_tempAliveCount + (uint)aliveHomeCount;
		this.m_residentialData.m_tempEmptyCount = this.m_residentialData.m_tempEmptyCount + (uint)emptyHomeCount;
		this.m_residentialData.m_tempLevel = this.m_residentialData.m_tempLevel + (uint)(level * homeCount);
		switch (level)
		{
		case 0:
			this.m_residentialData.m_tempLevel1 = this.m_residentialData.m_tempLevel1 + (uint)homeCount;
			break;
		case 1:
			this.m_residentialData.m_tempLevel2 = this.m_residentialData.m_tempLevel2 + (uint)homeCount;
			break;
		case 2:
			this.m_residentialData.m_tempLevel3 = this.m_residentialData.m_tempLevel3 + (uint)homeCount;
			break;
		case 3:
			this.m_residentialData.m_tempLevel4 = this.m_residentialData.m_tempLevel4 + (uint)homeCount;
			break;
		case 4:
			this.m_residentialData.m_tempLevel5 = this.m_residentialData.m_tempLevel5 + (uint)homeCount;
			break;
		}
		this.m_residentialConsumption.m_tempElectricityConsumption = this.m_residentialConsumption.m_tempElectricityConsumption + (uint)electricityConsumption;
		this.m_residentialConsumption.m_tempHeatingConsumption = this.m_residentialConsumption.m_tempHeatingConsumption + (uint)heatingConsumption;
		this.m_residentialConsumption.m_tempWaterConsumption = this.m_residentialConsumption.m_tempWaterConsumption + (uint)waterConsumption;
		this.m_residentialConsumption.m_tempSewageAccumulation = this.m_residentialConsumption.m_tempSewageAccumulation + (uint)sewageAccumulation;
		this.m_residentialConsumption.m_tempGarbageAccumulation = this.m_residentialConsumption.m_tempGarbageAccumulation + (uint)garbageAccumulation;
		this.m_residentialConsumption.m_tempBuildingCount = this.m_residentialConsumption.m_tempBuildingCount + 1;
		this.m_residentialConsumption.m_tempGarbagePiles = this.m_residentialConsumption.m_tempGarbagePiles + (uint)garbagePiles;
		this.m_residentialConsumption.m_tempWaterPollution = this.m_residentialConsumption.m_tempWaterPollution + (uint)waterPollution;
		this.m_residentialConsumption.m_tempDeadCount = this.m_residentialConsumption.m_tempDeadCount + (uint)behaviour.m_deadCount;
		this.m_residentialConsumption.m_tempSickCount = this.m_residentialConsumption.m_tempSickCount + (uint)behaviour.m_totalSickCount;
		if (subService == ItemClass.SubService.ResidentialLowEco || subService == ItemClass.SubService.ResidentialHighEco)
		{
			this.m_selfsufficientData.m_tempHomeOrWorkCount = this.m_selfsufficientData.m_tempHomeOrWorkCount + (uint)homeCount;
			this.m_selfsufficientData.m_tempAliveCount = this.m_selfsufficientData.m_tempAliveCount + (uint)aliveHomeCount;
			this.m_selfsufficientData.m_tempEmptyCount = this.m_selfsufficientData.m_tempEmptyCount + (uint)emptyHomeCount;
		}
	}

	public void AddResidentialData(int buildingArea, bool abandoned, bool burned, bool collapsed, ItemClass.SubService subService)
	{
		this.m_residentialData.m_tempBuildingCount = this.m_residentialData.m_tempBuildingCount + 1;
		this.m_residentialData.m_tempBuildingArea = this.m_residentialData.m_tempBuildingArea + (uint)buildingArea;
		if (abandoned)
		{
			this.m_residentialData.m_tempAbandonedCount = this.m_residentialData.m_tempAbandonedCount + 1;
		}
		else if (burned)
		{
			this.m_residentialData.m_tempBurnedCount = this.m_residentialData.m_tempBurnedCount + 1;
		}
		else if (collapsed)
		{
			this.m_residentialData.m_tempCollapsedCount = this.m_residentialData.m_tempCollapsedCount + 1;
		}
		if (subService == ItemClass.SubService.ResidentialLowEco || subService == ItemClass.SubService.ResidentialHighEco)
		{
			this.m_selfsufficientData.m_tempBuildingArea = this.m_selfsufficientData.m_tempBuildingArea + (uint)buildingArea;
		}
	}

	public void AddCommercialData(ref Citizen.BehaviourData behaviour, int health, int happiness, int crimeRate, int workCount, int aliveWorkCount, int emptyWorkCount, int visitCount, int aliveVisitCount, int emptyVisitCount, int level, int electricityConsumption, int heatingConsumption, int waterConsumption, int sewageAccumulation, int garbageAccumulation, int incomeAccumulation, int garbagePiles, int waterPollution, int import, int export, ItemClass.SubService subService)
	{
		this.m_commercialData.m_tempHealth = this.m_commercialData.m_tempHealth + (uint)(health * workCount);
		this.m_commercialData.m_tempHappiness = this.m_commercialData.m_tempHappiness + (uint)(happiness * workCount);
		this.m_commercialData.m_tempCrimeRate = this.m_commercialData.m_tempCrimeRate + (uint)(crimeRate * workCount);
		this.m_commercialData.m_tempHomeOrWorkCount = this.m_commercialData.m_tempHomeOrWorkCount + (uint)workCount;
		this.m_commercialData.m_tempAliveCount = this.m_commercialData.m_tempAliveCount + (uint)aliveWorkCount;
		this.m_commercialData.m_tempEmptyCount = this.m_commercialData.m_tempEmptyCount + (uint)emptyWorkCount;
		this.m_commercialData.m_tempLevel = this.m_commercialData.m_tempLevel + (uint)(level * workCount);
		switch (level)
		{
		case 0:
			this.m_commercialData.m_tempLevel1 = this.m_commercialData.m_tempLevel1 + (uint)workCount;
			break;
		case 1:
			this.m_commercialData.m_tempLevel2 = this.m_commercialData.m_tempLevel2 + (uint)workCount;
			break;
		case 2:
			this.m_commercialData.m_tempLevel3 = this.m_commercialData.m_tempLevel3 + (uint)workCount;
			break;
		case 3:
			this.m_commercialData.m_tempLevel4 = this.m_commercialData.m_tempLevel4 + (uint)workCount;
			break;
		case 4:
			this.m_commercialData.m_tempLevel5 = this.m_commercialData.m_tempLevel5 + (uint)workCount;
			break;
		}
		this.m_commercialConsumption.m_tempElectricityConsumption = this.m_commercialConsumption.m_tempElectricityConsumption + (uint)electricityConsumption;
		this.m_commercialConsumption.m_tempHeatingConsumption = this.m_commercialConsumption.m_tempHeatingConsumption + (uint)heatingConsumption;
		this.m_commercialConsumption.m_tempWaterConsumption = this.m_commercialConsumption.m_tempWaterConsumption + (uint)waterConsumption;
		this.m_commercialConsumption.m_tempSewageAccumulation = this.m_commercialConsumption.m_tempSewageAccumulation + (uint)sewageAccumulation;
		this.m_commercialConsumption.m_tempGarbageAccumulation = this.m_commercialConsumption.m_tempGarbageAccumulation + (uint)garbageAccumulation;
		this.m_commercialConsumption.m_tempBuildingCount = this.m_commercialConsumption.m_tempBuildingCount + 1;
		this.m_commercialConsumption.m_tempGarbagePiles = this.m_commercialConsumption.m_tempGarbagePiles + (uint)garbagePiles;
		this.m_commercialConsumption.m_tempWaterPollution = this.m_commercialConsumption.m_tempWaterPollution + (uint)waterPollution;
		this.m_commercialConsumption.m_tempDeadCount = this.m_commercialConsumption.m_tempDeadCount + (uint)behaviour.m_deadCount;
		this.m_commercialConsumption.m_tempSickCount = this.m_commercialConsumption.m_tempSickCount + (uint)behaviour.m_sickCount;
		this.m_commercialConsumption.m_tempExportAmount = this.m_commercialConsumption.m_tempExportAmount + (uint)export;
		this.m_commercialConsumption.m_tempImportAmount = this.m_commercialConsumption.m_tempImportAmount + (uint)import;
		this.m_visitorData.m_tempHomeOrWorkCount = this.m_visitorData.m_tempHomeOrWorkCount + (uint)visitCount;
		this.m_visitorData.m_tempAliveCount = this.m_visitorData.m_tempAliveCount + (uint)aliveVisitCount;
		this.m_visitorData.m_tempEmptyCount = this.m_visitorData.m_tempEmptyCount + (uint)emptyVisitCount;
		if (subService != ItemClass.SubService.CommercialLeisure)
		{
			if (subService != ItemClass.SubService.CommercialTourist)
			{
				if (subService == ItemClass.SubService.CommercialEco)
				{
					this.m_organicData.m_tempHomeOrWorkCount = this.m_organicData.m_tempHomeOrWorkCount + (uint)workCount;
					this.m_organicData.m_tempAliveCount = this.m_organicData.m_tempAliveCount + (uint)aliveWorkCount;
					this.m_organicData.m_tempEmptyCount = this.m_organicData.m_tempEmptyCount + (uint)emptyWorkCount;
				}
			}
			else
			{
				this.m_touristData.m_tempHomeOrWorkCount = this.m_touristData.m_tempHomeOrWorkCount + (uint)workCount;
				this.m_touristData.m_tempAliveCount = this.m_touristData.m_tempAliveCount + (uint)aliveWorkCount;
				this.m_touristData.m_tempEmptyCount = this.m_touristData.m_tempEmptyCount + (uint)emptyWorkCount;
			}
		}
		else
		{
			this.m_leisureData.m_tempHomeOrWorkCount = this.m_leisureData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_leisureData.m_tempAliveCount = this.m_leisureData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_leisureData.m_tempEmptyCount = this.m_leisureData.m_tempEmptyCount + (uint)emptyWorkCount;
		}
	}

	public void AddCommercialData(int buildingArea, bool abandoned, bool burned, bool collapsed, ItemClass.SubService subService)
	{
		this.m_commercialData.m_tempBuildingCount = this.m_commercialData.m_tempBuildingCount + 1;
		this.m_commercialData.m_tempBuildingArea = this.m_commercialData.m_tempBuildingArea + (uint)buildingArea;
		if (abandoned)
		{
			this.m_commercialData.m_tempAbandonedCount = this.m_commercialData.m_tempAbandonedCount + 1;
		}
		else if (burned)
		{
			this.m_commercialData.m_tempBurnedCount = this.m_commercialData.m_tempBurnedCount + 1;
		}
		else if (collapsed)
		{
			this.m_commercialData.m_tempCollapsedCount = this.m_commercialData.m_tempCollapsedCount + 1;
		}
		if (subService != ItemClass.SubService.CommercialLeisure)
		{
			if (subService != ItemClass.SubService.CommercialTourist)
			{
				if (subService == ItemClass.SubService.CommercialEco)
				{
					this.m_organicData.m_tempBuildingArea = this.m_organicData.m_tempBuildingArea + (uint)buildingArea;
				}
			}
			else
			{
				this.m_touristData.m_tempBuildingArea = this.m_touristData.m_tempBuildingArea + (uint)buildingArea;
			}
		}
		else
		{
			this.m_leisureData.m_tempBuildingArea = this.m_leisureData.m_tempBuildingArea + (uint)buildingArea;
		}
	}

	public void AddIndustrialData(ref Citizen.BehaviourData behaviour, int health, int happiness, int crimeRate, int workCount, int aliveWorkCount, int emptyWorkCount, int level, int electricityConsumption, int heatingConsumption, int waterConsumption, int sewageAccumulation, int garbageAccumulation, int incomeAccumulation, int garbagePiles, int waterPollution, int import, int export, ItemClass.SubService subService)
	{
		this.m_industrialData.m_tempHealth = this.m_industrialData.m_tempHealth + (uint)(health * workCount);
		this.m_industrialData.m_tempHappiness = this.m_industrialData.m_tempHappiness + (uint)(happiness * workCount);
		this.m_industrialData.m_tempCrimeRate = this.m_industrialData.m_tempCrimeRate + (uint)(crimeRate * workCount);
		this.m_industrialData.m_tempHomeOrWorkCount = this.m_industrialData.m_tempHomeOrWorkCount + (uint)workCount;
		this.m_industrialData.m_tempAliveCount = this.m_industrialData.m_tempAliveCount + (uint)aliveWorkCount;
		this.m_industrialData.m_tempEmptyCount = this.m_industrialData.m_tempEmptyCount + (uint)emptyWorkCount;
		this.m_industrialData.m_tempLevel = this.m_industrialData.m_tempLevel + (uint)(level * workCount);
		switch (level)
		{
		case 0:
			this.m_industrialData.m_tempLevel1 = this.m_industrialData.m_tempLevel1 + (uint)workCount;
			break;
		case 1:
			this.m_industrialData.m_tempLevel2 = this.m_industrialData.m_tempLevel2 + (uint)workCount;
			break;
		case 2:
			this.m_industrialData.m_tempLevel3 = this.m_industrialData.m_tempLevel3 + (uint)workCount;
			break;
		case 3:
			this.m_industrialData.m_tempLevel4 = this.m_industrialData.m_tempLevel4 + (uint)workCount;
			break;
		case 4:
			this.m_industrialData.m_tempLevel5 = this.m_industrialData.m_tempLevel5 + (uint)workCount;
			break;
		}
		this.m_industrialConsumption.m_tempElectricityConsumption = this.m_industrialConsumption.m_tempElectricityConsumption + (uint)electricityConsumption;
		this.m_industrialConsumption.m_tempHeatingConsumption = this.m_industrialConsumption.m_tempHeatingConsumption + (uint)heatingConsumption;
		this.m_industrialConsumption.m_tempWaterConsumption = this.m_industrialConsumption.m_tempWaterConsumption + (uint)waterConsumption;
		this.m_industrialConsumption.m_tempSewageAccumulation = this.m_industrialConsumption.m_tempSewageAccumulation + (uint)sewageAccumulation;
		this.m_industrialConsumption.m_tempGarbageAccumulation = this.m_industrialConsumption.m_tempGarbageAccumulation + (uint)garbageAccumulation;
		this.m_industrialConsumption.m_tempBuildingCount = this.m_industrialConsumption.m_tempBuildingCount + 1;
		this.m_industrialConsumption.m_tempGarbagePiles = this.m_industrialConsumption.m_tempGarbagePiles + (uint)garbagePiles;
		this.m_industrialConsumption.m_tempWaterPollution = this.m_industrialConsumption.m_tempWaterPollution + (uint)waterPollution;
		this.m_industrialConsumption.m_tempDeadCount = this.m_industrialConsumption.m_tempDeadCount + (uint)behaviour.m_deadCount;
		this.m_industrialConsumption.m_tempSickCount = this.m_industrialConsumption.m_tempSickCount + (uint)behaviour.m_sickCount;
		this.m_industrialConsumption.m_tempExportAmount = this.m_industrialConsumption.m_tempExportAmount + (uint)export;
		this.m_industrialConsumption.m_tempImportAmount = this.m_industrialConsumption.m_tempImportAmount + (uint)import;
		switch (subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			this.m_forestryData.m_tempHomeOrWorkCount = this.m_forestryData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_forestryData.m_tempAliveCount = this.m_forestryData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_forestryData.m_tempEmptyCount = this.m_forestryData.m_tempEmptyCount + (uint)emptyWorkCount;
			break;
		case ItemClass.SubService.IndustrialFarming:
			this.m_farmingData.m_tempHomeOrWorkCount = this.m_farmingData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_farmingData.m_tempAliveCount = this.m_farmingData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_farmingData.m_tempEmptyCount = this.m_farmingData.m_tempEmptyCount + (uint)emptyWorkCount;
			break;
		case ItemClass.SubService.IndustrialOil:
			this.m_oilData.m_tempHomeOrWorkCount = this.m_oilData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_oilData.m_tempAliveCount = this.m_oilData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_oilData.m_tempEmptyCount = this.m_oilData.m_tempEmptyCount + (uint)emptyWorkCount;
			break;
		case ItemClass.SubService.IndustrialOre:
			this.m_oreData.m_tempHomeOrWorkCount = this.m_oreData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_oreData.m_tempAliveCount = this.m_oreData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_oreData.m_tempEmptyCount = this.m_oreData.m_tempEmptyCount + (uint)emptyWorkCount;
			break;
		}
	}

	public void AddIndustrialData(int buildingArea, bool abandoned, bool burned, bool collapsed, ItemClass.SubService subService)
	{
		this.m_industrialData.m_tempBuildingCount = this.m_industrialData.m_tempBuildingCount + 1;
		this.m_industrialData.m_tempBuildingArea = this.m_industrialData.m_tempBuildingArea + (uint)buildingArea;
		if (abandoned)
		{
			this.m_industrialData.m_tempAbandonedCount = this.m_industrialData.m_tempAbandonedCount + 1;
		}
		else if (burned)
		{
			this.m_industrialData.m_tempBurnedCount = this.m_industrialData.m_tempBurnedCount + 1;
		}
		else if (collapsed)
		{
			this.m_industrialData.m_tempCollapsedCount = this.m_industrialData.m_tempCollapsedCount + 1;
		}
		switch (subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			this.m_forestryData.m_tempBuildingArea = this.m_forestryData.m_tempBuildingArea + (uint)buildingArea;
			break;
		case ItemClass.SubService.IndustrialFarming:
			this.m_farmingData.m_tempBuildingArea = this.m_farmingData.m_tempBuildingArea + (uint)buildingArea;
			break;
		case ItemClass.SubService.IndustrialOil:
			this.m_oilData.m_tempBuildingArea = this.m_oilData.m_tempBuildingArea + (uint)buildingArea;
			break;
		case ItemClass.SubService.IndustrialOre:
			this.m_oreData.m_tempBuildingArea = this.m_oreData.m_tempBuildingArea + (uint)buildingArea;
			break;
		}
	}

	public void AddOfficeData(ref Citizen.BehaviourData behaviour, int health, int happiness, int crimeRate, int workCount, int aliveWorkCount, int emptyWorkCount, int level, int electricityConsumption, int heatingConsumption, int waterConsumption, int sewageAccumulation, int garbageAccumulation, int incomeAccumulation, int garbagePiles, int waterPollution, ItemClass.SubService subService)
	{
		this.m_officeData.m_tempHealth = this.m_officeData.m_tempHealth + (uint)(health * workCount);
		this.m_officeData.m_tempHappiness = this.m_officeData.m_tempHappiness + (uint)(happiness * workCount);
		this.m_officeData.m_tempCrimeRate = this.m_officeData.m_tempCrimeRate + (uint)(crimeRate * workCount);
		this.m_officeData.m_tempHomeOrWorkCount = this.m_officeData.m_tempHomeOrWorkCount + (uint)workCount;
		this.m_officeData.m_tempAliveCount = this.m_officeData.m_tempAliveCount + (uint)aliveWorkCount;
		this.m_officeData.m_tempEmptyCount = this.m_officeData.m_tempEmptyCount + (uint)emptyWorkCount;
		this.m_officeData.m_tempLevel = this.m_officeData.m_tempLevel + (uint)(level * workCount);
		switch (level)
		{
		case 0:
			this.m_officeData.m_tempLevel1 = this.m_officeData.m_tempLevel1 + (uint)workCount;
			break;
		case 1:
			this.m_officeData.m_tempLevel2 = this.m_officeData.m_tempLevel2 + (uint)workCount;
			break;
		case 2:
			this.m_officeData.m_tempLevel3 = this.m_officeData.m_tempLevel3 + (uint)workCount;
			break;
		case 3:
			this.m_officeData.m_tempLevel4 = this.m_officeData.m_tempLevel4 + (uint)workCount;
			break;
		case 4:
			this.m_officeData.m_tempLevel5 = this.m_officeData.m_tempLevel5 + (uint)workCount;
			break;
		}
		this.m_officeConsumption.m_tempElectricityConsumption = this.m_officeConsumption.m_tempElectricityConsumption + (uint)electricityConsumption;
		this.m_officeConsumption.m_tempHeatingConsumption = this.m_officeConsumption.m_tempHeatingConsumption + (uint)heatingConsumption;
		this.m_officeConsumption.m_tempWaterConsumption = this.m_officeConsumption.m_tempWaterConsumption + (uint)waterConsumption;
		this.m_officeConsumption.m_tempSewageAccumulation = this.m_officeConsumption.m_tempSewageAccumulation + (uint)sewageAccumulation;
		this.m_officeConsumption.m_tempGarbageAccumulation = this.m_officeConsumption.m_tempGarbageAccumulation + (uint)garbageAccumulation;
		this.m_officeConsumption.m_tempBuildingCount = this.m_officeConsumption.m_tempBuildingCount + 1;
		this.m_officeConsumption.m_tempGarbagePiles = this.m_officeConsumption.m_tempGarbagePiles + (uint)garbagePiles;
		this.m_officeConsumption.m_tempWaterPollution = this.m_officeConsumption.m_tempWaterPollution + (uint)waterPollution;
		this.m_officeConsumption.m_tempDeadCount = this.m_officeConsumption.m_tempDeadCount + (uint)behaviour.m_deadCount;
		this.m_officeConsumption.m_tempSickCount = this.m_officeConsumption.m_tempSickCount + (uint)behaviour.m_sickCount;
		if (subService == ItemClass.SubService.OfficeHightech)
		{
			this.m_hightechData.m_tempHomeOrWorkCount = this.m_hightechData.m_tempHomeOrWorkCount + (uint)workCount;
			this.m_hightechData.m_tempAliveCount = this.m_hightechData.m_tempAliveCount + (uint)aliveWorkCount;
			this.m_hightechData.m_tempEmptyCount = this.m_hightechData.m_tempEmptyCount + (uint)emptyWorkCount;
		}
	}

	public void AddOfficeData(int buildingArea, bool abandoned, bool burned, bool collapsed, ItemClass.SubService subService)
	{
		this.m_officeData.m_tempBuildingCount = this.m_officeData.m_tempBuildingCount + 1;
		this.m_officeData.m_tempBuildingArea = this.m_officeData.m_tempBuildingArea + (uint)buildingArea;
		if (abandoned)
		{
			this.m_officeData.m_tempAbandonedCount = this.m_officeData.m_tempAbandonedCount + 1;
		}
		else if (burned)
		{
			this.m_officeData.m_tempBurnedCount = this.m_officeData.m_tempBurnedCount + 1;
		}
		else if (collapsed)
		{
			this.m_officeData.m_tempCollapsedCount = this.m_officeData.m_tempCollapsedCount + 1;
		}
		if (subService == ItemClass.SubService.OfficeHightech)
		{
			this.m_hightechData.m_tempBuildingArea = this.m_hightechData.m_tempBuildingArea + (uint)buildingArea;
		}
	}

	public void AddUsageData(int electricityUsage, int heatingUsage, int waterUsage, int sewageUsage)
	{
		this.m_usageData.m_tempElectricityUsage = this.m_usageData.m_tempElectricityUsage + (uint)electricityUsage;
		this.m_usageData.m_tempHeatingUsage = this.m_usageData.m_tempHeatingUsage + (uint)heatingUsage;
		this.m_usageData.m_tempWaterUsage = this.m_usageData.m_tempWaterUsage + (uint)waterUsage;
		this.m_usageData.m_tempSewageUsage = this.m_usageData.m_tempSewageUsage + (uint)sewageUsage;
	}

	public int GetElectricityCapacity()
	{
		return (int)(this.m_productionData.m_finalElectricityCapacity * 16u);
	}

	public int GetHeatingCapacity()
	{
		return (int)(this.m_productionData.m_finalHeatingCapacity * 16u);
	}

	public int GetWaterCapacity()
	{
		return (int)(this.m_productionData.m_finalWaterCapacity * 16u);
	}

	public int GetSewageCapacity()
	{
		return (int)(this.m_productionData.m_finalSewageCapacity * 16u);
	}

	public int GetIncinerationCapacity()
	{
		return (int)(this.m_productionData.m_finalIncinerationCapacity * 16u);
	}

	public int GetGarbageAmount()
	{
		return (int)this.m_productionData.m_finalGarbageAmount;
	}

	public int GetGarbageCapacity()
	{
		return (int)this.m_productionData.m_finalGarbageCapacity;
	}

	public int GetHealCapacity()
	{
		return (int)this.m_productionData.m_finalHealCapacity;
	}

	public int GetCremateCapacity()
	{
		return (int)this.m_productionData.m_finalCremateCapacity;
	}

	public int GetDeadAmount()
	{
		return (int)this.m_productionData.m_finalDeadAmount;
	}

	public int GetDeadCapacity()
	{
		return (int)this.m_productionData.m_finalDeadCapacity;
	}

	public int GetEducation1Capacity()
	{
		return (int)this.m_productionData.m_finalEducation1Capacity;
	}

	public int GetEducation2Capacity()
	{
		return (int)this.m_productionData.m_finalEducation2Capacity;
	}

	public int GetEducation3Capacity()
	{
		return (int)this.m_productionData.m_finalEducation3Capacity;
	}

	public int GetCriminalAmount()
	{
		return (int)this.m_productionData.m_finalCriminalAmount;
	}

	public int GetCriminalCapacity()
	{
		return (int)this.m_productionData.m_finalCriminalCapacity;
	}

	public int GetExtraCriminals()
	{
		return (int)((this.m_productionData.m_averageCriminalExtra + 50u) / 100u);
	}

	public int GetElectricityConsumption()
	{
		return (int)(this.m_playerConsumption.m_finalElectricityConsumption * 16u + this.m_residentialConsumption.m_finalElectricityConsumption * 16u + this.m_commercialConsumption.m_finalElectricityConsumption * 16u + this.m_industrialConsumption.m_finalElectricityConsumption * 16u + this.m_officeConsumption.m_finalElectricityConsumption * 16u);
	}

	public int GetHeatingConsumption()
	{
		return (int)(this.m_playerConsumption.m_finalHeatingConsumption * 16u + this.m_residentialConsumption.m_finalHeatingConsumption * 16u + this.m_commercialConsumption.m_finalHeatingConsumption * 16u + this.m_industrialConsumption.m_finalHeatingConsumption * 16u + this.m_officeConsumption.m_finalHeatingConsumption * 16u);
	}

	public int GetWaterConsumption()
	{
		return (int)(this.m_playerConsumption.m_finalWaterConsumption * 16u + this.m_residentialConsumption.m_finalWaterConsumption * 16u + this.m_commercialConsumption.m_finalWaterConsumption * 16u + this.m_industrialConsumption.m_finalWaterConsumption * 16u + this.m_officeConsumption.m_finalWaterConsumption * 16u);
	}

	public int GetSewageAccumulation()
	{
		return (int)(this.m_playerConsumption.m_finalSewageAccumulation * 16u + this.m_residentialConsumption.m_finalSewageAccumulation * 16u + this.m_commercialConsumption.m_finalSewageAccumulation * 16u + this.m_industrialConsumption.m_finalSewageAccumulation * 16u + this.m_officeConsumption.m_finalSewageAccumulation * 16u);
	}

	public int GetGarbageAccumulation()
	{
		return (int)(this.m_playerConsumption.m_finalGarbageAccumulation * 16u + this.m_residentialConsumption.m_finalGarbageAccumulation * 16u + this.m_commercialConsumption.m_finalGarbageAccumulation * 16u + this.m_industrialConsumption.m_finalGarbageAccumulation * 16u + this.m_officeConsumption.m_finalGarbageAccumulation * 16u);
	}

	public int GetIncomeAccumulation()
	{
		return (int)(this.m_playerConsumption.m_finalIncomeAccumulation * 16u + this.m_residentialConsumption.m_finalIncomeAccumulation * 16u + this.m_commercialConsumption.m_finalIncomeAccumulation * 16u + this.m_industrialConsumption.m_finalIncomeAccumulation * 16u + this.m_officeConsumption.m_finalIncomeAccumulation * 16u);
	}

	public int GetGarbagePiles()
	{
		int num = (int)((ushort)this.m_playerConsumption.m_finalGarbagePiles * this.m_playerConsumption.m_finalBuildingCount + (ushort)this.m_residentialConsumption.m_finalGarbagePiles * this.m_residentialConsumption.m_finalBuildingCount + (ushort)this.m_commercialConsumption.m_finalGarbagePiles * this.m_commercialConsumption.m_finalBuildingCount + (ushort)this.m_industrialConsumption.m_finalGarbagePiles * this.m_industrialConsumption.m_finalBuildingCount + (ushort)this.m_officeConsumption.m_finalGarbagePiles * this.m_officeConsumption.m_finalBuildingCount);
		int num2 = (int)(this.m_playerConsumption.m_finalBuildingCount + this.m_residentialConsumption.m_finalBuildingCount + this.m_commercialConsumption.m_finalBuildingCount + this.m_industrialConsumption.m_finalBuildingCount + this.m_officeConsumption.m_finalBuildingCount);
		if (num2 != 0)
		{
			return (num + (num2 >> 1)) / num2;
		}
		return 0;
	}

	public int GetWaterPollution()
	{
		int num = (int)((ushort)this.m_playerConsumption.m_finalWaterPollution * this.m_playerConsumption.m_finalBuildingCount + (ushort)this.m_residentialConsumption.m_finalWaterPollution * this.m_residentialConsumption.m_finalBuildingCount + (ushort)this.m_commercialConsumption.m_finalWaterPollution * this.m_commercialConsumption.m_finalBuildingCount + (ushort)this.m_industrialConsumption.m_finalWaterPollution * this.m_industrialConsumption.m_finalBuildingCount + (ushort)this.m_officeConsumption.m_finalWaterPollution * this.m_officeConsumption.m_finalBuildingCount);
		int num2 = (int)(this.m_playerConsumption.m_finalBuildingCount + this.m_residentialConsumption.m_finalBuildingCount + this.m_commercialConsumption.m_finalBuildingCount + this.m_industrialConsumption.m_finalBuildingCount + this.m_officeConsumption.m_finalBuildingCount);
		if (num2 != 0)
		{
			return (num + (num2 >> 1)) / num2;
		}
		return 0;
	}

	public int GetDeadCount()
	{
		return (int)(this.m_playerConsumption.m_finalDeadCount + this.m_residentialConsumption.m_finalDeadCount + this.m_commercialConsumption.m_finalDeadCount + this.m_industrialConsumption.m_finalDeadCount + this.m_officeConsumption.m_finalDeadCount);
	}

	public int GetSickCount()
	{
		return (int)this.m_residentialConsumption.m_finalSickCount;
	}

	public int GetExportAmount()
	{
		return (int)(this.m_playerConsumption.m_finalExportAmount + this.m_residentialConsumption.m_finalExportAmount + this.m_commercialConsumption.m_finalExportAmount + this.m_industrialConsumption.m_finalExportAmount + this.m_officeConsumption.m_finalExportAmount);
	}

	public int GetImportAmount()
	{
		return (int)(this.m_playerConsumption.m_finalImportAmount + this.m_residentialConsumption.m_finalImportAmount + this.m_commercialConsumption.m_finalImportAmount + this.m_industrialConsumption.m_finalImportAmount + this.m_officeConsumption.m_finalImportAmount);
	}

	public int GetLandValue()
	{
		return (int)this.m_groundData.m_finalLandvalue;
	}

	public int GetGroundPollution()
	{
		return (int)(this.m_groundData.m_finalPollution * 100 / 255);
	}

	public int GetEducation1Need()
	{
		return (int)this.m_childData.m_finalCount;
	}

	public int GetEducation2Need()
	{
		return (int)this.m_teenData.m_finalCount;
	}

	public int GetEducation3Need()
	{
		return (int)(this.m_youngData.m_finalCount / 3u);
	}

	public int GetWorkerCount()
	{
		return (int)(this.m_commercialData.m_finalAliveCount + this.m_industrialData.m_finalAliveCount + this.m_officeData.m_finalAliveCount + this.m_playerData.m_finalAliveCount);
	}

	public int GetWorkplaceCount()
	{
		return (int)(this.m_commercialData.m_finalHomeOrWorkCount + this.m_industrialData.m_finalHomeOrWorkCount + this.m_officeData.m_finalHomeOrWorkCount + this.m_playerData.m_finalHomeOrWorkCount);
	}

	public int GetUnemployment()
	{
		int num = (int)(this.m_youngData.m_finalCount + this.m_adultData.m_finalCount);
		int num2 = (int)(this.m_educated0Data.m_finalUnemployed + this.m_educated1Data.m_finalUnemployed + this.m_educated2Data.m_finalUnemployed + this.m_educated3Data.m_finalUnemployed);
		return Mathf.Min((num2 * 100 + (num >> 1)) / Mathf.Max(1, num), 100);
	}

	public int GetEducation1Rate()
	{
		int num = (int)(this.m_teenData.m_finalCount + this.m_youngData.m_finalCount + this.m_adultData.m_finalCount + this.m_seniorData.m_finalCount);
		if (num != 0)
		{
			return Mathf.Min(100, (int)((this.m_education1Data.m_finalCount * 100u + (uint)(num >> 1)) / (uint)num));
		}
		return 0;
	}

	public int GetEducation2Rate()
	{
		int num = (int)(this.m_youngData.m_finalCount + this.m_adultData.m_finalCount + this.m_seniorData.m_finalCount);
		if (num != 0)
		{
			return Mathf.Min(100, (int)((this.m_education2Data.m_finalCount * 100u + (uint)(num >> 1)) / (uint)num));
		}
		return 0;
	}

	public int GetEducation3Rate()
	{
		int num = (int)(this.m_youngData.m_finalCount * 2u / 3u + this.m_adultData.m_finalCount + this.m_seniorData.m_finalCount);
		if (num != 0)
		{
			return Mathf.Min(100, (int)((this.m_education3Data.m_finalCount * 100u + (uint)(num >> 1)) / (uint)num));
		}
		return 0;
	}

	public int GetWaterStorageAmount()
	{
		return (int)this.m_productionData.m_finalWaterStorageAmount;
	}

	public int GetWaterStorageCapacity()
	{
		return (int)this.m_productionData.m_finalWaterStorageCapacity;
	}

	public int GetShelterCitizenNumber()
	{
		return (int)this.m_productionData.m_finalShelterCitizenNumber;
	}

	public int GetShelterCitizenCapacity()
	{
		return (int)this.m_productionData.m_finalShelterCitizenCapacity;
	}

	public int CalculateResidentialDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level1, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level1)), this.m_residentialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level1, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level1)), this.m_residentialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level2, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level2)), this.m_residentialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level2, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level2)), this.m_residentialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level3, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level3)), this.m_residentialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level3, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level3)), this.m_residentialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level4, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level4)), this.m_residentialData.m_finalLevel4, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level4, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level4)), this.m_residentialData.m_finalLevel4, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level5, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level5)), this.m_residentialData.m_finalLevel5, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level5, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level5)), this.m_residentialData.m_finalLevel5, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateResidentialLowDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level1, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level1)), this.m_residentialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level2, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level2)), this.m_residentialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level3, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level3)), this.m_residentialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level4, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level4)), this.m_residentialData.m_finalLevel4, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.Level5, (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(ItemClass.Level.Level5)), this.m_residentialData.m_finalLevel5, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateResidentialHighDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level1, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level1)), this.m_residentialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level2, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level2)), this.m_residentialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level3, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level3)), this.m_residentialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level4, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level4)), this.m_residentialData.m_finalLevel4, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level5, (int)((Citizen.Wealth)12 - Citizen.GetWealthLevel(ItemClass.Level.Level5)), this.m_residentialData.m_finalLevel5, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateCommercialDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level1, 11, this.m_commercialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level1, 12, this.m_commercialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level2, 10, this.m_commercialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level2, 11, this.m_commercialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level3, 9, this.m_commercialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level3, 10, this.m_commercialData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateCommercialLowDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level1, 11, this.m_commercialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level2, 10, this.m_commercialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level3, 9, this.m_commercialData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateCommercialHighDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level1, 12, this.m_commercialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level2, 11, this.m_commercialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level3, 10, this.m_commercialData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateWorkplaceDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level1, 11, this.m_industrialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level2, 10, this.m_industrialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level3, 9, this.m_industrialData.m_finalLevel3, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level1, 11, this.m_officeData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level2, 10, this.m_officeData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level3, 9, this.m_officeData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateIndustrialDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level1, 11, this.m_industrialData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level2, 10, this.m_industrialData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level3, 9, this.m_industrialData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	public int CalculateOfficeDemandOffset()
	{
		int num = 0;
		int num2 = 0;
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level1, 11, this.m_officeData.m_finalLevel1, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level2, 10, this.m_officeData.m_finalLevel2, ref num, ref num2);
		this.AddTaxEffect(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level3, 9, this.m_officeData.m_finalLevel3, ref num, ref num2);
		if (num2 != 0)
		{
			num = (num * 5 + (num2 >> 1)) / num2;
		}
		return num;
	}

	private void AddTaxEffect(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, int limit, byte count, ref int result, ref int divider)
	{
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(service, subService, level, this.m_taxationPolicies);
		if (taxRate > limit)
		{
			result -= (taxRate - limit) * (int)count;
		}
		divider += (int)count;
	}

	public Vector3 m_nameLocation;

	public Vector2 m_nameSize;

	public ulong m_randomSeed;

	public uint m_totalAlpha;

	public District.Flags m_flags;

	public DistrictPolicies.Specialization m_specializationPolicies;

	public DistrictPolicies.Specialization m_specializationPoliciesEffect;

	public DistrictPolicies.Services m_servicePolicies;

	public DistrictPolicies.Services m_servicePoliciesEffect;

	public DistrictPolicies.Taxation m_taxationPolicies;

	public DistrictPolicies.Taxation m_taxationPoliciesEffect;

	public DistrictPolicies.CityPlanning m_cityPlanningPolicies;

	public DistrictPolicies.CityPlanning m_cityPlanningPoliciesEffect;

	public DistrictPolicies.Special m_specialPolicies;

	public DistrictPolicies.Special m_specialPoliciesEffect;

	public DistrictPolicies.Event m_eventPolicies;

	public DistrictPolicies.Event m_eventPoliciesEffect;

	public DistrictPopulationData m_populationData;

	public DistrictProductionData m_productionData;

	public DistrictGroundData m_groundData;

	public DistrictAgeData m_childData;

	public DistrictAgeData m_teenData;

	public DistrictAgeData m_youngData;

	public DistrictAgeData m_adultData;

	public DistrictAgeData m_seniorData;

	public DistrictAgeData m_birthData;

	public DistrictAgeData m_deathData;

	public DistrictEducationData m_educated0Data;

	public DistrictEducationData m_educated1Data;

	public DistrictEducationData m_educated2Data;

	public DistrictEducationData m_educated3Data;

	public DistrictAgeData m_education1Data;

	public DistrictAgeData m_education2Data;

	public DistrictAgeData m_education3Data;

	public DistrictAgeData m_student1Data;

	public DistrictAgeData m_student2Data;

	public DistrictAgeData m_student3Data;

	public DistrictTouristData m_tourist1Data;

	public DistrictTouristData m_tourist2Data;

	public DistrictTouristData m_tourist3Data;

	public DistrictPrivateData m_residentialData;

	public DistrictPrivateData m_commercialData;

	public DistrictPrivateData m_visitorData;

	public DistrictPrivateData m_industrialData;

	public DistrictPrivateData m_officeData;

	public DistrictPrivateData m_playerData;

	public DistrictSpecializationData m_farmingData;

	public DistrictSpecializationData m_forestryData;

	public DistrictSpecializationData m_oreData;

	public DistrictSpecializationData m_oilData;

	public DistrictSpecializationData m_leisureData;

	public DistrictSpecializationData m_touristData;

	public DistrictSpecializationData m_organicData;

	public DistrictSpecializationData m_selfsufficientData;

	public DistrictSpecializationData m_hightechData;

	public DistrictConsumptionData m_playerConsumption;

	public DistrictConsumptionData m_residentialConsumption;

	public DistrictConsumptionData m_commercialConsumption;

	public DistrictConsumptionData m_industrialConsumption;

	public DistrictConsumptionData m_officeConsumption;

	public DistrictUsageData m_usageData;

	public DistrictResourceData m_importData;

	public DistrictResourceData m_exportData;

	public DistrictSubCultureData m_hippieData;

	public DistrictSubCultureData m_hipsterData;

	public DistrictSubCultureData m_redneckData;

	public DistrictSubCultureData m_gangstaData;

	public byte m_finalHappiness;

	public byte m_finalCrimeRate;

	public ushort m_Style;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		CustomName = 4,
		All = -1
	}
}
