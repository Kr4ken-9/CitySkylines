﻿using System;
using UnityEngine;

public class TextureChannelCombiner : ScriptableObject
{
	public Texture2D m_channelTarget;

	public Texture2D m_normalTarget;
}
