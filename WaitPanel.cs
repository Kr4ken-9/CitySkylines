﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class WaitPanel : MenuPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.m_Binding = base.GetComponent<BindPropertyByKey>();
	}

	public static void ShowModal(string id)
	{
		WaitPanel.ShowModal(Locale.Get(id, "Title"), Locale.Get(id, "Message"));
	}

	public static void ShowModal(string title, string message)
	{
		WaitPanel waitPanel = UIView.get_library().ShowModal<WaitPanel>(typeof(WaitPanel).Name, delegate(UIComponent c, int r)
		{
		});
		waitPanel.SetMessage(title, message);
	}

	public static void Hide()
	{
		UIView.get_library().Hide(typeof(WaitPanel).Name, 0);
	}

	public virtual void SetMessage(string title, string message)
	{
		if (this.m_Binding != null)
		{
			this.m_Binding.SetProperties(TooltipHelper.Format(new string[]
			{
				"title",
				title,
				"message",
				message
			}));
		}
	}

	private BindPropertyByKey m_Binding;
}
