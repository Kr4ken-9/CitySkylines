﻿using System;
using ColossalFramework.Globalization;

public class ZoneTypeGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, ItemClass.Zone zone)
	{
		if (base.CanActivate(guideInfo) && zone != ItemClass.Zone.None && zone != ItemClass.Zone.Unzoned && zone != ItemClass.Zone.Distant)
		{
			GuideTriggerBase.ActivationInfo activationInfo = new ZoneTypeGuide.ZoneTypeActivationInfo(guideInfo, zone);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class ZoneTypeActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public ZoneTypeActivationInfo(GuideInfo guideInfo, ItemClass.Zone zone) : base(guideInfo)
		{
			this.m_zone = zone;
		}

		public override string GetName()
		{
			string name = this.m_guideInfo.m_name;
			string arg = this.m_zone.ToString();
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			string icon = this.m_guideInfo.m_icon;
			string arg = this.m_zone.ToString();
			return StringUtils.SafeFormat(icon, arg);
		}

		public override string FormatText(string pattern)
		{
			string text = this.m_zone.ToString();
			string arg = Locale.Get("ZONING_TITLE", text);
			return StringUtils.SafeFormat(pattern, arg);
		}

		public override IUITag GetTag()
		{
			string text = this.m_guideInfo.m_tag;
			string arg = this.m_zone.ToString();
			text = StringUtils.SafeFormat(text, arg);
			return MonoTutorialTag.Find(text);
		}

		protected ItemClass.Zone m_zone;
	}
}
