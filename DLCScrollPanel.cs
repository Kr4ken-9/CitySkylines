﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class DLCScrollPanel : UICustomControl
{
	private int currentScrollStep
	{
		get
		{
			return this.m_targetStep;
		}
		set
		{
			this.m_targetStep = Mathf.Clamp(value, 0, this.m_scrollSteps - 1);
			int num = Mathf.Abs(this.currentActualScrollStep - value);
			int num2 = Mathf.Clamp(value, 0, this.m_scrollSteps - 1);
			float x = this.m_scrollPanel.get_scrollPosition().x;
			float num3 = (float)num2 * this.m_scrollStepDistance;
			float num4 = (!this.m_canAutoScroll) ? (this.m_manualScrollInterpSpeed * (float)num) : (this.m_autoScrollInterpSpeed * (float)num);
			if (ValueAnimator.IsAnimating(this.m_animationName))
			{
				ValueAnimator.Cancel(this.m_animationName);
			}
			ValueAnimator.Animate(this.m_animationName, delegate(float val)
			{
				this.m_scrollPanel.set_scrollPosition(new Vector2(val, 0f));
				this.RefreshArrows();
			}, new AnimatedFloat(x, num3, num4));
		}
	}

	private int currentActualScrollStep
	{
		get
		{
			float num = this.m_scrollPanel.get_scrollPosition().x;
			num /= this.m_scrollStepDistance;
			return Mathf.Clamp(Mathf.RoundToInt(num), 0, this.m_scrollSteps - 1);
		}
	}

	private void RefreshArrows()
	{
		int currentActualScrollStep = this.currentActualScrollStep;
		this.m_previous.set_isEnabled(currentActualScrollStep > 0);
		this.m_next.set_isEnabled(currentActualScrollStep < this.m_scrollSteps - 1);
	}

	private void Awake()
	{
		this.m_next.add_eventClick(new MouseEventHandler(this.OnNext));
		this.m_previous.add_eventClick(new MouseEventHandler(this.OnPrev));
		this.m_scrollPanel.add_eventScrollPositionChanged(new PropertyChangedEventHandler<Vector2>(this.OnScrolled));
		this.m_scrollPanel.add_eventMouseWheel(new MouseEventHandler(this.OnMouseWheeled));
		this.m_autoScrollBlockingPanel.add_eventMouseEnter(new MouseEventHandler(this.OnMouseEntered));
		this.m_autoScrollBlockingPanel.add_eventMouseLeave(new MouseEventHandler(this.OnMouseLeft));
	}

	private void OnScrolled(UIComponent comp, Vector2 v)
	{
		this.RefreshArrows();
	}

	private void Start()
	{
		this.RefreshArrowAvailability();
		int num = 0;
		foreach (UIComponent current in this.m_scrollPanel.get_components())
		{
			if (current.get_isVisible())
			{
				num++;
			}
		}
		this.m_scrollSteps = num - this.m_visibleItems + 1;
		if (this.m_scrollSteps > 1)
		{
			this.m_scrollStepDistance = (this.m_scrollPanel.CalculateViewSize().x - this.m_scrollPanel.get_width()) / (float)(this.m_scrollSteps - 1);
		}
		else
		{
			this.m_scrollStepDistance = 0f;
		}
		this.RefreshArrows();
		base.InvokeRepeating("AutoScroll", this.m_autoScrollInterval, this.m_autoScrollInterval);
	}

	private void RefreshArrowAvailability()
	{
		this.m_arrowsAvailable = (this.m_scrollPanel.CalculateViewSize().x > this.m_scrollPanel.get_width() + 6f);
		this.m_next.set_isVisible(this.m_arrowsAvailable);
		this.m_previous.set_isVisible(this.m_arrowsAvailable);
	}

	private void OnNext(UIComponent comp, UIMouseEventParameter p)
	{
		this.currentScrollStep++;
	}

	private void OnPrev(UIComponent comp, UIMouseEventParameter p)
	{
		this.currentScrollStep--;
	}

	private void OnEnable()
	{
		this.EnableAutoscrolling();
	}

	private void OnDisable()
	{
		this.DisableAutoscrolling();
	}

	private void OnMouseEntered(UIComponent comp, UIMouseEventParameter p)
	{
		this.DisableAutoscrolling();
	}

	private void OnMouseLeft(UIComponent comp, UIMouseEventParameter p)
	{
		this.EnableAutoscrolling();
	}

	private void EnableAutoscrolling()
	{
		this.m_canAutoScroll = true;
	}

	private void DisableAutoscrolling()
	{
		this.m_canAutoScroll = false;
	}

	public void OnMouseWheeled(UIComponent c, UIMouseEventParameter p)
	{
		this.m_accumulatedMouseWheelDelta += p.get_wheelDelta();
		while (this.m_accumulatedMouseWheelDelta <= -1f)
		{
			this.currentScrollStep++;
			this.m_accumulatedMouseWheelDelta += 1f;
		}
		while (this.m_accumulatedMouseWheelDelta >= 1f)
		{
			this.currentScrollStep--;
			this.m_accumulatedMouseWheelDelta -= 1f;
		}
	}

	private void AutoScroll()
	{
		if (this.m_arrowsAvailable && this.m_canAutoScroll)
		{
			if (this.currentScrollStep >= this.m_scrollSteps - 1)
			{
				this.currentScrollStep = 0;
			}
			else
			{
				this.currentScrollStep++;
			}
		}
	}

	public UIScrollablePanel m_scrollPanel;

	public UIPanel m_autoScrollBlockingPanel;

	public UIButton m_next;

	public UIButton m_previous;

	private readonly float m_autoScrollInterpSpeed = 0.4f;

	private readonly float m_manualScrollInterpSpeed = 0.1f;

	public int m_visibleItems;

	private int m_scrollSteps;

	private float m_scrollStepDistance;

	public float m_autoScrollInterval = 5f;

	private bool m_arrowsAvailable;

	private bool m_canAutoScroll;

	private float m_accumulatedMouseWheelDelta;

	private int m_targetStep;

	public string m_animationName = "ScrollStep";
}
