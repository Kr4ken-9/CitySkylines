﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class WindInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("StrengthGradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[15].m_negativeColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[15].m_targetColor);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[15].m_activeColor);
					uITextureSprite2.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					uITextureSprite2.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					uITextureSprite2.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.m_initialized = true;
		}
	}

	private bool m_initialized;
}
