﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

[ExecuteInEditMode]
public class OverlayEffect : MonoBehaviour
{
	private void OnEnable()
	{
		this.CreateBoxMesh();
		this.m_undergroundView = base.GetComponentInChildren<UndergroundView>();
		this.m_overlayMaterial = new Material(this.m_overlayShader);
		this.m_overlayMaterial.set_hideFlags(52);
		this.m_shapeMaterial = new Material(this.m_shapeShader);
		this.m_shapeMaterial.set_hideFlags(52);
		this.m_shapeMaterialBlend = new Material(this.m_shapeShaderBlend);
		this.m_shapeMaterialBlend.set_hideFlags(52);
		this.ID_UndergroundTexture = Shader.PropertyToID("_UndergroundTexture");
		this.ID_UndergroundUVScale = Shader.PropertyToID("_UndergroundUVScale");
		this.ID_OverlayTexture = Shader.PropertyToID("_OverlayTexture");
		this.ID_CornersAC = Shader.PropertyToID("_CornersAC");
		this.ID_CornersBD = Shader.PropertyToID("_CornersBD");
		this.ID_CenterPos = Shader.PropertyToID("_CenterPos");
		this.ID_Start01 = Shader.PropertyToID("_Start01");
		this.ID_End01 = Shader.PropertyToID("_End01");
		this.ID_PointX = Shader.PropertyToID("_PointX");
		this.ID_PointZ = Shader.PropertyToID("_PointZ");
		this.ID_CutStart = Shader.PropertyToID("_CutStart");
		this.ID_CutEnd = Shader.PropertyToID("_CutEnd");
		this.ID_Size = Shader.PropertyToID("_Size");
		this.ID_LimitsY = Shader.PropertyToID("_LimitsY");
		this.ID_DarkenedFrame = Shader.PropertyToID("_DarkenedFrame");
		this.ID_FlashIntensity = Shader.PropertyToID("_FlashIntensity");
	}

	private void OnDisable()
	{
		if (this.m_overlayMaterial != null)
		{
			Object.DestroyImmediate(this.m_overlayMaterial);
			this.m_overlayMaterial = null;
		}
		if (this.m_shapeMaterial != null)
		{
			Object.DestroyImmediate(this.m_shapeMaterial);
			this.m_shapeMaterial = null;
		}
		if (this.m_shapeMaterialBlend != null)
		{
			Object.DestroyImmediate(this.m_shapeMaterialBlend);
			this.m_shapeMaterialBlend = null;
		}
		if (this.m_boxMesh != null)
		{
			Object.DestroyImmediate(this.m_boxMesh);
			this.m_boxMesh = null;
		}
		if (this.m_overlayRGBA != null)
		{
			RenderTexture.ReleaseTemporary(this.m_overlayRGBA);
			this.m_overlayRGBA = null;
		}
		this.m_undergroundView = null;
	}

	private void OnPostRender()
	{
		if (this.m_overlayRGBA != null)
		{
			RenderTexture.ReleaseTemporary(this.m_overlayRGBA);
			this.m_overlayRGBA = null;
		}
		int width = RenderTexture.get_active().get_width();
		int height = RenderTexture.get_active().get_height();
		this.m_overlayRGBA = RenderTexture.GetTemporary(width, height, 0, 2, 1);
		Graphics.SetRenderTarget(this.m_overlayRGBA.get_colorBuffer(), Graphics.get_activeDepthBuffer());
		GL.Clear(false, true, new Color(0f, 0f, 0f, 0f));
		if (Application.get_isPlaying() && Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			try
			{
				RenderManager.Managers_RenderOverlay(Singleton<RenderManager>.get_instance().CurrentCameraInfo);
			}
			finally
			{
			}
		}
		Graphics.SetRenderTarget(null);
	}

	private Vector2 GetFrame(int width, int height)
	{
		float num = (float)width / (float)height / Camera.get_main().get_aspect();
		if (num >= 1f)
		{
			return new Vector2(0f, 0.5f * (1f - 1f / num));
		}
		return new Vector2(0.5f * (1f - num), 0f);
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Texture texture;
		if (this.m_undergroundView.BeginUsingTexture(out texture))
		{
			this.m_overlayMaterial.SetTexture(this.ID_UndergroundTexture, texture);
			Vector4 one = Vector4.get_one();
			one.x = (float)source.get_width() / (float)texture.get_width();
			one.y = (float)source.get_height() / (float)texture.get_height();
			this.m_overlayMaterial.SetVector(this.ID_UndergroundUVScale, one);
			this.m_overlayMaterial.EnableKeyword("UNDERGROUND_ON");
		}
		else
		{
			this.m_overlayMaterial.SetTexture(this.ID_UndergroundTexture, null);
			this.m_overlayMaterial.DisableKeyword("UNDERGROUND_ON");
		}
		if (Singleton<GameAreaManager>.get_exists() && Singleton<GameAreaManager>.get_instance().ThumbnailMode)
		{
			this.m_overlayMaterial.DisableKeyword("INFOSHOT_PREVIEW");
			if (this.m_thumbnailRendering)
			{
				this.m_overlayMaterial.EnableKeyword("THUMBSHOT_ON");
				this.m_overlayMaterial.DisableKeyword("THUMBSHOT_PREVIEW");
			}
			else
			{
				this.m_overlayMaterial.EnableKeyword("THUMBSHOT_PREVIEW");
				this.m_overlayMaterial.DisableKeyword("THUMBSHOT_ON");
				this.m_overlayMaterial.SetVector(this.ID_DarkenedFrame, this.GetFrame(AssetImporterThumbnails.thumbWidth, AssetImporterThumbnails.thumbHeight));
			}
		}
		else
		{
			this.m_overlayMaterial.DisableKeyword("THUMBSHOT_PREVIEW");
			this.m_overlayMaterial.DisableKeyword("THUMBSHOT_ON");
			if (this.m_tooltipMode)
			{
				if (this.m_thumbnailRendering)
				{
					this.m_overlayMaterial.DisableKeyword("INFOSHOT_PREVIEW");
					this.m_overlayMaterial.EnableKeyword("THUMBSHOT_ON");
				}
				else
				{
					this.m_overlayMaterial.EnableKeyword("INFOSHOT_PREVIEW");
					this.m_overlayMaterial.SetVector(this.ID_DarkenedFrame, this.GetFrame(SnapshotTool.tooltipWidth, SnapshotTool.tooltipHeight));
				}
			}
			else
			{
				this.m_overlayMaterial.DisableKeyword("INFOSHOT_PREVIEW");
			}
		}
		float num = (120f - (float)DateTime.Now.Subtract(this.m_lastScreenshotTimestamp).TotalMilliseconds) / 120f;
		num *= Mathf.Abs(num);
		this.m_overlayMaterial.SetFloat(this.ID_FlashIntensity, (num >= 1f) ? 0f : Mathf.Max(0f, num));
		this.m_overlayMaterial.SetTexture(this.ID_OverlayTexture, this.m_overlayRGBA);
		Graphics.Blit(source, destination, this.m_overlayMaterial);
		this.m_overlayMaterial.SetTexture(this.ID_UndergroundTexture, null);
		this.m_overlayMaterial.SetTexture(this.ID_OverlayTexture, null);
		this.m_undergroundView.EndUsingTexture();
		if (this.m_overlayRGBA != null)
		{
			RenderTexture.ReleaseTemporary(this.m_overlayRGBA);
			this.m_overlayRGBA = null;
		}
	}

	private void CreateBoxMesh()
	{
		Vector3[] array = new Vector3[8];
		int[] triangles = new int[36];
		int num = 0;
		int num2 = 0;
		array[num++] = new Vector3(-0.5f, -0.5f, -0.5f);
		array[num++] = new Vector3(0.5f, -0.5f, -0.5f);
		array[num++] = new Vector3(-0.5f, 0.5f, -0.5f);
		array[num++] = new Vector3(0.5f, 0.5f, -0.5f);
		array[num++] = new Vector3(-0.5f, -0.5f, 0.5f);
		array[num++] = new Vector3(0.5f, -0.5f, 0.5f);
		array[num++] = new Vector3(-0.5f, 0.5f, 0.5f);
		array[num++] = new Vector3(0.5f, 0.5f, 0.5f);
		OverlayEffect.CreateQuad(triangles, ref num2, 0, 2, 3, 1);
		OverlayEffect.CreateQuad(triangles, ref num2, 4, 5, 7, 6);
		OverlayEffect.CreateQuad(triangles, ref num2, 2, 6, 7, 3);
		OverlayEffect.CreateQuad(triangles, ref num2, 0, 1, 5, 4);
		OverlayEffect.CreateQuad(triangles, ref num2, 4, 6, 2, 0);
		OverlayEffect.CreateQuad(triangles, ref num2, 5, 1, 3, 7);
		this.m_boxMesh = new Mesh();
		this.m_boxMesh.set_hideFlags(52);
		this.m_boxMesh.set_vertices(array);
		this.m_boxMesh.set_triangles(triangles);
	}

	private static void CreateQuad(int[] triangles, ref int index, int a, int b, int c, int d)
	{
		triangles[index++] = a;
		triangles[index++] = b;
		triangles[index++] = d;
		triangles[index++] = d;
		triangles[index++] = b;
		triangles[index++] = c;
	}

	public void DrawEffect(RenderManager.CameraInfo cameraInfo, Material material, int pass, Bounds bounds)
	{
		if (bounds.Intersects(cameraInfo.m_nearBounds))
		{
			if (material.SetPass(pass))
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(cameraInfo.m_position + cameraInfo.m_forward * (cameraInfo.m_near + 1f), cameraInfo.m_rotation, new Vector3(100f, 100f, 1f));
				Graphics.DrawMeshNow(this.m_boxMesh, matrix4x);
			}
		}
		else if (material.SetPass(pass))
		{
			Matrix4x4 matrix4x2 = default(Matrix4x4);
			matrix4x2.SetTRS(bounds.get_center(), Quaternion.get_identity(), bounds.get_size());
			Graphics.DrawMeshNow(this.m_boxMesh, matrix4x2);
		}
	}

	public void DrawQuad(RenderManager.CameraInfo cameraInfo, Color color, Quad3 quad, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		Vector3 vector = quad.Min();
		Vector3 vector2 = quad.Max();
		float num = Vector2.Distance(cameraInfo.m_position, (vector + vector2) * 0.5f) * 0.001f + 1f;
		Vector4 vector3;
		vector3..ctor(quad.a.x, quad.a.z, quad.c.x, quad.c.z);
		Vector4 vector4;
		vector4..ctor(quad.b.x, quad.b.z, quad.d.x, quad.d.z);
		Vector4 vector5;
		if (renderLimits)
		{
			vector5..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector5..ctor(-100000f, minY, maxY, 100000f);
		}
		vector.y = Mathf.Min(vector.y, minY);
		vector2.y = Mathf.Max(vector2.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector6;
		vector6..ctor(num, num, num);
		bounds.SetMinMax(vector - vector6, vector2 + vector6);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_color(color.get_linear());
			material.SetVector(this.ID_CornersAC, vector3);
			material.SetVector(this.ID_CornersBD, vector4);
			material.SetVector(this.ID_LimitsY, vector5);
			this.DrawEffect(cameraInfo, material, 0, bounds);
		}
	}

	public void DrawQuad(RenderManager.CameraInfo cameraInfo, Texture2D texture, Color color, Quad3 quad, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		Vector3 vector = quad.Min();
		Vector3 vector2 = quad.Max();
		float num = Vector2.Distance(cameraInfo.m_position, (vector + vector2) * 0.5f) * 0.001f + 1f;
		Vector4 vector3;
		vector3..ctor(quad.a.x, quad.a.z, quad.c.x, quad.c.z);
		Vector4 vector4;
		vector4..ctor(quad.b.x, quad.b.z, quad.d.x, quad.d.z);
		Vector4 vector5;
		if (renderLimits)
		{
			vector5..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector5..ctor(-100000f, minY, maxY, 100000f);
		}
		vector.y = Mathf.Min(vector.y, minY);
		vector2.y = Mathf.Max(vector2.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector6;
		vector6..ctor(num, num, num);
		bounds.SetMinMax(vector - vector6, vector2 + vector6);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_mainTexture(texture);
			material.set_color(color.get_linear());
			material.SetVector(this.ID_CornersAC, vector3);
			material.SetVector(this.ID_CornersBD, vector4);
			material.SetVector(this.ID_LimitsY, vector5);
			this.DrawEffect(cameraInfo, material, 4, bounds);
		}
	}

	public void DrawCircle(RenderManager.CameraInfo cameraInfo, Color color, Vector3 center, float size, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		float num = Vector2.Distance(cameraInfo.m_position, center) * 0.001f + 1f;
		Vector4 vector;
		vector..ctor(center.x, center.z, size * -0.5f, size * 0.5f);
		Vector4 vector2;
		if (renderLimits)
		{
			vector2..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector2..ctor(-100000f, minY, maxY, 100000f);
		}
		Vector3 vector3 = center - new Vector3(size * 0.5f, 0f, size * 0.5f);
		Vector3 vector4 = center + new Vector3(size * 0.5f, 0f, size * 0.5f);
		vector3.y = Mathf.Min(vector3.y, minY);
		vector4.y = Mathf.Max(vector4.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector5;
		vector5..ctor(num, num, num);
		bounds.SetMinMax(vector3 - vector5, vector4 + vector5);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_color(color.get_linear());
			material.SetVector(this.ID_CenterPos, vector);
			material.SetVector(this.ID_LimitsY, vector2);
			this.DrawEffect(cameraInfo, material, 1, bounds);
		}
	}

	public void DrawSegment(RenderManager.CameraInfo cameraInfo, Color color, Segment3 segment, float size, float dashLen, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		Vector3 vector = segment.Min() - new Vector3(size * 0.5f, 0f, size * 0.5f);
		Vector3 vector2 = segment.Max() + new Vector3(size * 0.5f, 0f, size * 0.5f);
		float num = Vector2.Distance(cameraInfo.m_position, (vector + vector2) * 0.5f) * 0.001f + 1f;
		Vector4 vector3;
		vector3..ctor(segment.a.x, segment.a.z, -100000f, -100000f);
		Vector4 vector4;
		vector4..ctor(segment.b.x, segment.b.z, -100000f, -100000f);
		Vector4 vector5;
		vector5..ctor(size * 0.5f, dashLen, (dashLen == 0f) ? 0f : 1f, 0f);
		Vector4 vector6;
		if (renderLimits)
		{
			vector6..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector6..ctor(-100000f, minY, maxY, 100000f);
		}
		vector.y = Mathf.Min(vector.y, minY);
		vector2.y = Mathf.Max(vector2.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector7;
		vector7..ctor(num, num, num);
		bounds.SetMinMax(vector - vector7, vector2 + vector7);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_color(color.get_linear());
			material.SetVector(this.ID_Start01, vector3);
			material.SetVector(this.ID_End01, vector4);
			material.SetVector(this.ID_Size, vector5);
			material.SetVector(this.ID_LimitsY, vector6);
			this.DrawEffect(cameraInfo, material, 2, bounds);
		}
	}

	public void DrawSegment(RenderManager.CameraInfo cameraInfo, Color color, Segment3 segment1, Segment3 segment2, float size, float dashLen, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		Vector3 vector = Vector3.Min(segment1.Min(), segment2.Min()) - new Vector3(size * 0.5f, 0f, size * 0.5f);
		Vector3 vector2 = Vector3.Max(segment1.Max(), segment2.Max()) + new Vector3(size * 0.5f, 0f, size * 0.5f);
		float num = Vector2.Distance(cameraInfo.m_position, (vector + vector2) * 0.5f) * 0.001f + 1f;
		Vector4 vector3;
		vector3..ctor(segment1.a.x, segment1.a.z, segment2.a.x, segment2.a.z);
		Vector4 vector4;
		vector4..ctor(segment1.b.x, segment1.b.z, segment2.b.x, segment2.b.z);
		Vector4 vector5;
		vector5..ctor(size * 0.5f, dashLen, (dashLen == 0f) ? 0f : 1f, 0f);
		Vector4 vector6;
		if (renderLimits)
		{
			vector6..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector6..ctor(-100000f, minY, maxY, 100000f);
		}
		vector.y = Mathf.Min(vector.y, minY);
		vector2.y = Mathf.Max(vector2.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector7;
		vector7..ctor(num, num, num);
		bounds.SetMinMax(vector - vector7, vector2 + vector7);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_color(color.get_linear());
			material.SetVector(this.ID_Start01, vector3);
			material.SetVector(this.ID_End01, vector4);
			material.SetVector(this.ID_Size, vector5);
			material.SetVector(this.ID_LimitsY, vector6);
			this.DrawEffect(cameraInfo, material, 2, bounds);
		}
	}

	public void DrawBezier(RenderManager.CameraInfo cameraInfo, Color color, Bezier3 bezier, float size, float cutStart, float cutEnd, float minY, float maxY, bool renderLimits, bool alphaBlend)
	{
		Vector3 vector = bezier.Min() - new Vector3(size * 0.5f, 0f, size * 0.5f);
		Vector3 vector2 = bezier.Max() + new Vector3(size * 0.5f, 0f, size * 0.5f);
		float num = Vector2.Distance(cameraInfo.m_position, (vector + vector2) * 0.5f) * 0.001f + 1f;
		Segment3 segment;
		segment..ctor(bezier.a, bezier.a);
		float num2;
		Vector3 vector3 = VectorUtils.NormalizeXZ(bezier.b - bezier.a, ref num2);
		if (num2 > 0.1f)
		{
			segment.a.x = segment.a.x + (vector3.x * (cutStart - size) + vector3.z * size * 0.5f);
			segment.a.z = segment.a.z + (vector3.z * (cutStart - size) - vector3.x * size * 0.5f);
			segment.b.x = segment.b.x + (vector3.x * (cutStart - size) - vector3.z * size * 0.5f);
			segment.b.z = segment.b.z + (vector3.z * (cutStart - size) + vector3.x * size * 0.5f);
		}
		else
		{
			segment..ctor(new Vector3(-100000f, -100000f, -100000f), new Vector3(-100000f, -100000f, -100000f));
		}
		Segment3 segment2;
		segment2..ctor(bezier.d, bezier.d);
		Vector3 vector4 = VectorUtils.NormalizeXZ(bezier.c - bezier.d, ref num2);
		if (num2 > 0.1f)
		{
			segment2.a.x = segment2.a.x + (vector4.x * (cutEnd - size) + vector4.z * size * 0.5f);
			segment2.a.z = segment2.a.z + (vector4.z * (cutEnd - size) - vector4.x * size * 0.5f);
			segment2.b.x = segment2.b.x + (vector4.x * (cutEnd - size) - vector4.z * size * 0.5f);
			segment2.b.z = segment2.b.z + (vector4.z * (cutEnd - size) + vector4.x * size * 0.5f);
		}
		else
		{
			segment2..ctor(new Vector3(-100000f, -100000f, -100000f), new Vector3(-100000f, -100000f, -100000f));
		}
		Vector4 vector5;
		vector5..ctor(bezier.a.x, bezier.b.x, bezier.c.x, bezier.d.x);
		Vector4 vector6;
		vector6..ctor(bezier.a.z, bezier.b.z, bezier.c.z, bezier.d.z);
		Vector4 vector7;
		vector7..ctor(segment.a.x, segment.a.z, segment.b.x, segment.b.z);
		Vector4 vector8;
		vector8..ctor(segment2.a.x, segment2.a.z, segment2.b.x, segment2.b.z);
		Vector4 vector9;
		if (renderLimits)
		{
			vector9..ctor(minY, -100000f, 100000f, maxY);
		}
		else
		{
			vector9..ctor(-100000f, minY, maxY, 100000f);
		}
		vector.y = Mathf.Min(vector.y, minY);
		vector2.y = Mathf.Max(vector2.y, maxY);
		Bounds bounds = default(Bounds);
		Vector3 vector10;
		vector10..ctor(num, num, num);
		bounds.SetMinMax(vector - vector10, vector2 + vector10);
		if (bounds.Intersects(cameraInfo.m_bounds))
		{
			Material material = (!alphaBlend) ? this.m_shapeMaterial : this.m_shapeMaterialBlend;
			material.set_color(color.get_linear());
			material.SetVector(this.ID_PointX, vector5);
			material.SetVector(this.ID_PointZ, vector6);
			material.SetVector(this.ID_CutStart, vector7);
			material.SetVector(this.ID_CutEnd, vector8);
			material.SetFloat(this.ID_Size, size * 0.5f);
			material.SetVector(this.ID_LimitsY, vector9);
			this.DrawEffect(cameraInfo, material, 3, bounds);
		}
	}

	public ToolController m_toolController;

	public Shader m_overlayShader;

	public Shader m_shapeShader;

	public Shader m_shapeShaderBlend;

	public bool m_thumbnailRendering;

	public bool m_tooltipMode;

	public DateTime m_lastScreenshotTimestamp;

	private UndergroundView m_undergroundView;

	private Material m_overlayMaterial;

	private Material m_shapeMaterial;

	private Material m_shapeMaterialBlend;

	private RenderTexture m_overlayRGBA;

	private Mesh m_boxMesh;

	private int ID_UndergroundTexture;

	private int ID_UndergroundUVScale;

	private int ID_OverlayTexture;

	private int ID_CornersAC;

	private int ID_CornersBD;

	private int ID_CenterPos;

	private int ID_Start01;

	private int ID_End01;

	private int ID_PointX;

	private int ID_PointZ;

	private int ID_CutStart;

	private int ID_CutEnd;

	private int ID_Size;

	private int ID_LimitsY;

	private int ID_DarkenedFrame;

	private int ID_FlashIntensity;
}
