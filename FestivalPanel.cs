﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class FestivalPanel : EventBuildingWorldInfoPanel
{
	protected override string problemIconTemplate
	{
		get
		{
			return FestivalPanel.kProblemIconTemplate;
		}
	}

	public float securityBudget
	{
		get
		{
			return this.m_securityBudget / 100f;
		}
		set
		{
			this.m_securityBudget = value * 100f;
			if (this.m_securityBudgetLabel != null)
			{
				this.m_securityBudgetLabel.set_text(StringUtils.SafeFormat(value.ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()), new object[0]));
				this.m_securityBudgetLabel.set_tooltip(StringUtils.SafeFormat(value.ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()), new object[0]) + " / " + Locale.Get("DATETIME_WEEKLOWERCASE"));
			}
		}
	}

	protected override void Start()
	{
		this.m_futureEvents = new EventManager.FutureEvent[this.FUTURE_EVENT_COUNT];
		this.m_bandAdvertisement = base.Find<UIPanel>("BandAdvertisement");
		this.m_premiumStudio = base.Find<UIPanel>("PremiumStudio");
		this.m_totalExpenses = base.Find<UILabel>("ExpensesNumber");
		this.m_futureConcert1 = base.Find<UIPanel>("FutureConcert1");
		this.m_futureConcert2 = base.Find<UIPanel>("FutureConcert2");
		this.m_nowPlaying = base.Find<UILabel>("LabelNowPlaying");
		this.m_nowPlayingBand = base.Find<UILabel>("LabelNowPlayingBandname");
		this.m_visitorCount = base.Find<UILabel>("LabelVisitors");
		this.m_upgrade1 = base.Find<UIButton>("UpgradeButton1");
		this.m_upgrade2 = base.Find<UIButton>("UpgradeButton2");
		this.m_pastConcert1 = base.Find<UIPanel>("PastConcert1");
		this.m_pastConcert2 = base.Find<UIPanel>("PastConcert2");
		this.m_pastConcertsTitle = base.Find<UIPanel>("PastConcertsTitle");
		this.m_pastConcertsContainer = base.Find<UIPanel>("PanelPastConcerts");
		this.m_concertSection = base.Find<UIPanel>("ConcertSection");
		this.m_colorFieldButton = base.Find<UIButton>("ColorFieldButton");
		this.m_colorField = base.Find<UIColorField>("BuildingColor");
		this.m_colorField.add_eventColorPickerOpen(delegate(UIColorField dropdown, UIColorPicker popup, ref bool overridden)
		{
			this.m_colorFieldButton.set_isInteractive(false);
		});
		this.m_colorField.add_eventColorPickerClose(delegate(UIColorField dropdown, UIColorPicker popup, ref bool overridden)
		{
			this.m_colorFieldButton.set_isInteractive(true);
		});
		this.m_securityBudgetSlider = base.Find<UISlider>("SliderSecurityBudget");
		this.m_securityBudgetLabel = base.Find<UILabel>("LabelSecurityBudget");
		this.m_securityBudgetSlider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSecuritySliderValueChanged));
		this.RefreshPolicyToolTips();
		base.Start();
	}

	private void OnSecuritySliderValueChanged(UIComponent component, float value)
	{
		ushort eventIndex = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
		EventInfo info = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info;
		ConcertAI concertAI = info.m_eventAI as ConcertAI;
		concertAI.SetSecurityBudget(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex], (int)value);
	}

	private void PopulateBands()
	{
		ushort building = this.m_InstanceID.Building;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
		BuildingAI buildingAI = info.m_buildingAI;
		this.m_bands = new UITemplateList<UIPanel>(base.Find<UIPanel>("FestivalBandsContainer"), "FestivalBand");
		int num = PrefabCollection<EventInfo>.LoadedCount();
		uint num2 = 0u;
		while ((ulong)num2 < (ulong)((long)num))
		{
			EventInfo loaded = PrefabCollection<EventInfo>.GetLoaded(num2);
			if (buildingAI.SupportEvents(loaded.m_type, loaded.m_group))
			{
				UIPanel uIPanel = this.m_bands.AddItem();
				uIPanel.set_objectUserData(num2);
				ConcertAI concertAI = loaded.GetAI() as ConcertAI;
				if (concertAI != null)
				{
					uIPanel.Find<UILabel>("Name").set_text(loaded.get_name());
					uIPanel.Find<UILabel>("Popularity").set_text(ConcertAI.GetPopularity(loaded).ToString());
					UIPanel uIPanel2 = uIPanel.Find<UIPanel>("NumberFieldTicketPrice");
					uIPanel2.set_objectUserData(num2);
					uIPanel2.GetComponent<NumberField>().eventValueChangedWithComponent += new NumberField.ValueChangedHandlerWithComponent(this.OnTicketPriceChanged);
				}
			}
			num2 += 1u;
		}
	}

	private void OnTicketPriceChanged(UIComponent comp, int value)
	{
		uint index = (uint)comp.get_objectUserData();
		EventInfo loaded = PrefabCollection<EventInfo>.GetLoaded(index);
		ConcertAI concertAI = loaded.GetAI() as ConcertAI;
		EventData data = default(EventData);
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			concertAI.SetTicketPrice(0, ref data, value * 100);
		});
	}

	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		this.m_hasToRefresh = true;
		if (this.m_bands == null)
		{
			this.PopulateBands();
		}
		this.RefreshBands();
		this.RefreshTicketPrices();
	}

	private void RefreshTicketPrices()
	{
		foreach (UIPanel current in this.m_bands.items)
		{
			uint index = (uint)current.get_objectUserData();
			EventInfo loaded = PrefabCollection<EventInfo>.GetLoaded(index);
			ConcertAI concertAI = loaded.GetAI() as ConcertAI;
			if (concertAI != null)
			{
				EventData eventData = default(EventData);
				current.Find<UIPanel>("NumberFieldTicketPrice").GetComponent<NumberField>().value = concertAI.GetTicketPrice(0, ref eventData) / 100;
			}
		}
	}

	private void RefreshBands()
	{
		foreach (UIPanel current in this.m_bands.items)
		{
			uint index = (uint)current.get_objectUserData();
			EventInfo loaded = PrefabCollection<EventInfo>.GetLoaded(index);
			ConcertAI concertAI = loaded.GetAI() as ConcertAI;
			if (concertAI != null)
			{
				current.Find<UILabel>("Popularity").set_text(ConcertAI.GetPopularity(loaded).ToString());
			}
		}
	}

	private void RefreshEventData()
	{
		ushort eventIndex = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
		this.m_colorField.set_selectedColor(Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info.m_eventAI.GetBuildingColor(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex]));
	}

	private void SetEventData()
	{
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			ushort eventIndex = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
			Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info.m_eventAI.SetColor(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex], this.m_colorField.get_selectedColor());
		});
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			ushort eventIndex = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
			EventData eventData = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex];
			EventInfo info2 = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info;
			ConcertAI concertAI = info2.m_eventAI as ConcertAI;
			if (Singleton<EventManager>.get_instance().GetFutureEvents(building, this.m_futureEvents, this.FUTURE_EVENT_COUNT) != this.FUTURE_EVENT_COUNT)
			{
				Debug.LogError("Could not get enough future events.");
			}
			else if ((eventData.m_flags & (EventData.Flags.Active | EventData.Flags.Expired | EventData.Flags.Completed | EventData.Flags.Disorganizing | EventData.Flags.Cancelled)) != EventData.Flags.None)
			{
				this.RefreshFutureConcert(this.m_futureConcert1, this.m_futureEvents[0]);
				this.RefreshFutureConcert(this.m_futureConcert2, this.m_futureEvents[1]);
			}
			else
			{
				this.RefreshCurrentConcert(this.m_futureConcert1, eventData);
				this.RefreshFutureConcert(this.m_futureConcert2, this.m_futureEvents[0]);
			}
			if (this.m_hasToRefresh)
			{
				this.RefreshEventData();
				this.m_securityBudgetSlider.set_maxValue((float)(concertAI.m_securityBudget * 2));
				this.m_securityBudgetSlider.set_value((float)concertAI.GetSecurityBudget(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex]));
				this.m_hasToRefresh = false;
			}
			else
			{
				this.SetEventData();
			}
			if ((eventData.m_flags & EventData.Flags.Active) != EventData.Flags.None)
			{
				this.m_nowPlaying.set_isVisible(true);
				this.m_nowPlayingBand.set_text(eventData.Info.get_name());
			}
			else
			{
				this.m_nowPlaying.set_isVisible(false);
				this.m_nowPlayingBand.set_text(string.Empty);
			}
			if (concertAI != null)
			{
				int num;
				int num2;
				int num3;
				concertAI.CountVisitors(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex], out num, out num2, out num3);
				this.m_visitorCount.set_text(string.Concat(new string[]
				{
					Locale.Get("SPORT_VISITORS"),
					" ",
					num.ToString(),
					" / ",
					num3.ToString()
				}));
			}
			bool flag = false;
			bool isVisible = false;
			ushort nextBuildingEvent = this.GetNextBuildingEvent(eventIndex);
			if (nextBuildingEvent != 0)
			{
				eventData = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)nextBuildingEvent];
				flag = true;
				this.RefreshPastConcert(this.m_pastConcert1, eventData);
				nextBuildingEvent = this.GetNextBuildingEvent(nextBuildingEvent);
				if (nextBuildingEvent != 0 && flag)
				{
					eventData = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)nextBuildingEvent];
					isVisible = true;
					this.RefreshPastConcert(this.m_pastConcert2, eventData);
				}
			}
			this.m_pastConcert1.set_isVisible(flag);
			this.m_pastConcert2.set_isVisible(isVisible);
			this.m_pastConcertsContainer.set_isVisible(flag);
			this.m_pastConcertsTitle.set_isVisible(flag);
			this.m_concertSection.FitChildrenVertically();
			base.get_component().FitChildrenVertically();
			BuildingAI buildingAI = info.m_buildingAI;
			BuildingInfo upgradeInfo = buildingAI.GetUpgradeInfo(building, ref instance.m_buildings.m_buffer[(int)building]);
			ItemClass.Level level = info.m_class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						this.m_upgrade1.set_isEnabled(false);
						this.m_upgrade1.set_disabledBgSprite(this.m_upgradeCompletedTexture);
						this.m_upgrade1.Find<UISprite>("Star1").set_spriteName(this.m_upgradeCompletedStar);
						this.m_upgrade1.Find<UISprite>("Star2").set_spriteName(this.m_upgradeCompletedStar);
						this.m_upgrade1.set_tooltip(string.Empty);
						this.m_upgrade2.set_isEnabled(false);
						this.m_upgrade2.set_disabledBgSprite(this.m_upgradeCompletedTexture);
						this.m_upgrade2.Find<UISprite>("Star1").set_spriteName(this.m_upgradeCompletedStar);
						this.m_upgrade2.Find<UISprite>("Star2").set_spriteName(this.m_upgradeCompletedStar);
						this.m_upgrade2.Find<UISprite>("Star3").set_spriteName(this.m_upgradeCompletedStar);
						this.m_upgrade2.set_tooltip(string.Empty);
					}
				}
				else
				{
					this.m_upgrade1.set_isEnabled(false);
					this.m_upgrade1.set_disabledBgSprite(this.m_upgradeCompletedTexture);
					this.m_upgrade1.Find<UISprite>("Star1").set_spriteName(this.m_upgradeCompletedStar);
					this.m_upgrade1.Find<UISprite>("Star2").set_spriteName(this.m_upgradeCompletedStar);
					this.m_upgrade1.set_tooltip(string.Empty);
					this.m_upgrade2.set_isEnabled(upgradeInfo.m_UnlockMilestone.IsPassed() && Singleton<EconomyManager>.get_instance().LastCashAmount >= (long)upgradeInfo.GetConstructionCost());
					this.m_upgrade2.set_disabledBgSprite(this.m_beforeUpgradeTexture);
					this.m_upgrade2.Find<UISprite>("Star1").set_spriteName(this.m_beforeUpgradeStar);
					this.m_upgrade2.Find<UISprite>("Star2").set_spriteName(this.m_beforeUpgradeStar);
					this.m_upgrade2.Find<UISprite>("Star3").set_spriteName(this.m_beforeUpgradeStar);
					this.m_upgrade2.set_tooltip(this.SummarizeConditions(upgradeInfo));
				}
			}
			else
			{
				this.m_upgrade1.set_isEnabled(upgradeInfo.m_UnlockMilestone.IsPassed() && Singleton<EconomyManager>.get_instance().LastCashAmount >= (long)upgradeInfo.GetConstructionCost());
				this.m_upgrade1.set_disabledBgSprite(this.m_beforeUpgradeTexture);
				this.m_upgrade1.Find<UISprite>("Star1").set_spriteName(this.m_beforeUpgradeStar);
				this.m_upgrade1.Find<UISprite>("Star2").set_spriteName(this.m_beforeUpgradeStar);
				this.m_upgrade1.set_tooltip(this.SummarizeConditions(upgradeInfo));
				this.m_upgrade2.set_isEnabled(false);
				this.m_upgrade2.set_disabledBgSprite(this.m_beforeUpgradeTexture);
				this.m_upgrade2.Find<UISprite>("Star1").set_spriteName(this.m_beforeUpgradeStar);
				this.m_upgrade2.Find<UISprite>("Star2").set_spriteName(this.m_beforeUpgradeStar);
				this.m_upgrade2.Find<UISprite>("Star3").set_spriteName(this.m_beforeUpgradeStar);
				this.m_upgrade2.set_tooltip(this.SummarizeConditions(upgradeInfo.m_buildingAI.GetUpgradeInfo(building, ref instance.m_buildings.m_buffer[(int)building])));
			}
			if (this.m_bands != null)
			{
				this.RefreshBands();
			}
			int num4 = 0;
			int num5 = 0;
			if (Singleton<DistrictManager>.get_instance().IsCityPolicySet(DistrictPolicies.Policies.PremiumStudio))
			{
				num4 = 300000;
				this.m_premiumStudio.Find<UILabel>("WeeklyUpkeepTotal").set_text(base.FormatMoney(num4) + "/" + Locale.Get("DATETIME_WEEKLOWERCASE"));
				base.ColorButton(this.m_premiumStudio.Find<UIButton>("PolicyStats"), this.m_policyActiveColor);
				base.ColorButton(this.m_premiumStudio.Find<UIButton>("PolicyButton"), this.m_policyActiveColor);
			}
			else
			{
				this.m_premiumStudio.Find<UILabel>("WeeklyUpkeepTotal").set_text(" - ");
				base.ColorButton(this.m_premiumStudio.Find<UIButton>("PolicyStats"), this.m_policyInactiveColor);
				base.ColorButton(this.m_premiumStudio.Find<UIButton>("PolicyButton"), Color.get_white());
			}
			if (Singleton<DistrictManager>.get_instance().IsCityPolicySet(DistrictPolicies.Policies.BandAdvertisement))
			{
				num5 = 900000;
				this.m_bandAdvertisement.Find<UILabel>("WeeklyUpkeepTotal").set_text(base.FormatMoney(num5) + "/" + Locale.Get("DATETIME_WEEKLOWERCASE"));
				base.ColorButton(this.m_bandAdvertisement.Find<UIButton>("PolicyStats"), this.m_policyActiveColor);
				base.ColorButton(this.m_bandAdvertisement.Find<UIButton>("PolicyButton"), this.m_policyActiveColor);
			}
			else
			{
				this.m_bandAdvertisement.Find<UILabel>("WeeklyUpkeepTotal").set_text(" - ");
				base.ColorButton(this.m_bandAdvertisement.Find<UIButton>("PolicyStats"), this.m_policyInactiveColor);
				base.ColorButton(this.m_bandAdvertisement.Find<UIButton>("PolicyButton"), Color.get_white());
			}
			int money = num4 + num5;
			this.m_totalExpenses.set_text(base.FormatMoney(money));
		}
	}

	private ushort GetNextBuildingEvent(ushort eventIndex)
	{
		do
		{
			eventIndex = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].m_nextBuildingEvent;
		}
		while ((Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].m_flags & EventData.Flags.Cancelled) != EventData.Flags.None && eventIndex != 0);
		return eventIndex;
	}

	private string SummarizeConditions(BuildingInfo buildingInfo)
	{
		MilestoneInfo unlockMilestone = buildingInfo.m_UnlockMilestone;
		string text = StringUtils.SafeFormat(Locale.Get("FESTIVALPANEL_UPGRADECOST"), (buildingInfo.GetConstructionCost() / 100).ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()));
		text = text + "\n\n" + Locale.Get("FESTIVALPANEL_CONDITIONS") + "\n";
		CombinedMilestone combinedMilestone = unlockMilestone as CombinedMilestone;
		if (combinedMilestone != null)
		{
			bool flag = true;
			MilestoneInfo[] requirePassed = combinedMilestone.m_requirePassed;
			for (int i = 0; i < requirePassed.Length; i++)
			{
				MilestoneInfo milestoneInfo = requirePassed[i];
				if (!flag)
				{
					text += "\n";
				}
				MilestoneInfo.ProgressInfo localizedProgress = milestoneInfo.GetLocalizedProgress();
				if (milestoneInfo.m_name != "Festival Area3 Requirement 3")
				{
					text = string.Concat(new string[]
					{
						text,
						localizedProgress.m_description,
						" (",
						localizedProgress.m_progress,
						")"
					});
				}
				else if (!milestoneInfo.IsPassed())
				{
					text = string.Concat(new string[]
					{
						text,
						localizedProgress.m_description,
						" (",
						localizedProgress.m_progress,
						")"
					});
				}
				else
				{
					text = string.Concat(new string[]
					{
						text,
						localizedProgress.m_description,
						" (",
						Locale.Get("FESTIVALPANEL_MILESTONEPASSED"),
						")"
					});
				}
				flag = false;
			}
		}
		return text;
	}

	private void RefreshCurrentConcert(UIPanel panel, EventData concert)
	{
		panel.Find<UILabel>("Name").set_text(concert.Info.get_name());
		panel.Find<UILabel>("Date").set_text(concert.StartTime.ToString("dd/MM/yyyy"));
	}

	private void RefreshFutureConcert(UIPanel panel, EventManager.FutureEvent concert)
	{
		panel.Find<UILabel>("Name").set_text(concert.m_info.get_name());
		panel.Find<UILabel>("Date").set_text(concert.m_startTime.ToString("dd/MM/yyyy"));
	}

	private void RefreshPastConcert(UIPanel panel, EventData pastConcert)
	{
		panel.Find<UILabel>("Name").set_text(pastConcert.Info.get_name());
		string text = pastConcert.m_popularityDelta.ToString();
		if (pastConcert.m_popularityDelta > 0)
		{
			text = "+" + text;
		}
		panel.Find<UILabel>("PopChange").set_text(text);
		panel.Find<UILabel>("TicketIncome").set_text((pastConcert.m_ticketMoney / 100uL).ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()));
	}

	public override void TempShow(Vector3 worldPosition, InstanceID instanceID)
	{
		base.movingPanel.Hide();
		WorldInfoPanel.Show<FestivalPanel>(worldPosition, instanceID);
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.33f));
	}

	public void TogglePremiumStudio()
	{
		base.TogglePolicy(DistrictPolicies.Policies.PremiumStudio);
	}

	public void ToggleBandAdvertisement()
	{
		base.TogglePolicy(DistrictPolicies.Policies.BandAdvertisement);
	}

	public void UpgradeToLevel2()
	{
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			Singleton<BuildingManager>.get_instance().UpgradeBuilding(this.m_InstanceID.Building);
		});
		this.m_upgrade1.get_tooltipBox().Hide();
	}

	public void UpgradeToLevel3()
	{
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			Singleton<BuildingManager>.get_instance().UpgradeBuilding(this.m_InstanceID.Building);
		});
		this.m_upgrade2.get_tooltipBox().Hide();
	}

	private void OnLocaleChanged()
	{
		this.RefreshPolicyToolTips();
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void RefreshPolicyToolTips()
	{
		this.RefreshPolicyTooltip(this.m_bandAdvertisement);
		this.RefreshPolicyTooltip(this.m_premiumStudio);
	}

	private void RefreshPolicyTooltip(UIPanel policyPanel)
	{
		UIButton uIButton = policyPanel.Find<UIButton>("PolicyButton");
		UIButton uIButton2 = policyPanel.Find<UIButton>("PolicyStats");
		uIButton.set_tooltipBox(base.policiesTooltip);
		uIButton.set_tooltip(TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("POLICIES", policyPanel.get_name()),
			"text",
			Locale.Get("POLICIES_DETAIL", policyPanel.get_name())
		}));
		uIButton2.set_tooltipBox(base.policiesTooltip);
		uIButton2.set_tooltip(TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("POLICIES", policyPanel.get_name()),
			"text",
			Locale.Get("POLICIES_DETAIL", policyPanel.get_name())
		}));
	}

	private readonly int FUTURE_EVENT_COUNT = 2;

	private UIPanel m_bandAdvertisement;

	private UIPanel m_premiumStudio;

	private UIPanel m_futureConcert1;

	private UIPanel m_futureConcert2;

	private UILabel m_totalExpenses;

	private UILabel m_nowPlaying;

	private UILabel m_nowPlayingBand;

	private UILabel m_visitorCount;

	private UIPanel m_pastConcertsTitle;

	private UIPanel m_pastConcertsContainer;

	private UIPanel m_concertSection;

	private UIColorField m_colorField;

	private UIButton m_colorFieldButton;

	private UIButton m_upgrade1;

	private UIButton m_upgrade2;

	private UISlider m_securityBudgetSlider;

	private UILabel m_securityBudgetLabel;

	public UITextureAtlas m_atlas;

	[UISprite("m_atlas")]
	public string m_upgradeCompletedTexture;

	[UISprite("m_atlas")]
	public string m_beforeUpgradeTexture;

	[UISprite("m_atlas")]
	public string m_upgradeCompletedStar;

	[UISprite("m_atlas")]
	public string m_beforeUpgradeStar;

	private UITemplateList<UIPanel> m_bands;

	private static string kProblemIconTemplate = "ProblemIconTemplateFestival";

	private UIPanel m_pastConcert1;

	private UIPanel m_pastConcert2;

	private bool m_hasToRefresh;

	private EventManager.FutureEvent[] m_futureEvents;

	private float m_securityBudget;
}
