﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class PublicTransportInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_BusCitizens = base.Find<UILabel>("BusCitizens");
		this.m_BusTourists = base.Find<UILabel>("BusTourists");
		this.m_MetroCitizens = base.Find<UILabel>("MetroCitizens");
		this.m_MetroTourists = base.Find<UILabel>("MetroTourists");
		this.m_TrainCitizens = base.Find<UILabel>("TrainCitizens");
		this.m_TrainTourists = base.Find<UILabel>("TrainTourists");
		this.m_ShipCitizens = base.Find<UILabel>("ShipCitizens");
		this.m_ShipTourists = base.Find<UILabel>("ShipTourists");
		this.m_PlaneCitizens = base.Find<UILabel>("PlaneCitizens");
		this.m_PlaneTourists = base.Find<UILabel>("PlaneTourists");
		this.m_TaxiCitizens = base.Find<UILabel>("TaxiCitizens");
		this.m_TaxiTourists = base.Find<UILabel>("TaxiTourists");
		this.m_TramCitizens = base.Find<UILabel>("TramCitizens");
		this.m_TramTourists = base.Find<UILabel>("TramTourists");
		this.m_MonorailCitizens = base.Find<UILabel>("MonorailCitizens");
		this.m_MonorailTourists = base.Find<UILabel>("MonorailTourists");
		this.m_CableCarCitizens = base.Find<UILabel>("CableCarCitizens");
		this.m_CableCarTourists = base.Find<UILabel>("CableCarTourists");
		this.m_TotalCitizens = base.Find<UILabel>("TotalCitizens");
		this.m_TotalTourists = base.Find<UILabel>("TotalTourists");
		PublicTransportDetailPanel publicTransportDetailPanel = UIView.get_library().Get<PublicTransportDetailPanel>("PublicTransportDetailPanel");
		if (publicTransportDetailPanel != null)
		{
			publicTransportDetailPanel.SetPosition(base.get_component().get_absolutePosition() + new Vector3(base.get_component().get_size().x, 0f, 0f));
		}
	}

	private void OpenDetailPanel(int idx)
	{
		PublicTransportDetailPanel publicTransportDetailPanel = UIView.get_library().Show<PublicTransportDetailPanel>("PublicTransportDetailPanel", true, false);
		publicTransportDetailPanel.SetActiveTab(idx);
		publicTransportDetailPanel.SetPosition(base.get_component().get_absolutePosition() + new Vector3(base.get_component().get_size().x, 0f, 0f));
	}

	public void OpenDetailPanelDefaultTab()
	{
		PublicTransportDetailPanel publicTransportDetailPanel = UIView.get_library().Show<PublicTransportDetailPanel>("PublicTransportDetailPanel", true, false);
		publicTransportDetailPanel.SetPosition(base.get_component().get_absolutePosition() + new Vector3(base.get_component().get_size().x, 0f, 0f));
	}

	private void SetSpriteColor(UIPanel entry, Color col)
	{
		col.a = entry.get_opacity();
		entry.set_color(col);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC);
				base.Find<UIButton>("BusDetail").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
				{
					r.Use();
					this.OpenDetailPanel(0);
				});
				UIButton uIButton = base.Find<UIButton>("TramDetail");
				if (Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Tram))
				{
					uIButton.Show();
					uIButton.add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
					{
						r.Use();
						this.OpenDetailPanel(1);
					});
				}
				else
				{
					uIButton.Hide();
				}
				base.Find<UIButton>("MetroDetail").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
				{
					r.Use();
					this.OpenDetailPanel(2);
				});
				base.Find<UIButton>("TrainDetail").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
				{
					r.Use();
					this.OpenDetailPanel(3);
				});
				UIButton uIButton2 = base.Find<UIButton>("ShipDetail");
				if (flag)
				{
					uIButton2.Show();
					uIButton2.add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
					{
						r.Use();
						this.OpenDetailPanel(4);
					});
				}
				else
				{
					uIButton2.Hide();
				}
				UIButton uIButton3 = base.Find<UIButton>("PlaneDetail");
				if (flag)
				{
					uIButton3.Show();
					uIButton3.add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
					{
						r.Use();
						this.OpenDetailPanel(5);
					});
				}
				else
				{
					uIButton3.Hide();
				}
				UIButton uIButton4 = base.Find<UIButton>("MonorailDetail");
				if (flag)
				{
					uIButton4.Show();
					uIButton4.add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
					{
						r.Use();
						this.OpenDetailPanel(6);
					});
				}
				else
				{
					uIButton4.Hide();
				}
				UIComponent uIComponent = base.Find("BusLegend");
				UIComponent uIComponent2 = base.Find("MetroLegend");
				UIComponent uIComponent3 = base.Find("TrainLegend");
				UIComponent uIComponent4 = base.Find("ShipLegend");
				UIComponent uIComponent5 = base.Find("PlaneLegend");
				UIComponent uIComponent6 = base.Find("TaxiLegend");
				UIComponent uIComponent7 = base.Find("TramLegend");
				UIComponent uIComponent8 = base.Find("MonorailLegend");
				UIComponent uIComponent9 = base.Find("CableCarLegend");
				UIPanel entry = base.Find<UIPanel>("BusBackground");
				UIPanel entry2 = base.Find<UIPanel>("MetroBackground");
				UIPanel entry3 = base.Find<UIPanel>("TrainBackground");
				UIPanel entry4 = base.Find<UIPanel>("ShipBackground");
				UIPanel entry5 = base.Find<UIPanel>("PlaneBackground");
				UIPanel entry6 = base.Find<UIPanel>("TaxiBackground");
				UIPanel entry7 = base.Find<UIPanel>("TramBackground");
				UIPanel entry8 = base.Find<UIPanel>("MonorailBackground");
				UIPanel entry9 = base.Find<UIPanel>("CableCarBackground");
				if (Singleton<TransportManager>.get_exists())
				{
					uIComponent.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[0]);
					uIComponent2.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[1]);
					uIComponent3.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[2]);
					uIComponent4.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[3]);
					uIComponent5.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[4]);
					uIComponent6.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[5]);
					uIComponent7.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[6]);
					uIComponent8.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[8]);
					uIComponent9.Find<UISprite>("ColorActive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_transportColors[9]);
					uIComponent.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[0]);
					uIComponent2.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[1]);
					uIComponent3.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[2]);
					uIComponent4.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[3]);
					uIComponent5.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[4]);
					uIComponent6.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[5]);
					uIComponent7.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[6]);
					uIComponent8.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[8]);
					uIComponent9.Find<UISprite>("ColorInactive").set_color(Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[9]);
					this.SetSpriteColor(entry, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[0]);
					this.SetSpriteColor(entry2, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[1]);
					this.SetSpriteColor(entry3, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[2]);
					this.SetSpriteColor(entry4, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[3]);
					this.SetSpriteColor(entry5, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[4]);
					this.SetSpriteColor(entry6, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[5]);
					this.SetSpriteColor(entry7, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[6]);
					this.SetSpriteColor(entry8, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[8]);
					this.SetSpriteColor(entry9, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[9]);
					bool isVisible = Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Taxi);
					uIComponent6.set_isVisible(isVisible);
					UIComponent uIComponent10 = base.Find("TaxiPanel");
					if (uIComponent10 != null)
					{
						uIComponent10.set_isVisible(isVisible);
					}
					bool isVisible2 = Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Tram);
					uIComponent7.set_isVisible(isVisible2);
					uIComponent10 = base.Find("TramPanel");
					if (uIComponent10 != null)
					{
						uIComponent10.set_isVisible(isVisible2);
					}
					bool isVisible3 = Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Monorail);
					uIComponent8.set_isVisible(isVisible3);
					uIComponent10 = base.Find("MonorailPanel");
					if (uIComponent10 != null)
					{
						uIComponent10.set_isVisible(isVisible3);
					}
					bool isVisible4 = Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.CableCar);
					uIComponent9.set_isVisible(isVisible4);
					uIComponent10 = base.Find("CableCarPanel");
					if (uIComponent10 != null)
					{
						uIComponent10.set_isVisible(isVisible4);
					}
					UIComponent uIComponent11 = base.Find("TransportContainer");
					uIComponent11.FitChildrenVertically(5f);
					UIButton uIButton5 = base.Find<UIButton>("LinesOverview");
					UIComponent uIComponent12 = base.Find("Legend");
					uIComponent12.Find("Panel").FitChildrenVertically();
					uIComponent12.FitChildrenVertically(5f);
					uIButton5.set_relativePosition(new Vector3(uIComponent12.get_relativePosition().x, uIComponent11.get_relativePosition().y + uIComponent11.get_size().y + 5f, 0f));
					uIComponent12.set_relativePosition(new Vector3(uIComponent12.get_relativePosition().x, uIButton5.get_relativePosition().y + uIButton5.get_size().y + 5f, 0f));
					base.get_component().FitChildrenVertically(10f);
				}
			}
			this.m_initialized = true;
		}
		TransportManager instance = Singleton<TransportManager>.get_instance();
		uint averageCount = instance.m_passengers[0].m_residentPassengers.m_averageCount;
		uint averageCount2 = instance.m_passengers[0].m_touristPassengers.m_averageCount;
		uint averageCount3 = instance.m_passengers[1].m_residentPassengers.m_averageCount;
		uint averageCount4 = instance.m_passengers[1].m_touristPassengers.m_averageCount;
		uint averageCount5 = instance.m_passengers[2].m_residentPassengers.m_averageCount;
		uint averageCount6 = instance.m_passengers[2].m_touristPassengers.m_averageCount;
		uint averageCount7 = instance.m_passengers[3].m_residentPassengers.m_averageCount;
		uint averageCount8 = instance.m_passengers[3].m_touristPassengers.m_averageCount;
		uint averageCount9 = instance.m_passengers[4].m_residentPassengers.m_averageCount;
		uint averageCount10 = instance.m_passengers[4].m_touristPassengers.m_averageCount;
		uint averageCount11 = instance.m_passengers[5].m_residentPassengers.m_averageCount;
		uint averageCount12 = instance.m_passengers[5].m_touristPassengers.m_averageCount;
		uint averageCount13 = instance.m_passengers[6].m_residentPassengers.m_averageCount;
		uint averageCount14 = instance.m_passengers[6].m_touristPassengers.m_averageCount;
		uint averageCount15 = instance.m_passengers[8].m_residentPassengers.m_averageCount;
		uint averageCount16 = instance.m_passengers[8].m_touristPassengers.m_averageCount;
		uint averageCount17 = instance.m_passengers[9].m_residentPassengers.m_averageCount;
		uint averageCount18 = instance.m_passengers[9].m_touristPassengers.m_averageCount;
		this.m_BusCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount));
		this.m_BusTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount2));
		this.m_MetroCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount3));
		this.m_MetroTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount4));
		this.m_TrainCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount5));
		this.m_TrainTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount6));
		this.m_ShipCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount7));
		this.m_ShipTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount8));
		this.m_PlaneCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount9));
		this.m_PlaneTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount10));
		this.m_TaxiCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount11));
		this.m_TaxiTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount12));
		this.m_TramCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount13));
		this.m_TramTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount14));
		this.m_MonorailCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount15));
		this.m_MonorailTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount16));
		this.m_CableCarCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount17));
		this.m_CableCarTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount18));
		this.m_TotalCitizens.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount9 + averageCount7 + averageCount5 + averageCount3 + averageCount + averageCount11 + averageCount13 + averageCount15 + averageCount17));
		this.m_TotalTourists.set_text(StringUtils.SafeFormat(Locale.Get("INFO_PUBLICTRANSPORT_COUNT"), averageCount10 + averageCount8 + averageCount6 + averageCount4 + averageCount2 + averageCount12 + averageCount14 + averageCount16 + averageCount18));
	}

	private UILabel m_BusCitizens;

	private UILabel m_BusTourists;

	private UILabel m_MetroCitizens;

	private UILabel m_MetroTourists;

	private UILabel m_TrainCitizens;

	private UILabel m_TrainTourists;

	private UILabel m_ShipCitizens;

	private UILabel m_ShipTourists;

	private UILabel m_PlaneCitizens;

	private UILabel m_PlaneTourists;

	private UILabel m_TaxiCitizens;

	private UILabel m_TaxiTourists;

	private UILabel m_TramCitizens;

	private UILabel m_TramTourists;

	private UILabel m_MonorailCitizens;

	private UILabel m_MonorailTourists;

	private UILabel m_CableCarCitizens;

	private UILabel m_CableCarTourists;

	private UILabel m_TotalCitizens;

	private UILabel m_TotalTourists;

	private bool m_initialized;
}
