﻿using System;
using System.Collections;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class InfoManager : SimulationManagerBase<InfoManager, InfoProperties>, ISimulationManager
{
	public InfoManager.InfoMode CurrentMode
	{
		get
		{
			return this.m_actualMode;
		}
	}

	public InfoManager.SubInfoMode CurrentSubMode
	{
		get
		{
			return this.m_actualSubMode;
		}
	}

	public InfoManager.InfoMode NextMode
	{
		get
		{
			return this.m_currentMode;
		}
	}

	public InfoManager.SubInfoMode NextSubMode
	{
		get
		{
			return this.m_currentSubMode;
		}
	}

	public void SetCurrentMode(InfoManager.InfoMode mode, InfoManager.SubInfoMode subMode)
	{
		this.ToggleModeUI(mode);
		if (this.m_currentMode != mode || this.m_currentSubMode != subMode)
		{
			this.SetMode(mode, subMode);
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_gameInfoViews = 0u;
		IEnumerator enumerator = Enum.GetValues(typeof(InfoManager.InfoMode)).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				InfoManager.InfoMode infoMode = (InfoManager.InfoMode)enumerator.Current;
				if (!string.IsNullOrEmpty(EnumExtensions.Name<InfoManager.InfoMode>(infoMode, "Game")))
				{
					this.m_gameInfoViews |= 1u << (int)infoMode;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	public void UpdateInfoMode()
	{
		if ((this.m_currentMode != this.m_actualMode || this.m_currentSubMode != this.m_actualSubMode) && !Singleton<CoverageManager>.get_instance().WaitingForIt())
		{
			this.SetActualMode(this.m_currentMode, this.m_currentSubMode);
		}
	}

	public override void InitializeProperties(InfoProperties properties)
	{
		base.InitializeProperties(properties);
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
		this.SetMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		this.SetActualMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		this.ToggleModeUI(InfoManager.InfoMode.None);
	}

	public override void DestroyProperties(InfoProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_cameraController = null;
		}
		base.DestroyProperties(properties);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		this.SetActualMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
	}

	private void ToggleModeUI(InfoManager.InfoMode mode)
	{
		if (UIView.get_library() != null)
		{
			if (this.m_currentMode != mode)
			{
				switch (this.m_currentMode)
				{
				case InfoManager.InfoMode.Electricity:
					UIView.get_library().Hide("ElectricityInfoViewPanel");
					break;
				case InfoManager.InfoMode.Water:
					UIView.get_library().Hide("WaterInfoViewPanel");
					break;
				case InfoManager.InfoMode.CrimeRate:
					UIView.get_library().Hide("CrimeInfoViewPanel");
					break;
				case InfoManager.InfoMode.Health:
					UIView.get_library().Hide("HealthInfoViewPanel");
					break;
				case InfoManager.InfoMode.Happiness:
					UIView.get_library().Hide("HappinessInfoViewPanel");
					break;
				case InfoManager.InfoMode.Density:
					UIView.get_library().Hide("PopulationInfoViewPanel");
					break;
				case InfoManager.InfoMode.NoisePollution:
					UIView.get_library().Hide("NoisePollutionInfoViewPanel");
					break;
				case InfoManager.InfoMode.Transport:
					UIView.get_library().Hide("PublicTransportInfoViewPanel");
					break;
				case InfoManager.InfoMode.Pollution:
					UIView.get_library().Hide("PollutionInfoViewPanel");
					break;
				case InfoManager.InfoMode.NaturalResources:
					UIView.get_library().Hide("NaturalResourcesInfoViewPanel");
					break;
				case InfoManager.InfoMode.LandValue:
					UIView.get_library().Hide("LandValueInfoViewPanel");
					break;
				case InfoManager.InfoMode.Connections:
					UIView.get_library().Hide("OutsideConnectionsInfoViewPanel");
					break;
				case InfoManager.InfoMode.Traffic:
					UIView.get_library().Hide("TrafficInfoViewPanel");
					break;
				case InfoManager.InfoMode.Wind:
					UIView.get_library().Hide("WindInfoViewPanel");
					break;
				case InfoManager.InfoMode.Garbage:
					UIView.get_library().Hide("GarbageInfoViewPanel");
					break;
				case InfoManager.InfoMode.BuildingLevel:
					UIView.get_library().Hide("LevelsInfoViewPanel");
					break;
				case InfoManager.InfoMode.FireSafety:
					UIView.get_library().Hide("FireSafetyInfoViewPanel");
					break;
				case InfoManager.InfoMode.Education:
					UIView.get_library().Hide("EducationInfoViewPanel");
					break;
				case InfoManager.InfoMode.Entertainment:
					UIView.get_library().Hide("EntertainmentInfoViewPanel");
					break;
				case InfoManager.InfoMode.TerrainHeight:
					UIView.get_library().Hide("TerrainHeightInfoViewPanel");
					break;
				case InfoManager.InfoMode.Heating:
					UIView.get_library().Hide("HeatingInfoViewPanel");
					break;
				case InfoManager.InfoMode.Maintenance:
					UIView.get_library().Hide("RoadMaintenanceInfoViewPanel");
					break;
				case InfoManager.InfoMode.Snow:
					UIView.get_library().Hide("RoadSnowInfoViewPanel");
					break;
				case InfoManager.InfoMode.EscapeRoutes:
					UIView.get_library().Hide("EscapeRoutesInfoViewPanel");
					break;
				case InfoManager.InfoMode.Radio:
					UIView.get_library().Hide("RadioInfoViewPanel");
					break;
				case InfoManager.InfoMode.Destruction:
					UIView.get_library().Hide("DestructionInfoViewPanel");
					break;
				case InfoManager.InfoMode.DisasterDetection:
					UIView.get_library().Hide("DisasterDetectionInfoViewPanel");
					break;
				case InfoManager.InfoMode.DisasterHazard:
					UIView.get_library().Hide("DisasterRiskInfoViewPanel");
					break;
				case InfoManager.InfoMode.TrafficRoutes:
					UIView.get_library().Hide("TrafficRoutesInfoViewPanel");
					break;
				}
			}
			switch (mode)
			{
			case InfoManager.InfoMode.Electricity:
				UIView.get_library().Show("ElectricityInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Water:
				UIView.get_library().Show("WaterInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.CrimeRate:
				UIView.get_library().Show("CrimeInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Health:
				UIView.get_library().Show("HealthInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Happiness:
				UIView.get_library().Show("HappinessInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Density:
				UIView.get_library().Show("PopulationInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.NoisePollution:
				UIView.get_library().Show("NoisePollutionInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Transport:
				UIView.get_library().Show("PublicTransportInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Pollution:
				UIView.get_library().Show("PollutionInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.NaturalResources:
				UIView.get_library().Show("NaturalResourcesInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.LandValue:
				UIView.get_library().Show("LandValueInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Connections:
				UIView.get_library().Show("OutsideConnectionsInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Traffic:
				UIView.get_library().Show("TrafficInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Wind:
				UIView.get_library().Show("WindInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Garbage:
				UIView.get_library().Show("GarbageInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.BuildingLevel:
				UIView.get_library().Show("LevelsInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.FireSafety:
				UIView.get_library().Show("FireSafetyInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Education:
				UIView.get_library().Show("EducationInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Entertainment:
				UIView.get_library().Show("EntertainmentInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.TerrainHeight:
				UIView.get_library().Show("TerrainHeightInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Heating:
				UIView.get_library().Show("HeatingInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Maintenance:
				UIView.get_library().Show("RoadMaintenanceInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Snow:
				UIView.get_library().Show("RoadSnowInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.EscapeRoutes:
				UIView.get_library().Show("EscapeRoutesInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Radio:
				UIView.get_library().Show("RadioInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.Destruction:
				UIView.get_library().Show("DestructionInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.DisasterDetection:
				UIView.get_library().Show("DisasterDetectionInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.DisasterHazard:
				UIView.get_library().Show("DisasterRiskInfoViewPanel", true, true);
				break;
			case InfoManager.InfoMode.TrafficRoutes:
				UIView.get_library().Show("TrafficRoutesInfoViewPanel");
				break;
			}
		}
	}

	private void SetMode(InfoManager.InfoMode mode, InfoManager.SubInfoMode subMode)
	{
		this.m_currentMode = mode;
		this.m_currentSubMode = subMode;
		switch (mode)
		{
		case InfoManager.InfoMode.Electricity:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Water:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.CrimeRate:
			if (subMode == InfoManager.SubInfoMode.Default)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.PoliceDepartment, ItemClass.SubService.None, ItemClass.Level.Level1, 500f, false);
			}
			else
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			}
			break;
		case InfoManager.InfoMode.Health:
			if (subMode == InfoManager.SubInfoMode.WaterPower)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.HealthCare, ItemClass.SubService.None, ItemClass.Level.Level2, 500f, false);
			}
			else
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.HealthCare, ItemClass.SubService.None, ItemClass.Level.Level1, 500f, false);
			}
			break;
		case InfoManager.InfoMode.Happiness:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Density:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.NoisePollution:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Transport:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Pollution:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.NaturalResources:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.LandValue:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Districts:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Connections:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Traffic:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Wind:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Garbage:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Garbage, ItemClass.SubService.None, ItemClass.Level.None, 2000f, false);
			break;
		case InfoManager.InfoMode.BuildingLevel:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.FireSafety:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.FireDepartment, ItemClass.SubService.None, ItemClass.Level.Level1, 500f, false);
			break;
		case InfoManager.InfoMode.Education:
			if (subMode == InfoManager.SubInfoMode.WaterPower)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Education, ItemClass.SubService.None, ItemClass.Level.Level2, 500f, true);
			}
			else if (subMode == InfoManager.SubInfoMode.WindPower)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Education, ItemClass.SubService.None, ItemClass.Level.Level3, 500f, true);
			}
			else
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Education, ItemClass.SubService.None, ItemClass.Level.Level1, 500f, true);
			}
			break;
		case InfoManager.InfoMode.Entertainment:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, true);
			break;
		case InfoManager.InfoMode.TerrainHeight:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Heating:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Maintenance:
			if (subMode == InfoManager.SubInfoMode.Default)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Road, ItemClass.SubService.None, ItemClass.Level.Level2, 2000f, false);
			}
			else
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			}
			break;
		case InfoManager.InfoMode.Snow:
			if (subMode == InfoManager.SubInfoMode.Default)
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Road, ItemClass.SubService.None, ItemClass.Level.Level4, 2000f, false);
			}
			else
			{
				Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			}
			break;
		case InfoManager.InfoMode.EscapeRoutes:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Disaster, ItemClass.SubService.None, ItemClass.Level.Level4, 500f, true);
			break;
		case InfoManager.InfoMode.Radio:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Destruction:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.Disaster, ItemClass.SubService.None, ItemClass.Level.Level2, 2000f, false);
			break;
		case InfoManager.InfoMode.DisasterDetection:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.DisasterHazard:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.TrafficRoutes:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		case InfoManager.InfoMode.Underground:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		default:
			Singleton<CoverageManager>.get_instance().SetMode(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None, 300f, false);
			break;
		}
	}

	private void SetActualMode(InfoManager.InfoMode mode, InfoManager.SubInfoMode subMode)
	{
		this.m_actualMode = mode;
		this.m_actualSubMode = subMode;
		ImmaterialResourceManager.Resource resource = ImmaterialResourceManager.Resource.None;
		int num = 0;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		bool flag4 = false;
		bool flag5 = false;
		bool flag6 = false;
		bool flag7 = false;
		int num2 = 0;
		bool tunnelsVisibleInfo = false;
		bool flag8 = false;
		bool flag9 = false;
		bool flag10 = false;
		bool flag11 = false;
		bool transparentWater = false;
		bool flag12 = false;
		bool renderDirectionArrowsInfo = false;
		bool pathsVisible = false;
		bool pathVisible = false;
		DayNightProperties instance = DayNightProperties.instance;
		if (instance != null)
		{
			instance.Refresh();
		}
		if (mode == InfoManager.InfoMode.None)
		{
			if (this.m_cameraController != null)
			{
				this.m_cameraController.SetViewMode(CameraController.ViewMode.Normal);
			}
			if (this.m_properties != null)
			{
				Shader.SetGlobalColor("_InfoCurrentColor", this.m_properties.m_neutralColor.get_linear());
				Shader.SetGlobalColor("_InfoCurrentColorB", this.m_properties.m_neutralColor.get_linear());
				Shader.SetGlobalColor("_InfoTargetColor", this.m_properties.m_neutralColor.get_linear());
				Shader.SetGlobalColor("_InfoNegativeColor", this.m_properties.m_neutralColor.get_linear());
			}
			Shader.EnableKeyword("INFOMODE_OFF");
			Shader.DisableKeyword("INFOMODE_ON");
		}
		else
		{
			if (this.m_cameraController != null)
			{
				this.m_cameraController.SetViewMode(CameraController.ViewMode.Info);
			}
			if (this.m_properties != null)
			{
				Shader.SetGlobalColor("_InfoCurrentColor", this.m_properties.m_modeProperties[(int)mode].m_activeColor.get_linear());
				Shader.SetGlobalColor("_InfoCurrentColorB", this.m_properties.m_modeProperties[(int)mode].m_activeColorB.get_linear());
				Shader.SetGlobalColor("_InfoTargetColor", this.m_properties.m_modeProperties[(int)mode].m_targetColor.get_linear());
				Shader.SetGlobalColor("_InfoNegativeColor", this.m_properties.m_modeProperties[(int)mode].m_negativeColor.get_linear());
			}
			uint num3 = this.m_visitedInfoViews | 1u << (int)mode;
			if (this.m_visitedInfoViews != num3)
			{
				this.m_visitedInfoViews = num3;
				if ((num3 & this.m_gameInfoViews) == this.m_gameInfoViews && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True && !PlatformService.get_achievements().get_Item("WellInformed").get_achieved())
				{
					PlatformService.get_achievements().get_Item("WellInformed").Unlock();
				}
			}
			Shader.EnableKeyword("INFOMODE_ON");
			Shader.DisableKeyword("INFOMODE_OFF");
			switch (mode)
			{
			case InfoManager.InfoMode.Electricity:
				flag = true;
				if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag5 = true;
					flag11 = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WindPower)
				{
					flag10 = true;
				}
				break;
			case InfoManager.InfoMode.Water:
				flag2 = true;
				flag4 = true;
				if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag5 = true;
					flag6 = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WindPower)
				{
					flag8 = true;
				}
				else if (subMode == InfoManager.SubInfoMode.Default)
				{
					flag5 = true;
				}
				break;
			case InfoManager.InfoMode.NoisePollution:
				resource = ImmaterialResourceManager.Resource.NoisePollution;
				break;
			case InfoManager.InfoMode.Transport:
				num2 = -129;
				tunnelsVisibleInfo = true;
				renderDirectionArrowsInfo = true;
				if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag5 = true;
				}
				break;
			case InfoManager.InfoMode.Pollution:
				flag4 = true;
				flag8 = true;
				flag6 = true;
				break;
			case InfoManager.InfoMode.NaturalResources:
				flag9 = true;
				break;
			case InfoManager.InfoMode.LandValue:
				resource = ImmaterialResourceManager.Resource.LandValue;
				break;
			case InfoManager.InfoMode.Districts:
				flag7 = true;
				break;
			case InfoManager.InfoMode.Traffic:
				tunnelsVisibleInfo = true;
				renderDirectionArrowsInfo = true;
				break;
			case InfoManager.InfoMode.Wind:
				flag10 = true;
				break;
			case InfoManager.InfoMode.Garbage:
				if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag5 = true;
					flag6 = true;
				}
				break;
			case InfoManager.InfoMode.FireSafety:
				resource = ImmaterialResourceManager.Resource.FirewatchCoverage;
				flag12 = true;
				break;
			case InfoManager.InfoMode.TerrainHeight:
				flag11 = true;
				transparentWater = true;
				break;
			case InfoManager.InfoMode.Heating:
				flag3 = true;
				break;
			case InfoManager.InfoMode.EscapeRoutes:
				num2 = 128;
				tunnelsVisibleInfo = true;
				renderDirectionArrowsInfo = true;
				break;
			case InfoManager.InfoMode.Radio:
				resource = ImmaterialResourceManager.Resource.RadioCoverage;
				break;
			case InfoManager.InfoMode.DisasterDetection:
				if (subMode == InfoManager.SubInfoMode.Default)
				{
					flag5 = true;
					resource = ImmaterialResourceManager.Resource.EarthquakeCoverage;
				}
				else if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag5 = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WindPower)
				{
					resource = ImmaterialResourceManager.Resource.EarthquakeCoverage;
				}
				break;
			case InfoManager.InfoMode.DisasterHazard:
				num = 1 << (int)subMode;
				break;
			case InfoManager.InfoMode.TrafficRoutes:
				tunnelsVisibleInfo = true;
				if (subMode == InfoManager.SubInfoMode.Default)
				{
					pathsVisible = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					renderDirectionArrowsInfo = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WindPower)
				{
					pathVisible = true;
				}
				break;
			case InfoManager.InfoMode.Underground:
				if (subMode == InfoManager.SubInfoMode.Default)
				{
					tunnelsVisibleInfo = true;
				}
				else if (subMode == InfoManager.SubInfoMode.WaterPower)
				{
					flag4 = true;
				}
				break;
			}
		}
		Singleton<ImmaterialResourceManager>.get_instance().ResourceMapVisible = resource;
		Singleton<DisasterManager>.get_instance().HazardMapVisible = num;
		Singleton<ElectricityManager>.get_instance().ElectricityMapVisible = flag;
		Singleton<WaterManager>.get_instance().WaterMapVisible = (flag2 || flag4);
		Singleton<WaterManager>.get_instance().HeatingMapVisible = flag3;
		Singleton<DistrictManager>.get_instance().DistrictsInfoVisible = flag7;
		Singleton<TransportManager>.get_instance().TunnelsVisibleInfo = tunnelsVisibleInfo;
		Singleton<TransportManager>.get_instance().LinesVisible = num2;
		Singleton<WeatherManager>.get_instance().WindMapVisible = flag10;
		Singleton<TerrainManager>.get_instance().RenderTopographyInfo = flag11;
		Singleton<TerrainManager>.get_instance().TransparentWater = transparentWater;
		Singleton<NetManager>.get_instance().RenderDirectionArrowsInfo = renderDirectionArrowsInfo;
		Singleton<NetManager>.get_instance().PathVisualizer.PathsVisible = pathsVisible;
		Singleton<NetManager>.get_instance().NetAdjust.PathVisible = pathVisible;
		Singleton<BuildingManager>.get_instance().UpdateBuildingColors();
		Singleton<NetManager>.get_instance().UpdateSegmentColors();
		Singleton<NetManager>.get_instance().UpdateNodeColors();
		if (flag && flag10)
		{
			Shader.EnableKeyword("INFOMODE_ELECTRICITY");
			Shader.EnableKeyword("INFOMODE_WINDPOWER");
			Shader.EnableKeyword("INFOMODE_WIND");
		}
		else if (flag)
		{
			Shader.EnableKeyword("INFOMODE_ELECTRICITY");
			Shader.DisableKeyword("INFOMODE_WINDPOWER");
			Shader.DisableKeyword("INFOMODE_WIND");
		}
		else if (flag10)
		{
			Shader.DisableKeyword("INFOMODE_ELECTRICITY");
			Shader.DisableKeyword("INFOMODE_WINDPOWER");
			Shader.EnableKeyword("INFOMODE_WIND");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_ELECTRICITY");
			Shader.DisableKeyword("INFOMODE_WINDPOWER");
			Shader.DisableKeyword("INFOMODE_WIND");
		}
		if (flag && flag5)
		{
			Shader.EnableKeyword("INFOMODE_WATERPOWER");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_WATERPOWER");
		}
		if (flag2 && flag8)
		{
			Shader.EnableKeyword("INFOMODE_WATER");
			Shader.EnableKeyword("INFOMODE_GROUNDWATER");
			Shader.DisableKeyword("INFOMODE_POLLUTION");
		}
		else if (flag2 || flag3)
		{
			Shader.EnableKeyword("INFOMODE_WATER");
			Shader.DisableKeyword("INFOMODE_GROUNDWATER");
			Shader.DisableKeyword("INFOMODE_POLLUTION");
		}
		else if (flag8)
		{
			Shader.DisableKeyword("INFOMODE_WATER");
			Shader.DisableKeyword("INFOMODE_GROUNDWATER");
			Shader.EnableKeyword("INFOMODE_POLLUTION");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_WATER");
			Shader.DisableKeyword("INFOMODE_GROUNDWATER");
			Shader.DisableKeyword("INFOMODE_POLLUTION");
		}
		if (flag5 && flag6)
		{
			Shader.EnableKeyword("INFOMODE_FLOWING_WATER_POLLUTION");
			Shader.EnableKeyword("INFOMODE_FLOWING_WATER");
		}
		else if (flag5)
		{
			Shader.DisableKeyword("INFOMODE_FLOWING_WATER_POLLUTION");
			Shader.EnableKeyword("INFOMODE_FLOWING_WATER");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_FLOWING_WATER_POLLUTION");
			Shader.DisableKeyword("INFOMODE_FLOWING_WATER");
		}
		if (flag12)
		{
			Shader.EnableKeyword("INFOMODE_FIRE_SAFETY");
			Shader.DisableKeyword("INFOMODE_IMMATERIAL");
		}
		else if (resource != ImmaterialResourceManager.Resource.None)
		{
			Shader.DisableKeyword("INFOMODE_FIRE_SAFETY");
			Shader.EnableKeyword("INFOMODE_IMMATERIAL");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_FIRE_SAFETY");
			Shader.DisableKeyword("INFOMODE_IMMATERIAL");
		}
		if (flag7)
		{
			Shader.EnableKeyword("INFOMODE_DISTRICTS");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_DISTRICTS");
		}
		if (num2 != 0)
		{
			Shader.EnableKeyword("INFOMODE_TRANSPORT");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_TRANSPORT");
		}
		if (flag9)
		{
			Shader.EnableKeyword("INFOMODE_NATURAL");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_NATURAL");
		}
		if (flag11)
		{
			Shader.EnableKeyword("INFOMODE_TERRAIN");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_TERRAIN");
		}
		if (num != 0)
		{
			Shader.EnableKeyword("INFOMODE_DISASTER_HAZARD");
		}
		else
		{
			Shader.DisableKeyword("INFOMODE_DISASTER_HAZARD");
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("InfoManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.m_visitedInfoViews = 0u;
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new InfoManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	private InfoManager.InfoMode m_currentMode;

	private InfoManager.SubInfoMode m_currentSubMode;

	private InfoManager.InfoMode m_actualMode;

	private InfoManager.SubInfoMode m_actualSubMode;

	private CameraController m_cameraController;

	private uint m_visitedInfoViews;

	private uint m_gameInfoViews;

	public enum InfoMode
	{
		None,
		[EnumPosition("Game", "Electricity", 0)]
		Electricity,
		[EnumPosition("MapEditor", "Water", 0), EnumPosition("Game", "Water", 1)]
		Water,
		[EnumPosition("Game", "Crime", 12)]
		CrimeRate,
		[EnumPosition("Game", "Health", 5)]
		Health,
		[EnumPosition("Game", "Happiness", 4)]
		Happiness,
		[EnumPosition("Game", "Population", 14)]
		Density,
		[EnumPosition("Game", "NoisePollution", 10)]
		NoisePollution,
		[EnumPosition("MapEditor", "PublicTransport", 3), EnumPosition("Game", "PublicTransport", 13)]
		Transport,
		[EnumPosition("Game", "Pollution", 9)]
		Pollution,
		[EnumPosition("MapEditor", "Resources", 1), EnumPosition("Game", "Resources", 17)]
		NaturalResources,
		[EnumPosition("Game", "LandValue", 16)]
		LandValue,
		[EnumPosition("Game", "Districts", 18)]
		Districts,
		[EnumPosition("Game", "OutsideConnections", 15)]
		Connections,
		[EnumPosition("Game", "TrafficCongestion", 8)]
		Traffic,
		[EnumPosition("MapEditor", "WindSpeed", 2), EnumPosition("Game", "WindSpeed", 7)]
		Wind,
		[EnumPosition("Game", "Garbage", 2)]
		Garbage,
		[EnumPosition("Game", "Level", 6)]
		BuildingLevel,
		[EnumPosition("Game", "FireSafety", 11)]
		FireSafety,
		[EnumPosition("Game", "Education", 3)]
		Education,
		[EnumPosition("Game", "Entertainment", 19)]
		Entertainment,
		[EnumPosition("MapEditor", "TerrainHeight", 4), EnumPosition("Game", "TerrainHeight", 20)]
		TerrainHeight,
		[EnumPosition("Game", "Heating", 21)]
		Heating,
		[EnumPosition("Game", "Maintenance", 22)]
		Maintenance,
		[EnumPosition("Game", "Snow", 23)]
		Snow,
		[EnumPosition("Game", "EscapeRoutes", 24)]
		EscapeRoutes,
		[EnumPosition("Game", "Radio", 25)]
		Radio,
		[EnumPosition("Game", "Destruction", 26)]
		Destruction,
		[EnumPosition("Game", "DisasterDetection", 27)]
		DisasterDetection,
		DisasterHazard,
		[EnumPosition("AssetEditor", "TrafficRoutes", 1), EnumPosition("Game", "TrafficRoutes", 28)]
		TrafficRoutes,
		Underground
	}

	public enum SubInfoMode
	{
		Default,
		NormalPower = 0,
		WaterPower,
		WindPower,
		NormalWater = 0,
		FlowingWater,
		GroundWater,
		PipeWater,
		HealthCare = 0,
		DeathCare,
		ElementarySchool = 0,
		HighSchool,
		University,
		Import = 0,
		Export,
		Tourism,
		Beautification = 0,
		Monuments,
		NormalTransport = 0,
		WaterTransport,
		Police = 0,
		Prisons,
		MaintenanceDepots = 0,
		RoadCondition,
		SnowDumps = 0,
		RoadSnow,
		DisasterDetection = 0,
		TsunamiDetection,
		EarthquakeDetection,
		MeteorDetection,
		WeatherDetection,
		FloodHazard = 0,
		LightningHazard,
		MeteorHazard,
		SinkholeHazard,
		TornadoHazard,
		EarthquakeHazard,
		ForestFireHazard,
		Paths = 0,
		JunctionSettings,
		AdjustRoad,
		UndergroundTunnels = 0,
		UndergroundPipes,
		NormalGarbage = 0,
		FlowingGarbage
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "InfoManager");
			InfoManager instance = Singleton<InfoManager>.get_instance();
			s.WriteUInt32(instance.m_visitedInfoViews);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "InfoManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "InfoManager");
			InfoManager instance = Singleton<InfoManager>.get_instance();
			instance.m_visitedInfoViews = s.ReadUInt32();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "InfoManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "InfoManager");
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "InfoManager");
		}
	}
}
