﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class AreasWrapper : IAreas
{
	public AreasWrapper(GameAreaManager gameAreaManager)
	{
		this.m_gameAreaManager = gameAreaManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnAreasExtensionsReleased();
		this.m_AreasExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IAreasExtension>();
		this.OnAreasExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnAreasExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnAreasExtensionsCreated()
	{
		for (int i = 0; i < this.m_AreasExtensions.Count; i++)
		{
			try
			{
				this.m_AreasExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_AreasExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnAreasExtensionsReleased()
	{
		for (int i = 0; i < this.m_AreasExtensions.Count; i++)
		{
			try
			{
				this.m_AreasExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_AreasExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnCanUnlockArea(int x, int z, ref bool result)
	{
		for (int i = 0; i < this.m_AreasExtensions.Count; i++)
		{
			result = this.m_AreasExtensions[i].OnCanUnlockArea(x, z, result);
		}
	}

	public void OnGetAreaPrice(uint ore, uint oil, uint forest, uint fertility, uint water, bool road, bool train, bool ship, bool plane, float landFlatness, ref int price)
	{
		for (int i = 0; i < this.m_AreasExtensions.Count; i++)
		{
			price = this.m_AreasExtensions[i].OnGetAreaPrice(ore, oil, forest, fertility, water, road, train, ship, plane, landFlatness, price);
		}
	}

	public void OnUnlockArea(int x, int z)
	{
		for (int i = 0; i < this.m_AreasExtensions.Count; i++)
		{
			this.m_AreasExtensions[i].OnUnlockArea(x, z);
		}
	}

	public int startTileX
	{
		get
		{
			int result;
			int num;
			this.m_gameAreaManager.GetStartTile(out result, out num);
			return result;
		}
	}

	public int startTileZ
	{
		get
		{
			int num;
			int result;
			this.m_gameAreaManager.GetStartTile(out num, out result);
			return result;
		}
	}

	public int unlockedAreaCount
	{
		get
		{
			return this.m_gameAreaManager.m_areaCount;
		}
	}

	public int maxAreaCount
	{
		get
		{
			return this.m_gameAreaManager.m_maxAreaCount;
		}
		set
		{
			int num = Mathf.Clamp(value, 1, 25);
			if (this.m_gameAreaManager.m_maxAreaCount != num)
			{
				this.m_gameAreaManager.m_maxAreaCount = num;
				if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					if (Dispatcher.get_currentSafe() == ThreadHelper.get_dispatcher())
					{
						Singleton<UnlockManager>.get_instance().MilestonesUpdated();
					}
					else
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							Singleton<UnlockManager>.get_instance().MilestonesUpdated();
						});
					}
				}
			}
		}
	}

	public bool IsAreaUnlocked(int x, int z)
	{
		return this.m_gameAreaManager.IsUnlocked(x, z);
	}

	public bool CanUnlockArea(int x, int z)
	{
		return this.m_gameAreaManager.CanUnlock(x, z);
	}

	public int GetAreaPrice(int x, int z)
	{
		int tileIndex = this.m_gameAreaManager.GetTileIndex(x, z);
		return this.m_gameAreaManager.CalculateTilePrice(tileIndex);
	}

	public void UnlockArea(int x, int z, bool requireMoney)
	{
		if (Thread.CurrentThread == Singleton<SimulationManager>.get_instance().m_simulationThread)
		{
			this.UnlockAreaImpl(x, z, requireMoney);
		}
		else
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.UnlockAreaAction(x, z, requireMoney));
		}
	}

	[DebuggerHidden]
	private IEnumerator UnlockAreaAction(int x, int z, bool requireMoney)
	{
		AreasWrapper.<UnlockAreaAction>c__Iterator0 <UnlockAreaAction>c__Iterator = new AreasWrapper.<UnlockAreaAction>c__Iterator0();
		<UnlockAreaAction>c__Iterator.x = x;
		<UnlockAreaAction>c__Iterator.z = z;
		<UnlockAreaAction>c__Iterator.requireMoney = requireMoney;
		<UnlockAreaAction>c__Iterator.$this = this;
		return <UnlockAreaAction>c__Iterator;
	}

	private void UnlockAreaImpl(int x, int z, bool requireMoney)
	{
		int tileIndex = this.m_gameAreaManager.GetTileIndex(x, z);
		int num = 0;
		if (requireMoney)
		{
			num = this.m_gameAreaManager.CalculateTilePrice(tileIndex);
		}
		if (num != 0)
		{
			if (Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.LandPrice, num, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1) == num)
			{
				this.m_gameAreaManager.UnlockArea(tileIndex);
			}
		}
		else
		{
			this.m_gameAreaManager.UnlockArea(tileIndex);
		}
	}

	private GameAreaManager m_gameAreaManager;

	private List<IAreasExtension> m_AreasExtensions = new List<IAreasExtension>();
}
