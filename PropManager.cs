﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class PropManager : SimulationManagerBase<PropManager, PropProperties>, ISimulationManager, IRenderableManager, ITerrainManager
{
	public bool MarkersVisible
	{
		get
		{
			return this.m_markersVisible;
		}
		set
		{
			this.m_markersVisible = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_props = new Array16<PropInstance>(65536u);
		this.m_updatedProps = new ulong[1024];
		this.m_propGrid = new ushort[72900];
		this.m_automaticProps = new FastList<ushort>[20];
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_ObjectIndex = Shader.PropertyToID("_ObjectIndex");
		this.ID_HeightMap = Shader.PropertyToID("_HeightMap");
		this.ID_HeightMapping = Shader.PropertyToID("_HeightMapping");
		this.ID_SurfaceMapping = Shader.PropertyToID("_SurfaceMapping");
		this.ID_WaterHeightMap = Shader.PropertyToID("_WaterHeightMap");
		this.ID_WaterHeightMapping = Shader.PropertyToID("_WaterHeightMapping");
		this.ID_WaterSurfaceMapping = Shader.PropertyToID("_WaterSurfaceMapping");
		this.ID_MarkerAlpha = Shader.PropertyToID("_MarkerAlpha");
		this.ID_MainTex = Shader.PropertyToID("_MainTex");
		this.ID_XYSMap = Shader.PropertyToID("_XYSMap");
		this.ID_ACIMap = Shader.PropertyToID("_ACIMap");
		this.ID_AtlasRect = Shader.PropertyToID("_AtlasRect");
		this.ID_PropLocation = Shader.PropertyToID("_PropLocation");
		this.ID_PropObjectIndex = Shader.PropertyToID("_PropObjectIndex");
		this.ID_PropColor = Shader.PropertyToID("_PropColor");
		this.ID_RollLocation = Shader.PropertyToID("_RollLocation");
		this.ID_RollParams = Shader.PropertyToID("_RollParams");
		this.m_propLayer = LayerMask.NameToLayer("Props");
		this.m_markerLayer = LayerMask.NameToLayer("Markers");
		this.m_markersVisible = true;
		ushort num;
		this.m_props.CreateItem(out num);
	}

	private void OnDestroy()
	{
		if (this.m_lodRgbAtlas != null)
		{
			Object.Destroy(this.m_lodRgbAtlas);
			this.m_lodRgbAtlas = null;
		}
		if (this.m_lodXysAtlas != null)
		{
			Object.Destroy(this.m_lodXysAtlas);
			this.m_lodXysAtlas = null;
		}
		if (this.m_lodAciAtlas != null)
		{
			Object.Destroy(this.m_lodAciAtlas);
			this.m_lodAciAtlas = null;
		}
	}

	public override void InitializeProperties(PropProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_properties.m_placementEffect != null)
		{
			this.m_properties.m_placementEffect.InitializeEffect();
		}
		if (this.m_properties.m_bulldozeEffect != null)
		{
			this.m_properties.m_bulldozeEffect.InitializeEffect();
		}
	}

	public override void DestroyProperties(PropProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_properties.m_placementEffect != null)
			{
				this.m_properties.m_placementEffect.ReleaseEffect();
			}
			if (this.m_properties.m_bulldozeEffect != null)
			{
				this.m_properties.m_bulldozeEffect.ReleaseEffect();
			}
			if (this.m_lodRgbAtlas != null)
			{
				Object.Destroy(this.m_lodRgbAtlas);
				this.m_lodRgbAtlas = null;
			}
			if (this.m_lodXysAtlas != null)
			{
				Object.Destroy(this.m_lodXysAtlas);
				this.m_lodXysAtlas = null;
			}
			if (this.m_lodAciAtlas != null)
			{
				Object.Destroy(this.m_lodAciAtlas);
				this.m_lodAciAtlas = null;
			}
		}
		base.DestroyProperties(properties);
	}

	private void Update()
	{
		if (this.m_markersVisible)
		{
			this.m_markerAlpha = Mathf.Min(1f, this.m_markerAlpha + Time.get_deltaTime() * 2f);
		}
		else
		{
			this.m_markerAlpha = Mathf.Max(0f, this.m_markerAlpha - Time.get_deltaTime() * 2f);
		}
	}

	public override void CheckReferences()
	{
		base.CheckReferences();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.CheckReferencesImpl());
	}

	[DebuggerHidden]
	private IEnumerator CheckReferencesImpl()
	{
		return new PropManager.<CheckReferencesImpl>c__Iterator0();
	}

	public override void InitRenderData()
	{
		base.InitRenderData();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitRenderDataImpl());
	}

	[DebuggerHidden]
	private IEnumerator InitRenderDataImpl()
	{
		PropManager.<InitRenderDataImpl>c__Iterator1 <InitRenderDataImpl>c__Iterator = new PropManager.<InitRenderDataImpl>c__Iterator1();
		<InitRenderDataImpl>c__Iterator.$this = this;
		return <InitRenderDataImpl>c__Iterator;
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
		{
			Shader.SetGlobalFloat(this.ID_MarkerAlpha, this.m_markerAlpha);
			if (this.m_markerAlpha < 0.001f)
			{
				cameraInfo.m_layerMask &= ~(1 << this.m_markerLayer);
			}
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		FastList<RenderGroup> renderedGroups = Singleton<RenderManager>.get_instance().m_renderedGroups;
		int num = 1 << this.m_propLayer | 1 << Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer;
		if ((mode & ItemClass.Availability.Game) == ItemClass.Availability.None && this.m_markerAlpha >= 0.001f)
		{
			num |= 1 << this.m_markerLayer;
		}
		for (int i = 0; i < renderedGroups.m_size; i++)
		{
			RenderGroup renderGroup = renderedGroups.m_buffer[i];
			if ((renderGroup.m_instanceMask & num) != 0)
			{
				int num2 = renderGroup.m_x * 270 / 45;
				int num3 = renderGroup.m_z * 270 / 45;
				int num4 = (renderGroup.m_x + 1) * 270 / 45 - 1;
				int num5 = (renderGroup.m_z + 1) * 270 / 45 - 1;
				for (int j = num3; j <= num5; j++)
				{
					for (int k = num2; k <= num4; k++)
					{
						int num6 = j * 270 + k;
						ushort num7 = this.m_propGrid[num6];
						int num8 = 0;
						while (num7 != 0)
						{
							this.m_props.m_buffer[(int)num7].RenderInstance(cameraInfo, num7, renderGroup.m_instanceMask);
							num7 = this.m_props.m_buffer[(int)num7].m_nextGridProp;
							if (++num8 >= 65536)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
		}
		int num9 = PrefabCollection<PropInfo>.PrefabCount();
		for (int l = 0; l < num9; l++)
		{
			PropInfo prefab = PrefabCollection<PropInfo>.GetPrefab((uint)l);
			if (prefab != null && prefab.m_lodCount != 0)
			{
				PropInstance.RenderLod(cameraInfo, prefab);
			}
		}
	}

	public float SampleSmoothHeight(Vector3 worldPos)
	{
		float num = 0f;
		int num2 = Mathf.Max((int)((worldPos.x - 32f) / 64f + 135f), 0);
		int num3 = Mathf.Max((int)((worldPos.z - 32f) / 64f + 135f), 0);
		int num4 = Mathf.Min((int)((worldPos.x + 32f) / 64f + 135f), 269);
		int num5 = Mathf.Min((int)((worldPos.z + 32f) / 64f + 135f), 269);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = this.m_propGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					if (!this.m_props.m_buffer[(int)num6].Blocked)
					{
						Vector3 position = this.m_props.m_buffer[(int)num6].Position;
						Vector3 vector = worldPos - position;
						float num8 = 1024f;
						float num9 = vector.x * vector.x + vector.z * vector.z;
						if (num9 < num8)
						{
							PropInfo info = this.m_props.m_buffer[(int)num6].Info;
							float num10 = MathUtils.SmoothClamp01(1f - Mathf.Sqrt(num9 / num8));
							num10 = Mathf.Lerp(worldPos.y, position.y + info.m_generatedInfo.m_size.y * 1.25f, num10);
							if (num10 > num)
							{
								num = num10;
							}
						}
					}
					num6 = this.m_props.m_buffer[(int)num6].m_nextGridProp;
					if (++num7 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num;
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (this.m_propCount >= 50000)
			{
				return false;
			}
		}
		else if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_propCount + Singleton<TreeManager>.get_instance().m_treeCount >= 64)
			{
				return false;
			}
		}
		else if (this.m_propCount >= 65531)
		{
			return false;
		}
		return true;
	}

	public bool CreateProp(out ushort prop, ref Randomizer randomizer, PropInfo info, Vector3 position, float angle, bool single)
	{
		if (!this.CheckLimits())
		{
			prop = 0;
			return false;
		}
		ushort num;
		if (this.m_props.CreateItem(out num, ref randomizer))
		{
			prop = num;
			this.m_props.m_buffer[(int)prop].m_flags = 1;
			this.m_props.m_buffer[(int)prop].Info = info;
			this.m_props.m_buffer[(int)prop].Single = single;
			this.m_props.m_buffer[(int)prop].Blocked = false;
			this.m_props.m_buffer[(int)prop].Position = position;
			this.m_props.m_buffer[(int)prop].Angle = angle;
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			this.InitializeProp(prop, ref this.m_props.m_buffer[(int)prop], (mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None);
			this.UpdateProp(prop);
			this.m_propCount = (int)(this.m_props.ItemCount() - 1u);
			return true;
		}
		prop = 0;
		return false;
	}

	public void ReleaseProp(ushort prop)
	{
		this.ReleasePropImplementation(prop, ref this.m_props.m_buffer[(int)prop]);
	}

	private void InitializeProp(ushort prop, ref PropInstance data, bool assetEditor)
	{
		int num;
		int num2;
		if (assetEditor)
		{
			num = Mathf.Clamp(((int)(data.m_posX / 16) + 32768) * 270 / 65536, 0, 269);
			num2 = Mathf.Clamp(((int)(data.m_posZ / 16) + 32768) * 270 / 65536, 0, 269);
		}
		else
		{
			num = Mathf.Clamp(((int)data.m_posX + 32768) * 270 / 65536, 0, 269);
			num2 = Mathf.Clamp(((int)data.m_posZ + 32768) * 270 / 65536, 0, 269);
		}
		int num3 = num2 * 270 + num;
		while (!Monitor.TryEnter(this.m_propGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_props.m_buffer[(int)prop].m_nextGridProp = this.m_propGrid[num3];
			this.m_propGrid[num3] = prop;
		}
		finally
		{
			Monitor.Exit(this.m_propGrid);
		}
	}

	private void FinalizeProp(ushort prop, ref PropInstance data)
	{
		int num;
		int num2;
		if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
		{
			num = Mathf.Clamp(((int)(data.m_posX / 16) + 32768) * 270 / 65536, 0, 269);
			num2 = Mathf.Clamp(((int)(data.m_posZ / 16) + 32768) * 270 / 65536, 0, 269);
		}
		else
		{
			num = Mathf.Clamp(((int)data.m_posX + 32768) * 270 / 65536, 0, 269);
			num2 = Mathf.Clamp(((int)data.m_posZ + 32768) * 270 / 65536, 0, 269);
		}
		int num3 = num2 * 270 + num;
		while (!Monitor.TryEnter(this.m_propGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			ushort num4 = 0;
			ushort num5 = this.m_propGrid[num3];
			int num6 = 0;
			while (num5 != 0)
			{
				if (num5 == prop)
				{
					if (num4 == 0)
					{
						this.m_propGrid[num3] = data.m_nextGridProp;
					}
					else
					{
						this.m_props.m_buffer[(int)num4].m_nextGridProp = data.m_nextGridProp;
					}
					break;
				}
				num4 = num5;
				num5 = this.m_props.m_buffer[(int)num5].m_nextGridProp;
				if (++num6 > 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridProp = 0;
		}
		finally
		{
			Monitor.Exit(this.m_propGrid);
		}
		int x = num * 45 / 270;
		int z = num2 * 45 / 270;
		Singleton<RenderManager>.get_instance().UpdateGroup(x, z, this.m_propLayer);
		PropInfo info = data.Info;
		if (info != null && info.m_effectLayer != -1)
		{
			Singleton<RenderManager>.get_instance().UpdateGroup(x, z, info.m_effectLayer);
		}
	}

	public void MoveProp(ushort prop, Vector3 position)
	{
		this.MovePropImplementation(prop, ref this.m_props.m_buffer[(int)prop], position);
	}

	private void MovePropImplementation(ushort prop, ref PropInstance data, Vector3 position)
	{
		if (data.m_flags != 0)
		{
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			this.FinalizeProp(prop, ref data);
			data.Position = position;
			this.InitializeProp(prop, ref data, (mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None);
			this.UpdateProp(prop);
		}
	}

	private void ReleasePropImplementation(ushort prop, ref PropInstance data)
	{
		if (data.m_flags != 0)
		{
			InstanceID id = default(InstanceID);
			id.Prop = prop;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags |= 2;
			data.UpdateProp(prop);
			this.UpdatePropRenderer(prop, true);
			data.m_flags = 0;
			this.FinalizeProp(prop, ref data);
			this.m_props.ReleaseItem(prop);
			this.m_propCount = (int)(this.m_props.ItemCount() - 1u);
		}
	}

	public void UpdateProp(ushort prop)
	{
		this.m_updatedProps[prop >> 6] |= 1uL << (int)prop;
		this.m_propsUpdated = true;
	}

	public void UpdateProps(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)((minX - 8f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((minZ - 8f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((maxX + 8f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((maxZ + 8f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_propGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_props.m_buffer[(int)num5].Position;
					float num7 = Mathf.Max(Mathf.Max(minX - 8f - position.x, minZ - 8f - position.z), Mathf.Max(position.x - maxX - 8f, position.z - maxZ - 8f));
					if (num7 < 0f)
					{
						this.m_updatedProps[num5 >> 6] |= 1uL << (int)num5;
						this.m_propsUpdated = true;
					}
					num5 = this.m_props.m_buffer[(int)num5].m_nextGridProp;
					if (++num6 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void UpdatePropRenderer(ushort prop, bool updateGroup)
	{
		if (this.m_props.m_buffer[(int)prop].m_flags == 0)
		{
			return;
		}
		if (updateGroup)
		{
			int num;
			int num2;
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				num = Mathf.Clamp(((int)(this.m_props.m_buffer[(int)prop].m_posX / 16) + 32768) * 270 / 65536, 0, 269);
				num2 = Mathf.Clamp(((int)(this.m_props.m_buffer[(int)prop].m_posZ / 16) + 32768) * 270 / 65536, 0, 269);
			}
			else
			{
				num = Mathf.Clamp(((int)this.m_props.m_buffer[(int)prop].m_posX + 32768) * 270 / 65536, 0, 269);
				num2 = Mathf.Clamp(((int)this.m_props.m_buffer[(int)prop].m_posZ + 32768) * 270 / 65536, 0, 269);
			}
			int x = num * 45 / 270;
			int z = num2 * 45 / 270;
			PropInfo info = this.m_props.m_buffer[(int)prop].Info;
			if (info != null && info.m_prefabDataLayer != -1)
			{
				Singleton<RenderManager>.get_instance().UpdateGroup(x, z, info.m_prefabDataLayer);
			}
			if (info != null && info.m_effectLayer != -1)
			{
				Singleton<RenderManager>.get_instance().UpdateGroup(x, z, info.m_effectLayer);
			}
		}
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, int layer, ushort ignoreProp)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		int num = Mathf.Max((int)((vector.x - 8f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.y - 8f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 8f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.y + 8f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_propGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_props.m_buffer[(int)num5].Position;
					float num7 = Mathf.Max(Mathf.Max(vector.x - 8f - position.x, vector.y - 8f - position.z), Mathf.Max(position.x - vector2.x - 8f, position.z - vector2.y - 8f));
					if (num7 < 0f && this.m_props.m_buffer[(int)num5].OverlapQuad(num5, quad, minY, maxY, collisionType))
					{
						return true;
					}
					num5 = this.m_props.m_buffer[(int)num5].m_nextGridProp;
					if (++num6 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return false;
	}

	public bool RayCast(Segment3 ray, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers, PropInstance.Flags ignoreFlags, out Vector3 hit, out ushort propIndex)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = (int)(ray.a.x / 64f + 135f);
			int num2 = (int)(ray.a.z / 64f + 135f);
			int num3 = (int)(ray.b.x / 64f + 135f);
			int num4 = (int)(ray.b.z / 64f + 135f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 64f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 64f / num6;
				}
			}
			float num9 = 2f;
			float num10 = 10000f;
			propIndex = 0;
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			int num11 = num;
			int num12 = num2;
			do
			{
				Vector3 vector4 = vector3 + vector;
				int num13;
				int num14;
				int num15;
				int num16;
				if (num7 != 0)
				{
					if ((num11 == num && num7 > 0) || (num11 == num3 && num7 < 0))
					{
						num13 = Mathf.Max((int)((vector4.x - 72f) / 64f + 135f), 0);
					}
					else
					{
						num13 = Mathf.Max(num11, 0);
					}
					if ((num11 == num && num7 < 0) || (num11 == num3 && num7 > 0))
					{
						num14 = Mathf.Min((int)((vector4.x + 72f) / 64f + 135f), 269);
					}
					else
					{
						num14 = Mathf.Min(num11, 269);
					}
					num15 = Mathf.Max((int)((Mathf.Min(vector2.z, vector4.z) - 72f) / 64f + 135f), 0);
					num16 = Mathf.Min((int)((Mathf.Max(vector2.z, vector4.z) + 72f) / 64f + 135f), 269);
				}
				else
				{
					if ((num12 == num2 && num8 > 0) || (num12 == num4 && num8 < 0))
					{
						num15 = Mathf.Max((int)((vector4.z - 72f) / 64f + 135f), 0);
					}
					else
					{
						num15 = Mathf.Max(num12, 0);
					}
					if ((num12 == num2 && num8 < 0) || (num12 == num4 && num8 > 0))
					{
						num16 = Mathf.Min((int)((vector4.z + 72f) / 64f + 135f), 269);
					}
					else
					{
						num16 = Mathf.Min(num12, 269);
					}
					num13 = Mathf.Max((int)((Mathf.Min(vector2.x, vector4.x) - 72f) / 64f + 135f), 0);
					num14 = Mathf.Min((int)((Mathf.Max(vector2.x, vector4.x) + 72f) / 64f + 135f), 269);
				}
				for (int i = num15; i <= num16; i++)
				{
					for (int j = num13; j <= num14; j++)
					{
						ushort num17 = this.m_propGrid[i * 270 + j];
						int num18 = 0;
						while (num17 != 0)
						{
							PropInstance.Flags flags = (PropInstance.Flags)this.m_props.m_buffer[(int)num17].m_flags;
							if ((flags & ignoreFlags) == PropInstance.Flags.None && ray.DistanceSqr(this.m_props.m_buffer[(int)num17].Position) < 900f)
							{
								PropInfo info = this.m_props.m_buffer[(int)num17].Info;
								float num19;
								float num20;
								if ((service == ItemClass.Service.None || info.m_class.m_service == service) && (subService == ItemClass.SubService.None || info.m_class.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_class.m_layer & itemLayers) != ItemClass.Layer.None) && this.m_props.m_buffer[(int)num17].RayCast(num17, ray, out num19, out num20) && (num19 < num9 - 0.0001f || (num19 < num9 + 0.0001f && num20 < num10)))
								{
									num9 = num19;
									num10 = num20;
									propIndex = num17;
								}
							}
							num17 = this.m_props.m_buffer[(int)num17].m_nextGridProp;
							if (++num18 > 65536)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector2 = vector3;
				vector3 = vector4;
				num11 += num7;
				num12 += num8;
			}
			while ((num11 <= num3 || num7 <= 0) && (num11 >= num3 || num7 >= 0) && (num12 <= num4 || num8 <= 0) && (num12 >= num4 || num8 >= 0));
			if (num9 != 2f)
			{
				hit = ray.Position(num9);
				return true;
			}
		}
		hit = Vector3.get_zero();
		propIndex = 0;
		return false;
	}

	private static int GetPropIndex(ItemClass.Service service)
	{
		return service - ItemClass.Service.Residential;
	}

	public PropInfo GetRandomPropInfo(ref Randomizer r, ItemClass.Service service)
	{
		if (!this.m_propsRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random props not refreshed yet!");
			return null;
		}
		int num = PropManager.GetPropIndex(service);
		FastList<ushort> fastList = this.m_automaticProps[num];
		if (fastList == null)
		{
			return null;
		}
		if (fastList.m_size == 0)
		{
			return null;
		}
		num = r.Int32((uint)fastList.m_size);
		return PrefabCollection<PropInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	private void RefreshAutomaticProps()
	{
		int num = this.m_automaticProps.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_automaticProps[i] = null;
		}
		int num2 = PrefabCollection<PropInfo>.PrefabCount();
		for (int j = 0; j < num2; j++)
		{
			PropInfo prefab = PrefabCollection<PropInfo>.GetPrefab((uint)j);
			if (prefab != null && prefab.m_class != null && prefab.m_class.m_service != ItemClass.Service.None && prefab.m_placementStyle == ItemClass.Placement.Automatic)
			{
				if (prefab.m_requireHeightMap && prefab.m_class.m_service != ItemClass.Service.Disaster)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
					{
						"Automatic prop category cannot require heightmap (",
						prefab.m_class.m_service.ToString(),
						"->",
						PrefabCollection<PropInfo>.PrefabName((uint)j),
						")"
					}));
				}
				else
				{
					int propIndex = PropManager.GetPropIndex(prefab.m_class.m_service);
					if (this.m_automaticProps[propIndex] == null)
					{
						this.m_automaticProps[propIndex] = new FastList<ushort>();
					}
					this.m_automaticProps[propIndex].Add((ushort)j);
				}
			}
		}
		this.m_propsRefreshed = true;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_propsUpdated)
		{
			int num = this.m_updatedProps.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedProps[i];
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							ushort num3 = (ushort)(i << 6 | j);
							this.m_props.m_buffer[(int)num3].CalculateProp(num3);
						}
					}
				}
			}
			this.m_propsUpdated = false;
			for (int k = 0; k < num; k++)
			{
				ulong num4 = this.m_updatedProps[k];
				if (num4 != 0uL)
				{
					this.m_updatedProps[k] = 0uL;
					for (int l = 0; l < 64; l++)
					{
						if ((num4 & 1uL << l) != 0uL)
						{
							ushort num5 = (ushort)(k << 6 | l);
							this.m_props.m_buffer[(int)num5].UpdateProp(num5);
							this.UpdatePropRenderer(num5, true);
						}
					}
				}
			}
		}
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float x = surfaceArea.m_min.x;
		float z = surfaceArea.m_min.z;
		float x2 = surfaceArea.m_max.x;
		float z2 = surfaceArea.m_max.z;
		int num = Mathf.Max((int)((x - 8f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((z - 8f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((x2 + 8f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((z2 + 8f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_propGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_props.m_buffer[(int)num5].Position;
					float num7 = Mathf.Max(Mathf.Max(x - 8f - position.x, z - 8f - position.z), Mathf.Max(position.x - x2 - 8f, position.z - z2 - 8f));
					if (num7 < 0f)
					{
						this.m_props.m_buffer[(int)num5].TerrainUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_props.m_buffer[(int)num5].m_nextGridProp;
					if (++num6 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float x = heightArea.m_min.x;
		float z = heightArea.m_min.z;
		float x2 = heightArea.m_max.x;
		float z2 = heightArea.m_max.z;
		int num = Mathf.Max((int)((x - 8f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((z - 8f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((x2 + 8f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((z2 + 8f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_propGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_props.m_buffer[(int)num5].Position;
					float num7 = Mathf.Max(Mathf.Max(x - 8f - position.x, z - 8f - position.z), Mathf.Max(position.x - x2 - 8f, position.z - z2 - 8f));
					if (num7 < 0f)
					{
						this.m_props.m_buffer[(int)num5].AfterTerrainUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_props.m_buffer[(int)num5].m_nextGridProp;
					if (++num6 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		if (layer != this.m_propLayer && layer != this.m_markerLayer && layer != Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer)
		{
			return result;
		}
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_propGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					if (this.m_props.m_buffer[(int)num6].CalculateGroupData(num6, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
					num6 = this.m_props.m_buffer[(int)num6].m_nextGridProp;
					if (++num7 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		if (layer != this.m_propLayer && layer != this.m_markerLayer && layer != Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer)
		{
			return;
		}
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_propGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					this.m_props.m_buffer[(int)num6].PopulateGroupData(num6, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
					num6 = this.m_props.m_buffer[(int)num6].m_nextGridProp;
					if (++num7 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("PropManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 65536; i++)
		{
			if (this.m_props.m_buffer[i].m_flags != 0 && this.m_props.m_buffer[i].Info == null)
			{
				this.ReleaseProp((ushort)i);
			}
		}
		this.m_infoCount = PrefabCollection<PropInfo>.PrefabCount();
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new PropManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float PROPGRID_CELL_SIZE = 64f;

	public const int PROPGRID_RESOLUTION = 270;

	public const int MAX_PROP_COUNT = 65536;

	public const int MAX_MAP_PROPS = 50000;

	public const int MAX_ASSET_PROPS = 64;

	public int m_propCount;

	public int m_infoCount;

	[NonSerialized]
	public Array16<PropInstance> m_props;

	[NonSerialized]
	public ulong[] m_updatedProps;

	[NonSerialized]
	public bool m_propsUpdated;

	[NonSerialized]
	public ushort[] m_propGrid;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_ObjectIndex;

	[NonSerialized]
	public int ID_PropLocation;

	[NonSerialized]
	public int ID_PropObjectIndex;

	[NonSerialized]
	public int ID_PropColor;

	[NonSerialized]
	public int ID_HeightMap;

	[NonSerialized]
	public int ID_HeightMapping;

	[NonSerialized]
	public int ID_SurfaceMapping;

	[NonSerialized]
	public int ID_WaterHeightMap;

	[NonSerialized]
	public int ID_WaterHeightMapping;

	[NonSerialized]
	public int ID_WaterSurfaceMapping;

	[NonSerialized]
	public int ID_MarkerAlpha;

	[NonSerialized]
	public int ID_MainTex;

	[NonSerialized]
	public int ID_XYSMap;

	[NonSerialized]
	public int ID_ACIMap;

	[NonSerialized]
	public int ID_AtlasRect;

	[NonSerialized]
	public int ID_RollLocation;

	[NonSerialized]
	public int ID_RollParams;

	public Texture2D m_lodRgbAtlas;

	public Texture2D m_lodXysAtlas;

	public Texture2D m_lodAciAtlas;

	private FastList<ushort>[] m_automaticProps;

	private float m_markerAlpha;

	private int m_propLayer;

	private int m_markerLayer;

	private bool m_markersVisible;

	private bool m_propsRefreshed;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "PropManager");
			PropManager instance = Singleton<PropManager>.get_instance();
			PropInstance[] buffer = instance.m_props.m_buffer;
			int num = buffer.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uShort.Write(buffer[i].m_flags);
			}
			uShort.EndWrite();
			try
			{
				PrefabCollection<PropInfo>.BeginSerialize(s);
				for (int j = 1; j < num; j++)
				{
					if (buffer[j].m_flags != 0)
					{
						PrefabCollection<PropInfo>.Serialize((uint)buffer[j].m_infoIndex);
					}
				}
			}
			finally
			{
				PrefabCollection<PropInfo>.EndSerialize(s);
			}
			EncodedArray.Short @short = EncodedArray.Short.BeginWrite(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != 0)
				{
					@short.Write(buffer[k].m_posX);
				}
			}
			@short.EndWrite();
			EncodedArray.Short short2 = EncodedArray.Short.BeginWrite(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != 0)
				{
					short2.Write(buffer[l].m_posZ);
				}
			}
			short2.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int m = 1; m < num; m++)
			{
				if (buffer[m].m_flags != 0)
				{
					uShort2.Write(buffer[m].m_angle);
				}
			}
			uShort2.EndWrite();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "PropManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "PropManager");
			PropManager instance = Singleton<PropManager>.get_instance();
			PropInstance[] buffer = instance.m_props.m_buffer;
			ushort[] propGrid = instance.m_propGrid;
			int num = buffer.Length;
			int num2 = propGrid.Length;
			instance.m_props.ClearUnused();
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			bool assetEditor = updateMode == SimulationManager.UpdateMode.NewAsset || updateMode == SimulationManager.UpdateMode.LoadAsset;
			for (int i = 0; i < num2; i++)
			{
				propGrid[i] = 0;
			}
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int j = 1; j < num; j++)
			{
				buffer[j].m_flags = uShort.Read();
			}
			uShort.EndRead();
			PrefabCollection<PropInfo>.BeginDeserialize(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != 0)
				{
					buffer[k].m_infoIndex = (ushort)PrefabCollection<PropInfo>.Deserialize(true);
				}
			}
			PrefabCollection<PropInfo>.EndDeserialize(s);
			EncodedArray.Short @short = EncodedArray.Short.BeginRead(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != 0)
				{
					buffer[l].m_posX = @short.Read();
				}
				else
				{
					buffer[l].m_posX = 0;
				}
			}
			@short.EndRead();
			EncodedArray.Short short2 = EncodedArray.Short.BeginRead(s);
			for (int m = 1; m < num; m++)
			{
				if (buffer[m].m_flags != 0)
				{
					buffer[m].m_posZ = short2.Read();
				}
				else
				{
					buffer[m].m_posZ = 0;
				}
			}
			short2.EndRead();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int n = 1; n < num; n++)
			{
				if (buffer[n].m_flags != 0)
				{
					buffer[n].m_angle = uShort2.Read();
				}
				else
				{
					buffer[n].m_angle = 0;
				}
			}
			uShort2.EndRead();
			for (int num3 = 1; num3 < num; num3++)
			{
				buffer[num3].m_nextGridProp = 0;
				buffer[num3].m_posY = 0;
				if (buffer[num3].m_flags != 0)
				{
					instance.InitializeProp((ushort)num3, ref buffer[num3], assetEditor);
				}
				else
				{
					instance.m_props.ReleaseItem((ushort)num3);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "PropManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "PropManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<PropInfo>.BindPrefabs();
			PropManager instance = Singleton<PropManager>.get_instance();
			instance.RefreshAutomaticProps();
			PropInstance[] buffer = instance.m_props.m_buffer;
			int num = buffer.Length;
			for (int i = 1; i < num; i++)
			{
				if (buffer[i].m_flags != 0)
				{
					PropInfo info = buffer[i].Info;
					if (info != null)
					{
						buffer[i].m_infoIndex = (ushort)info.m_prefabDataIndex;
					}
				}
			}
			instance.m_propCount = (int)(instance.m_props.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "PropManager");
		}
	}
}
