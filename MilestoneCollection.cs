﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using UnityEngine;

public class MilestoneCollection : MonoBehaviour
{
	private void Awake()
	{
		MilestoneCollection.InitializeMilestones(this.m_Milestones);
	}

	private void OnDestroy()
	{
		MilestoneCollection.DestroyMilestones(this.m_Milestones);
	}

	public static MilestoneInfo FindMilestone(string name)
	{
		MilestoneInfo result;
		if (MilestoneCollection.m_dict.TryGetValue(name, out result))
		{
			return result;
		}
		CODebugBase<LogChannel>.Warn(LogChannel.Serialization, "Unknown milestone: " + name);
		return null;
	}

	public static void InitializeMilestones(MilestoneInfo[] milestones)
	{
		for (int i = 0; i < milestones.Length; i++)
		{
			MilestoneInfo milestoneInfo = milestones[i];
			if (MilestoneCollection.m_dict.ContainsKey(milestoneInfo.get_name()))
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Duplicate milestone name: " + milestoneInfo.get_name());
			}
			else
			{
				MilestoneCollection.m_dict.Add(milestoneInfo.get_name(), milestoneInfo);
			}
		}
	}

	public static void DestroyMilestones(MilestoneInfo[] milestones)
	{
		for (int i = 0; i < milestones.Length; i++)
		{
			MilestoneInfo milestoneInfo = milestones[i];
			MilestoneCollection.m_dict.Remove(milestoneInfo.get_name());
		}
	}

	public static MilestoneInfo[] GetManualMilestones(ManualMilestone.Type type)
	{
		List<MilestoneInfo> list = new List<MilestoneInfo>();
		foreach (KeyValuePair<string, MilestoneInfo> current in MilestoneCollection.m_dict)
		{
			ManualMilestone manualMilestone = current.Value as ManualMilestone;
			if (manualMilestone != null && !manualMilestone.m_hidden && manualMilestone.m_type == type && manualMilestone.m_prefab != null)
			{
				list.Add(current.Value);
			}
		}
		return list.ToArray();
	}

	public MilestoneInfo[] m_Milestones;

	private static Dictionary<string, MilestoneInfo> m_dict = new Dictionary<string, MilestoneInfo>();
}
