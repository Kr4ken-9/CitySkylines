﻿using System;
using ColossalFramework.Globalization;

public class UICurrencyDeltaWrapper : UIWrapper<long>
{
	public UICurrencyDeltaWrapper(long def) : base(def)
	{
	}

	public override void Check(long newVal)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			string text = (this.m_Value <= 0L) ? ((this.m_Value >= 0L) ? "<color #ff4500>" : "<color #800000>-") : "<color #006400>+";
			this.m_String = StringUtils.SafeFormat("{1}{0}</color>", new object[]
			{
				Math.Abs((double)this.m_Value / 100.0).ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()),
				text
			});
		}
	}

	public void Check(long newVal, string specialMoneyFormat)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			string text = (this.m_Value <= 0L) ? ((this.m_Value >= 0L) ? "<color #ff4500>" : "<color #800000>-") : "<color #006400>+";
			this.m_String = StringUtils.SafeFormat("{1}{0}</color>", new object[]
			{
				Math.Abs((double)this.m_Value / 100.0).ToString(specialMoneyFormat, LocaleManager.get_cultureInfo()),
				text
			});
		}
	}

	private const string kPosPrefix = "<color #006400>+";

	private const string kNegPrefix = "<color #800000>-";

	private const string kNeutralPrefix = "<color #ff4500>";

	private const string kFormat = "{1}{0}</color>";
}
