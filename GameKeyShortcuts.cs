﻿using System;
using Cities.DemoMode;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class GameKeyShortcuts : KeyShortcuts
{
	private void Awake()
	{
		this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
		this.m_InfoViewsPanel = UIView.Find<UIPanel>("InfoViewsPanel");
		this.m_UnlockingPanel = UIView.Find<UIPanel>("UnlockingPanel");
		if (this.m_UnlockingPanel != null)
		{
			this.m_UnlockingPanelClose = this.m_UnlockingPanel.Find<UIButton>("Close");
		}
	}

	private void SteamEscape()
	{
		BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (TutorialPanel.instance != null && TutorialPanel.instance.isVisible)
		{
			TutorialPanel.instance.OnClose();
		}
		else if (TutorialAdvisorPanel.instance != null && TutorialAdvisorPanel.instance.isVisible)
		{
			TutorialAdvisorPanel.instance.OnClose();
		}
		else if (ChirpPanel.instance != null && ChirpPanel.instance.isShowing)
		{
			ChirpPanel.instance.Hide();
		}
		else if (WorldInfoPanel.AnyWorldInfoPanelOpen())
		{
			WorldInfoPanel.HideAllWorldInfoPanels();
		}
		else if (CityInfoPanel.instance != null && CityInfoPanel.instance.isVisible)
		{
			CityInfoPanel.instance.OnCloseButton();
		}
		else if (this.m_UnlockingPanelClose != null && this.m_UnlockingPanelClose.get_isVisible())
		{
			this.m_UnlockingPanelClose.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BuildingTool>() != null && tool.m_relocate != 0)
		{
			tool.CancelRelocate();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (ToolsModifierControl.GetCurrentTool<GameAreaTool>() != null)
		{
			base.SelectUIButton("ZoomButton");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else if (this.m_InfoViewsPanel != null && this.m_InfoViewsPanel.get_isVisible())
		{
			base.SelectUIButton("InfoPanel");
		}
	}

	protected void Update()
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseMenu))
			{
				UIView.get_library().ShowModal("PauseMenu");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseSimulation))
			{
				base.SimulationPause();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.NormalSimSpeed))
			{
				base.SimulationSpeed1();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.MediumSimSpeed))
			{
				base.SimulationSpeed2();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FastSimSpeed))
			{
				base.SimulationSpeed3();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Escape))
			{
				this.SteamEscape();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FreeCamera))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Economy))
			{
				base.SelectUIButton("Money");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Policies))
			{
				base.SelectUIButton("Policies");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.AreaView))
			{
				base.SelectUIButton("ZoomButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Bulldoze))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Unlocking))
			{
				base.SelectUIButton("UnlockButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.InfoViews))
			{
				base.SelectUIButton("InfoPanel");
			}
		}
	}

	protected override void OnProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (this.m_ShortcutSimulationPause.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationPause();
			}
			else if (this.m_ShortcutSimulationSpeed1.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed1();
			}
			else if (this.m_ShortcutSimulationSpeed2.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed2();
			}
			else if (this.m_ShortcutSimulationSpeed3.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed3();
			}
			else if (eventType == 4 && keyCode == 27)
			{
				this.Escape();
			}
			else if (this.m_ShortcutInGameQuickSave.IsPressed(eventType, keyCode, modifiers))
			{
				this.QuickSave();
			}
			else if (this.m_ShortcutInGameQuickLoad.IsPressed(eventType, keyCode, modifiers))
			{
				this.QuickLoad();
			}
			else if (this.m_ShortcutInGameShortcutRoads.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Roads");
			}
			else if (this.m_ShortcutInGameShortcutZoning.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Zoning");
			}
			else if (this.m_ShortcutInGameShortcutDistricts.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("District");
			}
			else if (this.m_ShortcutInGameShortcutElectricity.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Electricity");
			}
			else if (this.m_ShortcutInGameShortcutWater.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("WaterAndSewage");
			}
			else if (this.m_ShortcutInGameShortcutGarbage.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Garbage");
			}
			else if (this.m_ShortcutInGameShortcutHealthcare.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Healthcare");
			}
			else if (this.m_ShortcutInGameShortcutFireDepartment.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FireDepartment");
			}
			else if (this.m_ShortcutInGameShortcutPoliceDepartment.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Police");
			}
			else if (this.m_ShortcutInGameShortcutEducation.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Education");
			}
			else if (this.m_ShortcutInGameShortcutPublicTransport.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransport");
			}
			else if (this.m_ShortcutInGameShortcutDecoration.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Beautification");
			}
			else if (this.m_ShortcutInGameShortcutMonuments.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Monuments");
			}
			else if (this.m_ShortcutInGameShortcutWonders.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Wonders");
			}
			else if (this.m_ShortcutInGameShortcutLandscaping.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Landscaping");
			}
			else if (this.m_ShortcutInGameShortcutEconomy.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Money");
			}
			else if (this.m_ShortcutInGameShortcutBudget.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Budget");
			}
			else if (this.m_ShortcutInGameShortcutTaxes.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Taxes");
			}
			else if (this.m_ShortcutInGameShortcutLoans.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Loans");
			}
			else if (this.m_ShortcutInGameShortcutPolicies.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Policies");
			}
			else if (this.m_ShortcutInGameShortcutInfoviews.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoPanel");
			}
			else if (this.m_ShortcutInGameShortcutBulldozer.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (this.m_ShortcutInGameShortcutBulldozerUnderground.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BulldozerUndergroundToggle");
			}
			else if (this.m_ShortcutInGameRoadsSmallGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsSmallGroup");
			}
			else if (this.m_ShortcutInGameRoadsMediumGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsMediumGroup");
			}
			else if (this.m_ShortcutInGameRoadsLargeGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsLargeGroup");
			}
			else if (this.m_ShortcutInGameRoadsHighwayGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsHighwayGroup");
			}
			else if (this.m_ShortcutInGameRoadsIntersectionGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsIntersectionGroup");
			}
			else if (this.m_ShortcutInGameRoadsMaintenanceGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("RoadsMaintenanceGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportBusGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportBusGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportTramGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportTramGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportMetroGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportMetroGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportTrainGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportTrainGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportShipGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportShipGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportPlaneGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportPlaneGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportTaxiGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportTaxiGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportMonorailGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportMonorailGroup");
			}
			else if (this.m_ShortcutInGamePublicTransportCableCarGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("PublicTransportCableCarGroup");
			}
			else if (this.m_ShortcutInGameDistrictSpecializationPaintGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("DistrictSpecializationPaintGroup");
			}
			else if (this.m_ShortcutInGameDistrictSpecializationIndustrialGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("DistrictSpecializationIndustrialGroup");
			}
			else if (this.m_ShortcutInGameDistrictSpecializationCommercialGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("DistrictSpecializationCommercialGroup");
			}
			else if (this.m_ShortcutInGameWaterServicesGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("WaterServicesGroup");
			}
			else if (this.m_ShortcutInGameWaterHeatingGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("WaterHeatingGroup");
			}
			else if (this.m_ShortcutInGameBeautificationParksGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationParksGroup");
			}
			else if (this.m_ShortcutInGameBeautificationPlazasGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationPlazasGroup");
			}
			else if (this.m_ShortcutInGameBeautificationOthersGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationOthersGroup");
			}
			else if (this.m_ShortcutInGameBeautificationExpansion1Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationExpansion1Group");
			}
			else if (this.m_ShortcutInGameBeautificationExpansion2Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BeautificationExpansion2Group");
			}
			else if (this.m_ShortcutInGameLandscapingToolsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingGroup");
			}
			else if (this.m_ShortcutInGameLandscapingPathsGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingPathsGroup");
			}
			else if (this.m_ShortcutInGameLandscapingTreesGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingTreesGroup");
			}
			else if (this.m_ShortcutInGameLandscapingRocksGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingRocksGroup");
			}
			else if (this.m_ShortcutInGameLandscapingWaterStructuresGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingWaterStructuresGroup");
			}
			else if (this.m_ShortcutInGameLandscapingDisastersGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("LandscapingDisastersGroup");
			}
			else if (this.m_ShortcutInGameMonumentLandmarksGroup.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentLandmarksGroup");
			}
			else if (this.m_ShortcutInGameMonumentExpansion1Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentExpansion1Group");
			}
			else if (this.m_ShortcutInGameMonumentExpansion2Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentExpansion2Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory1Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory1Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory2Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory2Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory3Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory3Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory4Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory4Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory5Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory5Group");
			}
			else if (this.m_ShortcutInGameMonumentCategory6Group.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MonumentCategory6Group");
			}
			else if (this.m_ShortcutInGameShortcutAdvisor.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("AdvisorButton");
			}
			else if (this.m_ShortcutInGameShortcutFreeCameraMode.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (this.m_ShortcutInGameShortcutUnlockingPanel.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("UnlockButton");
			}
			else if (this.m_ShortcutInGameShortcutGameAreas.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ZoomButton");
			}
			else if (this.m_ShortcutInGameShortcutCityInfo.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("CityInfoButton");
			}
			else if (this.m_ShortcutInGameShortcutZoneResidentialLow.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ResidentialLow");
			}
			else if (this.m_ShortcutInGameShortcutZoneResidentialHigh.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ResidentialHigh");
			}
			else if (this.m_ShortcutInGameShortcutZoneCommercialLow.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("CommercialLow");
			}
			else if (this.m_ShortcutInGameShortcutZoneCommercialHigh.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("CommercialHigh");
			}
			else if (this.m_ShortcutInGameShortcutZoneIndustrial.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Industrial");
			}
			else if (this.m_ShortcutInGameShortcutZoneOffices.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Office");
			}
			else if (this.m_ShortcutInGameShortcutDezone.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Unzoned");
			}
			else if (this.m_ShortcutInGameShortcutInfoElectricity.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoElectricity");
			}
			else if (this.m_ShortcutInGameShortcutInfoWater.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoWater");
			}
			else if (this.m_ShortcutInGameShortcutInfoCrime.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoCrime");
			}
			else if (this.m_ShortcutInGameShortcutInfoHealth.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoHealth");
			}
			else if (this.m_ShortcutInGameShortcutInfoHappiness.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoHappiness");
			}
			else if (this.m_ShortcutInGameShortcutInfoPopulation.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoPopulation");
			}
			else if (this.m_ShortcutInGameShortcutInfoNoisePollution.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoNoisePollution");
			}
			else if (this.m_ShortcutInGameShortcutInfoPublicTransport.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoPublicTransport");
			}
			else if (this.m_ShortcutInGameShortcutInfoPollution.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoPollution");
			}
			else if (this.m_ShortcutInGameShortcutInfoResources.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoResources");
			}
			else if (this.m_ShortcutInGameShortcutInfoLandValue.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoLandValue");
			}
			else if (this.m_ShortcutInGameShortcutInfoDistricts.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoDistricts");
			}
			else if (this.m_ShortcutInGameShortcutInfoOutsideConnections.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoOutsideConnections");
			}
			else if (this.m_ShortcutInGameShortcutInfoTrafficCongestion.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoTrafficCongestion");
			}
			else if (this.m_ShortcutInGameShortcutInfoWindSpeed.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoWindSpeed");
			}
			else if (this.m_ShortcutInGameShortcutInfoGarbage.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoGarbage");
			}
			else if (this.m_ShortcutInGameShortcutInfoLevel.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoLevel");
			}
			else if (this.m_ShortcutInGameShortcutInfoFireSafety.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoFireSafety");
			}
			else if (this.m_ShortcutInGameShortcutInfoEducation.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoEducation");
			}
			else if (this.m_ShortcutInGameShortcutInfoHeating.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoHeating");
			}
			else if (this.m_ShortcutInGameShortcutInfoMaintenance.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoMaintenance");
			}
			else if (this.m_ShortcutInGameShortcutInfoSnow.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoSnow");
			}
			else if (this.m_ShortcutInGameShortcutInfoEscapeRoutes.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoEscapeRoutes");
			}
			else if (this.m_ShortcutInGameShortcutInfoRadio.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoRadio");
			}
			else if (this.m_ShortcutInGameShortcutInfoDestruction.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoDestruction");
			}
			else if (this.m_ShortcutInGameShortcutInfoDisasterDetection.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoDisasterDetection");
			}
			else if (this.m_ShortcutInGameShortcutInfoTrafficRoutes.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("InfoTrafficRoutes");
			}
			else if (this.m_ShortcutInGameShortcutSnapToAngle.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAngles = !SnapSettingsPanel.snapToAngles;
			}
			else if (this.m_ShortcutInGameShortcutSnapToLength.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToLength = !SnapSettingsPanel.snapToLength;
			}
			else if (this.m_ShortcutInGameShortcutSnapToGrid.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToGrid = !SnapSettingsPanel.snapToGrid;
			}
			else if (this.m_ShortcutInGameShortcutSnapToHelperLines.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToHelperLine = !SnapSettingsPanel.snapToHelperLine;
			}
			else if (this.m_ShortcutInGameShortcutSnapToAll.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAll = !SnapSettingsPanel.snapToAll;
			}
			else if (this.m_ShortcutToggleSnappingMenu.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressSnappingMenuButton();
			}
			else if (this.m_ShortcutElevationStep.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressElevationStepButton();
			}
			else if (this.m_ShortcutOptionButton1.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(0);
			}
			else if (this.m_ShortcutOptionButton2.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(1);
			}
			else if (this.m_ShortcutOptionButton3.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(2);
			}
			else if (this.m_ShortcutOptionButton4.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(3);
			}
		}
		else if (eventType == 4 && keyCode == 27)
		{
			this.EscapeFromModal();
		}
	}

	private void QuickSave()
	{
		if (DemoModeLoader.instance != null)
		{
			return;
		}
		SavePanel savePanel = UIView.get_library().Get<SavePanel>("SavePanel");
		if (savePanel != null)
		{
			savePanel.QuickSave("QuickSave");
		}
	}

	private void QuickLoad()
	{
		if (DemoModeLoader.instance != null)
		{
			return;
		}
		LoadPanel loadPanel = UIView.get_library().Get<LoadPanel>("LoadPanel");
		if (loadPanel != null)
		{
			loadPanel.QuickLoad();
		}
	}

	private void EscapeFromModal()
	{
		if (UIView.GetModalComponent() == this.m_UnlockingPanel)
		{
			this.m_UnlockingPanelClose.SimulateClick();
		}
	}

	private void Escape()
	{
		BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (TutorialPanel.instance != null && TutorialPanel.instance.isVisible)
		{
			TutorialPanel.instance.OnClose();
		}
		else if (TutorialAdvisorPanel.instance != null && TutorialAdvisorPanel.instance.isVisible)
		{
			TutorialAdvisorPanel.instance.OnClose();
		}
		else if (ChirpPanel.instance != null && ChirpPanel.instance.isShowing)
		{
			ChirpPanel.instance.Hide();
		}
		else if (WorldInfoPanel.AnyWorldInfoPanelOpen())
		{
			WorldInfoPanel.HideAllWorldInfoPanels();
		}
		else if (CityInfoPanel.instance != null && CityInfoPanel.instance.isVisible)
		{
			CityInfoPanel.instance.OnCloseButton();
		}
		else if (this.m_UnlockingPanelClose != null && this.m_UnlockingPanelClose.get_isVisible())
		{
			this.m_UnlockingPanelClose.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BuildingTool>() != null && tool.m_relocate != 0)
		{
			tool.CancelRelocate();
		}
		else if (ToolsModifierControl.GetCurrentTool<TransportTool>() != null)
		{
			ToolsModifierControl.SetTool<DefaultTool>();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (ToolsModifierControl.GetCurrentTool<GameAreaTool>() != null)
		{
			base.SelectUIButton("ZoomButton");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else if (this.m_InfoViewsPanel != null && this.m_InfoViewsPanel.get_isVisible())
		{
			base.SelectUIButton("InfoPanel");
		}
		else if (Singleton<InfoManager>.get_exists() && Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.None)
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		}
		else
		{
			UIView.get_library().ShowModal("PauseMenu");
		}
	}

	private SavedInputKey m_ShortcutSimulationPause = new SavedInputKey(Settings.inGameSimulationPause, Settings.inputSettingsFile, DefaultSettings.inGameSimulationPause, true);

	private SavedInputKey m_ShortcutSimulationSpeed1 = new SavedInputKey(Settings.inGameSimulationSpeed1, Settings.inputSettingsFile, DefaultSettings.inGameSimulationSpeed1, true);

	private SavedInputKey m_ShortcutSimulationSpeed2 = new SavedInputKey(Settings.inGameSimulationSpeed2, Settings.inputSettingsFile, DefaultSettings.inGameSimulationSpeed2, true);

	private SavedInputKey m_ShortcutSimulationSpeed3 = new SavedInputKey(Settings.inGameSimulationSpeed3, Settings.inputSettingsFile, DefaultSettings.inGameSimulationSpeed3, true);

	private SavedInputKey m_ShortcutInGameRoadsSmallGroup = new SavedInputKey(Settings.inGameShortcutRoadsSmallGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsSmallGroup, true);

	private SavedInputKey m_ShortcutInGameRoadsMediumGroup = new SavedInputKey(Settings.inGameShortcutRoadsMediumGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsMediumGroup, true);

	private SavedInputKey m_ShortcutInGameRoadsLargeGroup = new SavedInputKey(Settings.inGameShortcutRoadsLargeGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsLargeGroup, true);

	private SavedInputKey m_ShortcutInGameRoadsHighwayGroup = new SavedInputKey(Settings.inGameShortcutRoadsHighwayGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsHighwayGroup, true);

	private SavedInputKey m_ShortcutInGameRoadsIntersectionGroup = new SavedInputKey(Settings.inGameShortcutRoadsIntersectionGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsIntersectionGroup, true);

	private SavedInputKey m_ShortcutInGameRoadsMaintenanceGroup = new SavedInputKey(Settings.inGameShortcutRoadsMaintenanceGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoadsMaintenanceGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportBusGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportBusGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportBusGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportTramGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportTramGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportTramGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportMetroGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportMetroGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportMetroGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportTrainGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportTrainGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportTrainGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportShipGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportShipGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportShipGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportPlaneGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportPlaneGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportPlaneGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportTaxiGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportTaxiGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportTaxiGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportMonorailGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportMonorailGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportMonorailGroup, true);

	private SavedInputKey m_ShortcutInGamePublicTransportCableCarGroup = new SavedInputKey(Settings.inGameShortcutPublicTransportCableCarGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransportCableCarGroup, true);

	private SavedInputKey m_ShortcutInGameDistrictSpecializationPaintGroup = new SavedInputKey(Settings.inGameShortcutDistrictSpecializationPaintGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDistrictSpecializationPaintGroup, true);

	private SavedInputKey m_ShortcutInGameDistrictSpecializationIndustrialGroup = new SavedInputKey(Settings.inGameShortcutDistrictSpecializationIndustrialGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDistrictSpecializationIndustrialGroup, true);

	private SavedInputKey m_ShortcutInGameDistrictSpecializationCommercialGroup = new SavedInputKey(Settings.inGameShortcutDistrictSpecializationCommercialGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDistrictSpecializationCommercialGroup, true);

	private SavedInputKey m_ShortcutInGameWaterServicesGroup = new SavedInputKey(Settings.inGameShortcutWaterServicesGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutWaterServicesGroup, true);

	private SavedInputKey m_ShortcutInGameWaterHeatingGroup = new SavedInputKey(Settings.inGameShortcutWaterHeatingGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutWaterHeatingGroup, true);

	private SavedInputKey m_ShortcutInGameBeautificationParksGroup = new SavedInputKey(Settings.inGameShortcutBeautificationParksGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBeautificationParksGroup, true);

	private SavedInputKey m_ShortcutInGameBeautificationPlazasGroup = new SavedInputKey(Settings.inGameShortcutBeautificationPlazasGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBeautificationPlazasGroup, true);

	private SavedInputKey m_ShortcutInGameBeautificationOthersGroup = new SavedInputKey(Settings.inGameShortcutBeautificationOthersGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBeautificationOthersGroup, true);

	private SavedInputKey m_ShortcutInGameBeautificationExpansion1Group = new SavedInputKey(Settings.inGameShortcutBeautificationExpansion1Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBeautificationExpansion1Group, true);

	private SavedInputKey m_ShortcutInGameBeautificationExpansion2Group = new SavedInputKey(Settings.inGameShortcutBeautificationExpansion1Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBeautificationExpansion2Group, true);

	private SavedInputKey m_ShortcutInGameLandscapingToolsGroup = new SavedInputKey(Settings.inGameShortcutLandscapingToolsGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingToolsGroup, true);

	private SavedInputKey m_ShortcutInGameLandscapingPathsGroup = new SavedInputKey(Settings.inGameShortcutLandscapingPathsGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingPathsGroup, true);

	private SavedInputKey m_ShortcutInGameLandscapingTreesGroup = new SavedInputKey(Settings.inGameShortcutLandscapingTreesGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingTreesGroup, true);

	private SavedInputKey m_ShortcutInGameLandscapingRocksGroup = new SavedInputKey(Settings.inGameShortcutLandscapingRocksGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingRocksGroup, true);

	private SavedInputKey m_ShortcutInGameLandscapingWaterStructuresGroup = new SavedInputKey(Settings.inGameShortcutLandscapingWaterStructuresGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingWaterStructuresGroup, true);

	private SavedInputKey m_ShortcutInGameLandscapingDisastersGroup = new SavedInputKey(Settings.inGameShortcutLandscapingDisastersGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscapingDisastersGroup, true);

	private SavedInputKey m_ShortcutInGameMonumentLandmarksGroup = new SavedInputKey(Settings.inGameShortcutMonumentLandmarksGroup, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentLandmarksGroup, true);

	private SavedInputKey m_ShortcutInGameMonumentExpansion1Group = new SavedInputKey(Settings.inGameShortcutMonumentExpansion1Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentExpansion1Group, true);

	private SavedInputKey m_ShortcutInGameMonumentExpansion2Group = new SavedInputKey(Settings.inGameShortcutMonumentExpansion2Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentExpansion2Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory1Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory1Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory1Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory2Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory2Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory2Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory3Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory3Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory3Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory4Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory4Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory4Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory5Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory5Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory5Group, true);

	private SavedInputKey m_ShortcutInGameMonumentCategory6Group = new SavedInputKey(Settings.inGameShortcutMonumentCategory6Group, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonumentCategory6Group, true);

	private SavedInputKey m_ShortcutInGameShortcutRoads = new SavedInputKey(Settings.inGameShortcutRoads, Settings.inputSettingsFile, DefaultSettings.inGameShortcutRoads, true);

	private SavedInputKey m_ShortcutInGameShortcutZoning = new SavedInputKey(Settings.inGameShortcutZoning, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoning, true);

	private SavedInputKey m_ShortcutInGameShortcutDistricts = new SavedInputKey(Settings.inGameShortcutDistricts, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDistricts, true);

	private SavedInputKey m_ShortcutInGameShortcutElectricity = new SavedInputKey(Settings.inGameShortcutElectricity, Settings.inputSettingsFile, DefaultSettings.inGameShortcutElectricity, true);

	private SavedInputKey m_ShortcutInGameShortcutWater = new SavedInputKey(Settings.inGameShortcutWater, Settings.inputSettingsFile, DefaultSettings.inGameShortcutWater, true);

	private SavedInputKey m_ShortcutInGameShortcutGarbage = new SavedInputKey(Settings.inGameShortcutGarbage, Settings.inputSettingsFile, DefaultSettings.inGameShortcutGarbage, true);

	private SavedInputKey m_ShortcutInGameShortcutHealthcare = new SavedInputKey(Settings.inGameShortcutHealthcare, Settings.inputSettingsFile, DefaultSettings.inGameShortcutHealthcare, true);

	private SavedInputKey m_ShortcutInGameShortcutFireDepartment = new SavedInputKey(Settings.inGameShortcutFireDepartment, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFireDepartment, true);

	private SavedInputKey m_ShortcutInGameShortcutPoliceDepartment = new SavedInputKey(Settings.inGameShortcutPoliceDepartment, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPoliceDepartment, true);

	private SavedInputKey m_ShortcutInGameShortcutEducation = new SavedInputKey(Settings.inGameShortcutEducation, Settings.inputSettingsFile, DefaultSettings.inGameShortcutEducation, true);

	private SavedInputKey m_ShortcutInGameShortcutPublicTransport = new SavedInputKey(Settings.inGameShortcutPublicTransport, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPublicTransport, true);

	private SavedInputKey m_ShortcutInGameShortcutDecoration = new SavedInputKey(Settings.inGameShortcutDecoration, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDecoration, true);

	private SavedInputKey m_ShortcutInGameShortcutMonuments = new SavedInputKey(Settings.inGameShortcutMonuments, Settings.inputSettingsFile, DefaultSettings.inGameShortcutMonuments, true);

	private SavedInputKey m_ShortcutInGameShortcutWonders = new SavedInputKey(Settings.inGameShortcutWonders, Settings.inputSettingsFile, DefaultSettings.inGameShortcutWonders, true);

	private SavedInputKey m_ShortcutInGameShortcutLandscaping = new SavedInputKey(Settings.inGameShortcutLandscaping, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLandscaping, true);

	private SavedInputKey m_ShortcutInGameShortcutEconomy = new SavedInputKey(Settings.inGameShortcutEconomy, Settings.inputSettingsFile, DefaultSettings.inGameShortcutEconomy, true);

	private SavedInputKey m_ShortcutInGameShortcutBudget = new SavedInputKey(Settings.inGameShortcutBudget, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBudget, true);

	private SavedInputKey m_ShortcutInGameShortcutTaxes = new SavedInputKey(Settings.inGameShortcutTaxes, Settings.inputSettingsFile, DefaultSettings.inGameShortcutTaxes, true);

	private SavedInputKey m_ShortcutInGameShortcutLoans = new SavedInputKey(Settings.inGameShortcutLoans, Settings.inputSettingsFile, DefaultSettings.inGameShortcutLoans, true);

	private SavedInputKey m_ShortcutInGameShortcutPolicies = new SavedInputKey(Settings.inGameShortcutPolicies, Settings.inputSettingsFile, DefaultSettings.inGameShortcutPolicies, true);

	private SavedInputKey m_ShortcutInGameShortcutBulldozer = new SavedInputKey(Settings.inGameShortcutBulldozer, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBulldozer, true);

	private SavedInputKey m_ShortcutInGameShortcutBulldozerUnderground = new SavedInputKey(Settings.inGameShortcutBulldozerUnderground, Settings.inputSettingsFile, DefaultSettings.inGameShortcutBulldozerUnderground, true);

	private SavedInputKey m_ShortcutInGameShortcutAdvisor = new SavedInputKey(Settings.inGameShortcutAdvisor, Settings.inputSettingsFile, DefaultSettings.inGameShortcutAdvisor, true);

	private SavedInputKey m_ShortcutInGameShortcutFreeCameraMode = new SavedInputKey(Settings.inGameShortcutFreeCameraMode, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFreeCameraMode, true);

	private SavedInputKey m_ShortcutInGameShortcutUnlockingPanel = new SavedInputKey(Settings.inGameShortcutUnlockingPanel, Settings.inputSettingsFile, DefaultSettings.inGameShortcutUnlockingPanel, true);

	private SavedInputKey m_ShortcutInGameShortcutGameAreas = new SavedInputKey(Settings.inGameShortcutGameAreas, Settings.inputSettingsFile, DefaultSettings.inGameShortcutGameAreas, true);

	private SavedInputKey m_ShortcutInGameShortcutCityInfo = new SavedInputKey(Settings.inGameShortcutCityInfo, Settings.inputSettingsFile, DefaultSettings.inGameShortcutCityInfo, true);

	private SavedInputKey m_ShortcutInGameQuickSave = new SavedInputKey(Settings.inGameShortcutQuickSave, Settings.inputSettingsFile, DefaultSettings.inGameShortcutQuickSave, true);

	private SavedInputKey m_ShortcutInGameQuickLoad = new SavedInputKey(Settings.inGameShortcutQuickLoad, Settings.inputSettingsFile, DefaultSettings.inGameShortcutQuickLoad, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneResidentialLow = new SavedInputKey(Settings.inGameShortcutZoneResidentialLow, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneResidentialLow, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneResidentialHigh = new SavedInputKey(Settings.inGameShortcutZoneResidentialHigh, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneResidentialHigh, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneCommercialLow = new SavedInputKey(Settings.inGameShortcutZoneCommercialLow, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneCommercialLow, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneCommercialHigh = new SavedInputKey(Settings.inGameShortcutZoneCommercialHigh, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneCommercialHigh, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneIndustrial = new SavedInputKey(Settings.inGameShortcutZoneIndustrial, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneIndustrial, true);

	private SavedInputKey m_ShortcutInGameShortcutZoneOffices = new SavedInputKey(Settings.inGameShortcutZoneOffices, Settings.inputSettingsFile, DefaultSettings.inGameShortcutZoneOffices, true);

	private SavedInputKey m_ShortcutInGameShortcutDezone = new SavedInputKey(Settings.inGameShortcutDezone, Settings.inputSettingsFile, DefaultSettings.inGameShortcutDezone, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoviews = new SavedInputKey(Settings.inGameShortcutInfoviews, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoviews, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoElectricity = new SavedInputKey(Settings.inGameShortcutInfoElectricity, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoElectricity, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoWater = new SavedInputKey(Settings.inGameShortcutInfoWater, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoWater, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoCrime = new SavedInputKey(Settings.inGameShortcutInfoCrime, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoCrime, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoHealth = new SavedInputKey(Settings.inGameShortcutInfoHealth, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoHealth, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoHappiness = new SavedInputKey(Settings.inGameShortcutInfoHappiness, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoHappiness, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoPopulation = new SavedInputKey(Settings.inGameShortcutInfoPopulation, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoPopulation, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoNoisePollution = new SavedInputKey(Settings.inGameShortcutInfoNoisePollution, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoNoisePollution, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoPublicTransport = new SavedInputKey(Settings.inGameShortcutInfoPublicTransport, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoPublicTransport, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoPollution = new SavedInputKey(Settings.inGameShortcutInfoPollution, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoPollution, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoResources = new SavedInputKey(Settings.inGameShortcutInfoResources, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoResources, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoLandValue = new SavedInputKey(Settings.inGameShortcutInfoLandValue, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoLandValue, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoDistricts = new SavedInputKey(Settings.inGameShortcutInfoDistricts, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoDistricts, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoOutsideConnections = new SavedInputKey(Settings.inGameShortcutInfoOutsideConnections, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoOutsideConnections, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoTrafficCongestion = new SavedInputKey(Settings.inGameShortcutInfoTrafficCongestion, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoTrafficCongestion, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoWindSpeed = new SavedInputKey(Settings.inGameShortcutInfoWindSpeed, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoWindSpeed, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoGarbage = new SavedInputKey(Settings.inGameShortcutInfoGarbage, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoGarbage, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoLevel = new SavedInputKey(Settings.inGameShortcutInfoLevel, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoLevel, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoFireSafety = new SavedInputKey(Settings.inGameShortcutInfoFireSafety, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoFireSafety, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoEducation = new SavedInputKey(Settings.inGameShortcutInfoEducation, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoEducation, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoEscapeRoutes = new SavedInputKey(Settings.inGameShortcutInfoEscapeRoutes, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoEscapeRoutes, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoRadio = new SavedInputKey(Settings.inGameShortcutInfoRadio, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoRadio, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoDestruction = new SavedInputKey(Settings.inGameShortcutInfoDestruction, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoDestruction, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoDisasterDetection = new SavedInputKey(Settings.inGameShortcutInfoDisasterDetection, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoDisasterDetection, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoTrafficRoutes = new SavedInputKey(Settings.inGameShortcutInfoTrafficRoutes, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoTrafficRoutes, true);

	private SavedInputKey m_ShortcutToggleSnappingMenu = new SavedInputKey(Settings.sharedShortcutToggleSnappingMenu, Settings.inputSettingsFile, DefaultSettings.sharedShortcutToggleSnappingMenu, true);

	private SavedInputKey m_ShortcutElevationStep = new SavedInputKey(Settings.sharedShortcutElevationStep, Settings.inputSettingsFile, DefaultSettings.sharedShortcutElevationStep, true);

	private SavedInputKey m_ShortcutOptionButton1 = new SavedInputKey(Settings.sharedShortcutOptionsButton1, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton1, true);

	private SavedInputKey m_ShortcutOptionButton2 = new SavedInputKey(Settings.sharedShortcutOptionsButton2, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton2, true);

	private SavedInputKey m_ShortcutOptionButton3 = new SavedInputKey(Settings.sharedShortcutOptionsButton3, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton3, true);

	private SavedInputKey m_ShortcutOptionButton4 = new SavedInputKey(Settings.sharedShortcutOptionsButton4, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton4, true);

	private SavedInputKey m_ShortcutInGameShortcutSnapToAngle = new SavedInputKey(Settings.inGameShortcutSnapToAngle, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAngle, true);

	private SavedInputKey m_ShortcutInGameShortcutSnapToLength = new SavedInputKey(Settings.inGameShortcutSnapToLength, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToLength, true);

	private SavedInputKey m_ShortcutInGameShortcutSnapToGrid = new SavedInputKey(Settings.inGameShortcutSnapToGrid, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToGrid, true);

	private SavedInputKey m_ShortcutInGameShortcutSnapToHelperLines = new SavedInputKey(Settings.inGameShortcutSnapToHelperLines, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToHelperLines, true);

	private SavedInputKey m_ShortcutInGameShortcutSnapToAll = new SavedInputKey(Settings.inGameShortcutSnapToAll, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAll, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoHeating = new SavedInputKey(Settings.inGameShortcutInfoHeating, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoHeating, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoMaintenance = new SavedInputKey(Settings.inGameShortcutInfoMaintenance, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoMaintenance, true);

	private SavedInputKey m_ShortcutInGameShortcutInfoSnow = new SavedInputKey(Settings.inGameShortcutInfoSnow, Settings.inputSettingsFile, DefaultSettings.inGameShortcutInfoSnow, true);

	private UIButton m_CloseToolbarButton;

	private UIPanel m_InfoViewsPanel;

	private UIPanel m_UnlockingPanel;

	private UIButton m_UnlockingPanelClose;
}
