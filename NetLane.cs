﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct NetLane
{
	public void RefreshInstance(uint laneID, NetInfo.Lane laneInfo, float startAngle, float endAngle, bool invert, ref RenderManager.Instance data, ref int propIndex)
	{
		NetLaneProps laneProps = laneInfo.m_laneProps;
		if (laneProps != null && laneProps.m_props != null)
		{
			bool flag = (byte)(laneInfo.m_finalDirection & NetInfo.Direction.Both) == 2 || (byte)(laneInfo.m_finalDirection & NetInfo.Direction.AvoidBoth) == 11;
			bool flag2 = flag != invert;
			int num = laneProps.m_props.Length;
			for (int i = 0; i < num; i++)
			{
				NetLaneProps.Prop prop = laneProps.m_props[i];
				if (this.m_length >= prop.m_minLength)
				{
					int num2 = 2;
					if (prop.m_repeatDistance > 1f)
					{
						num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_length / prop.m_repeatDistance));
					}
					int num3 = propIndex;
					propIndex = num3 + (num2 + 1 >> 1);
					float num4 = prop.m_segmentOffset * 0.5f;
					if (this.m_length != 0f)
					{
						num4 = Mathf.Clamp(num4 + prop.m_position.z / this.m_length, -0.5f, 0.5f);
					}
					if (flag2)
					{
						num4 = -num4;
					}
					PropInfo finalProp = prop.m_finalProp;
					if (finalProp != null)
					{
						Randomizer randomizer;
						randomizer..ctor((int)(laneID + (uint)i));
						for (int j = 1; j <= num2; j += 2)
						{
							if (randomizer.Int32(100u) < prop.m_probability)
							{
								float num5 = num4 + (float)j / (float)num2;
								PropInfo variation = finalProp.GetVariation(ref randomizer);
								randomizer.Int32(10000u);
								if (prop.m_colorMode == NetLaneProps.ColorMode.Default)
								{
									variation.GetColor(ref randomizer);
								}
								Vector3 worldPos = this.m_bezier.Position(num5);
								Vector3 vector = this.m_bezier.Tangent(num5);
								if (vector != Vector3.get_zero())
								{
									if (flag2)
									{
										vector = -vector;
									}
									vector.y = 0f;
									if (prop.m_position.x != 0f)
									{
										vector = Vector3.Normalize(vector);
										worldPos.x += vector.z * prop.m_position.x;
										worldPos.z -= vector.x * prop.m_position.x;
									}
									float num6 = Mathf.Atan2(vector.x, -vector.z);
									if (prop.m_cornerAngle != 0f || prop.m_position.x != 0f)
									{
										float num7 = endAngle - startAngle;
										if (num7 > 3.14159274f)
										{
											num7 -= 6.28318548f;
										}
										if (num7 < -3.14159274f)
										{
											num7 += 6.28318548f;
										}
										float num8 = startAngle + num7 * num5;
										num7 = num8 - num6;
										if (num7 > 3.14159274f)
										{
											num7 -= 6.28318548f;
										}
										if (num7 < -3.14159274f)
										{
											num7 += 6.28318548f;
										}
										num6 += num7 * prop.m_cornerAngle;
										if (num7 != 0f && prop.m_position.x != 0f)
										{
											float num9 = Mathf.Tan(num7);
											worldPos.x += vector.x * num9 * prop.m_position.x;
											worldPos.z += vector.z * num9 * prop.m_position.x;
										}
									}
									if (variation.m_requireWaterMap)
									{
										worldPos.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, false, 0f);
									}
									else
									{
										worldPos.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(worldPos);
									}
									data.m_extraData.SetUShort(num3++, (ushort)Mathf.Clamp(Mathf.RoundToInt(worldPos.y * 64f), 0, 65535));
								}
							}
						}
					}
					TreeInfo finalTree = prop.m_finalTree;
					if (finalTree != null)
					{
						Randomizer randomizer2;
						randomizer2..ctor((int)(laneID + (uint)i));
						for (int k = 1; k <= num2; k += 2)
						{
							if (randomizer2.Int32(100u) < prop.m_probability)
							{
								float num10 = num4 + (float)k / (float)num2;
								finalTree.GetVariation(ref randomizer2);
								randomizer2.Int32(10000u);
								randomizer2.Int32(10000u);
								Vector3 worldPos2 = this.m_bezier.Position(num10);
								worldPos2.y += prop.m_position.y;
								if (prop.m_position.x != 0f)
								{
									Vector3 vector2 = this.m_bezier.Tangent(num10);
									if (flag2)
									{
										vector2 = -vector2;
									}
									vector2.y = 0f;
									vector2 = Vector3.Normalize(vector2);
									worldPos2.x += vector2.z * prop.m_position.x;
									worldPos2.z -= vector2.x * prop.m_position.x;
								}
								worldPos2.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(worldPos2);
								data.m_extraData.SetUShort(num3++, (ushort)Mathf.Clamp(Mathf.RoundToInt(worldPos2.y * 64f), 0, 65535));
							}
						}
					}
				}
			}
		}
	}

	public void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort segmentID, uint laneID, NetInfo.Lane laneInfo, NetNode.Flags startFlags, NetNode.Flags endFlags, Color startColor, Color endColor, float startAngle, float endAngle, bool invert, int layerMask, Vector4 objectIndex1, Vector4 objectIndex2, ref RenderManager.Instance data, ref int propIndex)
	{
		NetLaneProps laneProps = laneInfo.m_laneProps;
		if (laneProps != null && laneProps.m_props != null)
		{
			bool flag = (byte)(laneInfo.m_finalDirection & NetInfo.Direction.Both) == 2 || (byte)(laneInfo.m_finalDirection & NetInfo.Direction.AvoidBoth) == 11;
			bool flag2 = flag != invert;
			if (flag)
			{
				NetNode.Flags flags = startFlags;
				startFlags = endFlags;
				endFlags = flags;
			}
			Texture texture = null;
			Vector4 zero = Vector4.get_zero();
			Vector4 zero2 = Vector4.get_zero();
			Texture texture2 = null;
			Vector4 zero3 = Vector4.get_zero();
			Vector4 zero4 = Vector4.get_zero();
			int num = laneProps.m_props.Length;
			for (int i = 0; i < num; i++)
			{
				NetLaneProps.Prop prop = laneProps.m_props[i];
				if (this.m_length >= prop.m_minLength)
				{
					int num2 = 2;
					if (prop.m_repeatDistance > 1f)
					{
						num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_length / prop.m_repeatDistance));
					}
					int num3 = propIndex;
					if (propIndex != -1)
					{
						propIndex = num3 + (num2 + 1 >> 1);
					}
					if (prop.CheckFlags((NetLane.Flags)this.m_flags, startFlags, endFlags))
					{
						float num4 = prop.m_segmentOffset * 0.5f;
						if (this.m_length != 0f)
						{
							num4 = Mathf.Clamp(num4 + prop.m_position.z / this.m_length, -0.5f, 0.5f);
						}
						if (flag2)
						{
							num4 = -num4;
						}
						PropInfo finalProp = prop.m_finalProp;
						if (finalProp != null && (layerMask & 1 << finalProp.m_prefabDataLayer) != 0)
						{
							Color color = (prop.m_colorMode != NetLaneProps.ColorMode.EndState) ? startColor : endColor;
							Randomizer randomizer;
							randomizer..ctor((int)(laneID + (uint)i));
							for (int j = 1; j <= num2; j += 2)
							{
								if (randomizer.Int32(100u) < prop.m_probability)
								{
									float num5 = num4 + (float)j / (float)num2;
									PropInfo variation = finalProp.GetVariation(ref randomizer);
									float scale = variation.m_minScale + (float)randomizer.Int32(10000u) * (variation.m_maxScale - variation.m_minScale) * 0.0001f;
									if (prop.m_colorMode == NetLaneProps.ColorMode.Default)
									{
										color = variation.GetColor(ref randomizer);
									}
									Vector3 vector = this.m_bezier.Position(num5);
									if (propIndex != -1)
									{
										vector.y = (float)data.m_extraData.GetUShort(num3++) * 0.015625f;
									}
									vector.y += prop.m_position.y;
									if (cameraInfo.CheckRenderDistance(vector, variation.m_maxRenderDistance))
									{
										Vector3 vector2 = this.m_bezier.Tangent(num5);
										if (vector2 != Vector3.get_zero())
										{
											if (flag2)
											{
												vector2 = -vector2;
											}
											vector2.y = 0f;
											if (prop.m_position.x != 0f)
											{
												vector2 = Vector3.Normalize(vector2);
												vector.x += vector2.z * prop.m_position.x;
												vector.z -= vector2.x * prop.m_position.x;
											}
											float num6 = Mathf.Atan2(vector2.x, -vector2.z);
											if (prop.m_cornerAngle != 0f || prop.m_position.x != 0f)
											{
												float num7 = endAngle - startAngle;
												if (num7 > 3.14159274f)
												{
													num7 -= 6.28318548f;
												}
												if (num7 < -3.14159274f)
												{
													num7 += 6.28318548f;
												}
												float num8 = startAngle + num7 * num5;
												num7 = num8 - num6;
												if (num7 > 3.14159274f)
												{
													num7 -= 6.28318548f;
												}
												if (num7 < -3.14159274f)
												{
													num7 += 6.28318548f;
												}
												num6 += num7 * prop.m_cornerAngle;
												if (num7 != 0f && prop.m_position.x != 0f)
												{
													float num9 = Mathf.Tan(num7);
													vector.x += vector2.x * num9 * prop.m_position.x;
													vector.z += vector2.z * num9 * prop.m_position.x;
												}
											}
											Vector4 objectIndex3 = (num5 <= 0.5f) ? objectIndex1 : objectIndex2;
											num6 += prop.m_angle * 0.0174532924f;
											InstanceID id = default(InstanceID);
											id.NetSegment = segmentID;
											if (variation.m_requireWaterMap)
											{
												if (texture == null)
												{
													Singleton<TerrainManager>.get_instance().GetHeightMapping(Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segmentID].m_middlePosition, out texture, out zero, out zero2);
												}
												if (texture2 == null)
												{
													Singleton<TerrainManager>.get_instance().GetWaterMapping(Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segmentID].m_middlePosition, out texture2, out zero3, out zero4);
												}
												PropInstance.RenderInstance(cameraInfo, variation, id, vector, scale, num6, color, objectIndex3, true, texture, zero, zero2, texture2, zero3, zero4);
											}
											else if (!variation.m_requireHeightMap)
											{
												PropInstance.RenderInstance(cameraInfo, variation, id, vector, scale, num6, color, objectIndex3, true);
											}
										}
									}
								}
							}
						}
						TreeInfo finalTree = prop.m_finalTree;
						if (finalTree != null && (layerMask & 1 << finalTree.m_prefabDataLayer) != 0)
						{
							Randomizer randomizer2;
							randomizer2..ctor((int)(laneID + (uint)i));
							for (int k = 1; k <= num2; k += 2)
							{
								if (randomizer2.Int32(100u) < prop.m_probability)
								{
									float num10 = num4 + (float)k / (float)num2;
									TreeInfo variation2 = finalTree.GetVariation(ref randomizer2);
									float scale2 = variation2.m_minScale + (float)randomizer2.Int32(10000u) * (variation2.m_maxScale - variation2.m_minScale) * 0.0001f;
									float brightness = variation2.m_minBrightness + (float)randomizer2.Int32(10000u) * (variation2.m_maxBrightness - variation2.m_minBrightness) * 0.0001f;
									Vector3 position = this.m_bezier.Position(num10);
									if (propIndex != -1)
									{
										position.y = (float)data.m_extraData.GetUShort(num3++) * 0.015625f;
									}
									position.y += prop.m_position.y;
									if (prop.m_position.x != 0f)
									{
										Vector3 vector3 = this.m_bezier.Tangent(num10);
										if (flag2)
										{
											vector3 = -vector3;
										}
										vector3.y = 0f;
										vector3 = Vector3.Normalize(vector3);
										position.x += vector3.z * prop.m_position.x;
										position.z -= vector3.x * prop.m_position.x;
									}
									TreeInstance.RenderInstance(cameraInfo, variation2, position, scale2, brightness);
								}
							}
						}
					}
				}
			}
		}
	}

	public void RenderDestroyedInstance(RenderManager.CameraInfo cameraInfo, ushort segmentID, uint laneID, NetInfo netInfo, NetInfo.Lane laneInfo, NetNode.Flags startFlags, NetNode.Flags endFlags, Color startColor, Color endColor, float startAngle, float endAngle, bool invert, int layerMask, Vector4 objectIndex1, Vector4 objectIndex2, ref RenderManager.Instance data, ref int propIndex)
	{
		NetLaneProps laneProps = laneInfo.m_laneProps;
		if (laneProps != null && laneProps.m_props != null)
		{
			bool flag = (byte)(laneInfo.m_finalDirection & NetInfo.Direction.Both) == 2 || (byte)(laneInfo.m_finalDirection & NetInfo.Direction.AvoidBoth) == 11;
			bool flag2 = flag != invert;
			if (flag)
			{
				NetNode.Flags flags = startFlags;
				startFlags = endFlags;
				endFlags = flags;
			}
			int num = laneProps.m_props.Length;
			for (int i = 0; i < num; i++)
			{
				NetLaneProps.Prop prop = laneProps.m_props[i];
				if (this.m_length >= prop.m_minLength)
				{
					int num2 = 2;
					if (prop.m_repeatDistance > 1f)
					{
						num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_length / prop.m_repeatDistance));
					}
					int num3 = propIndex;
					if (propIndex != -1)
					{
						propIndex = num3 + (num2 + 1 >> 1);
					}
					if (prop.CheckFlags((NetLane.Flags)this.m_flags, startFlags, endFlags))
					{
						float num4 = prop.m_segmentOffset * 0.5f;
						if (this.m_length != 0f)
						{
							num4 = Mathf.Clamp(num4 + prop.m_position.z / this.m_length, -0.5f, 0.5f);
						}
						if (flag2)
						{
							num4 = -num4;
						}
						PropInfo finalProp = prop.m_finalProp;
						if (finalProp != null && (layerMask & 1 << finalProp.m_prefabDataLayer) != 0)
						{
							Color color = (prop.m_colorMode != NetLaneProps.ColorMode.EndState) ? startColor : endColor;
							Randomizer randomizer;
							randomizer..ctor((int)(laneID + (uint)i));
							for (int j = 1; j <= num2; j += 2)
							{
								if (randomizer.Int32(100u) < prop.m_probability)
								{
									float num5 = num4 + (float)j / (float)num2;
									PropInfo variation = finalProp.GetVariation(ref randomizer);
									float scale = variation.m_minScale + (float)randomizer.Int32(10000u) * (variation.m_maxScale - variation.m_minScale) * 0.0001f;
									if (prop.m_colorMode == NetLaneProps.ColorMode.Default)
									{
										color = variation.GetColor(ref randomizer);
									}
									if (variation.m_isDecal || variation.m_surviveCollapse)
									{
										Vector3 vector = this.m_bezier.Position(num5);
										if (propIndex != -1)
										{
											vector.y = (float)data.m_extraData.GetUShort(num3++) * 0.015625f;
										}
										vector.y += prop.m_position.y;
										if (cameraInfo.CheckRenderDistance(vector, variation.m_maxRenderDistance))
										{
											Vector3 vector2 = this.m_bezier.Tangent(num5);
											if (vector2 != Vector3.get_zero())
											{
												if (flag2)
												{
													vector2 = -vector2;
												}
												vector2.y = 0f;
												if (prop.m_position.x != 0f)
												{
													vector2 = Vector3.Normalize(vector2);
													vector.x += vector2.z * prop.m_position.x;
													vector.z -= vector2.x * prop.m_position.x;
												}
												float num6 = Mathf.Atan2(vector2.x, -vector2.z);
												if (prop.m_cornerAngle != 0f || prop.m_position.x != 0f)
												{
													float num7 = endAngle - startAngle;
													if (num7 > 3.14159274f)
													{
														num7 -= 6.28318548f;
													}
													if (num7 < -3.14159274f)
													{
														num7 += 6.28318548f;
													}
													float num8 = startAngle + num7 * num5;
													num7 = num8 - num6;
													if (num7 > 3.14159274f)
													{
														num7 -= 6.28318548f;
													}
													if (num7 < -3.14159274f)
													{
														num7 += 6.28318548f;
													}
													num6 += num7 * prop.m_cornerAngle;
													if (num7 != 0f && prop.m_position.x != 0f)
													{
														float num9 = Mathf.Tan(num7);
														vector.x += vector2.x * num9 * prop.m_position.x;
														vector.z += vector2.z * num9 * prop.m_position.x;
													}
												}
												Vector4 objectIndex3 = (num5 <= 0.5f) ? objectIndex1 : objectIndex2;
												num6 += prop.m_angle * 0.0174532924f;
												PropInstance.RenderInstance(cameraInfo, variation, new InstanceID
												{
													NetSegment = segmentID
												}, vector, scale, num6, color, objectIndex3, true);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if ((byte)(laneInfo.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) == 0)
		{
			return;
		}
		Randomizer randomizer2;
		randomizer2..ctor(laneID);
		int num10 = Mathf.RoundToInt(this.m_length * 0.07f);
		int k = 0;
		while (k < num10)
		{
			PropInfo propInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer2, ItemClass.Service.Road);
			propInfo = propInfo.GetVariation(ref randomizer2);
			float num11 = propInfo.m_minScale + (float)randomizer2.Int32(10000u) * (propInfo.m_maxScale - propInfo.m_minScale) * 0.0001f;
			Color color2 = propInfo.GetColor(ref randomizer2);
			float num12 = (float)randomizer2.Int32(1000u) * 0.001f;
			float angle = (float)randomizer2.Int32(1000u) * 0.006283186f;
			if (propInfo.m_isDecal)
			{
				goto IL_606;
			}
			if (netInfo.m_netAI.IsOverground())
			{
				float num13 = netInfo.m_halfWidth - Mathf.Abs(laneInfo.m_position);
				float num14 = Mathf.Max(propInfo.m_generatedInfo.m_size.x, propInfo.m_generatedInfo.m_size.z) * num11 * 0.5f - 0.5f;
				if (num13 < num14)
				{
					goto IL_65D;
				}
			}
			if (laneInfo.m_verticalOffset < 0.5f)
			{
				goto IL_606;
			}
			IL_65D:
			k++;
			continue;
			IL_606:
			Vector3 position = this.m_bezier.Position(num12);
			Vector4 objectIndex4 = (num12 <= 0.5f) ? objectIndex1 : objectIndex2;
			InstanceID id = default(InstanceID);
			id.NetSegment = segmentID;
			if (!propInfo.m_requireHeightMap)
			{
				PropInstance.RenderInstance(cameraInfo, propInfo, id, position, num11, angle, color2, objectIndex4, true);
				goto IL_65D;
			}
			goto IL_65D;
		}
	}

	public void ReserveSpace(float len)
	{
		byte b = (byte)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u);
		if (b != this.m_reservedFrame)
		{
			if ((this.m_reservedFrame + 1 & 255) == b)
			{
				this.m_reservedPrev = this.m_reservedNext;
			}
			else
			{
				this.m_reservedPrev = 0;
			}
			this.m_reservedNext = 0;
			this.m_reservedFrame = b;
		}
		if (this.m_length != 0f)
		{
			this.m_reservedNext = (byte)Mathf.Min((int)this.m_reservedNext + Mathf.RoundToInt(len * 255f / this.m_length), 255);
		}
	}

	public bool ReserveSpace(float len, ushort reserveID)
	{
		byte b = (byte)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u);
		if (b != this.m_reservedFrame)
		{
			if ((this.m_reservedFrame + 1 & 255) == b)
			{
				this.m_reservedPrev = this.m_reservedNext;
			}
			else
			{
				this.m_reservedPrev = 0;
			}
			this.m_reservedNext = 0;
			this.m_reservedFrame = b;
		}
		if ((this.m_reservedPrev == 0 && this.m_reservedNext == 0) || this.m_lastReserveID == reserveID)
		{
			if (this.m_length != 0f)
			{
				this.m_reservedNext = (byte)Mathf.Min((int)this.m_reservedNext + Mathf.RoundToInt(len * 255f / this.m_length), 255);
			}
			this.m_lastReserveID = reserveID;
			return true;
		}
		return false;
	}

	public bool CheckSpace(float len)
	{
		byte b = (byte)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u);
		if (b != this.m_reservedFrame)
		{
			if ((this.m_reservedFrame + 1 & 255) == b)
			{
				this.m_reservedPrev = this.m_reservedNext;
			}
			else
			{
				this.m_reservedPrev = 0;
			}
			this.m_reservedNext = 0;
			this.m_reservedFrame = b;
		}
		return this.m_reservedPrev == 0 || (float)this.m_reservedPrev * this.m_length <= (this.m_length - len) * 255f;
	}

	public bool CheckSpace(float len, ushort ignoreID)
	{
		byte b = (byte)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u);
		if (b != this.m_reservedFrame)
		{
			if ((this.m_reservedFrame + 1 & 255) == b)
			{
				this.m_reservedPrev = this.m_reservedNext;
			}
			else
			{
				this.m_reservedPrev = 0;
			}
			this.m_reservedNext = 0;
			this.m_reservedFrame = b;
		}
		int num = Mathf.Max((int)this.m_reservedPrev, (int)this.m_reservedNext);
		return num == 0 || (float)num * this.m_length <= (this.m_length - len) * 255f || (this.m_lastReserveID == ignoreID && ignoreID != 0);
	}

	public float GetReservedSpace()
	{
		byte b = (byte)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u);
		if (b != this.m_reservedFrame)
		{
			if ((this.m_reservedFrame + 1 & 255) == b)
			{
				this.m_reservedPrev = this.m_reservedNext;
			}
			else
			{
				this.m_reservedPrev = 0;
			}
			this.m_reservedNext = 0;
			this.m_reservedFrame = b;
		}
		return (float)this.m_reservedPrev * this.m_length / 255f;
	}

	public void GetClosestPosition(Vector3 point, out Vector3 position, out float laneOffset)
	{
		float num = 1E+11f;
		laneOffset = 0f;
		Vector3 vector = this.m_bezier.a;
		for (int i = 1; i <= 16; i++)
		{
			Vector3 vector2 = this.m_bezier.Position((float)i / 16f);
			float num3;
			float num2 = Segment3.DistanceSqr(vector, vector2, point, ref num3);
			if (num2 < num)
			{
				num = num2;
				laneOffset = ((float)i - 1f + num3) / 16f;
			}
			vector = vector2;
		}
		float num4 = 0.03125f;
		for (int j = 0; j < 4; j++)
		{
			Vector3 vector3 = this.m_bezier.Position(Mathf.Max(0f, laneOffset - num4));
			Vector3 vector4 = this.m_bezier.Position(laneOffset);
			Vector3 vector5 = this.m_bezier.Position(Mathf.Min(1f, laneOffset + num4));
			float num6;
			float num5 = Segment3.DistanceSqr(vector3, vector4, point, ref num6);
			float num8;
			float num7 = Segment3.DistanceSqr(vector4, vector5, point, ref num8);
			if (num5 < num7)
			{
				laneOffset = Mathf.Max(0f, laneOffset - num4 * (1f - num6));
			}
			else
			{
				laneOffset = Mathf.Min(1f, laneOffset + num4 * num8);
			}
			num4 *= 0.5f;
		}
		position = this.m_bezier.Position(laneOffset);
	}

	public Vector3 CalculatePosition(float laneOffset)
	{
		return this.m_bezier.Position(laneOffset);
	}

	public Vector3 CalculateDirection(float laneOffset)
	{
		return this.m_bezier.Tangent(laneOffset);
	}

	public void CalculatePositionAndDirection(float laneOffset, out Vector3 position, out Vector3 direction)
	{
		position = this.m_bezier.Position(laneOffset);
		direction = this.m_bezier.Tangent(laneOffset);
	}

	public void CalculateStopPositionAndDirection(float laneOffset, float stopOffset, out Vector3 position, out Vector3 direction)
	{
		position = this.m_bezier.Position(laneOffset);
		direction = this.m_bezier.Tangent(laneOffset);
		Vector3 normalized = Vector3.Cross(Vector3.get_up(), direction).get_normalized();
		position += normalized * (MathUtils.SmootherStep(0.5f, 0f, Mathf.Abs(laneOffset - 0.5f)) * stopOffset);
	}

	public float UpdateLength()
	{
		Vector3 vector = this.m_bezier.b - this.m_bezier.a;
		Vector3 vector2 = this.m_bezier.c - this.m_bezier.b;
		Vector3 vector3 = this.m_bezier.d - this.m_bezier.c;
		float magnitude = vector.get_magnitude();
		float magnitude2 = vector2.get_magnitude();
		float magnitude3 = vector3.get_magnitude();
		if (magnitude > 0.1f)
		{
			vector /= magnitude;
		}
		if (magnitude3 > 0.1f)
		{
			vector3 /= magnitude3;
		}
		this.m_length = magnitude + magnitude2 + magnitude3;
		this.m_curve = 1.57079637f * (1f - Vector3.Dot(vector, vector3));
		if (this.m_length > 0.1f)
		{
			this.m_curve /= this.m_length;
		}
		return this.m_length;
	}

	public Bezier3 GetSubCurve(float laneOffset0, float laneOffset1)
	{
		if (laneOffset0 < laneOffset1)
		{
			return this.m_bezier.Cut(laneOffset0, laneOffset1);
		}
		if (laneOffset0 > laneOffset1)
		{
			return this.m_bezier.Invert().Cut(1f - laneOffset0, 1f - laneOffset1);
		}
		Vector3 vector = this.m_bezier.Position(laneOffset0);
		return new Bezier3(vector, vector, vector, vector);
	}

	public bool CalculateGroupData(uint laneID, NetInfo.Lane laneInfo, bool destroyed, NetNode.Flags startFlags, NetNode.Flags endFlags, bool invert, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays, ref bool hasProps)
	{
		bool result = false;
		NetLaneProps laneProps = laneInfo.m_laneProps;
		if (laneProps != null && laneProps.m_props != null)
		{
			bool flag = (byte)(laneInfo.m_finalDirection & NetInfo.Direction.Both) == 2 || (byte)(laneInfo.m_finalDirection & NetInfo.Direction.AvoidBoth) == 11;
			if (flag)
			{
				NetNode.Flags flags = startFlags;
				startFlags = endFlags;
				endFlags = flags;
			}
			int num = laneProps.m_props.Length;
			for (int i = 0; i < num; i++)
			{
				NetLaneProps.Prop prop = laneProps.m_props[i];
				if (prop.CheckFlags((NetLane.Flags)this.m_flags, startFlags, endFlags))
				{
					if (this.m_length >= prop.m_minLength)
					{
						int num2 = 2;
						if (prop.m_repeatDistance > 1f)
						{
							num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_length / prop.m_repeatDistance));
						}
						PropInfo finalProp = prop.m_finalProp;
						if (finalProp != null)
						{
							hasProps = true;
							if (finalProp.m_prefabDataLayer == layer || finalProp.m_effectLayer == layer)
							{
								Randomizer randomizer;
								randomizer..ctor((int)(laneID + (uint)i));
								for (int j = 1; j <= num2; j += 2)
								{
									if (randomizer.Int32(100u) < prop.m_probability)
									{
										PropInfo variation = finalProp.GetVariation(ref randomizer);
										randomizer.Int32(10000u);
										variation.GetColor(ref randomizer);
										if ((variation.m_isDecal || !destroyed) && PropInstance.CalculateGroupData(variation, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
										{
											result = true;
										}
									}
								}
							}
						}
						if (!destroyed)
						{
							TreeInfo finalTree = prop.m_finalTree;
							if (finalTree != null)
							{
								hasProps = true;
								if (finalTree.m_prefabDataLayer == layer)
								{
									Randomizer randomizer2;
									randomizer2..ctor((int)(laneID + (uint)i));
									for (int k = 1; k <= num2; k += 2)
									{
										if (randomizer2.Int32(100u) < prop.m_probability)
										{
											finalTree.GetVariation(ref randomizer2);
											randomizer2.Int32(10000u);
											randomizer2.Int32(10000u);
											if (TreeInstance.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
											{
												result = true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return result;
	}

	public void PopulateGroupData(ushort segmentID, uint laneID, NetInfo.Lane laneInfo, bool destroyed, NetNode.Flags startFlags, NetNode.Flags endFlags, float startAngle, float endAngle, bool invert, bool terrainHeight, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool hasProps)
	{
		NetLaneProps laneProps = laneInfo.m_laneProps;
		if (laneProps != null && laneProps.m_props != null)
		{
			bool flag = (byte)(laneInfo.m_finalDirection & NetInfo.Direction.Both) == 2 || (byte)(laneInfo.m_finalDirection & NetInfo.Direction.AvoidBoth) == 11;
			bool flag2 = flag != invert;
			if (flag)
			{
				NetNode.Flags flags = startFlags;
				startFlags = endFlags;
				endFlags = flags;
			}
			int num = laneProps.m_props.Length;
			for (int i = 0; i < num; i++)
			{
				NetLaneProps.Prop prop = laneProps.m_props[i];
				if (prop.CheckFlags((NetLane.Flags)this.m_flags, startFlags, endFlags))
				{
					if (this.m_length >= prop.m_minLength)
					{
						int num2 = 2;
						if (prop.m_repeatDistance > 1f)
						{
							num2 *= Mathf.Max(1, Mathf.RoundToInt(this.m_length / prop.m_repeatDistance));
						}
						float num3 = prop.m_segmentOffset * 0.5f;
						if (this.m_length != 0f)
						{
							num3 = Mathf.Clamp(num3 + prop.m_position.z / this.m_length, -0.5f, 0.5f);
						}
						if (flag2)
						{
							num3 = -num3;
						}
						PropInfo finalProp = prop.m_finalProp;
						if (finalProp != null)
						{
							hasProps = true;
							if (finalProp.m_prefabDataLayer == layer || finalProp.m_effectLayer == layer)
							{
								Color color = Color.get_white();
								Randomizer randomizer;
								randomizer..ctor((int)(laneID + (uint)i));
								for (int j = 1; j <= num2; j += 2)
								{
									if (randomizer.Int32(100u) < prop.m_probability)
									{
										float num4 = num3 + (float)j / (float)num2;
										PropInfo variation = finalProp.GetVariation(ref randomizer);
										float scale = variation.m_minScale + (float)randomizer.Int32(10000u) * (variation.m_maxScale - variation.m_minScale) * 0.0001f;
										if (prop.m_colorMode == NetLaneProps.ColorMode.Default)
										{
											color = variation.GetColor(ref randomizer);
										}
										if (variation.m_isDecal || !destroyed)
										{
											Vector3 vector = this.m_bezier.Position(num4);
											Vector3 vector2 = this.m_bezier.Tangent(num4);
											if (vector2 != Vector3.get_zero())
											{
												if (flag2)
												{
													vector2 = -vector2;
												}
												vector2.y = 0f;
												if (prop.m_position.x != 0f)
												{
													vector2 = Vector3.Normalize(vector2);
													vector.x += vector2.z * prop.m_position.x;
													vector.z -= vector2.x * prop.m_position.x;
												}
												float num5 = Mathf.Atan2(vector2.x, -vector2.z);
												if (prop.m_cornerAngle != 0f || prop.m_position.x != 0f)
												{
													float num6 = endAngle - startAngle;
													if (num6 > 3.14159274f)
													{
														num6 -= 6.28318548f;
													}
													if (num6 < -3.14159274f)
													{
														num6 += 6.28318548f;
													}
													float num7 = startAngle + num6 * num4;
													num6 = num7 - num5;
													if (num6 > 3.14159274f)
													{
														num6 -= 6.28318548f;
													}
													if (num6 < -3.14159274f)
													{
														num6 += 6.28318548f;
													}
													num5 += num6 * prop.m_cornerAngle;
													if (num6 != 0f && prop.m_position.x != 0f)
													{
														float num8 = Mathf.Tan(num6);
														vector.x += vector2.x * num8 * prop.m_position.x;
														vector.z += vector2.z * num8 * prop.m_position.x;
													}
												}
												if (terrainHeight)
												{
													if (variation.m_requireWaterMap)
													{
														vector.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector, false, 0f);
													}
													else
													{
														vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
													}
												}
												vector.y += prop.m_position.y;
												InstanceID id = default(InstanceID);
												id.NetSegment = segmentID;
												num5 += prop.m_angle * 0.0174532924f;
												PropInstance.PopulateGroupData(variation, layer, id, vector, scale, num5, color, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
											}
										}
									}
								}
							}
						}
						if (!destroyed)
						{
							TreeInfo finalTree = prop.m_finalTree;
							if (finalTree != null)
							{
								hasProps = true;
								if (finalTree.m_prefabDataLayer == layer)
								{
									Randomizer randomizer2;
									randomizer2..ctor((int)(laneID + (uint)i));
									for (int k = 1; k <= num2; k += 2)
									{
										if (randomizer2.Int32(100u) < prop.m_probability)
										{
											float num9 = num3 + (float)k / (float)num2;
											TreeInfo variation2 = finalTree.GetVariation(ref randomizer2);
											float scale2 = variation2.m_minScale + (float)randomizer2.Int32(10000u) * (variation2.m_maxScale - variation2.m_minScale) * 0.0001f;
											float brightness = variation2.m_minBrightness + (float)randomizer2.Int32(10000u) * (variation2.m_maxBrightness - variation2.m_minBrightness) * 0.0001f;
											Vector3 vector3 = this.m_bezier.Position(num9);
											if (prop.m_position.x != 0f)
											{
												Vector3 vector4 = this.m_bezier.Tangent(num9);
												if (flag2)
												{
													vector4 = -vector4;
												}
												vector4.y = 0f;
												vector4 = Vector3.Normalize(vector4);
												vector3.x += vector4.z * prop.m_position.x;
												vector3.z -= vector4.x * prop.m_position.x;
											}
											if (terrainHeight)
											{
												vector3.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector3);
											}
											vector3.y += prop.m_position.y;
											TreeInstance.PopulateGroupData(variation2, vector3, scale2, brightness, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public Bezier3 m_bezier;

	public float m_length;

	public float m_curve;

	public uint m_nextLane;

	public ushort m_flags;

	public ushort m_segment;

	public ushort m_nodes;

	public ushort m_lastReserveID;

	public byte m_firstTarget;

	public byte m_lastTarget;

	public byte m_reservedPrev;

	public byte m_reservedNext;

	public byte m_reservedFrame;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Inverted = 4,
		JoinedJunction = 8,
		JoinedJunctionInverted = 12,
		Forward = 16,
		Left = 32,
		Right = 64,
		LeftForward = 48,
		LeftRight = 96,
		ForwardRight = 80,
		LeftForwardRight = 112,
		Stop = 256,
		Stop2 = 512,
		Stops = 768,
		YieldStart = 1024,
		YieldEnd = 2048,
		StartOneWayLeft = 4096,
		StartOneWayRight = 8192,
		EndOneWayLeft = 16384,
		EndOneWayRight = 32768,
		StartOneWayLeftInverted = 4100,
		StartOneWayRightInverted = 8196,
		EndOneWayLeftInverted = 16388,
		EndOneWayRightInverted = 32772
	}
}
