﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public sealed class CityServiceWorldInfoPanel : BuildingWorldInfoPanel
{
	public UIComponent movingPanel
	{
		get
		{
			if (this.m_MovingPanel == null)
			{
				this.m_MovingPanel = UIView.Find("MovingPanel");
				this.m_MovingPanel.Find<UIButton>("Close").add_eventClick(new MouseEventHandler(this.OnMovingPanelCloseClicked));
				this.m_MovingPanel.Hide();
			}
			return this.m_MovingPanel;
		}
	}

	protected override void Start()
	{
		base.Start();
		this.m_Type = base.Find<UILabel>("Type");
		this.m_Status = base.Find<UILabel>("Status");
		this.m_Upkeep = base.Find<UILabel>("Upkeep");
		this.m_Thumbnail = base.Find<UISprite>("Thumbnail");
		this.m_BuildingInfo = base.Find<UILabel>("Info");
		this.m_BuildingDesc = base.Find<UILabel>("Desc");
		this.m_BuildingService = base.Find<UISprite>("Service");
		this.m_BudgetButton = base.Find<UIButton>("Budget");
		this.m_EmptyButton = base.Find<UIButton>("SpecialAction");
		this.m_MoveButton = base.Find<UIButton>("RelocateAction");
		this.m_BudgetButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy));
		this.m_ActionPanel = base.Find<UIPanel>("ActionPanel");
		this.m_RebuildButton = base.Find<UIButton>("RebuildButton");
		this.m_AcceptIntercityTrains = base.Find<UICheckBox>("AcceptIntercityTrains");
		this.m_RebuildButton.set_isVisible(false);
	}

	[DebuggerHidden]
	private IEnumerator ToggleBuilding(ushort id, bool value)
	{
		CityServiceWorldInfoPanel.<ToggleBuilding>c__Iterator0 <ToggleBuilding>c__Iterator = new CityServiceWorldInfoPanel.<ToggleBuilding>c__Iterator0();
		<ToggleBuilding>c__Iterator.id = id;
		<ToggleBuilding>c__Iterator.value = value;
		return <ToggleBuilding>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator ToggleEmptying(ushort id, bool value)
	{
		CityServiceWorldInfoPanel.<ToggleEmptying>c__Iterator1 <ToggleEmptying>c__Iterator = new CityServiceWorldInfoPanel.<ToggleEmptying>c__Iterator1();
		<ToggleEmptying>c__Iterator.id = id;
		<ToggleEmptying>c__Iterator.value = value;
		return <ToggleEmptying>c__Iterator;
	}

	public bool isCityServiceEnabled
	{
		get
		{
			return Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_productionRate != 0;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && this.m_InstanceID.Building != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleBuilding(this.m_InstanceID.Building, value));
			}
		}
	}

	public bool isEmptying
	{
		get
		{
			return Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Building != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_flags & Building.Flags.Downgrading) != Building.Flags.None;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && this.m_InstanceID.Building != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleEmptying(this.m_InstanceID.Building, value));
			}
		}
	}

	public bool acceptsInterCityTrains
	{
		get
		{
			return !this.isEmptying;
		}
		set
		{
			this.isEmptying = !value;
		}
	}

	private void OnMovingPanelCloseClicked(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_IsRelocating = false;
		ToolsModifierControl.GetTool<BuildingTool>().CancelRelocate();
	}

	private void TempHide()
	{
		ToolsModifierControl.cameraController.ClearTarget();
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.33f), delegate
		{
			UIView.get_library().Hide(base.GetType().Name);
		});
		this.movingPanel.Find<UILabel>("MovingLabel").set_text(LocaleFormatter.FormatGeneric("BUILDING_MOVING", new object[]
		{
			base.buildingName
		}));
		this.movingPanel.Show();
	}

	public void TempShow(Vector3 worldPosition, InstanceID instanceID)
	{
		this.movingPanel.Hide();
		WorldInfoPanel.Show<CityServiceWorldInfoPanel>(worldPosition, instanceID);
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.33f));
	}

	private void Update()
	{
		if (this.m_IsRelocating)
		{
			BuildingTool currentTool = ToolsModifierControl.GetCurrentTool<BuildingTool>();
			if (currentTool != null && base.IsValidTarget() && currentTool.m_relocate != 0 && !this.movingPanel.get_isVisible())
			{
				this.movingPanel.Show();
				return;
			}
			if (!base.IsValidTarget() || (currentTool != null && currentTool.m_relocate == 0))
			{
				ToolsModifierControl.mainToolbar.ResetLastTool();
				this.movingPanel.Hide();
				this.m_IsRelocating = false;
			}
		}
	}

	private void RelocateCompleted(InstanceID newID)
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.m_IsRelocating = false;
		if (!newID.IsEmpty)
		{
			this.m_InstanceID = newID;
		}
		if (base.IsValidTarget())
		{
			BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
				Vector3 worldPosition;
				Quaternion quaternion;
				Vector3 vector;
				if (InstanceManager.GetPosition(this.m_InstanceID, out worldPosition, out quaternion, out vector))
				{
					worldPosition.y += vector.y * 0.8f;
				}
				this.TempShow(worldPosition, this.m_InstanceID);
			}
		}
		else
		{
			this.movingPanel.Hide();
			BuildingTool tool2 = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool2 == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
			}
			base.Hide();
		}
	}

	protected override void OnHide()
	{
		if (this.m_IsRelocating && ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.movingPanel.Hide();
	}

	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		if (this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			this.m_ShowHideRoutesButton.set_isVisible(this.CanBuildingHaveRoutes(this.m_InstanceID.Building));
		}
	}

	private bool CanBuildingHaveRoutes(ushort id)
	{
		return !Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)id].Info.m_circular && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)id].Info.m_placementMode != BuildingInfo.PlacementMode.OnWater;
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Building building2 = instance.m_buildings.m_buffer[(int)building];
			BuildingInfo info = building2.Info;
			BuildingAI buildingAI = info.m_buildingAI;
			TransportStationAI transportStationAI = buildingAI as TransportStationAI;
			if (transportStationAI != null)
			{
				TransportInfo transportLineInfo = transportStationAI.GetTransportLineInfo();
				TransportInfo secondaryTransportLineInfo = transportStationAI.GetSecondaryTransportLineInfo();
				this.m_AcceptIntercityTrains.set_isVisible((transportLineInfo != null && transportLineInfo.m_class.m_subService == ItemClass.SubService.PublicTransportTrain) || (secondaryTransportLineInfo != null && secondaryTransportLineInfo.m_class.m_subService == ItemClass.SubService.PublicTransportTrain));
			}
			else
			{
				this.m_AcceptIntercityTrains.set_isVisible(false);
			}
			this.m_Type.set_text(Singleton<BuildingManager>.get_instance().GetDefaultBuildingName(building, InstanceID.Empty));
			this.m_Status.set_text(buildingAI.GetLocalizedStatus(building, ref instance.m_buildings.m_buffer[(int)this.m_InstanceID.Building]));
			this.m_Upkeep.set_text(LocaleFormatter.FormatUpkeep(buildingAI.GetResourceRate(building, ref instance.m_buildings.m_buffer[(int)building], EconomyManager.Resource.Maintenance), false));
			this.m_Thumbnail.set_atlas(info.m_Atlas);
			this.m_Thumbnail.set_spriteName(info.m_Thumbnail);
			if (this.m_Thumbnail.get_atlas() != null && !string.IsNullOrEmpty(this.m_Thumbnail.get_spriteName()))
			{
				UITextureAtlas.SpriteInfo spriteInfo = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_spriteName());
				if (spriteInfo != null)
				{
					this.m_Thumbnail.set_size(spriteInfo.get_pixelSize());
				}
			}
			this.m_BuildingDesc.set_text(info.GetLocalizedDescriptionShort());
			this.m_BuildingInfo.set_text(buildingAI.GetLocalizedStats(building, ref instance.m_buildings.m_buffer[(int)building]));
			ItemClass.Service service = info.GetService();
			if (service == ItemClass.Service.Disaster)
			{
				service = ItemClass.Service.FireDepartment;
			}
			if (service != ItemClass.Service.None)
			{
				string nameByValue = Utils.GetNameByValue<ItemClass.Service>(service, "Game");
				this.m_BuildingService.set_spriteName("ToolbarIcon" + nameByValue);
				this.m_BuildingService.set_tooltip(Locale.Get("MAIN_TOOL", nameByValue));
			}
			this.m_BuildingService.set_isVisible(service != ItemClass.Service.None);
			this.m_EmptyButton.set_isVisible(buildingAI != null && buildingAI.CanBeEmptied());
			this.m_EmptyButton.set_isEnabled(buildingAI != null && buildingAI.CanBeEmptied(building, ref instance.m_buildings.m_buffer[(int)building]));
			this.m_MoveButton.set_isEnabled(buildingAI != null && buildingAI.CanBeRelocated(building, ref instance.m_buildings.m_buffer[(int)building]));
			this.SetSpecialActionButtonImages();
			if ((building2.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				this.m_RebuildButton.set_tooltip((!this.IsDisasterServiceRequired()) ? LocaleFormatter.FormatCost(buildingAI.GetRelocationCost(), false) : Locale.Get("CITYSERVICE_TOOLTIP_DISASTERSERVICEREQUIRED"));
				this.m_RebuildButton.set_isVisible(Singleton<LoadingManager>.get_instance().SupportsExpansion(2));
				this.m_RebuildButton.set_isEnabled(this.CanRebuild());
				this.m_ActionPanel.set_isVisible(false);
			}
			else
			{
				this.m_RebuildButton.set_isVisible(false);
				this.m_ActionPanel.set_isVisible(true);
			}
		}
		this.m_BudgetButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy));
	}

	public void SetSpecialActionButtonImages()
	{
		this.m_EmptyButton.set_tooltipLocaleID((!this.isEmptying) ? "CITYSERVICE_EMPTY" : "CITYSERVICE_STOPEMPTY");
		this.m_EmptyButton.set_normalFgSprite((!this.isEmptying) ? "EmptyIcon" : "StopEmptyingIcon");
	}

	public void OnBudgetClicked()
	{
		if (ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy))
		{
			ToolsModifierControl.mainToolbar.ShowEconomyPanel(1);
			WorldInfoPanel.Hide<CityServiceWorldInfoPanel>();
		}
	}

	public void OnSpecialActionClicked()
	{
		this.isEmptying = !this.isEmptying;
	}

	public void OnRelocateBuilding()
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted += new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		ToolsModifierControl.keepThisWorldInfoPanel = true;
		BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
		buildingTool.m_prefab = null;
		buildingTool.m_relocate = (int)this.m_InstanceID.Building;
		this.m_IsRelocating = true;
		this.TempHide();
	}

	public void OnRebuildClicked()
	{
		ushort buildingID = this.m_InstanceID.Building;
		if (buildingID != 0)
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)buildingID].Info;
				if (info != null && (instance.m_buildings.m_buffer[(int)buildingID].m_flags & Building.Flags.Collapsed) != Building.Flags.None)
				{
					int relocationCost = info.m_buildingAI.GetRelocationCost();
					Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Construction, relocationCost, info.m_class);
					Vector3 position = instance.m_buildings.m_buffer[(int)buildingID].m_position;
					float angle = instance.m_buildings.m_buffer[(int)buildingID].m_angle;
					this.RebuildBuilding(info, position, angle, buildingID, info.m_fixedHeight);
					if (info.m_subBuildings != null && info.m_subBuildings.Length != 0)
					{
						Matrix4x4 matrix4x = default(Matrix4x4);
						matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
						for (int i = 0; i < info.m_subBuildings.Length; i++)
						{
							BuildingInfo buildingInfo = info.m_subBuildings[i].m_buildingInfo;
							Vector3 position2 = matrix4x.MultiplyPoint(info.m_subBuildings[i].m_position);
							float angle2 = info.m_subBuildings[i].m_angle * 0.0174532924f + angle;
							bool fixedHeight = info.m_subBuildings[i].m_fixedHeight;
							ushort num = this.RebuildBuilding(buildingInfo, position2, angle2, 0, fixedHeight);
							if (buildingID != 0 && num != 0)
							{
								instance.m_buildings.m_buffer[(int)buildingID].m_subBuilding = num;
								instance.m_buildings.m_buffer[(int)num].m_parentBuilding = buildingID;
								Building[] expr_1CD_cp_0 = instance.m_buildings.m_buffer;
								ushort expr_1CD_cp_1 = num;
								expr_1CD_cp_0[(int)expr_1CD_cp_1].m_flags = (expr_1CD_cp_0[(int)expr_1CD_cp_1].m_flags | Building.Flags.Untouchable);
								buildingID = num;
							}
						}
					}
				}
			});
		}
	}

	private ushort RebuildBuilding(BuildingInfo info, Vector3 position, float angle, ushort buildingID, bool fixedHeight)
	{
		ushort num = 0;
		bool flag = false;
		if (buildingID != 0)
		{
			Singleton<BuildingManager>.get_instance().RelocateBuilding(buildingID, position, angle);
			num = buildingID;
			flag = true;
		}
		else if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, position, angle, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			if (fixedHeight)
			{
				Building[] expr_68_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
				ushort expr_68_cp_1 = num;
				expr_68_cp_0[(int)expr_68_cp_1].m_flags = (expr_68_cp_0[(int)expr_68_cp_1].m_flags | Building.Flags.FixedHeight);
			}
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			flag = true;
		}
		if (flag)
		{
			int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
			if (publicServiceIndex != -1)
			{
				Singleton<BuildingManager>.get_instance().m_buildingDestroyed2.Disable();
				Singleton<GuideManager>.get_instance().m_serviceNotUsed[publicServiceIndex].Disable();
				Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Deactivate();
				Singleton<CoverageManager>.get_instance().CoverageUpdated(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			}
			BuildingTool.DispatchPlacementEffect(info, 0, position, angle, info.m_cellWidth, info.m_cellLength, false, false);
		}
		return num;
	}

	public bool CanRebuild()
	{
		ushort building = this.m_InstanceID.Building;
		if (building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_levelUpProgress == 255)
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].Info;
			if (info != null)
			{
				int relocationCost = info.m_buildingAI.GetRelocationCost();
				if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, relocationCost) == relocationCost)
				{
					return true;
				}
			}
		}
		return false;
	}

	private bool IsDisasterServiceRequired()
	{
		ushort building = this.m_InstanceID.Building;
		return building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_levelUpProgress != 255;
	}

	private UIButton m_BudgetButton;

	private UIButton m_EmptyButton;

	private UIButton m_MoveButton;

	private UIPanel m_ActionPanel;

	private UIButton m_RebuildButton;

	private UIComponent m_MovingPanel;

	private UILabel m_Type;

	private UILabel m_Status;

	private UILabel m_Upkeep;

	private UISprite m_Thumbnail;

	private UILabel m_BuildingInfo;

	private UILabel m_BuildingDesc;

	private UISprite m_BuildingService;

	private UICheckBox m_AcceptIntercityTrains;

	private bool m_IsRelocating;
}
