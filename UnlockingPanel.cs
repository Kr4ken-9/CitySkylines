﻿using System;
using System.Collections;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class UnlockingPanel : ToolsModifierControl
{
	private void SetCurrentMilestone()
	{
		this.SetCurrentMilestone(false);
	}

	private void SetCurrentMilestone(bool justUnlocked)
	{
		MilestoneInfo currentMilestone = this.m_CurrentMilestone;
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo currentMilestone2 = Singleton<UnlockManager>.get_instance().GetCurrentMilestone();
			if (currentMilestone2 != null)
			{
				this.m_CurrentMilestone = ((!justUnlocked) ? currentMilestone2 : currentMilestone2.m_prevMilestone);
			}
			else
			{
				this.m_CurrentMilestone = Singleton<UnlockManager>.get_instance().m_properties.m_progressionMilestones[Singleton<UnlockManager>.get_instance().m_properties.m_progressionMilestones.Length - 1];
			}
		}
		if (this.m_CurrentMilestone != currentMilestone)
		{
			this.RefreshMilestonesPanel();
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.CacheLoadedAssets();
		this.RefreshMonumentsPanel();
		this.PopulateWonders();
		this.SetCurrentMilestone();
		this.RefreshGoalsVisibility();
	}

	private bool isScenarioGame
	{
		get
		{
			return !string.IsNullOrEmpty(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset);
		}
	}

	protected void RefreshGoalsVisibility()
	{
		bool isScenarioGame = this.isScenarioGame;
		this.m_tabstrip.set_selectedIndex((!isScenarioGame) ? 0 : (this.m_tabstrip.get_tabCount() - 1));
		this.m_goalsTabGameObject.SetActive(isScenarioGame);
		this.m_goalsContainerGameObject.SetActive(isScenarioGame);
	}

	protected static string GetUnlockText(ItemClass.Service service)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[(int)service];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	protected static string GetUnlockText(UnlockManager.Feature feature)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	private void MilestoneUpdated()
	{
		UIComponent uIComponent = this.m_Tabstrip.Find("Monuments");
		UIComponent uIComponent2 = this.m_Tabstrip.Find("Wonders");
		bool flag = ToolsModifierControl.IsUnlocked(ItemClass.Service.Monument);
		bool flag2 = ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Wonders);
		uIComponent.set_isEnabled(flag);
		uIComponent2.set_isEnabled(flag2);
		uIComponent.set_tooltip((!flag) ? UnlockingPanel.GetUnlockText(ItemClass.Service.Monument) : string.Empty);
		uIComponent2.set_tooltip((!flag2) ? UnlockingPanel.GetUnlockText(UnlockManager.Feature.Wonders) : string.Empty);
		if (Singleton<SimulationManager>.get_exists())
		{
			this.m_AchievementsTab.set_isEnabled(Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True);
		}
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.MilestoneUpdated);
		}
		PlatformService.get_achievements().add_eventRefresh(new Achievements.RefreshHandler(this.RefreshAchievements));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.MilestoneUpdated);
		}
		PlatformService.get_achievements().remove_eventRefresh(new Achievements.RefreshHandler(this.RefreshAchievements));
	}

	private void OnLocaleChanged()
	{
		this.MilestoneUpdated();
		this.RefreshMonumentsPanel();
		this.PopulateWonders();
		this.RefreshMilestonesPanel();
		this.RefreshAchievements();
	}

	private void Awake()
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().RegisterUnlockingPanel(this);
		}
		this.m_StartPosition = base.get_component().get_transform().get_position();
		this.m_StartSize = base.get_component().get_size();
		this.m_TSBar = UIView.Find("TSBar");
		this.m_Trigger = this.m_TSBar.Find<UIButton>("UnlockButton");
		this.m_Tabstrip = base.Find<UITabstrip>("Tabstrip");
		this.m_Congrats = base.Find<UITextureSprite>("Congrats");
		Renderer[] componentsInChildren = this.m_Fireworks.GetComponentsInChildren<Renderer>();
		this.m_FireworkMaterials = new Material[componentsInChildren.Length];
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			this.m_FireworkMaterials[i] = componentsInChildren[i].get_material();
		}
		this.m_MonumentsContainer = base.Find<UIScrollablePanel>("MonumentContainer");
		this.m_MonumentsContainer.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnMilestoneGroupChanged));
		this.m_WondersContainer = base.Find<UIScrollablePanel>("WonderContainer");
		this.m_WondersContainer.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnMilestoneGroupChanged));
		this.m_MilestoneContainer = base.Find<UIScrollablePanel>("MilestoneContainer");
		this.m_MilestoneContainer.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnMilestoneGroupChanged));
		UIComponent uIComponent = base.Find("MilestoneOverview");
		this.m_MilestoneTitle = uIComponent.Find<UILabel>("MilestoneTitle");
		this.m_MilestoneDesc = uIComponent.Find<UILabel>("MilestoneDesc");
		this.m_MilestoneProgress = uIComponent.Find<UIProgressBar>("MilestoneProgress");
		this.m_MilestoneProgressText = uIComponent.Find<UILabel>("MilestoneProgressText");
		UIComponent uIComponent2 = base.Find("MonumentsOverview");
		this.m_MonumentTitle = uIComponent2.Find<UILabel>("MilestoneTitle");
		this.m_MonumentDesc = uIComponent2.Find<UILabel>("MilestoneDesc");
		this.m_MonumentProgress = uIComponent2.Find<UIProgressBar>("MilestoneProgress");
		this.m_MonumentProgressText = uIComponent2.Find<UILabel>("MilestoneProgressText");
		this.m_AchievementsTab = base.Find<UIButton>("Achievements");
		this.m_AchievementsContainer = base.Find<UIScrollablePanel>("AchievementContainer");
		this.m_AchievementsContainer.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnMilestoneGroupChanged));
		base.get_component().add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnMilestonePanelVisible));
		this.RefreshAchievements();
		this.m_tabstrip = base.Find<UITabstrip>("Tabstrip");
		UIComponent uIComponent3 = this.m_tabstrip.Find<UIComponent>("Goals");
		this.m_goalsTabGameObject = uIComponent3.get_gameObject();
		UIComponent uIComponent4 = base.Find<UITabContainer>("TabContainer").Find<UIComponent>("Goals");
		this.m_goalsContainerGameObject = uIComponent4.get_gameObject();
		base.get_component().Hide();
	}

	private void OnMilestonePanelVisible(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UITabContainer tabPages = this.m_Tabstrip.get_tabPages();
			tabPages.get_components()[tabPages.get_selectedIndex()].Focus();
		}
	}

	private void OnMilestoneGroupChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			comp.Focus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	public void Toggle()
	{
		if (base.get_component().get_isVisible())
		{
			this.Hide();
		}
		else
		{
			this.Show();
		}
	}

	public bool canPreviousMilestone
	{
		get
		{
			return this.m_CurrentMilestone != null && this.m_CurrentMilestone.m_prevMilestone != null;
		}
	}

	public bool canNextMilestone
	{
		get
		{
			return Singleton<UnlockManager>.get_exists() && this.m_CurrentMilestone != null && this.m_CurrentMilestone.m_nextMilestone != null;
		}
	}

	public void PreviousMilestone()
	{
		if (Singleton<UnlockManager>.get_exists() && this.canPreviousMilestone && this.m_CurrentMilestone != null && this.m_CurrentMilestone.m_prevMilestone != null)
		{
			this.m_CurrentMilestone = this.m_CurrentMilestone.m_prevMilestone;
			this.RefreshMilestonesPanel();
		}
	}

	public void NextMilestone()
	{
		if (Singleton<UnlockManager>.get_exists() && this.canNextMilestone && this.m_CurrentMilestone != null && this.m_CurrentMilestone.m_nextMilestone != null)
		{
			this.m_CurrentMilestone = this.m_CurrentMilestone.m_nextMilestone;
			this.RefreshMilestonesPanel();
		}
	}

	private int GetMonumentCategoryOrder(string name)
	{
		switch (name)
		{
		case "MonumentLandmarks":
			return 0;
		case "MonumentExpansion1":
			return 1;
		case "MonumentExpansion2":
			return 2;
		case "MonumentFootball":
			return 50;
		case "MonumentConcerts":
			return 80;
		case "MonumentCategory1":
			return 100;
		case "MonumentCategory2":
			return 101;
		case "MonumentCategory3":
			return 102;
		case "MonumentCategory4":
			return 103;
		case "MonumentCategory5":
			return 104;
		case "MonumentCategory6":
			return 105;
		case "MonumentModderPack":
			return 200;
		}
		return -1;
	}

	private MilestoneInfo GetMonumentCategoryMilestone(string name)
	{
		switch (name)
		{
		case "MonumentLandmarks":
			return Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[17];
		case "MonumentExpansion1":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[23];
		case "MonumentExpansion2":
			return Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[17];
		case "MonumentFootball":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[26];
		case "MonumentConcerts":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[31];
		case "MonumentCategory1":
			return Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[17];
		case "MonumentCategory2":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[14];
		case "MonumentCategory3":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[15];
		case "MonumentCategory4":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[16];
		case "MonumentCategory5":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[17];
		case "MonumentCategory6":
			return Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[18];
		case "MonumentModderPack":
			return Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[17];
		}
		return null;
	}

	protected int MonumentCategorySort(string a, string b)
	{
		return this.GetMonumentCategoryOrder(a).CompareTo(this.GetMonumentCategoryOrder(b));
	}

	private void CacheLoadedAssets()
	{
		HashSet<string> hashSet = new HashSet<string>();
		this.m_Monuments.Clear();
		this.m_Wonders.Clear();
		uint num = 0u;
		while ((ulong)num < (ulong)((long)PrefabCollection<BuildingInfo>.LoadedCount()))
		{
			BuildingInfo loaded = PrefabCollection<BuildingInfo>.GetLoaded(num);
			if (loaded != null && loaded.m_placementStyle == ItemClass.Placement.Manual)
			{
				if (loaded.m_class.m_service == ItemClass.Service.Monument && loaded.m_UnlockMilestone != null)
				{
					this.m_Monuments.Add(loaded);
					if (!hashSet.Contains(loaded.category) && loaded.category != "Default")
					{
						hashSet.Add(loaded.category);
					}
				}
				if (loaded.m_buildingAI.IsWonder())
				{
					this.m_Wonders.Add(loaded);
				}
			}
			num += 1u;
		}
		this.m_MonumentCategories = new string[hashSet.Count];
		int num2 = 0;
		foreach (string current in hashSet)
		{
			this.m_MonumentCategories[num2++] = current;
		}
		Array.Sort<string>(this.m_MonumentCategories, new Comparison<string>(this.MonumentCategorySort));
		this.m_MonumentsCategoryMilestoneInfo = new MilestoneInfo[this.m_MonumentCategories.Length];
		for (int i = 0; i < this.m_MonumentCategories.Length; i++)
		{
			this.m_MonumentsCategoryMilestoneInfo[i] = this.GetMonumentCategoryMilestone(this.m_MonumentCategories[i]);
		}
		this.m_CurrentMonumentCategory = 0;
	}

	public bool canPreviousMonumentCategory
	{
		get
		{
			return this.m_CurrentMonumentCategory > 0;
		}
	}

	public bool canNextMonumentCategory
	{
		get
		{
			return this.m_CurrentMonumentCategory != -1 && this.m_CurrentMonumentCategory < this.m_MonumentCategories.Length - 1;
		}
	}

	public void PreviousMonumentCategory()
	{
		if (this.canPreviousMonumentCategory)
		{
			this.m_CurrentMonumentCategory--;
			this.RefreshMonumentsPanel();
		}
	}

	public void NextMonumentCategory()
	{
		if (this.canNextMonumentCategory)
		{
			this.m_CurrentMonumentCategory++;
			this.RefreshMonumentsPanel();
		}
	}

	private void RefreshMonumentsPanel()
	{
		if (this.m_CurrentMonumentCategory != -1)
		{
			MilestoneInfo milestoneInfo = this.m_MonumentsCategoryMilestoneInfo[this.m_CurrentMonumentCategory];
			if (milestoneInfo != null)
			{
				MilestoneInfo.ProgressInfo localizedProgress = milestoneInfo.GetLocalizedProgress();
				this.m_MonumentTitle.set_text(Locale.Get("UNLOCK_MONUMENT_TITLE", this.m_MonumentCategories[this.m_CurrentMonumentCategory]));
				this.m_MonumentDesc.set_text(localizedProgress.m_description);
				UITemplateManager.ClearInstances(UnlockingPanel.kMonumentRequirementTemplate);
				UITemplateManager.ClearInstances(UnlockingPanel.kUnlockMonumentTemplate);
				for (int i = 0; i < this.m_Monuments.Count; i++)
				{
					BuildingInfo buildingInfo = this.m_Monuments[i];
					ItemClass @class = buildingInfo.m_class;
					if (this.m_MonumentCategories[this.m_CurrentMonumentCategory] == buildingInfo.category)
					{
						this.AddUnlockMonument(UnlockingPanel.kUnlockMonumentTemplate, buildingInfo.get_name(), buildingInfo.m_Atlas, buildingInfo.m_Thumbnail, "ToolbarIcon" + Utils.GetNameByValue<ItemClass.Service>(@class.m_service, "Game"), buildingInfo.m_UnlockMilestone, buildingInfo.GetLocalizedDescription(), this.m_MonumentsCategoryMilestoneInfo[this.m_CurrentMonumentCategory]);
					}
				}
			}
			else
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Monument categories are incorrect. This may be caused by an old workshop asset.");
			}
		}
	}

	private void PopulateWonders()
	{
		UITemplateManager.ClearInstances(UnlockingPanel.kWonderRequirementTemplate);
		UITemplateManager.ClearInstances(UnlockingPanel.kUnlockWonderTemplate);
		for (int i = 0; i < this.m_Wonders.Count; i++)
		{
			BuildingInfo buildingInfo = this.m_Wonders[i];
			ItemClass @class = buildingInfo.m_class;
			string text = Utils.GetNameByValue<ItemClass.Service>(@class.m_service, "Game");
			if (string.IsNullOrEmpty(text) && @class.m_service == ItemClass.Service.Disaster)
			{
				text = "FireDepartment";
			}
			this.AddUnlockWonder(UnlockingPanel.kUnlockWonderTemplate, buildingInfo.get_name(), buildingInfo.m_Atlas, buildingInfo.m_Thumbnail, "ToolbarIcon" + text, buildingInfo.m_UnlockMilestone, null);
		}
	}

	private void RefreshMilestonesPanel()
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			if (this.m_CurrentMilestone != null)
			{
				string text = this.m_CurrentMilestone.GetLocalizedProgress().m_description;
				if (text == null)
				{
					text = "Description missing";
				}
				this.m_MilestoneTitle.set_text(this.m_CurrentMilestone.GetLocalizedName());
				this.m_MilestoneDesc.set_text(text);
			}
			else
			{
				this.m_MilestoneTitle.set_text(string.Empty);
				this.m_MilestoneDesc.set_text(string.Empty);
			}
			UITemplateManager.ClearInstances(UnlockingPanel.kUnlockItemTemplate);
			UITemplateManager.ClearInstances(UnlockingPanel.kUnlockServiceTemplate);
			UITemplateManager.ClearInstances(UnlockingPanel.kUnlockTitleTemplate);
			int num = PrefabCollection<BuildingInfo>.LoadedCount();
			int num2 = PrefabCollection<NetInfo>.LoadedCount();
			List<PrefabInfo> list = new List<PrefabInfo>(num + num2);
			uint num3 = 0u;
			while ((ulong)num3 < (ulong)((long)num))
			{
				BuildingInfo loaded = PrefabCollection<BuildingInfo>.GetLoaded(num3);
				if (loaded != null && loaded.m_placementStyle == ItemClass.Placement.Manual && loaded.GetUnlockMilestone() == this.m_CurrentMilestone)
				{
					list.Add(loaded);
				}
				num3 += 1u;
			}
			uint num4 = 0u;
			while ((ulong)num4 < (ulong)((long)num2))
			{
				NetInfo loaded2 = PrefabCollection<NetInfo>.GetLoaded(num4);
				if (loaded2 != null && loaded2.m_placementStyle == ItemClass.Placement.Manual && loaded2.m_UnlockMilestone == this.m_CurrentMilestone)
				{
					list.Add(loaded2);
				}
				num4 += 1u;
			}
			bool flag = true;
			int maxAreaCount = Singleton<GameAreaManager>.get_instance().MaxAreaCount;
			for (int i = 0; i < maxAreaCount; i++)
			{
				MilestoneInfo[] areaMilestones = Singleton<UnlockManager>.get_instance().m_properties.m_AreaMilestones;
				if (areaMilestones[Mathf.Clamp(i, 0, areaMilestones.Length - 1)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_AREAS"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, "UnlockArea", Locale.Get("UNLOCK_AREA_TITLE"), base.get_component().GetUIView().get_defaultAtlas(), "ToolbarIconZoomOutGlobe", Locale.Get("AREA_DESC", Mathf.Clamp(i, 0, (int)(Locale.Count("AREA_DESC") - 1u))));
				}
			}
			flag = true;
			for (int j = 0; j < this.kFeatures.Length; j++)
			{
				PositionData<UnlockManager.Feature> feature = this.kFeatures[j];
				if (Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_FEATURES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, feature);
				}
			}
			for (int k = 0; k < this.kPolicyTypes.Length; k++)
			{
				PositionData<DistrictPolicies.Types> type = this.kPolicyTypes[k];
				if (Singleton<DistrictManager>.get_instance().IsAnyPolicyLoaded(type.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_PolicyTypeMilestones[(int)type.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_FEATURES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, type);
				}
			}
			flag = true;
			for (int l = 0; l < this.kServices.Length; l++)
			{
				PositionData<ItemClass.Service> service = this.kServices[l];
				if (Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[(int)service.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SERVICES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, service);
				}
			}
			for (int m = 0; m < this.kServiceFeatures.Length; m++)
			{
				PositionData<UnlockManager.Feature> feature2 = this.kServiceFeatures[m];
				if (Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature2.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (feature2.get_enumValue() != UnlockManager.Feature.Football || Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.MatchSecurity))
					{
						if ((feature2.get_enumValue() != UnlockManager.Feature.Ferry && feature2.get_enumValue() != UnlockManager.Feature.Blimp) || SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
						{
							if (feature2.get_enumValue() != UnlockManager.Feature.Concerts || SteamHelper.IsDLCOwned(SteamHelper.DLC.MusicFestival))
							{
								if (flag)
								{
									flag = false;
									this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SERVICES"));
								}
								this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, feature2);
							}
						}
					}
				}
			}
			for (int n = 0; n < this.kSubServices.Length; n++)
			{
				PositionData<ItemClass.SubService> service2 = this.kSubServices[n];
				if (Singleton<TransportManager>.get_instance().TransportServiceLoaded(service2.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SubServiceMilestones[(int)service2.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SERVICES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, service2);
				}
			}
			flag = true;
			for (int num5 = 0; num5 < this.kIndustryPolicies.Length; num5++)
			{
				PositionData<DistrictPolicies.Policies> policy = this.kIndustryPolicies[num5];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policy.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SPECIALIZATION"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy);
				}
			}
			flag = true;
			for (int num6 = 0; num6 < this.kResidentialPolicies.Length; num6++)
			{
				PositionData<DistrictPolicies.Policies> policy2 = this.kResidentialPolicies[num6];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy2.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policy2.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SPECIALIZATION_RESIDENTIAL"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy2);
				}
			}
			flag = true;
			for (int num7 = 0; num7 < this.kOfficePolicies.Length; num7++)
			{
				PositionData<DistrictPolicies.Policies> policy3 = this.kOfficePolicies[num7];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy3.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policy3.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SPECIALIZATION_OFFICE"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy3);
				}
			}
			flag = true;
			for (int num8 = 0; num8 < this.kCommercialPolicies.Length; num8++)
			{
				PositionData<DistrictPolicies.Policies> policy4 = this.kCommercialPolicies[num8];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy4.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policy4.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_SPECIALIZATION_COMMERCIAL"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy4);
				}
			}
			flag = true;
			for (int num9 = 0; num9 < this.kServicesPolicies.Length; num9++)
			{
				PositionData<DistrictPolicies.Policies> policy5 = this.kServicesPolicies[num9];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy5.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_ServicePolicyMilestones[(int)(policy5.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_POLICIES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy5);
				}
			}
			for (int num10 = 0; num10 < this.kTaxationPolicies.Length; num10++)
			{
				PositionData<DistrictPolicies.Policies> policy6 = this.kTaxationPolicies[num10];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy6.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_TaxationPolicyMilestones[(int)(policy6.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_POLICIES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy6);
				}
			}
			for (int num11 = 0; num11 < this.kCityPlanningPolicies.Length; num11++)
			{
				PositionData<DistrictPolicies.Policies> policy7 = this.kCityPlanningPolicies[num11];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy7.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_CityPlanningPolicyMilestones[(int)(policy7.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_POLICIES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy7);
				}
			}
			for (int num12 = 0; num12 < this.kSpecialPolicies.Length; num12++)
			{
				PositionData<DistrictPolicies.Policies> policy8 = this.kSpecialPolicies[num12];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy8.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_SpecialPolicyMilestones[(int)(policy8.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_POLICIES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy8);
				}
			}
			for (int num13 = 0; num13 < this.kEventPolicies.Length; num13++)
			{
				PositionData<DistrictPolicies.Policies> policy9 = this.kEventPolicies[num13];
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(policy9.get_enumValue()) && Singleton<UnlockManager>.get_instance().m_properties.m_EventPolicyMilestones[(int)(policy9.get_enumValue() & (DistrictPolicies.Policies)31)] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_POLICIES"));
					}
					this.AddUnlockService(UnlockingPanel.kUnlockServiceTemplate, policy9);
				}
			}
			flag = true;
			for (int num14 = 0; num14 < this.kZones.Length; num14++)
			{
				PositionData<ItemClass.Zone> type2 = this.kZones[num14];
				if (Singleton<UnlockManager>.get_instance().m_properties.m_ZoneMilestones[(int)type2.get_enumValue()] == this.m_CurrentMilestone)
				{
					if (flag)
					{
						flag = false;
						this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_ZONES"));
					}
					this.AddUnlockItem(UnlockingPanel.kUnlockItemTemplate, type2.get_enumName(), type2.GetLocalizedName(), null, "Zoning" + type2.get_enumName(), "ToolbarIconZoning", type2.GetLocalizedDescription());
				}
			}
			flag = true;
			for (int num15 = 0; num15 < list.Count; num15++)
			{
				PrefabInfo prefabInfo = list[num15];
				PrefabAI aI = prefabInfo.GetAI();
				if (aI.WorksAsNet())
				{
					ItemClass.Service service3 = prefabInfo.GetService();
					if ((service3 != ItemClass.Service.None && service3 != ItemClass.Service.Water && service3 != ItemClass.Service.Beautification) || (service3 == ItemClass.Service.Beautification && prefabInfo.GetClassLevel() <= ItemClass.Level.Level2))
					{
						if (flag)
						{
							flag = false;
							this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_ROADS"));
						}
						this.AddUnlockItem(UnlockingPanel.kUnlockItemTemplate, prefabInfo);
					}
				}
			}
			flag = true;
			for (int num16 = 0; num16 < list.Count; num16++)
			{
				PrefabInfo prefabInfo2 = list[num16];
				PrefabAI aI2 = prefabInfo2.GetAI();
				if (aI2.WorksAsNet())
				{
					ItemClass.Service service4 = prefabInfo2.GetService();
					if (service4 == ItemClass.Service.Water && prefabInfo2.GetClassLevel() <= ItemClass.Level.Level2)
					{
						if (flag)
						{
							flag = false;
							this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_PIPES"));
						}
						this.AddUnlockItem(UnlockingPanel.kUnlockItemTemplate, prefabInfo2);
					}
				}
			}
			flag = true;
			for (int num17 = 0; num17 < list.Count; num17++)
			{
				PrefabInfo prefabInfo3 = list[num17];
				PrefabAI aI3 = prefabInfo3.GetAI();
				if (aI3.WorksAsNet())
				{
					ItemClass.Service service5 = prefabInfo3.GetService();
					if (service5 == ItemClass.Service.Beautification && prefabInfo3.GetClassLevel() >= ItemClass.Level.Level3)
					{
						if (flag)
						{
							flag = false;
							this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_CANALS"));
						}
						this.AddUnlockItem(UnlockingPanel.kUnlockItemTemplate, prefabInfo3);
					}
				}
			}
			flag = true;
			for (int num18 = 0; num18 < list.Count; num18++)
			{
				PrefabInfo prefabInfo4 = list[num18];
				PrefabAI aI4 = prefabInfo4.GetAI();
				if (aI4.WorksAsBuilding())
				{
					ItemClass.Service service6 = prefabInfo4.GetService();
					if (service6 != ItemClass.Service.None)
					{
						if (flag)
						{
							flag = false;
							this.AddUnlockTitle(UnlockingPanel.kUnlockTitleTemplate, Locale.Get("UNLOCK_BUILDINGS"));
						}
						this.AddUnlockItem(UnlockingPanel.kUnlockItemTemplate, prefabInfo4);
					}
				}
			}
		}
	}

	private void AddUnlockTitle(string template, string title)
	{
		GameObject asGameObject = UITemplateManager.GetAsGameObject(template);
		asGameObject.set_name(base.get_name());
		UILabel uILabel = this.m_MilestoneContainer.AttachUIComponent(asGameObject) as UILabel;
		uILabel.set_text(title);
	}

	private void AddUnlockMonument(string template, string name, UITextureAtlas atlas, string thumbnail, string group, MilestoneInfo info, string tooltip, MilestoneInfo topLevelInfo)
	{
		if (info == null)
		{
			return;
		}
		GameObject asGameObject = UITemplateManager.GetAsGameObject(template);
		asGameObject.set_name(name);
		UIPanel item = this.m_MonumentsContainer.AttachUIComponent(asGameObject) as UIPanel;
		item.set_objectUserData(info);
		if (tooltip != null)
		{
			item.set_tooltip(tooltip);
		}
		UILabel uILabel = item.Find<UILabel>("Name");
		UISprite uISprite = item.Find<UISprite>("Service");
		UISprite uISprite2 = item.Find<UISprite>("Thumbnail");
		UILabel uILabel2 = item.Find<UILabel>("Description");
		UIComponent uIComponent = item.Find("Requirements");
		uIComponent.set_isVisible(ToolsModifierControl.IsUnlocked(topLevelInfo));
		item.Find<UILabel>("ReqLabel").set_text(Locale.Get((!uIComponent.get_isVisible()) ? "UNLOCK_REQUIREMENTS_HIDDEN" : "UNLOCK_REQUIREMENTS"));
		uIComponent.add_eventFitChildren(delegate
		{
			item.FitChildrenVertically(10f);
		});
		FastList<MilestoneInfo.ProgressInfo> fastList;
		MilestoneInfo.ProgressInfo localizedProgress = info.GetLocalizedProgress(out fastList);
		bool flag = info.IsPassed();
		if (fastList.m_size == 0)
		{
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(UnlockingPanel.kMonumentRequirementTemplate);
			asGameObject2.set_name(name + "Requirements");
			UIComponent uIComponent2 = uIComponent.AttachUIComponent(asGameObject2);
			UILabel uILabel3 = uIComponent2.Find<UILabel>("ReqDesc");
			UICheckBox uICheckBox = uIComponent2.Find<UICheckBox>("ReqPassed");
			UILabel uILabel4 = uIComponent2.Find<UILabel>("ReqProgressText");
			UIProgressBar uIProgressBar = uIComponent2.Find<UIProgressBar>("ReqProgress");
			uILabel3.set_text(localizedProgress.m_description);
			uICheckBox.set_isChecked(flag);
			uILabel4.set_text(localizedProgress.m_progress);
			UIComponent arg_1EE_0 = uILabel4;
			bool isVisible = !flag && !Mathf.Approximately(localizedProgress.m_max, 1f) && localizedProgress.m_max > 1f;
			uIProgressBar.set_isVisible(isVisible);
			arg_1EE_0.set_isVisible(isVisible);
			uIProgressBar.set_minValue(localizedProgress.m_min);
			uIProgressBar.set_maxValue(localizedProgress.m_max);
			uIProgressBar.set_value(localizedProgress.m_current);
			uIComponent2.FitChildrenVertically();
		}
		else
		{
			for (int i = 0; i < fastList.m_size; i++)
			{
				GameObject asGameObject3 = UITemplateManager.GetAsGameObject(UnlockingPanel.kMonumentRequirementTemplate);
				asGameObject3.set_name(name + "Requirements" + i);
				UIComponent uIComponent3 = uIComponent.AttachUIComponent(asGameObject3);
				UILabel uILabel5 = uIComponent3.Find<UILabel>("ReqDesc");
				UICheckBox uICheckBox2 = uIComponent3.Find<UICheckBox>("ReqPassed");
				UILabel uILabel6 = uIComponent3.Find<UILabel>("ReqProgressText");
				UIProgressBar uIProgressBar2 = uIComponent3.Find<UIProgressBar>("ReqProgress");
				uILabel5.set_text(fastList[i].m_description);
				uICheckBox2.set_isChecked(flag || fastList[i].m_passed);
				uILabel6.set_text(fastList[i].m_progress);
				UIComponent arg_344_0 = uILabel6;
				bool isVisible = !flag && !Mathf.Approximately(fastList[i].m_max, 1f) && fastList[i].m_max > 1f;
				uIProgressBar2.set_isVisible(isVisible);
				arg_344_0.set_isVisible(isVisible);
				uIProgressBar2.set_minValue(fastList[i].m_min);
				uIProgressBar2.set_maxValue(fastList[i].m_max);
				uIProgressBar2.set_value(fastList[i].m_current);
				uIComponent3.FitChildrenVertically();
			}
		}
		uILabel.set_text(Locale.Get("BUILDING_TITLE", name));
		uILabel2.set_text(Locale.Get("BUILDING_DESC", name));
		if (uISprite != null)
		{
			uISprite.set_spriteName(group);
		}
		if (atlas != null && atlas.get_Item(thumbnail) != null && !string.IsNullOrEmpty(thumbnail))
		{
			uISprite2.set_atlas(atlas);
		}
		if (string.IsNullOrEmpty(thumbnail) || uISprite2.get_atlas().get_Item(thumbnail) == null)
		{
			thumbnail = "ThumbnailBuildingDefault";
		}
		uISprite2.set_spriteName(thumbnail);
	}

	private void AddUnlockWonder(string template, string name, UITextureAtlas atlas, string thumbnail, string group, MilestoneInfo info, string tooltip)
	{
		if (info == null)
		{
			return;
		}
		GameObject asGameObject = UITemplateManager.GetAsGameObject(template);
		asGameObject.set_name(name);
		UIPanel uIPanel = this.m_WondersContainer.AttachUIComponent(asGameObject) as UIPanel;
		if (tooltip != null)
		{
			uIPanel.set_tooltip(tooltip);
		}
		uIPanel.set_objectUserData(info);
		UILabel uILabel = uIPanel.Find<UILabel>("Name");
		UILabel uILabel2 = uIPanel.Find<UILabel>("Description");
		UISprite uISprite = uIPanel.Find<UISprite>("Service");
		UISprite uISprite2 = uIPanel.Find<UISprite>("Thumbnail");
		UIPanel uIPanel2 = uIPanel.Find<UIPanel>("Requirements");
		FastList<MilestoneInfo.ProgressInfo> fastList;
		info.GetLocalizedProgress(out fastList);
		uILabel.set_text(Locale.Get("BUILDING_TITLE", name));
		uILabel2.set_text(Locale.Get("BUILDING_DESC", name));
		if (uISprite != null)
		{
			uISprite.set_spriteName(group);
		}
		if (atlas != null && atlas.get_Item(thumbnail) != null && !string.IsNullOrEmpty(thumbnail))
		{
			uISprite2.set_atlas(atlas);
		}
		if (string.IsNullOrEmpty(thumbnail) || uISprite2.get_atlas().get_Item(thumbnail) == null)
		{
			thumbnail = "ThumbnailBuildingDefault";
		}
		uISprite2.set_spriteName(thumbnail);
		for (int i = 0; i < fastList.m_size; i++)
		{
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(UnlockingPanel.kWonderRequirementTemplate);
			asGameObject2.set_name(name + "Requirements" + i);
			UICheckBox uICheckBox = uIPanel2.AttachUIComponent(asGameObject2) as UICheckBox;
			UISprite uISprite3 = uICheckBox.Find<UISprite>("Sprite");
			uICheckBox.set_isChecked(fastList[i].m_passed);
			if (fastList[i].m_prefabs.m_size > 0)
			{
				PrefabInfo prefabInfo = fastList[i].m_prefabs[0];
				if (prefabInfo != null)
				{
					uICheckBox.set_objectUserData(prefabInfo);
					uISprite3.set_atlas(prefabInfo.m_Atlas);
					uISprite3.set_spriteName(prefabInfo.m_Thumbnail);
					uISprite3.set_tooltip(fastList[i].m_description);
				}
			}
		}
	}

	private void AddUnlockItem(string template, PrefabInfo prefabInfo)
	{
		string group;
		if (prefabInfo.GetService() == ItemClass.Service.Beautification && prefabInfo.category != null && prefabInfo.category.StartsWith("Landscaping"))
		{
			group = "ToolbarIconLandscaping";
		}
		else if (prefabInfo.category != null && prefabInfo.category == "FireDepartmentDisaster")
		{
			group = "ToolbarIconFireDepartment";
		}
		else
		{
			group = "ToolbarIcon" + Utils.GetNameByValue<ItemClass.Service>(prefabInfo.GetService(), "Game");
		}
		this.AddUnlockItem(template, prefabInfo.get_name(), prefabInfo.GetLocalizedTitle(), prefabInfo.m_Atlas, prefabInfo.m_Thumbnail, group, prefabInfo.GetLocalizedDescription());
	}

	private void AddUnlockService(string template, PositionData<DistrictPolicies.Policies> policy)
	{
		this.AddUnlockService(template, policy.get_enumName(), policy.GetLocalizedName(), base.get_component().GetUIView().get_defaultAtlas(), "IconPolicy" + policy.get_enumName(), Locale.Get("POLICIES_DETAIL", policy.get_enumName()));
	}

	private void AddUnlockService(string template, PositionData<DistrictPolicies.Types> type)
	{
		this.AddUnlockService(template, type.get_enumName(), type.GetLocalizedName(), base.get_component().GetUIView().get_defaultAtlas(), "ToolbarIconPolicies", Locale.Get("POLICYTYPE_DESC", type.get_enumName()));
	}

	private void AddUnlockService(string template, PositionData<UnlockManager.Feature> feature)
	{
		string thumbnail;
		if (feature.get_enumValue() == UnlockManager.Feature.Concerts)
		{
			thumbnail = "FeatureMonumentLevel2";
		}
		else
		{
			thumbnail = "Feature" + feature.get_enumName();
		}
		this.AddUnlockService(template, feature.get_enumName(), feature.GetLocalizedName(), base.get_component().GetUIView().get_defaultAtlas(), thumbnail, Locale.Get("FEATURES_DESC", feature.get_enumName()));
	}

	private void AddUnlockService(string template, PositionData<ItemClass.Service> service)
	{
		this.AddUnlockService(template, service.get_enumName(), service.GetLocalizedName(), base.get_component().GetUIView().get_defaultAtlas(), "ToolbarIcon" + service.get_enumName(), Locale.Get("SERVICE_DESC", service.get_enumName()));
	}

	private void AddUnlockService(string template, PositionData<ItemClass.SubService> service)
	{
		this.AddUnlockService(template, service.get_enumName(), service.GetLocalizedName(), base.get_component().GetUIView().get_defaultAtlas(), "SubBarPublicTransport" + service.get_enumName(), Locale.Get("SUBSERVICE_DESC", service.get_enumName()));
	}

	private void AddUnlockItem(string template, string name, string localizedName, UITextureAtlas atlas, string thumbnail, string group, string tooltip)
	{
		GameObject asGameObject = UITemplateManager.GetAsGameObject(template);
		asGameObject.set_name(name);
		UIPanel uIPanel = this.m_MilestoneContainer.AttachUIComponent(asGameObject) as UIPanel;
		if (tooltip != null)
		{
			uIPanel.set_tooltip(tooltip);
		}
		UILabel uILabel = uIPanel.Find<UILabel>("Name");
		UISprite uISprite = uIPanel.Find<UISprite>("Service");
		UISprite uISprite2 = uIPanel.Find<UISprite>("Thumbnail");
		uILabel.set_text(localizedName);
		if (uISprite != null)
		{
			uISprite.set_spriteName(group);
		}
		if (atlas != null && atlas.get_Item(thumbnail) != null && !string.IsNullOrEmpty(thumbnail))
		{
			uISprite2.set_atlas(atlas);
		}
		if (string.IsNullOrEmpty(thumbnail) || uISprite2.get_atlas().get_Item(thumbnail) == null)
		{
			thumbnail = "ThumbnailBuildingDefault";
		}
		uISprite2.set_spriteName(thumbnail);
	}

	private void AddUnlockService(string template, string name, string localizedName, UITextureAtlas atlas, string thumbnail, string tooltip)
	{
		GameObject asGameObject = UITemplateManager.GetAsGameObject(template);
		asGameObject.set_name(name);
		UIPanel uIPanel = this.m_MilestoneContainer.AttachUIComponent(asGameObject) as UIPanel;
		if (tooltip != null)
		{
			uIPanel.set_tooltip(tooltip);
		}
		UILabel uILabel = uIPanel.Find<UILabel>("Name");
		UISprite uISprite = uIPanel.Find<UISprite>("Thumbnail");
		uILabel.set_text(localizedName);
		if (atlas != null && atlas.get_Item(thumbnail) != null && !string.IsNullOrEmpty(thumbnail))
		{
			uISprite.set_atlas(atlas);
		}
		uISprite.set_spriteName(thumbnail);
		UITextureAtlas.SpriteInfo spriteInfo = uISprite.get_atlas().get_Item(thumbnail);
		if (spriteInfo != null)
		{
			uISprite.set_size(spriteInfo.get_pixelSize());
		}
	}

	public int population
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount;
			}
			return 0;
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible())
		{
			if (this.m_Fireworks.get_isPlaying())
			{
				this.SetFireWorksRenderQueue(this.m_Congrats.get_renderMaterial().get_renderQueue() - this.m_RenderQueueOffset);
			}
			if (this.m_CurrentMilestone != null)
			{
				MilestoneInfo.ProgressInfo localizedProgress = this.m_CurrentMilestone.GetLocalizedProgress();
				string text = localizedProgress.m_progress;
				if (text == null)
				{
					text = "Progress missing";
				}
				this.m_MilestoneProgressText.set_text(text);
				this.m_MilestoneProgress.set_minValue(localizedProgress.m_min);
				this.m_MilestoneProgress.set_maxValue(localizedProgress.m_max);
				this.m_MilestoneProgress.set_value(localizedProgress.m_current);
			}
			if (this.m_CurrentMonumentCategory != -1)
			{
				if (this.m_MonumentsCategoryMilestoneInfo[this.m_CurrentMonumentCategory] != null)
				{
					MilestoneInfo.ProgressInfo localizedProgress2 = this.m_MonumentsCategoryMilestoneInfo[this.m_CurrentMonumentCategory].GetLocalizedProgress();
					string text2 = localizedProgress2.m_progress;
					if (text2 == null)
					{
						text2 = "Progress missing";
					}
					this.m_MonumentProgressText.set_text(text2);
					this.m_MonumentProgress.set_minValue(localizedProgress2.m_min);
					this.m_MonumentProgress.set_maxValue(localizedProgress2.m_max);
					this.m_MonumentProgress.set_value(localizedProgress2.m_current);
				}
				else
				{
					this.m_MonumentProgressText.set_text(string.Empty);
					this.m_MonumentProgress.set_minValue(0f);
					this.m_MonumentProgress.set_maxValue(1f);
					this.m_MonumentProgress.set_value(1f);
				}
				this.RefreshMonumentsStatus(ToolsModifierControl.IsUnlocked(this.m_MonumentsCategoryMilestoneInfo[this.m_CurrentMonumentCategory]));
			}
			this.RefreshWondersStatus();
		}
	}

	private UICheckBox FindCheckbox(IList<UIComponent> components, PrefabInfo search)
	{
		for (int i = 0; i < components.Count; i++)
		{
			if (object.ReferenceEquals(components[i].get_objectUserData(), search))
			{
				return components[i] as UICheckBox;
			}
		}
		return null;
	}

	private void RefreshMonumentsStatus(bool unlocked)
	{
		if (this.m_MonumentsContainer.get_isVisible())
		{
			for (int i = 0; i < this.m_MonumentsContainer.get_childCount(); i++)
			{
				UIComponent uIComponent = this.m_MonumentsContainer.get_components()[i];
				if (uIComponent.IsVisibleInParent())
				{
					MilestoneInfo milestoneInfo = uIComponent.get_objectUserData() as MilestoneInfo;
					UIComponent uIComponent2 = uIComponent.Find("Requirements");
					uIComponent.Find<UILabel>("ReqLabel").set_text(Locale.Get((!unlocked) ? "UNLOCK_REQUIREMENTS_HIDDEN" : "UNLOCK_REQUIREMENTS"));
					uIComponent2.set_isVisible(unlocked);
					if (uIComponent2.get_isVisible())
					{
						UISprite uISprite = uIComponent.Find<UISprite>("LockIcon");
						FastList<MilestoneInfo.ProgressInfo> fastList;
						MilestoneInfo.ProgressInfo localizedProgress = milestoneInfo.GetLocalizedProgress(out fastList);
						bool flag = milestoneInfo.IsPassed();
						uISprite.set_isVisible(!flag);
						if (fastList.m_size == 0)
						{
							UIComponent uIComponent3 = uIComponent2.get_components()[0];
							uIComponent3.Find<UICheckBox>("ReqPassed").set_isChecked(flag);
							UILabel uILabel = uIComponent3.Find<UILabel>("ReqProgressText");
							uILabel.set_text(localizedProgress.m_progress);
							UIProgressBar uIProgressBar = uIComponent3.Find<UIProgressBar>("ReqProgress");
							uIProgressBar.set_value(localizedProgress.m_current);
							if (uIProgressBar.get_isVisible() && flag)
							{
								UIComponent arg_13B_0 = uIProgressBar;
								bool isVisible = false;
								uILabel.set_isVisible(isVisible);
								arg_13B_0.set_isVisible(isVisible);
								uIComponent3.FitChildrenVertically();
							}
							else if (!uIProgressBar.get_isVisible() && !flag)
							{
								UIComponent arg_16F_0 = uIProgressBar;
								bool isVisible = true;
								uILabel.set_isVisible(isVisible);
								arg_16F_0.set_isVisible(isVisible);
								uIComponent3.FitChildrenVertically();
							}
						}
						else
						{
							for (int j = 0; j < fastList.m_size; j++)
							{
								UIComponent uIComponent4 = uIComponent2.get_components()[j];
								uIComponent4.Find<UICheckBox>("ReqPassed").set_isChecked(flag || fastList[j].m_passed);
								UILabel uILabel2 = uIComponent4.Find<UILabel>("ReqProgressText");
								uILabel2.set_text(fastList[j].m_progress);
								UIProgressBar uIProgressBar2 = uIComponent4.Find<UIProgressBar>("ReqProgress");
								uIProgressBar2.set_value(fastList[j].m_current);
								if (uIProgressBar2.get_isVisible() && (fastList[j].m_passed || flag))
								{
									UIComponent arg_24C_0 = uIProgressBar2;
									bool isVisible = false;
									uILabel2.set_isVisible(isVisible);
									arg_24C_0.set_isVisible(isVisible);
									uIComponent4.FitChildrenVertically();
								}
								else if (!uIProgressBar2.get_isVisible() && !fastList[j].m_passed && !flag)
								{
									UIComponent arg_297_0 = uIProgressBar2;
									bool isVisible = true;
									uILabel2.set_isVisible(isVisible);
									arg_297_0.set_isVisible(isVisible);
									uIComponent4.FitChildrenVertically();
								}
							}
						}
					}
				}
			}
		}
	}

	private void RefreshAchievements()
	{
		bool active = PlatformService.get_active();
		this.m_AchievementsTab.set_isVisible(active);
		if (active)
		{
			Achievements achievements = PlatformService.get_achievements();
			int num = 0;
			IEnumerator enumerator = achievements.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					Achievement achievement = (Achievement)enumerator.Current;
					UIPanel uIPanel;
					if (this.m_AchievementsContainer.get_childCount() > num)
					{
						uIPanel = (this.m_AchievementsContainer.get_components()[num] as UIPanel);
					}
					else
					{
						GameObject asGameObject = UITemplateManager.GetAsGameObject(UnlockingPanel.kUnlockAchievementTemplate);
						asGameObject.set_name(base.get_name());
						uIPanel = (this.m_AchievementsContainer.AttachUIComponent(asGameObject) as UIPanel);
					}
					uIPanel.Find<UILabel>("Name").set_text(Locale.Get("ACH_TITLE", achievement.get_id()));
					uIPanel.Find<UILabel>("Description").set_text(Locale.Get("ACH_DESC", achievement.get_id()));
					uIPanel.Find<UITextureSprite>("Image").set_texture(achievement.get_icon());
					num++;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
	}

	private void RefreshWondersStatus()
	{
		if (this.m_WondersContainer.get_isVisible())
		{
			for (int i = 0; i < this.m_WondersContainer.get_childCount(); i++)
			{
				UIComponent uIComponent = this.m_WondersContainer.get_components()[i];
				if (uIComponent.IsVisibleInParent())
				{
					MilestoneInfo milestoneInfo = uIComponent.get_objectUserData() as MilestoneInfo;
					UIPanel uIPanel = uIComponent.Find<UIPanel>("Requirements");
					UISprite uISprite = uIComponent.Find<UISprite>("LockIcon");
					FastList<MilestoneInfo.ProgressInfo> fastList;
					uISprite.set_isVisible(!milestoneInfo.GetLocalizedProgress(out fastList).m_passed);
					for (int j = 0; j < fastList.m_size; j++)
					{
						if (fastList[j].m_prefabs.m_size > 0)
						{
							PrefabInfo prefabInfo = fastList[j].m_prefabs[0];
							if (prefabInfo != null)
							{
								UICheckBox uICheckBox = this.FindCheckbox(uIPanel.get_components(), prefabInfo);
								if (uICheckBox != null)
								{
									uICheckBox.set_isChecked(fastList[j].m_passed);
								}
							}
						}
					}
				}
			}
		}
	}

	private void SetFireWorksRenderQueue(int rdrq)
	{
		for (int i = 0; i < this.m_FireworkMaterials.Length; i++)
		{
			this.m_FireworkMaterials[i].set_renderQueue(rdrq);
		}
	}

	public UIComponent ShowModal()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			ToolsModifierControl.SetTool<DefaultTool>();
		}
		TutorialPanel.Hide();
		this.SetCurrentMilestone(true);
		if (base.get_component().get_isVisible())
		{
			base.get_component().Hide();
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = true;
			});
		}
		Cursor.set_visible(true);
		Cursor.set_lockState(0);
		this.m_Tabstrip.set_selectedIndex(0);
		this.m_Congrats.Show();
		this.SetFireWorksRenderQueue(this.m_Congrats.get_renderMaterial().get_renderQueue() - this.m_RenderQueueOffset);
		this.m_Fireworks.Play();
		base.get_component().Show(true);
		base.get_component().Focus();
		if (!this.m_IsModal)
		{
			UIComponent cc = UIView.GetAView().get_panelsLibraryModalEffect();
			if (cc != null)
			{
				cc.FitTo(null);
				if (!UIView.HasModalInput())
				{
					cc.Show(false);
					ValueAnimator.Animate("ModalEffect67419", delegate(float val)
					{
						cc.set_opacity(val);
					}, new AnimatedFloat(0f, 1f, 0.7f, 10));
				}
				cc.set_zOrder(base.get_component().get_zOrder() - 1);
			}
			UIView.PushModal(base.get_component(), new UIView.ModalPoppedCallback(this.PopModal));
			this.m_IsModal = true;
		}
		base.get_transform().set_position(this.m_StartPosition);
		base.get_component().set_size(this.m_StartSize);
		base.get_component().set_opacity(1f);
		return base.get_component();
	}

	private void PopModal(UIComponent comp)
	{
		this.m_IsModal = false;
	}

	public UIComponent Show()
	{
		if ((base.get_component().get_opacity() != 1f && base.get_component().get_isVisible()) || !base.get_component().get_isVisible())
		{
			Cursor.set_visible(true);
			Cursor.set_lockState(0);
			this.m_Congrats.Hide();
			this.m_Fireworks.Stop();
			base.get_component().Show(true);
			base.get_component().Focus();
			ValueAnimator.Animate("UnlockingPanel", delegate(float val)
			{
				base.get_transform().set_position(Vector3.Lerp(this.m_Trigger.get_transform().get_position(), this.m_StartPosition, val));
				base.get_component().set_size(Vector2.Lerp(this.m_Trigger.get_size(), this.m_StartSize, val));
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(0f, 1f, this.m_ShowHideTime));
			if (Singleton<UnlockManager>.get_exists())
			{
				GenericGuide guide = Singleton<UnlockManager>.get_instance().m_milestonesNotUsedGuide;
				if (guide != null && !guide.m_disabled)
				{
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						guide.Disable();
					});
				}
			}
		}
		return base.get_component();
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	public UIComponent Hide()
	{
		this.m_Fireworks.Stop();
		if (base.get_component().get_isVisible())
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				if (this.m_IsModal)
				{
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
						if (!Singleton<SimulationManager>.get_instance().SimulationPaused)
						{
							Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 1;
						}
					});
				}
				else
				{
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
					});
				}
			}
			ValueAnimator.Animate("UnlockingPanel", delegate(float val)
			{
				base.get_transform().set_position(Vector3.Lerp(this.m_Trigger.get_transform().get_position(), this.m_StartPosition, val));
				base.get_component().set_size(Vector2.Lerp(this.m_Trigger.get_size(), this.m_StartSize, val));
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(1f, 0f, this.m_ShowHideTime), delegate
			{
				if (this.m_IsModal)
				{
					UIView.PopModal();
					UIComponent cc = UIView.GetAView().get_panelsLibraryModalEffect();
					if (cc != null)
					{
						if (!UIView.HasModalInput())
						{
							ValueAnimator.Animate("ModalEffect67419", delegate(float val)
							{
								cc.set_opacity(val);
							}, new AnimatedFloat(1f, 0f, 0.7f, 10), delegate
							{
								cc.Hide();
							});
						}
						else
						{
							cc.set_zOrder(UIView.GetModalComponent().get_zOrder() - 1);
						}
					}
				}
				base.get_component().Hide();
			});
		}
		return base.get_component();
	}

	public float milestoneProgress
	{
		get
		{
			if (Singleton<UnlockManager>.get_exists())
			{
				MilestoneInfo currentMilestone = Singleton<UnlockManager>.get_instance().GetCurrentMilestone();
				if (currentMilestone != null)
				{
					return currentMilestone.GetProgress();
				}
			}
			return 1f;
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	public void OnClosed()
	{
		this.Hide();
	}

	private static readonly string kWonderRequirementTemplate = "WonderRequirementTemplate";

	private static readonly string kUnlockMonumentTemplate = "UnlockMonumentTemplate";

	private static readonly string kMonumentRequirementTemplate = "MonumentRequirementTemplate";

	private static readonly string kUnlockWonderTemplate = "UnlockWonderTemplate";

	private static readonly string kUnlockItemTemplate = "UnlockItemTemplate";

	private static readonly string kUnlockAchievementTemplate = "UnlockAchievementTemplate";

	private static readonly string kUnlockServiceTemplate = "UnlockServiceTemplate";

	private static readonly string kUnlockTitleTemplate = "UnlockTitleTemplate";

	private readonly PositionData<UnlockManager.Feature>[] kFeatures = Utils.GetOrderedEnumData<UnlockManager.Feature>("General");

	private readonly PositionData<UnlockManager.Feature>[] kServiceFeatures = Utils.GetOrderedEnumData<UnlockManager.Feature>("Services");

	private readonly PositionData<ItemClass.Service>[] kServices = Utils.GetOrderedEnumData<ItemClass.Service>("Game");

	private readonly PositionData<ItemClass.SubService>[] kSubServices = Utils.GetOrderedEnumData<ItemClass.SubService>("Game");

	private readonly PositionData<ItemClass.Zone>[] kZones = Utils.GetOrderedEnumData<ItemClass.Zone>();

	private readonly PositionData<DistrictPolicies.Policies>[] kIndustryPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Industry");

	private readonly PositionData<DistrictPolicies.Policies>[] kCommercialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Commercial");

	private readonly PositionData<DistrictPolicies.Policies>[] kResidentialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Residential");

	private readonly PositionData<DistrictPolicies.Policies>[] kOfficePolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Office");

	private readonly PositionData<DistrictPolicies.Policies>[] kServicesPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Services");

	private readonly PositionData<DistrictPolicies.Policies>[] kTaxationPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Taxation");

	private readonly PositionData<DistrictPolicies.Policies>[] kCityPlanningPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("CityPlanning");

	private readonly PositionData<DistrictPolicies.Policies>[] kSpecialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Special");

	private readonly PositionData<DistrictPolicies.Policies>[] kEventPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Event");

	private readonly PositionData<DistrictPolicies.Types>[] kPolicyTypes = Utils.GetOrderedEnumData<DistrictPolicies.Types>();

	private string[] m_MonumentCategories;

	private int m_CurrentMonumentCategory = -1;

	private MilestoneInfo[] m_MonumentsCategoryMilestoneInfo;

	public float m_ShowHideTime = 0.15f;

	public ParticleSystem m_Fireworks;

	public int m_RenderQueueOffset = 1;

	private Material[] m_FireworkMaterials;

	protected UIComponent m_TSBar;

	private UIButton m_Trigger;

	private Vector3 m_StartPosition;

	private Vector2 m_StartSize;

	private MilestoneInfo m_CurrentMilestone;

	private bool m_IsModal;

	private UIScrollablePanel m_MilestoneContainer;

	private UIScrollablePanel m_MonumentsContainer;

	private UIScrollablePanel m_WondersContainer;

	private UIScrollablePanel m_AchievementsContainer;

	private UIButton m_AchievementsTab;

	private UITextureSprite m_Congrats;

	private UIProgressBar m_MilestoneProgress;

	private UILabel m_MilestoneProgressText;

	private UILabel m_MilestoneTitle;

	private UILabel m_MilestoneDesc;

	private UIProgressBar m_MonumentProgress;

	private UILabel m_MonumentProgressText;

	private UILabel m_MonumentTitle;

	private UILabel m_MonumentDesc;

	private UITabstrip m_Tabstrip;

	private List<BuildingInfo> m_Monuments = new List<BuildingInfo>();

	private List<BuildingInfo> m_Wonders = new List<BuildingInfo>();

	private GameObject m_goalsTabGameObject;

	private GameObject m_goalsContainerGameObject;

	private UITabstrip m_tabstrip;
}
