﻿using System;
using ColossalFramework;
using UnityEngine;

public class BuildingInfoBase : PrefabInfo
{
	protected void InitMeshInfo(Vector3 size)
	{
		string tag = this.m_material.GetTag("BuildingType", false);
		this.m_noBase = (tag == "NoBase" || tag == "WaterFlow" || tag == "Floating");
		this.m_hasVertexAnimation = (tag == "WindTurbine");
		this.m_isFloating = (tag == "Floating");
		this.m_requireWaterMap = this.m_isFloating;
		this.m_requireHeightMap = (this.m_requireWaterMap || tag == "Fence");
		this.m_generatedRequireHeightMap = false;
		this.RefreshLevelOfDetail(size);
		this.m_lodLocations = new Matrix4x4[8];
		this.m_lodStates = new Vector4[8];
		this.m_lodObjectIndices = new Vector4[8];
		this.m_lodColors = new Vector4[8];
		this.m_lodCount = 0;
		this.m_lodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
	}

	protected void DestroyMeshInfo()
	{
		if (this.m_lodMeshCombined1 != null)
		{
			Object.Destroy(this.m_lodMeshCombined1);
			this.m_lodMeshCombined1 = null;
		}
		if (this.m_lodMeshCombined4 != null)
		{
			Object.Destroy(this.m_lodMeshCombined4);
			this.m_lodMeshCombined4 = null;
		}
		if (this.m_lodMeshCombined8 != null)
		{
			Object.Destroy(this.m_lodMeshCombined8);
			this.m_lodMeshCombined8 = null;
		}
		if (this.m_lodMaterialCombined != null)
		{
			Object.Destroy(this.m_lodMaterialCombined);
			this.m_lodMaterialCombined = null;
		}
		if (this.m_generatedMesh != null)
		{
			Object.Destroy(this.m_generatedMesh);
			this.m_generatedMesh = null;
		}
		this.m_mesh = null;
		this.m_lodMesh = null;
		this.m_material = null;
		this.m_extraMaterial = null;
		this.m_lodMaterial = null;
		this.m_lodMeshData = null;
		this.m_hasLodData = false;
		this.m_lodHeightMap = null;
		this.m_lodWaterHeightMap = null;
		if (this.m_generatedInfo != null && this.m_generatedInfo.m_buildingInfo == this)
		{
			this.m_generatedInfo.m_buildingInfo = null;
		}
	}

	public void RefreshLevelOfDetail(Vector3 size)
	{
		if (this.m_lodMesh != null)
		{
			float num = RenderManager.LevelOfDetailFactor * 10f;
			float num2 = size.x * size.z + size.x * size.y + size.z * size.y;
			this.m_minLodDistance = Mathf.Min(1000f, Mathf.Sqrt(num2) * num + 200f);
			num2 = size.x * size.z + size.x * (size.y + 50f) + size.z * (size.y + 50f);
			this.m_maxLodDistance = Mathf.Min(1000f, Mathf.Sqrt(num2) * num + 200f);
		}
		else
		{
			this.m_minLodDistance = 100000f;
			this.m_maxLodDistance = 100000f;
		}
	}

	public virtual void InitMeshData(Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aciAtlas)
	{
		if (this.m_lodMesh == null || this.m_lodMaterial == null)
		{
			return;
		}
		if (this.m_lodMeshCombined1 == null)
		{
			this.m_lodMeshCombined1 = new Mesh();
		}
		if (this.m_lodMeshCombined4 == null)
		{
			this.m_lodMeshCombined4 = new Mesh();
		}
		if (this.m_lodMeshCombined8 == null)
		{
			this.m_lodMeshCombined8 = new Mesh();
		}
		if (this.m_lodMaterialCombined == null)
		{
			string[] shaderKeywords;
			if (this.m_lodMaterial != null)
			{
				this.m_lodMaterialCombined = new Material(this.m_lodMaterial);
				shaderKeywords = this.m_lodMaterial.get_shaderKeywords();
			}
			else
			{
				this.m_lodMaterialCombined = new Material(this.m_material);
				shaderKeywords = this.m_material.get_shaderKeywords();
			}
			for (int i = 0; i < shaderKeywords.Length; i++)
			{
				this.m_lodMaterialCombined.EnableKeyword(shaderKeywords[i]);
			}
			this.m_lodMaterialCombined.EnableKeyword("MULTI_INSTANCE");
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<BuildingManager>.get_instance().ID_MainTex))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<BuildingManager>.get_instance().ID_MainTex, rgbAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<BuildingManager>.get_instance().ID_XYSMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<BuildingManager>.get_instance().ID_XYSMap, xysAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<BuildingManager>.get_instance().ID_ACIMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<BuildingManager>.get_instance().ID_ACIMap, aciAtlas);
		}
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(this.m_lodMesh);
		int num = meshData.m_vertices.Length;
		int num2 = meshData.m_triangles.Length;
		if (num != 0 && (meshData.m_tangents == null || meshData.m_tangents.Length == 0))
		{
			throw new PrefabException(this, "LOD has no tangents");
		}
		meshData.m_uvs3 = new Vector2[num];
		meshData.m_uvs4 = new Vector2[num];
		Vector2 vector;
		vector..ctor(atlasRect.get_x(), atlasRect.get_y());
		Vector2 vector2;
		vector2..ctor(atlasRect.get_width(), atlasRect.get_height());
		for (int j = 0; j < num; j++)
		{
			meshData.m_uvs3[j] = vector;
			meshData.m_uvs4[j] = vector2;
		}
		if (meshData.m_colors.Length != num)
		{
			meshData.m_colors = new Color32[num];
			for (int k = 0; k < meshData.m_colors.Length; k++)
			{
				meshData.m_colors[k] = new Color32(255, 255, 255, 255);
			}
		}
		for (int l = 0; l < num; l++)
		{
			Vector2 vector3 = meshData.m_uvs[l];
			vector3.x = atlasRect.get_x() + atlasRect.get_width() * vector3.x;
			vector3.y = atlasRect.get_y() + atlasRect.get_height() * vector3.y;
			meshData.m_uvs[l] = vector3;
		}
		int num3 = 0;
		int num4 = 0;
		for (int m = 0; m < num2; m += 3)
		{
			int num5 = meshData.m_triangles[m];
			int num6 = meshData.m_triangles[m + 1];
			int num7 = meshData.m_triangles[m + 2];
			bool flag = meshData.m_vertices[num5].y > -0.04f && meshData.m_vertices[num5].y < 0.04f;
			bool flag2 = meshData.m_vertices[num6].y > -0.04f && meshData.m_vertices[num6].y < 0.04f;
			bool flag3 = meshData.m_vertices[num7].y > -0.04f && meshData.m_vertices[num7].y < 0.04f;
			if (!flag || !flag2 || !flag3)
			{
				if ((flag && flag2) || (flag2 && flag3) || (flag3 && flag))
				{
					num3 += 4;
					num4 += 6;
				}
			}
		}
		RenderGroup.MeshData meshData2 = null;
		if (num4 != 0)
		{
			RenderGroup.VertexArrays vertexArrays = meshData.VertexArrayMask() | RenderGroup.VertexArrays.Uvs3 | RenderGroup.VertexArrays.Uvs4;
			meshData2 = new RenderGroup.MeshData(vertexArrays, num3, num4);
			int num8 = 0;
			int num9 = 0;
			Color32 color;
			color..ctor(255, 255, 255, 255);
			float num10 = 1f;
			float num11 = 0f;
			for (int n = 0; n < num2; n += 3)
			{
				int num12 = meshData.m_triangles[n];
				int num13 = meshData.m_triangles[n + 1];
				int num14 = meshData.m_triangles[n + 2];
				bool flag4 = meshData.m_vertices[num12].y > -0.04f && meshData.m_vertices[num12].y < 0.04f;
				bool flag5 = meshData.m_vertices[num13].y > -0.04f && meshData.m_vertices[num13].y < 0.04f;
				bool flag6 = meshData.m_vertices[num14].y > -0.04f && meshData.m_vertices[num14].y < 0.04f;
				if (!flag4 || !flag5 || !flag6)
				{
					if ((flag4 && flag5) || (flag5 && flag6) || (flag6 && flag4))
					{
						Vector3 zero = Vector3.get_zero();
						Vector3 vector4 = Vector3.get_zero();
						Vector3 zero2 = Vector3.get_zero();
						Vector3 vector5 = Vector3.get_zero();
						if (flag4 && flag5)
						{
							zero..ctor(meshData.m_vertices[num12].x, 0f, meshData.m_vertices[num12].z);
							Vector3 vector6;
							vector6..ctor(meshData.m_normals[num12].x, 0f, meshData.m_normals[num12].z);
							vector4 = vector6.get_normalized();
							zero2..ctor(meshData.m_vertices[num13].x, 0f, meshData.m_vertices[num13].z);
							Vector3 vector7;
							vector7..ctor(meshData.m_normals[num13].x, 0f, meshData.m_normals[num13].z);
							vector5 = vector7.get_normalized();
						}
						else if (flag5 && flag6)
						{
							zero..ctor(meshData.m_vertices[num13].x, 0f, meshData.m_vertices[num13].z);
							Vector3 vector8;
							vector8..ctor(meshData.m_normals[num13].x, 0f, meshData.m_normals[num13].z);
							vector4 = vector8.get_normalized();
							zero2..ctor(meshData.m_vertices[num14].x, 0f, meshData.m_vertices[num14].z);
							Vector3 vector9;
							vector9..ctor(meshData.m_normals[num14].x, 0f, meshData.m_normals[num14].z);
							vector5 = vector9.get_normalized();
						}
						else if (flag6 && flag4)
						{
							zero..ctor(meshData.m_vertices[num14].x, 0f, meshData.m_vertices[num14].z);
							Vector3 vector10;
							vector10..ctor(meshData.m_normals[num14].x, 0f, meshData.m_normals[num14].z);
							vector4 = vector10.get_normalized();
							zero2..ctor(meshData.m_vertices[num12].x, 0f, meshData.m_vertices[num12].z);
							Vector3 vector11;
							vector11..ctor(meshData.m_normals[num12].x, 0f, meshData.m_normals[num12].z);
							vector5 = vector11.get_normalized();
						}
						if (vector4.get_sqrMagnitude() < 0.01f)
						{
							vector4 = zero.get_normalized();
						}
						if (vector5.get_sqrMagnitude() < 0.01f)
						{
							vector5 = zero2.get_normalized();
						}
						float num15 = Vector3.Distance(zero, zero2);
						float num16 = num11 + num15 * 0.125f;
						meshData2.m_vertices[num8] = zero - new Vector3(0f, num10, 0f);
						meshData2.m_normals[num8] = vector4;
						meshData2.m_tangents[num8] = new Vector4(vector4.z, 0f, -vector4.x, 1f);
						meshData2.m_colors[num8] = color;
						meshData2.m_uvs[num8] = new Vector2(num11, -0.125f);
						meshData2.m_uvs3[num8] = vector;
						meshData2.m_uvs4[num8] = vector2;
						num8++;
						meshData2.m_vertices[num8] = zero;
						meshData2.m_normals[num8] = vector4;
						meshData2.m_tangents[num8] = new Vector4(vector4.z, 0f, -vector4.x, 1f);
						meshData2.m_colors[num8] = color;
						meshData2.m_uvs[num8] = new Vector2(num11, 0f);
						meshData2.m_uvs3[num8] = vector;
						meshData2.m_uvs4[num8] = vector2;
						num8++;
						meshData2.m_vertices[num8] = zero2 - new Vector3(0f, num10, 0f);
						meshData2.m_normals[num8] = vector5;
						meshData2.m_tangents[num8] = new Vector4(vector5.z, 0f, -vector5.x, 1f);
						meshData2.m_colors[num8] = color;
						meshData2.m_uvs[num8] = new Vector2(num16, -0.125f);
						meshData2.m_uvs3[num8] = vector;
						meshData2.m_uvs4[num8] = vector2;
						num8++;
						meshData2.m_vertices[num8] = zero2;
						meshData2.m_normals[num8] = vector5;
						meshData2.m_tangents[num8] = new Vector4(vector5.z, 0f, -vector5.x, 1f);
						meshData2.m_colors[num8] = color;
						meshData2.m_uvs[num8] = new Vector2(num16, 0f);
						meshData2.m_uvs3[num8] = vector;
						meshData2.m_uvs4[num8] = vector2;
						num8++;
						meshData2.m_triangles[num9++] = num8 - 2;
						meshData2.m_triangles[num9++] = num8 - 3;
						meshData2.m_triangles[num9++] = num8 - 4;
						meshData2.m_triangles[num9++] = num8 - 1;
						meshData2.m_triangles[num9++] = num8 - 3;
						meshData2.m_triangles[num9++] = num8 - 2;
						num11 = num16;
					}
				}
			}
		}
		else if (!this.m_noBase)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD has no base: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		if ((num + num3) * 8 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3 | RenderGroup.VertexArrays.Uvs4, num + num3, num2 + num4);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3 | RenderGroup.VertexArrays.Uvs4, (num + num3) * 4, (num2 + num4) * 4);
		RenderGroup.MeshData meshData5 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3 | RenderGroup.VertexArrays.Uvs4, (num + num3) * 8, (num2 + num4) * 8);
		int num17 = 0;
		int num18 = 0;
		for (int num19 = 0; num19 < 8; num19++)
		{
			byte a = (byte)(num19 * 32);
			for (int num20 = 0; num20 < num2; num20++)
			{
				if (num19 < 1)
				{
					meshData3.m_triangles[num18] = meshData.m_triangles[num20] + num17;
				}
				if (num19 < 4)
				{
					meshData4.m_triangles[num18] = meshData.m_triangles[num20] + num17;
				}
				if (num19 < 8)
				{
					meshData5.m_triangles[num18] = meshData.m_triangles[num20] + num17;
				}
				num18++;
			}
			for (int num21 = 0; num21 < num; num21++)
			{
				Color32 color2 = meshData.m_colors[num21];
				color2.a = a;
				if (num19 < 1)
				{
					meshData3.m_vertices[num17] = meshData.m_vertices[num21];
					meshData3.m_normals[num17] = meshData.m_normals[num21];
					meshData3.m_tangents[num17] = meshData.m_tangents[num21];
					meshData3.m_uvs[num17] = meshData.m_uvs[num21];
					meshData3.m_uvs3[num17] = meshData.m_uvs3[num21];
					meshData3.m_uvs4[num17] = meshData.m_uvs4[num21];
					meshData3.m_colors[num17] = color2;
				}
				if (num19 < 4)
				{
					meshData4.m_vertices[num17] = meshData.m_vertices[num21];
					meshData4.m_normals[num17] = meshData.m_normals[num21];
					meshData4.m_tangents[num17] = meshData.m_tangents[num21];
					meshData4.m_uvs[num17] = meshData.m_uvs[num21];
					meshData4.m_uvs3[num17] = meshData.m_uvs3[num21];
					meshData4.m_uvs4[num17] = meshData.m_uvs4[num21];
					meshData4.m_colors[num17] = color2;
				}
				if (num19 < 8)
				{
					meshData5.m_vertices[num17] = meshData.m_vertices[num21];
					meshData5.m_normals[num17] = meshData.m_normals[num21];
					meshData5.m_tangents[num17] = meshData.m_tangents[num21];
					meshData5.m_uvs[num17] = meshData.m_uvs[num21];
					meshData5.m_uvs3[num17] = meshData.m_uvs3[num21];
					meshData5.m_uvs4[num17] = meshData.m_uvs4[num21];
					meshData5.m_colors[num17] = color2;
				}
				num17++;
			}
			for (int num22 = 0; num22 < num4; num22++)
			{
				if (num19 < 1)
				{
					meshData3.m_triangles[num18] = meshData2.m_triangles[num22] + num17;
				}
				if (num19 < 4)
				{
					meshData4.m_triangles[num18] = meshData2.m_triangles[num22] + num17;
				}
				if (num19 < 8)
				{
					meshData5.m_triangles[num18] = meshData2.m_triangles[num22] + num17;
				}
				num18++;
			}
			for (int num23 = 0; num23 < num3; num23++)
			{
				Color32 color3 = meshData2.m_colors[num23];
				color3.a = a;
				if (num19 < 1)
				{
					meshData3.m_vertices[num17] = meshData2.m_vertices[num23];
					meshData3.m_normals[num17] = meshData2.m_normals[num23];
					meshData3.m_tangents[num17] = meshData2.m_tangents[num23];
					meshData3.m_uvs[num17] = meshData2.m_uvs[num23];
					meshData3.m_uvs3[num17] = meshData2.m_uvs3[num23];
					meshData3.m_uvs4[num17] = meshData2.m_uvs4[num23];
					meshData3.m_colors[num17] = color3;
				}
				if (num19 < 4)
				{
					meshData4.m_vertices[num17] = meshData2.m_vertices[num23];
					meshData4.m_normals[num17] = meshData2.m_normals[num23];
					meshData4.m_tangents[num17] = meshData2.m_tangents[num23];
					meshData4.m_uvs[num17] = meshData2.m_uvs[num23];
					meshData4.m_uvs3[num17] = meshData2.m_uvs3[num23];
					meshData4.m_uvs4[num17] = meshData2.m_uvs4[num23];
					meshData4.m_colors[num17] = color3;
				}
				if (num19 < 8)
				{
					meshData5.m_vertices[num17] = meshData2.m_vertices[num23];
					meshData5.m_normals[num17] = meshData2.m_normals[num23];
					meshData5.m_tangents[num17] = meshData2.m_tangents[num23];
					meshData5.m_uvs[num17] = meshData2.m_uvs[num23];
					meshData5.m_uvs3[num17] = meshData2.m_uvs3[num23];
					meshData5.m_uvs4[num17] = meshData2.m_uvs4[num23];
					meshData5.m_colors[num17] = color3;
				}
				num17++;
			}
		}
		this.m_lodMeshData = meshData;
		this.m_lodMeshBase = meshData2;
		meshData3.PopulateMesh(this.m_lodMeshCombined1);
		meshData4.PopulateMesh(this.m_lodMeshCombined4);
		meshData5.PopulateMesh(this.m_lodMeshCombined8);
	}

	public BuildingInfoGen m_generatedInfo;

	public Mesh m_overrideMainMesh;

	public Renderer m_overrideMainRenderer;

	public GameObject m_lodObject;

	public bool m_disableSnow;

	public bool m_lodHasDifferentShader;

	[NonSerialized]
	public Mesh m_mesh;

	[NonSerialized]
	public Mesh m_generatedMesh;

	[NonSerialized]
	public Mesh m_lodMesh;

	[NonSerialized]
	public Material m_material;

	[NonSerialized]
	public Material m_extraMaterial;

	[NonSerialized]
	public Material m_lodMaterial;

	[NonSerialized]
	public float m_minLodDistance;

	[NonSerialized]
	public float m_maxLodDistance;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshBase;

	[NonSerialized]
	public Mesh m_lodMeshCombined1;

	[NonSerialized]
	public Mesh m_lodMeshCombined4;

	[NonSerialized]
	public Mesh m_lodMeshCombined8;

	[NonSerialized]
	public Material m_lodMaterialCombined;

	[NonSerialized]
	public Matrix4x4[] m_lodLocations;

	[NonSerialized]
	public Vector4[] m_lodStates;

	[NonSerialized]
	public Vector4[] m_lodObjectIndices;

	[NonSerialized]
	public Vector4[] m_lodColors;

	[NonSerialized]
	public int m_lodCount;

	[NonSerialized]
	public Vector3 m_lodMin;

	[NonSerialized]
	public Vector3 m_lodMax;

	[NonSerialized]
	public Texture m_lodHeightMap;

	[NonSerialized]
	public Vector4 m_lodHeightMapping;

	[NonSerialized]
	public Vector4 m_lodSurfaceMapping;

	[NonSerialized]
	public Texture m_lodWaterHeightMap;

	[NonSerialized]
	public Vector4 m_lodWaterHeightMapping;

	[NonSerialized]
	public Vector4 m_lodWaterSurfaceMapping;

	[NonSerialized]
	public bool m_hasLodData;

	[NonSerialized]
	public bool m_noBase;

	[NonSerialized]
	public bool m_requireWaterMap;

	[NonSerialized]
	public bool m_requireHeightMap;

	[NonSerialized]
	public bool m_hasVertexAnimation;

	[NonSerialized]
	public bool m_isFloating;

	[NonSerialized]
	public bool m_generatedRequireHeightMap;
}
