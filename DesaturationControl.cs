﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class DesaturationControl : MonoBehaviour
{
	private void Awake()
	{
		this.m_Target = base.GetComponent<UITextureSprite>();
		if (this.m_Target == null)
		{
			base.set_enabled(false);
		}
		this._DesaturateAmount = Shader.PropertyToID("_DesaturateAmount");
	}

	private void Start()
	{
		this.m_Target.get_renderMaterial().SetFloat(this._DesaturateAmount, this.m_DesaturationAmount);
	}

	private void Update()
	{
		this.m_Target.get_renderMaterial().SetFloat(this._DesaturateAmount, this.m_DesaturationAmount);
	}

	[Range(0f, 1f)]
	public float m_DesaturationAmount;

	private int _DesaturateAmount;

	private UITextureSprite m_Target;
}
