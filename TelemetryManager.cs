﻿using System;
using System.IO;
using ColossalFramework;
using ColossalFramework.HTTP.Paradox;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class TelemetryManager : Singleton<TelemetryManager>
{
	private void Awake()
	{
		UIView.add_eventExceptionForwarded(new UIView.ForwardExceptionHandler(this.OnExceptionForwarded));
		try
		{
			SettingsFile[] expr_17 = new SettingsFile[1];
			int arg_2B_1 = 0;
			SettingsFile settingsFile = new SettingsFile();
			settingsFile.set_systemFileName(Settings.colossal);
			expr_17[arg_2B_1] = settingsFile;
			GameSettings.AddSettingsFile(expr_17);
			SavedBool savedBool = new SavedBool(Settings.firstTime, Settings.colossal, true);
			CODebugBase<LogChannel>.Log(LogChannel.Core, "Telemetry enabled");
			Telemetry telemetry = new Telemetry();
			if (savedBool)
			{
				telemetry.AddEvent(3, new Telemetry.Pair[0]);
				savedBool.set_value(false);
			}
			telemetry.AddEvent(0, new Telemetry.Pair[0]);
			telemetry.AddEvent(5, new Telemetry.Pair[]
			{
				new Telemetry.Pair("machineid", SystemInfo.get_deviceUniqueIdentifier()),
				new Telemetry.Pair("machinemodel", SystemInfo.get_deviceModel()),
				new Telemetry.Pair("gfxdevice", SystemInfo.get_graphicsDeviceName()),
				new Telemetry.Pair("gfxversion", SystemInfo.get_graphicsDeviceVersion()),
				new Telemetry.Pair("gfxmemory", SystemInfo.get_graphicsMemorySize()),
				new Telemetry.Pair("gfxshadermodel", SystemInfo.get_graphicsShaderLevel()),
				new Telemetry.Pair("os", SystemInfo.get_operatingSystem()),
				new Telemetry.Pair("oslanguage", Application.get_systemLanguage()),
				new Telemetry.Pair("cpu", SystemInfo.get_processorType()),
				new Telemetry.Pair("cpucount", SystemInfo.get_processorCount()),
				new Telemetry.Pair("sysmemory", SystemInfo.get_systemMemorySize())
			});
			telemetry.Push();
			Telemetry telemetry2 = new Telemetry();
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "AfterDark")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "SnowFall")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "MatchDay")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football2345))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "Stadiums")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football2))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "Stadiums2")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football3))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "Stadiums3")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football4))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "Stadiums4")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football5))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "Stadiums5")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack1))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "ArtDeco")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack2))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "HighTech")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "NaturalDisasters")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation1))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "RadioStation1")
				});
			}
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.OrientalBuildings))
			{
				telemetry2.AddEvent("dlc", new Telemetry.Pair[]
				{
					new Telemetry.Pair("dlc_name", "OrientalBuildings")
				});
			}
			telemetry2.Push();
		}
		catch (GameSettingsException ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Game Settings error " + ex.Message);
		}
		catch (Exception ex2)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex2.GetType() + ": Telemetry event failed " + ex2.Message);
		}
	}

	public void OnExceptionForwarded(Exception ex)
	{
		try
		{
			if (ex != null)
			{
				Telemetry telemetry = new Telemetry();
				telemetry.AddEvent("error", new Telemetry.Pair[]
				{
					new Telemetry.Pair("type", ex.GetType().Name),
					new Telemetry.Pair("message", ex.Message)
				});
				telemetry.Push();
			}
		}
		catch (Exception ex2)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex2.GetType() + ": Telemetry event failed " + ex2.Message);
		}
	}

	public void SessionLoaded(long mainTime, long scenesTime, long simulationTime)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("session_loaded", new Telemetry.Pair[]
			{
				new Telemetry.Pair("totalTime", Math.Max(mainTime, Math.Max(scenesTime, simulationTime))),
				new Telemetry.Pair("main", mainTime),
				new Telemetry.Pair("scenes", scenesTime),
				new Telemetry.Pair("simulationTime", simulationTime),
				new Telemetry.Pair("gameTime", (!Singleton<SimulationManager>.get_exists()) ? "Similation not running o_O" : Singleton<SimulationManager>.get_instance().m_currentGameTime.ToString("dd/MM/yyyy H:mm:ss"))
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void StartSession(string mapName, string playerMap, SimulationManager.UpdateMode mode, SimulationMetaData ngs)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("start_session", new Telemetry.Pair[]
			{
				new Telemetry.Pair("type", playerMap),
				new Telemetry.Pair("start_flag", mode.ToString()),
				new Telemetry.Pair("map_name", Path.GetFileName(mapName))
			});
			if (ngs != null)
			{
				telemetry.AddEvent("start_session", new Telemetry.Pair[]
				{
					new Telemetry.Pair("environment", ngs.m_environment),
					new Telemetry.Pair("invert_traffic", ngs.m_invertTraffic),
					new Telemetry.Pair("guid", ngs.m_gameInstanceIdentifier)
				});
			}
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void EndSession(ItemClass.Availability availability)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("end_session", new Telemetry.Pair[]
			{
				new Telemetry.Pair("type", availability.ToString())
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void CustomContentInfo(int buildingsCount, int propsCount, int treeCount, int vehicleCount)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("custom_content", new Telemetry.Pair[]
			{
				new Telemetry.Pair("buildings", buildingsCount),
				new Telemetry.Pair("props", propsCount),
				new Telemetry.Pair("trees", treeCount),
				new Telemetry.Pair("vehicles", vehicleCount)
			});
			telemetry.AddEvent("custom_mods", new Telemetry.Pair[]
			{
				new Telemetry.Pair("enabledModCount", Singleton<PluginManager>.get_instance().get_enabledModCount()),
				new Telemetry.Pair("modCount", Singleton<PluginManager>.get_instance().get_modCount())
			});
			telemetry.Push();
			foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
			{
				if (current.get_isEnabled())
				{
					Telemetry telemetry2 = new Telemetry();
					telemetry2.AddEvent("mod_used", new Telemetry.Pair[]
					{
						new Telemetry.Pair("modName", current.get_name()),
						new Telemetry.Pair("modWorkshopID", (!(current.get_publishedFileID() != PublishedFileId.invalid)) ? "none" : current.get_publishedFileID().ToString()),
						new Telemetry.Pair("assemblyInfo", current.get_assembliesString())
					});
					telemetry2.Push();
				}
			}
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void ParadoxLogin(bool autoLogin)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent(2, new Telemetry.Pair[]
			{
				new Telemetry.Pair("autologin", autoLogin)
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void ParadoxAccountCreated()
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent(4, new Telemetry.Pair[0]);
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	private void OnApplicationQuit()
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent(1, new Telemetry.Pair[]
			{
				new Telemetry.Pair("session_length", Mathf.RoundToInt(Time.get_realtimeSinceStartup()))
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void MilestoneUnlocked(MilestoneInfo info)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("unlocking", new Telemetry.Pair[]
			{
				new Telemetry.Pair("name", (!(info != null)) ? "Name unavailable" : info.get_name()),
				new Telemetry.Pair("gameTime", (!Singleton<SimulationManager>.get_exists()) ? "Similation not running o_O" : Singleton<SimulationManager>.get_instance().m_currentGameTime.ToString("dd/MM/yyyy H:mm:ss"))
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void OnGetDLCClicked(string idName, PublishedFileId fileID)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("store_clicked", new Telemetry.Pair[]
			{
				new Telemetry.Pair("name", idName),
				new Telemetry.Pair("publishedFileID", fileID)
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void OnStoreClicked()
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("store_clicked", new Telemetry.Pair[0]);
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void OnFeedClicked(uint steamAppID)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("newsfeed_clicked", new Telemetry.Pair[]
			{
				new Telemetry.Pair("target", steamAppID.ToString())
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}

	public void OnFeedClicked(string url)
	{
		try
		{
			Telemetry telemetry = new Telemetry();
			telemetry.AddEvent("newsfeed_clicked", new Telemetry.Pair[]
			{
				new Telemetry.Pair("target", url)
			});
			telemetry.Push();
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.HTTP, ex.GetType() + ": Telemetry event failed " + ex.Message);
		}
	}
}
