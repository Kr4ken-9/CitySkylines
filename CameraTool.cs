﻿using System;
using UnityEngine;

public class CameraTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		if (this.m_cameraController != null)
		{
			this.m_cameraController.m_freeCamera = true;
		}
	}

	protected override void OnDisable()
	{
		if (this.m_cameraController != null)
		{
			this.m_cameraController.m_freeCamera = false;
		}
		base.OnDisable();
	}

	protected override void OnToolLateUpdate()
	{
		base.OnToolLateUpdate();
		ToolBase.OverrideInfoMode = false;
	}

	private CameraController m_cameraController;
}
