﻿using System;

public sealed class EnvironmentGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return info.mapEditorCategory.Contains("Environment");
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "EnvironmentGeneral")
			{
				return 0;
			}
			if (name == "EnvironmentRocks")
			{
				return 1;
			}
			if (name == "EnvironmentAncient")
			{
				return 2;
			}
			if (name == "EnvironmentMedieval")
			{
				return 3;
			}
			if (name == "EnvironmentModern")
			{
				return 4;
			}
			if (name == "EnvironmentVehicles")
			{
				return 5;
			}
		}
		return 2147483647;
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentGeneral", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentRocks", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentAncient", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentMedieval", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentModern", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("EnvironmentVehicles", this.GetCategoryOrder(base.get_name()), "Environment"), "PROPS_CATEGORY");
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (GeneratedGroupPanel.GroupFilter)35;
		}
	}
}
