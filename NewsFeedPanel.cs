﻿using System;
using ColossalFramework;
using ColossalFramework.HTTP;
using ColossalFramework.HTTP.Paradox;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;

public class NewsFeedPanel : UICustomControl
{
	public static string steamNewsFeedURL
	{
		get;
		set;
	}

	private void Awake()
	{
		this.m_Link = base.Find<UIButton>("Entry");
		this.m_Prev = base.Find<UIButton>("Prev");
		this.m_Next = base.Find<UIButton>("Next");
		this.m_Image = base.Find<UIUriSprite>("Image");
		this.m_Link.set_localeID("NEWSFEED_REFRESHING");
		this.RefreshFeed();
	}

	private void OnFeedPrev(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_CurrentFeedIndex = (this.m_CurrentFeedIndex - 1) % this.m_Feed.get_Count();
		if (this.m_CurrentFeedIndex < 0)
		{
			this.m_CurrentFeedIndex += this.m_Feed.get_Count();
		}
		this.SetFeed(this.m_CurrentFeedIndex);
	}

	private void OnFeedNext(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_CurrentFeedIndex = (this.m_CurrentFeedIndex + 1) % this.m_Feed.get_Count();
		this.SetFeed(this.m_CurrentFeedIndex);
	}

	private void OnFeedClicked(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_steamAppID() != 0u)
		{
			Singleton<TelemetryManager>.get_instance().OnFeedClicked(this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_steamAppID());
			PlatformService.ActivateGameOverlayToStore(this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_steamAppID(), 2);
		}
		else if (!string.IsNullOrEmpty(this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_url()))
		{
			Singleton<TelemetryManager>.get_instance().OnFeedClicked(this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_url());
			PlatformService.ActivateGameOverlayToWebPage(this.m_Feed.get_Item(this.m_CurrentFeedIndex).get_url());
		}
	}

	private void SetFeed(int index)
	{
		if (this.m_Feed.get_Count() >= 0 && index < this.m_Feed.get_Count())
		{
			this.m_CurrentFeedIndex = index;
			this.m_Link.set_text(this.m_Feed.get_Item(index).get_title());
			this.m_Link.set_isEnabled(this.m_Feed.get_Item(index).get_steamAppID() != 0u || !string.IsNullOrEmpty(this.m_Feed.get_Item(index).get_url()));
			if (!string.IsNullOrEmpty(this.m_Feed.get_Item(index).get_imageURL()))
			{
				this.m_Image.set_url(this.m_Feed.get_Item(index).get_imageURL());
			}
		}
	}

	private void RefreshFeed()
	{
		this.m_Link.remove_eventClick(new MouseEventHandler(this.OnFeedClicked));
		this.m_Prev.remove_eventClick(new MouseEventHandler(this.OnFeedPrev));
		this.m_Next.remove_eventClick(new MouseEventHandler(this.OnFeedNext));
		Request request2 = new Request("get", NewsFeedPanel.steamNewsFeedURL);
		request2.AddHeader("Accept-Language", "en");
		request2.Send(delegate(Request request)
		{
			if (request != null)
			{
				this.m_Feed = new Feed(request);
				if (this.m_Feed.get_ready())
				{
					this.SetFeed(this.m_CurrentFeedIndex);
					this.m_Link.add_eventClick(new MouseEventHandler(this.OnFeedClicked));
					this.m_Prev.add_eventClick(new MouseEventHandler(this.OnFeedPrev));
					this.m_Next.add_eventClick(new MouseEventHandler(this.OnFeedNext));
				}
				else
				{
					this.m_Link.set_localeID("NEWSFEED_ERROR");
					this.m_Link.set_isEnabled(false);
				}
			}
			else
			{
				this.m_Link.set_text("NEWSFEED_ERROR");
				this.m_Link.set_isEnabled(false);
			}
		});
	}

	private UIButton m_Prev;

	private UIButton m_Next;

	private UIButton m_Link;

	private UIUriSprite m_Image;

	private Feed m_Feed;

	private int m_CurrentFeedIndex;
}
