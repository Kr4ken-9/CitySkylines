﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct VehicleParked
{
	public bool RenderInstance(RenderManager.CameraInfo cameraInfo, ushort parkedVehicleID)
	{
		VehicleParked.Flags flags = (VehicleParked.Flags)this.m_flags;
		if ((flags & (VehicleParked.Flags.Created | VehicleParked.Flags.Deleted | VehicleParked.Flags.Parking)) != VehicleParked.Flags.Created)
		{
			return false;
		}
		VehicleInfo info = this.Info;
		if (info == null)
		{
			return false;
		}
		if (!cameraInfo.CheckRenderDistance(this.m_position, info.m_maxRenderDistance))
		{
			return false;
		}
		if (!cameraInfo.Intersect(this.m_position, info.m_generatedInfo.m_size.z * 0.5f + 15f))
		{
			return false;
		}
		if ((cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) == 0)
		{
			return false;
		}
		Color color = info.m_vehicleAI.GetColor(parkedVehicleID, ref this, Singleton<InfoManager>.get_instance().CurrentMode);
		color.a = 0f;
		if (cameraInfo.CheckRenderDistance(this.m_position, info.m_lodRenderDistance))
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Matrix4x4 matrix4x = info.m_vehicleAI.CalculateBodyMatrix(flags, ref this.m_position, ref this.m_rotation);
			Matrix4x4 matrix4x2 = info.m_vehicleAI.CalculateTyreMatrix(flags, ref this.m_position, ref this.m_rotation, ref matrix4x);
			Vector4 vector;
			vector.x = 0f;
			vector.y = this.m_travelDistance;
			vector.z = 0f;
			vector.w = 0f;
			MaterialPropertyBlock materialBlock = instance.m_materialBlock;
			materialBlock.Clear();
			materialBlock.SetMatrix(instance.ID_TyreMatrix, matrix4x2);
			materialBlock.SetVector(instance.ID_TyrePosition, vector);
			materialBlock.SetVector(instance.ID_LightState, Vector4.get_zero());
			materialBlock.SetColor(instance.ID_Color, color);
			bool flag = true;
			if (info.m_subMeshes != null)
			{
				for (int i = 0; i < info.m_subMeshes.Length; i++)
				{
					VehicleInfo.MeshInfo meshInfo = info.m_subMeshes[i];
					VehicleInfoBase subInfo = meshInfo.m_subInfo;
					if (((meshInfo.m_parkedFlagsRequired | meshInfo.m_parkedFlagsForbidden) & (VehicleParked.Flags)this.m_flags) == meshInfo.m_parkedFlagsRequired && meshInfo.m_vehicleFlagsRequired == (Vehicle.Flags)0)
					{
						if (subInfo != null)
						{
							VehicleManager expr_1E9_cp_0 = instance;
							expr_1E9_cp_0.m_drawCallData.m_defaultCalls = expr_1E9_cp_0.m_drawCallData.m_defaultCalls + 1;
							subInfo.m_material.SetVectorArray(instance.ID_TyreLocation, subInfo.m_generatedInfo.m_tyres);
							Graphics.DrawMesh(subInfo.m_mesh, matrix4x, subInfo.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
						}
					}
					else if (subInfo == null)
					{
						flag = false;
					}
				}
			}
			if (flag)
			{
				VehicleManager expr_26A_cp_0 = instance;
				expr_26A_cp_0.m_drawCallData.m_defaultCalls = expr_26A_cp_0.m_drawCallData.m_defaultCalls + 1;
				info.m_material.SetVectorArray(instance.ID_TyreLocation, info.m_generatedInfo.m_tyres);
				Graphics.DrawMesh(info.m_mesh, matrix4x, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
			}
		}
		else
		{
			Matrix4x4 matrix4x3 = info.m_vehicleAI.CalculateBodyMatrix(flags, ref this.m_position, ref this.m_rotation);
			bool flag2 = true;
			if (info.m_subMeshes != null)
			{
				for (int j = 0; j < info.m_subMeshes.Length; j++)
				{
					VehicleInfo.MeshInfo meshInfo2 = info.m_subMeshes[j];
					VehicleInfoBase subInfo2 = meshInfo2.m_subInfo;
					if (((meshInfo2.m_parkedFlagsRequired | meshInfo2.m_parkedFlagsForbidden) & (VehicleParked.Flags)this.m_flags) == meshInfo2.m_parkedFlagsRequired && meshInfo2.m_vehicleFlagsRequired == (Vehicle.Flags)0)
					{
						if (subInfo2 != null)
						{
							subInfo2.m_lodTransforms[subInfo2.m_lodCount] = matrix4x3;
							subInfo2.m_lodLightStates[subInfo2.m_lodCount] = Vector4.get_zero();
							subInfo2.m_lodColors[subInfo2.m_lodCount] = color.get_linear();
							subInfo2.m_lodMin = Vector3.Min(subInfo2.m_lodMin, this.m_position);
							subInfo2.m_lodMax = Vector3.Max(subInfo2.m_lodMax, this.m_position);
							if (++subInfo2.m_lodCount == subInfo2.m_lodTransforms.Length)
							{
								Vehicle.RenderLod(cameraInfo, subInfo2);
							}
						}
					}
					else if (subInfo2 == null)
					{
						flag2 = false;
					}
				}
			}
			if (flag2)
			{
				info.m_lodTransforms[info.m_lodCount] = matrix4x3;
				info.m_lodLightStates[info.m_lodCount] = Vector4.get_zero();
				info.m_lodColors[info.m_lodCount] = color.get_linear();
				info.m_lodMin = Vector3.Min(info.m_lodMin, this.m_position);
				info.m_lodMax = Vector3.Max(info.m_lodMax, this.m_position);
				if (++info.m_lodCount == info.m_lodTransforms.Length)
				{
					Vehicle.RenderLod(cameraInfo, info);
				}
			}
		}
		return true;
	}

	public void RenderOverlay(RenderManager.CameraInfo cameraInfo, ushort vehicleID, Color color)
	{
		VehicleInfo info = this.Info;
		VehicleParked.Flags flags = (VehicleParked.Flags)this.m_flags;
		Matrix4x4 matrix4x = info.m_vehicleAI.CalculateBodyMatrix(flags, ref this.m_position, ref this.m_rotation);
		Vector3 vector = info.m_generatedInfo.m_size * 0.5f;
		vector.x += 0.15f;
		vector.z += 0.3f;
		Quad3 quad = default(Quad3);
		quad.a = matrix4x.MultiplyPoint(new Vector3(-vector.x, 0f, -vector.z));
		quad.b = matrix4x.MultiplyPoint(new Vector3(vector.x, 0f, -vector.z));
		quad.c = matrix4x.MultiplyPoint(new Vector3(vector.x, 0f, vector.z));
		quad.d = matrix4x.MultiplyPoint(new Vector3(-vector.x, 0f, vector.z));
		ToolManager expr_111_cp_0 = Singleton<ToolManager>.get_instance();
		expr_111_cp_0.m_drawCallData.m_overlayCalls = expr_111_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, this.m_position.y - 2f, this.m_position.y + info.m_generatedInfo.m_size.y + 2f, false, true);
	}

	public void CheckOverlayAlpha(ref float alpha)
	{
		VehicleInfo info = this.Info;
		float num = Mathf.Min(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * 0.5f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num)));
	}

	public VehicleInfo Info
	{
		get
		{
			return PrefabCollection<VehicleInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public bool RayCast(ushort vehicleID, Segment3 ray, VehicleParked.Flags ignoreFlags, out float t)
	{
		t = 2f;
		if (((VehicleParked.Flags)this.m_flags & ignoreFlags) != VehicleParked.Flags.None)
		{
			return false;
		}
		VehicleInfo info = this.Info;
		Vector3 size = info.m_generatedInfo.m_size;
		Vector3 position = this.m_position;
		position.y += size.y * 0.5f;
		if (ray.DistanceSqr(position) >= size.get_sqrMagnitude() * 0.25f)
		{
			return false;
		}
		ray.a -= this.m_position;
		ray.b -= this.m_position;
		Quaternion quaternion = Quaternion.Inverse(this.m_rotation);
		ray.a = quaternion * ray.a;
		ray.b = quaternion * ray.b;
		if (info.m_lodMeshData != null)
		{
			Vector3[] vertices = info.m_lodMeshData.m_vertices;
			int[] triangles = info.m_lodMeshData.m_triangles;
			if (triangles != null)
			{
				int num = triangles.Length;
				for (int i = 0; i < num; i += 3)
				{
					float num2;
					float num3;
					float num4;
					if (Triangle3.Intersect(vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]], ray.a, ray.b, ref num2, ref num3, ref num4) && num4 < t)
					{
						t = num4;
					}
				}
			}
		}
		return t != 2f;
	}

	public Vector3 GetClosestDoorPosition(Vector3 position, VehicleInfo.DoorType type)
	{
		Vector3 result = this.m_position;
		VehicleInfo info = this.Info;
		float num = 1000000f;
		if (info.m_doors != null)
		{
			for (int i = 0; i < info.m_doors.Length; i++)
			{
				if ((info.m_doors[i].m_type & type) != (VehicleInfo.DoorType)0)
				{
					Vector3 vector = this.m_rotation * info.m_doors[i].m_location + this.m_position;
					float num2 = Vector3.SqrMagnitude(vector - position);
					if (num2 < num)
					{
						num = num2;
						result = vector;
					}
				}
			}
		}
		return result;
	}

	public Vector3 GetRandomDoorPosition(ref Randomizer r, VehicleInfo.DoorType type)
	{
		VehicleInfo info = this.Info;
		if (info.m_doors != null)
		{
			int num = 0;
			for (int i = 0; i < info.m_doors.Length; i++)
			{
				if ((info.m_doors[i].m_type & type) != (VehicleInfo.DoorType)0)
				{
					num++;
				}
			}
			if (num != 0)
			{
				num = r.Int32((uint)num);
				for (int j = 0; j < info.m_doors.Length; j++)
				{
					if ((info.m_doors[j].m_type & type) != (VehicleInfo.DoorType)0 && num-- == 0)
					{
						return this.m_rotation * info.m_doors[j].m_location + this.m_position;
					}
				}
			}
		}
		return this.m_position;
	}

	public Quaternion m_rotation;

	public Vector3 m_position;

	public float m_travelDistance;

	public uint m_ownerCitizen;

	public ushort m_flags;

	public ushort m_nextGridParked;

	public ushort m_infoIndex;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Updated = 4,
		Parking = 8,
		CustomName = 16,
		All = 65535
	}
}
