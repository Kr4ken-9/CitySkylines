﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class UnlockingGoalsTab : ToolsModifierControl
{
	private void Awake()
	{
		this.m_conditionPanels = new List<UnlockingGoalsTab.ConditionPanel>();
		this.m_checkTitleExpandableItems = new List<ExpandableItem>();
		this.m_winContainer = base.Find<UIScrollablePanel>("WinContainer");
		this.m_loseContainer = base.Find<UIScrollablePanel>("LoseContainer");
		base.get_component().add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnVisChanged));
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnVisChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.InvokeRepeating("RefreshConditionItems", 0f, 0.5f);
			if (Singleton<UnlockManager>.get_exists())
			{
				GenericGuide guide = Singleton<UnlockManager>.get_instance().m_scenarioGoalsNotUsedGuide;
				if (guide != null && !guide.m_disabled)
				{
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						guide.Disable();
					});
				}
			}
		}
		else
		{
			base.CancelInvoke("RefreshConditionItems");
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		if (updateMode == SimulationManager.UpdateMode.LoadGame || updateMode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.RefreshTheWholeTab();
		}
	}

	private void DestroyChildren(UIComponent comp)
	{
		foreach (UIComponent current in comp.get_components())
		{
			Object.Destroy(current.get_gameObject());
		}
	}

	private void RefreshConditionItems()
	{
		foreach (UnlockingGoalsTab.ConditionPanel current in this.m_conditionPanels)
		{
			if (current.m_panel.get_isVisible())
			{
				this.RefreshConditionPanel(current.m_panel, current.m_milestoneInfo);
			}
		}
	}

	private void RefreshTheWholeTab()
	{
		this.DestroyChildren(this.m_winContainer);
		this.DestroyChildren(this.m_loseContainer);
		this.m_conditionPanels.Clear();
		this.m_checkTitleExpandableItems.Clear();
		List<TriggerMilestone> list = new List<TriggerMilestone>();
		List<TriggerMilestone> list2 = new List<TriggerMilestone>();
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
		{
			TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
			for (int i = 0; i < scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = scenarioTriggers[i];
				if (triggerMilestone.m_effects != null)
				{
					TriggerEffect[] effects = triggerMilestone.m_effects;
					for (int j = 0; j < effects.Length; j++)
					{
						TriggerEffect triggerEffect = effects[j];
						bool flag = false;
						bool flag2 = false;
						WinEffect winEffect = triggerEffect as WinEffect;
						if (winEffect != null)
						{
							flag = true;
						}
						LoseEffect loseEffect = triggerEffect as LoseEffect;
						if (loseEffect != null)
						{
							flag2 = true;
						}
						if (flag)
						{
							list.Add(triggerMilestone);
							this.AddTriggerPath(triggerMilestone, this.m_winContainer, true);
						}
						else if (flag2)
						{
							list2.Add(triggerMilestone);
							this.AddTriggerPath(triggerMilestone, this.m_loseContainer, false);
						}
					}
				}
			}
		}
		this.RemoveTitlesWhereNeeded();
	}

	private void RemoveTitlesWhereNeeded()
	{
		foreach (ExpandableItem current in this.m_checkTitleExpandableItems)
		{
			UIComponent component = current.get_gameObject().GetComponent<UIComponent>();
			UIPanel uIPanel = component.Find<UIPanel>("ContentPanel");
			if (uIPanel.get_childCount() == 1)
			{
				current.RemoveTitle();
			}
		}
	}

	private void AddTriggerPath(TriggerMilestone trigger, UIComponent container, bool good)
	{
		UIComponent uIComponent = UITemplateManager.Get<UIComponent>("HighLevelGoal");
		container.AttachUIComponent(uIComponent.get_gameObject());
		uIComponent.Find<UILabel>("HighLevelGoalLabel").set_text(this.GetHighLevelGoalLabel(trigger));
		this.m_currentRootTriggerContainer = uIComponent.Find<UIPanel>("ContentPanel");
		ExpandableItem component = uIComponent.GetComponent<ExpandableItem>();
		component.ForceExpand(0f);
		this.m_currentRootExpandableItem = component;
		this.m_firstTriggerInPath = true;
		this.ListConditions(trigger, component, good);
	}

	private void ListConditions(TriggerMilestone currentTriggerMilestone, ExpandableItem rootExpandableItem, bool good)
	{
		bool flag = false;
		bool flag2 = false;
		if (currentTriggerMilestone.m_conditions != null)
		{
			MilestoneInfo[] conditions = currentTriggerMilestone.m_conditions;
			for (int i = 0; i < conditions.Length; i++)
			{
				MilestoneInfo milestoneInfo = conditions[i];
				TriggerMilestone triggerMilestone = milestoneInfo as TriggerMilestone;
				if (triggerMilestone == null)
				{
					if (!flag || flag2)
					{
						this.AddTrigger(currentTriggerMilestone, !flag && this.CountListableConditions(currentTriggerMilestone) > 1);
						flag = true;
						flag2 = false;
					}
					this.AddCondition(milestoneInfo, good);
				}
				else
				{
					this.ListConditions(triggerMilestone, rootExpandableItem, good);
					flag2 = true;
				}
			}
		}
	}

	private void AddTrigger(TriggerMilestone trigger, bool invincible)
	{
		if (this.m_firstTriggerInPath)
		{
			this.m_firstTriggerInPath = false;
		}
		else
		{
			UIPanel uIPanel = UITemplateManager.Get<UIPanel>("TriggerSeparator");
			this.m_currentRootTriggerContainer.AttachUIComponent(uIPanel.get_gameObject());
		}
		UIComponent uIComponent = UITemplateManager.Get<UIComponent>("Trigger");
		this.m_currentRootTriggerContainer.AttachUIComponent(uIComponent.get_gameObject());
		this.m_currentConditionsContainer = uIComponent.Find<UIPanel>("ContentPanel");
		ExpandableItem component = uIComponent.GetComponent<ExpandableItem>();
		component.m_parentExpandableItem = this.m_currentRootExpandableItem;
		uIComponent.Find<UILabel>("TriggerLabel").set_text(Locale.Get("CONDITIONORDER", trigger.m_conditionOrder.ToString()));
		if (!invincible)
		{
			this.m_checkTitleExpandableItems.Add(component);
		}
	}

	private int CountListableConditions(TriggerMilestone trigger)
	{
		int num = 0;
		if (trigger.m_conditions != null)
		{
			MilestoneInfo[] conditions = trigger.m_conditions;
			for (int i = 0; i < conditions.Length; i++)
			{
				MilestoneInfo milestoneInfo = conditions[i];
				TriggerMilestone triggerMilestone = milestoneInfo as TriggerMilestone;
				if (triggerMilestone == null)
				{
					num++;
				}
			}
		}
		return num;
	}

	private void AddCondition(MilestoneInfo condition, bool good)
	{
		UIPanel uIPanel = UITemplateManager.Get<UIPanel>("ConditionItem");
		this.m_currentConditionsContainer.AttachUIComponent(uIPanel.get_gameObject());
		this.RefreshConditionPanel(uIPanel, condition);
		UnlockingGoalsTab.ConditionPanel item = default(UnlockingGoalsTab.ConditionPanel);
		item.m_milestoneInfo = condition;
		item.m_panel = uIPanel;
		this.m_conditionPanels.Add(item);
		UIProgressBar uIProgressBar = uIPanel.Find<UIProgressBar>("ConditionProgress");
		uIProgressBar.set_progressSprite((!good) ? this.m_scrollbarFillBad : this.m_scrollbarFillGood);
		UISprite uISprite = uIPanel.Find<UISprite>("Done");
		uISprite.set_spriteName((!good) ? this.m_checkBoxBad : this.m_checkBoxGood);
	}

	private string GetHighLevelGoalLabel(TriggerMilestone milestone)
	{
		TriggerEffect[] effects = milestone.m_effects;
		for (int i = 0; i < effects.Length; i++)
		{
			TriggerEffect triggerEffect = effects[i];
			WinEffect winEffect = triggerEffect as WinEffect;
			if (winEffect != null)
			{
				return Locale.Get("GOALSPANEL_TO_WIN");
			}
			LoseEffect loseEffect = triggerEffect as LoseEffect;
			if (loseEffect != null)
			{
				return Locale.Get("GOALSPANEL_TO_LOSE");
			}
		}
		return "Undefined";
	}

	private void OnLocaleChanged()
	{
		this.RefreshTheWholeTab();
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void RefreshConditionPanel(UIPanel panel, MilestoneInfo info)
	{
		MilestoneInfo.ProgressInfo localizedProgress = info.GetLocalizedProgress();
		panel.Find<UILabel>("Description").set_text(localizedProgress.m_description);
		panel.Find<UILabel>("ConditionProgressText").set_text(localizedProgress.m_progress);
		panel.Find<UISprite>("Done").set_isVisible(info.IsPassed());
		UIProgressBar uIProgressBar = panel.Find<UIProgressBar>("ConditionProgress");
		uIProgressBar.set_minValue(localizedProgress.m_min);
		uIProgressBar.set_maxValue(localizedProgress.m_max);
		uIProgressBar.set_value(localizedProgress.m_current);
	}

	private UIScrollablePanel m_winContainer;

	private UIScrollablePanel m_loseContainer;

	public UITextureAtlas m_atlas;

	[UISprite("m_atlas")]
	public string m_checkBoxGood;

	[UISprite("m_atlas")]
	public string m_checkBoxBad;

	[UISprite("m_atlas")]
	public string m_scrollbarFillGood;

	[UISprite("m_atlas")]
	public string m_scrollbarFillBad;

	private List<UnlockingGoalsTab.ConditionPanel> m_conditionPanels;

	private UIPanel m_currentConditionsContainer;

	private UIPanel m_currentRootTriggerContainer;

	private ExpandableItem m_currentRootExpandableItem;

	private bool m_firstTriggerInPath;

	private List<ExpandableItem> m_checkTitleExpandableItems;

	private struct ConditionPanel
	{
		public UIPanel m_panel;

		public MilestoneInfo m_milestoneInfo;
	}
}
