﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.Threading;
using UnityEngine;

public class ColorCorrectionManager : SingletonResource<ColorCorrectionManager>
{
	public static string colorCorrectionsPath
	{
		get
		{
			if (ColorCorrectionManager.m_ColorCorrectionsPath == null)
			{
				ColorCorrectionManager.m_ColorCorrectionsPath = Path.Combine(DataLocation.get_addonsPath(), "ColorCorrections");
			}
			if (!Directory.Exists(ColorCorrectionManager.m_ColorCorrectionsPath))
			{
				Directory.CreateDirectory(ColorCorrectionManager.m_ColorCorrectionsPath);
			}
			return ColorCorrectionManager.m_ColorCorrectionsPath;
		}
	}

	public int lastSelection
	{
		get
		{
			if (this.m_SavedBuiltin != -1 && string.IsNullOrEmpty(this.m_SavedAsset))
			{
				return this.m_SavedBuiltin;
			}
			if (this.m_SavedBuiltin == -1 && !string.IsNullOrEmpty(this.m_SavedAsset))
			{
				for (int i = 0; i < this.m_UserAssets.Count; i++)
				{
					if (this.m_UserAssets[i].get_name() == this.m_SavedAsset.get_value())
					{
						return i + this.m_BuiltinLUTs.Length + 1;
					}
				}
			}
			return 0;
		}
	}

	public int currentSelection
	{
		set
		{
			int num = this.m_BuiltinLUTs.Length + 1;
			if (value < num)
			{
				this.m_SavedBuiltin.set_value(value);
				this.m_SavedAsset.set_value(string.Empty);
				this.SetLUT((value != 0) ? this.m_BuiltinLUTs[value - 1].get_texture() : null);
			}
			else
			{
				this.m_SavedBuiltin.set_value(-1);
				this.m_SavedAsset.set_value(this.m_UserAssets[value - num].get_name());
				ColorCorrectionMetaData colorCorrectionMetaData = this.m_UserAssets[value - num].Instantiate<ColorCorrectionMetaData>();
				colorCorrectionMetaData.assetRef.Cache();
				Image image = new Image(colorCorrectionMetaData.assetRef.get_data());
				this.SetLUT(Texture3DWrapper.Convert(image));
			}
		}
	}

	private void Awake()
	{
		this.m_LUTWrapper = ScriptableObject.CreateInstance<Texture3DWrapper>();
		this.UpdateItems();
	}

	private void Start()
	{
		for (int i = 0; i < ColorCorrectionManager.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + ColorCorrectionManager.m_Extensions[i], ColorCorrectionManager.colorCorrectionsPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
			this.m_FileSystemReporter[i].Start();
		}
	}

	private void OnDestroy()
	{
		for (int i = 0; i < ColorCorrectionManager.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Stop();
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.UpdateItems();
		});
	}

	public void SetLUT(Texture3D desired)
	{
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			ColorCorrectionLut mainLUT = gameObject.GetComponent<CameraController>().m_MainLUT;
			if (mainLUT != null)
			{
				if (desired != null)
				{
					mainLUT.m_LUT = this.m_LUTWrapper;
					this.m_LUTWrapper.set_texture(desired);
				}
				else if (Singleton<RenderManager>.get_exists())
				{
					mainLUT.m_LUT = Singleton<RenderManager>.get_instance().m_properties.m_ColorCorrectionLUT;
				}
			}
		}
	}

	private void UpdateItems()
	{
		try
		{
			PackageManager.DisableEvents();
			foreach (Package current in this.m_TransientPackages)
			{
				PackageManager.Remove(current, true);
			}
			this.m_TransientPackages.Clear();
			bool flag = false;
			DirectoryInfo directoryInfo = new DirectoryInfo(ColorCorrectionManager.colorCorrectionsPath);
			if (directoryInfo.Exists)
			{
				try
				{
					FileInfo[] files = directoryInfo.GetFiles();
					FileInfo[] array = files;
					for (int i = 0; i < array.Length; i++)
					{
						FileInfo fileInfo = array[i];
						if (string.Compare(Path.GetExtension(fileInfo.Name), ".png", StringComparison.OrdinalIgnoreCase) == 0)
						{
							string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileInfo.Name);
							Package.Asset asset = new Package.Asset(fileNameWithoutExtension + "_Data", fileInfo.FullName, Package.AssetType.Data, false);
							ColorCorrectionMetaData colorCorrectionMetaData = new ColorCorrectionMetaData();
							Package package = new Package(fileNameWithoutExtension);
							colorCorrectionMetaData.name = fileNameWithoutExtension;
							colorCorrectionMetaData.assetRef = package.AddAsset(asset);
							asset.Cache();
							package.AddAsset(fileNameWithoutExtension, colorCorrectionMetaData, UserAssetType.ColorCorrection, false);
							Locale locale = new Locale();
							Locale arg_13B_0 = locale;
							Locale.Key key = default(Locale.Key);
							key.m_Identifier = "BUILTIN_COLORCORRECTION";
							key.m_Key = fileNameWithoutExtension;
							arg_13B_0.AddLocalizedString(key, fileNameWithoutExtension);
							package.AddAsset(fileNameWithoutExtension + "_Locale", locale, false);
							this.m_TransientPackages.Add(package);
							flag = true;
						}
					}
				}
				catch (Exception arg)
				{
					Debug.LogError("An exception occured while loading custom color corrections" + arg);
				}
			}
			if (flag)
			{
				PackageManager.ForcePackagesChanged();
				this.currentSelection = this.lastSelection;
			}
		}
		finally
		{
			PackageManager.EnabledEvents();
		}
	}

	public string[] items
	{
		get
		{
			List<string> list = new List<string>();
			list.Add("None");
			for (int i = 0; i < this.m_BuiltinLUTs.Length; i++)
			{
				string name = this.m_BuiltinLUTs[i].get_name();
				list.Add(name);
			}
			this.m_UserAssets.Clear();
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.ColorCorrection
			}))
			{
				if (current.get_isEnabled())
				{
					list.Add(current.get_package().get_packageName() + "." + current.get_name());
					this.m_UserAssets.Add(current);
				}
			}
			return list.ToArray();
		}
	}

	public Texture3DWrapper[] m_BuiltinLUTs;

	private static string[] m_Extensions = Image.GetExtensions(14);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[ColorCorrectionManager.m_Extensions.Length];

	private List<Package> m_TransientPackages = new List<Package>();

	private List<Package.Asset> m_UserAssets = new List<Package.Asset>();

	private Texture3DWrapper m_LUTWrapper;

	private SavedInt m_SavedBuiltin = new SavedInt(Settings.builtinLUT, Settings.gameSettingsFile, DefaultSettings.builtinLUT, true);

	private SavedString m_SavedAsset = new SavedString(Settings.userLUT, Settings.gameSettingsFile, DefaultSettings.userLUT, true);

	private static string m_ColorCorrectionsPath;
}
