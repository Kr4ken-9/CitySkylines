﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class SaunaAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.HealthCare;
		radius1 = this.m_healthCareRadius;
		if (this.m_noiseAccumulation != 0)
		{
			resource2 = ImmaterialResourceManager.Resource.NoisePollution;
			radius2 = this.m_noiseRadius;
		}
		else
		{
			resource2 = ImmaterialResourceManager.Resource.None;
			radius2 = 0f;
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Health:
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		case InfoManager.InfoMode.Happiness:
		case InfoManager.InfoMode.Density:
		{
			IL_18:
			if (infoMode != InfoManager.InfoMode.Connections)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode != InfoManager.SubInfoMode.WindPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (data.m_tempExport != 0 || data.m_finalExport != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		case InfoManager.InfoMode.NoisePollution:
		{
			int noiseAccumulation = this.m_noiseAccumulation;
			return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
		}
		}
		goto IL_18;
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Health;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, num * 5 / 4, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, num * 5 / 4, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, num * 5 / 4, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_healthCareAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHealth, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Entertainment, (float)this.m_healthCareAccumulation, this.m_healthCareRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_healthCareAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHealth, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.Entertainment, (float)(-(float)this.m_healthCareAccumulation), this.m_healthCareRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_customBuffer2 = buildingData.m_customBuffer1;
			buildingData.m_tempImport = 0;
			buildingData.m_tempExport = 0;
			buildingData.m_customBuffer1 = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
		visitPlaceCount += this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.GetVisitBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveVisitorCount, ref totalVisitorCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		int num = productionRate * this.m_healthCareAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.HealthCare, num, buildingData.m_position, this.m_healthCareRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		buildingData.m_tempExport = (byte)Mathf.Clamp(behaviour.m_touristCount * 255 / Mathf.Max(1, aliveVisitorCount), (int)buildingData.m_tempExport, 255);
		buildingData.m_customBuffer1 = (ushort)Mathf.Clamp(aliveVisitorCount, (int)buildingData.m_customBuffer1, 65535);
		if (buildingData.m_finalExport != 0)
		{
			District[] expr_C9_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_C9_cp_0_cp_1 = district;
			expr_C9_cp_0_cp_0[(int)expr_C9_cp_0_cp_1].m_playerConsumption.m_tempExportAmount = expr_C9_cp_0_cp_0[(int)expr_C9_cp_0_cp_1].m_playerConsumption.m_tempExportAmount + (uint)buildingData.m_finalExport;
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalVisitorCount);
		int num2 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount0 + 99) / 100, this.m_visitPlaceCount0 * 5 / 4);
		int num3 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount1 + 99) / 100, this.m_visitPlaceCount1 * 5 / 4);
		int num4 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount2 + 99) / 100, this.m_visitPlaceCount2 * 5 / 4);
		int num5 = Mathf.Max(1, num2 + num3 + num4);
		int num6 = Mathf.Max(0, num5 - totalVisitorCount);
		int num7 = Mathf.Max(Mathf.Max(num2, num3), Mathf.Max(1, num4));
		int num8 = num6 * num2 / num5;
		int num9 = num6 * num3 / num5;
		int num10 = num6 * num4 / num5;
		if (num8 + num9 + num10 > 0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = Mathf.Max(1, num8 * 8 / num7);
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			offer.Amount = num8 + num9 + num10;
			offer.Active = false;
			switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
			{
			case 0:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
				break;
			case 1:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
				break;
			case 2:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
				break;
			case 3:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
				break;
			}
		}
		int num11 = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num11 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num11, buildingData.m_position, this.m_noiseRadius);
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_visitPlaceCount0 != 0 || this.m_visitPlaceCount1 != 0 || this.m_visitPlaceCount2 != 0;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_VISITOR_CAPACITY", new object[]
			{
				num
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int customBuffer = (int)data.m_customBuffer2;
		return LocaleFormatter.FormatGeneric("AIINFO_VISITORS", new object[]
		{
			customBuffer
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_visitPlaceCount0 != 0 || this.m_visitPlaceCount1 != 0 || this.m_visitPlaceCount2 != 0;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 1;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 1;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 1;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 1;

	[CustomizableProperty("Low Wealth", "Visitors", 0)]
	public int m_visitPlaceCount0 = 10;

	[CustomizableProperty("Medium Wealth", "Visitors", 1)]
	public int m_visitPlaceCount1 = 10;

	[CustomizableProperty("High Wealth", "Visitors", 2)]
	public int m_visitPlaceCount2 = 10;

	[CustomizableProperty("Healthcare Accumulation")]
	public int m_healthCareAccumulation = 200;

	[CustomizableProperty("Healthcare Radius")]
	public float m_healthCareRadius = 500f;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;
}
