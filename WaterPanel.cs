﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class WaterPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	protected override void OnHideOptionBars()
	{
		this.ShowResetWaterPanel(false);
		this.ShowWaterPanel(false);
		this.ShowSeaHeightPanel(false);
	}

	protected override void Start()
	{
		if (this.m_OptionsBar != null)
		{
			if (this.m_OptionsWaterPanel == null)
			{
				this.m_OptionsWaterPanel = this.m_OptionsBar.Find<UIPanel>("WaterPanel");
			}
			if (this.m_OptionsResetWaterPanel == null)
			{
				this.m_OptionsResetWaterPanel = this.m_OptionsBar.Find<UIPanel>("ResetWaterPanel");
			}
			if (this.m_OptionsSeaHeightPanel == null)
			{
				this.m_OptionsSeaHeightPanel = this.m_OptionsBar.Find<UIPanel>("SeaHeightPanel");
			}
		}
		base.Start();
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		this.SpawnEntry("PlaceWater", 0);
		this.SpawnEntry("MoveSeaLevel", 1);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		WaterTool waterTool = ToolsModifierControl.SetTool<WaterTool>();
		if (waterTool != null)
		{
			waterTool.m_mode = (WaterTool.Mode)zOrder;
		}
		if (zOrder == 0)
		{
			this.ShowResetWaterPanel(true);
			this.ShowWaterPanel(true);
			this.ShowSeaHeightPanel(false);
		}
		else if (zOrder == 1)
		{
			this.ShowResetWaterPanel(true);
			this.ShowWaterPanel(false);
			this.ShowSeaHeightPanel(true);
		}
	}

	private void ShowResetWaterPanel(bool show)
	{
		if (this.m_OptionsResetWaterPanel != null)
		{
			this.m_OptionsResetWaterPanel.set_isVisible(show);
			this.m_OptionsResetWaterPanel.set_zOrder(1);
		}
	}

	private void ShowWaterPanel(bool show)
	{
		if (this.m_OptionsWaterPanel != null)
		{
			this.m_OptionsWaterPanel.set_isVisible(show);
			this.m_OptionsWaterPanel.set_zOrder(0);
		}
	}

	private void ShowSeaHeightPanel(bool show)
	{
		if (this.m_OptionsSeaHeightPanel != null)
		{
			this.m_OptionsSeaHeightPanel.set_isVisible(show);
			this.m_OptionsSeaHeightPanel.set_zOrder(1);
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("WATER_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("WATER_DESC", name)
		});
		return base.CreateButton(name, tooltip, "Water" + name, index, GeneratedPanel.tooltipBox);
	}

	private UIPanel m_OptionsWaterPanel;

	private UIPanel m_OptionsResetWaterPanel;

	private UIPanel m_OptionsSeaHeightPanel;
}
