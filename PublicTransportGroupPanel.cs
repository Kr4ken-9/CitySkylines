﻿using System;
using ColossalFramework;

public class PublicTransportGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.PublicTransport;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		switch (name)
		{
		case "PublicTransportBus":
			return 0;
		case "PublicTransportTram":
			return 1;
		case "PublicTransportMetro":
			return 2;
		case "PublicTransportTrain":
			return 3;
		case "PublicTransportShip":
			return 4;
		case "PublicTransportPlane":
			return 5;
		case "PublicTransportMonorail":
			return 6;
		case "PublicTransportCableCar":
			return 7;
		case "PublicTransportTaxi":
			return 8;
		}
		return 2147483647;
	}

	protected override GeneratedGroupPanel.GroupInfo CreateGroupInfo(string name, PrefabInfo info)
	{
		return new PublicTransportGroupPanel.PTGroupInfo(name, this.GetCategoryOrder(name), info.GetSubService());
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (GeneratedGroupPanel.GroupFilter)7;
		}
	}

	public class PTGroupInfo : GeneratedGroupPanel.GroupInfo
	{
		public PTGroupInfo(string name, int order, ItemClass.SubService subService) : base(name, order)
		{
			this.m_SubService = subService;
		}

		public override string serviceName
		{
			get
			{
				return EnumExtensions.Name<ItemClass.SubService>(this.m_SubService);
			}
		}

		public override string unlockText
		{
			get
			{
				if (this.m_SubService == ItemClass.SubService.PublicTransportShip)
				{
					if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
					{
						return GeneratedGroupPanel.GetUnlockText(UnlockManager.Feature.Ferry);
					}
				}
				else if (this.m_SubService == ItemClass.SubService.PublicTransportPlane && SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
				{
					return GeneratedGroupPanel.GetUnlockText(UnlockManager.Feature.Blimp);
				}
				return GeneratedGroupPanel.GetUnlockText(this.m_SubService);
			}
		}

		public override bool isUnlocked
		{
			get
			{
				if (this.m_SubService == ItemClass.SubService.PublicTransportShip)
				{
					if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
					{
						return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Ferry);
					}
				}
				else if (this.m_SubService == ItemClass.SubService.PublicTransportPlane && SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
				{
					return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Blimp);
				}
				return ToolsModifierControl.IsUnlocked(this.m_SubService);
			}
		}

		private ItemClass.SubService m_SubService;
	}
}
