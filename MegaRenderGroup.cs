﻿using System;
using ColossalFramework;
using UnityEngine;
using UnityEngine.Rendering;

public class MegaRenderGroup
{
	public MegaRenderGroup(int x, int z)
	{
		this.m_x = x;
		this.m_z = z;
		this.m_position.x = ((float)x + 0.5f - 4.5f) * 1920f;
		this.m_position.y = 0f;
		this.m_position.z = ((float)z + 0.5f - 4.5f) * 1920f;
		this.m_matrix.SetTRS(this.m_position, Quaternion.get_identity(), Vector3.get_one());
	}

	public void ReleaseGroup()
	{
		MegaRenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			MegaRenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if (meshLayer.m_mesh != null)
			{
				Object.Destroy(meshLayer.m_mesh);
				meshLayer.m_mesh = null;
			}
			meshLayer.m_surfaceTexA = null;
			meshLayer.m_surfaceTexB = null;
			meshLayer = nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void Refresh()
	{
		MegaRenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			MegaRenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if (meshLayer.m_meshDirty)
			{
				this.UpdateMesh(meshLayer);
			}
			meshLayer = nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void Render()
	{
		MegaRenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = this.m_layersRendered2 & ~this.m_instanceMask;
		int num2 = this.m_layersRendered1 & ~num;
		int num3 = 0;
		while (meshLayer != null)
		{
			MegaRenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if ((num & 1 << meshLayer.m_layer) != 0)
			{
				if (meshLayer.m_meshDirty)
				{
					this.UpdateMesh(meshLayer);
				}
				if (meshLayer.m_mesh == null)
				{
					num2 |= 1 << meshLayer.m_layer;
				}
				else
				{
					this.RenderMesh(meshLayer);
				}
			}
			meshLayer = nextLayer;
			if (++num3 >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		this.m_layersRendered1 = 0;
		this.m_layersRendered2 = 0;
		this.m_instanceMask = 0;
		this.m_groupMask = num2;
	}

	public void SetAllLayersDirty()
	{
		MegaRenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			meshLayer.m_meshDirty = true;
			meshLayer = meshLayer.m_nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void SetLayerDirty(int layerIndex)
	{
		MegaRenderGroup.MeshLayer meshLayer = null;
		MegaRenderGroup.MeshLayer meshLayer2 = this.m_layers;
		int num = 0;
		while (meshLayer2 != null)
		{
			if (meshLayer2.m_layer == layerIndex)
			{
				meshLayer2.m_meshDirty = true;
				return;
			}
			meshLayer = meshLayer2;
			meshLayer2 = meshLayer2.m_nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		meshLayer2 = new MegaRenderGroup.MeshLayer();
		meshLayer2.m_layer = layerIndex;
		meshLayer2.m_meshDirty = true;
		if (meshLayer != null)
		{
			meshLayer.m_nextLayer = meshLayer2;
		}
		else
		{
			this.m_layers = meshLayer2;
		}
	}

	private void UpdateMesh(MegaRenderGroup.MeshLayer layer)
	{
		RenderManager instance = Singleton<RenderManager>.get_instance();
		layer.m_vertexCount = 0;
		layer.m_triangleCount = 0;
		layer.m_objectCount = 0;
		layer.m_surfaceTexA = null;
		layer.m_surfaceTexB = null;
		layer.m_heightMap = null;
		layer.m_hasWaterHeightMap = false;
		int num = 5;
		int num2 = this.m_x * num;
		int num3 = this.m_z * num;
		int num4 = (this.m_x + 1) * num - 1;
		int num5 = (this.m_z + 1) * num - 1;
		FastList<CombineInstance> fastList = new FastList<CombineInstance>();
		fastList.EnsureCapacity(num * num);
		Vector3 vector;
		vector..ctor(1000000f, 1000000f, 1000000f);
		Vector3 vector2;
		vector2..ctor(-1000000f, -1000000f, -1000000f);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				int num6 = i * 45 + j;
				RenderGroup renderGroup = instance.m_groups[num6];
				if (renderGroup != null)
				{
					RenderGroup.MeshLayer layer2 = renderGroup.GetLayer(layer.m_layer);
					if (layer2 != null)
					{
						if (layer2.m_meshDirty)
						{
							renderGroup.UpdateMesh(layer2);
						}
						if (layer2.m_mesh != null && layer2.m_triangleCount != 0)
						{
							Matrix4x4 transform = default(Matrix4x4);
							transform.SetTRS(renderGroup.m_position - this.m_position, Quaternion.get_identity(), Vector3.get_one());
							for (RenderGroup.MeshWrapper meshWrapper = layer2.m_mesh; meshWrapper != null; meshWrapper = meshWrapper.m_next)
							{
								if (meshWrapper.m_mesh != null)
								{
									CombineInstance item = default(CombineInstance);
									item.set_mesh(meshWrapper.m_mesh);
									item.set_transform(transform);
									fastList.Add(item);
								}
							}
							layer.m_vertexCount += layer2.m_vertexCount;
							layer.m_triangleCount += layer2.m_triangleCount;
							layer.m_objectCount += layer2.m_objectCount;
							if (layer2.m_surfaceTexA != null && layer.m_surfaceTexA == null)
							{
								layer.m_surfaceTexA = layer2.m_surfaceTexA;
								layer.m_surfaceTexB = layer2.m_surfaceTexB;
								layer.m_surfaceMapping = layer2.m_surfaceMapping;
							}
							if (layer2.m_heightMap != null && layer.m_heightMap == null)
							{
								layer.m_heightMap = layer2.m_heightMap;
								layer.m_heightMapping = layer2.m_heightMapping;
								layer.m_surfaceMapping = layer2.m_surfaceMapping;
							}
							layer.m_hasWaterHeightMap |= layer2.m_hasWaterHeightMap;
							vector = Vector3.Min(vector, layer2.m_bounds.get_min());
							vector2 = Vector3.Max(vector2, layer2.m_bounds.get_max());
						}
					}
				}
			}
		}
		if (layer.m_vertexCount == 0 || layer.m_vertexCount > 30000)
		{
			if (layer.m_mesh != null)
			{
				Object.Destroy(layer.m_mesh);
				layer.m_mesh = null;
			}
		}
		else
		{
			if (layer.m_mesh == null)
			{
				layer.m_mesh = new Mesh();
			}
			layer.m_mesh.CombineMeshes(fastList.ToArray(), true, true);
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(vector - this.m_position, vector2 - this.m_position);
			layer.m_mesh.set_bounds(bounds);
		}
		if (layer.m_layer == instance.lightSystem.m_lightLayer)
		{
			LightSystem lightSystem = instance.lightSystem;
			layer.m_commandBuffer = lightSystem.m_lightBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = lightSystem.m_lightMaterialVolumeGroup;
			layer.m_shadows = false;
		}
		else if (layer.m_layer == instance.lightSystem.m_lightLayerFloating)
		{
			LightSystem lightSystem2 = instance.lightSystem;
			layer.m_commandBuffer = lightSystem2.m_lightBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = lightSystem2.m_lightFloatingMaterialVolumeGroup;
			layer.m_shadows = false;
		}
		else if (layer.m_layer == Singleton<NetManager>.get_instance().m_arrowLayer)
		{
			layer.m_commandBuffer = instance.m_overlayBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = null;
			layer.m_shadows = false;
		}
		else
		{
			layer.m_commandBuffer = null;
			layer.m_bufferMaterial = null;
			layer.m_renderMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_shadows = true;
		}
		layer.m_meshDirty = false;
	}

	private void RenderMesh(MegaRenderGroup.MeshLayer layer)
	{
		if (layer.m_mesh != null)
		{
			int num = 0;
			RenderManager instance = Singleton<RenderManager>.get_instance();
			if (layer.m_surfaceTexA != null || layer.m_heightMap != null)
			{
				TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
				BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
				instance2.m_materialBlock1.Clear();
				if (layer.m_surfaceTexA != null)
				{
					instance2.m_materialBlock1.SetTexture(instance2.ID_SurfaceTexA, layer.m_surfaceTexA);
					instance2.m_materialBlock1.SetTexture(instance2.ID_SurfaceTexB, layer.m_surfaceTexB);
				}
				if (layer.m_heightMap != null)
				{
					instance2.m_materialBlock1.SetTexture(instance3.ID_HeightMap, layer.m_heightMap);
					instance2.m_materialBlock1.SetVector(instance3.ID_HeightMapping, layer.m_heightMapping);
				}
				if (layer.m_hasWaterHeightMap)
				{
					Texture texture;
					Vector4 vector;
					Vector4 vector2;
					Singleton<TerrainManager>.get_instance().GetWaterMapping(this.m_position, out texture, out vector, out vector2);
					instance2.m_materialBlock1.SetTexture(instance3.ID_WaterHeightMap, texture);
					instance2.m_materialBlock1.SetVector(instance3.ID_WaterHeightMapping, vector);
					instance2.m_materialBlock1.SetVector(instance3.ID_WaterSurfaceMapping, vector2);
				}
				instance2.m_materialBlock1.SetVector(instance2.ID_SurfaceMapping, layer.m_surfaceMapping);
				if (layer.m_bufferMaterial != null)
				{
					num++;
					layer.m_commandBuffer.DrawMesh(layer.m_mesh, this.m_matrix, layer.m_bufferMaterial, 0, -1, instance2.m_materialBlock1);
				}
				if (layer.m_renderMaterial != null)
				{
					num++;
					Graphics.DrawMesh(layer.m_mesh, this.m_matrix, layer.m_renderMaterial, layer.m_layer, null, 0, instance2.m_materialBlock1, layer.m_shadows, layer.m_shadows);
				}
			}
			else
			{
				if (layer.m_bufferMaterial != null)
				{
					num++;
					layer.m_commandBuffer.DrawMesh(layer.m_mesh, this.m_matrix, layer.m_bufferMaterial);
				}
				if (layer.m_renderMaterial != null)
				{
					num++;
					Graphics.DrawMesh(layer.m_mesh, this.m_matrix, layer.m_renderMaterial, layer.m_layer, null, 0, null, layer.m_shadows, layer.m_shadows);
				}
			}
			RenderManager expr_23F_cp_0 = instance;
			expr_23F_cp_0.m_drawCallData.m_lodCalls = expr_23F_cp_0.m_drawCallData.m_lodCalls + num;
			RenderManager expr_252_cp_0 = instance;
			expr_252_cp_0.m_drawCallData.m_batchedCalls = expr_252_cp_0.m_drawCallData.m_batchedCalls + (layer.m_objectCount - 1);
		}
	}

	public int m_x;

	public int m_z;

	public int m_layersRendered1;

	public int m_layersRendered2;

	public int m_instanceMask;

	public int m_groupMask;

	public Vector3 m_position;

	public Matrix4x4 m_matrix;

	[NonSerialized]
	public MegaRenderGroup.MeshLayer m_layers;

	public class MeshLayer
	{
		public CommandBuffer m_commandBuffer;

		public Material m_bufferMaterial;

		public Material m_renderMaterial;

		public MegaRenderGroup.MeshLayer m_nextLayer;

		public Mesh m_mesh;

		public Texture m_surfaceTexA;

		public Texture m_surfaceTexB;

		public Texture m_heightMap;

		public Vector4 m_surfaceMapping;

		public Vector4 m_heightMapping;

		public bool m_hasWaterHeightMap;

		public int m_layer;

		public int m_vertexCount;

		public int m_triangleCount;

		public int m_objectCount;

		public bool m_meshDirty;

		public bool m_requireSurfaceMaps;

		public bool m_shadows;
	}
}
