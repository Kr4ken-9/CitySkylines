﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct Vehicle
{
	public bool RenderInstance(RenderManager.CameraInfo cameraInfo, ushort vehicleID)
	{
		if ((this.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
		{
			return false;
		}
		VehicleInfo info = this.Info;
		if (info == null)
		{
			return false;
		}
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vector3 framePosition = this.GetFramePosition(targetFrame - 32u);
		float maxDistance = Mathf.Min(Mathf.Max(info.m_maxRenderDistance, RenderManager.LevelOfDetailFactor * 5000f), info.m_maxRenderDistance * (1f + cameraInfo.m_height * 0.0005f) + cameraInfo.m_height * 0.4f);
		if (!cameraInfo.CheckRenderDistance(framePosition, maxDistance))
		{
			return false;
		}
		if (!cameraInfo.Intersect(framePosition, info.m_generatedInfo.m_size.z * 0.5f + 15f))
		{
			return false;
		}
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		bool flag = frameData2.m_underground && frameData.m_underground;
		bool flag2 = frameData2.m_insideBuilding && frameData.m_insideBuilding;
		bool flag3 = frameData2.m_transition || frameData.m_transition;
		if (flag2 && !flag3)
		{
			return false;
		}
		if (flag && !flag3)
		{
			if ((cameraInfo.m_layerMask & 1 << Singleton<VehicleManager>.get_instance().m_undergroundLayer) == 0)
			{
				return false;
			}
		}
		else if ((cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) == 0)
		{
			return false;
		}
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 position = bezier.Position(num);
		Bezier3 bezier2 = default(Bezier3);
		bezier2.a = frameData.m_swayPosition;
		bezier2.b = frameData.m_swayPosition + frameData.m_swayVelocity * 0.333f;
		bezier2.c = frameData2.m_swayPosition - frameData2.m_swayVelocity * 0.333f;
		bezier2.d = frameData2.m_swayPosition;
		Vector3 swayPosition = bezier2.Position(num);
		swayPosition.x *= info.m_leanMultiplier / Mathf.Max(1f, info.m_generatedInfo.m_wheelGauge);
		swayPosition.z *= info.m_nodMultiplier / Mathf.Max(1f, info.m_generatedInfo.m_wheelBase);
		Vector4 lightState = (num < 0.5f) ? frameData.m_lightIntensity : frameData2.m_lightIntensity;
		Quaternion rotation = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num);
		Color color = info.m_vehicleAI.GetColor(vehicleID, ref this, Singleton<InfoManager>.get_instance().CurrentMode);
		color.a = ((num < 0.5f) ? frameData.m_blinkState : frameData2.m_blinkState);
		Vector4 tyrePosition;
		tyrePosition.x = frameData.m_steerAngle + (frameData2.m_steerAngle - frameData.m_steerAngle) * num;
		tyrePosition.y = frameData.m_travelDistance + (frameData2.m_travelDistance - frameData.m_travelDistance) * num;
		tyrePosition.z = 0f;
		tyrePosition.w = 0f;
		Vector3 velocity = Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num) * 3.75f;
		float acceleration = frameData2.m_velocity.get_magnitude() - frameData.m_velocity.get_magnitude();
		InstanceID id = default(InstanceID);
		id.Vehicle = vehicleID;
		Vehicle.RenderInstance(cameraInfo, info, position, rotation, swayPosition, lightState, tyrePosition, velocity, acceleration, color, this.m_flags, id, flag || flag3, !flag || flag3);
		return true;
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, VehicleInfo info, Vector3 position, Quaternion rotation, Vector3 swayPosition, Vector4 lightState, Vector4 tyrePosition, Vector3 velocity, float acceleration, Color color, Vehicle.Flags flags, InstanceID id, bool underground, bool overground)
	{
		if ((cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) == 0)
		{
			return;
		}
		Vector3 one = Vector3.get_one();
		if ((flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
		{
			one..ctor(-1f, 1f, -1f);
			Vector4 vector = lightState;
			lightState.x = vector.y;
			lightState.y = vector.x;
			lightState.z = vector.w;
			lightState.w = vector.z;
		}
		info.m_vehicleAI.RenderExtraStuff(cameraInfo, id, position, rotation, tyrePosition);
		if (cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Matrix4x4 matrix4x = info.m_vehicleAI.CalculateBodyMatrix(flags, ref position, ref rotation, ref one, ref swayPosition);
			Matrix4x4 matrix4x2 = info.m_vehicleAI.CalculateTyreMatrix(flags, ref position, ref rotation, ref one, ref matrix4x);
			if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
			{
				RenderGroup.MeshData effectMeshData = info.m_vehicleAI.GetEffectMeshData();
				EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix4x, effectMeshData, info.m_generatedInfo.m_tyres, info.m_lightPositions);
				if (info.m_effects != null)
				{
					for (int i = 0; i < info.m_effects.Length; i++)
					{
						VehicleInfo.Effect effect = info.m_effects[i];
						if (((effect.m_vehicleFlagsRequired | effect.m_vehicleFlagsForbidden) & flags) == effect.m_vehicleFlagsRequired && effect.m_parkedFlagsRequired == VehicleParked.Flags.None)
						{
							effect.m_effect.RenderEffect(id, area, velocity, acceleration, 1f, -1f, Singleton<SimulationManager>.get_instance().m_simulationTimeDelta, cameraInfo);
						}
					}
				}
			}
			if ((flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
			{
				tyrePosition.x = -tyrePosition.x;
				tyrePosition.y = -tyrePosition.y;
			}
			MaterialPropertyBlock materialBlock = instance.m_materialBlock;
			materialBlock.Clear();
			materialBlock.SetMatrix(instance.ID_TyreMatrix, matrix4x2);
			materialBlock.SetVector(instance.ID_TyrePosition, tyrePosition);
			materialBlock.SetVector(instance.ID_LightState, lightState);
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode != ItemClass.Availability.AssetEditor)
			{
				materialBlock.SetColor(instance.ID_Color, color);
			}
			bool flag = true;
			if (info.m_subMeshes != null)
			{
				for (int j = 0; j < info.m_subMeshes.Length; j++)
				{
					VehicleInfo.MeshInfo meshInfo = info.m_subMeshes[j];
					VehicleInfoBase subInfo = meshInfo.m_subInfo;
					if (((meshInfo.m_vehicleFlagsRequired | meshInfo.m_vehicleFlagsForbidden) & flags) == meshInfo.m_vehicleFlagsRequired && meshInfo.m_parkedFlagsRequired == VehicleParked.Flags.None)
					{
						if (subInfo != null)
						{
							VehicleManager expr_283_cp_0 = instance;
							expr_283_cp_0.m_drawCallData.m_defaultCalls = expr_283_cp_0.m_drawCallData.m_defaultCalls + 1;
							if (underground)
							{
								if (subInfo.m_undergroundMaterial == null && subInfo.m_material != null)
								{
									VehicleProperties properties = instance.m_properties;
									if (properties != null)
									{
										subInfo.m_undergroundMaterial = new Material(properties.m_undergroundShader);
										subInfo.m_undergroundMaterial.CopyPropertiesFromMaterial(subInfo.m_material);
									}
								}
								subInfo.m_undergroundMaterial.SetVectorArray(instance.ID_TyreLocation, subInfo.m_generatedInfo.m_tyres);
								Graphics.DrawMesh(subInfo.m_mesh, matrix4x, subInfo.m_undergroundMaterial, instance.m_undergroundLayer, null, 0, materialBlock);
							}
							if (overground)
							{
								subInfo.m_material.SetVectorArray(instance.ID_TyreLocation, subInfo.m_generatedInfo.m_tyres);
								Graphics.DrawMesh(subInfo.m_mesh, matrix4x, subInfo.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
							}
						}
					}
					else if (subInfo == null)
					{
						flag = false;
					}
				}
			}
			if (flag)
			{
				VehicleManager expr_3AC_cp_0 = instance;
				expr_3AC_cp_0.m_drawCallData.m_defaultCalls = expr_3AC_cp_0.m_drawCallData.m_defaultCalls + 1;
				if (underground)
				{
					if (info.m_undergroundMaterial == null && info.m_material != null)
					{
						VehicleProperties properties2 = instance.m_properties;
						if (properties2 != null)
						{
							info.m_undergroundMaterial = new Material(properties2.m_undergroundShader);
							info.m_undergroundMaterial.CopyPropertiesFromMaterial(info.m_material);
						}
					}
					info.m_undergroundMaterial.SetVectorArray(instance.ID_TyreLocation, info.m_generatedInfo.m_tyres);
					Graphics.DrawMesh(info.m_mesh, matrix4x, info.m_undergroundMaterial, instance.m_undergroundLayer, null, 0, materialBlock);
				}
				if (overground)
				{
					info.m_material.SetVectorArray(instance.ID_TyreLocation, info.m_generatedInfo.m_tyres);
					Graphics.DrawMesh(info.m_mesh, matrix4x, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
				}
			}
		}
		else
		{
			Matrix4x4 matrix4x3 = info.m_vehicleAI.CalculateBodyMatrix(flags, ref position, ref rotation, ref one, ref swayPosition);
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				Matrix4x4 matrix4x4 = info.m_vehicleAI.CalculateTyreMatrix(flags, ref position, ref rotation, ref one, ref matrix4x3);
				VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
				MaterialPropertyBlock materialBlock2 = instance2.m_materialBlock;
				materialBlock2.Clear();
				materialBlock2.SetMatrix(instance2.ID_TyreMatrix, matrix4x4);
				materialBlock2.SetVector(instance2.ID_TyrePosition, tyrePosition);
				materialBlock2.SetVector(instance2.ID_LightState, lightState);
				Mesh mesh = null;
				Material material = null;
				if (info.m_lodObject != null)
				{
					MeshFilter component = info.m_lodObject.GetComponent<MeshFilter>();
					if (component != null)
					{
						mesh = component.get_sharedMesh();
					}
					Renderer component2 = info.m_lodObject.GetComponent<Renderer>();
					if (component2 != null)
					{
						material = component2.get_sharedMaterial();
					}
				}
				if (mesh != null && material != null)
				{
					materialBlock2.SetVectorArray(instance2.ID_TyreLocation, info.m_generatedInfo.m_tyres);
					Graphics.DrawMesh(mesh, matrix4x3, material, info.m_prefabDataLayer, null, 0, materialBlock2);
				}
			}
			else if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
			{
				RenderGroup.MeshData effectMeshData2 = info.m_vehicleAI.GetEffectMeshData();
				EffectInfo.SpawnArea area2 = new EffectInfo.SpawnArea(matrix4x3, effectMeshData2, info.m_generatedInfo.m_tyres, info.m_lightPositions);
				if (info.m_effects != null)
				{
					for (int k = 0; k < info.m_effects.Length; k++)
					{
						VehicleInfo.Effect effect2 = info.m_effects[k];
						if (((effect2.m_vehicleFlagsRequired | effect2.m_vehicleFlagsForbidden) & flags) == effect2.m_vehicleFlagsRequired && effect2.m_parkedFlagsRequired == VehicleParked.Flags.None)
						{
							effect2.m_effect.RenderEffect(id, area2, velocity, acceleration, 1f, -1f, Singleton<SimulationManager>.get_instance().m_simulationTimeDelta, cameraInfo);
						}
					}
				}
			}
			bool flag2 = true;
			if (info.m_subMeshes != null)
			{
				for (int l = 0; l < info.m_subMeshes.Length; l++)
				{
					VehicleInfo.MeshInfo meshInfo2 = info.m_subMeshes[l];
					VehicleInfoBase subInfo2 = meshInfo2.m_subInfo;
					if (((meshInfo2.m_vehicleFlagsRequired | meshInfo2.m_vehicleFlagsForbidden) & flags) == meshInfo2.m_vehicleFlagsRequired && meshInfo2.m_parkedFlagsRequired == VehicleParked.Flags.None)
					{
						if (subInfo2 != null)
						{
							if (underground)
							{
								subInfo2.m_undergroundLodTransforms[subInfo2.m_undergroundLodCount] = matrix4x3;
								subInfo2.m_undergroundLodLightStates[subInfo2.m_undergroundLodCount] = lightState;
								subInfo2.m_undergroundLodColors[subInfo2.m_undergroundLodCount] = color.get_linear();
								subInfo2.m_undergroundLodMin = Vector3.Min(subInfo2.m_undergroundLodMin, position);
								subInfo2.m_undergroundLodMax = Vector3.Max(subInfo2.m_undergroundLodMax, position);
								if (++subInfo2.m_undergroundLodCount == subInfo2.m_undergroundLodTransforms.Length)
								{
									Vehicle.RenderUndergroundLod(cameraInfo, subInfo2);
								}
							}
							if (overground)
							{
								subInfo2.m_lodTransforms[subInfo2.m_lodCount] = matrix4x3;
								subInfo2.m_lodLightStates[subInfo2.m_lodCount] = lightState;
								subInfo2.m_lodColors[subInfo2.m_lodCount] = color.get_linear();
								subInfo2.m_lodMin = Vector3.Min(subInfo2.m_lodMin, position);
								subInfo2.m_lodMax = Vector3.Max(subInfo2.m_lodMax, position);
								if (++subInfo2.m_lodCount == subInfo2.m_lodTransforms.Length)
								{
									Vehicle.RenderLod(cameraInfo, subInfo2);
								}
							}
						}
					}
					else if (subInfo2 == null)
					{
						flag2 = false;
					}
				}
			}
			if (flag2)
			{
				if (underground)
				{
					info.m_undergroundLodTransforms[info.m_undergroundLodCount] = matrix4x3;
					info.m_undergroundLodLightStates[info.m_undergroundLodCount] = lightState;
					info.m_undergroundLodColors[info.m_undergroundLodCount] = color.get_linear();
					info.m_undergroundLodMin = Vector3.Min(info.m_undergroundLodMin, position);
					info.m_undergroundLodMax = Vector3.Max(info.m_undergroundLodMax, position);
					if (++info.m_undergroundLodCount == info.m_undergroundLodTransforms.Length)
					{
						Vehicle.RenderUndergroundLod(cameraInfo, info);
					}
				}
				if (overground)
				{
					info.m_lodTransforms[info.m_lodCount] = matrix4x3;
					info.m_lodLightStates[info.m_lodCount] = lightState;
					info.m_lodColors[info.m_lodCount] = color.get_linear();
					info.m_lodMin = Vector3.Min(info.m_lodMin, position);
					info.m_lodMax = Vector3.Max(info.m_lodMax, position);
					if (++info.m_lodCount == info.m_lodTransforms.Length)
					{
						Vehicle.RenderLod(cameraInfo, info);
					}
				}
			}
		}
	}

	public void RenderOverlay(RenderManager.CameraInfo cameraInfo, ushort vehicleID, Color color)
	{
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 vector = bezier.Position(num);
		Bezier3 bezier2 = default(Bezier3);
		bezier2.a = frameData.m_swayPosition;
		bezier2.b = frameData.m_swayPosition + frameData.m_swayVelocity * 0.333f;
		bezier2.c = frameData2.m_swayPosition - frameData2.m_swayVelocity * 0.333f;
		bezier2.d = frameData2.m_swayPosition;
		Vector3 vector2 = bezier2.Position(num);
		vector2.x *= info.m_leanMultiplier / Mathf.Max(1f, info.m_generatedInfo.m_wheelGauge);
		vector2.z *= info.m_nodMultiplier / Mathf.Max(1f, info.m_generatedInfo.m_wheelBase);
		Quaternion quaternion = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num);
		Vector3 one = Vector3.get_one();
		Matrix4x4 matrix4x = info.m_vehicleAI.CalculateBodyMatrix(this.m_flags, ref vector, ref quaternion, ref one, ref vector2);
		Vector3 vector3 = info.m_generatedInfo.m_size * 0.5f;
		vector3.x += 0.15f;
		vector3.z += 0.3f;
		Quad3 quad = default(Quad3);
		quad.a = matrix4x.MultiplyPoint(new Vector3(-vector3.x, 0f, -vector3.z));
		quad.b = matrix4x.MultiplyPoint(new Vector3(vector3.x, 0f, -vector3.z));
		quad.c = matrix4x.MultiplyPoint(new Vector3(vector3.x, 0f, vector3.z));
		quad.d = matrix4x.MultiplyPoint(new Vector3(-vector3.x, 0f, vector3.z));
		ToolManager expr_2A5_cp_0 = Singleton<ToolManager>.get_instance();
		expr_2A5_cp_0.m_drawCallData.m_overlayCalls = expr_2A5_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, vector.y - info.m_generatedInfo.m_negativeHeight - 2f, vector.y + info.m_generatedInfo.m_size.y + 2f, false, true);
	}

	public void CheckOverlayAlpha(ref float alpha)
	{
		VehicleInfo info = this.Info;
		float num = Mathf.Min(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * 0.5f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num)));
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, VehicleInfoBase info)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_lodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_lodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else if (info.m_lodCount <= 8)
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMeshCombined16;
			num = 16;
		}
		if (info.m_lodCount < num)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(cameraInfo.m_forward * -100000f, Quaternion.get_identity(), Vector3.get_one());
			for (int i = info.m_lodCount; i < num; i++)
			{
				info.m_lodTransforms[i] = matrix4x;
				info.m_lodLightStates[i] = Vector4.get_zero();
				info.m_lodColors[i] = new Color(0f, 0f, 0f, 0f);
			}
		}
		materialBlock.SetMatrixArray(instance.ID_VehicleTransform, info.m_lodTransforms);
		materialBlock.SetVectorArray(instance.ID_VehicleLightState, info.m_lodLightStates);
		materialBlock.SetVectorArray(instance.ID_VehicleColor, info.m_lodColors);
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_lodMin - new Vector3(100f, 100f, 100f), info.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			VehicleManager expr_1E7_cp_0 = instance;
			expr_1E7_cp_0.m_drawCallData.m_lodCalls = expr_1E7_cp_0.m_drawCallData.m_lodCalls + 1;
			VehicleManager expr_1FA_cp_0 = instance;
			expr_1FA_cp_0.m_drawCallData.m_batchedCalls = expr_1FA_cp_0.m_drawCallData.m_batchedCalls + (info.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_lodMaterialCombined, info.m_prefabDataLayer, null, 0, materialBlock);
		}
		info.m_lodCount = 0;
	}

	public static void RenderUndergroundLod(RenderManager.CameraInfo cameraInfo, VehicleInfoBase info)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		if (info.m_undergroundLodMaterial == null)
		{
			info.m_undergroundLodMaterial = new Material(instance.m_properties.m_undergroundShader);
			info.m_undergroundLodMaterial.CopyPropertiesFromMaterial(info.m_lodMaterialCombined);
			info.m_undergroundLodMaterial.EnableKeyword("MULTI_INSTANCE");
		}
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_undergroundLodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_undergroundLodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else if (info.m_undergroundLodCount <= 8)
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMeshCombined16;
			num = 16;
		}
		if (info.m_undergroundLodCount < num)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(cameraInfo.m_forward * -100000f, Quaternion.get_identity(), Vector3.get_one());
			for (int i = info.m_undergroundLodCount; i < num; i++)
			{
				info.m_undergroundLodTransforms[i] = matrix4x;
				info.m_undergroundLodLightStates[i] = Vector4.get_zero();
				info.m_undergroundLodColors[i] = new Color(0f, 0f, 0f, 0f);
			}
		}
		materialBlock.SetMatrixArray(instance.ID_VehicleTransform, info.m_undergroundLodTransforms);
		materialBlock.SetVectorArray(instance.ID_VehicleLightState, info.m_undergroundLodLightStates);
		materialBlock.SetVectorArray(instance.ID_VehicleColor, info.m_undergroundLodColors);
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_undergroundLodMin - new Vector3(100f, 100f, 100f), info.m_undergroundLodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_undergroundLodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_undergroundLodMax = new Vector3(-100000f, -100000f, -100000f);
			VehicleManager expr_22F_cp_0 = instance;
			expr_22F_cp_0.m_drawCallData.m_lodCalls = expr_22F_cp_0.m_drawCallData.m_lodCalls + 1;
			VehicleManager expr_242_cp_0 = instance;
			expr_242_cp_0.m_drawCallData.m_batchedCalls = expr_242_cp_0.m_drawCallData.m_batchedCalls + (info.m_undergroundLodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_undergroundLodMaterial, instance.m_undergroundLayer, null, 0, materialBlock);
		}
		info.m_undergroundLodCount = 0;
	}

	public void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort vehicleID)
	{
		if ((this.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
		{
			return;
		}
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		if (info.m_isLargeVehicle)
		{
			if (Vector3.SqrMagnitude(listenerInfo.m_position - frameData.m_position) >= 360000f)
			{
				return;
			}
		}
		else if (Vector3.SqrMagnitude(listenerInfo.m_position - frameData.m_position) >= 22500f)
		{
			return;
		}
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Vector3 vector = Vector3.Lerp(frameData.m_position, frameData2.m_position, num);
		Vector3 velocity = Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num) * 3.75f;
		Vector3 vector2 = Vector3.Lerp(frameData.m_swayPosition, frameData2.m_swayPosition, num);
		Quaternion quaternion = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num);
		Vector3 one = Vector3.get_one();
		Matrix4x4 matrix = info.m_vehicleAI.CalculateBodyMatrix(this.m_flags, ref vector, ref quaternion, ref one, ref vector2);
		InstanceID id = default(InstanceID);
		id.Vehicle = vehicleID;
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix, info.m_lodMeshData);
		float audioAcceleration = info.m_vehicleAI.GetAudioAcceleration(ref frameData, ref frameData2, num);
		if (info.m_effects != null)
		{
			for (int i = 0; i < info.m_effects.Length; i++)
			{
				VehicleInfo.Effect effect = info.m_effects[i];
				if (((effect.m_vehicleFlagsRequired | effect.m_vehicleFlagsForbidden) & this.m_flags) == effect.m_vehicleFlagsRequired && effect.m_parkedFlagsRequired == VehicleParked.Flags.None)
				{
					effect.m_effect.PlayEffect(id, area, velocity, audioAcceleration, 1f, listenerInfo, Singleton<VehicleManager>.get_instance().m_audioGroup);
				}
			}
		}
	}

	public Vector3 GetSmoothPosition(ushort vehicleID)
	{
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		return bezier.Position(num);
	}

	public void GetSmoothPosition(ushort vehicleID, out Vector3 position, out Quaternion rotation)
	{
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		position = bezier.Position(num);
		rotation = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num);
		if ((this.m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
		{
			rotation = Quaternion.AngleAxis(180f, Vector3.get_up()) * rotation;
		}
	}

	public Vector3 GetSmoothVelocity(ushort vehicleID)
	{
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		return Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num) * 3.75f;
	}

	public VehicleInfo Info
	{
		get
		{
			return PrefabCollection<VehicleInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public Vehicle.Frame GetLastFrameData()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0;
		case 1:
			return this.m_frame1;
		case 2:
			return this.m_frame2;
		case 3:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public Vector3 GetLastFramePosition()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0.m_position;
		case 1:
			return this.m_frame1.m_position;
		case 2:
			return this.m_frame2.m_position;
		case 3:
			return this.m_frame3.m_position;
		default:
			return this.m_frame0.m_position;
		}
	}

	public Vector3 GetLastFrameVelocity()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0.m_velocity;
		case 1:
			return this.m_frame1.m_velocity;
		case 2:
			return this.m_frame2.m_velocity;
		case 3:
			return this.m_frame3.m_velocity;
		default:
			return this.m_frame0.m_velocity;
		}
	}

	public Vehicle.Frame GetFrameData(uint simulationFrame)
	{
		simulationFrame = (simulationFrame >> 4 & 3u);
		switch (simulationFrame)
		{
		case 0u:
			return this.m_frame0;
		case 1u:
			return this.m_frame1;
		case 2u:
			return this.m_frame2;
		case 3u:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public Vector3 GetFramePosition(uint simulationFrame)
	{
		simulationFrame = (simulationFrame >> 4 & 3u);
		switch (simulationFrame)
		{
		case 0u:
			return this.m_frame0.m_position;
		case 1u:
			return this.m_frame1.m_position;
		case 2u:
			return this.m_frame2.m_position;
		case 3u:
			return this.m_frame3.m_position;
		default:
			return this.m_frame0.m_position;
		}
	}

	public void SetFrameData(uint simulationFrame, Vehicle.Frame data)
	{
		this.m_lastFrame = (byte)(simulationFrame >> 4 & 3u);
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public void SetLastFrameData(Vehicle.Frame data)
	{
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public Vector4 GetTargetPos(int index)
	{
		switch (index)
		{
		case 0:
			return this.m_targetPos0;
		case 1:
			return this.m_targetPos1;
		case 2:
			return this.m_targetPos2;
		case 3:
			return this.m_targetPos3;
		default:
			return this.m_targetPos0;
		}
	}

	public void SetTargetPos(int index, Vector4 pos)
	{
		switch (index)
		{
		case 0:
			this.m_targetPos0 = pos;
			return;
		case 1:
			this.m_targetPos1 = pos;
			return;
		case 2:
			this.m_targetPos2 = pos;
			return;
		case 3:
			this.m_targetPos3 = pos;
			return;
		default:
			return;
		}
	}

	private uint GetTargetFrame(VehicleInfo info, ushort vehicleID)
	{
		if (info.m_vehicleType != VehicleInfo.VehicleType.Bicycle)
		{
			ushort firstVehicle = this.GetFirstVehicle(vehicleID);
			uint num = (uint)(((int)firstVehicle << 4) / 16384);
			return Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num2 = 0;
		if (this.m_citizenUnits != 0u)
		{
			uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)this.m_citizenUnits)].m_citizen0;
			if (citizen != 0u)
			{
				num2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
			}
		}
		if (num2 != 0)
		{
			uint num3 = (uint)(((int)num2 << 4) / 65536);
			return Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num3;
		}
		uint num4 = (uint)(((int)vehicleID << 4) / 16384);
		return Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num4;
	}

	public bool RayCast(ushort vehicleID, Segment3 ray, Vehicle.Flags ignoreFlags, out float t)
	{
		t = 2f;
		if ((this.m_flags & ignoreFlags) != (Vehicle.Flags)0)
		{
			return false;
		}
		VehicleInfo info = this.Info;
		uint targetFrame = this.GetTargetFrame(info, vehicleID);
		Vehicle.Frame frameData = this.GetFrameData(targetFrame - 32u);
		Vehicle.Frame frameData2 = this.GetFrameData(targetFrame - 16u);
		float num = ((targetFrame & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 vector = bezier.Position(num);
		Vector3 size = info.m_generatedInfo.m_size;
		Vector3 vector2 = vector;
		vector2.y += size.y * 0.5f - info.m_generatedInfo.m_negativeHeight * 0.5f;
		size.y += info.m_generatedInfo.m_negativeHeight;
		if (ray.DistanceSqr(vector2) >= size.get_sqrMagnitude() * 0.25f)
		{
			return false;
		}
		ray.a -= vector;
		ray.b -= vector;
		Quaternion quaternion = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, t);
		Quaternion quaternion2 = Quaternion.Inverse(quaternion);
		ray.a = quaternion2 * ray.a;
		ray.b = quaternion2 * ray.b;
		if (info.m_lodMeshData != null)
		{
			Vector3[] vertices = info.m_lodMeshData.m_vertices;
			int[] triangles = info.m_lodMeshData.m_triangles;
			if (triangles != null)
			{
				int num2 = triangles.Length;
				for (int i = 0; i < num2; i += 3)
				{
					float num3;
					float num4;
					float num5;
					if (Triangle3.Intersect(vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]], ray.a, ray.b, ref num3, ref num4, ref num5) && num5 < t)
					{
						t = num5;
					}
				}
			}
		}
		return t != 2f;
	}

	public ushort GetFirstVehicle(ushort vehicleID)
	{
		if (this.m_leadingVehicle == 0)
		{
			return vehicleID;
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort leadingVehicle = this.m_leadingVehicle;
		int num = 0;
		while (leadingVehicle != 0)
		{
			vehicleID = leadingVehicle;
			leadingVehicle = instance.m_vehicles.m_buffer[(int)vehicleID].m_leadingVehicle;
			if (++num > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return vehicleID;
	}

	public ushort GetLastVehicle(ushort vehicleID)
	{
		if (this.m_trailingVehicle == 0)
		{
			return vehicleID;
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort trailingVehicle = this.m_trailingVehicle;
		int num = 0;
		while (trailingVehicle != 0)
		{
			vehicleID = trailingVehicle;
			trailingVehicle = instance.m_vehicles.m_buffer[(int)vehicleID].m_trailingVehicle;
			if (++num > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return vehicleID;
	}

	public float CalculateTotalLength(ushort vehicleID)
	{
		int num;
		return this.CalculateTotalLength(vehicleID, out num);
	}

	public float CalculateTotalLength(ushort vehicleID, out int totalNoise)
	{
		VehicleInfo info = this.Info;
		float num = info.m_generatedInfo.m_size.z;
		totalNoise = info.m_vehicleAI.GetNoiseLevel();
		if ((this.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
		{
			if (info.m_trailers != null)
			{
				bool flag = info.m_vehicleAI.VerticalTrailers();
				float num2 = ((this.m_flags & Vehicle.Flags.Inverted) == (Vehicle.Flags)0) ? info.m_attachOffsetBack : info.m_attachOffsetFront;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				for (int i = 0; i < info.m_trailers.Length; i++)
				{
					if (randomizer.Int32(100u) < info.m_trailers[i].m_probability)
					{
						num -= num2;
						VehicleInfo info2 = info.m_trailers[i].m_info;
						bool flag2 = randomizer.Int32(100u) < info.m_trailers[i].m_invertProbability;
						num += ((!flag) ? info2.m_generatedInfo.m_size.z : info2.m_generatedInfo.m_size.y);
						num -= ((!flag2) ? info2.m_attachOffsetFront : info2.m_attachOffsetBack);
						num2 = ((!flag2) ? info2.m_attachOffsetBack : info2.m_attachOffsetFront);
						totalNoise += info2.m_vehicleAI.GetNoiseLevel();
					}
				}
			}
		}
		else if (this.m_leadingVehicle == 0 && this.m_trailingVehicle != 0)
		{
			bool flag3 = info.m_vehicleAI.VerticalTrailers();
			num -= (((this.m_flags & Vehicle.Flags.Inverted) == (Vehicle.Flags)0) ? info.m_attachOffsetBack : info.m_attachOffsetFront);
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num3 = this.m_trailingVehicle;
			int num4 = 0;
			while (num3 != 0)
			{
				ushort trailingVehicle = instance.m_vehicles.m_buffer[(int)num3].m_trailingVehicle;
				VehicleInfo info3 = instance.m_vehicles.m_buffer[(int)num3].Info;
				num += ((!flag3) ? info3.m_generatedInfo.m_size.z : info3.m_generatedInfo.m_size.y);
				if ((instance.m_vehicles.m_buffer[(int)num3].m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
				{
					num -= info3.m_attachOffsetBack;
					if (trailingVehicle != 0)
					{
						num -= info3.m_attachOffsetFront;
					}
				}
				else
				{
					num -= info3.m_attachOffsetFront;
					if (trailingVehicle != 0)
					{
						num -= info3.m_attachOffsetBack;
					}
				}
				totalNoise += info3.m_vehicleAI.GetNoiseLevel();
				num3 = trailingVehicle;
				if (++num4 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return num;
	}

	public void Spawn(ushort vehicleID)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		VehicleInfo info = this.Info;
		if ((this.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
		{
			this.m_flags |= Vehicle.Flags.Spawned;
			instance.AddToGrid(vehicleID, ref this, info.m_isLargeVehicle);
		}
		if (this.m_leadingVehicle == 0 && this.m_trailingVehicle == 0 && info.m_trailers != null)
		{
			bool flag = info.m_vehicleAI.VerticalTrailers();
			ushort num = vehicleID;
			bool flag2 = (instance.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0;
			Vehicle.Frame lastFrameData = this.GetLastFrameData();
			float num2 = (!flag) ? (info.m_generatedInfo.m_size.z * 0.5f) : 0f;
			num2 -= (((this.m_flags & Vehicle.Flags.Inverted) == (Vehicle.Flags)0) ? info.m_attachOffsetBack : info.m_attachOffsetFront);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			for (int i = 0; i < info.m_trailers.Length; i++)
			{
				if (randomizer.Int32(100u) < info.m_trailers[i].m_probability)
				{
					VehicleInfo info2 = info.m_trailers[i].m_info;
					bool flag3 = randomizer.Int32(100u) < info.m_trailers[i].m_invertProbability;
					num2 += ((!flag) ? (info2.m_generatedInfo.m_size.z * 0.5f) : info2.m_generatedInfo.m_size.y);
					num2 -= ((!flag3) ? info2.m_attachOffsetFront : info2.m_attachOffsetBack);
					Vector3 position = lastFrameData.m_position - lastFrameData.m_rotation * new Vector3(0f, (!flag) ? 0f : num2, (!flag) ? num2 : 0f);
					ushort num3;
					if (instance.CreateVehicle(out num3, ref Singleton<SimulationManager>.get_instance().m_randomizer, info2, position, (TransferManager.TransferReason)this.m_transferType, false, false))
					{
						instance.m_vehicles.m_buffer[(int)num].m_trailingVehicle = num3;
						instance.m_vehicles.m_buffer[(int)num3].m_leadingVehicle = num;
						if (flag3)
						{
							Vehicle[] expr_24A_cp_0 = instance.m_vehicles.m_buffer;
							ushort expr_24A_cp_1 = num3;
							expr_24A_cp_0[(int)expr_24A_cp_1].m_flags = (expr_24A_cp_0[(int)expr_24A_cp_1].m_flags | Vehicle.Flags.Inverted);
						}
						if (flag2)
						{
							Vehicle[] expr_270_cp_0 = instance.m_vehicles.m_buffer;
							ushort expr_270_cp_1 = num3;
							expr_270_cp_0[(int)expr_270_cp_1].m_flags = (expr_270_cp_0[(int)expr_270_cp_1].m_flags | Vehicle.Flags.Reversed);
						}
						instance.m_vehicles.m_buffer[(int)num3].m_frame0.m_rotation = lastFrameData.m_rotation;
						instance.m_vehicles.m_buffer[(int)num3].m_frame1.m_rotation = lastFrameData.m_rotation;
						instance.m_vehicles.m_buffer[(int)num3].m_frame2.m_rotation = lastFrameData.m_rotation;
						instance.m_vehicles.m_buffer[(int)num3].m_frame3.m_rotation = lastFrameData.m_rotation;
						info2.m_vehicleAI.FrameDataUpdated(num3, ref instance.m_vehicles.m_buffer[(int)num3], ref instance.m_vehicles.m_buffer[(int)num3].m_frame0);
						instance.m_vehicles.m_buffer[(int)num3].Spawn(num3);
						num = num3;
					}
					num2 += ((!flag) ? (info2.m_generatedInfo.m_size.z * 0.5f) : 0f);
					num2 -= ((!flag3) ? info2.m_attachOffsetBack : info2.m_attachOffsetFront);
				}
			}
		}
	}

	public void Unspawn(ushort vehicleID)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		if (this.m_leadingVehicle == 0 && this.m_trailingVehicle != 0)
		{
			ushort num = this.m_trailingVehicle;
			this.m_trailingVehicle = 0;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				instance.m_vehicles.m_buffer[(int)num].m_leadingVehicle = 0;
				instance.m_vehicles.m_buffer[(int)num].m_trailingVehicle = 0;
				instance.ReleaseVehicle(num);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if ((this.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			VehicleInfo info = this.Info;
			if (info != null)
			{
				instance.RemoveFromGrid(vehicleID, ref this, info.m_isLargeVehicle);
			}
			this.m_flags &= ~Vehicle.Flags.Spawned;
		}
	}

	public static bool GetClosestFreeTrailer(ushort vehicleID, Vector3 position, out ushort trailerID, out uint unitID)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		float num = 1E+10f;
		trailerID = 0;
		unitID = 0u;
		int num2 = 0;
		while (vehicleID != 0)
		{
			float num3 = Vector3.SqrMagnitude(position - instance.m_vehicles.m_buffer[(int)vehicleID].GetLastFrameData().m_position);
			if (num3 < num)
			{
				uint notFullCitizenUnit = instance.m_vehicles.m_buffer[(int)vehicleID].GetNotFullCitizenUnit(CitizenUnit.Flags.Vehicle);
				if (notFullCitizenUnit != 0u)
				{
					num = num3;
					trailerID = vehicleID;
					unitID = notFullCitizenUnit;
				}
			}
			vehicleID = instance.m_vehicles.m_buffer[(int)vehicleID].m_trailingVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return trailerID != 0;
	}

	public uint GetNotFullCitizenUnit(CitizenUnit.Flags flag)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = this.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0 && !instance.m_units.m_buffer[(int)((UIntPtr)num)].Full())
			{
				return num;
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0u;
	}

	public Vector3 GetClosestDoorPosition(Vector3 position, VehicleInfo.DoorType type)
	{
		Vehicle.Frame lastFrameData = this.GetLastFrameData();
		Vector3 result = lastFrameData.m_position;
		VehicleInfo info = this.Info;
		float num = 1000000f;
		if (info.m_doors != null)
		{
			for (int i = 0; i < info.m_doors.Length; i++)
			{
				if ((info.m_doors[i].m_type & type) != (VehicleInfo.DoorType)0)
				{
					Vector3 vector = info.m_doors[i].m_location;
					if ((this.m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
					{
						vector.x = -vector.x;
						vector.z = -vector.z;
					}
					vector = lastFrameData.m_rotation * vector + lastFrameData.m_position;
					float num2 = Vector3.SqrMagnitude(vector - position);
					if (num2 < num)
					{
						num = num2;
						result = vector;
					}
				}
			}
		}
		return result;
	}

	public Vector3 GetRandomDoorPosition(ref Randomizer r, VehicleInfo.DoorType type)
	{
		Vehicle.Frame lastFrameData = this.GetLastFrameData();
		VehicleInfo info = this.Info;
		if (info.m_doors != null)
		{
			int num = 0;
			for (int i = 0; i < info.m_doors.Length; i++)
			{
				if ((info.m_doors[i].m_type & type) != (VehicleInfo.DoorType)0)
				{
					num++;
				}
			}
			if (num != 0)
			{
				num = r.Int32((uint)num);
				for (int j = 0; j < info.m_doors.Length; j++)
				{
					if ((info.m_doors[j].m_type & type) != (VehicleInfo.DoorType)0 && num-- == 0)
					{
						Vector3 location = info.m_doors[j].m_location;
						if ((this.m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
						{
							location.x = -location.x;
							location.z = -location.z;
						}
						return lastFrameData.m_rotation * location + lastFrameData.m_position;
					}
				}
			}
		}
		return lastFrameData.m_position;
	}

	public const Vehicle.Flags AllFlags = Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding | Vehicle.Flags.LeftHandDrive;

	public Vehicle.Frame m_frame0;

	public Vehicle.Frame m_frame1;

	public Vehicle.Frame m_frame2;

	public Vehicle.Frame m_frame3;

	public Segment3 m_segment;

	public Vector4 m_targetPos0;

	public Vector4 m_targetPos1;

	public Vector4 m_targetPos2;

	public Vector4 m_targetPos3;

	public Vehicle.Flags m_flags;

	public Vehicle.Flags2 m_flags2;

	public uint m_citizenUnits;

	public uint m_path;

	public ushort m_sourceBuilding;

	public ushort m_targetBuilding;

	public ushort m_nextGridVehicle;

	public ushort m_nextOwnVehicle;

	public ushort m_nextGuestVehicle;

	public ushort m_nextLineVehicle;

	public ushort m_transferSize;

	public ushort m_transportLine;

	public ushort m_leadingVehicle;

	public ushort m_trailingVehicle;

	public ushort m_cargoParent;

	public ushort m_firstCargo;

	public ushort m_nextCargo;

	public ushort m_infoIndex;

	public ushort m_waterSource;

	public byte m_transferType;

	public byte m_waitCounter;

	public byte m_blockCounter;

	public byte m_lastFrame;

	public byte m_pathPositionIndex;

	public byte m_lastPathOffset;

	public byte m_gateIndex;

	[Flags]
	public enum Flags
	{
		Created = 1,
		Deleted = 2,
		Spawned = 4,
		Inverted = 8,
		TransferToTarget = 16,
		TransferToSource = 32,
		Emergency1 = 64,
		Emergency2 = 128,
		WaitingPath = 256,
		Stopped = 512,
		Leaving = 1024,
		Arriving = 2048,
		Reversed = 4096,
		TakingOff = 8192,
		Flying = 16384,
		Landing = 32768,
		WaitingSpace = 65536,
		WaitingCargo = 131072,
		GoingBack = 262144,
		WaitingTarget = 524288,
		Importing = 1048576,
		Exporting = 2097152,
		Parking = 4194304,
		CustomName = 8388608,
		OnGravel = 16777216,
		WaitingLoading = 33554432,
		Congestion = 67108864,
		DummyTraffic = 134217728,
		Underground = 268435456,
		Transition = 536870912,
		InsideBuilding = 1073741824,
		LeftHandDrive = -2147483648
	}

	[Flags]
	public enum Flags2
	{
		Floating = 1,
		Blown = 2,
		Yielding = 4
	}

	public struct Frame
	{
		public Frame(Vector3 position, Quaternion rotation)
		{
			this.m_rotation = rotation;
			this.m_lightIntensity = Vector4.get_zero();
			this.m_swayVelocity = Vector3.get_zero();
			this.m_swayPosition = Vector3.get_zero();
			this.m_velocity = Vector3.get_zero();
			this.m_position = position;
			this.m_angleVelocity = 0f;
			this.m_steerAngle = 0f;
			this.m_travelDistance = 0f;
			this.m_blinkState = 0f;
			this.m_underground = false;
			this.m_transition = false;
			this.m_insideBuilding = false;
		}

		public Quaternion m_rotation;

		public Vector4 m_lightIntensity;

		public Vector3 m_swayVelocity;

		public Vector3 m_swayPosition;

		public Vector3 m_velocity;

		public Vector3 m_position;

		public float m_angleVelocity;

		public float m_steerAngle;

		public float m_travelDistance;

		public float m_blinkState;

		public bool m_underground;

		public bool m_transition;

		public bool m_insideBuilding;
	}
}
