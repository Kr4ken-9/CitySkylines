﻿using System;
using System.Reflection;
using System.Threading;
using ColossalFramework.UI;

public class REPropertySet : UICustomControl
{
	public event REPropertySet.PropertyChangedHandler EventPropertyChanged
	{
		add
		{
			REPropertySet.PropertyChangedHandler propertyChangedHandler = this.EventPropertyChanged;
			REPropertySet.PropertyChangedHandler propertyChangedHandler2;
			do
			{
				propertyChangedHandler2 = propertyChangedHandler;
				propertyChangedHandler = Interlocked.CompareExchange<REPropertySet.PropertyChangedHandler>(ref this.EventPropertyChanged, (REPropertySet.PropertyChangedHandler)Delegate.Combine(propertyChangedHandler2, value), propertyChangedHandler);
			}
			while (propertyChangedHandler != propertyChangedHandler2);
		}
		remove
		{
			REPropertySet.PropertyChangedHandler propertyChangedHandler = this.EventPropertyChanged;
			REPropertySet.PropertyChangedHandler propertyChangedHandler2;
			do
			{
				propertyChangedHandler2 = propertyChangedHandler;
				propertyChangedHandler = Interlocked.CompareExchange<REPropertySet.PropertyChangedHandler>(ref this.EventPropertyChanged, (REPropertySet.PropertyChangedHandler)Delegate.Remove(propertyChangedHandler2, value), propertyChangedHandler);
			}
			while (propertyChangedHandler != propertyChangedHandler2);
		}
	}

	public void SetTarget(object target, FieldInfo targetField)
	{
		if (this.Validate(target, targetField))
		{
			this.m_Target = target;
			this.m_TargetField = targetField;
			string labelText = string.Empty;
			CustomizablePropertyAttribute[] array = (CustomizablePropertyAttribute[])targetField.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true);
			labelText = ((array.Length <= 0) ? "Error: property not supported" : array[0].name);
			this.Initialize(target, targetField, labelText);
		}
	}

	protected virtual void Initialize(object target, FieldInfo targetField, string labelText)
	{
	}

	protected virtual bool Validate(object target, FieldInfo targetField)
	{
		return false;
	}

	protected void SetValue(object value)
	{
		if (this.m_Target != null && this.m_TargetField != null && this.m_TargetField.GetValue(this.m_Target) != value)
		{
			this.m_TargetField.SetValue(this.m_Target, value);
			if (this.EventPropertyChanged != null)
			{
				this.EventPropertyChanged();
			}
		}
	}

	protected object m_Target;

	protected FieldInfo m_TargetField;

	public delegate void PropertyChangedHandler();
}
