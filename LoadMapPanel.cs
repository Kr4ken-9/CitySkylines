﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class LoadMapPanel : LoadSavePanelBase<MapMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_SaveList = base.Find<UIListBox>("SaveList");
		this.m_SaveList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnListingSelectionChanged));
		this.m_Theme = base.Find<UILabel>("MapTheme");
		this.m_MapName = base.Find<UILabel>("MapName");
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_LoadButton = base.Find<UIButton>("Load");
		this.m_ThemeOverridePanel = base.Find<UIPanel>("OverridePanel");
		this.m_Themes = new Dictionary<string, List<MapThemeMetaData>>();
		this.m_ThemeOverrideDropDown = base.Find<UIDropDown>("OverrideMapTheme");
	}

	private void OnListingSelectionChanged(UIComponent comp, int sel)
	{
		if (sel > -1)
		{
			MapMetaData listingMetaData = base.GetListingMetaData(sel);
			this.m_MapName.set_text(listingMetaData.mapName);
			this.m_Theme.set_text(LoadSavePanelBase<MapMetaData>.GetThemeString(null, listingMetaData.environment, null));
			if (!string.IsNullOrEmpty(this.m_Theme.get_text()))
			{
				this.m_Theme.Show();
				this.m_SelectedEnvironment = listingMetaData.environment;
				if (this.m_ThemeOverridePanel != null)
				{
					base.RefreshOverrideThemes();
					base.SelectThemeOverride(listingMetaData.mapThemeRef);
					this.m_ThemeOverridePanel.Show();
				}
			}
			else
			{
				this.m_Theme.Hide();
				if (this.m_ThemeOverridePanel != null)
				{
					this.m_ThemeOverridePanel.Hide();
				}
			}
			if (this.m_SnapShot.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShot.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShot.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShot.get_texture() != null)
				{
					this.m_SnapShot.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShot.set_texture(null);
			}
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			ToolsMenu.SetFocus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveList.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				if (this.m_newScenarioPanel == null)
				{
					this.OnClosed();
				}
				else
				{
					this.m_newScenarioPanel.OnClosed();
				}
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnLoad();
			}
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_SaveList && sel != -1)
		{
			if (this.m_newScenarioPanel == null)
			{
				if (this.m_SaveList.get_selectedIndex() != sel)
				{
					this.m_SaveList.set_selectedIndex(sel);
				}
				this.OnLoad();
			}
			else
			{
				this.m_newScenarioPanel.OnLoadMap();
			}
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("LoadMapPanel.Refresh()"))
		{
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
			bool flag3 = SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC);
			bool flag4 = SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.MapMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						MapMetaData mapMetaData = current.Instantiate<MapMetaData>();
						if (mapMetaData.environment != "Winter" || flag)
						{
							bool flag5 = mapMetaData.IsBuiltinMap(current.get_package().get_packagePath());
							bool flag6 = NewGamePanel.IsNDScenarioMap(mapMetaData.mapName);
							bool flag7 = NewGamePanel.IsMTScenarioMap(mapMetaData.mapName);
							bool flag8 = NewGamePanel.IsGCScenarioMap(mapMetaData.mapName);
							if ((!flag5 || this.m_showBuiltinMaps) && (!flag5 || flag2 || !flag6) && (!flag5 || flag3 || !flag7) && (!flag5 || flag4 || !flag8))
							{
								string text = string.Empty;
								if (this.m_newScenarioPanel != null)
								{
									text = mapMetaData.mapName;
									if (Locale.Exists("MAPNAME", text))
									{
										text = Locale.Get("MAPNAME", text);
									}
								}
								else
								{
									text = current.get_name();
								}
								if (flag6)
								{
									text = "<sprite NaturalDisastersIcon> " + text;
								}
								if (flag7)
								{
									text = "<sprite MassTransitIcon> " + text;
								}
								if (flag8)
								{
									text = "<sprite GreenCitiesIcon> " + text;
								}
								base.AddToListing(text, mapMetaData.timeStamp, current, mapMetaData, true);
							}
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			foreach (Package.Asset current2 in SaveHelper.GetMapsOnDisk())
			{
				MapMetaData mmd = new MapMetaData
				{
					mapName = current2.get_name(),
					assetRef = current2
				};
				base.AddToListing("<color red>" + current2.get_name() + "</color>", current2, mmd);
			}
			this.m_SaveList.set_items(base.GetListingItems());
			if (base.GetListingCount() > 0)
			{
				int num = base.FindIndexOf(LoadMapPanel.m_LastSaveName);
				this.m_SaveList.set_selectedIndex((num == -1) ? 0 : num);
				this.m_LoadButton.set_isEnabled(true);
			}
			else
			{
				this.m_LoadButton.set_isEnabled(false);
			}
			base.RebuildThemeDictionary();
		}
	}

	public void OnLoad()
	{
		MapMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
		if (this.m_ThemeOverrideDropDown != null && this.m_Themes != null)
		{
			if (this.m_ThemeOverrideDropDown.get_items().Length > 0 && this.m_ThemeOverrideDropDown.get_selectedIndex() > 0 && !string.IsNullOrEmpty(this.m_SelectedEnvironment) && this.m_Themes.ContainsKey(this.m_SelectedEnvironment) && this.m_Themes[this.m_SelectedEnvironment].Count >= this.m_ThemeOverrideDropDown.get_selectedIndex())
			{
				listingMetaData.mapThemeRef = this.m_Themes[this.m_SelectedEnvironment][this.m_ThemeOverrideDropDown.get_selectedIndex() - 1].mapThemeRef;
			}
			else if (this.m_ThemeOverrideDropDown.get_selectedIndex() == 0)
			{
				listingMetaData.mapThemeRef = null;
			}
		}
		if (base.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.LoadRoutine();
				}
			});
		}
		else
		{
			this.LoadRoutine();
		}
	}

	private void LoadRoutine()
	{
		base.CloseEverything();
		LoadMapPanel.m_LastSaveName = base.GetListingName(this.m_SaveList.get_selectedIndex());
		SaveMapPanel.lastLoadedName = LoadMapPanel.m_LastSaveName;
		MapMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
		base.PrintModsInfo(listingMetaData.mods);
		SaveMapPanel.lastLoadedAsset = listingMetaData.mapName;
		SaveMapPanel.lastPublished = listingMetaData.isPublished;
		string listingPackageName = base.GetListingPackageName(this.m_SaveList.get_selectedIndex());
		PublishedFileId invalid = PublishedFileId.invalid;
		ulong num;
		if (ulong.TryParse(listingPackageName, out num))
		{
			invalid..ctor(num);
		}
		SimulationMetaData simulationMetaData = new SimulationMetaData();
		simulationMetaData.m_WorkshopPublishedFileId = invalid;
		simulationMetaData.m_updateMode = SimulationManager.UpdateMode.LoadMap;
		if (!string.IsNullOrEmpty(this.m_forceEnvironment))
		{
			simulationMetaData.m_environment = this.m_forceEnvironment;
		}
		if (listingMetaData.mapThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(listingMetaData.mapThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		Singleton<LoadingManager>.get_instance().LoadLevel(base.GetListingData(this.m_SaveList.get_selectedIndex()), "MapEditor", "InMapEditor", simulationMetaData);
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	public Package.Asset selectedAsset
	{
		get
		{
			if (this.m_SaveList.get_selectedIndex() != -1)
			{
				return base.GetListingData(this.m_SaveList.get_selectedIndex());
			}
			return null;
		}
	}

	public string selectedCityName
	{
		get
		{
			if (this.m_SaveList.get_selectedIndex() != -1)
			{
				return base.GetListingMetaData(this.m_SaveList.get_selectedIndex()).mapName + " " + Locale.Get("SCENARIONAME_CITY");
			}
			return null;
		}
	}

	public string selectedOverrideThemeRef
	{
		get
		{
			if (this.m_ThemeOverrideDropDown != null && this.m_Themes != null && this.m_ThemeOverrideDropDown.get_items().Length > 0 && this.m_ThemeOverrideDropDown.get_selectedIndex() > 0 && !string.IsNullOrEmpty(this.m_SelectedEnvironment) && this.m_Themes.ContainsKey(this.m_SelectedEnvironment) && this.m_Themes[this.m_SelectedEnvironment].Count >= this.m_ThemeOverrideDropDown.get_selectedIndex())
			{
				return this.m_Themes[this.m_SelectedEnvironment][this.m_ThemeOverrideDropDown.get_selectedIndex() - 1].mapThemeRef;
			}
			return null;
		}
	}

	public string m_forceEnvironment;

	private static string m_LastSaveName;

	private UILabel m_MapName;

	private UITextureSprite m_SnapShot;

	private UIListBox m_SaveList;

	private UIButton m_LoadButton;

	private UILabel m_Theme;

	public NewScenarioPanel m_newScenarioPanel;

	public bool m_showBuiltinMaps;
}
