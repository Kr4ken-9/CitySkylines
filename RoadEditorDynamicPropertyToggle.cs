﻿using System;
using System.Reflection;
using System.Threading;
using ColossalFramework.UI;
using UnityEngine;

internal class RoadEditorDynamicPropertyToggle : UICustomControl
{
	public event RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle EventToggle
	{
		add
		{
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle = this.EventToggle;
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle2;
			do
			{
				dynamicPropertyToggleHandle2 = dynamicPropertyToggleHandle;
				dynamicPropertyToggleHandle = Interlocked.CompareExchange<RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle>(ref this.EventToggle, (RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle)Delegate.Combine(dynamicPropertyToggleHandle2, value), dynamicPropertyToggleHandle);
			}
			while (dynamicPropertyToggleHandle != dynamicPropertyToggleHandle2);
		}
		remove
		{
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle = this.EventToggle;
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle2;
			do
			{
				dynamicPropertyToggleHandle2 = dynamicPropertyToggleHandle;
				dynamicPropertyToggleHandle = Interlocked.CompareExchange<RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle>(ref this.EventToggle, (RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle)Delegate.Remove(dynamicPropertyToggleHandle2, value), dynamicPropertyToggleHandle);
			}
			while (dynamicPropertyToggleHandle != dynamicPropertyToggleHandle2);
		}
	}

	public event RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle EventDelete
	{
		add
		{
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle = this.EventDelete;
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle2;
			do
			{
				dynamicPropertyToggleHandle2 = dynamicPropertyToggleHandle;
				dynamicPropertyToggleHandle = Interlocked.CompareExchange<RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle>(ref this.EventDelete, (RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle)Delegate.Combine(dynamicPropertyToggleHandle2, value), dynamicPropertyToggleHandle);
			}
			while (dynamicPropertyToggleHandle != dynamicPropertyToggleHandle2);
		}
		remove
		{
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle = this.EventDelete;
			RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle dynamicPropertyToggleHandle2;
			do
			{
				dynamicPropertyToggleHandle2 = dynamicPropertyToggleHandle;
				dynamicPropertyToggleHandle = Interlocked.CompareExchange<RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle>(ref this.EventDelete, (RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle)Delegate.Remove(dynamicPropertyToggleHandle2, value), dynamicPropertyToggleHandle);
			}
			while (dynamicPropertyToggleHandle != dynamicPropertyToggleHandle2);
		}
	}

	private void Awake()
	{
		this.m_DefaultColor = this.m_SelectButton.get_color();
	}

	public void ToggleColor(bool selected)
	{
		Color color = (!selected) ? this.m_DefaultColor : this.m_SelectedColor;
		this.m_SelectButton.set_color(color);
		this.m_SelectButton.set_focusedColor(color);
		this.m_SelectButton.set_hoveredColor(color);
		this.m_SelectButton.set_pressedColor(color);
		this.m_SelectButton.set_disabledColor(color);
	}

	public object targetObject
	{
		get
		{
			return this.m_TargetObject;
		}
		set
		{
			this.m_TargetObject = value;
		}
	}

	public FieldInfo field
	{
		get
		{
			return this.m_Field;
		}
		set
		{
			this.m_Field = value;
		}
	}

	public object targetElement
	{
		get
		{
			return this.m_TargetElement;
		}
		set
		{
			this.m_TargetElement = value;
			this.Refresh();
		}
	}

	public void Refresh()
	{
		if (this.m_TargetElement != null)
		{
			if (this.m_TargetElement is NetInfo.Lane)
			{
				NetInfo.Lane lane = this.m_TargetElement as NetInfo.Lane;
				this.m_SelectButton.set_text(StringUtils.SafeFormat("{0}, {1}, {2}", new object[]
				{
					lane.m_laneType.ToString(),
					lane.m_vehicleType.ToString(),
					lane.m_position
				}));
			}
			else if (this.m_TargetElement is NetLaneProps.Prop)
			{
				NetLaneProps.Prop prop = this.m_TargetElement as NetLaneProps.Prop;
				if (prop.m_prop != null)
				{
					this.m_SelectButton.set_text(prop.m_prop.get_name());
				}
				else if (prop.m_tree != null)
				{
					this.m_SelectButton.set_text(prop.m_tree.get_name());
				}
				else
				{
					this.m_SelectButton.set_text("New prop");
				}
			}
			else
			{
				string meshName = AssetEditorRoadUtils.GetMeshName(this.m_TargetElement);
				if (!string.IsNullOrEmpty(meshName))
				{
					this.m_SelectButton.set_text(meshName);
				}
				else
				{
					this.m_SelectButton.set_text("Unsupported format");
				}
			}
		}
		else
		{
			this.m_SelectButton.set_text("Null");
		}
	}

	private void OnEnable()
	{
		this.m_SelectButton.add_eventClick(new MouseEventHandler(this.OnSelectButtonClick));
		this.m_DeleteButton.add_eventClick(new MouseEventHandler(this.OnDeleteButtonClick));
	}

	private void OnDisable()
	{
		this.m_SelectButton.remove_eventClick(new MouseEventHandler(this.OnSelectButtonClick));
		this.m_DeleteButton.remove_eventClick(new MouseEventHandler(this.OnDeleteButtonClick));
	}

	private void OnSelectButtonClick(UIComponent c, UIMouseEventParameter e)
	{
		if (this.EventToggle != null)
		{
			this.EventToggle(this);
		}
	}

	private void OnDeleteButtonClick(UIComponent c, UIMouseEventParameter e)
	{
		if (this.EventDelete != null)
		{
			this.EventDelete(this);
		}
	}

	public UIButton m_SelectButton;

	public UIButton m_DeleteButton;

	private object m_TargetObject;

	private FieldInfo m_Field;

	private object m_TargetElement;

	public Color m_SelectedColor;

	private Color m_DefaultColor;

	public delegate void DynamicPropertyToggleHandle(RoadEditorDynamicPropertyToggle toggle);
}
