﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class GarbageTruckAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Garbage)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.TransferToSource) == (Vehicle.Flags)0)
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_GARBAGE_RETURN");
				}
				if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_GARBAGE_UNLOAD");
				}
				if (data.m_targetBuilding != 0)
				{
					target = InstanceID.Empty;
					target.Building = data.m_targetBuilding;
					return Locale.Get("VEHICLE_STATUS_GARBAGE_TRANSFER");
				}
			}
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CONFUSED");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_GARBAGE_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_GARBAGE_WAIT");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_GARBAGE_COLLECT");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0 && data.m_transferSize != 0)
		{
			int transferSize = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			if (info != null)
			{
				info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
			}
		}
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				int num = Mathf.Min(0, (int)data.m_transferSize - this.m_cargoCapacity);
				info.m_buildingAI.ModifyMaterialBuffer(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
				num = Mathf.Max(0, -num);
				data.m_transferSize += (ushort)num;
			}
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
			if ((instance.m_buildings.m_buffer[(int)sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Importing;
				}
				else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Exporting;
				}
			}
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding == data.m_targetBuilding)
		{
			if (data.m_path == 0u)
			{
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			else
			{
				this.TrySpawn(vehicleID, ref data);
			}
		}
		else
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if (targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
				if ((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Exporting;
					}
					else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Importing;
					}
				}
			}
			else
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					if (data.m_transferSize > 0)
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = 7;
						offer.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer.Position = data.GetLastFramePosition();
						}
						offer.Amount = 1;
						offer.Active = true;
						Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
				if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					if ((int)data.m_transferSize < this.m_cargoCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
					{
						TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
						offer2.Priority = 7;
						offer2.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer2.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer2.Position = data.GetLastFramePosition();
						}
						offer2.Amount = 1;
						offer2.Active = true;
						Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer2);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
			}
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
			else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		int num = 0;
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			num = (int)data.m_transferSize;
		}
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			num = Mathf.Min(0, (int)data.m_transferSize - this.m_cargoCapacity);
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
		info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
		}
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			data.m_transferSize += (ushort)Mathf.Max(0, -num);
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		int num = 0;
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			num = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			if ((int)vehicleData.m_transferSize < this.m_cargoCapacity)
			{
				this.TryCollectGarbage(vehicleID, ref vehicleData, ref frameData);
			}
			if ((int)vehicleData.m_transferSize >= this.m_cargoCapacity && (vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0)
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & (Vehicle.Flags.TransferToSource | Vehicle.Flags.GoingBack)) == Vehicle.Flags.TransferToSource && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_productionRate == 0 || (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None) && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && leaderData.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 targetPos;
			info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
			return;
		}
	}

	private void TryCollectGarbage(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) == Vehicle.Flags.Underground)
		{
			return;
		}
		Vector3 position = frameData.m_position;
		float num = position.x - 32f;
		float num2 = position.z - 32f;
		float num3 = position.x + 32f;
		float num4 = position.z + 32f;
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					this.TryCollectGarbage(vehicleID, ref vehicleData, ref frameData, num9, ref instance.m_buildings.m_buffer[(int)num9]);
					num9 = instance.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private void TryCollectGarbage(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort buildingID, ref Building building)
	{
		Vector3 vector = building.CalculateSidewalkPosition();
		if (Vector3.SqrMagnitude(vector - frameData.m_position) < 1024f)
		{
			int num = Mathf.Min(0, (int)vehicleData.m_transferSize - this.m_cargoCapacity);
			if (num == 0)
			{
				return;
			}
			BuildingInfo info = building.Info;
			info.m_buildingAI.ModifyMaterialBuffer(buildingID, ref building, (TransferManager.TransferReason)vehicleData.m_transferType, ref num);
			if (num != 0)
			{
				vehicleData.m_transferSize += (ushort)Mathf.Max(0, -num);
			}
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 endPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public override int GetNoiseLevel()
	{
		return (this.m_info.m_class.m_level != ItemClass.Level.Level2) ? 9 : 4;
	}

	[CustomizableProperty("Cargo capacity")]
	public int m_cargoCapacity = 1;
}
