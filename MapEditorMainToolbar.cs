﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class MapEditorMainToolbar : MainToolbar
{
	public ImportHeightmapPanel importHeightmapsPanel
	{
		get
		{
			if (this.m_ImportHeightmapsPanel == null)
			{
				this.m_ImportHeightmapsPanel = UIView.Find<UIPanel>("ImportHeightmapPanel").GetComponent<ImportHeightmapPanel>();
			}
			return this.m_ImportHeightmapsPanel;
		}
	}

	public ExportHeightmapPanel exportHeightmapsPanel
	{
		get
		{
			if (this.m_ExportHeightmapsPanel == null)
			{
				this.m_ExportHeightmapsPanel = UIView.Find<UIPanel>("ExportHeightmapPanel").GetComponent<ExportHeightmapPanel>();
			}
			return this.m_ExportHeightmapsPanel;
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHideHeightmapsPanels));
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHideHeightmapsPanels));
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		UITabstrip strip = base.get_component().Find<UITabstrip>("MainToolstrip");
		base.SpawnSubEntry(strip, "Terrain", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		UIButton uIButton = base.SpawnButtonEntry(strip, "ImportHeightmap", "MAPEDITOR_TOOL", "ToolbarIcon", true);
		this.m_ImportHeightmapsIndex = uIButton.get_zOrder();
		UIButton uIButton2 = base.SpawnButtonEntry(strip, "ExportHeightmap", "MAPEDITOR_TOOL", "ToolbarIcon", true);
		this.m_ExportHeightmapsIndex = uIButton2.get_zOrder();
		base.SpawnSeparator(strip);
		base.SpawnSubEntry(strip, "Water", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "Resource", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "Forest", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		for (int i = 0; i < MapEditorMainToolbar.kServices.Length; i++)
		{
			base.SpawnSubEntry(strip, MapEditorMainToolbar.kServices[i].get_enumName(), "MAIN_TOOL", null, "ToolbarIcon", true);
		}
		base.SpawnSeparator(strip);
		base.SpawnSubEntry(strip, "Environment", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSeparator(strip);
		base.SpawnSubEntry(strip, "MapSettings", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	private void ShowHideHeightmapsPanels(UIComponent comp, int index)
	{
		UITabstrip uITabstrip = comp as UITabstrip;
		if (index == this.m_ImportHeightmapsIndex)
		{
			this.importHeightmapsPanel.Show();
			this.exportHeightmapsPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_ExportHeightmapsIndex)
		{
			this.importHeightmapsPanel.Hide();
			this.exportHeightmapsPanel.Show();
			uITabstrip.get_tabContainer().Hide();
		}
		else
		{
			this.importHeightmapsPanel.Hide();
			this.exportHeightmapsPanel.Hide();
			uITabstrip.get_tabContainer().Show();
		}
	}

	private static readonly PositionData<ItemClass.Service>[] kServices = Utils.GetOrderedEnumData<ItemClass.Service>("MapEditor");

	private int m_ImportHeightmapsIndex;

	private int m_ExportHeightmapsIndex;

	private ImportHeightmapPanel m_ImportHeightmapsPanel;

	private ExportHeightmapPanel m_ExportHeightmapsPanel;
}
