﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public sealed class RoadsPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Road;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 1;
		}
	}

	protected override bool IsPlacementRelevant(BuildingInfo info)
	{
		bool flag = true;
		if ((base.isMapEditor || base.isAssetEditor) && info.m_paths != null)
		{
			for (int i = 0; i < info.m_paths.Length; i++)
			{
				flag &= info.m_paths[i].m_netInfo.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
			}
		}
		if (base.isAssetEditor)
		{
			flag &= info.m_buildingAI.WorksAsNet();
		}
		return base.IsPlacementRelevant(info) && flag;
	}

	protected override bool IsServiceValid(NetInfo info)
	{
		if (base.isAssetEditor)
		{
			return !(info == ToolsModifierControl.toolController.m_editPrefabInfo) && (info.GetService() == this.service || info.GetService() == ItemClass.Service.PublicTransport || info.GetService() == ItemClass.Service.Beautification);
		}
		if (base.isMapEditor)
		{
			return info.GetService() == this.service || info.GetService() == ItemClass.Service.PublicTransport;
		}
		return info.GetService() == this.service && (info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)3, new Comparison<PrefabInfo>(base.ItemsTypeSort));
		if (ToolsModifierControl.toolController.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor) || ToolsModifierControl.toolController.m_mode.IsFlagSet(ItemClass.Availability.MapEditor))
		{
			base.RefreshDLCBadges();
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		NetInfo netInfo = objectUserData as NetInfo;
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (netInfo != null)
		{
			base.ShowRoadsOptionPanel();
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				netTool.Prefab = netInfo;
			}
		}
		if (buildingInfo != null)
		{
			base.HideRoadsOptionPanel();
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
