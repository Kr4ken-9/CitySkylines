﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class ChirperWrapper : IChirper
{
	public ChirperWrapper(MonoBehaviour behaviour)
	{
		this.m_Container = behaviour;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnChirperExtensionsReleased();
		this.m_ChirperExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IChirperExtension>();
		this.OnChirperExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnChirperExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnChirperExtensionsCreated()
	{
		for (int i = 0; i < this.m_ChirperExtensions.Count; i++)
		{
			try
			{
				this.m_ChirperExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_ChirperExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnChirperExtensionsReleased()
	{
		for (int i = 0; i < this.m_ChirperExtensions.Count; i++)
		{
			try
			{
				this.m_ChirperExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_ChirperExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	internal void OnChirperExtensionsMessagesUpdated()
	{
		for (int i = 0; i < this.m_ChirperExtensions.Count; i++)
		{
			this.m_ChirperExtensions[i].OnMessagesUpdated();
		}
	}

	internal void OnChirperExtensionsNewMessage(IChirperMessage message)
	{
		for (int i = 0; i < this.m_ChirperExtensions.Count; i++)
		{
			this.m_ChirperExtensions[i].OnNewMessage(message);
		}
	}

	internal void OnChirperExtensionsUpdate()
	{
		for (int i = 0; i < this.m_ChirperExtensions.Count; i++)
		{
			this.m_ChirperExtensions[i].OnUpdate();
		}
	}

	public bool DestroyBuiltinChirper()
	{
		if (ChirpPanel.instance != null)
		{
			Object.Destroy(ChirpPanel.instance.get_gameObject());
			return true;
		}
		return false;
	}

	public bool ShowBuiltinChirper(bool show)
	{
		if (ChirpPanel.instance != null)
		{
			ChirpPanel.instance.get_gameObject().SetActive(show);
			return true;
		}
		return false;
	}

	public bool SetBuiltinChirperFree(bool free)
	{
		if (ChirpPanel.instance != null)
		{
			UIAnchorStyle anchor;
			if (free)
			{
				anchor = 5;
			}
			else
			{
				anchor = 65;
			}
			ChirpPanel.instance.get_component().set_anchor(anchor);
			return true;
		}
		return false;
	}

	public bool SetBuiltinChirperAnchor(ChirperAnchor anchor)
	{
		if (ChirpPanel.instance != null)
		{
			if (anchor == null)
			{
				ChirpPanel.instance.get_component().set_pivot(0);
			}
			else if (anchor == 1)
			{
				ChirpPanel.instance.get_component().set_pivot(1);
			}
			else if (anchor == 2)
			{
				ChirpPanel.instance.get_component().set_pivot(2);
			}
			else if (anchor == 3)
			{
				ChirpPanel.instance.get_component().set_pivot(6);
			}
			else if (anchor == 4)
			{
				ChirpPanel.instance.get_component().set_pivot(7);
			}
			else if (anchor == 5)
			{
				ChirpPanel.instance.get_component().set_pivot(8);
			}
			return true;
		}
		return false;
	}

	public Vector2 builtinChirperPosition
	{
		get
		{
			if (ChirpPanel.instance != null)
			{
				return ChirpPanel.instance.get_component().get_relativePosition();
			}
			return Vector2.get_zero();
		}
		set
		{
			if (ChirpPanel.instance != null)
			{
				ChirpPanel.instance.get_component().set_relativePosition(value);
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator<MessageBase[]> SimulationGetMessages()
	{
		return new ChirperWrapper.<SimulationGetMessages>c__Iterator0();
	}

	[DebuggerHidden]
	private IEnumerator SynchronizeMessagesCoroutine()
	{
		ChirperWrapper.<SynchronizeMessagesCoroutine>c__Iterator1 <SynchronizeMessagesCoroutine>c__Iterator = new ChirperWrapper.<SynchronizeMessagesCoroutine>c__Iterator1();
		<SynchronizeMessagesCoroutine>c__Iterator.$this = this;
		return <SynchronizeMessagesCoroutine>c__Iterator;
	}

	public void SyncMessages()
	{
		this.m_Container.StartCoroutine(this.SynchronizeMessagesCoroutine());
	}

	[DebuggerHidden]
	private IEnumerator SimulationDeleteMessage(IChirperMessage message)
	{
		ChirperWrapper.<SimulationDeleteMessage>c__Iterator2 <SimulationDeleteMessage>c__Iterator = new ChirperWrapper.<SimulationDeleteMessage>c__Iterator2();
		<SimulationDeleteMessage>c__Iterator.message = message;
		return <SimulationDeleteMessage>c__Iterator;
	}

	public void DeleteMessage(IChirperMessage message)
	{
		if (Singleton<SimulationManager>.get_exists() && Singleton<MessageManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.SimulationDeleteMessage(message));
		}
	}

	private MonoBehaviour m_Container;

	private List<IChirperExtension> m_ChirperExtensions = new List<IChirperExtension>();
}
