﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class DistrictWorldInfoPanel : WorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_DistrictName = base.Find<UITextField>("DistrictName");
		this.m_DistrictName.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_SpecializationSprite = base.Find<UISprite>("SpecializationSprite");
		this.m_CommercialSpecializationSprite = base.Find<UISprite>("CommercialSpecializationSprite");
		this.m_ResidentialSpecializationSprite = base.Find<UISprite>("ResidentialSpecializationSprite");
		this.m_OfficeSpecializationSprite = base.Find<UISprite>("OfficeSpecializationSprite");
		this.m_PoliciesList = base.Find<UIPanel>("PoliciesList");
		this.m_PoliciesButton = base.Find<UIButton>("PoliciesButton");
		this.m_Style = base.Find<UIDropDown>("StyleDropdown");
		if (this.m_Style != null)
		{
			this.m_Style.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnStyleChanged));
		}
		base.get_component().add_eventPositionChanged(delegate(UIComponent c, Vector2 v)
		{
			if (Vector3.Magnitude(base.get_component().get_position() - this.m_PrevPos) > 0.001f)
			{
				this.m_Style.ClosePopup(true);
			}
		});
		this.m_HappinessIcon = base.Find<UISprite>("Happiness");
		this.m_AverageLandValue = base.Find<UILabel>("AverageLandValue");
		this.m_ZoneChart = base.Find<UIRadialChart>("ZoneChart");
		this.m_AgeChart = base.Find<UIRadialChart>("AgeChart");
		UIRadialChart.SliceSettings arg_14E_0 = this.m_AgeChart.GetSlice(0);
		Color32 color = this.m_ChildColor;
		this.m_AgeChart.GetSlice(0).set_outterColor(color);
		arg_14E_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_179_0 = this.m_AgeChart.GetSlice(1);
		color = this.m_TeenColor;
		this.m_AgeChart.GetSlice(1).set_outterColor(color);
		arg_179_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_1A4_0 = this.m_AgeChart.GetSlice(2);
		color = this.m_YoungColor;
		this.m_AgeChart.GetSlice(2).set_outterColor(color);
		arg_1A4_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_1CF_0 = this.m_AgeChart.GetSlice(3);
		color = this.m_AdultColor;
		this.m_AgeChart.GetSlice(3).set_outterColor(color);
		arg_1CF_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_1FA_0 = this.m_AgeChart.GetSlice(4);
		color = this.m_SeniorColor;
		this.m_AgeChart.GetSlice(4).set_outterColor(color);
		arg_1FA_0.set_innerColor(color);
		this.m_ChildLegend = base.Find<UILabel>("ChildAmount");
		this.m_TeenLegend = base.Find<UILabel>("TeenAmount");
		this.m_YoungLegend = base.Find<UILabel>("YoungAmount");
		this.m_AdultLegend = base.Find<UILabel>("AdultAmount");
		this.m_SeniorLegend = base.Find<UILabel>("SeniorAmount");
		this.m_PopulationAmount = base.Find<UILabel>("PopulationAmount");
		this.m_HouseholdsAmount = base.Find<UILabel>("HouseholdsAmount");
		this.m_WorkersAmount = base.Find<UILabel>("WorkersAmount");
		this.m_TouristsAmount = base.Find<UILabel>("TouristsAmount");
		this.m_NoResidents = base.Find<UILabel>("NoResidents");
		this.m_NoResidents.Hide();
		this.m_ChildLegend.set_color(this.m_ChildColor);
		this.m_TeenLegend.set_color(this.m_TeenColor);
		this.m_YoungLegend.set_color(this.m_YoungColor);
		this.m_AdultLegend.set_color(this.m_AdultColor);
		this.m_SeniorLegend.set_color(this.m_SeniorColor);
		this.m_ResidentialLevelProgress = base.Find<UIPanel>("ResidentialLevelProgress");
		for (int i = 0; i < 5; i++)
		{
			this.m_ResidentialLevels[i] = this.m_ResidentialLevelProgress.Find<UIProgressBar>("Level" + (i + 1).ToString() + "Bar");
		}
		this.m_CommercialLevelProgress = base.Find<UIPanel>("CommercialLevelProgress");
		for (int j = 0; j < 5; j++)
		{
			this.m_CommercialLevels[j] = this.m_CommercialLevelProgress.Find<UIProgressBar>("Level" + (j + 1).ToString() + "Bar");
		}
		this.m_IndustrialLevelProgress = base.Find<UIPanel>("IndustrialLevelProgress");
		for (int k = 0; k < 5; k++)
		{
			this.m_IndustrialLevels[k] = this.m_IndustrialLevelProgress.Find<UIProgressBar>("Level" + (k + 1).ToString() + "Bar");
		}
		this.m_OfficeLevelProgress = base.Find<UIPanel>("OfficeLevelProgress");
		for (int l = 0; l < 5; l++)
		{
			this.m_OfficeLevels[l] = this.m_OfficeLevelProgress.Find<UIProgressBar>("Level" + (l + 1).ToString() + "Bar");
		}
		this.CreatePoliciesTemplates();
		this.m_ResidentialNo = base.Find("ResidentialThumb").Find<UISprite>("NoNoNo");
		this.m_CommercialNo = base.Find("CommercialThumb").Find<UISprite>("NoNoNo");
		this.m_IndustrialNo = base.Find("IndustrialThumb").Find<UISprite>("NoNoNo");
		this.m_OfficeNo = base.Find("OfficeThumb").Find<UISprite>("NoNoNo");
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		UIRadialChart.SliceSettings arg_3F_0 = this.m_ZoneChart.GetSlice(0);
		Color32 color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[2];
		this.m_ZoneChart.GetSlice(0).set_outterColor(color);
		arg_3F_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_83_0 = this.m_ZoneChart.GetSlice(1);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[4];
		this.m_ZoneChart.GetSlice(1).set_outterColor(color);
		arg_83_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_C7_0 = this.m_ZoneChart.GetSlice(2);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6];
		this.m_ZoneChart.GetSlice(2).set_outterColor(color);
		arg_C7_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_10B_0 = this.m_ZoneChart.GetSlice(3);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7];
		this.m_ZoneChart.GetSlice(3).set_outterColor(color);
		arg_10B_0.set_innerColor(color);
	}

	public void OnPoliciesClick()
	{
		if (ToolsModifierControl.GetTool<DistrictTool>() != ToolsModifierControl.GetCurrentTool<DistrictTool>())
		{
			ToolsModifierControl.keepThisWorldInfoPanel = true;
		}
		ToolsModifierControl.mainToolbar.ShowPoliciesPanel(this.m_InstanceID.District);
	}

	private void OnStyleChanged(UIComponent comp, int value)
	{
		byte district = this.m_InstanceID.District;
		if (Singleton<DistrictManager>.get_exists())
		{
			Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_Style = (ushort)value;
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		DistrictWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new DistrictWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		if (this.m_InstanceID.Type == InstanceType.District && this.m_InstanceID.District != 0)
		{
			return Singleton<DistrictManager>.get_instance().GetDistrictName((int)this.m_InstanceID.District);
		}
		return string.Empty;
	}

	protected override void OnVisibilityChanged(UIComponent comp, bool isVisible)
	{
		base.OnVisibilityChanged(comp, isVisible);
		if (!isVisible && ToolsModifierControl.policiesPanel != null)
		{
			ToolsModifierControl.policiesPanel.Set(0);
		}
	}

	protected override void OnSetTarget()
	{
		ToolsModifierControl.policiesPanel.Set(this.m_InstanceID.District);
		this.m_DistrictName.set_text(this.GetName());
		bool flag = false;
		if (Singleton<SimulationManager>.get_exists() && Singleton<SimulationManager>.get_instance().m_metaData != null)
		{
			flag = (Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Europe");
		}
		if (Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().m_Styles != null)
		{
			if (Singleton<DistrictManager>.get_instance().m_Styles.Length == 0)
			{
				this.m_Style.set_items(new string[1]);
				this.m_Style.get_items()[0] = Locale.Get("STYLES_DEFAULT");
				this.m_Style.set_isEnabled(false);
				this.m_Style.set_tooltip(Locale.Get("STYLES_MISSING_TOOLTIP"));
			}
			else
			{
				string[] array = new string[1 + Singleton<DistrictManager>.get_instance().m_Styles.Length];
				array[0] = Locale.Get("STYLES_DEFAULT");
				ushort num = 0;
				while ((int)num < Singleton<DistrictManager>.get_instance().m_Styles.Length)
				{
					DistrictStyle districtStyle = Singleton<DistrictManager>.get_instance().m_Styles[(int)num];
					string text = districtStyle.Name;
					if (districtStyle.BuiltIn)
					{
						if (districtStyle.Name.Equals(DistrictStyle.kEuropeanStyleName))
						{
							text = Locale.Get((!flag) ? "STYLES_EUROPEAN" : "STYLES_NORMAL");
						}
						else if (districtStyle.Name.Equals(DistrictStyle.kEuropeanSuburbiaStyleName))
						{
							text = Locale.Get("STYLES_EUROPEANSUBURBIA");
						}
					}
					array[(int)(1 + num)] = text;
					num += 1;
				}
				this.m_Style.set_items(array);
				this.m_Style.set_selectedIndex((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)this.m_InstanceID.District].m_Style);
			}
		}
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.FloorToInt(num * 100f), 0, 100);
	}

	private void UpdateLevel(UIPanel panel, UIProgressBar[] bars, int maxLevels, float val, bool exists)
	{
		float width = panel.get_width();
		float width2 = width / (float)maxLevels;
		for (int i = 0; i < 5; i++)
		{
			if (i < maxLevels)
			{
				bars[i].set_width(width2);
				bars[i].Show();
				if (exists)
				{
					if (Mathf.FloorToInt(val) >= i)
					{
						bars[i].set_progressColor(this.m_LevelCompleteColor);
						bars[i].set_value(1f);
					}
					else if (Mathf.FloorToInt(val) + 1 == i)
					{
						bars[i].set_progressColor(this.m_LevelProgressColor);
						bars[i].set_value(Mathf.Repeat(val, 1f));
					}
					else
					{
						bars[i].set_value(0f);
					}
				}
				else
				{
					bars[i].set_value(0f);
				}
			}
			else
			{
				bars[i].Hide();
			}
		}
	}

	private void CreatePoliciesTemplates()
	{
		UITemplateManager.ClearInstances(DistrictWorldInfoPanel.kPolicyEntryTemplate);
		for (int i = 0; i < DistrictWorldInfoPanel.kPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kPolicies[i].get_enumValue()))
			{
				if (DistrictWorldInfoPanel.kPolicies[i].get_enumValue() >> 5 != DistrictPolicies.Policies.Forest)
				{
					UILabel uILabel = this.m_PoliciesList.AttachUIComponent(UITemplateManager.GetAsGameObject(DistrictWorldInfoPanel.kPolicyEntryTemplate)) as UILabel;
					uILabel.Find<UISprite>("Sprite").set_spriteName("IconPolicy" + DistrictWorldInfoPanel.kPolicies[i].get_enumName());
					uILabel.set_name(DistrictWorldInfoPanel.kPolicies[i].get_enumName());
					uILabel.set_tooltip(Locale.Get("POLICIES", DistrictWorldInfoPanel.kPolicies[i].get_enumName()));
					uILabel.Hide();
				}
			}
		}
	}

	private void UpdatePolicies()
	{
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < DistrictWorldInfoPanel.kPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kPolicies[i].get_enumValue()))
			{
				if (DistrictWorldInfoPanel.kPolicies[i].get_enumValue() >> 5 != DistrictPolicies.Policies.Forest)
				{
					this.m_PoliciesList.get_components()[num].set_isVisible(Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kPolicies[i].get_enumValue(), this.m_InstanceID.District));
					if (this.m_PoliciesList.get_components()[num].get_isVisible())
					{
						num2++;
					}
					num++;
				}
			}
		}
		this.m_PoliciesList.set_opacity((num2 <= 0) ? 0f : 1f);
		base.get_component().FitChildrenVertically();
		UIComponent expr_EC = base.get_component();
		expr_EC.set_height(expr_EC.get_height() + 30f);
	}

	private void OnLocaleChanged()
	{
		int num = 0;
		for (int i = 0; i < DistrictWorldInfoPanel.kPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kPolicies[i].get_enumValue()))
			{
				if (DistrictWorldInfoPanel.kPolicies[i].get_enumValue() >> 5 != DistrictPolicies.Policies.Forest)
				{
					this.m_PoliciesList.get_components()[num].set_tooltip(Locale.Get("POLICIES", DistrictWorldInfoPanel.kPolicies[i].get_enumName()));
					num++;
				}
			}
		}
	}

	private string GetIndustrySpecialization(out string spriteName)
	{
		spriteName = string.Empty;
		for (int i = 0; i < DistrictWorldInfoPanel.kIndustryPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					spriteName = "IconPolicy" + DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumName();
					return Locale.Get("POLICIES", DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumName());
				}
			}
		}
		return Locale.Get("NO_SPECIALIZATION");
	}

	private bool HasIndustrySpecialization()
	{
		for (int i = 0; i < DistrictWorldInfoPanel.kIndustryPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kIndustryPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					return true;
				}
			}
		}
		return false;
	}

	private string GetCommercialSpecialization(out string spriteName)
	{
		spriteName = string.Empty;
		for (int i = 0; i < DistrictWorldInfoPanel.kCommercialPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					spriteName = "IconPolicy" + DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumName();
					return Locale.Get("POLICIES", DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumName());
				}
			}
		}
		return Locale.Get("NO_SPECIALIZATION");
	}

	private bool HasCommercialSpecialization()
	{
		for (int i = 0; i < DistrictWorldInfoPanel.kCommercialPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kCommercialPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					return true;
				}
			}
		}
		return false;
	}

	private string GetResidentialSpecialization(out string spriteName)
	{
		spriteName = string.Empty;
		for (int i = 0; i < DistrictWorldInfoPanel.kResidentialPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					spriteName = "IconPolicy" + DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumName();
					return Locale.Get("POLICIES", DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumName());
				}
			}
		}
		return Locale.Get("NO_SPECIALIZATION");
	}

	private bool HasResidentialSpecialization()
	{
		for (int i = 0; i < DistrictWorldInfoPanel.kResidentialPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kResidentialPolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					return true;
				}
			}
		}
		return false;
	}

	private string GetOfficeSpecialization(out string spriteName)
	{
		spriteName = string.Empty;
		for (int i = 0; i < DistrictWorldInfoPanel.kOfficePolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kOfficePolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kOfficePolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					spriteName = "IconPolicy" + DistrictWorldInfoPanel.kOfficePolicies[i].get_enumName();
					return Locale.Get("POLICIES", DistrictWorldInfoPanel.kOfficePolicies[i].get_enumName());
				}
			}
		}
		return Locale.Get("NO_SPECIALIZATION");
	}

	private bool HasOfficeSpecialization()
	{
		for (int i = 0; i < DistrictWorldInfoPanel.kOfficePolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictWorldInfoPanel.kOfficePolicies[i].get_enumValue()))
			{
				if (Singleton<DistrictManager>.get_instance().IsDistrictPolicySet(DistrictWorldInfoPanel.kOfficePolicies[i].get_enumValue(), this.m_InstanceID.District))
				{
					return true;
				}
			}
		}
		return false;
	}

	protected override void UpdateBindings()
	{
		this.m_PrevPos = base.get_component().get_position();
		base.UpdateBindings();
		if (Singleton<DistrictManager>.get_exists() && this.m_InstanceID.Type == InstanceType.District)
		{
			byte district = this.m_InstanceID.District;
			string happinessString = ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_finalHappiness));
			this.m_HappinessIcon.set_spriteName(happinessString);
			int finalCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_childData.m_finalCount;
			int finalCount2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_teenData.m_finalCount;
			int finalCount3 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_youngData.m_finalCount;
			int finalCount4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_adultData.m_finalCount;
			int finalCount5 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_seniorData.m_finalCount;
			this.m_ChildLegend.set_text(finalCount.ToString());
			this.m_TeenLegend.set_text(finalCount2.ToString());
			this.m_YoungLegend.set_text(finalCount3.ToString());
			this.m_AdultLegend.set_text(finalCount4.ToString());
			this.m_SeniorLegend.set_text(finalCount5.ToString());
			int finalCount6 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_populationData.m_finalCount;
			int finalAliveCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_residentialData.m_finalAliveCount;
			int finalHomeOrWorkCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_residentialData.m_finalHomeOrWorkCount;
			int num = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_commercialData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_industrialData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_officeData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_playerData.m_finalAliveCount);
			int num2 = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_commercialData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_industrialData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_officeData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_playerData.m_finalHomeOrWorkCount);
			int num3 = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_tourist1Data.m_averageCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_tourist2Data.m_averageCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_tourist3Data.m_averageCount);
			this.m_PopulationAmount.set_text(finalCount6.ToString());
			this.m_HouseholdsAmount.set_text(LocaleFormatter.FormatGeneric("DISTRICT_COUNTFORMAT", new object[]
			{
				finalAliveCount,
				finalHomeOrWorkCount
			}));
			this.m_WorkersAmount.set_text(LocaleFormatter.FormatGeneric("DISTRICT_COUNTFORMAT", new object[]
			{
				num,
				num2
			}));
			this.m_TouristsAmount.set_text(num3.ToString());
			int value = DistrictWorldInfoPanel.GetValue(finalCount, finalCount6);
			int value2 = DistrictWorldInfoPanel.GetValue(finalCount2, finalCount6);
			int value3 = DistrictWorldInfoPanel.GetValue(finalCount3, finalCount6);
			int num4 = DistrictWorldInfoPanel.GetValue(finalCount4, finalCount6);
			int value4 = DistrictWorldInfoPanel.GetValue(finalCount5, finalCount6);
			int num5 = value + value2 + value3 + num4 + value4;
			if (num5 != 0 && num5 != 100)
			{
				num4 = 100 - (value + value2 + value3 + value4);
			}
			this.m_AgeChart.SetValues(new int[]
			{
				value,
				value2,
				value3,
				num4,
				value4
			});
			int finalHomeOrWorkCount2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_residentialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount3 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_commercialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_industrialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount5 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_officeData.m_finalHomeOrWorkCount;
			int total = finalHomeOrWorkCount2 + finalHomeOrWorkCount3 + finalHomeOrWorkCount4 + finalHomeOrWorkCount5;
			int num6 = DistrictWorldInfoPanel.GetValue(finalHomeOrWorkCount2, total);
			int num7 = DistrictWorldInfoPanel.GetValue(finalHomeOrWorkCount3, total);
			int num8 = DistrictWorldInfoPanel.GetValue(finalHomeOrWorkCount4, total);
			int num9 = DistrictWorldInfoPanel.GetValue(finalHomeOrWorkCount5, total);
			int num10 = num6 + num7 + num8 + num9;
			if (num10 != 0 && num10 != 100)
			{
				if (num6 > num7 && num6 > num8 && num6 > num9)
				{
					num6 = 100 - (num7 + num8 + num9);
				}
				else if (num7 > num6 && num7 > num8 && num7 > num9)
				{
					num7 = 100 - (num6 + num8 + num9);
				}
				else if (num8 > num7 && num8 > num6 && num8 > num9)
				{
					num8 = 100 - (num7 + num6 + num9);
				}
				else if (num9 > num7 && num9 > num8 && num9 > num6)
				{
					num9 = 100 - (num7 + num8 + num6);
				}
			}
			this.m_ZoneChart.SetValues(new int[]
			{
				num6,
				num7,
				num8,
				num9
			});
			bool flag = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_residentialData.m_finalHomeOrWorkCount != 0u;
			this.m_NoResidents.set_isVisible(!flag);
			this.m_AgeChart.set_isVisible(flag);
			this.m_ResidentialNo.set_isVisible(!flag);
			bool flag2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_commercialData.m_finalHomeOrWorkCount != 0u;
			this.m_CommercialNo.set_isVisible(!flag2);
			bool flag3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_industrialData.m_finalHomeOrWorkCount != 0u;
			this.m_IndustrialNo.set_isVisible(!flag3);
			bool flag4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_officeData.m_finalHomeOrWorkCount != 0u;
			this.m_OfficeNo.set_isVisible(!flag4);
			float val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_residentialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_ResidentialLevelProgress, this.m_ResidentialLevels, ZonedBuildingWorldInfoPanel.GetMaxLevel(ItemClass.Zone.ResidentialLow), val, flag);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_commercialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_CommercialLevelProgress, this.m_CommercialLevels, (!this.HasCommercialSpecialization()) ? ZonedBuildingWorldInfoPanel.GetMaxLevel(ItemClass.Zone.CommercialLow) : 1, val, flag2);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_industrialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_IndustrialLevelProgress, this.m_IndustrialLevels, (!this.HasIndustrySpecialization()) ? ZonedBuildingWorldInfoPanel.GetMaxLevel(ItemClass.Zone.Industrial) : 1, val, flag3);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].m_officeData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_OfficeLevelProgress, this.m_OfficeLevels, (!this.HasOfficeSpecialization()) ? ZonedBuildingWorldInfoPanel.GetMaxLevel(ItemClass.Zone.Office) : 1, val, flag4);
			this.m_AverageLandValue.set_text(StringUtils.SafeFormat(Locale.Get("DISTRICT_LANDVALUE_AVERAGE"), new object[]
			{
				Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)district].GetLandValue(),
				Locale.Get("MONEY_CURRENCY")
			}));
			this.m_PoliciesButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Policies));
			this.UpdatePolicies();
			string text;
			this.m_SpecializationSprite.set_tooltip(this.GetIndustrySpecialization(out text));
			this.m_SpecializationSprite.set_spriteName(text);
			this.m_SpecializationSprite.set_isVisible(!string.IsNullOrEmpty(text));
			this.m_CommercialSpecializationSprite.set_tooltip(this.GetCommercialSpecialization(out text));
			this.m_CommercialSpecializationSprite.set_spriteName(text);
			this.m_CommercialSpecializationSprite.set_isVisible(!string.IsNullOrEmpty(text));
			this.m_ResidentialSpecializationSprite.set_tooltip(this.GetResidentialSpecialization(out text));
			this.m_ResidentialSpecializationSprite.set_spriteName(text);
			this.m_ResidentialSpecializationSprite.set_isVisible(!string.IsNullOrEmpty(text));
			this.m_OfficeSpecializationSprite.set_tooltip(this.GetOfficeSpecialization(out text));
			this.m_OfficeSpecializationSprite.set_spriteName(text);
			this.m_OfficeSpecializationSprite.set_isVisible(!string.IsNullOrEmpty(text));
		}
	}

	private static readonly string kPolicyEntryTemplate = "DistrictPolicyEntryTemplate";

	private static readonly PositionData<DistrictPolicies.Policies>[] kPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>();

	private static readonly PositionData<DistrictPolicies.Policies>[] kIndustryPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Industry");

	private static readonly PositionData<DistrictPolicies.Policies>[] kCommercialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Commercial");

	private static readonly PositionData<DistrictPolicies.Policies>[] kResidentialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Residential");

	private static readonly PositionData<DistrictPolicies.Policies>[] kOfficePolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Office");

	public Color m_LevelProgressColor = Color.get_blue();

	public Color m_LevelCompleteColor = Color.get_green();

	public Color32 m_ChildColor;

	public Color32 m_TeenColor;

	public Color32 m_YoungColor;

	public Color32 m_AdultColor;

	public Color32 m_SeniorColor;

	private UIRadialChart m_AgeChart;

	private UILabel m_ChildLegend;

	private UILabel m_TeenLegend;

	private UILabel m_YoungLegend;

	private UILabel m_AdultLegend;

	private UILabel m_SeniorLegend;

	private UISprite m_SpecializationSprite;

	private UISprite m_CommercialSpecializationSprite;

	private UISprite m_ResidentialSpecializationSprite;

	private UISprite m_OfficeSpecializationSprite;

	private UITextField m_DistrictName;

	private UIRadialChart m_ZoneChart;

	private UISprite m_HappinessIcon;

	private UILabel m_PopulationAmount;

	private UILabel m_AverageLandValue;

	private UILabel m_HouseholdsAmount;

	private UILabel m_WorkersAmount;

	private UILabel m_TouristsAmount;

	private UILabel m_NoResidents;

	private UIPanel m_ResidentialLevelProgress;

	private UIProgressBar[] m_ResidentialLevels = new UIProgressBar[5];

	private UIPanel m_CommercialLevelProgress;

	private UIProgressBar[] m_CommercialLevels = new UIProgressBar[5];

	private UIPanel m_IndustrialLevelProgress;

	private UIProgressBar[] m_IndustrialLevels = new UIProgressBar[5];

	private UIPanel m_OfficeLevelProgress;

	private UIProgressBar[] m_OfficeLevels = new UIProgressBar[5];

	private UISprite m_ResidentialNo;

	private UISprite m_CommercialNo;

	private UISprite m_IndustrialNo;

	private UISprite m_OfficeNo;

	private UIButton m_PoliciesButton;

	private UIPanel m_PoliciesList;

	private UIDropDown m_Style;

	private Vector3 m_PrevPos;
}
