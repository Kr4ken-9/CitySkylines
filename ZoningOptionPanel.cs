﻿using System;
using ColossalFramework.UI;

public class ZoningOptionPanel : OptionPanelBase
{
	private void Awake()
	{
		base.HidePanel();
		ZoneTool zoneTool = ToolsModifierControl.GetTool<ZoneTool>();
		if (zoneTool != null)
		{
			UITabstrip strip = base.get_component() as UITabstrip;
			if (strip != null)
			{
				strip.set_selectedIndex(0);
				strip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int index)
				{
					switch (index)
					{
					case 0:
						zoneTool.m_mode = ZoneTool.Mode.Fill;
						break;
					case 1:
						zoneTool.m_mode = ZoneTool.Mode.Select;
						break;
					case 2:
						zoneTool.m_mode = ZoneTool.Mode.Brush;
						zoneTool.m_brushSize = 10f;
						break;
					case 3:
						zoneTool.m_mode = ZoneTool.Mode.Brush;
						zoneTool.m_brushSize = 50f;
						break;
					}
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						switch (strip.get_selectedIndex())
						{
						case 0:
							zoneTool.m_mode = ZoneTool.Mode.Fill;
							break;
						case 1:
							zoneTool.m_mode = ZoneTool.Mode.Select;
							break;
						case 2:
							zoneTool.m_mode = ZoneTool.Mode.Brush;
							zoneTool.m_brushSize = 10f;
							break;
						case 3:
							zoneTool.m_mode = ZoneTool.Mode.Brush;
							zoneTool.m_brushSize = 50f;
							break;
						}
					}
				});
			}
		}
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}
}
