﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class FootballPanel : EventBuildingWorldInfoPanel
{
	protected override string problemIconTemplate
	{
		get
		{
			return FootballPanel.kProblemIconTemplate;
		}
	}

	public float ticketPrice
	{
		get
		{
			return FootballPanel.m_ticketPrice;
		}
		set
		{
			FootballPanel.m_ticketPrice = value * 100f;
			if (this.m_ticketPriceLabel != null)
			{
				this.m_ticketPriceLabel.set_text("₡" + value.ToString());
			}
		}
	}

	public Color teamColor
	{
		get
		{
			return FootballPanel.m_teamColor;
		}
		set
		{
			FootballPanel.m_teamColor = value;
			this.UpdateTeamColor();
		}
	}

	private void UpdateTeamColor()
	{
		if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			this.m_teamName1.set_textColor(FootballPanel.m_teamColor);
			this.UpdateOutlineColor(this.m_teamName1);
			this.m_teamColorIcon.set_color(FootballPanel.m_teamColor);
			this.m_teamColorIconNoButton.set_color(FootballPanel.m_teamColor);
			this.m_slicedSpriteTeamColor.set_color(FootballPanel.m_teamColor);
		}
	}

	protected override void Start()
	{
		base.Start();
		this.m_colorFieldButton = base.Find<UIButton>("ColorFieldButton");
		this.m_ticketPriceLabel = base.Find<UILabel>("TicketPrice");
		this.m_panelPastMatches = base.Find<UIPanel>("Panel PastMatches");
		this.m_pastMatchesTotalWonLost = base.Find<UILabel>("TotalMatchesWonLost");
		this.m_pastMatchesTotalMoneyEarned = base.Find<UILabel>("TotalMoneyEarned");
		this.m_notOperating = base.Find<UILabel>("NotOperating");
		this.m_currentMatch = base.Find<UILabel>("CurrentMatch");
		this.m_nextMatch = base.Find<UILabel>("NextMatch");
		this.m_nextMatchDate = base.Find<UILabel>("NextMatchDate");
		this.m_visitorCount = base.Find<UILabel>("LabelVisitors");
		this.m_labelTeamName = base.Find<UILabel>("LabelTeamName");
		this.m_teamName1 = base.Find<UILabel>("TeamName1");
		this.m_teamName2 = base.Find<UILabel>("TeamName2");
		this.m_teamColorIcon = base.Find<UISprite>("TeamColorIcon");
		this.m_teamColorIconNoButton = base.Find<UISprite>("TeamColorIconNoButton");
		this.m_colorField = base.Find<UIColorField>("ColorField");
		this.m_slicedSpriteTeamColor = base.Find<UISlicedSprite>("SlicedSprite TeamColor");
		this.m_slicedSpriteInactiveOverlay = base.Find<UISlicedSprite>("SlicedSprite InactiveOverlay");
		this.m_labelVS = base.Find<UILabel>("VS");
		this.m_panelPolicies = base.Find<UIPanel>("Panel Policies");
		this.m_totalExpenses = base.Find<UILabel>("ExpensesNumber");
		this.RefreshPolicyToolTips();
		base.get_component().add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnVisChanged));
	}

	private void RefreshPolicyToolTips()
	{
		for (int i = 1; i <= 3; i++)
		{
			UIPanel uIPanel = this.m_panelPolicies.Find<UIPanel>("Policy " + i.ToString());
			UIButton uIButton = uIPanel.Find<UIButton>("PolicyButton");
			UIButton uIButton2 = uIPanel.Find<UIButton>("PolicyStats");
			uIButton.set_tooltipBox(base.policiesTooltip);
			uIButton.set_tooltip(TooltipHelper.Format(new string[]
			{
				"title",
				Locale.Get("POLICIES", this.GetPolicy(i).ToString()),
				"text",
				Locale.Get("POLICIES_DETAIL", this.GetPolicy(i).ToString())
			}));
			uIButton2.set_tooltipBox(base.policiesTooltip);
			uIButton2.set_tooltip(TooltipHelper.Format(new string[]
			{
				"title",
				Locale.Get("POLICIES", this.GetPolicy(i).ToString()),
				"text",
				Locale.Get("POLICIES_DETAIL", this.GetPolicy(i).ToString())
			}));
		}
	}

	private void OnVisChanged(UIComponent component, bool visible)
	{
		if (visible)
		{
			this.OnBecameVisible();
		}
	}

	private void OnBecameVisible()
	{
		this.m_hasToLoad = true;
	}

	private void OnEnable()
	{
		UIColorField uIColorField = base.Find<UIColorField>("ColorField");
		uIColorField.add_eventColorPickerOpen(new UIColorField.PopupEventHandler(this.OnColorPickerOpen));
		uIColorField.add_eventColorPickerClose(new UIColorField.PopupEventHandler(this.OnColorPickerClose));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		UIColorField uIColorField = base.Find<UIColorField>("ColorField");
		uIColorField.remove_eventColorPickerOpen(new UIColorField.PopupEventHandler(this.OnColorPickerOpen));
		uIColorField.remove_eventColorPickerClose(new UIColorField.PopupEventHandler(this.OnColorPickerClose));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnColorPickerOpen(UIColorField dropdown, UIColorPicker popup, ref bool overridden)
	{
		this.m_colorFieldButton.set_isInteractive(false);
	}

	private void OnColorPickerClose(UIColorField dropdown, UIColorPicker popup, ref bool overridden)
	{
		this.m_colorFieldButton.set_isInteractive(true);
	}

	private void UpdateOutlineColor(UILabel label)
	{
		Color color = label.get_textColor();
		float num = (color.r + color.g + color.b) / 3f;
		if (num < 0.45f)
		{
			label.set_outlineColor(this.m_outlineColorLight);
			label.set_dropShadowColor(this.m_outlineColorLight);
		}
		else
		{
			label.set_outlineColor(this.m_outlineColorDark);
			label.set_dropShadowColor(this.m_outlineColorDark);
		}
	}

	private void RefreshMatchInfo()
	{
		int eventIndex = (int)Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
		EventData eventData = Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex];
		EventData eventData2 = eventData;
		EventInfo info = Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex].Info;
		if (this.m_hasToLoad)
		{
			this.m_hasToLoad = false;
			this.LoadEventData((ushort)eventIndex);
		}
		else
		{
			this.SaveEventData();
		}
		SportMatchAI sportMatchAI = info.m_eventAI as SportMatchAI;
		if (sportMatchAI != null)
		{
			this.m_colorField.set_isVisible(sportMatchAI.m_allowChangeColor);
			this.m_NameField.set_isEnabled(sportMatchAI.m_allowChangeColor);
			string teamName = sportMatchAI.GetTeamName((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex]);
			string opponentName = sportMatchAI.GetOpponentName((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex]);
			Color opponentColor = sportMatchAI.GetOpponentColor((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex]);
			this.m_labelTeamName.set_text(" " + teamName);
			this.m_shortTeamName = teamName;
			this.m_teamName1.set_text(teamName);
			while (this.m_teamName1.get_width() > 220f)
			{
				this.m_shortTeamName = this.m_shortTeamName.Substring(0, this.m_shortTeamName.Length - 5);
				this.m_shortTeamName = this.m_shortTeamName.Trim();
				this.m_shortTeamName += ". FC";
				this.m_teamName1.set_text(this.m_shortTeamName);
			}
			this.m_teamName2.set_text(opponentName);
			this.m_teamName2.set_textColor(opponentColor);
			this.UpdateOutlineColor(this.m_teamName2);
			int num;
			int num2;
			int num3;
			sportMatchAI.CountVisitors((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex], out num, out num2, out num3);
			this.m_visitorCount.set_text(string.Concat(new string[]
			{
				Locale.Get("SPORT_VISITORS"),
				" ",
				num.ToString(),
				" / ",
				num3.ToString()
			}));
		}
		if (base.isCityServiceEnabled && (eventData.m_flags & EventData.Flags.Cancelled) == EventData.Flags.None)
		{
			this.m_slicedSpriteInactiveOverlay.set_isVisible(false);
			this.m_teamName1.set_isVisible(true);
			this.m_teamName2.set_isVisible(true);
			this.m_labelVS.set_isVisible(true);
			this.m_notOperating.set_isVisible(false);
			if ((eventData.m_flags & EventData.Flags.Active) != EventData.Flags.None || (eventData.m_flags & EventData.Flags.Completed) != EventData.Flags.None)
			{
				this.m_nextMatch.set_isVisible(false);
				this.m_nextMatchDate.set_isVisible(false);
				this.m_currentMatch.set_isVisible(true);
			}
			else
			{
				this.m_nextMatch.set_isVisible(true);
				this.m_nextMatchDate.set_isVisible(true);
				this.m_currentMatch.set_isVisible(false);
				this.m_nextMatchDate.set_text(eventData2.StartTime.Date.ToString("dd/MM/yyyy"));
			}
		}
		else
		{
			this.m_slicedSpriteInactiveOverlay.set_isVisible(true);
			this.m_notOperating.set_isVisible(true);
			this.m_nextMatch.set_isVisible(false);
			this.m_nextMatchDate.set_isVisible(false);
			this.m_currentMatch.set_isVisible(false);
			this.m_teamName1.set_isVisible(false);
			this.m_teamName2.set_isVisible(false);
			this.m_labelVS.set_isVisible(false);
		}
		int num4 = 0;
		int num5 = 0;
		ulong num6 = 0uL;
		for (int i = 1; i <= 6; i++)
		{
			UISlicedSprite uISlicedSprite = this.m_panelPastMatches.Find<UISlicedSprite>("PastMatch " + i.ToString());
			UILabel uILabel = uISlicedSprite.Find<UILabel>("PastMatchWin");
			UILabel uILabel2 = uISlicedSprite.Find<UILabel>("PastMatchDate");
			UILabel uILabel3 = uISlicedSprite.Find<UILabel>("PastMatchMoney");
			UISprite uISprite = uISlicedSprite.Find<UISprite>("TicketPriceIcon");
			ushort num7 = eventData2.m_nextBuildingEvent;
			if (i == 1 && (eventData.m_flags & EventData.Flags.Cancelled) != EventData.Flags.None)
			{
				num7 = (ushort)eventIndex;
			}
			if (num7 != 0)
			{
				eventData2 = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)num7];
				uILabel2.set_text(eventData2.StartTime.Date.ToString("dd/MM/yyyy"));
				uILabel3.set_text(base.FormatMoney(eventData2.m_ticketMoney));
				uISprite.set_isVisible(true);
				num6 += eventData2.m_rewardMoney;
				num6 += eventData2.m_ticketMoney;
				if ((eventData2.m_flags & EventData.Flags.Cancelled) != EventData.Flags.None)
				{
					uILabel.set_text(Locale.Get("SPORT_CANCELLED"));
					uISlicedSprite.set_color(this.m_pastMatchLostColor);
				}
				else if ((eventData2.m_flags & EventData.Flags.Success) != EventData.Flags.None)
				{
					uILabel.set_text(Locale.Get("SPORT_WON") + " " + base.FormatMoney(eventData2.m_rewardMoney));
					uISlicedSprite.set_color(this.m_pastMatchWonColor);
					num5++;
				}
				else
				{
					uILabel.set_text(Locale.Get("SPORT_LOST"));
					uISlicedSprite.set_color(this.m_pastMatchLostColor);
					num4++;
				}
				SportMatchAI sportMatchAI2 = eventData2.Info.m_eventAI as SportMatchAI;
				if (sportMatchAI2 != null)
				{
					uISlicedSprite.set_tooltip(sportMatchAI2.GetOpponentName(num7, ref eventData2));
				}
			}
			else
			{
				uISlicedSprite.set_color(this.m_pastMatchGreyedOutColor);
				uILabel2.set_text("       -");
				uILabel.set_text("-");
				uILabel3.set_text("-  ");
				uISprite.set_isVisible(false);
				uISlicedSprite.set_tooltip(string.Empty);
			}
		}
		this.m_pastMatchesTotalWonLost.set_text(string.Concat(new object[]
		{
			num5.ToString(),
			" ",
			Locale.Get("SPORT_WON"),
			" / ",
			num4,
			" ",
			Locale.Get("SPORT_LOST")
		}));
		this.m_pastMatchesTotalMoneyEarned.set_text(base.FormatMoney(num6));
		int num8 = Singleton<EventManager>.get_instance().CountEvents(info.m_type, (EventManager.EventGroup)(-1), true);
		int num9 = 0;
		for (int j = 1; j <= 3; j++)
		{
			bool flag = Singleton<DistrictManager>.get_instance().IsCityPolicySet(this.GetPolicy(j));
			UIPanel uIPanel = this.m_panelPolicies.Find<UIPanel>("Policy " + j.ToString());
			UIButton b = uIPanel.Find<UIButton>("PolicyStats");
			UIButton b2 = uIPanel.Find<UIButton>("PolicyButton");
			UILabel uILabel4 = uIPanel.Find<UILabel>("WeeklyUpkeep");
			UILabel uILabel5 = uIPanel.Find<UILabel>("StadiumCount");
			UILabel uILabel6 = uIPanel.Find<UILabel>("WeeklyUpkeepTotal");
			int num10 = 0;
			if (flag)
			{
				num10 = this.GetPolicyCost(j);
				base.ColorButton(b, this.m_policyActiveColor);
				base.ColorButton(b2, this.m_policyActiveColor);
			}
			else
			{
				base.ColorButton(b, this.m_policyInactiveColor);
				base.ColorButton(b2, Color.get_white());
			}
			int num11 = num10 * num8;
			if (!flag)
			{
				uILabel4.set_text("-");
				uILabel5.set_text("-");
				uILabel6.set_text("-");
			}
			else
			{
				uILabel4.set_text(base.FormatMoney(num10) + "/" + Locale.Get("DATETIME_WEEKLOWERCASE"));
				uILabel5.set_text("x" + num8.ToString());
				uILabel6.set_text(base.FormatMoney(num11));
			}
			num9 += num11;
		}
		this.m_totalExpenses.set_text(base.FormatMoney(num9));
	}

	private void SaveEventData()
	{
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			int eventIndex = (int)Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_eventIndex;
			EventInfo info = Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex].Info;
			if (info != null)
			{
				info.m_eventAI.SetTicketPrice((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex], (int)FootballPanel.m_ticketPrice);
				info.m_eventAI.SetColor((ushort)eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[eventIndex], FootballPanel.m_teamColor);
			}
		});
	}

	private void LoadEventData(ushort eventIndex)
	{
		EventInfo info = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info;
		if (info != null)
		{
			Color color = info.m_eventAI.GetBuildingColor(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex]);
			base.Find<UIColorField>("ColorField").set_selectedColor(color);
			this.teamColor = color;
			int ticketPrice = info.m_eventAI.GetTicketPrice(eventIndex, ref Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex]);
			base.Find<UISlider>("Slider").set_value((float)ticketPrice / 100f);
			this.ticketPrice = (float)ticketPrice / 100f;
		}
	}

	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		this.m_hasToLoad = true;
	}

	private int GetPolicyCost(int index)
	{
		if (index == 2)
		{
			return 50000;
		}
		if (index != 3)
		{
			return 0;
		}
		return 900000;
	}

	private DistrictPolicies.Policies GetPolicy(int index)
	{
		if (index == 1)
		{
			return DistrictPolicies.Policies.ComeOneComeAll;
		}
		if (index == 2)
		{
			return DistrictPolicies.Policies.MatchSecurity;
		}
		if (index != 3)
		{
			Debug.LogError("Invalid policy index: " + index);
			return DistrictPolicies.Policies.None;
		}
		return DistrictPolicies.Policies.SubsidizedYouth;
	}

	public void ToggleComeOneComeAll()
	{
		base.TogglePolicy(DistrictPolicies.Policies.ComeOneComeAll);
	}

	public void ToggleMatchSecurity()
	{
		base.TogglePolicy(DistrictPolicies.Policies.MatchSecurity);
	}

	public void ToggleSubsidizedYouth()
	{
		base.TogglePolicy(DistrictPolicies.Policies.SubsidizedYouth);
	}

	private void Update()
	{
		if (this.m_isRelocating)
		{
			BuildingTool currentTool = ToolsModifierControl.GetCurrentTool<BuildingTool>();
			if (currentTool != null && base.IsValidTarget() && currentTool.m_relocate != 0 && !base.movingPanel.get_isVisible())
			{
				base.movingPanel.Show();
				return;
			}
			if (!base.IsValidTarget() || (currentTool != null && currentTool.m_relocate == 0))
			{
				ToolsModifierControl.mainToolbar.ResetLastTool();
				base.movingPanel.Hide();
				this.m_isRelocating = false;
			}
		}
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			this.RefreshMatchInfo();
		}
	}

	private void OnLocaleChanged()
	{
		this.RefreshPolicyToolTips();
	}

	public override void TempShow(Vector3 worldPosition, InstanceID instanceID)
	{
		base.movingPanel.Hide();
		WorldInfoPanel.Show<FootballPanel>(worldPosition, instanceID);
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.33f));
	}

	private static string kProblemIconTemplate = "ProblemIconTemplateFootball";

	public Color m_outlineColorDark = Color.get_black();

	public Color m_outlineColorLight = Color.get_white();

	private UILabel m_ticketPriceLabel;

	private UIPanel m_panelPastMatches;

	private UILabel m_pastMatchesTotalWonLost;

	private UILabel m_pastMatchesTotalMoneyEarned;

	private UILabel m_visitorCount;

	private UILabel m_notOperating;

	private UILabel m_nextMatch;

	private UILabel m_currentMatch;

	private UILabel m_nextMatchDate;

	private UILabel m_labelTeamName;

	private UILabel m_teamName1;

	private UILabel m_teamName2;

	private UISprite m_teamColorIcon;

	private UISprite m_teamColorIconNoButton;

	private UIColorField m_colorField;

	private UISlicedSprite m_slicedSpriteTeamColor;

	private UISlicedSprite m_slicedSpriteInactiveOverlay;

	private UILabel m_labelVS;

	private UIPanel m_panelPolicies;

	private UILabel m_totalExpenses;

	private string m_shortTeamName;

	public Color m_pastMatchWonColor;

	public Color m_pastMatchLostColor;

	public Color m_pastMatchGreyedOutColor;

	private UIButton m_colorFieldButton;

	private static float m_ticketPrice;

	private static Color m_teamColor;

	private bool m_hasToLoad = true;
}
