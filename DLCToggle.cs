﻿using System;
using ColossalFramework.UI;

public class DLCToggle : UICustomControl
{
	private void Start()
	{
		if (SteamHelper.IsDLCOwned((SteamHelper.DLC)this.m_DLC_AppID))
		{
			base.get_component().set_isVisible(this.m_Show == DLCToggle.Action.Show);
		}
	}

	public int m_DLC_AppID;

	public DLCToggle.Action m_Show;

	public enum Action
	{
		Show,
		Hide
	}
}
