﻿using System;
using System.Reflection;
using ColossalFramework.UI;

public class REFloatSet : REPropertySet
{
	private void OnEnable()
	{
		if (this.m_Input != null)
		{
			this.m_Input.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
	}

	private void OnDisable()
	{
		if (this.m_Input != null)
		{
			this.m_Input.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
	}

	private void TextSubmitted(UIComponent component, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			base.SetValue(num);
		}
	}

	protected override void Initialize(object target, FieldInfo targetField, string labelText)
	{
		base.get_component().Disable();
		if (this.m_Label != null)
		{
			this.m_Label.set_text(labelText);
		}
		if (this.m_Input != null)
		{
			this.m_Input.set_text(targetField.GetValue(target).ToString());
		}
		base.get_component().Enable();
	}

	protected override bool Validate(object target, FieldInfo targetField)
	{
		return target != null && targetField != null && targetField.FieldType == typeof(float);
	}

	public UILabel m_Label;

	public UITextField m_Input;
}
