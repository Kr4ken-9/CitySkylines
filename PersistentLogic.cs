﻿using System;
using ColossalFramework;
using UnityEngine;

public class PersistentLogic : MonoBehaviour
{
	private void Awake()
	{
		if (!PersistentLogic.exists)
		{
			Debug.Log("Cmd+Tab hook initiated...");
			Object.DontDestroyOnLoad(base.get_gameObject());
			PersistentLogic.exists = true;
		}
		else
		{
			Object.Destroy(base.get_gameObject());
		}
	}

	private void OnGUI()
	{
		Event current = Event.get_current();
		if (current != null && current.get_type() == 4 && current.get_keyCode() == 9 && current.get_modifiers() == 4 && Screen.get_fullScreen())
		{
			Debug.Log("Toggling to Windowed mode requested");
			Screen.set_fullScreen(false);
		}
	}

	private void OnApplicationFocus(bool focusStatus)
	{
		SavedBool savedBool = new SavedBool(Settings.fullscreen, Settings.gameSettingsFile, true);
		if (savedBool && !Screen.get_fullScreen())
		{
			Debug.Log("Toggling to Fullscreen mode requested");
			Screen.set_fullScreen(true);
		}
	}

	private static bool exists;
}
