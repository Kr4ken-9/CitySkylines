﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class TransferManager : SimulationManagerBase<TransferManager, TransferProperties>, ISimulationManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_outgoingOffers = new TransferManager.TransferOffer[180224];
		this.m_incomingOffers = new TransferManager.TransferOffer[180224];
		this.m_outgoingCount = new ushort[704];
		this.m_incomingCount = new ushort[704];
		this.m_outgoingAmount = new int[88];
		this.m_incomingAmount = new int[88];
	}

	private static float GetDistanceMultiplier(TransferManager.TransferReason material)
	{
		switch (material)
		{
		case TransferManager.TransferReason.Garbage:
			return 5E-07f;
		case TransferManager.TransferReason.Crime:
			return 1E-05f;
		case TransferManager.TransferReason.Sick:
			return 1E-06f;
		case TransferManager.TransferReason.Dead:
			return 1E-05f;
		case TransferManager.TransferReason.Worker0:
			return 1E-07f;
		case TransferManager.TransferReason.Worker1:
			return 1E-07f;
		case TransferManager.TransferReason.Worker2:
			return 1E-07f;
		case TransferManager.TransferReason.Worker3:
			return 1E-07f;
		case TransferManager.TransferReason.Student1:
			return 2E-07f;
		case TransferManager.TransferReason.Student2:
			return 2E-07f;
		case TransferManager.TransferReason.Student3:
			return 2E-07f;
		case TransferManager.TransferReason.Fire:
			return 1E-05f;
		case TransferManager.TransferReason.Bus:
			return 1E-05f;
		case TransferManager.TransferReason.Oil:
			return 1E-07f;
		case TransferManager.TransferReason.Ore:
			return 1E-07f;
		case TransferManager.TransferReason.Logs:
			return 1E-07f;
		case TransferManager.TransferReason.Grain:
			return 1E-07f;
		case TransferManager.TransferReason.Goods:
			return 1E-07f;
		case TransferManager.TransferReason.PassengerTrain:
			return 1E-05f;
		case TransferManager.TransferReason.Coal:
			return 1E-07f;
		case TransferManager.TransferReason.Family0:
			return 1E-08f;
		case TransferManager.TransferReason.Family1:
			return 1E-08f;
		case TransferManager.TransferReason.Family2:
			return 1E-08f;
		case TransferManager.TransferReason.Family3:
			return 1E-08f;
		case TransferManager.TransferReason.Single0:
			return 1E-08f;
		case TransferManager.TransferReason.Single1:
			return 1E-08f;
		case TransferManager.TransferReason.Single2:
			return 1E-08f;
		case TransferManager.TransferReason.Single3:
			return 1E-08f;
		case TransferManager.TransferReason.PartnerYoung:
			return 1E-08f;
		case TransferManager.TransferReason.PartnerAdult:
			return 1E-08f;
		case TransferManager.TransferReason.Shopping:
			return 2E-07f;
		case TransferManager.TransferReason.Petrol:
			return 1E-07f;
		case TransferManager.TransferReason.Food:
			return 1E-07f;
		case TransferManager.TransferReason.LeaveCity0:
			return 1E-08f;
		case TransferManager.TransferReason.LeaveCity1:
			return 1E-08f;
		case TransferManager.TransferReason.LeaveCity2:
			return 1E-08f;
		case TransferManager.TransferReason.Entertainment:
			return 2E-07f;
		case TransferManager.TransferReason.Lumber:
			return 1E-07f;
		case TransferManager.TransferReason.GarbageMove:
			return 5E-07f;
		case TransferManager.TransferReason.MetroTrain:
			return 1E-05f;
		case TransferManager.TransferReason.PassengerPlane:
			return 1E-05f;
		case TransferManager.TransferReason.PassengerShip:
			return 1E-05f;
		case TransferManager.TransferReason.DeadMove:
			return 5E-07f;
		case TransferManager.TransferReason.DummyCar:
			return -1E-08f;
		case TransferManager.TransferReason.DummyTrain:
			return -1E-08f;
		case TransferManager.TransferReason.DummyShip:
			return -1E-08f;
		case TransferManager.TransferReason.DummyPlane:
			return -1E-08f;
		case TransferManager.TransferReason.Single0B:
			return 1E-08f;
		case TransferManager.TransferReason.Single1B:
			return 1E-08f;
		case TransferManager.TransferReason.Single2B:
			return 1E-08f;
		case TransferManager.TransferReason.Single3B:
			return 1E-08f;
		case TransferManager.TransferReason.ShoppingB:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingC:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingD:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingE:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingF:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingG:
			return 2E-07f;
		case TransferManager.TransferReason.ShoppingH:
			return 2E-07f;
		case TransferManager.TransferReason.EntertainmentB:
			return 2E-07f;
		case TransferManager.TransferReason.EntertainmentC:
			return 2E-07f;
		case TransferManager.TransferReason.EntertainmentD:
			return 2E-07f;
		case TransferManager.TransferReason.Taxi:
			return 1E-05f;
		case TransferManager.TransferReason.CriminalMove:
			return 5E-07f;
		case TransferManager.TransferReason.Tram:
			return 1E-05f;
		case TransferManager.TransferReason.Snow:
			return 5E-07f;
		case TransferManager.TransferReason.SnowMove:
			return 5E-07f;
		case TransferManager.TransferReason.RoadMaintenance:
			return 5E-07f;
		case TransferManager.TransferReason.SickMove:
			return 1E-07f;
		case TransferManager.TransferReason.ForestFire:
			return 1E-05f;
		case TransferManager.TransferReason.Collapsed:
			return 1E-05f;
		case TransferManager.TransferReason.Collapsed2:
			return 1E-05f;
		case TransferManager.TransferReason.Fire2:
			return 1E-05f;
		case TransferManager.TransferReason.Sick2:
			return 1E-06f;
		case TransferManager.TransferReason.FloodWater:
			return 5E-07f;
		case TransferManager.TransferReason.EvacuateA:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateB:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateC:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateD:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateVipA:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateVipB:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateVipC:
			return 1E-05f;
		case TransferManager.TransferReason.EvacuateVipD:
			return 1E-05f;
		case TransferManager.TransferReason.Ferry:
			return 1E-05f;
		case TransferManager.TransferReason.CableCar:
			return 1E-05f;
		case TransferManager.TransferReason.Blimp:
			return 1E-05f;
		case TransferManager.TransferReason.Monorail:
			return 1E-05f;
		default:
			return 1E-07f;
		}
	}

	private static TransferManager.TransferReason GetFrameReason(int frameIndex)
	{
		switch (frameIndex)
		{
		case 27:
			return TransferManager.TransferReason.Single0;
		case 28:
		case 30:
		case 32:
		case 34:
			IL_2D:
			switch (frameIndex)
			{
			case 59:
				return TransferManager.TransferReason.Single1;
			case 60:
			case 62:
			case 64:
			case 66:
				IL_5A:
				switch (frameIndex)
				{
				case 91:
					return TransferManager.TransferReason.Single2;
				case 92:
				case 94:
				case 96:
				case 98:
					IL_87:
					switch (frameIndex)
					{
					case 123:
						return TransferManager.TransferReason.Single3;
					case 124:
					case 126:
					case 128:
					case 130:
						IL_B4:
						switch (frameIndex)
						{
						case 155:
							return TransferManager.TransferReason.Single0B;
						case 156:
						case 158:
						case 160:
						case 162:
							IL_E4:
							switch (frameIndex)
							{
							case 187:
								return TransferManager.TransferReason.Single1B;
							case 188:
							case 190:
							case 192:
							case 194:
								IL_114:
								switch (frameIndex)
								{
								case 219:
									return TransferManager.TransferReason.Single2B;
								case 220:
								case 222:
								case 224:
								case 226:
									IL_144:
									switch (frameIndex)
									{
									case 15:
										return TransferManager.TransferReason.Goods;
									case 16:
									case 18:
										IL_161:
										switch (frameIndex)
										{
										case 47:
											return TransferManager.TransferReason.Family0;
										case 48:
										case 50:
											IL_17E:
											switch (frameIndex)
											{
											case 79:
												return TransferManager.TransferReason.Student1;
											case 80:
											case 82:
												IL_19B:
												switch (frameIndex)
												{
												case 111:
													return TransferManager.TransferReason.Family1;
												case 112:
												case 114:
													IL_1B8:
													switch (frameIndex)
													{
													case 143:
														return TransferManager.TransferReason.Student2;
													case 144:
													case 146:
														IL_1D8:
														switch (frameIndex)
														{
														case 175:
															return TransferManager.TransferReason.Family2;
														case 176:
														case 178:
															IL_1F8:
															switch (frameIndex)
															{
															case 251:
																return TransferManager.TransferReason.Single3B;
															case 252:
															case 254:
																IL_218:
																switch (frameIndex)
																{
																case 1:
																	return TransferManager.TransferReason.Snow;
																case 2:
																	IL_22C:
																	if (frameIndex == 7)
																	{
																		return TransferManager.TransferReason.Worker0;
																	}
																	if (frameIndex == 11)
																	{
																		return TransferManager.TransferReason.Fire;
																	}
																	if (frameIndex == 23)
																	{
																		return TransferManager.TransferReason.DummyCar;
																	}
																	if (frameIndex == 39)
																	{
																		return TransferManager.TransferReason.PartnerYoung;
																	}
																	if (frameIndex == 43)
																	{
																		return TransferManager.TransferReason.Grain;
																	}
																	if (frameIndex == 55)
																	{
																		return TransferManager.TransferReason.PassengerShip;
																	}
																	if (frameIndex == 71)
																	{
																		return TransferManager.TransferReason.Worker1;
																	}
																	if (frameIndex == 75)
																	{
																		return TransferManager.TransferReason.Bus;
																	}
																	if (frameIndex == 87)
																	{
																		return TransferManager.TransferReason.DummyTrain;
																	}
																	if (frameIndex == 103)
																	{
																		return TransferManager.TransferReason.GarbageMove;
																	}
																	if (frameIndex == 107)
																	{
																		return TransferManager.TransferReason.Lumber;
																	}
																	if (frameIndex == 119)
																	{
																		return TransferManager.TransferReason.CriminalMove;
																	}
																	if (frameIndex == 135)
																	{
																		return TransferManager.TransferReason.Worker2;
																	}
																	if (frameIndex == 139)
																	{
																		return TransferManager.TransferReason.PassengerTrain;
																	}
																	if (frameIndex == 151)
																	{
																		return TransferManager.TransferReason.DummyShip;
																	}
																	if (frameIndex == 167)
																	{
																		return TransferManager.TransferReason.PartnerAdult;
																	}
																	if (frameIndex == 171)
																	{
																		return TransferManager.TransferReason.Food;
																	}
																	if (frameIndex == 183)
																	{
																		return TransferManager.TransferReason.Taxi;
																	}
																	if (frameIndex == 199)
																	{
																		return TransferManager.TransferReason.Worker3;
																	}
																	if (frameIndex == 203)
																	{
																		return TransferManager.TransferReason.MetroTrain;
																	}
																	if (frameIndex == 207)
																	{
																		return TransferManager.TransferReason.Student3;
																	}
																	if (frameIndex == 211)
																	{
																		return TransferManager.TransferReason.ShoppingG;
																	}
																	if (frameIndex == 215)
																	{
																		return TransferManager.TransferReason.DummyPlane;
																	}
																	if (frameIndex == 231)
																	{
																		return TransferManager.TransferReason.DeadMove;
																	}
																	if (frameIndex == 235)
																	{
																		return TransferManager.TransferReason.Logs;
																	}
																	if (frameIndex == 239)
																	{
																		return TransferManager.TransferReason.Family3;
																	}
																	if (frameIndex == 243)
																	{
																		return TransferManager.TransferReason.ShoppingH;
																	}
																	if (frameIndex != 247)
																	{
																		return TransferManager.TransferReason.None;
																	}
																	return TransferManager.TransferReason.Tram;
																case 3:
																	return TransferManager.TransferReason.Garbage;
																}
																goto IL_22C;
															case 253:
																return TransferManager.TransferReason.EvacuateVipD;
															case 255:
																return TransferManager.TransferReason.Ore;
															}
															goto IL_218;
														case 177:
															return TransferManager.TransferReason.Monorail;
														case 179:
															return TransferManager.TransferReason.ShoppingF;
														}
														goto IL_1F8;
													case 145:
														return TransferManager.TransferReason.Blimp;
													case 147:
														return TransferManager.TransferReason.ShoppingE;
													}
													goto IL_1D8;
												case 113:
													return TransferManager.TransferReason.CableCar;
												case 115:
													return TransferManager.TransferReason.ShoppingD;
												}
												goto IL_1B8;
											case 81:
												return TransferManager.TransferReason.Ferry;
											case 83:
												return TransferManager.TransferReason.ShoppingC;
											}
											goto IL_19B;
										case 49:
											return TransferManager.TransferReason.FloodWater;
										case 51:
											return TransferManager.TransferReason.ShoppingB;
										}
										goto IL_17E;
									case 17:
										return TransferManager.TransferReason.Sick2;
									case 19:
										return TransferManager.TransferReason.Shopping;
									}
									goto IL_161;
								case 221:
									return TransferManager.TransferReason.EvacuateD;
								case 223:
									return TransferManager.TransferReason.PassengerPlane;
								case 225:
									return TransferManager.TransferReason.Fire2;
								case 227:
									return TransferManager.TransferReason.EntertainmentD;
								}
								goto IL_144;
							case 189:
								return TransferManager.TransferReason.EvacuateVipC;
							case 191:
								return TransferManager.TransferReason.Petrol;
							case 193:
								return TransferManager.TransferReason.SickMove;
							case 195:
								return TransferManager.TransferReason.Dead;
							}
							goto IL_114;
						case 157:
							return TransferManager.TransferReason.EvacuateC;
						case 159:
							return TransferManager.TransferReason.LeaveCity2;
						case 161:
							return TransferManager.TransferReason.Collapsed2;
						case 163:
							return TransferManager.TransferReason.EntertainmentC;
						}
						goto IL_E4;
					case 125:
						return TransferManager.TransferReason.EvacuateVipB;
					case 127:
						return TransferManager.TransferReason.Coal;
					case 129:
						return TransferManager.TransferReason.SnowMove;
					case 131:
						return TransferManager.TransferReason.Sick;
					}
					goto IL_B4;
				case 93:
					return TransferManager.TransferReason.EvacuateB;
				case 95:
					return TransferManager.TransferReason.LeaveCity1;
				case 97:
					return TransferManager.TransferReason.Collapsed;
				case 99:
					return TransferManager.TransferReason.EntertainmentB;
				}
				goto IL_87;
			case 61:
				return TransferManager.TransferReason.EvacuateVipA;
			case 63:
				return TransferManager.TransferReason.Oil;
			case 65:
				return TransferManager.TransferReason.RoadMaintenance;
			case 67:
				return TransferManager.TransferReason.Crime;
			}
			goto IL_5A;
		case 29:
			return TransferManager.TransferReason.EvacuateA;
		case 31:
			return TransferManager.TransferReason.LeaveCity0;
		case 33:
			return TransferManager.TransferReason.ForestFire;
		case 35:
			return TransferManager.TransferReason.Entertainment;
		}
		goto IL_2D;
	}

	private void MatchOffers(TransferManager.TransferReason material)
	{
		if (material == TransferManager.TransferReason.None)
		{
			return;
		}
		float distanceMultiplier = TransferManager.GetDistanceMultiplier(material);
		float num;
		if (distanceMultiplier != 0f)
		{
			num = 0.01f / distanceMultiplier;
		}
		else
		{
			num = 0f;
		}
		for (int i = 7; i >= 0; i--)
		{
			int num2 = (int)(material * TransferManager.TransferReason.Student1 + i);
			int num3 = (int)this.m_incomingCount[num2];
			int num4 = (int)this.m_outgoingCount[num2];
			int num5 = 0;
			int num6 = 0;
			while (num5 < num3 || num6 < num4)
			{
				if (num5 < num3)
				{
					TransferManager.TransferOffer transferOffer = this.m_incomingOffers[num2 * 256 + num5];
					Vector3 position = transferOffer.Position;
					int num7 = transferOffer.Amount;
					do
					{
						int num8 = Mathf.Max(0, 2 - i);
						int num9 = -1;
						int num10 = -1;
						float num11 = -1f;
						int num12 = num6;
						for (int j = i; j >= num8; j--)
						{
							int num13 = (int)(material * TransferManager.TransferReason.Student1 + j);
							int num14 = (int)this.m_outgoingCount[num13];
							float num15 = (float)j + 0.1f;
							if (num11 >= num15)
							{
								break;
							}
							for (int k = num12; k < num14; k++)
							{
								TransferManager.TransferOffer transferOffer2 = this.m_outgoingOffers[num13 * 256 + k];
								float num16 = Vector3.SqrMagnitude(transferOffer2.Position - position);
								float num17;
								if (distanceMultiplier < 0f)
								{
									num17 = num15 - num15 / (1f - num16 * distanceMultiplier);
								}
								else
								{
									num17 = num15 / (1f + num16 * distanceMultiplier);
								}
								if (num17 > num11)
								{
									num9 = j;
									num10 = k;
									num11 = num17;
									if (num16 < num)
									{
										break;
									}
								}
							}
							num12 = 0;
						}
						if (num9 == -1)
						{
							break;
						}
						int num18 = (int)(material * TransferManager.TransferReason.Student1 + num9);
						TransferManager.TransferOffer transferOffer3 = this.m_outgoingOffers[num18 * 256 + num10];
						int num19 = transferOffer3.Amount;
						int num20 = Mathf.Min(num7, num19);
						if (num20 != 0)
						{
							this.StartTransfer(material, transferOffer3, transferOffer, num20);
						}
						num7 -= num20;
						num19 -= num20;
						if (num19 == 0)
						{
							int num21 = (int)(this.m_outgoingCount[num18] - 1);
							this.m_outgoingCount[num18] = (ushort)num21;
							this.m_outgoingOffers[num18 * 256 + num10] = this.m_outgoingOffers[num18 * 256 + num21];
							if (num18 == num2)
							{
								num4 = num21;
							}
						}
						else
						{
							transferOffer3.Amount = num19;
							this.m_outgoingOffers[num18 * 256 + num10] = transferOffer3;
						}
						transferOffer.Amount = num7;
					}
					while (num7 != 0);
					IL_29D:
					if (num7 == 0)
					{
						num3--;
						this.m_incomingCount[num2] = (ushort)num3;
						this.m_incomingOffers[num2 * 256 + num5] = this.m_incomingOffers[num2 * 256 + num3];
						goto IL_319;
					}
					transferOffer.Amount = num7;
					this.m_incomingOffers[num2 * 256 + num5] = transferOffer;
					num5++;
					goto IL_319;
					goto IL_29D;
				}
				IL_319:
				if (num6 < num4)
				{
					TransferManager.TransferOffer transferOffer4 = this.m_outgoingOffers[num2 * 256 + num6];
					Vector3 position2 = transferOffer4.Position;
					int num22 = transferOffer4.Amount;
					do
					{
						int num23 = Mathf.Max(0, 2 - i);
						int num24 = -1;
						int num25 = -1;
						float num26 = -1f;
						int num27 = num5;
						for (int l = i; l >= num23; l--)
						{
							int num28 = (int)(material * TransferManager.TransferReason.Student1 + l);
							int num29 = (int)this.m_incomingCount[num28];
							float num30 = (float)l + 0.1f;
							if (num26 >= num30)
							{
								break;
							}
							for (int m = num27; m < num29; m++)
							{
								TransferManager.TransferOffer transferOffer5 = this.m_incomingOffers[num28 * 256 + m];
								float num31 = Vector3.SqrMagnitude(transferOffer5.Position - position2);
								float num32;
								if (distanceMultiplier < 0f)
								{
									num32 = num30 - num30 / (1f - num31 * distanceMultiplier);
								}
								else
								{
									num32 = num30 / (1f + num31 * distanceMultiplier);
								}
								if (num32 > num26)
								{
									num24 = l;
									num25 = m;
									num26 = num32;
									if (num31 < num)
									{
										break;
									}
								}
							}
							num27 = 0;
						}
						if (num24 == -1)
						{
							break;
						}
						int num33 = (int)(material * TransferManager.TransferReason.Student1 + num24);
						TransferManager.TransferOffer transferOffer6 = this.m_incomingOffers[num33 * 256 + num25];
						int num34 = transferOffer6.Amount;
						int num35 = Mathf.Min(num22, num34);
						if (num35 != 0)
						{
							this.StartTransfer(material, transferOffer4, transferOffer6, num35);
						}
						num22 -= num35;
						num34 -= num35;
						if (num34 == 0)
						{
							int num36 = (int)(this.m_incomingCount[num33] - 1);
							this.m_incomingCount[num33] = (ushort)num36;
							this.m_incomingOffers[num33 * 256 + num25] = this.m_incomingOffers[num33 * 256 + num36];
							if (num33 == num2)
							{
								num3 = num36;
							}
						}
						else
						{
							transferOffer6.Amount = num34;
							this.m_incomingOffers[num33 * 256 + num25] = transferOffer6;
						}
						transferOffer4.Amount = num22;
					}
					while (num22 != 0);
					IL_559:
					if (num22 == 0)
					{
						num4--;
						this.m_outgoingCount[num2] = (ushort)num4;
						this.m_outgoingOffers[num2 * 256 + num6] = this.m_outgoingOffers[num2 * 256 + num4];
						continue;
					}
					transferOffer4.Amount = num22;
					this.m_outgoingOffers[num2 * 256 + num6] = transferOffer4;
					num6++;
					continue;
					goto IL_559;
				}
			}
		}
		for (int n = 0; n < 8; n++)
		{
			int num37 = (int)(material * TransferManager.TransferReason.Student1 + n);
			this.m_incomingCount[num37] = 0;
			this.m_outgoingCount[num37] = 0;
		}
		this.m_incomingAmount[(int)material] = 0;
		this.m_outgoingAmount[(int)material] = 0;
	}

	private void StartTransfer(TransferManager.TransferReason material, TransferManager.TransferOffer offerOut, TransferManager.TransferOffer offerIn, int delta)
	{
		bool active = offerIn.Active;
		bool active2 = offerOut.Active;
		if (active && offerIn.Vehicle != 0)
		{
			Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
			ushort vehicle = offerIn.Vehicle;
			VehicleInfo info = vehicles.m_buffer[(int)vehicle].Info;
			offerOut.Amount = delta;
			info.m_vehicleAI.StartTransfer(vehicle, ref vehicles.m_buffer[(int)vehicle], material, offerOut);
		}
		else if (active2 && offerOut.Vehicle != 0)
		{
			Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
			ushort vehicle2 = offerOut.Vehicle;
			VehicleInfo info2 = vehicles2.m_buffer[(int)vehicle2].Info;
			offerIn.Amount = delta;
			info2.m_vehicleAI.StartTransfer(vehicle2, ref vehicles2.m_buffer[(int)vehicle2], material, offerIn);
		}
		else if (active && offerIn.Citizen != 0u)
		{
			Array32<Citizen> citizens = Singleton<CitizenManager>.get_instance().m_citizens;
			uint citizen = offerIn.Citizen;
			CitizenInfo citizenInfo = citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
			if (citizenInfo != null)
			{
				offerOut.Amount = delta;
				citizenInfo.m_citizenAI.StartTransfer(citizen, ref citizens.m_buffer[(int)((UIntPtr)citizen)], material, offerOut);
			}
		}
		else if (active2 && offerOut.Citizen != 0u)
		{
			Array32<Citizen> citizens2 = Singleton<CitizenManager>.get_instance().m_citizens;
			uint citizen2 = offerOut.Citizen;
			CitizenInfo citizenInfo2 = citizens2.m_buffer[(int)((UIntPtr)citizen2)].GetCitizenInfo(citizen2);
			if (citizenInfo2 != null)
			{
				offerIn.Amount = delta;
				citizenInfo2.m_citizenAI.StartTransfer(citizen2, ref citizens2.m_buffer[(int)((UIntPtr)citizen2)], material, offerIn);
			}
		}
		else if (active2 && offerOut.Building != 0)
		{
			Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
			ushort building = offerOut.Building;
			BuildingInfo info3 = buildings.m_buffer[(int)building].Info;
			offerIn.Amount = delta;
			info3.m_buildingAI.StartTransfer(building, ref buildings.m_buffer[(int)building], material, offerIn);
		}
		else if (active && offerIn.Building != 0)
		{
			Array16<Building> buildings2 = Singleton<BuildingManager>.get_instance().m_buildings;
			ushort building2 = offerIn.Building;
			BuildingInfo info4 = buildings2.m_buffer[(int)building2].Info;
			offerOut.Amount = delta;
			info4.m_buildingAI.StartTransfer(building2, ref buildings2.m_buffer[(int)building2], material, offerOut);
		}
	}

	public void AddOutgoingOffer(TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		for (int i = offer.Priority; i >= 0; i--)
		{
			int num = (int)(material * TransferManager.TransferReason.Student1 + i);
			int num2 = (int)this.m_outgoingCount[num];
			if (num2 < 256)
			{
				int num3 = num * 256 + num2;
				this.m_outgoingOffers[num3] = offer;
				this.m_outgoingCount[num] = (ushort)(num2 + 1);
				this.m_outgoingAmount[(int)material] += offer.Amount;
				return;
			}
		}
	}

	public void AddIncomingOffer(TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		for (int i = offer.Priority; i >= 0; i--)
		{
			int num = (int)(material * TransferManager.TransferReason.Student1 + i);
			int num2 = (int)this.m_incomingCount[num];
			if (num2 < 256)
			{
				int num3 = num * 256 + num2;
				this.m_incomingOffers[num3] = offer;
				this.m_incomingCount[num] = (ushort)(num2 + 1);
				this.m_incomingAmount[(int)material] += offer.Amount;
				return;
			}
		}
	}

	public void RemoveOutgoingOffer(TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		for (int i = 0; i < 8; i++)
		{
			int num = (int)(material * TransferManager.TransferReason.Student1 + i);
			int num2 = (int)this.m_outgoingCount[num];
			for (int j = num2 - 1; j >= 0; j--)
			{
				int num3 = num * 256 + j;
				if (this.m_outgoingOffers[num3].m_object == offer.m_object)
				{
					this.m_outgoingAmount[(int)material] -= this.m_outgoingOffers[num3].Amount;
					this.m_outgoingOffers[num3] = this.m_outgoingOffers[num * 256 + --num2];
				}
			}
			this.m_outgoingCount[num] = (ushort)num2;
		}
	}

	public void RemoveIncomingOffer(TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		for (int i = 0; i < 8; i++)
		{
			int num = (int)(material * TransferManager.TransferReason.Student1 + i);
			int num2 = (int)this.m_incomingCount[num];
			for (int j = num2 - 1; j >= 0; j--)
			{
				int num3 = num * 256 + j;
				if (this.m_incomingOffers[num3].m_object == offer.m_object)
				{
					this.m_incomingAmount[(int)material] -= this.m_incomingOffers[num3].Amount;
					this.m_incomingOffers[num3] = this.m_incomingOffers[num * 256 + --num2];
				}
			}
			this.m_incomingCount[num] = (ushort)num2;
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0)
		{
			int frameIndex = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			this.MatchOffers(TransferManager.GetFrameReason(frameIndex));
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new TransferManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("TransferManager.UpdateData");
		base.UpdateData(mode);
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
		for (int i = 0; i < 88; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				int num = i * 8 + j;
				int num2 = (int)this.m_incomingCount[num];
				for (int k = num2 - 1; k >= 0; k--)
				{
					int num3 = num * 256 + k;
					bool flag = false;
					InstanceType type = this.m_incomingOffers[num3].m_object.Type;
					if (type != InstanceType.Vehicle)
					{
						if (type != InstanceType.Citizen)
						{
							if (type == InstanceType.Building)
							{
								flag = (instance3.m_buildings.m_buffer[(int)this.m_incomingOffers[num3].m_object.Building].Info == null);
							}
						}
						else
						{
							uint citizen = this.m_incomingOffers[num3].m_object.Citizen;
							flag = (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen) == null);
						}
					}
					else
					{
						flag = (instance.m_vehicles.m_buffer[(int)this.m_incomingOffers[num3].m_object.Vehicle].Info == null);
					}
					if (flag)
					{
						this.m_incomingAmount[i] -= this.m_incomingOffers[num3].Amount;
						this.m_incomingOffers[num3] = this.m_incomingOffers[num * 256 + --num2];
					}
				}
				this.m_incomingCount[num] = (ushort)num2;
				int num4 = (int)this.m_outgoingCount[num];
				for (int l = num4 - 1; l >= 0; l--)
				{
					int num5 = num * 256 + l;
					bool flag2 = false;
					InstanceType type2 = this.m_outgoingOffers[num5].m_object.Type;
					if (type2 != InstanceType.Vehicle)
					{
						if (type2 != InstanceType.Citizen)
						{
							if (type2 == InstanceType.Building)
							{
								flag2 = (instance3.m_buildings.m_buffer[(int)this.m_outgoingOffers[num5].m_object.Building].Info == null);
							}
						}
						else
						{
							uint citizen2 = this.m_outgoingOffers[num5].m_object.Citizen;
							flag2 = (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].GetCitizenInfo(citizen2) == null);
						}
					}
					else
					{
						flag2 = (instance.m_vehicles.m_buffer[(int)this.m_outgoingOffers[num5].m_object.Vehicle].Info == null);
					}
					if (flag2)
					{
						this.m_outgoingAmount[i] -= this.m_outgoingOffers[num5].Amount;
						this.m_outgoingOffers[num5] = this.m_outgoingOffers[num * 256 + --num4];
					}
				}
				this.m_outgoingCount[num] = (ushort)num4;
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const int TRANSFER_REASON_COUNT = 88;

	public const int TRANSFER_PRIORITY_COUNT = 8;

	public const int TRANSFER_OFFER_COUNT = 256;

	public const float REASON_CELL_SIZE_SMALL = 37.5f;

	public const float REASON_CELL_SIZE_LARGE = 270f;

	public const float LARGE_POS_LIMIT = 4800f;

	public int m_unitCount;

	private TransferManager.TransferOffer[] m_outgoingOffers;

	private TransferManager.TransferOffer[] m_incomingOffers;

	private ushort[] m_outgoingCount;

	private ushort[] m_incomingCount;

	private int[] m_outgoingAmount;

	private int[] m_incomingAmount;

	public enum TransferReason
	{
		Garbage,
		Crime,
		Sick,
		Dead,
		Worker0,
		Worker1,
		Worker2,
		Worker3,
		Student1,
		Student2,
		Student3,
		Fire,
		Bus,
		Oil,
		Ore,
		Logs,
		Grain,
		Goods,
		PassengerTrain,
		Coal,
		Family0,
		Family1,
		Family2,
		Family3,
		Single0,
		Single1,
		Single2,
		Single3,
		PartnerYoung,
		PartnerAdult,
		Shopping,
		Petrol,
		Food,
		LeaveCity0,
		LeaveCity1,
		LeaveCity2,
		Entertainment,
		Lumber,
		GarbageMove,
		MetroTrain,
		PassengerPlane,
		PassengerShip,
		DeadMove,
		DummyCar,
		DummyTrain,
		DummyShip,
		DummyPlane,
		Single0B,
		Single1B,
		Single2B,
		Single3B,
		ShoppingB,
		ShoppingC,
		ShoppingD,
		ShoppingE,
		ShoppingF,
		ShoppingG,
		ShoppingH,
		EntertainmentB,
		EntertainmentC,
		EntertainmentD,
		Taxi,
		CriminalMove,
		Tram,
		Snow,
		SnowMove,
		RoadMaintenance,
		SickMove,
		ForestFire,
		Collapsed,
		Collapsed2,
		Fire2,
		Sick2,
		FloodWater,
		EvacuateA,
		EvacuateB,
		EvacuateC,
		EvacuateD,
		EvacuateVipA,
		EvacuateVipB,
		EvacuateVipC,
		EvacuateVipD,
		Ferry,
		CableCar,
		Blimp,
		Monorail,
		None = 255
	}

	public struct TransferOffer
	{
		public bool Active
		{
			get
			{
				return (this.m_flags & 1u) != 0u;
			}
			set
			{
				if (value)
				{
					this.m_flags |= 1u;
				}
				else
				{
					this.m_flags &= 4294967294u;
				}
			}
		}

		public int Priority
		{
			get
			{
				return (int)((this.m_flags & 240u) >> 4);
			}
			set
			{
				this.m_flags = ((this.m_flags & 4294967055u) | (uint)((uint)Mathf.Clamp(value, 0, 7) << 4));
			}
		}

		public int Amount
		{
			get
			{
				return (int)((this.m_flags & 65280u) >> 8);
			}
			set
			{
				this.m_flags = ((this.m_flags & 4294902015u) | (uint)((uint)Mathf.Clamp(value, 0, 255) << 8));
			}
		}

		public int PositionX
		{
			get
			{
				return (int)((this.m_flags & 16711680u) >> 16);
			}
			set
			{
				this.m_flags = ((this.m_flags & 4278255615u) | (uint)((uint)Mathf.Clamp(value, 0, 255) << 16));
			}
		}

		public int PositionZ
		{
			get
			{
				return (int)((this.m_flags & 4278190080u) >> 24);
			}
			set
			{
				this.m_flags = ((this.m_flags & 16777215u) | (uint)((uint)Mathf.Clamp(value, 0, 255) << 24));
			}
		}

		public Vector3 Position
		{
			get
			{
				Vector3 result;
				if ((this.m_flags & 2u) != 0u)
				{
					result.x = (((this.m_flags & 16711680u) >> 16) - 127.5f) * 270f;
					result.y = 0f;
					result.z = (((this.m_flags & 4278190080u) >> 24) - 127.5f) * 270f;
				}
				else
				{
					result.x = (((this.m_flags & 16711680u) >> 16) - 127.5f) * 37.5f;
					result.y = 0f;
					result.z = (((this.m_flags & 4278190080u) >> 24) - 127.5f) * 37.5f;
				}
				return result;
			}
			set
			{
				uint num = this.m_flags & 65533u;
				if (value.x < -4800f || value.x > 4800f || value.z < -4800f || value.z > 4800f)
				{
					num |= 2u;
					num |= (uint)((uint)Mathf.Clamp((int)(value.x / 270f + 128f), 0, 255) << 16);
					num |= (uint)((uint)Mathf.Clamp((int)(value.z / 270f + 128f), 0, 255) << 24);
				}
				else
				{
					num |= (uint)((uint)Mathf.Clamp((int)(value.x / 37.5f + 128f), 0, 255) << 16);
					num |= (uint)((uint)Mathf.Clamp((int)(value.z / 37.5f + 128f), 0, 255) << 24);
				}
				this.m_flags = num;
			}
		}

		public ushort Building
		{
			get
			{
				return this.m_object.Building;
			}
			set
			{
				this.m_object.Building = value;
			}
		}

		public ushort Vehicle
		{
			get
			{
				return this.m_object.Vehicle;
			}
			set
			{
				this.m_object.Vehicle = value;
			}
		}

		public uint Citizen
		{
			get
			{
				return this.m_object.Citizen;
			}
			set
			{
				this.m_object.Citizen = value;
			}
		}

		public ushort TransportLine
		{
			get
			{
				return this.m_object.TransportLine;
			}
			set
			{
				this.m_object.TransportLine = value;
			}
		}

		public ushort NetSegment
		{
			get
			{
				return this.m_object.NetSegment;
			}
			set
			{
				this.m_object.NetSegment = value;
			}
		}

		public const uint FLAG_ACTIVE = 1u;

		public const uint FLAG_LARGE_POS = 2u;

		public const uint FLAG_PRIORITY = 240u;

		public const uint FLAG_AMOUNT = 65280u;

		public const uint FLAG_POSITION_X = 16711680u;

		public const uint FLAG_POSITION_Z = 4278190080u;

		public const uint OBJECT_BUILDING = 16777216u;

		public const uint OBJECT_VEHICLE = 33554432u;

		public const uint OBJECT_CITIZEN = 67108864u;

		public const uint OBJECT_TRANSPORT = 134217728u;

		public const uint OBJECT_TYPE = 4278190080u;

		public const uint OBJECT_INDEX = 16777215u;

		public uint m_flags;

		public InstanceID m_object;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "TransferManager");
			TransferManager instance = Singleton<TransferManager>.get_instance();
			for (int i = 0; i < 88; i++)
			{
				s.WriteInt32(instance.m_incomingAmount[i]);
				s.WriteInt32(instance.m_outgoingAmount[i]);
			}
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int j = 0; j < 88; j++)
			{
				for (int k = 0; k < 8; k++)
				{
					int num = j * 8 + k;
					uShort.Write(instance.m_incomingCount[num]);
				}
				for (int l = 0; l < 8; l++)
				{
					int num2 = j * 8 + l;
					uShort.Write(instance.m_outgoingCount[num2]);
				}
			}
			uShort.EndWrite();
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginWrite(s);
			for (int m = 0; m < 88; m++)
			{
				for (int n = 0; n < 8; n++)
				{
					int num3 = m * 8 + n;
					uint num4 = (uint)instance.m_incomingCount[num3];
					num3 *= 256;
					for (uint num5 = 0u; num5 < num4; num5 += 1u)
					{
						@bool.Write(instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num3 + (long)((ulong)num5)))))].Active);
					}
				}
				for (int num6 = 0; num6 < 8; num6++)
				{
					int num7 = m * 8 + num6;
					uint num8 = (uint)instance.m_outgoingCount[num7];
					num7 *= 256;
					for (uint num9 = 0u; num9 < num8; num9 += 1u)
					{
						@bool.Write(instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num7 + (long)((ulong)num9)))))].Active);
					}
				}
			}
			@bool.EndWrite();
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int num10 = 0; num10 < 88; num10++)
			{
				for (int num11 = 0; num11 < 8; num11++)
				{
					int num12 = num10 * 8 + num11;
					uint num13 = (uint)instance.m_incomingCount[num12];
					num12 *= 256;
					for (uint num14 = 0u; num14 < num13; num14 += 1u)
					{
						@byte.Write((byte)instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num12 + (long)((ulong)num14)))))].Priority);
					}
				}
				for (int num15 = 0; num15 < 8; num15++)
				{
					int num16 = num10 * 8 + num15;
					uint num17 = (uint)instance.m_outgoingCount[num16];
					num16 *= 256;
					for (uint num18 = 0u; num18 < num17; num18 += 1u)
					{
						@byte.Write((byte)instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num16 + (long)((ulong)num18)))))].Priority);
					}
				}
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int num19 = 0; num19 < 88; num19++)
			{
				for (int num20 = 0; num20 < 8; num20++)
				{
					int num21 = num19 * 8 + num20;
					uint num22 = (uint)instance.m_incomingCount[num21];
					num21 *= 256;
					for (uint num23 = 0u; num23 < num22; num23 += 1u)
					{
						byte2.Write((byte)instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num21 + (long)((ulong)num23)))))].Amount);
					}
				}
				for (int num24 = 0; num24 < 8; num24++)
				{
					int num25 = num19 * 8 + num24;
					uint num26 = (uint)instance.m_outgoingCount[num25];
					num25 *= 256;
					for (uint num27 = 0u; num27 < num26; num27 += 1u)
					{
						byte2.Write((byte)instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num25 + (long)((ulong)num27)))))].Amount);
					}
				}
			}
			byte2.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int num28 = 0; num28 < 88; num28++)
			{
				for (int num29 = 0; num29 < 8; num29++)
				{
					int num30 = num28 * 8 + num29;
					uint num31 = (uint)instance.m_incomingCount[num30];
					num30 *= 256;
					for (uint num32 = 0u; num32 < num31; num32 += 1u)
					{
						byte3.Write((byte)instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num30 + (long)((ulong)num32)))))].PositionX);
					}
				}
				for (int num33 = 0; num33 < 8; num33++)
				{
					int num34 = num28 * 8 + num33;
					uint num35 = (uint)instance.m_outgoingCount[num34];
					num34 *= 256;
					for (uint num36 = 0u; num36 < num35; num36 += 1u)
					{
						byte3.Write((byte)instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num34 + (long)((ulong)num36)))))].PositionX);
					}
				}
			}
			byte3.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int num37 = 0; num37 < 88; num37++)
			{
				for (int num38 = 0; num38 < 8; num38++)
				{
					int num39 = num37 * 8 + num38;
					uint num40 = (uint)instance.m_incomingCount[num39];
					num39 *= 256;
					for (uint num41 = 0u; num41 < num40; num41 += 1u)
					{
						byte4.Write((byte)instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num39 + (long)((ulong)num41)))))].PositionZ);
					}
				}
				for (int num42 = 0; num42 < 8; num42++)
				{
					int num43 = num37 * 8 + num42;
					uint num44 = (uint)instance.m_outgoingCount[num43];
					num43 *= 256;
					for (uint num45 = 0u; num45 < num44; num45 += 1u)
					{
						byte4.Write((byte)instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num43 + (long)((ulong)num45)))))].PositionZ);
					}
				}
			}
			byte4.EndWrite();
			EncodedArray.Byte byte5 = EncodedArray.Byte.BeginWrite(s);
			for (int num46 = 0; num46 < 88; num46++)
			{
				for (int num47 = 0; num47 < 8; num47++)
				{
					int num48 = num46 * 8 + num47;
					uint num49 = (uint)instance.m_incomingCount[num48];
					num48 *= 256;
					for (uint num50 = 0u; num50 < num49; num50 += 1u)
					{
						byte5.Write((byte)instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num48 + (long)((ulong)num50)))))].m_object.Type);
					}
				}
				for (int num51 = 0; num51 < 8; num51++)
				{
					int num52 = num46 * 8 + num51;
					uint num53 = (uint)instance.m_outgoingCount[num52];
					num52 *= 256;
					for (uint num54 = 0u; num54 < num53; num54 += 1u)
					{
						byte5.Write((byte)instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num52 + (long)((ulong)num54)))))].m_object.Type);
					}
				}
			}
			byte5.EndWrite();
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int num55 = 0; num55 < 88; num55++)
			{
				for (int num56 = 0; num56 < 8; num56++)
				{
					int num57 = num55 * 8 + num56;
					uint num58 = (uint)instance.m_incomingCount[num57];
					num57 *= 256;
					for (uint num59 = 0u; num59 < num58; num59 += 1u)
					{
						uInt.Write(instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num57 + (long)((ulong)num59)))))].m_object.Index);
					}
				}
				for (int num60 = 0; num60 < 8; num60++)
				{
					int num61 = num55 * 8 + num60;
					uint num62 = (uint)instance.m_outgoingCount[num61];
					num61 *= 256;
					for (uint num63 = 0u; num63 < num62; num63 += 1u)
					{
						uInt.Write(instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num61 + (long)((ulong)num63)))))].m_object.Index);
					}
				}
			}
			uInt.EndWrite();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "TransferManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "TransferManager");
			TransferManager instance = Singleton<TransferManager>.get_instance();
			if (s.get_version() >= 37u)
			{
				int num = 88;
				if (s.get_version() < 225u)
				{
					num = 64;
				}
				else if (s.get_version() < 269u)
				{
					num = 72;
				}
				else if (s.get_version() < 286u)
				{
					num = 80;
				}
				for (int i = 0; i < num; i++)
				{
					instance.m_incomingAmount[i] = s.ReadInt32();
					instance.m_outgoingAmount[i] = s.ReadInt32();
				}
				for (int j = num; j < 88; j++)
				{
					instance.m_incomingAmount[j] = 0;
					instance.m_outgoingAmount[j] = 0;
				}
				EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
				for (int k = 0; k < num; k++)
				{
					for (int l = 0; l < 8; l++)
					{
						int num2 = k * 8 + l;
						instance.m_incomingCount[num2] = uShort.Read();
					}
					for (int m = 0; m < 8; m++)
					{
						int num3 = k * 8 + m;
						instance.m_outgoingCount[num3] = uShort.Read();
					}
				}
				for (int n = num; n < 88; n++)
				{
					for (int num4 = 0; num4 < 8; num4++)
					{
						int num5 = n * 8 + num4;
						instance.m_incomingCount[num5] = 0;
					}
					for (int num6 = 0; num6 < 8; num6++)
					{
						int num7 = n * 8 + num6;
						instance.m_outgoingCount[num7] = 0;
					}
				}
				uShort.EndRead();
				EncodedArray.Bool @bool = EncodedArray.Bool.BeginRead(s);
				for (int num8 = 0; num8 < num; num8++)
				{
					for (int num9 = 0; num9 < 8; num9++)
					{
						int num10 = num8 * 8 + num9;
						uint num11 = (uint)instance.m_incomingCount[num10];
						num10 *= 256;
						for (uint num12 = 0u; num12 < num11; num12 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num10 + (long)((ulong)num12)))))].Active = @bool.Read();
						}
					}
					for (int num13 = 0; num13 < 8; num13++)
					{
						int num14 = num8 * 8 + num13;
						uint num15 = (uint)instance.m_outgoingCount[num14];
						num14 *= 256;
						for (uint num16 = 0u; num16 < num15; num16 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num14 + (long)((ulong)num16)))))].Active = @bool.Read();
						}
					}
				}
				@bool.EndRead();
				EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
				for (int num17 = 0; num17 < num; num17++)
				{
					for (int num18 = 0; num18 < 8; num18++)
					{
						int num19 = num17 * 8 + num18;
						uint num20 = (uint)instance.m_incomingCount[num19];
						num19 *= 256;
						for (uint num21 = 0u; num21 < num20; num21 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num19 + (long)((ulong)num21)))))].Priority = (int)@byte.Read();
						}
					}
					for (int num22 = 0; num22 < 8; num22++)
					{
						int num23 = num17 * 8 + num22;
						uint num24 = (uint)instance.m_outgoingCount[num23];
						num23 *= 256;
						for (uint num25 = 0u; num25 < num24; num25 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num23 + (long)((ulong)num25)))))].Priority = (int)@byte.Read();
						}
					}
				}
				@byte.EndRead();
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int num26 = 0; num26 < num; num26++)
				{
					for (int num27 = 0; num27 < 8; num27++)
					{
						int num28 = num26 * 8 + num27;
						uint num29 = (uint)instance.m_incomingCount[num28];
						num28 *= 256;
						for (uint num30 = 0u; num30 < num29; num30 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num28 + (long)((ulong)num30)))))].Amount = (int)byte2.Read();
						}
					}
					for (int num31 = 0; num31 < 8; num31++)
					{
						int num32 = num26 * 8 + num31;
						uint num33 = (uint)instance.m_outgoingCount[num32];
						num32 *= 256;
						for (uint num34 = 0u; num34 < num33; num34 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num32 + (long)((ulong)num34)))))].Amount = (int)byte2.Read();
						}
					}
				}
				byte2.EndRead();
				EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
				for (int num35 = 0; num35 < num; num35++)
				{
					for (int num36 = 0; num36 < 8; num36++)
					{
						int num37 = num35 * 8 + num36;
						uint num38 = (uint)instance.m_incomingCount[num37];
						num37 *= 256;
						for (uint num39 = 0u; num39 < num38; num39 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num37 + (long)((ulong)num39)))))].PositionX = (int)byte3.Read();
						}
					}
					for (int num40 = 0; num40 < 8; num40++)
					{
						int num41 = num35 * 8 + num40;
						uint num42 = (uint)instance.m_outgoingCount[num41];
						num41 *= 256;
						for (uint num43 = 0u; num43 < num42; num43 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num41 + (long)((ulong)num43)))))].PositionX = (int)byte3.Read();
						}
					}
				}
				byte3.EndRead();
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int num44 = 0; num44 < num; num44++)
				{
					for (int num45 = 0; num45 < 8; num45++)
					{
						int num46 = num44 * 8 + num45;
						uint num47 = (uint)instance.m_incomingCount[num46];
						num46 *= 256;
						for (uint num48 = 0u; num48 < num47; num48 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num46 + (long)((ulong)num48)))))].PositionZ = (int)byte4.Read();
						}
					}
					for (int num49 = 0; num49 < 8; num49++)
					{
						int num50 = num44 * 8 + num49;
						uint num51 = (uint)instance.m_outgoingCount[num50];
						num50 *= 256;
						for (uint num52 = 0u; num52 < num51; num52 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num50 + (long)((ulong)num52)))))].PositionZ = (int)byte4.Read();
						}
					}
				}
				byte4.EndRead();
				EncodedArray.Byte byte5 = EncodedArray.Byte.BeginRead(s);
				for (int num53 = 0; num53 < num; num53++)
				{
					for (int num54 = 0; num54 < 8; num54++)
					{
						int num55 = num53 * 8 + num54;
						uint num56 = (uint)instance.m_incomingCount[num55];
						num55 *= 256;
						for (uint num57 = 0u; num57 < num56; num57 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num55 + (long)((ulong)num57)))))].m_object.Type = (InstanceType)byte5.Read();
						}
					}
					for (int num58 = 0; num58 < 8; num58++)
					{
						int num59 = num53 * 8 + num58;
						uint num60 = (uint)instance.m_outgoingCount[num59];
						num59 *= 256;
						for (uint num61 = 0u; num61 < num60; num61 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num59 + (long)((ulong)num61)))))].m_object.Type = (InstanceType)byte5.Read();
						}
					}
				}
				byte5.EndRead();
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int num62 = 0; num62 < num; num62++)
				{
					for (int num63 = 0; num63 < 8; num63++)
					{
						int num64 = num62 * 8 + num63;
						uint num65 = (uint)instance.m_incomingCount[num64];
						num64 *= 256;
						for (uint num66 = 0u; num66 < num65; num66 += 1u)
						{
							instance.m_incomingOffers[(int)(checked((IntPtr)(unchecked((long)num64 + (long)((ulong)num66)))))].m_object.Index = uInt.Read();
						}
					}
					for (int num67 = 0; num67 < 8; num67++)
					{
						int num68 = num62 * 8 + num67;
						uint num69 = (uint)instance.m_outgoingCount[num68];
						num68 *= 256;
						for (uint num70 = 0u; num70 < num69; num70 += 1u)
						{
							instance.m_outgoingOffers[(int)(checked((IntPtr)(unchecked((long)num68 + (long)((ulong)num70)))))].m_object.Index = uInt.Read();
						}
					}
				}
				uInt.EndRead();
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "TransferManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "TransferManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "TransferManager");
		}
	}
}
