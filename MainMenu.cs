﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class MainMenu : MenuPanel
{
	protected override void Awake()
	{
		PlatformService.ActivateActionSet("InGameControls");
		this.m_DLCPanel = UIView.Find("DLCPanel");
		this.m_VersionNumber = UIView.Find<UILabel>("VersionNumber");
		this.m_VersionNumber.set_text(BuildConfig.applicationVersion);
		MainMenu.m_CurrentlyActiveMenu = base.get_component();
		MainMenu.m_MainMenu = base.get_component();
		this.m_CreditsScroller.Hide();
		if (this.m_MenuContainer != null)
		{
			this.m_MenuPanel = this.m_MenuContainer.Find("Group");
			this.m_NewsFeedPanel = this.m_MenuContainer.Find("NewsFeedPanel");
			this.m_WorkshopPanel = this.m_MenuContainer.Find("WorkshopAdPanel");
			this.m_ModsCount = this.m_MenuContainer.Find("Mods");
		}
		this.Refresh(true);
		Singleton<AudioManager>.get_instance().isAfterDarkMenu = SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC);
	}

	public string screenShotPath
	{
		get
		{
			if (string.IsNullOrEmpty(this.m_ScreenshotPath))
			{
				this.m_ScreenshotPath = Path.Combine(DataLocation.get_localApplicationData(), "Screenshots");
			}
			Directory.CreateDirectory(this.m_ScreenshotPath);
			return this.m_ScreenshotPath;
		}
	}

	private void OnGUI()
	{
		if (this.m_Screenshot.IsPressed(Event.get_current()))
		{
			Application.CaptureScreenshot(PathUtils.MakeUniquePath(Path.Combine(this.screenShotPath, "Screenshot.png")), 1);
		}
		else if (this.m_HiresScreenshot.IsPressed(Event.get_current()))
		{
			Application.CaptureScreenshot(PathUtils.MakeUniquePath(Path.Combine(this.screenShotPath, "HiresScreenshot.png")), 4);
		}
	}

	public static void SetFocus()
	{
		if (MainMenu.m_MainMenu != null)
		{
			MainMenu.m_MainMenu.Focus();
		}
	}

	private void OnDestroy()
	{
		MainMenu.m_MainMenu = null;
	}

	private void OnPackagesChanged()
	{
		this.Refresh(false);
	}

	private void OnEnable()
	{
		PackageManager.add_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		PackageManager.add_eventAssetStateChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		PackageManager.remove_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		PackageManager.remove_eventAssetStateChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	public void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	private void OnLocaleChanged()
	{
		this.Refresh(true);
	}

	private void Refresh(bool firstTime)
	{
		UIView.Show(true);
		if (firstTime)
		{
			UITemplateManager.ClearInstances("MainMenuButtonTemplate");
			for (int i = 0; i < this.m_MenuItems.Count; i++)
			{
				if (i == 0)
				{
					this.m_ContinueButton = this.CreateMenuItem("MainMenuButtonTemplate", this.m_MenuItems[i], Locale.Get("MENUITEM", this.m_MenuItems[i]));
				}
				else
				{
					UIComponent uIComponent = this.CreateMenuItem("MainMenuButtonTemplate", this.m_MenuItems[i], Locale.Get("MENUITEM", this.m_MenuItems[i]));
					if (this.m_MenuItems[i] == "LoadGame")
					{
						this.m_LoadGameButton = uIComponent;
					}
					else if (this.m_MenuItems[i] == "NewGame")
					{
						this.m_NewGameButton = uIComponent;
					}
				}
			}
		}
		if (this.m_LoadGameButton != null)
		{
			this.m_LoadGameButton.set_isEnabled(SaveHelper.AreThereSaveGames());
		}
		if (this.m_NewGameButton != null)
		{
			this.m_NewGameButton.set_isEnabled(SaveHelper.AreThereMaps() || SaveHelper.AreTherePublishedScenarios());
		}
		if (this.m_ContinueButton != null)
		{
			if (SaveHelper.AreThereSaveGames())
			{
				this.m_ContinueButton.Show();
			}
			else
			{
				this.m_ContinueButton.Hide();
			}
		}
		base.get_component().FitChildren();
	}

	protected virtual void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p != null && p.get_source().get_parent() == base.get_component())
		{
			base.Invoke(p.get_source().get_stringUserData(), 0f);
		}
	}

	private void Continue()
	{
		this.CheckForDisasterActivation();
	}

	private void CheckForDisasterActivation()
	{
		SavedBool hasSetDisastersOnOff = new SavedBool(Settings.hasSetDisastersOnOff, Settings.gameSettingsFile, false);
		if (!hasSetDisastersOnOff.get_value() && SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			ConfirmPanel.ShowModal("TEXTFORRANDOMDISASTERS", delegate(UIComponent c, int r)
			{
				if (r == 0 || r == 1)
				{
					hasSetDisastersOnOff.set_value(true);
					SavedBool savedBool = new SavedBool(Settings.randomDisastersEnabled, Settings.gameSettingsFile, DefaultSettings.randomDisastersEnabled, true);
					savedBool.set_value(r == 1);
					this.ContinueGame();
				}
			});
		}
		else
		{
			this.ContinueGame();
		}
	}

	private void ContinueGame()
	{
		SaveGameMetaData latestSaveGame = SaveHelper.GetLatestSaveGame();
		if (latestSaveGame != null)
		{
			SimulationMetaData simulationMetaData = new SimulationMetaData
			{
				m_CityName = latestSaveGame.cityName,
				m_updateMode = SimulationManager.UpdateMode.LoadGame
			};
			if (Singleton<PluginManager>.get_instance().get_enabledModCount() > 0)
			{
				simulationMetaData.m_disableAchievements = SimulationMetaData.MetaBool.True;
			}
			Singleton<LoadingManager>.get_instance().LoadLevel(latestSaveGame.assetRef, "Game", "InGame", simulationMetaData);
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.Exit();
			p.Use();
		}
	}

	private void Exit()
	{
		ConfirmPanel.ShowModal("MAINMENU_CONFIRM_EXITGAME", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
				Singleton<LoadingManager>.get_instance().QuitApplication();
			}
			else
			{
				base.get_component().Focus();
			}
		});
	}

	private void NewGame()
	{
		UIView.get_library().ShowModal("NewGamePanel");
	}

	private void LoadGame()
	{
		UIView.get_library().ShowModal("LoadPanel");
	}

	private void Options()
	{
		UIView.get_library().ShowModal("OptionsPanel");
	}

	private void ContentManager()
	{
		UIView.get_library().ShowModal("ContentManagerPanel");
	}

	public void ModsManager()
	{
		ContentManagerPanel contentManagerPanel = UIView.get_library().ShowModal<ContentManagerPanel>("ContentManagerPanel");
		if (contentManagerPanel != null)
		{
			contentManagerPanel.SetCategory(4);
		}
	}

	public void OnStoreClick()
	{
		Singleton<TelemetryManager>.get_instance().OnStoreClicked();
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlay(7);
		}
	}

	protected virtual void Tools()
	{
		MenuPanel.GoToMenu(this.m_ToolsMenu);
	}

	public void CreditsEnded()
	{
		Singleton<AudioManager>.get_instance().isShowingCredits = false;
		CreditsScroller cs = this.m_CreditsScroller.GetComponent<CreditsScroller>();
		this.m_MenuPanel.Show();
		this.m_WorkshopPanel.Show();
		this.m_ModsCount.Show();
		this.m_NewsFeedPanel.Show();
		if (this.m_ParadoxPanelWasVisibleBeforeCredits)
		{
			this.m_ParadoxPanel.Show();
		}
		this.m_CreditsScroller.set_isVisible(false);
		ValueAnimator.Animate("CreditsEndFademenus", delegate(float val)
		{
			this.m_BackgroundImage.set_color(Color32.Lerp(cs.m_BackgroundColor, Color.get_white(), Mathf.Clamp01(val)));
			this.m_WorkshopPanel.set_opacity(Mathf.Clamp01(val - 1f));
			this.m_DLCPanel.set_opacity(Mathf.Clamp01(val - 0.25f));
			this.m_VersionNumber.set_opacity(Mathf.Clamp01(val - 1f));
			this.m_ModsCount.set_opacity(Mathf.Clamp01(val - 1f));
			this.m_MenuPanel.set_opacity(Mathf.Clamp01(val - 0.5f));
			this.m_NewsFeedPanel.set_opacity(Mathf.Clamp01(val));
		}, new AnimatedFloat(0f, 2f, cs.m_CreditsFadeinTime));
	}

	private void Credits()
	{
		Singleton<AudioManager>.get_instance().isShowingCredits = true;
		CreditsScroller cs = this.m_CreditsScroller.GetComponent<CreditsScroller>();
		this.m_ParadoxPanelWasVisibleBeforeCredits = this.m_ParadoxPanel.get_component().get_isVisible();
		this.m_ParadoxPanel.Hide();
		ValueAnimator.Animate("CreditsAppearFademenus", delegate(float val)
		{
			this.m_BackgroundImage.set_color(Color32.Lerp(cs.m_BackgroundColor, Color.get_white(), Mathf.Clamp01(val)));
			this.m_DLCPanel.set_opacity(Mathf.Clamp01(val - 0.75f));
			this.m_VersionNumber.set_opacity(Mathf.Clamp01(val));
			this.m_WorkshopPanel.set_opacity(Mathf.Clamp01(val));
			this.m_ModsCount.set_opacity(Mathf.Clamp01(val));
			this.m_MenuPanel.set_opacity(Mathf.Clamp01(val - 0.5f));
			this.m_NewsFeedPanel.set_opacity(Mathf.Clamp01(val - 1f));
		}, new AnimatedFloat(2f, 0f, cs.m_MenuFadeoutTime), delegate
		{
			this.m_MenuPanel.Hide();
			this.m_WorkshopPanel.Hide();
			this.m_ModsCount.Hide();
			this.m_NewsFeedPanel.Hide();
			this.m_CreditsScroller.Show();
			cs.StartScrolling();
		});
	}

	private void OnNewGamePanelClosed(UIComponent comp)
	{
		comp.Hide();
	}

	private void OnOptionPanelClosed(UIComponent comp)
	{
		comp.Hide();
	}

	public void OnLoadPanelClosed(UIComponent comp, int ret)
	{
		this.Refresh(false);
		comp.Hide();
	}

	private const string kMainMenuButtonTemplate = "MainMenuButtonTemplate";

	public static UIComponent m_CurrentlyActiveMenu;

	public static UIComponent m_MainMenu;

	private UIComponent m_ContinueButton;

	private UIComponent m_NewGameButton;

	private UIComponent m_LoadGameButton;

	public UIComponent m_ToolsMenu;

	public List<string> m_MenuItems;

	public UIComponent m_MenuContainer;

	public UIComponent m_CreditsScroller;

	public ParadoxAccountLogin m_ParadoxPanel;

	public UITextureSprite m_BackgroundImage;

	private UIComponent m_ModsCount;

	private UIComponent m_MenuPanel;

	private UIComponent m_NewsFeedPanel;

	private UIComponent m_WorkshopPanel;

	private bool m_ParadoxPanelWasVisibleBeforeCredits;

	private UILabel m_VersionNumber;

	private UIComponent m_DLCPanel;

	private string m_ScreenshotPath;

	private SavedInputKey m_Screenshot = new SavedInputKey(Settings.screenshot, Settings.inputSettingsFile, DefaultSettings.screenshot, true);

	private SavedInputKey m_HiresScreenshot = new SavedInputKey(Settings.hiresScreenshot, Settings.inputSettingsFile, DefaultSettings.hiresScreenshot, true);
}
