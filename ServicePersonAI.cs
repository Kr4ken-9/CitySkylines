﻿using System;
using ColossalFramework.Globalization;

public class ServicePersonAI : HumanAI
{
	protected override bool StartPathFind(ushort instanceID, ref CitizenInstance citizenData)
	{
		return true;
	}

	public override string GenerateName(ushort instanceID, bool useCitizen)
	{
		if (this.m_info.m_prefabDataIndex != -1)
		{
			string text = PrefabCollection<CitizenInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
			return Locale.Get("SERVICEPERSON_TITLE", text);
		}
		return null;
	}
}
