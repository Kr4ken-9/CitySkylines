﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class LivestockExtractorAI : IndustrialExtractorAI
{
	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		int num = this.TargetAnimals(buildingID, ref buildingData);
		int num2 = this.CountAnimals(buildingID, ref buildingData);
		if (num2 < num && (buildingData.m_flags & Building.Flags.Evacuating) == Building.Flags.None)
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 30;
			int num2 = data.Length * 30;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num3);
			target.z += Mathf.Sin(num3);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 30;
			int num2 = data.Length * 30;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		ushort num;
		if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num2++;
			}
			num = nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	private int TargetAnimals(ushort buildingID, ref Building data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		return Mathf.Max(100, data.Width * data.Length * 30 + randomizer.Int32(100u)) / 100;
	}
}
