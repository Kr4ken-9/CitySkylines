﻿using System;
using ColossalFramework;
using UnityEngine;

[Serializable]
public class ItemClass : ScriptableObject
{
	public static int GetPrivateServiceIndex(ItemClass.Service service)
	{
		switch (service)
		{
		case ItemClass.Service.Residential:
			return 0;
		case ItemClass.Service.Commercial:
			return 1;
		case ItemClass.Service.Industrial:
			return 2;
		case ItemClass.Service.Natural:
			return 3;
		case ItemClass.Service.Unused2:
			return 4;
		case ItemClass.Service.Citizen:
			return 5;
		case ItemClass.Service.Tourism:
			return 6;
		case ItemClass.Service.Office:
			return 7;
		default:
			return -1;
		}
	}

	public static int GetPublicServiceIndex(ItemClass.Service service)
	{
		switch (service)
		{
		case ItemClass.Service.Road:
			return 0;
		case ItemClass.Service.Electricity:
			return 1;
		case ItemClass.Service.Water:
			return 2;
		case ItemClass.Service.Beautification:
			return 3;
		case ItemClass.Service.Garbage:
			return 4;
		case ItemClass.Service.HealthCare:
			return 5;
		case ItemClass.Service.PoliceDepartment:
			return 6;
		case ItemClass.Service.Education:
			return 7;
		case ItemClass.Service.Monument:
			return 8;
		case ItemClass.Service.FireDepartment:
			return 9;
		case ItemClass.Service.PublicTransport:
			return 10;
		case ItemClass.Service.Disaster:
			return 11;
		default:
			return -1;
		}
	}

	public static int GetPrivateSubServiceIndex(ItemClass.SubService subService)
	{
		switch (subService)
		{
		case ItemClass.SubService.ResidentialLow:
			return 0;
		case ItemClass.SubService.ResidentialHigh:
			return 1;
		case ItemClass.SubService.CommercialLow:
			return 2;
		case ItemClass.SubService.CommercialHigh:
			return 3;
		case ItemClass.SubService.IndustrialGeneric:
			return 4;
		case ItemClass.SubService.IndustrialForestry:
			return 5;
		case ItemClass.SubService.IndustrialFarming:
			return 6;
		case ItemClass.SubService.IndustrialOil:
			return 7;
		case ItemClass.SubService.IndustrialOre:
			return 8;
		case ItemClass.SubService.CommercialLeisure:
			return 9;
		case ItemClass.SubService.CommercialTourist:
			return 10;
		case ItemClass.SubService.OfficeGeneric:
			return 11;
		case ItemClass.SubService.OfficeHightech:
			return 12;
		case ItemClass.SubService.CommercialEco:
			return 13;
		case ItemClass.SubService.ResidentialLowEco:
			return 14;
		case ItemClass.SubService.ResidentialHighEco:
			return 15;
		}
		return -1;
	}

	public static int GetPublicSubServiceIndex(ItemClass.SubService subService)
	{
		switch (subService)
		{
		case ItemClass.SubService.PublicTransportBus:
			return 0;
		case ItemClass.SubService.PublicTransportMetro:
			return 1;
		case ItemClass.SubService.PublicTransportTrain:
			return 2;
		case ItemClass.SubService.PublicTransportShip:
			return 3;
		case ItemClass.SubService.PublicTransportPlane:
			return 4;
		case ItemClass.SubService.PublicTransportTaxi:
			return 5;
		case ItemClass.SubService.PublicTransportTram:
			return 6;
		case ItemClass.SubService.Unused2:
			return 9;
		case ItemClass.SubService.PublicTransportMonorail:
			return 7;
		case ItemClass.SubService.PublicTransportCableCar:
			return 8;
		}
		return -1;
	}

	public ItemClass.Zone GetZone()
	{
		switch (this.m_service)
		{
		case ItemClass.Service.Residential:
			return (this.m_subService != ItemClass.SubService.ResidentialHigh && this.m_subService != ItemClass.SubService.ResidentialHighEco) ? ItemClass.Zone.ResidentialLow : ItemClass.Zone.ResidentialHigh;
		case ItemClass.Service.Commercial:
			return (this.m_subService != ItemClass.SubService.CommercialHigh) ? ItemClass.Zone.CommercialLow : ItemClass.Zone.CommercialHigh;
		case ItemClass.Service.Industrial:
			return ItemClass.Zone.Industrial;
		case ItemClass.Service.Office:
			return ItemClass.Zone.Office;
		}
		return ItemClass.Zone.None;
	}

	public ItemClass.Zone GetSecondaryZone()
	{
		ItemClass.SubService subService = this.m_subService;
		if (subService != ItemClass.SubService.CommercialLeisure && subService != ItemClass.SubService.CommercialTourist && subService != ItemClass.SubService.CommercialEco)
		{
			return ItemClass.Zone.None;
		}
		return ItemClass.Zone.CommercialHigh;
	}

	public static bool CheckCollisionType(float minY1, float maxY1, float minY2, float maxY2, ItemClass.CollisionType collisionType1, ItemClass.CollisionType collisionType2)
	{
		int num = (int)(collisionType2 | (int)collisionType1 << 4);
		switch (num)
		{
		case 0:
		case 1:
		case 2:
		case 4:
		case 8:
			goto IL_B4;
		case 3:
		case 5:
		case 6:
		case 7:
			IL_32:
			switch (num)
			{
			case 16:
			case 17:
				goto IL_B4;
			case 18:
			case 20:
			case 24:
				goto IL_C6;
			case 19:
			case 21:
			case 22:
			case 23:
				IL_5F:
				switch (num)
				{
				case 64:
				case 68:
					goto IL_B4;
				case 65:
				case 66:
					goto IL_CE;
				case 67:
					IL_7C:
					switch (num)
					{
					case 32:
						goto IL_B4;
					case 33:
						goto IL_CE;
					case 34:
					case 35:
						IL_99:
						if (num == 128)
						{
							goto IL_B4;
						}
						if (num != 129)
						{
							return true;
						}
						goto IL_CE;
					case 36:
						goto IL_C6;
					}
					goto IL_99;
				}
				goto IL_7C;
				IL_CE:
				return minY1 <= maxY2;
			}
			goto IL_5F;
			IL_C6:
			return minY2 <= maxY1;
		}
		goto IL_32;
		IL_B4:
		return minY1 <= maxY2 && minY2 <= maxY1;
	}

	public const int PrivateServiceCount = 8;

	public const int PublicServiceCount = 12;

	public const int ServiceCount = 20;

	public const int PrivateSubServiceCount = 16;

	public const int PublicSubServiceCount = 10;

	public const int SubServiceCount = 26;

	public const int LevelCount = 5;

	public const int ZoneCount = 8;

	public ItemClass.Service m_service;

	public ItemClass.SubService m_subService;

	public ItemClass.Level m_level;

	public ItemClass.Layer m_layer = ItemClass.Layer.Default;

	public enum Service
	{
		None,
		[EnumPosition("AssetImporter", "Residential", 12)]
		Residential,
		[EnumPosition("AssetImporter", "Commercial", 13)]
		Commercial,
		[EnumPosition("AssetImporter", "Industrial", 14)]
		Industrial,
		Natural,
		Unused2,
		Citizen,
		Tourism,
		[EnumPosition("AssetImporter", "Office", 15)]
		Office,
		[EnumPosition("Game", "Roads", 0), EnumPosition("MapEditor", "Roads", 0), EnumPosition("AssetImporter", "Roads", 0), EnumPosition("Budget2", "Roads", 0)]
		Road,
		[EnumPosition("Game", "Electricity", 1), EnumPosition("Budget", "Electricity", 0), EnumPosition("AssetImporter", "Electricity", 1)]
		Electricity,
		[EnumPosition("Game", "WaterAndSewage", 2), EnumPosition("Budget", "WaterAndSewage", 1), EnumPosition("AssetImporter", "WaterAndSewage", 2)]
		Water,
		[EnumPosition("Game", "Beautification", 10), EnumPosition("Budget", "Beautification", 7), EnumPosition("AssetImporter", "Beautification", 10)]
		Beautification,
		[EnumPosition("Game", "Garbage", 3), EnumPosition("Budget", "Garbage", 2), EnumPosition("AssetImporter", "Garbage", 3)]
		Garbage,
		[EnumPosition("Game", "Healthcare", 4), EnumPosition("Budget", "Healthcare", 3), EnumPosition("AssetImporter", "Healthcare", 4)]
		HealthCare,
		[EnumPosition("Game", "Police", 6), EnumPosition("Budget", "Police", 5), EnumPosition("AssetImporter", "Police", 6)]
		PoliceDepartment,
		[EnumPosition("Game", "Education", 8), EnumPosition("Budget", "Education", 6), EnumPosition("AssetImporter", "Education", 8)]
		Education,
		[EnumPosition("Game", "Monuments", 11), EnumPosition("Budget", "Monuments", 11), EnumPosition("AssetImporter", "Monuments", 11)]
		Monument,
		[EnumPosition("Game", "FireDepartment", 5), EnumPosition("Budget", "FireDepartment", 4), EnumPosition("AssetImporter", "FireDepartment", 5)]
		FireDepartment,
		[EnumPosition("Game", "PublicTransport", 9), EnumPosition("AssetImporter", "PublicTransport", 9)]
		PublicTransport,
		Disaster
	}

	public enum SubService
	{
		None,
		[EnumPosition("AssetImporter", "ResidentialLow", 0)]
		ResidentialLow,
		[EnumPosition("AssetImporter", "ResidentialHigh", 1)]
		ResidentialHigh,
		[EnumPosition("AssetImporter", "CommercialLow", 2)]
		CommercialLow,
		[EnumPosition("AssetImporter", "CommercialHigh", 3)]
		CommercialHigh,
		[EnumPosition("AssetImporter", "Industrial", 4)]
		IndustrialGeneric,
		[EnumPosition("AssetImporter", "IndustrialForestry", 5)]
		IndustrialForestry,
		[EnumPosition("AssetImporter", "IndustrialFarming", 6)]
		IndustrialFarming,
		[EnumPosition("AssetImporter", "IndustrialOil", 7)]
		IndustrialOil,
		[EnumPosition("AssetImporter", "IndustrialOre", 8)]
		IndustrialOre,
		[EnumPosition("Game", "Bus", 0), EnumPosition("Budget", "Bus", 0)]
		PublicTransportBus,
		[EnumPosition("Game", "Metro", 2), EnumPosition("Budget", "Metro", 2)]
		PublicTransportMetro,
		[EnumPosition("Game", "Train", 3), EnumPosition("Budget", "Train", 3)]
		PublicTransportTrain,
		[EnumPosition("Game", "Ship", 4), EnumPosition("Budget", "Ship", 4)]
		PublicTransportShip,
		[EnumPosition("Game", "Plane", 5), EnumPosition("Budget", "Plane", 5)]
		PublicTransportPlane,
		[EnumPosition("Game", "Taxi", 8), EnumPosition("Budget", "Taxi", 8)]
		PublicTransportTaxi,
		[EnumPosition("Game", "Tram", 1), EnumPosition("Budget", "Tram", 1)]
		PublicTransportTram,
		Unused2,
		[EnumPosition("AssetImporter", "CommercialLeisure", 9)]
		CommercialLeisure,
		[EnumPosition("AssetImporter", "CommercialTourist", 10)]
		CommercialTourist,
		[EnumPosition("Game", "Monorail", 6), EnumPosition("Budget", "Monorail", 6)]
		PublicTransportMonorail,
		[EnumPosition("Game", "CableCar", 7), EnumPosition("Budget", "CableCar", 7)]
		PublicTransportCableCar,
		[EnumPosition("AssetImporter", "Office", 11)]
		OfficeGeneric,
		[EnumPosition("AssetImporter", "OfficeHightech", 12)]
		OfficeHightech,
		[EnumPosition("AssetImporter", "CommercialEco", 13)]
		CommercialEco,
		[EnumPosition("AssetImporter", "ResidentialLowEco", 14)]
		ResidentialLowEco,
		[EnumPosition("AssetImporter", "ResidentialHighEco", 15)]
		ResidentialHighEco
	}

	public enum Level
	{
		None = -1,
		Level1,
		Level2,
		Level3,
		Level4,
		Level5
	}

	[Flags]
	public enum Layer
	{
		None = 0,
		Default = 1,
		WaterPipes = 2,
		PublicTransport = 4,
		MetroTunnels = 8,
		AirplanePaths = 16,
		ShipPaths = 32,
		Markers = 64,
		WaterStructures = 128,
		BlimpPaths = 256
	}

	public enum Zone
	{
		[EnumPosition(6)]
		Unzoned,
		Distant,
		[EnumPosition(0), EnumPosition("Decoration", "ResidentialLow", 0), EnumPosition("Taxes", "ResidentialLow", 0)]
		ResidentialLow,
		[EnumPosition(1), EnumPosition("Decoration", "ResidentialHigh", 1), EnumPosition("Taxes", "ResidentialHigh", 1)]
		ResidentialHigh,
		[EnumPosition(2), EnumPosition("Decoration", "CommercialLow", 2), EnumPosition("Taxes", "CommercialLow", 2)]
		CommercialLow,
		[EnumPosition(3), EnumPosition("Decoration", "CommercialHigh", 3), EnumPosition("Taxes", "CommercialHigh", 3)]
		CommercialHigh,
		[EnumPosition(4), EnumPosition("Decoration", "Industrial", 4), EnumPosition("Taxes", "Industrial", 4)]
		Industrial,
		[EnumPosition(5), EnumPosition("Decoration", "Office", 5), EnumPosition("Taxes", "Office", 5)]
		Office,
		None = 15
	}

	public enum Placement
	{
		Manual,
		Automatic,
		Procedural
	}

	[Flags]
	public enum Availability
	{
		None = 0,
		Game = 1,
		MapEditor = 2,
		AssetEditor = 4,
		ThemeEditor = 8,
		ScenarioEditor = 16,
		GameAndMap = 3,
		GameAndAsset = 5,
		MapAndAsset = 6,
		Editors = 254,
		All = 255
	}

	public enum CollisionType
	{
		Undefined,
		Underground,
		Terrain,
		Elevated = 4,
		Zoned = 8
	}
}
