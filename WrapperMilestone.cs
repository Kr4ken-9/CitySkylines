﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using UnityEngine;

public class WrapperMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (this.m_requirePassed != null)
		{
			if (this.m_requirePassed.m_hidden)
			{
				if (totalProgress.m_description == null)
				{
					this.GetProgressInfo(ref totalProgress, this.m_requirePassed);
				}
				else
				{
					subProgress.EnsureCapacity(subProgress.m_size + 1);
					this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size], this.m_requirePassed);
					subProgress.m_size++;
				}
			}
			else
			{
				this.m_requirePassed.GetLocalizedProgressImpl(this.m_targetCount, ref totalProgress, subProgress);
			}
		}
		else if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		if (!(this.m_requirePassed != null))
		{
			return new GeneratedString.String(string.Empty);
		}
		if (this.m_requirePassed.m_hidden)
		{
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_WRAPPER_DESC"), new GeneratedString[]
			{
				this.m_requirePassed.GetDescription()
			});
		}
		return this.m_requirePassed.GetDescriptionImpl(this.m_targetCount);
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int targetCount = this.m_targetCount;
		info.m_min = 0f;
		info.m_max = (float)targetCount;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
		}
		info.m_description = string.Empty;
		info.m_progress = string.Empty;
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info, MilestoneInfo milestone)
	{
		MilestoneInfo.Data data = this.m_data;
		int num = 0;
		info.m_min = 0f;
		info.m_max = 100f;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			if (data.m_passedCount != 0)
			{
				num = 100;
			}
			else
			{
				num = Mathf.Clamp(Mathf.RoundToInt(milestone.GetProgress() * 100f), 0, 99);
			}
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
		}
		string localizedName = milestone.GetLocalizedName();
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_WRAPPER_DESC"), localizedName);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_WRAPPER_PROG"), num);
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		int result = 0;
		if (this.m_requirePassed != null)
		{
			result = this.m_requirePassed.GetComparisonValue();
		}
		return result;
	}

	public override MilestoneInfo.Data GetData()
	{
		if (this.m_data == null)
		{
			WrapperMilestone.WrapperData wrapperData = new WrapperMilestone.WrapperData();
			if (this.RefreshRequirePassed())
			{
				wrapperData.m_requirePassed = this.m_requirePassed.GetData();
			}
			this.m_data = wrapperData;
			this.m_data.m_isAssigned = true;
		}
		return this.m_data;
	}

	public override void SetData(MilestoneInfo.Data data)
	{
		if (data == null || data.m_isAssigned)
		{
			return;
		}
		data.m_isAssigned = true;
		WrapperMilestone.WrapperData wrapperData = data as WrapperMilestone.WrapperData;
		if (wrapperData != null)
		{
			if (this.m_data == null)
			{
				this.m_data = wrapperData;
			}
			else
			{
				this.m_data.m_passedCount = wrapperData.m_passedCount;
				this.m_data.m_progress = wrapperData.m_progress;
			}
			this.m_written = true;
			if (this.RefreshRequirePassed())
			{
				this.m_requirePassed.SetData(wrapperData.m_requirePassed);
				if (this.m_data == wrapperData)
				{
					wrapperData.m_requirePassed = this.m_requirePassed.GetData();
				}
			}
			return;
		}
		if (data != null)
		{
			MilestoneInfo.Data data2 = this.GetData();
			data2.m_passedCount = data.m_passedCount;
			data2.m_progress = data.m_progress;
			this.m_written = true;
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		int num = 0;
		if (this.RefreshRequirePassed())
		{
			num += Mathf.Min(this.m_targetCount, Singleton<UnlockManager>.get_instance().CheckMilestone(this.m_requirePassed, forceUnlock, forceRelock));
		}
		if (forceUnlock)
		{
			data.m_progress = (long)this.m_targetCount;
			data.m_passedCount = 1;
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			data.m_progress = (long)Mathf.Min(this.m_targetCount, num);
			if (data.m_progress >= (long)this.m_targetCount)
			{
				data.m_passedCount = 1;
			}
		}
		return data;
	}

	public override void ResetWrittenStatus()
	{
		base.ResetWrittenStatus();
		if (this.RefreshRequirePassed())
		{
			this.m_requirePassed.ResetWrittenStatus();
		}
	}

	public override void ResetImpl(int maxPassCount, bool directReference, bool onlyUnwritten)
	{
		base.ResetImpl(maxPassCount, directReference, onlyUnwritten);
		if (this.RefreshRequirePassed())
		{
			this.m_requirePassed.ResetImpl(this.m_targetCount, false, onlyUnwritten);
		}
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32((int)this.m_type);
		if (this.RefreshRequirePassed())
		{
			s.WriteUniqueString(this.m_requirePassed.m_name);
		}
		else if (this.m_milestoneName != null)
		{
			s.WriteUniqueString(this.m_milestoneName);
		}
		else
		{
			s.WriteUniqueString(null);
		}
		s.WriteInt32(this.m_targetCount);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_type = (WrapperMilestone.WrapperType)s.ReadInt32();
		this.m_requirePassed = null;
		this.m_milestoneName = s.ReadUniqueString();
		this.m_targetCount = s.ReadInt32();
	}

	private bool RefreshRequirePassed()
	{
		if (this.m_requirePassed == null && this.m_milestoneName != null)
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.m_requirePassed = MilestoneCollection.FindMilestone(this.m_milestoneName);
				if (this.m_requirePassed != null)
				{
					this.m_milestoneName = null;
				}
			}).Wait();
		}
		return this.m_requirePassed != null;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float result = 0f;
		if (this.m_requirePassed != null)
		{
			if (this.m_requirePassed.m_hidden)
			{
				result = this.m_requirePassed.GetProgress();
			}
			else if (this.m_targetCount != 0)
			{
				result = Mathf.Clamp01((float)data.m_progress / (float)this.m_targetCount);
			}
		}
		return result;
	}

	public WrapperMilestone.WrapperType m_type;

	public MilestoneInfo m_requirePassed;

	public int m_targetCount = 1;

	[NonSerialized]
	private string m_milestoneName;

	public enum WrapperType
	{
		Milestone,
		Create,
		Fill
	}

	public class WrapperData : MilestoneInfo.Data
	{
		public override void Serialize(DataSerializer s)
		{
			base.Serialize(s);
			s.WriteObject<MilestoneInfo.Data>(this.m_requirePassed);
		}

		public override void Deserialize(DataSerializer s)
		{
			base.Deserialize(s);
			this.m_requirePassed = s.ReadObject<MilestoneInfo.Data>();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
			base.AfterDeserialize(s);
		}

		public MilestoneInfo.Data m_requirePassed;
	}
}
