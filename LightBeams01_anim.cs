﻿using System;
using UnityEngine;

public class LightBeams01_anim : MonoBehaviour
{
	private void Update()
	{
		float num = Time.get_time() * this.textureSpeed;
		base.GetComponent<Renderer>().get_material().SetTextureOffset("_MainTex", new Vector2(num, 0f));
	}

	public float textureSpeed = 10f;
}
