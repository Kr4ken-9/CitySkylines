﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SaveMapPanel : LoadSavePanelBase<MapMetaData>
{
	public static bool isSaving
	{
		get
		{
			return SaveMapPanel.m_IsSaving;
		}
	}

	public static string lastLoadedName
	{
		set
		{
			SaveMapPanel.m_LastSaveName = value;
		}
	}

	public static string lastLoadedAsset
	{
		set
		{
			SaveMapPanel.m_LastMapName = value;
		}
	}

	public static bool lastPublished
	{
		set
		{
			SaveMapPanel.m_LastPublished = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		SaveMapPanel.m_IsSaving = false;
		this.OnLocaleChanged();
		this.m_MapName = base.Find<UITextField>("MapName");
		this.m_SaveName = base.Find<UITextField>("SaveName");
		this.m_Publish = base.Find<UICheckBox>("Publish");
		this.m_Publish.set_isEnabled(false);
		this.m_Publish.set_tooltip(Locale.Get("MAPEDITOR_UNFITREQUIREMENTS"));
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_CurrentSnapShot = base.Find<UILabel>("CurrentSnapShot");
		this.m_FileList = base.Find<UIListBox>("SaveList");
		UIButton uIButton = base.Find<UIButton>("Previous");
		uIButton.add_eventClick(new MouseEventHandler(this.PreviousSnapshot));
		uIButton.add_eventDoubleClick(new MouseEventHandler(this.PreviousSnapshot));
		UIButton uIButton2 = base.Find<UIButton>("Next");
		uIButton2.add_eventClick(new MouseEventHandler(this.NextSnapshot));
		uIButton2.add_eventDoubleClick(new MouseEventHandler(this.NextSnapshot));
		this.m_WaterTile0 = base.Find<UICheckBox>("WaterTile0");
		this.m_ShipConnections = base.Find<UICheckBox>("ShipConnections");
		this.m_TrainConnections = base.Find<UICheckBox>("TrainConnections");
		this.m_PlaneConnections = base.Find<UICheckBox>("PlaneConnections");
		this.m_RoadInConnections = base.Find<UICheckBox>("RoadInConnections");
		this.m_RoadOutConnections = base.Find<UICheckBox>("RoadOutConnections");
		this.m_ResourceOil = base.Find<UICheckBox>("ResourceOil");
		this.m_ResourceOre = base.Find<UICheckBox>("ResourceOre");
		this.m_ResourceForest = base.Find<UICheckBox>("ResourceForest");
		this.m_ResourceFertility = base.Find<UICheckBox>("ResourceFertility");
		this.m_SaveButton = base.Find<UIButton>("Save");
		this.m_SaveButton.set_isEnabled(false);
		this.m_MapName.set_text(string.Empty);
	}

	protected override void OnLocaleChanged()
	{
		SaveMapPanel.m_LastSaveName = Locale.Get("DEFAULTSAVENAMES", "NewMap");
		SaveMapPanel.m_LastMapName = Locale.Get("DEFAULTSAVENAMES", "New Map");
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
			if (tool != null)
			{
				string snapShotPath = tool.snapShotPath;
				if (!string.IsNullOrEmpty(snapShotPath))
				{
					for (int i = 0; i < SaveMapPanel.m_Extensions.Length; i++)
					{
						this.m_FileSystemReporter[i] = new FileSystemReporter("*" + SaveMapPanel.m_Extensions[i], snapShotPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
						if (this.m_FileSystemReporter[i] != null)
						{
							this.m_FileSystemReporter[i].Start();
						}
					}
				}
			}
			if (this.m_Publish.get_isEnabled())
			{
				this.m_Publish.set_isChecked(SaveMapPanel.m_LastPublished);
			}
			else
			{
				this.m_Publish.set_isChecked(false);
			}
			this.m_MapName.set_text(SaveMapPanel.m_LastMapName);
			this.m_SaveName.set_text(SaveMapPanel.m_LastSaveName);
			this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
			this.m_SaveName.SelectAll();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			for (int j = 0; j < SaveMapPanel.m_Extensions.Length; j++)
			{
				if (this.m_FileSystemReporter[j] != null)
				{
					this.m_FileSystemReporter[j].Stop();
					this.m_FileSystemReporter[j].Dispose();
					this.m_FileSystemReporter[j] = null;
				}
			}
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveName.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnSave();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	private void UpdateRequirements()
	{
		int x;
		int z;
		Singleton<GameAreaManager>.get_instance().GetStartTile(out x, out z);
		uint num;
		uint num2;
		uint num3;
		uint num4;
		uint num5;
		Singleton<NaturalResourceManager>.get_instance().GetTileResources(x, z, out num, out num2, out num3, out num4, out num5);
		this.m_WaterTile0.set_isChecked(num5 != 0u);
		int num6;
		int num7;
		Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, out num6, out num7);
		this.m_ShipConnections.set_isChecked(num6 != 0);
		Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, out num6, out num7);
		this.m_TrainConnections.set_isChecked(num6 != 0);
		Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, out num6, out num7);
		this.m_PlaneConnections.set_isChecked(num6 != 0);
		Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.Road, ItemClass.SubService.None, out num6, out num7);
		this.m_RoadInConnections.set_isChecked(num6 != 0);
		this.m_RoadOutConnections.set_isChecked(num7 != 0);
		Singleton<NaturalResourceManager>.get_instance().CalculateUnlockableResources(out num, out num2, out num3, out num4, out num5);
		this.m_ResourceOil.set_isChecked(num2 != 0u);
		this.m_ResourceOre.set_isChecked(num != 0u);
		this.m_ResourceFertility.set_isChecked(num4 != 0u);
		this.m_ResourceForest.set_isChecked(num3 != 0u);
		bool flag = num5 != 0u && num6 != 0 && num7 != 0;
		if (!flag)
		{
			this.m_Publish.set_isChecked(false);
		}
		this.m_Publish.set_isEnabled(flag);
		this.m_Publish.set_tooltip((!flag) ? Locale.Get("MAPEDITOR_UNFITREQUIREMENTS") : string.Empty);
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.FetchSnapshots();
		});
	}

	private void FetchSnapshots()
	{
		string b = (this.m_SnapshotPaths.Count <= 0) ? null : this.m_SnapshotPaths[this.m_CurrentSnapshot];
		this.m_CurrentSnapshot = 0;
		this.m_SnapshotPaths.Clear();
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null)
		{
			string snapShotPath = tool.snapShotPath;
			if (snapShotPath != null)
			{
				FileInfo[] fileInfo = SaveHelper.GetFileInfo(snapShotPath);
				if (fileInfo != null)
				{
					FileInfo[] array = fileInfo;
					for (int i = 0; i < array.Length; i++)
					{
						FileInfo fileInfo2 = array[i];
						if (string.Compare(Path.GetExtension(fileInfo2.Name), ".png", StringComparison.OrdinalIgnoreCase) == 0)
						{
							this.m_SnapshotPaths.Add(fileInfo2.FullName);
							if (fileInfo2.FullName == b)
							{
								this.m_CurrentSnapshot = this.m_SnapshotPaths.Count - 1;
							}
						}
					}
				}
				UIComponent arg_10C_0 = base.Find("Previous");
				bool isEnabled = this.m_SnapshotPaths.Count > 1;
				base.Find("Next").set_isEnabled(isEnabled);
				arg_10C_0.set_isEnabled(isEnabled);
				this.RefreshSnapshot();
			}
		}
	}

	private void RefreshSnapshot()
	{
		if (this.m_SnapShotSprite.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShotSprite.get_texture());
		}
		if (this.m_SnapshotPaths.Count > 0)
		{
			Image image = new Image(this.m_SnapshotPaths[this.m_CurrentSnapshot]);
			image.Resize(400, 224);
			this.m_SnapShotSprite.set_texture(image.CreateTexture());
			this.m_CurrentSnapShot.set_text(this.m_CurrentSnapshot + 1 + " / " + this.m_SnapshotPaths.Count);
		}
		else
		{
			this.m_SnapShotSprite.set_texture(Object.Instantiate<Texture>(this.m_DefaultMapPreviewTexture) as Texture2D);
			this.m_CurrentSnapShot.set_text(Locale.Get("NO_SNAPSHOTS"));
		}
	}

	private void PreviousSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot - 1) % this.m_SnapshotPaths.Count;
		if (this.m_CurrentSnapshot < 0)
		{
			this.m_CurrentSnapshot += this.m_SnapshotPaths.Count;
		}
		this.RefreshSnapshot();
	}

	private void NextSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot + 1) % this.m_SnapshotPaths.Count;
		this.RefreshSnapshot();
	}

	protected override void Refresh()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
		this.UpdateRequirements();
		this.FetchSnapshots();
		base.ClearListing();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				try
				{
					MapMetaData mapMetaData = current.Instantiate<MapMetaData>();
					if (!mapMetaData.IsBuiltinMap(current.get_package().get_packagePath()))
					{
						base.AddToListing(current.get_name(), mapMetaData.timeStamp, current, mapMetaData, true);
					}
				}
				catch (Exception ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
				}
			}
		}
		this.m_FileList.set_items(base.GetListingItems());
		this.m_SaveName.set_text(SaveMapPanel.m_LastSaveName);
		this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
		this.m_SaveName.SelectAll();
	}

	public void OnSaveNameChanged(UIComponent comp, string text)
	{
		this.m_FileList.set_selectedIndex(base.FindIndexOf(text));
	}

	public void OnSaveSelectionChanged(UIComponent comp, int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_SaveName.set_text(base.GetListingName(index));
			this.m_SaveName.MoveToEnd();
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.m_SaveName.set_text(base.GetListingName(sel));
			this.OnSave();
		}
	}

	private bool CheckValid()
	{
		return !StringExtensions.IsNullOrWhiteSpace(this.m_SaveName.get_text()) && !StringExtensions.IsNullOrWhiteSpace(this.m_MapName.get_text());
	}

	public void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_SaveButton.set_isEnabled(this.CheckValid());
		}
	}

	public void OnSave()
	{
		if (!this.CheckValid())
		{
			return;
		}
		string savePathName = SaveMapPanel.GetSavePathName(this.m_SaveName.get_text());
		if (!File.Exists(savePathName))
		{
			this.SaveRoutine(this.m_SaveName.get_text(), this.m_MapName.get_text(), this.m_Publish.get_isChecked() && this.m_Publish.get_isEnabled());
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_SAVEOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveRoutine(this.m_SaveName.get_text(), this.m_MapName.get_text(), this.m_Publish.get_isChecked() && this.m_Publish.get_isEnabled());
				}
			});
		}
	}

	private void SaveRoutine(string fileName, string mapName, bool publish)
	{
		SaveMapPanel.m_LastSaveName = fileName;
		SaveMapPanel.m_LastMapName = mapName;
		SaveMapPanel.m_LastPublished = publish;
		base.StartCoroutine(this.SaveMap(fileName, mapName, publish));
	}

	public void AutoSave(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SaveMapPanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Auto save requested");
				SaveMapPanel.m_IsSaving = true;
				base.StartCoroutine(this.SaveMap(savename, string.IsNullOrEmpty(SaveMapPanel.m_LastMapName) ? "AutoSavedMap" : SaveMapPanel.m_LastMapName, false));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Auto save request");
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator SaveMap(string saveName, string mapName, bool publish)
	{
		SaveMapPanel.<SaveMap>c__Iterator0 <SaveMap>c__Iterator = new SaveMapPanel.<SaveMap>c__Iterator0();
		<SaveMap>c__Iterator.saveName = saveName;
		<SaveMap>c__Iterator.mapName = mapName;
		<SaveMap>c__Iterator.publish = publish;
		<SaveMap>c__Iterator.$this = this;
		return <SaveMap>c__Iterator;
	}

	public void OpenSnapshotFolders()
	{
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null && !string.IsNullOrEmpty(tool.snapShotPath))
		{
			Utils.OpenInFileBrowser(tool.snapShotPath);
		}
	}

	private static string GetSavePathName(string saveName)
	{
		string path = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		return Path.Combine(DataLocation.get_mapLocation(), path);
	}

	private static string GetTempSavePath()
	{
		return Path.Combine(DataLocation.get_mapLocation(), "Temp");
	}

	public const int kPreviewWidth = 400;

	public const int kPreviewHeight = 224;

	public Texture m_DefaultMapPreviewTexture;

	public UIComponent[] m_TabFocusList;

	private static string m_LastSaveName = "NewMap";

	private static string m_LastMapName = "New Map";

	private static bool m_LastPublished;

	private static readonly string[] m_Extensions = Image.GetExtensions(94);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[SaveMapPanel.m_Extensions.Length];

	private List<string> m_SnapshotPaths = new List<string>();

	private UITextureSprite m_SnapShotSprite;

	private UILabel m_CurrentSnapShot;

	private int m_CurrentSnapshot;

	private UIButton m_SaveButton;

	private UITextField m_MapName;

	private UICheckBox m_WaterTile0;

	private UICheckBox m_ShipConnections;

	private UICheckBox m_TrainConnections;

	private UICheckBox m_PlaneConnections;

	private UICheckBox m_RoadInConnections;

	private UICheckBox m_RoadOutConnections;

	private UICheckBox m_ResourceOil;

	private UICheckBox m_ResourceOre;

	private UICheckBox m_ResourceForest;

	private UICheckBox m_ResourceFertility;

	private UIListBox m_FileList;

	private UITextField m_SaveName;

	private UICheckBox m_Publish;

	private Task m_PackageSaveTask;

	private static volatile bool m_IsSaving;
}
