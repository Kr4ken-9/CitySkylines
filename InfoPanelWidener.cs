﻿using System;
using ColossalFramework.UI;

public class InfoPanelWidener : UICustomControl
{
	private void Awake()
	{
		this.m_orgPanelWidth = base.get_component().get_width();
		this.m_titleTextField.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnTextChanged));
		this.m_titleTextField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextSubmitted));
		this.m_titleTextField.add_eventTextCancelled(new PropertyChangedEventHandler<string>(this.OnTextCancelled));
		this.m_orgTextfieldWidth = this.m_titleTextField.get_width();
	}

	private void OnTextCancelled(UIComponent component, string value)
	{
		this.RefreshPanelWidth();
	}

	private void OnTextSubmitted(UIComponent component, string value)
	{
		this.RefreshPanelWidth();
	}

	private void OnTextChanged(UIComponent comp, string text)
	{
		this.RefreshPanelWidth();
	}

	private void RefreshPanelWidth()
	{
		using (UIFontRenderer uIFontRenderer = this.m_titleTextField.get_font().ObtainRenderer())
		{
			float num = uIFontRenderer.MeasureString(this.m_titleTextField.get_text()).x - this.m_orgTextfieldWidth;
			if (num > 0f)
			{
				base.get_component().set_width(this.m_orgPanelWidth + num + 10f);
			}
			else
			{
				base.get_component().set_width(this.m_orgPanelWidth);
			}
		}
	}

	private float m_orgPanelWidth;

	private float m_orgTextfieldWidth;

	public UITextField m_titleTextField;
}
