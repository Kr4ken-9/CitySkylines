﻿using System;
using ColossalFramework.IO;
using UnityEngine;

public struct InstanceID : IEquatable<InstanceID>
{
	public bool IsEmpty
	{
		get
		{
			return (this.m_id & 16777215u) == 0u;
		}
	}

	public InstanceType Type
	{
		get
		{
			return (InstanceType)((this.m_id & 4278190080u) >> 24);
		}
		set
		{
			this.m_id = ((this.m_id & 16777215u) | (uint)((uint)Mathf.Clamp((int)value, 0, 255) << 24));
		}
	}

	public uint RawData
	{
		get
		{
			return this.m_id;
		}
		set
		{
			this.m_id = value;
		}
	}

	public uint Index
	{
		get
		{
			return this.m_id & 16777215u;
		}
		set
		{
			this.m_id = ((this.m_id & 4278190080u) | (value & 16777215u));
		}
	}

	public ushort Building
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 16777216u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (16777216u | (uint)value);
		}
	}

	public ushort Vehicle
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 33554432u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (33554432u | (uint)value);
		}
	}

	public byte District
	{
		get
		{
			return (byte)(((this.m_id & 4278190080u) != 50331648u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (50331648u | (uint)value);
		}
	}

	public uint Citizen
	{
		get
		{
			return ((this.m_id & 4278190080u) != 67108864u) ? 0u : (this.m_id & 16777215u);
		}
		set
		{
			this.m_id = (67108864u | value);
		}
	}

	public ushort NetNode
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 83886080u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (83886080u | (uint)value);
		}
	}

	public ushort NetSegment
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 100663296u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (100663296u | (uint)value);
		}
	}

	public ushort ParkedVehicle
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 117440512u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (117440512u | (uint)value);
		}
	}

	public ushort TransportLine
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 134217728u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (134217728u | (uint)value);
		}
	}

	public ushort CitizenInstance
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 150994944u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (150994944u | (uint)value);
		}
	}

	public ushort Prop
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 167772160u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (167772160u | (uint)value);
		}
	}

	public uint Tree
	{
		get
		{
			return ((this.m_id & 4278190080u) != 184549376u) ? 0u : (this.m_id & 16777215u);
		}
		set
		{
			this.m_id = (184549376u | value);
		}
	}

	public ushort Event
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 201326592u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (201326592u | (uint)value);
		}
	}

	public uint NetLane
	{
		get
		{
			return ((this.m_id & 4278190080u) != 218103808u) ? 0u : (this.m_id & 16777215u);
		}
		set
		{
			this.m_id = (218103808u | value);
		}
	}

	public ushort Disaster
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 268435456u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (268435456u | (uint)value);
		}
	}

	public byte Lightning
	{
		get
		{
			return (byte)(((this.m_id & 4278190080u) != 285212672u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (285212672u | (uint)value);
		}
	}

	public ushort RadioChannel
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 301989888u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (301989888u | (uint)value);
		}
	}

	public ushort RadioContent
	{
		get
		{
			return (ushort)(((this.m_id & 4278190080u) != 318767104u) ? 0u : (this.m_id & 16777215u));
		}
		set
		{
			this.m_id = (318767104u | (uint)value);
		}
	}

	public void SetBuildingProp(ushort building, int propIndex)
	{
		this.m_id = (uint)(234881024 | (int)building | propIndex << 16);
	}

	public void GetBuildingProp(out ushort building, out int propIndex)
	{
		if ((this.m_id & 4278190080u) == 234881024u)
		{
			building = (ushort)(this.m_id & 65535u);
			propIndex = (int)(this.m_id >> 16 & 255u);
		}
		else
		{
			building = 0;
			propIndex = 0;
		}
	}

	public void SetNetLaneProp(uint lane, int propIndex)
	{
		this.m_id = (251658240u | lane | (uint)((uint)propIndex << 18));
	}

	public void GetNetLaneProp(out uint lane, out int propIndex)
	{
		if ((this.m_id & 4278190080u) == 251658240u)
		{
			lane = (this.m_id & 262143u);
			propIndex = (int)(this.m_id >> 18 & 63u);
		}
		else
		{
			lane = 0u;
			propIndex = 0;
		}
	}

	public static bool operator ==(InstanceID x, InstanceID y)
	{
		return x.m_id == y.m_id;
	}

	public static bool operator !=(InstanceID x, InstanceID y)
	{
		return x.m_id != y.m_id;
	}

	public static bool operator <=(InstanceID x, InstanceID y)
	{
		return x.m_id <= y.m_id;
	}

	public static bool operator >=(InstanceID x, InstanceID y)
	{
		return x.m_id >= y.m_id;
	}

	public static bool operator <(InstanceID x, InstanceID y)
	{
		return x.m_id < y.m_id;
	}

	public static bool operator >(InstanceID x, InstanceID y)
	{
		return x.m_id > y.m_id;
	}

	public override bool Equals(object obj)
	{
		return ((InstanceID)obj).m_id == this.m_id;
	}

	public bool Equals(InstanceID obj)
	{
		return obj.m_id == this.m_id;
	}

	public override int GetHashCode()
	{
		return this.m_id.GetHashCode();
	}

	public override string ToString()
	{
		return this.m_id.ToString();
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt32(this.m_id);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_id = s.ReadUInt32();
	}

	private const uint OBJECT_TYPE = 4278190080u;

	private const uint OBJECT_INDEX = 16777215u;

	private const uint OBJECT_BUILDING = 16777216u;

	private const uint OBJECT_VEHICLE = 33554432u;

	private const uint OBJECT_DISTRICT = 50331648u;

	private const uint OBJECT_CITIZEN = 67108864u;

	private const uint OBJECT_NETNODE = 83886080u;

	private const uint OBJECT_NETSEGMENT = 100663296u;

	private const uint OBJECT_PARKEDVEHICLE = 117440512u;

	private const uint OBJECT_TRANSPORT = 134217728u;

	private const uint OBJECT_CITIZENINSTANCE = 150994944u;

	private const uint OBJECT_PROP = 167772160u;

	private const uint OBJECT_TREE = 184549376u;

	private const uint OBJECT_EVENT = 201326592u;

	private const uint OBJECT_NETLANE = 218103808u;

	private const uint OBJECT_BUILDINGPROP = 234881024u;

	private const uint OBJECT_NETLANEPROP = 251658240u;

	private const uint OBJECT_DISASTER = 268435456u;

	private const uint OBJECT_LIGHTNING = 285212672u;

	private const uint OBJECT_RADIOCHANNEL = 301989888u;

	private const uint OBJECT_RADIOCONTENT = 318767104u;

	private uint m_id;

	public static InstanceID Empty = default(InstanceID);
}
