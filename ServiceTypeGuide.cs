﻿using System;
using ColossalFramework;

public class ServiceTypeGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, ItemClass.Service service)
	{
		if (base.CanActivate(guideInfo) && service != ItemClass.Service.None)
		{
			GuideTriggerBase.ActivationInfo activationInfo = new ServiceTypeGuide.ServiceTypeActivationInfo(guideInfo, service);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class ServiceTypeActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public ServiceTypeActivationInfo(GuideInfo guideInfo, ItemClass.Service service) : base(guideInfo)
		{
			this.m_service = service;
		}

		public override string GetName()
		{
			string name = this.m_guideInfo.m_name;
			string arg = EnumExtensions.Name<ItemClass.Service>(this.m_service);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			string icon = this.m_guideInfo.m_icon;
			string arg = EnumExtensions.Name<ItemClass.Service>(this.m_service);
			return StringUtils.SafeFormat(icon, arg);
		}

		public override string FormatText(string pattern)
		{
			string arg = EnumExtensions.Name<ItemClass.Service>(this.m_service);
			return StringUtils.SafeFormat(pattern, arg);
		}

		public override IUITag GetTag()
		{
			string text = this.m_guideInfo.m_tag;
			string arg = EnumExtensions.Name<ItemClass.Service>(this.m_service);
			text = StringUtils.SafeFormat(text, arg);
			return MonoTutorialTag.Find(text);
		}

		protected ItemClass.Service m_service;
	}
}
