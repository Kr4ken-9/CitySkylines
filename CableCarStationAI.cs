﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CableCarStationAI : DepotAI
{
	public int GetPassengerCount(ushort buildingID, ref Building data)
	{
		return (int)data.m_customBuffer1;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		this.RefreshLines(buildingID, ref data);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseVehicles(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		this.RefreshLines(buildingID, ref data);
	}

	private void RefreshLines(ushort buildingID, ref Building buildingData)
	{
		if (this.m_transportLineInfo != null)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			ushort num = 0;
			ushort num2 = 0;
			ushort num3 = 0;
			ushort num4 = 0;
			ushort num5 = 0;
			ushort num6 = 0;
			ushort num7 = this.FindPathSegment(buildingID, ref buildingData);
			if (num7 != 0)
			{
				ushort num8 = 0;
				ushort num9 = 0;
				ushort num10;
				bool flag;
				Vector3 pos;
				Vector3 pos2;
				if (this.FindTargetSegment(instance.m_segments.m_buffer[(int)num7].m_startNode, num7, true, out num10, out flag))
				{
					num8 = NetSegment.FindOwnerBuilding(num10, 363f);
					if (num8 != 0 && this.GetStopPositions(num10, !flag, out pos, out pos2))
					{
						this.FindLineNode(num8, ref instance2.m_buildings.m_buffer[(int)num8], pos, pos2, true, true, out num, out num2);
					}
				}
				if (this.FindTargetSegment(instance.m_segments.m_buffer[(int)num7].m_endNode, num7, true, out num10, out flag))
				{
					num9 = NetSegment.FindOwnerBuilding(num10, 363f);
					if (num9 != 0 && this.GetStopPositions(num10, flag, out pos, out pos2))
					{
						this.FindLineNode(num9, ref instance2.m_buildings.m_buffer[(int)num9], pos, pos2, true, true, out num5, out num6);
					}
				}
				if (this.GetStopPositions(num7, false, out pos, out pos2))
				{
					this.FindLineNode(buildingID, ref buildingData, pos, pos2, num8 != 0 || num9 != 0, true, out num3, out num4);
				}
			}
			if (num3 != 0)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num3].GetSegment(i);
					if (segment != 0)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
						if (startNode == num3)
						{
							ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
							if (endNode == num5)
							{
								num5 = 0;
							}
							else
							{
								instance.ReleaseSegment(segment, true);
							}
						}
						else
						{
							ushort num11 = startNode;
							if (num11 == num)
							{
								num = 0;
							}
							else
							{
								instance.ReleaseSegment(segment, true);
							}
						}
					}
				}
				if (num != 0)
				{
					ushort num12;
					this.CreateConnectionSegment(out num12, num, num3);
				}
				if (num5 != 0)
				{
					ushort num13;
					this.CreateConnectionSegment(out num13, num3, num5);
				}
			}
			if (num4 != 0)
			{
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = instance.m_nodes.m_buffer[(int)num4].GetSegment(j);
					if (segment2 != 0)
					{
						ushort startNode2 = instance.m_segments.m_buffer[(int)segment2].m_startNode;
						if (startNode2 == num4)
						{
							ushort endNode2 = instance.m_segments.m_buffer[(int)segment2].m_endNode;
							if (endNode2 == num2)
							{
								num2 = 0;
							}
							else
							{
								instance.ReleaseSegment(segment2, true);
							}
						}
						else
						{
							ushort num14 = startNode2;
							if (num14 == num6)
							{
								num6 = 0;
							}
							else
							{
								instance.ReleaseSegment(segment2, true);
							}
						}
					}
				}
				if (num2 != 0)
				{
					ushort num15;
					this.CreateConnectionSegment(out num15, num4, num2);
				}
				if (num6 != 0)
				{
					ushort num16;
					this.CreateConnectionSegment(out num16, num6, num4);
				}
			}
		}
	}

	private bool CreateConnectionSegment(out ushort segment, ushort startNode, ushort endNode)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = position2 - position;
		vector = VectorUtils.NormalizeXZ(vector);
		if (instance.CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo, startNode, endNode, vector, -vector, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, false))
		{
			NetSegment[] expr_8F_cp_0 = instance.m_segments.m_buffer;
			ushort expr_8F_cp_1 = segment;
			expr_8F_cp_0[(int)expr_8F_cp_1].m_flags = (expr_8F_cp_0[(int)expr_8F_cp_1].m_flags | NetSegment.Flags.Untouchable);
			instance.UpdateSegment(segment);
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
			return true;
		}
		segment = 0;
		return false;
	}

	private ushort FindPathSegment(ushort buildingID, ref Building buildingData)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = buildingData.m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if ((info.m_vehicleTypes & this.m_transportInfo.m_vehicleType) != VehicleInfo.VehicleType.None && info.m_hasPedestrianLanes)
					{
						return segment;
					}
				}
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	private bool GetStopPositions(ushort segment, bool invertDirection, out Vector3 forwardPos, out Vector3 backwardPos)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
		bool flag = false;
		bool flag2 = false;
		forwardPos = Vector3.get_zero();
		backwardPos = Vector3.get_zero();
		if (info.m_lanes != null)
		{
			for (int i = 0; i < info.m_lanes.Length; i++)
			{
				int num;
				uint num2;
				if (info.m_lanes[i].m_laneType == NetInfo.LaneType.Pedestrian && (info.m_lanes[i].m_stopType & this.m_transportInfo.m_vehicleType) != VehicleInfo.VehicleType.None && instance.m_segments.m_buffer[(int)segment].GetClosestLane(i, NetInfo.LaneType.Vehicle, this.m_transportInfo.m_vehicleType, out num, out num2))
				{
					float num3 = info.m_lanes[num].m_stopOffset;
					if ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
					{
						num3 = -num3;
					}
					Vector3 vector;
					Vector3 vector2;
					instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].CalculateStopPositionAndDirection(0.5019608f, num3, out vector, out vector2);
					NetInfo.Direction direction = info.m_lanes[num].m_finalDirection;
					if (invertDirection)
					{
						direction = NetInfo.InvertDirection(direction);
					}
					if (direction == NetInfo.Direction.Forward || direction == NetInfo.Direction.AvoidBackward)
					{
						forwardPos = vector;
						flag = true;
					}
					else if (direction == NetInfo.Direction.Backward || direction == NetInfo.Direction.AvoidForward)
					{
						backwardPos = vector;
						flag2 = true;
					}
					if (flag && flag2)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	private bool FindTargetSegment(ushort startNode, ushort ignoreSegment, bool setRefreshflag, out ushort segment, out bool invertDirection)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = startNode;
		ushort num2 = ignoreSegment;
		bool flag = false;
		int num3 = 0;
		while (num != 0)
		{
			if (setRefreshflag)
			{
				NetNode[] expr_2B_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_2B_cp_1 = num;
				expr_2B_cp_0[(int)expr_2B_cp_1].m_flags = (expr_2B_cp_0[(int)expr_2B_cp_1].m_flags | NetNode.Flags.Transition);
			}
			bool flag2 = false;
			for (int i = 0; i < 8; i++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment2 != 0 && segment2 != ignoreSegment && segment2 != num2)
				{
					if ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
					{
						if (flag)
						{
							segment = segment2;
							invertDirection = (instance.m_segments.m_buffer[(int)segment2].m_endNode == num);
							return true;
						}
					}
					else
					{
						flag = true;
					}
					num = instance.m_segments.m_buffer[(int)segment2].GetOtherNode(num);
					num2 = segment2;
					flag2 = true;
					break;
				}
			}
			if (!flag2)
			{
				break;
			}
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		segment = 0;
		invertDirection = false;
		return false;
	}

	private void ReleaseLines(ushort node)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
			if (segment != 0)
			{
				instance.ReleaseSegment(segment, true);
			}
		}
	}

	private void FindLineNode(ushort buildingID, ref Building buildingData, Vector3 pos1, Vector3 pos2, bool canCreate, bool canDelete, out ushort node1, out ushort node2)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = buildingData.m_netNode;
		node1 = 0;
		node2 = 0;
		int num3 = 0;
		while (num2 != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)num2].Info;
			ushort nextBuildingNode = instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
			if (info.m_class.m_layer == ItemClass.Layer.PublicTransport && info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_subService == this.m_info.m_class.m_subService)
			{
				Vector3 position = instance.m_nodes.m_buffer[(int)num2].m_position;
				if (Vector3.SqrMagnitude(position - pos1) < 1f)
				{
					node1 = num2;
				}
				else if (Vector3.SqrMagnitude(position - pos2) < 1f)
				{
					node2 = num2;
				}
				else if (canDelete)
				{
					if (num != 0)
					{
						instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode = nextBuildingNode;
					}
					else
					{
						buildingData.m_netNode = nextBuildingNode;
					}
					this.ReleaseLines(num2);
					instance.ReleaseNode(num2);
					num2 = num;
				}
			}
			num = num2;
			num2 = nextBuildingNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (node1 == 0 && canCreate && instance.CreateNode(out node1, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo, pos1, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			NetNode[] expr_1BB_cp_0 = instance.m_nodes.m_buffer;
			ushort expr_1BB_cp_1 = node1;
			expr_1BB_cp_0[(int)expr_1BB_cp_1].m_flags = (expr_1BB_cp_0[(int)expr_1BB_cp_1].m_flags | (NetNode.Flags.Untouchable | NetNode.Flags.Fixed));
			if ((buildingData.m_flags & Building.Flags.Active) == Building.Flags.None)
			{
				NetNode[] expr_1F0_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_1F0_cp_1 = node1;
				expr_1F0_cp_0[(int)expr_1F0_cp_1].m_flags = (expr_1F0_cp_0[(int)expr_1F0_cp_1].m_flags | NetNode.Flags.Disabled);
			}
			instance.UpdateNode(node1);
			instance.m_nodes.m_buffer[(int)node1].m_nextBuildingNode = buildingData.m_netNode;
			buildingData.m_netNode = node1;
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
		}
		if (node2 == 0 && canCreate && instance.CreateNode(out node2, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo, pos2, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			NetNode[] expr_28A_cp_0 = instance.m_nodes.m_buffer;
			ushort expr_28A_cp_1 = node2;
			expr_28A_cp_0[(int)expr_28A_cp_1].m_flags = (expr_28A_cp_0[(int)expr_28A_cp_1].m_flags | (NetNode.Flags.Untouchable | NetNode.Flags.Fixed));
			if ((buildingData.m_flags & Building.Flags.Active) == Building.Flags.None)
			{
				NetNode[] expr_2BF_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_2BF_cp_1 = node2;
				expr_2BF_cp_0[(int)expr_2BF_cp_1].m_flags = (expr_2BF_cp_0[(int)expr_2BF_cp_1].m_flags | NetNode.Flags.Disabled);
			}
			instance.UpdateNode(node2);
			instance.m_nodes.m_buffer[(int)node2].m_nextBuildingNode = buildingData.m_netNode;
			buildingData.m_netNode = node2;
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
		}
	}

	private int CountLines(ushort buildingID, ref Building buildingData)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = buildingData.m_netNode;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)num].Info;
			if (info.m_class.m_layer == ItemClass.Layer.PublicTransport && info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_subService == this.m_info.m_class.m_subService)
			{
				num2 += instance.m_nodes.m_buffer[(int)num].CountSegments();
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		int num = 0;
		float num2 = 0f;
		if (this.m_transportLineInfo != null)
		{
			int num3 = this.CountLines(buildingID, ref buildingData);
			if (num3 != 0)
			{
				int num4;
				float num5;
				this.m_transportLineInfo.m_netAI.GetTransportAccumulation(out num4, out num5);
				num += num4 * num3 / 2;
				num2 = Mathf.Max(num2, num5);
			}
		}
		if (num != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (num != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PublicTransport, (float)num, num2, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			int num = 0;
			float num2 = 0f;
			if (this.m_transportLineInfo != null)
			{
				int num3 = this.CountLines(buildingID, ref buildingData);
				if (num3 != 0)
				{
					int num4;
					float num5;
					this.m_transportLineInfo.m_netAI.GetTransportAccumulation(out num4, out num5);
					num += num4 * num3 / 2;
					num2 = Mathf.Max(num2, num5);
				}
			}
			if (num != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (num != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.PublicTransport, (float)(-(float)num), num2, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	private void ReleaseVehicles(ushort buildingID, ref Building data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (instance.m_vehicles.m_buffer[(int)num].m_transportLine == 0)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				if (info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_subService == this.m_info.m_class.m_subService)
				{
					info.m_vehicleAI.SetTarget(num, ref instance.m_vehicles.m_buffer[(int)num], 0);
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		data.m_customBuffer1 = 0;
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num;
		int num2;
		this.GetVehicleCount(buildingID, ref buildingData, out num, out num2);
		NetManager instance = Singleton<NetManager>.get_instance();
		int num3 = 0;
		bool flag = false;
		float num4 = 0f;
		bool flag2 = false;
		ushort sourceNode = 0;
		ushort num5 = 0;
		ushort sourceNode2 = 0;
		ushort num6 = 0;
		ushort num7 = buildingData.m_netNode;
		int num8 = 0;
		while (num7 != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)num7].Info;
			if (info.m_class.m_layer == ItemClass.Layer.PublicTransport)
			{
				num3 += (int)instance.m_nodes.m_buffer[(int)num7].m_finalCounter;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num7].GetSegment(i);
					if (segment != 0)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
						if (startNode == num7)
						{
							num4 += instance.m_segments.m_buffer[(int)segment].m_averageLength;
							ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
							if (num5 == 0)
							{
								sourceNode = num7;
								num5 = endNode;
							}
							else if (num6 == 0)
							{
								sourceNode2 = num7;
								num6 = endNode;
							}
							if ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.PathLength) == NetSegment.Flags.None)
							{
								flag2 = true;
							}
							break;
						}
					}
				}
			}
			else if ((info.m_vehicleTypes & this.m_transportInfo.m_vehicleType) != VehicleInfo.VehicleType.None)
			{
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = instance.m_nodes.m_buffer[(int)num7].GetSegment(j);
					if (segment2 != 0)
					{
						ushort startNode2 = instance.m_segments.m_buffer[(int)segment2].m_startNode;
						if (startNode2 == num7)
						{
							CableCarStationAI.CalculateLanePassengers(instance.m_segments.m_buffer[(int)segment2].m_lanes, ref num3);
						}
					}
				}
				if ((instance.m_nodes.m_buffer[(int)num7].m_flags & NetNode.Flags.Transition) == NetNode.Flags.None)
				{
					flag = true;
				}
			}
			num7 = instance.m_nodes.m_buffer[(int)num7].m_nextBuildingNode;
			if (++num8 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int num9;
		if (flag2)
		{
			num9 = num2;
		}
		else
		{
			num9 = Mathf.CeilToInt((float)budget * num4 / (this.m_transportInfo.m_defaultVehicleDistance * 100f));
		}
		if (num5 != 0 && num < num9 && this.CreateVehicle(buildingID, ref buildingData, sourceNode, num5) != 0)
		{
			num++;
			num2++;
		}
		if (num6 != 0 && num < num9 && this.CreateVehicle(buildingID, ref buildingData, sourceNode2, num6) != 0)
		{
			num++;
			num2++;
		}
		if (num2 > num9)
		{
			int index = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num2);
			ushort activeVehicle = this.GetActiveVehicle(buildingID, ref buildingData, index);
			if (activeVehicle != 0)
			{
				VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
				if ((instance2.m_vehicles.m_buffer[(int)activeVehicle].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
				{
					VehicleInfo info2 = instance2.m_vehicles.m_buffer[(int)activeVehicle].Info;
					info2.m_vehicleAI.SetTarget(activeVehicle, ref instance2.m_vehicles.m_buffer[(int)activeVehicle], 0);
				}
			}
		}
		buildingData.m_customBuffer1 = (ushort)Mathf.Min(num3, 65535);
		if (flag)
		{
			this.RefreshLines(buildingID, ref buildingData);
		}
	}

	private ushort CreateVehicle(ushort buildingID, ref Building buildingData, ushort sourceNode, ushort targetNode)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		VehicleInfo randomVehicleInfo = instance.GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		if (randomVehicleInfo != null)
		{
			Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)sourceNode].m_position;
			ushort num;
			if (randomVehicleInfo.m_vehicleAI.CanSpawnAt(position) && instance.CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, position, this.m_transportInfo.m_vehicleReason, false, true))
			{
				randomVehicleInfo.m_vehicleAI.SetSource(num, ref instance.m_vehicles.m_buffer[(int)num], buildingID);
				randomVehicleInfo.m_vehicleAI.SetTarget(num, ref instance.m_vehicles.m_buffer[(int)num], targetNode);
				return num;
			}
		}
		return 0;
	}

	private void GetVehicleCount(ushort buildingID, ref Building buildingData, out int total, out int active)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		total = 0;
		active = 0;
		ushort num = buildingData.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			total++;
			if ((instance.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
			{
				active++;
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private ushort GetActiveVehicle(ushort buildingID, ref Building buildingData, int index)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = buildingData.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if ((instance.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && index-- == 0)
			{
				return num;
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	private static void CalculateLanePassengers(uint firstLane, ref int passengerCount)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint num = firstLane;
		int num2 = 0;
		while (num != 0u)
		{
			ushort nodes = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nodes;
			if (nodes != 0)
			{
				CableCarStationAI.CalculateLaneNodePassengers(nodes, ref passengerCount);
			}
			num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
			if (++num2 > 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private static void CalculateLaneNodePassengers(ushort firstNode, ref int passengerCount)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = firstNode;
		int num2 = 0;
		while (num != 0)
		{
			passengerCount += (int)instance.m_nodes.m_buffer[(int)num].m_finalCounter;
			num = instance.m_nodes.m_buffer[(int)num].m_nextLaneNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public override TransportInfo GetTransportLineInfo()
	{
		return this.m_transportInfo;
	}

	public override TransportInfo GetSecondaryTransportLineInfo()
	{
		return this.m_secondaryTransportInfo;
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string str = string.Empty;
		int passengerCount = this.GetPassengerCount(buildingID, ref data);
		str = str + LocaleFormatter.FormatGeneric("AIINFO_PASSENGERS_SERVICED", new object[]
		{
			passengerCount
		}) + Environment.NewLine;
		return str + base.GetLocalizedStats(buildingID, ref data);
	}

	public NetInfo m_transportLineInfo;
}
