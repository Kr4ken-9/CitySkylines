﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class DistrictManager : SimulationManagerBase<DistrictManager, DistrictProperties>, ISimulationManager, IRenderableManager
{
	public bool DistrictsVisible
	{
		get
		{
			return this.m_districtsVisible;
		}
		set
		{
			if (this.m_districtsVisible != value)
			{
				this.m_districtsVisible = value;
			}
		}
	}

	public bool NamesVisible
	{
		get
		{
			return this.m_namesVisible;
		}
		set
		{
			if (this.m_namesVisible != value)
			{
				this.m_namesVisible = value;
			}
		}
	}

	public bool DistrictsInfoVisible
	{
		get
		{
			return this.m_districtsInfoVisible;
		}
		set
		{
			if (this.m_districtsInfoVisible != value)
			{
				this.m_districtsInfoVisible = value;
			}
		}
	}

	public int HighlightDistrict
	{
		get
		{
			return this.m_highlightDistrict;
		}
		set
		{
			if (this.m_highlightDistrict != value)
			{
				this.m_highlightDistrict = value;
			}
		}
	}

	public DistrictPolicies.Policies HighlightPolicy
	{
		get
		{
			return this.m_highlightPolicy;
		}
		set
		{
			if (this.m_highlightPolicy != value)
			{
				this.m_highlightPolicy = value;
				this.AreaModified(0, 0, 511, 511, false);
			}
		}
	}

	private void RequestCharacterInfo()
	{
		if (this.m_properties == null)
		{
			return;
		}
		UIDynamicFont uIDynamicFont = this.m_properties.m_areaNameFont as UIDynamicFont;
		if (uIDynamicFont == null)
		{
			return;
		}
		if (!UIFontManager.IsDirty(uIDynamicFont))
		{
			return;
		}
		for (int i = 1; i < 128; i++)
		{
			if (this.m_districts.m_buffer[i].m_flags != District.Flags.None)
			{
				uIDynamicFont.AddCharacterRequest(this.GetDistrictName(i), 2, 0);
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Combine(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		this.m_districts = new Array8<District>(128u);
		this.m_districtGrid = new DistrictManager.Cell[262144];
		for (int i = 0; i < this.m_districtGrid.Length; i++)
		{
			this.m_districtGrid[i].m_district1 = 0;
			this.m_districtGrid[i].m_district2 = 1;
			this.m_districtGrid[i].m_district3 = 2;
			this.m_districtGrid[i].m_district4 = 3;
			this.m_districtGrid[i].m_alpha1 = 255;
			this.m_districtGrid[i].m_alpha2 = 0;
			this.m_districtGrid[i].m_alpha3 = 0;
			this.m_districtGrid[i].m_alpha4 = 0;
		}
		this.m_modifyLock = new object();
		this.m_namesVisible = true;
		this.m_districtsVisible = false;
		this.m_districtsInfoVisible = false;
		this.m_highlightDistrict = -1;
		this.m_highlightPolicy = DistrictPolicies.Policies.None;
		this.m_colorBuffer = new Color32[262144];
		this.m_distanceBuffer = new ushort[65536];
		this.m_indexBuffer = new ushort[65536];
		this.m_tempData = new DistrictManager.TempDistrictData[128];
		this.m_districtTexture1 = new Texture2D(512, 512, 5, false, true);
		this.m_districtTexture2 = new Texture2D(512, 512, 5, false, true);
		this.m_districtTexture1.set_wrapMode(1);
		this.m_districtTexture2.set_wrapMode(1);
		this.ID_Districts1 = Shader.PropertyToID("_Districts1");
		this.ID_Districts2 = Shader.PropertyToID("_Districts2");
		this.ID_DistrictMapping = Shader.PropertyToID("_DistrictMapping");
		this.ID_Highlight1 = Shader.PropertyToID("_Highlight1");
		this.ID_Highlight2 = Shader.PropertyToID("_Highlight2");
		this.m_modifiedX1 = 0;
		this.m_modifiedZ1 = 0;
		this.m_modifiedX2 = 511;
		this.m_modifiedZ2 = 511;
		this.m_fullUpdate = true;
		byte b;
		this.CreateDistrict(out b);
	}

	private void OnDestroy()
	{
		if (this.m_districtTexture1 != null)
		{
			Object.Destroy(this.m_districtTexture1);
			this.m_districtTexture1 = null;
		}
		if (this.m_districtTexture2 != null)
		{
			Object.Destroy(this.m_districtTexture2);
			this.m_districtTexture2 = null;
		}
		if (this.m_nameMesh != null)
		{
			Object.Destroy(this.m_nameMesh);
			this.m_nameMesh = null;
		}
		if (this.m_iconMesh != null)
		{
			Object.Destroy(this.m_iconMesh);
			this.m_iconMesh = null;
		}
		if (this.m_nameMaterial != null)
		{
			Object.Destroy(this.m_nameMaterial);
			this.m_nameMaterial = null;
		}
		if (this.m_iconMaterial != null)
		{
			Object.Destroy(this.m_iconMaterial);
			this.m_iconMaterial = null;
		}
		if (this.m_areaMaterial != null)
		{
			Object.Destroy(this.m_areaMaterial);
			this.m_areaMaterial = null;
		}
	}

	public override void InitializeProperties(DistrictProperties properties)
	{
		base.InitializeProperties(properties);
		this.m_nameMaterial = new Material(properties.m_areaNameShader);
		this.m_nameMaterial.CopyPropertiesFromMaterial(properties.m_areaNameFont.get_material());
		this.m_iconMaterial = new Material(properties.m_areaIconShader);
		this.m_iconMaterial.CopyPropertiesFromMaterial(properties.m_areaIconAtlas.get_material());
		this.m_areaMaterial = new Material(properties.m_areaMaterial);
		Font.add_textureRebuilt(new Action<Font>(this.UpdateNamesCallback));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	public override void DestroyProperties(DistrictProperties properties)
	{
		if (this.m_properties == properties)
		{
			Font.remove_textureRebuilt(new Action<Font>(this.UpdateNamesCallback));
			LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
			if (this.m_nameMaterial != null)
			{
				Object.Destroy(this.m_nameMaterial);
				this.m_nameMaterial = null;
			}
			if (this.m_iconMaterial != null)
			{
				Object.Destroy(this.m_iconMaterial);
				this.m_iconMaterial = null;
			}
			if (this.m_areaMaterial != null)
			{
				Object.Destroy(this.m_areaMaterial);
				this.m_areaMaterial = null;
			}
		}
		base.DestroyProperties(properties);
	}

	private void UpdateNamesCallback(Font font)
	{
		if (this.m_properties != null && this.m_properties.m_areaNameFont.get_baseFont() == font)
		{
			this.UpdateNames();
		}
	}

	private void OnLocaleChanged()
	{
		if (this.m_properties != null)
		{
			this.UpdateNames();
		}
	}

	private void UpdateNames()
	{
		UIFontManager.Invalidate(this.m_properties.m_areaNameFont);
		this.m_namesModified = false;
		UIRenderData uIRenderData = UIRenderData.Obtain();
		UIRenderData uIRenderData2 = UIRenderData.Obtain();
		try
		{
			uIRenderData.Clear();
			uIRenderData2.Clear();
			PoolList<Vector3> vertices = uIRenderData2.get_vertices();
			PoolList<Vector3> normals = uIRenderData2.get_normals();
			PoolList<Color32> colors = uIRenderData2.get_colors();
			PoolList<Vector2> uvs = uIRenderData2.get_uvs();
			PoolList<int> triangles = uIRenderData2.get_triangles();
			PoolList<Vector3> vertices2 = uIRenderData.get_vertices();
			PoolList<Vector3> normals2 = uIRenderData.get_normals();
			PoolList<Color32> colors2 = uIRenderData.get_colors();
			PoolList<Vector2> uvs2 = uIRenderData.get_uvs();
			PoolList<int> triangles2 = uIRenderData.get_triangles();
			for (int i = 1; i < 128; i++)
			{
				if (this.m_districts.m_buffer[i].m_flags != District.Flags.None)
				{
					string text = this.GetDistrictName(i);
					text += "\n";
					PositionData<DistrictPolicies.Policies>[] orderedEnumData = Utils.GetOrderedEnumData<DistrictPolicies.Policies>();
					for (int j = 0; j < orderedEnumData.Length; j++)
					{
						if (this.IsDistrictPolicySet(orderedEnumData[j].get_enumValue(), (byte)i))
						{
							string str = "IconPolicy" + orderedEnumData[j].get_enumName();
							text = text + "<sprite " + str + "> ";
						}
					}
					if (text != null)
					{
						int count = normals2.get_Count();
						int count2 = normals.get_Count();
						using (UIFontRenderer uIFontRenderer = this.m_properties.m_areaNameFont.ObtainRenderer())
						{
							UIDynamicFont.DynamicFontRenderer dynamicFontRenderer = uIFontRenderer as UIDynamicFont.DynamicFontRenderer;
							if (dynamicFontRenderer != null)
							{
								dynamicFontRenderer.set_spriteAtlas(this.m_properties.m_areaIconAtlas);
								dynamicFontRenderer.set_spriteBuffer(uIRenderData2);
							}
							float num = 450f;
							uIFontRenderer.set_defaultColor(new Color32(255, 255, 255, 64));
							uIFontRenderer.set_textScale(2f);
							uIFontRenderer.set_pixelRatio(1f);
							uIFontRenderer.set_processMarkup(true);
							uIFontRenderer.set_multiLine(true);
							uIFontRenderer.set_wordWrap(true);
							uIFontRenderer.set_textAlign(1);
							uIFontRenderer.set_maxSize(new Vector2(num, 900f));
							uIFontRenderer.set_shadow(false);
							uIFontRenderer.set_shadowColor(Color.get_black());
							uIFontRenderer.set_shadowOffset(Vector2.get_one());
							Vector2 nameSize = uIFontRenderer.MeasureString(text);
							float num2 = nameSize.x;
							if (nameSize.x > num)
							{
								num2 = num + (nameSize.x - num) * 0.5f;
								num = nameSize.x;
								uIFontRenderer.set_maxSize(new Vector2(num, 900f));
								nameSize = uIFontRenderer.MeasureString(text);
							}
							this.m_districts.m_buffer[i].m_nameSize = nameSize;
							vertices2.Add(new Vector3(-num2, -nameSize.y, 1f));
							vertices2.Add(new Vector3(-num2, nameSize.y, 1f));
							vertices2.Add(new Vector3(num2, nameSize.y, 1f));
							vertices2.Add(new Vector3(num2, -nameSize.y, 1f));
							colors2.Add(new Color32(0, 0, 0, 255));
							colors2.Add(new Color32(0, 0, 0, 255));
							colors2.Add(new Color32(0, 0, 0, 255));
							colors2.Add(new Color32(0, 0, 0, 255));
							uvs2.Add(new Vector2(-1f, -1f));
							uvs2.Add(new Vector2(-1f, 1f));
							uvs2.Add(new Vector2(1f, 1f));
							uvs2.Add(new Vector2(1f, -1f));
							triangles2.Add(vertices2.get_Count() - 4);
							triangles2.Add(vertices2.get_Count() - 3);
							triangles2.Add(vertices2.get_Count() - 1);
							triangles2.Add(vertices2.get_Count() - 1);
							triangles2.Add(vertices2.get_Count() - 3);
							triangles2.Add(vertices2.get_Count() - 2);
							uIFontRenderer.set_vectorOffset(new Vector3(num * -0.5f, nameSize.y * 0.5f, 0f));
							uIFontRenderer.Render(text, uIRenderData);
						}
						int count3 = vertices2.get_Count();
						int count4 = normals2.get_Count();
						Vector3 nameLocation = this.m_districts.m_buffer[i].m_nameLocation;
						for (int k = count; k < count4; k++)
						{
							normals2.set_Item(k, nameLocation);
						}
						for (int l = count4; l < count3; l++)
						{
							normals2.Add(nameLocation);
						}
						count3 = vertices.get_Count();
						count4 = normals.get_Count();
						for (int m = count2; m < count4; m++)
						{
							normals.set_Item(m, nameLocation);
						}
						for (int n = count4; n < count3; n++)
						{
							normals.Add(nameLocation);
						}
					}
				}
			}
			if (this.m_nameMesh == null)
			{
				this.m_nameMesh = new Mesh();
			}
			this.m_nameMesh.Clear();
			this.m_nameMesh.set_vertices(vertices2.ToArray());
			this.m_nameMesh.set_normals(normals2.ToArray());
			this.m_nameMesh.set_colors32(colors2.ToArray());
			this.m_nameMesh.set_uv(uvs2.ToArray());
			this.m_nameMesh.set_triangles(triangles2.ToArray());
			this.m_nameMesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(9830.4f, 1024f, 9830.4f)));
			if (this.m_iconMesh == null)
			{
				this.m_iconMesh = new Mesh();
			}
			this.m_iconMesh.Clear();
			this.m_iconMesh.set_vertices(vertices.ToArray());
			this.m_iconMesh.set_normals(normals.ToArray());
			this.m_iconMesh.set_colors32(colors.ToArray());
			this.m_iconMesh.set_uv(uvs.ToArray());
			this.m_iconMesh.set_triangles(triangles.ToArray());
			this.m_iconMesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(9830.4f, 1024f, 9830.4f)));
		}
		finally
		{
			uIRenderData.Release();
			uIRenderData2.Release();
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if ((this.m_districtsVisible || this.m_districtsInfoVisible) && this.m_modifiedX2 >= this.m_modifiedX1 && this.m_modifiedZ2 >= this.m_modifiedZ1)
		{
			this.UpdateTexture();
		}
		if (this.m_namesVisible && this.m_namesModified)
		{
			this.UpdateNames();
		}
	}

	public void RenderHighlight(RenderManager.CameraInfo cameraInfo, byte district, Color color)
	{
		if (district == 0)
		{
			return;
		}
		Vector3 nameLocation = this.m_districts.m_buffer[(int)district].m_nameLocation;
		Vector2 nameSize = this.m_districts.m_buffer[(int)district].m_nameSize;
		float num = Vector3.Distance(nameLocation, cameraInfo.m_position);
		float num2 = num * 0.00045f + 0.1f / (1f + num * 0.001f);
		Vector3 vector = Vector3.Normalize(new Vector3(cameraInfo.m_right.x, 0f, cameraInfo.m_right.z));
		Vector3 vector2;
		vector2..ctor(-vector.z, 0f, vector.x);
		vector *= (nameSize.x + 10f) * 0.5f * num2;
		vector2 *= (nameSize.y + 10f) * 0.5f * num2;
		Quad3 quad;
		quad.a = nameLocation - vector - vector2;
		quad.b = nameLocation + vector - vector2;
		quad.c = nameLocation + vector + vector2;
		quad.d = nameLocation - vector + vector2;
		ToolManager expr_13E_cp_0 = Singleton<ToolManager>.get_instance();
		expr_13E_cp_0.m_drawCallData.m_overlayCalls = expr_13E_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, nameLocation.y, nameLocation.y, true, true);
	}

	public void CheckOverlayAlpha(RenderManager.CameraInfo cameraInfo, byte district, ref float alpha)
	{
		if (district == 0)
		{
			return;
		}
		Vector3 nameLocation = this.m_districts.m_buffer[(int)district].m_nameLocation;
		Vector2 nameSize = this.m_districts.m_buffer[(int)district].m_nameSize;
		float num = Vector3.Distance(nameLocation, cameraInfo.m_position);
		float num2 = num * 0.00045f + 0.1f / (1f + num * 0.001f);
		float num3 = Mathf.Min(nameSize.x + 10f, nameSize.y + 10f) * num2 * 0.5f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num3)));
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if ((this.m_districtsVisible || this.m_districtsInfoVisible) && this.m_areaMaterial != null)
		{
			this.m_areaMaterial.SetTexture(this.ID_Districts1, this.m_districtTexture1);
			this.m_areaMaterial.SetTexture(this.ID_Districts2, this.m_districtTexture2);
			Vector4 vector;
			vector.z = 0.000101725258f;
			vector.x = 0.5f;
			vector.y = 0.5f;
			vector.w = ((this.m_highlightDistrict <= 0) ? 0f : 1f);
			this.m_areaMaterial.SetVector(this.ID_DistrictMapping, vector);
			Color32 color;
			color..ctor(128, 128, 128, 128);
			Color32 color2;
			color2..ctor(128, 128, 128, 128);
			this.AddDistrictColor1((byte)Mathf.Max(0, this.m_highlightDistrict), 255, ref color);
			this.AddDistrictColor2((byte)Mathf.Max(0, this.m_highlightDistrict), DistrictPolicies.Policies.None, 255, true, ref color2);
			this.m_areaMaterial.SetColor(this.ID_Highlight1, color);
			this.m_areaMaterial.SetColor(this.ID_Highlight2, color2);
			if (this.m_highlightPolicy != DistrictPolicies.Policies.None)
			{
				this.m_areaMaterial.EnableKeyword("POLICYTOOL_ON");
			}
			else
			{
				this.m_areaMaterial.DisableKeyword("POLICYTOOL_ON");
			}
			Vector3 vector2;
			vector2..ctor(0f, 512f, 0f);
			Vector3 vector3;
			vector3..ctor(9830.4f, 1024f, 9830.4f);
			Bounds bounds;
			bounds..ctor(vector2, vector3 + Vector3.get_one());
			DistrictManager expr_1C2_cp_0 = Singleton<DistrictManager>.get_instance();
			expr_1C2_cp_0.m_drawCallData.m_overlayCalls = expr_1C2_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_areaMaterial, 0, bounds);
		}
	}

	protected override void EndOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_namesVisible)
		{
			if (this.m_nameMesh != null && this.m_nameMaterial != null)
			{
				this.m_nameMaterial.set_color(this.m_properties.m_areaNameColor);
				if (this.m_nameMaterial.SetPass(0))
				{
					DistrictManager expr_5E_cp_0 = Singleton<DistrictManager>.get_instance();
					expr_5E_cp_0.m_drawCallData.m_overlayCalls = expr_5E_cp_0.m_drawCallData.m_overlayCalls + 1;
					Graphics.DrawMeshNow(this.m_nameMesh, Matrix4x4.get_identity());
				}
			}
			if (this.m_iconMesh != null && this.m_iconMaterial != null && this.m_iconMaterial.SetPass(0))
			{
				DistrictManager expr_B8_cp_0 = Singleton<DistrictManager>.get_instance();
				expr_B8_cp_0.m_drawCallData.m_overlayCalls = expr_B8_cp_0.m_drawCallData.m_overlayCalls + 1;
				Graphics.DrawMeshNow(this.m_iconMesh, Matrix4x4.get_identity());
			}
		}
	}

	private void UpdateTexture()
	{
		while (!Monitor.TryEnter(this.m_modifyLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		int num;
		int num2;
		int num3;
		int num4;
		bool fullUpdate;
		try
		{
			num = this.m_modifiedX1;
			num2 = this.m_modifiedZ1;
			num3 = this.m_modifiedX2;
			num4 = this.m_modifiedZ2;
			fullUpdate = this.m_fullUpdate;
			this.m_modifiedX1 = 10000;
			this.m_modifiedZ1 = 10000;
			this.m_modifiedX2 = -10000;
			this.m_modifiedZ2 = -10000;
			this.m_fullUpdate = false;
		}
		finally
		{
			Monitor.Exit(this.m_modifyLock);
		}
		int[] areaGrid = Singleton<GameAreaManager>.get_instance().m_areaGrid;
		int num5 = Mathf.RoundToInt(99.99999f);
		int num6 = (5 * num5 >> 1) - 256;
		if ((num3 - num + 1) * (num4 - num2 + 1) > 65536)
		{
			num = 1;
			num2 = 1;
			num3 = 510;
			num4 = 510;
			if (fullUpdate)
			{
				for (int i = num2; i <= num4; i++)
				{
					for (int j = num; j <= num3; j++)
					{
						int num7 = i * 512 + j;
						DistrictManager.Cell cell = this.m_districtGrid[num7];
						Color32 color;
						color..ctor(128, 128, 128, 128);
						this.AddDistrictColor1(cell.m_district1, cell.m_alpha1, ref color);
						this.AddDistrictColor1(cell.m_district2, cell.m_alpha2, ref color);
						this.AddDistrictColor1(cell.m_district3, cell.m_alpha3, ref color);
						this.AddDistrictColor1(cell.m_district4, cell.m_alpha4, ref color);
						this.m_colorBuffer[num7] = color;
					}
				}
				this.m_districtTexture1.SetPixels32(this.m_colorBuffer);
				this.m_districtTexture1.Apply();
			}
			for (int k = num2; k <= num4; k++)
			{
				for (int l = num; l <= num3; l++)
				{
					int num8 = k * 512 + l;
					DistrictManager.Cell cell2 = this.m_districtGrid[num8];
					bool inArea = false;
					int num9 = (l + num6) / num5;
					int num10 = (k + num6) / num5;
					if (num9 >= 0 && num9 < 5 && num10 >= 0 && num10 < 5)
					{
						inArea = (areaGrid[num10 * 5 + num9] != 0);
					}
					Color32 color2;
					color2..ctor(128, 128, 128, 128);
					this.AddDistrictColor2(cell2.m_district1, this.m_highlightPolicy, cell2.m_alpha1, inArea, ref color2);
					this.AddDistrictColor2(cell2.m_district2, this.m_highlightPolicy, cell2.m_alpha2, inArea, ref color2);
					this.AddDistrictColor2(cell2.m_district3, this.m_highlightPolicy, cell2.m_alpha3, inArea, ref color2);
					this.AddDistrictColor2(cell2.m_district4, this.m_highlightPolicy, cell2.m_alpha4, inArea, ref color2);
					this.m_colorBuffer[num8] = color2;
				}
			}
			this.m_districtTexture2.SetPixels32(this.m_colorBuffer);
			this.m_districtTexture2.Apply();
		}
		else
		{
			num = Mathf.Max(1, num);
			num2 = Mathf.Max(1, num2);
			num3 = Mathf.Min(510, num3);
			num4 = Mathf.Min(510, num4);
			for (int m = num2; m <= num4; m++)
			{
				for (int n = num; n <= num3; n++)
				{
					int num11 = m * 512 + n;
					DistrictManager.Cell cell3 = this.m_districtGrid[num11];
					if (fullUpdate)
					{
						Color32 color3;
						color3..ctor(128, 128, 128, 128);
						this.AddDistrictColor1(cell3.m_district1, cell3.m_alpha1, ref color3);
						this.AddDistrictColor1(cell3.m_district2, cell3.m_alpha2, ref color3);
						this.AddDistrictColor1(cell3.m_district3, cell3.m_alpha3, ref color3);
						this.AddDistrictColor1(cell3.m_district4, cell3.m_alpha4, ref color3);
						this.m_districtTexture1.SetPixel(n, m, color3);
					}
					bool inArea2 = false;
					int num12 = (n + num6) / num5;
					int num13 = (m + num6) / num5;
					if (num12 >= 0 && num12 < 5 && num13 >= 0 && num13 < 5)
					{
						inArea2 = (areaGrid[num13 * 5 + num12] != 0);
					}
					Color32 color4;
					color4..ctor(128, 128, 128, 128);
					this.AddDistrictColor2(cell3.m_district1, this.m_highlightPolicy, cell3.m_alpha1, inArea2, ref color4);
					this.AddDistrictColor2(cell3.m_district2, this.m_highlightPolicy, cell3.m_alpha2, inArea2, ref color4);
					this.AddDistrictColor2(cell3.m_district3, this.m_highlightPolicy, cell3.m_alpha3, inArea2, ref color4);
					this.AddDistrictColor2(cell3.m_district4, this.m_highlightPolicy, cell3.m_alpha4, inArea2, ref color4);
					this.m_districtTexture2.SetPixel(n, m, color4);
				}
			}
			if (fullUpdate)
			{
				this.m_districtTexture1.Apply();
			}
			this.m_districtTexture2.Apply();
		}
	}

	private void AddDistrictColor1(byte district, byte alpha, ref Color32 color1)
	{
		if ((district & 1) != 0)
		{
			color1.r = (byte)Mathf.Max((int)color1.r, (int)alpha);
		}
		else
		{
			color1.r = (byte)Mathf.Min((int)color1.r, (int)(255 - alpha));
		}
		if ((district & 2) != 0)
		{
			color1.g = (byte)Mathf.Max((int)color1.g, (int)alpha);
		}
		else
		{
			color1.g = (byte)Mathf.Min((int)color1.g, (int)(255 - alpha));
		}
		if ((district & 4) != 0)
		{
			color1.b = (byte)Mathf.Max((int)color1.b, (int)alpha);
		}
		else
		{
			color1.b = (byte)Mathf.Min((int)color1.b, (int)(255 - alpha));
		}
		if ((district & 8) != 0)
		{
			color1.a = (byte)Mathf.Max((int)color1.a, (int)alpha);
		}
		else
		{
			color1.a = (byte)Mathf.Min((int)color1.a, (int)(255 - alpha));
		}
	}

	private void AddDistrictColor2(byte district, DistrictPolicies.Policies policy, byte alpha, bool inArea, ref Color32 color2)
	{
		if ((district & 16) != 0)
		{
			color2.r = (byte)Mathf.Max((int)color2.r, (int)alpha);
		}
		else
		{
			color2.r = (byte)Mathf.Min((int)color2.r, (int)(255 - alpha));
		}
		if ((district & 32) != 0)
		{
			color2.g = (byte)Mathf.Max((int)color2.g, (int)alpha);
		}
		else
		{
			color2.g = (byte)Mathf.Min((int)color2.g, (int)(255 - alpha));
		}
		if ((district & 64) != 0)
		{
			color2.b = (byte)Mathf.Max((int)color2.b, (int)alpha);
		}
		else
		{
			color2.b = (byte)Mathf.Min((int)color2.b, (int)(255 - alpha));
		}
		if (policy != DistrictPolicies.Policies.None)
		{
			if (this.m_districts.m_buffer[(int)district].IsPolicySet(policy) && (inArea || district != 0))
			{
				color2.a = (byte)Mathf.Max((int)color2.a, (int)alpha);
			}
			else
			{
				color2.a = (byte)Mathf.Min((int)color2.a, (int)(255 - alpha));
			}
		}
		else
		{
			color2.a = (byte)Mathf.Min((int)color2.a, (int)(255 - alpha));
		}
	}

	public void LoadPolicy(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			this.m_industryPoliciesLoaded |= (DistrictPolicies.Specialization)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Farming:
			this.m_servicePoliciesLoaded |= (DistrictPolicies.Services)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Oil:
			this.m_taxationPoliciesLoaded |= (DistrictPolicies.Taxation)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Ore:
			this.m_cityPlanningPoliciesLoaded |= (DistrictPolicies.CityPlanning)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Leisure:
			this.m_specialPoliciesLoaded |= (DistrictPolicies.Special)(1 << (int)policy);
			return;
		case DistrictPolicies.Policies.Tourist:
			this.m_eventPoliciesLoaded |= (DistrictPolicies.Event)(1 << (int)policy);
			return;
		default:
			return;
		}
	}

	public void UnloadPolicy(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			this.m_industryPoliciesLoaded &= (DistrictPolicies.Specialization)(~(DistrictPolicies.Specialization)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Farming:
			this.m_servicePoliciesLoaded &= (DistrictPolicies.Services)(~(DistrictPolicies.Services)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Oil:
			this.m_taxationPoliciesLoaded &= (DistrictPolicies.Taxation)(~(DistrictPolicies.Taxation)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Ore:
			this.m_cityPlanningPoliciesLoaded &= (DistrictPolicies.CityPlanning)(~(DistrictPolicies.CityPlanning)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Leisure:
			this.m_specialPoliciesLoaded &= (DistrictPolicies.Special)(~(DistrictPolicies.Special)(1 << (int)policy));
			return;
		case DistrictPolicies.Policies.Tourist:
			this.m_eventPoliciesLoaded &= (DistrictPolicies.Event)(~(DistrictPolicies.Event)(1 << (int)policy));
			return;
		default:
			return;
		}
	}

	public bool CheckLimits()
	{
		return this.m_districtCount < 127;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetDistrictName(int district, string name)
	{
		DistrictManager.<SetDistrictName>c__Iterator0 <SetDistrictName>c__Iterator = new DistrictManager.<SetDistrictName>c__Iterator0();
		<SetDistrictName>c__Iterator.district = district;
		<SetDistrictName>c__Iterator.name = name;
		<SetDistrictName>c__Iterator.$this = this;
		return <SetDistrictName>c__Iterator;
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ, bool fullUpdate)
	{
		while (!Monitor.TryEnter(this.m_modifyLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_modifiedX1 = Mathf.Min(this.m_modifiedX1, minX);
			this.m_modifiedZ1 = Mathf.Min(this.m_modifiedZ1, minZ);
			this.m_modifiedX2 = Mathf.Max(this.m_modifiedX2, maxX);
			this.m_modifiedZ2 = Mathf.Max(this.m_modifiedZ2, maxZ);
			this.m_fullUpdate = (this.m_fullUpdate || fullUpdate);
		}
		finally
		{
			Monitor.Exit(this.m_modifyLock);
		}
	}

	public bool RayCast(Segment3 ray, Vector3 rayRight, out Vector3 hit, out byte district)
	{
		Vector3 vector = Vector3.Normalize(new Vector3(rayRight.x, 0f, rayRight.z));
		Vector3 vector2;
		vector2..ctor(-vector.z, 0f, vector.x);
		float num = ray.Length();
		district = 0;
		hit = Vector3.get_zero();
		float num2 = 1f;
		for (int i = 1; i < 128; i++)
		{
			if (this.m_districts.m_buffer[i].m_flags != District.Flags.None)
			{
				Vector3 nameLocation = this.m_districts.m_buffer[i].m_nameLocation;
				float num3;
				if (Segment1.Intersect(ray.a.y, ray.b.y, nameLocation.y, ref num3) && num3 < num2)
				{
					Vector3 vector3 = ray.Position(num3) - nameLocation;
					if (vector3.get_sqrMagnitude() < 122500f)
					{
						Vector2 nameSize = this.m_districts.m_buffer[i].m_nameSize;
						nameSize.x += 10f;
						nameSize.y += 10f;
						float num4 = num3 * num;
						float num5 = num4 * 0.00045f + 0.1f / (1f + num4 * 0.001f);
						float num6 = Mathf.Abs(Vector3.Dot(vector, vector3));
						float num7 = Mathf.Abs(Vector3.Dot(vector2, vector3));
						if (num6 < nameSize.x * num5 * 0.5f && num7 < nameSize.y * num5 * 0.5f)
						{
							district = (byte)i;
							hit = nameLocation;
							num2 = num3;
						}
					}
				}
			}
		}
		return district != 0;
	}

	public void NamesModified()
	{
		int num = this.m_distanceBuffer.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_distanceBuffer[i] = 0;
		}
		for (int j = 0; j < 128; j++)
		{
			this.m_tempData[j] = default(DistrictManager.TempDistrictData);
		}
		int num2 = 2;
		int num3 = 1024;
		int num4 = 0;
		int num5 = 0;
		for (int k = 0; k < 256; k++)
		{
			for (int l = 0; l < 256; l++)
			{
				int num6 = k * num3 + l * num2;
				byte district = this.m_districtGrid[num6].m_district1;
				if (district != 0 && (l == 0 || k == 0 || l == 255 || k == 255 || this.m_districtGrid[num6 - num3].m_district1 != district || this.m_districtGrid[num6 - num2].m_district1 != district || this.m_districtGrid[num6 + num2].m_district1 != district || this.m_districtGrid[num6 + num3].m_district1 != district))
				{
					int num7 = (k << 8) + l;
					this.m_distanceBuffer[num7] = 1;
					this.m_indexBuffer[num5] = (ushort)num7;
					num5 = (num5 + 1 & 65535);
					DistrictManager.TempDistrictData[] expr_16A_cp_0 = this.m_tempData;
					byte expr_16A_cp_1 = district;
					expr_16A_cp_0[(int)expr_16A_cp_1].m_averageX = expr_16A_cp_0[(int)expr_16A_cp_1].m_averageX + l;
					DistrictManager.TempDistrictData[] expr_185_cp_0 = this.m_tempData;
					byte expr_185_cp_1 = district;
					expr_185_cp_0[(int)expr_185_cp_1].m_averageZ = expr_185_cp_0[(int)expr_185_cp_1].m_averageZ + k;
					DistrictManager.TempDistrictData[] expr_1A0_cp_0 = this.m_tempData;
					byte expr_1A0_cp_1 = district;
					expr_1A0_cp_0[(int)expr_1A0_cp_1].m_divider = expr_1A0_cp_0[(int)expr_1A0_cp_1].m_divider + 1;
				}
			}
		}
		for (int m = 0; m < 128; m++)
		{
			int divider = (int)this.m_tempData[m].m_divider;
			if (divider != 0)
			{
				this.m_tempData[m].m_averageX = (this.m_tempData[m].m_averageX + (divider >> 1)) / divider;
				this.m_tempData[m].m_averageZ = (this.m_tempData[m].m_averageZ + (divider >> 1)) / divider;
			}
		}
		while (num4 != num5)
		{
			int num8 = (int)this.m_indexBuffer[num4];
			num4 = (num4 + 1 & 65535);
			int num9 = num8 & 255;
			int num10 = num8 >> 8;
			int num11 = num10 * num3 + num9 * num2;
			byte district2 = this.m_districtGrid[num11].m_district1;
			int num12 = num9 - this.m_tempData[(int)district2].m_averageX;
			int num13 = num10 - this.m_tempData[(int)district2].m_averageZ;
			int num14 = 262144 - 131072 / (int)this.m_distanceBuffer[num8] - num12 * num12 - num13 * num13;
			if (num14 > this.m_tempData[(int)district2].m_bestScore)
			{
				this.m_tempData[(int)district2].m_bestScore = num14;
				this.m_tempData[(int)district2].m_bestLocation = (ushort)num8;
			}
			int num15 = num8 - 1;
			if (num9 > 0 && this.m_distanceBuffer[num15] == 0 && this.m_districtGrid[num11 - num2].m_district1 == district2)
			{
				this.m_distanceBuffer[num15] = this.m_distanceBuffer[num8] + 1;
				this.m_indexBuffer[num5] = (ushort)num15;
				num5 = (num5 + 1 & 65535);
			}
			num15 = num8 + 1;
			if (num9 < 255 && this.m_distanceBuffer[num15] == 0 && this.m_districtGrid[num11 + num2].m_district1 == district2)
			{
				this.m_distanceBuffer[num15] = this.m_distanceBuffer[num8] + 1;
				this.m_indexBuffer[num5] = (ushort)num15;
				num5 = (num5 + 1 & 65535);
			}
			num15 = num8 - 256;
			if (num10 > 0 && this.m_distanceBuffer[num15] == 0 && this.m_districtGrid[num11 - num3].m_district1 == district2)
			{
				this.m_distanceBuffer[num15] = this.m_distanceBuffer[num8] + 1;
				this.m_indexBuffer[num5] = (ushort)num15;
				num5 = (num5 + 1 & 65535);
			}
			num15 = num8 + 256;
			if (num10 < 255 && this.m_distanceBuffer[num15] == 0 && this.m_districtGrid[num11 + num3].m_district1 == district2)
			{
				this.m_distanceBuffer[num15] = this.m_distanceBuffer[num8] + 1;
				this.m_indexBuffer[num5] = (ushort)num15;
				num5 = (num5 + 1 & 65535);
			}
		}
		for (int n = 0; n < 128; n++)
		{
			ushort bestLocation = this.m_tempData[n].m_bestLocation;
			Vector3 vector;
			vector.x = 19.2f * (float)(bestLocation & 255) * 2f - 4915.2f;
			vector.y = 0f;
			vector.z = 19.2f * (float)(bestLocation >> 8) * 2f - 4915.2f;
			vector.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector, false, 0f);
			this.m_districts.m_buffer[n].m_nameLocation = vector;
		}
		this.m_namesModified = true;
	}

	public bool IsCityPolicySet(DistrictPolicies.Policies policy)
	{
		return this.m_districts.m_buffer[0].IsPolicySet(policy);
	}

	public void SetCityPolicy(DistrictPolicies.Policies policy)
	{
		for (int i = 0; i < 128; i++)
		{
			this.m_districts.m_buffer[i].SetPolicy(policy);
		}
		this.AreaModified(0, 0, 511, 511, false);
		this.NamesModified();
		if (policy >> 5 != DistrictPolicies.Policies.Forest)
		{
			this.m_policiesNotUsed.Disable();
			this.CheckUniquePolicies();
		}
	}

	public void UnsetCityPolicy(DistrictPolicies.Policies policy)
	{
		for (int i = 0; i < 128; i++)
		{
			this.m_districts.m_buffer[i].UnsetPolicy(policy);
		}
		this.AreaModified(0, 0, 511, 511, false);
		this.NamesModified();
		if (policy >> 5 != DistrictPolicies.Policies.Forest)
		{
			this.CheckUniquePolicies();
		}
	}

	public bool IsDistrictPolicySet(DistrictPolicies.Policies policy, byte district)
	{
		return this.m_districts.m_buffer[(int)district].IsPolicySet(policy);
	}

	public void SetDistrictPolicy(DistrictPolicies.Policies policy, byte district)
	{
		this.m_districts.m_buffer[(int)district].SetPolicy(policy);
		int minX;
		int minZ;
		int maxX;
		int maxZ;
		this.GetDistrictArea(district, out minX, out minZ, out maxX, out maxZ);
		this.AreaModified(minX, minZ, maxX, maxZ, false);
		this.NamesModified();
		if (policy >> 5 != DistrictPolicies.Policies.Forest)
		{
			this.m_policiesNotUsed.Disable();
			if ((this.m_districtPoliciesSet += 1u) == 1u && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (!PlatformService.get_achievements().get_Item("Lawmaker").get_achieved())
					{
						PlatformService.get_achievements().get_Item("Lawmaker").Unlock();
					}
				});
			}
			this.CheckUniquePolicies();
		}
	}

	public void UnsetDistrictPolicy(DistrictPolicies.Policies policy, byte district)
	{
		this.m_districts.m_buffer[(int)district].UnsetPolicy(policy);
		int minX;
		int minZ;
		int maxX;
		int maxZ;
		this.GetDistrictArea(district, out minX, out minZ, out maxX, out maxZ);
		this.AreaModified(minX, minZ, maxX, maxZ, false);
		this.NamesModified();
		if (policy >> 5 != DistrictPolicies.Policies.Forest)
		{
			this.CheckUniquePolicies();
		}
	}

	public void GetDistrictArea(byte district, out int minX, out int minZ, out int maxX, out int maxZ)
	{
		minX = 10000;
		minZ = 10000;
		maxX = -10000;
		maxZ = -10000;
		for (int i = 0; i < 512; i++)
		{
			for (int j = 0; j < 512; j++)
			{
				int num = i * 512 + j;
				DistrictManager.Cell cell = this.m_districtGrid[num];
				if (cell.m_alpha1 != 0 && cell.m_district1 == district)
				{
					if (j < minX)
					{
						minX = j;
					}
					if (i < minZ)
					{
						minZ = i;
					}
					if (j > maxX)
					{
						maxX = j;
					}
					if (i > maxZ)
					{
						maxZ = i;
					}
				}
				else if (cell.m_alpha2 != 0 && cell.m_district2 == district)
				{
					if (j < minX)
					{
						minX = j;
					}
					if (i < minZ)
					{
						minZ = i;
					}
					if (j > maxX)
					{
						maxX = j;
					}
					if (i > maxZ)
					{
						maxZ = i;
					}
				}
				else if (cell.m_alpha3 != 0 && cell.m_district3 == district)
				{
					if (j < minX)
					{
						minX = j;
					}
					if (i < minZ)
					{
						minZ = i;
					}
					if (j > maxX)
					{
						maxX = j;
					}
					if (i > maxZ)
					{
						maxZ = i;
					}
				}
				else if (cell.m_alpha4 != 0 && cell.m_district4 == district)
				{
					if (j < minX)
					{
						minX = j;
					}
					if (i < minZ)
					{
						minZ = i;
					}
					if (j > maxX)
					{
						maxX = j;
					}
					if (i > maxZ)
					{
						maxZ = i;
					}
				}
			}
		}
	}

	public bool CreateDistrict(out byte district)
	{
		byte b;
		if (this.m_districts.CreateItem(out b))
		{
			district = b;
			this.m_districts.m_buffer[(int)district].m_flags = District.Flags.Created;
			this.m_districts.m_buffer[(int)district].m_totalAlpha = 0u;
			this.m_districts.m_buffer[(int)district].m_specializationPolicies = this.m_districts.m_buffer[0].m_specializationPolicies;
			this.m_districts.m_buffer[(int)district].m_specializationPoliciesEffect = DistrictPolicies.Specialization.None;
			this.m_districts.m_buffer[(int)district].m_servicePolicies = this.m_districts.m_buffer[0].m_servicePolicies;
			this.m_districts.m_buffer[(int)district].m_servicePoliciesEffect = DistrictPolicies.Services.None;
			this.m_districts.m_buffer[(int)district].m_taxationPolicies = this.m_districts.m_buffer[0].m_taxationPolicies;
			this.m_districts.m_buffer[(int)district].m_taxationPoliciesEffect = DistrictPolicies.Taxation.None;
			this.m_districts.m_buffer[(int)district].m_cityPlanningPolicies = this.m_districts.m_buffer[0].m_cityPlanningPolicies;
			this.m_districts.m_buffer[(int)district].m_cityPlanningPoliciesEffect = DistrictPolicies.CityPlanning.None;
			this.m_districts.m_buffer[(int)district].m_specialPolicies = this.m_districts.m_buffer[0].m_specialPolicies;
			this.m_districts.m_buffer[(int)district].m_specialPoliciesEffect = DistrictPolicies.Special.None;
			this.m_districts.m_buffer[(int)district].m_eventPolicies = this.m_districts.m_buffer[0].m_eventPolicies;
			this.m_districts.m_buffer[(int)district].m_eventPoliciesEffect = DistrictPolicies.Event.None;
			this.m_districts.m_buffer[(int)district].m_randomSeed = Singleton<SimulationManager>.get_instance().m_randomizer.ULong64();
			this.m_districts.m_buffer[(int)district].m_populationData = default(DistrictPopulationData);
			this.m_districts.m_buffer[(int)district].m_populationData.m_tempCount = 10000000u;
			this.m_districts.m_buffer[(int)district].m_finalHappiness = 0;
			this.m_districts.m_buffer[(int)district].m_finalCrimeRate = 0;
			this.m_districts.m_buffer[(int)district].m_Style = 0;
			this.m_districts.m_buffer[(int)district].m_nameLocation = Vector3.get_zero();
			this.m_districts.m_buffer[(int)district].m_nameSize = Vector2.get_zero();
			this.m_districts.m_buffer[(int)district].m_productionData = default(DistrictProductionData);
			this.m_districts.m_buffer[(int)district].m_groundData = default(DistrictGroundData);
			this.m_districts.m_buffer[(int)district].m_childData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_teenData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_youngData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_adultData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_seniorData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_birthData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_deathData = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_educated0Data = default(DistrictEducationData);
			this.m_districts.m_buffer[(int)district].m_educated1Data = default(DistrictEducationData);
			this.m_districts.m_buffer[(int)district].m_educated2Data = default(DistrictEducationData);
			this.m_districts.m_buffer[(int)district].m_educated3Data = default(DistrictEducationData);
			this.m_districts.m_buffer[(int)district].m_education1Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_education2Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_education3Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_student1Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_student2Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_student3Data = default(DistrictAgeData);
			this.m_districts.m_buffer[(int)district].m_residentialData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_commercialData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_visitorData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_industrialData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_officeData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_playerData = default(DistrictPrivateData);
			this.m_districts.m_buffer[(int)district].m_farmingData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_forestryData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_oreData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_oilData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_leisureData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_touristData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_organicData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_selfsufficientData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_hightechData = default(DistrictSpecializationData);
			this.m_districts.m_buffer[(int)district].m_playerConsumption = default(DistrictConsumptionData);
			this.m_districts.m_buffer[(int)district].m_residentialConsumption = default(DistrictConsumptionData);
			this.m_districts.m_buffer[(int)district].m_commercialConsumption = default(DistrictConsumptionData);
			this.m_districts.m_buffer[(int)district].m_industrialConsumption = default(DistrictConsumptionData);
			this.m_districts.m_buffer[(int)district].m_officeConsumption = default(DistrictConsumptionData);
			this.m_districts.m_buffer[(int)district].m_usageData = default(DistrictUsageData);
			this.m_districts.m_buffer[(int)district].m_importData = default(DistrictResourceData);
			this.m_districts.m_buffer[(int)district].m_exportData = default(DistrictResourceData);
			this.m_districts.m_buffer[(int)district].m_tourist1Data = default(DistrictTouristData);
			this.m_districts.m_buffer[(int)district].m_tourist2Data = default(DistrictTouristData);
			this.m_districts.m_buffer[(int)district].m_tourist3Data = default(DistrictTouristData);
			this.m_districts.m_buffer[(int)district].m_hippieData = default(DistrictSubCultureData);
			this.m_districts.m_buffer[(int)district].m_hipsterData = default(DistrictSubCultureData);
			this.m_districts.m_buffer[(int)district].m_redneckData = default(DistrictSubCultureData);
			this.m_districts.m_buffer[(int)district].m_gangstaData = default(DistrictSubCultureData);
			this.m_districtCount = (int)(this.m_districts.ItemCount() - 1u);
			if (this.m_districtCount == 3 && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (!PlatformService.get_achievements().get_Item("CityPlanner").get_achieved())
					{
						PlatformService.get_achievements().get_Item("CityPlanner").Unlock();
					}
				});
			}
			return true;
		}
		district = 0;
		return false;
	}

	private void CheckUniquePolicies()
	{
		int num = 0;
		DistrictPolicies.Services services = this.m_districts.m_buffer[0].m_servicePolicies;
		DistrictPolicies.Taxation taxation = this.m_districts.m_buffer[0].m_taxationPolicies;
		DistrictPolicies.CityPlanning cityPlanning = this.m_districts.m_buffer[0].m_cityPlanningPolicies;
		DistrictPolicies.Special special = this.m_districts.m_buffer[0].m_specialPolicies;
		DistrictPolicies.Event @event = this.m_districts.m_buffer[0].m_eventPolicies;
		for (int i = 1; i < 128; i++)
		{
			bool flag = false;
			DistrictPolicies.Services servicePolicies = this.m_districts.m_buffer[i].m_servicePolicies;
			DistrictPolicies.Taxation taxationPolicies = this.m_districts.m_buffer[i].m_taxationPolicies;
			DistrictPolicies.CityPlanning cityPlanningPolicies = this.m_districts.m_buffer[i].m_cityPlanningPolicies;
			DistrictPolicies.Special specialPolicies = this.m_districts.m_buffer[i].m_specialPolicies;
			DistrictPolicies.Event eventPolicies = this.m_districts.m_buffer[i].m_eventPolicies;
			if ((servicePolicies | services) != services)
			{
				flag = true;
				services |= servicePolicies;
			}
			if ((taxationPolicies | taxation) != taxation)
			{
				flag = true;
				taxation |= taxationPolicies;
			}
			if ((cityPlanningPolicies | cityPlanning) != cityPlanning)
			{
				flag = true;
				cityPlanning |= cityPlanningPolicies;
			}
			if ((specialPolicies | special) != special)
			{
				flag = true;
				special |= specialPolicies;
			}
			if ((eventPolicies | @event) != @event)
			{
				flag = true;
				@event |= eventPolicies;
			}
			if (flag)
			{
				num++;
			}
		}
		if (num >= 10 && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (!PlatformService.get_achievements().get_Item("Distroy").get_achieved())
				{
					PlatformService.get_achievements().get_Item("Distroy").Unlock();
				}
			});
		}
	}

	public void ReleaseDistrict(byte district)
	{
		this.ReleaseDistrictImplementation(district, ref this.m_districts.m_buffer[(int)district]);
		this.NamesModified();
	}

	public byte GetDistrict(int x, int z)
	{
		int num = z * 512 + x;
		return this.m_districtGrid[num].m_district1;
	}

	public byte GetDistrict(Vector3 worldPos)
	{
		int num = Mathf.Clamp((int)(worldPos.x / 19.2f + 256f), 0, 511);
		int num2 = Mathf.Clamp((int)(worldPos.z / 19.2f + 256f), 0, 511);
		int num3 = num2 * 512 + num;
		return this.m_districtGrid[num3].m_district1;
	}

	public byte SampleDistrict(Vector3 worldPos)
	{
		int num = Mathf.RoundToInt(worldPos.x * 13.333333f + 65536f - 128f);
		int num2 = Mathf.RoundToInt(worldPos.z * 13.333333f + 65536f - 128f);
		int num3 = Mathf.Clamp(num >> 8, 0, 511);
		int num4 = Mathf.Clamp(num2 >> 8, 0, 511);
		int num5 = Mathf.Min(num3 + 1, 511);
		int num6 = Mathf.Min(num4 + 1, 511);
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		int num13 = 0;
		this.SetBitAlphas(this.m_districtGrid[num4 * 512 + num3], (255 - (num & 255)) * (255 - (num2 & 255)), ref num7, ref num8, ref num9, ref num10, ref num11, ref num12, ref num13);
		this.SetBitAlphas(this.m_districtGrid[num4 * 512 + num5], (num & 255) * (255 - (num2 & 255)), ref num7, ref num8, ref num9, ref num10, ref num11, ref num12, ref num13);
		this.SetBitAlphas(this.m_districtGrid[num6 * 512 + num3], (255 - (num & 255)) * (num2 & 255), ref num7, ref num8, ref num9, ref num10, ref num11, ref num12, ref num13);
		this.SetBitAlphas(this.m_districtGrid[num6 * 512 + num5], (num & 255) * (num2 & 255), ref num7, ref num8, ref num9, ref num10, ref num11, ref num12, ref num13);
		byte b = 0;
		if (num7 > 0)
		{
			b |= 1;
		}
		if (num8 > 0)
		{
			b |= 2;
		}
		if (num9 > 0)
		{
			b |= 4;
		}
		if (num10 > 0)
		{
			b |= 8;
		}
		if (num11 > 0)
		{
			b |= 16;
		}
		if (num12 > 0)
		{
			b |= 32;
		}
		if (num13 > 0)
		{
			b |= 64;
		}
		return b;
	}

	private void SetBitAlphas(DistrictManager.Cell cell, int alpha, ref int b1, ref int b2, ref int b3, ref int b4, ref int b5, ref int b6, ref int b7)
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		this.SetBitAlphas((int)cell.m_district1, (int)cell.m_alpha1, ref num, ref num2, ref num3, ref num4, ref num5, ref num6, ref num7);
		this.SetBitAlphas((int)cell.m_district2, (int)cell.m_alpha2, ref num, ref num2, ref num3, ref num4, ref num5, ref num6, ref num7);
		this.SetBitAlphas((int)cell.m_district3, (int)cell.m_alpha3, ref num, ref num2, ref num3, ref num4, ref num5, ref num6, ref num7);
		this.SetBitAlphas((int)cell.m_district4, (int)cell.m_alpha4, ref num, ref num2, ref num3, ref num4, ref num5, ref num6, ref num7);
		b1 += num * alpha;
		b2 += num2 * alpha;
		b3 += num3 * alpha;
		b4 += num4 * alpha;
		b5 += num5 * alpha;
		b6 += num6 * alpha;
		b7 += num7 * alpha;
	}

	private void SetBitAlphas(int district, int alpha, ref int b1, ref int b2, ref int b3, ref int b4, ref int b5, ref int b6, ref int b7)
	{
		if ((district & 1) != 0)
		{
			b1 = Mathf.Max(b1, alpha - 128);
		}
		else
		{
			b1 = Mathf.Min(b1, 128 - alpha);
		}
		if ((district & 2) != 0)
		{
			b2 = Mathf.Max(b2, alpha - 128);
		}
		else
		{
			b2 = Mathf.Min(b2, 128 - alpha);
		}
		if ((district & 4) != 0)
		{
			b3 = Mathf.Max(b3, alpha - 128);
		}
		else
		{
			b3 = Mathf.Min(b3, 128 - alpha);
		}
		if ((district & 8) != 0)
		{
			b4 = Mathf.Max(b4, alpha - 128);
		}
		else
		{
			b4 = Mathf.Min(b4, 128 - alpha);
		}
		if ((district & 16) != 0)
		{
			b5 = Mathf.Max(b5, alpha - 128);
		}
		else
		{
			b5 = Mathf.Min(b5, 128 - alpha);
		}
		if ((district & 32) != 0)
		{
			b6 = Mathf.Max(b6, alpha - 128);
		}
		else
		{
			b6 = Mathf.Min(b6, 128 - alpha);
		}
		if ((district & 64) != 0)
		{
			b7 = Mathf.Max(b7, alpha - 128);
		}
		else
		{
			b7 = Mathf.Min(b7, 128 - alpha);
		}
	}

	public void ModifyCell(int x, int z, DistrictManager.Cell cell)
	{
		if (cell.m_alpha2 > cell.m_alpha1)
		{
			this.Exchange(ref cell.m_alpha1, ref cell.m_alpha2, ref cell.m_district1, ref cell.m_district2);
		}
		if (cell.m_alpha3 > cell.m_alpha1)
		{
			this.Exchange(ref cell.m_alpha1, ref cell.m_alpha3, ref cell.m_district1, ref cell.m_district3);
		}
		if (cell.m_alpha4 > cell.m_alpha1)
		{
			this.Exchange(ref cell.m_alpha1, ref cell.m_alpha4, ref cell.m_district1, ref cell.m_district4);
		}
		int num = z * 512 + x;
		DistrictManager.Cell cell2 = this.m_districtGrid[num];
		this.m_districtGrid[num] = cell;
		District[] expr_E4_cp_0 = this.m_districts.m_buffer;
		byte expr_E4_cp_1 = cell.m_district1;
		expr_E4_cp_0[(int)expr_E4_cp_1].m_totalAlpha = expr_E4_cp_0[(int)expr_E4_cp_1].m_totalAlpha + (uint)cell.m_alpha1;
		District[] expr_10E_cp_0 = this.m_districts.m_buffer;
		byte expr_10E_cp_1 = cell.m_district2;
		expr_10E_cp_0[(int)expr_10E_cp_1].m_totalAlpha = expr_10E_cp_0[(int)expr_10E_cp_1].m_totalAlpha + (uint)cell.m_alpha2;
		District[] expr_138_cp_0 = this.m_districts.m_buffer;
		byte expr_138_cp_1 = cell.m_district3;
		expr_138_cp_0[(int)expr_138_cp_1].m_totalAlpha = expr_138_cp_0[(int)expr_138_cp_1].m_totalAlpha + (uint)cell.m_alpha3;
		District[] expr_162_cp_0 = this.m_districts.m_buffer;
		byte expr_162_cp_1 = cell.m_district4;
		expr_162_cp_0[(int)expr_162_cp_1].m_totalAlpha = expr_162_cp_0[(int)expr_162_cp_1].m_totalAlpha + (uint)cell.m_alpha4;
		this.EraseDistrict(cell2.m_district1, ref this.m_districts.m_buffer[(int)cell2.m_district1], (uint)cell2.m_alpha1);
		this.EraseDistrict(cell2.m_district2, ref this.m_districts.m_buffer[(int)cell2.m_district2], (uint)cell2.m_alpha2);
		this.EraseDistrict(cell2.m_district3, ref this.m_districts.m_buffer[(int)cell2.m_district3], (uint)cell2.m_alpha3);
		this.EraseDistrict(cell2.m_district4, ref this.m_districts.m_buffer[(int)cell2.m_district4], (uint)cell2.m_alpha4);
	}

	private void Exchange(ref byte alpha1, ref byte alpha2, ref byte district1, ref byte district2)
	{
		byte b = alpha2;
		byte b2 = district2;
		alpha2 = alpha1;
		district2 = district1;
		alpha1 = b;
		district1 = b2;
	}

	private void EraseDistrict(byte district, ref District data, uint amount)
	{
		if (amount >= data.m_totalAlpha)
		{
			if (district == 0)
			{
				data.m_totalAlpha = 0u;
			}
			else
			{
				this.ReleaseDistrictImplementation(district, ref this.m_districts.m_buffer[(int)district]);
			}
		}
		else
		{
			data.m_totalAlpha -= amount;
		}
	}

	private void ReleaseDistrictImplementation(byte district, ref District data)
	{
		if (data.m_flags != District.Flags.None)
		{
			InstanceID id = default(InstanceID);
			id.District = district;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags = District.Flags.None;
			data.m_totalAlpha = 0u;
			this.m_districts.ReleaseItem(district);
			this.m_districtCount = (int)(this.m_districts.ItemCount() - 1u);
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num2 = num * 128 >> 8;
			int num3 = (num + 1) * 128 >> 8;
			for (int i = num2; i < num3; i++)
			{
				if ((this.m_districts.m_buffer[i].m_flags & District.Flags.Created) != District.Flags.None)
				{
					this.m_districts.m_buffer[i].SimulationStep((byte)i);
				}
			}
		}
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep <= 1 && properties != null)
		{
			int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (num4 != 75)
			{
				if (num4 == 231)
				{
					if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Policies))
					{
						this.m_policiesNotUsed.Activate(properties.m_policiesNotUsed);
					}
				}
			}
			else if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Districts))
			{
				this.m_districtsNotUsed.Activate(properties.m_districtsNotUsed);
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("DistrictManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_districtsNotUsed == null)
		{
			this.m_districtsNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_policiesNotUsed == null)
		{
			this.m_policiesNotUsed = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		base.LateUpdateData(mode);
		this.NamesModified();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new DistrictManager.Data());
	}

	public string GetDistrictName(int district)
	{
		if (this.m_districts.m_buffer[district].m_flags != District.Flags.None)
		{
			string text = null;
			if ((this.m_districts.m_buffer[district].m_flags & District.Flags.CustomName) != District.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.District = (byte)district;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateName(district);
			}
			return text;
		}
		return null;
	}

	private string GenerateName(int district)
	{
		Randomizer randomizer;
		randomizer..ctor(this.m_districts.m_buffer[district].m_randomSeed);
		string format = Locale.Get("DISTRICT_PATTERN", randomizer.Int32(Locale.Count("DISTRICT_PATTERN")));
		string arg = Locale.Get("DISTRICT_NAME", randomizer.Int32(Locale.Count("DISTRICT_NAME")));
		return StringUtils.SafeFormat(format, arg);
	}

	public bool IsAnyPolicyLoaded(DistrictPolicies.Types type)
	{
		switch (type)
		{
		case DistrictPolicies.Types.Specialization:
			return this.m_industryPoliciesLoaded != DistrictPolicies.Specialization.None;
		case DistrictPolicies.Types.Services:
			return this.m_servicePoliciesLoaded != DistrictPolicies.Services.None;
		case DistrictPolicies.Types.Taxation:
			return this.m_taxationPoliciesLoaded != DistrictPolicies.Taxation.None;
		case DistrictPolicies.Types.CityPlanning:
			return this.m_cityPlanningPoliciesLoaded != DistrictPolicies.CityPlanning.None;
		case DistrictPolicies.Types.Special:
			return this.m_specialPoliciesLoaded != DistrictPolicies.Special.None;
		case DistrictPolicies.Types.Event:
			return this.m_eventPoliciesLoaded != DistrictPolicies.Event.None;
		default:
			return false;
		}
	}

	public bool IsPolicyLoaded(DistrictPolicies.Policies policy)
	{
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			return (this.m_industryPoliciesLoaded & (DistrictPolicies.Specialization)(1 << (int)policy)) != DistrictPolicies.Specialization.None;
		case DistrictPolicies.Policies.Farming:
			return (this.m_servicePoliciesLoaded & (DistrictPolicies.Services)(1 << (int)policy)) != DistrictPolicies.Services.None;
		case DistrictPolicies.Policies.Oil:
			return (this.m_taxationPoliciesLoaded & (DistrictPolicies.Taxation)(1 << (int)policy)) != DistrictPolicies.Taxation.None;
		case DistrictPolicies.Policies.Ore:
			return (this.m_cityPlanningPoliciesLoaded & (DistrictPolicies.CityPlanning)(1 << (int)policy)) != DistrictPolicies.CityPlanning.None;
		case DistrictPolicies.Policies.Leisure:
			return (this.m_specialPoliciesLoaded & (DistrictPolicies.Special)(1 << (int)policy)) != DistrictPolicies.Special.None;
		case DistrictPolicies.Policies.Tourist:
			return (this.m_eventPoliciesLoaded & (DistrictPolicies.Event)(1 << (int)policy)) != DistrictPolicies.Event.None;
		default:
			return false;
		}
	}

	public bool IsPolicyLoaded(DistrictPolicies.Event policy)
	{
		return (this.m_eventPoliciesLoaded & policy) != DistrictPolicies.Event.None;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float DISTRICTGRID_CELL_SIZE = 19.2f;

	public const int DISTRICTGRID_RESOLUTION = 512;

	public const int MAX_DISTRICT_COUNT = 128;

	public int m_districtCount;

	[NonSerialized]
	public Array8<District> m_districts;

	[NonSerialized]
	public DistrictManager.Cell[] m_districtGrid;

	[NonSerialized]
	public uint m_nextPolicyMessageFrame1;

	[NonSerialized]
	public uint m_nextPolicyMessageFrame2;

	[NonSerialized]
	public GenericGuide m_districtsNotUsed;

	[NonSerialized]
	public GenericGuide m_policiesNotUsed;

	public DistrictStyle[] m_Styles;

	private int m_modifiedX1;

	private int m_modifiedZ1;

	private int m_modifiedX2;

	private int m_modifiedZ2;

	private bool m_fullUpdate;

	private bool m_districtsVisible;

	private bool m_namesVisible;

	private bool m_districtsInfoVisible;

	private bool m_namesModified;

	private int m_highlightDistrict;

	private object m_modifyLock;

	private DistrictPolicies.Policies m_highlightPolicy;

	private Texture2D m_districtTexture1;

	private Texture2D m_districtTexture2;

	private Color32[] m_colorBuffer;

	private ushort[] m_distanceBuffer;

	private ushort[] m_indexBuffer;

	private DistrictManager.TempDistrictData[] m_tempData;

	private Mesh m_nameMesh;

	private Mesh m_iconMesh;

	private Material m_nameMaterial;

	private Material m_iconMaterial;

	private Material m_areaMaterial;

	private int ID_Districts1;

	private int ID_Districts2;

	private int ID_DistrictMapping;

	private int ID_Highlight1;

	private int ID_Highlight2;

	private uint m_districtPoliciesSet;

	private DistrictPolicies.Specialization m_industryPoliciesLoaded;

	private DistrictPolicies.Services m_servicePoliciesLoaded;

	private DistrictPolicies.Taxation m_taxationPoliciesLoaded;

	private DistrictPolicies.CityPlanning m_cityPlanningPoliciesLoaded;

	private DistrictPolicies.Special m_specialPoliciesLoaded;

	private DistrictPolicies.Event m_eventPoliciesLoaded;

	public struct Cell
	{
		public byte m_district1;

		public byte m_district2;

		public byte m_district3;

		public byte m_district4;

		public byte m_alpha1;

		public byte m_alpha2;

		public byte m_alpha3;

		public byte m_alpha4;
	}

	private struct TempDistrictData
	{
		public int m_averageX;

		public int m_averageZ;

		public int m_bestScore;

		public short m_divider;

		public ushort m_bestLocation;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "DistrictManager");
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			District[] buffer = instance.m_districts.m_buffer;
			DistrictManager.Cell[] districtGrid = instance.m_districtGrid;
			int num = buffer.Length;
			int num2 = districtGrid.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginWrite(s);
			for (int j = 0; j < 32; j++)
			{
				for (int k = 0; k < num; k++)
				{
					@bool.Write((buffer[k].m_specializationPolicies & (DistrictPolicies.Specialization)(1 << j)) != DistrictPolicies.Specialization.None);
				}
			}
			for (int l = 0; l < 32; l++)
			{
				for (int m = 0; m < num; m++)
				{
					@bool.Write((buffer[m].m_servicePolicies & (DistrictPolicies.Services)(1 << l)) != DistrictPolicies.Services.None);
				}
			}
			for (int n = 0; n < 32; n++)
			{
				for (int num3 = 0; num3 < num; num3++)
				{
					@bool.Write((buffer[num3].m_taxationPolicies & (DistrictPolicies.Taxation)(1 << n)) != DistrictPolicies.Taxation.None);
				}
			}
			for (int num4 = 0; num4 < 32; num4++)
			{
				for (int num5 = 0; num5 < num; num5++)
				{
					@bool.Write((buffer[num5].m_cityPlanningPolicies & (DistrictPolicies.CityPlanning)(1 << num4)) != DistrictPolicies.CityPlanning.None);
				}
			}
			for (int num6 = 0; num6 < 32; num6++)
			{
				for (int num7 = 0; num7 < num; num7++)
				{
					@bool.Write((buffer[num7].m_specialPolicies & (DistrictPolicies.Special)(1 << num6)) != DistrictPolicies.Special.None);
				}
			}
			for (int num8 = 0; num8 < 32; num8++)
			{
				for (int num9 = 0; num9 < num; num9++)
				{
					@bool.Write((buffer[num9].m_specializationPoliciesEffect & (DistrictPolicies.Specialization)(1 << num8)) != DistrictPolicies.Specialization.None);
				}
			}
			for (int num10 = 0; num10 < 32; num10++)
			{
				for (int num11 = 0; num11 < num; num11++)
				{
					@bool.Write((buffer[num11].m_servicePoliciesEffect & (DistrictPolicies.Services)(1 << num10)) != DistrictPolicies.Services.None);
				}
			}
			for (int num12 = 0; num12 < 32; num12++)
			{
				for (int num13 = 0; num13 < num; num13++)
				{
					@bool.Write((buffer[num13].m_taxationPoliciesEffect & (DistrictPolicies.Taxation)(1 << num12)) != DistrictPolicies.Taxation.None);
				}
			}
			for (int num14 = 0; num14 < 32; num14++)
			{
				for (int num15 = 0; num15 < num; num15++)
				{
					@bool.Write((buffer[num15].m_cityPlanningPoliciesEffect & (DistrictPolicies.CityPlanning)(1 << num14)) != DistrictPolicies.CityPlanning.None);
				}
			}
			for (int num16 = 0; num16 < 32; num16++)
			{
				for (int num17 = 0; num17 < num; num17++)
				{
					@bool.Write((buffer[num17].m_specialPoliciesEffect & (DistrictPolicies.Special)(1 << num16)) != DistrictPolicies.Special.None);
				}
			}
			for (int num18 = 0; num18 < 32; num18++)
			{
				for (int num19 = 0; num19 < num; num19++)
				{
					@bool.Write((buffer[num19].m_eventPolicies & (DistrictPolicies.Event)(1 << num18)) != DistrictPolicies.Event.None);
				}
			}
			for (int num20 = 0; num20 < 32; num20++)
			{
				for (int num21 = 0; num21 < num; num21++)
				{
					@bool.Write((buffer[num21].m_eventPoliciesEffect & (DistrictPolicies.Event)(1 << num20)) != DistrictPolicies.Event.None);
				}
			}
			@bool.EndWrite();
			for (int num22 = 0; num22 < num; num22++)
			{
				if (buffer[num22].m_flags != District.Flags.None)
				{
					s.WriteULong64(buffer[num22].m_randomSeed);
					buffer[num22].m_populationData.Serialize(s);
					s.WriteUInt8((uint)buffer[num22].m_finalHappiness);
					s.WriteUInt8((uint)buffer[num22].m_finalCrimeRate);
					buffer[num22].m_childData.Serialize(s);
					buffer[num22].m_teenData.Serialize(s);
					buffer[num22].m_youngData.Serialize(s);
					buffer[num22].m_adultData.Serialize(s);
					buffer[num22].m_seniorData.Serialize(s);
					buffer[num22].m_residentialData.Serialize(s);
					buffer[num22].m_commercialData.Serialize(s);
					buffer[num22].m_visitorData.Serialize(s);
					buffer[num22].m_industrialData.Serialize(s);
					buffer[num22].m_officeData.Serialize(s);
					buffer[num22].m_playerData.Serialize(s);
					buffer[num22].m_farmingData.Serialize(s);
					buffer[num22].m_forestryData.Serialize(s);
					buffer[num22].m_oreData.Serialize(s);
					buffer[num22].m_oilData.Serialize(s);
					buffer[num22].m_leisureData.Serialize(s);
					buffer[num22].m_touristData.Serialize(s);
					buffer[num22].m_organicData.Serialize(s);
					buffer[num22].m_selfsufficientData.Serialize(s);
					buffer[num22].m_hightechData.Serialize(s);
					buffer[num22].m_birthData.Serialize(s);
					buffer[num22].m_deathData.Serialize(s);
					buffer[num22].m_educated0Data.Serialize(s);
					buffer[num22].m_educated1Data.Serialize(s);
					buffer[num22].m_educated2Data.Serialize(s);
					buffer[num22].m_educated3Data.Serialize(s);
					buffer[num22].m_education1Data.Serialize(s);
					buffer[num22].m_education2Data.Serialize(s);
					buffer[num22].m_education3Data.Serialize(s);
					buffer[num22].m_student1Data.Serialize(s);
					buffer[num22].m_student2Data.Serialize(s);
					buffer[num22].m_student3Data.Serialize(s);
					buffer[num22].m_productionData.Serialize(s);
					buffer[num22].m_playerConsumption.Serialize(s);
					buffer[num22].m_residentialConsumption.Serialize(s);
					buffer[num22].m_commercialConsumption.Serialize(s);
					buffer[num22].m_industrialConsumption.Serialize(s);
					buffer[num22].m_officeConsumption.Serialize(s);
					buffer[num22].m_usageData.Serialize(s);
					buffer[num22].m_groundData.Serialize(s);
					buffer[num22].m_importData.Serialize(s);
					buffer[num22].m_exportData.Serialize(s);
					buffer[num22].m_tourist1Data.Serialize(s);
					buffer[num22].m_tourist2Data.Serialize(s);
					buffer[num22].m_tourist3Data.Serialize(s);
					buffer[num22].m_hippieData.Serialize(s);
					buffer[num22].m_hipsterData.Serialize(s);
					buffer[num22].m_redneckData.Serialize(s);
					buffer[num22].m_gangstaData.Serialize(s);
					ushort style = buffer[num22].m_Style;
					s.WriteBool(style > 0);
					if (style > 0)
					{
						if ((int)style <= Singleton<DistrictManager>.get_instance().m_Styles.Length)
						{
							s.WriteUniqueString(Singleton<DistrictManager>.get_instance().m_Styles[(int)(style - 1)].FullName);
						}
						else
						{
							s.WriteUniqueString("Unknown style");
						}
					}
				}
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int num23 = 0; num23 < num2; num23++)
			{
				@byte.Write(districtGrid[num23].m_district1);
			}
			for (int num24 = 0; num24 < num2; num24++)
			{
				@byte.Write(districtGrid[num24].m_district2);
			}
			for (int num25 = 0; num25 < num2; num25++)
			{
				@byte.Write(districtGrid[num25].m_district3);
			}
			for (int num26 = 0; num26 < num2; num26++)
			{
				@byte.Write(districtGrid[num26].m_district4);
			}
			for (int num27 = 0; num27 < num2; num27++)
			{
				@byte.Write(districtGrid[num27].m_alpha1);
			}
			for (int num28 = 0; num28 < num2; num28++)
			{
				@byte.Write(districtGrid[num28].m_alpha2);
			}
			for (int num29 = 0; num29 < num2; num29++)
			{
				@byte.Write(districtGrid[num29].m_alpha3);
			}
			for (int num30 = 0; num30 < num2; num30++)
			{
				@byte.Write(districtGrid[num30].m_alpha4);
			}
			@byte.EndWrite();
			s.WriteObject<GenericGuide>(instance.m_districtsNotUsed);
			s.WriteObject<GenericGuide>(instance.m_policiesNotUsed);
			s.WriteUInt32(instance.m_nextPolicyMessageFrame1);
			s.WriteUInt32(instance.m_nextPolicyMessageFrame2);
			s.WriteUInt32(instance.m_districtPoliciesSet);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "DistrictManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "DistrictManager");
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			District[] buffer = instance.m_districts.m_buffer;
			DistrictManager.Cell[] districtGrid = instance.m_districtGrid;
			int num = buffer.Length;
			int num2 = districtGrid.Length;
			instance.m_districts.ClearUnused();
			this.m_TempStyleMap = new Dictionary<int, string>();
			if (s.get_version() < 120u)
			{
				Singleton<InstanceManager>.get_instance().ResetData();
			}
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				buffer[i].m_flags = (District.Flags)uInt.Read();
				buffer[i].m_specializationPolicies = DistrictPolicies.Specialization.None;
				buffer[i].m_specializationPoliciesEffect = DistrictPolicies.Specialization.None;
				buffer[i].m_servicePolicies = DistrictPolicies.Services.None;
				buffer[i].m_servicePoliciesEffect = DistrictPolicies.Services.None;
				buffer[i].m_taxationPolicies = DistrictPolicies.Taxation.None;
				buffer[i].m_taxationPoliciesEffect = DistrictPolicies.Taxation.None;
				buffer[i].m_cityPlanningPolicies = DistrictPolicies.CityPlanning.None;
				buffer[i].m_cityPlanningPoliciesEffect = DistrictPolicies.CityPlanning.None;
				buffer[i].m_specialPolicies = DistrictPolicies.Special.None;
				buffer[i].m_specialPoliciesEffect = DistrictPolicies.Special.None;
				buffer[i].m_eventPolicies = DistrictPolicies.Event.None;
				buffer[i].m_eventPoliciesEffect = DistrictPolicies.Event.None;
			}
			uInt.EndRead();
			if (s.get_version() >= 70u)
			{
				EncodedArray.Bool @bool = EncodedArray.Bool.BeginRead(s);
				for (int j = 0; j < 32; j++)
				{
					for (int k = 0; k < num; k++)
					{
						if (@bool.Read())
						{
							District[] expr_17E_cp_0 = buffer;
							int expr_17E_cp_1 = k;
							expr_17E_cp_0[expr_17E_cp_1].m_specializationPolicies = (expr_17E_cp_0[expr_17E_cp_1].m_specializationPolicies | (DistrictPolicies.Specialization)(1 << j));
						}
					}
				}
				for (int l = 0; l < 32; l++)
				{
					for (int m = 0; m < num; m++)
					{
						if (@bool.Read())
						{
							District[] expr_1D2_cp_0 = buffer;
							int expr_1D2_cp_1 = m;
							expr_1D2_cp_0[expr_1D2_cp_1].m_servicePolicies = (expr_1D2_cp_0[expr_1D2_cp_1].m_servicePolicies | (DistrictPolicies.Services)(1 << l));
						}
					}
				}
				for (int n = 0; n < 32; n++)
				{
					for (int num3 = 0; num3 < num; num3++)
					{
						if (@bool.Read())
						{
							District[] expr_226_cp_0 = buffer;
							int expr_226_cp_1 = num3;
							expr_226_cp_0[expr_226_cp_1].m_taxationPolicies = (expr_226_cp_0[expr_226_cp_1].m_taxationPolicies | (DistrictPolicies.Taxation)(1 << n));
						}
					}
				}
				for (int num4 = 0; num4 < 32; num4++)
				{
					for (int num5 = 0; num5 < num; num5++)
					{
						if (@bool.Read())
						{
							District[] expr_27A_cp_0 = buffer;
							int expr_27A_cp_1 = num5;
							expr_27A_cp_0[expr_27A_cp_1].m_cityPlanningPolicies = (expr_27A_cp_0[expr_27A_cp_1].m_cityPlanningPolicies | (DistrictPolicies.CityPlanning)(1 << num4));
						}
					}
				}
				for (int num6 = 0; num6 < 32; num6++)
				{
					for (int num7 = 0; num7 < num; num7++)
					{
						if (@bool.Read())
						{
							District[] expr_2CE_cp_0 = buffer;
							int expr_2CE_cp_1 = num7;
							expr_2CE_cp_0[expr_2CE_cp_1].m_specialPolicies = (expr_2CE_cp_0[expr_2CE_cp_1].m_specialPolicies | (DistrictPolicies.Special)(1 << num6));
						}
					}
				}
				if (s.get_version() >= 171u)
				{
					for (int num8 = 0; num8 < 32; num8++)
					{
						for (int num9 = 0; num9 < num; num9++)
						{
							if (@bool.Read())
							{
								District[] expr_332_cp_0 = buffer;
								int expr_332_cp_1 = num9;
								expr_332_cp_0[expr_332_cp_1].m_specializationPoliciesEffect = (expr_332_cp_0[expr_332_cp_1].m_specializationPoliciesEffect | (DistrictPolicies.Specialization)(1 << num8));
							}
						}
					}
					for (int num10 = 0; num10 < 32; num10++)
					{
						for (int num11 = 0; num11 < num; num11++)
						{
							if (@bool.Read())
							{
								District[] expr_386_cp_0 = buffer;
								int expr_386_cp_1 = num11;
								expr_386_cp_0[expr_386_cp_1].m_servicePoliciesEffect = (expr_386_cp_0[expr_386_cp_1].m_servicePoliciesEffect | (DistrictPolicies.Services)(1 << num10));
							}
						}
					}
					for (int num12 = 0; num12 < 32; num12++)
					{
						for (int num13 = 0; num13 < num; num13++)
						{
							if (@bool.Read())
							{
								District[] expr_3DA_cp_0 = buffer;
								int expr_3DA_cp_1 = num13;
								expr_3DA_cp_0[expr_3DA_cp_1].m_taxationPoliciesEffect = (expr_3DA_cp_0[expr_3DA_cp_1].m_taxationPoliciesEffect | (DistrictPolicies.Taxation)(1 << num12));
							}
						}
					}
					for (int num14 = 0; num14 < 32; num14++)
					{
						for (int num15 = 0; num15 < num; num15++)
						{
							if (@bool.Read())
							{
								District[] expr_42E_cp_0 = buffer;
								int expr_42E_cp_1 = num15;
								expr_42E_cp_0[expr_42E_cp_1].m_cityPlanningPoliciesEffect = (expr_42E_cp_0[expr_42E_cp_1].m_cityPlanningPoliciesEffect | (DistrictPolicies.CityPlanning)(1 << num14));
							}
						}
					}
					for (int num16 = 0; num16 < 32; num16++)
					{
						for (int num17 = 0; num17 < num; num17++)
						{
							if (@bool.Read())
							{
								District[] expr_482_cp_0 = buffer;
								int expr_482_cp_1 = num17;
								expr_482_cp_0[expr_482_cp_1].m_specialPoliciesEffect = (expr_482_cp_0[expr_482_cp_1].m_specialPoliciesEffect | (DistrictPolicies.Special)(1 << num16));
							}
						}
					}
				}
				if (s.get_version() >= 248u)
				{
					for (int num18 = 0; num18 < 32; num18++)
					{
						for (int num19 = 0; num19 < num; num19++)
						{
							if (@bool.Read())
							{
								District[] expr_4E6_cp_0 = buffer;
								int expr_4E6_cp_1 = num19;
								expr_4E6_cp_0[expr_4E6_cp_1].m_eventPolicies = (expr_4E6_cp_0[expr_4E6_cp_1].m_eventPolicies | (DistrictPolicies.Event)(1 << num18));
							}
						}
					}
					for (int num20 = 0; num20 < 32; num20++)
					{
						for (int num21 = 0; num21 < num; num21++)
						{
							if (@bool.Read())
							{
								District[] expr_53A_cp_0 = buffer;
								int expr_53A_cp_1 = num21;
								expr_53A_cp_0[expr_53A_cp_1].m_eventPoliciesEffect = (expr_53A_cp_0[expr_53A_cp_1].m_eventPoliciesEffect | (DistrictPolicies.Event)(1 << num20));
							}
						}
					}
				}
				@bool.EndRead();
			}
			else if (s.get_version() >= 43u)
			{
				EncodedArray.Bool bool2 = EncodedArray.Bool.BeginRead(s);
				for (int num22 = 0; num22 < 32; num22++)
				{
					for (int num23 = 0; num23 < num; num23++)
					{
						bool2.Read();
					}
				}
				bool2.EndRead();
			}
			for (int num24 = 0; num24 < num; num24++)
			{
				buffer[num24].m_totalAlpha = 0u;
				buffer[num24].m_nameLocation = Vector3.get_zero();
				buffer[num24].m_nameSize = Vector2.get_zero();
				if (buffer[num24].m_flags != District.Flags.None)
				{
					if (s.get_version() >= 56u)
					{
						buffer[num24].m_randomSeed = s.ReadULong64();
					}
					else
					{
						buffer[num24].m_randomSeed = Singleton<SimulationManager>.get_instance().m_randomizer.ULong64();
					}
					if (s.get_version() >= 62u)
					{
						buffer[num24].m_populationData.Deserialize(s);
						if (s.get_version() < 64u)
						{
							s.ReadUInt24();
						}
						buffer[num24].m_finalHappiness = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[num24].m_populationData = default(DistrictPopulationData);
						buffer[num24].m_populationData.m_tempCount = 10000000u;
						buffer[num24].m_finalHappiness = 0;
					}
					if (s.get_version() >= 203u)
					{
						buffer[num24].m_finalCrimeRate = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[num24].m_finalCrimeRate = 0;
					}
					if (s.get_version() >= 63u)
					{
						buffer[num24].m_childData.Deserialize(s);
						buffer[num24].m_teenData.Deserialize(s);
						buffer[num24].m_youngData.Deserialize(s);
						buffer[num24].m_adultData.Deserialize(s);
						buffer[num24].m_seniorData.Deserialize(s);
						buffer[num24].m_residentialData.Deserialize(s);
						buffer[num24].m_commercialData.Deserialize(s);
						if (s.get_version() >= 71u)
						{
							buffer[num24].m_visitorData.Deserialize(s);
						}
						else
						{
							buffer[num24].m_visitorData = default(DistrictPrivateData);
						}
						buffer[num24].m_industrialData.Deserialize(s);
						buffer[num24].m_officeData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_childData = default(DistrictAgeData);
						buffer[num24].m_teenData = default(DistrictAgeData);
						buffer[num24].m_youngData = default(DistrictAgeData);
						buffer[num24].m_adultData = default(DistrictAgeData);
						buffer[num24].m_seniorData = default(DistrictAgeData);
						buffer[num24].m_residentialData = default(DistrictPrivateData);
						buffer[num24].m_commercialData = default(DistrictPrivateData);
						buffer[num24].m_visitorData = default(DistrictPrivateData);
						buffer[num24].m_industrialData = default(DistrictPrivateData);
						buffer[num24].m_officeData = default(DistrictPrivateData);
					}
					if (s.get_version() >= 144u)
					{
						buffer[num24].m_playerData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_playerData = default(DistrictPrivateData);
					}
					if (s.get_version() >= 218u)
					{
						buffer[num24].m_farmingData.Deserialize(s);
						buffer[num24].m_forestryData.Deserialize(s);
						buffer[num24].m_oreData.Deserialize(s);
						buffer[num24].m_oilData.Deserialize(s);
						buffer[num24].m_leisureData.Deserialize(s);
						buffer[num24].m_touristData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_farmingData = default(DistrictSpecializationData);
						buffer[num24].m_forestryData = default(DistrictSpecializationData);
						buffer[num24].m_oreData = default(DistrictSpecializationData);
						buffer[num24].m_oilData = default(DistrictSpecializationData);
						buffer[num24].m_leisureData = default(DistrictSpecializationData);
						buffer[num24].m_touristData = default(DistrictSpecializationData);
					}
					if (s.get_version() >= 109006u)
					{
						buffer[num24].m_organicData.Deserialize(s);
						buffer[num24].m_selfsufficientData.Deserialize(s);
						buffer[num24].m_hightechData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_organicData = default(DistrictSpecializationData);
						buffer[num24].m_selfsufficientData = default(DistrictSpecializationData);
						buffer[num24].m_hightechData = default(DistrictSpecializationData);
					}
					if (s.get_version() >= 167u)
					{
						buffer[num24].m_birthData.Deserialize(s);
						buffer[num24].m_deathData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_birthData = default(DistrictAgeData);
						buffer[num24].m_deathData = default(DistrictAgeData);
					}
					if (s.get_version() >= 67u)
					{
						buffer[num24].m_educated0Data.Deserialize(s);
						buffer[num24].m_educated1Data.Deserialize(s);
						buffer[num24].m_educated2Data.Deserialize(s);
						buffer[num24].m_educated3Data.Deserialize(s);
					}
					else
					{
						buffer[num24].m_educated0Data = default(DistrictEducationData);
						buffer[num24].m_educated1Data = default(DistrictEducationData);
						buffer[num24].m_educated2Data = default(DistrictEducationData);
						buffer[num24].m_educated3Data = default(DistrictEducationData);
					}
					if (s.get_version() >= 113u)
					{
						buffer[num24].m_education1Data.Deserialize(s);
						buffer[num24].m_education2Data.Deserialize(s);
						buffer[num24].m_education3Data.Deserialize(s);
					}
					else
					{
						buffer[num24].m_education1Data = default(DistrictAgeData);
						buffer[num24].m_education2Data = default(DistrictAgeData);
						buffer[num24].m_education3Data = default(DistrictAgeData);
					}
					if (s.get_version() >= 143u)
					{
						buffer[num24].m_student1Data.Deserialize(s);
						buffer[num24].m_student2Data.Deserialize(s);
						buffer[num24].m_student3Data.Deserialize(s);
					}
					else
					{
						buffer[num24].m_student1Data = default(DistrictAgeData);
						buffer[num24].m_student2Data = default(DistrictAgeData);
						buffer[num24].m_student3Data = default(DistrictAgeData);
					}
					if (s.get_version() >= 92u)
					{
						buffer[num24].m_productionData.Deserialize(s);
						buffer[num24].m_playerConsumption.Deserialize(s);
						buffer[num24].m_residentialConsumption.Deserialize(s);
						buffer[num24].m_commercialConsumption.Deserialize(s);
						buffer[num24].m_industrialConsumption.Deserialize(s);
						buffer[num24].m_officeConsumption.Deserialize(s);
					}
					else
					{
						buffer[num24].m_productionData = default(DistrictProductionData);
						buffer[num24].m_playerConsumption = default(DistrictConsumptionData);
						buffer[num24].m_residentialConsumption = default(DistrictConsumptionData);
						buffer[num24].m_commercialConsumption = default(DistrictConsumptionData);
						buffer[num24].m_industrialConsumption = default(DistrictConsumptionData);
						buffer[num24].m_officeConsumption = default(DistrictConsumptionData);
					}
					if (s.get_version() >= 239u)
					{
						buffer[num24].m_usageData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_usageData = default(DistrictUsageData);
					}
					if (s.get_version() >= 97u)
					{
						buffer[num24].m_groundData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_groundData = default(DistrictGroundData);
					}
					if (s.get_version() >= 118u)
					{
						buffer[num24].m_importData.Deserialize(s);
						buffer[num24].m_exportData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_importData = default(DistrictResourceData);
						buffer[num24].m_exportData = default(DistrictResourceData);
					}
					if (s.get_version() >= 119u)
					{
						buffer[num24].m_tourist1Data.Deserialize(s);
						buffer[num24].m_tourist2Data.Deserialize(s);
						buffer[num24].m_tourist3Data.Deserialize(s);
					}
					else
					{
						buffer[num24].m_tourist1Data = default(DistrictTouristData);
						buffer[num24].m_tourist2Data = default(DistrictTouristData);
						buffer[num24].m_tourist3Data = default(DistrictTouristData);
					}
					if (s.get_version() >= 207u)
					{
						buffer[num24].m_hippieData.Deserialize(s);
						buffer[num24].m_hipsterData.Deserialize(s);
						buffer[num24].m_redneckData.Deserialize(s);
						buffer[num24].m_gangstaData.Deserialize(s);
					}
					else
					{
						buffer[num24].m_hippieData = default(DistrictSubCultureData);
						buffer[num24].m_hipsterData = default(DistrictSubCultureData);
						buffer[num24].m_redneckData = default(DistrictSubCultureData);
						buffer[num24].m_gangstaData = default(DistrictSubCultureData);
					}
					if (s.get_version() >= 217u)
					{
						bool flag = s.ReadBool();
						if (flag)
						{
							this.m_TempStyleMap.Add(num24, s.ReadUniqueString());
						}
						buffer[num24].m_Style = 0;
					}
					else
					{
						buffer[num24].m_Style = 0;
					}
					if (s.get_version() < 120u)
					{
						InstanceID id = default(InstanceID);
						id.District = (byte)num24;
						Singleton<InstanceManager>.get_instance().SetName(id, s.ReadUniqueString());
					}
				}
				else if (num24 == 0)
				{
					District[] expr_10F0_cp_0 = buffer;
					int expr_10F0_cp_1 = num24;
					expr_10F0_cp_0[expr_10F0_cp_1].m_flags = (expr_10F0_cp_0[expr_10F0_cp_1].m_flags | District.Flags.Created);
				}
				else
				{
					instance.m_districts.ReleaseItem((byte)num24);
				}
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int num25 = 0; num25 < num2; num25++)
			{
				districtGrid[num25].m_district1 = @byte.Read();
			}
			for (int num26 = 0; num26 < num2; num26++)
			{
				districtGrid[num26].m_district2 = @byte.Read();
			}
			for (int num27 = 0; num27 < num2; num27++)
			{
				districtGrid[num27].m_district3 = @byte.Read();
			}
			for (int num28 = 0; num28 < num2; num28++)
			{
				districtGrid[num28].m_district4 = @byte.Read();
			}
			for (int num29 = 0; num29 < num2; num29++)
			{
				districtGrid[num29].m_alpha1 = @byte.Read();
			}
			for (int num30 = 0; num30 < num2; num30++)
			{
				districtGrid[num30].m_alpha2 = @byte.Read();
			}
			for (int num31 = 0; num31 < num2; num31++)
			{
				districtGrid[num31].m_alpha3 = @byte.Read();
			}
			for (int num32 = 0; num32 < num2; num32++)
			{
				districtGrid[num32].m_alpha4 = @byte.Read();
			}
			@byte.EndRead();
			if (s.get_version() >= 156u)
			{
				instance.m_districtsNotUsed = s.ReadObject<GenericGuide>();
				instance.m_policiesNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_districtsNotUsed = null;
				instance.m_policiesNotUsed = null;
			}
			if (s.get_version() >= 148u)
			{
				instance.m_nextPolicyMessageFrame1 = s.ReadUInt32();
				instance.m_nextPolicyMessageFrame2 = s.ReadUInt32();
			}
			else
			{
				instance.m_nextPolicyMessageFrame1 = 0u;
				instance.m_nextPolicyMessageFrame2 = 0u;
			}
			if (s.get_version() >= 178u)
			{
				instance.m_districtPoliciesSet = s.ReadUInt32();
			}
			else
			{
				instance.m_districtPoliciesSet = 0u;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "DistrictManager");
		}

		private ushort FindStyleByName(string name)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			if (instance.m_Styles != null)
			{
				ushort num = 0;
				while ((int)num < instance.m_Styles.Length)
				{
					if (instance.m_Styles[(int)num].FullName.Equals(name))
					{
						return num + 1;
					}
					num += 1;
				}
			}
			CODebugBase<LogChannel>.Warn(LogChannel.Modding, "Warning: Unknown district style: " + name);
			return 0;
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "DistrictManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			District[] buffer = instance.m_districts.m_buffer;
			DistrictManager.Cell[] districtGrid = instance.m_districtGrid;
			int num = districtGrid.Length;
			for (int i = 0; i < num; i++)
			{
				DistrictManager.Cell cell = districtGrid[i];
				District[] expr_60_cp_0 = buffer;
				byte expr_60_cp_1 = cell.m_district1;
				expr_60_cp_0[(int)expr_60_cp_1].m_totalAlpha = expr_60_cp_0[(int)expr_60_cp_1].m_totalAlpha + (uint)cell.m_alpha1;
				District[] expr_80_cp_0 = buffer;
				byte expr_80_cp_1 = cell.m_district2;
				expr_80_cp_0[(int)expr_80_cp_1].m_totalAlpha = expr_80_cp_0[(int)expr_80_cp_1].m_totalAlpha + (uint)cell.m_alpha2;
				District[] expr_A0_cp_0 = buffer;
				byte expr_A0_cp_1 = cell.m_district3;
				expr_A0_cp_0[(int)expr_A0_cp_1].m_totalAlpha = expr_A0_cp_0[(int)expr_A0_cp_1].m_totalAlpha + (uint)cell.m_alpha3;
				District[] expr_C0_cp_0 = buffer;
				byte expr_C0_cp_1 = cell.m_district4;
				expr_C0_cp_0[(int)expr_C0_cp_1].m_totalAlpha = expr_C0_cp_0[(int)expr_C0_cp_1].m_totalAlpha + (uint)cell.m_alpha4;
			}
			foreach (int current in this.m_TempStyleMap.Keys)
			{
				instance.m_districts.m_buffer[current].m_Style = this.FindStyleByName(this.m_TempStyleMap[current]);
			}
			instance.m_districtCount = (int)(instance.m_districts.ItemCount() - 1u);
			instance.AreaModified(0, 0, 511, 511, true);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "DistrictManager");
		}

		private Dictionary<int, string> m_TempStyleMap;
	}
}
