﻿using System;
using ColossalFramework;
using UnityEngine;

public struct RadioChannelData
{
	public RadioChannelInfo Info
	{
		get
		{
			return PrefabCollection<RadioChannelInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public void SimulationTick(ushort channelIndex)
	{
		if (this.m_currentContent != 0)
		{
			this.m_playPosition = (ushort)Mathf.Min((int)(this.m_playPosition + 1), 65535);
			int num = (int)(this.m_playPosition * 256 / 60);
			AudioManager instance = Singleton<AudioManager>.get_instance();
			RadioContentInfo info = instance.m_radioContents.m_buffer[(int)this.m_currentContent].Info;
			if (num >= info.m_lengthSeconds && (this.m_flags & RadioChannelData.Flags.Active) == RadioChannelData.Flags.None)
			{
				this.ChangeContent(channelIndex);
			}
		}
		else
		{
			this.ChangeContent(channelIndex);
		}
		this.UpdateNext(channelIndex);
	}

	public void ChangeContent(ushort channelIndex)
	{
		AudioManager instance = Singleton<AudioManager>.get_instance();
		ushort currentContent = this.m_currentContent;
		if (this.m_currentContent != 0)
		{
			RadioContentData[] expr_2E_cp_0 = instance.m_radioContents.m_buffer;
			ushort expr_2E_cp_1 = this.m_currentContent;
			expr_2E_cp_0[(int)expr_2E_cp_1].m_flags = (expr_2E_cp_0[(int)expr_2E_cp_1].m_flags & ~RadioContentData.Flags.Playing);
			this.m_currentContent = 0;
		}
		if ((this.m_flags & RadioChannelData.Flags.PlayDefault) == RadioChannelData.Flags.None)
		{
			if (this.m_nextContent != 0)
			{
				this.m_currentContent = this.m_nextContent;
				this.m_nextContent = 0;
				this.m_playPosition = 0;
				RadioContentData[] expr_8B_cp_0 = instance.m_radioContents.m_buffer;
				ushort expr_8B_cp_1 = this.m_currentContent;
				expr_8B_cp_0[(int)expr_8B_cp_1].m_flags = (expr_8B_cp_0[(int)expr_8B_cp_1].m_flags & ~RadioContentData.Flags.Queued);
				RadioContentData[] expr_AF_cp_0 = instance.m_radioContents.m_buffer;
				ushort expr_AF_cp_1 = this.m_currentContent;
				expr_AF_cp_0[(int)expr_AF_cp_1].m_flags = (expr_AF_cp_0[(int)expr_AF_cp_1].m_flags | RadioContentData.Flags.Playing);
			}
			else
			{
				RadioContentInfo radioContentInfo = this.FindNextContentInfo(currentContent);
				if (radioContentInfo != null)
				{
					if (instance.CreateRadioContent(out this.m_currentContent, radioContentInfo))
					{
						this.m_playPosition = 0;
						RadioContentData[] expr_FE_cp_0 = instance.m_radioContents.m_buffer;
						ushort expr_FE_cp_1 = this.m_currentContent;
						expr_FE_cp_0[(int)expr_FE_cp_1].m_flags = (expr_FE_cp_0[(int)expr_FE_cp_1].m_flags | RadioContentData.Flags.Playing);
					}
				}
				else
				{
					this.m_flags |= RadioChannelData.Flags.PlayDefault;
				}
			}
		}
		this.UpdateNext(channelIndex);
	}

	private void UpdateNext(ushort channelIndex)
	{
		if ((this.m_flags & RadioChannelData.Flags.PlayDefault) == RadioChannelData.Flags.None && this.m_nextContent == 0)
		{
			AudioManager instance = Singleton<AudioManager>.get_instance();
			RadioContentInfo radioContentInfo = this.FindNextContentInfo(this.m_currentContent);
			if (radioContentInfo != null)
			{
				if (instance.CreateRadioContent(out this.m_nextContent, radioContentInfo))
				{
					RadioContentData[] expr_59_cp_0 = instance.m_radioContents.m_buffer;
					ushort expr_59_cp_1 = this.m_nextContent;
					expr_59_cp_0[(int)expr_59_cp_1].m_flags = (expr_59_cp_0[(int)expr_59_cp_1].m_flags | RadioContentData.Flags.Queued);
				}
			}
			else
			{
				this.m_flags |= RadioChannelData.Flags.PlayDefault;
			}
		}
	}

	private void Initialize()
	{
		if ((this.m_flags & RadioChannelData.Flags.Initialized) == RadioChannelData.Flags.None)
		{
			RadioChannelInfo info = this.Info;
			if (info.m_stateChain != null && info.m_stateChain.Length != 0)
			{
				this.m_stateChainIndex = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)info.m_stateChain.Length);
				int num = 1;
				uint num2 = 0u;
				while ((ulong)num2 < (ulong)((long)info.m_stateChain.Length))
				{
					if (num2 != 0u && (int)(this.m_stateChainIndex += 1) >= info.m_stateChain.Length)
					{
						this.m_stateChainIndex = 0;
					}
					int num3 = Mathf.Clamp(info.m_stateChain[(int)this.m_stateChainIndex].m_minCount, 1, 256);
					int num4 = Mathf.Clamp(info.m_stateChain[(int)this.m_stateChainIndex].m_maxCount, num3, 256);
					num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num3, num4);
					if (num > 0)
					{
						break;
					}
					num2 += 1u;
				}
				this.m_stateRepeatCount = (byte)(num - 1);
				this.m_stateRepeatCount = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(0, (int)this.m_stateRepeatCount);
			}
			this.m_flags |= RadioChannelData.Flags.Initialized;
		}
	}

	private RadioContentInfo FindNextContentInfo(ushort previousContent)
	{
		this.Initialize();
		RadioChannelInfo info = this.Info;
		RadioContentInfo.ContentType type = RadioContentInfo.ContentType.Music;
		if (info.m_stateChain != null && info.m_stateChain.Length != 0)
		{
			if ((int)this.m_stateChainIndex >= info.m_stateChain.Length)
			{
				this.m_stateChainIndex = 0;
			}
			if (this.m_stateRepeatCount != 0)
			{
				this.m_stateRepeatCount -= 1;
			}
			else
			{
				int num = 1;
				uint num2 = 0u;
				while ((ulong)num2 < (ulong)((long)info.m_stateChain.Length))
				{
					if ((int)(this.m_stateChainIndex += 1) >= info.m_stateChain.Length)
					{
						this.m_stateChainIndex = 0;
					}
					int num3 = Mathf.Clamp(info.m_stateChain[(int)this.m_stateChainIndex].m_minCount, 1, 256);
					int num4 = Mathf.Clamp(info.m_stateChain[(int)this.m_stateChainIndex].m_maxCount, num3, 256);
					num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num3, num4);
					if (num > 0)
					{
						break;
					}
					num2 += 1u;
				}
				this.m_stateRepeatCount = (byte)(num - 1);
			}
			type = info.m_stateChain[(int)this.m_stateChainIndex].m_contentType;
		}
		int num5 = 0;
		if (previousContent != 0)
		{
			RadioContentInfo info2 = Singleton<AudioManager>.get_instance().m_radioContents.m_buffer[(int)previousContent].Info;
			if (info2 != null)
			{
				num5 = info2.m_groupIndex;
			}
		}
		FastList<ushort> fastList = Singleton<AudioManager>.get_instance().CollectRadioContentInfo(type, info);
		int num6 = 0;
		int num7 = 0;
		for (int i = 0; i < fastList.m_size; i++)
		{
			ushort index = fastList.m_buffer[i];
			RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)index);
			int cooldown = prefab.m_cooldown;
			if (num5 == 0 || num5 != prefab.m_groupIndex)
			{
				num6 += Mathf.Clamp(cooldown - 150, 0, 350);
			}
			num7 += Mathf.Clamp(cooldown, 1, 500);
		}
		if (num6 != 0)
		{
			num6 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num6);
			for (int j = 0; j < fastList.m_size; j++)
			{
				ushort index2 = fastList.m_buffer[j];
				RadioContentInfo prefab2 = PrefabCollection<RadioContentInfo>.GetPrefab((uint)index2);
				if (num5 == 0 || num5 != prefab2.m_groupIndex)
				{
					int num8 = Mathf.Clamp(prefab2.m_cooldown - 150, 0, 350);
					if (num6 < num8)
					{
						return prefab2;
					}
					num6 -= num8;
				}
			}
		}
		else if (num7 != 0)
		{
			num7 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num7);
			for (int k = 0; k < fastList.m_size; k++)
			{
				ushort index3 = fastList.m_buffer[k];
				RadioContentInfo prefab3 = PrefabCollection<RadioContentInfo>.GetPrefab((uint)index3);
				int num9 = Mathf.Clamp(prefab3.m_cooldown, 1, 500);
				if (num7 < num9)
				{
					return prefab3;
				}
				num7 -= num9;
			}
		}
		return null;
	}

	public RadioChannelData.Flags m_flags;

	public ushort m_infoIndex;

	public ushort m_currentContent;

	public ushort m_nextContent;

	public ushort m_playPosition;

	public byte m_stateChainIndex;

	public byte m_stateRepeatCount;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Initialized = 4,
		PlayDefault = 8,
		Active = 16,
		All = -1
	}
}
