﻿using System;
using ColossalFramework;
using UnityEngine;

public class HelicopterDanglingAI : VehicleAI
{
	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation, scale);
		return result;
	}

	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (data.m_leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort firstVehicle = data.GetFirstVehicle(vehicleID);
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)firstVehicle].Info;
			return info.m_vehicleAI.GetColor(firstVehicle, ref instance.m_vehicles.m_buffer[(int)firstVehicle], infoMode);
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_frame0.m_swayPosition = data.m_frame0.m_position;
		data.m_frame0.m_swayVelocity = data.m_frame0.m_velocity;
		data.m_frame1.m_swayPosition = data.m_frame1.m_position;
		data.m_frame1.m_swayVelocity = data.m_frame1.m_velocity;
		data.m_frame2.m_swayPosition = data.m_frame2.m_position;
		data.m_frame2.m_swayVelocity = data.m_frame2.m_velocity;
		data.m_frame3.m_swayPosition = data.m_frame3.m_position;
		data.m_frame3.m_swayVelocity = data.m_frame3.m_velocity;
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		data.m_frame0.m_swayPosition = data.m_frame0.m_position;
		data.m_frame0.m_swayVelocity = data.m_frame0.m_velocity;
		data.m_frame1.m_swayPosition = data.m_frame1.m_position;
		data.m_frame1.m_swayVelocity = data.m_frame1.m_velocity;
		data.m_frame2.m_swayPosition = data.m_frame2.m_position;
		data.m_frame2.m_swayVelocity = data.m_frame2.m_velocity;
		data.m_frame3.m_swayPosition = data.m_frame3.m_position;
		data.m_frame3.m_swayVelocity = data.m_frame3.m_velocity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if (vehicleData.m_leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort leadingVehicle = vehicleData.m_leadingVehicle;
			frameData.m_position += frameData.m_velocity * 0.4f;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.4f;
			frameData.m_swayVelocity *= 0.001f;
			frameData.m_swayVelocity.y = frameData.m_swayVelocity.y - 10f;
			frameData.m_swayPosition.y = frameData.m_swayPosition.y - 10f;
			Vehicle.Flags flags = instance.m_vehicles.m_buffer[(int)leadingVehicle].m_flags;
			Vehicle.Frame lastFrameData = instance.m_vehicles.m_buffer[(int)leadingVehicle].GetLastFrameData();
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)leadingVehicle].Info;
			Vehicle.Flags flags2 = Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.GoingBack | Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.LeftHandDrive;
			vehicleData.m_flags = ((vehicleData.m_flags & ~flags2) | (flags & flags2));
			float attachOffsetBack = info.m_attachOffsetBack;
			Vector3 vector = lastFrameData.m_position + lastFrameData.m_rotation * new Vector3(0f, attachOffsetBack, 0f);
			Vector3 vector2 = vector - frameData.m_swayPosition;
			float magnitude = vector2.get_magnitude();
			Vector3 vector3;
			if (magnitude > 0.1f)
			{
				float attachOffsetFront = this.m_info.m_attachOffsetFront;
				vector3 = vector2 * (1f + (attachOffsetFront - this.m_info.m_generatedInfo.m_size.y) / magnitude);
			}
			else
			{
				vector3 = Vector3.get_zero();
			}
			Vector3 vector4 = vector3 / 0.6f;
			float sqrMagnitude = vector4.get_sqrMagnitude();
			if (sqrMagnitude > 0.01f)
			{
				float num = Vector3.Dot(vector4, frameData.m_swayVelocity) / sqrMagnitude;
				frameData.m_swayVelocity += vector4 * Mathf.Clamp01(1f - num);
			}
			Vector3 vector5 = frameData.m_swayPosition + vector3;
			frameData.m_velocity = (vector5 - frameData.m_position) / 0.6f;
			frameData.m_position = vector5;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.6f;
			frameData.m_steerAngle = 0f;
			frameData.m_travelDistance = 0f;
			frameData.m_lightIntensity = lastFrameData.m_lightIntensity;
			frameData.m_underground = lastFrameData.m_underground;
			frameData.m_transition = lastFrameData.m_transition;
			vehicleData.m_targetPos0 = lastFrameData.m_position;
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = leaderData.m_targetPos0;
			vehicleData.m_targetPos2 = leaderData.m_targetPos1;
			vehicleData.m_targetPos3 = leaderData.m_targetPos2;
			if (magnitude > 0.1f)
			{
				Vector3 vector6 = Vector3.Cross(lastFrameData.m_rotation * Vector3.get_right(), vector2);
				if (vector6.get_sqrMagnitude() > 0.01f)
				{
					frameData.m_rotation = Quaternion.LookRotation(vector6, vector2);
				}
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.4f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}
}
