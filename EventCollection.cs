﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class EventCollection : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(EventCollection.InitializePrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading(base.get_gameObject().get_name());
		PrefabCollection<EventInfo>.DestroyPrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private static IEnumerator InitializePrefabs(string name, EventInfo[] prefabs, string[] replaces)
	{
		EventCollection.<InitializePrefabs>c__Iterator0 <InitializePrefabs>c__Iterator = new EventCollection.<InitializePrefabs>c__Iterator0();
		<InitializePrefabs>c__Iterator.name = name;
		<InitializePrefabs>c__Iterator.prefabs = prefabs;
		<InitializePrefabs>c__Iterator.replaces = replaces;
		return <InitializePrefabs>c__Iterator;
	}

	public EventInfo[] m_prefabs;

	public string[] m_replacedNames;
}
