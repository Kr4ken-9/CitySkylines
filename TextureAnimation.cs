﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

internal class TextureAnimation : MonoBehaviour
{
	public float RunTimeInSeconds
	{
		get
		{
			return 1f / this.FramesPerSecond * (float)(this.Columns * this.Rows);
		}
	}

	private void Start()
	{
		this.materialCopy = new Material(base.GetComponent<Renderer>().get_sharedMaterial());
		base.GetComponent<Renderer>().set_sharedMaterial(this.materialCopy);
		Vector2 vector;
		vector..ctor(1f / (float)this.Columns, 1f / (float)this.Rows);
		base.GetComponent<Renderer>().get_sharedMaterial().SetTextureScale("_MainTex", vector);
	}

	private void OnEnable()
	{
		base.StartCoroutine(this.UpdateTiling());
	}

	[DebuggerHidden]
	private IEnumerator UpdateTiling()
	{
		TextureAnimation.<UpdateTiling>c__Iterator0 <UpdateTiling>c__Iterator = new TextureAnimation.<UpdateTiling>c__Iterator0();
		<UpdateTiling>c__Iterator.$this = this;
		return <UpdateTiling>c__Iterator;
	}

	public int Columns = 2;

	public int Rows = 2;

	public float FramesPerSecond = 10f;

	public bool RunOnce = true;

	private Material materialCopy;
}
