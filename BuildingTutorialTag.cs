﻿using System;
using ColossalFramework;
using UnityEngine;

public class BuildingTutorialTag : IUITag
{
	public BuildingTutorialTag(ushort id)
	{
		this.m_ID = id;
	}

	public bool isDynamic
	{
		get
		{
			return true;
		}
	}

	public bool isValid
	{
		get
		{
			return true;
		}
	}

	public bool isRelative
	{
		get
		{
			return true;
		}
	}

	public bool locationLess
	{
		get
		{
			return false;
		}
	}

	public Vector3 position
	{
		get
		{
			return Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_ID].m_position;
		}
	}

	private ushort m_ID;
}
