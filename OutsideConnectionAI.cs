﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class OutsideConnectionAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Connections)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
		}
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			return this.m_info.m_color0;
		}
		return base.GetColor(buildingID, ref data, infoMode);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		data.m_flags |= Building.Flags.Active;
		Singleton<BuildingManager>.get_instance().AddOutsideConnection(buildingID);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		data.m_flags |= Building.Flags.Active;
		Singleton<BuildingManager>.get_instance().AddOutsideConnection(buildingID);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		OutsideConnectionAI.RemoveConnectionOffers(buildingID, ref data, this.m_dummyTrafficReason);
		Singleton<BuildingManager>.get_instance().RemoveOutsideConnection(buildingID);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public static void RemoveConnectionOffers(ushort buildingID, ref Building data, TransferManager.TransferReason dummyTrafficReason)
	{
		TransferManager instance = Singleton<TransferManager>.get_instance();
		if ((data.m_flags & Building.Flags.Outgoing) != Building.Flags.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Ore, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Oil, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Grain, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Logs, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Goods, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Coal, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Petrol, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Food, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Lumber, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single0, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single1, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single2, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single3, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single0B, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single1B, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single2B, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Single3B, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Family0, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Family1, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Family2, offer);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Family3, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Shopping, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingB, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingC, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingD, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingE, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingF, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingG, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.ShoppingH, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Entertainment, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.EntertainmentB, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.EntertainmentC, offer);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.EntertainmentD, offer);
			if (dummyTrafficReason != TransferManager.TransferReason.None)
			{
				instance.RemoveOutgoingOffer(dummyTrafficReason, offer);
			}
		}
		if ((data.m_flags & Building.Flags.Incoming) != Building.Flags.None)
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Building = buildingID;
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Ore, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Oil, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Grain, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Logs, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Goods, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Coal, offer2);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Petrol, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Food, offer2);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.Lumber, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single0, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single1, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single2, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single3, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single0B, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single1B, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single2B, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Single3B, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Family0, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Family1, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Family2, offer2);
			instance.RemoveIncomingOffer(TransferManager.TransferReason.Family3, offer2);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.LeaveCity0, offer2);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.LeaveCity1, offer2);
			instance.RemoveOutgoingOffer(TransferManager.TransferReason.LeaveCity2, offer2);
			if (dummyTrafficReason != TransferManager.TransferReason.None)
			{
				instance.RemoveIncomingOffer(dummyTrafficReason, offer2);
			}
		}
	}

	public override void PathfindSuccess(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		OutsideConnectionAI.PathfindSuccessImpl(buildingID, ref data, type);
	}

	public override void PathfindFailure(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		OutsideConnectionAI.PathfindFailureImpl(buildingID, ref data, type);
	}

	public static void PathfindSuccessImpl(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		switch (type)
		{
		case BuildingAI.PathFindType.EnteringCargo:
			data.m_education3 = (byte)Mathf.Min(255, (int)(data.m_education3 + 2));
			break;
		case BuildingAI.PathFindType.LeavingCargo:
			data.m_teens = (byte)Mathf.Min(255, (int)(data.m_teens + 2));
			break;
		case BuildingAI.PathFindType.EnteringHuman:
			data.m_workerProblemTimer = (byte)Mathf.Min(255, (int)(data.m_workerProblemTimer + 2));
			break;
		case BuildingAI.PathFindType.LeavingHuman:
			data.m_incomingProblemTimer = (byte)Mathf.Min(255, (int)(data.m_incomingProblemTimer + 2));
			break;
		case BuildingAI.PathFindType.EnteringDummy:
			data.m_outgoingProblemTimer = (byte)Mathf.Min(255, (int)(data.m_outgoingProblemTimer + 2));
			break;
		case BuildingAI.PathFindType.LeavingDummy:
			data.m_youngs = (byte)Mathf.Min(255, (int)(data.m_youngs + 2));
			break;
		case BuildingAI.PathFindType.VirtualCargo:
			if (data.m_education3 < 4)
			{
				data.m_education3 += 1;
			}
			break;
		}
	}

	public static void PathfindFailureImpl(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		switch (type)
		{
		case BuildingAI.PathFindType.EnteringCargo:
			data.m_adults = (byte)Mathf.Min(255, (int)(data.m_adults + 2));
			break;
		case BuildingAI.PathFindType.LeavingCargo:
			data.m_serviceProblemTimer = (byte)Mathf.Min(255, (int)(data.m_serviceProblemTimer + 2));
			break;
		case BuildingAI.PathFindType.EnteringHuman:
			data.m_taxProblemTimer = (byte)Mathf.Min(255, (int)(data.m_taxProblemTimer + 2));
			break;
		case BuildingAI.PathFindType.LeavingHuman:
			data.m_seniors = (byte)Mathf.Min(255, (int)(data.m_seniors + 2));
			break;
		case BuildingAI.PathFindType.EnteringDummy:
			data.m_education1 = (byte)Mathf.Min(255, (int)(data.m_education1 + 2));
			break;
		case BuildingAI.PathFindType.LeavingDummy:
			data.m_education2 = (byte)Mathf.Min(255, (int)(data.m_education2 + 2));
			break;
		}
	}

	private static int TickPathfindStatus(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		switch (type)
		{
		case BuildingAI.PathFindType.EnteringCargo:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_education3, ref data.m_adults);
		case BuildingAI.PathFindType.LeavingCargo:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_teens, ref data.m_serviceProblemTimer);
		case BuildingAI.PathFindType.EnteringHuman:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_workerProblemTimer, ref data.m_taxProblemTimer);
		case BuildingAI.PathFindType.LeavingHuman:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_incomingProblemTimer, ref data.m_seniors);
		case BuildingAI.PathFindType.EnteringDummy:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_outgoingProblemTimer, ref data.m_education1);
		case BuildingAI.PathFindType.LeavingDummy:
			return OutsideConnectionAI.TickPathfindStatus(ref data.m_youngs, ref data.m_education2);
		default:
			return 0;
		}
	}

	private static int TickPathfindStatus(ref byte success, ref byte failure)
	{
		int result = ((int)success << 8) / Mathf.Max(1, (int)(success + failure));
		if (success > failure)
		{
			success = (byte)(success + 1 >> 1);
			failure = (byte)(failure >> 1);
		}
		else
		{
			success = (byte)(success >> 1);
			failure = (byte)(failure + 1 >> 1);
		}
		return result;
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (!OutsideConnectionAI.StartConnectionTransfer(buildingID, ref data, material, offer, this.m_touristFactor0, this.m_touristFactor1, this.m_touristFactor2))
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public static bool StartConnectionTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer, int touristFactor0, int touristFactor1, int touristFactor2)
	{
		bool flag = false;
		int amount = offer.Amount;
		offer.Amount = 1;
		for (int i = 0; i < amount; i++)
		{
			flag |= OutsideConnectionAI.StartConnectionTransferImpl(buildingID, ref data, material, offer, touristFactor0, touristFactor1, touristFactor2);
		}
		return flag;
	}

	private static bool StartConnectionTransferImpl(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer, int touristFactor0, int touristFactor1, int touristFactor2)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		VehicleInfo vehicleInfo = null;
		Citizen.Education education = Citizen.Education.Uneducated;
		int num = 0;
		bool flag = false;
		bool flag2 = false;
		if (material == TransferManager.TransferReason.DummyCar)
		{
			ushort building = offer.Building;
			if (building != 0)
			{
				Vector3 position = instance.m_buildings.m_buffer[(int)building].m_position;
				if (Vector3.SqrMagnitude(position - data.m_position) > 40000f)
				{
					flag2 = true;
					switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(34u))
					{
					case 0:
						material = TransferManager.TransferReason.Ore;
						break;
					case 1:
						material = TransferManager.TransferReason.Coal;
						break;
					case 2:
						material = TransferManager.TransferReason.Oil;
						break;
					case 3:
						material = TransferManager.TransferReason.Petrol;
						break;
					case 4:
						material = TransferManager.TransferReason.Grain;
						break;
					case 5:
						material = TransferManager.TransferReason.Food;
						break;
					case 6:
						material = TransferManager.TransferReason.Logs;
						break;
					case 7:
						material = TransferManager.TransferReason.Lumber;
						break;
					case 8:
						material = TransferManager.TransferReason.Goods;
						break;
					case 9:
						material = TransferManager.TransferReason.Goods;
						break;
					case 10:
						material = TransferManager.TransferReason.Single0;
						break;
					case 11:
						material = TransferManager.TransferReason.Single1;
						break;
					case 12:
						material = TransferManager.TransferReason.Single2;
						break;
					case 13:
						material = TransferManager.TransferReason.Single3;
						break;
					case 14:
						material = TransferManager.TransferReason.Single0B;
						break;
					case 15:
						material = TransferManager.TransferReason.Single1B;
						break;
					case 16:
						material = TransferManager.TransferReason.Single2B;
						break;
					case 17:
						material = TransferManager.TransferReason.Single3B;
						break;
					case 18:
						material = TransferManager.TransferReason.Family0;
						break;
					case 19:
						material = TransferManager.TransferReason.Family1;
						break;
					case 20:
						material = TransferManager.TransferReason.Family2;
						break;
					case 21:
						material = TransferManager.TransferReason.Family3;
						break;
					case 22:
						material = TransferManager.TransferReason.Shopping;
						break;
					case 23:
						material = TransferManager.TransferReason.ShoppingB;
						break;
					case 24:
						material = TransferManager.TransferReason.ShoppingC;
						break;
					case 25:
						material = TransferManager.TransferReason.ShoppingD;
						break;
					case 26:
						material = TransferManager.TransferReason.ShoppingE;
						break;
					case 27:
						material = TransferManager.TransferReason.ShoppingF;
						break;
					case 28:
						material = TransferManager.TransferReason.ShoppingG;
						break;
					case 29:
						material = TransferManager.TransferReason.ShoppingH;
						break;
					case 30:
						material = TransferManager.TransferReason.Entertainment;
						break;
					case 31:
						material = TransferManager.TransferReason.EntertainmentB;
						break;
					case 32:
						material = TransferManager.TransferReason.EntertainmentC;
						break;
					case 33:
						material = TransferManager.TransferReason.EntertainmentD;
						break;
					}
				}
			}
		}
		switch (material)
		{
		case TransferManager.TransferReason.Oil:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOil, ItemClass.Level.Level2);
			goto IL_597;
		case TransferManager.TransferReason.Ore:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOre, ItemClass.Level.Level2);
			goto IL_597;
		case TransferManager.TransferReason.Logs:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialForestry, ItemClass.Level.Level2);
			goto IL_597;
		case TransferManager.TransferReason.Grain:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialFarming, ItemClass.Level.Level2);
			goto IL_597;
		case TransferManager.TransferReason.Goods:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level1);
			goto IL_597;
		case TransferManager.TransferReason.Coal:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOre, ItemClass.Level.Level1);
			goto IL_597;
		case TransferManager.TransferReason.Family0:
			education = Citizen.Education.Uneducated;
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2, 5);
			goto IL_597;
		case TransferManager.TransferReason.Family1:
			education = Citizen.Education.OneSchool;
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2, 5);
			goto IL_597;
		case TransferManager.TransferReason.Family2:
			education = Citizen.Education.TwoSchools;
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2, 5);
			goto IL_597;
		case TransferManager.TransferReason.Family3:
			education = Citizen.Education.ThreeSchools;
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2, 5);
			goto IL_597;
		case TransferManager.TransferReason.Single0:
		case TransferManager.TransferReason.Single0B:
			education = Citizen.Education.Uneducated;
			num = 1;
			goto IL_597;
		case TransferManager.TransferReason.Single1:
		case TransferManager.TransferReason.Single1B:
			education = Citizen.Education.OneSchool;
			num = 1;
			goto IL_597;
		case TransferManager.TransferReason.Single2:
		case TransferManager.TransferReason.Single2B:
			education = Citizen.Education.TwoSchools;
			num = 1;
			goto IL_597;
		case TransferManager.TransferReason.Single3:
		case TransferManager.TransferReason.Single3B:
			education = Citizen.Education.ThreeSchools;
			num = 1;
			goto IL_597;
		case TransferManager.TransferReason.Shopping:
		case TransferManager.TransferReason.Entertainment:
		case TransferManager.TransferReason.ShoppingB:
		case TransferManager.TransferReason.ShoppingC:
		case TransferManager.TransferReason.ShoppingD:
		case TransferManager.TransferReason.ShoppingE:
		case TransferManager.TransferReason.ShoppingF:
		case TransferManager.TransferReason.ShoppingG:
		case TransferManager.TransferReason.ShoppingH:
		case TransferManager.TransferReason.EntertainmentB:
		case TransferManager.TransferReason.EntertainmentC:
		case TransferManager.TransferReason.EntertainmentD:
			flag = true;
			goto IL_597;
		case TransferManager.TransferReason.Petrol:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOil, ItemClass.Level.Level1);
			goto IL_597;
		case TransferManager.TransferReason.Food:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialFarming, ItemClass.Level.Level1);
			goto IL_597;
		case TransferManager.TransferReason.Lumber:
			vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, ItemClass.SubService.IndustrialForestry, ItemClass.Level.Level1);
			goto IL_597;
		case TransferManager.TransferReason.DummyTrain:
			if (offer.Building != buildingID)
			{
				flag2 = true;
				if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
				{
					vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, ItemClass.Level.Level4);
				}
				else
				{
					vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, ItemClass.Level.Level1);
				}
			}
			goto IL_597;
		case TransferManager.TransferReason.DummyShip:
			if (offer.Building != buildingID)
			{
				flag2 = true;
				if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
				{
					vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, ItemClass.Level.Level4);
				}
				else
				{
					vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, ItemClass.Level.Level1);
				}
			}
			goto IL_597;
		case TransferManager.TransferReason.DummyPlane:
			if (offer.Building != buildingID)
			{
				flag2 = true;
				vehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, ItemClass.Level.Level1);
			}
			goto IL_597;
		}
		return false;
		IL_597:
		if (num != 0)
		{
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			ushort building2 = offer.Building;
			if (building2 != 0)
			{
				uint num2 = 0u;
				if (!flag2)
				{
					num2 = instance.m_buildings.m_buffer[(int)building2].GetEmptyCitizenUnit(CitizenUnit.Flags.Home);
				}
				if (num2 != 0u || flag2)
				{
					int family = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u);
					ushort num3 = 0;
					Citizen.Gender gender = Citizen.Gender.Male;
					for (int i = 0; i < num; i++)
					{
						uint num4 = 0u;
						int num5 = (i >= 2) ? 0 : 90;
						int num6 = (i >= 2) ? 15 : 105;
						int age = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num5, num6);
						Citizen.Education education2 = (i >= 2) ? Citizen.Education.Uneducated : education;
						bool flag4;
						if (i == 1)
						{
							bool flag3 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < 5;
							Citizen.Gender gender2;
							if (flag3)
							{
								gender2 = gender;
							}
							else
							{
								gender2 = ((gender != Citizen.Gender.Male) ? Citizen.Gender.Male : Citizen.Gender.Female);
							}
							flag4 = instance2.CreateCitizen(out num4, age, family, ref Singleton<SimulationManager>.get_instance().m_randomizer, gender2);
						}
						else
						{
							flag4 = instance2.CreateCitizen(out num4, age, family, ref Singleton<SimulationManager>.get_instance().m_randomizer);
						}
						if (!flag4)
						{
							break;
						}
						if (i == 0)
						{
							gender = Citizen.GetGender(num4);
						}
						if (education2 >= Citizen.Education.OneSchool)
						{
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education1 = true;
						}
						if (education2 >= Citizen.Education.TwoSchools)
						{
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education1 = true;
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education2 = true;
						}
						if (education2 >= Citizen.Education.ThreeSchools)
						{
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education1 = true;
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education2 = true;
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].Education3 = true;
						}
						if (flag2)
						{
							Citizen[] expr_7B9_cp_0 = instance2.m_citizens.m_buffer;
							UIntPtr expr_7B9_cp_1 = (UIntPtr)num4;
							expr_7B9_cp_0[(int)expr_7B9_cp_1].m_flags = (expr_7B9_cp_0[(int)expr_7B9_cp_1].m_flags | Citizen.Flags.DummyTraffic);
						}
						else
						{
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].SetHome(num4, 0, num2);
						}
						Citizen[] expr_7FE_cp_0 = instance2.m_citizens.m_buffer;
						UIntPtr expr_7FE_cp_1 = (UIntPtr)num4;
						expr_7FE_cp_0[(int)expr_7FE_cp_1].m_flags = (expr_7FE_cp_0[(int)expr_7FE_cp_1].m_flags | Citizen.Flags.MovingIn);
						CitizenInfo citizenInfo = instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].GetCitizenInfo(num4);
						ushort num7;
						if (citizenInfo != null && instance2.CreateCitizenInstance(out num7, ref Singleton<SimulationManager>.get_instance().m_randomizer, citizenInfo, num4))
						{
							if (num3 == 0)
							{
								citizenInfo.m_citizenAI.SetSource(num7, ref instance2.m_instances.m_buffer[(int)num7], buildingID);
								citizenInfo.m_citizenAI.SetTarget(num7, ref instance2.m_instances.m_buffer[(int)num7], building2);
								num3 = num7;
							}
							else
							{
								citizenInfo.m_citizenAI.SetSource(num7, ref instance2.m_instances.m_buffer[(int)num7], buildingID);
								citizenInfo.m_citizenAI.JoinTarget(num7, ref instance2.m_instances.m_buffer[(int)num7], num3);
							}
							instance2.m_citizens.m_buffer[(int)((UIntPtr)num4)].CurrentLocation = Citizen.Location.Moving;
						}
					}
				}
			}
		}
		if (flag)
		{
			CitizenManager instance3 = Singleton<CitizenManager>.get_instance();
			ushort building3 = offer.Building;
			if (building3 != 0)
			{
				int family2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u);
				uint num8 = 0u;
				if (!flag2)
				{
					num8 = instance.m_buildings.m_buffer[(int)building3].GetEmptyCitizenUnit(CitizenUnit.Flags.Visit);
				}
				if (num8 != 0u || flag2)
				{
					int age2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(45, 240);
					Citizen.Wealth wealth = Citizen.Wealth.High;
					int num9 = touristFactor0 + touristFactor1 + touristFactor2;
					if (num9 != 0)
					{
						int num10 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num9);
						if (num10 < touristFactor0)
						{
							wealth = Citizen.Wealth.Low;
						}
						else if (num10 < touristFactor0 + touristFactor1)
						{
							wealth = Citizen.Wealth.Medium;
						}
					}
					uint num11;
					if (instance3.CreateCitizen(out num11, age2, family2, ref Singleton<SimulationManager>.get_instance().m_randomizer))
					{
						Citizen[] expr_A0E_cp_0 = instance3.m_citizens.m_buffer;
						UIntPtr expr_A0E_cp_1 = (UIntPtr)num11;
						expr_A0E_cp_0[(int)expr_A0E_cp_1].m_flags = (expr_A0E_cp_0[(int)expr_A0E_cp_1].m_flags | Citizen.Flags.Tourist);
						Citizen[] expr_A2F_cp_0 = instance3.m_citizens.m_buffer;
						UIntPtr expr_A2F_cp_1 = (UIntPtr)num11;
						expr_A2F_cp_0[(int)expr_A2F_cp_1].m_flags = (expr_A2F_cp_0[(int)expr_A2F_cp_1].m_flags | Citizen.Flags.MovingIn);
						instance3.m_citizens.m_buffer[(int)((UIntPtr)num11)].WealthLevel = wealth;
						if (flag2)
						{
							Citizen[] expr_A73_cp_0 = instance3.m_citizens.m_buffer;
							UIntPtr expr_A73_cp_1 = (UIntPtr)num11;
							expr_A73_cp_0[(int)expr_A73_cp_1].m_flags = (expr_A73_cp_0[(int)expr_A73_cp_1].m_flags | Citizen.Flags.DummyTraffic);
						}
						else
						{
							instance3.m_citizens.m_buffer[(int)((UIntPtr)num11)].SetVisitplace(num11, 0, num8);
						}
						CitizenInfo citizenInfo2 = instance3.m_citizens.m_buffer[(int)((UIntPtr)num11)].GetCitizenInfo(num11);
						ushort num12;
						if (citizenInfo2 != null && instance3.CreateCitizenInstance(out num12, ref Singleton<SimulationManager>.get_instance().m_randomizer, citizenInfo2, num11))
						{
							citizenInfo2.m_citizenAI.SetSource(num12, ref instance3.m_instances.m_buffer[(int)num12], buildingID);
							citizenInfo2.m_citizenAI.SetTarget(num12, ref instance3.m_instances.m_buffer[(int)num12], building3);
							instance3.m_citizens.m_buffer[(int)((UIntPtr)num11)].CurrentLocation = Citizen.Location.Moving;
						}
						if (!flag2)
						{
							StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticArray>(StatisticType.IncomingTourists);
							statisticBase = statisticBase.Acquire<StatisticInt32>((int)wealth, 3);
							statisticBase.Add(1);
						}
					}
				}
			}
		}
		if (vehicleInfo != null)
		{
			Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
			ushort num13;
			if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num13, ref Singleton<SimulationManager>.get_instance().m_randomizer, vehicleInfo, data.m_position, material, false, true))
			{
				if (flag2)
				{
					Vehicle[] expr_BB7_cp_0 = vehicles.m_buffer;
					ushort expr_BB7_cp_1 = num13;
					expr_BB7_cp_0[(int)expr_BB7_cp_1].m_flags = (expr_BB7_cp_0[(int)expr_BB7_cp_1].m_flags | Vehicle.Flags.DummyTraffic);
					Vehicle[] expr_BD6_cp_0 = vehicles.m_buffer;
					ushort expr_BD6_cp_1 = num13;
					expr_BD6_cp_0[(int)expr_BD6_cp_1].m_flags = (expr_BD6_cp_0[(int)expr_BD6_cp_1].m_flags & ~Vehicle.Flags.WaitingCargo);
				}
				vehicleInfo.m_vehicleAI.SetSource(num13, ref vehicles.m_buffer[(int)num13], buildingID);
				vehicleInfo.m_vehicleAI.StartTransfer(num13, ref vehicles.m_buffer[(int)num13], material, offer);
				if (!flag2)
				{
					ushort building4 = offer.Building;
					if (building4 != 0)
					{
						int amount;
						int num14;
						vehicleInfo.m_vehicleAI.GetSize(num13, ref vehicles.m_buffer[(int)num13], out amount, out num14);
						OutsideConnectionAI.ImportResource(building4, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building4], material, amount);
					}
				}
			}
		}
		return true;
	}

	private static void GetRandomLanePos(ushort buildingID, ref Building data, ref Randomizer randomizer, NetInfo.LaneType laneType, NetInfo.Direction direction, out Vector3 position, out Vector3 target)
	{
		ushort num = data.FindParentNode(buildingID);
		if (num == 0)
		{
			position = data.m_position;
			target = data.m_position;
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info.m_lanes != null)
				{
					bool flag = (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
					bool flag2 = instance.m_segments.m_buffer[(int)segment].m_startNode != num;
					NetInfo.Direction direction2;
					if (flag != flag2)
					{
						direction2 = NetInfo.InvertDirection(direction);
					}
					else
					{
						direction2 = direction;
					}
					int num2 = 0;
					for (int j = 0; j < info.m_lanes.Length; j++)
					{
						if (info.m_lanes[j].m_laneType == laneType && (byte)(info.m_lanes[j].m_finalDirection & direction2) != 0)
						{
							num2++;
						}
					}
					if (num2 != 0)
					{
						num2 = randomizer.Int32((uint)num2);
						uint num3 = instance.m_segments.m_buffer[(int)segment].m_lanes;
						int num4 = 0;
						while (num4 < info.m_lanes.Length && num3 != 0u)
						{
							if (info.m_lanes[num4].m_laneType == laneType && (byte)(info.m_lanes[num4].m_finalDirection & direction2) != 0 && num2-- == 0)
							{
								if (flag2)
								{
									position = instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_bezier.d;
									target = position + (instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_bezier.c - position).get_normalized();
								}
								else
								{
									position = instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_bezier.a;
									target = position + (instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_bezier.b - position).get_normalized();
								}
								return;
							}
							num3 = instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_nextLane;
							num4++;
						}
					}
				}
			}
		}
		position = data.m_position;
		target = data.m_position;
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		OutsideConnectionAI.GetRandomLanePos(buildingID, ref data, ref randomizer, NetInfo.LaneType.Vehicle, NetInfo.Direction.Forward, out position, out target);
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		OutsideConnectionAI.GetRandomLanePos(buildingID, ref data, ref randomizer, NetInfo.LaneType.Vehicle, NetInfo.Direction.Forward, out position, out target);
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		OutsideConnectionAI.GetRandomLanePos(buildingID, ref data, ref randomizer, NetInfo.LaneType.Vehicle, NetInfo.Direction.Backward, out position, out target);
		direction = Vector2.get_zero();
		specialFlags = CitizenInstance.Flags.None;
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		OutsideConnectionAI.GetRandomLanePos(buildingID, ref data, ref randomizer, NetInfo.LaneType.Vehicle, NetInfo.Direction.Backward, out position, out target);
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
	}

	public override void SimulationStep(ushort buildingID, ref Building data)
	{
		base.SimulationStep(buildingID, ref data);
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			int productionRate = OutsideConnectionAI.GetProductionRate(100, budget);
			OutsideConnectionAI.AddConnectionOffers(buildingID, ref data, productionRate, this.m_cargoCapacity, this.m_residentCapacity, this.m_touristFactor0, this.m_touristFactor1, this.m_touristFactor2, this.m_dummyTrafficReason, this.m_dummyTrafficFactor);
		}
	}

	public static void AddConnectionOffers(ushort buildingID, ref Building data, int productionRate, int cargoCapacity, int residentCapacity, int touristFactor0, int touristFactor1, int touristFactor2, TransferManager.TransferReason dummyTrafficReason, int dummyTrafficFactor)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		TransferManager instance2 = Singleton<TransferManager>.get_instance();
		cargoCapacity = (cargoCapacity * productionRate + 99) / 100;
		residentCapacity = (residentCapacity * productionRate + 99) / 100;
		touristFactor0 = (touristFactor0 * productionRate + 99) / 100;
		touristFactor1 = (touristFactor1 * productionRate + 99) / 100;
		touristFactor2 = (touristFactor2 * productionRate + 99) / 100;
		dummyTrafficFactor = (dummyTrafficFactor * productionRate + 99) / 100;
		dummyTrafficFactor = (dummyTrafficFactor * OutsideConnectionAI.DummyTrafficProbability() + 99) / 100;
		int num = (cargoCapacity + instance.m_randomizer.Int32(16u)) / 16;
		int num2 = (residentCapacity + instance.m_randomizer.Int32(16u)) / 16;
		if ((data.m_flags & Building.Flags.Outgoing) != Building.Flags.None)
		{
			int num3 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.LeavingHuman);
			int num4 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.LeavingCargo);
			int num5 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.LeavingDummy);
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			offer.Position = data.m_position * ((float)instance.m_randomizer.Int32(100, 400) * 0.01f);
			offer.Active = true;
			int num6 = num;
			if (num6 != 0)
			{
				if (num6 * num4 + instance.m_randomizer.Int32(256u) >> 8 == 0)
				{
					offer.Priority = 0;
					offer.Amount = 1;
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Ore, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Oil, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Grain, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Logs, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Goods, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Coal, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Petrol, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Food, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Lumber, offer);
					}
				}
				else
				{
					offer.Priority = 0;
					offer.Amount = num;
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Ore, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Oil, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Grain, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Logs, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Goods, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Coal, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Petrol, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Food, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Lumber, offer);
				}
			}
			int num7 = Singleton<ZoneManager>.get_instance().GetIncomingResidentDemand() * num2 / 100;
			if (num7 > 0)
			{
				num7 = num7 * num3 + instance.m_randomizer.Int32(256u) >> 8;
				if (num7 == 0)
				{
					offer.Priority = 0;
					offer.Amount = 1;
					if (instance.m_randomizer.Int32(2u) == 0)
					{
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single0, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single1, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single2, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single3, offer);
						}
					}
					else
					{
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single0B, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single1B, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single2B, offer);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddOutgoingOffer(TransferManager.TransferReason.Single3B, offer);
						}
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Family0, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Family1, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Family2, offer);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Family3, offer);
					}
				}
				else
				{
					offer.Priority = 0;
					offer.Amount = num7;
					if (instance.m_randomizer.Int32(2u) == 0)
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single0, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single1, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single2, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single3, offer);
					}
					else
					{
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single0B, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single1B, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single2B, offer);
						instance2.AddOutgoingOffer(TransferManager.TransferReason.Single3B, offer);
					}
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Family0, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Family1, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Family2, offer);
					instance2.AddOutgoingOffer(TransferManager.TransferReason.Family3, offer);
				}
			}
			int num8 = (dummyTrafficFactor + instance.m_randomizer.Int32(100u)) / 100;
			if (num8 > 0 && dummyTrafficReason != TransferManager.TransferReason.None)
			{
				num8 = num8 * num5 + instance.m_randomizer.Int32(256u) >> 8;
				if (num8 == 0)
				{
					offer.Priority = 7;
					offer.Amount = 1;
					if (instance.m_randomizer.Int32(4u) == 0)
					{
						instance2.AddOutgoingOffer(dummyTrafficReason, offer);
					}
				}
				else
				{
					offer.Priority = 7;
					offer.Amount = num8;
					instance2.AddOutgoingOffer(dummyTrafficReason, offer);
				}
			}
			int num9;
			Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.Attractiveness, out num9);
			num9 = 100 * num9 / Mathf.Max(num9 + 200, 200);
			int num10 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHomeOrWorkCount;
			num10 = (100 * num10 + 20000) / Mathf.Max(num10 + 20000, 20000);
			int num11 = num10 * num9;
			int num12 = (num11 * touristFactor0 + instance.m_randomizer.Int32(160000u)) / 160000;
			int num13 = (num11 * touristFactor1 + instance.m_randomizer.Int32(160000u)) / 160000;
			int num14 = (num11 * touristFactor2 + instance.m_randomizer.Int32(160000u)) / 160000;
			num11 = num12 + num13 + num14;
			if (num11 != 0)
			{
				if (num11 * num3 + instance.m_randomizer.Int32(256u) >> 8 == 0)
				{
					offer.Priority = instance.m_randomizer.Int32(8u);
					offer.Amount = 1;
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						switch (instance.m_randomizer.Int32(8u))
						{
						case 0:
							instance2.AddIncomingOffer(TransferManager.TransferReason.Shopping, offer);
							break;
						case 1:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingB, offer);
							break;
						case 2:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingC, offer);
							break;
						case 3:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingD, offer);
							break;
						case 4:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingE, offer);
							break;
						case 5:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingF, offer);
							break;
						case 6:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingG, offer);
							break;
						case 7:
							instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingH, offer);
							break;
						}
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						switch (instance.m_randomizer.Int32(4u))
						{
						case 0:
							instance2.AddIncomingOffer(TransferManager.TransferReason.Entertainment, offer);
							break;
						case 1:
							instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentB, offer);
							break;
						case 2:
							instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentC, offer);
							break;
						case 3:
							instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentD, offer);
							break;
						}
					}
				}
				else
				{
					offer.Priority = instance.m_randomizer.Int32(8u);
					offer.Amount = num12 + num13 + num14;
					switch (instance.m_randomizer.Int32(8u))
					{
					case 0:
						instance2.AddIncomingOffer(TransferManager.TransferReason.Shopping, offer);
						break;
					case 1:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingB, offer);
						break;
					case 2:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingC, offer);
						break;
					case 3:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingD, offer);
						break;
					case 4:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingE, offer);
						break;
					case 5:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingF, offer);
						break;
					case 6:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingG, offer);
						break;
					case 7:
						instance2.AddIncomingOffer(TransferManager.TransferReason.ShoppingH, offer);
						break;
					}
					switch (instance.m_randomizer.Int32(4u))
					{
					case 0:
						instance2.AddIncomingOffer(TransferManager.TransferReason.Entertainment, offer);
						break;
					case 1:
						instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentB, offer);
						break;
					case 2:
						instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentC, offer);
						break;
					case 3:
						instance2.AddIncomingOffer(TransferManager.TransferReason.EntertainmentD, offer);
						break;
					}
				}
			}
		}
		if ((data.m_flags & Building.Flags.Incoming) != Building.Flags.None)
		{
			int num15 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.EnteringHuman);
			int num16 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.EnteringCargo);
			int num17 = OutsideConnectionAI.TickPathfindStatus(buildingID, ref data, BuildingAI.PathFindType.EnteringDummy);
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Building = buildingID;
			offer2.Position = data.m_position * ((float)instance.m_randomizer.Int32(100, 400) * 0.01f);
			offer2.Active = false;
			int num18 = num;
			if (num18 != 0)
			{
				if (num18 * num16 + instance.m_randomizer.Int32(256u) >> 8 == 0)
				{
					offer2.Priority = 0;
					offer2.Amount = 1;
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Ore, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Oil, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Grain, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Logs, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Goods, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Coal, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Petrol, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Food, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Lumber, offer2);
					}
				}
				else
				{
					offer2.Priority = 0;
					offer2.Amount = num;
					instance2.AddIncomingOffer(TransferManager.TransferReason.Ore, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Oil, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Grain, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Logs, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Goods, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Coal, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Petrol, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Food, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Lumber, offer2);
				}
			}
			int num19 = num2;
			if (num19 > 0)
			{
				if (num19 * num15 + instance.m_randomizer.Int32(256u) >> 8 == 0)
				{
					offer2.Priority = 0;
					offer2.Amount = 1;
					if (instance.m_randomizer.Int32(2u) == 0)
					{
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single0, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single1, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single2, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single3, offer2);
						}
					}
					else
					{
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single0B, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single1B, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single2B, offer2);
						}
						if (instance.m_randomizer.Int32(16u) == 0)
						{
							instance2.AddIncomingOffer(TransferManager.TransferReason.Single3B, offer2);
						}
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Family0, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Family1, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Family2, offer2);
					}
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Family3, offer2);
					}
				}
				else
				{
					offer2.Priority = 0;
					offer2.Amount = num2;
					if (instance.m_randomizer.Int32(2u) == 0)
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single0, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single1, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single2, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single3, offer2);
					}
					else
					{
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single0B, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single1B, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single2B, offer2);
						instance2.AddIncomingOffer(TransferManager.TransferReason.Single3B, offer2);
					}
					instance2.AddIncomingOffer(TransferManager.TransferReason.Family0, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Family1, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Family2, offer2);
					instance2.AddIncomingOffer(TransferManager.TransferReason.Family3, offer2);
				}
			}
			int num20 = (dummyTrafficFactor + instance.m_randomizer.Int32(100u)) / 100;
			if (num20 > 0 && dummyTrafficReason != TransferManager.TransferReason.None)
			{
				num20 = num20 * num17 + instance.m_randomizer.Int32(256u) >> 8;
				if (num20 == 0)
				{
					offer2.Priority = 7;
					offer2.Amount = 1;
					if (instance.m_randomizer.Int32(4u) == 0)
					{
						instance2.AddIncomingOffer(dummyTrafficReason, offer2);
					}
				}
				else
				{
					offer2.Priority = 7;
					offer2.Amount = num20;
					instance2.AddIncomingOffer(dummyTrafficReason, offer2);
				}
			}
			int num21 = Mathf.Max(1, (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHomeOrWorkCount);
			int num22 = (num21 + instance.m_randomizer.Int32(10u)) / 10;
			int num23 = (num22 * touristFactor0 + instance.m_randomizer.Int32(16000u)) / 16000;
			int num24 = (num22 * touristFactor1 + instance.m_randomizer.Int32(16000u)) / 16000;
			int num25 = (num22 * touristFactor2 + instance.m_randomizer.Int32(16000u)) / 16000;
			if (num23 != 0)
			{
				num23 = num23 * num15 + instance.m_randomizer.Int32(256u) >> 8;
				if (num23 == 0)
				{
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						offer2.Priority = instance.m_randomizer.Int32(8u);
						offer2.Amount = 1;
						instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity0, offer2);
					}
				}
				else
				{
					offer2.Priority = instance.m_randomizer.Int32(8u);
					offer2.Amount = num23;
					instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity0, offer2);
				}
			}
			if (num24 != 0)
			{
				num24 = num24 * num15 + instance.m_randomizer.Int32(256u) >> 8;
				if (num24 == 0)
				{
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						offer2.Priority = instance.m_randomizer.Int32(8u);
						offer2.Amount = 1;
						instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity1, offer2);
					}
				}
				else
				{
					offer2.Priority = instance.m_randomizer.Int32(8u);
					offer2.Amount = num24;
					instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity1, offer2);
				}
			}
			if (num25 != 0)
			{
				num25 = num25 * num15 + instance.m_randomizer.Int32(256u) >> 8;
				if (num25 == 0)
				{
					if (instance.m_randomizer.Int32(16u) == 0)
					{
						offer2.Priority = instance.m_randomizer.Int32(8u);
						offer2.Amount = 1;
						instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity2, offer2);
					}
				}
				else
				{
					offer2.Priority = instance.m_randomizer.Int32(8u);
					offer2.Amount = num25;
					instance2.AddOutgoingOffer(TransferManager.TransferReason.LeaveCity2, offer2);
				}
			}
		}
	}

	private static int DummyTrafficProbability()
	{
		uint vehicleCount = (uint)Singleton<VehicleManager>.get_instance().m_vehicleCount;
		uint instanceCount = (uint)Singleton<CitizenManager>.get_instance().m_instanceCount;
		if (vehicleCount * 65536u > instanceCount * 16384u)
		{
			return (int)(2048000u / (16384u + vehicleCount * 4u) - 25u);
		}
		return (int)(8192000u / (65536u + instanceCount * 4u) - 25u);
	}

	protected static void ImportResource(ushort buildingID, ref Building data, TransferManager.TransferReason resource, int amount)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(data.m_position);
		switch (resource)
		{
		case TransferManager.TransferReason.Oil:
			goto IL_81;
		case TransferManager.TransferReason.Ore:
		case TransferManager.TransferReason.Coal:
		{
			District[] expr_6F_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
			byte expr_6F_cp_0_cp_1 = district;
			expr_6F_cp_0_cp_0[(int)expr_6F_cp_0_cp_1].m_importData.m_tempOre = expr_6F_cp_0_cp_0[(int)expr_6F_cp_0_cp_1].m_importData.m_tempOre + (uint)amount;
			return;
		}
		case TransferManager.TransferReason.Logs:
			goto IL_AD;
		case TransferManager.TransferReason.Grain:
			goto IL_D9;
		case TransferManager.TransferReason.Goods:
		{
			District[] expr_11F_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
			byte expr_11F_cp_0_cp_1 = district;
			expr_11F_cp_0_cp_0[(int)expr_11F_cp_0_cp_1].m_importData.m_tempGoods = expr_11F_cp_0_cp_0[(int)expr_11F_cp_0_cp_1].m_importData.m_tempGoods + (uint)amount;
			return;
		}
		case TransferManager.TransferReason.PassengerTrain:
			IL_38:
			if (resource == TransferManager.TransferReason.Petrol)
			{
				goto IL_81;
			}
			if (resource == TransferManager.TransferReason.Food)
			{
				goto IL_D9;
			}
			if (resource != TransferManager.TransferReason.Lumber)
			{
				return;
			}
			goto IL_AD;
		}
		goto IL_38;
		IL_81:
		District[] expr_9B_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_9B_cp_0_cp_1 = district;
		expr_9B_cp_0_cp_0[(int)expr_9B_cp_0_cp_1].m_importData.m_tempOil = expr_9B_cp_0_cp_0[(int)expr_9B_cp_0_cp_1].m_importData.m_tempOil + (uint)amount;
		return;
		IL_AD:
		District[] expr_C7_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_C7_cp_0_cp_1 = district;
		expr_C7_cp_0_cp_0[(int)expr_C7_cp_0_cp_1].m_importData.m_tempForestry = expr_C7_cp_0_cp_0[(int)expr_C7_cp_0_cp_1].m_importData.m_tempForestry + (uint)amount;
		return;
		IL_D9:
		District[] expr_F3_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_F3_cp_0_cp_1 = district;
		expr_F3_cp_0_cp_0[(int)expr_F3_cp_0_cp_1].m_importData.m_tempAgricultural = expr_F3_cp_0_cp_0[(int)expr_F3_cp_0_cp_1].m_importData.m_tempAgricultural + (uint)amount;
	}

	public static int GetProductionRate(int productionRate, int budget)
	{
		if (budget < 100)
		{
			budget = (budget * budget + 99) / 100;
		}
		else if (budget > 150)
		{
			budget = 125;
		}
		else if (budget > 100)
		{
			budget -= (100 - budget) * (100 - budget) / 100;
		}
		return (productionRate * budget + 99) / 100;
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		CityNameGroups.Environment cityNameGroups = Singleton<BuildingManager>.get_instance().m_cityNameGroups;
		if (cityNameGroups == null)
		{
			return null;
		}
		int num = 0;
		if (this.m_useCloseNames)
		{
			num += cityNameGroups.m_closeDistance.Length;
		}
		if (this.m_useMediumNames)
		{
			num += cityNameGroups.m_mediumDistance.Length;
		}
		if (this.m_useFarNames)
		{
			num += cityNameGroups.m_farDistance.Length;
		}
		if (num != 0)
		{
			Randomizer randomizer;
			randomizer..ctor(caller.Index);
			num = randomizer.Int32((uint)num);
			string text = null;
			if (text == null && this.m_useCloseNames)
			{
				if (num < cityNameGroups.m_closeDistance.Length)
				{
					text = cityNameGroups.m_closeDistance[num];
				}
				else
				{
					num -= cityNameGroups.m_closeDistance.Length;
				}
			}
			if (text == null && this.m_useMediumNames)
			{
				if (num < cityNameGroups.m_mediumDistance.Length)
				{
					text = cityNameGroups.m_mediumDistance[num];
				}
				else
				{
					num -= cityNameGroups.m_mediumDistance.Length;
				}
			}
			if (text == null && this.m_useFarNames)
			{
				if (num < cityNameGroups.m_farDistance.Length)
				{
					text = cityNameGroups.m_farDistance[num];
				}
				else
				{
					num -= cityNameGroups.m_farDistance.Length;
				}
			}
			uint num2 = Locale.Count("CONNECTIONS_PATTERN", text);
			string format = Locale.Get("CONNECTIONS_PATTERN", text, randomizer.Int32(num2));
			uint num3 = Locale.Count("CONNECTIONS_NAME", text);
			string arg = Locale.Get("CONNECTIONS_NAME", text, randomizer.Int32(num3));
			return StringUtils.SafeFormat(format, arg);
		}
		return null;
	}

	public override TransportInfo GetTransportLineInfo()
	{
		return this.m_transportInfo;
	}

	public int m_cargoCapacity = 20;

	public int m_residentCapacity = 1000;

	public int m_touristFactor0 = 325;

	public int m_touristFactor1 = 125;

	public int m_touristFactor2 = 50;

	public bool m_useCloseNames = true;

	public bool m_useMediumNames;

	public bool m_useFarNames;

	public TransferManager.TransferReason m_dummyTrafficReason = TransferManager.TransferReason.None;

	public int m_dummyTrafficFactor = 1000;

	public TransportInfo m_transportInfo;
}
