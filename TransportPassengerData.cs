﻿using System;
using ColossalFramework.IO;

public struct TransportPassengerData
{
	public void Add(ref TransportPassengerData data)
	{
		this.m_residentPassengers.Add(ref data.m_residentPassengers);
		this.m_touristPassengers.Add(ref data.m_touristPassengers);
		this.m_childPassengers.Add(ref data.m_childPassengers);
		this.m_teenPassengers.Add(ref data.m_teenPassengers);
		this.m_youngPassengers.Add(ref data.m_youngPassengers);
		this.m_adultPassengers.Add(ref data.m_adultPassengers);
		this.m_seniorPassengers.Add(ref data.m_seniorPassengers);
		this.m_carOwningPassengers.Add(ref data.m_carOwningPassengers);
	}

	public void Update()
	{
		this.m_residentPassengers.Update();
		this.m_touristPassengers.Update();
		this.m_childPassengers.Update();
		this.m_teenPassengers.Update();
		this.m_youngPassengers.Update();
		this.m_adultPassengers.Update();
		this.m_seniorPassengers.Update();
		this.m_carOwningPassengers.Update();
	}

	public void Reset()
	{
		this.m_residentPassengers.Reset();
		this.m_touristPassengers.Reset();
		this.m_childPassengers.Reset();
		this.m_teenPassengers.Reset();
		this.m_youngPassengers.Reset();
		this.m_adultPassengers.Reset();
		this.m_seniorPassengers.Reset();
		this.m_carOwningPassengers.Reset();
	}

	public void Serialize(DataSerializer s)
	{
		this.m_residentPassengers.Serialize(s);
		this.m_touristPassengers.Serialize(s);
		this.m_childPassengers.Serialize(s);
		this.m_teenPassengers.Serialize(s);
		this.m_youngPassengers.Serialize(s);
		this.m_adultPassengers.Serialize(s);
		this.m_seniorPassengers.Serialize(s);
		this.m_carOwningPassengers.Serialize(s);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_residentPassengers.Deserialize(s);
		this.m_touristPassengers.Deserialize(s);
		this.m_childPassengers.Deserialize(s);
		this.m_teenPassengers.Deserialize(s);
		this.m_youngPassengers.Deserialize(s);
		this.m_adultPassengers.Deserialize(s);
		this.m_seniorPassengers.Deserialize(s);
		if (s.get_version() >= 176u)
		{
			this.m_carOwningPassengers.Deserialize(s);
		}
		else
		{
			this.m_carOwningPassengers = default(TransportPassengerData.GroupData);
		}
	}

	public TransportPassengerData.GroupData m_residentPassengers;

	public TransportPassengerData.GroupData m_touristPassengers;

	public TransportPassengerData.GroupData m_childPassengers;

	public TransportPassengerData.GroupData m_teenPassengers;

	public TransportPassengerData.GroupData m_youngPassengers;

	public TransportPassengerData.GroupData m_adultPassengers;

	public TransportPassengerData.GroupData m_seniorPassengers;

	public TransportPassengerData.GroupData m_carOwningPassengers;

	public struct GroupData
	{
		public void Add(ref TransportPassengerData.GroupData data)
		{
			this.m_tempCount += data.m_tempCount;
		}

		public void Update()
		{
			uint num = this.m_finalCount + this.m_tempCount >> 1;
			if (num < this.m_averageCount)
			{
				this.m_averageCount = (this.m_averageCount * 8u + this.m_finalCount + this.m_tempCount) / 10u;
			}
			else if (num > this.m_averageCount)
			{
				this.m_averageCount = (this.m_averageCount * 8u + this.m_finalCount + this.m_tempCount + 9u) / 10u;
			}
			this.m_finalCount = this.m_tempCount;
		}

		public void Reset()
		{
			this.m_tempCount = 0u;
		}

		public void Serialize(DataSerializer s)
		{
			s.WriteUInt24(this.m_tempCount);
			s.WriteUInt24(this.m_finalCount);
			s.WriteUInt24(this.m_averageCount);
		}

		public void Deserialize(DataSerializer s)
		{
			this.m_tempCount = s.ReadUInt24();
			this.m_finalCount = s.ReadUInt24();
			this.m_averageCount = s.ReadUInt24();
		}

		public uint m_tempCount;

		public uint m_finalCount;

		public uint m_averageCount;
	}
}
