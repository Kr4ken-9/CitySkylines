﻿using System;
using ColossalFramework.IO;

public struct DistrictResourceData
{
	public void Add(ref DistrictResourceData data)
	{
		this.m_tempOre += data.m_tempOre;
		this.m_tempOil += data.m_tempOil;
		this.m_tempForestry += data.m_tempForestry;
		this.m_tempAgricultural += data.m_tempAgricultural;
		this.m_tempGoods += data.m_tempGoods;
	}

	public void Update()
	{
		this.m_averageOre = (this.m_averageOre * 18u + this.m_finalOre + this.m_tempOre + 18u) / 20u;
		this.m_averageOil = (this.m_averageOil * 18u + this.m_finalOil + this.m_tempOil + 18u) / 20u;
		this.m_averageForestry = (this.m_averageForestry * 18u + this.m_finalForestry + this.m_tempForestry + 18u) / 20u;
		this.m_averageAgricultural = (this.m_averageAgricultural * 18u + this.m_finalAgricultural + this.m_tempAgricultural + 18u) / 20u;
		this.m_averageGoods = (this.m_averageGoods * 18u + this.m_finalGoods + this.m_tempGoods + 18u) / 20u;
		this.m_finalOre = this.m_tempOre;
		this.m_finalOil = this.m_tempOil;
		this.m_finalForestry = this.m_tempForestry;
		this.m_finalAgricultural = this.m_tempAgricultural;
		this.m_finalGoods = this.m_tempGoods;
	}

	public void Reset()
	{
		this.m_tempOre = 0u;
		this.m_tempOil = 0u;
		this.m_tempForestry = 0u;
		this.m_tempAgricultural = 0u;
		this.m_tempGoods = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt32(this.m_tempOre);
		s.WriteUInt32(this.m_tempOil);
		s.WriteUInt32(this.m_tempForestry);
		s.WriteUInt32(this.m_tempAgricultural);
		s.WriteUInt32(this.m_tempGoods);
		s.WriteUInt32(this.m_finalOre);
		s.WriteUInt32(this.m_finalOil);
		s.WriteUInt32(this.m_finalForestry);
		s.WriteUInt32(this.m_finalAgricultural);
		s.WriteUInt32(this.m_finalGoods);
		s.WriteUInt32(this.m_averageOre);
		s.WriteUInt32(this.m_averageOil);
		s.WriteUInt32(this.m_averageForestry);
		s.WriteUInt32(this.m_averageAgricultural);
		s.WriteUInt32(this.m_averageGoods);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempOre = s.ReadUInt32();
		this.m_tempOil = s.ReadUInt32();
		this.m_tempForestry = s.ReadUInt32();
		this.m_tempAgricultural = s.ReadUInt32();
		this.m_tempGoods = s.ReadUInt32();
		this.m_finalOre = s.ReadUInt32();
		this.m_finalOil = s.ReadUInt32();
		this.m_finalForestry = s.ReadUInt32();
		this.m_finalAgricultural = s.ReadUInt32();
		this.m_finalGoods = s.ReadUInt32();
		if (s.get_version() >= 122u)
		{
			this.m_averageOre = s.ReadUInt32();
			this.m_averageOil = s.ReadUInt32();
			this.m_averageForestry = s.ReadUInt32();
			this.m_averageAgricultural = s.ReadUInt32();
			this.m_averageGoods = s.ReadUInt32();
		}
		else
		{
			this.m_averageOre = this.m_finalOre;
			this.m_averageOil = this.m_finalOil;
			this.m_averageForestry = this.m_finalForestry;
			this.m_averageAgricultural = this.m_finalAgricultural;
			this.m_averageGoods = this.m_finalGoods;
		}
	}

	public uint m_tempOre;

	public uint m_tempOil;

	public uint m_tempForestry;

	public uint m_tempAgricultural;

	public uint m_tempGoods;

	public uint m_finalOre;

	public uint m_finalOil;

	public uint m_finalForestry;

	public uint m_finalAgricultural;

	public uint m_finalGoods;

	public uint m_averageOre;

	public uint m_averageOil;

	public uint m_averageForestry;

	public uint m_averageAgricultural;

	public uint m_averageGoods;
}
