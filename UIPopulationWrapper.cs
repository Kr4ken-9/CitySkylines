﻿using System;

public class UIPopulationWrapper : UIWrapper<uint>
{
	public UIPopulationWrapper(uint def) : base(def)
	{
	}

	public override void Check(uint newVal)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			this.m_String = StringUtils.SafeFormat("{0:N0}", this.m_Value);
		}
	}

	private const string kFormat = "{0:N0}";
}
