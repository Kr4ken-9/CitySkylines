﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class PathVisualizer : MonoBehaviour
{
	public bool showPedestrians
	{
		get
		{
			return this.m_showPedestrians;
		}
		set
		{
			if (this.m_showPedestrians != value)
			{
				this.m_showPedestrians = value;
				this.m_filterModified = true;
			}
		}
	}

	public bool showCyclists
	{
		get
		{
			return this.m_showCyclists;
		}
		set
		{
			if (this.m_showCyclists != value)
			{
				this.m_showCyclists = value;
				this.m_filterModified = true;
			}
		}
	}

	public bool showPrivateVehicles
	{
		get
		{
			return this.m_showPrivateVehicles;
		}
		set
		{
			if (this.m_showPrivateVehicles != value)
			{
				this.m_showPrivateVehicles = value;
				this.m_filterModified = true;
			}
		}
	}

	public bool showPublicTransport
	{
		get
		{
			return this.m_showPublicTransport;
		}
		set
		{
			if (this.m_showPublicTransport != value)
			{
				this.m_showPublicTransport = value;
				this.m_filterModified = true;
			}
		}
	}

	public bool showTrucks
	{
		get
		{
			return this.m_showTrucks;
		}
		set
		{
			if (this.m_showTrucks != value)
			{
				this.m_showTrucks = value;
				this.m_filterModified = true;
			}
		}
	}

	public bool showCityServiceVehicles
	{
		get
		{
			return this.m_showCityServiceVehicles;
		}
		set
		{
			if (this.m_showCityServiceVehicles != value)
			{
				this.m_showCityServiceVehicles = value;
				this.m_filterModified = true;
			}
		}
	}

	private void Awake()
	{
		this.m_paths = new Dictionary<InstanceID, PathVisualizer.Path>();
		this.m_renderPaths = new FastList<PathVisualizer.Path>();
		this.m_removePaths = new FastList<PathVisualizer.Path>();
		this.m_stepPaths = new FastList<PathVisualizer.Path>();
		this.m_targets = new HashSet<InstanceID>();
		this.m_showPedestrians = true;
		this.m_showCyclists = true;
		this.m_showPrivateVehicles = true;
		this.m_showPublicTransport = true;
		this.m_showTrucks = true;
		this.m_showCityServiceVehicles = true;
	}

	private void OnDestroy()
	{
		this.DestroyPaths();
	}

	public bool PathsVisible
	{
		get
		{
			return this.m_pathsVisible;
		}
		set
		{
			this.m_pathsVisible = value;
		}
	}

	public void DestroyPaths()
	{
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (KeyValuePair<InstanceID, PathVisualizer.Path> current in this.m_paths)
			{
				PathVisualizer.Path value = current.Value;
				Mesh[] meshes = value.m_meshes;
				if (meshes != null)
				{
					for (int i = 0; i < meshes.Length; i++)
					{
						Mesh mesh = meshes[i];
						if (mesh != null)
						{
							Object.Destroy(mesh);
						}
					}
					value.m_meshes = null;
				}
			}
			this.m_paths.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
	}

	public void RenderPath(RenderManager.CameraInfo cameraInfo, PathVisualizer.Path path, bool secondary)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		TransportManager instance3 = Singleton<TransportManager>.get_instance();
		if (path.m_meshData != null)
		{
			this.UpdateMesh(path);
		}
		Mesh[] meshes = path.m_meshes;
		Material material = (!secondary) ? path.m_material : path.m_material2;
		if (meshes != null && material != null)
		{
			Bezier3[] lineCurves = path.m_lineCurves;
			Vector2[] curveOffsets = path.m_curveOffsets;
			Vector3 vector;
			Quaternion quaternion;
			Vector3 vector2;
			if (lineCurves != null && curveOffsets != null && lineCurves.Length == curveOffsets.Length && InstanceManager.GetPosition(path.m_id, out vector, out quaternion, out vector2))
			{
				while (lineCurves.Length > path.m_curveIndex)
				{
					Bezier3 bezier = lineCurves[path.m_curveIndex];
					Bezier3 bezier2;
					if (path.m_curveIndex + 1 < lineCurves.Length)
					{
						bezier2 = lineCurves[path.m_curveIndex + 1];
					}
					else
					{
						bezier2 = bezier;
					}
					float num;
					float num2;
					float num3;
					float num5;
					float num4;
					float num7;
					float num6;
					if (path.m_requireSurfaceLine)
					{
						num = VectorUtils.LengthSqrXZ(vector - bezier.a);
						num2 = VectorUtils.LengthSqrXZ(vector - bezier.d);
						num3 = VectorUtils.LengthSqrXZ(bezier.d - bezier.a);
						Bezier2 bezier3;
						bezier3..ctor(VectorUtils.XZ(bezier.a), VectorUtils.XZ(bezier.b), VectorUtils.XZ(bezier.c), VectorUtils.XZ(bezier.d));
						num4 = bezier3.DistanceSqr(VectorUtils.XZ(vector), ref num5);
						bezier3..ctor(VectorUtils.XZ(bezier2.a), VectorUtils.XZ(bezier2.b), VectorUtils.XZ(bezier2.c), VectorUtils.XZ(bezier2.d));
						num6 = bezier3.DistanceSqr(VectorUtils.XZ(vector), ref num7);
					}
					else
					{
						num = Vector3.SqrMagnitude(vector - bezier.a);
						num2 = Vector3.SqrMagnitude(vector - bezier.d);
						num3 = Vector3.SqrMagnitude(bezier.d - bezier.a);
						num4 = bezier.DistanceSqr(vector, ref num5);
						num6 = bezier2.DistanceSqr(vector, ref num7);
					}
					if (num5 > 0.99f || (num2 < num && num > num3))
					{
						path.m_pathOffset = curveOffsets[path.m_curveIndex++].y;
					}
					else
					{
						if (num6 < num4 && path.m_curveIndex + 1 < lineCurves.Length)
						{
							Vector2 vector3 = curveOffsets[++path.m_curveIndex];
							path.m_pathOffset = vector3.x + (vector3.y - vector3.x) * num7;
							break;
						}
						Vector2 vector4 = curveOffsets[path.m_curveIndex];
						path.m_pathOffset = vector4.x + (vector4.y - vector4.x) * num5;
						break;
					}
				}
			}
			material.set_color(path.m_color);
			material.SetFloat(instance3.ID_StartOffset, path.m_pathOffset);
			int num8 = meshes.Length;
			for (int i = 0; i < num8; i++)
			{
				Mesh mesh = meshes[i];
				if (mesh != null && cameraInfo.Intersect(mesh.get_bounds()))
				{
					if (path.m_requireSurfaceLine)
					{
						instance2.SetWaterMaterialProperties(mesh.get_bounds().get_center(), material);
					}
					if (material.SetPass(0))
					{
						NetManager expr_39D_cp_0 = instance;
						expr_39D_cp_0.m_drawCallData.m_overlayCalls = expr_39D_cp_0.m_drawCallData.m_overlayCalls + 1;
						Graphics.DrawMeshNow(mesh, Matrix4x4.get_identity());
					}
				}
			}
		}
	}

	public void RenderPaths(RenderManager.CameraInfo cameraInfo, int layerMask)
	{
		if (!this.m_pathsVisible)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (KeyValuePair<InstanceID, PathVisualizer.Path> current in this.m_paths)
			{
				PathVisualizer.Path value = current.Value;
				if (value.m_canRelease)
				{
					this.m_removePaths.Add(value);
				}
				else if ((layerMask & 1 << value.m_layer) != 0 || (value.m_layer2 != -1 && (layerMask & 1 << value.m_layer2) != 0))
				{
					this.m_renderPaths.Add(value);
				}
			}
			for (int i = 0; i < this.m_removePaths.m_size; i++)
			{
				this.m_paths.Remove(this.m_removePaths.m_buffer[i].m_id);
			}
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
		for (int j = 0; j < this.m_removePaths.m_size; j++)
		{
			Mesh[] meshes = this.m_removePaths.m_buffer[j].m_meshes;
			if (meshes != null)
			{
				for (int k = 0; k < meshes.Length; k++)
				{
					Mesh mesh = meshes[k];
					if (mesh != null)
					{
						Object.Destroy(mesh);
					}
				}
			}
		}
		this.m_removePaths.Clear();
		for (int l = 0; l < this.m_renderPaths.m_size; l++)
		{
			PathVisualizer.Path path = this.m_renderPaths.m_buffer[l];
			this.RenderPath(cameraInfo, path, (layerMask & 1 << path.m_layer) == 0);
		}
		this.m_renderPaths.Clear();
	}

	private void UpdateMesh(PathVisualizer.Path path)
	{
		while (!Monitor.TryEnter(path, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		RenderGroup.MeshData[] meshData;
		try
		{
			meshData = path.m_meshData;
			path.m_curveIndex = path.m_startCurveIndex;
			path.m_meshData = null;
		}
		finally
		{
			Monitor.Exit(path);
		}
		if (meshData != null)
		{
			path.m_color = Color.get_black();
			if (path.m_id.Vehicle != 0)
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)path.m_id.Vehicle].Info;
				if (info != null)
				{
					path.m_color = info.m_vehicleAI.GetColor(path.m_id.Vehicle, ref instance.m_vehicles.m_buffer[(int)path.m_id.Vehicle], InfoManager.InfoMode.TrafficRoutes);
				}
			}
			else if (path.m_id.CitizenInstance != 0)
			{
				CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
				CitizenInfo info2 = instance2.m_instances.m_buffer[(int)path.m_id.CitizenInstance].Info;
				if (info2 != null)
				{
					path.m_color = info2.m_citizenAI.GetColor(path.m_id.CitizenInstance, ref instance2.m_instances.m_buffer[(int)path.m_id.CitizenInstance], InfoManager.InfoMode.TrafficRoutes);
				}
			}
			Mesh[] array = path.m_meshes;
			int num = 0;
			if (array != null)
			{
				num = array.Length;
			}
			if (num != meshData.Length)
			{
				Mesh[] array2 = new Mesh[meshData.Length];
				int num2 = Mathf.Min(num, array2.Length);
				for (int i = 0; i < num2; i++)
				{
					array2[i] = array[i];
				}
				for (int j = num2; j < array2.Length; j++)
				{
					array2[j] = new Mesh();
				}
				for (int k = num2; k < num; k++)
				{
					Object.Destroy(array[k]);
				}
				array = array2;
				path.m_meshes = array;
			}
			for (int l = 0; l < meshData.Length; l++)
			{
				array[l].Clear();
				array[l].set_vertices(meshData[l].m_vertices);
				array[l].set_normals(meshData[l].m_normals);
				array[l].set_tangents(meshData[l].m_tangents);
				array[l].set_uv(meshData[l].m_uvs);
				array[l].set_uv2(meshData[l].m_uvs2);
				array[l].set_colors32(meshData[l].m_colors);
				array[l].set_triangles(meshData[l].m_triangles);
				array[l].set_bounds(meshData[l].m_bounds);
			}
		}
	}

	public void SimulationStep(int subStep)
	{
		if (!this.m_pathsVisible)
		{
			return;
		}
		InstanceID instanceID = Singleton<InstanceManager>.get_instance().GetSelectedInstance();
		if (instanceID.Citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort instance3 = instance.m_citizens.m_buffer[(int)((UIntPtr)instanceID.Citizen)].m_instance;
			ushort vehicle = instance.m_citizens.m_buffer[(int)((UIntPtr)instanceID.Citizen)].m_vehicle;
			if (instance3 != 0 && instance.m_instances.m_buffer[(int)instance3].m_path != 0u)
			{
				instanceID.CitizenInstance = instance3;
			}
			else if (vehicle != 0 && instance2.m_vehicles.m_buffer[(int)vehicle].m_path != 0u)
			{
				instanceID.Vehicle = vehicle;
			}
		}
		if (instanceID.Vehicle != 0)
		{
			CitizenManager instance4 = Singleton<CitizenManager>.get_instance();
			VehicleManager instance5 = Singleton<VehicleManager>.get_instance();
			instanceID.Vehicle = instance5.m_vehicles.m_buffer[(int)instanceID.Vehicle].GetFirstVehicle(instanceID.Vehicle);
			VehicleInfo info = instance5.m_vehicles.m_buffer[(int)instanceID.Vehicle].Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				InstanceID ownerID = info.m_vehicleAI.GetOwnerID(instanceID.Vehicle, ref instance5.m_vehicles.m_buffer[(int)instanceID.Vehicle]);
				if (ownerID.Citizen != 0u)
				{
					ownerID.CitizenInstance = instance4.m_citizens.m_buffer[(int)((UIntPtr)ownerID.Citizen)].m_instance;
				}
				if (ownerID.CitizenInstance != 0)
				{
					instanceID = ownerID;
				}
			}
		}
		if (instanceID != this.m_lastInstance || this.m_filterModified)
		{
			this.m_filterModified = false;
			this.PreAddInstances();
			if (instanceID.Vehicle != 0 || instanceID.CitizenInstance != 0)
			{
				Singleton<GuideManager>.get_instance().m_routeButton.Disable();
				if (instanceID.CitizenInstance != 0 && !this.m_citizenPathChecked && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
				{
					this.m_citizenPathChecked = true;
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						if (!PlatformService.get_achievements().get_Item("Reporting").get_achieved())
						{
							PlatformService.get_achievements().get_Item("Reporting").Unlock();
						}
					});
				}
				this.AddInstance(instanceID);
			}
			else if (instanceID.NetSegment != 0 || instanceID.Building != 0 || instanceID.District != 0)
			{
				this.AddPaths(instanceID, 0, 256);
			}
			this.PostAddInstances();
			this.m_lastInstance = instanceID;
			this.m_pathRefreshFrame = 0;
		}
		else if (instanceID.NetSegment != 0 || instanceID.Building != 0 || instanceID.District != 0)
		{
			if (this.m_pathRefreshFrame == 0)
			{
				this.PreAddInstances();
			}
			this.AddPaths(instanceID, this.m_pathRefreshFrame, this.m_pathRefreshFrame + 1);
			this.m_pathRefreshFrame++;
			if (this.m_pathRefreshFrame >= 256)
			{
				this.PostAddInstances();
				this.m_pathRefreshFrame = 0;
			}
		}
		for (int i = 0; i < this.m_stepPaths.m_size; i++)
		{
			this.StepPath(this.m_stepPaths.m_buffer[i]);
		}
	}

	private void PreAddInstances()
	{
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (KeyValuePair<InstanceID, PathVisualizer.Path> current in this.m_paths)
			{
				PathVisualizer.Path value = current.Value;
				value.m_stillNeeded = false;
			}
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
		this.m_neededPathCount = 0;
	}

	private void PostAddInstances()
	{
		this.m_stepPaths.Clear();
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (KeyValuePair<InstanceID, PathVisualizer.Path> current in this.m_paths)
			{
				PathVisualizer.Path value = current.Value;
				value.m_canRelease = !value.m_stillNeeded;
				if (value.m_stillNeeded)
				{
					this.m_stepPaths.Add(value);
				}
			}
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
	}

	private void AddPaths(InstanceID target, int min, int max)
	{
		this.m_targets.Clear();
		if (target.Building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			NetManager instance2 = Singleton<NetManager>.get_instance();
			int num = 0;
			while (target.Building != 0)
			{
				this.m_targets.Add(target);
				ushort num2 = instance.m_buildings.m_buffer[(int)target.Building].m_netNode;
				int num3 = 0;
				while (num2 != 0)
				{
					NetInfo info = instance2.m_nodes.m_buffer[(int)num2].Info;
					if (info.m_class.m_layer != ItemClass.Layer.PublicTransport)
					{
						for (int i = 0; i < 8; i++)
						{
							ushort segment = instance2.m_nodes.m_buffer[(int)num2].GetSegment(i);
							if (segment != 0 && instance2.m_segments.m_buffer[(int)segment].m_startNode == num2 && (instance2.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
							{
								InstanceID empty = InstanceID.Empty;
								empty.NetSegment = segment;
								this.m_targets.Add(empty);
							}
						}
					}
					num2 = instance2.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
					if (++num3 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				target.Building = instance.m_buildings.m_buffer[(int)target.Building].m_subBuilding;
				if (++num > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		else
		{
			this.m_targets.Add(target);
		}
		this.AddPathsImpl(min, max);
	}

	private void AddPathsImpl(int min, int max)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
		DistrictManager instance4 = Singleton<DistrictManager>.get_instance();
		if (this.m_showPrivateVehicles || this.m_showPublicTransport || this.m_showTrucks || this.m_showCityServiceVehicles)
		{
			VehicleManager instance5 = Singleton<VehicleManager>.get_instance();
			int num = min * 16384 >> 8;
			int num2 = max * 16384 >> 8;
			for (int i = num; i < num2; i++)
			{
				if ((instance5.m_vehicles.m_buffer[i].m_flags & (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.WaitingPath)) == Vehicle.Flags.Created)
				{
					VehicleInfo info = instance5.m_vehicles.m_buffer[i].Info;
					ItemClass.Service service = info.m_class.m_service;
					switch (service)
					{
					case ItemClass.Service.Residential:
						if (!this.m_showPrivateVehicles)
						{
							goto IL_3B6;
						}
						goto IL_12D;
					case ItemClass.Service.Commercial:
						IL_CB:
						if (service != ItemClass.Service.PublicTransport)
						{
							if (!this.m_showCityServiceVehicles)
							{
								goto IL_3B6;
							}
							goto IL_12D;
						}
						else
						{
							if (!this.m_showPublicTransport)
							{
								goto IL_3B6;
							}
							goto IL_12D;
						}
						break;
					case ItemClass.Service.Industrial:
						if (!this.m_showTrucks)
						{
							goto IL_3B6;
						}
						goto IL_12D;
					}
					goto IL_CB;
					IL_12D:
					bool flag = false;
					int num3 = (int)instance5.m_vehicles.m_buffer[i].m_pathPositionIndex;
					if (num3 == 255)
					{
						num3 = 0;
					}
					else
					{
						num3 >>= 1;
					}
					uint num4 = instance5.m_vehicles.m_buffer[i].m_path;
					bool flag2 = false;
					int num5 = 0;
					while (num4 != 0u && !flag && !flag2)
					{
						int positionCount = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].m_positionCount;
						for (int j = num3; j < positionCount; j++)
						{
							PathUnit.Position position = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].GetPosition(j);
							InstanceID empty = InstanceID.Empty;
							empty.NetSegment = position.m_segment;
							if (this.m_targets.Contains(empty))
							{
								if (instance2.m_segments.m_buffer[(int)position.m_segment].m_modifiedIndex < instance.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].m_buildIndex)
								{
									NetInfo info2 = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
									if (info2.m_lanes != null && (int)position.m_lane < info2.m_lanes.Length && (byte)(info2.m_lanes[(int)position.m_lane].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
									{
										flag = true;
										break;
									}
								}
								flag2 = true;
								break;
							}
						}
						num3 = 0;
						num4 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].m_nextPathUnit;
						if (++num5 >= 262144)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
					InstanceID targetID = info.m_vehicleAI.GetTargetID((ushort)i, ref instance5.m_vehicles.m_buffer[i]);
					flag |= this.m_targets.Contains(targetID);
					if (targetID.Building != 0)
					{
						Vector3 position2 = instance3.m_buildings.m_buffer[(int)targetID.Building].m_position;
						InstanceID empty2 = InstanceID.Empty;
						empty2.District = instance4.GetDistrict(position2);
						flag |= this.m_targets.Contains(empty2);
					}
					if (flag)
					{
						InstanceID empty3 = InstanceID.Empty;
						empty3.Vehicle = (ushort)i;
						this.AddInstance(empty3);
						if (this.m_neededPathCount >= 100)
						{
							return;
						}
					}
				}
				IL_3B6:;
			}
		}
		if (this.m_showPedestrians || this.m_showCyclists)
		{
			CitizenManager instance6 = Singleton<CitizenManager>.get_instance();
			VehicleManager instance7 = Singleton<VehicleManager>.get_instance();
			int num6 = min * 65536 >> 8;
			int num7 = max * 65536 >> 8;
			for (int k = num6; k < num7; k++)
			{
				if ((instance6.m_instances.m_buffer[k].m_flags & (CitizenInstance.Flags.Created | CitizenInstance.Flags.Deleted | CitizenInstance.Flags.WaitingPath)) == CitizenInstance.Flags.Created)
				{
					VehicleInfo.VehicleType vehicleType = VehicleInfo.VehicleType.None;
					uint citizen = instance6.m_instances.m_buffer[k].m_citizen;
					if (citizen != 0u)
					{
						ushort vehicle = instance6.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
						if (vehicle != 0)
						{
							VehicleInfo info3 = instance7.m_vehicles.m_buffer[(int)vehicle].Info;
							vehicleType = info3.m_vehicleType;
						}
					}
					if (vehicleType != VehicleInfo.VehicleType.Bicycle)
					{
						if (vehicleType != VehicleInfo.VehicleType.None)
						{
							goto IL_7AE;
						}
						if (!this.m_showPedestrians)
						{
							goto IL_7AE;
						}
					}
					else if (!this.m_showCyclists)
					{
						goto IL_7AE;
					}
					bool flag3 = false;
					int num8 = (int)instance6.m_instances.m_buffer[k].m_pathPositionIndex;
					if (num8 == 255)
					{
						num8 = 0;
					}
					else
					{
						num8 >>= 1;
					}
					uint num9 = instance6.m_instances.m_buffer[k].m_path;
					bool flag4 = false;
					int num10 = 0;
					while (num9 != 0u && !flag3 && !flag4)
					{
						int positionCount2 = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_positionCount;
						for (int l = num8; l < positionCount2; l++)
						{
							PathUnit.Position position3 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetPosition(l);
							InstanceID empty4 = InstanceID.Empty;
							empty4.NetSegment = position3.m_segment;
							if (this.m_targets.Contains(empty4))
							{
								if (instance2.m_segments.m_buffer[(int)position3.m_segment].m_modifiedIndex < instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_buildIndex)
								{
									NetInfo info4 = instance2.m_segments.m_buffer[(int)position3.m_segment].Info;
									if (info4.m_lanes != null && (int)position3.m_lane < info4.m_lanes.Length && (info4.m_lanes[(int)position3.m_lane].m_laneType == NetInfo.LaneType.Pedestrian || (info4.m_lanes[(int)position3.m_lane].m_laneType == NetInfo.LaneType.Vehicle && info4.m_lanes[(int)position3.m_lane].m_vehicleType == VehicleInfo.VehicleType.Bicycle)))
									{
										flag3 = true;
										break;
									}
								}
								flag4 = true;
								break;
							}
						}
						num8 = 0;
						num9 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_nextPathUnit;
						if (++num10 >= 262144)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
					CitizenInfo info5 = instance6.m_instances.m_buffer[k].Info;
					InstanceID targetID2 = info5.m_citizenAI.GetTargetID((ushort)k, ref instance6.m_instances.m_buffer[k]);
					flag3 |= this.m_targets.Contains(targetID2);
					if (targetID2.Building != 0)
					{
						Vector3 position4 = instance3.m_buildings.m_buffer[(int)targetID2.Building].m_position;
						InstanceID empty5 = InstanceID.Empty;
						empty5.District = instance4.GetDistrict(position4);
						flag3 |= this.m_targets.Contains(empty5);
					}
					if (flag3)
					{
						InstanceID empty6 = InstanceID.Empty;
						empty6.CitizenInstance = (ushort)k;
						this.AddInstance(empty6);
						if (this.m_neededPathCount >= 100)
						{
							return;
						}
					}
				}
				IL_7AE:;
			}
		}
	}

	private void AddInstance(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			PathVisualizer.Path path;
			if (this.m_paths.TryGetValue(id, out path))
			{
				if (!path.m_stillNeeded)
				{
					path.m_stillNeeded = true;
					this.m_neededPathCount++;
				}
				path.m_canRelease = false;
			}
			else
			{
				path = new PathVisualizer.Path();
				path.m_id = id;
				path.m_stillNeeded = true;
				path.m_refreshRequired = true;
				this.m_paths.Add(id, path);
				this.m_stepPaths.Add(path);
				this.m_neededPathCount++;
			}
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
	}

	private void StepPath(PathVisualizer.Path path)
	{
		InstanceID id = path.m_id;
		uint num = 0u;
		if (id.CitizenInstance != 0)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			num = instance.m_instances.m_buffer[(int)id.CitizenInstance].m_path;
		}
		else if (id.Vehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			num = instance2.m_vehicles.m_buffer[(int)id.Vehicle].m_path;
		}
		if (num != 0u)
		{
			if (num == path.m_nextPathUnit)
			{
				PathManager instance3 = Singleton<PathManager>.get_instance();
				path.m_pathUnit = num;
				path.m_nextPathUnit = instance3.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
			}
			else if (num != path.m_pathUnit)
			{
				path.m_refreshRequired = true;
			}
		}
		if (path.m_refreshRequired && this.RefreshPath(path))
		{
			path.m_refreshRequired = false;
		}
	}

	private TransportInfo GetTransportInfo(PathVisualizer.Path path)
	{
		if (path.m_id.Vehicle != 0)
		{
			VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)path.m_id.Vehicle].Info;
			if (info != null)
			{
				VehicleInfo.VehicleType vehicleType = info.m_vehicleType;
				switch (vehicleType)
				{
				case VehicleInfo.VehicleType.Metro:
					return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Metro);
				case VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Metro:
					IL_56:
					if (vehicleType != VehicleInfo.VehicleType.Ship)
					{
						if (vehicleType != VehicleInfo.VehicleType.Plane)
						{
							if (vehicleType == VehicleInfo.VehicleType.Tram)
							{
								return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Tram);
							}
							if (vehicleType != VehicleInfo.VehicleType.Helicopter)
							{
								if (vehicleType == VehicleInfo.VehicleType.Ferry)
								{
									goto IL_C1;
								}
								if (vehicleType == VehicleInfo.VehicleType.Monorail)
								{
									return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Monorail);
								}
								if (vehicleType == VehicleInfo.VehicleType.CableCar)
								{
									return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.CableCar);
								}
								if (vehicleType != VehicleInfo.VehicleType.Blimp)
								{
									return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Bus);
								}
							}
						}
						return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Airplane);
					}
					IL_C1:
					return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Ship);
				case VehicleInfo.VehicleType.Train:
					return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Train);
				}
				goto IL_56;
			}
		}
		else if (path.m_id.CitizenInstance != 0)
		{
			return Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.Bus);
		}
		return null;
	}

	private bool RefreshPath(PathVisualizer.Path path)
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		PathManager instance3 = Singleton<PathManager>.get_instance();
		TerrainManager instance4 = Singleton<TerrainManager>.get_instance();
		VehicleManager instance5 = Singleton<VehicleManager>.get_instance();
		CitizenManager instance6 = Singleton<CitizenManager>.get_instance();
		if (path.m_id.Vehicle != 0)
		{
			if ((instance5.m_vehicles.m_buffer[(int)path.m_id.Vehicle].m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
			{
				return false;
			}
		}
		else if (path.m_id.CitizenInstance != 0 && (instance6.m_instances.m_buffer[(int)path.m_id.CitizenInstance].m_flags & CitizenInstance.Flags.WaitingPath) != CitizenInstance.Flags.None)
		{
			return false;
		}
		TransportInfo transportInfo = this.GetTransportInfo(path);
		if (transportInfo != null)
		{
			path.m_material = transportInfo.m_lineMaterial2;
			path.m_material2 = transportInfo.m_secondaryLineMaterial2;
			path.m_requireSurfaceLine = transportInfo.m_requireSurfaceLine;
			path.m_layer = transportInfo.m_prefabDataLayer;
			path.m_layer2 = transportInfo.m_secondaryLayer;
		}
		TransportLine.TempUpdateMeshData[] array;
		if (path.m_requireSurfaceLine)
		{
			array = new TransportLine.TempUpdateMeshData[81];
		}
		else
		{
			array = new TransportLine.TempUpdateMeshData[1];
		}
		int num = 0;
		float num2 = 0f;
		Vector3 zero = Vector3.get_zero();
		int num3 = 0;
		NetInfo.LaneType laneTypes = NetInfo.LaneType.None;
		VehicleInfo.VehicleType vehicleTypes = VehicleInfo.VehicleType.None;
		uint num4;
		if (path.m_id.Vehicle != 0)
		{
			VehicleInfo info = instance5.m_vehicles.m_buffer[(int)path.m_id.Vehicle].Info;
			if (info != null)
			{
				laneTypes = (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle);
				vehicleTypes = info.m_vehicleType;
			}
			num4 = instance5.m_vehicles.m_buffer[(int)path.m_id.Vehicle].m_path;
			num3 = (int)instance5.m_vehicles.m_buffer[(int)path.m_id.Vehicle].m_pathPositionIndex;
			if (num3 == 255)
			{
				num3 = 0;
			}
			else
			{
				num3 >>= 1;
			}
		}
		else if (path.m_id.CitizenInstance != 0)
		{
			laneTypes = (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Pedestrian);
			vehicleTypes = VehicleInfo.VehicleType.Bicycle;
			num4 = instance6.m_instances.m_buffer[(int)path.m_id.CitizenInstance].m_path;
			num3 = (int)instance6.m_instances.m_buffer[(int)path.m_id.CitizenInstance].m_pathPositionIndex;
			if (num3 == 255)
			{
				num3 = 0;
			}
			else
			{
				num3 >>= 1;
			}
		}
		else
		{
			num4 = 0u;
		}
		path.m_pathUnit = num4;
		byte b = 0;
		if (num4 != 0u)
		{
			path.m_nextPathUnit = instance3.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].m_nextPathUnit;
			b = instance3.m_pathUnits.m_buffer[(int)((UIntPtr)num4)].m_pathFindFlags;
			if ((b & 4) != 0)
			{
				TransportLine.CalculatePathSegmentCount(num4, num3, laneTypes, vehicleTypes, ref array, ref num, ref num2, ref zero);
			}
		}
		else
		{
			path.m_nextPathUnit = 0u;
		}
		if (num != 0)
		{
			if (path.m_requireSurfaceLine)
			{
				TransportLine.TempUpdateMeshData[] expr_2F4_cp_0 = array;
				int expr_2F4_cp_1 = instance4.GetPatchIndex(zero);
				expr_2F4_cp_0[expr_2F4_cp_1].m_pathSegmentCount = expr_2F4_cp_0[expr_2F4_cp_1].m_pathSegmentCount + 1;
			}
			else
			{
				TransportLine.TempUpdateMeshData[] expr_30E_cp_0 = array;
				int expr_30E_cp_1 = 0;
				expr_30E_cp_0[expr_30E_cp_1].m_pathSegmentCount = expr_30E_cp_0[expr_30E_cp_1].m_pathSegmentCount + 1;
			}
		}
		int num5 = 0;
		for (int i = 0; i < array.Length; i++)
		{
			int pathSegmentCount = array[i].m_pathSegmentCount;
			if (pathSegmentCount != 0)
			{
				RenderGroup.MeshData meshData = new RenderGroup.MeshData();
				meshData.m_vertices = new Vector3[pathSegmentCount * 8];
				meshData.m_normals = new Vector3[pathSegmentCount * 8];
				meshData.m_tangents = new Vector4[pathSegmentCount * 8];
				meshData.m_uvs = new Vector2[pathSegmentCount * 8];
				meshData.m_uvs2 = new Vector2[pathSegmentCount * 8];
				meshData.m_colors = new Color32[pathSegmentCount * 8];
				meshData.m_triangles = new int[pathSegmentCount * 30];
				array[i].m_meshData = meshData;
				num5++;
			}
		}
		path.m_lineCurves = new Bezier3[num];
		path.m_curveOffsets = new Vector2[num];
		int num6 = 0;
		float lengthScale = Mathf.Ceil(num2 / 64f) / num2;
		float num7 = 0f;
		if (num4 != 0u && (b & 4) != 0)
		{
			Vector3 vector;
			Vector3 vector2;
			TransportLine.FillPathSegments(num4, num3, laneTypes, vehicleTypes, ref array, path.m_lineCurves, path.m_curveOffsets, ref num6, ref num7, lengthScale, out vector, out vector2, path.m_requireSurfaceLine, false);
		}
		if (num != 0)
		{
			if (path.m_requireSurfaceLine)
			{
				int patchIndex = instance4.GetPatchIndex(zero);
				TransportLine.FillPathNode(zero, array[patchIndex].m_meshData, array[patchIndex].m_pathSegmentIndex, 4f, (!path.m_requireSurfaceLine) ? 5f : 20f, true);
				TransportLine.TempUpdateMeshData[] expr_4B6_cp_0 = array;
				int expr_4B6_cp_1 = patchIndex;
				expr_4B6_cp_0[expr_4B6_cp_1].m_pathSegmentIndex = expr_4B6_cp_0[expr_4B6_cp_1].m_pathSegmentIndex + 1;
			}
			else
			{
				TransportLine.FillPathNode(zero, array[0].m_meshData, array[0].m_pathSegmentIndex, 4f, (!path.m_requireSurfaceLine) ? 5f : 20f, false);
				TransportLine.TempUpdateMeshData[] expr_511_cp_0 = array;
				int expr_511_cp_1 = 0;
				expr_511_cp_0[expr_511_cp_1].m_pathSegmentIndex = expr_511_cp_0[expr_511_cp_1].m_pathSegmentIndex + 1;
			}
		}
		RenderGroup.MeshData[] array2 = new RenderGroup.MeshData[num5];
		int num8 = 0;
		for (int j = 0; j < array.Length; j++)
		{
			if (array[j].m_meshData != null)
			{
				array[j].m_meshData.UpdateBounds();
				if (path.m_requireSurfaceLine)
				{
					Vector3 min = array[j].m_meshData.m_bounds.get_min();
					Vector3 max = array[j].m_meshData.m_bounds.get_max();
					max.y += 1024f;
					array[j].m_meshData.m_bounds.SetMinMax(min, max);
				}
				array2[num8++] = array[j].m_meshData;
			}
		}
		while (!Monitor.TryEnter(path, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			path.m_meshData = array2;
			path.m_startCurveIndex = 0;
		}
		finally
		{
			Monitor.Exit(path);
		}
		return true;
	}

	public void UpdateData()
	{
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (KeyValuePair<InstanceID, PathVisualizer.Path> current in this.m_paths)
			{
				PathVisualizer.Path value = current.Value;
				value.m_stillNeeded = false;
				value.m_canRelease = true;
			}
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
	}

	public bool IsPathVisible(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_paths, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		bool result;
		try
		{
			result = this.m_paths.ContainsKey(id);
		}
		finally
		{
			Monitor.Exit(this.m_paths);
		}
		return result;
	}

	private Dictionary<InstanceID, PathVisualizer.Path> m_paths;

	private FastList<PathVisualizer.Path> m_renderPaths;

	private FastList<PathVisualizer.Path> m_removePaths;

	private FastList<PathVisualizer.Path> m_stepPaths;

	private HashSet<InstanceID> m_targets;

	private InstanceID m_lastInstance;

	private bool m_pathsVisible;

	private int m_neededPathCount;

	private int m_pathRefreshFrame;

	private bool m_showPedestrians;

	private bool m_showCyclists;

	private bool m_showPrivateVehicles;

	private bool m_showPublicTransport;

	private bool m_showTrucks;

	private bool m_showCityServiceVehicles;

	private bool m_filterModified;

	private bool m_citizenPathChecked;

	public class Path
	{
		public RenderGroup.MeshData[] m_meshData;

		public Bezier3[] m_lineCurves;

		public Vector2[] m_curveOffsets;

		public Mesh[] m_meshes;

		public Material m_material;

		public Material m_material2;

		public Color m_color;

		public InstanceID m_id;

		public float m_pathOffset;

		public int m_layer;

		public int m_layer2;

		public int m_curveIndex;

		public int m_startCurveIndex;

		public uint m_pathUnit;

		public uint m_nextPathUnit;

		public bool m_requireSurfaceLine;

		public bool m_refreshRequired;

		public bool m_stillNeeded;

		public bool m_canRelease;
	}
}
