﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RoadBridgeAI : RoadBaseAI
{
	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Pollution || base.ColorizeProps(infoMode);
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		float num2 = (Mathf.Max(0f, startHeight) + Mathf.Max(0f, endHeight)) * 0.5f;
		int num3 = Mathf.RoundToInt(num / 8f + 0.1f);
		int num4 = Mathf.RoundToInt(num2 / 4f + 0.1f);
		int result = this.m_constructionCost * num3 + this.m_elevationCost * num4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Outside) == NetNode.Flags.None)
		{
			if (this.m_middlePillarInfo != null && (data.m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
			{
				building = this.m_middlePillarInfo;
				heightOffset = this.m_middlePillarOffset - 1f - this.m_middlePillarInfo.m_generatedInfo.m_size.y;
				return;
			}
			if (this.m_bridgePillarInfo != null)
			{
				building = this.m_bridgePillarInfo;
				heightOffset = this.m_bridgePillarOffset - 1f - this.m_bridgePillarInfo.m_generatedInfo.m_size.y;
				return;
			}
		}
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override float GetNodeBuildingAngle(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		float[] angleBuffer = instance.m_angleBuffer;
		int num = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null && info.m_class.m_service == ItemClass.Service.Road)
				{
					ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
					Vector3 vector;
					if (nodeID == startNode)
					{
						vector = instance.m_segments.m_buffer[(int)segment].m_startDirection;
					}
					else
					{
						vector = instance.m_segments.m_buffer[(int)segment].m_endDirection;
					}
					float num2 = Mathf.Atan2(vector.x, -vector.z) * 0.159154937f + 1.25f;
					angleBuffer[num++] = num2 - Mathf.Floor(num2);
					num2 += 0.5f;
					angleBuffer[num++] = num2 - Mathf.Floor(num2);
				}
			}
		}
		float result = 0f;
		if (num != 0)
		{
			if (num == 2)
			{
				result = angleBuffer[0] + 0.75f;
			}
			else
			{
				bool flag;
				float num3;
				float num4;
				do
				{
					flag = true;
					for (int j = 1; j < num; j++)
					{
						num3 = angleBuffer[j - 1];
						num4 = angleBuffer[j];
						if (num3 > num4)
						{
							angleBuffer[j - 1] = num4;
							angleBuffer[j] = num3;
							flag = false;
						}
					}
				}
				while (!flag);
				num3 = angleBuffer[num - 1];
				num4 = angleBuffer[0];
				float num5 = num4 + 1f - num3;
				result = (num3 + num4) * 0.5f;
				for (int k = 1; k < num; k++)
				{
					num3 = angleBuffer[k - 1];
					num4 = angleBuffer[k];
					float num6 = num4 - num3;
					if (num6 > num5)
					{
						num5 = num6;
						result = (num3 + num4) * 0.5f;
					}
				}
			}
		}
		return result;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if (this.m_bridgePillarInfo == null || !this.m_canModify)
		{
			return this.m_info.m_halfWidth - 1000f;
		}
		return this.m_info.m_halfWidth;
	}

	public override bool RequireDoubleSegments()
	{
		return this.m_doubleLength;
	}

	public override bool CanModify()
	{
		return this.m_canModify;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 5;
	}

	public override bool IsOverground()
	{
		return true;
	}

	public override ToolBase.ToolErrors CanConnectTo(ushort node, ushort segment, ulong[] segmentMask)
	{
		ToolBase.ToolErrors toolErrors = base.CanConnectTo(node, segment, segmentMask);
		NetManager instance = Singleton<NetManager>.get_instance();
		if (node != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
				if (segment2 != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment2].Info;
					if (info.m_class.m_service == ItemClass.Service.PublicTransport && info.m_class.m_subService == ItemClass.SubService.PublicTransportTrain)
					{
						toolErrors |= ToolBase.ToolErrors.ObjectCollision;
						if (segmentMask != null)
						{
							segmentMask[segment2 >> 6] |= 1uL << (int)segment2;
						}
					}
				}
			}
		}
		else if (segment != 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
			if (info2.m_class.m_service == ItemClass.Service.PublicTransport && info2.m_class.m_subService == ItemClass.SubService.PublicTransportTrain)
			{
				toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				if (segmentMask != null)
				{
					segmentMask[segment >> 6] |= 1uL << (int)segment;
				}
			}
		}
		return toolErrors;
	}

	public override bool CanIntersect(NetInfo with)
	{
		return with != null && (with.m_vehicleTypes & this.m_info.m_vehicleTypes) != VehicleInfo.VehicleType.None;
	}

	public override string GenerateName(ushort segmentID, ref NetSegment data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)data.m_nameSeed);
		string text = null;
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None && (!this.m_highwayRules || this.m_info.m_forwardVehicleLaneCount + this.m_info.m_backwardVehicleLaneCount >= 2))
		{
			text = "BRIDGE_NAME_PATTERN";
		}
		if (text == null)
		{
			return string.Empty;
		}
		uint num = Locale.Count(text);
		string format = Locale.Get(text, randomizer.Int32(num));
		string arg = base.GenerateStreetName(ref randomizer);
		return StringUtils.SafeFormat(format, arg);
	}

	public BuildingInfo m_bridgePillarInfo;

	public BuildingInfo m_middlePillarInfo;

	[CustomizableProperty("Elevation Cost", "Properties")]
	public int m_elevationCost = 2000;

	[CustomizableProperty("Bridge Pillar Offset", "Properties")]
	public float m_bridgePillarOffset;

	[CustomizableProperty("Middle Pillar Offset", "Properties")]
	public float m_middlePillarOffset;

	[CustomizableProperty("Double Length", "Properties")]
	public bool m_doubleLength;

	[CustomizableProperty("Can Modify", "Properties")]
	public bool m_canModify = true;
}
