﻿using System;

public sealed class PropsBillboardsGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Prop;
		}
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsBillboardsLogo", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsBillboardsSmallBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsBillboardsMediumBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsBillboardsLargeBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "PropsBillboardsLogo")
			{
				return 0;
			}
			if (name == "PropsBillboardsSmallBillboard")
			{
				return 1;
			}
			if (name == "PropsBillboardsMediumBillboard")
			{
				return 2;
			}
			if (name == "PropsBillboardsLargeBillboard")
			{
				return 3;
			}
		}
		return 2147483647;
	}
}
