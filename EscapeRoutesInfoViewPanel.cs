﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class EscapeRoutesInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_CoverageGradient = base.Find<UITextureSprite>("CoverageGradient");
		this.m_label = base.Find<UILabel>("Label");
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.m_label.set_text(Locale.Get("INFOVIEWS", "EscapeRoutes"));
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UISprite uISprite3 = base.Find<UISprite>("ColorEvacuated");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[25].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[25].m_inactiveColor);
					uISprite3.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[25].m_negativeColor);
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
			}
			this.OnLocaleChanged();
			this.m_initialized = true;
		}
	}

	private UITextureSprite m_CoverageGradient;

	private UILabel m_label;

	private bool m_initialized;
}
