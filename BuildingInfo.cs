﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

[ExecuteInEditMode]
[Serializable]
public class BuildingInfo : BuildingInfoBase
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_generatedInfo == null)
		{
			throw new PrefabException(this, "Generated info missing");
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(this.m_class.m_service);
		if (privateServiceIndex != -1)
		{
			if (this.m_placementStyle == ItemClass.Placement.Manual)
			{
				throw new PrefabException(this, "Private building cannot have manual placement style");
			}
			if (this.m_paths != null && this.m_paths.Length != 0)
			{
				throw new PrefabException(this, "Private building cannot include roads or other net types");
			}
		}
		else if (this.m_placementStyle == ItemClass.Placement.Automatic)
		{
			throw new PrefabException(this, "Player building cannot have automatic placement style");
		}
		this.m_lodMissing = false;
		bool flag = false;
		Renderer component = base.GetComponent<Renderer>();
		MeshFilter component2 = base.GetComponent<MeshFilter>();
		if (component2 != null || component != null || this.m_overrideMainMesh != null || this.m_overrideMainRenderer != null)
		{
			if (component2 == null && this.m_overrideMainMesh == null)
			{
				throw new PrefabException(this, "MeshFilter or SkinRenderer missing");
			}
			if (component == null && this.m_overrideMainRenderer == null)
			{
				throw new PrefabException(this, "Renderer missing");
			}
			if (this.m_overrideMainMesh != null)
			{
				this.m_mesh = this.m_overrideMainMesh;
			}
			else
			{
				this.m_mesh = component2.get_sharedMesh();
			}
			if (this.m_overrideMainRenderer != null)
			{
				this.m_material = this.m_overrideMainRenderer.get_sharedMaterial();
			}
			else
			{
				this.m_material = component.get_sharedMaterial();
			}
			if (this.m_mesh == null)
			{
				throw new PrefabException(this, "Mesh missing");
			}
			if (this.m_material == null)
			{
				throw new PrefabException(this, "Material missing");
			}
			if (this.m_generatedInfo.m_size == Vector3.get_zero())
			{
				throw new PrefabException(this, "Generated info has zero size");
			}
			if (this.m_generatedInfo.m_buildingInfo != null)
			{
				if (this.m_generatedInfo.m_buildingInfo.m_mesh != this.m_mesh)
				{
					throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_buildingInfo.get_gameObject().get_name() + ")");
				}
			}
			else
			{
				this.m_generatedInfo.m_buildingInfo = this;
			}
			if (this.m_material.HasProperty("_FloorParams"))
			{
				Vector4 vector = this.m_material.GetVector("_FloorParams");
				float num = vector.y - vector.x;
				float z = vector.z;
				if (num > -0.001f && num < 0.001f)
				{
					vector.y = vector.x + 3f;
					this.m_material.SetVector("_FloorParams", vector);
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "First floor height zero: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
				if (z > -0.001f && z < 0.001f)
				{
					vector.z = 3f;
					this.m_material.SetVector("_FloorParams", vector);
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "Other floor height zero: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
			}
			Texture2D texture2D = this.m_material.get_mainTexture() as Texture2D;
			if (texture2D != null)
			{
				if (texture2D.get_format() == 12)
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "Diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
				Texture2D texture2D2 = this.m_material.GetTexture(Singleton<BuildingManager>.get_instance().ID_XYSMap) as Texture2D;
				if (texture2D2 != null && (texture2D2.get_width() != texture2D.get_width() || texture2D2.get_height() != texture2D.get_height()))
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "XYS size doesn't match diffuse: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
				Texture2D texture2D3 = this.m_material.GetTexture(Singleton<BuildingManager>.get_instance().ID_ACIMap) as Texture2D;
				if (texture2D3 != null && (texture2D3.get_width() != texture2D3.get_width() || texture2D3.get_height() != texture2D3.get_height()))
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "ACI size doesn't match diffuse: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
			}
			if (this.m_lodObject != null)
			{
				component2 = this.m_lodObject.GetComponent<MeshFilter>();
				if (component2 == null)
				{
					throw new PrefabException(this, "LOD MeshFilter missing");
				}
				if (this.m_lodObject.GetComponent<Renderer>() == null)
				{
					throw new PrefabException(this, "LOD Renderer missing");
				}
				this.m_lodMesh = component2.get_sharedMesh();
				this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
				if (this.m_lodMesh == null)
				{
					throw new PrefabException(this, "LOD Mesh missing");
				}
				if (this.m_lodMaterial == null)
				{
					throw new PrefabException(this, "LOD Material missing");
				}
				if (this.m_lodMaterial.get_shader() != this.m_material.get_shader() && !this.m_lodHasDifferentShader)
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD has different shader: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
			}
			else
			{
				this.m_lodMissing = true;
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
			base.InitMeshInfo(this.m_generatedInfo.m_max - this.m_generatedInfo.m_min);
			if (!this.m_noBase && (this.m_generatedInfo.m_baseArea == null || this.m_generatedInfo.m_baseArea.Length == 0))
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "No base generated: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
			if (this.m_useColorVariations)
			{
				this.m_color0 = this.m_material.GetColor("_ColorV0");
				this.m_color1 = this.m_material.GetColor("_ColorV1");
				this.m_color2 = this.m_material.GetColor("_ColorV2");
				this.m_color3 = this.m_material.GetColor("_ColorV3");
			}
			else
			{
				this.m_color0 = this.m_material.get_color();
				this.m_color1 = this.m_material.get_color();
				this.m_color2 = this.m_material.get_color();
				this.m_color3 = this.m_material.get_color();
			}
			flag = true;
		}
		else
		{
			this.m_color0 = Color.get_white();
			this.m_color1 = Color.get_white();
			this.m_color2 = Color.get_white();
			this.m_color3 = Color.get_white();
		}
		this.m_anyMeshRequireWaterMap = this.m_requireWaterMap;
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				if (!(this.m_subMeshes[i].m_subInfo is BuildingInfoSub))
				{
					throw new PrefabException(this, "Submesh (" + this.m_subMeshes[i].m_subInfo.get_gameObject().get_name() + ") is not BuildingInfoSub");
				}
				this.m_subMeshes[i].m_subInfo.InitializePrefab();
				this.m_anyMeshRequireWaterMap |= this.m_subMeshes[i].m_subInfo.m_requireWaterMap;
				if (!flag)
				{
					this.m_color0 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
					this.m_color1 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
					this.m_color2 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
					this.m_color3 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
					flag = true;
				}
				this.m_subMeshes[i].m_matrix.SetTRS(this.m_subMeshes[i].m_position, Quaternion.AngleAxis(this.m_subMeshes[i].m_angle, Vector3.get_down()), Vector3.get_one());
			}
		}
		if (this.m_buildingAI == null)
		{
			this.m_buildingAI = base.GetComponent<BuildingAI>();
			this.m_buildingAI.m_info = this;
		}
		if (privateServiceIndex != -1 && this.m_buildingAI.GetType() == typeof(BuildingAI))
		{
			throw new PrefabException(this, "Correct BuildingAI not assigned");
		}
		int num2;
		int num3;
		this.m_buildingAI.GetWidthRange(out num2, out num3);
		int num4;
		int num5;
		this.m_buildingAI.GetLengthRange(out num4, out num5);
		if (this.m_cellWidth < num2 || this.m_cellWidth > num3 || this.m_cellLength < num4 || this.m_cellLength > num5)
		{
			throw new PrefabException(this, "Invalid dimensions (Max sizes: 16x8, 15x9, 14x11, 13x12, 12x13, 11x14, 9x15, 8x16)");
		}
		int num6;
		int num7;
		float num8;
		this.m_buildingAI.GetDecorationArea(out num6, out num7, out num8);
		this.m_size = this.m_generatedInfo.m_size;
		if (this.m_generatedInfo.m_baseArea == null || this.m_generatedInfo.m_baseArea.Length == 0)
		{
			this.m_maxHeightOffset = Mathf.Clamp(Mathf.Max((float)this.m_cellWidth * 4f, (float)this.m_cellLength * 4f), 4f, 16f);
		}
		else
		{
			this.m_maxHeightOffset = Mathf.Clamp(Mathf.Max(this.m_size.x, this.m_size.z) * 0.5f, 4f, 16f);
		}
		this.m_size.y = Mathf.Max(this.m_size.y, this.m_maxHeightOffset);
		Vector3 vector2;
		vector2..ctor((float)num6 * 4f, this.m_size.y * 0.5f, (float)Mathf.Max(4, num7) * 4f);
		this.m_renderSize = Mathf.Max(vector2.get_magnitude(), this.m_size.y);
		this.m_collisionHeight = this.m_size.y;
		if (this.m_cellWidth == 0 && this.m_cellLength == 0 && this.m_paths != null)
		{
			Vector3 vector3;
			vector3..ctor(100000f, 100000f, 100000f);
			Vector3 vector4;
			vector4..ctor(-100000f, -100000f, -100000f);
			if (this.m_paths != null)
			{
				for (int j = 0; j < this.m_paths.Length; j++)
				{
					Vector3[] nodes = this.m_paths[j].m_nodes;
					for (int k = 0; k < nodes.Length; k++)
					{
						vector3 = Vector3.Min(vector3, nodes[k]);
						vector4 = Vector3.Max(vector4, nodes[k]);
					}
				}
			}
			this.m_centerOffset = (vector3 + vector4) * 0.5f;
			this.m_centerOffset.z = -this.m_centerOffset.z;
		}
		else
		{
			this.m_centerOffset = Vector3.get_zero();
		}
		this.m_hasPedestrianPaths = false;
		this.m_fixedHeight = false;
		if (this.m_paths != null)
		{
			for (int l = 0; l < this.m_paths.Length; l++)
			{
				if (!(this.m_paths[l].m_netInfo != null))
				{
					throw new PrefabException(this, "NetInfo missing");
				}
				if (this.m_paths[l].m_netInfo.m_class.m_service == ItemClass.Service.Beautification)
				{
					this.m_hasPedestrianPaths = true;
				}
				if (this.m_paths[l].m_netInfo.m_useFixedHeight)
				{
					this.m_fixedHeight = true;
				}
			}
		}
		if (this.m_subBuildings != null)
		{
			for (int m = 0; m < this.m_subBuildings.Length; m++)
			{
				if (!(this.m_subBuildings[m].m_buildingInfo != null))
				{
					throw new PrefabException(this, "Sub building BuildingInfo missing");
				}
				if (this.m_subBuildings[m].m_fixedHeight)
				{
					this.m_fixedHeight = true;
				}
			}
		}
		if (this.m_props != null)
		{
			for (int n = 0; n < this.m_props.Length; n++)
			{
				if (this.m_cellLength == 0)
				{
					this.m_props[n].m_requiredLength = 0;
				}
				else
				{
					this.m_props[n].m_requiredLength = Mathf.CeilToInt((float)this.m_cellLength + (Mathf.Abs(this.m_props[n].m_position.z / 8f) - (float)this.m_cellLength * 0.5f));
				}
				this.m_props[n].m_radAngle = this.m_props[n].m_angle * 0.0174532924f;
				this.m_props[n].m_index = n;
			}
			if (this.m_props.Length > 64)
			{
				throw new PrefabException(this, "Too many props (" + this.m_props.Length + " / 64)");
			}
		}
		this.m_buildingAI.InitializePrefab();
		MilestoneInfo unlockMilestone = this.GetUnlockMilestone();
		if (unlockMilestone != null)
		{
			string text = "BuildingUnlocked[" + base.get_gameObject().get_name() + "]";
			this.m_unlocked = new SavedBool(text, Settings.userGameState, false);
			this.m_alreadyUnlocked = this.m_unlocked.get_value();
		}
	}

	public override void TempInitializePrefab()
	{
		if (this.m_buildingAI == null)
		{
			this.m_buildingAI = base.GetComponent<BuildingAI>();
			this.m_buildingAI.m_info = this;
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_buildingAI != null)
		{
			this.m_buildingAI.DestroyPrefab();
			this.m_buildingAI = null;
		}
		base.DestroyMeshInfo();
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				this.m_subMeshes[i].m_subInfo.DestroyPrefab();
			}
		}
		this.m_unlocked = null;
		this.m_collapsedInfo = null;
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		if (this.m_mesh != null)
		{
			base.RefreshLevelOfDetail(this.m_generatedInfo.m_max - this.m_generatedInfo.m_min);
		}
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				this.m_subMeshes[i].m_subInfo.RefreshLevelOfDetail();
			}
		}
	}

	public override void InitializePrefabInstance(PrefabInfo prefabInfo)
	{
		base.InitializePrefabInstance(prefabInfo);
		this.m_animator = base.get_gameObject().GetComponent<Animator>();
		this.m_buildingAI = base.get_gameObject().GetComponent<BuildingAI>();
		this.m_renderers = base.get_gameObject().GetComponentsInChildren<Renderer>(true);
		if (this.m_buildingAI != null)
		{
			this.m_buildingAI.m_info = this;
		}
		if (this.m_overrideMainRenderer != null)
		{
			this.m_material = new Material(this.m_overrideMainRenderer.get_sharedMaterial());
			if (this.m_overrideMainRenderer is SkinnedMeshRenderer)
			{
				this.m_material.EnableKeyword("ANIMATED_INSTANCE");
			}
			this.m_overrideMainRenderer.set_sharedMaterial(this.m_material);
		}
		BuildingInfo buildingInfo = prefabInfo as BuildingInfo;
		if (this.m_renderers != null)
		{
			for (int i = 0; i < this.m_renderers.Length; i++)
			{
				if (this.m_renderers[i] != this.m_overrideMainRenderer)
				{
					Material sharedMaterial = this.m_renderers[i].get_sharedMaterial();
					if (sharedMaterial != null)
					{
						this.m_renderers[i].set_sharedMaterial(new Material(sharedMaterial));
					}
				}
			}
		}
		if (buildingInfo != null && this.m_overrideMainRenderer != null && buildingInfo.m_generatedMesh != null && buildingInfo.m_extraMaterial != null)
		{
			this.m_extraMaterial = new Material(buildingInfo.m_extraMaterial);
			GameObject gameObject = Object.Instantiate<GameObject>(this.m_overrideMainRenderer.get_gameObject());
			gameObject.set_name("Generated Mesh");
			gameObject.get_transform().set_parent(base.get_transform());
			gameObject.get_transform().set_localPosition(this.m_overrideMainRenderer.get_transform().get_localPosition());
			gameObject.get_transform().set_localRotation(this.m_overrideMainRenderer.get_transform().get_localRotation());
			gameObject.get_transform().set_localScale(this.m_overrideMainRenderer.get_transform().get_localScale());
			SkinnedMeshRenderer component = gameObject.GetComponent<SkinnedMeshRenderer>();
			if (component != null)
			{
				component.set_sharedMaterial(this.m_extraMaterial);
				component.set_sharedMesh(buildingInfo.m_generatedMesh);
			}
			else
			{
				MeshRenderer component2 = gameObject.GetComponent<MeshRenderer>();
				MeshFilter component3 = gameObject.GetComponent<MeshFilter>();
				component2.set_sharedMaterial(this.m_extraMaterial);
				component3.set_sharedMesh(buildingInfo.m_generatedMesh);
			}
		}
	}

	public override void DestroyPrefabInstance()
	{
		if (this.m_material != null)
		{
			Object.Destroy(this.m_material);
			this.m_material = null;
		}
		if (this.m_extraMaterial != null)
		{
			Object.Destroy(this.m_extraMaterial);
			this.m_extraMaterial = null;
		}
		if (this.m_renderers != null)
		{
			for (int i = 0; i < this.m_renderers.Length; i++)
			{
				if (this.m_renderers[i] != this.m_overrideMainRenderer)
				{
					Material sharedMaterial = this.m_renderers[i].get_sharedMaterial();
					if (sharedMaterial != null)
					{
						this.m_renderers[i].set_sharedMaterial(null);
						Object.Destroy(sharedMaterial);
					}
				}
			}
			this.m_renderers = null;
		}
		this.m_animator = null;
		base.DestroyPrefabInstance();
	}

	public void SetRenderParameters(ushort buildingID, Vector3 position, Quaternion rotation, Vector4 buildingState, Vector4 objectIndex, float active, int state, bool playActiveState, Color color1, Color color2)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		InfoManager instance2 = Singleton<InfoManager>.get_instance();
		BuildingManager expr_12_cp_0 = instance;
		expr_12_cp_0.m_drawCallData.m_defaultCalls = expr_12_cp_0.m_drawCallData.m_defaultCalls + 1;
		Transform transform = base.get_transform();
		transform.set_position(position);
		transform.set_rotation(rotation);
		this.m_animator.SetFloat(instance.ID_Speed, active);
		if (state != -1000000)
		{
			this.m_animator.SetInteger(instance.ID_State, state);
		}
		bool flag = false;
		if (this.m_instanceChanged)
		{
			this.m_instanceChanged = false;
			flag = true;
			if (active > 0.01f && playActiveState)
			{
				this.m_animator.Play("active", -1, Random.get_value());
			}
			else
			{
				this.m_animator.Play("inactive", -1, Random.get_value());
			}
		}
		if (this.m_instanceDirty)
		{
			this.m_instanceDirty = false;
			flag = true;
		}
		if (active < 0.1f)
		{
			this.m_animator.set_speed((1f - active * 9f) * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		else
		{
			this.m_animator.set_speed(active * Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		}
		if (this.m_material != null)
		{
			this.m_material.SetVector(instance.ID_BuildingState, buildingState);
			this.m_material.SetVector(instance.ID_ObjectIndex, objectIndex);
			this.m_material.SetColor(instance.ID_Color, color1);
		}
		if (this.m_extraMaterial != null)
		{
			this.m_extraMaterial.SetVector(instance.ID_BuildingState, buildingState);
			this.m_extraMaterial.SetVector(instance.ID_ObjectIndex, objectIndex);
			this.m_extraMaterial.SetColor(instance.ID_Color, color1);
		}
		if (this.m_renderers != null)
		{
			if (instance2.CurrentMode != this.m_lastInfoMode)
			{
				this.m_lastInfoMode = instance2.CurrentMode;
				flag = true;
			}
			if (instance2.CurrentSubMode != this.m_lastSubInfoMode)
			{
				this.m_lastSubInfoMode = instance2.CurrentSubMode;
				flag = true;
			}
			for (int i = 0; i < this.m_renderers.Length; i++)
			{
				Renderer renderer = this.m_renderers[i];
				if (renderer != this.m_overrideMainRenderer)
				{
					bool flag2 = active > 0.01f;
					if (flag2 != renderer.get_enabled())
					{
						renderer.set_enabled(flag2);
					}
					if (flag)
					{
						Material sharedMaterial = renderer.get_sharedMaterial();
						if (sharedMaterial != null && sharedMaterial.HasProperty("_ColorV0"))
						{
							sharedMaterial.SetVector(instance.ID_BuildingState, buildingState);
							sharedMaterial.SetVector(instance.ID_ObjectIndex, objectIndex);
							BuildingColorGroup component = renderer.GetComponent<BuildingColorGroup>();
							if (component != null && component.m_colorGroup != 0)
							{
								if (component.m_colorGroup == 1)
								{
									sharedMaterial.SetColor(instance.ID_Color, color1);
								}
								else if (component.m_colorGroup == 2)
								{
									sharedMaterial.SetColor(instance.ID_Color, color2);
								}
							}
							else if (this.m_lastInfoMode != InfoManager.InfoMode.None)
							{
								sharedMaterial.SetColor(instance.ID_Color, color1);
							}
							else
							{
								Randomizer randomizer;
								randomizer..ctor((int)buildingID | i << 16);
								switch (randomizer.Int32(4u))
								{
								case 0:
									sharedMaterial.SetColor(instance.ID_Color, sharedMaterial.GetColor("_ColorV0"));
									break;
								case 1:
									sharedMaterial.SetColor(instance.ID_Color, sharedMaterial.GetColor("_ColorV1"));
									break;
								case 2:
									sharedMaterial.SetColor(instance.ID_Color, sharedMaterial.GetColor("_ColorV2"));
									break;
								case 3:
									sharedMaterial.SetColor(instance.ID_Color, sharedMaterial.GetColor("_ColorV3"));
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	public void SetRenderParameters(Vector3 position, Quaternion rotation, Vector4 buildingState, Vector4 objectIndex, int state, Color color)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		BuildingManager expr_0C_cp_0 = instance;
		expr_0C_cp_0.m_drawCallData.m_defaultCalls = expr_0C_cp_0.m_drawCallData.m_defaultCalls + 1;
		Transform transform = base.get_transform();
		transform.set_position(position);
		transform.set_rotation(rotation);
		this.m_animator.SetInteger(instance.ID_State, state);
		this.m_animator.set_speed(Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
		if (this.m_material != null)
		{
			this.m_material.SetVector(instance.ID_BuildingState, buildingState);
			this.m_material.SetVector(instance.ID_ObjectIndex, objectIndex);
			this.m_material.SetColor(instance.ID_Color, color);
		}
		if (this.m_extraMaterial != null)
		{
			this.m_extraMaterial.SetVector(instance.ID_BuildingState, buildingState);
			this.m_extraMaterial.SetVector(instance.ID_ObjectIndex, objectIndex);
			this.m_extraMaterial.SetColor(instance.ID_Color, color);
		}
	}

	public override void CheckReferences()
	{
		this.m_hasParkingSpaces = VehicleInfo.VehicleType.None;
		this.m_maxPropDistance = 0f;
		this.m_treeLayers = 0;
		this.m_collisionHeight = this.m_size.y;
		this.m_lightsOnTerrain = false;
		if (this.m_props != null)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < this.m_props.Length; i++)
			{
				if (this.m_props[i].m_prop != null)
				{
					if (this.m_props[i].m_prop.m_prefabInitialized)
					{
						this.m_props[i].m_finalProp = this.m_props[i].m_prop;
					}
					else
					{
						this.m_props[i].m_finalProp = PrefabCollection<PropInfo>.FindLoaded(this.m_props[i].m_prop.get_gameObject().get_name());
					}
					if (this.m_props[i].m_finalProp == null)
					{
						throw new PrefabException(this, "Referenced prop is not loaded (" + this.m_props[i].m_prop.get_gameObject().get_name() + ")");
					}
					if (!this.m_props[i].m_fixedHeight || this.m_props[i].m_position.y < 16f)
					{
						if (this.m_props[i].m_finalProp.m_specialPlaces != null && this.m_props[i].m_finalProp.m_specialPlaces.Length != 0)
						{
							num3++;
						}
						else
						{
							PropInfo.DoorType doorType = this.m_props[i].m_finalProp.m_doorType;
							if ((doorType & PropInfo.DoorType.Enter) != PropInfo.DoorType.None)
							{
								num++;
							}
							if ((doorType & PropInfo.DoorType.Exit) != PropInfo.DoorType.None)
							{
								num2++;
							}
						}
					}
					if (!this.m_props[i].m_fixedHeight && this.m_props[i].m_finalProp.m_effectLayer == Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer)
					{
						this.m_lightsOnTerrain = true;
					}
					this.m_maxPropDistance = Mathf.Max(this.m_maxPropDistance, this.m_props[i].m_finalProp.m_maxRenderDistance);
					PropInfoGen generatedInfo = this.m_props[i].m_finalProp.m_generatedInfo;
					float num4 = generatedInfo.m_center.y + generatedInfo.m_size.y * 0.5f;
					num4 *= this.m_props[i].m_finalProp.m_maxScale;
					if (this.m_props[i].m_fixedHeight)
					{
						num4 += this.m_props[i].m_position.y;
					}
					this.m_collisionHeight = Mathf.Max(this.m_collisionHeight, num4);
				}
				else if (this.m_props[i].m_tree != null)
				{
					if (this.m_props[i].m_tree.m_prefabInitialized)
					{
						this.m_props[i].m_finalTree = this.m_props[i].m_tree;
					}
					else
					{
						this.m_props[i].m_finalTree = PrefabCollection<TreeInfo>.FindLoaded(this.m_props[i].m_tree.get_gameObject().get_name());
					}
					if (this.m_props[i].m_finalTree == null)
					{
						throw new PrefabException(this, "Referenced tree is not loaded (" + this.m_props[i].m_tree.get_gameObject().get_name() + ")");
					}
					this.m_treeLayers |= 1 << this.m_props[i].m_finalTree.m_prefabDataLayer;
					TreeInfoGen generatedInfo2 = this.m_props[i].m_finalTree.m_generatedInfo;
					float num5 = generatedInfo2.m_center.y + generatedInfo2.m_size.y * 0.5f;
					num5 *= this.m_props[i].m_finalTree.m_maxScale;
					if (this.m_props[i].m_fixedHeight)
					{
						num5 += this.m_props[i].m_position.y;
					}
					this.m_collisionHeight = Mathf.Max(this.m_collisionHeight, num5);
				}
			}
			if (num != 0)
			{
				this.m_enterDoors = new BuildingInfo.Prop[num];
				num = 0;
			}
			if (num2 != 0)
			{
				this.m_exitDoors = new BuildingInfo.Prop[num2];
				num2 = 0;
			}
			if (num3 != 0)
			{
				this.m_specialPlaces = new BuildingInfo.Prop[num3];
				num3 = 0;
			}
			for (int j = 0; j < this.m_props.Length; j++)
			{
				if (this.m_props[j].m_finalProp != null)
				{
					if (this.m_props[j].m_finalProp.m_parkingSpaces != null && this.m_props[j].m_finalProp.m_parkingSpaces.Length != 0)
					{
						for (int k = 0; k < this.m_props[j].m_finalProp.m_parkingSpaces.Length; k++)
						{
							VehicleInfo.VehicleType vehicleType = this.m_props[j].m_finalProp.m_parkingSpaces[k].m_type;
							if (vehicleType == VehicleInfo.VehicleType.None)
							{
								vehicleType = VehicleInfo.VehicleType.Car;
							}
							this.m_hasParkingSpaces |= vehicleType;
						}
					}
					if (!this.m_props[j].m_fixedHeight || this.m_props[j].m_position.y < 16f)
					{
						if (this.m_props[j].m_finalProp.m_specialPlaces != null && this.m_props[j].m_finalProp.m_specialPlaces.Length != 0)
						{
							this.m_specialPlaces[num3++] = this.m_props[j];
						}
						else
						{
							PropInfo.DoorType doorType2 = this.m_props[j].m_finalProp.m_doorType;
							if ((doorType2 & PropInfo.DoorType.Enter) != PropInfo.DoorType.None)
							{
								this.m_enterDoors[num++] = this.m_props[j];
							}
							if ((doorType2 & PropInfo.DoorType.Exit) != PropInfo.DoorType.None)
							{
								this.m_exitDoors[num2++] = this.m_props[j];
							}
						}
					}
				}
			}
		}
	}

	public void CalculateGeneratedInfo()
	{
		MeshFilter[] componentsInChildren = base.GetComponentsInChildren<MeshFilter>(true);
		SkinnedMeshRenderer[] componentsInChildren2 = base.GetComponentsInChildren<SkinnedMeshRenderer>(true);
		if (componentsInChildren.Length == 0 && componentsInChildren2.Length == 0)
		{
			this.m_generatedInfo.m_baseArea = new Vector2[0];
			this.m_generatedInfo.m_baseNormals = new Vector2[0];
			this.m_generatedInfo.m_size = default(Vector3);
			this.m_generatedInfo.m_min = default(Vector3);
			this.m_generatedInfo.m_max = default(Vector3);
			this.m_generatedInfo.m_walkWidth = 0;
			this.m_generatedInfo.m_walkLength = 0;
			this.m_generatedInfo.m_walkLevel = new float[0];
			this.m_generatedInfo.m_heights = new float[0];
			this.m_generatedInfo.m_triangleArea = 0f;
			this.m_generatedInfo.m_uvmapArea = 0f;
			return;
		}
		this.CalculateGeneratedInfo(componentsInChildren, componentsInChildren2);
	}

	public void CalculateGeneratedInfo(MeshFilter[] filters, SkinnedMeshRenderer[] renderers2)
	{
		FastList<Mesh> fastList = new FastList<Mesh>();
		bool flag = false;
		bool flag2 = false;
		for (int i = 0; i < filters.Length; i++)
		{
			if (filters[i].get_sharedMesh() != null)
			{
				fastList.Add(filters[i].get_sharedMesh());
				flag = true;
			}
		}
		for (int j = 0; j < renderers2.Length; j++)
		{
			if (renderers2[j].get_sharedMesh() != null)
			{
				fastList.Add(renderers2[j].get_sharedMesh());
				flag2 = true;
			}
		}
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		float num5 = 0f;
		this.m_generatedInfo.m_min = new Vector3(100000f, 100000f, 100000f);
		this.m_generatedInfo.m_max = new Vector3(-100000f, -100000f, -100000f);
		this.m_generatedInfo.m_triangleArea = 0f;
		this.m_generatedInfo.m_uvmapArea = 0f;
		int num6 = 0;
		int num7 = 0;
		for (int k = 0; k < fastList.m_size; k++)
		{
			Vector3[] vertices = fastList.m_buffer[k].get_vertices();
			Vector2[] uv = fastList.m_buffer[k].get_uv();
			for (int l = 0; l < vertices.Length; l++)
			{
				num3 = Mathf.Max(num3, vertices[l].y);
				this.m_generatedInfo.m_min = Vector3.Min(this.m_generatedInfo.m_min, vertices[l]);
				this.m_generatedInfo.m_max = Vector3.Max(this.m_generatedInfo.m_max, vertices[l]);
			}
			int[] triangles = fastList.m_buffer[k].get_triangles();
			float num8 = 0f;
			float num9 = 0f;
			for (int m = 0; m < triangles.Length; m += 3)
			{
				int num10 = triangles[m];
				int num11 = triangles[m + 1];
				int num12 = triangles[m + 2];
				Vector2 vector = uv[num10];
				Vector2 vector2 = uv[num11];
				Vector2 vector3 = uv[num12];
				Vector2 vector4 = Vector2.Min(Vector2.Min(vector, vector2), vector3);
				Vector2 vector5 = Vector2.Max(Vector2.Max(vector, vector2), vector3);
				if (vector4.x > 1.1f || vector4.y > 1.1f || vector5.x < -0.1f || vector5.y < -0.1f)
				{
					num7++;
				}
				else
				{
					float num13 = Triangle3.Area(vertices[num10], vertices[num11], vertices[num12]);
					float num14 = Triangle2.Area(vector, vector2, vector3);
					if (float.IsNaN(num13) || float.IsNaN(num14))
					{
						num7++;
					}
					else
					{
						num8 += num13;
						num9 += num14;
					}
				}
				if (k == 0)
				{
					bool flag3 = vertices[num10].y > -0.04f && vertices[num10].y < 0.04f;
					bool flag4 = vertices[num11].y > -0.04f && vertices[num11].y < 0.04f;
					bool flag5 = vertices[num12].y > -0.04f && vertices[num12].y < 0.04f;
					if (!flag3 || !flag4 || !flag5)
					{
						if ((flag3 && flag4) || (flag4 && flag5) || (flag5 && flag3))
						{
							num6++;
							if (flag3)
							{
								num = Mathf.Min(num, vertices[num10].x);
								num2 = Mathf.Max(num2, vertices[num10].x);
								num4 = Mathf.Min(num4, vertices[num10].z);
								num5 = Mathf.Max(num5, vertices[num10].z);
							}
							if (flag4)
							{
								num = Mathf.Min(num, vertices[num11].x);
								num2 = Mathf.Max(num2, vertices[num11].x);
								num4 = Mathf.Min(num4, vertices[num11].z);
								num5 = Mathf.Max(num5, vertices[num11].z);
							}
							if (flag5)
							{
								num = Mathf.Min(num, vertices[num12].x);
								num2 = Mathf.Max(num2, vertices[num12].x);
								num4 = Mathf.Min(num4, vertices[num12].z);
								num5 = Mathf.Max(num5, vertices[num12].z);
							}
						}
					}
				}
			}
			if (num8 > this.m_generatedInfo.m_triangleArea)
			{
				this.m_generatedInfo.m_triangleArea = num8;
				this.m_generatedInfo.m_uvmapArea = num9;
			}
		}
		if (num7 != 0)
		{
			Debug.LogWarning(string.Concat(new object[]
			{
				"Suspicious uv coordinates (",
				num7,
				" polygons): ",
				base.get_gameObject().get_name()
			}), base.get_gameObject());
		}
		this.m_generatedInfo.m_size = new Vector3(Mathf.Max(-num, num2) * 2f, num3, Mathf.Max(-num4, num5) * 2f);
		this.m_generatedInfo.m_baseArea = new Vector2[num6 * 2];
		this.m_generatedInfo.m_baseNormals = new Vector2[num6 * 2];
		this.m_generatedInfo.m_heights = new float[289];
		this.m_generatedInfo.m_walkWidth = Mathf.RoundToInt(this.m_generatedInfo.m_size.x);
		this.m_generatedInfo.m_walkLength = Mathf.RoundToInt(this.m_generatedInfo.m_size.z);
		this.m_generatedInfo.m_walkLevel = new float[this.m_generatedInfo.m_walkWidth * this.m_generatedInfo.m_walkLength];
		for (int n = 0; n < this.m_generatedInfo.m_walkLevel.Length; n++)
		{
			this.m_generatedInfo.m_walkLevel[n] = -1000000f;
		}
		float[] array = new float[this.m_generatedInfo.m_walkWidth * this.m_generatedInfo.m_walkLength];
		BoneWeight[] array2 = null;
		if (flag2 && !flag)
		{
			array2 = new BoneWeight[num6 * 2];
		}
		num6 = 0;
		for (int num15 = 0; num15 < fastList.m_size; num15++)
		{
			Vector3[] vertices2 = fastList.m_buffer[num15].get_vertices();
			Vector3[] normals = fastList.m_buffer[num15].get_normals();
			BoneWeight[] array3 = null;
			if (flag2 && !flag)
			{
				array3 = fastList.m_buffer[num15].get_boneWeights();
			}
			int[] triangles2 = fastList.m_buffer[num15].get_triangles();
			for (int num16 = 0; num16 < triangles2.Length; num16 += 3)
			{
				int num17 = triangles2[num16];
				int num18 = triangles2[num16 + 1];
				int num19 = triangles2[num16 + 2];
				if (this.m_generatedInfo.m_max.x > this.m_generatedInfo.m_min.x && this.m_generatedInfo.m_max.z > this.m_generatedInfo.m_min.z)
				{
					Vector3 vector6 = Vector3.Min(Vector3.Min(vertices2[num17], vertices2[num18]), vertices2[num19]);
					Vector3 vector7 = Vector3.Max(Vector3.Max(vertices2[num17], vertices2[num18]), vertices2[num19]);
					int num20 = Mathf.Clamp(Mathf.FloorToInt((vector6.x - this.m_generatedInfo.m_min.x) * 16f / (this.m_generatedInfo.m_max.x - this.m_generatedInfo.m_min.x)), 0, 16);
					int num21 = Mathf.Clamp(Mathf.CeilToInt((vector7.x - this.m_generatedInfo.m_min.x) * 16f / (this.m_generatedInfo.m_max.x - this.m_generatedInfo.m_min.x)), 0, 16);
					int num22 = Mathf.Clamp(Mathf.FloorToInt((vector6.z - this.m_generatedInfo.m_min.z) * 16f / (this.m_generatedInfo.m_max.z - this.m_generatedInfo.m_min.z)), 0, 16);
					int num23 = Mathf.Clamp(Mathf.CeilToInt((vector7.z - this.m_generatedInfo.m_min.z) * 16f / (this.m_generatedInfo.m_max.z - this.m_generatedInfo.m_min.z)), 0, 16);
					for (int num24 = num22; num24 <= num23; num24++)
					{
						for (int num25 = num20; num25 <= num21; num25++)
						{
							Vector3 vector8;
							vector8.x = (float)num25 * (this.m_generatedInfo.m_max.x - this.m_generatedInfo.m_min.x) * 0.0625f + this.m_generatedInfo.m_min.x;
							vector8.y = this.m_generatedInfo.m_max.y + 10f;
							vector8.z = (float)num24 * (this.m_generatedInfo.m_max.z - this.m_generatedInfo.m_min.z) * 0.0625f + this.m_generatedInfo.m_min.z;
							Vector3 vector9 = vector8;
							vector9.y = this.m_generatedInfo.m_min.y - 10f;
							float num26;
							float num27;
							float num28;
							if (Triangle3.Intersect(vertices2[num17], vertices2[num18], vertices2[num19], vector8, vector9, ref num26, ref num27, ref num28))
							{
								this.m_generatedInfo.m_heights[num24 * 17 + num25] = Mathf.Max(this.m_generatedInfo.m_heights[num24 * 17 + num25], vector8.y + (vector9.y - vector8.y) * num28);
							}
						}
					}
				}
				if (this.m_generatedInfo.m_walkWidth != 0 && this.m_generatedInfo.m_walkLength != 0)
				{
					Vector3 vector10 = Vector3.Normalize(Vector3.Cross(vertices2[num18] - vertices2[num17], vertices2[num19] - vertices2[num17]));
					float num29 = Triangle3.Area(vertices2[num17], vertices2[num18], vertices2[num19]);
					if (vector10.y > 0.7f && num29 > 0.01f)
					{
						Vector3 vector11 = Vector3.Min(Vector3.Min(vertices2[num17], vertices2[num18]), vertices2[num19]);
						Vector3 vector12 = Vector3.Max(Vector3.Max(vertices2[num17], vertices2[num18]), vertices2[num19]);
						int num30 = Mathf.Clamp(Mathf.RoundToInt(vector11.x + (float)this.m_generatedInfo.m_walkWidth * 0.5f), 0, this.m_generatedInfo.m_walkWidth - 1);
						int num31 = Mathf.Clamp(Mathf.RoundToInt(vector12.x + (float)this.m_generatedInfo.m_walkWidth * 0.5f), 0, this.m_generatedInfo.m_walkWidth - 1);
						int num32 = Mathf.Clamp(Mathf.RoundToInt(vector11.z + (float)this.m_generatedInfo.m_walkLength * 0.5f), 0, this.m_generatedInfo.m_walkLength - 1);
						int num33 = Mathf.Clamp(Mathf.RoundToInt(vector12.z + (float)this.m_generatedInfo.m_walkLength * 0.5f), 0, this.m_generatedInfo.m_walkLength - 1);
						for (int num34 = num32; num34 <= num33; num34++)
						{
							for (int num35 = num30; num35 <= num31; num35++)
							{
								Vector3 vector13;
								vector13.x = (float)num35 + 0.5f - (float)this.m_generatedInfo.m_walkWidth * 0.5f;
								vector13.y = this.m_generatedInfo.m_max.y + 10f;
								vector13.z = (float)num34 + 0.5f - (float)this.m_generatedInfo.m_walkLength * 0.5f;
								Vector3 vector14 = vector13;
								vector14.y = this.m_generatedInfo.m_min.y - 10f;
								float num36;
								float num37;
								float num38;
								if (Triangle3.Intersect(vertices2[num17], vertices2[num18], vertices2[num19], vector13, vector14, ref num36, ref num37, ref num38))
								{
									int num39 = num34 * this.m_generatedInfo.m_walkWidth + num35;
									float num40 = vector13.y + (vector14.y - vector13.y) * num38;
									float num41 = this.m_generatedInfo.m_walkLevel[num39];
									float num42 = array[num39];
									if (num42 != 0f)
									{
										num41 /= num42;
									}
									if (Mathf.Abs(num40) < Mathf.Abs(num41))
									{
										if (Mathf.Abs(num40 - num41) < 2f)
										{
											this.m_generatedInfo.m_walkLevel[num39] += num40 * num29;
											array[num39] += num29;
										}
										else
										{
											this.m_generatedInfo.m_walkLevel[num39] = num40 * num29;
											array[num39] = num29;
										}
									}
								}
							}
						}
					}
				}
				if (num15 == 0)
				{
					bool flag6 = vertices2[num17].y > -0.04f && vertices2[num17].y < 0.04f;
					bool flag7 = vertices2[num18].y > -0.04f && vertices2[num18].y < 0.04f;
					bool flag8 = vertices2[num19].y > -0.04f && vertices2[num19].y < 0.04f;
					if (!flag6 || !flag7 || !flag8)
					{
						if (flag6 && flag7)
						{
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num17].x, vertices2[num17].z);
							Vector2[] arg_FD3_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_FD3_0_cp_1 = num6;
							Vector2 vector15;
							vector15..ctor(normals[num17].x, normals[num17].z);
							arg_FD3_0_cp_0[arg_FD3_0_cp_1] = vector15.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num17];
							}
							num6++;
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num18].x, vertices2[num18].z);
							Vector2[] arg_10C9_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_10C9_0_cp_1 = num6;
							Vector2 vector16;
							vector16..ctor(normals[num18].x, normals[num18].z);
							arg_10C9_0_cp_0[arg_10C9_0_cp_1] = vector16.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num18];
							}
							num6++;
						}
						else if (flag7 && flag8)
						{
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num18].x, vertices2[num18].z);
							Vector2[] arg_11D2_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_11D2_0_cp_1 = num6;
							Vector2 vector17;
							vector17..ctor(normals[num18].x, normals[num18].z);
							arg_11D2_0_cp_0[arg_11D2_0_cp_1] = vector17.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num18];
							}
							num6++;
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num19].x, vertices2[num19].z);
							Vector2[] arg_12C8_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_12C8_0_cp_1 = num6;
							Vector2 vector18;
							vector18..ctor(normals[num19].x, normals[num19].z);
							arg_12C8_0_cp_0[arg_12C8_0_cp_1] = vector18.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num19];
							}
							num6++;
						}
						else if (flag8 && flag6)
						{
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num19].x, vertices2[num19].z);
							Vector2[] arg_13D1_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_13D1_0_cp_1 = num6;
							Vector2 vector19;
							vector19..ctor(normals[num19].x, normals[num19].z);
							arg_13D1_0_cp_0[arg_13D1_0_cp_1] = vector19.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num19];
							}
							num6++;
							this.m_generatedInfo.m_baseArea[num6] = new Vector2(vertices2[num17].x, vertices2[num17].z);
							Vector2[] arg_14C7_0_cp_0 = this.m_generatedInfo.m_baseNormals;
							int arg_14C7_0_cp_1 = num6;
							Vector2 vector20;
							vector20..ctor(normals[num17].x, normals[num17].z);
							arg_14C7_0_cp_0[arg_14C7_0_cp_1] = vector20.get_normalized();
							if (this.m_generatedInfo.m_baseNormals[num6].get_sqrMagnitude() < 0.01f)
							{
								this.m_generatedInfo.m_baseNormals[num6] = this.m_generatedInfo.m_baseArea[num6].get_normalized();
							}
							if (flag2 && !flag)
							{
								array2[num6] = array3[num17];
							}
							num6++;
						}
					}
				}
			}
		}
		for (int num43 = 0; num43 < this.m_generatedInfo.m_walkLevel.Length; num43++)
		{
			float num44 = array[num43];
			if (num44 != 0f)
			{
				this.m_generatedInfo.m_walkLevel[num43] /= num44;
			}
		}
		Bounds localBounds = default(Bounds);
		if (num6 != 0)
		{
			for (int num45 = 0; num45 < fastList.m_size; num45++)
			{
				if (num45 == 0)
				{
					Vector3[] vertices3 = fastList.m_buffer[num45].get_vertices();
					Vector3[] normals2 = fastList.m_buffer[num45].get_normals();
					Vector4[] tangents = fastList.m_buffer[num45].get_tangents();
					Color32[] colors = fastList.m_buffer[num45].get_colors32();
					Vector2[] uv2 = fastList.m_buffer[num45].get_uv();
					BoneWeight[] array4 = null;
					if (flag2 && !flag)
					{
						array4 = fastList.m_buffer[num45].get_boneWeights();
					}
					int[] triangles3 = fastList.m_buffer[num45].get_triangles();
					Vector3[] array5 = new Vector3[vertices3.Length + num6 * 2];
					Vector3[] array6 = new Vector3[vertices3.Length + num6 * 2];
					Vector4[] array7 = new Vector4[vertices3.Length + num6 * 2];
					Color32[] array8 = new Color32[vertices3.Length + num6 * 2];
					Vector2[] array9 = new Vector2[vertices3.Length + num6 * 2];
					BoneWeight[] array10 = null;
					if (flag2 && !flag)
					{
						array10 = new BoneWeight[vertices3.Length + num6 * 2];
					}
					int[] array11 = new int[triangles3.Length + num6 * 3];
					Array.Copy(vertices3, array5, vertices3.Length);
					Array.Copy(normals2, array6, normals2.Length);
					Array.Copy(tangents, array7, tangents.Length);
					Array.Copy(uv2, array9, uv2.Length);
					if (flag2 && !flag)
					{
						Array.Copy(array4, array10, array4.Length);
					}
					Array.Copy(triangles3, array11, triangles3.Length);
					int num46 = vertices3.Length;
					int num47 = triangles3.Length;
					for (int num48 = 0; num48 < vertices3.Length; num48++)
					{
						Color32 color;
						if (num48 < colors.Length)
						{
							color = colors[num48];
							color.a = 0;
						}
						else
						{
							color..ctor(255, 255, 255, 0);
						}
						array8[num48] = color;
					}
					Color32 color2;
					color2..ctor(255, 255, 255, 255);
					float num49 = 1f;
					float num50 = 0f;
					for (int num51 = 0; num51 < this.m_generatedInfo.m_baseArea.Length; num51 += 2)
					{
						Vector3 vector21;
						vector21..ctor(this.m_generatedInfo.m_baseArea[num51].x, 0f, this.m_generatedInfo.m_baseArea[num51].y);
						Vector3 vector22;
						vector22..ctor(this.m_generatedInfo.m_baseArea[num51 + 1].x, 0f, this.m_generatedInfo.m_baseArea[num51 + 1].y);
						Vector3 vector23;
						vector23..ctor(this.m_generatedInfo.m_baseNormals[num51].x, 0f, this.m_generatedInfo.m_baseNormals[num51].y);
						Vector3 vector24;
						vector24..ctor(this.m_generatedInfo.m_baseNormals[num51 + 1].x, 0f, this.m_generatedInfo.m_baseNormals[num51 + 1].y);
						float num52 = Vector3.Distance(vector21, vector22);
						float num53 = num50 + num52 * 0.125f;
						array5[num46] = vector21 - new Vector3(0f, num49, 0f);
						array6[num46] = vector23;
						array7[num46] = new Vector4(vector23.z, 0f, -vector23.x, 1f);
						array8[num46] = color2;
						array9[num46] = new Vector2(num50, -0.125f);
						if (flag2 && !flag)
						{
							array10[num46] = array2[num51];
						}
						num46++;
						array5[num46] = vector21;
						array6[num46] = vector23;
						array7[num46] = new Vector4(vector23.z, 0f, -vector23.x, 1f);
						array8[num46] = color2;
						array9[num46] = new Vector2(num50, 0f);
						if (flag2 && !flag)
						{
							array10[num46] = array2[num51];
						}
						num46++;
						array5[num46] = vector22 - new Vector3(0f, num49, 0f);
						array6[num46] = vector24;
						array7[num46] = new Vector4(vector24.z, 0f, -vector24.x, 1f);
						array8[num46] = color2;
						array9[num46] = new Vector2(num53, -0.125f);
						if (flag2 && !flag)
						{
							array10[num46] = array2[num51 + 1];
						}
						num46++;
						array5[num46] = vector22;
						array6[num46] = vector24;
						array7[num46] = new Vector4(vector24.z, 0f, -vector24.x, 1f);
						array8[num46] = color2;
						array9[num46] = new Vector2(num53, 0f);
						if (flag2 && !flag)
						{
							array10[num46] = array2[num51 + 1];
						}
						num46++;
						array11[num47++] = num46 - 2;
						array11[num47++] = num46 - 3;
						array11[num47++] = num46 - 4;
						array11[num47++] = num46 - 1;
						array11[num47++] = num46 - 3;
						array11[num47++] = num46 - 2;
						num50 = num53;
					}
					fastList.m_buffer[num45].Clear(false);
					fastList.m_buffer[num45].set_vertices(array5);
					fastList.m_buffer[num45].set_normals(array6);
					fastList.m_buffer[num45].set_tangents(array7);
					fastList.m_buffer[num45].set_colors32(array8);
					fastList.m_buffer[num45].set_uv(array9);
					if (flag2 && !flag)
					{
						fastList.m_buffer[num45].set_boneWeights(array10);
						Vector2[] array12 = new Vector2[array5.Length];
						Vector2[] array13 = new Vector2[array5.Length];
						for (int num54 = 0; num54 < array5.Length; num54++)
						{
							Vector3 vector25 = array5[num54];
							array12[num54] = new Vector2(vector25.x, vector25.y);
							array13[num54] = new Vector2(vector25.z, 1f);
						}
						fastList.m_buffer[num45].set_uv2(array12);
						fastList.m_buffer[num45].set_uv3(array13);
					}
					fastList.m_buffer[num45].set_triangles(array11);
					fastList.m_buffer[num45].UploadMeshData(false);
					Bounds bounds = fastList.m_buffer[num45].get_bounds();
					Vector3 min = bounds.get_min();
					min.y = Mathf.Min(min.y, -16f);
					bounds.set_min(min);
					fastList.m_buffer[num45].set_bounds(bounds);
					localBounds.Encapsulate(bounds);
				}
				else
				{
					Bounds bounds2 = fastList.m_buffer[num45].get_bounds();
					localBounds.Encapsulate(bounds2);
				}
			}
		}
		for (int num55 = 0; num55 < renderers2.Length; num55++)
		{
			renderers2[num55].set_localBounds(localBounds);
		}
	}

	private void OnDrawGizmosSelected()
	{
		if (this.m_instanceID.IsEmpty)
		{
			Gizmos.set_matrix(base.get_transform().get_localToWorldMatrix());
			if (this.m_generatedInfo.m_baseArea != null && this.m_generatedInfo.m_baseArea.Length != 0)
			{
				Gizmos.set_color(Color.get_red());
				for (int i = 0; i < this.m_generatedInfo.m_baseArea.Length; i += 2)
				{
					Vector3 vector;
					vector..ctor(this.m_generatedInfo.m_baseArea[i].x, 0f, this.m_generatedInfo.m_baseArea[i].y);
					Vector3 vector2;
					vector2..ctor(this.m_generatedInfo.m_baseArea[i + 1].x, 0f, this.m_generatedInfo.m_baseArea[i + 1].y);
					Vector3 vector3;
					vector3..ctor(this.m_generatedInfo.m_baseArea[i].x, -1f, this.m_generatedInfo.m_baseArea[i].y);
					Vector3 vector4;
					vector4..ctor(this.m_generatedInfo.m_baseArea[i + 1].x, -1f, this.m_generatedInfo.m_baseArea[i + 1].y);
					Gizmos.DrawLine(vector, vector2);
					Gizmos.DrawLine(vector3, vector4);
					Gizmos.DrawLine(vector, vector3);
					Gizmos.DrawLine(vector2, vector4);
				}
			}
			Gizmos.set_color(Color.get_yellow());
			Gizmos.DrawWireCube(new Vector3(0f, this.m_generatedInfo.m_size.y * 0.5f, 0f), new Vector3((float)this.m_cellWidth * 8f, this.m_generatedInfo.m_size.y, (float)this.m_cellLength * 8f));
			int num = this.m_cellLength;
			int num2 = 0;
			int num3 = 0;
			if (num < 4)
			{
				if (this.m_expandFrontYard)
				{
					num3 = 4 - num;
				}
				else
				{
					num2 = 4 - num;
				}
				num = 4;
			}
			for (int j = 0; j < num; j++)
			{
				for (int k = 0; k < this.m_cellWidth; k++)
				{
					if (j < num2 || j >= num - num3)
					{
						Gizmos.set_color(((k + j & 1) != 0) ? new Color(0.2f, 0.4f, 1f, 0.75f) : new Color(0.1f, 0.2f, 0.5f, 0.75f));
					}
					else if (this.m_fullPavement)
					{
						Gizmos.set_color(((k + j & 1) != 0) ? new Color(0.7f, 0.7f, 0.7f, 0.75f) : new Color(0.5f, 0.5f, 0.5f, 0.75f));
					}
					else if (this.m_fullGravel)
					{
						Gizmos.set_color(((k + j & 1) != 0) ? new Color(0.7f, 0.4f, 0.2f, 0.75f) : new Color(0.5f, 0.2f, 0.1f, 0.75f));
					}
					else
					{
						Gizmos.set_color(((k + j & 1) != 0) ? new Color(0.4f, 1f, 0.2f, 0.75f) : new Color(0.2f, 0.5f, 0.1f, 0.75f));
					}
					Gizmos.DrawCube(new Vector3(((float)k - (float)this.m_cellWidth * 0.5f + 0.5f) * 8f, -0.5f, ((float)j - (float)this.m_cellLength * 0.5f - (float)num2 + 0.5f) * 8f), new Vector3(8f, 1f, 8f));
				}
			}
			Gizmos.set_color(new Color(0.6f, 0.6f, 0.6f, 0.75f));
			Gizmos.DrawCube(new Vector3(0f, -0.5f, (float)this.m_cellLength * 4f + (float)num3 * 8f + 1.5f), new Vector3(128f, 1f, 3f));
			Gizmos.DrawCube(new Vector3(0f, -0.5f, (float)this.m_cellLength * 4f + (float)num3 * 8f + 14.5f), new Vector3(128f, 1f, 3f));
			Gizmos.set_color(new Color(0.2f, 0.2f, 0.2f, 0.75f));
			Gizmos.DrawCube(new Vector3(0f, -0.5f, (float)this.m_cellLength * 4f + (float)num3 * 8f + 8f), new Vector3(128f, 1f, 10f));
			if (this.m_pavementAreas != null)
			{
				Gizmos.set_color(Color.get_black());
				for (int l = 0; l < this.m_pavementAreas.Length; l += 4)
				{
					Vector3 vector5;
					vector5..ctor(this.m_pavementAreas[l].x, 0f, this.m_pavementAreas[l].y);
					Vector3 vector6;
					vector6..ctor(this.m_pavementAreas[l + 1].x, 0f, this.m_pavementAreas[l + 1].y);
					Vector3 vector7;
					vector7..ctor(this.m_pavementAreas[l + 2].x, 0f, this.m_pavementAreas[l + 2].y);
					Vector3 vector8;
					vector8..ctor(this.m_pavementAreas[l + 3].x, 0f, this.m_pavementAreas[l + 3].y);
					Gizmos.DrawLine(vector5, vector6);
					Gizmos.DrawLine(vector6, vector7);
					Gizmos.DrawLine(vector7, vector8);
					Gizmos.DrawLine(vector8, vector5);
				}
			}
			if (base.GetComponent<Renderer>() != null && BuildingInfo.m_renderFloors)
			{
				Material sharedMaterial = base.GetComponent<Renderer>().get_sharedMaterial();
				if (sharedMaterial != null)
				{
					Vector4 vector9 = sharedMaterial.GetVector("_FloorParams");
					Gizmos.set_color(new Color(1f, 0.3f, 0.3f, 0.5f));
					if (vector9.x + vector9.y - 0.1f < vector9.w + vector9.z)
					{
						Gizmos.DrawCube(new Vector3(0f, vector9.x - 0.1f, 0f), new Vector3(this.m_generatedInfo.m_size.x, 0.2f, this.m_generatedInfo.m_size.z));
					}
					float num4 = vector9.y - 0.1f;
					int num5 = 1;
					while (num4 < vector9.w && num4 < this.m_generatedInfo.m_size.y)
					{
						Gizmos.set_color(((num5 & 1) != 0) ? new Color(1f, 1f, 0.3f, 0.5f) : new Color(1f, 0.3f, 0.3f, 0.5f));
						Gizmos.DrawCube(new Vector3(0f, num4, 0f), new Vector3(this.m_generatedInfo.m_size.x, 0.2f, this.m_generatedInfo.m_size.z));
						num4 += vector9.z;
						if (vector9.z < 0.01f)
						{
							break;
						}
						num5++;
					}
				}
			}
			if (this.m_paths != null)
			{
				for (int m = 0; m < this.m_paths.Length; m++)
				{
					BuildingInfo.PathInfo pathInfo = this.m_paths[m];
					if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null)
					{
						for (int n = 0; n < pathInfo.m_nodes.Length; n++)
						{
							if (pathInfo.m_forbidLaneConnection != null && pathInfo.m_forbidLaneConnection.Length > n && pathInfo.m_forbidLaneConnection[n])
							{
								Gizmos.set_color(new Color(1f, 0.3f, 0.3f, 0.5f));
							}
							else
							{
								Gizmos.set_color(new Color(0.3f, 1f, 1f, 0.5f));
							}
							Vector3 vector10 = pathInfo.m_nodes[n];
							vector10.z = -vector10.z;
							Gizmos.DrawSphere(vector10, pathInfo.m_netInfo.m_halfWidth);
							if (n != 0)
							{
								Gizmos.set_color(new Color(0.3f, 1f, 1f, 0.5f));
								Vector3 vector11 = pathInfo.m_nodes[n - 1];
								vector11.z = -vector11.z;
								Vector3 vector12;
								vector12..ctor(vector10.z - vector11.z, 0f, vector11.x - vector10.x);
								Vector3 vector13 = vector12.get_normalized() * pathInfo.m_netInfo.m_halfWidth;
								Gizmos.DrawLine(vector11, vector10);
								Gizmos.DrawLine(vector11 - vector13, vector10 - vector13);
								Gizmos.DrawLine(vector11 + vector13, vector10 + vector13);
								Gizmos.DrawLine(vector11 - vector13, vector11 + vector13);
								Gizmos.DrawLine(vector10 - vector13, vector10 + vector13);
							}
						}
					}
				}
			}
			if (this.m_props != null)
			{
				for (int num6 = 0; num6 < this.m_props.Length; num6++)
				{
					if (this.m_props[num6].m_prop != null && this.m_props[num6].m_prop.GetComponent<Renderer>() == null)
					{
						Gizmos.set_color(new Color(1f, 0f, 1f, 0.5f));
						Vector3 one = Vector3.get_one();
						Vector3 position = this.m_props[num6].m_position;
						position.y += this.m_props[num6].m_prop.m_generatedInfo.m_size.y * 0.5f;
						Matrix4x4 matrix4x = default(Matrix4x4);
						matrix4x.SetTRS(position, Quaternion.AngleAxis(this.m_props[num6].m_angle, Vector3.get_down()), one);
						Gizmos.set_matrix(base.get_transform().get_localToWorldMatrix() * matrix4x);
						Gizmos.DrawCube(Vector3.get_zero(), this.m_props[num6].m_prop.m_generatedInfo.m_size);
					}
				}
			}
			if (this.m_subBuildings != null)
			{
				for (int num7 = 0; num7 < this.m_subBuildings.Length; num7++)
				{
					if (this.m_subBuildings[num7].m_buildingInfo != null)
					{
						Matrix4x4 matrix4x2 = default(Matrix4x4);
						matrix4x2.SetTRS(this.m_subBuildings[num7].m_position, Quaternion.AngleAxis(this.m_subBuildings[num7].m_angle, Vector3.get_down()), Vector3.get_one());
						Gizmos.set_matrix(base.get_transform().get_localToWorldMatrix() * matrix4x2);
						Gizmos.set_color(Color.get_yellow());
						Gizmos.DrawWireCube(new Vector3(0f, this.m_subBuildings[num7].m_buildingInfo.m_generatedInfo.m_size.y * 0.5f, 0f), new Vector3((float)this.m_subBuildings[num7].m_buildingInfo.m_cellWidth * 8f, this.m_subBuildings[num7].m_buildingInfo.m_generatedInfo.m_size.y, (float)this.m_subBuildings[num7].m_buildingInfo.m_cellLength * 8f));
					}
				}
			}
		}
	}

	protected override void LateUpdate()
	{
		if (this.m_instanceID.IsEmpty)
		{
			Shader.SetGlobalFloat("_MarkerAlpha", 1f);
			if (this.m_props != null)
			{
				for (int i = 0; i < this.m_props.Length; i++)
				{
					if (this.m_props[i].m_prop != null)
					{
						if (this.m_props[i].m_prop.GetComponent<Renderer>() != null)
						{
							if (this.m_props[i].m_prop.m_mesh == null)
							{
								this.m_props[i].m_prop.m_mesh = this.m_props[i].m_prop.GetComponent<MeshFilter>().get_sharedMesh();
							}
							if (this.m_props[i].m_prop.m_mesh != null)
							{
								if (this.m_props[i].m_prop.m_material == null)
								{
									this.m_props[i].m_prop.m_material = this.m_props[i].m_prop.GetComponent<Renderer>().get_sharedMaterial();
									this.m_props[i].m_prop.m_isDecal = (this.m_props[i].m_prop.m_material.GetTag("RenderType", false) == "Decal");
								}
								Material material = this.m_props[i].m_prop.m_material;
								Vector3 one = Vector3.get_one();
								if (this.m_props[i].m_prop.m_isDecal)
								{
									if (BuildingInfo.m_propDecalSceneMaterial == null)
									{
										BuildingInfo.m_propDecalSceneMaterial = new Material(Shader.Find("Custom/Props/Decal/Scene"));
									}
									BuildingInfo.m_propDecalSceneMaterial.CopyPropertiesFromMaterial(material);
									material = BuildingInfo.m_propDecalSceneMaterial;
									one..ctor(1f, 0.01f, 1f);
								}
								Matrix4x4 matrix4x = default(Matrix4x4);
								matrix4x.SetTRS(this.m_props[i].m_position, Quaternion.AngleAxis(this.m_props[i].m_angle, Vector3.get_down()), one);
								matrix4x = base.get_transform().get_localToWorldMatrix() * matrix4x;
								Graphics.DrawMesh(this.m_props[i].m_prop.m_mesh, matrix4x, material, this.m_props[i].m_prop.get_gameObject().get_layer());
							}
						}
					}
					else if (this.m_props[i].m_tree != null)
					{
						if (this.m_props[i].m_tree.m_mesh == null)
						{
							this.m_props[i].m_tree.m_mesh = this.m_props[i].m_tree.GetComponent<MeshFilter>().get_sharedMesh();
						}
						if (this.m_props[i].m_tree.m_material == null)
						{
							this.m_props[i].m_tree.m_material = this.m_props[i].m_tree.GetComponent<Renderer>().get_sharedMaterial();
						}
						Matrix4x4 matrix4x2 = default(Matrix4x4);
						matrix4x2.SetTRS(this.m_props[i].m_position, Quaternion.get_identity(), Vector3.get_one());
						matrix4x2 = base.get_transform().get_localToWorldMatrix() * matrix4x2;
						Graphics.DrawMesh(this.m_props[i].m_tree.m_mesh, matrix4x2, this.m_props[i].m_tree.m_material, this.m_props[i].m_tree.get_gameObject().get_layer());
					}
				}
			}
			if (this.m_subMeshes != null)
			{
				for (int j = 0; j < this.m_subMeshes.Length; j++)
				{
					if (this.m_subMeshes[j].m_subInfo != null)
					{
						if (this.m_subMeshes[j].m_subInfo.m_mesh == null)
						{
							this.m_subMeshes[j].m_subInfo.m_mesh = this.m_subMeshes[j].m_subInfo.GetComponent<MeshFilter>().get_sharedMesh();
						}
						if (this.m_subMeshes[j].m_subInfo.m_material == null)
						{
							this.m_subMeshes[j].m_subInfo.m_material = this.m_subMeshes[j].m_subInfo.GetComponent<Renderer>().get_sharedMaterial();
						}
						Matrix4x4 matrix4x3 = default(Matrix4x4);
						matrix4x3.SetTRS(this.m_subMeshes[j].m_position, Quaternion.AngleAxis(this.m_subMeshes[j].m_angle, Vector3.get_down()), Vector3.get_one());
						matrix4x3 = base.get_transform().get_localToWorldMatrix() * matrix4x3;
						Graphics.DrawMesh(this.m_subMeshes[j].m_subInfo.m_mesh, matrix4x3, this.m_subMeshes[j].m_subInfo.m_material, this.m_subMeshes[j].m_subInfo.get_gameObject().get_layer());
					}
				}
			}
			if (this.m_subBuildings != null)
			{
				for (int k = 0; k < this.m_subBuildings.Length; k++)
				{
					if (this.m_subBuildings[k].m_buildingInfo != null)
					{
						if (this.m_subBuildings[k].m_buildingInfo.m_mesh == null)
						{
							MeshFilter component = this.m_subBuildings[k].m_buildingInfo.GetComponent<MeshFilter>();
							if (!(component != null))
							{
								goto IL_65D;
							}
							this.m_subBuildings[k].m_buildingInfo.m_mesh = component.get_sharedMesh();
						}
						if (this.m_subBuildings[k].m_buildingInfo.m_material == null)
						{
							Renderer component2 = this.m_subBuildings[k].m_buildingInfo.GetComponent<Renderer>();
							if (!(component2 != null))
							{
								goto IL_65D;
							}
							this.m_subBuildings[k].m_buildingInfo.m_material = component2.get_sharedMaterial();
						}
						Matrix4x4 matrix4x4 = default(Matrix4x4);
						matrix4x4.SetTRS(this.m_subBuildings[k].m_position, Quaternion.AngleAxis(this.m_subBuildings[k].m_angle, Vector3.get_down()), Vector3.get_one());
						matrix4x4 = base.get_transform().get_localToWorldMatrix() * matrix4x4;
						Graphics.DrawMesh(this.m_subBuildings[k].m_buildingInfo.m_mesh, matrix4x4, this.m_subBuildings[k].m_buildingInfo.m_material, this.m_subBuildings[k].m_buildingInfo.get_gameObject().get_layer());
					}
					IL_65D:;
				}
			}
		}
		else
		{
			base.LateUpdate();
		}
	}

	public override void InitMeshData(Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aciAtlas)
	{
		base.InitMeshData(atlasRect, rgbAtlas, xysAtlas, aciAtlas);
		int num = 0;
		if (this.m_lodMeshData != null && this.m_lodMeshData.m_vertices != null)
		{
			num = this.m_lodMeshData.m_vertices.Length;
		}
		int num2 = Mathf.Max(new int[]
		{
			2 + this.m_cellLength + this.m_cellWidth
		}) * 80;
		if (num > num2)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, StringUtils.SafeFormat("LOD vertex count high ({0} > {1}): {2}", new object[]
			{
				num,
				num2,
				base.get_gameObject().get_name()
			}), base.get_gameObject());
		}
	}

	public override void ReadMesh(out Vector3[] vertices, out int[] triangles)
	{
		Mesh mesh = this.m_mesh;
		if (mesh != null)
		{
			try
			{
				vertices = mesh.get_vertices();
				triangles = mesh.get_triangles();
				return;
			}
			catch (UnityException)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Failed to read building mesh!");
			}
		}
		vertices = null;
		triangles = null;
	}

	public override void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
		BuildingDecoration.RenderBuildingMesh(cameraInfo, this);
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("BUILDING_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("BUILDING_TITLE", base.get_gameObject().get_name());
	}

	public override string GetUncheckedLocalizedTitle()
	{
		if (Locale.Exists("BUILDING_TITLE", base.get_gameObject().get_name()))
		{
			return Locale.Get("BUILDING_TITLE", base.get_gameObject().get_name());
		}
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("BUILDING_DESC", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescriptionShort()
	{
		return Locale.Get("BUILDING_SHORT_DESC", base.get_gameObject().get_name());
	}

	public override void LoadDecorations()
	{
		BuildingDecoration.LoadDecorations(this);
	}

	public override void SaveDecorations()
	{
		BuildingDecoration.SaveDecorations(this);
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		int width;
		int length;
		float num;
		this.GetDecorationArea(out width, out length, out num);
		float minX = Mathf.Min(heightArea.m_min.x, surfaceArea.m_min.x);
		float minZ = Mathf.Min(heightArea.m_min.z, surfaceArea.m_min.z);
		float maxX = Mathf.Max(heightArea.m_max.x, surfaceArea.m_max.x);
		float maxZ = Mathf.Max(heightArea.m_max.z, surfaceArea.m_max.z);
		Vector3 position;
		position..ctor(0f, 60f, num);
		Building.TerrainUpdated(this, 0, position, 0f, width, length, minX, minZ, maxX, maxZ, false);
	}

	public override void EnsureCellSurface()
	{
		BuildingDecoration.EnsureCellSurface(this);
	}

	public override void SetCellSurface(int index, TerrainModify.Surface surface)
	{
		this.m_cellSurfaces[index] = surface;
	}

	public override int GetWidth()
	{
		return this.m_cellWidth;
	}

	public override int GetLength()
	{
		return this.m_cellLength;
	}

	public override void SetWidth(int width)
	{
		this.m_cellWidth = width;
	}

	public override void SetLength(int length)
	{
		this.m_cellLength = length;
	}

	public override void GetWidthRange(out int minWidth, out int maxWidth)
	{
		this.m_buildingAI.GetWidthRange(out minWidth, out maxWidth);
	}

	public override void GetLengthRange(out int minLength, out int maxLength)
	{
		this.m_buildingAI.GetLengthRange(out minLength, out maxLength);
	}

	public override void GetDecorationArea(out int width, out int length, out float offset)
	{
		this.m_buildingAI.GetDecorationArea(out width, out length, out offset);
	}

	public override void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		this.m_buildingAI.GetDecorationDirections(out negX, out posX, out negZ, out posZ);
	}

	public override PrefabAI GetAI()
	{
		return this.m_buildingAI;
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public override MilestoneInfo GetUnlockMilestone()
	{
		if (this.m_buildingAI != null)
		{
			return this.m_buildingAI.GetUnlockMilestone();
		}
		return this.m_UnlockMilestone;
	}

	public override bool CanBeBuilt()
	{
		return this.m_buildingAI.CanBeBuilt();
	}

	public override int GetConstructionCost()
	{
		return this.m_buildingAI.GetConstructionCost();
	}

	public override int GetMaintenanceCost()
	{
		return this.m_buildingAI.GetMaintenanceCost();
	}

	public bool IgnoreBuildingCollision()
	{
		return this.m_buildingAI.IgnoreBuildingCollision();
	}

	public const int WidthCount = 4;

	public const int LengthCount = 4;

	public ItemClass m_class;

	public MilestoneInfo m_UnlockMilestone;

	public ItemClass.Placement m_placementStyle;

	[CustomizableProperty("Availability", "Building general")]
	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	public bool m_AssetEditorTemplate = true;

	public bool m_AssetEditorPillarTemplate;

	public BuildingInfo.PlacementMode m_placementMode;

	public BuildingInfo.ZoningMode m_zoningMode;

	public float m_placementOffset;

	public int m_cellWidth;

	public int m_cellLength;

	[CustomizableProperty("Circular", "Building general")]
	public bool m_circular;

	public bool m_fullPavement;

	public bool m_fullGravel;

	public bool m_clipTerrain;

	[CustomizableProperty("Expand front yard", "Building general")]
	public bool m_expandFrontYard;

	public bool m_useColorVariations = true;

	[CustomizableProperty("Flatten terrain", "Building general")]
	public bool m_flattenTerrain = true;

	[CustomizableProperty("Flatten full area", "Building general")]
	public bool m_flattenFullArea;

	public bool m_weakTerrainRuining;

	public bool m_autoRemove;

	public bool m_randomEffectTiming;

	public Vector2[] m_pavementAreas;

	public TerrainModify.Surface[] m_cellSurfaces;

	public BuildingInfo.Prop[] m_props;

	public BuildingInfo.MeshInfo[] m_subMeshes;

	public BuildingInfo.SubInfo[] m_subBuildings;

	public BuildingInfo.PathInfo[] m_paths;

	public AudioInfo m_customLoopSound;

	[NonSerialized]
	public BuildingAI m_buildingAI;

	[NonSerialized]
	public BuildingInfoSub m_collapsedInfo;

	[NonSerialized]
	public int m_collapsedRotations;

	[NonSerialized]
	public Color m_color0;

	[NonSerialized]
	public Color m_color1;

	[NonSerialized]
	public Color m_color2;

	[NonSerialized]
	public Color m_color3;

	[NonSerialized]
	public BuildingTypeGuide m_notUsedGuide;

	[NonSerialized]
	public float m_renderSize;

	[NonSerialized]
	public float m_collisionHeight;

	[NonSerialized]
	public Vector3 m_centerOffset;

	[NonSerialized]
	public bool m_lodMissing;

	[NonSerialized]
	public VehicleInfo.VehicleType m_hasParkingSpaces;

	[NonSerialized]
	public int m_treeLayers;

	[NonSerialized]
	public BuildingInfo.Prop[] m_enterDoors;

	[NonSerialized]
	public BuildingInfo.Prop[] m_exitDoors;

	[NonSerialized]
	public BuildingInfo.Prop[] m_specialPlaces;

	[NonSerialized]
	public float m_maxPropDistance;

	[NonSerialized]
	public float m_maxHeightOffset;

	[NonSerialized]
	public Vector3 m_size;

	[NonSerialized]
	public bool m_hasPedestrianPaths;

	[NonSerialized]
	public SavedBool m_unlocked;

	[NonSerialized]
	public bool m_alreadyUnlocked;

	[NonSerialized]
	public bool m_fixedHeight;

	[NonSerialized]
	public bool m_dontSpawnNormally;

	[NonSerialized]
	public bool m_lightsOnTerrain;

	[NonSerialized]
	public bool m_anyMeshRequireWaterMap;

	[NonSerialized]
	private Animator m_animator;

	[NonSerialized]
	private Renderer[] m_renderers;

	[NonSerialized]
	private InfoManager.InfoMode m_lastInfoMode;

	[NonSerialized]
	private InfoManager.SubInfoMode m_lastSubInfoMode;

	[NonSerialized]
	public bool m_instanceDirty;

	[NonSerialized]
	public static bool m_renderFloors;

	[NonSerialized]
	private static Material m_propDecalSceneMaterial;

	public enum PlacementMode
	{
		Roadside,
		Shoreline,
		OnWater,
		OnGround,
		OnSurface,
		OnTerrain,
		ShorelineOrGround
	}

	public enum ZoningMode
	{
		Straight,
		CornerLeft,
		CornerRight
	}

	public enum TrafficLights
	{
		Default,
		ForceOn,
		ForceOff
	}

	[Serializable]
	public class Prop
	{
		public PropInfo m_prop;

		public TreeInfo m_tree;

		public Vector3 m_position;

		public float m_angle;

		public int m_probability = 100;

		public bool m_fixedHeight;

		[NonSerialized]
		public PropInfo m_finalProp;

		[NonSerialized]
		public TreeInfo m_finalTree;

		[NonSerialized]
		public int m_requiredLength;

		[NonSerialized]
		public float m_radAngle;

		[NonSerialized]
		public int m_index;
	}

	[Serializable]
	public class MeshInfo
	{
		public BuildingInfoBase m_subInfo;

		public Building.Flags m_flagsRequired;

		public Building.Flags m_flagsForbidden;

		public Vector3 m_position;

		public float m_angle;

		[NonSerialized]
		public Matrix4x4 m_matrix;
	}

	[Serializable]
	public class SubInfo
	{
		public BuildingInfo m_buildingInfo;

		public Vector3 m_position;

		public float m_angle;

		public bool m_fixedHeight;
	}

	[Serializable]
	public class PathInfo
	{
		public NetInfo m_netInfo;

		public Vector3[] m_nodes;

		public Vector3[] m_curveTargets;

		public bool[] m_forbidLaneConnection;

		public BuildingInfo.TrafficLights[] m_trafficLights;

		public bool[] m_yieldSigns;

		public float m_maxSnapDistance = 0.1f;

		public bool m_invertSegments;
	}
}
