﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class ParkAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_entertainmentAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.Entertainment;
			radius1 = this.m_entertainmentRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Entertainment)
		{
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.Connections)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode != InfoManager.SubInfoMode.WindPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (data.m_tempExport != 0 || data.m_finalExport != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Entertainment;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, 0, num * 5 / 4, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, 0, num * 5 / 4, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, 0, num * 5 / 4, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_entertainmentAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Entertainment, (float)this.m_entertainmentAccumulation, this.m_entertainmentRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_entertainmentAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.Entertainment, (float)(-(float)this.m_entertainmentAccumulation), this.m_entertainmentRadius);
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		Vector3 vector;
		Vector3 vector2;
		bool flag;
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline && BuildingTool.SnapToCanal(position, out vector, out vector2, out flag, 40f, false))
		{
			angle = Mathf.Atan2(vector2.x, -vector2.z);
			vector += vector2 * (this.m_info.m_generatedInfo.m_max.z - 7f);
			position.x = vector.x;
			position.z = vector.z;
			if (!flag)
			{
				toolErrors |= ToolBase.ToolErrors.ShoreNotFound;
			}
		}
		return toolErrors | base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline)
		{
			min = 12f / Mathf.Max(14f, (float)this.m_info.m_cellLength * 8f);
			max = 1f;
			return true;
		}
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		if (this.CountAnimals(buildingID, ref buildingData) < this.TargetAnimals(buildingID, ref buildingData))
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_customBuffer2 = buildingData.m_customBuffer1;
			buildingData.m_tempImport = 0;
			buildingData.m_tempExport = 0;
			buildingData.m_customBuffer1 = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		visitPlaceCount += this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.GetVisitBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveVisitorCount, ref totalVisitorCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		int num = (productionRate * this.m_entertainmentAccumulation + 99) / 100;
		if ((servicePolicies & DistrictPolicies.Services.RecreationBoost) != DistrictPolicies.Services.None)
		{
			int num2 = (int)buildingData.m_productionRate;
			if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				num2 = 0;
			}
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			int num3 = this.GetMaintenanceCost() / 100;
			num3 = num2 * budget / 100 * num3 / 500;
			District[] expr_BF_cp_0 = instance.m_districts.m_buffer;
			byte expr_BF_cp_1 = district;
			expr_BF_cp_0[(int)expr_BF_cp_1].m_servicePoliciesEffect = (expr_BF_cp_0[(int)expr_BF_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.RecreationBoost);
			num = (num * 120 + 99) / 100;
			Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num3, this.m_info.m_class);
		}
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Entertainment, num, buildingData.m_position, this.m_entertainmentRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		buildingData.m_tempExport = (byte)Mathf.Clamp(behaviour.m_touristCount * 255 / Mathf.Max(1, aliveVisitorCount), (int)buildingData.m_tempExport, 255);
		buildingData.m_customBuffer1 = (ushort)Mathf.Clamp(aliveVisitorCount, (int)buildingData.m_customBuffer1, 65535);
		if (buildingData.m_finalExport != 0)
		{
			District[] expr_17F_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_17F_cp_0_cp_1 = district;
			expr_17F_cp_0_cp_0[(int)expr_17F_cp_0_cp_1].m_playerConsumption.m_tempExportAmount = expr_17F_cp_0_cp_0[(int)expr_17F_cp_0_cp_1].m_playerConsumption.m_tempExportAmount + (uint)buildingData.m_finalExport;
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalVisitorCount);
		int num4 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount0 + 99) / 100, this.m_visitPlaceCount0 * 5 / 4);
		int num5 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount1 + 99) / 100, this.m_visitPlaceCount1 * 5 / 4);
		int num6 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount2 + 99) / 100, this.m_visitPlaceCount2 * 5 / 4);
		int num7 = Mathf.Max(1, num4 + num5 + num6);
		int num8 = Mathf.Max(0, num7 - totalVisitorCount);
		int num9 = Mathf.Max(Mathf.Max(num4, num5), Mathf.Max(1, num6));
		int num10 = num8 * num4 / num7;
		int num11 = num8 * num5 / num7;
		int num12 = num8 * num6 / num7;
		if (num10 + num11 + num12 > 0)
		{
			int num13 = Mathf.Max(Mathf.Max(num10, num11), Mathf.Max(1, num12));
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = Mathf.Max(1, num13 * 8 / num9);
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			offer.Amount = num10 + num11 + num12;
			offer.Active = false;
			switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
			{
			case 0:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
				break;
			case 1:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
				break;
			case 2:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
				break;
			case 3:
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
				break;
			}
		}
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num3);
			target.z += Mathf.Sin(num3);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = this.m_info.m_size.y + (float)randomizer.Int32(1000u) * 0.1f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, ItemClass.Service.Garbage, ItemClass.SubService.None);
		ushort num;
		if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num2++;
			}
			num = nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	private int TargetAnimals(ushort buildingID, ref Building data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		return Mathf.Max(100, data.Width * data.Length * 5 + randomizer.Int32(100u)) / 100;
	}

	public override string GetLocalizedTooltip()
	{
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_VISITOR_CAPACITY", new object[]
			{
				num
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num = (int)(((ushort)data.m_finalExport * data.m_customBuffer2 + 254) / 255);
		int customBuffer = (int)data.m_customBuffer2;
		string str = LocaleFormatter.FormatGeneric("AIINFO_VISITORS", new object[]
		{
			customBuffer
		});
		str += Environment.NewLine;
		return str + LocaleFormatter.FormatGeneric("AIINFO_TOURISTS", new object[]
		{
			num
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_visitPlaceCount0 != 0 || this.m_visitPlaceCount1 != 0 || this.m_visitPlaceCount2 != 0;
	}

	[CustomizableProperty("Low Wealth Tourists", "Tourists", 0)]
	public int m_visitPlaceCount0 = 50;

	[CustomizableProperty("Medium Wealth Tourists", "Tourists", 1)]
	public int m_visitPlaceCount1 = 50;

	[CustomizableProperty("High Wealth Tourists", "Tourists", 2)]
	public int m_visitPlaceCount2 = 50;

	[CustomizableProperty("Entertainment Accumulation")]
	public int m_entertainmentAccumulation = 100;

	[CustomizableProperty("Entertainment Radius")]
	public float m_entertainmentRadius = 400f;
}
