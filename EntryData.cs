﻿using System;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class EntryData
{
	public EntryData(Package.Asset asset)
	{
		this.m_Asset = asset;
		this.m_PublishedFileId = asset.get_package().GetPublishedFileID();
		this.GenerateData();
	}

	public EntryData(PluginManager.PluginInfo info)
	{
		this.m_PluginInfo = info;
		this.m_PublishedFileId = info.get_publishedFileID();
		this.GenerateData();
	}

	public EntryData(PublishedFileId id)
	{
		this.m_PublishedFileId = id;
		this.GenerateData();
	}

	public bool selected
	{
		get
		{
			return this.m_Selected;
		}
		set
		{
			this.m_Selected = value;
			if (this.m_AttachedEntry != null)
			{
				this.m_AttachedEntry.UpdateBackground();
			}
		}
	}

	public Package.Asset asset
	{
		get
		{
			return this.m_Asset;
		}
	}

	public Package package
	{
		get
		{
			return (!(this.m_Asset != null)) ? null : this.m_Asset.get_package();
		}
	}

	public PluginManager.PluginInfo pluginInfo
	{
		get
		{
			return this.m_PluginInfo;
		}
	}

	public UGCDetails workshopDetails
	{
		get
		{
			return this.m_WorkshopDetails;
		}
	}

	public PublishedFileId publishedFileId
	{
		get
		{
			return this.m_PublishedFileId;
		}
	}

	public bool detailsPending
	{
		get
		{
			return this.m_DetailsPending;
		}
	}

	public PackageEntry attachedEntry
	{
		get
		{
			return this.m_AttachedEntry;
		}
		set
		{
			this.m_AttachedEntry = value;
		}
	}

	public string entryName
	{
		get
		{
			return this.m_EntryName;
		}
	}

	public string authorName
	{
		get
		{
			return this.m_AuthorName;
		}
	}

	public SteamHelper.DLC_BitMask requiredMask
	{
		get
		{
			return this.m_RequiredMask;
		}
	}

	public Texture2D previewImage
	{
		get
		{
			return this.m_PreviewImage;
		}
		set
		{
			this.m_PreviewImage = value;
		}
	}

	public MetaData metaData
	{
		get
		{
			return this.m_MetaData;
		}
	}

	public DateTime updated
	{
		get
		{
			if (this.m_PublishedFileId != PublishedFileId.invalid && !this.m_DetailsPending)
			{
				DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
				return dateTime.AddSeconds(this.m_WorkshopDetails.timeUpdated).ToLocalTime();
			}
			if (this.m_Asset != null)
			{
				if (this.m_MetaData != null && !(this.m_MetaData is ColorCorrectionMetaData) && this.m_Asset.get_name() != DistrictStyle.kEuropeanStyleName && this.m_Asset.get_name() != DistrictStyle.kEuropeanSuburbiaStyleName)
				{
					return this.m_MetaData.getTimeStamp.ToLocalTime();
				}
				if (this.package != null && !string.IsNullOrEmpty(this.package.get_packagePath()))
				{
					if (PackageManager.IsCloudPath(this.package.get_packagePath()) && PlatformService.get_cloud().get_enabled())
					{
						return PlatformService.get_cloud().GetTimeStamp(PackageManager.GetCloudPath(this.package.get_packagePath())).ToLocalTime();
					}
					return File.GetLastWriteTimeUtc(this.package.get_packagePath()).ToLocalTime();
				}
			}
			else if (this.m_PluginInfo != null)
			{
				return PackageEntry.GetLocalModTimeUpdated(this.m_PluginInfo.get_modPath()).ToLocalTime();
			}
			return DateTime.Now;
		}
	}

	public bool IsMatch(CategoryContentPanel.SearchTokens tokens)
	{
		if (this.m_Asset != null && this.metaData != null && tokens.filters.Count > 0)
		{
			bool flag = false;
			foreach (string current in tokens.filters)
			{
				string[] getTags = this.metaData.getTags;
				for (int i = 0; i < getTags.Length; i++)
				{
					string text = getTags[i];
					if (text.IndexOf(current, StringComparison.CurrentCultureIgnoreCase) != -1)
					{
						flag = true;
						break;
					}
				}
				if (flag)
				{
					break;
				}
			}
			if (!flag)
			{
				return false;
			}
		}
		if (tokens.searchTerms.Count > 0)
		{
			foreach (string current2 in tokens.searchTerms)
			{
				if (this.m_EntryName.IndexOf(current2, StringComparison.CurrentCultureIgnoreCase) != -1)
				{
					bool result = true;
					return result;
				}
				if (this.m_AuthorName.IndexOf(current2, StringComparison.CurrentCultureIgnoreCase) != -1)
				{
					bool result = true;
					return result;
				}
			}
			return false;
		}
		return true;
	}

	public bool IsActive()
	{
		if (this.m_Asset != null)
		{
			return this.m_Asset.get_isEnabled();
		}
		return this.m_PluginInfo != null && this.m_PluginInfo.get_isEnabled();
	}

	public bool IsBuiltin()
	{
		if (this.m_MetaData != null)
		{
			return this.m_MetaData.getBuiltin;
		}
		return this.m_PluginInfo != null && this.m_PluginInfo.get_isBuiltin();
	}

	public void SetActive(bool active)
	{
		if (this.m_Asset != null)
		{
			if (this.m_Asset.get_type() == UserAssetType.DistrictStyleMetaData)
			{
				SavedBool agreed = new SavedBool(Settings.agreedStylesDisclaimer, Settings.gameSettingsFile, false);
				if (agreed)
				{
					this.m_Asset.set_isEnabled(active);
				}
				else if (active)
				{
					DisclaimerPanel.ShowModal(Settings.agreedStylesDisclaimer, agreed, delegate(UIComponent cp, int r)
					{
						if (r > 0)
						{
							agreed.set_value(true);
							this.m_Asset.set_isEnabled(active);
							if (r == 1)
							{
								agreed.Delete();
							}
						}
						else
						{
							this.SetActive(false);
						}
					});
				}
				else
				{
					this.m_Asset.set_isEnabled(active);
					if (this.m_AttachedEntry != null)
					{
						this.m_AttachedEntry.RevertChecked();
					}
				}
			}
			else if (this.IsDLCEnabled())
			{
				this.m_Asset.set_isEnabled(active);
				if (this.m_Asset.get_type() == UserAssetType.CustomAssetMetaData)
				{
					this.ActivateSideAssets(active);
				}
				PackageManager.ForceAssetStateChanged();
			}
		}
		else if (this.m_PluginInfo != null)
		{
			SavedBool agreed = new SavedBool(Settings.agreedModsDisclaimer, Settings.gameSettingsFile, false);
			if (agreed)
			{
				this.m_PluginInfo.set_isEnabled(active);
			}
			else if (active)
			{
				if (this.m_PluginInfo.get_isBuiltin())
				{
					this.m_PluginInfo.set_isEnabled(true);
				}
				else
				{
					DisclaimerPanel.ShowModal(Settings.agreedModsDisclaimer, agreed, delegate(UIComponent cp, int r)
					{
						if (r > 0)
						{
							agreed.set_value(true);
							this.m_PluginInfo.set_isEnabled(true);
							if (r == 1)
							{
								agreed.Delete();
							}
						}
						else
						{
							this.SetActive(false);
							if (this.m_AttachedEntry != null)
							{
								this.m_AttachedEntry.RevertChecked();
							}
						}
					});
				}
			}
			else
			{
				this.m_PluginInfo.set_isEnabled(false);
			}
		}
	}

	private void ActivateSideAssets(bool active)
	{
		CustomAssetMetaData customAssetMetaData = (this.m_MetaData == null) ? null : ((CustomAssetMetaData)this.m_MetaData);
		if (customAssetMetaData != null)
		{
			foreach (Package.Asset current in this.package.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.CustomAssetMetaData
			}))
			{
				CustomAssetMetaData customAssetMetaData2 = current.Instantiate<CustomAssetMetaData>();
				if (customAssetMetaData2 != null && (customAssetMetaData2.type == CustomAssetMetaData.Type.PropVariation || customAssetMetaData2.type == CustomAssetMetaData.Type.SubBuilding || customAssetMetaData2.type == CustomAssetMetaData.Type.Trailer || customAssetMetaData2.type == CustomAssetMetaData.Type.Pillar || customAssetMetaData2.type == CustomAssetMetaData.Type.RoadElevation))
				{
					current.set_isEnabled(active);
				}
			}
		}
	}

	public bool IsDLCEnabled()
	{
		SteamHelper.DLC_BitMask ownedDLCMask = SteamHelper.GetOwnedDLCMask();
		return (this.m_RequiredMask & ~ownedDLCMask) == (SteamHelper.DLC_BitMask)0;
	}

	public void ResetAttachedEntry(PackageEntry source)
	{
		if (this.m_AttachedEntry == source)
		{
			this.m_AttachedEntry = null;
		}
	}

	public void OnDetailsReceived(UGCDetails details, bool ioError)
	{
		if (this.m_PublishedFileId == details.publishedFileId)
		{
			this.m_WorkshopDetails = details;
			if (this.m_Asset == null && this.m_PluginInfo == null)
			{
				this.m_EntryName = this.m_WorkshopDetails.title;
			}
			this.m_AuthorName = new Friend(this.m_WorkshopDetails.creatorID).get_personaName();
			PlatformService.add_eventPersonaStateChange(new PlatformService.PersonaStateChangeHandler(this.OnNameReceived));
			this.m_DetailsPending = false;
			if (this.m_AttachedEntry != null)
			{
				this.m_AttachedEntry.SetEntry(this);
			}
		}
	}

	private void OnNameReceived(UserID id, PersonaChange flags)
	{
		if (this.m_WorkshopDetails.creatorID != UserID.invalid && id == this.m_WorkshopDetails.creatorID)
		{
			PlatformService.remove_eventPersonaStateChange(new PlatformService.PersonaStateChangeHandler(this.OnNameReceived));
			this.m_AuthorName = new Friend(this.m_WorkshopDetails.creatorID).get_personaName();
			if (this.m_AttachedEntry != null)
			{
				this.m_AttachedEntry.SetNameLabel(this.m_EntryName, this.m_AuthorName);
			}
		}
	}

	private SteamHelper.DLC_BitMask GetScenarioDLCMask(string assetName)
	{
		switch (assetName)
		{
		case "Alpine Villages.Alpine Villages_Data":
		case "By The Dam.By The Dam_Data":
		case "Floodland.Floodland_Data":
		case "Island Hopping.Island Hopping_Data":
		case "Tornado Country.Tornado Country_Data":
			return SteamHelper.DLC_BitMask.NaturalDisastersDLC;
		case "Ferry Empire.Ferry Empire_Data":
		case "Fix the Traffic.Fix the Traffic_Data":
		case "Trains.Trains_Data":
			return SteamHelper.DLC_BitMask.InMotionDLC;
		case "City of Gardens.City of Gardens_Data":
		case "Clean Up Crew.Clean Up Crew_Data":
		case "Green Power.Green Power_Data":
			return SteamHelper.DLC_BitMask.GreenCitiesDLC;
		}
		return (SteamHelper.DLC_BitMask)0;
	}

	private SteamHelper.DLC_BitMask GetMapDLCMask(MapMetaData mmd)
	{
		if (mmd.getEnvironment == "Winter")
		{
			return SteamHelper.DLC_BitMask.SnowFallDLC;
		}
		if (mmd.builtin)
		{
			if (NewGamePanel.IsNDScenarioMap(mmd.mapName))
			{
				return SteamHelper.DLC_BitMask.NaturalDisastersDLC;
			}
			if (NewGamePanel.IsMTScenarioMap(mmd.mapName))
			{
				return SteamHelper.DLC_BitMask.InMotionDLC;
			}
			if (NewGamePanel.IsGCScenarioMap(mmd.mapName))
			{
				return SteamHelper.DLC_BitMask.GreenCitiesDLC;
			}
		}
		return (SteamHelper.DLC_BitMask)0;
	}

	private void GenerateData()
	{
		if (this.m_PublishedFileId != PublishedFileId.invalid)
		{
			this.m_DetailsPending = true;
		}
		if (this.m_Asset != null)
		{
			string fileName = Path.GetFileName(this.asset.get_package().get_packagePath());
			if (this.m_Asset.get_type() == UserAssetType.MapMetaData)
			{
				MapMetaData mapMetaData = this.m_Asset.Instantiate<MapMetaData>();
				this.m_MetaData = mapMetaData;
				if (mapMetaData != null)
				{
					string text = (!string.IsNullOrEmpty(mapMetaData.mapName)) ? mapMetaData.mapName : this.asset.get_name();
					if (Locale.Exists("MAPNAME", text))
					{
						text = Locale.Get("MAPNAME", text);
					}
					this.m_EntryName = text + " (" + fileName + ")";
					this.m_RequiredMask = this.GetMapDLCMask(mapMetaData);
				}
			}
			else if (this.m_Asset.get_type() == UserAssetType.SaveGameMetaData)
			{
				SaveGameMetaData saveGameMetaData = this.m_Asset.Instantiate<SaveGameMetaData>();
				this.m_MetaData = saveGameMetaData;
				if (saveGameMetaData != null)
				{
					string str = (!string.IsNullOrEmpty(saveGameMetaData.cityName)) ? saveGameMetaData.cityName : this.asset.get_name();
					this.m_EntryName = str + " (" + fileName + ")";
				}
			}
			else if (this.m_Asset.get_type() == UserAssetType.CustomAssetMetaData)
			{
				CustomAssetMetaData customAssetMetaData = this.m_Asset.Instantiate<CustomAssetMetaData>();
				this.m_MetaData = customAssetMetaData;
				if (customAssetMetaData != null)
				{
					string str2 = (!string.IsNullOrEmpty(customAssetMetaData.name)) ? customAssetMetaData.name : this.asset.get_name();
					this.m_EntryName = str2 + " (" + fileName + ")";
					this.m_RequiredMask = AssetImporterAssetTemplate.GetAssetDLCMask(customAssetMetaData);
				}
			}
			else if (this.m_Asset.get_type() == UserAssetType.ColorCorrection)
			{
				ColorCorrectionMetaData colorCorrectionMetaData = this.m_Asset.Instantiate<ColorCorrectionMetaData>();
				this.m_MetaData = colorCorrectionMetaData;
				if (colorCorrectionMetaData != null)
				{
					string str3 = (!string.IsNullOrEmpty(colorCorrectionMetaData.name)) ? colorCorrectionMetaData.name : this.asset.get_name();
					this.m_EntryName = str3 + " (" + fileName + ")";
				}
			}
			else if (this.m_Asset.get_type() == UserAssetType.MapThemeMetaData)
			{
				MapThemeMetaData mapThemeMetaData = this.m_Asset.Instantiate<MapThemeMetaData>();
				this.m_MetaData = mapThemeMetaData;
				if (mapThemeMetaData != null)
				{
					string str4 = (!string.IsNullOrEmpty(mapThemeMetaData.name)) ? mapThemeMetaData.name : this.asset.get_name();
					this.m_EntryName = str4 + " (" + fileName + ")";
				}
			}
			else if (this.m_Asset.get_type() == UserAssetType.ScenarioMetaData)
			{
				ScenarioMetaData scenarioMetaData = this.m_Asset.Instantiate<ScenarioMetaData>();
				this.m_MetaData = scenarioMetaData;
				if (scenarioMetaData != null)
				{
					string str5 = (!string.IsNullOrEmpty(scenarioMetaData.scenarioName)) ? scenarioMetaData.scenarioName : this.asset.get_name();
					string text2 = StringUtils.SafeFormat("{0}.{1}", new object[]
					{
						scenarioMetaData.assetRef.get_package().get_packageName(),
						scenarioMetaData.assetRef.get_name()
					});
					if (Locale.Exists("SCENARIO_NAME", text2))
					{
						str5 = Locale.Get("SCENARIO_NAME", text2);
					}
					this.m_EntryName = str5 + " (" + fileName + ")";
					this.m_RequiredMask = ((!scenarioMetaData.builtin) ? ((SteamHelper.DLC_BitMask)0) : this.GetScenarioDLCMask(text2));
				}
			}
		}
		else if (this.m_PluginInfo != null)
		{
			this.m_EntryName = this.m_PluginInfo.get_name();
			try
			{
				IUserMod[] instances = this.m_PluginInfo.GetInstances<IUserMod>();
				if (instances.Length == 1)
				{
					if (Locale.Exists("MOD_NAME", instances[0].get_Name()))
					{
						this.m_EntryName = Locale.Get("MOD_NAME", instances[0].get_Name());
					}
					else
					{
						this.m_EntryName = instances[0].get_Name();
					}
					this.m_EntryName += " - ";
					if (Locale.Exists("MOD_DESCRIPTION", instances[0].get_Name()))
					{
						this.m_EntryName += Locale.Get("MOD_DESCRIPTION", instances[0].get_Name());
					}
					else
					{
						this.m_EntryName += instances[0].get_Description();
					}
				}
				else
				{
					this.m_EntryName = instances[0].get_Name() + " - " + instances[0].get_Description();
					DebugOutputPanel.AddMessage(1, "Multiple IUserMod implemented for the same mod. Only one IUserMod is accepted per mod. " + this.m_PluginInfo.ToString());
				}
			}
			catch (UnityException ex)
			{
				Debug.LogException(ex);
				UIView.ForwardException(new ModException("A Mod caused an error", ex));
			}
			catch (Exception ex2)
			{
				Debug.LogException(ex2);
				UIView.ForwardException(new ModException("A Mod caused an error", ex2));
			}
		}
		else if (this.m_PublishedFileId != PublishedFileId.invalid)
		{
			this.m_EntryName = this.m_PublishedFileId.get_AsUInt64().ToString();
		}
	}

	public string GetNameForTelemetry()
	{
		string result = null;
		if (this.asset != null)
		{
			result = this.asset.get_fullName();
		}
		else if (this.pluginInfo != null)
		{
			result = PopsTelemetryEventFormatting.GetModName(this.pluginInfo);
		}
		else if (this.m_PublishedFileId != PublishedFileId.invalid)
		{
			foreach (Package current in PackageManager.get_allPackages())
			{
				if (current.GetPublishedFileID() == this.m_PublishedFileId)
				{
					string packageMainAsset = current.get_packageMainAsset();
					result = this.publishedFileId.ToString() + (string.IsNullOrEmpty(packageMainAsset) ? string.Empty : ("." + packageMainAsset));
					break;
				}
			}
		}
		return result;
	}

	public static int SortByName(EntryData a, EntryData b)
	{
		return string.Compare(a.entryName, b.entryName);
	}

	public static int SortByAuthor(EntryData a, EntryData b)
	{
		if (a.publishedFileId != PublishedFileId.invalid && b.publishedFileId == PublishedFileId.invalid)
		{
			return 1;
		}
		if (a.publishedFileId == PublishedFileId.invalid && b.publishedFileId != PublishedFileId.invalid)
		{
			return -1;
		}
		if (!(a.publishedFileId != PublishedFileId.invalid) || !(b.publishedFileId != PublishedFileId.invalid))
		{
			if (a.publishedFileId == PublishedFileId.invalid && b.publishedFileId == PublishedFileId.invalid)
			{
				if (a.IsBuiltin() && !b.IsBuiltin())
				{
					return -1;
				}
				if (!a.IsBuiltin() && b.IsBuiltin())
				{
					return 1;
				}
			}
			return EntryData.SortByName(a, b);
		}
		if (a.authorName == b.authorName)
		{
			return EntryData.SortByName(a, b);
		}
		return a.authorName.CompareTo(b.authorName);
	}

	public static int SortByActive(EntryData a, EntryData b)
	{
		if (a.IsActive() && !b.IsActive())
		{
			return -1;
		}
		if (!a.IsActive() && b.IsActive())
		{
			return 1;
		}
		return EntryData.SortByName(a, b);
	}

	public static int SortByUpdated(EntryData a, EntryData b)
	{
		DateTime updated = a.updated;
		DateTime updated2 = b.updated;
		if (updated == updated2)
		{
			return EntryData.SortByName(a, b);
		}
		return updated2.CompareTo(updated);
	}

	public static int SortBySubscribed(EntryData a, EntryData b)
	{
		bool flag = a.publishedFileId != PublishedFileId.invalid && !a.detailsPending;
		bool flag2 = b.publishedFileId != PublishedFileId.invalid && !b.detailsPending;
		if (!flag && !flag2)
		{
			return EntryData.SortByName(a, b);
		}
		if (flag && !flag2)
		{
			return 1;
		}
		if (!flag && flag2)
		{
			return -1;
		}
		uint timeAddedToUserList = a.workshopDetails.timeAddedToUserList;
		uint timeAddedToUserList2 = b.workshopDetails.timeAddedToUserList;
		if (timeAddedToUserList == timeAddedToUserList2)
		{
			return EntryData.SortByName(a, b);
		}
		return (timeAddedToUserList <= timeAddedToUserList2) ? -1 : 1;
	}

	public static int SortByPath(EntryData a, EntryData b)
	{
		if (a.IsBuiltin() && !b.IsBuiltin())
		{
			return -1;
		}
		if (!a.IsBuiltin() && b.IsBuiltin())
		{
			return 1;
		}
		if (a.IsBuiltin() && b.IsBuiltin())
		{
			return EntryData.SortByName(a, b);
		}
		if (a.publishedFileId == PublishedFileId.invalid && b.publishedFileId != PublishedFileId.invalid)
		{
			return 1;
		}
		if (a.publishedFileId != PublishedFileId.invalid && b.publishedFileId == PublishedFileId.invalid)
		{
			return -1;
		}
		if (a.publishedFileId != PublishedFileId.invalid && b.publishedFileId != PublishedFileId.invalid)
		{
			return EntryData.SortByName(a, b);
		}
		string packagePath = a.package.get_packagePath();
		string packagePath2 = b.package.get_packagePath();
		if (packagePath == packagePath2)
		{
			return EntryData.SortByName(a, b);
		}
		return string.Compare(packagePath, packagePath2);
	}

	private Package.Asset m_Asset;

	private PluginManager.PluginInfo m_PluginInfo;

	private UGCDetails m_WorkshopDetails;

	private PublishedFileId m_PublishedFileId = PublishedFileId.invalid;

	private bool m_DetailsPending;

	private SteamHelper.DLC_BitMask m_RequiredMask;

	private MetaData m_MetaData;

	private Texture2D m_PreviewImage;

	private PackageEntry m_AttachedEntry;

	private string m_EntryName = string.Empty;

	private string m_AuthorName = string.Empty;

	private bool m_Selected;
}
