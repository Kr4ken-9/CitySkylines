﻿using System;
using ColossalFramework;
using UnityEngine;

public class DecorationRenderer : MonoBehaviour
{
	private void Awake()
	{
		this.ID_TileRect = Shader.PropertyToID("_TileRect");
		this.m_renderCamera = base.get_gameObject().AddComponent<Camera>();
		this.m_renderCamera.set_eventMask(0);
		this.m_renderCamera.set_enabled(false);
		this.m_renderCamera.set_targetTexture(null);
		this.m_renderCamera.set_backgroundColor(new Color(0f, 0f, 0f, 0f));
		this.m_renderCamera.set_clearFlags(2);
		this.m_renderCamera.ResetAspect();
		this.m_renderCamera.set_fieldOfView(10f);
		this.m_renderCamera.set_nearClipPlane(1f);
		this.m_renderCamera.set_farClipPlane(100f);
		this.m_renderCamera.set_cullingMask(0);
		this.m_renderCamera.set_renderingPath(0);
		this.m_renderCamera.set_useOcclusionCulling(false);
		this.m_renderDiffuseTexture = new RenderTexture(128, 128, 24, 0, 1);
		this.m_renderDiffuseTexture.set_filterMode(2);
		this.m_renderDiffuseTexture.set_autoGenerateMips(true);
		this.m_renderXycaTexture = new RenderTexture(128, 128, 24, 0, 1);
		this.m_renderXycaTexture.set_filterMode(2);
		this.m_renderXycaTexture.set_autoGenerateMips(true);
	}

	private void OnDestroy()
	{
		if (this.m_renderDiffuseTexture != null)
		{
			Object.Destroy(this.m_renderDiffuseTexture);
			this.m_renderDiffuseTexture = null;
		}
		if (this.m_renderXycaTexture != null)
		{
			Object.Destroy(this.m_renderXycaTexture);
			this.m_renderXycaTexture = null;
		}
	}

	private void OnPostRender()
	{
		if (this.m_cameraInfo == null)
		{
			return;
		}
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainProperties properties = instance.m_properties;
		int num = 4;
		Quaternion quaternion = Quaternion.Inverse(this.m_cameraInfo.m_rotation);
		float num2 = Mathf.Tan(this.m_renderCamera.get_fieldOfView() * 0.0174532924f * 0.5f);
		float num3 = 1f / (float)num;
		Vector3 scale = Vector3.get_one() / (float)num;
		Vector3 vector;
		vector..ctor(0.5f / (float)num - 0.5f, 0.5f / (float)num - 0.5f, 0.5f / num2);
		Vector3 position = vector - quaternion * new Vector3(0f, 0.5f / (float)num, 0f);
		Vector4 tileRect;
		tileRect..ctor(vector.x - 0.5f / (float)num, vector.y - 0.5f / (float)num, vector.x + 0.5f / (float)num, vector.y + 0.5f / (float)num);
		if (properties.m_useGrassDecorations)
		{
			this.RenderDecorations(properties, properties.m_grassDecorations, quaternion, scale, position, tileRect, num3, num2);
		}
		position.y += num3;
		tileRect.y += num3;
		tileRect.w += num3;
		if (properties.m_useFertileDecorations)
		{
			this.RenderDecorations(properties, properties.m_fertileDecorations, quaternion, scale, position, tileRect, num3, num2);
		}
		position.y += num3;
		tileRect.y += num3;
		tileRect.w += num3;
		if (properties.m_useCliffDecorations)
		{
			this.RenderDecorations(properties, properties.m_cliffDecorations, quaternion, scale, position, tileRect, num3, num2);
		}
	}

	private void RenderDecorations(TerrainProperties properties, DecorationInfo[] decorations, Quaternion rotation, Vector3 scale, Vector3 position, Vector4 tileRect, float offset, float tan)
	{
		int num = decorations.Length;
		for (int i = 0; i < num; i++)
		{
			DecorationInfo decorationInfo = decorations[i];
			if (!decorationInfo.m_initialized)
			{
				decorationInfo.Initialize(properties);
			}
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(position, rotation, scale);
			decorationInfo.m_renderMaterial.SetVector(this.ID_TileRect, tileRect);
			if (this.m_diffusePass)
			{
				if (decorationInfo.m_renderMaterial.SetPass(2))
				{
					TerrainManager expr_6E_cp_0 = Singleton<TerrainManager>.get_instance();
					expr_6E_cp_0.m_drawCallData.m_defaultCalls = expr_6E_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMeshNow(decorationInfo.m_mesh, matrix4x);
				}
			}
			else
			{
				if (decorationInfo.m_renderMaterial.SetPass(0))
				{
					TerrainManager expr_A7_cp_0 = Singleton<TerrainManager>.get_instance();
					expr_A7_cp_0.m_drawCallData.m_defaultCalls = expr_A7_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMeshNow(decorationInfo.m_mesh, matrix4x);
				}
				if (decorationInfo.m_renderMaterial.SetPass(1))
				{
					TerrainManager expr_DB_cp_0 = Singleton<TerrainManager>.get_instance();
					expr_DB_cp_0.m_drawCallData.m_defaultCalls = expr_DB_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMeshNow(decorationInfo.m_mesh, matrix4x);
				}
			}
			position.x += offset;
			tileRect.x += offset;
			tileRect.z += offset;
		}
	}

	public void BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainProperties properties = instance.m_properties;
		if (properties == null || (!properties.m_useGrassDecorations && !properties.m_useFertileDecorations && !properties.m_useCliffDecorations))
		{
			return;
		}
		this.m_cameraInfo = cameraInfo;
		try
		{
			bool flag = Quaternion.Angle(this.m_lastCameraRotation, this.m_cameraInfo.m_rotation) > 0.1f || !this.m_renderDiffuseTexture.IsCreated();
			bool flag2 = Quaternion.Angle(this.m_lastCameraRotation, this.m_cameraInfo.m_rotation) > 0.1f || !this.m_renderXycaTexture.IsCreated();
			if (properties.m_useGrassDecorations != this.m_lastGrassDecorations || properties.m_useFertileDecorations != this.m_lastFertileDecorations || properties.m_useCliffDecorations != this.m_lastCliffDecorations)
			{
				flag = true;
				flag2 = true;
			}
			if (flag)
			{
				this.m_diffusePass = true;
				Camera arg_112_0 = this.m_renderCamera;
				Color color;
				color..ctor(0.5f, 0.5f, 0.5f, 0f);
				arg_112_0.set_backgroundColor(color.get_gamma());
				this.m_renderCamera.set_targetTexture(this.m_renderDiffuseTexture);
				this.m_renderCamera.Render();
				this.m_lastCameraRotation = this.m_cameraInfo.m_rotation;
				this.m_lastGrassDecorations = properties.m_useGrassDecorations;
				this.m_lastFertileDecorations = properties.m_useFertileDecorations;
				this.m_lastCliffDecorations = properties.m_useCliffDecorations;
			}
			if (flag2)
			{
				this.m_diffusePass = false;
				Camera arg_19D_0 = this.m_renderCamera;
				Color color2;
				color2..ctor(0.5f, 0.5f, 1f, 0f);
				arg_19D_0.set_backgroundColor(color2.get_gamma());
				this.m_renderCamera.set_targetTexture(this.m_renderXycaTexture);
				this.m_renderCamera.Render();
				this.m_lastCameraRotation = this.m_cameraInfo.m_rotation;
				this.m_lastGrassDecorations = properties.m_useGrassDecorations;
				this.m_lastFertileDecorations = properties.m_useFertileDecorations;
				this.m_lastCliffDecorations = properties.m_useCliffDecorations;
			}
			this.m_renderCamera.set_targetTexture(null);
			instance.m_decorationMaterial.SetTexture(instance.ID_DecorationDiffuse, this.m_renderDiffuseTexture);
			instance.m_decorationMaterial.SetTexture(instance.ID_DecorationXYCA, this.m_renderXycaTexture);
		}
		finally
		{
			this.m_cameraInfo = null;
		}
	}

	public RenderTexture m_renderDiffuseTexture;

	public RenderTexture m_renderXycaTexture;

	private Camera m_renderCamera;

	private RenderManager.CameraInfo m_cameraInfo;

	private Quaternion m_lastCameraRotation;

	private bool m_lastGrassDecorations;

	private bool m_lastFertileDecorations;

	private bool m_lastCliffDecorations;

	private bool m_diffusePass;

	private int ID_TileRect;
}
