﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class NaturalResourceManager : SimulationManagerBase<NaturalResourceManager, NaturalResourceProperties>, ISimulationManager, IRenderableManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_naturalResources = new NaturalResourceManager.ResourceCell[262144];
		this.m_areaResources = new NaturalResourceManager.AreaCell[81];
		this.m_tempPollutionDisposeRate = 1;
		this.m_finalPollutionDisposeRate = 1;
		Vector4 vector;
		vector.z = 5.787037E-05f;
		vector.x = 0.5f;
		vector.y = 0.5f;
		vector.w = 0.2f;
		this.m_resourceTexture = new Texture2D(512, 512, 3, false, true);
		this.m_resourceTexture.set_wrapMode(1);
		this.m_destructionTexture = new Texture2D(512, 512, 3, false, true);
		this.m_destructionTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_NaturalResources", this.m_resourceTexture);
		Shader.SetGlobalTexture("_NaturalDestruction", this.m_destructionTexture);
		Shader.SetGlobalVector("_NaturalResourceMapping", vector);
		this.m_modifiedX1 = new int[512];
		this.m_modifiedX2 = new int[512];
		for (int i = 0; i < 512; i++)
		{
			this.m_modifiedX1[i] = 0;
			this.m_modifiedX2[i] = 511;
		}
		this.m_modified = true;
		this.m_modifiedBX1 = new int[512];
		this.m_modifiedBX2 = new int[512];
		for (int j = 0; j < 512; j++)
		{
			this.m_modifiedBX1[j] = 0;
			this.m_modifiedBX2[j] = 511;
		}
		this.m_modifiedB = true;
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		if (this.m_resourceTexture != null)
		{
			Object.Destroy(this.m_resourceTexture);
			this.m_resourceTexture = null;
		}
		if (this.m_destructionTexture != null)
		{
			Object.Destroy(this.m_destructionTexture);
			this.m_destructionTexture = null;
		}
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (this.m_ResourceWrapper == null)
		{
			this.m_ResourceWrapper = new ResourceWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_ResourceWrapper != null)
		{
			this.m_ResourceWrapper.Release();
			this.m_ResourceWrapper = null;
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_modified)
		{
			this.m_modified = false;
			this.UpdateTexture();
		}
		if (this.m_modifiedB)
		{
			this.m_modifiedB = false;
			this.UpdateTextureB();
		}
	}

	private void UpdateTexture()
	{
		for (int i = 0; i < 512; i++)
		{
			if (this.m_modifiedX2[i] >= this.m_modifiedX1[i])
			{
				while (!Monitor.TryEnter(this.m_naturalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				int num;
				int num2;
				try
				{
					num = this.m_modifiedX1[i];
					num2 = this.m_modifiedX2[i];
					this.m_modifiedX1[i] = 10000;
					this.m_modifiedX2[i] = -10000;
				}
				finally
				{
					Monitor.Exit(this.m_naturalResources);
				}
				for (int j = num; j <= num2; j++)
				{
					Color color;
					if (i == 0 || j == 0 || i == 511 || j == 511)
					{
						color..ctor(0.5f, 0.5f, 0.5f, 0f);
					}
					else
					{
						int num3 = 0;
						int num4 = 0;
						int num5 = 0;
						int num6 = 0;
						int num7 = 0;
						int num8 = 0;
						this.AddResource(j - 1, i - 1, 5, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j, i - 1, 7, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j + 1, i - 1, 5, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j - 1, i, 7, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j, i, 14, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j + 1, i, 7, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j - 1, i + 1, 5, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j, i + 1, 7, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						this.AddResource(j + 1, i + 1, 5, ref num3, ref num4, ref num5, ref num6, ref num7, ref num8);
						color.r = (float)(num3 - num4 + 15810) * 3.16255537E-05f;
						color.g = (float)(num5 - num6 + 15810) * 3.16255537E-05f;
						int num9 = num8 * 4 - num7;
						if (num9 > 0)
						{
							color.b = (float)(15810 + num9 / 4) * 3.16255537E-05f;
						}
						else
						{
							color.b = (float)(15810 + num9) * 3.16255537E-05f;
						}
						color.a = 1f;
					}
					this.m_resourceTexture.SetPixel(j, i, color);
				}
			}
		}
		this.m_resourceTexture.Apply();
	}

	private void UpdateTextureB()
	{
		for (int i = 0; i < 512; i++)
		{
			if (this.m_modifiedBX2[i] >= this.m_modifiedBX1[i])
			{
				while (!Monitor.TryEnter(this.m_naturalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				int num;
				int num2;
				try
				{
					num = this.m_modifiedBX1[i];
					num2 = this.m_modifiedBX2[i];
					this.m_modifiedBX1[i] = 10000;
					this.m_modifiedBX2[i] = -10000;
				}
				finally
				{
					Monitor.Exit(this.m_naturalResources);
				}
				for (int j = num; j <= num2; j++)
				{
					Color color;
					if (i == 0 || j == 0 || i == 511 || j == 511)
					{
						color..ctor(0f, 0f, 0f, 1f);
					}
					else
					{
						int num3 = 0;
						int num4 = 0;
						int num5 = 0;
						this.AddResource(j - 1, i - 1, 5, ref num3, ref num4, ref num5);
						this.AddResource(j, i - 1, 7, ref num3, ref num4, ref num5);
						this.AddResource(j + 1, i - 1, 5, ref num3, ref num4, ref num5);
						this.AddResource(j - 1, i, 7, ref num3, ref num4, ref num5);
						this.AddResource(j, i, 14, ref num3, ref num4, ref num5);
						this.AddResource(j + 1, i, 7, ref num3, ref num4, ref num5);
						this.AddResource(j - 1, i + 1, 5, ref num3, ref num4, ref num5);
						this.AddResource(j, i + 1, 7, ref num3, ref num4, ref num5);
						this.AddResource(j + 1, i + 1, 5, ref num3, ref num4, ref num5);
						color.r = (float)num3 * 6.325111E-05f;
						color.g = (float)num4 * 6.325111E-05f;
						color.b = (float)num5 * 6.325111E-05f;
						color.a = 1f;
					}
					this.m_destructionTexture.SetPixel(j, i, color);
				}
			}
		}
		this.m_destructionTexture.Apply();
	}

	private void AddResource(int x, int z, int multiplier, ref int ore, ref int oil, ref int sand, ref int fertility, ref int forest, ref int shore)
	{
		x = Mathf.Clamp(x, 0, 511);
		z = Mathf.Clamp(z, 0, 511);
		NaturalResourceManager.ResourceCell resourceCell = this.m_naturalResources[z * 512 + x];
		ore += (int)resourceCell.m_ore * multiplier;
		oil += (int)resourceCell.m_oil * multiplier;
		sand += (int)resourceCell.m_sand * multiplier;
		fertility += (int)resourceCell.m_fertility * multiplier;
		forest += (int)resourceCell.m_forest * multiplier;
		shore += (int)resourceCell.m_shore * multiplier;
	}

	private void AddResource(int x, int z, int multiplier, ref int pollution, ref int burned, ref int destroyed)
	{
		x = Mathf.Clamp(x, 0, 511);
		z = Mathf.Clamp(z, 0, 511);
		NaturalResourceManager.ResourceCell resourceCell = this.m_naturalResources[z * 512 + x];
		pollution += (int)resourceCell.m_pollution * multiplier;
		burned += (int)resourceCell.m_burned * multiplier;
		destroyed += (int)resourceCell.m_destroyed * multiplier;
	}

	public float CalculateForestProximity(Vector3 position, float radius)
	{
		float num = 33.75f;
		int num2 = 512;
		int num3 = Mathf.Max((int)((position.x - radius) / num + (float)num2 * 0.5f), 0);
		int num4 = Mathf.Max((int)((position.z - radius) / num + (float)num2 * 0.5f), 0);
		int num5 = Mathf.Min((int)((position.x + radius) / num + (float)num2 * 0.5f), num2 - 1);
		int num6 = Mathf.Min((int)((position.z + radius) / num + (float)num2 * 0.5f), num2 - 1);
		radius *= radius;
		float num7 = 0f;
		float num8 = 0f;
		for (int i = num4; i <= num6; i++)
		{
			float num9 = ((float)i - (float)num2 * 0.5f + 0.5f) * num - position.z;
			num9 *= num9;
			for (int j = num3; j <= num5; j++)
			{
				float num10 = ((float)j - (float)num2 * 0.5f + 0.5f) * num - position.x;
				num10 *= num10;
				if (num9 + num10 < radius)
				{
					float num11 = 1f - (num9 + num10) / radius;
					num7 += (float)this.m_naturalResources[i * num2 + j].m_forest * num11;
					num8 += 255f * num11;
				}
			}
		}
		if (num8 != 0f)
		{
			num7 /= num8;
		}
		return num7;
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ)
	{
		minX = Mathf.Max(0, minX - 1);
		minZ = Mathf.Max(0, minZ - 1);
		maxX = Mathf.Min(511, maxX + 1);
		maxZ = Mathf.Min(511, maxZ + 1);
		while (!Monitor.TryEnter(this.m_naturalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = minZ; i <= maxZ; i++)
			{
				this.m_modifiedX1[i] = Mathf.Min(this.m_modifiedX1[i], minX);
				this.m_modifiedX2[i] = Mathf.Max(this.m_modifiedX2[i], maxX);
			}
			this.m_modified = true;
		}
		finally
		{
			Monitor.Exit(this.m_naturalResources);
		}
	}

	public void AreaModifiedB(int minX, int minZ, int maxX, int maxZ)
	{
		minX = Mathf.Max(0, minX - 1);
		minZ = Mathf.Max(0, minZ - 1);
		maxX = Mathf.Min(511, maxX + 1);
		maxZ = Mathf.Min(511, maxZ + 1);
		while (!Monitor.TryEnter(this.m_naturalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = minZ; i <= maxZ; i++)
			{
				this.m_modifiedBX1[i] = Mathf.Min(this.m_modifiedBX1[i], minX);
				this.m_modifiedBX2[i] = Mathf.Max(this.m_modifiedBX2[i], maxX);
			}
			this.m_modifiedB = true;
		}
		finally
		{
			Monitor.Exit(this.m_naturalResources);
		}
	}

	public void TreesModified(Vector3 position)
	{
		int num = Mathf.Clamp((int)(position.x / 33.75f + 256f), 0, 511);
		int num2 = Mathf.Clamp((int)(position.z / 33.75f + 256f), 0, 511);
		float num3 = ((float)num - 256f) * 33.75f;
		float num4 = ((float)num2 - 256f) * 33.75f;
		float num5 = ((float)(num + 1) - 256f) * 33.75f;
		float num6 = ((float)(num2 + 1) - 256f) * 33.75f;
		int num7 = Mathf.Max((int)(num3 / 32f + 270f), 0);
		int num8 = Mathf.Max((int)(num4 / 32f + 270f), 0);
		int num9 = Mathf.Min((int)(num5 / 32f + 270f), 539);
		int num10 = Mathf.Min((int)(num6 / 32f + 270f), 539);
		TreeManager instance = Singleton<TreeManager>.get_instance();
		int num11 = 0;
		int num12 = 0;
		for (int i = num8; i <= num10; i++)
		{
			for (int j = num7; j <= num9; j++)
			{
				uint num13 = instance.m_treeGrid[i * 540 + j];
				int num14 = 0;
				while (num13 != 0u)
				{
					if ((instance.m_trees.m_buffer[(int)((UIntPtr)num13)].m_flags & 3) == 1)
					{
						Vector3 position2 = instance.m_trees.m_buffer[(int)((UIntPtr)num13)].Position;
						if (position2.x >= num3 && position2.z >= num4 && position2.x <= num5 && position2.z <= num6)
						{
							num11 += 15;
							num12 += instance.m_trees.m_buffer[(int)((UIntPtr)num13)].GrowState;
						}
					}
					num13 = instance.m_trees.m_buffer[(int)((UIntPtr)num13)].m_nextGridTree;
					if (++num14 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		byte b = (byte)Mathf.Min(num11 * 4, 255);
		byte b2 = (byte)Mathf.Min(num12 * 4, 255);
		NaturalResourceManager.ResourceCell resourceCell = this.m_naturalResources[num2 * 512 + num];
		if (b != resourceCell.m_forest || b2 != resourceCell.m_tree)
		{
			bool flag = b != resourceCell.m_forest;
			resourceCell.m_forest = b;
			resourceCell.m_tree = b2;
			if (b > 0)
			{
				resourceCell.m_fertility = 0;
			}
			this.m_naturalResources[num2 * 512 + num] = resourceCell;
			if (flag)
			{
				this.AreaModified(num, num2, num, num2);
			}
		}
	}

	private static int CountAndModify(ref byte data, ref int deltaBuffer, ref int resultDelta, int cellDelta)
	{
		if (data != 0 || cellDelta > 0)
		{
			deltaBuffer += cellDelta;
		}
		if (deltaBuffer >= 1048576)
		{
			int num = Mathf.Min((int)(255 - data), deltaBuffer >> 20);
			data += (byte)num;
			deltaBuffer -= num << 20;
			resultDelta += num;
		}
		else if (deltaBuffer <= -1048576)
		{
			int num2 = Mathf.Min((int)data, -deltaBuffer >> 20);
			data -= (byte)num2;
			deltaBuffer += num2 << 20;
			resultDelta -= num2;
		}
		return (int)data;
	}

	private static int CountAndKeep(ref byte data, ref int deltaBuffer, ref int resultDelta, int cellDelta)
	{
		if (data != 0 || cellDelta > 0)
		{
			deltaBuffer += cellDelta;
		}
		if (deltaBuffer >= 1048576)
		{
			int num = Mathf.Min((int)(255 - data), deltaBuffer >> 20);
			deltaBuffer -= num << 20;
			resultDelta += num;
		}
		else if (deltaBuffer <= -1048576)
		{
			int num2 = Mathf.Min((int)data, -deltaBuffer >> 20);
			deltaBuffer += num2 << 20;
			resultDelta -= num2;
		}
		return (int)data;
	}

	public int CountResource(NaturalResourceManager.Resource resource, Vector3 position, float radius)
	{
		int num;
		int num2;
		int num3;
		return this.CountResource(resource, position, radius, 0, out num, out num2, out num3, false);
	}

	private int CountResource(NaturalResourceManager.Resource resource, Vector3 position, float radius, int cellDelta, out int numCells, out int totalCells, out int resultDelta, bool refresh)
	{
		float num = 33.75f;
		int num2 = 512;
		radius += num * 0.51f;
		int num3 = Mathf.Max((int)((position.x - radius) / num + (float)num2 * 0.5f), 0);
		int num4 = Mathf.Max((int)((position.z - radius) / num + (float)num2 * 0.5f), 0);
		int num5 = Mathf.Min((int)((position.x + radius) / num + (float)num2 * 0.5f), num2 - 1);
		int num6 = Mathf.Min((int)((position.z + radius) / num + (float)num2 * 0.5f), num2 - 1);
		int num7 = 0;
		numCells = 0;
		totalCells = 0;
		resultDelta = 0;
		int num8 = Singleton<SimulationManager>.get_instance().m_randomizer.Bits32(20);
		if (cellDelta < 0)
		{
			num8 = -num8;
		}
		byte b = 0;
		if (cellDelta != 0)
		{
			b = ((resource < NaturalResourceManager.Resource.Pollution) ? 1 : 2);
		}
		for (int i = num4; i <= num6; i++)
		{
			float num9 = ((float)i - (float)num2 * 0.5f + 0.5f) * num - position.z;
			for (int j = num3; j <= num5; j++)
			{
				float num10 = ((float)j - (float)num2 * 0.5f + 0.5f) * num - position.x;
				if (num9 * num9 + num10 * num10 < radius * radius || radius == 0f)
				{
					if (cellDelta != 0)
					{
						this.m_ResourceWrapper.OnBeforeResourcesModified(j, i, resource, cellDelta >> 20);
					}
					NaturalResourceManager.ResourceCell resourceCell = this.m_naturalResources[i * num2 + j];
					totalCells++;
					int num11 = 0;
					int num12 = resultDelta;
					switch (resource)
					{
					case NaturalResourceManager.Resource.Ore:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_ore, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Sand:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_sand, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Oil:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_oil, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Fertility:
						num11 = NaturalResourceManager.CountAndKeep(ref resourceCell.m_fertility, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Forest:
						num11 = NaturalResourceManager.CountAndKeep(ref resourceCell.m_forest, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Pollution:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_pollution, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Burned:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_burned, ref num8, ref resultDelta, cellDelta);
						break;
					case NaturalResourceManager.Resource.Destroyed:
						num11 = NaturalResourceManager.CountAndModify(ref resourceCell.m_destroyed, ref num8, ref resultDelta, cellDelta);
						break;
					}
					if (num11 != 0)
					{
						numCells++;
						num7 += num11;
					}
					if (cellDelta != 0)
					{
						if (!refresh)
						{
							resourceCell.m_modified |= b;
						}
						this.m_naturalResources[i * num2 + j] = resourceCell;
						if (resultDelta != num12)
						{
							this.m_ResourceWrapper.OnAfterResourcesModified(j, i, resource, resultDelta - num12);
						}
					}
				}
			}
		}
		if (refresh && cellDelta != 0)
		{
			if (resource >= NaturalResourceManager.Resource.Pollution)
			{
				this.AreaModifiedB(num3, num4, num5, num6);
			}
			else
			{
				this.AreaModified(num3, num4, num5, num6);
			}
		}
		return num7;
	}

	public int TryFetchResource(NaturalResourceManager.Resource resource, int rate, int max, Vector3 position, float radius)
	{
		int num2;
		int num3;
		int num4;
		int num = this.CountResource(resource, position, radius, 0, out num2, out num3, out num4, false);
		rate = Mathf.Min(Mathf.Min(rate, max), Mathf.Min(num, num * rate + 255 >> 8));
		if (num2 == 0 || rate == 0)
		{
			return 0;
		}
		int cellDelta = -(int)(((long)rate << 20) / (long)num2);
		this.CountResource(resource, position, radius, cellDelta, out num2, out num3, out num4, false);
		num4 = -num4;
		if (num4 > rate)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, string.Concat(new object[]
			{
				resource.ToString(),
				": Delta > Rate (",
				num4,
				" > ",
				rate,
				")\n",
				Environment.StackTrace
			}));
			num4 = rate;
		}
		StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
		StatisticBase statisticBase = instance.Acquire<StatisticArray>(StatisticType.NaturalResourcesExtracted);
		switch (resource)
		{
		case NaturalResourceManager.Resource.Ore:
			this.m_usedResources.m_tempOre = this.m_usedResources.m_tempOre + (uint)num4;
			statisticBase.Acquire<StatisticInt32>(0, 4).Add(num4);
			break;
		case NaturalResourceManager.Resource.Oil:
			this.m_usedResources.m_tempOil = this.m_usedResources.m_tempOil + (uint)num4;
			statisticBase.Acquire<StatisticInt32>(1, 4).Add(num4);
			break;
		case NaturalResourceManager.Resource.Fertility:
			this.m_usedResources.m_tempFertility = this.m_usedResources.m_tempFertility + (uint)num4;
			statisticBase.Acquire<StatisticInt32>(2, 4).Add(num4);
			break;
		case NaturalResourceManager.Resource.Forest:
			this.m_usedResources.m_tempForest = this.m_usedResources.m_tempForest + (uint)num4;
			statisticBase.Acquire<StatisticInt32>(3, 4).Add(num4);
			break;
		}
		return num4;
	}

	public int TryDumpResource(NaturalResourceManager.Resource resource, int rate, int max, Vector3 position, float radius)
	{
		return this.TryDumpResource(resource, rate, max, position, radius, false);
	}

	public int TryDumpResource(NaturalResourceManager.Resource resource, int rate, int max, Vector3 position, float radius, bool refresh)
	{
		radius = Mathf.Max(33.75f, (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(200, 1000) * (radius * 0.001f));
		int num;
		int num2;
		int num3;
		this.CountResource(resource, position, radius, 0, out num, out num2, out num3, false);
		rate = Mathf.Min(rate, max);
		if (num2 == 0 || rate == 0)
		{
			return 0;
		}
		int cellDelta = (int)(((long)rate << 20) / (long)num2);
		this.CountResource(resource, position, radius, cellDelta, out num, out num2, out num3, refresh);
		return rate;
	}

	public void CheckPollution(Vector3 pos, out byte groundPollution)
	{
		int num = Mathf.Clamp((int)(pos.x / 33.75f + 256f), 0, 511);
		int num2 = Mathf.Clamp((int)(pos.z / 33.75f + 256f), 0, 511);
		int num3 = num2 * 512 + num;
		groundPollution = this.m_naturalResources[num3].m_pollution;
	}

	public void CheckForest(Vector3 pos, out byte forestDensity)
	{
		int num = Mathf.Clamp((int)(pos.x / 33.75f + 256f), 0, 511);
		int num2 = Mathf.Clamp((int)(pos.z / 33.75f + 256f), 0, 511);
		int num3 = num2 * 512 + num;
		forestDensity = this.m_naturalResources[num3].m_forest;
	}

	public void AveragePollutionAndWater(Rect area, out float groundPollution, out float waterProximity)
	{
		float num = area.get_xMin() / 33.75f + 256f;
		float num2 = area.get_yMin() / 33.75f + 256f;
		float num3 = area.get_xMax() / 33.75f + 256f;
		float num4 = area.get_yMax() / 33.75f + 256f;
		int num5 = Mathf.Clamp((int)num, 0, 511);
		int num6 = Mathf.Clamp((int)num2, 0, 511);
		int num7 = Mathf.Clamp((int)num3, 0, 511);
		int num8 = Mathf.Clamp((int)num4, 0, 511);
		groundPollution = 0f;
		waterProximity = 0f;
		float num9 = 1f / ((num3 - num) * (num4 - num2) * 255f);
		for (int i = num6; i <= num8; i++)
		{
			float num10 = 1f;
			if (num2 > (float)i)
			{
				num10 -= num2 - (float)i;
			}
			if (num4 < (float)(i + 1))
			{
				num10 -= (float)(i + 1) - num4;
			}
			for (int j = num5; j <= num7; j++)
			{
				float num11 = 1f;
				if (num > (float)j)
				{
					num11 -= num - (float)j;
				}
				if (num3 < (float)(j + 1))
				{
					num11 -= (float)(j + 1) - num3;
				}
				float num12 = num11 * num10 * num9;
				int num13 = i * 512 + j;
				groundPollution += (float)this.m_naturalResources[num13].m_pollution * num12;
				waterProximity += (float)this.m_naturalResources[num13].m_water * num12;
			}
		}
	}

	public void AddPollutionDisposeRate(int delta)
	{
		this.m_tempPollutionDisposeRate = Mathf.Clamp(this.m_tempPollutionDisposeRate + delta, 0, 127);
	}

	protected override void SimulationStepImpl(int subStep)
	{
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		if ((properties.m_mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (subStep <= 1)
			{
				uint num = (Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u) << 4;
				for (uint num2 = 0u; num2 < 16u; num2 += 1u)
				{
					this.SimulationStepImpl2(num | num2);
				}
			}
		}
		else if (subStep != 0 && subStep != 1000)
		{
			this.SimulationStepImpl2(Singleton<SimulationManager>.get_instance().m_currentFrameIndex);
		}
	}

	private void SimulationStepImpl2(uint simulationFrame)
	{
		int num = (int)(simulationFrame & 1023u);
		int num2 = num * 512 >> 10;
		int num3 = num * 512 * 512 >> 10 & 511;
		int num4 = ((num + 1) * 512 * 512 >> 10) - 1 & 511;
		int num5 = -1;
		int maxX = -1;
		int num6 = -1;
		int maxX2 = -1;
		Vector3 position;
		position.z = ((float)num2 - 256f + 0.5f) * 33.75f;
		position.y = 0f;
		int num7 = 18;
		int num8 = (num2 * num7 >> 10) * (512 * num7) + num7 * num3;
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		if (num == 0)
		{
			this.m_finalPollutionDisposeRate = this.m_tempPollutionDisposeRate;
			this.m_tempPollutionDisposeRate = 1;
			int num9 = this.m_areaResources.Length;
			for (int i = 0; i < num9; i++)
			{
				NaturalResourceManager.ApplyAreaResources(ref this.m_areaResources[i]);
			}
		}
		if ((simulationFrame & 4095u) == 0u)
		{
			NaturalResourceManager.ApplyAreaResources(ref this.m_usedResources);
		}
		for (int j = num3; j <= num4; j++)
		{
			position.x = ((float)j - 256f + 0.5f) * 33.75f;
			int num10 = num2 * 512 + j;
			NaturalResourceManager.ResourceCell resourceCell = this.m_naturalResources[num10];
			int num11;
			int num12;
			int num13;
			instance.CountWaterCoverage(position, 20f, out num11, out num12, out num13);
			int num14 = num8 >> 10;
			NaturalResourceManager.AddAreaResources(ref this.m_areaResources[num14], ref resourceCell, num11);
			num8 += num7;
			if ((int)resourceCell.m_water < num11)
			{
				num11 = Mathf.Min((int)(resourceCell.m_water + 10), num11);
			}
			else if ((int)resourceCell.m_water > num11)
			{
				num11 = Mathf.Max((int)(resourceCell.m_water - 10), num11);
			}
			else
			{
				num11 = (int)resourceCell.m_water;
			}
			if ((int)resourceCell.m_shore < num12)
			{
				num12 = Mathf.Min((int)(resourceCell.m_shore + 5), num12);
			}
			else if ((int)resourceCell.m_shore > num12)
			{
				num12 = Mathf.Max((int)(resourceCell.m_shore - 2), num12);
			}
			else
			{
				num12 = (int)resourceCell.m_shore;
			}
			int num15 = (int)resourceCell.m_burned;
			if (num15 > 0)
			{
				num15 = Mathf.Max(num15 - 2, 0);
			}
			int num16 = (int)resourceCell.m_destroyed;
			if (num16 > 0)
			{
				num16 = Mathf.Max(num16 - 2, 0);
			}
			int num17 = num13 - (int)resourceCell.m_pollution;
			if (num17 > 0)
			{
				num13 = Mathf.Min((int)resourceCell.m_pollution + (num17 >> 5) + 1, num13);
			}
			else if (num17 < 0)
			{
				num13 = Mathf.Max((int)resourceCell.m_pollution - (-num17 * this.m_finalPollutionDisposeRate >> 6) - this.m_finalPollutionDisposeRate, num13);
			}
			else
			{
				num13 = (int)resourceCell.m_pollution;
			}
			bool flag = false;
			if (num12 != (int)resourceCell.m_shore || (resourceCell.m_modified & 1) != 0)
			{
				resourceCell.m_water = (byte)num11;
				resourceCell.m_shore = (byte)num12;
				resourceCell.m_modified &= 254;
				if (num5 == -1)
				{
					num5 = j;
				}
				maxX = j;
				flag = true;
			}
			else if (num11 != (int)resourceCell.m_water)
			{
				resourceCell.m_water = (byte)num11;
				this.m_naturalResources[num10] = resourceCell;
				flag = true;
			}
			if (num13 != (int)resourceCell.m_pollution || num15 != (int)resourceCell.m_burned || num16 != (int)resourceCell.m_destroyed)
			{
				resourceCell.m_pollution = (byte)num13;
				resourceCell.m_burned = (byte)num15;
				resourceCell.m_destroyed = (byte)num16;
				resourceCell.m_modified &= 253;
				if (num6 == -1)
				{
					num6 = j;
				}
				maxX2 = j;
				flag = true;
			}
			if (flag)
			{
				this.m_naturalResources[num10] = resourceCell;
			}
		}
		if (num5 != -1)
		{
			this.AreaModified(num5, num2, maxX, num2);
		}
		if (num6 != -1)
		{
			this.AreaModifiedB(num6, num2, maxX2, num2);
		}
	}

	private static void AddAreaResources(ref NaturalResourceManager.AreaCell areaCell, ref NaturalResourceManager.ResourceCell resourceCell, int water)
	{
		areaCell.m_tempOre += (uint)resourceCell.m_ore;
		areaCell.m_tempOil += (uint)resourceCell.m_oil;
		areaCell.m_tempForest += (uint)resourceCell.m_forest;
		areaCell.m_tempFertility += (uint)resourceCell.m_fertility;
		areaCell.m_tempWater += (uint)water;
	}

	private static void ApplyAreaResources(ref NaturalResourceManager.AreaCell areaCell)
	{
		areaCell.m_finalOre = areaCell.m_tempOre;
		areaCell.m_finalOil = areaCell.m_tempOil;
		areaCell.m_finalForest = areaCell.m_tempForest;
		areaCell.m_finalFertility = areaCell.m_tempFertility;
		areaCell.m_finalWater = areaCell.m_tempWater;
		areaCell.m_tempOre = 0u;
		areaCell.m_tempOil = 0u;
		areaCell.m_tempForest = 0u;
		areaCell.m_tempFertility = 0u;
		areaCell.m_tempWater = 0u;
	}

	public void GetTileResources(int x, int z, out uint ore, out uint oil, out uint forest, out uint fertility, out uint water)
	{
		int num = 2;
		ore = 0u;
		oil = 0u;
		forest = 0u;
		fertility = 0u;
		water = 0u;
		this.GetTileResourcesImpl(x + num, z + num, ref ore, ref oil, ref forest, ref fertility, ref water);
	}

	private void GetTileResourcesImpl(int x, int z, ref uint ore, ref uint oil, ref uint forest, ref uint fertility, ref uint water)
	{
		int num = z * 9 + x;
		this.GetTileResourcesImpl(ref this.m_areaResources[num], ref ore, ref oil, ref forest, ref fertility, ref water);
	}

	private void GetTileResourcesImpl(ref NaturalResourceManager.AreaCell cell, ref uint ore, ref uint oil, ref uint forest, ref uint fertility, ref uint water)
	{
		ore += cell.m_finalOre;
		oil += cell.m_finalOil;
		forest += cell.m_finalForest;
		fertility += cell.m_finalFertility;
		water += cell.m_finalWater;
	}

	public void CalculateUnlockedResources(out uint ore, out uint oil, out uint forest, out uint fertility, out uint water)
	{
		ore = 0u;
		oil = 0u;
		forest = 0u;
		fertility = 0u;
		water = 0u;
		GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
		int num = 2;
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (instance.IsUnlocked(j, i))
				{
					this.GetTileResourcesImpl(j + num, i + num, ref ore, ref oil, ref forest, ref fertility, ref water);
				}
			}
		}
	}

	public void CalculateUnlockableResources(out uint ore, out uint oil, out uint forest, out uint fertility, out uint water)
	{
		ore = 0u;
		oil = 0u;
		forest = 0u;
		fertility = 0u;
		water = 0u;
		int num = 2;
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				this.GetTileResourcesImpl(j + num, i + num, ref ore, ref oil, ref forest, ref fertility, ref water);
			}
		}
	}

	public void CalculateTotalResources(out uint ore, out uint oil, out uint forest, out uint fertility, out uint water)
	{
		ore = 0u;
		oil = 0u;
		forest = 0u;
		fertility = 0u;
		water = 0u;
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				this.GetTileResourcesImpl(j, i, ref ore, ref oil, ref forest, ref fertility, ref water);
			}
		}
	}

	public void CalculateUsedResources(out uint ore, out uint oil, out uint forest, out uint fertility)
	{
		ore = ((this.m_usedResources.m_tempOre <= this.m_usedResources.m_finalOre) ? this.m_usedResources.m_finalOre : this.m_usedResources.m_tempOre);
		oil = ((this.m_usedResources.m_tempOil <= this.m_usedResources.m_finalOil) ? this.m_usedResources.m_finalOil : this.m_usedResources.m_tempOil);
		forest = ((this.m_usedResources.m_tempForest <= this.m_usedResources.m_finalForest) ? this.m_usedResources.m_finalForest : this.m_usedResources.m_tempForest);
		fertility = ((this.m_usedResources.m_tempFertility <= this.m_usedResources.m_finalFertility) ? this.m_usedResources.m_finalFertility : this.m_usedResources.m_tempFertility);
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new NaturalResourceManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float RESOURCEGRID_CELL_SIZE = 33.75f;

	public const int RESOURCEGRID_RESOLUTION = 512;

	public NaturalResourceManager.ResourceCell[] m_naturalResources;

	public NaturalResourceManager.AreaCell[] m_areaResources;

	public NaturalResourceManager.AreaCell m_usedResources;

	public Texture2D m_resourceTexture;

	public Texture2D m_destructionTexture;

	private int[] m_modifiedX1;

	private int[] m_modifiedX2;

	private bool m_modified;

	private int[] m_modifiedBX1;

	private int[] m_modifiedBX2;

	private bool m_modifiedB;

	private int m_tempPollutionDisposeRate;

	private int m_finalPollutionDisposeRate;

	[NonSerialized]
	public ResourceWrapper m_ResourceWrapper;

	public enum Resource
	{
		[EnumPosition("Resource", "Ore", 0)]
		Ore,
		[EnumPosition("Decorative", "Sand", 1)]
		Sand,
		[EnumPosition("Resource", "Oil", 2)]
		Oil,
		[EnumPosition("Resource", "Fertility", 3)]
		Fertility,
		Forest,
		Tree,
		Water,
		Wind = 8,
		Sun,
		Pollution,
		Burned,
		Destroyed,
		None = 255
	}

	public enum ExtractedResource
	{
		None = -1,
		Ore,
		Oil,
		Fertility,
		Forest,
		Count
	}

	public struct ResourceCell
	{
		public byte m_ore;

		public byte m_oil;

		public byte m_sand;

		public byte m_forest;

		public byte m_tree;

		public byte m_fertility;

		public byte m_pollution;

		public byte m_water;

		public byte m_shore;

		public byte m_burned;

		public byte m_destroyed;

		public byte m_modified;
	}

	public struct AreaCell
	{
		public uint m_tempOre;

		public uint m_tempOil;

		public uint m_tempForest;

		public uint m_tempFertility;

		public uint m_tempWater;

		public uint m_finalOre;

		public uint m_finalOil;

		public uint m_finalForest;

		public uint m_finalFertility;

		public uint m_finalWater;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "NaturalResourceManager");
			NaturalResourceManager instance = Singleton<NaturalResourceManager>.get_instance();
			NaturalResourceManager.ResourceCell[] naturalResources = instance.m_naturalResources;
			NaturalResourceManager.AreaCell[] areaResources = instance.m_areaResources;
			int num = naturalResources.Length;
			int num2 = areaResources.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				@byte.Write(naturalResources[i].m_ore);
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int j = 0; j < num; j++)
			{
				byte2.Write(naturalResources[j].m_oil);
			}
			byte2.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int k = 0; k < num; k++)
			{
				byte3.Write(naturalResources[k].m_sand);
			}
			byte3.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int l = 0; l < num; l++)
			{
				byte4.Write(naturalResources[l].m_forest);
			}
			byte4.EndWrite();
			EncodedArray.Byte byte5 = EncodedArray.Byte.BeginWrite(s);
			for (int m = 0; m < num; m++)
			{
				if (naturalResources[m].m_forest != 0)
				{
					byte5.Write(naturalResources[m].m_tree);
				}
			}
			byte5.EndWrite();
			EncodedArray.Byte byte6 = EncodedArray.Byte.BeginWrite(s);
			for (int n = 0; n < num; n++)
			{
				if (naturalResources[n].m_forest == 0)
				{
					byte6.Write(naturalResources[n].m_fertility);
				}
			}
			byte6.EndWrite();
			EncodedArray.Byte byte7 = EncodedArray.Byte.BeginWrite(s);
			for (int num3 = 0; num3 < num; num3++)
			{
				byte7.Write(naturalResources[num3].m_water);
			}
			byte7.EndWrite();
			EncodedArray.Byte byte8 = EncodedArray.Byte.BeginWrite(s);
			for (int num4 = 0; num4 < num; num4++)
			{
				byte8.Write(naturalResources[num4].m_shore);
			}
			byte8.EndWrite();
			EncodedArray.Byte byte9 = EncodedArray.Byte.BeginWrite(s);
			for (int num5 = 0; num5 < num; num5++)
			{
				byte9.Write(naturalResources[num5].m_pollution);
			}
			byte9.EndWrite();
			EncodedArray.Byte byte10 = EncodedArray.Byte.BeginWrite(s);
			for (int num6 = 0; num6 < num; num6++)
			{
				byte10.Write(naturalResources[num6].m_burned);
			}
			byte10.EndWrite();
			EncodedArray.Byte byte11 = EncodedArray.Byte.BeginWrite(s);
			for (int num7 = 0; num7 < num; num7++)
			{
				byte11.Write(naturalResources[num7].m_destroyed);
			}
			byte11.EndWrite();
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int num8 = 0; num8 < num2; num8++)
			{
				uInt.Write(areaResources[num8].m_tempOre);
			}
			for (int num9 = 0; num9 < num2; num9++)
			{
				uInt.Write(areaResources[num9].m_tempOil);
			}
			for (int num10 = 0; num10 < num2; num10++)
			{
				uInt.Write(areaResources[num10].m_tempForest);
			}
			for (int num11 = 0; num11 < num2; num11++)
			{
				uInt.Write(areaResources[num11].m_tempFertility);
			}
			for (int num12 = 0; num12 < num2; num12++)
			{
				uInt.Write(areaResources[num12].m_finalOre);
			}
			for (int num13 = 0; num13 < num2; num13++)
			{
				uInt.Write(areaResources[num13].m_finalOil);
			}
			for (int num14 = 0; num14 < num2; num14++)
			{
				uInt.Write(areaResources[num14].m_finalForest);
			}
			for (int num15 = 0; num15 < num2; num15++)
			{
				uInt.Write(areaResources[num15].m_finalFertility);
			}
			for (int num16 = 0; num16 < num2; num16++)
			{
				uInt.Write(areaResources[num16].m_tempWater);
			}
			for (int num17 = 0; num17 < num2; num17++)
			{
				uInt.Write(areaResources[num17].m_finalWater);
			}
			uInt.EndWrite();
			s.WriteUInt32(instance.m_usedResources.m_tempOre);
			s.WriteUInt32(instance.m_usedResources.m_tempOil);
			s.WriteUInt32(instance.m_usedResources.m_tempForest);
			s.WriteUInt32(instance.m_usedResources.m_tempFertility);
			s.WriteUInt32(instance.m_usedResources.m_finalOre);
			s.WriteUInt32(instance.m_usedResources.m_finalOil);
			s.WriteUInt32(instance.m_usedResources.m_finalForest);
			s.WriteUInt32(instance.m_usedResources.m_finalFertility);
			s.WriteInt8(instance.m_tempPollutionDisposeRate);
			s.WriteInt8(instance.m_finalPollutionDisposeRate);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "NaturalResourceManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "NaturalResourceManager");
			NaturalResourceManager instance = Singleton<NaturalResourceManager>.get_instance();
			NaturalResourceManager.ResourceCell[] naturalResources = instance.m_naturalResources;
			NaturalResourceManager.AreaCell[] areaResources = instance.m_areaResources;
			int num = naturalResources.Length;
			int num2 = areaResources.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				naturalResources[i].m_ore = @byte.Read();
			}
			@byte.EndRead();
			if (s.get_version() < 22u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int j = 0; j < num; j++)
				{
					if (naturalResources[j].m_ore == 0)
					{
						byte2.Read();
					}
				}
				byte2.EndRead();
			}
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
			for (int k = 0; k < num; k++)
			{
				naturalResources[k].m_oil = byte3.Read();
			}
			byte3.EndRead();
			if (s.get_version() < 22u)
			{
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int l = 0; l < num; l++)
				{
					if (naturalResources[l].m_oil == 0)
					{
						byte4.Read();
					}
				}
				byte4.EndRead();
			}
			if (s.get_version() >= 27u)
			{
				EncodedArray.Byte byte5 = EncodedArray.Byte.BeginRead(s);
				for (int m = 0; m < num; m++)
				{
					naturalResources[m].m_sand = byte5.Read();
				}
				byte5.EndRead();
			}
			else
			{
				for (int n = 0; n < num; n++)
				{
					naturalResources[n].m_sand = 0;
				}
			}
			EncodedArray.Byte byte6 = EncodedArray.Byte.BeginRead(s);
			for (int num3 = 0; num3 < num; num3++)
			{
				naturalResources[num3].m_forest = byte6.Read();
			}
			byte6.EndRead();
			EncodedArray.Byte byte7 = EncodedArray.Byte.BeginRead(s);
			for (int num4 = 0; num4 < num; num4++)
			{
				if (naturalResources[num4].m_forest != 0)
				{
					naturalResources[num4].m_tree = byte7.Read();
				}
				else
				{
					naturalResources[num4].m_tree = 0;
				}
			}
			byte7.EndRead();
			EncodedArray.Byte byte8 = EncodedArray.Byte.BeginRead(s);
			for (int num5 = 0; num5 < num; num5++)
			{
				if (naturalResources[num5].m_forest == 0)
				{
					naturalResources[num5].m_fertility = byte8.Read();
				}
				else
				{
					naturalResources[num5].m_fertility = 0;
				}
			}
			byte8.EndRead();
			if (s.get_version() >= 22u)
			{
				EncodedArray.Byte byte9 = EncodedArray.Byte.BeginRead(s);
				for (int num6 = 0; num6 < num; num6++)
				{
					naturalResources[num6].m_water = byte9.Read();
				}
				byte9.EndRead();
			}
			else
			{
				for (int num7 = 0; num7 < num; num7++)
				{
					naturalResources[num7].m_water = 0;
				}
			}
			if (s.get_version() >= 223u)
			{
				EncodedArray.Byte byte10 = EncodedArray.Byte.BeginRead(s);
				for (int num8 = 0; num8 < num; num8++)
				{
					naturalResources[num8].m_shore = byte10.Read();
				}
				byte10.EndRead();
			}
			else
			{
				for (int num9 = 0; num9 < num; num9++)
				{
					naturalResources[num9].m_shore = naturalResources[num9].m_water;
				}
			}
			EncodedArray.Byte byte11 = EncodedArray.Byte.BeginRead(s);
			for (int num10 = 0; num10 < num; num10++)
			{
				naturalResources[num10].m_pollution = byte11.Read();
			}
			byte11.EndRead();
			if (s.get_version() >= 265u)
			{
				EncodedArray.Byte byte12 = EncodedArray.Byte.BeginRead(s);
				for (int num11 = 0; num11 < num; num11++)
				{
					naturalResources[num11].m_burned = byte12.Read();
				}
				byte12.EndRead();
			}
			else
			{
				for (int num12 = 0; num12 < num; num12++)
				{
					naturalResources[num12].m_burned = 0;
				}
			}
			if (s.get_version() >= 265u)
			{
				EncodedArray.Byte byte13 = EncodedArray.Byte.BeginRead(s);
				for (int num13 = 0; num13 < num; num13++)
				{
					naturalResources[num13].m_destroyed = byte13.Read();
				}
				byte13.EndRead();
			}
			else
			{
				for (int num14 = 0; num14 < num; num14++)
				{
					naturalResources[num14].m_destroyed = 0;
				}
			}
			if (s.get_version() < 116u)
			{
				EncodedArray.Byte byte14 = EncodedArray.Byte.BeginRead(s);
				for (int num15 = 0; num15 < num; num15++)
				{
					if (naturalResources[num15].m_water != 0 || s.get_version() < 22u)
					{
						byte14.Read();
					}
				}
				byte14.EndRead();
			}
			if (s.get_version() >= 111u)
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int num16 = 0; num16 < num2; num16++)
				{
					areaResources[num16].m_tempOre = uInt.Read();
				}
				for (int num17 = 0; num17 < num2; num17++)
				{
					areaResources[num17].m_tempOil = uInt.Read();
				}
				for (int num18 = 0; num18 < num2; num18++)
				{
					areaResources[num18].m_tempForest = uInt.Read();
				}
				for (int num19 = 0; num19 < num2; num19++)
				{
					areaResources[num19].m_tempFertility = uInt.Read();
				}
				for (int num20 = 0; num20 < num2; num20++)
				{
					areaResources[num20].m_finalOre = uInt.Read();
				}
				for (int num21 = 0; num21 < num2; num21++)
				{
					areaResources[num21].m_finalOil = uInt.Read();
				}
				for (int num22 = 0; num22 < num2; num22++)
				{
					areaResources[num22].m_finalForest = uInt.Read();
				}
				for (int num23 = 0; num23 < num2; num23++)
				{
					areaResources[num23].m_finalFertility = uInt.Read();
				}
				if (s.get_version() >= 125u)
				{
					for (int num24 = 0; num24 < num2; num24++)
					{
						areaResources[num24].m_tempWater = uInt.Read();
					}
					for (int num25 = 0; num25 < num2; num25++)
					{
						areaResources[num25].m_finalWater = uInt.Read();
					}
				}
				else
				{
					for (int num26 = 0; num26 < num2; num26++)
					{
						areaResources[num26].m_tempWater = 0u;
						areaResources[num26].m_finalWater = 0u;
					}
				}
				uInt.EndRead();
			}
			else
			{
				for (int num27 = 0; num27 < num2; num27++)
				{
					areaResources[num27].m_tempOre = 0u;
					areaResources[num27].m_tempOil = 0u;
					areaResources[num27].m_tempForest = 0u;
					areaResources[num27].m_tempFertility = 0u;
					areaResources[num27].m_finalOre = 0u;
					areaResources[num27].m_finalOil = 0u;
					areaResources[num27].m_finalForest = 0u;
					areaResources[num27].m_finalFertility = 0u;
					areaResources[num27].m_tempWater = 0u;
					areaResources[num27].m_finalWater = 0u;
				}
			}
			if (s.get_version() >= 116u)
			{
				instance.m_usedResources.m_tempOre = s.ReadUInt32();
				instance.m_usedResources.m_tempOil = s.ReadUInt32();
				instance.m_usedResources.m_tempForest = s.ReadUInt32();
				instance.m_usedResources.m_tempFertility = s.ReadUInt32();
				instance.m_usedResources.m_finalOre = s.ReadUInt32();
				instance.m_usedResources.m_finalOil = s.ReadUInt32();
				instance.m_usedResources.m_finalForest = s.ReadUInt32();
				instance.m_usedResources.m_finalFertility = s.ReadUInt32();
			}
			else
			{
				instance.m_usedResources.m_tempOre = 0u;
				instance.m_usedResources.m_tempOil = 0u;
				instance.m_usedResources.m_tempForest = 0u;
				instance.m_usedResources.m_tempFertility = 0u;
				instance.m_usedResources.m_finalOre = 0u;
				instance.m_usedResources.m_finalOil = 0u;
				instance.m_usedResources.m_finalForest = 0u;
				instance.m_usedResources.m_finalFertility = 0u;
			}
			if (s.get_version() >= 147u)
			{
				instance.m_tempPollutionDisposeRate = s.ReadInt8();
				instance.m_finalPollutionDisposeRate = s.ReadInt8();
			}
			else
			{
				instance.m_tempPollutionDisposeRate = 0;
				instance.m_finalPollutionDisposeRate = 0;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "NaturalResourceManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "NaturalResourceManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			NaturalResourceManager instance = Singleton<NaturalResourceManager>.get_instance();
			instance.AreaModified(0, 0, 511, 511);
			instance.AreaModifiedB(0, 0, 511, 511);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "NaturalResourceManager");
		}
	}
}
