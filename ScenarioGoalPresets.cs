﻿using System;
using ColossalFramework.Globalization;
using UnityEngine;

public class ScenarioGoalPresets : ScriptableObject
{
	public ScenarioGoalPresets.StatisticPreset[] m_statisticPresets;

	public ScenarioGoalPresets.ServicePreset[] m_servicePresets;

	public MilestoneInfo[] m_progressionMilestones;

	[Serializable]
	public class StatisticPreset
	{
		public string GetDescription()
		{
			string text = this.m_value.ToString();
			if (this.m_arrayIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text, this.m_value, this.m_arrayIndex);
			}
			if (this.m_divider != StatisticType.None)
			{
				string text2 = this.m_divider.ToString();
				if (this.m_dividerIndex != -1)
				{
					StatisticMilestone.ModifyValueName(ref text2, this.m_divider, this.m_dividerIndex);
				}
				text = StringUtils.SafeFormat("{0}/{1}", new object[]
				{
					text,
					text2
				});
			}
			if (this.m_add != StatisticType.None)
			{
				string text3 = this.m_add.ToString();
				if (this.m_addIndex != -1)
				{
					StatisticMilestone.ModifyValueName(ref text3, this.m_add, this.m_addIndex);
				}
				text = StringUtils.SafeFormat("{0}+{1}", new object[]
				{
					text,
					text3
				});
			}
			if (this.m_subtract != StatisticType.None)
			{
				string text4 = this.m_subtract.ToString();
				if (this.m_subtractIndex != -1)
				{
					StatisticMilestone.ModifyValueName(ref text4, this.m_subtract, this.m_subtractIndex);
				}
				text = StringUtils.SafeFormat("{0}-{1}", new object[]
				{
					text,
					text4
				});
			}
			switch (this.m_targetType)
			{
			case StatisticMilestone.TargetType.TotalInt_Greater:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_DESC", text), "X");
			case StatisticMilestone.TargetType.LatestInt_Greater:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_DESC", text), "X");
			case StatisticMilestone.TargetType.LatestInt_Less:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_DESC", text), "X");
			case StatisticMilestone.TargetType.TotalFloat_Greater:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_DESC", text), "X");
			case StatisticMilestone.TargetType.LatestFloat_Greater:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_DESC", text), "X");
			case StatisticMilestone.TargetType.LatestFloat_Less:
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_DESC", text), "X");
			default:
				return "Undefined";
			}
		}

		public StatisticType m_value = StatisticType.None;

		public StatisticType m_divider = StatisticType.None;

		public StatisticType m_add = StatisticType.None;

		public StatisticType m_subtract = StatisticType.None;

		public int m_arrayIndex = -1;

		public int m_dividerIndex = -1;

		public int m_addIndex = -1;

		public int m_subtractIndex = -1;

		public int m_resultOffsetInt;

		public float m_resultOffsetFloat;

		public StatisticMilestone.TargetType m_targetType = StatisticMilestone.TargetType.TotalInt_Greater;

		public int m_minTotalInt;

		public int m_maxTotalInt;

		public int m_minLatestInt;

		public int m_maxLatestInt;

		public float m_minTotalFloat;

		public float m_maxTotalFloat;

		public float m_minLatestFloat;

		public float m_maxLatestFloat;
	}

	[Serializable]
	public class ServicePreset
	{
		public string GetDescription()
		{
			if (!string.IsNullOrEmpty(this.m_localeKey))
			{
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_BUILDING_COUNT_DESC", this.m_localeKey), "X");
			}
			return "Undefined";
		}

		public ItemClass.Service m_service;

		public ItemClass.SubService m_subService;

		public ItemClass.Level m_level = ItemClass.Level.None;

		public bool m_requireDifferentBuildings;

		public string m_localeKey = string.Empty;
	}
}
