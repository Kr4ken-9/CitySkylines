﻿using System;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class FadeEffect : ImageEffectBase
{
	public float amount
	{
		get
		{
			return this.m_Amount;
		}
		set
		{
			this.m_Amount = value;
			base.set_enabled(this.m_Amount > 0f);
		}
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		base.get_material().SetFloat("_Amount", this.m_Amount);
		Graphics.Blit(source, destination, base.get_material());
	}

	private float m_Amount;
}
