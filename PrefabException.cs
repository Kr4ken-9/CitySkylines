﻿using System;

public class PrefabException : Exception
{
	public PrefabException(PrefabInfo info, string message) : base(message)
	{
		this.m_prefabInfo = info;
	}

	public PrefabInfo m_prefabInfo;
}
