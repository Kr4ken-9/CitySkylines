﻿using System;
using ColossalFramework;
using UnityEngine;

public class AudioGroup
{
	public AudioGroup(int maxActiveCount, SavedFloat volume)
	{
		this.m_maxActiveCount = maxActiveCount;
		this.m_groupVolume = volume;
		this.m_cachedVolume = volume;
		this.m_playerData = new AudioGroup.PlayerData[maxActiveCount];
		for (int i = 0; i < maxActiveCount; i++)
		{
			this.m_playerData[i] = new AudioGroup.PlayerData();
		}
	}

	public void Reset()
	{
		this.m_playerDataCount = 0;
		if (this.m_playerData != null)
		{
			for (int i = 0; i < this.m_playerData.Length; i++)
			{
				this.m_playerData[i].m_info = null;
				this.m_playerData[i].m_clip = null;
			}
		}
		while (this.m_players != null)
		{
			AudioManager.AudioPlayer nextPlayer = this.m_players.m_nextPlayer;
			Singleton<AudioManager>.get_instance().ReleasePlayer(this.m_players);
			this.m_players = nextPlayer;
		}
	}

	public void AddPlayer(AudioManager.ListenerInfo listenerInfo, int playerID, AudioInfo info, Vector3 position, Vector3 velocity, float distance, float volume, float pitch)
	{
		if (this.m_totalVolume < 0.01f)
		{
			return;
		}
		float num;
		if (info.m_is3D)
		{
			num = distance - Vector3.Distance(position, listenerInfo.m_position);
		}
		else
		{
			num = volume;
		}
		if (this.m_playerDataCount == this.m_maxActiveCount)
		{
			if (num <= this.m_playerData[this.m_playerDataCount - 1].m_priority)
			{
				return;
			}
		}
		else if (num < 0.01f)
		{
			return;
		}
		int num2 = 0;
		while (num2 < this.m_playerDataCount && this.m_playerData[num2].m_priority >= num)
		{
			num2++;
		}
		if (num2 < this.m_maxActiveCount)
		{
			for (int i = Mathf.Min(this.m_maxActiveCount - 1, this.m_playerDataCount); i > num2; i--)
			{
				AudioGroup.PlayerData playerData = this.m_playerData[i];
				this.m_playerData[i] = this.m_playerData[i - 1];
				this.m_playerData[i - 1] = playerData;
			}
			AudioGroup.PlayerData playerData2 = this.m_playerData[num2];
			playerData2.m_id = playerID;
			playerData2.m_info = info;
			playerData2.m_clip = null;
			playerData2.m_position = ((!info.m_is3D) ? Vector3.get_zero() : position);
			playerData2.m_velocity = ((!info.m_is3D) ? Vector3.get_zero() : velocity);
			playerData2.m_targetDistance = ((!info.m_is3D) ? 1f : distance);
			playerData2.m_targetVolume = info.m_volume * volume * this.m_cachedVolume;
			playerData2.m_targetPitch = info.m_pitch * pitch;
			playerData2.m_priority = num;
			playerData2.m_fadeSpeed = 1f / info.m_fadeLength;
			playerData2.m_loop = info.m_loop;
			playerData2.m_is3d = info.m_is3D;
			playerData2.m_startTimeSource = -1;
			playerData2.m_randomStartTime = info.m_randomTime;
			if (this.m_playerDataCount < this.m_maxActiveCount)
			{
				this.m_playerDataCount++;
			}
		}
	}

	public void AddPlayer(int playerID, AudioClip clip, float volume, bool loop)
	{
		if (this.m_totalVolume < 0.01f)
		{
			return;
		}
		if (this.m_playerDataCount == this.m_maxActiveCount)
		{
			if (volume <= this.m_playerData[this.m_playerDataCount - 1].m_priority)
			{
				return;
			}
		}
		else if (volume < 0.01f)
		{
			return;
		}
		int num = 0;
		while (num < this.m_playerDataCount && this.m_playerData[num].m_priority >= volume)
		{
			num++;
		}
		if (num < this.m_maxActiveCount)
		{
			for (int i = Mathf.Min(this.m_maxActiveCount - 1, this.m_playerDataCount); i > num; i--)
			{
				AudioGroup.PlayerData playerData = this.m_playerData[i];
				this.m_playerData[i] = this.m_playerData[i - 1];
				this.m_playerData[i - 1] = playerData;
			}
			AudioGroup.PlayerData playerData2 = this.m_playerData[num];
			playerData2.m_id = playerID;
			playerData2.m_info = null;
			playerData2.m_clip = clip;
			playerData2.m_position = Vector3.get_zero();
			playerData2.m_velocity = Vector3.get_zero();
			playerData2.m_targetDistance = 1f;
			playerData2.m_targetVolume = volume * this.m_cachedVolume;
			playerData2.m_targetPitch = 1f;
			playerData2.m_priority = volume;
			playerData2.m_fadeSpeed = 10f;
			playerData2.m_loop = loop;
			playerData2.m_is3d = false;
			playerData2.m_startTimeSource = -1;
			playerData2.m_randomStartTime = false;
			if (this.m_playerDataCount < this.m_maxActiveCount)
			{
				this.m_playerDataCount++;
			}
		}
	}

	public void AddPlayer(int playerID, AudioInfo info, float volume)
	{
		if (this.m_totalVolume < 0.01f || info == null)
		{
			return;
		}
		if (this.m_playerDataCount == this.m_maxActiveCount)
		{
			if (volume <= this.m_playerData[this.m_playerDataCount - 1].m_priority)
			{
				return;
			}
		}
		else if (volume < 0.01f)
		{
			return;
		}
		int num = 0;
		while (num < this.m_playerDataCount && this.m_playerData[num].m_priority >= volume)
		{
			num++;
		}
		if (num < this.m_maxActiveCount)
		{
			for (int i = Mathf.Min(this.m_maxActiveCount - 1, this.m_playerDataCount); i > num; i--)
			{
				AudioGroup.PlayerData playerData = this.m_playerData[i];
				this.m_playerData[i] = this.m_playerData[i - 1];
				this.m_playerData[i - 1] = playerData;
			}
			AudioGroup.PlayerData playerData2 = this.m_playerData[num];
			playerData2.m_id = playerID;
			playerData2.m_info = info;
			playerData2.m_clip = null;
			playerData2.m_position = Vector3.get_zero();
			playerData2.m_velocity = Vector3.get_zero();
			playerData2.m_targetDistance = 1f;
			playerData2.m_targetVolume = info.m_volume * volume * this.m_cachedVolume;
			playerData2.m_targetPitch = info.m_pitch;
			playerData2.m_priority = volume;
			playerData2.m_fadeSpeed = 1f / info.m_fadeLength;
			playerData2.m_loop = info.m_loop;
			playerData2.m_is3d = info.m_is3D;
			playerData2.m_startTimeSource = -1;
			playerData2.m_randomStartTime = info.m_randomTime;
			if (this.m_playerDataCount < this.m_maxActiveCount)
			{
				this.m_playerDataCount++;
			}
		}
	}

	public void UpdatePlayers(AudioManager.ListenerInfo listenerInfo, float masterVolume)
	{
		AudioManager.AudioPlayer audioPlayer = null;
		AudioManager.AudioPlayer audioPlayer2 = this.m_players;
		int num = 0;
		while (audioPlayer2 != null)
		{
			if (audioPlayer2.m_source.get_loop())
			{
				bool flag = false;
				int id = audioPlayer2.m_id;
				AudioInfo info = audioPlayer2.m_info;
				AudioClip clip = audioPlayer2.m_source.get_clip();
				for (int i = 0; i < this.m_playerDataCount; i++)
				{
					AudioGroup.PlayerData playerData = this.m_playerData[i];
					if (playerData.m_id == id && playerData.m_info == info && (info != null || playerData.m_clip == clip))
					{
						float num2 = playerData.m_targetVolume * (1f - (float)i / (float)this.m_maxActiveCount);
						if (playerData.m_is3d)
						{
							if (audioPlayer2.m_source.get_maxDistance() < playerData.m_targetDistance)
							{
								audioPlayer2.m_source.set_maxDistance(Mathf.Min(playerData.m_targetDistance, audioPlayer2.m_source.get_maxDistance() + Time.get_deltaTime() * playerData.m_fadeSpeed * 100f));
							}
							else if (audioPlayer2.m_source.get_maxDistance() > playerData.m_targetDistance)
							{
								audioPlayer2.m_source.set_maxDistance(Mathf.Max(playerData.m_targetDistance, audioPlayer2.m_source.get_maxDistance() - Time.get_deltaTime() * playerData.m_fadeSpeed * 100f));
							}
							audioPlayer2.m_transform.set_position(playerData.m_position);
							audioPlayer2.m_velocity = playerData.m_velocity;
							audioPlayer2.m_source.set_dopplerLevel(listenerInfo.m_dopplerLevel);
						}
						if (audioPlayer2.m_source.get_pitch() < playerData.m_targetPitch)
						{
							audioPlayer2.m_source.set_pitch(Mathf.Min(playerData.m_targetPitch, audioPlayer2.m_source.get_pitch() + Time.get_deltaTime() * playerData.m_fadeSpeed));
						}
						else if (audioPlayer2.m_source.get_pitch() > playerData.m_targetPitch)
						{
							audioPlayer2.m_source.set_pitch(Mathf.Max(playerData.m_targetPitch, audioPlayer2.m_source.get_pitch() - Time.get_deltaTime() * playerData.m_fadeSpeed));
						}
						if (audioPlayer2.m_source.get_volume() < num2)
						{
							audioPlayer2.m_source.set_volume(Mathf.Min(num2, audioPlayer2.m_source.get_volume() + Time.get_deltaTime() * playerData.m_fadeSpeed));
						}
						else if (audioPlayer2.m_source.get_volume() > num2)
						{
							audioPlayer2.m_source.set_volume(Mathf.Max(num2, audioPlayer2.m_source.get_volume() - Time.get_deltaTime() * playerData.m_fadeSpeed));
						}
						audioPlayer2.m_source.set_priority(255 - i * 255 / this.m_maxActiveCount);
						playerData.m_info = null;
						playerData.m_clip = null;
						flag = true;
						break;
					}
				}
				if (flag)
				{
					if (audioPlayer2.m_notReady && audioPlayer2.m_source.get_clip().get_loadState() == 2)
					{
						audioPlayer2.m_notReady = false;
						audioPlayer2.m_source.Play();
					}
				}
				else
				{
					if (audioPlayer2.m_is3d)
					{
						audioPlayer2.m_transform.set_position(audioPlayer2.m_transform.get_position() + audioPlayer2.m_velocity * Time.get_deltaTime());
						audioPlayer2.m_source.set_dopplerLevel(listenerInfo.m_dopplerLevel);
					}
					audioPlayer2.m_source.set_volume(Mathf.Max(0f, audioPlayer2.m_source.get_volume() - Time.get_deltaTime() * audioPlayer2.m_fadeSpeed));
					if (audioPlayer2.m_source.get_volume() < 0.01f)
					{
						AudioManager.AudioPlayer nextPlayer = audioPlayer2.m_nextPlayer;
						if (audioPlayer != null)
						{
							audioPlayer.m_nextPlayer = nextPlayer;
						}
						else
						{
							this.m_players = nextPlayer;
						}
						Singleton<AudioManager>.get_instance().ReleasePlayer(audioPlayer2);
						audioPlayer2 = nextPlayer;
						continue;
					}
				}
			}
			else
			{
				bool flag2 = false;
				int id2 = audioPlayer2.m_id;
				AudioInfo info2 = audioPlayer2.m_info;
				AudioClip clip2 = audioPlayer2.m_source.get_clip();
				for (int j = 0; j < this.m_playerDataCount; j++)
				{
					AudioGroup.PlayerData playerData2 = this.m_playerData[j];
					if (playerData2.m_id == id2 && playerData2.m_info == info2 && (info2 != null || playerData2.m_clip == clip2))
					{
						playerData2.m_info = null;
						playerData2.m_clip = null;
						flag2 = true;
						break;
					}
				}
				if (audioPlayer2.m_is3d)
				{
					audioPlayer2.m_transform.set_position(audioPlayer2.m_transform.get_position() + audioPlayer2.m_velocity * Time.get_deltaTime());
					audioPlayer2.m_source.set_dopplerLevel(listenerInfo.m_dopplerLevel);
				}
				float num3 = Mathf.Max(0f, 1f - (float)num / (float)this.m_maxActiveCount);
				if (num3 < audioPlayer2.m_source.get_volume())
				{
					audioPlayer2.m_source.set_volume(Mathf.Max(num3, audioPlayer2.m_source.get_volume() - Time.get_deltaTime() * audioPlayer2.m_fadeSpeed));
				}
				audioPlayer2.m_source.set_priority(Mathf.Max(0, 255 - num * 255 / this.m_maxActiveCount));
				if (audioPlayer2.m_notReady)
				{
					if (audioPlayer2.m_source.get_clip().get_loadState() == 2)
					{
						audioPlayer2.m_notReady = false;
						audioPlayer2.m_source.Play();
					}
					num++;
				}
				else
				{
					if ((!audioPlayer2.m_source.get_isPlaying() || audioPlayer2.m_source.get_volume() < 0.01f) && !flag2)
					{
						AudioManager.AudioPlayer nextPlayer2 = audioPlayer2.m_nextPlayer;
						if (audioPlayer != null)
						{
							audioPlayer.m_nextPlayer = nextPlayer2;
						}
						else
						{
							this.m_players = nextPlayer2;
						}
						Singleton<AudioManager>.get_instance().ReleasePlayer(audioPlayer2);
						audioPlayer2 = nextPlayer2;
						continue;
					}
					num++;
				}
			}
			audioPlayer = audioPlayer2;
			audioPlayer2 = audioPlayer2.m_nextPlayer;
		}
		for (int k = 0; k < this.m_playerDataCount; k++)
		{
			AudioGroup.PlayerData playerData3 = this.m_playerData[k];
			AudioManager.AudioPlayer audioPlayer3 = null;
			float time = 0f;
			if (playerData3.m_info != null)
			{
				audioPlayer3 = Singleton<AudioManager>.get_instance().ObtainPlayer(playerData3.m_info);
				if (playerData3.m_randomStartTime)
				{
					time = Random.get_value() * playerData3.m_info.GetLength();
				}
			}
			else if (playerData3.m_clip != null)
			{
				audioPlayer3 = Singleton<AudioManager>.get_instance().ObtainPlayer(playerData3.m_clip);
				if (playerData3.m_randomStartTime)
				{
					time = Random.get_value() * playerData3.m_clip.get_length();
				}
			}
			if (audioPlayer3 != null)
			{
				audioPlayer3.m_id = playerData3.m_id;
				audioPlayer3.m_nextPlayer = this.m_players;
				audioPlayer3.m_is3d = playerData3.m_is3d;
				audioPlayer3.m_fadeSpeed = playerData3.m_fadeSpeed;
				this.m_players = audioPlayer3;
				float num4 = playerData3.m_targetVolume * (1f - (float)k / (float)this.m_maxActiveCount);
				if (playerData3.m_is3d)
				{
					audioPlayer3.m_source.set_dopplerLevel(listenerInfo.m_dopplerLevel);
					audioPlayer3.m_source.set_maxDistance(playerData3.m_targetDistance);
					audioPlayer3.m_transform.set_position(playerData3.m_position);
					audioPlayer3.m_velocity = playerData3.m_velocity;
				}
				audioPlayer3.m_source.set_pitch(playerData3.m_targetPitch);
				audioPlayer3.m_source.set_loop(playerData3.m_loop);
				audioPlayer3.m_source.set_minDistance(0f);
				audioPlayer3.m_source.set_rolloffMode(1);
				audioPlayer3.m_source.set_spatialBlend((!playerData3.m_is3d) ? 0f : 1f);
				audioPlayer3.m_source.set_reverbZoneMix((!playerData3.m_is3d) ? 0f : 1f);
				if (playerData3.m_loop)
				{
					audioPlayer3.m_source.set_volume(Mathf.Min(num4, Time.get_deltaTime() * playerData3.m_fadeSpeed));
				}
				else
				{
					audioPlayer3.m_source.set_volume(num4);
				}
				audioPlayer3.m_source.set_time(time);
				audioPlayer3.m_source.set_priority(255 - k * 255 / this.m_maxActiveCount);
				if (audioPlayer3.m_source.get_clip().get_loadState() == 2)
				{
					audioPlayer3.m_notReady = false;
					audioPlayer3.m_source.Play();
				}
				else
				{
					audioPlayer3.m_notReady = true;
				}
			}
		}
		this.m_playerDataCount = 0;
		this.m_cachedVolume = this.m_groupVolume;
		this.m_totalVolume = this.m_cachedVolume * masterVolume;
	}

	private SavedFloat m_groupVolume;

	private AudioManager.AudioPlayer m_players;

	private AudioGroup.PlayerData[] m_playerData;

	private int m_maxActiveCount;

	private int m_playerDataCount;

	public float m_cachedVolume;

	public float m_totalVolume;

	public class PlayerData
	{
		public int m_id;

		public AudioInfo m_info;

		public AudioClip m_clip;

		public Vector3 m_position;

		public Vector3 m_velocity;

		public float m_targetDistance;

		public float m_targetVolume;

		public float m_targetPitch;

		public float m_priority;

		public float m_fadeSpeed;

		public int m_startTimeSource;

		public bool m_loop;

		public bool m_is3d;

		public bool m_randomStartTime;
	}
}
