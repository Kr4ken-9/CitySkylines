﻿using System;

public class BarChart
{
	public BarChart(BarChartBar[] bars)
	{
		this.m_bars = bars;
		this.m_values = new long[bars.Length];
	}

	public void Update()
	{
		long num = -9223372036854775808L;
		for (int i = 0; i < this.m_bars.Length; i++)
		{
			this.m_values[i] = this.m_bars[i].PollIncome();
			if (this.m_values[i] > num)
			{
				num = this.m_values[i];
			}
		}
		if (num == 0L)
		{
			num = 1L;
		}
		for (int j = 0; j < this.m_bars.Length; j++)
		{
			this.m_bars[j].SetValue(this.m_values[j], num);
		}
	}

	private BarChartBar[] m_bars;

	private long[] m_values;
}
