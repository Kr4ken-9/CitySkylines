﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class RoadMaintenanceInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("CoverageGradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("ConditionGradient");
				if (Singleton<CoverageManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[23].m_negativeColor);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[23].m_targetColor);
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[23].m_activeColor);
				uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[23].m_inactiveColor);
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
		}
	}

	private bool m_initialized;
}
