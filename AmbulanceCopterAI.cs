﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class AmbulanceCopterAI : HelicopterAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Health)
		{
			return base.GetColor(vehicleID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		uint patientCitizen = this.GetPatientCitizen(vehicleID, ref data);
		bool flag = false;
		if (patientCitizen != 0u)
		{
			flag = (Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation == Citizen.Location.Moving);
		}
		if (flag)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_RETURN");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_WAIT");
			}
			if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_CARRYING");
			}
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_RETURN");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_WAIT2");
			}
			if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_AMBULANCE_COPTER_EMERGENCY");
			}
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "AmbulanceCopter";
		current = (int)data.m_transferSize;
		max = this.m_travelCapacity;
		if ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && current == max)
		{
			current = max - 1;
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_patientCapacity + this.m_paramedicCount, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			uint patientCitizen = this.GetPatientCitizen(vehicleID, ref data);
			if (patientCitizen != 0u && Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation == Citizen.Location.Moving)
			{
				Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation = Citizen.Location.Home;
			}
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~(Vehicle.Flags.Landing | Vehicle.Flags.WaitingTarget);
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 0f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 0f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			data.m_flags &= ~Vehicle.Flags.Landing;
			data.m_flags |= Vehicle.Flags.Emergency2;
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else if ((int)data.m_transferSize >= this.m_travelCapacity || this.ShouldReturnToSource(vehicleID, ref data))
		{
			data.m_flags &= ~Vehicle.Flags.Landing;
			data.m_flags |= Vehicle.Flags.GoingBack;
		}
		else if (this.GetPatientCitizen(vehicleID, ref data) != 0u)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = 7;
			offer.Vehicle = vehicleID;
			offer.Position = data.GetLastFramePosition();
			offer.Amount = 1;
			offer.Active = true;
			Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.SickMove, offer);
			data.m_flags |= Vehicle.Flags.WaitingTarget;
		}
		else
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Priority = 7;
			offer2.Vehicle = vehicleID;
			offer2.Position = data.GetLastFramePosition();
			offer2.Amount = 1;
			offer2.Active = true;
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Sick2, offer2);
			data.m_flags |= Vehicle.Flags.WaitingTarget;
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	private uint GetPatientCitizen(ushort vehicleID, ref Vehicle data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick)
				{
					return citizen;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0u;
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Sick2)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				uint citizen = offer.Citizen;
				ushort buildingByLocation = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation();
				this.SetTarget(vehicleID, ref data, buildingByLocation);
				Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, vehicleID, 0u);
			}
		}
		else if (material == TransferManager.TransferReason.SickMove)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_patientCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 && this.CanLeave(vehicleID, ref vehicleData))
		{
			vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
			vehicleData.m_flags |= Vehicle.Flags.Leaving;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0 && this.GetPatientCitizen(vehicleID, ref data) == 0u)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Sick2, offer);
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.SickMove, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		uint patientCitizen = this.GetPatientCitizen(vehicleID, ref data);
		if (patientCitizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation != Citizen.Location.Moving)
			{
				ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].m_instance;
				if (instance2 != 0)
				{
					instance.ReleaseCitizenInstance(instance2);
				}
				instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation = Citizen.Location.Moving;
				for (int i = 0; i < this.m_paramedicCount; i++)
				{
					this.CreateParamedic(vehicleID, ref data, Citizen.AgePhase.Adult0);
				}
				data.m_flags |= Vehicle.Flags.Stopped;
			}
			else
			{
				instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].SetVehicle(patientCitizen, 0, 0u);
				instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].SetVisitplace(patientCitizen, data.m_targetBuilding, 0u);
				instance.m_citizens.m_buffer[(int)((UIntPtr)patientCitizen)].CurrentLocation = Citizen.Location.Visit;
				data.m_transferSize += 1;
			}
		}
		data.m_flags &= ~Vehicle.Flags.Emergency2;
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		bool result = true;
		bool flag = false;
		ushort num = 0;
		uint num2 = vehicleData.m_citizenUnits;
		int num3 = 0;
		while (num2 != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num2)].GetCitizen(i);
				if (citizen != 0u && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick)
				{
					ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance3 != 0)
					{
						CitizenInfo info = instance2.m_instances.m_buffer[(int)instance3].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service)
						{
							if ((instance2.m_instances.m_buffer[(int)instance3].m_flags & CitizenInstance.Flags.EnteringVehicle) == CitizenInstance.Flags.None)
							{
								if ((instance2.m_instances.m_buffer[(int)instance3].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
								{
									flag = true;
								}
								else if (num == 0)
								{
									num = instance3;
								}
								else
								{
									instance2.ReleaseCitizenInstance(instance3);
								}
							}
							result = false;
						}
					}
				}
			}
			num2 = nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!flag && num != 0)
		{
			CitizenInfo citizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Female, Citizen.SubCulture.Generic, Citizen.AgePhase.Adult1);
			if (citizenInfo != null)
			{
				instance2.m_instances.m_buffer[(int)num].Info = citizenInfo;
			}
			else
			{
				citizenInfo = instance2.m_instances.m_buffer[(int)num].Info;
			}
			citizenInfo.m_citizenAI.SetTarget(num, ref instance2.m_instances.m_buffer[(int)num], 0);
		}
		return result;
	}

	private void CreateParamedic(ushort vehicleID, ref Vehicle data, Citizen.AgePhase agePhase)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		CitizenInfo groupCitizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Female, Citizen.SubCulture.Generic, agePhase);
		if (groupCitizenInfo != null)
		{
			int family = instance.m_randomizer.Int32(256u);
			uint num = 0u;
			if (instance2.CreateCitizen(out num, 90, family, ref instance.m_randomizer, groupCitizenInfo.m_gender))
			{
				ushort num2;
				if (instance2.CreateCitizenInstance(out num2, ref instance.m_randomizer, groupCitizenInfo, num))
				{
					Vector3 randomDoorPosition = data.GetRandomDoorPosition(ref instance.m_randomizer, VehicleInfo.DoorType.Exit);
					groupCitizenInfo.m_citizenAI.SetCurrentVehicle(num2, ref instance2.m_instances.m_buffer[(int)num2], 0, 0u, randomDoorPosition);
					groupCitizenInfo.m_citizenAI.SetTarget(num2, ref instance2.m_instances.m_buffer[(int)num2], data.m_targetBuilding);
					instance2.m_citizens.m_buffer[(int)((UIntPtr)num)].SetVehicle(num, vehicleID, 0u);
				}
				else
				{
					instance2.ReleaseCitizen(num);
				}
			}
		}
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 0f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 targetPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out targetPos2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, 0f));
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 endPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public int m_paramedicCount = 2;

	[CustomizableProperty("Patient capacity")]
	public int m_patientCapacity = 2;

	public int m_travelCapacity = 10;
}
