﻿using System;
using System.Collections.Generic;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class EffectList : TriggerPanelList
{
	private void Awake()
	{
		this.m_container = base.Find<UIScrollablePanel>("ScrollablePanel");
		this.m_effectItems = new UITemplateList<EffectItem>(this.m_container, "EffectItem");
		this.m_effectItemIndices = new List<int>();
	}

	protected override void OnTriggerChanged()
	{
		if (base.currentTrigger.m_effects != null)
		{
			EffectItem[] array = this.m_effectItems.SetItemCount(base.currentTrigger.m_effects.Length);
			int num = 0;
			EffectItem[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				EffectItem effectItem = array2[i];
				effectItem.Init(this, false);
				effectItem.LoadEffect(base.currentTrigger.m_effects[num]);
				num++;
			}
			base.Find<UIPanel>("PanelAddItem").set_zOrder(this.m_container.get_childCount() - 1);
		}
		else
		{
			this.m_effectItems.SetItemCount(0);
		}
	}

	public void OnAddItemClicked()
	{
		this.AddItem();
	}

	public EffectItem AddItem()
	{
		EffectItem effectItem = this.m_effectItems.AddItem();
		effectItem.Init(this, true);
		base.Find<UIPanel>("PanelAddItem").set_zOrder(this.m_container.get_childCount() - 1);
		return effectItem;
	}

	public void DeleteItem(EffectItem item)
	{
		item.DeleteEffect();
		this.m_effectItems.RemoveItem(item);
	}

	public string[] GetAvailableOptions(EffectItem.EffectType originType)
	{
		this.m_effectItemIndices.Clear();
		List<string> list = new List<string>();
		EffectItem.EffectType[] array = (EffectItem.EffectType[])Enum.GetValues(typeof(EffectItem.EffectType));
		for (int i = 0; i < array.Length; i++)
		{
			EffectItem.EffectType effectType = (EffectItem.EffectType)i;
			if (this.CanAddOption(effectType, originType))
			{
				list.Add(this.AddOption(effectType, i));
			}
		}
		return list.ToArray();
	}

	private bool CanAddOption(EffectItem.EffectType effectType, EffectItem.EffectType originEffectType)
	{
		bool result;
		switch (effectType)
		{
		case EffectItem.EffectType.None:
			return false;
		case EffectItem.EffectType.Disaster:
			return SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		case EffectItem.EffectType.CashReward:
		case EffectItem.EffectType.CashFine:
			if (originEffectType == EffectItem.EffectType.CashFine || originEffectType == EffectItem.EffectType.CashReward)
			{
				return true;
			}
			foreach (EffectItem current in this.m_effectItems.items)
			{
				if (current.m_currentEffectType == EffectItem.EffectType.CashFine || current.m_currentEffectType == EffectItem.EffectType.CashReward)
				{
					result = false;
					return result;
				}
			}
			return true;
		case EffectItem.EffectType.StoryMessage:
			return originEffectType == EffectItem.EffectType.StoryMessage || !this.OptionOfTypeExists(EffectItem.EffectType.StoryMessage);
		case EffectItem.EffectType.Chirp:
			return originEffectType == EffectItem.EffectType.Chirp || !this.OptionOfTypeExists(EffectItem.EffectType.Chirp);
		case EffectItem.EffectType.Win:
		case EffectItem.EffectType.Lose:
			if (originEffectType == EffectItem.EffectType.Win || originEffectType == EffectItem.EffectType.Lose)
			{
				return true;
			}
			foreach (EffectItem current2 in this.m_effectItems.items)
			{
				if (current2.m_currentEffectType == EffectItem.EffectType.Lose || current2.m_currentEffectType == EffectItem.EffectType.Win)
				{
					result = false;
					return result;
				}
			}
			return true;
		default:
			return true;
		}
		return result;
	}

	private bool OptionOfTypeExists(EffectItem.EffectType effectType)
	{
		foreach (EffectItem current in this.m_effectItems.items)
		{
			if (current.m_currentEffectType == effectType)
			{
				return true;
			}
		}
		return false;
	}

	private string AddOption(EffectItem.EffectType effectType, int effectItemIndex)
	{
		this.m_effectItemIndices.Add(effectItemIndex);
		return Locale.Get("TRIGGEREFFECT", effectType.ToString());
	}

	public EffectItem.EffectType GetEffectTypeByDropdownIndex(int dropdownIndex)
	{
		return (EffectItem.EffectType)this.m_effectItemIndices[dropdownIndex];
	}

	private UITemplateList<EffectItem> m_effectItems;

	private UIScrollablePanel m_container;

	private List<int> m_effectItemIndices;
}
