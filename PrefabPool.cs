﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PrefabPool : MonoBehaviour
{
	public void Initialize(PrefabInfo prefabInfo)
	{
		this.m_prefabInfo = prefabInfo;
		this.m_buffer = new Dictionary<InstanceID, PrefabInfo>();
		this.m_freeInstances = new FastList<PrefabInfo>();
	}

	public void Release()
	{
		foreach (KeyValuePair<InstanceID, PrefabInfo> current in this.m_buffer)
		{
			current.Value.DestroyPrefabInstance();
			if (current.Value != null)
			{
				Object.Destroy(current.Value.get_gameObject());
			}
		}
		this.m_prefabInfo = null;
		this.m_buffer = null;
		this.m_freeInstances = null;
	}

	public T ObtainPrefabInstance<T>(InstanceID id, int maxCount) where T : PrefabInfo
	{
		PrefabInfo prefabInfo;
		if (this.m_buffer.TryGetValue(id, out prefabInfo))
		{
			prefabInfo.m_instanceNeeded = true;
			return (T)((object)prefabInfo);
		}
		if (this.m_freeInstances.m_size != 0)
		{
			int num = --this.m_freeInstances.m_size;
			PrefabInfo prefabInfo2 = this.m_freeInstances.m_buffer[num];
			this.m_freeInstances.m_buffer[num] = null;
			prefabInfo2.m_instanceID = id;
			prefabInfo2.m_instanceNeeded = true;
			prefabInfo2.m_instanceChanged = true;
			this.m_buffer.Add(id, prefabInfo2);
			prefabInfo2.get_gameObject().SetActive(true);
			return (T)((object)prefabInfo2);
		}
		if (this.m_buffer.Count < maxCount && PrefabPool.m_canCreateInstances > 0)
		{
			GameObject gameObject = Object.Instantiate<GameObject>(this.m_prefabInfo.get_gameObject());
			gameObject.get_transform().set_parent(base.get_transform());
			T component = gameObject.GetComponent<T>();
			component.InitializePrefabInstance(this.m_prefabInfo);
			component.m_instanceID = id;
			component.m_instanceNeeded = true;
			component.m_instanceChanged = true;
			component.m_instancePool = this;
			this.m_buffer.Add(id, component);
			PrefabPool.m_canCreateInstances--;
			return (T)((object)null);
		}
		return (T)((object)null);
	}

	public T GetPrefabInstance<T>(InstanceID id) where T : PrefabInfo
	{
		PrefabInfo prefabInfo;
		if (this.m_buffer.TryGetValue(id, out prefabInfo))
		{
			return (T)((object)prefabInfo);
		}
		return (T)((object)null);
	}

	public void ReleasePrefabInstance(PrefabInfo instance)
	{
		if (!instance.m_instanceID.IsEmpty)
		{
			instance.get_gameObject().SetActive(false);
			this.m_buffer.Remove(instance.m_instanceID);
			instance.m_instanceID = InstanceID.Empty;
			instance.m_instanceNeeded = false;
			this.m_freeInstances.Add(instance);
		}
	}

	public void TryDestroyFreeInstance()
	{
		if (this.m_freeInstances.m_size != 0 && PrefabPool.m_canCreateInstances > 0)
		{
			int num = --this.m_freeInstances.m_size;
			PrefabInfo prefabInfo = this.m_freeInstances.m_buffer[num];
			this.m_freeInstances.m_buffer[num] = null;
			prefabInfo.DestroyPrefabInstance();
			Object.Destroy(prefabInfo.get_gameObject());
			PrefabPool.m_canCreateInstances--;
		}
	}

	public PrefabInfo m_prefabInfo;

	public Dictionary<InstanceID, PrefabInfo> m_buffer;

	public FastList<PrefabInfo> m_freeInstances;

	public int m_targetCount;

	public static int m_canCreateInstances = 1;
}
