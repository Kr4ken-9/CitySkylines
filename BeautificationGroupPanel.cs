﻿using System;
using ColossalFramework;

public sealed class BeautificationGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Beautification;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (!base.isAssetEditor) ? GeneratedGroupPanel.GroupFilter.Building : ((GeneratedGroupPanel.GroupFilter)41);
		}
	}

	protected override bool IsCategoryRelevant(string category)
	{
		if (base.isAssetEditor)
		{
			if (category == "LandscapingPaths" || category == "LandscapingTrees" || category == "PropsRocks")
			{
				return true;
			}
		}
		else if (category == "BeautificationParksnPlazas" || category == "BeautificationParks" || category == "BeautificationPlazas" || category == "BeautificationOthers" || category == "BeautificationExpansion1" || category == "BeautificationExpansion2" || category == "MonumentModderPack")
		{
			return true;
		}
		return false;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (base.isAssetEditor)
		{
			if (name != null)
			{
				if (name == "LandscapingPaths")
				{
					return 0;
				}
				if (name == "LandscapingTrees")
				{
					return 1;
				}
				if (name == "LandscapingRocks")
				{
					return 2;
				}
			}
			return 2147483647;
		}
		if (name != null)
		{
			if (name == "BeautificationParks")
			{
				return 0;
			}
			if (name == "BeautificationPlazas")
			{
				return 1;
			}
			if (name == "BeautificationOthers")
			{
				return 2;
			}
			if (name == "BeautificationExpansion1")
			{
				return 3;
			}
			if (name == "BeautificationExpansion2")
			{
				return 4;
			}
			if (name == "MonumentModderPack")
			{
				return 5;
			}
		}
		return 2147483647;
	}

	private UnlockManager.Feature GetCategoryFeature(string name)
	{
		if (name != null)
		{
			if (name == "BeautificationExpansion1")
			{
				return UnlockManager.Feature.CommercialSpecialization;
			}
		}
		return UnlockManager.Feature.None;
	}

	protected override GeneratedGroupPanel.GroupInfo CreateGroupInfo(string name, PrefabInfo info)
	{
		if (base.isAssetEditor)
		{
			return new GeneratedGroupPanel.GroupInfo(name, this.GetCategoryOrder(name));
		}
		return new BeautificationGroupPanel.PTGroupInfo(name, this.GetCategoryOrder(name), info.GetService(), this.GetCategoryFeature(name));
	}

	public class PTGroupInfo : GeneratedGroupPanel.GroupInfo
	{
		public PTGroupInfo(string name, int order, ItemClass.Service service, UnlockManager.Feature feature) : base(name, order)
		{
			this.m_Service = service;
			this.m_Feature = feature;
		}

		public override string serviceName
		{
			get
			{
				return EnumExtensions.Name<ItemClass.Service>(this.m_Service);
			}
		}

		public override string unlockText
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return base.unlockText;
				}
				return GeneratedGroupPanel.GetUnlockText(this.m_Feature);
			}
		}

		public override bool isUnlocked
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return base.isUnlocked;
				}
				return ToolsModifierControl.IsUnlocked(this.m_Feature);
			}
		}

		private ItemClass.Service m_Service;

		private UnlockManager.Feature m_Feature;
	}
}
