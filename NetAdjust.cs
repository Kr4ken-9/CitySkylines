﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class NetAdjust : MonoBehaviour
{
	private void Awake()
	{
		this.m_originalSegments = new HashSet<ushort>();
		this.m_includedSegments = new HashSet<ushort>();
		this.m_startPath = new FastList<ushort>();
		this.m_endPath = new FastList<ushort>();
		this.m_tempPath = new FastList<ushort>();
		this.m_segmentQueue = new FastList<ushort>();
		this.m_segmentData = new Dictionary<ushort, NetAdjust.SegmentData>();
	}

	private void OnDestroy()
	{
		this.DestroyPath();
	}

	public void Initialize(Material material, Material material2)
	{
		if (material != null)
		{
			if (this.m_material != null)
			{
				Object.Destroy(this.m_material);
			}
			this.m_material = new Material(material);
		}
		if (material2 != null)
		{
			if (this.m_material2 != null)
			{
				Object.Destroy(this.m_material2);
			}
			this.m_material2 = new Material(material2);
		}
	}

	public void DestroyPath()
	{
		if (this.m_mesh != null)
		{
			for (int i = 0; i < this.m_mesh.Length; i++)
			{
				if (this.m_mesh[i] != null)
				{
					Object.Destroy(this.m_mesh[i]);
				}
			}
			this.m_mesh = null;
		}
		if (this.m_material != null)
		{
			Object.Destroy(this.m_material);
			this.m_material = null;
		}
		if (this.m_material2 != null)
		{
			Object.Destroy(this.m_material2);
			this.m_material2 = null;
		}
	}

	public bool PathVisible
	{
		get
		{
			return this.m_pathVisible;
		}
		set
		{
			this.m_pathVisible = value;
		}
	}

	public void RenderPath(RenderManager.CameraInfo cameraInfo, int layerMask)
	{
		if (!this.m_pathVisible)
		{
			return;
		}
		if (this.m_meshData != null)
		{
			this.UpdateMesh();
		}
		Material material = (!this.m_cachedIsSurfacePath) ? this.m_material : this.m_material2;
		if (this.m_mesh != null && material != null)
		{
			TransportManager instance = Singleton<TransportManager>.get_instance();
			TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
			material.SetFloat(instance.ID_StartOffset, -1000f);
			for (int i = 0; i < this.m_mesh.Length; i++)
			{
				if (this.m_cachedIsSurfacePath)
				{
					instance2.SetWaterMaterialProperties(this.m_mesh[i].get_bounds().get_center(), material);
				}
				if (material.SetPass(0))
				{
					NetManager expr_B3_cp_0 = Singleton<NetManager>.get_instance();
					expr_B3_cp_0.m_drawCallData.m_overlayCalls = expr_B3_cp_0.m_drawCallData.m_overlayCalls + 1;
					Graphics.DrawMeshNow(this.m_mesh[i], Matrix4x4.get_identity());
				}
			}
		}
	}

	public bool RenderOverlay(RenderManager.CameraInfo cameraInfo, ushort segment, Color color, int subIndex)
	{
		if (!this.m_pathVisible)
		{
			return false;
		}
		if (subIndex == 1)
		{
			Vector3 position = this.m_cachedStartAdjustmentPoint.m_position;
			ToolManager expr_2B_cp_0 = Singleton<ToolManager>.get_instance();
			expr_2B_cp_0.m_drawCallData.m_overlayCalls = expr_2B_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, 20f, position.y - 100f, position.y + 100f, false, true);
			return true;
		}
		if (subIndex == 2)
		{
			Vector3 position2 = this.m_cachedEndAdjustmentPoint.m_position;
			ToolManager expr_8B_cp_0 = Singleton<ToolManager>.get_instance();
			expr_8B_cp_0.m_drawCallData.m_overlayCalls = expr_8B_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position2, 20f, position2.y - 100f, position2.y + 100f, false, true);
			return true;
		}
		return subIndex == -1;
	}

	private void UpdateMesh()
	{
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		RenderGroup.MeshData[] meshData;
		try
		{
			meshData = this.m_meshData;
			this.m_meshData = null;
			this.m_cachedStartAdjustmentPoint = this.m_startAdjustmentPoint;
			this.m_cachedEndAdjustmentPoint = this.m_endAdjustmentPoint;
			this.m_cachedIsSurfacePath = this.m_isSurfacePath;
		}
		finally
		{
			Monitor.Exit(this);
		}
		if (meshData != null)
		{
			Mesh[] array = this.m_mesh;
			int num = 0;
			if (array != null)
			{
				num = array.Length;
			}
			if (num != meshData.Length)
			{
				Mesh[] array2 = new Mesh[meshData.Length];
				int num2 = Mathf.Min(num, array2.Length);
				for (int i = 0; i < num2; i++)
				{
					array2[i] = array[i];
				}
				for (int j = num2; j < array2.Length; j++)
				{
					array2[j] = new Mesh();
				}
				for (int k = num2; k < num; k++)
				{
					Object.Destroy(array[k]);
				}
				array = array2;
				this.m_mesh = array;
			}
			for (int l = 0; l < meshData.Length; l++)
			{
				array[l].Clear();
				array[l].set_vertices(meshData[l].m_vertices);
				array[l].set_normals(meshData[l].m_normals);
				array[l].set_tangents(meshData[l].m_tangents);
				array[l].set_uv(meshData[l].m_uvs);
				array[l].set_uv2(meshData[l].m_uvs2);
				array[l].set_colors32(meshData[l].m_colors);
				array[l].set_triangles(meshData[l].m_triangles);
				array[l].set_bounds(meshData[l].m_bounds);
			}
		}
	}

	public void SimulationStep(int subStep)
	{
		if (!this.m_pathVisible)
		{
			return;
		}
		InstanceID selectedInstance = Singleton<InstanceManager>.get_instance().GetSelectedInstance();
		if (selectedInstance != this.m_lastInstance || this.m_pathDirty)
		{
			this.CalculatePath(selectedInstance.NetSegment, 0);
			this.m_lastInstance = selectedInstance;
			this.m_pathDirty = false;
		}
	}

	public void SetHoverAdjustPoint(int index, ushort segment, ushort node)
	{
		if (!this.m_pathVisible)
		{
			return;
		}
		if (this.m_tempAdjustmentPoint.m_segment != segment || this.m_tempAdjustmentPoint.m_node != node || this.m_tempAdjustmentIndex != index)
		{
			this.m_tempAdjustmentPoint.m_segment = segment;
			this.m_tempAdjustmentPoint.m_node = node;
			this.m_tempAdjustmentIndex = index;
			this.CalculatePath(this.m_lastInstance.NetSegment, index);
		}
	}

	public void SegmentRemoved(ushort segment)
	{
		this.m_pathDirty = true;
	}

	public void ApplyModification(int index)
	{
		if (!this.m_pathVisible || this.m_tempAdjustmentIndex != index)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		if (this.m_originalSegments.Count == this.m_includedSegments.Count)
		{
			bool flag = true;
			foreach (ushort current in this.m_originalSegments)
			{
				if (!this.m_includedSegments.Contains(current))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				return;
			}
		}
		string name = null;
		ushort nameSeed = instance.m_segments.m_buffer[(int)this.m_lastInstance.NetSegment].m_nameSeed;
		if ((instance.m_segments.m_buffer[(int)this.m_lastInstance.NetSegment].m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
		{
			name = Singleton<InstanceManager>.get_instance().GetName(this.m_lastInstance);
		}
		foreach (ushort current2 in this.m_includedSegments)
		{
			instance.m_adjustedSegments[current2 >> 6] |= 1uL << (int)current2;
			if (!this.m_originalSegments.Contains(current2))
			{
				instance.SetSegmentNameImpl(current2, name);
				instance.m_segments.m_buffer[(int)current2].m_nameSeed = nameSeed;
			}
		}
		foreach (ushort current3 in this.m_originalSegments)
		{
			if (!this.m_includedSegments.Contains(current3))
			{
				instance.SetSegmentNameImpl(current3, null);
				instance.m_segments.m_buffer[(int)current3].m_nameSeed = 0;
				instance.m_adjustedSegments[current3 >> 6] &= ~(1uL << (int)current3);
			}
		}
		foreach (ushort current4 in this.m_includedSegments)
		{
			if (!this.m_originalSegments.Contains(current4))
			{
				instance.UpdateSegmentRenderer(current4, false);
			}
			instance.m_updateNameVisibility.Add(current4);
		}
		foreach (ushort current5 in this.m_originalSegments)
		{
			if (!this.m_includedSegments.Contains(current5))
			{
				instance.m_segments.m_buffer[(int)current5].UpdateNameSeed(current5);
				instance.UpdateSegmentRenderer(current5, false);
				instance.m_updateNameVisibility.Add(current5);
			}
		}
		this.CalculatePath(this.m_lastInstance.NetSegment, 0);
	}

	private void CalculatePath(ushort segment, int modifyIndex)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
		bool flag = false;
		if (info != null)
		{
			ItemClass.Service service = info.m_class.m_service;
			if (service != ItemClass.Service.Beautification)
			{
				if (service != ItemClass.Service.Electricity)
				{
					if (service == ItemClass.Service.PublicTransport)
					{
						flag = (info.m_class.m_subService == ItemClass.SubService.PublicTransportCableCar || info.m_class.m_subService == ItemClass.SubService.PublicTransportShip);
					}
				}
				else
				{
					flag = true;
				}
			}
			else
			{
				flag = (info.m_class.m_level == ItemClass.Level.Level5);
			}
		}
		TransportLine.TempUpdateMeshData[] array;
		if (flag)
		{
			array = new TransportLine.TempUpdateMeshData[81];
		}
		else
		{
			array = new TransportLine.TempUpdateMeshData[1];
		}
		ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
		this.m_tempPath.Clear();
		this.m_includedSegments.Clear();
		if (modifyIndex == 0)
		{
			this.m_originalSegments.Clear();
		}
		if (modifyIndex == 1)
		{
			for (int i = this.m_endPath.m_size - 2; i >= 0; i--)
			{
				ushort item = this.m_endPath.m_buffer[i];
				this.m_includedSegments.Add(item);
			}
			this.CalculatePath(segment, startNode, this.m_tempAdjustmentPoint.m_segment, this.m_tempAdjustmentPoint.m_node, this.m_startPath);
			if (this.m_startPath.m_size == 0)
			{
				this.CalculatePath(segment, startNode, 0, 0, this.m_startPath);
			}
		}
		else if (modifyIndex == 0)
		{
			this.CalculatePath(segment, startNode, 0, 0, this.m_startPath);
		}
		for (int j = 0; j < this.m_startPath.m_size - 1; j++)
		{
			ushort item2 = this.m_startPath.m_buffer[j];
			this.m_tempPath.Add(item2);
			this.m_includedSegments.Add(item2);
			if (modifyIndex == 0)
			{
				this.m_originalSegments.Add(item2);
			}
		}
		this.m_tempPath.Add(segment);
		this.m_includedSegments.Add(segment);
		if (modifyIndex == 0)
		{
			this.m_originalSegments.Add(segment);
		}
		if (modifyIndex == 2)
		{
			this.CalculatePath(segment, endNode, this.m_tempAdjustmentPoint.m_segment, this.m_tempAdjustmentPoint.m_node, this.m_endPath);
			if (this.m_endPath.m_size == 0)
			{
				this.CalculatePath(segment, endNode, 0, 0, this.m_endPath);
			}
		}
		else if (modifyIndex == 0)
		{
			this.CalculatePath(segment, endNode, 0, 0, this.m_endPath);
		}
		for (int k = this.m_endPath.m_size - 2; k >= 0; k--)
		{
			ushort item3 = this.m_endPath.m_buffer[k];
			this.m_tempPath.Add(item3);
			this.m_includedSegments.Add(item3);
			if (modifyIndex == 0)
			{
				this.m_originalSegments.Add(item3);
			}
		}
		for (int l = 0; l < this.m_tempPath.m_size; l++)
		{
			ushort num = this.m_tempPath.m_buffer[l];
			startNode = instance.m_segments.m_buffer[(int)num].m_startNode;
			endNode = instance.m_segments.m_buffer[(int)num].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			int num2 = 0;
			if (flag)
			{
				num2 = instance2.GetPatchIndex((position + position2) * 0.5f);
			}
			TransportLine.TempUpdateMeshData[] expr_3B9_cp_0 = array;
			int expr_3B9_cp_1 = num2;
			expr_3B9_cp_0[expr_3B9_cp_1].m_pathSegmentCount = expr_3B9_cp_0[expr_3B9_cp_1].m_pathSegmentCount + 1;
			ushort num3 = endNode;
			if (l < this.m_tempPath.m_size - 1)
			{
				ushort otherSegmentID = this.m_tempPath.m_buffer[l + 1];
				ushort sharedNode = instance.m_segments.m_buffer[(int)num].GetSharedNode(otherSegmentID);
				if (sharedNode != 0)
				{
					num3 = sharedNode;
				}
				Vector3 position3 = instance.m_nodes.m_buffer[(int)num3].m_position;
				if ((instance.m_nodes.m_buffer[(int)num3].m_flags & NetNode.Flags.Middle) == NetNode.Flags.None)
				{
					if (flag)
					{
						num2 = instance2.GetPatchIndex(position3);
					}
					TransportLine.TempUpdateMeshData[] expr_467_cp_0 = array;
					int expr_467_cp_1 = num2;
					expr_467_cp_0[expr_467_cp_1].m_pathSegmentCount = expr_467_cp_0[expr_467_cp_1].m_pathSegmentCount + 1;
				}
			}
			else
			{
				if (l != 0)
				{
					ushort otherSegmentID2 = this.m_tempPath.m_buffer[l - 1];
					ushort sharedNode2 = instance.m_segments.m_buffer[(int)num].GetSharedNode(otherSegmentID2);
					if (sharedNode2 != 0)
					{
						num3 = instance.m_segments.m_buffer[(int)num].GetOtherNode(sharedNode2);
					}
				}
				Vector3 position4 = instance.m_nodes.m_buffer[(int)num3].m_position;
				if (flag)
				{
					num2 = instance2.GetPatchIndex(position4);
				}
				TransportLine.TempUpdateMeshData[] expr_501_cp_0 = array;
				int expr_501_cp_1 = num2;
				expr_501_cp_0[expr_501_cp_1].m_pathSegmentCount = expr_501_cp_0[expr_501_cp_1].m_pathSegmentCount + 1;
			}
			if (l == 0)
			{
				ushort otherNode = instance.m_segments.m_buffer[(int)num].GetOtherNode(num3);
				Vector3 position5 = instance.m_nodes.m_buffer[(int)otherNode].m_position;
				if (flag)
				{
					num2 = instance2.GetPatchIndex(position5);
				}
				TransportLine.TempUpdateMeshData[] expr_562_cp_0 = array;
				int expr_562_cp_1 = num2;
				expr_562_cp_0[expr_562_cp_1].m_pathSegmentCount = expr_562_cp_0[expr_562_cp_1].m_pathSegmentCount + 1;
			}
			if (num == segment)
			{
				if (flag)
				{
					num2 = instance2.GetPatchIndex((position + position2) * 0.5f);
				}
				TransportLine.TempUpdateMeshData[] expr_5A1_cp_0 = array;
				int expr_5A1_cp_1 = num2;
				expr_5A1_cp_0[expr_5A1_cp_1].m_pathSegmentCount = expr_5A1_cp_0[expr_5A1_cp_1].m_pathSegmentCount + 1;
			}
		}
		int num4 = 0;
		for (int m = 0; m < array.Length; m++)
		{
			int pathSegmentCount = array[m].m_pathSegmentCount;
			if (pathSegmentCount != 0)
			{
				array[m].m_meshData = new RenderGroup.MeshData
				{
					m_vertices = new Vector3[pathSegmentCount * 8],
					m_normals = new Vector3[pathSegmentCount * 8],
					m_tangents = new Vector4[pathSegmentCount * 8],
					m_uvs = new Vector2[pathSegmentCount * 8],
					m_uvs2 = new Vector2[pathSegmentCount * 8],
					m_colors = new Color32[pathSegmentCount * 8],
					m_triangles = new int[pathSegmentCount * 30]
				};
				num4++;
			}
		}
		if (num4 != 0)
		{
			NetAdjust.AdjustmentPoint startAdjustmentPoint = default(NetAdjust.AdjustmentPoint);
			NetAdjust.AdjustmentPoint endAdjustmentPoint = default(NetAdjust.AdjustmentPoint);
			for (int n = 0; n < this.m_tempPath.m_size; n++)
			{
				ushort num5 = this.m_tempPath.m_buffer[n];
				startNode = instance.m_segments.m_buffer[(int)num5].m_startNode;
				endNode = instance.m_segments.m_buffer[(int)num5].m_endNode;
				Vector3 position6 = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position7 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				Vector3 startDirection = instance.m_segments.m_buffer[(int)num5].m_startDirection;
				Vector3 endDirection = instance.m_segments.m_buffer[(int)num5].m_endDirection;
				Bezier3 bezier = default(Bezier3);
				bezier.a = position6;
				bezier.d = position7;
				NetSegment.CalculateMiddlePoints(bezier.a, startDirection, bezier.d, endDirection, true, true, out bezier.b, out bezier.c);
				float endOffset = Vector3.Distance(bezier.a, bezier.b) + Vector3.Distance(bezier.b, bezier.c) + Vector3.Distance(bezier.c, bezier.d);
				int num6 = 0;
				if (flag)
				{
					num6 = instance2.GetPatchIndex((position6 + position7) * 0.5f);
				}
				Bezier3 arg_83D_0 = bezier;
				RenderGroup.MeshData arg_83D_1 = array[num6].m_meshData;
				Bezier3[] arg_83D_2 = null;
				TransportLine.TempUpdateMeshData[] expr_808_cp_0 = array;
				int expr_808_cp_1 = num6;
				int pathSegmentIndex;
				expr_808_cp_0[expr_808_cp_1].m_pathSegmentIndex = (pathSegmentIndex = expr_808_cp_0[expr_808_cp_1].m_pathSegmentIndex) + 1;
				TransportLine.FillPathSegment(arg_83D_0, arg_83D_1, arg_83D_2, pathSegmentIndex, 0, 0f, endOffset, 4f, (!flag) ? 5f : 20f, flag);
				ushort num7 = endNode;
				if (n < this.m_tempPath.m_size - 1)
				{
					ushort otherSegmentID3 = this.m_tempPath.m_buffer[n + 1];
					ushort sharedNode3 = instance.m_segments.m_buffer[(int)num5].GetSharedNode(otherSegmentID3);
					if (sharedNode3 != 0)
					{
						num7 = sharedNode3;
					}
					Vector3 position8 = instance.m_nodes.m_buffer[(int)num7].m_position;
					if ((instance.m_nodes.m_buffer[(int)num7].m_flags & NetNode.Flags.Middle) == NetNode.Flags.None)
					{
						if (flag)
						{
							num6 = instance2.GetPatchIndex(position8);
						}
						Vector3 arg_920_0 = position8;
						RenderGroup.MeshData arg_920_1 = array[num6].m_meshData;
						TransportLine.TempUpdateMeshData[] expr_8F3_cp_0 = array;
						int expr_8F3_cp_1 = num6;
						expr_8F3_cp_0[expr_8F3_cp_1].m_pathSegmentIndex = (pathSegmentIndex = expr_8F3_cp_0[expr_8F3_cp_1].m_pathSegmentIndex) + 1;
						TransportLine.FillPathNode(arg_920_0, arg_920_1, pathSegmentIndex, 4f, (!flag) ? 5f : 20f, flag);
					}
				}
				else
				{
					if (n != 0)
					{
						ushort otherSegmentID4 = this.m_tempPath.m_buffer[n - 1];
						ushort sharedNode4 = instance.m_segments.m_buffer[(int)num5].GetSharedNode(otherSegmentID4);
						if (sharedNode4 != 0)
						{
							num7 = instance.m_segments.m_buffer[(int)num5].GetOtherNode(sharedNode4);
						}
					}
					Vector3 position9 = instance.m_nodes.m_buffer[(int)num7].m_position;
					if (flag)
					{
						num6 = instance2.GetPatchIndex(position9);
					}
					Vector3 arg_9EF_0 = position9;
					RenderGroup.MeshData arg_9EF_1 = array[num6].m_meshData;
					TransportLine.TempUpdateMeshData[] expr_9C2_cp_0 = array;
					int expr_9C2_cp_1 = num6;
					expr_9C2_cp_0[expr_9C2_cp_1].m_pathSegmentIndex = (pathSegmentIndex = expr_9C2_cp_0[expr_9C2_cp_1].m_pathSegmentIndex) + 1;
					TransportLine.FillPathNode(arg_9EF_0, arg_9EF_1, pathSegmentIndex, 10f, (!flag) ? 5f : 20f, flag);
					endAdjustmentPoint = new NetAdjust.AdjustmentPoint
					{
						m_position = position9,
						m_segment = num5,
						m_node = num7
					};
				}
				if (n == 0)
				{
					ushort otherNode2 = instance.m_segments.m_buffer[(int)num5].GetOtherNode(num7);
					Vector3 position10 = instance.m_nodes.m_buffer[(int)otherNode2].m_position;
					if (flag)
					{
						num6 = instance2.GetPatchIndex(position10);
					}
					Vector3 arg_AAC_0 = position10;
					RenderGroup.MeshData arg_AAC_1 = array[num6].m_meshData;
					TransportLine.TempUpdateMeshData[] expr_A7F_cp_0 = array;
					int expr_A7F_cp_1 = num6;
					expr_A7F_cp_0[expr_A7F_cp_1].m_pathSegmentIndex = (pathSegmentIndex = expr_A7F_cp_0[expr_A7F_cp_1].m_pathSegmentIndex) + 1;
					TransportLine.FillPathNode(arg_AAC_0, arg_AAC_1, pathSegmentIndex, 10f, (!flag) ? 5f : 20f, flag);
					startAdjustmentPoint = new NetAdjust.AdjustmentPoint
					{
						m_position = position10,
						m_segment = num5,
						m_node = otherNode2
					};
				}
				if (num5 == segment)
				{
					Vector3 vector = bezier.Position(0.5f);
					if (flag)
					{
						num6 = instance2.GetPatchIndex((position6 + position7) * 0.5f);
					}
					Vector3 arg_B55_0 = vector;
					RenderGroup.MeshData arg_B55_1 = array[num6].m_meshData;
					TransportLine.TempUpdateMeshData[] expr_B28_cp_0 = array;
					int expr_B28_cp_1 = num6;
					expr_B28_cp_0[expr_B28_cp_1].m_pathSegmentIndex = (pathSegmentIndex = expr_B28_cp_0[expr_B28_cp_1].m_pathSegmentIndex) + 1;
					TransportLine.FillPathNode(arg_B55_0, arg_B55_1, pathSegmentIndex, 7f, (!flag) ? 5f : 20f, flag);
				}
			}
			RenderGroup.MeshData[] array2 = new RenderGroup.MeshData[num4];
			int num8 = 0;
			for (int num9 = 0; num9 < array.Length; num9++)
			{
				if (array[num9].m_meshData != null)
				{
					array[num9].m_meshData.UpdateBounds();
					if (flag)
					{
						Vector3 min = array[num9].m_meshData.m_bounds.get_min();
						Vector3 max = array[num9].m_meshData.m_bounds.get_max();
						max.y += 1024f;
						array[num9].m_meshData.m_bounds.SetMinMax(min, max);
					}
					array2[num8++] = array[num9].m_meshData;
				}
			}
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_meshData = array2;
				this.m_startAdjustmentPoint = startAdjustmentPoint;
				this.m_endAdjustmentPoint = endAdjustmentPoint;
				this.m_isSurfacePath = flag;
			}
			finally
			{
				Monitor.Exit(this);
			}
		}
		else
		{
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_meshData = new RenderGroup.MeshData[0];
				this.m_startAdjustmentPoint = default(NetAdjust.AdjustmentPoint);
				this.m_endAdjustmentPoint = default(NetAdjust.AdjustmentPoint);
				this.m_isSurfacePath = false;
			}
			finally
			{
				Monitor.Exit(this);
			}
		}
	}

	private void CalculatePath(ushort srcSegment, ushort srcNode, ushort dstSegment, ushort dstNode, FastList<ushort> result)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = dstSegment != 0 || dstNode != 0;
		ushort num = 0;
		this.m_firstSegment = 0;
		this.m_segmentQueue.Clear();
		this.m_segmentData.Clear();
		if (srcSegment != 0)
		{
			this.AddSegment(srcSegment, 0, 1u);
		}
		while (this.m_firstSegment < this.m_segmentQueue.m_size)
		{
			ushort num2 = this.m_segmentQueue.m_buffer[this.m_firstSegment++];
			NetAdjust.SegmentData segmentData = this.m_segmentData[num2];
			ushort num3;
			if (num2 == srcSegment)
			{
				num3 = srcNode;
			}
			else
			{
				ushort sharedNode = instance.m_segments.m_buffer[(int)num2].GetSharedNode(segmentData.m_source);
				num3 = instance.m_segments.m_buffer[(int)num2].GetOtherNode(sharedNode);
			}
			if (flag && (num3 == dstNode || num2 == dstSegment))
			{
				num = num2;
				break;
			}
			NetInfo info = instance.m_segments.m_buffer[(int)num2].Info;
			Vector3 direction = instance.m_segments.m_buffer[(int)num2].GetDirection(num3);
			uint num4 = 4294967295u;
			ushort num5 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num3].GetSegment(i);
				if (segment != 0 && segment != num2 && !this.m_includedSegments.Contains(segment) && (flag || instance.IsSameName(num2, segment)))
				{
					NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
					if (info.m_class.m_service == info2.m_class.m_service)
					{
						if (info.m_class.m_subService == info2.m_class.m_subService)
						{
							uint num6 = segmentData.m_cost + 1u;
							if ((instance.m_nodes.m_buffer[(int)num3].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
							{
								Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(num3);
								if (direction.x * direction2.x + direction.z * direction2.z > -0.93f)
								{
									num6 += 36864u;
								}
								if (info.m_halfWidth != info2.m_halfWidth)
								{
									num6 += 1u;
								}
							}
							if (flag)
							{
								this.AddSegment(segment, num2, num6);
							}
							else if (num6 < num4)
							{
								num4 = num6;
								num5 = segment;
							}
							else if (num6 == num4)
							{
								num5 = 0;
							}
						}
					}
				}
			}
			if (num5 != 0)
			{
				this.AddSegment(num5, num2, num4);
			}
		}
		if (!flag && this.m_segmentQueue.m_size != 0)
		{
			num = this.m_segmentQueue.m_buffer[this.m_segmentQueue.m_size - 1];
		}
		result.Clear();
		int num7 = 0;
		while (num != 0)
		{
			result.Add(num);
			num = this.m_segmentData[num].m_source;
			if (++num7 > 36864)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private bool AddSegment(ushort segmentID, ushort source, uint cost)
	{
		NetAdjust.SegmentData segmentData;
		if (!this.m_segmentData.TryGetValue(segmentID, out segmentData))
		{
			this.m_segmentData.Add(segmentID, new NetAdjust.SegmentData
			{
				m_cost = cost,
				m_source = source
			});
			if (this.m_firstSegment > 0 && this.m_firstSegment < this.m_segmentQueue.m_size)
			{
				ushort key = this.m_segmentQueue.m_buffer[this.m_firstSegment];
				if (this.m_segmentData[key].m_cost > cost)
				{
					this.m_segmentQueue.m_buffer[--this.m_firstSegment] = segmentID;
					return true;
				}
			}
			int i = this.m_segmentQueue.m_size;
			this.m_segmentQueue.Add(segmentID);
			while (i > this.m_firstSegment)
			{
				ushort num = this.m_segmentQueue.m_buffer[i - 1];
				if (this.m_segmentData[num].m_cost <= cost)
				{
					break;
				}
				this.m_segmentQueue.m_buffer[i--] = num;
			}
			if (i < this.m_segmentQueue.m_size - 1)
			{
				this.m_segmentQueue.m_buffer[i] = segmentID;
			}
			return true;
		}
		if (cost < segmentData.m_cost)
		{
			this.m_segmentData[segmentID] = new NetAdjust.SegmentData
			{
				m_cost = cost,
				m_source = source
			};
			int j;
			for (j = this.m_segmentQueue.m_size - 1; j >= this.m_firstSegment; j--)
			{
				if (this.m_segmentQueue.m_buffer[j] == segmentID)
				{
					break;
				}
			}
			while (j > this.m_firstSegment)
			{
				ushort num2 = this.m_segmentQueue.m_buffer[j - 1];
				if (this.m_segmentData[num2].m_cost <= cost)
				{
					break;
				}
				this.m_segmentQueue.m_buffer[j--] = num2;
			}
			if (j >= this.m_firstSegment)
			{
				this.m_segmentQueue.m_buffer[j] = segmentID;
			}
			return true;
		}
		return false;
	}

	public int CheckHoverSegment(ref ushort segment, Vector3 hoverPosition)
	{
		if (!this.m_pathVisible)
		{
			return 0;
		}
		int num = 0;
		float num2 = 20f;
		if (this.m_startAdjustmentPoint.m_segment != 0 || this.m_startAdjustmentPoint.m_node != 0)
		{
			float num3 = Vector3.Distance(this.m_startAdjustmentPoint.m_position, hoverPosition);
			if (num3 < num2)
			{
				num = 1;
				num2 = num3;
				segment = this.m_startAdjustmentPoint.m_segment;
			}
		}
		if (this.m_endAdjustmentPoint.m_segment != 0 || this.m_endAdjustmentPoint.m_node != 0)
		{
			float num4 = Vector3.Distance(this.m_endAdjustmentPoint.m_position, hoverPosition);
			if (num4 < num2)
			{
				num = 2;
				segment = this.m_endAdjustmentPoint.m_segment;
			}
		}
		if (num != 0)
		{
			return num;
		}
		if (this.m_includedSegments.Contains(segment))
		{
			return -1;
		}
		return 0;
	}

	public void UpdateData()
	{
		this.m_tempAdjustmentIndex = 0;
	}

	private InstanceID m_lastInstance;

	private bool m_pathVisible;

	private bool m_pathDirty;

	private int m_firstSegment;

	private Material m_material;

	private Material m_material2;

	private RenderGroup.MeshData[] m_meshData;

	private Mesh[] m_mesh;

	private NetAdjust.AdjustmentPoint m_startAdjustmentPoint;

	private NetAdjust.AdjustmentPoint m_endAdjustmentPoint;

	private NetAdjust.AdjustmentPoint m_cachedStartAdjustmentPoint;

	private NetAdjust.AdjustmentPoint m_cachedEndAdjustmentPoint;

	private NetAdjust.AdjustmentPoint m_tempAdjustmentPoint;

	private int m_tempAdjustmentIndex;

	private HashSet<ushort> m_originalSegments;

	private HashSet<ushort> m_includedSegments;

	private FastList<ushort> m_startPath;

	private FastList<ushort> m_endPath;

	private FastList<ushort> m_tempPath;

	private FastList<ushort> m_segmentQueue;

	private Dictionary<ushort, NetAdjust.SegmentData> m_segmentData;

	private bool m_isSurfacePath;

	private bool m_cachedIsSurfacePath;

	public struct AdjustmentPoint
	{
		public Vector3 m_position;

		public ushort m_segment;

		public ushort m_node;
	}

	public struct SegmentData
	{
		public uint m_cost;

		public ushort m_source;
	}
}
