﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterWizard : ToolsModifierControl
{
	public AssetImporterAssetType assetTypePanel
	{
		get
		{
			return this.m_AssetTypePanel;
		}
	}

	public AssetImporterAssetTemplate assetTemplatePanel
	{
		get
		{
			return this.m_AssetTemplatePanel;
		}
	}

	public AssetImporterAssetImport assetImportPanel
	{
		get
		{
			return this.m_AssetImportPanel;
		}
	}

	private void Awake()
	{
		this.m_WizardContainer = base.Find<UITabContainer>("WizardContainer");
	}

	public void Complete()
	{
		base.CloseEverything();
		this.Reset();
		UIView.get_library().Hide("AssetImporterPanel", 1);
	}

	public void GoTo(AssetImporterPanelBase panel)
	{
		this.m_LastActiveIndexes.Add(this.m_WizardContainer.get_selectedIndex());
		this.m_WizardContainer.set_selectedIndex(panel.index);
	}

	public void GoBack()
	{
		this.m_WizardContainer.set_selectedIndex(this.m_LastActiveIndexes[this.m_LastActiveIndexes.Count - 1]);
		this.m_LastActiveIndexes.RemoveAt(this.m_LastActiveIndexes.Count - 1);
	}

	public void Reset()
	{
		this.m_LastActiveIndexes.Clear();
		this.assetImportPanel.Reset();
		this.assetTemplatePanel.Reset();
		this.assetTypePanel.Reset();
		this.GoTo(this.assetTypePanel);
	}

	private void Start()
	{
		this.assetImportPanel.owner = this;
		this.assetTemplatePanel.owner = this;
		this.assetTypePanel.owner = this;
		this.GoTo(this.assetTypePanel);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			base.Find<UIButton>("Close").set_tooltipLocaleID((!this.willClose) ? string.Empty : "ASSETIMPORTER_BACKTOMAINMENU");
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			if (this.assetImportPanel.IsLargePreviewOpen)
			{
				this.assetImportPanel.OnCloseLargePreview();
			}
			else
			{
				this.OnClose();
				p.Use();
			}
		}
	}

	private bool willClose
	{
		get
		{
			return ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo == null;
		}
	}

	public void OnClose()
	{
		UIView.get_library().Hide("AssetImporterPanel", -1);
		if (this.willClose)
		{
			this.BackToMainMenu();
		}
	}

	private void BackToMainMenu()
	{
		Singleton<LoadingManager>.get_instance().UnloadLevel();
	}

	[SerializeField]
	protected AssetImporterAssetType m_AssetTypePanel;

	[SerializeField]
	protected AssetImporterAssetTemplate m_AssetTemplatePanel;

	[SerializeField]
	protected AssetImporterAssetImport m_AssetImportPanel;

	private List<int> m_LastActiveIndexes = new List<int>();

	private UITabContainer m_WizardContainer;
}
