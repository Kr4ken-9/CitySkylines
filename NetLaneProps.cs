﻿using System;
using UnityEngine;

[Serializable]
public class NetLaneProps : ScriptableObject
{
	[CustomizableProperty("Props")]
	public NetLaneProps.Prop[] m_props;

	public enum ColorMode
	{
		Default,
		StartState,
		EndState
	}

	[Serializable]
	public class Prop
	{
		public bool CheckFlags(NetLane.Flags laneFlags, NetNode.Flags startFlags, NetNode.Flags endFlags)
		{
			return ((this.m_flagsRequired | this.m_flagsForbidden) & laneFlags) == this.m_flagsRequired && ((this.m_startFlagsRequired | this.m_startFlagsForbidden) & startFlags) == this.m_startFlagsRequired && ((this.m_endFlagsRequired | this.m_endFlagsForbidden) & endFlags) == this.m_endFlagsRequired;
		}

		[BitMask, CustomizableProperty("Flags Required")]
		public NetLane.Flags m_flagsRequired;

		[BitMask, CustomizableProperty("Flags Forbidden")]
		public NetLane.Flags m_flagsForbidden;

		[BitMask, CustomizableProperty("Start Flags Required")]
		public NetNode.Flags m_startFlagsRequired;

		[BitMask, CustomizableProperty("Start Flags Forbidden")]
		public NetNode.Flags m_startFlagsForbidden;

		[BitMask, CustomizableProperty("End Flags Required")]
		public NetNode.Flags m_endFlagsRequired;

		[BitMask, CustomizableProperty("End Flags Forbidden")]
		public NetNode.Flags m_endFlagsForbidden;

		[CustomizableProperty("Color Mode")]
		public NetLaneProps.ColorMode m_colorMode;

		public PropInfo m_prop;

		public TreeInfo m_tree;

		[CustomizableProperty("Position")]
		public Vector3 m_position;

		[CustomizableProperty("Angle")]
		public float m_angle;

		[CustomizableProperty("Segment Offset")]
		public float m_segmentOffset;

		[CustomizableProperty("Repeat Distance")]
		public float m_repeatDistance;

		[CustomizableProperty("Min Length")]
		public float m_minLength;

		[CustomizableProperty("Corner Angle")]
		public float m_cornerAngle;

		[CustomizableProperty("Probability")]
		public int m_probability = 100;

		[NonSerialized]
		public PropInfo m_finalProp;

		[NonSerialized]
		public TreeInfo m_finalTree;
	}
}
