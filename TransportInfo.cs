﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

[Serializable]
public class TransportInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_lineMaterial != null)
		{
			this.m_lineMaterial2 = new Material(this.m_lineMaterial);
			this.m_requireSurfaceLine = (this.m_lineMaterial.GetTag("LineType", false) == "Surface");
		}
		if (this.m_secondaryLineMaterial != null)
		{
			this.m_secondaryLineMaterial2 = new Material(this.m_secondaryLineMaterial);
			if (this.m_secondaryLineMaterial.GetTag("LineType", false) == "Underground")
			{
				this.m_secondaryLayer = Singleton<TransportManager>.get_instance().m_metroLayer;
			}
			else
			{
				this.m_secondaryLayer = this.m_prefabDataLayer;
			}
		}
		else
		{
			this.m_secondaryLayer = -1;
		}
		if (this.m_pathMaterial != null)
		{
			this.m_pathMaterial2 = new Material(this.m_pathMaterial);
		}
		if (this.m_connectionMaterial != null)
		{
			this.m_connectionMaterial2 = new Material(this.m_connectionMaterial);
		}
		if (this.m_completedPassMilestone != null)
		{
			this.m_completedPassMilestone.SetPrefab(this);
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_lineMaterial2 != null)
		{
			Object.Destroy(this.m_lineMaterial2);
			this.m_lineMaterial2 = null;
		}
		if (this.m_secondaryLineMaterial2 != null)
		{
			Object.Destroy(this.m_secondaryLineMaterial2);
			this.m_secondaryLineMaterial2 = null;
		}
		if (this.m_pathMaterial2 != null)
		{
			Object.Destroy(this.m_pathMaterial2);
			this.m_pathMaterial2 = null;
		}
		if (this.m_connectionMaterial2 != null)
		{
			Object.Destroy(this.m_connectionMaterial2);
			this.m_connectionMaterial2 = null;
		}
		base.DestroyPrefab();
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("TRANSPORT_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("TRANSPORT_TITLE", base.get_gameObject().get_name());
	}

	public override string GetUncheckedLocalizedTitle()
	{
		if (Locale.Exists("TRANSPORT_TITLE", base.get_gameObject().get_name()))
		{
			return Locale.Get("TRANSPORT_TITLE", base.get_gameObject().get_name());
		}
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("TRANSPORT_DESC", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescriptionShort()
	{
		return Locale.Get("TRANSPORT_DESC", base.get_gameObject().get_name());
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public override MilestoneInfo GetUnlockMilestone()
	{
		return this.m_UnlockMilestone;
	}

	public const int TRANSPORT_TYPE_COUNT = 10;

	public ItemClass m_class;

	public MilestoneInfo m_UnlockMilestone;

	public ManualMilestone m_completedPassMilestone;

	public ItemClass.Placement m_placementStyle;

	public InfoManager.InfoMode m_infoMode;

	public InfoManager.SubInfoMode m_subInfoMode;

	public NetInfo m_netInfo;

	public Material m_lineMaterial;

	public Material m_secondaryLineMaterial;

	public Material m_pathMaterial;

	public Material m_connectionMaterial;

	public ItemClass.Availability m_pathVisibility = ItemClass.Availability.MapEditor;

	public float m_pathHalfWidth = 12f;

	public float m_pathTiling = 48f;

	public bool m_usePathNodes;

	public float m_defaultVehicleDistance = 1000f;

	public int m_maintenanceCostPerVehicle = 50;

	public ItemClass.Service m_stationService;

	public ItemClass.SubService m_stationSubService;

	public ItemClass.Layer m_stationLayer;

	public ItemClass.Service m_netService;

	public ItemClass.SubService m_netSubService;

	public ItemClass.Service m_secondaryNetService;

	public ItemClass.SubService m_secondaryNetSubService;

	public ItemClass.Layer m_netLayer;

	public TransportInfo.TransportType m_transportType;

	public VehicleInfo.VehicleType m_vehicleType = VehicleInfo.VehicleType.Car;

	public TransferManager.TransferReason m_vehicleReason = TransferManager.TransferReason.None;

	public NetLane.Flags m_stopFlag = NetLane.Flags.Stop;

	public bool m_avoidSameStopPlatform = true;

	[NonSerialized]
	public Material m_lineMaterial2;

	[NonSerialized]
	public Material m_secondaryLineMaterial2;

	[NonSerialized]
	public Material m_lineMaterial3;

	[NonSerialized]
	public Material m_pathMaterial2;

	[NonSerialized]
	public Material m_connectionMaterial2;

	[NonSerialized]
	public bool m_requireSurfaceLine;

	[NonSerialized]
	public int m_secondaryLayer;

	public enum TransportType
	{
		[EnumPosition(0)]
		Bus,
		[EnumPosition(1)]
		Metro,
		[EnumPosition(2)]
		Train,
		[EnumPosition(3)]
		Ship,
		[EnumPosition(4)]
		Airplane,
		[EnumPosition(5)]
		Taxi,
		[EnumPosition(6)]
		Tram,
		EvacuationBus,
		[EnumPosition(7)]
		Monorail,
		[EnumPosition(8)]
		CableCar
	}
}
