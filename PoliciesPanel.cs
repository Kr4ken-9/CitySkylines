﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class PoliciesPanel : ToolsModifierControl
{
	private UIComponent defaultTooltip
	{
		get
		{
			if (this.m_DefaultTooltip == null)
			{
				this.m_DefaultTooltip = UIView.Find("DefaultTooltip");
			}
			return this.m_DefaultTooltip;
		}
	}

	private UIComponent policiesTooltip
	{
		get
		{
			if (this.m_PoliciesTooltip == null)
			{
				this.m_PoliciesTooltip = UIView.Find("PoliciesTooltip");
			}
			return this.m_PoliciesTooltip;
		}
	}

	public byte targetDistrict
	{
		get
		{
			return this.m_District;
		}
	}

	private void Awake()
	{
		this.m_CloseButton = base.Find<UIButton>("Close");
		this.m_Tabstrip = base.Find<UITabstrip>("Tabstrip");
		this.m_Caption = base.Find<UILabel>("Caption");
		base.get_component().Hide();
	}

	private void Start()
	{
		this.m_SavePosition = base.closeToolbarButton.get_absolutePosition();
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded += new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded += new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		}
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded -= new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded -= new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		}
	}

	private void OnLocaleChanged()
	{
		this.RefreshPanel();
		this.Set(this.m_District);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.m_EnableAudio = true;
	}

	private void OnLevelUnloaded()
	{
		this.m_EnableAudio = false;
	}

	public void SetParentButton(UIButton button)
	{
		for (int i = 0; i < this.m_Tabstrip.get_tabCount(); i++)
		{
			this.m_Tabstrip.get_tabs().get_Item(i).GetComponent<TutorialUITag>().m_ParentOverride = button;
		}
	}

	private void RefreshPanel()
	{
		if (Singleton<DistrictManager>.get_instance().IsAnyPolicyLoaded(DistrictPolicies.Types.Special))
		{
			for (int i = 0; i < this.m_Tabstrip.get_tabCount(); i++)
			{
				this.m_Tabstrip.get_tabs().get_Item(i).set_width(this.m_Tabstrip.get_width() / (float)this.m_Tabstrip.get_tabCount());
			}
			this.m_Tabstrip.ShowTab("SpecialStrip");
		}
		else
		{
			int num = this.m_Tabstrip.get_tabCount() - 1;
			for (int j = 0; j < this.m_Tabstrip.get_tabCount(); j++)
			{
				this.m_Tabstrip.get_tabs().get_Item(j).set_width(this.m_Tabstrip.get_width() / (float)num);
			}
			this.m_Tabstrip.HideTab("SpecialStrip");
		}
		for (int k = 0; k < this.m_Tabstrip.get_tabCount(); k++)
		{
			UIButton uIButton = this.m_Tabstrip.get_tabs().get_Item(k) as UIButton;
			UIComponent container = this.m_Tabstrip.get_tabPages().get_components()[k];
			DistrictPolicies.Types enumByName = Utils.GetEnumByName<DistrictPolicies.Types>(uIButton.get_stringUserData());
			if (ToolsModifierControl.IsUnlocked(enumByName))
			{
				uIButton.set_isEnabled(true);
				uIButton.set_tooltip(null);
			}
			else
			{
				uIButton.set_isEnabled(false);
				uIButton.set_tooltip(uIButton.get_text() + " - " + this.GetUnlockText(enumByName));
			}
			PositionData<DistrictPolicies.Policies>[] orderedEnumData = Utils.GetOrderedEnumData<DistrictPolicies.Policies>(uIButton.get_stringUserData());
			this.m_ObjectIndex = 0;
			for (int l = 0; l < orderedEnumData.Length; l++)
			{
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(orderedEnumData[l].get_enumValue()))
				{
					this.SpawnPolicyEntry(container, orderedEnumData[l].get_enumName(), this.GetUnlockText(orderedEnumData[l].get_enumValue()), ToolsModifierControl.IsUnlocked(orderedEnumData[l].get_enumValue()));
				}
			}
		}
		this.m_Initialized = true;
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	private void UpdateColor(bool isDistrict, bool wasDistrict)
	{
		if (isDistrict && wasDistrict)
		{
			if (base.get_component().get_isVisible())
			{
				ValueAnimator.Animate("PoliciesColor", delegate(float val)
				{
					base.get_component().set_color(Color32.Lerp(base.get_component().get_color(), this.m_DistrictChangeColor, val));
				}, new AnimatedFloat(0f, 1f, this.m_DistrictChangeBlinkingSpeed, 2), delegate
				{
					ValueAnimator.Animate("PoliciesColor2", delegate(float val)
					{
						base.get_component().set_color(Color32.Lerp(base.get_component().get_color(), this.m_DistrictColor, val));
					}, new AnimatedFloat(0f, 1f, this.m_DistrictChangeBlinkingSpeed, 2));
				});
			}
			else
			{
				base.get_component().set_color(this.m_DistrictColor);
			}
		}
		else
		{
			Color32 targetColor = (!isDistrict) ? this.m_CityColor : this.m_DistrictColor;
			if (base.get_component().get_isVisible())
			{
				ValueAnimator.Animate("PoliciesColor", delegate(float val)
				{
					this.get_component().set_color(Color32.Lerp(this.get_component().get_color(), targetColor, val));
				}, new AnimatedFloat(0f, 1f, this.m_DistrictCityChangeTransitionSpeed));
			}
			else
			{
				base.get_component().set_color(targetColor);
			}
		}
	}

	public void Set(byte district)
	{
		if (!this.m_Initialized)
		{
			return;
		}
		if (this.m_Unset || this.m_District != district)
		{
			this.m_Unset = false;
			this.UpdateColor(district > 0, this.m_District > 0);
		}
		this.m_District = district;
		if (this.m_District > 0)
		{
			this.m_Caption.set_text(StringUtils.SafeFormat(Locale.Get("POLICIES_DISTRICTCAPTION"), Singleton<DistrictManager>.get_instance().GetDistrictName((int)this.m_District)));
		}
		else
		{
			this.m_Caption.set_text(Locale.Get("POLICIES_CITYCAPTION"));
		}
	}

	internal void Show(byte district)
	{
		if (!this.m_Initialized)
		{
			return;
		}
		this.RefreshPanel();
		DistrictTool districtTool = ToolsModifierControl.SetTool<DistrictTool>();
		if (districtTool != null)
		{
			districtTool.m_mode = DistrictTool.Mode.Select;
		}
		this.Set(district);
		if (!base.get_component().get_isVisible())
		{
			this.m_CloseButton.Hide();
			float num;
			float num2;
			if (this.m_DockingPosition == PoliciesPanel.DockingPosition.Left)
			{
				num = base.get_component().get_parent().get_relativePosition().x - base.get_component().get_size().x;
				num2 = num + base.get_component().get_size().x;
				num -= this.m_SafeMargin;
			}
			else
			{
				num = base.get_component().get_parent().get_relativePosition().x + base.get_component().get_parent().get_size().x;
				num2 = num - base.get_component().get_size().x;
				num += this.m_SafeMargin;
			}
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.x = val;
				relativePosition.y = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y - base.get_component().get_size().y;
				base.get_component().set_relativePosition(relativePosition);
				base.closeToolbarButton.set_absolutePosition(this.m_CloseButton.get_absolutePosition());
			}, new AnimatedFloat(num, num2, this.m_ShowHideTime, this.m_ShowEasingType));
			if (this.m_EnableAudio && Singleton<AudioManager>.get_exists() && this.m_SwooshInSound != null)
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_SwooshInSound, 1f);
			}
		}
	}

	internal void Hide()
	{
		float num;
		float num2;
		if (this.m_DockingPosition == PoliciesPanel.DockingPosition.Left)
		{
			num = base.get_component().get_parent().get_relativePosition().x - base.get_component().get_size().x;
			num2 = num + base.get_component().get_size().x;
			num -= this.m_SafeMargin;
		}
		else
		{
			num = base.get_component().get_parent().get_relativePosition().x + base.get_component().get_parent().get_size().x;
			num2 = num - base.get_component().get_size().x;
			num += this.m_SafeMargin;
		}
		ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
		{
			Vector3 relativePosition = base.get_component().get_relativePosition();
			relativePosition.x = val;
			relativePosition.y = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y - base.get_component().get_size().y;
			base.get_component().set_relativePosition(relativePosition);
		}, new AnimatedFloat(num2, num, this.m_ShowHideTime, this.m_HideEasingType), delegate
		{
			base.get_component().Hide();
		});
		this.m_CloseButton.Show();
		base.closeToolbarButton.set_absolutePosition(this.m_SavePosition);
		if (base.get_component().get_isVisible() && this.m_EnableAudio && Singleton<AudioManager>.get_exists() && this.m_SwooshOutSound != null)
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_SwooshOutSound, 1f);
		}
	}

	public void OnCloseButton()
	{
		base.CloseToolbar();
	}

	private string GetUnlockText(DistrictPolicies.Types policyType)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_PolicyTypeMilestones[(int)policyType];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	private string GetUnlockText(DistrictPolicies.Policies policy)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestone = Singleton<UnlockManager>.get_instance().GetMilestone(policy);
			if (!ToolsModifierControl.IsUnlocked(milestone))
			{
				return "<color #f4a755>" + milestone.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	private void SpawnPolicyEntry(UIComponent container, string name, string unlockText, bool isEnabled)
	{
		UIPanel uIPanel;
		if (container.get_childCount() > this.m_ObjectIndex)
		{
			uIPanel = (container.get_components()[this.m_ObjectIndex] as UIPanel);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(PoliciesPanel.kPolicyTemplate);
			asGameObject.set_name(name);
			uIPanel = (container.AttachUIComponent(asGameObject) as UIPanel);
		}
		uIPanel.FitTo(uIPanel.get_parent(), 0);
		uIPanel.set_stringUserData(name);
		uIPanel.set_objectUserData(this);
		uIPanel.set_isEnabled(isEnabled);
		UIButton uIButton = uIPanel.Find<UIButton>("PolicyButton");
		uIButton.set_text(Locale.Get("POLICIES", name));
		string text = "IconPolicy" + name;
		uIButton.set_pivot((this.m_DockingPosition != PoliciesPanel.DockingPosition.Left) ? 0 : 2);
		if (isEnabled)
		{
			uIButton.set_tooltipBox(this.policiesTooltip);
			uIButton.set_tooltip(TooltipHelper.Format(new string[]
			{
				"title",
				Locale.Get("POLICIES", name),
				"text",
				Locale.Get("POLICIES_DETAIL", name)
			}));
		}
		else
		{
			uIButton.set_tooltipBox(this.defaultTooltip);
			uIButton.set_tooltip(Locale.Get("POLICIES", name) + " - " + unlockText);
		}
		uIButton.set_normalFgSprite(text);
		uIButton.set_disabledFgSprite(text + "Disabled");
		this.m_ObjectIndex++;
	}

	private bool m_Unset = true;

	private byte m_District;

	private UITabstrip m_Tabstrip;

	public AudioClip m_SwooshInSound;

	public AudioClip m_SwooshOutSound;

	public PoliciesPanel.DockingPosition m_DockingPosition;

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	public float m_DistrictChangeBlinkingSpeed = 0.1f;

	public float m_DistrictCityChangeTransitionSpeed = 0.3f;

	public Color32 m_DistrictChangeColor;

	public Color32 m_DistrictColor;

	public Color32 m_CityColor;

	private UILabel m_Caption;

	private bool m_Initialized;

	private UIButton m_CloseButton;

	private Vector3 m_SavePosition;

	private bool m_EnableAudio;

	private int m_ObjectIndex;

	private static readonly string kPolicyTemplate = "PolicyTemplate";

	private UIComponent m_DefaultTooltip;

	private UIComponent m_PoliciesTooltip;

	public enum DockingPosition
	{
		Right,
		Left
	}
}
