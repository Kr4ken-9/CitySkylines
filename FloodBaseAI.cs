﻿using System;

public class FloodBaseAI : DisasterAI
{
	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.Default;
		return true;
	}
}
