﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class ParadoxAccountCreate : MenuPanel
{
	protected override void Initialize()
	{
		this.m_Country.set_items(LocaleInfo.get_countries());
		this.m_Country.set_filteredItems(new int[]
		{
			7
		});
		this.m_Language.set_items(LocaleInfo.get_languages());
		this.m_Language.set_filteredItems(new int[]
		{
			6
		});
		this.m_Country.set_selectedIndex(-1);
		this.m_Language.set_selectedIndex(-1);
		this.m_Privacy.add_eventClick(delegate(UIComponent c, UIMouseEventParameter p)
		{
			PlatformService.ActivateGameOverlayToWebPage(ParadoxAccountCreate.kPdxPrivacy);
		});
		this.m_TermsOfUse.add_eventClick(delegate(UIComponent c, UIMouseEventParameter p)
		{
			PlatformService.ActivateGameOverlayToWebPage(ParadoxAccountCreate.kPdxTermsOfUse);
		});
		this.m_Legal.set_isChecked(false);
		this.m_SubscribeNewsletter.set_isChecked(false);
		this.InitializeDate();
		base.Initialize();
	}

	private void InitializeDate()
	{
		this.m_Month.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnBirthDateChanged));
		this.m_Year.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnBirthDateChanged));
		DateTime now = DateTime.Now;
		List<string> list = new List<string>(now.Year - 1900);
		list.Add(string.Empty);
		for (int i = 1900; i <= now.Year; i++)
		{
			list.Add(i.ToString());
		}
		List<string> list2 = new List<string>(12);
		list2.Add(string.Empty);
		for (int j = 1; j <= 12; j++)
		{
			list2.Add(j.ToString("00"));
		}
		this.m_Year.set_items(list.ToArray());
		this.m_Month.set_items(list2.ToArray());
		this.OnBirthDateChanged(null, 0);
	}

	private void GetDaysInMonth()
	{
		List<string> list = new List<string>();
		list.Add(string.Empty);
		int num;
		if (!int.TryParse(this.m_Year.get_items()[this.m_Year.get_selectedIndex()], out num))
		{
			num = -1;
		}
		int num2;
		if (!int.TryParse(this.m_Month.get_items()[this.m_Month.get_selectedIndex()], out num2))
		{
			num2 = -1;
		}
		int num3;
		if (num2 == -1 || num == -1)
		{
			num3 = 31;
		}
		else
		{
			num3 = DateTime.DaysInMonth(num, num2);
		}
		for (int i = 1; i <= num3; i++)
		{
			list.Add(i.ToString("00"));
		}
		this.m_Day.set_items(list.ToArray());
		if (this.m_Day.get_selectedIndex() >= this.m_Day.get_items().Length)
		{
			this.m_Day.set_selectedIndex(this.m_Day.get_items().Length - 1);
		}
	}

	private void OnBirthDateChanged(UIComponent comp, int sel)
	{
		this.GetDaysInMonth();
	}

	public void OnCreate()
	{
		if (!this.ValidateForm())
		{
			return;
		}
		this.m_Create.set_isEnabled(false);
		this.m_CreatingAccount = true;
		PopsManager.AccountCreateData data = new PopsManager.AccountCreateData(this.m_Email.get_text(), this.m_Password.get_text(), LocaleInfo.get_languagesISO()[this.m_Language.get_selectedIndex()].ToUpper(), LocaleInfo.get_countriesISO()[this.m_Country.get_selectedIndex()].ToLower(), new DateTime?(new DateTime(int.Parse(this.m_Year.get_items()[this.m_Year.get_selectedIndex()]), int.Parse(this.m_Month.get_items()[this.m_Month.get_selectedIndex()]), int.Parse(this.m_Day.get_items()[this.m_Day.get_selectedIndex()]))), "Cities Main menu", null, "cities_skylines", null, null, null, this.m_FirstName.get_text(), this.m_LastName.get_text(), this.m_AddressLine1.get_text(), this.m_AddressLine2.get_text(), this.m_City.get_text(), this.m_State.get_text(), this.m_Zipcode.get_text(), this.m_Phone.get_text(), ParadoxAccountCreate.emailTemplate, ParadoxAccountCreate.landingUrl, null);
		Singleton<PopsManager>.get_instance().CreateAccount(data, new Action<PopsManager.AccountActionResult>(this.OnAccountCreated));
	}

	private void OnAccountCreated(PopsManager.AccountActionResult result)
	{
		if (result.Success)
		{
			UIView.get_library().Hide(base.GetType().Name, 1);
		}
		else
		{
			UIView.get_library().ShowModal<ExceptionPanel>("ExceptionPanel").SetMessage(Locale.Get("EXCEPTIONTITLE", "PdxAccountError"), StringUtils.SafeFormat("Error: {0}", result.StatusMessage), true);
			Debug.Log(StringUtils.SafeFormat("Error: {0}", result.StatusMessage.ToString()));
		}
		this.m_Create.set_isEnabled(true);
		this.m_CreatingAccount = false;
	}

	protected void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 13)
			{
				this.OnCreate();
				p.Use();
			}
			else if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	private bool ValidateForm()
	{
		return !string.IsNullOrEmpty(this.m_Email.get_text()) && !string.IsNullOrEmpty(this.m_Password.get_text()) && this.m_Day.get_selectedIndex() > -1 && this.m_Month.get_selectedIndex() > -1 && this.m_Year.get_selectedIndex() > -1 && this.m_Country.get_selectedIndex() > -1 && this.m_Language.get_selectedIndex() > -1 && !string.IsNullOrEmpty(this.m_Day.get_items()[this.m_Day.get_selectedIndex()]) && !string.IsNullOrEmpty(this.m_Month.get_items()[this.m_Month.get_selectedIndex()]) && !string.IsNullOrEmpty(this.m_Year.get_items()[this.m_Year.get_selectedIndex()]) && !string.IsNullOrEmpty(this.m_Country.get_items()[this.m_Country.get_selectedIndex()]) && !string.IsNullOrEmpty(this.m_Language.get_items()[this.m_Language.get_selectedIndex()]) && this.m_Legal.get_isChecked();
	}

	private void Update()
	{
		if (base.get_component().get_isVisible() && !this.m_CreatingAccount)
		{
			this.m_Create.set_isEnabled(this.ValidateForm());
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_Email.Focus();
		}
	}

	private static readonly string kPdxTermsOfUse = "https://www.paradoxplaza.com/terms-use";

	private static readonly string kPdxPrivacy = "https://www.paradoxplaza.com/privacy";

	public static string emailTemplate;

	public static string landingUrl;

	public bool m_CreatingAccount;

	public UIComponent[] m_TabFocusList;

	public UITextField m_Email;

	public UITextField m_Password;

	public UITextField m_FirstName;

	public UITextField m_LastName;

	public UITextField m_AddressLine1;

	public UITextField m_AddressLine2;

	public UITextField m_City;

	public UITextField m_Zipcode;

	public UITextField m_Phone;

	public UITextField m_State;

	public UIDropDown m_Country;

	public UIDropDown m_Language;

	public UIDropDown m_Day;

	public UIDropDown m_Month;

	public UIDropDown m_Year;

	public UICheckBox m_Legal;

	public UICheckBox m_SubscribeNewsletter;

	public UIButton m_Privacy;

	public UIButton m_TermsOfUse;

	public UIButton m_Create;
}
