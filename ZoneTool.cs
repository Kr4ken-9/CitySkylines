﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ZoneTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
		this.m_closeSegments = new ushort[16];
		this.m_fillBuffer1 = new ulong[64];
		this.m_fillBuffer2 = new ulong[64];
		this.m_fillBuffer3 = new ulong[64];
		this.m_fillPositions = new FastList<ZoneTool.FillPos>();
		this.m_dataLock = new object();
	}

	protected override void OnToolGUI(Event e)
	{
		bool isInsideUI = this.m_toolController.IsInsideUI;
		if (e.get_type() == null)
		{
			if (!isInsideUI)
			{
				if (e.get_button() == 0)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.BeginZoning());
				}
				else if (e.get_button() == 1)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.BeginDezoning());
				}
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.EndZoning(!isInsideUI));
			}
			else if (e.get_button() == 1)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.EndDezoning(!isInsideUI));
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		Singleton<TerrainManager>.get_instance().RenderZones = true;
		Shader.SetGlobalFloat("_ForceZoneColors", 1f);
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Shader.SetGlobalFloat("_ForceZoneColors", 0f);
		Singleton<TerrainManager>.get_instance().RenderZones = false;
		this.m_validPosition = false;
		this.m_zoning = false;
		this.m_dezoning = false;
		this.m_mouseRayValid = false;
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		bool zoning;
		bool dezoning;
		bool validPosition;
		Vector3 startPosition;
		Vector3 mousePosition;
		Vector3 startDirection;
		Vector3 mouseDirection;
		try
		{
			zoning = this.m_zoning;
			dezoning = this.m_dezoning;
			validPosition = this.m_validPosition;
			startPosition = this.m_startPosition;
			mousePosition = this.m_mousePosition;
			startDirection = this.m_startDirection;
			mouseDirection = this.m_mouseDirection;
			for (int i = 0; i < 64; i++)
			{
				this.m_fillBuffer3[i] = this.m_fillBuffer2[i];
			}
		}
		finally
		{
			Monitor.Exit(this.m_dataLock);
		}
		if ((!zoning && !dezoning && !validPosition) || !Cursor.get_visible() || this.m_toolController.IsInsideUI)
		{
			base.RenderOverlay(cameraInfo);
			return;
		}
		Color color;
		if (zoning || dezoning)
		{
			color = Singleton<ZoneManager>.get_instance().m_properties.m_activeColor;
		}
		else if (this.m_zone <= ItemClass.Zone.Unzoned || dezoning)
		{
			color = Singleton<ZoneManager>.get_instance().m_properties.m_unzoneColor;
		}
		else
		{
			color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[(int)this.m_zone];
		}
		ZoneTool.Mode mode = this.m_mode;
		if (mode != ZoneTool.Mode.Select)
		{
			if (mode != ZoneTool.Mode.Brush)
			{
				if (mode == ZoneTool.Mode.Fill)
				{
					Vector3 vector = mouseDirection;
					Vector3 vector2;
					vector2..ctor(vector.z, 0f, -vector.x);
					int num = -1;
					ulong num2 = this.m_fillBuffer3[0];
					for (int j = 0; j <= 64; j++)
					{
						int num3 = -1;
						if (j == 64 || this.m_fillBuffer3[j] != num2)
						{
							if (num != -1)
							{
								int num4 = j - 1;
								for (int k = 0; k <= 64; k++)
								{
									if (k == 64 || (num2 & 1uL << k) == 0uL)
									{
										if (num3 != -1)
										{
											int num5 = k - 1;
											Vector3 vector3 = mousePosition + vector * (float)((num3 - 32) * 8) + vector2 * (float)((num - 32) * 8);
											Vector3 vector4 = mousePosition + vector * (float)((num5 - 32) * 8) + vector2 * (float)((num4 - 32) * 8);
											float num6 = Mathf.Round(((vector4.x - vector3.x) * vector.x + (vector4.z - vector3.z) * vector.z) * 0.125f) * 8f;
											float num7 = Mathf.Round(((vector4.x - vector3.x) * vector2.x + (vector4.z - vector3.z) * vector2.z) * 0.125f) * 8f;
											float num8 = (num6 < 0f) ? -4f : 4f;
											float num9 = (num7 < 0f) ? -4f : 4f;
											Quad3 quad = default(Quad3);
											quad.a = vector3 - vector * num8 - vector2 * num9;
											quad.b = vector3 - vector * num8 + vector2 * (num7 + num9);
											quad.c = vector3 + vector * (num6 + num8) + vector2 * (num7 + num9);
											quad.d = vector3 + vector * (num6 + num8) - vector2 * num9;
											if (num8 != num9)
											{
												Vector3 b = quad.b;
												quad.b = quad.d;
												quad.d = b;
											}
											ToolManager expr_624_cp_0 = Singleton<ToolManager>.get_instance();
											expr_624_cp_0.m_drawCallData.m_overlayCalls = expr_624_cp_0.m_drawCallData.m_overlayCalls + 1;
											Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, -1f, 1025f, false, false);
										}
										num3 = -1;
									}
									else if (num3 == -1)
									{
										num3 = k;
									}
								}
							}
							if (j != 64 && this.m_fillBuffer3[j] != 0uL)
							{
								num = j;
								num2 = this.m_fillBuffer3[j];
							}
							else
							{
								num = -1;
							}
						}
						else if (num == -1 && this.m_fillBuffer3[j] != 0uL)
						{
							num = j;
							num2 = this.m_fillBuffer3[j];
						}
					}
				}
			}
			else
			{
				ToolManager expr_36C_cp_0 = Singleton<ToolManager>.get_instance();
				expr_36C_cp_0.m_drawCallData.m_overlayCalls = expr_36C_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, mousePosition, this.m_brushSize, -1f, 1025f, false, true);
			}
		}
		else
		{
			Vector3 vector5 = (!zoning && !dezoning) ? mousePosition : startPosition;
			Vector3 vector6 = mousePosition;
			Vector3 vector7 = (!zoning && !dezoning) ? mouseDirection : startDirection;
			Vector3 vector8;
			vector8..ctor(vector7.z, 0f, -vector7.x);
			float num10 = Mathf.Round(((vector6.x - vector5.x) * vector7.x + (vector6.z - vector5.z) * vector7.z) * 0.125f) * 8f;
			float num11 = Mathf.Round(((vector6.x - vector5.x) * vector8.x + (vector6.z - vector5.z) * vector8.z) * 0.125f) * 8f;
			float num12 = (num10 < 0f) ? -4f : 4f;
			float num13 = (num11 < 0f) ? -4f : 4f;
			Quad3 quad2 = default(Quad3);
			quad2.a = vector5 - vector7 * num12 - vector8 * num13;
			quad2.b = vector5 - vector7 * num12 + vector8 * (num11 + num13);
			quad2.c = vector5 + vector7 * (num10 + num12) + vector8 * (num11 + num13);
			quad2.d = vector5 + vector7 * (num10 + num12) - vector8 * num13;
			if (num12 != num13)
			{
				Vector3 b2 = quad2.b;
				quad2.b = quad2.d;
				quad2.d = b2;
			}
			ToolManager expr_330_cp_0 = Singleton<ToolManager>.get_instance();
			expr_330_cp_0.m_drawCallData.m_overlayCalls = expr_330_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad2, -1f, 1025f, false, true);
		}
		base.RenderOverlay(cameraInfo);
	}

	protected override void OnToolUpdate()
	{
		ItemClass.Zone zone;
		if (this.m_zone <= ItemClass.Zone.Unzoned || this.m_dezoning)
		{
			zone = ItemClass.Zone.Unzoned;
		}
		else
		{
			zone = this.m_zone;
		}
		if (this.m_zoneCursors != null && (ItemClass.Zone)this.m_zoneCursors.Length > zone)
		{
			base.ToolCursor = this.m_zoneCursors[(int)zone];
		}
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		Vector3 cameraDirection = Vector3.Cross(Camera.get_main().get_transform().get_right(), Vector3.get_up());
		cameraDirection.Normalize();
		while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
			this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
			this.m_cameraDirection = cameraDirection;
			this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		}
		finally
		{
			Monitor.Exit(this.m_dataLock);
		}
		ToolBase.OverrideInfoMode = false;
	}

	[DebuggerHidden]
	private IEnumerator BeginZoning()
	{
		ZoneTool.<BeginZoning>c__Iterator0 <BeginZoning>c__Iterator = new ZoneTool.<BeginZoning>c__Iterator0();
		<BeginZoning>c__Iterator.$this = this;
		return <BeginZoning>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator BeginDezoning()
	{
		ZoneTool.<BeginDezoning>c__Iterator1 <BeginDezoning>c__Iterator = new ZoneTool.<BeginDezoning>c__Iterator1();
		<BeginDezoning>c__Iterator.$this = this;
		return <BeginDezoning>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator EndZoning(bool applyChanges)
	{
		ZoneTool.<EndZoning>c__Iterator2 <EndZoning>c__Iterator = new ZoneTool.<EndZoning>c__Iterator2();
		<EndZoning>c__Iterator.applyChanges = applyChanges;
		<EndZoning>c__Iterator.$this = this;
		return <EndZoning>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator EndDezoning(bool applyChanges)
	{
		ZoneTool.<EndDezoning>c__Iterator3 <EndDezoning>c__Iterator = new ZoneTool.<EndDezoning>c__Iterator3();
		<EndDezoning>c__Iterator.applyChanges = applyChanges;
		<EndDezoning>c__Iterator.$this = this;
		return <EndDezoning>c__Iterator;
	}

	private void Snap(ref Vector3 point, ref Vector3 direction, ref ItemClass.Zone zone, ref bool occupied1, ref bool occupied2, ref ZoneBlock block)
	{
		direction..ctor(Mathf.Cos(block.m_angle), 0f, Mathf.Sin(block.m_angle));
		Vector3 vector = direction * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Vector3 vector3 = block.m_position + vector * 0.5f + vector2 * 0.5f;
		Vector2 vector4;
		vector4..ctor(point.x - vector3.x, point.z - vector3.z);
		int num = Mathf.RoundToInt((vector4.x * vector.x + vector4.y * vector.z) * 0.015625f);
		int num2 = Mathf.RoundToInt((vector4.x * vector2.x + vector4.y * vector2.z) * 0.015625f);
		point.x = vector3.x + (float)num * vector.x + (float)num2 * vector2.x;
		point.z = vector3.z + (float)num * vector.z + (float)num2 * vector2.z;
		if (num >= -4 && num < 0 && num2 >= -4 && num2 < 4)
		{
			zone = block.GetZone(num + 4, num2 + 4);
			occupied1 = block.IsOccupied1(num + 4, num2 + 4);
			occupied2 = block.IsOccupied2(num + 4, num2 + 4);
		}
	}

	public override void SimulationStep()
	{
		while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		Ray mouseRay;
		Vector3 cameraDirection;
		bool mouseRayValid;
		try
		{
			mouseRay = this.m_mouseRay;
			cameraDirection = this.m_cameraDirection;
			mouseRayValid = this.m_mouseRayValid;
		}
		finally
		{
			Monitor.Exit(this.m_dataLock);
		}
		if (this.m_mode == ZoneTool.Mode.Fill)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<ZoneManager>.get_instance().m_optionsNotUsed.Activate(properties.m_zoneOptionsNotUsed);
			}
		}
		else
		{
			GenericGuide optionsNotUsed = Singleton<ZoneManager>.get_instance().m_optionsNotUsed;
			if (optionsNotUsed != null && !optionsNotUsed.m_disabled)
			{
				optionsNotUsed.Disable();
			}
		}
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
		{
			ZoneTool.Mode mode = this.m_mode;
			if (mode != ZoneTool.Mode.Select)
			{
				if (mode != ZoneTool.Mode.Brush)
				{
					if (mode == ZoneTool.Mode.Fill)
					{
						Singleton<NetManager>.get_instance().GetClosestSegments(raycastOutput.m_hitPos, this.m_closeSegments, out this.m_closeSegmentCount);
						float num = 256f;
						ushort num2 = 0;
						for (int i = 0; i < this.m_closeSegmentCount; i++)
						{
							Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_closeSegments[i]].GetClosestZoneBlock(raycastOutput.m_hitPos, ref num, ref num2);
						}
						if (num2 != 0)
						{
							ZoneBlock zoneBlock = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)num2];
							Vector3 forward = Vector3.get_forward();
							ItemClass.Zone requiredZone = ItemClass.Zone.Unzoned;
							bool occupied = false;
							bool occupied2 = false;
							this.Snap(ref raycastOutput.m_hitPos, ref forward, ref requiredZone, ref occupied, ref occupied2, ref zoneBlock);
							if (this.CalculateFillBuffer(raycastOutput.m_hitPos, forward, requiredZone, occupied, occupied2))
							{
								while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
								{
								}
								try
								{
									for (int j = 0; j < 64; j++)
									{
										this.m_fillBuffer2[j] = this.m_fillBuffer1[j];
									}
									this.m_mouseDirection = forward;
									this.m_mousePosition = raycastOutput.m_hitPos;
									this.m_validPosition = true;
								}
								finally
								{
									Monitor.Exit(this.m_dataLock);
								}
							}
							else
							{
								this.m_validPosition = false;
							}
						}
						else
						{
							this.m_validPosition = false;
						}
					}
				}
				else
				{
					while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
					{
					}
					try
					{
						this.m_mousePosition = raycastOutput.m_hitPos;
						this.m_validPosition = true;
					}
					finally
					{
						Monitor.Exit(this.m_dataLock);
					}
					if (this.m_zoning != this.m_dezoning)
					{
						this.ApplyBrush();
					}
				}
			}
			else if (!this.m_zoning && !this.m_dezoning)
			{
				Singleton<NetManager>.get_instance().GetClosestSegments(raycastOutput.m_hitPos, this.m_closeSegments, out this.m_closeSegmentCount);
				float num3 = 256f;
				ushort num4 = 0;
				for (int k = 0; k < this.m_closeSegmentCount; k++)
				{
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_closeSegments[k]].GetClosestZoneBlock(raycastOutput.m_hitPos, ref num3, ref num4);
				}
				if (num4 != 0)
				{
					ZoneBlock zoneBlock2 = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)num4];
					Vector3 forward2 = Vector3.get_forward();
					ItemClass.Zone zone = ItemClass.Zone.Unzoned;
					bool flag = false;
					bool flag2 = false;
					this.Snap(ref raycastOutput.m_hitPos, ref forward2, ref zone, ref flag, ref flag2, ref zoneBlock2);
					while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
					{
					}
					try
					{
						this.m_mouseDirection = forward2;
						this.m_mousePosition = raycastOutput.m_hitPos;
						this.m_validPosition = true;
					}
					finally
					{
						Monitor.Exit(this.m_dataLock);
					}
				}
				else
				{
					while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
					{
					}
					try
					{
						this.m_mouseDirection = cameraDirection;
						this.m_mousePosition = raycastOutput.m_hitPos;
						this.m_validPosition = true;
					}
					finally
					{
						Monitor.Exit(this.m_dataLock);
					}
				}
			}
			else
			{
				while (!Monitor.TryEnter(this.m_dataLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				try
				{
					this.m_mousePosition = raycastOutput.m_hitPos;
					this.m_validPosition = true;
				}
				finally
				{
					Monitor.Exit(this.m_dataLock);
				}
			}
		}
		else
		{
			this.m_validPosition = false;
		}
	}

	private bool CalculateFillBuffer(Vector3 position, Vector3 direction, ItemClass.Zone requiredZone, bool occupied1, bool occupied2)
	{
		for (int i = 0; i < 64; i++)
		{
			this.m_fillBuffer1[i] = 0uL;
		}
		if (!occupied2)
		{
			float angle = Mathf.Atan2(-direction.x, direction.z);
			float num = position.x - 256f;
			float num2 = position.z - 256f;
			float num3 = position.x + 256f;
			float num4 = position.z + 256f;
			int num5 = Mathf.Max((int)((num - 46f) / 64f + 75f), 0);
			int num6 = Mathf.Max((int)((num2 - 46f) / 64f + 75f), 0);
			int num7 = Mathf.Min((int)((num3 + 46f) / 64f + 75f), 149);
			int num8 = Mathf.Min((int)((num4 + 46f) / 64f + 75f), 149);
			ZoneManager instance = Singleton<ZoneManager>.get_instance();
			for (int j = num6; j <= num8; j++)
			{
				for (int k = num5; k <= num7; k++)
				{
					ushort num9 = instance.m_zoneGrid[j * 150 + k];
					int num10 = 0;
					while (num9 != 0)
					{
						Vector3 position2 = instance.m_blocks.m_buffer[(int)num9].m_position;
						float num11 = Mathf.Max(Mathf.Max(num - 46f - position2.x, num2 - 46f - position2.z), Mathf.Max(position2.x - num3 - 46f, position2.z - num4 - 46f));
						if (num11 < 0f)
						{
							this.CalculateFillBuffer(position, direction, angle, num9, ref instance.m_blocks.m_buffer[(int)num9], requiredZone, occupied1, occupied2);
						}
						num9 = instance.m_blocks.m_buffer[(int)num9].m_nextGridBlock;
						if (++num10 >= 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
		if ((this.m_fillBuffer1[32] & 4294967296uL) != 0uL)
		{
			this.m_fillPositions.Clear();
			int l = 0;
			int num12 = 32;
			int num13 = 32;
			int num14 = 32;
			int num15 = 32;
			ZoneTool.FillPos fillPos;
			fillPos.m_x = 32;
			fillPos.m_z = 32;
			this.m_fillPositions.Add(fillPos);
			this.m_fillBuffer1[32] &= 18446744069414584319uL;
			while (l < this.m_fillPositions.m_size)
			{
				fillPos = this.m_fillPositions.m_buffer[l++];
				if (fillPos.m_z > 0)
				{
					ZoneTool.FillPos item = fillPos;
					item.m_z -= 1;
					if ((this.m_fillBuffer1[(int)item.m_z] & 1uL << (int)item.m_x) != 0uL)
					{
						this.m_fillPositions.Add(item);
						this.m_fillBuffer1[(int)item.m_z] &= ~(1uL << (int)item.m_x);
						if ((int)item.m_z < num13)
						{
							num13 = (int)item.m_z;
						}
					}
				}
				if (fillPos.m_x > 0)
				{
					ZoneTool.FillPos item2 = fillPos;
					item2.m_x -= 1;
					if ((this.m_fillBuffer1[(int)item2.m_z] & 1uL << (int)item2.m_x) != 0uL)
					{
						this.m_fillPositions.Add(item2);
						this.m_fillBuffer1[(int)item2.m_z] &= ~(1uL << (int)item2.m_x);
						if ((int)item2.m_x < num12)
						{
							num12 = (int)item2.m_x;
						}
					}
				}
				if (fillPos.m_x < 63)
				{
					ZoneTool.FillPos item3 = fillPos;
					item3.m_x += 1;
					if ((this.m_fillBuffer1[(int)item3.m_z] & 1uL << (int)item3.m_x) != 0uL)
					{
						this.m_fillPositions.Add(item3);
						this.m_fillBuffer1[(int)item3.m_z] &= ~(1uL << (int)item3.m_x);
						if ((int)item3.m_x > num14)
						{
							num14 = (int)item3.m_x;
						}
					}
				}
				if (fillPos.m_z < 63)
				{
					ZoneTool.FillPos item4 = fillPos;
					item4.m_z += 1;
					if ((this.m_fillBuffer1[(int)item4.m_z] & 1uL << (int)item4.m_x) != 0uL)
					{
						this.m_fillPositions.Add(item4);
						this.m_fillBuffer1[(int)item4.m_z] &= ~(1uL << (int)item4.m_x);
						if ((int)item4.m_z > num15)
						{
							num15 = (int)item4.m_z;
						}
					}
				}
			}
			for (int m = 0; m < 64; m++)
			{
				this.m_fillBuffer1[m] = 0uL;
			}
			for (int n = 0; n < this.m_fillPositions.m_size; n++)
			{
				ZoneTool.FillPos fillPos2 = this.m_fillPositions.m_buffer[n];
				this.m_fillBuffer1[(int)fillPos2.m_z] |= 1uL << (int)fillPos2.m_x;
			}
			return true;
		}
		for (int num16 = 0; num16 < 64; num16++)
		{
			this.m_fillBuffer1[num16] = 0uL;
		}
		return false;
	}

	private void CalculateFillBuffer(Vector3 position, Vector3 direction, float angle, ushort blockIndex, ref ZoneBlock block, ItemClass.Zone requiredZone, bool occupied1, bool occupied2)
	{
		float num = Mathf.Abs(block.m_angle - angle) * 0.636619747f;
		num -= Mathf.Floor(num);
		if (num >= 0.01f && num <= 0.99f)
		{
			return;
		}
		int rowCount = block.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(block.m_angle), 0f, Mathf.Sin(block.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		for (int i = 0; i < rowCount; i++)
		{
			Vector3 vector3 = ((float)i - 3.5f) * vector2;
			for (int j = 0; j < 4; j++)
			{
				if ((block.m_valid & 1uL << (i << 3 | j)) != 0uL)
				{
					if (block.GetZone(j, i) == requiredZone)
					{
						if (occupied1)
						{
							if (requiredZone == ItemClass.Zone.Unzoned && (block.m_occupied1 & 1uL << (i << 3 | j)) == 0uL)
							{
								goto IL_278;
							}
						}
						else if (occupied2)
						{
							if (requiredZone == ItemClass.Zone.Unzoned && (block.m_occupied2 & 1uL << (i << 3 | j)) == 0uL)
							{
								goto IL_278;
							}
						}
						else if (((block.m_occupied1 | block.m_occupied2) & 1uL << (i << 3 | j)) != 0uL)
						{
							goto IL_278;
						}
						Vector3 vector4 = ((float)j - 3.5f) * vector;
						Vector3 vector5 = block.m_position + vector4 + vector3 - position;
						float num2 = (vector5.x * direction.x + vector5.z * direction.z) * 0.125f + 32f;
						float num3 = (vector5.x * direction.z - vector5.z * direction.x) * 0.125f + 32f;
						int num4 = Mathf.RoundToInt(num2);
						int num5 = Mathf.RoundToInt(num3);
						if (num4 >= 0 && num4 < 64 && num5 >= 0 && num5 < 64)
						{
							if (Mathf.Abs(num2 - (float)num4) < 0.0125f && Mathf.Abs(num3 - (float)num5) < 0.0125f)
							{
								this.m_fillBuffer1[num5] |= 1uL << num4;
							}
						}
					}
				}
				IL_278:;
			}
		}
	}

	private void ApplyFill()
	{
		if (!this.m_validPosition)
		{
			return;
		}
		Vector3 mousePosition = this.m_mousePosition;
		Vector3 mouseDirection = this.m_mouseDirection;
		float angle = Mathf.Atan2(-mouseDirection.x, mouseDirection.z);
		float num = mousePosition.x - 256f;
		float num2 = mousePosition.z - 256f;
		float num3 = mousePosition.x + 256f;
		float num4 = mousePosition.z + 256f;
		int num5 = Mathf.Max((int)((num - 46f) / 64f + 75f), 0);
		int num6 = Mathf.Max((int)((num2 - 46f) / 64f + 75f), 0);
		int num7 = Mathf.Min((int)((num3 + 46f) / 64f + 75f), 149);
		int num8 = Mathf.Min((int)((num4 + 46f) / 64f + 75f), 149);
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		bool flag = false;
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_zoneGrid[i * 150 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = instance.m_blocks.m_buffer[(int)num9].m_position;
					float num11 = Mathf.Max(Mathf.Max(num - 46f - position.x, num2 - 46f - position.z), Mathf.Max(position.x - num3 - 46f, position.z - num4 - 46f));
					if (num11 < 0f && this.ApplyFillBuffer(mousePosition, mouseDirection, angle, num9, ref instance.m_blocks.m_buffer[(int)num9]))
					{
						flag = true;
					}
					num9 = instance.m_blocks.m_buffer[(int)num9].m_nextGridBlock;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (flag)
		{
			if (this.m_zoning)
			{
				this.UsedZone(this.m_zone);
			}
			EffectInfo fillEffect = instance.m_properties.m_fillEffect;
			if (fillEffect != null)
			{
				EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(mousePosition, Vector3.get_up(), 1f);
				Singleton<EffectManager>.get_instance().DispatchEffect(fillEffect, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
			}
		}
	}

	private bool ApplyFillBuffer(Vector3 position, Vector3 direction, float angle, ushort blockIndex, ref ZoneBlock block)
	{
		int rowCount = block.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(block.m_angle), 0f, Mathf.Sin(block.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		bool flag = false;
		for (int i = 0; i < rowCount; i++)
		{
			Vector3 vector3 = ((float)i - 3.5f) * vector2;
			for (int j = 0; j < 4; j++)
			{
				Vector3 vector4 = ((float)j - 3.5f) * vector;
				Vector3 vector5 = block.m_position + vector4 + vector3 - position;
				float num = (vector5.x * direction.x + vector5.z * direction.z) * 0.125f + 32f;
				float num2 = (vector5.x * direction.z - vector5.z * direction.x) * 0.125f + 32f;
				int num3 = Mathf.Clamp(Mathf.RoundToInt(num), 0, 63);
				int num4 = Mathf.Clamp(Mathf.RoundToInt(num2), 0, 63);
				bool flag2 = false;
				int num5 = -1;
				while (num5 <= 1 && !flag2)
				{
					int num6 = -1;
					while (num6 <= 1 && !flag2)
					{
						int num7 = num3 + num6;
						int num8 = num4 + num5;
						if (num7 >= 0 && num7 < 64 && num8 >= 0 && num8 < 64)
						{
							if ((num - (float)num7) * (num - (float)num7) + (num2 - (float)num8) * (num2 - (float)num8) < 0.5625f)
							{
								if ((this.m_fillBuffer1[num8] & 1uL << num7) != 0uL)
								{
									if (this.m_zoning)
									{
										if ((this.m_zone == ItemClass.Zone.Unzoned || block.GetZone(j, i) == ItemClass.Zone.Unzoned) && block.SetZone(j, i, this.m_zone))
										{
											flag = true;
										}
									}
									else if (this.m_dezoning && block.SetZone(j, i, ItemClass.Zone.Unzoned))
									{
										flag = true;
									}
									flag2 = true;
								}
							}
						}
						num6++;
					}
					num5++;
				}
			}
		}
		if (flag)
		{
			block.RefreshZoning(blockIndex);
			return true;
		}
		return false;
	}

	private void ApplyZoning()
	{
		Vector2 vector = VectorUtils.XZ(this.m_startPosition);
		Vector2 vector2 = VectorUtils.XZ(this.m_mousePosition);
		Vector2 vector3 = VectorUtils.XZ(this.m_startDirection);
		Vector2 vector4;
		vector4..ctor(vector3.y, -vector3.x);
		float num = Mathf.Round(((vector2.x - vector.x) * vector3.x + (vector2.y - vector.y) * vector3.y) * 0.125f) * 8f;
		float num2 = Mathf.Round(((vector2.x - vector.x) * vector4.x + (vector2.y - vector.y) * vector4.y) * 0.125f) * 8f;
		float num3 = (num < 0f) ? -4f : 4f;
		float num4 = (num2 < 0f) ? -4f : 4f;
		Quad2 quad = default(Quad2);
		quad.a = vector - vector3 * num3 - vector4 * num4;
		quad.b = vector - vector3 * num3 + vector4 * (num2 + num4);
		quad.c = vector + vector3 * (num + num3) + vector4 * (num2 + num4);
		quad.d = vector + vector3 * (num + num3) - vector4 * num4;
		if (num3 == num4)
		{
			Vector2 b = quad.b;
			quad.b = quad.d;
			quad.d = b;
		}
		Vector2 vector5 = quad.Min();
		Vector2 vector6 = quad.Max();
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		int num5 = Mathf.Max((int)((vector5.x - 46f) / 64f + 75f), 0);
		int num6 = Mathf.Max((int)((vector5.y - 46f) / 64f + 75f), 0);
		int num7 = Mathf.Min((int)((vector6.x + 46f) / 64f + 75f), 149);
		int num8 = Mathf.Min((int)((vector6.y + 46f) / 64f + 75f), 149);
		bool flag = false;
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_zoneGrid[i * 150 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = instance.m_blocks.m_buffer[(int)num9].m_position;
					float num11 = Mathf.Max(Mathf.Max(vector5.x - 46f - position.x, vector5.y - 46f - position.z), Mathf.Max(position.x - vector6.x - 46f, position.z - vector6.y - 46f));
					if (num11 < 0f && this.ApplyZoning(num9, ref instance.m_blocks.m_buffer[(int)num9], quad))
					{
						flag = true;
					}
					num9 = instance.m_blocks.m_buffer[(int)num9].m_nextGridBlock;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (flag)
		{
			if (this.m_zoning)
			{
				this.UsedZone(this.m_zone);
			}
			EffectInfo fillEffect = instance.m_properties.m_fillEffect;
			if (fillEffect != null)
			{
				EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea((vector + vector2) * 0.5f, Vector3.get_up(), 1f);
				Singleton<EffectManager>.get_instance().DispatchEffect(fillEffect, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
			}
		}
	}

	private bool ApplyZoning(ushort blockIndex, ref ZoneBlock data, Quad2 quad2)
	{
		int rowCount = data.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(data.m_angle), Mathf.Sin(data.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = VectorUtils.XZ(data.m_position);
		Quad2 quad3 = default(Quad2);
		quad3.a = vector3 - 4f * vector - 4f * vector2;
		quad3.b = vector3 + 4f * vector - 4f * vector2;
		quad3.c = vector3 + 4f * vector + (float)(rowCount - 4) * vector2;
		quad3.d = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		if (!quad3.Intersect(quad2))
		{
			return false;
		}
		bool flag = false;
		for (int i = 0; i < rowCount; i++)
		{
			Vector2 vector4 = ((float)i - 3.5f) * vector2;
			for (int j = 0; j < 4; j++)
			{
				Vector2 vector5 = ((float)j - 3.5f) * vector;
				Vector2 vector6 = vector3 + vector5 + vector4;
				if (quad2.Intersect(vector6))
				{
					if (this.m_zoning)
					{
						if ((this.m_zone == ItemClass.Zone.Unzoned || data.GetZone(j, i) == ItemClass.Zone.Unzoned) && data.SetZone(j, i, this.m_zone))
						{
							flag = true;
						}
					}
					else if (this.m_dezoning && data.SetZone(j, i, ItemClass.Zone.Unzoned))
					{
						flag = true;
					}
				}
			}
		}
		if (flag)
		{
			data.RefreshZoning(blockIndex);
			return true;
		}
		return false;
	}

	private void UsedZone(ItemClass.Zone zone)
	{
		if (zone != ItemClass.Zone.None)
		{
			ZoneManager instance = Singleton<ZoneManager>.get_instance();
			instance.m_zonesNotUsed.Disable();
			instance.m_zoneNotUsed[(int)zone].Disable();
			switch (zone)
			{
			case ItemClass.Zone.ResidentialLow:
			case ItemClass.Zone.ResidentialHigh:
				instance.m_zoneDemandResidential.Deactivate();
				break;
			case ItemClass.Zone.CommercialLow:
			case ItemClass.Zone.CommercialHigh:
				instance.m_zoneDemandCommercial.Deactivate();
				break;
			case ItemClass.Zone.Industrial:
			case ItemClass.Zone.Office:
				instance.m_zoneDemandWorkplace.Deactivate();
				break;
			}
		}
	}

	private void ApplyBrush(ushort blockIndex, ref ZoneBlock data, Vector3 position, float brushRadius)
	{
		Vector3 vector = data.m_position - position;
		if (Mathf.Abs(vector.x) > 46f + brushRadius || Mathf.Abs(vector.z) > 46f + brushRadius)
		{
			return;
		}
		int num = (int)((data.m_flags & 65280u) >> 8);
		Vector3 vector2 = new Vector3(Mathf.Cos(data.m_angle), 0f, Mathf.Sin(data.m_angle)) * 8f;
		Vector3 vector3;
		vector3..ctor(vector2.z, 0f, -vector2.x);
		bool flag = false;
		for (int i = 0; i < num; i++)
		{
			Vector3 vector4 = ((float)i - 3.5f) * vector3;
			for (int j = 0; j < 4; j++)
			{
				Vector3 vector5 = ((float)j - 3.5f) * vector2;
				Vector3 vector6 = vector + vector5 + vector4;
				float num2 = vector6.x * vector6.x + vector6.z * vector6.z;
				if (num2 <= brushRadius * brushRadius)
				{
					if (this.m_zoning)
					{
						if ((this.m_zone == ItemClass.Zone.Unzoned || data.GetZone(j, i) == ItemClass.Zone.Unzoned) && data.SetZone(j, i, this.m_zone))
						{
							flag = true;
						}
					}
					else if (this.m_dezoning && data.SetZone(j, i, ItemClass.Zone.Unzoned))
					{
						flag = true;
					}
				}
			}
		}
		if (flag)
		{
			data.RefreshZoning(blockIndex);
			if (this.m_zoning)
			{
				this.UsedZone(this.m_zone);
			}
		}
	}

	private void ApplyBrush()
	{
		float num = this.m_brushSize * 0.5f;
		Vector3 mousePosition = this.m_mousePosition;
		float num2 = mousePosition.x - num;
		float num3 = mousePosition.z - num;
		float num4 = mousePosition.x + num;
		float num5 = mousePosition.z + num;
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		int num6 = Mathf.Max((int)((num2 - 46f) / 64f + 75f), 0);
		int num7 = Mathf.Max((int)((num3 - 46f) / 64f + 75f), 0);
		int num8 = Mathf.Min((int)((num4 + 46f) / 64f + 75f), 149);
		int num9 = Mathf.Min((int)((num5 + 46f) / 64f + 75f), 149);
		for (int i = num7; i <= num9; i++)
		{
			for (int j = num6; j <= num8; j++)
			{
				ushort num10 = instance.m_zoneGrid[i * 150 + j];
				int num11 = 0;
				while (num10 != 0)
				{
					Vector3 position = instance.m_blocks.m_buffer[(int)num10].m_position;
					float num12 = Mathf.Max(Mathf.Max(num2 - 46f - position.x, num3 - 46f - position.z), Mathf.Max(position.x - num4 - 46f, position.z - num5 - 46f));
					if (num12 < 0f)
					{
						this.ApplyBrush(num10, ref instance.m_blocks.m_buffer[(int)num10], mousePosition, num);
					}
					num10 = instance.m_blocks.m_buffer[(int)num10].m_nextGridBlock;
					if (++num11 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public ZoneTool.Mode m_mode = ZoneTool.Mode.Fill;

	public ItemClass.Zone m_zone = ItemClass.Zone.ResidentialLow;

	public float m_brushSize = 50f;

	public CursorInfo[] m_zoneCursors;

	private Vector3 m_startPosition;

	private Vector3 m_startDirection;

	private Vector3 m_mousePosition;

	private Vector3 m_mouseDirection;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private Vector3 m_cameraDirection;

	private bool m_zoning;

	private bool m_dezoning;

	private bool m_validPosition;

	private ushort[] m_closeSegments;

	private int m_closeSegmentCount;

	private ulong[] m_fillBuffer1;

	private ulong[] m_fillBuffer2;

	private ulong[] m_fillBuffer3;

	private FastList<ZoneTool.FillPos> m_fillPositions;

	private bool m_mouseRayValid;

	private object m_dataLock;

	public enum Mode
	{
		Select,
		Brush,
		Fill
	}

	private struct FillPos
	{
		public byte m_x;

		public byte m_z;
	}
}
