﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using ColossalFramework.Threading;
using UnityEngine;

public class BuildingTool : ToolBase
{
	public event BuildingTool.RelocateCompleted m_relocateCompleted
	{
		add
		{
			BuildingTool.RelocateCompleted relocateCompleted = this.m_relocateCompleted;
			BuildingTool.RelocateCompleted relocateCompleted2;
			do
			{
				relocateCompleted2 = relocateCompleted;
				relocateCompleted = Interlocked.CompareExchange<BuildingTool.RelocateCompleted>(ref this.m_relocateCompleted, (BuildingTool.RelocateCompleted)Delegate.Combine(relocateCompleted2, value), relocateCompleted);
			}
			while (relocateCompleted != relocateCompleted2);
		}
		remove
		{
			BuildingTool.RelocateCompleted relocateCompleted = this.m_relocateCompleted;
			BuildingTool.RelocateCompleted relocateCompleted2;
			do
			{
				relocateCompleted2 = relocateCompleted;
				relocateCompleted = Interlocked.CompareExchange<BuildingTool.RelocateCompleted>(ref this.m_relocateCompleted, (BuildingTool.RelocateCompleted)Delegate.Remove(relocateCompleted2, value), relocateCompleted);
			}
			while (relocateCompleted != relocateCompleted2);
		}
	}

	public event BuildingTool.PlacementCompleted m_placementCompleted
	{
		add
		{
			BuildingTool.PlacementCompleted placementCompleted = this.m_placementCompleted;
			BuildingTool.PlacementCompleted placementCompleted2;
			do
			{
				placementCompleted2 = placementCompleted;
				placementCompleted = Interlocked.CompareExchange<BuildingTool.PlacementCompleted>(ref this.m_placementCompleted, (BuildingTool.PlacementCompleted)Delegate.Combine(placementCompleted2, value), placementCompleted);
			}
			while (placementCompleted != placementCompleted2);
		}
		remove
		{
			BuildingTool.PlacementCompleted placementCompleted = this.m_placementCompleted;
			BuildingTool.PlacementCompleted placementCompleted2;
			do
			{
				placementCompleted2 = placementCompleted;
				placementCompleted = Interlocked.CompareExchange<BuildingTool.PlacementCompleted>(ref this.m_placementCompleted, (BuildingTool.PlacementCompleted)Delegate.Remove(placementCompleted2, value), placementCompleted);
			}
			while (placementCompleted != placementCompleted2);
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_buildElevationUp = new SavedInputKey(Settings.buildElevationUp, Settings.gameSettingsFile, DefaultSettings.buildElevationUp, true);
		this.m_buildElevationDown = new SavedInputKey(Settings.buildElevationDown, Settings.gameSettingsFile, DefaultSettings.buildElevationDown, true);
		this.m_tempZoneBuffer = new HashSet<ushort>();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
	}

	protected override void OnToolGUI(Event e)
	{
		if (this.m_toolController.IsInsideUI)
		{
			return;
		}
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.CreateBuilding());
			}
			else if (e.get_button() == 1)
			{
				this.m_angleChanged = false;
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 1 && !this.m_angleChanged && buildingInfo != null && ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnSurface || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnTerrain || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnGround || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnWater || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround))
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleAngle());
			}
		}
		else if (this.m_buildElevationUp.IsPressed(e) || (e.get_type() == 4 && SteamController.GetDigitalActionDown(SteamController.DigitalInput.ElevationUp)))
		{
			Singleton<SimulationManager>.get_instance().AddAction<bool>(this.ChangeElevation(1));
		}
		else if (this.m_buildElevationDown.IsPressed(e) || (e.get_type() == 4 && SteamController.GetDigitalActionDown(SteamController.DigitalInput.ElevationDown)))
		{
			Singleton<SimulationManager>.get_instance().AddAction<bool>(this.ChangeElevation(-1));
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		base.ToolCursor = this.m_buildCursor;
		this.m_toolController.ClearColliding();
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		this.m_productionRate = 0;
		this.m_constructionCost = 0;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Singleton<CoverageManager>.get_instance().SetBuilding(null, new Vector3(-1000000f, -1000000f, -1000000f), 0f, 0);
		Singleton<TerrainManager>.get_instance().RenderZones = false;
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		this.m_productionRate = 0;
		this.m_constructionCost = 0;
		this.m_mouseRayValid = false;
	}

	public override void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (buildingInfo != null && this.m_placementErrors != ToolBase.ToolErrors.Pending && this.m_placementErrors != ToolBase.ToolErrors.RaycastFailed && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			Building building = default(Building);
			building.m_position = this.m_cachedPosition;
			building.m_angle = this.m_cachedAngle;
			this.m_toolController.RenderCollidingNotifications(cameraInfo, 0, 0);
			float elevation = this.GetElevation(buildingInfo);
			Color color = buildingInfo.m_buildingAI.GetColor(0, ref building, Singleton<InfoManager>.get_instance().CurrentMode);
			buildingInfo.m_buildingAI.RenderBuildGeometry(cameraInfo, this.m_cachedPosition, this.m_cachedAngle, elevation);
			BuildingTool.RenderGeometry(cameraInfo, buildingInfo, 0, this.m_cachedPosition, this.m_cachedAngle, true, color);
			if (buildingInfo.m_subBuildings != null && buildingInfo.m_subBuildings.Length != 0)
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(this.m_cachedPosition, Quaternion.AngleAxis(this.m_cachedAngle * 57.29578f, Vector3.get_down()), Vector3.get_one());
				for (int i = 0; i < buildingInfo.m_subBuildings.Length; i++)
				{
					BuildingInfo buildingInfo2 = buildingInfo.m_subBuildings[i].m_buildingInfo;
					Vector3 position = matrix4x.MultiplyPoint(buildingInfo.m_subBuildings[i].m_position);
					float angle = buildingInfo.m_subBuildings[i].m_angle * 0.0174532924f + this.m_cachedAngle;
					buildingInfo2.m_buildingAI.RenderBuildGeometry(cameraInfo, position, angle, elevation);
					BuildingTool.RenderGeometry(cameraInfo, buildingInfo2, 0, position, angle, true, color);
				}
			}
		}
		base.RenderGeometry(cameraInfo);
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (buildingInfo != null && this.m_placementErrors != ToolBase.ToolErrors.Pending && this.m_placementErrors != ToolBase.ToolErrors.RaycastFailed && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			Color toolColor = base.GetToolColor(false, this.m_placementErrors != ToolBase.ToolErrors.None);
			Color toolColor2 = base.GetToolColor(true, false);
			this.m_toolController.RenderColliding(cameraInfo, toolColor, toolColor2, toolColor, toolColor2, 0, 0);
			if (num != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingTool.RenderOverlay(cameraInfo, ref instance.m_buildings.m_buffer[num], toolColor2, toolColor2);
				ushort subBuilding = instance.m_buildings.m_buffer[num].m_subBuilding;
				int num2 = 0;
				while (subBuilding != 0)
				{
					BuildingTool.RenderOverlay(cameraInfo, ref instance.m_buildings.m_buffer[(int)subBuilding], toolColor2, toolColor2);
					subBuilding = instance.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
					if (++num2 > 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			buildingInfo.m_buildingAI.RenderBuildOverlay(cameraInfo, toolColor, this.m_cachedPosition, this.m_cachedAngle, this.m_cachedSegment);
			BuildingTool.RenderOverlay(cameraInfo, buildingInfo, 0, this.m_cachedPosition, this.m_cachedAngle, toolColor, true);
			if (buildingInfo.m_subBuildings != null && buildingInfo.m_subBuildings.Length != 0)
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(this.m_cachedPosition, Quaternion.AngleAxis(this.m_cachedAngle * 57.29578f, Vector3.get_down()), Vector3.get_one());
				for (int i = 0; i < buildingInfo.m_subBuildings.Length; i++)
				{
					BuildingInfo buildingInfo2 = buildingInfo.m_subBuildings[i].m_buildingInfo;
					Vector3 position = matrix4x.MultiplyPoint(buildingInfo.m_subBuildings[i].m_position);
					float angle = buildingInfo.m_subBuildings[i].m_angle * 0.0174532924f + this.m_cachedAngle;
					buildingInfo2.m_buildingAI.RenderBuildOverlay(cameraInfo, toolColor, position, angle, default(Segment3));
					BuildingTool.RenderOverlay(cameraInfo, buildingInfo2, 0, position, angle, toolColor, true);
				}
			}
		}
		base.RenderOverlay(cameraInfo);
	}

	public static void RenderGeometry(RenderManager.CameraInfo cameraInfo, BuildingInfo info, int length, Vector3 position, float angle, bool radius, Color color)
	{
		Building building = default(Building);
		building.m_position = position;
		building.m_angle = angle;
		if (info.m_mesh != null)
		{
			Quaternion quaternion = Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			instance.m_materialBlock.Clear();
			instance.m_materialBlock.SetVector(instance.ID_BuildingState, new Vector4(0f, 1000f, 0f, 100f));
			instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, RenderManager.DefaultColorLocation);
			instance.m_materialBlock.SetColor(instance.ID_Color, color);
			if (info.m_requireHeightMap)
			{
				Texture texture;
				Vector4 vector;
				Vector4 vector2;
				Singleton<TerrainManager>.get_instance().GetHeightMapping(position, out texture, out vector, out vector2);
				instance.m_materialBlock.SetTexture(instance.ID_HeightMap, texture);
				instance.m_materialBlock.SetVector(instance.ID_HeightMapping, vector);
				instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, vector2);
			}
			ToolManager expr_FF_cp_0 = Singleton<ToolManager>.get_instance();
			expr_FF_cp_0.m_drawCallData.m_defaultCalls = expr_FF_cp_0.m_drawCallData.m_defaultCalls + 1;
			Graphics.DrawMesh(info.m_mesh, position, quaternion, info.m_material, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
		}
		if (radius)
		{
			Vector3 vector3 = position;
			Vector3 vector4 = position;
			float num = 0f;
			float num2 = 0f;
			int num3 = 0;
			int num4 = 0;
			NaturalResourceManager.Resource resource;
			NaturalResourceManager.Resource resource2;
			info.m_buildingAI.GetNaturalResourceRadius(0, ref building, out resource, out vector3, out num, out resource2, out vector4, out num2);
			if (resource != NaturalResourceManager.Resource.None && num >= 1f)
			{
				if (resource != NaturalResourceManager.Resource.Water)
				{
					num += 38.4f * (1f + 19.2f / num);
				}
				num3 = 1 << (int)BuildingTool.GetIcon(resource);
			}
			else
			{
				num = 0f;
				vector3 = position;
			}
			if (resource2 != NaturalResourceManager.Resource.None && num2 >= 1f)
			{
				if (resource2 != NaturalResourceManager.Resource.Water)
				{
					num2 += 38.4f * (1f + 19.2f / num2);
				}
				if (num < 1f)
				{
					num = num2;
					num3 = 1 << (int)BuildingTool.GetIcon(resource2);
					vector3 = vector4;
					num2 = 0f;
					vector4 = position;
				}
				else if (Vector3.SqrMagnitude(vector4 - vector3) < 1f && Mathf.Abs(num2 - num) < 1f)
				{
					num3 |= 1 << (int)BuildingTool.GetIcon(resource2);
					num2 = 0f;
					vector4 = position;
				}
				else
				{
					num4 = 1 << (int)BuildingTool.GetIcon(resource2);
				}
			}
			else
			{
				num2 = 0f;
				vector4 = position;
			}
			ImmaterialResourceManager.Resource resource3;
			float num5;
			ImmaterialResourceManager.Resource resource4;
			float num6;
			info.m_buildingAI.GetImmaterialResourceRadius(0, ref building, out resource3, out num5, out resource4, out num6);
			if (resource3 != ImmaterialResourceManager.Resource.None && num5 >= 1f)
			{
				num5 += 38.4f * (1f + 19.2f / num5);
				if (num < 1f)
				{
					num = num5;
					num3 = 1 << (int)BuildingTool.GetIcon(resource3);
				}
				else if (Vector3.SqrMagnitude(position - vector3) < 1f && Mathf.Abs(num5 - num) < 1f)
				{
					num3 |= 1 << (int)BuildingTool.GetIcon(resource3);
				}
				else if (num2 < 1f)
				{
					num2 = num5;
					num4 = 1 << (int)BuildingTool.GetIcon(resource3);
				}
				else if (Vector3.SqrMagnitude(position - vector4) < 1f && Mathf.Abs(num5 - num2) < 1f)
				{
					num4 |= 1 << (int)BuildingTool.GetIcon(resource3);
				}
			}
			if (resource4 != ImmaterialResourceManager.Resource.None && num6 >= 1f)
			{
				num6 += 38.4f * (1f + 19.2f / num6);
				if (num < 1f)
				{
					num = num6;
					num3 = 1 << (int)BuildingTool.GetIcon(resource4);
				}
				else if (Vector3.SqrMagnitude(position - vector3) < 1f && Mathf.Abs(num6 - num) < 1f)
				{
					num3 |= 1 << (int)BuildingTool.GetIcon(resource4);
				}
				else if (num2 < 1f)
				{
					num2 = num6;
					num4 = 1 << (int)BuildingTool.GetIcon(resource4);
				}
				else if (Vector3.SqrMagnitude(position - vector4) < 1f && Mathf.Abs(num6 - num2) < 1f)
				{
					num4 |= 1 << (int)BuildingTool.GetIcon(resource4);
				}
			}
			if (num2 > num)
			{
				Vector3 vector5 = vector3;
				vector3 = vector4;
				vector4 = vector5;
				int num7 = num3;
				num3 = num4;
				num4 = num7;
				float num8 = num;
				num = num2;
				num2 = num8;
			}
			Vector3 vector6 = Vector3.Normalize(Vector3.Cross(Vector3.get_up(), cameraInfo.m_right));
			if (num >= 1f)
			{
				int num9 = 0;
				for (int i = 0; i < 32; i++)
				{
					if ((num3 & 1 << i) != 0)
					{
						num9++;
					}
				}
				Vector3 vector7;
				if (num2 >= 1f)
				{
					vector7 = vector3 + vector6 * ((num + num2) * 0.5f);
				}
				else
				{
					vector7 = vector3 + vector6 * (num * 0.65f);
				}
				vector7 -= cameraInfo.m_right * ((float)(num9 - 1) * 10f);
				for (int j = 0; j < 32; j++)
				{
					if ((num3 & 1 << j) != 0)
					{
						NotificationEvent.Type type = (NotificationEvent.Type)j;
						NotificationEvent.RenderInstance(cameraInfo, type, vector7, 1.5f, 1f);
						vector7 += cameraInfo.m_right * 20f;
					}
				}
			}
			if (num2 >= 1f)
			{
				int num10 = 0;
				for (int k = 0; k < 32; k++)
				{
					if ((num4 & 1 << k) != 0)
					{
						num10++;
					}
				}
				Vector3 vector8 = vector4 + vector6 * (num2 * 0.7f);
				vector8 -= cameraInfo.m_right * ((float)(num10 - 1) * 10f);
				for (int l = 0; l < 32; l++)
				{
					if ((num4 & 1 << l) != 0)
					{
						NotificationEvent.Type type2 = (NotificationEvent.Type)l;
						NotificationEvent.RenderInstance(cameraInfo, type2, vector8, 1.5f, 1f);
						vector8 += cameraInfo.m_right * 20f;
					}
				}
			}
		}
	}

	private static NotificationEvent.Type GetIcon(NaturalResourceManager.Resource resource)
	{
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return NotificationEvent.Type.NotificationRemoval;
		}
		return NotificationEvent.Type.Pollution;
	}

	private static NotificationEvent.Type GetIcon(ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return NotificationEvent.Type.NoisePollution;
		}
		if (resource != ImmaterialResourceManager.Resource.Entertainment)
		{
			return NotificationEvent.Type.NotificationRemoval;
		}
		return NotificationEvent.Type.Happy;
	}

	public static void RenderOverlay(RenderManager.CameraInfo cameraInfo, BuildingInfo info, int length, Vector3 position, float angle, Color color, bool radius)
	{
		if (info == null)
		{
			return;
		}
		if (length == 0)
		{
			length = info.m_cellLength;
		}
		if (info.m_cellWidth == 0 || length == 0)
		{
			return;
		}
		Building building = default(Building);
		building.m_position = position;
		building.m_angle = angle;
		if (info.m_circular)
		{
			ToolManager expr_5B_cp_0 = Singleton<ToolManager>.get_instance();
			expr_5B_cp_0.m_drawCallData.m_overlayCalls = expr_5B_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, (float)Mathf.Max(info.m_cellWidth, info.m_cellLength) * 8f, position.y - 100f, position.y + info.m_size.y + 100f, false, false);
		}
		else
		{
			Vector3 vector;
			vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
			Vector3 vector2;
			vector2..ctor(vector.z, 0f, -vector.x);
			vector *= (float)info.m_cellWidth * 4f;
			vector2 *= (float)length * 4f;
			Quad3 quad = default(Quad3);
			quad.a = position - vector - vector2;
			quad.b = position - vector + vector2;
			quad.c = position + vector + vector2;
			quad.d = position + vector - vector2;
			ToolManager expr_17A_cp_0 = Singleton<ToolManager>.get_instance();
			expr_17A_cp_0.m_drawCallData.m_overlayCalls = expr_17A_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, position.y - 100f, position.y + info.m_size.y + 100f, false, false);
		}
		if (radius)
		{
			Vector3 vector3 = position;
			Vector3 vector4 = position;
			float num = 0f;
			float num2 = 0f;
			Color color2 = Color.get_black();
			Color color3 = Color.get_black();
			NaturalResourceManager.Resource resource;
			NaturalResourceManager.Resource resource2;
			info.m_buildingAI.GetNaturalResourceRadius(0, ref building, out resource, out vector3, out num, out resource2, out vector4, out num2);
			if (resource != NaturalResourceManager.Resource.None && num >= 1f)
			{
				if (resource != NaturalResourceManager.Resource.Water)
				{
					num += 38.4f * (1f + 19.2f / num);
				}
				color2 = Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource];
			}
			else
			{
				num = 0f;
				vector3 = position;
			}
			if (resource2 != NaturalResourceManager.Resource.None && num2 >= 1f)
			{
				if (resource2 != NaturalResourceManager.Resource.Water)
				{
					num2 += 38.4f * (1f + 19.2f / num2);
				}
				if (num < 1f)
				{
					num = num2;
					color2 = Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource2];
					vector3 = vector4;
					num2 = 0f;
					vector4 = position;
				}
				else if (Vector3.SqrMagnitude(vector4 - vector3) < 1f && Mathf.Abs(num2 - num) < 1f)
				{
					color2 = (color2 + Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource2]) * 0.5f;
					num2 = 0f;
					vector4 = position;
				}
				else
				{
					color3 = Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource2];
				}
			}
			else
			{
				num2 = 0f;
				vector4 = position;
			}
			ImmaterialResourceManager.Resource resource3;
			float num3;
			ImmaterialResourceManager.Resource resource4;
			float num4;
			info.m_buildingAI.GetImmaterialResourceRadius(0, ref building, out resource3, out num3, out resource4, out num4);
			if (resource3 != ImmaterialResourceManager.Resource.None && num3 >= 1f)
			{
				num3 += 38.4f * (1f + 19.2f / num3);
				if (num < 1f)
				{
					num = num3;
					color2 = Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource3];
				}
				else if (Vector3.SqrMagnitude(position - vector3) < 1f && Mathf.Abs(num3 - num) < 1f)
				{
					color2 = (color2 + Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource3]) * 0.5f;
				}
				else if (num2 < 1f)
				{
					num2 = num3;
					color3 = Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource3];
				}
				else if (Vector3.SqrMagnitude(position - vector4) < 1f && Mathf.Abs(num3 - num2) < 1f)
				{
					color3 = (color3 + Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource3]) * 0.5f;
				}
			}
			if (resource4 != ImmaterialResourceManager.Resource.None && num4 >= 1f)
			{
				num4 += 38.4f * (1f + 19.2f / num4);
				if (num < 1f)
				{
					num = num4;
					color2 = Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource4];
				}
				else if (Vector3.SqrMagnitude(position - vector3) < 1f && Mathf.Abs(num4 - num) < 1f)
				{
					color2 = (color2 + Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource4]) * 0.5f;
				}
				else if (num2 < 1f)
				{
					num2 = num4;
					color3 = Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource4];
				}
				else if (Vector3.SqrMagnitude(position - vector4) < 1f && Mathf.Abs(num4 - num2) < 1f)
				{
					color3 = (color3 + Singleton<ImmaterialResourceManager>.get_instance().m_properties.m_resourceColors[(int)resource4]) * 0.5f;
				}
			}
			if (num2 > num)
			{
				Vector3 vector5 = vector3;
				vector3 = vector4;
				vector4 = vector5;
				Color color4 = color2;
				color2 = color3;
				color3 = color4;
				float num5 = num;
				num = num2;
				num2 = num5;
			}
			if (num >= 1f)
			{
				ToolManager expr_669_cp_0 = Singleton<ToolManager>.get_instance();
				expr_669_cp_0.m_drawCallData.m_overlayCalls = expr_669_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color2, vector3, num * 2f, -1f, 1025f, false, true);
			}
			if (num2 >= 1f)
			{
				ToolManager expr_6B4_cp_0 = Singleton<ToolManager>.get_instance();
				expr_6B4_cp_0.m_drawCallData.m_overlayCalls = expr_6B4_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color3, vector4, num2 * 2f, -1f, 1025f, false, true);
			}
		}
	}

	public static void RenderBulldozeNotification(RenderManager.CameraInfo cameraInfo, ref Building building)
	{
		BuildingInfo info = building.Info;
		if (info == null)
		{
			return;
		}
		if (!BuildingTool.IsImportantBuilding(info, ref building))
		{
			Vector3 position = building.m_position;
			position.y += info.m_size.y;
			NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.Bulldozer, position, 1f, 1f);
		}
	}

	public static void RenderOverlay(RenderManager.CameraInfo cameraInfo, ref Building building, Color importantColor, Color nonImportantColor)
	{
		BuildingInfo info = building.Info;
		if (info == null)
		{
			return;
		}
		Color color = (!BuildingTool.IsImportantBuilding(info, ref building)) ? nonImportantColor : importantColor;
		if (info.m_circular)
		{
			ToolManager expr_3D_cp_0 = Singleton<ToolManager>.get_instance();
			expr_3D_cp_0.m_drawCallData.m_overlayCalls = expr_3D_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, building.m_position, (float)Mathf.Max(info.m_cellWidth, building.Length) * 8f, building.m_position.y - 100f, building.m_position.y + info.m_size.y + 100f, false, false);
		}
		else
		{
			Vector3 vector;
			vector..ctor(Mathf.Cos(building.m_angle), 0f, Mathf.Sin(building.m_angle));
			Vector3 vector2;
			vector2..ctor(vector.z, 0f, -vector.x);
			vector *= (float)info.m_cellWidth * 4f;
			vector2 *= (float)building.Length * 4f;
			Quad3 quad = default(Quad3);
			quad.a = building.m_position - vector - vector2;
			quad.b = building.m_position - vector + vector2;
			quad.c = building.m_position + vector + vector2;
			quad.d = building.m_position + vector - vector2;
			ToolManager expr_189_cp_0 = Singleton<ToolManager>.get_instance();
			expr_189_cp_0.m_drawCallData.m_overlayCalls = expr_189_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, building.m_position.y - 100f, building.m_position.y + info.m_size.y + 100f, false, false);
		}
	}

	public static void CheckOverlayAlpha(ref Building building, ref float alpha)
	{
		BuildingInfo info = building.Info;
		if (info == null)
		{
			return;
		}
		BuildingTool.CheckOverlayAlpha(info, ref alpha);
	}

	public static void CheckOverlayAlpha(BuildingInfo info, ref float alpha)
	{
		if (info == null)
		{
			return;
		}
		float num = (float)Mathf.Min(info.m_cellWidth, info.m_cellLength) * 4f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num)));
	}

	protected override void OnToolUpdate()
	{
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (buildingInfo == null)
		{
			return;
		}
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				string text = null;
				if (num != 0)
				{
					int constructionCost = this.m_constructionCost;
					if (constructionCost != 0)
					{
						text = StringUtils.SafeFormat(Locale.Get("TOOL_RELOCATE_COST"), constructionCost / 100);
					}
				}
				else
				{
					int constructionCost2 = this.m_constructionCost;
					if (constructionCost2 != 0)
					{
						text = StringUtils.SafeFormat(Locale.Get("TOOL_CONSTRUCTION_COST"), constructionCost2 / 100);
					}
				}
				string constructionInfo = buildingInfo.m_buildingAI.GetConstructionInfo(this.m_productionRate);
				if (constructionInfo != null)
				{
					if (text != null)
					{
						text = text + "\n" + constructionInfo;
					}
					else
					{
						text = constructionInfo;
					}
				}
				base.ShowToolInfo(true, text, this.m_cachedPosition);
			}
			else
			{
				base.ShowToolInfo(true, null, this.m_cachedPosition);
			}
		}
		else
		{
			base.ShowToolInfo(false, null, this.m_cachedPosition);
		}
		if (((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnSurface || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnTerrain || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnGround || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnWater || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround) && Input.GetKey(324))
		{
			float axis = Input.GetAxis("Mouse X");
			if (axis != 0f)
			{
				this.m_angleChanged = true;
				Singleton<SimulationManager>.get_instance().AddAction(this.DeltaAngle(axis * 10f));
			}
		}
	}

	protected override void OnToolLateUpdate()
	{
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (buildingInfo == null)
		{
			return;
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		this.m_cachedSegment = this.m_connectionSegment;
		this.m_cachedPosition = this.m_mousePosition;
		this.m_cachedAngle = this.m_mouseAngle;
		InfoManager.InfoMode mode;
		InfoManager.SubInfoMode subMode;
		buildingInfo.m_buildingAI.GetPlacementInfoMode(out mode, out subMode, this.GetElevation(buildingInfo));
		ToolBase.ForceInfoMode(mode, subMode);
		if (this.m_placementErrors == ToolBase.ToolErrors.None && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			Singleton<CoverageManager>.get_instance().SetBuilding(buildingInfo, this.m_cachedPosition, this.m_cachedAngle, (ushort)this.m_relocate);
		}
		else
		{
			Singleton<CoverageManager>.get_instance().SetBuilding(buildingInfo, new Vector3(-1000000f, -1000000f, -1000000f), 0f, (ushort)this.m_relocate);
		}
		Singleton<TerrainManager>.get_instance().RenderZones = (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.Roadside);
	}

	public void CancelRelocate()
	{
		if (this.m_relocateCompleted != null)
		{
			this.m_relocateCompleted(InstanceID.Empty);
		}
	}

	[DebuggerHidden]
	private IEnumerator CreateBuilding()
	{
		BuildingTool.<CreateBuilding>c__Iterator0 <CreateBuilding>c__Iterator = new BuildingTool.<CreateBuilding>c__Iterator0();
		<CreateBuilding>c__Iterator.$this = this;
		return <CreateBuilding>c__Iterator;
	}

	private ushort CreateBuilding(BuildingInfo info, Vector3 position, float angle, int relocating, bool needMoney, bool fixedHeight)
	{
		ushort num = 0;
		if (needMoney)
		{
			int constructionCost = this.m_constructionCost;
			Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Construction, constructionCost, info.m_class);
		}
		bool flag = false;
		if (relocating != 0)
		{
			Singleton<BuildingManager>.get_instance().RelocateBuilding((ushort)relocating, position, angle);
			InstanceID id = default(InstanceID);
			id.Building = (ushort)relocating;
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_relocateCompleted != null)
				{
					this.m_relocateCompleted(id);
				}
			});
			num = (ushort)relocating;
			relocating = 0;
			this.m_relocate = 0;
			flag = true;
		}
		else if (info.m_buildingAI.WorksAsNet())
		{
			float elevation = this.GetElevation(info);
			Building building = default(Building);
			building.m_buildIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex;
			building.m_position = position;
			building.m_angle = angle;
			building.Width = info.m_cellWidth;
			building.Length = info.m_cellLength;
			BuildingDecoration.LoadPaths(info, 0, ref building, elevation);
			if (Mathf.Abs(elevation) < 1f)
			{
				BuildingDecoration.LoadProps(info, 0, ref building);
			}
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			flag = true;
		}
		else if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, position, angle, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			if (fixedHeight)
			{
				Building[] expr_171_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
				ushort expr_171_cp_1 = num;
				expr_171_cp_0[(int)expr_171_cp_1].m_flags = (expr_171_cp_0[(int)expr_171_cp_1].m_flags | Building.Flags.FixedHeight);
			}
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			flag = true;
		}
		if (flag)
		{
			if (info.m_notUsedGuide != null)
			{
				info.m_notUsedGuide.Disable();
			}
			info.m_buildingAI.PlacementSucceeded();
			Singleton<GuideManager>.get_instance().m_notEnoughMoney.Deactivate();
			int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
			if (publicServiceIndex != -1)
			{
				Singleton<GuideManager>.get_instance().m_serviceNotUsed[publicServiceIndex].Disable();
				Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Deactivate();
				Singleton<CoverageManager>.get_instance().CoverageUpdated(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			}
			BuildingTool.DispatchPlacementEffect(info, 0, position, angle, info.m_cellWidth, info.m_cellLength, false, false);
		}
		return num;
	}

	[DebuggerHidden]
	private IEnumerator DeltaAngle(float delta)
	{
		BuildingTool.<DeltaAngle>c__Iterator1 <DeltaAngle>c__Iterator = new BuildingTool.<DeltaAngle>c__Iterator1();
		<DeltaAngle>c__Iterator.delta = delta;
		<DeltaAngle>c__Iterator.$this = this;
		return <DeltaAngle>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator ToggleAngle()
	{
		BuildingTool.<ToggleAngle>c__Iterator2 <ToggleAngle>c__Iterator = new BuildingTool.<ToggleAngle>c__Iterator2();
		<ToggleAngle>c__Iterator.$this = this;
		return <ToggleAngle>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator<bool> ChangeElevation(int delta)
	{
		BuildingTool.<ChangeElevation>c__Iterator3 <ChangeElevation>c__Iterator = new BuildingTool.<ChangeElevation>c__Iterator3();
		<ChangeElevation>c__Iterator.delta = delta;
		<ChangeElevation>c__Iterator.$this = this;
		return <ChangeElevation>c__Iterator;
	}

	private float GetElevation(BuildingInfo info)
	{
		if (info == null)
		{
			return 0f;
		}
		int num;
		int num2;
		info.m_buildingAI.GetElevationLimits(out num, out num2);
		if (num == num2)
		{
			return 0f;
		}
		return (float)Mathf.Clamp(this.m_elevation, num, num2) * 12f;
	}

	public static void DispatchPlacementEffect(BuildingInfo info, ushort buildingID, Vector3 pos, float angle, int width, int length, bool bulldozing, bool collapsed)
	{
		EffectInfo effectInfo;
		if (bulldozing)
		{
			effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_bulldozeEffect;
		}
		else
		{
			effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_placementEffect;
		}
		if (effectInfo != null)
		{
			if (collapsed)
			{
				BuildingInfoBase collapsedInfo = info.m_collapsedInfo;
				if (collapsedInfo != null)
				{
					Randomizer randomizer;
					randomizer..ctor((int)buildingID);
					int num = randomizer.Int32(4u);
					if ((1 << num & info.m_collapsedRotations) == 0)
					{
						num = (num + 1 & 3);
					}
					Vector3 vector = info.m_generatedInfo.m_min;
					Vector3 vector2 = info.m_generatedInfo.m_max;
					float num2 = (float)width * 4f;
					float num3 = (float)length * 4f;
					float num4 = Building.CalculateLocalMeshOffset(info, length);
					vector = Vector3.Max(vector - new Vector3(4f, 0f, 4f + num4), new Vector3(-num2, 0f, -num3));
					vector2 = Vector3.Min(vector2 + new Vector3(4f, 0f, 4f - num4), new Vector3(num2, 0f, num3));
					Vector3 vector3 = (vector + vector2) * 0.5f;
					Vector3 vector4 = vector2 - vector;
					float num5 = (((num & 1) != 0) ? vector4.z : vector4.x) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
					float num6 = (((num & 1) != 0) ? vector4.x : vector4.z) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
					Quaternion quaternion = Quaternion.AngleAxis((float)num * 90f, Vector3.get_down());
					Matrix4x4 matrix4x = Matrix4x4.TRS(new Vector3(vector3.x, 0f, vector3.z + num4), quaternion, new Vector3(num5, 1f, num6));
					Vector3 vector5 = Building.CalculateMeshPosition(info, pos, angle, length);
					Quaternion quaternion2 = Building.CalculateMeshRotation(angle);
					Matrix4x4 matrix4x2 = Matrix4x4.TRS(vector5, quaternion2, Vector3.get_one());
					EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(matrix4x2 * matrix4x, collapsedInfo.m_lodMeshData);
					InstanceID instance = default(InstanceID);
					Singleton<EffectManager>.get_instance().DispatchEffect(effectInfo, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, true);
				}
			}
			else
			{
				Vector3 vector6 = Building.CalculateMeshPosition(info, pos, angle, length);
				Quaternion quaternion3 = Building.CalculateMeshRotation(angle);
				Matrix4x4 matrix = Matrix4x4.TRS(vector6, quaternion3, Vector3.get_one());
				EffectInfo.SpawnArea spawnArea2 = new EffectInfo.SpawnArea(matrix, info.m_lodMeshData);
				Singleton<EffectManager>.get_instance().DispatchEffect(effectInfo, spawnArea2, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
			}
		}
	}

	private void FindClosestZone(BuildingInfo info, ushort block, Vector3 refPos, ref float minD, ref float min2, ref Vector3 minPos, ref float minAngle)
	{
		if (block == 0)
		{
			return;
		}
		ZoneBlock zoneBlock = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)block];
		if (Mathf.Abs(zoneBlock.m_position.x - refPos.x) >= 52f || Mathf.Abs(zoneBlock.m_position.z - refPos.z) >= 52f)
		{
			return;
		}
		int rowCount = zoneBlock.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(zoneBlock.m_angle), 0f, Mathf.Sin(zoneBlock.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		for (int i = 0; i < rowCount; i++)
		{
			Vector3 vector3 = ((float)i - 3.5f) * vector2;
			int num = 0;
			while ((long)num < 4L)
			{
				if ((zoneBlock.m_valid & 1uL << (i << 3 | num)) != 0uL)
				{
					Vector3 vector4 = ((float)num - 3.5f) * vector;
					Vector3 vector5 = zoneBlock.m_position + vector4 + vector3;
					float num2 = Mathf.Sqrt((vector5.x - refPos.x) * (vector5.x - refPos.x) + (vector5.z - refPos.z) * (vector5.z - refPos.z));
					float num3 = Vector3.Dot(vector, refPos - zoneBlock.m_position);
					if (num2 <= minD - 0.2f || (num2 < minD + 0.2f && num3 < min2))
					{
						minD = num2;
						min2 = num3;
						if ((info.m_cellWidth & 1) == 0)
						{
							Vector3 vector6 = vector5 + vector2 * 0.5f;
							Vector3 vector7 = vector5 - vector2 * 0.5f;
							float num4 = (vector6.x - refPos.x) * (vector6.x - refPos.x) + (vector6.z - refPos.z) * (vector6.z - refPos.z);
							float num5 = (vector7.x - refPos.x) * (vector7.x - refPos.x) + (vector7.z - refPos.z) * (vector7.z - refPos.z);
							if (num4 < num5)
							{
								minPos = zoneBlock.m_position + ((float)info.m_cellLength * 0.5f - 4f) * vector + ((float)i - 3f) * vector2;
							}
							else
							{
								minPos = zoneBlock.m_position + ((float)info.m_cellLength * 0.5f - 4f) * vector + ((float)i - 4f) * vector2;
							}
						}
						else
						{
							minPos = zoneBlock.m_position + ((float)info.m_cellLength * 0.5f - 4f) * vector + ((float)i - 3.5f) * vector2;
						}
						minPos.y = refPos.y;
						minAngle = zoneBlock.m_angle + 1.57079637f;
					}
				}
				num++;
			}
		}
	}

	private static bool IsImportantBuilding(BuildingInfo info, ushort id)
	{
		return BuildingTool.IsImportantBuilding(info, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)id]);
	}

	private static bool IsImportantBuilding(BuildingInfo info, ref Building building)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
		return (publicServiceIndex != -1 && !info.m_autoRemove && (building.m_levelUpProgress != 255 || (building.m_flags & Building.Flags.Collapsed) == Building.Flags.None)) || (building.m_flags & Building.Flags.Untouchable) != Building.Flags.None || building.m_fireIntensity != 0;
	}

	public static bool CheckCollidingBuildings(BuildingInfo other, ulong[] buildingMask, ulong[] segmentMask)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num = buildingMask.Length;
		bool result = false;
		for (int i = 0; i < num; i++)
		{
			ulong num2 = buildingMask[i];
			if (num2 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num2 & 1uL << j) != 0uL)
					{
						int num3 = i << 6 | j;
						BuildingInfo info = instance.m_buildings.m_buffer[num3].Info;
						if (other != null && info.m_buildingAI.AllowOverlap(other) && other.m_buildingAI.AllowOverlap(info))
						{
							num2 &= ~(1uL << j);
						}
						else if ((instance.m_buildings.m_buffer[num3].m_flags & Building.Flags.Untouchable) != Building.Flags.None)
						{
							if (BuildingTool.CheckParentNode((ushort)num3, buildingMask, segmentMask))
							{
								result = true;
							}
						}
						else if (BuildingTool.IsImportantBuilding(info, (ushort)num3))
						{
							result = true;
						}
					}
				}
				buildingMask[i] = num2;
			}
		}
		return result;
	}

	private static bool CheckParentNode(ushort building, ulong[] buildingMask, ulong[] segmentMask)
	{
		ushort num = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].FindParentNode(building);
		NetManager instance = Singleton<NetManager>.get_instance();
		if (num == 0)
		{
			return true;
		}
		NetInfo info = instance.m_nodes.m_buffer[(int)num].Info;
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
		if ((publicServiceIndex != -1 && !info.m_autoRemove) || (instance.m_nodes.m_buffer[(int)num].m_flags & NetNode.Flags.Untouchable) != NetNode.Flags.None)
		{
			return true;
		}
		bool flag = false;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
			if (segment != 0 && (segmentMask[segment >> 6] & 1uL << (int)segment) == 0uL)
			{
				info = instance.m_segments.m_buffer[(int)segment].Info;
				publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
				if ((publicServiceIndex != -1 && !info.m_autoRemove) || (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
				{
					flag = true;
				}
				else
				{
					segmentMask[segment >> 6] |= 1uL << (int)segment;
				}
			}
		}
		if (!flag)
		{
			buildingMask[building >> 6] &= ~(1uL << (int)building);
		}
		return flag;
	}

	public static void ReleaseNonImportantBuildings(ulong[] buildingMask)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num = buildingMask.Length;
		for (int i = 0; i < num; i++)
		{
			ulong num2 = buildingMask[i];
			if (num2 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num2 & 1uL << j) != 0uL)
					{
						int num3 = i << 6 | j;
						BuildingInfo info = instance.m_buildings.m_buffer[num3].Info;
						if (!BuildingTool.IsImportantBuilding(info, (ushort)num3))
						{
							instance.ReleaseBuilding((ushort)num3);
							buildingMask[i] &= ~(1uL << j);
						}
					}
				}
			}
		}
	}

	public static ToolBase.ToolErrors CheckSpace(BuildingInfo info, BuildingInfo.PlacementMode placementMode, int relocating, Vector3 pos, float minY, float maxY, float angle, int width, int length, bool test, ulong[] collidingSegmentBuffer, ulong[] collidingBuildingBuffer)
	{
		ToolBase.ToolErrors toolErrors = BuildingTool.CheckSpaceImpl(info, placementMode, relocating, pos, minY, maxY, angle, width, length, test, collidingSegmentBuffer, collidingBuildingBuffer);
		if (info.m_subBuildings != null && info.m_subBuildings.Length != 0)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(pos, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
			for (int i = 0; i < info.m_subBuildings.Length; i++)
			{
				BuildingInfo buildingInfo = info.m_subBuildings[i].m_buildingInfo;
				Vector3 vector = matrix4x.MultiplyPoint(info.m_subBuildings[i].m_position);
				float angle2 = info.m_subBuildings[i].m_angle * 0.0174532924f + angle;
				float minY2;
				float maxY2;
				float num;
				Building.SampleBuildingHeight(vector, angle2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out minY2, out maxY2, out num);
				toolErrors |= BuildingTool.CheckSpaceImpl(buildingInfo, placementMode, relocating, vector, minY2, maxY2, angle2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, test, collidingSegmentBuffer, collidingBuildingBuffer);
			}
		}
		if (!test)
		{
			NetTool.ReleaseNonImportantSegments(collidingSegmentBuffer);
			BuildingTool.ReleaseNonImportantBuildings(collidingBuildingBuffer);
		}
		return toolErrors;
	}

	private static ToolBase.ToolErrors CheckSpaceImpl(BuildingInfo info, BuildingInfo.PlacementMode placementMode, int relocating, Vector3 pos, float minY, float maxY, float angle, int width, int length, bool test, ulong[] collidingSegmentBuffer, ulong[] collidingBuildingBuffer)
	{
		if (info.IgnoreBuildingCollision())
		{
			return ToolBase.ToolErrors.None;
		}
		Vector2 vector;
		vector..ctor(Mathf.Cos(angle), Mathf.Sin(angle));
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			vector *= (float)width * 4f - 0.8f;
			vector2 *= (float)length * 4f - 0.8f;
		}
		else
		{
			vector *= (float)width * 4f;
			vector2 *= (float)length * 4f;
		}
		if (info.m_circular)
		{
			vector *= 0.7f;
			vector2 *= 0.7f;
		}
		Vector2 vector3 = VectorUtils.XZ(pos);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - vector - vector2;
		quad.b = vector3 + vector - vector2;
		quad.c = vector3 + vector + vector2;
		quad.d = vector3 - vector + vector2;
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		ItemClass.CollisionType collisionType = info.m_buildingAI.GetCollisionType();
		float num;
		float num2;
		if (info.m_buildingAI.GetWaterStructureCollisionRange(out num, out num2))
		{
			if (num >= num2)
			{
				instance2.OverlapQuad(quad, pos.y + 4f, maxY, ItemClass.CollisionType.Elevated, ItemClass.Layer.WaterStructures, 0, 0, 0, collidingSegmentBuffer);
			}
			else
			{
				if (num > 0f)
				{
					Quad2 quad2 = quad;
					quad2.a = Vector2.Lerp(quad.a, quad.d, 0f);
					quad2.b = Vector2.Lerp(quad.b, quad.c, 0f);
					quad2.c = Vector2.Lerp(quad.b, quad.c, num);
					quad2.d = Vector2.Lerp(quad.a, quad.d, num);
					instance2.OverlapQuad(quad2, pos.y + 4f, maxY, ItemClass.CollisionType.Elevated, ItemClass.Layer.WaterStructures, 0, 0, 0, collidingSegmentBuffer);
				}
				if (num2 > num)
				{
					Quad2 quad3 = quad;
					quad3.a = Vector2.Lerp(quad.a, quad.d, num);
					quad3.b = Vector2.Lerp(quad.b, quad.c, num);
					quad3.c = Vector2.Lerp(quad.b, quad.c, num2);
					quad3.d = Vector2.Lerp(quad.a, quad.d, num2);
					instance2.OverlapQuad(quad3, minY, maxY, collisionType, ItemClass.Layer.WaterStructures, 0, 0, 0, collidingSegmentBuffer);
				}
				if (num2 < 1f)
				{
					Quad2 quad4 = quad;
					quad4.a = Vector2.Lerp(quad.a, quad.d, num2);
					quad4.b = Vector2.Lerp(quad.b, quad.c, num2);
					quad4.c = Vector2.Lerp(quad.b, quad.c, 1f);
					quad4.d = Vector2.Lerp(quad.a, quad.d, 1f);
					instance2.OverlapQuad(quad4, pos.y + 4f, maxY, ItemClass.CollisionType.Elevated, ItemClass.Layer.WaterStructures, 0, 0, 0, collidingSegmentBuffer);
				}
			}
			instance2.OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, ItemClass.Layer.WaterStructures, 0, 0, 0, collidingSegmentBuffer);
		}
		else
		{
			instance2.OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, 0, 0, 0, collidingSegmentBuffer);
		}
		instance.OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, (ushort)relocating, 0, 0, collidingBuildingBuffer);
		BuildingTool.IgnoreRelocateSegments((ushort)relocating, collidingSegmentBuffer, collidingBuildingBuffer);
		if (NetTool.CheckCollidingSegments(collidingSegmentBuffer, collidingBuildingBuffer, 0))
		{
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if (BuildingTool.CheckCollidingBuildings(info, collidingBuildingBuffer, collidingSegmentBuffer))
		{
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if (Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad))
		{
			toolErrors |= ToolBase.ToolErrors.OutOfArea;
		}
		if (info.m_placementMode == BuildingInfo.PlacementMode.OnGround || info.m_placementMode == BuildingInfo.PlacementMode.Roadside || (info.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround && placementMode == BuildingInfo.PlacementMode.OnGround))
		{
			if (Singleton<TerrainManager>.get_instance().HasWater(vector3))
			{
				toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
			}
			if (!info.m_circular)
			{
				if (Singleton<TerrainManager>.get_instance().HasWater(quad.a))
				{
					toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
				}
				if (Singleton<TerrainManager>.get_instance().HasWater(quad.b))
				{
					toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
				}
				if (Singleton<TerrainManager>.get_instance().HasWater(quad.c))
				{
					toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
				}
				if (Singleton<TerrainManager>.get_instance().HasWater(quad.d))
				{
					toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
				}
			}
		}
		else if (info.m_placementMode == BuildingInfo.PlacementMode.OnWater)
		{
			if (!Singleton<TerrainManager>.get_instance().HasWater(vector3))
			{
				toolErrors |= ToolBase.ToolErrors.WaterNotFound;
			}
			if (!info.m_circular)
			{
				if (!Singleton<TerrainManager>.get_instance().HasWater(quad.a))
				{
					toolErrors |= ToolBase.ToolErrors.WaterNotFound;
				}
				if (!Singleton<TerrainManager>.get_instance().HasWater(quad.b))
				{
					toolErrors |= ToolBase.ToolErrors.WaterNotFound;
				}
				if (!Singleton<TerrainManager>.get_instance().HasWater(quad.c))
				{
					toolErrors |= ToolBase.ToolErrors.WaterNotFound;
				}
				if (!Singleton<TerrainManager>.get_instance().HasWater(quad.d))
				{
					toolErrors |= ToolBase.ToolErrors.WaterNotFound;
				}
			}
		}
		return toolErrors;
	}

	public static void IgnoreRelocateSegments(ushort relocateID, ulong[] collidingSegmentBuffer, ulong[] collidingBuildingBuffer)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		int num = 0;
		while (relocateID != 0)
		{
			if (collidingSegmentBuffer != null)
			{
				ushort num2 = instance.m_buildings.m_buffer[(int)relocateID].m_netNode;
				int num3 = 0;
				while (num2 != 0)
				{
					for (int i = 0; i < 8; i++)
					{
						ushort segment = instance2.m_nodes.m_buffer[(int)num2].GetSegment(i);
						if (segment != 0 && (instance2.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
						{
							collidingSegmentBuffer[segment >> 6] &= ~(1uL << (int)segment);
						}
					}
					num2 = instance2.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
					if (++num3 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (collidingBuildingBuffer != null)
			{
				ushort num4 = instance.m_buildings.m_buffer[(int)relocateID].m_netNode;
				int num5 = 0;
				while (num4 != 0)
				{
					ushort building = instance2.m_nodes.m_buffer[(int)num4].m_building;
					if (building != 0)
					{
						collidingBuildingBuffer[building >> 6] &= ~(1uL << (int)building);
					}
					num4 = instance2.m_nodes.m_buffer[(int)num4].m_nextBuildingNode;
					if (++num5 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				collidingBuildingBuffer[relocateID >> 6] &= ~(1uL << (int)relocateID);
			}
			relocateID = instance.m_buildings.m_buffer[(int)relocateID].m_subBuilding;
			if (++num > 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private void GetAllZoneBlocks(ushort buildingID, HashSet<ushort> buffer)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		int num = 0;
		while (buildingID != 0)
		{
			ushort num2 = instance.m_buildings.m_buffer[(int)buildingID].m_netNode;
			int num3 = 0;
			while (num2 != 0)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance2.m_nodes.m_buffer[(int)num2].GetSegment(i);
					if (segment != 0 && instance2.m_segments.m_buffer[(int)segment].m_startNode == num2 && (instance2.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
					{
						ushort blockStartLeft = instance2.m_segments.m_buffer[(int)segment].m_blockStartLeft;
						ushort blockStartRight = instance2.m_segments.m_buffer[(int)segment].m_blockStartRight;
						ushort blockEndLeft = instance2.m_segments.m_buffer[(int)segment].m_blockEndLeft;
						ushort blockEndRight = instance2.m_segments.m_buffer[(int)segment].m_blockEndRight;
						if (blockStartLeft != 0)
						{
							buffer.Add(blockStartLeft);
						}
						if (blockStartRight != 0)
						{
							buffer.Add(blockStartRight);
						}
						if (blockEndLeft != 0)
						{
							buffer.Add(blockEndLeft);
						}
						if (blockEndRight != 0)
						{
							buffer.Add(blockEndRight);
						}
					}
				}
				num2 = instance2.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
				if (++num3 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			buildingID = instance.m_buildings.m_buffer[(int)buildingID].m_subBuilding;
			if (++num > 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public override void SimulationStep()
	{
		BuildingInfo buildingInfo;
		int num;
		this.GetPrefabInfo(out buildingInfo, out num);
		if (buildingInfo == null)
		{
			return;
		}
		ulong[] collidingSegmentBuffer;
		ulong[] collidingBuildingBuffer;
		this.m_toolController.BeginColliding(out collidingSegmentBuffer, out collidingBuildingBuffer);
		try
		{
			ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
			ToolBase.RaycastOutput raycastOutput;
			if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
			{
				Vector3 vector = raycastOutput.m_hitPos;
				float num2 = this.m_mouseAngle;
				bool flag = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None;
				float num3 = 0f;
				Segment3 connectionSegment = default(Segment3);
				float elevation = this.GetElevation(buildingInfo);
				ToolBase.ToolErrors toolErrors;
				int productionRate;
				int num32;
				if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.Roadside)
				{
					toolErrors = ToolBase.ToolErrors.GridNotFound;
					float num4 = raycastOutput.m_hitPos.x - 8f;
					float num5 = raycastOutput.m_hitPos.z - 8f;
					float num6 = raycastOutput.m_hitPos.x + 8f;
					float num7 = raycastOutput.m_hitPos.z + 8f;
					ZoneManager instance = Singleton<ZoneManager>.get_instance();
					float num8 = 8f;
					float num9 = 1000000f;
					int num10 = Mathf.Max((int)((num4 - 46f) / 64f + 75f), 0);
					int num11 = Mathf.Max((int)((num5 - 46f) / 64f + 75f), 0);
					int num12 = Mathf.Min((int)((num6 + 46f) / 64f + 75f), 149);
					int num13 = Mathf.Min((int)((num7 + 46f) / 64f + 75f), 149);
					this.m_tempZoneBuffer.Clear();
					if (num != 0)
					{
						this.GetAllZoneBlocks((ushort)num, this.m_tempZoneBuffer);
					}
					for (int i = num11; i <= num13; i++)
					{
						for (int j = num10; j <= num12; j++)
						{
							ushort num14 = instance.m_zoneGrid[i * 150 + j];
							int num15 = 0;
							while (num14 != 0)
							{
								if (!this.m_tempZoneBuffer.Contains(num14))
								{
									Vector3 position = instance.m_blocks.m_buffer[(int)num14].m_position;
									float num16 = Mathf.Max(Mathf.Max(num4 - 46f - position.x, num5 - 46f - position.z), Mathf.Max(position.x - num6 - 46f, position.z - num7 - 46f));
									if (num16 < 0f)
									{
										this.FindClosestZone(buildingInfo, num14, raycastOutput.m_hitPos, ref num8, ref num9, ref vector, ref num2);
									}
								}
								num14 = instance.m_blocks.m_buffer[(int)num14].m_nextGridBlock;
								if (++num15 >= 49152)
								{
									CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
									break;
								}
							}
						}
					}
					if (num8 < 8f)
					{
						int num17;
						if (Singleton<ZoneManager>.get_instance().CheckSpace(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, out num17))
						{
							float num18;
							float num19;
							float num20;
							Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num18, out num19, out num20);
							ToolBase.ToolErrors toolErrors2 = BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector, num18, num20 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
							if (num19 - num18 > buildingInfo.m_maxHeightOffset)
							{
								toolErrors2 |= ToolBase.ToolErrors.SlopeTooSteep;
							}
							if (toolErrors2 == ToolBase.ToolErrors.None)
							{
								vector.y = num20;
							}
							toolErrors = toolErrors2;
						}
						else if (num17 < 0)
						{
							Vector3 vector2 = new Vector3(Mathf.Cos(num2), 0f, Mathf.Sin(num2)) * 8f;
							int num21 = buildingInfo.m_cellWidth >> 1;
							for (int k = 1; k <= num21; k++)
							{
								Vector3 vector3 = vector - vector2 * (float)k;
								if (Singleton<ZoneManager>.get_instance().CheckSpace(vector3, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, out num17))
								{
									float num22;
									float num23;
									float num24;
									Building.SampleBuildingHeight(vector3, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num22, out num23, out num24);
									ToolBase.ToolErrors toolErrors3 = BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector3, num22, num24 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
									if (num23 - num22 > buildingInfo.m_maxHeightOffset)
									{
										toolErrors3 |= ToolBase.ToolErrors.SlopeTooSteep;
									}
									if (toolErrors3 == ToolBase.ToolErrors.None)
									{
										vector3.y = num24;
										vector = vector3;
									}
									toolErrors = toolErrors3;
									break;
								}
							}
						}
						else if (num17 > 0)
						{
							Vector3 vector4 = new Vector3(Mathf.Cos(num2), 0f, Mathf.Sin(num2)) * 8f;
							int num25 = buildingInfo.m_cellWidth >> 1;
							for (int l = 1; l <= num25; l++)
							{
								Vector3 vector5 = vector + vector4 * (float)l;
								if (Singleton<ZoneManager>.get_instance().CheckSpace(vector5, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, out num17))
								{
									float num26;
									float num27;
									float num28;
									Building.SampleBuildingHeight(vector5, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num26, out num27, out num28);
									ToolBase.ToolErrors toolErrors4 = BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector5, num26, num28 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
									if (num27 - num26 > buildingInfo.m_maxHeightOffset)
									{
										toolErrors4 |= ToolBase.ToolErrors.SlopeTooSteep;
									}
									if (toolErrors4 == ToolBase.ToolErrors.None)
									{
										vector5.y = num28;
										vector = vector5;
									}
									toolErrors = toolErrors4;
									break;
								}
							}
						}
						if (toolErrors != ToolBase.ToolErrors.None)
						{
							float num29;
							float num30;
							float num31;
							Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num29, out num30, out num31);
							this.m_toolController.ResetColliding();
							toolErrors = BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector, num29, num31 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
							if (num30 - num29 > buildingInfo.m_maxHeightOffset)
							{
								toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
							}
							vector.y = num31;
						}
					}
					toolErrors |= buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
				}
				else if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.Shoreline || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround)
				{
					toolErrors = ToolBase.ToolErrors.ShoreNotFound;
					Vector3 vector6;
					Vector3 vector7;
					bool flag3;
					bool flag2 = BuildingTool.SnapToCanal(vector, out vector6, out vector7, out flag3, 40f, false);
					Vector3 vector8;
					Vector3 vector9;
					bool shorePos = Singleton<TerrainManager>.get_instance().GetShorePos(vector6, 50f, out vector8, out vector9, out num3);
					if (flag2)
					{
						vector = vector6;
						num2 = Mathf.Atan2(vector7.x, -vector7.z);
						float num33 = Mathf.Max(0f, vector6.y);
						float num34;
						float num35;
						float num36;
						Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num34, out num35, out num36, ref num33);
						num34 -= 20f;
						num36 = Mathf.Max(vector.y, num36);
						float y = vector.y;
						vector.y = num36;
						toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
						toolErrors |= BuildingTool.CheckSpace(buildingInfo, BuildingInfo.PlacementMode.Shoreline, num, vector, num34, num36 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
						if (y - num34 > 128f)
						{
							toolErrors |= ToolBase.ToolErrors.HeightTooHigh;
						}
					}
					else if (shorePos)
					{
						vector = vector8;
						if (Singleton<TerrainManager>.get_instance().GetShorePos(vector, 50f, out vector8, out vector9, out num3))
						{
							vector8 += vector9.get_normalized() * buildingInfo.m_placementOffset;
							vector = vector8;
							num2 = Mathf.Atan2(vector9.x, -vector9.z);
							float num37;
							float num38;
							float num39;
							Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num37, out num38, out num39);
							num37 = Mathf.Min(num3, num37);
							num39 = Mathf.Max(vector.y, num39);
							float y2 = vector.y;
							vector.y = num39;
							toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
							toolErrors |= BuildingTool.CheckSpace(buildingInfo, BuildingInfo.PlacementMode.Shoreline, num, vector, num37, num39 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
							if (y2 - num3 > 128f)
							{
								toolErrors |= ToolBase.ToolErrors.HeightTooHigh;
							}
							if (num39 <= num3)
							{
								toolErrors = ((toolErrors & ~(ToolBase.ToolErrors.HeightTooHigh | ToolBase.ToolErrors.CannotConnect | ToolBase.ToolErrors.CannotBuildOnWater)) | ToolBase.ToolErrors.ShoreNotFound);
							}
						}
						else
						{
							toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
							toolErrors = ((toolErrors & ~(ToolBase.ToolErrors.HeightTooHigh | ToolBase.ToolErrors.CannotConnect | ToolBase.ToolErrors.CannotBuildOnWater)) | ToolBase.ToolErrors.ShoreNotFound);
						}
					}
					else if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround)
					{
						Quaternion quaternion = Quaternion.AngleAxis(this.m_angle, Vector3.get_down());
						vector -= quaternion * buildingInfo.m_centerOffset;
						num2 = this.m_angle * 0.0174532924f;
						float num40;
						float num41;
						float num42;
						Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num40, out num41, out num42);
						vector.y = num42;
						toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
						toolErrors |= BuildingTool.CheckSpace(buildingInfo, BuildingInfo.PlacementMode.OnGround, num, vector, num40, num42 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
						if ((toolErrors & ToolBase.ToolErrors.CannotBuildOnWater) == ToolBase.ToolErrors.None && num41 - num40 > buildingInfo.m_maxHeightOffset)
						{
							toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
						}
					}
					else
					{
						toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
						toolErrors = ((toolErrors & ~(ToolBase.ToolErrors.HeightTooHigh | ToolBase.ToolErrors.CannotConnect | ToolBase.ToolErrors.CannotBuildOnWater)) | ToolBase.ToolErrors.ShoreNotFound);
					}
				}
				else if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnSurface || buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnTerrain)
				{
					Quaternion quaternion2 = Quaternion.AngleAxis(this.m_angle, Vector3.get_down());
					vector -= quaternion2 * buildingInfo.m_centerOffset;
					num2 = this.m_angle * 0.0174532924f;
					float minY;
					float num43;
					float num44;
					Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out minY, out num43, out num44);
					vector.y = num44;
					toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
					toolErrors |= BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector, minY, num44 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
				}
				else if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnGround)
				{
					Quaternion quaternion3 = Quaternion.AngleAxis(this.m_angle, Vector3.get_down());
					vector -= quaternion3 * buildingInfo.m_centerOffset;
					num2 = this.m_angle * 0.0174532924f;
					float num45;
					float num46;
					float num47;
					Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out num45, out num46, out num47);
					vector.y = num47;
					toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
					toolErrors |= BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector, num45, num47 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
					if (num46 - num45 > buildingInfo.m_maxHeightOffset)
					{
						toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
					}
				}
				else if (buildingInfo.m_placementMode == BuildingInfo.PlacementMode.OnWater)
				{
					Quaternion quaternion4 = Quaternion.AngleAxis(this.m_angle, Vector3.get_down());
					vector -= quaternion4 * buildingInfo.m_centerOffset;
					num2 = this.m_angle * 0.0174532924f;
					float minY2;
					float num48;
					float num49;
					Building.SampleBuildingHeight(vector, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, buildingInfo, out minY2, out num48, out num49);
					vector.y = num49;
					toolErrors = buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
					toolErrors |= BuildingTool.CheckSpace(buildingInfo, buildingInfo.m_placementMode, num, vector, minY2, num49 + buildingInfo.m_collisionHeight, num2, buildingInfo.m_cellWidth, buildingInfo.m_cellLength, true, collidingSegmentBuffer, collidingBuildingBuffer);
				}
				else
				{
					toolErrors = ToolBase.ToolErrors.Pending;
					toolErrors |= buildingInfo.m_buildingAI.CheckBuildPosition((ushort)num, ref vector, ref num2, num3, elevation, ref connectionSegment, out productionRate, out num32);
				}
				if (buildingInfo.m_subBuildings != null && buildingInfo.m_subBuildings.Length != 0)
				{
					Matrix4x4 matrix4x = default(Matrix4x4);
					matrix4x.SetTRS(vector, Quaternion.AngleAxis(num2 * 57.29578f, Vector3.get_down()), Vector3.get_one());
					for (int m = 0; m < buildingInfo.m_subBuildings.Length; m++)
					{
						BuildingInfo buildingInfo2 = buildingInfo.m_subBuildings[m].m_buildingInfo;
						Vector3 vector10 = matrix4x.MultiplyPoint(buildingInfo.m_subBuildings[m].m_position);
						float num50 = buildingInfo.m_subBuildings[m].m_angle * 0.0174532924f + num2;
						Segment3 segment = default(Segment3);
						int num51;
						int num52;
						toolErrors |= buildingInfo2.m_buildingAI.CheckBuildPosition((ushort)num, ref vector10, ref num50, num3, elevation, ref segment, out num51, out num52);
						num32 += num52;
					}
				}
				if (flag && Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, num32) != num32)
				{
					toolErrors |= ToolBase.ToolErrors.NotEnoughMoney;
				}
				if (!Singleton<BuildingManager>.get_instance().CheckLimits())
				{
					toolErrors |= ToolBase.ToolErrors.TooManyObjects;
				}
				if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None && this.m_toolController.m_editPrefabInfo != null)
				{
					BuildingInfo buildingInfo3 = this.m_toolController.m_editPrefabInfo as BuildingInfo;
					if (buildingInfo3 != null && buildingInfo3.m_buildingAI != null && !(buildingInfo3.m_buildingAI is IntersectionAI))
					{
						toolErrors = ToolBase.ToolErrors.None;
					}
				}
				this.m_mousePosition = vector;
				this.m_mouseAngle = (((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None) ? (this.m_angle * 0.0174532924f) : num2);
				this.m_connectionSegment = connectionSegment;
				this.m_productionRate = productionRate;
				this.m_constructionCost = num32;
				this.m_placementErrors = toolErrors;
			}
			else
			{
				this.m_placementErrors = ToolBase.ToolErrors.RaycastFailed;
				this.m_connectionSegment = default(Segment3);
			}
		}
		finally
		{
			this.m_toolController.EndColliding();
		}
	}

	public static bool SnapToCanal(Vector3 refPos, out Vector3 pos, out Vector3 dir, out bool isQuay, float maxDistance, bool center)
	{
		bool result = false;
		pos = refPos;
		dir = Vector3.get_forward();
		isQuay = false;
		float num = refPos.x - maxDistance - 100f;
		float num2 = refPos.z - maxDistance - 100f;
		float num3 = refPos.x + maxDistance + 100f;
		float num4 = refPos.z + maxDistance + 100f;
		int num5 = Mathf.Max((int)(num / 64f + 135f), 0);
		int num6 = Mathf.Max((int)(num2 / 64f + 135f), 0);
		int num7 = Mathf.Min((int)(num3 / 64f + 135f), 269);
		int num8 = Mathf.Min((int)(num4 / 64f + 135f), 269);
		Array16<NetSegment> segments = Singleton<NetManager>.get_instance().m_segments;
		ushort[] segmentGrid = Singleton<NetManager>.get_instance().m_segmentGrid;
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = segmentGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					NetSegment.Flags flags = segments.m_buffer[(int)num9].m_flags;
					if ((flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created)
					{
						NetInfo info = segments.m_buffer[(int)num9].Info;
						if (info.m_class.m_service == ItemClass.Service.Beautification && info.m_class.m_level >= ItemClass.Level.Level4)
						{
							Vector3 min = segments.m_buffer[(int)num9].m_bounds.get_min();
							Vector3 max = segments.m_buffer[(int)num9].m_bounds.get_max();
							if (min.x < num3 && min.z < num4 && max.x > num && max.z > num2)
							{
								Vector3 vector;
								Vector3 vector2;
								segments.m_buffer[(int)num9].GetClosestPositionAndDirection(refPos, out vector, out vector2);
								float num11 = Vector3.Distance(vector, refPos) - info.m_halfWidth;
								if (num11 < maxDistance)
								{
									Vector3 vector3;
									vector3..ctor(vector2.z, 0f, -vector2.x);
									dir = vector3.get_normalized();
									if (info.m_class.m_level == ItemClass.Level.Level4)
									{
										if ((segments.m_buffer[(int)num9].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None)
										{
											dir = -dir;
										}
										if (center)
										{
											pos = vector + dir * 25f;
										}
										else
										{
											pos = vector + dir * 3f;
										}
										isQuay = true;
									}
									else
									{
										if (Vector3.Dot(vector - refPos, dir) < 0f)
										{
											dir = -dir;
										}
										if (center)
										{
											pos = vector - dir * Mathf.Max(0f, info.m_halfWidth - 24f);
										}
										else
										{
											pos = vector - dir * info.m_halfWidth;
										}
										isQuay = false;
									}
									maxDistance = num11;
									result = true;
								}
							}
						}
					}
					num9 = segments.m_buffer[(int)num9].m_nextGridSegment;
					if (++num10 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_placementErrors;
	}

	private void GetPrefabInfo(out BuildingInfo info, out int relocating)
	{
		info = this.m_prefab;
		relocating = this.m_relocate;
		if (info != null)
		{
			relocating = 0;
			return;
		}
		if (relocating != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[relocating].m_flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created)
			{
				info = instance.m_buildings.m_buffer[relocating].Info;
				return;
			}
		}
		info = null;
		relocating = 0;
	}

	public BuildingInfo m_prefab;

	public float m_angle;

	public int m_relocate;

	public CursorInfo m_buildCursor;

	private Segment3 m_connectionSegment;

	private Vector3 m_mousePosition;

	private float m_mouseAngle;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private ToolBase.ToolErrors m_placementErrors;

	private Segment3 m_cachedSegment;

	private Vector3 m_cachedPosition;

	private float m_cachedAngle;

	private int m_productionRate;

	private int m_constructionCost;

	private int m_elevation;

	private bool m_angleChanged;

	private bool m_mouseRayValid;

	private HashSet<ushort> m_tempZoneBuffer;

	private SavedInputKey m_buildElevationUp;

	private SavedInputKey m_buildElevationDown;

	public delegate void RelocateCompleted(InstanceID newID);

	public delegate void PlacementCompleted(ushort building);
}
