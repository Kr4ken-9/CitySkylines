﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TramAI : TramBaseAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_arriveEffect != null)
		{
			this.m_arriveEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_arriveEffect != null)
		{
			this.m_arriveEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (data.m_leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort firstVehicle = data.GetFirstVehicle(vehicleID);
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)firstVehicle].Info;
			return info.m_vehicleAI.GetColor(firstVehicle, ref instance.m_vehicles.m_buffer[(int)firstVehicle], infoMode);
		}
		if (infoMode == InfoManager.InfoMode.Transport || infoMode == InfoManager.InfoMode.None)
		{
			ushort transportLine = data.m_transportLine;
			if (transportLine != 0)
			{
				return Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].GetColor();
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(vehicleID, ref data, infoMode);
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_TRAM_STOPPED");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_TRAM_RETURN");
		}
		if (data.m_transportLine != 0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_TRAM_ROUTE");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_TRAM_STOPPED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_passengerCapacity;
		if (data.m_leadingVehicle == 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort trailingVehicle = data.m_trailingVehicle;
			int num = 0;
			while (trailingVehicle != 0)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)trailingVehicle].Info;
				if (instance.m_vehicles.m_buffer[(int)trailingVehicle].m_leadingVehicle != 0)
				{
					int num2;
					int num3;
					info.m_vehicleAI.GetBufferStatus(trailingVehicle, ref instance.m_vehicles.m_buffer[(int)trailingVehicle], out localeKey, out num2, out num3);
					current += num2;
					max += num3;
				}
				trailingVehicle = instance.m_vehicles.m_buffer[(int)trailingVehicle].m_trailingVehicle;
				if (++num > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public override bool GetProgressStatus(ushort vehicleID, ref Vehicle data, out float current, out float max)
	{
		ushort transportLine = data.m_transportLine;
		ushort targetBuilding = data.m_targetBuilding;
		if (transportLine != 0 && targetBuilding != 0)
		{
			float num;
			float max2;
			float num2;
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].GetStopProgress(targetBuilding, out num, out max2, out num2);
			uint path = data.m_path;
			bool result;
			if (path == 0u || (data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
			{
				current = num;
				result = false;
			}
			else
			{
				current = BusAI.GetPathProgress(path, (int)data.m_pathPositionIndex, num, max2, out result);
			}
			max = num2;
			return result;
		}
		current = 0f;
		max = 0f;
		return true;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_passengerCapacity, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveSource(vehicleID, ref data);
		this.RemoveLine(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].AddVehicle(vehicleID, ref data, false);
		}
	}

	protected override void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingAI.PathFindType pathFindType = ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingTransport : BuildingAI.PathFindType.EnteringTransport;
			if (pathFindType == BuildingAI.PathFindType.EnteringTransport || (data.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.PathfindSuccess(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], pathFindType);
			}
		}
		base.PathfindSuccess(vehicleID, ref data);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingAI.PathFindType pathFindType = ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingTransport : BuildingAI.PathFindType.EnteringTransport;
			if (pathFindType == BuildingAI.PathFindType.EnteringTransport || (data.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.PathfindFailure(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], pathFindType);
			}
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		bool flag = data.m_sourceBuilding != 0;
		if (flag)
		{
			this.RemoveSource(vehicleID, ref data);
		}
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			if (!flag)
			{
				data.Unspawn(vehicleID);
				data.m_targetPos0 = data.GetLastFramePosition();
				data.m_targetPos0.w = 2f;
				data.m_targetPos1 = data.m_targetPos0;
				data.m_targetPos2 = data.m_targetPos0;
				data.m_targetPos3 = data.m_targetPos0;
			}
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		data.m_targetBuilding = targetBuilding;
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && building == data.m_sourceBuilding)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void SetTransportLine(ushort vehicleID, ref Vehicle data, ushort transportLine)
	{
		this.RemoveLine(vehicleID, ref data);
		data.m_transportLine = transportLine;
		if (transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].AddVehicle(vehicleID, ref data, true);
		}
		else
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == (TransferManager.TransferReason)data.m_transferType)
		{
			this.SetTransportLine(vehicleID, ref data, offer.TransportLine);
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, reason, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = this.m_passengerCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if (vehicleData.m_leadingVehicle == 0 && (vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			vehicleData.m_waitCounter += 1;
			if (this.CanLeave(vehicleID, ref vehicleData))
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				ushort trailingVehicle = vehicleData.m_trailingVehicle;
				bool flag = true;
				int num = 0;
				while (trailingVehicle != 0)
				{
					VehicleInfo info = instance.m_vehicles.m_buffer[(int)trailingVehicle].Info;
					if (!info.m_vehicleAI.CanLeave(trailingVehicle, ref instance.m_vehicles.m_buffer[(int)trailingVehicle]))
					{
						flag = false;
						break;
					}
					trailingVehicle = instance.m_vehicles.m_buffer[(int)trailingVehicle].m_trailingVehicle;
					if (++num > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				if (flag)
				{
					vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
					vehicleData.m_flags |= Vehicle.Flags.Leaving;
					vehicleData.m_waitCounter = 0;
					if (vehicleData.m_transportLine != 0)
					{
						Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)vehicleData.m_transportLine].LeaveStop(vehicleData.m_targetBuilding);
					}
				}
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTransportLine(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveLine(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].RemoveVehicle(vehicleID, ref data);
			data.m_transportLine = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		ushort targetBuilding = data.m_targetBuilding;
		ushort num = 0;
		if (data.m_transportLine != 0)
		{
			num = TransportLine.GetNextStop(data.m_targetBuilding);
		}
		else if ((data.m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
		{
			num = TransportLine.GetNextStop(data.m_targetBuilding);
			Vector3 lastFramePosition = data.GetLastFramePosition();
			if (Mathf.Max(Mathf.Abs(lastFramePosition.x), Mathf.Abs(lastFramePosition.z)) > 4800f && this.CheckPassengers(vehicleID, ref data, targetBuilding, num) == 0)
			{
				num = 0;
			}
		}
		this.UnloadPassengers(vehicleID, ref data, targetBuilding, num);
		if (num == 0)
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				return true;
			}
			data.m_flags &= ~Vehicle.Flags.Arriving;
			data.m_flags |= Vehicle.Flags.Stopped;
			data.m_waitCounter = 0;
		}
		else
		{
			data.m_targetBuilding = num;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				return true;
			}
			this.LoadPassengers(vehicleID, ref data, targetBuilding, num);
			data.m_flags &= ~Vehicle.Flags.Arriving;
			data.m_flags |= Vehicle.Flags.Stopped;
			data.m_waitCounter = 0;
		}
		return false;
	}

	private void UnloadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0)
		{
			return;
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		TransportManager instance3 = Singleton<TransportManager>.get_instance();
		Vector3 position = instance2.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 targetPos = Vector3.get_zero();
		if (nextStop != 0)
		{
			targetPos = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		}
		int num = 0;
		int num2 = 0;
		while (vehicleID != 0)
		{
			if (data.m_transportLine != 0)
			{
				BusAI.TransportArriveAtTarget(vehicleID, ref instance.m_vehicles.m_buffer[(int)vehicleID], position, targetPos, ref num, ref instance3.m_lines.m_buffer[(int)data.m_transportLine].m_passengers, nextStop == 0);
			}
			else
			{
				BusAI.TransportArriveAtTarget(vehicleID, ref instance.m_vehicles.m_buffer[(int)vehicleID], position, targetPos, ref num, ref instance3.m_passengers[(int)this.m_transportInfo.m_transportType], nextStop == 0);
			}
			vehicleID = instance.m_vehicles.m_buffer[(int)vehicleID].m_trailingVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticArray>(StatisticType.PassengerCount);
		statisticBase.Acquire<StatisticInt32>((int)this.m_transportInfo.m_transportType, 10).Add(num);
		num += (int)instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num, 65535);
	}

	private int CheckPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0 || nextStop == 0)
		{
			return 0;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		if (instance2.m_nodes.m_buffer[(int)currentStop].m_maxWaitTime != 255)
		{
			return 0;
		}
		Vector3 position = instance2.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		int num = Mathf.Max((int)((position.x - 64f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - 64f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + 64f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + 64f) / 8f + 1080f), 2159);
		int num5 = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_citizenGrid[i * 2160 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num6].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num6].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num6].m_targetPos;
						float num8 = Vector3.SqrMagnitude(vector - position);
						if (num8 < 4096f)
						{
							CitizenInfo info = instance.m_instances.m_buffer[(int)num6].Info;
							if (info.m_citizenAI.TransportArriveAtSource(num6, ref instance.m_instances.m_buffer[(int)num6], position, position2))
							{
								num5++;
							}
						}
					}
					num6 = nextGridInstance;
					if (++num7 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num5;
	}

	private void LoadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0 || nextStop == 0)
		{
			return;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		NetManager instance3 = Singleton<NetManager>.get_instance();
		Vector3 position = instance3.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 position2 = instance3.m_nodes.m_buffer[(int)nextStop].m_position;
		instance3.m_nodes.m_buffer[(int)currentStop].m_maxWaitTime = 0;
		int num = (int)instance3.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		bool flag = false;
		int num2 = Mathf.Max((int)((position.x - 64f) / 8f + 1080f), 0);
		int num3 = Mathf.Max((int)((position.z - 64f) / 8f + 1080f), 0);
		int num4 = Mathf.Min((int)((position.x + 64f) / 8f + 1080f), 2159);
		int num5 = Mathf.Min((int)((position.z + 64f) / 8f + 1080f), 2159);
		int num6 = num3;
		while (num6 <= num5 && !flag)
		{
			int num7 = num2;
			while (num7 <= num4 && !flag)
			{
				ushort num8 = instance.m_citizenGrid[num6 * 2160 + num7];
				int num9 = 0;
				while (num8 != 0 && !flag)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num8].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num8].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num8].m_targetPos;
						float num10 = Vector3.SqrMagnitude(vector - position);
						if (num10 < 4096f)
						{
							CitizenInfo info = instance.m_instances.m_buffer[(int)num8].Info;
							if (info.m_citizenAI.TransportArriveAtSource(num8, ref instance.m_instances.m_buffer[(int)num8], position, position2))
							{
								ushort num11;
								uint unitID;
								if (Vehicle.GetClosestFreeTrailer(vehicleID, vector, out num11, out unitID))
								{
									if (info.m_citizenAI.SetCurrentVehicle(num8, ref instance.m_instances.m_buffer[(int)num8], num11, unitID, position))
									{
										num++;
										Vehicle[] expr_248_cp_0 = instance2.m_vehicles.m_buffer;
										ushort expr_248_cp_1 = num11;
										expr_248_cp_0[(int)expr_248_cp_1].m_transferSize = expr_248_cp_0[(int)expr_248_cp_1].m_transferSize + 1;
									}
									else
									{
										flag = true;
									}
								}
								else
								{
									flag = true;
								}
							}
						}
					}
					num8 = nextGridInstance;
					if (++num9 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				num7++;
			}
			num6++;
		}
		instance3.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num, 65535);
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingLoading) == (Vehicle.Flags)0)
		{
			return this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		vehicleData.m_waitCounter = (byte)Mathf.Min((int)(vehicleData.m_waitCounter + 1), 255);
		if (vehicleData.m_waitCounter >= 16)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		return false;
	}

	protected override void ArrivingToDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		base.ArrivingToDestination(vehicleID, ref vehicleData);
		if (this.m_arriveEffect != null)
		{
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				if (vehicleData.m_sourceBuilding != 0)
				{
					Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].m_position;
					InstanceID instance = default(InstanceID);
					instance.Building = vehicleData.m_sourceBuilding;
					EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(position, Vector3.get_up(), 0f);
					Singleton<EffectManager>.get_instance().DispatchEffect(this.m_arriveEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<BuildingManager>.get_instance().m_audioGroup);
				}
			}
			else if (vehicleData.m_targetBuilding != 0)
			{
				Vector3 position2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
				InstanceID instance2 = default(InstanceID);
				instance2.NetNode = vehicleData.m_targetBuilding;
				EffectInfo.SpawnArea spawnArea2 = new EffectInfo.SpawnArea(position2, Vector3.get_up(), 0f);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_arriveEffect, instance2, spawnArea2, Vector3.get_zero(), 0f, 1f, Singleton<BuildingManager>.get_instance().m_audioGroup);
			}
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_leadingVehicle == 0)
		{
			Vector3 startPos;
			if ((vehicleData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
			{
				ushort lastVehicle = vehicleData.GetLastVehicle(vehicleID);
				startPos = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)lastVehicle].m_targetPos0;
			}
			else
			{
				startPos = vehicleData.m_targetPos0;
			}
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				if (vehicleData.m_sourceBuilding != 0)
				{
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
					Randomizer randomizer;
					randomizer..ctor((int)vehicleID);
					Vector3 endPos;
					Vector3 vector;
					info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out endPos, out vector);
					return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos);
				}
			}
			else if (vehicleData.m_targetBuilding != 0)
			{
				Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
				return this.StartPathFind(vehicleID, ref vehicleData, startPos, position);
			}
		}
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		return (vehicleData.m_leadingVehicle != 0 || vehicleData.m_waitCounter >= 12) && base.CanLeave(vehicleID, ref vehicleData) && (vehicleData.m_leadingVehicle != 0 || vehicleData.m_transportLine == 0 || Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)vehicleData.m_transportLine].CanLeaveStop(vehicleData.m_targetBuilding, vehicleData.m_waitCounter >> 4));
	}

	public override int GetTicketPrice(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_transportLine == 0)
		{
			return this.m_ticketPrice;
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(vehicleData.m_targetPos3);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Event @event = instance.m_districts.m_buffer[(int)district].m_eventPolicies & Singleton<EventManager>.get_instance().GetEventPolicyMask();
		if ((servicePolicies & DistrictPolicies.Services.FreeTransport) != DistrictPolicies.Services.None)
		{
			District[] expr_80_cp_0 = instance.m_districts.m_buffer;
			byte expr_80_cp_1 = district;
			expr_80_cp_0[(int)expr_80_cp_1].m_servicePoliciesEffect = (expr_80_cp_0[(int)expr_80_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.FreeTransport);
			return 0;
		}
		if ((@event & DistrictPolicies.Event.ComeOneComeAll) != DistrictPolicies.Event.None)
		{
			District[] expr_AC_cp_0 = instance.m_districts.m_buffer;
			byte expr_AC_cp_1 = district;
			expr_AC_cp_0[(int)expr_AC_cp_1].m_eventPoliciesEffect = (expr_AC_cp_0[(int)expr_AC_cp_1].m_eventPoliciesEffect | DistrictPolicies.Event.ComeOneComeAll);
			return 0;
		}
		if ((servicePolicies & DistrictPolicies.Services.HighTicketPrices) != DistrictPolicies.Services.None)
		{
			District[] expr_D8_cp_0 = instance.m_districts.m_buffer;
			byte expr_D8_cp_1 = district;
			expr_D8_cp_0[(int)expr_D8_cp_1].m_servicePoliciesEffect = (expr_D8_cp_0[(int)expr_D8_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.HighTicketPrices);
			return this.m_ticketPrice * 5 / 4;
		}
		return this.m_ticketPrice;
	}

	public override InstanceID GetOwnerID(ushort vehicleID, ref Vehicle vehicleData)
	{
		return default(InstanceID);
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.NetNode = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public EffectInfo m_arriveEffect;

	[CustomizableProperty("PassengerCapacity")]
	public int m_passengerCapacity = 20;

	public int m_ticketPrice = 200;
}
