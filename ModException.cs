﻿using System;

public class ModException : Exception
{
	public ModException(string message, Exception inner) : base(message, inner)
	{
	}
}
