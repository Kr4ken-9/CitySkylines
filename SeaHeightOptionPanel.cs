﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class SeaHeightOptionPanel : ToolsModifierControl
{
	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIInput.add_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
		else
		{
			UIInput.remove_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
	}

	private void ProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (eventType != 4)
		{
			return;
		}
		if (this.m_RaiseSeaHeight.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_HeightSlider.set_value(this.m_HeightSlider.get_value() + SeaHeightOptionPanel.kSeaHeightInterval);
		}
		else if (this.m_LowerSeaHeight.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_HeightSlider.set_value(this.m_HeightSlider.get_value() - SeaHeightOptionPanel.kSeaHeightInterval);
		}
	}

	private void SetHeight(float height)
	{
		Singleton<TerrainManager>.get_instance().WaterSimulation.m_nextSeaLevel = height;
	}

	private void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_HeightSlider.set_value(Singleton<TerrainManager>.get_instance().WaterSimulation.m_nextSeaLevel);
		}
	}

	private void Awake()
	{
		this.m_RaiseSeaHeight = new SavedInputKey(Settings.mapEditorRaiseSeaHeight, Settings.inputSettingsFile, DefaultSettings.mapEditorRaiseSeaHeight, true);
		this.m_LowerSeaHeight = new SavedInputKey(Settings.mapEditorLowerSeaHeight, Settings.inputSettingsFile, DefaultSettings.mapEditorLowerSeaHeight, true);
		this.Hide();
		this.m_HeightSlider = base.Find<UISlider>("Height");
		this.m_HeightTextfield = base.Find<UITextField>("Height");
		this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
		this.m_HeightSlider.add_eventValueChanged(delegate(UIComponent sender, float value)
		{
			this.m_HeightTextfield.set_text(value.ToString("0.00"));
			this.SetHeight(value);
		});
		this.m_HeightTextfield.add_eventTextSubmitted(delegate(UIComponent sender, string s)
		{
			float num;
			if (float.TryParse(s, out num))
			{
				this.m_HeightSlider.set_value(num);
				this.SetHeight(num);
			}
			else
			{
				this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
			}
		});
		this.m_HeightTextfield.add_eventTextCancelled(delegate(UIComponent sender, string s)
		{
			this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
		});
	}

	private static readonly float kSeaHeightInterval = 10f;

	private SavedInputKey m_RaiseSeaHeight;

	private SavedInputKey m_LowerSeaHeight;

	private UISlider m_HeightSlider;

	private UITextField m_HeightTextfield;
}
