﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class LevelHeightOptionPanel : ToolsModifierControl
{
	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIInput.add_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
		else
		{
			UIInput.remove_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
	}

	private void ProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (eventType != 4)
		{
			return;
		}
		if (this.m_RaiseLevelHeight.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_HeightSlider.set_value(this.m_HeightSlider.get_value() + LevelHeightOptionPanel.kLevelHeightInterval);
		}
		else if (this.m_LowerLevelHeight.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_HeightSlider.set_value(this.m_HeightSlider.get_value() - LevelHeightOptionPanel.kLevelHeightInterval);
		}
	}

	private void SetHeight(float height)
	{
		TerrainTool tool = ToolsModifierControl.GetTool<TerrainTool>();
		if (tool != null)
		{
			tool.m_startPosition.y = height;
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible())
		{
			TerrainTool tool = ToolsModifierControl.GetTool<TerrainTool>();
			if (tool != null)
			{
				this.m_HeightSlider.set_value(tool.m_startPosition.y);
			}
		}
	}

	private void Awake()
	{
		this.m_RaiseLevelHeight = new SavedInputKey(Settings.mapEditorRaiseLevelHeight, Settings.inputSettingsFile, DefaultSettings.mapEditorRaiseLevelHeight, true);
		this.m_LowerLevelHeight = new SavedInputKey(Settings.mapEditorLowerLevelHeight, Settings.inputSettingsFile, DefaultSettings.mapEditorLowerLevelHeight, true);
		this.Hide();
		this.m_HeightSlider = base.Find<UISlider>("Height");
		this.m_HeightTextfield = base.Find<UITextField>("Height");
		this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
		this.m_HeightSlider.add_eventValueChanged(delegate(UIComponent sender, float value)
		{
			this.m_HeightTextfield.set_text(value.ToString("0.00"));
			this.SetHeight(value);
		});
		this.m_HeightTextfield.add_eventTextSubmitted(delegate(UIComponent sender, string s)
		{
			float num;
			if (float.TryParse(s, out num))
			{
				this.m_HeightSlider.set_value(num);
				this.SetHeight(num);
			}
			else
			{
				this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
			}
		});
		this.m_HeightTextfield.add_eventTextCancelled(delegate(UIComponent sender, string s)
		{
			this.m_HeightTextfield.set_text(this.m_HeightSlider.get_value().ToString("0.00"));
		});
	}

	private static readonly float kLevelHeightInterval = 10f;

	private SavedInputKey m_RaiseLevelHeight;

	private SavedInputKey m_LowerLevelHeight;

	private UISlider m_HeightSlider;

	private UITextField m_HeightTextfield;
}
