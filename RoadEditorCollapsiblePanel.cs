﻿using System;
using ColossalFramework.UI;

public class RoadEditorCollapsiblePanel : UICustomControl
{
	public UIButton LabelButton
	{
		get
		{
			return this.m_Button;
		}
	}

	public UIPanel Container
	{
		get
		{
			return this.m_Panel;
		}
	}

	public void AddComponent(UIComponent comp, bool keepLast = false)
	{
		this.Container.AttachUIComponent(comp.get_gameObject());
		if (keepLast)
		{
			this.m_KeepLast = comp;
		}
		this.EnsureKeepLast();
	}

	private void EnsureKeepLast()
	{
		if (this.m_KeepLast != null)
		{
			this.m_KeepLast.set_zOrder(2147483647);
		}
	}

	private void OnEnable()
	{
		this.m_Button.add_eventClick(new MouseEventHandler(this.OnButtonClick));
		this.m_Panel.Hide();
		this.m_Button.set_normalFgSprite(this.m_ClosedSprite);
	}

	private void OnDisable()
	{
		this.m_Button.remove_eventClick(new MouseEventHandler(this.OnButtonClick));
	}

	private void OnButtonClick(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.Toggle();
	}

	private void Toggle()
	{
		if (this.m_Panel.get_isVisible())
		{
			this.m_Panel.Hide();
			this.m_Button.set_normalFgSprite(this.m_ClosedSprite);
		}
		else
		{
			this.m_Panel.Show();
			this.m_Button.set_normalFgSprite(this.m_OpenSprite);
		}
	}

	public string m_ClosedSprite;

	public string m_OpenSprite;

	public UIButton m_Button;

	public UIPanel m_Panel;

	private UIComponent m_KeepLast;
}
