﻿using System;
using ColossalFramework;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ICities;

public static class PluginHelper
{
	public static void ValidatePlugins()
	{
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			if (current.get_isEnabled())
			{
				for (int i = 0; i < PluginHelper.kDeprecatedIDs.Length; i++)
				{
					if (current.get_publishedFileID() == PluginHelper.kDeprecatedIDs[i])
					{
						string name = current.get_name();
						IUserMod[] instances = current.GetInstances<IUserMod>();
						if (instances.Length > 0)
						{
							name = instances[0].get_Name();
						}
						DebugOutputPanel.AddMessage(1, "[DEPRECATED MOD] The mod " + name + " has been marked as redundant by the developers and has been automatically disabled. It can be re-enabled from the Content panel but is likely to conflict with new features added by the developers in the latest patch. (This message is harmless)");
						current.set_isEnabled(false);
						PluginHelper.Act(current, PluginHelper.kDeprecatedIDs[i]);
					}
				}
			}
		}
	}

	private static void Act(PluginManager.PluginInfo info, PublishedFileId id)
	{
		if (id == PluginHelper.kDeprecatedIDs[0])
		{
			SavedFloat savedFloat = new SavedFloat(Settings.filmGrainAmount, Settings.gameSettingsFile, DefaultSettings.filmGrainAmount, true);
			savedFloat.set_value(0f);
			DebugOutputPanel.AddMessage(2, "[DEPRECATED MOD] Your film grain settings has been set to zero to match the deprecated mod parameters. (This message is harmless)");
		}
	}

	private static PublishedFileId[] kDeprecatedIDs = new PublishedFileId[]
	{
		new PublishedFileId(406018056uL)
	};
}
