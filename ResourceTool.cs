﻿using System;
using ColossalFramework;
using UnityEngine;

public class ResourceTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
	}

	protected override void OnToolGUI(Event e)
	{
		if (!this.m_toolController.IsInsideUI && e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = true;
			}
			else if (e.get_button() == 1)
			{
				this.m_mouseRightDown = true;
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = false;
			}
			else if (e.get_button() == 1)
			{
				this.m_mouseRightDown = false;
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		this.m_toolController.SetBrush(null, Vector3.get_zero(), 1f);
		this.m_mouseLeftDown = false;
		this.m_mouseRightDown = false;
	}

	protected override void OnToolUpdate()
	{
		if (this.m_resourceCursors != null && (NaturalResourceManager.Resource)this.m_resourceCursors.Length > this.m_resource)
		{
			base.ToolCursor = this.m_resourceCursors[(int)this.m_resource];
		}
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
		ToolBase.OverrideInfoMode = false;
	}

	public override void SimulationStep()
	{
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (ToolBase.RayCast(input, out raycastOutput))
		{
			this.m_mousePosition = raycastOutput.m_hitPos;
			if (this.m_mouseLeftDown != this.m_mouseRightDown)
			{
				this.ApplyBrush(this.m_mouseRightDown);
			}
		}
	}

	private void ApplyBrush(bool negate)
	{
		float[] brushData = this.m_toolController.BrushData;
		float num = this.m_brushSize * 0.5f;
		float num2 = 33.75f;
		int num3 = 512;
		NaturalResourceManager.ResourceCell[] naturalResources = Singleton<NaturalResourceManager>.get_instance().m_naturalResources;
		float strength = this.m_strength;
		Vector3 mousePosition = this.m_mousePosition;
		int num4 = Mathf.Max((int)((mousePosition.x - num) / num2 + (float)num3 * 0.5f), 0);
		int num5 = Mathf.Max((int)((mousePosition.z - num) / num2 + (float)num3 * 0.5f), 0);
		int num6 = Mathf.Min((int)((mousePosition.x + num) / num2 + (float)num3 * 0.5f), num3 - 1);
		int num7 = Mathf.Min((int)((mousePosition.z + num) / num2 + (float)num3 * 0.5f), num3 - 1);
		for (int i = num5; i <= num7; i++)
		{
			float num8 = (((float)i - (float)num3 * 0.5f + 0.5f) * num2 - mousePosition.z + num) / this.m_brushSize * 64f - 0.5f;
			int num9 = Mathf.Clamp(Mathf.FloorToInt(num8), 0, 63);
			int num10 = Mathf.Clamp(Mathf.CeilToInt(num8), 0, 63);
			for (int j = num4; j <= num6; j++)
			{
				float num11 = (((float)j - (float)num3 * 0.5f + 0.5f) * num2 - mousePosition.x + num) / this.m_brushSize * 64f - 0.5f;
				int num12 = Mathf.Clamp(Mathf.FloorToInt(num11), 0, 63);
				int num13 = Mathf.Clamp(Mathf.CeilToInt(num11), 0, 63);
				float num14 = brushData[num9 * 64 + num12];
				float num15 = brushData[num9 * 64 + num13];
				float num16 = brushData[num10 * 64 + num12];
				float num17 = brushData[num10 * 64 + num13];
				float num18 = num14 + (num15 - num14) * (num11 - (float)num12);
				float num19 = num16 + (num17 - num16) * (num11 - (float)num12);
				float num20 = num18 + (num19 - num18) * (num8 - (float)num9);
				NaturalResourceManager.ResourceCell resourceCell = naturalResources[i * num3 + j];
				int num21 = (int)(255f * strength * num20);
				if (negate)
				{
					num21 = -num21;
				}
				if (this.m_resource == NaturalResourceManager.Resource.Ore)
				{
					this.ChangeMaterial(ref resourceCell.m_ore, ref resourceCell.m_oil, num21);
				}
				else if (this.m_resource == NaturalResourceManager.Resource.Oil)
				{
					this.ChangeMaterial(ref resourceCell.m_oil, ref resourceCell.m_ore, num21);
				}
				else if (this.m_resource == NaturalResourceManager.Resource.Sand)
				{
					this.ChangeMaterial(ref resourceCell.m_sand, ref resourceCell.m_fertility, num21);
				}
				else if (this.m_resource == NaturalResourceManager.Resource.Fertility && resourceCell.m_forest == 0)
				{
					this.ChangeMaterial(ref resourceCell.m_fertility, ref resourceCell.m_sand, num21);
				}
				naturalResources[i * num3 + j] = resourceCell;
			}
		}
		Singleton<NaturalResourceManager>.get_instance().AreaModified(num4, num5, num6, num7);
	}

	private void ChangeMaterial(ref byte amount, ref byte other, int change)
	{
		if (change > 0)
		{
			if ((int)other >= change)
			{
				other = (byte)((int)other - change);
			}
			else
			{
				amount = (byte)Mathf.Min((int)amount + change - (int)other, 255);
				other = 0;
			}
		}
		else
		{
			amount = (byte)Mathf.Max((int)amount + change, 0);
		}
	}

	public NaturalResourceManager.Resource m_resource;

	public float m_brushSize = 200f;

	public float m_strength = 0.5f;

	public Texture2D m_brush;

	public CursorInfo[] m_resourceCursors;

	private Vector3 m_mousePosition;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private bool m_mouseLeftDown;

	private bool m_mouseRightDown;
}
