﻿using System;
using ColossalFramework.Math;
using UnityEngine;

public class SoundEffect : EffectInfo
{
	public override void PlayEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup)
	{
		Vector3 position = area.m_matrix.MultiplyPoint(this.m_position);
		this.PlaySound(id, listenerInfo, audioGroup, position, velocity, this.m_range, magnitude, 1f);
	}

	public void PlaySound(InstanceID id, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup, Vector3 position, Vector3 velocity, float range, float volume, float pitch)
	{
		if (!this.m_audioInfo.m_is3D || Vector3.SqrMagnitude(position - listenerInfo.m_position) < range * range)
		{
			Randomizer randomizer;
			randomizer..ctor(id.RawData);
			AudioInfo variation = this.m_audioInfo.GetVariation(ref randomizer);
			audioGroup.AddPlayer(listenerInfo, (int)id.RawData, variation, position, velocity, range, volume, pitch);
		}
	}

	public override bool RequirePlay()
	{
		return true;
	}

	public AudioInfo m_audioInfo;

	public float m_range = 200f;

	public Vector3 m_position = Vector3.get_zero();
}
