﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ForestFireAI : DisasterAI
{
	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector3 vector = data.m_targetPosition;
			float num = 100f;
			Vector3 vector2;
			vector2.x = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector2.y = 0f;
			vector2.z = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector += vector2 * num;
			float num2 = 300f + 300f * ((float)data.m_intensity * 0.01f);
			float num3 = num2;
			float num4 = num2 + num * 2f;
			float num5 = vector.x - num4;
			float num6 = vector.z - num4;
			float num7 = vector.x + num4;
			float num8 = vector.z + num4;
			int num9 = Mathf.Max((int)(num5 / 38.4f + 128f), 2);
			int num10 = Mathf.Max((int)(num6 / 38.4f + 128f), 2);
			int num11 = Mathf.Min((int)(num7 / 38.4f + 128f), 253);
			int num12 = Mathf.Min((int)(num8 / 38.4f + 128f), 253);
			NaturalResourceManager instance = Singleton<NaturalResourceManager>.get_instance();
			Vector3 vector3;
			vector3.y = 0f;
			for (int i = num10; i <= num12; i++)
			{
				vector3.z = ((float)i - 128f + 0.5f) * 38.4f - vector.z;
				for (int j = num9; j <= num11; j++)
				{
					vector3.x = ((float)j - 128f + 0.5f) * 38.4f - vector.x;
					float magnitude = vector3.get_magnitude();
					if (magnitude < num4)
					{
						float num13 = Mathf.Min(1f, 1f - (magnitude - num3) / (num4 - num3));
						byte b;
						instance.CheckForest(vector + vector3, out b);
						num13 *= Mathf.Clamp01((float)b * 0.01f);
						if (num13 > 0f)
						{
							int num14 = i * 256 + j;
							map[num14] = (byte)(255 - (int)(255 - map[num14]) * (255 - Mathf.RoundToInt(num13 * 255f)) / 255);
						}
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			InstanceID id = default(InstanceID);
			id.Disaster = disasterID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
			uint num = ForestFireAI.FindClosestTree(data.m_targetPosition);
			if (num != 0u)
			{
				TreeManager instance = Singleton<TreeManager>.get_instance();
				data.m_targetPosition = instance.m_trees.m_buffer[(int)((UIntPtr)num)].Position;
				int fireIntensity = Mathf.Min(255, (int)(128 + data.m_intensity * 127 / 100));
				if (Singleton<TreeManager>.get_instance().BurnTree(num, group, fireIntensity))
				{
					data.m_flags |= DisasterData.Flags.Significant;
					base.ActivateNow(disasterID, ref data);
				}
			}
		}
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			Singleton<WeatherManager>.get_instance().m_targetRain = 0f;
		}
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		InstanceID id = default(InstanceID);
		id.Disaster = disasterID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		if (group == null || group.m_refCount <= 1)
		{
			return false;
		}
		uint num = (uint)((uint)(data.m_intensity + 20) << 8);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num2 < num;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = (uint)((uint)(data.m_intensity + 20) << 8);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return (int)(1600u / (1u + num2 / (uint)Mathf.Max(1, (int)num >> 4)));
	}

	private static uint FindClosestTree(Vector3 pos)
	{
		TreeManager instance = Singleton<TreeManager>.get_instance();
		int num = Mathf.Max((int)(pos.x / 32f + 270f), 0);
		int num2 = Mathf.Max((int)(pos.z / 32f + 270f), 0);
		int num3 = Mathf.Min((int)(pos.x / 32f + 270f), 539);
		int num4 = Mathf.Min((int)(pos.z / 32f + 270f), 539);
		int num5 = num + 1;
		int num6 = num2 + 1;
		int num7 = num3 - 1;
		int num8 = num4 - 1;
		uint num9 = 0u;
		float num10 = 1E+12f;
		float num11 = 0f;
		while (num != num5 || num2 != num6 || num3 != num7 || num4 != num8)
		{
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					if (j >= num5 && i >= num6 && j <= num7 && i <= num8)
					{
						j = num7;
					}
					else
					{
						uint num12 = instance.m_treeGrid[i * 540 + j];
						int num13 = 0;
						while (num12 != 0u)
						{
							if ((instance.m_trees.m_buffer[(int)((UIntPtr)num12)].m_flags & 67) == 1 && instance.m_trees.m_buffer[(int)((UIntPtr)num12)].GrowState != 0)
							{
								Vector3 position = instance.m_trees.m_buffer[(int)((UIntPtr)num12)].Position;
								float num14 = Vector3.SqrMagnitude(position - pos);
								if (num14 < num10)
								{
									num9 = num12;
									num10 = num14;
								}
							}
							num12 = instance.m_trees.m_buffer[(int)((UIntPtr)num12)].m_nextGridTree;
							if (++num13 >= 262144)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			if (num9 != 0u && num10 <= num11 * num11)
			{
				return num9;
			}
			num11 += 32f;
			num5 = num;
			num6 = num2;
			num7 = num3;
			num8 = num4;
			num = Mathf.Max(num - 1, 0);
			num2 = Mathf.Max(num2 - 1, 0);
			num3 = Mathf.Min(num3 + 1, 539);
			num4 = Mathf.Min(num4 + 1, 539);
		}
		return num9;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.ForestFireHazard;
		return true;
	}
}
