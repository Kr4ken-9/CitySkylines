﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class ChirpPanel : ToolsModifierControl
{
	public void OnChirpOptions()
	{
		ChirpOptionsPanel chirpOptionsPanel = UIView.get_library().ShowModal<ChirpOptionsPanel>("ChirpOptionsPanel");
		chirpOptionsPanel.owner = this;
	}

	public void SetChirperImage(string indexName)
	{
		UIButton uIButton = base.Find<UIButton>("Zone");
		uIButton.set_atlas(this.m_ChirperAtlas);
		int i;
		for (i = 0; i < this.m_ChirperSet.Length; i++)
		{
			if (this.m_ChirperSet[i].m_SetName == indexName)
			{
				break;
			}
		}
		if (this.m_ChirperSet[i].m_RequiredDLCMask != (SteamHelper.DLC_BitMask)0 && (SteamHelper.GetOwnedDLCMask() & this.m_ChirperSet[i].m_RequiredDLCMask) == (SteamHelper.DLC_BitMask)0)
		{
			i = 0;
		}
		uIButton.set_normalBgSprite(this.m_ChirperSet[i].m_Normal);
		uIButton.set_pressedBgSprite(this.m_ChirperSet[i].m_Pressed);
		uIButton.set_hoveredBgSprite(this.m_ChirperSet[i].m_Hovered);
		uIButton.set_size(this.m_ChirperAtlas.get_Item(this.m_ChirperSet[i].m_Normal).get_pixelSize());
		base.get_component().set_relativePosition(new Vector3(base.get_component().get_relativePosition().x, -uIButton.get_relativePosition().y + 5f, base.get_component().get_relativePosition().z));
		if (this.m_ChirperSet[i].m_SetName != DefaultSettings.chirperImage && !PlatformService.get_achievements().get_Item("DoesMyBumLookBigInThis").get_achieved())
		{
			PlatformService.get_achievements().get_Item("DoesMyBumLookBigInThis").Unlock();
		}
		if (!string.IsNullOrEmpty(this.m_ChirperSet[i].m_Achievement) && !PlatformService.get_achievements().get_Item(this.m_ChirperSet[i].m_Achievement).get_achieved())
		{
			PlatformService.get_achievements().get_Item(this.m_ChirperSet[i].m_Achievement).Unlock();
		}
	}

	public bool isShowing
	{
		get
		{
			return this.m_Showing;
		}
	}

	public static ChirpPanel instance
	{
		get
		{
			return ChirpPanel.sInstance;
		}
	}

	public void Expand()
	{
		this.Expand(0f);
	}

	public void Expand(float timeout)
	{
		this.Show(timeout);
	}

	public void Collapse()
	{
		this.Hide();
	}

	public void AddMessage(IChirperMessage message)
	{
		this.AddMessage(message, true);
	}

	public void AddMessage(IChirperMessage message, bool show)
	{
		this.AddEntry(message, false);
		if (show && this.m_AutoExpand)
		{
			this.Show(this.m_MessageTimeout);
		}
	}

	public void ClearMessages()
	{
		this.m_NewMessageCount = 0;
		UITemplateManager.ClearInstances(ChirpPanel.kChirpTemplate);
	}

	[DebuggerHidden]
	private IEnumerator<MessageBase[]> SimulationGetMessages()
	{
		return new ChirpPanel.<SimulationGetMessages>c__Iterator0();
	}

	[DebuggerHidden]
	private IEnumerator SynchronizeMessagesCoroutine()
	{
		ChirpPanel.<SynchronizeMessagesCoroutine>c__Iterator1 <SynchronizeMessagesCoroutine>c__Iterator = new ChirpPanel.<SynchronizeMessagesCoroutine>c__Iterator1();
		<SynchronizeMessagesCoroutine>c__Iterator.$this = this;
		return <SynchronizeMessagesCoroutine>c__Iterator;
	}

	public void SynchronizeMessages()
	{
		base.StartCoroutine(this.SynchronizeMessagesCoroutine());
	}

	private void Awake()
	{
		ChirpPanel.sInstance = this;
		this.m_Counter = base.Find<UILabel>("Counter");
		this.m_Counter.Hide();
		this.m_Chirps = base.Find<UIPanel>("Chirps");
		this.m_Container = base.Find<UIScrollablePanel>("Container");
		this.m_DefaultSize = base.get_component().get_size();
		base.get_component().set_size(new Vector2(38f, 45f));
		this.m_Chirps.Hide();
		new GameObject("ChirperBehaviourContainer", new Type[]
		{
			typeof(ChirperBehaviourContainer)
		});
	}

	private void OnDestroy()
	{
		ChirpPanel.sInstance = null;
	}

	private void OnEnable()
	{
		this.SynchronizeMessages();
		if (Singleton<MessageManager>.get_exists())
		{
			Singleton<MessageManager>.get_instance().m_messagesUpdated += new Action(this.SynchronizeMessages);
			Singleton<MessageManager>.get_instance().m_newMessages += new MessageManager.NewMessageHandler(this.AddMessage);
			LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.SynchronizeMessages));
		}
		this.SetChirperImage(this.m_SelectedChirper);
	}

	private void OnDisable()
	{
		if (Singleton<MessageManager>.get_exists())
		{
			Singleton<MessageManager>.get_instance().m_messagesUpdated -= new Action(this.SynchronizeMessages);
			Singleton<MessageManager>.get_instance().m_newMessages -= new MessageManager.NewMessageHandler(this.AddMessage);
			LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.SynchronizeMessages));
		}
	}

	public void Show(float timeout)
	{
		this.m_Counter.Hide();
		this.m_NewMessageCount = 0;
		if (!this.m_Showing)
		{
			this.m_Timeout = timeout;
		}
		this.m_Showing = true;
		if (!this.m_Chirps.get_isVisible())
		{
			this.m_Chirps.Show();
			this.m_Container.ScrollToBottom();
			ValueAnimator.Animate("ChirpPanelX", delegate(float val)
			{
				Vector2 size = base.get_component().get_size();
				size.x = val;
				base.get_component().set_size(size);
			}, new AnimatedFloat(38f, this.m_DefaultSize.x, this.m_ShowHideTime, this.m_ShowEasingType), delegate
			{
				if (this.m_Showing)
				{
					ValueAnimator.Animate("ChirpPanelY", delegate(float val)
					{
						Vector2 size = base.get_component().get_size();
						size.y = val;
						base.get_component().set_size(size);
					}, new AnimatedFloat(45f, this.m_DefaultSize.y, this.m_ShowHideTime, this.m_ShowEasingType));
				}
			});
		}
	}

	public void Hide()
	{
		this.m_Showing = false;
		if (this.m_Chirps.get_isVisible())
		{
			if (ValueAnimator.IsAnimating("ChirpPanelX"))
			{
				ValueAnimator.Animate("ChirpPanelX", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.x = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(this.m_DefaultSize.x, 38f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					this.m_Chirps.Hide();
				});
			}
			else
			{
				ValueAnimator.Animate("ChirpPanelY", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.y = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(this.m_DefaultSize.y, 45f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					ValueAnimator.Animate("ChirpPanelX", delegate(float val)
					{
						Vector2 size = base.get_component().get_size();
						size.x = val;
						base.get_component().set_size(size);
					}, new AnimatedFloat(this.m_DefaultSize.x, 38f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
					{
						this.m_Chirps.Hide();
					});
				});
			}
		}
	}

	public void OnMouseDown(UIComponent component, UIMouseEventParameter p)
	{
		this.m_Timeout = 0f;
	}

	[DebuggerHidden]
	private IEnumerator SimulationDeleteMessage(MessageBase message)
	{
		ChirpPanel.<SimulationDeleteMessage>c__Iterator2 <SimulationDeleteMessage>c__Iterator = new ChirpPanel.<SimulationDeleteMessage>c__Iterator2();
		<SimulationDeleteMessage>c__Iterator.message = message;
		return <SimulationDeleteMessage>c__Iterator;
	}

	public void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p != null && p.get_source() is UIButton && p.get_source().get_name() == "Remove")
		{
			if (Singleton<SimulationManager>.get_exists() && Singleton<MessageManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.SimulationDeleteMessage((MessageBase)p.get_source().get_parent().get_objectUserData()));
			}
			UITemplateManager.RemoveInstance(ChirpPanel.kChirpTemplate, p.get_source().get_parent());
			if (this.m_Container.get_childCount() == 0)
			{
				this.Hide();
			}
		}
	}

	public static float Integrate(float src, float dst, float speed, float time)
	{
		float num = 1f - Mathf.Pow(1f - speed, time);
		return src + (dst - src) * num;
	}

	public static Vector3 Integrate(Vector3 src, Vector3 dst, float speed, float time)
	{
		return new Vector3(ChirpPanel.Integrate(src.x, dst.x, speed, time), ChirpPanel.Integrate(src.y, dst.y, speed, time), ChirpPanel.Integrate(src.z, dst.z, speed, time));
	}

	private void Update()
	{
		if (this.m_Showing && this.m_Timeout > 0f)
		{
			this.m_Timeout -= Time.get_deltaTime();
			if (this.m_Timeout <= 0f)
			{
				this.Hide();
			}
		}
	}

	public void Toggle()
	{
		if (this.m_Showing)
		{
			this.Hide();
		}
		else
		{
			this.Show(0f);
		}
	}

	protected void OnTargetClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != null && ToolsModifierControl.cameraController != null)
		{
			InstanceID id = (InstanceID)p.get_source().get_objectUserData();
			if (InstanceManager.IsValid(id))
			{
				ToolsModifierControl.cameraController.SetTarget(id, ToolsModifierControl.cameraController.get_transform().get_position(), true);
			}
		}
	}

	public void AddEntry(IChirperMessage message, bool noAudio)
	{
		if (this.m_NotificationSound != null && !noAudio)
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_NotificationSound, this.m_ChirperAudioVolume.get_value());
		}
		if (!this.m_Showing)
		{
			this.m_NewMessageCount++;
			this.m_Counter.Show();
			this.m_Counter.set_text(this.m_NewMessageCount.ToString());
		}
		int childCount = this.m_Container.get_childCount();
		UIPanel uIPanel;
		if (childCount + 1 > this.m_MessageBufferSize && childCount > 0)
		{
			uIPanel = (this.m_Container.get_components()[0] as UIPanel);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(ChirpPanel.kChirpTemplate);
			uIPanel = (this.m_Container.AttachUIComponent(asGameObject) as UIPanel);
		}
		UIButton uIButton = uIPanel.Find<UIButton>("Sender");
		uIButton.set_objectUserData(new InstanceID
		{
			Citizen = message.get_senderID()
		});
		uIButton.add_eventClick(new MouseEventHandler(this.OnTargetClick));
		UILabel uILabel = uIPanel.Find<UILabel>("Content");
		uIPanel.FitTo(uIPanel.get_parent(), 0);
		uIButton.set_text(message.get_senderName());
		uILabel.set_text(message.get_text());
		uIPanel.set_objectUserData(message);
		uIPanel.FitChildrenVertically(10f);
		uIPanel.set_zOrder(2147483647);
		this.m_Container.ScrollToBottom();
	}

	private static readonly string kChirpTemplate = "ChirpTemplate";

	public AudioClip m_NotificationSound;

	public int m_MessageBufferSize = 16;

	private SavedBool m_AutoExpand = new SavedBool(Settings.autoExpandChirper, Settings.gameSettingsFile, DefaultSettings.autoExpandChirper, true);

	private SavedFloat m_ChirperAudioVolume = new SavedFloat(Settings.chirperAudioVolume, Settings.gameSettingsFile, DefaultSettings.chirperAudioVolume, true);

	private SavedString m_SelectedChirper = new SavedString(Settings.chirperImage, Settings.sharedSettings, DefaultSettings.chirperImage, true);

	public float m_MessageTimeout = 6f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private int m_NewMessageCount;

	private UIScrollablePanel m_Container;

	private UIPanel m_Chirps;

	private UILabel m_Counter;

	private Vector2 m_DefaultSize;

	private bool m_Showing;

	private float m_Timeout;

	private static ChirpPanel sInstance;

	public UITextureAtlas m_ChirperAtlas;

	public ChirpPanel.ChirperSet[] m_ChirperSet;

	[Serializable]
	public class ChirperSet
	{
		public string m_SetName;

		[UISprite("m_ChirperAtlas")]
		public string m_Normal;

		[UISprite("m_ChirperAtlas")]
		public string m_Pressed;

		[UISprite("m_ChirperAtlas")]
		public string m_Hovered;

		public string m_Achievement;

		[BitMask]
		public SteamHelper.DLC_BitMask m_RequiredDLCMask;
	}
}
