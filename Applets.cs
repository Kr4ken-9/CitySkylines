﻿using System;
using ColossalFramework;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Applets : MonoBehaviour
{
	private void Awake()
	{
		string[] array = null;
		if (!string.IsNullOrEmpty(this.m_Tool))
		{
			this.m_CommandParameters = this.m_CommandParameters + " --tool=" + this.m_Tool;
			array = this.m_CommandParameters.Split(new char[]
			{
				' '
			}, StringSplitOptions.RemoveEmptyEntries);
		}
		if (!BootStrapper.RunTools(array))
		{
			Starter.m_started = false;
			SceneManager.LoadScene("Startup", 0);
		}
	}

	public string m_Tool;

	public string m_CommandParameters;
}
