﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class DisasterPropertiesPanel : UICustomControl
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		foreach (DisasterProbabilityPanel current in this.m_DisasterProbabilityPanels)
		{
			current.label = Locale.Get("DISASTER_TITLE", current.get_component().get_stringUserData());
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.m_Defaults = new ValueStorage();
		UIPanel uIPanel = base.get_component().Find<UIPanel>("ProbPanels");
		while (uIPanel.get_childCount() > 0)
		{
			UIComponent uIComponent = uIPanel.get_components()[uIPanel.get_childCount() - 1];
			UITemplateManager.RemoveInstance(DisasterPropertiesPanel.kDisasterProbTemplateName, uIComponent);
		}
		this.m_DisasterProbabilityPanels = new List<DisasterProbabilityPanel>();
		this.m_ResetButton = base.Find<UIButton>("Reset");
		this.m_ResetButton.add_eventClick(new MouseEventHandler(this.Reset));
		float num = DisasterPropertiesPanel.CalculateConversionBase(Singleton<DisasterManager>.get_instance().m_properties.m_disasterSettings);
		DisasterProperties.DisasterSettings[] disasterSettings = Singleton<DisasterManager>.get_instance().m_properties.m_disasterSettings;
		for (int i = 0; i < disasterSettings.Length; i++)
		{
			DisasterProperties.DisasterSettings disasterSettings2 = disasterSettings[i];
			int num2 = (int)((float)disasterSettings2.m_randomProbability * num);
			this.m_Defaults.Store<int>(disasterSettings2.m_disasterName, num2);
			DisasterProbabilityPanel disasterProbabilityPanel = UITemplateManager.Get<DisasterProbabilityPanel>(DisasterPropertiesPanel.kDisasterProbTemplateName);
			uIPanel.AttachUIComponent(disasterProbabilityPanel.get_gameObject());
			disasterProbabilityPanel.label = Locale.Get("DISASTER_TITLE", disasterSettings2.m_disasterName);
			disasterProbabilityPanel.get_component().set_stringUserData(disasterSettings2.m_disasterName);
			disasterProbabilityPanel.probability = num2;
			disasterProbabilityPanel.EventProbabilityChanged += new DisasterProbabilityPanel.EventProbabilityChangedHandler(this.OnProbabilityChanged);
			disasterProbabilityPanel.get_component().Show();
			this.m_DisasterProbabilityPanels.Add(disasterProbabilityPanel);
		}
	}

	private static float CalculateConversionBase(DisasterProperties.DisasterSettings[] settings)
	{
		int num = 0;
		for (int i = 0; i < settings.Length; i++)
		{
			DisasterProperties.DisasterSettings disasterSettings = settings[i];
			if (disasterSettings.m_randomProbability > num)
			{
				num = disasterSettings.m_randomProbability;
			}
		}
		if (num > 100)
		{
			return 100f / (float)num;
		}
		return 1f;
	}

	public void ResetAll()
	{
		this.Reset(null, null);
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public void Show()
	{
		Vector3 relativePosition = base.get_component().Find<UIPanel>("ProbPanels").get_relativePosition();
		base.get_component().Find<UIPanel>("ProbPanels").set_relativePosition(new Vector3(relativePosition.x, 50f, relativePosition.z));
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition2 = base.get_component().get_relativePosition();
				relativePosition2.y = val;
				base.get_component().set_relativePosition(relativePosition2);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	private void Reset(UIComponent component, UIMouseEventParameter eventParam)
	{
		foreach (DisasterProbabilityPanel current in this.m_DisasterProbabilityPanels)
		{
			current.probability = this.m_Defaults.Get<int>(current.get_component().get_stringUserData());
		}
	}

	private void OnProbabilityChanged(DisasterProbabilityPanel source)
	{
		DisasterProperties.DisasterSettings disasterSettings = default(DisasterProperties.DisasterSettings);
		disasterSettings.m_disasterName = source.get_component().get_stringUserData();
		disasterSettings.m_randomProbability = source.probability;
		int num = this.NameToIndex(disasterSettings.m_disasterName);
		Singleton<DisasterManager>.get_instance().m_properties.m_disasterSettings[num] = disasterSettings;
	}

	private int NameToIndex(string name)
	{
		DisasterProperties.DisasterSettings[] disasterSettings = Singleton<DisasterManager>.get_instance().m_properties.m_disasterSettings;
		int num = disasterSettings.Length;
		for (int i = 0; i < num; i++)
		{
			if (disasterSettings[i].m_disasterName == name)
			{
				return i;
			}
		}
		return -1;
	}

	private static readonly string kDisasterProbTemplateName = "DisasterProbTemplate";

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private ValueStorage m_Defaults;

	private UIButton m_ResetButton;

	private UIComponent m_DisasterProbTemplate;

	private List<DisasterProbabilityPanel> m_DisasterProbabilityPanels;
}
