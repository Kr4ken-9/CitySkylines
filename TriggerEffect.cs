﻿using System;
using ColossalFramework.IO;

[Serializable]
public class TriggerEffect : IDataContainer
{
	public virtual void Activate()
	{
	}

	public virtual void Serialize(DataSerializer s)
	{
	}

	public virtual void Deserialize(DataSerializer s)
	{
	}

	public virtual void AfterDeserialize(DataSerializer s)
	{
	}
}
