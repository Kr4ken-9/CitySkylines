﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class AssetEditorSettingsPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public override void RefreshPanel()
	{
		this.CleanPanel();
		this.SpawnEntry("Snapshot", 0);
		if (AssetImporterThumbnails.NeedThumbnails(ToolsModifierControl.toolController.m_editPrefabInfo))
		{
			this.SpawnEntry("Thumbshot", 1);
			this.SpawnEntry("Infoshot", 2);
		}
		base.RefreshPanel();
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		if (zOrder == 0)
		{
			SnapshotTool snapshotTool = ToolsModifierControl.SetTool<SnapshotTool>();
			snapshotTool.Mode = SnapshotTool.SnapshotMode.Snapshot;
		}
		else if (zOrder == 1)
		{
			SnapshotTool snapshotTool2 = ToolsModifierControl.SetTool<SnapshotTool>();
			snapshotTool2.Mode = SnapshotTool.SnapshotMode.Thumbnail;
		}
		else if (zOrder == 2)
		{
			SnapshotTool snapshotTool3 = ToolsModifierControl.SetTool<SnapshotTool>();
			snapshotTool3.Mode = SnapshotTool.SnapshotMode.Infotooltip;
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("DECORATIONSETTINGS_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("DECORATIONSETTINGS_DESC", name)
		});
		return base.CreateButton(name, tooltip, "AssetEditorSettings" + name, index, GeneratedPanel.tooltipBox);
	}
}
