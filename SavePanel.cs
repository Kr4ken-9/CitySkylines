﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SavePanel : LoadSavePanelBase<SaveGameMetaData>
{
	public static string lastLoadedName
	{
		set
		{
			SavePanel.m_LastSaveName = value;
		}
	}

	public static bool isSaving
	{
		get
		{
			return SavePanel.m_IsSaving;
		}
	}

	public static bool lastCloudSetting
	{
		set
		{
			SavePanel.m_LastCloudSetting = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		SavePanel.m_IsSaving = false;
		this.OnLocaleChanged();
		this.m_UseCloud = base.Find<UICheckBox>("UseCloud");
		this.m_SaveName = base.Find<UITextField>("SaveName");
		this.m_FileList = base.Find<UIListBox>("SaveList");
		this.m_CityName = base.Find<UILabel>("CityName");
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_SaveButton = base.Find<UIButton>("Save");
		this.m_SaveButton.set_isEnabled(false);
	}

	protected override void OnLocaleChanged()
	{
		if (string.IsNullOrEmpty(SavePanel.m_LastSaveName))
		{
			SavePanel.m_LastSaveName = Locale.Get("DEFAULTSAVENAMES", "NewSave");
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			string cityName = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			this.m_CityName.set_text((cityName == null) ? "Unknown" : cityName);
			base.StartCoroutine(this.Snapshot(644, 360));
		}
		else if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
		}
	}

	[DebuggerHidden]
	private IEnumerator Snapshot(int width, int height)
	{
		SavePanel.<Snapshot>c__Iterator0 <Snapshot>c__Iterator = new SavePanel.<Snapshot>c__Iterator0();
		<Snapshot>c__Iterator.width = width;
		<Snapshot>c__Iterator.height = height;
		<Snapshot>c__Iterator.$this = this;
		return <Snapshot>c__Iterator;
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveName.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnSave();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	protected override void Refresh()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
		base.ClearListing();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.SaveGameMetaData
		}))
		{
			if (!PackageHelper.IsDemoModeSave(current))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						SaveGameMetaData saveGameMetaData = current.Instantiate<SaveGameMetaData>();
						base.AddToListing(current.get_name(), saveGameMetaData.timeStamp, current, saveGameMetaData, true);
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
		}
		this.m_FileList.set_items(base.GetListingItems());
		this.m_SaveName.set_text(SavePanel.m_LastSaveName);
		this.m_UseCloud.set_isChecked(SavePanel.m_LastCloudSetting);
		this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
		this.m_SaveName.SelectAll();
	}

	public void OnSaveNameChanged(UIComponent comp, string text)
	{
		this.m_FileList.set_selectedIndex(base.FindIndexOf(text));
	}

	public void OnSaveSelectionChanged(UIComponent comp, int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_SaveName.set_text(base.GetListingName(index));
			this.m_SaveName.MoveToEnd();
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.m_SaveName.set_text(base.GetListingName(sel));
			this.OnSave();
		}
	}

	public void QuickSave(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SavePanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Quick save requested");
				SavePanel.m_IsSaving = true;
				savename = PathUtils.AddTimeStamp(savename);
				base.StartCoroutine(this.Snapshot(644, 360));
				base.StartCoroutine(this.SaveGame(savename, false));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Quick save request");
			}
		}
	}

	public void AutoSave(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SavePanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Auto save requested");
				SavePanel.m_IsSaving = true;
				base.StartCoroutine(this.Snapshot(644, 360));
				base.StartCoroutine(this.SaveGame(savename, false));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Auto save request");
			}
		}
	}

	public bool SaveGame(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SavePanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Save requested");
				SavePanel.m_IsSaving = true;
				base.StartCoroutine(this.Snapshot(644, 360));
				base.StartCoroutine(this.SaveGame(savename, false));
				return true;
			}
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring request");
		}
		return false;
	}

	private bool CheckValid()
	{
		return !StringExtensions.IsNullOrWhiteSpace(this.m_SaveName.get_text());
	}

	public void Update()
	{
		if (base.get_component().get_isVisible())
		{
			if (!PlatformService.get_active() || !PlatformService.get_cloud().get_enabled())
			{
				this.m_UseCloud.set_tooltipLocaleID("SAVEGAME_CLOUDUNSUPPORTED");
				UICheckBox arg_4E_0 = this.m_UseCloud;
				bool flag = false;
				this.m_UseCloud.set_isEnabled(flag);
				arg_4E_0.set_isChecked(flag);
			}
			else
			{
				this.m_UseCloud.set_isEnabled(true);
				this.m_UseCloud.set_tooltipLocaleID(string.Empty);
			}
			this.m_SaveButton.set_isEnabled(this.CheckValid());
		}
	}

	public void OnSave()
	{
		if (!this.CheckValid())
		{
			return;
		}
		bool useCloud = this.m_UseCloud.get_isChecked() && PlatformService.get_active() && PlatformService.get_cloud().get_enabled();
		string savePathName = SavePanel.GetSavePathName(this.m_SaveName.get_text(), useCloud);
		if (!((!useCloud) ? File.Exists(savePathName) : PlatformService.get_cloud().Exists(savePathName)))
		{
			this.SaveRoutine(this.m_SaveName.get_text(), useCloud);
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_SAVEOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveRoutine(this.m_SaveName.get_text(), useCloud);
				}
			});
		}
	}

	private void SaveRoutine(string fileName, bool useCloud)
	{
		SavePanel.m_LastSaveName = fileName;
		SavePanel.m_LastCloudSetting = useCloud;
		base.StartCoroutine(this.SaveGame(fileName, useCloud));
	}

	[DebuggerHidden]
	private IEnumerator SaveGame(string saveName, bool useCloud)
	{
		SavePanel.<SaveGame>c__Iterator1 <SaveGame>c__Iterator = new SavePanel.<SaveGame>c__Iterator1();
		<SaveGame>c__Iterator.saveName = saveName;
		<SaveGame>c__Iterator.useCloud = useCloud;
		<SaveGame>c__Iterator.$this = this;
		return <SaveGame>c__Iterator;
	}

	private static string GetSavePathName(string saveName, bool useCloud)
	{
		string text = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		if (useCloud)
		{
			return text;
		}
		return Path.Combine(DataLocation.get_saveLocation(), text);
	}

	private static string GetTempSavePath()
	{
		return Path.Combine(DataLocation.get_saveLocation(), "Temp");
	}

	public const int kPreviewWidth = 400;

	public const int kPreviewHeight = 224;

	public UIComponent[] m_TabFocusList;

	private static string m_LastSaveName = string.Empty;

	private static bool m_LastCloudSetting;

	private UIListBox m_FileList;

	private UITextField m_SaveName;

	private UITextureSprite m_SnapShotSprite;

	private UILabel m_CityName;

	private UICheckBox m_UseCloud;

	private UIButton m_SaveButton;

	private Task m_PackageSaveTask;

	private Task m_SnapshotImageTask;

	private object m_ImagesLock = new object();

	private Image m_SnapshotImage;

	private Image m_SnapshotSteamImage;

	private static volatile bool m_IsSaving;
}
