﻿using System;
using ColossalFramework;
using UnityEngine;

public struct PathUnit
{
	public void SetPosition(int index, PathUnit.Position position)
	{
		switch (index)
		{
		case 0:
			this.m_position00 = position;
			return;
		case 1:
			this.m_position01 = position;
			return;
		case 2:
			this.m_position02 = position;
			return;
		case 3:
			this.m_position03 = position;
			return;
		case 4:
			this.m_position04 = position;
			return;
		case 5:
			this.m_position05 = position;
			return;
		case 6:
			this.m_position06 = position;
			return;
		case 7:
			this.m_position07 = position;
			return;
		case 8:
			this.m_position08 = position;
			return;
		case 9:
			this.m_position09 = position;
			return;
		case 10:
			this.m_position10 = position;
			return;
		case 11:
			this.m_position11 = position;
			return;
		default:
			return;
		}
	}

	public PathUnit.Position GetPosition(int index)
	{
		switch (index)
		{
		case 0:
			return this.m_position00;
		case 1:
			return this.m_position01;
		case 2:
			return this.m_position02;
		case 3:
			return this.m_position03;
		case 4:
			return this.m_position04;
		case 5:
			return this.m_position05;
		case 6:
			return this.m_position06;
		case 7:
			return this.m_position07;
		case 8:
			return this.m_position08;
		case 9:
			return this.m_position09;
		case 10:
			return this.m_position10;
		case 11:
			return this.m_position11;
		default:
			return default(PathUnit.Position);
		}
	}

	public bool GetPosition(int index, out PathUnit.Position position)
	{
		position = this.GetPosition(index);
		NetManager instance = Singleton<NetManager>.get_instance();
		return position.m_segment != 0 && (instance.m_segments.m_buffer[(int)position.m_segment].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created && instance.m_segments.m_buffer[(int)position.m_segment].m_modifiedIndex < this.m_buildIndex;
	}

	public bool GetNextPosition(int index, out PathUnit.Position position)
	{
		if (index < (int)(this.m_positionCount - 1))
		{
			return this.GetPosition(index + 1, out position);
		}
		if (this.m_nextPathUnit != 0u)
		{
			return Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)this.m_nextPathUnit)].GetPosition(0, out position);
		}
		position = default(PathUnit.Position);
		return false;
	}

	public void MoveLastPosition(uint unitID, float distance)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		uint num = unitID;
		uint nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
		PathUnit.Position pathPos;
		if (!this.GetPosition(0, out pathPos))
		{
			return;
		}
		int num2 = 0;
		while (nextPathUnit != 0u)
		{
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetPosition((int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_positionCount - 1), out pathPos))
			{
				return;
			}
			num = nextPathUnit;
			nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
			if (++num2 >= 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				return;
			}
		}
		int positionCount = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_positionCount;
		if (positionCount > 1 && !instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetPosition(positionCount - 2, out pathPos))
		{
			return;
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetPosition(positionCount - 1, out position))
		{
			return;
		}
		if (pathPos.m_segment != 0 && position.m_segment != 0)
		{
			uint laneID = PathManager.GetLaneID(pathPos);
			uint laneID2 = PathManager.GetLaneID(position);
			Vector3 refPos = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
			byte b;
			PathUnit.CalculatePathPositionOffset(laneID2, refPos, out b);
			if (b <= position.m_offset)
			{
				float num3 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].m_bezier.Travel((float)position.m_offset * 0.003921569f, distance);
				b = (byte)Mathf.Clamp(Mathf.RoundToInt(num3 * 255f), 0, 255);
			}
			else
			{
				float num4 = 1f - instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].m_bezier.Invert().Travel(1f - (float)position.m_offset * 0.003921569f, distance);
				b = (byte)Mathf.Clamp(Mathf.RoundToInt(num4 * 255f), 0, 255);
			}
			if (b != position.m_offset)
			{
				position.m_offset = b;
				instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].SetPosition(positionCount - 1, position);
			}
		}
	}

	public bool GetLastPosition(out PathUnit.Position position)
	{
		uint num = this.m_nextPathUnit;
		if (num != 0u)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			int num2 = 0;
			uint nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
			while (nextPathUnit != 0u)
			{
				num = nextPathUnit;
				nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
				if (++num2 >= 262144)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					position = default(PathUnit.Position);
					return false;
				}
			}
			return instance.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetLastPosition(out position);
		}
		return this.GetPosition((int)(this.m_positionCount - 1), out position);
	}

	public bool CalculatePathPositionOffset(int index, Vector3 refPos, out byte offset)
	{
		PathUnit.Position pathPos;
		if (this.GetPosition(index, out pathPos))
		{
			uint laneID = PathManager.GetLaneID(pathPos);
			PathUnit.CalculatePathPositionOffset(laneID, refPos, out offset);
			return true;
		}
		offset = 0;
		return false;
	}

	public static void CalculatePathPositionOffset(uint laneID, Vector3 refPos, out byte offset)
	{
		Vector3 vector;
		float num;
		Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetClosestPosition(refPos, out vector, out num);
		offset = (byte)Mathf.Clamp(Mathf.RoundToInt(num * 255f), 0, 255);
	}

	public static bool GetNextPosition(ref uint unitID, ref int index, out PathUnit.Position position, out bool invalid)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		if (index < (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)unitID)].m_positionCount - 1))
		{
			index++;
			invalid = !instance.m_pathUnits.m_buffer[(int)((UIntPtr)unitID)].GetPosition(index, out position);
			return !invalid;
		}
		unitID = instance.m_pathUnits.m_buffer[(int)((UIntPtr)unitID)].m_nextPathUnit;
		if (unitID != 0u)
		{
			index = 0;
			invalid = !instance.m_pathUnits.m_buffer[(int)((UIntPtr)unitID)].GetPosition(index, out position);
			return !invalid;
		}
		position = default(PathUnit.Position);
		invalid = false;
		return false;
	}

	public const byte FLAG_CREATED = 1;

	public const byte FLAG_IGNORE_FLOODED = 2;

	public const byte FLAG_COMBUSTION = 4;

	public const byte FLAG_IS_HEAVY = 16;

	public const byte FLAG_IGNORE_BLOCKED = 32;

	public const byte FLAG_STABLE_PATH = 64;

	public const byte FLAG_RANDOM_PARKING = 128;

	public const byte FLAG_QUEUED = 1;

	public const byte FLAG_CALCULATING = 2;

	public const byte FLAG_READY = 4;

	public const byte FLAG_FAILED = 8;

	public const int MAX_POSITIONS = 12;

	public PathUnit.Position m_position00;

	public PathUnit.Position m_position01;

	public PathUnit.Position m_position02;

	public PathUnit.Position m_position03;

	public PathUnit.Position m_position04;

	public PathUnit.Position m_position05;

	public PathUnit.Position m_position06;

	public PathUnit.Position m_position07;

	public PathUnit.Position m_position08;

	public PathUnit.Position m_position09;

	public PathUnit.Position m_position10;

	public PathUnit.Position m_position11;

	public uint m_buildIndex;

	public uint m_nextPathUnit;

	public float m_length;

	public ushort m_vehicleTypes;

	public byte m_simulationFlags;

	public byte m_pathFindFlags;

	public byte m_laneTypes;

	public byte m_positionCount;

	public byte m_referenceCount;

	public struct Position
	{
		public ushort m_segment;

		public byte m_offset;

		public byte m_lane;
	}
}
