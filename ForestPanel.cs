﻿using System;
using ColossalFramework.UI;

public sealed class ForestPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Beautification;
		}
	}

	protected override void OnHideOptionBars()
	{
		if (this.m_OptionsBrushPanel != null)
		{
			this.m_OptionsBrushPanel.Hide();
		}
	}

	protected override void Start()
	{
		if (this.m_OptionsBar != null && this.m_OptionsBrushPanel == null)
		{
			this.m_OptionsBrushPanel = this.m_OptionsBar.Find<UIPanel>("BrushPanel");
		}
		base.Start();
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Tree, new Comparison<PrefabInfo>(base.ItemsGenericSort), true);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		TreeInfo treeInfo = objectUserData as TreeInfo;
		if (treeInfo != null)
		{
			TreeTool treeTool = ToolsModifierControl.SetTool<TreeTool>();
			if (treeTool != null)
			{
				if (this.m_OptionsBrushPanel != null)
				{
					this.m_OptionsBrushPanel.Show();
				}
				treeTool.m_prefab = treeInfo;
			}
		}
	}

	private UIPanel m_OptionsBrushPanel;
}
