﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class EventBuildingWorldInfoPanel : BuildingWorldInfoPanel
{
	public UIComponent movingPanel
	{
		get
		{
			if (this.m_movingPanel == null)
			{
				this.m_movingPanel = UIView.Find("MovingPanel");
				this.m_movingPanel.Find<UIButton>("Close").add_eventClick(new MouseEventHandler(this.OnMovingPanelCloseClicked));
				this.m_movingPanel.Hide();
			}
			return this.m_movingPanel;
		}
	}

	protected UIComponent policiesTooltip
	{
		get
		{
			if (this.m_policiesTooltip == null)
			{
				this.m_policiesTooltip = UIView.Find("FootballPoliciesTooltip");
			}
			return this.m_policiesTooltip;
		}
	}

	public bool isCityServiceEnabled
	{
		get
		{
			return Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_productionRate != 0;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && this.m_InstanceID.Building != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleBuilding(this.m_InstanceID.Building, value));
			}
		}
	}

	[DebuggerHidden]
	protected IEnumerator ToggleBuilding(ushort id, bool value)
	{
		EventBuildingWorldInfoPanel.<ToggleBuilding>c__Iterator0 <ToggleBuilding>c__Iterator = new EventBuildingWorldInfoPanel.<ToggleBuilding>c__Iterator0();
		<ToggleBuilding>c__Iterator.id = id;
		<ToggleBuilding>c__Iterator.value = value;
		return <ToggleBuilding>c__Iterator;
	}

	protected override void Start()
	{
		base.Start();
		this.m_relocateButton = base.Find<UIButton>("RelocateAction");
		this.m_RebuildButton = base.Find<UIButton>("RebuildButton");
		this.m_RebuildButton.set_isVisible(false);
		this.m_thumbnail = base.Find<UISprite>("Thumbnail");
		this.m_upkeep = base.Find<UILabel>("LabelUpkeep");
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Building building2 = instance.m_buildings.m_buffer[(int)building];
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			BuildingAI buildingAI = info.m_buildingAI;
			if ((building2.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				this.m_RebuildButton.set_tooltip((!this.IsDisasterServiceRequired()) ? LocaleFormatter.FormatCost(buildingAI.GetRelocationCost(), false) : Locale.Get("CITYSERVICE_TOOLTIP_DISASTERSERVICEREQUIRED"));
				this.m_RebuildButton.set_isVisible(Singleton<LoadingManager>.get_instance().SupportsExpansion(2));
				this.m_RebuildButton.set_isEnabled(this.CanRebuild());
				this.m_relocateButton.set_isVisible(false);
			}
			else
			{
				this.m_RebuildButton.set_isVisible(false);
				this.m_relocateButton.set_isVisible(true);
			}
			this.m_upkeep.set_text(LocaleFormatter.FormatUpkeep(buildingAI.GetResourceRate(building, ref instance.m_buildings.m_buffer[(int)building], EconomyManager.Resource.Maintenance), false));
			this.m_thumbnail.set_spriteName(info.m_InfoTooltipThumbnail);
		}
	}

	protected void TogglePolicy(DistrictPolicies.Policies policy)
	{
		if (Singleton<DistrictManager>.get_instance().IsCityPolicySet(policy))
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<DistrictManager>.get_instance().UnsetCityPolicy(policy);
			});
		}
		else
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<DistrictManager>.get_instance().SetCityPolicy(policy);
			});
		}
	}

	protected void ColorButton(UIButton b, Color c)
	{
		b.set_color(c);
		b.set_focusedColor(c);
		b.set_hoveredColor(c);
		b.set_pressedColor(c);
	}

	protected string FormatMoney(int money)
	{
		return ((double)money / 100.0).ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo());
	}

	protected string FormatMoney(ulong money)
	{
		return (money / 100.0).ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo());
	}

	public bool CanRebuild()
	{
		ushort building = this.m_InstanceID.Building;
		if (building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_levelUpProgress == 255)
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].Info;
			if (info != null)
			{
				int relocationCost = info.m_buildingAI.GetRelocationCost();
				if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, relocationCost) == relocationCost)
				{
					return true;
				}
			}
		}
		return false;
	}

	public void OnRebuildClicked()
	{
		ushort buildingID = this.m_InstanceID.Building;
		if (buildingID != 0)
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)buildingID].Info;
				if (info != null && (instance.m_buildings.m_buffer[(int)buildingID].m_flags & Building.Flags.Collapsed) != Building.Flags.None)
				{
					int relocationCost = info.m_buildingAI.GetRelocationCost();
					Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Construction, relocationCost, info.m_class);
					Vector3 position = instance.m_buildings.m_buffer[(int)buildingID].m_position;
					float angle = instance.m_buildings.m_buffer[(int)buildingID].m_angle;
					this.RebuildBuilding(info, position, angle, buildingID, info.m_fixedHeight);
					if (info.m_subBuildings != null && info.m_subBuildings.Length != 0)
					{
						Matrix4x4 matrix4x = default(Matrix4x4);
						matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
						for (int i = 0; i < info.m_subBuildings.Length; i++)
						{
							BuildingInfo buildingInfo = info.m_subBuildings[i].m_buildingInfo;
							Vector3 position2 = matrix4x.MultiplyPoint(info.m_subBuildings[i].m_position);
							float angle2 = info.m_subBuildings[i].m_angle * 0.0174532924f + angle;
							bool fixedHeight = info.m_subBuildings[i].m_fixedHeight;
							ushort num = this.RebuildBuilding(buildingInfo, position2, angle2, 0, fixedHeight);
							if (buildingID != 0 && num != 0)
							{
								instance.m_buildings.m_buffer[(int)buildingID].m_subBuilding = num;
								instance.m_buildings.m_buffer[(int)num].m_parentBuilding = buildingID;
								Building[] expr_1CD_cp_0 = instance.m_buildings.m_buffer;
								ushort expr_1CD_cp_1 = num;
								expr_1CD_cp_0[(int)expr_1CD_cp_1].m_flags = (expr_1CD_cp_0[(int)expr_1CD_cp_1].m_flags | Building.Flags.Untouchable);
								buildingID = num;
							}
						}
					}
				}
			});
		}
	}

	private ushort RebuildBuilding(BuildingInfo info, Vector3 position, float angle, ushort buildingID, bool fixedHeight)
	{
		ushort num = 0;
		bool flag = false;
		if (buildingID != 0)
		{
			Singleton<BuildingManager>.get_instance().RelocateBuilding(buildingID, position, angle);
			num = buildingID;
			flag = true;
		}
		else if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, position, angle, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			if (fixedHeight)
			{
				Building[] expr_68_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
				ushort expr_68_cp_1 = num;
				expr_68_cp_0[(int)expr_68_cp_1].m_flags = (expr_68_cp_0[(int)expr_68_cp_1].m_flags | Building.Flags.FixedHeight);
			}
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			flag = true;
		}
		if (flag)
		{
			int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
			if (publicServiceIndex != -1)
			{
				Singleton<GuideManager>.get_instance().m_serviceNotUsed[publicServiceIndex].Disable();
				Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Deactivate();
				Singleton<CoverageManager>.get_instance().CoverageUpdated(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			}
			BuildingTool.DispatchPlacementEffect(info, 0, position, angle, info.m_cellWidth, info.m_cellLength, false, false);
		}
		return num;
	}

	private bool IsDisasterServiceRequired()
	{
		ushort building = this.m_InstanceID.Building;
		return building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_levelUpProgress != 255;
	}

	protected void OnMovingPanelCloseClicked(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_isRelocating = false;
		ToolsModifierControl.GetTool<BuildingTool>().CancelRelocate();
	}

	public void OnRelocateBuilding()
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted += new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		ToolsModifierControl.keepThisWorldInfoPanel = true;
		BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
		buildingTool.m_prefab = null;
		buildingTool.m_relocate = (int)this.m_InstanceID.Building;
		this.m_isRelocating = true;
		this.TempHide();
	}

	protected void TempHide()
	{
		ToolsModifierControl.cameraController.ClearTarget();
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.33f), delegate
		{
			UIView.get_library().Hide(base.GetType().Name);
		});
		this.movingPanel.Find<UILabel>("MovingLabel").set_text(LocaleFormatter.FormatGeneric("BUILDING_MOVING", new object[]
		{
			base.buildingName
		}));
		this.movingPanel.Show();
	}

	public abstract void TempShow(Vector3 worldPosition, InstanceID instanceID);

	protected void RelocateCompleted(InstanceID newID)
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.m_isRelocating = false;
		if (!newID.IsEmpty)
		{
			this.m_InstanceID = newID;
		}
		if (base.IsValidTarget())
		{
			BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
				Vector3 worldPosition;
				Quaternion quaternion;
				Vector3 vector;
				if (InstanceManager.GetPosition(this.m_InstanceID, out worldPosition, out quaternion, out vector))
				{
					worldPosition.y += vector.y * 0.8f;
				}
				this.TempShow(worldPosition, this.m_InstanceID);
			}
		}
		else
		{
			this.movingPanel.Hide();
			BuildingTool tool2 = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool2 == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
			}
			base.Hide();
		}
	}

	protected override void OnHide()
	{
		if (this.m_isRelocating && ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.movingPanel.Hide();
	}

	private UIButton m_relocateButton;

	private UIButton m_RebuildButton;

	private UILabel m_upkeep;

	public Color m_policyActiveColor;

	public Color m_policyInactiveColor;

	private UISprite m_thumbnail;

	protected UIComponent m_movingPanel;

	protected bool m_isRelocating;

	private UIComponent m_policiesTooltip;
}
