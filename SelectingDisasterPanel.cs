﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class SelectingDisasterPanel : ToolsModifierControl
{
	public static SelectingDisasterPanel instance
	{
		get
		{
			if (SelectingDisasterPanel.m_instance == null)
			{
				SelectingDisasterPanel.m_instance = UIView.Find<UIPanel>("SelectingDisasterPanel").GetComponent<SelectingDisasterPanel>();
			}
			return SelectingDisasterPanel.m_instance;
		}
	}

	private void Awake()
	{
		SelectingDisasterPanel.m_instance = UIView.Find<UIPanel>("SelectingDisasterPanel").GetComponent<SelectingDisasterPanel>();
		this.m_label = base.Find<UILabel>("SelectingDisasterLabel");
		this.Hide();
	}

	public void SelectDisaster(InstanceID id)
	{
		this.m_currentEffectItem.OnDisasterSelected(id.Disaster);
		this.Hide();
	}

	public void Show(EffectItemDisaster effectItem)
	{
		this.m_label.set_text(LocaleFormatter.FormatGeneric("SCENARIOEDITOR_SELECTINGDISASTERFOR", new object[]
		{
			SelectedTrigger.instance.m_triggerMilestone.GetLocalizedName()
		}));
		base.get_component().set_isVisible(true);
		this.m_currentEffectItem = effectItem;
	}

	public void Hide()
	{
		base.get_component().set_isVisible(false);
		this.m_currentEffectItem = null;
	}

	private void Update()
	{
		if (this.m_currentEffectItem != null)
		{
			DefaultTool currentTool = ToolsModifierControl.GetCurrentTool<DefaultTool>();
			if (currentTool == null)
			{
				this.Hide();
			}
		}
	}

	private static SelectingDisasterPanel m_instance;

	private UILabel m_label;

	[HideInInspector]
	public EffectItemDisaster m_currentEffectItem;
}
