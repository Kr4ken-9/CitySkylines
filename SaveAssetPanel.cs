﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SaveAssetPanel : LoadSavePanelBase<CustomAssetMetaData>
{
	public static bool isSaving
	{
		get
		{
			return SaveAssetPanel.m_IsSaving;
		}
	}

	public static string lastLoadedName
	{
		set
		{
			SaveAssetPanel.m_LastSaveName = value;
		}
	}

	public static string lastLoadedAsset
	{
		set
		{
			SaveAssetPanel.m_LastAssetName = value;
		}
	}

	public static string lastAssetDescription
	{
		set
		{
			SaveAssetPanel.m_LastAssetDescription = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		SaveAssetPanel.m_IsSaving = false;
		this.OnLocaleChanged();
		this.m_AssetName = base.Find<UITextField>("AssetName");
		this.m_AssetDesc = base.Find<UITextField>("AssetDesc");
		this.m_AssetDescLabel = base.Find<UILabel>("AssetDescLabel");
		this.m_SaveName = base.Find<UITextField>("SaveName");
		this.m_TooltipImage = base.Find<UISprite>("TooltipImage");
		this.m_Thumbnail = base.Find<UIButton>("Thumbnail");
		this.m_ThumbnailContainer = base.Find<UIPanel>("ThumbnailContainer");
		this.m_CurrentThumbshotLabel = base.Find<UILabel>("CurrentThumb");
		this.m_CurrentTooltipLabel = base.Find<UILabel>("CurrentTooltip");
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_CurrentSnapShot = base.Find<UILabel>("CurrentSnapShot");
		this.m_FileList = base.Find<UIListBox>("SaveList");
		UIButton uIButton = base.Find<UIButton>("Previous");
		uIButton.add_eventClick(new MouseEventHandler(this.PreviousSnapshot));
		uIButton.add_eventDoubleClick(new MouseEventHandler(this.PreviousSnapshot));
		UIButton uIButton2 = base.Find<UIButton>("Next");
		uIButton2.add_eventClick(new MouseEventHandler(this.NextSnapshot));
		uIButton2.add_eventDoubleClick(new MouseEventHandler(this.NextSnapshot));
		this.m_PrevThumb = base.Find<UIButton>("PreviousThumb");
		this.m_PrevThumb.add_eventClick(new MouseEventHandler(this.PreviousThumb));
		this.m_PrevThumb.add_eventDoubleClick(new MouseEventHandler(this.PreviousThumb));
		this.m_NextThumb = base.Find<UIButton>("NextThumb");
		this.m_NextThumb.add_eventClick(new MouseEventHandler(this.NextThumb));
		this.m_NextThumb.add_eventDoubleClick(new MouseEventHandler(this.NextThumb));
		this.m_PrevTooltip = base.Find<UIButton>("PreviousTooltip");
		this.m_PrevTooltip.add_eventClick(new MouseEventHandler(this.PreviousTooltip));
		this.m_PrevTooltip.add_eventDoubleClick(new MouseEventHandler(this.PreviousTooltip));
		this.m_NextTooltip = base.Find<UIButton>("NextTooltip");
		this.m_NextTooltip.add_eventClick(new MouseEventHandler(this.NextTooltip));
		this.m_NextTooltip.add_eventDoubleClick(new MouseEventHandler(this.NextTooltip));
		this.m_TooltipImageLabel = base.Find<UILabel>("TooltipImageLabel");
		this.m_SaveButton = base.Find<UIButton>("Save");
		this.m_SaveButton.set_isEnabled(false);
		this.m_IgnoreChangesTimeStamp = DateTime.Now.Ticks;
	}

	protected override void OnLocaleChanged()
	{
		SaveAssetPanel.m_LastSaveName = Locale.Get("DEFAULTSAVENAMES", "NewAsset");
		SaveAssetPanel.m_LastAssetName = Locale.Get("DEFAULTSAVENAMES", "New Asset");
	}

	public override void OnClosed()
	{
		base.OnClosed();
		this.CleanStagingArea();
	}

	private void InitializeThumbnails()
	{
		if (AssetImporterThumbnails.NeedThumbnails(ToolsModifierControl.toolController.m_editPrefabInfo))
		{
			if (this.m_StagingPath == null)
			{
				this.PrepareStagingArea();
			}
			this.m_OriginalTooltipAtlas = ToolsModifierControl.toolController.m_editPrefabInfo.m_InfoTooltipAtlas;
			this.m_OriginalTooltipSprite = ToolsModifierControl.toolController.m_editPrefabInfo.m_InfoTooltipThumbnail;
			this.m_OriginalThumbnailAtlas = ToolsModifierControl.toolController.m_editPrefabInfo.m_Atlas;
			this.m_OriginalThumbnailSprite = ToolsModifierControl.toolController.m_editPrefabInfo.m_Thumbnail;
			if (this.m_OriginalTooltipAtlas != null)
			{
				this.m_CurrentTooltip = this.m_TooltipPaths.Count;
			}
			else if (this.m_TooltipPaths.Count > 0)
			{
				this.m_CurrentTooltip = 0;
			}
			else
			{
				this.m_CurrentTooltip = -1;
			}
			this.WriteCurrentTooltip();
			if (this.m_OriginalThumbnailAtlas != null)
			{
				this.m_CurrentThumbshot = this.m_ThumbshotPaths.Count;
			}
			else
			{
				this.m_CurrentThumbshot = 0;
			}
			this.WriteCurrentThumbshot();
		}
		else
		{
			this.m_ThumbnailContainer.set_isVisible(false);
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.m_CompulsoryShotBeingTaken = false;
			this.m_intersectionDecorationTask = null;
			if (ToolsModifierControl.toolController.m_editPrefabInfo != null)
			{
				IntersectionAI component = ToolsModifierControl.toolController.m_editPrefabInfo.GetComponent<IntersectionAI>();
				if (component != null)
				{
					this.m_intersectionDecorationTask = Singleton<SimulationManager>.get_instance().AddAction(this.ThreadSaveDecoration(ToolsModifierControl.toolController.m_editPrefabInfo));
				}
			}
			this.Refresh();
			this.InitializeThumbnails();
			base.get_component().CenterToParent();
			SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
			if (tool != null)
			{
				string snapShotPath = tool.snapShotPath;
				if (!string.IsNullOrEmpty(snapShotPath))
				{
					for (int i = 0; i < SaveAssetPanel.m_Extensions.Length; i++)
					{
						this.m_FileSystemReporter[i] = new FileSystemReporter("*" + SaveAssetPanel.m_Extensions[i], snapShotPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
						if (this.m_FileSystemReporter[i] != null)
						{
							this.m_FileSystemReporter[i].Start();
						}
					}
				}
			}
			this.m_AssetName.set_text(SaveAssetPanel.m_LastAssetName);
			this.m_AssetDesc.set_text(SaveAssetPanel.m_LastAssetDescription);
			if (ToolsModifierControl.toolController.m_editPrefabInfo is VehicleInfo)
			{
				this.m_AssetDesc.Hide();
				this.m_AssetDescLabel.Hide();
			}
			else
			{
				this.m_AssetDesc.Show();
				this.m_AssetDescLabel.Show();
			}
			this.m_SaveName.set_text(SaveAssetPanel.m_LastSaveName);
			this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
			this.m_SaveName.SelectAll();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			for (int j = 0; j < SaveAssetPanel.m_Extensions.Length; j++)
			{
				if (this.m_FileSystemReporter[j] != null)
				{
					this.m_FileSystemReporter[j].Stop();
					this.m_FileSystemReporter[j].Dispose();
					this.m_FileSystemReporter[j] = null;
				}
			}
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveName.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnSave();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.FetchSnapshots();
		});
	}

	private void FetchSnapshots()
	{
		string b = (this.m_SnapshotPaths.Count <= 0) ? null : this.m_SnapshotPaths[this.m_CurrentSnapshot];
		this.m_CurrentSnapshot = 0;
		this.m_SnapshotPaths.Clear();
		this.m_ThumbshotPaths.Clear();
		this.m_TooltipPaths.Clear();
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null)
		{
			string snapShotPath = tool.snapShotPath;
			if (snapShotPath != null)
			{
				FileInfo[] fileInfo = SaveHelper.GetFileInfo(snapShotPath);
				if (fileInfo != null)
				{
					FileInfo[] array = fileInfo;
					for (int i = 0; i < array.Length; i++)
					{
						FileInfo fileInfo2 = array[i];
						if (string.Compare(Path.GetExtension(fileInfo2.Name), ".png", StringComparison.OrdinalIgnoreCase) == 0)
						{
							if (Path.GetFileName(fileInfo2.Name).Contains("snapshot"))
							{
								this.m_SnapshotPaths.Add(fileInfo2.FullName);
								if (fileInfo2.FullName == b)
								{
									this.m_CurrentSnapshot = this.m_SnapshotPaths.Count - 1;
								}
							}
							else if (Path.GetFileName(fileInfo2.Name).Contains("thumbnail"))
							{
								this.m_ThumbshotPaths.Add(fileInfo2.FullName);
							}
							else if (Path.GetFileName(fileInfo2.Name).Contains("tooltip"))
							{
								this.m_TooltipPaths.Add(fileInfo2.FullName);
							}
						}
					}
				}
				UIComponent arg_1A1_0 = base.Find("Previous");
				bool isEnabled = this.m_SnapshotPaths.Count > 1;
				base.Find("Next").set_isEnabled(isEnabled);
				arg_1A1_0.set_isEnabled(isEnabled);
				this.RefreshSnapshot();
			}
		}
	}

	private void CheckCompulsoryShots()
	{
		bool flag = AssetImporterThumbnails.NeedThumbnails(ToolsModifierControl.toolController.m_editPrefabInfo);
		if (flag && this.m_ThumbshotPaths.Count == 0 && !this.m_CompulsoryShotBeingTaken)
		{
			base.StartCoroutine(this.TakeCompulsoryShot(SnapshotTool.SnapshotMode.Thumbnail));
		}
		if (flag && this.m_TooltipPaths.Count == 0 && !this.m_CompulsoryShotBeingTaken)
		{
			base.StartCoroutine(this.TakeCompulsoryShot(SnapshotTool.SnapshotMode.Infotooltip));
		}
		if (this.m_SnapshotPaths.Count == 0 && !this.m_CompulsoryShotBeingTaken)
		{
			base.StartCoroutine(this.TakeCompulsoryShot(SnapshotTool.SnapshotMode.Snapshot));
		}
		this.m_ThumbshotAtlases = new UITextureAtlas[this.m_ThumbshotPaths.Count];
		this.m_TooltipShotAtlases = new UITextureAtlas[this.m_TooltipPaths.Count];
	}

	private void OnCompulsoryShotFinished()
	{
		ToolsModifierControl.SetTool<DefaultTool>();
		Singleton<GameAreaManager>.get_instance().SkipFading();
		Camera camera = Singleton<RenderManager>.get_instance().CurrentCameraInfo.m_camera;
		camera.get_transform().set_position(this.m_StoredCamPos);
		camera.get_transform().set_rotation(this.m_StoredCamRot);
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		CameraController cameraController = null;
		if (gameObject != null)
		{
			cameraController = gameObject.GetComponent<CameraController>();
		}
		if (cameraController != null)
		{
			cameraController.set_enabled(true);
		}
		this.m_CompulsoryShotBeingTaken = false;
		this.Refresh();
		this.InitializeThumbnails();
	}

	[DebuggerHidden]
	private IEnumerator TakeCompulsoryShot(SnapshotTool.SnapshotMode mode)
	{
		SaveAssetPanel.<TakeCompulsoryShot>c__Iterator0 <TakeCompulsoryShot>c__Iterator = new SaveAssetPanel.<TakeCompulsoryShot>c__Iterator0();
		<TakeCompulsoryShot>c__Iterator.mode = mode;
		<TakeCompulsoryShot>c__Iterator.$this = this;
		return <TakeCompulsoryShot>c__Iterator;
	}

	private void RefreshSnapshot()
	{
		if (this.m_SnapShotSprite.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShotSprite.get_texture());
		}
		if (this.m_SnapshotPaths.Count > 0)
		{
			Image image = new Image(this.m_SnapshotPaths[this.m_CurrentSnapshot]);
			image.Resize(400, 224);
			this.m_SnapShotSprite.set_texture(image.CreateTexture());
			this.m_CurrentSnapShot.set_text(this.m_CurrentSnapshot + 1 + " / " + this.m_SnapshotPaths.Count);
		}
		else
		{
			this.m_SnapShotSprite.set_texture(Object.Instantiate<Texture>(this.m_DefaultAssetPreviewTexture) as Texture2D);
			this.m_CurrentSnapShot.set_text(Locale.Get("NO_SNAPSHOTS"));
		}
	}

	private int GetThumbshotCount()
	{
		return (!(this.m_OriginalThumbnailAtlas != null)) ? this.m_ThumbshotPaths.Count : (this.m_ThumbshotPaths.Count + 1);
	}

	private int GetTooltipShotCount()
	{
		return (!(this.m_OriginalTooltipAtlas != null)) ? this.m_TooltipPaths.Count : (this.m_TooltipPaths.Count + 1);
	}

	private void PreviousThumb(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentThumbshot = (this.m_CurrentThumbshot - 1) % this.GetThumbshotCount();
		if (this.m_CurrentThumbshot < 0)
		{
			this.m_CurrentThumbshot += this.GetThumbshotCount();
		}
		this.WriteCurrentThumbshot();
	}

	private void NextThumb(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentThumbshot = (this.m_CurrentThumbshot + 1) % this.GetThumbshotCount();
		this.WriteCurrentThumbshot();
	}

	private void PreviousTooltip(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentTooltip = (this.m_CurrentTooltip - 1) % this.GetTooltipShotCount();
		if (this.m_CurrentTooltip < 0)
		{
			this.m_CurrentTooltip += this.GetTooltipShotCount();
		}
		this.WriteCurrentTooltip();
	}

	private void NextTooltip(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentTooltip = (this.m_CurrentTooltip + 1) % this.GetTooltipShotCount();
		this.WriteCurrentTooltip();
	}

	private void PreviousSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot - 1) % this.m_SnapshotPaths.Count;
		if (this.m_CurrentSnapshot < 0)
		{
			this.m_CurrentSnapshot += this.m_SnapshotPaths.Count;
		}
		this.RefreshSnapshot();
	}

	private void NextSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot + 1) % this.m_SnapshotPaths.Count;
		this.RefreshSnapshot();
	}

	protected override void Refresh()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
		this.FetchSnapshots();
		if (base.get_component().get_isVisible())
		{
			this.CheckCompulsoryShots();
		}
		base.ClearListing();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.CustomAssetMetaData
		}))
		{
			if (current != null && current.get_isEnabled() && current.get_isMainAsset())
			{
				try
				{
					CustomAssetMetaData customAssetMetaData = current.Instantiate<CustomAssetMetaData>();
					base.AddToListing(current.get_name(), customAssetMetaData.timeStamp, current, customAssetMetaData, true);
				}
				catch (Exception ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
				}
			}
		}
		this.m_FileList.set_items(base.GetListingItems());
		this.m_SaveName.set_text(SaveAssetPanel.m_LastSaveName);
		this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
		this.m_SaveName.SelectAll();
	}

	public void OnSaveNameChanged(UIComponent comp, string text)
	{
		this.m_FileList.set_selectedIndex(base.FindIndexOf(text));
	}

	public void OnSaveSelectionChanged(UIComponent comp, int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_SaveName.set_text(base.GetListingName(index));
			this.m_SaveName.MoveToEnd();
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.m_SaveName.set_text(base.GetListingName(sel));
			this.OnSave();
		}
	}

	private bool CheckValid()
	{
		bool flag = AssetImporterThumbnails.NeedThumbnails(ToolsModifierControl.toolController.m_editPrefabInfo);
		return !StringExtensions.IsNullOrWhiteSpace(this.m_SaveName.get_text()) && !StringExtensions.IsNullOrWhiteSpace(this.m_AssetName.get_text()) && !this.m_CompulsoryShotBeingTaken && (!flag || (this.m_ThumbshotPaths.Count > 0 && this.m_TooltipPaths.Count > 0)) && this.m_SnapshotPaths.Count > 0;
	}

	public void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_SaveButton.set_isEnabled(this.CheckValid());
		}
	}

	public void OnSave()
	{
		if (!this.CheckValid())
		{
			return;
		}
		string savePathName = SaveAssetPanel.GetSavePathName(this.m_SaveName.get_text());
		if (!File.Exists(savePathName))
		{
			this.SaveRoutine(this.m_SaveName.get_text(), this.m_AssetName.get_text(), this.m_AssetDesc.get_text());
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_SAVEOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveRoutine(this.m_SaveName.get_text(), this.m_AssetName.get_text(), this.m_AssetDesc.get_text());
				}
			});
		}
		this.CleanStagingArea();
	}

	public void AutoSave(string fileName)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SaveAssetPanel.m_IsSaving)
			{
				SaveAssetPanel.m_IsSaving = true;
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Auto save requested");
				base.StartCoroutine(this.SaveAsset(fileName, string.IsNullOrEmpty(SaveAssetPanel.m_LastAssetName) ? "AutoSavedAsset" : SaveAssetPanel.m_LastAssetName, string.IsNullOrEmpty(SaveAssetPanel.m_LastAssetDescription) ? "Autosave" : SaveAssetPanel.m_LastAssetDescription, true));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Auto save request");
			}
		}
	}

	private void SaveRoutine(string fileName, string mapName, string description)
	{
		SaveAssetPanel.m_LastSaveName = fileName;
		SaveAssetPanel.m_LastAssetName = mapName;
		SaveAssetPanel.m_LastAssetDescription = description;
		base.StartCoroutine(this.SaveAsset(fileName, mapName, description, false));
	}

	[DebuggerHidden]
	private IEnumerator ThreadSaveDecoration(PrefabInfo newInfo)
	{
		SaveAssetPanel.<ThreadSaveDecoration>c__Iterator1 <ThreadSaveDecoration>c__Iterator = new SaveAssetPanel.<ThreadSaveDecoration>c__Iterator1();
		<ThreadSaveDecoration>c__Iterator.newInfo = newInfo;
		return <ThreadSaveDecoration>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SaveAsset(string saveName, string assetName, string description, bool ignoreThumbnail)
	{
		SaveAssetPanel.<SaveAsset>c__Iterator2 <SaveAsset>c__Iterator = new SaveAssetPanel.<SaveAsset>c__Iterator2();
		<SaveAsset>c__Iterator.ignoreThumbnail = ignoreThumbnail;
		<SaveAsset>c__Iterator.saveName = saveName;
		<SaveAsset>c__Iterator.assetName = assetName;
		<SaveAsset>c__Iterator.description = description;
		<SaveAsset>c__Iterator.$this = this;
		return <SaveAsset>c__Iterator;
	}

	private SaveAssetPanel.TempAtlasData TempNullAtlas(PrefabInfo info)
	{
		SaveAssetPanel.TempAtlasData result = default(SaveAssetPanel.TempAtlasData);
		result.m_Atlas = info.m_Atlas;
		result.m_InfoTooltipAtlas = info.m_InfoTooltipAtlas;
		result.m_Thumbnail = info.m_Thumbnail;
		result.m_InfoTooltipThumbnail = info.m_InfoTooltipThumbnail;
		info.m_Atlas = null;
		info.m_InfoTooltipAtlas = null;
		info.m_Thumbnail = null;
		info.m_InfoTooltipThumbnail = null;
		return result;
	}

	private void RestoreAtlas(PrefabInfo info, SaveAssetPanel.TempAtlasData atlasData)
	{
		info.m_Atlas = atlasData.m_Atlas;
		info.m_InfoTooltipAtlas = atlasData.m_InfoTooltipAtlas;
		info.m_Thumbnail = atlasData.m_Thumbnail;
		info.m_InfoTooltipThumbnail = atlasData.m_InfoTooltipThumbnail;
	}

	private GameObject GetLODObject()
	{
		PrefabInfo editPrefabInfo = ToolsModifierControl.toolController.m_editPrefabInfo;
		Type typeFromHandle = typeof(PrefabInfo);
		if (typeFromHandle == typeof(BuildingInfo))
		{
			return ((BuildingInfo)editPrefabInfo).m_lodObject;
		}
		if (typeFromHandle == typeof(VehicleInfo))
		{
			return ((VehicleInfo)editPrefabInfo).m_lodObject;
		}
		if (typeFromHandle == typeof(PropInfo))
		{
			return ((PropInfo)editPrefabInfo).m_lodObject;
		}
		return null;
	}

	private void PerformanceWarning(int triangles, int textureHeight, int textureWidth, int lodTriangles, int lodTextureHeight, int lodTextureWidth)
	{
		CustomAssetMetaData.Type type = CustomAssetMetaData.Type.Unknown;
		Type type2 = ToolsModifierControl.toolController.m_editPrefabInfo.GetType();
		if (type2 == typeof(BuildingInfo))
		{
			type = CustomAssetMetaData.Type.Building;
		}
		if (type2 == typeof(VehicleInfo))
		{
			type = CustomAssetMetaData.Type.Vehicle;
		}
		if (type2 == typeof(PropInfo))
		{
			type = CustomAssetMetaData.Type.Prop;
		}
		if (type2 == typeof(TreeInfo))
		{
			type = CustomAssetMetaData.Type.Tree;
		}
		string text = AssetWarning.WarningMessage(type, triangles, textureHeight, textureWidth, lodTriangles, lodTextureHeight, lodTextureWidth);
		if (text != string.Empty)
		{
			UIView.get_library().ShowModal<ExceptionPanel>("ExceptionPanel").SetMessage("Warning!", StringUtils.SafeFormat("{0}" + Locale.Get("ASSET_WARNING_SAVING"), text), false);
		}
	}

	private static string GetSavePathName(string saveName)
	{
		string path = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		return Path.Combine(DataLocation.get_assetsPath(), path);
	}

	private void WriteCurrentThumbshot()
	{
		int thumbshotCount = this.GetThumbshotCount();
		if (this.m_CurrentThumbshot == this.m_ThumbshotPaths.Count)
		{
			if (this.m_OriginalThumbnailAtlas != null && this.m_OriginalThumbnailSprite != null)
			{
				this.m_Thumbnail.set_atlas(this.m_OriginalThumbnailAtlas);
				this.m_Thumbnail.set_normalFgSprite(this.m_OriginalThumbnailSprite);
				this.m_Thumbnail.set_focusedFgSprite(this.m_OriginalThumbnailSprite + "Focused");
				this.m_Thumbnail.set_hoveredFgSprite(this.m_OriginalThumbnailSprite + "Hovered");
				this.m_Thumbnail.set_pressedFgSprite(this.m_OriginalThumbnailSprite + "Pressed");
				this.m_Thumbnail.set_disabledFgSprite(this.m_OriginalThumbnailSprite + "Disabled");
				this.m_CurrentThumbshotLabel.set_text("1 / " + thumbshotCount);
				this.m_ThumbnailContainer.set_isVisible(true);
				this.m_NextThumb.set_isVisible(thumbshotCount > 1);
				this.m_PrevThumb.set_isVisible(thumbshotCount > 1);
				this.WriteThumbnail(true);
			}
			else
			{
				this.m_ThumbnailContainer.set_isVisible(false);
			}
		}
		else if (this.m_CurrentThumbshot >= 0 && this.m_CurrentThumbshot < this.m_ThumbshotPaths.Count)
		{
			this.m_CurrentThumbshotLabel.set_text(((!(this.m_OriginalThumbnailAtlas != null)) ? 1 : 2) + this.m_CurrentThumbshot + " / " + thumbshotCount);
			if (this.m_ThumbshotAtlases[this.m_CurrentThumbshot] != null)
			{
				this.m_Thumbnail.set_atlas(this.m_ThumbshotAtlases[this.m_CurrentThumbshot]);
				this.m_Thumbnail.set_normalFgSprite("thumb");
				this.m_Thumbnail.set_focusedFgSprite("thumbFocused");
				this.m_Thumbnail.set_hoveredFgSprite("thumbHovered");
				this.m_Thumbnail.set_pressedFgSprite("thumbPressed");
				this.m_Thumbnail.set_disabledFgSprite("thumbDisabled");
				this.m_ThumbnailContainer.set_isVisible(true);
				this.m_NextThumb.set_isVisible(thumbshotCount > 1);
				this.m_PrevThumb.set_isVisible(thumbshotCount > 1);
				this.WriteThumbnail(true);
			}
			else
			{
				Image image = new Image(this.m_ThumbshotPaths[this.m_CurrentThumbshot]);
				if (image.get_width() > 0 && image.get_height() > 0)
				{
					File.WriteAllBytes(this.m_ThumbPath + ".png", image.GetFormattedImage(0));
				}
				this.ReloadThumbnail(true);
			}
		}
	}

	private void WriteCurrentTooltip()
	{
		int tooltipShotCount = this.GetTooltipShotCount();
		if (this.m_CurrentTooltip == this.m_TooltipPaths.Count)
		{
			this.m_TooltipImage.set_atlas(this.m_OriginalTooltipAtlas);
			this.m_TooltipImage.set_spriteName(this.m_OriginalTooltipSprite);
			this.m_CurrentTooltipLabel.set_text("1 / " + tooltipShotCount);
			this.WriteTooltip();
			this.m_TooltipImage.set_isVisible(true);
			this.m_TooltipImageLabel.set_isVisible(true);
			this.m_NextTooltip.set_isVisible(tooltipShotCount > 1);
			this.m_PrevTooltip.set_isVisible(tooltipShotCount > 1);
			this.m_CurrentTooltipLabel.set_isVisible(true);
		}
		else if (this.m_CurrentTooltip >= 0 && this.m_CurrentTooltip < this.m_TooltipPaths.Count)
		{
			if (this.m_TooltipShotAtlases[this.m_CurrentTooltip] != null)
			{
				this.m_TooltipImage.set_atlas(this.m_TooltipShotAtlases[this.m_CurrentTooltip]);
				this.m_TooltipImage.set_spriteName("tooltip");
				this.WriteTooltip();
			}
			else
			{
				Image image = new Image(this.m_TooltipPaths[this.m_CurrentTooltip]);
				if (image.get_width() > 0 && image.get_height() > 0)
				{
					File.WriteAllBytes(Path.Combine(this.m_StagingPath, "asset_tooltip.png"), image.GetFormattedImage(0));
				}
			}
			this.m_CurrentTooltipLabel.set_text(((!(this.m_OriginalTooltipAtlas != null)) ? 1 : 2) + this.m_CurrentTooltip + " / " + tooltipShotCount);
			this.m_TooltipImage.set_isVisible(true);
			this.m_TooltipImageLabel.set_isVisible(true);
			this.m_NextTooltip.set_isVisible(tooltipShotCount > 1);
			this.m_PrevTooltip.set_isVisible(tooltipShotCount > 1);
			this.m_CurrentTooltipLabel.set_isVisible(true);
			this.ReloadTooltip();
		}
		else
		{
			this.m_TooltipImageLabel.set_isVisible(true);
			this.m_CurrentTooltipLabel.set_isVisible(true);
			this.m_CurrentTooltipLabel.set_text(Locale.Get("NO_TOOLTIPIMAGES"));
			this.m_TooltipImage.set_isVisible(false);
			this.m_NextTooltip.set_isVisible(false);
			this.m_PrevTooltip.set_isVisible(false);
		}
	}

	private void WriteTooltip()
	{
		if (this.m_TooltipImage != null && this.m_TooltipImage.get_atlas() != null)
		{
			UITextureAtlas.SpriteInfo spriteInfo = this.m_TooltipImage.get_atlas().get_Item(this.m_TooltipImage.get_spriteName());
			if (spriteInfo != null)
			{
				Texture2D texture = spriteInfo.get_texture();
				if (texture != null)
				{
					File.WriteAllBytes(Path.Combine(this.m_StagingPath, "asset_tooltip.png"), texture.EncodeToPNG());
				}
			}
		}
		this.m_IgnoreChangesTimeStamp = DateTime.Now.Ticks + 20000000L;
	}

	private void WriteThumbnail(bool writeNormal)
	{
		if (this.m_Thumbnail != null && this.m_Thumbnail.get_atlas() != null)
		{
			Texture2D texture;
			if (writeNormal)
			{
				texture = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_normalFgSprite()).get_texture();
				if (texture != null)
				{
					File.WriteAllBytes(this.m_ThumbPath + ".png", texture.EncodeToPNG());
				}
			}
			texture = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_focusedFgSprite()).get_texture();
			if (texture != null)
			{
				File.WriteAllBytes(this.m_ThumbPath + "_focused.png", texture.EncodeToPNG());
			}
			texture = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_hoveredFgSprite()).get_texture();
			if (texture != null)
			{
				File.WriteAllBytes(this.m_ThumbPath + "_hovered.png", texture.EncodeToPNG());
			}
			texture = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_pressedFgSprite()).get_texture();
			if (texture != null)
			{
				File.WriteAllBytes(this.m_ThumbPath + "_pressed.png", texture.EncodeToPNG());
			}
			texture = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_disabledFgSprite()).get_texture();
			if (texture != null)
			{
				File.WriteAllBytes(this.m_ThumbPath + "_disabled.png", texture.EncodeToPNG());
			}
		}
		this.m_IgnoreChangesTimeStamp = DateTime.Now.Ticks + 20000000L;
	}

	private void PrepareStagingArea()
	{
		string path = Guid.NewGuid().ToString();
		this.m_StagingPath = Path.Combine(Path.Combine(DataLocation.get_localApplicationData(), "AssetStagingArea"), path);
		Directory.CreateDirectory(this.m_StagingPath);
		this.m_ThumbPath = Path.Combine(this.m_StagingPath, "asset_thumb");
		this.m_ThumbName = "thumb";
		this.m_ThumbReporter = new FileSystemReporter("*.png", this.m_StagingPath, new FileSystemReporter.ReporterEventHandler(this.RefreshStaging));
		if (this.m_ThumbReporter != null)
		{
			this.m_ThumbReporter.Start();
		}
	}

	public void OnOpenThumbnailFolder(UIComponent component, UIMouseEventParameter p)
	{
		Utils.OpenInFileBrowser(this.m_StagingPath);
	}

	private void RefreshStaging(object sender, ReporterEventArgs e)
	{
		FileSystemEventArgs fileSystemEventArgs = e.get_arguments() as FileSystemEventArgs;
		if (DateTime.Now.Ticks > this.m_IgnoreChangesTimeStamp && (fileSystemEventArgs.ChangeType == WatcherChangeTypes.Changed || fileSystemEventArgs.ChangeType == WatcherChangeTypes.Created))
		{
			string text = fileSystemEventArgs.FullPath;
			text = Path.GetFileNameWithoutExtension(text);
			int num = text.IndexOf("thumb");
			int num2 = text.IndexOf("tooltip");
			if (num >= 0)
			{
				string type = text.Substring(num + 5);
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.ReloadThumbnail(type.Equals(string.Empty));
				});
			}
			else if (num2 >= 0)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.ReloadTooltip();
				});
			}
		}
	}

	private void CleanStagingArea()
	{
		if (this.m_ThumbReporter != null)
		{
			this.m_ThumbReporter.Stop();
			this.m_ThumbReporter.Dispose();
			this.m_ThumbReporter = null;
		}
		try
		{
			if (this.m_StagingPath != null && !this.m_StagingPath.Equals(string.Empty))
			{
				DirectoryUtils.DeleteDirectory(this.m_StagingPath);
				this.m_StagingPath = null;
			}
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Failed to delete " + this.m_StagingPath + "\n" + ex.ToString());
		}
	}

	private void ReloadTooltip()
	{
		string text = Path.Combine(this.m_StagingPath, "asset_tooltip.png");
		if (File.Exists(text))
		{
			Image image = new Image(text);
			if (image != null && image.get_width() > 0 && image.get_height() > 0)
			{
				image.Resize(SaveAssetPanel.tooltipWidth, SaveAssetPanel.tooltipHeight);
				Texture2D texture2D = image.CreateTexture();
				texture2D.set_name("tooltip");
				this.m_TooltipImage.set_atlas(AssetImporterThumbnails.CreateThumbnailAtlas(new Texture2D[]
				{
					texture2D
				}, "tooltip"));
				this.m_TooltipImage.set_spriteName("tooltip");
				this.m_TooltipImage.set_isVisible(true);
				if (this.m_CurrentTooltip == this.m_TooltipPaths.Count)
				{
					this.m_OriginalTooltipAtlas = this.m_TooltipImage.get_atlas();
					this.m_OriginalTooltipSprite = "tooltip";
				}
				else if (this.m_CurrentTooltip >= 0 && this.m_CurrentTooltip < this.m_TooltipPaths.Count)
				{
					this.m_TooltipShotAtlases[this.m_CurrentTooltip] = this.m_TooltipImage.get_atlas();
				}
			}
			else
			{
				this.m_TooltipImage.set_isVisible(false);
			}
		}
		else
		{
			this.m_TooltipImage.set_isVisible(false);
		}
	}

	private void ReloadThumbnail(bool regenerate)
	{
		Texture2D texture2D = null;
		Texture2D texture2D2 = null;
		Texture2D texture2D3 = null;
		Texture2D texture2D4 = null;
		Texture2D texture2D5 = AssetImporterThumbnails.LoadThumbnail(string.Empty, Path.Combine(this.m_StagingPath, "asset"));
		texture2D5.set_name(this.m_ThumbName);
		if (!regenerate)
		{
			texture2D2 = AssetImporterThumbnails.LoadThumbnail("focused", Path.Combine(this.m_StagingPath, "asset"));
			texture2D2.set_name("thumbFocused");
			texture2D3 = AssetImporterThumbnails.LoadThumbnail("hovered", Path.Combine(this.m_StagingPath, "asset"));
			texture2D3.set_name("thumbHovered");
			texture2D4 = AssetImporterThumbnails.LoadThumbnail("pressed", Path.Combine(this.m_StagingPath, "asset"));
			texture2D4.set_name("thumbPressed");
			texture2D = AssetImporterThumbnails.LoadThumbnail("disabled", Path.Combine(this.m_StagingPath, "asset"));
			texture2D.set_name("thumbDisabled");
		}
		Texture2D[] textures = new Texture2D[]
		{
			texture2D5,
			texture2D2,
			texture2D3,
			texture2D4,
			texture2D
		};
		AssetImporterThumbnails.GenerateMissingThumbnailVariants(ref textures);
		this.m_Thumbnail.set_atlas(AssetImporterThumbnails.CreateThumbnailAtlas(textures, ToolsModifierControl.toolController.m_editPrefabInfo.get_name()));
		this.m_Thumbnail.set_normalFgSprite("thumb");
		this.m_Thumbnail.set_focusedFgSprite("thumbFocused");
		this.m_Thumbnail.set_hoveredFgSprite("thumbHovered");
		this.m_Thumbnail.set_pressedFgSprite("thumbPressed");
		this.m_Thumbnail.set_disabledFgSprite("thumbDisabled");
		if (this.m_CurrentThumbshot == this.m_ThumbshotPaths.Count)
		{
			this.m_OriginalThumbnailAtlas = this.m_Thumbnail.get_atlas();
			this.m_OriginalThumbnailSprite = this.m_ThumbName;
		}
		else
		{
			this.m_ThumbshotAtlases[this.m_CurrentThumbshot] = this.m_Thumbnail.get_atlas();
		}
		this.m_ThumbnailContainer.set_isVisible(true);
		int thumbshotCount = this.GetThumbshotCount();
		this.m_NextThumb.set_isVisible(thumbshotCount > 1);
		this.m_PrevThumb.set_isVisible(thumbshotCount > 1);
		if (regenerate)
		{
			this.WriteThumbnail(false);
		}
	}

	public void OpenSnapshotFolders()
	{
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null && !string.IsNullOrEmpty(tool.snapShotPath))
		{
			Utils.OpenInFileBrowser(tool.snapShotPath);
		}
	}

	private List<BuildingInfo.SubInfo> InstantiateSubBuildings(string saveName)
	{
		List<BuildingInfo.SubInfo> list = new List<BuildingInfo.SubInfo>();
		Dictionary<BuildingInfo, BuildingInfo> dictionary = new Dictionary<BuildingInfo, BuildingInfo>();
		BuildingInfo buildingInfo = ToolsModifierControl.toolController.m_editPrefabInfo as BuildingInfo;
		if (buildingInfo != null)
		{
			BuildingInfo.SubInfo[] subBuildings = buildingInfo.m_subBuildings;
			for (int i = 0; i < subBuildings.Length; i++)
			{
				BuildingInfo.SubInfo subInfo = subBuildings[i];
				if (subInfo.m_buildingInfo != null)
				{
					BuildingInfo.SubInfo subInfo2 = new BuildingInfo.SubInfo();
					if (!dictionary.ContainsKey(subInfo.m_buildingInfo))
					{
						GameObject gameObject = this.InstantiateSubBuilding(subInfo.m_buildingInfo.get_gameObject(), saveName);
						subInfo2.m_buildingInfo = gameObject.GetComponent<BuildingInfo>();
						dictionary[subInfo.m_buildingInfo] = subInfo2.m_buildingInfo;
					}
					else
					{
						subInfo2.m_buildingInfo = dictionary[subInfo.m_buildingInfo];
					}
					subInfo2.m_position = subInfo.m_position;
					subInfo2.m_angle = subInfo.m_angle;
					subInfo2.m_fixedHeight = subInfo.m_fixedHeight;
					list.Add(subInfo2);
				}
			}
		}
		return list;
	}

	private GameObject InstantiateSubBuilding(GameObject template, string saveName)
	{
		GameObject gameObject = Object.Instantiate<GameObject>(template);
		int num = template.get_name().LastIndexOf(".");
		string name = (num >= 0) ? template.get_name().Remove(0, num + 1) : template.get_name();
		gameObject.set_name(name);
		gameObject.SetActive(false);
		BuildingInfo component = gameObject.GetComponent<BuildingInfo>();
		component.m_generatedInfo = ScriptableObject.CreateInstance<BuildingInfoGen>();
		component.m_generatedInfo.set_name(gameObject.get_name() + " (GeneratedInfo)");
		component.CalculateGeneratedInfo();
		GameObject lodObject = template.GetComponent<BuildingInfo>().m_lodObject;
		if (lodObject != null)
		{
			component.m_lodObject = Object.Instantiate<GameObject>(lodObject);
			component.m_lodObject.set_name(lodObject.get_name());
			component.m_lodObject.SetActive(false);
			Renderer component2 = component.m_lodObject.GetComponent<Renderer>();
			if (component2 != null && component2.get_sharedMaterial() != null)
			{
				component2.set_sharedMaterial(component2.get_material());
			}
		}
		Renderer component3 = component.GetComponent<Renderer>();
		if (component3 != null && component3.get_sharedMaterial() != null)
		{
			component3.set_sharedMaterial(component3.get_material());
		}
		component.m_subBuildings = new BuildingInfo.SubInfo[0];
		component.m_placementStyle = ItemClass.Placement.Procedural;
		component.m_availableIn = ItemClass.Availability.Game;
		component.CheckReferences();
		component.InitializePrefab();
		component.m_prefabInitialized = true;
		component.m_InfoTooltipAtlas = null;
		component.m_InfoTooltipThumbnail = null;
		component.m_Atlas = null;
		component.m_Thumbnail = null;
		return component.get_gameObject();
	}

	private string StripName(string name)
	{
		int num = name.LastIndexOf(".");
		if (num >= 0)
		{
			return name.Remove(0, num + 1);
		}
		return name;
	}

	public const int kPreviewWidth = 400;

	public const int kPreviewHeight = 224;

	public static readonly int tooltipWidth = 492;

	public static readonly int tooltipHeight = 147;

	public UITextureAtlas thumbnailAtlas;

	public Texture m_DefaultAssetPreviewTexture;

	public UIComponent[] m_TabFocusList;

	private static string m_LastSaveName = "NewAsset";

	private static string m_LastAssetName = "New Asset";

	private static string m_LastAssetDescription = string.Empty;

	private static readonly string[] m_Extensions = Image.GetExtensions(94);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[SaveAssetPanel.m_Extensions.Length];

	private UITextureSprite m_SnapShotSprite;

	private List<string> m_SnapshotPaths = new List<string>();

	private UILabel m_CurrentSnapShot;

	private int m_CurrentSnapshot;

	private UIPanel m_ThumbnailContainer;

	private UIButton m_Thumbnail;

	private List<string> m_ThumbshotPaths = new List<string>();

	private int m_CurrentThumbshot;

	private UILabel m_CurrentThumbshotLabel;

	private UIButton m_PrevThumb;

	private UIButton m_NextThumb;

	private UISprite m_TooltipImage;

	private List<string> m_TooltipPaths = new List<string>();

	private int m_CurrentTooltip;

	private UILabel m_CurrentTooltipLabel;

	private UILabel m_TooltipImageLabel;

	private UIButton m_PrevTooltip;

	private UIButton m_NextTooltip;

	private UIButton m_SaveButton;

	private bool m_CompulsoryShotBeingTaken;

	private Vector3 m_StoredCamPos;

	private Quaternion m_StoredCamRot;

	private AsyncTask m_intersectionDecorationTask;

	private UITextField m_AssetName;

	private UITextField m_AssetDesc;

	private UILabel m_AssetDescLabel;

	private UITextureAtlas m_OriginalThumbnailAtlas;

	private string m_OriginalThumbnailSprite;

	private UITextureAtlas[] m_ThumbshotAtlases;

	private UITextureAtlas m_OriginalTooltipAtlas;

	private string m_OriginalTooltipSprite;

	private UITextureAtlas[] m_TooltipShotAtlases;

	private UIListBox m_FileList;

	private UITextField m_SaveName;

	private Task m_PackageSaveTask;

	private string m_StagingPath;

	private string m_ThumbPath;

	private string m_ThumbName;

	private FileSystemReporter m_ThumbReporter;

	private long m_IgnoreChangesTimeStamp;

	private static volatile bool m_IsSaving;

	public struct TempAtlasData
	{
		public UITextureAtlas m_Atlas;

		public UITextureAtlas m_InfoTooltipAtlas;

		public string m_Thumbnail;

		public string m_InfoTooltipThumbnail;
	}
}
