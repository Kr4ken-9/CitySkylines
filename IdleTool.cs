﻿using System;

public class IdleTool : ToolBase
{
	protected override void OnToolLateUpdate()
	{
		base.OnToolLateUpdate();
		ToolBase.OverrideInfoMode = false;
	}
}
