﻿using System;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public abstract class ImportAssetLodded : ImportAsset
{
	public ImportAssetLodded(GameObject template, PreviewCamera camera) : base(template, camera)
	{
		this.m_LodTriangleTarget = 150;
	}

	protected void CreateLODMaterial()
	{
		if (this.m_LODObject != null && this.m_Object != null)
		{
			MeshRenderer component = this.m_LODObject.GetComponent<MeshRenderer>();
			if (component != null)
			{
				Renderer component2 = this.m_Object.GetComponent<Renderer>();
				if (component2 != null && component2.get_sharedMaterial() != null && component2.get_sharedMaterial().get_shader() != null)
				{
					component.set_sharedMaterial(new Material(component2.get_sharedMaterial()));
				}
				else
				{
					Shader shader = Shader.Find("Custom/Buildings/Building/Default");
					if (shader != null)
					{
						component.set_sharedMaterial(new Material(shader));
					}
					else
					{
						CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "Template has no renderer or shader, and fallback shader not found; LOD object will not be rendered correctly.");
					}
				}
			}
		}
	}

	protected override void CreateLODObject()
	{
		if (this.m_IsImportedAsset)
		{
			string text = Path.Combine(this.m_Path, Path.GetFileNameWithoutExtension(this.m_Filename) + ImportAsset.sLODModelSignature + Path.GetExtension(this.m_Filename));
			if (File.Exists(text))
			{
				this.LoadLODModel(text);
				MeshFilter component = this.m_LODObject.GetComponent<MeshFilter>();
				if (component != null && component.get_sharedMesh() != null)
				{
					this.m_OriginalLodMesh = RuntimeMeshUtils.CopyMesh(component.get_sharedMesh());
				}
				string basePath = Path.Combine(this.m_Path, Path.GetFileNameWithoutExtension(this.m_Filename)) + ImportAsset.sLODModelSignature;
				string text2 = AssetImporterTextureLoader.FindTexture(basePath, AssetImporterTextureLoader.SourceType.DIFFUSE);
				if (text2 != null)
				{
					this.m_NeedToBakeLODTextures = false;
				}
				else
				{
					this.m_NeedToBakeLODTextures = true;
				}
			}
			else
			{
				this.m_NeedToBuildLODModel = true;
				this.CreateEmptyLODObject();
			}
		}
		else
		{
			this.m_NeedToBuildLODModel = false;
			this.m_NeedToBakeLODTextures = false;
			BuildingInfo component2 = this.m_TemplateObject.GetComponent<BuildingInfo>();
			VehicleInfo component3 = this.m_TemplateObject.GetComponent<VehicleInfo>();
			CitizenInfo component4 = this.m_TemplateObject.GetComponent<CitizenInfo>();
			if (component2 != null)
			{
				GameObject lodObject = component2.m_lodObject;
				if (lodObject != null)
				{
					this.m_LODObject = UnityEngine.Object.Instantiate<GameObject>(lodObject);
				}
			}
			else if (component3 != null)
			{
				GameObject lodObject2 = component3.m_lodObject;
				if (lodObject2 != null)
				{
					this.m_LODObject = UnityEngine.Object.Instantiate<GameObject>(lodObject2);
				}
			}
			else if (component4 != null)
			{
				GameObject lodObject3 = component4.m_lodObject;
				if (lodObject3 != null)
				{
					this.m_LODObject = UnityEngine.Object.Instantiate<GameObject>(lodObject3);
				}
			}
			if (this.m_LODObject != null)
			{
				MeshFilter component5 = this.m_LODObject.GetComponent<MeshFilter>();
				if (component5 != null)
				{
					component5.set_sharedMesh(component5.get_mesh());
					this.m_OriginalLodMesh = RuntimeMeshUtils.CopyMesh(component5.get_sharedMesh());
				}
				Renderer component6 = this.m_LODObject.GetComponent<Renderer>();
				if (component6 != null)
				{
					component6.set_sharedMaterial(component6.get_material());
				}
			}
		}
		if (this.m_LODObject != null)
		{
			this.m_LODObject.SetActive(false);
		}
	}

	protected override void FinalizeLOD()
	{
		if (this.m_LODObject != null && this.m_IsImportedAsset)
		{
			this.CreateLODMaterial();
			if (this.m_NeedToBuildLODModel)
			{
				this.m_Tasks[1][0] = this.BuildLOD(this.m_Object, this.m_LODObject);
			}
			else if (this.m_NeedToBakeLODTextures)
			{
				Renderer component = this.m_LODObject.GetComponent<MeshRenderer>();
				if (component != null && component.get_sharedMaterial() != null)
				{
					this.m_Tasks[0][1] = this.BakeLODTextures(this.m_Object, this.m_LODObject);
				}
				else
				{
					CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD object renderer or material not found");
				}
			}
			else
			{
				bool flag = this is ImportAssetModel;
				AssetImporterTextureLoader.LoadTextures(null, this.m_LODObject, this.textureTypes, this.m_Path, this.m_Filename, true, this.textureAnisoLevel, flag, flag);
			}
		}
	}

	public override void DestroyAsset()
	{
		if (this.m_OriginalLodMesh != null)
		{
			UnityEngine.Object.DestroyImmediate(this.m_OriginalLodMesh);
		}
		if (this.m_LODObject != null)
		{
			MeshFilter component = this.m_LODObject.GetComponent<MeshFilter>();
			if (component != null)
			{
				UnityEngine.Object.DestroyImmediate(component.get_sharedMesh());
			}
			if (this.m_IsImportedAsset)
			{
				Renderer[] componentsInChildren = this.m_LODObject.GetComponentsInChildren<MeshRenderer>(true);
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					for (int j = 0; j < componentsInChildren[i].get_sharedMaterials().Length; j++)
					{
						Material material = componentsInChildren[i].get_sharedMaterials()[j];
						if (material.HasProperty("_MainTex"))
						{
							Texture texture = componentsInChildren[i].get_sharedMaterials()[j].GetTexture("_MainTex");
							if (texture != null)
							{
								UnityEngine.Object.DestroyImmediate(texture);
							}
						}
						if (material.HasProperty("_XYSMap"))
						{
							Texture texture2 = componentsInChildren[i].get_sharedMaterials()[j].GetTexture("_XYSMap");
							if (texture2 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture2);
							}
						}
						if (material.HasProperty("_ACIMap"))
						{
							Texture texture3 = componentsInChildren[i].get_sharedMaterials()[j].GetTexture("_ACIMap");
							if (texture3 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture3);
							}
						}
						if (material.HasProperty("_XYCAMap"))
						{
							Texture texture4 = componentsInChildren[i].get_sharedMaterials()[j].GetTexture("_XYCAMap");
							if (texture4 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture4);
							}
						}
					}
				}
			}
			UnityEngine.Object.DestroyImmediate(this.m_LODObject);
			this.m_LODObject = null;
		}
		base.DestroyAsset();
	}

	public override void ApplyTransform(Vector3 scale, Vector3 rotation, bool bottomPivot)
	{
		base.ApplyTransform(scale, rotation, bottomPivot);
		if (this.m_LODObject == null)
		{
			return;
		}
		MeshFilter component = this.m_LODObject.GetComponent<MeshFilter>();
		if (component != null && this.m_OriginalLodMesh != null)
		{
			RuntimeMeshUtils.ProcessMeshTransform(this.m_OriginalLodMesh, component.get_sharedMesh(), scale, bottomPivot, rotation);
		}
	}

	protected void LoadLODModel(string lodPath)
	{
		this.m_NeedToBuildLODModel = false;
		this.m_Importer.set_filePath(lodPath);
		this.m_Importer.set_importSkinMesh(true);
		this.m_LODObject = this.m_Importer.Import();
		if (this.m_LODObject == null)
		{
			this.m_NeedToBuildLODModel = true;
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD object found but loading failed, will generate one instead");
		}
	}

	protected void CreateEmptyLODObject()
	{
		this.m_LODObject = new GameObject(Path.GetFileNameWithoutExtension(this.m_Filename) + ImportAsset.sLODModelSignature);
		if (this.m_LODObject != null)
		{
			MeshFilter meshFilter = this.m_LODObject.AddComponent<MeshFilter>();
			if (meshFilter != null)
			{
				meshFilter.set_sharedMesh(new Mesh());
			}
			MeshRenderer meshRenderer = this.m_LODObject.AddComponent<MeshRenderer>();
			Shader shader = Shader.Find("Custom/Buildings/Building/Default");
			if (meshRenderer != null && shader != null)
			{
				meshRenderer.set_sharedMaterial(new Material(shader));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "Default building material not found");
			}
		}
	}

	protected Task BuildLOD(GameObject sourceObject, GameObject targetObject)
	{
		if (sourceObject == null || targetObject == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Builder: source or target game object does not exist");
			return null;
		}
		Mesh sharedMesh = this.GetSharedMesh(sourceObject);
		if (sharedMesh == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Builder: no source mesh found");
			return null;
		}
		MeshSimplification.SimplificationMesh source = default(MeshSimplification.SimplificationMesh);
		source.vertices = sharedMesh.get_vertices();
		source.triangles = sharedMesh.get_triangles();
		source.normals = sharedMesh.get_normals();
		source.tangents = sharedMesh.get_tangents();
		source.uv = sharedMesh.get_uv();
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Creating LOD Creator Thread  [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		TaskDistributor taskDistributor = new TaskDistributor("TaskDistributor");
		Task result = taskDistributor.Dispatch(delegate
		{
			try
			{
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Creating LOD [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				MeshSimplification.SimplificationParameters simplificationParameters = new MeshSimplification.SimplificationParameters();
				simplificationParameters.maxTriangles = this.m_LodTriangleTarget;
				MeshSimplification.SimplificationMesh result = MeshSimplification.SimplifyMesh(source, simplificationParameters);
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Finished creating LOD [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					MeshSimplification.SimplificationPostProcess(result, targetObject.GetComponent<MeshFilter>().get_sharedMesh());
					ImportAssetLodded.SetLODColors(this.m_Object);
					Task task = this.BakeLODTextures(sourceObject, targetObject);
					MultiAsyncTaskWrapper multiAsyncTaskWrapper = LoadSaveStatus.activeTask as MultiAsyncTaskWrapper;
					if (multiAsyncTaskWrapper != null)
					{
						multiAsyncTaskWrapper.SetTask(1, 0, task);
					}
					else
					{
						LoadSaveStatus.activeTask = new AsyncTaskWrapper("LOD", new Task[]
						{
							task
						});
					}
				});
			}
			catch (Exception ex)
			{
				CODebugBase<LogChannel>.Error(LogChannel.AssetImporter, string.Concat(new object[]
				{
					ex.GetType(),
					" ",
					ex.Message,
					" ",
					ex.StackTrace
				}));
				UIView.ForwardException(ex);
			}
		});
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Created LOD Creator Thread [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		return result;
	}

	protected Task BakeLODTextures(GameObject sourceObject, GameObject targetObject)
	{
		if (sourceObject == null || targetObject == null)
		{
			return null;
		}
		Mesh sharedMesh = this.GetSharedMesh(sourceObject);
		if (sharedMesh == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Texture Baker: No mesh in source object");
		}
		Vector3[] sourceVertices = sharedMesh.get_vertices();
		Vector3[] sourceNormals = sharedMesh.get_normals();
		Vector4[] sourceTangents = sharedMesh.get_tangents();
		Vector2[] sourceUVS = sharedMesh.get_uv();
		int[] sourceTriangles = sharedMesh.get_triangles();
		Mesh sharedMesh2 = targetObject.GetComponent<MeshFilter>().get_sharedMesh();
		if (sharedMesh2 == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Texture Baker: No mesh in target object");
			return null;
		}
		Vector3[] targetVertices = sharedMesh2.get_vertices();
		Vector3[] targetNormals = sharedMesh2.get_normals();
		Vector4[] targetTangents = sharedMesh2.get_tangents();
		Vector2[] targetUVS = sharedMesh2.get_uv();
		int[] targetTriangles = sharedMesh2.get_triangles();
		Material sourceMaterial = this.GetRenderer(sourceObject).get_sharedMaterial();
		if (sourceMaterial == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Texture Baker: Source material not found");
			return null;
		}
		Texture2D texture2D = (Texture2D)sourceMaterial.GetTexture("_MainTex");
		Texture2D texture2D2 = (!sourceMaterial.HasProperty("_XYSMap")) ? null : ((Texture2D)sourceMaterial.GetTexture("_XYSMap"));
		Texture2D texture2D3 = null;
		if (sourceMaterial.HasProperty("_ACIMap"))
		{
			texture2D3 = (Texture2D)sourceMaterial.GetTexture("_ACIMap");
		}
		else if (sourceMaterial.HasProperty("_APRMap"))
		{
			texture2D3 = (Texture2D)sourceMaterial.GetTexture("_APRMap");
		}
		if (texture2D == null)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.AssetImporter, "LOD Texture Baker: Source diffuse texture not found, nothing to bake");
			return null;
		}
		int sourceWidth = texture2D.get_width();
		int sourceHeight = texture2D.get_height();
		Color32[] sourceDiffuse = texture2D.GetPixels32();
		Color32[] sourceXYS = (!(texture2D2 == null)) ? texture2D2.GetPixels32() : null;
		Color32[] sourceACI = (!(texture2D3 == null)) ? texture2D3.GetPixels32() : null;
		int threadCount = Mathf.Max(1, SystemInfo.get_processorCount() - 1);
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Creating LOD Texture Baker Thread  [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		TaskDistributor taskDistributor = new TaskDistributor("TaskDistributor");
		Task result = taskDistributor.Dispatch(delegate
		{
			try
			{
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Baking LOD textures [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				int targetWidth;
				int targetHeight;
				Color32[] targetDiffuse;
				Color32[] targetXYS;
				Color32[] targetACI;
				MeshSimplification.BakeTextures(sourceVertices, sourceNormals, sourceTangents, sourceUVS, sourceTriangles, sourceWidth, sourceHeight, sourceDiffuse, sourceXYS, sourceACI, targetVertices, targetNormals, targetTangents, targetUVS, targetTriangles, out targetWidth, out targetHeight, out targetDiffuse, out targetXYS, out targetACI, true, true, 3f, 0f, 2f, 32, 256, 1, threadCount);
				CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
				{
					"******Finished baking LOD textures [",
					Thread.CurrentThread.Name,
					Thread.CurrentThread.ManagedThreadId,
					"]"
				}));
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
					{
						"******Applying LOD texture baking results [",
						Thread.CurrentThread.Name,
						Thread.CurrentThread.ManagedThreadId,
						"]"
					}));
					Texture2D texture2D4 = new Texture2D(targetWidth, targetHeight, 3, false, false);
					texture2D4.set_name(targetObject.get_name() + " (Diffuse)");
					texture2D4.SetPixels32(targetDiffuse);
					texture2D4.Apply();
					AssetImporterTextureLoader.ApplyTexture(targetObject, "_MainTex", texture2D4);
					Texture2D texture2D5 = new Texture2D(targetWidth, targetHeight, 3, false, true);
					texture2D5.set_name(targetObject.get_name() + " (XYSMap)");
					texture2D5.SetPixels32(targetXYS);
					texture2D5.Apply();
					AssetImporterTextureLoader.ApplyTexture(targetObject, "_XYSMap", texture2D5);
					Texture2D texture2D6 = new Texture2D(targetWidth, targetHeight, 3, false, false);
					texture2D6.set_name(targetObject.get_name() + " (ACImap)");
					texture2D6.SetPixels32(targetACI);
					texture2D6.Apply();
					if (sourceMaterial.HasProperty("_ACIMap"))
					{
						AssetImporterTextureLoader.ApplyTexture(targetObject, "_ACIMap", texture2D6);
					}
					else if (sourceMaterial.HasProperty("_APRMap"))
					{
						AssetImporterTextureLoader.ApplyTexture(targetObject, "_APRMap", texture2D6);
					}
					CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
					{
						"******Finished applying LOD texture baking results [",
						Thread.CurrentThread.Name,
						Thread.CurrentThread.ManagedThreadId,
						"]"
					}));
				});
			}
			catch (Exception ex)
			{
				CODebugBase<LogChannel>.Error(LogChannel.AssetImporter, string.Concat(new object[]
				{
					ex.GetType(),
					" ",
					ex.Message,
					" ",
					ex.StackTrace
				}));
				UIView.ForwardException(ex);
			}
		});
		CODebugBase<LogChannel>.VerboseLog(LogChannel.AssetImporter, string.Concat(new object[]
		{
			"***Created LOD Texture Baker Thread [",
			Thread.CurrentThread.Name,
			Thread.CurrentThread.ManagedThreadId,
			"]"
		}));
		return result;
	}

	public static void SetLODColors(GameObject obj)
	{
		if (obj != null)
		{
			VehicleInfo component = obj.GetComponent<VehicleInfo>();
			if (component != null && component.m_lodObject != null)
			{
				MeshFilter component2 = component.m_lodObject.GetComponent<MeshFilter>();
				if (component2 != null)
				{
					Mesh sharedMesh = component2.get_sharedMesh();
					if (sharedMesh != null && sharedMesh.get_vertices() != null)
					{
						Color32[] array = new Color32[sharedMesh.get_vertices().Length];
						for (int i = 0; i < array.Length; i++)
						{
							array[i] = new Color32(0, 0, 0, 255);
						}
						sharedMesh.set_colors32(array);
					}
				}
				component.CalculateGeneratedInfo();
			}
		}
	}

	protected bool m_NeedToBuildLODModel;

	protected bool m_NeedToBakeLODTextures;

	protected Mesh m_OriginalLodMesh;

	protected int m_LodTriangleTarget;
}
