﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class GeneratedGroupPanel : ToolsModifierControl
{
	public static string ResolveLegacyCategoryName(ItemClass.Service service, string category)
	{
		if (category == "BeautificationParksnPlazas")
		{
			return "BeautificationParks";
		}
		if (category == "BeautificationProps")
		{
			return "LandscapingTrees";
		}
		if (category == "BeautificationPaths")
		{
			return "LandscapingPaths";
		}
		if (category == "Default")
		{
			if (service == ItemClass.Service.FireDepartment)
			{
				return "FireDepartmentFire";
			}
			if (service == ItemClass.Service.Electricity)
			{
				return "ElectricityDefault";
			}
			if (service == ItemClass.Service.HealthCare)
			{
				return "HealthcareDefault";
			}
			if (service == ItemClass.Service.PoliceDepartment)
			{
				return "PoliceDefault";
			}
			if (service == ItemClass.Service.Education)
			{
				return "EducationDefault";
			}
		}
		return category;
	}

	public virtual ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public virtual string serviceName
	{
		get
		{
			return null;
		}
	}

	public virtual GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.None;
		}
	}

	public virtual Comparison<GeneratedGroupPanel.GroupInfo> sortingMethod
	{
		get
		{
			return new Comparison<GeneratedGroupPanel.GroupInfo>(this.SortByOrder);
		}
	}

	protected virtual bool IsServiceValid(PrefabInfo info)
	{
		return info.GetService() == this.service;
	}

	private bool IsPlacementRelevant(NetInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool IsPlacementRelevant(BuildingInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool IsPlacementRelevant(TransportInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_pathVisibility.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool IsPlacementRelevant(TreeInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool IsPlacementRelevant(PropInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool IsPlacementRelevant(DisasterInfo info)
	{
		bool flag = info.m_disasterAI.CanSelfTrigger();
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	private bool FilterWonders(GeneratedGroupPanel.GroupFilter filter, BuildingInfo info)
	{
		if (filter.IsFlagSet((GeneratedGroupPanel.GroupFilter)18))
		{
			return true;
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Wonder))
		{
			return info.m_buildingAI.IsWonder();
		}
		return filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Building) && !info.m_buildingAI.IsWonder();
	}

	protected int SortByOrder(GeneratedGroupPanel.GroupInfo a, GeneratedGroupPanel.GroupInfo b)
	{
		return a.order.CompareTo(b.order);
	}

	protected virtual GeneratedGroupPanel.GroupInfo CreateGroupInfo(string name, PrefabInfo info)
	{
		return new GeneratedGroupPanel.GroupInfo(name, this.GetCategoryOrder(name));
	}

	protected virtual int GetCategoryOrder(string name)
	{
		return 0;
	}

	protected virtual bool IsCategoryRelevant(string category)
	{
		return true;
	}

	private void AddGroup(PoolList<GeneratedGroupPanel.GroupInfo> groupItems, PrefabInfo info)
	{
		bool flag = true;
		string category = info.category;
		if (this.IsCategoryRelevant(category))
		{
			for (int i = 0; i < groupItems.get_Count(); i++)
			{
				if (groupItems.get_Item(i).name == category)
				{
					flag = false;
				}
			}
			if (flag)
			{
				groupItems.Add(this.CreateGroupInfo(category, info));
			}
		}
	}

	private PoolList<GeneratedGroupPanel.GroupInfo> CollectAssets(GeneratedGroupPanel.GroupFilter filter, Comparison<GeneratedGroupPanel.GroupInfo> comparison)
	{
		PoolList<GeneratedGroupPanel.GroupInfo> poolList = PoolList<GeneratedGroupPanel.GroupInfo>.Obtain();
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Net))
		{
			uint num = 0u;
			while ((ulong)num < (ulong)((long)PrefabCollection<NetInfo>.LoadedCount()))
			{
				NetInfo loaded = PrefabCollection<NetInfo>.GetLoaded(num);
				if (loaded != null && this.IsServiceValid(loaded) && this.IsPlacementRelevant(loaded))
				{
					this.AddGroup(poolList, loaded);
				}
				num += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Building) || filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Wonder))
		{
			uint num2 = 0u;
			while ((ulong)num2 < (ulong)((long)PrefabCollection<BuildingInfo>.LoadedCount()))
			{
				BuildingInfo loaded2 = PrefabCollection<BuildingInfo>.GetLoaded(num2);
				if (loaded2 != null && this.FilterWonders(filter, loaded2) && this.IsServiceValid(loaded2) && this.IsPlacementRelevant(loaded2))
				{
					this.AddGroup(poolList, loaded2);
				}
				num2 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Transport))
		{
			uint num3 = 0u;
			while ((ulong)num3 < (ulong)((long)PrefabCollection<TransportInfo>.LoadedCount()))
			{
				TransportInfo loaded3 = PrefabCollection<TransportInfo>.GetLoaded(num3);
				if (loaded3 != null && this.IsServiceValid(loaded3) && this.IsPlacementRelevant(loaded3))
				{
					this.AddGroup(poolList, loaded3);
				}
				num3 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Tree))
		{
			uint num4 = 0u;
			while ((ulong)num4 < (ulong)((long)PrefabCollection<TreeInfo>.LoadedCount()))
			{
				TreeInfo loaded4 = PrefabCollection<TreeInfo>.GetLoaded(num4);
				if (loaded4 != null && this.IsServiceValid(loaded4) && this.IsPlacementRelevant(loaded4))
				{
					this.AddGroup(poolList, loaded4);
				}
				num4 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Prop))
		{
			uint num5 = 0u;
			while ((ulong)num5 < (ulong)((long)PrefabCollection<PropInfo>.LoadedCount()))
			{
				PropInfo loaded5 = PrefabCollection<PropInfo>.GetLoaded(num5);
				if (loaded5 != null && this.IsServiceValid(loaded5) && this.IsPlacementRelevant(loaded5))
				{
					this.AddGroup(poolList, loaded5);
				}
				num5 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedGroupPanel.GroupFilter.Disaster))
		{
			uint num6 = 0u;
			while ((ulong)num6 < (ulong)((long)PrefabCollection<DisasterInfo>.LoadedCount()))
			{
				DisasterInfo loaded6 = PrefabCollection<DisasterInfo>.GetLoaded(num6);
				if (loaded6 != null && this.IsPlacementRelevant(loaded6))
				{
					this.AddGroup(poolList, loaded6);
				}
				num6 += 1u;
			}
		}
		poolList.Sort(comparison);
		return poolList;
	}

	public void PopulateGroups()
	{
		this.PopulateGroups(GeneratedGroupPanel.GroupFilter.All, new Comparison<GeneratedGroupPanel.GroupInfo>(this.SortByOrder));
	}

	public void PopulateGroups(GeneratedGroupPanel.GroupFilter filter)
	{
		this.PopulateGroups(filter, new Comparison<GeneratedGroupPanel.GroupInfo>(this.SortByOrder));
	}

	public void PopulateGroups(GeneratedGroupPanel.GroupFilter filter, Comparison<GeneratedGroupPanel.GroupInfo> comparison)
	{
		using (PoolList<GeneratedGroupPanel.GroupInfo> poolList = this.CollectAssets(filter, comparison))
		{
			for (int i = 0; i < poolList.get_Count(); i++)
			{
				this.CreateGroupItem(poolList.get_Item(i), "MAIN_CATEGORY");
			}
		}
	}

	protected void CreateGroupItem(GeneratedGroupPanel.GroupInfo info, string localeID)
	{
		this.SpawnButtonEntry(this.m_Strip, StringExtensions.IsNullOrWhiteSpace(info.subPanelType) ? EnumExtensions.Name<ItemClass.Service>(this.service) : info.subPanelType, info.name, false, localeID, info.unlockText, "SubBar", info.isUnlocked, false);
	}

	private void Awake()
	{
		this.m_Strip = base.Find<UITabstrip>("GroupToolstrip");
	}

	protected void DefaultGroup(string name)
	{
		this.SpawnButtonEntry(this.m_Strip, name, name + "Default", true, null, "SubBar", true, false);
	}

	protected static string GetUnlockText(ItemClass.SubService service)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_SubServiceMilestones[(int)service];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	protected static string GetUnlockText(UnlockManager.Feature feature)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	protected UIButton SpawnButtonEntry(UITabstrip strip, string name, string category, bool isDefaultCategory, string localeID, string spriteBase, bool enabled, bool forceFillContainer)
	{
		return this.SpawnButtonEntry(strip, name, category, isDefaultCategory, localeID, null, spriteBase, enabled, forceFillContainer);
	}

	protected UIButton SpawnButtonEntry(UITabstrip strip, string name, string category, bool isDefaultCategory, string localeID, string unlockText, string spriteBase, bool enabled, bool forceFillContainer)
	{
		Type type = Type.GetType(name + "Panel");
		if (type != null && !type.IsSubclassOf(typeof(GeneratedScrollPanel)))
		{
			type = null;
		}
		UIButton uIButton;
		if (strip.get_childCount() > this.m_ObjectIndex)
		{
			uIButton = (strip.get_components()[this.m_ObjectIndex] as UIButton);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(GeneratedGroupPanel.kSubbarButtonTemplate);
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(GeneratedGroupPanel.kSubbarPanelTemplate);
			uIButton = (strip.AddTab(category, asGameObject, asGameObject2, new Type[]
			{
				type
			}) as UIButton);
		}
		uIButton.set_isEnabled(enabled);
		uIButton.get_gameObject().GetComponent<TutorialUITag>().tutorialTag = category + "Group";
		GeneratedScrollPanel generatedScrollPanel = strip.GetComponentInContainer(uIButton, type) as GeneratedScrollPanel;
		if (generatedScrollPanel != null)
		{
			generatedScrollPanel.get_component().set_isInteractive(true);
			generatedScrollPanel.m_OptionsBar = this.m_OptionsBar;
			generatedScrollPanel.m_DefaultInfoTooltipAtlas = this.m_DefaultInfoTooltipAtlas;
			if (forceFillContainer || enabled)
			{
				generatedScrollPanel.category = ((!isDefaultCategory) ? category : string.Empty);
				generatedScrollPanel.RefreshPanel();
			}
		}
		string text = spriteBase + category;
		uIButton.set_normalFgSprite(text);
		uIButton.set_focusedFgSprite(text + "Focused");
		uIButton.set_hoveredFgSprite(text + "Hovered");
		uIButton.set_pressedFgSprite(text + "Pressed");
		uIButton.set_disabledFgSprite(text + "Disabled");
		if (!string.IsNullOrEmpty(localeID) && !string.IsNullOrEmpty(unlockText))
		{
			uIButton.set_tooltip(Locale.Get(localeID, category) + " - " + unlockText);
		}
		else if (!string.IsNullOrEmpty(localeID))
		{
			uIButton.set_tooltip(Locale.Get(localeID, category));
		}
		this.m_ObjectIndex++;
		return uIButton;
	}

	public void RefreshPanel()
	{
		this.m_ObjectIndex = 0;
		if (this.CustomRefreshPanel())
		{
			return;
		}
		if (this.groupFilter != GeneratedGroupPanel.GroupFilter.None)
		{
			this.PopulateGroups(this.groupFilter, this.sortingMethod);
		}
		else if (!string.IsNullOrEmpty(this.serviceName))
		{
			this.DefaultGroup(this.serviceName);
		}
		else
		{
			this.DefaultGroup(EnumExtensions.Name<ItemClass.Service>(this.service));
		}
	}

	protected virtual bool CustomRefreshPanel()
	{
		return false;
	}

	protected void OnVisibilityChanged(UIComponent comp, bool isVisible)
	{
		if (isVisible)
		{
			this.m_Strip.set_isVisible(this.m_Strip.get_childCount() > 1);
			if (this.m_Strip.get_selectedIndex() == -1)
			{
				this.m_Strip.set_selectedIndex(0);
			}
			if (this.m_Strip.get_selectedIndex() != 0 && this.m_Strip.get_childCount() > this.m_Strip.get_selectedIndex() && !this.m_Strip.get_components()[this.m_Strip.get_selectedIndex()].get_isEnabled())
			{
				this.m_Strip.set_selectedIndex(0);
			}
		}
	}

	private static readonly string kSubbarButtonTemplate = "SubbarButtonTemplate";

	private static readonly string kSubbarPanelTemplate = "SubbarPanelTemplate";

	private int m_ObjectIndex;

	public UIComponent m_OptionsBar;

	public UITextureAtlas m_DefaultInfoTooltipAtlas;

	protected UITabstrip m_Strip;

	public enum GroupFilter
	{
		None,
		Net,
		Building,
		Transport = 4,
		Tree = 8,
		Wonder = 16,
		Prop = 32,
		Disaster = 64,
		All = 127
	}

	public class GroupInfo
	{
		public GroupInfo(string name, int order, string subPanelType)
		{
			this.m_Name = name;
			this.m_Order = order;
			this.m_SubPanelType = subPanelType;
		}

		public GroupInfo(string name, int order)
		{
			this.m_Name = name;
			this.m_Order = order;
			this.m_SubPanelType = null;
		}

		public string subPanelType
		{
			get
			{
				return this.m_SubPanelType;
			}
		}

		public string name
		{
			get
			{
				return this.m_Name;
			}
		}

		public virtual string serviceName
		{
			get
			{
				return EnumExtensions.Name<ItemClass.Service>(ItemClass.Service.None);
			}
		}

		public virtual string unlockText
		{
			get
			{
				return null;
			}
		}

		public virtual bool isUnlocked
		{
			get
			{
				return true;
			}
		}

		public int order
		{
			get
			{
				return this.m_Order;
			}
		}

		private string m_Name;

		private int m_Order;

		private string m_SubPanelType;
	}
}
