﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class SchoolAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Education)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
		ItemClass.Level level = ItemClass.Level.Level1;
		if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
		{
			level = ItemClass.Level.Level2;
		}
		else if (currentSubMode == InfoManager.SubInfoMode.WindPower)
		{
			level = ItemClass.Level.Level3;
		}
		if (level != this.m_info.m_class.m_level)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public void GetStudentCount(ushort buildingID, ref Building data, out int count, out int capacity, out int global)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		count = (int)data.m_customBuffer1;
		capacity = Mathf.Min((productionRate * this.m_studentCount + 99) / 100, this.m_studentCount * 5 / 4);
		global = 0;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(ItemClass.Service.Education);
		int size = serviceBuildings.m_size;
		ushort[] buffer = serviceBuildings.m_buffer;
		if (buffer != null && size <= buffer.Length)
		{
			for (int i = 0; i < size; i++)
			{
				ushort num = buffer[i];
				if (num != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
					if (info.m_class.m_service == ItemClass.Service.Education && info.m_class.m_level == this.m_info.m_class.m_level)
					{
						global += (int)instance.m_buildings.m_buffer[(int)num].m_customBuffer1;
					}
				}
			}
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Education;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level1)
		{
			subMode = InfoManager.SubInfoMode.Default;
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			subMode = InfoManager.SubInfoMode.WaterPower;
		}
		else
		{
			subMode = InfoManager.SubInfoMode.WindPower;
		}
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, this.m_studentCount * 5 / 4);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, this.m_studentCount * 5 / 4);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, this.m_studentCount * 5 / 4);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_educationAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
			if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.EducationUniversity, (float)this.m_educationAccumulation, this.m_educationRadius);
			}
			else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.EducationHighSchool, (float)this.m_educationAccumulation, this.m_educationRadius);
			}
			else
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.EducationElementary, (float)this.m_educationAccumulation, this.m_educationRadius);
			}
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_educationAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.EducationUniversity, (float)(-(float)this.m_educationAccumulation), this.m_educationRadius);
			}
			else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.EducationHighSchool, (float)(-(float)this.m_educationAccumulation), this.m_educationRadius);
			}
			else
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.EducationElementary, (float)(-(float)this.m_educationAccumulation), this.m_educationRadius);
			}
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Student3, offer);
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Student2, offer);
		}
		else
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Student1, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_educationRadius * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
			int num = 0;
			int num2 = 0;
			base.GetStudentBehaviour(buildingID, ref buildingData, ref behaviourData, ref num, ref num2);
			buildingData.m_customBuffer1 = (ushort)num;
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = 0;
		int num2 = 0;
		base.GetStudentBehaviour(buildingID, ref buildingData, ref behaviour, ref num, ref num2);
		if (num != 0)
		{
			behaviour.m_crimeAccumulation = behaviour.m_crimeAccumulation * aliveWorkerCount / (aliveWorkerCount + num);
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		int num3 = productionRate * this.m_educationAccumulation / 100;
		if ((servicePolicies & DistrictPolicies.Services.EducationalBlimps) != DistrictPolicies.Services.None)
		{
			num3 = (num3 * 21 + 10) / 20;
			District[] expr_AD_cp_0 = instance.m_districts.m_buffer;
			byte expr_AD_cp_1 = district;
			expr_AD_cp_0[(int)expr_AD_cp_1].m_servicePoliciesEffect = (expr_AD_cp_0[(int)expr_AD_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.EducationalBlimps);
		}
		if (num3 != 0)
		{
			if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationUniversity, num3, buildingData.m_position, this.m_educationRadius);
			}
			else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationHighSchool, num3, buildingData.m_position, this.m_educationRadius);
			}
			else
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationElementary, num3, buildingData.m_position, this.m_educationRadius);
			}
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		buildingData.m_customBuffer1 = (ushort)num;
		if ((servicePolicies & DistrictPolicies.Services.EducationBoost) != DistrictPolicies.Services.None)
		{
			District[] expr_177_cp_0 = instance.m_districts.m_buffer;
			byte expr_177_cp_1 = district;
			expr_177_cp_0[(int)expr_177_cp_1].m_servicePoliciesEffect = (expr_177_cp_0[(int)expr_177_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.EducationBoost);
			int num4 = this.GetMaintenanceCost() / 100;
			num4 = (finalProductionRate * num4 + 399) / 400;
			if (num4 != 0)
			{
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num4, this.m_info.m_class);
			}
		}
		if (this.m_info.m_class.m_level == ItemClass.Level.Level3 && (servicePolicies & DistrictPolicies.Services.SchoolsOut) != DistrictPolicies.Services.None)
		{
			District[] expr_1FA_cp_0 = instance.m_districts.m_buffer;
			byte expr_1FA_cp_1 = district;
			expr_1FA_cp_0[(int)expr_1FA_cp_1].m_servicePoliciesEffect = (expr_1FA_cp_0[(int)expr_1FA_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.SchoolsOut);
		}
		int num5 = Mathf.Min((finalProductionRate * this.m_studentCount + 99) / 100, this.m_studentCount * 5 / 4);
		int num6 = num5 - num2;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level1)
		{
			District[] expr_25C_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_25C_cp_0_cp_1 = district;
			expr_25C_cp_0_cp_0[(int)expr_25C_cp_0_cp_1].m_productionData.m_tempEducation1Capacity = expr_25C_cp_0_cp_0[(int)expr_25C_cp_0_cp_1].m_productionData.m_tempEducation1Capacity + (uint)num5;
			District[] expr_280_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_280_cp_0_cp_1 = district;
			expr_280_cp_0_cp_0[(int)expr_280_cp_0_cp_1].m_student1Data.m_tempCount = expr_280_cp_0_cp_0[(int)expr_280_cp_0_cp_1].m_student1Data.m_tempCount + (uint)num;
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			District[] expr_2BE_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_2BE_cp_0_cp_1 = district;
			expr_2BE_cp_0_cp_0[(int)expr_2BE_cp_0_cp_1].m_productionData.m_tempEducation2Capacity = expr_2BE_cp_0_cp_0[(int)expr_2BE_cp_0_cp_1].m_productionData.m_tempEducation2Capacity + (uint)num5;
			District[] expr_2E2_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_2E2_cp_0_cp_1 = district;
			expr_2E2_cp_0_cp_0[(int)expr_2E2_cp_0_cp_1].m_student2Data.m_tempCount = expr_2E2_cp_0_cp_0[(int)expr_2E2_cp_0_cp_1].m_student2Data.m_tempCount + (uint)num;
		}
		else
		{
			District[] expr_30A_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_30A_cp_0_cp_1 = district;
			expr_30A_cp_0_cp_0[(int)expr_30A_cp_0_cp_1].m_productionData.m_tempEducation3Capacity = expr_30A_cp_0_cp_0[(int)expr_30A_cp_0_cp_1].m_productionData.m_tempEducation3Capacity + (uint)num5;
			District[] expr_32E_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_32E_cp_0_cp_1 = district;
			expr_32E_cp_0_cp_0[(int)expr_32E_cp_0_cp_1].m_student3Data.m_tempCount = expr_32E_cp_0_cp_0[(int)expr_32E_cp_0_cp_1].m_student3Data.m_tempCount + (uint)num;
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount + num2);
		if (num6 >= 1)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = Mathf.Max(1, num6 * 8 / num5);
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			offer.Amount = num6;
			if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
			{
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Student3, offer);
			}
			else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
			{
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Student2, offer);
			}
			else
			{
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Student1, offer);
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_studentCount != 0;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override bool GetEducationLevel1()
	{
		return this.m_info.m_class.m_level == ItemClass.Level.Level1;
	}

	public override bool GetEducationLevel2()
	{
		return this.m_info.m_class.m_level == ItemClass.Level.Level2;
	}

	public override bool GetEducationLevel3()
	{
		return this.m_info.m_class.m_level == ItemClass.Level.Level3;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_STUDENT_COUNT", new object[]
			{
				this.m_studentCount
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num;
		int num2;
		int num3;
		this.GetStudentCount(buildingID, ref data, out num, out num2, out num3);
		string str = LocaleFormatter.FormatGeneric("AIINFO_STUDENTS", new object[]
		{
			num,
			num2
		}) + Environment.NewLine;
		string localeID = string.Empty;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level1)
		{
			localeID = "AIINFO_ELEMENTARY_STUDENTCOUNT";
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			localeID = "AIINFO_HIGHSCHOOL_STUDENTCOUNT";
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			localeID = "AIINFO_UNIVERSITY_STUDENTCOUNT";
		}
		return str + LocaleFormatter.FormatGeneric(localeID, new object[]
		{
			num3
		});
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 9;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 5;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 1;

	[CustomizableProperty("Student Count")]
	public int m_studentCount = 100;

	[CustomizableProperty("Education Accumulation")]
	public int m_educationAccumulation = 100;

	[CustomizableProperty("Education Radius")]
	public float m_educationRadius = 500f;
}
