﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class SaraBorealisProperties : MonoBehaviour
{
	private void OnEnable()
	{
		SaraBorealisProperties.ID_PermTable1D = Shader.PropertyToID("_PermTable1D");
		SaraBorealisProperties.ID_PermTable2D = Shader.PropertyToID("_PermTable2D");
		SaraBorealisProperties.ID_Gradient4D = Shader.PropertyToID("_Gradient4D");
		SaraBorealisProperties.ID_Frequency = Shader.PropertyToID("_Frequency");
		SaraBorealisProperties.ID_Lacunarity = Shader.PropertyToID("_Lacunarity");
		SaraBorealisProperties.ID_Gain = Shader.PropertyToID("_Gain");
		SaraBorealisProperties.ID_Speed = Shader.PropertyToID("_Speed");
		SaraBorealisProperties.ID_Power = Shader.PropertyToID("_Power");
		SaraBorealisProperties.ID_Amount = Shader.PropertyToID("_Amount");
		this.m_Perlin = new ImprovedPerlinNoise(this.m_Seed);
		this.m_Perlin.LoadResourcesFor4DNoise();
		this.InitSkydomeMesh();
	}

	private void OnDisable()
	{
		if (this.m_SkydomeMesh)
		{
			Object.DestroyImmediate(this.m_SkydomeMesh);
			this.m_SkydomeMesh = null;
		}
	}

	private void InitSkydomeMesh()
	{
		if (this.m_SkydomeBaseMesh != null)
		{
			Mesh mesh = new Mesh();
			Vector3[] vertices = this.m_SkydomeBaseMesh.get_vertices();
			for (int i = 0; i < vertices.Length; i++)
			{
				Vector3[] expr_31_cp_0 = vertices;
				int expr_31_cp_1 = i;
				expr_31_cp_0[expr_31_cp_1].y = expr_31_cp_0[expr_31_cp_1].y * 0.85f;
			}
			mesh.set_vertices(vertices);
			mesh.set_triangles(this.m_SkydomeBaseMesh.get_triangles());
			mesh.set_normals(this.m_SkydomeBaseMesh.get_normals());
			mesh.set_uv(this.m_SkydomeBaseMesh.get_uv());
			mesh.set_uv2(this.m_SkydomeBaseMesh.get_uv2());
			mesh.set_bounds(new Bounds(Vector3.get_zero(), Vector3.get_one() * 2E+09f));
			mesh.set_hideFlags(52);
			mesh.set_name("SkydomeMesh");
			this.m_SkydomeMesh = mesh;
		}
		else
		{
			base.set_enabled(false);
		}
	}

	private void Update()
	{
		if (Singleton<WeatherManager>.get_exists())
		{
			this.m_Control = Singleton<WeatherManager>.get_instance().GetNorthernLightsVisibility();
		}
		if (this.m_Control > 0f)
		{
			this.m_SkyMaterial.SetTexture(SaraBorealisProperties.ID_PermTable1D, this.m_Perlin.GetPermutationTable1D());
			this.m_SkyMaterial.SetTexture(SaraBorealisProperties.ID_PermTable2D, this.m_Perlin.GetPermutationTable2D());
			this.m_SkyMaterial.SetTexture(SaraBorealisProperties.ID_Gradient4D, this.m_Perlin.GetGradient4D());
			this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Amount, this.m_Control);
			this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Frequency, this.m_Frequency);
			this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Lacunarity, this.m_Lacunarity);
			this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Gain, this.m_Gain);
			if (Singleton<SimulationManager>.get_exists())
			{
				this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Speed, Singleton<SimulationManager>.get_instance().m_simulationTimer2 * this.m_Speed);
			}
			this.m_SkyMaterial.SetFloat(SaraBorealisProperties.ID_Power, this.m_Power);
			Graphics.DrawMesh(this.m_SkydomeMesh, Vector3.get_zero(), Quaternion.get_identity(), this.m_SkyMaterial, this.m_SkyLayer);
		}
	}

	private static int ID_PermTable1D;

	private static int ID_PermTable2D;

	private static int ID_Gradient4D;

	private static int ID_Frequency;

	private static int ID_Lacunarity;

	private static int ID_Gain;

	private static int ID_Speed;

	private static int ID_Power;

	private static int ID_Amount;

	public int m_SkyLayer = 29;

	public Material m_SkyMaterial;

	public Mesh m_SkydomeBaseMesh;

	private Mesh m_SkydomeMesh;

	private ImprovedPerlinNoise m_Perlin;

	[Range(0f, 1f)]
	public float m_Control;

	public int m_Seed;

	public float m_Speed;

	public float m_Frequency = 10f;

	public float m_Lacunarity = 2f;

	public float m_Gain = 0.5f;

	public float m_Power = 3.4f;
}
