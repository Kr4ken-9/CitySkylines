﻿using System;

public class UltimateRecyclingPlantAI : LandfillSiteAI
{
	public override bool EnableNotUsedGuide()
	{
		return false;
	}

	public override bool IsWonder()
	{
		return true;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}
}
