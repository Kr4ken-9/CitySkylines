﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class MilestonesWrapper : IMilestones
{
	public MilestonesWrapper(UnlockManager unlockManager)
	{
		this.m_unlockManager = unlockManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnMilestonesExtensionsReleased();
		this.m_MilestonesExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IMilestonesExtension>();
		this.OnMilestonesExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnMilestonesExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnMilestonesExtensionsCreated()
	{
		for (int i = 0; i < this.m_MilestonesExtensions.Count; i++)
		{
			try
			{
				this.m_MilestonesExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_MilestonesExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnMilestonesExtensionsReleased()
	{
		for (int i = 0; i < this.m_MilestonesExtensions.Count; i++)
		{
			try
			{
				this.m_MilestonesExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_MilestonesExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnRefreshMilestones()
	{
		for (int i = 0; i < this.m_MilestonesExtensions.Count; i++)
		{
			this.m_MilestonesExtensions[i].OnRefreshMilestones();
		}
	}

	public int OnGetPopulationTarget(int originalTarget, int scaledTarget)
	{
		for (int i = 0; i < this.m_MilestonesExtensions.Count; i++)
		{
			scaledTarget = this.m_MilestonesExtensions[i].OnGetPopulationTarget(originalTarget, scaledTarget);
		}
		return scaledTarget;
	}

	public string[] EnumerateMilestones()
	{
		while (!Monitor.TryEnter(this.m_unlockManager.m_allMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		string[] result;
		try
		{
			Dictionary<string, MilestoneInfo>.KeyCollection keys = this.m_unlockManager.m_allMilestones.Keys;
			string[] array = new string[keys.Count];
			keys.CopyTo(array, 0);
			result = array;
		}
		finally
		{
			Monitor.Exit(this.m_unlockManager.m_allMilestones);
		}
		return result;
	}

	public void UnlockMilestone(string name)
	{
		MilestoneInfo milestoneInfo = null;
		while (!Monitor.TryEnter(this.m_unlockManager.m_allMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (!this.m_unlockManager.m_allMilestones.TryGetValue(name, out milestoneInfo))
			{
				milestoneInfo = null;
			}
		}
		finally
		{
			Monitor.Exit(this.m_unlockManager.m_allMilestones);
		}
		if (milestoneInfo != null)
		{
			this.m_unlockManager.CheckMilestone(milestoneInfo, true, false);
		}
		else
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Unknown milestone: " + name);
		}
	}

	private UnlockManager m_unlockManager;

	private List<IMilestonesExtension> m_MilestonesExtensions = new List<IMilestonesExtension>();
}
