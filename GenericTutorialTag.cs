﻿using System;
using UnityEngine;

public class GenericTutorialTag : IUITag
{
	public bool isValid
	{
		get
		{
			return true;
		}
	}

	public bool isDynamic
	{
		get
		{
			return false;
		}
	}

	public bool isRelative
	{
		get
		{
			return false;
		}
	}

	public bool locationLess
	{
		get
		{
			return true;
		}
	}

	public Vector3 position
	{
		get
		{
			return Vector3.get_zero();
		}
	}
}
