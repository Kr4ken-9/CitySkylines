﻿using System;
using ColossalFramework.IO;

public class StatisticArray : StatisticBase
{
	public override void Reset()
	{
		if (this.m_array != null)
		{
			for (int i = 0; i < this.m_array.Length; i++)
			{
				if (this.m_array[i] != null)
				{
					this.m_array[i].Reset();
				}
			}
		}
	}

	public override StatisticBase Acquire<T>(int index, int count)
	{
		if (this.m_array == null)
		{
			this.m_array = new StatisticBase[count];
		}
		if (this.m_array.Length < count)
		{
			StatisticBase[] array = new StatisticBase[count];
			for (int i = 0; i < this.m_array.Length; i++)
			{
				array[i] = this.m_array[i];
			}
			this.m_array = array;
		}
		StatisticBase statisticBase = this.m_array[index];
		if (statisticBase == null)
		{
			statisticBase = Activator.CreateInstance<T>();
			this.m_array[index] = statisticBase;
		}
		return statisticBase;
	}

	public override void Add(int delta)
	{
		throw new Exception("Method not supported");
	}

	public override void Set(int value)
	{
		throw new Exception("Method not supported");
	}

	public override void Add(long delta)
	{
		throw new Exception("Method not supported");
	}

	public override void Set(long value)
	{
		throw new Exception("Method not supported");
	}

	public override void Add(float delta)
	{
		throw new Exception("Method not supported");
	}

	public override void Set(float value)
	{
		throw new Exception("Method not supported");
	}

	public override void Tick()
	{
		if (this.m_array != null)
		{
			for (int i = 0; i < this.m_array.Length; i++)
			{
				if (this.m_array[i] != null)
				{
					this.m_array[i].Tick();
				}
			}
		}
	}

	public override void Serialize(DataSerializer s)
	{
		if (this.m_array != null)
		{
			s.WriteUInt16((uint)this.m_array.Length);
			for (int i = 0; i < this.m_array.Length; i++)
			{
				s.WriteObject<StatisticBase>(this.m_array[i]);
			}
		}
		else
		{
			s.WriteUInt16(0u);
		}
	}

	public override void Deserialize(DataSerializer s)
	{
		int num = (int)s.ReadUInt16();
		if (num != 0)
		{
			if (this.m_array == null || this.m_array.Length < num)
			{
				this.m_array = new StatisticBase[num];
			}
			for (int i = 0; i < num; i++)
			{
				this.m_array[i] = s.ReadObject<StatisticBase>();
			}
		}
		if (this.m_array != null)
		{
			for (int j = num; j < this.m_array.Length; j++)
			{
				if (this.m_array[j] != null)
				{
					this.m_array[j].Reset();
				}
			}
		}
	}

	public override void AfterDeserialize(DataSerializer s)
	{
	}

	public override StatisticBase Get(int index)
	{
		StatisticBase[] array = this.m_array;
		if (array != null && array.Length > index)
		{
			return array[index];
		}
		return null;
	}

	public override int GetLatestInt32()
	{
		int num = 0;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetLatestInt32();
				}
			}
		}
		return num;
	}

	public override int GetTotalInt32()
	{
		int num = 0;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetTotalInt32();
				}
			}
		}
		return num;
	}

	public override long GetLatestInt64()
	{
		long num = 0L;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetLatestInt64();
				}
			}
		}
		return num;
	}

	public override long GetTotalInt64()
	{
		long num = 0L;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetTotalInt64();
				}
			}
		}
		return num;
	}

	public override float GetLatestFloat()
	{
		float num = 0f;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetLatestFloat();
				}
			}
		}
		return num;
	}

	public override float GetTotalFloat()
	{
		float num = 0f;
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					num += statisticBase.GetTotalFloat();
				}
			}
		}
		return num;
	}

	public override void GetValues(double[] buffer, out int count, out DateTime startTime, out DateTime endTime)
	{
		for (int i = 0; i < buffer.Length; i++)
		{
			buffer[i] = 0.0;
		}
		count = 0;
		startTime = default(DateTime);
		endTime = default(DateTime);
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int j = 0; j < array.Length; j++)
			{
				StatisticBase statisticBase = array[j];
				if (statisticBase != null)
				{
					statisticBase.AddValues(buffer, ref count, ref startTime, ref endTime);
				}
			}
		}
	}

	public override void AddValues(double[] buffer, ref int count, ref DateTime startTime, ref DateTime endTime)
	{
		StatisticBase[] array = this.m_array;
		if (array != null)
		{
			for (int i = 0; i < array.Length; i++)
			{
				StatisticBase statisticBase = array[i];
				if (statisticBase != null)
				{
					statisticBase.AddValues(buffer, ref count, ref startTime, ref endTime);
				}
			}
		}
	}

	private StatisticBase[] m_array;
}
