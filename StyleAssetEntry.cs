﻿using System;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class StyleAssetEntry : UICustomControl
{
	public virtual Package.Asset asset
	{
		get
		{
			return this.m_Asset;
		}
		set
		{
			this.m_Asset = value;
		}
	}

	public string entryName
	{
		get
		{
			return this.m_NameLabel.get_text();
		}
		set
		{
			this.m_NameInMeta = value;
			int num = this.m_NameInMeta.LastIndexOf(".");
			if (num != -1)
			{
				if (PackageManager.FindAssetByName(value) == null)
				{
					this.m_NameLabel.set_text("<color #FF3232>MISSING - </color>" + this.m_NameInMeta.Substring(num + 1));
				}
				else
				{
					this.m_NameLabel.set_text(this.m_NameInMeta.Substring(num + 1));
				}
			}
			else
			{
				this.m_NameLabel.set_text("<color #FF3232>Invalid asset reference</color>");
			}
		}
	}

	public Texture image
	{
		get
		{
			return this.m_Image.get_texture();
		}
		set
		{
			this.m_Image.set_texture(value);
		}
	}

	public PublishedFileId publishedFileId
	{
		get
		{
			return this.m_PublishedFileId;
		}
		set
		{
			this.m_PublishedFileId = value;
			if (this.m_ViewButton != null)
			{
				UIComponent arg_3C_0 = this.m_ViewButton;
				bool flag = this.m_PublishedFileId != PublishedFileId.invalid;
				this.m_ViewButton.set_isVisible(flag);
				arg_3C_0.set_isEnabled(flag);
			}
		}
	}

	public void OpenSubItemWorkshopInSteam(UIComponent c, UIMouseEventParameter p)
	{
		if (this.publishedFileId != PublishedFileId.invalid && PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToWorkshopItem(this.publishedFileId);
		}
	}

	public void OnRemoveSubAsset(UIComponent c, UIMouseEventParameter p)
	{
		DistrictStyleMetaData districtStyleMetaData = this.m_Asset.Instantiate<DistrictStyleMetaData>();
		string[] array = new string[districtStyleMetaData.assets.Length - 1];
		int num = 0;
		for (int i = 0; i < districtStyleMetaData.assets.Length; i++)
		{
			if (districtStyleMetaData.assets[i] != this.m_NameInMeta)
			{
				array[num++] = districtStyleMetaData.assets[i];
			}
		}
		districtStyleMetaData.assets = array;
		StylesHelper.SaveStyle(districtStyleMetaData, this.asset.get_name(), this.asset.get_isWorkshopAsset(), null);
		GroupedPackageEntry.ExpandSubAssets(this.m_Asset);
	}

	private void Awake()
	{
		this.m_Image = base.Find<UITextureSprite>("ImageAsset");
		this.m_NameLabel = base.Find<UILabel>("NameAsset");
		this.m_ViewButton = base.Find<UIButton>("ViewAsset");
		if (this.m_ViewButton != null)
		{
			this.m_ViewButton.add_eventClick(new MouseEventHandler(this.OpenSubItemWorkshopInSteam));
		}
		this.m_RemoveButton = base.Find<UIButton>("RemoveAsset");
		if (this.m_RemoveButton != null)
		{
			this.m_RemoveButton.add_eventClick(new MouseEventHandler(this.OnRemoveSubAsset));
		}
	}

	private void Start()
	{
		UIPanel uIPanel = base.get_component() as UIPanel;
		if (base.get_component().get_zOrder() % 3 == 0 || base.get_component().get_zOrder() % 4 == 0)
		{
			uIPanel.set_backgroundSprite(string.Empty);
		}
	}

	private void Reset()
	{
		if (this.m_Image.get_texture() != null)
		{
			Object.Destroy(this.m_Image.get_texture());
		}
		this.m_Image.set_texture(null);
	}

	private void OnDestroy()
	{
		this.Reset();
	}

	private UIButton m_ViewButton;

	private UIButton m_RemoveButton;

	private UITextureSprite m_Image;

	private UILabel m_NameLabel;

	private PublishedFileId m_PublishedFileId = PublishedFileId.invalid;

	private string m_NameInMeta;

	private Package.Asset m_Asset;
}
