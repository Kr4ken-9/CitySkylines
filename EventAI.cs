﻿using System;
using ColossalFramework;
using UnityEngine;

public class EventAI : PrefabAI
{
	public virtual void InitializeAI()
	{
	}

	public virtual void ReleaseAI()
	{
	}

	public virtual string GetDebugString(ushort eventID, ref EventData data)
	{
		int num;
		int num2;
		int num3;
		this.CountVisitors(eventID, ref data, out num, out num2, out num3);
		string eventName = this.GetEventName(eventID, ref data);
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		if ((data.m_flags & EventData.Flags.Expired) != EventData.Flags.None)
		{
			return StringUtils.SafeFormat("{0}\nExpired", eventName);
		}
		if ((data.m_flags & EventData.Flags.Disorganizing) != EventData.Flags.None)
		{
			uint num4 = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint num5 = (uint)Mathf.RoundToInt(this.m_disorganizeDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint val = data.m_startFrame + num4 + num5;
			if ((data.m_flags & EventData.Flags.Success) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nDisorganizing (Success)\nEnding in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, val) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			if ((data.m_flags & EventData.Flags.Failure) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nDisorganizing (Failure)\nEnding in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, val) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			return StringUtils.SafeFormat("{0}\nDisorganizing\nEnding in {1}\nVisitors: {2}(+{3})/{4}", new object[]
			{
				eventName,
				(Math.Max(currentFrameIndex, val) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
				num,
				num2,
				num3
			});
		}
		else if ((data.m_flags & EventData.Flags.Completed) != EventData.Flags.None)
		{
			if ((data.m_flags & EventData.Flags.Success) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nCompleted (Success)\nExpiring in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, data.m_expireFrame) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			if ((data.m_flags & EventData.Flags.Failure) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nCompleted (Failure)\nExpiring in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, data.m_expireFrame) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			return StringUtils.SafeFormat("{0}\nCompleted\nExpiring in {1}\nVisitors: {2}(+{3})/{4}", new object[]
			{
				eventName,
				(Math.Max(currentFrameIndex, data.m_expireFrame) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
				num,
				num2,
				num3
			});
		}
		else
		{
			if ((data.m_flags & EventData.Flags.Cancelled) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nCancelled\nExpiring in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, data.m_expireFrame) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
			{
				uint num6 = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
				uint val2 = data.m_startFrame + num6;
				return StringUtils.SafeFormat("{0}\nActive\nEvent ending in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, val2) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			if ((data.m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
			{
				return StringUtils.SafeFormat("{0}\nPreparing\nEvent beginning in {1}\nVisitors: {2}(+{3})/{4}", new object[]
				{
					eventName,
					(Math.Max(currentFrameIndex, data.m_startFrame) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR,
					num,
					num2,
					num3
				});
			}
			uint num7 = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint val3 = data.m_startFrame - num7;
			return StringUtils.SafeFormat("{0}\nPreparing beginning in {1}", new object[]
			{
				eventName,
				(Math.Max(currentFrameIndex, val3) - currentFrameIndex) * SimulationManager.DAYTIME_FRAME_TO_HOUR
			});
		}
	}

	protected virtual string GetEventName(ushort eventID, ref EventData data)
	{
		return PrefabCollection<EventInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
	}

	public virtual void GetAnimationState(ushort eventID, ref EventData data, ref float active, ref int state, ref bool playActiveState, ref Color color1, ref Color color2)
	{
	}

	public virtual bool GetAudioState(ushort eventID, ref EventData data, ref float volume, ref float maxDistance, ref AudioInfo clip)
	{
		return true;
	}

	public virtual void CreateEvent(ushort eventID, ref EventData data)
	{
		uint prevExpireFrame = 0u;
		if (data.m_nextBuildingEvent != 0)
		{
			prevExpireFrame = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)data.m_nextBuildingEvent].m_expireFrame;
		}
		data.m_startFrame = this.CalculateStartFrame(prevExpireFrame, true);
		data.m_expireFrame = this.CalculateExpireFrame(data.m_startFrame);
	}

	public virtual uint CalculateStartFrame(uint prevExpireFrame, bool checkCurrentFrame)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		uint dAYTIME_FRAMES = SimulationManager.DAYTIME_FRAMES;
		uint num = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num2 = (uint)Mathf.RoundToInt(this.m_startTime * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num3 = num2 - num & dAYTIME_FRAMES - 1u;
		uint num5;
		if (checkCurrentFrame)
		{
			uint num4 = 0u;
			if (instance.m_enableDayNight)
			{
				uint dayTimeFrame = instance.m_dayTimeFrame;
				num4 = (num3 - dayTimeFrame & dAYTIME_FRAMES - 1u);
			}
			uint currentFrameIndex = instance.m_currentFrameIndex;
			num5 = currentFrameIndex + num4 + num;
		}
		else
		{
			uint num6 = 0u;
			if (instance.m_enableDayNight)
			{
				uint num7 = prevExpireFrame + instance.m_dayTimeOffsetFrames & SimulationManager.DAYTIME_FRAMES - 1u;
				num6 = (num3 - num7 & dAYTIME_FRAMES - 1u);
			}
			num5 = prevExpireFrame + num6 + num;
		}
		if (prevExpireFrame > num5 - num)
		{
			num5 += (prevExpireFrame - (num5 - num) + dAYTIME_FRAMES - 1u) / dAYTIME_FRAMES * dAYTIME_FRAMES;
		}
		return num5;
	}

	public virtual uint CalculateExpireFrame(uint startFrame)
	{
		uint num = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num2 = (uint)Mathf.RoundToInt(this.m_disorganizeDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num3 = (uint)Mathf.RoundToInt(this.m_nextDelayMin * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num4 = (uint)Mathf.RoundToInt(this.m_nextDelayMax * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num5 = Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(num3, num4);
		return startFrame + num + num2 + num5;
	}

	public virtual void ReleaseEvent(ushort eventID, ref EventData data)
	{
	}

	public virtual void SimulationStep(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & EventData.Flags.Expired) != EventData.Flags.None)
		{
			return;
		}
		if ((data.m_flags & EventData.Flags.Disorganizing) != EventData.Flags.None)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint disorganizingEndFrame = this.GetDisorganizingEndFrame(eventID, ref data);
			if (currentFrameIndex >= disorganizingEndFrame)
			{
				this.EndDisorganizing(eventID, ref data);
			}
		}
		else if ((data.m_flags & (EventData.Flags.Completed | EventData.Flags.Cancelled)) != EventData.Flags.None)
		{
			uint currentFrameIndex2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint expireFrame = this.GetExpireFrame(eventID, ref data);
			if (currentFrameIndex2 >= expireFrame)
			{
				this.Expire(eventID, ref data);
			}
		}
		else if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
		{
			uint currentFrameIndex3 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint endFrame = this.GetEndFrame(eventID, ref data);
			if (currentFrameIndex3 >= endFrame)
			{
				this.EndEvent(eventID, ref data);
			}
		}
		else if ((data.m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
		{
			uint currentFrameIndex4 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint startFrame = this.GetStartFrame(eventID, ref data);
			if (currentFrameIndex4 >= startFrame)
			{
				this.BeginEvent(eventID, ref data);
			}
		}
		else
		{
			uint currentFrameIndex5 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint prepareStartFrame = this.GetPrepareStartFrame(eventID, ref data);
			if (currentFrameIndex5 >= prepareStartFrame)
			{
				this.BeginPreparing(eventID, ref data);
			}
		}
	}

	private uint GetDisorganizingEndFrame(ushort eventID, ref EventData data)
	{
		uint num = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		uint num2 = (uint)Mathf.RoundToInt(this.m_disorganizeDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		return data.m_startFrame + num + num2;
	}

	private uint GetExpireFrame(ushort eventID, ref EventData data)
	{
		return data.m_expireFrame;
	}

	private uint GetEndFrame(ushort eventID, ref EventData data)
	{
		uint num = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		return data.m_startFrame + num;
	}

	private uint GetStartFrame(ushort eventID, ref EventData data)
	{
		return data.m_startFrame;
	}

	private uint GetPrepareStartFrame(ushort eventID, ref EventData data)
	{
		uint num = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
		return data.m_startFrame - num;
	}

	public void SkipState_OnlyForDebugging(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & EventData.Flags.Expired) != EventData.Flags.None)
		{
			return;
		}
		uint num;
		if ((data.m_flags & EventData.Flags.Disorganizing) != EventData.Flags.None)
		{
			num = this.GetDisorganizingEndFrame(eventID, ref data);
			this.EndDisorganizing(eventID, ref data);
		}
		else if ((data.m_flags & (EventData.Flags.Completed | EventData.Flags.Cancelled)) != EventData.Flags.None)
		{
			num = this.GetExpireFrame(eventID, ref data);
			this.Expire(eventID, ref data);
		}
		else if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
		{
			num = this.GetEndFrame(eventID, ref data);
			this.EndEvent(eventID, ref data);
		}
		else if ((data.m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
		{
			num = this.GetStartFrame(eventID, ref data);
			this.BeginEvent(eventID, ref data);
		}
		else
		{
			num = this.GetPrepareStartFrame(eventID, ref data);
			this.BeginPreparing(eventID, ref data);
		}
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		if (currentFrameIndex < num)
		{
			uint num2 = num - currentFrameIndex;
			if (data.m_startFrame < num2)
			{
				data.m_startFrame = 0u;
			}
			else
			{
				data.m_startFrame -= num2;
			}
			if (data.m_expireFrame < num2)
			{
				data.m_expireFrame = 0u;
			}
			else
			{
				data.m_expireFrame -= num2;
			}
		}
	}

	protected virtual void BeginPreparing(ushort eventID, ref EventData data)
	{
		data.m_flags |= EventData.Flags.Preparing;
	}

	protected virtual void BeginEvent(ushort eventID, ref EventData data)
	{
		data.m_flags = ((data.m_flags & ~EventData.Flags.Preparing) | EventData.Flags.Active);
	}

	protected virtual void EndEvent(ushort eventID, ref EventData data)
	{
		data.m_flags = ((data.m_flags & ~EventData.Flags.Active) | (EventData.Flags.Completed | EventData.Flags.Disorganizing));
	}

	protected virtual void EndDisorganizing(ushort eventID, ref EventData data)
	{
		data.m_flags &= ~EventData.Flags.Disorganizing;
	}

	protected virtual void Cancel(ushort eventID, ref EventData data)
	{
		data.m_flags = ((data.m_flags & ~(EventData.Flags.Preparing | EventData.Flags.Active)) | EventData.Flags.Cancelled);
	}

	protected virtual void Expire(ushort eventID, ref EventData data)
	{
		data.m_flags |= EventData.Flags.Expired;
	}

	public virtual void SetColor(ushort eventID, ref EventData data, Color32 newColor)
	{
		data.m_color = newColor;
	}

	public virtual void SetTicketPrice(ushort eventID, ref EventData data, int newPrice)
	{
		data.m_ticketPrice = (ushort)newPrice;
	}

	public virtual void SetSecurityBudget(ushort eventID, ref EventData data, int newBudget)
	{
		data.m_securityBudget = (ushort)newBudget;
	}

	public virtual void BuildingDeactivated(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & (EventData.Flags.Completed | EventData.Flags.Cancelled)) == EventData.Flags.None)
		{
			this.Cancel(eventID, ref data);
		}
	}

	public virtual void BuildingRelocated(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
		{
			this.Cancel(eventID, ref data);
		}
	}

	public virtual void VisitorEnter(ushort eventID, ref EventData data, ushort buildingID, uint citizenID)
	{
	}

	public virtual BuildingInfo ModifySubBuildingInfo(BuildingInfo orig)
	{
		return orig;
	}

	public virtual int GetEventPriority(int freeSpace, int maxSpace)
	{
		return freeSpace * 8 / maxSpace;
	}

	public virtual Color32 GetCitizenColor(ushort eventID, ref EventData data, uint citizen)
	{
		return new Color32(0, 0, 0, 0);
	}

	public virtual Color32 GetBuildingColor(ushort eventID, ref EventData data)
	{
		return new Color32(0, 0, 0, 0);
	}

	public virtual bool IsBuildingActive(ushort eventID, ref EventData data)
	{
		return true;
	}

	public virtual DistrictPolicies.Event GetEventPolicyMask(ushort eventID, ref EventData data)
	{
		return DistrictPolicies.Event.None;
	}

	public virtual int GetBudget(ushort eventID, ref EventData data)
	{
		if (data.m_building != 0)
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].Info;
			return Singleton<EconomyManager>.get_instance().GetBudget(info.m_class);
		}
		return 100;
	}

	public virtual int GetTicketPrice(ushort eventID, ref EventData data)
	{
		return (int)data.m_ticketPrice;
	}

	public virtual int GetSecurityBudget(ushort eventID, ref EventData data)
	{
		return (int)data.m_securityBudget;
	}

	public virtual int GetCapacity(ushort eventID, ref EventData data, int orig, int max)
	{
		return orig;
	}

	public virtual CitizenInstance.Flags GetHangAroundFlags(ushort eventID, ref EventData data, ushort buildingID, ref Vector3 position, ref Vector2 direction)
	{
		return CitizenInstance.Flags.HangAround;
	}

	public virtual void ModifyNoise(ushort eventID, ref EventData data, ref int accumulation, ref float range)
	{
	}

	public virtual int GetCrimeAccumulation(ushort eventID, ref EventData data, int orig)
	{
		return orig;
	}

	public void CountVisitors(ushort eventID, ref EventData data, out int arrivedVisitors, out int incomingVisitors, out int maxVisitors)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		incomingVisitors = 0;
		arrivedVisitors = 0;
		maxVisitors = 0;
		ushort building = data.m_building;
		if (building != 0)
		{
			uint num = instance.m_buildings.m_buffer[(int)building].m_citizenUnits;
			int num2 = 0;
			while (num != 0u)
			{
				uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				if ((ushort)(instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
				{
					for (int i = 0; i < 5; i++)
					{
						uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
						if (citizen != 0u)
						{
							if (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == building)
							{
								arrivedVisitors++;
							}
							else
							{
								incomingVisitors++;
							}
						}
						maxVisitors++;
					}
				}
				num = nextUnit;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public float m_prepareDuration = 14f;

	public float m_startTime = 15f;

	public float m_eventDuration = 7f;

	public float m_disorganizeDuration = 7f;

	public float m_nextDelayMin = 40f;

	public float m_nextDelayMax = 40f;

	public int m_ticketPrice = 3000;

	public int m_securityBudget;

	[NonSerialized]
	public EventInfo m_info;
}
