﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

[Serializable]
public class MilestoneInfo : ScriptableObject
{
	private void OnEnable()
	{
		this.m_name = base.get_name();
	}

	public virtual string GetLocalizedName()
	{
		return Locale.Get("UNLOCK_MILESTONE_TITLE", this.m_name);
	}

	public MilestoneInfo.ProgressInfo GetLocalizedProgress()
	{
		MilestoneInfo.m_totalProgress.m_description = null;
		MilestoneInfo.m_totalProgress.m_progress = null;
		if (MilestoneInfo.m_totalProgress.m_prefabs != null)
		{
			MilestoneInfo.m_totalProgress.m_prefabs.Clear();
		}
		MilestoneInfo.m_subProgress.Clear();
		this.GetLocalizedProgressImpl(1, ref MilestoneInfo.m_totalProgress, MilestoneInfo.m_subProgress);
		if (!this.m_checkNeeded && !this.m_checkDisabled && Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().MilestonCheckNeeded(this);
		}
		return MilestoneInfo.m_totalProgress;
	}

	public MilestoneInfo.ProgressInfo GetLocalizedProgress(out FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		MilestoneInfo.m_totalProgress.m_description = null;
		MilestoneInfo.m_totalProgress.m_progress = null;
		if (MilestoneInfo.m_totalProgress.m_prefabs != null)
		{
			MilestoneInfo.m_totalProgress.m_prefabs.Clear();
		}
		MilestoneInfo.m_subProgress.Clear();
		this.GetLocalizedProgressImpl(1, ref MilestoneInfo.m_totalProgress, MilestoneInfo.m_subProgress);
		subProgress = MilestoneInfo.m_subProgress;
		if (!this.m_checkNeeded && !this.m_checkDisabled && Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().MilestonCheckNeeded(this);
		}
		return MilestoneInfo.m_totalProgress;
	}

	public virtual void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
	}

	public GeneratedString GetDescription()
	{
		return this.GetDescriptionImpl(1);
	}

	public virtual GeneratedString GetDescriptionImpl(int targetCount)
	{
		return new GeneratedString.Locale("UNLOCK_MILESTONE_TITLE", this.m_name);
	}

	public virtual int GetComparisonValue()
	{
		return -1;
	}

	public virtual MilestoneInfo.Data GetData()
	{
		if (this.m_data == null)
		{
			this.m_data = new MilestoneInfo.Data();
			this.m_data.m_isAssigned = true;
		}
		return this.m_data;
	}

	public virtual void SetData(MilestoneInfo.Data data)
	{
		if (data == null || data.m_isAssigned)
		{
			return;
		}
		data.m_isAssigned = true;
		if (this.m_data == null)
		{
			this.m_data = data;
		}
		else
		{
			this.m_data.m_passedCount = data.m_passedCount;
			this.m_data.m_progress = data.m_progress;
		}
		this.m_written = true;
	}

	public virtual MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		return this.GetData();
	}

	public virtual void ResetWrittenStatus()
	{
		this.m_written = false;
	}

	public void Reset(bool onlyUnwritten)
	{
		this.ResetImpl(1, true, onlyUnwritten);
	}

	public virtual void ResetImpl(int maxPassCount, bool directReference, bool onlyUnwritten)
	{
		if (!this.m_written || !onlyUnwritten)
		{
			MilestoneInfo.Data data = this.GetData();
			data.m_passedCount = 0;
			data.m_progress = 0L;
		}
		this.m_maxPassCount = Mathf.Max(this.m_maxPassCount, maxPassCount);
		if (directReference)
		{
			this.m_directReference = true;
		}
		UnlockManager instance = Singleton<UnlockManager>.get_instance();
		while (!Monitor.TryEnter(instance.m_allMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			instance.m_allMilestones[this.m_name] = this;
		}
		finally
		{
			Monitor.Exit(instance.m_allMilestones);
		}
	}

	public virtual void Serialize(DataSerializer s)
	{
		s.WriteInt32(this.m_rewardCash);
		s.WriteBool(this.m_openUnlockPanel);
		s.WriteBool(this.m_hidden);
		s.WriteBool(this.m_canRelock);
		s.WriteBool(this.m_isGlobal);
	}

	public virtual void Deserialize(DataSerializer s)
	{
		this.m_rewardCash = s.ReadInt32();
		this.m_openUnlockPanel = s.ReadBool();
		this.m_hidden = s.ReadBool();
		this.m_canRelock = s.ReadBool();
		this.m_isGlobal = s.ReadBool();
	}

	public virtual float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		return (data.m_progress == 0L) ? 0f : 1f;
	}

	public bool IsPassed()
	{
		MilestoneInfo.Data data = this.m_data;
		return data != null && data.m_passedCount != 0;
	}

	public MilestoneInfo m_prevMilestone;

	public MilestoneInfo m_nextMilestone;

	public int m_rewardCash;

	public MessageInfo m_reachMessage;

	public string m_steamAchievementName;

	public bool m_openUnlockPanel;

	public bool m_hidden;

	public bool m_canRelock;

	public bool m_isGlobal;

	[NonSerialized]
	public MilestoneInfo.Data m_data;

	[NonSerialized]
	public bool m_written;

	[NonSerialized]
	public int m_maxPassCount;

	[NonSerialized]
	public bool m_directReference;

	[NonSerialized]
	public bool m_checkNeeded;

	[NonSerialized]
	public bool m_checkDisabled;

	[NonSerialized]
	public string m_name;

	private static MilestoneInfo.ProgressInfo m_totalProgress = default(MilestoneInfo.ProgressInfo);

	private static FastList<MilestoneInfo.ProgressInfo> m_subProgress = new FastList<MilestoneInfo.ProgressInfo>();

	public struct ProgressInfo
	{
		public bool m_passed;

		public float m_min;

		public float m_max;

		public float m_current;

		public string m_description;

		public string m_progress;

		public FastList<PrefabInfo> m_prefabs;
	}

	public class Data : IDataContainer
	{
		public virtual void Serialize(DataSerializer s)
		{
			s.WriteInt32(this.m_passedCount);
			s.WriteLong64(this.m_progress);
		}

		public virtual void Deserialize(DataSerializer s)
		{
			if (s.get_version() >= 146u)
			{
				this.m_passedCount = s.ReadInt32();
				this.m_progress = s.ReadLong64();
			}
			else
			{
				this.m_passedCount = ((!s.ReadBool()) ? 0 : 1);
				this.m_progress = ((this.m_passedCount == 0) ? 0L : 1L);
				if (s.get_version() < 128u)
				{
					s.ReadObjectArray<MilestoneInfo.Data>();
				}
			}
			this.m_isAssigned = false;
		}

		public virtual void AfterDeserialize(DataSerializer s)
		{
		}

		public int m_passedCount;

		public long m_progress;

		public bool m_isAssigned;
	}
}
