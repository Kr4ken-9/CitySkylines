﻿using System;
using System.IO;
using ColossalFramework.IO;
using ColossalFramework.Packaging;

[Serializable]
public class MapMetaData : MetaData
{
	public bool IsBuiltinMap(string path)
	{
		return this.builtin && path.StartsWith(Path.Combine(DataLocation.get_gameContentPath(), "Maps"));
	}

	public bool isPublished
	{
		get
		{
			return this.publishedHash == this.mapName.GetHashCode();
		}
	}

	public override bool getBuiltin
	{
		get
		{
			return this.builtin;
		}
	}

	public override string getEnvironment
	{
		get
		{
			return this.environment;
		}
	}

	public override int getBuildableArea
	{
		get
		{
			return this.buildableArea;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public bool builtin;

	public string environment;

	public string mapThemeRef;

	public string mapName;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;

	public int publishedHash;

	public int buildableArea;

	public float maxResourceAmount;

	public uint oilAmount;

	public uint oreAmount;

	public uint forestAmount;

	public uint fertileLandAmount;

	public uint waterAmount;

	public int incomingRoads;

	public int outgoingRoads;

	public int incomingTrainTracks;

	public int outgoingTrainTracks;

	public int incomingShipPath;

	public int outgoingShipPath;

	public int incomingPlanePath;

	public int outgoingPlanePath;

	public ModInfo[] mods;
}
