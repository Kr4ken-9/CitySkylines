﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public abstract class LoadSavePanelBase<T> : ToolsModifierControl where T : MetaData
{
	public bool showMissingThemeWarning
	{
		get
		{
			return this.m_ShowMissingThemeWarning;
		}
	}

	protected virtual void Awake()
	{
		this.m_Items = new List<LoadSavePanelBase<T>.ListItem>();
		this.m_BuiltinItems = new List<LoadSavePanelBase<T>.ListItem>();
		this.RefreshSortTypeToImplDict();
		this.SortImpl = null;
		this.m_SortBy = base.Find<UIDropDown>("SortBy");
		if (this.m_SortBy != null)
		{
			this.m_SortBy.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnSortSelectionChanged));
			this.UpdateSorting();
		}
	}

	protected static string ResolveName(string str)
	{
		string[] array = str.Split(new string[]
		{
			":"
		}, StringSplitOptions.RemoveEmptyEntries);
		ulong num;
		if (array.Length == 2 && array[0] == "steamid" && ulong.TryParse(array[1], out num))
		{
			return new Friend(new UserID(num)).get_personaName();
		}
		return "Unknown";
	}

	protected static string GetThemeString(string metaDataRef, string environment, string formatting = null)
	{
		string text;
		if (metaDataRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(metaDataRef);
			if (asset == null)
			{
				int num = metaDataRef.IndexOf(".");
				if (num != -1)
				{
					text = metaDataRef.Substring(num + 1);
				}
				else
				{
					text = metaDataRef;
				}
				if (!string.IsNullOrEmpty(formatting))
				{
					text = LocaleFormatter.FormatGeneric(formatting, new object[]
					{
						text
					});
				}
				else
				{
					text = "<color #ff3232>" + text + "</color>";
				}
			}
			else
			{
				MapThemeMetaData mapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				if (mapThemeMetaData != null)
				{
					text = mapThemeMetaData.name;
				}
				else
				{
					text = asset.get_name();
				}
				if (!string.IsNullOrEmpty(formatting))
				{
					text = LocaleFormatter.FormatGeneric(formatting, new object[]
					{
						text
					});
				}
				if (!string.IsNullOrEmpty(asset.get_package().get_packageAuthor()))
				{
					text = text + " (" + LocaleFormatter.FormatGeneric("PACKAGEAUTHOR", new object[]
					{
						LoadSavePanelBase<T>.ResolveName(asset.get_package().get_packageAuthor())
					}) + ")";
				}
			}
		}
		else
		{
			text = environment;
			if (Locale.Exists("THEME_NAME", text))
			{
				text = Locale.Get("THEME_NAME", text);
			}
			if (!string.IsNullOrEmpty(formatting))
			{
				text = LocaleFormatter.FormatGeneric(formatting, new object[]
				{
					text
				});
			}
		}
		return text;
	}

	protected void RebuildThemeDictionary()
	{
		if (this.m_Themes == null)
		{
			return;
		}
		foreach (string current in this.m_Themes.Keys)
		{
			this.m_Themes[current].Clear();
		}
		foreach (Package.Asset current2 in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapThemeMetaData
		}))
		{
			if (current2 != null && current2.get_isEnabled())
			{
				MapThemeMetaData mapThemeMetaData = current2.Instantiate<MapThemeMetaData>();
				mapThemeMetaData.SetSelfRef(current2);
				if (!this.m_Themes.ContainsKey(mapThemeMetaData.environment))
				{
					List<MapThemeMetaData> value = new List<MapThemeMetaData>();
					this.m_Themes[mapThemeMetaData.environment] = value;
				}
				this.m_Themes[mapThemeMetaData.environment].Add(mapThemeMetaData);
			}
		}
		this.RefreshOverrideThemes();
	}

	protected void RefreshOverrideThemes()
	{
		if (this.m_Themes == null || this.m_ThemeOverrideDropDown == null)
		{
			return;
		}
		if (!string.IsNullOrEmpty(this.m_SelectedEnvironment))
		{
			if (!this.m_Themes.ContainsKey(this.m_SelectedEnvironment))
			{
				this.m_Themes[this.m_SelectedEnvironment] = new List<MapThemeMetaData>();
			}
			List<MapThemeMetaData> list = this.m_Themes[this.m_SelectedEnvironment];
			string[] array = new string[list.Count + 1];
			array[0] = Locale.Get("MAPTHEME_NONE");
			for (int i = 0; i < list.Count; i++)
			{
				string name = list[i].name;
				if (list[i] != null)
				{
					array[i + 1] = name;
				}
			}
			this.m_ThemeOverrideDropDown.set_items(array);
		}
		else
		{
			this.m_ThemeOverrideDropDown.set_items(null);
		}
	}

	protected void SelectThemeOverride(string name)
	{
		this.m_ShowMissingThemeWarning = false;
		if (this.m_Themes == null || this.m_ThemeOverrideDropDown == null)
		{
			return;
		}
		if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(this.m_SelectedEnvironment))
		{
			this.m_ThemeOverrideDropDown.set_selectedIndex(0);
		}
		else if (!this.m_Themes.ContainsKey(this.m_SelectedEnvironment) || this.m_Themes[this.m_SelectedEnvironment].Count == 0)
		{
			this.m_ThemeOverrideDropDown.set_selectedIndex(0);
			this.m_ShowMissingThemeWarning = true;
		}
		else
		{
			List<MapThemeMetaData> list = this.m_Themes[this.m_SelectedEnvironment];
			for (int i = 0; i < list.Count; i++)
			{
				if (name == list[i].mapThemeRef)
				{
					this.m_ThemeOverrideDropDown.set_selectedIndex(i + 1);
					return;
				}
			}
			this.m_ThemeOverrideDropDown.set_selectedIndex(0);
			this.m_ShowMissingThemeWarning = true;
		}
	}

	protected ModInfo[] EmbedModInfo()
	{
		List<ModInfo> list = new List<ModInfo>();
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			if (current.get_isEnabled())
			{
				list.Add(new ModInfo
				{
					modName = current.get_name(),
					modWorkshopID = current.get_publishedFileID().get_AsUInt64()
				});
			}
		}
		return list.ToArray();
	}

	protected ModInfo[] EmbedAssetInfo()
	{
		List<ModInfo> list = new List<ModInfo>();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.CustomAssetMetaData
		}))
		{
			if (current.get_isEnabled())
			{
				list.Add(new ModInfo
				{
					modName = current.get_fullName(),
					modWorkshopID = current.get_package().GetPublishedFileID().get_AsUInt64()
				});
			}
		}
		return list.ToArray();
	}

	protected void PrintModsInfo(ModInfo[] mods)
	{
		if (mods == null)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "Asset version is too old and does not contain mods info");
			return;
		}
		if (mods.Length == 0)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "No mods were used when this asset was created");
			return;
		}
		string text = string.Empty;
		for (int i = 0; i < mods.Length; i++)
		{
			text = text + mods[i].modName + "\n";
		}
		CODebugBase<LogChannel>.Log(LogChannel.Modding, "The following mods were used when this asset was created: \n" + text);
	}

	protected void PrintModsInfo(CustomAssetMetaData meta)
	{
		if (meta.mods == null)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "Asset version is too old and does not contain mods info");
			return;
		}
		if (meta.mods.Length == 0)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "No mods were used when this asset was created");
			return;
		}
		string text = string.Empty;
		for (int i = 0; i < meta.mods.Length; i++)
		{
			text = text + meta.mods[i].modName + "\n";
		}
		CODebugBase<LogChannel>.Log(LogChannel.Modding, "The following mods were used when this asset was created: \n" + text);
	}

	protected void PrintModsInfo(MapMetaData meta)
	{
		if (meta.mods == null)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "Asset version is too old and does not contain mods info");
			return;
		}
		if (meta.mods.Length == 0)
		{
			CODebugBase<LogChannel>.Log(LogChannel.Modding, "No mods were used when this asset was created");
			return;
		}
		string text = string.Empty;
		for (int i = 0; i < meta.mods.Length; i++)
		{
			text = text + meta.mods[i].modName + "\n";
		}
		CODebugBase<LogChannel>.Log(LogChannel.Modding, "The following mods were used when this asset was created: \n" + text);
	}

	public virtual void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	protected virtual void OnLocaleChanged()
	{
		this.RefreshSortTypeToImplDict();
	}

	private void OnEnable()
	{
		PackageManager.add_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.Refresh));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		PackageManager.remove_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.Refresh));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	protected virtual void Refresh()
	{
	}

	protected void OnDelete(string saveName)
	{
		ConfirmPanel.ShowModal("CONFIRM_SAVEDELETE", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.DoDelete(saveName);
			}
		});
	}

	private bool DoDelete(string path)
	{
		try
		{
			File.Delete(path);
		}
		catch (Exception ex)
		{
			Debug.LogError("An exception occured " + ex);
			UIView.ForwardException(ex);
			return false;
		}
		return true;
	}

	protected void ClearListing()
	{
		this.m_Items.Clear();
		this.m_BuiltinItems.Clear();
	}

	protected void AddToListing(string name, Package.Asset asset, T mmd)
	{
		this.AddToListing(name, default(DateTime), asset, mmd, false);
	}

	protected void AddToListing(string name, DateTime timeStamp, Package.Asset asset, T mmd, bool addTimeStamp = true)
	{
		string text = string.Empty;
		if (asset.get_isCloudAsset())
		{
			if (PlatformService.get_platformType() == 1)
			{
				text += "<sprite TGPCloud> ";
			}
			else if (PlatformService.get_platformType() == 2)
			{
				text += "<sprite QQCloud> ";
			}
			else
			{
				text += "<sprite SteamCloud> ";
			}
		}
		if (asset.get_isWorkshopAsset())
		{
			if (PlatformService.get_platformType() == 1)
			{
				text += "<sprite TGPSpaceWork> ";
			}
			else if (PlatformService.get_platformType() == 2)
			{
				text += "<sprite QQSpaceWork> ";
			}
			else
			{
				text += "<sprite SteamWorkshop> ";
			}
		}
		text += name;
		if (addTimeStamp)
		{
			text += Environment.NewLine;
			if (asset.get_isCloudAsset() || asset.get_isWorkshopAsset())
			{
				text = text + "        <color #50869a>" + timeStamp.ToString() + "</color>";
			}
			else
			{
				text = text + "<color #50869a>" + timeStamp.ToString() + "</color>";
			}
		}
		LoadSavePanelBase<T>.ListItem item = new LoadSavePanelBase<T>.ListItem(text, asset, mmd, timeStamp);
		if (mmd.getBuiltin)
		{
			this.m_BuiltinItems.Add(item);
		}
		else
		{
			this.m_Items.Add(item);
		}
		this.SortItems();
	}

	private LoadSavePanelBase<T>.ListItem GetItem(int index)
	{
		int count = this.m_BuiltinItems.Count;
		if (index < count)
		{
			return this.m_BuiltinItems[index];
		}
		return this.m_Items[index - count];
	}

	protected string[] GetListingItems()
	{
		int listingCount = this.GetListingCount();
		string[] array = new string[listingCount];
		for (int i = 0; i < listingCount; i++)
		{
			array[i] = this.GetItem(i).text;
		}
		return array;
	}

	protected string GetListingItem(int index)
	{
		return this.GetItem(index).text;
	}

	protected int FindIndexOf(string text)
	{
		int listingCount = this.GetListingCount();
		for (int i = 0; i < listingCount; i++)
		{
			if (string.Compare(this.GetItem(i).asset.get_name(), text, true) == 0)
			{
				return i;
			}
		}
		return -1;
	}

	protected string GetListingName(int index)
	{
		return this.GetItem(index).asset.get_name();
	}

	protected Package.Asset GetListingData(int index)
	{
		return this.GetItem(index).mmd.assetRef;
	}

	protected T GetListingMetaData(int index)
	{
		return this.GetItem(index).mmd;
	}

	protected string GetListingPath(int index)
	{
		return this.GetItem(index).asset.get_package().get_packagePath();
	}

	protected bool IsListingItemFromWorkshop(int index)
	{
		return this.GetItem(index).asset.get_isWorkshopAsset();
	}

	protected string GetListingPackageName(int index)
	{
		return this.GetItem(index).asset.get_package().get_packageName();
	}

	protected int GetListingCount()
	{
		return this.m_BuiltinItems.Count + this.m_Items.Count;
	}

	private void SortItems()
	{
		if (this.SortImpl != null)
		{
			this.m_BuiltinItems.Sort(this.SortImpl);
			this.m_Items.Sort(this.SortImpl);
		}
	}

	private int SortByName(LoadSavePanelBase<T>.ListItem a, LoadSavePanelBase<T>.ListItem b)
	{
		return a.text.CompareTo(b.text);
	}

	private int SortByModified(LoadSavePanelBase<T>.ListItem a, LoadSavePanelBase<T>.ListItem b)
	{
		return b.timeStamp.CompareTo(a.timeStamp);
	}

	private int SortByTheme(LoadSavePanelBase<T>.ListItem a, LoadSavePanelBase<T>.ListItem b)
	{
		return a.mmd.getEnvironment.CompareTo(b.mmd.getEnvironment);
	}

	private int SortByBuildable(LoadSavePanelBase<T>.ListItem a, LoadSavePanelBase<T>.ListItem b)
	{
		return b.mmd.getBuildableArea.CompareTo(a.mmd.getBuildableArea);
	}

	private int SortByPopulation(LoadSavePanelBase<T>.ListItem a, LoadSavePanelBase<T>.ListItem b)
	{
		int value = this.ParsePopulation(a.mmd.getPopulation);
		return this.ParsePopulation(b.mmd.getPopulation).CompareTo(value);
	}

	private int ParsePopulation(string popStr)
	{
		if (!string.IsNullOrEmpty(popStr))
		{
			if (LoadSavePanelBase<T>.<>f__mg$cache0 == null)
			{
				LoadSavePanelBase<T>.<>f__mg$cache0 = new Func<char, bool>(char.IsDigit);
			}
			int result;
			if (int.TryParse(new string(popStr.Where(LoadSavePanelBase<T>.<>f__mg$cache0).ToArray<char>()), out result))
			{
				return result;
			}
		}
		return -1;
	}

	protected void SetSorting(string id)
	{
		string strB = Locale.Get(id);
		int num = this.m_SortBy.get_items().Length;
		for (int i = 0; i < num; i++)
		{
			if (string.Compare(this.m_SortBy.get_items()[i], strB, true) == 0)
			{
				this.SetSorting(i);
				break;
			}
		}
	}

	private void SetSorting(int index)
	{
		this.m_SortBy.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnSortSelectionChanged));
		this.m_SortBy.set_selectedIndex(index);
		this.m_SortBy.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnSortSelectionChanged));
		this.UpdateSorting();
	}

	private void UpdateSorting()
	{
		string selectedValue = this.m_SortBy.get_selectedValue();
		if (!this.m_SortTypeToImplDict.TryGetValue(selectedValue, out this.SortImpl))
		{
			this.SortImpl = null;
		}
	}

	private void RefreshSortTypeToImplDict()
	{
		this.m_SortTypeToImplDict = new Dictionary<string, Comparison<LoadSavePanelBase<T>.ListItem>>();
		this.m_SortTypeToImplDict.Add(Locale.Get("PANEL_SORT_NAME"), new Comparison<LoadSavePanelBase<T>.ListItem>(this.SortByName));
		this.m_SortTypeToImplDict.Add(Locale.Get("PANEL_SORT_MODIFIED"), new Comparison<LoadSavePanelBase<T>.ListItem>(this.SortByModified));
		this.m_SortTypeToImplDict.Add(Locale.Get("PANEL_SORT_THEME"), new Comparison<LoadSavePanelBase<T>.ListItem>(this.SortByTheme));
		this.m_SortTypeToImplDict.Add(Locale.Get("PANEL_SORT_BUILDABLE"), new Comparison<LoadSavePanelBase<T>.ListItem>(this.SortByBuildable));
		this.m_SortTypeToImplDict.Add(Locale.Get("PANEL_SORT_POPULATION"), new Comparison<LoadSavePanelBase<T>.ListItem>(this.SortByPopulation));
	}

	protected void OnSortSelectionChanged(UIComponent comp, int index)
	{
		this.UpdateSorting();
		this.Refresh();
	}

	private List<LoadSavePanelBase<T>.ListItem> m_BuiltinItems;

	private List<LoadSavePanelBase<T>.ListItem> m_Items;

	protected UIPanel m_ThemeOverridePanel;

	protected UIDropDown m_ThemeOverrideDropDown;

	protected UIDropDown m_SortBy;

	private Dictionary<string, Comparison<LoadSavePanelBase<T>.ListItem>> m_SortTypeToImplDict;

	protected Dictionary<string, List<MapThemeMetaData>> m_Themes;

	protected string m_SelectedEnvironment;

	private bool m_ShowMissingThemeWarning;

	private Comparison<LoadSavePanelBase<T>.ListItem> SortImpl;

	[CompilerGenerated]
	private static Func<char, bool> <>f__mg$cache0;

	public struct ListItem
	{
		public ListItem(string aText, Package.Asset aAsset, T aMmd, DateTime aTimeStamp)
		{
			this.text = aText;
			this.asset = aAsset;
			this.mmd = aMmd;
			this.timeStamp = aTimeStamp;
		}

		public string text;

		public Package.Asset asset;

		public T mmd;

		public DateTime timeStamp;
	}
}
