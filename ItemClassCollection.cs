﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using UnityEngine;

public class ItemClassCollection : MonoBehaviour
{
	private void Awake()
	{
		ItemClassCollection.InitializeClasses(this.m_classes);
	}

	private void OnDestroy()
	{
		ItemClassCollection.DestroyClasses(this.m_classes);
	}

	public static ItemClass FindClass(string name)
	{
		ItemClass result;
		if (ItemClassCollection.m_classDict.TryGetValue(name, out result))
		{
			return result;
		}
		CODebugBase<LogChannel>.Warn(LogChannel.Serialization, "Unknown class: " + name);
		return null;
	}

	public static void InitializeClasses(ItemClass[] classes)
	{
		for (int i = 0; i < classes.Length; i++)
		{
			ItemClass itemClass = classes[i];
			if (ItemClassCollection.m_classDict.ContainsKey(itemClass.get_name()))
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Duplicate class name: " + itemClass.get_name());
			}
			else
			{
				ItemClassCollection.m_classDict.Add(itemClass.get_name(), itemClass);
			}
		}
	}

	public static void DestroyClasses(ItemClass[] classes)
	{
		for (int i = 0; i < classes.Length; i++)
		{
			ItemClass itemClass = classes[i];
			ItemClassCollection.m_classDict.Remove(itemClass.get_name());
		}
	}

	public ItemClass[] m_classes;

	private static Dictionary<string, ItemClass> m_classDict = new Dictionary<string, ItemClass>();
}
