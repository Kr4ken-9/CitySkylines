﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public sealed class MeteorWorldInfoPanel : WorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_type = base.Find<UILabel>("Type");
		this.m_speed = base.Find<UILabel>("Speed");
		this.m_targetCity = base.Find<UILabel>("TargetCity");
		this.m_NameField = base.Find<UITextField>("VehicleName");
		this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
	}

	protected override void OnSetTarget()
	{
		this.m_NameField.set_text(this.GetName());
		base.OnSetTarget();
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		this.m_type.set_text(Locale.Get("VEHICLE_TITLE", "Meteor"));
		this.m_targetCity.set_text(Locale.Get("VEHICLE_STATUS_TAXI_HEADING") + " " + Singleton<SimulationManager>.get_instance().m_metaData.m_CityName);
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			Vehicle vehicle2 = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle];
			string text = StringUtils.SafeFormat(Locale.Get("METEORINFOVIEWPANEL_SPEED") + ": {0} km/h", Mathf.RoundToInt(vehicle2.GetSmoothVelocity(vehicle).get_magnitude() * 3.6f));
			this.m_speed.set_text(text);
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		MeteorWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new MeteorWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		ushort num = 0;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(this.m_InstanceID);
		if (group != null)
		{
			num = group.m_ownerInstance.Disaster;
		}
		if (num != 0)
		{
			return Singleton<DisasterManager>.get_instance().GetDisasterName(num);
		}
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				return Singleton<VehicleManager>.get_instance().GetVehicleName(firstVehicle);
			}
		}
		else if (this.m_InstanceID.Type == InstanceType.ParkedVehicle && this.m_InstanceID.ParkedVehicle != 0)
		{
			ushort parkedVehicle = this.m_InstanceID.ParkedVehicle;
			return Singleton<VehicleManager>.get_instance().GetParkedVehicleName(parkedVehicle);
		}
		return string.Empty;
	}

	private UILabel m_type;

	private UILabel m_targetCity;

	private UILabel m_speed;

	private UITextField m_NameField;
}
