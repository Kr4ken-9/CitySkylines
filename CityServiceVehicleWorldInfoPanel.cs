﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class CityServiceVehicleWorldInfoPanel : VehicleWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Owner = base.Find<UIButton>("Owner");
		this.m_Owner.add_eventClick(new MouseEventHandler(base.OnTargetClick));
		this.m_Load = base.Find<UILabel>("Load");
		this.m_LoadInfo = base.Find<UILabel>("LoadInfo");
		this.m_LoadBar = base.Find<UIProgressBar>("LoadBar");
		this.m_VehicleType = base.Find<UISprite>("VehicleType");
		this.m_VehicleType.add_eventSpriteNameChanged(new PropertyChangedEventHandler<string>(this.IconChanged));
	}

	private void IconChanged(UIComponent comp, string text)
	{
		UITextureAtlas.SpriteInfo spriteInfo = this.m_VehicleType.get_atlas().get_Item(text);
		if (spriteInfo != null)
		{
			this.m_VehicleType.set_size(spriteInfo.get_pixelSize());
		}
	}

	private string GetVehicleIcon(ItemClass.Service service, ItemClass.SubService subService, VehicleInfo.VehicleType type)
	{
		if (service == ItemClass.Service.None)
		{
			return CityServiceVehicleWorldInfoPanel.GetVehicleTypeIcon(type);
		}
		if (service == ItemClass.Service.Disaster)
		{
			service = ItemClass.Service.FireDepartment;
		}
		if (service == ItemClass.Service.PublicTransport)
		{
			return PublicTransportVehicleWorldInfoPanel.GetVehicleTypeIcon(type);
		}
		string nameByValue = Utils.GetNameByValue<ItemClass.Service>(service, "Game");
		string text = "ToolbarIcon" + nameByValue;
		UITextureAtlas.SpriteInfo spriteInfo = this.m_VehicleType.get_atlas().get_Item(text);
		if (spriteInfo != null)
		{
			return text;
		}
		return CityServiceVehicleWorldInfoPanel.GetVehicleTypeIcon(type);
	}

	private static string GetVehicleTypeIcon(VehicleInfo.VehicleType type)
	{
		if (type != VehicleInfo.VehicleType.Ship)
		{
			return "IconServiceVehicle";
		}
		return "IconCargoShip";
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			InstanceID instanceID = this.m_InstanceID;
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				instanceID.Vehicle = firstVehicle;
				VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].Info;
				this.m_VehicleType.set_spriteName(this.GetVehicleIcon(info.GetService(), info.GetSubService(), info.m_vehicleType));
				InstanceID ownerID = info.m_vehicleAI.GetOwnerID(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle]);
				if (ownerID.Building != 0)
				{
					this.m_Owner.set_objectUserData(ownerID);
					this.m_Owner.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(ownerID.Building, instanceID));
					this.m_Owner.set_isEnabled((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)ownerID.Building].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.None);
				}
				else
				{
					this.m_Owner.set_objectUserData(ownerID);
					this.m_Owner.set_text(string.Empty);
					this.m_Owner.set_isEnabled(false);
				}
				base.ShortenTextToFitParent(this.m_Owner);
				if (this.m_Load != null || this.m_LoadInfo != null || this.m_LoadBar != null)
				{
					string text;
					int num;
					int num2;
					info.m_vehicleAI.GetBufferStatus(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle], out text, out num, out num2);
					if (num2 > 0)
					{
						int percentage = base.GetPercentage(num, num2);
						if (this.m_Load != null)
						{
							this.m_Load.get_parent().set_isVisible(true);
							this.m_Load.set_text(Locale.Get("VEHICLE_BUFFER", text));
						}
						if (this.m_LoadInfo != null)
						{
							this.m_LoadInfo.get_parent().set_isVisible(true);
							this.m_LoadInfo.set_text(StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), percentage));
						}
						if (this.m_LoadBar != null)
						{
							this.m_LoadBar.get_parent().set_isVisible(true);
							this.m_LoadBar.set_maxValue((float)num2);
							this.m_LoadBar.set_value((float)num);
						}
					}
					else
					{
						if (this.m_Load != null)
						{
							this.m_Load.get_parent().set_isVisible(false);
						}
						if (this.m_LoadInfo != null)
						{
							this.m_LoadInfo.get_parent().set_isVisible(false);
						}
						if (this.m_LoadBar != null)
						{
							this.m_LoadBar.get_parent().set_isVisible(false);
						}
					}
				}
			}
		}
	}

	private UIButton m_Owner;

	private UILabel m_Load;

	private UILabel m_LoadInfo;

	private UIProgressBar m_LoadBar;

	private UISprite m_VehicleType;
}
