﻿using System;
using ColossalFramework;
using UnityEngine;

public class MonorailPylonAI : BuildingAI
{
	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		if (testOnly)
		{
			return false;
		}
		if ((data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			data.m_flags |= Building.Flags.Collapsed;
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
			Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, Building.Flags.Collapsed);
			return true;
		}
		return false;
	}

	protected override bool CalculatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None && base.CalculatePropGroupData(buildingID, ref buildingData, props, trees, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
	}

	protected override void PopulatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			base.PopulatePropGroupData(buildingID, ref buildingData, props, trees, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
	}
}
