﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public struct DistrictPopulationData
{
	public void Add(ref DistrictPopulationData data)
	{
		this.m_tempCount += data.m_tempCount;
	}

	public void Update()
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u;
		short num2 = (short)Mathf.Clamp((int)(this.m_tempCount - this.m_finalCount), -32767, 32767);
		this.m_finalCount = this.m_tempCount;
		switch (num)
		{
		case 0u:
			this.m_delta0 = num2;
			break;
		case 1u:
			this.m_delta1 = num2;
			break;
		case 2u:
			this.m_delta2 = num2;
			break;
		case 3u:
			this.m_delta3 = num2;
			break;
		case 4u:
			this.m_delta4 = num2;
			break;
		case 5u:
			this.m_delta5 = num2;
			break;
		case 6u:
			this.m_delta6 = num2;
			break;
		case 7u:
			this.m_delta7 = num2;
			break;
		case 8u:
			this.m_delta8 = num2;
			break;
		case 9u:
			this.m_delta9 = num2;
			break;
		case 10u:
			this.m_delta10 = num2;
			break;
		case 11u:
			this.m_delta11 = num2;
			break;
		case 12u:
			this.m_delta12 = num2;
			break;
		case 13u:
			this.m_delta13 = num2;
			break;
		case 14u:
			this.m_delta14 = num2;
			break;
		case 15u:
			this.m_delta15 = num2;
			break;
		}
	}

	public void Reset()
	{
		this.m_tempCount = 0u;
	}

	public int GetWeeklyDelta()
	{
		return (int)(this.m_delta0 + this.m_delta1 + this.m_delta2 + this.m_delta3 + this.m_delta4 + this.m_delta5 + this.m_delta6 + this.m_delta7 + this.m_delta8 + this.m_delta9 + this.m_delta10 + this.m_delta11 + this.m_delta12 + this.m_delta13 + this.m_delta14 + this.m_delta15);
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempCount);
		s.WriteUInt24(this.m_finalCount);
		s.WriteInt16((int)this.m_delta0);
		s.WriteInt16((int)this.m_delta1);
		s.WriteInt16((int)this.m_delta2);
		s.WriteInt16((int)this.m_delta3);
		s.WriteInt16((int)this.m_delta4);
		s.WriteInt16((int)this.m_delta5);
		s.WriteInt16((int)this.m_delta6);
		s.WriteInt16((int)this.m_delta7);
		s.WriteInt16((int)this.m_delta8);
		s.WriteInt16((int)this.m_delta9);
		s.WriteInt16((int)this.m_delta10);
		s.WriteInt16((int)this.m_delta11);
		s.WriteInt16((int)this.m_delta12);
		s.WriteInt16((int)this.m_delta13);
		s.WriteInt16((int)this.m_delta14);
		s.WriteInt16((int)this.m_delta15);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempCount = s.ReadUInt24();
		this.m_finalCount = s.ReadUInt24();
		if (s.get_version() >= 158u)
		{
			this.m_delta0 = (short)s.ReadInt16();
			this.m_delta1 = (short)s.ReadInt16();
			this.m_delta2 = (short)s.ReadInt16();
			this.m_delta3 = (short)s.ReadInt16();
			this.m_delta4 = (short)s.ReadInt16();
			this.m_delta5 = (short)s.ReadInt16();
			this.m_delta6 = (short)s.ReadInt16();
			this.m_delta7 = (short)s.ReadInt16();
			this.m_delta8 = (short)s.ReadInt16();
			this.m_delta9 = (short)s.ReadInt16();
			this.m_delta10 = (short)s.ReadInt16();
			this.m_delta11 = (short)s.ReadInt16();
			this.m_delta12 = (short)s.ReadInt16();
			this.m_delta13 = (short)s.ReadInt16();
			this.m_delta14 = (short)s.ReadInt16();
			this.m_delta15 = (short)s.ReadInt16();
		}
		else
		{
			this.m_delta0 = 0;
			this.m_delta1 = 0;
			this.m_delta2 = 0;
			this.m_delta3 = 0;
			this.m_delta4 = 0;
			this.m_delta5 = 0;
			this.m_delta6 = 0;
			this.m_delta7 = 0;
			this.m_delta8 = 0;
			this.m_delta9 = 0;
			this.m_delta10 = 0;
			this.m_delta11 = 0;
			this.m_delta12 = 0;
			this.m_delta13 = 0;
			this.m_delta14 = 0;
			this.m_delta15 = 0;
		}
	}

	public uint m_tempCount;

	public uint m_finalCount;

	public short m_delta0;

	public short m_delta1;

	public short m_delta2;

	public short m_delta3;

	public short m_delta4;

	public short m_delta5;

	public short m_delta6;

	public short m_delta7;

	public short m_delta8;

	public short m_delta9;

	public short m_delta10;

	public short m_delta11;

	public short m_delta12;

	public short m_delta13;

	public short m_delta14;

	public short m_delta15;
}
