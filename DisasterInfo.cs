﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class DisasterInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_disasterAI == null)
		{
			this.m_disasterAI = base.GetComponent<DisasterAI>();
			this.m_disasterAI.m_info = this;
			this.m_disasterAI.InitializeAI();
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_disasterAI != null)
		{
			this.m_disasterAI.ReleaseAI();
			this.m_disasterAI = null;
		}
		base.DestroyPrefab();
	}

	public override string GetLocalizedTitle()
	{
		if (this.m_baseInfo != null)
		{
			return Locale.Get("DISASTER_TITLE", this.m_baseInfo.get_gameObject().get_name());
		}
		return Locale.Get("DISASTER_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		if (this.m_baseInfo != null)
		{
			return new GeneratedString.Locale("DISASTER_TITLE", this.m_baseInfo.get_gameObject().get_name());
		}
		return new GeneratedString.Locale("DISASTER_TITLE", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("DISASTER_DESC", base.get_gameObject().get_name());
	}

	public void CheckUnlocking()
	{
		MilestoneInfo unlockMilestone = this.m_UnlockMilestone;
		if (unlockMilestone != null)
		{
			Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
		}
	}

	public override PrefabAI GetAI()
	{
		return this.m_disasterAI;
	}

	public MilestoneInfo m_UnlockMilestone;

	public ItemClass.Placement m_placementStyle;

	public Color m_markerColor = new Color(1f, 0f, 0.5f, 0.25f);

	public Mesh m_markerMesh;

	public int m_randomProbability;

	public int m_cooldownFrames = 256;

	public UITextureAtlas m_IconAtlas;

	[UISprite("m_IconAtlas")]
	public string m_Icon;

	public DisasterInfo m_baseInfo;

	public RadioContentInfo m_warningBroadcast;

	public RadioContentInfo m_activeBroadcast;

	[NonSerialized]
	public DisasterAI m_disasterAI;

	[NonSerialized]
	public uint m_lastSpawnFrame;

	[NonSerialized]
	public DisasterTypeGuide m_disasterWarningGuide;

	[NonSerialized]
	public int m_finalRandomProbability;
}
