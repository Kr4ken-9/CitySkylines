﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class LandfillSiteAI : PlayerBuildingAI
{
	public override void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		resource1 = NaturalResourceManager.Resource.Pollution;
		position1 = data.m_position;
		radius1 = this.m_pollutionRadius;
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.NoisePollution:
		{
			int noiseAccumulation = this.m_noiseAccumulation;
			return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
		}
		case InfoManager.InfoMode.Transport:
			IL_14:
			switch (infoMode)
			{
			case InfoManager.InfoMode.Connections:
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			case InfoManager.InfoMode.Traffic:
			case InfoManager.InfoMode.Wind:
			{
				IL_2D:
				if (infoMode != InfoManager.InfoMode.Electricity)
				{
					return base.GetColor(buildingID, ref data, infoMode);
				}
				if (this.m_electricityProduction == 0)
				{
					return base.GetColor(buildingID, ref data, infoMode);
				}
				if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
				{
					Color activeColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
					activeColor.a = 0f;
					return activeColor;
				}
				Color inactiveColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
				inactiveColor.a = 0f;
				return inactiveColor;
			}
			case InfoManager.InfoMode.Garbage:
				if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
			}
			goto IL_2D;
		case InfoManager.InfoMode.Pollution:
		{
			int pollutionAccumulation = this.m_pollutionAccumulation;
			return ColorUtils.LinearLerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor, Mathf.Clamp01((float)pollutionAccumulation * 0.04f));
		}
		}
		goto IL_14;
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		if (resource == NaturalResourceManager.Resource.Pollution)
		{
			int num;
			if (this.m_electricityProduction == 0 && this.m_materialProduction == 0)
			{
				num = (int)(data.m_customBuffer1 / 100);
				num = (num * this.m_pollutionAccumulation + 99) / 100;
			}
			else
			{
				int num2 = (int)data.m_productionRate;
				if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
				{
					num2 = 0;
				}
				int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
				num2 = PlayerBuildingAI.GetProductionRate(num2, budget);
				int num3 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
				num2 = Mathf.Min(num2, num3 / (this.m_garbageCapacity / 200));
				num = (num2 * this.m_pollutionAccumulation + 99) / 100;
			}
			return num;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		if (this.m_electricityProduction != 0)
		{
			int num = (int)data.m_productionRate;
			if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				num = 0;
			}
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			num = PlayerBuildingAI.GetProductionRate(num, budget);
			int num2 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
			num = Mathf.Min(num, num2 / (this.m_garbageCapacity / 200));
			return (num * this.m_electricityProduction + 99) / 100;
		}
		return base.GetElectricityRate(buildingID, ref data);
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		int num2 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		num = Mathf.Min(num, num2 / (this.m_garbageCapacity / 200));
		int num3 = this.m_garbageConsumption;
		int electricityProduction = this.m_electricityProduction;
		int materialProduction = this.m_materialProduction;
		if (materialProduction != 0)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
			if ((servicePolicies & DistrictPolicies.Services.RecyclePlastic) != DistrictPolicies.Services.None)
			{
				int num4 = Mathf.Max(1, materialProduction + electricityProduction);
				num4 = (materialProduction * 20 + (num4 >> 1)) / num4;
				num3 = (num3 * (100 + num4) + 50) / 100;
			}
		}
		return -((num * num3 + 99) / 100);
	}

	public override int GetGarbageAmount(ushort buildingID, ref Building data)
	{
		return Mathf.Min(this.m_garbageCapacity, (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer));
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Garbage, ref num, ref num2, ref num3, ref num4);
		int num5 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		if (this.m_materialProduction != 0)
		{
			return StringUtils.SafeFormat("{0}\nGarbage: {1} (+{2})\nMaterials: {3}", new object[]
			{
				base.GetDebugString(buildingID, ref data),
				num5,
				num2,
				data.m_customBuffer2
			});
		}
		return StringUtils.SafeFormat("{0}\nGarbage: {1} (+{2})", new object[]
		{
			base.GetDebugString(buildingID, ref data),
			num5,
			num2
		});
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Garbage;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.SetPrefab(this.m_info);
		}
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data))
		{
			return Locale.Get("BUILDING_STATUS_FULL");
		}
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EMPTYING");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data) && this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.Relock();
		}
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
		data.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
		this.CheckCapacity(buildingID, ref data);
		this.SetEmptying(buildingID, ref data, (data.m_flags & Building.Flags.Downgrading) != Building.Flags.None);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainGarbage, position, 1.5f);
		if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Sad, NaturalResourceManager.Resource.Pollution, (float)this.m_pollutionAccumulation, this.m_pollutionRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseGarbage, position, 1.5f);
			if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Happy, NaturalResourceManager.Resource.Pollution, (float)(-(float)this.m_pollutionAccumulation), this.m_pollutionRadius);
			}
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		if (this.CountAnimals(buildingID, ref buildingData) < this.m_animalCount)
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			if (this.IsFull(buildingID, ref buildingData))
			{
				Singleton<BuildingManager>.get_instance().m_landfillSiteFull.Activate(properties.m_landfillSiteFull, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_landfillSiteFull.Deactivate(buildingID, false);
			}
			if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None && this.CanBeRelocated(buildingID, ref buildingData))
			{
				Singleton<BuildingManager>.get_instance().m_landfillSiteEmpty.Activate(properties.m_landfillSiteEmpty, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_landfillSiteEmpty.Deactivate(buildingID, false);
			}
		}
		if (buildingData.m_customBuffer1 >= 100 && this.m_electricityProduction == 0 && this.m_materialProduction == 0)
		{
			int num = (int)(buildingData.m_customBuffer1 / 100);
			num = (num * this.m_pollutionAccumulation + 99) / 100;
			if (num != 0)
			{
				Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num, num, buildingData.m_position, this.m_pollutionRadius);
			}
		}
		if (this.m_garbageConsumption == 0)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			int num2 = Mathf.Min(this.m_garbageCapacity, (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer));
			District[] expr_169_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_169_cp_0_cp_1 = district;
			expr_169_cp_0_cp_0[(int)expr_169_cp_0_cp_1].m_productionData.m_tempGarbageAmount = expr_169_cp_0_cp_0[(int)expr_169_cp_0_cp_1].m_productionData.m_tempGarbageAmount + (uint)num2;
			District[] expr_18D_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_18D_cp_0_cp_1 = district;
			expr_18D_cp_0_cp_0[(int)expr_18D_cp_0_cp_1].m_productionData.m_tempGarbageCapacity = expr_18D_cp_0_cp_0[(int)expr_18D_cp_0_cp_1].m_productionData.m_tempGarbageCapacity + (uint)this.m_garbageCapacity;
		}
		this.CheckCapacity(buildingID, ref buildingData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempExport = 0;
		}
	}

	public override ToolBase.ToolErrors CheckBulldozing(ushort buildingID, ref Building data)
	{
		int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
		if (this.m_garbageConsumption == 0 && num != 0 && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			return ToolBase.ToolErrors.NotEmpty;
		}
		return base.CheckBulldozing(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		ItemClass.SubService subService = ItemClass.SubService.None;
		if (material != TransferManager.TransferReason.Lumber)
		{
			if (material != TransferManager.TransferReason.GarbageMove)
			{
				if (material != TransferManager.TransferReason.Garbage)
				{
					if (material != TransferManager.TransferReason.Coal)
					{
						if (material != TransferManager.TransferReason.Petrol)
						{
							base.StartTransfer(buildingID, ref data, material, offer);
						}
						else
						{
							subService = ItemClass.SubService.IndustrialOil;
						}
					}
					else
					{
						subService = ItemClass.SubService.IndustrialOre;
					}
				}
				else
				{
					VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
					if (randomVehicleInfo != null)
					{
						Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
						ushort num;
						if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
						{
							randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
							randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
						}
					}
				}
			}
			else
			{
				VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				if (randomVehicleInfo2 != null)
				{
					Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
					ushort num2;
					if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, data.m_position, material, false, true))
					{
						randomVehicleInfo2.m_vehicleAI.SetSource(num2, ref vehicles2.m_buffer[(int)num2], buildingID);
						randomVehicleInfo2.m_vehicleAI.StartTransfer(num2, ref vehicles2.m_buffer[(int)num2], material, offer);
					}
				}
			}
		}
		else
		{
			subService = ItemClass.SubService.IndustrialForestry;
		}
		if (subService != ItemClass.SubService.None)
		{
			VehicleInfo randomVehicleInfo3 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, ItemClass.Service.Industrial, subService, ItemClass.Level.Level1);
			if (randomVehicleInfo3 != null)
			{
				Array16<Vehicle> vehicles3 = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num3;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num3, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo3, data.m_position, material, false, true))
				{
					randomVehicleInfo3.m_vehicleAI.SetSource(num3, ref vehicles3.m_buffer[(int)num3], buildingID);
					randomVehicleInfo3.m_vehicleAI.StartTransfer(num3, ref vehicles3.m_buffer[(int)num3], material, offer);
					ushort building = offer.Building;
					if (building != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
					{
						int amount;
						int num4;
						randomVehicleInfo3.m_vehicleAI.GetSize(num3, ref vehicles3.m_buffer[(int)num3], out amount, out num4);
						CommonBuildingAI.ExportResource(buildingID, ref data, material, amount);
					}
					data.m_outgoingProblemTimer = 0;
				}
			}
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material != TransferManager.TransferReason.Lumber)
		{
			if (material == TransferManager.TransferReason.GarbageMove)
			{
				bool flag = this.IsFull(buildingID, ref data);
				int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
				amountDelta = Mathf.Clamp(amountDelta, -num, this.m_garbageCapacity - num);
				num += amountDelta;
				data.m_customBuffer1 = (ushort)(num / 1000);
				data.m_garbageBuffer = (ushort)(num - (int)(data.m_customBuffer1 * 1000));
				bool flag2 = this.IsFull(buildingID, ref data);
				if (flag != flag2)
				{
					if (flag2)
					{
						if (this.m_fullPassMilestone != null)
						{
							this.m_fullPassMilestone.Unlock();
						}
					}
					else if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Relock();
					}
				}
				return;
			}
			if (material == TransferManager.TransferReason.Garbage)
			{
				bool flag3 = this.IsFull(buildingID, ref data);
				int num2 = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
				amountDelta = Mathf.Clamp(amountDelta, 0, this.m_garbageCapacity - num2);
				num2 += amountDelta;
				data.m_customBuffer1 = (ushort)(num2 / 1000);
				data.m_garbageBuffer = (ushort)(num2 - (int)(data.m_customBuffer1 * 1000));
				bool flag4 = this.IsFull(buildingID, ref data);
				if (flag3 != flag4)
				{
					if (flag4)
					{
						if (this.m_fullPassMilestone != null)
						{
							this.m_fullPassMilestone.Unlock();
						}
					}
					else if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Relock();
					}
				}
				return;
			}
			if (material != TransferManager.TransferReason.Coal && material != TransferManager.TransferReason.Petrol)
			{
				base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
				return;
			}
		}
		int customBuffer = (int)data.m_customBuffer2;
		amountDelta = Mathf.Clamp(amountDelta, -customBuffer, 0);
		data.m_customBuffer2 = (ushort)(customBuffer + amountDelta);
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Garbage)
		{
			amount = 0;
			max = 1000000;
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override float ElectricityGridRadius()
	{
		if (this.m_electricityProduction != 0)
		{
			return 64f;
		}
		return base.ElectricityGridRadius();
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None || data.m_fireIntensity != 0)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_collectRadius * 0.01f;
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Garbage, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.GarbageMove, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.GarbageMove, offer);
		if (this.m_materialProduction != 0)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Coal, offer);
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Lumber, offer);
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Petrol, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Notification.Problem problem = buildingData.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			if (buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		buildingData.m_problems = problem;
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
			bool flag = this.IsFull(buildingID, ref buildingData);
			int num = finalProductionRate;
			int num2 = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer);
			int num3 = this.m_garbageConsumption;
			int num4 = this.m_electricityProduction;
			int num5 = this.m_materialProduction;
			int num6 = finalProductionRate;
			if ((servicePolicies & DistrictPolicies.Services.RecyclePlastic) != DistrictPolicies.Services.None && num5 != 0)
			{
				District[] expr_93_cp_0 = instance.m_districts.m_buffer;
				byte expr_93_cp_1 = district;
				expr_93_cp_0[(int)expr_93_cp_1].m_servicePoliciesEffect = (expr_93_cp_0[(int)expr_93_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.RecyclePlastic);
				int num7 = Mathf.Max(1, num5 + num4);
				num7 = (num5 * 20 + (num7 >> 1)) / num7;
				num3 = (num3 * (100 + num7) + 50) / 100;
				num5 = (num5 * 120 + 50) / 100;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 625, this.m_info.m_class);
			}
			if (num3 != 0)
			{
				District[] expr_117_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_117_cp_0_cp_1 = district;
				expr_117_cp_0_cp_0[(int)expr_117_cp_0_cp_1].m_productionData.m_tempIncinerationCapacity = expr_117_cp_0_cp_0[(int)expr_117_cp_0_cp_1].m_productionData.m_tempIncinerationCapacity + (uint)((finalProductionRate * num3 + 99) / 100);
				num6 = Mathf.Min(finalProductionRate, num2 / (this.m_garbageCapacity / 200));
			}
			num3 = (num6 * num3 + 99) / 100;
			num4 = (num6 * num4 + 99) / 100;
			num5 = (num6 * num5 + 99) / 100;
			bool flag2 = false;
			if (num5 != 0 && num2 >= num3)
			{
				int num8 = num4;
				int num9 = (num6 * this.m_pollutionAccumulation + 99) / 100;
				int num10 = this.MaxOutgoingLoadSize();
				int num11 = num10 * 4;
				int num12 = num11 - (int)buildingData.m_customBuffer2;
				if (num12 < num5)
				{
					num3 = (num3 * num12 + (num5 >> 1)) / num5;
					num8 = (num8 * num12 + (num5 >> 1)) / num5;
					num9 = (num9 * num12 + (num5 >> 1)) / num5;
					num5 = num12;
				}
				if (num3 > 0)
				{
					num2 -= num3;
					buildingData.m_customBuffer1 = (ushort)(num2 / 1000);
					buildingData.m_garbageBuffer = (ushort)(num2 - (int)(buildingData.m_customBuffer1 * 1000));
				}
				if (num8 > 0)
				{
					num12 = num4 * 2 - (int)buildingData.m_electricityBuffer;
					if (num12 > 0)
					{
						int num13 = Mathf.Min(num8, num12);
						buildingData.m_electricityBuffer += (ushort)num13;
					}
				}
				if (num5 > 0)
				{
					buildingData.m_customBuffer2 += (ushort)num5;
				}
				if (num9 != 0)
				{
					Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num9, num9, buildingData.m_position, this.m_pollutionRadius);
				}
			}
			else if (num4 != 0 && num2 >= num3)
			{
				num2 -= num3;
				buildingData.m_customBuffer1 = (ushort)(num2 / 1000);
				buildingData.m_garbageBuffer = (ushort)(num2 - (int)(buildingData.m_customBuffer1 * 1000));
				int num14 = num4 * 2 - (int)buildingData.m_electricityBuffer;
				if (num14 > 0)
				{
					int num15 = Mathf.Min(num4, num14);
					buildingData.m_electricityBuffer += (ushort)num15;
				}
				int num16 = (num6 * this.m_pollutionAccumulation + 99) / 100;
				if (num16 != 0)
				{
					Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num16, num16, buildingData.m_position, this.m_pollutionRadius);
				}
			}
			else if (this.m_electricityProduction != 0 || this.m_materialProduction != 0)
			{
				flag2 = true;
			}
			if (num4 != 0)
			{
				int num17 = Mathf.Min((int)buildingData.m_electricityBuffer, num4);
				if (num17 > 0)
				{
					int num18 = Singleton<ElectricityManager>.get_instance().TryDumpElectricity(buildingData.m_position, num4, num17);
					buildingData.m_electricityBuffer -= (ushort)num18;
				}
				District[] expr_3B2_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_3B2_cp_0_cp_1 = district;
				expr_3B2_cp_0_cp_0[(int)expr_3B2_cp_0_cp_1].m_productionData.m_tempElectricityCapacity = expr_3B2_cp_0_cp_0[(int)expr_3B2_cp_0_cp_1].m_productionData.m_tempElectricityCapacity + (uint)num4;
				if (this.m_pollutionAccumulation != 0)
				{
					District[] expr_3E1_cp_0_cp_0 = instance.m_districts.m_buffer;
					byte expr_3E1_cp_0_cp_1 = district;
					expr_3E1_cp_0_cp_0[(int)expr_3E1_cp_0_cp_1].m_productionData.m_tempPollutingElectricity = expr_3E1_cp_0_cp_0[(int)expr_3E1_cp_0_cp_1].m_productionData.m_tempPollutingElectricity + (uint)num4;
				}
			}
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
			int num19 = (num * this.m_garbageTruckCount + 99) / 100;
			if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
			{
				int num20 = 0;
				int num21 = 0;
				int num22 = 0;
				int num23 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.GarbageMove, ref num20, ref num21, ref num22, ref num23);
				if (num2 > 0 && num20 < num19)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.GarbageMove, offer);
				}
			}
			else
			{
				int num24 = 0;
				int num25 = 0;
				int num26 = 0;
				int num27 = 0;
				int num28 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Garbage, ref num24, ref num26, ref num27, ref num28);
				base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.GarbageMove, ref num25, ref num26, ref num27, ref num28);
				int num29 = this.m_garbageCapacity - num2;
				if (this.m_garbageConsumption == 0)
				{
					num29 -= num27;
				}
				int num30 = (this.m_garbageConsumption != 0) ? 20000 : 1;
				int num31 = num29 / 20000;
				bool flag3 = num29 >= num30 && (num31 > 1 || num24 >= num19 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0);
				bool flag4 = num29 >= num30 && num24 < num19 && (num31 > 1 || !flag3);
				if (flag3)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					if (this.m_garbageConsumption != 0)
					{
						offer2.Priority = Mathf.Max(1, num29 * 6 / this.m_garbageCapacity);
					}
					else
					{
						offer2.Priority = 0;
					}
					offer2.Building = buildingID;
					offer2.Position = buildingData.m_position;
					offer2.Amount = Mathf.Max(1, num31 - 1);
					offer2.Active = false;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.GarbageMove, offer2);
				}
				if (flag4)
				{
					TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
					offer3.Priority = 2 - num24;
					offer3.Building = buildingID;
					offer3.Position = buildingData.m_position;
					offer3.Amount = 1;
					offer3.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Garbage, offer3);
				}
				if (this.m_materialProduction != 0)
				{
					int num32 = 0;
					int num33 = 0;
					int num34 = 0;
					int num35 = 0;
					this.CalculateRawMaterialVehicles(buildingID, ref buildingData, ref num32, ref num33, ref num34, ref num35);
					buildingData.m_tempExport = (byte)Mathf.Clamp(num35, (int)buildingData.m_tempExport, 255);
					int num36 = this.MaxOutgoingLoadSize();
					int num37 = (num * this.m_garbageTruckCount + 99) / 100;
					int customBuffer = (int)buildingData.m_customBuffer2;
					if (customBuffer >= num36 && num32 < num37)
					{
						TransferManager.TransferOffer offer4 = default(TransferManager.TransferOffer);
						offer4.Priority = customBuffer * 8 / num36;
						offer4.Building = buildingID;
						offer4.Position = buildingData.m_position;
						offer4.Amount = Mathf.Min(customBuffer / num36, num37 - num32);
						offer4.Active = true;
						int num38 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u);
						if (num38 != 0)
						{
							if (num38 != 1)
							{
								if (num38 == 2)
								{
									Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Petrol, offer4);
								}
							}
							else
							{
								Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Lumber, offer4);
							}
						}
						else
						{
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Coal, offer4);
						}
					}
				}
				if (flag2 && num24 == 0)
				{
					finalProductionRate = 0;
				}
			}
			bool flag5 = this.IsFull(buildingID, ref buildingData);
			if (flag != flag5)
			{
				if (flag5)
				{
					if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Unlock();
					}
				}
				else if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Relock();
				}
			}
			int num39 = finalProductionRate * this.m_noiseAccumulation / 100;
			if (num39 != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num39, buildingData.m_position, this.m_noiseRadius);
			}
		}
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
	}

	protected void CalculateRawMaterialVehicles(ushort buildingID, ref Building data, ref int count, ref int cargo, ref int capacity, ref int outside)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			TransferManager.TransferReason transferType = (TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num].m_transferType;
			if (transferType == TransferManager.TransferReason.Coal || transferType == TransferManager.TransferReason.Lumber || transferType == TransferManager.TransferReason.Petrol)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				int num3;
				int num4;
				info.m_vehicleAI.GetSize(num, ref instance.m_vehicles.m_buffer[(int)num], out num3, out num4);
				cargo += Mathf.Min(num3, num4);
				capacity += num4;
				count++;
				if ((instance.m_vehicles.m_buffer[(int)num].m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
				{
					outside++;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	private void CheckCapacity(ushort buildingID, ref Building buildingData)
	{
		if (this.m_electricityProduction == 0 && this.m_materialProduction == 0)
		{
			int num = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_garbageBuffer);
			if (num >= this.m_garbageCapacity)
			{
				if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityFull)
				{
					buildingData.m_flags |= Building.Flags.CapacityFull;
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
					Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				}
			}
			else if (num >= this.m_garbageCapacity >> 1)
			{
				if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityStep1)
				{
					buildingData.m_flags = ((buildingData.m_flags & ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2)) | Building.Flags.CapacityStep1);
					buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
					Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				}
			}
			else if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.None)
			{
				buildingData.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
				Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			}
		}
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num3);
			target.z += Mathf.Sin(num3);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = this.m_info.m_size.y + (float)randomizer.Int32(1000u) * 0.1f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		ushort num;
		if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num2++;
			}
			num = nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	public override void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
		Notification.Problem problem = data.m_problems;
		Notification.Problem problems = data.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if (this.m_garbageConsumption == 0 && emptying)
		{
			data.m_flags |= Building.Flags.Downgrading;
			if (data.m_customBuffer1 * 1000 + data.m_garbageBuffer == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		else
		{
			data.m_flags &= ~Building.Flags.Downgrading;
		}
		data.m_problems = problem;
		if (problem != problems)
		{
			Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, problem);
			Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = this.m_pollutionAccumulation;
		noise = this.m_noiseAccumulation;
	}

	public override bool IsFull(ushort buildingID, ref Building data)
	{
		if (this.m_electricityProduction == 0 && this.m_materialProduction == 0)
		{
			int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
			return num >= this.m_garbageCapacity;
		}
		return false;
	}

	public override bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		if (this.m_electricityProduction == 0 && this.m_materialProduction == 0)
		{
			int num = (int)(data.m_customBuffer1 * 1000 + data.m_garbageBuffer);
			return num == 0;
		}
		return true;
	}

	public override bool CanBeEmptied()
	{
		return this.m_electricityProduction == 0 && this.m_materialProduction == 0;
	}

	public override bool CanBeEmptied(ushort buildingID, ref Building data)
	{
		if (this.m_electricityProduction == 0 && this.m_materialProduction == 0)
		{
			if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
			{
				return true;
			}
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
			int size = serviceBuildings.m_size;
			ushort[] buffer = serviceBuildings.m_buffer;
			if (buffer != null && size <= buffer.Length)
			{
				for (int i = 0; i < size; i++)
				{
					ushort num = buffer[i];
					if (num != 0 && num != buildingID)
					{
						BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_level == this.m_info.m_class.m_level && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None && instance.m_buildings.m_buffer[(int)num].m_productionRate != 0 && !info.m_buildingAI.IsFull(num, ref instance.m_buildings.m_buffer[(int)num]))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		string text2;
		if (this.m_garbageConsumption > 0)
		{
			text2 = LocaleFormatter.FormatGeneric("AIINFO_PROCESSING_RATE", new object[]
			{
				this.m_garbageConsumption * 16
			});
			if (this.m_electricityProduction != 0)
			{
				text2 = text2 + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_PRODUCTION", new object[]
				{
					this.m_electricityProduction * 16 / 1000
				});
			}
		}
		else
		{
			text2 = LocaleFormatter.FormatGeneric("AIINFO_CAPACITY", new object[]
			{
				this.m_garbageCapacity
			});
		}
		text2 = text2 + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_GARBAGETRUCK_CAPACITY", new object[]
		{
			this.m_garbageTruckCount
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			text2
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num = (productionRate * this.m_garbageTruckCount + 99) / 100;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.GarbageMove, ref num2, ref num3, ref num4, ref num5);
		}
		else
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Garbage, ref num2, ref num3, ref num4, ref num5);
		}
		string text = string.Empty;
		if (this.m_garbageConsumption > 0)
		{
			text = text + LocaleFormatter.FormatGeneric("AIINFO_GARBAGE_RESERVES", new object[]
			{
				this.GetGarbageAmount(buildingID, ref data),
				this.m_garbageCapacity
			}) + Environment.NewLine;
			text = text + LocaleFormatter.FormatGeneric("AIINFO_PROCESSING_RATE", new object[]
			{
				-this.GetGarbageRate(buildingID, ref data) * 16
			}) + Environment.NewLine;
			if (this.m_electricityProduction != 0)
			{
				int electricityRate = this.GetElectricityRate(buildingID, ref data);
				text = text + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_PRODUCTION", new object[]
				{
					electricityRate * 16 / 1000
				}) + Environment.NewLine;
			}
			text += LocaleFormatter.FormatGeneric("AIINFO_GARBAGE_TRUCKS", new object[]
			{
				num2,
				num
			});
		}
		else
		{
			int garbageAmount = this.GetGarbageAmount(buildingID, ref data);
			int num6 = 0;
			if (garbageAmount != 0)
			{
				num6 = Mathf.Max(1, garbageAmount * 100 / this.m_garbageCapacity);
			}
			text = text + LocaleFormatter.FormatGeneric("AIINFO_FULL", new object[]
			{
				num6
			}) + Environment.NewLine;
			text += LocaleFormatter.FormatGeneric("AIINFO_GARBAGE_TRUCKS", new object[]
			{
				num2,
				num
			});
		}
		return text;
	}

	private int MaxOutgoingLoadSize()
	{
		return 8000;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 2;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 2;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 1;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3;

	[CustomizableProperty("Garbage Truck Count")]
	public int m_garbageTruckCount = 3;

	[CustomizableProperty("Garbage Capacity")]
	public int m_garbageCapacity = 10000000;

	[CustomizableProperty("Garbage Consumption")]
	public int m_garbageConsumption = 1000;

	[CustomizableProperty("Electricity Production")]
	public int m_electricityProduction = 1000;

	[CustomizableProperty("Material Production")]
	public int m_materialProduction;

	[CustomizableProperty("Pollution Accumulation", "Pollution")]
	public int m_pollutionAccumulation = 100;

	[CustomizableProperty("Pollution Radius", "Pollution")]
	public float m_pollutionRadius = 150f;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation = 50;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;

	[CustomizableProperty("Animal Count")]
	public int m_animalCount = 3;

	[CustomizableProperty("Collect Radius")]
	public float m_collectRadius = 2000f;

	public ManualMilestone m_fullPassMilestone;
}
