﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class DemandWrapper : IDemand
{
	public DemandWrapper(ZoneManager zoneManager)
	{
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnDemandExtensionsReleased();
		this.m_DemandExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IDemandExtension>();
		this.OnDemandExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnDemandExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnDemandExtensionsCreated()
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			try
			{
				this.m_DemandExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_DemandExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnDemandExtensionsReleased()
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			try
			{
				this.m_DemandExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_DemandExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnCalculateResidentialDemand(ref int demand)
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			demand = this.m_DemandExtensions[i].OnCalculateResidentialDemand(demand);
		}
	}

	public void OnCalculateCommercialDemand(ref int demand)
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			demand = this.m_DemandExtensions[i].OnCalculateCommercialDemand(demand);
		}
	}

	public void OnCalculateWorkplaceDemand(ref int demand)
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			demand = this.m_DemandExtensions[i].OnCalculateWorkplaceDemand(demand);
		}
	}

	public void OnUpdateDemand(int lastDemand, ref int nextDemand, int targetDemand)
	{
		for (int i = 0; i < this.m_DemandExtensions.Count; i++)
		{
			nextDemand = this.m_DemandExtensions[i].OnUpdateDemand(lastDemand, nextDemand, targetDemand);
		}
	}

	private List<IDemandExtension> m_DemandExtensions = new List<IDemandExtension>();
}
