﻿using System;
using UnityEngine;

public class ImprovedPerlinNoise
{
	public ImprovedPerlinNoise(int seed)
	{
		Random.InitState(seed);
		int i;
		for (i = 0; i < 256; i++)
		{
			this.m_perm[i] = i;
		}
		while (--i != 0)
		{
			int num = this.m_perm[i];
			int num2 = Random.Range(0, 256);
			this.m_perm[i] = this.m_perm[num2];
			this.m_perm[num2] = num;
		}
		for (i = 0; i < 256; i++)
		{
			this.m_perm[256 + i] = this.m_perm[i];
		}
	}

	public Texture2D GetPermutationTable1D()
	{
		return this.m_permTable1D;
	}

	public Texture2D GetPermutationTable2D()
	{
		return this.m_permTable2D;
	}

	public Texture2D GetGradient2D()
	{
		return this.m_gradient2D;
	}

	public Texture2D GetGradient3D()
	{
		return this.m_gradient3D;
	}

	public Texture2D GetGradient4D()
	{
		return this.m_gradient4D;
	}

	public void LoadResourcesFor2DNoise()
	{
		this.LoadPermTable1D();
		this.LoadGradient2D();
	}

	public void LoadResourcesFor3DNoise()
	{
		this.LoadPermTable2D();
		this.LoadGradient3D();
	}

	public void LoadResourcesFor4DNoise()
	{
		this.LoadPermTable1D();
		this.LoadPermTable2D();
		this.LoadGradient4D();
	}

	private void LoadPermTable1D()
	{
		if (this.m_permTable1D)
		{
			return;
		}
		this.m_permTable1D = new Texture2D(256, 1, 1, false, true);
		this.m_permTable1D.set_filterMode(0);
		this.m_permTable1D.set_wrapMode(0);
		for (int i = 0; i < 256; i++)
		{
			this.m_permTable1D.SetPixel(i, 1, new Color(0f, 0f, 0f, (float)this.m_perm[i] / 255f));
		}
		this.m_permTable1D.Apply();
	}

	private void LoadPermTable2D()
	{
		if (this.m_permTable2D)
		{
			return;
		}
		this.m_permTable2D = new Texture2D(256, 256, 5, false, true);
		this.m_permTable2D.set_filterMode(0);
		this.m_permTable2D.set_wrapMode(0);
		for (int i = 0; i < 256; i++)
		{
			for (int j = 0; j < 256; j++)
			{
				int num = this.m_perm[i] + j;
				int num2 = this.m_perm[num];
				int num3 = this.m_perm[num + 1];
				int num4 = this.m_perm[i + 1] + j;
				int num5 = this.m_perm[num4];
				int num6 = this.m_perm[num4 + 1];
				this.m_permTable2D.SetPixel(i, j, new Color((float)num2 / 255f, (float)num3 / 255f, (float)num5 / 255f, (float)num6 / 255f));
			}
		}
		this.m_permTable2D.Apply();
	}

	private void LoadGradient2D()
	{
		if (this.m_gradient2D)
		{
			return;
		}
		this.m_gradient2D = new Texture2D(8, 1, 3, false, true);
		this.m_gradient2D.set_filterMode(0);
		this.m_gradient2D.set_wrapMode(0);
		for (int i = 0; i < 8; i++)
		{
			float num = (ImprovedPerlinNoise.GRADIENT2[i * 2] + 1f) * 0.5f;
			float num2 = (ImprovedPerlinNoise.GRADIENT2[i * 2 + 1] + 1f) * 0.5f;
			this.m_gradient2D.SetPixel(i, 0, new Color(num, num2, 0f, 1f));
		}
		this.m_gradient2D.Apply();
	}

	private void LoadGradient3D()
	{
		if (this.m_gradient3D)
		{
			return;
		}
		this.m_gradient3D = new Texture2D(256, 1, 3, false, true);
		this.m_gradient3D.set_filterMode(0);
		this.m_gradient3D.set_wrapMode(0);
		for (int i = 0; i < 256; i++)
		{
			int num = this.m_perm[i] % 16;
			float num2 = (ImprovedPerlinNoise.GRADIENT3[num * 3] + 1f) * 0.5f;
			float num3 = (ImprovedPerlinNoise.GRADIENT3[num * 3 + 1] + 1f) * 0.5f;
			float num4 = (ImprovedPerlinNoise.GRADIENT3[num * 3 + 2] + 1f) * 0.5f;
			this.m_gradient3D.SetPixel(i, 0, new Color(num2, num3, num4, 1f));
		}
		this.m_gradient3D.Apply();
	}

	private void LoadGradient4D()
	{
		if (this.m_gradient4D)
		{
			return;
		}
		this.m_gradient4D = new Texture2D(256, 1, 5, false, true);
		this.m_gradient4D.set_filterMode(0);
		this.m_gradient4D.set_wrapMode(0);
		for (int i = 0; i < 256; i++)
		{
			int num = this.m_perm[i] % 32;
			float num2 = (ImprovedPerlinNoise.GRADIENT4[num * 4] + 1f) * 0.5f;
			float num3 = (ImprovedPerlinNoise.GRADIENT4[num * 4 + 1] + 1f) * 0.5f;
			float num4 = (ImprovedPerlinNoise.GRADIENT4[num * 4 + 2] + 1f) * 0.5f;
			float num5 = (ImprovedPerlinNoise.GRADIENT4[num * 4 + 3] + 1f) * 0.5f;
			this.m_gradient4D.SetPixel(i, 0, new Color(num2, num3, num4, num5));
		}
		this.m_gradient4D.Apply();
	}

	private const int SIZE = 256;

	private int[] m_perm = new int[512];

	private Texture2D m_permTable1D;

	private Texture2D m_permTable2D;

	private Texture2D m_gradient2D;

	private Texture2D m_gradient3D;

	private Texture2D m_gradient4D;

	private static float[] GRADIENT2 = new float[]
	{
		0f,
		1f,
		1f,
		1f,
		1f,
		0f,
		1f,
		-1f,
		0f,
		-1f,
		-1f,
		-1f,
		-1f,
		0f,
		-1f,
		1f
	};

	private static float[] GRADIENT3 = new float[]
	{
		1f,
		1f,
		0f,
		-1f,
		1f,
		0f,
		1f,
		-1f,
		0f,
		-1f,
		-1f,
		0f,
		1f,
		0f,
		1f,
		-1f,
		0f,
		1f,
		1f,
		0f,
		-1f,
		-1f,
		0f,
		-1f,
		0f,
		1f,
		1f,
		0f,
		-1f,
		1f,
		0f,
		1f,
		-1f,
		0f,
		-1f,
		-1f,
		1f,
		1f,
		0f,
		0f,
		-1f,
		1f,
		-1f,
		1f,
		0f,
		0f,
		-1f,
		-1f
	};

	private static float[] GRADIENT4 = new float[]
	{
		0f,
		-1f,
		-1f,
		-1f,
		0f,
		-1f,
		-1f,
		1f,
		0f,
		-1f,
		1f,
		-1f,
		0f,
		-1f,
		1f,
		1f,
		0f,
		1f,
		-1f,
		-1f,
		0f,
		1f,
		-1f,
		1f,
		0f,
		1f,
		1f,
		-1f,
		0f,
		1f,
		1f,
		1f,
		-1f,
		-1f,
		0f,
		-1f,
		-1f,
		1f,
		0f,
		-1f,
		1f,
		-1f,
		0f,
		-1f,
		1f,
		1f,
		0f,
		-1f,
		-1f,
		-1f,
		0f,
		1f,
		-1f,
		1f,
		0f,
		1f,
		1f,
		-1f,
		0f,
		1f,
		1f,
		1f,
		0f,
		1f,
		-1f,
		0f,
		-1f,
		-1f,
		1f,
		0f,
		-1f,
		-1f,
		-1f,
		0f,
		-1f,
		1f,
		1f,
		0f,
		-1f,
		1f,
		-1f,
		0f,
		1f,
		-1f,
		1f,
		0f,
		1f,
		-1f,
		-1f,
		0f,
		1f,
		1f,
		1f,
		0f,
		1f,
		1f,
		0f,
		-1f,
		-1f,
		0f,
		0f,
		-1f,
		-1f,
		0f,
		0f,
		-1f,
		1f,
		0f,
		0f,
		-1f,
		1f,
		0f,
		0f,
		1f,
		-1f,
		0f,
		0f,
		1f,
		-1f,
		0f,
		0f,
		1f,
		1f,
		0f,
		0f,
		1f,
		1f,
		0f
	};
}
