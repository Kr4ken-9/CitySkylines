﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class RadioPanel : ToolsModifierControl
{
	private void Awake()
	{
		this.m_radioButton = base.Find<UIButton>("RadioButton");
		this.m_radioPanel = base.Find<UIPanel>("RadioPlayingPanel");
		this.m_scrollablePanel = base.Find<UIScrollablePanel>("TextScroller");
		this.m_scrollText = base.Find<UILabel>("RadioText");
		this.m_radioListButton = base.Find<UIButton>("RadioDropDownButton");
		this.m_radioListButton.add_eventClicked(new MouseEventHandler(this.OnListOpenButtonClick));
		this.m_radioList = base.Find<UIPanel>("StationsList");
		this.m_stationButtons = new UITemplateList<UIButton>(this.m_radioList, "RadioStationButtonTemplate");
		this.m_radioList.set_isVisible(false);
		this.m_muteButton = base.Find<UIMultiStateButton>("MuteButton");
		this.m_muteButton.set_activeStateIndex((!Singleton<AudioManager>.get_instance().MuteRadio) ? 0 : 1);
		this.m_muteButton.add_eventActiveStateIndexChanged(new PropertyChangedEventHandler<int>(this.OnMuteButtonIndexChanged));
		this.HideRadio();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		Singleton<AudioManager>.get_instance().m_radioContentChanged += new AudioManager.RadioContentChangedHandler(this.OnRadioContentChanged);
	}

	private void OnDestroy()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().RadioChannel(this.m_selectedStation);
		}
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		Singleton<AudioManager>.get_instance().m_radioContentChanged -= new AudioManager.RadioContentChangedHandler(this.OnRadioContentChanged);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		int num = PrefabCollection<RadioChannelInfo>.LoadedCount();
		this.m_stations = new RadioChannelInfo[num];
		ushort num2 = 0;
		while ((int)num2 < num)
		{
			this.m_stations[(int)num2] = PrefabCollection<RadioChannelInfo>.GetLoaded((uint)num2);
			num2 += 1;
		}
		this.m_stationIndices = new int[this.m_stations.Length - 1];
		RadioChannelInfo activeRadioChannelInfo = Singleton<AudioManager>.get_instance().GetActiveRadioChannelInfo();
		this.m_selectedStation = activeRadioChannelInfo;
		this.AssignStationToButton(this.m_radioListButton, activeRadioChannelInfo);
		this.AssignStationToButton(this.m_radioButton, activeRadioChannelInfo);
		this.StopScrolling();
		this.StartScrollingIfNeeded();
	}

	private void OnRadioContentChanged()
	{
		this.StartScrollingIfNeeded();
	}

	private void OnMuteButtonIndexChanged(UIComponent comp, int index)
	{
		if (index == 0)
		{
			this.Unmute();
		}
		else
		{
			this.Mute();
		}
	}

	private void Mute()
	{
		Singleton<AudioManager>.get_instance().MuteRadio = true;
	}

	private void Unmute()
	{
		Singleton<AudioManager>.get_instance().MuteRadio = false;
	}

	private void SelectStation(RadioChannelInfo station)
	{
		SavedBool hasSeenGoldFMWarning = new SavedBool(Settings.hasSeenStreamingWarningGoldFM, Settings.gameSettingsFile, false);
		bool flag = !hasSeenGoldFMWarning;
		if (flag)
		{
			string text = Environment.NewLine + "- " + Locale.Get("RADIO_CHANNEL_TITLE", "GoldFM");
			MessageBoxPanel.ShowModal("STREAMINGWARNING", delegate(UIComponent cp, int r)
			{
				if (r == 0)
				{
					hasSeenGoldFMWarning.set_value(true);
					this.DoSelectStation(station);
				}
			}, false, new string[]
			{
				text
			});
		}
		else
		{
			this.DoSelectStation(station);
		}
	}

	private void DoSelectStation(RadioChannelInfo station)
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().RadioChannel(this.m_selectedStation);
		}
		this.m_selectedStation = station;
		this.AssignStationToButton(this.m_radioListButton, station);
		this.AssignStationToButton(this.m_radioButton, station);
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			Singleton<AudioManager>.get_instance().SetActiveRadioChannelInfo(station);
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.StopScrolling();
				this.StartScrollingIfNeeded();
			});
		});
	}

	private void OnListOpenButtonClick(UIComponent comp, UIMouseEventParameter param)
	{
		if (this.m_radioList.get_isVisible())
		{
			this.CloseList();
		}
		else
		{
			this.OpenList();
		}
		param.Use();
	}

	private void OnListButtonClicked(UIComponent comp, UIMouseEventParameter param)
	{
		this.SelectStation(this.m_stations[this.m_stationIndices[comp.get_zOrder()]]);
		this.CloseList();
	}

	private void OpenList()
	{
		this.RefreshStationList();
		this.m_radioList.set_isVisible(true);
	}

	private void CloseList()
	{
		this.m_radioList.set_isVisible(false);
	}

	private void RefreshStationList()
	{
		UIButton[] array = this.m_stationButtons.SetItemCount(this.m_stations.Length - 1);
		int num = 0;
		for (int i = 0; i < this.m_stations.Length; i++)
		{
			if (this.m_stations[i] != this.m_selectedStation)
			{
				this.AssignStationToButton(array[num], this.m_stations[i]);
				array[num].remove_eventClicked(new MouseEventHandler(this.OnListButtonClicked));
				array[num].add_eventClicked(new MouseEventHandler(this.OnListButtonClicked));
				this.m_stationIndices[num] = i;
				num++;
			}
		}
	}

	private void AssignStationToButton(UIButton button, RadioChannelInfo station)
	{
		if (station.m_Thumbnail == null)
		{
			Debug.LogError("Station " + station.GetLocalizedTitle() + " has no thumbnail assigned.");
		}
		button.set_normalFgSprite(station.m_Thumbnail);
		button.set_hoveredFgSprite(station.m_Thumbnail + "Hovered");
		button.set_pressedFgSprite(station.m_Thumbnail + "Pressed");
		button.set_focusedFgSprite(station.m_Thumbnail);
		button.set_disabledFgSprite(station.m_Thumbnail + "Disabled");
		button.set_tooltip(station.GetLocalizedTitle());
	}

	public void ShowRadio()
	{
		this.m_radioButton.set_isVisible(false);
		this.m_radioPanel.set_isVisible(true);
		this.m_isVisible = true;
		this.m_radioListButton.Focus();
		this.StartScrollingIfNeeded();
	}

	private void StartScrollingIfNeeded()
	{
		if (this.m_isVisible && !this.m_isScrolling)
		{
			this.RefreshScrollingText();
			this.m_scrollablePanel.set_scrollPosition(Vector2.get_zero());
			if (this.m_scrollText.get_width() > this.m_scrollablePanel.get_width())
			{
				this.WaitAndStartScrolling();
			}
		}
	}

	private void WaitAndStartScrolling()
	{
		base.Invoke("StartScrolling", 0.8f);
	}

	private void StartScrolling()
	{
		this.m_isScrolling = true;
		float num = this.m_scrollText.get_width() - this.m_scrollablePanel.get_width();
		ValueAnimator.Animate("RadioPanelScrolling", delegate(float val)
		{
			this.m_scrollablePanel.set_scrollPosition(new Vector2(val, 0f));
		}, new AnimatedFloat(0f, num, num / this.m_scrollSpeed), delegate
		{
			this.OnCompletedScrolling();
		});
	}

	private void OnCompletedScrolling()
	{
		this.m_isScrolling = false;
		base.Invoke("StartScrollingIfNeeded", 2.5f);
	}

	private void StopScrolling()
	{
		this.m_isScrolling = false;
		ValueAnimator.Cancel("RadioPanelScrolling");
		this.m_scrollablePanel.set_scrollPosition(Vector2.get_zero());
	}

	private void RefreshScrollingText()
	{
		ushort activeRadioChannel = Singleton<AudioManager>.get_instance().GetActiveRadioChannel();
		if (activeRadioChannel != 0)
		{
			ushort currentContent = Singleton<AudioManager>.get_instance().m_radioChannels.m_buffer[(int)activeRadioChannel].m_currentContent;
			if (currentContent != 0)
			{
				RadioContentInfo info = Singleton<AudioManager>.get_instance().m_radioContents.m_buffer[(int)currentContent].Info;
				string localizedTitle;
				if (info.m_contentType == RadioContentInfo.ContentType.Music)
				{
					localizedTitle = info.GetLocalizedTitle();
				}
				else
				{
					localizedTitle = this.m_selectedStation.GetLocalizedTitle();
				}
				if (this.m_scrollText.get_text() != localizedTitle)
				{
					this.m_scrollText.set_text(localizedTitle);
				}
			}
			else if (activeRadioChannel == 1)
			{
				this.m_scrollText.set_text(this.m_selectedStation.GetLocalizedTitle());
			}
		}
	}

	public void HideRadio()
	{
		if (this.m_radioList.get_isVisible())
		{
			this.CloseList();
		}
		this.StopScrolling();
		this.m_radioButton.set_isVisible(true);
		this.m_radioPanel.set_isVisible(false);
		this.m_isVisible = false;
	}

	private UIScrollablePanel m_scrollablePanel;

	private UILabel m_scrollText;

	private UIButton m_radioButton;

	private UIButton m_radioListButton;

	private UIPanel m_radioList;

	private UIPanel m_radioPanel;

	private bool m_isVisible;

	private bool m_isScrolling;

	public float m_scrollSpeed = 20f;

	private RadioChannelInfo[] m_stations;

	private RadioChannelInfo m_selectedStation;

	private UIMultiStateButton m_muteButton;

	private UITemplateList<UIButton> m_stationButtons;

	private int[] m_stationIndices;
}
