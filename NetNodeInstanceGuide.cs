﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class NetNodeInstanceGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, ushort nodeID, Notification.Problem problems, bool ignoreID)
	{
		if (this.m_nodeID != 0 && !this.Validate())
		{
			this.Deactivate();
		}
		if (this.m_nodeID == 0 || this.m_nodeID == nodeID || ignoreID)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			this.m_nodeID = nodeID;
			this.m_buildIndex = instance.m_nodes.m_buffer[(int)this.m_nodeID].m_buildIndex;
			this.m_problems = problems;
			if (base.CanActivate(guideInfo))
			{
				Vector3 position = instance.m_nodes.m_buffer[(int)this.m_nodeID].m_position;
				if (Singleton<SimulationManager>.get_instance().m_simulationView.Intersect(position, -10f))
				{
					GuideTriggerBase.ActivationInfo activationInfo = new NetNodeInstanceGuide.NetNodeInstanceActivationInfo(guideInfo, nodeID);
					base.Activate(guideInfo, activationInfo);
				}
			}
		}
	}

	public override void Deactivate()
	{
		this.m_nodeID = 0;
		this.m_buildIndex = 0u;
		this.m_problems = Notification.Problem.None;
		base.Deactivate();
	}

	public override void Disable()
	{
		this.m_nodeID = 0;
		this.m_buildIndex = 0u;
		this.m_problems = Notification.Problem.None;
		base.Disable();
	}

	public override bool Validate()
	{
		if (this.m_nodeID == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		return instance.m_nodes.m_buffer[(int)this.m_nodeID].m_flags != NetNode.Flags.None && instance.m_nodes.m_buffer[(int)this.m_nodeID].m_buildIndex == this.m_buildIndex && (instance.m_nodes.m_buffer[(int)this.m_nodeID].m_problems & this.m_problems) == this.m_problems && base.Validate();
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteULong64((ulong)this.m_problems);
		s.WriteUInt32(this.m_buildIndex);
		s.WriteUInt16((uint)this.m_nodeID);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		if (s.get_version() >= 221u && Singleton<SimulationManager>.get_instance().m_metaData.m_saveAppVersion >= BuildConfig.MakeVersionNumber(1u, 3u, 0u, BuildConfig.ReleaseType.Prototype, 1u, BuildConfig.BuildType.Unknown))
		{
			this.m_problems = (Notification.Problem)s.ReadULong64();
		}
		else
		{
			ulong num = (ulong)s.ReadUInt32();
			num = ((num & (ulong)-1073741824) << 32 | (num & 1073741823uL));
			this.m_problems = (Notification.Problem)num;
		}
		this.m_buildIndex = s.ReadUInt32();
		this.m_nodeID = (ushort)s.ReadUInt16();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public Notification.Problem m_problems;

	public uint m_buildIndex;

	public ushort m_nodeID;

	public class NetNodeInstanceActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public NetNodeInstanceActivationInfo(GuideInfo guideInfo, ushort nodeID) : base(guideInfo)
		{
			this.m_nodeID = nodeID;
		}

		public override string GetName()
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_nodeID].Info;
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<NetInfo>.PrefabName((uint)info.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_nodeID].Info;
			string icon = this.m_guideInfo.m_icon;
			string thumbnail = info.m_Thumbnail;
			return StringUtils.SafeFormat(icon, thumbnail);
		}

		public override string FormatText(string pattern)
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_nodeID].Info;
			string text = PrefabCollection<NetInfo>.PrefabName((uint)info.m_prefabDataIndex);
			string @unchecked = Locale.GetUnchecked("NET_TITLE", text);
			return StringUtils.SafeFormat(pattern, @unchecked);
		}

		public override IUITag GetTag()
		{
			return new NetNodeTutorialTag(this.m_nodeID);
		}

		protected ushort m_nodeID;
	}
}
