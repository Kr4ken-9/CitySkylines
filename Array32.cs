﻿using System;
using ColossalFramework.Math;

public class Array32<T>
{
	public Array32(uint arraySize)
	{
		this.m_size = arraySize;
		this.m_buffer = new T[arraySize];
		this.m_unusedCount = arraySize - 1u;
		this.m_unusedItems = new uint[arraySize];
		for (uint num = 1u; num < arraySize; num += 1u)
		{
			this.m_unusedItems[(int)((UIntPtr)(num - 1u))] = num;
		}
		this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount++))] = 0u;
	}

	public bool CreateItem(out uint item)
	{
		if (this.m_unusedCount == 0u)
		{
			item = 0u;
			return false;
		}
		item = this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount -= 1u))];
		return true;
	}

	public bool CreateItem(out uint item, ref Randomizer r)
	{
		if (this.m_unusedCount == 0u)
		{
			item = 0u;
			return false;
		}
		int num = r.Int32(this.m_unusedCount);
		item = this.m_unusedItems[num];
		this.m_unusedItems[num] = this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount -= 1u))];
		return true;
	}

	public uint NextFreeItem()
	{
		if (this.m_unusedCount == 0u)
		{
			return 0u;
		}
		return this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount - 1u))];
	}

	public uint NextFreeItem(ref Randomizer r)
	{
		if (this.m_unusedCount == 0u)
		{
			return 0u;
		}
		int num = r.Int32(this.m_unusedCount);
		return this.m_unusedItems[num];
	}

	public void ReleaseItem(uint item)
	{
		this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount++))] = item;
	}

	public void ClearUnused()
	{
		this.m_unusedCount = 0u;
	}

	public uint ItemCount()
	{
		return this.m_size - this.m_unusedCount;
	}

	public uint m_size;

	public T[] m_buffer;

	private uint m_unusedCount;

	private uint[] m_unusedItems;
}
