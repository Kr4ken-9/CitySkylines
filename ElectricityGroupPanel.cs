﻿using System;

public class ElectricityGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Electricity;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Building;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "ElectricityDefault")
			{
				return 0;
			}
			if (name == "MonumentModderPack")
			{
				return 1;
			}
		}
		return 2147483647;
	}
}
