﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CanalAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		if (this.m_transportInfo != null && this.m_transportInfo.m_prefabDataIndex != -1)
		{
			Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
		}
	}

	public override void SegmentLoaded(ushort segmentID, ref NetSegment data)
	{
		base.SegmentLoaded(segmentID, ref data);
		if (this.m_transportInfo != null && this.m_transportInfo.m_prefabDataIndex != -1)
		{
			Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
		}
	}

	public override void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
		if (this.m_transportInfo != null && this.m_transportInfo.m_prefabDataIndex != -1)
		{
			Singleton<NetManager>.get_instance().RemoveServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, false);
		}
		base.ReleaseSegment(segmentID, ref data);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + this.m_info.m_minHeight + 1000f;
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (maxElevation > 8f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override bool UseMinHeight()
	{
		return true;
	}

	public override bool IgnoreWater()
	{
		return true;
	}

	public override bool DisplayTempSegment()
	{
		return false;
	}

	public override bool IsCombatible(NetInfo with)
	{
		return base.IsCombatible(with) && this.m_info.m_minHeight == with.m_minHeight;
	}

	public override bool Snap(ushort segmentID, ref NetSegment data, NetInfo info, Vector3 refPos, float elevation, out float distance, out Vector3 snapPos, out Vector3 snapDir)
	{
		distance = 0f;
		snapPos = refPos;
		snapDir = Vector3.get_forward();
		if (info.m_class.m_layer != ItemClass.Layer.Default)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		data.GetClosestPositionAndDirection(refPos, out snapPos, out snapDir);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		float num = Vector2.Dot(VectorUtils.XZ(refPos - position), VectorUtils.XZ(data.m_startDirection));
		float num2 = Vector2.Dot(VectorUtils.XZ(refPos - position2), VectorUtils.XZ(data.m_endDirection));
		snapDir.y = 0f;
		snapDir.Normalize();
		bool flag = false;
		if (num <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position - refPos);
			snapDir.y = 0f;
		}
		else if (num2 <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position2 - refPos);
			snapDir.y = 0f;
		}
		else
		{
			if (NetSegment.IsStraight(position, data.m_startDirection, position2, data.m_endDirection))
			{
				Vector3 vector = snapPos - position;
				Vector3 vector2 = snapPos - position2;
				if (vector2.get_sqrMagnitude() < vector.get_sqrMagnitude())
				{
					float num3 = Vector3.Dot(snapDir, vector2);
					num3 = Mathf.Round(num3 * 0.125f) * 8f - num3;
					snapPos += snapDir * num3;
				}
				else
				{
					float num4 = Vector3.Dot(snapDir, vector);
					num4 = Mathf.Round(num4 * 0.125f) * 8f - num4;
					snapPos += snapDir * num4;
				}
			}
			snapDir..ctor(snapDir.z, 0f, -snapDir.x);
			flag = (Vector3.Dot(snapPos - refPos, snapDir) < 0f);
			if (flag)
			{
				snapDir = -snapDir;
			}
		}
		distance = Vector3.Distance(snapPos, refPos);
		float num5 = this.m_info.m_halfWidth + Mathf.Ceil(info.m_netAI.GetCollisionHalfWidth() * 0.25f - 0.01f) * 4f;
		if (info.m_class.m_service == ItemClass.Service.Beautification)
		{
			if (info.m_class.m_level == ItemClass.Level.Level5)
			{
				if (distance < num5 + 24f)
				{
					num5 += 24f;
					snapPos -= snapDir * num5;
					snapPos.y = NetSegment.SampleTerrainHeight(info, snapPos, false, elevation);
					return true;
				}
			}
			else if (info.m_class.m_level >= ItemClass.Level.Level3 && distance < num5 + 1f)
			{
				num5 += 1f;
				snapPos -= snapDir * num5;
				snapPos.y = NetSegment.SampleTerrainHeight(info, snapPos, false, elevation);
				return true;
			}
		}
		else if (distance < num5 + 12f)
		{
			if (info.m_class.m_service != ItemClass.Service.Electricity && (info.m_class.m_service != ItemClass.Service.PublicTransport || info.m_class.m_subService != ItemClass.SubService.PublicTransportCableCar) && (distance > num5 + 4f || elevation < 1f))
			{
				num5 += 8f;
				elevation += 1f;
			}
			snapPos -= snapDir * num5;
			snapPos.y = CanalAI.GetActualHeight(segmentID, ref data, snapPos, info.m_halfWidth, flag) + elevation;
			return true;
		}
		return false;
	}

	private static float GetActualHeight(ushort segmentID, ref NetSegment data, Vector3 refPos, float halfWidth, bool right)
	{
		Vector3 vector;
		Vector3 startDir;
		bool smoothStart;
		data.CalculateCorner(segmentID, true, true, !right, out vector, out startDir, out smoothStart);
		Vector3 vector2;
		Vector3 endDir;
		bool smoothEnd;
		data.CalculateCorner(segmentID, true, false, right, out vector2, out endDir, out smoothEnd);
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateMiddlePoints(vector, startDir, vector2, endDir, smoothStart, smoothEnd, out vector3, out vector4);
		Bezier3 bezier;
		bezier..ctor(vector, vector3, vector4, vector2);
		float num;
		bezier.DistanceSqr(refPos, ref num);
		return bezier.Position(num).y + Mathf.Abs(VectorUtils.NormalizeXZ(bezier.Tangent(num)).y) * halfWidth * 1.5f;
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		if (index == 0)
		{
			heights &= ~TerrainModify.Heights.BlockHeight;
			return true;
		}
		if (index != 1)
		{
			return false;
		}
		surface = TerrainModify.Surface.None;
		heights = TerrainModify.Heights.DigHeight;
		if ((data.m_flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
		{
			leftY = 0f;
			rightY = 0f;
		}
		else
		{
			leftY = this.m_waterBlockOffset;
			rightY = this.m_waterBlockOffset;
		}
		return true;
	}

	public override bool SegmentModifyMask(ushort segmentID, ref NetSegment data, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftStartY, ref float rightStartY, ref float leftEndY, ref float rightEndY)
	{
		if (index == 0)
		{
			heights &= ~TerrainModify.Heights.BlockHeight;
			return true;
		}
		if (index != 1)
		{
			return false;
		}
		surface = TerrainModify.Surface.None;
		heights = TerrainModify.Heights.DigHeight;
		if ((data.m_flags & NetSegment.Flags.CrossingStart) != NetSegment.Flags.None)
		{
			leftStartY = 0f;
			rightStartY = 0f;
		}
		else
		{
			leftStartY = this.m_waterBlockOffset;
			rightStartY = this.m_waterBlockOffset;
		}
		if ((data.m_flags & NetSegment.Flags.CrossingEnd) != NetSegment.Flags.None)
		{
			leftEndY = 0f;
			rightEndY = 0f;
		}
		else
		{
			leftEndY = this.m_waterBlockOffset;
			rightEndY = this.m_waterBlockOffset;
		}
		return true;
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		NetNode.Flags flags = instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags;
		NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags;
		if ((flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingStart;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingStart;
		}
		if ((flags2 & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingEnd;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingEnd;
		}
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		bool flag2 = false;
		int num = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null && info.m_class.m_service == this.m_info.m_class.m_service)
				{
					if (info.m_minHeight != this.m_info.m_minHeight)
					{
						flag = true;
					}
					if (++num == 1)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
						if (startNode == nodeID)
						{
							flag2 = (instance.m_nodes.m_buffer[(int)endNode].m_position.y - data.m_position.y > 10f);
						}
						else
						{
							flag2 = (instance.m_nodes.m_buffer[(int)startNode].m_position.y - data.m_position.y > 10f);
						}
					}
				}
			}
		}
		if (flag)
		{
			data.m_flags |= NetNode.Flags.Transition;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.Transition;
		}
		if (flag2 && num == 1)
		{
			data.m_flags |= NetNode.Flags.LevelCrossing;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.LevelCrossing;
		}
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
	}

	public override void UpdateGuide(GuideController guideController)
	{
		GenericGuide canalsNotUsed = Singleton<NetManager>.get_instance().m_canalsNotUsed;
		if (canalsNotUsed != null)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				canalsNotUsed.Activate(guideController.m_canalsNotUsed);
			}
		}
	}

	public override void ConstructSucceeded()
	{
		GenericGuide canalsNotUsed = Singleton<NetManager>.get_instance().m_canalsNotUsed;
		if (canalsNotUsed != null)
		{
			canalsNotUsed.Disable();
		}
	}

	public override void BulldozeSucceeded()
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			GenericGuide canalDemolished = Singleton<NetManager>.get_instance().m_canalDemolished;
			if (canalDemolished != null)
			{
				canalDemolished.Activate(properties.m_canalDemolished);
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		NetManager instance = Singleton<NetManager>.get_instance();
		if (startPoint.m_node != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)startPoint.m_node].Info;
			if (info.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (startPoint.m_segment != 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)startPoint.m_segment].Info;
			if (info2.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		if (endPoint.m_node != 0)
		{
			NetInfo info3 = instance.m_nodes.m_buffer[(int)endPoint.m_node].Info;
			if (info3.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (endPoint.m_segment != 0)
		{
			NetInfo info4 = instance.m_segments.m_buffer[(int)endPoint.m_segment].Info;
			if (info4.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		Vector3 vector;
		Vector3 vector2;
		NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, true, true, out vector, out vector2);
		Bezier2 bezier;
		bezier.a = VectorUtils.XZ(startPoint.m_position);
		bezier.b = VectorUtils.XZ(vector);
		bezier.c = VectorUtils.XZ(vector2);
		bezier.d = VectorUtils.XZ(endPoint.m_position);
		int num = Mathf.CeilToInt(Vector2.Distance(bezier.a, bezier.d) * 0.02f) + 1;
		for (int i = 0; i <= num; i++)
		{
			Vector2 vector3 = bezier.Position((float)i / (float)num);
			Vector2 vector4 = bezier.Tangent((float)i / (float)num);
			Vector2 vector5;
			vector5..ctor(vector4.y, -vector4.x);
			vector4 = vector5.get_normalized();
			Vector2 vector6 = vector3 + vector4 * this.m_info.m_halfWidth;
			Vector2 vector7 = vector3 - vector4 * this.m_info.m_halfWidth;
			float num2 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(VectorUtils.X_Y(vector6));
			float num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(VectorUtils.X_Y(vector7));
			if (Mathf.Abs(num2 - num3) > this.m_info.m_halfWidth * this.m_info.m_maxSlope * 2f)
			{
				toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
			}
		}
		if (test)
		{
			Vector2 vector8 = bezier.Min() - new Vector2(this.m_info.m_halfWidth + 24f, this.m_info.m_halfWidth + 24f);
			Vector2 vector9 = bezier.Max() + new Vector2(this.m_info.m_halfWidth + 24f, this.m_info.m_halfWidth + 24f);
			int num4 = Mathf.Max((int)((vector8.x - 64f) / 64f + 135f), 0);
			int num5 = Mathf.Max((int)((vector8.y - 64f) / 64f + 135f), 0);
			int num6 = Mathf.Min((int)((vector9.x + 64f) / 64f + 135f), 269);
			int num7 = Mathf.Min((int)((vector9.y + 64f) / 64f + 135f), 269);
			for (int j = num5; j <= num7; j++)
			{
				for (int k = num4; k <= num6; k++)
				{
					ushort num8 = instance.m_segmentGrid[j * 270 + k];
					int num9 = 0;
					while (num8 != 0)
					{
						if (num8 != startPoint.m_segment && num8 != endPoint.m_segment)
						{
							ushort startNode = instance.m_segments.m_buffer[(int)num8].m_startNode;
							ushort endNode = instance.m_segments.m_buffer[(int)num8].m_endNode;
							if (startNode != startPoint.m_node && startNode != endPoint.m_node && endNode != startPoint.m_node && endNode != endPoint.m_node)
							{
								Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
								Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
								float num10 = Mathf.Max(Mathf.Max(vector8.x - 64f - position.x, vector8.y - 64f - position.z), Mathf.Max(position.x - vector9.x - 64f, position.z - vector9.y - 64f));
								float num11 = Mathf.Max(Mathf.Max(vector8.x - 64f - position2.x, vector8.y - 64f - position2.z), Mathf.Max(position2.x - vector9.x - 64f, position2.z - vector9.y - 64f));
								if (num10 < 0f || num11 < 0f)
								{
									NetInfo info5 = instance.m_segments.m_buffer[(int)num8].Info;
									if (info5.m_class.m_service == ItemClass.Service.Beautification && info5.m_class.m_level == ItemClass.Level.Level5)
									{
										float num12 = 1000000f;
										bool flag = false;
										if (startPoint.m_node != 0)
										{
											flag = instance.m_nodes.m_buffer[(int)startPoint.m_node].IsConnectedTo(startNode, endNode);
										}
										else if (startPoint.m_segment != 0)
										{
											ushort startNode2 = instance.m_segments.m_buffer[(int)startPoint.m_segment].m_startNode;
											ushort endNode2 = instance.m_segments.m_buffer[(int)startPoint.m_segment].m_endNode;
											flag = (instance.m_nodes.m_buffer[(int)startNode2].IsConnectedTo(startNode, endNode) || instance.m_nodes.m_buffer[(int)endNode2].IsConnectedTo(startNode, endNode));
										}
										bool flag2 = false;
										if (endPoint.m_node != 0)
										{
											flag2 = instance.m_nodes.m_buffer[(int)endPoint.m_node].IsConnectedTo(startNode, endNode);
										}
										else if (endPoint.m_segment != 0)
										{
											ushort startNode3 = instance.m_segments.m_buffer[(int)endPoint.m_segment].m_startNode;
											ushort endNode3 = instance.m_segments.m_buffer[(int)endPoint.m_segment].m_endNode;
											flag2 = (instance.m_nodes.m_buffer[(int)startNode3].IsConnectedTo(startNode, endNode) || instance.m_nodes.m_buffer[(int)endNode3].IsConnectedTo(startNode, endNode));
										}
										if (!flag)
										{
											Vector3 vector10 = instance.m_segments.m_buffer[(int)num8].GetClosestPosition(startPoint.m_position) - startPoint.m_position;
											num12 = Mathf.Min(num12, VectorUtils.LengthSqrXZ(vector10));
										}
										if (!flag && !flag2)
										{
											Vector3 vector11 = instance.m_segments.m_buffer[(int)num8].GetClosestPosition(vector) - vector;
											num12 = Mathf.Min(num12, VectorUtils.LengthSqrXZ(vector11));
											Vector3 vector12 = instance.m_segments.m_buffer[(int)num8].GetClosestPosition(vector2) - vector2;
											num12 = Mathf.Min(num12, VectorUtils.LengthSqrXZ(vector12));
										}
										if (!flag2)
										{
											Vector3 vector13 = instance.m_segments.m_buffer[(int)num8].GetClosestPosition(endPoint.m_position) - endPoint.m_position;
											num12 = Mathf.Min(num12, VectorUtils.LengthSqrXZ(vector13));
										}
										float num13 = this.m_info.m_halfWidth + info5.m_halfWidth + 23f;
										if (num12 < num13 * num13)
										{
											toolErrors |= ToolBase.ToolErrors.CanalTooClose;
										}
									}
								}
							}
						}
						num8 = instance.m_segments.m_buffer[(int)num8].m_nextGridSegment;
						if (++num9 >= 36864)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
		return toolErrors;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		result.g = 255;
		result.a = 0;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		result.g = 255;
		result.a = 0;
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public override float GetVScale()
	{
		return 0.02f;
	}

	public override ItemClass.Layer GetCollisionLayers()
	{
		return this.m_info.m_class.m_layer | ItemClass.Layer.MetroTunnels | ItemClass.Layer.WaterStructures;
	}

	public override bool ShowTerrainTopography()
	{
		return true;
	}

	public override bool CanConnect(NetInfo other)
	{
		if (other.m_class.m_service == this.m_info.m_class.m_service)
		{
			return other.m_class.m_level == this.m_info.m_class.m_level;
		}
		return (other.m_vehicleTypes & this.m_info.m_vehicleTypes) != VehicleInfo.VehicleType.None && !other.m_useFixedHeight;
	}

	[CustomizableProperty("Water Block Offset", "Properties")]
	public float m_waterBlockOffset = -20f;

	public TransportInfo m_transportInfo;
}
