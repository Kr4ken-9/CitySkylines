﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public abstract class KeyShortcuts : UIKeyShortcuts
{
	protected void SelectUIButton(string tagString)
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().ShortcutPressed(tagString);
		}
		TutorialUITag tutorialUITag = MonoTutorialTag.Find(tagString) as TutorialUITag;
		if (tutorialUITag != null && tutorialUITag.finalTarget != null)
		{
			for (int i = 100; i > 0; i--)
			{
				if (tutorialUITag.target == tutorialUITag.finalTarget)
				{
					tutorialUITag.target.SimulateClick();
					return;
				}
				tutorialUITag.target.SimulateClick();
				if (!tutorialUITag.target.get_isEnabled())
				{
					return;
				}
			}
			CODebugBase<LogChannel>.Error(LogChannel.CommandLine, "SelectUIButton() was terminated to prevent an infinite loop. This might be some kind of bug... :D");
		}
	}

	protected void SimulationPause()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().ShortcutPressed("simulation_pause");
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().SimulationPaused = !Singleton<SimulationManager>.get_instance().SimulationPaused;
			});
		}
	}

	protected void SimulationSpeed1()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().ShortcutPressed("simulation_speed_1");
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 1;
			});
		}
	}

	protected void SimulationSpeed2()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().ShortcutPressed("simulation_speed_2");
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 2;
			});
		}
	}

	protected void SimulationSpeed3()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().ShortcutPressed("simulation_speed_3");
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 3;
			});
		}
	}

	protected void PressToolModeButton(int index)
	{
		UIComponent currentlyVisibleOptionPanel = this.currentlyVisibleOptionPanel;
		if (currentlyVisibleOptionPanel != null)
		{
			UIComponent uIComponent = this.currentlyVisibleOptionPanel.Find<UIComponent>("ToolMode");
			if (uIComponent != null)
			{
				foreach (UIComponent current in uIComponent.get_components())
				{
					if (current.get_zOrder() == index)
					{
						UIButton uIButton = current as UIButton;
						if (uIButton != null)
						{
							uIButton.SimulateClick();
							return;
						}
					}
				}
			}
			if (currentlyVisibleOptionPanel.get_name() == "ZoningOptionPanel")
			{
				this.PressZoningToolButton(index);
			}
			else
			{
				this.PressBrushSizeButton(index);
			}
		}
	}

	private void PressZoningToolButton(int index)
	{
		UIComponent currentlyVisibleOptionPanel = this.currentlyVisibleOptionPanel;
		if (currentlyVisibleOptionPanel != null)
		{
			UIButton uIButton = null;
			switch (index)
			{
			case 0:
				uIButton = currentlyVisibleOptionPanel.Find<UIButton>("Fill");
				break;
			case 1:
				uIButton = currentlyVisibleOptionPanel.Find<UIButton>("Marquee");
				break;
			case 2:
				uIButton = currentlyVisibleOptionPanel.Find<UIButton>("BrushSmall");
				break;
			case 3:
				uIButton = currentlyVisibleOptionPanel.Find<UIButton>("BrushLarge");
				break;
			}
			if (uIButton != null)
			{
				uIButton.SimulateClick();
			}
		}
	}

	private void PressBrushSizeButton(int index)
	{
		UIComponent currentlyVisibleOptionPanel = this.currentlyVisibleOptionPanel;
		if (currentlyVisibleOptionPanel != null)
		{
			UIButton uIButton = null;
			if (index != 0)
			{
				if (index != 1)
				{
					if (index == 2)
					{
						uIButton = currentlyVisibleOptionPanel.Find<UIButton>("BrushLarge");
					}
				}
				else
				{
					uIButton = currentlyVisibleOptionPanel.Find<UIButton>("BrushMedium");
				}
			}
			else
			{
				uIButton = currentlyVisibleOptionPanel.Find<UIButton>("BrushSmall");
			}
			if (uIButton != null)
			{
				uIButton.SimulateClick();
			}
		}
	}

	protected void PressSnappingMenuButton()
	{
		UIComponent currentlyVisibleOptionPanel = this.currentlyVisibleOptionPanel;
		if (currentlyVisibleOptionPanel != null)
		{
			UIComponent uIComponent = currentlyVisibleOptionPanel.Find<UIComponent>("SnappingToggle");
			if (uIComponent != null)
			{
				uIComponent.SimulateClick();
			}
		}
	}

	protected void PressElevationStepButton()
	{
		UIComponent currentlyVisibleOptionPanel = this.currentlyVisibleOptionPanel;
		if (currentlyVisibleOptionPanel != null)
		{
			UIComponent uIComponent = currentlyVisibleOptionPanel.Find<UIComponent>("ElevationStep");
			if (uIComponent != null)
			{
				uIComponent.SimulateClick();
			}
			else
			{
				UIComponent uIComponent2 = currentlyVisibleOptionPanel.Find<UIComponent>("BrushStrength");
				if (uIComponent2 != null)
				{
					uIComponent2.SimulateClick();
				}
			}
		}
	}

	private UIPanel optionsBar
	{
		get
		{
			if (this.m_optionsBar == null)
			{
				this.m_optionsBar = UIView.Find<UIPanel>("OptionsBar");
			}
			return this.m_optionsBar;
		}
	}

	private UIComponent currentlyVisibleOptionPanel
	{
		get
		{
			if (this.m_currentlyVisibleOptionPanel == null || !this.m_currentlyVisibleOptionPanel.get_isVisible())
			{
				foreach (UIComponent current in this.optionsBar.get_components())
				{
					if (current.get_isVisible())
					{
						this.m_currentlyVisibleOptionPanel = current;
						break;
					}
				}
			}
			return this.m_currentlyVisibleOptionPanel;
		}
	}

	private UIPanel m_optionsBar;

	private UIComponent m_currentlyVisibleOptionPanel;
}
