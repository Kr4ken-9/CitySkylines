﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct TreeInstance
{
	public void RenderInstance(RenderManager.CameraInfo cameraInfo, uint treeID, int layerMask)
	{
		if (this.GrowState == 0 || (this.m_flags & 4) != 0)
		{
			return;
		}
		TreeInfo info = this.Info;
		Vector3 position = this.Position;
		if (!cameraInfo.Intersect(position, info.m_generatedInfo.m_size.y * info.m_maxScale))
		{
			return;
		}
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float scale = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float brightness = info.m_minBrightness + (float)randomizer.Int32(10000u) * (info.m_maxBrightness - info.m_minBrightness) * 0.0001f;
		TreeInstance.RenderInstance(cameraInfo, info, position, scale, brightness);
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, TreeInfo info, Vector3 position, float scale, float brightness)
	{
		if (info.m_prefabInitialized)
		{
			if (cameraInfo == null || info.m_lodMesh1 == null || cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
			{
				TreeManager instance = Singleton<TreeManager>.get_instance();
				MaterialPropertyBlock materialBlock = instance.m_materialBlock;
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(position, Quaternion.get_identity(), new Vector3(scale, scale, scale));
				Color color = info.m_defaultColor * brightness;
				color.a = Singleton<WeatherManager>.get_instance().GetWindSpeed(position);
				materialBlock.Clear();
				materialBlock.SetColor(instance.ID_Color, color);
				TreeManager expr_97_cp_0 = instance;
				expr_97_cp_0.m_drawCallData.m_defaultCalls = expr_97_cp_0.m_drawCallData.m_defaultCalls + 1;
				Graphics.DrawMesh(info.m_mesh, matrix4x, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
			}
			else
			{
				position.y += info.m_generatedInfo.m_center.y * (scale - 1f);
				Color color2 = info.m_defaultColor * brightness;
				color2.a = Singleton<WeatherManager>.get_instance().GetWindSpeed(position);
				info.m_lodLocations[info.m_lodCount] = new Vector4(position.x, position.y, position.z, scale);
				info.m_lodColors[info.m_lodCount] = color2.get_linear();
				info.m_lodMin = Vector3.Min(info.m_lodMin, position);
				info.m_lodMax = Vector3.Max(info.m_lodMax, position);
				if (++info.m_lodCount == info.m_lodLocations.Length)
				{
					TreeInstance.RenderLod(cameraInfo, info);
				}
			}
		}
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, TreeInfo info)
	{
		TreeManager instance = Singleton<TreeManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_lodCount <= 1)
		{
			mesh = info.m_lodMesh1;
			num = 1;
		}
		else if (info.m_lodCount <= 4)
		{
			mesh = info.m_lodMesh4;
			num = 4;
		}
		else if (info.m_lodCount <= 8)
		{
			mesh = info.m_lodMesh8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMesh16;
			num = 16;
		}
		for (int i = info.m_lodCount; i < num; i++)
		{
			info.m_lodLocations[i] = cameraInfo.m_forward * -100000f;
			info.m_lodColors[i] = Color.get_black();
		}
		materialBlock.SetVectorArray(instance.ID_TreeLocation, info.m_lodLocations);
		materialBlock.SetVectorArray(instance.ID_TreeColor, info.m_lodColors);
		Bounds bounds = default(Bounds);
		bounds.SetMinMax(info.m_lodMin - new Vector3(100f, 100f, 100f), info.m_lodMax + new Vector3(100f, 100f, 100f));
		mesh.set_bounds(bounds);
		info.m_lodMin = new Vector3(100000f, 100000f, 100000f);
		info.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
		TreeManager expr_17C_cp_0 = instance;
		expr_17C_cp_0.m_drawCallData.m_lodCalls = expr_17C_cp_0.m_drawCallData.m_lodCalls + 1;
		TreeManager expr_18F_cp_0 = instance;
		expr_18F_cp_0.m_drawCallData.m_batchedCalls = expr_18F_cp_0.m_drawCallData.m_batchedCalls + (info.m_lodCount - 1);
		Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_lodMaterial, info.m_prefabDataLayer, null, 0, materialBlock);
		info.m_lodCount = 0;
	}

	public TreeInfo Info
	{
		get
		{
			return PrefabCollection<TreeInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public bool Single
	{
		get
		{
			return (this.m_flags & 16) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 16;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -17);
			}
		}
	}

	public bool FixedHeight
	{
		get
		{
			return (this.m_flags & 32) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 32;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -33);
			}
		}
	}

	public int GrowState
	{
		get
		{
			return (int)((uint)(this.m_flags & 3840) >> 8);
		}
		set
		{
			this.m_flags = (ushort)(((int)this.m_flags & -3841) | Mathf.Clamp(value, 0, 15) << 8);
		}
	}

	public bool Hidden
	{
		get
		{
			return (this.m_flags & 4) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 4;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -5);
			}
		}
	}

	public Vector3 Position
	{
		get
		{
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				Vector3 result;
				result.x = (float)this.m_posX * 0.0164794922f;
				result.y = (float)this.m_posY * 0.015625f;
				result.z = (float)this.m_posZ * 0.0164794922f;
				return result;
			}
			Vector3 result2;
			result2.x = (float)this.m_posX * 0.263671875f;
			result2.y = (float)this.m_posY * 0.015625f;
			result2.z = (float)this.m_posZ * 0.263671875f;
			return result2;
		}
		set
		{
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				this.m_posX = (short)Mathf.Clamp(Mathf.RoundToInt(value.x * 60.68148f), -32767, 32767);
				this.m_posZ = (short)Mathf.Clamp(Mathf.RoundToInt(value.z * 60.68148f), -32767, 32767);
				this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(value.y * 64f), 0, 65535);
			}
			else
			{
				this.m_posX = (short)Mathf.Clamp(Mathf.RoundToInt(value.x * 3.79259253f), -32767, 32767);
				this.m_posZ = (short)Mathf.Clamp(Mathf.RoundToInt(value.z * 3.79259253f), -32767, 32767);
				this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(value.y * 64f), 0, 65535);
			}
		}
	}

	public void CalculateTree(uint treeID)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if ((this.m_flags & 32) == 0)
		{
			Vector3 position = this.Position;
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(position.y * 64f), 0, 65535);
		}
		this.CheckOverlap(treeID);
	}

	private void CheckOverlap(uint treeID)
	{
		TreeInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		ItemClass.CollisionType collisionType;
		if ((this.m_flags & 32) == 0)
		{
			collisionType = ItemClass.CollisionType.Terrain;
		}
		else
		{
			collisionType = ItemClass.CollisionType.Elevated;
		}
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float num = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num2 = info.m_generatedInfo.m_size.y * num;
		Vector3 position = this.Position;
		float y = position.y;
		float maxY = position.y + num2;
		float num3 = (!this.Single) ? 4.5f : 0.3f;
		Quad2 quad = default(Quad2);
		Vector2 vector = VectorUtils.XZ(position);
		quad.a = vector + new Vector2(-num3, -num3);
		quad.b = vector + new Vector2(-num3, num3);
		quad.c = vector + new Vector2(num3, num3);
		quad.d = vector + new Vector2(num3, -num3);
		bool flag = false;
		if (Singleton<NetManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0))
		{
			flag = true;
		}
		if (Singleton<BuildingManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0))
		{
			flag = true;
		}
		if (flag)
		{
			this.GrowState = 0;
		}
		else if (this.GrowState == 0)
		{
			this.GrowState = 1;
		}
	}

	public void UpdateTree(uint treeID)
	{
		if ((this.m_flags & 1) == 0)
		{
			return;
		}
		Vector3 position = this.Position;
		TreeInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		if (info.m_createRuining)
		{
			float minX = position.x - 4f;
			float minZ = position.z - 4f;
			float maxX = position.x + 4f;
			float maxZ = position.z + 4f;
			TerrainModify.UpdateArea(minX, minZ, maxX, maxZ, false, true, false);
		}
		Singleton<NaturalResourceManager>.get_instance().TreesModified(position);
	}

	public void TerrainUpdated(uint treeID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if (this.GrowState != 0)
		{
			TreeInstance.TerrainUpdated(this.Info, this.Position);
		}
	}

	public static void TerrainUpdated(TreeInfo info, Vector3 position)
	{
		if (info == null)
		{
			return;
		}
		if (info.m_createRuining)
		{
			Vector3 a = position + new Vector3(-4.5f, 0f, -4.5f);
			Vector3 b = position + new Vector3(-4.5f, 0f, 4.5f);
			Vector3 c = position + new Vector3(4.5f, 0f, 4.5f);
			Vector3 d = position + new Vector3(4.5f, 0f, -4.5f);
			TerrainModify.Edges edges = TerrainModify.Edges.All;
			TerrainModify.Heights heights = TerrainModify.Heights.None;
			TerrainModify.Surface surface = TerrainModify.Surface.Ruined;
			TerrainModify.ApplyQuad(a, b, c, d, edges, heights, surface);
		}
	}

	public void AfterTerrainUpdated(uint treeID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if ((this.m_flags & 32) == 0)
		{
			Vector3 position = this.Position;
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			ushort num = (ushort)Mathf.Clamp(Mathf.RoundToInt(position.y * 64f), 0, 65535);
			if (num != this.m_posY)
			{
				int growState = this.GrowState;
				this.m_posY = num;
				this.CheckOverlap(treeID);
				int growState2 = this.GrowState;
				if (growState2 != growState)
				{
					Singleton<TreeManager>.get_instance().UpdateTree(treeID);
				}
				else if (growState2 != 0)
				{
					Singleton<TreeManager>.get_instance().UpdateTreeRenderer(treeID, true);
				}
			}
		}
	}

	public bool OverlapQuad(uint treeID, Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType)
	{
		if (this.Hidden)
		{
			return false;
		}
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		float num = 0.3f;
		Vector3 position = this.Position;
		if (position.x - num > vector2.x || position.x + num < vector.x)
		{
			return false;
		}
		if (position.z - num > vector2.y || position.z + num < vector.y)
		{
			return false;
		}
		TreeInfo info = this.Info;
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float num2 = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num3 = info.m_generatedInfo.m_size.y * num2;
		ItemClass.CollisionType collisionType2 = ItemClass.CollisionType.Terrain;
		if ((this.m_flags & 32) != 0)
		{
			collisionType2 = ItemClass.CollisionType.Elevated;
		}
		float y = position.y;
		float maxY2 = position.y + num3;
		if (!ItemClass.CheckCollisionType(minY, maxY, y, maxY2, collisionType, collisionType2))
		{
			return false;
		}
		Vector2 vector3 = VectorUtils.XZ(position);
		Quad2 quad2 = default(Quad2);
		quad2.a = vector3 + new Vector2(-num, num);
		quad2.b = vector3 + new Vector2(num, num);
		quad2.c = vector3 + new Vector2(num, -num);
		quad2.d = vector3 + new Vector2(-num, -num);
		return quad.Intersect(quad2);
	}

	public bool RayCast(uint treeID, Segment3 ray, out float t, out float targetSqr)
	{
		t = 2f;
		targetSqr = 0f;
		if (this.GrowState == 0)
		{
			return false;
		}
		TreeInfo info = this.Info;
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float num = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num2 = info.m_generatedInfo.m_size.y * num;
		float num3 = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * num * 0.5f;
		Vector3 position = this.Position;
		Bounds bounds;
		bounds..ctor(new Vector3(position.x, position.y + num2 * 0.5f, position.z), new Vector3(num3, num2, num3));
		if (!bounds.IntersectRay(new Ray(ray.a, ray.b - ray.a)))
		{
			return false;
		}
		float num4 = (info.m_generatedInfo.m_size.x + info.m_generatedInfo.m_size.z) * num * 0.125f;
		float num5 = Mathf.Min(num4, num2 * 0.45f);
		Segment3 segment;
		segment..ctor(position, position);
		segment.a.y = segment.a.y + num5;
		segment.b.y = segment.b.y + (num2 - num5);
		bool result = false;
		float num7;
		float num8;
		float num6 = ray.DistanceSqr(segment, ref num7, ref num8);
		if (num6 < num4 * num4)
		{
			t = num7;
			targetSqr = num6;
			result = true;
		}
		if (Segment1.Intersect(ray.a.y, ray.b.y, position.y, ref num7))
		{
			num4 = num3;
			num6 = Vector3.SqrMagnitude(ray.Position(num7) - position);
			if (num6 < num4 * num4 && num7 < t)
			{
				t = num7;
				targetSqr = num6;
				result = true;
			}
		}
		return result;
	}

	public bool CalculateGroupData(uint treeID, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return this.GrowState != 0 && !this.Hidden && TreeInstance.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
	}

	public static bool CalculateGroupData(ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		vertexCount += 4;
		triangleCount += 6;
		objectCount++;
		vertexArrays |= (RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Colors);
		return true;
	}

	public void PopulateGroupData(uint treeID, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		int growState = this.GrowState;
		if (growState == 0 || this.Hidden)
		{
			return;
		}
		TreeInfo info = this.Info;
		Vector3 position = this.Position;
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float scale = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float brightness = info.m_minBrightness + (float)randomizer.Int32(10000u) * (info.m_maxBrightness - info.m_minBrightness) * 0.0001f;
		TreeInstance.PopulateGroupData(info, position, scale, brightness, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
	}

	public static void PopulateGroupData(TreeInfo info, Vector3 position, float scale, float brightness, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		float num = info.m_generatedInfo.m_size.y * scale;
		float num2 = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * scale * 0.5f;
		min = Vector3.Min(min, position - new Vector3(num2, 0f, num2));
		max = Vector3.Max(max, position + new Vector3(num2, num, num2));
		maxRenderDistance = Mathf.Max(maxRenderDistance, 30000f);
		maxInstanceDistance = Mathf.Max(maxInstanceDistance, 425f);
		Color32 color = (info.m_defaultColor * brightness).get_linear();
		color.a = (byte)Mathf.Clamp(Mathf.RoundToInt(Singleton<WeatherManager>.get_instance().GetWindSpeed(position) * 128f), 0, 255);
		position -= groupPosition;
		position.y += info.m_generatedInfo.m_center.y * scale;
		data.m_vertices[vertexIndex] = position + new Vector3(info.m_renderUv0B.x * scale, info.m_renderUv0B.y * scale, 0f);
		data.m_uvs[vertexIndex] = info.m_renderUv0;
		data.m_uvs2[vertexIndex] = info.m_renderUv0B * scale;
		data.m_colors[vertexIndex] = color;
		vertexIndex++;
		data.m_vertices[vertexIndex] = position + new Vector3(info.m_renderUv1B.x * scale, info.m_renderUv1B.y * scale, 0f);
		data.m_uvs[vertexIndex] = info.m_renderUv1;
		data.m_uvs2[vertexIndex] = info.m_renderUv1B * scale;
		data.m_colors[vertexIndex] = color;
		vertexIndex++;
		data.m_vertices[vertexIndex] = position + new Vector3(info.m_renderUv2B.x * scale, info.m_renderUv2B.y * scale, 0f);
		data.m_uvs[vertexIndex] = info.m_renderUv2;
		data.m_uvs2[vertexIndex] = info.m_renderUv2B * scale;
		data.m_colors[vertexIndex] = color;
		vertexIndex++;
		data.m_vertices[vertexIndex] = position + new Vector3(info.m_renderUv3B.x * scale, info.m_renderUv3B.y * scale, 0f);
		data.m_uvs[vertexIndex] = info.m_renderUv3;
		data.m_uvs2[vertexIndex] = info.m_renderUv3B * scale;
		data.m_colors[vertexIndex] = color;
		vertexIndex++;
		data.m_triangles[triangleIndex++] = vertexIndex - 4;
		data.m_triangles[triangleIndex++] = vertexIndex - 3;
		data.m_triangles[triangleIndex++] = vertexIndex - 2;
		data.m_triangles[triangleIndex++] = vertexIndex - 2;
		data.m_triangles[triangleIndex++] = vertexIndex - 3;
		data.m_triangles[triangleIndex++] = vertexIndex - 1;
	}

	public uint m_nextGridTree;

	public short m_posX;

	public short m_posZ;

	public ushort m_posY;

	public ushort m_flags;

	public ushort m_infoIndex;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Hidden = 4,
		Single = 16,
		FixedHeight = 32,
		FireDamage = 64,
		Burning = 128,
		GrowState = 3840,
		All = 65535
	}
}
