﻿using System;
using ColossalFramework;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterAssetType : AssetImporterPanelBase
{
	private UITabstrip infoViewButton
	{
		get
		{
			if (this.m_InfoViewButton == null)
			{
				this.m_InfoViewButton = UIView.Find<UITabstrip>("InfoMenu");
			}
			return this.m_InfoViewButton;
		}
	}

	public void Reset()
	{
		this.m_ContinueButton.set_isEnabled(false);
		if (this.m_Selected != null)
		{
			this.m_Selected.set_state(0);
		}
		this.m_Selected = null;
	}

	private void Awake()
	{
		this.m_ContinueButton = base.Find<UIButton>("Continue");
		this.m_ContinueButton.add_eventClick(new MouseEventHandler(this.OnContinue));
		this.m_ContinueButton.set_isEnabled(false);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.m_ContinueButton.set_isEnabled(this.m_Selected != null);
		}
	}

	public void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != null && !string.IsNullOrEmpty(p.get_source().get_stringUserData()))
		{
			if (this.m_Selected != null)
			{
				this.m_Selected.set_state(0);
			}
			this.m_Selected = (p.get_source() as UIButton);
			if (this.m_Selected != null && Singleton<AudioManager>.get_exists() && this.m_SelectionSound != null)
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_SelectionSound, 1f);
			}
			this.m_ContinueButton.set_isEnabled(this.m_Selected != null);
		}
	}

	private void OnLostFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_lostFocus() == this.m_Selected)
		{
			this.ShowSelection();
		}
	}

	private void ShowSelection()
	{
		if (this.m_Selected != null && !this.m_Selected.get_containsMouse())
		{
			this.m_Selected.set_state(1);
		}
	}

	protected void OnMouseUp(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != this.m_Selected && p.get_source() != UIInput.get_hoveredComponent())
		{
			p.get_source().Unfocus();
		}
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 13)
		{
			this.m_ContinueButton.SimulateClick();
			p.Use();
		}
	}

	private void SetupPrefab(BuildingInfo info, string thumbSprite, string infoSprite, ItemClass.Availability availability)
	{
		BuildingInfo buildingInfo = Object.Instantiate<BuildingInfo>(info);
		buildingInfo.set_name(info.get_name());
		buildingInfo.InitializePrefab();
		buildingInfo.CheckReferences();
		buildingInfo.m_prefabInitialized = true;
		buildingInfo.get_gameObject().SetActive(false);
		buildingInfo.m_availableIn = EnumExtensions.SetFlags<ItemClass.Availability>(buildingInfo.m_availableIn, availability);
		Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier = Guid.NewGuid().ToString();
		Singleton<SimulationManager>.get_instance().m_metaData.m_WorkshopPublishedFileId = PublishedFileId.invalid;
		ToolsModifierControl.toolController.m_editPrefabInfo = buildingInfo;
		ToolsModifierControl.toolController.m_templatePrefabInfo = info;
		buildingInfo.m_Atlas = this.m_DefaultSpriteAtlas;
		buildingInfo.m_Thumbnail = thumbSprite;
		buildingInfo.m_InfoTooltipAtlas = this.m_DefaultSpriteAtlas;
		buildingInfo.m_InfoTooltipThumbnail = infoSprite;
	}

	private void OnContinue(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_Selected != null)
		{
			this.infoViewButton.set_isVisible(this.m_Selected.get_stringUserData() == "Intersection");
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
			}
			UIPanel uIPanel = UIView.Find<UIPanel>("InfoViewsPanel");
			if (uIPanel != null && uIPanel.get_isVisible())
			{
				uIPanel.Find<UIButton>("CloseButton").SimulateClick();
			}
			if (this.m_Selected.get_stringUserData() == "Intersection")
			{
				this.SetupPrefab(this.m_EmptyIntersection, this.m_DefaultIntersectionSprite, this.m_DefaultIntersectionTooltipSprite, ItemClass.Availability.All);
				base.owner.Complete();
			}
			else if (this.m_Selected.get_stringUserData() == "Park")
			{
				this.SetupPrefab(this.m_EmptyPark, this.m_DefaultParkSprite, this.m_DefaultParkTooltipSprite, ItemClass.Availability.Game);
				base.owner.Complete();
			}
			else if (this.m_Selected.get_stringUserData() == "Building")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.Default;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Buildings);
			}
			else if (this.m_Selected.get_stringUserData() == "Prop")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.NoPreview;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Props);
			}
			else if (this.m_Selected.get_stringUserData() == "Tree")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.NoPreview;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Trees);
			}
			else if (this.m_Selected.get_stringUserData() == "Vehicle")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.Default;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Vehicles);
			}
			else if (this.m_Selected.get_stringUserData() == "Citizen")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.Default;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Citizens);
			}
			else if (this.m_Selected.get_stringUserData() == "Road")
			{
				base.owner.assetTemplatePanel.Reset();
				base.owner.assetTemplatePanel.templateMode = AssetImporterAssetTemplate.TemplateMode.Default;
				base.owner.GoTo(base.owner.assetTemplatePanel);
				base.owner.assetTemplatePanel.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Roads);
			}
		}
	}

	public AudioClip m_SelectionSound;

	public BuildingInfo m_EmptyIntersection;

	public BuildingInfo m_EmptyPark;

	protected UIButton m_Selected;

	private UIButton m_ContinueButton;

	public PropInfo m_PropPrefabTemplate;

	public TreeInfo m_TreePrefabTemplate;

	public UITextureAtlas m_DefaultSpriteAtlas;

	[UISprite("m_DefaultSpriteAtlas")]
	public string m_DefaultIntersectionSprite;

	[UISprite("m_DefaultSpriteAtlas")]
	public string m_DefaultParkSprite;

	[UISprite("m_DefaultSpriteAtlas")]
	public string m_DefaultIntersectionTooltipSprite;

	[UISprite("m_DefaultSpriteAtlas")]
	public string m_DefaultParkTooltipSprite;

	private UITabstrip m_InfoViewButton;
}
