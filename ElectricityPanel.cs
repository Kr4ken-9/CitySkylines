﻿using System;
using ColossalFramework.UI;

public sealed class ElectricityPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Electricity;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)3);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		NetInfo netInfo = objectUserData as NetInfo;
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (netInfo != null)
		{
			base.ShowPowerLinesOptionPanel();
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				netTool.Prefab = netInfo;
			}
		}
		if (buildingInfo != null)
		{
			base.HidePowerLinesOptionPanel();
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
