﻿using System;
using ColossalFramework;
using UnityEngine;

public class StructureFireAI : DisasterAI
{
	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			InstanceID id = default(InstanceID);
			id.Disaster = disasterID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
			ushort num = StructureFireAI.FindClosestBuilding(data.m_targetPosition);
			if (num != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if (info.m_buildingAI.BurnBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, false))
				{
					data.m_targetPosition = instance.m_buildings.m_buffer[(int)num].m_position;
					base.ActivateNow(disasterID, ref data);
				}
			}
		}
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = (uint)((uint)(data.m_intensity + 20) << 8);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num2 < num;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = (uint)((uint)(data.m_intensity + 20) << 8);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return (int)(1600u / (1u + num2 / (uint)Mathf.Max(1, (int)num >> 4)));
	}

	private static ushort FindClosestBuilding(Vector3 pos)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num = Mathf.Max((int)(pos.x / 64f + 135f), 0);
		int num2 = Mathf.Max((int)(pos.z / 64f + 135f), 0);
		int num3 = Mathf.Min((int)(pos.x / 64f + 135f), 269);
		int num4 = Mathf.Min((int)(pos.z / 64f + 135f), 269);
		int num5 = num + 1;
		int num6 = num2 + 1;
		int num7 = num3 - 1;
		int num8 = num4 - 1;
		ushort num9 = 0;
		float num10 = 1E+12f;
		float num11 = 0f;
		while (num != num5 || num2 != num6 || num3 != num7 || num4 != num8)
		{
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					if (j >= num5 && i >= num6 && j <= num7 && i <= num8)
					{
						j = num7;
					}
					else
					{
						ushort num12 = instance.m_buildingGrid[i * 270 + j];
						int num13 = 0;
						while (num12 != 0)
						{
							if ((instance.m_buildings.m_buffer[(int)num12].m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Collapsed)) == Building.Flags.Created && instance.m_buildings.m_buffer[(int)num12].m_fireIntensity == 0 && instance.m_buildings.m_buffer[(int)num12].GetLastFrameData().m_fireDamage == 0)
							{
								BuildingInfo info = instance.m_buildings.m_buffer[(int)num12].Info;
								if (info.m_buildingAI.BurnBuilding(num12, ref instance.m_buildings.m_buffer[(int)num12], null, true))
								{
									Vector3 position = instance.m_buildings.m_buffer[(int)num12].m_position;
									float num14 = Vector3.SqrMagnitude(position - pos);
									if (num14 < num10)
									{
										num9 = num12;
										num10 = num14;
									}
								}
							}
							num12 = instance.m_buildings.m_buffer[(int)num12].m_nextGridBuilding;
							if (++num13 >= 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			if (num9 != 0 && num10 <= num11 * num11)
			{
				return num9;
			}
			num11 += 64f;
			num5 = num;
			num6 = num2;
			num7 = num3;
			num8 = num4;
			num = Mathf.Max(num - 1, 0);
			num2 = Mathf.Max(num2 - 1, 0);
			num3 = Mathf.Min(num3 + 1, 269);
			num4 = Mathf.Min(num4 + 1, 269);
		}
		return num9;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}
}
