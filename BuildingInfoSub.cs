﻿using System;
using ColossalFramework;
using UnityEngine;

public class BuildingInfoSub : BuildingInfoBase
{
	public override void InitializePrefab()
	{
		if (this.m_referenceCount++ == 0)
		{
			base.InitializePrefab();
			if (this.m_generatedInfo == null)
			{
				throw new PrefabException(this, "Generated info missing");
			}
			Renderer component = base.GetComponent<Renderer>();
			MeshFilter component2 = base.GetComponent<MeshFilter>();
			if (component2 != null || component != null)
			{
				if (component2 == null)
				{
					throw new PrefabException(this, "MeshFilter missing");
				}
				if (component == null)
				{
					throw new PrefabException(this, "Renderer missing");
				}
				this.m_mesh = component2.get_sharedMesh();
				this.m_material = component.get_sharedMaterial();
				if (this.m_mesh == null)
				{
					throw new PrefabException(this, "Mesh missing");
				}
				if (this.m_material == null)
				{
					throw new PrefabException(this, "Material missing");
				}
				if (this.m_generatedInfo.m_buildingInfo != null)
				{
					if (this.m_generatedInfo.m_buildingInfo.m_mesh != this.m_mesh)
					{
						throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_buildingInfo.get_gameObject().get_name() + ")");
					}
				}
				else
				{
					this.m_generatedInfo.m_buildingInfo = this;
				}
				Texture2D texture2D = this.m_material.get_mainTexture() as Texture2D;
				if (texture2D != null && texture2D.get_format() == 12)
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "Diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
				if (this.m_lodObject != null)
				{
					component2 = this.m_lodObject.GetComponent<MeshFilter>();
					if (component2 == null)
					{
						throw new PrefabException(this, "LOD MeshFilter missing");
					}
					if (this.m_lodObject.GetComponent<Renderer>() == null)
					{
						throw new PrefabException(this, "LOD Renderer missing");
					}
					this.m_lodMesh = component2.get_sharedMesh();
					this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
					if (this.m_lodMesh == null)
					{
						throw new PrefabException(this, "LOD Mesh missing");
					}
					if (this.m_lodMaterial == null)
					{
						throw new PrefabException(this, "LOD Material missing");
					}
					if (this.m_lodMaterial.get_shader() != this.m_material.get_shader() && !this.m_lodHasDifferentShader)
					{
						CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD has different shader: " + base.get_gameObject().get_name(), base.get_gameObject());
					}
				}
				else
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
				base.InitMeshInfo(this.m_generatedInfo.m_max - this.m_generatedInfo.m_min);
				if (this.m_generatedInfo.m_baseArea == null || this.m_generatedInfo.m_baseArea.Length == 0)
				{
					this.m_noBase = true;
				}
			}
		}
	}

	public override void DestroyPrefab()
	{
		if (--this.m_referenceCount == 0)
		{
			base.DestroyMeshInfo();
			base.DestroyPrefab();
		}
	}

	public override void RefreshLevelOfDetail()
	{
		if (this.m_mesh != null)
		{
			base.RefreshLevelOfDetail(this.m_generatedInfo.m_max - this.m_generatedInfo.m_min);
		}
	}

	[NonSerialized]
	private int m_referenceCount;
}
