﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

[Serializable]
public class CombinedMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		int num = 0;
		if (this.m_requirePassed != null)
		{
			if (string.IsNullOrEmpty(this.m_customPassedID))
			{
				for (int i = 0; i < this.m_requirePassed.Length; i++)
				{
					if (!this.m_requirePassed[i].m_hidden)
					{
						num++;
					}
				}
			}
			else
			{
				num++;
			}
		}
		if (!this.m_hidden && this.m_delayWeeks != 0)
		{
			num++;
		}
		if (!this.m_hidden && num > 1)
		{
			if (totalProgress.m_description == null)
			{
				this.GetPassedProgressInfo(ref totalProgress);
			}
			else
			{
				subProgress.EnsureCapacity(subProgress.m_size + 1);
				this.GetPassedProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
				subProgress.m_size++;
			}
		}
		if (this.m_requirePassed != null)
		{
			if (string.IsNullOrEmpty(this.m_customPassedID))
			{
				for (int j = 0; j < this.m_requirePassed.Length; j++)
				{
					if (!this.m_requirePassed[j].m_hidden)
					{
						this.m_requirePassed[j].GetLocalizedProgressImpl(this.m_maxPassedCount, ref totalProgress, subProgress);
					}
				}
			}
			else if (totalProgress.m_description == null)
			{
				this.GetCustomProgressInfo(ref totalProgress);
			}
			else
			{
				subProgress.EnsureCapacity(subProgress.m_size + 1);
				this.GetCustomProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
				subProgress.m_size++;
			}
		}
		if (!this.m_hidden && this.m_delayWeeks != 0)
		{
			if (totalProgress.m_description == null)
			{
				this.GetTimeProgressInfo(ref totalProgress);
			}
			else
			{
				subProgress.EnsureCapacity(subProgress.m_size + 1);
				this.GetTimeProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
				subProgress.m_size++;
			}
		}
	}

	private void GetPassedProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int num = this.m_requirePassedLimit + (this.m_delayWeeks << 12);
		if (this.m_forbidPassed != null && this.m_forbidPassed.Length != 0)
		{
			num += this.m_forbidPassedLimit;
		}
		info.m_min = 0f;
		info.m_max = (float)num;
		long num2;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num2 = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num2 = 0L;
		}
		if (num2 > (long)num)
		{
			num2 = (long)num;
		}
		if (this.m_delayWeeks != 0)
		{
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_PASSED_TIME_DESC"), new object[]
			{
				this.m_requirePassedLimit,
				this.m_delayWeeks
			});
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_PASSED_TIME_PROG"), num2 * 100L / (long)num);
		}
		else
		{
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_PASSED_DESC"), num);
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_PASSED_PROG"), new object[]
			{
				num2,
				num
			});
		}
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	private void GetCustomProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_requirePassedLimit;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)this.m_requirePassedLimit)
		{
			num = (long)this.m_requirePassedLimit;
		}
		if (!info.m_passed && num == (long)this.m_requirePassedLimit)
		{
			num -= 1L;
			info.m_current -= 1f;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_CUSTOM_DESC", this.m_customPassedID), this.m_requirePassedLimit);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_CUSTOM_PROG", this.m_customPassedID), new object[]
		{
			num,
			this.m_requirePassedLimit
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	private void GetTimeProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_delayWeeks;
		int num;
		if (data != null)
		{
			if (data.m_passedCount != 0)
			{
				info.m_passed = true;
				info.m_current = (float)this.m_delayWeeks;
				num = this.m_delayWeeks;
			}
			else if (this.m_startFrame != 0u)
			{
				num = Mathf.Min(this.m_delayWeeks - 1, (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex - this.m_startFrame >> 12));
				info.m_passed = false;
				info.m_current = (float)num;
			}
			else
			{
				info.m_passed = false;
				info.m_current = 0f;
				num = 0;
			}
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_TIME_DESC"), this.m_delayWeeks);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_COMBINED_TIME_PROG"), new object[]
		{
			num,
			this.m_delayWeeks
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		int num = 0;
		if (this.m_requirePassed != null)
		{
			for (int i = 0; i < this.m_requirePassed.Length; i++)
			{
				num = Mathf.Max(num, this.m_requirePassed[i].GetComparisonValue());
			}
		}
		return num;
	}

	public override MilestoneInfo.Data GetData()
	{
		if (this.m_data == null)
		{
			CombinedMilestone.CombinedData combinedData = new CombinedMilestone.CombinedData();
			if (this.m_requirePassed != null && this.m_requirePassed.Length != 0)
			{
				combinedData.m_requirePassed = new MilestoneInfo.Data[this.m_requirePassed.Length];
				for (int i = 0; i < this.m_requirePassed.Length; i++)
				{
					combinedData.m_requirePassed[i] = this.m_requirePassed[i].GetData();
				}
			}
			if (this.m_forbidPassed != null && this.m_forbidPassed.Length != 0)
			{
				combinedData.m_forbidPassed = new MilestoneInfo.Data[this.m_forbidPassed.Length];
				for (int j = 0; j < this.m_forbidPassed.Length; j++)
				{
					combinedData.m_forbidPassed[j] = this.m_forbidPassed[j].GetData();
				}
			}
			this.m_data = combinedData;
			this.m_data.m_isAssigned = true;
			combinedData.m_startFrame = this.m_startFrame;
		}
		else
		{
			CombinedMilestone.CombinedData combinedData2 = this.m_data as CombinedMilestone.CombinedData;
			if (combinedData2 != null)
			{
				combinedData2.m_startFrame = this.m_startFrame;
			}
		}
		return this.m_data;
	}

	public override void SetData(MilestoneInfo.Data data)
	{
		if (data == null || data.m_isAssigned)
		{
			return;
		}
		data.m_isAssigned = true;
		CombinedMilestone.CombinedData combinedData = data as CombinedMilestone.CombinedData;
		if (combinedData != null)
		{
			this.m_startFrame = combinedData.m_startFrame;
			int num;
			if (this.m_requirePassed != null)
			{
				num = this.m_requirePassed.Length;
			}
			else
			{
				num = 0;
			}
			int num2;
			if (this.m_forbidPassed != null)
			{
				num2 = this.m_forbidPassed.Length;
			}
			else
			{
				num2 = 0;
			}
			int num3;
			if (combinedData.m_requirePassed != null)
			{
				num3 = combinedData.m_requirePassed.Length;
			}
			else
			{
				num3 = 0;
			}
			int num4;
			if (combinedData.m_forbidPassed != null)
			{
				num4 = combinedData.m_forbidPassed.Length;
			}
			else
			{
				num4 = 0;
			}
			if (num == num3 && num2 == num4)
			{
				if (this.m_data == null)
				{
					this.m_data = combinedData;
				}
				else
				{
					this.m_data.m_passedCount = combinedData.m_passedCount;
					this.m_data.m_progress = combinedData.m_progress;
				}
				this.m_written = true;
				for (int i = 0; i < num; i++)
				{
					this.m_requirePassed[i].SetData(combinedData.m_requirePassed[i]);
					if (this.m_data == combinedData)
					{
						combinedData.m_requirePassed[i] = this.m_requirePassed[i].GetData();
					}
				}
				for (int j = 0; j < num2; j++)
				{
					this.m_forbidPassed[j].SetData(combinedData.m_forbidPassed[j]);
					if (this.m_data == combinedData)
					{
						combinedData.m_forbidPassed[j] = this.m_forbidPassed[j].GetData();
					}
				}
				return;
			}
		}
		if (data != null)
		{
			this.m_startFrame = 0u;
			MilestoneInfo.Data data2 = this.GetData();
			data2.m_passedCount = data.m_passedCount;
			data2.m_progress = data.m_progress;
			this.m_written = true;
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		int num = 0;
		int num2 = 0;
		if (this.m_requirePassed != null)
		{
			for (int i = 0; i < this.m_requirePassed.Length; i++)
			{
				num += Mathf.Min(this.m_maxPassedCount, Singleton<UnlockManager>.get_instance().CheckMilestone(this.m_requirePassed[i], forceUnlock, forceRelock));
			}
		}
		int num3 = 0;
		if (this.m_forbidPassed != null && this.m_forbidPassed.Length != 0)
		{
			for (int j = 0; j < this.m_forbidPassed.Length; j++)
			{
				num2 += Mathf.Min(this.m_maxPassedCount, Singleton<UnlockManager>.get_instance().CheckMilestone(this.m_forbidPassed[j], false, false));
			}
			num3 = Mathf.Max(0, this.m_forbidPassed.Length - this.m_forbidPassedLimit + 1);
		}
		if (forceUnlock)
		{
			data.m_progress = (long)(this.m_requirePassedLimit + num3 + (this.m_delayWeeks << 12));
			data.m_passedCount = 1;
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			long num4 = 0L;
			if (this.m_requirePassedLimit > 0)
			{
				num4 += (long)Mathf.Min(this.m_requirePassedLimit, num);
			}
			if (num3 > 0)
			{
				num4 += (long)(num3 - Mathf.Min(num3, num2));
			}
			if (num4 < (long)(this.m_requirePassedLimit + num3))
			{
				this.m_startFrame = 0u;
				data.m_progress = num4;
				data.m_passedCount = 0;
			}
			else if (this.m_delayWeeks != 0)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				if (this.m_startFrame == 0u)
				{
					this.m_startFrame = currentFrameIndex;
				}
				if (currentFrameIndex - this.m_startFrame >= (uint)((uint)this.m_delayWeeks << 12))
				{
					data.m_progress = (long)(this.m_requirePassedLimit + num3 + (this.m_delayWeeks << 12));
					data.m_passedCount = 1;
				}
				else
				{
					num4 += (long)((ulong)(currentFrameIndex - this.m_startFrame));
					data.m_progress = num4;
					data.m_passedCount = 0;
				}
			}
			else
			{
				data.m_progress = num4;
				data.m_passedCount = 1;
			}
		}
		return data;
	}

	public override void ResetWrittenStatus()
	{
		base.ResetWrittenStatus();
		if (this.m_requirePassed != null)
		{
			for (int i = 0; i < this.m_requirePassed.Length; i++)
			{
				this.m_requirePassed[i].ResetWrittenStatus();
			}
		}
		if (this.m_forbidPassed != null)
		{
			for (int j = 0; j < this.m_forbidPassed.Length; j++)
			{
				this.m_forbidPassed[j].ResetWrittenStatus();
			}
		}
	}

	public override void ResetImpl(int maxPassCount, bool directReference, bool onlyUnwritten)
	{
		base.ResetImpl(maxPassCount, directReference, onlyUnwritten);
		if (!this.m_written || !onlyUnwritten)
		{
			this.m_startFrame = 0u;
		}
		if (this.m_requirePassed != null)
		{
			for (int i = 0; i < this.m_requirePassed.Length; i++)
			{
				this.m_requirePassed[i].ResetImpl(this.m_maxPassedCount, false, onlyUnwritten);
			}
		}
		if (this.m_forbidPassed != null)
		{
			for (int j = 0; j < this.m_forbidPassed.Length; j++)
			{
				this.m_forbidPassed[j].ResetImpl(this.m_maxPassedCount, false, onlyUnwritten);
			}
		}
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 0f;
		float num2 = 0f;
		if (this.m_requirePassed != null)
		{
			for (int i = 0; i < this.m_requirePassed.Length; i++)
			{
				if (!this.m_requirePassed[i].m_hidden)
				{
					num += this.m_requirePassed[i].GetProgress();
					num2 += 1f;
				}
			}
		}
		if (this.m_forbidPassed != null)
		{
			for (int j = 0; j < this.m_forbidPassed.Length; j++)
			{
				if (!this.m_forbidPassed[j].m_hidden && this.m_forbidPassed[j].GetProgress() == 1f)
				{
					num = 0f;
				}
			}
		}
		if (num2 != 0f)
		{
			num /= num2;
		}
		if (this.m_delayWeeks != 0)
		{
			uint num3 = this.m_startFrame;
			uint referenceFrameIndex = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex;
			if (num3 == 0u)
			{
				num3 = referenceFrameIndex;
			}
			float num4 = (referenceFrameIndex - num3) / (float)(this.m_delayWeeks << 12);
			num = Mathf.Min(num, Mathf.Clamp01(num4));
		}
		return num;
	}

	public MilestoneInfo[] m_requirePassed;

	public MilestoneInfo[] m_forbidPassed;

	public int m_maxPassedCount = 1;

	public int m_requirePassedLimit = 1;

	public int m_forbidPassedLimit = 1;

	public int m_delayWeeks;

	public string m_customPassedID;

	private uint m_startFrame;

	public class CombinedData : MilestoneInfo.Data
	{
		public override void Serialize(DataSerializer s)
		{
			base.Serialize(s);
			s.WriteObjectArray<MilestoneInfo.Data>(this.m_requirePassed);
			s.WriteObjectArray<MilestoneInfo.Data>(this.m_forbidPassed);
			s.WriteUInt32(this.m_startFrame);
		}

		public override void Deserialize(DataSerializer s)
		{
			base.Deserialize(s);
			this.m_requirePassed = s.ReadObjectArray<MilestoneInfo.Data>();
			this.m_forbidPassed = s.ReadObjectArray<MilestoneInfo.Data>();
			if (s.get_version() >= 141u)
			{
				this.m_startFrame = s.ReadUInt32();
			}
			else
			{
				this.m_startFrame = 0u;
			}
		}

		public override void AfterDeserialize(DataSerializer s)
		{
			base.AfterDeserialize(s);
		}

		public MilestoneInfo.Data[] m_requirePassed;

		public MilestoneInfo.Data[] m_forbidPassed;

		public uint m_startFrame;
	}
}
