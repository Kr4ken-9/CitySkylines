﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class FireSafetyInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_SafetyMeter = base.Find<UISlider>("SafetyMeter");
		this.m_RateGradient = base.Find<UITextureSprite>("RateGradient");
		this.m_CoverageGradient = base.Find<UITextureSprite>("CoverageGradient");
		this.m_SafetyGradient = base.Find<UITextureSprite>("SafetyGradient");
		this.m_SafetyLabel = base.Find<UILabel>("SafetyLabel");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[18].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[18].m_inactiveColor);
					this.m_RateGradient.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[18].m_targetColor);
					this.m_RateGradient.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[18].m_negativeColor);
					this.m_SafetyGradient.get_renderMaterial().CopyPropertiesFromMaterial(this.m_RateGradient.get_renderMaterial());
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
			}
			this.m_initialized = true;
		}
		int num = 0;
		if (Singleton<ImmaterialResourceManager>.get_exists())
		{
			Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.FireHazard, out num);
		}
		num = Mathf.Clamp(num, 0, 100);
		this.m_SafetyLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_FIRE_SAFETY"),
			" - ",
			num,
			"%"
		}));
		this.m_SafetyMeter.set_value((float)num);
	}

	private UISlider m_SafetyMeter;

	private UILabel m_SafetyLabel;

	private UITextureSprite m_RateGradient;

	private UITextureSprite m_CoverageGradient;

	private UITextureSprite m_SafetyGradient;

	private bool m_initialized;
}
