﻿using System;
using ColossalFramework.UI;

public class DistrictOptionPanel : OptionPanelBase
{
	private void Awake()
	{
		base.HidePanel();
		DistrictTool districtTool = ToolsModifierControl.GetTool<DistrictTool>();
		if (districtTool != null)
		{
			UITabstrip strip = base.get_component() as UITabstrip;
			if (strip != null)
			{
				strip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int index)
				{
					if (index != 0)
					{
						if (index != 1)
						{
							if (index == 2)
							{
								districtTool.m_brushSize = (float)this.m_LargeBrushSize;
							}
						}
						else
						{
							districtTool.m_brushSize = (float)this.m_MediumBrushSize;
						}
					}
					else
					{
						districtTool.m_brushSize = (float)this.m_SmallBrushSize;
					}
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						int selectedIndex = strip.get_selectedIndex();
						if (selectedIndex != 0)
						{
							if (selectedIndex != 1)
							{
								if (selectedIndex == 2)
								{
									districtTool.m_brushSize = (float)this.m_LargeBrushSize;
								}
							}
							else
							{
								districtTool.m_brushSize = (float)this.m_MediumBrushSize;
							}
						}
						else
						{
							districtTool.m_brushSize = (float)this.m_SmallBrushSize;
						}
					}
				});
			}
		}
	}

	private void Start()
	{
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(1);
	}

	public int m_SmallBrushSize = 50;

	public int m_MediumBrushSize = 200;

	public int m_LargeBrushSize = 400;
}
