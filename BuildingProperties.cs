﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class BuildingProperties : MonoBehaviour
{
	protected virtual void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	public static void EnsurePaletteSettings(Texture texture)
	{
		texture.set_wrapMode(1);
		texture.set_filterMode(0);
	}

	public static void EnsureTextureSettings(Texture texture)
	{
		texture.set_wrapMode(0);
		texture.set_filterMode(2);
		texture.set_anisoLevel(4);
	}

	private void OverrideFromMapTheme()
	{
		MapThemeMetaData mapThemeMetaData = Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData;
		if (mapThemeMetaData != null)
		{
			if (mapThemeMetaData.buildingFloorDiffuse != null)
			{
				this.m_floorDiffuse = mapThemeMetaData.buildingFloorDiffuse.Instantiate<Texture2D>();
				BuildingProperties.EnsureTextureSettings(this.m_floorDiffuse);
			}
			if (mapThemeMetaData.buildingBaseDiffuse != null)
			{
				this.m_baseDiffuse = mapThemeMetaData.buildingBaseDiffuse.Instantiate<Texture2D>();
				BuildingProperties.EnsureTextureSettings(this.m_baseDiffuse);
			}
			if (mapThemeMetaData.buildingBaseNormal != null)
			{
				this.m_baseNormal = mapThemeMetaData.buildingBaseNormal.Instantiate<Texture2D>();
				BuildingProperties.EnsureTextureSettings(this.m_baseNormal);
			}
			if (mapThemeMetaData.buildingBurntDiffuse != null)
			{
				this.m_burnedDiffuse = mapThemeMetaData.buildingBurntDiffuse.Instantiate<Texture2D>();
				BuildingProperties.EnsureTextureSettings(this.m_burnedDiffuse);
			}
			if (mapThemeMetaData.buildingAbandonedDiffuse != null)
			{
				this.m_abandonedDiffuse = mapThemeMetaData.buildingAbandonedDiffuse.Instantiate<Texture2D>();
				BuildingProperties.EnsureTextureSettings(this.m_abandonedDiffuse);
			}
			if (mapThemeMetaData.lightColorPalette != null)
			{
				this.m_lightColorPalette = mapThemeMetaData.lightColorPalette.Instantiate<Texture2D>();
				BuildingProperties.EnsurePaletteSettings(this.m_lightColorPalette);
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		BuildingProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new BuildingProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	public void InitializeShaderProperties()
	{
		Shader.SetGlobalTexture("_BuildingBaseDiffuse", this.m_baseDiffuse);
		Shader.SetGlobalTexture("_BuildingBaseNormal", this.m_baseNormal);
		Shader.SetGlobalTexture("_BuildingFloorDiffuse", this.m_floorDiffuse);
		Shader.SetGlobalTexture("_BuildingBurnedDiffuse", this.m_burnedDiffuse);
		Shader.SetGlobalTexture("_BuildingAbandonedDiffuse", this.m_abandonedDiffuse);
		Shader.SetGlobalTexture("_BuildingLightColorPalette", this.m_lightColorPalette);
		Shader.DisableKeyword("SNOW_ON");
	}

	protected virtual void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("BuildingProperties");
			Singleton<BuildingManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Texture m_baseDiffuse;

	public Texture m_baseNormal;

	public Texture m_floorDiffuse;

	public Texture m_burnedDiffuse;

	public Texture m_abandonedDiffuse;

	public Texture m_lightColorPalette;

	public EffectInfo m_placementEffect;

	public EffectInfo m_bulldozeEffect;

	public EffectInfo m_levelupEffect;

	public EffectInfo m_fireEffect;

	public EffectInfo m_collapseEffect;

	public EffectInfo m_collapseFloodedEffect;

	public Material m_highlightMaterial;

	public AudioInfo[] m_serviceSounds;

	public AudioInfo[] m_subServiceSounds;

	public CityNameGroups m_cityNameGroups;

	public Color m_burnedColor = new Color(0.39f, 0.37f, 0.35f);
}
