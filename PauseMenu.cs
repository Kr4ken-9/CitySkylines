﻿using System;
using System.Collections.Generic;
using Cities.DemoMode;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class PauseMenu : MenuPanel
{
	protected override void Awake()
	{
		this.m_MenuContainerPanel = base.Find<UIPanel>("Menu");
	}

	protected override void Initialize()
	{
		base.Initialize();
		this.Refresh();
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.Refresh();
	}

	private void Refresh()
	{
		UITemplateManager.ClearInstances("PauseMenuSeparatorTemplate");
		UITemplateManager.ClearInstances("PauseMenuButtonTemplate");
		if (Singleton<ToolManager>.get_exists())
		{
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.Game)
			{
				if (DemoModeLoader.instance != null)
				{
					UIPanel uIPanel = base.Find<UIPanel>("Menu");
					float num = (float)uIPanel.get_autoLayoutPadding().get_vertical();
					Vector2 size = base.get_component().get_size();
					size.y = uIPanel.get_relativePosition().y;
					for (int i = 0; i < this.m_DemoModeGameMenuItems.Count; i++)
					{
						UIComponent uIComponent;
						if (this.m_DemoModeGameMenuItems[i] == "-")
						{
							uIComponent = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
						}
						else
						{
							uIComponent = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_DemoModeGameMenuItems[i], Locale.Get("MENUITEM", this.m_DemoModeGameMenuItems[i]), this.m_MenuContainerPanel);
						}
						size.y += uIComponent.get_size().y + num;
					}
					base.get_component().set_size(size);
				}
				else
				{
					UIPanel uIPanel2 = base.Find<UIPanel>("Menu");
					float num2 = (float)uIPanel2.get_autoLayoutPadding().get_vertical();
					Vector2 size2 = base.get_component().get_size();
					size2.y = uIPanel2.get_relativePosition().y;
					for (int j = 0; j < this.m_InGameMenuItems.Count; j++)
					{
						UIComponent uIComponent2;
						if (this.m_InGameMenuItems[j] == "-")
						{
							uIComponent2 = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
						}
						else
						{
							uIComponent2 = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_InGameMenuItems[j], Locale.Get("MENUITEM", this.m_InGameMenuItems[j]), this.m_MenuContainerPanel);
						}
						size2.y += uIComponent2.get_size().y + num2;
					}
					base.get_component().set_size(size2);
				}
			}
			else if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.MapEditor)
			{
				UIPanel uIPanel3 = base.Find<UIPanel>("Menu");
				float num3 = (float)uIPanel3.get_autoLayoutPadding().get_vertical();
				Vector2 size3 = base.get_component().get_size();
				size3.y = uIPanel3.get_relativePosition().y;
				for (int k = 0; k < this.m_InMapEditorMenuItems.Count; k++)
				{
					UIComponent uIComponent3;
					if (this.m_InMapEditorMenuItems[k] == "-")
					{
						uIComponent3 = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
					}
					else
					{
						uIComponent3 = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_InMapEditorMenuItems[k], Locale.Get("MENUITEM", this.m_InMapEditorMenuItems[k]), this.m_MenuContainerPanel);
					}
					size3.y += uIComponent3.get_size().y + num3;
				}
				base.get_component().set_size(size3);
			}
			else if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				UIPanel uIPanel4 = base.Find<UIPanel>("Menu");
				float num4 = (float)uIPanel4.get_autoLayoutPadding().get_vertical();
				Vector2 size4 = base.get_component().get_size();
				size4.y = uIPanel4.get_relativePosition().y;
				for (int l = 0; l < this.m_DecorationEditorMenuItems.Count; l++)
				{
					UIComponent uIComponent4;
					if (this.m_DecorationEditorMenuItems[l] == "-")
					{
						uIComponent4 = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
					}
					else
					{
						uIComponent4 = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_DecorationEditorMenuItems[l], Locale.Get("MENUITEM", this.m_DecorationEditorMenuItems[l]), this.m_MenuContainerPanel);
					}
					size4.y += uIComponent4.get_size().y + num4;
					if (uIComponent4.get_name().Equals("SaveAsset"))
					{
						this.m_SaveAssetButton = (uIComponent4 as UIButton);
						this.m_SaveAssetButton.set_disabledTextColor(new Color32(255, 255, 255, 255));
						this.m_SaveAssetButton.set_disabledColor(new Color32(255, 255, 255, 255));
					}
				}
				base.get_component().set_size(size4);
			}
			else if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.ThemeEditor)
			{
				UIPanel uIPanel5 = base.Find<UIPanel>("Menu");
				float num5 = (float)uIPanel5.get_autoLayoutPadding().get_vertical();
				Vector2 size5 = base.get_component().get_size();
				size5.y = uIPanel5.get_relativePosition().y;
				for (int m = 0; m < this.m_ThemeEditorMenuItems.Count; m++)
				{
					UIComponent uIComponent5;
					if (this.m_ThemeEditorMenuItems[m] == "-")
					{
						uIComponent5 = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
					}
					else
					{
						uIComponent5 = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_ThemeEditorMenuItems[m], Locale.Get("MENUITEM", this.m_ThemeEditorMenuItems[m]), this.m_MenuContainerPanel);
					}
					size5.y += uIComponent5.get_size().y + num5;
					if (uIComponent5.get_name().Equals("SaveTheme"))
					{
						this.m_SaveAssetButton = (uIComponent5 as UIButton);
						this.m_SaveAssetButton.set_disabledTextColor(new Color32(255, 255, 255, 255));
						this.m_SaveAssetButton.set_disabledColor(new Color32(255, 255, 255, 255));
					}
				}
				base.get_component().set_size(size5);
			}
			else if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.ScenarioEditor)
			{
				UIPanel uIPanel6 = base.Find<UIPanel>("Menu");
				float num6 = (float)uIPanel6.get_autoLayoutPadding().get_vertical();
				Vector2 size6 = base.get_component().get_size();
				size6.y = uIPanel6.get_relativePosition().y;
				for (int n = 0; n < this.m_ScenarioEditorMenuItems.Count; n++)
				{
					UIComponent uIComponent6;
					if (this.m_ScenarioEditorMenuItems[n] == "-")
					{
						uIComponent6 = this.CreateMenuItem("PauseMenuSeparatorTemplate", string.Empty, string.Empty, this.m_MenuContainerPanel);
					}
					else
					{
						uIComponent6 = this.CreateMenuItem("PauseMenuButtonTemplate", this.m_ScenarioEditorMenuItems[n], Locale.Get("MENUITEM", this.m_ScenarioEditorMenuItems[n]), this.m_MenuContainerPanel);
					}
					size6.y += uIComponent6.get_size().y + num6;
				}
				base.get_component().set_size(size6);
			}
		}
	}

	public void Update()
	{
		if (this.m_MenuContainerPanel.get_isVisible() && (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor || Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.ThemeEditor) && this.m_SaveAssetButton != null)
		{
			if (LoadSaveStatus.activeTask != null && LoadSaveStatus.activeTask.isExecuting)
			{
				this.m_SaveAssetButton.set_isEnabled(false);
				this.m_SaveAssetButton.set_tooltip(Locale.Get("LOADSTATUS", LoadSaveStatus.activeTask.name));
			}
			else
			{
				this.m_SaveAssetButton.set_isEnabled(true);
				this.m_SaveAssetButton.set_tooltip(string.Empty);
			}
		}
	}

	public override void OnClosed()
	{
		this.m_DelayFrame = false;
		base.OnClosed();
	}

	public void LateUpdate()
	{
		if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseMenu))
		{
			if (this.m_DelayFrame)
			{
				this.OnClosed();
				return;
			}
			this.m_DelayFrame = true;
		}
	}

	public void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
			if (Singleton<SimulationManager>.get_exists() && !this.isScenarioEditor)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = true;
				});
			}
			if (Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().MuteAll = true;
			}
			Cursor.set_visible(true);
			Cursor.set_lockState(0);
		}
		else
		{
			if (Singleton<SimulationManager>.get_exists() && !this.isScenarioEditor)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
				});
			}
			if (Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().MuteAll = false;
			}
		}
	}

	public bool isScenarioEditor
	{
		get
		{
			return Singleton<ToolManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.ScenarioEditor);
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	protected virtual void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p != null && p.get_source().get_parent() == this.m_MenuContainerPanel)
		{
			base.Invoke(p.get_source().get_stringUserData(), 0f);
		}
	}

	private void Resume()
	{
		this.OnClosed();
	}

	private void Options()
	{
		UIView.get_library().ShowModal("OptionsPanel");
	}

	private void Quit()
	{
		string id = "CONFIRMEXIT";
		if (Singleton<ToolManager>.get_exists())
		{
			if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
			{
				id = "CONFIRMEXIT_MAPEDITOR";
			}
			else if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
			{
				id = "CONFIRMEXIT_ASSETEDITOR";
			}
		}
		ExitConfirmPanel.ShowModal(id, delegate(UIComponent comp, int ret)
		{
			if (ret == 0)
			{
				this.OnClosed();
				Singleton<LoadingManager>.get_instance().UnloadLevel();
			}
			else if (ret == 1)
			{
				this.OnClosed();
				Singleton<LoadingManager>.get_instance().QuitApplication();
			}
		});
	}

	private void Statistics()
	{
		UIView.get_library().ShowModal("StatisticsPanel");
	}

	private void NewAsset()
	{
		AssetImporterWizard assetImporterWizard = UIView.get_library().ShowModal<AssetImporterWizard>("AssetImporterPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
		assetImporterWizard.Reset();
	}

	private void LoadAsset()
	{
		UIView.get_library().ShowModal("LoadAssetPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void SaveAsset()
	{
		UIView.get_library().ShowModal("SaveAssetPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void LoadGame()
	{
		UIView.get_library().ShowModal("LoadPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void SaveGame()
	{
		UIView.get_library().ShowModal("SavePanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void NewTheme()
	{
		ConfirmPanel.ShowModal("CONFIRM_NEWMAP", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				UIView.get_library().ShowModal("NewThemePanel", delegate(UIComponent comp2, int ret2)
				{
					if (ret2 == 1)
					{
						this.OnClosed();
					}
				});
			}
		});
	}

	private void LoadTheme()
	{
		UIView.get_library().ShowModal("LoadThemePanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void SaveTheme()
	{
		UIView.get_library().ShowModal("SaveThemePanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void NewMap()
	{
		ConfirmPanel.ShowModal("CONFIRM_NEWMAP", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				UIView.get_library().ShowModal("NewMapPanel", delegate(UIComponent comp2, int ret2)
				{
					if (ret2 == 1)
					{
						this.OnClosed();
					}
				});
			}
		});
	}

	private void LoadMap()
	{
		UIView.get_library().ShowModal("LoadMapPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void SaveMap()
	{
		UIView.get_library().ShowModal("SaveMapPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void Restart()
	{
		this.OnClosed();
		DemoModeLoader.instance.Restart();
	}

	private void NewScenario()
	{
		ConfirmPanel.ShowModal("CONFIRM_NEWSCENARIO", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				UIView.get_library().ShowModal("NewScenarioPanel", delegate(UIComponent comp2, int ret2)
				{
					if (ret2 == 1)
					{
						this.OnClosed();
					}
				});
			}
		});
	}

	private void LoadScenario()
	{
		UIView.get_library().ShowModal("LoadScenarioPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void SaveScenario()
	{
		UIView.get_library().ShowModal("SaveScenarioPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
	}

	private void UpdateMapOrSave()
	{
		UIView.get_library().ShowModal("NewScenarioPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.OnClosed();
			}
		});
		NewScenarioPanel newScenarioPanel = UIView.get_library().Get<NewScenarioPanel>("NewScenarioPanel");
		newScenarioPanel.updateScenario = true;
	}

	private const string kPauseMenuButtonTemplate = "PauseMenuButtonTemplate";

	private const string kPauseMenuSeparatorTemplate = "PauseMenuSeparatorTemplate";

	public List<string> m_InGameMenuItems;

	public List<string> m_InMapEditorMenuItems;

	public List<string> m_DecorationEditorMenuItems;

	public List<string> m_ThemeEditorMenuItems;

	public List<string> m_ScenarioEditorMenuItems;

	public List<string> m_DemoModeGameMenuItems;

	private UIComponent m_MenuContainerPanel;

	private UIButton m_SaveAssetButton;

	public GameObject m_MenuItemPrefab;

	public GameObject m_MenuItemSeparatorPrefab;

	private bool m_DelayFrame;
}
