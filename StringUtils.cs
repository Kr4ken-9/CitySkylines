﻿using System;
using UnityEngine;

public static class StringUtils
{
	public static string RemoveLeadingAndTrailingChar(string str, string c)
	{
		bool flag;
		return StringUtils.RemoveLeadingAndTrailingChar(str, c, c, out flag);
	}

	public static string RemoveLeadingAndTrailingChar(string str, string l, string r)
	{
		bool flag;
		return StringUtils.RemoveLeadingAndTrailingChar(str, l, r, out flag);
	}

	public static string RemoveLeadingAndTrailingChar(string str, string l, string r, out bool changed)
	{
		str = str.Trim();
		if (str.StartsWith(l) && str.EndsWith(r))
		{
			changed = true;
			return str.Substring(1, str.Length - 2);
		}
		changed = false;
		return str;
	}

	public static string SafeFormat(string format, object arg)
	{
		string result;
		try
		{
			result = string.Format(format, arg);
		}
		catch (Exception ex)
		{
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				": '",
				format,
				"' with 1 arguments."
			}));
			result = format;
		}
		return result;
	}

	public static string SafeFormat(string format, params object[] args)
	{
		string result;
		try
		{
			result = string.Format(format, args);
		}
		catch (Exception ex)
		{
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				": '",
				format,
				"' with ",
				args.Length,
				" arguments."
			}));
			result = format;
		}
		return result;
	}
}
