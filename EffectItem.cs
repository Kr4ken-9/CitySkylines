﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class EffectItem : UICustomControl
{
	private void Awake()
	{
		this.m_dropdown = base.Find<UIDropDown>("Dropdown");
		this.m_dropdown.add_eventEnterFocus(new FocusEventHandler(this.OnDropDownEnterFocus));
		this.m_dropdown.add_eventMouseEnter(new MouseEventHandler(this.OnDropDownMouseEnter));
		this.m_dropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnIndexChanged));
	}

	private void HideAllSubItems()
	{
		this.m_effectItemDisaster.get_component().set_isVisible(false);
		this.m_effectItemFineReward.get_component().set_isVisible(false);
		this.m_effectItemMessage.get_component().set_isVisible(false);
		this.m_effectItemWinLose.get_component().set_isVisible(false);
	}

	public void Init(EffectList effectList, bool pickDefaultOption)
	{
		this.HideAllSubItems();
		this.m_effectList = effectList;
		if (pickDefaultOption)
		{
			this.m_dropdown.set_items(this.m_effectList.GetAvailableOptions(EffectItem.EffectType.None));
			EffectItem.EffectType effectTypeByDropdownIndex = this.m_effectList.GetEffectTypeByDropdownIndex(0);
			EffectSubItem item = this.GetItem(effectTypeByDropdownIndex);
			item.SetDefaultValues(effectTypeByDropdownIndex);
			item.Show(effectTypeByDropdownIndex);
			this.m_currentSubItem = item;
		}
	}

	public void LoadEffect(TriggerEffect effect)
	{
		this.HideAllSubItems();
		this.m_currentEffectType = EffectItem.EffectType.None;
		DisasterEffect disasterEffect = effect as DisasterEffect;
		if (disasterEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.Disaster;
			this.m_effectItemDisaster.Load(disasterEffect);
		}
		RewardEffect rewardEffect = effect as RewardEffect;
		if (rewardEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.CashReward;
			this.m_effectItemFineReward.Load(rewardEffect);
		}
		FeeEffect feeEffect = effect as FeeEffect;
		if (feeEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.CashFine;
			this.m_effectItemFineReward.Load(feeEffect);
		}
		LoseEffect loseEffect = effect as LoseEffect;
		if (loseEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.Lose;
			this.m_effectItemWinLose.Load(loseEffect);
		}
		WinEffect winEffect = effect as WinEffect;
		if (winEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.Win;
			this.m_effectItemWinLose.Load(winEffect);
		}
		MessageEffect messageEffect = effect as MessageEffect;
		if (messageEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.StoryMessage;
			this.m_effectItemMessage.Load(messageEffect);
		}
		ChirpEffect chirpEffect = effect as ChirpEffect;
		if (chirpEffect != null)
		{
			this.m_currentEffectType = EffectItem.EffectType.Chirp;
			this.m_effectItemMessage.Load(chirpEffect);
		}
		this.m_currentSubItem = this.GetItem(this.m_currentEffectType);
		this.m_dropdown.set_items(this.m_effectList.GetAvailableOptions(this.m_currentEffectType));
		for (int i = 0; i < this.m_dropdown.get_items().Length; i++)
		{
			if (this.m_effectList.GetEffectTypeByDropdownIndex(i) == this.m_currentEffectType)
			{
				this.m_dropdown.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnIndexChanged));
				this.m_dropdown.set_selectedIndex(i);
				this.m_dropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnIndexChanged));
				return;
			}
		}
		Debug.LogError("Effect type not in available options: " + this.m_currentEffectType.ToString());
	}

	private void OnDropDownEnterFocus(UIComponent component, UIFocusEventParameter eventParam)
	{
		this.m_dropdown.set_items(this.m_effectList.GetAvailableOptions(this.m_currentEffectType));
	}

	private void OnDropDownMouseEnter(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.m_dropdown.set_items(this.m_effectList.GetAvailableOptions(this.m_currentEffectType));
	}

	private void OnIndexChanged(UIComponent comp, int dropdownIndex)
	{
		if (this.m_currentSubItem != null)
		{
			this.m_currentSubItem.Hide();
		}
		this.m_currentEffectType = this.m_effectList.GetEffectTypeByDropdownIndex(dropdownIndex);
		this.m_currentSubItem = this.GetItem(this.m_currentEffectType);
		this.m_currentSubItem.SetDefaultValues(this.m_currentEffectType);
		this.m_currentSubItem.Show(this.m_currentEffectType);
	}

	private EffectSubItem GetItem(EffectItem.EffectType type)
	{
		switch (type)
		{
		case EffectItem.EffectType.Disaster:
			return this.m_effectItemDisaster;
		case EffectItem.EffectType.CashReward:
		case EffectItem.EffectType.CashFine:
			return this.m_effectItemFineReward;
		case EffectItem.EffectType.StoryMessage:
		case EffectItem.EffectType.Chirp:
			return this.m_effectItemMessage;
		case EffectItem.EffectType.Win:
		case EffectItem.EffectType.Lose:
			return this.m_effectItemWinLose;
		default:
			Debug.LogError("No valid effect type.");
			return null;
		}
	}

	public void OnDeleteButtonPressed()
	{
		this.m_effectList.DeleteItem(this);
	}

	public void DeleteEffect()
	{
		this.m_currentSubItem.Hide();
	}

	private EffectList m_effectList;

	private UIDropDown m_dropdown;

	public EffectSubItem m_currentSubItem;

	public EffectItemDisaster m_effectItemDisaster;

	public EffectItemFineReward m_effectItemFineReward;

	public EffectItemMessage m_effectItemMessage;

	public EffectItemWinLose m_effectItemWinLose;

	public EffectItem.EffectType m_currentEffectType;

	public TriggerEffect m_currentTriggerEffect;

	public enum EffectType
	{
		None,
		Disaster,
		CashReward,
		CashFine,
		StoryMessage,
		Chirp,
		Win,
		Lose
	}
}
