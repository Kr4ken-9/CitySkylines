﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class MapSettingsPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		this.SpawnEntry("StartTile", 0);
		this.SpawnEntry("Snapshot", 1);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		if (zOrder == 0)
		{
			ToolsModifierControl.SetTool<GameAreaTool>();
		}
		else if (zOrder == 1)
		{
			ToolsModifierControl.SetTool<SnapshotTool>();
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("MAPSETTINGS_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("MAPSETTINGS_DESC", name)
		});
		return base.CreateButton(name, tooltip, "MapSettings" + name, index, GeneratedPanel.tooltipBox);
	}
}
