﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class ShelterAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.EscapeRoutes)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else if (infoMode == InfoManager.InfoMode.DisasterHazard)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.Connections)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (data.m_tempImport != 0 || data.m_finalImport != 0)
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[17];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None || data.m_fireIntensity != 0)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data);
		}
		else if ((data.m_flags & (Building.Flags.Active | Building.Flags.Downgrading)) == Building.Flags.Active)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data, 1f, 300f);
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.EscapeRoutes;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		data.m_electricityBuffer = (ushort)(this.m_electricityStockpileAmount * 30 / 100);
		data.m_waterBuffer = (ushort)(this.m_waterStockpileAmount * 30 / 100);
		data.m_customBuffer1 = (ushort)(this.m_goodsStockpileAmount * 30 / 100);
		data.m_flags |= Building.Flags.Downgrading;
		this.EnsureCitizenUnits(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		if (version < 286u)
		{
			data.m_flags |= Building.Flags.Downgrading;
		}
		this.EnsureCitizenUnits(buildingID, ref data);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		ushort num = TransportTool.FindBuildingLine(buildingID);
		if (num != 0)
		{
			Singleton<TransportManager>.get_instance().ReleaseLine(num);
		}
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		this.EnsureCitizenUnits(buildingID, ref data);
		ushort num = TransportTool.FindBuildingLine(buildingID);
		if (num != 0)
		{
			TransportManager instance = Singleton<TransportManager>.get_instance();
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			VehicleManager instance3 = Singleton<VehicleManager>.get_instance();
			ushort stops = instance.m_lines.m_buffer[(int)num].m_stops;
			if (stops != 0)
			{
				VehicleInfo randomVehicleInfo = instance3.GetRandomVehicleInfo(ref instance2.m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				if (randomVehicleInfo != null)
				{
					Vector3 newPos;
					Vector3 vector;
					this.CalculateSpawnPosition(buildingID, ref data, ref instance2.m_randomizer, randomVehicleInfo, out newPos, out vector);
					instance.m_lines.m_buffer[(int)num].MoveStop(num, 0, newPos, false);
				}
			}
		}
	}

	private void EnsureCitizenUnits(ushort buildingID, ref Building data)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		bool extraCapacity = false;
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
		for (int i = 0; i < serviceBuildings.m_size; i++)
		{
			ushort num = serviceBuildings.m_buffer[i];
			if (num != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if (info.m_class.m_level == this.m_info.m_class.m_level && info.m_buildingAI is DoomsdayVaultAI)
				{
					extraCapacity = true;
				}
			}
		}
		this.EnsureCitizenUnits(buildingID, ref data, extraCapacity);
	}

	public void EnsureCitizenUnits(ushort buildingID, ref Building data, bool extraCapacity)
	{
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int visitCount = (!extraCapacity) ? this.m_capacity : (this.m_capacity * 13 / 10);
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, visitCount, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_disasterCoverageAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.DisasterCoverage, (float)this.m_disasterCoverageAccumulation, this.m_evacuationRange);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_disasterCoverageAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.DisasterCoverage, (float)(-(float)this.m_disasterCoverageAccumulation), this.m_evacuationRange);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.Goods)
		{
			int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
			int num2 = this.m_goodsStockpileAmount * num / 100;
			int customBuffer = (int)data.m_customBuffer1;
			amountDelta = Mathf.Clamp(amountDelta, 0, num2 - customBuffer);
			data.m_customBuffer1 = (ushort)(customBuffer + amountDelta);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateA, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateB, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateC, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateD, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateVipA, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateVipB, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateVipC, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.EvacuateVipD, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_evacuationRange * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Work) != 0)
			{
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenWorkBehaviour(ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
			}
			else if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenVisitBehaviour(ref behaviour, ref aliveVisitorCount, ref totalVisitorCount);
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		buildingData.m_finalExport = buildingData.m_tempExport;
		buildingData.m_tempExport = 0;
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		uint num = (instance.m_currentFrameIndex & 3840u) >> 8;
		if (num == 15u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_tempImport = 0;
		}
		if (buildingData.m_productionRate == 0)
		{
			int num2 = 100;
			int budget = base.GetBudget(buildingID, ref buildingData);
			num2 = PlayerBuildingAI.GetProductionRate(num2, budget);
			int num3 = num2 * this.m_electricityConsumption / 100;
			int num4 = num2 * this.m_waterConsumption / 100;
			int goodsConsumptionRate = this.m_goodsConsumptionRate;
			if ((int)buildingData.m_electricityBuffer >= num3)
			{
				buildingData.m_electricityBuffer -= (ushort)num3;
			}
			else
			{
				buildingData.m_electricityBuffer = 0;
			}
			if ((int)buildingData.m_waterBuffer >= num4)
			{
				buildingData.m_waterBuffer -= (ushort)num4;
			}
			else
			{
				buildingData.m_waterBuffer = 0;
			}
			if ((int)buildingData.m_customBuffer1 >= goodsConsumptionRate)
			{
				buildingData.m_customBuffer1 -= (ushort)goodsConsumptionRate;
			}
			else
			{
				buildingData.m_customBuffer1 = 0;
			}
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoFood);
		}
		if ((buildingData.m_flags & Building.Flags.Downgrading) == Building.Flags.None)
		{
			int budget2 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			int productionRate = PlayerBuildingAI.GetProductionRate(100, budget2);
			float num5 = (float)productionRate * this.m_evacuationRange * 0.01f;
			NetManager instance2 = Singleton<NetManager>.get_instance();
			DisasterManager instance3 = Singleton<DisasterManager>.get_instance();
			int num6 = 0;
			ushort num7 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)buildingID].m_netNode;
			int num8 = 0;
			while (num7 != 0)
			{
				NetInfo info = instance2.m_nodes.m_buffer[(int)num7].Info;
				if (info.m_class.m_layer == ItemClass.Layer.PublicTransport)
				{
					instance3.AddEvacuationArea(buildingData.m_position, num5 * 0.35f);
					num6++;
				}
				num7 = instance2.m_nodes.m_buffer[(int)num7].m_nextBuildingNode;
				if (++num8 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			num5 *= 10f / (10f + (float)num6);
			instance3.AddEvacuationArea(buildingData.m_position, num5);
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = productionRate * this.m_disasterCoverageAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.DisasterCoverage, num, buildingData.m_position, this.m_evacuationRange);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoFood);
		int num2 = 100 + Mathf.Min((int)buildingData.m_finalExport, 100);
		int num3 = this.m_goodsStockpileAmount * num2 / 100;
		if ((int)buildingData.m_customBuffer1 >= this.m_goodsConsumptionRate)
		{
			if ((int)buildingData.m_customBuffer1 < this.m_goodsStockpileAmount / 10)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.NoFood);
			}
			buildingData.m_customBuffer1 -= (ushort)this.m_goodsConsumptionRate;
		}
		else
		{
			finalProductionRate = Mathf.Min(finalProductionRate, (int)(buildingData.m_customBuffer1 * 100 + 99) / this.m_goodsConsumptionRate);
			problem = Notification.AddProblems(problem, Notification.Problem.NoFood | Notification.Problem.MajorProblem);
			buildingData.m_customBuffer1 = 0;
		}
		buildingData.m_problems = problem;
		int num4 = this.MaxIncomingLoadSize();
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Goods, ref num5, ref num6, ref num7, ref num8);
		buildingData.m_tempImport = (byte)Mathf.Clamp(num8, (int)buildingData.m_tempImport, 255);
		int num9 = num3 - (int)buildingData.m_customBuffer1 - num7;
		num9 -= num4 >> 1;
		if (num9 >= 0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = num9 * 8 / num4;
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			offer.Amount = 1;
			offer.Active = false;
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Goods, offer);
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if (finalProductionRate != 0)
		{
			uint num10 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 7u;
			int num11 = this.m_capacity + this.m_capacity * 3 * Mathf.Min((int)buildingData.m_finalExport, 100) / 1000;
			int num12 = num11 - totalVisitorCount;
			District[] expr_23C_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_23C_cp_0_cp_1 = district;
			expr_23C_cp_0_cp_0[(int)expr_23C_cp_0_cp_1].m_productionData.m_tempShelterCitizenNumber = expr_23C_cp_0_cp_0[(int)expr_23C_cp_0_cp_1].m_productionData.m_tempShelterCitizenNumber + (uint)totalVisitorCount;
			District[] expr_262_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_262_cp_0_cp_1 = district;
			expr_262_cp_0_cp_0[(int)expr_262_cp_0_cp_1].m_productionData.m_tempShelterCitizenCapacity = expr_262_cp_0_cp_0[(int)expr_262_cp_0_cp_1].m_productionData.m_tempShelterCitizenCapacity + (uint)num11;
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount + totalVisitorCount);
			if (num12 >= 1 && (buildingData.m_flags & Building.Flags.Downgrading) == Building.Flags.None)
			{
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Priority = Mathf.Max(1, (num12 * 4 + num11 - 1) / num11);
				offer2.Building = buildingID;
				offer2.Position = buildingData.m_position;
				if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.VIPArea) != DistrictPolicies.CityPlanning.None)
				{
					offer2.Amount = Mathf.Max(1, num12 >> 2);
					if (num12 >= 4 || num10 == 0u || num10 == 4u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipA, offer2);
					}
					if (num12 >= 4 || num10 == 1u || num10 == 5u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipB, offer2);
					}
					if (num12 >= 4 || num10 == 2u || num10 == 6u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipC, offer2);
					}
					if (num12 >= 4 || num10 == 3u || num10 == 7u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipD, offer2);
					}
				}
				else
				{
					offer2.Amount = Mathf.Max(1, num12 >> 3);
					if (num12 >= 8 || num10 == 0u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateA, offer2);
					}
					if (num12 >= 8 || num10 == 1u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateB, offer2);
					}
					if (num12 >= 8 || num10 == 2u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateC, offer2);
					}
					if (num12 >= 8 || num10 == 3u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateD, offer2);
					}
					if (num12 >= 8 || num10 == 4u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipA, offer2);
					}
					if (num12 >= 8 || num10 == 5u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipB, offer2);
					}
					if (num12 >= 8 || num10 == 6u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipC, offer2);
					}
					if (num12 >= 8 || num10 == 7u)
					{
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.EvacuateVipD, offer2);
					}
				}
			}
		}
		this.HandleEvacuationVehicles(buildingID, ref buildingData, finalProductionRate);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_escapeRoutes.Activate(properties.m_escapeRoutes, buildingID);
			int detectedDisasterCount = Singleton<DisasterManager>.get_instance().m_detectedDisasterCount;
			if (detectedDisasterCount != 0 && (buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
			{
				Singleton<BuildingManager>.get_instance().m_evacuationNotUsed.Activate(properties.m_evacuationNotUsed);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_evacuationNotUsed.Deactivate();
			}
			if (totalVisitorCount != 0 && (buildingData.m_flags & Building.Flags.Downgrading) == Building.Flags.None && detectedDisasterCount == 0)
			{
				Singleton<BuildingManager>.get_instance().m_citizensInShelter.Activate(properties.m_citizensInShelter, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_citizensInShelter.Deactivate(buildingID, false);
			}
		}
	}

	private void HandleEvacuationVehicles(ushort buildingID, ref Building data, int productionRate)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		TransferManager.TransferReason transferReason = TransferManager.TransferReason.EvacuateA;
		int num = (productionRate * this.m_evacuationBusCount + 99) / 100;
		int num2 = num;
		ushort num3 = TransportTool.FindBuildingLine(buildingID);
		ushort num4 = 0;
		if (num3 == 0)
		{
			num2 = 0;
		}
		else
		{
			TransportManager instance2 = Singleton<TransportManager>.get_instance();
			num4 = instance2.m_lines.m_buffer[(int)num3].m_stops;
			if (!instance2.m_lines.m_buffer[(int)num3].Complete)
			{
				num4 = 0;
			}
			if (num4 == 0)
			{
				num2 = 0;
			}
		}
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			num2 = 0;
		}
		if (num3 != 0)
		{
			TransportManager instance3 = Singleton<TransportManager>.get_instance();
			bool flag = num4 != 0 && num2 != 0;
			instance3.m_lines.m_buffer[(int)num3].SetActive(flag, flag);
		}
		ushort num5 = data.m_ownVehicles;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		while (num5 != 0)
		{
			if ((TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num5].m_transferType == transferReason)
			{
				if ((instance.m_vehicles.m_buffer[(int)num5].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
				{
					if (num6 >= num2)
					{
						VehicleInfo info = instance.m_vehicles.m_buffer[(int)num5].Info;
						info.m_vehicleAI.SetTransportLine(num5, ref instance.m_vehicles.m_buffer[(int)num5], 0);
					}
					else
					{
						num6++;
					}
				}
				num7++;
			}
			num5 = instance.m_vehicles.m_buffer[(int)num5].m_nextOwnVehicle;
			if (++num8 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (num2 > num6 && num > num7)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				Vector3 position;
				Vector3 vector;
				this.CalculateSpawnPosition(buildingID, ref data, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, out position, out vector);
				ushort num9;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num9, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, position, transferReason, false, true))
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.TransportLine = num3;
					offer.Position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num4].m_position;
					offer.Amount = 1;
					offer.Active = false;
					randomVehicleInfo.m_vehicleAI.SetSource(num9, ref vehicles.m_buffer[(int)num9], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num9, ref vehicles.m_buffer[(int)num9], transferReason, offer);
				}
			}
		}
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		return demolish && base.CollapseBuilding(buildingID, ref data, group, testOnly, demolish, burnAmount);
	}

	public override bool BurnBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly)
	{
		return false;
	}

	public override void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
		if (emptying || data.m_productionRate != 0)
		{
			Notification.Problem problem = data.m_problems;
			Notification.Problem problems = data.m_problems;
			problem = Notification.RemoveProblems(problem, Notification.Problem.Evacuating);
			if (emptying)
			{
				data.m_flags |= Building.Flags.Downgrading;
				base.EmptyBuilding(buildingID, ref data, CitizenUnit.Flags.Visit, true);
			}
			else
			{
				data.m_flags &= ~Building.Flags.Downgrading;
				problem = Notification.AddProblems(problem, Notification.Problem.Evacuating);
			}
			data.m_problems = problem;
			if (problem != problems)
			{
				Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, problem);
			}
		}
	}

	public override void SetProductionRate(ushort buildingID, ref Building data, byte rate)
	{
		base.SetProductionRate(buildingID, ref data, rate);
		if (rate == 0 && (data.m_flags & Building.Flags.Downgrading) == Building.Flags.None)
		{
			this.SetEmptying(buildingID, ref data, true);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void UpdateGuide(GuideController guideController)
	{
		if (this.m_info.m_notUsedGuide != null && !this.m_info.m_notUsedGuide.m_disabled)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
				bool flag = false;
				for (int i = 0; i < serviceBuildings.m_size; i++)
				{
					ushort num = serviceBuildings.m_buffer[i];
					if (num != 0)
					{
						BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
						if (info.m_class.m_level == this.m_info.m_class.m_level)
						{
							flag = true;
							break;
						}
					}
				}
				if (flag)
				{
					this.m_info.m_notUsedGuide.Disable();
				}
				else
				{
					this.m_info.m_notUsedGuide.Activate(guideController.m_sheltersNotUsed, this.m_info);
				}
			}
		}
	}

	protected override bool CanStockpileElectricity(ushort buildingID, ref Building data, out int stockpileAmount, out int stockpileRate)
	{
		int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
		int num2 = this.m_electricityStockpileAmount * num / 100;
		stockpileAmount = num2;
		stockpileRate = this.m_electricityStockpileRate;
		return true;
	}

	protected override bool CanStockpileWater(ushort buildingID, ref Building data, out int stockpileAmount, out int stockpileRate)
	{
		int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
		int num2 = this.m_waterStockpileAmount * num / 100;
		stockpileAmount = num2;
		stockpileRate = this.m_waterStockpileRate;
		return true;
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		onlyCollapse = false;
		return false;
	}

	private int MaxIncomingLoadSize()
	{
		return 4000;
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			Vector3 spawnPosition = this.m_spawnPosition;
			Vector3 spawnTarget = this.m_spawnTarget;
			position = data.CalculatePosition(spawnPosition);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(spawnPosition * 2f - spawnTarget);
			}
			else
			{
				target = data.CalculatePosition(spawnTarget);
			}
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			Vector3 spawnPosition = this.m_spawnPosition;
			Vector3 spawnTarget = this.m_spawnTarget;
			position = data.CalculatePosition(spawnPosition);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic != SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(spawnPosition * 2f - spawnTarget);
			}
			else
			{
				target = data.CalculatePosition(spawnTarget);
			}
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override TransportInfo GetTransportLineInfo()
	{
		return this.m_transportInfo;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_SHELTER_CAPACITY", new object[]
			{
				this.m_capacity
			}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_SHELTER_VEHICLE_COUNT", new object[]
			{
				this.m_evacuationBusCount
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int num4 = this.m_capacity + this.m_capacity * 3 * Mathf.Min((int)data.m_finalExport, 100) / 1000;
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num5 = (productionRate * this.m_evacuationBusCount + 99) / 100;
		string str = LocaleFormatter.FormatGeneric("AIINFO_SHELTER_CITIZENS", new object[]
		{
			num3,
			num4
		});
		return str + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_SHELTER_VEHICLE_COUNT", new object[]
		{
			num5
		});
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	public void GetFoodStatus(ushort buildingID, ref Building data, out int amount, out int max)
	{
		int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
		int num2 = this.m_goodsStockpileAmount * num / 100;
		amount = Mathf.Clamp((int)data.m_customBuffer1, 0, num2);
		max = num2;
	}

	public void GetElectricityStatus(ushort buildingID, ref Building data, out int amount, out int max)
	{
		int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
		int num2 = this.m_electricityStockpileAmount * num / 100;
		amount = Mathf.Clamp((int)data.m_electricityBuffer, 0, num2);
		max = num2;
	}

	public void GetWaterStatus(ushort buildingID, ref Building data, out int amount, out int max)
	{
		int num = 100 + Mathf.Min((int)data.m_finalExport, 100);
		int num2 = this.m_waterStockpileAmount * num / 100;
		amount = Mathf.Clamp((int)data.m_waterBuffer, 0, num2);
		max = num2;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 9;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 5;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 1;

	[CustomizableProperty("Shelter Capacity")]
	public int m_capacity = 1000;

	[CustomizableProperty("Goods Stockpile Size")]
	public int m_goodsStockpileAmount = 6400;

	[CustomizableProperty("Goods Consumption Rate")]
	public int m_goodsConsumptionRate = 100;

	[CustomizableProperty("Disaster Coverage Accumulation")]
	public int m_disasterCoverageAccumulation = 100;

	[CustomizableProperty("Evacuation Range")]
	public float m_evacuationRange = 500f;

	[CustomizableProperty("Evacuation Bus Count")]
	public int m_evacuationBusCount = 10;

	[CustomizableProperty("Electricity Stockpile Size", "Electricity")]
	public int m_electricityStockpileAmount = 640;

	[CustomizableProperty("Electricity Stockpile Rate", "Electricity")]
	public int m_electricityStockpileRate = 10;

	[CustomizableProperty("Water/Sewage Stockpile Size", "Water")]
	public int m_waterStockpileAmount = 640;

	[CustomizableProperty("Water/Sewage Stockpile Rate", "Water")]
	public int m_waterStockpileRate = 10;

	public Vector3 m_spawnPosition;

	public Vector3 m_spawnTarget;

	public bool m_canInvertTarget;

	public TransportInfo m_transportInfo;
}
