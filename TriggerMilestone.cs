﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class TriggerMilestone : MilestoneInfo
{
	public override string GetLocalizedName()
	{
		if (string.IsNullOrEmpty(this.m_triggerName))
		{
			return this.GenerateName();
		}
		return this.m_triggerName;
	}

	private string GenerateName()
	{
		return StringUtils.SafeFormat(Locale.Get("MILESTONE_TRIGGER_DESC"), this.m_triggerIndex);
	}

	public void SetName(string name)
	{
		if (string.IsNullOrEmpty(name) || name == this.GenerateName())
		{
			this.m_triggerName = null;
		}
		else
		{
			this.m_triggerName = name;
		}
	}

	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		if (string.IsNullOrEmpty(this.m_triggerName))
		{
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_TRIGGER_DESC"), new GeneratedString[]
			{
				new GeneratedString.Long((long)this.m_triggerIndex)
			});
		}
		return new GeneratedString.String(this.m_triggerName);
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int num = 0;
		if (this.m_conditions != null)
		{
			num = this.m_conditions.Length;
		}
		info.m_min = 0f;
		info.m_max = (float)num;
		int num2;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num2 = (int)data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num2 = 0;
		}
		if (num2 > num)
		{
			num2 = num;
		}
		info.m_description = this.GetLocalizedName();
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_TRIGGER_PROG"), new object[]
		{
			num2,
			num
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		int num = 0;
		if (this.m_conditions != null)
		{
			for (int i = 0; i < this.m_conditions.Length; i++)
			{
				num = Mathf.Max(num, this.m_conditions[i].GetComparisonValue());
			}
		}
		return num;
	}

	public bool CanAddTrigger(TriggerMilestone info)
	{
		if (info == this)
		{
			return false;
		}
		if (info.m_conditions != null)
		{
			for (int i = 0; i < info.m_conditions.Length; i++)
			{
				TriggerMilestone triggerMilestone = info.m_conditions[i] as TriggerMilestone;
				if (triggerMilestone != null && !this.CanAddTrigger(triggerMilestone))
				{
					return false;
				}
			}
		}
		return true;
	}

	public T AddCondition<T>() where T : MilestoneInfo
	{
		T t = ScriptableObject.CreateInstance<T>();
		int num = 0;
		if (this.m_conditions != null)
		{
			num = this.m_conditions.Length;
		}
		MilestoneInfo[] array = new MilestoneInfo[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = this.m_conditions[i];
		}
		array[num] = t;
		this.m_conditions = array;
		return t;
	}

	public bool AddCondition(TriggerMilestone info)
	{
		if (!this.CanAddTrigger(info))
		{
			return false;
		}
		int num = 0;
		if (this.m_conditions != null)
		{
			num = this.m_conditions.Length;
		}
		MilestoneInfo[] array = new MilestoneInfo[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = this.m_conditions[i];
		}
		array[num] = info;
		this.m_conditions = array;
		return true;
	}

	public T ReplaceCondition<T>(int index) where T : MilestoneInfo
	{
		if (this.m_conditions == null)
		{
			return (T)((object)null);
		}
		int num = this.m_conditions.Length;
		if (index < 0 || index >= num)
		{
			return (T)((object)null);
		}
		MilestoneInfo milestoneInfo = this.m_conditions[index];
		T t = ScriptableObject.CreateInstance<T>();
		this.m_conditions[index] = t;
		if (milestoneInfo != null && !(milestoneInfo is TriggerMilestone))
		{
			Object.Destroy(milestoneInfo);
		}
		return t;
	}

	public bool ReplaceCondition(int index, TriggerMilestone info)
	{
		if (!this.CanAddTrigger(info))
		{
			return false;
		}
		if (this.m_conditions == null)
		{
			return false;
		}
		int num = this.m_conditions.Length;
		if (index < 0 || index >= num)
		{
			return false;
		}
		MilestoneInfo milestoneInfo = this.m_conditions[index];
		this.m_conditions[index] = info;
		if (milestoneInfo != null && !(milestoneInfo is TriggerMilestone))
		{
			Object.Destroy(milestoneInfo);
		}
		return true;
	}

	public bool RemoveCondition(MilestoneInfo info)
	{
		if (this.m_conditions == null)
		{
			return false;
		}
		for (int i = 0; i < this.m_conditions.Length; i++)
		{
			if (this.m_conditions[i] == info)
			{
				return this.RemoveCondition(i);
			}
		}
		return false;
	}

	public bool RemoveCondition(int index)
	{
		if (this.m_conditions == null)
		{
			return false;
		}
		int num = this.m_conditions.Length;
		if (index < 0 || index >= num)
		{
			return false;
		}
		MilestoneInfo milestoneInfo = this.m_conditions[index];
		MilestoneInfo[] array = new MilestoneInfo[num - 1];
		for (int i = 0; i < index; i++)
		{
			array[i] = this.m_conditions[i];
		}
		for (int j = index + 1; j < num; j++)
		{
			array[j - 1] = this.m_conditions[j];
		}
		this.m_conditions = array;
		if (milestoneInfo != null && !(milestoneInfo is TriggerMilestone))
		{
			Object.Destroy(milestoneInfo);
		}
		return true;
	}

	public T AddEffect<T>() where T : TriggerEffect, new()
	{
		T t = Activator.CreateInstance<T>();
		int num = 0;
		if (this.m_effects != null)
		{
			num = this.m_effects.Length;
		}
		TriggerEffect[] array = new TriggerEffect[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = this.m_effects[i];
		}
		array[num] = t;
		this.m_effects = array;
		return t;
	}

	public bool RemoveEffect(TriggerEffect effect)
	{
		if (this.m_effects == null)
		{
			return false;
		}
		for (int i = 0; i < this.m_effects.Length; i++)
		{
			if (this.m_effects[i] == effect)
			{
				return this.RemoveEffect(i);
			}
		}
		return false;
	}

	public bool RemoveEffect(int index)
	{
		if (this.m_effects == null)
		{
			return false;
		}
		int num = this.m_effects.Length;
		if (index < 0 || index >= num)
		{
			return false;
		}
		TriggerEffect[] array = new TriggerEffect[num - 1];
		for (int i = 0; i < index; i++)
		{
			array[i] = this.m_effects[i];
		}
		for (int j = index + 1; j < num; j++)
		{
			array[j - 1] = this.m_effects[j];
		}
		this.m_effects = array;
		return true;
	}

	public override MilestoneInfo.Data GetData()
	{
		if (this.m_data == null)
		{
			TriggerMilestone.TriggerData triggerData = new TriggerMilestone.TriggerData();
			if (this.m_conditions != null && this.m_conditions.Length != 0)
			{
				triggerData.m_conditions = new MilestoneInfo.Data[this.m_conditions.Length];
				triggerData.m_passedConditions = new bool[this.m_conditions.Length];
				for (int i = 0; i < this.m_conditions.Length; i++)
				{
					triggerData.m_conditions[i] = this.m_conditions[i].GetData();
				}
			}
			this.m_data = triggerData;
			this.m_data.m_isAssigned = true;
		}
		return this.m_data;
	}

	public override void SetData(MilestoneInfo.Data data)
	{
		if (data == null || data.m_isAssigned)
		{
			return;
		}
		data.m_isAssigned = true;
		TriggerMilestone.TriggerData triggerData = data as TriggerMilestone.TriggerData;
		TriggerMilestone.TriggerData triggerData2 = this.m_data as TriggerMilestone.TriggerData;
		if (triggerData != null)
		{
			int num;
			if (this.m_conditions != null)
			{
				num = this.m_conditions.Length;
			}
			else
			{
				num = 0;
			}
			int num2;
			if (triggerData.m_conditions != null)
			{
				num2 = triggerData.m_conditions.Length;
			}
			else
			{
				num2 = 0;
			}
			if (num == num2)
			{
				if (triggerData2 == null)
				{
					triggerData2 = triggerData;
					this.m_data = triggerData;
				}
				else
				{
					triggerData2.m_passedCount = triggerData.m_passedCount;
					triggerData2.m_progress = triggerData.m_progress;
					triggerData2.m_conditionIndex = triggerData.m_conditionIndex;
					triggerData2.m_repeatCount = triggerData.m_repeatCount;
					triggerData2.m_repeatFrameIndex = triggerData.m_repeatFrameIndex;
				}
				this.m_written = true;
				for (int i = 0; i < num; i++)
				{
					this.m_conditions[i].SetData(triggerData.m_conditions[i]);
					if (triggerData2 == triggerData)
					{
						triggerData.m_conditions[i] = this.m_conditions[i].GetData();
					}
					else
					{
						triggerData2.m_passedConditions[i] = triggerData.m_passedConditions[i];
					}
				}
				if (this.m_conditionOrder == TriggerMilestone.ConditionOrder.OneByOne && this.m_conditions != null)
				{
					int conditionIndex = triggerData2.m_conditionIndex;
					if (!this.m_checkDisabled)
					{
						int num3 = 0;
						while (num3 < this.m_conditions.Length && num3 <= conditionIndex)
						{
							MilestoneInfo milestoneInfo = this.m_conditions[num3];
							if (!(milestoneInfo is TriggerMilestone))
							{
								milestoneInfo.m_checkDisabled = false;
							}
							num3++;
						}
					}
				}
				return;
			}
		}
		if (data != null)
		{
			triggerData2 = (this.GetData() as TriggerMilestone.TriggerData);
			triggerData2.m_passedCount = data.m_passedCount;
			triggerData2.m_progress = data.m_progress;
			this.m_written = true;
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		TriggerMilestone.TriggerData triggerData = this.GetData() as TriggerMilestone.TriggerData;
		if (triggerData.m_passedCount != 0 && !this.m_canRelock && !forceRelock)
		{
			return triggerData;
		}
		int num = triggerData.m_conditionIndex;
		if ((this.m_conditionOrder != TriggerMilestone.ConditionOrder.OneByOne || forceUnlock || forceRelock) && this.m_conditions != null)
		{
			num = this.m_conditions.Length;
		}
		int num2 = 0;
		int num3 = 0;
		if (this.m_conditions != null)
		{
			num3 = this.m_conditions.Length;
			int num4 = 0;
			while (num4 < this.m_conditions.Length && num4 <= num)
			{
				MilestoneInfo milestoneInfo = this.m_conditions[num4];
				if (milestoneInfo is TriggerMilestone)
				{
					MilestoneInfo.Data data = milestoneInfo.GetData();
					if (triggerData.m_passedConditions != null && triggerData.m_passedConditions.Length > num4 && this.m_conditionOrder != TriggerMilestone.ConditionOrder.Simultaneous)
					{
						if (data.m_passedCount > 0 || forceUnlock)
						{
							triggerData.m_passedConditions[num4] = true;
						}
						else if (forceRelock)
						{
							triggerData.m_passedConditions[num4] = false;
						}
						if (triggerData.m_passedConditions[num4])
						{
							num2++;
							num = Mathf.Max(num, num4 + 1);
						}
					}
					else if (data.m_passedCount != 0 || forceUnlock)
					{
						num2++;
						num = Mathf.Max(num, num4 + 1);
					}
				}
				else
				{
					if (forceRelock)
					{
						milestoneInfo.m_canRelock = true;
					}
					if (Singleton<UnlockManager>.get_instance().CheckMilestone(milestoneInfo, forceUnlock, forceRelock) > 0)
					{
						num2++;
						num = Mathf.Max(num, num4 + 1);
					}
					milestoneInfo.m_canRelock = (this.m_conditionOrder == TriggerMilestone.ConditionOrder.Simultaneous);
					milestoneInfo.m_checkDisabled = false;
				}
				num4++;
			}
		}
		bool flag = triggerData.m_passedCount >= 1;
		if (forceUnlock)
		{
			triggerData.m_progress = (long)num3;
			triggerData.m_passedCount = 1;
		}
		else if (forceRelock)
		{
			triggerData.m_progress = 0L;
			triggerData.m_passedCount = 0;
			triggerData.m_repeatFrameIndex = 0u;
			num = 0;
		}
		else
		{
			triggerData.m_progress = (long)Mathf.Min(num3, num2);
			triggerData.m_passedCount = ((triggerData.m_progress < (long)num3) ? 0 : 1);
		}
		triggerData.m_conditionIndex = num;
		if (triggerData.m_passedCount >= 1 && !flag)
		{
			if (triggerData.m_repeatCount < this.m_maxRepeatCount)
			{
				triggerData.m_repeatCount++;
				triggerData.m_repeatFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex + (uint)(Mathf.Max(1, this.m_repeatCooldown) * 4096);
			}
			else
			{
				triggerData.m_repeatFrameIndex = 0u;
			}
			if (this.m_effects != null)
			{
				string scenarioName = Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset;
				for (int i = 0; i < this.m_effects.Length; i++)
				{
					TriggerEffect triggerEffect = this.m_effects[i];
					if (triggerEffect != null)
					{
						triggerEffect.Activate();
						if (!string.IsNullOrEmpty(scenarioName))
						{
							if (triggerEffect is WinEffect)
							{
								ThreadHelper.get_dispatcher().Dispatch(delegate
								{
									if (Singleton<PopsManager>.get_exists())
									{
										Singleton<PopsManager>.get_instance().ScenarioEnd(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioName, true, this.GetNameForTelemetry(), NewScenarioGamePanel.GetRequiredDLC(scenarioName));
									}
								});
							}
							else if (triggerEffect is LoseEffect)
							{
								ThreadHelper.get_dispatcher().Dispatch(delegate
								{
									if (Singleton<PopsManager>.get_exists())
									{
										Singleton<PopsManager>.get_instance().ScenarioEnd(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioName, false, this.GetNameForTelemetry(), NewScenarioGamePanel.GetRequiredDLC(scenarioName));
									}
								});
							}
						}
					}
				}
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					UIView.get_library().Get<StoryMessagePanel>("StoryMessagePanel").AddMilestoneToBuffer(this);
				});
			}
		}
		return triggerData;
	}

	private string GetNameForTelemetry()
	{
		if (!string.IsNullOrEmpty(this.m_triggerName))
		{
			return this.m_triggerName;
		}
		return "Trigger " + this.m_triggerIndex.ToString();
	}

	public override void ResetWrittenStatus()
	{
		base.ResetWrittenStatus();
		if (this.m_conditions != null)
		{
			for (int i = 0; i < this.m_conditions.Length; i++)
			{
				this.m_conditions[i].ResetWrittenStatus();
			}
		}
	}

	public override void ResetImpl(int maxPassCount, bool directReference, bool onlyUnwritten)
	{
		base.ResetImpl(maxPassCount, directReference, onlyUnwritten);
		if (this.m_conditions != null)
		{
			for (int i = 0; i < this.m_conditions.Length; i++)
			{
				this.m_conditions[i].ResetImpl(1, false, onlyUnwritten);
			}
		}
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32(this.m_triggerIndex);
		s.WriteUniqueString(this.m_triggerName);
		s.WriteInt32(this.m_maxRepeatCount);
		s.WriteInt32(this.m_repeatCooldown);
		s.WriteInt32((int)this.m_conditionOrder);
		if (this.m_conditions != null)
		{
			s.WriteInt32(this.m_conditions.Length);
			for (int i = 0; i < this.m_conditions.Length; i++)
			{
				MilestoneInfo milestoneInfo = this.m_conditions[i];
				if (milestoneInfo != null)
				{
					s.WriteSharedType(milestoneInfo.GetType());
				}
				else
				{
					s.WriteSharedType(null);
				}
			}
			for (int j = 0; j < this.m_conditions.Length; j++)
			{
				MilestoneInfo milestoneInfo2 = this.m_conditions[j];
				TriggerMilestone triggerMilestone = milestoneInfo2 as TriggerMilestone;
				if (triggerMilestone != null)
				{
					s.WriteInt32(triggerMilestone.m_triggerIndex);
				}
				else
				{
					milestoneInfo2.Serialize(s);
				}
			}
		}
		else
		{
			s.WriteInt32(0);
		}
		s.WriteObjectArray<TriggerEffect>(this.m_effects);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_triggerIndex = s.ReadInt32();
		this.m_triggerName = s.ReadUniqueString();
		if (s.get_version() >= 283u)
		{
			this.m_maxRepeatCount = s.ReadInt32();
			this.m_repeatCooldown = s.ReadInt32();
			this.m_conditionOrder = (TriggerMilestone.ConditionOrder)s.ReadInt32();
		}
		else
		{
			this.m_maxRepeatCount = 0;
			this.m_repeatCooldown = 5;
			this.m_conditionOrder = TriggerMilestone.ConditionOrder.AnyOrder;
		}
		int count = s.ReadInt32();
		Type[] conditionTypes = new Type[count];
		this.m_subTriggerIndex = new int[count];
		for (int i = 0; i < count; i++)
		{
			conditionTypes[i] = s.ReadSharedType();
		}
		if ((this.m_conditions != null && this.m_conditions.Length != 0) || (conditionTypes != null && conditionTypes.Length != 0))
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_conditions != null)
				{
					for (int k = 0; k < this.m_conditions.Length; k++)
					{
						if (this.m_conditions[k] != null)
						{
							Object.Destroy(this.m_conditions[k]);
						}
					}
				}
				this.m_conditions = new MilestoneInfo[count];
				for (int l = 0; l < count; l++)
				{
					Type type2 = conditionTypes[l];
					if (type2 != null && type2 != typeof(TriggerMilestone))
					{
						this.m_conditions[l] = (ScriptableObject.CreateInstance(type2) as MilestoneInfo);
					}
				}
			}).Wait();
		}
		for (int j = 0; j < count; j++)
		{
			Type type = conditionTypes[j];
			if (type != null)
			{
				if (type == typeof(TriggerMilestone))
				{
					this.m_subTriggerIndex[j] = s.ReadInt32();
				}
				else
				{
					this.m_subTriggerIndex[j] = -1;
					this.m_conditions[j].Deserialize(s);
					this.m_conditions[j].m_canRelock = (this.m_conditionOrder == TriggerMilestone.ConditionOrder.Simultaneous);
					this.m_conditions[j].m_checkDisabled = (this.m_checkDisabled || this.m_conditionOrder == TriggerMilestone.ConditionOrder.OneByOne);
				}
			}
			else
			{
				this.m_subTriggerIndex[j] = -1;
			}
		}
		this.m_effects = s.ReadObjectArray<TriggerEffect>();
	}

	public void AssignTriggers(Dictionary<int, TriggerMilestone> triggers)
	{
		if (this.m_conditions != null && this.m_subTriggerIndex != null)
		{
			for (int i = 0; i < this.m_subTriggerIndex.Length; i++)
			{
				TriggerMilestone triggerMilestone;
				if (this.m_subTriggerIndex[i] != -1 && triggers.TryGetValue(this.m_subTriggerIndex[i], out triggerMilestone))
				{
					this.m_conditions[i] = triggerMilestone;
				}
			}
		}
	}

	public override float GetProgress()
	{
		TriggerMilestone.TriggerData triggerData = this.m_data as TriggerMilestone.TriggerData;
		if (triggerData == null)
		{
			return 0f;
		}
		if (triggerData.m_passedCount != 0)
		{
			return 1f;
		}
		int num = triggerData.m_conditionIndex;
		if (this.m_conditionOrder != TriggerMilestone.ConditionOrder.OneByOne && this.m_conditions != null)
		{
			num = this.m_conditions.Length;
		}
		float num2 = 0f;
		float num3 = 0f;
		if (this.m_conditions != null)
		{
			for (int i = 0; i < this.m_conditions.Length; i++)
			{
				if (!this.m_conditions[i].m_hidden)
				{
					if (num >= i)
					{
						num2 += this.m_conditions[i].GetProgress();
					}
					num3 += 1f;
				}
			}
		}
		if (num3 != 0f)
		{
			num2 /= num3;
		}
		return num2;
	}

	public int m_triggerIndex;

	public string m_triggerName;

	public int m_maxRepeatCount;

	public int m_repeatCooldown = 5;

	public TriggerMilestone.ConditionOrder m_conditionOrder;

	public MilestoneInfo[] m_conditions;

	public TriggerEffect[] m_effects;

	private int[] m_subTriggerIndex;

	public enum ConditionOrder
	{
		AnyOrder,
		OneByOne,
		Simultaneous
	}

	public class TriggerData : MilestoneInfo.Data
	{
		public override void Serialize(DataSerializer s)
		{
			base.Serialize(s);
			s.WriteObjectArray<MilestoneInfo.Data>(this.m_conditions);
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginWrite(s);
			if (this.m_conditions != null)
			{
				for (int i = 0; i < this.m_conditions.Length; i++)
				{
					if (this.m_passedConditions != null && i < this.m_passedConditions.Length)
					{
						@bool.Write(this.m_passedConditions[i]);
					}
					else
					{
						@bool.Write(false);
					}
				}
			}
			@bool.EndWrite();
			s.WriteInt32(this.m_conditionIndex);
			s.WriteInt32(this.m_repeatCount);
			s.WriteUInt32(this.m_repeatFrameIndex);
		}

		public override void Deserialize(DataSerializer s)
		{
			base.Deserialize(s);
			this.m_conditions = s.ReadObjectArray<MilestoneInfo.Data>();
			if (this.m_conditions != null)
			{
				this.m_passedConditions = new bool[this.m_conditions.Length];
			}
			else
			{
				this.m_passedConditions = null;
			}
			if (s.get_version() >= 284u)
			{
				EncodedArray.Bool @bool = EncodedArray.Bool.BeginRead(s);
				if (this.m_passedConditions != null)
				{
					for (int i = 0; i < this.m_passedConditions.Length; i++)
					{
						this.m_passedConditions[i] = @bool.Read();
					}
				}
				@bool.EndRead();
			}
			if (s.get_version() >= 284u)
			{
				this.m_conditionIndex = s.ReadInt32();
				this.m_repeatCount = s.ReadInt32();
				this.m_repeatFrameIndex = s.ReadUInt32();
			}
			else
			{
				this.m_conditionIndex = 0;
				this.m_repeatCount = 0;
				this.m_repeatFrameIndex = 0u;
			}
		}

		public override void AfterDeserialize(DataSerializer s)
		{
			base.AfterDeserialize(s);
		}

		public MilestoneInfo.Data[] m_conditions;

		public bool[] m_passedConditions;

		public int m_conditionIndex;

		public int m_repeatCount;

		public uint m_repeatFrameIndex;
	}
}
