﻿using System;
using System.Reflection;
using ColossalFramework.UI;
using UnityEngine;

public class REVectorSet : REPropertySet
{
	private void OnEnable()
	{
		if (this.m_X != null)
		{
			this.m_X.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
		if (this.m_Y != null)
		{
			this.m_Y.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
		if (this.m_Z != null)
		{
			this.m_Z.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
	}

	private void OnDisable()
	{
		if (this.m_X != null)
		{
			this.m_X.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
		if (this.m_Y != null)
		{
			this.m_Y.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
		if (this.m_Z != null)
		{
			this.m_Z.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.TextSubmitted));
		}
	}

	private void TextSubmitted(UIComponent component, string value)
	{
		float num;
		float num2;
		float num3;
		if (float.TryParse(this.m_X.get_text(), out num) && float.TryParse(this.m_Y.get_text(), out num2) && float.TryParse(this.m_Z.get_text(), out num3))
		{
			base.SetValue(new Vector3(num, num2, num3));
		}
	}

	protected override void Initialize(object target, FieldInfo targetField, string labelText)
	{
		base.get_component().Disable();
		if (this.m_Label != null)
		{
			this.m_Label.set_text(labelText);
		}
		Vector3 vector = (Vector3)targetField.GetValue(target);
		if (this.m_X != null)
		{
			this.m_X.set_text(vector.x.ToString());
		}
		if (this.m_Y != null)
		{
			this.m_Y.set_text(vector.y.ToString());
		}
		if (this.m_Z != null)
		{
			this.m_Z.set_text(vector.z.ToString());
		}
		base.get_component().Enable();
	}

	protected override bool Validate(object target, FieldInfo targetField)
	{
		return target != null && targetField != null && targetField.FieldType == typeof(Vector3);
	}

	public UILabel m_Label;

	public UITextField m_X;

	public UITextField m_Y;

	public UITextField m_Z;
}
