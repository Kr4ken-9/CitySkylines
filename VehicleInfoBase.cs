﻿using System;
using ColossalFramework;
using UnityEngine;

public class VehicleInfoBase : PrefabInfo
{
	protected void InitMeshInfo(Vector3 size)
	{
		if (this.m_mesh == null)
		{
			MeshFilter component = base.GetComponent<MeshFilter>();
			this.m_mesh = component.get_sharedMesh();
		}
		if (this.m_material == null)
		{
			this.m_material = base.GetComponent<Renderer>().get_sharedMaterial();
		}
		if (this.m_lodObject != null)
		{
			MeshFilter component2 = this.m_lodObject.GetComponent<MeshFilter>();
			this.m_lodMesh = component2.get_sharedMesh();
			this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
		}
		this.RefreshLevelOfDetail(size);
		this.m_lodTransforms = new Matrix4x4[16];
		this.m_lodLightStates = new Vector4[16];
		this.m_lodColors = new Vector4[16];
		this.m_lodCount = 0;
		this.m_lodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
		this.m_undergroundLodTransforms = new Matrix4x4[16];
		this.m_undergroundLodLightStates = new Vector4[16];
		this.m_undergroundLodColors = new Vector4[16];
		this.m_undergroundLodCount = 0;
		this.m_undergroundLodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_undergroundLodMax = new Vector3(-100000f, -100000f, -100000f);
	}

	protected void DestroyMeshInfo()
	{
		if (this.m_lodMeshCombined1 != null)
		{
			Object.Destroy(this.m_lodMeshCombined1);
			this.m_lodMeshCombined1 = null;
		}
		if (this.m_lodMeshCombined4 != null)
		{
			Object.Destroy(this.m_lodMeshCombined4);
			this.m_lodMeshCombined4 = null;
		}
		if (this.m_lodMeshCombined8 != null)
		{
			Object.Destroy(this.m_lodMeshCombined8);
			this.m_lodMeshCombined8 = null;
		}
		if (this.m_lodMeshCombined16 != null)
		{
			Object.Destroy(this.m_lodMeshCombined16);
			this.m_lodMeshCombined16 = null;
		}
		if (this.m_lodMaterialCombined != null)
		{
			Object.Destroy(this.m_lodMaterialCombined);
			this.m_lodMaterialCombined = null;
		}
		if (this.m_undergroundMaterial != null)
		{
			Object.Destroy(this.m_undergroundMaterial);
			this.m_undergroundMaterial = null;
		}
		if (this.m_undergroundLodMaterial != null)
		{
			Object.Destroy(this.m_undergroundLodMaterial);
			this.m_undergroundLodMaterial = null;
		}
		this.m_mesh = null;
		this.m_lodMesh = null;
		this.m_material = null;
		this.m_lodMaterial = null;
		this.m_lodMeshData = null;
		this.m_hasLodData = false;
		if (this.m_generatedInfo != null && this.m_generatedInfo.m_vehicleInfo == this)
		{
			this.m_generatedInfo.m_vehicleInfo = null;
		}
	}

	public void RefreshLevelOfDetail(Vector3 size)
	{
		float num = RenderManager.LevelOfDetailFactor * 125f;
		float num2 = 2f * size.y * size.z;
		num2 += 2f * size.x * size.z;
		num2 += 2f * size.x * size.y;
		this.m_maxRenderDistance = Mathf.Sqrt(num2) * num + 100f;
		if (this.m_lodMesh != null)
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance * 0.2f;
		}
		else
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance;
		}
	}

	public void InitMeshData(Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aciAtlas)
	{
		if (this.m_lodMesh == null || this.m_lodMaterial == null)
		{
			return;
		}
		if (this.m_lodMeshCombined1 == null)
		{
			this.m_lodMeshCombined1 = new Mesh();
		}
		if (this.m_lodMeshCombined4 == null)
		{
			this.m_lodMeshCombined4 = new Mesh();
		}
		if (this.m_lodMeshCombined8 == null)
		{
			this.m_lodMeshCombined8 = new Mesh();
		}
		if (this.m_lodMeshCombined16 == null)
		{
			this.m_lodMeshCombined16 = new Mesh();
		}
		if (this.m_lodMaterialCombined == null)
		{
			string[] shaderKeywords;
			if (this.m_lodMaterial != null)
			{
				this.m_lodMaterialCombined = new Material(this.m_lodMaterial);
				shaderKeywords = this.m_lodMaterial.get_shaderKeywords();
			}
			else
			{
				this.m_lodMaterialCombined = new Material(this.m_material);
				shaderKeywords = this.m_material.get_shaderKeywords();
			}
			for (int i = 0; i < shaderKeywords.Length; i++)
			{
				this.m_lodMaterialCombined.EnableKeyword(shaderKeywords[i]);
			}
			this.m_lodMaterialCombined.EnableKeyword("MULTI_INSTANCE");
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<VehicleManager>.get_instance().ID_MainTex))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<VehicleManager>.get_instance().ID_MainTex, rgbAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<VehicleManager>.get_instance().ID_XYSMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<VehicleManager>.get_instance().ID_XYSMap, xysAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<VehicleManager>.get_instance().ID_ACIMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<VehicleManager>.get_instance().ID_ACIMap, aciAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<VehicleManager>.get_instance().ID_AtlasRect))
		{
			this.m_lodMaterialCombined.SetVector(Singleton<VehicleManager>.get_instance().ID_AtlasRect, new Vector4(atlasRect.get_x(), atlasRect.get_y(), atlasRect.get_width(), atlasRect.get_height()));
		}
		this.m_lodMeshData = new RenderGroup.MeshData(this.m_lodMesh);
		this.m_lodMeshData.UpdateBounds();
		Vector3[] vertices = this.m_lodMeshData.m_vertices;
		Vector3[] normals = this.m_lodMeshData.m_normals;
		Vector4[] tangents = this.m_lodMeshData.m_tangents;
		Vector2[] uvs = this.m_lodMeshData.m_uvs;
		Color32[] array = this.m_lodMeshData.m_colors;
		int[] triangles = this.m_lodMeshData.m_triangles;
		int num = vertices.Length;
		int num2 = triangles.Length;
		if (num * 16 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		if (array.Length != num)
		{
			array = new Color32[num];
			for (int j = 0; j < array.Length; j++)
			{
				array[j] = new Color32(255, 255, 255, 255);
			}
		}
		for (int k = 0; k < num; k++)
		{
			Vector2 vector = uvs[k];
			vector.x = atlasRect.get_x() + atlasRect.get_width() * vector.x;
			vector.y = atlasRect.get_y() + atlasRect.get_height() * vector.y;
			uvs[k] = vector;
		}
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num, num2);
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 4, num2 * 4);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 8, num2 * 8);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 16, num2 * 16);
		int num3 = 0;
		int num4 = 0;
		for (int l = 0; l < 16; l++)
		{
			byte a = (byte)(l * 16);
			for (int m = 0; m < num2; m++)
			{
				if (l < 1)
				{
					meshData.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 4)
				{
					meshData2.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 8)
				{
					meshData3.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 16)
				{
					meshData4.m_triangles[num4] = triangles[m] + num3;
				}
				num4++;
			}
			for (int n = 0; n < num; n++)
			{
				Color32 color = array[n];
				color.a = a;
				if (l < 1)
				{
					meshData.m_vertices[num3] = vertices[n];
					meshData.m_normals[num3] = normals[n];
					meshData.m_tangents[num3] = tangents[n];
					meshData.m_uvs[num3] = uvs[n];
					meshData.m_colors[num3] = color;
				}
				if (l < 4)
				{
					meshData2.m_vertices[num3] = vertices[n];
					meshData2.m_normals[num3] = normals[n];
					meshData2.m_tangents[num3] = tangents[n];
					meshData2.m_uvs[num3] = uvs[n];
					meshData2.m_colors[num3] = color;
				}
				if (l < 8)
				{
					meshData3.m_vertices[num3] = vertices[n];
					meshData3.m_normals[num3] = normals[n];
					meshData3.m_tangents[num3] = tangents[n];
					meshData3.m_uvs[num3] = uvs[n];
					meshData3.m_colors[num3] = color;
				}
				if (l < 16)
				{
					meshData4.m_vertices[num3] = vertices[n];
					meshData4.m_normals[num3] = normals[n];
					meshData4.m_tangents[num3] = tangents[n];
					meshData4.m_uvs[num3] = uvs[n];
					meshData4.m_colors[num3] = color;
				}
				num3++;
			}
		}
		meshData.PopulateMesh(this.m_lodMeshCombined1);
		meshData2.PopulateMesh(this.m_lodMeshCombined4);
		meshData3.PopulateMesh(this.m_lodMeshCombined8);
		meshData4.PopulateMesh(this.m_lodMeshCombined16);
	}

	public VehicleInfoGen m_generatedInfo;

	public GameObject m_lodObject;

	[NonSerialized]
	public Mesh m_mesh;

	[NonSerialized]
	public Mesh m_lodMesh;

	[NonSerialized]
	public Material m_material;

	[NonSerialized]
	public Material m_undergroundMaterial;

	[NonSerialized]
	public Material m_lodMaterial;

	[NonSerialized]
	public float m_lodRenderDistance;

	[NonSerialized]
	public float m_maxRenderDistance;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData;

	[NonSerialized]
	public Mesh m_lodMeshCombined1;

	[NonSerialized]
	public Mesh m_lodMeshCombined4;

	[NonSerialized]
	public Mesh m_lodMeshCombined8;

	[NonSerialized]
	public Mesh m_lodMeshCombined16;

	[NonSerialized]
	public Material m_lodMaterialCombined;

	[NonSerialized]
	public Matrix4x4[] m_lodTransforms;

	[NonSerialized]
	public Vector4[] m_lodLightStates;

	[NonSerialized]
	public Vector4[] m_lodColors;

	[NonSerialized]
	public int m_lodCount;

	[NonSerialized]
	public Vector3 m_lodMin;

	[NonSerialized]
	public Vector3 m_lodMax;

	[NonSerialized]
	public Material m_undergroundLodMaterial;

	[NonSerialized]
	public Matrix4x4[] m_undergroundLodTransforms;

	[NonSerialized]
	public Vector4[] m_undergroundLodLightStates;

	[NonSerialized]
	public Vector4[] m_undergroundLodColors;

	[NonSerialized]
	public int m_undergroundLodCount;

	[NonSerialized]
	public Vector3 m_undergroundLodMin;

	[NonSerialized]
	public Vector3 m_undergroundLodMax;

	[NonSerialized]
	public bool m_hasLodData;
}
