﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class TreeCollection : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(TreeCollection.InitializePrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading(base.get_gameObject().get_name());
		PrefabCollection<TreeInfo>.DestroyPrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private static IEnumerator InitializePrefabs(string name, TreeInfo[] prefabs, string[] replaces)
	{
		TreeCollection.<InitializePrefabs>c__Iterator0 <InitializePrefabs>c__Iterator = new TreeCollection.<InitializePrefabs>c__Iterator0();
		<InitializePrefabs>c__Iterator.name = name;
		<InitializePrefabs>c__Iterator.prefabs = prefabs;
		<InitializePrefabs>c__Iterator.replaces = replaces;
		return <InitializePrefabs>c__Iterator;
	}

	public TreeInfo[] m_prefabs;

	public string[] m_replacedNames;
}
