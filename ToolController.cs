﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public class ToolController : MonoBehaviour
{
	public event ToolController.EditPrefabChanged eventEditPrefabChanged
	{
		add
		{
			ToolController.EditPrefabChanged editPrefabChanged = this.eventEditPrefabChanged;
			ToolController.EditPrefabChanged editPrefabChanged2;
			do
			{
				editPrefabChanged2 = editPrefabChanged;
				editPrefabChanged = Interlocked.CompareExchange<ToolController.EditPrefabChanged>(ref this.eventEditPrefabChanged, (ToolController.EditPrefabChanged)Delegate.Combine(editPrefabChanged2, value), editPrefabChanged);
			}
			while (editPrefabChanged != editPrefabChanged2);
		}
		remove
		{
			ToolController.EditPrefabChanged editPrefabChanged = this.eventEditPrefabChanged;
			ToolController.EditPrefabChanged editPrefabChanged2;
			do
			{
				editPrefabChanged2 = editPrefabChanged;
				editPrefabChanged = Interlocked.CompareExchange<ToolController.EditPrefabChanged>(ref this.eventEditPrefabChanged, (ToolController.EditPrefabChanged)Delegate.Remove(editPrefabChanged2, value), editPrefabChanged);
			}
			while (editPrefabChanged != editPrefabChanged2);
		}
	}

	public ToolBase[] Tools
	{
		get
		{
			return this.m_tools;
		}
	}

	public ToolBase CurrentTool
	{
		get
		{
			return this.m_currentTool;
		}
		set
		{
			if (value != this.m_currentTool)
			{
				this.SetTool(value);
			}
		}
	}

	public ToolBase NextTool
	{
		get
		{
			return this.m_nextTool;
		}
	}

	public bool IsInsideUI
	{
		get
		{
			return this.m_isInsideUI;
		}
	}

	public bool HasInputFocus
	{
		get
		{
			return this.m_hasInputFocus;
		}
	}

	public CursorInfo ToolCursor
	{
		get
		{
			return this.m_toolCursor;
		}
		set
		{
			this.m_toolCursor = value;
		}
	}

	public string screenShotPath
	{
		get
		{
			if (string.IsNullOrEmpty(this.m_ScreenshotPath))
			{
				this.m_ScreenshotPath = Path.Combine(DataLocation.get_localApplicationData(), "Screenshots");
			}
			Directory.CreateDirectory(this.m_ScreenshotPath);
			return this.m_ScreenshotPath;
		}
	}

	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("ToolController");
		this.m_brushData = new float[4096];
		this.m_tools = base.GetComponents<ToolBase>();
		this.m_brushMaterial2 = new Material(this.m_brushMaterial);
		this.ID_BrushTex = Shader.PropertyToID("_BrushTex");
		this.ID_BrushWS = Shader.PropertyToID("_BrushWS");
		this.m_collidingSegments1 = new ulong[576];
		this.m_collidingSegments2 = new ulong[576];
		this.m_collidingBuildings1 = new ulong[768];
		this.m_collidingBuildings2 = new ulong[768];
		this.m_collidingDepth = 0;
		this.CurrentTool = base.GetComponent<DefaultTool>();
		Singleton<ToolManager>.get_instance().InitializeProperties(this);
		UIView.PopAllModal();
		this.m_enableDevUI = Application.get_isEditor();
		if (!this.m_enableDevUI)
		{
			if (File.Exists("dev"))
			{
				this.m_enableDevUI = true;
			}
			string[] arguments = CommandLine.arguments;
			if (arguments != null)
			{
				for (int i = 0; i < arguments.Length; i++)
				{
					if (arguments[i] == "--enable-dev-ui")
					{
						this.m_enableDevUI = true;
						break;
					}
				}
			}
		}
		Singleton<LoadingManager>.get_instance().m_levelPreLoaded += new LoadingManager.LevelPreLoadedHandler(this.ResetTool);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("ToolController");
		Singleton<ToolManager>.get_instance().DestroyProperties(this);
		if (this.m_brushMaterial2 != null)
		{
			Object.Destroy(this.m_brushMaterial2);
			this.m_brushMaterial2 = null;
		}
		this.m_prevPrefabInfo = null;
		if (this.m_wasToolCursor != null)
		{
			if (UIView.get_activeCursor() != null)
			{
				Cursor.SetCursor(UIView.get_activeCursor().m_texture, UIView.get_activeCursor().m_hotspot, 0);
			}
			else
			{
				Cursor.SetCursor(null, Vector2.get_zero(), 0);
			}
			this.m_wasToolCursor = null;
		}
		Singleton<LoadingManager>.get_instance().m_levelPreLoaded -= new LoadingManager.LevelPreLoadedHandler(this.ResetTool);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	private void ResetTool()
	{
		ToolsModifierControl.SetTool<DefaultTool>();
	}

	private void OnGUI()
	{
		if (this.m_Screenshot.IsPressed(Event.get_current()))
		{
			Application.CaptureScreenshot(PathUtils.MakeUniquePath(Path.Combine(this.screenShotPath, "Screenshot.png")), 1);
		}
		else if (this.m_HiresScreenshot.IsPressed(Event.get_current()))
		{
			Application.CaptureScreenshot(PathUtils.MakeUniquePath(Path.Combine(this.screenShotPath, "HiresScreenshot.png")), 4);
		}
	}

	private void Update()
	{
		if (this.m_enableDevUI)
		{
			if (Input.GetKeyDown(9))
			{
				if (this.m_developerUI == null)
				{
					this.m_developerUI = base.get_gameObject().AddComponent<DeveloperUI>();
				}
				else if (this.m_developerUI.get_enabled())
				{
					this.m_developerUI.set_enabled(false);
				}
				else
				{
					this.m_developerUI.set_enabled(true);
				}
			}
			if (this.m_developerUI != null)
			{
				this.m_developerUI.UpdateTimeHack();
			}
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_isInsideUI = (mousePosition.x < 0f || mousePosition.x > (float)Screen.get_width() || mousePosition.y < 0f || mousePosition.y > (float)Screen.get_height() || UIView.IsInsideUI());
		this.m_hasInputFocus = UIView.HasInputFocus();
		if ((this.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None && Singleton<LoadingManager>.get_instance().m_loadingComplete && this.m_editPrefabInfo != this.m_prevPrefabInfo)
		{
			base.StartCoroutine(this.ChangeEditPrefab());
		}
	}

	[DebuggerHidden]
	private IEnumerator ChangeEditPrefab()
	{
		ToolController.<ChangeEditPrefab>c__Iterator0 <ChangeEditPrefab>c__Iterator = new ToolController.<ChangeEditPrefab>c__Iterator0();
		<ChangeEditPrefab>c__Iterator.$this = this;
		return <ChangeEditPrefab>c__Iterator;
	}

	private void LateUpdate()
	{
		if (this.m_isInsideUI || this.m_toolCursor == null || Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (this.m_wasToolCursor != null)
			{
				if (UIView.get_activeCursor() != null)
				{
					Cursor.SetCursor(UIView.get_activeCursor().m_texture, UIView.get_activeCursor().m_hotspot, 0);
				}
				else
				{
					Cursor.SetCursor(null, Vector2.get_zero(), 0);
				}
				this.m_wasToolCursor = null;
			}
		}
		else if (this.m_wasToolCursor != this.m_toolCursor || UIView.get_activeCursor() != null)
		{
			Cursor.SetCursor(this.m_toolCursor.m_texture, this.m_toolCursor.m_hotspot, 0);
			this.m_wasToolCursor = this.m_toolCursor;
		}
	}

	public void SetBrush(Texture2D brush, Vector3 brushPosition, float brushSize)
	{
		if (this.m_brush != brush)
		{
			this.m_brush = brush;
			if (this.m_brush != null)
			{
				for (int i = 0; i < 64; i++)
				{
					for (int j = 0; j < 64; j++)
					{
						this.m_brushData[i * 64 + j] = this.m_brush.GetPixel(j, i).a;
					}
				}
			}
		}
		this.m_brushPosition = brushPosition;
		this.m_brushSize = brushSize;
	}

	public void RenderBrush(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_brush != null)
		{
			this.m_brushMaterial2.SetTexture(this.ID_BrushTex, this.m_brush);
			Vector4 vector = this.m_brushPosition;
			vector.w = this.m_brushSize;
			this.m_brushMaterial2.SetVector(this.ID_BrushWS, vector);
			Vector3 vector2;
			vector2..ctor(this.m_brushPosition.x, 512f, this.m_brushPosition.z);
			Vector3 vector3;
			vector3..ctor(this.m_brushSize, 1224f, this.m_brushSize);
			Bounds bounds;
			bounds..ctor(vector2, vector3);
			ToolManager expr_A0_cp_0 = Singleton<ToolManager>.get_instance();
			expr_A0_cp_0.m_drawCallData.m_overlayCalls = expr_A0_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_brushMaterial2, 0, bounds);
		}
	}

	private void SetTool(ToolBase tool)
	{
		this.m_nextTool = tool;
		if (this.m_currentTool != null)
		{
			this.m_currentTool.set_enabled(false);
		}
		this.m_currentTool = tool;
		this.m_nextTool = null;
		if (this.m_currentTool != null)
		{
			this.m_currentTool.set_enabled(true);
		}
	}

	public void RenderCollidingNotifications(RenderManager.CameraInfo cameraInfo, ushort ignoreSegment, ushort ignoreBuilding)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = this.m_collidingSegments2.Length;
		for (int i = 0; i < num; i++)
		{
			ulong num2 = this.m_collidingSegments2[i];
			if (num2 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num2 & 1uL << j) != 0uL)
					{
						int num3 = i << 6 | j;
						if (num3 != (int)ignoreSegment)
						{
							NetTool.RenderBulldozeNotification(cameraInfo, ref instance.m_segments.m_buffer[num3]);
						}
					}
				}
			}
		}
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		num = this.m_collidingBuildings2.Length;
		for (int k = 0; k < num; k++)
		{
			ulong num4 = this.m_collidingBuildings2[k];
			if (num4 != 0uL)
			{
				for (int l = 0; l < 64; l++)
				{
					if ((num4 & 1uL << l) != 0uL)
					{
						int num5 = k << 6 | l;
						if (num5 != (int)ignoreBuilding)
						{
							BuildingTool.RenderBulldozeNotification(cameraInfo, ref instance2.m_buildings.m_buffer[num5]);
						}
					}
				}
			}
		}
	}

	public void RenderColliding(RenderManager.CameraInfo cameraInfo, Color importantSegmentColor, Color nonImportantSegmentColor, Color importantBuildingColor, Color nonImportantBuildingColor, ushort ignoreSegment, ushort ignoreBuilding)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = this.m_collidingSegments2.Length;
		ushort num2 = 0;
		if (ignoreSegment != 0)
		{
			num2 = DefaultTool.FindSecondarySegment(ignoreSegment);
		}
		for (int i = 0; i < num; i++)
		{
			ulong num3 = this.m_collidingSegments2[i];
			if (num3 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num3 & 1uL << j) != 0uL)
					{
						int num4 = i << 6 | j;
						if (num4 != (int)ignoreSegment && num4 != (int)num2)
						{
							NetTool.RenderOverlay(cameraInfo, ref instance.m_segments.m_buffer[num4], importantSegmentColor, nonImportantSegmentColor);
						}
					}
				}
			}
		}
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		num = this.m_collidingBuildings2.Length;
		for (int k = 0; k < num; k++)
		{
			ulong num5 = this.m_collidingBuildings2[k];
			if (num5 != 0uL)
			{
				for (int l = 0; l < 64; l++)
				{
					if ((num5 & 1uL << l) != 0uL)
					{
						int num6 = k << 6 | l;
						if (num6 != (int)ignoreBuilding)
						{
							BuildingTool.RenderOverlay(cameraInfo, ref instance2.m_buildings.m_buffer[num6], importantBuildingColor, nonImportantBuildingColor);
						}
					}
				}
			}
		}
	}

	public void ReadPrefabMesh()
	{
		PrefabInfo editPrefabInfo = this.m_editPrefabInfo;
		if (editPrefabInfo != null)
		{
			editPrefabInfo.ReadMesh(out this.m_prefabVertices, out this.m_prefabTriangles);
			return;
		}
		this.m_prefabVertices = null;
		this.m_prefabTriangles = null;
	}

	[DebuggerHidden]
	public IEnumerator RemoveDecorations()
	{
		return new ToolController.<RemoveDecorations>c__Iterator1();
	}

	[DebuggerHidden]
	public IEnumerator LoadDecorations()
	{
		return new ToolController.<LoadDecorations>c__Iterator2();
	}

	[DebuggerHidden]
	public IEnumerator SaveDecorations()
	{
		return new ToolController.<SaveDecorations>c__Iterator3();
	}

	public float[] BrushData
	{
		get
		{
			return this.m_brushData;
		}
	}

	public void BeginColliding(out ulong[] collidingSegments, out ulong[] collidingBuildings)
	{
		if (this.m_collidingDepth++ == 0)
		{
			this.ResetColliding();
		}
		collidingSegments = this.m_collidingSegments1;
		collidingBuildings = this.m_collidingBuildings1;
	}

	public void ResetColliding()
	{
		int num = this.m_collidingSegments1.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_collidingSegments1[i] = 0uL;
		}
		num = this.m_collidingBuildings1.Length;
		for (int j = 0; j < num; j++)
		{
			this.m_collidingBuildings1[j] = 0uL;
		}
	}

	public void EndColliding()
	{
		if (--this.m_collidingDepth == 0)
		{
			int num = this.m_collidingSegments1.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_collidingSegments2[i] = this.m_collidingSegments1[i];
			}
			num = this.m_collidingBuildings1.Length;
			for (int j = 0; j < num; j++)
			{
				this.m_collidingBuildings2[j] = this.m_collidingBuildings1[j];
			}
		}
	}

	public void ClearColliding()
	{
		int num = this.m_collidingSegments1.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_collidingSegments1[i] = 0uL;
			this.m_collidingSegments2[i] = 0uL;
		}
		num = this.m_collidingBuildings1.Length;
		for (int j = 0; j < num; j++)
		{
			this.m_collidingBuildings1[j] = 0uL;
			this.m_collidingBuildings2[j] = 0uL;
		}
	}

	public bool RaycastEditObject(Segment3 ray, out Vector3 hitPos)
	{
		Vector3[] prefabVertices = this.m_prefabVertices;
		int[] prefabTriangles = this.m_prefabTriangles;
		float num = 2f;
		Vector3 vector;
		vector..ctor(0f, 60f, 0f);
		ray.a -= vector;
		ray.b -= vector;
		if (prefabVertices != null && prefabTriangles != null)
		{
			int num2 = prefabVertices.Length;
			int num3 = prefabTriangles.Length;
			for (int i = 0; i < num3; i += 3)
			{
				int num4 = prefabTriangles[i];
				int num5 = prefabTriangles[i + 1];
				int num6 = prefabTriangles[i + 2];
				float num7;
				float num8;
				float num9;
				if (Mathf.Max(num4, Mathf.Max(num5, num6)) < num2 && Triangle3.Intersect(prefabVertices[num4], prefabVertices[num5], prefabVertices[num6], ray.a, ray.b, ref num7, ref num8, ref num9) && num9 < num)
				{
					num = num9;
				}
			}
		}
		if (num != 2f)
		{
			hitPos = ray.Position(num) + vector;
			return true;
		}
		hitPos = Vector3.get_zero();
		return false;
	}

	public ItemClass.Availability m_mode = ItemClass.Availability.Game;

	public PrefabInfo m_editPrefabInfo;

	public Material m_brushMaterial;

	public Texture2D[] m_brushes;

	public Color m_validColor = new Color(1f, 1f, 1f, 0.5f);

	public Color m_warningColor = new Color(1f, 1f, 0.1875f, 0.75f);

	public Color m_errorColor = new Color(1f, 0.25f, 0.1875f, 0.75f);

	public Color m_validColorInfo = new Color(0f, 0f, 0f, 0.5f);

	public Color m_warningColorInfo = new Color(1f, 1f, 0.1875f, 0.75f);

	public Color m_errorColorInfo = new Color(1f, 0.25f, 0.1875f, 0.75f);

	public bool m_enableDevUI;

	[NonSerialized]
	public PrefabInfo m_prevPrefabInfo;

	[NonSerialized]
	public Vector3[] m_prefabVertices;

	[NonSerialized]
	public int[] m_prefabTriangles;

	[NonSerialized]
	public DeveloperUI m_developerUI;

	[NonSerialized]
	public PrefabInfo m_templatePrefabInfo;

	[NonSerialized]
	public bool m_disablePropCollisions;

	private ToolBase m_currentTool;

	private ToolBase m_nextTool;

	private Texture2D m_brush;

	private Vector3 m_brushPosition;

	private float m_brushSize;

	private float[] m_brushData;

	private ToolBase[] m_tools;

	private Material m_brushMaterial2;

	private int ID_BrushTex;

	private int ID_BrushWS;

	private bool m_isInsideUI;

	private bool m_hasInputFocus;

	private ulong[] m_collidingSegments1;

	private ulong[] m_collidingSegments2;

	private ulong[] m_collidingBuildings1;

	private ulong[] m_collidingBuildings2;

	private int m_collidingDepth;

	private CursorInfo m_toolCursor;

	private CursorInfo m_wasToolCursor;

	private SavedInputKey m_Screenshot = new SavedInputKey(Settings.screenshot, Settings.inputSettingsFile, DefaultSettings.screenshot, true);

	private SavedInputKey m_HiresScreenshot = new SavedInputKey(Settings.hiresScreenshot, Settings.inputSettingsFile, DefaultSettings.hiresScreenshot, true);

	[NonSerialized]
	public SavedFloat m_MouseLightIntensity = new SavedFloat(Settings.mouseLightIntensity, Settings.gameSettingsFile, DefaultSettings.mouseLightIntensity, true);

	private string m_ScreenshotPath;

	public delegate void EditPrefabChanged(PrefabInfo info);
}
