﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class DecorationPropertiesPanel : ToolsModifierControl
{
	private void Awake()
	{
		this.m_CellWidth = base.Find<UIDropDown>("CellWidth");
		this.m_CellLength = base.Find<UIDropDown>("CellLength");
		this.m_Container = base.Find<UIScrollablePanel>("Container");
		this.m_SizePanel = base.Find<UIPanel>("Size");
		this.m_SelectRef = GameObject.Find("SelectReference").GetComponent<UIPanel>();
		this.m_SelectRef.set_isVisible(false);
	}

	private void Start()
	{
		base.get_component().Hide();
		this.m_DefaultGroupHeight = UITemplateManager.Peek(DecorationPropertiesPanel.kGroupPropertyTemplate).get_size().y;
		if (ToolsModifierControl.toolController != null)
		{
			this.Refresh(ToolsModifierControl.toolController.m_editPrefabInfo);
		}
	}

	private void OnEnable()
	{
		if (ToolsModifierControl.toolController != null)
		{
			ToolsModifierControl.toolController.eventEditPrefabChanged += new ToolController.EditPrefabChanged(this.OnEditPrefabChanged);
		}
		this.m_CellWidth.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.m_CellLength.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.BindBuildingEvents();
	}

	private void OnDisable()
	{
		if (ToolsModifierControl.toolController != null)
		{
			ToolsModifierControl.toolController.eventEditPrefabChanged -= new ToolController.EditPrefabChanged(this.OnEditPrefabChanged);
		}
		this.m_CellWidth.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.m_CellLength.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.UnBindBuildingEvents();
	}

	private void OnEditPrefabChanged(PrefabInfo info)
	{
		this.ClearBuildings();
		if (info != null && info is BuildingInfo)
		{
			this.CreateSubBuildings(info as BuildingInfo);
		}
		this.Refresh(info);
	}

	private void BindBuildingEvents()
	{
		this.UnBindBuildingEvents();
		BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
		if (tool != null)
		{
			tool.m_placementCompleted += new BuildingTool.PlacementCompleted(this.OnBuildingCreated);
		}
		BulldozeTool tool2 = ToolsModifierControl.GetTool<BulldozeTool>();
		if (tool2 != null)
		{
			tool2.m_BuildingDeleted += new BulldozeTool.BuildingDeleted(this.OnBuildingReleased);
		}
		DefaultTool tool3 = ToolsModifierControl.GetTool<DefaultTool>();
		if (tool3 != null)
		{
			tool3.m_BuildingRelocated += new DefaultTool.BuildingRelocated(this.OnBuildingRelocated);
		}
	}

	private void UnBindBuildingEvents()
	{
		BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
		if (tool != null)
		{
			tool.m_placementCompleted -= new BuildingTool.PlacementCompleted(this.OnBuildingCreated);
		}
		BulldozeTool tool2 = ToolsModifierControl.GetTool<BulldozeTool>();
		if (tool2 != null)
		{
			tool2.m_BuildingDeleted -= new BulldozeTool.BuildingDeleted(this.OnBuildingReleased);
		}
		DefaultTool tool3 = ToolsModifierControl.GetTool<DefaultTool>();
		if (tool3 != null)
		{
			tool3.m_BuildingRelocated -= new DefaultTool.BuildingRelocated(this.OnBuildingRelocated);
		}
	}

	private void Clear()
	{
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kPropertyTemplate);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kPropertyVector3Template);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kGroupPropertyTemplate);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kColorPropertyTemplate);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kPropertyEnumTemplate);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kPropertyBoolTemplate);
		UITemplateManager.ClearInstances(DecorationPropertiesPanel.kPropertyReferenceTemplate);
		this.m_GroupStates.Clear();
		this.m_TrailerCount = 0;
		this.m_TrailerAsset = null;
		this.m_TrailerOffsetComponent = null;
		this.m_TrailerOffsetField = null;
		this.UnBindBuildingEvents();
	}

	private void RebuildSizes(PrefabInfo info)
	{
		int num;
		int num2;
		info.GetWidthRange(out num, out num2);
		string[] array = new string[num2 - num + 1];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = (num2 - i).ToString();
		}
		this.m_CellWidth.set_items(array);
		info.GetLengthRange(out num, out num2);
		string[] array2 = new string[num2 - num + 1];
		for (int j = 0; j < array2.Length; j++)
		{
			array2[j] = (num2 - j).ToString();
		}
		this.m_CellLength.set_items(array2);
	}

	private void RefreshSize(UIComponent comp, int id)
	{
		PrefabInfo editPrefabInfo = ToolsModifierControl.toolController.m_editPrefabInfo;
		if (editPrefabInfo != null)
		{
			int num;
			int num2;
			float num3;
			editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
			if (comp == this.m_CellWidth)
			{
				this.m_CellLength.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
				string selectedValue = this.m_CellLength.get_selectedValue();
				editPrefabInfo.SetWidth(int.Parse(this.m_CellWidth.get_selectedValue()));
				this.RebuildSizes(editPrefabInfo);
				this.m_CellLength.set_selectedValue(selectedValue);
				this.m_CellLength.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
			}
			else if (comp == this.m_CellLength)
			{
				this.m_CellWidth.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
				string selectedValue2 = this.m_CellWidth.get_selectedValue();
				editPrefabInfo.SetLength(int.Parse(this.m_CellLength.get_selectedValue()));
				this.RebuildSizes(editPrefabInfo);
				this.m_CellWidth.set_selectedValue(selectedValue2);
				this.m_CellWidth.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
			}
			editPrefabInfo.EnsureCellSurface();
			int num4;
			int num5;
			float num6;
			editPrefabInfo.GetDecorationArea(out num4, out num5, out num6);
			float minX = (float)Mathf.Min(-num, -num4) * 4f;
			float maxX = (float)Mathf.Max(num, num4) * 4f;
			float minZ = Mathf.Min((float)(-(float)num2) * 4f + num3, (float)(-(float)num5) * 4f + num6);
			float maxZ = Mathf.Max((float)num2 * 4f + num3, (float)num5 * 4f + num6);
			TerrainModify.UpdateArea(minX, minZ, maxX, maxZ, false, true, false);
		}
	}

	private void RefreshSize(PrefabInfo info)
	{
		this.m_CellLength.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.m_CellWidth.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.RebuildSizes(info);
		this.m_CellWidth.set_selectedValue(info.GetWidth().ToString());
		this.m_CellLength.set_selectedValue(info.GetLength().ToString());
		this.m_CellWidth.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
		this.m_CellLength.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.RefreshSize));
	}

	private int PropertySort(FieldInfo a, FieldInfo b)
	{
		CustomizablePropertyAttribute[] array = a.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true) as CustomizablePropertyAttribute[];
		CustomizablePropertyAttribute[] array2 = b.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true) as CustomizablePropertyAttribute[];
		if (array.Length <= 0 || array2.Length <= 0)
		{
			return 0;
		}
		if (string.IsNullOrEmpty(array[0].group) && !string.IsNullOrEmpty(array2[0].group))
		{
			return 1;
		}
		if (!string.IsNullOrEmpty(array[0].group) && string.IsNullOrEmpty(array2[0].group))
		{
			return -1;
		}
		int num = string.Compare(array[0].group, array2[0].group);
		if (num != 0)
		{
			return num;
		}
		if (array[0].priority > array2[0].priority)
		{
			return 1;
		}
		if (array[0].priority < array2[0].priority)
		{
			return -1;
		}
		return string.Compare(array[0].name, array2[0].name);
	}

	public void ProcessIndirectFields()
	{
		BuildingInfo buildingInfo = ToolsModifierControl.toolController.m_editPrefabInfo as BuildingInfo;
		VehicleInfo vehicleInfo = ToolsModifierControl.toolController.m_editPrefabInfo as VehicleInfo;
		PropInfo propInfo = ToolsModifierControl.toolController.m_editPrefabInfo as PropInfo;
		TreeInfo treeInfo = ToolsModifierControl.toolController.m_editPrefabInfo as TreeInfo;
		CitizenInfo citizenInfo = ToolsModifierControl.toolController.m_editPrefabInfo as CitizenInfo;
		if (buildingInfo != null)
		{
			MilestoneInfo unlockMilestone;
			if (this.m_SelectedMilestone == DecorationPropertiesPanel.kUseTemplateMilestone && ToolsModifierControl.toolController.m_templatePrefabInfo != null)
			{
				buildingInfo.m_UnlockMilestone = ((BuildingInfo)ToolsModifierControl.toolController.m_templatePrefabInfo).m_UnlockMilestone;
			}
			else if (this.m_SelectedMilestone == DecorationPropertiesPanel.kAlwaysUnlocked)
			{
				buildingInfo.m_UnlockMilestone = null;
			}
			else if (Singleton<UnlockManager>.get_instance().m_allMilestones.TryGetValue(this.m_SelectedMilestone, out unlockMilestone))
			{
				buildingInfo.m_UnlockMilestone = unlockMilestone;
			}
			else
			{
				buildingInfo.m_UnlockMilestone = null;
			}
			buildingInfo.m_useColorVariations = this.m_UseColorVariations;
			if (buildingInfo.m_subBuildings.Length > 0)
			{
				this.UnBindBuildingEvents();
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				foreach (KeyValuePair<ushort, BuildingInfo.SubInfo> current in this.m_SubBuildings)
				{
					ushort key = current.Key;
					BuildingInfo buildingInfo2 = current.Value.m_buildingInfo;
					Vector3 vector;
					vector..ctor(current.Value.m_position.x, current.Value.m_position.y + 60f, current.Value.m_position.z);
					float num = current.Value.m_angle * 0.0174532924f;
					Building building = instance.m_buildings.m_buffer[(int)key];
					bool flag = (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)key].m_flags & Building.Flags.FixedHeight) != Building.Flags.None != current.Value.m_fixedHeight;
					if (current.Value.m_fixedHeight)
					{
						Building[] expr_21C_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
						ushort expr_21C_cp_1 = key;
						expr_21C_cp_0[(int)expr_21C_cp_1].m_flags = (expr_21C_cp_0[(int)expr_21C_cp_1].m_flags | Building.Flags.FixedHeight);
					}
					else
					{
						Building[] expr_245_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
						ushort expr_245_cp_1 = key;
						expr_245_cp_0[(int)expr_245_cp_1].m_flags = (expr_245_cp_0[(int)expr_245_cp_1].m_flags & ~Building.Flags.FixedHeight);
					}
					if (buildingInfo2 != building.Info)
					{
						instance.UpdateBuildingInfo(key, buildingInfo2);
					}
					if (vector != building.m_position || num != building.m_angle || flag)
					{
						instance.RelocateBuilding(key, vector, num);
					}
				}
				this.SaveSubBuildings();
				this.BindBuildingEvents();
			}
		}
		if (vehicleInfo != null)
		{
			VehicleInfo trailerAsset = this.m_TrailerAsset;
			if (trailerAsset != null)
			{
				if (this.m_TrailerOffsetComponent != null)
				{
					DecorationPropertiesPanel.ReflectionInfo reflectionInfo = this.m_TrailerOffsetField.get_objectUserData() as DecorationPropertiesPanel.ReflectionInfo;
					if (reflectionInfo != null && (VehicleInfo)reflectionInfo.targetObject != trailerAsset)
					{
						reflectionInfo.targetObject = trailerAsset;
						this.m_TrailerOffsetField.set_objectUserData(reflectionInfo);
						this.m_TrailerOffsetField.set_text(trailerAsset.m_attachOffsetFront.ToString());
						this.m_TrailerOffsetComponent.set_isVisible(trailerAsset != null);
					}
				}
				if (this.m_TrailerCount > DecorationPropertiesPanel.m_MaxTrailers)
				{
					this.m_TrailerCount = DecorationPropertiesPanel.m_MaxTrailers;
					if (this.m_TrailerCountField != null)
					{
						this.m_TrailerCountField.set_color(Color.get_red());
					}
				}
				vehicleInfo.m_trailers = new VehicleInfo.VehicleTrailer[this.m_TrailerCount];
				for (int i = 0; i < this.m_TrailerCount; i++)
				{
					vehicleInfo.m_trailers[i].m_info = trailerAsset;
					vehicleInfo.m_trailers[i].m_invertProbability = 0;
					vehicleInfo.m_trailers[i].m_probability = 100;
				}
				this.CopyTrailerColorFromMain();
			}
			else
			{
				vehicleInfo.m_trailers = null;
				if (this.m_TrailerOffsetComponent != null)
				{
					this.m_TrailerOffsetComponent.set_isVisible(false);
				}
			}
		}
		if (propInfo != null)
		{
			int num2 = this.m_Variations.Length;
			for (int j = 0; j < num2; j++)
			{
				PropInfo.Variation variation = default(PropInfo.Variation);
				variation.m_prop = (this.m_Variations[j].m_object as PropInfo);
				variation.m_probability = this.m_Variations[j].m_probability;
				propInfo.m_variations[j] = variation;
			}
		}
		if (treeInfo != null)
		{
			int num3 = this.m_Variations.Length;
			for (int k = 0; k < num3; k++)
			{
				TreeInfo.Variation variation2 = new TreeInfo.Variation();
				variation2.m_tree = (this.m_Variations[k].m_object as TreeInfo);
				variation2.m_probability = this.m_Variations[k].m_probability;
				treeInfo.m_variations[k] = variation2;
			}
		}
		if (citizenInfo != null)
		{
			float num4 = 0f;
			int num5 = 0;
			if (citizenInfo.GetAI() is ResidentAI)
			{
				if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Idle2)
				{
					num5 = 4;
				}
				else if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Walk)
				{
					num4 = 1f;
				}
				else if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Panic)
				{
					num5 = 1;
				}
				else if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Sit)
				{
					num5 = 2;
				}
				else if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Bike1)
				{
					num5 = 3;
					num4 = 1f;
				}
				else if (this.m_PreviewAnimation == DecorationPropertiesPanel.PreviewAnimation.Bike2)
				{
					num5 = 3;
				}
			}
			else if (this.m_SpecialPreviewAnimation == DecorationPropertiesPanel.SpecialPreviewAnimation.Move)
			{
				num4 = 1f;
			}
			Animator component = citizenInfo.GetComponent<Animator>();
			component.SetFloat(Singleton<CitizenManager>.get_instance().ID_Speed, num4);
			component.SetInteger(Singleton<CitizenManager>.get_instance().ID_State, num5);
		}
	}

	private void AddVehicleFields(VehicleInfo info)
	{
		if (info.m_placementStyle == ItemClass.Placement.Procedural)
		{
			return;
		}
		BicycleAI bicycleAI = info.GetComponent<PrefabAI>() as BicycleAI;
		if (bicycleAI != null)
		{
			return;
		}
		int num = 0;
		string text = null;
		if (info.m_trailers != null && info.m_trailers.Length > 0)
		{
			num = info.m_trailers.Length;
			text = info.m_trailers[0].m_info.get_name();
		}
		float width;
		UIComponent container = this.FindGroup("Trailers", out width);
		UIComponent uIComponent = this.AddField(container, "Trailer count", width, typeof(int), "m_TrailerCount", this, num);
		this.m_TrailerCountField = uIComponent.Find<UITextField>("Value");
		this.AddField(container, "Trailer asset", width, typeof(VehicleInfo), "m_TrailerAsset", this, text);
		VehicleInfo vehicleInfo = (text == null) ? null : PrefabCollection<VehicleInfo>.FindLoaded(text);
		float num2 = 0f;
		if (vehicleInfo != null)
		{
			num2 = vehicleInfo.m_attachOffsetFront;
		}
		this.m_TrailerOffsetComponent = this.AddField(container, "Trailer offset", width, typeof(float), "m_attachOffsetFront", vehicleInfo, num2);
		if (this.m_TrailerOffsetComponent != null)
		{
			this.m_TrailerOffsetField = this.m_TrailerOffsetComponent.Find<UITextField>("Value");
		}
		if (vehicleInfo == null)
		{
			this.m_TrailerOffsetComponent.set_isVisible(false);
		}
		this.m_TrailerCount = num;
		if (text != null)
		{
			GameObject gameObject = PrefabCollection<VehicleInfo>.FindLoaded(text).get_gameObject();
			this.m_TrailerAsset = (this.InstantiateReference(gameObject) as VehicleInfo);
		}
		else
		{
			this.m_TrailerAsset = null;
		}
	}

	private PrefabInfo InstantiateReference(GameObject template)
	{
		PrefabInfo component = Object.Instantiate<GameObject>(template).GetComponent<PrefabInfo>();
		component.get_gameObject().set_name(template.get_name());
		component.get_gameObject().SetActive(false);
		VehicleInfo vehicleInfo = component as VehicleInfo;
		if (vehicleInfo != null)
		{
			ImportAssetLodded.SetLODColors(component.get_gameObject());
			vehicleInfo.m_generatedInfo = ScriptableObject.CreateInstance<VehicleInfoGen>();
			vehicleInfo.m_generatedInfo.set_name(component.get_gameObject().get_name() + " (GeneratedInfo)");
			vehicleInfo.CalculateGeneratedInfo();
			VehicleInfo component2 = template.GetComponent<VehicleInfo>();
			if (component2.m_lodObject != null)
			{
				vehicleInfo.m_lodObject = Object.Instantiate<GameObject>(component2.m_lodObject);
				if (vehicleInfo.m_lodObject != null)
				{
					vehicleInfo.m_lodObject.SetActive(false);
					Renderer component3 = vehicleInfo.m_lodObject.GetComponent<Renderer>();
					if (component3 != null && component3.get_sharedMaterial() != null)
					{
						component3.set_sharedMaterial(component3.get_material());
					}
				}
			}
		}
		PropInfo propInfo = component as PropInfo;
		if (propInfo != null)
		{
			propInfo.m_isCustomContent = true;
			propInfo.m_availableIn = ItemClass.Availability.Game;
			propInfo.m_variations = new PropInfo.Variation[0];
			propInfo.m_generatedInfo = ScriptableObject.CreateInstance<PropInfoGen>();
			propInfo.m_generatedInfo.set_name(component.get_gameObject().get_name() + " (GeneratedInfo)");
			propInfo.CalculateGeneratedInfo();
			PropInfo component4 = template.GetComponent<PropInfo>();
			if (component4.m_lodObject != null)
			{
				propInfo.m_lodObject = Object.Instantiate<GameObject>(component4.m_lodObject);
				if (propInfo.m_lodObject != null)
				{
					propInfo.get_gameObject().set_name(component4.m_lodObject.get_name());
					propInfo.m_lodObject.SetActive(false);
					Renderer component5 = propInfo.m_lodObject.GetComponent<Renderer>();
					if (component5 != null && component5.get_sharedMaterial() != null)
					{
						component5.set_sharedMaterial(component5.get_material());
					}
				}
			}
		}
		TreeInfo treeInfo = component as TreeInfo;
		if (treeInfo != null)
		{
			treeInfo.m_isCustomContent = true;
			treeInfo.m_availableIn = ItemClass.Availability.Game;
			treeInfo.m_variations = new TreeInfo.Variation[0];
		}
		Renderer component6 = component.GetComponent<Renderer>();
		if (component6 != null && component6.get_sharedMaterial() != null)
		{
			component6.set_sharedMaterial(component6.get_material());
		}
		component.InitializePrefab();
		component.CheckReferences();
		component.m_prefabInitialized = true;
		return component;
	}

	private void AddBuildingFields(BuildingInfo info)
	{
		float width = this.m_Container.get_size().x - (float)this.m_Container.get_autoLayoutPadding().get_horizontal();
		this.m_Milestones = new List<string>();
		if (ToolsModifierControl.toolController.m_templatePrefabInfo != null && ((BuildingInfo)ToolsModifierControl.toolController.m_templatePrefabInfo).m_UnlockMilestone != null)
		{
			this.m_Milestones.Add(DecorationPropertiesPanel.kUseTemplateMilestone);
		}
		this.m_Milestones.Add(DecorationPropertiesPanel.kAlwaysUnlocked);
		foreach (string current in Singleton<UnlockManager>.get_instance().m_allMilestones.Keys)
		{
			this.m_Milestones.Add(current);
		}
		int defaultMilestoneIndex = this.GetDefaultMilestoneIndex();
		this.m_SelectedMilestone = this.m_Milestones[defaultMilestoneIndex];
		UIDropDown uIDropDown = this.AddField(this.m_Container, "Milestone", width, typeof(List<string>), "m_SelectedMilestone", defaultMilestoneIndex, this, this.m_Milestones).Find<UIDropDown>("Value");
		uIDropDown.set_size(new Vector2(uIDropDown.get_size().x + 130f, uIDropDown.get_size().y));
		uIDropDown.set_position(new Vector3(uIDropDown.get_position().x - 130f, uIDropDown.get_position().y, uIDropDown.get_position().z));
		uIDropDown.set_autoListWidth(true);
		if (info.m_mesh != null)
		{
			float width2;
			UIComponent container = this.FindGroup("Building general", out width2);
			this.AddField(container, "Use color variations", width2, typeof(bool), "m_UseColorVariations", this, info.m_useColorVariations);
			this.m_UseColorVariations = info.m_useColorVariations;
		}
		this.BindBuildingEvents();
		if (this.m_SubBuildings.Count > 0)
		{
			float width3;
			UIComponent container2 = this.FindGroup("Sub buildings", out width3);
			int num = 0;
			foreach (BuildingInfo.SubInfo current2 in this.m_SubBuildings.Values)
			{
				this.AddField(container2, string.Empty, width3, typeof(BuildingInfo.SubInfo), string.Empty, num, current2, current2);
				num++;
			}
		}
	}

	private int GetDefaultMilestoneIndex()
	{
		MilestoneInfo unlockMilestone = ((BuildingInfo)ToolsModifierControl.toolController.m_editPrefabInfo).m_UnlockMilestone;
		int num;
		if (this.m_Milestones.Contains(DecorationPropertiesPanel.kUseTemplateMilestone) && unlockMilestone == ((BuildingInfo)ToolsModifierControl.toolController.m_templatePrefabInfo).m_UnlockMilestone)
		{
			num = this.m_Milestones.IndexOf(DecorationPropertiesPanel.kUseTemplateMilestone);
		}
		else
		{
			num = ((!(unlockMilestone != null)) ? this.m_Milestones.IndexOf(DecorationPropertiesPanel.kAlwaysUnlocked) : this.m_Milestones.IndexOf(unlockMilestone.get_name()));
			num = ((num == -1) ? 0 : num);
		}
		return num;
	}

	private void CreateSubBuildings(BuildingInfo info)
	{
		if (info.m_subBuildings == null)
		{
			info.m_subBuildings = new BuildingInfo.SubInfo[0];
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		BuildingInfo.SubInfo[] subBuildings = info.m_subBuildings;
		for (int i = 0; i < subBuildings.Length; i++)
		{
			BuildingInfo.SubInfo subInfo = subBuildings[i];
			if (subInfo.m_buildingInfo != null)
			{
				Vector3 position;
				position..ctor(subInfo.m_position.x, subInfo.m_position.y + 60f, subInfo.m_position.z);
				ushort num;
				if (instance.CreateBuilding(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, subInfo.m_buildingInfo, position, subInfo.m_angle * 0.0174532924f, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
				{
					if (subInfo.m_fixedHeight)
					{
						Building[] expr_C5_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
						ushort expr_C5_cp_1 = num;
						expr_C5_cp_0[(int)expr_C5_cp_1].m_flags = (expr_C5_cp_0[(int)expr_C5_cp_1].m_flags | Building.Flags.FixedHeight);
					}
					BuildingInfo.SubInfo value = this.CreateSubInfo(instance.m_buildings.m_buffer[(int)num], subInfo.m_fixedHeight);
					this.m_SubBuildings[num] = value;
				}
			}
		}
	}

	private void ClearBuildings()
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		for (ushort num = 1; num < 49152; num += 1)
		{
			if (instance.m_buildings.m_buffer[(int)num].m_flags != Building.Flags.None)
			{
				instance.ReleaseBuilding(num);
			}
		}
		this.m_SubBuildings = new Dictionary<ushort, BuildingInfo.SubInfo>();
	}

	private void OnBuildingCreated(ushort building)
	{
		if (ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<BuildingTool>())
		{
			ToolsModifierControl.SetTool<DefaultTool>();
		}
		Building building2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building];
		this.m_SubBuildings[building] = this.CreateSubInfo(building2, true);
		this.SaveSubBuildings();
		this.Refresh();
	}

	private void OnBuildingRelocated(ushort building)
	{
		BuildingInfo.SubInfo subInfo;
		if (this.m_SubBuildings.TryGetValue(building, out subInfo))
		{
			Building building2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building];
			subInfo.m_position = new Vector3(building2.m_position.x, building2.m_position.y - 60f, building2.m_position.z);
			subInfo.m_angle = building2.m_angle * 57.29578f;
			this.SaveSubBuildings();
			this.Refresh();
		}
	}

	private void OnBuildingReleased(ushort building)
	{
		this.m_SubBuildings.Remove(building);
		this.SaveSubBuildings();
		this.Refresh();
	}

	private void SaveSubBuildings()
	{
		BuildingInfo buildingInfo = ToolsModifierControl.toolController.m_editPrefabInfo as BuildingInfo;
		if (buildingInfo != null)
		{
			List<BuildingInfo.SubInfo> list = new List<BuildingInfo.SubInfo>();
			foreach (BuildingInfo.SubInfo current in this.m_SubBuildings.Values)
			{
				list.Add(current);
			}
			buildingInfo.m_subBuildings = list.ToArray();
		}
	}

	private BuildingInfo.SubInfo CreateSubInfo(Building building, bool fixedHeight = true)
	{
		return new BuildingInfo.SubInfo
		{
			m_buildingInfo = building.Info,
			m_position = new Vector3(building.m_position.x, building.m_position.y - 60f, building.m_position.z),
			m_angle = building.m_angle * 57.29578f,
			m_fixedHeight = fixedHeight
		};
	}

	private UIComponent FindGroup(string group, out float width)
	{
		DecorationPropertiesPanel.GroupInfo groupInfo;
		UIComponent result;
		if (!this.m_GroupStates.TryGetValue(group, out groupInfo))
		{
			UIComponent uIComponent = this.m_Container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kGroupPropertyTemplate));
			UIButton uIButton = uIComponent.Find<UIButton>("Toggle");
			uIButton.set_text(group);
			uIButton.set_stringUserData(group);
			uIButton.add_eventClick(new MouseEventHandler(this.OnGroupClicked));
			uIButton.set_normalFgSprite("PropertyGroupClosed");
			UIPanel uIPanel = uIComponent.Find<UIPanel>("GroupContainer");
			uIPanel.Hide();
			uIPanel.set_size(new Vector2(uIPanel.get_size().x, 0f));
			this.m_GroupStates.Add(group, new DecorationPropertiesPanel.GroupInfo
			{
				m_Folded = true,
				m_Container = uIComponent,
				m_PropertyContainer = uIPanel
			});
			result = uIPanel;
			width = uIPanel.get_size().x - (float)uIPanel.get_autoLayoutPadding().get_horizontal();
		}
		else
		{
			result = groupInfo.m_PropertyContainer;
			width = groupInfo.m_PropertyContainer.get_size().x - (float)groupInfo.m_PropertyContainer.get_autoLayoutPadding().get_horizontal();
		}
		return result;
	}

	private UIComponent AddField(UIComponent container, string locale, float width, Type type, string name, object target, object initialValue)
	{
		return this.AddField(container, locale, width, type, name, -1, target, initialValue);
	}

	private UIComponent AddField(UIComponent container, string locale, float width, Type type, string name, int arrayIndex, object target, object initialValue)
	{
		UIComponent uIComponent = null;
		if (type.IsEnum)
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyEnumTemplate));
			UIDropDown uIDropDown = uIComponent.Find<UIDropDown>("Value");
			uIDropDown.set_objectUserData(target);
			uIDropDown.set_stringUserData(name);
			string[] names = Enum.GetNames(type);
			uIDropDown.set_items(names);
			int[] array = Enum.GetValues(type) as int[];
			int num = (int)initialValue;
			for (int i = 0; i < names.Length; i++)
			{
				if (num == array[i])
				{
					uIDropDown.set_selectedIndex(i);
					break;
				}
			}
			uIDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConfirmEnum));
		}
		else if (type == typeof(List<string>))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyEnumTemplate));
			UIDropDown uIDropDown2 = uIComponent.Find<UIDropDown>("Value");
			uIDropDown2.set_objectUserData(target);
			uIDropDown2.set_stringUserData(name);
			uIDropDown2.set_items((initialValue as List<string>).ToArray());
			uIDropDown2.set_selectedIndex(arrayIndex);
			uIDropDown2.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConfirmListSelection));
		}
		else if (type.IsArray)
		{
			Type type2 = target.GetType();
			FieldInfo field = type2.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
			Array array2 = field.GetValue(target) as Array;
			if (array2 == null && type2 == typeof(VehicleInfo) && name == "m_lightPositions")
			{
				array2 = new Vector3[]
				{
					new Vector3(-0.9f, 1f, -0.1f),
					new Vector3(0.9f, 1f, -0.1f)
				};
				field.SetValue(target, array2);
			}
			if (array2 != null)
			{
				for (int j = 0; j < array2.Length; j++)
				{
					this.AddField(container, locale + j.ToString(), width, type.GetElementType(), name, j, target, array2.GetValue(j));
				}
			}
		}
		else if (type == typeof(Vector3))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyVector3Template));
			UITextField uITextField = uIComponent.Find<UITextField>("ValueX");
			uITextField.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnConfirmChange));
			uITextField.set_allowFloats(true);
			uITextField.set_text(((Vector3)initialValue).x.ToString("0.000"));
			uITextField.set_objectUserData(new DecorationPropertiesPanel.ReflectionInfo
			{
				targetObject = target,
				fieldName = name,
				elementIndex = arrayIndex,
				nestedInfo = new DecorationPropertiesPanel.ReflectionInfo(),
				nestedInfo = 
				{
					targetObject = target,
					fieldName = "x"
				}
			});
			UIComponent uIComponent2 = uIComponent.Find("X");
			uIComponent2.set_hoverCursor(this.m_DragCursorInfo);
			uIComponent2.set_objectUserData(uITextField);
			uIComponent2.add_eventMouseDown(new MouseEventHandler(this.OnStartDragValue));
			uIComponent2.add_eventMouseMove(new MouseEventHandler(this.OnDragValue));
			uIComponent2.add_eventMouseUp(new MouseEventHandler(this.OnStopDragValue));
			UITextField uITextField2 = uIComponent.Find<UITextField>("ValueY");
			uITextField2.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnConfirmChange));
			uITextField2.set_allowFloats(true);
			uITextField2.set_text(((Vector3)initialValue).y.ToString("0.000"));
			uITextField2.set_objectUserData(new DecorationPropertiesPanel.ReflectionInfo
			{
				targetObject = target,
				fieldName = name,
				elementIndex = arrayIndex,
				nestedInfo = new DecorationPropertiesPanel.ReflectionInfo(),
				nestedInfo = 
				{
					targetObject = target,
					fieldName = "y"
				}
			});
			UIComponent uIComponent3 = uIComponent.Find("Y");
			uIComponent3.set_hoverCursor(this.m_DragCursorInfo);
			uIComponent3.set_objectUserData(uITextField2);
			uIComponent3.add_eventMouseDown(new MouseEventHandler(this.OnStartDragValue));
			uIComponent3.add_eventMouseMove(new MouseEventHandler(this.OnDragValue));
			uIComponent3.add_eventMouseUp(new MouseEventHandler(this.OnStopDragValue));
			UITextField uITextField3 = uIComponent.Find<UITextField>("ValueZ");
			uITextField3.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnConfirmChange));
			uITextField3.set_allowFloats(true);
			uITextField3.set_text(((Vector3)initialValue).z.ToString("0.000"));
			uITextField3.set_objectUserData(new DecorationPropertiesPanel.ReflectionInfo
			{
				targetObject = target,
				fieldName = name,
				elementIndex = arrayIndex,
				nestedInfo = new DecorationPropertiesPanel.ReflectionInfo(),
				nestedInfo = 
				{
					targetObject = target,
					fieldName = "z"
				}
			});
			UIComponent uIComponent4 = uIComponent.Find("Z");
			uIComponent4.set_hoverCursor(this.m_DragCursorInfo);
			uIComponent4.set_objectUserData(uITextField3);
			uIComponent4.add_eventMouseDown(new MouseEventHandler(this.OnStartDragValue));
			uIComponent4.add_eventMouseMove(new MouseEventHandler(this.OnDragValue));
			uIComponent4.add_eventMouseUp(new MouseEventHandler(this.OnStopDragValue));
		}
		else if (type == typeof(int) || type == typeof(float))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyTemplate));
			UITextField uITextField4 = uIComponent.Find<UITextField>("Value");
			uITextField4.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnConfirmChange));
			uITextField4.set_objectUserData(new DecorationPropertiesPanel.ReflectionInfo
			{
				targetObject = target,
				fieldName = name,
				elementIndex = arrayIndex
			});
			uITextField4.set_allowFloats(type == typeof(float));
			uITextField4.set_text(initialValue.ToString());
			if (type == typeof(float))
			{
				UIComponent uIComponent5 = uIComponent.Find("Name");
				uIComponent5.set_hoverCursor(this.m_DragCursorInfo);
				uIComponent5.set_objectUserData(uITextField4);
				uIComponent5.add_eventMouseDown(new MouseEventHandler(this.OnStartDragValue));
				uIComponent5.add_eventMouseMove(new MouseEventHandler(this.OnDragValue));
				uIComponent5.add_eventMouseUp(new MouseEventHandler(this.OnStopDragValue));
			}
		}
		else if (type == typeof(bool))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyBoolTemplate));
			UICheckBox uICheckBox = uIComponent.Find<UICheckBox>("Value");
			uICheckBox.set_isChecked((bool)initialValue);
			uICheckBox.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnConfirmBool));
			uICheckBox.set_objectUserData(target);
			uICheckBox.set_stringUserData(name);
		}
		else if (type == typeof(VehicleInfo) || type == typeof(BuildingInfo))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyReferenceTemplate));
			UIButton uIButton = uIComponent.Find<UIButton>("SelectButton");
			uIButton.set_objectUserData(target);
			uIButton.set_stringUserData(name);
			uIButton.set_text((initialValue == null) ? "..." : ((string)initialValue));
			uIButton.add_eventClick(new MouseEventHandler(this.OnSelectReference));
		}
		else if (type == typeof(DecorationPropertiesPanel.VariationField))
		{
			DecorationPropertiesPanel.VariationField variationField = initialValue as DecorationPropertiesPanel.VariationField;
			this.AddField(container, "Prop [" + (arrayIndex + 1).ToString() + "]", width, typeof(PropInfo), "m_object", variationField, (!(variationField.m_object != null)) ? null : variationField.m_object.get_name());
			this.AddField(container, "Probability [" + (arrayIndex + 1).ToString() + "]", width, typeof(int), "m_probability", variationField, variationField.m_probability);
		}
		else if (type == typeof(PropInfo) || type == typeof(TreeInfo))
		{
			uIComponent = container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyReferenceTemplate));
			UIButton uIButton2 = uIComponent.Find<UIButton>("SelectButton");
			uIButton2.set_objectUserData(target);
			uIButton2.set_stringUserData(name);
			uIButton2.set_text((initialValue == null) ? "..." : ((string)initialValue));
			uIButton2.add_eventClick(new MouseEventHandler(this.OnSelectReference));
		}
		else if (type == typeof(BuildingInfo.SubInfo))
		{
			BuildingInfo.SubInfo subInfo = (BuildingInfo.SubInfo)target;
			string initialValue2 = (!(subInfo.m_buildingInfo != null)) ? null : subInfo.m_buildingInfo.get_name();
			this.AddField(container, "Building [" + arrayIndex + "]", width, typeof(BuildingInfo), "m_buildingInfo", target, initialValue2);
			this.AddField(container, "Position [" + arrayIndex + "]", width, typeof(Vector3), "m_position", target, subInfo.m_position);
			this.AddField(container, "Angle [" + arrayIndex + "]", width, typeof(float), "m_angle", target, subInfo.m_angle);
			this.AddField(container, "Fixed height [" + arrayIndex + "]", width, typeof(bool), "m_fixedHeight", target, subInfo.m_fixedHeight);
		}
		if (uIComponent != null)
		{
			float num2 = 5f;
			uIComponent.set_relativePosition(new Vector3(num2, uIComponent.get_relativePosition().y, uIComponent.get_relativePosition().z));
			uIComponent.set_size(new Vector2(width - num2 * 2f, uIComponent.get_size().y));
			uIComponent.Find<UITextComponent>("Name").set_text(locale);
		}
		return uIComponent;
	}

	private void OnConfirmListSelection(UIComponent comp, int selected)
	{
		FieldInfo field = comp.get_objectUserData().GetType().GetField(comp.get_stringUserData(), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
		UIDropDown uIDropDown = comp.Find<UIDropDown>("Value");
		field.SetValue(comp.get_objectUserData(), uIDropDown.get_selectedValue());
		this.ProcessIndirectFields();
	}

	private void AddAutomaticFields(object target)
	{
		List<FieldInfo> list = new List<FieldInfo>();
		FieldInfo[] fields = target.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
		for (int i = 0; i < fields.Length; i++)
		{
			FieldInfo fieldInfo = fields[i];
			if (fieldInfo.IsDefined(typeof(CustomizablePropertyAttribute), true))
			{
				list.Add(fieldInfo);
			}
		}
		list.Sort(new Comparison<FieldInfo>(this.PropertySort));
		for (int j = 0; j < list.Count; j++)
		{
			FieldInfo fieldInfo2 = list[j];
			CustomizablePropertyAttribute[] array = fieldInfo2.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true) as CustomizablePropertyAttribute[];
			if (array.Length > 0)
			{
				UIComponent container = this.m_Container;
				float width = this.m_Container.get_size().x - (float)this.m_Container.get_autoLayoutPadding().get_horizontal();
				string name = array[0].name;
				string group = array[0].group;
				if (!string.IsNullOrEmpty(group))
				{
					container = this.FindGroup(group, out width);
				}
				this.AddField(container, name, width, fieldInfo2.FieldType, fieldInfo2.Name, target, fieldInfo2.GetValue(target));
			}
		}
	}

	private void AddMaterialFields(PrefabInfo info, DecorationPropertiesPanel.MatInfo[] matProps)
	{
		string text = "Material";
		for (int i = 0; i < matProps.Length; i++)
		{
			DecorationPropertiesPanel.MatInfo matInfo = matProps[i];
			UIComponent uIComponent = this.m_Container;
			float num = this.m_Container.get_size().x - (float)this.m_Container.get_autoLayoutPadding().get_horizontal();
			DecorationPropertiesPanel.GroupInfo groupInfo;
			if (!this.m_GroupStates.TryGetValue(text, out groupInfo))
			{
				UIComponent uIComponent2 = this.m_Container.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kGroupPropertyTemplate));
				UIButton uIButton = uIComponent2.Find<UIButton>("Toggle");
				uIButton.set_text(text);
				uIButton.set_stringUserData(text);
				uIButton.add_eventClick(new MouseEventHandler(this.OnGroupClicked));
				uIButton.set_normalFgSprite("PropertyGroupClosed");
				UIPanel uIPanel = uIComponent2.Find<UIPanel>("GroupContainer");
				uIPanel.Hide();
				uIPanel.set_size(new Vector2(uIPanel.get_size().x, 0f));
				this.m_GroupStates.Add(text, new DecorationPropertiesPanel.GroupInfo
				{
					m_Folded = true,
					m_Container = uIComponent2,
					m_PropertyContainer = uIPanel
				});
				uIComponent = uIPanel;
				num = uIPanel.get_size().x - (float)uIPanel.get_autoLayoutPadding().get_horizontal();
			}
			else
			{
				uIComponent = groupInfo.m_PropertyContainer;
				num = groupInfo.m_PropertyContainer.get_size().x - (float)groupInfo.m_PropertyContainer.get_autoLayoutPadding().get_horizontal();
			}
			string name = matInfo.name;
			if (matInfo.type == "Color")
			{
				UIComponent uIComponent3 = uIComponent.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kColorPropertyTemplate));
				float num2 = 5f;
				uIComponent3.set_relativePosition(new Vector3(num2, uIComponent3.get_relativePosition().y, uIComponent3.get_relativePosition().z));
				uIComponent3.set_size(new Vector2(num - num2 * 2f, uIComponent3.get_size().y));
				uIComponent3.Find<UITextComponent>("Name").set_text(name);
				UIColorField uIColorField = uIComponent3.Find<UIColorField>("Value");
				uIColorField.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnConfirmColor));
				uIColorField.set_objectUserData(info.GetComponent<Renderer>().get_sharedMaterial());
				uIColorField.set_stringUserData(matInfo.uniform);
				Color color = info.GetComponent<Renderer>().get_sharedMaterial().GetColor(matInfo.uniform);
				color.a = 1f;
				uIColorField.set_selectedColor(color);
			}
			else if (matInfo.type == "Float")
			{
				UIComponent uIComponent4 = uIComponent.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyTemplate));
				float num3 = 5f;
				uIComponent4.set_relativePosition(new Vector3(num3, uIComponent4.get_relativePosition().y, uIComponent4.get_relativePosition().z));
				uIComponent4.set_size(new Vector2(num - num3 * 2f, uIComponent4.get_size().y));
				uIComponent4.Find<UITextComponent>("Name").set_text(name);
				UITextField uITextField = uIComponent4.Find<UITextField>("Value");
				uITextField.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnConfirmMaterialChange));
				uITextField.set_objectUserData(info.GetComponent<Renderer>().get_sharedMaterial());
				uITextField.set_stringUserData(matInfo.uniform);
				uITextField.set_allowFloats(true);
				uITextField.set_text(this.GetUniformComponent(info.GetComponent<Renderer>().get_sharedMaterial(), matInfo.uniform).ToString());
			}
			else if (matInfo.type == "Bool")
			{
				UIComponent uIComponent5 = uIComponent.AttachUIComponent(UITemplateManager.GetAsGameObject(DecorationPropertiesPanel.kPropertyBoolTemplate));
				float num4 = 5f;
				uIComponent5.set_relativePosition(new Vector3(num4, uIComponent5.get_relativePosition().y, uIComponent5.get_relativePosition().z));
				uIComponent5.set_size(new Vector2(num - num4 * 2f, uIComponent5.get_size().y));
				uIComponent5.Find<UITextComponent>("Name").set_text(name);
				UICheckBox uICheckBox = uIComponent5.Find<UICheckBox>("Value");
				Material sharedMaterial = info.GetComponent<Renderer>().get_sharedMaterial();
				uICheckBox.set_isChecked(sharedMaterial.GetFloat(matInfo.uniform) != 0f);
				uICheckBox.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnConfirmMaterialChangeBool));
				uICheckBox.set_objectUserData(sharedMaterial);
				uICheckBox.set_stringUserData(matInfo.uniform);
			}
		}
	}

	public void Refresh()
	{
		if (ToolsModifierControl.toolController != null)
		{
			this.Refresh(ToolsModifierControl.toolController.m_editPrefabInfo);
		}
	}

	public void RefreshVisibility(PrefabInfo info)
	{
		if (info != null)
		{
			BuildingInfo buildingInfo = info as BuildingInfo;
			if ((buildingInfo != null && buildingInfo.GetAI() is IntersectionAI) || info is NetInfo)
			{
				base.get_component().Hide();
			}
			else
			{
				base.get_component().Show();
			}
		}
		else
		{
			base.get_component().Hide();
		}
	}

	private void Refresh(PrefabInfo info)
	{
		this.Clear();
		if (info != null)
		{
			BuildingInfo buildingInfo = info as BuildingInfo;
			VehicleInfo vehicleInfo = info as VehicleInfo;
			TreeInfo treeInfo = info as TreeInfo;
			PropInfo propInfo = info as PropInfo;
			CitizenInfo citizenInfo = info as CitizenInfo;
			NetInfo netInfo = info as NetInfo;
			if (netInfo != null)
			{
				base.get_component().Hide();
				return;
			}
			if (buildingInfo != null)
			{
				if (buildingInfo.GetAI() is IntersectionAI)
				{
					base.get_component().Hide();
					return;
				}
				this.m_SizePanel.set_isVisible(true);
				base.get_component().Show();
				this.AddBuildingFields(buildingInfo);
				this.RefreshSize(info);
			}
			else if (vehicleInfo != null)
			{
				this.m_SizePanel.set_isVisible(false);
				base.get_component().Show();
				this.AddVehicleFields(vehicleInfo);
			}
			else if (treeInfo != null)
			{
				this.m_SizePanel.set_isVisible(false);
				base.get_component().Show();
				this.AddPropVariationFields(treeInfo);
			}
			else if (propInfo != null)
			{
				this.m_SizePanel.set_isVisible(false);
				base.get_component().Show();
				this.AddPropVariationFields(propInfo);
			}
			else
			{
				if (!(citizenInfo != null))
				{
					return;
				}
				this.m_SizePanel.set_isVisible(false);
				base.get_component().Show();
				this.AddCitizenFields(citizenInfo);
			}
			this.AddAutomaticFields(info);
			PrefabAI aI = info.GetAI();
			if (aI != null)
			{
				this.AddAutomaticFields(aI);
			}
			if (info.GetComponent<Renderer>() != null)
			{
				if (treeInfo == null)
				{
					List<DecorationPropertiesPanel.MatInfo> list = new List<DecorationPropertiesPanel.MatInfo>(DecorationPropertiesPanel.matPropsCommon);
					if (buildingInfo != null)
					{
						list.AddRange(DecorationPropertiesPanel.matPropsBuildings);
					}
					this.AddMaterialFields(info, list.ToArray());
				}
				else
				{
					List<DecorationPropertiesPanel.MatInfo> list2 = new List<DecorationPropertiesPanel.MatInfo>(DecorationPropertiesPanel.matPropsTrees);
					this.AddMaterialFields(info, list2.ToArray());
				}
			}
		}
		else
		{
			base.get_component().Hide();
		}
	}

	private void AddCitizenFields(CitizenInfo info)
	{
		this.m_PreviewAnimation = DecorationPropertiesPanel.PreviewAnimation.Idle1;
		this.m_SpecialPreviewAnimation = DecorationPropertiesPanel.SpecialPreviewAnimation.Idle;
		float width = this.m_Container.get_size().x - (float)this.m_Container.get_autoLayoutPadding().get_horizontal();
		if (info.GetAI() is ResidentAI)
		{
			this.AddField(this.m_Container, "Preview animation", width, typeof(DecorationPropertiesPanel.PreviewAnimation), "m_PreviewAnimation", this, this.m_PreviewAnimation);
		}
		else
		{
			this.AddField(this.m_Container, "Preview animation", width, typeof(DecorationPropertiesPanel.SpecialPreviewAnimation), "m_SpecialPreviewAnimation", this, this.m_SpecialPreviewAnimation);
		}
		this.AddField(this.m_Container, "Gender", width, typeof(Citizen.Gender), "m_gender", info, info.m_gender);
		this.AddField(this.m_Container, "Age phase", width, typeof(Citizen.AgePhase), "m_agePhase", info, info.m_agePhase);
	}

	private void AddPropVariationFields(PrefabInfo info)
	{
		PropInfo propInfo = info as PropInfo;
		TreeInfo treeInfo = info as TreeInfo;
		if (propInfo != null)
		{
			int num = propInfo.m_variations.Length;
			this.m_Variations = new DecorationPropertiesPanel.VariationField[num];
			if (num > 0)
			{
				float width;
				UIComponent container = this.FindGroup("Variations", out width);
				for (int i = 0; i < num; i++)
				{
					this.m_Variations[i] = new DecorationPropertiesPanel.VariationField();
					if (propInfo.m_variations[i].m_prop != null && propInfo.m_variations[i].m_prop.m_isCustomContent)
					{
						this.m_Variations[i].m_object = propInfo.m_variations[i].m_prop;
						this.m_Variations[i].m_probability = propInfo.m_variations[i].m_probability;
					}
				}
				this.AddField(container, string.Empty, width, typeof(DecorationPropertiesPanel.VariationField[]), "m_Variations", this, this.m_Variations);
			}
		}
		else if (treeInfo != null)
		{
			int num2 = treeInfo.m_variations.Length;
			this.m_Variations = new DecorationPropertiesPanel.VariationField[num2];
			if (num2 > 0)
			{
				float width;
				UIComponent container2 = this.FindGroup("Variations", out width);
				for (int j = 0; j < num2; j++)
				{
					this.m_Variations[j] = new DecorationPropertiesPanel.VariationField();
					if (treeInfo.m_variations[j].m_tree != null && treeInfo.m_variations[j].m_tree.m_isCustomContent)
					{
						this.m_Variations[j].m_object = treeInfo.m_variations[j].m_tree;
						this.m_Variations[j].m_probability = treeInfo.m_variations[j].m_probability;
					}
				}
				this.AddField(container2, string.Empty, width, typeof(DecorationPropertiesPanel.VariationField[]), "m_Variations", this, this.m_Variations);
			}
		}
	}

	private float CalculatePropertiesHeight(UIPanel comp)
	{
		float num = 0f;
		for (int i = 0; i < comp.get_childCount(); i++)
		{
			num += comp.get_components()[i].get_size().y + (float)comp.get_autoLayoutPadding().get_vertical();
		}
		return num;
	}

	private float CalculateHeight(UIPanel container)
	{
		float num = 0f;
		num += this.m_DefaultGroupHeight;
		return num + this.CalculatePropertiesHeight(container);
	}

	private void OnGroupClicked(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		if (uIButton != null && !string.IsNullOrEmpty(uIButton.get_stringUserData()))
		{
			DecorationPropertiesPanel.GroupInfo groupInfo;
			if (this.m_GroupStates.TryGetValue(uIButton.get_stringUserData(), out groupInfo))
			{
				uIButton.set_normalFgSprite((!groupInfo.m_Folded) ? "PropertyGroupClosed" : "PropertyGroupOpen");
				if (groupInfo.m_Folded)
				{
					UIPanel propertyContainer = groupInfo.m_PropertyContainer;
					propertyContainer.Show();
					float num = this.CalculateHeight(propertyContainer);
					ValueAnimator.Animate("PropGroup" + uIButton.get_stringUserData(), delegate(float val)
					{
						Vector2 size = groupInfo.m_Container.get_size();
						size.y = val;
						groupInfo.m_Container.set_size(size);
					}, new AnimatedFloat(this.m_DefaultGroupHeight, num, 0.2f));
				}
				else
				{
					UIPanel container = groupInfo.m_PropertyContainer;
					float num2 = this.CalculateHeight(container);
					ValueAnimator.Animate("PropGroup" + uIButton.get_stringUserData(), delegate(float val)
					{
						Vector2 size = groupInfo.m_Container.get_size();
						size.y = val;
						groupInfo.m_Container.set_size(size);
					}, new AnimatedFloat(num2, this.m_DefaultGroupHeight, 0.2f), delegate
					{
						container.Hide();
					});
				}
				groupInfo.m_Folded = !groupInfo.m_Folded;
				this.m_GroupStates[uIButton.get_stringUserData()] = groupInfo;
			}
		}
	}

	private float GetUniformComponent(Material mat, string uniform)
	{
		string[] array = uniform.Split(new char[]
		{
			'.'
		});
		Vector4 vector = mat.GetVector(array[0]);
		if (array[1] == "x")
		{
			return vector.x;
		}
		if (array[1] == "y")
		{
			return vector.y;
		}
		if (array[1] == "z")
		{
			return vector.z;
		}
		if (array[1] == "w")
		{
			return vector.w;
		}
		return 0f;
	}

	private void OnConfirmReference(PrefabInfo reference)
	{
		if (reference != null && !(reference is BuildingInfo))
		{
			reference = this.InstantiateReference(reference.get_gameObject());
		}
		if (reference != null && this.m_CurrentRefObject != null)
		{
			FieldInfo field = this.m_CurrentRefObject.GetType().GetField(this.m_CurrentRefName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
			field.SetValue(this.m_CurrentRefObject, reference);
			UIButton uIButton = this.m_CurrentRefComponent as UIButton;
			if (uIButton != null)
			{
				uIButton.set_text(reference.get_name());
			}
		}
		this.m_CurrentRefObject = null;
		this.m_SelectRef.set_isVisible(false);
		this.ProcessIndirectFields();
	}

	private void OnSelectReference(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_SelectRef.get_isVisible())
		{
			return;
		}
		VehicleInfo vehicleInfo = ToolsModifierControl.toolController.m_editPrefabInfo as VehicleInfo;
		PropInfo propInfo = ToolsModifierControl.toolController.m_editPrefabInfo as PropInfo;
		TreeInfo treeInfo = ToolsModifierControl.toolController.m_editPrefabInfo as TreeInfo;
		BuildingInfo buildingInfo = ToolsModifierControl.toolController.m_editPrefabInfo as BuildingInfo;
		if (vehicleInfo != null || propInfo != null || treeInfo != null || buildingInfo != null)
		{
			AssetImporterAssetTemplate component = this.m_SelectRef.GetComponent<AssetImporterAssetTemplate>();
			component.ReferenceCallback = new AssetImporterAssetTemplate.ReferenceCallbackDelegate(this.OnConfirmReference);
			component.Reset();
			AssetImporterAssetTemplate.Filter filter;
			if (vehicleInfo != null)
			{
				VehicleAI vehicleAI = vehicleInfo.GetAI() as VehicleAI;
				CarAI carAI = vehicleAI as CarAI;
				TrainAI trainAI = vehicleAI as TrainAI;
				TramAI tramAI = vehicleAI as TramAI;
				HelicopterAI helicopterAI = vehicleAI as HelicopterAI;
				if (carAI != null)
				{
					filter = AssetImporterAssetTemplate.Filter.CarTrailers;
				}
				else if (trainAI != null)
				{
					filter = AssetImporterAssetTemplate.Filter.TrainCars;
				}
				else if (tramAI != null)
				{
					filter = AssetImporterAssetTemplate.Filter.TramTrailer;
				}
				else if (helicopterAI != null)
				{
					filter = AssetImporterAssetTemplate.Filter.HelicopterTrailer;
				}
				else
				{
					filter = AssetImporterAssetTemplate.Filter.Nothing;
				}
			}
			else if (propInfo != null)
			{
				filter = AssetImporterAssetTemplate.Filter.CustomProps;
			}
			else if (treeInfo != null)
			{
				filter = AssetImporterAssetTemplate.Filter.CustomTrees;
			}
			else if (buildingInfo != null)
			{
				filter = AssetImporterAssetTemplate.Filter.Buildings;
			}
			else
			{
				filter = AssetImporterAssetTemplate.Filter.Nothing;
			}
			component.RefreshWithFilter(filter);
			this.m_CurrentRefComponent = comp;
			this.m_CurrentRefObject = comp.get_objectUserData();
			this.m_CurrentRefName = comp.get_stringUserData();
			this.m_SelectRef.set_isVisible(true);
		}
	}

	private void CopyTrailerColorFromMain()
	{
		VehicleInfo vehicleInfo = ToolsModifierControl.toolController.m_editPrefabInfo as VehicleInfo;
		if (vehicleInfo != null)
		{
			Renderer component = vehicleInfo.GetComponent<Renderer>();
			if (component != null && component.get_sharedMaterial() != null)
			{
				Color color = component.get_sharedMaterial().GetColor("_Color");
				if (vehicleInfo.m_trailers != null && vehicleInfo.m_trailers.Length > 0)
				{
					VehicleInfo info = vehicleInfo.m_trailers[0].m_info;
					Renderer component2 = info.GetComponent<Renderer>();
					if (component2 != null && component2.get_sharedMaterial() != null)
					{
						component2.get_sharedMaterial().SetColor("_Color", color);
					}
					if (info.m_lodObject != null)
					{
						Renderer component3 = info.m_lodObject.GetComponent<Renderer>();
						if (component3 != null && component3.get_sharedMaterial() != null)
						{
							component3.get_sharedMaterial().SetColor("_Color", color);
						}
					}
				}
			}
		}
	}

	private void OnConfirmColor(UIComponent comp, Color color)
	{
		Material material = comp.get_objectUserData() as Material;
		VehicleInfo vehicleInfo = ToolsModifierControl.toolController.m_editPrefabInfo as VehicleInfo;
		BuildingInfo buildingInfo = ToolsModifierControl.toolController.m_editPrefabInfo as BuildingInfo;
		PropInfo propInfo = ToolsModifierControl.toolController.m_editPrefabInfo as PropInfo;
		Renderer renderer = null;
		if (vehicleInfo != null)
		{
			color.a = 0f;
			if (vehicleInfo.m_lodObject != null)
			{
				renderer = vehicleInfo.m_lodObject.GetComponent<Renderer>();
			}
			this.CopyTrailerColorFromMain();
		}
		if (buildingInfo != null && buildingInfo.m_lodObject != null)
		{
			renderer = buildingInfo.m_lodObject.GetComponent<Renderer>();
		}
		else if (propInfo != null && propInfo.m_lodObject != null)
		{
			renderer = propInfo.m_lodObject.GetComponent<Renderer>();
		}
		if (renderer != null && renderer.get_sharedMaterial() != null)
		{
			renderer.get_sharedMaterial().SetColor("_Color", color);
		}
		material.SetColor("_Color", color);
		string stringUserData = comp.get_stringUserData();
		material.SetColor(stringUserData, color);
		this.ProcessIndirectFields();
	}

	private void OnConfirmMaterialChangeBool(UIComponent comp, bool value)
	{
		Material material = comp.get_objectUserData() as Material;
		string stringUserData = comp.get_stringUserData();
		float num = (!value) ? 0f : 1f;
		material.SetFloat(stringUserData, num);
	}

	private void OnConfirmMaterialChange(UIComponent comp, string text)
	{
		Material material = comp.get_objectUserData() as Material;
		string stringUserData = comp.get_stringUserData();
		float num = 0f;
		if (float.TryParse(text, out num))
		{
			comp.set_color(Color.get_white());
		}
		else
		{
			comp.set_color(Color.get_red());
		}
		string[] array = stringUserData.Split(new char[]
		{
			'.'
		});
		Vector4 vector = material.GetVector(array[0]);
		if (array[1] == "x")
		{
			vector.x = num;
		}
		else if (array[1] == "y")
		{
			vector.y = num;
		}
		else if (array[1] == "z")
		{
			vector.z = num;
		}
		else if (array[1] == "w")
		{
			vector.w = num;
		}
		material.SetVector(array[0], vector);
		this.ProcessIndirectFields();
	}

	private void OnConfirmBool(UIComponent comp, bool value)
	{
		FieldInfo field = comp.get_objectUserData().GetType().GetField(comp.get_stringUserData(), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
		field.SetValue(comp.get_objectUserData(), value);
		this.ProcessIndirectFields();
	}

	private void OnConfirmEnum(UIComponent comp, int selected)
	{
		FieldInfo field = comp.get_objectUserData().GetType().GetField(comp.get_stringUserData(), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
		int[] array = Enum.GetValues(field.FieldType) as int[];
		field.SetValue(comp.get_objectUserData(), array[selected]);
		this.ProcessIndirectFields();
	}

	private void OnStartDragValue(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_Dragging = (p.get_source().get_objectUserData() as UITextField);
	}

	private void OnDragValue(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_Dragging != null)
		{
			DecorationPropertiesPanel.ReflectionInfo reflectionInfo = this.m_Dragging.get_objectUserData() as DecorationPropertiesPanel.ReflectionInfo;
			if (reflectionInfo == null || reflectionInfo.targetObject == null)
			{
				return;
			}
			UITextField dragging = this.m_Dragging;
			if (dragging != null)
			{
				FieldInfo field = reflectionInfo.targetObject.GetType().GetField(reflectionInfo.fieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
				Type type = field.FieldType;
				if (type.IsArray)
				{
					type = type.GetElementType();
				}
				if (type == typeof(Vector3) || type == typeof(float))
				{
					float num = p.get_moveDelta().x * 0.01f;
					float num2;
					if (float.TryParse(dragging.get_text(), out num2))
					{
						dragging.set_text((num2 + num).ToString("0.000"));
					}
				}
			}
		}
	}

	private void OnStopDragValue(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_Dragging = null;
	}

	private void OnConfirmChange(UIComponent comp, string text)
	{
		DecorationPropertiesPanel.ReflectionInfo reflectionInfo = comp.get_objectUserData() as DecorationPropertiesPanel.ReflectionInfo;
		if (comp == null || reflectionInfo == null || reflectionInfo.targetObject == null)
		{
			return;
		}
		FieldInfo field = reflectionInfo.targetObject.GetType().GetField(reflectionInfo.fieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
		Type type = field.FieldType;
		if (type.IsArray)
		{
			type = type.GetElementType();
		}
		if (type == typeof(int))
		{
			int num;
			if (int.TryParse(text, out num))
			{
				comp.set_color(Color.get_white());
				if (reflectionInfo.elementIndex == -1)
				{
					field.SetValue(reflectionInfo.targetObject, num);
				}
				else
				{
					Array array = (Array)field.GetValue(reflectionInfo.targetObject);
					array.SetValue(num, reflectionInfo.elementIndex);
				}
				this.ProcessIndirectFields();
			}
			else
			{
				comp.set_color(Color.get_red());
			}
		}
		else if (type == typeof(float))
		{
			float num2;
			if (float.TryParse(text, out num2))
			{
				comp.set_color(Color.get_white());
				if (reflectionInfo.elementIndex == -1)
				{
					field.SetValue(reflectionInfo.targetObject, num2);
				}
				else
				{
					Array array2 = (Array)field.GetValue(reflectionInfo.targetObject);
					array2.SetValue(num2, reflectionInfo.elementIndex);
				}
				this.ProcessIndirectFields();
			}
			else
			{
				comp.set_color(Color.get_red());
			}
		}
		else if (type == typeof(Vector3))
		{
			float num3;
			if (float.TryParse(text, out num3))
			{
				comp.set_color(Color.get_white());
				if (reflectionInfo.elementIndex == -1)
				{
					Vector3 vector = (Vector3)field.GetValue(reflectionInfo.targetObject);
					if (reflectionInfo.nestedInfo.fieldName == "x")
					{
						vector.x = num3;
					}
					if (reflectionInfo.nestedInfo.fieldName == "y")
					{
						vector.y = num3;
					}
					if (reflectionInfo.nestedInfo.fieldName == "z")
					{
						vector.z = num3;
					}
					field.SetValue(reflectionInfo.targetObject, vector);
				}
				else
				{
					Array array3 = (Array)field.GetValue(reflectionInfo.targetObject);
					Vector3 vector2 = (Vector3)array3.GetValue(reflectionInfo.elementIndex);
					if (reflectionInfo.nestedInfo.fieldName == "x")
					{
						vector2.x = num3;
					}
					if (reflectionInfo.nestedInfo.fieldName == "y")
					{
						vector2.y = num3;
					}
					if (reflectionInfo.nestedInfo.fieldName == "z")
					{
						vector2.z = num3;
					}
					array3.SetValue(vector2, reflectionInfo.elementIndex);
				}
				this.ProcessIndirectFields();
			}
			else
			{
				comp.set_color(Color.get_red());
			}
		}
	}

	private static readonly string kPropertyTemplate = "PropertySet";

	private static readonly string kPropertyVector3Template = "PropertyVector3Set";

	private static readonly string kGroupPropertyTemplate = "GroupPropertySet";

	private static readonly string kColorPropertyTemplate = "PropertyColorSet";

	private static readonly string kPropertyEnumTemplate = "PropertyEnumSet";

	private static readonly string kPropertyBoolTemplate = "PropertyBoolSet";

	private static readonly string kPropertyReferenceTemplate = "PropertyReferenceSet";

	private static readonly string kUseTemplateMilestone = "Use template milestone";

	private static readonly string kAlwaysUnlocked = "Always unlocked";

	public CursorInfo m_DragCursorInfo;

	private UIScrollablePanel m_Container;

	private UIPanel m_SizePanel;

	private UIDropDown m_CellWidth;

	private UIDropDown m_CellLength;

	private static readonly int m_MaxTrailers = 25;

	private int m_TrailerCount;

	private VehicleInfo m_TrailerAsset;

	private UIComponent m_TrailerOffsetComponent;

	private UITextField m_TrailerOffsetField;

	private UITextField m_TrailerCountField;

	private DecorationPropertiesPanel.VariationField[] m_Variations;

	private DecorationPropertiesPanel.PreviewAnimation m_PreviewAnimation;

	private DecorationPropertiesPanel.SpecialPreviewAnimation m_SpecialPreviewAnimation;

	private Dictionary<ushort, BuildingInfo.SubInfo> m_SubBuildings;

	private List<string> m_Milestones;

	private string m_SelectedMilestone;

	private bool m_UseColorVariations;

	private UIPanel m_SelectRef;

	private UIComponent m_CurrentRefComponent;

	private object m_CurrentRefObject;

	private string m_CurrentRefName;

	private float m_DefaultGroupHeight;

	private Dictionary<string, DecorationPropertiesPanel.GroupInfo> m_GroupStates = new Dictionary<string, DecorationPropertiesPanel.GroupInfo>();

	private static readonly DecorationPropertiesPanel.MatInfo[] matPropsCommon = new DecorationPropertiesPanel.MatInfo[]
	{
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Color",
			name = "Color Variation #0",
			uniform = "_ColorV0"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Color",
			name = "Color Variation #1",
			uniform = "_ColorV1"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Color",
			name = "Color Variation #2",
			uniform = "_ColorV2"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Color",
			name = "Color Variation #3",
			uniform = "_ColorV3"
		}
	};

	private static readonly DecorationPropertiesPanel.MatInfo[] matPropsBuildings = new DecorationPropertiesPanel.MatInfo[]
	{
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Float",
			name = "Ground Offset",
			uniform = "_FloorParams.x"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Float",
			name = "First Height",
			uniform = "_FloorParams.y"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Float",
			name = "Other Height",
			uniform = "_FloorParams.z"
		},
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Float",
			name = "Floor Count",
			uniform = "_FloorParams.w"
		}
	};

	private static readonly DecorationPropertiesPanel.MatInfo[] matPropsTrees = new DecorationPropertiesPanel.MatInfo[]
	{
		new DecorationPropertiesPanel.MatInfo
		{
			type = "Bool",
			name = "Disable snow",
			uniform = "_DisableSnow"
		}
	};

	private UITextField m_Dragging;

	public class VariationField
	{
		public PrefabInfo m_object;

		public int m_probability;
	}

	public enum PreviewAnimation
	{
		Idle1,
		Idle2,
		Walk,
		Panic,
		Sit,
		Bike1,
		Bike2
	}

	public enum SpecialPreviewAnimation
	{
		Idle,
		Move
	}

	public struct GroupInfo
	{
		public bool m_Folded;

		public UIComponent m_Container;

		public UIPanel m_PropertyContainer;
	}

	private class MatInfo
	{
		public string name;

		public string uniform;

		public string type;
	}

	private class ReflectionInfo
	{
		public object targetObject;

		public string fieldName;

		public int elementIndex = -1;

		public DecorationPropertiesPanel.ReflectionInfo nestedInfo;
	}
}
