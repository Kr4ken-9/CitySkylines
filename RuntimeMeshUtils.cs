﻿using System;
using UnityEngine;

public static class RuntimeMeshUtils
{
	public static void TransformMesh(Mesh source, Mesh dest, Matrix4x4 trans)
	{
		Vector3[] vertices = source.get_vertices();
		Vector3[] normals = source.get_normals();
		Color[] colors = source.get_colors();
		Vector2[] uv = source.get_uv();
		Vector4[] tangents = source.get_tangents();
		int[] triangles = source.get_triangles();
		for (int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = trans.MultiplyPoint(vertices[i]);
		}
		Matrix4x4 matrix4x = trans.get_inverse();
		matrix4x.m30 = 0f;
		matrix4x.m31 = 0f;
		matrix4x.m32 = 0f;
		matrix4x.m33 = 1f;
		matrix4x.m23 = 0f;
		matrix4x.m13 = 0f;
		matrix4x.m03 = 0f;
		matrix4x = matrix4x.get_transpose();
		for (int j = 0; j < normals.Length; j++)
		{
			normals[j] = matrix4x.MultiplyVector(normals[j]);
			normals[j].Normalize();
		}
		dest.Clear();
		dest.set_vertices(vertices);
		dest.set_normals(normals);
		dest.set_colors(colors);
		dest.set_uv(uv);
		dest.set_tangents(tangents);
		dest.set_triangles(triangles);
	}

	public static Mesh CopyMesh(Mesh mesh)
	{
		Mesh mesh2 = new Mesh();
		mesh2.set_name(mesh.get_name());
		mesh2.set_vertices(mesh.get_vertices());
		mesh2.set_normals(mesh.get_normals());
		mesh2.set_colors(mesh.get_colors());
		mesh2.set_uv(mesh.get_uv());
		mesh2.set_tangents(mesh.get_tangents());
		mesh2.set_triangles(mesh.get_triangles());
		return mesh2;
	}

	public static void ResetMesh(Mesh source, Mesh dest)
	{
		dest.set_name(source.get_name());
		dest.set_vertices(source.get_vertices());
		dest.set_normals(source.get_normals());
		dest.set_colors(source.get_colors());
		dest.set_uv(source.get_uv());
		dest.set_tangents(source.get_tangents());
		dest.set_triangles(source.get_triangles());
	}

	public static Vector3 CalculatePivotOffset(Mesh source)
	{
		Vector3[] vertices = source.get_vertices();
		float num = float.PositiveInfinity;
		float num2 = float.PositiveInfinity;
		float num3 = float.NegativeInfinity;
		float num4 = float.PositiveInfinity;
		float num5 = float.NegativeInfinity;
		for (int i = 0; i < vertices.Length; i++)
		{
			if (vertices[i].y < num)
			{
				num = vertices[i].y;
			}
			if (vertices[i].x < num2)
			{
				num2 = vertices[i].x;
			}
			if (vertices[i].x > num3)
			{
				num3 = vertices[i].x;
			}
			if (vertices[i].z < num4)
			{
				num4 = vertices[i].z;
			}
			if (vertices[i].z > num5)
			{
				num5 = vertices[i].z;
			}
		}
		return new Vector3(-(num2 + num3) * 0.5f, -num, -(num4 + num5) * 0.5f);
	}

	public static void ProcessMeshTransform(Mesh source, Mesh dest, Vector3 scale, bool adjustPivot, Vector3 rotation)
	{
		RuntimeMeshUtils.TransformMesh(source, dest, Matrix4x4.TRS(Vector3.get_zero(), Quaternion.Euler(rotation), Vector3.get_one()));
		Vector3 vector = Vector3.get_zero();
		if (adjustPivot)
		{
			vector = RuntimeMeshUtils.CalculatePivotOffset(dest) * scale.x;
		}
		RuntimeMeshUtils.TransformMesh(dest, dest, Matrix4x4.TRS(vector, Quaternion.get_identity(), scale));
	}
}
