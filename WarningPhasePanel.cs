﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class WarningPhasePanel : ToolsModifierControl
{
	public bool isOpen
	{
		get
		{
			return this.m_isOpen;
		}
	}

	private DisasterReportPanel disasterReportPanel
	{
		get
		{
			if (this.m_disasterReportPanel == null)
			{
				this.m_disasterReportPanel = UIView.get_library().Get<DisasterReportPanel>("DisasterReportPanel");
			}
			return this.m_disasterReportPanel;
		}
	}

	private void Awake()
	{
		this.m_newsTickerText = base.Find<UILabel>("TickerText");
		this.m_newsTickerScrollablePanel = base.Find<UIScrollablePanel>("TextScroller");
		this.m_incomingDisastersPanel = base.Find<UIPanel>("IncomingDisastersPanel");
		this.m_incomingDisastersList = new UITemplateList<UIButton>(this.m_incomingDisastersPanel, "IncomingDisasterButton");
		this.m_reportsButtonPanel = base.Find<UIPanel>("ReportsButtonPanel");
		this.m_infoViewsPanel = base.Find<UIPanel>("TickerAndInfoViews");
		this.m_evacButton = base.Find<UIButton>("EvacButton");
		this.m_evacButton.add_eventClick(new MouseEventHandler(this.OnBigRedButton));
		this.m_approachingDisasters = new List<DisasterData>();
		this.m_locatedDisasterTypes = new HashSet<Type>();
		this.m_mainPanel = base.Find<UIPanel>("PanelOpenClose");
		if (!SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			base.get_component().set_isVisible(false);
		}
		else
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnDestroy()
	{
		base.CancelInvoke();
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private bool IsDisasterTypeAdded(DisasterData disasterData)
	{
		foreach (DisasterData current in this.m_approachingDisasters)
		{
			if (current.Info.m_disasterAI.GetType() == disasterData.Info.m_disasterAI.GetType())
			{
				return true;
			}
		}
		return false;
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		this.Hide(0f);
		this.m_cachedApproachingDisastersCount = 0;
		this.Refresh();
	}

	private void CheckForOldReports()
	{
		if (this.disasterReportPanel.RemoveOldReports() && this.m_isOpen)
		{
			this.Refresh();
		}
	}

	private void Refresh()
	{
		this.m_approachingDisasters.Clear();
		this.m_locatedDisasterTypes.Clear();
		this.disasterReportPanel.RemoveOldReports();
		string text = string.Empty;
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		for (int i = 1; i < instance.m_disasters.m_size; i++)
		{
			if ((instance.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None)
			{
				DisasterInfo disasterInfo = instance.m_disasters.m_buffer[i].Info;
				if (disasterInfo != null)
				{
					if (disasterInfo.m_baseInfo != null)
					{
						disasterInfo = disasterInfo.m_baseInfo;
					}
					if (!this.IsDisasterTypeAdded(instance.m_disasters.m_buffer[i]))
					{
						this.m_approachingDisasters.Add(instance.m_disasters.m_buffer[i]);
					}
					if ((instance.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None)
					{
						this.m_locatedDisasterTypes.Add(disasterInfo.m_disasterAI.GetType());
					}
					string localizedTitle = disasterInfo.GetLocalizedTitle();
					string text2 = ((float)instance.m_disasters.m_buffer[i].m_intensity / 10f).ToString("F1");
					string str = string.Empty;
					if ((instance.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.Warning) != DisasterData.Flags.None)
					{
						if ((instance.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.CustomName) != DisasterData.Flags.None)
						{
							str = StringUtils.SafeFormat(Locale.Get("WARNINGPANEL_INCOMINGDISASTER_CUSTOMNAME", disasterInfo.get_gameObject().get_name()), new object[]
							{
								localizedTitle,
								text2
							});
						}
						else
						{
							str = StringUtils.SafeFormat(Locale.Get("WARNINGPANEL_INCOMINGDISASTER", disasterInfo.get_gameObject().get_name()), text2);
						}
					}
					else
					{
						str = Locale.Get("DISASTERACTIVE", disasterInfo.get_gameObject().get_name());
					}
					if (text != string.Empty)
					{
						text += " - ";
					}
					text += str;
				}
			}
		}
		this.disasterReportPanel.CheckForUnreportedDisasters(this.m_approachingDisasters.Count > 0);
		UIButton[] array = this.m_incomingDisastersList.SetItemCount(this.m_approachingDisasters.Count);
		this.m_subInfoModes = new InfoManager.SubInfoMode[this.m_approachingDisasters.Count];
		int num = 0;
		InfoManager.SubInfoMode subInfoMode = InfoManager.SubInfoMode.Default;
		UIButton[] array2 = array;
		for (int j = 0; j < array2.Length; j++)
		{
			UIButton uIButton = array2[j];
			DisasterInfo disasterInfo2 = this.m_approachingDisasters[num].Info;
			if (disasterInfo2.m_baseInfo != null)
			{
				disasterInfo2 = disasterInfo2.m_baseInfo;
			}
			uIButton.set_normalFgSprite(disasterInfo2.m_Icon);
			uIButton.set_hoveredFgSprite(disasterInfo2.m_Icon + "Hovered");
			uIButton.set_pressedFgSprite(disasterInfo2.m_Icon + "Pressed");
			uIButton.set_focusedFgSprite(disasterInfo2.m_Icon + "Focused");
			uIButton.set_disabledFgSprite(disasterInfo2.m_Icon);
			if (this.m_locatedDisasterTypes.Contains(disasterInfo2.m_disasterAI.GetType()))
			{
				uIButton.set_isEnabled(true);
				uIButton.set_tooltip(StringUtils.SafeFormat(Locale.Get("WARNINGPANEL_INCOMING"), disasterInfo2.GetLocalizedTitle()));
				uIButton.set_normalBgSprite(this.m_incomingDisasterButtonBackground);
				uIButton.set_hoveredBgSprite(this.m_incomingDisasterButtonBackground + "Hovered");
				uIButton.set_pressedBgSprite(this.m_incomingDisasterButtonBackground + "Pressed");
				uIButton.set_focusedBgSprite(this.m_incomingDisasterButtonBackground + "Focused");
			}
			else
			{
				uIButton.set_isEnabled(false);
				uIButton.set_tooltip(Locale.Get("WARNINGPANEL_NODETECTIONSERVICE"));
				uIButton.set_normalBgSprite(string.Empty);
				uIButton.set_hoveredBgSprite(string.Empty);
				uIButton.set_pressedBgSprite(string.Empty);
				uIButton.set_focusedBgSprite(string.Empty);
				uIButton.set_disabledBgSprite(string.Empty);
			}
			uIButton.set_zOrder(num);
			uIButton.add_eventClicked(new MouseEventHandler(this.OnInfoViewButtonClicked));
			if (disasterInfo2.m_disasterAI.GetHazardSubMode(out subInfoMode))
			{
				this.m_subInfoModes[num] = subInfoMode;
			}
			else
			{
				this.m_subInfoModes[num] = InfoManager.SubInfoMode.Default;
				Debug.LogError("Could not get hazard sub mode");
			}
			num++;
		}
		this.m_newsTickerText.set_text(text);
		float y = this.m_incomingDisastersPanel.get_relativePosition().y;
		this.m_incomingDisastersPanel.CenterToParent();
		this.m_incomingDisastersPanel.set_relativePosition(new Vector2(this.m_incomingDisastersPanel.get_relativePosition().x, y));
		this.RefreshReportsButtonVisibility();
		this.m_infoViewsPanel.set_isVisible(this.m_approachingDisasters.Count > 0);
		if (this.CountShelters() > 0)
		{
			this.m_evacButton.set_isEnabled(true);
			this.SetEvacuatingState(this.CountEvacuatingShelters() > 0);
		}
		else
		{
			this.m_evacButton.set_isEnabled(false);
			this.m_evacButton.set_tooltipLocaleID("WARNINGPANEL_NOSHELTERS_TOOLTIP");
		}
		this.RefreshPanelWidth();
		if (this.m_cachedApproachingDisastersCount < this.m_approachingDisasters.Count)
		{
			this.Show(false);
		}
		this.m_cachedApproachingDisastersCount = this.m_approachingDisasters.Count;
	}

	public void RefreshReportsButtonVisibility()
	{
		this.m_reportsButtonPanel.set_isVisible(this.disasterReportPanel.reportsAvailable);
		this.RefreshPanelWidth();
	}

	private float RefreshPanelWidth()
	{
		bool isVisible = this.m_mainPanel.get_isVisible();
		this.m_mainPanel.set_isVisible(true);
		this.m_mainPanel.set_autoLayout(true);
		this.m_mainPanel.set_autoLayout(false);
		this.m_mainPanel.FitChildrenHorizontally();
		float width = this.m_mainPanel.get_width();
		this.m_mainPanel.set_isVisible(isVisible);
		return width;
	}

	private void Update()
	{
		if (base.get_component().get_isVisible() && Singleton<InfoManager>.get_exists())
		{
			foreach (UIComponent current in this.m_incomingDisastersPanel.get_components())
			{
				UIButton uIButton = current as UIButton;
				if (uIButton != null && (Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.DisasterHazard || Singleton<InfoManager>.get_instance().CurrentSubMode != this.m_subInfoModes[current.get_zOrder()]) && uIButton.get_state() == 1)
				{
					uIButton.set_state(0);
				}
			}
		}
	}

	private void OnInfoViewButtonClicked(UIComponent comp, UIMouseEventParameter eventParam)
	{
		base.CloseToolbar();
		if (ToolsModifierControl.GetCurrentTool<TransportTool>() != null)
		{
			ToolsModifierControl.SetTool<DefaultTool>();
		}
		if (Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.DisasterHazard || Singleton<InfoManager>.get_instance().CurrentSubMode != this.m_subInfoModes[comp.get_zOrder()])
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.DisasterHazard, this.m_subInfoModes[comp.get_zOrder()]);
			this.UpdateAdvisor(comp.get_zOrder());
		}
		else
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
			comp.Unfocus();
			this.UpdateAdvisor(-1);
		}
	}

	private void UpdateAdvisor(int index)
	{
		if (this.m_approachingDisasters != null && index >= 0 && index < this.m_approachingDisasters.Count)
		{
			DisasterInfo info = this.m_approachingDisasters[index].Info;
			string text = "InfoViewDisasterHazard";
			string icon = info.m_Icon;
			string spriteName = TutorialAdvisorPanel.GetSpriteName(text);
			SavedBool savedBool = new SavedBool(text, Settings.userGameState, true);
			if (savedBool)
			{
				ToolsModifierControl.advisorPanel.Show(text, icon, spriteName, 0f, false, true);
				savedBool.set_value(false);
			}
			else
			{
				ToolsModifierControl.advisorPanel.Refresh(text, icon, spriteName);
			}
		}
		else
		{
			ToolsModifierControl.advisorPanel.Refresh(null);
		}
	}

	private void Show(bool refresh = true)
	{
		if (!this.m_isOpen)
		{
			this.m_isOpen = true;
			base.InvokeRepeating("CheckForOldReports", 5f, 5f);
			if (refresh)
			{
				this.Refresh();
			}
			float num = this.RefreshPanelWidth();
			ValueAnimator.Animate("WarningPanelScaleX", delegate(float val)
			{
				Vector2 size = this.m_mainPanel.get_size();
				size.x = val;
				this.m_mainPanel.set_size(size);
			}, new AnimatedFloat(0f, num, 0.7f, 13), delegate
			{
				this.WaitAndStartScrolling();
			});
		}
	}

	private void Hide(float time = 0.7f)
	{
		if (this.m_isOpen)
		{
			this.m_isOpen = false;
			ValueAnimator.Animate("WarningPanelScaleX", delegate(float val)
			{
				Vector2 size = this.m_mainPanel.get_size();
				size.x = val;
				this.m_mainPanel.set_size(size);
			}, new AnimatedFloat(this.m_mainPanel.get_width(), 0f, time, 13), delegate
			{
				base.CancelInvoke();
			});
		}
	}

	private void OnEnable()
	{
		Singleton<DisasterManager>.get_instance().m_detectedDisastersUpdated += new DisasterManager.DetectedDisastersUpdatedHandler(this.OnDetectedDisastersUpdated);
	}

	private void OnDisable()
	{
		Singleton<DisasterManager>.get_instance().m_detectedDisastersUpdated -= new DisasterManager.DetectedDisastersUpdatedHandler(this.OnDetectedDisastersUpdated);
	}

	private void OnDetectedDisastersUpdated()
	{
		this.Refresh();
	}

	private void WaitAndStartScrolling()
	{
		if (this.m_isOpen)
		{
			this.m_newsTickerScrollablePanel.set_scrollPosition(Vector2.get_zero());
			base.Invoke("StartScrolling", 0.8f);
		}
	}

	private void StartScrolling()
	{
		if (this.m_isOpen)
		{
			float num = this.m_newsTickerText.get_width() - this.m_newsTickerScrollablePanel.get_width();
			ValueAnimator.Animate("WarningPanelScrolling", delegate(float val)
			{
				this.m_newsTickerScrollablePanel.set_scrollPosition(new Vector2(val, 0f));
			}, new AnimatedFloat(0f, num, num / this.m_scrollSpeed), delegate
			{
				this.OnCompletedScrolling();
			});
		}
	}

	private void OnCompletedScrolling()
	{
		if (this.m_isOpen)
		{
			base.Invoke("WaitAndStartScrolling", 2.5f);
		}
	}

	private void SetEvacuatingState(bool isEvacuating)
	{
		if (isEvacuating)
		{
			this.m_evacButton.set_tooltipLocaleID("WARNINGPANEL_RELEASEALL_TOOLTIP");
			this.m_evacButton.set_normalBgSprite(this.m_releaseButtonSprite);
			this.m_evacButton.set_hoveredBgSprite(this.m_releaseButtonSprite + "Hovered");
			this.m_evacButton.set_pressedBgSprite(this.m_releaseButtonSprite + "Pressed");
		}
		else
		{
			this.m_evacButton.set_tooltipLocaleID("WARNINGPANEL_EVACUATEALL_TOOLTIP");
			this.m_evacButton.set_normalBgSprite(this.m_evacuateButtonSprite);
			this.m_evacButton.set_hoveredBgSprite(this.m_evacuateButtonSprite + "Hovered");
			this.m_evacButton.set_pressedBgSprite(this.m_evacuateButtonSprite + "Pressed");
		}
		this.m_isEvacuating = isEvacuating;
	}

	public void OnBigRedButton(UIComponent component, UIMouseEventParameter eventParam)
	{
		if (!this.m_isEvacuating)
		{
			Singleton<DisasterManager>.get_instance().EvacuateAll(false);
			this.SetEvacuatingState(true);
		}
		else
		{
			Singleton<DisasterManager>.get_instance().EvacuateAll(true);
			this.SetEvacuatingState(false);
			this.Refresh();
		}
		this.m_evacButton.Unfocus();
	}

	public void OnReportsButton()
	{
		DisasterReportPanel disasterReportPanel = UIView.get_library().Get<DisasterReportPanel>("DisasterReportPanel");
		disasterReportPanel.Show();
	}

	private int CountShelters()
	{
		int num = 0;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(ItemClass.Service.Disaster);
		if (serviceBuildings != null)
		{
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num2 = serviceBuildings.m_buffer[i];
				if (num2 != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num2].Info;
					if (info != null)
					{
						ShelterAI shelterAI = info.m_buildingAI as ShelterAI;
						if (shelterAI != null)
						{
							num++;
						}
					}
				}
			}
		}
		return num;
	}

	private int CountEvacuatingShelters()
	{
		int num = 0;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(ItemClass.Service.Disaster);
		if (serviceBuildings != null)
		{
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num2 = serviceBuildings.m_buffer[i];
				if (num2 != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num2].Info;
					if (info != null)
					{
						ShelterAI shelterAI = info.m_buildingAI as ShelterAI;
						if (shelterAI != null && (instance.m_buildings.m_buffer[(int)num2].m_flags & Building.Flags.Downgrading) == Building.Flags.None)
						{
							num++;
						}
					}
				}
			}
		}
		return num;
	}

	public void OnOpenCloseButton()
	{
		if (this.m_isOpen)
		{
			this.Hide(0.7f);
		}
		else
		{
			this.Show(true);
		}
	}

	private bool m_isOpen = true;

	private UIPanel m_incomingDisastersPanel;

	private UIPanel m_reportsButtonPanel;

	private UIPanel m_infoViewsPanel;

	private UITemplateList<UIButton> m_incomingDisastersList;

	public float m_scrollSpeed = 20f;

	private UIScrollablePanel m_newsTickerScrollablePanel;

	private UILabel m_newsTickerText;

	public UITextureAtlas m_atlas;

	[UISprite("m_atlas")]
	public string m_evacuateButtonSprite;

	[UISprite("m_atlas")]
	public string m_releaseButtonSprite;

	[UISprite("m_atlas")]
	public string m_incomingDisasterButtonBackground;

	public Color m_evacButtonColor;

	public Color m_releaseButtonColor;

	private InfoManager.SubInfoMode[] m_subInfoModes;

	private List<DisasterData> m_approachingDisasters;

	private int m_cachedApproachingDisastersCount;

	private HashSet<Type> m_locatedDisasterTypes;

	private UIButton m_evacButton;

	private bool m_isEvacuating;

	private UIPanel m_mainPanel;

	private DisasterReportPanel m_disasterReportPanel;
}
