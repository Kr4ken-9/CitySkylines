﻿using System;
using ColossalFramework;
using UnityEngine;

public class LoadingAnimationData : MonoBehaviour
{
	private void Awake()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().LoadingAnimationComponent.SetData(this.m_mesh, this.m_material, this.m_barBG, this.m_barFG);
		}
		Object.Destroy(base.get_gameObject());
	}

	public Mesh m_mesh;

	public Material m_material;

	public Material m_barBG;

	public Material m_barFG;
}
