﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class ExitConfirmPanel : MenuPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.m_Binding = base.GetComponent<BindPropertyByKey>();
	}

	public static void ShowModal(string id, UIView.ModalPoppedReturnCallback callback)
	{
		ExitConfirmPanel exitConfirmPanel = UIView.get_library().ShowModal<ExitConfirmPanel>("ExitConfirmPanel", callback);
		exitConfirmPanel.SetMessage(Locale.Get(id, "Title"), Locale.Get(id, "Message"));
	}

	public void SetMessage(string title, string message)
	{
		if (this.m_Binding != null)
		{
			this.m_Binding.SetProperties(TooltipHelper.Format(new string[]
			{
				"title",
				title,
				"message",
				message
			}));
		}
	}

	protected virtual void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("ToMainMenu");
			uIButton.Focus();
		}
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	protected virtual void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				p.Use();
				this.OnClosed();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnReturnMainMenu();
			}
		}
	}

	public virtual void OnReturnMainMenu()
	{
		UIView.get_library().Hide(base.GetType().Name, 0);
	}

	public virtual void OnExitGame()
	{
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	private BindPropertyByKey m_Binding;
}
