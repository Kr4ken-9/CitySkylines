﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class TerrainProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	public static void EnsureTextureSettings(Texture2D texture)
	{
		texture.set_wrapMode(0);
		texture.set_filterMode(2);
		texture.set_anisoLevel(4);
	}

	private void OverrideFromMapTheme()
	{
		MapThemeMetaData mapThemeMetaData = Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData;
		if (mapThemeMetaData != null)
		{
			if (mapThemeMetaData.grassDiffuseAsset != null)
			{
				this.m_grassDiffuse = mapThemeMetaData.grassDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_grassDiffuse);
			}
			if (mapThemeMetaData.ruinedDiffuseAsset != null)
			{
				this.m_ruinedDiffuse = mapThemeMetaData.ruinedDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_ruinedDiffuse);
			}
			if (mapThemeMetaData.pavementDiffuseAsset != null)
			{
				this.m_pavementDiffuse = mapThemeMetaData.pavementDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_pavementDiffuse);
			}
			if (mapThemeMetaData.gravelDiffuseAsset != null)
			{
				this.m_gravelDiffuse = mapThemeMetaData.gravelDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_gravelDiffuse);
			}
			if (mapThemeMetaData.cliffDiffuseAsset != null)
			{
				this.m_cliffDiffuse = mapThemeMetaData.cliffDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_cliffDiffuse);
			}
			if (mapThemeMetaData.sandDiffuseAsset != null)
			{
				this.m_sandDiffuse = mapThemeMetaData.sandDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_sandDiffuse);
			}
			if (mapThemeMetaData.oilDiffuseAsset != null)
			{
				this.m_oilDiffuse = mapThemeMetaData.oilDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_oilDiffuse);
			}
			if (mapThemeMetaData.oreDiffuseAsset != null)
			{
				this.m_oreDiffuse = mapThemeMetaData.oreDiffuseAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_oreDiffuse);
			}
			if (mapThemeMetaData.cliffSandNormalAsset != null)
			{
				this.m_cliffSandNormal = mapThemeMetaData.cliffSandNormalAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_cliffSandNormal);
			}
			this.m_grassTiling = mapThemeMetaData.grassTiling;
			this.m_ruinedTiling = mapThemeMetaData.ruinedTiling;
			this.m_pavementTiling = mapThemeMetaData.pavementTiling;
			this.m_gravelTiling = mapThemeMetaData.gravelTiling;
			this.m_cliffTiling = mapThemeMetaData.cliffDiffuseTiling;
			this.m_sandTiling = mapThemeMetaData.sandDiffuseTiling;
			this.m_oilTiling = mapThemeMetaData.oilTiling;
			this.m_oreTiling = mapThemeMetaData.oreTiling;
			this.m_cliffSandNormalTiling = mapThemeMetaData.cliffSandNormalTiling;
			this.m_grassPollutionColorOffset = mapThemeMetaData.grassPollutionColorOffset;
			this.m_grassFieldColorOffset = mapThemeMetaData.grassFieldColorOffset;
			this.m_grassFertilityColorOffset = mapThemeMetaData.grassFertilityColorOffset;
			this.m_grassForestColorOffset = mapThemeMetaData.grassForestColorOffset;
			if (mapThemeMetaData.waterFoamAsset != null)
			{
				this.m_waterFoam = mapThemeMetaData.waterFoamAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_waterFoam);
			}
			if (mapThemeMetaData.waterNormalAsset != null)
			{
				this.m_waterNormal = mapThemeMetaData.waterNormalAsset.Instantiate<Texture2D>();
				TerrainProperties.EnsureTextureSettings(this.m_waterNormal);
			}
			this.m_waterColorClean = mapThemeMetaData.waterClean;
			this.m_waterColorDirty = mapThemeMetaData.waterDirty;
			this.m_waterColorUnder = mapThemeMetaData.waterUnder;
			this.m_useGrassDecorations = mapThemeMetaData.grassDetailEnabled;
			this.m_useFertileDecorations = mapThemeMetaData.fertileDetailEnabled;
			this.m_useCliffDecorations = mapThemeMetaData.rocksDetailEnabled;
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		TerrainProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new TerrainProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	public void InitializeShaderProperties()
	{
		Shader.SetGlobalTexture("_TerrainGrassDiffuse", this.m_grassDiffuse);
		Shader.SetGlobalTexture("_TerrainRuinedDiffuse", this.m_ruinedDiffuse);
		Shader.SetGlobalTexture("_TerrainPavementDiffuse", this.m_pavementDiffuse);
		Shader.SetGlobalTexture("_TerrainGravelDiffuse", this.m_gravelDiffuse);
		Shader.SetGlobalTexture("_TerrainCliffDiffuse", this.m_cliffDiffuse);
		Shader.SetGlobalTexture("_TerrainOreDiffuse", this.m_oreDiffuse);
		Shader.SetGlobalTexture("_TerrainOilDiffuse", this.m_oilDiffuse);
		Shader.SetGlobalTexture("_TerrainSandDiffuse", this.m_sandDiffuse);
		Shader.SetGlobalTexture("_TerrainCliffSandNormal", this.m_cliffSandNormal);
		Shader.SetGlobalTexture("_WaterNormal", this.m_waterNormal);
		Shader.SetGlobalTexture("_WaterFoam", this.m_waterFoam);
		Shader.SetGlobalTexture("_WaterArrow", this.m_waterArrow);
		Shader.SetGlobalColor("_WaterColorClean", new Color(this.m_waterColorClean.r, this.m_waterColorClean.g, this.m_waterColorClean.b, this.m_waterRainFoam));
		Shader.SetGlobalColor("_WaterColorDirty", this.m_waterColorDirty);
		Shader.SetGlobalColor("_WaterColorUnder", this.m_waterColorUnder);
		Shader.SetGlobalVector("_GrassPollutionColorOffset", new Vector4(this.m_grassPollutionColorOffset.x, this.m_grassPollutionColorOffset.y, this.m_grassPollutionColorOffset.z, this.m_cliffSandNormalTiling));
		Shader.SetGlobalVector("_GrassFieldColorOffset", this.m_grassFieldColorOffset);
		Shader.SetGlobalVector("_GrassFertilityColorOffset", this.m_grassFertilityColorOffset);
		Shader.SetGlobalVector("_GrassForestColorOffset", this.m_grassForestColorOffset);
		Shader.SetGlobalVector("_TerrainTextureTiling1", new Vector4(this.m_pavementTiling, this.m_ruinedTiling, this.m_sandTiling, this.m_cliffTiling));
		Shader.SetGlobalVector("_TerrainTextureTiling2", new Vector4(this.m_grassTiling, this.m_gravelTiling, this.m_oreTiling, this.m_oilTiling));
		Shader.SetGlobalColor("_TerrainBrushColor1", this.m_brushColor1);
		Shader.SetGlobalColor("_TerrainBrushColor2", this.m_brushColor2);
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("TerrainProperties");
			Singleton<TerrainManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Shader m_terrainShader;

	public Shader m_topographyShader;

	public Shader m_decorationShader;

	public Shader m_decoRenderShader;

	public Shader m_waterShader;

	public Shader m_waterTransparentShader;

	public DecorationInfo[] m_grassDecorations;

	public DecorationInfo[] m_fertileDecorations;

	public DecorationInfo[] m_cliffDecorations;

	public bool m_useGrassDecorations = true;

	public bool m_useFertileDecorations = true;

	public bool m_useCliffDecorations = true;

	public Texture2D m_grassDiffuse;

	public Texture2D m_ruinedDiffuse;

	public Texture2D m_pavementDiffuse;

	public Texture2D m_gravelDiffuse;

	public Texture2D m_cliffDiffuse;

	public Texture2D m_oreDiffuse;

	public Texture2D m_oilDiffuse;

	public Texture2D m_sandDiffuse;

	public Texture2D m_cliffSandNormal;

	public Texture2D m_waterNormal;

	public Texture2D m_waterFoam;

	public Texture2D m_waterArrow;

	public Color m_waterColorClean = new Color(0.2f, 0.3f, 0.4f);

	public Color m_waterColorDirty = new Color(0.6f, 0.5f, 0.3f);

	public Color m_waterColorUnder = new Color(0.55f, 0.45f, 0.4f);

	public float m_waterRainFoam = 0.5f;

	public Vector3 m_grassPollutionColorOffset = new Vector3(0.03f, 0.015f, 0.04f);

	public Vector3 m_grassFieldColorOffset = new Vector3(0.04f, 0f, 0f);

	public Vector3 m_grassFertilityColorOffset = new Vector3(0.02f, 0.02f, 0.01f);

	public Vector3 m_grassForestColorOffset = new Vector3(-0.015f, -0.025f, -0.007f);

	public float m_pavementTiling = 0.061f;

	public float m_ruinedTiling = 0.033f;

	public float m_sandTiling = 0.014f;

	public float m_cliffTiling = 0.005f;

	public float m_grassTiling = 0.029f;

	public float m_gravelTiling = 0.06f;

	public float m_oreTiling = 0.031f;

	public float m_oilTiling = 0.032f;

	public float m_cliffSandNormalTiling = 0.014f;

	public Color m_brushColor1 = new Color(1f, 1f, 0.25f, 0.25f);

	public Color m_brushColor2 = new Color(1f, 0f, 0f, 0.25f);

	public int m_dirtPrice = 10;
}
