﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class SurfacePanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < SurfacePanel.kSurfaces.Length; i++)
		{
			this.SpawnEntry(SurfacePanel.kSurfaces[i].get_enumName(), i);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		SurfaceTool surfaceTool = ToolsModifierControl.SetTool<SurfaceTool>();
		if (surfaceTool != null)
		{
			surfaceTool.m_surface = SurfacePanel.kSurfaces[comp.get_zOrder()].get_enumValue();
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("SURFACE_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("SURFACE_DESC", name),
			"locked",
			false.ToString()
		});
		return base.CreateButton(name, tooltip, "Surface" + name, index, GeneratedPanel.tooltipBox);
	}

	private static readonly PositionData<TerrainModify.Surface>[] kSurfaces = Utils.GetOrderedEnumData<TerrainModify.Surface>();
}
