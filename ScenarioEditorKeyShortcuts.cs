﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class ScenarioEditorKeyShortcuts : KeyShortcuts
{
	private void Awake()
	{
		this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
		this.m_CloseInfoViews = UIView.Find<UIPanel>("InfoViewsPanel").Find<UIButton>("CloseButton");
	}

	private void SteamEscape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
	}

	protected void Update()
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Escape))
			{
				this.SteamEscape();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FreeCamera))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Bulldoze))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.InfoViews))
			{
				base.SelectUIButton("InfoPanel");
			}
		}
	}

	protected override void OnProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (eventType == 4 && keyCode == 27)
			{
				this.Escape();
			}
			else if (this.m_ShortcutTerrainTool.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Terrain");
			}
			else if (this.m_ShortcutBulldozer.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (this.m_ShortcutBulldozerUnderground.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BulldozerUndergroundToggle");
			}
			else if (this.m_ShortcutInGameShortcutFreeCameraMode.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (this.m_ShortcutPlaceDisasters.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Disasters");
			}
			else if (this.m_ShortcutTriggerPanel.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Trigger");
			}
			else if (this.m_ShortcutSettings.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ScenarioSettings");
			}
		}
	}

	private void Escape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (WorldInfoPanel.AnyWorldInfoPanelOpen())
		{
			WorldInfoPanel.HideAllWorldInfoPanels();
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else
		{
			UIView.get_library().ShowModal("PauseMenu");
		}
	}

	private SavedInputKey m_ShortcutInGameShortcutFreeCameraMode = new SavedInputKey(Settings.inGameShortcutFreeCameraMode, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFreeCameraMode, true);

	private SavedInputKey m_ShortcutBulldozer = new SavedInputKey(Settings.scenarioEditorBulldozer, Settings.inputSettingsFile, DefaultSettings.scenarioEditorBulldozer, true);

	private SavedInputKey m_ShortcutBulldozerUnderground = new SavedInputKey(Settings.scenarioEditorBulldozerUnderground, Settings.inputSettingsFile, DefaultSettings.scenarioEditorBulldozerUnderground, true);

	private SavedInputKey m_ShortcutTerrainTool = new SavedInputKey(Settings.scenarioEditorTerrainTool, Settings.inputSettingsFile, DefaultSettings.scenarioEditorTerrainTool, true);

	private SavedInputKey m_ShortcutPlaceDisasters = new SavedInputKey(Settings.scenarioEditorPlaceDisasters, Settings.inputSettingsFile, DefaultSettings.scenarioEditorPlaceDisasters, true);

	private SavedInputKey m_ShortcutTriggerPanel = new SavedInputKey(Settings.scenarioEditorTriggerPanel, Settings.inputSettingsFile, DefaultSettings.scenarioEditorTriggerPanel, true);

	private SavedInputKey m_ShortcutSettings = new SavedInputKey(Settings.scenarioEditorSettings, Settings.inputSettingsFile, DefaultSettings.scenarioEditorSettings, true);

	private UIButton m_CloseToolbarButton;

	private UIButton m_CloseInfoViews;
}
