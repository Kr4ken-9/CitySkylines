﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class TimeMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		int targetGameWeeks = this.m_targetGameWeeks;
		return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_TIME_DESC"), new GeneratedString[]
		{
			new GeneratedString.Long((long)targetGameWeeks)
		});
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int targetGameWeeks = this.m_targetGameWeeks;
		info.m_min = 0f;
		info.m_max = (float)targetGameWeeks;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)targetGameWeeks)
		{
			num = (long)targetGameWeeks;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_TIME_DESC"), targetGameWeeks);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_TIME_PROG"), new object[]
		{
			num,
			targetGameWeeks
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		return this.m_targetGameWeeks;
	}

	public override MilestoneInfo.Data GetData()
	{
		if (this.m_data == null)
		{
			this.m_data = new TimeMilestone.TimeData
			{
				m_startFrame = 0u
			};
			this.m_data.m_isAssigned = true;
		}
		return this.m_data;
	}

	public override void SetData(MilestoneInfo.Data data)
	{
		if (data == null || data.m_isAssigned)
		{
			return;
		}
		data.m_isAssigned = true;
		TimeMilestone.TimeData timeData = data as TimeMilestone.TimeData;
		if (timeData != null)
		{
			if (this.m_data == null)
			{
				this.m_data = timeData;
			}
			else
			{
				this.m_data.m_passedCount = timeData.m_passedCount;
				this.m_data.m_progress = timeData.m_progress;
				TimeMilestone.TimeData timeData2 = this.m_data as TimeMilestone.TimeData;
				if (timeData2 != null)
				{
					timeData2.m_startFrame = timeData.m_startFrame;
				}
			}
			this.m_written = true;
		}
		else if (data != null)
		{
			MilestoneInfo.Data data2 = this.GetData();
			data2.m_passedCount = data.m_passedCount;
			data2.m_progress = data.m_progress;
			this.m_written = true;
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		int targetGameWeeks = this.m_targetGameWeeks;
		if (forceUnlock)
		{
			data.m_progress = (long)targetGameWeeks;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
			TimeMilestone.TimeData timeData = data as TimeMilestone.TimeData;
			if (timeData != null)
			{
				timeData.m_startFrame = 0u;
			}
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
			TimeMilestone.TimeData timeData2 = data as TimeMilestone.TimeData;
			if (timeData2 != null)
			{
				timeData2.m_startFrame = 0u;
			}
		}
		else
		{
			int num = 0;
			TimeMilestone.TimeData timeData3 = data as TimeMilestone.TimeData;
			if (timeData3 != null)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				if (timeData3.m_startFrame == 0u)
				{
					timeData3.m_startFrame = currentFrameIndex;
				}
				if (currentFrameIndex > timeData3.m_startFrame)
				{
					num = (int)(currentFrameIndex - timeData3.m_startFrame >> 12);
				}
			}
			data.m_progress = (long)Mathf.Min(targetGameWeeks, num);
			data.m_passedCount = ((data.m_progress != (long)targetGameWeeks) ? 0 : 1);
		}
		return data;
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32(this.m_targetGameWeeks);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		if (s.get_version() >= 279u)
		{
			this.m_targetGameWeeks = s.ReadInt32();
		}
		else
		{
			this.m_targetGameWeeks = 0;
		}
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		int targetGameWeeks = this.m_targetGameWeeks;
		if (targetGameWeeks != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)targetGameWeeks));
		}
		return num;
	}

	public int m_targetGameWeeks = 10;

	public class TimeData : MilestoneInfo.Data
	{
		public override void Serialize(DataSerializer s)
		{
			base.Serialize(s);
			s.WriteUInt32(this.m_startFrame);
		}

		public override void Deserialize(DataSerializer s)
		{
			base.Deserialize(s);
			this.m_startFrame = s.ReadUInt32();
			if (s.get_version() < 302u)
			{
				this.m_startFrame = 0u;
			}
		}

		public override void AfterDeserialize(DataSerializer s)
		{
			base.AfterDeserialize(s);
		}

		public uint m_startFrame;
	}
}
