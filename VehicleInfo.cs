﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class VehicleInfo : VehicleInfoBase
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_generatedInfo == null)
		{
			throw new PrefabException(this, "Generated info missing");
		}
		this.m_isLargeVehicle = ((this.m_vehicleType & (VehicleInfo.VehicleType.Ship | VehicleInfo.VehicleType.Plane | VehicleInfo.VehicleType.Helicopter | VehicleInfo.VehicleType.Meteor | VehicleInfo.VehicleType.Vortex | VehicleInfo.VehicleType.Ferry | VehicleInfo.VehicleType.Blimp)) != VehicleInfo.VehicleType.None);
		bool flag = false;
		MeshFilter component = base.GetComponent<MeshFilter>();
		if (component != null && component.get_sharedMesh() != null)
		{
			this.m_mesh = component.get_sharedMesh();
			this.m_material = base.GetComponent<Renderer>().get_sharedMaterial();
			string tag = this.m_material.GetTag("RenderType", false);
			if (this.m_material == null)
			{
				throw new PrefabException(this, "Material missing");
			}
			if (this.m_generatedInfo.m_size == Vector3.get_zero())
			{
				throw new PrefabException(this, "Generated info has zero size");
			}
			if (this.m_generatedInfo.m_vehicleInfo != null)
			{
				if (this.m_generatedInfo.m_vehicleInfo.m_mesh != this.m_mesh)
				{
					throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_vehicleInfo.get_gameObject().get_name() + ")");
				}
			}
			else
			{
				this.m_generatedInfo.m_vehicleInfo = this;
			}
			if (this.m_generatedInfo.m_tyres == null || this.m_generatedInfo.m_tyres.Length == 0)
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Tyre array is null or zero size: " + base.get_gameObject().get_name(), base.get_gameObject());
				this.m_generatedInfo.m_tyres = new Vector4[]
				{
					new Vector4(0f, 0f, 0f, 1f)
				};
			}
			Texture2D texture2D = this.m_material.get_mainTexture() as Texture2D;
			if (texture2D != null && texture2D.get_format() == 12 && tag != "Effect")
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
			if (this.m_lodObject != null)
			{
				component = this.m_lodObject.GetComponent<MeshFilter>();
				this.m_lodMesh = component.get_sharedMesh();
				this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
				if (this.m_lodMaterial.get_shader() != this.m_material.get_shader())
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD has different shader: " + base.get_gameObject().get_name(), base.get_gameObject());
				}
			}
			else if (tag != "Effect")
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
			base.InitMeshInfo(this.m_generatedInfo.m_size);
			if (this.m_useColorVariations)
			{
				this.m_color0 = this.m_material.GetColor("_ColorV0");
				this.m_color1 = this.m_material.GetColor("_ColorV1");
				this.m_color2 = this.m_material.GetColor("_ColorV2");
				this.m_color3 = this.m_material.GetColor("_ColorV3");
			}
			else
			{
				this.m_color0 = this.m_material.get_color();
				this.m_color1 = this.m_material.get_color();
				this.m_color2 = this.m_material.get_color();
				this.m_color3 = this.m_material.get_color();
			}
			flag = true;
		}
		else
		{
			this.m_color0 = Color.get_white();
			this.m_color1 = Color.get_white();
			this.m_color2 = Color.get_white();
			this.m_color3 = Color.get_white();
		}
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				if (this.m_subMeshes[i].m_subInfo != null)
				{
					if (!(this.m_subMeshes[i].m_subInfo is VehicleInfoSub))
					{
						throw new PrefabException(this, "Submesh (" + this.m_subMeshes[i].m_subInfo.get_gameObject().get_name() + ") is not VehicleInfoSub");
					}
					this.m_subMeshes[i].m_subInfo.InitializePrefab();
					this.m_maxRenderDistance = Mathf.Max(this.m_maxRenderDistance, this.m_subMeshes[i].m_subInfo.m_maxRenderDistance);
					if (!flag)
					{
						this.m_color0 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
						this.m_color1 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
						this.m_color2 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
						this.m_color3 = this.m_subMeshes[i].m_subInfo.m_material.get_color();
						flag = true;
					}
				}
			}
		}
		if (this.m_effects != null)
		{
			for (int j = 0; j < this.m_effects.Length; j++)
			{
				if (this.m_effects[j].m_effect != null)
				{
					this.m_effects[j].m_effect.InitializeEffect();
				}
			}
		}
		if (!this.m_isLargeVehicle)
		{
			this.m_maxRenderDistance = Mathf.Min(this.m_maxRenderDistance, 2000f);
		}
		if (this.m_lodMesh != null)
		{
			this.m_lodRenderDistance = Mathf.Min(this.m_maxRenderDistance * 0.2f, 1000f);
		}
		else
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance;
		}
		if (this.m_vehicleAI == null)
		{
			this.m_vehicleAI = base.GetComponent<VehicleAI>();
			this.m_vehicleAI.m_info = this;
			this.m_vehicleAI.InitializeAI();
		}
	}

	public override void DestroyPrefab()
	{
		base.DestroyMeshInfo();
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				if (this.m_subMeshes[i].m_subInfo != null)
				{
					this.m_subMeshes[i].m_subInfo.DestroyPrefab();
				}
			}
		}
		if (this.m_effects != null)
		{
			for (int j = 0; j < this.m_effects.Length; j++)
			{
				if (this.m_effects[j].m_effect != null)
				{
					this.m_effects[j].m_effect.ReleaseEffect();
				}
			}
		}
		if (this.m_vehicleAI != null)
		{
			this.m_vehicleAI.ReleaseAI();
			this.m_vehicleAI = null;
		}
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		if (this.m_mesh != null)
		{
			base.RefreshLevelOfDetail(this.m_generatedInfo.m_size);
		}
		if (!this.m_isLargeVehicle)
		{
			this.m_maxRenderDistance = Mathf.Min(this.m_maxRenderDistance, 2000f);
		}
		if (this.m_lodMesh != null)
		{
			this.m_lodRenderDistance = Mathf.Min(this.m_maxRenderDistance * 0.2f, 1000f);
		}
		else
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance;
		}
		if (this.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_subMeshes.Length; i++)
			{
				if (this.m_subMeshes[i].m_subInfo != null)
				{
					this.m_subMeshes[i].m_subInfo.RefreshLevelOfDetail();
				}
			}
		}
	}

	public void CalculateGeneratedInfo()
	{
		MeshFilter[] componentsInChildren = base.GetComponentsInChildren<MeshFilter>(true);
		this.CalculateGeneratedInfo(componentsInChildren);
	}

	public void CalculateGeneratedInfo(MeshFilter[] filters)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		float num5 = 0f;
		float num6 = 0f;
		for (int i = 0; i < filters.Length; i++)
		{
			Vector3[] vertices = filters[i].get_sharedMesh().get_vertices();
			for (int j = 0; j < vertices.Length; j++)
			{
				num = Mathf.Min(num, vertices[j].x);
				num2 = Mathf.Max(num2, vertices[j].x);
				num3 = Mathf.Min(num3, vertices[j].y);
				num4 = Mathf.Max(num4, vertices[j].y);
				num5 = Mathf.Min(num5, vertices[j].z);
				num6 = Mathf.Max(num6, vertices[j].z);
			}
		}
		this.m_generatedInfo.m_size = new Vector3(Mathf.Max(-num, num2) * 2f, num4, Mathf.Max(-num5, num6) * 2f);
		this.m_generatedInfo.m_negativeHeight = Mathf.Max(0f, -num3);
		this.m_generatedInfo.m_triangleArea = 0f;
		this.m_generatedInfo.m_uvmapArea = 0f;
		FastList<Vector4> fastList = new FastList<Vector4>();
		for (int k = 0; k < filters.Length; k++)
		{
			Vector3[] vertices2 = filters[k].get_sharedMesh().get_vertices();
			Vector2[] uv = filters[k].get_sharedMesh().get_uv();
			Color32[] array = filters[k].get_sharedMesh().get_colors32();
			int[] triangles = filters[k].get_sharedMesh().get_triangles();
			if (array.Length != vertices2.Length)
			{
				array = new Color32[vertices2.Length];
			}
			bool flag = false;
			for (int l = 0; l < vertices2.Length; l++)
			{
				Color32 color = array[l];
				if (color.g >= 254 && color.b < 254)
				{
					array[l] = new Color32(color.r, 0, color.b, 0);
					flag = true;
				}
				else
				{
					array[l] = new Color32(0, 0, 255, 255);
				}
			}
			for (int m = 0; m < triangles.Length; m += 3)
			{
				int num7 = triangles[m];
				int num8 = triangles[m + 1];
				int num9 = triangles[m + 2];
				this.m_generatedInfo.m_triangleArea += Triangle3.Area(vertices2[num7], vertices2[num8], vertices2[num9]);
				this.m_generatedInfo.m_uvmapArea += Triangle2.Area(uv[num7], uv[num8], uv[num9]);
			}
			if (flag)
			{
				Debug.Log(base.get_gameObject().get_name() + ": Custom tyres detected", base.get_gameObject());
				for (int n = 0; n < vertices2.Length; n++)
				{
					if (array[n].a == 0)
					{
						if (array[n].b != 255)
						{
							array[n] = new Color32(array[n].r, (byte)(fastList.m_size * 8), array[n].b, 255);
							Vector3 vector = vertices2[n];
							Vector3 vector2 = vertices2[n];
							Vector3 vector3 = Vector3.get_zero();
							int num10 = 0;
							for (int num11 = 0; num11 < vertices2.Length; num11++)
							{
								if (array[num11].a == 0 && array[num11].b == array[n].b)
								{
									array[num11] = new Color32(array[num11].r, (byte)(fastList.m_size * 8), array[num11].b, 255);
									vector = Vector3.Min(vector, vertices2[num11]);
									vector2 = Vector3.Max(vector2, vertices2[num11]);
									vector3 += vertices2[num11];
									num10++;
								}
							}
							Bounds bounds = default(Bounds);
							bounds.SetMinMax(vector, vector2);
							Vector3 vector4 = vector3 / (float)Mathf.Max(1, num10);
							Vector3 extents = bounds.get_extents();
							float num12 = Mathf.Max(extents.x, Mathf.Max(extents.y, extents.z));
							fastList.Add(new Vector4(vector4.x, vector4.y, vector4.z, num12));
						}
						else
						{
							array[n].a = 255;
						}
					}
				}
			}
			else
			{
				for (int num13 = 0; num13 < vertices2.Length; num13++)
				{
					if (vertices2[num13].y < 0.04f && array[num13].r == 0)
					{
						Bounds bounds2;
						this.ColorizeElement(vertices2, array, triangles, num13, new Color32(255, (byte)(fastList.m_size * 8), 255, 255), 0.001f, out bounds2);
						Vector3 center = bounds2.get_center();
						Vector3 extents2 = bounds2.get_extents();
						float num14 = Mathf.Max(extents2.x, Mathf.Max(extents2.y, extents2.z));
						fastList.Add(new Vector4(center.x, center.y, center.z, num14));
					}
				}
			}
			filters[k].get_sharedMesh().set_colors32(array);
		}
		this.m_generatedInfo.m_tyres = fastList.ToArray();
		num = 0f;
		num2 = 0f;
		num5 = 0f;
		num6 = 0f;
		for (int num15 = 0; num15 < fastList.m_size; num15++)
		{
			num = Mathf.Min(num, fastList.m_buffer[num15].x);
			num2 = Mathf.Max(num2, fastList.m_buffer[num15].x);
			num5 = Mathf.Min(num5, fastList.m_buffer[num15].z);
			num6 = Mathf.Max(num6, fastList.m_buffer[num15].z);
		}
		MeshRenderer[] componentsInChildren = base.GetComponentsInChildren<MeshRenderer>(true);
		for (int num16 = 0; num16 < componentsInChildren.Length; num16++)
		{
			for (int num17 = 0; num17 < componentsInChildren[num16].get_sharedMaterials().Length; num17++)
			{
				Material material = componentsInChildren[num16].get_sharedMaterials()[num17];
				if (material.get_shader().get_name().StartsWith("Custom/Vehicles/Vehicle"))
				{
					material.SetVector("_TyreParams", new Vector4(num5, (num5 + num6) * 0.5f, num6, num2 - num));
				}
			}
		}
		this.m_generatedInfo.m_wheelGauge = num2 - num;
		this.m_generatedInfo.m_wheelBase = num6 - num5;
		if (this.m_generatedInfo.m_wheelGauge < 0.01f || fastList.m_size <= 1)
		{
			this.m_generatedInfo.m_wheelGauge = this.m_generatedInfo.m_size.x * 0.5f;
		}
		if (this.m_generatedInfo.m_wheelBase < 0.01f || fastList.m_size <= 1)
		{
			this.m_generatedInfo.m_wheelBase = this.m_generatedInfo.m_size.z * 0.7f;
		}
	}

	private void ColorizeElement(Vector3[] vertices, Color32[] colors, int[] triangles, int vertex, Color32 color, float tolerance, out Bounds bounds)
	{
		bool[] array = new bool[vertices.Length];
		FastList<Vector3> fastList = new FastList<Vector3>();
		colors[vertex] = color;
		array[vertex] = true;
		fastList.Add(vertices[vertex]);
		Vector3 vector;
		vector..ctor(tolerance, tolerance, tolerance);
		bounds..ctor(vertices[vertex], vector * 2f);
		bool flag;
		do
		{
			flag = false;
			for (int i = 0; i < triangles.Length; i += 3)
			{
				int num = triangles[i];
				int num2 = triangles[i + 1];
				int num3 = triangles[i + 2];
				if (!array[num] || !array[num2] || !array[num3])
				{
					bool flag2 = array[num] || array[num2] || array[num3];
					if (!flag2)
					{
						Vector3 vector2 = vertices[num];
						if (bounds.Contains(vector2))
						{
							for (int j = 0; j < fastList.m_size; j++)
							{
								Vector3 vector3 = fastList.m_buffer[j] - vector2;
								if (vector3.x > -tolerance && vector3.x < tolerance && vector3.y > -tolerance && vector3.y < tolerance && vector3.z > -tolerance && vector3.z < tolerance)
								{
									colors[num] = color;
									array[num] = true;
									flag2 = true;
								}
							}
						}
						vector2 = vertices[num2];
						if (bounds.Contains(vector2))
						{
							for (int k = 0; k < fastList.m_size; k++)
							{
								Vector3 vector4 = fastList.m_buffer[k] - vector2;
								if (vector4.x > -tolerance && vector4.x < tolerance && vector4.y > -tolerance && vector4.y < tolerance && vector4.z > -tolerance && vector4.z < tolerance)
								{
									colors[num2] = color;
									array[num2] = true;
									flag2 = true;
								}
							}
						}
						vector2 = vertices[num3];
						if (bounds.Contains(vector2))
						{
							for (int l = 0; l < fastList.m_size; l++)
							{
								Vector3 vector5 = fastList.m_buffer[l] - vector2;
								if (vector5.x > -tolerance && vector5.x < tolerance && vector5.y > -tolerance && vector5.y < tolerance && vector5.z > -tolerance && vector5.z < tolerance)
								{
									colors[num3] = color;
									array[num3] = true;
									flag2 = true;
								}
							}
						}
					}
					if (flag2)
					{
						if (!array[num])
						{
							colors[num] = color;
							array[num] = true;
							fastList.Add(vertices[num]);
							bounds.Encapsulate(vertices[num] - vector);
							bounds.Encapsulate(vertices[num] + vector);
						}
						if (!array[num2])
						{
							colors[num2] = color;
							array[num2] = true;
							fastList.Add(vertices[num2]);
							bounds.Encapsulate(vertices[num2] - vector);
							bounds.Encapsulate(vertices[num2] + vector);
						}
						if (!array[num3])
						{
							colors[num3] = color;
							array[num3] = true;
							fastList.Add(vertices[num3]);
							bounds.Encapsulate(vertices[num3] - vector);
							bounds.Encapsulate(vertices[num3] + vector);
						}
						flag = true;
					}
				}
			}
		}
		while (flag);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.set_matrix(base.get_transform().get_localToWorldMatrix());
		if (this.m_generatedInfo != null && this.m_generatedInfo.m_tyres != null)
		{
			Gizmos.set_color(new Color(1f, 0.2f, 1f, 0.75f));
			for (int i = 0; i < this.m_generatedInfo.m_tyres.Length; i++)
			{
				Gizmos.DrawSphere(this.m_generatedInfo.m_tyres[i], this.m_generatedInfo.m_tyres[i].w);
			}
		}
		if (this.m_doors != null)
		{
			for (int j = 0; j < this.m_doors.Length; j++)
			{
				switch (this.m_doors[j].m_type)
				{
				case VehicleInfo.DoorType.Enter:
					Gizmos.set_color(new Color(0f, 1f, 0f, 0.75f));
					break;
				case VehicleInfo.DoorType.Exit:
					Gizmos.set_color(new Color(1f, 0f, 0f, 0.75f));
					break;
				case VehicleInfo.DoorType.Both:
					Gizmos.set_color(new Color(1f, 1f, 0f, 0.75f));
					break;
				default:
					Gizmos.set_color(new Color(1f, 1f, 1f, 0.75f));
					break;
				}
				Vector3 location = this.m_doors[j].m_location;
				location.y += 1f;
				Gizmos.DrawCube(location, new Vector3(0.5f, 2f, 1f));
			}
		}
		if (this.m_generatedInfo != null)
		{
			Gizmos.set_color(new Color(0f, 1f, 1f, 0.75f));
			Gizmos.DrawCube(new Vector3(0f, this.m_generatedInfo.m_size.y * 0.5f, this.m_generatedInfo.m_size.z * 0.5f - this.m_attachOffsetFront), new Vector3(0.25f, this.m_generatedInfo.m_size.y, 0.25f));
			Gizmos.DrawCube(new Vector3(0f, this.m_generatedInfo.m_size.y * 0.5f, this.m_attachOffsetBack - this.m_generatedInfo.m_size.z * 0.5f), new Vector3(0.25f, this.m_generatedInfo.m_size.y, 0.25f));
		}
	}

	public override PrefabAI GetAI()
	{
		return this.m_vehicleAI;
	}

	public override void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
		float num = 0f;
		if (this.m_vehicleAI != null)
		{
			num = this.m_vehicleAI.GetEditorVerticalOffset();
		}
		this.RenderMesh(cameraInfo, new Vector3(0f, num, 0f));
	}

	public void RenderMesh(RenderManager.CameraInfo cameraInfo, Vector3 offset)
	{
		if (this.m_lodMeshData == null && this.m_lodMesh != null && this.m_lodMesh.get_vertexCount() > 0)
		{
			this.m_lodMeshData = new RenderGroup.MeshData(this.m_lodMesh);
			this.m_lodMeshData.UpdateBounds();
		}
		Vector3 position = new Vector3(0f, 60f, 0f) + offset;
		Vehicle.RenderInstance(cameraInfo, this, position, Quaternion.get_identity(), Vector3.get_zero(), Vector4.get_zero(), Vector4.get_zero(), Vector3.get_zero(), 0f, this.m_color0, Vehicle.Flags.Created | Vehicle.Flags.Spawned, InstanceID.Empty, false, true);
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public ItemClass m_class;

	public ItemClass.Placement m_placementStyle;

	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	public VehicleInfo.VehicleType m_vehicleType;

	public float m_acceleration = 1f;

	public float m_braking = 2f;

	public float m_turning = 0.2f;

	[CustomizableProperty("Max speed")]
	public float m_maxSpeed = 20f;

	public float m_springs = 0.9f;

	public float m_dampers = 0.5f;

	public float m_nodMultiplier = 1f;

	public float m_leanMultiplier = 1f;

	public float m_attachOffsetFront;

	public float m_attachOffsetBack;

	[CustomizableProperty("Use color variations", "Vehicle general")]
	public bool m_useColorVariations = true;

	[CustomizableProperty("Position", "Lights")]
	public Vector3[] m_lightPositions;

	public VehicleInfo.VehicleDoor[] m_doors;

	public VehicleInfo.VehicleTrailer[] m_trailers;

	public VehicleInfo.MeshInfo[] m_subMeshes;

	public VehicleInfo.Effect[] m_effects;

	[NonSerialized]
	public VehicleAI m_vehicleAI;

	[NonSerialized]
	public Color m_color0;

	[NonSerialized]
	public Color m_color1;

	[NonSerialized]
	public Color m_color2;

	[NonSerialized]
	public Color m_color3;

	[NonSerialized]
	public bool m_isLargeVehicle;

	[Flags]
	public enum VehicleType
	{
		None = 0,
		Car = 1,
		Metro = 2,
		Train = 4,
		Ship = 8,
		Plane = 16,
		Bicycle = 32,
		Tram = 64,
		Helicopter = 128,
		Meteor = 256,
		Vortex = 512,
		Ferry = 1024,
		Monorail = 2048,
		CableCar = 4096,
		Blimp = 8192,
		All = 65535
	}

	[Flags]
	public enum DoorType
	{
		Enter = 1,
		Exit = 2,
		Both = 3,
		Load = 16
	}

	[Serializable]
	public struct VehicleDoor
	{
		public VehicleInfo.DoorType m_type;

		public Vector3 m_location;
	}

	[Serializable]
	public struct VehicleTrailer
	{
		public VehicleInfo m_info;

		public int m_probability;

		public int m_invertProbability;
	}

	[Serializable]
	public class MeshInfo
	{
		public VehicleInfoBase m_subInfo;

		[BitMask]
		public Vehicle.Flags m_vehicleFlagsRequired;

		[BitMask]
		public Vehicle.Flags m_vehicleFlagsForbidden;

		[BitMask]
		public VehicleParked.Flags m_parkedFlagsRequired;

		[BitMask]
		public VehicleParked.Flags m_parkedFlagsForbidden;
	}

	[Serializable]
	public struct Effect
	{
		public EffectInfo m_effect;

		[BitMask]
		public Vehicle.Flags m_vehicleFlagsRequired;

		[BitMask]
		public Vehicle.Flags m_vehicleFlagsForbidden;

		[BitMask]
		public VehicleParked.Flags m_parkedFlagsRequired;

		[BitMask]
		public VehicleParked.Flags m_parkedFlagsForbidden;
	}
}
