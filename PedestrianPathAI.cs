﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class PedestrianPathAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		if (elevation < -8f)
		{
			mode = InfoManager.InfoMode.Underground;
			subMode = InfoManager.SubInfoMode.Default;
		}
		else
		{
			mode = InfoManager.InfoMode.None;
			subMode = InfoManager.SubInfoMode.Default;
		}
	}

	public override void ReleaseNode(ushort nodeID, ref NetNode data)
	{
		if (data.m_lane != 0u)
		{
			this.RemoveLaneConnection(nodeID, ref data);
		}
		base.ReleaseNode(nodeID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		if (!this.m_invisible)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Flood);
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			Vector3 vector = (position + position2) * 0.5f;
			float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
			if (num > vector.y + 1f)
			{
				if ((data.m_flags & NetSegment.Flags.Flooded) == NetSegment.Flags.None)
				{
					data.m_flags |= NetSegment.Flags.Flooded;
					data.m_modifiedIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex++;
				}
				problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.Flooded;
				if (num > vector.y)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.Flood);
				}
			}
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(vector);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.BikeBan) != DistrictPolicies.CityPlanning.None)
			{
				data.m_flags |= NetSegment.Flags.BikeBan;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.BikeBan;
			}
			data.m_problems = problem;
		}
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None && (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(segmentID & 15)))
		{
			int delta = Mathf.RoundToInt(data.m_averageLength);
			StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.DestroyedLength);
			statisticBase.Add(delta);
		}
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = ((this.m_tunnelInfo == null || (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) == ItemClass.Availability.None) ? 0 : -3);
		max = ((this.m_elevatedInfo == null && this.m_bridgeInfo == null) ? 0 : 2);
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (this.m_invisible)
		{
			return this.m_info;
		}
		if (maxElevation > 255f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) != ItemClass.Availability.None)
		{
			if (this.m_tunnelInfo != null && maxElevation < -8f)
			{
				return this.m_tunnelInfo;
			}
			if (this.m_slopeInfo != null && minElevation < -8f)
			{
				return this.m_slopeInfo;
			}
		}
		if (this.m_bridgeInfo != null && maxElevation > 25f && length > 45f && !curved && (enableDouble || !this.m_bridgeInfo.m_netAI.RequireDoubleSegments()))
		{
			return this.m_bridgeInfo;
		}
		if (this.m_elevatedInfo != null && maxElevation > 0.1f)
		{
			return this.m_elevatedInfo;
		}
		if (maxElevation > 4f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth;
	}

	public override bool SupportUnderground()
	{
		return this.m_tunnelInfo != null && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) != ItemClass.Availability.None;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return false;
		}
		if (DefaultTool.IsOutOfCityArea(segmentID))
		{
			return false;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_7C_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_7C_cp_1 = disaster;
				expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength = expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Notification.Problem problems = data.m_problems;
		data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
		if (data.m_problems != problems)
		{
			Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
		return true;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_4B_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_4B_cp_1 = disaster;
				expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength = expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			Notification.Problem problems = data.m_problems;
			data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
			if (data.m_problems != problems)
			{
				Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
			}
		}
	}

	public override void AfterTerrainUpdate(ushort nodeID, ref NetNode data)
	{
		base.AfterTerrainUpdate(nodeID, ref data);
		if (!this.m_info.m_useFixedHeight)
		{
			Vector3 position = data.m_position;
			float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			if (Mathf.Abs(num - data.m_position.y) > 0.1f)
			{
				position.y = num;
				Singleton<NetManager>.get_instance().MoveNode(nodeID, position);
			}
		}
	}

	public override void AfterTerrainUpdate(ushort segmentID, ref NetSegment data)
	{
		base.AfterTerrainUpdate(segmentID, ref data);
		if (this.m_info.m_hasTerrainHeightProps)
		{
			Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		}
	}

	public override void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
		Singleton<NetManager>.get_instance().UpdateNode(nodeID, 0, 10);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		uint num = 0u;
		byte offset = 0;
		float num2 = 1E+10f;
		if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
		{
			if (this.m_connectService1 != ItemClass.Service.None)
			{
				float maxDistance = this.m_maxConnectDistance1;
				if ((data.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
				{
					maxDistance = this.m_maxConnectDistanceEnd1;
				}
				PathUnit.Position pathPos;
				PathUnit.Position position;
				float num3;
				float num4;
				if (PathManager.FindPathPosition(data.m_position, this.m_connectService1, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, this.m_canConnectTunnel, true, maxDistance, out pathPos, out position, out num3, out num4) && num3 < num2)
				{
					num2 = num3;
					num = PathManager.GetLaneID(pathPos);
					offset = pathPos.m_offset;
				}
			}
			if (this.m_connectService2 != ItemClass.Service.None)
			{
				float maxDistance2 = this.m_maxConnectDistance2;
				if ((data.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
				{
					maxDistance2 = this.m_maxConnectDistanceEnd2;
				}
				PathUnit.Position pathPos2;
				PathUnit.Position position2;
				float num5;
				float num6;
				if (PathManager.FindPathPosition(data.m_position, this.m_connectService2, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, this.m_canConnectTunnel, true, maxDistance2, out pathPos2, out position2, out num5, out num6) && num5 < num2)
				{
					num = PathManager.GetLaneID(pathPos2);
					offset = pathPos2.m_offset;
				}
			}
		}
		if (num != data.m_lane)
		{
			if (data.m_lane != 0u)
			{
				this.RemoveLaneConnection(nodeID, ref data);
			}
			if (num != 0u)
			{
				this.AddLaneConnection(nodeID, ref data, num, offset);
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].CalculatePosition((float)offset * 0.003921569f);
		if (vector.y != data.m_position.y)
		{
			data.m_position.y = vector.y;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
		float num4 = (float)data.m_elevation;
		if (this.IsUnderground())
		{
			num4 = -num4;
		}
		float num5 = NetSegment.SampleTerrainHeight(this.m_info, data.m_position, false, num4);
		if (num5 != data.m_position.y)
		{
			data.m_position.y = num5;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		if (test)
		{
			ushort num = middlePoint.m_segment;
			if (startPoint.m_segment == num || endPoint.m_segment == num)
			{
				num = 0;
			}
			if (num != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) == NetSegment.Flags.None)
			{
				NetInfo info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info;
				if (this.m_elevatedInfo == info)
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
				if (this.m_bridgeInfo == info)
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
				if (this.m_tunnelInfo == info)
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
				if (this.m_slopeInfo == info)
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
			}
		}
		return toolErrors;
	}

	public override bool CanUpgradeTo(NetInfo info)
	{
		return this.m_elevatedInfo != info && this.m_bridgeInfo != info && this.m_tunnelInfo != info && this.m_slopeInfo != info && base.CanUpgradeTo(info);
	}

	public override bool IsUnderground()
	{
		return this.m_underground;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	public override float GetEndRadius()
	{
		return 0f;
	}

	public ItemClass.Service m_connectService1 = ItemClass.Service.Road;

	public ItemClass.Service m_connectService2 = ItemClass.Service.PublicTransport;

	public float m_maxConnectDistance1 = 8f;

	public float m_maxConnectDistance2 = 8f;

	public float m_maxConnectDistanceEnd1 = 16.5f;

	public float m_maxConnectDistanceEnd2 = 16.5f;

	public NetInfo m_elevatedInfo;

	public NetInfo m_bridgeInfo;

	public NetInfo m_slopeInfo;

	public NetInfo m_tunnelInfo;

	public bool m_invisible;

	public bool m_underground;

	public bool m_canConnectTunnel;
}
