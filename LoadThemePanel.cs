﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class LoadThemePanel : LoadSavePanelBase<MapThemeMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_SaveList = base.Find<UIListBox>("SaveList");
		this.m_SaveList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnListingSelectionChanged));
		this.m_Theme = base.Find<UILabel>("MapTheme");
		this.m_MapName = base.Find<UILabel>("MapName");
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_LoadButton = base.Find<UIButton>("Load");
	}

	private void OnListingSelectionChanged(UIComponent comp, int sel)
	{
		if (sel > -1)
		{
			MapThemeMetaData listingMetaData = base.GetListingMetaData(sel);
			this.m_MapName.set_text(listingMetaData.name);
			string text = listingMetaData.environment;
			if (Locale.Exists("THEME_NAME", text))
			{
				text = Locale.Get("THEME_NAME", text);
			}
			this.m_Theme.set_text(text);
			if (this.m_SnapShot.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShot.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShot.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShot.get_texture() != null)
				{
					this.m_SnapShot.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShot.set_texture(null);
			}
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			ToolsMenu.SetFocus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveList.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnLoad();
			}
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_SaveList && sel != -1)
		{
			if (this.m_SaveList.get_selectedIndex() != sel)
			{
				this.m_SaveList.set_selectedIndex(sel);
			}
			this.OnLoad();
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("LoadMapPanel.Refresh()"))
		{
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.MapThemeMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						MapThemeMetaData mapThemeMetaData = current.Instantiate<MapThemeMetaData>();
						mapThemeMetaData.SetSelfRef(current);
						if (mapThemeMetaData.environment != "Winter" || flag)
						{
							base.AddToListing(current.get_name(), mapThemeMetaData.timeStamp, current, mapThemeMetaData, true);
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			this.m_SaveList.set_items(base.GetListingItems());
			if (base.GetListingCount() > 0)
			{
				int num = base.FindIndexOf(LoadThemePanel.m_LastSaveName);
				this.m_SaveList.set_selectedIndex((num == -1) ? 0 : num);
				this.m_LoadButton.set_isEnabled(true);
			}
			else
			{
				this.m_LoadButton.set_isEnabled(false);
			}
		}
	}

	public void OnLoad()
	{
		base.CloseEverything();
		LoadThemePanel.m_LastSaveName = base.GetListingName(this.m_SaveList.get_selectedIndex());
		SaveMapPanel.lastLoadedName = LoadThemePanel.m_LastSaveName;
		MapThemeMetaData listingMetaData = base.GetListingMetaData(this.m_SaveList.get_selectedIndex());
		base.PrintModsInfo(listingMetaData.mods);
		SaveMapPanel.lastLoadedAsset = listingMetaData.name;
		SaveMapPanel.lastPublished = listingMetaData.isPublished;
		string listingPackageName = base.GetListingPackageName(this.m_SaveList.get_selectedIndex());
		PublishedFileId invalid = PublishedFileId.invalid;
		ulong num;
		if (ulong.TryParse(listingPackageName, out num))
		{
			invalid..ctor(num);
		}
		SimulationMetaData simulationMetaData = new SimulationMetaData();
		simulationMetaData.m_WorkshopPublishedFileId = invalid;
		simulationMetaData.m_updateMode = SimulationManager.UpdateMode.LoadMap;
		simulationMetaData.m_MapThemeMetaData = listingMetaData;
		simulationMetaData.m_environment = listingMetaData.environment;
		if (!string.IsNullOrEmpty(this.m_forceEnvironment))
		{
			simulationMetaData.m_environment = this.m_forceEnvironment;
		}
		Package.Asset asset = PackageManager.FindAssetByName("System.BuiltinTerrainMap-1aa5g6dfs748sf_Data");
		if (asset != null)
		{
			Singleton<LoadingManager>.get_instance().LoadLevel(asset, "ThemeEditor", "InThemeEditor", simulationMetaData);
		}
		else
		{
			Package.Asset asset2 = PackageManager.FindAssetByName("System.BuiltinTerrainMap-" + listingMetaData.environment);
			SystemMapMetaData systemMapMetaData = asset2.Instantiate<SystemMapMetaData>();
			Singleton<LoadingManager>.get_instance().LoadLevel(systemMapMetaData.assetRef, "ThemeEditor", "InThemeEditor", simulationMetaData);
		}
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	public string m_forceEnvironment;

	private static string m_LastSaveName;

	private UILabel m_MapName;

	private UITextureSprite m_SnapShot;

	private UIListBox m_SaveList;

	private UIButton m_LoadButton;

	private UILabel m_Theme;
}
