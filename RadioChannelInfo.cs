﻿using System;
using ColossalFramework.Globalization;

public class RadioChannelInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
	}

	public override void DestroyPrefab()
	{
		base.DestroyPrefab();
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("RADIO_CHANNEL_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("RADIO_CHANNEL_TITLE", base.get_gameObject().get_name());
	}

	public RadioChannelInfo.State[] m_stateChain;

	[NonSerialized]
	public bool m_hasChannelData;

	[Serializable]
	public struct State
	{
		public RadioContentInfo.ContentType m_contentType;

		public int m_minCount;

		public int m_maxCount;
	}
}
