﻿using System;
using ColossalFramework.IO;

public struct WaterWave
{
	public void Serialize(DataSerializer s)
	{
		s.WriteUInt16((uint)this.m_type);
		s.WriteUInt16((uint)this.m_minX);
		s.WriteUInt16((uint)this.m_minZ);
		s.WriteUInt16((uint)this.m_maxX);
		s.WriteUInt16((uint)this.m_maxZ);
		s.WriteUInt16((uint)this.m_origX);
		s.WriteUInt16((uint)this.m_origZ);
		s.WriteInt16((int)this.m_dirX);
		s.WriteInt16((int)this.m_dirZ);
		s.WriteInt16((int)this.m_delta);
		s.WriteUInt16((uint)this.m_duration);
		s.WriteUInt16((uint)this.m_currentTime);
	}

	public void Deserialize(DataSerializer s)
	{
		if (s.get_version() >= 261u)
		{
			this.m_type = (ushort)s.ReadUInt16();
		}
		else
		{
			this.m_type = ((!s.ReadBool()) ? 0 : 1);
		}
		this.m_minX = (ushort)s.ReadUInt16();
		this.m_minZ = (ushort)s.ReadUInt16();
		this.m_maxX = (ushort)s.ReadUInt16();
		this.m_maxZ = (ushort)s.ReadUInt16();
		this.m_origX = (ushort)s.ReadUInt16();
		this.m_origZ = (ushort)s.ReadUInt16();
		this.m_dirX = (short)s.ReadInt16();
		this.m_dirZ = (short)s.ReadInt16();
		this.m_delta = (short)s.ReadInt16();
		this.m_duration = (ushort)s.ReadUInt16();
		this.m_currentTime = (ushort)s.ReadUInt16();
	}

	public int GetSeaLevel(int original, int x, int z)
	{
		if (x < (int)this.m_minX || z < (int)this.m_minZ || x > (int)this.m_maxX || z > (int)this.m_maxZ)
		{
			return original;
		}
		int num = (x - (int)this.m_origX) * (int)this.m_dirX + (z - (int)this.m_origZ) * (int)this.m_dirZ >> 8;
		int num2 = (int)this.m_currentTime - num;
		if (num2 <= 0 || num2 >= (int)this.m_duration)
		{
			return original;
		}
		int num3 = (int)this.m_delta * (65536 - (int)this.m_currentTime) >> 16;
		int num4 = this.m_duration >> 6;
		int multiplier = num3 - FixedMath.Cos((num2 << 10) / num4, num3);
		return original - (FixedMath.Sin(num2 * 1536 / num4, multiplier) >> 1);
	}

	public const ushort TYPE_NONE = 0;

	public const ushort TYPE_TSUNAMI = 1;

	public const ushort TYPE_IMPACT = 2;

	public ushort m_type;

	public ushort m_minX;

	public ushort m_minZ;

	public ushort m_maxX;

	public ushort m_maxZ;

	public ushort m_origX;

	public ushort m_origZ;

	public short m_dirX;

	public short m_dirZ;

	public short m_delta;

	public ushort m_duration;

	public ushort m_currentTime;
}
