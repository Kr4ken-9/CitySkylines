﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class SnapSettingsPanel : ToolsModifierControl
{
	public static bool snapToAngles
	{
		get
		{
			return (SnapSettingsPanel.m_netTool.m_snap & NetTool.Snapping.Angle) != NetTool.Snapping.None;
		}
		set
		{
			if (value)
			{
				SnapSettingsPanel.m_netTool.m_snap |= NetTool.Snapping.Angle;
			}
			else
			{
				SnapSettingsPanel.m_netTool.m_snap &= ~NetTool.Snapping.Angle;
			}
		}
	}

	public static bool snapToLength
	{
		get
		{
			return (SnapSettingsPanel.m_netTool.m_snap & NetTool.Snapping.Length) != NetTool.Snapping.None;
		}
		set
		{
			if (value)
			{
				SnapSettingsPanel.m_netTool.m_snap |= NetTool.Snapping.Length;
			}
			else
			{
				SnapSettingsPanel.m_netTool.m_snap &= ~NetTool.Snapping.Length;
			}
		}
	}

	public static bool snapToGrid
	{
		get
		{
			return (SnapSettingsPanel.m_netTool.m_snap & NetTool.Snapping.Grid) != NetTool.Snapping.None;
		}
		set
		{
			if (value)
			{
				SnapSettingsPanel.m_netTool.m_snap |= NetTool.Snapping.Grid;
			}
			else
			{
				SnapSettingsPanel.m_netTool.m_snap &= ~NetTool.Snapping.Grid;
			}
		}
	}

	public static bool snapToHelperLine
	{
		get
		{
			return (SnapSettingsPanel.m_netTool.m_snap & NetTool.Snapping.HelperLine) != NetTool.Snapping.None;
		}
		set
		{
			if (value)
			{
				SnapSettingsPanel.m_netTool.m_snap |= NetTool.Snapping.HelperLine;
			}
			else
			{
				SnapSettingsPanel.m_netTool.m_snap &= ~NetTool.Snapping.HelperLine;
			}
		}
	}

	public static bool snapToAll
	{
		get
		{
			return SnapSettingsPanel.snapToAngles && SnapSettingsPanel.snapToGrid && SnapSettingsPanel.snapToHelperLine && SnapSettingsPanel.snapToLength;
		}
		set
		{
			SnapSettingsPanel.snapToAngles = value;
			SnapSettingsPanel.snapToGrid = value;
			SnapSettingsPanel.snapToHelperLine = value;
			SnapSettingsPanel.snapToLength = value;
		}
	}

	public bool snapToAllNonStatic
	{
		get
		{
			return SnapSettingsPanel.snapToAll;
		}
		set
		{
			SnapSettingsPanel.snapToAll = value;
		}
	}

	public bool snapToAngleshNonStatic
	{
		get
		{
			return SnapSettingsPanel.snapToAngles;
		}
		set
		{
			SnapSettingsPanel.snapToAngles = value;
		}
	}

	public bool snapToLengthNonStatic
	{
		get
		{
			return SnapSettingsPanel.snapToLength;
		}
		set
		{
			SnapSettingsPanel.snapToLength = value;
		}
	}

	public bool snapToGridNonStatic
	{
		get
		{
			return SnapSettingsPanel.snapToGrid;
		}
		set
		{
			SnapSettingsPanel.snapToGrid = value;
		}
	}

	public bool snapToHelperLineNonStatic
	{
		get
		{
			return SnapSettingsPanel.snapToHelperLine;
		}
		set
		{
			SnapSettingsPanel.snapToHelperLine = value;
		}
	}

	private void Awake()
	{
		SnapSettingsPanel.m_netTool = ToolsModifierControl.GetTool<NetTool>();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		SnapSettingsPanel.snapToAngles = true;
		SnapSettingsPanel.snapToGrid = true;
		SnapSettingsPanel.snapToLength = true;
		SnapSettingsPanel.snapToHelperLine = true;
	}

	public static void ToggleVisibility(bool show)
	{
		if (show)
		{
			UIView.get_library().Show("SnapSettingsPanel");
		}
		else
		{
			UIView.get_library().Hide("SnapSettingsPanel");
		}
	}

	public static bool isVisible
	{
		get
		{
			return UIView.get_library().Get<UIComponent>("SnapSettingsPanel").get_isVisible();
		}
	}

	private static NetTool m_netTool;
}
