﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class WaterOptionPanel : ToolsModifierControl
{
	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIInput.add_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
		else
		{
			UIInput.remove_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
	}

	private void ProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (eventType != 4)
		{
			return;
		}
		if (this.m_IncreaseCapacity.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_CapacitySlider.set_value(this.m_CapacitySlider.get_value() + WaterOptionPanel.kCapacityInterval);
		}
		else if (this.m_DecreaseCapacity.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_CapacitySlider.set_value(this.m_CapacitySlider.get_value() - WaterOptionPanel.kCapacityInterval);
		}
	}

	private void SetCapacity(float capacity)
	{
		WaterTool tool = ToolsModifierControl.GetTool<WaterTool>();
		if (tool != null)
		{
			tool.m_capacity = capacity;
		}
	}

	private void Awake()
	{
		this.m_IncreaseCapacity = new SavedInputKey(Settings.mapEditorIncreaseCapacity, Settings.inputSettingsFile, DefaultSettings.mapEditorIncreaseCapacity, true);
		this.m_DecreaseCapacity = new SavedInputKey(Settings.mapEditorDecreaseCapacity, Settings.inputSettingsFile, DefaultSettings.mapEditorDecreaseCapacity, true);
		this.Hide();
		this.m_CapacitySlider = base.Find<UISlider>("Capacity");
		this.m_CapacityTextfield = base.Find<UITextField>("Capacity");
		this.m_CapacityTextfield.set_text(this.m_CapacitySlider.get_value().ToString("0.00"));
		this.m_CapacitySlider.add_eventValueChanged(delegate(UIComponent sender, float value)
		{
			this.m_CapacityTextfield.set_text(value.ToString("0.00"));
			this.SetCapacity(value);
		});
		this.m_CapacityTextfield.add_eventTextSubmitted(delegate(UIComponent sender, string s)
		{
			float num;
			if (float.TryParse(s, out num))
			{
				this.m_CapacitySlider.set_value(num);
				this.SetCapacity(num);
			}
			else
			{
				this.m_CapacityTextfield.set_text(this.m_CapacitySlider.get_value().ToString("0.00"));
			}
		});
		this.m_CapacityTextfield.add_eventTextCancelled(delegate(UIComponent sender, string s)
		{
			this.m_CapacityTextfield.set_text(this.m_CapacitySlider.get_value().ToString("0.00"));
		});
	}

	private static readonly float kCapacityInterval = 0.1f;

	private SavedInputKey m_IncreaseCapacity;

	private SavedInputKey m_DecreaseCapacity;

	private UISlider m_CapacitySlider;

	private UITextField m_CapacityTextfield;
}
