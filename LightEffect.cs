﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class LightEffect : EffectInfo
{
	public override void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_batchedLight && !id.IsEmpty)
		{
			return;
		}
		Vector3 vector = this.m_position;
		if (this.m_positionIndex >= 0)
		{
			if (area.m_positions2 == null || this.m_positionIndex >= area.m_positions2.Length)
			{
				return;
			}
			vector = area.m_positions2[this.m_positionIndex];
		}
		if (area.m_meshData != null && this.m_alignment != (LightEffect.Alignment)0)
		{
			if ((this.m_alignment & LightEffect.Alignment.Left) != (LightEffect.Alignment)0)
			{
				vector.x += area.m_meshData.m_bounds.get_min().x;
			}
			if ((this.m_alignment & LightEffect.Alignment.Right) != (LightEffect.Alignment)0)
			{
				vector.x += area.m_meshData.m_bounds.get_max().x;
			}
			if ((this.m_alignment & LightEffect.Alignment.Down) != (LightEffect.Alignment)0)
			{
				vector.y += area.m_meshData.m_bounds.get_min().y;
			}
			if ((this.m_alignment & LightEffect.Alignment.Up) != (LightEffect.Alignment)0)
			{
				vector.y += area.m_meshData.m_bounds.get_max().y;
			}
			if ((this.m_alignment & LightEffect.Alignment.Backward) != (LightEffect.Alignment)0)
			{
				vector.z += area.m_meshData.m_bounds.get_min().z;
			}
			if ((this.m_alignment & LightEffect.Alignment.Forward) != (LightEffect.Alignment)0)
			{
				vector.z += area.m_meshData.m_bounds.get_max().z;
			}
		}
		vector = area.m_matrix.MultiplyPoint(vector);
		Light component = base.GetComponent<Light>();
		float num = component.get_intensity() * magnitude;
		float num2 = Vector3.Distance(vector, cameraInfo.m_position);
		if (num2 < this.m_fadeEndDistance || this.m_batchedLight)
		{
			LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
			if (num2 > this.m_fadeStartDistance && !this.m_batchedLight)
			{
				num *= (this.m_fadeEndDistance - num2) / (this.m_fadeEndDistance - this.m_fadeStartDistance);
			}
			Randomizer randomizer;
			randomizer..ctor(id.Index);
			float num3 = this.m_offRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (this.m_offRange.y - this.m_offRange.x);
			num *= MathUtils.SmoothStep(num3 + 0.01f, num3 - 0.01f, lightSystem.DayLightIntensity);
			if (this.m_blinkType != LightEffect.BlinkType.None)
			{
				Vector4 blinkVector = LightEffect.GetBlinkVector(this.m_blinkType);
				float num4 = num3 * 3.71f + Singleton<SimulationManager>.get_instance().m_simulationTimer / blinkVector.w;
				num4 = (num4 - Mathf.Floor(num4)) * blinkVector.w;
				float num5 = MathUtils.SmoothStep(blinkVector.x, blinkVector.y, num4);
				float num6 = MathUtils.SmoothStep(blinkVector.w, blinkVector.z, num4);
				num *= 1f - num5 * num6;
			}
			Color color = component.get_color();
			if (this.m_variationColors != null && this.m_variationColors.Length != 0)
			{
				Color color2 = this.m_variationColors[randomizer.Int32((uint)this.m_variationColors.Length)];
				color = Color.Lerp(color, color2, (float)randomizer.Int32(1000u) * 0.001f);
			}
			if (timeOffset >= 0f && this.m_renderDuration != 0f)
			{
				if (timeOffset > this.m_renderDuration)
				{
					return;
				}
				num *= this.m_intensityCurve.Evaluate(timeOffset / this.m_renderDuration);
			}
			if (num > 0.001f)
			{
				if (component.get_type() == null)
				{
					Vector3 dir;
					if (this.m_rotationSpeed != 0)
					{
						float num7 = (float)(this.m_rotationSpeed * 6) * Singleton<SimulationManager>.get_instance().m_simulationTimer;
						Quaternion quaternion = Quaternion.AngleAxis(num7, this.m_rotationAxis);
						dir = area.m_matrix.MultiplyVector(quaternion * base.get_transform().get_forward());
					}
					else
					{
						dir = area.m_matrix.MultiplyVector(base.get_transform().get_forward());
					}
					lightSystem.DrawLight(component.get_type(), vector, dir, velocity, color, num, component.get_range(), component.get_spotAngle(), this.m_spotLeaking, true);
				}
				else
				{
					lightSystem.DrawLight(component.get_type(), vector, Vector3.get_forward(), velocity, color, num, component.get_range(), component.get_spotAngle(), 0f, true);
				}
			}
		}
	}

	public static Vector4 GetBlinkVector(LightEffect.BlinkType type)
	{
		switch (type)
		{
		case LightEffect.BlinkType.Blink_050_050:
			return new Vector4(0.49f, 0.5f, 0.99f, 1f);
		case LightEffect.BlinkType.MildFade_0125_0125:
			return new Vector4(0f, 0.5f, -0.25f, 0.25f);
		case LightEffect.BlinkType.MediumFade_500_500:
			return new Vector4(0f, 6f, 4f, 10f);
		case LightEffect.BlinkType.StrongBlaze_0125_0125:
			return new Vector4(0f, 0.3f, -0.1f, 0.25f);
		case LightEffect.BlinkType.StrongFade_250_250:
			return new Vector4(0f, 2.5f, 2.5f, 5f);
		case LightEffect.BlinkType.Blink_025_025:
			return new Vector4(0.24f, 0.25f, 0.49f, 0.5f);
		default:
			return new Vector4(0f, -1f, 2f, 1f);
		}
	}

	protected override void CreateEffect()
	{
		base.CreateEffect();
		if (this.m_batchedLight)
		{
			LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
			Light component = base.GetComponent<Light>();
			this.m_groupLayer = base.get_gameObject().get_layer();
			if (this.m_groupLayer == lightSystem.m_lightLayer)
			{
				this.m_groupLayerFloating = lightSystem.m_lightLayerFloating;
			}
			this.m_lightType = component.get_type();
			this.m_lightSpotAngle = component.get_spotAngle();
			this.m_lightRange = component.get_range();
			this.m_lightIntensity = component.get_intensity();
			this.m_lightColor = component.get_color();
			this.m_lightRotation = base.get_transform().get_localRotation();
		}
	}

	public override bool CalculateGroupData(int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		if (layer == this.m_groupLayer)
		{
			vertexCount += 6;
			triangleCount += 12;
			objectCount++;
			vertexArrays |= (RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3);
			return true;
		}
		if (layer == this.m_groupLayerFloating)
		{
			vertexCount += 6;
			triangleCount += 12;
			objectCount++;
			vertexArrays |= (RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs3 | RenderGroup.VertexArrays.Uvs4);
			return true;
		}
		return false;
	}

	public override void PopulateGroupData(int layer, InstanceID id, Vector3 pos, Vector3 dir, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if (layer == this.m_groupLayer || layer == this.m_groupLayerFloating)
		{
			bool flag = layer == this.m_groupLayerFloating;
			Vector3 vector;
			vector..ctor(this.m_lightRange, this.m_lightRange, this.m_lightRange);
			Vector3 vector2 = pos - vector;
			Vector3 vector3 = pos + vector;
			min = Vector3.Min(min, vector2);
			max = Vector3.Max(max, vector3);
			Vector3 vector4 = pos - groupPosition;
			maxRenderDistance = Mathf.Max(maxRenderDistance, 30000f);
			Vector3 vector5;
			if (this.m_lightType == null)
			{
				Quaternion quaternion = Quaternion.LookRotation(dir);
				vector5 = quaternion * (this.m_lightRotation * Vector3.get_forward());
			}
			else
			{
				vector5 = Vector3.get_forward();
			}
			Vector4 vector6;
			vector6..ctor(vector5.x, vector5.y, vector5.z, 100000f);
			if (this.m_lightType == null)
			{
				vector6.w = -Mathf.Cos(0.0174532924f * this.m_lightSpotAngle * 0.5f);
			}
			Vector2 vector7;
			vector7..ctor(1f / (this.m_lightRange * this.m_lightRange), this.m_lightIntensity);
			Randomizer randomizer;
			randomizer..ctor(id.Index);
			Vector2 vector8;
			vector8.x = this.m_offRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (this.m_offRange.y - this.m_offRange.x);
			vector8.y = (float)this.m_blinkType;
			ushort building = id.Building;
			if (building == 0)
			{
				int num;
				id.GetBuildingProp(out building, out num);
			}
			Vector2 vector9;
			Vector2 vector11;
			if (building != 0)
			{
				vector9 = RenderManager.GetColorLocation((uint)building);
				if (flag)
				{
					Vector3 vector10 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].CalculateMeshPosition();
					vector11 = VectorUtils.XZ(vector10);
					pos.y -= vector10.y;
					vector4.y -= vector10.y;
				}
				else
				{
					vector11 = Vector2.get_zero();
				}
			}
			else
			{
				vector9 = RenderManager.DefaultColorLocation;
				vector11 = Vector2.get_zero();
			}
			Color color = this.m_lightColor;
			if (this.m_variationColors != null && this.m_variationColors.Length != 0)
			{
				Color color2 = this.m_variationColors[randomizer.Int32((uint)this.m_variationColors.Length)];
				color = Color.Lerp(color, color2, (float)randomizer.Int32(1000u) * 0.001f);
			}
			color = color.get_linear();
			color.a = ((this.m_lightType != null) ? 0f : this.m_spotLeaking);
			float num2 = this.m_lightRange / Mathf.Cos(0.5235988f);
			float num3 = num2 * 0.5f;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x - num3, vector4.y + this.m_lightRange, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x + num3, vector4.y + this.m_lightRange, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x + num2, vector4.y, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x + num3, vector4.y - this.m_lightRange, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x - num3, vector4.y - this.m_lightRange, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_vertices[vertexIndex] = new Vector3(vector4.x - num2, vector4.y, vector4.z + this.m_lightRange);
			data.m_normals[vertexIndex] = pos;
			data.m_tangents[vertexIndex] = vector6;
			data.m_uvs[vertexIndex] = vector7;
			data.m_uvs2[vertexIndex] = vector8;
			data.m_uvs3[vertexIndex] = vector9;
			if (flag)
			{
				data.m_uvs4[vertexIndex] = vector11;
			}
			data.m_colors[vertexIndex] = color;
			vertexIndex++;
			data.m_triangles[triangleIndex++] = vertexIndex - 6;
			data.m_triangles[triangleIndex++] = vertexIndex - 5;
			data.m_triangles[triangleIndex++] = vertexIndex - 1;
			data.m_triangles[triangleIndex++] = vertexIndex - 1;
			data.m_triangles[triangleIndex++] = vertexIndex - 5;
			data.m_triangles[triangleIndex++] = vertexIndex - 4;
			data.m_triangles[triangleIndex++] = vertexIndex - 1;
			data.m_triangles[triangleIndex++] = vertexIndex - 4;
			data.m_triangles[triangleIndex++] = vertexIndex - 2;
			data.m_triangles[triangleIndex++] = vertexIndex - 2;
			data.m_triangles[triangleIndex++] = vertexIndex - 4;
			data.m_triangles[triangleIndex++] = vertexIndex - 3;
		}
	}

	public override bool RequireRender()
	{
		return !this.m_batchedLight;
	}

	public override float RenderDistance()
	{
		return (!this.m_batchedLight) ? this.m_fadeEndDistance : 0f;
	}

	public override float RenderDuration()
	{
		return this.m_renderDuration;
	}

	public override int GroupLayer()
	{
		return this.m_groupLayer;
	}

	public float m_fadeStartDistance = 500f;

	public float m_fadeEndDistance = 1000f;

	public float m_fadeSpeed = 10f;

	public Vector2 m_offRange = new Vector2(1000f, 1000f);

	[BitMask]
	public LightEffect.Alignment m_alignment;

	public Vector3 m_position = Vector3.get_zero();

	public int m_positionIndex = -1;

	public bool m_batchedLight;

	public float m_spotLeaking;

	public Color[] m_variationColors;

	public LightEffect.BlinkType m_blinkType;

	public int m_rotationSpeed;

	public Vector3 m_rotationAxis = Vector3.get_up();

	public float m_renderDuration;

	public AnimationCurve m_intensityCurve;

	[NonSerialized]
	private int m_groupLayer;

	[NonSerialized]
	private int m_groupLayerFloating;

	[NonSerialized]
	private LightType m_lightType;

	[NonSerialized]
	private float m_lightSpotAngle;

	[NonSerialized]
	private float m_lightRange;

	[NonSerialized]
	private float m_lightIntensity;

	[NonSerialized]
	private Color m_lightColor;

	[NonSerialized]
	private Quaternion m_lightRotation;

	[Flags]
	public enum Alignment
	{
		Left = 1,
		Right = 2,
		Down = 16,
		Up = 32,
		Backward = 256,
		Forward = 512
	}

	public enum BlinkType
	{
		None,
		Blink_050_050,
		MildFade_0125_0125,
		MediumFade_500_500,
		StrongBlaze_0125_0125,
		StrongFade_250_250,
		Blink_025_025
	}
}
