﻿using System;
using UnityEngine;

public class PlasmaballAnim : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
		float num = Time.get_time() * this.textureSpeed;
		base.GetComponent<Renderer>().get_material().SetTextureOffset("_MainTex", new Vector2(num * 0.8f, num));
		float num2 = Time.get_deltaTime() * this.rotationSpeed1 * 500f;
		base.get_transform().Rotate(Vector3.get_left() * Time.get_deltaTime() * num2);
		float num3 = Time.get_deltaTime() * this.rotationSpeed2 * 750f;
		base.get_transform().Rotate(Vector3.get_up() * Time.get_deltaTime() * num3);
	}

	public float textureSpeed = 1f;

	public float rotationSpeed1 = 10f;

	public float rotationSpeed2 = 10f;
}
