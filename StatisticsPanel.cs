﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class StatisticsPanel : MenuPanel
{
	protected override void Awake()
	{
		UICheckBox uICheckBox;
		for (int i = 0; i < this.StatisticsNames.Length; i++)
		{
			uICheckBox = base.Find<UICheckBox>(this.StatisticsNames[i] + "Checkbox");
			uICheckBox.set_isChecked(false);
			uICheckBox.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnCheckboxChanged));
			UILabel uILabel = uICheckBox.Find("OnOff") as UILabel;
			uILabel.set_textColor(this.StatisticsColors[i]);
		}
		this.m_Graph = base.Find<UIGraph>("StatisticsGraph");
		this.m_Graph.Clear();
		uICheckBox = base.Find<UICheckBox>("PopulationCheckbox");
		uICheckBox.set_isChecked(true);
		if (!SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
		{
			UICheckBox uICheckBox2 = base.Find<UICheckBox>("CyclistsCheckbox");
			uICheckBox2.set_isVisible(false);
		}
		if (!SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			UICheckBox uICheckBox3 = base.Find<UICheckBox>("DisastersCheckbox");
			UICheckBox uICheckBox4 = base.Find<UICheckBox>("CasualtiesCheckbox");
			UICheckBox uICheckBox5 = base.Find<UICheckBox>("CollapsedCheckbox");
			UICheckBox uICheckBox6 = base.Find<UICheckBox>("DestroyedRoadsCheckbox");
			uICheckBox3.set_isVisible(false);
			uICheckBox4.set_isVisible(false);
			uICheckBox5.set_isVisible(false);
			uICheckBox6.set_isVisible(false);
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
			this.m_Graph.Clear();
			for (int i = 0; i < this.StatisticsNames.Length; i++)
			{
				UICheckBox uICheckBox = base.Find<UICheckBox>(this.StatisticsNames[i] + "Checkbox");
				if (uICheckBox.get_isChecked())
				{
					this.OnCheckboxChanged(uICheckBox, true);
				}
			}
		}
		else
		{
			this.m_Graph.Clear();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	private void OnCheckboxChanged(UIComponent comp, bool check)
	{
		string stringUserData = comp.get_stringUserData();
		UILabel uILabel = comp.Find("OnOff") as UILabel;
		string text;
		if (uILabel != null)
		{
			text = uILabel.get_localeID();
		}
		else
		{
			text = string.Empty;
		}
		if (!check)
		{
			this.m_Graph.RemoveCurve(stringUserData);
		}
		else
		{
			double[] array = new double[256];
			int num = 0;
			int num2 = 0;
			DateTime dateTime = new DateTime(2015, 1, 1);
			DateTime dateTime2 = new DateTime(2015, 1, 2);
			DateTime d = new DateTime(2015, 1, 1);
			DateTime d2 = new DateTime(2015, 1, 2);
			int num3 = 0;
			float num4 = float.NegativeInfinity;
			StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Get(StatisticType.PlayerMoney);
			if (statisticBase != null)
			{
				statisticBase.GetValues(array, out num, out dateTime, out dateTime2);
			}
			if (stringUserData.Equals("Treasury"))
			{
				num3 = 0;
				StatisticBase statisticBase2 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.PlayerMoney);
				if (statisticBase2 != null)
				{
					statisticBase2.GetValues(array, out num2, out d, out d2);
					for (int i = 0; i < num2; i++)
					{
						array[i] *= 0.01;
					}
				}
			}
			else if (stringUserData.Equals("Budget"))
			{
				num3 = 1;
				StatisticBase statisticBase3 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.ServiceExpenses);
				if (statisticBase3 != null)
				{
					StatisticBase statisticBase4 = statisticBase3.Get(8);
					StatisticBase statisticBase5 = statisticBase3.Get(9);
					StatisticBase statisticBase6 = statisticBase3.Get(10);
					StatisticBase statisticBase7 = statisticBase3.Get(11);
					StatisticBase statisticBase8 = statisticBase3.Get(12);
					StatisticBase statisticBase9 = statisticBase3.Get(13);
					StatisticBase statisticBase10 = statisticBase3.Get(14);
					StatisticBase statisticBase11 = statisticBase3.Get(15);
					StatisticBase statisticBase12 = statisticBase3.Get(16);
					StatisticBase statisticBase13 = statisticBase3.Get(17);
					StatisticBase statisticBase14 = statisticBase3.Get(18);
					StatisticBase statisticBase15 = statisticBase3.Get(19);
					if (statisticBase4 != null && statisticBase5 != null && statisticBase6 != null && statisticBase7 != null && statisticBase8 != null && statisticBase9 != null && statisticBase10 != null && statisticBase11 != null && statisticBase12 != null && statisticBase13 != null && statisticBase14 != null && statisticBase15 != null)
					{
						double[] array2 = new double[256];
						statisticBase4.GetValues(array, out num2, out d, out d2);
						int num5;
						statisticBase5.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int j = 0; j < num2; j++)
						{
							array[j] += array2[j];
						}
						statisticBase6.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int k = 0; k < num2; k++)
						{
							array[k] += array2[k];
						}
						statisticBase7.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int l = 0; l < num2; l++)
						{
							array[l] += array2[l];
						}
						statisticBase8.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int m = 0; m < num2; m++)
						{
							array[m] += array2[m];
						}
						statisticBase9.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int n = 0; n < num2; n++)
						{
							array[n] += array2[n];
						}
						statisticBase10.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num6 = 0; num6 < num2; num6++)
						{
							array[num6] += array2[num6];
						}
						statisticBase11.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num7 = 0; num7 < num2; num7++)
						{
							array[num7] += array2[num7];
						}
						statisticBase12.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num8 = 0; num8 < num2; num8++)
						{
							array[num8] += array2[num8];
						}
						statisticBase13.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num9 = 0; num9 < num2; num9++)
						{
							array[num9] += array2[num9];
						}
						statisticBase14.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num10 = 0; num10 < num2; num10++)
						{
							array[num10] += array2[num10];
						}
						statisticBase15.GetValues(array2, out num5, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array2, ref num2, ref num5);
						for (int num11 = 0; num11 < num2; num11++)
						{
							array[num11] += array2[num11];
						}
						for (int num12 = 0; num12 < num2; num12++)
						{
							array[num12] *= 0.01;
						}
					}
				}
			}
			else if (stringUserData.Equals("Value"))
			{
				num3 = 2;
				StatisticBase statisticBase16 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.PlayerMoney);
				StatisticBase statisticBase17 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.PlayerDebt);
				StatisticBase statisticBase18 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.CityValue);
				if (statisticBase16 != null && statisticBase17 != null && statisticBase18 != null)
				{
					double[] array3 = new double[256];
					statisticBase16.GetValues(array, out num2, out d, out d2);
					int num13;
					statisticBase17.GetValues(array3, out num13, out d, out d2);
					StatisticsPanel.ScaleBuffers(array, array3, ref num2, ref num13);
					for (int num14 = 0; num14 < num2; num14++)
					{
						array[num14] -= array3[num14];
					}
					statisticBase18.GetValues(array3, out num13, out d, out d2);
					StatisticsPanel.ScaleBuffers(array, array3, ref num2, ref num13);
					for (int num15 = 0; num15 < num2; num15++)
					{
						array[num15] += array3[num15];
					}
					for (int num16 = 0; num16 < num2; num16++)
					{
						array[num16] *= 0.01;
					}
				}
			}
			else if (stringUserData.Equals("Taxation"))
			{
				num3 = 3;
				StatisticBase statisticBase19 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.ServiceIncome);
				if (statisticBase19 != null)
				{
					StatisticBase statisticBase20 = statisticBase19.Get(0);
					StatisticBase statisticBase21 = statisticBase19.Get(1);
					StatisticBase statisticBase22 = statisticBase19.Get(2);
					StatisticBase statisticBase23 = statisticBase19.Get(7);
					if (statisticBase20 != null && statisticBase21 != null && statisticBase22 != null && statisticBase23 != null)
					{
						double[] array4 = new double[256];
						statisticBase20.GetValues(array, out num2, out d, out d2);
						int num17;
						statisticBase21.GetValues(array4, out num17, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array4, ref num2, ref num17);
						for (int num18 = 0; num18 < num2; num18++)
						{
							array[num18] += array4[num18];
						}
						statisticBase22.GetValues(array4, out num17, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array4, ref num2, ref num17);
						for (int num19 = 0; num19 < num2; num19++)
						{
							array[num19] += array4[num19];
						}
						statisticBase23.GetValues(array4, out num17, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array4, ref num2, ref num17);
						for (int num20 = 0; num20 < num2; num20++)
						{
							array[num20] += array4[num20];
						}
						for (int num21 = 0; num21 < num2; num21++)
						{
							array[num21] *= 0.01;
						}
					}
				}
			}
			else if (stringUserData.Equals("Population"))
			{
				num3 = 4;
				StatisticBase statisticBase24 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.EducatedCount);
				if (statisticBase24 != null)
				{
					statisticBase24.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Moving"))
			{
				num3 = 5;
				StatisticBase statisticBase25 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.MoveRate);
				if (statisticBase25 != null)
				{
					statisticBase25.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Birth"))
			{
				num3 = 6;
				StatisticBase statisticBase26 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.BirthRate);
				if (statisticBase26 != null)
				{
					statisticBase26.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Death"))
			{
				num3 = 7;
				StatisticBase statisticBase27 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.DeathRate);
				if (statisticBase27 != null)
				{
					statisticBase27.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Employment"))
			{
				num3 = 8;
				StatisticBase statisticBase28 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.Unemployed);
				StatisticBase statisticBase29 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.EligibleWorkers);
				if (statisticBase28 != null && statisticBase29 != null)
				{
					double[] array5 = new double[256];
					statisticBase28.GetValues(array, out num2, out d, out d2);
					int num22;
					statisticBase29.GetValues(array5, out num22, out d, out d2);
					StatisticsPanel.ScaleBuffers(array, array5, ref num2, ref num22);
					for (int num23 = 0; num23 < num2; num23++)
					{
						double num24 = array5[num23];
						if (num24 != 0.0)
						{
							double num25 = (1.0 - array[num23] / array5[num23]) * 100.0;
							if (num25 < 0.0)
							{
								array[num23] = 0.0;
							}
							else if (num25 > 100.0)
							{
								array[num23] = 100.0;
							}
							else
							{
								array[num23] = num25;
							}
						}
						else
						{
							array[num23] = 0.0;
						}
					}
				}
			}
			else if (stringUserData.Equals("Jobs"))
			{
				num3 = 9;
				StatisticBase statisticBase30 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.WorkplaceCount);
				if (statisticBase30 != null)
				{
					statisticBase30.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Education"))
			{
				num3 = 10;
				StatisticBase statisticBase31 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.EducatedCount);
				if (statisticBase31 != null)
				{
					StatisticBase statisticBase32 = statisticBase31.Get(0);
					StatisticBase statisticBase33 = statisticBase31.Get(1);
					StatisticBase statisticBase34 = statisticBase31.Get(2);
					StatisticBase statisticBase35 = statisticBase31.Get(3);
					if (statisticBase32 != null && statisticBase33 != null && statisticBase34 != null && statisticBase35 != null)
					{
						double[] array6 = new double[256];
						double[] array7 = new double[256];
						double[] array8 = new double[256];
						statisticBase32.GetValues(array, out num2, out d, out d2);
						int num26;
						statisticBase33.GetValues(array6, out num26, out d, out d2);
						int num27;
						statisticBase34.GetValues(array7, out num27, out d, out d2);
						int num28;
						statisticBase35.GetValues(array8, out num28, out d, out d2);
						StatisticsPanel.ScaleBuffers(array, array6, ref num2, ref num26);
						StatisticsPanel.ScaleBuffers(array7, array8, ref num27, ref num28);
						StatisticsPanel.ScaleBuffers(array, array7, ref num2, ref num27);
						StatisticsPanel.ScaleBuffers(array6, array8, ref num26, ref num28);
						for (int num29 = 0; num29 < num2; num29++)
						{
							double num30 = array6[num29] + array7[num29] * 2.0 + array8[num29] * 3.0;
							double num31 = array[num29] + array6[num29] + array7[num29] + array8[num29];
							if (num31 != 0.0)
							{
								array[num29] = num30 / num31;
							}
							else
							{
								array[num29] = 0.0;
							}
						}
					}
				}
			}
			else if (stringUserData.Equals("Students"))
			{
				num3 = 11;
				StatisticBase statisticBase36 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.StudentCount);
				if (statisticBase36 != null)
				{
					statisticBase36.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Tourist"))
			{
				num3 = 12;
				StatisticBase statisticBase37 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.TouristVisits);
				if (statisticBase37 != null)
				{
					statisticBase37.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("TouristIncome"))
			{
				num3 = 13;
				StatisticBase statisticBase38 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.ServiceIncome).Get(6);
				if (statisticBase38 != null)
				{
					statisticBase38.GetValues(array, out num2, out d, out d2);
					for (int num32 = 0; num32 < num2; num32++)
					{
						array[num32] *= 0.01;
					}
				}
			}
			else if (stringUserData.Equals("Happiness"))
			{
				num3 = 14;
				StatisticBase statisticBase39 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.CityHappiness);
				if (statisticBase39 != null)
				{
					statisticBase39.GetValues(array, out num2, out d, out d2);
				}
				num4 = 100f;
			}
			else if (stringUserData.Equals("Residential"))
			{
				num3 = 15;
				StatisticBase statisticBase40 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.BuildingArea).Get(0);
				if (statisticBase40 != null)
				{
					statisticBase40.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Commercial"))
			{
				num3 = 16;
				StatisticBase statisticBase41 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.BuildingArea).Get(1);
				if (statisticBase41 != null)
				{
					statisticBase41.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Industry"))
			{
				num3 = 17;
				StatisticBase statisticBase42 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.BuildingArea).Get(2);
				if (statisticBase42 != null)
				{
					statisticBase42.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Office"))
			{
				num3 = 18;
				StatisticBase statisticBase43 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.BuildingArea).Get(3);
				if (statisticBase43 != null)
				{
					statisticBase43.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Prisoners"))
			{
				num3 = 19;
				StatisticBase statisticBase44 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.PrisonerAmount);
				if (statisticBase44 != null)
				{
					statisticBase44.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Cyclists"))
			{
				num3 = 20;
				StatisticBase statisticBase45 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.CyclistAmount);
				if (statisticBase45 != null)
				{
					statisticBase45.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Disasters"))
			{
				num3 = 21;
				StatisticBase statisticBase46 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.DisasterCount);
				if (statisticBase46 != null)
				{
					statisticBase46.GetValues(array, out num2, out d, out d2);
					for (int num33 = 0; num33 < num2; num33++)
					{
						array[num33] /= 256.0;
					}
				}
			}
			else if (stringUserData.Equals("Casualties"))
			{
				num3 = 22;
				StatisticBase statisticBase47 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.CasualtyCount);
				if (statisticBase47 != null)
				{
					statisticBase47.GetValues(array, out num2, out d, out d2);
				}
			}
			else if (stringUserData.Equals("Collapsed"))
			{
				num3 = 23;
				StatisticBase statisticBase48 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.CollapsedCount);
				if (statisticBase48 != null)
				{
					statisticBase48.GetValues(array, out num2, out d, out d2);
					for (int num34 = 0; num34 < num2; num34++)
					{
						array[num34] /= 256.0;
					}
				}
			}
			else if (stringUserData.Equals("DestroyedRoads"))
			{
				num3 = 24;
				StatisticBase statisticBase49 = Singleton<StatisticsManager>.get_instance().Get(StatisticType.DestroyedLength);
				if (statisticBase49 != null)
				{
					statisticBase49.GetValues(array, out num2, out d, out d2);
				}
			}
			if (num2 != 0)
			{
				Color32 color = this.StatisticsColors[num3];
				float[] array9 = new float[num];
				if (d == dateTime && d2 == dateTime2 && num2 == num)
				{
					for (int num35 = 0; num35 < num; num35++)
					{
						array9[num35] = (float)array[num35];
					}
				}
				else
				{
					float num36 = Mathf.Max(1f, (float)(dateTime2.Ticks - dateTime.Ticks));
					float num37 = Mathf.Max(1f, (float)(d2.Ticks - d.Ticks));
					float num38 = (float)(dateTime.Ticks - d.Ticks) / num37;
					float num39 = num36 / num37;
					for (int num40 = 0; num40 < num; num40++)
					{
						float num41 = ((float)num40 / (float)num * num39 + num38) * (float)num2;
						int num42 = Mathf.FloorToInt(num41);
						if (num42 < 0)
						{
							array9[num40] = 0f;
						}
						else if (num42 >= num2 - 1)
						{
							array9[num40] = (float)array[num2 - 1];
						}
						else
						{
							float num43 = Mathf.Clamp01(num41 - (float)num42);
							double num44 = array[num42];
							double num45 = array[num42 + 1];
							array9[num40] = (float)(num44 + (num45 - num44) * (double)num43);
						}
					}
				}
				this.m_Graph.set_StartTime(dateTime);
				this.m_Graph.set_EndTime(dateTime2);
				this.m_Graph.AddCurve(stringUserData, text, array9, 1f, color, num4);
			}
		}
	}

	private static void ScaleBuffers(double[] buffer1, double[] buffer2, ref int count1, ref int count2)
	{
		int num = Mathf.Min(count1, count2);
		if (num != 0)
		{
			if (num != count1)
			{
				float num2 = (float)num;
				float num3 = (float)count1;
				for (int i = 0; i < num; i++)
				{
					float num4 = (float)i * num3 / num2;
					int num5 = Mathf.FloorToInt(num4);
					float num6 = Mathf.Clamp01(num4 - (float)num5);
					if (num5 == count1 - 1)
					{
						buffer1[i] = buffer1[num5];
					}
					else
					{
						double num7 = buffer1[num5];
						double num8 = buffer1[num5 + 1];
						buffer1[i] = num7 + (num8 - num7) * (double)num6;
					}
				}
			}
			if (num != count2)
			{
				float num9 = (float)num;
				float num10 = (float)count2;
				for (int j = 0; j < num; j++)
				{
					float num11 = (float)j * num10 / num9;
					int num12 = Mathf.FloorToInt(num11);
					float num13 = Mathf.Clamp01(num11 - (float)num12);
					if (num12 == count2 - 1)
					{
						buffer2[j] = buffer2[num12];
					}
					else
					{
						double num14 = buffer2[num12];
						double num15 = buffer2[num12 + 1];
						buffer2[j] = num14 + (num15 - num14) * (double)num13;
					}
				}
			}
		}
		count1 = num;
		count2 = num;
	}

	public float CheckboxSpacing = 50f;

	public Color32[] StatisticsColors = new Color32[25];

	private string[] StatisticsNames = new string[]
	{
		"Treasury",
		"Budget",
		"Value",
		"Taxation",
		"Population",
		"Moving",
		"Birth",
		"Death",
		"Employment",
		"Jobs",
		"Education",
		"Students",
		"Tourist",
		"TouristIncome",
		"Happiness",
		"Residential",
		"Commercial",
		"Industry",
		"Office",
		"Prisoners",
		"Cyclists",
		"Disasters",
		"Casualties",
		"Collapsed",
		"DestroyedRoads"
	};

	private UIGraph m_Graph;
}
