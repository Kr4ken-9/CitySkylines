﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class OfficeBuildingAI : PrivateBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_info.m_class.m_service != ItemClass.Service.Office)
		{
			throw new PrefabException(this.m_info, "OfficeBuildingAI used with other service type (" + this.m_info.m_class.m_service + ")");
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Connections)
		{
			if (infoMode != InfoManager.InfoMode.BuildingLevel)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (this.m_info.m_class.m_subService == ItemClass.SubService.OfficeGeneric)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7] * 0.5f, 0.333f + (float)this.m_info.m_class.m_level * 0.333f);
			}
			return Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7] * 0.5f;
		}
		else
		{
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.WaterPower || !this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
			if (outgoingTransferReason != TransferManager.TransferReason.None && (data.m_tempExport != 0 || data.m_finalExport != 0))
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)outgoingTransferReason];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.PrivateIncome)
		{
			int width = data.Width;
			int length = data.Length;
			int num = width * length;
			int num2 = (100 * num + 99) / 100;
			return num2 * 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		return (100 * num + 99) / 100;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			return StringUtils.SafeFormat("{0}\n{1}: {2}", new object[]
			{
				base.GetDebugString(buildingID, ref data),
				this.GetOutgoingTransferReason().ToString(),
				data.m_customBuffer2
			});
		}
		return base.GetDebugString(buildingID, ref data);
	}

	public override string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		if ((data.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_IMPOSSIBLE");
		}
		if (this.m_info.m_class.m_subService != ItemClass.SubService.OfficeGeneric)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_SPECIAL_INDUSTRY");
		}
		if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_WORKERS_HAPPY");
		}
		if (data.m_problems != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_DISTRESS");
		}
		if (data.m_levelUpProgress == 0)
		{
			return base.GetLevelUpInfo(buildingID, ref data, out progress);
		}
		if (data.m_levelUpProgress == 1)
		{
			progress = 0.933333337f;
			return Locale.Get("LEVELUP_HIGHRISE_BAN");
		}
		int num = (int)((data.m_levelUpProgress & 15) - 1);
		int num2 = (data.m_levelUpProgress >> 4) - 1;
		if (num <= num2)
		{
			progress = (float)num * 0.06666667f;
			return Locale.Get("LEVELUP_LOWTECH");
		}
		progress = (float)num2 * 0.06666667f;
		return Locale.Get("LEVELUP_SERVICES_NEEDED");
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
		if (specialization != DistrictPolicies.Specialization.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			District[] expr_39_cp_0 = instance.m_districts.m_buffer;
			byte expr_39_cp_1 = district;
			expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect = (expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect | specialization);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainTaxes, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseTaxes, position, 1.5f);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material != TransferManager.TransferReason.None && material == this.GetOutgoingTransferReason())
		{
			int num = Mathf.Min(this.MaxOutgoingLoadSize(), (int)data.m_customBuffer2);
			ushort building = offer.Building;
			if (num > 0 && building != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
				if (info != null)
				{
					info.m_buildingAI.ModifyMaterialBuffer(building, ref instance.m_buildings.m_buffer[(int)building], material, ref num);
				}
				if (num > 0 && (instance.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					CommonBuildingAI.ExportResource(buildingID, ref data, material, num);
					data.m_tempExport = 255;
				}
				info.m_buildingAI.PathfindSuccess(building, ref instance.m_buildings.m_buffer[(int)building], BuildingAI.PathFindType.VirtualCargo);
			}
			if (num > 0)
			{
				data.m_customBuffer2 = (ushort)Mathf.Max(0, (int)data.m_customBuffer2 - num);
				data.m_outgoingProblemTimer = 0;
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material != TransferManager.TransferReason.None && material == this.GetOutgoingTransferReason())
		{
			int customBuffer = (int)data.m_customBuffer2;
			amountDelta = Mathf.Clamp(amountDelta, -customBuffer, 0);
			data.m_customBuffer2 = (ushort)(customBuffer + amountDelta);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(outgoingTransferReason, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			instance2.m_districts.m_buffer[(int)district].AddOfficeData(buildingData.Width * buildingData.Length, (buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255, this.m_info.m_class.m_subService);
		}
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0)
		{
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && this.m_info.m_class.m_level == ItemClass.Level.Level3 && instance.m_randomizer.Int32(10u) == 0 && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
			{
				District[] expr_157_cp_0 = instance2.m_districts.m_buffer;
				byte expr_157_cp_1 = district;
				expr_157_cp_0[(int)expr_157_cp_1].m_cityPlanningPoliciesEffect = (expr_157_cp_0[(int)expr_157_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				buildingData.m_flags |= Building.Flags.Demolishing;
				instance.m_currentBuildIndex += 1u;
			}
			if (instance.m_randomizer.Int32(10u) == 0)
			{
				DistrictPolicies.Specialization specializationPolicies = instance2.m_districts.m_buffer[(int)district].m_specializationPolicies;
				DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
				if (specialization != DistrictPolicies.Specialization.None)
				{
					if ((specializationPolicies & specialization) == DistrictPolicies.Specialization.None)
					{
						if (Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
						{
							buildingData.m_flags |= Building.Flags.Demolishing;
							instance.m_currentBuildIndex += 1u;
						}
					}
					else
					{
						District[] expr_213_cp_0 = instance2.m_districts.m_buffer;
						byte expr_213_cp_1 = district;
						expr_213_cp_0[(int)expr_213_cp_1].m_specializationPoliciesEffect = (expr_213_cp_0[(int)expr_213_cp_1].m_specializationPoliciesEffect | specialization);
					}
				}
				else if ((specializationPolicies & DistrictPolicies.Specialization.Hightech) != DistrictPolicies.Specialization.None && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
				{
					buildingData.m_flags |= Building.Flags.Demolishing;
					instance.m_currentBuildIndex += 1u;
				}
			}
		}
		uint num = (instance.m_currentFrameIndex & 3840u) >> 8;
		if (num == 15u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
		}
		buildingData.m_tempExport = (byte)Mathf.Max(0, (int)(buildingData.m_tempExport - 4));
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Taxation taxationPolicies = instance.m_districts.m_buffer[(int)district].m_taxationPolicies;
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		District[] expr_6A_cp_0 = instance.m_districts.m_buffer;
		byte expr_6A_cp_1 = district;
		expr_6A_cp_0[(int)expr_6A_cp_1].m_servicePoliciesEffect = (expr_6A_cp_0[(int)expr_6A_cp_1].m_servicePoliciesEffect | (servicePolicies & (DistrictPolicies.Services.PowerSaving | DistrictPolicies.Services.WaterSaving | DistrictPolicies.Services.SmokeDetectors | DistrictPolicies.Services.Recycling | DistrictPolicies.Services.ExtraInsulation | DistrictPolicies.Services.NoElectricity | DistrictPolicies.Services.OnlyElectricity)));
		District[] expr_8E_cp_0 = instance.m_districts.m_buffer;
		byte expr_8E_cp_1 = district;
		expr_8E_cp_0[(int)expr_8E_cp_1].m_cityPlanningPoliciesEffect = (expr_8E_cp_0[(int)expr_8E_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods));
		if ((taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseOffice | DistrictPolicies.Taxation.TaxLowerOffice)) != (DistrictPolicies.Taxation.TaxRaiseOffice | DistrictPolicies.Taxation.TaxLowerOffice))
		{
			District[] expr_C4_cp_0 = instance.m_districts.m_buffer;
			byte expr_C4_cp_1 = district;
			expr_C4_cp_0[(int)expr_C4_cp_1].m_taxationPoliciesEffect = (expr_C4_cp_0[(int)expr_C4_cp_1].m_taxationPoliciesEffect | (taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseOffice | DistrictPolicies.Taxation.TaxLowerOffice)));
		}
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = base.HandleWorkers(buildingID, ref buildingData, ref behaviourData, ref num, ref num2, ref num3);
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num4 = 0;
		}
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		int width = buildingData.Width;
		int length = buildingData.Length;
		int num5 = this.MaxOutgoingLoadSize();
		int num6 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
		int num7 = num6 * 500;
		int num8 = Mathf.Max(num7, num5 * 2);
		if (num4 != 0 && outgoingTransferReason != TransferManager.TransferReason.None)
		{
			int num9 = num8 - (int)buildingData.m_customBuffer2;
			num4 = Mathf.Max(0, Mathf.Min(num4, (num9 * 200 + num8 - 1) / num8));
			int num10 = (num6 * num4 + 9) / 10;
			num10 = Mathf.Max(0, Mathf.Min(num10, num9));
			buildingData.m_customBuffer2 += (ushort)num10;
			IndustrialBuildingAI.ProductType productType = IndustrialBuildingAI.GetProductType(outgoingTransferReason);
			if (productType != IndustrialBuildingAI.ProductType.None)
			{
				StatisticsManager instance2 = Singleton<StatisticsManager>.get_instance();
				StatisticBase statisticBase = instance2.Acquire<StatisticArray>(StatisticType.GoodsProduced);
				statisticBase.Acquire<StatisticInt32>((int)productType, 5).Add(num10);
			}
			num4 = (num10 + 9) / 10;
		}
		else
		{
			num4 = (num4 * num6 + 99) / 100;
		}
		int num11;
		int waterConsumption;
		int sewageAccumulation;
		int num12;
		int num13;
		this.GetConsumptionRates(new Randomizer((int)buildingID), num4, out num11, out waterConsumption, out sewageAccumulation, out num12, out num13);
		int heatingConsumption = 0;
		if (num11 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num11 * 3 + 8 >> 4);
				num13 = num13 * 95 / 100;
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num11 + 2 >> 2);
			}
		}
		if (num12 != 0 && (servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
		{
			num12 = Mathf.Max(1, num12 * 85 / 100);
			num13 = num13 * 95 / 100;
		}
		if (num4 != 0)
		{
			int num14 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num11, ref heatingConsumption, ref waterConsumption, ref sewageAccumulation, ref num12, servicePolicies);
			num4 = (num4 * num14 + 99) / 100;
			if (num4 != 0)
			{
				if (num13 != 0)
				{
					Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PrivateIncome, num13, this.m_info.m_class, taxationPolicies);
				}
				int num15;
				int num16;
				this.GetPollutionRates(num4, cityPlanningPolicies, out num15, out num16);
				if (num15 != 0 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
				{
					Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num15, num15, buildingData.m_position, 60f);
				}
				if (num16 != 0)
				{
					Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num16, buildingData.m_position, 60f);
				}
				if (num14 < 100)
				{
					buildingData.m_flags |= Building.Flags.RateReduced;
				}
				else
				{
					buildingData.m_flags &= ~Building.Flags.RateReduced;
				}
				buildingData.m_flags |= Building.Flags.Active;
			}
			else
			{
				buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
			}
		}
		else
		{
			num11 = 0;
			heatingConsumption = 0;
			waterConsumption = 0;
			sewageAccumulation = 0;
			num12 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
		}
		int num17 = 0;
		int wellbeing = 0;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (behaviourData.m_healthAccumulation != 0)
		{
			if (num != 0)
			{
				num17 = (behaviourData.m_healthAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Health, behaviourData.m_healthAccumulation, buildingData.m_position, radius);
		}
		if (behaviourData.m_wellbeingAccumulation != 0)
		{
			if (num != 0)
			{
				wellbeing = (behaviourData.m_wellbeingAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Wellbeing, behaviourData.m_wellbeingAccumulation, buildingData.m_position, radius);
		}
		int num18 = Citizen.GetHappiness(num17, wellbeing) * 15 / 100;
		if (num3 != 0)
		{
			num18 += num * 40 / num3;
		}
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) == Notification.Problem.None)
		{
			num18 += 20;
		}
		if (buildingData.m_problems == Notification.Problem.None)
		{
			num18 += 25;
		}
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(this.m_info.m_class, taxationPolicies);
		int num19 = (int)((ItemClass.Level)9 - this.m_info.m_class.m_level);
		int num20 = (int)((ItemClass.Level)11 - this.m_info.m_class.m_level);
		if (taxRate < num19)
		{
			num18 += num19 - taxRate;
		}
		if (taxRate > num20)
		{
			num18 -= taxRate - num20;
		}
		if (taxRate >= num20 + 4)
		{
			if (buildingData.m_taxProblemTimer != 0 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
			{
				int num21 = taxRate - num20 >> 2;
				buildingData.m_taxProblemTimer = (byte)Mathf.Min(255, (int)buildingData.m_taxProblemTimer + num21);
				if (buildingData.m_taxProblemTimer >= 96)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_taxProblemTimer >= 32)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
				}
			}
		}
		else
		{
			buildingData.m_taxProblemTimer = (byte)Mathf.Max(0, (int)(buildingData.m_taxProblemTimer - 1));
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
		}
		num18 = Mathf.Clamp(num18, 0, 100);
		buildingData.m_health = (byte)num17;
		buildingData.m_happiness = (byte)num18;
		buildingData.m_citizenCount = (byte)num;
		base.HandleDead(buildingID, ref buildingData, ref behaviourData, num2);
		int num22 = behaviourData.m_crimeAccumulation / 10;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num22 = num22 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num22, num);
		int num23 = (int)buildingData.m_crimeBuffer;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num, buildingData.m_position, radius);
			int num24 = behaviourData.m_educated0Count * 100 + behaviourData.m_educated1Count * 50 + behaviourData.m_educated2Count * 30;
			num24 = num24 / num + 50;
			buildingData.m_fireHazard = (byte)num24;
			num23 = (num23 + (num >> 1)) / num;
		}
		else
		{
			buildingData.m_fireHazard = 0;
			num23 = 0;
		}
		SimulationManager instance3 = Singleton<SimulationManager>.get_instance();
		uint num25 = (instance3.m_currentFrameIndex & 3840u) >> 8;
		if ((ulong)num25 == (ulong)((long)(buildingID & 15)) && this.m_info.m_class.m_subService == ItemClass.SubService.OfficeGeneric && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance3.m_currentBuildIndex && (buildingData.m_flags & Building.Flags.Upgrading) == Building.Flags.None)
		{
			this.CheckBuildingLevel(buildingID, ref buildingData, ref frameData, ref behaviourData, num);
		}
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoPlaceforGoods);
			if (outgoingTransferReason != TransferManager.TransferReason.None)
			{
				if ((int)buildingData.m_customBuffer2 > num8 - (num7 >> 1))
				{
					buildingData.m_outgoingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_outgoingProblemTimer + 1));
					if (buildingData.m_outgoingProblemTimer >= 192)
					{
						problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods | Notification.Problem.MajorProblem);
					}
					else if (buildingData.m_outgoingProblemTimer >= 128)
					{
						problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods);
					}
				}
				else
				{
					buildingData.m_outgoingProblemTimer = 0;
				}
			}
			buildingData.m_problems = problem;
			instance.m_districts.m_buffer[(int)district].AddOfficeData(ref behaviourData, num17, num18, num23, num3, num, Mathf.Max(0, num3 - num2), (int)this.m_info.m_class.m_level, num11, heatingConsumption, waterConsumption, sewageAccumulation, num12, num13, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255), this.m_info.m_class.m_subService);
			if (buildingData.m_fireIntensity == 0 && outgoingTransferReason != TransferManager.TransferReason.None)
			{
				int customBuffer = (int)buildingData.m_customBuffer2;
				if (customBuffer >= num5)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = customBuffer * 8 / num5;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = customBuffer / num5;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(outgoingTransferReason, offer);
				}
			}
			base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
			base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		}
	}

	private void CheckBuildingLevel(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, ref Citizen.BehaviourData behaviour, int workerCount)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		int num = behaviour.m_educated1Count + behaviour.m_educated2Count * 2 + behaviour.m_educated3Count * 3;
		int averageEducation;
		ItemClass.Level level;
		int num2;
		if (workerCount != 0)
		{
			averageEducation = (num * 100 + (workerCount >> 1)) / workerCount;
			num = (num * 12 + (workerCount >> 1)) / workerCount;
			if (num < 15)
			{
				level = ItemClass.Level.Level1;
				num2 = 1 + num;
			}
			else if (num < 30)
			{
				level = ItemClass.Level.Level2;
				num2 = 1 + (num - 15);
			}
			else
			{
				level = ItemClass.Level.Level3;
				num2 = 1;
			}
			if (level < this.m_info.m_class.m_level)
			{
				num2 = 1;
			}
			else if (level > this.m_info.m_class.m_level)
			{
				num2 = 15;
			}
		}
		else
		{
			level = ItemClass.Level.Level1;
			averageEducation = 0;
			num2 = 0;
		}
		int num3 = this.CalculateServiceValue(buildingID, ref buildingData);
		ItemClass.Level level2;
		int num4;
		if (num2 != 0)
		{
			if (num3 < 45)
			{
				level2 = ItemClass.Level.Level1;
				num4 = 1 + (num3 * 15 + 22) / 45;
			}
			else if (num3 < 90)
			{
				level2 = ItemClass.Level.Level2;
				num4 = 1 + ((num3 - 45) * 15 + 22) / 45;
			}
			else
			{
				level2 = ItemClass.Level.Level3;
				num4 = 1;
			}
			if (level2 < this.m_info.m_class.m_level)
			{
				num4 = 1;
			}
			else if (level2 > this.m_info.m_class.m_level)
			{
				num4 = 15;
			}
		}
		else
		{
			level2 = ItemClass.Level.Level1;
			num4 = 0;
		}
		bool flag = false;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			if (num3 < 30)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level3 && num3 < 60)
		{
			flag = true;
		}
		ItemClass.Level level3 = (ItemClass.Level)Mathf.Min((int)level, (int)level2);
		Singleton<BuildingManager>.get_instance().m_LevelUpWrapper.OnCalculateOfficeLevelUp(ref level3, ref num2, ref num4, ref flag, averageEducation, num3, buildingID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		if (flag)
		{
			buildingData.m_serviceProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_serviceProblemTimer + 1));
			if (buildingData.m_serviceProblemTimer >= 8)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TooFewServices | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_serviceProblemTimer >= 4)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
			}
			else
			{
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
			}
		}
		else
		{
			buildingData.m_serviceProblemTimer = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
		}
		if (level3 > this.m_info.m_class.m_level)
		{
			num2 = 0;
			num4 = 0;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && level3 == ItemClass.Level.Level3)
			{
				District[] expr_312_cp_0 = instance.m_districts.m_buffer;
				byte expr_312_cp_1 = district;
				expr_312_cp_0[(int)expr_312_cp_1].m_cityPlanningPoliciesEffect = (expr_312_cp_0[(int)expr_312_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				level3 = ItemClass.Level.Level2;
				num2 = 1;
			}
			if (buildingData.m_problems == Notification.Problem.None && level3 > this.m_info.m_class.m_level && this.GetUpgradeInfo(buildingID, ref buildingData) != null && !Singleton<DisasterManager>.get_instance().IsEvacuating(buildingData.m_position))
			{
				frameData.m_constructState = 0;
				base.StartUpgrading(buildingID, ref buildingData);
			}
		}
		buildingData.m_levelUpProgress = (byte)(num2 | num4 << 4);
	}

	private int CalculateServiceValue(ushort buildingID, ref Building data)
	{
		ushort[] array;
		int num;
		Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResources(data.m_position, out array, out num);
		int resourceRate = (int)array[num + 7];
		int resourceRate2 = (int)array[num + 2];
		int resourceRate3 = (int)array[num];
		int resourceRate4 = (int)array[num + 6];
		int resourceRate5 = (int)array[num + 1];
		int resourceRate6 = (int)array[num + 13];
		int resourceRate7 = (int)array[num + 3];
		int resourceRate8 = (int)array[num + 4];
		int resourceRate9 = (int)array[num + 5];
		int resourceRate10 = (int)array[num + 8];
		int resourceRate11 = (int)array[num + 18];
		int resourceRate12 = (int)array[num + 20];
		int resourceRate13 = (int)array[num + 21];
		int resourceRate14 = (int)array[num + 23];
		int num2 = 0;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate, 100, 500, 50, 100) / 3;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate2, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate3, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate4, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate5, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate6, 100, 500, 50, 100) / 6;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate7, 100, 500, 50, 100) / 7;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate8, 100, 500, 50, 100) / 7;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate9, 100, 500, 50, 100) / 7;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate12, 50, 100, 80, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate13, 100, 1000, 0, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate14, 50, 100, 80, 100) / 5;
		num2 -= ImmaterialResourceManager.CalculateResourceEffect(resourceRate10, 100, 500, 50, 100) / 4;
		num2 -= ImmaterialResourceManager.CalculateResourceEffect(resourceRate11, 100, 500, 50, 100) / 3;
		byte resourceRate15;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out resourceRate15);
		return num2 - ImmaterialResourceManager.CalculateResourceEffect((int)resourceRate15, 50, 255, 50, 100) / 4;
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 127;
		fireTolerance = 10;
		return true;
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
		case ImmaterialResourceManager.Resource.FireDepartment:
		case ImmaterialResourceManager.Resource.PoliceDepartment:
		case ImmaterialResourceManager.Resource.DeathCare:
		{
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num);
			int num2 = ImmaterialResourceManager.CalculateResourceEffect(num, 100, 500, 50, 100);
			int num3 = ImmaterialResourceManager.CalculateResourceEffect(num + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num3 - num2) / 250f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.EducationElementary:
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		case ImmaterialResourceManager.Resource.EducationUniversity:
		{
			int num4;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num4);
			int num5 = ImmaterialResourceManager.CalculateResourceEffect(num4, 100, 500, 50, 100);
			int num6 = ImmaterialResourceManager.CalculateResourceEffect(num4 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num6 - num5) / 350f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PublicTransport:
		{
			int num7;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num7);
			int num8 = ImmaterialResourceManager.CalculateResourceEffect(num7, 100, 500, 50, 100);
			int num9 = ImmaterialResourceManager.CalculateResourceEffect(num7 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num9 - num8) / 150f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.NoisePollution:
		{
			int num10;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num10);
			int num11 = ImmaterialResourceManager.CalculateResourceEffect(num10, 10, 100, 0, 100);
			int num12 = ImmaterialResourceManager.CalculateResourceEffect(num10 + Mathf.RoundToInt(amount), 10, 100, 0, 100);
			return Mathf.Clamp((float)(num12 - num11) / 200f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Entertainment:
		{
			int num13;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num13);
			int num14 = ImmaterialResourceManager.CalculateResourceEffect(num13, 100, 500, 50, 100);
			int num15 = ImmaterialResourceManager.CalculateResourceEffect(num13 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num15 - num14) / 300f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Abandonment:
		{
			int num16;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num16);
			int num17 = ImmaterialResourceManager.CalculateResourceEffect(num16, 15, 50, 10, 20);
			int num18 = ImmaterialResourceManager.CalculateResourceEffect(num16 + Mathf.RoundToInt(amount), 15, 50, 10, 20);
			return Mathf.Clamp((float)(num18 - num17) / 150f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.RadioCoverage:
		case ImmaterialResourceManager.Resource.DisasterCoverage:
		{
			int num19;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num19);
			int num20 = ImmaterialResourceManager.CalculateResourceEffect(num19, 50, 100, 20, 25);
			int num21 = ImmaterialResourceManager.CalculateResourceEffect(num19 + Mathf.RoundToInt(amount), 50, 100, 20, 25);
			return Mathf.Clamp((float)(num21 - num20) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FirewatchCoverage:
		{
			int num22;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num22);
			int num23 = ImmaterialResourceManager.CalculateResourceEffect(num22, 100, 1000, 0, 25);
			int num24 = ImmaterialResourceManager.CalculateResourceEffect(num22 + Mathf.RoundToInt(amount), 100, 1000, 0, 25);
			return Mathf.Clamp((float)(num24 - num23) / 100f, -1f, 1f);
		}
		}
		return base.GetEventImpact(buildingID, ref data, resource, amount);
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return base.GetEventImpact(buildingID, ref data, resource, amount);
		}
		byte b;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out b);
		int num = ImmaterialResourceManager.CalculateResourceEffect((int)b, 50, 255, 50, 100);
		int num2 = ImmaterialResourceManager.CalculateResourceEffect((int)b + Mathf.RoundToInt(amount), 50, 255, 50, 100);
		return Mathf.Clamp((float)(num2 - num) / 200f, -1f, 1f);
	}

	public override int CalculateHomeCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		level0 = 0;
		level1 = 0;
		level2 = 0;
		level3 = 0;
		if (@class.m_subService == ItemClass.SubService.OfficeGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 50;
				level0 = 0;
				level1 = 40;
				level2 = 50;
				level3 = 10;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 110;
				level0 = 0;
				level1 = 20;
				level2 = 50;
				level3 = 30;
			}
			else
			{
				num = 170;
				level0 = 0;
				level1 = 0;
				level2 = 40;
				level3 = 60;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.OfficeHightech)
		{
			num = 55;
			level0 = 0;
			level1 = 10;
			level2 = 40;
			level3 = 50;
		}
		if (num != 0)
		{
			num = Mathf.Max(200, width * length * num + r.Int32(100u)) / 100;
			int num2 = level0 + level1 + level2 + level3;
			if (num2 != 0)
			{
				level0 = (num * level0 + r.Int32((uint)num2)) / num2;
				num -= level0;
			}
			num2 = level1 + level2 + level3;
			if (num2 != 0)
			{
				level1 = (num * level1 + r.Int32((uint)num2)) / num2;
				num -= level1;
			}
			num2 = level2 + level3;
			if (num2 != 0)
			{
				level2 = (num * level2 + r.Int32((uint)num2)) / num2;
				num -= level2;
			}
			level3 = num;
		}
	}

	public override int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		if (@class.m_subService == ItemClass.SubService.OfficeGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 50;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 70;
			}
			else
			{
				num = 80;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.OfficeHightech)
		{
			num = 100;
		}
		if (num != 0)
		{
			num = Mathf.Max(100, width * length * num + r.Int32(100u)) / 100;
		}
		return num;
	}

	public override void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		ItemClass @class = this.m_info.m_class;
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
		if (@class.m_subService == ItemClass.SubService.OfficeGeneric)
		{
			ItemClass.Level level = @class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						electricityConsumption = 120;
						waterConsumption = 110;
						sewageAccumulation = 110;
						garbageAccumulation = 25;
						incomeAccumulation = 300;
					}
				}
				else
				{
					electricityConsumption = 100;
					waterConsumption = 100;
					sewageAccumulation = 100;
					garbageAccumulation = 50;
					incomeAccumulation = 200;
				}
			}
			else
			{
				electricityConsumption = 80;
				waterConsumption = 90;
				sewageAccumulation = 90;
				garbageAccumulation = 100;
				incomeAccumulation = 140;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.OfficeHightech)
		{
			electricityConsumption = 130;
			waterConsumption = 100;
			sewageAccumulation = 100;
			garbageAccumulation = 50;
			incomeAccumulation = 260;
		}
		if (electricityConsumption != 0)
		{
			electricityConsumption = Mathf.Max(100, productionRate * electricityConsumption + r.Int32(100u)) / 100;
		}
		if (waterConsumption != 0)
		{
			int num = r.Int32(100u);
			waterConsumption = Mathf.Max(100, productionRate * waterConsumption + num) / 100;
			if (sewageAccumulation != 0)
			{
				sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + num) / 100;
			}
		}
		else if (sewageAccumulation != 0)
		{
			sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + r.Int32(100u)) / 100;
		}
		if (garbageAccumulation != 0)
		{
			garbageAccumulation = Mathf.Max(100, productionRate * garbageAccumulation + r.Int32(100u)) / 100;
		}
		if (incomeAccumulation != 0)
		{
			incomeAccumulation = productionRate * incomeAccumulation;
		}
	}

	public override void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		groundPollution = 0;
		noisePollution = 0;
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.CountUnchecked("BUILDING_NAME", text);
		if (num != 0u)
		{
			return Locale.Get("BUILDING_NAME", text, randomizer.Int32(num));
		}
		text = this.m_info.m_class.m_level.ToString();
		num = Locale.Count("OFFICE_NAME", text);
		return Locale.Get("OFFICE_NAME", text, randomizer.Int32(num));
	}

	private int MaxOutgoingLoadSize()
	{
		return 2000;
	}

	private TransferManager.TransferReason GetOutgoingTransferReason()
	{
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService != ItemClass.SubService.OfficeHightech)
		{
			return TransferManager.TransferReason.None;
		}
		return TransferManager.TransferReason.Goods;
	}

	private DistrictPolicies.Specialization SpecialPolicyNeeded()
	{
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService != ItemClass.SubService.OfficeHightech)
		{
			return DistrictPolicies.Specialization.None;
		}
		return DistrictPolicies.Specialization.Hightech;
	}
}
