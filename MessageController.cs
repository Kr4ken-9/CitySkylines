﻿using System;
using ColossalFramework;
using UnityEngine;

public class MessageController : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("MessageController");
		Singleton<MessageManager>.get_instance().InitializeProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("MessageController");
		Singleton<MessageManager>.get_instance().DestroyProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	public float m_messageNextTimeout = 30f;
}
