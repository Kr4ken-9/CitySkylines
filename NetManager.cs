﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public class NetManager : SimulationManagerBase<NetManager, NetProperties>, ISimulationManager, IRenderableManager, ITerrainManager
{
	public bool RenderDirectionArrows
	{
		get
		{
			return this.m_renderDirectionArrows;
		}
		set
		{
			this.m_renderDirectionArrows = value;
			this.UpdateDirectionArrowVisibility();
		}
	}

	public bool RenderDirectionArrowsInfo
	{
		get
		{
			return this.m_renderDirectionArrowsInfo;
		}
		set
		{
			this.m_renderDirectionArrowsInfo = value;
			this.UpdateDirectionArrowVisibility();
		}
	}

	public bool RoadNamesVisible
	{
		get
		{
			return this.m_roadNamesVisible;
		}
		set
		{
			this.m_roadNamesVisible = value;
		}
	}

	private void UpdateDirectionArrowVisibility()
	{
		if (this.m_mainCamera != null)
		{
			if (this.m_renderDirectionArrows || this.m_renderDirectionArrowsInfo)
			{
				Camera expr_2D = this.m_mainCamera;
				expr_2D.set_cullingMask(expr_2D.get_cullingMask() | 1 << this.m_arrowLayer);
			}
			else
			{
				Camera expr_4F = this.m_mainCamera;
				expr_4F.set_cullingMask(expr_4F.get_cullingMask() & ~(1 << this.m_arrowLayer));
			}
		}
	}

	public PathVisualizer PathVisualizer
	{
		get
		{
			return this.m_pathVisualizer;
		}
	}

	public NetAdjust NetAdjust
	{
		get
		{
			return this.m_netAdjust;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_nodes = new Array16<NetNode>(32768u);
		this.m_segments = new Array16<NetSegment>(36864u);
		this.m_lanes = new Array32<NetLane>(262144u);
		this.m_updatedNodes = new ulong[512];
		this.m_updatedSegments = new ulong[576];
		this.m_adjustedSegments = new ulong[576];
		this.m_nodeGrid = new ushort[72900];
		this.m_segmentGrid = new ushort[72900];
		this.m_angleBuffer = new float[16];
		this.m_tempNodeBuffer = new FastList<ushort>();
		this.m_tempSegmentBuffer = new FastList<ushort>();
		this.m_nameInstanceBuffer = new FastList<uint>();
		this.m_updateNameVisibility = new HashSet<ushort>();
		this.m_serviceSegments = new FastList<ushort>[22];
		for (int i = 0; i < this.m_serviceSegments.Length; i++)
		{
			this.m_serviceSegments[i] = new FastList<ushort>();
		}
		this.m_tileNodesCount = new int[550];
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_LeftMatrix = Shader.PropertyToID("_LeftMatrix");
		this.ID_RightMatrix = Shader.PropertyToID("_RightMatrix");
		this.ID_LeftMatrixB = Shader.PropertyToID("_LeftMatrixB");
		this.ID_RightMatrixB = Shader.PropertyToID("_RightMatrixB");
		this.ID_MeshScale = Shader.PropertyToID("_MeshScale");
		this.ID_CenterPos = Shader.PropertyToID("_CenterPos");
		this.ID_SideScale = Shader.PropertyToID("_SideScale");
		this.ID_ObjectIndex = Shader.PropertyToID("_ObjectIndex");
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_SurfaceTexA = Shader.PropertyToID("_SurfaceTexA");
		this.ID_SurfaceTexB = Shader.PropertyToID("_SurfaceTexB");
		this.ID_SurfaceMapping = Shader.PropertyToID("_SurfaceMapping");
		this.ID_MainTex = Shader.PropertyToID("_MainTex");
		this.ID_XYSMap = Shader.PropertyToID("_XYSMap");
		this.ID_APRMap = Shader.PropertyToID("_APRMap");
		this.ID_LeftMatrices = Shader.PropertyToID("_LeftMatrices");
		this.ID_RightMatrices = Shader.PropertyToID("_RightMatrices");
		this.ID_LeftMatricesB = Shader.PropertyToID("_LeftMatricesB");
		this.ID_RightMatricesB = Shader.PropertyToID("_RightMatricesB");
		this.ID_MeshScales = Shader.PropertyToID("_MeshScales");
		this.ID_CenterPositions = Shader.PropertyToID("_CenterPositions");
		this.ID_SideScales = Shader.PropertyToID("_SideScales");
		this.ID_MeshLocations = Shader.PropertyToID("_MeshLocations");
		this.ID_ObjectIndices = Shader.PropertyToID("_ObjectIndices");
		this.m_roadLayer = LayerMask.NameToLayer("Road");
		this.m_arrowLayer = LayerMask.NameToLayer("DirectionArrows");
		this.m_colorUpdateLock = new object();
		this.m_colorUpdateMinSegment = 0;
		this.m_colorUpdateMaxSegment = 0;
		this.m_colorUpdateMinNode = 0;
		this.m_colorUpdateMaxNode = 0;
		this.m_smoothColors = new HashSet<InstanceID>();
		this.m_removeSmoothColors = new FastList<InstanceID>();
		this.m_newSmoothColors = new FastList<InstanceID>();
		ushort num;
		this.m_nodes.CreateItem(out num);
		this.m_segments.CreateItem(out num);
		uint num2;
		this.m_lanes.CreateItem(out num2);
		SavedBool savedBool = new SavedBool(Settings.roadNamesVisible, Settings.gameSettingsFile, DefaultSettings.roadNamesVisible);
		this.m_pathVisualizer = base.get_gameObject().AddComponent<PathVisualizer>();
		this.m_netAdjust = base.get_gameObject().AddComponent<NetAdjust>();
		this.m_roadNamesVisible = true;
		this.m_roadNamesVisibleSetting = savedBool.get_value();
	}

	private void OnDestroy()
	{
		if (this.m_lodRgbAtlas != null)
		{
			Object.Destroy(this.m_lodRgbAtlas);
			this.m_lodRgbAtlas = null;
		}
		if (this.m_lodXysAtlas != null)
		{
			Object.Destroy(this.m_lodXysAtlas);
			this.m_lodXysAtlas = null;
		}
		if (this.m_lodAprAtlas != null)
		{
			Object.Destroy(this.m_lodAprAtlas);
			this.m_lodAprAtlas = null;
		}
		if (this.m_nameMaterial != null)
		{
			Object.Destroy(this.m_nameMaterial);
			this.m_nameMaterial = null;
		}
	}

	public override void InitializeProperties(NetProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_properties.m_placementEffect != null)
		{
			this.m_properties.m_placementEffect.InitializeEffect();
		}
		if (this.m_properties.m_bulldozeEffect != null)
		{
			this.m_properties.m_bulldozeEffect.InitializeEffect();
		}
		if (this.m_properties.m_nameShader != null)
		{
			this.m_nameMaterial = new Material(this.m_properties.m_nameShader);
			this.m_nameMaterial.CopyPropertiesFromMaterial(this.m_properties.m_nameFont.get_material());
		}
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_mainCamera = gameObject.GetComponent<Camera>();
			this.UpdateDirectionArrowVisibility();
		}
		gameObject = GameObject.FindGameObjectWithTag("UndergroundView");
		if (gameObject != null)
		{
			this.m_undergroundCamera = gameObject.GetComponent<Camera>();
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (this.m_netAdjust != null)
		{
			this.m_netAdjust.Initialize(properties.m_adjustMaterial, properties.m_adjustMaterialSurface);
		}
	}

	public override void DestroyProperties(NetProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_properties.m_placementEffect != null)
			{
				this.m_properties.m_placementEffect.ReleaseEffect();
			}
			if (this.m_properties.m_bulldozeEffect != null)
			{
				this.m_properties.m_bulldozeEffect.ReleaseEffect();
			}
			if (this.m_nameMaterial != null)
			{
				Object.Destroy(this.m_nameMaterial);
				this.m_nameMaterial = null;
			}
			this.m_mainCamera = null;
			this.m_undergroundCamera = null;
			LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
			if (this.m_pathVisualizer != null)
			{
				this.m_pathVisualizer.DestroyPaths();
			}
			if (this.m_netAdjust != null)
			{
				this.m_netAdjust.DestroyPath();
			}
		}
		base.DestroyProperties(properties);
	}

	private void Update()
	{
		if (this.m_roadNamesVisible)
		{
			this.m_roadNameAlpha = Mathf.Min(1f, this.m_roadNameAlpha + Time.get_deltaTime() * 2f);
		}
		else
		{
			this.m_roadNameAlpha = Mathf.Max(0f, this.m_roadNameAlpha - Time.get_deltaTime() * 2f);
		}
	}

	private void OnLocaleChanged()
	{
		RenderManager instance = Singleton<RenderManager>.get_instance();
		for (int i = 0; i < 36864; i++)
		{
			if ((this.m_segments.m_buffer[i].m_flags & NetSegment.Flags.Created) != NetSegment.Flags.None)
			{
				uint holder = (uint)(49152 + i);
				uint num;
				if (Singleton<RenderManager>.get_instance().GetInstanceIndex(holder, out num))
				{
					if ((this.m_segments.m_buffer[i].m_flags & NetSegment.Flags.NameVisible2) != NetSegment.Flags.None)
					{
						string segmentName = this.GetSegmentName((ushort)i);
						UIFont nameFont = this.m_properties.m_nameFont;
						instance.m_instances[(int)((UIntPtr)num)].m_nameData = Singleton<InstanceManager>.get_instance().GetNameData(segmentName, nameFont, true);
					}
					else
					{
						instance.m_instances[(int)((UIntPtr)num)].m_nameData = null;
					}
				}
			}
		}
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		Material material = Singleton<RenderManager>.get_instance().m_groupLayerMaterials[this.m_roadLayer];
		if (material != null)
		{
			if (material.HasProperty(this.ID_MainTex))
			{
				material.SetTexture(this.ID_MainTex, this.m_lodRgbAtlas);
			}
			if (material.HasProperty(this.ID_XYSMap))
			{
				material.SetTexture(this.ID_XYSMap, this.m_lodXysAtlas);
			}
			if (material.HasProperty(this.ID_APRMap))
			{
				material.SetTexture(this.ID_APRMap, this.m_lodAprAtlas);
			}
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		FastList<RenderGroup> renderedGroups = Singleton<RenderManager>.get_instance().m_renderedGroups;
		this.m_nameInstanceBuffer.Clear();
		this.m_visibleRoadNameSegment = 0;
		this.m_visibleTrafficLightNode = 0;
		for (int i = 0; i < renderedGroups.m_size; i++)
		{
			RenderGroup renderGroup = renderedGroups.m_buffer[i];
			if (renderGroup.m_instanceMask != 0)
			{
				int num = renderGroup.m_x * 270 / 45;
				int num2 = renderGroup.m_z * 270 / 45;
				int num3 = (renderGroup.m_x + 1) * 270 / 45 - 1;
				int num4 = (renderGroup.m_z + 1) * 270 / 45 - 1;
				for (int j = num2; j <= num4; j++)
				{
					for (int k = num; k <= num3; k++)
					{
						int num5 = j * 270 + k;
						ushort num6 = this.m_nodeGrid[num5];
						int num7 = 0;
						while (num6 != 0)
						{
							this.m_nodes.m_buffer[(int)num6].RenderInstance(cameraInfo, num6, renderGroup.m_instanceMask);
							num6 = this.m_nodes.m_buffer[(int)num6].m_nextGridNode;
							if (++num7 >= 32768)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				for (int l = num2; l <= num4; l++)
				{
					for (int m = num; m <= num3; m++)
					{
						int num8 = l * 270 + m;
						ushort num9 = this.m_segmentGrid[num8];
						int num10 = 0;
						while (num9 != 0)
						{
							this.m_segments.m_buffer[(int)num9].RenderInstance(cameraInfo, num9, renderGroup.m_instanceMask);
							num9 = this.m_segments.m_buffer[(int)num9].m_nextGridSegment;
							if (++num10 >= 36864)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
		}
		this.m_lastVisibleRoadNameSegment = this.m_visibleRoadNameSegment;
		this.m_lastVisibleTrafficLightNode = this.m_visibleTrafficLightNode;
		int num11 = PrefabCollection<NetInfo>.PrefabCount();
		for (int n = 0; n < num11; n++)
		{
			NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)n);
			if (prefab != null)
			{
				if (prefab.m_segments != null)
				{
					for (int num12 = 0; num12 < prefab.m_segments.Length; num12++)
					{
						NetInfo.Segment segment = prefab.m_segments[num12];
						NetInfo.LodValue combinedLod = segment.m_combinedLod;
						if (combinedLod != null && combinedLod.m_lodCount != 0)
						{
							NetSegment.RenderLod(cameraInfo, combinedLod);
						}
					}
				}
				if (prefab.m_nodes != null)
				{
					for (int num13 = 0; num13 < prefab.m_nodes.Length; num13++)
					{
						NetInfo.Node node = prefab.m_nodes[num13];
						NetInfo.LodValue combinedLod2 = node.m_combinedLod;
						if (combinedLod2 != null && combinedLod2.m_lodCount != 0)
						{
							if (node.m_directConnect)
							{
								NetSegment.RenderLod(cameraInfo, combinedLod2);
							}
							else
							{
								NetNode.RenderLod(cameraInfo, combinedLod2);
							}
						}
					}
				}
			}
		}
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (cameraInfo.m_camera != null)
		{
			this.m_pathVisualizer.RenderPaths(cameraInfo, cameraInfo.m_camera.get_cullingMask());
			this.m_netAdjust.RenderPath(cameraInfo, cameraInfo.m_camera.get_cullingMask());
		}
	}

	protected override void EndOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & (ItemClass.Availability.Game | ItemClass.Availability.MapEditor | ItemClass.Availability.ScenarioEditor)) != ItemClass.Availability.None && this.m_roadNameAlpha >= 0.001f && !this.m_renderDirectionArrows && !this.m_renderDirectionArrowsInfo && this.m_roadNamesVisibleSetting)
		{
			for (int i = 0; i < this.m_nameInstanceBuffer.m_size; i++)
			{
				this.RenderName(cameraInfo, ref Singleton<RenderManager>.get_instance().m_instances[(int)((UIntPtr)this.m_nameInstanceBuffer.m_buffer[i])]);
			}
		}
		this.m_nameInstanceBuffer.Clear();
	}

	private void RenderName(RenderManager.CameraInfo cameraInfo, ref RenderManager.Instance instanceData)
	{
		InstanceManager.NameData nameData = instanceData.m_nameData;
		if (nameData == null)
		{
			return;
		}
		if (nameData.m_dirty)
		{
			InstanceManager.RefreshNameData(nameData);
		}
		if (nameData.m_mesh == null || this.m_nameMaterial == null)
		{
			return;
		}
		Matrix4x4 dataMatrix = instanceData.m_dataMatrix2;
		Vector4 column = dataMatrix.GetColumn(0);
		Vector4 column2 = dataMatrix.GetColumn(3);
		Vector3 vector = instanceData.m_position - cameraInfo.m_position;
		Vector3 vector2 = Vector3.Cross(cameraInfo.m_up, vector);
		if ((column2.x - column.x) * vector2.x + (column2.z - column.z) * vector2.z < 0f)
		{
			Vector4 column3 = dataMatrix.GetColumn(1);
			Vector4 column4 = dataMatrix.GetColumn(2);
			dataMatrix.SetColumn(0, column2);
			dataMatrix.SetColumn(1, column4);
			dataMatrix.SetColumn(2, column3);
			dataMatrix.SetColumn(3, column);
		}
		Color nameColor = this.m_properties.m_nameColor;
		nameColor.a *= this.m_roadNameAlpha;
		this.m_nameMaterial.set_color(nameColor);
		this.m_nameMaterial.SetMatrix(this.ID_LeftMatrix, dataMatrix);
		if (this.m_nameMaterial.SetPass(0))
		{
			this.m_drawCallData.m_overlayCalls = this.m_drawCallData.m_overlayCalls + 1;
			Graphics.DrawMeshNow(nameData.m_mesh, instanceData.m_position, Quaternion.get_identity());
		}
	}

	protected override void UndergroundOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_undergroundCamera != null)
		{
			this.m_pathVisualizer.RenderPaths(cameraInfo, this.m_undergroundCamera.get_cullingMask());
			this.m_netAdjust.RenderPath(cameraInfo, this.m_undergroundCamera.get_cullingMask());
		}
	}

	public override void CheckReferences()
	{
		base.CheckReferences();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.CheckReferencesImpl());
	}

	[DebuggerHidden]
	private IEnumerator CheckReferencesImpl()
	{
		return new NetManager.<CheckReferencesImpl>c__Iterator0();
	}

	public override void InitRenderData()
	{
		base.InitRenderData();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitRenderDataImpl());
	}

	[DebuggerHidden]
	private IEnumerator InitRenderDataImpl()
	{
		NetManager.<InitRenderDataImpl>c__Iterator1 <InitRenderDataImpl>c__Iterator = new NetManager.<InitRenderDataImpl>c__Iterator1();
		<InitRenderDataImpl>c__Iterator.$this = this;
		return <InitRenderDataImpl>c__Iterator;
	}

	public Coroutine RebuildLods()
	{
		NetInfo.ClearLodValues();
		return base.StartCoroutine(this.InitRenderDataImpl());
	}

	public bool UpdateColorMap(Texture2D colorMap)
	{
		if (this.m_newSmoothColors.m_size != 0)
		{
			while (!Monitor.TryEnter(this.m_newSmoothColors, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				for (int i = 0; i < this.m_newSmoothColors.m_size; i++)
				{
					this.m_smoothColors.Add(this.m_newSmoothColors.m_buffer[i]);
				}
				this.m_newSmoothColors.m_size = 0;
			}
			finally
			{
				Monitor.Exit(this.m_newSmoothColors);
			}
		}
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		if (currentMode != InfoManager.InfoMode.None)
		{
			this.m_smoothColors.Clear();
		}
		if (this.m_colorUpdateMaxSegment < this.m_colorUpdateMinSegment && this.m_colorUpdateMaxNode < this.m_colorUpdateMinNode && this.m_smoothColors.Count == 0)
		{
			return false;
		}
		if (Singleton<CoverageManager>.get_instance().WaitingForIt())
		{
			return false;
		}
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		int colorUpdateMinSegment;
		int colorUpdateMaxSegment;
		int colorUpdateMinNode;
		int colorUpdateMaxNode;
		try
		{
			colorUpdateMinSegment = this.m_colorUpdateMinSegment;
			colorUpdateMaxSegment = this.m_colorUpdateMaxSegment;
			colorUpdateMinNode = this.m_colorUpdateMinNode;
			colorUpdateMaxNode = this.m_colorUpdateMaxNode;
			this.m_colorUpdateMinSegment = 36864;
			this.m_colorUpdateMaxSegment = -1;
			this.m_colorUpdateMinNode = 32768;
			this.m_colorUpdateMaxNode = -1;
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
		for (int j = colorUpdateMinSegment; j <= colorUpdateMaxSegment; j++)
		{
			if (this.m_segments.m_buffer[j].m_flags != NetSegment.Flags.None)
			{
				InstanceID empty = InstanceID.Empty;
				empty.NetSegment = (ushort)j;
				if (!this.m_smoothColors.Contains(empty))
				{
					int num;
					int num2;
					RenderManager.GetColorLocation((uint)(49152 + j), out num, out num2);
					NetInfo info = this.m_segments.m_buffer[j].Info;
					Color color = info.m_netAI.GetColor((ushort)j, ref this.m_segments.m_buffer[j], currentMode);
					colorMap.SetPixel(num, num2, color);
				}
			}
		}
		for (int k = colorUpdateMinNode; k <= colorUpdateMaxNode; k++)
		{
			if (this.m_nodes.m_buffer[k].m_flags != NetNode.Flags.None)
			{
				InstanceID empty2 = InstanceID.Empty;
				empty2.NetNode = (ushort)k;
				if (!this.m_smoothColors.Contains(empty2))
				{
					int num3;
					int num4;
					RenderManager.GetColorLocation((uint)(86016 + k), out num3, out num4);
					NetInfo info2 = this.m_nodes.m_buffer[k].Info;
					Color color2 = info2.m_netAI.GetColor((ushort)k, ref this.m_nodes.m_buffer[k], currentMode);
					colorMap.SetPixel(num3, num4, color2);
				}
			}
		}
		if (colorUpdateMinSegment == 0)
		{
			int num5;
			int num6;
			RenderManager.GetColorLocation(49152u, out num5, out num6);
			if (currentMode != InfoManager.InfoMode.None)
			{
				colorMap.SetPixel(num5, num6, Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
			}
			else
			{
				colorMap.SetPixel(num5, num6, Color.get_white());
			}
		}
		if (colorUpdateMinNode == 0)
		{
			int num7;
			int num8;
			RenderManager.GetColorLocation(86016u, out num7, out num8);
			if (currentMode != InfoManager.InfoMode.None)
			{
				colorMap.SetPixel(num7, num8, Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
			}
			else
			{
				colorMap.SetPixel(num7, num8, Color.get_white());
			}
		}
		float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		if (this.m_smoothColors.Count != 0 && simulationTimeDelta > 0f)
		{
			float num9 = 1f - Mathf.Pow(0.5f, simulationTimeDelta);
			foreach (InstanceID current in this.m_smoothColors)
			{
				int num10;
				int num11;
				Color color3;
				if (current.NetNode != 0)
				{
					RenderManager.GetColorLocation(86016u + (uint)current.NetNode, out num10, out num11);
					NetInfo info3 = this.m_nodes.m_buffer[(int)current.NetNode].Info;
					color3 = info3.m_netAI.GetColor(current.NetNode, ref this.m_nodes.m_buffer[(int)current.NetNode], currentMode);
				}
				else
				{
					RenderManager.GetColorLocation((uint)(49152 + current.NetSegment), out num10, out num11);
					NetInfo info4 = this.m_segments.m_buffer[(int)current.NetSegment].Info;
					color3 = info4.m_netAI.GetColor(current.NetSegment, ref this.m_segments.m_buffer[(int)current.NetSegment], currentMode);
				}
				Color pixel = colorMap.GetPixel(num10, num11);
				float num12 = color3.r - pixel.r;
				float num13 = color3.g - pixel.g;
				float num14 = color3.b - pixel.b;
				float num15 = color3.a - pixel.a;
				bool flag = true;
				if (num12 < -0.005f)
				{
					num12 = Mathf.Min(-0.005f, num12 * num9);
					flag = false;
				}
				else if (num12 > 0.005f)
				{
					num12 = Mathf.Max(0.005f, num12 * num9);
					flag = false;
				}
				if (num13 < -0.005f)
				{
					num13 = Mathf.Min(-0.005f, num13 * num9);
					flag = false;
				}
				else if (num13 > 0.005f)
				{
					num13 = Mathf.Max(0.005f, num13 * num9);
					flag = false;
				}
				if (num14 < -0.005f)
				{
					num14 = Mathf.Min(-0.005f, num14 * num9);
					flag = false;
				}
				else if (num14 > 0.005f)
				{
					num14 = Mathf.Max(0.005f, num14 * num9);
					flag = false;
				}
				if (num15 < -0.005f)
				{
					num15 = Mathf.Min(-0.005f, num15 * num9);
					flag = false;
				}
				else if (num15 > 0.005f)
				{
					num15 = Mathf.Max(0.005f, num15 * num9);
					flag = false;
				}
				if (flag)
				{
					colorMap.SetPixel(num10, num11, color3);
					this.m_removeSmoothColors.Add(current);
				}
				else
				{
					pixel.r += num12;
					pixel.g += num13;
					pixel.b += num14;
					pixel.a += num15;
					colorMap.SetPixel(num10, num11, pixel);
				}
			}
			for (int l = 0; l < this.m_removeSmoothColors.m_size; l++)
			{
				this.m_smoothColors.Remove(this.m_removeSmoothColors.m_buffer[l]);
			}
			this.m_removeSmoothColors.m_size = 0;
		}
		return true;
	}

	public float SampleSmoothHeight(Vector3 worldPos)
	{
		float num = 0f;
		int num2 = Mathf.Max((int)((worldPos.x - 96f) / 64f + 135f), 0);
		int num3 = Mathf.Max((int)((worldPos.z - 96f) / 64f + 135f), 0);
		int num4 = Mathf.Min((int)((worldPos.x + 96f) / 64f + 135f), 269);
		int num5 = Mathf.Min((int)((worldPos.z + 96f) / 64f + 135f), 269);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = this.m_segmentGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort startNode = this.m_segments.m_buffer[(int)num6].m_startNode;
					ushort endNode = this.m_segments.m_buffer[(int)num6].m_endNode;
					Vector3 position = this.m_nodes.m_buffer[(int)startNode].m_position;
					Vector3 position2 = this.m_nodes.m_buffer[(int)endNode].m_position;
					NetInfo info = this.m_segments.m_buffer[(int)num6].Info;
					float num8 = info.m_halfWidth + 80f;
					float num9 = Mathf.Max(Mathf.Max(worldPos.x - num8 - position.x, worldPos.z - num8 - position.z), Mathf.Max(position.x - worldPos.x - num8, position.z - worldPos.z - num8));
					float num10 = Mathf.Max(Mathf.Max(worldPos.x - num8 - position2.x, worldPos.z - num8 - position2.z), Mathf.Max(position2.x - worldPos.x - num8, position2.z - worldPos.z - num8));
					if (num9 < 0f || num10 < 0f)
					{
						float num12;
						float num11 = Segment2.DistanceSqr(VectorUtils.XZ(position), VectorUtils.XZ(position2), VectorUtils.XZ(worldPos), ref num12);
						if (num11 < (info.m_halfWidth + 16f) * (info.m_halfWidth + 16f))
						{
							num11 = Mathf.Sqrt(num11) - info.m_halfWidth;
							float num13 = position.y + (position2.y - position.y) * num12 + info.m_maxHeight;
							num13 = Mathf.Lerp(worldPos.y, num13, MathUtils.SmoothClamp01(1f - num11 * 0.0625f));
							if (num13 > num)
							{
								num = num13;
							}
						}
					}
					num6 = this.m_segments.m_buffer[(int)num6].m_nextGridSegment;
					if (++num7 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.set_color(Color.get_yellow());
		for (uint num = 0u; num < this.m_nodes.m_size; num += 1u)
		{
			if (this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_flags != NetNode.Flags.None)
			{
				uint lane = this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_lane;
				if (lane != 0u)
				{
					Vector3 position = this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_position;
					Vector3 vector = this.m_lanes.m_buffer[(int)((UIntPtr)lane)].CalculatePosition((float)this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_laneOffset * 0.003921569f);
					if ((this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_flags & NetNode.Flags.Disabled) != NetNode.Flags.None)
					{
						Gizmos.set_color(Color.get_gray());
					}
					else
					{
						Gizmos.set_color(Color.get_green());
					}
					Gizmos.DrawSphere(position, 2f);
					Gizmos.DrawSphere(vector, 1f);
					Gizmos.DrawLine(position, vector);
					Gizmos.set_color(Color.get_yellow());
				}
				else if ((this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_flags & NetNode.Flags.Disabled) != NetNode.Flags.None)
				{
					Gizmos.set_color(Color.get_gray());
					Gizmos.DrawSphere(this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_position, 2f);
					Gizmos.set_color(Color.get_yellow());
				}
				else
				{
					Gizmos.DrawSphere(this.m_nodes.m_buffer[(int)((UIntPtr)num)].m_position, 2f);
				}
			}
		}
		for (uint num2 = 0u; num2 < this.m_segments.m_size; num2 += 1u)
		{
			if (this.m_segments.m_buffer[(int)((UIntPtr)num2)].m_flags != NetSegment.Flags.None)
			{
				Vector3 position2 = this.m_nodes.m_buffer[(int)this.m_segments.m_buffer[(int)((UIntPtr)num2)].m_startNode].m_position;
				Vector3 position3 = this.m_nodes.m_buffer[(int)this.m_segments.m_buffer[(int)((UIntPtr)num2)].m_endNode].m_position;
				Gizmos.DrawLine(position2, position3);
			}
		}
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (this.m_nodeCount >= 16384)
			{
				return false;
			}
			if (this.m_segmentCount >= 16384)
			{
				return false;
			}
			if (this.m_laneCount >= 131072)
			{
				return false;
			}
		}
		else if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_nodeCount >= 1024)
			{
				return false;
			}
			if (this.m_segmentCount >= 1024)
			{
				return false;
			}
			if (this.m_laneCount >= 8192)
			{
				return false;
			}
		}
		else
		{
			if (this.m_nodeCount >= 32256)
			{
				return false;
			}
			if (this.m_segmentCount >= 36352)
			{
				return false;
			}
			if (this.m_laneCount >= 258048)
			{
				return false;
			}
		}
		return true;
	}

	public bool CreateNode(out ushort node, ref Randomizer randomizer, NetInfo info, Vector3 position, uint buildIndex)
	{
		ushort num;
		if (this.m_nodes.CreateItem(out num, ref randomizer))
		{
			node = num;
			this.m_nodes.m_buffer[(int)node].m_position = position;
			this.m_nodes.m_buffer[(int)node].m_flags = NetNode.Flags.Created;
			this.m_nodes.m_buffer[(int)node].Info = info;
			this.m_nodes.m_buffer[(int)node].m_buildIndex = buildIndex;
			this.m_nodes.m_buffer[(int)node].m_building = 0;
			this.m_nodes.m_buffer[(int)node].m_lane = 0u;
			this.m_nodes.m_buffer[(int)node].m_nextLaneNode = 0;
			this.m_nodes.m_buffer[(int)node].m_nextBuildingNode = 0;
			this.m_nodes.m_buffer[(int)node].m_nextGridNode = 0;
			this.m_nodes.m_buffer[(int)node].m_laneOffset = 0;
			this.m_nodes.m_buffer[(int)node].m_elevation = 0;
			this.m_nodes.m_buffer[(int)node].m_heightOffset = 0;
			this.m_nodes.m_buffer[(int)node].m_problems = Notification.Problem.None;
			this.m_nodes.m_buffer[(int)node].m_transportLine = 0;
			this.m_nodes.m_buffer[(int)node].m_connectCount = 0;
			this.m_nodes.m_buffer[(int)node].m_maxWaitTime = 0;
			this.m_nodes.m_buffer[(int)node].m_coverage = 0;
			this.m_nodes.m_buffer[(int)node].m_tempCounter = 0;
			this.m_nodes.m_buffer[(int)node].m_finalCounter = 0;
			info.m_netAI.CreateNode(node, ref this.m_nodes.m_buffer[(int)node]);
			this.InitializeNode(node, ref this.m_nodes.m_buffer[(int)node]);
			this.UpdateNode(node);
			this.UpdateNodeColors(node);
			this.m_nodes.m_buffer[(int)node].UpdateBounds(node);
			this.m_nodeCount = (int)(this.m_nodes.ItemCount() - 1u);
			return true;
		}
		node = 0;
		return false;
	}

	public void MoveNode(ushort node, Vector3 position)
	{
		this.MoveNode(node, ref this.m_nodes.m_buffer[(int)node], position);
	}

	private void MoveNode(ushort node, ref NetNode data, Vector3 position)
	{
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				this.FinalizeSegment(segment, ref this.m_segments.m_buffer[(int)segment]);
			}
		}
		this.FinalizeNode(node, ref data);
		data.m_position = position;
		this.InitializeNode(node, ref data);
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = data.GetSegment(j);
			if (segment2 != 0)
			{
				this.InitializeSegment(segment2, ref this.m_segments.m_buffer[(int)segment2]);
			}
		}
		this.UpdateNode(node);
	}

	private void InitializeNode(ushort node, ref NetNode data)
	{
		int num = Mathf.Clamp((int)(data.m_position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		this.m_nodes.m_buffer[(int)node].m_nextGridNode = this.m_nodeGrid[num3];
		this.m_nodeGrid[num3] = node;
	}

	private void FinalizeNode(ushort node, ref NetNode data)
	{
		int num = Mathf.Clamp((int)(data.m_position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		ushort num4 = 0;
		ushort num5 = this.m_nodeGrid[num3];
		int num6 = 0;
		while (num5 != 0)
		{
			if (num5 == node)
			{
				if (num4 == 0)
				{
					this.m_nodeGrid[num3] = data.m_nextGridNode;
				}
				else
				{
					this.m_nodes.m_buffer[(int)num4].m_nextGridNode = data.m_nextGridNode;
				}
				break;
			}
			num4 = num5;
			num5 = this.m_nodes.m_buffer[(int)num5].m_nextGridNode;
			if (++num6 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_nextGridNode = 0;
	}

	private void ReleaseNodeSegment(ushort node, ref ushort nodeSegment)
	{
		if (nodeSegment != 0)
		{
			if (this.m_segments.m_buffer[(int)nodeSegment].m_startNode == node)
			{
				this.m_segments.m_buffer[(int)nodeSegment].m_startNode = 0;
			}
			else if (this.m_segments.m_buffer[(int)nodeSegment].m_endNode == node)
			{
				this.m_segments.m_buffer[(int)nodeSegment].m_endNode = 0;
			}
			this.ReleaseSegmentImplementation(nodeSegment);
			nodeSegment = 0;
		}
	}

	public void ReleaseNode(ushort node)
	{
		this.PreReleaseNodeImplementation(node, ref this.m_nodes.m_buffer[(int)node], false, false);
		this.ReleaseNodeImplementation(node, ref this.m_nodes.m_buffer[(int)node]);
	}

	private void PreReleaseNodeImplementation(ushort node, bool checkDeleted, bool checkTouchable)
	{
		this.PreReleaseNodeImplementation(node, ref this.m_nodes.m_buffer[(int)node], checkDeleted, checkTouchable);
	}

	private void PreReleaseNodeImplementation(ushort node, ref NetNode data, bool checkDeleted, bool checkTouchable)
	{
		if (data.m_flags != NetNode.Flags.None)
		{
			if (checkTouchable && (data.m_flags & NetNode.Flags.Untouchable) != NetNode.Flags.None)
			{
				return;
			}
			if (checkDeleted)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0 && (this.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Deleted) == NetSegment.Flags.None)
					{
						return;
					}
				}
			}
			if ((data.m_flags & NetNode.Flags.Deleted) == NetNode.Flags.None)
			{
				data.m_flags |= NetNode.Flags.Deleted;
				this.FinalizeNode(node, ref data);
				if (data.m_segment0 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment0);
				}
				if (data.m_segment1 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment1);
				}
				if (data.m_segment2 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment2);
				}
				if (data.m_segment3 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment3);
				}
				if (data.m_segment4 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment4);
				}
				if (data.m_segment5 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment5);
				}
				if (data.m_segment6 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment6);
				}
				if (data.m_segment7 != 0)
				{
					this.PreReleaseSegmentImplementation(data.m_segment7);
				}
				data.UpdateNode(node);
				this.UpdateNodeRenderer(node, true);
			}
		}
	}

	private void ReleaseNodeImplementation(ushort node)
	{
		this.ReleaseNodeImplementation(node, ref this.m_nodes.m_buffer[(int)node]);
	}

	private void ReleaseNodeImplementation(ushort node, ref NetNode data)
	{
		if (data.m_flags != NetNode.Flags.None)
		{
			NetInfo info = data.Info;
			if (info != null)
			{
				info.m_netAI.ReleaseNode(node, ref data);
			}
			data.m_flags = NetNode.Flags.None;
			this.ReleaseNodeSegment(node, ref data.m_segment0);
			this.ReleaseNodeSegment(node, ref data.m_segment1);
			this.ReleaseNodeSegment(node, ref data.m_segment2);
			this.ReleaseNodeSegment(node, ref data.m_segment3);
			this.ReleaseNodeSegment(node, ref data.m_segment4);
			this.ReleaseNodeSegment(node, ref data.m_segment5);
			this.ReleaseNodeSegment(node, ref data.m_segment6);
			this.ReleaseNodeSegment(node, ref data.m_segment7);
			data.m_nextBuildingNode = 0;
			if (data.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().ReleaseBuilding(data.m_building);
				data.m_building = 0;
			}
			this.m_nodes.ReleaseItem(node);
			this.m_nodeCount = (int)(this.m_nodes.ItemCount() - 1u);
		}
	}

	private void UpdateSegment(ushort segment, ushort fromNode, int level)
	{
		this.m_updatedSegments[segment >> 6] |= 1uL << (int)segment;
		this.m_segmentsUpdated = true;
		if (level <= 0)
		{
			ushort startNode = this.m_segments.m_buffer[(int)segment].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)segment].m_endNode;
			if (startNode != 0 && startNode != fromNode)
			{
				this.UpdateNode(startNode, segment, level + 1);
			}
			if (endNode != 0 && endNode != fromNode)
			{
				this.UpdateNode(endNode, segment, level + 1);
			}
		}
	}

	public void UpdateSegment(ushort segment)
	{
		this.UpdateSegment(segment, 0, 0);
	}

	public void UpdateSegmentFlags(ushort segment)
	{
		if (this.m_segments.m_buffer[(int)segment].m_flags == NetSegment.Flags.None)
		{
			return;
		}
		NetInfo info = this.m_segments.m_buffer[(int)segment].Info;
		if (info == null)
		{
			return;
		}
		info.m_netAI.UpdateSegmentFlags(segment, ref this.m_segments.m_buffer[(int)segment]);
	}

	public void UpdateSegmentRenderer(ushort segment, bool updateGroup)
	{
		if (this.m_segments.m_buffer[(int)segment].m_flags == NetSegment.Flags.None)
		{
			return;
		}
		Singleton<RenderManager>.get_instance().UpdateInstance((uint)(49152 + segment));
		if (updateGroup)
		{
			NetInfo info = this.m_segments.m_buffer[(int)segment].Info;
			if (info == null)
			{
				return;
			}
			ushort startNode = this.m_segments.m_buffer[(int)segment].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)segment].m_endNode;
			Vector3 position = this.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = this.m_nodes.m_buffer[(int)endNode].m_position;
			Vector3 vector = (position + position2) * 0.5f;
			int num = Mathf.Clamp((int)(vector.x / 64f + 135f), 0, 269);
			int num2 = Mathf.Clamp((int)(vector.z / 64f + 135f), 0, 269);
			int x = num * 45 / 270;
			int z = num2 * 45 / 270;
			int num3 = 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer;
			if ((info.m_nodes != null && info.m_nodes.Length != 0) || (info.m_segments != null && info.m_segments.Length != 0))
			{
				num3 |= info.m_netLayers;
			}
			if (info.m_hasForwardVehicleLanes != info.m_hasBackwardVehicleLanes)
			{
				num3 |= 1 << this.m_arrowLayer;
			}
			if (info.m_lanes != null)
			{
				uint num4 = this.m_segments.m_buffer[(int)segment].m_lanes;
				int num5 = 0;
				while (num5 < info.m_lanes.Length && num4 != 0u)
				{
					NetLaneProps laneProps = info.m_lanes[num5].m_laneProps;
					if (laneProps != null && laneProps.m_props != null)
					{
						int num6 = laneProps.m_props.Length;
						for (int i = 0; i < num6; i++)
						{
							NetLaneProps.Prop prop = laneProps.m_props[i];
							if (prop.m_finalProp != null)
							{
								num3 |= 1 << prop.m_finalProp.m_prefabDataLayer;
								if (prop.m_finalProp.m_effectLayer != -1)
								{
									num3 |= 1 << prop.m_finalProp.m_effectLayer;
								}
							}
							if (prop.m_finalTree != null)
							{
								num3 |= 1 << prop.m_finalTree.m_prefabDataLayer;
							}
						}
					}
					num4 = this.m_lanes.m_buffer[(int)((UIntPtr)num4)].m_nextLane;
					num5++;
				}
			}
			for (int j = 0; j < 32; j++)
			{
				if ((num3 & 1 << j) != 0)
				{
					Singleton<RenderManager>.get_instance().UpdateGroup(x, z, j);
				}
			}
		}
	}

	public void UpdateNode(ushort node, ushort fromSegment, int level)
	{
		this.m_updatedNodes[node >> 6] |= 1uL << (int)node;
		this.m_nodesUpdated = true;
		if (level <= 1)
		{
			ushort segment = this.m_nodes.m_buffer[(int)node].m_segment0;
			ushort segment2 = this.m_nodes.m_buffer[(int)node].m_segment1;
			ushort segment3 = this.m_nodes.m_buffer[(int)node].m_segment2;
			ushort segment4 = this.m_nodes.m_buffer[(int)node].m_segment3;
			ushort segment5 = this.m_nodes.m_buffer[(int)node].m_segment4;
			ushort segment6 = this.m_nodes.m_buffer[(int)node].m_segment5;
			ushort segment7 = this.m_nodes.m_buffer[(int)node].m_segment6;
			ushort segment8 = this.m_nodes.m_buffer[(int)node].m_segment7;
			if (segment != 0 && segment != fromSegment)
			{
				this.UpdateSegment(segment, node, level + 1);
			}
			if (segment2 != 0 && segment2 != fromSegment)
			{
				this.UpdateSegment(segment2, node, level + 1);
			}
			if (segment3 != 0 && segment3 != fromSegment)
			{
				this.UpdateSegment(segment3, node, level + 1);
			}
			if (segment4 != 0 && segment4 != fromSegment)
			{
				this.UpdateSegment(segment4, node, level + 1);
			}
			if (segment5 != 0 && segment5 != fromSegment)
			{
				this.UpdateSegment(segment5, node, level + 1);
			}
			if (segment6 != 0 && segment6 != fromSegment)
			{
				this.UpdateSegment(segment6, node, level + 1);
			}
			if (segment7 != 0 && segment7 != fromSegment)
			{
				this.UpdateSegment(segment7, node, level + 1);
			}
			if (segment8 != 0 && segment8 != fromSegment)
			{
				this.UpdateSegment(segment8, node, level + 1);
			}
		}
	}

	public void UpdateNode(ushort node)
	{
		this.UpdateNode(node, 0, 0);
	}

	public void UpdateNodeFlags(ushort node)
	{
		if (this.m_nodes.m_buffer[(int)node].m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetInfo info = this.m_nodes.m_buffer[(int)node].Info;
		if (info == null)
		{
			return;
		}
		info.m_netAI.UpdateNodeFlags(node, ref this.m_nodes.m_buffer[(int)node]);
	}

	public void UpdateNodeRenderer(ushort node, bool updateGroup)
	{
		if (this.m_nodes.m_buffer[(int)node].m_flags == NetNode.Flags.None)
		{
			return;
		}
		Singleton<RenderManager>.get_instance().UpdateInstance(86016u + (uint)node);
		if (updateGroup)
		{
			NetInfo info = this.m_nodes.m_buffer[(int)node].Info;
			if (info == null)
			{
				return;
			}
			Vector3 position = this.m_nodes.m_buffer[(int)node].m_position;
			int num = Mathf.Clamp((int)(position.x / 64f + 135f), 0, 269);
			int num2 = Mathf.Clamp((int)(position.z / 64f + 135f), 0, 269);
			int x = num * 45 / 270;
			int z = num2 * 45 / 270;
			int num3 = info.m_netLayers | 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer;
			for (int i = 0; i < 32; i++)
			{
				if ((num3 & 1 << i) != 0)
				{
					Singleton<RenderManager>.get_instance().UpdateGroup(x, z, i);
				}
			}
		}
	}

	public bool CreateSegment(out ushort segment, ref Randomizer randomizer, NetInfo info, ushort startNode, ushort endNode, Vector3 startDirection, Vector3 endDirection, uint buildIndex, uint modifiedIndex, bool invert)
	{
		ushort num;
		if (!this.m_segments.CreateItem(out num, ref randomizer))
		{
			segment = 0;
			return false;
		}
		segment = num;
		if (!this.m_nodes.m_buffer[(int)startNode].AddSegment(segment))
		{
			this.m_segments.ReleaseItem(num);
			segment = 0;
			return false;
		}
		if (!this.m_nodes.m_buffer[(int)endNode].AddSegment(segment))
		{
			this.m_nodes.m_buffer[(int)startNode].RemoveSegment(segment);
			this.m_segments.ReleaseItem(num);
			segment = 0;
			return false;
		}
		this.m_segments.m_buffer[(int)segment].m_flags = NetSegment.Flags.Created;
		if (invert)
		{
			NetSegment[] expr_BF_cp_0 = this.m_segments.m_buffer;
			ushort expr_BF_cp_1 = segment;
			expr_BF_cp_0[(int)expr_BF_cp_1].m_flags = (expr_BF_cp_0[(int)expr_BF_cp_1].m_flags | NetSegment.Flags.Invert);
		}
		this.m_segments.m_buffer[(int)segment].Info = info;
		this.m_segments.m_buffer[(int)segment].m_buildIndex = buildIndex;
		this.m_segments.m_buffer[(int)segment].m_modifiedIndex = modifiedIndex;
		this.m_segments.m_buffer[(int)segment].m_startNode = startNode;
		this.m_segments.m_buffer[(int)segment].m_endNode = endNode;
		this.m_segments.m_buffer[(int)segment].m_startDirection = startDirection;
		this.m_segments.m_buffer[(int)segment].m_endDirection = endDirection;
		this.m_segments.m_buffer[(int)segment].m_problems = Notification.Problem.None;
		this.m_segments.m_buffer[(int)segment].m_blockStartLeft = 0;
		this.m_segments.m_buffer[(int)segment].m_blockStartRight = 0;
		this.m_segments.m_buffer[(int)segment].m_blockEndLeft = 0;
		this.m_segments.m_buffer[(int)segment].m_blockEndRight = 0;
		this.m_segments.m_buffer[(int)segment].m_lanes = 0u;
		this.m_segments.m_buffer[(int)segment].m_path = 0u;
		this.m_segments.m_buffer[(int)segment].m_trafficBuffer = 0;
		this.m_segments.m_buffer[(int)segment].m_trafficDensity = 0;
		this.m_segments.m_buffer[(int)segment].m_trafficLightState0 = 0;
		this.m_segments.m_buffer[(int)segment].m_trafficLightState1 = 0;
		this.m_segments.m_buffer[(int)segment].m_startLeftSegment = 0;
		this.m_segments.m_buffer[(int)segment].m_startRightSegment = 0;
		this.m_segments.m_buffer[(int)segment].m_endLeftSegment = 0;
		this.m_segments.m_buffer[(int)segment].m_endRightSegment = 0;
		this.m_segments.m_buffer[(int)segment].m_averageLength = 0f;
		this.m_segments.m_buffer[(int)segment].m_nextGridSegment = 0;
		this.m_segments.m_buffer[(int)segment].m_cornerAngleStart = 0;
		this.m_segments.m_buffer[(int)segment].m_cornerAngleEnd = 0;
		this.m_segments.m_buffer[(int)segment].m_fireCoverage = 0;
		this.m_segments.m_buffer[(int)segment].m_wetness = 0;
		this.m_segments.m_buffer[(int)segment].m_condition = 0;
		this.m_segments.m_buffer[(int)segment].m_nameSeed = 0;
		this.m_segments.m_buffer[(int)segment].m_noiseBuffer = 0;
		this.m_segments.m_buffer[(int)segment].m_noiseDensity = 0;
		info.m_netAI.CreateSegment(segment, ref this.m_segments.m_buffer[(int)segment]);
		if (info.m_lanes != null)
		{
			this.CreateLanes(out this.m_segments.m_buffer[(int)segment].m_lanes, ref randomizer, segment, info.m_lanes.Length);
		}
		this.InitializeSegment(segment, ref this.m_segments.m_buffer[(int)segment]);
		this.UpdateSegment(segment);
		this.UpdateSegmentColors(segment);
		this.m_segments.m_buffer[(int)segment].UpdateBounds(segment);
		this.m_segments.m_buffer[(int)segment].UpdateZones(segment);
		this.m_segmentCount = (int)(this.m_segments.ItemCount() - 1u);
		return true;
	}

	private void InitializeSegment(ushort segment, ref NetSegment data)
	{
		Vector3 vector = (this.m_nodes.m_buffer[(int)data.m_startNode].m_position + this.m_nodes.m_buffer[(int)data.m_endNode].m_position) * 0.5f;
		int num = Mathf.Clamp((int)(vector.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(vector.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		this.m_segments.m_buffer[(int)segment].m_nextGridSegment = this.m_segmentGrid[num3];
		this.m_segmentGrid[num3] = segment;
	}

	private void FinalizeSegment(ushort segment, ref NetSegment data)
	{
		Vector3 vector = (this.m_nodes.m_buffer[(int)data.m_startNode].m_position + this.m_nodes.m_buffer[(int)data.m_endNode].m_position) * 0.5f;
		int num = Mathf.Clamp((int)(vector.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(vector.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		ushort num4 = 0;
		ushort num5 = this.m_segmentGrid[num3];
		int num6 = 0;
		while (num5 != 0)
		{
			if (num5 == segment)
			{
				if (num4 == 0)
				{
					this.m_segmentGrid[num3] = data.m_nextGridSegment;
				}
				else
				{
					this.m_segments.m_buffer[(int)num4].m_nextGridSegment = data.m_nextGridSegment;
				}
				break;
			}
			num4 = num5;
			num5 = this.m_segments.m_buffer[(int)num5].m_nextGridSegment;
			if (++num6 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_nextGridSegment = 0;
	}

	private void ReleaseSegmentNode(ushort segment, ref ushort segmentNode, bool keepNodes)
	{
		if (segmentNode != 0)
		{
			this.m_nodes.m_buffer[(int)segmentNode].RemoveSegment(segment);
			if (!keepNodes && (this.m_nodes.m_buffer[(int)segmentNode].m_flags & NetNode.Flags.Untouchable) == NetNode.Flags.None && this.m_nodes.m_buffer[(int)segmentNode].CountSegments() == 0)
			{
				this.ReleaseNodeImplementation(segmentNode);
			}
			else
			{
				this.UpdateNode(segmentNode, segment, 1);
			}
			segmentNode = 0;
		}
	}

	private void ReleaseSegmentBlock(ushort segment, ref ushort segmentBlock)
	{
		if (segmentBlock != 0)
		{
			Singleton<ZoneManager>.get_instance().ReleaseBlock(segmentBlock);
			segmentBlock = 0;
		}
	}

	public void ReleaseSegment(ushort segment, bool keepNodes)
	{
		this.PreReleaseSegmentImplementation(segment, ref this.m_segments.m_buffer[(int)segment], keepNodes);
		this.ReleaseSegmentImplementation(segment, ref this.m_segments.m_buffer[(int)segment], keepNodes);
	}

	private void PreReleaseSegmentImplementation(ushort segment)
	{
		this.PreReleaseSegmentImplementation(segment, ref this.m_segments.m_buffer[(int)segment], false);
	}

	private void PreReleaseSegmentImplementation(ushort segment, ref NetSegment data, bool keepNodes)
	{
		if ((data.m_flags & NetSegment.Flags.Deleted) == NetSegment.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.Deleted;
			this.FinalizeSegment(segment, ref data);
			if (!keepNodes)
			{
				if (data.m_startNode != 0)
				{
					this.PreReleaseNodeImplementation(data.m_startNode, true, true);
				}
				if (data.m_endNode != 0)
				{
					this.PreReleaseNodeImplementation(data.m_endNode, true, true);
				}
			}
			data.UpdateSegment(segment);
			data.UpdateZones(segment);
			this.UpdateSegmentRenderer(segment, true);
		}
	}

	private void ReleaseSegmentImplementation(ushort segment)
	{
		this.ReleaseSegmentImplementation(segment, ref this.m_segments.m_buffer[(int)segment], false);
	}

	private void ReleaseSegmentImplementation(ushort segment, ref NetSegment data, bool keepNodes)
	{
		if (data.m_flags != NetSegment.Flags.None)
		{
			NetInfo info = data.Info;
			if (info != null)
			{
				info.m_netAI.ReleaseSegment(segment, ref data);
			}
			if (this.m_netAdjust != null)
			{
				this.m_netAdjust.SegmentRemoved(segment);
			}
			InstanceID id = default(InstanceID);
			id.NetSegment = segment;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags = NetSegment.Flags.None;
			this.m_adjustedSegments[segment >> 6] &= ~(1uL << (int)segment);
			this.ReleaseSegmentNode(segment, ref data.m_startNode, keepNodes);
			this.ReleaseSegmentNode(segment, ref data.m_endNode, keepNodes);
			this.ReleaseSegmentBlock(segment, ref data.m_blockStartLeft);
			this.ReleaseSegmentBlock(segment, ref data.m_blockStartRight);
			this.ReleaseSegmentBlock(segment, ref data.m_blockEndLeft);
			this.ReleaseSegmentBlock(segment, ref data.m_blockEndRight);
			if (data.m_lanes != 0u)
			{
				this.ReleaseLanes(data.m_lanes);
				data.m_lanes = 0u;
			}
			if (data.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
			}
			this.m_segments.ReleaseItem(segment);
			this.m_segmentCount = (int)(this.m_segments.ItemCount() - 1u);
		}
	}

	public bool CreateLanes(out uint firstLane, ref Randomizer randomizer, ushort segment, int count)
	{
		firstLane = 0u;
		if (count == 0)
		{
			return true;
		}
		NetLane netLane = default(NetLane);
		uint num = 0u;
		for (int i = 0; i < count; i++)
		{
			uint num2;
			if (!this.m_lanes.CreateItem(out num2, ref randomizer))
			{
				this.ReleaseLanes(firstLane);
				firstLane = 0u;
				return false;
			}
			if (i == 0)
			{
				firstLane = num2;
			}
			else
			{
				netLane.m_nextLane = num2;
				this.m_lanes.m_buffer[(int)((UIntPtr)num)] = netLane;
			}
			netLane = default(NetLane);
			netLane.m_flags = 1;
			netLane.m_segment = segment;
			num = num2;
		}
		this.m_lanes.m_buffer[(int)((UIntPtr)num)] = netLane;
		this.m_laneCount = (int)(this.m_lanes.ItemCount() - 1u);
		return true;
	}

	public void ReleaseLanes(uint firstLane)
	{
		int num = 0;
		while (firstLane != 0u)
		{
			uint nextLane = this.m_lanes.m_buffer[(int)((UIntPtr)firstLane)].m_nextLane;
			this.ReleaseLaneImplementation(firstLane, ref this.m_lanes.m_buffer[(int)((UIntPtr)firstLane)]);
			firstLane = nextLane;
			if (++num > 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		this.m_laneCount = (int)(this.m_lanes.ItemCount() - 1u);
	}

	private void ReleaseLaneImplementation(uint lane, ref NetLane data)
	{
		if (data.m_nodes != 0)
		{
			ushort num = data.m_nodes;
			data.m_nodes = 0;
			int num2 = 0;
			while (num != 0)
			{
				ushort nextLaneNode = this.m_nodes.m_buffer[(int)num].m_nextLaneNode;
				this.m_nodes.m_buffer[(int)num].m_nextLaneNode = 0;
				this.m_nodes.m_buffer[(int)num].m_lane = 0u;
				this.m_nodes.m_buffer[(int)num].m_laneOffset = 0;
				this.UpdateNode(num, 0, 10);
				num = nextLaneNode;
				if (++num2 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		data = default(NetLane);
		this.m_lanes.ReleaseItem(lane);
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float num = Mathf.Min(heightArea.m_min.x, surfaceArea.m_min.x);
		float num2 = Mathf.Min(heightArea.m_min.z, surfaceArea.m_min.z);
		float num3 = Mathf.Max(heightArea.m_max.x, surfaceArea.m_max.x);
		float num4 = Mathf.Max(heightArea.m_max.z, surfaceArea.m_max.z);
		int num5 = Mathf.Max((int)((num - 64f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 64f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 64f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 64f) / 64f + 135f), 269);
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = this.m_nodeGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = this.m_nodes.m_buffer[(int)num9].m_position;
					float num11 = Mathf.Max(Mathf.Max(num - 64f - position.x, num2 - 64f - position.z), Mathf.Max(position.x - num3 - 64f, position.z - num4 - 64f));
					if (num11 < 0f)
					{
						this.m_nodes.m_buffer[(int)num9].TerrainUpdated(num9, num, num2, num3, num4);
					}
					num9 = this.m_nodes.m_buffer[(int)num9].m_nextGridNode;
					if (++num10 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = num6; k <= num8; k++)
		{
			for (int l = num5; l <= num7; l++)
			{
				ushort num12 = this.m_segmentGrid[k * 270 + l];
				int num13 = 0;
				while (num12 != 0)
				{
					ushort startNode = this.m_segments.m_buffer[(int)num12].m_startNode;
					ushort endNode = this.m_segments.m_buffer[(int)num12].m_endNode;
					Vector3 position2 = this.m_nodes.m_buffer[(int)startNode].m_position;
					Vector3 position3 = this.m_nodes.m_buffer[(int)endNode].m_position;
					float num14 = Mathf.Max(Mathf.Max(num - 64f - position2.x, num2 - 64f - position2.z), Mathf.Max(position2.x - num3 - 64f, position2.z - num4 - 64f));
					float num15 = Mathf.Max(Mathf.Max(num - 64f - position3.x, num2 - 64f - position3.z), Mathf.Max(position3.x - num3 - 64f, position3.z - num4 - 64f));
					if (num14 < 0f || num15 < 0f)
					{
						this.m_segments.m_buffer[(int)num12].TerrainUpdated(num12, num, num2, num3, num4);
					}
					num12 = this.m_segments.m_buffer[(int)num12].m_nextGridSegment;
					if (++num13 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float num = Mathf.Min(heightArea.m_min.x, surfaceArea.m_min.x);
		float num2 = Mathf.Min(heightArea.m_min.z, surfaceArea.m_min.z);
		float num3 = Mathf.Max(heightArea.m_max.x, surfaceArea.m_max.x);
		float num4 = Mathf.Max(heightArea.m_max.z, surfaceArea.m_max.z);
		int num5 = Mathf.Max((int)((num - 64f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 64f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 64f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 64f) / 64f + 135f), 269);
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = this.m_nodeGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = this.m_nodes.m_buffer[(int)num9].m_position;
					float num11 = Mathf.Max(Mathf.Max(num - 64f - position.x, num2 - 64f - position.z), Mathf.Max(position.x - num3 - 64f, position.z - num4 - 64f));
					if (num11 < 0f)
					{
						this.m_nodes.m_buffer[(int)num9].AfterTerrainUpdate(num9, num, num2, num3, num4);
					}
					num9 = this.m_nodes.m_buffer[(int)num9].m_nextGridNode;
					if (++num10 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = num6; k <= num8; k++)
		{
			for (int l = num5; l <= num7; l++)
			{
				ushort num12 = this.m_segmentGrid[k * 270 + l];
				int num13 = 0;
				while (num12 != 0)
				{
					ushort startNode = this.m_segments.m_buffer[(int)num12].m_startNode;
					ushort endNode = this.m_segments.m_buffer[(int)num12].m_endNode;
					Vector2 vector = VectorUtils.XZ(this.m_nodes.m_buffer[(int)startNode].m_position);
					Vector2 vector2 = VectorUtils.XZ(this.m_nodes.m_buffer[(int)endNode].m_position);
					float num14 = Mathf.Max(Mathf.Max(num - 64f - vector.x, num2 - 64f - vector.y), Mathf.Max(vector.x - num3 - 64f, vector.y - num3 - 64f));
					float num15 = Mathf.Max(Mathf.Max(num - 64f - vector2.x, num2 - 64f - vector2.y), Mathf.Max(vector2.x - num3 - 64f, vector2.y - num4 - 64f));
					if (num14 < 0f || num15 < 0f)
					{
						this.m_segments.m_buffer[(int)num12].AfterTerrainUpdate(num12, num, num2, num3, num4);
					}
					num12 = this.m_segments.m_buffer[(int)num12].m_nextGridSegment;
					if (++num13 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void GetClosestSegments(Vector3 pos, ushort[] segments, out int count)
	{
		count = 0;
		float num = 32f;
		for (int i = 0; i < 5; i++)
		{
			float num2 = pos.x - num;
			float num3 = pos.z - num;
			float num4 = pos.x + num;
			float num5 = pos.z + num;
			int num6 = Mathf.Max((int)((num2 - 64f) / 64f + 135f), 0);
			int num7 = Mathf.Max((int)((num3 - 64f) / 64f + 135f), 0);
			int num8 = Mathf.Min((int)((num4 + 64f) / 64f + 135f), 269);
			int num9 = Mathf.Min((int)((num5 + 64f) / 64f + 135f), 269);
			for (int j = num7; j <= num9; j++)
			{
				for (int k = num6; k <= num8; k++)
				{
					ushort num10 = this.m_segmentGrid[j * 270 + k];
					int num11 = 0;
					while (num10 != 0)
					{
						ushort startNode = this.m_segments.m_buffer[(int)num10].m_startNode;
						ushort endNode = this.m_segments.m_buffer[(int)num10].m_endNode;
						Vector3 position = this.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position2 = this.m_nodes.m_buffer[(int)endNode].m_position;
						float num12 = Mathf.Max(Mathf.Max(num2 - 64f - position.x, num3 - 64f - position.z), Mathf.Max(position.x - num4 - 64f, position.z - num5 - 64f));
						float num13 = Mathf.Max(Mathf.Max(num2 - 64f - position2.x, num3 - 64f - position2.z), Mathf.Max(position2.x - num4 - 64f, position2.z - num5 - 64f));
						if ((num12 < 0f || num13 < 0f) && Mathf.Min(position.x, position2.x) <= num4 && Mathf.Min(position.z, position2.z) <= num5 && Mathf.Max(position.x, position2.x) >= num2 && Mathf.Max(position.z, position2.z) >= num3)
						{
							bool flag = true;
							for (int l = 0; l < count; l++)
							{
								if (segments[l] == num10)
								{
									flag = false;
									break;
								}
							}
							if (flag)
							{
								segments[count++] = num10;
								if (count == segments.Length)
								{
									return;
								}
							}
						}
						num10 = this.m_segments.m_buffer[(int)num10].m_nextGridSegment;
						if (++num11 >= 36864)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
			num *= 2f;
		}
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, ItemClass.Layer layers, ushort ignoreNode1, ushort ignoreNode2, ushort ignoreSegment)
	{
		return this.OverlapQuad(quad, minY, maxY, collisionType, layers, ItemClass.Layer.None, ignoreNode1, ignoreNode2, ignoreSegment, null);
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, ItemClass.Layer layers, ushort ignoreNode1, ushort ignoreNode2, ushort ignoreSegment, ulong[] segmentMask)
	{
		return this.OverlapQuad(quad, minY, maxY, collisionType, layers, ItemClass.Layer.None, ignoreNode1, ignoreNode2, ignoreSegment, segmentMask);
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, ItemClass.Layer requireLayers, ItemClass.Layer forbidLayers, ushort ignoreNode1, ushort ignoreNode2, ushort ignoreSegment, ulong[] segmentMask)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		int num = Mathf.Max((int)((vector.x - 64f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.y - 64f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 64f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.y + 64f) / 64f + 135f), 269);
		ushort num5 = 0;
		ushort num6 = 0;
		if (ignoreSegment != 0)
		{
			ushort startNode = this.m_segments.m_buffer[(int)ignoreSegment].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)ignoreSegment].m_endNode;
			NetNode.Flags flags = this.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = this.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & NetNode.Flags.Middle) != NetNode.Flags.None)
			{
				num5 = startNode;
			}
			if ((flags2 & NetNode.Flags.Middle) != NetNode.Flags.None)
			{
				num6 = endNode;
			}
		}
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num7 = this.m_segmentGrid[i * 270 + j];
				int num8 = 0;
				while (num7 != 0)
				{
					if (num7 != ignoreSegment && (this.m_updatedSegments[num7 >> 6] & 1uL << (int)num7) == 0uL)
					{
						NetInfo info = this.m_segments.m_buffer[(int)num7].Info;
						if (info != null)
						{
							ItemClass.Layer collisionLayers = info.m_netAI.GetCollisionLayers();
							if (info != null && (requireLayers == ItemClass.Layer.None || (collisionLayers & requireLayers) != ItemClass.Layer.None) && (collisionLayers & forbidLayers) == ItemClass.Layer.None)
							{
								ushort startNode2 = this.m_segments.m_buffer[(int)num7].m_startNode;
								ushort endNode2 = this.m_segments.m_buffer[(int)num7].m_endNode;
								if (startNode2 != ignoreNode1 && startNode2 != ignoreNode2 && startNode2 != num5 && startNode2 != num6 && endNode2 != ignoreNode1 && endNode2 != ignoreNode2 && endNode2 != num5 && endNode2 != num6)
								{
									Vector3 position = this.m_nodes.m_buffer[(int)startNode2].m_position;
									Vector3 position2 = this.m_nodes.m_buffer[(int)endNode2].m_position;
									float num9 = Mathf.Max(Mathf.Max(vector.x - 64f - position.x, vector.y - 64f - position.z), Mathf.Max(position.x - vector2.x - 64f, position.z - vector2.y - 64f));
									float num10 = Mathf.Max(Mathf.Max(vector.x - 64f - position2.x, vector.y - 64f - position2.z), Mathf.Max(position2.x - vector2.x - 64f, position2.z - vector2.y - 64f));
									if ((num9 < 0f || num10 < 0f) && this.m_segments.m_buffer[(int)num7].OverlapQuad(num7, quad, minY, maxY, collisionType))
									{
										if (segmentMask == null)
										{
											return true;
										}
										segmentMask[num7 >> 6] |= 1uL << (int)num7;
										result = true;
									}
								}
							}
						}
					}
					num7 = this.m_segments.m_buffer[(int)num7].m_nextGridSegment;
					if (++num8 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public bool RayCast(NetInfo connectedType, Segment3 ray, float snapElevation, bool nameOnly, ItemClass.Service service, ItemClass.Service service2, ItemClass.SubService subService, ItemClass.SubService subService2, ItemClass.Layer itemLayers, ItemClass.Layer itemLayers2, NetNode.Flags ignoreNodeFlags, NetSegment.Flags ignoreSegmentFlags, out Vector3 hit, out ushort nodeIndex, out ushort segmentIndex)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = (int)(ray.a.x / 64f + 135f);
			int num2 = (int)(ray.a.z / 64f + 135f);
			int num3 = (int)(ray.b.x / 64f + 135f);
			int num4 = (int)(ray.b.z / 64f + 135f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 > 0.001f)
				{
					vector *= 64f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 > 0.001f)
				{
					vector *= 64f / num6;
				}
			}
			float num9 = 2f;
			float num10 = 16f;
			float num11 = 2f;
			float num12 = 16f;
			ushort num13 = 0;
			ushort num14 = 0;
			ushort num15 = 0;
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			int num16 = num;
			int num17 = num2;
			do
			{
				Vector3 vector4 = vector3 + vector;
				int num18;
				int num19;
				int num20;
				int num21;
				if (num7 != 0)
				{
					if ((num16 == num && num7 > 0) || (num16 == num3 && num7 < 0))
					{
						num18 = Mathf.Max((int)((vector4.x - 64f) / 64f + 135f), 0);
					}
					else
					{
						num18 = Mathf.Max(num16, 0);
					}
					if ((num16 == num && num7 < 0) || (num16 == num3 && num7 > 0))
					{
						num19 = Mathf.Min((int)((vector4.x + 64f) / 64f + 135f), 269);
					}
					else
					{
						num19 = Mathf.Min(num16, 269);
					}
					num20 = Mathf.Max((int)((Mathf.Min(vector2.z, vector4.z) - 64f) / 64f + 135f), 0);
					num21 = Mathf.Min((int)((Mathf.Max(vector2.z, vector4.z) + 64f) / 64f + 135f), 269);
				}
				else
				{
					if ((num17 == num2 && num8 > 0) || (num17 == num4 && num8 < 0))
					{
						num20 = Mathf.Max((int)((vector4.z - 64f) / 64f + 135f), 0);
					}
					else
					{
						num20 = Mathf.Max(num17, 0);
					}
					if ((num17 == num2 && num8 < 0) || (num17 == num4 && num8 > 0))
					{
						num21 = Mathf.Min((int)((vector4.z + 64f) / 64f + 135f), 269);
					}
					else
					{
						num21 = Mathf.Min(num17, 269);
					}
					num18 = Mathf.Max((int)((Mathf.Min(vector2.x, vector4.x) - 64f) / 64f + 135f), 0);
					num19 = Mathf.Min((int)((Mathf.Max(vector2.x, vector4.x) + 64f) / 64f + 135f), 269);
				}
				for (int i = num20; i <= num21; i++)
				{
					for (int j = num18; j <= num19; j++)
					{
						ushort num22 = this.m_nodeGrid[i * 270 + j];
						int num23 = 0;
						while (num22 != 0)
						{
							NetNode.Flags flags = this.m_nodes.m_buffer[(int)num22].m_flags;
							NetInfo info = this.m_nodes.m_buffer[(int)num22].Info;
							ItemClass connectionClass = info.GetConnectionClass();
							if ((((service == ItemClass.Service.None || connectionClass.m_service == service) && (subService == ItemClass.SubService.None || connectionClass.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (connectionClass.m_layer & itemLayers) != ItemClass.Layer.None)) || (info.m_intersectClass != null && (service == ItemClass.Service.None || info.m_intersectClass.m_service == service) && (subService == ItemClass.SubService.None || info.m_intersectClass.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_intersectClass.m_layer & itemLayers) != ItemClass.Layer.None)) || (info.m_netAI.CanIntersect(connectedType) && connectionClass.m_service == service2 && (subService2 == ItemClass.SubService.None || connectionClass.m_subService == subService2) && (itemLayers2 == ItemClass.Layer.None || (connectionClass.m_layer & itemLayers2) != ItemClass.Layer.None))) && (flags & ignoreNodeFlags) == NetNode.Flags.None && (connectedType == null || (info.m_netAI.CanConnect(connectedType) && connectedType.m_netAI.CanConnect(info))))
							{
								bool flag = false;
								if ((flags & (NetNode.Flags.Middle | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Untouchable) && this.m_nodes.m_buffer[(int)num22].CountSegments(NetSegment.Flags.Untouchable, 0) >= 2)
								{
									flag = true;
								}
								float num24;
								float num25;
								if (!flag && this.m_nodes.m_buffer[(int)num22].RayCast(ray, snapElevation, out num24, out num25) && (num25 < num12 || (num25 == num12 && num24 < num11)))
								{
									num11 = num24;
									num12 = num25;
									num14 = num22;
								}
							}
							num22 = this.m_nodes.m_buffer[(int)num22].m_nextGridNode;
							if (++num23 > 32768)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				for (int k = num20; k <= num21; k++)
				{
					for (int l = num18; l <= num19; l++)
					{
						ushort num26 = this.m_segmentGrid[k * 270 + l];
						int num27 = 0;
						while (num26 != 0)
						{
							NetSegment.Flags flags2 = this.m_segments.m_buffer[(int)num26].m_flags;
							NetInfo info2 = this.m_segments.m_buffer[(int)num26].Info;
							ItemClass connectionClass2 = info2.GetConnectionClass();
							if (((service == ItemClass.Service.None || connectionClass2.m_service == service) && (subService == ItemClass.SubService.None || connectionClass2.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (connectionClass2.m_layer & itemLayers) != ItemClass.Layer.None || nameOnly)) || (info2.m_intersectClass != null && (service == ItemClass.Service.None || info2.m_intersectClass.m_service == service) && (subService == ItemClass.SubService.None || info2.m_intersectClass.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info2.m_intersectClass.m_layer & itemLayers) != ItemClass.Layer.None || nameOnly)) || (info2.m_netAI.CanIntersect(connectedType) && connectionClass2.m_service == service2 && (subService2 == ItemClass.SubService.None || connectionClass2.m_subService == subService2) && (itemLayers2 == ItemClass.Layer.None || (connectionClass2.m_layer & itemLayers2) != ItemClass.Layer.None || nameOnly)))
							{
								bool flag2 = (flags2 & ignoreSegmentFlags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None && !nameOnly;
								flags2 &= ~NetSegment.Flags.Untouchable;
								float num28;
								float num29;
								if ((flags2 & ignoreSegmentFlags) == NetSegment.Flags.None && (connectedType == null || (info2.m_netAI.CanConnect(connectedType) && connectedType.m_netAI.CanConnect(info2))) && this.m_segments.m_buffer[(int)num26].RayCast(num26, ray, snapElevation, nameOnly, out num28, out num29) && (num29 < num10 || (num29 == num10 && num28 < num9)))
								{
									ushort startNode = this.m_segments.m_buffer[(int)num26].m_startNode;
									ushort endNode = this.m_segments.m_buffer[(int)num26].m_endNode;
									Vector3 position = this.m_nodes.m_buffer[(int)startNode].m_position;
									Vector3 position2 = this.m_nodes.m_buffer[(int)endNode].m_position;
									float num30 = this.m_nodes.m_buffer[(int)startNode].Info.GetMinNodeDistance();
									float num31 = this.m_nodes.m_buffer[(int)endNode].Info.GetMinNodeDistance();
									num10 = num29;
									num9 = num28;
									Vector3 vector5 = ray.a + (ray.b - ray.a) * num28;
									NetNode.Flags flags3 = this.m_nodes.m_buffer[(int)startNode].m_flags;
									if ((flags3 & NetNode.Flags.End) != NetNode.Flags.None)
									{
										flags3 &= ~NetNode.Flags.Moveable;
									}
									NetNode.Flags flags4 = this.m_nodes.m_buffer[(int)endNode].m_flags;
									if ((flags4 & NetNode.Flags.End) != NetNode.Flags.None)
									{
										flags4 &= ~NetNode.Flags.Moveable;
									}
									if (flag2)
									{
										num30 = 1000f;
										num31 = 1000f;
									}
									bool flag3 = (flags3 & (NetNode.Flags.Moveable | ignoreNodeFlags)) == NetNode.Flags.None;
									bool flag4 = (flags4 & (NetNode.Flags.Moveable | ignoreNodeFlags)) == NetNode.Flags.None;
									float num32 = VectorUtils.LengthSqrXZ(vector5 - position) / (num30 * num30);
									float num33 = VectorUtils.LengthSqrXZ(vector5 - position2) / (num31 * num31);
									if (flag3 && num32 < 1f && (!flag4 || num32 < num33) && !nameOnly)
									{
										bool flag5 = false;
										if ((flags3 & (NetNode.Flags.Middle | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Untouchable) && this.m_nodes.m_buffer[(int)startNode].CountSegments(NetSegment.Flags.Untouchable, 0) >= 2)
										{
											flag5 = true;
										}
										if (!flag5)
										{
											num13 = startNode;
											if (!flag2)
											{
												num15 = num26;
											}
										}
									}
									else if (flag4 && num33 < 1f && !nameOnly)
									{
										bool flag6 = false;
										if ((flags4 & (NetNode.Flags.Middle | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Untouchable) && this.m_nodes.m_buffer[(int)endNode].CountSegments(NetSegment.Flags.Untouchable, 0) >= 2)
										{
											flag6 = true;
										}
										if (!flag6)
										{
											num13 = endNode;
											if (!flag2)
											{
												num15 = num26;
											}
										}
									}
									else if (!flag2)
									{
										num13 = 0;
										num15 = num26;
									}
								}
							}
							num26 = this.m_segments.m_buffer[(int)num26].m_nextGridSegment;
							if (++num27 > 36864)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector2 = vector3;
				vector3 = vector4;
				num16 += num7;
				num17 += num8;
			}
			while ((num16 <= num3 || num7 <= 0) && (num16 >= num3 || num7 >= 0) && (num17 <= num4 || num8 <= 0) && (num17 >= num4 || num8 >= 0));
			if (num12 < num10 || (num12 == num10 && num11 < num9))
			{
				num9 = num11;
				num13 = num14;
			}
			if (num9 != 2f)
			{
				hit = ray.Position(num9);
				nodeIndex = num13;
				segmentIndex = num15;
				return true;
			}
		}
		hit = Vector3.get_zero();
		nodeIndex = 0;
		segmentIndex = 0;
		return false;
	}

	public void UpdateNodeNotifications(ushort node, Notification.Problem oldProblems, Notification.Problem newProblems)
	{
		if (this.m_nodes.m_buffer[(int)node].m_flags == NetNode.Flags.None)
		{
			return;
		}
		Vector3 position = this.m_nodes.m_buffer[(int)node].m_position;
		Notification.Problem problem = Notification.Problem.None;
		if ((newProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			if ((oldProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				problem = (Notification.Problem.FatalProblem | (oldProblems & ~newProblems));
			}
		}
		else if ((newProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
		{
			if ((oldProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
			{
				problem = (Notification.Problem.MajorProblem | (oldProblems & ~newProblems));
			}
		}
		else
		{
			problem = (oldProblems & ~newProblems);
		}
		if (problem != Notification.Problem.None)
		{
			NetInfo info = this.m_nodes.m_buffer[(int)node].Info;
			position.y += Mathf.Max(5f, info.m_maxHeight);
			Singleton<NotificationManager>.get_instance().AddRemovalEvent(problem, position, 1f);
		}
		int num = Mathf.Clamp((int)(position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(position.z / 64f + 135f), 0, 269);
		int x = num * 45 / 270;
		int z = num2 * 45 / 270;
		Singleton<RenderManager>.get_instance().UpdateGroup(x, z, Singleton<NotificationManager>.get_instance().m_notificationLayer);
	}

	public void UpdateSegmentNotifications(ushort segment, Notification.Problem oldProblems, Notification.Problem newProblems)
	{
		if (this.m_segments.m_buffer[(int)segment].m_flags == NetSegment.Flags.None)
		{
			return;
		}
		Notification.Problem problem = Notification.Problem.None;
		if ((newProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			if ((oldProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				problem = (Notification.Problem.FatalProblem | (oldProblems & ~newProblems));
			}
		}
		else if ((newProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
		{
			if ((oldProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
			{
				problem = (Notification.Problem.MajorProblem | (oldProblems & ~newProblems));
			}
		}
		else
		{
			problem = (oldProblems & ~newProblems);
		}
		if (problem != Notification.Problem.None)
		{
			Vector3 middlePosition = this.m_segments.m_buffer[(int)segment].m_middlePosition;
			NetInfo info = this.m_segments.m_buffer[(int)segment].Info;
			middlePosition.y += Mathf.Max(5f, info.m_maxHeight);
			Singleton<NotificationManager>.get_instance().AddRemovalEvent(problem, middlePosition, 1f);
		}
		ushort startNode = this.m_segments.m_buffer[(int)segment].m_startNode;
		ushort endNode = this.m_segments.m_buffer[(int)segment].m_endNode;
		Vector3 position = this.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = this.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		int num = Mathf.Clamp((int)(vector.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(vector.z / 64f + 135f), 0, 269);
		int x = num * 45 / 270;
		int z = num2 * 45 / 270;
		Singleton<RenderManager>.get_instance().UpdateGroup(x, z, Singleton<NotificationManager>.get_instance().m_notificationLayer);
	}

	public void AddTileNode(Vector3 position, ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			int num = Singleton<GameAreaManager>.get_instance().GetAreaIndex(position);
			if (num != -1)
			{
				num *= 22;
				int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
				if (publicSubServiceIndex != -1)
				{
					num += publicSubServiceIndex + 12;
				}
				else
				{
					num += publicServiceIndex;
				}
				this.m_tileNodesCount[num]++;
			}
		}
	}

	public int GetTileNodeCount(int x, int z, ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			int num = Singleton<GameAreaManager>.get_instance().GetTileIndex(x, z);
			if (num != -1)
			{
				num *= 22;
				int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
				if (publicSubServiceIndex != -1)
				{
					num += publicSubServiceIndex + 12;
				}
				else
				{
					num += publicServiceIndex;
				}
				return this.m_tileNodesCount[num];
			}
		}
		return 0;
	}

	public void AddServiceSegment(ushort segment, ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
			if (publicSubServiceIndex != -1)
			{
				this.m_serviceSegments[publicSubServiceIndex + 12].Add(segment);
			}
			else
			{
				this.m_serviceSegments[publicServiceIndex].Add(segment);
			}
		}
	}

	public void RemoveServiceSegment(ushort segment, ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
			if (publicSubServiceIndex != -1)
			{
				this.m_serviceSegments[publicSubServiceIndex + 12].Remove(segment);
			}
			else
			{
				this.m_serviceSegments[publicServiceIndex].Remove(segment);
			}
		}
	}

	public FastList<ushort> GetServiceSegments(ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex == -1)
		{
			return null;
		}
		int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
		if (publicSubServiceIndex != -1)
		{
			return this.m_serviceSegments[publicSubServiceIndex + 12];
		}
		return this.m_serviceSegments[publicServiceIndex];
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_nodesUpdated || this.m_segmentsUpdated)
		{
			bool flag = this.m_nodesUpdated;
			bool flag2 = this.m_segmentsUpdated;
			this.m_nodesUpdated = false;
			this.m_segmentsUpdated = false;
			for (int i = 0; i < 3; i++)
			{
				if (flag)
				{
					int num = this.m_updatedNodes.Length;
					for (int j = 0; j < num; j++)
					{
						ulong num2 = this.m_updatedNodes[j];
						if (num2 != 0uL)
						{
							for (int k = 0; k < 64; k++)
							{
								if ((num2 & 1uL << k) != 0uL)
								{
									ushort num3 = (ushort)(j << 6 | k);
									this.m_nodes.m_buffer[(int)num3].CalculateNode(num3);
								}
							}
						}
					}
				}
				if (flag2)
				{
					int num4 = this.m_updatedSegments.Length;
					for (int l = 0; l < num4; l++)
					{
						ulong num5 = this.m_updatedSegments[l];
						if (num5 != 0uL)
						{
							for (int m = 0; m < 64; m++)
							{
								if ((num5 & 1uL << m) != 0uL)
								{
									ushort num6 = (ushort)(l << 6 | m);
									this.m_segments.m_buffer[(int)num6].CalculateSegment(num6);
								}
							}
						}
					}
				}
				if (flag2)
				{
					int num7 = this.m_updatedSegments.Length;
					for (int n = 0; n < num7; n++)
					{
						ulong num8 = this.m_updatedSegments[n];
						if (num8 != 0uL)
						{
							for (int num9 = 0; num9 < 64; num9++)
							{
								if ((num8 & 1uL << num9) != 0uL)
								{
									ushort num10 = (ushort)(n << 6 | num9);
									this.m_segments.m_buffer[(int)num10].UpdateBounds(num10);
									this.m_segments.m_buffer[(int)num10].UpdateLanes(num10, false);
								}
							}
						}
					}
				}
				if (flag)
				{
					int num11 = this.m_updatedNodes.Length;
					for (int num12 = 0; num12 < num11; num12++)
					{
						ulong num13 = this.m_updatedNodes[num12];
						if (num13 != 0uL)
						{
							for (int num14 = 0; num14 < 64; num14++)
							{
								if ((num13 & 1uL << num14) != 0uL)
								{
									ushort num15 = (ushort)(num12 << 6 | num14);
									this.m_nodes.m_buffer[(int)num15].UpdateBounds(num15);
									this.m_nodes.m_buffer[(int)num15].UpdateLaneConnection(num15);
								}
							}
						}
					}
				}
				if (!this.m_nodesUpdated && !this.m_segmentsUpdated)
				{
					break;
				}
				if (this.m_nodesUpdated)
				{
					flag = true;
					this.m_nodesUpdated = false;
				}
				if (this.m_segmentsUpdated)
				{
					flag2 = true;
					this.m_segmentsUpdated = false;
				}
			}
			if (flag)
			{
				int num16 = this.m_updatedNodes.Length;
				for (int num17 = 0; num17 < num16; num17++)
				{
					ulong num18 = this.m_updatedNodes[num17];
					if (num18 != 0uL)
					{
						this.m_updatedNodes[num17] = 0uL;
						for (int num19 = 0; num19 < 64; num19++)
						{
							if ((num18 & 1uL << num19) != 0uL)
							{
								ushort num20 = (ushort)(num17 << 6 | num19);
								Notification.Problem problems = this.m_nodes.m_buffer[(int)num20].m_problems;
								this.m_nodes.m_buffer[(int)num20].UpdateNode(num20);
								Notification.Problem problems2 = this.m_nodes.m_buffer[(int)num20].m_problems;
								if (problems2 != problems)
								{
									this.UpdateNodeNotifications(num20, problems, problems2);
								}
								this.UpdateNodeFlags(num20);
								this.UpdateNodeRenderer(num20, true);
							}
						}
					}
				}
			}
			if (flag2)
			{
				int num21 = this.m_updatedSegments.Length;
				for (int num22 = 0; num22 < num21; num22++)
				{
					ulong num23 = this.m_updatedSegments[num22];
					if (num23 != 0uL)
					{
						this.m_updatedSegments[num22] = 0uL;
						for (int num24 = 0; num24 < 64; num24++)
						{
							if ((num23 & 1uL << num24) != 0uL)
							{
								ushort num25 = (ushort)(num22 << 6 | num24);
								this.m_segments.m_buffer[(int)num25].UpdateSegment(num25);
								this.UpdateSegmentFlags(num25);
								this.UpdateSegmentRenderer(num25, true);
							}
						}
					}
				}
			}
		}
		if (this.m_updateNameVisibility.Count != 0)
		{
			this.UpdateNameVisibilities();
		}
		if (subStep != 0)
		{
			bool flag3 = false;
			int num26 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num27 = num26 * 144;
			int num28 = (num26 + 1) * 144 - 1;
			int num29 = 0;
			for (int num30 = num27; num30 <= num28; num30++)
			{
				if ((this.m_segments.m_buffer[num30].m_flags & NetSegment.Flags.Created) != NetSegment.Flags.None)
				{
					NetInfo info = this.m_segments.m_buffer[num30].Info;
					Notification.Problem problems3 = this.m_segments.m_buffer[(int)((ushort)num30)].m_problems;
					info.m_netAI.SimulationStep((ushort)num30, ref this.m_segments.m_buffer[num30]);
					int wetness = (int)this.m_segments.m_buffer[num30].m_wetness;
					if (wetness > num29)
					{
						num29 = wetness;
					}
					Notification.Problem problems4 = this.m_segments.m_buffer[(int)((ushort)num30)].m_problems;
					if (problems4 != problems3)
					{
						this.UpdateSegmentNotifications((ushort)num30, problems3, problems4);
					}
				}
				flag3 = true;
			}
			if (flag3)
			{
				InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
				switch (currentMode)
				{
				case InfoManager.InfoMode.None:
					if (this.m_wetnessChanged != 0)
					{
						this.UpdateSegmentColors((ushort)num27, (ushort)num28);
					}
					goto IL_623;
				case InfoManager.InfoMode.Electricity:
				case InfoManager.InfoMode.Water:
				case InfoManager.InfoMode.NoisePollution:
				case InfoManager.InfoMode.Pollution:
					goto IL_5F6;
				case InfoManager.InfoMode.CrimeRate:
				case InfoManager.InfoMode.Health:
				case InfoManager.InfoMode.Happiness:
				case InfoManager.InfoMode.Density:
				case InfoManager.InfoMode.Transport:
					IL_5D2:
					switch (currentMode)
					{
					case InfoManager.InfoMode.Heating:
					case InfoManager.InfoMode.Maintenance:
					case InfoManager.InfoMode.Snow:
						goto IL_5F6;
					default:
						if (currentMode != InfoManager.InfoMode.Traffic)
						{
							goto IL_623;
						}
						goto IL_5F6;
					}
					break;
				}
				goto IL_5D2;
				IL_5F6:
				this.UpdateSegmentColors((ushort)num27, (ushort)num28);
			}
			IL_623:
			if (num26 == 255)
			{
				this.m_lastMaxWetness = this.m_currentMaxWetness;
				this.m_currentMaxWetness = 0;
			}
			else if (num29 > this.m_currentMaxWetness)
			{
				this.m_currentMaxWetness = num29;
			}
		}
		if (subStep != 0)
		{
			bool flag4 = false;
			int num31 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num32 = num31 * 128;
			int num33 = (num31 + 1) * 128 - 1;
			for (int num34 = num32; num34 <= num33; num34++)
			{
				if ((this.m_nodes.m_buffer[num34].m_flags & NetNode.Flags.Created) != NetNode.Flags.None)
				{
					Notification.Problem problems5 = this.m_nodes.m_buffer[num34].m_problems;
					NetInfo info2 = this.m_nodes.m_buffer[num34].Info;
					info2.m_netAI.SimulationStep((ushort)num34, ref this.m_nodes.m_buffer[num34]);
					Notification.Problem problems6 = this.m_nodes.m_buffer[num34].m_problems;
					if (problems6 != problems5)
					{
						this.UpdateNodeNotifications((ushort)num34, problems5, problems6);
					}
				}
				flag4 = true;
			}
			if (flag4)
			{
				InfoManager.InfoMode currentMode2 = Singleton<InfoManager>.get_instance().CurrentMode;
				switch (currentMode2)
				{
				case InfoManager.InfoMode.Heating:
				case InfoManager.InfoMode.Maintenance:
				case InfoManager.InfoMode.Snow:
					break;
				default:
					switch (currentMode2)
					{
					case InfoManager.InfoMode.None:
						if (this.m_wetnessChanged != 0)
						{
							this.UpdateNodeColors((ushort)num32, (ushort)num33);
						}
						goto IL_7D6;
					case InfoManager.InfoMode.Electricity:
						IL_786:
						switch (currentMode2)
						{
						case InfoManager.InfoMode.NoisePollution:
						case InfoManager.InfoMode.Pollution:
							goto IL_7A9;
						case InfoManager.InfoMode.Transport:
							IL_79B:
							if (currentMode2 != InfoManager.InfoMode.Traffic)
							{
								goto IL_7D6;
							}
							goto IL_7A9;
						}
						goto IL_79B;
					case InfoManager.InfoMode.Water:
						goto IL_7A9;
					}
					goto IL_786;
				}
				IL_7A9:
				this.UpdateNodeColors((ushort)num32, (ushort)num33);
			}
		}
		IL_7D6:
		if (this.m_wetnessChanged != 0)
		{
			this.m_wetnessChanged--;
		}
		this.m_pathVisualizer.SimulationStep(subStep);
		this.m_netAdjust.SimulationStep(subStep);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep <= 1)
		{
			int num35 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num36 = num35 * PrefabCollection<NetInfo>.PrefabCount() >> 10;
			int num37 = ((num35 + 1) * PrefabCollection<NetInfo>.PrefabCount() >> 10) - 1;
			for (int num38 = num36; num38 <= num37; num38++)
			{
				NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)num38);
				if (prefab != null)
				{
					MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
					if (unlockMilestone != null)
					{
						Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
					}
					if (properties != null && Singleton<UnlockManager>.get_instance().Unlocked(prefab.m_class.m_service) && Singleton<UnlockManager>.get_instance().Unlocked(prefab.m_UnlockMilestone) && properties != null)
					{
						prefab.m_netAI.UpdateGuide(properties);
					}
				}
			}
		}
		if (subStep <= 1 && properties != null)
		{
			int num39 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (num39 != 189)
			{
				if (num39 != 217)
				{
					if (num39 == 75)
					{
						if (this.m_lastVisibleTrafficLightNode != 0)
						{
							this.m_yieldLights.Activate(properties.m_yieldLights, this.m_lastVisibleTrafficLightNode, Notification.Problem.None, true);
						}
					}
				}
				else if (this.m_lastVisibleRoadNameSegment != 0)
				{
					this.m_roadNames.Activate(properties.m_roadNames, this.m_lastVisibleRoadNameSegment, true);
				}
			}
			else
			{
				this.m_roadsNotUsed.Activate(properties.m_roadsNotUsed);
			}
		}
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<NetInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
				}
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<NetInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	public override bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_nodeGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					if (this.m_nodes.m_buffer[(int)num6].CalculateGroupData(num6, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
					num6 = this.m_nodes.m_buffer[(int)num6].m_nextGridNode;
					if (++num7 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = num2; k <= num4; k++)
		{
			for (int l = num; l <= num3; l++)
			{
				int num8 = k * 270 + l;
				ushort num9 = this.m_segmentGrid[num8];
				int num10 = 0;
				while (num9 != 0)
				{
					if (this.m_segments.m_buffer[(int)num9].CalculateGroupData(num9, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
					num9 = this.m_segments.m_buffer[(int)num9].m_nextGridSegment;
					if (++num10 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_nodeGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					this.m_nodes.m_buffer[(int)num6].PopulateGroupData(num6, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance, ref requireSurfaceMaps);
					num6 = this.m_nodes.m_buffer[(int)num6].m_nextGridNode;
					if (++num7 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = num2; k <= num4; k++)
		{
			for (int l = num; l <= num3; l++)
			{
				int num8 = k * 270 + l;
				ushort num9 = this.m_segmentGrid[num8];
				int num10 = 0;
				while (num9 != 0)
				{
					this.m_segments.m_buffer[(int)num9].PopulateGroupData(num9, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance, ref requireSurfaceMaps);
					num9 = this.m_segments.m_buffer[(int)num9].m_nextGridSegment;
					if (++num10 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetSegmentName(ushort segmentID, string name)
	{
		NetManager.<SetSegmentName>c__Iterator2 <SetSegmentName>c__Iterator = new NetManager.<SetSegmentName>c__Iterator2();
		<SetSegmentName>c__Iterator.segmentID = segmentID;
		<SetSegmentName>c__Iterator.name = name;
		<SetSegmentName>c__Iterator.$this = this;
		return <SetSegmentName>c__Iterator;
	}

	public bool SetSegmentNameImpl(ushort segmentID, string name)
	{
		NetSegment.Flags flags = this.m_segments.m_buffer[(int)segmentID].m_flags;
		if (segmentID != 0 && flags != NetSegment.Flags.None)
		{
			if (!StringExtensions.IsNullOrWhiteSpace(name) && name != this.GenerateSegmentName(segmentID))
			{
				this.m_segments.m_buffer[(int)segmentID].m_flags = (flags | NetSegment.Flags.CustomName);
				InstanceID id = default(InstanceID);
				id.NetSegment = segmentID;
				Singleton<InstanceManager>.get_instance().SetName(id, name);
				Singleton<GuideManager>.get_instance().m_renameNotUsed.Disable();
				this.UpdateSegmentRenderer(segmentID, false);
				this.m_updateNameVisibility.Add(segmentID);
				return true;
			}
			if ((flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
			{
				this.m_segments.m_buffer[(int)segmentID].m_flags = (flags & ~NetSegment.Flags.CustomName);
				InstanceID id2 = default(InstanceID);
				id2.NetSegment = segmentID;
				Singleton<InstanceManager>.get_instance().SetName(id2, null);
				this.UpdateSegmentRenderer(segmentID, false);
				this.m_updateNameVisibility.Add(segmentID);
			}
		}
		return false;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetPriorityRoad(ushort segmentID, bool priority)
	{
		NetManager.<SetPriorityRoad>c__Iterator3 <SetPriorityRoad>c__Iterator = new NetManager.<SetPriorityRoad>c__Iterator3();
		<SetPriorityRoad>c__Iterator.segmentID = segmentID;
		<SetPriorityRoad>c__Iterator.priority = priority;
		<SetPriorityRoad>c__Iterator.$this = this;
		return <SetPriorityRoad>c__Iterator;
	}

	private void SetPriorityRoadImpl(ushort segmentID, ushort ignoreSegment, bool priority)
	{
		if (segmentID != 0 && this.m_segments.m_buffer[(int)segmentID].m_flags != NetSegment.Flags.None)
		{
			NetInfo info = this.m_segments.m_buffer[(int)segmentID].Info;
			ushort startNode = this.m_segments.m_buffer[(int)segmentID].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)segmentID].m_endNode;
			Vector3 startDirection = this.m_segments.m_buffer[(int)segmentID].m_startDirection;
			Vector3 endDirection = this.m_segments.m_buffer[(int)segmentID].m_endDirection;
			if ((this.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = this.m_nodes.m_buffer[(int)startNode].GetSegment(i);
					if (segment != 0)
					{
						if (segment != ignoreSegment)
						{
							if (segmentID != segment)
							{
								NetInfo info2 = this.m_segments.m_buffer[(int)segment].Info;
								if (info.m_class.m_service == info2.m_class.m_service)
								{
									if (info.m_class.m_subService == info2.m_class.m_subService)
									{
										Vector3 direction = this.m_segments.m_buffer[(int)segment].GetDirection(startNode);
										if (startDirection.x * direction.x + startDirection.z * direction.z > -0.93f)
										{
											NetSegment.Flags flags = (this.m_segments.m_buffer[(int)segment].m_startNode != startNode) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
											if (priority)
											{
												NetSegment[] expr_1E7_cp_0 = this.m_segments.m_buffer;
												ushort expr_1E7_cp_1 = segment;
												expr_1E7_cp_0[(int)expr_1E7_cp_1].m_flags = (expr_1E7_cp_0[(int)expr_1E7_cp_1].m_flags | flags);
											}
											else
											{
												NetSegment[] expr_20C_cp_0 = this.m_segments.m_buffer;
												ushort expr_20C_cp_1 = segment;
												expr_20C_cp_0[(int)expr_20C_cp_1].m_flags = (expr_20C_cp_0[(int)expr_20C_cp_1].m_flags & ~flags);
											}
											this.m_segments.m_buffer[(int)segment].UpdateLanes(segment, true);
										}
									}
								}
							}
						}
					}
				}
			}
			if ((this.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = this.m_nodes.m_buffer[(int)endNode].GetSegment(j);
					if (segment2 != 0)
					{
						if (segment2 != ignoreSegment)
						{
							if (segmentID != segment2)
							{
								NetInfo info3 = this.m_segments.m_buffer[(int)segment2].Info;
								if (info.m_class.m_service == info3.m_class.m_service)
								{
									if (info.m_class.m_subService == info3.m_class.m_subService)
									{
										Vector3 direction2 = this.m_segments.m_buffer[(int)segment2].GetDirection(endNode);
										if (endDirection.x * direction2.x + endDirection.z * direction2.z > -0.93f)
										{
											NetSegment.Flags flags2 = (this.m_segments.m_buffer[(int)segment2].m_startNode != endNode) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
											if (priority)
											{
												NetSegment[] expr_395_cp_0 = this.m_segments.m_buffer;
												ushort expr_395_cp_1 = segment2;
												expr_395_cp_0[(int)expr_395_cp_1].m_flags = (expr_395_cp_0[(int)expr_395_cp_1].m_flags | flags2);
											}
											else
											{
												NetSegment[] expr_3BA_cp_0 = this.m_segments.m_buffer;
												ushort expr_3BA_cp_1 = segment2;
												expr_3BA_cp_0[(int)expr_3BA_cp_1].m_flags = (expr_3BA_cp_0[(int)expr_3BA_cp_1].m_flags & ~flags2);
											}
											this.m_segments.m_buffer[(int)segment2].UpdateLanes(segment2, true);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void UpdateNameVisibilities()
	{
		using (HashSet<ushort>.Enumerator enumerator = this.m_updateNameVisibility.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				this.UpdateNameVisibilityPass1(enumerator.Current);
			}
		}
		using (HashSet<ushort>.Enumerator enumerator2 = this.m_updateNameVisibility.GetEnumerator())
		{
			while (enumerator2.MoveNext())
			{
				this.UpdateNameVisibilityPass2(enumerator2.Current);
			}
		}
		this.m_updateNameVisibility.Clear();
	}

	private void UpdateNameVisibilityPass1(ushort segment)
	{
		ushort startNode = this.m_segments.m_buffer[(int)segment].m_startNode;
		ushort endNode = this.m_segments.m_buffer[(int)segment].m_endNode;
		float averageLength = this.m_segments.m_buffer[(int)segment].m_averageLength;
		bool flag = true;
		int num = 0;
		while (num < 8 && flag)
		{
			ushort segment2 = this.m_nodes.m_buffer[(int)startNode].GetSegment(num);
			if (segment2 != 0 && segment2 != segment && this.IsSameName(segment, segment2))
			{
				float averageLength2 = this.m_segments.m_buffer[(int)segment2].m_averageLength;
				if (averageLength2 > averageLength || (averageLength2 == averageLength && segment2 > segment))
				{
					flag = false;
				}
			}
			num++;
		}
		int num2 = 0;
		while (num2 < 8 && flag)
		{
			ushort segment3 = this.m_nodes.m_buffer[(int)endNode].GetSegment(num2);
			if (segment3 != 0 && segment3 != segment && this.IsSameName(segment, segment3))
			{
				float averageLength3 = this.m_segments.m_buffer[(int)segment3].m_averageLength;
				if (averageLength3 > averageLength || (averageLength3 == averageLength && segment3 > segment))
				{
					flag = false;
				}
			}
			num2++;
		}
		if (flag)
		{
			NetSegment[] expr_16A_cp_0 = this.m_segments.m_buffer;
			expr_16A_cp_0[(int)segment].m_flags = (expr_16A_cp_0[(int)segment].m_flags | NetSegment.Flags.NameVisible1);
		}
		else
		{
			NetSegment[] expr_191_cp_0 = this.m_segments.m_buffer;
			expr_191_cp_0[(int)segment].m_flags = (expr_191_cp_0[(int)segment].m_flags & ~NetSegment.Flags.NameVisible1);
		}
	}

	private void UpdateNameVisibilityPass2(ushort segment)
	{
		bool flag = (this.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.NameVisible1) != NetSegment.Flags.None;
		if (!flag)
		{
			ushort startNode = this.m_segments.m_buffer[(int)segment].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)segment].m_endNode;
			flag = true;
			int num = 0;
			while (num < 8 && flag)
			{
				ushort segment2 = this.m_nodes.m_buffer[(int)startNode].GetSegment(num);
				if (segment2 != 0 && segment2 != segment && this.IsSameName(segment, segment2) && (this.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.NameVisible1) != NetSegment.Flags.None)
				{
					flag = false;
				}
				num++;
			}
			int num2 = 0;
			while (num2 < 8 && flag)
			{
				ushort segment3 = this.m_nodes.m_buffer[(int)endNode].GetSegment(num2);
				if (segment3 != 0 && segment3 != segment && this.IsSameName(segment, segment3) && (this.m_segments.m_buffer[(int)segment3].m_flags & NetSegment.Flags.NameVisible1) != NetSegment.Flags.None)
				{
					flag = false;
				}
				num2++;
			}
			if (flag)
			{
				NetSegment[] expr_159_cp_0 = this.m_segments.m_buffer;
				expr_159_cp_0[(int)segment].m_flags = (expr_159_cp_0[(int)segment].m_flags | NetSegment.Flags.NameVisible1);
			}
		}
		if (flag != ((this.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.NameVisible2) != NetSegment.Flags.None))
		{
			if (flag)
			{
				NetSegment[] expr_1A9_cp_0 = this.m_segments.m_buffer;
				expr_1A9_cp_0[(int)segment].m_flags = (expr_1A9_cp_0[(int)segment].m_flags | NetSegment.Flags.NameVisible2);
			}
			else
			{
				NetSegment[] expr_1D0_cp_0 = this.m_segments.m_buffer;
				expr_1D0_cp_0[(int)segment].m_flags = (expr_1D0_cp_0[(int)segment].m_flags & ~NetSegment.Flags.NameVisible2);
			}
			this.UpdateSegmentRenderer(segment, false);
		}
	}

	public bool IsSameName(ushort segment1, ushort segment2)
	{
		NetInfo info = this.m_segments.m_buffer[(int)segment1].Info;
		NetInfo info2 = this.m_segments.m_buffer[(int)segment2].Info;
		if (info.m_class.m_service != info2.m_class.m_service)
		{
			return false;
		}
		if (info.m_class.m_subService != info2.m_class.m_subService)
		{
			return false;
		}
		bool flag = (this.m_segments.m_buffer[(int)segment1].m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None;
		bool flag2 = (this.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None;
		if (flag != flag2)
		{
			return false;
		}
		if (flag)
		{
			InstanceID id = default(InstanceID);
			id.NetSegment = segment1;
			string name = Singleton<InstanceManager>.get_instance().GetName(id);
			id.NetSegment = segment2;
			string name2 = Singleton<InstanceManager>.get_instance().GetName(id);
			return name == name2;
		}
		ushort nameSeed = this.m_segments.m_buffer[(int)segment1].m_nameSeed;
		ushort nameSeed2 = this.m_segments.m_buffer[(int)segment2].m_nameSeed;
		return nameSeed == nameSeed2;
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("NetManager.UpdateData");
		base.UpdateData(mode);
		int num = PrefabCollection<NetInfo>.PrefabCount();
		this.m_infoCount = num;
		for (int i = 1; i < 32768; i++)
		{
			if (this.m_nodes.m_buffer[i].m_flags != NetNode.Flags.None)
			{
				if (this.m_nodes.m_buffer[i].Info == null)
				{
					this.ReleaseNode((ushort)i);
				}
				else if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
				{
					NetNode[] expr_9C_cp_0 = this.m_nodes.m_buffer;
					int expr_9C_cp_1 = i;
					expr_9C_cp_0[expr_9C_cp_1].m_flags = (expr_9C_cp_0[expr_9C_cp_1].m_flags | NetNode.Flags.Original);
				}
			}
		}
		for (int j = 1; j < 36864; j++)
		{
			if (this.m_segments.m_buffer[j].m_flags != NetSegment.Flags.None)
			{
				if (this.m_segments.m_buffer[j].Info == null)
				{
					this.ReleaseSegment((ushort)j, false);
				}
				else if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
				{
					NetSegment[] expr_12F_cp_0 = this.m_segments.m_buffer;
					int expr_12F_cp_1 = j;
					expr_12F_cp_0[expr_12F_cp_1].m_flags = (expr_12F_cp_0[expr_12F_cp_1].m_flags | NetSegment.Flags.Original);
				}
			}
		}
		for (int k = 0; k < num; k++)
		{
			NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)k);
			if (prefab != null && prefab.m_UnlockMilestone != null)
			{
				switch (mode)
				{
				case SimulationManager.UpdateMode.NewMap:
				case SimulationManager.UpdateMode.LoadMap:
				case SimulationManager.UpdateMode.NewAsset:
				case SimulationManager.UpdateMode.LoadAsset:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.NewGameFromMap:
				case SimulationManager.UpdateMode.NewScenarioFromMap:
				case SimulationManager.UpdateMode.UpdateScenarioFromMap:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.LoadGame:
				case SimulationManager.UpdateMode.NewScenarioFromGame:
				case SimulationManager.UpdateMode.LoadScenario:
				case SimulationManager.UpdateMode.NewGameFromScenario:
				case SimulationManager.UpdateMode.UpdateScenarioFromGame:
					prefab.m_UnlockMilestone.Reset(true);
					break;
				}
			}
		}
		Randomizer randomizer = default(Randomizer);
		Singleton<VehicleManager>.get_instance().RefreshTransferVehicles();
		VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref randomizer, ItemClass.Service.Road, ItemClass.SubService.None, ItemClass.Level.Level4);
		this.m_treatWetAsSnow = (randomVehicleInfo != null && randomVehicleInfo.m_class.m_level == ItemClass.Level.Level4);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_outsideNodeNotConnected == null)
		{
			this.m_outsideNodeNotConnected = new NetNodeInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_transportNodeNotConnected == null)
		{
			this.m_transportNodeNotConnected = new NetNodeInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_shortRoadTraffic == null)
		{
			this.m_shortRoadTraffic = new NetSegmentInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_optionsNotUsed == null)
		{
			this.m_optionsNotUsed = new ServiceTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_manualUpgrade == null)
		{
			this.m_manualUpgrade = new ServiceTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_elevationNotUsed == null)
		{
			this.m_elevationNotUsed = new ServiceTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_upgradeExistingRoad == null)
		{
			this.m_upgradeExistingRoad = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadsNotUsed == null)
		{
			this.m_roadsNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_onewayRoadPlacement == null)
		{
			this.m_onewayRoadPlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_canalsNotUsed == null)
		{
			this.m_canalsNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_quaysNotUsed == null)
		{
			this.m_quaysNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_canalDemolished == null)
		{
			this.m_canalDemolished = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_upgradeWaterPipes == null)
		{
			this.m_upgradeWaterPipes = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadDestroyed == null)
		{
			this.m_roadDestroyed = new NetSegmentInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadDestroyed2 == null)
		{
			this.m_roadDestroyed2 = new ServiceTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadNames == null)
		{
			this.m_roadNames = new NetSegmentInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_yieldLights == null)
		{
			this.m_yieldLights = new NetNodeInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_visualAids == null)
		{
			this.m_visualAids = new GenericGuide();
		}
		if (this.m_pathVisualizer != null)
		{
			this.m_pathVisualizer.UpdateData();
		}
		if (this.m_netAdjust != null)
		{
			this.m_netAdjust.UpdateData();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("NetManager.LateUpdateData");
		base.LateUpdateData(mode);
		if (Singleton<SimulationManager>.get_instance().m_metaData.m_dataFormatVersion < 313u)
		{
			for (int i = 1; i < 36864; i++)
			{
				if (this.m_segments.m_buffer[i].m_flags != NetSegment.Flags.None)
				{
					this.m_segments.m_buffer[i].UpdateNameSeed((ushort)i);
				}
			}
			for (int j = 1; j < 36864; j++)
			{
				if (this.m_segments.m_buffer[j].m_flags != NetSegment.Flags.None)
				{
					this.UpdateNameVisibilityPass1((ushort)j);
				}
			}
			for (int k = 1; k < 36864; k++)
			{
				if (this.m_segments.m_buffer[k].m_flags != NetSegment.Flags.None)
				{
					this.UpdateNameVisibilityPass2((ushort)k);
				}
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new NetManager.Data());
	}

	public void UpdateSegmentColors(ushort segment)
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinSegment = Mathf.Min(this.m_colorUpdateMinSegment, (int)segment);
			this.m_colorUpdateMaxSegment = Mathf.Max(this.m_colorUpdateMaxSegment, (int)segment);
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateSegmentColors(ushort min, ushort max)
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinSegment = Mathf.Min(this.m_colorUpdateMinSegment, (int)min);
			this.m_colorUpdateMaxSegment = Mathf.Max(this.m_colorUpdateMaxSegment, (int)max);
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateSegmentColors()
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinSegment = 0;
			this.m_colorUpdateMaxSegment = 36863;
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateNodeColors(ushort node)
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinNode = Mathf.Min(this.m_colorUpdateMinNode, (int)node);
			this.m_colorUpdateMaxNode = Mathf.Max(this.m_colorUpdateMaxNode, (int)node);
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateNodeColors(ushort min, ushort max)
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinNode = Mathf.Min(this.m_colorUpdateMinNode, (int)min);
			this.m_colorUpdateMaxNode = Mathf.Max(this.m_colorUpdateMaxNode, (int)max);
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateNodeColors()
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMinNode = 0;
			this.m_colorUpdateMaxNode = 32767;
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void AddSmoothColor(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_newSmoothColors, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_newSmoothColors.Add(id);
		}
		finally
		{
			Monitor.Exit(this.m_newSmoothColors);
		}
	}

	public string GetSegmentName(ushort segmentID)
	{
		if (this.m_segments.m_buffer[(int)segmentID].m_flags != NetSegment.Flags.None)
		{
			string text = null;
			if ((this.m_segments.m_buffer[(int)segmentID].m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.NetSegment = segmentID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateSegmentName(segmentID);
			}
			return text;
		}
		return null;
	}

	public string GetDefaultSegmentName(ushort segmentID)
	{
		return this.GenerateSegmentName(segmentID);
	}

	private string GenerateSegmentName(ushort segmentID)
	{
		NetInfo info = this.m_segments.m_buffer[(int)segmentID].Info;
		if (info != null)
		{
			return info.m_netAI.GenerateName(segmentID, ref this.m_segments.m_buffer[(int)segmentID]);
		}
		return "Invalid";
	}

	public bool IsPriorityRoad(ushort segmentID, out bool canToggle)
	{
		bool flag = true;
		canToggle = false;
		NetSegment.Flags flags = this.m_segments.m_buffer[(int)segmentID].m_flags;
		if (segmentID != 0 && flags != NetSegment.Flags.None)
		{
			ushort num = segmentID;
			ushort num2 = this.m_segments.m_buffer[(int)segmentID].m_startNode;
			int num3 = 0;
			while (num2 != 0)
			{
				ushort num4 = 0;
				int num5 = 0;
				while (num5 < 8 && num4 == 0)
				{
					ushort segment = this.m_nodes.m_buffer[(int)num2].GetSegment(num5);
					if (segment != 0 && segment != num && segment != segmentID && this.IsSameName(segmentID, segment))
					{
						this.IsPriorityRoadImpl(segment, num, ref flag, ref canToggle);
						num = segment;
						num4 = this.m_segments.m_buffer[(int)segment].GetOtherNode(num2);
					}
					num5++;
				}
				num2 = num4;
				if (++num3 >= 36864)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			num = segmentID;
			num2 = this.m_segments.m_buffer[(int)segmentID].m_endNode;
			num3 = 0;
			while (num2 != 0)
			{
				ushort num6 = 0;
				int num7 = 0;
				while (num7 < 8 && num6 == 0)
				{
					ushort segment2 = this.m_nodes.m_buffer[(int)num2].GetSegment(num7);
					if (segment2 != 0 && segment2 != num && segment2 != segmentID && this.IsSameName(segmentID, segment2))
					{
						this.IsPriorityRoadImpl(segment2, num, ref flag, ref canToggle);
						num = segment2;
						num6 = this.m_segments.m_buffer[(int)segment2].GetOtherNode(num2);
					}
					num7++;
				}
				num2 = num6;
				if (++num3 >= 36864)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.IsPriorityRoadImpl(segmentID, 0, ref flag, ref canToggle);
		}
		return flag && canToggle;
	}

	private void IsPriorityRoadImpl(ushort segmentID, ushort ignoreSegment, ref bool result, ref bool canToggle)
	{
		if (segmentID != 0 && this.m_segments.m_buffer[(int)segmentID].m_flags != NetSegment.Flags.None)
		{
			NetInfo info = this.m_segments.m_buffer[(int)segmentID].Info;
			ushort startNode = this.m_segments.m_buffer[(int)segmentID].m_startNode;
			ushort endNode = this.m_segments.m_buffer[(int)segmentID].m_endNode;
			Vector3 startDirection = this.m_segments.m_buffer[(int)segmentID].m_startDirection;
			Vector3 endDirection = this.m_segments.m_buffer[(int)segmentID].m_endDirection;
			if ((this.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = this.m_nodes.m_buffer[(int)startNode].GetSegment(i);
					if (segment != 0)
					{
						if (segment != ignoreSegment)
						{
							if (segmentID != segment)
							{
								NetInfo info2 = this.m_segments.m_buffer[(int)segment].Info;
								if (info.m_class.m_service == info2.m_class.m_service)
								{
									if (info.m_class.m_subService == info2.m_class.m_subService)
									{
										Vector3 direction = this.m_segments.m_buffer[(int)segment].GetDirection(startNode);
										if (startDirection.x * direction.x + startDirection.z * direction.z > -0.93f)
										{
											NetSegment.Flags flags = (this.m_segments.m_buffer[(int)segment].m_startNode != startNode) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
											if ((this.m_segments.m_buffer[(int)segment].m_flags & flags) == NetSegment.Flags.None)
											{
												result = false;
											}
											canToggle = true;
										}
									}
								}
							}
						}
					}
				}
			}
			if ((this.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = this.m_nodes.m_buffer[(int)endNode].GetSegment(j);
					if (segment2 != 0)
					{
						if (segment2 != ignoreSegment)
						{
							if (segmentID != segment2)
							{
								NetInfo info3 = this.m_segments.m_buffer[(int)segment2].Info;
								if (info.m_class.m_service == info3.m_class.m_service)
								{
									if (info.m_class.m_subService == info3.m_class.m_subService)
									{
										Vector3 direction2 = this.m_segments.m_buffer[(int)segment2].GetDirection(endNode);
										if (endDirection.x * direction2.x + endDirection.z * direction2.z > -0.93f)
										{
											NetSegment.Flags flags2 = (this.m_segments.m_buffer[(int)segment2].m_startNode != endNode) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
											if ((this.m_segments.m_buffer[(int)segment2].m_flags & flags2) == NetSegment.Flags.None)
											{
												result = false;
											}
											canToggle = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const int MAX_NODE_COUNT = 32768;

	public const int MAX_SEGMENT_COUNT = 36864;

	public const int MAX_LANE_COUNT = 262144;

	public const int MAX_MAP_NODES = 16384;

	public const int MAX_MAP_SEGMENTS = 16384;

	public const int MAX_MAP_LANES = 131072;

	public const int MAX_ASSET_NODES = 1024;

	public const int MAX_ASSET_SEGMENTS = 1024;

	public const int MAX_ASSET_LANES = 8192;

	public const float NODEGRID_CELL_SIZE = 64f;

	public const int NODEGRID_RESOLUTION = 270;

	public int m_nodeCount;

	public int m_segmentCount;

	public int m_laneCount;

	public int m_infoCount;

	[NonSerialized]
	public Array16<NetNode> m_nodes;

	[NonSerialized]
	public Array16<NetSegment> m_segments;

	[NonSerialized]
	public Array32<NetLane> m_lanes;

	[NonSerialized]
	public ulong[] m_updatedNodes;

	[NonSerialized]
	public ulong[] m_updatedSegments;

	[NonSerialized]
	public bool m_nodesUpdated;

	[NonSerialized]
	public bool m_segmentsUpdated;

	[NonSerialized]
	public ushort[] m_nodeGrid;

	[NonSerialized]
	public ushort[] m_segmentGrid;

	[NonSerialized]
	public float[] m_angleBuffer;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_LeftMatrix;

	[NonSerialized]
	public int ID_RightMatrix;

	[NonSerialized]
	public int ID_LeftMatrixB;

	[NonSerialized]
	public int ID_RightMatrixB;

	[NonSerialized]
	public int ID_MeshScale;

	[NonSerialized]
	public int ID_ObjectIndex;

	[NonSerialized]
	public int ID_CenterPos;

	[NonSerialized]
	public int ID_SideScale;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_SurfaceTexA;

	[NonSerialized]
	public int ID_SurfaceTexB;

	[NonSerialized]
	public int ID_SurfaceMapping;

	[NonSerialized]
	public int ID_MainTex;

	[NonSerialized]
	public int ID_XYSMap;

	[NonSerialized]
	public int ID_APRMap;

	[NonSerialized]
	public int ID_LeftMatrices;

	[NonSerialized]
	public int ID_RightMatrices;

	[NonSerialized]
	public int ID_LeftMatricesB;

	[NonSerialized]
	public int ID_RightMatricesB;

	[NonSerialized]
	public int ID_MeshScales;

	[NonSerialized]
	public int ID_ObjectIndices;

	[NonSerialized]
	public int ID_CenterPositions;

	[NonSerialized]
	public int ID_SideScales;

	[NonSerialized]
	public int ID_MeshLocations;

	[NonSerialized]
	public NetNodeInstanceGuide m_outsideNodeNotConnected;

	[NonSerialized]
	public NetNodeInstanceGuide m_transportNodeNotConnected;

	[NonSerialized]
	public NetSegmentInstanceGuide m_shortRoadTraffic;

	[NonSerialized]
	public ServiceTypeGuide m_optionsNotUsed;

	[NonSerialized]
	public ServiceTypeGuide m_elevationNotUsed;

	[NonSerialized]
	public ServiceTypeGuide m_manualUpgrade;

	[NonSerialized]
	public GenericGuide m_upgradeExistingRoad;

	[NonSerialized]
	public GenericGuide m_roadsNotUsed;

	[NonSerialized]
	public GenericGuide m_onewayRoadPlacement;

	[NonSerialized]
	public GenericGuide m_canalsNotUsed;

	[NonSerialized]
	public GenericGuide m_quaysNotUsed;

	[NonSerialized]
	public GenericGuide m_canalDemolished;

	[NonSerialized]
	public GenericGuide m_upgradeWaterPipes;

	[NonSerialized]
	public NetSegmentInstanceGuide m_roadDestroyed;

	[NonSerialized]
	public ServiceTypeGuide m_roadDestroyed2;

	[NonSerialized]
	public NetSegmentInstanceGuide m_roadNames;

	[NonSerialized]
	public NetNodeInstanceGuide m_yieldLights;

	[NonSerialized]
	public GenericGuide m_visualAids;

	[NonSerialized]
	public bool m_treatWetAsSnow;

	[NonSerialized]
	public int m_wetnessChanged;

	[NonSerialized]
	public int m_lastMaxWetness;

	[NonSerialized]
	public int m_currentMaxWetness;

	[NonSerialized]
	public FastList<ushort> m_tempNodeBuffer;

	[NonSerialized]
	public FastList<ushort> m_tempSegmentBuffer;

	[NonSerialized]
	public FastList<uint> m_nameInstanceBuffer;

	[NonSerialized]
	public HashSet<ushort> m_updateNameVisibility;

	[NonSerialized]
	public ulong[] m_adjustedSegments;

	[NonSerialized]
	public int m_arrowLayer;

	[NonSerialized]
	public bool m_roadNamesVisibleSetting;

	[NonSerialized]
	public ushort m_visibleRoadNameSegment;

	[NonSerialized]
	public ushort m_lastVisibleRoadNameSegment;

	[NonSerialized]
	public ushort m_visibleTrafficLightNode;

	[NonSerialized]
	public ushort m_lastVisibleTrafficLightNode;

	public Texture2D m_lodRgbAtlas;

	public Texture2D m_lodXysAtlas;

	public Texture2D m_lodAprAtlas;

	private FastList<ushort>[] m_serviceSegments;

	private HashSet<InstanceID> m_smoothColors;

	private FastList<InstanceID> m_removeSmoothColors;

	private FastList<InstanceID> m_newSmoothColors;

	private int m_roadLayer;

	private int m_colorUpdateMinSegment;

	private int m_colorUpdateMaxSegment;

	private int m_colorUpdateMinNode;

	private int m_colorUpdateMaxNode;

	private object m_colorUpdateLock;

	private int[] m_tileNodesCount;

	private bool m_renderDirectionArrows;

	private bool m_renderDirectionArrowsInfo;

	private bool m_roadNamesVisible;

	private float m_roadNameAlpha;

	private int m_nameModifiedCount;

	private Camera m_mainCamera;

	private Camera m_undergroundCamera;

	private PathVisualizer m_pathVisualizer;

	private NetAdjust m_netAdjust;

	private Material m_nameMaterial;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "NetManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			NetManager instance2 = Singleton<NetManager>.get_instance();
			NetLane[] buffer = instance2.m_lanes.m_buffer;
			NetNode[] buffer2 = instance2.m_nodes.m_buffer;
			NetSegment[] buffer3 = instance2.m_segments.m_buffer;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			int num3 = buffer3.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uShort.Write(buffer[i].m_flags);
			}
			uShort.EndWrite();
			for (int j = 1; j < num; j++)
			{
				if (buffer[j].m_flags != 0)
				{
					s.WriteUInt24(buffer[j].m_nextLane);
				}
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != 0)
				{
					@byte.Write(buffer[k].m_reservedPrev);
				}
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != 0)
				{
					byte2.Write(buffer[l].m_reservedNext);
				}
			}
			byte2.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int m = 1; m < num; m++)
			{
				if (buffer[m].m_flags != 0)
				{
					byte3.Write(buffer[m].m_reservedFrame);
				}
			}
			byte3.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int n = 1; n < num; n++)
			{
				if (buffer[n].m_flags != 0)
				{
					uShort2.Write(buffer[n].m_lastReserveID);
				}
			}
			uShort2.EndWrite();
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int num4 = 1; num4 < num2; num4++)
			{
				uInt.Write((uint)buffer2[num4].m_flags);
			}
			uInt.EndWrite();
			EncodedArray.ULong uLong = EncodedArray.ULong.BeginWrite(s);
			for (int num5 = 1; num5 < num2; num5++)
			{
				if (buffer2[num5].m_flags != NetNode.Flags.None)
				{
					uLong.Write((ulong)buffer2[num5].m_problems);
				}
			}
			uLong.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int num6 = 1; num6 < num2; num6++)
			{
				if (buffer2[num6].m_flags != NetNode.Flags.None)
				{
					byte4.Write(buffer2[num6].m_maxWaitTime);
				}
			}
			byte4.EndWrite();
			EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginWrite(s);
			for (int num7 = 1; num7 < num2; num7++)
			{
				if (buffer2[num7].m_flags != NetNode.Flags.None)
				{
					uShort3.Write(buffer2[num7].m_tempCounter);
					uShort3.Write(buffer2[num7].m_finalCounter);
				}
			}
			uShort3.EndWrite();
			for (int num8 = 1; num8 < num2; num8++)
			{
				if (buffer2[num8].m_flags != NetNode.Flags.None)
				{
					s.WriteVector3(buffer2[num8].m_position);
					int num9 = 0;
					for (int num10 = 0; num10 < 8; num10++)
					{
						if (buffer2[num8].GetSegment(num10) != 0)
						{
							num9++;
						}
					}
					s.WriteUInt8((uint)num9);
					for (int num11 = 0; num11 < 8; num11++)
					{
						ushort segment = buffer2[num8].GetSegment(num11);
						if (segment != 0)
						{
							s.WriteUInt16((uint)segment);
						}
					}
					s.WriteUInt32(buffer2[num8].m_buildIndex);
					s.WriteUInt16((uint)buffer2[num8].m_building);
					s.WriteUInt16((uint)buffer2[num8].m_nextBuildingNode);
					s.WriteUInt24(buffer2[num8].m_lane);
					s.WriteUInt8((uint)buffer2[num8].m_laneOffset);
					s.WriteUInt8((uint)buffer2[num8].m_elevation);
					s.WriteUInt8((uint)buffer2[num8].m_heightOffset);
					s.WriteUInt8((uint)buffer2[num8].m_connectCount);
				}
			}
			EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginWrite(s);
			for (int num12 = 1; num12 < num3; num12++)
			{
				uInt2.Write((uint)buffer3[num12].m_flags);
			}
			uInt2.EndWrite();
			EncodedArray.ULong uLong2 = EncodedArray.ULong.BeginWrite(s);
			for (int num13 = 1; num13 < num3; num13++)
			{
				if (buffer3[num13].m_flags != NetSegment.Flags.None)
				{
					uLong2.Write((ulong)buffer3[num13].m_problems);
				}
			}
			uLong2.EndWrite();
			EncodedArray.Byte byte5 = EncodedArray.Byte.BeginWrite(s);
			for (int num14 = 1; num14 < num3; num14++)
			{
				if (buffer3[num14].m_flags != NetSegment.Flags.None)
				{
					byte5.Write(buffer3[num14].m_trafficLightState0);
					byte5.Write(buffer3[num14].m_trafficLightState1);
				}
			}
			byte5.EndWrite();
			EncodedArray.Byte byte6 = EncodedArray.Byte.BeginWrite(s);
			for (int num15 = 1; num15 < num3; num15++)
			{
				if (buffer3[num15].m_flags != NetSegment.Flags.None)
				{
					byte6.Write(buffer3[num15].m_fireCoverage);
				}
			}
			byte6.EndWrite();
			EncodedArray.Byte byte7 = EncodedArray.Byte.BeginWrite(s);
			for (int num16 = 1; num16 < num3; num16++)
			{
				if (buffer3[num16].m_flags != NetSegment.Flags.None)
				{
					byte7.Write(buffer3[num16].m_wetness);
				}
			}
			byte7.EndWrite();
			EncodedArray.Byte byte8 = EncodedArray.Byte.BeginWrite(s);
			for (int num17 = 1; num17 < num3; num17++)
			{
				if (buffer3[num17].m_flags != NetSegment.Flags.None)
				{
					byte8.Write(buffer3[num17].m_condition);
				}
			}
			byte8.EndWrite();
			EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginWrite(s);
			for (int num18 = 1; num18 < num3; num18++)
			{
				if (buffer3[num18].m_flags != NetSegment.Flags.None)
				{
					uShort4.Write(buffer3[num18].m_noiseBuffer);
				}
			}
			uShort4.EndWrite();
			EncodedArray.Byte byte9 = EncodedArray.Byte.BeginWrite(s);
			for (int num19 = 1; num19 < num3; num19++)
			{
				if (buffer3[num19].m_flags != NetSegment.Flags.None)
				{
					byte9.Write(buffer3[num19].m_noiseDensity);
				}
			}
			byte9.EndWrite();
			for (int num20 = 1; num20 < num3; num20++)
			{
				if (buffer3[num20].m_flags != NetSegment.Flags.None)
				{
					s.WriteUInt32(buffer3[num20].m_buildIndex);
					s.WriteUInt32(buffer3[num20].m_modifiedIndex);
					s.WriteUInt24(buffer3[num20].m_lanes);
					s.WriteUInt24(buffer3[num20].m_path);
					s.WriteVector3(buffer3[num20].m_startDirection);
					s.WriteVector3(buffer3[num20].m_endDirection);
					s.WriteUInt16((uint)buffer3[num20].m_startNode);
					s.WriteUInt16((uint)buffer3[num20].m_endNode);
					s.WriteUInt16((uint)buffer3[num20].m_blockStartLeft);
					s.WriteUInt16((uint)buffer3[num20].m_blockStartRight);
					s.WriteUInt16((uint)buffer3[num20].m_blockEndLeft);
					s.WriteUInt16((uint)buffer3[num20].m_blockEndRight);
					s.WriteUInt16((uint)buffer3[num20].m_trafficBuffer);
					s.WriteUInt8((uint)buffer3[num20].m_trafficDensity);
					s.WriteUInt16((uint)buffer3[num20].m_nameSeed);
				}
			}
			int num21 = PrefabCollection<NetInfo>.PrefabCount();
			uint num22 = 0u;
			uint num23 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int num24 = 0; num24 < num21; num24++)
				{
					NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)num24);
					if (prefab != null && prefab.m_notUsedGuide != null && prefab.m_prefabDataIndex != -1)
					{
						num22 += 1u;
					}
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num23 += 1u;
					}
				}
			}
			s.WriteUInt16(num22);
			s.WriteUInt16(num23);
			try
			{
				PrefabCollection<NetInfo>.BeginSerialize(s);
				for (int num25 = 1; num25 < num2; num25++)
				{
					if (buffer2[num25].m_flags != NetNode.Flags.None)
					{
						PrefabCollection<NetInfo>.Serialize((uint)buffer2[num25].m_infoIndex);
					}
				}
				for (int num26 = 1; num26 < num3; num26++)
				{
					if (buffer3[num26].m_flags != NetSegment.Flags.None)
					{
						PrefabCollection<NetInfo>.Serialize((uint)buffer3[num26].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int num27 = 0; num27 < num21; num27++)
					{
						NetInfo prefab2 = PrefabCollection<NetInfo>.GetPrefab((uint)num27);
						if (prefab2 != null && prefab2.m_notUsedGuide != null && prefab2.m_prefabDataIndex != -1)
						{
							PrefabCollection<NetInfo>.Serialize((uint)prefab2.m_prefabDataIndex);
						}
					}
					for (int num28 = 0; num28 < num21; num28++)
					{
						NetInfo prefab3 = PrefabCollection<NetInfo>.GetPrefab((uint)num28);
						if (prefab3 != null && prefab3.m_UnlockMilestone != null && prefab3.m_prefabDataIndex != -1)
						{
							PrefabCollection<NetInfo>.Serialize((uint)prefab3.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<NetInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int num29 = 0; num29 < num21; num29++)
				{
					NetInfo prefab4 = PrefabCollection<NetInfo>.GetPrefab((uint)num29);
					if (prefab4 != null && prefab4.m_notUsedGuide != null && prefab4.m_prefabDataIndex != -1)
					{
						s.WriteObject<NetTypeGuide>(prefab4.m_notUsedGuide);
					}
				}
				for (int num30 = 0; num30 < num21; num30++)
				{
					NetInfo prefab5 = PrefabCollection<NetInfo>.GetPrefab((uint)num30);
					if (prefab5 != null && prefab5.m_UnlockMilestone != null && prefab5.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab5.m_UnlockMilestone.GetData());
					}
				}
			}
			s.WriteObject<NetNodeInstanceGuide>(instance2.m_outsideNodeNotConnected);
			s.WriteObject<NetNodeInstanceGuide>(instance2.m_transportNodeNotConnected);
			s.WriteObject<ServiceTypeGuide>(instance2.m_optionsNotUsed);
			s.WriteObject<ServiceTypeGuide>(instance2.m_manualUpgrade);
			s.WriteObject<ServiceTypeGuide>(instance2.m_elevationNotUsed);
			s.WriteObject<GenericGuide>(instance2.m_upgradeExistingRoad);
			s.WriteObject<GenericGuide>(instance2.m_roadsNotUsed);
			s.WriteObject<GenericGuide>(instance2.m_onewayRoadPlacement);
			s.WriteObject<NetSegmentInstanceGuide>(instance2.m_shortRoadTraffic);
			s.WriteObject<GenericGuide>(instance2.m_canalsNotUsed);
			s.WriteObject<GenericGuide>(instance2.m_quaysNotUsed);
			s.WriteObject<GenericGuide>(instance2.m_canalDemolished);
			s.WriteObject<GenericGuide>(instance2.m_upgradeWaterPipes);
			s.WriteObject<NetSegmentInstanceGuide>(instance2.m_roadDestroyed);
			s.WriteObject<ServiceTypeGuide>(instance2.m_roadDestroyed2);
			s.WriteObject<NetSegmentInstanceGuide>(instance2.m_roadNames);
			s.WriteObject<NetNodeInstanceGuide>(instance2.m_yieldLights);
			s.WriteObject<GenericGuide>(instance2.m_visualAids);
			s.WriteUInt8((uint)instance2.m_lastMaxWetness);
			s.WriteUInt8((uint)instance2.m_currentMaxWetness);
			s.WriteUInt24((uint)instance2.m_nameModifiedCount);
			s.WriteUInt24((uint)instance2.m_adjustedSegments.Length);
			EncodedArray.ULong uLong3 = EncodedArray.ULong.BeginWrite(s);
			for (int num31 = 0; num31 < instance2.m_adjustedSegments.Length; num31++)
			{
				uLong3.Write(instance2.m_adjustedSegments[num31]);
			}
			uLong3.EndWrite();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "NetManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "NetManager");
			NetManager instance = Singleton<NetManager>.get_instance();
			NetLane[] buffer = instance.m_lanes.m_buffer;
			NetNode[] buffer2 = instance.m_nodes.m_buffer;
			NetSegment[] buffer3 = instance.m_segments.m_buffer;
			ushort[] nodeGrid = instance.m_nodeGrid;
			ushort[] segmentGrid = instance.m_segmentGrid;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			int num3 = buffer3.Length;
			int num4 = nodeGrid.Length;
			int num5 = segmentGrid.Length;
			instance.m_lanes.ClearUnused();
			instance.m_nodes.ClearUnused();
			instance.m_segments.ClearUnused();
			for (int i = 0; i < num4; i++)
			{
				nodeGrid[i] = 0;
			}
			for (int j = 0; j < num5; j++)
			{
				segmentGrid[j] = 0;
			}
			for (int k = 0; k < instance.m_serviceSegments.Length; k++)
			{
				instance.m_serviceSegments[k].Clear();
			}
			for (int l = 0; l < instance.m_tileNodesCount.Length; l++)
			{
				instance.m_tileNodesCount[l] = 0;
			}
			if (s.get_version() >= 24u)
			{
				EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
				for (int m = 1; m < num; m++)
				{
					buffer[m].m_flags = uShort.Read();
				}
				uShort.EndRead();
			}
			for (int n = 1; n < num; n++)
			{
				buffer[n].m_bezier = default(Bezier3);
				buffer[n].m_length = 0f;
				buffer[n].m_curve = 0f;
				buffer[n].m_segment = 0;
				buffer[n].m_nodes = 0;
				buffer[n].m_firstTarget = 0;
				buffer[n].m_lastTarget = 0;
				if (s.get_version() >= 24u && buffer[n].m_flags != 0)
				{
					buffer[n].m_nextLane = s.ReadUInt24();
					if (s.get_version() < 48u)
					{
						s.ReadUInt16();
					}
				}
				else
				{
					buffer[n].m_flags = 0;
					buffer[n].m_nextLane = 0u;
					instance.m_lanes.ReleaseItem((uint)n);
				}
			}
			if (s.get_version() >= 163u)
			{
				EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
				for (int num6 = 1; num6 < num; num6++)
				{
					if (buffer[num6].m_flags != 0)
					{
						buffer[num6].m_reservedPrev = @byte.Read();
					}
					else
					{
						buffer[num6].m_reservedPrev = 0;
					}
				}
				@byte.EndRead();
			}
			else
			{
				for (int num7 = 1; num7 < num; num7++)
				{
					buffer[num7].m_reservedPrev = 0;
				}
			}
			if (s.get_version() >= 163u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int num8 = 1; num8 < num; num8++)
				{
					if (buffer[num8].m_flags != 0)
					{
						buffer[num8].m_reservedNext = byte2.Read();
					}
					else
					{
						buffer[num8].m_reservedNext = 0;
					}
				}
				byte2.EndRead();
			}
			else
			{
				for (int num9 = 1; num9 < num; num9++)
				{
					buffer[num9].m_reservedNext = 0;
				}
			}
			if (s.get_version() >= 163u)
			{
				EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
				for (int num10 = 1; num10 < num; num10++)
				{
					if (buffer[num10].m_flags != 0)
					{
						buffer[num10].m_reservedFrame = byte3.Read();
					}
					else
					{
						buffer[num10].m_reservedFrame = 0;
					}
				}
				byte3.EndRead();
			}
			else
			{
				for (int num11 = 1; num11 < num; num11++)
				{
					buffer[num11].m_reservedFrame = 0;
				}
			}
			if (s.get_version() >= 206u)
			{
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
				for (int num12 = 1; num12 < num; num12++)
				{
					if (buffer[num12].m_flags != 0)
					{
						buffer[num12].m_lastReserveID = uShort2.Read();
					}
					else
					{
						buffer[num12].m_lastReserveID = 0;
					}
				}
				uShort2.EndRead();
			}
			else
			{
				for (int num13 = 1; num13 < num; num13++)
				{
					buffer[num13].m_lastReserveID = 0;
				}
			}
			if (s.get_version() >= 202u)
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int num14 = 1; num14 < num2; num14++)
				{
					buffer2[num14].m_flags = (NetNode.Flags)uInt.Read();
				}
				uInt.EndRead();
			}
			else
			{
				EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginRead(s);
				for (int num15 = 1; num15 < 16384; num15++)
				{
					buffer2[num15].m_flags = (NetNode.Flags)uInt2.Read();
					if (s.get_version() < 60u)
					{
						NetNode[] expr_548_cp_0 = buffer2;
						int expr_548_cp_1 = num15;
						expr_548_cp_0[expr_548_cp_1].m_flags = (expr_548_cp_0[expr_548_cp_1].m_flags & ~NetNode.Flags.Outside);
					}
				}
				for (int num16 = 16384; num16 < num2; num16++)
				{
					buffer2[num16].m_flags = NetNode.Flags.None;
				}
				uInt2.EndRead();
			}
			if (s.get_version() >= 221u && Singleton<SimulationManager>.get_instance().m_metaData.m_saveAppVersion >= BuildConfig.MakeVersionNumber(1u, 3u, 0u, BuildConfig.ReleaseType.Prototype, 1u, BuildConfig.BuildType.Unknown))
			{
				EncodedArray.ULong uLong = EncodedArray.ULong.BeginRead(s);
				for (int num17 = 1; num17 < num2; num17++)
				{
					if (buffer2[num17].m_flags != NetNode.Flags.None)
					{
						buffer2[num17].m_problems = (Notification.Problem)uLong.Read();
					}
					else
					{
						buffer2[num17].m_problems = Notification.Problem.None;
					}
				}
				uLong.EndRead();
			}
			else if (s.get_version() >= 61u)
			{
				EncodedArray.UInt uInt3 = EncodedArray.UInt.BeginRead(s);
				for (int num18 = 1; num18 < num2; num18++)
				{
					if (buffer2[num18].m_flags != NetNode.Flags.None)
					{
						ulong num19 = (ulong)uInt3.Read();
						num19 = ((num19 & (ulong)-1073741824) << 32 | (num19 & 1073741823uL));
						buffer2[num18].m_problems = (Notification.Problem)num19;
					}
					else
					{
						buffer2[num18].m_problems = Notification.Problem.None;
					}
				}
				uInt3.EndRead();
			}
			else
			{
				for (int num20 = 1; num20 < num2; num20++)
				{
					buffer2[num20].m_problems = Notification.Problem.None;
				}
			}
			if (s.get_version() >= 86u)
			{
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int num21 = 1; num21 < num2; num21++)
				{
					if (buffer2[num21].m_flags != NetNode.Flags.None)
					{
						buffer2[num21].m_maxWaitTime = byte4.Read();
					}
					else
					{
						buffer2[num21].m_maxWaitTime = 0;
					}
				}
				byte4.EndRead();
			}
			else
			{
				for (int num22 = 1; num22 < num2; num22++)
				{
					buffer2[num22].m_maxWaitTime = 0;
				}
			}
			if (s.get_version() >= 105u)
			{
				EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginRead(s);
				for (int num23 = 1; num23 < num2; num23++)
				{
					if (buffer2[num23].m_flags != NetNode.Flags.None)
					{
						buffer2[num23].m_tempCounter = uShort3.Read();
						buffer2[num23].m_finalCounter = uShort3.Read();
					}
					else
					{
						buffer2[num23].m_tempCounter = 0;
						buffer2[num23].m_finalCounter = 0;
					}
				}
				uShort3.EndRead();
			}
			else
			{
				for (int num24 = 1; num24 < num2; num24++)
				{
					buffer2[num24].m_tempCounter = 0;
					buffer2[num24].m_finalCounter = 0;
				}
			}
			for (int num25 = 1; num25 < num2; num25++)
			{
				buffer2[num25].m_segment0 = 0;
				buffer2[num25].m_segment1 = 0;
				buffer2[num25].m_segment2 = 0;
				buffer2[num25].m_segment3 = 0;
				buffer2[num25].m_segment4 = 0;
				buffer2[num25].m_segment5 = 0;
				buffer2[num25].m_segment6 = 0;
				buffer2[num25].m_segment7 = 0;
				buffer2[num25].m_nextGridNode = 0;
				buffer2[num25].m_nextLaneNode = 0;
				buffer2[num25].m_transportLine = 0;
				buffer2[num25].m_coverage = 0;
				if (buffer2[num25].m_flags != NetNode.Flags.None)
				{
					buffer2[num25].m_position = s.ReadVector3();
					int num26 = (int)s.ReadUInt8();
					for (int num27 = 0; num27 < num26; num27++)
					{
						buffer2[num25].AddSegment((ushort)s.ReadUInt16());
					}
					if (s.get_version() >= 45u)
					{
						buffer2[num25].m_buildIndex = s.ReadUInt32();
					}
					else
					{
						buffer2[num25].m_buildIndex = 0u;
					}
					if (s.get_version() >= 6u)
					{
						buffer2[num25].m_building = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer2[num25].m_building = 0;
					}
					if (s.get_version() >= 80u)
					{
						buffer2[num25].m_nextBuildingNode = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer2[num25].m_nextBuildingNode = 0;
					}
					if (s.get_version() >= 48u)
					{
						buffer2[num25].m_lane = s.ReadUInt24();
						buffer2[num25].m_laneOffset = (byte)s.ReadUInt8();
					}
					else
					{
						buffer2[num25].m_lane = 0u;
						buffer2[num25].m_laneOffset = 0;
					}
					if (s.get_version() >= 57u)
					{
						buffer2[num25].m_elevation = (byte)s.ReadUInt8();
					}
					else
					{
						buffer2[num25].m_elevation = 0;
					}
					if (s.get_version() >= 59u)
					{
						buffer2[num25].m_heightOffset = (byte)s.ReadUInt8();
					}
					else
					{
						buffer2[num25].m_heightOffset = 0;
					}
					if (s.get_version() >= 80u)
					{
						buffer2[num25].m_connectCount = (byte)s.ReadUInt8();
					}
					else
					{
						buffer2[num25].m_connectCount = 0;
					}
				}
				else
				{
					buffer2[num25].m_position = Vector3.get_zero();
					buffer2[num25].m_nextBuildingNode = 0;
					buffer2[num25].m_buildIndex = 0u;
					buffer2[num25].m_building = 0;
					buffer2[num25].m_lane = 0u;
					buffer2[num25].m_laneOffset = 0;
					buffer2[num25].m_elevation = 0;
					buffer2[num25].m_heightOffset = 0;
					buffer2[num25].m_connectCount = 0;
					instance.m_nodes.ReleaseItem((ushort)num25);
				}
			}
			if (s.get_version() >= 222u)
			{
				EncodedArray.UInt uInt4 = EncodedArray.UInt.BeginRead(s);
				for (int num28 = 1; num28 < num3; num28++)
				{
					buffer3[num28].m_flags = (NetSegment.Flags)uInt4.Read();
				}
				uInt4.EndRead();
			}
			else
			{
				EncodedArray.UInt uInt5 = EncodedArray.UInt.BeginRead(s);
				for (int num29 = 1; num29 < 32768; num29++)
				{
					buffer3[num29].m_flags = (NetSegment.Flags)uInt5.Read();
				}
				for (int num30 = 32768; num30 < num3; num30++)
				{
					buffer3[num30].m_flags = NetSegment.Flags.None;
				}
				uInt5.EndRead();
			}
			if (s.get_version() >= 226u)
			{
				EncodedArray.ULong uLong2 = EncodedArray.ULong.BeginRead(s);
				for (int num31 = 1; num31 < num3; num31++)
				{
					if (buffer3[num31].m_flags != NetSegment.Flags.None)
					{
						buffer3[num31].m_problems = (Notification.Problem)uLong2.Read();
					}
					else
					{
						buffer3[num31].m_problems = Notification.Problem.None;
					}
				}
				uLong2.EndRead();
			}
			else
			{
				for (int num32 = 1; num32 < num3; num32++)
				{
					buffer3[num32].m_problems = Notification.Problem.None;
				}
			}
			if (s.get_version() >= 103u)
			{
				EncodedArray.Byte byte5 = EncodedArray.Byte.BeginRead(s);
				for (int num33 = 1; num33 < num3; num33++)
				{
					if (buffer3[num33].m_flags != NetSegment.Flags.None)
					{
						buffer3[num33].m_trafficLightState0 = byte5.Read();
						buffer3[num33].m_trafficLightState1 = byte5.Read();
					}
					else
					{
						buffer3[num33].m_trafficLightState0 = 0;
						buffer3[num33].m_trafficLightState1 = 0;
					}
				}
				byte5.EndRead();
			}
			else
			{
				for (int num34 = 1; num34 < num3; num34++)
				{
					buffer3[num34].m_trafficLightState0 = 0;
					buffer3[num34].m_trafficLightState1 = 0;
				}
			}
			if (s.get_version() >= 185u)
			{
				EncodedArray.Byte byte6 = EncodedArray.Byte.BeginRead(s);
				for (int num35 = 1; num35 < num3; num35++)
				{
					if (buffer3[num35].m_flags != NetSegment.Flags.None)
					{
						buffer3[num35].m_fireCoverage = byte6.Read();
					}
					else
					{
						buffer3[num35].m_fireCoverage = 0;
					}
				}
				byte6.EndRead();
			}
			else
			{
				for (int num36 = 1; num36 < num3; num36++)
				{
					buffer3[num36].m_fireCoverage = 0;
				}
			}
			if (s.get_version() >= 224u)
			{
				EncodedArray.Byte byte7 = EncodedArray.Byte.BeginRead(s);
				for (int num37 = 1; num37 < num3; num37++)
				{
					if (buffer3[num37].m_flags != NetSegment.Flags.None)
					{
						buffer3[num37].m_wetness = byte7.Read();
					}
					else
					{
						buffer3[num37].m_wetness = 0;
					}
				}
				byte7.EndRead();
			}
			else
			{
				for (int num38 = 1; num38 < num3; num38++)
				{
					buffer3[num38].m_wetness = 0;
				}
			}
			if (s.get_version() >= 229u)
			{
				EncodedArray.Byte byte8 = EncodedArray.Byte.BeginRead(s);
				for (int num39 = 1; num39 < num3; num39++)
				{
					if (buffer3[num39].m_flags != NetSegment.Flags.None)
					{
						buffer3[num39].m_condition = byte8.Read();
					}
					else
					{
						buffer3[num39].m_condition = 0;
					}
				}
				byte8.EndRead();
			}
			else
			{
				for (int num40 = 1; num40 < num3; num40++)
				{
					buffer3[num40].m_condition = 0;
				}
			}
			if (s.get_version() >= 109007u)
			{
				EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginRead(s);
				for (int num41 = 1; num41 < num3; num41++)
				{
					if (buffer3[num41].m_flags != NetSegment.Flags.None)
					{
						buffer3[num41].m_noiseBuffer = uShort4.Read();
					}
					else
					{
						buffer3[num41].m_noiseBuffer = 0;
					}
				}
				uShort4.EndRead();
			}
			else
			{
				for (int num42 = 1; num42 < num3; num42++)
				{
					buffer3[num42].m_noiseBuffer = 0;
				}
			}
			if (s.get_version() >= 109007u)
			{
				EncodedArray.Byte byte9 = EncodedArray.Byte.BeginRead(s);
				for (int num43 = 1; num43 < num3; num43++)
				{
					if (buffer3[num43].m_flags != NetSegment.Flags.None)
					{
						buffer3[num43].m_noiseDensity = byte9.Read();
					}
					else
					{
						buffer3[num43].m_noiseDensity = 0;
					}
				}
				byte9.EndRead();
			}
			else
			{
				for (int num44 = 1; num44 < num3; num44++)
				{
					buffer3[num44].m_noiseDensity = 0;
				}
			}
			for (int num45 = 1; num45 < num3; num45++)
			{
				buffer3[num45].m_startLeftSegment = 0;
				buffer3[num45].m_startRightSegment = 0;
				buffer3[num45].m_endLeftSegment = 0;
				buffer3[num45].m_endRightSegment = 0;
				buffer3[num45].m_averageLength = 0f;
				buffer3[num45].m_nextGridSegment = 0;
				buffer3[num45].m_cornerAngleStart = 0;
				buffer3[num45].m_cornerAngleEnd = 0;
				if (buffer3[num45].m_flags != NetSegment.Flags.None)
				{
					NetSegment[] expr_1121_cp_0 = buffer3;
					int expr_1121_cp_1 = num45;
					expr_1121_cp_0[expr_1121_cp_1].m_flags = (expr_1121_cp_0[expr_1121_cp_1].m_flags & ~NetSegment.Flags.PathLength);
					buffer3[num45].m_buildIndex = s.ReadUInt32();
					if (s.get_version() >= 46u)
					{
						buffer3[num45].m_modifiedIndex = s.ReadUInt32();
					}
					else
					{
						buffer3[num45].m_modifiedIndex = buffer3[num45].m_buildIndex;
					}
					if (s.get_version() >= 24u)
					{
						buffer3[num45].m_lanes = s.ReadUInt24();
					}
					else
					{
						buffer3[num45].m_lanes = 0u;
					}
					if (s.get_version() >= 50u)
					{
						buffer3[num45].m_path = s.ReadUInt24();
					}
					else
					{
						buffer3[num45].m_path = 0u;
					}
					buffer3[num45].m_startDirection = s.ReadVector3();
					buffer3[num45].m_endDirection = s.ReadVector3();
					buffer3[num45].m_startNode = (ushort)s.ReadUInt16();
					buffer3[num45].m_endNode = (ushort)s.ReadUInt16();
					buffer3[num45].m_blockStartLeft = (ushort)s.ReadUInt16();
					buffer3[num45].m_blockStartRight = (ushort)s.ReadUInt16();
					buffer3[num45].m_blockEndLeft = (ushort)s.ReadUInt16();
					buffer3[num45].m_blockEndRight = (ushort)s.ReadUInt16();
					if (s.get_version() >= 77u)
					{
						buffer3[num45].m_trafficBuffer = (ushort)s.ReadUInt16();
						buffer3[num45].m_trafficDensity = (byte)s.ReadUInt8();
					}
					else
					{
						buffer3[num45].m_trafficBuffer = 0;
						buffer3[num45].m_trafficDensity = 0;
					}
					if (s.get_version() >= 312u)
					{
						buffer3[num45].m_nameSeed = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer3[num45].m_nameSeed = 0;
					}
					if (s.get_version() < 109007u)
					{
						buffer3[num45].m_noiseDensity = buffer3[num45].m_trafficDensity;
					}
				}
				else
				{
					buffer3[num45].m_buildIndex = 0u;
					buffer3[num45].m_modifiedIndex = 0u;
					buffer3[num45].m_startDirection = Vector3.get_zero();
					buffer3[num45].m_endDirection = Vector3.get_zero();
					buffer3[num45].m_startNode = 0;
					buffer3[num45].m_endNode = 0;
					buffer3[num45].m_blockStartLeft = 0;
					buffer3[num45].m_blockStartRight = 0;
					buffer3[num45].m_blockEndLeft = 0;
					buffer3[num45].m_blockEndRight = 0;
					buffer3[num45].m_lanes = 0u;
					buffer3[num45].m_path = 0u;
					buffer3[num45].m_trafficBuffer = 0;
					buffer3[num45].m_trafficDensity = 0;
					buffer3[num45].m_nameSeed = 0;
					instance.m_segments.ReleaseItem((ushort)num45);
				}
			}
			uint num46 = 0u;
			uint num47 = 0u;
			if (s.get_version() >= 128u)
			{
				num46 = s.ReadUInt16();
				this.m_guideInfoIndex = new uint[num46];
				this.m_netNotUsedGuides = new NetTypeGuide[num46];
			}
			if (s.get_version() >= 128u)
			{
				num47 = s.ReadUInt16();
				this.m_milestoneInfoIndex = new uint[num47];
				this.m_UnlockMilestones = new MilestoneInfo.Data[num47];
			}
			if (s.get_version() >= 19u)
			{
				PrefabCollection<NetInfo>.BeginDeserialize(s);
				for (int num48 = 1; num48 < num2; num48++)
				{
					if (buffer2[num48].m_flags != NetNode.Flags.None)
					{
						buffer2[num48].m_infoIndex = (ushort)PrefabCollection<NetInfo>.Deserialize(true);
					}
				}
				for (int num49 = 1; num49 < num3; num49++)
				{
					if (buffer3[num49].m_flags != NetSegment.Flags.None)
					{
						buffer3[num49].m_infoIndex = (ushort)PrefabCollection<NetInfo>.Deserialize(true);
					}
				}
				for (uint num50 = 0u; num50 < num46; num50 += 1u)
				{
					this.m_guideInfoIndex[(int)((UIntPtr)num50)] = PrefabCollection<NetInfo>.Deserialize(false);
				}
				for (uint num51 = 0u; num51 < num47; num51 += 1u)
				{
					this.m_milestoneInfoIndex[(int)((UIntPtr)num51)] = PrefabCollection<NetInfo>.Deserialize(false);
				}
				PrefabCollection<NetInfo>.EndDeserialize(s);
			}
			for (uint num52 = 0u; num52 < num46; num52 += 1u)
			{
				this.m_netNotUsedGuides[(int)((UIntPtr)num52)] = s.ReadObject<NetTypeGuide>();
			}
			for (uint num53 = 0u; num53 < num47; num53 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num53)] = s.ReadObject<MilestoneInfo.Data>();
			}
			if (s.get_version() >= 82u)
			{
				instance.m_outsideNodeNotConnected = s.ReadObject<NetNodeInstanceGuide>();
			}
			else
			{
				instance.m_outsideNodeNotConnected = null;
			}
			if (s.get_version() >= 134u)
			{
				instance.m_transportNodeNotConnected = s.ReadObject<NetNodeInstanceGuide>();
				if (s.get_version() >= 135u)
				{
					instance.m_optionsNotUsed = s.ReadObject<ServiceTypeGuide>();
					instance.m_manualUpgrade = s.ReadObject<ServiceTypeGuide>();
				}
				else
				{
					s.ReadObject<GenericGuide>();
					s.ReadObject<GenericGuide>();
					instance.m_optionsNotUsed = null;
					instance.m_manualUpgrade = null;
				}
			}
			else
			{
				instance.m_transportNodeNotConnected = null;
				instance.m_optionsNotUsed = null;
				instance.m_manualUpgrade = null;
			}
			if (s.get_version() >= 160u)
			{
				instance.m_elevationNotUsed = s.ReadObject<ServiceTypeGuide>();
			}
			else
			{
				instance.m_elevationNotUsed = null;
			}
			if (s.get_version() >= 107u)
			{
				instance.m_upgradeExistingRoad = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_upgradeExistingRoad = null;
			}
			if (s.get_version() >= 156u)
			{
				instance.m_roadsNotUsed = s.ReadObject<GenericGuide>();
				instance.m_onewayRoadPlacement = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_roadsNotUsed = null;
				instance.m_onewayRoadPlacement = null;
			}
			if (s.get_version() >= 166u)
			{
				instance.m_shortRoadTraffic = s.ReadObject<NetSegmentInstanceGuide>();
			}
			else
			{
				instance.m_shortRoadTraffic = null;
			}
			if (s.get_version() >= 236u)
			{
				instance.m_canalsNotUsed = s.ReadObject<GenericGuide>();
				instance.m_quaysNotUsed = s.ReadObject<GenericGuide>();
				instance.m_canalDemolished = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_canalsNotUsed = null;
				instance.m_quaysNotUsed = null;
				instance.m_canalDemolished = null;
			}
			if (s.get_version() >= 237u)
			{
				instance.m_upgradeWaterPipes = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_upgradeWaterPipes = null;
			}
			if (s.get_version() >= 295u)
			{
				instance.m_roadDestroyed = s.ReadObject<NetSegmentInstanceGuide>();
				instance.m_roadDestroyed2 = s.ReadObject<ServiceTypeGuide>();
			}
			else
			{
				instance.m_roadDestroyed = null;
				instance.m_roadDestroyed2 = null;
			}
			if (s.get_version() >= 318u)
			{
				instance.m_roadNames = s.ReadObject<NetSegmentInstanceGuide>();
				instance.m_yieldLights = s.ReadObject<NetNodeInstanceGuide>();
				instance.m_visualAids = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_roadNames = null;
				instance.m_yieldLights = null;
				instance.m_visualAids = null;
			}
			if (s.get_version() >= 237u)
			{
				instance.m_lastMaxWetness = (int)s.ReadUInt8();
				instance.m_currentMaxWetness = (int)s.ReadUInt8();
			}
			else
			{
				instance.m_lastMaxWetness = 0;
				instance.m_currentMaxWetness = 0;
			}
			if (s.get_version() >= 319u)
			{
				instance.m_nameModifiedCount = (int)s.ReadUInt24();
			}
			else
			{
				instance.m_nameModifiedCount = 0;
			}
			if (s.get_version() >= 100u && s.get_version() < 139u)
			{
				s.ReadBool();
			}
			if (s.get_version() >= 321u)
			{
				uint num54 = s.ReadUInt24();
				EncodedArray.ULong uLong3 = EncodedArray.ULong.BeginRead(s);
				int num55 = 0;
				while ((long)num55 < (long)((ulong)num54))
				{
					instance.m_adjustedSegments[num55] = uLong3.Read();
					num55++;
				}
				for (int num56 = (int)num54; num56 < instance.m_adjustedSegments.Length; num56++)
				{
					instance.m_adjustedSegments[num56] = 0uL;
				}
				uLong3.EndRead();
			}
			for (int num57 = 1; num57 < num2; num57++)
			{
				if (buffer2[num57].m_flags != NetNode.Flags.None)
				{
					instance.InitializeNode((ushort)num57, ref buffer2[num57]);
				}
			}
			for (int num58 = 1; num58 < num3; num58++)
			{
				if (buffer3[num58].m_flags != NetSegment.Flags.None)
				{
					instance.InitializeSegment((ushort)num58, ref buffer3[num58]);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "NetManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "NetManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<NetInfo>.BindPrefabs();
			PrefabCollection<TransportInfo>.BindPrefabs();
			NetManager instance = Singleton<NetManager>.get_instance();
			NetLane[] buffer = instance.m_lanes.m_buffer;
			NetNode[] buffer2 = instance.m_nodes.m_buffer;
			NetSegment[] buffer3 = instance.m_segments.m_buffer;
			int num = buffer2.Length;
			int num2 = buffer3.Length;
			bool flag = Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True;
			int num3 = PrefabCollection<NetInfo>.PrefabCount();
			for (int i = 0; i < num3; i++)
			{
				NetInfo prefab = PrefabCollection<NetInfo>.GetPrefab((uint)i);
				if (prefab != null && prefab.m_notUsedGuide != null)
				{
					prefab.m_notUsedGuide = null;
				}
				if (prefab != null && prefab.m_lanes != null)
				{
					prefab.m_hasForwardVehicleLanes = false;
					prefab.m_hasBackwardVehicleLanes = false;
					prefab.m_forwardVehicleLaneCount = 0;
					prefab.m_backwardVehicleLaneCount = 0;
					for (int j = 0; j < prefab.m_lanes.Length; j++)
					{
						NetInfo.Lane lane = prefab.m_lanes[j];
						if (flag && (byte)(lane.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.Parking | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) != 0)
						{
							lane.m_finalDirection = NetInfo.InvertDirection(lane.m_direction);
						}
						else
						{
							lane.m_finalDirection = lane.m_direction;
						}
						if ((byte)(prefab.m_lanes[j].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
						{
							if ((byte)(prefab.m_lanes[j].m_finalDirection & NetInfo.Direction.Forward) != 0)
							{
								prefab.m_hasForwardVehicleLanes = true;
								prefab.m_forwardVehicleLaneCount++;
							}
							if ((byte)(prefab.m_lanes[j].m_finalDirection & NetInfo.Direction.Backward) != 0)
							{
								prefab.m_hasBackwardVehicleLanes = true;
								prefab.m_backwardVehicleLaneCount++;
							}
						}
					}
				}
			}
			if (this.m_guideInfoIndex != null)
			{
				int num4 = this.m_guideInfoIndex.Length;
				for (int k = 0; k < num4; k++)
				{
					NetInfo prefab2 = PrefabCollection<NetInfo>.GetPrefab(this.m_guideInfoIndex[k]);
					if (prefab2 != null)
					{
						prefab2.m_notUsedGuide = this.m_netNotUsedGuides[k];
					}
				}
			}
			if (this.m_milestoneInfoIndex != null)
			{
				int num5 = this.m_milestoneInfoIndex.Length;
				for (int l = 0; l < num5; l++)
				{
					NetInfo prefab3 = PrefabCollection<NetInfo>.GetPrefab(this.m_milestoneInfoIndex[l]);
					if (prefab3 != null)
					{
						MilestoneInfo unlockMilestone = prefab3.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[l]);
						}
					}
				}
			}
			for (int m = 1; m < num2; m++)
			{
				if (buffer3[m].m_flags != NetSegment.Flags.None)
				{
					buffer3[m].UpdateStartSegments((ushort)m);
					buffer3[m].UpdateEndSegments((ushort)m);
				}
			}
			bool flag2 = s.get_version() >= 220u && Singleton<SimulationManager>.get_instance().m_metaData.m_saveAppVersion >= BuildConfig.MakeVersionNumber(1u, 3u, 0u, BuildConfig.ReleaseType.Prototype, 1u, BuildConfig.BuildType.Unknown);
			for (int n = 1; n < num; n++)
			{
				if (buffer2[n].m_flags != NetNode.Flags.None)
				{
					if (buffer2[n].m_lane != 0u)
					{
						buffer2[n].m_nextLaneNode = buffer[(int)((UIntPtr)buffer2[n].m_lane)].m_nodes;
						buffer[(int)((UIntPtr)buffer2[n].m_lane)].m_nodes = (ushort)n;
					}
					NetInfo info = buffer2[n].Info;
					if (info != null)
					{
						buffer2[n].m_infoIndex = (ushort)info.m_prefabDataIndex;
						info.m_netAI.NodeLoaded((ushort)n, ref buffer2[n], s.get_version());
						info.m_netAI.TrafficDirectionUpdated((ushort)n, ref buffer2[n]);
						if (!flag2 && !info.m_requireDirectRenderers)
						{
							buffer2[n].m_connectCount = 0;
						}
					}
					buffer2[n].UpdateBounds((ushort)n);
					instance.UpdateNodeFlags((ushort)n);
					instance.UpdateNodeRenderer((ushort)n, true);
				}
			}
			for (int num6 = 1; num6 < num2; num6++)
			{
				if (buffer3[num6].m_flags != NetSegment.Flags.None)
				{
					NetInfo info2 = buffer3[num6].Info;
					if (info2 != null)
					{
						buffer3[num6].m_infoIndex = (ushort)info2.m_prefabDataIndex;
						info2.m_netAI.SegmentLoaded((ushort)num6, ref buffer3[num6]);
					}
					if (buffer3[num6].m_path != 0u)
					{
						PathUnit[] expr_4A4_cp_0 = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer;
						UIntPtr expr_4A4_cp_1 = (UIntPtr)buffer3[num6].m_path;
						expr_4A4_cp_0[(int)expr_4A4_cp_1].m_referenceCount = expr_4A4_cp_0[(int)expr_4A4_cp_1].m_referenceCount + 1;
					}
					buffer3[num6].UpdateBounds((ushort)num6);
					buffer3[num6].UpdateLanes((ushort)num6, true);
					instance.UpdateSegmentFlags((ushort)num6);
					instance.UpdateSegmentRenderer((ushort)num6, true);
				}
			}
			if (s.get_version() < 238u)
			{
				for (int num7 = 1; num7 < num; num7++)
				{
					if (buffer2[num7].m_flags != NetNode.Flags.None)
					{
						NetInfo info3 = buffer2[num7].Info;
						if (info3 != null)
						{
							info3.m_netAI.UpdateLaneConnection((ushort)num7, ref buffer2[num7]);
						}
					}
				}
			}
			instance.UpdateSegmentColors();
			instance.UpdateNodeColors();
			instance.m_laneCount = (int)(instance.m_lanes.ItemCount() - 1u);
			instance.m_nodeCount = (int)(instance.m_nodes.ItemCount() - 1u);
			instance.m_segmentCount = (int)(instance.m_segments.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "NetManager");
		}

		private uint[] m_guideInfoIndex;

		private uint[] m_milestoneInfoIndex;

		private NetTypeGuide[] m_netNotUsedGuides;

		private MilestoneInfo.Data[] m_UnlockMilestones;
	}
}
