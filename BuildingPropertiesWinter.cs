﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Math;
using ColossalFramework.Threading;
using UnityEngine;

[ExecuteInEditMode]
public class BuildingPropertiesWinter : BuildingProperties
{
	protected override void Awake()
	{
		base.Awake();
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_simulationDataReady += new LoadingManager.SimulationDataReadyHandler(this.SimulationDataReady);
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.GenerateSnow());
		}
	}

	protected override void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_simulationDataReady -= new LoadingManager.SimulationDataReadyHandler(this.SimulationDataReady);
			if (this.m_snowMaterial != null)
			{
				Object.Destroy(this.m_snowMaterial);
				this.m_snowMaterial = null;
			}
		}
		base.OnDestroy();
	}

	private void SimulationDataReady()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.SimulationDataReadyImpl());
	}

	[DebuggerHidden]
	private IEnumerator GenerateSnow()
	{
		BuildingPropertiesWinter.<GenerateSnow>c__Iterator0 <GenerateSnow>c__Iterator = new BuildingPropertiesWinter.<GenerateSnow>c__Iterator0();
		<GenerateSnow>c__Iterator.$this = this;
		return <GenerateSnow>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SimulationDataReadyImpl()
	{
		BuildingPropertiesWinter.<SimulationDataReadyImpl>c__Iterator1 <SimulationDataReadyImpl>c__Iterator = new BuildingPropertiesWinter.<SimulationDataReadyImpl>c__Iterator1();
		<SimulationDataReadyImpl>c__Iterator.$this = this;
		return <SimulationDataReadyImpl>c__Iterator;
	}

	private Task GenerateSnow(BuildingInfoBase info)
	{
		if (info.m_mesh != null && !info.m_noBase && !info.m_hasVertexAnimation && !info.m_disableSnow && info.m_generatedMesh == null)
		{
			info.m_generatedRequireHeightMap = true;
			info.m_extraMaterial = this.m_snowMaterial;
			return ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				BuildingPropertiesWinter.GenerateSnowImpl(info);
			});
		}
		return null;
	}

	private static void GenerateSnowImpl(BuildingInfoBase info)
	{
		Vector3[] origVertices = null;
		Color32[] origColors = null;
		int[] origTriangles = null;
		BoneWeight[] origBoneWeights = null;
		string buildingType = null;
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			origVertices = info.m_mesh.get_vertices();
			origColors = info.m_mesh.get_colors32();
			origTriangles = info.m_mesh.get_triangles();
			origBoneWeights = info.m_mesh.get_boneWeights();
			buildingType = info.m_material.GetTag("BuildingType", false);
		}).Wait();
		bool isAnimated = true;
		if (origBoneWeights == null || origBoneWeights.Length == 0)
		{
			isAnimated = false;
		}
		BoneWeight weight = default(BoneWeight);
		weight.set_weight0(1f);
		FastList<BuildingPropertiesWinter.SnowPoint> fastList = new FastList<BuildingPropertiesWinter.SnowPoint>();
		Dictionary<Vector3, int> dictionary = new Dictionary<Vector3, int>();
		FastList<int> fastList2 = new FastList<int>();
		if (origVertices != null && origVertices.Length != 0 && (origColors == null || origColors.Length == 0))
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Vertex colors missing: " + info.get_gameObject().get_name(), info.get_gameObject());
			});
		}
		bool flag = buildingType == "AnimUV";
		int i = 0;
		while (i < origTriangles.Length)
		{
			int num = origTriangles[i];
			int num2 = origTriangles[i + 1];
			int num3 = origTriangles[i + 2];
			if (origColors == null || origColors.Length == 0)
			{
				goto IL_225;
			}
			Color32 color = origColors[num];
			Color32 color2 = origColors[num2];
			Color32 color3 = origColors[num3];
			if (color.r != 255 || (!flag && color.g != 255) || color.b > 150 || color2.r != 255 || (!flag && color2.g != 255) || color2.b > 150 || color3.r != 255 || (!flag && color3.g != 255) || color3.b > 150)
			{
				goto IL_225;
			}
			IL_4E8:
			i += 3;
			continue;
			IL_225:
			Vector3 vector = origVertices[num];
			Vector3 vector2 = origVertices[num2];
			Vector3 vector3 = origVertices[num3];
			if (Triangle3.Area(vector, vector2, vector3) < 0.04f)
			{
				goto IL_4E8;
			}
			if (Vector3.Normalize(Vector3.Cross(vector2 - vector, vector3 - vector)).y <= 0.7f)
			{
				goto IL_4E8;
			}
			Vector3 vector4 = BuildingPropertiesWinter.RoundPos(vector);
			Vector3 vector5 = BuildingPropertiesWinter.RoundPos(vector2);
			Vector3 vector6 = BuildingPropertiesWinter.RoundPos(vector3);
			if (vector4 == vector5 || vector5 == vector6 || vector6 == vector4)
			{
				goto IL_4E8;
			}
			int size;
			if (!dictionary.TryGetValue(vector4, out size))
			{
				size = fastList.m_size;
				dictionary.Add(vector4, size);
				BuildingPropertiesWinter.SnowPoint item = default(BuildingPropertiesWinter.SnowPoint);
				item.m_position = vector;
				item.m_edges = new FastList<int>();
				if (isAnimated)
				{
					item.m_weight = origBoneWeights[num];
				}
				fastList.Add(item);
			}
			int size2;
			if (!dictionary.TryGetValue(vector5, out size2))
			{
				size2 = fastList.m_size;
				dictionary.Add(vector5, size2);
				BuildingPropertiesWinter.SnowPoint item2 = default(BuildingPropertiesWinter.SnowPoint);
				item2.m_position = vector2;
				item2.m_edges = new FastList<int>();
				if (isAnimated)
				{
					item2.m_weight = origBoneWeights[num2];
				}
				fastList.Add(item2);
			}
			int size3;
			if (!dictionary.TryGetValue(vector6, out size3))
			{
				size3 = fastList.m_size;
				dictionary.Add(vector6, size3);
				BuildingPropertiesWinter.SnowPoint item3 = default(BuildingPropertiesWinter.SnowPoint);
				item3.m_position = vector3;
				item3.m_edges = new FastList<int>();
				if (isAnimated)
				{
					item3.m_weight = origBoneWeights[num3];
				}
				fastList.Add(item3);
			}
			fastList.m_buffer[size].m_edges.Add(size2);
			fastList.m_buffer[size].m_edges.Add(size3);
			fastList.m_buffer[size2].m_edges.Add(size3);
			fastList.m_buffer[size2].m_edges.Add(size);
			fastList.m_buffer[size3].m_edges.Add(size);
			fastList.m_buffer[size3].m_edges.Add(size2);
			fastList2.Add(size);
			fastList2.Add(size2);
			fastList2.Add(size3);
			goto IL_4E8;
		}
		if (info.m_generatedInfo != null && info.m_generatedInfo.m_baseArea != null)
		{
			Vector2[] baseArea = info.m_generatedInfo.m_baseArea;
			Vector2[] baseNormals = info.m_generatedInfo.m_baseNormals;
			Dictionary<Vector3, Vector3> dictionary2 = new Dictionary<Vector3, Vector3>();
			for (int j = 0; j < baseArea.Length; j += 2)
			{
				Vector3 vector7;
				vector7..ctor(baseArea[j + 1].x, 0f, baseArea[j + 1].y);
				Vector3 vector8;
				vector8..ctor(baseArea[j].x, 0f, baseArea[j].y);
				float num4 = Mathf.Min(1f, Vector3.Distance(vector7, vector8));
				if (num4 >= 0.2f)
				{
					Vector3 vector9;
					vector9..ctor(baseNormals[j + 1].x, 0f, baseNormals[j + 1].y);
					Vector3 vector10;
					vector10..ctor(baseNormals[j].x, 0f, baseNormals[j].y);
					vector9 *= num4;
					vector10 *= num4;
					vector7 = BuildingPropertiesWinter.RoundPos(vector7);
					vector8 = BuildingPropertiesWinter.RoundPos(vector8);
					Vector3 vector11;
					if (dictionary2.TryGetValue(vector7, out vector11))
					{
						float num5 = Vector3.Dot(vector9, vector11);
						if (num5 > 0.001f)
						{
							vector9 *= Mathf.Max(0f, 1f - num5 * (0.5f / vector9.get_sqrMagnitude()));
							vector11 *= Mathf.Max(0f, 1f - num5 * (0.5f / vector11.get_sqrMagnitude()));
						}
						vector9 += vector11;
					}
					dictionary2[vector7] = vector9;
					if (dictionary2.TryGetValue(vector8, out vector11))
					{
						float num6 = Vector3.Dot(vector10, vector11);
						if (num6 > 0.001f)
						{
							vector10 *= Mathf.Max(0f, 1f - num6 * (0.5f / vector10.get_sqrMagnitude()));
							vector11 *= Mathf.Max(0f, 1f - num6 * (0.5f / vector11.get_sqrMagnitude()));
						}
						vector10 += vector11;
					}
					dictionary2[vector8] = vector10;
				}
			}
			for (int k = 0; k < baseArea.Length; k += 2)
			{
				Vector3 vector12;
				vector12..ctor(baseArea[k + 1].x, 0f, baseArea[k + 1].y);
				Vector3 vector13;
				vector13..ctor(baseArea[k].x, 0f, baseArea[k].y);
				float num7 = Mathf.Min(1f, Vector3.Distance(vector12, vector13));
				if (num7 >= 0.2f)
				{
					Vector3 vector14 = BuildingPropertiesWinter.RoundPos(vector12);
					Vector3 vector15 = BuildingPropertiesWinter.RoundPos(vector13);
					Vector3 vector16;
					if (!dictionary2.TryGetValue(vector14, out vector16))
					{
						vector16..ctor(baseNormals[k + 1].x, 0f, baseNormals[k + 1].y);
					}
					Vector3 vector17;
					if (!dictionary2.TryGetValue(vector15, out vector17))
					{
						vector17..ctor(baseNormals[k].x, 0f, baseNormals[k].y);
					}
					Vector3 vector18 = vector13 + vector17;
					Vector3 vector19 = vector12 + vector16;
					Vector3 vector20 = BuildingPropertiesWinter.RoundPos(vector18);
					Vector3 vector21 = BuildingPropertiesWinter.RoundPos(vector19);
					if (!(vector20 == vector15) && !(vector21 == vector14) && !(vector20 == vector21))
					{
						int size4;
						if (!dictionary.TryGetValue(vector14, out size4))
						{
							size4 = fastList.m_size;
							dictionary.Add(vector14, size4);
							BuildingPropertiesWinter.SnowPoint item4 = default(BuildingPropertiesWinter.SnowPoint);
							item4.m_isBase = true;
							item4.m_position = vector12;
							item4.m_edges = new FastList<int>();
							if (isAnimated)
							{
								item4.m_weight = weight;
							}
							fastList.Add(item4);
						}
						int size5;
						if (!dictionary.TryGetValue(vector15, out size5))
						{
							size5 = fastList.m_size;
							dictionary.Add(vector15, size5);
							BuildingPropertiesWinter.SnowPoint item5 = default(BuildingPropertiesWinter.SnowPoint);
							item5.m_isBase = true;
							item5.m_position = vector13;
							item5.m_edges = new FastList<int>();
							if (isAnimated)
							{
								item5.m_weight = weight;
							}
							fastList.Add(item5);
						}
						int size6;
						if (!dictionary.TryGetValue(vector20, out size6))
						{
							size6 = fastList.m_size;
							dictionary.Add(vector20, size6);
							BuildingPropertiesWinter.SnowPoint item6 = default(BuildingPropertiesWinter.SnowPoint);
							item6.m_isBase = true;
							item6.m_keepHeight = true;
							item6.m_position = vector18;
							item6.m_edges = new FastList<int>();
							if (isAnimated)
							{
								item6.m_weight = weight;
							}
							fastList.Add(item6);
						}
						int size7;
						if (!dictionary.TryGetValue(vector21, out size7))
						{
							size7 = fastList.m_size;
							dictionary.Add(vector21, size7);
							BuildingPropertiesWinter.SnowPoint item7 = default(BuildingPropertiesWinter.SnowPoint);
							item7.m_isBase = true;
							item7.m_keepHeight = true;
							item7.m_position = vector19;
							item7.m_edges = new FastList<int>();
							if (isAnimated)
							{
								item7.m_weight = weight;
							}
							fastList.Add(item7);
						}
						fastList.m_buffer[size4].m_edges.Add(size5);
						fastList.m_buffer[size4].m_edges.Add(size6);
						fastList.m_buffer[size5].m_edges.Add(size6);
						fastList.m_buffer[size5].m_edges.Add(size4);
						fastList.m_buffer[size6].m_edges.Add(size4);
						fastList.m_buffer[size6].m_edges.Add(size5);
						fastList.m_buffer[size6].m_edges.Add(size4);
						fastList.m_buffer[size7].m_edges.Add(size4);
						fastList.m_buffer[size4].m_edges.Add(size6);
						fastList.m_buffer[size4].m_edges.Add(size7);
						fastList2.Add(size4);
						fastList2.Add(size5);
						fastList2.Add(size6);
						fastList2.Add(size4);
						fastList2.Add(size6);
						fastList2.Add(size7);
					}
				}
			}
		}
		for (int l = 0; l < fastList.m_size; l++)
		{
			BuildingPropertiesWinter.SnowPoint snowPoint = fastList.m_buffer[l];
			Vector3 vector22 = Vector3.get_zero();
			float num8 = 1f;
			float num9 = 0f;
			for (int m = 0; m < snowPoint.m_edges.m_size; m++)
			{
				Vector3 vector23 = fastList.m_buffer[snowPoint.m_edges.m_buffer[m]].m_position - snowPoint.m_position;
				float magnitude = vector23.get_magnitude();
				if (magnitude > 0.001f)
				{
					num8 = Mathf.Min(num8, magnitude);
					num9 = Mathf.Max(num9, magnitude);
					vector22 += vector23 / magnitude;
				}
			}
			float num10 = (num8 + Mathf.Min(1f, num9)) * 0.5f;
			vector22 = vector22.get_normalized() * (num10 * 0.5f);
			if (vector22.y < 0f)
			{
				vector22.x *= 1f + vector22.y * 0.5f;
				vector22.z *= 1f + vector22.y * 0.5f;
				vector22.y = 0f;
			}
			for (int n = 0; n < snowPoint.m_edges.m_size; n++)
			{
				Vector3 vector24 = fastList.m_buffer[snowPoint.m_edges.m_buffer[n]].m_position - snowPoint.m_position;
				float sqrMagnitude = vector24.get_sqrMagnitude();
				if (sqrMagnitude > 1.00000011E-06f)
				{
					float num11 = Vector3.Dot(vector22, vector24) / sqrMagnitude;
					if (num11 > 0.3f)
					{
						vector22 += vector24 * (0.3f - num11);
					}
				}
			}
			snowPoint.m_position2 = snowPoint.m_position + vector22;
			if (!snowPoint.m_keepHeight)
			{
				snowPoint.m_position2.y = snowPoint.m_position2.y + num10 * num10 * 0.5f;
			}
			fastList.m_buffer[l] = snowPoint;
		}
		Dictionary<int, Vector3> edgeNormals = new Dictionary<int, Vector3>();
		for (int num12 = 0; num12 < fastList2.m_size; num12 += 3)
		{
			int num13 = fastList2[num12];
			int num14 = fastList2[num12 + 1];
			int num15 = fastList2[num12 + 2];
			BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num13, num14, num15, true, true, true);
			if (BuildingPropertiesWinter.CountEdges(fastList, num13, num14) == 1)
			{
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num13, num14, num13, false, false, true);
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num13, num14, num14, true, false, true);
			}
			if (BuildingPropertiesWinter.CountEdges(fastList, num14, num15) == 1)
			{
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num14, num15, num14, false, true, true);
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num14, num15, num15, false, false, true);
			}
			if (BuildingPropertiesWinter.CountEdges(fastList, num15, num13) == 1)
			{
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num15, num13, num15, false, false, true);
				BuildingPropertiesWinter.AddEdgeNormal(fastList, edgeNormals, num15, num13, num13, true, false, true);
			}
		}
		FastList<Vector3> newVertices = new FastList<Vector3>();
		FastList<Vector3> newNormals = new FastList<Vector3>();
		FastList<Vector4> newTangents = new FastList<Vector4>();
		FastList<Vector2> newUvs = new FastList<Vector2>();
		FastList<Color32> newColors = new FastList<Color32>();
		FastList<BoneWeight> newBoneWeights = null;
		if (isAnimated)
		{
			newBoneWeights = new FastList<BoneWeight>();
		}
		FastList<int> newTriangles = new FastList<int>();
		for (int num16 = 0; num16 < fastList2.m_size; num16 += 3)
		{
			if (newVertices.m_size > 64979)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					CODebugBase<LogChannel>.Warn(LogChannel.Core, info.get_gameObject().get_name() + ": Snow mesh has too many vertices!", info.get_gameObject());
				});
				break;
			}
			int num17 = fastList2[num16];
			int num18 = fastList2[num16 + 1];
			int num19 = fastList2[num16 + 2];
			BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num17, num18, num19, true, true, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
			if (BuildingPropertiesWinter.CountEdges(fastList, num17, num18) == 1)
			{
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num17, num18, num17, false, false, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num17, num18, num18, true, false, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
			}
			if (BuildingPropertiesWinter.CountEdges(fastList, num18, num19) == 1)
			{
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num18, num19, num18, false, true, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num18, num19, num19, false, false, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
			}
			if (BuildingPropertiesWinter.CountEdges(fastList, num19, num17) == 1)
			{
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num19, num17, num19, false, false, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
				BuildingPropertiesWinter.CreateTriangle(fastList, edgeNormals, num19, num17, num17, true, false, true, newVertices, newNormals, newTangents, newUvs, newColors, newTriangles, newBoneWeights, info.m_requireHeightMap);
			}
		}
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			if (isAnimated)
			{
				Mesh mesh = Object.Instantiate<Mesh>(info.m_mesh);
				mesh.Clear();
				mesh.set_vertices(newVertices.ToArray());
				mesh.set_normals(newNormals.ToArray());
				mesh.set_tangents(newTangents.ToArray());
				mesh.set_uv(newUvs.ToArray());
				mesh.set_colors32(newColors.ToArray());
				if (isAnimated)
				{
					mesh.set_boneWeights(newBoneWeights.ToArray());
				}
				mesh.set_triangles(newTriangles.ToArray());
				info.m_generatedMesh = mesh;
			}
			else
			{
				Mesh mesh2 = new Mesh();
				mesh2.Clear();
				mesh2.set_vertices(newVertices.ToArray());
				mesh2.set_normals(newNormals.ToArray());
				mesh2.set_tangents(newTangents.ToArray());
				mesh2.set_uv(newUvs.ToArray());
				mesh2.set_colors32(newColors.ToArray());
				mesh2.set_triangles(newTriangles.ToArray());
				info.m_generatedMesh = mesh2;
			}
		}).Wait();
	}

	private static Vector3 RoundPos(Vector3 pos)
	{
		Vector3 result;
		result.x = (float)Mathf.RoundToInt(pos.x * 100f) * 0.01f;
		result.y = (float)Mathf.RoundToInt(pos.y * 100f) * 0.01f;
		result.z = (float)Mathf.RoundToInt(pos.z * 100f) * 0.01f;
		return result;
	}

	private static int CountEdges(FastList<BuildingPropertiesWinter.SnowPoint> snowPoints, int aIndex, int bIndex)
	{
		int num = 0;
		BuildingPropertiesWinter.SnowPoint snowPoint = snowPoints.m_buffer[aIndex];
		for (int i = 0; i < snowPoint.m_edges.m_size; i++)
		{
			if (snowPoint.m_edges.m_buffer[i] == bIndex)
			{
				num++;
			}
		}
		return num;
	}

	private static void AddEdgeNormal(FastList<BuildingPropertiesWinter.SnowPoint> snowPoints, Dictionary<int, Vector3> edgeNormals, int aIndex, int bIndex, int cIndex, bool aUpper, bool bUpper, bool cUpper)
	{
		Vector3 vector;
		if (aUpper)
		{
			vector = snowPoints.m_buffer[aIndex].m_position2;
			aIndex = (aIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[aIndex].m_keepHeight)
			{
				return;
			}
			vector = snowPoints.m_buffer[aIndex].m_position;
			aIndex <<= 1;
		}
		Vector3 vector2;
		if (bUpper)
		{
			vector2 = snowPoints.m_buffer[bIndex].m_position2;
			bIndex = (bIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[bIndex].m_keepHeight)
			{
				return;
			}
			vector2 = snowPoints.m_buffer[bIndex].m_position;
			bIndex <<= 1;
		}
		Vector3 vector3;
		if (cUpper)
		{
			vector3 = snowPoints.m_buffer[cIndex].m_position2;
			cIndex = (cIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[cIndex].m_keepHeight)
			{
				return;
			}
			vector3 = snowPoints.m_buffer[cIndex].m_position;
			cIndex <<= 1;
		}
		Vector3 value = Vector3.Normalize(Vector3.Cross(vector2 - vector, vector3 - vector));
		edgeNormals[aIndex | bIndex << 16] = value;
		edgeNormals[bIndex | cIndex << 16] = value;
		edgeNormals[cIndex | aIndex << 16] = value;
	}

	private static void CreateTriangle(FastList<BuildingPropertiesWinter.SnowPoint> snowPoints, Dictionary<int, Vector3> edgeNormals, int aIndex, int bIndex, int cIndex, bool aUpper, bool bUpper, bool cUpper, FastList<Vector3> vertices, FastList<Vector3> normals, FastList<Vector4> tangents, FastList<Vector2> uvs, FastList<Color32> colors, FastList<int> triangles, FastList<BoneWeight> boneWeights, bool useTerrainHeight)
	{
		bool isBase = snowPoints.m_buffer[aIndex].m_isBase;
		bool isBase2 = snowPoints.m_buffer[bIndex].m_isBase;
		bool isBase3 = snowPoints.m_buffer[cIndex].m_isBase;
		BoneWeight weight = snowPoints.m_buffer[aIndex].m_weight;
		BoneWeight weight2 = snowPoints.m_buffer[bIndex].m_weight;
		BoneWeight weight3 = snowPoints.m_buffer[cIndex].m_weight;
		Vector3 vector;
		if (aUpper)
		{
			vector = snowPoints.m_buffer[aIndex].m_position2;
			aIndex = (aIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[aIndex].m_keepHeight)
			{
				return;
			}
			vector = snowPoints.m_buffer[aIndex].m_position;
			aIndex <<= 1;
		}
		Vector3 vector2;
		if (bUpper)
		{
			vector2 = snowPoints.m_buffer[bIndex].m_position2;
			bIndex = (bIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[bIndex].m_keepHeight)
			{
				return;
			}
			vector2 = snowPoints.m_buffer[bIndex].m_position;
			bIndex <<= 1;
		}
		Vector3 vector3;
		if (cUpper)
		{
			vector3 = snowPoints.m_buffer[cIndex].m_position2;
			cIndex = (cIndex << 1) + 1;
		}
		else
		{
			if (snowPoints.m_buffer[cIndex].m_keepHeight)
			{
				return;
			}
			vector3 = snowPoints.m_buffer[cIndex].m_position;
			cIndex <<= 1;
		}
		Color32 item;
		item..ctor((!isBase && !useTerrainHeight) ? 0 : 255, (!isBase || vector.y != 0f) ? 0 : 255, 0, 255);
		Color32 item2;
		item2..ctor((!isBase2 && !useTerrainHeight) ? 0 : 255, (!isBase2 || vector2.y != 0f) ? 0 : 255, 0, 255);
		Color32 item3;
		item3..ctor((!isBase3 && !useTerrainHeight) ? 0 : 255, (!isBase3 || vector3.y != 0f) ? 0 : 255, 0, 255);
		Vector3 vector4;
		if (!edgeNormals.TryGetValue(aIndex << 16 | bIndex, out vector4))
		{
			if (isBase && isBase2 && vector.y == 0f && vector2.y == 0f)
			{
				vector4 = Vector3.Normalize(Vector3.Cross(vector3 - vector, vector2 - vector));
			}
			else
			{
				vector4 = Vector3.Normalize(Vector3.Cross(vector2 - vector, vector3 - vector));
			}
		}
		Vector3 vector5;
		if (!edgeNormals.TryGetValue(bIndex << 16 | cIndex, out vector5))
		{
			if (isBase2 && isBase3 && vector2.y == 0f && vector3.y == 0f)
			{
				vector5 = Vector3.Normalize(Vector3.Cross(vector3 - vector, vector2 - vector));
			}
			else
			{
				vector5 = Vector3.Normalize(Vector3.Cross(vector2 - vector, vector3 - vector));
			}
		}
		Vector3 vector6;
		if (!edgeNormals.TryGetValue(cIndex << 16 | aIndex, out vector6))
		{
			if (isBase3 && isBase && vector3.y == 0f && vector.y == 0f)
			{
				vector6 = Vector3.Normalize(Vector3.Cross(vector3 - vector, vector2 - vector));
			}
			else
			{
				vector6 = Vector3.Normalize(Vector3.Cross(vector2 - vector, vector3 - vector));
			}
		}
		float num = Mathf.Sqrt(Line3.DistanceSqr(vector, vector2, vector3));
		float num2 = Mathf.Sqrt(Line3.DistanceSqr(vector2, vector3, vector));
		float num3 = Mathf.Sqrt(Line3.DistanceSqr(vector3, vector, vector2));
		triangles.Add(vertices.m_size);
		vertices.Add(vector);
		normals.Add(new Vector3(0f, num2, 0f));
		tangents.Add(new Vector4(vector4.x, vector4.z, vector5.x, vector5.z));
		uvs.Add(new Vector2(vector6.x, vector6.z));
		if (boneWeights != null)
		{
			boneWeights.Add(weight);
		}
		colors.Add(item);
		triangles.Add(vertices.m_size);
		vertices.Add(vector2);
		normals.Add(new Vector3(0f, 0f, num3));
		tangents.Add(new Vector4(vector4.x, vector4.z, vector5.x, vector5.z));
		uvs.Add(new Vector2(vector6.x, vector6.z));
		if (boneWeights != null)
		{
			boneWeights.Add(weight2);
		}
		colors.Add(item2);
		triangles.Add(vertices.m_size);
		vertices.Add(vector3);
		normals.Add(new Vector3(num, 0f, 0f));
		tangents.Add(new Vector4(vector4.x, vector4.z, vector5.x, vector5.z));
		uvs.Add(new Vector2(vector6.x, vector6.z));
		if (boneWeights != null)
		{
			boneWeights.Add(weight3);
		}
		colors.Add(item3);
	}

	public Shader m_snowShader;

	[NonSerialized]
	private Material m_snowMaterial;

	[NonSerialized]
	private FastList<Task> m_tasks;

	private struct SnowPoint
	{
		public bool m_isBase;

		public bool m_keepHeight;

		public Vector3 m_position;

		public Vector3 m_position2;

		public BoneWeight m_weight;

		public FastList<int> m_edges;
	}
}
