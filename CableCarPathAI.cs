﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CableCarPathAI : PlayerNetAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
	}

	public override void DestroyPrefab()
	{
		base.DestroyPrefab();
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[9];
		}
		if (infoMode == InfoManager.InfoMode.Traffic)
		{
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)data.m_trafficDensity * 0.01f));
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(segmentID, ref data, infoMode);
		}
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[9];
		}
		if (infoMode == InfoManager.InfoMode.Traffic)
		{
			int num = 0;
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_trafficDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)num * 0.01f));
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(nodeID, ref data, infoMode);
		}
		NetManager instance2 = Singleton<NetManager>.get_instance();
		bool flag = false;
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = data.GetSegment(j);
			if (segment2 != 0 && (instance2.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				flag = true;
			}
		}
		if (flag)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 4;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if (this.m_pylonInfo != null && this.m_pylonInfo.Length != 0 && (data.m_flags & (NetNode.Flags.End | NetNode.Flags.Double)) != NetNode.Flags.Double)
		{
			int num = Mathf.Clamp((int)(data.m_elevation / 12), 0, this.m_pylonInfo.Length - 1);
			building = this.m_pylonInfo[num];
			heightOffset = -12f * (float)num;
		}
		else
		{
			building = null;
			heightOffset = 0f;
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_flags &= ~NetNode.Flags.Transition;
		for (int i = 0; i < 8; i++)
		{
			ushort num = data.GetSegment(i);
			if (num != 0)
			{
				ushort otherNode = instance.m_segments.m_buffer[(int)num].GetOtherNode(nodeID);
				int num2 = 0;
				while ((instance.m_nodes.m_buffer[(int)otherNode].m_flags & NetNode.Flags.Transition) != NetNode.Flags.None)
				{
					NetNode[] expr_5E_cp_0 = instance.m_nodes.m_buffer;
					ushort expr_5E_cp_1 = otherNode;
					expr_5E_cp_0[(int)expr_5E_cp_1].m_flags = (expr_5E_cp_0[(int)expr_5E_cp_1].m_flags & ~NetNode.Flags.Transition);
					for (int j = 0; j < 8; j++)
					{
						ushort segment = instance.m_nodes.m_buffer[(int)otherNode].GetSegment(j);
						if (segment != 0 && segment != num)
						{
							otherNode = instance.m_segments.m_buffer[(int)segment].GetOtherNode(otherNode);
							num = segment;
							break;
						}
					}
					if (++num2 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override bool IsOverground()
	{
		return true;
	}

	public override bool RequireDoubleSegments()
	{
		return this.m_pylonInfo != null && this.m_pylonInfo.Length != 0;
	}

	public override bool FlattenOnlyOneDoubleSegment()
	{
		return true;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return 1000f;
		}
		return 0f;
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		return false;
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingStart;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingStart;
		}
		if ((instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingEnd;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingEnd;
		}
	}

	public override NetSegment.Flags UpdateSegmentFlags(ref NetNode startNode, ref NetNode endNode)
	{
		NetSegment.Flags flags = base.UpdateSegmentFlags(ref startNode, ref endNode);
		if ((startNode.m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			flags |= NetSegment.Flags.CrossingStart;
		}
		if ((endNode.m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			flags |= NetSegment.Flags.CrossingEnd;
		}
		return flags;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		if (instance.m_nodes.m_buffer[(int)data.m_startNode].CountSegments() == 1)
		{
			ushort building = instance.m_nodes.m_buffer[(int)data.m_startNode].m_building;
			if (building != 0)
			{
				BuildingInfo info = instance2.m_buildings.m_buffer[(int)building].Info;
				if (info.m_buildingAI.CollapseBuilding(building, ref instance2.m_buildings.m_buffer[(int)building], group, false, false, 255))
				{
					instance.m_nodes.m_buffer[(int)data.m_startNode].m_building = 0;
					Building[] expr_E0_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_E0_cp_1 = building;
					expr_E0_cp_0[(int)expr_E0_cp_1].m_flags = (expr_E0_cp_0[(int)expr_E0_cp_1].m_flags | Building.Flags.Original);
					Building[] expr_FE_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_FE_cp_1 = building;
					expr_FE_cp_0[(int)expr_FE_cp_1].m_flags = (expr_FE_cp_0[(int)expr_FE_cp_1].m_flags & ~Building.Flags.Untouchable);
				}
			}
		}
		if (instance.m_nodes.m_buffer[(int)data.m_endNode].CountSegments() == 1)
		{
			ushort building2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_building;
			if (building2 != 0)
			{
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)building2].Info;
				if (info2.m_buildingAI.CollapseBuilding(building2, ref instance2.m_buildings.m_buffer[(int)building2], group, false, false, 255))
				{
					instance.m_nodes.m_buffer[(int)data.m_endNode].m_building = 0;
					Building[] expr_1C5_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_1C5_cp_1 = building2;
					expr_1C5_cp_0[(int)expr_1C5_cp_1].m_flags = (expr_1C5_cp_0[(int)expr_1C5_cp_1].m_flags | Building.Flags.Original);
					Building[] expr_1E4_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_1E4_cp_1 = building2;
					expr_1E4_cp_0[(int)expr_1E4_cp_1].m_flags = (expr_1E4_cp_0[(int)expr_1E4_cp_1].m_flags & ~Building.Flags.Untouchable);
				}
			}
		}
		instance.ReleaseSegment(segmentID, false);
		return true;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.UpdateSegmentRenderer(segmentID, true);
		instance.UpdateSegmentColors(segmentID);
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)data.m_startNode].GetSegment(i);
			if (segment != 0 && segment != segmentID && (instance.m_segments.m_buffer[(int)segment].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed | NetSegment.Flags.Untouchable)) == NetSegment.Flags.Created)
			{
				ushort num = DefaultTool.FindSecondarySegment(segment);
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				info.m_netAI.CollapseSegment(segment, ref instance.m_segments.m_buffer[(int)segment], group, false);
				if (num != 0 && num != segmentID && (instance.m_segments.m_buffer[(int)num].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed | NetSegment.Flags.Untouchable)) == NetSegment.Flags.Created)
				{
					info = instance.m_segments.m_buffer[(int)num].Info;
					info.m_netAI.CollapseSegment(num, ref instance.m_segments.m_buffer[(int)num], group, false);
				}
			}
		}
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = instance.m_nodes.m_buffer[(int)data.m_endNode].GetSegment(j);
			if (segment2 != 0 && segment2 != segmentID && (instance.m_segments.m_buffer[(int)segment2].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed | NetSegment.Flags.Untouchable)) == NetSegment.Flags.Created)
			{
				ushort num2 = DefaultTool.FindSecondarySegment(segment2);
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment2].Info;
				info2.m_netAI.CollapseSegment(segment2, ref instance.m_segments.m_buffer[(int)segment2], group, false);
				if (num2 != 0 && num2 != segmentID && (instance.m_segments.m_buffer[(int)num2].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed | NetSegment.Flags.Untouchable)) == NetSegment.Flags.Created)
				{
					info2 = instance.m_segments.m_buffer[(int)num2].Info;
					info2.m_netAI.CollapseSegment(num2, ref instance.m_segments.m_buffer[(int)num2], group, false);
				}
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		NetManager instance = Singleton<NetManager>.get_instance();
		middlePoint.m_position = (startPoint.m_position + endPoint.m_position) * 0.5f;
		middlePoint.m_elevation = (startPoint.m_elevation + endPoint.m_elevation) * 0.5f;
		Vector3 vector = endPoint.m_position - startPoint.m_position;
		if (VectorUtils.LengthSqrXZ(vector) > 0.01f)
		{
			middlePoint.m_direction = VectorUtils.NormalizeXZ(vector);
			endPoint.m_direction = middlePoint.m_direction;
		}
		if (startPoint.m_node != 0)
		{
			bool flag = false;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)startPoint.m_node].GetSegment(i);
				if (segment != 0)
				{
					if (flag)
					{
						toolErrors |= ToolBase.ToolErrors.InvalidShape;
						break;
					}
					vector = instance.m_segments.m_buffer[(int)segment].GetDirection(startPoint.m_node);
					if (vector.x * middlePoint.m_direction.x + vector.z * middlePoint.m_direction.z >= -0.7f)
					{
						toolErrors |= ToolBase.ToolErrors.InvalidShape;
					}
					flag = true;
				}
			}
		}
		else if (startPoint.m_segment != 0)
		{
			toolErrors |= ToolBase.ToolErrors.InvalidShape;
		}
		if (endPoint.m_node != 0)
		{
			bool flag2 = false;
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)endPoint.m_node].GetSegment(j);
				if (segment2 != 0)
				{
					if (flag2)
					{
						toolErrors |= ToolBase.ToolErrors.InvalidShape;
						break;
					}
					vector = instance.m_segments.m_buffer[(int)segment2].GetDirection(endPoint.m_node);
					if (vector.x * endPoint.m_direction.x + vector.z * endPoint.m_direction.z <= 0.7f)
					{
						toolErrors |= ToolBase.ToolErrors.InvalidShape;
					}
					flag2 = true;
				}
			}
		}
		else if (endPoint.m_segment != 0)
		{
			toolErrors |= ToolBase.ToolErrors.InvalidShape;
		}
		return toolErrors;
	}

	public override float GetTerrainLowerOffset()
	{
		return Mathf.Max(0f, this.m_info.m_minHeight - 3f);
	}

	public override float GetMinSegmentLength()
	{
		return 15f;
	}

	public BuildingInfo[] m_pylonInfo;
}
