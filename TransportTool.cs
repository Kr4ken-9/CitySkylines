﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TransportTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
		this.m_lastMoveIndex = -2;
		this.m_lastAddIndex = -2;
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
	}

	protected override void OnToolGUI(Event e)
	{
		bool isInsideUI = this.m_toolController.IsInsideUI;
		if (e.get_type() == null)
		{
			if (!isInsideUI)
			{
				if (e.get_button() == 0)
				{
					if (this.m_mode == TransportTool.Mode.NewLine)
					{
						if (this.m_errors == ToolBase.ToolErrors.None)
						{
							Singleton<SimulationManager>.get_instance().AddAction(this.NewLine());
						}
					}
					else if (this.m_mode == TransportTool.Mode.AddStops && this.m_errors == ToolBase.ToolErrors.None)
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.AddStop());
					}
				}
				else if (e.get_button() == 1)
				{
					if (this.m_mode == TransportTool.Mode.NewLine)
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.RemoveStop());
					}
					else if (this.m_mode == TransportTool.Mode.AddStops)
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.CancelPrevStop());
					}
					else if (this.m_mode == TransportTool.Mode.MoveStops)
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.CancelMoveStop());
					}
				}
			}
		}
		else if (e.get_type() == 1 && e.get_button() == 0 && this.m_mode == TransportTool.Mode.MoveStops)
		{
			Singleton<SimulationManager>.get_instance().AddAction<bool>(this.MoveStop(!isInsideUI));
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_errors = ToolBase.ToolErrors.Pending;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		Singleton<SimulationManager>.get_instance().AddAction(this.DisableTool());
		this.m_errors = ToolBase.ToolErrors.Pending;
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_errors != ToolBase.ToolErrors.Pending && this.m_errors != ToolBase.ToolErrors.RaycastFailed && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			ushort lastEditLine = this.m_lastEditLine;
			int hoverStopIndex = this.m_hoverStopIndex;
			int hoverSegmentIndex = this.m_hoverSegmentIndex;
			Vector3 hitPosition = this.m_hitPosition;
			if (lastEditLine != 0 && (hoverStopIndex != -1 || hoverSegmentIndex != -1))
			{
				TransportTool.RenderOverlay(cameraInfo, lastEditLine, hoverStopIndex, hoverSegmentIndex, hitPosition);
			}
		}
		base.RenderOverlay(cameraInfo);
	}

	private static void RenderOverlay(RenderManager.CameraInfo cameraInfo, ushort line, int stopIndex, int segmentIndex, Vector3 position)
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		Color color = instance.m_lines.m_buffer[(int)line].GetColor();
		if (stopIndex != -1)
		{
			ushort stop = instance.m_lines.m_buffer[(int)line].GetStop(stopIndex);
			if (stop != 0)
			{
				position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)stop].m_position;
				ToolManager expr_68_cp_0 = Singleton<ToolManager>.get_instance();
				expr_68_cp_0.m_drawCallData.m_overlayCalls = expr_68_cp_0.m_drawCallData.m_overlayCalls + 1;
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, 10f, position.y - 100f, position.y + 100f, false, true);
			}
		}
		else if (segmentIndex != -1)
		{
			ToolManager expr_BF_cp_0 = Singleton<ToolManager>.get_instance();
			expr_BF_cp_0.m_drawCallData.m_overlayCalls = expr_BF_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, 5f, position.y - 100f, position.y + 100f, false, true);
		}
	}

	protected override void OnToolUpdate()
	{
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			TransportTool.Mode mode = this.m_mode;
			ushort lastEditLine = this.m_lastEditLine;
			int hoverStopIndex = this.m_hoverStopIndex;
			int hoverSegmentIndex = this.m_hoverSegmentIndex;
			Vector3 hitPosition = this.m_hitPosition;
			string text = null;
			if (this.m_errors != ToolBase.ToolErrors.Pending && this.m_errors != ToolBase.ToolErrors.RaycastFailed)
			{
				if (mode == TransportTool.Mode.NewLine)
				{
					if (hoverStopIndex != -1)
					{
						text = Locale.Get("TOOL_DRAG_STOP");
					}
					else if (hoverSegmentIndex != -1)
					{
						text = Locale.Get("TOOL_DRAG_LINE");
					}
					else
					{
						text = Locale.Get("TOOL_NEW_LINE");
					}
				}
				else if (mode == TransportTool.Mode.AddStops)
				{
					if (lastEditLine != 0)
					{
						ushort stops = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lastEditLine].m_stops;
						if (stops != 0)
						{
							Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)stops].m_position;
							if (Vector3.SqrMagnitude(hitPosition - position) < 6.25f)
							{
								text = Locale.Get("TOOL_CLOSE_LINE");
							}
						}
					}
					if (text == null)
					{
						text = Locale.Get("TOOL_ADD_STOP");
					}
				}
				else if (mode == TransportTool.Mode.MoveStops)
				{
					if (hoverStopIndex != -1)
					{
						text = Locale.Get("TOOL_MOVE_STOP");
					}
					else if (hoverSegmentIndex != -1)
					{
						text = Locale.Get("TOOL_ADD_STOP");
					}
				}
			}
			base.ShowToolInfo(true, text, this.m_hitPosition);
		}
		else
		{
			base.ShowToolInfo(false, null, this.m_hitPosition);
		}
	}

	protected override void OnToolLateUpdate()
	{
		TransportInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		if (this.m_building != 0)
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[this.m_building].Info;
			if (info != null)
			{
				InfoManager.InfoMode mode;
				InfoManager.SubInfoMode subMode;
				info.m_buildingAI.GetPlacementInfoMode(out mode, out subMode, 0f);
				ToolBase.ForceInfoMode(mode, subMode);
			}
		}
		else
		{
			ToolBase.ForceInfoMode(prefab.m_infoMode, prefab.m_subInfoMode);
		}
	}

	[DebuggerHidden]
	private IEnumerator NewLine()
	{
		TransportTool.<NewLine>c__Iterator0 <NewLine>c__Iterator = new TransportTool.<NewLine>c__Iterator0();
		<NewLine>c__Iterator.$this = this;
		return <NewLine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator RemoveStop()
	{
		TransportTool.<RemoveStop>c__Iterator1 <RemoveStop>c__Iterator = new TransportTool.<RemoveStop>c__Iterator1();
		<RemoveStop>c__Iterator.$this = this;
		return <RemoveStop>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator AddStop()
	{
		TransportTool.<AddStop>c__Iterator2 <AddStop>c__Iterator = new TransportTool.<AddStop>c__Iterator2();
		<AddStop>c__Iterator.$this = this;
		return <AddStop>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator<bool> MoveStop(bool applyChanges)
	{
		TransportTool.<MoveStop>c__Iterator3 <MoveStop>c__Iterator = new TransportTool.<MoveStop>c__Iterator3();
		<MoveStop>c__Iterator.applyChanges = applyChanges;
		<MoveStop>c__Iterator.$this = this;
		return <MoveStop>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator CancelPrevStop()
	{
		TransportTool.<CancelPrevStop>c__Iterator4 <CancelPrevStop>c__Iterator = new TransportTool.<CancelPrevStop>c__Iterator4();
		<CancelPrevStop>c__Iterator.$this = this;
		return <CancelPrevStop>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator CancelMoveStop()
	{
		TransportTool.<CancelMoveStop>c__Iterator5 <CancelMoveStop>c__Iterator = new TransportTool.<CancelMoveStop>c__Iterator5();
		<CancelMoveStop>c__Iterator.$this = this;
		return <CancelMoveStop>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator DisableTool()
	{
		TransportTool.<DisableTool>c__Iterator6 <DisableTool>c__Iterator = new TransportTool.<DisableTool>c__Iterator6();
		<DisableTool>c__Iterator.$this = this;
		return <DisableTool>c__Iterator;
	}

	private void ResetTool()
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		if (this.m_lastEditLine != 0)
		{
			TransportLine[] expr_27_cp_0 = instance.m_lines.m_buffer;
			ushort expr_27_cp_1 = this.m_lastEditLine;
			expr_27_cp_0[(int)expr_27_cp_1].m_flags = (expr_27_cp_0[(int)expr_27_cp_1].m_flags & ~(TransportLine.Flags.Hidden | TransportLine.Flags.Selected));
		}
		if (this.m_tempLine != 0 && (instance.m_lines.m_buffer[(int)this.m_tempLine].m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
		{
			instance.m_lines.m_buffer[(int)this.m_tempLine].CloneLine(this.m_tempLine, 0);
			instance.m_lines.m_buffer[(int)this.m_tempLine].UpdateMeshData(this.m_tempLine);
			TransportLine[] expr_BC_cp_0 = instance.m_lines.m_buffer;
			ushort expr_BC_cp_1 = this.m_tempLine;
			expr_BC_cp_0[(int)expr_BC_cp_1].m_flags = (expr_BC_cp_0[(int)expr_BC_cp_1].m_flags | TransportLine.Flags.Hidden);
		}
		this.m_tempLine = 0;
		this.m_lastEditLine = 0;
		this.m_lastMoveIndex = -2;
		this.m_lastAddIndex = -2;
		this.m_lastMovePos = Vector3.get_zero();
		this.m_lastAddPos = Vector3.get_zero();
		this.m_line = 0;
		this.m_mode = TransportTool.Mode.NewLine;
		this.m_errors = ToolBase.ToolErrors.Pending;
		this.m_mouseRayValid = false;
	}

	public void StartEditingBuildingLine(TransportInfo info, ushort buildingID)
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		this.EnsureTempLine(info, 0, -2, -2, Vector3.get_zero(), false);
		ushort num = TransportTool.FindBuildingLine(buildingID);
		if (num == 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			SimulationManager instance3 = Singleton<SimulationManager>.get_instance();
			VehicleManager instance4 = Singleton<VehicleManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)buildingID].Info;
			if (info2 != null)
			{
				VehicleInfo randomVehicleInfo = instance4.GetRandomVehicleInfo(ref instance3.m_randomizer, info2.m_class.m_service, info2.m_class.m_subService, info2.m_class.m_level);
				if (randomVehicleInfo != null)
				{
					Vector3 position;
					Vector3 vector;
					info2.m_buildingAI.CalculateSpawnPosition(buildingID, ref instance2.m_buildings.m_buffer[(int)buildingID], ref instance3.m_randomizer, randomVehicleInfo, out position, out vector);
					if (instance.CreateLine(out num, ref instance3.m_randomizer, info, true))
					{
						instance.m_lines.m_buffer[(int)num].SetActive(false, false);
						instance.m_lines.m_buffer[(int)num].m_building = buildingID;
						if (!instance.m_lines.m_buffer[(int)num].AddStop(num, 0, position, false))
						{
							instance.ReleaseLine(num);
							num = 0;
						}
					}
				}
			}
		}
		if (num != 0 && !instance.m_lines.m_buffer[(int)num].Complete)
		{
			this.m_line = num;
			this.m_mode = TransportTool.Mode.AddStops;
			this.m_lastMoveIndex = -2;
			this.m_lastAddIndex = -2;
		}
	}

	public static ushort FindBuildingLine(ushort buildingID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)buildingID].m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)num].Info;
			if (info.m_class.m_layer == ItemClass.Layer.PublicTransport)
			{
				ushort transportLine = instance.m_nodes.m_buffer[(int)num].m_transportLine;
				if (transportLine != 0)
				{
					return transportLine;
				}
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public override void SimulationStep()
	{
		TransportInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		if (this.m_line != 0 && (Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_line].m_flags & (TransportLine.Flags.Created | TransportLine.Flags.Deleted)) != TransportLine.Flags.Created)
		{
			this.ResetTool();
		}
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		switch (this.m_mode)
		{
		case TransportTool.Mode.NewLine:
		{
			Vector3 zero = Vector3.get_zero();
			ushort num = 0;
			int num2 = -1;
			int hoverSegmentIndex = -1;
			bool flag = false;
			if (this.m_mouseRayValid)
			{
				int transportTypes = 1 << (int)prefab.m_transportType;
				flag = Singleton<TransportManager>.get_instance().RayCast(this.m_mouseRay, this.m_mouseRayLength, transportTypes, out zero, out num, out num2, out hoverSegmentIndex);
			}
			if (flag)
			{
				TransportInfo info = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num].Info;
				if (info == prefab)
				{
					bool flag2 = this.m_building == 0 || num2 != 0;
					if (flag2)
					{
						flag2 = this.EnsureTempLine(prefab, num, -2, -2, zero, false);
					}
					if (flag2)
					{
						this.m_hitPosition = zero;
						this.m_fixedPlatform = false;
						this.m_hoverStopIndex = num2;
						this.m_hoverSegmentIndex = hoverSegmentIndex;
						if (this.m_hoverSegmentIndex != -1 && !Singleton<NetManager>.get_instance().CheckLimits())
						{
							toolErrors |= ToolBase.ToolErrors.TooManyObjects;
						}
					}
					else
					{
						this.EnsureTempLine(prefab, 0, -2, -2, Vector3.get_zero(), false);
						this.m_hoverStopIndex = -1;
						this.m_hoverSegmentIndex = -1;
						toolErrors |= ToolBase.ToolErrors.RaycastFailed;
					}
				}
				else
				{
					flag = false;
				}
			}
			if (!flag)
			{
				ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
				input.m_buildingService = new ToolBase.RaycastService(prefab.m_stationService, prefab.m_stationSubService, prefab.m_stationLayer);
				input.m_netService = new ToolBase.RaycastService(prefab.m_netService, prefab.m_netSubService, prefab.m_netLayer);
				input.m_ignoreTerrain = true;
				input.m_ignoreSegmentFlags = ((prefab.m_netService == ItemClass.Service.None) ? NetSegment.Flags.All : NetSegment.Flags.None);
				input.m_ignoreBuildingFlags = ((prefab.m_stationService == ItemClass.Service.None) ? Building.Flags.All : Building.Flags.None);
				ToolBase.RaycastOutput raycastOutput = default(ToolBase.RaycastOutput);
				bool flag3 = false;
				if (this.m_mouseRayValid && this.m_building == 0)
				{
					flag3 = ToolBase.RayCast(input, out raycastOutput);
				}
				bool fixedPlatform = false;
				if (flag3)
				{
					flag3 = this.GetStopPosition(prefab, raycastOutput.m_netSegment, raycastOutput.m_building, 0, ref raycastOutput.m_hitPos, out fixedPlatform);
				}
				if (flag3)
				{
					flag3 = this.CanAddStop(prefab, 0, -1, raycastOutput.m_hitPos);
				}
				if (flag3)
				{
					flag3 = this.EnsureTempLine(prefab, 0, -2, -1, raycastOutput.m_hitPos, fixedPlatform);
				}
				if (flag3)
				{
					this.m_hitPosition = raycastOutput.m_hitPos;
					this.m_fixedPlatform = fixedPlatform;
					this.m_hoverStopIndex = -1;
					this.m_hoverSegmentIndex = -1;
					if (!Singleton<NetManager>.get_instance().CheckLimits())
					{
						toolErrors |= ToolBase.ToolErrors.TooManyObjects;
					}
					if (!Singleton<TransportManager>.get_instance().CheckLimits())
					{
						toolErrors |= ToolBase.ToolErrors.TooManyObjects;
					}
				}
				else
				{
					this.EnsureTempLine(prefab, 0, -2, -2, Vector3.get_zero(), fixedPlatform);
					this.m_hoverStopIndex = -1;
					this.m_hoverSegmentIndex = -1;
					toolErrors |= ToolBase.ToolErrors.RaycastFailed;
				}
			}
			break;
		}
		case TransportTool.Mode.AddStops:
			if (this.m_line == 0)
			{
				this.m_mode = TransportTool.Mode.NewLine;
				toolErrors |= ToolBase.ToolErrors.RaycastFailed;
			}
			else
			{
				TransportManager instance = Singleton<TransportManager>.get_instance();
				ToolBase.RaycastInput input2 = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
				input2.m_buildingService = new ToolBase.RaycastService(prefab.m_stationService, prefab.m_stationSubService, prefab.m_stationLayer);
				input2.m_netService = new ToolBase.RaycastService(prefab.m_netService, prefab.m_netSubService, prefab.m_netLayer);
				input2.m_ignoreTerrain = true;
				input2.m_ignoreSegmentFlags = ((prefab.m_netService == ItemClass.Service.None) ? NetSegment.Flags.All : NetSegment.Flags.None);
				input2.m_ignoreBuildingFlags = ((prefab.m_stationService == ItemClass.Service.None) ? Building.Flags.All : Building.Flags.None);
				ToolBase.RaycastOutput raycastOutput2 = default(ToolBase.RaycastOutput);
				bool flag4 = false;
				if (this.m_mouseRayValid)
				{
					flag4 = ToolBase.RayCast(input2, out raycastOutput2);
				}
				bool fixedPlatform2 = false;
				if (flag4)
				{
					ushort firstStop = 0;
					if (this.m_line != 0 && !instance.m_lines.m_buffer[(int)this.m_line].Complete)
					{
						firstStop = instance.m_lines.m_buffer[(int)this.m_line].m_stops;
					}
					flag4 = this.GetStopPosition(prefab, raycastOutput2.m_netSegment, raycastOutput2.m_building, firstStop, ref raycastOutput2.m_hitPos, out fixedPlatform2);
				}
				if (flag4)
				{
					flag4 = this.CanAddStop(prefab, this.m_line, -1, raycastOutput2.m_hitPos);
				}
				if (flag4)
				{
					flag4 = this.EnsureTempLine(prefab, this.m_line, -2, -1, raycastOutput2.m_hitPos, fixedPlatform2);
				}
				if (flag4)
				{
					this.m_hitPosition = raycastOutput2.m_hitPos;
					this.m_fixedPlatform = fixedPlatform2;
					if (!Singleton<NetManager>.get_instance().CheckLimits())
					{
						toolErrors |= ToolBase.ToolErrors.TooManyObjects;
					}
					instance.UpdateLinesNow();
					bool flag5;
					if (this.m_tempLine != 0 && !instance.m_lines.m_buffer[(int)this.m_tempLine].CheckPrevPath(-1, out flag5) && flag5)
					{
						toolErrors |= ToolBase.ToolErrors.PathNotFound;
					}
				}
				else
				{
					this.EnsureTempLine(prefab, this.m_line, -2, -2, Vector3.get_zero(), fixedPlatform2);
					toolErrors |= ToolBase.ToolErrors.RaycastFailed;
				}
			}
			break;
		case TransportTool.Mode.MoveStops:
			if (this.m_line == 0)
			{
				this.m_mode = TransportTool.Mode.NewLine;
				toolErrors |= ToolBase.ToolErrors.RaycastFailed;
			}
			else
			{
				ToolBase.RaycastInput input3 = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
				input3.m_buildingService = new ToolBase.RaycastService(prefab.m_stationService, prefab.m_stationSubService, prefab.m_stationLayer);
				input3.m_netService = new ToolBase.RaycastService(prefab.m_netService, prefab.m_netSubService, prefab.m_netLayer);
				input3.m_ignoreTerrain = true;
				input3.m_ignoreSegmentFlags = ((prefab.m_netService == ItemClass.Service.None) ? NetSegment.Flags.All : NetSegment.Flags.None);
				input3.m_ignoreBuildingFlags = ((prefab.m_stationService == ItemClass.Service.None) ? Building.Flags.All : Building.Flags.None);
				ToolBase.RaycastOutput raycastOutput3 = default(ToolBase.RaycastOutput);
				bool flag6 = false;
				if (this.m_mouseRayValid)
				{
					flag6 = ToolBase.RayCast(input3, out raycastOutput3);
				}
				bool fixedPlatform3 = false;
				if (flag6)
				{
					flag6 = this.GetStopPosition(prefab, raycastOutput3.m_netSegment, raycastOutput3.m_building, 0, ref raycastOutput3.m_hitPos, out fixedPlatform3);
				}
				if (this.m_hoverStopIndex != -1)
				{
					if (flag6)
					{
						flag6 = this.CanMoveStop(prefab, this.m_line, this.m_hoverStopIndex, raycastOutput3.m_hitPos);
					}
					if (flag6)
					{
						flag6 = this.EnsureTempLine(prefab, this.m_line, this.m_hoverStopIndex, -2, raycastOutput3.m_hitPos, fixedPlatform3);
					}
				}
				else if (this.m_hoverSegmentIndex != -1)
				{
					if (flag6)
					{
						flag6 = this.CanAddStop(prefab, this.m_line, this.m_hoverSegmentIndex + 1, raycastOutput3.m_hitPos);
					}
					if (flag6)
					{
						flag6 = this.EnsureTempLine(prefab, this.m_line, -2, this.m_hoverSegmentIndex + 1, raycastOutput3.m_hitPos, fixedPlatform3);
					}
				}
				if (flag6)
				{
					this.m_hitPosition = raycastOutput3.m_hitPos;
					this.m_fixedPlatform = fixedPlatform3;
					if (this.m_hoverSegmentIndex != -1 && !Singleton<NetManager>.get_instance().CheckLimits())
					{
						toolErrors |= ToolBase.ToolErrors.TooManyObjects;
					}
					TransportManager instance2 = Singleton<TransportManager>.get_instance();
					instance2.UpdateLinesNow();
					if (this.m_tempLine != 0)
					{
						if (this.m_hoverStopIndex != -1)
						{
							bool flag7;
							if (!instance2.m_lines.m_buffer[(int)this.m_tempLine].CheckPrevPath(this.m_hoverStopIndex, out flag7) && flag7 && instance2.m_lines.m_buffer[(int)this.m_line].CheckPrevPath(this.m_hoverStopIndex, out flag7))
							{
								toolErrors |= ToolBase.ToolErrors.PathNotFound;
							}
							if (!instance2.m_lines.m_buffer[(int)this.m_tempLine].CheckNextPath(this.m_hoverStopIndex, out flag7) && flag7 && instance2.m_lines.m_buffer[(int)this.m_line].CheckNextPath(this.m_hoverStopIndex, out flag7))
							{
								toolErrors |= ToolBase.ToolErrors.PathNotFound;
							}
						}
						else if (this.m_hoverSegmentIndex != -1)
						{
							bool flag7;
							if (!instance2.m_lines.m_buffer[(int)this.m_tempLine].CheckPrevPath(this.m_hoverSegmentIndex + 1, out flag7) && flag7)
							{
								toolErrors |= ToolBase.ToolErrors.PathNotFound;
							}
							if (!instance2.m_lines.m_buffer[(int)this.m_tempLine].CheckNextPath(this.m_hoverSegmentIndex + 1, out flag7) && flag7)
							{
								toolErrors |= ToolBase.ToolErrors.PathNotFound;
							}
						}
					}
				}
				else
				{
					this.EnsureTempLine(prefab, this.m_line, -2, -2, Vector3.get_zero(), fixedPlatform3);
					toolErrors |= ToolBase.ToolErrors.RaycastFailed;
				}
			}
			break;
		default:
			toolErrors |= ToolBase.ToolErrors.RaycastFailed;
			break;
		}
		this.m_errors = toolErrors;
	}

	private bool CanMoveStop(TransportInfo info, ushort sourceLine, int moveIndex, Vector3 movePos)
	{
		return sourceLine != 0 && Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)sourceLine].CanMoveStop(sourceLine, moveIndex, movePos);
	}

	private bool CanAddStop(TransportInfo info, ushort sourceLine, int addIndex, Vector3 addPos)
	{
		return sourceLine == 0 || Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)sourceLine].CanAddStop(sourceLine, addIndex, addPos);
	}

	private bool EnsureTempLine(TransportInfo info, ushort sourceLine, int moveIndex, int addIndex, Vector3 addPos, bool fixedPlatform)
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		if (this.m_tempLine != 0)
		{
			if ((instance.m_lines.m_buffer[(int)this.m_tempLine].m_flags & TransportLine.Flags.Temporary) == TransportLine.Flags.None)
			{
				this.m_tempLine = 0;
				this.SetEditLine(0, true);
			}
			else if (instance.m_lines.m_buffer[(int)this.m_tempLine].Info != info)
			{
				instance.ReleaseLine(this.m_tempLine);
				this.m_tempLine = 0;
				this.SetEditLine(0, true);
			}
		}
		if (this.m_tempLine == 0)
		{
			for (int i = 1; i < 256; i++)
			{
				if ((instance.m_lines.m_buffer[i].m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
				{
					if (instance.m_lines.m_buffer[i].Info != info)
					{
						instance.ReleaseLine((ushort)i);
					}
					else
					{
						this.m_tempLine = (ushort)i;
						this.SetEditLine(sourceLine, true);
					}
					break;
				}
			}
		}
		if (this.m_tempLine == 0 && Singleton<TransportManager>.get_instance().CreateLine(out this.m_tempLine, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, false))
		{
			TransportLine[] expr_141_cp_0 = instance.m_lines.m_buffer;
			ushort expr_141_cp_1 = this.m_tempLine;
			expr_141_cp_0[(int)expr_141_cp_1].m_flags = (expr_141_cp_0[(int)expr_141_cp_1].m_flags | TransportLine.Flags.Temporary);
			this.SetEditLine(sourceLine, true);
		}
		if (this.m_tempLine != 0)
		{
			this.SetEditLine(sourceLine, false);
			if (this.m_lastMoveIndex != moveIndex || this.m_lastAddIndex != addIndex || this.m_lastAddPos != addPos)
			{
				if (this.m_lastAddIndex != -2 && instance.m_lines.m_buffer[(int)this.m_tempLine].RemoveStop(this.m_tempLine, this.m_lastAddIndex))
				{
					this.m_lastAddIndex = -2;
					this.m_lastAddPos = Vector3.get_zero();
				}
				if (this.m_lastMoveIndex != -2 && instance.m_lines.m_buffer[(int)this.m_tempLine].MoveStop(this.m_tempLine, this.m_lastMoveIndex, this.m_lastMovePos, fixedPlatform))
				{
					this.m_lastMoveIndex = -2;
					this.m_lastMovePos = Vector3.get_zero();
				}
				instance.m_lines.m_buffer[(int)this.m_tempLine].CopyMissingPaths(sourceLine);
				Vector3 lastMovePos;
				if (moveIndex != -2 && instance.m_lines.m_buffer[(int)this.m_tempLine].MoveStop(this.m_tempLine, moveIndex, addPos, fixedPlatform, out lastMovePos))
				{
					this.m_lastMoveIndex = moveIndex;
					this.m_lastMovePos = lastMovePos;
					this.m_lastAddPos = addPos;
				}
				if (addIndex != -2 && instance.m_lines.m_buffer[(int)this.m_tempLine].AddStop(this.m_tempLine, addIndex, addPos, fixedPlatform))
				{
					this.m_lastAddIndex = addIndex;
					this.m_lastAddPos = addPos;
				}
				instance.UpdateLine(this.m_tempLine);
			}
			instance.m_lines.m_buffer[(int)this.m_tempLine].m_color = instance.m_lines.m_buffer[(int)sourceLine].m_color;
			TransportLine[] expr_333_cp_0 = instance.m_lines.m_buffer;
			ushort expr_333_cp_1 = this.m_tempLine;
			expr_333_cp_0[(int)expr_333_cp_1].m_flags = (expr_333_cp_0[(int)expr_333_cp_1].m_flags & ~TransportLine.Flags.Hidden);
			if ((instance.m_lines.m_buffer[(int)sourceLine].m_flags & TransportLine.Flags.CustomColor) != TransportLine.Flags.None)
			{
				TransportLine[] expr_378_cp_0 = instance.m_lines.m_buffer;
				ushort expr_378_cp_1 = this.m_tempLine;
				expr_378_cp_0[(int)expr_378_cp_1].m_flags = (expr_378_cp_0[(int)expr_378_cp_1].m_flags | TransportLine.Flags.CustomColor);
			}
			else
			{
				TransportLine[] expr_3A4_cp_0 = instance.m_lines.m_buffer;
				ushort expr_3A4_cp_1 = this.m_tempLine;
				expr_3A4_cp_0[(int)expr_3A4_cp_1].m_flags = (expr_3A4_cp_0[(int)expr_3A4_cp_1].m_flags & ~TransportLine.Flags.CustomColor);
			}
			return true;
		}
		this.SetEditLine(0, false);
		return false;
	}

	private void SetEditLine(ushort line, bool forceRefresh)
	{
		if (line != this.m_lastEditLine || forceRefresh)
		{
			TransportManager instance = Singleton<TransportManager>.get_instance();
			if (this.m_lastEditLine != 0)
			{
				TransportLine[] expr_39_cp_0 = instance.m_lines.m_buffer;
				ushort expr_39_cp_1 = this.m_lastEditLine;
				expr_39_cp_0[(int)expr_39_cp_1].m_flags = (expr_39_cp_0[(int)expr_39_cp_1].m_flags & ~(TransportLine.Flags.Hidden | TransportLine.Flags.Selected));
			}
			this.m_lastEditLine = line;
			this.m_lastMoveIndex = -2;
			this.m_lastAddIndex = -2;
			this.m_lastMovePos = Vector3.get_zero();
			this.m_lastAddPos = Vector3.get_zero();
			if (this.m_lastEditLine != 0)
			{
				TransportLine[] expr_95_cp_0 = instance.m_lines.m_buffer;
				ushort expr_95_cp_1 = this.m_lastEditLine;
				expr_95_cp_0[(int)expr_95_cp_1].m_flags = (expr_95_cp_0[(int)expr_95_cp_1].m_flags | (TransportLine.Flags.Hidden | TransportLine.Flags.Selected));
			}
			if (this.m_tempLine != 0)
			{
				instance.m_lines.m_buffer[(int)this.m_tempLine].CloneLine(this.m_tempLine, this.m_lastEditLine);
				instance.m_lines.m_buffer[(int)this.m_tempLine].UpdateMeshData(this.m_tempLine);
			}
		}
	}

	private bool GetStopPosition(TransportInfo info, ushort segment, ushort building, ushort firstStop, ref Vector3 hitPos, out bool fixedPlatform)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		fixedPlatform = false;
		if (segment != 0)
		{
			if ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
			{
				building = NetSegment.FindOwnerBuilding(segment, 363f);
				if (building != 0)
				{
					BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)building].Info;
					TransportInfo transportLineInfo = info2.m_buildingAI.GetTransportLineInfo();
					TransportInfo secondaryTransportLineInfo = info2.m_buildingAI.GetSecondaryTransportLineInfo();
					if ((transportLineInfo != null && transportLineInfo.m_transportType == info.m_transportType) || (secondaryTransportLineInfo != null && secondaryTransportLineInfo.m_transportType == info.m_transportType))
					{
						segment = 0;
					}
					else
					{
						building = 0;
					}
				}
			}
			Vector3 point;
			uint num;
			int num2;
			float num3;
			Vector3 vector;
			uint num4;
			int num5;
			float num6;
			if (segment != 0 && instance.m_segments.m_buffer[(int)segment].GetClosestLanePosition(hitPos, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, info.m_vehicleType, out point, out num, out num2, out num3) && instance.m_segments.m_buffer[(int)segment].GetClosestLanePosition(point, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, out vector, out num4, out num5, out num6))
			{
				NetLane.Flags flags = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_flags;
				if ((flags & NetLane.Flags.Stops & ~(info.m_stopFlag != NetLane.Flags.None)) != NetLane.Flags.None)
				{
					return false;
				}
				NetInfo.Lane lane = instance.m_segments.m_buffer[(int)segment].Info.m_lanes[num5];
				float num7 = lane.m_stopOffset;
				if ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
				{
					num7 = -num7;
				}
				Vector3 vector2;
				instance.m_lanes.m_buffer[(int)((UIntPtr)num4)].CalculateStopPositionAndDirection(0.5019608f, num7, out hitPos, out vector2);
				fixedPlatform = true;
				return true;
			}
		}
		if (building != 0)
		{
			ushort num8 = 0;
			if ((instance2.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.Untouchable) != Building.Flags.None)
			{
				num8 = Building.FindParentBuilding(building);
			}
			if (this.m_building != 0 && firstStop != 0 && (this.m_building == (int)building || this.m_building == (int)num8))
			{
				hitPos = instance.m_nodes.m_buffer[(int)firstStop].m_position;
				return true;
			}
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				BuildingInfo info3 = instance2.m_buildings.m_buffer[(int)building].Info;
				TransportInfo transportLineInfo2 = info3.m_buildingAI.GetTransportLineInfo();
				if (transportLineInfo2 == null && num8 != 0)
				{
					building = num8;
					info3 = instance2.m_buildings.m_buffer[(int)building].Info;
					transportLineInfo2 = info3.m_buildingAI.GetTransportLineInfo();
				}
				TransportInfo secondaryTransportLineInfo2 = info3.m_buildingAI.GetSecondaryTransportLineInfo();
				if ((transportLineInfo2 != null && transportLineInfo2.m_transportType == info.m_transportType) || (secondaryTransportLineInfo2 != null && secondaryTransportLineInfo2.m_transportType == info.m_transportType))
				{
					Vector3 vector3 = Vector3.get_zero();
					int num9 = 1000000;
					for (int i = 0; i < 12; i++)
					{
						Randomizer randomizer;
						randomizer..ctor((ulong)((long)i));
						Vector3 vector4;
						Vector3 vector5;
						info3.m_buildingAI.CalculateSpawnPosition(building, ref instance2.m_buildings.m_buffer[(int)building], ref randomizer, randomVehicleInfo, out vector4, out vector5);
						int num10 = 0;
						if (info.m_avoidSameStopPlatform)
						{
							num10 = this.GetLineCount(vector4, vector5 - vector4, info.m_transportType);
						}
						if (num10 < num9)
						{
							vector3 = vector4;
							num9 = num10;
						}
						else if (num10 == num9 && Vector3.SqrMagnitude(vector4 - hitPos) < Vector3.SqrMagnitude(vector3 - hitPos))
						{
							vector3 = vector4;
						}
					}
					if (firstStop != 0)
					{
						Vector3 position = instance.m_nodes.m_buffer[(int)firstStop].m_position;
						if (Vector3.SqrMagnitude(position - vector3) < 16384f)
						{
							uint lane2 = instance.m_nodes.m_buffer[(int)firstStop].m_lane;
							if (lane2 != 0u)
							{
								ushort segment2 = instance.m_lanes.m_buffer[(int)((UIntPtr)lane2)].m_segment;
								if (segment2 != 0 && (instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
								{
									ushort num11 = NetSegment.FindOwnerBuilding(segment2, 363f);
									if (building == num11)
									{
										hitPos = position;
										return true;
									}
								}
							}
						}
					}
					hitPos = vector3;
					return num9 != 1000000;
				}
			}
		}
		return false;
	}

	private int GetLineCount(Vector3 stopPosition, Vector3 stopDirection, TransportInfo.TransportType transportType)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		TransportManager instance2 = Singleton<TransportManager>.get_instance();
		stopDirection.Normalize();
		Segment3 segment;
		segment..ctor(stopPosition - stopDirection * 16f, stopPosition + stopDirection * 16f);
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 4f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.z - 4f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 4f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.z + 4f) / 64f + 135f), 269);
		int num5 = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_nodeGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort transportLine = instance.m_nodes.m_buffer[(int)num6].m_transportLine;
					if (transportLine != 0)
					{
						TransportInfo info = instance2.m_lines.m_buffer[(int)transportLine].Info;
						if (info.m_transportType == transportType && (instance2.m_lines.m_buffer[(int)transportLine].m_flags & TransportLine.Flags.Temporary) == TransportLine.Flags.None && segment.DistanceSqr(instance.m_nodes.m_buffer[(int)num6].m_position) < 16f)
						{
							num5++;
						}
					}
					num6 = instance.m_nodes.m_buffer[(int)num6].m_nextGridNode;
					if (++num7 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num5;
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_errors;
	}

	public TransportInfo m_prefab;

	public int m_building;

	private TransportTool.Mode m_mode;

	private ushort m_line;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	protected bool m_mouseRayValid;

	private Vector3 m_hitPosition;

	private bool m_fixedPlatform;

	private ushort m_tempLine;

	private ushort m_lastEditLine;

	private int m_lastMoveIndex;

	private int m_lastAddIndex;

	private Vector3 m_lastMovePos;

	private Vector3 m_lastAddPos;

	private int m_hoverStopIndex;

	private int m_hoverSegmentIndex;

	private ToolBase.ToolErrors m_errors;

	private enum Mode
	{
		NewLine,
		AddStops,
		MoveStops
	}
}
