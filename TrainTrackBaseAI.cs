﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TrainTrackBaseAI : PlayerNetAI
{
	public override void RenderNode(ushort nodeID, ref NetNode nodeData, RenderManager.CameraInfo cameraInfo)
	{
		if ((nodeData.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower && (this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
		{
			Vector3 position = nodeData.m_position;
			position.y += this.GetSnapElevation();
			float x = Vector3.Distance(cameraInfo.m_position, position);
			float num = MathUtils.SmoothStep(1000f, 500f, x);
			if (num < 0.001f)
			{
				return;
			}
			bool flag = (nodeData.m_flags & NetNode.Flags.OneWayIn) == NetNode.Flags.None;
			if (!flag)
			{
				return;
			}
			InstanceID instanceID;
			int num2;
			Singleton<ToolManager>.get_instance().m_properties.GetComponent<DefaultTool>().GetHoverInstance(out instanceID, out num2);
			if (flag)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				float num3 = this.GetCollisionHalfWidth() * 0.75f;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = nodeData.GetSegment(i);
					if (segment != 0)
					{
						NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
						bool flag2 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
						bool flag3 = (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
						if ((flag2 != flag3) ? info.m_hasBackwardVehicleLanes : info.m_hasForwardVehicleLanes)
						{
							float scale = (instanceID.NetNode != nodeID || num2 != i + 1) ? 0.75f : 1f;
							Vector3 vector = (!flag2) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
							Vector3 position2 = position + vector * num3;
							NetSegment.Flags flags = (!flag2) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
							if ((instance.m_segments.m_buffer[(int)segment].m_flags & flags) != NetSegment.Flags.None)
							{
								NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.YieldOn, position2, scale, num);
							}
							else
							{
								NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.YieldOff, position2, scale, num);
							}
						}
					}
				}
			}
		}
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
			int num2 = this.m_noiseAccumulation * num / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num2 * 1.25f);
		}
		if (infoMode != InfoManager.InfoMode.Transport)
		{
			if (infoMode == InfoManager.InfoMode.None)
			{
				Color color = this.m_info.m_color;
				if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
				{
					float num3 = 0.5f;
					color.r = color.r * (1f - num3) + num3 * 0.75f;
					color.g = color.g * (1f - num3) + num3 * 0.75f;
					color.b = color.b * (1f - num3) + num3 * 0.75f;
				}
				color.a = (float)(255 - data.m_wetness) * 0.003921569f;
				return color;
			}
			if (infoMode == InfoManager.InfoMode.Traffic)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)data.m_trafficDensity * 0.01f));
			}
			if (infoMode != InfoManager.InfoMode.Destruction)
			{
				return base.GetColor(segmentID, ref data, infoMode);
			}
			if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (this.m_info.m_class.m_subService == ItemClass.SubService.PublicTransportMonorail)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[8], 0.2f);
			}
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[2], 0.2f);
		}
	}

	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Traffic || base.ColorizeProps(infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num = 0;
			if (this.m_noiseAccumulation != 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				int num2 = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0)
					{
						num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
						num2++;
					}
				}
				if (num2 != 0)
				{
					num /= num2;
				}
			}
			int num3 = 100 - (num - 100) * (num - 100) / 100;
			int num4 = this.m_noiseAccumulation * num3 / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num4 * 1.25f);
		}
		if (infoMode != InfoManager.InfoMode.Transport)
		{
			if (infoMode == InfoManager.InfoMode.None)
			{
				int num5 = 0;
				int num6 = 0;
				NetManager instance2 = Singleton<NetManager>.get_instance();
				int num7 = 0;
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = data.GetSegment(j);
					if (segment2 != 0)
					{
						if ((instance2.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
						{
							num5++;
						}
						num6 += (int)instance2.m_segments.m_buffer[(int)segment2].m_wetness;
						num7++;
					}
				}
				if (num7 != 0)
				{
					num6 /= num7;
				}
				Color color = this.m_info.m_color;
				if (num5 != 0)
				{
					float num8 = (float)num5 / (float)num7 * 0.5f;
					color.r = color.r * (1f - num8) + num8 * 0.75f;
					color.g = color.g * (1f - num8) + num8 * 0.75f;
					color.b = color.b * (1f - num8) + num8 * 0.75f;
				}
				color.a = (float)(255 - num6) * 0.003921569f;
				return color;
			}
			if (infoMode == InfoManager.InfoMode.Traffic)
			{
				int num9 = 0;
				NetManager instance3 = Singleton<NetManager>.get_instance();
				int num10 = 0;
				for (int k = 0; k < 8; k++)
				{
					ushort segment3 = data.GetSegment(k);
					if (segment3 != 0)
					{
						num9 += (int)instance3.m_segments.m_buffer[(int)segment3].m_trafficDensity;
						num10++;
					}
				}
				if (num10 != 0)
				{
					num9 /= num10;
				}
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Mathf.Clamp01((float)num9 * 0.01f));
			}
			if (infoMode != InfoManager.InfoMode.Destruction)
			{
				return base.GetColor(nodeID, ref data, infoMode);
			}
			NetManager instance4 = Singleton<NetManager>.get_instance();
			bool flag = false;
			for (int l = 0; l < 8; l++)
			{
				ushort segment4 = data.GetSegment(l);
				if (segment4 != 0 && (instance4.m_segments.m_buffer[(int)segment4].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
				{
					flag = true;
				}
			}
			if (flag)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (this.m_info.m_class.m_subService == ItemClass.SubService.PublicTransportMonorail)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[8], 0.2f);
			}
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<TransportManager>.get_instance().m_properties.m_transportColors[2], 0.2f);
		}
	}

	public override void GetNodeState(ushort nodeID, ref NetNode nodeData, ushort segmentID, ref NetSegment segmentData, out NetNode.Flags flags, out Color color)
	{
		flags = nodeData.m_flags;
		color = Color.get_gray().get_gamma();
		if ((nodeData.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
		{
			TrainTrackBaseAI.GetLevelCrossingNodeState(nodeID, ref nodeData, segmentID, ref segmentData, ref flags, ref color);
		}
	}

	public static void GetLevelCrossingNodeState(ushort nodeID, ref NetNode nodeData, ushort segmentID, ref NetSegment segmentData, ref NetNode.Flags flags, ref Color color)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - 15u;
		uint num2 = (uint)(((int)nodeID << 8) / 32768);
		uint num3 = num - num2 & 255u;
		float num4 = num3 + Singleton<SimulationManager>.get_instance().m_referenceTimer;
		RoadBaseAI.TrafficLightState trafficLightState;
		RoadBaseAI.TrafficLightState trafficLightState2;
		RoadBaseAI.GetTrafficLightState(nodeID, ref segmentData, num - num2, out trafficLightState, out trafficLightState2);
		color.a = 0.5f;
		switch (trafficLightState)
		{
		case RoadBaseAI.TrafficLightState.Green:
			color.g = 1f;
			color.a = 0.5f;
			break;
		case RoadBaseAI.TrafficLightState.RedToGreen:
			if (num3 < 45u)
			{
				color.g = 0f;
			}
			else if (num3 < 60u)
			{
				color.r = 1f;
			}
			else
			{
				color.g = 1f;
			}
			color.a = 0.5f + Mathf.Clamp((90f - num4) / 180f, 0f, 0.5f);
			break;
		case RoadBaseAI.TrafficLightState.Red:
			color.g = 0f;
			color.a = 1f;
			break;
		case RoadBaseAI.TrafficLightState.GreenToRed:
			if (num3 < 45u)
			{
				color.r = 1f;
			}
			else
			{
				color.g = 0f;
			}
			color.a = 0.5f + Mathf.Clamp(num4 / 180f, 0f, 0.5f);
			break;
		}
		if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
		{
			color.a = 1f - color.a;
		}
	}

	public override void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
		base.NodeLoaded(nodeID, ref data, version);
		Singleton<NetManager>.get_instance().AddTileNode(data.m_position, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
	}

	public override int RayCastNodeButton(ushort nodeID, ref NetNode data, Segment3 ray)
	{
		if ((data.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower && (this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
		{
			Vector3 position = data.m_position;
			position.y += this.GetSnapElevation();
			float num = Vector3.Distance(ray.a, position);
			if (num < 1000f)
			{
				bool flag = (data.m_flags & NetNode.Flags.OneWayIn) == NetNode.Flags.None;
				float num2 = 0.0675f * Mathf.Pow(num, 0.65f);
				if (flag)
				{
					NetManager instance = Singleton<NetManager>.get_instance();
					float num3 = this.GetCollisionHalfWidth() * 0.75f;
					for (int i = 0; i < 8; i++)
					{
						ushort segment = data.GetSegment(i);
						if (segment != 0)
						{
							NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
							bool flag2 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
							bool flag3 = (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
							if ((flag2 != flag3) ? info.m_hasBackwardVehicleLanes : info.m_hasForwardVehicleLanes)
							{
								Vector3 vector = (!flag2) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
								Vector3 vector2 = position + vector * num3;
								if (ray.DistanceSqr(vector2) < num2 * num2)
								{
									return i + 1;
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}

	public override void ClickNodeButton(ushort nodeID, ref NetNode data, int index)
	{
		if ((data.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower && (this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None && index >= 1 && index <= 8 && (data.m_flags & NetNode.Flags.OneWayIn) == NetNode.Flags.None)
		{
			ushort segment = data.GetSegment(index - 1);
			if (segment != 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				bool flag = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
				NetSegment.Flags flags = (!flag) ? NetSegment.Flags.YieldEnd : NetSegment.Flags.YieldStart;
				NetSegment[] expr_BA_cp_0 = instance.m_segments.m_buffer;
				ushort expr_BA_cp_1 = segment;
				expr_BA_cp_0[(int)expr_BA_cp_1].m_flags = (expr_BA_cp_0[(int)expr_BA_cp_1].m_flags ^ flags);
				instance.m_segments.m_buffer[(int)segment].UpdateLanes(segment, true);
				Singleton<NetManager>.get_instance().m_yieldLights.Disable();
			}
		}
	}

	public override void ManualActivation(ushort segmentID, ref NetSegment data, NetInfo oldInfo)
	{
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			Vector3 position3 = (position + position2) * 0.5f;
			float num = Vector3.Distance(position, position2);
			float num2 = (float)this.m_noiseAccumulation * num / this.m_noiseRadius;
			float num3 = Mathf.Max(num, this.m_noiseRadius);
			if (oldInfo != null)
			{
				int num4;
				float num5;
				oldInfo.m_netAI.GetNoiseAccumulation(out num4, out num5);
				if (num4 != 0)
				{
					num2 -= (float)num4 * num / num5;
					num3 = Mathf.Max(num3, num5);
				}
			}
			if (num2 != 0f)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position3, (num2 <= 0f) ? NotificationEvent.Type.Happy : NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, num2, num3);
			}
		}
	}

	public override void ManualDeactivation(ushort segmentID, ref NetSegment data)
	{
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			Vector3 position3 = (position + position2) * 0.5f;
			float num = Vector3.Distance(position, position2);
			float num2 = (float)this.m_noiseAccumulation * num / this.m_noiseRadius;
			float radius = Mathf.Max(num, this.m_noiseRadius);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(position3, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, -num2, radius);
		}
	}

	public override void GetNoiseAccumulation(out int noiseAccumulation, out float noiseRadius)
	{
		noiseAccumulation = this.m_noiseAccumulation;
		noiseRadius = this.m_noiseRadius;
	}

	public override void TrafficDirectionUpdated(ushort nodeID, ref NetNode data)
	{
		base.TrafficDirectionUpdated(nodeID, ref data);
		this.UpdateOutsideFlags(nodeID, ref data);
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			building = this.m_outsideConnection;
			heightOffset = -2f;
		}
		else
		{
			base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
		}
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Flood);
		float num = 0f;
		uint num2 = data.m_lanes;
		int num3 = 0;
		while (num3 < this.m_info.m_lanes.Length && num2 != 0u)
		{
			NetInfo.Lane lane = this.m_info.m_lanes[num3];
			if (lane.m_laneType == NetInfo.LaneType.Vehicle)
			{
				num += instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_length;
			}
			num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
			num3++;
		}
		int num4 = Mathf.RoundToInt(num) << 4;
		int num5 = 0;
		int num6 = 0;
		if (num4 != 0)
		{
			num5 = (int)((byte)Mathf.Min((int)(data.m_trafficBuffer * 100) / num4, 100));
			num6 = (int)((byte)Mathf.Min((int)(data.m_noiseBuffer * 250) / num4, 100));
		}
		data.m_trafficBuffer = 0;
		data.m_noiseBuffer = 0;
		if (num5 > (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Min((int)(data.m_trafficDensity + 5), num5);
		}
		else if (num5 < (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Max((int)(data.m_trafficDensity - 5), num5);
		}
		if (num6 > (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Min((int)(data.m_noiseDensity + 2), num6);
		}
		else if (num6 < (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Max((int)(data.m_noiseDensity - 2), num6);
		}
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		bool flag = false;
		if ((this.m_info.m_setVehicleFlags & Vehicle.Flags.Underground) == (Vehicle.Flags)0)
		{
			float num7 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
			if (num7 > vector.y + 1f && num7 > 0f)
			{
				flag = true;
				data.m_flags |= NetSegment.Flags.Flooded;
				problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.Flooded;
				if (num7 > vector.y && num7 > 0f)
				{
					flag = true;
					problem = Notification.AddProblems(problem, Notification.Problem.Flood);
				}
			}
			int num8 = (int)data.m_wetness;
			if (!instance.m_treatWetAsSnow)
			{
				if (flag)
				{
					num8 = 255;
				}
				else
				{
					int num9 = -(num8 + 63 >> 5);
					float num10 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
					if (num10 != 0f)
					{
						int num11 = Mathf.RoundToInt(Mathf.Min(num10 * 4000f, 1000f));
						num9 += Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num11, num11 + 99) / 100;
					}
					num8 = Mathf.Clamp(num8 + num9, 0, 255);
				}
			}
			if (num8 != (int)data.m_wetness)
			{
				if (Mathf.Abs((int)data.m_wetness - num8) > 10)
				{
					data.m_wetness = (byte)num8;
					InstanceID empty = InstanceID.Empty;
					empty.NetSegment = segmentID;
					instance.AddSmoothColor(empty);
					empty.NetNode = data.m_startNode;
					instance.AddSmoothColor(empty);
					empty.NetNode = data.m_endNode;
					instance.AddSmoothColor(empty);
				}
				else
				{
					data.m_wetness = (byte)num8;
					instance.m_wetnessChanged = 256;
				}
			}
		}
		int num12 = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
		int num13 = this.m_noiseAccumulation * num12 / 100;
		if (num13 != 0)
		{
			float num14 = Vector3.Distance(position, position2);
			int num15 = Mathf.FloorToInt(num14 / this.m_noiseRadius);
			for (int i = 0; i < num15; i++)
			{
				Vector3 position3 = Vector3.Lerp(position, position2, (float)(i + 1) / (float)(num15 + 1));
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num13, position3, this.m_noiseRadius);
			}
		}
		data.m_problems = problem;
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		if ((data.m_flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
		{
			TrainTrackBaseAI.LevelCrossingSimulationStep(nodeID, ref data);
		}
		int num = 0;
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
		}
		int num3 = 100 - (num - 100) * (num - 100) / 100;
		int num4 = this.m_noiseAccumulation * num3 / 100;
		if (num4 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num4, data.m_position, this.m_noiseRadius);
		}
	}

	public static void LevelCrossingSimulationStep(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		bool flag = false;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info.m_class.m_service != ItemClass.Service.Road)
				{
					if (info.m_lanes != null)
					{
						bool flag2 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID;
						uint num = instance.m_segments.m_buffer[(int)segment].m_lanes;
						int num2 = 0;
						while (num2 < info.m_lanes.Length && num != 0u)
						{
							if (info.m_lanes[num2].m_laneType == NetInfo.LaneType.Vehicle)
							{
								Vector3 pos = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].CalculatePosition((!flag2) ? 1f : 0f);
								if (TrainTrackBaseAI.CheckOverlap(pos))
								{
									flag = true;
								}
							}
							num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
							num2++;
						}
					}
					RoadBaseAI.TrafficLightState trafficLightState;
					RoadBaseAI.TrafficLightState trafficLightState2;
					bool flag3;
					bool flag4;
					RoadBaseAI.GetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment], currentFrameIndex - 256u, out trafficLightState, out trafficLightState2, out flag3, out flag4);
					if (flag3)
					{
						flag = true;
					}
				}
			}
		}
		bool flag5 = flag;
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = data.GetSegment(j);
			if (segment2 != 0)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment2].Info;
				RoadBaseAI.TrafficLightState trafficLightState3;
				RoadBaseAI.TrafficLightState trafficLightState4;
				RoadBaseAI.GetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment2], currentFrameIndex - 256u, out trafficLightState3, out trafficLightState4);
				trafficLightState3 &= ~RoadBaseAI.TrafficLightState.RedToGreen;
				trafficLightState4 &= ~RoadBaseAI.TrafficLightState.RedToGreen;
				if (info2.m_class.m_service == ItemClass.Service.Road)
				{
					if (flag5)
					{
						if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
						{
							trafficLightState3 = RoadBaseAI.TrafficLightState.GreenToRed;
						}
					}
					else if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState3 = RoadBaseAI.TrafficLightState.RedToGreen;
					}
					trafficLightState4 = RoadBaseAI.TrafficLightState.Green;
				}
				else if (flag5)
				{
					if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState3 = RoadBaseAI.TrafficLightState.RedToGreen;
					}
					if ((trafficLightState4 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState4 = RoadBaseAI.TrafficLightState.GreenToRed;
					}
				}
				else
				{
					if ((trafficLightState3 & RoadBaseAI.TrafficLightState.Red) == RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState3 = RoadBaseAI.TrafficLightState.GreenToRed;
					}
					if ((trafficLightState4 & RoadBaseAI.TrafficLightState.Red) != RoadBaseAI.TrafficLightState.Green)
					{
						trafficLightState4 = RoadBaseAI.TrafficLightState.RedToGreen;
					}
				}
				RoadBaseAI.SetTrafficLightState(nodeID, ref instance.m_segments.m_buffer[(int)segment2], currentFrameIndex, trafficLightState3, trafficLightState4, false, false);
			}
		}
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetNode.Flags flags = data.m_flags & ~(NetNode.Flags.Transition | NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights);
		int num = 0;
		int num2 = 0;
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null)
				{
					if (info.m_createPavement)
					{
						flags |= NetNode.Flags.Transition;
					}
					if (info.m_class.m_service == ItemClass.Service.Road)
					{
						num++;
					}
					else if (info.m_class.m_service == ItemClass.Service.PublicTransport)
					{
						num2++;
					}
				}
			}
		}
		if (num >= 1 && num2 >= 2)
		{
			flags |= (NetNode.Flags.LevelCrossing | NetNode.Flags.TrafficLights);
		}
		data.m_flags = flags;
	}

	private static bool CheckOverlap(Vector3 pos)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((pos.x - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((pos.z - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((pos.x + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((pos.z + 10f) / 32f + 270f), 539);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = TrainTrackBaseAI.CheckOverlap(pos, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Vector3 pos, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		if (otherData.m_segment.DistanceSqr(pos, ref num) < 4f)
		{
			overlap = true;
		}
		return otherData.m_nextGridVehicle;
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.RoadNotConnected);
		VehicleInfo.VehicleType vehicleTypes = this.m_info.m_vehicleTypes & ~VehicleInfo.VehicleType.Car;
		int num = 0;
		int num2 = 0;
		data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle, vehicleTypes, true, ref num, ref num2);
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_7C_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_7C_cp_1 = data.m_building;
				expr_7C_cp_0[(int)expr_7C_cp_1].m_flags = (expr_7C_cp_0[(int)expr_7C_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_A9_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_A9_cp_1 = data.m_building;
				expr_A9_cp_0[(int)expr_A9_cp_1].m_flags = (expr_A9_cp_0[(int)expr_A9_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_D7_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_D7_cp_1 = data.m_building;
				expr_D7_cp_0[(int)expr_D7_cp_1].m_flags = (expr_D7_cp_0[(int)expr_D7_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_101_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_101_cp_1 = data.m_building;
				expr_101_cp_0[(int)expr_101_cp_1].m_flags = (expr_101_cp_0[(int)expr_101_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
		else if ((num != 0 && num2 == 0) || (num == 0 && num2 != 0))
		{
			problem = Notification.AddProblems(problem, Notification.Problem.RoadNotConnected);
		}
		num = 0;
		num2 = 0;
		data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Car, true, ref num, ref num2);
		if ((num != 0 && num2 == 0) || (num == 0 && num2 != 0))
		{
			problem = Notification.AddProblems(problem, Notification.Problem.RoadNotConnected);
		}
		data.m_problems = problem;
	}

	private void UpdateOutsideFlags(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			VehicleInfo.VehicleType vehicleTypes = this.m_info.m_vehicleTypes & ~VehicleInfo.VehicleType.Car;
			int num = 0;
			int num2 = 0;
			data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle, vehicleTypes, true, ref num, ref num2);
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_60_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_60_cp_1 = data.m_building;
				expr_60_cp_0[(int)expr_60_cp_1].m_flags = (expr_60_cp_0[(int)expr_60_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_8C_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_8C_cp_1 = data.m_building;
				expr_8C_cp_0[(int)expr_8C_cp_1].m_flags = (expr_8C_cp_0[(int)expr_8C_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_B9_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_B9_cp_1 = data.m_building;
				expr_B9_cp_0[(int)expr_B9_cp_1].m_flags = (expr_B9_cp_0[(int)expr_B9_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_E2_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_E2_cp_1 = data.m_building;
				expr_E2_cp_0[(int)expr_E2_cp_1].m_flags = (expr_E2_cp_0[(int)expr_E2_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override ToolBase.ToolErrors CanConnectTo(ushort node, ushort segment, ulong[] segmentMask)
	{
		ToolBase.ToolErrors toolErrors = base.CanConnectTo(node, segment, segmentMask);
		NetManager instance = Singleton<NetManager>.get_instance();
		if (node != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
				if (segment2 != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment2].Info;
					if (info.m_class.m_service == ItemClass.Service.Road && info.m_netAI.IsOverground())
					{
						toolErrors |= ToolBase.ToolErrors.ObjectCollision;
						if (segmentMask != null)
						{
							segmentMask[segment2 >> 6] |= 1uL << (int)segment2;
						}
					}
				}
			}
		}
		else if (segment != 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
			if (info2.m_class.m_service == ItemClass.Service.Road && info2.m_netAI.IsOverground())
			{
				toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				if (segmentMask != null)
				{
					segmentMask[segment >> 6] |= 1uL << (int)segment;
				}
			}
		}
		return toolErrors;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return false;
		}
		if (DefaultTool.IsOutOfCityArea(segmentID))
		{
			return false;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_7C_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_7C_cp_1 = disaster;
				expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedTrackLength = expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedTrackLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Notification.Problem problems = data.m_problems;
		data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
		if (data.m_problems != problems)
		{
			Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
		return true;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_4B_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_4B_cp_1 = disaster;
				expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedTrackLength = expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedTrackLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			Notification.Problem problems = data.m_problems;
			data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
			if (data.m_problems != problems)
			{
				Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
			}
		}
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (segmentInfo.m_requireSurfaceMaps)
		{
			if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 255;
		}
		else
		{
			if (vector2.y > -0.5f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 128;
		}
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (nodeInfo.m_requireSurfaceMaps)
		{
			if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 255;
		}
		else
		{
			if (vector2.y > -0.5f)
			{
				result.g = 255;
			}
			else
			{
				result.g = 0;
			}
			result.a = 128;
		}
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	[CustomizableProperty("Noise Accumulation", "Properties")]
	public int m_noiseAccumulation = 10;

	[CustomizableProperty("Noise Radius", "Properties")]
	public float m_noiseRadius = 40f;

	public BuildingInfo m_outsideConnection;
}
