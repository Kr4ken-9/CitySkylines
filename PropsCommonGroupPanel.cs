﻿using System;

public sealed class PropsCommonGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Prop;
		}
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsCommonAccessories", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsCommonGarbage", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsCommonCommunications", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsCommonStreets", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsCommonLights", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "PropsCommonAccessories")
			{
				return 0;
			}
			if (name == "PropsCommonGarbage")
			{
				return 1;
			}
			if (name == "PropsCommonCommunications")
			{
				return 2;
			}
			if (name == "PropsCommonStreets")
			{
				return 3;
			}
			if (name == "PropsCommonLights")
			{
				return 4;
			}
		}
		return 2147483647;
	}
}
