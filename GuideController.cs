﻿using System;
using ColossalFramework;
using UnityEngine;

public class GuideController : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("GuideController");
		Singleton<GuideManager>.get_instance().InitializeProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("GuideController");
		Singleton<GuideManager>.get_instance().DestroyProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	public float m_tutorialHideTimeout = 60f;

	public float m_tutorialNextTimeout = 30f;

	public GuideInfo m_cameraNotZoomed;

	public GuideInfo m_cameraNotMoved;

	public GuideInfo m_cameraNotRotated;

	public GuideInfo m_cameraNotTilted;

	public GuideInfo m_buildingAbandoned1;

	public GuideInfo m_buildingAbandoned2;

	public GuideInfo m_buildingOnFire;

	public GuideInfo m_buildingBurned;

	public GuideInfo m_buildingLevelUp;

	public GuideInfo m_zoningNotUsed1;

	public GuideInfo m_zoningNotUsed2;

	public GuideInfo m_zoningDemand;

	public GuideInfo m_serviceNotUsed;

	public GuideInfo m_serviceNeeded;

	public GuideInfo m_serviceNeeded2;

	public GuideInfo m_buildingNotUsed;

	public GuideInfo m_outsideNotConnected;

	public GuideInfo m_areaNotUnlocked;

	public GuideInfo m_buildNextToRoad;

	public GuideInfo m_buildNextToWater;

	public GuideInfo m_drainPipeMissing;

	public GuideInfo m_waterPumpMissing;

	public GuideInfo m_speedNotUsed;

	public GuideInfo m_speedNotUsed2;

	public GuideInfo m_economyNotUsed;

	public GuideInfo m_loanNeededGuide;

	public GuideInfo m_infoviewNotUsed;

	public GuideInfo m_upgradeExistingRoad;

	public GuideInfo m_generalDemand;

	public GuideInfo m_notEnoughMoney;

	public GuideInfo m_lineNotFinished;

	public GuideInfo m_shortRoadTraffic;

	public GuideInfo m_milestonesNotUsed;

	public GuideInfo m_roadOptionsNotUsed;

	public GuideInfo m_elevationNotUsed;

	public GuideInfo m_zoneOptionsNotUsed;

	public GuideInfo m_manualUpgrade;

	public GuideInfo m_deathCareNeeded;

	public GuideInfo m_bulldozerNotUsed;

	public GuideInfo m_roadsNotUsed;

	public GuideInfo m_landfillSiteFull;

	public GuideInfo m_cemeteryFull;

	public GuideInfo m_landfillSiteEmpty;

	public GuideInfo m_cemeteryEmpty;

	public GuideInfo m_districtsNotUsed;

	public GuideInfo m_policiesNotUsed;

	public GuideInfo m_windTurbinePlacement;

	public GuideInfo m_harborPlacement;

	public GuideInfo m_onewayRoadPlacement;

	public GuideInfo m_worldInfoNotUsed;

	public GuideInfo m_renameNotUsed;

	public GuideInfo m_zonedBuildingInfo;

	public GuideInfo m_canalsNotUsed;

	public GuideInfo m_quaysNotUsed;

	public GuideInfo m_canalDemolished;

	public GuideInfo m_snowDumpNotUsed;

	public GuideInfo m_heatingBuilt;

	public GuideInfo m_roadMaintenanceBuilt;

	public GuideInfo m_tramDepotBuilt;

	public GuideInfo m_depotNotConnected;

	public GuideInfo m_heatingNotUsed;

	public GuideInfo m_snowDumpFull;

	public GuideInfo m_upgradeWaterPipes;

	public GuideInfo m_buildingFlooded;

	public GuideInfo m_buildingFlooded2;

	public GuideInfo m_terrainToolNotUsed;

	public GuideInfo m_notEnoughDirt;

	public GuideInfo m_tooMuchDirt;

	public GuideInfo m_teamColorNotUsed;

	public GuideInfo m_matchPreparing;

	public GuideInfo m_disasterWarning;

	public GuideInfo m_escapeRoutes;

	public GuideInfo m_buildingDestroyed;

	public GuideInfo m_buildingDestroyed2;

	public GuideInfo m_evacuationNotUsed;

	public GuideInfo m_scenarioGoalsNotUsed;

	public GuideInfo m_waterOutletOnLand;

	public GuideInfo m_roadDestroyed;

	public GuideInfo m_roadDestroyed2;

	public GuideInfo m_sheltersNotUsed;

	public GuideInfo m_citizensInShelter;

	public GuideInfo m_monorailLine;

	public GuideInfo m_localOnlyStation;

	public GuideInfo m_endOfLineStation;

	public GuideInfo m_ferryBusHub;

	public GuideInfo m_roadNames;

	public GuideInfo m_roadNamesOpen;

	public GuideInfo m_ferryLine;

	public GuideInfo m_ferryDepot;

	public GuideInfo m_blimpLine;

	public GuideInfo m_blimpDepot;

	public GuideInfo m_routeButton;

	public GuideInfo m_yieldLights;

	public GuideInfo m_visualAids;

	public GuideInfo m_undergroundBulldozer;

	public GuideInfo m_festivalColorNotUsed;

	public GuideInfo m_bandPreparing;

	public GuideInfo m_bandPopularity;

	public GuideInfo m_ticketPricesNotUsed;
}
