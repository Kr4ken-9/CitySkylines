﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class UndergroundView : MonoBehaviour
{
	private void OnEnable()
	{
		this.m_mainCamera = base.get_transform().get_parent().GetComponent<Camera>();
		this.m_undergroundCamera = base.GetComponent<Camera>();
		Camera expr_28 = this.m_undergroundCamera;
		expr_28.set_depthTextureMode(expr_28.get_depthTextureMode() | 1);
	}

	private void OnDisable()
	{
		this.m_mainCamera = null;
		this.m_undergroundCamera = null;
		if (this.m_undergroundRGBD != null)
		{
			RenderTexture.ReleaseTemporary(this.m_undergroundRGBD);
			this.m_undergroundRGBD = null;
		}
	}

	private void LateUpdate()
	{
		if (this.m_undergroundRGBD != null)
		{
			RenderTexture.ReleaseTemporary(this.m_undergroundRGBD);
			this.m_undergroundRGBD = null;
		}
		if (this.m_undergroundCamera != null && this.m_mainCamera != null)
		{
			if (this.m_undergroundCamera.get_cullingMask() != 0)
			{
				int width = Screen.get_width();
				int height = Screen.get_height();
				this.m_undergroundRGBD = RenderTexture.GetTemporary(width, height, 24, 2, 1);
				this.m_undergroundCamera.set_fieldOfView(this.m_mainCamera.get_fieldOfView());
				this.m_undergroundCamera.set_nearClipPlane(this.m_mainCamera.get_nearClipPlane());
				this.m_undergroundCamera.set_farClipPlane(this.m_mainCamera.get_farClipPlane());
				this.m_undergroundCamera.set_rect(this.m_mainCamera.get_rect());
				this.m_undergroundCamera.set_targetTexture(this.m_undergroundRGBD);
				this.m_undergroundCamera.set_enabled(true);
			}
			else
			{
				this.m_undergroundCamera.set_enabled(false);
			}
		}
	}

	private void OnPostRender()
	{
		if (this.m_undergroundRGBD != null && Application.get_isPlaying() && Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			try
			{
				RenderManager.Managers_UndergroundOverlay(Singleton<RenderManager>.get_instance().CurrentCameraInfo);
			}
			finally
			{
			}
		}
	}

	public bool BeginUsingTexture(out Texture tx)
	{
		if (this.m_undergroundCamera != null && this.m_undergroundCamera.get_cullingMask() != 0)
		{
			tx = this.m_undergroundRGBD;
			return tx != null;
		}
		tx = null;
		return false;
	}

	public void EndUsingTexture()
	{
		if (this.m_undergroundCamera != null)
		{
			this.m_undergroundCamera.set_targetTexture(null);
		}
		if (this.m_undergroundRGBD != null)
		{
			RenderTexture.ReleaseTemporary(this.m_undergroundRGBD);
			this.m_undergroundRGBD = null;
		}
	}

	private Camera m_mainCamera;

	private Camera m_undergroundCamera;

	private RenderTexture m_undergroundRGBD;
}
