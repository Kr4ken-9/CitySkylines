﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class TreeProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		TreeProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new TreeProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("TreeProperties");
			Singleton<TreeManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Shader m_renderShader;

	public EffectInfo m_placementEffect;

	public EffectInfo m_bulldozeEffect;

	public EffectInfo m_fireEffect;
}
