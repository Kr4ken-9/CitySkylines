﻿using System;

public sealed class ThemeSettingsGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("ThemeSettings");
		return true;
	}
}
