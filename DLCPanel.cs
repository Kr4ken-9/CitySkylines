﻿using System;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;

public class DLCPanel : UICustomControl
{
	private void Awake()
	{
		base.Find("DeluxeEdition").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.DeluxeDLC));
		base.Find("AfterDark").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC));
		base.Find("WinterWonderland").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC));
		base.Find("MatchDay").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.Football));
		base.Find("ArtDeco").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack1));
		base.Find("Stadiums").set_isVisible(SteamHelper.IsSomeStadiumDLCOwned());
		base.Find("NaturalDisasters").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC));
		base.Find("HighTech").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack2));
		base.Find("RelaxationStation").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation1));
		if (SteamHelper.IsDLCAvailable(SteamHelper.DLC.OrientalBuildings))
		{
			base.Find("OrientalBuildings").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.OrientalBuildings));
		}
		else
		{
			base.Find("OrientalBuildings").set_isVisible(false);
		}
		base.Find("MassTransit").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC));
		base.Find("RockStation").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation2));
		base.Find("Concerts").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.MusicFestival));
		base.Find("GreenCities").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC));
		base.Find("EuroContentPack").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack3));
		UIButton uIButton = base.Find<UIButton>("BuyDeluxeEdition");
		uIButton.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.DeluxeDLC));
		uIButton.add_eventClick(new MouseEventHandler(this.DeluxeClicked));
		UIButton uIButton2 = base.Find<UIButton>("BuyAfterDark");
		uIButton2.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC));
		uIButton2.add_eventClick(new MouseEventHandler(this.ADClicked));
		UIButton uIButton3 = base.Find<UIButton>("BuyWinterWonderland");
		uIButton3.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC));
		uIButton3.add_eventClick(new MouseEventHandler(this.WWClicked));
		UIButton uIButton4 = base.Find<UIButton>("BuyMatchDay");
		uIButton4.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.Football));
		uIButton4.add_eventClick(new MouseEventHandler(this.MDClicked));
		UIButton uIButton5 = base.Find<UIButton>("BuyArtDeco");
		uIButton5.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack1));
		uIButton5.add_eventClick(new MouseEventHandler(this.ArtDecoClicked));
		UIButton uIButton6 = base.Find<UIButton>("BuyNaturalDisasters");
		uIButton6.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC));
		uIButton6.add_eventClick(new MouseEventHandler(this.NaturalDisastersClicked));
		UIButton uIButton7 = base.Find<UIButton>("BuyHighTech");
		uIButton7.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack2));
		uIButton7.add_eventClick(new MouseEventHandler(this.HighTechClicked));
		UIButton uIButton8 = base.Find<UIButton>("BuyRelaxationStation");
		uIButton8.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation1));
		uIButton8.add_eventClick(new MouseEventHandler(this.RelaxationStationClicked));
		UIButton uIButton9 = base.Find<UIButton>("BuyOrientalBuildings");
		if (SteamHelper.IsDLCAvailable(SteamHelper.DLC.OrientalBuildings))
		{
			uIButton9.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.OrientalBuildings));
		}
		else
		{
			uIButton9.set_isVisible(false);
		}
		uIButton9.add_eventClick(new MouseEventHandler(this.OrientalBuildingsClicked));
		UIButton uIButton10 = base.Find<UIButton>("BuyMassTransit");
		uIButton10.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC));
		uIButton10.add_eventClick(new MouseEventHandler(this.MassTransitClicked));
		UIButton uIButton11 = base.Find<UIButton>("BuyRockStation");
		uIButton11.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation2));
		uIButton11.add_eventClick(new MouseEventHandler(this.RockStationClicked));
		UIButton uIButton12 = base.Find<UIButton>("BuyConcerts");
		uIButton12.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.MusicFestival));
		uIButton12.add_eventClick(new MouseEventHandler(this.ConcertsClicked));
		UIButton uIButton13 = base.Find<UIButton>("BuyGreenCities");
		uIButton13.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC));
		uIButton13.add_eventClick(new MouseEventHandler(this.GreenCitiesClicked));
		UIButton uIButton14 = base.Find<UIButton>("BuyEuroContentPack");
		uIButton14.set_isVisible(!SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack3));
		uIButton14.add_eventClick(new MouseEventHandler(this.EuroContentPackClicked));
	}

	private void DeluxeClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			if (PlatformService.get_apiBackend() == null)
			{
				PlatformService.ActivateGameOverlayToWebPage("http://store.steampowered.com/app/255710/");
			}
			else if (PlatformService.get_apiBackend() == 1)
			{
				PlatformService.ActivateGameOverlayToStore(346791u, 0);
			}
		}
	}

	private void ADClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(369150u, 0);
		}
	}

	private void WWClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(420610u, 0);
		}
	}

	private void MDClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(456200u, 0);
		}
	}

	private void ArtDecoClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(515190u, 0);
		}
	}

	private void NaturalDisastersClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(515191u, 0);
		}
	}

	private void HighTechClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(547500u, 0);
		}
	}

	private void RelaxationStationClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(547501u, 0);
		}
	}

	private void OrientalBuildingsClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(563850u, 0);
		}
	}

	private void MassTransitClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(547502u, 0);
		}
	}

	private void RockStationClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(614582u, 0);
		}
	}

	private void ConcertsClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(614581u, 0);
		}
	}

	private void GreenCitiesClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(614580u, 0);
		}
	}

	private void EuroContentPackClicked(UIComponent c, UIMouseEventParameter p)
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(715190u, 0);
		}
	}
}
