﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class DestructionInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_CoverageGradient = base.Find<UITextureSprite>("CoverageGradient");
		this.m_label = base.Find<UILabel>("Label");
		this.m_label2 = base.Find<UILabel>("DestructionLabel");
		this.m_labelBuilding = base.Find<UILabel>("Building");
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.m_label.set_text(Locale.Get("INFOVIEWS", "Destruction"));
		this.m_label2.set_text(Locale.Get("INFOVIEWS", "Destruction"));
		this.m_labelBuilding.set_text(Locale.Get("BUILDING_TITLE", "Disaster Response Unit"));
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized && Singleton<LoadingManager>.get_exists())
		{
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				if (!Singleton<InfoManager>.get_exists())
				{
					return;
				}
				uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[27].m_activeColor);
				uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[27].m_inactiveColor);
				this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[27].m_negativeColor);
				this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[27].m_targetColor);
			}
			this.OnLocaleChanged();
			this.m_initialized = true;
		}
	}

	private UITextureSprite m_CoverageGradient;

	private UILabel m_label;

	private UILabel m_label2;

	private UILabel m_labelBuilding;

	private bool m_initialized;
}
