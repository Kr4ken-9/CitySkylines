﻿using System;

public class FusionPowerPlantAI : PowerPlantAI
{
	public override bool IsWonder()
	{
		return true;
	}

	public override void GetElectricityProduction(out int min, out int max)
	{
		min = 1000000;
		max = 1000000;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}
}
