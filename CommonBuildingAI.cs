﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CommonBuildingAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Garbage:
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, (float)Mathf.Min(100, (int)(data.m_garbageBuffer / 50)) * 0.01f);
		case InfoManager.InfoMode.BuildingLevel:
		case InfoManager.InfoMode.Education:
		case InfoManager.InfoMode.TerrainHeight:
		case InfoManager.InfoMode.Maintenance:
		case InfoManager.InfoMode.Snow:
		case InfoManager.InfoMode.Radio:
		case InfoManager.InfoMode.DisasterDetection:
			IL_41:
			switch (infoMode)
			{
			case InfoManager.InfoMode.None:
			{
				ushort eventIndex = data.m_eventIndex;
				bool flag = true;
				Color result;
				if (eventIndex != 0)
				{
					EventManager instance = Singleton<EventManager>.get_instance();
					EventInfo info = instance.m_events.m_buffer[(int)eventIndex].Info;
					Color32 buildingColor = info.m_eventAI.GetBuildingColor(eventIndex, ref instance.m_events.m_buffer[(int)eventIndex]);
					if (buildingColor.a == 255)
					{
						result = buildingColor;
					}
					else
					{
						result = base.GetColor(buildingID, ref data, infoMode);
					}
					flag = info.m_eventAI.IsBuildingActive(eventIndex, ref instance.m_events.m_buffer[(int)eventIndex]);
				}
				else
				{
					result = base.GetColor(buildingID, ref data, infoMode);
				}
				if (this.ShowConsumption(buildingID, ref data) && data.m_fireIntensity == 0 && flag)
				{
					bool flag2;
					Singleton<ElectricityManager>.get_instance().CheckElectricity(data.m_position, out flag2);
					result.a = ((!flag2 && this.ElectricityGridRadius() != 0f) ? 0f : 1f);
				}
				else
				{
					result.a = 0f;
				}
				return result;
			}
			case InfoManager.InfoMode.Electricity:
			{
				if (!this.ShowConsumption(buildingID, ref data))
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				bool flag3;
				Singleton<ElectricityManager>.get_instance().CheckElectricity(data.m_position, out flag3);
				if (flag3)
				{
					Color targetColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
					targetColor.a = 0f;
					return targetColor;
				}
				Color negativeColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
				negativeColor.a = 0f;
				return negativeColor;
			}
			case InfoManager.InfoMode.Water:
			{
				if (!this.ShowConsumption(buildingID, ref data))
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				bool flag4;
				bool flag5;
				byte b;
				Singleton<WaterManager>.get_instance().CheckWater(data.m_position, out flag4, out flag5, out b);
				if (flag4 && flag5)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
				}
				if (flag4)
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, 0.5f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			case InfoManager.InfoMode.CrimeRate:
				if (this.ShowConsumption(buildingID, ref data))
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, (float)data.m_crimeBuffer / Mathf.Max(1f, (float)data.m_citizenCount * 100f));
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			case InfoManager.InfoMode.Happiness:
				if (this.ShowConsumption(buildingID, ref data))
				{
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, (float)Citizen.GetHappinessLevel((int)data.m_happiness) * 0.25f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		case InfoManager.InfoMode.FireSafety:
		{
			int num;
			int num2;
			int num3;
			if (this.ShowConsumption(buildingID, ref data) && this.GetFireParameters(buildingID, ref data, out num, out num2, out num3))
			{
				if (num != 0)
				{
					DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
					byte district = instance2.GetDistrict(data.m_position);
					DistrictPolicies.Services servicePolicies = instance2.m_districts.m_buffer[(int)district].m_servicePolicies;
					if ((servicePolicies & DistrictPolicies.Services.SmokeDetectors) != DistrictPolicies.Services.None)
					{
						num = num * 75 / 100;
					}
				}
				Vector3 position = data.CalculateSidewalkPosition();
				int num4 = (int)(Singleton<CoverageManager>.get_instance().FindFireCoverage(position) * 100 / 255);
				num4 = Mathf.Min(100, (10 + num3) * (25 + num4) * 2000 / ((100 + num) * (100 + num2)));
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, (float)num4 * 0.01f);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		case InfoManager.InfoMode.Entertainment:
			if (this.ShowConsumption(buildingID, ref data))
			{
				int num5;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.Entertainment, data.m_position, out num5);
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Mathf.Clamp01((float)num5 * 0.005f));
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.Heating:
		{
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			bool flag6;
			Singleton<WaterManager>.get_instance().CheckHeating(data.m_position, out flag6);
			if (flag6)
			{
				Color targetColor2 = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
				targetColor2.a = 0f;
				return targetColor2;
			}
			Color negativeColor2 = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			negativeColor2.a = 0f;
			return negativeColor2;
		}
		case InfoManager.InfoMode.EscapeRoutes:
			if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Collapsed)) == Building.Flags.Evacuating)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.Destruction:
			if ((data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (data.m_levelUpProgress == 255)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		case InfoManager.InfoMode.DisasterHazard:
			if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
			}
			return Singleton<DisasterManager>.get_instance().SampleDisasterHazardMap(data.m_position);
		}
		goto IL_41;
	}

	public static Color GetNoisePollutionColor(float amount)
	{
		InfoManager instance = Singleton<InfoManager>.get_instance();
		amount = Mathf.Clamp01(amount * 0.04f);
		Color color;
		Color color2;
		if (amount >= 0.5f)
		{
			color = instance.m_properties.m_modeProperties[7].m_activeColorB;
			color2 = instance.m_properties.m_modeProperties[7].m_targetColor;
			amount = amount * 2f - 1f;
		}
		else
		{
			color = instance.m_properties.m_neutralColor;
			color2 = instance.m_properties.m_modeProperties[7].m_activeColorB;
			amount *= 2f;
		}
		return ColorUtils.LinearLerp(color, color2, amount);
	}

	public static Color GetRadioCoverageColor(float amount)
	{
		InfoManager instance = Singleton<InfoManager>.get_instance();
		amount = Mathf.Clamp01(Mathf.Sqrt(amount * 0.01f));
		Color color;
		Color color2;
		if (amount >= 0.5f)
		{
			color = instance.m_properties.m_modeProperties[26].m_activeColorB;
			color2 = instance.m_properties.m_modeProperties[26].m_targetColor;
			amount = amount * 2f - 1f;
		}
		else if (amount >= 0.25f)
		{
			color = instance.m_properties.m_neutralColor;
			color2 = instance.m_properties.m_modeProperties[26].m_activeColorB;
			amount *= 2f;
		}
		else
		{
			color = instance.m_properties.m_modeProperties[26].m_negativeColor;
			color2 = ColorUtils.LinearLerp(instance.m_properties.m_neutralColor, instance.m_properties.m_modeProperties[26].m_activeColorB, 0.5f);
			amount *= 4f;
		}
		return ColorUtils.LinearLerp(color, color2, amount);
	}

	protected virtual bool ShowConsumption(ushort buildingID, ref Building data)
	{
		return true;
	}

	public override int GetGarbageAmount(ushort buildingID, ref Building data)
	{
		return (int)data.m_garbageBuffer;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Home) != 0)
			{
				num4 += 5;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u)
					{
						num3++;
					}
				}
			}
			else if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Work) != 0)
			{
				num6 += 5;
				for (int j = 0; j < 5; j++)
				{
					uint citizen2 = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(j);
					if (citizen2 != 0u)
					{
						num5++;
					}
				}
			}
			else if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Student) != 0)
			{
				num8 += 5;
				for (int k = 0; k < 5; k++)
				{
					uint citizen3 = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(k);
					if (citizen3 != 0u)
					{
						num7++;
					}
				}
			}
			for (int l = 0; l < 5; l++)
			{
				uint citizen4 = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(l);
				if (citizen4 != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen4)].Dead && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen4)].GetBuildingByLocation() == buildingID)
				{
					num9++;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return StringUtils.SafeFormat("Electricity: {0}\nWater: {1} ({2}% polluted)\nSewage: {3}\nGarbage: {4}\nCrime: {5}\nResidents: {6}/{7}\nWorkers: {8}/{9}\nStudents: {10}/{11}\nDead: {12}", new object[]
		{
			data.m_electricityBuffer,
			data.m_waterBuffer,
			(int)(data.m_waterPollution * 100 / 255),
			data.m_sewageBuffer,
			data.m_garbageBuffer,
			data.m_crimeBuffer,
			num3,
			num4,
			num5,
			num6,
			num7,
			num8,
			num9
		});
	}

	public override void RefreshInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		if ((data.m_flags & (Building.Flags.Completed | Building.Flags.Collapsed)) != Building.Flags.Completed)
		{
			if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				bool requireHeightMap = false;
				BuildingInfoBase collapsedInfo = this.m_info.m_collapsedInfo;
				if (collapsedInfo != null)
				{
					requireHeightMap = collapsedInfo.m_requireHeightMap;
				}
				BuildingAI.RefreshInstance(this.m_info, cameraInfo, buildingID, ref data, layerMask, ref instance, requireHeightMap);
			}
			else
			{
				if ((data.m_flags & Building.Flags.Upgrading) != Building.Flags.None)
				{
					BuildingInfo upgradeInfo = this.GetUpgradeInfo(buildingID, ref data);
					if (upgradeInfo != null && instance.m_dataInt0 == upgradeInfo.m_prefabDataIndex)
					{
						BuildingAI.RefreshInstance(upgradeInfo, cameraInfo, buildingID, ref data, layerMask, ref instance, false);
						return;
					}
				}
				BuildingAI.RefreshInstance(this.m_info, cameraInfo, buildingID, ref data, layerMask, ref instance, false);
			}
		}
		else
		{
			base.RefreshInstance(cameraInfo, buildingID, ref data, layerMask, ref instance);
			Randomizer randomizer;
			randomizer..ctor((int)buildingID);
			PropInfo randomPropInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer, ItemClass.Service.Garbage);
			if (randomPropInfo != null)
			{
				int width = data.Width;
				int length = data.Length;
				for (int i = 0; i < 8; i++)
				{
					PropInfo variation = randomPropInfo.GetVariation(ref randomizer);
					randomizer.Int32(10000u);
					randomizer.Int32(10000u);
					variation.GetColor(ref randomizer);
					Vector3 vector;
					vector.x = ((float)randomizer.Int32(10000u) * 0.0001f - 0.5f) * (float)width * 4f;
					vector.y = 0f;
					vector.z = (float)randomizer.Int32(10000u) * 0.0001f - 0.5f + (float)length * 4f;
					vector = instance.m_dataMatrix0.MultiplyPoint(vector);
					vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
					instance.m_extraData.SetUShort(64 + i, (ushort)Mathf.Clamp(Mathf.RoundToInt(vector.y * 64f), 0, 65535));
				}
			}
		}
	}

	public override void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		if ((data.m_flags & (Building.Flags.Completed | Building.Flags.Collapsed)) != Building.Flags.Completed)
		{
			if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				uint num = (uint)(((int)buildingID << 8) / 49152);
				uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
				float num3 = ((num2 & 255u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.00390625f;
				Building.Frame frameData = data.GetFrameData(num2 - 512u);
				Building.Frame frameData2 = data.GetFrameData(num2 - 256u);
				instance.m_dataVector0.x = Mathf.Max(0f, (Mathf.Lerp((float)frameData.m_fireDamage, (float)frameData2.m_fireDamage, num3) - 127f) * 0.0078125f);
				instance.m_dataVector0.y = 0f;
				instance.m_dataVector0.z = (((data.m_flags & Building.Flags.Abandoned) == Building.Flags.None) ? 0f : 1f);
				float num4 = 0f;
				Randomizer randomizer;
				randomizer..ctor((int)buildingID);
				int num5 = randomizer.Int32(4u);
				if (frameData.m_constructState != 0)
				{
					float y = this.m_info.m_size.y;
					float num6 = (float)this.GetCollapseTime();
					num6 /= Mathf.Max(1f, num6 - 6f);
					float num7 = Mathf.Lerp((float)frameData.m_constructState, (float)frameData2.m_constructState, num3) * 0.003921569f;
					num4 = 1f - num6 * (1f - num7);
					instance.m_dataVector0.y = Mathf.Max(instance.m_dataVector0.y, y * num4);
					if (instance.m_dataVector0.y > 0.1f)
					{
						base.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, false, true);
						float angle = data.m_angle;
						Vector3 position = instance.m_position;
						Quaternion rotation = instance.m_rotation;
						Matrix4x4 dataMatrix = instance.m_dataMatrix1;
						float num8 = (float)randomizer.Int32(1000u) * 0.00628318544f;
						float num9 = (float)randomizer.Int32(10, 45);
						float num10 = (1f - num4) * (1f - num4) * num9;
						Vector3 vector;
						vector..ctor(Mathf.Cos(num8), 0f, Mathf.Sin(num8));
						float num11 = (y - instance.m_dataVector0.y) * (y - instance.m_dataVector0.y) / y;
						instance.m_position.x = instance.m_position.x + vector.z * num11 * num9 * 0.01f;
						instance.m_position.z = instance.m_position.z - vector.x * num11 * num9 * 0.01f;
						instance.m_position.y = instance.m_position.y - num11;
						instance.m_rotation = Quaternion.AngleAxis(num10, vector) * Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
						instance.m_dataMatrix1.SetTRS(instance.m_position, instance.m_rotation, Vector3.get_one());
						instance.m_dataVector0.y = y;
						this.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
						base.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, true, false);
						instance.m_dataVector0.y = y - num11;
						instance.m_position = position;
						instance.m_rotation = rotation;
						instance.m_dataMatrix1 = dataMatrix;
					}
					else if ((data.m_flags & Building.Flags.Demolishing) == Building.Flags.None)
					{
						base.RenderDestroyedProps(cameraInfo, buildingID, ref data, layerMask, ref instance, false, true);
					}
				}
				else if ((data.m_flags & Building.Flags.Demolishing) == Building.Flags.None)
				{
					base.RenderDestroyedProps(cameraInfo, buildingID, ref data, layerMask, ref instance, false, true);
				}
				float num12 = Mathf.Clamp01(1f - num4);
				instance.m_dataVector0.x = -instance.m_dataVector0.x;
				if ((data.m_flags & Building.Flags.Demolishing) == Building.Flags.None && num12 > 0.01f)
				{
					BuildingInfoBase collapsedInfo = this.m_info.m_collapsedInfo;
					if (collapsedInfo != null)
					{
						if ((1 << num5 & this.m_info.m_collapsedRotations) == 0)
						{
							num5 = (num5 + 1 & 3);
						}
						Vector3 vector2 = this.m_info.m_generatedInfo.m_min;
						Vector3 vector3 = this.m_info.m_generatedInfo.m_max;
						float num13 = (float)data.Width * 4f;
						float num14 = (float)data.Length * 4f;
						float num15 = Building.CalculateLocalMeshOffset(this.m_info, data.Length);
						vector2 = Vector3.Max(vector2 - new Vector3(4f, 0f, 4f + num15), new Vector3(-num13, 0f, -num14));
						vector3 = Vector3.Min(vector3 + new Vector3(4f, 0f, 4f - num15), new Vector3(num13, 0f, num14));
						Vector3 vector4 = (vector2 + vector3) * 0.5f;
						Vector3 vector5 = vector3 - vector2;
						float num16 = (((num5 & 1) != 0) ? vector5.z : vector5.x) * num12 / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
						float num17 = (((num5 & 1) != 0) ? vector5.x : vector5.z) * num12 / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
						Quaternion quaternion = Quaternion.AngleAxis((float)num5 * 90f, Vector3.get_down());
						instance.m_dataVector0.y = Mathf.Max(instance.m_dataVector0.y, collapsedInfo.m_generatedInfo.m_size.y);
						Matrix4x4 matrix = Matrix4x4.TRS(new Vector3(vector4.x, 0f, vector4.z + num15), quaternion, new Vector3(num16, num12, num17));
						BuildingAI.RenderMesh(cameraInfo, this.m_info, collapsedInfo, matrix, ref instance);
					}
				}
				if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
				{
					this.RenderCollapseEffect(cameraInfo, buildingID, ref data, num4);
				}
			}
			else
			{
				uint num18 = (uint)(((int)buildingID << 8) / 49152);
				uint num19 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num18;
				float num20 = ((num19 & 255u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.00390625f;
				Building.Frame frameData3 = data.GetFrameData(num19 - 512u);
				Building.Frame frameData4 = data.GetFrameData(num19 - 256u);
				float num21 = 0f;
				BuildingInfo buildingInfo;
				BuildingInfo buildingInfo2;
				if ((data.m_flags & Building.Flags.Upgrading) != Building.Flags.None)
				{
					BuildingInfo upgradeInfo = this.GetUpgradeInfo(buildingID, ref data);
					if (upgradeInfo != null)
					{
						buildingInfo = this.m_info;
						buildingInfo2 = upgradeInfo;
					}
					else
					{
						buildingInfo = null;
						buildingInfo2 = this.m_info;
					}
				}
				else
				{
					buildingInfo = null;
					buildingInfo2 = this.m_info;
				}
				float num22 = buildingInfo2.m_size.y;
				if (buildingInfo != null)
				{
					num22 = Mathf.Max(num22, buildingInfo.m_size.y);
				}
				float num23 = (float)this.GetConstructionTime();
				num23 /= Mathf.Max(1f, num23 - 6f);
				float num24 = Mathf.Max(0.5f, num22 / 60f);
				float num25 = Mathf.Ceil(num22 / num24 / 6f) * 6f;
				float num26 = (num25 * 2f + 6f) * num23 * Mathf.Lerp((float)frameData3.m_constructState, (float)frameData4.m_constructState, num20) * 0.003921569f;
				float num27 = (num26 - 6f) * num24;
				if (num27 >= buildingInfo2.m_size.y && instance.m_dataInt0 != buildingInfo2.m_prefabDataIndex)
				{
					BuildingAI.RefreshInstance(buildingInfo2, cameraInfo, buildingID, ref data, layerMask, ref instance, false);
				}
				float num28;
				if (num26 > num25)
				{
					num28 = Mathf.Min(num25, num25 * 2f + 6f - num26);
				}
				else
				{
					num28 = num26;
				}
				if (frameData4.m_productionState < frameData3.m_productionState)
				{
					instance.m_dataVector3.w = Mathf.Lerp((float)frameData3.m_productionState, (float)frameData4.m_productionState + 256f, num20) * 0.00390625f;
					if (instance.m_dataVector3.w >= 1f)
					{
						instance.m_dataVector3.w = instance.m_dataVector3.w - 1f;
					}
				}
				else
				{
					instance.m_dataVector3.w = Mathf.Lerp((float)frameData3.m_productionState, (float)frameData4.m_productionState, num20) * 0.00390625f;
				}
				if (buildingInfo != null)
				{
					instance.m_position = Building.CalculateMeshPosition(buildingInfo, data.m_position, data.m_angle, data.Length);
					instance.m_rotation = Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down());
					instance.m_dataMatrix1.SetTRS(instance.m_position, instance.m_rotation, Vector3.get_one());
					instance.m_dataColor0 = buildingInfo.m_buildingAI.GetColor(buildingID, ref data, Singleton<InfoManager>.get_instance().CurrentMode);
					float num29 = num26 * num24;
					float num30;
					if (num29 > buildingInfo.m_size.y)
					{
						num30 = buildingInfo.m_size.y * 2f - num29;
					}
					else
					{
						num30 = buildingInfo.m_size.y;
					}
					if (num30 > 0f)
					{
						instance.m_dataVector0.y = -num30;
						instance.m_dataVector0.x = num28 * num24;
						buildingInfo.m_buildingAI.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
						num21 = Mathf.Max(num21, instance.m_dataVector0.y);
						if (instance.m_dataVector0.y >= buildingInfo.m_size.y && instance.m_dataInt0 == buildingInfo.m_prefabDataIndex)
						{
							layerMask &= ~(1 << Singleton<TreeManager>.get_instance().m_treeLayer);
							buildingInfo.m_buildingAI.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, true, true);
						}
					}
				}
				float num31 = data.m_angle;
				int length = data.Length;
				int num32 = 0;
				if (buildingInfo != null && buildingInfo2 != null)
				{
					if (buildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft && buildingInfo2.m_zoningMode == BuildingInfo.ZoningMode.CornerRight)
					{
						num31 -= 1.57079637f;
						num32 = -1;
						length = data.Width;
					}
					else if (buildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerRight && buildingInfo2.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft)
					{
						num31 += 1.57079637f;
						num32 = 1;
						length = data.Width;
					}
				}
				instance.m_position = Building.CalculateMeshPosition(buildingInfo2, data.m_position, num31, length);
				instance.m_rotation = Quaternion.AngleAxis(num31 * 57.29578f, Vector3.get_down());
				instance.m_dataMatrix1.SetTRS(instance.m_position, instance.m_rotation, Vector3.get_one());
				instance.m_dataColor0 = buildingInfo2.m_buildingAI.GetColor(buildingID, ref data, Singleton<InfoManager>.get_instance().CurrentMode);
				if (num27 > 0f)
				{
					instance.m_dataVector0.y = -num27;
					instance.m_dataVector0.x = num28 * num24;
					buildingInfo2.m_buildingAI.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
					num21 = Mathf.Max(num21, instance.m_dataVector0.y);
					if (num27 >= buildingInfo2.m_size.y && instance.m_dataInt0 == buildingInfo2.m_prefabDataIndex)
					{
						layerMask &= ~(1 << Singleton<TreeManager>.get_instance().m_treeLayer);
						buildingInfo2.m_buildingAI.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, true, true);
					}
				}
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				if (instance2.m_common != null)
				{
					BuildingInfoBase construction = instance2.m_common.m_construction;
					Vector3 vector6 = buildingInfo2.m_generatedInfo.m_max;
					Vector3 vector7 = buildingInfo2.m_generatedInfo.m_min;
					if (buildingInfo != null)
					{
						Vector3 zero = Vector3.get_zero();
						zero.z = -Building.CalculateLocalMeshOffset(buildingInfo2, length);
						if (num32 == -1)
						{
							zero.x -= Building.CalculateLocalMeshOffset(buildingInfo, data.Length);
							Vector3 max = buildingInfo.m_generatedInfo.m_max;
							Vector3 min = buildingInfo.m_generatedInfo.m_min;
							vector6 = Vector3.Max(vector6, new Vector3(max.z, max.y, -min.x) - zero);
							vector7 = Vector3.Min(vector7, new Vector3(min.z, min.y, -max.x) - zero);
						}
						else if (num32 == 1)
						{
							zero.x += Building.CalculateLocalMeshOffset(buildingInfo, data.Length);
							Vector3 max2 = buildingInfo.m_generatedInfo.m_max;
							Vector3 min2 = buildingInfo.m_generatedInfo.m_min;
							vector6 = Vector3.Max(vector6, new Vector3(max2.z, max2.y, max2.x) - zero);
							vector7 = Vector3.Min(vector7, new Vector3(min2.z, min2.y, min2.x) - zero);
						}
						else
						{
							zero.z += Building.CalculateLocalMeshOffset(buildingInfo, data.Length);
							vector6 = Vector3.Max(vector6, buildingInfo.m_generatedInfo.m_max - zero);
							vector7 = Vector3.Min(vector7, buildingInfo.m_generatedInfo.m_min - zero);
						}
					}
					Vector3 vector8 = vector6 - vector7;
					float num33 = (vector8.x + 1f) / Mathf.Max(1f, construction.m_generatedInfo.m_size.x);
					float num34 = (vector8.z + 1f) / Mathf.Max(1f, construction.m_generatedInfo.m_size.z);
					Vector3 vector9;
					vector9..ctor((vector6.x + vector7.x) * 0.5f, 0f, (vector6.z + vector7.z) * 0.5f);
					Vector3 vector10;
					vector10..ctor(num33, num24, num34);
					Matrix4x4 matrix2 = Matrix4x4.TRS(vector9, Quaternion.get_identity(), vector10);
					if (num28 > 0f)
					{
						instance.m_dataVector0.y = num28;
						BuildingAI.RenderMesh(cameraInfo, buildingInfo2, construction, matrix2, ref instance);
						num21 = Mathf.Max(num21, instance.m_dataVector0.y);
					}
				}
				instance.m_dataVector0.y = num21;
			}
		}
		else
		{
			if (data.m_garbageBuffer >= 1000)
			{
				Randomizer randomizer2;
				randomizer2..ctor((int)buildingID);
				PropInfo randomPropInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer2, ItemClass.Service.Garbage);
				if (randomPropInfo != null && (layerMask & 1 << randomPropInfo.m_prefabDataLayer) != 0 && !randomPropInfo.m_requireHeightMap)
				{
					int num35 = Mathf.Min(8, (int)(data.m_garbageBuffer / 1000));
					int width = data.Width;
					int length2 = data.Length;
					for (int i = 0; i < num35; i++)
					{
						PropInfo variation = randomPropInfo.GetVariation(ref randomizer2);
						float scale = variation.m_minScale + (float)randomizer2.Int32(10000u) * (variation.m_maxScale - variation.m_minScale) * 0.0001f;
						float angle2 = (float)randomizer2.Int32(10000u) * 0.0006283185f;
						Color color = variation.GetColor(ref randomizer2);
						Vector3 vector11;
						vector11.x = ((float)randomizer2.Int32(10000u) * 0.0001f - 0.5f) * (float)width * 4f;
						vector11.y = 0f;
						vector11.z = (float)randomizer2.Int32(10000u) * 0.0001f - 0.5f + (float)length2 * 4f;
						vector11 = instance.m_dataMatrix0.MultiplyPoint(vector11);
						vector11.y = (float)instance.m_extraData.GetUShort(64 + i) * 0.015625f;
						if (cameraInfo.CheckRenderDistance(vector11, variation.m_maxRenderDistance))
						{
							Vector4 objectIndex;
							objectIndex..ctor(0.001953125f, 0.00260416674f, 0f, 0f);
							PropInstance.RenderInstance(cameraInfo, variation, new InstanceID
							{
								Building = buildingID
							}, vector11, scale, angle2, color, objectIndex, (data.m_flags & Building.Flags.Active) != Building.Flags.None);
						}
					}
				}
			}
			uint num36 = (uint)(((int)buildingID << 8) / 49152);
			uint num37 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num36;
			float num38 = ((num37 & 255u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.00390625f;
			Building.Frame frameData5 = data.GetFrameData(num37 - 512u);
			Building.Frame frameData6 = data.GetFrameData(num37 - 256u);
			instance.m_dataVector0.x = Mathf.Max(0f, (Mathf.Lerp((float)frameData5.m_fireDamage, (float)frameData6.m_fireDamage, num38) - 127f) * 0.0078125f);
			instance.m_dataVector0.z = (((data.m_flags & Building.Flags.Abandoned) == Building.Flags.None) ? 0f : 1f);
			if (frameData6.m_productionState < frameData5.m_productionState)
			{
				instance.m_dataVector3.w = Mathf.Lerp((float)frameData5.m_productionState, (float)frameData6.m_productionState + 256f, num38) * 0.00390625f;
				if (instance.m_dataVector3.w >= 1f)
				{
					instance.m_dataVector3.w = instance.m_dataVector3.w - 1f;
				}
			}
			else
			{
				instance.m_dataVector3.w = Mathf.Lerp((float)frameData5.m_productionState, (float)frameData6.m_productionState, num38) * 0.00390625f;
			}
			base.RenderInstance(cameraInfo, buildingID, ref data, layerMask, ref instance);
			if (data.m_fireIntensity != 0 && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
			{
				this.RenderFireEffect(cameraInfo, buildingID, ref data, instance.m_dataVector0.x);
				this.RenderFireEffectProps(cameraInfo, buildingID, ref data, ref instance, (float)data.m_fireIntensity * 0.003921569f, instance.m_dataVector0.x, true, true);
			}
		}
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			uint num = (uint)(((int)buildingID << 8) / 49152);
			uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
			float num3 = ((num2 & 255u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.00390625f;
			Building.Frame frameData = data.GetFrameData(num2 - 512u);
			Building.Frame frameData2 = data.GetFrameData(num2 - 256u);
			float num4 = (float)this.GetCollapseTime();
			num4 /= Mathf.Max(1f, num4 - 6f);
			float num5 = Mathf.Lerp((float)frameData.m_constructState, (float)frameData2.m_constructState, num3) * 0.003921569f;
			float collapseProgress = 1f - num4 * (1f - num5);
			this.PlayCollapseEffect(listenerInfo, buildingID, ref data, collapseProgress);
		}
		else if (data.m_fireIntensity != 0)
		{
			uint num6 = (uint)(((int)buildingID << 8) / 49152);
			uint num7 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num6;
			float num8 = ((num7 & 255u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.00390625f;
			Building.Frame frameData3 = data.GetFrameData(num7 - 512u);
			Building.Frame frameData4 = data.GetFrameData(num7 - 256u);
			float fireDamage = Mathf.Max(0f, (Mathf.Lerp((float)frameData3.m_fireDamage, (float)frameData4.m_fireDamage, num8) - 127f) * 0.0078125f);
			this.PlayFireEffect(listenerInfo, buildingID, ref data, fireDamage);
		}
		else if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data, 1f);
		}
	}

	public void RenderFireEffect(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, float fireDamage)
	{
		float num = (0.5f - Mathf.Abs(fireDamage - 0.5f)) * (float)data.m_fireIntensity * 0.007843138f;
		if (num < 0.01f)
		{
			return;
		}
		float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		Vector3 vector;
		Quaternion quaternion;
		data.CalculateMeshPosition(out vector, out quaternion);
		Matrix4x4 matrix = Matrix4x4.TRS(vector, quaternion, Vector3.get_one());
		InstanceID id = default(InstanceID);
		id.Building = buildingID;
		num /= 1f + this.m_info.m_generatedInfo.m_triangleArea * 0.0002f;
		EffectInfo fireEffect = Singleton<BuildingManager>.get_instance().m_properties.m_fireEffect;
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix, this.m_info.m_lodMeshData);
		fireEffect.RenderEffect(id, area, Vector3.get_zero(), 0f, num, -1f, simulationTimeDelta, cameraInfo);
	}

	private void RenderFireEffectProps(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, ref RenderManager.Instance instance, float fireIntensity, float fireDamage, bool renderFixed, bool renderNonfixed)
	{
		float num = (0.5f - Mathf.Abs(fireDamage - 0.5f)) * fireIntensity;
		if (num < 0.01f)
		{
			return;
		}
		if (this.m_info.m_props == null)
		{
			return;
		}
		int length = data.Length;
		for (int i = 0; i < this.m_info.m_props.Length; i++)
		{
			BuildingInfo.Prop prop = this.m_info.m_props[i];
			Randomizer randomizer;
			randomizer..ctor((int)buildingID << 6 | prop.m_index);
			if (randomizer.Int32(100u) < prop.m_probability && length >= prop.m_requiredLength)
			{
				PropInfo finalProp = prop.m_finalProp;
				TreeInfo treeInfo = prop.m_finalTree;
				if (!(finalProp != null))
				{
					if (treeInfo != null)
					{
						treeInfo = treeInfo.GetVariation(ref randomizer);
						float num2 = treeInfo.m_minScale + (float)randomizer.Int32(10000u) * (treeInfo.m_maxScale - treeInfo.m_minScale) * 0.0001f;
						if ((!prop.m_fixedHeight) ? renderNonfixed : renderFixed)
						{
							Vector3 vector = instance.m_dataMatrix1.MultiplyPoint(prop.m_position);
							if (!prop.m_fixedHeight || this.m_info.m_requireHeightMap)
							{
								vector.y = (float)instance.m_extraData.GetUShort(i) * 0.015625f;
							}
							if (cameraInfo.Intersect(vector, treeInfo.m_generatedInfo.m_size.y))
							{
								float timeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta * num;
								InstanceID id = default(InstanceID);
								id.SetBuildingProp(buildingID, i);
								EffectInfo fireEffect = Singleton<BuildingManager>.get_instance().m_properties.m_fireEffect;
								float radius = (treeInfo.m_generatedInfo.m_size.x + treeInfo.m_generatedInfo.m_size.z) * num2 * 0.05f;
								EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(vector, Vector3.get_up(), radius, treeInfo.m_generatedInfo.m_size.y * num2);
								fireEffect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, timeDelta, cameraInfo);
							}
						}
					}
				}
			}
		}
	}

	public void RenderCollapseEffect(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, float collapseProgress)
	{
		if (collapseProgress < 0.01f || collapseProgress > 0.99f)
		{
			return;
		}
		BuildingInfoBase collapsedInfo = this.m_info.m_collapsedInfo;
		if (collapsedInfo != null)
		{
			float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
			int width = data.Width;
			int length = data.Length;
			Randomizer randomizer;
			randomizer..ctor((int)buildingID);
			int num = randomizer.Int32(4u);
			if ((1 << num & this.m_info.m_collapsedRotations) == 0)
			{
				num = (num + 1 & 3);
			}
			Vector3 vector = this.m_info.m_generatedInfo.m_min;
			Vector3 vector2 = this.m_info.m_generatedInfo.m_max;
			float num2 = (float)width * 4f;
			float num3 = (float)length * 4f;
			float num4 = Building.CalculateLocalMeshOffset(this.m_info, length);
			vector = Vector3.Max(vector - new Vector3(4f, 0f, 4f + num4), new Vector3(-num2, 0f, -num3));
			vector2 = Vector3.Min(vector2 + new Vector3(4f, 0f, 4f - num4), new Vector3(num2, 0f, num3));
			Vector3 vector3 = (vector + vector2) * 0.5f;
			Vector3 vector4 = vector2 - vector;
			float num5 = (((num & 1) != 0) ? vector4.z : vector4.x) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
			float num6 = (((num & 1) != 0) ? vector4.x : vector4.z) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
			Quaternion quaternion = Quaternion.AngleAxis((float)num * 90f, Vector3.get_down());
			Matrix4x4 matrix4x = Matrix4x4.TRS(new Vector3(vector3.x, 0f, vector3.z + num4), quaternion, new Vector3(num5, 1f, num6));
			Vector3 vector5;
			Quaternion quaternion2;
			data.CalculateMeshPosition(out vector5, out quaternion2);
			Matrix4x4 matrix4x2 = Matrix4x4.TRS(vector5, quaternion2, Vector3.get_one());
			EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix4x2 * matrix4x, collapsedInfo.m_lodMeshData);
			InstanceID id = default(InstanceID);
			id.Building = buildingID;
			EffectInfo effectInfo;
			if ((data.m_flags & Building.Flags.Flooded) != Building.Flags.None)
			{
				effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_collapseFloodedEffect;
			}
			else
			{
				effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_collapseEffect;
			}
			effectInfo.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, simulationTimeDelta, cameraInfo);
		}
	}

	public void PlayFireEffect(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data, float fireDamage)
	{
		float num = (0.5f - Mathf.Abs(fireDamage - 0.5f)) * (float)data.m_fireIntensity * 0.007843138f;
		if (num < 0.01f)
		{
			return;
		}
		Vector3 vector;
		Quaternion quaternion;
		data.CalculateMeshPosition(out vector, out quaternion);
		Matrix4x4 matrix = Matrix4x4.TRS(vector, quaternion, Vector3.get_one());
		InstanceID id = default(InstanceID);
		id.Building = buildingID;
		EffectInfo fireEffect = Singleton<BuildingManager>.get_instance().m_properties.m_fireEffect;
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix, this.m_info.m_lodMeshData);
		fireEffect.PlayEffect(id, area, Vector3.get_zero(), 0f, num, listenerInfo, Singleton<BuildingManager>.get_instance().m_audioGroup);
	}

	public void PlayCollapseEffect(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data, float collapseProgress)
	{
		if (collapseProgress < 0.01f || collapseProgress > 0.99f)
		{
			return;
		}
		Vector3 vector;
		Quaternion quaternion;
		data.CalculateMeshPosition(out vector, out quaternion);
		Matrix4x4 matrix = Matrix4x4.TRS(vector, quaternion, Vector3.get_one());
		InstanceID id = default(InstanceID);
		id.Building = buildingID;
		EffectInfo effectInfo;
		if ((data.m_flags & Building.Flags.Flooded) != Building.Flags.None)
		{
			effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_collapseFloodedEffect;
		}
		else
		{
			effectInfo = Singleton<BuildingManager>.get_instance().m_properties.m_collapseEffect;
		}
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(matrix, this.m_info.m_lodMeshData);
		effectInfo.PlayEffect(id, area, Vector3.get_zero(), 0f, 1f, listenerInfo, Singleton<BuildingManager>.get_instance().m_audioGroup);
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (!this.m_ignoreNoPropsWarning && (this.m_info.m_props == null || this.m_info.m_props.Length == 0))
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "No props placed: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
	}

	public override void DestroyPrefab()
	{
		base.DestroyPrefab();
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		if (this.GetConstructionTime() == 0)
		{
			data.m_frame0.m_constructState = 255;
			this.BuildingCompleted(buildingID, ref data);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ManualDeactivation(buildingID, ref data);
		this.BuildingDeactivated(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BeginRelocating(ushort buildingID, ref Building data)
	{
		base.BeginRelocating(buildingID, ref data);
		this.ManualDeactivation(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		this.ManualActivation(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building data)
	{
		base.SimulationStep(buildingID, ref data);
		if ((data.m_flags & Building.Flags.Demolishing) != Building.Flags.None)
		{
			uint num = (uint)(((int)buildingID << 8) / 49152);
			uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - num;
			if ((data.m_flags & Building.Flags.Collapsed) == Building.Flags.None || data.GetFrameData(num2 - 256u).m_constructState == 0)
			{
				Singleton<BuildingManager>.get_instance().ReleaseBuilding(buildingID);
			}
		}
	}

	private void BuildingCollapsed(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		if ((buildingData.m_flags & Building.Flags.Demolishing) == Building.Flags.None)
		{
			InstanceID id = default(InstanceID);
			id.Building = buildingID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
			Singleton<InstanceManager>.get_instance().SetGroup(id, null);
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
			if ((buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
			{
				if (frameData.m_fireDamage == 255)
				{
					buildingData.m_problems = (Notification.Problem.Fire | Notification.Problem.FatalProblem);
				}
				else
				{
					buildingData.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
				}
			}
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = buildingData.m_netNode;
			ushort num2 = 0;
			int num3 = 0;
			while (num != 0)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
					if ((instance.m_segments.m_buffer[(int)segment].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed | NetSegment.Flags.Untouchable)) == (NetSegment.Flags.Created | NetSegment.Flags.Untouchable))
					{
						NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
						info.m_netAI.ParentCollapsed(segment, ref instance.m_segments.m_buffer[(int)segment], group);
					}
				}
				ushort nextBuildingNode = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				if (instance.m_nodes.m_buffer[(int)num].CountSegments() == 0)
				{
					if (num2 != 0)
					{
						instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode = nextBuildingNode;
					}
					else
					{
						buildingData.m_netNode = nextBuildingNode;
					}
					instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode = 0;
					instance.ReleaseNode(num);
				}
				else
				{
					num2 = num;
				}
				num = nextBuildingNode;
				if (++num3 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_flags & Building.Flags.RoadAccessFailed) != Building.Flags.None && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(16u) == 0)
		{
			buildingData.m_flags &= ~Building.Flags.RoadAccessFailed;
		}
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			if ((ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(buildingID & 15)))
			{
				StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.CollapsedCount);
				statisticBase.Add(256);
			}
			if (frameData.m_constructState != 0)
			{
				int collapseTime = this.GetCollapseTime();
				frameData.m_constructState = (byte)Mathf.Max(0, (int)frameData.m_constructState - 1088 / collapseTime);
				buildingData.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
				if (frameData.m_constructState == 0)
				{
					this.BuildingCollapsed(buildingID, ref buildingData, ref frameData);
				}
			}
			else if (buildingData.m_fireIntensity != 0)
			{
				InstanceID id = default(InstanceID);
				id.Building = buildingID;
				Singleton<InstanceManager>.get_instance().SetGroup(id, null);
				buildingData.m_fireIntensity = 0;
				Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
			}
			else if ((buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
			{
				if (buildingData.m_levelUpProgress != 255)
				{
					int num = 0;
					int num2 = 0;
					int num3 = 0;
					int num4 = 0;
					this.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Collapsed, ref num, ref num2, ref num3, ref num4);
					this.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Collapsed2, ref num, ref num2, ref num3, ref num4);
					if (num == 0)
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = 4;
						offer.Building = buildingID;
						offer.Position = buildingData.m_position;
						offer.Amount = 1;
						offer.Active = false;
						if ((buildingData.m_flags & Building.Flags.RoadAccessFailed) != Building.Flags.None)
						{
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Collapsed2, offer);
						}
						else if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(16u) == 0)
						{
							offer.Priority = 1;
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Collapsed2, offer);
						}
						else
						{
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Collapsed, offer);
						}
					}
					if (frameData.m_fireDamage == 255)
					{
						buildingData.m_problems = (Notification.Problem.Fire | Notification.Problem.FatalProblem);
					}
					else
					{
						buildingData.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
					}
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						if (Singleton<LoadingManager>.get_instance().SupportsExpansion(2) && Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.DisasterResponse))
						{
							if (Singleton<BuildingManager>.get_instance().m_buildingDestroyed.m_disabled && ItemClass.GetPublicServiceIndex(this.m_info.m_class.m_service) != -1)
							{
								Singleton<BuildingManager>.get_instance().m_buildingDestroyed2.Activate(properties.m_buildingDestroyed2, buildingID);
							}
							else
							{
								Singleton<BuildingManager>.get_instance().m_buildingDestroyed.Activate(properties.m_buildingDestroyed, buildingID);
							}
						}
						else if (frameData.m_fireDamage == 255)
						{
							Singleton<BuildingManager>.get_instance().m_buildingBurned.Activate(properties.m_buildingBurned, buildingID);
						}
					}
				}
				else if (this.m_info.m_placementStyle == ItemClass.Placement.Automatic)
				{
					buildingData.m_problems = (Notification.Problem.StructureVisited | Notification.Problem.FatalProblem);
				}
				else
				{
					buildingData.m_problems = (Notification.Problem.StructureVisitedService | Notification.Problem.FatalProblem);
				}
			}
			float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Abandonment, 10, buildingData.m_position, radius);
		}
		else if ((buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None)
		{
			if ((buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None && !this.CollapseIfFlooded(buildingID, ref buildingData, ref frameData))
			{
				GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
				if (properties2 != null)
				{
					Singleton<BuildingManager>.get_instance().m_buildingAbandoned1.Activate(properties2.m_buildingAbandoned1, buildingID);
					Singleton<BuildingManager>.get_instance().m_buildingAbandoned2.Activate(properties2.m_buildingAbandoned2, buildingID);
				}
				if (buildingData.m_majorProblemTimer < 255)
				{
					buildingData.m_majorProblemTimer += 1;
				}
			}
			float radius2 = (float)(buildingData.Width + buildingData.Length) * 2.5f;
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Abandonment, 10, buildingData.m_position, radius2);
		}
		else if ((buildingData.m_flags & Building.Flags.Completed) == Building.Flags.None)
		{
			bool flag = (buildingData.m_flags & Building.Flags.Upgrading) != Building.Flags.None;
			int constructionTime = this.GetConstructionTime();
			if (constructionTime == 0)
			{
				frameData.m_constructState = 255;
			}
			else
			{
				frameData.m_constructState = (byte)Mathf.Min(255, (int)frameData.m_constructState + 1088 / constructionTime);
				buildingData.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
			}
			if (frameData.m_constructState == 255)
			{
				frameData.m_fireDamage = 0;
				this.BuildingCompleted(buildingID, ref buildingData);
				if ((buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
				{
					GuideController properties3 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties3 != null)
					{
						Singleton<BuildingManager>.get_instance().m_buildingLevelUp.Deactivate(buildingID, true);
					}
				}
			}
			else if (flag && (buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
			{
				GuideController properties4 = Singleton<GuideManager>.get_instance().m_properties;
				if (properties4 != null)
				{
					Singleton<BuildingManager>.get_instance().m_buildingLevelUp.Activate(properties4.m_buildingLevelUp, buildingID);
				}
			}
			if (flag)
			{
				this.SimulationStepActive(buildingID, ref buildingData, ref frameData);
			}
		}
		else
		{
			this.SimulationStepActive(buildingID, ref buildingData, ref frameData);
		}
	}

	protected bool CollapseIfFlooded(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		bool flag;
		if ((buildingData.m_flags & Building.Flags.Untouchable) == Building.Flags.None && this.CanSufferFromFlood(out flag))
		{
			float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(buildingData.m_position));
			if (num > buildingData.m_position.y + Mathf.Max(4f, this.m_info.m_collisionHeight))
			{
				frameData.m_constructState = (byte)Mathf.Max(0, (int)frameData.m_constructState - 1088 / this.GetCollapseTime());
				buildingData.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
				if (frameData.m_constructState == 0)
				{
					InstanceID id = default(InstanceID);
					id.Building = buildingID;
					Singleton<InstanceManager>.get_instance().SetGroup(id, null);
				}
				buildingData.m_fireIntensity = 0;
				buildingData.m_garbageBuffer = 0;
				buildingData.m_flags |= Building.Flags.Collapsed;
				if (this.m_info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
				{
					Singleton<BuildingManager>.get_instance().UpdateParkingSpaces(buildingID, ref buildingData);
				}
				Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<BuildingManager>.get_instance().m_buildingFlooded.Deactivate(buildingID, false);
					Singleton<BuildingManager>.get_instance().m_buildingFlooded2.Deactivate(buildingID, false);
					Singleton<BuildingManager>.get_instance().m_buildingAbandoned1.Deactivate(buildingID, false);
					Singleton<BuildingManager>.get_instance().m_buildingAbandoned2.Deactivate(buildingID, false);
				}
				if (buildingData.m_subBuilding != 0 && buildingData.m_parentBuilding == 0)
				{
					int num2 = 0;
					ushort subBuilding = buildingData.m_subBuilding;
					while (subBuilding != 0)
					{
						BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].Info;
						info.m_buildingAI.CollapseBuilding(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding], null, false, false, 0);
						subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
						if (++num2 > 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	protected void EmptyBuilding(ushort buildingID, ref Building data, CitizenUnit.Flags flags, bool onlyMoving)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flags) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u)
					{
						ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
						if (((!onlyMoving && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == buildingID) || (instance2 != 0 && instance.m_instances.m_buffer[(int)instance2].m_targetBuilding == buildingID)) && !instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Collapsed)
						{
							ushort num3 = 0;
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding == buildingID)
							{
								num3 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding;
							}
							else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding == buildingID)
							{
								num3 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
							}
							else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding == buildingID)
							{
								if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested)
								{
									instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested = false;
									if (instance2 != 0)
									{
										instance.ReleaseCitizenInstance(instance2);
									}
								}
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, 0, 0u);
								num3 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
							}
							if (num3 != 0)
							{
								CitizenInfo citizenInfo = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
								HumanAI humanAI = citizenInfo.m_citizenAI as HumanAI;
								if (humanAI != null)
								{
									Citizen[] expr_242_cp_0 = instance.m_citizens.m_buffer;
									UIntPtr expr_242_cp_1 = (UIntPtr)citizen;
									expr_242_cp_0[(int)expr_242_cp_1].m_flags = (expr_242_cp_0[(int)expr_242_cp_1].m_flags & ~Citizen.Flags.Evacuating);
									humanAI.StartMoving(citizen, ref instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)], buildingID, num3);
								}
							}
						}
					}
				}
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected virtual int GetConstructionTime()
	{
		return 0;
	}

	protected virtual int GetCollapseTime()
	{
		return Mathf.Clamp(Mathf.RoundToInt(Mathf.Sqrt(this.m_info.m_size.y)), 8, 40);
	}

	protected virtual void BuildingCompleted(ushort buildingID, ref Building buildingData)
	{
		Building.Flags flags = buildingData.m_flags;
		if ((flags & Building.Flags.Upgrading) != Building.Flags.None)
		{
			flags &= ~Building.Flags.Upgrading;
			flags |= Building.Flags.Completed;
			buildingData.m_flags = flags;
			BuildingInfo upgradeInfo = this.GetUpgradeInfo(buildingID, ref buildingData);
			if (upgradeInfo != null)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingInfo(buildingID, upgradeInfo);
				upgradeInfo.m_buildingAI.BuildingUpgraded(buildingID, ref buildingData);
				this.ManualActivation(buildingID, ref buildingData);
				return;
			}
		}
		flags |= Building.Flags.Completed;
		buildingData.m_flags = flags;
		if (this.GetConstructionTime() != 0)
		{
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
		}
		this.ManualActivation(buildingID, ref buildingData);
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		if (!base.CanUseGroupMesh(buildingID, ref buildingData))
		{
			return false;
		}
		if ((buildingData.m_flags & Building.Flags.Demolishing) != Building.Flags.None)
		{
			return false;
		}
		Building.Frame lastFrameData = buildingData.GetLastFrameData();
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			return lastFrameData.m_constructState == 0 && buildingData.m_fireIntensity == 0;
		}
		if ((buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None)
		{
			return buildingData.m_fireIntensity == 0;
		}
		return (buildingData.m_flags & Building.Flags.Completed) != Building.Flags.None && lastFrameData.m_fireDamage == 0 && buildingData.m_fireIntensity == 0;
	}

	public override Color32 GetGroupVertexColor(ushort buildingID, ref Building buildingData, BuildingInfoBase info, int vertexIndex, bool baseVertex)
	{
		Color32 groupVertexColor = base.GetGroupVertexColor(buildingID, ref buildingData, info, vertexIndex, baseVertex);
		groupVertexColor.r = buildingData.GetLastFrameData().m_fireDamage;
		groupVertexColor.b = (((buildingData.m_flags & Building.Flags.Abandoned) == Building.Flags.None) ? 0 : 255);
		return groupVertexColor;
	}

	public override ToolBase.ToolErrors CheckBulldozing(ushort buildingID, ref Building data)
	{
		if (data.m_fireIntensity != 0)
		{
			return ToolBase.ToolErrors.ObjectOnFire;
		}
		return base.CheckBulldozing(buildingID, ref data);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		if (relocateID != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (instance.m_buildings.m_buffer[(int)relocateID].m_fireIntensity != 0)
			{
				toolErrors |= ToolBase.ToolErrors.ObjectOnFire;
			}
			else if ((instance.m_buildings.m_buffer[(int)relocateID].m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				if (instance.m_buildings.m_buffer[(int)relocateID].GetLastFrameData().m_fireDamage == 255)
				{
					toolErrors |= ToolBase.ToolErrors.BurnedDown;
				}
				else
				{
					toolErrors |= ToolBase.ToolErrors.Collapsed;
				}
			}
		}
		return toolErrors;
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.Garbage)
		{
			int garbageBuffer = (int)data.m_garbageBuffer;
			amountDelta = Mathf.Clamp(amountDelta, -garbageBuffer, 65535 - garbageBuffer);
			data.m_garbageBuffer = (ushort)(garbageBuffer + amountDelta);
		}
		else if (material == TransferManager.TransferReason.Crime)
		{
			int crimeBuffer = (int)data.m_crimeBuffer;
			amountDelta = Mathf.Clamp(amountDelta, -crimeBuffer, 65535 - crimeBuffer);
			data.m_crimeBuffer = (ushort)(crimeBuffer + amountDelta);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Garbage)
		{
			amount = (int)data.m_garbageBuffer;
			max = 65535;
		}
		else if (material == TransferManager.TransferReason.Crime)
		{
			amount = (int)data.m_crimeBuffer;
			max = 65535;
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override float ElectricityGridRadius()
	{
		return 32f;
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Garbage, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Crime, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Sick, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Sick2, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Dead, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Fire, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Fire2, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ForestFire, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Collapsed, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Collapsed2, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Worker0, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Worker1, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Worker2, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Worker3, offer);
		data.m_flags &= ~Building.Flags.Active;
		this.EmptyBuilding(buildingID, ref data, CitizenUnit.Flags.Created, false);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected virtual void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Garbage);
		if (buildingData.m_garbageBuffer >= 2000)
		{
			int num = (int)(buildingData.m_garbageBuffer / 1000);
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u) == 0)
			{
				Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num, num, buildingData.m_position, 0f);
			}
			if (num >= 3)
			{
				if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Garbage))
				{
					if (num >= 6)
					{
						problem = Notification.AddProblems(problem, Notification.Problem.Garbage | Notification.Problem.MajorProblem);
					}
					else
					{
						problem = Notification.AddProblems(problem, Notification.Problem.Garbage);
					}
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						int publicServiceIndex = ItemClass.GetPublicServiceIndex(ItemClass.Service.Garbage);
						Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Activate(properties.m_serviceNeeded, ItemClass.Service.Garbage);
					}
				}
				else
				{
					buildingData.m_garbageBuffer = 2000;
				}
			}
		}
		buildingData.m_problems = problem;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (buildingData.m_crimeBuffer != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.CrimeRate, (int)buildingData.m_crimeBuffer, buildingData.m_position, radius);
		}
		int num2;
		int num3;
		int num4;
		if (this.GetFireParameters(buildingID, ref buildingData, out num2, out num3, out num4))
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			if ((servicePolicies & DistrictPolicies.Services.SmokeDetectors) != DistrictPolicies.Services.None)
			{
				num2 = num2 * 75 / 100;
			}
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods) != DistrictPolicies.CityPlanning.None)
			{
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 10, this.m_info.m_class);
			}
		}
		num2 = 100 - (10 + num4) * 50000 / ((100 + num2) * (100 + num3));
		if (num2 > 0)
		{
			num2 = num2 * buildingData.Width * buildingData.Length;
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.FireHazard, num2, buildingData.m_position, radius);
		}
		Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.FirewatchCoverage, 50, buildingData.m_position, 100f);
		if (Singleton<DisasterManager>.get_instance().IsEvacuating(buildingData.m_position))
		{
			if ((buildingData.m_flags & Building.Flags.Evacuating) == Building.Flags.None && this.CanEvacuate())
			{
				int num5;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.RadioCoverage, buildingData.m_position, out num5);
				if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < num5 + 10)
				{
					this.SetEvacuating(buildingID, ref buildingData, true);
				}
			}
		}
		else if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			this.SetEvacuating(buildingID, ref buildingData, false);
		}
	}

	protected virtual void SetEvacuating(ushort buildingID, ref Building data, bool evacuating)
	{
		if (evacuating)
		{
			data.m_flags |= Building.Flags.Evacuating;
			this.BuildingDeactivated(buildingID, ref data);
		}
		else
		{
			data.m_flags &= ~Building.Flags.Evacuating;
		}
	}

	protected virtual bool CanEvacuate()
	{
		return true;
	}

	protected int HandleCommonConsumption(ushort buildingID, ref Building data, ref Building.Frame frameData, ref int electricityConsumption, ref int heatingConsumption, ref int waterConsumption, ref int sewageAccumulation, ref int garbageAccumulation, DistrictPolicies.Services policies)
	{
		int num = 100;
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
		bool flag = data.m_electricityProblemTimer != 0;
		bool flag2 = false;
		bool flag3 = false;
		int electricityUsage = 0;
		int heatingUsage = 0;
		int waterUsage = 0;
		int sewageUsage = 0;
		if (electricityConsumption != 0)
		{
			int num2 = Mathf.RoundToInt((20f - Singleton<WeatherManager>.get_instance().SampleTemperature(data.m_position, false)) * 8f);
			num2 = Mathf.Clamp(num2, 0, 400);
			int num3 = heatingConsumption;
			heatingConsumption = (num3 * num2 + Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u)) / 100;
			if ((policies & DistrictPolicies.Services.PowerSaving) != DistrictPolicies.Services.None)
			{
				electricityConsumption = Mathf.Max(1, electricityConsumption * 90 / 100);
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 32, this.m_info.m_class);
			}
			bool flag4 = false;
			int num4 = heatingConsumption * 2 - (int)data.m_heatingBuffer;
			if (num4 > 0 && (policies & DistrictPolicies.Services.OnlyElectricity) == DistrictPolicies.Services.None)
			{
				int num5 = Singleton<WaterManager>.get_instance().TryFetchHeating(data.m_position, heatingConsumption, num4, out flag4);
				data.m_heatingBuffer += (ushort)num5;
			}
			if ((int)data.m_heatingBuffer < heatingConsumption)
			{
				if ((policies & DistrictPolicies.Services.NoElectricity) != DistrictPolicies.Services.None)
				{
					flag3 = true;
					data.m_heatingProblemTimer = (byte)Mathf.Min(255, (int)(data.m_heatingProblemTimer + 1));
					if (data.m_heatingProblemTimer >= 65)
					{
						num = 0;
						problem = Notification.AddProblems(problem, Notification.Problem.Heating | Notification.Problem.MajorProblem);
					}
					else if (data.m_heatingProblemTimer >= 3)
					{
						num /= 2;
						problem = Notification.AddProblems(problem, Notification.Problem.Heating);
					}
				}
				else
				{
					num2 = ((num2 + 50) * (heatingConsumption - (int)data.m_heatingBuffer) + heatingConsumption - 1) / heatingConsumption;
					electricityConsumption += (num3 * num2 + Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u)) / 100;
					if (flag4)
					{
						flag3 = true;
						data.m_heatingProblemTimer = (byte)Mathf.Min(255, (int)(data.m_heatingProblemTimer + 1));
						if (data.m_heatingProblemTimer >= 3)
						{
							problem = Notification.AddProblems(problem, Notification.Problem.Heating);
						}
					}
				}
				heatingUsage = (int)data.m_heatingBuffer;
				data.m_heatingBuffer = 0;
			}
			else
			{
				heatingUsage = heatingConsumption;
				data.m_heatingBuffer -= (ushort)heatingConsumption;
			}
			int num6;
			int num7;
			if (this.CanStockpileElectricity(buildingID, ref data, out num6, out num7))
			{
				num4 = num6 + electricityConsumption * 2 - (int)data.m_electricityBuffer;
				if (num4 > 0)
				{
					int num8 = electricityConsumption;
					if ((int)data.m_electricityBuffer < num6)
					{
						num8 += Mathf.Min(num7, num6 - (int)data.m_electricityBuffer);
					}
					int num9 = Singleton<ElectricityManager>.get_instance().TryFetchElectricity(data.m_position, num8, num4);
					data.m_electricityBuffer += (ushort)num9;
					if (num9 < num4 && num9 < num8)
					{
						flag2 = true;
						problem = Notification.AddProblems(problem, Notification.Problem.Electricity);
						if (data.m_electricityProblemTimer < 64)
						{
							data.m_electricityProblemTimer = 64;
						}
					}
				}
			}
			else
			{
				num4 = electricityConsumption * 2 - (int)data.m_electricityBuffer;
				if (num4 > 0)
				{
					int num10 = Singleton<ElectricityManager>.get_instance().TryFetchElectricity(data.m_position, electricityConsumption, num4);
					data.m_electricityBuffer += (ushort)num10;
				}
			}
			if ((int)data.m_electricityBuffer < electricityConsumption)
			{
				flag2 = true;
				data.m_electricityProblemTimer = (byte)Mathf.Min(255, (int)(data.m_electricityProblemTimer + 1));
				if (data.m_electricityProblemTimer >= 65)
				{
					num = 0;
					problem = Notification.AddProblems(problem, Notification.Problem.Electricity | Notification.Problem.MajorProblem);
				}
				else if (data.m_electricityProblemTimer >= 3)
				{
					num /= 2;
					problem = Notification.AddProblems(problem, Notification.Problem.Electricity);
				}
				electricityUsage = (int)data.m_electricityBuffer;
				data.m_electricityBuffer = 0;
				if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Electricity))
				{
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						int publicServiceIndex = ItemClass.GetPublicServiceIndex(ItemClass.Service.Electricity);
						int electricityCapacity = instance.m_districts.m_buffer[0].GetElectricityCapacity();
						int electricityConsumption2 = instance.m_districts.m_buffer[0].GetElectricityConsumption();
						if (electricityCapacity >= electricityConsumption2)
						{
							Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Activate(properties.m_serviceNeeded2, ItemClass.Service.Electricity);
						}
						else
						{
							Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Activate(properties.m_serviceNeeded, ItemClass.Service.Electricity);
						}
					}
				}
			}
			else
			{
				electricityUsage = electricityConsumption;
				data.m_electricityBuffer -= (ushort)electricityConsumption;
			}
		}
		else
		{
			heatingConsumption = 0;
		}
		if (!flag2)
		{
			data.m_electricityProblemTimer = 0;
		}
		if (flag != flag2)
		{
			Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
		}
		if (!flag3)
		{
			data.m_heatingProblemTimer = 0;
		}
		bool flag5 = false;
		int num11 = sewageAccumulation;
		if (waterConsumption != 0)
		{
			if ((policies & DistrictPolicies.Services.WaterSaving) != DistrictPolicies.Services.None)
			{
				waterConsumption = Mathf.Max(1, waterConsumption * 85 / 100);
				if (sewageAccumulation != 0)
				{
					sewageAccumulation = Mathf.Max(1, sewageAccumulation * 85 / 100);
				}
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 32, this.m_info.m_class);
			}
			int num12;
			int num13;
			if (this.CanStockpileWater(buildingID, ref data, out num12, out num13))
			{
				int num14 = num12 + waterConsumption * 2 - (int)data.m_waterBuffer;
				if (num14 > 0)
				{
					int num15 = waterConsumption;
					if ((int)data.m_waterBuffer < num12)
					{
						num15 += Mathf.Min(num13, num12 - (int)data.m_waterBuffer);
					}
					int num16 = Singleton<WaterManager>.get_instance().TryFetchWater(data.m_position, num15, num14, ref data.m_waterPollution);
					data.m_waterBuffer += (ushort)num16;
					if (num16 < num14 && num16 < num15)
					{
						flag5 = true;
						problem = Notification.AddProblems(problem, Notification.Problem.Water);
						if (data.m_waterProblemTimer < 64)
						{
							data.m_waterProblemTimer = 64;
						}
					}
				}
			}
			else
			{
				int num17 = waterConsumption * 2 - (int)data.m_waterBuffer;
				if (num17 > 0)
				{
					int num18 = Singleton<WaterManager>.get_instance().TryFetchWater(data.m_position, waterConsumption, num17, ref data.m_waterPollution);
					data.m_waterBuffer += (ushort)num18;
				}
			}
			if ((int)data.m_waterBuffer < waterConsumption)
			{
				flag5 = true;
				data.m_waterProblemTimer = (byte)Mathf.Min(255, (int)(data.m_waterProblemTimer + 1));
				if (data.m_waterProblemTimer >= 65)
				{
					num = 0;
					problem = Notification.AddProblems(problem, Notification.Problem.Water | Notification.Problem.MajorProblem);
				}
				else if (data.m_waterProblemTimer >= 3)
				{
					num /= 2;
					problem = Notification.AddProblems(problem, Notification.Problem.Water);
				}
				num11 = sewageAccumulation * (waterConsumption + (int)data.m_waterBuffer) / (waterConsumption << 1);
				waterUsage = (int)data.m_waterBuffer;
				data.m_waterBuffer = 0;
				if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Water))
				{
					GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties2 != null)
					{
						int publicServiceIndex2 = ItemClass.GetPublicServiceIndex(ItemClass.Service.Water);
						int waterCapacity = instance.m_districts.m_buffer[0].GetWaterCapacity();
						int waterConsumption2 = instance.m_districts.m_buffer[0].GetWaterConsumption();
						if (waterCapacity >= waterConsumption2)
						{
							Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex2].Activate(properties2.m_serviceNeeded2, ItemClass.Service.Water);
						}
						else
						{
							Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex2].Activate(properties2.m_serviceNeeded, ItemClass.Service.Water);
						}
					}
				}
			}
			else
			{
				num11 = sewageAccumulation;
				waterUsage = waterConsumption;
				data.m_waterBuffer -= (ushort)waterConsumption;
			}
		}
		int num19;
		int num20;
		if (this.CanStockpileWater(buildingID, ref data, out num19, out num20))
		{
			int num21 = Mathf.Max(0, num19 + num11 * 2 - (int)data.m_sewageBuffer);
			if (num21 < num11)
			{
				if (!flag5 && (data.m_problems & Notification.Problem.Water) == Notification.Problem.None)
				{
					flag5 = true;
					data.m_waterProblemTimer = (byte)Mathf.Min(255, (int)(data.m_waterProblemTimer + 1));
					if (data.m_waterProblemTimer >= 65)
					{
						num = 0;
						problem = Notification.AddProblems(problem, Notification.Problem.Sewage | Notification.Problem.MajorProblem);
					}
					else if (data.m_waterProblemTimer >= 3)
					{
						num /= 2;
						problem = Notification.AddProblems(problem, Notification.Problem.Sewage);
					}
				}
				sewageUsage = num21;
				data.m_sewageBuffer = (ushort)(num19 + num11 * 2);
			}
			else
			{
				sewageUsage = num11;
				data.m_sewageBuffer += (ushort)num11;
			}
			int num22 = num11 + Mathf.Max(num11, num20);
			num21 = Mathf.Min(num22, (int)data.m_sewageBuffer);
			if (num21 > 0)
			{
				int num23 = Singleton<WaterManager>.get_instance().TryDumpSewage(data.m_position, num22, num21);
				data.m_sewageBuffer -= (ushort)num23;
				if (num23 < num22 && num23 < num21 && !flag5 && (data.m_problems & Notification.Problem.Water) == Notification.Problem.None)
				{
					flag5 = true;
					problem = Notification.AddProblems(problem, Notification.Problem.Sewage);
					if (data.m_waterProblemTimer < 64)
					{
						data.m_waterProblemTimer = 64;
					}
				}
			}
		}
		else if (num11 != 0)
		{
			int num24 = Mathf.Max(0, num11 * 2 - (int)data.m_sewageBuffer);
			if (num24 < num11)
			{
				if (!flag5 && (data.m_problems & Notification.Problem.Water) == Notification.Problem.None)
				{
					flag5 = true;
					data.m_waterProblemTimer = (byte)Mathf.Min(255, (int)(data.m_waterProblemTimer + 1));
					if (data.m_waterProblemTimer >= 65)
					{
						num = 0;
						problem = Notification.AddProblems(problem, Notification.Problem.Sewage | Notification.Problem.MajorProblem);
					}
					else if (data.m_waterProblemTimer >= 3)
					{
						num /= 2;
						problem = Notification.AddProblems(problem, Notification.Problem.Sewage);
					}
				}
				sewageUsage = num24;
				data.m_sewageBuffer = (ushort)(num11 * 2);
			}
			else
			{
				sewageUsage = num11;
				data.m_sewageBuffer += (ushort)num11;
			}
			num24 = Mathf.Min(num11 * 2, (int)data.m_sewageBuffer);
			if (num24 > 0)
			{
				int num25 = Singleton<WaterManager>.get_instance().TryDumpSewage(data.m_position, num11 * 2, num24);
				data.m_sewageBuffer -= (ushort)num25;
			}
		}
		if (!flag5)
		{
			data.m_waterProblemTimer = 0;
		}
		if (garbageAccumulation != 0)
		{
			int num26 = (int)(65535 - data.m_garbageBuffer);
			if (num26 < garbageAccumulation)
			{
				num = 0;
				data.m_garbageBuffer = (ushort)num26;
			}
			else
			{
				data.m_garbageBuffer += (ushort)garbageAccumulation;
			}
		}
		if (garbageAccumulation != 0)
		{
			int num27 = (int)data.m_garbageBuffer;
			if (num27 >= 200 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u) == 0 && Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Garbage))
			{
				int num28 = 0;
				int num29 = 0;
				int num30 = 0;
				int num31 = 0;
				this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.Garbage, ref num28, ref num29, ref num30, ref num31);
				num27 -= num30 - num29;
				if (num27 >= 200)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = num27 / 1000;
					offer.Building = buildingID;
					offer.Position = data.m_position;
					offer.Amount = 1;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Garbage, offer);
				}
			}
		}
		bool flag6;
		if (this.CanSufferFromFlood(out flag6))
		{
			float num32 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(data.m_position));
			if (num32 > data.m_position.y)
			{
				bool flag7 = num32 > data.m_position.y + Mathf.Max(4f, this.m_info.m_collisionHeight);
				if ((!flag6 || flag7) && (data.m_flags & Building.Flags.Flooded) == Building.Flags.None && data.m_fireIntensity == 0)
				{
					DisasterManager instance2 = Singleton<DisasterManager>.get_instance();
					ushort num33 = instance2.FindDisaster<FloodBaseAI>(data.m_position);
					if (num33 == 0)
					{
						DisasterInfo disasterInfo = DisasterManager.FindDisasterInfo<GenericFloodAI>();
						if (disasterInfo != null && instance2.CreateDisaster(out num33, disasterInfo))
						{
							instance2.m_disasters.m_buffer[(int)num33].m_intensity = 10;
							instance2.m_disasters.m_buffer[(int)num33].m_targetPosition = data.m_position;
							disasterInfo.m_disasterAI.StartNow(num33, ref instance2.m_disasters.m_buffer[(int)num33]);
						}
					}
					if (num33 != 0)
					{
						InstanceID srcID = default(InstanceID);
						InstanceID dstID = default(InstanceID);
						srcID.Disaster = num33;
						dstID.Building = buildingID;
						Singleton<InstanceManager>.get_instance().CopyGroup(srcID, dstID);
						DisasterInfo info = instance2.m_disasters.m_buffer[(int)num33].Info;
						info.m_disasterAI.ActivateNow(num33, ref instance2.m_disasters.m_buffer[(int)num33]);
						if ((instance2.m_disasters.m_buffer[(int)num33].m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
						{
							instance2.DetectDisaster(num33, false);
							instance2.FollowDisaster(num33);
						}
					}
					data.m_flags |= Building.Flags.Flooded;
				}
				if (flag7)
				{
					frameData.m_constructState = (byte)Mathf.Max(0, (int)frameData.m_constructState - 1088 / this.GetCollapseTime());
					data.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
					InstanceID id = default(InstanceID);
					id.Building = buildingID;
					InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
					if (group != null)
					{
						ushort disaster = group.m_ownerInstance.Disaster;
						if (disaster != 0)
						{
							DisasterData[] expr_D1E_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
							ushort expr_D1E_cp_1 = disaster;
							expr_D1E_cp_0[(int)expr_D1E_cp_1].m_collapsedCount = expr_D1E_cp_0[(int)expr_D1E_cp_1].m_collapsedCount + 1;
						}
					}
					if (frameData.m_constructState == 0)
					{
						Singleton<InstanceManager>.get_instance().SetGroup(id, null);
					}
					data.m_levelUpProgress = 0;
					data.m_fireIntensity = 0;
					data.m_garbageBuffer = 0;
					data.m_flags |= Building.Flags.Collapsed;
					num = 0;
					this.RemovePeople(buildingID, ref data, 90);
					this.BuildingDeactivated(buildingID, ref data);
					if (this.m_info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
					{
						Singleton<BuildingManager>.get_instance().UpdateParkingSpaces(buildingID, ref data);
					}
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
					GuideController properties3 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties3 != null)
					{
						Singleton<BuildingManager>.get_instance().m_buildingFlooded.Deactivate(buildingID, false);
						Singleton<BuildingManager>.get_instance().m_buildingFlooded2.Deactivate(buildingID, false);
					}
					if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
					{
						int num34 = 0;
						ushort subBuilding = data.m_subBuilding;
						while (subBuilding != 0)
						{
							BuildingInfo info2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].Info;
							info2.m_buildingAI.CollapseBuilding(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding], group, false, false, 0);
							subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
							if (++num34 > 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				else if (!flag6)
				{
					if ((data.m_flags & Building.Flags.RoadAccessFailed) == Building.Flags.None)
					{
						int num35 = 0;
						int num36 = 0;
						int num37 = 0;
						int num38 = 0;
						this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.FloodWater, ref num35, ref num36, ref num37, ref num38);
						if (num35 == 0)
						{
							TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
							offer2.Priority = 5;
							offer2.Building = buildingID;
							offer2.Position = data.m_position;
							offer2.Amount = 1;
							Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.FloodWater, offer2);
						}
					}
					if (num32 > data.m_position.y + 1f)
					{
						num = 0;
						problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
					}
					else
					{
						num /= 2;
						problem = Notification.AddProblems(problem, Notification.Problem.Flood);
					}
					GuideController properties4 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties4 != null)
					{
						if (Singleton<LoadingManager>.get_instance().SupportsExpansion(2) && Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.WaterPumping))
						{
							Singleton<BuildingManager>.get_instance().m_buildingFlooded2.Activate(properties4.m_buildingFlooded2, buildingID);
						}
						else
						{
							Singleton<BuildingManager>.get_instance().m_buildingFlooded.Activate(properties4.m_buildingFlooded, buildingID);
						}
					}
				}
			}
			else if ((data.m_flags & Building.Flags.Flooded) != Building.Flags.None)
			{
				InstanceID id2 = default(InstanceID);
				id2.Building = buildingID;
				Singleton<InstanceManager>.get_instance().SetGroup(id2, null);
				data.m_flags &= ~Building.Flags.Flooded;
			}
		}
		byte district = instance.GetDistrict(data.m_position);
		instance.m_districts.m_buffer[(int)district].AddUsageData(electricityUsage, heatingUsage, waterUsage, sewageUsage);
		data.m_problems = problem;
		return num;
	}

	protected virtual bool CanSufferFromFlood(out bool onlyCollapse)
	{
		onlyCollapse = false;
		return true;
	}

	protected virtual bool CanStockpileElectricity(ushort buildingID, ref Building data, out int stockpileAmount, out int stockpileRate)
	{
		stockpileAmount = 0;
		stockpileRate = 0;
		return false;
	}

	protected virtual bool CanStockpileWater(ushort buildingID, ref Building data, out int stockpileAmount, out int stockpileRate)
	{
		stockpileAmount = 0;
		stockpileRate = 0;
		return false;
	}

	protected void CalculateOwnVehicles(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int count, ref int cargo, ref int capacity, ref int outside)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if ((TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num].m_transferType == material)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				int num3;
				int num4;
				info.m_vehicleAI.GetSize(num, ref instance.m_vehicles.m_buffer[(int)num], out num3, out num4);
				cargo += Mathf.Min(num3, num4);
				capacity += num4;
				count++;
				if ((instance.m_vehicles.m_buffer[(int)num].m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
				{
					outside++;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void CalculateGuestVehicles(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int count, ref int cargo, ref int capacity, ref int outside)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_guestVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if ((TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num].m_transferType == material)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				int num3;
				int num4;
				info.m_vehicleAI.GetSize(num, ref instance.m_vehicles.m_buffer[(int)num], out num3, out num4);
				cargo += Mathf.Min(num3, num4);
				capacity += num4;
				count++;
				if ((instance.m_vehicles.m_buffer[(int)num].m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
				{
					outside++;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextGuestVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void HandleDead(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, int citizenCount)
	{
		Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Death);
		if (behaviour.m_deadCount != 0 && Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.DeathCare))
		{
			buildingData.m_deathProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_deathProblemTimer + 1));
			if (buildingData.m_deathProblemTimer >= 128)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Death | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_deathProblemTimer >= 64)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Death);
			}
			int num = behaviour.m_deadCount;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			this.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Dead, ref num2, ref num3, ref num4, ref num5);
			num -= num4;
			if (num > 0)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = (int)(buildingData.m_deathProblemTimer * 7 / 128);
				offer.Building = buildingID;
				offer.Position = buildingData.m_position;
				offer.Amount = 1;
				offer.Active = false;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Dead, offer);
			}
		}
		else
		{
			buildingData.m_deathProblemTimer = 0;
		}
		buildingData.m_problems = problem;
	}

	protected void HandleSick(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, int citizenCount)
	{
		Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.DirtyWater | Notification.Problem.Pollution | Notification.Problem.Noise);
		if (behaviour.m_sickCount != 0 && Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.HealthCare))
		{
			byte b;
			Singleton<NaturalResourceManager>.get_instance().CheckPollution(buildingData.m_position, out b);
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.NoisePollution, buildingData.m_position, out num);
			int num2 = (int)(buildingData.m_waterPollution * 2);
			int num3;
			int num4;
			if (this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLowEco || this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialHighEco)
			{
				num3 = (int)(b * 200 / 255);
				num4 = num * 150 / 255;
			}
			else
			{
				num3 = (int)(b * 100 / 255);
				num4 = num * 100 / 255;
			}
			int num5;
			if (num2 < 35)
			{
				num5 = num2;
			}
			else
			{
				num5 = num2 * 2 - 35;
			}
			if (num5 > 10 && num5 > num3 && num5 > num4)
			{
				buildingData.m_healthProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_healthProblemTimer + 1));
				if (buildingData.m_healthProblemTimer >= 96)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.DirtyWater | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_healthProblemTimer >= 32)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.DirtyWater);
				}
			}
			else if (num3 > 10 && num3 > num4)
			{
				buildingData.m_healthProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_healthProblemTimer + 1));
				if (buildingData.m_healthProblemTimer >= 96)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.Pollution | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_healthProblemTimer >= 32)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.Pollution);
				}
			}
			else if (num4 > 10)
			{
				buildingData.m_healthProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_healthProblemTimer + 1));
				if (buildingData.m_healthProblemTimer >= 96)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.Noise | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_healthProblemTimer >= 32)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.Noise);
				}
			}
			else
			{
				buildingData.m_healthProblemTimer = 0;
			}
		}
		else
		{
			buildingData.m_healthProblemTimer = 0;
		}
		buildingData.m_problems = problem;
	}

	protected void HandleCrime(ushort buildingID, ref Building data, int crimeAccumulation, int citizenCount)
	{
		if (crimeAccumulation != 0)
		{
			if (Singleton<SimulationManager>.get_instance().m_isNightTime)
			{
				crimeAccumulation = crimeAccumulation * 5 >> 2;
			}
			if (data.m_eventIndex != 0)
			{
				EventManager instance = Singleton<EventManager>.get_instance();
				EventInfo info = instance.m_events.m_buffer[(int)data.m_eventIndex].Info;
				crimeAccumulation = info.m_eventAI.GetCrimeAccumulation(data.m_eventIndex, ref instance.m_events.m_buffer[(int)data.m_eventIndex], crimeAccumulation);
			}
			crimeAccumulation = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)crimeAccumulation);
			if (!Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.PoliceDepartment))
			{
				crimeAccumulation = 0;
			}
		}
		data.m_crimeBuffer = (ushort)Mathf.Min(citizenCount * 100, (int)data.m_crimeBuffer + crimeAccumulation);
		int crimeBuffer = (int)data.m_crimeBuffer;
		if (citizenCount != 0 && crimeBuffer > citizenCount * 25 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u) == 0)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.Crime, ref num, ref num2, ref num3, ref num4);
			if (num == 0)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = crimeBuffer / Mathf.Max(1, citizenCount * 10);
				offer.Building = buildingID;
				offer.Position = data.m_position;
				offer.Amount = 1;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Crime, offer);
			}
		}
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Crime);
		if ((int)data.m_crimeBuffer > citizenCount * 90)
		{
			problem = Notification.AddProblems(problem, Notification.Problem.Crime | Notification.Problem.MajorProblem);
		}
		else if ((int)data.m_crimeBuffer > citizenCount * 60)
		{
			problem = Notification.AddProblems(problem, Notification.Problem.Crime);
		}
		data.m_problems = problem;
	}

	protected void RemovePeople(ushort buildingID, ref Building data, int killPercentage)
	{
		InstanceID empty = InstanceID.Empty;
		empty.Building = buildingID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(empty);
		DisasterHelpers.RemovePeople(group, buildingID, ref data.m_citizenUnits, killPercentage, ref Singleton<SimulationManager>.get_instance().m_randomizer);
	}

	protected void HandleFire(ushort buildingID, ref Building data, ref Building.Frame frameData, DistrictPolicies.Services policies)
	{
		int num;
		int num2;
		int num3;
		if (this.GetFireParameters(buildingID, ref data, out num, out num2, out num3) && (policies & DistrictPolicies.Services.SmokeDetectors) != DistrictPolicies.Services.None)
		{
			num = num * 75 / 100;
			Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 32, this.m_info.m_class);
		}
		if (num != 0 && data.m_fireIntensity == 0 && frameData.m_fireDamage == 0 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8388608u) < num && Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.FireDepartment) && !Singleton<BuildingManager>.get_instance().m_firesDisabled)
		{
			float num4 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(data.m_position));
			if (num4 <= data.m_position.y)
			{
				DisasterInfo disasterInfo = DisasterManager.FindDisasterInfo<StructureFireAI>();
				if (disasterInfo != null)
				{
					DisasterManager instance = Singleton<DisasterManager>.get_instance();
					ushort num5;
					if (instance.CreateDisaster(out num5, disasterInfo))
					{
						int num6 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u);
						num6 = 10 + num6 * num6 * num6 * num6 / 1055699;
						instance.m_disasters.m_buffer[(int)num5].m_intensity = (byte)num6;
						instance.m_disasters.m_buffer[(int)num5].m_targetPosition = data.m_position;
						disasterInfo.m_disasterAI.StartNow(num5, ref instance.m_disasters.m_buffer[(int)num5]);
						disasterInfo.m_disasterAI.ActivateNow(num5, ref instance.m_disasters.m_buffer[(int)num5]);
						InstanceID srcID = default(InstanceID);
						InstanceID dstID = default(InstanceID);
						srcID.Disaster = num5;
						dstID.Building = buildingID;
						Singleton<InstanceManager>.get_instance().CopyGroup(srcID, dstID);
						data.m_flags &= ~Building.Flags.Flooded;
						data.m_fireIntensity = (byte)num2;
						frameData.m_fireDamage = 133;
						this.BuildingDeactivated(buildingID, ref data);
						Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
						Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
						DisasterData[] expr_20D_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
						ushort expr_20D_cp_1 = num5;
						expr_20D_cp_0[(int)expr_20D_cp_1].m_buildingFireCount = expr_20D_cp_0[(int)expr_20D_cp_1].m_buildingFireCount + 1;
						if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
						{
							int num7 = 0;
							ushort subBuilding = data.m_subBuilding;
							while (subBuilding != 0)
							{
								BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].Info;
								int num8;
								int num9;
								int num10;
								if (info.m_buildingAI.GetFireParameters(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding], out num8, out num9, out num10))
								{
									dstID.Building = subBuilding;
									Singleton<InstanceManager>.get_instance().CopyGroup(srcID, dstID);
									Building[] expr_2BA_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
									ushort expr_2BA_cp_1 = subBuilding;
									expr_2BA_cp_0[(int)expr_2BA_cp_1].m_flags = (expr_2BA_cp_0[(int)expr_2BA_cp_1].m_flags & ~Building.Flags.Flooded);
									Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_fireIntensity = (byte)num2;
									Building.Frame lastFrameData = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].GetLastFrameData();
									lastFrameData.m_fireDamage = 133;
									Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].SetLastFrameData(lastFrameData);
									info.m_buildingAI.BuildingDeactivated(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding]);
									Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(subBuilding, true);
									Singleton<BuildingManager>.get_instance().UpdateBuildingColors(subBuilding);
								}
								subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
								if (++num7 > 49152)
								{
									CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
									break;
								}
							}
						}
					}
				}
			}
		}
		if (data.m_fireIntensity != 0)
		{
			int num11;
			if (num3 != 0)
			{
				num11 = ((int)data.m_fireIntensity + num3) / num3 + 3 >> 2;
			}
			else
			{
				num11 = 255;
			}
			if (num11 != 0)
			{
				num11 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1, num11);
				frameData.m_fireDamage = (byte)Mathf.Min((int)frameData.m_fireDamage + num11, 255);
				this.HandleFireSpread(buildingID, ref data, (int)frameData.m_fireDamage);
				if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
				{
					int num12 = 0;
					ushort subBuilding2 = data.m_subBuilding;
					while (subBuilding2 != 0)
					{
						BuildingInfo info2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding2].Info;
						int num13;
						int num14;
						int num15;
						if (info2.m_buildingAI.GetFireParameters(subBuilding2, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding2], out num13, out num14, out num15))
						{
							Building.Frame lastFrameData2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding2].GetLastFrameData();
							lastFrameData2.m_fireDamage = frameData.m_fireDamage;
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding2].SetLastFrameData(lastFrameData2);
						}
						subBuilding2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding2].m_subBuilding;
						if (++num12 > 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
				if (frameData.m_fireDamage == 255)
				{
					frameData.m_constructState = (byte)Mathf.Max(0, (int)frameData.m_constructState - 1088 / this.GetCollapseTime());
					data.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
					InstanceID id = default(InstanceID);
					id.Building = buildingID;
					InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
					if (group != null && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
					{
						ushort disaster = group.m_ownerInstance.Disaster;
						if (disaster != 0)
						{
							DisasterData[] expr_5D9_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
							ushort expr_5D9_cp_1 = disaster;
							expr_5D9_cp_0[(int)expr_5D9_cp_1].m_collapsedCount = expr_5D9_cp_0[(int)expr_5D9_cp_1].m_collapsedCount + 1;
						}
					}
					if (frameData.m_constructState == 0)
					{
						Singleton<InstanceManager>.get_instance().SetGroup(id, null);
					}
					data.m_levelUpProgress = 0;
					data.m_fireIntensity = 0;
					data.m_garbageBuffer = 0;
					data.m_flags |= Building.Flags.Collapsed;
					this.RemovePeople(buildingID, ref data, 90);
					this.BuildingDeactivated(buildingID, ref data);
					if (this.m_info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
					{
						Singleton<BuildingManager>.get_instance().UpdateParkingSpaces(buildingID, ref data);
					}
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						Singleton<BuildingManager>.get_instance().m_buildingOnFire.Deactivate(buildingID, false);
					}
					if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
					{
						int num16 = 0;
						ushort subBuilding3 = data.m_subBuilding;
						while (subBuilding3 != 0)
						{
							BuildingInfo info3 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].Info;
							if (frameData.m_constructState == 0)
							{
								id.Building = subBuilding3;
								Singleton<InstanceManager>.get_instance().SetGroup(id, null);
							}
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].m_levelUpProgress = 0;
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].m_fireIntensity = 0;
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].m_garbageBuffer = 0;
							Building[] expr_75D_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
							ushort expr_75D_cp_1 = subBuilding3;
							expr_75D_cp_0[(int)expr_75D_cp_1].m_flags = (expr_75D_cp_0[(int)expr_75D_cp_1].m_flags | Building.Flags.Collapsed);
							Building.Frame lastFrameData3 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].GetLastFrameData();
							lastFrameData3.m_constructState = frameData.m_constructState;
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].SetLastFrameData(lastFrameData3);
							info3.m_buildingAI.BuildingDeactivated(subBuilding3, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3]);
							if (info3.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
							{
								Singleton<BuildingManager>.get_instance().UpdateParkingSpaces(subBuilding3, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3]);
							}
							Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(subBuilding3, true);
							Singleton<BuildingManager>.get_instance().UpdateBuildingColors(subBuilding3);
							Singleton<BuildingManager>.get_instance().UpdateFlags(subBuilding3, Building.Flags.Collapsed);
							subBuilding3 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding3].m_subBuilding;
							if (++num16 > 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				else
				{
					float num17 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(data.m_position));
					if (num17 > data.m_position.y + 1f)
					{
						InstanceID id2 = default(InstanceID);
						id2.Building = buildingID;
						Singleton<InstanceManager>.get_instance().SetGroup(id2, null);
						data.m_fireIntensity = 0;
						Building.Flags flags = data.m_flags;
						if (data.m_productionRate != 0 && (data.m_flags & Building.Flags.Evacuating) == Building.Flags.None)
						{
							data.m_flags |= Building.Flags.Active;
						}
						Building.Flags flags2 = data.m_flags;
						Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, frameData.m_fireDamage == 0 || (data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None);
						Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
						if (flags2 != flags)
						{
							Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, flags2 ^ flags);
						}
						GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
						if (properties2 != null)
						{
							Singleton<BuildingManager>.get_instance().m_buildingOnFire.Deactivate(buildingID, false);
						}
						if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
						{
							int num18 = 0;
							ushort subBuilding4 = data.m_subBuilding;
							while (subBuilding4 != 0)
							{
								id2.Building = subBuilding4;
								Singleton<InstanceManager>.get_instance().SetGroup(id2, null);
								Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding4].m_fireIntensity = 0;
								Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(subBuilding4, true);
								Singleton<BuildingManager>.get_instance().UpdateBuildingColors(subBuilding4);
								subBuilding4 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding4].m_subBuilding;
								if (++num18 > 49152)
								{
									CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
									break;
								}
							}
						}
					}
					else
					{
						num2 = Mathf.Min(5000, (int)data.m_fireIntensity * data.Width * data.Length);
						int num19 = 0;
						int num20 = 0;
						int num21 = 0;
						int num22 = 0;
						this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.Fire, ref num19, ref num20, ref num21, ref num22);
						this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.Fire2, ref num19, ref num20, ref num21, ref num22);
						if (num21 * 25 < num2)
						{
							TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
							offer.Priority = Mathf.Max(8 - num19 - 1, 4);
							offer.Building = buildingID;
							offer.Position = data.m_position;
							offer.Amount = 1;
							if ((policies & DistrictPolicies.Services.HelicopterPriority) != DistrictPolicies.Services.None)
							{
								DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
								byte district = instance2.GetDistrict(data.m_position);
								District[] expr_B16_cp_0 = instance2.m_districts.m_buffer;
								byte expr_B16_cp_1 = district;
								expr_B16_cp_0[(int)expr_B16_cp_1].m_servicePoliciesEffect = (expr_B16_cp_0[(int)expr_B16_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.HelicopterPriority);
								Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Fire2, offer);
							}
							else if ((data.m_flags & Building.Flags.RoadAccessFailed) != Building.Flags.None)
							{
								Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Fire2, offer);
							}
							else
							{
								Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Fire, offer);
							}
						}
					}
				}
			}
			if (data.m_fireIntensity != 0)
			{
				if (frameData.m_fireDamage >= 192)
				{
					data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.Fire | Notification.Problem.MajorProblem);
				}
				else
				{
					data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.Fire);
				}
				Vector3 position = data.CalculateSidewalkPosition();
				PathUnit.Position position2;
				if (PathManager.FindPathPosition(position, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, false, false, 32f, out position2))
				{
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)position2.m_segment].AddTraffic(65535, 0);
				}
				float num23 = VectorUtils.LengthXZ(this.m_info.m_size) * 0.5f;
				int num24 = Mathf.Max(10, Mathf.RoundToInt((float)data.m_fireIntensity * Mathf.Min(1f, num23 / 33.75f)));
				Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Burned, num24, num24, data.m_position, num23, true);
			}
		}
		else
		{
			if (frameData.m_fireDamage != 0 && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
			{
				frameData.m_fireDamage = (byte)Mathf.Max((int)(frameData.m_fireDamage - 1), 0);
				if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
				{
					int num25 = 0;
					ushort subBuilding5 = data.m_subBuilding;
					while (subBuilding5 != 0)
					{
						Building.Frame lastFrameData4 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding5].GetLastFrameData();
						lastFrameData4.m_fireDamage = (byte)Mathf.Min((int)frameData.m_fireDamage, (int)lastFrameData4.m_fireDamage);
						Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding5].SetLastFrameData(lastFrameData4);
						subBuilding5 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding5].m_subBuilding;
						if (++num25 > 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
				if (frameData.m_fireDamage == 0)
				{
					data.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, frameData);
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
					if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
					{
						int num26 = 0;
						ushort subBuilding6 = data.m_subBuilding;
						while (subBuilding6 != 0)
						{
							Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(subBuilding6, true);
							subBuilding6 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding6].m_subBuilding;
							if (++num26 > 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.Fire);
		}
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		Building.Flags flags = data.m_flags;
		if ((flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			if (testOnly)
			{
				return true;
			}
			data.m_levelUpProgress = 0;
			data.m_fireIntensity = 0;
			data.m_garbageBuffer = 0;
			data.m_flags |= Building.Flags.Collapsed;
			if (demolish)
			{
				data.m_flags |= Building.Flags.Demolishing;
				if ((data.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
				{
					Notification.Problem problems = data.m_problems;
					data.m_problems = Notification.Problem.None;
					if (data.m_problems != problems)
					{
						Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, data.m_problems);
					}
				}
			}
			uint num = (uint)(((int)buildingID << 8) / 49152);
			uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - num;
			Building.Frame frameData = data.GetFrameData(num2);
			int num3 = Mathf.Max(0, (int)((uint)frameData.m_constructState - 1088u * (255u - (num2 & 255u)) / (uint)((uint)this.GetCollapseTime() << 8)));
			frameData.m_constructState = (byte)num3;
			frameData.m_fireDamage = (byte)Mathf.Min(Mathf.Max((int)frameData.m_fireDamage, burnAmount), 254);
			data.SetFrameData(num2, frameData);
			if ((data.m_flags & Building.Flags.Untouchable) == Building.Flags.None && group != null)
			{
				ushort disaster = group.m_ownerInstance.Disaster;
				if (disaster != 0)
				{
					DisasterData[] expr_154_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
					ushort expr_154_cp_1 = disaster;
					expr_154_cp_0[(int)expr_154_cp_1].m_collapsedCount = expr_154_cp_0[(int)expr_154_cp_1].m_collapsedCount + 1;
				}
			}
			if (num3 != 0)
			{
				InstanceID id = default(InstanceID);
				id.Building = buildingID;
				Singleton<InstanceManager>.get_instance().SetGroup(id, group);
			}
			this.RemovePeople(buildingID, ref data, 90);
			this.BuildingDeactivated(buildingID, ref data);
			if (this.m_info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
			{
				Singleton<BuildingManager>.get_instance().UpdateParkingSpaces(buildingID, ref data);
			}
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
			Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
			if ((data.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
			{
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<BuildingManager>.get_instance().m_buildingOnFire.Deactivate(buildingID, false);
				}
			}
		}
		else if (demolish && (flags & Building.Flags.Demolishing) == Building.Flags.None)
		{
			if (testOnly)
			{
				return true;
			}
			data.m_flags |= Building.Flags.Demolishing;
			if ((data.m_flags & Building.Flags.Untouchable) == Building.Flags.None)
			{
				Notification.Problem problems2 = data.m_problems;
				data.m_problems = Notification.Problem.None;
				if (data.m_problems != problems2)
				{
					Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems2, data.m_problems);
				}
			}
		}
		Building.Flags flags2 = data.m_flags;
		if (flags2 != flags)
		{
			Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, flags2 ^ flags);
			if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
			{
				int num4 = 0;
				ushort subBuilding = data.m_subBuilding;
				while (subBuilding != 0)
				{
					BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].Info;
					info.m_buildingAI.CollapseBuilding(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding], group, testOnly, demolish, burnAmount);
					subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
					if (++num4 > 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			return true;
		}
		return false;
	}

	public override bool BurnBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num;
		int num2;
		int num3;
		if (this.GetFireParameters(buildingID, ref data, out num, out num2, out num3) && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			float num4 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(data.m_position));
			if (num4 <= data.m_position.y)
			{
				if (!testOnly)
				{
					InstanceID id = default(InstanceID);
					id.Building = buildingID;
					Singleton<InstanceManager>.get_instance().SetGroup(id, group);
					if (data.m_fireIntensity == 0 && group != null)
					{
						ushort disaster = group.m_ownerInstance.Disaster;
						if (disaster != 0)
						{
							DisasterData[] expr_B2_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
							ushort expr_B2_cp_1 = disaster;
							expr_B2_cp_0[(int)expr_B2_cp_1].m_buildingFireCount = expr_B2_cp_0[(int)expr_B2_cp_1].m_buildingFireCount + 1;
						}
					}
					Building.Flags flags = data.m_flags;
					data.m_flags &= ~Building.Flags.Flooded;
					data.m_fireIntensity = (byte)Mathf.Max((int)data.m_fireIntensity, num2);
					Building.Frame lastFrameData = data.GetLastFrameData();
					lastFrameData.m_fireDamage = (byte)Mathf.Max((int)lastFrameData.m_fireDamage, 133);
					data.SetLastFrameData(lastFrameData);
					this.BuildingDeactivated(buildingID, ref data);
					Building.Flags flags2 = data.m_flags;
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
					if (flags2 != flags)
					{
						instance.UpdateFlags(buildingID, flags2 ^ flags);
					}
					if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
					{
						int num5 = 0;
						ushort subBuilding = data.m_subBuilding;
						while (subBuilding != 0)
						{
							BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].Info;
							info.m_buildingAI.BurnBuilding(subBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding], group, testOnly);
							subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
							if (++num5 > 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	public override bool NearObjectInFire(ushort buildingID, ref Building data, InstanceID itemID, Vector3 itemPos)
	{
		if (itemID.Type != InstanceType.Tree)
		{
			return false;
		}
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		this.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.ForestFire, ref num, ref num2, ref num3, ref num4);
		if (num == 0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = Mathf.Max(8 - num - 1, 4);
			offer.Building = buildingID;
			offer.Position = data.m_position;
			offer.Amount = 1;
			Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ForestFire, offer);
		}
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(itemID);
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterManager instance = Singleton<DisasterManager>.get_instance();
				if ((instance.m_disasters.m_buffer[(int)disaster].m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
				{
					instance.DetectDisaster(disaster, this.m_info.m_class.m_service == ItemClass.Service.FireDepartment);
					instance.FollowDisaster(disaster);
				}
			}
		}
		return true;
	}

	public override float MaxFireDetectDistance(ushort buildingID, ref Building data)
	{
		return 100f;
	}

	private void HandleFireSpread(ushort buildingID, ref Building buildingData, int fireDamage)
	{
		int width = buildingData.Width;
		int length = buildingData.Length;
		Vector2 vector = VectorUtils.XZ(buildingData.m_position);
		Vector2 vector2;
		vector2..ctor(Mathf.Cos(buildingData.m_angle), Mathf.Sin(buildingData.m_angle));
		Vector2 vector3;
		vector3..ctor(vector2.y, -vector2.x);
		float num = (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8, 32);
		Quad2 quad;
		quad.a = vector - ((float)width * 4f + num) * vector2 - ((float)length * 4f + num) * vector3;
		quad.b = vector + ((float)width * 4f + num) * vector2 - ((float)length * 4f + num) * vector3;
		quad.c = vector + ((float)width * 4f + num) * vector2 + ((float)length * 4f + num) * vector3;
		quad.d = vector - ((float)width * 4f + num) * vector2 + ((float)length * 4f + num) * vector3;
		Vector2 vector4 = quad.Min();
		Vector2 vector5 = quad.Max();
		float minY = buildingData.m_position.y - (float)buildingData.m_baseHeight;
		float maxY = buildingData.m_position.y + this.m_info.m_size.y;
		float num2 = (float)((int)buildingData.m_fireIntensity * (64 - Mathf.Abs(fireDamage - 192)));
		InstanceID id = default(InstanceID);
		id.Building = buildingID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterManager instance = Singleton<DisasterManager>.get_instance();
				DisasterInfo info = instance.m_disasters.m_buffer[(int)disaster].Info;
				int fireSpreadProbability = info.m_disasterAI.GetFireSpreadProbability(disaster, ref instance.m_disasters.m_buffer[(int)disaster]);
				num2 *= (float)fireSpreadProbability * 0.01f;
			}
		}
		int num3 = Mathf.Max((int)((vector4.x - 72f) / 64f + 135f), 0);
		int num4 = Mathf.Max((int)((vector4.y - 72f) / 64f + 135f), 0);
		int num5 = Mathf.Min((int)((vector5.x + 72f) / 64f + 135f), 269);
		int num6 = Mathf.Min((int)((vector5.y + 72f) / 64f + 135f), 269);
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		for (int i = num4; i <= num6; i++)
		{
			for (int j = num3; j <= num5; j++)
			{
				ushort num7 = instance2.m_buildingGrid[i * 270 + j];
				int num8 = 0;
				while (num7 != 0)
				{
					if (num7 != buildingID && (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(262144u) * num < num2)
					{
						CommonBuildingAI.TrySpreadFire(quad, minY, maxY, num7, ref instance2.m_buildings.m_buffer[(int)num7], group);
					}
					num7 = instance2.m_buildings.m_buffer[(int)num7].m_nextGridBuilding;
					if (++num8 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		Vector3 vector6 = VectorUtils.X_Y(vector2);
		Vector3 vector7 = VectorUtils.X_Y(vector3);
		int num9 = Mathf.Max((int)((vector4.x - 32f) / 32f + 270f), 0);
		int num10 = Mathf.Max((int)((vector4.y - 32f) / 32f + 270f), 0);
		int num11 = Mathf.Min((int)((vector5.x + 32f) / 32f + 270f), 539);
		int num12 = Mathf.Min((int)((vector5.y + 32f) / 32f + 270f), 539);
		TreeManager instance3 = Singleton<TreeManager>.get_instance();
		for (int k = num10; k <= num12; k++)
		{
			for (int l = num9; l <= num11; l++)
			{
				uint num13 = instance3.m_treeGrid[k * 540 + l];
				int num14 = 0;
				while (num13 != 0u)
				{
					Vector3 position = instance3.m_trees.m_buffer[(int)((UIntPtr)num13)].Position;
					Vector3 vector8 = position - buildingData.m_position;
					vector8 -= Mathf.Clamp(Vector3.Dot(vector8, vector6), (float)(-(float)width) * 4f, (float)width * 4f) * vector6;
					float magnitude = (vector8 - Mathf.Clamp(Vector3.Dot(vector8, vector7), (float)(-(float)length) * 4f, (float)length * 4f) * vector7).get_magnitude();
					if (magnitude < 32f && (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(131072u) * magnitude < num2)
					{
						instance3.BurnTree(num13, group, (int)buildingData.m_fireIntensity);
					}
					num13 = instance3.m_trees.m_buffer[(int)((UIntPtr)num13)].m_nextGridTree;
					if (++num14 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private static void TrySpreadFire(Quad2 quad, float minY, float maxY, ushort buildingID, ref Building buildingData, InstanceManager.Group group)
	{
		BuildingInfo info = buildingData.Info;
		int num;
		int num2;
		int num3;
		if (info.m_buildingAI.GetFireParameters(buildingID, ref buildingData, out num, out num2, out num3) && (buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Abandoned)) == Building.Flags.Completed && buildingData.m_fireIntensity == 0 && buildingData.GetLastFrameData().m_fireDamage == 0 && buildingData.OverlapQuad(buildingID, quad, minY, maxY, ItemClass.CollisionType.Undefined))
		{
			if ((buildingData.m_flags & Building.Flags.Untouchable) != Building.Flags.None)
			{
				ushort num4 = Building.FindParentBuilding(buildingID);
				if (num4 != 0)
				{
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					info = instance.m_buildings.m_buffer[(int)num4].Info;
					if (info.m_buildingAI.GetFireParameters(buildingID, ref instance.m_buildings.m_buffer[(int)num4], out num, out num2, out num3) && (instance.m_buildings.m_buffer[(int)num4].m_flags & (Building.Flags.Completed | Building.Flags.Abandoned)) == Building.Flags.Completed && instance.m_buildings.m_buffer[(int)num4].m_fireIntensity == 0 && instance.m_buildings.m_buffer[(int)num4].GetLastFrameData().m_fireDamage == 0)
					{
						info.m_buildingAI.BurnBuilding(num4, ref instance.m_buildings.m_buffer[(int)num4], group, false);
					}
				}
			}
			else
			{
				info.m_buildingAI.BurnBuilding(buildingID, ref buildingData, group, false);
			}
		}
	}

	protected void GetHomeBehaviour(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount, ref int homeCount, ref int aliveHomeCount, ref int emptyHomeCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Home) != 0)
			{
				int num3 = 0;
				int num4 = 0;
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenHomeBehaviour(ref behaviour, ref num3, ref num4);
				if (num3 != 0)
				{
					aliveHomeCount++;
					aliveCount += num3;
				}
				if (num4 != 0)
				{
					totalCount += num4;
				}
				else
				{
					emptyHomeCount++;
				}
				homeCount++;
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void GetWorkBehaviour(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Work) != 0)
			{
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void GetStudentBehaviour(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Student) != 0)
			{
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void GetVisitBehaviour(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	protected void HandleWorkPlaces(ushort buildingID, ref Building data, int workPlaces0, int workPlaces1, int workPlaces2, int workPlaces3, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount)
	{
		int num = workPlaces0 + workPlaces1 + workPlaces2 + workPlaces3;
		if (totalWorkerCount < num && data.m_citizenUnits != 0u)
		{
			int num2 = behaviour.m_educated0Count;
			int num3 = behaviour.m_educated1Count;
			int num4 = behaviour.m_educated2Count;
			int educated3Count = behaviour.m_educated3Count;
			int num5 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u);
			if (num5 == 0)
			{
				int num6 = num - totalWorkerCount;
				int num7 = (workPlaces3 * 300 + workPlaces2 * 200 + workPlaces1 * 100) / (num + 1);
				int num8 = (educated3Count * 300 + num4 * 200 + num3 * 100) / (aliveWorkerCount + 1);
				if (educated3Count < workPlaces3 && num6 > 0)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = Mathf.Max(1, (workPlaces3 - educated3Count) * 8 / workPlaces3);
					offer.Building = buildingID;
					offer.Position = data.m_position;
					int num9 = Mathf.Min(num6, workPlaces3 - educated3Count);
					offer.Amount = num9;
					num6 -= num9;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker3, offer);
				}
				else if (educated3Count > workPlaces3)
				{
					num4 += educated3Count - workPlaces3;
				}
				if (num4 < workPlaces2 && num6 > 0)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					offer2.Priority = Mathf.Max(1, (workPlaces2 - num4) * 8 / workPlaces2);
					offer2.Building = buildingID;
					offer2.Position = data.m_position;
					int num10 = Mathf.Min(num6, workPlaces2 - num4);
					offer2.Amount = num10;
					num6 -= num10;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker2, offer2);
				}
				else if (num4 > workPlaces2)
				{
					num3 += num4 - workPlaces2;
				}
				if (num3 < workPlaces1 && num6 > 0)
				{
					TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
					offer3.Priority = Mathf.Max(1, (workPlaces1 - num3) * 8 / workPlaces1);
					offer3.Building = buildingID;
					offer3.Position = data.m_position;
					int num11 = Mathf.Min(num6, workPlaces1 - num3);
					offer3.Amount = num11;
					num6 -= num11;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker1, offer3);
				}
				else if (num3 > workPlaces1)
				{
					num2 += num3 - workPlaces1;
				}
				if (num2 < workPlaces0 && num6 > 0)
				{
					TransferManager.TransferOffer offer4 = default(TransferManager.TransferOffer);
					offer4.Priority = Mathf.Max(1, (workPlaces0 - num2) * 8 / workPlaces0);
					offer4.Building = buildingID;
					offer4.Position = data.m_position;
					int num12 = Mathf.Min(num6, workPlaces0 - num2);
					offer4.Amount = num12;
					num6 -= num12;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker0, offer4);
				}
				if (num8 < num7 - 100 && Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Service.Education))
				{
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						int publicServiceIndex = ItemClass.GetPublicServiceIndex(ItemClass.Service.Education);
						Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Activate(properties.m_serviceNeeded, ItemClass.Service.Education);
					}
				}
			}
			else if (num5 == 1)
			{
				int num13 = 0;
				if (num2 >= workPlaces0)
				{
					num13++;
					if (num3 >= workPlaces1)
					{
						num13++;
						if (num4 >= workPlaces2)
						{
							num13++;
						}
					}
				}
				int num14 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num13, 3);
				TransferManager.TransferOffer offer5 = default(TransferManager.TransferOffer);
				offer5.Priority = 0;
				offer5.Building = buildingID;
				offer5.Position = data.m_position;
				offer5.Amount = 1;
				switch (num14)
				{
				case 0:
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker0, offer5);
					break;
				case 1:
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker1, offer5);
					break;
				case 2:
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker2, offer5);
					break;
				case 3:
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Worker3, offer5);
					break;
				}
			}
		}
	}

	protected static void ExportResource(ushort buildingID, ref Building data, TransferManager.TransferReason resource, int amount)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(data.m_position);
		switch (resource)
		{
		case TransferManager.TransferReason.Oil:
			goto IL_81;
		case TransferManager.TransferReason.Ore:
		case TransferManager.TransferReason.Coal:
		{
			District[] expr_6F_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
			byte expr_6F_cp_0_cp_1 = district;
			expr_6F_cp_0_cp_0[(int)expr_6F_cp_0_cp_1].m_exportData.m_tempOre = expr_6F_cp_0_cp_0[(int)expr_6F_cp_0_cp_1].m_exportData.m_tempOre + (uint)amount;
			return;
		}
		case TransferManager.TransferReason.Logs:
			goto IL_AD;
		case TransferManager.TransferReason.Grain:
			goto IL_D9;
		case TransferManager.TransferReason.Goods:
		{
			District[] expr_11F_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
			byte expr_11F_cp_0_cp_1 = district;
			expr_11F_cp_0_cp_0[(int)expr_11F_cp_0_cp_1].m_exportData.m_tempGoods = expr_11F_cp_0_cp_0[(int)expr_11F_cp_0_cp_1].m_exportData.m_tempGoods + (uint)amount;
			return;
		}
		case TransferManager.TransferReason.PassengerTrain:
			IL_38:
			if (resource == TransferManager.TransferReason.Petrol)
			{
				goto IL_81;
			}
			if (resource == TransferManager.TransferReason.Food)
			{
				goto IL_D9;
			}
			if (resource != TransferManager.TransferReason.Lumber)
			{
				return;
			}
			goto IL_AD;
		}
		goto IL_38;
		IL_81:
		District[] expr_9B_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_9B_cp_0_cp_1 = district;
		expr_9B_cp_0_cp_0[(int)expr_9B_cp_0_cp_1].m_exportData.m_tempOil = expr_9B_cp_0_cp_0[(int)expr_9B_cp_0_cp_1].m_exportData.m_tempOil + (uint)amount;
		return;
		IL_AD:
		District[] expr_C7_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_C7_cp_0_cp_1 = district;
		expr_C7_cp_0_cp_0[(int)expr_C7_cp_0_cp_1].m_exportData.m_tempForestry = expr_C7_cp_0_cp_0[(int)expr_C7_cp_0_cp_1].m_exportData.m_tempForestry + (uint)amount;
		return;
		IL_D9:
		District[] expr_F3_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
		byte expr_F3_cp_0_cp_1 = district;
		expr_F3_cp_0_cp_0[(int)expr_F3_cp_0_cp_1].m_exportData.m_tempAgricultural = expr_F3_cp_0_cp_0[(int)expr_F3_cp_0_cp_1].m_exportData.m_tempAgricultural + (uint)amount;
	}

	public override bool CalculateGroupData(ushort buildingID, ref Building buildingData, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Collapsed)) != Building.Flags.Completed)
		{
			bool result = false;
			if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				if (this.m_info.m_prefabDataLayer == layer)
				{
					result = true;
					if (this.m_info.m_buildingAI.CanUseGroupMesh(buildingID, ref buildingData))
					{
						BuildingInfoBase collapsedInfo = this.m_info.m_collapsedInfo;
						if (collapsedInfo != null && collapsedInfo.m_lodMeshData != null)
						{
							vertexCount += collapsedInfo.m_lodMeshData.m_vertices.Length;
							triangleCount += collapsedInfo.m_lodMeshData.m_triangles.Length;
							objectCount++;
							vertexArrays |= (collapsedInfo.m_lodMeshData.VertexArrayMask() | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs2);
							if (collapsedInfo.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
							{
								vertexCount += collapsedInfo.m_lodMeshBase.m_vertices.Length;
								triangleCount += collapsedInfo.m_lodMeshBase.m_triangles.Length;
							}
						}
					}
				}
			}
			else
			{
				if (this.m_info.m_prefabDataLayer == layer)
				{
					result = true;
				}
				if (this.CalculatePropGroupData(buildingID, ref buildingData, true, false, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
				{
					result = true;
				}
			}
			return result;
		}
		return base.CalculateGroupData(buildingID, ref buildingData, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
	}

	public override void PopulateGroupData(ushort buildingID, ref Building buildingData, ref float height, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Collapsed)) != Building.Flags.Completed)
		{
			if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				BuildingInfoBase collapsedInfo = this.m_info.m_collapsedInfo;
				if (collapsedInfo != null)
				{
					height = collapsedInfo.m_generatedInfo.m_size.y;
					if (this.m_info.m_prefabDataLayer == layer)
					{
						int width = buildingData.Width;
						int length = buildingData.Length;
						Vector3 vector = new Vector3(Mathf.Cos(buildingData.m_angle), 0f, Mathf.Sin(buildingData.m_angle)) * 8f;
						Vector3 vector2;
						vector2..ctor(vector.z, 0f, -vector.x);
						Vector3 vector3 = buildingData.m_position - (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
						Vector3 vector4 = buildingData.m_position + (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
						Vector3 vector5 = buildingData.m_position + (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
						Vector3 vector6 = buildingData.m_position - (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
						float num = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x));
						float num2 = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z));
						float num3 = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x));
						float num4 = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z));
						min = Vector3.Min(min, new Vector3(num, buildingData.m_position.y - (float)buildingData.m_baseHeight, num2));
						max = Vector3.Max(max, new Vector3(num3, buildingData.m_position.y + this.m_info.m_size.y, num4));
						maxRenderDistance = Mathf.Max(maxRenderDistance, 20000f);
						maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
						if (this.m_info.m_buildingAI.CanUseGroupMesh(buildingID, ref buildingData))
						{
							Randomizer randomizer;
							randomizer..ctor((int)buildingID);
							int num5 = randomizer.Int32(4u);
							if ((1 << num5 & this.m_info.m_collapsedRotations) == 0)
							{
								num5 = (num5 + 1 & 3);
							}
							Vector3 vector7 = this.m_info.m_generatedInfo.m_min;
							Vector3 vector8 = this.m_info.m_generatedInfo.m_max;
							float num6 = (float)width * 4f;
							float num7 = (float)length * 4f;
							float num8 = Building.CalculateLocalMeshOffset(this.m_info, length);
							vector7 = Vector3.Max(vector7 - new Vector3(4f, 0f, 4f + num8), new Vector3(-num6, 0f, -num7));
							vector8 = Vector3.Min(vector8 + new Vector3(4f, 0f, 4f - num8), new Vector3(num6, 0f, num7));
							Vector3 vector9 = (vector7 + vector8) * 0.5f;
							Vector3 vector10 = vector8 - vector7;
							float num9 = (((num5 & 1) != 0) ? vector10.z : vector10.x) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
							float num10 = (((num5 & 1) != 0) ? vector10.x : vector10.z) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
							Quaternion quaternion = Quaternion.AngleAxis((float)num5 * 90f, Vector3.get_down());
							Matrix4x4 matrix4x = Matrix4x4.TRS(new Vector3(vector9.x, 0f, vector9.z + num8), quaternion, new Vector3(num9, 1f, num10));
							if (collapsedInfo.m_lodMeshData != null)
							{
								Vector3 vector11;
								Quaternion quaternion2;
								buildingData.CalculateMeshPosition(out vector11, out quaternion2);
								Matrix4x4 matrix4x2 = Matrix4x4.TRS(vector11 - groupPosition, quaternion2, Vector3.get_one()) * matrix4x;
								Vector4 colorLocation = RenderManager.GetColorLocation((uint)buildingID);
								Vector2 vector12;
								vector12..ctor(colorLocation.x, colorLocation.y);
								int[] triangles = collapsedInfo.m_lodMeshData.m_triangles;
								int num11 = triangles.Length;
								for (int i = 0; i < num11; i++)
								{
									data.m_triangles[triangleIndex++] = triangles[i] + vertexIndex;
								}
								RenderGroup.VertexArrays vertexArrays = collapsedInfo.m_lodMeshData.VertexArrayMask();
								Vector3[] vertices = collapsedInfo.m_lodMeshData.m_vertices;
								Vector3[] normals = collapsedInfo.m_lodMeshData.m_normals;
								Vector4[] tangents = collapsedInfo.m_lodMeshData.m_tangents;
								Vector2[] uvs = collapsedInfo.m_lodMeshData.m_uvs;
								Vector2[] uvs2 = collapsedInfo.m_lodMeshData.m_uvs3;
								Vector2[] uvs3 = collapsedInfo.m_lodMeshData.m_uvs4;
								Color32[] colors = collapsedInfo.m_lodMeshData.m_colors;
								int num12 = vertices.Length;
								for (int j = 0; j < num12; j++)
								{
									Color32 groupVertexColor = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, collapsedInfo, j, false);
									Vector3 vector13 = vertices[j];
									Vector3 vector14 = matrix4x2.MultiplyPoint(vector13);
									if (collapsedInfo.m_requireHeightMap)
									{
										vector14.y = vector13.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector14 + groupPosition);
									}
									data.m_vertices[vertexIndex] = vector14;
									if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
									{
										data.m_normals[vertexIndex] = matrix4x2.MultiplyVector(normals[j]);
									}
									else
									{
										data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
									}
									if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
									{
										Vector4 vector15 = tangents[j];
										Vector3 vector16 = matrix4x2.MultiplyVector(vector15);
										vector15.x = vector16.x;
										vector15.y = vector16.y;
										vector15.z = vector16.z;
										data.m_tangents[vertexIndex] = vector15;
									}
									else
									{
										data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
									}
									if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
									{
										data.m_uvs[vertexIndex] = uvs[j];
									}
									else
									{
										data.m_uvs[vertexIndex] = Vector2.get_zero();
									}
									data.m_colors[vertexIndex] = groupVertexColor;
									data.m_uvs2[vertexIndex] = vector12;
									Color32 color = colors[j];
									Vector2 vector17 = uvs3[j];
									if (color.r == 255 && color.g == 255 && color.b <= 5)
									{
										vector17.x = -vector17.x;
									}
									data.m_uvs3[vertexIndex] = uvs2[j];
									data.m_uvs4[vertexIndex] = vector17;
									vertexIndex++;
								}
								if (collapsedInfo.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
								{
									triangles = collapsedInfo.m_lodMeshBase.m_triangles;
									num11 = triangles.Length;
									for (int k = 0; k < num11; k++)
									{
										data.m_triangles[triangleIndex++] = triangles[k] + vertexIndex;
									}
									vertexArrays = collapsedInfo.m_lodMeshBase.VertexArrayMask();
									vertices = collapsedInfo.m_lodMeshBase.m_vertices;
									normals = collapsedInfo.m_lodMeshBase.m_normals;
									tangents = collapsedInfo.m_lodMeshBase.m_tangents;
									uvs = collapsedInfo.m_lodMeshBase.m_uvs;
									uvs2 = collapsedInfo.m_lodMeshBase.m_uvs3;
									uvs3 = collapsedInfo.m_lodMeshBase.m_uvs4;
									colors = collapsedInfo.m_lodMeshBase.m_colors;
									num12 = vertices.Length;
									for (int l = 0; l < num12; l++)
									{
										Color32 groupVertexColor2 = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, collapsedInfo, l, true);
										Vector3 vector18 = vertices[l];
										vector18.y *= (float)buildingData.m_baseHeight;
										Vector3 vector19 = matrix4x2.MultiplyPoint(vector18);
										if (collapsedInfo.m_requireHeightMap)
										{
											vector19.y = vector18.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector19 + groupPosition);
										}
										data.m_vertices[vertexIndex] = vector19;
										if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
										{
											data.m_normals[vertexIndex] = matrix4x2.MultiplyVector(normals[l]);
										}
										else
										{
											data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
										}
										if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
										{
											Vector4 vector20 = tangents[l];
											Vector3 vector21 = matrix4x2.MultiplyVector(vector20);
											vector20.x = vector21.x;
											vector20.y = vector21.y;
											vector20.z = vector21.z;
											data.m_tangents[vertexIndex] = vector20;
										}
										else
										{
											data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
										}
										if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
										{
											Vector2 vector22 = uvs[l];
											vector22.y *= (float)buildingData.m_baseHeight;
											data.m_uvs[vertexIndex] = vector22;
										}
										else
										{
											data.m_uvs[vertexIndex] = Vector2.get_zero();
										}
										data.m_colors[vertexIndex] = groupVertexColor2;
										data.m_uvs2[vertexIndex] = vector12;
										Color32 color2 = colors[l];
										Vector2 vector23 = uvs3[l];
										if (color2.r == 255 && color2.g == 255 && color2.b <= 5)
										{
											vector23.x = -vector23.x;
										}
										data.m_uvs3[vertexIndex] = uvs2[l];
										data.m_uvs4[vertexIndex] = vector23;
										vertexIndex++;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				if (this.m_info.m_prefabDataLayer == layer)
				{
					int width2 = buildingData.Width;
					int length2 = buildingData.Length;
					Vector3 vector24 = new Vector3(Mathf.Cos(buildingData.m_angle), 0f, Mathf.Sin(buildingData.m_angle)) * 8f;
					Vector3 vector25;
					vector25..ctor(vector24.z, 0f, -vector24.x);
					Vector3 vector26 = buildingData.m_position - (float)width2 * 0.5f * vector24 - (float)length2 * 0.5f * vector25;
					Vector3 vector27 = buildingData.m_position + (float)width2 * 0.5f * vector24 - (float)length2 * 0.5f * vector25;
					Vector3 vector28 = buildingData.m_position + (float)width2 * 0.5f * vector24 + (float)length2 * 0.5f * vector25;
					Vector3 vector29 = buildingData.m_position - (float)width2 * 0.5f * vector24 + (float)length2 * 0.5f * vector25;
					float num13 = Mathf.Min(Mathf.Min(vector26.x, vector27.x), Mathf.Min(vector28.x, vector29.x));
					float num14 = Mathf.Min(Mathf.Min(vector26.z, vector27.z), Mathf.Min(vector28.z, vector29.z));
					float num15 = Mathf.Max(Mathf.Max(vector26.x, vector27.x), Mathf.Max(vector28.x, vector29.x));
					float num16 = Mathf.Max(Mathf.Max(vector26.z, vector27.z), Mathf.Max(vector28.z, vector29.z));
					min = Vector3.Min(min, new Vector3(num13, buildingData.m_position.y - (float)buildingData.m_baseHeight, num14));
					max = Vector3.Max(max, new Vector3(num15, buildingData.m_position.y + this.m_info.m_size.y, num16));
					maxRenderDistance = Mathf.Max(maxRenderDistance, 20000f);
					maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
				}
				this.PopulatePropGroupData(buildingID, ref buildingData, true, false, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
			}
		}
		else
		{
			base.PopulateGroupData(buildingID, ref buildingData, ref height, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
	}

	public override void GetTerrainLimits(ushort buildingID, ref Building data, Vector3 meshPosition, out float min, out float max)
	{
		min = meshPosition.y - 255f;
		max = 1000000f;
		if (this.m_info.m_requireHeightMap && (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Roadside || this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnGround))
		{
			min = meshPosition.y - this.m_info.m_maxHeightOffset;
			max = meshPosition.y + this.m_info.m_maxHeightOffset;
		}
		if ((data.m_flags & Building.Flags.FixedHeight) != Building.Flags.None)
		{
			max = meshPosition.y;
		}
	}

	public override void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		negX = false;
		posX = false;
		negZ = false;
		posZ = true;
	}

	public bool m_ignoreNoPropsWarning;
}
