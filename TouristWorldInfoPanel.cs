﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public sealed class TouristWorldInfoPanel : HumanWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_AgeWealth = base.Find<UILabel>("AgeWealth");
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (this.m_InstanceID.Type == InstanceType.Citizen && this.m_InstanceID.Citizen != 0u)
			{
				uint citizen = this.m_InstanceID.Citizen;
				this.m_AgeWealth.set_text(HumanWorldInfoPanel.GetAgeAndWealth(instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].WealthLevel, Citizen.GetAgeGroup(instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Age)));
			}
		}
	}

	private UILabel m_AgeWealth;
}
