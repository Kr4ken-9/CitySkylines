﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using PopsApi;
using UnityEngine;

public class PopsManager : Singleton<PopsManager>
{
	public event PopsManager.OnLoginChangedHandler EventLoginChanged
	{
		add
		{
			PopsManager.OnLoginChangedHandler onLoginChangedHandler = this.EventLoginChanged;
			PopsManager.OnLoginChangedHandler onLoginChangedHandler2;
			do
			{
				onLoginChangedHandler2 = onLoginChangedHandler;
				onLoginChangedHandler = Interlocked.CompareExchange<PopsManager.OnLoginChangedHandler>(ref this.EventLoginChanged, (PopsManager.OnLoginChangedHandler)Delegate.Combine(onLoginChangedHandler2, value), onLoginChangedHandler);
			}
			while (onLoginChangedHandler != onLoginChangedHandler2);
		}
		remove
		{
			PopsManager.OnLoginChangedHandler onLoginChangedHandler = this.EventLoginChanged;
			PopsManager.OnLoginChangedHandler onLoginChangedHandler2;
			do
			{
				onLoginChangedHandler2 = onLoginChangedHandler;
				onLoginChangedHandler = Interlocked.CompareExchange<PopsManager.OnLoginChangedHandler>(ref this.EventLoginChanged, (PopsManager.OnLoginChangedHandler)Delegate.Remove(onLoginChangedHandler2, value), onLoginChangedHandler);
			}
			while (onLoginChangedHandler != onLoginChangedHandler2);
		}
	}

	public bool LoggedIn
	{
		get
		{
			return this.m_CachedLoggedIn;
		}
	}

	private void InitializePopsAccount()
	{
		this.m_AccountEnabled = true;
	}

	private void DisableAccount()
	{
		this.m_AccountEnabled = false;
	}

	public void PollAccountIsOnline(Action<PopsManager.AccountActionResult> callback)
	{
		if (!this.m_AccountEnabled)
		{
			if (callback != null)
			{
				callback(new PopsManager.AccountActionResult(false, "Paradox account services currently unavailable"));
			}
			return;
		}
		base.StartCoroutine(this.PollAccountIsOnlineCoroutine(callback));
	}

	public void CreateAccount(PopsManager.AccountCreateData data, Action<PopsManager.AccountActionResult> callback)
	{
		if (!this.m_AccountEnabled)
		{
			if (callback != null)
			{
				callback(new PopsManager.AccountActionResult(false, "Paradox account services currently unavailable"));
			}
			return;
		}
		base.StartCoroutine(this.CreateAccountCoroutine(data, callback));
	}

	public void LogInWithAuthToken(string token, Action<PopsManager.AccountActionResult> callback)
	{
		if (!this.m_AccountEnabled)
		{
			if (callback != null)
			{
				callback(new PopsManager.AccountActionResult(false, "Paradox account services currently unavailable"));
			}
			return;
		}
		base.StartCoroutine(this.LoginWithAuthTokenCoroutine(token, callback));
	}

	public void Login(string username, string password, bool subscribeToNewsletter, Action<PopsManager.AccountActionResult> callback)
	{
		if (!this.m_AccountEnabled)
		{
			if (callback != null)
			{
				callback(new PopsManager.AccountActionResult(false, "Paradox account services currently unavailable"));
			}
			return;
		}
		base.StartCoroutine(this.LoginCoroutine(username, password, subscribeToNewsletter, callback));
	}

	public void NewsletterSubscribe(Action<PopsManager.AccountActionResult> callback)
	{
		if (!this.m_AccountEnabled)
		{
			if (callback != null)
			{
				callback(new PopsManager.AccountActionResult(false, "Paradox account services currently unavailable"));
			}
			return;
		}
		base.StartCoroutine(this.NewsletterSubscribeCoroutine(callback));
	}

	public void Logout()
	{
		if (!this.m_AccountEnabled)
		{
			return;
		}
		this.m_CachedLoggedIn = false;
		this.m_PopsApiWrapper.AuthTokenRemove();
		SettingsFile settingsFile = GameSettings.FindSettingsFileByName(Settings.gameSettingsFile);
		if (settingsFile != null)
		{
			settingsFile.DeleteEntry(Settings.pdx_acc_nm);
			settingsFile.DeleteEntry(Settings.pdx_acc_ps);
		}
		if (this.EventLoginChanged != null)
		{
			this.EventLoginChanged();
		}
	}

	public string AuthTokenRetrieve()
	{
		if (!this.m_AccountEnabled)
		{
			return string.Empty;
		}
		string text = null;
		try
		{
			text = this.m_PopsApiWrapper.AuthTokenRetrieve();
		}
		catch
		{
			text = null;
		}
		if (text == null)
		{
			this.m_CachedLoggedIn = false;
		}
		return text;
	}

	[DebuggerHidden]
	private IEnumerator NewsletterSubscribeCoroutine(Action<PopsManager.AccountActionResult> callback)
	{
		PopsManager.<NewsletterSubscribeCoroutine>c__Iterator0 <NewsletterSubscribeCoroutine>c__Iterator = new PopsManager.<NewsletterSubscribeCoroutine>c__Iterator0();
		<NewsletterSubscribeCoroutine>c__Iterator.callback = callback;
		<NewsletterSubscribeCoroutine>c__Iterator.$this = this;
		return <NewsletterSubscribeCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator PollAccountIsOnlineCoroutine(Action<PopsManager.AccountActionResult> callback)
	{
		PopsManager.<PollAccountIsOnlineCoroutine>c__Iterator1 <PollAccountIsOnlineCoroutine>c__Iterator = new PopsManager.<PollAccountIsOnlineCoroutine>c__Iterator1();
		<PollAccountIsOnlineCoroutine>c__Iterator.callback = callback;
		<PollAccountIsOnlineCoroutine>c__Iterator.$this = this;
		return <PollAccountIsOnlineCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator CreateAccountCoroutine(PopsManager.AccountCreateData data, Action<PopsManager.AccountActionResult> callback)
	{
		PopsManager.<CreateAccountCoroutine>c__Iterator2 <CreateAccountCoroutine>c__Iterator = new PopsManager.<CreateAccountCoroutine>c__Iterator2();
		<CreateAccountCoroutine>c__Iterator.data = data;
		<CreateAccountCoroutine>c__Iterator.callback = callback;
		<CreateAccountCoroutine>c__Iterator.$this = this;
		return <CreateAccountCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoginWithAuthTokenCoroutine(string token, Action<PopsManager.AccountActionResult> callback)
	{
		PopsManager.<LoginWithAuthTokenCoroutine>c__Iterator3 <LoginWithAuthTokenCoroutine>c__Iterator = new PopsManager.<LoginWithAuthTokenCoroutine>c__Iterator3();
		<LoginWithAuthTokenCoroutine>c__Iterator.token = token;
		<LoginWithAuthTokenCoroutine>c__Iterator.callback = callback;
		<LoginWithAuthTokenCoroutine>c__Iterator.$this = this;
		return <LoginWithAuthTokenCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoginCoroutine(string username, string password, bool subscribeToNewsletter, Action<PopsManager.AccountActionResult> callback)
	{
		PopsManager.<LoginCoroutine>c__Iterator4 <LoginCoroutine>c__Iterator = new PopsManager.<LoginCoroutine>c__Iterator4();
		<LoginCoroutine>c__Iterator.username = username;
		<LoginCoroutine>c__Iterator.password = password;
		<LoginCoroutine>c__Iterator.callback = callback;
		<LoginCoroutine>c__Iterator.subscribeToNewsletter = subscribeToNewsletter;
		<LoginCoroutine>c__Iterator.$this = this;
		return <LoginCoroutine>c__Iterator;
	}

	private void Awake()
	{
		this.InitializePopsTelemetry();
		this.InitializePopsAccount();
		try
		{
			this.m_PopsApiWrapper = new PopsApiWrapper("cities_v2", BuildConfig.applicationVersion, null, null, PlatformService.get_platformType().ToString().ToLower(), PlatformService.get_userID().ToString(), false, this.m_UseTelemetry, true, new PopsApiWrapper.LogCallback(this.LogCallback), 3, false);
		}
		catch (Exception ex)
		{
			Debug.LogError("Failed to initialize Paradox Online Publishing Services handle: " + ex.ToString());
			this.m_PopsApiWrapper = null;
			this.DisableTelemetry();
			this.DisableAccount();
		}
		this.SendStartupEvents();
	}

	private void LogCallback(string message)
	{
		Debug.Log(StringUtils.SafeFormat("PopsApi: {0}", message));
	}

	private void OnEnable()
	{
		this.RegisterTelemetryEvents();
	}

	private void OnDisable()
	{
		this.UnRegisterTelemetryEvents();
	}

	private void OnApplicationQuit()
	{
		this.DisableAccount();
		this.DisableTelemetry();
		try
		{
			if (this.m_PopsApiWrapper != null)
			{
				this.m_PopsApiWrapper.Dispose();
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
		this.m_PopsApiWrapper = null;
	}

	private void InitializePopsTelemetry()
	{
		this.m_ShortcutsUsed = new HashSet<string>();
		this.m_EntryBuffer = new List<PopsApiWrapper.TelemetryEntry>();
		this.m_UseTelemetry = true;
		this.m_Debug = false;
	}

	private void DisableTelemetry()
	{
		this.m_UseTelemetry = false;
	}

	public void Send(PopsApiWrapper.TelemetryEntry telemetryEntry)
	{
		try
		{
			if (this.m_Debug)
			{
				this.LogEvent(telemetryEntry);
			}
			if (this.m_UseTelemetry)
			{
				this.m_PopsApiWrapper.SendTelemetry(telemetryEntry.eventName, (List<KeyValuePair<string, string>>)telemetryEntry.keyValuePairs);
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void Buffer(PopsApiWrapper.TelemetryEntry telemetryEntry)
	{
		this.m_EntryBuffer.Add(telemetryEntry);
	}

	public void Push()
	{
		try
		{
			if (this.m_Debug)
			{
				foreach (PopsApiWrapper.TelemetryEntry current in this.m_EntryBuffer)
				{
					this.LogEvent(current);
				}
			}
			if (this.m_EntryBuffer.Count > 0 && this.m_UseTelemetry)
			{
				this.m_PopsApiWrapper.SendTelemetryMulti(this.m_EntryBuffer);
			}
			this.m_EntryBuffer.Clear();
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private void LogEvent(PopsApiWrapper.TelemetryEntry telemetryEntry)
	{
		if (Debug.get_isDebugBuild() || Application.get_isEditor())
		{
			Debug.Log(StringUtils.SafeFormat("Pops telemetry event: {0}", this.FormatDebugEvent(telemetryEntry)));
		}
	}

	private void SendStartupEvents()
	{
		try
		{
			this.Buffer(PopsTelemetryEventFormatting.HardwareReport());
			this.BufferDLCEvents();
			this.Buffer(PopsTelemetryEventFormatting.GameLanguage(SingletonLite<LocaleManager>.get_instance().get_language(), CultureInfo.CurrentUICulture.TwoLetterISOLanguageName));
			this.Push();
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private void BufferDLCEvents()
	{
		IEnumerator enumerator = Enum.GetValues(typeof(SteamHelper.DLC)).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				SteamHelper.DLC dLC = (SteamHelper.DLC)enumerator.Current;
				if (this.SendTelemetryForDLC(dLC) && SteamHelper.IsDLCAvailable(dLC) && SteamHelper.IsAppOwned((uint)dLC))
				{
					this.Buffer(PopsTelemetryEventFormatting.DLC(dLC, SteamHelper.IsDLCOwned(dLC)));
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	private bool SendTelemetryForDLC(SteamHelper.DLC dlc)
	{
		return dlc != SteamHelper.DLC.None && dlc != SteamHelper.DLC.NotAllowed && dlc != SteamHelper.DLC.Football2 && dlc != SteamHelper.DLC.Football3 && dlc != SteamHelper.DLC.Football4 && dlc != SteamHelper.DLC.Football5;
	}

	private void RegisterTelemetryEvents()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		Singleton<LoadingManager>.get_instance().m_levelPreUnloaded += new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		Singleton<UnlockManager>.get_instance().EventMilestoneUnlocked += new UnlockManager.MilestoneUnlockedHandler(this.OnMilestoneUnlocked);
	}

	private void UnRegisterTelemetryEvents()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		Singleton<LoadingManager>.get_instance().m_levelPreUnloaded -= new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		Singleton<UnlockManager>.get_instance().EventMilestoneUnlocked -= new UnlockManager.MilestoneUnlockedHandler(this.OnMilestoneUnlocked);
	}

	public void OnBuildingPlaced(Building building)
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.BuildingPlaced(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, building.Info.m_dlcRequired, building.Info.m_isCustomContent, Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, building.Info.get_name(), building.m_position.x, building.m_position.z));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private void OnMilestoneUnlocked(MilestoneInfo info)
	{
		try
		{
			if (!string.IsNullOrEmpty(info.get_name()) && info.m_openUnlockPanel)
			{
				this.Send(PopsTelemetryEventFormatting.MilestoneReached(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, info, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime));
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void Playthrough(SimulationManager.UpdateMode updateMode, string mapName)
	{
		try
		{
			this.Buffer(PopsTelemetryEventFormatting.Playthrough(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, updateMode, mapName, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, true));
			foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
			{
				if (current.get_isEnabled())
				{
					this.Buffer(PopsTelemetryEventFormatting.ModUsed(current));
				}
			}
			this.Push();
			this.m_RadioStationTimer = Time.get_time();
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void ScenarioStart(string name, bool builtin, PublishedFileId id, SteamHelper.DLC dlc)
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.ScenarioStarted(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, dlc, !builtin, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, name));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void ScenarioEnd(string name, bool win, string trigger, SteamHelper.DLC dlc)
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.ScenarioEnded(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, dlc, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, name, win, trigger));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private void OnLevelUnloaded()
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.Playthrough(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode, Singleton<SimulationManager>.get_instance().m_metaData.m_MapName, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, false));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void UGCManaged(string name, bool check)
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.UGCManaged(check, name));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void BufferUGCManaged(string name, bool check)
	{
		try
		{
			this.Buffer(PopsTelemetryEventFormatting.UGCManaged(check, name));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private void OnLocaleChanged()
	{
		try
		{
			string language = SingletonLite<LocaleManager>.get_instance().get_language();
			string twoLetterISOLanguageName = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;
			this.Send(PopsTelemetryEventFormatting.GameLanguage(language, twoLetterISOLanguageName));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void ShortcutPressed(string tagString)
	{
		try
		{
			if (!this.m_ShortcutsUsed.Contains(tagString))
			{
				this.m_ShortcutsUsed.Add(tagString);
				this.Send(PopsTelemetryEventFormatting.ShortcutUsed(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode, tagString));
			}
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void GuideMessage(string name)
	{
		try
		{
			this.Send(PopsTelemetryEventFormatting.GuideMessageReceived(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, name));
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	public void RadioChannel(RadioChannelInfo lastStation)
	{
		try
		{
			float time = Time.get_time();
			this.Send(PopsTelemetryEventFormatting.RadioChannel(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, lastStation.get_name(), (int)(time - this.m_RadioStationTimer)));
			this.m_RadioStationTimer = time;
		}
		catch (Exception ex)
		{
			Debug.LogWarning("Failed to send telemetry event: " + ex.ToString());
		}
	}

	private string FormatDebugEvent(PopsApiWrapper.TelemetryEntry telemetryEntry)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("{\"");
		stringBuilder.Append(telemetryEntry.eventName);
		stringBuilder.Append("\": {");
		bool flag = true;
		foreach (KeyValuePair<string, string> current in telemetryEntry.keyValuePairs)
		{
			if (flag)
			{
				flag = false;
			}
			else
			{
				stringBuilder.Append(", ");
			}
			stringBuilder.Append("\"");
			stringBuilder.Append(current.Key);
			stringBuilder.Append("\": \"");
			stringBuilder.Append(current.Value);
			stringBuilder.Append("\"");
		}
		stringBuilder.Append("}}");
		return stringBuilder.ToString();
	}

	private bool m_CachedLoggedIn;

	private bool m_AccountEnabled;

	private PopsApiWrapper m_PopsApiWrapper;

	private List<PopsApiWrapper.TelemetryEntry> m_EntryBuffer;

	private HashSet<string> m_ShortcutsUsed;

	private float m_RadioStationTimer;

	private bool m_UseTelemetry;

	private bool m_Debug;

	public delegate void OnLoginChangedHandler();

	public struct AccountCreateData
	{
		public AccountCreateData(string email, string password, string languageCode, string countryCode, DateTime? dateOfBirth, string source, string referer, string sourceService, string campaign, string medium, string channel, string firstName, string lastName, string addressLine1, string addressLine2, string city, string state, string zipCode, string phone, string emailTemplate, string landingUrl, string refererAccount)
		{
			this.Email = email;
			this.Password = password;
			this.LanguageCode = languageCode;
			this.CountryCode = countryCode;
			this.DateOfBirth = dateOfBirth;
			this.Source = source;
			this.Referer = referer;
			this.SourceService = sourceService;
			this.Campaign = campaign;
			this.Medium = medium;
			this.Channel = channel;
			this.FirstName = firstName;
			this.LastName = lastName;
			this.AddressLine1 = addressLine1;
			this.AddressLine2 = addressLine2;
			this.City = city;
			this.State = state;
			this.ZipCode = zipCode;
			this.Phone = phone;
			this.EmailTemplate = emailTemplate;
			this.LandingUrl = landingUrl;
			this.RefererAccount = refererAccount;
		}

		public string Email;

		public string Password;

		public string LanguageCode;

		public string CountryCode;

		public DateTime? DateOfBirth;

		public string Source;

		public string Referer;

		public string SourceService;

		public string Campaign;

		public string Medium;

		public string Channel;

		public string FirstName;

		public string LastName;

		public string AddressLine1;

		public string AddressLine2;

		public string City;

		public string State;

		public string ZipCode;

		public string Phone;

		public string EmailTemplate;

		public string LandingUrl;

		public string RefererAccount;
	}

	public struct AccountActionResult
	{
		public AccountActionResult(bool success, string statusMessage)
		{
			this.Success = success;
			this.StatusMessage = statusMessage;
		}

		public bool Success;

		public string StatusMessage;
	}
}
