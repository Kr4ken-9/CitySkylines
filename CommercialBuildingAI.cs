﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class CommercialBuildingAI : PrivateBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		TransferManager.TransferReason incomingResource = this.m_incomingResource;
		switch (incomingResource)
		{
		case TransferManager.TransferReason.Oil:
		case TransferManager.TransferReason.Ore:
		case TransferManager.TransferReason.Logs:
		case TransferManager.TransferReason.Grain:
		case TransferManager.TransferReason.Goods:
		case TransferManager.TransferReason.Coal:
			goto IL_5A;
		case TransferManager.TransferReason.PassengerTrain:
			IL_32:
			if (incomingResource != TransferManager.TransferReason.Petrol && incomingResource != TransferManager.TransferReason.Food && incomingResource != TransferManager.TransferReason.Lumber && incomingResource != TransferManager.TransferReason.None)
			{
				throw new PrefabException(this.m_info, "Invalid incoming resource (" + this.m_incomingResource.ToString() + ")");
			}
			goto IL_5A;
		}
		goto IL_32;
		IL_5A:
		if (this.m_info.m_class.m_service != ItemClass.Service.Commercial)
		{
			throw new PrefabException(this.m_info, "CommercialBuildingAI used with other service type (" + this.m_info.m_class.m_service + ")");
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Connections)
		{
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode == InfoManager.SubInfoMode.Default)
			{
				TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason();
				if (incomingTransferReason != TransferManager.TransferReason.None && (data.m_tempImport != 0 || data.m_finalImport != 0))
				{
					return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)incomingTransferReason];
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			else
			{
				if (currentSubMode != InfoManager.SubInfoMode.WindPower)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				if (data.m_tempExport != 0 || data.m_finalExport != 0)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.BuildingLevel)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			Color color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[4];
			Color color2 = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[5];
			Color color3 = Color.Lerp(color, color2, 0.5f) * 0.5f;
			if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow || this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, color3, 0.333f + (float)this.m_info.m_class.m_level * 0.333f);
			}
			return color3;
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.PrivateIncome)
		{
			int width = data.Width;
			int length = data.Length;
			int num = width * length;
			int num2 = (100 * num + 99) / 100;
			return num2 * 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		return (100 * num + 99) / 100;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		return StringUtils.SafeFormat("{0}: {1}\n{2}: {3}", new object[]
		{
			this.GetIncomingTransferReason().ToString(),
			data.m_customBuffer1,
			this.GetOutgoingTransferReason(buildingID).ToString(),
			data.m_customBuffer2
		});
	}

	public override string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		if ((data.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_IMPOSSIBLE");
		}
		if (this.m_info.m_class.m_subService != ItemClass.SubService.CommercialLow && this.m_info.m_class.m_subService != ItemClass.SubService.CommercialHigh)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_SPECIAL_INDUSTRY");
		}
		if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_COMMERCIAL_HAPPY");
		}
		if (data.m_problems != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_DISTRESS");
		}
		if (data.m_levelUpProgress == 0)
		{
			return base.GetLevelUpInfo(buildingID, ref data, out progress);
		}
		if (data.m_levelUpProgress == 1)
		{
			progress = 0.933333337f;
			return Locale.Get("LEVELUP_HIGHRISE_BAN");
		}
		int num = (int)((data.m_levelUpProgress & 15) - 1);
		int num2 = (data.m_levelUpProgress >> 4) - 1;
		if (num <= num2)
		{
			progress = (float)num * 0.06666667f;
			return Locale.Get("LEVELUP_LOWWEALTH");
		}
		progress = (float)num2 * 0.06666667f;
		return Locale.Get("LEVELUP_LOWLANDVALUE");
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override BuildingInfoSub FindCollapsedInfo(out int rotations)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (!(instance.m_common != null))
		{
			rotations = 0;
			return null;
		}
		if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow)
		{
			return base.FindCollapsedInfo(instance.m_common.m_collapsedLow, out rotations);
		}
		return base.FindCollapsedInfo(instance.m_common.m_collapsedHigh, out rotations);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int width = data.Width;
		int length = data.Length;
		int num = this.MaxIncomingLoadSize();
		int num2 = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), width, length);
		int num3 = Mathf.Max(num2 * 500, num * 4);
		data.m_customBuffer1 = (ushort)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num3 - num, num3);
		DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
		if (specialization != DistrictPolicies.Specialization.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			District[] expr_91_cp_0 = instance.m_districts.m_buffer;
			byte expr_91_cp_1 = district;
			expr_91_cp_0[(int)expr_91_cp_1].m_specializationPoliciesEffect = (expr_91_cp_0[(int)expr_91_cp_1].m_specializationPoliciesEffect | specialization);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		if (version < 187u)
		{
			int width = data.Width;
			int length = data.Length;
			int num = this.MaxIncomingLoadSize();
			int num2 = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), width, length);
			int num3 = Mathf.Max(num2 * 500, num * 2);
			int num4 = Mathf.Max(num2 * 500, num * 4);
			data.m_customBuffer1 = (ushort)((int)data.m_customBuffer1 + num4 - num3);
		}
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainTaxes, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseTaxes, position, 1.5f);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		switch (material)
		{
		case TransferManager.TransferReason.ShoppingB:
		case TransferManager.TransferReason.ShoppingC:
		case TransferManager.TransferReason.ShoppingD:
		case TransferManager.TransferReason.ShoppingE:
		case TransferManager.TransferReason.ShoppingF:
		case TransferManager.TransferReason.ShoppingG:
		case TransferManager.TransferReason.ShoppingH:
			break;
		default:
			if (material != TransferManager.TransferReason.Shopping)
			{
				if (material == this.GetIncomingTransferReason())
				{
					int width = data.Width;
					int length = data.Length;
					int num = this.MaxIncomingLoadSize();
					int num2 = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), width, length);
					int num3 = Mathf.Max(num2 * 500, num * 4);
					int customBuffer = (int)data.m_customBuffer1;
					amountDelta = Mathf.Clamp(amountDelta, 0, num3 - customBuffer);
					data.m_customBuffer1 = (ushort)(customBuffer + amountDelta);
				}
				else
				{
					base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
				}
				return;
			}
			break;
		}
		int customBuffer2 = (int)data.m_customBuffer2;
		amountDelta = Mathf.Clamp(amountDelta, -customBuffer2, 0);
		data.m_customBuffer2 = (ushort)(customBuffer2 + amountDelta);
		data.m_outgoingProblemTimer = 0;
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason();
		if (incomingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(incomingTransferReason, offer);
		}
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason(buildingID);
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(outgoingTransferReason, offer2);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			instance2.m_districts.m_buffer[(int)district].AddCommercialData(buildingData.Width * buildingData.Length, (buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255, this.m_info.m_class.m_subService);
		}
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0)
		{
			if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh && (cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && this.m_info.m_class.m_level == ItemClass.Level.Level3 && instance.m_randomizer.Int32(10u) == 0 && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
			{
				District[] expr_16D_cp_0 = instance2.m_districts.m_buffer;
				byte expr_16D_cp_1 = district;
				expr_16D_cp_0[(int)expr_16D_cp_1].m_cityPlanningPoliciesEffect = (expr_16D_cp_0[(int)expr_16D_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				buildingData.m_flags |= Building.Flags.Demolishing;
				instance.m_currentBuildIndex += 1u;
			}
			if (instance.m_randomizer.Int32(10u) == 0)
			{
				DistrictPolicies.Specialization specializationPolicies = instance2.m_districts.m_buffer[(int)district].m_specializationPolicies;
				DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
				if (specialization != DistrictPolicies.Specialization.None)
				{
					if ((specializationPolicies & specialization) == DistrictPolicies.Specialization.None)
					{
						if (Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
						{
							buildingData.m_flags |= Building.Flags.Demolishing;
							instance.m_currentBuildIndex += 1u;
						}
					}
					else
					{
						District[] expr_229_cp_0 = instance2.m_districts.m_buffer;
						byte expr_229_cp_1 = district;
						expr_229_cp_0[(int)expr_229_cp_1].m_specializationPoliciesEffect = (expr_229_cp_0[(int)expr_229_cp_1].m_specializationPoliciesEffect | specialization);
					}
				}
				else if ((specializationPolicies & (DistrictPolicies.Specialization.Leisure | DistrictPolicies.Specialization.Tourist | DistrictPolicies.Specialization.Organic)) != DistrictPolicies.Specialization.None && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
				{
					buildingData.m_flags |= Building.Flags.Demolishing;
					instance.m_currentBuildIndex += 1u;
				}
			}
		}
		uint num = (instance.m_currentFrameIndex & 3840u) >> 8;
		if (num == 15u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempImport = 0;
			buildingData.m_tempExport = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Taxation taxationPolicies = instance.m_districts.m_buffer[(int)district].m_taxationPolicies;
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		District[] expr_6A_cp_0 = instance.m_districts.m_buffer;
		byte expr_6A_cp_1 = district;
		expr_6A_cp_0[(int)expr_6A_cp_1].m_servicePoliciesEffect = (expr_6A_cp_0[(int)expr_6A_cp_1].m_servicePoliciesEffect | (servicePolicies & (DistrictPolicies.Services.PowerSaving | DistrictPolicies.Services.WaterSaving | DistrictPolicies.Services.SmokeDetectors | DistrictPolicies.Services.Recycling | DistrictPolicies.Services.RecreationalUse | DistrictPolicies.Services.ExtraInsulation | DistrictPolicies.Services.NoElectricity | DistrictPolicies.Services.OnlyElectricity)));
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							District[] expr_206_cp_0 = instance.m_districts.m_buffer;
							byte expr_206_cp_1 = district;
							expr_206_cp_0[(int)expr_206_cp_1].m_cityPlanningPoliciesEffect = (expr_206_cp_0[(int)expr_206_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods));
						}
					}
					else
					{
						District[] expr_1DC_cp_0 = instance.m_districts.m_buffer;
						byte expr_1DC_cp_1 = district;
						expr_1DC_cp_0[(int)expr_1DC_cp_1].m_cityPlanningPoliciesEffect = (expr_1DC_cp_0[(int)expr_1DC_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods));
					}
				}
				else
				{
					District[] expr_18E_cp_0 = instance.m_districts.m_buffer;
					byte expr_18E_cp_1 = district;
					expr_18E_cp_0[(int)expr_18E_cp_1].m_taxationPoliciesEffect = (expr_18E_cp_0[(int)expr_18E_cp_1].m_taxationPoliciesEffect | (taxationPolicies & DistrictPolicies.Taxation.DontTaxLeisure));
					District[] expr_1B2_cp_0 = instance.m_districts.m_buffer;
					byte expr_1B2_cp_1 = district;
					expr_1B2_cp_0[(int)expr_1B2_cp_1].m_cityPlanningPoliciesEffect = (expr_1B2_cp_0[(int)expr_1B2_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & (DistrictPolicies.CityPlanning.NoLoudNoises | DistrictPolicies.CityPlanning.LightningRods)));
				}
			}
			else
			{
				if ((taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseComHigh | DistrictPolicies.Taxation.TaxLowerComHigh)) != (DistrictPolicies.Taxation.TaxRaiseComHigh | DistrictPolicies.Taxation.TaxLowerComHigh))
				{
					District[] expr_140_cp_0 = instance.m_districts.m_buffer;
					byte expr_140_cp_1 = district;
					expr_140_cp_0[(int)expr_140_cp_1].m_taxationPoliciesEffect = (expr_140_cp_0[(int)expr_140_cp_1].m_taxationPoliciesEffect | (taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseComHigh | DistrictPolicies.Taxation.TaxLowerComHigh)));
				}
				District[] expr_164_cp_0 = instance.m_districts.m_buffer;
				byte expr_164_cp_1 = district;
				expr_164_cp_0[(int)expr_164_cp_1].m_cityPlanningPoliciesEffect = (expr_164_cp_0[(int)expr_164_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & (DistrictPolicies.CityPlanning.BigBusiness | DistrictPolicies.CityPlanning.LightningRods)));
			}
		}
		else
		{
			if ((taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseComLow | DistrictPolicies.Taxation.TaxLowerComLow)) != (DistrictPolicies.Taxation.TaxRaiseComLow | DistrictPolicies.Taxation.TaxLowerComLow))
			{
				District[] expr_E1_cp_0 = instance.m_districts.m_buffer;
				byte expr_E1_cp_1 = district;
				expr_E1_cp_0[(int)expr_E1_cp_1].m_taxationPoliciesEffect = (expr_E1_cp_0[(int)expr_E1_cp_1].m_taxationPoliciesEffect | (taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseComLow | DistrictPolicies.Taxation.TaxLowerComLow)));
			}
			District[] expr_105_cp_0 = instance.m_districts.m_buffer;
			byte expr_105_cp_1 = district;
			expr_105_cp_0[(int)expr_105_cp_1].m_cityPlanningPoliciesEffect = (expr_105_cp_0[(int)expr_105_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & (DistrictPolicies.CityPlanning.SmallBusiness | DistrictPolicies.CityPlanning.LightningRods)));
		}
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = base.HandleWorkers(buildingID, ref buildingData, ref behaviourData, ref num, ref num2, ref num3);
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num4 = 0;
		}
		int width = buildingData.Width;
		int length = buildingData.Length;
		int num5 = this.MaxIncomingLoadSize();
		int num6 = 0;
		int num7 = 0;
		base.GetVisitBehaviour(buildingID, ref buildingData, ref behaviourData, ref num6, ref num7);
		int num8 = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), width, length);
		int num9 = Mathf.Max(0, num8 - num7);
		int num10 = num8 * 500;
		int num11 = Mathf.Max(num10, num5 * 4);
		int num12 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
		TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason();
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason(buildingID);
		if (incomingTransferReason != TransferManager.TransferReason.None && num4 != 0 && num12 != 0)
		{
			int num13 = num11 - (int)buildingData.m_customBuffer1;
			int num14 = Mathf.Max(0, Mathf.Min(num4, (num13 * 200 + num11 - 1) / num11));
			int num15 = (num12 * num14 + 9) / 10;
			num15 = Mathf.Max(0, Mathf.Min(num15, num13));
			buildingData.m_customBuffer1 += (ushort)num15;
		}
		if (num4 != 0)
		{
			int num16 = num11;
			if (incomingTransferReason != TransferManager.TransferReason.None)
			{
				num16 = Mathf.Min(num16, (int)buildingData.m_customBuffer1);
			}
			if (outgoingTransferReason != TransferManager.TransferReason.None)
			{
				num16 = Mathf.Min(num16, num11 - (int)buildingData.m_customBuffer2);
			}
			num4 = Mathf.Max(0, Mathf.Min(num4, (num16 * 200 + num11 - 1) / num11));
			int num17 = (num8 * num4 + 9) / 10;
			if (Singleton<SimulationManager>.get_instance().m_isNightTime)
			{
				num17 = num17 + 1 >> 1;
			}
			num17 = Mathf.Max(0, Mathf.Min(num17, num16));
			if (incomingTransferReason != TransferManager.TransferReason.None)
			{
				buildingData.m_customBuffer1 -= (ushort)num17;
			}
			if (outgoingTransferReason != TransferManager.TransferReason.None)
			{
				buildingData.m_customBuffer2 += (ushort)num17;
			}
			num4 = (num17 + 9) / 10;
		}
		int num18;
		int num19;
		int num20;
		int num21;
		int num22;
		this.GetConsumptionRates(new Randomizer((int)buildingID), num4, out num18, out num19, out num20, out num21, out num22);
		int heatingConsumption = 0;
		if (num18 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num18 * 3 + 8 >> 4);
				num22 = num22 * 95 / 100;
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num18 + 2 >> 2);
			}
		}
		if (num21 != 0 && (servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
		{
			num21 = Mathf.Max(1, num21 * 85 / 100);
			num22 = num22 * 95 / 100;
		}
		ItemClass.SubService subService2 = this.m_info.m_class.m_subService;
		int num23;
		if (subService2 != ItemClass.SubService.CommercialLeisure)
		{
			if (subService2 != ItemClass.SubService.CommercialTourist && subService2 != ItemClass.SubService.CommercialEco)
			{
				num23 = Singleton<EconomyManager>.get_instance().GetTaxRate(this.m_info.m_class, taxationPolicies);
			}
			else if ((buildingData.m_flags & Building.Flags.HighDensity) != Building.Flags.None)
			{
				num23 = Singleton<EconomyManager>.get_instance().GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, this.m_info.m_class.m_level, taxationPolicies);
			}
			else
			{
				num23 = Singleton<EconomyManager>.get_instance().GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, this.m_info.m_class.m_level, taxationPolicies);
			}
		}
		else
		{
			if ((buildingData.m_flags & Building.Flags.HighDensity) != Building.Flags.None)
			{
				num23 = Singleton<EconomyManager>.get_instance().GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, this.m_info.m_class.m_level, taxationPolicies);
			}
			else
			{
				num23 = Singleton<EconomyManager>.get_instance().GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, this.m_info.m_class.m_level, taxationPolicies);
			}
			if ((taxationPolicies & DistrictPolicies.Taxation.DontTaxLeisure) != DistrictPolicies.Taxation.None)
			{
				num23 = 0;
			}
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.NoLoudNoises) != DistrictPolicies.CityPlanning.None && Singleton<SimulationManager>.get_instance().m_isNightTime)
			{
				num18 = num18 + 1 >> 1;
				num19 = num19 + 1 >> 1;
				num20 = num20 + 1 >> 1;
				num21 = num21 + 1 >> 1;
				num22 = 0;
			}
		}
		if (num4 != 0)
		{
			int num24 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num18, ref heatingConsumption, ref num19, ref num20, ref num21, servicePolicies);
			num4 = (num4 * num24 + 99) / 100;
			if (num4 != 0)
			{
				int num25 = num22;
				if (num25 != 0)
				{
					if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow)
					{
						if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.SmallBusiness) != DistrictPolicies.CityPlanning.None)
						{
							Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 12, this.m_info.m_class);
							num25 *= 2;
						}
					}
					else if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh && (cityPlanningPolicies & DistrictPolicies.CityPlanning.BigBusiness) != DistrictPolicies.CityPlanning.None)
					{
						Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 25, this.m_info.m_class);
						num25 *= 3;
					}
					if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
					{
						num25 = (num25 * 105 + 99) / 100;
					}
					num25 = Singleton<EconomyManager>.get_instance().AddPrivateIncome(num25, ItemClass.Service.Commercial, this.m_info.m_class.m_subService, this.m_info.m_class.m_level, num23);
					int num26 = (behaviourData.m_touristCount * num25 + (num6 >> 1)) / Mathf.Max(1, num6);
					int num27 = Mathf.Max(0, num25 - num26);
					if (num27 != 0)
					{
						Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.CitizenIncome, num27, this.m_info.m_class);
					}
					if (num26 != 0)
					{
						Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.TourismIncome, num26, this.m_info.m_class);
					}
				}
				int num28;
				int num29;
				this.GetPollutionRates(num4, cityPlanningPolicies, out num28, out num29);
				if (num28 != 0 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
				{
					Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num28, num28, buildingData.m_position, 60f);
				}
				if (num29 != 0)
				{
					Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num29, buildingData.m_position, 60f);
				}
				if (num24 < 100)
				{
					buildingData.m_flags |= Building.Flags.RateReduced;
				}
				else
				{
					buildingData.m_flags &= ~Building.Flags.RateReduced;
				}
				buildingData.m_flags |= Building.Flags.Active;
			}
			else
			{
				buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
			}
		}
		else
		{
			num18 = 0;
			heatingConsumption = 0;
			num19 = 0;
			num20 = 0;
			num21 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
		}
		int num30 = 0;
		int wellbeing = 0;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (behaviourData.m_healthAccumulation != 0)
		{
			if (num + num6 != 0)
			{
				num30 = (behaviourData.m_healthAccumulation + (num + num6 >> 1)) / (num + num6);
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Health, behaviourData.m_healthAccumulation, buildingData.m_position, radius);
		}
		if (behaviourData.m_wellbeingAccumulation != 0)
		{
			if (num + num6 != 0)
			{
				wellbeing = (behaviourData.m_wellbeingAccumulation + (num + num6 >> 1)) / (num + num6);
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Wellbeing, behaviourData.m_wellbeingAccumulation, buildingData.m_position, radius);
		}
		int num31 = Citizen.GetHappiness(num30, wellbeing) * 15 / 100;
		int num32 = num * 20 / num3;
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) == Notification.Problem.None)
		{
			num31 += 20;
		}
		if (buildingData.m_problems == Notification.Problem.None)
		{
			num31 += 25;
		}
		num31 += Mathf.Min(num32, (int)buildingData.m_customBuffer1 * num32 / num11);
		num31 += num32 - Mathf.Min(num32, (int)buildingData.m_customBuffer2 * num32 / num11);
		int num33 = (int)((ItemClass.Level)8 - this.m_info.m_class.m_level);
		int num34 = (int)((ItemClass.Level)11 - this.m_info.m_class.m_level);
		if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh)
		{
			num33++;
			num34++;
		}
		if (num23 < num33)
		{
			num31 += num33 - num23;
		}
		if (num23 > num34)
		{
			num31 -= num23 - num34;
		}
		if (num23 >= num34 + 4)
		{
			if (buildingData.m_taxProblemTimer != 0 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
			{
				int num35 = num23 - num34 >> 2;
				buildingData.m_taxProblemTimer = (byte)Mathf.Min(255, (int)buildingData.m_taxProblemTimer + num35);
				if (buildingData.m_taxProblemTimer >= 96)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_taxProblemTimer >= 32)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
				}
			}
		}
		else
		{
			buildingData.m_taxProblemTimer = (byte)Mathf.Max(0, (int)(buildingData.m_taxProblemTimer - 1));
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
		}
		int num36;
		int num37;
		this.GetAccumulation(new Randomizer((int)buildingID), num4, num23, cityPlanningPolicies, taxationPolicies, out num36, out num37);
		if (num36 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Entertainment, num36, buildingData.m_position, radius);
		}
		if (num37 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Attractiveness, num37);
		}
		num31 = Mathf.Clamp(num31, 0, 100);
		buildingData.m_health = (byte)num30;
		buildingData.m_happiness = (byte)num31;
		buildingData.m_citizenCount = (byte)(num + num6);
		base.HandleDead(buildingID, ref buildingData, ref behaviourData, num2 + num7);
		int num38 = behaviourData.m_crimeAccumulation / 10;
		if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLeisure)
		{
			num38 = num38 * 5 + 3 >> 2;
		}
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num38 = num38 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num38, (int)buildingData.m_citizenCount);
		int num39 = (int)buildingData.m_crimeBuffer;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num, buildingData.m_position, radius);
			int num40 = behaviourData.m_educated0Count * 100 + behaviourData.m_educated1Count * 50 + behaviourData.m_educated2Count * 30;
			num40 = num40 / num + 50;
			buildingData.m_fireHazard = (byte)num40;
		}
		else
		{
			buildingData.m_fireHazard = 0;
		}
		if (buildingData.m_citizenCount != 0)
		{
			num39 = (num39 + (buildingData.m_citizenCount >> 1)) / (int)buildingData.m_citizenCount;
		}
		else
		{
			num39 = 0;
		}
		int num41 = 0;
		int num42 = 0;
		int num43 = 0;
		int num44 = 0;
		if (incomingTransferReason != TransferManager.TransferReason.None)
		{
			base.CalculateGuestVehicles(buildingID, ref buildingData, incomingTransferReason, ref num41, ref num42, ref num43, ref num44);
			buildingData.m_tempImport = (byte)Mathf.Clamp(num44, (int)buildingData.m_tempImport, 255);
		}
		buildingData.m_tempExport = (byte)Mathf.Clamp(behaviourData.m_touristCount, (int)buildingData.m_tempExport, 255);
		SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
		uint num45 = (instance2.m_currentFrameIndex & 3840u) >> 8;
		if ((ulong)num45 == (ulong)((long)(buildingID & 15)) && (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow || this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh) && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance2.m_currentBuildIndex && (buildingData.m_flags & Building.Flags.Upgrading) == Building.Flags.None)
		{
			this.CheckBuildingLevel(buildingID, ref buildingData, ref frameData, ref behaviourData, num6);
		}
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoCustomers | Notification.Problem.NoGoods);
			if ((int)buildingData.m_customBuffer2 > num11 - (num10 >> 1) && num6 <= num8 >> 1)
			{
				buildingData.m_outgoingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_outgoingProblemTimer + 1));
				if (buildingData.m_outgoingProblemTimer >= 192)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoCustomers | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_outgoingProblemTimer >= 128)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoCustomers);
				}
			}
			else
			{
				buildingData.m_outgoingProblemTimer = 0;
			}
			if (buildingData.m_customBuffer1 == 0 && incomingTransferReason != TransferManager.TransferReason.None)
			{
				buildingData.m_incomingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_incomingProblemTimer + 1));
				if (buildingData.m_incomingProblemTimer < 64)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoGoods);
				}
				else
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoGoods | Notification.Problem.MajorProblem);
				}
			}
			else
			{
				buildingData.m_incomingProblemTimer = 0;
			}
			buildingData.m_problems = problem;
			instance.m_districts.m_buffer[(int)district].AddCommercialData(ref behaviourData, num30, num31, num39, num3, num, Mathf.Max(0, num3 - num2), num8, num6, num9, (int)this.m_info.m_class.m_level, num18, heatingConsumption, num19, num20, num21, num22, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255), (int)buildingData.m_finalImport, (int)buildingData.m_finalExport, this.m_info.m_class.m_subService);
			if (buildingData.m_fireIntensity == 0 && incomingTransferReason != TransferManager.TransferReason.None)
			{
				int num46 = num11 - (int)buildingData.m_customBuffer1 - num43;
				num46 -= num5 >> 1;
				if (num46 >= 0)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = num46 * 8 / num5;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = false;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(incomingTransferReason, offer);
				}
			}
			if (buildingData.m_fireIntensity == 0 && outgoingTransferReason != TransferManager.TransferReason.None)
			{
				int num47 = (int)buildingData.m_customBuffer2 - num6 * 100;
				if (num47 >= 100 && num9 > 0)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					offer2.Priority = Mathf.Max(1, num47 * 8 / num11);
					offer2.Building = buildingID;
					offer2.Position = buildingData.m_position;
					offer2.Amount = Mathf.Min(num47 / 100, num9);
					offer2.Active = false;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(outgoingTransferReason, offer2);
				}
			}
			base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
			base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		}
	}

	private void CheckBuildingLevel(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, ref Citizen.BehaviourData behaviour, int visitorCount)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		int num = behaviour.m_wealth1Count + behaviour.m_wealth2Count * 2 + behaviour.m_wealth3Count * 3;
		int averageWealth;
		ItemClass.Level level;
		int num2;
		if (visitorCount != 0)
		{
			averageWealth = (num * 100 + (visitorCount >> 1)) / visitorCount;
			num = (num * 18 + (visitorCount >> 1)) / visitorCount;
			if (num >= 45 && !Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.EducationLevel3))
			{
				num = 44;
			}
			if (num >= 30 && !Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.EducationLevel2))
			{
				num = 29;
			}
			if (num < 30)
			{
				level = ItemClass.Level.Level1;
				num2 = 1 + Mathf.Max(0, num - 15);
			}
			else if (num < 45)
			{
				level = ItemClass.Level.Level2;
				num2 = 1 + (num - 30);
			}
			else
			{
				level = ItemClass.Level.Level3;
				num2 = 1;
			}
			if (level < this.m_info.m_class.m_level)
			{
				num2 = 1;
			}
			else if (level > this.m_info.m_class.m_level)
			{
				num2 = 15;
			}
		}
		else
		{
			level = ItemClass.Level.Level1;
			averageWealth = 0;
			num2 = 0;
		}
		int num3;
		Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.LandValue, buildingData.m_position, out num3);
		ItemClass.Level level2;
		int num4;
		if (num2 != 0)
		{
			if (num3 < 21)
			{
				level2 = ItemClass.Level.Level1;
				num4 = 1 + (num3 * 15 + 10) / 21;
			}
			else if (num3 < 41)
			{
				level2 = ItemClass.Level.Level2;
				num4 = 1 + ((num3 - 21) * 15 + 10) / 20;
			}
			else
			{
				level2 = ItemClass.Level.Level3;
				num4 = 1;
			}
			if (level2 < this.m_info.m_class.m_level)
			{
				num4 = 1;
			}
			else if (level2 > this.m_info.m_class.m_level)
			{
				num4 = 15;
			}
		}
		else
		{
			level2 = ItemClass.Level.Level1;
			num4 = 0;
		}
		bool flag = false;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			if (num3 <= 10)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level3 && num3 <= 30)
		{
			flag = true;
		}
		ItemClass.Level level3 = (ItemClass.Level)Mathf.Min((int)level, (int)level2);
		Singleton<BuildingManager>.get_instance().m_LevelUpWrapper.OnCalculateCommercialLevelUp(ref level3, ref num2, ref num4, ref flag, averageWealth, num3, buildingID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		if (flag)
		{
			buildingData.m_serviceProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_serviceProblemTimer + 1));
			if (buildingData.m_serviceProblemTimer >= 8)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandValueLow | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_serviceProblemTimer >= 4)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
			}
			else
			{
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
			}
		}
		else
		{
			buildingData.m_serviceProblemTimer = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
		}
		if (level3 > this.m_info.m_class.m_level)
		{
			num2 = 0;
			num4 = 0;
			if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialHigh && (cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && level3 == ItemClass.Level.Level3)
			{
				District[] expr_377_cp_0 = instance.m_districts.m_buffer;
				byte expr_377_cp_1 = district;
				expr_377_cp_0[(int)expr_377_cp_1].m_cityPlanningPoliciesEffect = (expr_377_cp_0[(int)expr_377_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				level3 = ItemClass.Level.Level2;
				num2 = 1;
			}
			if (buildingData.m_problems == Notification.Problem.None && level3 > this.m_info.m_class.m_level && this.GetUpgradeInfo(buildingID, ref buildingData) != null && !Singleton<DisasterManager>.get_instance().IsEvacuating(buildingData.m_position))
			{
				frameData.m_constructState = 0;
				base.StartUpgrading(buildingID, ref buildingData);
			}
		}
		buildingData.m_levelUpProgress = (byte)(num2 | num4 << 4);
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 127;
		fireTolerance = 10;
		return true;
	}

	private TransferManager.TransferReason GetIncomingTransferReason()
	{
		return this.m_incomingResource;
	}

	private int MaxIncomingLoadSize()
	{
		return 4000;
	}

	private TransferManager.TransferReason GetOutgoingTransferReason(ushort buildingID)
	{
		int num = 0;
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							num = 0;
						}
					}
					else
					{
						num = 80;
					}
				}
				else
				{
					num = 20;
				}
			}
			else
			{
				num = 4;
			}
		}
		else
		{
			num = 2;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		if (randomizer.Int32(100u) < num)
		{
			switch (randomizer.Int32(4u))
			{
			case 0:
				return TransferManager.TransferReason.Entertainment;
			case 1:
				return TransferManager.TransferReason.EntertainmentB;
			case 2:
				return TransferManager.TransferReason.EntertainmentC;
			case 3:
				return TransferManager.TransferReason.EntertainmentD;
			default:
				return TransferManager.TransferReason.Entertainment;
			}
		}
		else
		{
			switch (randomizer.Int32(8u))
			{
			case 0:
				return TransferManager.TransferReason.Shopping;
			case 1:
				return TransferManager.TransferReason.ShoppingB;
			case 2:
				return TransferManager.TransferReason.ShoppingC;
			case 3:
				return TransferManager.TransferReason.ShoppingD;
			case 4:
				return TransferManager.TransferReason.ShoppingE;
			case 5:
				return TransferManager.TransferReason.ShoppingF;
			case 6:
				return TransferManager.TransferReason.ShoppingG;
			case 7:
				return TransferManager.TransferReason.ShoppingH;
			default:
				return TransferManager.TransferReason.Shopping;
			}
		}
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
		case ImmaterialResourceManager.Resource.FireDepartment:
		case ImmaterialResourceManager.Resource.DeathCare:
		{
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num);
			int num2 = ImmaterialResourceManager.CalculateResourceEffect(num, 100, 500, 50, 100);
			int num3 = ImmaterialResourceManager.CalculateResourceEffect(num + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num3 - num2) / 200f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PoliceDepartment:
		case ImmaterialResourceManager.Resource.PublicTransport:
		case ImmaterialResourceManager.Resource.Entertainment:
		{
			int num4;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num4);
			int num5 = ImmaterialResourceManager.CalculateResourceEffect(num4, 100, 500, 50, 100);
			int num6 = ImmaterialResourceManager.CalculateResourceEffect(num4 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num6 - num5) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.EducationElementary:
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		case ImmaterialResourceManager.Resource.EducationUniversity:
		{
			int num7;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num7);
			int num8 = ImmaterialResourceManager.CalculateResourceEffect(num7, 100, 500, 50, 100);
			int num9 = ImmaterialResourceManager.CalculateResourceEffect(num7 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num9 - num8) / 300f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.NoisePollution:
		{
			int num10;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num10);
			int num11 = ImmaterialResourceManager.CalculateResourceEffect(num10, 10, 100, 0, 100);
			int num12 = ImmaterialResourceManager.CalculateResourceEffect(num10 + Mathf.RoundToInt(amount), 10, 100, 0, 100);
			return Mathf.Clamp((float)(num12 - num11) / 250f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Abandonment:
		{
			int num13;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num13);
			int num14 = ImmaterialResourceManager.CalculateResourceEffect(num13, 15, 50, 10, 20);
			int num15 = ImmaterialResourceManager.CalculateResourceEffect(num13 + Mathf.RoundToInt(amount), 15, 50, 10, 20);
			return Mathf.Clamp((float)(num15 - num14) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.CargoTransport:
		{
			int num16;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num16);
			int num17 = ImmaterialResourceManager.CalculateResourceEffect(num16, 100, 500, 50, 100);
			int num18 = ImmaterialResourceManager.CalculateResourceEffect(num16 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num18 - num17) / 150f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.RadioCoverage:
		case ImmaterialResourceManager.Resource.DisasterCoverage:
		{
			int num19;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num19);
			int num20 = ImmaterialResourceManager.CalculateResourceEffect(num19, 50, 100, 20, 25);
			int num21 = ImmaterialResourceManager.CalculateResourceEffect(num19 + Mathf.RoundToInt(amount), 50, 100, 20, 25);
			return Mathf.Clamp((float)(num21 - num20) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FirewatchCoverage:
		{
			int num22;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num22);
			int num23 = ImmaterialResourceManager.CalculateResourceEffect(num22, 100, 1000, 0, 25);
			int num24 = ImmaterialResourceManager.CalculateResourceEffect(num22 + Mathf.RoundToInt(amount), 100, 1000, 0, 25);
			return Mathf.Clamp((float)(num24 - num23) / 100f, -1f, 1f);
		}
		}
		return base.GetEventImpact(buildingID, ref data, resource, amount);
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return base.GetEventImpact(buildingID, ref data, resource, amount);
		}
		byte b;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out b);
		int num = ImmaterialResourceManager.CalculateResourceEffect((int)b, 50, 255, 50, 100);
		int num2 = ImmaterialResourceManager.CalculateResourceEffect((int)b + Mathf.RoundToInt(amount), 50, 255, 50, 100);
		return Mathf.Clamp((float)(num2 - num) / 150f, -1f, 1f);
	}

	public override int CalculateHomeCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							num = 100;
						}
					}
					else
					{
						num = 250;
					}
				}
				else
				{
					num = 250;
				}
			}
			else if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 200;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 300;
			}
			else
			{
				num = 400;
			}
		}
		else if (@class.m_level == ItemClass.Level.Level1)
		{
			num = 90;
		}
		else if (@class.m_level == ItemClass.Level.Level2)
		{
			num = 100;
		}
		else
		{
			num = 110;
		}
		if (num != 0)
		{
			num = Mathf.Max(200, width * length * num + r.Int32(100u)) / 100;
		}
		return num;
	}

	public override void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		level0 = 100;
		level1 = 0;
		level2 = 0;
		level3 = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							num = 100;
							level0 = 50;
							level1 = 50;
							level2 = 0;
							level3 = 0;
						}
					}
					else
					{
						num = 100;
						level0 = 20;
						level1 = 20;
						level2 = 30;
						level3 = 30;
					}
				}
				else
				{
					num = 100;
					level0 = 30;
					level1 = 30;
					level2 = 20;
					level3 = 20;
				}
			}
			else if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 75;
				level0 = 0;
				level1 = 40;
				level2 = 50;
				level3 = 10;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 100;
				level0 = 0;
				level1 = 20;
				level2 = 50;
				level3 = 30;
			}
			else
			{
				num = 125;
				level0 = 0;
				level1 = 0;
				level2 = 40;
				level3 = 60;
			}
		}
		else if (@class.m_level == ItemClass.Level.Level1)
		{
			num = 50;
			level0 = 100;
			level1 = 0;
			level2 = 0;
			level3 = 0;
		}
		else if (@class.m_level == ItemClass.Level.Level2)
		{
			num = 75;
			level0 = 20;
			level1 = 60;
			level2 = 20;
			level3 = 0;
		}
		else
		{
			num = 100;
			level0 = 5;
			level1 = 15;
			level2 = 30;
			level3 = 50;
		}
		if (num != 0)
		{
			num = Mathf.Max(200, width * length * num + r.Int32(100u)) / 100;
			int num2 = level0 + level1 + level2 + level3;
			if (num2 != 0)
			{
				level0 = (num * level0 + r.Int32((uint)num2)) / num2;
				num -= level0;
			}
			num2 = level1 + level2 + level3;
			if (num2 != 0)
			{
				level1 = (num * level1 + r.Int32((uint)num2)) / num2;
				num -= level1;
			}
			num2 = level2 + level3;
			if (num2 != 0)
			{
				level2 = (num * level2 + r.Int32((uint)num2)) / num2;
				num -= level2;
			}
			level3 = num;
		}
	}

	public override int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		if (@class.m_subService == ItemClass.SubService.CommercialEco)
		{
			num = 50;
		}
		if (num != 0)
		{
			num = Mathf.Max(100, width * length * num + r.Int32(100u)) / 100;
		}
		return num;
	}

	public override void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		ItemClass @class = this.m_info.m_class;
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							electricityConsumption = 72;
							waterConsumption = 70;
							sewageAccumulation = 70;
							garbageAccumulation = 40;
							incomeAccumulation = 700;
						}
					}
					else
					{
						electricityConsumption = 30;
						waterConsumption = 80;
						sewageAccumulation = 80;
						garbageAccumulation = 60;
						incomeAccumulation = 600;
					}
				}
				else
				{
					electricityConsumption = 50;
					waterConsumption = 60;
					sewageAccumulation = 60;
					garbageAccumulation = 50;
					incomeAccumulation = 1000;
				}
			}
			else
			{
				ItemClass.Level level = @class.m_level;
				if (level != ItemClass.Level.Level1)
				{
					if (level != ItemClass.Level.Level2)
					{
						if (level == ItemClass.Level.Level3)
						{
							electricityConsumption = 30;
							waterConsumption = 50;
							sewageAccumulation = 50;
							garbageAccumulation = 25;
							incomeAccumulation = 800;
						}
					}
					else
					{
						electricityConsumption = 30;
						waterConsumption = 60;
						sewageAccumulation = 60;
						garbageAccumulation = 50;
						incomeAccumulation = 560;
					}
				}
				else
				{
					electricityConsumption = 30;
					waterConsumption = 70;
					sewageAccumulation = 70;
					garbageAccumulation = 50;
					incomeAccumulation = 280;
				}
			}
		}
		else
		{
			ItemClass.Level level2 = @class.m_level;
			if (level2 != ItemClass.Level.Level1)
			{
				if (level2 != ItemClass.Level.Level2)
				{
					if (level2 == ItemClass.Level.Level3)
					{
						electricityConsumption = 70;
						waterConsumption = 100;
						sewageAccumulation = 100;
						garbageAccumulation = 25;
						incomeAccumulation = 1120;
					}
				}
				else
				{
					electricityConsumption = 60;
					waterConsumption = 80;
					sewageAccumulation = 80;
					garbageAccumulation = 50;
					incomeAccumulation = 800;
				}
			}
			else
			{
				electricityConsumption = 50;
				waterConsumption = 60;
				sewageAccumulation = 60;
				garbageAccumulation = 100;
				incomeAccumulation = 520;
			}
		}
		if (electricityConsumption != 0)
		{
			electricityConsumption = Mathf.Max(100, productionRate * electricityConsumption + r.Int32(100u)) / 100;
		}
		if (waterConsumption != 0)
		{
			int num = r.Int32(100u);
			waterConsumption = Mathf.Max(100, productionRate * waterConsumption + num) / 100;
			if (sewageAccumulation != 0)
			{
				sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + num) / 100;
			}
		}
		else if (sewageAccumulation != 0)
		{
			sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + r.Int32(100u)) / 100;
		}
		if (garbageAccumulation != 0)
		{
			garbageAccumulation = Mathf.Max(100, productionRate * garbageAccumulation + r.Int32(100u)) / 100;
		}
		if (incomeAccumulation != 0)
		{
			incomeAccumulation = productionRate * incomeAccumulation;
		}
	}

	public override void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		ItemClass @class = this.m_info.m_class;
		noisePollution = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.CommercialLow)
		{
			if (subService != ItemClass.SubService.CommercialHigh)
			{
				if (subService != ItemClass.SubService.CommercialLeisure)
				{
					if (subService != ItemClass.SubService.CommercialTourist)
					{
						if (subService == ItemClass.SubService.CommercialEco)
						{
							noisePollution = 0;
						}
					}
					else
					{
						noisePollution = 150;
					}
				}
				else if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.NoLoudNoises) != DistrictPolicies.CityPlanning.None)
				{
					noisePollution = 150;
				}
				else
				{
					noisePollution = 300;
				}
			}
			else
			{
				ItemClass.Level level = @class.m_level;
				if (level != ItemClass.Level.Level1)
				{
					if (level != ItemClass.Level.Level2)
					{
						if (level == ItemClass.Level.Level3)
						{
							noisePollution = 150;
						}
					}
					else
					{
						noisePollution = 175;
					}
				}
				else
				{
					noisePollution = 200;
				}
			}
		}
		else
		{
			ItemClass.Level level2 = @class.m_level;
			if (level2 != ItemClass.Level.Level1)
			{
				if (level2 != ItemClass.Level.Level2)
				{
					if (level2 == ItemClass.Level.Level3)
					{
						noisePollution = 100;
					}
				}
				else
				{
					noisePollution = 120;
				}
			}
			else
			{
				noisePollution = 140;
			}
		}
		groundPollution = 0;
		noisePollution = (productionRate * noisePollution + 99) / 100;
	}

	private void GetAccumulation(Randomizer r, int productionRate, int taxRate, DistrictPolicies.CityPlanning cityPlanningPolicies, DistrictPolicies.Taxation taxationPolicies, out int entertainment, out int attractiveness)
	{
		ItemClass @class = this.m_info.m_class;
		entertainment = 0;
		attractiveness = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.CommercialLeisure)
		{
			if (subService == ItemClass.SubService.CommercialTourist)
			{
				attractiveness = 8;
			}
		}
		else
		{
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.NoLoudNoises) != DistrictPolicies.CityPlanning.None)
			{
				entertainment = 25;
				attractiveness = 2;
			}
			else
			{
				entertainment = 50;
				attractiveness = 4;
			}
			if ((taxationPolicies & DistrictPolicies.Taxation.DontTaxLeisure) != DistrictPolicies.Taxation.None)
			{
				entertainment += 50;
				attractiveness += 4;
			}
			else
			{
				entertainment += 50 / (taxRate + 1);
				attractiveness += 4 / ((taxRate >> 1) + 1);
			}
		}
		if (entertainment != 0)
		{
			entertainment = (productionRate * entertainment + r.Int32(100u)) / 100;
		}
		if (attractiveness != 0)
		{
			attractiveness = (productionRate * attractiveness + r.Int32(100u)) / 100;
		}
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.CountUnchecked("BUILDING_NAME", text);
		if (num != 0u)
		{
			return Locale.Get("BUILDING_NAME", text, randomizer.Int32(num));
		}
		if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow)
		{
			text = this.m_info.m_class.m_level.ToString();
			num = Locale.Count("COMMERCIAL_LOW_NAME", text);
			return Locale.Get("COMMERCIAL_LOW_NAME", text, randomizer.Int32(num));
		}
		text = this.m_info.m_class.m_level.ToString();
		num = Locale.Count("COMMERCIAL_HIGH_NAME", text);
		return Locale.Get("COMMERCIAL_HIGH_NAME", text, randomizer.Int32(num));
	}

	private DistrictPolicies.Specialization SpecialPolicyNeeded()
	{
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService == ItemClass.SubService.CommercialLeisure)
		{
			return DistrictPolicies.Specialization.Leisure;
		}
		if (subService == ItemClass.SubService.CommercialTourist)
		{
			return DistrictPolicies.Specialization.Tourist;
		}
		if (subService != ItemClass.SubService.CommercialEco)
		{
			return DistrictPolicies.Specialization.None;
		}
		return DistrictPolicies.Specialization.Organic;
	}

	public TransferManager.TransferReason m_incomingResource = TransferManager.TransferReason.Goods;
}
