﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class LivestockAI : AnimalAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("ANIMAL_STATUS_FLYING");
		}
		if ((data.m_flags & CitizenInstance.Flags.Floating) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		if (data.GetLastFrameData().m_velocity.get_sqrMagnitude() < 0.01f)
		{
			target = InstanceID.Empty;
			return Locale.Get("ANIMAL_STATUS_EATING");
		}
		target = InstanceID.Empty;
		return Locale.Get("ANIMAL_STATUS_WANDERING");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
		if (data.m_citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(data.m_citizen);
		}
		else if ((data.m_targetBuilding == 0 && (data.m_flags & CitizenInstance.Flags.Blown) == CitizenInstance.Flags.None) || (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
		}
	}

	public override void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(instanceID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
			data.m_frame0.m_velocity = Vector3.get_zero();
			data.m_frame0.m_position = vector;
			data.m_frame0.m_rotation = rotation;
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos = vector2;
			data.Spawn(instanceID);
		}
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
			}
		}
	}

	public override bool AddWind(ushort instanceID, ref CitizenInstance citizenData, Vector3 wind, InstanceManager.Group group)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None && ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None || wind.y > 19.62f))
		{
			CitizenInstance.Frame lastFrameData = citizenData.GetLastFrameData();
			lastFrameData.m_velocity = lastFrameData.m_velocity * 0.875f + wind * 0.125f;
			wind.y = 0f;
			if (wind.get_sqrMagnitude() > 0.01f)
			{
				lastFrameData.m_rotation = Quaternion.Lerp(lastFrameData.m_rotation, Quaternion.LookRotation(wind), 0.1f);
			}
			citizenData.SetLastFrameData(lastFrameData);
			if ((citizenData.m_flags & CitizenInstance.Flags.Blown) == CitizenInstance.Flags.None)
			{
				this.SetTarget(instanceID, ref citizenData, 0);
				citizenData.m_flags |= CitizenInstance.Flags.Blown;
				citizenData.m_waitCounter = 0;
			}
			InstanceID empty = InstanceID.Empty;
			empty.CitizenInstance = instanceID;
			Singleton<InstanceManager>.get_instance().SetGroup(empty, group);
			return true;
		}
		return false;
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
			frameData.m_velocity.y = frameData.m_velocity.y - 2.4525f;
			frameData.m_velocity *= 0.99f;
			frameData.m_position += frameData.m_velocity * 0.5f;
			float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
			if (num > frameData.m_position.y)
			{
				frameData.m_velocity = Vector3.get_zero();
				frameData.m_position.y = num;
				citizenData.m_waitCounter = (byte)Mathf.Min((int)(citizenData.m_waitCounter + 1), 255);
				if (citizenData.m_waitCounter >= 100)
				{
					citizenData.Unspawn(instanceID);
					return;
				}
			}
			else
			{
				citizenData.m_waitCounter = 0;
			}
		}
		else
		{
			float sqrMagnitude = frameData.m_velocity.get_sqrMagnitude();
			if (sqrMagnitude > 0.01f)
			{
				frameData.m_position += frameData.m_velocity * 0.5f;
			}
			Vector3 vector = citizenData.m_targetPos - frameData.m_position;
			float sqrMagnitude2 = vector.get_sqrMagnitude();
			float num2 = Mathf.Max(sqrMagnitude * 3f, 6f);
			if (sqrMagnitude2 < num2 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(20u) == 0 && citizenData.m_targetBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
				Vector3 vector2;
				Vector3 vector3;
				Vector2 vector4;
				CitizenInstance.Flags flags;
				info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, instanceID, out vector2, out vector3, out vector4, out flags);
				vector2.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector2);
				citizenData.m_targetPos = vector2;
				vector = citizenData.m_targetPos - frameData.m_position;
				sqrMagnitude2 = vector.get_sqrMagnitude();
			}
			float num3 = this.m_info.m_walkSpeed;
			float num4 = 2f;
			if (sqrMagnitude2 < 4f)
			{
				vector = Vector3.get_zero();
			}
			else
			{
				float num5 = Mathf.Sqrt(sqrMagnitude2);
				num3 = Mathf.Min(num3, num5 * 0.5f);
				Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
				vector = quaternion * vector;
				if (vector.z < Mathf.Abs(vector.x) * 5f)
				{
					if (vector.x >= 0f)
					{
						vector.x = Mathf.Max(1f, vector.x);
					}
					else
					{
						vector.x = Mathf.Min(-1f, vector.x);
					}
					vector.z = Mathf.Abs(vector.x) * 5f;
					num3 = Mathf.Min(0.5f, num5 * 0.1f);
				}
				vector = Vector3.ClampMagnitude(frameData.m_rotation * vector, num3);
			}
			Vector3 vector5 = vector - frameData.m_velocity;
			float magnitude = vector5.get_magnitude();
			vector5 *= num4 / Mathf.Max(magnitude, num4);
			frameData.m_velocity += vector5;
			citizenData.m_targetPos.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position + frameData.m_velocity);
			frameData.m_velocity.y = citizenData.m_targetPos.y - frameData.m_position.y;
			float sqrMagnitude3 = frameData.m_velocity.get_sqrMagnitude();
			if (sqrMagnitude3 > 0.01f)
			{
				Vector3 vector6 = frameData.m_velocity;
				if (!lodPhysics)
				{
					Vector3 vector7 = Vector3.get_zero();
					float num6 = 0f;
					base.CheckCollisions(instanceID, ref citizenData, frameData.m_position, frameData.m_position + frameData.m_velocity, citizenData.m_targetBuilding, ref vector7, ref num6);
					if (num6 > 0.01f)
					{
						vector7 *= 1f / num6;
						vector7 = Vector3.ClampMagnitude(vector7, Mathf.Sqrt(sqrMagnitude3) * 0.5f);
						frameData.m_velocity += vector7;
						vector6 += vector7 * 0.25f;
					}
				}
				frameData.m_position += frameData.m_velocity * 0.5f;
				if (vector6.get_sqrMagnitude() > 0.01f)
				{
					frameData.m_rotation = Quaternion.LookRotation(vector6);
				}
			}
		}
		if (this.m_randomEffect != null && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(40u) == 0)
		{
			InstanceID instance2 = default(InstanceID);
			instance2.CitizenInstance = instanceID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(frameData.m_position, Vector3.get_up(), 0f);
			float num7 = 3.75f;
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_randomEffect, instance2, spawnArea, frameData.m_velocity * num7, 0f, 1f, Singleton<CitizenManager>.get_instance().m_audioGroup);
		}
	}

	public EffectInfo m_randomEffect;
}
