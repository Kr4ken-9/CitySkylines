﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class RenderProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		RenderProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new RenderProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void InitializeShaderProperties()
	{
		Shader.SetGlobalTexture("_EnvironmentCubemap", this.m_cubemap);
		Shader.SetGlobalColor("_EnvironmentFogColor", this.m_fogColor);
		Shader.SetGlobalTexture("_LightRainTexture", this.m_lightRainTexture);
		Shader.SetGlobalVector("_LightRainParams", new Vector4(this.m_lightRainUV.x, this.m_lightRainUV.y, this.m_lightRainSpeed, 0f));
		if (Application.get_isPlaying())
		{
			if (SingletonResource<ColorCorrectionManager>.get_instance().lastSelection == 0)
			{
				SingletonResource<ColorCorrectionManager>.get_instance().SetLUT(this.m_ColorCorrectionLUT.get_texture());
			}
			else
			{
				SingletonResource<ColorCorrectionManager>.get_instance().currentSelection = SingletonResource<ColorCorrectionManager>.get_instance().lastSelection;
			}
		}
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("RenderProperties");
			Singleton<RenderManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Cubemap m_cubemap;

	public Texture m_lightRainTexture;

	public float m_lightRainSpeed = 10f;

	public Vector2 m_lightRainUV = new Vector2(0.15f, 0.3f);

	public Color m_fogColor = new Color(0.6f, 0.75f, 0.9f, 1f);

	public float m_fogHeight = 5000f;

	public Shader m_lightShader;

	public Shader m_lightVolumeShader;

	public Shader m_lightFloatingVolumeShader;

	public Shader[] m_groupLayerShaders;

	public Color m_ambientLight = new Color(0.39f, 0.39f, 0.45f, 1f);

	[Range(0f, 3800f)]
	public float m_edgeFogDistance;

	public bool m_useVolumeFog;

	public Color m_volumeFogColor;

	public Color m_pollutionFogColor;

	[Range(0f, 8f)]
	public float m_pollutionFogIntensity;

	[Range(0f, 2f)]
	public float m_pollutionFogIntensityWater;

	public Color m_inscatteringColor;

	[Range(0f, 0.01f)]
	public float m_volumeFogDensity;

	[Range(0f, 4800f)]
	public float m_volumeFogStart;

	[Range(0f, 4800f)]
	public float m_volumeFogDistance = 4800f;

	public Transform m_sun;

	public float m_inscatteringExponent = 15f;

	public float m_inscatteringIntensity = 1f;

	[Range(0f, 4800f)]
	public float m_inscatteringStartDistance;

	[Range(0.001f, 4800f)]
	public float m_inscatteringTransitionSoftness = 0.001f;

	public Texture3DWrapper m_ColorCorrectionLUT;
}
