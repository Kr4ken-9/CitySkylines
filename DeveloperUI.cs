﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using UnityEngine;

public class DeveloperUI : MonoBehaviour
{
	public static void LabelOutline(Rect rect, string text, Color outColor, Color inColor, GUIStyle style, float offset)
	{
		float num = offset * 0.5f;
		GUIStyle gUIStyle = new GUIStyle(style);
		Color color = GUI.get_color();
		gUIStyle.get_normal().set_textColor(outColor);
		GUI.set_color(outColor);
		rect.set_x(rect.get_x() - num);
		GUI.Label(rect, text, gUIStyle);
		rect.set_x(rect.get_x() + offset);
		GUI.Label(rect, text, gUIStyle);
		rect.set_x(rect.get_x() - num);
		rect.set_y(rect.get_y() - num);
		GUI.Label(rect, text, gUIStyle);
		rect.set_y(rect.get_y() + offset);
		GUI.Label(rect, text, gUIStyle);
		rect.set_y(rect.get_y() - num);
		gUIStyle.get_normal().set_textColor(inColor);
		GUI.set_color(color);
		GUI.Label(rect, text, gUIStyle);
	}

	private void Awake()
	{
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
		this.m_supportedLocaleIDs = SingletonLite<LocaleManager>.get_instance().get_supportedLocaleIDs();
	}

	private void Update()
	{
		if (Singleton<SimulationManager>.get_instance().m_metaData != null && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
		{
			Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements = SimulationMetaData.MetaBool.True;
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				Singleton<UnlockManager>.get_instance().MilestonesUpdated();
			}
		}
		if (Input.GetKeyDown(258))
		{
			this.LoadNextLanguage();
		}
		if (Input.GetKeyDown(264))
		{
			this.LoadPreviousLanguage();
		}
	}

	public void UpdateTimeHack()
	{
		if (this.m_HackTimeSpeed != 0f)
		{
			float num = Time.get_deltaTime() * this.m_HackTimeSpeed * this.m_HackTimeSpeed / 576f;
			uint num2 = (uint)(num * SimulationManager.DAYTIME_FRAMES);
			num2 = (Singleton<SimulationManager>.get_instance().m_dayTimeOffsetFrames + num2 & SimulationManager.DAYTIME_FRAMES - 1u);
			Singleton<SimulationManager>.get_instance().m_dayTimeOffsetFrames = num2;
		}
	}

	private void OnGUI()
	{
		if (this.m_TutorialTagTest)
		{
			int num = 0;
			int num2 = 0;
			GUILayout.BeginArea(new Rect(340f, 50f, 150f, 700f));
			foreach (GuideInfo current in this.m_Guides)
			{
				if (num2 >= 28)
				{
					num2 = 0;
					num++;
					GUILayout.EndArea();
					GUILayout.BeginArea(new Rect(340f + (float)num * 170f, 50f, 150f, 700f));
				}
				if (GUILayout.Button(current.m_name, new GUILayoutOption[0]))
				{
					GenericGuide guide = new GenericGuide();
					GenericGuide.GenericActivationInfo activationInfo = new GenericGuide.GenericActivationInfo(current);
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						Singleton<GuideManager>.get_instance().Activate(guide, activationInfo);
					});
				}
				num2++;
			}
			GUILayout.EndArea();
		}
		if (this.m_ChirpTestToggled)
		{
			GUILayout.BeginArea(new Rect(340f, 130f, 200f, 20f));
			this.m_ChirpScroll = GUILayout.HorizontalSlider(this.m_ChirpScroll, 0f, Mathf.Ceil((float)(this.m_TestChirpCollection.TotalCount / 24)) + 1f, new GUILayoutOption[0]);
			int num3 = (int)this.m_ChirpScroll;
			GUILayout.EndArea();
			int num4 = -num3;
			int num5 = 0;
			GUILayout.BeginArea(new Rect(340f + (float)num4 * 220f, 150f, 200f, 700f));
			IEnumerator enumerator2 = this.m_TestChirpCollection.GetEnumerator();
			try
			{
				while (enumerator2.MoveNext())
				{
					Locale.Key key = (Locale.Key)enumerator2.Current;
					if (num4 >= 0)
					{
						string identifier = key.m_Identifier;
						string key2 = key.m_Key;
						int index = key.m_Index;
						if (GUILayout.Button(DeveloperUI.TestChirpCollection.Format(identifier, key2, index), new GUILayoutOption[0]))
						{
							CustomCitizenMessage message = new CustomCitizenMessage(Singleton<MessageManager>.get_instance().GetRandomResidentID(), Locale.Get(identifier, key2, index), null);
							ChirpPanel.instance.AddMessage(message, true);
						}
					}
					num5++;
					if (num5 > 24)
					{
						num5 = 0;
						num4++;
						GUILayout.EndArea();
						GUILayout.BeginArea(new Rect(340f + (float)num4 * 220f, 150f, 200f, 700f));
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator2 as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			GUILayout.EndArea();
		}
		GUILayout.BeginArea(new Rect(170f, 50f, 150f, 700f));
		if (DayNightProperties.instance != null)
		{
			GUILayout.Label("Time", new GUILayoutOption[0]);
			float num6 = GUILayout.HorizontalSlider(Singleton<SimulationManager>.get_instance().m_currentDayTimeHour, 0f, 24f, new GUILayoutOption[0]);
			if (num6 != Singleton<SimulationManager>.get_instance().m_currentDayTimeHour)
			{
				num6 /= 24f;
				uint num7 = (uint)(num6 * SimulationManager.DAYTIME_FRAMES);
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				Singleton<SimulationManager>.get_instance().m_dayTimeOffsetFrames = (num7 - currentFrameIndex & SimulationManager.DAYTIME_FRAMES - 1u);
			}
			GUILayout.Label("Day/Night speed", new GUILayoutOption[0]);
			this.m_HackTimeSpeed = GUILayout.HorizontalSlider(this.m_HackTimeSpeed, 0f, 24f, new GUILayoutOption[0]);
		}
		GUILayout.Label("Rain Intensity", new GUILayoutOption[0]);
		float currentRain = Singleton<WeatherManager>.get_instance().m_currentRain;
		float num8 = GUILayout.HorizontalSlider(currentRain, 0f, 1f, new GUILayoutOption[0]);
		if (num8 != currentRain)
		{
			Singleton<WeatherManager>.get_instance().m_targetRain = num8;
			Singleton<WeatherManager>.get_instance().m_currentRain = num8;
		}
		if (DayNightProperties.instance != null)
		{
			RainParticleProperties component = DayNightProperties.instance.GetComponent<RainParticleProperties>();
			if (component != null)
			{
				component.ForceRainMotionBlur = GUILayout.Toggle(component.ForceRainMotionBlur, "Force rain motion blur", new GUILayoutOption[0]);
			}
		}
		GUILayout.Label("Fog Intensity", new GUILayoutOption[0]);
		float currentFog = Singleton<WeatherManager>.get_instance().m_currentFog;
		float num9 = GUILayout.HorizontalSlider(currentFog, 0f, 1f, new GUILayoutOption[0]);
		if (num9 != currentFog)
		{
			Singleton<WeatherManager>.get_instance().m_targetFog = num9;
			Singleton<WeatherManager>.get_instance().m_currentFog = num9;
		}
		GUILayout.Label("Cloud Intensity", new GUILayoutOption[0]);
		float currentCloud = Singleton<WeatherManager>.get_instance().m_currentCloud;
		float num10 = GUILayout.HorizontalSlider(currentCloud, 0f, 1f, new GUILayoutOption[0]);
		if (num10 != currentCloud)
		{
			Singleton<WeatherManager>.get_instance().m_targetCloud = num10;
			Singleton<WeatherManager>.get_instance().m_currentCloud = num10;
		}
		GUILayout.Label("Aurora Intensity", new GUILayoutOption[0]);
		float currentNorthernLights = Singleton<WeatherManager>.get_instance().m_currentNorthernLights;
		float num11 = GUILayout.HorizontalSlider(currentNorthernLights, 0f, 1f, new GUILayoutOption[0]);
		if (num11 != currentNorthernLights)
		{
			Singleton<WeatherManager>.get_instance().m_targetNorthernLights = num11;
			Singleton<WeatherManager>.get_instance().m_currentNorthernLights = num11;
		}
		GUILayout.EndArea();
		GUILayout.BeginArea(new Rect(0f, 50f, 150f, 700f));
		if (GUILayout.Button("Get More Money", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.GetMoney(5000000));
		}
		if (GUILayout.Button("Levelup Building", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.RandomLevelUp());
		}
		if (GUILayout.Button("Abandon Building", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.RandomAbandon());
		}
		if (GUILayout.Button("Remove Buildings", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.RemoveBuildings());
		}
		if (GUILayout.Button("Reset Statistics", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.ResetStatistics());
		}
		if (GUILayout.Button("Unlock Milestone", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.UnlockNextProgressionMilestone());
		}
		if (GUILayout.Button("Unlock Progression", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.UnlockAllProgressionMilestones());
		}
		if (GUILayout.Button("Unlock Unique Buildings", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.UnlockAllMonumentMilestones());
		}
		if (GUILayout.Button("Unlock Monuments", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.UnlockAllWonderMilestones());
		}
		if (GUILayout.Button("Reset Milestones", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.ResetMilestones());
		}
		if (PlatformService.get_active())
		{
			if (GUILayout.Button("Clear Achievements", new GUILayoutOption[0]))
			{
				PlatformService.get_achievements().ClearAll();
			}
			if (GUILayout.Button("Show Cloud", new GUILayoutOption[0]))
			{
				this.m_ShowCloud = !this.m_ShowCloud;
			}
		}
		if (GUILayout.Button("Clear Config", new GUILayoutOption[0]))
		{
			GameSettings.ClearAll(true);
		}
		if (GUILayout.Button("Show Tutorial Tags", new GUILayoutOption[0]))
		{
			this.ToggleTutorialTagTest();
		}
		if (GUILayout.Button("Show Chirps", new GUILayoutOption[0]))
		{
			this.ToggleChirpTest();
		}
		if (Singleton<EventManager>.get_instance().m_events.m_size > 1 && GUILayout.Button("Skip Event State", new GUILayoutOption[0]))
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.SkipEventState());
		}
		if (this.m_cameraController != null)
		{
			this.m_cameraController.m_unlimitedCamera = GUILayout.Toggle(this.m_cameraController.m_unlimitedCamera, "Unlimited Camera", new GUILayoutOption[0]);
			this.m_cameraController.m_analogController = GUILayout.Toggle(this.m_cameraController.m_analogController, "Analog Controller", new GUILayoutOption[0]);
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		instance.m_abandonmentDisabled = GUILayout.Toggle(instance.m_abandonmentDisabled, "Disable Abandonment", new GUILayoutOption[0]);
		instance.m_firesDisabled = GUILayout.Toggle(instance.m_firesDisabled, "Disable Fires", new GUILayoutOption[0]);
		ZoneManager instance2 = Singleton<ZoneManager>.get_instance();
		instance2.m_fullDemand = GUILayout.Toggle(instance2.m_fullDemand, "Full Demand", new GUILayoutOption[0]);
		LoadingManager instance3 = Singleton<LoadingManager>.get_instance();
		bool flag = instance3.m_loadingProfilerUI != null && instance3.m_loadingProfilerUI.get_enabled();
		bool flag2 = GUILayout.Toggle(flag, "Loading Profiler", new GUILayoutOption[0]);
		if (flag2 != flag)
		{
			if (flag2)
			{
				if (instance3.m_loadingProfilerUI == null)
				{
					instance3.m_loadingProfilerUI = instance3.get_gameObject().AddComponent<LoadingProfilerUI>();
				}
				else
				{
					instance3.m_loadingProfilerUI.set_enabled(true);
				}
			}
			else if (instance3.m_loadingProfilerUI != null)
			{
				instance3.m_loadingProfilerUI.set_enabled(false);
			}
		}
		if (instance3.m_loadingProfilerUI != null && instance3.m_loadingProfilerUI.get_enabled())
		{
			instance3.m_loadingProfilerUI.m_CustomContent = GUILayout.Toggle(instance3.m_loadingProfilerUI.m_CustomContent, "Show Custom Content Profiler", new GUILayoutOption[0]);
		}
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
			if (properties != null)
			{
				properties.m_disablePropCollisions = GUILayout.Toggle(properties.m_disablePropCollisions, "Disable Prop Collision", new GUILayoutOption[0]);
			}
		}
		if ((mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			this.m_showDisasters = GUILayout.Toggle(this.m_showDisasters, "Disaster UI", new GUILayoutOption[0]);
			if (this.m_showDisasters)
			{
				this.m_showTriggers = false;
				this.m_showLanguages = false;
			}
		}
		else
		{
			this.m_showDisasters = false;
		}
		if ((mode & ItemClass.Availability.Game) != ItemClass.Availability.None && !string.IsNullOrEmpty(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset))
		{
			this.m_showTriggers = GUILayout.Toggle(this.m_showTriggers, "Trigger UI", new GUILayoutOption[0]);
			if (this.m_showTriggers)
			{
				this.m_showDisasters = false;
				this.m_showLanguages = false;
			}
		}
		else
		{
			this.m_showTriggers = false;
		}
		this.m_showLanguages = GUILayout.Toggle(this.m_showLanguages, "Language UI", new GUILayoutOption[0]);
		if (this.m_showLanguages)
		{
			this.m_showDisasters = false;
			this.m_showTriggers = false;
		}
		if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (GUILayout.Button("Load Decorations", new GUILayoutOption[0]))
			{
				Singleton<SimulationManager>.get_instance().AddAction(Singleton<ToolManager>.get_instance().m_properties.LoadDecorations());
			}
			if (GUILayout.Button("Save Decorations", new GUILayoutOption[0]))
			{
				base.StartCoroutine(this.SaveDecorationsCR());
			}
		}
		GUILayout.EndArea();
		GUILayout.BeginArea(new Rect(340f, 50f, 150f, 700f));
		if (Application.get_isEditor() && (mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (GUILayout.Button("Test buildings", new GUILayoutOption[0]))
			{
				base.StartCoroutine(this.TestSerialization<BuildingInfo>());
			}
			if (GUILayout.Button("Test trees", new GUILayoutOption[0]))
			{
				base.StartCoroutine(this.TestSerialization<TreeInfo>());
			}
			if (GUILayout.Button("Test props", new GUILayoutOption[0]))
			{
				base.StartCoroutine(this.TestSerialization<PropInfo>());
			}
			if (GUILayout.Button("Test vehicles", new GUILayoutOption[0]))
			{
				base.StartCoroutine(this.TestSerialization<VehicleInfo>());
			}
		}
		GUILayout.EndArea();
		if (this.m_showDisasters && this.m_cameraController != null)
		{
			bool flag3 = false;
			int num12 = PrefabCollection<DisasterInfo>.LoadedCount();
			if (num12 != 0)
			{
				GUILayout.BeginArea(new Rect(340f, 50f, 150f, 1000f));
				for (int i = 0; i < num12; i++)
				{
					DisasterInfo loaded = PrefabCollection<DisasterInfo>.GetLoaded((uint)i);
					if (loaded != null && loaded.m_disasterAI.CanSelfTrigger())
					{
						if (GUILayout.Button(loaded.get_gameObject().get_name(), new GUILayoutOption[0]))
						{
							Singleton<SimulationManager>.get_instance().AddAction(this.StartDisaster(loaded, this.m_cameraController.m_currentPosition, -this.m_cameraController.m_currentAngle.x * 0.0174532924f));
						}
						flag3 = true;
					}
				}
				if (flag3)
				{
					DisasterManager instance4 = Singleton<DisasterManager>.get_instance();
					instance4.m_disableAutomaticFollow = GUILayout.Toggle(instance4.m_disableAutomaticFollow, "Disable auto-follow", new GUILayoutOption[0]);
					instance4.m_disableCameraShake = GUILayout.Toggle(instance4.m_disableCameraShake, "Disable camera shake", new GUILayoutOption[0]);
					if (GUILayout.Button("Random Disaster", new GUILayoutOption[0]))
					{
						Singleton<SimulationManager>.get_instance().AddAction(delegate
						{
							Singleton<DisasterManager>.get_instance().StartRandomDisaster();
						});
					}
				}
				GUILayout.EndArea();
			}
		}
		if (this.m_showTriggers)
		{
			UnlockManager unlockManager = Singleton<UnlockManager>.get_instance();
			if (unlockManager.m_scenarioTriggers != null && unlockManager.m_scenarioTriggers.Length != 0)
			{
				GUILayout.BeginArea(new Rect(340f, 50f, 150f, 1000f));
				for (int j = 0; j < unlockManager.m_scenarioTriggers.Length; j++)
				{
					TriggerMilestone milestone = unlockManager.m_scenarioTriggers[j];
					if (milestone != null && GUILayout.Button(milestone.GetLocalizedName(), new GUILayoutOption[0]))
					{
						Singleton<SimulationManager>.get_instance().AddAction(delegate
						{
							unlockManager.CheckMilestone(milestone, true, false);
						});
					}
				}
				GUILayout.EndArea();
			}
		}
		if (this.m_showLanguages)
		{
			LocaleManager instance5 = SingletonLite<LocaleManager>.get_instance();
			GUILayout.BeginArea(new Rect(340f, 50f, 150f, 1000f));
			if (GUILayout.Button("Prev Language", new GUILayoutOption[0]))
			{
				this.LoadPreviousLanguage();
			}
			if (GUILayout.Button("Next Language", new GUILayoutOption[0]))
			{
				this.LoadNextLanguage();
			}
			GUILayout.Space(10f);
			string language = instance5.get_language();
			Color color = GUI.get_color();
			for (int k = 0; k < this.m_supportedLocaleIDs.Length; k++)
			{
				string text = this.m_supportedLocaleIDs[k];
				if (language == this.m_supportedLocaleIDs[k])
				{
					GUI.set_color(Color.get_green());
					text = ">> " + text + " <<";
				}
				if (GUILayout.Button(text, new GUILayoutOption[0]))
				{
					instance5.LoadLocaleByIndex(k);
				}
				if (language == this.m_supportedLocaleIDs[k])
				{
					GUI.set_color(color);
				}
			}
			GUILayout.EndArea();
		}
		if (this.m_ShowCloud)
		{
			CloudHelper.OnGUI();
		}
	}

	private void LoadNextLanguage()
	{
		LocaleManager instance = SingletonLite<LocaleManager>.get_instance();
		int num = this.GetLanguageIndex() + 1;
		if (num >= instance.get_supportedLocaleIDs().Length)
		{
			num = 0;
		}
		instance.LoadLocaleByIndex(num);
	}

	private void LoadPreviousLanguage()
	{
		LocaleManager instance = SingletonLite<LocaleManager>.get_instance();
		int num = this.GetLanguageIndex() - 1;
		if (num < 0)
		{
			num = instance.get_supportedLocaleIDs().Length - 1;
		}
		instance.LoadLocaleByIndex(num);
	}

	private int GetLanguageIndex()
	{
		LocaleManager instance = SingletonLite<LocaleManager>.get_instance();
		for (int i = 0; i < instance.get_supportedLocaleIDs().Length; i++)
		{
			if (instance.get_supportedLocaleIDs()[i] == instance.get_language())
			{
				return i;
			}
		}
		return -1;
	}

	private void ToggleTutorialTagTest()
	{
		this.m_TutorialTagTest = !this.m_TutorialTagTest;
		if (this.m_TutorialTagTest)
		{
			this.m_Guides = new List<GuideInfo>();
			GuideController guideController = Object.FindObjectOfType<GuideController>();
			if (guideController != null)
			{
				FieldInfo[] fields = typeof(GuideController).GetFields(BindingFlags.Instance | BindingFlags.Public);
				FieldInfo[] array = fields;
				for (int i = 0; i < array.Length; i++)
				{
					FieldInfo fieldInfo = array[i];
					if (fieldInfo.FieldType == typeof(GuideInfo))
					{
						GuideInfo item = (GuideInfo)fieldInfo.GetValue(guideController);
						this.m_Guides.Add(item);
					}
				}
			}
		}
	}

	private void ToggleChirpTest()
	{
		this.m_ChirpTestToggled = !this.m_ChirpTestToggled;
		if (this.m_ChirpTestToggled)
		{
			if (this.m_TestChirpCollection == null)
			{
				this.m_TestChirpCollection = new DeveloperUI.TestChirpCollection();
			}
			this.m_TestChirpCollection.Refresh();
		}
	}

	[DebuggerHidden]
	private IEnumerator TestSerialization<T>() where T : PrefabInfo
	{
		return new DeveloperUI.<TestSerialization>c__Iterator0<T>();
	}

	[DebuggerHidden]
	private IEnumerator SaveDecorationsCR()
	{
		return new DeveloperUI.<SaveDecorationsCR>c__Iterator1();
	}

	[DebuggerHidden]
	private IEnumerator SkipEventState()
	{
		return new DeveloperUI.<SkipEventState>c__Iterator2();
	}

	[DebuggerHidden]
	private IEnumerator ResetMilestones()
	{
		return new DeveloperUI.<ResetMilestones>c__Iterator3();
	}

	[DebuggerHidden]
	private IEnumerator StartDisaster(DisasterInfo info, Vector3 target, float angle)
	{
		DeveloperUI.<StartDisaster>c__Iterator4 <StartDisaster>c__Iterator = new DeveloperUI.<StartDisaster>c__Iterator4();
		<StartDisaster>c__Iterator.info = info;
		<StartDisaster>c__Iterator.target = target;
		<StartDisaster>c__Iterator.angle = angle;
		return <StartDisaster>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator RandomLevelUp()
	{
		return new DeveloperUI.<RandomLevelUp>c__Iterator5();
	}

	[DebuggerHidden]
	private IEnumerator RandomAbandon()
	{
		return new DeveloperUI.<RandomAbandon>c__Iterator6();
	}

	[DebuggerHidden]
	private IEnumerator RemoveBuildings()
	{
		return new DeveloperUI.<RemoveBuildings>c__Iterator7();
	}

	[DebuggerHidden]
	private IEnumerator ResetStatistics()
	{
		return new DeveloperUI.<ResetStatistics>c__Iterator8();
	}

	[DebuggerHidden]
	private IEnumerator UnlockNextProgressionMilestone()
	{
		return new DeveloperUI.<UnlockNextProgressionMilestone>c__Iterator9();
	}

	[DebuggerHidden]
	private IEnumerator UnlockAllProgressionMilestones()
	{
		return new DeveloperUI.<UnlockAllProgressionMilestones>c__IteratorA();
	}

	[DebuggerHidden]
	private IEnumerator UnlockAllMonumentMilestones()
	{
		return new DeveloperUI.<UnlockAllMonumentMilestones>c__IteratorB();
	}

	[DebuggerHidden]
	private IEnumerator UnlockAllWonderMilestones()
	{
		return new DeveloperUI.<UnlockAllWonderMilestones>c__IteratorC();
	}

	[DebuggerHidden]
	private IEnumerator GetMoney(int amount)
	{
		DeveloperUI.<GetMoney>c__IteratorD <GetMoney>c__IteratorD = new DeveloperUI.<GetMoney>c__IteratorD();
		<GetMoney>c__IteratorD.amount = amount;
		return <GetMoney>c__IteratorD;
	}

	private CameraController m_cameraController;

	private float m_HackTimeSpeed;

	private bool m_showDisasters;

	private bool m_showTriggers;

	private bool m_showLanguages;

	private bool m_ShowCloud;

	private string[] m_supportedLocaleIDs;

	private bool m_TutorialTagTest;

	private List<GuideInfo> m_Guides;

	private bool m_ChirpTestToggled;

	private DeveloperUI.TestChirpCollection m_TestChirpCollection;

	private float m_ChirpScroll;

	private const int kVerticalEntries = 24;

	public class TestChirpCollection : IEnumerable
	{
		public void Refresh()
		{
			this.m_Chirps = new List<Locale.Key>();
			FieldInfo[] fields = typeof(LocaleID).GetFields(BindingFlags.Static | BindingFlags.Public);
			FieldInfo[] array = fields;
			for (int i = 0; i < array.Length; i++)
			{
				FieldInfo fieldInfo = array[i];
				LocaleAttribute[] array2 = (LocaleAttribute[])fieldInfo.GetCustomAttributes(typeof(LocaleAttribute), true);
				if (array2.Length > 0 && fieldInfo.FieldType == typeof(string))
				{
					LocaleAttribute[] array3 = array2;
					for (int j = 0; j < array3.Length; j++)
					{
						LocaleAttribute localeAttribute = array3[j];
						if (localeAttribute.get_category().ToLower() == "chirps")
						{
							string id = (string)fieldInfo.GetValue(null);
							List<Locale.Key> list = DeveloperUI.TestChirpCollection.ResolveKeys(id);
							this.m_Chirps.AddRange(list);
							foreach (Locale.Key current in list)
							{
								this.m_TotalCount += current.m_Index;
							}
							break;
						}
					}
				}
			}
		}

		private static List<Locale.Key> ResolveKeys(string id)
		{
			List<Locale.Key> list = new List<Locale.Key>();
			if (id == "CHIRP_POLICY")
			{
				IEnumerator enumerator = Enum.GetValues(typeof(DistrictPolicies.Services)).GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						DistrictPolicies.Services services = (DistrictPolicies.Services)enumerator.Current;
						string text = services.ToString();
						if (Locale.Exists(id, text))
						{
							List<Locale.Key> arg_7F_0 = list;
							Locale.Key item = default(Locale.Key);
							item.m_Identifier = id;
							item.m_Key = text;
							item.m_Index = (int)Locale.Count(id, text);
							arg_7F_0.Add(item);
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			else if (id == "CHIRP_DISASTER")
			{
				for (int i = 0; i < PrefabCollection<DisasterInfo>.LoadedCount(); i++)
				{
					string name = PrefabCollection<DisasterInfo>.GetLoaded((uint)i).get_name();
					if (Locale.Exists(id, name))
					{
						List<Locale.Key> arg_10D_0 = list;
						Locale.Key item2 = default(Locale.Key);
						item2.m_Identifier = id;
						item2.m_Key = name;
						item2.m_Index = (int)Locale.Count(id, name);
						arg_10D_0.Add(item2);
					}
				}
			}
			else if (id == "CHIRP_RANDOM_THEME")
			{
				string environment = Singleton<SimulationManager>.get_instance().m_metaData.m_environment;
				if (Locale.Exists(id, environment))
				{
					List<Locale.Key> arg_182_0 = list;
					Locale.Key item3 = default(Locale.Key);
					item3.m_Identifier = id;
					item3.m_Key = environment;
					item3.m_Index = (int)Locale.Count(id, environment);
					arg_182_0.Add(item3);
				}
			}
			if (Locale.Exists(id))
			{
				List<Locale.Key> arg_1B2_0 = list;
				Locale.Key item4 = default(Locale.Key);
				item4.m_Identifier = id;
				item4.m_Index = (int)Locale.Count(id);
				arg_1B2_0.Add(item4);
			}
			if (list.Count == 0)
			{
				Debug.LogWarning(StringUtils.SafeFormat("Chirp testing: locale id {0} requires manually resolving locale keys", id));
			}
			return list;
		}

		[DebuggerHidden]
		public IEnumerator GetEnumerator()
		{
			DeveloperUI.TestChirpCollection.<GetEnumerator>c__Iterator0 <GetEnumerator>c__Iterator = new DeveloperUI.TestChirpCollection.<GetEnumerator>c__Iterator0();
			<GetEnumerator>c__Iterator.$this = this;
			return <GetEnumerator>c__Iterator;
		}

		public int TotalCount
		{
			get
			{
				return this.m_TotalCount;
			}
		}

		public static string Format(string id, string key, int index)
		{
			return ((!id.StartsWith("CHIRP_")) ? id : id.Substring(6)) + (string.IsNullOrEmpty(key) ? string.Empty : ("[" + key + "]")) + ((index <= 0) ? string.Empty : (":" + index));
		}

		private List<Locale.Key> m_Chirps;

		private int m_TotalCount;
	}
}
