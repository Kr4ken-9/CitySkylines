﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class EnvironmentPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(BuildingInfo info)
	{
		return info.GetService() == ItemClass.Service.Beautification;
	}

	protected override bool IsServiceValid(NetInfo info)
	{
		return info.GetService() == ItemClass.Service.Beautification;
	}

	protected override bool IsServiceValid(PropInfo info)
	{
		return info.GetService() == ItemClass.Service.Beautification;
	}

	protected override void OnHideOptionBars()
	{
		if (this.m_OptionsBrushPanel != null)
		{
			this.m_OptionsBrushPanel.Hide();
		}
		base.HideCurvedPropsOptionPanel();
	}

	protected override void Start()
	{
		if (this.m_OptionsBar != null && this.m_OptionsBrushPanel == null)
		{
			this.m_OptionsBrushPanel = this.m_OptionsBar.Find<UIPanel>("BrushPanel");
		}
		base.Start();
	}

	private int ItemsSort(PrefabInfo a, PrefabInfo b)
	{
		int num = a.m_isCustomContent.CompareTo(b.m_isCustomContent);
		if (num == 0)
		{
			num = a.m_UIPriority.CompareTo(b.m_UIPriority);
		}
		if (num == 0)
		{
			num = a.GetService().CompareTo(b.GetService());
		}
		if (num == 0)
		{
			num = a.GetSubService().CompareTo(b.GetSubService());
		}
		if (num == 0)
		{
			num = a.GetClassLevel().CompareTo(b.GetClassLevel());
		}
		if (num == 0)
		{
			num = a.get_name().CompareTo(b.get_name());
		}
		return num;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		if (base.category == "EnvironmentGeneral")
		{
			for (int i = 0; i < EnvironmentPanel.kResources.Length; i++)
			{
				this.SpawnEntry(EnvironmentPanel.kResources[i].get_enumName(), i);
			}
		}
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)35, new Comparison<PrefabInfo>(this.ItemsSort), false);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		if (zOrder == 0 && base.category == "EnvironmentGeneral")
		{
			ResourceTool resourceTool = ToolsModifierControl.SetTool<ResourceTool>();
			if (resourceTool != null)
			{
				if (this.m_OptionsBrushPanel != null)
				{
					this.m_OptionsBrushPanel.Show();
				}
				base.HideCurvedPropsOptionPanel();
				resourceTool.m_resource = EnvironmentPanel.kResources[zOrder].get_enumValue();
			}
		}
		else
		{
			object objectUserData = comp.get_objectUserData();
			BuildingInfo buildingInfo = objectUserData as BuildingInfo;
			PropInfo propInfo = objectUserData as PropInfo;
			NetInfo netInfo = objectUserData as NetInfo;
			if (buildingInfo != null)
			{
				BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
				if (buildingTool != null)
				{
					if (this.m_OptionsBrushPanel != null)
					{
						this.m_OptionsBrushPanel.Hide();
					}
					base.HideCurvedPropsOptionPanel();
					buildingTool.m_prefab = buildingInfo;
					buildingTool.m_relocate = 0;
				}
			}
			else if (propInfo != null)
			{
				PropTool propTool = ToolsModifierControl.SetTool<PropTool>();
				if (propTool != null)
				{
					if (this.m_OptionsBrushPanel != null)
					{
						this.m_OptionsBrushPanel.Show();
					}
					base.HideCurvedPropsOptionPanel();
					propTool.m_prefab = propInfo;
				}
			}
			else if (netInfo != null)
			{
				NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
				if (netTool != null)
				{
					if (this.m_OptionsBrushPanel != null)
					{
						this.m_OptionsBrushPanel.Hide();
					}
					base.ShowCurvedPropsOptionPanel();
					netTool.Prefab = netInfo;
				}
			}
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("ENVIRONMENT_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("ENVIRONMENT_DESC", name)
		});
		return base.CreateButton(name, tooltip, "Resource" + name, index, GeneratedPanel.tooltipBox);
	}

	private static readonly PositionData<NaturalResourceManager.Resource>[] kResources = Utils.GetOrderedEnumData<NaturalResourceManager.Resource>("Decorative");

	private UIPanel m_OptionsBrushPanel;
}
