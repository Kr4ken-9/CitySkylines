﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class PublicTransportLineInfo : ToolsModifierControl
{
	public ushort lineID
	{
		get
		{
			return this.m_LineID;
		}
		set
		{
			this.SetLineID(value);
		}
	}

	private void SetLineID(ushort id)
	{
		this.m_LineID = id;
	}

	public bool isDay
	{
		get
		{
			bool flag;
			bool flag2;
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].GetActive(out flag, out flag2);
			return flag && !flag2;
		}
	}

	public bool isNight
	{
		get
		{
			bool flag;
			bool flag2;
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].GetActive(out flag, out flag2);
			return flag2 && !flag;
		}
	}

	public bool isDayAndNight
	{
		get
		{
			bool flag;
			bool flag2;
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].GetActive(out flag, out flag2);
			return flag && flag2;
		}
	}

	public void RefreshData(bool colors, bool visible)
	{
		if (Singleton<TransportManager>.get_exists())
		{
			this.m_LineName.set_text(Singleton<TransportManager>.get_instance().GetLineName(this.m_LineID));
			if (this.m_LineOperation == null || this.m_LineOperation.completedOrFailed)
			{
				bool flag;
				bool flag2;
				Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].GetActive(out flag, out flag2);
				this.m_DayLine.set_isChecked(flag && !flag2);
				this.m_NightLine.set_isChecked(flag2 && !flag);
				this.m_DayNightLine.set_isChecked(flag && flag2);
			}
			this.m_LineStops.set_text(Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].CountStops(this.m_LineID).ToString("N0"));
			this.m_LineVehicles.set_text(Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].CountVehicles(this.m_LineID).ToString("N0"));
			int averageCount = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_passengers.m_residentPassengers.m_averageCount;
			int averageCount2 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_passengers.m_touristPassengers.m_averageCount;
			this.m_LinePassengers.set_text((averageCount + averageCount2).ToString("N0"));
			this.m_LinePassengers.set_tooltip(LocaleFormatter.FormatGeneric("TRANSPORT_LINE_PASSENGERS", new object[]
			{
				averageCount,
				averageCount2
			}));
			this.m_PassengerCount = averageCount + averageCount2;
			this.m_lineIncompleteWarning.set_isVisible((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None);
			if (colors)
			{
				this.m_LineColor.set_selectedColor(Singleton<TransportManager>.get_instance().GetLineColor(this.m_LineID));
			}
			if (visible)
			{
				this.m_LineIsVisible.set_isChecked((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_flags & TransportLine.Flags.Hidden) == TransportLine.Flags.None);
			}
		}
	}

	public void SetBackgroundColor()
	{
		Color32 backgroundColor = this.m_BackgroundColor;
		backgroundColor.a = ((base.get_component().get_zOrder() % 2 != 0) ? 127 : 255);
		if (this.m_mouseIsOver)
		{
			backgroundColor.r = (byte)Mathf.Min(255, backgroundColor.r * 3 >> 1);
			backgroundColor.g = (byte)Mathf.Min(255, backgroundColor.g * 3 >> 1);
			backgroundColor.b = (byte)Mathf.Min(255, backgroundColor.b * 3 >> 1);
		}
		this.m_Background.set_color(backgroundColor);
	}

	public string lineName
	{
		get
		{
			return this.m_LineName.get_text();
		}
	}

	public string stopCounts
	{
		get
		{
			return this.m_LineStops.get_text();
		}
	}

	public string vehicleCounts
	{
		get
		{
			return this.m_LineVehicles.get_text();
		}
	}

	public int passengerCountsInt
	{
		get
		{
			return this.m_PassengerCount;
		}
	}

	public string passengerCounts
	{
		get
		{
			return this.m_LinePassengers.get_text();
		}
	}

	private void LateUpdate()
	{
		if (base.get_component().get_isVisible())
		{
			this.RefreshData(false, false);
		}
	}

	private void Awake()
	{
		base.get_component().add_eventZOrderChanged(delegate(UIComponent c, int r)
		{
			this.SetBackgroundColor();
		});
		this.m_LineIsVisible = base.Find<UICheckBox>("LineVisible");
		this.m_LineIsVisible.add_eventCheckChanged(delegate(UIComponent c, bool r)
		{
			if (this.m_LineID != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					if (r)
					{
						TransportLine[] expr_2A_cp_0 = Singleton<TransportManager>.get_instance().m_lines.m_buffer;
						ushort expr_2A_cp_1 = this.m_LineID;
						expr_2A_cp_0[(int)expr_2A_cp_1].m_flags = (expr_2A_cp_0[(int)expr_2A_cp_1].m_flags & ~TransportLine.Flags.Hidden);
					}
					else
					{
						TransportLine[] expr_5C_cp_0 = Singleton<TransportManager>.get_instance().m_lines.m_buffer;
						ushort expr_5C_cp_1 = this.m_LineID;
						expr_5C_cp_0[(int)expr_5C_cp_1].m_flags = (expr_5C_cp_0[(int)expr_5C_cp_1].m_flags | TransportLine.Flags.Hidden);
					}
				});
			}
		});
		this.m_LineColor = base.Find<UIColorField>("LineColor");
		this.m_LineColor.add_eventSelectedColorReleased(new PropertyChangedEventHandler<Color>(this.OnColorChanged));
		this.m_ColorFieldButton = this.m_LineColor.Find<UIButton>("Button");
		this.m_LineColor.add_eventColorPickerOpen(delegate(UIColorField field, UIColorPicker picker, ref bool overridden)
		{
			this.m_ColorFieldButton.set_isInteractive(false);
		});
		this.m_LineColor.add_eventColorPickerClose(delegate(UIColorField field, UIColorPicker picker, ref bool overridden)
		{
			this.m_ColorFieldButton.set_isInteractive(true);
		});
		this.m_LineName = base.Find<UILabel>("LineName");
		this.m_LineNameField = this.m_LineName.Find<UITextField>("LineNameField");
		this.m_LineNameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_LineName.add_eventMouseEnter(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.m_LineName.set_backgroundSprite("TextFieldPanelHovered");
		});
		this.m_LineName.add_eventMouseLeave(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.m_LineName.set_backgroundSprite(string.Empty);
		});
		this.m_LineName.add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.m_LineNameField.Show();
			this.m_LineNameField.set_text(this.m_LineName.get_text());
			this.m_LineNameField.Focus();
		});
		this.m_LineNameField.add_eventLeaveFocus(delegate(UIComponent c, UIFocusEventParameter r)
		{
			this.m_LineNameField.Hide();
			this.m_LineName.set_text(this.m_LineNameField.get_text());
		});
		this.m_DayLine = base.Find<UICheckBox>("DayLine");
		this.m_NightLine = base.Find<UICheckBox>("NightLine");
		this.m_DayNightLine = base.Find<UICheckBox>("DayNightLine");
		this.m_DayLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.m_LineID;
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetDayOnly(lineID));
			}
		});
		this.m_NightLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.m_LineID;
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetNightOnly(lineID));
			}
		});
		this.m_DayNightLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.m_LineID;
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetAllDay(lineID));
			}
		});
		this.m_LineStops = base.Find<UILabel>("LineStops");
		this.m_LineVehicles = base.Find<UILabel>("LineVehicles");
		this.m_LinePassengers = base.Find<UILabel>("LinePassengers");
		this.m_Background = base.Find("Background");
		this.m_BackgroundColor = this.m_Background.get_color();
		this.m_mouseIsOver = false;
		base.get_component().add_eventMouseEnter(new MouseEventHandler(this.OnMouseEnter));
		base.get_component().add_eventMouseLeave(new MouseEventHandler(this.OnMouseLeave));
		this.m_lineIncompleteWarning = base.Find<UIPanel>("WarningIncomplete");
		base.Find<UIButton>("DeleteLine").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			if (this.m_LineID != 0)
			{
				ConfirmPanel.ShowModal("CONFIRM_LINEDELETE", delegate(UIComponent comp, int ret)
				{
					if (ret == 1)
					{
						Singleton<SimulationManager>.get_instance().AddAction(delegate
						{
							Singleton<TransportManager>.get_instance().ReleaseLine(this.m_LineID);
						});
					}
				});
			}
		});
		base.Find<UIButton>("ViewLine").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			if (this.m_LineID != 0)
			{
				Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_stops].m_position;
				InstanceID instanceID = default(InstanceID);
				instanceID.TransportLine = this.m_LineID;
				WorldInfoPanel.Show<PublicTransportWorldInfoPanel>(position, instanceID);
				ToolsModifierControl.cameraController.SetTarget(instanceID, position, true);
			}
		});
		base.get_component().add_eventVisibilityChanged(delegate(UIComponent c, bool v)
		{
			if (v)
			{
				this.RefreshData(true, true);
			}
		});
	}

	private void OnMouseEnter(UIComponent comp, UIMouseEventParameter param)
	{
		if (!this.m_mouseIsOver)
		{
			this.m_mouseIsOver = true;
			this.SetBackgroundColor();
			if (this.m_LineID != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					if ((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_flags & TransportLine.Flags.Created) != TransportLine.Flags.None)
					{
						TransportLine[] expr_40_cp_0 = Singleton<TransportManager>.get_instance().m_lines.m_buffer;
						ushort expr_40_cp_1 = this.m_LineID;
						expr_40_cp_0[(int)expr_40_cp_1].m_flags = (expr_40_cp_0[(int)expr_40_cp_1].m_flags | TransportLine.Flags.Highlighted);
					}
				});
			}
		}
	}

	private void OnMouseLeave(UIComponent comp, UIMouseEventParameter param)
	{
		if (this.m_mouseIsOver)
		{
			this.m_mouseIsOver = false;
			this.SetBackgroundColor();
			if (this.m_LineID != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					if ((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)this.m_LineID].m_flags & TransportLine.Flags.Created) != TransportLine.Flags.None)
					{
						TransportLine[] expr_40_cp_0 = Singleton<TransportManager>.get_instance().m_lines.m_buffer;
						ushort expr_40_cp_1 = this.m_LineID;
						expr_40_cp_0[(int)expr_40_cp_1].m_flags = (expr_40_cp_0[(int)expr_40_cp_1].m_flags & ~TransportLine.Flags.Highlighted);
					}
				});
			}
		}
	}

	private void OnEnable()
	{
		Singleton<TransportManager>.get_instance().eventLineColorChanged += new TransportManager.LineColorChangedHandler(this.OnLineChanged);
		Singleton<TransportManager>.get_instance().eventLineNameChanged += new TransportManager.LineNameChangedHandler(this.OnLineChanged);
	}

	private void OnDisable()
	{
		Singleton<TransportManager>.get_instance().eventLineColorChanged -= new TransportManager.LineColorChangedHandler(this.OnLineChanged);
		Singleton<TransportManager>.get_instance().eventLineNameChanged -= new TransportManager.LineNameChangedHandler(this.OnLineChanged);
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.RenameCoroutine(this.m_LineID, text));
	}

	[DebuggerHidden]
	private IEnumerator RenameCoroutine(ushort id, string newName)
	{
		PublicTransportLineInfo.<RenameCoroutine>c__Iterator0 <RenameCoroutine>c__Iterator = new PublicTransportLineInfo.<RenameCoroutine>c__Iterator0();
		<RenameCoroutine>c__Iterator.id = id;
		<RenameCoroutine>c__Iterator.newName = newName;
		<RenameCoroutine>c__Iterator.$this = this;
		return <RenameCoroutine>c__Iterator;
	}

	private void OnLineChanged(ushort id)
	{
		if (id == this.m_LineID)
		{
			this.RefreshData(true, true);
		}
	}

	private void OnColorChanged(UIComponent comp, Color color)
	{
		base.StartCoroutine(this.ChangeColorCoroutine(this.m_LineID, color));
	}

	[DebuggerHidden]
	private IEnumerator ChangeColorCoroutine(ushort id, Color color)
	{
		PublicTransportLineInfo.<ChangeColorCoroutine>c__Iterator1 <ChangeColorCoroutine>c__Iterator = new PublicTransportLineInfo.<ChangeColorCoroutine>c__Iterator1();
		<ChangeColorCoroutine>c__Iterator.id = id;
		<ChangeColorCoroutine>c__Iterator.color = color;
		<ChangeColorCoroutine>c__Iterator.$this = this;
		return <ChangeColorCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetDayOnly(ushort id)
	{
		PublicTransportLineInfo.<SetDayOnly>c__Iterator2 <SetDayOnly>c__Iterator = new PublicTransportLineInfo.<SetDayOnly>c__Iterator2();
		<SetDayOnly>c__Iterator.id = id;
		return <SetDayOnly>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetNightOnly(ushort id)
	{
		PublicTransportLineInfo.<SetNightOnly>c__Iterator3 <SetNightOnly>c__Iterator = new PublicTransportLineInfo.<SetNightOnly>c__Iterator3();
		<SetNightOnly>c__Iterator.id = id;
		return <SetNightOnly>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetAllDay(ushort id)
	{
		PublicTransportLineInfo.<SetAllDay>c__Iterator4 <SetAllDay>c__Iterator = new PublicTransportLineInfo.<SetAllDay>c__Iterator4();
		<SetAllDay>c__Iterator.id = id;
		return <SetAllDay>c__Iterator;
	}

	private ushort m_LineID;

	private UICheckBox m_LineIsVisible;

	private UIColorField m_LineColor;

	private UIButton m_ColorFieldButton;

	private UILabel m_LineName;

	private UITextField m_LineNameField;

	private UICheckBox m_DayLine;

	private UICheckBox m_NightLine;

	private UICheckBox m_DayNightLine;

	private UILabel m_LineStops;

	private UILabel m_LineVehicles;

	private UILabel m_LinePassengers;

	private UIComponent m_Background;

	private Color32 m_BackgroundColor;

	private int m_PassengerCount;

	private UIPanel m_lineIncompleteWarning;

	private bool m_mouseIsOver;

	private AsyncTask m_LineOperation;
}
