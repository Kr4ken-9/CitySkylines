﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class SelectedTrigger : ToolsModifierControl
{
	public static SelectedTrigger instance
	{
		get
		{
			if (SelectedTrigger.m_instance == null)
			{
				SelectedTrigger.m_instance = UIView.Find<UIPanel>("TriggerPanel").GetComponent<SelectedTrigger>();
			}
			return SelectedTrigger.m_instance;
		}
	}

	public bool canRepeatIsChecked
	{
		get
		{
			return this.m_canRepeatIsChecked;
		}
		set
		{
			this.m_canRepeatIsChecked = value;
			this.OnCanRepeatChanged(value);
		}
	}

	private void Awake()
	{
		this.m_triggerName = base.Find<UITextField>("TextFieldTriggerName");
		this.m_triggerName.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnNameChanged));
		this.m_repeatCount = base.Find<UIPanel>("NumberFieldRepeatCount").GetComponent<NumberField>();
		this.m_repeatCount.eventValueChanged += new NumberField.ValueChangedHandler(this.OnRepeatCountChanged);
		this.m_cooldown = base.Find<UIPanel>("NumberFieldCooldown").GetComponent<NumberField>();
		this.m_cooldown.eventValueChanged += new NumberField.ValueChangedHandler(this.OnCooldownChanged);
		this.m_conditionOrderDropDown = base.Find<UIDropDown>("DropdownConditionOrder");
		this.m_conditionOrderDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionOrderIndexChanged));
		this.m_repeatCountPanel = base.Find<UIPanel>("NumberFieldRepeatCount");
		this.m_cooldownPanel = base.Find<UIPanel>("NumberFieldCooldown");
		this.LocalizeConditionOrderDropdown();
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDestroy()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.LocalizeConditionOrderDropdown();
	}

	private void LocalizeConditionOrderDropdown()
	{
		TriggerMilestone.ConditionOrder[] array = (TriggerMilestone.ConditionOrder[])Enum.GetValues(typeof(TriggerMilestone.ConditionOrder));
		string[] array2 = new string[array.Length];
		for (int i = 0; i < array2.Length; i++)
		{
			array2[i] = Locale.Get("CONDITIONORDER", array[i].ToString());
		}
		this.m_conditionOrderDropDown.set_items(array2);
	}

	public void SelectTrigger(TriggerMilestone triggerMilestone)
	{
		this.m_triggerMilestone = triggerMilestone;
		this.m_conditionList.currentTrigger = triggerMilestone;
		this.m_effectList.currentTrigger = triggerMilestone;
		this.Refresh();
	}

	private void Refresh()
	{
		this.m_triggerName.set_text(this.m_triggerMilestone.GetLocalizedName());
		this.m_repeatCount.value = this.m_triggerMilestone.m_maxRepeatCount;
		this.canRepeatIsChecked = (this.m_repeatCount.value > 0);
		this.m_cooldown.value = this.m_triggerMilestone.m_repeatCooldown;
		this.m_conditionOrderDropDown.set_selectedIndex((int)this.m_triggerMilestone.m_conditionOrder);
	}

	private void OnNameChanged(UIComponent comp, string newName)
	{
		this.m_triggerMilestone.SetName(newName);
		this.Refresh();
		this.m_triggerPanel.RefreshTriggerList();
	}

	private void OnRepeatCountChanged(int count)
	{
		this.m_triggerMilestone.m_maxRepeatCount = count;
	}

	private void OnCooldownChanged(int cooldown)
	{
		if (this.m_triggerMilestone != null)
		{
			this.m_triggerMilestone.m_repeatCooldown = cooldown;
		}
	}

	private void OnConditionOrderIndexChanged(UIComponent comp, int index)
	{
		this.m_triggerMilestone.m_conditionOrder = (TriggerMilestone.ConditionOrder)index;
	}

	private void OnCanRepeatChanged(bool isChecked)
	{
		if (this.m_triggerMilestone != null)
		{
			this.m_repeatCountPanel.set_isVisible(isChecked);
			this.m_cooldownPanel.set_isVisible(isChecked);
			if (isChecked)
			{
				this.m_triggerMilestone.m_maxRepeatCount = this.m_repeatCount.value;
			}
			else
			{
				this.m_triggerMilestone.m_maxRepeatCount = 0;
			}
		}
	}

	public TriggerPanel m_triggerPanel;

	public ConditionList m_conditionList;

	public EffectList m_effectList;

	[HideInInspector]
	public TriggerMilestone m_triggerMilestone;

	private UITextField m_triggerName;

	private NumberField m_repeatCount;

	private NumberField m_cooldown;

	private UIDropDown m_conditionOrderDropDown;

	private UICheckBox m_canRepeat;

	private UIPanel m_repeatCountPanel;

	private UIPanel m_cooldownPanel;

	private static SelectedTrigger m_instance;

	private bool m_canRepeatIsChecked;
}
