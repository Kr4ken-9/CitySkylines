﻿using System;
using ColossalFramework.UI;

public sealed class BeautificationPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Beautification;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 1;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)43);
		if (ToolsModifierControl.toolController.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor))
		{
			base.RefreshDLCBadges();
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		NetInfo netInfo = objectUserData as NetInfo;
		TreeInfo treeInfo = objectUserData as TreeInfo;
		PropInfo propInfo = objectUserData as PropInfo;
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				base.HideAllOptionPanels();
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
		if (netInfo != null)
		{
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				if (netInfo.GetClassLevel() == ItemClass.Level.Level3)
				{
					base.ShowFloodwallsOptionPanel();
				}
				else if (netInfo.GetClassLevel() == ItemClass.Level.Level4)
				{
					base.ShowQuaysOptionPanel();
				}
				else if (netInfo.GetClassLevel() == ItemClass.Level.Level5)
				{
					base.ShowCanalsOptionPanel();
				}
				else
				{
					base.ShowPathsOptionPanel();
				}
				netTool.Prefab = netInfo;
			}
		}
		if (treeInfo != null)
		{
			TreeTool treeTool = ToolsModifierControl.SetTool<TreeTool>();
			if (treeTool != null)
			{
				base.HideAllOptionPanels();
				treeTool.m_prefab = treeInfo;
				treeTool.m_mode = TreeTool.Mode.Single;
			}
		}
		if (propInfo != null)
		{
			PropTool propTool = ToolsModifierControl.SetTool<PropTool>();
			if (propTool != null)
			{
				base.HideAllOptionPanels();
				propTool.m_prefab = propInfo;
				propTool.m_mode = PropTool.Mode.Single;
			}
		}
	}
}
