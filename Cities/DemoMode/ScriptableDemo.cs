﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

namespace Cities.DemoMode
{
	public abstract class ScriptableDemo : ScriptableObject
	{
		public void Initialize(DemoController controller)
		{
			this.m_Controller = controller;
		}

		public abstract IEnumerator StartDemo();

		protected bool isRunning
		{
			get
			{
				return this.m_Controller.isRunning;
			}
		}

		public DemoCameraController camera
		{
			get
			{
				return this.m_Controller.cameraController;
			}
		}

		[DebuggerHidden]
		protected IEnumerator FadeOut(float time)
		{
			ScriptableDemo.<FadeOut>c__Iterator0 <FadeOut>c__Iterator = new ScriptableDemo.<FadeOut>c__Iterator0();
			<FadeOut>c__Iterator.time = time;
			<FadeOut>c__Iterator.$this = this;
			return <FadeOut>c__Iterator;
		}

		[DebuggerHidden]
		protected IEnumerator FadeIn(float time)
		{
			ScriptableDemo.<FadeIn>c__Iterator1 <FadeIn>c__Iterator = new ScriptableDemo.<FadeIn>c__Iterator1();
			<FadeIn>c__Iterator.time = time;
			<FadeIn>c__Iterator.$this = this;
			return <FadeIn>c__Iterator;
		}

		[DebuggerHidden]
		protected IEnumerator Wait(float time)
		{
			ScriptableDemo.<Wait>c__Iterator2 <Wait>c__Iterator = new ScriptableDemo.<Wait>c__Iterator2();
			<Wait>c__Iterator.time = time;
			return <Wait>c__Iterator;
		}

		[DebuggerHidden]
		protected IEnumerator DebugChangeFocus(float time, AnimationCurve focalDist, AnimationCurve focalSize, AnimationCurve aperture, AnimationCurve maxBlur, float debugTime)
		{
			ScriptableDemo.<DebugChangeFocus>c__Iterator3 <DebugChangeFocus>c__Iterator = new ScriptableDemo.<DebugChangeFocus>c__Iterator3();
			<DebugChangeFocus>c__Iterator.debugTime = debugTime;
			<DebugChangeFocus>c__Iterator.focalDist = focalDist;
			<DebugChangeFocus>c__Iterator.focalSize = focalSize;
			<DebugChangeFocus>c__Iterator.aperture = aperture;
			<DebugChangeFocus>c__Iterator.maxBlur = maxBlur;
			<DebugChangeFocus>c__Iterator.$this = this;
			return <DebugChangeFocus>c__Iterator;
		}

		[DebuggerHidden]
		protected IEnumerator ChangeFocus(float time, AnimationCurve focalDist, AnimationCurve focalSize, AnimationCurve aperture, AnimationCurve maxBlur)
		{
			ScriptableDemo.<ChangeFocus>c__Iterator4 <ChangeFocus>c__Iterator = new ScriptableDemo.<ChangeFocus>c__Iterator4();
			<ChangeFocus>c__Iterator.time = time;
			<ChangeFocus>c__Iterator.focalDist = focalDist;
			<ChangeFocus>c__Iterator.focalSize = focalSize;
			<ChangeFocus>c__Iterator.aperture = aperture;
			<ChangeFocus>c__Iterator.maxBlur = maxBlur;
			<ChangeFocus>c__Iterator.$this = this;
			return <ChangeFocus>c__Iterator;
		}

		[DebuggerHidden]
		protected IEnumerator ChangeFocus(float time, float startFocalDist, float endFocalDist, float startFocalSize, float endFocalSize, float startAperture, float endAperture, float startMaxSize, float endMaxSize)
		{
			ScriptableDemo.<ChangeFocus>c__Iterator5 <ChangeFocus>c__Iterator = new ScriptableDemo.<ChangeFocus>c__Iterator5();
			<ChangeFocus>c__Iterator.time = time;
			<ChangeFocus>c__Iterator.startFocalDist = startFocalDist;
			<ChangeFocus>c__Iterator.endFocalDist = endFocalDist;
			<ChangeFocus>c__Iterator.startFocalSize = startFocalSize;
			<ChangeFocus>c__Iterator.endFocalSize = endFocalSize;
			<ChangeFocus>c__Iterator.startAperture = startAperture;
			<ChangeFocus>c__Iterator.endAperture = endAperture;
			<ChangeFocus>c__Iterator.startMaxSize = startMaxSize;
			<ChangeFocus>c__Iterator.endMaxSize = endMaxSize;
			<ChangeFocus>c__Iterator.$this = this;
			return <ChangeFocus>c__Iterator;
		}

		protected InstanceID GetRandomBuilding(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Placement placement)
		{
			ushort randomBuilding = this.GetRandomBuilding(service, subService, placement, ItemClass.Layer.PublicTransport | ItemClass.Layer.ShipPaths | ItemClass.Layer.Markers);
			InstanceID id = default(InstanceID);
			id.Building = randomBuilding;
			if (InstanceManager.IsValid(id))
			{
				this.m_Controller.cameraController.SetTarget(id, 45f, 45f, 0f);
			}
			return InstanceID.Empty;
		}

		private ushort GetRandomBuilding(ItemClass.Service service, ItemClass.SubService subservice, ItemClass.Placement placementStyle, ItemClass.Layer layer)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			ushort num = (ushort)instance.m_randomizer.UInt32(1u, 49151u);
			for (int i = 0; i < 49152; i++)
			{
				if ((num += 1) == 49152)
				{
					num = 1;
				}
				if (instance2.m_buildings.m_buffer[(int)num].m_flags != Building.Flags.None)
				{
					if (placementStyle == (ItemClass.Placement)100 || placementStyle == instance2.m_buildings.m_buffer[(int)num].Info.m_placementStyle)
					{
						if (layer == (ItemClass.Layer.PublicTransport | ItemClass.Layer.ShipPaths | ItemClass.Layer.Markers) || (instance2.m_buildings.m_buffer[(int)num].Info.m_class.m_layer & layer) != ItemClass.Layer.None)
						{
							if (service == ItemClass.Service.None && subservice == ItemClass.SubService.None)
							{
								return num;
							}
							if (service != ItemClass.Service.None)
							{
								if (instance2.m_buildings.m_buffer[(int)num].Info.GetService() == service)
								{
									if (subservice == ItemClass.SubService.None)
									{
										return num;
									}
									if (instance2.m_buildings.m_buffer[(int)num].Info.GetSubService() == subservice)
									{
										return num;
									}
								}
							}
							else if (subservice != ItemClass.SubService.None && instance2.m_buildings.m_buffer[(int)num].Info.GetSubService() == subservice)
							{
								return num;
							}
						}
					}
				}
			}
			return (ushort)instance.m_randomizer.UInt32(1u, 49151u);
		}

		protected InstanceID GetTrain()
		{
			List<ushort> list = new List<ushort>();
			InstanceID result = default(InstanceID);
			for (ushort num = 1; num < 256; num += 1)
			{
				if ((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num].m_flags & (TransportLine.Flags.Created | TransportLine.Flags.Temporary)) == TransportLine.Flags.Created)
				{
					TransportInfo info = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num].Info;
					if (info.m_transportType == TransportInfo.TransportType.Train)
					{
						list.Add(num);
					}
				}
			}
			int index = Random.Range(0, list.Count);
			ushort num2 = list[index];
			int num3 = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num2].CountVehicles(num2);
			ushort vehicle = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num2].GetVehicle(Random.Range(0, num3));
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			result.Vehicle = firstVehicle;
			return result;
		}

		private DemoController m_Controller;

		private const ItemClass.Placement kAnyPlacement = (ItemClass.Placement)100;

		private const ItemClass.Layer kAnyLayer = ItemClass.Layer.PublicTransport | ItemClass.Layer.ShipPaths | ItemClass.Layer.Markers;
	}
}
