﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;
using UnityStandardAssets.CinematicEffects;

namespace Cities.DemoMode
{
	public class DemoController : MonoBehaviour
	{
		public FadeEffect fadeEffect
		{
			get
			{
				return this.m_FadeEffect;
			}
		}

		public DemoCameraController cameraController
		{
			get
			{
				return this.m_DemoCameraController;
			}
		}

		public bool isRunning
		{
			get
			{
				return this.m_DemoModeOn;
			}
		}

		private void OnEnable()
		{
			if (this.m_DemoScript == null)
			{
				base.set_enabled(false);
				return;
			}
			this.m_SMAA = base.GetComponent<SMAA>();
			this.m_TAA = base.GetComponent<TemporalAntiAliasing>();
			this.m_Camera = base.GetComponent<Camera>();
			this.m_FadeEffect = base.GetComponent<FadeEffect>();
			this.m_CameraController = base.GetComponent<CameraController>();
			this.m_DemoCameraController = base.GetComponent<DemoCameraController>();
			if (this.m_DemoCameraController == null)
			{
				this.m_DemoCameraController = base.get_gameObject().AddComponent<DemoCameraController>();
			}
			this.m_DemoCameraController.set_enabled(false);
			this.m_DemoScript.Initialize(this);
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}

		private void OnDisable()
		{
			base.StopAllCoroutines();
			Object.Destroy(this.m_DemoCameraController);
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}

		private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
		{
			this.NotifyDemoModeChanged(false);
		}

		public void StopDemo()
		{
			this.NotifyDemoModeChanged(false);
		}

		public void Update()
		{
			if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				return;
			}
			if (Input.get_anyKey() || Input.GetAxis("Mouse X") != 0f || Input.GetAxis("Mouse Y") != 0f || Input.GetAxis("Mouse ScrollWheel") != 0f)
			{
				this.NotifyDemoModeChanged(false);
			}
			this.m_ToDemoTime += Time.get_deltaTime();
			if (this.m_ToDemoTime > this.m_TimeToDemo)
			{
				this.NotifyDemoModeChanged(true);
			}
		}

		private void NotifyDemoModeChanged(bool on)
		{
			if (!this.m_DemoModeOn)
			{
				this.m_ToDemoTime = 0f;
			}
			if (on && !this.m_DemoModeOn)
			{
				this.m_DemoModeOn = true;
				ToolsModifierControl.SetTool<CameraTool>();
				this.m_DemoCameraController.SetTarget(InstanceID.Empty, 45f, 45f, 0f);
				this.m_CameraController.set_enabled(false);
				this.m_DemoCameraController.SetLookup(this.m_CameraController.get_transform().get_position(), this.m_CameraController.get_transform().get_rotation(), Vector3.get_zero());
				this.m_DemoCameraController.set_enabled(true);
				this.m_Camera.set_rect(new Rect(0f, 0f, 1f, 1f));
				this.m_Camera.set_nearClipPlane(2f);
				this.m_Camera.set_farClipPlane(24000f);
				this.m_SMAA.set_enabled(false);
				this.m_TAA.set_enabled(true);
				base.StartCoroutine(this.m_DemoScript.StartDemo());
				UIView.get_library().Hide("PauseMenu", -1);
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
					Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 1;
				});
			}
			else if (!on && this.m_DemoModeOn)
			{
				this.m_SMAA.set_enabled(true);
				this.m_TAA.set_enabled(false);
				this.m_DemoCameraController.SetTarget(InstanceID.Empty, 45f, 45f, 0f);
				UIView.get_library().Hide("StatisticsPanel", -1);
				base.StopAllCoroutines();
				this.m_DemoModeOn = false;
				this.m_DemoCameraController.ShowUI(true);
				this.m_CameraController.set_enabled(true);
				this.m_DemoCameraController.StopOrbit();
				this.m_DemoCameraController.StopScroll();
				this.m_DemoCameraController.set_enabled(false);
				this.m_ToDemoTime = 0f;
				this.m_FadeEffect.set_enabled(false);
				ToolsModifierControl.SetTool<DefaultTool>();
			}
		}

		public ScriptableDemo m_DemoScript;

		public float m_TimeToDemo = 5f;

		private float m_ToDemoTime;

		private bool m_DemoModeOn;

		private FadeEffect m_FadeEffect;

		private CameraController m_CameraController;

		private DemoCameraController m_DemoCameraController;

		private Camera m_Camera;

		private SMAA m_SMAA;

		private TemporalAntiAliasing m_TAA;
	}
}
