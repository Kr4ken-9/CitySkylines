﻿using System;
using ColossalFramework;
using ColossalFramework.Packaging;
using UnityEngine;

namespace Cities.DemoMode
{
	public class DemoModeLoader : MonoBehaviour
	{
		private DemoController controller
		{
			get
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				return gameObject.GetComponent<DemoController>();
			}
		}

		private void Awake()
		{
			if (DemoModeLoader.instance == null)
			{
				DemoModeLoader.instance = this;
				Object.DontDestroyOnLoad(base.get_gameObject());
				this.LoadMap();
			}
			else
			{
				base.set_enabled(false);
				Object.Destroy(base.get_gameObject());
			}
		}

		public void Restart()
		{
			this.LoadMap();
		}

		private void OnEnable()
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}

		private void OnDisable()
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}

		private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
		{
			this.m_CurrentPlayTime = 0f;
		}

		private void Update()
		{
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				this.m_CurrentPlayTime += Time.get_deltaTime();
				if (this.m_CurrentPlayTime > this.m_PlayTimeInterval - 15f && !this.m_CounterStarted)
				{
					this.m_CounterStarted = true;
					LoadSaveStatus.activeTask = new AsyncCounterWrapper(15f);
				}
				if (this.m_CurrentPlayTime > this.m_PlayTimeInterval)
				{
					this.controller.StopDemo();
					this.m_CurrentPlayTime = 0f;
					this.m_CounterStarted = false;
					this.LoadMap();
				}
			}
		}

		private void LoadMap()
		{
			if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading)
			{
				string text = this.m_AvailableEnvs[this.m_CurrentEnv];
				Debug.Log("LoadMap starting " + this.m_SaveGameName + " with " + text);
				Package.Asset asset = PackageManager.FindAssetByName((!(text == "Winter")) ? this.m_SaveGameName : this.m_WinterSaveGameName);
				SimulationMetaData ngs = new SimulationMetaData
				{
					m_environment = text,
					m_updateMode = SimulationManager.UpdateMode.LoadGame,
					m_disableAchievements = SimulationMetaData.MetaBool.True
				};
				this.m_CurrentEnv = (this.m_CurrentEnv + 1) % this.m_AvailableEnvs.Length;
				Singleton<LoadingManager>.get_instance().LoadLevel(asset, "DemoMode", "InGame", ngs);
			}
		}

		public float m_PlayTimeInterval = 300f;

		public string m_SaveGameName;

		public string m_WinterSaveGameName;

		private bool m_CounterStarted;

		private float m_CurrentPlayTime;

		public static DemoModeLoader instance;

		private int m_CurrentEnv;

		private string[] m_AvailableEnvs = new string[]
		{
			"Tropical",
			"North",
			"Winter",
			"Sunny"
		};
	}
}
