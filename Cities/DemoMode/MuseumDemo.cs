﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

namespace Cities.DemoMode
{
	[CreateAssetMenu(fileName = "MuseumDemo.asset", menuName = "Create scriptable demo...")]
	public class MuseumDemo : ScriptableDemo
	{
		private void SetupFirstVistaWithDOF()
		{
			if (Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Winter")
			{
				base.camera.distanceFromTarget = 10f;
			}
			else
			{
				base.camera.distanceFromTarget = 20f;
			}
			Quaternion rotation = Quaternion.Euler(new Vector3(1.402f, -20.7f, 0f));
			base.camera.SetLookup(new Vector3(382.2f, 107f, -3733.9f), rotation, Vector3.get_zero());
			base.camera.SetDOFSettings(this.m_FocalDist1.Evaluate(0f), this.m_FocalSize1.Evaluate(0f), this.m_Aperture1.Evaluate(0f), this.m_MaxBlur1.Evaluate(0f));
			base.camera.Scroll(Vector3.get_up(), 0.6f);
			base.camera.Orbit(0.35f, 0.18f, 1.88f);
		}

		private void SetupSecondVistaScroll()
		{
			base.camera.distanceFromTarget = 0f;
			Vector3 vector;
			vector..ctor(-460f, 207f, -3270f);
			Vector3 vector2;
			vector2..ctor(-1442f, 207f, -1230f);
			Quaternion rotation = Quaternion.Euler(new Vector3(13f, 64.283f, 0f));
			Vector3 vector3 = vector2 - vector;
			base.camera.SetLookup(vector, rotation, Vector3.get_zero());
			base.camera.SetDOFSettings(500f, 0.3f, 3.3f, 2f);
			base.camera.Scroll(vector3.get_normalized(), vector3.get_magnitude() / 18f);
		}

		private void SetupThirdVistaScroll()
		{
			base.camera.distanceFromTarget = 0f;
			Vector3 vector;
			vector..ctor(-2333.4f, 155.6f, 1717.2f);
			Vector3 vector2;
			vector2..ctor(-2216.4f, 155.6f, 1591.2f);
			Quaternion rotation = Quaternion.Euler(new Vector3(14.359f, -132.875f, 0f));
			Vector3 vector3 = vector2 - vector;
			base.camera.SetLookup(vector, rotation, Vector3.get_zero());
			base.camera.SetDOFSettings(80f, 1.5f, 22f, 3f);
			base.camera.Scroll(vector3.get_normalized(), vector3.get_magnitude() / 12f);
		}

		private void SetupFourthVistaBridgeScroll()
		{
			base.camera.distanceFromTarget = 0f;
			Vector3 vector;
			vector..ctor(-2699f, 218.2f, 1635f);
			Vector3 vector2;
			vector2..ctor(-2118.7f, 185.9f, 1026f);
			Quaternion rotation = Quaternion.Euler(new Vector3(12.8f, 155f, 0f));
			Vector3 vector3 = vector2 - vector;
			base.camera.SetLookup(vector, rotation, Vector3.get_zero());
			base.camera.SetDOFSettings(200f, 0.42f, 3.2f, 2f);
			base.camera.Scroll(vector3.get_normalized(), vector3.get_magnitude() / 19f);
			base.camera.Orbit(3.4f, 0f, 0f);
		}

		private void SetupFifthVistaParkScroll()
		{
			base.camera.distanceFromTarget = 0f;
			Vector3 vector;
			vector..ctor(-487.1f, 134.8f, 80.4f);
			Vector3 vector2;
			vector2..ctor(-487.1f, 131.8f, 368.5f);
			Quaternion rotation = Quaternion.Euler(new Vector3(12.431f, -9.337001f, 0f));
			Vector3 vector3 = vector2 - vector;
			base.camera.SetLookup(vector, rotation, Vector3.get_zero());
			base.camera.SetDOFSettings(200f, 0.42f, 3.2f, 2f);
			base.camera.Scroll(vector3.get_normalized(), vector3.get_magnitude() / 14f);
			base.camera.Orbit(-0.8f, -0.3f, 0f);
		}

		private void SetTopDownOrbitOnRandomBuilding(ItemClass.Service type, float distance)
		{
			base.camera.distanceFromTarget = distance;
			base.camera.Orbit(0.7f, 0.18f, 0.88f);
			InstanceID randomBuilding = base.GetRandomBuilding(type, ItemClass.SubService.None, ItemClass.Placement.Manual);
			base.camera.SetTarget(randomBuilding, 45f, 45f, 0f);
		}

		private bool SetBuildingOrbitSettings(ushort id, float distance, float angleX, float angleY, float YOffset = 0f)
		{
			InstanceID id2 = default(InstanceID);
			id2.Building = id;
			if (InstanceManager.IsValid(id2))
			{
				base.camera.distanceFromTarget = distance;
				base.camera.Orbit(0.35f, 0.18f, 1.88f);
				base.camera.SetTarget(id2, angleX, angleY, YOffset);
				return true;
			}
			return false;
		}

		[DebuggerHidden]
		private IEnumerator FadeOut()
		{
			MuseumDemo.<FadeOut>c__Iterator0 <FadeOut>c__Iterator = new MuseumDemo.<FadeOut>c__Iterator0();
			<FadeOut>c__Iterator.$this = this;
			return <FadeOut>c__Iterator;
		}

		[DebuggerHidden]
		private IEnumerator FadeIn()
		{
			MuseumDemo.<FadeIn>c__Iterator1 <FadeIn>c__Iterator = new MuseumDemo.<FadeIn>c__Iterator1();
			<FadeIn>c__Iterator.$this = this;
			return <FadeIn>c__Iterator;
		}

		private void ResetCamera()
		{
			base.camera.StopScroll();
			base.camera.StopOrbit();
			base.camera.SetTarget(InstanceID.Empty, 45f, 45f, 0f);
		}

		[DebuggerHidden]
		public override IEnumerator StartDemo()
		{
			MuseumDemo.<StartDemo>c__Iterator2 <StartDemo>c__Iterator = new MuseumDemo.<StartDemo>c__Iterator2();
			<StartDemo>c__Iterator.$this = this;
			return <StartDemo>c__Iterator;
		}

		public float m_ShotFadeTime = 0.5f;

		public float m_ShotFadedTime = 0.7f;

		public AnimationCurve m_FocalDist1;

		public AnimationCurve m_FocalSize1;

		public AnimationCurve m_Aperture1;

		public AnimationCurve m_MaxBlur1;
	}
}
