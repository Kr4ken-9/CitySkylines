﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace Cities.DemoMode
{
	public class DemoCameraController : MonoBehaviour
	{
		public float distanceFromTarget
		{
			get
			{
				return this.m_DistanceFromTarget;
			}
			set
			{
				this.m_DistanceFromTarget = value;
				this.m_DistanceToTarget = value;
			}
		}

		public void SetTarget(InstanceID id, float angleX = 45f, float angleY = 45f, float YOffset = 0f)
		{
			if (Singleton<InstanceManager>.get_instance().FollowInstance(id))
			{
				this.m_targetInstance = id;
				Vector3 position;
				Quaternion quaternion;
				Vector3 size;
				if (InstanceManager.GetPosition(id, out position, out quaternion, out size))
				{
					Quaternion rotation = Quaternion.Euler(new Vector3(angleX, angleY, 0f));
					position.y += YOffset;
					this.SetLookup(position, rotation, size);
				}
			}
			else if (!this.m_targetInstance.IsEmpty)
			{
				this.m_targetInstance = InstanceID.Empty;
			}
		}

		public void ClearTarget()
		{
			this.SetTarget(InstanceID.Empty, 45f, 45f, 0f);
		}

		public void ChangeTarget(InstanceID oldID, InstanceID newID)
		{
			if (!this.m_targetInstance.IsEmpty && this.m_targetInstance == oldID)
			{
				this.SetTarget(newID, 45f, 45f, 0f);
			}
		}

		public void ShowUI(bool show)
		{
			UIView.Show(show);
			Singleton<NotificationManager>.get_instance().NotificationsVisible = show;
			Singleton<GameAreaManager>.get_instance().BordersVisible = show;
			Singleton<DistrictManager>.get_instance().NamesVisible = show;
			Singleton<PropManager>.get_instance().MarkersVisible = show;
			Singleton<GuideManager>.get_instance().TutorialDisabled = show;
			Singleton<DisasterManager>.get_instance().MarkersVisible = show;
			Singleton<NetManager>.get_instance().RoadNamesVisible = show;
		}

		private void Awake()
		{
			this.m_Camera = base.GetComponent<Camera>();
			this.m_DepthOfField = base.GetComponent<DepthOfField>();
		}

		private void Update()
		{
			bool flag = false;
			if (this.m_IsOrbitting)
			{
				this.m_Angle.y = this.m_Angle.y + Time.get_deltaTime() * this.m_OrbitSpeed.y;
				this.m_Angle.x = this.m_Angle.x + Time.get_deltaTime() * this.m_OrbitSpeed.x;
				this.m_DistanceToTarget -= Time.get_deltaTime() * this.m_ZoomSpeed;
				flag = true;
			}
			if (this.m_IsScrolling)
			{
				this.m_Position += Time.get_deltaTime() * this.m_ScrollDirection * this.m_ScrollSpeed;
				flag = true;
			}
			if (flag)
			{
				this.UpdateLookup();
			}
		}

		private void LateUpdate()
		{
			Vector3 position;
			Quaternion quaternion;
			Vector3 size;
			if (!this.m_IsOrbitting && !this.m_IsScrolling && this.m_targetInstance.Vehicle != 0 && InstanceManager.GetPosition(this.m_targetInstance, out position, out quaternion, out size))
			{
				Quaternion rotation = Quaternion.Euler(new Vector3(this.m_TrainViewAngle, quaternion.get_eulerAngles().y, 0f));
				position.y += this.m_TrainYOffset;
				this.SetLookup(position, rotation, size);
				this.m_DepthOfField.focalSize = 0.42f;
				this.m_DepthOfField.aperture = 3.2f;
				this.m_DepthOfField.maxBlurSize = 2f;
			}
		}

		public void EnableDOF(bool enable)
		{
			this.m_DepthOfField.set_enabled(enable);
		}

		public void SetDOFSettings(float focalLength, float focalSize, float aperture, float maxBlurSize)
		{
			this.m_DepthOfField.focalLength = focalLength;
			this.m_DepthOfField.focalSize = focalSize;
			this.m_DepthOfField.aperture = aperture;
			this.m_DepthOfField.maxBlurSize = maxBlurSize;
		}

		private void OnEnable()
		{
			this.m_DistanceToTarget = 0f;
			Singleton<InstanceManager>.get_instance().m_instanceChanged += new InstanceManager.ChangeAction(this.ChangeTarget);
		}

		private void OnDisable()
		{
			Singleton<InstanceManager>.get_instance().m_instanceChanged -= new InstanceManager.ChangeAction(this.ChangeTarget);
		}

		private void SetPositionRotation(Vector3 position, Quaternion rotation)
		{
			base.get_transform().set_rotation(rotation);
			base.get_transform().set_position(position);
		}

		public void SetLookup(Vector3 position, Quaternion rotation, Vector3 size)
		{
			this.m_Angle = new Vector2(rotation.get_eulerAngles().y, rotation.get_eulerAngles().x);
			this.m_DistanceToTarget = this.m_DistanceFromTarget + size.get_magnitude();
			this.m_Position = position;
			this.SetDOFSettings(this.m_DistanceToTarget, 0.7f, 11.5f, 4f);
			this.UpdateLookup();
		}

		private void UpdateLookup()
		{
			float num = this.m_DistanceToTarget / Mathf.Tan(0.0174532924f * this.m_Camera.get_fieldOfView());
			Quaternion quaternion = Quaternion.AngleAxis(this.m_Angle.x, Vector3.get_up()) * Quaternion.AngleAxis(this.m_Angle.y, Vector3.get_right());
			Vector3 position = this.m_Position + quaternion * new Vector3(0f, 0f, -num);
			this.SetPositionRotation(position, quaternion);
		}

		public void Orbit(float xAngleSpeed, float yAngleSpeed, float zoomSpeed)
		{
			this.m_IsOrbitting = true;
			this.m_OrbitSpeed = new Vector2(xAngleSpeed, yAngleSpeed);
			this.m_ZoomSpeed = zoomSpeed;
		}

		public void StopOrbit()
		{
			this.m_IsOrbitting = false;
		}

		public void Scroll(Vector3 direction, float speed)
		{
			this.m_ScrollDirection = direction;
			this.m_ScrollSpeed = speed;
			this.m_IsScrolling = true;
		}

		public void StopScroll()
		{
			this.m_IsScrolling = false;
		}

		private DepthOfField m_DepthOfField;

		private InstanceID m_targetInstance;

		private Camera m_Camera;

		private Vector2 m_OrbitSpeed;

		private bool m_IsOrbitting;

		private bool m_IsScrolling;

		private Vector3 m_ScrollDirection;

		private float m_ScrollSpeed;

		private float m_DistanceToTarget;

		private float m_ZoomSpeed;

		public float m_TrainYOffset = 4f;

		public float m_TrainViewAngle = 12f;

		public Vector2 m_Angle;

		public Vector3 m_Position;

		public float m_DistanceFromTarget;
	}
}
