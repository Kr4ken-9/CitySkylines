﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class NaturalResourceProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		NaturalResourceProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new NaturalResourceProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void InitializeShaderProperties()
	{
		string[] names = Enum.GetNames(typeof(NaturalResourceManager.Resource));
		int num = Mathf.Min(this.m_resourceColors.Length, names.Length);
		for (int i = 0; i < num; i++)
		{
			Shader.SetGlobalColor("_NaturalResourceColor" + names[i], this.m_resourceColors[i].get_linear());
		}
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("NaturalResourceProperties");
			Singleton<NaturalResourceManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Color[] m_resourceColors;
}
