﻿using System;
using System.Collections.Generic;
using ColossalFramework.Globalization;

public class AssetWarning
{
	public static string WarningMessage(CustomAssetMetaData meta)
	{
		return AssetWarning.WarningMessage(meta.type, meta.triangles, meta.textureHeight, meta.textureWidth, meta.lodTriangles, meta.lodTextureHeight, meta.lodTextureWidth);
	}

	public static string WarningMessage(CustomAssetMetaData.Type type, int triangles, int textureHeight, int textureWidth, int lodTriangles, int lodTextureHeight, int lodTextureWidth)
	{
		string text = string.Empty;
		int num;
		if (!AssetWarning.Triangles.TryGetValue(type, out num))
		{
			return string.Empty;
		}
		int num2;
		if (!AssetWarning.LODTriangles.TryGetValue(type, out num2))
		{
			return string.Empty;
		}
		if (triangles > num)
		{
			text += StringUtils.SafeFormat(Locale.Get("ASSET_WARNING_TRIANGLE") + "\n", new object[]
			{
				triangles,
				num
			});
		}
		if (lodTriangles > num2)
		{
			text += StringUtils.SafeFormat(Locale.Get("ASSET_WARNING_LOD_TRIANGLE") + "\n", new object[]
			{
				lodTriangles,
				num2
			});
		}
		if (textureHeight > AssetWarning.TextureHeight || textureWidth > AssetWarning.TextureWidth)
		{
			text += StringUtils.SafeFormat(Locale.Get("ASSET_WARNING_TEXTURE") + "\n", new object[]
			{
				textureWidth,
				textureHeight,
				AssetWarning.TextureWidth,
				AssetWarning.TextureHeight
			});
		}
		if (lodTextureHeight > AssetWarning.LODTextureHeight || lodTextureWidth > AssetWarning.LODTextureWidth)
		{
			text += StringUtils.SafeFormat(Locale.Get("ASSET_WARNING_LOD_TEXTURE") + "\n", new object[]
			{
				lodTextureWidth,
				lodTextureHeight,
				AssetWarning.LODTextureWidth,
				AssetWarning.LODTextureHeight
			});
		}
		return text;
	}

	public static readonly Dictionary<CustomAssetMetaData.Type, int> Triangles = new Dictionary<CustomAssetMetaData.Type, int>
	{
		{
			CustomAssetMetaData.Type.Building,
			15000
		},
		{
			CustomAssetMetaData.Type.Vehicle,
			7000
		},
		{
			CustomAssetMetaData.Type.Prop,
			1000
		},
		{
			CustomAssetMetaData.Type.Tree,
			1000
		},
		{
			CustomAssetMetaData.Type.Trailer,
			0
		},
		{
			CustomAssetMetaData.Type.Unknown,
			0
		},
		{
			CustomAssetMetaData.Type.SubBuilding,
			0
		},
		{
			CustomAssetMetaData.Type.PropVariation,
			0
		},
		{
			CustomAssetMetaData.Type.Citizen,
			0
		}
	};

	public static Dictionary<CustomAssetMetaData.Type, int> LODTriangles = new Dictionary<CustomAssetMetaData.Type, int>
	{
		{
			CustomAssetMetaData.Type.Building,
			300
		},
		{
			CustomAssetMetaData.Type.Vehicle,
			100
		},
		{
			CustomAssetMetaData.Type.Prop,
			100
		},
		{
			CustomAssetMetaData.Type.Tree,
			0
		},
		{
			CustomAssetMetaData.Type.Trailer,
			0
		},
		{
			CustomAssetMetaData.Type.Unknown,
			0
		},
		{
			CustomAssetMetaData.Type.SubBuilding,
			0
		},
		{
			CustomAssetMetaData.Type.PropVariation,
			0
		},
		{
			CustomAssetMetaData.Type.Citizen,
			0
		}
	};

	public static readonly int TextureHeight = 4096;

	public static readonly int TextureWidth = 4096;

	public static readonly int LODTextureHeight = 512;

	public static readonly int LODTextureWidth = 512;
}
