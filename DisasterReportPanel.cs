﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class DisasterReportPanel : UICustomControl
{
	public bool reportsAvailable
	{
		get
		{
			return this.m_reports.Count > 0;
		}
	}

	private WarningPhasePanel warningPhasePanel
	{
		get
		{
			if (this.m_warningPhasePanel == null)
			{
				this.m_warningPhasePanel = Object.FindObjectOfType<WarningPhasePanel>();
			}
			return this.m_warningPhasePanel;
		}
	}

	private void Awake()
	{
		this.m_messageText = base.Find<UILabel>("MessageText");
		this.m_okayButton = base.Find<UIButton>("OK");
		this.m_listBox = base.Find<UIListBox>("ReportListBox");
		this.m_listBox.add_eventItemClicked(new PropertyChangedEventHandler<int>(this.OnListBoxIndexChanged));
		this.m_reports = new List<DisasterReportPanel.Report>();
		if (!SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			base.get_component().set_isVisible(false);
		}
		else
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.Refresh();
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		base.get_component().set_isVisible(false);
		this.m_reports.Clear();
		this.Refresh();
	}

	public void OnDismissReportButton()
	{
		int num = this.m_listBox.get_selectedIndex();
		if (this.m_reports.Count > 1)
		{
			this.m_reports.RemoveAt(num);
			num--;
			if (num < 0)
			{
				num = 0;
			}
			this.OnListBoxIndexChanged(null, num);
			this.Refresh();
		}
		else
		{
			this.m_reports.Clear();
			this.Hide();
			this.warningPhasePanel.RefreshReportsButtonVisibility();
		}
	}

	public void CheckForUnreportedDisasters(bool disastersAreApproaching)
	{
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			this.CheckForUnreportedDisastersSimulationThread(disastersAreApproaching);
		});
	}

	private void CheckForUnreportedDisastersSimulationThread(bool disastersAreApproaching)
	{
		List<ushort> list = new List<ushort>();
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		for (int i = 1; i < instance.m_disasters.m_size; i++)
		{
			if ((instance.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.UnReported) != DisasterData.Flags.None)
			{
				DisasterInfo disasterInfo = instance.m_disasters.m_buffer[i].Info;
				if (disasterInfo != null)
				{
					if (disasterInfo.m_baseInfo != null)
					{
						disasterInfo = disasterInfo.m_baseInfo;
					}
					list.Add((ushort)i);
				}
			}
		}
		if (list.Count == 0 || (disastersAreApproaching && list.Count < 10))
		{
			return;
		}
		DisasterReportPanel.Report newReport = new DisasterReportPanel.Report();
		newReport.m_expiryDate = Singleton<SimulationManager>.get_instance().m_currentGameTime + new TimeSpan(49, 0, 0, 0);
		newReport.m_title = this.CurrentDateString();
		GeneratedString disasterNames;
		if (list.Count == 1)
		{
			disasterNames = this.GetDisasterName(list[0]);
		}
		else
		{
			List<GeneratedString> list2 = new List<GeneratedString>();
			for (int j = 0; j < list.Count; j++)
			{
				list2.Add(this.GetDisasterName(list[j]));
			}
			disasterNames = new GeneratedString.Join(new GeneratedString.String(", "), list2.ToArray());
		}
		newReport.m_disasterNames = disasterNames;
		foreach (ushort current in list)
		{
			newReport.totalCitizensLost += instance.m_disasters.m_buffer[(int)current].m_casualtiesCount;
			newReport.totalBuildingsDestroyed += (uint)instance.m_disasters.m_buffer[(int)current].m_collapsedCount;
			newReport.totalRoadMetersDestroyed += instance.m_disasters.m_buffer[(int)current].m_destroyedRoadLength;
			newReport.totalTrainTrackMetersDestroyed += instance.m_disasters.m_buffer[(int)current].m_destroyedTrackLength;
			newReport.totalPowerLineMetersDestroyed += instance.m_disasters.m_buffer[(int)current].m_destroyedPowerLength;
			newReport.totalBuildingFires += (uint)instance.m_disasters.m_buffer[(int)current].m_buildingFireCount;
			newReport.totalForestFires += instance.m_disasters.m_buffer[(int)current].m_treeFireCount;
			newReport.totalBuildingsUpgraded += (uint)instance.m_disasters.m_buffer[(int)current].m_upgradedCount;
			DisasterData[] expr_2E6_cp_0 = instance.m_disasters.m_buffer;
			ushort expr_2E6_cp_1 = current;
			expr_2E6_cp_0[(int)expr_2E6_cp_1].m_flags = (expr_2E6_cp_0[(int)expr_2E6_cp_1].m_flags & ~DisasterData.Flags.UnReported);
		}
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.m_reports.Insert(0, newReport);
			this.Refresh();
		});
	}

	private string CurrentDateString()
	{
		DateTime currentGameTime = Singleton<SimulationManager>.get_instance().m_currentGameTime;
		return string.Concat(new string[]
		{
			currentGameTime.Day.ToString("D2"),
			"/",
			currentGameTime.Month.ToString("D2"),
			"/",
			currentGameTime.Year.ToString()
		});
	}

	private GeneratedString GetDisasterName(ushort disasterID)
	{
		string prefabName = this.GetPrefabName(Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)disasterID].Info);
		if (prefabName == "Chirpynado")
		{
			return new GeneratedString.Locale("DISASTERNAME_CHIRPYNADO");
		}
		string a = Locale.Get("DISASTER_TITLE", prefabName);
		string disasterName = Singleton<DisasterManager>.get_instance().GetDisasterName(disasterID);
		if (a == disasterName)
		{
			return new GeneratedString.Locale("DISASTER_TITLE", prefabName);
		}
		return new GeneratedString.Join(new GeneratedString.String(" "), new GeneratedString[]
		{
			new GeneratedString.Locale("DISASTER_TITLE", prefabName),
			new GeneratedString.String(disasterName)
		});
	}

	private string GetPrefabName(DisasterInfo info)
	{
		string result = "Could not get prefab name.";
		if (info.m_prefabDataIndex != -1)
		{
			result = PrefabCollection<DisasterInfo>.PrefabName((uint)info.m_prefabDataIndex);
		}
		return result;
	}

	public bool RemoveOldReports()
	{
		List<int> list = new List<int>();
		for (int i = 0; i < this.m_reports.Count; i++)
		{
			if (this.m_reports[i].m_expiryDate < Singleton<SimulationManager>.get_instance().m_currentGameTime)
			{
				list.Add(i);
			}
		}
		foreach (int current in list)
		{
			this.DeleteReport(current);
		}
		if (list.Count > 0)
		{
			if (this.m_reports.Count > 0 && base.get_component().get_isVisible())
			{
				this.Refresh();
			}
			if (this.m_reports.Count == 0 && base.get_component().get_isVisible())
			{
				this.Hide();
			}
			this.warningPhasePanel.RefreshReportsButtonVisibility();
		}
		return list.Count > 0;
	}

	private void DeleteReport(int reportIndex)
	{
		this.m_reports.RemoveAt(reportIndex);
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && (p.get_keycode() == 27 || p.get_keycode() == 13))
		{
			this.OnCloseButton();
			p.Use();
		}
	}

	private void Refresh()
	{
		string[] array = new string[this.m_reports.Count];
		for (int i = 0; i < this.m_reports.Count; i++)
		{
			array[i] = this.m_reports[i].m_title;
		}
		this.m_listBox.set_items(array);
		if (this.m_reports.Count > 0)
		{
			if (this.m_listBox.get_selectedIndex() < 0)
			{
				this.m_listBox.set_selectedIndex(0);
			}
			this.m_messageText.set_text(this.m_reports[this.m_listBox.get_selectedIndex()].GetLocalizedReport());
		}
		if (this.warningPhasePanel.isOpen)
		{
			this.warningPhasePanel.RefreshReportsButtonVisibility();
		}
	}

	private void OnListBoxIndexChanged(UIComponent comp, int index)
	{
		this.m_messageText.set_text(this.m_reports[index].GetLocalizedReport());
	}

	public void Show()
	{
		this.m_listBox.set_selectedIndex(0);
		this.Refresh();
		base.get_component().set_isVisible(true);
		base.get_component().CenterToParent();
		ValueAnimator.Animate("DisasterReportPanelFadeIn", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f), delegate
		{
			this.m_okayButton.Focus();
		});
	}

	private void Hide()
	{
		ValueAnimator.Animate("DisasterReportPanelFadeOut", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.2f), delegate
		{
			base.get_component().set_isVisible(false);
		});
	}

	public void OnCloseButton()
	{
		this.Hide();
	}

	private const int MAX_DISASTER_REPORT_AGE = 49;

	private const int MAX_UNREPORTED_DISASTERS_COUNT = 10;

	private UILabel m_messageText;

	private UIButton m_okayButton;

	private UIListBox m_listBox;

	private List<DisasterReportPanel.Report> m_reports;

	private WarningPhasePanel m_warningPhasePanel;

	private class Report
	{
		public string GetLocalizedReport()
		{
			string text = StringUtils.SafeFormat(Locale.Get("DISASTERREPORT_AFTERMATH"), this.m_disasterNames.ToString());
			text = text + Environment.NewLine + Environment.NewLine;
			if (this.totalCitizensLost + this.totalBuildingsDestroyed + this.totalRoadMetersDestroyed + this.totalTrainTrackMetersDestroyed + this.totalPowerLineMetersDestroyed + this.totalBuildingFires + this.totalForestFires + this.totalBuildingsUpgraded == 0u)
			{
				text += Locale.Get("DISASTERREPORT_NODAMAGE");
			}
			else
			{
				if (this.totalCitizensLost == 0u)
				{
					text = text + Locale.Get("DISASTERREPORT_NOCASUALTIES") + Environment.NewLine + Environment.NewLine;
				}
				else
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERINFOWINDOW2"),
						": ",
						this.totalCitizensLost.ToString(),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalBuildingsDestroyed > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERINFOWINDOW1"),
						": ",
						this.totalBuildingsDestroyed.ToString(),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalRoadMetersDestroyed > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERINFOWINDOW3"),
						": ",
						this.totalRoadMetersDestroyed.ToString(),
						Locale.Get("DISASTERREPORT_UNIT_METERS"),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalTrainTrackMetersDestroyed > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERREPORT_TRACKSDESTROYED"),
						": ",
						this.totalTrainTrackMetersDestroyed.ToString(),
						Locale.Get("DISASTERREPORT_UNIT_METERS"),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalPowerLineMetersDestroyed > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERREPORT_POWERLINESDESTROYED"),
						": ",
						this.totalPowerLineMetersDestroyed.ToString(),
						Locale.Get("DISASTERREPORT_UNIT_METERS"),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalBuildingFires > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERREPORT_BUILDINGFIRES"),
						": ",
						this.totalBuildingFires.ToString(),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalForestFires > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERREPORT_FORESTFIRES"),
						": ",
						this.totalForestFires.ToString(),
						Environment.NewLine,
						Environment.NewLine
					});
				}
				if (this.totalBuildingsUpgraded > 0u)
				{
					text = string.Concat(new string[]
					{
						text,
						Locale.Get("DISASTERREPORT_BUILDINGSUPGRADED"),
						": ",
						this.totalBuildingsUpgraded.ToString(),
						Environment.NewLine,
						Environment.NewLine
					});
				}
			}
			return text;
		}

		public DateTime m_expiryDate;

		public string m_title;

		public GeneratedString m_disasterNames;

		public uint totalCitizensLost;

		public uint totalBuildingsDestroyed;

		public uint totalRoadMetersDestroyed;

		public uint totalTrainTrackMetersDestroyed;

		public uint totalPowerLineMetersDestroyed;

		public uint totalBuildingFires;

		public uint totalForestFires;

		public uint totalBuildingsUpgraded;
	}
}
