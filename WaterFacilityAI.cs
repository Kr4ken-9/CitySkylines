﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class WaterFacilityAI : PlayerBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_waterConsumption != 0 || this.m_sewageAccumulation != 0)
		{
			throw new PrefabException(this.m_info, "Water facility cannot have water consumption or sewage accumulation");
		}
	}

	public override void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		if (this.m_maxWaterDistance != 0f)
		{
			resource1 = NaturalResourceManager.Resource.Water;
			position1 = data.CalculatePosition(this.m_waterLocationOffset);
			radius1 = this.m_maxWaterDistance;
		}
		else
		{
			resource1 = NaturalResourceManager.Resource.None;
			position1 = data.m_position;
			radius1 = 0f;
		}
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Water)
		{
			if ((data.m_flags & Building.Flags.Active) == Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
			}
			if (this.m_waterIntake != 0 || this.m_waterOutlet != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			if (this.m_sewageOutlet != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColorB;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				int noiseAccumulation = this.m_noiseAccumulation;
				return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
			}
			if (infoMode != InfoManager.InfoMode.Pollution)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (this.m_waterIntake != 0 || this.m_waterOutlet != 0)
			{
				float t = Mathf.Clamp01((float)data.m_waterPollution * 0.0117647061f);
				return ColorUtils.LinearLerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor, t);
			}
			if (this.m_sewageOutlet != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return num * (this.m_waterIntake - this.m_waterOutlet - this.m_sewageOutlet) / 100;
	}

	public static void GetWaterFacilityNumbers(out int activeIntakes, out int inactiveIntakes, out int activeOutlets, out int inactiveOutlets, out int activeTreatmentPlants, out int inactiveTreatmentPlants)
	{
		activeIntakes = 0;
		inactiveIntakes = 0;
		activeOutlets = 0;
		inactiveOutlets = 0;
		activeTreatmentPlants = 0;
		inactiveTreatmentPlants = 0;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(ItemClass.Service.Water);
		int size = serviceBuildings.m_size;
		ushort[] buffer = serviceBuildings.m_buffer;
		if (buffer != null && size <= buffer.Length)
		{
			for (int i = 0; i < size; i++)
			{
				ushort num = buffer[i];
				if (num != 0)
				{
					bool flag = (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None;
					WaterFacilityAI waterFacilityAI = instance.m_buildings.m_buffer[(int)num].Info.m_buildingAI as WaterFacilityAI;
					if (waterFacilityAI != null)
					{
						if (waterFacilityAI.m_waterIntake == 0 || waterFacilityAI.m_waterOutlet == 0 || waterFacilityAI.m_waterStorage == 0)
						{
							if (waterFacilityAI.m_sewageOutlet == 0 || waterFacilityAI.m_sewageStorage == 0)
							{
								if (waterFacilityAI.m_waterIntake != 0)
								{
									if (flag)
									{
										activeIntakes++;
									}
									else
									{
										inactiveIntakes++;
									}
								}
								else if (waterFacilityAI.m_waterOutlet == 0)
								{
									if (waterFacilityAI.m_sewageOutlet != 0)
									{
										if (waterFacilityAI.m_outletPollution == 100)
										{
											if (flag)
											{
												activeOutlets++;
											}
											else
											{
												inactiveOutlets++;
											}
										}
										else if (flag)
										{
											activeTreatmentPlants++;
										}
										else
										{
											inactiveTreatmentPlants++;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public override string GetConstructionInfo(int productionRate)
	{
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return base.GetConstructionInfo(productionRate);
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return base.GetConstructionInfo(productionRate);
		}
		if (this.m_waterIntake != 0)
		{
			return StringUtils.SafeFormat(Locale.Get("TOOL_WATER_ESTIMATE"), this.m_waterIntake * productionRate * 16 / 100);
		}
		if ((this.m_waterOutlet != 0 || this.m_sewageOutlet != 0) && productionRate == 0)
		{
			return Locale.Get("TOOL_SHORELINE_RECOMMENDED");
		}
		return base.GetConstructionInfo(productionRate);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Water;
		if (this.m_useGroundWater)
		{
			subMode = InfoManager.SubInfoMode.WindPower;
		}
		else
		{
			subMode = InfoManager.SubInfoMode.WaterPower;
		}
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainWater, position, 1.5f);
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseWater, position, 1.5f);
			if (this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.FloodWater, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.FloodWater && this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.FloodWater && this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			int num = (int)(data.m_customBuffer2 * 1000 + data.m_sewageBuffer);
			amountDelta = Mathf.Max(amountDelta, 0);
			num += amountDelta;
			data.m_customBuffer2 = (ushort)(num / 1000);
			data.m_sewageBuffer = (ushort)(num - (int)(data.m_customBuffer2 * 1000));
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			int num = (int)data.m_productionRate;
			if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
			{
				num = 0;
			}
			else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
			{
				num = Mathf.Min(num, 50);
			}
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			num = PlayerBuildingAI.GetProductionRate(num, budget);
			return (float)num * this.m_vehicleRadius * 0.01f;
		}
		return base.GetCurrentRange(buildingID, ref data);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			productionRate = 100;
		}
		else if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			productionRate = 100;
		}
		else if (this.m_useGroundWater)
		{
			productionRate = 100;
		}
		else
		{
			Vector3 vector = Building.CalculatePosition(position, angle, this.m_waterLocationOffset);
			vector.y = 0f;
			Vector3 vector2;
			Vector3 vector3;
			bool flag;
			if (BuildingTool.SnapToCanal(position, out vector2, out vector3, out flag, 40f, true))
			{
				vector = vector2;
				vector.y = 0f;
				if (this.m_waterIntake == 0 || Singleton<TerrainManager>.get_instance().GetClosestWaterPos(ref vector, this.m_maxWaterDistance * 0.5f))
				{
					productionRate = 100;
				}
				else
				{
					productionRate = 0;
				}
				angle = Mathf.Atan2(vector3.x, -vector3.z);
				vector2 -= vector3 * this.m_waterLocationOffset.z;
				position.x = vector2.x;
				position.z = vector2.z;
			}
			else if (Singleton<TerrainManager>.get_instance().GetClosestWaterPos(ref vector, this.m_maxWaterDistance * 0.5f))
			{
				productionRate = 100;
			}
			else
			{
				productionRate = 0;
			}
		}
		return result;
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return base.GetWaterStructureCollisionRange(out min, out max);
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return base.GetWaterStructureCollisionRange(out min, out max);
		}
		if (this.m_useGroundWater)
		{
			return base.GetWaterStructureCollisionRange(out min, out max);
		}
		float num = Mathf.Max(0f, (float)this.m_info.m_cellLength * 4f + this.m_waterLocationOffset.z - 37f);
		min = 0f;
		max = num / Mathf.Max(num + 2f, (float)this.m_info.m_cellLength * 8f);
		return true;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			bool flag = false;
			if (buildingData.m_netNode != 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				for (int i = 0; i < 8; i++)
				{
					if (instance.m_nodes.m_buffer[(int)buildingData.m_netNode].GetSegment(i) != 0)
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				finalProductionRate = 0;
			}
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(buildingData.m_position);
			if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
			{
				int num = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_waterBuffer);
				int num2 = this.m_waterOutlet * finalProductionRate / 100;
				int num3 = this.m_waterIntake * finalProductionRate / 100 + num2;
				if (num3 != 0)
				{
					int num4 = this.m_waterStorage + num3 - num;
					if (num4 > 0)
					{
						int num5 = Singleton<WaterManager>.get_instance().TryFetchWater(buildingData.m_netNode, num3, num4, ref buildingData.m_waterPollution);
						num += num5;
					}
				}
				if (num2 != 0)
				{
					int num6 = Mathf.Min(num, num2);
					if (num6 > 0)
					{
						int num7 = Singleton<WaterManager>.get_instance().TryDumpWater(buildingData.m_netNode, num2, num6, buildingData.m_waterPollution);
						num -= num7;
					}
				}
				District[] expr_15A_cp_0_cp_0 = instance2.m_districts.m_buffer;
				byte expr_15A_cp_0_cp_1 = district;
				expr_15A_cp_0_cp_0[(int)expr_15A_cp_0_cp_1].m_productionData.m_tempWaterStorageAmount = expr_15A_cp_0_cp_0[(int)expr_15A_cp_0_cp_1].m_productionData.m_tempWaterStorageAmount + (uint)Mathf.Min(num, this.m_waterStorage);
				District[] expr_18A_cp_0_cp_0 = instance2.m_districts.m_buffer;
				byte expr_18A_cp_0_cp_1 = district;
				expr_18A_cp_0_cp_0[(int)expr_18A_cp_0_cp_1].m_productionData.m_tempWaterStorageCapacity = expr_18A_cp_0_cp_0[(int)expr_18A_cp_0_cp_1].m_productionData.m_tempWaterStorageCapacity + (uint)this.m_waterStorage;
				buildingData.m_customBuffer1 = (ushort)(num / 1000);
				buildingData.m_waterBuffer = (ushort)(num - (int)(buildingData.m_customBuffer1 * 1000));
			}
			else if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
			{
				int num8 = (int)(buildingData.m_customBuffer2 * 1000 + buildingData.m_sewageBuffer);
				int num9 = this.m_sewageOutlet * finalProductionRate / 100;
				if (num9 != 0)
				{
					int num10 = Mathf.Min(num8, num9);
					if (num10 > 0)
					{
						int num11 = Singleton<WaterManager>.get_instance().TryDumpSewage(buildingData.m_netNode, num9, num10);
						num8 -= num11;
					}
				}
				buildingData.m_customBuffer2 = (ushort)(num8 / 1000);
				buildingData.m_sewageBuffer = (ushort)(num8 - (int)(buildingData.m_customBuffer2 * 1000));
				int num12 = 0;
				int num13 = 0;
				int num14 = 0;
				int num15 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.FloodWater, ref num12, ref num13, ref num14, ref num15);
				int num16 = (finalProductionRate * this.m_pumpingVehicles + 99) / 100;
				if (num12 < num16)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = (num16 - num12) * 3 / num16;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = num16 - num12;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.FloodWater, offer);
				}
			}
			else
			{
				int num17 = this.m_waterIntake * finalProductionRate / 100;
				if (num17 != 0)
				{
					int num18 = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_waterBuffer);
					int num19 = num17 * 2 - num18;
					if (num19 > 0)
					{
						int num20;
						if (this.m_useGroundWater)
						{
							num20 = num19;
							Vector3 pos = buildingData.CalculatePosition(this.m_waterLocationOffset);
							Singleton<NaturalResourceManager>.get_instance().CheckPollution(pos, out buildingData.m_waterPollution);
						}
						else
						{
							num20 = this.HandleWaterSource(buildingID, ref buildingData, false, 0, num17, num19, this.m_maxWaterDistance);
						}
						num18 += num20;
						if (num20 == 0)
						{
							finalProductionRate = 0;
						}
					}
					else
					{
						finalProductionRate = 0;
					}
					int num21 = Mathf.Min(num18, num17);
					if (num21 > 0)
					{
						int num22 = Singleton<WaterManager>.get_instance().TryDumpWater(buildingData.m_netNode, num17, num21, buildingData.m_waterPollution);
						num18 -= num22;
					}
					if (this.m_useGroundWater || buildingData.m_waterSource != 0)
					{
						District[] expr_3F4_cp_0_cp_0 = instance2.m_districts.m_buffer;
						byte expr_3F4_cp_0_cp_1 = district;
						expr_3F4_cp_0_cp_0[(int)expr_3F4_cp_0_cp_1].m_productionData.m_tempWaterCapacity = expr_3F4_cp_0_cp_0[(int)expr_3F4_cp_0_cp_1].m_productionData.m_tempWaterCapacity + (uint)num17;
					}
					buildingData.m_customBuffer1 = (ushort)(num18 / 1000);
					buildingData.m_waterBuffer = (ushort)(num18 - (int)(buildingData.m_customBuffer1 * 1000));
				}
				int num23 = this.m_waterOutlet * finalProductionRate / 100;
				if (num23 != 0)
				{
					int num24 = (int)(buildingData.m_customBuffer1 * 1000 + buildingData.m_waterBuffer);
					int num25 = num23 * 4 - num24;
					if (num25 > 0)
					{
						int num26 = Singleton<WaterManager>.get_instance().TryFetchWater(buildingData.m_netNode, num23, num25, ref buildingData.m_waterPollution);
						num24 += num26;
					}
					int num27 = Mathf.Min(num24 - num23 * 2, num23);
					if (num27 > 0)
					{
						int num28;
						if (this.m_useGroundWater)
						{
							num28 = num27;
							Vector3 position = buildingData.CalculatePosition(this.m_waterLocationOffset);
							int num29 = (num27 * (int)buildingData.m_waterPollution + 99) / 100;
							Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num29, num29, position, this.m_maxWaterDistance);
						}
						else
						{
							num28 = this.HandleWaterSource(buildingID, ref buildingData, true, (int)buildingData.m_waterPollution, num23, num27, this.m_maxWaterDistance);
						}
						num24 -= num28;
						if (num28 == 0)
						{
							finalProductionRate = 0;
						}
					}
					else
					{
						finalProductionRate = 0;
					}
					buildingData.m_customBuffer1 = (ushort)(num24 / 1000);
					buildingData.m_waterBuffer = (ushort)(num24 - (int)(buildingData.m_customBuffer1 * 1000));
				}
				int num30 = this.m_sewageOutlet * finalProductionRate / 100;
				if (num30 != 0)
				{
					int num31 = (int)(buildingData.m_customBuffer2 * 1000 + buildingData.m_sewageBuffer);
					int num32 = num30 * 4 - num31;
					if (num32 > 0)
					{
						int num33 = Singleton<WaterManager>.get_instance().TryFetchSewage(buildingData.m_netNode, num30, num32);
						num31 += num33;
					}
					int num34 = Mathf.Min(num31 - num30 * 2, num30);
					if (num34 > 0)
					{
						int num35;
						if (this.m_useGroundWater)
						{
							num35 = num34;
							Vector3 position2 = buildingData.CalculatePosition(this.m_waterLocationOffset);
							int num36 = (num34 * this.m_outletPollution + 99) / 100;
							Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num36, num36, position2, this.m_maxWaterDistance);
						}
						else
						{
							num35 = this.HandleWaterSource(buildingID, ref buildingData, true, this.m_outletPollution, num30, num34, this.m_maxWaterDistance);
						}
						num31 -= num35;
						if (num35 == 0)
						{
							finalProductionRate = 0;
						}
					}
					else
					{
						finalProductionRate = 0;
					}
					if (this.m_useGroundWater || buildingData.m_waterSource != 0)
					{
						District[] expr_65F_cp_0_cp_0 = instance2.m_districts.m_buffer;
						byte expr_65F_cp_0_cp_1 = district;
						expr_65F_cp_0_cp_0[(int)expr_65F_cp_0_cp_1].m_productionData.m_tempSewageCapacity = expr_65F_cp_0_cp_0[(int)expr_65F_cp_0_cp_1].m_productionData.m_tempSewageCapacity + (uint)num30;
					}
					buildingData.m_customBuffer2 = (ushort)(num31 / 1000);
					buildingData.m_sewageBuffer = (ushort)(num31 - (int)(buildingData.m_customBuffer2 * 1000));
				}
			}
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
			int num37 = finalProductionRate * this.m_noiseAccumulation / 100;
			if (num37 != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num37, buildingData.m_position, this.m_noiseRadius);
			}
			if ((buildingData.m_flags & Building.Flags.Loading1) != Building.Flags.None)
			{
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<BuildingManager>.get_instance().m_waterOutletOnLand.Activate(properties.m_waterOutletOnLand, buildingID);
				}
			}
		}
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnGround || this.m_info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			onlyCollapse = true;
			return true;
		}
		onlyCollapse = false;
		return false;
	}

	private int HandleWaterSource(ushort buildingID, ref Building data, bool output, int pollution, int rate, int max, float radius)
	{
		uint num = (uint)(Mathf.Min(rate, max) >> 1);
		if (num == 0u)
		{
			return 0;
		}
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		if (data.m_waterSource != 0)
		{
			bool flag = false;
			WaterSource sourceData = waterSimulation.LockWaterSource(data.m_waterSource);
			try
			{
				if (output)
				{
					uint num2 = num;
					if (num2 < sourceData.m_water >> 3)
					{
						num2 = sourceData.m_water >> 3;
					}
					sourceData.m_outputRate = num2 + 3u >> 2;
					sourceData.m_water += num;
					sourceData.m_pollution += num * (uint)pollution / (uint)Mathf.Max(100, waterSimulation.GetPollutionDisposeRate() * 100);
				}
				else
				{
					uint num3 = num;
					if (num3 < sourceData.m_water >> 3)
					{
						num3 >>= 1;
					}
					sourceData.m_inputRate = num3 + 3u >> 2;
					if (num > sourceData.m_water)
					{
						num = sourceData.m_water;
					}
					if (sourceData.m_water != 0u)
					{
						data.m_waterPollution = (byte)Mathf.Min(255f, 255u * sourceData.m_pollution / sourceData.m_water);
						sourceData.m_pollution = (uint)((ulong)sourceData.m_pollution * (ulong)(sourceData.m_water - num) / (ulong)sourceData.m_water);
					}
					else
					{
						data.m_waterPollution = 0;
					}
					sourceData.m_water -= num;
					Vector3 vector = sourceData.m_inputPosition;
					if (!instance.HasWater(VectorUtils.XZ(vector)))
					{
						vector = data.CalculatePosition(this.m_waterLocationOffset);
						vector.y = 0f;
						if (instance.GetClosestWaterPos(ref vector, radius))
						{
							sourceData.m_inputPosition = vector;
							sourceData.m_outputPosition = vector;
						}
						else
						{
							flag = true;
						}
					}
				}
			}
			finally
			{
				waterSimulation.UnlockWaterSource(data.m_waterSource, sourceData);
			}
			if (flag)
			{
				waterSimulation.ReleaseWaterSource(data.m_waterSource);
				data.m_waterSource = 0;
			}
		}
		else
		{
			bool flag2 = false;
			Vector3 vector2 = data.CalculatePosition(this.m_waterLocationOffset);
			Vector3 vector3;
			Vector3 vector4;
			bool flag3;
			if (BuildingTool.SnapToCanal(vector2, out vector3, out vector4, out flag3, 0f, true))
			{
				vector2 = vector3;
				flag2 = true;
			}
			if (!flag2)
			{
				vector2.y = 0f;
				flag2 = instance.GetClosestWaterPos(ref vector2, radius);
			}
			if (flag2 || output)
			{
				WaterSource sourceData2 = default(WaterSource);
				sourceData2.m_type = 2;
				sourceData2.m_inputPosition = vector2;
				sourceData2.m_outputPosition = vector2;
				if (output)
				{
					sourceData2.m_outputRate = num + 3u >> 2;
					sourceData2.m_water = num;
					sourceData2.m_pollution = num * (uint)pollution / (uint)Mathf.Max(100, waterSimulation.GetPollutionDisposeRate() * 100);
					if (!waterSimulation.CreateWaterSource(out data.m_waterSource, sourceData2))
					{
						num = 0u;
					}
					if (!flag2)
					{
						data.m_flags |= Building.Flags.Loading1;
					}
					else
					{
						data.m_flags &= ~Building.Flags.Loading1;
					}
				}
				else
				{
					sourceData2.m_inputRate = num + 3u >> 2;
					waterSimulation.CreateWaterSource(out data.m_waterSource, sourceData2);
					num = 0u;
				}
			}
			else
			{
				num = 0u;
			}
		}
		return (int)((int)num << 1);
	}

	public override void PlacementFailed()
	{
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return;
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return;
		}
		if (!this.m_useGroundWater)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<BuildingManager>.get_instance().m_buildNextToWater.Activate(properties.m_buildNextToWater);
			}
		}
	}

	public override void PlacementSucceeded()
	{
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return;
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return;
		}
		if (!this.m_useGroundWater)
		{
			GenericGuide buildNextToWater = Singleton<BuildingManager>.get_instance().m_buildNextToWater;
			if (buildNextToWater != null)
			{
				buildNextToWater.Deactivate();
			}
		}
		if (this.m_waterIntake != 0)
		{
			BuildingTypeGuide waterPumpMissingGuide = Singleton<WaterManager>.get_instance().m_waterPumpMissingGuide;
			if (waterPumpMissingGuide != null)
			{
				waterPumpMissingGuide.Deactivate();
			}
		}
		if (this.m_sewageOutlet != 0)
		{
			BuildingTypeGuide drainPipeMissingGuide = Singleton<WaterManager>.get_instance().m_drainPipeMissingGuide;
			if (drainPipeMissingGuide != null)
			{
				drainPipeMissingGuide.Deactivate();
			}
		}
	}

	public override void UpdateGuide(GuideController guideController)
	{
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return;
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return;
		}
		if (this.m_waterIntake != 0)
		{
			BuildingTypeGuide waterPumpMissingGuide = Singleton<WaterManager>.get_instance().m_waterPumpMissingGuide;
			if (waterPumpMissingGuide != null)
			{
				int waterCapacity = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterCapacity();
				int sewageCapacity = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetSewageCapacity();
				if (waterCapacity == 0 && sewageCapacity != 0)
				{
					waterPumpMissingGuide.Activate(guideController.m_waterPumpMissing, this.m_info);
				}
				else
				{
					waterPumpMissingGuide.Deactivate();
				}
			}
		}
		if (this.m_sewageOutlet != 0)
		{
			BuildingTypeGuide drainPipeMissingGuide = Singleton<WaterManager>.get_instance().m_drainPipeMissingGuide;
			if (drainPipeMissingGuide != null)
			{
				int waterCapacity2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterCapacity();
				int sewageCapacity2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetSewageCapacity();
				if (waterCapacity2 != 0 && sewageCapacity2 == 0)
				{
					drainPipeMissingGuide.Activate(guideController.m_drainPipeMissing, this.m_info);
				}
				else
				{
					drainPipeMissingGuide.Deactivate();
				}
			}
		}
		base.UpdateGuide(guideController);
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				text,
				LocaleFormatter.Info2,
				LocaleFormatter.FormatGeneric("AINFO_WATER_STORAGE_CAPACITY", new object[]
				{
					this.m_waterStorage
				}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AINFO_WATER_IO_CAPACITY", new object[]
				{
					this.m_waterIntake * 16
				})
			}));
		}
		if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				text,
				LocaleFormatter.Info2,
				LocaleFormatter.FormatGeneric("AIINFO_VEHICLE_CAPACITY", new object[]
				{
					this.m_pumpingVehicles
				}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AINFO_WATER_STORAGE_CAPACITY", new object[]
				{
					this.m_sewageStorage
				})
			}));
		}
		if (this.m_waterIntake > 0)
		{
			return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				text,
				LocaleFormatter.Info2,
				LocaleFormatter.FormatGeneric("AIINFO_WATER_INTAKE", new object[]
				{
					this.m_waterIntake * 16
				})
			}));
		}
		if (this.m_waterOutlet > 0)
		{
			return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				text,
				LocaleFormatter.Info2,
				LocaleFormatter.FormatGeneric("AIINFO_WATER_OUTLET", new object[]
				{
					this.m_waterOutlet * 16
				})
			}));
		}
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_WATER_OUTLET", new object[]
			{
				this.m_sewageOutlet * 16
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string text = string.Empty;
		if (this.m_waterIntake != 0 && this.m_waterOutlet != 0 && this.m_waterStorage != 0)
		{
			int num = (int)(data.m_customBuffer1 * 1000 + data.m_waterBuffer);
			if (num != 0)
			{
				num = Mathf.Clamp((int)((long)num * 100L / (long)this.m_waterStorage), 1, 100);
			}
			text += LocaleFormatter.FormatGeneric("AINFO_WATER_STORAGE_STATUS", new object[]
			{
				num
			});
		}
		else if (this.m_sewageOutlet != 0 && this.m_sewageStorage != 0 && this.m_pumpingVehicles != 0)
		{
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
			int num2 = (productionRate * this.m_pumpingVehicles + 99) / 100;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.FloodWater, ref num3, ref num4, ref num5, ref num6);
			int num7 = (int)(data.m_customBuffer2 * 1000 + data.m_sewageBuffer);
			if (num7 != 0)
			{
				num7 = Mathf.Clamp((int)((long)num7 * 100L / (long)this.m_sewageStorage), 1, 100);
			}
			text = text + LocaleFormatter.FormatGeneric("AIINFO_VEHICLES", new object[]
			{
				num3,
				num2
			}) + Environment.NewLine;
			text += LocaleFormatter.FormatGeneric("AINFO_WATER_STORAGE_STATUS", new object[]
			{
				num7
			});
		}
		else if (this.m_waterIntake > 0)
		{
			int num8 = this.GetWaterRate(buildingID, ref data) * 16;
			text += LocaleFormatter.FormatGeneric("AIINFO_WATER_INTAKE", new object[]
			{
				num8
			});
		}
		else if (this.m_waterOutlet > 0)
		{
			int num9 = -this.GetWaterRate(buildingID, ref data) * 16;
			text += LocaleFormatter.FormatGeneric("AIINFO_WATER_OUTLET", new object[]
			{
				num9
			});
		}
		else
		{
			int num10 = -this.GetWaterRate(buildingID, ref data) * 16;
			text += LocaleFormatter.FormatGeneric("AIINFO_WATER_OUTLET", new object[]
			{
				num10
			});
		}
		return text;
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 10;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 10;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 10;

	[CustomizableProperty("Water Intake", "Water")]
	public int m_waterIntake = 1000;

	[CustomizableProperty("Water Outlet", "Water")]
	public int m_waterOutlet;

	[CustomizableProperty("Sewage Outlet", "Water")]
	public int m_sewageOutlet = 1000;

	[CustomizableProperty("Water Storage", "Water")]
	public int m_waterStorage;

	[CustomizableProperty("Sewage Storage", "Water")]
	public int m_sewageStorage;

	[CustomizableProperty("Pumping Vehicles", "Water")]
	public int m_pumpingVehicles;

	[CustomizableProperty("Vehicle Radius", "Water")]
	public float m_vehicleRadius = 2000f;

	public Vector3 m_waterLocationOffset = Vector3.get_zero();

	[CustomizableProperty("Max Water Distance", "Water")]
	public float m_maxWaterDistance = 100f;

	[CustomizableProperty("Use Ground Water", "Water")]
	public bool m_useGroundWater;

	[CustomizableProperty("Outlet Pollution", "Pollution")]
	public int m_outletPollution = 100;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation = 50;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;
}
