﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.UI;
using UnityEngine;

public static class PackageHelper
{
	public static bool IsDemoModeSave(Package.Asset asset)
	{
		return !(asset == null) && !(asset.get_package() == null) && !string.IsNullOrEmpty(asset.get_package().get_packagePath()) && asset.get_package().get_packagePath().StartsWith(Path.Combine(DataLocation.get_gameContentPath(), "Maps"));
	}

	public static bool CustomSerialize(Package p, object o, PackageWriter w)
	{
		Type type = o.GetType();
		if (type == typeof(TransportInfo))
		{
			TransportInfo transportInfo = (TransportInfo)o;
			w.Write(transportInfo.get_name());
			return true;
		}
		if (type == typeof(BuildingInfo.PathInfo))
		{
			BuildingInfo.PathInfo pathInfo = (BuildingInfo.PathInfo)o;
			w.Write((!(pathInfo.m_netInfo != null)) ? string.Empty : pathInfo.m_netInfo.get_name());
			w.Write(pathInfo.m_nodes);
			w.Write(pathInfo.m_curveTargets);
			w.Write(pathInfo.m_invertSegments);
			w.Write(pathInfo.m_maxSnapDistance);
			w.Write(pathInfo.m_forbidLaneConnection);
			w.Write((int[])pathInfo.m_trafficLights);
			w.Write(pathInfo.m_yieldSigns);
			return true;
		}
		if (type == typeof(BuildingInfo.Prop))
		{
			BuildingInfo.Prop prop = (BuildingInfo.Prop)o;
			w.Write((!(prop.m_prop != null)) ? string.Empty : prop.m_prop.get_name());
			w.Write((!(prop.m_tree != null)) ? string.Empty : prop.m_tree.get_name());
			w.Write(prop.m_position);
			w.Write(prop.m_angle);
			w.Write(prop.m_probability);
			w.Write(prop.m_fixedHeight);
			return true;
		}
		if (type == typeof(PropInfo.Variation))
		{
			PropInfo.Variation variation = (PropInfo.Variation)o;
			w.Write((!(variation.m_prop != null)) ? string.Empty : PackageHelper.StripName(variation.m_prop.get_name()));
			w.Write(variation.m_probability);
			return true;
		}
		if (type == typeof(TreeInfo.Variation))
		{
			TreeInfo.Variation variation2 = (TreeInfo.Variation)o;
			w.Write((!(variation2.m_tree != null)) ? string.Empty : PackageHelper.StripName(variation2.m_tree.get_name()));
			w.Write(variation2.m_probability);
			return true;
		}
		if (type == typeof(MessageInfo))
		{
			MessageInfo messageInfo = (MessageInfo)o;
			w.Write((messageInfo.m_firstID1 == null) ? string.Empty : messageInfo.m_firstID1);
			w.Write((messageInfo.m_firstID2 == null) ? string.Empty : messageInfo.m_firstID2);
			w.Write((messageInfo.m_repeatID1 == null) ? string.Empty : messageInfo.m_repeatID1);
			w.Write((messageInfo.m_repeatID2 == null) ? string.Empty : messageInfo.m_repeatID2);
			return true;
		}
		if (type == typeof(ItemClass))
		{
			ItemClass itemClass = (ItemClass)o;
			w.Write(itemClass.get_name());
			return true;
		}
		if (type == typeof(AudioInfo))
		{
			return true;
		}
		if (type == typeof(ModInfo))
		{
			ModInfo modInfo = (ModInfo)o;
			w.Write(modInfo.modName);
			w.Write(modInfo.modWorkshopID);
			return true;
		}
		if (type == typeof(DisasterProperties.DisasterSettings))
		{
			DisasterProperties.DisasterSettings disasterSettings = (DisasterProperties.DisasterSettings)o;
			w.Write(disasterSettings.m_disasterName);
			w.Write(disasterSettings.m_randomProbability);
			return true;
		}
		if (type == typeof(UITextureAtlas))
		{
			UITextureAtlas uITextureAtlas = (UITextureAtlas)o;
			w.Write(uITextureAtlas.get_name());
			Texture2D texture2D = uITextureAtlas.get_material().get_mainTexture() as Texture2D;
			byte[] array = texture2D.EncodeToPNG();
			w.Write(array.Length);
			w.Write(array);
			w.Write(uITextureAtlas.get_material().get_shader().get_name());
			w.Write(uITextureAtlas.get_padding());
			List<UITextureAtlas.SpriteInfo> sprites = uITextureAtlas.get_sprites();
			w.Write(sprites.Count);
			for (int i = 0; i < sprites.Count; i++)
			{
				w.Write(sprites[i].get_region().get_x());
				w.Write(sprites[i].get_region().get_y());
				w.Write(sprites[i].get_region().get_width());
				w.Write(sprites[i].get_region().get_height());
				w.Write(sprites[i].get_name());
			}
			return true;
		}
		if (type == typeof(VehicleInfo.Effect))
		{
			VehicleInfo.Effect effect = (VehicleInfo.Effect)o;
			w.Write(effect.m_effect.get_name());
			w.Write((int)effect.m_parkedFlagsForbidden);
			w.Write((int)effect.m_parkedFlagsRequired);
			w.Write((int)effect.m_vehicleFlagsForbidden);
			w.Write((int)effect.m_vehicleFlagsRequired);
			return true;
		}
		if (type == typeof(VehicleInfo.VehicleDoor))
		{
			VehicleInfo.VehicleDoor vehicleDoor = (VehicleInfo.VehicleDoor)o;
			w.Write((int)vehicleDoor.m_type);
			w.Write(vehicleDoor.m_location);
			return true;
		}
		if (type == typeof(VehicleInfo.VehicleTrailer))
		{
			VehicleInfo.VehicleTrailer vehicleTrailer = (VehicleInfo.VehicleTrailer)o;
			w.Write(PackageHelper.StripName(vehicleTrailer.m_info.get_name()));
			w.Write(vehicleTrailer.m_probability);
			w.Write(vehicleTrailer.m_invertProbability);
			return true;
		}
		if (type == typeof(ManualMilestone) || type == typeof(CombinedMilestone))
		{
			MilestoneInfo milestoneInfo = (MilestoneInfo)o;
			w.Write(milestoneInfo.get_name());
			return true;
		}
		if (type == typeof(TransportInfo))
		{
			TransportInfo transportInfo2 = (TransportInfo)o;
			w.Write(transportInfo2.get_name());
			return true;
		}
		if (type == typeof(NetInfo))
		{
			NetInfo netInfo = (NetInfo)o;
			w.Write(netInfo.get_name());
			return true;
		}
		if (type == typeof(BuildingInfo.MeshInfo))
		{
			BuildingInfo.MeshInfo meshInfo = (BuildingInfo.MeshInfo)o;
			if (meshInfo.m_subInfo != null)
			{
				GameObject gameObject = meshInfo.m_subInfo.get_gameObject();
				Package.Asset asset = p.AddAsset(gameObject.get_name(), gameObject, false);
				w.Write(asset.get_checksum());
			}
			else
			{
				w.Write(string.Empty);
			}
			PackageHelper.CustomSerialize(p, meshInfo.m_flagsForbidden, w);
			PackageHelper.CustomSerialize(p, meshInfo.m_flagsRequired, w);
			w.Write(meshInfo.m_position);
			w.Write(meshInfo.m_angle);
			return true;
		}
		if (type == typeof(Building.Flags))
		{
			Building.Flags value = (Building.Flags)o;
			w.Write((int)value);
			return true;
		}
		if (type == typeof(VehicleInfo.MeshInfo))
		{
			VehicleInfo.MeshInfo meshInfo2 = (VehicleInfo.MeshInfo)o;
			if (meshInfo2.m_subInfo != null)
			{
				GameObject gameObject2 = meshInfo2.m_subInfo.get_gameObject();
				Package.Asset asset2 = p.AddAsset(gameObject2.get_name(), gameObject2, false);
				w.Write(asset2.get_checksum());
			}
			else
			{
				w.Write(string.Empty);
			}
			PackageHelper.CustomSerialize(p, meshInfo2.m_vehicleFlagsForbidden, w);
			PackageHelper.CustomSerialize(p, meshInfo2.m_vehicleFlagsRequired, w);
			PackageHelper.CustomSerialize(p, meshInfo2.m_parkedFlagsForbidden, w);
			PackageHelper.CustomSerialize(p, meshInfo2.m_parkedFlagsRequired, w);
			return true;
		}
		if (type == typeof(Vehicle.Flags))
		{
			Vehicle.Flags value2 = (Vehicle.Flags)o;
			w.Write((int)value2);
			return true;
		}
		if (type == typeof(VehicleParked.Flags))
		{
			VehicleParked.Flags value3 = (VehicleParked.Flags)o;
			w.Write((int)value3);
			return true;
		}
		if (type == typeof(DepotAI.SpawnPoint))
		{
			DepotAI.SpawnPoint spawnPoint = (DepotAI.SpawnPoint)o;
			w.Write(spawnPoint.m_position);
			w.Write(spawnPoint.m_target);
			return true;
		}
		if (type == typeof(PropInfo.Effect))
		{
			PropInfo.Effect effect2 = (PropInfo.Effect)o;
			w.Write(effect2.m_effect.get_name());
			w.Write(effect2.m_position);
			w.Write(effect2.m_direction);
			return true;
		}
		if (type == typeof(BuildingInfo.SubInfo))
		{
			BuildingInfo.SubInfo subInfo = (BuildingInfo.SubInfo)o;
			w.Write(PackageHelper.StripName(subInfo.m_buildingInfo.get_name()));
			w.Write(subInfo.m_position);
			w.Write(subInfo.m_angle);
			w.Write(subInfo.m_fixedHeight);
			return true;
		}
		if (type == typeof(PropInfo.ParkingSpace))
		{
			PropInfo.ParkingSpace parkingSpace = (PropInfo.ParkingSpace)o;
			w.Write(parkingSpace.m_position);
			w.Write(parkingSpace.m_direction);
			w.Write(parkingSpace.m_size);
			return true;
		}
		if (type == typeof(PropInfo.SpecialPlace))
		{
			PropInfo.SpecialPlace specialPlace = (PropInfo.SpecialPlace)o;
			w.Write((int)specialPlace.m_specialFlags);
			w.Write(specialPlace.m_position);
			w.Write(specialPlace.m_direction);
			return true;
		}
		if (type == typeof(NetInfo.Lane))
		{
			NetInfo.Lane lane = (NetInfo.Lane)o;
			w.Write(lane.m_position);
			w.Write(lane.m_width);
			w.Write(lane.m_verticalOffset);
			w.Write(lane.m_stopOffset);
			w.Write(lane.m_speedLimit);
			w.Write((int)lane.m_direction);
			w.Write((int)lane.m_laneType);
			w.Write((int)lane.m_vehicleType);
			w.Write((int)lane.m_stopType);
			PackageHelper.CustomSerialize(p, lane.m_laneProps, w);
			w.Write(lane.m_allowConnect);
			w.Write(lane.m_useTerrainHeight);
			w.Write(lane.m_centerPlatform);
			w.Write(lane.m_elevated);
			return true;
		}
		if (type == typeof(NetLaneProps))
		{
			NetLaneProps netLaneProps = (NetLaneProps)o;
			int num = netLaneProps.m_props.Length;
			w.Write(num);
			for (int j = 0; j < num; j++)
			{
				PackageHelper.CustomSerialize(p, netLaneProps.m_props[j], w);
			}
			return true;
		}
		if (type == typeof(NetLaneProps.Prop))
		{
			NetLaneProps.Prop prop2 = (NetLaneProps.Prop)o;
			w.Write((int)prop2.m_flagsRequired);
			w.Write((int)prop2.m_flagsForbidden);
			w.Write((int)prop2.m_startFlagsRequired);
			w.Write((int)prop2.m_startFlagsForbidden);
			w.Write((int)prop2.m_endFlagsRequired);
			w.Write((int)prop2.m_endFlagsForbidden);
			w.Write((int)prop2.m_colorMode);
			w.Write((!(prop2.m_prop != null)) ? string.Empty : prop2.m_prop.get_name());
			w.Write((!(prop2.m_tree != null)) ? string.Empty : prop2.m_tree.get_name());
			w.Write(prop2.m_position);
			w.Write(prop2.m_angle);
			w.Write(prop2.m_segmentOffset);
			w.Write(prop2.m_repeatDistance);
			w.Write(prop2.m_minLength);
			w.Write(prop2.m_cornerAngle);
			w.Write(prop2.m_probability);
			return true;
		}
		if (type == typeof(NetInfo.Segment))
		{
			NetInfo.Segment segment = (NetInfo.Segment)o;
			Mesh mesh = segment.m_mesh;
			w.Write((!(mesh != null)) ? string.Empty : p.AddAsset(mesh.get_name(), mesh, true).get_checksum());
			Material material = segment.m_material;
			w.Write((!(material != null)) ? string.Empty : p.AddAsset(material.get_name(), material, true).get_checksum());
			Mesh lodMesh = segment.m_lodMesh;
			w.Write((!(lodMesh != null)) ? string.Empty : p.AddAsset(lodMesh.get_name(), lodMesh, true).get_checksum());
			Material lodMaterial = segment.m_lodMaterial;
			w.Write((!(lodMaterial != null)) ? string.Empty : p.AddAsset(lodMaterial.get_name(), lodMaterial, true).get_checksum());
			w.Write((int)segment.m_forwardRequired);
			w.Write((int)segment.m_forwardForbidden);
			w.Write((int)segment.m_backwardRequired);
			w.Write((int)segment.m_backwardForbidden);
			w.Write(segment.m_emptyTransparent);
			w.Write(segment.m_disableBendNodes);
			return true;
		}
		if (type == typeof(NetInfo.Node))
		{
			NetInfo.Node node = (NetInfo.Node)o;
			Mesh mesh2 = node.m_mesh;
			w.Write((!(mesh2 != null)) ? string.Empty : p.AddAsset(mesh2.get_name(), mesh2, true).get_checksum());
			Material material2 = node.m_material;
			w.Write((!(material2 != null)) ? string.Empty : p.AddAsset(material2.get_name(), material2, true).get_checksum());
			Mesh lodMesh2 = node.m_lodMesh;
			w.Write((!(lodMesh2 != null)) ? string.Empty : p.AddAsset(lodMesh2.get_name(), lodMesh2, true).get_checksum());
			Material lodMaterial2 = node.m_lodMaterial;
			w.Write((!(lodMaterial2 != null)) ? string.Empty : p.AddAsset(lodMaterial2.get_name(), lodMaterial2, true).get_checksum());
			w.Write((int)node.m_flagsRequired);
			w.Write((int)node.m_flagsForbidden);
			w.Write((int)node.m_connectGroup);
			w.Write(node.m_directConnect);
			w.Write(node.m_emptyTransparent);
			return true;
		}
		if (type == typeof(BuildingInfo))
		{
			w.Write((o == null) ? string.Empty : ((BuildingInfo)o).get_name());
			return true;
		}
		return false;
	}

	public static object CustomDeserialize(Package p, Type t, PackageReader r)
	{
		if (t == typeof(TransportInfo))
		{
			return PrefabCollection<TransportInfo>.FindLoaded(r.ReadString());
		}
		if (t == typeof(ItemClass))
		{
			return ItemClassCollection.FindClass(r.ReadString());
		}
		if (t == typeof(BuildingInfo.Prop))
		{
			return new BuildingInfo.Prop
			{
				m_prop = PrefabCollection<PropInfo>.FindLoaded(r.ReadString()),
				m_tree = PrefabCollection<TreeInfo>.FindLoaded(r.ReadString()),
				m_position = r.ReadVector3(),
				m_angle = r.ReadSingle(),
				m_probability = r.ReadInt32(),
				m_fixedHeight = r.ReadBoolean()
			};
		}
		if (t == typeof(PropInfo.Variation))
		{
			return new PropInfo.Variation
			{
				m_prop = PrefabCollection<PropInfo>.FindLoaded(p.get_packageName() + "." + r.ReadString()),
				m_probability = r.ReadInt32()
			};
		}
		if (t == typeof(TreeInfo.Variation))
		{
			return new TreeInfo.Variation
			{
				m_tree = PrefabCollection<TreeInfo>.FindLoaded(p.get_packageName() + "." + r.ReadString()),
				m_probability = r.ReadInt32()
			};
		}
		if (t == typeof(BuildingInfo.PathInfo))
		{
			BuildingInfo.PathInfo pathInfo = new BuildingInfo.PathInfo();
			pathInfo.m_netInfo = PrefabCollection<NetInfo>.FindLoaded(r.ReadString());
			pathInfo.m_nodes = r.ReadVector3Array();
			pathInfo.m_curveTargets = r.ReadVector3Array();
			pathInfo.m_invertSegments = r.ReadBoolean();
			pathInfo.m_maxSnapDistance = r.ReadSingle();
			if (p.get_version() >= 5)
			{
				pathInfo.m_forbidLaneConnection = r.ReadBooleanArray();
				pathInfo.m_trafficLights = (BuildingInfo.TrafficLights[])r.ReadInt32Array();
				pathInfo.m_yieldSigns = r.ReadBooleanArray();
			}
			return pathInfo;
		}
		if (t == typeof(MessageInfo))
		{
			MessageInfo messageInfo = new MessageInfo();
			messageInfo.m_firstID1 = r.ReadString();
			if (messageInfo.m_firstID1.Equals(string.Empty))
			{
				messageInfo.m_firstID1 = null;
			}
			messageInfo.m_firstID2 = r.ReadString();
			if (messageInfo.m_firstID2.Equals(string.Empty))
			{
				messageInfo.m_firstID2 = null;
			}
			messageInfo.m_repeatID1 = r.ReadString();
			if (messageInfo.m_repeatID1.Equals(string.Empty))
			{
				messageInfo.m_repeatID1 = null;
			}
			messageInfo.m_repeatID2 = r.ReadString();
			if (messageInfo.m_repeatID2.Equals(string.Empty))
			{
				messageInfo.m_repeatID2 = null;
			}
			return messageInfo;
		}
		if (t == typeof(AudioInfo))
		{
			return null;
		}
		if (t == typeof(ModInfo))
		{
			return new ModInfo
			{
				modName = r.ReadString(),
				modWorkshopID = r.ReadUInt64()
			};
		}
		if (t == typeof(DisasterProperties.DisasterSettings))
		{
			return new DisasterProperties.DisasterSettings
			{
				m_disasterName = r.ReadString(),
				m_randomProbability = r.ReadInt32()
			};
		}
		if (t == typeof(UITextureAtlas))
		{
			UITextureAtlas uITextureAtlas = ScriptableObject.CreateInstance<UITextureAtlas>();
			uITextureAtlas.set_name(r.ReadString());
			int num;
			int num2;
			Texture2D texture2D;
			if (p.get_version() <= 3)
			{
				num = r.ReadInt32();
				num2 = r.ReadInt32();
				texture2D = new Texture2D(num, num2, 5, false, false);
				Color[] pixels = r.ReadColorArray();
				texture2D.SetPixels(pixels);
				texture2D.Apply();
			}
			else
			{
				texture2D = new Texture2D(1, 1, 5, false, false);
				texture2D.LoadImage(r.ReadByteArray());
				num = texture2D.get_width();
				num2 = texture2D.get_height();
			}
			string text = r.ReadString();
			Shader shader = Shader.Find(text);
			Material material = null;
			if (shader != null)
			{
				material = new Material(shader);
				material.set_mainTexture(texture2D);
			}
			else
			{
				Debug.Log("Warning: texture atlas shader *" + text + "* not found.");
			}
			uITextureAtlas.set_material(material);
			uITextureAtlas.set_padding(r.ReadInt32());
			List<UITextureAtlas.SpriteInfo> list = new List<UITextureAtlas.SpriteInfo>();
			int num3 = r.ReadInt32();
			for (int i = 0; i < num3; i++)
			{
				Rect region = default(Rect);
				region.set_x(r.ReadSingle());
				region.set_y(r.ReadSingle());
				region.set_width(r.ReadSingle());
				region.set_height(r.ReadSingle());
				string name = r.ReadString();
				UITextureAtlas.SpriteInfo spriteInfo = new UITextureAtlas.SpriteInfo();
				spriteInfo.set_name(name);
				spriteInfo.set_region(region);
				spriteInfo.set_texture(new Texture2D(Mathf.FloorToInt((float)num * region.get_width()), Mathf.FloorToInt((float)num2 * region.get_height()), 5, false));
				spriteInfo.get_texture().set_name(name);
				Color[] pixels2 = texture2D.GetPixels(Mathf.FloorToInt((float)num * region.get_xMin()), Mathf.FloorToInt((float)num2 * region.get_yMin()), Mathf.FloorToInt((float)num * region.get_width()), Mathf.FloorToInt((float)num2 * region.get_height()));
				spriteInfo.get_texture().SetPixels(pixels2);
				spriteInfo.get_texture().Apply(false);
				list.Add(spriteInfo);
			}
			uITextureAtlas.AddSprites(list);
			return uITextureAtlas;
		}
		if (t == typeof(VehicleInfo.Effect))
		{
			VehicleInfo.Effect effect;
			effect.m_effect = EffectCollection.FindEffect(r.ReadString());
			effect.m_parkedFlagsForbidden = (VehicleParked.Flags)r.ReadInt32();
			effect.m_parkedFlagsRequired = (VehicleParked.Flags)r.ReadInt32();
			effect.m_vehicleFlagsForbidden = (Vehicle.Flags)r.ReadInt32();
			effect.m_vehicleFlagsRequired = (Vehicle.Flags)r.ReadInt32();
			return effect;
		}
		if (t == typeof(VehicleInfo.VehicleDoor))
		{
			VehicleInfo.VehicleDoor vehicleDoor;
			vehicleDoor.m_type = (VehicleInfo.DoorType)r.ReadInt32();
			vehicleDoor.m_location = r.ReadVector3();
			return vehicleDoor;
		}
		if (t == typeof(VehicleInfo.VehicleTrailer))
		{
			string name2 = p.get_packageName() + "." + r.ReadString();
			VehicleInfo.VehicleTrailer vehicleTrailer;
			vehicleTrailer.m_info = PrefabCollection<VehicleInfo>.FindLoaded(name2);
			vehicleTrailer.m_probability = r.ReadInt32();
			vehicleTrailer.m_invertProbability = r.ReadInt32();
			return vehicleTrailer;
		}
		if (t == typeof(ManualMilestone) || t == typeof(CombinedMilestone))
		{
			return MilestoneCollection.FindMilestone(r.ReadString());
		}
		if (t == typeof(TransportInfo))
		{
			return PrefabCollection<TransportInfo>.FindLoaded(r.ReadString());
		}
		if (t == typeof(NetInfo))
		{
			Package.Asset asset = p.Find(p.get_packageMainAsset());
			CustomAssetMetaData customAssetMetaData = (!(asset != null)) ? null : asset.Instantiate<CustomAssetMetaData>();
			if (customAssetMetaData != null && customAssetMetaData.type == CustomAssetMetaData.Type.Road)
			{
				return PrefabCollection<NetInfo>.FindLoaded(p.get_packageName() + "." + PackageHelper.StripName(r.ReadString()));
			}
			return PrefabCollection<NetInfo>.FindLoaded(r.ReadString());
		}
		else
		{
			if (t == typeof(BuildingInfo.MeshInfo))
			{
				BuildingInfo.MeshInfo meshInfo = new BuildingInfo.MeshInfo();
				string text2 = r.ReadString();
				if (text2.Length > 0)
				{
					Package.Asset asset2 = p.FindByChecksum(text2);
					GameObject gameObject = asset2.Instantiate<GameObject>();
					meshInfo.m_subInfo = gameObject.GetComponent<BuildingInfoBase>();
					gameObject.SetActive(false);
					if (meshInfo.m_subInfo.m_lodObject != null)
					{
						meshInfo.m_subInfo.m_lodObject.SetActive(false);
					}
				}
				else
				{
					meshInfo.m_subInfo = null;
				}
				meshInfo.m_flagsForbidden = (Building.Flags)PackageHelper.CustomDeserialize(p, typeof(Building.Flags), r);
				meshInfo.m_flagsRequired = (Building.Flags)PackageHelper.CustomDeserialize(p, typeof(Building.Flags), r);
				meshInfo.m_position = r.ReadVector3();
				meshInfo.m_angle = r.ReadSingle();
				return meshInfo;
			}
			if (t == typeof(VehicleInfo.MeshInfo))
			{
				VehicleInfo.MeshInfo meshInfo2 = new VehicleInfo.MeshInfo();
				string text3 = r.ReadString();
				if (text3.Length > 0)
				{
					Package.Asset asset3 = p.FindByChecksum(text3);
					GameObject gameObject2 = asset3.Instantiate<GameObject>();
					meshInfo2.m_subInfo = gameObject2.GetComponent<VehicleInfoBase>();
					gameObject2.SetActive(false);
					if (meshInfo2.m_subInfo.m_lodObject != null)
					{
						meshInfo2.m_subInfo.m_lodObject.SetActive(false);
					}
				}
				else
				{
					meshInfo2.m_subInfo = null;
				}
				meshInfo2.m_vehicleFlagsForbidden = (Vehicle.Flags)PackageHelper.CustomDeserialize(p, typeof(Vehicle.Flags), r);
				meshInfo2.m_vehicleFlagsRequired = (Vehicle.Flags)PackageHelper.CustomDeserialize(p, typeof(Vehicle.Flags), r);
				meshInfo2.m_parkedFlagsForbidden = (VehicleParked.Flags)PackageHelper.CustomDeserialize(p, typeof(VehicleParked.Flags), r);
				meshInfo2.m_parkedFlagsRequired = (VehicleParked.Flags)PackageHelper.CustomDeserialize(p, typeof(VehicleParked.Flags), r);
				return meshInfo2;
			}
			if (t == typeof(Building.Flags))
			{
				return (Building.Flags)r.ReadInt32();
			}
			if (t == typeof(Vehicle.Flags))
			{
				return (Vehicle.Flags)r.ReadInt32();
			}
			if (t == typeof(VehicleParked.Flags))
			{
				return (VehicleParked.Flags)r.ReadInt32();
			}
			if (t == typeof(DepotAI.SpawnPoint))
			{
				return new DepotAI.SpawnPoint
				{
					m_position = r.ReadVector3(),
					m_target = r.ReadVector3()
				};
			}
			if (t == typeof(PropInfo.Effect))
			{
				return new PropInfo.Effect
				{
					m_effect = EffectCollection.FindEffect(r.ReadString()),
					m_position = r.ReadVector3(),
					m_direction = r.ReadVector3()
				};
			}
			if (t == typeof(BuildingInfo.SubInfo))
			{
				BuildingInfo.SubInfo subInfo = new BuildingInfo.SubInfo();
				string text4 = r.ReadString();
				subInfo.m_buildingInfo = PrefabCollection<BuildingInfo>.FindLoaded(p.get_packageName() + "." + text4);
				if (subInfo.m_buildingInfo == null)
				{
					subInfo.m_buildingInfo = PrefabCollection<BuildingInfo>.FindLoaded(text4);
				}
				subInfo.m_position = r.ReadVector3();
				subInfo.m_angle = r.ReadSingle();
				subInfo.m_fixedHeight = r.ReadBoolean();
				return subInfo;
			}
			if (t == typeof(PropInfo.ParkingSpace))
			{
				return new PropInfo.ParkingSpace
				{
					m_position = r.ReadVector3(),
					m_direction = r.ReadVector3(),
					m_size = r.ReadVector3()
				};
			}
			if (t == typeof(PropInfo.SpecialPlace))
			{
				return new PropInfo.SpecialPlace
				{
					m_specialFlags = (CitizenInstance.Flags)r.ReadInt32(),
					m_position = r.ReadVector3(),
					m_direction = r.ReadVector3()
				};
			}
			if (t == typeof(NetInfo.Lane))
			{
				return new NetInfo.Lane
				{
					m_position = r.ReadSingle(),
					m_width = r.ReadSingle(),
					m_verticalOffset = r.ReadSingle(),
					m_stopOffset = r.ReadSingle(),
					m_speedLimit = r.ReadSingle(),
					m_direction = (NetInfo.Direction)r.ReadInt32(),
					m_laneType = (NetInfo.LaneType)r.ReadInt32(),
					m_vehicleType = (VehicleInfo.VehicleType)r.ReadInt32(),
					m_stopType = (VehicleInfo.VehicleType)r.ReadInt32(),
					m_laneProps = (NetLaneProps)PackageHelper.CustomDeserialize(p, typeof(NetLaneProps), r),
					m_allowConnect = r.ReadBoolean(),
					m_useTerrainHeight = r.ReadBoolean(),
					m_centerPlatform = r.ReadBoolean(),
					m_elevated = r.ReadBoolean()
				};
			}
			if (t == typeof(NetLaneProps))
			{
				NetLaneProps netLaneProps = ScriptableObject.CreateInstance<NetLaneProps>();
				int num4 = r.ReadInt32();
				netLaneProps.m_props = new NetLaneProps.Prop[num4];
				for (int j = 0; j < num4; j++)
				{
					object obj = PackageHelper.CustomDeserialize(p, typeof(NetLaneProps.Prop), r);
					netLaneProps.m_props[j] = ((obj == null) ? null : ((NetLaneProps.Prop)obj));
				}
				return netLaneProps;
			}
			if (t == typeof(NetLaneProps.Prop))
			{
				return new NetLaneProps.Prop
				{
					m_flagsRequired = (NetLane.Flags)r.ReadInt32(),
					m_flagsForbidden = (NetLane.Flags)r.ReadInt32(),
					m_startFlagsRequired = (NetNode.Flags)r.ReadInt32(),
					m_startFlagsForbidden = (NetNode.Flags)r.ReadInt32(),
					m_endFlagsRequired = (NetNode.Flags)r.ReadInt32(),
					m_endFlagsForbidden = (NetNode.Flags)r.ReadInt32(),
					m_colorMode = (NetLaneProps.ColorMode)r.ReadInt32(),
					m_prop = PrefabCollection<PropInfo>.FindLoaded(r.ReadString()),
					m_tree = PrefabCollection<TreeInfo>.FindLoaded(r.ReadString()),
					m_position = r.ReadVector3(),
					m_angle = r.ReadSingle(),
					m_segmentOffset = r.ReadSingle(),
					m_repeatDistance = r.ReadSingle(),
					m_minLength = r.ReadSingle(),
					m_cornerAngle = r.ReadSingle(),
					m_probability = r.ReadInt32()
				};
			}
			if (t == typeof(NetInfo.Segment))
			{
				NetInfo.Segment segment = new NetInfo.Segment();
				Package.Asset asset4 = p.FindByChecksum(r.ReadString());
				segment.m_mesh = ((!(asset4 != null)) ? null : asset4.Instantiate<Mesh>());
				Package.Asset asset5 = p.FindByChecksum(r.ReadString());
				segment.m_material = ((!(asset5 != null)) ? null : asset5.Instantiate<Material>());
				Package.Asset asset6 = p.FindByChecksum(r.ReadString());
				segment.m_lodMesh = ((!(asset6 != null)) ? null : asset6.Instantiate<Mesh>());
				Package.Asset asset7 = p.FindByChecksum(r.ReadString());
				segment.m_lodMaterial = ((!(asset7 != null)) ? null : asset7.Instantiate<Material>());
				segment.m_forwardRequired = (NetSegment.Flags)r.ReadInt32();
				segment.m_forwardForbidden = (NetSegment.Flags)r.ReadInt32();
				segment.m_backwardRequired = (NetSegment.Flags)r.ReadInt32();
				segment.m_backwardForbidden = (NetSegment.Flags)r.ReadInt32();
				segment.m_emptyTransparent = r.ReadBoolean();
				segment.m_disableBendNodes = r.ReadBoolean();
				return segment;
			}
			if (t == typeof(NetInfo.Node))
			{
				NetInfo.Node node = new NetInfo.Node();
				Package.Asset asset8 = p.FindByChecksum(r.ReadString());
				node.m_mesh = ((!(asset8 != null)) ? null : asset8.Instantiate<Mesh>());
				Package.Asset asset9 = p.FindByChecksum(r.ReadString());
				node.m_material = ((!(asset9 != null)) ? null : asset9.Instantiate<Material>());
				Package.Asset asset10 = p.FindByChecksum(r.ReadString());
				node.m_lodMesh = ((!(asset10 != null)) ? null : asset10.Instantiate<Mesh>());
				Package.Asset asset11 = p.FindByChecksum(r.ReadString());
				node.m_lodMaterial = ((!(asset11 != null)) ? null : asset11.Instantiate<Material>());
				node.m_flagsRequired = (NetNode.Flags)r.ReadInt32();
				node.m_flagsForbidden = (NetNode.Flags)r.ReadInt32();
				node.m_connectGroup = (NetInfo.ConnectGroup)r.ReadInt32();
				node.m_directConnect = r.ReadBoolean();
				node.m_emptyTransparent = r.ReadBoolean();
				return node;
			}
			if (t != typeof(BuildingInfo))
			{
				return null;
			}
			Package.Asset asset12 = p.Find(p.get_packageMainAsset());
			CustomAssetMetaData customAssetMetaData2 = (!(asset12 != null)) ? null : asset12.Instantiate<CustomAssetMetaData>();
			if (customAssetMetaData2 != null && customAssetMetaData2.type == CustomAssetMetaData.Type.Road)
			{
				return PrefabCollection<BuildingInfo>.FindLoaded(p.get_packageName() + "." + PackageHelper.StripName(r.ReadString()));
			}
			return PrefabCollection<BuildingInfo>.FindLoaded(r.ReadString());
		}
	}

	public static int UnknownTypeHandler(string type)
	{
		if (type == "UnlockManager+Milestone, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
		{
			return 4;
		}
		return -1;
	}

	public static string ResolveLegacyMemberHandler(Type type, string name)
	{
		if ((type == typeof(SystemMapMetaData) || type == typeof(MapMetaData) || type == typeof(SaveGameMetaData)) && name == "saveRef")
		{
			return "assetRef";
		}
		return name;
	}

	public static string ResolveLegacyTypeHandler(string type)
	{
		if (type == "LoadSaveMapPanel+MapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
		{
			return "MapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";
		}
		if (type == "LoadSavePanelBase+MapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
		{
			return "MapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";
		}
		if (type == "LoadSavePanelBase+SystemMapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
		{
			return "SystemMapMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";
		}
		if (type == "LoadSavePanelBase+SaveGameMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
		{
			return "SaveGameMetaData, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";
		}
		return type;
	}

	public static void ExtractSystemPackage(string[] args)
	{
		Package package = PackageManager.GetPackage("System");
		package.Extract();
	}

	public static void ExtractMapPackages(string[] args)
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapMetaData
		}))
		{
			current.get_package().Extract();
		}
	}

	public static string StripName(string name)
	{
		int num = name.LastIndexOf(".");
		return (num >= 0) ? name.Remove(0, num + 1) : name;
	}
}
