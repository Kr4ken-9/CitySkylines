﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class ResetWaterOptionPanel : ToolsModifierControl
{
	private void Awake()
	{
		this.m_ResetSeaLevel = new SavedInputKey(Settings.mapEditorResetSeaLevel, Settings.inputSettingsFile, DefaultSettings.mapEditorResetSeaLevel, true);
		this.Hide();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIInput.add_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
		else
		{
			UIInput.remove_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
		}
	}

	private void ProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (eventType != 4)
		{
			return;
		}
		if (this.m_ResetSeaLevel.IsPressed(eventType, keyCode, modifiers))
		{
			this.ResetWater();
		}
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	public void ResetWater()
	{
		Singleton<TerrainManager>.get_instance().WaterSimulation.m_resetWater = true;
	}

	private SavedInputKey m_ResetSeaLevel;
}
