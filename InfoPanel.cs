﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class InfoPanel : ToolsModifierControl
{
	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.SetName();
	}

	private void SetName()
	{
		if (Singleton<SimulationManager>.get_exists())
		{
			string cityName = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			if (cityName != null)
			{
				this.m_MapName.set_text(cityName);
			}
		}
	}

	private void Awake()
	{
		this.m_MapName = base.Find<UITextComponent>("Name");
		this.m_TimeControl = base.Find<UISlider>("LightRotation");
		if (this.m_TimeControl != null)
		{
			this.m_TimeControl.set_value(12f);
		}
		this.m_SnowToggle = base.Find<UICheckBox>("SnowToggle");
		if (this.m_SnowToggle != null)
		{
			if (Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Winter" || (Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData != null && Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData.environment == "Winter"))
			{
				this.m_SnowToggle.set_isVisible(true);
				this.m_SnowToggle.set_isChecked(true);
				this.m_SnowToggle.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnToggleSnow));
			}
			else
			{
				this.m_SnowToggle.set_isVisible(false);
			}
		}
		UIComponent uIComponent = base.Find("Heat'o'meter");
		if (uIComponent != null)
		{
			uIComponent.set_isVisible(Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation));
		}
	}

	private void OnToggleSnow(UIComponent c, bool enabled)
	{
		if (enabled)
		{
			Shader.EnableKeyword("SNOW_ON");
		}
		else
		{
			Shader.DisableKeyword("SNOW_ON");
		}
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void Update()
	{
		if (Singleton<ToolManager>.get_exists() && this.m_TimeControl != null && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Editors) != ItemClass.Availability.None)
		{
			float num = Mathf.Clamp(this.m_TimeControl.get_value(), 0f, 24f);
			if (num != Singleton<SimulationManager>.get_instance().m_currentDayTimeHour)
			{
				num /= 24f;
				uint num2 = (uint)(num * SimulationManager.DAYTIME_FRAMES);
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				Singleton<SimulationManager>.get_instance().m_dayTimeOffsetFrames = (num2 - currentFrameIndex & SimulationManager.DAYTIME_FRAMES - 1u);
			}
		}
	}

	private string GetDemandIndex(int demand)
	{
		string result = InfoPanel.sDemandIndexes[0];
		if (demand > 0 && demand <= 20)
		{
			result = InfoPanel.sDemandIndexes[1];
		}
		else if (demand > 20 && demand <= 50)
		{
			result = InfoPanel.sDemandIndexes[2];
		}
		else if (demand > 50 && demand <= 90)
		{
			result = InfoPanel.sDemandIndexes[3];
		}
		else if (demand > 90)
		{
			result = InfoPanel.sDemandIndexes[4];
		}
		return result;
	}

	public string residentialDemandTooltip
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				int residentialDemand = Singleton<ZoneManager>.get_instance().m_residentialDemand;
				return Locale.Get("MAIN_RESIDENTIAL_DEMAND", this.GetDemandIndex(residentialDemand));
			}
			return "Demand is not available";
		}
	}

	public string commercialDemandTooltip
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				int commercialDemand = Singleton<ZoneManager>.get_instance().m_commercialDemand;
				return Locale.Get("MAIN_COMMERCIAL_DEMAND", this.GetDemandIndex(commercialDemand));
			}
			return "Demand is not available";
		}
	}

	public string workplaceDemandTooltip
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				int workplaceDemand = Singleton<ZoneManager>.get_instance().m_workplaceDemand;
				return Locale.Get("MAIN_INDUSTRYOFFICE_DEMAND", this.GetDemandIndex(workplaceDemand));
			}
			return "Demand is not available";
		}
	}

	public float residentialDemand
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				return (float)Singleton<ZoneManager>.get_instance().m_residentialDemand;
			}
			return 0f;
		}
	}

	public float commercialDemand
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				return (float)Singleton<ZoneManager>.get_instance().m_commercialDemand;
			}
			return 0f;
		}
	}

	public float workplaceDemand
	{
		get
		{
			if (Singleton<ZoneManager>.get_exists())
			{
				return (float)Singleton<ZoneManager>.get_instance().m_workplaceDemand;
			}
			return 0f;
		}
	}

	public string happinessIcon
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				this.m_HappinessIcon.Check(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_finalHappiness);
			}
			return this.m_HappinessIcon.result;
		}
	}

	public void OnFreeCameraClick()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() == null)
		{
			this.m_LastTool = ToolsModifierControl.toolController.CurrentTool;
			this.m_LastInfoMode = Singleton<InfoManager>.get_instance().NextMode;
			this.m_LastSubInfoMode = Singleton<InfoManager>.get_instance().NextSubMode;
			ToolsModifierControl.SetTool<CameraTool>();
		}
		else
		{
			ToolsModifierControl.toolController.CurrentTool = this.m_LastTool;
			if (this.m_LastTool == ToolsModifierControl.GetTool<DefaultTool>())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(this.m_LastInfoMode, this.m_LastSubInfoMode);
			}
		}
	}

	public void OnCityInfoClicked(UIComponent comp, UIMouseEventParameter p)
	{
		CityInfoPanel cityInfoPanel = UIView.get_library().Get<CityInfoPanel>("CityInfoPanel");
		if (cityInfoPanel.isVisible)
		{
			UIView.get_library().Hide("CityInfoPanel");
		}
		else
		{
			UIView.get_library().Show<CityInfoPanel>("CityInfoPanel");
			cityInfoPanel.get_transform().set_position(p.get_source().get_transform().get_position());
			cityInfoPanel.SetRenameCallback(new Action(this.SetName));
		}
	}

	private static readonly string[] sDemandIndexes = new string[]
	{
		"None",
		"Low",
		"Medium",
		"High",
		"Critical"
	};

	private UITextComponent m_MapName;

	private UISlider m_TimeControl;

	private UICheckBox m_SnowToggle;

	private UIHappinessWrapper m_HappinessIcon = new UIHappinessWrapper(255);

	private ToolBase m_LastTool;

	private InfoManager.InfoMode m_LastInfoMode;

	private InfoManager.SubInfoMode m_LastSubInfoMode;
}
