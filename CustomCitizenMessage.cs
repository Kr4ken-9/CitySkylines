﻿using System;

public class CustomCitizenMessage : CitizenMessage
{
	public CustomCitizenMessage(uint senderID, string message, string tag) : base(senderID, message, null, tag)
	{
	}

	public override string GetText()
	{
		string text = this.m_messageID;
		if (!string.IsNullOrEmpty(this.m_tag))
		{
			string text2 = this.m_tag.Replace(" ", string.Empty);
			text = StringUtils.SafeFormat(text, new object[]
			{
				this.m_tag,
				text2
			});
		}
		return text;
	}
}
