﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class ImmaterialResourceManager : SimulationManagerBase<ImmaterialResourceManager, ImmaterialResourceProperties>, ISimulationManager
{
	public ImmaterialResourceManager.Resource ResourceMapVisible
	{
		get
		{
			return this.m_resourceMapVisible;
		}
		set
		{
			if (this.m_resourceMapVisible != value)
			{
				this.m_resourceMapVisible = value;
				this.UpdateResourceMapping();
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_localFinalResources = new ushort[1572864];
		this.m_localTempResources = new ushort[1572864];
		this.m_globalFinalResources = new int[24];
		this.m_globalTempResources = new int[24];
		this.m_totalFinalResources = new int[24];
		this.m_totalTempResources = new int[24];
		this.m_totalTempResourcesMul = new long[24];
		this.m_tempCircleMinX = new int[256];
		this.m_tempCircleMaxX = new int[256];
		this.m_tempSectorSlopes = new float[256];
		this.m_tempSectorDistances = new float[256];
		this.m_resourceMapVisible = ImmaterialResourceManager.Resource.None;
		this.m_resourceTexture = new Texture2D(256, 256, 1, false, true);
		this.m_resourceTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_ImmaterialResources", this.m_resourceTexture);
		this.UpdateResourceMapping();
		this.m_modifiedX1 = new int[256];
		this.m_modifiedX2 = new int[256];
		for (int i = 0; i < 256; i++)
		{
			this.m_modifiedX1[i] = 0;
			this.m_modifiedX2[i] = 255;
		}
		this.m_modified = true;
	}

	private void OnDestroy()
	{
		if (this.m_resourceTexture != null)
		{
			Object.Destroy(this.m_resourceTexture);
			this.m_resourceTexture = null;
		}
	}

	private void UpdateResourceMapping()
	{
		Vector4 vector;
		vector.z = 0.000101725258f;
		vector.x = 0.5f;
		vector.y = 0.5f;
		vector.w = 0.25f;
		Shader.SetGlobalVector("_ImmaterialResourceMapping", vector);
		if (this.m_resourceMapVisible != ImmaterialResourceManager.Resource.None)
		{
			this.AreaModified(0, 0, 255, 255);
			this.UpdateTexture();
			Shader.EnableKeyword("RESOURCE_OVERLAY_IMMATERIAL");
			Shader.SetGlobalColor("_ImmaterialResourceColor", this.m_properties.m_resourceColors[(int)this.m_resourceMapVisible].get_linear());
		}
		else
		{
			Shader.DisableKeyword("RESOURCE_OVERLAY_IMMATERIAL");
		}
	}

	private void LateUpdate()
	{
		if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			return;
		}
		if (this.m_resourceMapVisible != ImmaterialResourceManager.Resource.None && this.m_modified)
		{
			this.m_modified = false;
			this.UpdateTexture();
		}
	}

	private void UpdateTexture()
	{
		for (int i = 0; i < 256; i++)
		{
			if (this.m_modifiedX2[i] >= this.m_modifiedX1[i])
			{
				while (!Monitor.TryEnter(this.m_localFinalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				int num;
				int num2;
				try
				{
					num = this.m_modifiedX1[i];
					num2 = this.m_modifiedX2[i];
					this.m_modifiedX1[i] = 10000;
					this.m_modifiedX2[i] = -10000;
				}
				finally
				{
					Monitor.Exit(this.m_localFinalResources);
				}
				for (int j = num; j <= num2; j++)
				{
					int num3 = 0;
					this.AddLocalResource(j - 1, i - 1, 5, ref num3);
					this.AddLocalResource(j, i - 1, 7, ref num3);
					this.AddLocalResource(j + 1, i - 1, 5, ref num3);
					this.AddLocalResource(j - 1, i, 7, ref num3);
					this.AddLocalResource(j, i, 14, ref num3);
					this.AddLocalResource(j + 1, i, 7, ref num3);
					this.AddLocalResource(j - 1, i + 1, 5, ref num3);
					this.AddLocalResource(j, i + 1, 7, ref num3);
					this.AddLocalResource(j + 1, i + 1, 5, ref num3);
					float num4 = Mathf.Clamp01(Mathf.Sqrt((float)num3 * 0.000161290329f));
					Color color;
					color.r = num4;
					color.g = num4;
					color.b = num4;
					color.a = num4;
					this.m_resourceTexture.SetPixel(j, i, color);
				}
			}
		}
		this.m_resourceTexture.Apply();
	}

	private void AddLocalResource(int x, int z, int multiplier, ref int amount)
	{
		x = Mathf.Clamp(x, 0, 255);
		z = Mathf.Clamp(z, 0, 255);
		int num = (int)((z * 256 + x) * 24 + this.m_resourceMapVisible);
		amount += (int)this.m_localFinalResources[num] * multiplier;
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ)
	{
		minX = Mathf.Max(0, minX - 1);
		minZ = Mathf.Max(0, minZ - 1);
		maxX = Mathf.Min(255, maxX + 1);
		maxZ = Mathf.Min(255, maxZ + 1);
		while (!Monitor.TryEnter(this.m_localFinalResources, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = minZ; i <= maxZ; i++)
			{
				this.m_modifiedX1[i] = Mathf.Min(this.m_modifiedX1[i], minX);
				this.m_modifiedX2[i] = Mathf.Max(this.m_modifiedX2[i], maxX);
			}
			this.m_modified = true;
		}
		finally
		{
			Monitor.Exit(this.m_localFinalResources);
		}
	}

	private void AddResource(ref ushort buffer, int rate)
	{
		buffer = (ushort)Mathf.Min((int)buffer + rate, 65535);
	}

	private void AddResource(ref int buffer, int rate)
	{
		buffer = Mathf.Min(buffer + rate, 2147483647);
	}

	public int AddResource(ImmaterialResourceManager.Resource resource, int rate, Vector3 position, float radius)
	{
		if (rate == 0)
		{
			return 0;
		}
		float num = Mathf.Max(0f, Mathf.Min(radius, 19.2f));
		float num2 = Mathf.Max(38.4f, radius + 19.2f);
		int num3 = Mathf.Max((int)((position.x - radius) / 38.4f + 128f), 2);
		int num4 = Mathf.Max((int)((position.z - radius) / 38.4f + 128f), 2);
		int num5 = Mathf.Min((int)((position.x + radius) / 38.4f + 128f), 253);
		int num6 = Mathf.Min((int)((position.z + radius) / 38.4f + 128f), 253);
		for (int i = num4; i <= num6; i++)
		{
			float num7 = ((float)i - 128f + 0.5f) * 38.4f - position.z;
			for (int j = num3; j <= num5; j++)
			{
				float num8 = ((float)j - 128f + 0.5f) * 38.4f - position.x;
				float num9 = num7 * num7 + num8 * num8;
				if (num9 < num2 * num2)
				{
					int num10 = rate;
					if (num9 > num * num)
					{
						float num11 = Mathf.Clamp01((num2 - Mathf.Sqrt(num9)) / (num2 - num));
						num10 = Mathf.RoundToInt((float)num10 * num11);
					}
					int num12 = (int)((i * 256 + j) * 24 + resource);
					this.AddResource(ref this.m_localTempResources[num12], num10);
				}
			}
		}
		return rate;
	}

	public int AddObstructedResource(ImmaterialResourceManager.Resource resource, int rate, Vector3 position, float radius)
	{
		if (rate == 0)
		{
			return 0;
		}
		ushort[] finalHeights = Singleton<TerrainManager>.get_instance().FinalHeights;
		for (int i = 0; i < 256; i++)
		{
			this.m_tempSectorSlopes[i] = -100f;
			this.m_tempSectorDistances[i] = 0f;
		}
		float num = radius * 0.5f;
		float num2 = Mathf.Max(38.4f, radius + 19.2f);
		int num3 = Mathf.Clamp((int)(position.x / 38.4f + 128f), 2, 253);
		int num4 = num3;
		int num5 = Mathf.Clamp((int)(position.z / 38.4f + 128f), 2, 253);
		int num6 = num5;
		float num7 = position.x - ((float)num3 - 128f + 0.5f) * 38.4f;
		float num8 = position.z - ((float)num5 - 128f + 0.5f) * 38.4f;
		if (num7 > 9.6f)
		{
			num4 = Mathf.Min(num4 + 1, 253);
		}
		else if (num7 < -9.6f)
		{
			num3 = Mathf.Max(num3 - 1, 2);
		}
		if (num8 > 9.6f)
		{
			num6 = Mathf.Min(num6 + 1, 253);
		}
		else if (num8 < -9.6f)
		{
			num5 = Mathf.Max(num5 - 1, 2);
		}
		int num9 = num5;
		int num10 = num6;
		int num11 = num9 + 1;
		int num12 = num10 - 1;
		int num13 = 0;
		bool flag;
		do
		{
			flag = false;
			float num14 = 38.4f * (0.75f + (float)num13++);
			num14 *= num14;
			for (int j = num9; j <= num10; j++)
			{
				int num15 = (j > num5) ? j : (num5 - j + num9);
				float num16 = ((float)num15 - 128f + 0.5f) * 38.4f;
				float num17 = num16 - position.z;
				int num18 = Mathf.Clamp(Mathf.RoundToInt(num16 / 16f + 540f), 0, 1080);
				int num19 = num3;
				int num20 = num4;
				if (num15 >= num11 && num15 <= num12)
				{
					num19 = Mathf.Min(num19, this.m_tempCircleMinX[num15] - 1);
					num20 = Mathf.Max(num20, this.m_tempCircleMaxX[num15] + 1);
				}
				int k = num19;
				while (k >= 2)
				{
					float num21 = ((float)k - 128f + 0.5f) * 38.4f;
					float num22 = num21 - position.x;
					int num23 = Mathf.Clamp(Mathf.RoundToInt(num21 / 16f + 540f), 0, 1080);
					float num24 = num17 * num17 + num22 * num22;
					if ((num19 == k || num24 < num14) && num24 < num2 * num2)
					{
						float num25 = Mathf.Sqrt(num24);
						float num26 = (float)rate;
						ushort num27 = finalHeights[num18 * 1081 + num23];
						float num28 = 0.015625f * (float)num27;
						float num29 = (num28 - position.y) / Mathf.Max(1f, num25);
						float num30 = Mathf.Atan2(num17, num22) * 40.7436638f;
						float num31 = Mathf.Min(38.4f / Mathf.Max(1f, num25) * 20.3718319f, 64f);
						int num32 = Mathf.RoundToInt(num30 - num31) & 255;
						int num33 = Mathf.RoundToInt(num30 + num31) & 255;
						float num34 = 0f;
						float num35 = 0f;
						int num36 = num32;
						while (true)
						{
							float num37 = this.m_tempSectorSlopes[num36];
							float num38 = this.m_tempSectorDistances[num36];
							float num39 = Mathf.Clamp(num25 - num38, 1f, 38.4f);
							num34 += (num37 - num29) * num39;
							num35 += num39;
							if (num29 > num37)
							{
								this.m_tempSectorSlopes[num36] = num29;
								this.m_tempSectorDistances[num36] = num25;
							}
							if (num36 == num33)
							{
								break;
							}
							num36 = (num36 + 1 & 255);
						}
						num34 /= Mathf.Max(1f, num35);
						num26 *= 1.5f / Mathf.Max(1f, num34 * 20f + 2.625f) - 0.5f;
						if (num26 > 0f)
						{
							if (num25 > num)
							{
								num26 *= Mathf.Clamp01((num2 - num25) / (num2 - num));
							}
							int num40 = (int)((num15 * 256 + k) * 24 + resource);
							this.AddResource(ref this.m_localTempResources[num40], Mathf.RoundToInt(num26));
						}
						flag = true;
					}
					if (num24 >= num14)
					{
						break;
					}
					num19 = k--;
				}
				k = num20;
				while (k <= 253)
				{
					float num41 = ((float)k - 128f + 0.5f) * 38.4f;
					float num42 = num41 - position.x;
					int num43 = Mathf.Clamp(Mathf.RoundToInt(num41 / 16f + 540f), 0, 1080);
					float num44 = num17 * num17 + num42 * num42;
					if ((num20 == k || num44 < num14) && k != num3 && num44 < num2 * num2)
					{
						float num45 = Mathf.Sqrt(num44);
						float num46 = (float)rate;
						ushort num47 = finalHeights[num18 * 1081 + num43];
						float num48 = 0.015625f * (float)num47;
						float num49 = (num48 - position.y) / Mathf.Max(1f, num45);
						float num50 = Mathf.Atan2(num17, num42) * 40.7436638f;
						float num51 = Mathf.Min(38.4f / Mathf.Max(1f, num45) * 20.3718319f, 64f);
						int num52 = Mathf.RoundToInt(num50 - num51) & 255;
						int num53 = Mathf.RoundToInt(num50 + num51) & 255;
						float num54 = 0f;
						float num55 = 0f;
						int num56 = num52;
						while (true)
						{
							float num57 = this.m_tempSectorSlopes[num56];
							float num58 = this.m_tempSectorDistances[num56];
							float num59 = Mathf.Clamp(num45 - num58, 1f, 38.4f);
							num54 += (num57 - num49) * num59;
							num55 += num59;
							if (num49 > num57)
							{
								this.m_tempSectorSlopes[num56] = num49;
								this.m_tempSectorDistances[num56] = num45;
							}
							if (num56 == num53)
							{
								break;
							}
							num56 = (num56 + 1 & 255);
						}
						num54 /= Mathf.Max(1f, num55);
						num46 *= 1.5f / Mathf.Max(1f, num54 * 20f + 2.625f) - 0.5f;
						if (num46 > 0f)
						{
							if (num45 > num)
							{
								num46 *= Mathf.Clamp01((num2 - num45) / (num2 - num));
							}
							int num60 = (int)((num15 * 256 + k) * 24 + resource);
							this.AddResource(ref this.m_localTempResources[num60], Mathf.RoundToInt(num46));
						}
						flag = true;
					}
					if (num44 >= num14)
					{
						break;
					}
					num20 = k++;
				}
				this.m_tempCircleMinX[num15] = num19;
				this.m_tempCircleMaxX[num15] = num20;
			}
			num11 = num9;
			num12 = num10;
			if (num9 > 2)
			{
				num9--;
			}
			if (num10 < 253)
			{
				num10++;
			}
		}
		while (flag);
		return rate;
	}

	public int AddResource(ImmaterialResourceManager.Resource resource, int rate)
	{
		if (rate == 0)
		{
			return 0;
		}
		this.AddResource(ref this.m_globalTempResources[(int)resource], rate);
		return rate;
	}

	public static int CalculateResourceEffect(int resourceRate, int middleRate, int maxRate, int middleEffect, int maxEffect)
	{
		if (resourceRate <= 0)
		{
			return 0;
		}
		if (resourceRate < middleRate)
		{
			return middleEffect * resourceRate / middleRate;
		}
		if (resourceRate < maxRate)
		{
			return middleEffect + (maxEffect - middleEffect) * (resourceRate - middleRate) / (maxRate - middleRate);
		}
		return maxEffect;
	}

	private static bool CalculateLocalResources(int x, int z, ushort[] buffer, int[] global, ushort[] target, int index)
	{
		int num = (int)buffer[index] + global[0];
		int num2 = (int)buffer[index + 1] + global[1];
		int num3 = (int)buffer[index + 2] + global[2];
		int num4 = (int)buffer[index + 3] + global[3];
		int num5 = (int)buffer[index + 4] + global[4];
		int num6 = (int)buffer[index + 5] + global[5];
		int num7 = (int)buffer[index + 6] + global[6];
		int num8 = (int)buffer[index + 7] + global[7];
		int num9 = (int)buffer[index + 8] + global[8];
		int num10 = (int)buffer[index + 9] + global[9];
		int num11 = (int)buffer[index + 10] + global[10];
		int num12 = (int)buffer[index + 11] + global[11];
		int num13 = (int)buffer[index + 12] + global[12];
		int num14 = (int)buffer[index + 13] + global[13];
		int num15 = (int)buffer[index + 14] + global[14];
		int num16 = (int)buffer[index + 15] + global[15];
		int num17 = (int)buffer[index + 16] + global[16];
		int num18 = (int)buffer[index + 17] + global[17];
		int num19 = (int)buffer[index + 18] + global[18];
		int num20 = (int)buffer[index + 19] + global[19];
		int num21 = (int)buffer[index + 20] + global[20];
		int num22 = (int)buffer[index + 21];
		int num23 = (int)buffer[index + 22] + global[22];
		int num24 = (int)buffer[index + 23] + global[23];
		Rect area;
		area..ctor(((float)x - 128f - 1.5f) * 38.4f, ((float)z - 128f - 1.5f) * 38.4f, 153.6f, 153.6f);
		float num25;
		float num26;
		Singleton<NaturalResourceManager>.get_instance().AveragePollutionAndWater(area, out num25, out num26);
		int num27 = (int)(num25 * 100f);
		int num28 = (int)(num26 * 100f);
		if (num28 > 33 && num28 < 99)
		{
			area..ctor(((float)x - 128f + 0.25f) * 38.4f, ((float)z - 128f + 0.25f) * 38.4f, 19.2f, 19.2f);
			Singleton<NaturalResourceManager>.get_instance().AveragePollutionAndWater(area, out num25, out num26);
			num28 = Mathf.Max(Mathf.Min(num28, (int)(num26 * 100f)), 33);
		}
		num18 = num18 * 2 / (num2 + 50);
		if (num13 == 0)
		{
			num10 = 0;
			num11 = 50;
			num12 = 50;
		}
		else
		{
			num10 /= num13;
			num11 /= num13;
			num12 /= num13;
			num17 += Mathf.Min(num13, 10) * 10;
		}
		bool flag = Singleton<GameAreaManager>.get_instance().PointOutOfArea(VectorUtils.X_Y(area.get_center()));
		flag |= (x <= 1 || x >= 254 || z <= 1 || z >= 254);
		if (flag)
		{
			num15 = 0;
		}
		else
		{
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num3, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num2, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num4, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num5, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num6, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num7, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num8, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num20, 100, 500, 50, 100);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num14, 100, 500, 100, 200);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num12, 60, 100, 0, 50);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num11, 60, 100, 0, 50);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num21, 50, 100, 20, 25);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num24, 50, 100, 20, 25);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num22, 100, 1000, 0, 25);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(100 - num12, 60, 100, 0, 50);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(100 - num11, 60, 100, 0, 50);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(num27, 50, 255, 50, 100);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(num9, 10, 100, 0, 100);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(num10, 10, 100, 0, 100);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(num18, 50, 100, 10, 50);
			num15 -= ImmaterialResourceManager.CalculateResourceEffect(num19, 15, 50, 100, 200);
			num15 += ImmaterialResourceManager.CalculateResourceEffect(num28, 33, 67, 300, 0) * Mathf.Max(0, 32 - num27) >> 5;
			num15 /= 10;
		}
		num = Mathf.Clamp(num, 0, 65535);
		num2 = Mathf.Clamp(num2, 0, 65535);
		num3 = Mathf.Clamp(num3, 0, 65535);
		num4 = Mathf.Clamp(num4, 0, 65535);
		num5 = Mathf.Clamp(num5, 0, 65535);
		num6 = Mathf.Clamp(num6, 0, 65535);
		num7 = Mathf.Clamp(num7, 0, 65535);
		num8 = Mathf.Clamp(num8, 0, 65535);
		num9 = Mathf.Clamp(num9, 0, 65535);
		num10 = Mathf.Clamp(num10, 0, 65535);
		num11 = Mathf.Clamp(num11, 0, 65535);
		num12 = Mathf.Clamp(num12, 0, 65535);
		num13 = Mathf.Clamp(num13, 0, 65535);
		num14 = Mathf.Clamp(num14, 0, 65535);
		num15 = Mathf.Clamp(num15, 0, 65535);
		num16 = Mathf.Clamp(num16, 0, 65535);
		num17 = Mathf.Clamp(num17, 0, 65535);
		num18 = Mathf.Clamp(num18, 0, 65535);
		num19 = Mathf.Clamp(num19, 0, 65535);
		num20 = Mathf.Clamp(num20, 0, 65535);
		num21 = Mathf.Clamp(num21, 0, 65535);
		num22 = Mathf.Clamp(num22, 0, 65535);
		num23 = Mathf.Clamp(num23, 0, 65535);
		num24 = Mathf.Clamp(num24, 0, 65535);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(x * 2, z * 2);
		instance.m_districts.m_buffer[(int)district].AddGroundData(num15, num27, num17);
		bool result = false;
		if (num != (int)target[index])
		{
			target[index] = (ushort)num;
			result = true;
		}
		if (num2 != (int)target[index + 1])
		{
			target[index + 1] = (ushort)num2;
			result = true;
		}
		if (num3 != (int)target[index + 2])
		{
			target[index + 2] = (ushort)num3;
			result = true;
		}
		if (num4 != (int)target[index + 3])
		{
			target[index + 3] = (ushort)num4;
			result = true;
		}
		if (num5 != (int)target[index + 4])
		{
			target[index + 4] = (ushort)num5;
			result = true;
		}
		if (num6 != (int)target[index + 5])
		{
			target[index + 5] = (ushort)num6;
			result = true;
		}
		if (num7 != (int)target[index + 6])
		{
			target[index + 6] = (ushort)num7;
			result = true;
		}
		if (num8 != (int)target[index + 7])
		{
			target[index + 7] = (ushort)num8;
			result = true;
		}
		if (num9 != (int)target[index + 8])
		{
			target[index + 8] = (ushort)num9;
			result = true;
		}
		if (num10 != (int)target[index + 9])
		{
			target[index + 9] = (ushort)num10;
			result = true;
		}
		if (num11 != (int)target[index + 10])
		{
			target[index + 10] = (ushort)num11;
			result = true;
		}
		if (num12 != (int)target[index + 11])
		{
			target[index + 11] = (ushort)num12;
			result = true;
		}
		if (num13 != (int)target[index + 12])
		{
			target[index + 12] = (ushort)num13;
			result = true;
		}
		if (num14 != (int)target[index + 13])
		{
			target[index + 13] = (ushort)num14;
			result = true;
		}
		if (num15 != (int)target[index + 14])
		{
			target[index + 14] = (ushort)num15;
			result = true;
		}
		if (num16 != (int)target[index + 15])
		{
			target[index + 15] = (ushort)num16;
			result = true;
		}
		if (num17 != (int)target[index + 16])
		{
			target[index + 16] = (ushort)num17;
			result = true;
		}
		if (num18 != (int)target[index + 17])
		{
			target[index + 17] = (ushort)num18;
			result = true;
		}
		if (num19 != (int)target[index + 18])
		{
			target[index + 18] = (ushort)num19;
			result = true;
		}
		if (num20 != (int)target[index + 19])
		{
			target[index + 19] = (ushort)num20;
			result = true;
		}
		if (num21 != (int)target[index + 20])
		{
			target[index + 20] = (ushort)num21;
			result = true;
		}
		if (num22 != (int)target[index + 21])
		{
			target[index + 21] = (ushort)num22;
			result = true;
		}
		if (num23 != (int)target[index + 22])
		{
			target[index + 22] = (ushort)num23;
			result = true;
		}
		if (num24 != (int)target[index + 23])
		{
			target[index + 23] = (ushort)num24;
			result = true;
		}
		return result;
	}

	private static void CalculateTotalResources(int[] buffer, long[] bufferMul, int[] target)
	{
		int num = buffer[0];
		int num2 = buffer[1];
		int num3 = buffer[2];
		int num4 = buffer[3];
		int num5 = buffer[4];
		int num6 = buffer[5];
		int num7 = buffer[6];
		int num8 = buffer[7];
		int num9 = buffer[8];
		int num10 = buffer[9];
		int num11 = buffer[10];
		int num12 = buffer[11];
		int num13 = buffer[12];
		int num14 = buffer[13];
		int num15 = buffer[14];
		int num16 = buffer[15];
		int num17 = buffer[16];
		int num18 = buffer[17];
		int num19 = buffer[18];
		int num20 = buffer[19];
		int num21 = buffer[20];
		int num22 = buffer[21];
		int num23 = buffer[22];
		int num24 = buffer[23];
		long num25 = bufferMul[0];
		long num26 = bufferMul[1];
		long num27 = bufferMul[2];
		long num28 = bufferMul[3];
		long num29 = bufferMul[4];
		long num30 = bufferMul[5];
		long num31 = bufferMul[6];
		long num32 = bufferMul[7];
		long num33 = bufferMul[8];
		long num34 = bufferMul[9];
		long num35 = bufferMul[10];
		long num36 = bufferMul[11];
		long num37 = bufferMul[12];
		long num38 = bufferMul[13];
		long num39 = bufferMul[14];
		long num40 = bufferMul[15];
		long num41 = bufferMul[17];
		long num42 = bufferMul[18];
		long num43 = bufferMul[19];
		long num44 = bufferMul[20];
		long num45 = bufferMul[23];
		if (num17 != 0)
		{
			num = (int)(num25 / (long)num17);
			num2 = (int)(num26 / (long)num17);
			num3 = (int)(num27 / (long)num17);
			num4 = (int)(num28 / (long)num17);
			num5 = (int)(num29 / (long)num17);
			num6 = (int)(num30 / (long)num17);
			num7 = (int)(num31 / (long)num17);
			num8 = (int)(num32 / (long)num17);
			num10 = (int)(num34 / (long)num17);
			num11 = (int)(num35 / (long)num17);
			num12 = (int)(num36 / (long)num17);
			num13 = (int)(num37 / (long)num17);
			num14 = (int)(num38 / (long)num17);
			num15 = (int)(num39 / (long)num17);
			num16 = (int)(num40 / (long)num17);
			num9 = (int)(num33 / (long)num17);
			num18 = (int)(num41 / (long)num17);
			num19 = (int)(num42 / (long)num17);
			num20 = (int)(num43 / (long)num17);
			num21 = (int)(num44 / (long)num17);
			num24 = (int)(num45 / (long)num17);
		}
		else
		{
			num = 0;
			num2 = 0;
			num3 = 0;
			num4 = 0;
			num5 = 0;
			num6 = 0;
			num7 = 0;
			num8 = 0;
			num10 = 0;
			num11 = 50;
			num12 = 50;
			num13 = 0;
			num14 = 0;
			num15 = 0;
			num9 = 0;
			num18 = 0;
			num19 = 0;
			num20 = 0;
			num21 = 0;
			num24 = 0;
		}
		num16 += num15;
		num = Mathf.Clamp(num, 0, 2147483647);
		num2 = Mathf.Clamp(num2, 0, 2147483647);
		num3 = Mathf.Clamp(num3, 0, 2147483647);
		num4 = Mathf.Clamp(num4, 0, 2147483647);
		num5 = Mathf.Clamp(num5, 0, 2147483647);
		num6 = Mathf.Clamp(num6, 0, 2147483647);
		num7 = Mathf.Clamp(num7, 0, 2147483647);
		num8 = Mathf.Clamp(num8, 0, 2147483647);
		num9 = Mathf.Clamp(num9, 0, 2147483647);
		num10 = Mathf.Clamp(num10, 0, 2147483647);
		num11 = Mathf.Clamp(num11, 0, 2147483647);
		num12 = Mathf.Clamp(num12, 0, 2147483647);
		num13 = Mathf.Clamp(num13, 0, 2147483647);
		num14 = Mathf.Clamp(num14, 0, 2147483647);
		num15 = Mathf.Clamp(num15, 0, 2147483647);
		num16 = Mathf.Clamp(num16, 0, 2147483647);
		num17 = Mathf.Clamp(num17, 0, 2147483647);
		num18 = Mathf.Clamp(num18, 0, 2147483647);
		num19 = Mathf.Clamp(num19, 0, 2147483647);
		num20 = Mathf.Clamp(num20, 0, 2147483647);
		num21 = Mathf.Clamp(num21, 0, 2147483647);
		num22 = Mathf.Clamp(num22, 0, 2147483647);
		num23 = Mathf.Clamp(num23, 0, 2147483647);
		num24 = Mathf.Clamp(num24, 0, 2147483647);
		target[0] = num;
		target[2] = num3;
		target[1] = num2;
		target[3] = num4;
		target[4] = num5;
		target[5] = num6;
		target[6] = num7;
		target[7] = num8;
		target[8] = num9;
		target[9] = num10;
		target[10] = num11;
		target[11] = num12;
		target[12] = num13;
		target[13] = num14;
		target[14] = num15;
		target[15] = num16;
		target[16] = num17;
		target[17] = num18;
		target[18] = num19;
		target[19] = num20;
		target[20] = num21;
		target[21] = num22;
		target[22] = num23;
		target[23] = num24;
	}

	public void CheckResource(ImmaterialResourceManager.Resource resource, Vector3 position, out int local, out int total)
	{
		int num = Mathf.Clamp((int)(position.x / 38.4f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(position.z / 38.4f + 128f), 0, 255);
		int num3 = (int)((num2 * 256 + num) * 24 + resource);
		local = (int)this.m_localFinalResources[num3];
		total = this.m_totalFinalResources[(int)resource];
	}

	public void CheckLocalResource(ImmaterialResourceManager.Resource resource, Vector3 position, out int local)
	{
		int num = Mathf.Clamp((int)(position.x / 38.4f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(position.z / 38.4f + 128f), 0, 255);
		int num3 = (int)((num2 * 256 + num) * 24 + resource);
		local = (int)this.m_localFinalResources[num3];
	}

	public void CheckLocalResources(Vector3 position, out ushort[] resources, out int index)
	{
		int num = Mathf.Clamp((int)(position.x / 38.4f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(position.z / 38.4f + 128f), 0, 255);
		index = (num2 * 256 + num) * 24;
		resources = this.m_localFinalResources;
	}

	public void CheckTotalResource(ImmaterialResourceManager.Resource resource, out int total)
	{
		total = this.m_totalFinalResources[(int)resource];
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			int num = (int)(currentFrameIndex & 255u);
			int num2 = num * 256 >> 8;
			int num3 = num * 256 * 256 >> 8 & 255;
			int num4 = ((num + 1) * 256 * 256 >> 8) - 1 & 255;
			int num5 = -1;
			int maxX = -1;
			for (int i = num3; i <= num4; i++)
			{
				int num6 = (num2 * 256 + i) * 24;
				if (ImmaterialResourceManager.CalculateLocalResources(i, num2, this.m_localTempResources, this.m_globalFinalResources, this.m_localFinalResources, num6))
				{
					if (num5 == -1)
					{
						num5 = i;
					}
					maxX = i;
				}
				int num7 = (int)this.m_localFinalResources[num6 + 16];
				for (int j = 0; j < 24; j++)
				{
					int num8 = (int)this.m_localFinalResources[num6 + j];
					this.m_totalTempResources[j] += num8;
					this.m_totalTempResourcesMul[j] += (long)(num8 * num7);
					this.m_localTempResources[num6 + j] = 0;
				}
			}
			if (num == 255)
			{
				ImmaterialResourceManager.CalculateTotalResources(this.m_totalTempResources, this.m_totalTempResourcesMul, this.m_totalFinalResources);
				StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
				StatisticBase statisticBase = instance.Acquire<StatisticArray>(StatisticType.ImmaterialResource);
				for (int k = 0; k < 24; k++)
				{
					this.m_globalFinalResources[k] = this.m_globalTempResources[k];
					this.m_globalTempResources[k] = 0;
					this.m_totalTempResources[k] = 0;
					this.m_totalTempResourcesMul[k] = 0L;
					statisticBase.Acquire<StatisticInt32>(k, 24).Set(this.m_totalFinalResources[k]);
				}
			}
			if (num5 != -1)
			{
				this.AreaModified(num5, num2, maxX, num2);
			}
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new ImmaterialResourceManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const float RESOURCEGRID_CELL_SIZE = 38.4f;

	public const int RESOURCEGRID_RESOLUTION = 256;

	public const int RESOURCE_COUNT = 24;

	private ushort[] m_localFinalResources;

	private ushort[] m_localTempResources;

	private Texture2D m_resourceTexture;

	private int[] m_globalFinalResources;

	private int[] m_globalTempResources;

	private int[] m_totalFinalResources;

	private int[] m_totalTempResources;

	private long[] m_totalTempResourcesMul;

	private int[] m_modifiedX1;

	private int[] m_modifiedX2;

	private bool m_modified;

	private ImmaterialResourceManager.Resource m_resourceMapVisible = ImmaterialResourceManager.Resource.None;

	private int[] m_tempCircleMinX;

	private int[] m_tempCircleMaxX;

	private float[] m_tempSectorSlopes;

	private float[] m_tempSectorDistances;

	public enum Resource
	{
		HealthCare,
		FireDepartment,
		PoliceDepartment,
		EducationElementary,
		EducationHighSchool,
		EducationUniversity,
		DeathCare,
		PublicTransport,
		NoisePollution,
		CrimeRate,
		Health,
		Wellbeing,
		Density,
		Entertainment,
		LandValue,
		Attractiveness,
		Coverage,
		FireHazard,
		Abandonment,
		CargoTransport,
		RadioCoverage,
		FirewatchCoverage,
		EarthquakeCoverage,
		DisasterCoverage,
		None = 255
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "ImmaterialResourceManager");
			ImmaterialResourceManager instance = Singleton<ImmaterialResourceManager>.get_instance();
			ushort[] localFinalResources = instance.m_localFinalResources;
			int num = localFinalResources.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				uShort.Write(localFinalResources[i]);
			}
			uShort.EndWrite();
			ushort[] localTempResources = instance.m_localTempResources;
			int num2 = localTempResources.Length;
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int j = 0; j < num2; j++)
			{
				uShort2.Write(localTempResources[j]);
			}
			uShort2.EndWrite();
			int[] totalFinalResources = instance.m_totalFinalResources;
			int[] totalTempResources = instance.m_totalTempResources;
			long[] totalTempResourcesMul = instance.m_totalTempResourcesMul;
			int[] globalFinalResources = instance.m_globalFinalResources;
			int[] globalTempResources = instance.m_globalTempResources;
			int num3 = totalFinalResources.Length;
			for (int k = 0; k < num3; k++)
			{
				s.WriteInt32(totalFinalResources[k]);
				s.WriteInt32(totalTempResources[k]);
				s.WriteLong64(totalTempResourcesMul[k]);
				s.WriteInt32(globalFinalResources[k]);
				s.WriteInt32(globalTempResources[k]);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "ImmaterialResourceManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "ImmaterialResourceManager");
			ImmaterialResourceManager instance = Singleton<ImmaterialResourceManager>.get_instance();
			int num = 24;
			if (s.get_version() < 112u)
			{
				num = 17;
			}
			else if (s.get_version() < 133u)
			{
				num = 18;
			}
			else if (s.get_version() < 136u)
			{
				num = 19;
			}
			else if (s.get_version() < 267u)
			{
				num = 20;
			}
			else if (s.get_version() < 268u)
			{
				num = 21;
			}
			else if (s.get_version() < 285u)
			{
				num = 22;
			}
			else if (s.get_version() < 305u)
			{
				num = 23;
			}
			ushort[] localFinalResources = instance.m_localFinalResources;
			int num2 = 65536;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int i = 0; i < num2; i++)
			{
				int num3 = i * 24;
				for (int j = 0; j < num; j++)
				{
					localFinalResources[num3 + j] = uShort.Read();
				}
				for (int k = num; k < 24; k++)
				{
					localFinalResources[num3 + k] = 0;
				}
			}
			uShort.EndRead();
			ushort[] localTempResources = instance.m_localTempResources;
			int num4 = 65536;
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int l = 0; l < num4; l++)
			{
				int num5 = l * 24;
				for (int m = 0; m < num; m++)
				{
					localTempResources[num5 + m] = uShort2.Read();
				}
				for (int n = num; n < 24; n++)
				{
					localTempResources[num5 + n] = 0;
				}
			}
			uShort2.EndRead();
			int[] totalFinalResources = instance.m_totalFinalResources;
			int[] totalTempResources = instance.m_totalTempResources;
			long[] totalTempResourcesMul = instance.m_totalTempResourcesMul;
			int[] globalFinalResources = instance.m_globalFinalResources;
			int[] globalTempResources = instance.m_globalTempResources;
			if (s.get_version() >= 242u)
			{
				for (int num6 = 0; num6 < num; num6++)
				{
					totalFinalResources[num6] = s.ReadInt32();
					totalTempResources[num6] = s.ReadInt32();
					totalTempResourcesMul[num6] = s.ReadLong64();
					globalFinalResources[num6] = s.ReadInt32();
					globalTempResources[num6] = s.ReadInt32();
				}
			}
			else if (s.get_version() >= 147u)
			{
				for (int num7 = 0; num7 < num; num7++)
				{
					totalFinalResources[num7] = s.ReadInt32();
					totalTempResources[num7] = s.ReadInt32();
					totalTempResourcesMul[num7] = (long)s.ReadInt32();
					globalFinalResources[num7] = s.ReadInt32();
					globalTempResources[num7] = s.ReadInt32();
				}
			}
			else
			{
				for (int num8 = 0; num8 < num; num8++)
				{
					totalFinalResources[num8] = s.ReadInt32();
					totalTempResources[num8] = s.ReadInt32();
					totalTempResourcesMul[num8] = (long)s.ReadInt32();
					globalFinalResources[num8] = 0;
					globalTempResources[num8] = 0;
				}
			}
			for (int num9 = num; num9 < 24; num9++)
			{
				totalFinalResources[num9] = 0;
				totalTempResources[num9] = 0;
				totalTempResourcesMul[num9] = 0L;
				globalFinalResources[num9] = 0;
				globalTempResources[num9] = 0;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "ImmaterialResourceManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "ImmaterialResourceManager");
			ImmaterialResourceManager instance = Singleton<ImmaterialResourceManager>.get_instance();
			instance.AreaModified(0, 0, 255, 255);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "ImmaterialResourceManager");
		}
	}
}
