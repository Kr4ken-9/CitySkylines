﻿using System;
using UnityEngine;

[Serializable]
public class VehicleInfoGen : ScriptableObject
{
	public Vector4[] m_tyres;

	public Vector3 m_size;

	public float m_negativeHeight;

	public float m_wheelGauge;

	public float m_wheelBase;

	public float m_triangleArea;

	public float m_uvmapArea;

	[NonSerialized]
	public VehicleInfoBase m_vehicleInfo;
}
