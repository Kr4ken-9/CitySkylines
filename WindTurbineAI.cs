﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class WindTurbineAI : PowerPlantAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Wind)
		{
			return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, Singleton<WeatherManager>.get_instance().SampleWindSpeed(data.m_position, false));
		}
		return base.GetColor(buildingID, ref data, infoMode);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		num = Mathf.RoundToInt((float)num * Singleton<WeatherManager>.get_instance().SampleWindSpeed(data.m_position, false));
		int num2;
		int num3;
		this.GetElectricityProduction(out num2, out num3);
		return num3 * num / 100;
	}

	public override string GetConstructionInfo(int productionRate)
	{
		int num;
		int num2;
		this.GetElectricityProduction(out num, out num2);
		return StringUtils.SafeFormat(Locale.Get("TOOL_ELECTRICITY_ESTIMATE"), num2 * productionRate * 16 / 100000);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Electricity;
		subMode = InfoManager.SubInfoMode.WindPower;
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		num = Mathf.RoundToInt((float)num * Singleton<WeatherManager>.get_instance().SampleWindSpeed(data.m_position, false));
		uint num2 = (uint)(((int)buildingID << 8) / 49152);
		uint num3 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - num2;
		Building.Frame lastFrameData = data.GetLastFrameData();
		data.SetFrameData(num3 - 512u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num3 - 256u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num3, lastFrameData);
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return false;
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		productionRate = Mathf.Clamp(Mathf.RoundToInt(Singleton<WeatherManager>.get_instance().SampleWindSpeed(position, true) * 100f), 0, 100);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_windTurbinePlacement.Activate(properties.m_windTurbinePlacement);
		}
		return result;
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		finalProductionRate = Mathf.RoundToInt((float)finalProductionRate * Singleton<WeatherManager>.get_instance().SampleWindSpeed(buildingData.m_position, false));
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		frameData.m_productionState = (byte)((int)frameData.m_productionState + finalProductionRate);
	}

	protected override void AddProductionCapacity(ref District districtData, int electricityProduction)
	{
		districtData.m_productionData.m_tempElectricityCapacity = districtData.m_productionData.m_tempElectricityCapacity + (uint)electricityProduction;
		districtData.m_productionData.m_tempRenewableElectricity = districtData.m_productionData.m_tempRenewableElectricity + (uint)electricityProduction;
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		onlyCollapse = (this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnWater || this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnSurface);
		return true;
	}

	public override void PlacementFailed()
	{
	}

	public override void PlacementSucceeded()
	{
		Singleton<BuildingManager>.get_instance().m_windTurbinePlacement.Disable();
	}

	public override void GetElectricityProduction(out int min, out int max)
	{
		min = 0;
		max = this.m_electricityProduction;
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string str = base.GetLocalizedStats(buildingID, ref data) + Environment.NewLine;
		int num = Mathf.RoundToInt(Singleton<WeatherManager>.get_instance().SampleWindSpeed(data.m_position, false) * 10f);
		return str + LocaleFormatter.FormatGeneric("AIINFO_WINDSPEED", new object[]
		{
			num
		});
	}
}
