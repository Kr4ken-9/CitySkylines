﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class StatisticMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		string text = this.m_value.ToString();
		if (this.m_arrayIndex != -1)
		{
			StatisticMilestone.ModifyValueName(ref text, this.m_value, this.m_arrayIndex);
		}
		if (this.m_divider != StatisticType.None)
		{
			string text2 = this.m_divider.ToString();
			if (this.m_dividerIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text2, this.m_divider, this.m_dividerIndex);
			}
			text = StringUtils.SafeFormat("{0}/{1}", new object[]
			{
				text,
				text2
			});
		}
		if (this.m_add != StatisticType.None)
		{
			string text3 = this.m_add.ToString();
			if (this.m_addIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text3, this.m_add, this.m_addIndex);
			}
			text = StringUtils.SafeFormat("{0}+{1}", new object[]
			{
				text,
				text3
			});
		}
		if (this.m_subtract != StatisticType.None)
		{
			string text4 = this.m_subtract.ToString();
			if (this.m_subtractIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text4, this.m_subtract, this.m_subtractIndex);
			}
			text = StringUtils.SafeFormat("{0}-{1}", new object[]
			{
				text,
				text4
			});
		}
		switch (this.GetTargetType())
		{
		case StatisticMilestone.TargetType.TotalInt_Greater:
		{
			long value = (long)(this.m_targetTotalInt - this.m_resultOffsetInt);
			this.ModifyValue(ref value);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_TOTAL_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Long(value)
			});
		}
		case StatisticMilestone.TargetType.LatestInt_Greater:
		{
			long value2 = (long)(this.m_targetLatestInt - this.m_resultOffsetInt);
			this.ModifyValue(ref value2);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_LATEST_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Long(value2)
			});
		}
		case StatisticMilestone.TargetType.LatestInt_Less:
		{
			long value3 = (long)(this.m_targetLatestInt - this.m_resultOffsetInt);
			this.ModifyValue(ref value3);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_LATEST_INV_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Long(value3)
			});
		}
		case StatisticMilestone.TargetType.TotalFloat_Greater:
		{
			float targetTotalFloat = this.m_targetTotalFloat;
			this.ModifyValue(ref targetTotalFloat);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_TOTAL_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Float(targetTotalFloat)
			});
		}
		case StatisticMilestone.TargetType.LatestFloat_Greater:
		{
			float targetLatestFloat = this.m_targetLatestFloat;
			this.ModifyValue(ref targetLatestFloat);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_LATEST_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Float(targetLatestFloat)
			});
		}
		case StatisticMilestone.TargetType.LatestFloat_Less:
		{
			float targetLatestFloat2 = this.m_targetLatestFloat;
			this.ModifyValue(ref targetLatestFloat2);
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_STAT_LATEST_INV_DESC", text), new GeneratedString[]
			{
				new GeneratedString.Float(targetLatestFloat2)
			});
		}
		default:
			return new GeneratedString.String(string.Empty);
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		string text = this.m_value.ToString();
		if (this.m_arrayIndex != -1)
		{
			StatisticMilestone.ModifyValueName(ref text, this.m_value, this.m_arrayIndex);
		}
		if (this.m_divider != StatisticType.None)
		{
			string text2 = this.m_divider.ToString();
			if (this.m_dividerIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text2, this.m_divider, this.m_dividerIndex);
			}
			text = StringUtils.SafeFormat("{0}/{1}", new object[]
			{
				text,
				text2
			});
		}
		if (this.m_add != StatisticType.None)
		{
			string text3 = this.m_add.ToString();
			if (this.m_addIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text3, this.m_add, this.m_addIndex);
			}
			text = StringUtils.SafeFormat("{0}+{1}", new object[]
			{
				text,
				text3
			});
		}
		if (this.m_subtract != StatisticType.None)
		{
			string text4 = this.m_subtract.ToString();
			if (this.m_subtractIndex != -1)
			{
				StatisticMilestone.ModifyValueName(ref text4, this.m_subtract, this.m_subtractIndex);
			}
			text = StringUtils.SafeFormat("{0}-{1}", new object[]
			{
				text,
				text4
			});
		}
		StatisticMilestone.TargetType targetType = this.GetTargetType();
		switch (targetType)
		{
		case StatisticMilestone.TargetType.TotalInt_Greater:
		{
			long num = (long)(this.m_targetTotalInt - this.m_resultOffsetInt);
			info.m_min = 0f;
			info.m_max = (float)num;
			long num2;
			if (data != null)
			{
				info.m_passed = (data.m_passedCount != 0);
				info.m_current = (float)data.m_progress;
				num2 = data.m_progress - (long)this.m_resultOffsetInt;
			}
			else
			{
				info.m_passed = false;
				info.m_current = 0f;
				num2 = (long)(-(long)this.m_resultOffsetInt);
			}
			this.ModifyValue(ref num);
			this.ModifyValue(ref num2);
			if (num2 > num && !this.m_checkDisabled)
			{
				num2 = num;
			}
			if (num <= 0L)
			{
				info.m_current = 0f;
			}
			if (info.m_passed)
			{
				num2 = num;
				info.m_current = info.m_max;
			}
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_DESC", text), num);
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_PROG", text), new object[]
			{
				num2,
				num
			});
			break;
		}
		case StatisticMilestone.TargetType.LatestInt_Greater:
		case StatisticMilestone.TargetType.LatestInt_Less:
		{
			long num3 = (long)(this.m_targetLatestInt - this.m_resultOffsetInt);
			info.m_min = 0f;
			info.m_max = Mathf.Max(1f, Mathf.Abs((float)num3));
			long num4;
			if (data != null)
			{
				info.m_passed = (data.m_passedCount != 0);
				if (num3 > 0L != data.m_progress > 0L)
				{
					info.m_current = 0f;
				}
				else
				{
					info.m_current = Mathf.Abs((float)data.m_progress);
				}
				num4 = data.m_progress - (long)this.m_resultOffsetInt;
			}
			else
			{
				info.m_passed = false;
				info.m_current = 0f;
				num4 = (long)(-(long)this.m_resultOffsetInt);
			}
			this.ModifyValue(ref num3);
			this.ModifyValue(ref num4);
			if (targetType == StatisticMilestone.TargetType.LatestInt_Greater)
			{
				if (num4 > num3 && !this.m_checkDisabled)
				{
					num4 = num3;
				}
				if (num3 <= 0L)
				{
					info.m_current = 0f;
				}
				if (info.m_passed)
				{
					num4 = num3;
					info.m_current = info.m_max;
				}
				info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_DESC", text), num3);
				info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_PROG", text), new object[]
				{
					num4,
					num3
				});
			}
			else
			{
				if (num4 < num3 && !this.m_checkDisabled)
				{
					num4 = num3;
				}
				if (num3 >= 0L)
				{
					info.m_current = 0f;
				}
				if (info.m_passed)
				{
					num4 = num3;
					info.m_current = info.m_max;
				}
				info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_DESC", text), num3);
				info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_PROG", text), new object[]
				{
					num4,
					num3
				});
			}
			break;
		}
		case StatisticMilestone.TargetType.TotalFloat_Greater:
		{
			float targetTotalFloat = this.m_targetTotalFloat;
			info.m_min = 0f;
			info.m_max = targetTotalFloat;
			float num5;
			if (data != null)
			{
				info.m_passed = (data.m_passedCount != 0);
				info.m_current = (float)data.m_progress * targetTotalFloat / 100f;
				num5 = (float)data.m_progress * targetTotalFloat / 100f;
			}
			else
			{
				info.m_passed = false;
				info.m_current = 0f;
				num5 = 0f;
			}
			this.ModifyValue(ref targetTotalFloat);
			this.ModifyValue(ref num5);
			if (num5 > targetTotalFloat && !this.m_checkDisabled)
			{
				num5 = targetTotalFloat;
			}
			if (targetTotalFloat <= 0f)
			{
				info.m_current = 0f;
			}
			if (info.m_passed)
			{
				num5 = targetTotalFloat;
				info.m_current = info.m_max;
			}
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_DESC", text), targetTotalFloat);
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_TOTAL_PROG", text), new object[]
			{
				num5,
				targetTotalFloat
			});
			break;
		}
		case StatisticMilestone.TargetType.LatestFloat_Greater:
		case StatisticMilestone.TargetType.LatestFloat_Less:
		{
			float targetLatestFloat = this.m_targetLatestFloat;
			info.m_min = 0f;
			info.m_max = Mathf.Max(0.01f, Mathf.Abs(targetLatestFloat));
			float num6;
			if (data != null)
			{
				info.m_passed = (data.m_passedCount != 0);
				info.m_current = Mathf.Abs((float)data.m_progress * targetLatestFloat / 100f);
				num6 = (float)data.m_progress * targetLatestFloat / 100f;
			}
			else
			{
				info.m_passed = false;
				info.m_current = 0f;
				num6 = 0f;
			}
			this.ModifyValue(ref targetLatestFloat);
			this.ModifyValue(ref num6);
			if (targetType == StatisticMilestone.TargetType.LatestFloat_Greater)
			{
				if (num6 > targetLatestFloat && !this.m_checkDisabled)
				{
					num6 = targetLatestFloat;
				}
				if (targetLatestFloat <= 0f)
				{
					info.m_current = 0f;
				}
				if (info.m_passed)
				{
					num6 = targetLatestFloat;
					info.m_current = info.m_max;
				}
				info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_DESC", text), targetLatestFloat);
				info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_PROG", text), new object[]
				{
					num6,
					targetLatestFloat
				});
			}
			else
			{
				if (num6 < targetLatestFloat && !this.m_checkDisabled)
				{
					num6 = targetLatestFloat;
				}
				if (targetLatestFloat >= 0f)
				{
					info.m_current = 0f;
				}
				if (info.m_passed)
				{
					num6 = targetLatestFloat;
					info.m_current = info.m_max;
				}
				info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_DESC", text), targetLatestFloat);
				info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_STAT_LATEST_INV_PROG", text), new object[]
				{
					num6,
					targetLatestFloat
				});
			}
			break;
		}
		}
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public static void ModifyValueName(ref string name, StatisticType type, int arrayIndex)
	{
		switch (type)
		{
		case StatisticType.CitizenHealth:
		case StatisticType.CitizenHappiness:
		case StatisticType.AbandonedBuildings:
		case StatisticType.BuildingArea:
			name = StringUtils.SafeFormat("{0}.{1}", new object[]
			{
				name,
				StatisticMilestone.m_buildingTypeStrings[arrayIndex]
			});
			return;
		case StatisticType.ImmaterialResource:
		{
			string arg_C7_0 = "{0}.{1}";
			object[] expr_B0 = new object[2];
			expr_B0[0] = name;
			int arg_C6_1 = 1;
			ImmaterialResourceManager.Resource resource = (ImmaterialResourceManager.Resource)arrayIndex;
			expr_B0[arg_C6_1] = resource.ToString();
			name = StringUtils.SafeFormat(arg_C7_0, expr_B0);
			return;
		}
		case StatisticType.PlayerDebt:
		case StatisticType.FullLifespans:
		{
			IL_35:
			if (type == StatisticType.Unemployed || type == StatisticType.EligibleWorkers)
			{
				goto IL_129;
			}
			if (type == StatisticType.SpecializationArea)
			{
				name = StringUtils.SafeFormat("{0}.{1}", new object[]
				{
					name,
					StatisticMilestone.m_specializationTypeStrings[arrayIndex]
				});
				return;
			}
			if (type != StatisticType.PassengerCount)
			{
				return;
			}
			string arg_17A_0 = "{0}.{1}";
			object[] expr_163 = new object[2];
			expr_163[0] = name;
			int arg_179_1 = 1;
			TransportInfo.TransportType transportType = (TransportInfo.TransportType)arrayIndex;
			expr_163[arg_179_1] = transportType.ToString();
			name = StringUtils.SafeFormat(arg_17A_0, expr_163);
			return;
		}
		case StatisticType.ServiceIncome:
		case StatisticType.ServiceExpenses:
			name = StringUtils.SafeFormat("{0}.{1}", new object[]
			{
				name,
				(arrayIndex + ItemClass.Service.Residential).ToString()
			});
			return;
		case StatisticType.StudentCount:
			name = StringUtils.SafeFormat("{0}.Level{1}", new object[]
			{
				name,
				arrayIndex + 1
			});
			return;
		case StatisticType.EducatedCount:
			goto IL_129;
		}
		goto IL_35;
		IL_129:
		string arg_14C_0 = "{0}.{1}";
		object[] expr_135 = new object[2];
		expr_135[0] = name;
		int arg_14B_1 = 1;
		Citizen.Education education = (Citizen.Education)arrayIndex;
		expr_135[arg_14B_1] = education.ToString();
		name = StringUtils.SafeFormat(arg_14C_0, expr_135);
	}

	private void ModifyValue(ref long value)
	{
		StatisticType value2 = this.m_value;
		switch (value2)
		{
		case StatisticType.PlayerMoney:
			break;
		case StatisticType.ElectricityCapacity:
			goto IL_62;
		case StatisticType.WaterCapacity:
		case StatisticType.SewageCapacity:
		case StatisticType.IncinerationCapacity:
			value *= 16L;
			return;
		default:
			switch (value2)
			{
			case StatisticType.PlayerDebt:
			case StatisticType.ServiceIncome:
			case StatisticType.ServiceExpenses:
				break;
			default:
				if (value2 == StatisticType.RenewableElectricity || value2 == StatisticType.PollutingElectricity)
				{
					goto IL_62;
				}
				if (value2 != StatisticType.CityValue)
				{
					return;
				}
				break;
			}
			break;
		}
		value /= 100L;
		return;
		IL_62:
		value = value * 16L / 1000L;
	}

	private void ModifyValue(ref float value)
	{
		if (this.m_divider != StatisticType.None)
		{
			value *= 100f;
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = 1000000000000000000L;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Get(this.m_value);
			StatisticBase statisticBase2 = Singleton<StatisticsManager>.get_instance().Get(this.m_divider);
			StatisticBase statisticBase3 = Singleton<StatisticsManager>.get_instance().Get(this.m_add);
			StatisticBase statisticBase4 = Singleton<StatisticsManager>.get_instance().Get(this.m_subtract);
			if (statisticBase != null && this.m_arrayIndex != -1)
			{
				statisticBase = statisticBase.Get(this.m_arrayIndex);
			}
			if (statisticBase2 != null && this.m_dividerIndex != -1)
			{
				statisticBase2 = statisticBase2.Get(this.m_dividerIndex);
			}
			if (statisticBase3 != null && this.m_addIndex != -1)
			{
				statisticBase3 = statisticBase3.Get(this.m_addIndex);
			}
			if (statisticBase4 != null && this.m_subtractIndex != -1)
			{
				statisticBase4 = statisticBase4.Get(this.m_subtractIndex);
			}
			if (statisticBase != null)
			{
				StatisticMilestone.TargetType targetType = this.GetTargetType();
				switch (targetType)
				{
				case StatisticMilestone.TargetType.TotalInt_Greater:
				{
					long num = statisticBase.GetTotalInt64();
					if (statisticBase2 != null)
					{
						long totalInt = statisticBase2.GetTotalInt64();
						if (totalInt != 0L)
						{
							num /= totalInt;
						}
					}
					if (statisticBase3 != null)
					{
						num += statisticBase3.GetTotalInt64();
					}
					if (statisticBase4 != null)
					{
						num -= statisticBase4.GetTotalInt64();
					}
					num += (long)this.m_resultOffsetInt;
					data.m_progress = num;
					data.m_passedCount = ((num < (long)this.m_targetTotalInt) ? 0 : 1);
					break;
				}
				case StatisticMilestone.TargetType.LatestInt_Greater:
				case StatisticMilestone.TargetType.LatestInt_Less:
				{
					long num2 = statisticBase.GetLatestInt64();
					if (statisticBase2 != null)
					{
						long latestInt = statisticBase2.GetLatestInt64();
						if (latestInt != 0L)
						{
							num2 /= latestInt;
						}
					}
					if (statisticBase3 != null)
					{
						num2 += statisticBase3.GetLatestInt64();
					}
					if (statisticBase4 != null)
					{
						num2 -= statisticBase4.GetLatestInt64();
					}
					num2 += (long)this.m_resultOffsetInt;
					data.m_progress = num2;
					if (targetType == StatisticMilestone.TargetType.LatestInt_Greater)
					{
						data.m_passedCount = ((num2 < (long)this.m_targetLatestInt) ? 0 : 1);
					}
					else
					{
						data.m_passedCount = ((num2 > (long)this.m_targetLatestInt) ? 0 : 1);
					}
					break;
				}
				case StatisticMilestone.TargetType.TotalFloat_Greater:
				{
					float num3 = statisticBase.GetTotalFloat();
					if (statisticBase2 != null)
					{
						float totalFloat = statisticBase2.GetTotalFloat();
						if (totalFloat != 0f)
						{
							num3 /= totalFloat;
						}
					}
					if (statisticBase3 != null)
					{
						num3 += statisticBase3.GetTotalFloat();
					}
					if (statisticBase4 != null)
					{
						num3 -= statisticBase4.GetTotalFloat();
					}
					num3 += this.m_resultOffsetFloat;
					data.m_passedCount = ((num3 < this.m_targetTotalFloat) ? 0 : 1);
					if (this.m_targetTotalFloat != 0f)
					{
						data.m_progress = (long)Mathf.FloorToInt(Mathf.Clamp01(num3 / this.m_targetTotalFloat) * 100f);
					}
					else
					{
						data.m_progress = (long)data.m_passedCount;
					}
					break;
				}
				case StatisticMilestone.TargetType.LatestFloat_Greater:
				case StatisticMilestone.TargetType.LatestFloat_Less:
				{
					float num4 = statisticBase.GetLatestFloat();
					if (statisticBase2 != null)
					{
						float latestFloat = statisticBase2.GetLatestFloat();
						if (latestFloat != 0f)
						{
							num4 /= latestFloat;
						}
					}
					if (statisticBase3 != null)
					{
						num4 += statisticBase3.GetLatestFloat();
					}
					if (statisticBase4 != null)
					{
						num4 -= statisticBase4.GetLatestFloat();
					}
					num4 += this.m_resultOffsetFloat;
					if (targetType == StatisticMilestone.TargetType.LatestFloat_Greater)
					{
						data.m_passedCount = ((num4 < this.m_targetLatestFloat) ? 0 : 1);
					}
					else
					{
						data.m_passedCount = ((num4 > this.m_targetLatestFloat) ? 0 : 1);
					}
					if (this.m_targetLatestFloat != 0f)
					{
						data.m_progress = (long)Mathf.FloorToInt(Mathf.Clamp01(num4 / this.m_targetLatestFloat) * 100f);
					}
					else
					{
						data.m_progress = (long)data.m_passedCount;
					}
					break;
				}
				default:
					data.m_progress = 1000000000000000000L;
					data.m_passedCount = Mathf.Max(1, data.m_passedCount);
					break;
				}
			}
			else
			{
				data.m_progress = 0L;
				data.m_passedCount = 0;
			}
		}
		return data;
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt8((int)this.m_value);
		s.WriteInt8((int)this.m_divider);
		s.WriteInt8((int)this.m_subtract);
		s.WriteInt8(this.m_arrayIndex);
		s.WriteInt8(this.m_dividerIndex);
		s.WriteInt8(this.m_subtractIndex);
		s.WriteInt32(this.m_resultOffsetInt);
		s.WriteInt32(this.m_targetTotalInt);
		s.WriteInt32(this.m_targetLatestInt);
		s.WriteFloat(this.m_resultOffsetFloat);
		s.WriteFloat(this.m_targetTotalFloat);
		s.WriteFloat(this.m_targetLatestFloat);
		s.WriteInt8((int)this.m_add);
		s.WriteInt8(this.m_addIndex);
		s.WriteInt8((int)this.m_targetType);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_value = (StatisticType)s.ReadInt8();
		this.m_divider = (StatisticType)s.ReadInt8();
		this.m_subtract = (StatisticType)s.ReadInt8();
		this.m_arrayIndex = s.ReadInt8();
		this.m_dividerIndex = s.ReadInt8();
		this.m_subtractIndex = s.ReadInt8();
		this.m_resultOffsetInt = s.ReadInt32();
		this.m_targetTotalInt = s.ReadInt32();
		this.m_targetLatestInt = s.ReadInt32();
		this.m_resultOffsetFloat = s.ReadFloat();
		this.m_targetTotalFloat = s.ReadFloat();
		this.m_targetLatestFloat = s.ReadFloat();
		if (s.get_version() >= 294u)
		{
			this.m_add = (StatisticType)s.ReadInt8();
			this.m_addIndex = s.ReadInt8();
		}
		else
		{
			this.m_add = StatisticType.None;
			this.m_addIndex = -1;
		}
		if (s.get_version() >= 278u)
		{
			this.m_targetType = (StatisticMilestone.TargetType)s.ReadInt8();
		}
		else
		{
			this.m_targetType = StatisticMilestone.TargetType.Automatic;
			s.ReadBool();
		}
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		if (this.m_targetTotalInt != 0)
		{
			return Mathf.Clamp01((float)data.m_progress / (float)this.m_targetTotalInt);
		}
		if (this.m_targetLatestInt != 0)
		{
			return Mathf.Clamp01((float)data.m_progress / (float)this.m_targetLatestInt);
		}
		if (this.m_targetTotalFloat != 0f)
		{
			return (float)data.m_progress * 0.01f;
		}
		if (this.m_targetLatestFloat != 0f)
		{
			return (float)data.m_progress * 0.01f;
		}
		return 1f;
	}

	private StatisticMilestone.TargetType GetTargetType()
	{
		if (this.m_targetType != StatisticMilestone.TargetType.Automatic)
		{
			return this.m_targetType;
		}
		if (this.m_targetTotalInt != 0)
		{
			return StatisticMilestone.TargetType.TotalInt_Greater;
		}
		if (this.m_targetLatestInt > 0)
		{
			return StatisticMilestone.TargetType.LatestInt_Greater;
		}
		if (this.m_targetLatestInt < 0)
		{
			return StatisticMilestone.TargetType.LatestInt_Less;
		}
		if (this.m_targetTotalFloat != 0f)
		{
			return StatisticMilestone.TargetType.TotalFloat_Greater;
		}
		if (this.m_targetLatestFloat > 0f)
		{
			return StatisticMilestone.TargetType.LatestFloat_Greater;
		}
		if (this.m_targetLatestFloat < 0f)
		{
			return StatisticMilestone.TargetType.LatestFloat_Less;
		}
		return StatisticMilestone.TargetType.Nothing;
	}

	public StatisticType m_value = StatisticType.None;

	public StatisticType m_divider = StatisticType.None;

	public StatisticType m_add = StatisticType.None;

	public StatisticType m_subtract = StatisticType.None;

	public int m_arrayIndex = -1;

	public int m_dividerIndex = -1;

	public int m_addIndex = -1;

	public int m_subtractIndex = -1;

	public int m_resultOffsetInt;

	public int m_targetTotalInt;

	public int m_targetLatestInt;

	public float m_resultOffsetFloat;

	public float m_targetTotalFloat;

	public float m_targetLatestFloat;

	public StatisticMilestone.TargetType m_targetType;

	private static string[] m_buildingTypeStrings = new string[]
	{
		"Residential",
		"Commercial",
		"Industrial",
		"Office",
		"Player"
	};

	private static string[] m_specializationTypeStrings = new string[]
	{
		"Farming",
		"Forestry",
		"Ore",
		"Oil",
		"Leisure",
		"Tourist",
		"Organic",
		"Selfsufficient",
		"Hightech"
	};

	public enum TargetType
	{
		Automatic,
		Nothing,
		TotalInt_Greater,
		LatestInt_Greater,
		LatestInt_Less,
		TotalFloat_Greater,
		LatestFloat_Greater,
		LatestFloat_Less
	}
}
