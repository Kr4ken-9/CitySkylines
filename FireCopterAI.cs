﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class FireCopterAI : HelicopterAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_fillEffect != null)
		{
			this.m_fillEffect.InitializeEffect();
		}
		if (this.m_dropEndEffect != null)
		{
			this.m_dropEndEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_fillEffect != null)
		{
			this.m_fillEffect.ReleaseEffect();
		}
		if (this.m_dropEndEffect != null)
		{
			this.m_dropEndEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.FireSafety)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_WAIT");
		}
		if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
		{
			if (data.m_targetBuilding != 0)
			{
				if (data.m_transferType == 68)
				{
					target = InstanceID.Empty;
					target.Building = data.m_targetBuilding;
					return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_EMERGENCY2");
				}
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_EMERGENCY");
			}
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_EXTINGUISH");
			}
			if (data.m_transferSize == 0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_FIRE_COPTER_FILLING");
			}
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "FireCopter";
		current = (int)data.m_transferSize;
		max = this.m_fireFightingCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.Landing | Vehicle.Flags.WaitingTarget);
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 0f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 0f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			data.m_flags &= ~Vehicle.Flags.Landing;
			data.m_flags &= ~Vehicle.Flags.GoingBack;
			data.m_flags |= Vehicle.Flags.Emergency2;
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0 && this.m_dropEndEffect != null)
			{
				InstanceID instance = default(InstanceID);
				instance.Vehicle = vehicleID;
				EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(data.GetLastFramePosition(), Vector3.get_up(), 0f);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_dropEndEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<VehicleManager>.get_instance().m_audioGroup);
			}
			data.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2);
			if (!this.ShouldReturnToSource(vehicleID, ref data))
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 3;
				offer.Vehicle = vehicleID;
				if (data.m_sourceBuilding != 0)
				{
					offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
				}
				else
				{
					offer.Position = data.GetLastFramePosition();
				}
				offer.Amount = 1;
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
				data.m_flags |= Vehicle.Flags.WaitingTarget;
			}
			else
			{
				data.m_flags &= ~Vehicle.Flags.Landing;
				data.m_flags |= Vehicle.Flags.GoingBack;
			}
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & (Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_fireFightingCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		frameData.m_blinkState = (((leaderData.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2)) == (Vehicle.Flags)0) ? 0f : 10f);
		if (leaderData.m_transferSize == 0 && (leaderData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && (leaderData.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2)) != (Vehicle.Flags)0)
		{
			this.FindFillLocation(leaderID, ref leaderData);
		}
		base.SimulationStep(vehicleID, ref leaderData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if (leaderData.m_targetBuilding != 0)
		{
			if (leaderData.m_transferType == 68)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
				Vector3 position = instance.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].m_position;
				float maxDistance = info.m_buildingAI.MaxFireDetectDistance(leaderData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_targetBuilding]);
				uint num = FireCopterAI.FindBurningTree((int)vehicleID, position, maxDistance, frameData.m_position);
				if (num != 0u)
				{
					TreeManager instance2 = Singleton<TreeManager>.get_instance();
					position = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num)].Position;
					bool flag = VectorUtils.LengthSqrXZ(position - frameData.m_position) < 100f;
					if (flag)
					{
						if ((leaderData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
						{
							leaderData.m_flags = ((leaderData.m_flags & ~Vehicle.Flags.Emergency2) | Vehicle.Flags.Emergency1);
						}
						if (this.ExtinguishFire(leaderID, ref leaderData, num, ref instance2.m_trees.m_buffer[(int)((UIntPtr)num)]) && FireCopterAI.FindBurningTree((int)vehicleID, position, maxDistance, frameData.m_position) == 0u)
						{
							this.SetTarget(leaderID, ref leaderData, 0);
						}
					}
					else if ((leaderData.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0 && VectorUtils.LengthSqrXZ(position - frameData.m_position) >= 900f)
					{
						leaderData.m_flags = ((leaderData.m_flags & ~Vehicle.Flags.Emergency1) | Vehicle.Flags.Emergency2);
					}
				}
				else
				{
					this.SetTarget(leaderID, ref leaderData, 0);
				}
			}
			else
			{
				BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
				Vector3 vector;
				Quaternion quaternion;
				instance3.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].CalculateMeshPosition(out vector, out quaternion);
				bool flag = VectorUtils.LengthSqrXZ(vector - frameData.m_position) < 64f;
				if (flag)
				{
					if ((leaderData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
					{
						leaderData.m_flags = ((leaderData.m_flags & ~Vehicle.Flags.Emergency2) | Vehicle.Flags.Emergency1);
					}
					if (this.ExtinguishFire(leaderID, ref leaderData, leaderData.m_targetBuilding, ref instance3.m_buildings.m_buffer[(int)leaderData.m_targetBuilding]))
					{
						this.SetTarget(leaderID, ref leaderData, 0);
					}
				}
				else
				{
					if ((leaderData.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0)
					{
						leaderData.m_flags = ((leaderData.m_flags & ~Vehicle.Flags.Emergency1) | Vehicle.Flags.Emergency2);
					}
					if (instance3.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].m_fireIntensity == 0)
					{
						this.SetTarget(leaderID, ref leaderData, 0);
					}
				}
			}
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			if (this.ShouldReturnToSource(leaderID, ref leaderData))
			{
				this.SetTarget(leaderID, ref leaderData, 0);
			}
			else if (leaderData.m_transferSize == 0 && (leaderData.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2)) != (Vehicle.Flags)0)
			{
				this.FindFillLocation(leaderID, ref leaderData);
			}
		}
		else if ((ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 15u) == (ulong)((long)(leaderID & 15)) && !this.ShouldReturnToSource(leaderID, ref leaderData))
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = 3;
			offer.Vehicle = leaderID;
			offer.Position = frameData.m_position;
			offer.Amount = 1;
			offer.Active = true;
			Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)leaderData.m_transferType, offer);
		}
	}

	protected override Vector4 GetTargetPos(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_transferSize == 0 && (vehicleData.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			if (VectorUtils.LengthSqrXZ(vehicleData.GetLastFramePosition() - vehicleData.m_targetPos1) < 10000f && (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 15u) == (ulong)((long)(vehicleID & 15)))
			{
				vehicleData.m_targetPos1.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vehicleData.m_targetPos1, false, 0f);
			}
			return vehicleData.m_targetPos1;
		}
		return vehicleData.m_targetPos0;
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		if (data.m_transferSize == 0 && (data.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.GoingBack)) == (Vehicle.Flags)0)
		{
			data.m_transferSize = (ushort)this.m_fireFightingCapacity;
			data.m_flags &= ~Vehicle.Flags.Landing;
			data.m_flags |= Vehicle.Flags.Emergency2;
			if (this.m_fillEffect != null)
			{
				InstanceID instance = default(InstanceID);
				instance.Vehicle = vehicleID;
				EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(data.GetLastFramePosition(), Vector3.get_up(), 0f);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_fillEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<VehicleManager>.get_instance().m_audioGroup);
			}
		}
		return false;
	}

	private void FindFillLocation(ushort vehicleID, ref Vehicle data)
	{
		Vector2 source = VectorUtils.XZ(data.GetLastFrameData().m_position);
		Vector2 target = VectorUtils.XZ(data.m_targetPos0);
		Vector2 vector = FireCopterAI.FindClosestFillLocation(source, target);
		data.m_targetPos1 = VectorUtils.X_Y(vector);
		data.m_targetPos1.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(VectorUtils.X_Y(vector), false, 0f);
		data.m_targetPos1.w = 2.5f;
		if ((data.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0 && this.m_dropEndEffect != null)
		{
			InstanceID instance = default(InstanceID);
			instance.Vehicle = vehicleID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(data.GetLastFramePosition(), Vector3.get_up(), 0f);
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_dropEndEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<VehicleManager>.get_instance().m_audioGroup);
		}
		data.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.Landing);
	}

	private static Vector2 FindClosestFillLocation(Vector2 source, Vector2 target)
	{
		Segment2 segment;
		segment..ctor(source, target);
		Vector2 vector = segment.Min();
		Vector2 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x + 8640f) / 16f + 0.5f), 0);
		int num2 = Mathf.Max((int)((vector.y + 8640f) / 16f + 0.5f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 8640f) / 16f + 0.5f), 1080);
		int num4 = Mathf.Min((int)((vector2.y + 8640f) / 16f + 0.5f), 1080);
		int num5 = num + 1;
		int num6 = num2 + 1;
		int num7 = num3 - 1;
		int num8 = num4 - 1;
		Vector2 result = Vector2.get_zero();
		float num9 = Vector2.Distance(source, target);
		float num10 = 1000000f;
		float num11 = 0f;
		WaterSimulation.Cell[] array = Singleton<TerrainManager>.get_instance().WaterSimulation.BeginRead();
		try
		{
			while (num != num5 || num2 != num6 || num3 != num7 || num4 != num8)
			{
				for (int i = num2; i <= num4; i++)
				{
					Vector2 vector3;
					vector3.y = (float)i * 16f - 8640f;
					for (int j = num; j <= num3; j++)
					{
						if (j >= num5 && i >= num6 && j <= num7 && i <= num8)
						{
							j = num7;
						}
						else if (array[i * 1081 + j].m_height >= 128)
						{
							vector3.x = (float)j * 16f - 8640f;
							float num12 = Vector2.Distance(source, vector3) + Vector2.Distance(vector3, target) - num9;
							if (num12 < num10)
							{
								if (j > 0 && array[i * 1081 + j - 1].m_height >= 128)
								{
									vector3.x -= 8f;
								}
								else
								{
									num12 += 32f;
								}
								if (j < 1080 && array[i * 1081 + j + 1].m_height >= 128)
								{
									vector3.x += 8f;
								}
								else
								{
									num12 += 32f;
								}
								if (i > 0 && array[(i - 1) * 1081 + j].m_height >= 128)
								{
									vector3.y -= 8f;
								}
								else
								{
									num12 += 32f;
								}
								if (i < 1080 && array[(i + 1) * 1081 + j].m_height >= 128)
								{
									vector3.y += 8f;
								}
								else
								{
									num12 += 32f;
								}
								if (num12 < num10)
								{
									result = vector3;
									num10 = num12;
								}
							}
						}
					}
				}
				if (num10 <= num11)
				{
					return result;
				}
				num11 += 16f;
				num5 = num;
				num6 = num2;
				num7 = num3;
				num8 = num4;
				num = Mathf.Max(num - 1, 0);
				num2 = Mathf.Max(num2 - 1, 0);
				num3 = Mathf.Min(num3 + 1, 1080);
				num4 = Mathf.Min(num4 + 1, 1080);
			}
		}
		finally
		{
			Singleton<TerrainManager>.get_instance().WaterSimulation.EndRead();
		}
		return result;
	}

	private bool ExtinguishFire(ushort vehicleID, ref Vehicle data, ushort buildingID, ref Building buildingData)
	{
		int width = buildingData.Width;
		int length = buildingData.Length;
		int num = Mathf.Min(5000, (int)buildingData.m_fireIntensity * (width * length));
		if (num != 0)
		{
			int num2 = Mathf.Min(this.m_fireFightingRate, (int)data.m_transferSize);
			data.m_transferSize = (ushort)((int)data.m_transferSize - num2);
			if (num2 != 0)
			{
				num2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1, num2);
				num = Mathf.Max(num - num2, 0);
				num /= width * length;
				buildingData.m_fireIntensity = (byte)num;
				if (num == 0)
				{
					InstanceID id = default(InstanceID);
					id.Building = buildingID;
					Singleton<InstanceManager>.get_instance().SetGroup(id, null);
					if (data.m_sourceBuilding != 0)
					{
						int tempExport = (int)Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_tempExport;
						Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_tempExport = (byte)Mathf.Min(tempExport + 1, 255);
					}
					Building.Flags flags = buildingData.m_flags;
					if (buildingData.m_productionRate != 0 && (buildingData.m_flags & Building.Flags.Evacuating) == Building.Flags.None)
					{
						buildingData.m_flags |= Building.Flags.Active;
					}
					Building.Flags flags2 = buildingData.m_flags;
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, buildingData.GetLastFrameData().m_fireDamage == 0 || (buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None);
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
					if (flags2 != flags)
					{
						Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, flags2 ^ flags);
					}
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						Singleton<BuildingManager>.get_instance().m_buildingOnFire.Deactivate(buildingID, false);
					}
				}
				if (buildingData.m_subBuilding != 0 && buildingData.m_parentBuilding == 0)
				{
					int num3 = 0;
					ushort subBuilding = buildingData.m_subBuilding;
					while (subBuilding != 0)
					{
						Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_fireIntensity = (byte)Mathf.Min(num, (int)Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_fireIntensity);
						if (num == 0)
						{
							InstanceID id2 = default(InstanceID);
							id2.Building = subBuilding;
							Singleton<InstanceManager>.get_instance().SetGroup(id2, null);
							Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(subBuilding, true);
							Singleton<BuildingManager>.get_instance().UpdateBuildingColors(subBuilding);
						}
						subBuilding = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
						if (++num3 > 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
		return num == 0;
	}

	private bool ExtinguishFire(ushort vehicleID, ref Vehicle data, uint treeID, ref TreeInstance treeData)
	{
		TreeManager instance = Singleton<TreeManager>.get_instance();
		Vector3 position = treeData.Position;
		bool result = true;
		bool flag = false;
		int num = Mathf.Min(this.m_fireFightingRate, (int)data.m_transferSize);
		int size = instance.m_burningTrees.m_size;
		for (int i = 0; i < size; i++)
		{
			int num2 = (int)instance.m_burningTrees.m_buffer[i].m_fireIntensity;
			if (num2 != 0)
			{
				uint treeIndex = instance.m_burningTrees.m_buffer[i].m_treeIndex;
				if (instance.m_trees.m_buffer[(int)((UIntPtr)treeIndex)].GrowState != 0)
				{
					Vector3 position2 = instance.m_trees.m_buffer[(int)((UIntPtr)treeIndex)].Position;
					float num3 = VectorUtils.LengthSqrXZ(position2 - position);
					if (num3 < 1024f)
					{
						flag = true;
						num2 -= Mathf.Max(1, num / 10);
						if (num2 <= 0)
						{
							instance.m_burningTrees.m_buffer[i].m_fireIntensity = 0;
							TreeInstance.Flags flags = (TreeInstance.Flags)instance.m_trees.m_buffer[(int)((UIntPtr)treeIndex)].m_flags;
							flags &= ~TreeInstance.Flags.Burning;
							instance.m_trees.m_buffer[(int)((UIntPtr)treeIndex)].m_flags = (ushort)flags;
							InstanceID id = default(InstanceID);
							id.Tree = treeIndex;
							Singleton<InstanceManager>.get_instance().SetGroup(id, null);
						}
						else
						{
							instance.m_burningTrees.m_buffer[i].m_fireIntensity = (byte)num2;
							result = false;
						}
					}
				}
			}
		}
		if (flag)
		{
			data.m_transferSize = (ushort)((int)data.m_transferSize - num);
		}
		return result;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 0f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			if (leaderData.m_transferType == 68)
			{
				Vector3 position = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].m_position;
				float maxDistance = info2.m_buildingAI.MaxFireDetectDistance(leaderData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding]);
				uint num = FireCopterAI.FindBurningTree((int)vehicleID, position, maxDistance, refPos);
				if (num != 0u)
				{
					TreeInfo info3 = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num)].Info;
					position = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num)].Position;
					vehicleData.SetTargetPos(index++, this.CalculateForestTargetPoint(refPos, position, minSqrDistance, info3.m_generatedInfo.m_size.y + 30f));
				}
			}
			else
			{
				Vector3 targetPos2;
				Quaternion quaternion;
				instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].CalculateMeshPosition(out targetPos2, out quaternion);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, info2.m_size.y + 30f));
			}
		}
	}

	protected Vector4 CalculateForestTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float height)
	{
		Vector3 vector = targetPos - refPos;
		float num = VectorUtils.LengthSqrXZ(vector);
		Vector4 result;
		if (num > 250000f)
		{
			result = refPos + vector * Mathf.Sqrt(250000f / num);
		}
		else
		{
			result = targetPos;
		}
		result.w = height;
		return result;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			if (vehicleData.m_transferType != 68)
			{
				Vector3 endPos2;
				Quaternion quaternion;
				instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].CalculateMeshPosition(out endPos2, out quaternion);
				return base.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2, info2.m_size.y + 30f);
			}
			Vector3 position = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
			float maxDistance = info2.m_buildingAI.MaxFireDetectDistance(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding]);
			uint num = FireCopterAI.FindBurningTree((int)vehicleID, position, maxDistance, vehicleData.GetLastFramePosition());
			if (num != 0u)
			{
				TreeInfo info3 = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num)].Info;
				position = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num)].Position;
				return base.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, position, info3.m_generatedInfo.m_size.y + 30f);
			}
		}
		return false;
	}

	private static uint FindBurningTree(int seed, Vector3 pos, float maxDistance, Vector3 priorityPos)
	{
		TreeManager instance = Singleton<TreeManager>.get_instance();
		int num = Mathf.Max((int)((pos.x - maxDistance) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((pos.z - maxDistance) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((pos.x + maxDistance) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((pos.z + maxDistance) / 32f + 270f), 539);
		float num5 = maxDistance * maxDistance;
		int num6 = 1000000000;
		uint result = 0u;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num7 = instance.m_treeGrid[i * 540 + j];
				int num8 = 0;
				while (num7 != 0u)
				{
					TreeInstance.Flags flags = (TreeInstance.Flags)instance.m_trees.m_buffer[(int)((UIntPtr)num7)].m_flags;
					if ((flags & TreeInstance.Flags.Burning) != TreeInstance.Flags.None && instance.m_trees.m_buffer[(int)((UIntPtr)num7)].GrowState != 0)
					{
						Vector3 position = instance.m_trees.m_buffer[(int)((UIntPtr)num7)].Position;
						float num9 = VectorUtils.LengthSqrXZ(position - pos);
						if (num9 < num5)
						{
							Randomizer randomizer;
							randomizer..ctor((uint)(seed ^ (int)num7));
							int num10 = Mathf.RoundToInt(VectorUtils.LengthXZ(position - priorityPos));
							num10 = randomizer.Int32(num10 >> 1, num10);
							if (num10 < num6)
							{
								result = num7;
								num6 = num10;
							}
						}
					}
					num7 = instance.m_trees.m_buffer[(int)((UIntPtr)num7)].m_nextGridTree;
					if (++num8 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public EffectInfo m_fillEffect;

	public EffectInfo m_dropEndEffect;

	[CustomizableProperty("Firefighting capacity")]
	public int m_fireFightingCapacity = 1500;

	[CustomizableProperty("Firefighting rate")]
	public int m_fireFightingRate = 150;
}
