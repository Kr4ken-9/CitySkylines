﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class PackageEntry : UICustomControl
{
	public virtual void Reset()
	{
		if (this.m_EntryData != null)
		{
			this.m_EntryData.ResetAttachedEntry(this);
		}
		this.m_EntryData = null;
		if (this.m_SteamTags != null)
		{
			this.m_SteamTags.Hide();
		}
		if (this.m_LastUpdateLabel != null)
		{
			this.m_LastUpdateLabel.Hide();
		}
		if (this.m_UpdateBar != null)
		{
			this.m_UpdateBar.Hide();
		}
		if (this.m_ShareButton != null)
		{
			this.m_ShareButton.Hide();
		}
		if (this.m_GetAD != null)
		{
			this.m_GetAD.Hide();
		}
		if (this.m_GetSF != null)
		{
			this.m_GetSF.Hide();
		}
		if (this.m_GetND != null)
		{
			this.m_GetND.Hide();
		}
		if (this.m_ViewButton != null)
		{
			UIComponent arg_108_0 = this.m_ViewButton;
			bool flag = false;
			this.m_ViewButton.set_isVisible(flag);
			arg_108_0.set_isEnabled(flag);
		}
		if (this.m_OptionsButton != null)
		{
			this.m_OptionsButton.Hide();
		}
		if (this.m_Warning != null)
		{
			this.m_Warning.Hide();
		}
		this.m_WorkshopDetails = default(UGCDetails);
	}

	public void SetEntry(EntryData data)
	{
		this.Reset();
		this.m_EntryData = data;
		this.m_EntryData.attachedEntry = this;
		this.m_WorkshopDetails = this.m_EntryData.workshopDetails;
		if (this.m_EntryData.asset != null)
		{
			this.asset = this.m_EntryData.asset;
		}
		this.publishedFileId = data.publishedFileId;
		this.SetNameLabel(this.entryName, this.authorName);
		if (data.publishedFileId != PublishedFileId.invalid && !data.detailsPending)
		{
			this.SetDetails();
		}
		if (data.publishedFileId == PublishedFileId.invalid)
		{
			this.GetLocalTimeInfo(ref this.m_WorkshopDetails.timeCreated, ref this.m_WorkshopDetails.timeUpdated);
			if (this.m_LastUpdateLabel != null)
			{
				this.m_LastUpdateLabel.set_tooltip(this.FormatTimeInfo());
			}
		}
		if (this.m_ActiveCheckbox != null && this.m_ActiveCheckbox.get_isEnabled())
		{
			this.m_ActiveCheckbox.set_isChecked(this.m_EntryData.IsActive());
		}
		this.UpdateBackground();
		this.OnEntryDataChanged();
	}

	protected bool isWorkshopItem
	{
		get
		{
			return this.package == null && this.asset == null && this.pluginInfo == null;
		}
	}

	protected static string FormatPackageName(string entryName, string authorName, bool isWorkshopItem)
	{
		if (string.IsNullOrEmpty(authorName))
		{
			return entryName;
		}
		if (isWorkshopItem)
		{
			return entryName + " by " + authorName;
		}
		return entryName + "\nby " + authorName;
	}

	public virtual void SetNameLabel(string entryName, string authorName)
	{
		this.m_NameLabel.set_text(PackageEntry.FormatPackageName(entryName, authorName, this.isWorkshopItem));
	}

	public string entryName
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.entryName;
		}
	}

	public string authorName
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.authorName;
		}
	}

	public bool entryActive
	{
		get
		{
			return !(this.m_ActiveCheckbox != null) || this.m_ActiveCheckbox.get_isChecked();
		}
		set
		{
			if (this.m_ActiveCheckbox != null)
			{
				this.m_ActiveCheckbox.set_isChecked(value);
			}
		}
	}

	public bool isDLCDisabled
	{
		get
		{
			return this.m_EntryData != null && !this.m_EntryData.IsDLCEnabled();
		}
	}

	public Package package
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.package;
		}
	}

	private static void SetBadge(UIButton button, SteamHelper.DLC_BitMask requiredMask, SteamHelper.DLC_BitMask ownedMask, SteamHelper.DLC_BitMask dlc, string localeid)
	{
		if (button != null)
		{
			button.set_isVisible((requiredMask & dlc) != (SteamHelper.DLC_BitMask)0);
			button.set_isEnabled((ownedMask & dlc) == (SteamHelper.DLC_BitMask)0);
			button.set_tooltip(((ownedMask & dlc) == (SteamHelper.DLC_BitMask)0) ? Locale.Get(localeid) : string.Empty);
		}
	}

	public virtual Package.Asset asset
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.asset;
		}
		set
		{
			if (this.m_ActiveCheckbox != null && value != null && (value.get_type() == UserAssetType.CustomAssetMetaData || value.get_type() == UserAssetType.MapMetaData || value.get_type() == UserAssetType.ScenarioMetaData))
			{
				SteamHelper.DLC_BitMask requiredMask = this.m_EntryData.requiredMask;
				SteamHelper.DLC_BitMask ownedDLCMask = SteamHelper.GetOwnedDLCMask();
				bool flag = (requiredMask & ~ownedDLCMask) == (SteamHelper.DLC_BitMask)0;
				this.m_ActiveCheckbox.set_isEnabled(flag);
				if (flag)
				{
					this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_ONOFF"));
				}
				else
				{
					this.m_ActiveCheckbox.set_isChecked(false);
					if ((requiredMask & SteamHelper.DLC_BitMask.AfterDarkDLC) != (SteamHelper.DLC_BitMask)0)
					{
						this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_AD_REQUIRED"));
					}
					else if ((requiredMask & SteamHelper.DLC_BitMask.SnowFallDLC) != (SteamHelper.DLC_BitMask)0)
					{
						this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_SF_REQUIRED"));
					}
					else if ((requiredMask & SteamHelper.DLC_BitMask.NaturalDisastersDLC) != (SteamHelper.DLC_BitMask)0)
					{
						this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_ND_REQUIRED"));
					}
					else if ((requiredMask & SteamHelper.DLC_BitMask.InMotionDLC) != (SteamHelper.DLC_BitMask)0)
					{
						this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_MT_REQUIRED"));
					}
					else if ((requiredMask & SteamHelper.DLC_BitMask.GreenCitiesDLC) != (SteamHelper.DLC_BitMask)0)
					{
						this.m_ActiveCheckbox.get_label().set_text(Locale.Get("CONTENT_GC_REQUIRED"));
					}
				}
				PackageEntry.SetBadge(this.m_GetAD, requiredMask, ownedDLCMask, SteamHelper.DLC_BitMask.AfterDarkDLC, "CONTENT_AD_BUY");
				PackageEntry.SetBadge(this.m_GetSF, requiredMask, ownedDLCMask, SteamHelper.DLC_BitMask.SnowFallDLC, "CONTENT_SF_BUY");
				PackageEntry.SetBadge(this.m_GetND, requiredMask, ownedDLCMask, SteamHelper.DLC_BitMask.NaturalDisastersDLC, "CONTENT_ND_BUY");
				PackageEntry.SetBadge(this.m_GetMT, requiredMask, ownedDLCMask, SteamHelper.DLC_BitMask.InMotionDLC, "CONTENT_MT_BUY");
				PackageEntry.SetBadge(this.m_GetGC, requiredMask, ownedDLCMask, SteamHelper.DLC_BitMask.GreenCitiesDLC, "CONTENT_GC_BUY");
			}
		}
	}

	private void OnClick()
	{
		if (this.m_EntryData != null)
		{
			base.get_component().get_parent().GetComponent<CategoryContentPanel>().SelectAsset(this.m_EntryData, !this.m_EntryData.selected, true);
			this.UpdateBackground();
		}
	}

	public void UpdateBackground()
	{
		if (this.m_EntryData != null && this.m_EntryData.selected)
		{
			if (((UIPanel)base.get_component()).get_atlas() != this.m_SelectedBackgroundAtlas)
			{
				((UIPanel)base.get_component()).set_atlas(this.m_SelectedBackgroundAtlas);
			}
			if (((UIPanel)base.get_component()).get_backgroundSprite() != this.m_SelectedBackgroundSprite)
			{
				((UIPanel)base.get_component()).set_backgroundSprite(this.m_SelectedBackgroundSprite);
			}
		}
		else
		{
			if (((UIPanel)base.get_component()).get_atlas() != this.m_DefaultBackgroundAtlas)
			{
				((UIPanel)base.get_component()).set_atlas(this.m_DefaultBackgroundAtlas);
			}
			if (((UIPanel)base.get_component()).get_backgroundSprite() != this.m_DefaultBackgroundSprite)
			{
				((UIPanel)base.get_component()).set_backgroundSprite(this.m_DefaultBackgroundSprite);
			}
		}
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.asset = this.asset;
	}

	public PluginManager.PluginInfo pluginInfo
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.pluginInfo;
		}
	}

	public PublishedFileId publishedFileId
	{
		get
		{
			return (this.m_EntryData == null) ? PublishedFileId.invalid : this.m_EntryData.publishedFileId;
		}
		set
		{
			if (this.m_ViewButton != null)
			{
				UIComponent arg_35_0 = this.m_ViewButton;
				bool flag = this.publishedFileId != PublishedFileId.invalid;
				this.m_ViewButton.set_isVisible(flag);
				arg_35_0.set_isEnabled(flag);
			}
		}
	}

	private string FormatTimeSpan(TimeSpan ts)
	{
		double num = Math.Abs(ts.TotalSeconds);
		if (num < 0.0)
		{
			return "not yet happened ?_?";
		}
		if (num < 60.0)
		{
			return (ts.Seconds != 1) ? LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDSECONDS", new object[]
			{
				ts.Seconds.ToString()
			}) : Locale.Get("CONTENTMANAGER_LASTUPDATEDSECOND");
		}
		if (num < 120.0)
		{
			return Locale.Get("CONTENTMANAGER_LASTUPDATEDMINUTE");
		}
		if (num < 2700.0)
		{
			return LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDMINUTES", new object[]
			{
				ts.Minutes.ToString()
			});
		}
		if (num < 5400.0)
		{
			return Locale.Get("CONTENTMANAGER_LASTUPDATEDHOUR");
		}
		if (num < 86400.0)
		{
			return LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDHOURS", new object[]
			{
				ts.Hours.ToString()
			});
		}
		if (num < 172800.0)
		{
			return Locale.Get("CONTENTMANAGER_LASTUPDATEDDAY");
		}
		if (num < 604800.0)
		{
			return LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDDAYS", new object[]
			{
				ts.Days.ToString()
			});
		}
		if (num < 3024000.0)
		{
			int num2 = Convert.ToInt32(Math.Floor((double)ts.Days / 7.0));
			return (num2 > 1) ? LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDWEEKS", new object[]
			{
				num2.ToString()
			}) : Locale.Get("CONTENTMANAGER_LASTUPDATEDWEEK");
		}
		if (num < 31104000.0)
		{
			int num3 = Convert.ToInt32(Math.Floor((double)ts.Days / 30.0));
			return (num3 > 1) ? LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDMONTHS", new object[]
			{
				num3.ToString()
			}) : Locale.Get("CONTENTMANAGER_LASTUPDATEDMONTH");
		}
		int num4 = Convert.ToInt32(Math.Floor((double)ts.Days / 365.0));
		return (num4 > 1) ? LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATEDYEARS", new object[]
		{
			num4.ToString()
		}) : Locale.Get("CONTENTMANAGER_LASTUPDATEDYEAR");
	}

	private string SmartDateString()
	{
		DateTime updated = this.m_EntryData.updated;
		TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - updated.Ticks);
		string text = this.FormatTimeSpan(ts);
		return LocaleFormatter.FormatGeneric("CONTENTMANAGER_LASTUPDATED", new object[]
		{
			text.ToString()
		});
	}

	protected virtual void Awake()
	{
		this.m_LastUpdateLabel = base.Find<UILabel>("LastUpdate");
		if (this.m_LastUpdateLabel != null)
		{
			this.m_LastUpdateLabel.Hide();
		}
		this.m_SteamTags = base.Find<UILabel>("SteamTags");
		if (this.m_SteamTags != null)
		{
			this.m_LastUpdateLabel.Hide();
		}
		this.m_Image = base.Find<UITextureSprite>("Image");
		this.m_NameLabel = base.Find<UILabel>("Name");
		this.m_ActiveCheckbox = base.Find<UICheckBox>("Active");
		if (this.m_ActiveCheckbox != null)
		{
			this.m_ActiveCheckbox.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnCheckedChanged));
		}
		this.m_UpdateBar = base.Find<UIProgressBar>("UpdateBar");
		if (this.m_UpdateBar != null)
		{
			this.m_UpdateBar.Hide();
		}
		this.m_ShareButton = base.Find<UIButton>("Share");
		if (this.m_ShareButton != null)
		{
			this.m_ShareButton.add_eventClick(new MouseEventHandler(this.OnShare));
			this.m_ShareButton.Hide();
		}
		this.m_GetAD = base.Find<UIButton>("GetAD");
		if (this.m_GetAD != null)
		{
			this.m_GetAD.add_eventClick(new MouseEventHandler(this.OnGetAD));
			this.m_GetAD.Hide();
		}
		this.m_GetSF = base.Find<UIButton>("GetSF");
		if (this.m_GetSF != null)
		{
			this.m_GetSF.add_eventClick(new MouseEventHandler(this.OnGetSF));
			this.m_GetSF.Hide();
		}
		this.m_GetND = base.Find<UIButton>("GetND");
		if (this.m_GetND != null)
		{
			this.m_GetND.add_eventClick(new MouseEventHandler(this.OnGetND));
			this.m_GetND.Hide();
		}
		this.m_GetMT = base.Find<UIButton>("GetMT");
		if (this.m_GetMT != null)
		{
			this.m_GetMT.add_eventClick(new MouseEventHandler(this.OnGetMT));
			this.m_GetMT.Hide();
		}
		this.m_GetGC = base.Find<UIButton>("GetGC");
		if (this.m_GetGC != null)
		{
			this.m_GetGC.add_eventClick(new MouseEventHandler(this.OnGetGC));
			this.m_GetGC.Hide();
		}
		this.m_Warning = base.Find<UITextureSprite>("Warning");
		if (this.m_Warning != null)
		{
			this.m_Warning.Hide();
		}
		this.m_DeleteButton = base.Find<UIButton>("Delete");
		if (this.m_DeleteButton != null)
		{
			this.m_DeleteButton.add_eventClick(new MouseEventHandler(this.OnDelete));
		}
		this.m_ViewButton = base.Find<UIButton>("View");
		if (this.m_ViewButton != null)
		{
			UIComponent arg_317_0 = this.m_ViewButton;
			bool flag = false;
			this.m_ViewButton.set_isVisible(flag);
			arg_317_0.set_isEnabled(flag);
			this.m_ViewButton.add_eventClick(new MouseEventHandler(this.OpenWorkshopInSteam));
		}
		this.m_OptionsButton = base.Find<UIButton>("Options");
		if (this.m_OptionsButton != null)
		{
			this.m_OptionsButton.Hide();
			this.m_OptionsButton.add_eventClick(new MouseEventHandler(this.OpenOptions));
		}
		this.m_StyleStuff = base.Find("StyleStuff");
		if (this.m_StyleStuff != null)
		{
			this.m_StyleStuff.Hide();
		}
		this.m_StyleList = base.Find<UICheckboxDropDown>("Style");
		if (this.m_StyleList != null)
		{
			base.get_component().add_eventPositionChanged(delegate(UIComponent c, Vector2 v)
			{
				this.m_StyleList.ClosePopup();
			});
			this.m_StyleList.add_eventAfterDropdownClose(new UICheckboxDropDown.AfterPopupClosedHandler(this.IncludedInStylesChanged));
		}
		this.m_StyleCount = base.Find<UILabel>("StyleCount");
		this.m_AssetPresetLabel = base.Find<UILabel>("AssetPresetLabel");
		this.m_AssetPresetEnable = base.Find<UIButton>("AssetPresetEnable");
		if (this.m_AssetPresetEnable != null)
		{
			this.m_AssetPresetEnable.add_eventClick(new MouseEventHandler(this.EnableAssetPreset));
		}
		this.m_AssetPresetSubscribe = base.Find<UIButton>("AssetPresetSubscribe");
		if (this.m_AssetPresetSubscribe != null)
		{
			this.m_AssetPresetSubscribe.add_eventClick(new MouseEventHandler(this.SubscribeAssetPreset));
		}
		this.m_DefaultBackgroundAtlas = ((UIPanel)base.get_component()).get_atlas();
		this.m_DefaultBackgroundSprite = ((UIPanel)base.get_component()).get_backgroundSprite();
	}

	public void IncludedInStylesChanged(UIComponent c)
	{
		try
		{
			PackageManager.DisableEvents();
			string text = this.asset.get_package().get_packageName() + "." + this.asset.get_name();
			for (int i = 0; i < this.m_StyleList.get_items().Length; i++)
			{
				Package.Asset asset = (Package.Asset)this.m_StyleList.GetItemUserData(i);
				DistrictStyleMetaData districtStyleMetaData = asset.Instantiate<DistrictStyleMetaData>();
				if (this.m_StyleList.GetChecked(i))
				{
					bool flag = false;
					if (districtStyleMetaData.assets == null)
					{
						districtStyleMetaData.assets = new string[0];
					}
					for (int j = 0; j < districtStyleMetaData.assets.Length; j++)
					{
						if (districtStyleMetaData.assets[j].Equals(text))
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						string[] array = new string[districtStyleMetaData.assets.Length + 1];
						districtStyleMetaData.assets.CopyTo(array, 0);
						array[array.Length - 1] = text;
						districtStyleMetaData.assets = array;
						StylesHelper.SaveStyle(districtStyleMetaData, asset.get_name(), asset.get_isWorkshopAsset(), null);
					}
				}
				else
				{
					for (int k = 0; k < districtStyleMetaData.assets.Length; k++)
					{
						if (districtStyleMetaData.assets[k].Equals(text))
						{
							string[] array2 = new string[districtStyleMetaData.assets.Length - 1];
							Array.Copy(districtStyleMetaData.assets, 0, array2, 0, k);
							Array.Copy(districtStyleMetaData.assets, k + 1, array2, k, districtStyleMetaData.assets.Length - k - 1);
							districtStyleMetaData.assets = array2;
							StylesHelper.SaveStyle(districtStyleMetaData, asset.get_name(), asset.get_isWorkshopAsset(), null);
							break;
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Modding, string.Concat(new object[]
			{
				"Style package saving failed: ",
				ex.GetType(),
				" ",
				ex.Message,
				" ",
				ex.StackTrace
			}));
		}
		finally
		{
			PackageManager.EnabledEvents();
			PackageManager.ForcePackagesChanged();
		}
	}

	public void OpenOptions(UIComponent c, UIMouseEventParameter p)
	{
		OptionsMainPanel optionsMainPanel = UIView.get_library().ShowModal<OptionsMainPanel>("OptionsPanel");
		if (optionsMainPanel != null && this.pluginInfo != null)
		{
			IUserMod[] instances = this.pluginInfo.GetInstances<IUserMod>();
			if (instances.Length == 1)
			{
				optionsMainPanel.SelectMod(instances[0].get_Name());
			}
		}
	}

	public virtual void OpenWorkshopInSteam(UIComponent c, UIMouseEventParameter p)
	{
		if (this.publishedFileId != PublishedFileId.invalid && PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToWorkshopItem(this.publishedFileId);
		}
	}

	private string FormatTimeInfo()
	{
		string text = string.Empty;
		if (this.m_WorkshopDetails.timeCreated != 0u)
		{
			text += LocaleFormatter.FormatGeneric("CONTENTMANAGER_TIMECREATED", new object[]
			{
				PackageEntry.kEpoch.AddSeconds(this.m_WorkshopDetails.timeCreated).ToLocalTime().ToString()
			});
			if (this.m_WorkshopDetails.timeUpdated != 0u)
			{
				text += "\n";
			}
		}
		if (this.m_WorkshopDetails.timeUpdated != 0u)
		{
			text += LocaleFormatter.FormatGeneric("CONTENTMANAGER_TIMEUPDATED", new object[]
			{
				PackageEntry.kEpoch.AddSeconds(this.m_WorkshopDetails.timeUpdated).ToLocalTime().ToString()
			});
		}
		return text;
	}

	public void SetDetails()
	{
		if (this.m_EntryData.publishedFileId != PublishedFileId.invalid)
		{
			if (this.m_LastUpdateLabel != null)
			{
				this.m_LastUpdateLabel.set_tooltip(this.FormatTimeInfo());
			}
			this.SetNameLabel(this.entryName, this.authorName);
			if (this.m_ShareButton != null)
			{
				this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
			}
			this.Update();
		}
	}

	private DateTime GetLocalModTimeCreated(string modPath)
	{
		DateTime dateTime = DateTime.MinValue;
		string[] files = Directory.GetFiles(modPath);
		for (int i = 0; i < files.Length; i++)
		{
			string path = files[i];
			if (Path.GetExtension(path) == PluginManager.kModExtension)
			{
				DateTime creationTimeUtc = File.GetCreationTimeUtc(path);
				if (creationTimeUtc > dateTime)
				{
					dateTime = creationTimeUtc;
				}
			}
		}
		return dateTime;
	}

	public static DateTime GetLocalModTimeUpdated(string modPath)
	{
		DateTime dateTime = DateTime.MinValue;
		string[] files = Directory.GetFiles(modPath);
		for (int i = 0; i < files.Length; i++)
		{
			string path = files[i];
			if (Path.GetExtension(path) == PluginManager.kModExtension)
			{
				DateTime lastWriteTimeUtc = File.GetLastWriteTimeUtc(path);
				if (lastWriteTimeUtc > dateTime)
				{
					dateTime = lastWriteTimeUtc;
				}
			}
		}
		return dateTime;
	}

	private void GetLocalTimeInfo(ref uint created, ref uint updated)
	{
		if (this.asset != null)
		{
			if (this.asset.get_package() != null && !string.IsNullOrEmpty(this.asset.get_package().get_packagePath()))
			{
				if (PackageManager.IsCloudPath(this.package.get_packagePath()) && PlatformService.get_cloud().get_enabled())
				{
					DateTime d = PlatformService.get_cloud().GetTimeStamp(PackageManager.GetCloudPath(this.package.get_packagePath())).ToUniversalTime();
					updated = (uint)(d - PackageEntry.kEpoch).TotalSeconds;
				}
				else
				{
					created = (uint)(File.GetCreationTimeUtc(this.asset.get_package().get_packagePath()) - PackageEntry.kEpoch).TotalSeconds;
					updated = (uint)(File.GetLastWriteTimeUtc(this.asset.get_package().get_packagePath()) - PackageEntry.kEpoch).TotalSeconds;
				}
			}
			else
			{
				created = (uint)(DateTime.Now.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				updated = (uint)(DateTime.Now.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
			}
		}
		else if (this.package != null)
		{
			if (!string.IsNullOrEmpty(this.asset.get_package().get_packagePath()))
			{
				if (PackageManager.IsCloudPath(this.package.get_packagePath()))
				{
					if (PlatformService.get_cloud().get_enabled())
					{
						DateTime timeStamp = PlatformService.get_cloud().GetTimeStamp(PackageManager.GetCloudPath(this.package.get_packagePath()));
						updated = (uint)(timeStamp - PackageEntry.kEpoch).TotalSeconds;
					}
				}
				else
				{
					created = (uint)(File.GetCreationTimeUtc(this.package.get_packagePath()) - PackageEntry.kEpoch).TotalSeconds;
					updated = (uint)(File.GetLastWriteTimeUtc(this.package.get_packagePath()) - PackageEntry.kEpoch).TotalSeconds;
				}
			}
			else
			{
				created = (uint)(DateTime.Now.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				updated = (uint)(DateTime.Now.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
			}
		}
		else if (this.pluginInfo != null)
		{
			created = (uint)(this.GetLocalModTimeCreated(this.pluginInfo.get_modPath()) - PackageEntry.kEpoch).TotalSeconds;
			updated = (uint)(PackageEntry.GetLocalModTimeUpdated(this.pluginInfo.get_modPath()) - PackageEntry.kEpoch).TotalSeconds;
		}
	}

	private void OnCheckedChanged(UIComponent c, bool check)
	{
		if (this.m_EntryData != null && check != this.m_EntryData.IsActive())
		{
			if (Singleton<PopsManager>.get_exists())
			{
				Singleton<PopsManager>.get_instance().UGCManaged(this.m_EntryData.GetNameForTelemetry(), check);
			}
			this.m_EntryData.SetActive(check);
			if (this.pluginInfo != null)
			{
				this.DealWithOptions();
			}
		}
	}

	public void RevertChecked()
	{
		this.m_ActiveCheckbox.set_isChecked(false);
	}

	protected virtual void OnEntryDataChanged()
	{
		if (this.package != null)
		{
			UISprite uISprite = base.Find<UISprite>("CloudSprite");
			if (uISprite != null)
			{
				uISprite.set_isVisible(PackageManager.IsCloudPath(this.package.get_packagePath()));
			}
		}
		if (this.asset != null)
		{
			if (this.asset.get_type() == UserAssetType.MapMetaData)
			{
				MapMetaData mapMetaData = (this.m_EntryData.metaData == null) ? null : ((MapMetaData)this.m_EntryData.metaData);
				if (mapMetaData != null)
				{
					if (this.m_Image != null)
					{
						if (mapMetaData.imageRef != null && this.m_EntryData.previewImage == null)
						{
							this.m_EntryData.previewImage = mapMetaData.imageRef.Instantiate<Texture2D>();
						}
						if (this.m_EntryData.previewImage != null)
						{
							this.m_Image.set_texture(this.m_EntryData.previewImage);
						}
						else
						{
							this.m_Image.set_texture(this.m_DefaultMapPreview);
						}
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
						this.m_DeleteButton.set_isVisible(!mapMetaData.IsBuiltinMap(this.asset.get_package().get_packagePath()));
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_1E7_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_1E7_0.set_isEnabled(flag);
					}
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!mapMetaData.isPublished) ? "CONTENT_MAPUNPUBLISHED" : ((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED"));
						this.m_ShareButton.set_isEnabled(mapMetaData.isPublished);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (mapMetaData.IsBuiltinMap(this.package.get_packagePath()))
						{
							this.m_ShareButton.Hide();
						}
						else if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
				}
			}
			else if (this.asset.get_type() == UserAssetType.SaveGameMetaData)
			{
				SaveGameMetaData saveGameMetaData = (this.m_EntryData.metaData == null) ? null : ((SaveGameMetaData)this.m_EntryData.metaData);
				if (saveGameMetaData != null)
				{
					if (this.m_Image != null)
					{
						if (saveGameMetaData.imageRef != null && this.m_EntryData.previewImage == null)
						{
							this.m_EntryData.previewImage = saveGameMetaData.imageRef.Instantiate<Texture2D>();
						}
						if (this.m_EntryData.previewImage != null)
						{
							this.m_Image.set_texture(this.m_EntryData.previewImage);
						}
						else
						{
							this.m_Image.set_texture(this.m_DefaultMapPreview);
						}
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
						this.m_DeleteButton.set_isVisible(true);
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_490_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_490_0.set_isEnabled(flag);
					}
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
						this.m_ShareButton.set_isEnabled(true);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
					this.RefreshAssetPresetUI(false);
					this.m_WorkshopDetails.timeUpdated = (uint)(saveGameMetaData.timeStamp.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				}
			}
			else if (this.asset.get_type() == UserAssetType.CustomAssetMetaData)
			{
				CustomAssetMetaData customAssetMetaData = (this.m_EntryData.metaData == null) ? null : ((CustomAssetMetaData)this.m_EntryData.metaData);
				if (customAssetMetaData != null)
				{
					if (this.m_SteamTags != null && customAssetMetaData.steamTags != null && customAssetMetaData.steamTags.Length > 0)
					{
						this.m_SteamTags.Show();
						this.m_SteamTags.set_text(string.Join(", ", customAssetMetaData.steamTags));
					}
					else
					{
						this.m_SteamTags.Hide();
					}
					if (this.m_Image != null)
					{
						if (customAssetMetaData.imageRef != null && this.m_EntryData.previewImage == null)
						{
							this.m_EntryData.previewImage = customAssetMetaData.imageRef.Instantiate<Texture2D>();
						}
						if (this.m_EntryData.previewImage != null)
						{
							this.m_Image.set_texture(this.m_EntryData.previewImage);
						}
						else
						{
							this.m_Image.set_texture(this.m_DefaultAssetPreview);
						}
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
						this.m_DeleteButton.set_isVisible(true);
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_791_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_791_0.set_isEnabled(flag);
					}
					SteamHelper.DLC_BitMask assetDLCMask = AssetImporterAssetTemplate.GetAssetDLCMask(customAssetMetaData);
					SteamHelper.DLC_BitMask ownedDLCMask = SteamHelper.GetOwnedDLCMask();
					PackageEntry.SetBadge(this.m_GetAD, assetDLCMask, ownedDLCMask, SteamHelper.DLC_BitMask.AfterDarkDLC, "CONTENT_AD_BUY");
					PackageEntry.SetBadge(this.m_GetSF, assetDLCMask, ownedDLCMask, SteamHelper.DLC_BitMask.SnowFallDLC, "CONTENT_SF_BUY");
					PackageEntry.SetBadge(this.m_GetND, assetDLCMask, ownedDLCMask, SteamHelper.DLC_BitMask.NaturalDisastersDLC, "CONTENT_ND_BUY");
					PackageEntry.SetBadge(this.m_GetMT, assetDLCMask, ownedDLCMask, SteamHelper.DLC_BitMask.InMotionDLC, "CONTENT_MT_BUY");
					PackageEntry.SetBadge(this.m_GetGC, assetDLCMask, ownedDLCMask, SteamHelper.DLC_BitMask.GreenCitiesDLC, "CONTENT_GC_BUY");
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
						this.m_ShareButton.set_isEnabled(true);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
					if (this.m_Warning != null)
					{
						string text = AssetWarning.WarningMessage(customAssetMetaData);
						if (text != string.Empty)
						{
							this.m_Warning.Show();
							this.m_Warning.set_tooltip(Locale.Get("ASSET_WARNING_TOOLTIP") + "\n" + text);
						}
					}
					if (this.m_StyleStuff != null)
					{
						bool isVisible = false;
						for (int i = 0; i < customAssetMetaData.steamTags.Length; i++)
						{
							string text2 = customAssetMetaData.steamTags[i];
							if (this.asset.get_isWorkshopAsset() && (text2.Equals("Residential") || text2.Equals("Commercial") || text2.Equals("Industrial") || text2.Equals("Office")))
							{
								string a = this.package.get_packageName() + "." + this.asset.get_name();
								isVisible = true;
								int num = 0;
								List<PackageEntry.StyleEntryData> list = new List<PackageEntry.StyleEntryData>();
								foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
								{
									UserAssetType.DistrictStyleMetaData
								}))
								{
									if (this.asset != null)
									{
										DistrictStyleMetaData districtStyleMetaData = current.Instantiate<DistrictStyleMetaData>();
										if (districtStyleMetaData != null && !districtStyleMetaData.builtin)
										{
											PackageEntry.StyleEntryData item = default(PackageEntry.StyleEntryData);
											item.text = districtStyleMetaData.name;
											item.userData = current;
											item.isChecked = false;
											for (int j = 0; j < districtStyleMetaData.assets.Length; j++)
											{
												if (a == districtStyleMetaData.assets[j])
												{
													num++;
													item.isChecked = true;
												}
											}
											list.Add(item);
										}
									}
								}
								list.Sort((PackageEntry.StyleEntryData s1, PackageEntry.StyleEntryData s2) => s1.text.CompareTo(s2.text));
								this.m_StyleList.Clear();
								for (int k = 0; k < list.Count; k++)
								{
									this.m_StyleList.AddItem(list[k].text, list[k].isChecked, list[k].userData);
								}
								this.m_StyleCount.set_text(StringUtils.SafeFormat(Locale.Get("CONTENTMANAGER_ASSET_STYLECOUNT"), num));
								break;
							}
						}
						this.m_StyleStuff.set_isVisible(isVisible);
					}
					this.m_WorkshopDetails.timeUpdated = (uint)(customAssetMetaData.timeStamp.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				}
			}
			else if (this.asset.get_type() == UserAssetType.ColorCorrection)
			{
				ColorCorrectionMetaData colorCorrectionMetaData = (this.m_EntryData.metaData == null) ? null : ((ColorCorrectionMetaData)this.m_EntryData.metaData);
				if (colorCorrectionMetaData != null)
				{
					if (this.m_Image != null)
					{
						this.m_Image.set_texture(this.m_DefaultColorCorrectionPreview);
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
						this.m_DeleteButton.set_isVisible(!string.IsNullOrEmpty(this.package.get_packagePath()));
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_D04_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_D04_0.set_isEnabled(flag);
					}
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
						this.m_ShareButton.set_isEnabled(true);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
				}
			}
			else if (this.asset.get_type() == UserAssetType.MapThemeMetaData)
			{
				MapThemeMetaData mapThemeMetaData = (this.m_EntryData.metaData == null) ? null : ((MapThemeMetaData)this.m_EntryData.metaData);
				if (mapThemeMetaData != null)
				{
					if (this.m_Image != null)
					{
						if (mapThemeMetaData.imageRef != null && this.m_EntryData.previewImage == null)
						{
							this.m_EntryData.previewImage = mapThemeMetaData.imageRef.Instantiate<Texture2D>();
						}
						if (this.m_EntryData.previewImage != null)
						{
							this.m_Image.set_texture(this.m_EntryData.previewImage);
						}
						else
						{
							this.m_Image.set_texture(this.m_DefaultMapPreview);
						}
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_F65_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_F65_0.set_isEnabled(flag);
					}
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
						this.m_ShareButton.set_isEnabled(true);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
					this.m_WorkshopDetails.timeUpdated = (uint)(mapThemeMetaData.timeStamp.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				}
			}
			else if (this.asset.get_type() == UserAssetType.ScenarioMetaData)
			{
				ScenarioMetaData scenarioMetaData = (this.m_EntryData.metaData == null) ? null : ((ScenarioMetaData)this.m_EntryData.metaData);
				if (scenarioMetaData != null)
				{
					if (this.m_Image != null)
					{
						if (scenarioMetaData.imageRef != null && this.m_EntryData.previewImage == null)
						{
							this.m_EntryData.previewImage = scenarioMetaData.imageRef.Instantiate<Texture2D>();
						}
						if (this.m_EntryData.previewImage != null)
						{
							this.m_Image.set_texture(this.m_EntryData.previewImage);
						}
						else
						{
							this.m_Image.set_texture(this.m_DefaultMapPreview);
						}
						if (this.m_Image.get_texture() != null)
						{
							this.m_Image.get_texture().set_wrapMode(1);
						}
					}
					if (this.m_DeleteButton != null)
					{
						this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
						this.m_DeleteButton.set_isVisible(!scenarioMetaData.IsBuiltinScenario(this.package.get_packagePath()));
					}
					if (this.m_ViewButton != null)
					{
						UIComponent arg_1211_0 = this.m_ViewButton;
						bool flag = this.publishedFileId != PublishedFileId.invalid;
						this.m_ViewButton.set_isVisible(flag);
						arg_1211_0.set_isEnabled(flag);
					}
					if (this.m_ShareButton != null)
					{
						this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
						this.m_ShareButton.set_isEnabled(true);
						this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
						if (scenarioMetaData.IsBuiltinScenario(this.package.get_packagePath()))
						{
							this.m_ShareButton.Hide();
						}
						else if (this.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
					this.RefreshAssetPresetUI(scenarioMetaData.builtin);
					this.m_WorkshopDetails.timeUpdated = (uint)(scenarioMetaData.timeStamp.ToUniversalTime() - PackageEntry.kEpoch).TotalSeconds;
				}
			}
		}
		else if (this.pluginInfo != null)
		{
			this.DealWithOptions();
			if (this.m_DeleteButton != null)
			{
				this.m_DeleteButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
				this.m_DeleteButton.set_isVisible(!this.pluginInfo.get_isBuiltin());
			}
			if (this.m_ViewButton != null)
			{
				UIComponent arg_13F9_0 = this.m_ViewButton;
				bool flag = this.publishedFileId != PublishedFileId.invalid;
				this.m_ViewButton.set_isVisible(flag);
				arg_13F9_0.set_isEnabled(flag);
			}
			if (this.m_ShareButton != null)
			{
				this.m_ShareButton.set_tooltipLocaleID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_MAPPUBLISHED_UPDATE" : "CONTENT_MAPPUBLISHED");
				this.m_ShareButton.set_isEnabled(true);
				this.m_ShareButton.set_localeID((!(this.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
				if (this.pluginInfo.get_isBuiltin())
				{
					this.m_ShareButton.Hide();
				}
				else if (this.publishedFileId == PublishedFileId.invalid)
				{
					this.m_ShareButton.Show();
				}
				else if (this.m_EntryData.detailsPending)
				{
					this.m_ShareButton.Hide();
				}
				else
				{
					this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
				}
			}
		}
	}

	private void RefreshAssetPresetUI(bool builtin = false)
	{
		if (builtin)
		{
			if (this.m_AssetPresetEnable != null)
			{
				this.m_AssetPresetEnable.Hide();
			}
			if (this.m_AssetPresetLabel != null)
			{
				this.m_AssetPresetLabel.Hide();
			}
			if (this.m_AssetPresetSubscribe != null)
			{
				this.m_AssetPresetSubscribe.Hide();
			}
			return;
		}
		if (this.IsLegacyAssetList())
		{
			string tooltip = Locale.Get("CONTENT_PRESET_NOT_AVAILABLE_TOOLTIP");
			this.m_AssetPresetLabel.Hide();
			this.m_AssetPresetEnable.Show();
			this.m_AssetPresetEnable.set_isEnabled(false);
			this.m_AssetPresetEnable.set_tooltip(tooltip);
			this.m_AssetPresetSubscribe.Show();
			this.m_AssetPresetSubscribe.set_isEnabled(false);
			this.m_AssetPresetSubscribe.set_tooltip(tooltip);
			return;
		}
		int num = this.presetMods.Length + this.presetAssets.Length;
		HashSet<PublishedFileId> missingPresetSubscriptions = this.GetMissingPresetSubscriptions();
		if (this.m_AssetPresetLabel != null)
		{
			this.m_AssetPresetLabel.Show();
			this.m_AssetPresetLabel.set_suffix(" " + num.ToString());
		}
		if (this.m_AssetPresetSubscribe != null)
		{
			this.m_AssetPresetSubscribe.Show();
			if (missingPresetSubscriptions.Count > 0)
			{
				this.m_AssetPresetSubscribe.set_isEnabled(true);
				string text = StringUtils.SafeFormat(Locale.Get("CONTENT_SUBSCRIBEALL_TOOLTIP"), missingPresetSubscriptions.Count);
				int num2 = 0;
				foreach (PublishedFileId current in missingPresetSubscriptions)
				{
					text = text + "\n" + current.get_AsUInt64().ToString();
					num2++;
					if (num2 == 10)
					{
						if (missingPresetSubscriptions.Count > 10)
						{
							text += "\n...";
						}
						break;
					}
				}
				this.m_AssetPresetSubscribe.set_tooltip(text);
			}
			else
			{
				this.m_AssetPresetSubscribe.set_isEnabled(false);
				this.m_AssetPresetSubscribe.set_tooltip(Locale.Get("CONTENT_SUBSCRIBEALL_TOOLTIP_DISABLED"));
			}
		}
		if (this.m_AssetPresetEnable != null)
		{
			this.m_AssetPresetEnable.Show();
			string text2 = Locale.Get("CONTENT_ENABLEALL_TOOLTIP");
			if (num > 0)
			{
				this.m_AssetPresetEnable.set_isEnabled(true);
				if (missingPresetSubscriptions.Count > 0)
				{
					text2 = text2 + ". " + StringUtils.SafeFormat(Locale.Get("CONTENT_ENABLEALL_TOOLTIP_MISSING"), missingPresetSubscriptions.Count);
					int num3 = 0;
					foreach (PublishedFileId current2 in missingPresetSubscriptions)
					{
						text2 = text2 + "\n" + current2.get_AsUInt64().ToString();
						num3++;
						if (num3 == 10)
						{
							if (missingPresetSubscriptions.Count > 10)
							{
								text2 += "\n...";
							}
							break;
						}
					}
				}
			}
			else
			{
				this.m_AssetPresetEnable.set_isEnabled(false);
			}
			this.m_AssetPresetEnable.set_tooltip(text2);
		}
	}

	private void DealWithOptions()
	{
		if (this.m_OptionsButton != null)
		{
			try
			{
				IUserMod[] instances = this.pluginInfo.GetInstances<IUserMod>();
				if (instances.Length == 1)
				{
					MethodInfo method = instances[0].GetType().GetMethod("OnSettingsUI", BindingFlags.Instance | BindingFlags.Public);
					this.m_OptionsButton.set_isEnabled(this.pluginInfo.get_isEnabled());
					this.m_OptionsButton.set_isVisible(method != null);
				}
			}
			catch
			{
				Debug.LogError("Failed to handle options for a mod.");
			}
		}
	}

	protected virtual void Update()
	{
		if (base.get_component().get_isVisible())
		{
			if (this.m_LastUpdateLabel != null)
			{
				this.m_LastUpdateLabel.Show();
				this.m_LastUpdateLabel.set_text(this.SmartDateString());
			}
			if (this.m_UpdateBar != null && this.publishedFileId != PublishedFileId.invalid)
			{
				float subscribedItemProgress = PlatformService.get_workshop().GetSubscribedItemProgress(this.publishedFileId);
				this.m_UpdateBar.set_isVisible(subscribedItemProgress >= 0f);
				this.m_UpdateBar.set_value(subscribedItemProgress);
			}
		}
	}

	private void OnDelete(UIComponent component, UIMouseEventParameter p)
	{
		if (this.publishedFileId == PublishedFileId.invalid)
		{
			ConfirmPanel.ShowModal("CONTENT_CONFIRM_DELETE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					try
					{
						if (this.entryActive && Singleton<PopsManager>.get_exists())
						{
							Singleton<PopsManager>.get_instance().UGCManaged(this.m_EntryData.GetNameForTelemetry(), false);
						}
						if (this.asset != null)
						{
							if (PackageManager.IsCloudPath(this.package.get_packagePath()))
							{
								PlatformService.get_cloud().Delete(PackageManager.GetCloudPath(this.package.get_packagePath()));
							}
							else
							{
								File.Delete(this.package.get_packagePath());
							}
						}
						else if (this.pluginInfo != null)
						{
							this.pluginInfo.Unload();
							DirectoryUtils.DeleteDirectory(this.pluginInfo.get_modPath());
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "An error occurred while deleting asset" + ex.ToString());
					}
				}
			});
		}
		else
		{
			ConfirmPanel.ShowModal("CONTENT_CONFIRM_WORKSHOPDELETE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					if (this.entryActive && Singleton<PopsManager>.get_exists())
					{
						Singleton<PopsManager>.get_instance().UGCManaged(this.m_EntryData.GetNameForTelemetry(), false);
					}
					PlatformService.get_workshop().Unsubscribe(this.publishedFileId);
				}
			});
		}
	}

	private void OnGetAD(UIComponent component, UIMouseEventParameter p)
	{
		Singleton<TelemetryManager>.get_instance().OnGetDLCClicked(this.entryName, this.publishedFileId);
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(369150u, 0);
		}
	}

	private void OnGetSF(UIComponent component, UIMouseEventParameter p)
	{
		Singleton<TelemetryManager>.get_instance().OnGetDLCClicked(this.entryName, this.publishedFileId);
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(420610u, 0);
		}
	}

	private void OnGetND(UIComponent component, UIMouseEventParameter p)
	{
		Singleton<TelemetryManager>.get_instance().OnGetDLCClicked(this.entryName, this.publishedFileId);
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(515191u, 0);
		}
	}

	private void OnGetMT(UIComponent component, UIMouseEventParameter p)
	{
		Singleton<TelemetryManager>.get_instance().OnGetDLCClicked(this.entryName, this.publishedFileId);
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(547502u, 0);
		}
	}

	private void OnGetGC(UIComponent component, UIMouseEventParameter p)
	{
		Singleton<TelemetryManager>.get_instance().OnGetDLCClicked(this.entryName, this.publishedFileId);
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlayToStore(614580u, 0);
		}
	}

	private void OnShare(UIComponent component, UIMouseEventParameter p)
	{
		if (this.asset != null && this.asset.get_type() == UserAssetType.MapMetaData)
		{
			MapMetaData mapMetaData = (this.m_EntryData.metaData == null) ? null : ((MapMetaData)this.m_EntryData.metaData);
			if (mapMetaData.mapThemeRef != null)
			{
				Package.Asset asset = PackageManager.FindAssetByName(mapMetaData.mapThemeRef);
				if (asset == null || !asset.get_isWorkshopAsset())
				{
					ConfirmPanel.ShowModal("SHAREMAP_UNPUBLISHED_THEME", delegate(UIComponent comp, int ret)
					{
						if (ret == 1)
						{
							this.ShareRoutine();
						}
					});
				}
				else
				{
					this.ShareRoutine();
				}
			}
			else
			{
				this.ShareRoutine();
			}
		}
		else
		{
			this.ShareRoutine();
		}
	}

	private void ShareRoutine()
	{
		if (this.pluginInfo != null)
		{
			WorkshopModUploadPanel workshopModUploadPanel = UIView.get_library().ShowModal<WorkshopModUploadPanel>("WorkshopModUploadPanel");
			if (workshopModUploadPanel != null)
			{
				workshopModUploadPanel.SetAsset(this.pluginInfo.get_modPath(), this.publishedFileId);
			}
		}
		else if (this.asset != null && (this.asset.get_type() == UserAssetType.MapMetaData || this.asset.get_type() == UserAssetType.ScenarioMetaData || this.asset.get_type() == UserAssetType.SaveGameMetaData || this.asset.get_type() == UserAssetType.CustomAssetMetaData || this.asset.get_type() == UserAssetType.ColorCorrection || this.asset.get_type() == UserAssetType.DistrictStyleMetaData || this.asset.get_type() == UserAssetType.MapThemeMetaData || this.asset.get_type() == UserAssetType.ScenarioMetaData))
		{
			WorkshopAssetUploadPanel workshopAssetUploadPanel = UIView.get_library().ShowModal<WorkshopAssetUploadPanel>("WorkshopAssetUploadPanel");
			if (workshopAssetUploadPanel != null)
			{
				workshopAssetUploadPanel.SetAsset(this.asset, this.publishedFileId);
			}
		}
	}

	private void EnableAssetPreset(UIComponent component, UIMouseEventParameter eventParam)
	{
		PackageManager.DisableEvents();
		PluginManager.DisableEvents();
		this.DisableAllAssets();
		ModInfo[] presetAssets = this.presetAssets;
		for (int i = 0; i < presetAssets.Length; i++)
		{
			ModInfo info = presetAssets[i];
			Package.Asset asset = this.FindPresetAsset(info);
			if (asset != null)
			{
				asset.set_isEnabled(true);
			}
		}
		ModInfo[] presetMods = this.presetMods;
		for (int j = 0; j < presetMods.Length; j++)
		{
			ModInfo info2 = presetMods[j];
			PluginManager.PluginInfo pluginInfo = this.FindPresetPlugin(info2);
			if (pluginInfo != null)
			{
				try
				{
					pluginInfo.set_isEnabled(true);
				}
				catch (Exception ex)
				{
					Debug.LogError(ex);
				}
			}
		}
		PackageManager.EnabledEvents();
		PluginManager.EnabledEvents();
		PackageManager.ForcePackagesChanged();
		Singleton<PluginManager>.get_instance().ForcePluginsChanged();
	}

	private void SubscribeAssetPreset(UIComponent component, UIMouseEventParameter eventParam)
	{
		HashSet<PublishedFileId> missingPresetSubscriptions = this.GetMissingPresetSubscriptions();
		UIView.Find<UIPanel>("(Library) ContentManagerPanel").GetComponent<ContentManagerPanel>().MassSubscribe(missingPresetSubscriptions);
	}

	private HashSet<PublishedFileId> GetMissingPresetSubscriptions()
	{
		HashSet<PublishedFileId> hashSet = new HashSet<PublishedFileId>();
		ModInfo[] presetAssets = this.presetAssets;
		for (int i = 0; i < presetAssets.Length; i++)
		{
			ModInfo modInfo = presetAssets[i];
			PublishedFileId publishedFileId;
			publishedFileId..ctor(modInfo.modWorkshopID);
			if (publishedFileId != PublishedFileId.invalid && !ContentManagerPanel.subscribedItemsTable.Contains(publishedFileId))
			{
				hashSet.Add(new PublishedFileId(modInfo.modWorkshopID));
			}
		}
		ModInfo[] presetMods = this.presetMods;
		for (int j = 0; j < presetMods.Length; j++)
		{
			ModInfo modInfo2 = presetMods[j];
			PublishedFileId publishedFileId2;
			publishedFileId2..ctor(modInfo2.modWorkshopID);
			if (publishedFileId2 != PublishedFileId.invalid && !ContentManagerPanel.subscribedItemsTable.Contains(publishedFileId2))
			{
				hashSet.Add(new PublishedFileId(modInfo2.modWorkshopID));
			}
		}
		return hashSet;
	}

	private List<ModInfo> GetMissingPresetAssets()
	{
		List<ModInfo> list = new List<ModInfo>();
		ModInfo[] presetAssets = this.presetAssets;
		for (int i = 0; i < presetAssets.Length; i++)
		{
			ModInfo modInfo = presetAssets[i];
			if (this.FindPresetAsset(modInfo) == null)
			{
				list.Add(modInfo);
			}
		}
		ModInfo[] presetMods = this.presetMods;
		for (int j = 0; j < presetMods.Length; j++)
		{
			ModInfo modInfo2 = presetMods[j];
			if (this.FindPresetPlugin(modInfo2) == null)
			{
				list.Add(modInfo2);
			}
		}
		return list;
	}

	private void DisableAllAssets()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.CustomAssetMetaData
		}))
		{
			current.set_isEnabled(false);
		}
		foreach (PluginManager.PluginInfo current2 in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			try
			{
				current2.set_isEnabled(false);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex);
			}
		}
	}

	private bool IsLegacyAssetList()
	{
		SaveGameMetaData saveGameMetaData = (!(this.asset.get_type() == UserAssetType.SaveGameMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((SaveGameMetaData)this.m_EntryData.metaData));
		ScenarioMetaData scenarioMetaData = (!(this.asset.get_type() == UserAssetType.ScenarioMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((ScenarioMetaData)this.m_EntryData.metaData));
		return (saveGameMetaData == null || saveGameMetaData.assets == null) && (scenarioMetaData == null || scenarioMetaData.assets == null);
	}

	private ModInfo[] presetAssets
	{
		get
		{
			SaveGameMetaData saveGameMetaData = (!(this.asset.get_type() == UserAssetType.SaveGameMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((SaveGameMetaData)this.m_EntryData.metaData));
			ScenarioMetaData scenarioMetaData = (!(this.asset.get_type() == UserAssetType.ScenarioMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((ScenarioMetaData)this.m_EntryData.metaData));
			if (saveGameMetaData != null && saveGameMetaData.assets != null)
			{
				return saveGameMetaData.assets;
			}
			if (scenarioMetaData != null && scenarioMetaData.assets != null)
			{
				return scenarioMetaData.assets;
			}
			return new ModInfo[0];
		}
	}

	private ModInfo[] presetMods
	{
		get
		{
			SaveGameMetaData saveGameMetaData = (!(this.asset.get_type() == UserAssetType.SaveGameMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((SaveGameMetaData)this.m_EntryData.metaData));
			ScenarioMetaData scenarioMetaData = (!(this.asset.get_type() == UserAssetType.ScenarioMetaData)) ? null : ((this.m_EntryData.metaData == null) ? null : ((ScenarioMetaData)this.m_EntryData.metaData));
			if (saveGameMetaData != null && saveGameMetaData.mods != null)
			{
				return saveGameMetaData.mods;
			}
			if (scenarioMetaData != null && scenarioMetaData.mods != null)
			{
				return scenarioMetaData.mods;
			}
			return new ModInfo[0];
		}
	}

	private PluginManager.PluginInfo FindPresetPlugin(ModInfo info)
	{
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			if (info.modWorkshopID == current.get_publishedFileID().get_AsUInt64() && info.modName == current.get_name())
			{
				return current;
			}
		}
		return null;
	}

	private Package.Asset FindPresetAsset(ModInfo info)
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.CustomAssetMetaData
		}))
		{
			if (info.modWorkshopID == current.get_package().GetPublishedFileID().get_AsUInt64() && info.modName == current.get_fullName())
			{
				return current;
			}
		}
		return null;
	}

	private static readonly DateTime kEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

	public Texture m_DefaultMapPreview;

	public Texture m_DefaultAssetPreview;

	public Texture m_DefaultColorCorrectionPreview;

	public Texture m_DefaultMapThemePreview;

	protected UITextureSprite m_Image;

	protected UILabel m_NameLabel;

	protected UICheckBox m_ActiveCheckbox;

	protected EntryData m_EntryData;

	protected UILabel m_LastUpdateLabel;

	protected UILabel m_SteamTags;

	protected UIButton m_ShareButton;

	protected UIButton m_DeleteButton;

	protected UIButton m_ViewButton;

	protected UIButton m_OptionsButton;

	protected UIComponent m_StyleStuff;

	protected UIProgressBar m_UpdateBar;

	protected UICheckboxDropDown m_StyleList;

	protected UILabel m_StyleCount;

	protected UIButton m_GetAD;

	protected UIButton m_GetSF;

	protected UIButton m_GetND;

	protected UIButton m_GetMT;

	protected UIButton m_GetGC;

	protected UITextureSprite m_Warning;

	protected UILabel m_AssetPresetLabel;

	protected UIButton m_AssetPresetEnable;

	protected UIButton m_AssetPresetSubscribe;

	protected UGCDetails m_WorkshopDetails;

	protected UITextureAtlas m_DefaultBackgroundAtlas;

	protected string m_DefaultBackgroundSprite;

	public UITextureAtlas m_SelectedBackgroundAtlas;

	[UISprite("m_SelectedBackgroundAtlas")]
	public string m_SelectedBackgroundSprite;

	public struct StyleEntryData
	{
		public string text;

		public bool isChecked;

		public object userData;
	}
}
