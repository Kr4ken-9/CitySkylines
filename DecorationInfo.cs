﻿using System;
using ColossalFramework;
using UnityEngine;

public class DecorationInfo : MonoBehaviour
{
	public void Initialize(TerrainProperties properties)
	{
		this.m_initialized = true;
		MeshFilter component = base.GetComponent<MeshFilter>();
		if (component != null)
		{
			this.m_mesh = component.get_sharedMesh();
		}
		MeshRenderer component2 = base.GetComponent<MeshRenderer>();
		if (component2 != null)
		{
			this.m_renderMaterial = new Material(properties.m_decoRenderShader);
			Material sharedMaterial = component2.get_sharedMaterial();
			if (sharedMaterial == null)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, base.get_gameObject().get_name() + ": NULL material", base.get_gameObject());
			}
			else
			{
				this.m_renderMaterial.CopyPropertiesFromMaterial(sharedMaterial);
			}
		}
	}

	private void OnDestroy()
	{
		if (this.m_renderMaterial != null)
		{
			Object.Destroy(this.m_renderMaterial);
			this.m_renderMaterial = null;
		}
	}

	[NonSerialized]
	public bool m_initialized;

	[NonSerialized]
	public Mesh m_mesh;

	[NonSerialized]
	public Material m_renderMaterial;
}
