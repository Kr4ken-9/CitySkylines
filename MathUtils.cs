﻿using System;
using UnityEngine;

public static class MathUtils
{
	public static Rect LerpRect(Rect src, Rect dst, float lerp)
	{
		src.set_xMin(Mathf.Lerp(src.get_xMin(), dst.get_xMin(), lerp));
		src.set_xMax(Mathf.Lerp(src.get_xMax(), dst.get_xMax(), lerp));
		src.set_yMin(Mathf.Lerp(src.get_yMin(), dst.get_yMin(), lerp));
		src.set_yMax(Mathf.Lerp(src.get_yMax(), dst.get_yMax(), lerp));
		return src;
	}

	public static Vector3 SphericalToCartesian(Vector3 v)
	{
		Vector3 result = default(Vector3);
		result.y = v.x * Mathf.Sin(v.z);
		float num = v.x * Mathf.Cos(v.z);
		result.x = num * Mathf.Cos(v.y);
		result.z = num * Mathf.Sin(v.y);
		return result;
	}

	public static float Remap(float value, float srcMin, float srcMax, float dstMin, float dstMax)
	{
		float num = (value - srcMin) / (srcMax - srcMin);
		return num * (dstMax - dstMin) + dstMin;
	}

	public static float RemapClamp(float value, float srcMin, float srcMax, float dstMin, float dstMax)
	{
		return MathUtils.Remap(Mathf.Clamp(value, srcMin, srcMax), srcMin, srcMax, dstMin, dstMax);
	}

	public static Matrix4x4 RemoveScale(Matrix4x4 mat)
	{
		Matrix4x4 result = mat;
		Vector4 column = result.GetColumn(0);
		Vector4 column2 = result.GetColumn(1);
		Vector4 column3 = result.GetColumn(2);
		Vector3 vector;
		vector..ctor(column.x, column.y, column.z);
		Vector3 vector2;
		vector2..ctor(column2.x, column2.y, column2.z);
		Vector3 vector3;
		vector3..ctor(column3.x, column3.y, column3.z);
		vector.Normalize();
		vector2.Normalize();
		vector3.Normalize();
		result.SetColumn(0, new Vector4(vector.x, vector.y, vector.z, column.w));
		result.SetColumn(1, new Vector4(vector2.x, vector2.y, vector2.z, column2.w));
		result.SetColumn(2, new Vector4(vector3.x, vector3.y, vector3.z, column3.w));
		return result;
	}

	public static float Integrate(float src, float dst, float speed, float time)
	{
		float num = 1f - Mathf.Pow(1f - speed, time);
		return src + (dst - src) * num;
	}

	public static float Determinant3x3(Vector3 a, Vector3 b, Vector3 c)
	{
		return a.x * (b.y * c.z - b.z * c.y) + a.y * (b.z * c.x - b.x * c.z) + a.z * (b.x * c.y - b.y * c.x);
	}

	public static bool PointTetrahedronIntersect(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector3 p, out float u, out float v, out float t)
	{
		u = -1f;
		v = -1f;
		t = -1f;
		Vector3 b2 = b - a;
		Vector3 c2 = c - a;
		Vector3 vector = p - a;
		Vector3 a2 = a - d;
		float num = MathUtils.Determinant3x3(a2, b2, c2);
		if (num == 0f)
		{
			return false;
		}
		num = 1f / num;
		u = MathUtils.Determinant3x3(a2, vector, c2) * num;
		if (u < 0f || u > 1f)
		{
			return false;
		}
		v = MathUtils.Determinant3x3(a2, b2, vector) * num;
		if (v < 0f || v > 1f)
		{
			return false;
		}
		t = -MathUtils.Determinant3x3(vector, b2, c2) * num;
		return t >= 0f && t <= 1f && u + v + t <= 1f;
	}

	public static bool Solve(Vector3 a, Vector3 b, Vector3 c, Vector3 r, out float u, out float v, out float t)
	{
		float num = MathUtils.Determinant3x3(a, b, c);
		if (num == 0f)
		{
			u = 0f;
			v = 0f;
			t = 0f;
			return false;
		}
		num = 1f / num;
		u = MathUtils.Determinant3x3(r, b, c) * num;
		v = MathUtils.Determinant3x3(a, r, c) * num;
		t = MathUtils.Determinant3x3(a, b, r) * num;
		return true;
	}

	public static bool InsideFrustum(Vector3 min, Vector3 max, Plane a, Plane b, Plane c, Plane d, Plane e, Plane f)
	{
		return MathUtils.InsidePlane(min, max, a) && MathUtils.InsidePlane(min, max, b) && MathUtils.InsidePlane(min, max, c) && MathUtils.InsidePlane(min, max, d) && MathUtils.InsidePlane(min, max, e) && MathUtils.InsidePlane(min, max, f);
	}

	public static bool InsidePlane(Vector3 min, Vector3 max, Plane plane)
	{
		Vector3 normal = plane.get_normal();
		float num = plane.get_distance();
		num += normal.x * ((normal.x >= 0f) ? max.x : min.x);
		num += normal.y * ((normal.y >= 0f) ? max.y : min.y);
		num += normal.z * ((normal.z >= 0f) ? max.z : min.z);
		return num >= 0f;
	}

	public static bool InsideFrustum(Vector3 point, float radius, Plane a, Plane b, Plane c, Plane d, Plane e, Plane f)
	{
		radius = -radius;
		return a.GetDistanceToPoint(point) > radius && b.GetDistanceToPoint(point) > radius && c.GetDistanceToPoint(point) > radius && d.GetDistanceToPoint(point) > radius && e.GetDistanceToPoint(point) > radius && f.GetDistanceToPoint(point) > radius;
	}

	public static float SmoothClamp01(float s)
	{
		if (s <= 0f)
		{
			return 0f;
		}
		if (s >= 1f)
		{
			return 1f;
		}
		return 3f * s * s - 2f * s * s * s;
	}

	public static float SmoothStep(float edge0, float edge1, float x)
	{
		x = Mathf.Clamp01((x - edge0) / (edge1 - edge0));
		return x * x * (3f - 2f * x);
	}

	public static float SmootherStep(float edge0, float edge1, float x)
	{
		x = Mathf.Clamp01((x - edge0) / (edge1 - edge0));
		return x * x * x * (x * (x * 6f - 15f) + 10f);
	}
}
