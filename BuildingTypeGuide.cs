﻿using System;
using ColossalFramework.Globalization;

public class BuildingTypeGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, BuildingInfo buildingInfo)
	{
		if (base.CanActivate(guideInfo) && buildingInfo.m_prefabDataIndex != -1)
		{
			GuideTriggerBase.ActivationInfo activationInfo = new BuildingTypeGuide.BuildingTypeActivationInfo(guideInfo, buildingInfo);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class BuildingTypeActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public BuildingTypeActivationInfo(GuideInfo guideInfo, BuildingInfo buildingInfo) : base(guideInfo)
		{
			this.m_buildingInfo = buildingInfo;
		}

		public override string GetName()
		{
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_buildingInfo.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			string icon = this.m_guideInfo.m_icon;
			string thumbnail = this.m_buildingInfo.m_Thumbnail;
			return StringUtils.SafeFormat(icon, thumbnail);
		}

		public override string FormatText(string pattern)
		{
			string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_buildingInfo.m_prefabDataIndex);
			string arg = Locale.Get("BUILDING_TITLE", text);
			return StringUtils.SafeFormat(pattern, arg);
		}

		public override IUITag GetTag()
		{
			string text = this.m_guideInfo.m_tag;
			string arg = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_buildingInfo.m_prefabDataIndex);
			text = StringUtils.SafeFormat(text, arg);
			return MonoTutorialTag.Find(text);
		}

		protected BuildingInfo m_buildingInfo;
	}
}
