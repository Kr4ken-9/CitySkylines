﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class TutorialUITag : MonoTutorialTag
{
	public override bool isDynamic
	{
		get
		{
			return true;
		}
	}

	public UIComponent finalTarget
	{
		get
		{
			return this.m_Target;
		}
	}

	private bool IsEqual(float a, float b)
	{
		return a == b || Mathf.Abs(a - b) < 0.001f;
	}

	private bool IsNotEqual(float a, float b)
	{
		return a != b && Mathf.Abs(a - b) >= 0.001f;
	}

	private bool LesserThan(float a, float b)
	{
		return this.IsNotEqual(a, b) && a < b;
	}

	private bool GreaterThan(float a, float b)
	{
		return this.IsNotEqual(a, b) && a > b;
	}

	private bool IsClipped(UIComponent next, UIComponent parent, out TutorialUITag.ClipDirection dir)
	{
		dir = (TutorialUITag.ClipDirection)0;
		if (parent == null || parent == next)
		{
			return false;
		}
		UIScrollablePanel uIScrollablePanel = parent as UIScrollablePanel;
		if (uIScrollablePanel != null && uIScrollablePanel.get_clipChildren() && uIScrollablePanel.get_isVisible())
		{
			if (this.LesserThan(next.get_relativePosition().x, 0f))
			{
				dir |= TutorialUITag.ClipDirection.Left;
			}
			if (this.GreaterThan(next.get_relativePosition().x + next.get_size().x, parent.get_size().x))
			{
				dir |= TutorialUITag.ClipDirection.Right;
			}
			if (this.LesserThan(next.get_relativePosition().y, 0f))
			{
				dir |= TutorialUITag.ClipDirection.Top;
			}
			if (this.GreaterThan(next.get_relativePosition().y + next.get_size().y, parent.get_size().y))
			{
				dir |= TutorialUITag.ClipDirection.Bottom;
			}
			return dir != (TutorialUITag.ClipDirection)0;
		}
		return false;
	}

	public UIComponent target
	{
		get
		{
			UIComponent uIComponent = this.m_Target;
			while (uIComponent != null)
			{
				TutorialUITag component = uIComponent.GetComponent<TutorialUITag>();
				UIComponent uIComponent2 = (!(component != null) || !(component.m_ParentOverride != null)) ? uIComponent.get_parent() : component.m_ParentOverride;
				TutorialUITag.ClipDirection clipDirection;
				bool flag = this.IsClipped(uIComponent, uIComponent2, out clipDirection);
				if (uIComponent.get_isVisible() && !flag)
				{
					return uIComponent;
				}
				UITabContainer uITabContainer = uIComponent2 as UITabContainer;
				if (uIComponent.get_isVisible() && flag)
				{
					UIScrollablePanel uIScrollablePanel = uIComponent2 as UIScrollablePanel;
					bool flag2 = uIScrollablePanel.get_horizontalScrollbar() != null;
					bool flag3 = uIScrollablePanel.get_verticalScrollbar() != null;
					if ((clipDirection & TutorialUITag.ClipDirection.Left) != (TutorialUITag.ClipDirection)0 && flag2)
					{
						uIComponent = ((!(uIScrollablePanel.get_horizontalScrollbar().get_decrementButton() != null)) ? uIScrollablePanel.get_horizontalScrollbar() : uIScrollablePanel.get_horizontalScrollbar().get_decrementButton());
					}
					else if ((clipDirection & TutorialUITag.ClipDirection.Right) != (TutorialUITag.ClipDirection)0 && flag2)
					{
						uIComponent = ((!(uIScrollablePanel.get_horizontalScrollbar().get_incrementButton() != null)) ? uIScrollablePanel.get_horizontalScrollbar() : uIScrollablePanel.get_horizontalScrollbar().get_incrementButton());
					}
					else if ((clipDirection & TutorialUITag.ClipDirection.Top) != (TutorialUITag.ClipDirection)0 && flag3)
					{
						uIComponent = ((!(uIScrollablePanel.get_verticalScrollbar().get_decrementButton() != null)) ? uIScrollablePanel.get_verticalScrollbar() : uIScrollablePanel.get_verticalScrollbar().get_decrementButton());
					}
					else if ((clipDirection & TutorialUITag.ClipDirection.Bottom) != (TutorialUITag.ClipDirection)0 && flag3)
					{
						uIComponent = ((!(uIScrollablePanel.get_verticalScrollbar().get_incrementButton() != null)) ? uIScrollablePanel.get_verticalScrollbar() : uIScrollablePanel.get_verticalScrollbar().get_incrementButton());
					}
					else
					{
						uIComponent = uIComponent2;
					}
				}
				else if (uITabContainer != null)
				{
					UITabstrip owner = uITabContainer.get_owner();
					uIComponent = owner.get_tabs().get_Item(uIComponent.get_zOrder());
				}
				else
				{
					uIComponent = uIComponent2;
				}
			}
			return null;
		}
	}

	public override Vector3 position
	{
		get
		{
			UIComponent target = this.target;
			if (target == null)
			{
				return this.m_LastKnowPosition;
			}
			return this.m_LastKnowPosition = target.get_transform().get_position();
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_Target = base.GetComponent<UIComponent>();
	}

	public UIComponent m_ParentOverride;

	private UIComponent m_Target;

	private Vector3 m_LastKnowPosition;

	private const float kEpsilon = 0.001f;

	private enum ClipDirection
	{
		Top = 1,
		Bottom,
		Right = 4,
		Left = 8
	}
}
