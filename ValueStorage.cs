﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ValueStorage
{
	public ValueStorage()
	{
		this.m_Values = new Dictionary<string, object>();
	}

	public void Store<T>(string name, T value)
	{
		this.m_Values[name] = value;
	}

	public T Get<T>(string name)
	{
		if (this.m_Values.ContainsKey(name))
		{
			return (T)((object)this.m_Values[name]);
		}
		Debug.Log("Warning: getting invalid value from valuestorage: " + name);
		return default(T);
	}

	private Dictionary<string, object> m_Values;
}
