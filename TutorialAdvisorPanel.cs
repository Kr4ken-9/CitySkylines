﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class TutorialAdvisorPanel : UICustomControl
{
	public static string GetSpriteName(string name)
	{
		return name + "Advisor";
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			base.StartCoroutine(this.ShowWelcome());
		}
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.m_Title.set_text(Locale.Get("TUTORIAL_ADVISER_TITLE", this.m_LocaleID));
		this.m_Text.set_text(Locale.Get("TUTORIAL_ADVISER", this.m_LocaleID));
	}

	public static TutorialAdvisorPanel instance
	{
		get
		{
			return TutorialAdvisorPanel.sInstance;
		}
	}

	private void Awake()
	{
		this.m_Title = base.Find<UILabel>("Title");
		this.m_Icon = base.Find<UISprite>("Icon");
		this.m_Text = base.Find<UILabel>("Text");
		this.m_Sprite = base.Find<UISprite>("Sprite");
		TutorialAdvisorPanel.sInstance = this;
		base.get_component().Hide();
	}

	private void OnDestroy()
	{
		TutorialAdvisorPanel.sInstance = null;
	}

	private Vector3 desiredPosition
	{
		get
		{
			if (this.m_TSContainer.get_absolutePosition().x < this.m_StartSize.x)
			{
				return this.m_LocationHelperOthers.get_position();
			}
			return this.m_LocationHelper169.get_position();
		}
	}

	private Vector2 desiredSize
	{
		get
		{
			if (this.m_Sprite.get_atlas() != null && !string.IsNullOrEmpty(this.m_Sprite.get_spriteName()) && this.m_Sprite.get_atlas().get_Item(this.m_Sprite.get_spriteName()) != null)
			{
				this.m_Sprite.Show();
				return this.m_StartSize;
			}
			this.m_Sprite.Hide();
			return new Vector2(this.m_StartSize.x, this.m_StartSize.y - this.m_Sprite.get_size().y);
		}
	}

	private void Start()
	{
		this.m_StartSize = base.get_component().get_size();
		this.m_TSBar = UIView.Find("TSBar");
		this.m_TSContainer = this.m_TSBar.Find("TSContainer");
		this.m_Trigger = this.m_TSBar.Find<UIMultiStateButton>("AdvisorButton");
		this.SetDefaultMessage();
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		if (this.m_TSContainer != null && base.get_component().get_isVisible() && !ValueAnimator.IsAnimating("TutorialAdvisorPanel") && (this.m_LocationHelperOthers.get_position() == base.get_transform().get_position() || this.m_LocationHelper169.get_position() == base.get_transform().get_position()))
		{
			base.get_component().set_transformPosition(this.desiredPosition);
		}
	}

	private void SetDefaultMessage()
	{
		this.SetBindings("Default", "ToolbarIconZoomOutGlobe", string.Empty);
	}

	private void SetBindings(string localeID, string icon, string sprite)
	{
		this.m_LocaleID = localeID;
		this.m_Title.set_text(Locale.Get("TUTORIAL_ADVISER_TITLE", localeID));
		this.m_Icon.set_spriteName(icon);
		this.m_Text.set_text(Locale.Get("TUTORIAL_ADVISER", localeID));
		this.m_Sprite.set_spriteName(sprite);
		base.get_component().set_size(this.desiredSize);
	}

	[DebuggerHidden]
	public IEnumerator ShowWelcome()
	{
		TutorialAdvisorPanel.<ShowWelcome>c__Iterator0 <ShowWelcome>c__Iterator = new TutorialAdvisorPanel.<ShowWelcome>c__Iterator0();
		<ShowWelcome>c__Iterator.$this = this;
		return <ShowWelcome>c__Iterator;
	}

	public UIComponent Refresh(string localeID)
	{
		return this.Refresh(localeID, string.Empty, string.Empty);
	}

	public UIComponent Refresh(string localeID, string icon, string sprite)
	{
		if (localeID == null)
		{
			this.SetDefaultMessage();
		}
		else
		{
			this.SetBindings(localeID, icon, sprite);
		}
		base.get_component().BringToFront();
		return base.get_component();
	}

	public UIComponent Show(string localeID, string icon, string sprite, float timeout)
	{
		return this.Show(localeID, icon, sprite, timeout, false, false);
	}

	public UIComponent Show(string localeID, string icon, string sprite, float timeout, bool dontUpdate, bool hasAudio)
	{
		this.SetTimeout(timeout);
		if ((base.get_component().get_opacity() != 1f && base.get_component().get_isVisible()) || !base.get_component().get_isVisible())
		{
			base.get_component().Show(true);
			base.get_component().Focus();
			ValueAnimator.Animate("TutorialAdvisorPanel", delegate(float val)
			{
				base.get_transform().set_position(Vector3.Lerp(this.m_Trigger.get_transform().get_position(), this.desiredPosition, val));
				base.get_component().set_size(Vector2.Lerp(this.m_Trigger.get_size(), this.desiredSize, val));
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(0f, 1f, this.m_ShowHideTime));
		}
		if (!dontUpdate)
		{
			this.SetBindings(localeID, icon, sprite);
		}
		if (Singleton<AudioManager>.get_exists() && hasAudio && this.m_NotificationSound != null)
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_NotificationSound, 1f);
		}
		return base.get_component();
	}

	public UIComponent Hide()
	{
		if (base.get_component().get_isVisible())
		{
			ValueAnimator.Animate("TutorialAdvisorPanel", delegate(float val)
			{
				base.get_transform().set_position(Vector3.Lerp(this.m_Trigger.get_transform().get_position(), base.get_transform().get_position(), val));
				base.get_component().set_size(Vector2.Lerp(this.m_Trigger.get_size(), this.desiredSize, val));
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(1f, 0f, this.m_ShowHideTime), delegate
			{
				base.get_component().Hide();
			});
		}
		return base.get_component();
	}

	public bool isOpen
	{
		get
		{
			return this.isVisible && !this.m_IsClosing;
		}
	}

	public void OnClose()
	{
		this.Hide();
		this.m_IsClosing = true;
	}

	public void SetTimeout(float timeout)
	{
		base.get_component().set_zOrder(this.m_TSBar.get_zOrder() - 1);
		this.m_IsFadingOut = false;
		this.m_IsClosing = false;
		this.m_LastShown = Time.get_realtimeSinceStartup();
		this.m_Timeout = timeout;
	}

	public void OnMouseHover(UIComponent comp, UIMouseEventParameter p)
	{
		if (base.get_component() == comp && !this.m_IsClosing)
		{
			this.m_IsFadingOut = false;
			this.m_LastShown = Time.get_realtimeSinceStartup();
			if (base.get_component().get_opacity() != 1f)
			{
				ValueAnimator.Animate("TutorialAdvisorPanel", delegate(float val)
				{
					base.get_component().set_opacity(val);
				}, new AnimatedFloat(0f, 1f, 0.3f));
			}
		}
	}

	private void LateUpdate()
	{
		if (base.get_component().get_isVisible() && !this.m_IsFadingOut && this.m_Timeout > 0f && Time.get_realtimeSinceStartup() - this.m_LastShown > this.m_Timeout)
		{
			this.Hide();
			this.m_IsFadingOut = true;
		}
		this.m_Trigger.set_activeStateIndex((!this.isOpen) ? 0 : 1);
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClose();
			p.Use();
		}
	}

	public AudioClip m_NotificationSound;

	public Transform m_LocationHelperOthers;

	public Transform m_LocationHelper169;

	public float m_ShowHideTime = 0.15f;

	protected UIComponent m_TSBar;

	protected UIComponent m_TSContainer;

	private float m_LastShown;

	private float m_Timeout;

	private bool m_IsClosing;

	private bool m_IsFadingOut;

	private string m_LocaleID = "Default";

	private UIMultiStateButton m_Trigger;

	private UILabel m_Title;

	private UISprite m_Icon;

	private UILabel m_Text;

	private UISprite m_Sprite;

	private Vector2 m_StartSize;

	private static TutorialAdvisorPanel sInstance;
}
