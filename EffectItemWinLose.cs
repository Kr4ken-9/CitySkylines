﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class EffectItemWinLose : EffectSubItem
{
	private void Awake()
	{
		this.m_label = base.Find<UILabel>("Label");
		this.m_loseSprite = base.Find<UISprite>("SpriteLose");
		this.m_winSprite = base.Find<UISprite>("SpriteWin");
	}

	protected override void ShowImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.Win)
		{
			this.m_loseSprite.set_isVisible(false);
			this.m_winSprite.set_isVisible(true);
			this.m_label.set_text(Locale.Get("TRIGGERPANEL_PLAYERWINS"));
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.Lose)
		{
			this.m_loseSprite.set_isVisible(true);
			this.m_winSprite.set_isVisible(false);
			this.m_label.set_text(Locale.Get("TRIGGERPANEL_PLAYERLOSES"));
		}
	}

	protected override void SetDefaultValuesImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.Win)
		{
			base.currentTriggerEffect = base.currentTriggerMilestone.AddEffect<WinEffect>();
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.Lose)
		{
			base.currentTriggerEffect = base.currentTriggerMilestone.AddEffect<LoseEffect>();
		}
	}

	public void Load(WinEffect winEffect)
	{
		base.currentTriggerEffect = winEffect;
		this.m_currentEffectType = EffectItem.EffectType.Win;
		base.Show(EffectItem.EffectType.Win);
	}

	public void Load(LoseEffect loseEffect)
	{
		base.currentTriggerEffect = loseEffect;
		this.m_currentEffectType = EffectItem.EffectType.Lose;
		base.Show(EffectItem.EffectType.Lose);
	}

	protected override void HideImpl()
	{
		base.currentTriggerMilestone.RemoveEffect(base.currentTriggerEffect);
	}

	private UILabel m_label;

	private UISprite m_loseSprite;

	private UISprite m_winSprite;
}
