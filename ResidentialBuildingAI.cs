﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class ResidentialBuildingAI : PrivateBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_info.m_class.m_service != ItemClass.Service.Residential)
		{
			throw new PrefabException(this.m_info, "ResidentialBuildingAI used with other service type (" + this.m_info.m_class.m_service + ")");
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Health:
			if (this.ShowConsumption(buildingID, ref data) && data.m_citizenCount != 0)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, (float)Citizen.GetHealthLevel((int)data.m_health) * 0.2f);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.Happiness:
			IL_14:
			switch (infoMode)
			{
			case InfoManager.InfoMode.BuildingLevel:
				if (this.ShowConsumption(buildingID, ref data))
				{
					Color color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[2];
					Color color2 = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[3];
					Color color3 = Color.Lerp(color, color2, 0.5f) * 0.5f;
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, color3, 0.2f + (float)this.m_info.m_class.m_level * 0.2f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			case InfoManager.InfoMode.Education:
				if (this.ShowConsumption(buildingID, ref data) && data.m_citizenCount != 0)
				{
					InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
					int num;
					int num2;
					if (currentSubMode == InfoManager.SubInfoMode.Default)
					{
						num = (int)(data.m_education1 * 100);
						num2 = (int)(data.m_teens + data.m_youngs + data.m_adults + data.m_seniors);
					}
					else if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
					{
						num = (int)(data.m_education2 * 100);
						num2 = (int)(data.m_youngs + data.m_adults + data.m_seniors);
					}
					else
					{
						num = (int)(data.m_education3 * 100);
						num2 = (int)(data.m_youngs * 2 / 3 + data.m_adults + data.m_seniors);
					}
					if (num2 != 0)
					{
						num = (num + (num2 >> 1)) / num2;
					}
					num = Mathf.Clamp(num, 0, 100);
					return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor, (float)num * 0.01f);
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		case InfoManager.InfoMode.Density:
		{
			if (!this.ShowConsumption(buildingID, ref data) || data.m_citizenCount == 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			int num3 = (int)((data.m_citizenCount - data.m_youngs - data.m_adults - data.m_seniors) * 3);
			int num4 = (int)(data.m_youngs + data.m_adults);
			int seniors = (int)data.m_seniors;
			if (num3 == 0 && num4 == 0 && seniors == 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (num3 >= num4 && num3 >= seniors)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			if (num4 >= seniors)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColorB;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		}
		goto IL_14;
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.PrivateIncome)
		{
			Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
			return behaviourData.m_incomeAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
		return -((behaviourData.m_electricityConsumption + 99) / 100);
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
		return -((behaviourData.m_waterConsumption + 99) / 100);
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
		return (behaviourData.m_garbageAccumulation + 99) / 100;
	}

	public override string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		if ((data.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_IMPOSSIBLE");
		}
		if (this.m_info.m_class.m_level == ItemClass.Level.Level5)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_RESIDENTIAL_HAPPY");
		}
		if (data.m_problems != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_DISTRESS");
		}
		if (data.m_levelUpProgress == 0)
		{
			return base.GetLevelUpInfo(buildingID, ref data, out progress);
		}
		if (data.m_levelUpProgress == 1)
		{
			progress = 0.933333337f;
			return Locale.Get("LEVELUP_HIGHRISE_BAN");
		}
		int num = (int)((data.m_levelUpProgress & 15) - 1);
		int num2 = (data.m_levelUpProgress >> 4) - 1;
		if (num <= num2)
		{
			progress = (float)num * 0.06666667f;
			return Locale.Get("LEVELUP_LOWTECH");
		}
		progress = (float)num2 * 0.06666667f;
		return Locale.Get("LEVELUP_LOWLANDVALUE");
	}

	protected override string GetLocalizedStatusInactive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EVACUATED");
		}
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
		return StringUtils.SafeFormat(Locale.Get("BUILDING_STATUS_HOUSEHOLDS"), new object[]
		{
			num4,
			num3
		});
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.GetHomeBehaviour(buildingID, ref data, ref behaviourData, ref num, ref num2, ref num3, ref num4, ref num5);
		return StringUtils.SafeFormat(Locale.Get("BUILDING_STATUS_HOUSEHOLDS"), new object[]
		{
			num4,
			num3
		});
	}

	public override BuildingInfoSub FindCollapsedInfo(out int rotations)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (!(instance.m_common != null))
		{
			rotations = 0;
			return null;
		}
		if (this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLow || this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLowEco)
		{
			return base.FindCollapsedInfo(instance.m_common.m_collapsedLow, out rotations);
		}
		return base.FindCollapsedInfo(instance.m_common.m_collapsedHigh, out rotations);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
		if (specialization != DistrictPolicies.Specialization.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			District[] expr_39_cp_0 = instance.m_districts.m_buffer;
			byte expr_39_cp_1 = district;
			expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect = (expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect | specialization);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainPopulation, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LosePopulation, position, 1.5f);
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			instance2.m_districts.m_buffer[(int)district].AddResidentialData(buildingData.Width * buildingData.Length, (buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255, this.m_info.m_class.m_subService);
		}
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0)
		{
			if ((this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialHigh || this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialHighEco) && (cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && this.m_info.m_class.m_level == ItemClass.Level.Level5 && instance.m_randomizer.Int32(10u) == 0 && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
			{
				District[] expr_184_cp_0 = instance2.m_districts.m_buffer;
				byte expr_184_cp_1 = district;
				expr_184_cp_0[(int)expr_184_cp_1].m_cityPlanningPoliciesEffect = (expr_184_cp_0[(int)expr_184_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				buildingData.m_flags |= Building.Flags.Demolishing;
				instance.m_currentBuildIndex += 1u;
			}
			if (instance.m_randomizer.Int32(10u) == 0)
			{
				DistrictPolicies.Specialization specializationPolicies = instance2.m_districts.m_buffer[(int)district].m_specializationPolicies;
				DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
				if (specialization != DistrictPolicies.Specialization.None)
				{
					if ((specializationPolicies & specialization) == DistrictPolicies.Specialization.None)
					{
						if (Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
						{
							buildingData.m_flags |= Building.Flags.Demolishing;
							instance.m_currentBuildIndex += 1u;
						}
					}
					else
					{
						District[] expr_240_cp_0 = instance2.m_districts.m_buffer;
						byte expr_240_cp_1 = district;
						expr_240_cp_0[(int)expr_240_cp_1].m_specializationPoliciesEffect = (expr_240_cp_0[(int)expr_240_cp_1].m_specializationPoliciesEffect | specialization);
					}
				}
				else if ((specializationPolicies & DistrictPolicies.Specialization.Selfsufficient) != DistrictPolicies.Specialization.None && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
				{
					buildingData.m_flags |= Building.Flags.Demolishing;
					instance.m_currentBuildIndex += 1u;
				}
			}
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int citizenCount = 0;
		int num2 = 0;
		int aliveHomeCount = 0;
		int num3 = 0;
		base.GetHomeBehaviour(buildingID, ref buildingData, ref behaviourData, ref num, ref citizenCount, ref num2, ref aliveHomeCount, ref num3);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Taxation taxationPolicies = instance.m_districts.m_buffer[(int)district].m_taxationPolicies;
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		District[] expr_9F_cp_0 = instance.m_districts.m_buffer;
		byte expr_9F_cp_1 = district;
		expr_9F_cp_0[(int)expr_9F_cp_1].m_servicePoliciesEffect = (expr_9F_cp_0[(int)expr_9F_cp_1].m_servicePoliciesEffect | (servicePolicies & (DistrictPolicies.Services.PowerSaving | DistrictPolicies.Services.WaterSaving | DistrictPolicies.Services.SmokeDetectors | DistrictPolicies.Services.PetBan | DistrictPolicies.Services.Recycling | DistrictPolicies.Services.SmokingBan | DistrictPolicies.Services.ExtraInsulation | DistrictPolicies.Services.NoElectricity | DistrictPolicies.Services.OnlyElectricity)));
		if (this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLow || this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLowEco)
		{
			if ((taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseResLow | DistrictPolicies.Taxation.TaxLowerResLow)) != (DistrictPolicies.Taxation.TaxRaiseResLow | DistrictPolicies.Taxation.TaxLowerResLow))
			{
				District[] expr_FF_cp_0 = instance.m_districts.m_buffer;
				byte expr_FF_cp_1 = district;
				expr_FF_cp_0[(int)expr_FF_cp_1].m_taxationPoliciesEffect = (expr_FF_cp_0[(int)expr_FF_cp_1].m_taxationPoliciesEffect | (taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseResLow | DistrictPolicies.Taxation.TaxLowerResLow)));
			}
		}
		else if ((taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseResHigh | DistrictPolicies.Taxation.TaxLowerResHigh)) != (DistrictPolicies.Taxation.TaxRaiseResHigh | DistrictPolicies.Taxation.TaxLowerResHigh))
		{
			District[] expr_134_cp_0 = instance.m_districts.m_buffer;
			byte expr_134_cp_1 = district;
			expr_134_cp_0[(int)expr_134_cp_1].m_taxationPoliciesEffect = (expr_134_cp_0[(int)expr_134_cp_1].m_taxationPoliciesEffect | (taxationPolicies & (DistrictPolicies.Taxation.TaxRaiseResHigh | DistrictPolicies.Taxation.TaxLowerResHigh)));
		}
		District[] expr_158_cp_0 = instance.m_districts.m_buffer;
		byte expr_158_cp_1 = district;
		expr_158_cp_0[(int)expr_158_cp_1].m_cityPlanningPoliciesEffect = (expr_158_cp_0[(int)expr_158_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & (DistrictPolicies.CityPlanning.HighTechHousing | DistrictPolicies.CityPlanning.HeavyTrafficBan | DistrictPolicies.CityPlanning.EncourageBiking | DistrictPolicies.CityPlanning.BikeBan | DistrictPolicies.CityPlanning.OldTown | DistrictPolicies.CityPlanning.AntiSlip | DistrictPolicies.CityPlanning.LightningRods | DistrictPolicies.CityPlanning.VIPArea | DistrictPolicies.CityPlanning.CombustionEngineBan | DistrictPolicies.CityPlanning.ElectricCars)));
		int num4;
		int num5;
		int num6;
		int num7;
		int num8;
		this.GetConsumptionRates(new Randomizer((int)buildingID), 100, out num4, out num5, out num6, out num7, out num8);
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num4 = 0;
			num5 = 0;
			num6 = 0;
			num7 = 0;
			num8 = 0;
		}
		else
		{
			num4 = 1 + (num4 * behaviourData.m_electricityConsumption + 9999) / 10000;
			num5 = 1 + (num5 * behaviourData.m_waterConsumption + 9999) / 10000;
			num6 = 1 + (num6 * behaviourData.m_sewageAccumulation + 9999) / 10000;
			num7 = (num7 * behaviourData.m_garbageAccumulation + 9999) / 10000;
			num8 = (num8 * behaviourData.m_incomeAccumulation + 9999) / 10000;
		}
		int heatingConsumption = 0;
		if (num4 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num4 * 3 + 8 >> 4);
				num8 = num8 * 95 / 100;
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num4 + 2 >> 2);
			}
		}
		if (num7 != 0)
		{
			if ((servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
			{
				if ((servicePolicies & DistrictPolicies.Services.PetBan) != DistrictPolicies.Services.None)
				{
					num7 = Mathf.Max(1, num7 * 7650 / 10000);
				}
				else
				{
					num7 = Mathf.Max(1, num7 * 85 / 100);
				}
				num8 = num8 * 95 / 100;
			}
			else if ((servicePolicies & DistrictPolicies.Services.PetBan) != DistrictPolicies.Services.None)
			{
				num7 = Mathf.Max(1, num7 * 90 / 100);
			}
		}
		if (buildingData.m_fireIntensity == 0)
		{
			int num9 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num4, ref heatingConsumption, ref num5, ref num6, ref num7, servicePolicies);
			num8 = (num8 * num9 + 99) / 100;
			if (num8 != 0)
			{
				Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PrivateIncome, num8, this.m_info.m_class, taxationPolicies);
			}
			buildingData.m_flags |= Building.Flags.Active;
		}
		else
		{
			num4 = 0;
			heatingConsumption = 0;
			num5 = 0;
			num6 = 0;
			num7 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~Building.Flags.Active;
		}
		int num10 = 0;
		int wellbeing = 0;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (behaviourData.m_healthAccumulation != 0)
		{
			if (num != 0)
			{
				num10 = (behaviourData.m_healthAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Health, behaviourData.m_healthAccumulation, buildingData.m_position, radius);
		}
		if (behaviourData.m_wellbeingAccumulation != 0)
		{
			if (num != 0)
			{
				wellbeing = (behaviourData.m_wellbeingAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Wellbeing, behaviourData.m_wellbeingAccumulation, buildingData.m_position, radius);
		}
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(this.m_info.m_class, taxationPolicies);
		int num11 = (int)((Citizen.Wealth)11 - Citizen.GetWealthLevel(this.m_info.m_class.m_level));
		if (this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialHigh)
		{
			num11++;
		}
		if (taxRate >= num11 + 4)
		{
			if (buildingData.m_taxProblemTimer != 0 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
			{
				int num12 = taxRate - num11 >> 2;
				buildingData.m_taxProblemTimer = (byte)Mathf.Min(255, (int)buildingData.m_taxProblemTimer + num12);
				if (buildingData.m_taxProblemTimer >= 96)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_taxProblemTimer >= 32)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
				}
			}
		}
		else
		{
			buildingData.m_taxProblemTimer = (byte)Mathf.Max(0, (int)(buildingData.m_taxProblemTimer - 1));
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
		}
		int num13 = Citizen.GetHappiness(num10, wellbeing);
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
		{
			num13 -= num13 >> 1;
		}
		else if (buildingData.m_problems != Notification.Problem.None)
		{
			num13 -= num13 >> 2;
		}
		buildingData.m_health = (byte)num10;
		buildingData.m_happiness = (byte)num13;
		buildingData.m_citizenCount = (byte)num;
		buildingData.m_education1 = (byte)behaviourData.m_education1Count;
		buildingData.m_education2 = (byte)behaviourData.m_education2Count;
		buildingData.m_education3 = (byte)behaviourData.m_education3Count;
		buildingData.m_teens = (byte)behaviourData.m_teenCount;
		buildingData.m_youngs = (byte)behaviourData.m_youngCount;
		buildingData.m_adults = (byte)behaviourData.m_adultCount;
		buildingData.m_seniors = (byte)behaviourData.m_seniorCount;
		base.HandleSick(buildingID, ref buildingData, ref behaviourData, citizenCount);
		base.HandleDead(buildingID, ref buildingData, ref behaviourData, citizenCount);
		int num14 = behaviourData.m_crimeAccumulation / 10;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num14 = num14 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num14, num);
		int num15 = (int)buildingData.m_crimeBuffer;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num, buildingData.m_position, radius);
			int num16 = behaviourData.m_educated0Count * 30 + behaviourData.m_educated1Count * 15 + behaviourData.m_educated2Count * 10;
			num16 = num16 / num + 50;
			if ((int)buildingData.m_crimeBuffer > num * 40)
			{
				num16 += 30;
			}
			else if ((int)buildingData.m_crimeBuffer > num * 15)
			{
				num16 += 15;
			}
			else if ((int)buildingData.m_crimeBuffer > num * 5)
			{
				num16 += 10;
			}
			buildingData.m_fireHazard = (byte)num16;
			num15 = (num15 + (num >> 1)) / num;
		}
		else
		{
			buildingData.m_fireHazard = 0;
			num15 = 0;
		}
		int num17 = 0;
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.HighTechHousing) != DistrictPolicies.CityPlanning.None)
		{
			num17 += 25;
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.LandValue, 50, buildingData.m_position, radius);
		}
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.AntiSlip) != DistrictPolicies.CityPlanning.None)
		{
			num17 += behaviourData.m_seniorCount << 1;
		}
		if (num17 != 0)
		{
			Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, num17, this.m_info.m_class);
		}
		SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
		uint num18 = (instance2.m_currentFrameIndex & 3840u) >> 8;
		if ((ulong)num18 == (ulong)((long)(buildingID & 15)) && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance2.m_currentBuildIndex && (buildingData.m_flags & Building.Flags.Upgrading) == Building.Flags.None)
		{
			this.CheckBuildingLevel(buildingID, ref buildingData, ref frameData, ref behaviourData);
		}
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			if (num3 != 0 && (buildingData.m_problems & Notification.Problem.MajorProblem) == Notification.Problem.None && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u) == 0)
			{
				TransferManager.TransferReason homeReason = this.GetHomeReason(buildingID, ref buildingData, ref Singleton<SimulationManager>.get_instance().m_randomizer);
				if (homeReason != TransferManager.TransferReason.None)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = Mathf.Max(1, num3 * 8 / num2);
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = num3;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(homeReason, offer);
				}
			}
			instance.m_districts.m_buffer[(int)district].AddResidentialData(ref behaviourData, num, num10, num13, num15, num2, aliveHomeCount, num3, (int)this.m_info.m_class.m_level, num4, heatingConsumption, num5, num6, num7, num8, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255), this.m_info.m_class.m_subService);
			base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
			base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		}
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 127;
		fireTolerance = 8;
		return true;
	}

	private void CheckBuildingLevel(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, ref Citizen.BehaviourData behaviour)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		int num = behaviour.m_educated1Count + behaviour.m_educated2Count * 2 + behaviour.m_educated3Count * 3;
		int num2 = behaviour.m_teenCount + behaviour.m_youngCount * 2 + behaviour.m_adultCount * 3 + behaviour.m_seniorCount * 3;
		int averageEducation;
		ItemClass.Level level;
		int num3;
		if (num2 != 0)
		{
			averageEducation = (num * 300 + (num2 >> 1)) / num2;
			num = (num * 72 + (num2 >> 1)) / num2;
			if (num < 15)
			{
				level = ItemClass.Level.Level1;
				num3 = 1 + num;
			}
			else if (num < 30)
			{
				level = ItemClass.Level.Level2;
				num3 = 1 + (num - 15);
			}
			else if (num < 45)
			{
				level = ItemClass.Level.Level3;
				num3 = 1 + (num - 30);
			}
			else if (num < 60)
			{
				level = ItemClass.Level.Level4;
				num3 = 1 + (num - 45);
			}
			else
			{
				level = ItemClass.Level.Level5;
				num3 = 1;
			}
			if (level < this.m_info.m_class.m_level)
			{
				num3 = 1;
			}
			else if (level > this.m_info.m_class.m_level)
			{
				num3 = 15;
			}
		}
		else
		{
			level = ItemClass.Level.Level1;
			averageEducation = 0;
			num3 = 0;
		}
		int num4;
		Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.LandValue, buildingData.m_position, out num4);
		ItemClass.Level level2;
		int num5;
		if (num3 != 0)
		{
			if (num4 < 6)
			{
				level2 = ItemClass.Level.Level1;
				num5 = 1 + (num4 * 15 + 3) / 6;
			}
			else if (num4 < 21)
			{
				level2 = ItemClass.Level.Level2;
				num5 = 1 + ((num4 - 6) * 15 + 7) / 15;
			}
			else if (num4 < 41)
			{
				level2 = ItemClass.Level.Level3;
				num5 = 1 + ((num4 - 21) * 15 + 10) / 20;
			}
			else if (num4 < 61)
			{
				level2 = ItemClass.Level.Level4;
				num5 = 1 + ((num4 - 41) * 15 + 10) / 20;
			}
			else
			{
				level2 = ItemClass.Level.Level5;
				num5 = 1;
			}
			if (level2 < this.m_info.m_class.m_level)
			{
				num5 = 1;
			}
			else if (level2 > this.m_info.m_class.m_level)
			{
				num5 = 15;
			}
		}
		else
		{
			level2 = ItemClass.Level.Level1;
			num5 = 0;
		}
		bool flag = false;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			if (num4 == 0)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			if (num4 < 11)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level4)
		{
			if (num4 < 31)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level5 && num4 < 51)
		{
			flag = true;
		}
		ItemClass.Level level3 = (ItemClass.Level)Mathf.Min((int)level, (int)level2);
		Singleton<BuildingManager>.get_instance().m_LevelUpWrapper.OnCalculateResidentialLevelUp(ref level3, ref num3, ref num5, ref flag, averageEducation, num4, buildingID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		if (flag)
		{
			buildingData.m_serviceProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_serviceProblemTimer + 1));
			if (buildingData.m_serviceProblemTimer >= 8)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandValueLow | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_serviceProblemTimer >= 4)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
			}
			else
			{
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
			}
		}
		else
		{
			buildingData.m_serviceProblemTimer = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandValueLow);
		}
		if (level3 > this.m_info.m_class.m_level)
		{
			num3 = 0;
			num5 = 0;
			if (this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialHigh && (cityPlanningPolicies & DistrictPolicies.CityPlanning.HighriseBan) != DistrictPolicies.CityPlanning.None && level3 == ItemClass.Level.Level5)
			{
				District[] expr_41B_cp_0 = instance.m_districts.m_buffer;
				byte expr_41B_cp_1 = district;
				expr_41B_cp_0[(int)expr_41B_cp_1].m_cityPlanningPoliciesEffect = (expr_41B_cp_0[(int)expr_41B_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.HighriseBan);
				level3 = ItemClass.Level.Level4;
				num3 = 1;
			}
			if (buildingData.m_problems == Notification.Problem.None && level3 > this.m_info.m_class.m_level && this.GetUpgradeInfo(buildingID, ref buildingData) != null && !Singleton<DisasterManager>.get_instance().IsEvacuating(buildingData.m_position))
			{
				frameData.m_constructState = 0;
				base.StartUpgrading(buildingID, ref buildingData);
			}
		}
		buildingData.m_levelUpProgress = (byte)(num3 | num5 << 4);
	}

	public void LevelUp(ushort buildingID, ref Building buildingData, ItemClass.Level targetLevel)
	{
		if (targetLevel > this.m_info.m_class.m_level && this.GetUpgradeInfo(buildingID, ref buildingData) != null)
		{
			base.StartUpgrading(buildingID, ref buildingData);
		}
	}

	private TransferManager.TransferReason GetHomeReason(ushort buildingID, ref Building buildingData, ref Randomizer r)
	{
		if ((this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLow || this.m_info.m_class.m_subService == ItemClass.SubService.ResidentialLowEco) == (r.Int32(10u) != 0))
		{
			switch (this.m_info.m_class.m_level)
			{
			case ItemClass.Level.Level1:
				return TransferManager.TransferReason.Family0;
			case ItemClass.Level.Level2:
				return TransferManager.TransferReason.Family1;
			case ItemClass.Level.Level3:
				return TransferManager.TransferReason.Family2;
			case ItemClass.Level.Level4:
				return TransferManager.TransferReason.Family3;
			case ItemClass.Level.Level5:
				return TransferManager.TransferReason.Family3;
			default:
				return TransferManager.TransferReason.Family0;
			}
		}
		else if (r.Int32(2u) == 0)
		{
			switch (this.m_info.m_class.m_level)
			{
			case ItemClass.Level.Level1:
				return TransferManager.TransferReason.Single0;
			case ItemClass.Level.Level2:
				return TransferManager.TransferReason.Single1;
			case ItemClass.Level.Level3:
				return TransferManager.TransferReason.Single2;
			case ItemClass.Level.Level4:
				return TransferManager.TransferReason.Single3;
			case ItemClass.Level.Level5:
				return TransferManager.TransferReason.Single3;
			default:
				return TransferManager.TransferReason.Single0;
			}
		}
		else
		{
			switch (this.m_info.m_class.m_level)
			{
			case ItemClass.Level.Level1:
				return TransferManager.TransferReason.Single0B;
			case ItemClass.Level.Level2:
				return TransferManager.TransferReason.Single1B;
			case ItemClass.Level.Level3:
				return TransferManager.TransferReason.Single2B;
			case ItemClass.Level.Level4:
				return TransferManager.TransferReason.Single3B;
			case ItemClass.Level.Level5:
				return TransferManager.TransferReason.Single3B;
			default:
				return TransferManager.TransferReason.Single0B;
			}
		}
	}

	private static int GetAverageResidentRequirement(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Home) != 0)
			{
				int num5 = 0;
				int num6 = 0;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && !instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
					{
						num5 += ResidentialBuildingAI.GetResidentRequirement(resource, ref instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)]);
						num6++;
					}
				}
				if (num6 == 0)
				{
					num3 += 100;
					num4++;
				}
				else
				{
					num3 += num5;
					num4 += num6;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (num4 != 0)
		{
			return (num3 + (num4 >> 1)) / num4;
		}
		return 0;
	}

	private static int GetResidentRequirement(ImmaterialResourceManager.Resource resource, ref Citizen citizen)
	{
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
			return Citizen.GetHealthCareRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		case ImmaterialResourceManager.Resource.FireDepartment:
			return Citizen.GetFireDepartmentRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		case ImmaterialResourceManager.Resource.PoliceDepartment:
			return Citizen.GetPoliceDepartmentRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		case ImmaterialResourceManager.Resource.EducationElementary:
		{
			Citizen.AgePhase agePhase = Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age);
			if (agePhase < Citizen.AgePhase.Teen0)
			{
				return Citizen.GetEducationRequirement(agePhase);
			}
			return 0;
		}
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		{
			Citizen.AgePhase agePhase2 = Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age);
			if (agePhase2 >= Citizen.AgePhase.Teen0 && agePhase2 < Citizen.AgePhase.Young0)
			{
				return Citizen.GetEducationRequirement(agePhase2);
			}
			return 0;
		}
		case ImmaterialResourceManager.Resource.EducationUniversity:
		{
			Citizen.AgePhase agePhase3 = Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age);
			if (agePhase3 >= Citizen.AgePhase.Young0)
			{
				return Citizen.GetEducationRequirement(agePhase3);
			}
			return 0;
		}
		case ImmaterialResourceManager.Resource.DeathCare:
			return Citizen.GetDeathCareRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		case ImmaterialResourceManager.Resource.PublicTransport:
			return Citizen.GetTransportRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		case ImmaterialResourceManager.Resource.Entertainment:
			return Citizen.GetEntertainmentRequirement(Citizen.GetAgePhase(citizen.EducationLevel, citizen.Age));
		}
		return 100;
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
		{
			int averageResidentRequirement = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num);
			int num2 = ImmaterialResourceManager.CalculateResourceEffect(num, averageResidentRequirement, 500, 20, 40);
			int num3 = ImmaterialResourceManager.CalculateResourceEffect(num + Mathf.RoundToInt(amount), averageResidentRequirement, 500, 20, 40);
			return Mathf.Clamp((float)(num3 - num2) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FireDepartment:
		{
			int averageResidentRequirement2 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num4;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num4);
			int num5 = ImmaterialResourceManager.CalculateResourceEffect(num4, averageResidentRequirement2, 500, 20, 40);
			int num6 = ImmaterialResourceManager.CalculateResourceEffect(num4 + Mathf.RoundToInt(amount), averageResidentRequirement2, 500, 20, 40);
			return Mathf.Clamp((float)(num6 - num5) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PoliceDepartment:
		{
			int averageResidentRequirement3 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num7;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num7);
			int num8 = ImmaterialResourceManager.CalculateResourceEffect(num7, averageResidentRequirement3, 500, 20, 40);
			int num9 = ImmaterialResourceManager.CalculateResourceEffect(num7 + Mathf.RoundToInt(amount), averageResidentRequirement3, 500, 20, 40);
			return Mathf.Clamp((float)(num9 - num8) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.EducationElementary:
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		case ImmaterialResourceManager.Resource.EducationUniversity:
		{
			int averageResidentRequirement4 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num10;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num10);
			int num11 = ImmaterialResourceManager.CalculateResourceEffect(num10, averageResidentRequirement4, 500, 20, 40);
			int num12 = ImmaterialResourceManager.CalculateResourceEffect(num10 + Mathf.RoundToInt(amount), averageResidentRequirement4, 500, 20, 40);
			return Mathf.Clamp((float)(num12 - num11) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.DeathCare:
		{
			int averageResidentRequirement5 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num13;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num13);
			int num14 = ImmaterialResourceManager.CalculateResourceEffect(num13, averageResidentRequirement5, 500, 10, 20);
			int num15 = ImmaterialResourceManager.CalculateResourceEffect(num13 + Mathf.RoundToInt(amount), averageResidentRequirement5, 500, 10, 20);
			return Mathf.Clamp((float)(num15 - num14) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PublicTransport:
		{
			int averageResidentRequirement6 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num16;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num16);
			int num17 = ImmaterialResourceManager.CalculateResourceEffect(num16, averageResidentRequirement6, 500, 20, 40);
			int num18 = ImmaterialResourceManager.CalculateResourceEffect(num16 + Mathf.RoundToInt(amount), averageResidentRequirement6, 500, 20, 40);
			return Mathf.Clamp((float)(num18 - num17) / 20f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.NoisePollution:
		{
			int num19;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num19);
			int num20 = num19 * 100 / 255;
			int num21 = Mathf.Clamp(num19 + Mathf.RoundToInt(amount), 0, 255) * 100 / 255;
			return Mathf.Clamp((float)(num21 - num20) / 50f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Wellbeing:
			return Mathf.Clamp(amount, -1f, 1f);
		case ImmaterialResourceManager.Resource.Entertainment:
		{
			int averageResidentRequirement7 = ResidentialBuildingAI.GetAverageResidentRequirement(buildingID, ref data, resource);
			int num22;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num22);
			int num23 = ImmaterialResourceManager.CalculateResourceEffect(num22, averageResidentRequirement7, 500, 30, 60);
			int num24 = ImmaterialResourceManager.CalculateResourceEffect(num22 + Mathf.RoundToInt(amount), averageResidentRequirement7, 500, 30, 60);
			return Mathf.Clamp((float)(num24 - num23) / 30f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Abandonment:
		{
			int num25;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num25);
			int num26 = ImmaterialResourceManager.CalculateResourceEffect(num25, 15, 50, 10, 20);
			int num27 = ImmaterialResourceManager.CalculateResourceEffect(num25 + Mathf.RoundToInt(amount), 15, 50, 10, 20);
			return Mathf.Clamp((float)(num27 - num26) / 50f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.RadioCoverage:
		case ImmaterialResourceManager.Resource.DisasterCoverage:
		{
			int num28;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num28);
			int num29 = ImmaterialResourceManager.CalculateResourceEffect(num28, 50, 100, 20, 25);
			int num30 = ImmaterialResourceManager.CalculateResourceEffect(num28 + Mathf.RoundToInt(amount), 50, 100, 20, 25);
			return Mathf.Clamp((float)(num30 - num29) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FirewatchCoverage:
		{
			int num31;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num31);
			int num32 = ImmaterialResourceManager.CalculateResourceEffect(num31, 100, 1000, 0, 25);
			int num33 = ImmaterialResourceManager.CalculateResourceEffect(num31 + Mathf.RoundToInt(amount), 100, 1000, 0, 25);
			return Mathf.Clamp((float)(num33 - num32) / 100f, -1f, 1f);
		}
		}
		return base.GetEventImpact(buildingID, ref data, resource, amount);
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return base.GetEventImpact(buildingID, ref data, resource, amount);
		}
		byte b;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out b);
		int num = (int)(b * 100 / 255);
		int num2 = Mathf.Clamp((int)b + Mathf.RoundToInt(amount), 0, 255) * 100 / 255;
		return Mathf.Clamp((float)(num2 - num) / 50f, -1f, 1f);
	}

	public override int CalculateHomeCount(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num;
		if (@class.m_subService == ItemClass.SubService.ResidentialLow || @class.m_subService == ItemClass.SubService.ResidentialLowEco)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 20;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 25;
			}
			else if (@class.m_level == ItemClass.Level.Level3)
			{
				num = 30;
			}
			else if (@class.m_level == ItemClass.Level.Level4)
			{
				num = 35;
			}
			else
			{
				num = 40;
			}
		}
		else if (@class.m_level == ItemClass.Level.Level1)
		{
			num = 60;
		}
		else if (@class.m_level == ItemClass.Level.Level2)
		{
			num = 100;
		}
		else if (@class.m_level == ItemClass.Level.Level3)
		{
			num = 130;
		}
		else if (@class.m_level == ItemClass.Level.Level4)
		{
			num = 150;
		}
		else
		{
			num = 160;
		}
		return Mathf.Max(100, width * length * num + r.Int32(100u)) / 100;
	}

	public override int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		level0 = 0;
		level1 = 0;
		level2 = 0;
		level3 = 0;
	}

	public override int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		ItemClass @class = this.m_info.m_class;
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
		ItemClass.SubService subService = @class.m_subService;
		if (subService != ItemClass.SubService.ResidentialLow)
		{
			if (subService != ItemClass.SubService.ResidentialHigh)
			{
				if (subService != ItemClass.SubService.ResidentialLowEco)
				{
					if (subService == ItemClass.SubService.ResidentialHighEco)
					{
						switch (@class.m_level)
						{
						case ItemClass.Level.Level1:
							electricityConsumption = 21;
							waterConsumption = 60;
							sewageAccumulation = 60;
							garbageAccumulation = 49;
							incomeAccumulation = 49;
							break;
						case ItemClass.Level.Level2:
							electricityConsumption = 20;
							waterConsumption = 55;
							sewageAccumulation = 55;
							garbageAccumulation = 49;
							incomeAccumulation = 70;
							break;
						case ItemClass.Level.Level3:
							electricityConsumption = 18;
							waterConsumption = 50;
							sewageAccumulation = 50;
							garbageAccumulation = 28;
							incomeAccumulation = 91;
							break;
						case ItemClass.Level.Level4:
							electricityConsumption = 17;
							waterConsumption = 45;
							sewageAccumulation = 45;
							garbageAccumulation = 21;
							incomeAccumulation = 112;
							break;
						case ItemClass.Level.Level5:
							electricityConsumption = 15;
							waterConsumption = 40;
							sewageAccumulation = 40;
							garbageAccumulation = 14;
							incomeAccumulation = 140;
							break;
						}
					}
				}
				else
				{
					switch (@class.m_level)
					{
					case ItemClass.Level.Level1:
						electricityConsumption = 42;
						waterConsumption = 120;
						sewageAccumulation = 120;
						garbageAccumulation = 70;
						incomeAccumulation = 84;
						break;
					case ItemClass.Level.Level2:
						electricityConsumption = 42;
						waterConsumption = 110;
						sewageAccumulation = 110;
						garbageAccumulation = 70;
						incomeAccumulation = 112;
						break;
					case ItemClass.Level.Level3:
						electricityConsumption = 42;
						waterConsumption = 100;
						sewageAccumulation = 100;
						garbageAccumulation = 42;
						incomeAccumulation = 168;
						break;
					case ItemClass.Level.Level4:
						electricityConsumption = 42;
						waterConsumption = 90;
						sewageAccumulation = 90;
						garbageAccumulation = 28;
						incomeAccumulation = 252;
						break;
					case ItemClass.Level.Level5:
						electricityConsumption = 42;
						waterConsumption = 80;
						sewageAccumulation = 80;
						garbageAccumulation = 21;
						incomeAccumulation = 315;
						break;
					}
				}
			}
			else
			{
				switch (@class.m_level)
				{
				case ItemClass.Level.Level1:
					electricityConsumption = 30;
					waterConsumption = 60;
					sewageAccumulation = 60;
					garbageAccumulation = 70;
					incomeAccumulation = 70;
					break;
				case ItemClass.Level.Level2:
					electricityConsumption = 28;
					waterConsumption = 55;
					sewageAccumulation = 55;
					garbageAccumulation = 70;
					incomeAccumulation = 100;
					break;
				case ItemClass.Level.Level3:
					electricityConsumption = 26;
					waterConsumption = 50;
					sewageAccumulation = 50;
					garbageAccumulation = 40;
					incomeAccumulation = 130;
					break;
				case ItemClass.Level.Level4:
					electricityConsumption = 24;
					waterConsumption = 45;
					sewageAccumulation = 45;
					garbageAccumulation = 30;
					incomeAccumulation = 160;
					break;
				case ItemClass.Level.Level5:
					electricityConsumption = 22;
					waterConsumption = 40;
					sewageAccumulation = 40;
					garbageAccumulation = 20;
					incomeAccumulation = 200;
					break;
				}
			}
		}
		else
		{
			switch (@class.m_level)
			{
			case ItemClass.Level.Level1:
				electricityConsumption = 60;
				waterConsumption = 120;
				sewageAccumulation = 120;
				garbageAccumulation = 100;
				incomeAccumulation = 120;
				break;
			case ItemClass.Level.Level2:
				electricityConsumption = 60;
				waterConsumption = 110;
				sewageAccumulation = 110;
				garbageAccumulation = 100;
				incomeAccumulation = 160;
				break;
			case ItemClass.Level.Level3:
				electricityConsumption = 60;
				waterConsumption = 100;
				sewageAccumulation = 100;
				garbageAccumulation = 60;
				incomeAccumulation = 240;
				break;
			case ItemClass.Level.Level4:
				electricityConsumption = 60;
				waterConsumption = 90;
				sewageAccumulation = 90;
				garbageAccumulation = 40;
				incomeAccumulation = 360;
				break;
			case ItemClass.Level.Level5:
				electricityConsumption = 60;
				waterConsumption = 80;
				sewageAccumulation = 80;
				garbageAccumulation = 30;
				incomeAccumulation = 450;
				break;
			}
		}
		if (electricityConsumption != 0)
		{
			electricityConsumption = Mathf.Max(100, productionRate * electricityConsumption + r.Int32(100u)) / 100;
		}
		if (waterConsumption != 0)
		{
			int num = r.Int32(100u);
			waterConsumption = Mathf.Max(100, productionRate * waterConsumption + num) / 100;
			if (sewageAccumulation != 0)
			{
				sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + num) / 100;
			}
		}
		else if (sewageAccumulation != 0)
		{
			sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + r.Int32(100u)) / 100;
		}
		if (garbageAccumulation != 0)
		{
			garbageAccumulation = Mathf.Max(100, productionRate * garbageAccumulation + r.Int32(100u)) / 100;
		}
		if (incomeAccumulation != 0)
		{
			incomeAccumulation = productionRate * incomeAccumulation;
		}
	}

	public override void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		groundPollution = 0;
		noisePollution = 0;
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.CountUnchecked("BUILDING_NAME", text);
		if (num != 0u)
		{
			return Locale.Get("BUILDING_NAME", text, randomizer.Int32(num));
		}
		if (this.m_info.m_class.m_subService == ItemClass.SubService.CommercialLow)
		{
			text = this.m_info.m_class.m_level.ToString();
			num = Locale.Count("RESIDENCE_LOW_PATTERN", text);
			string format = Locale.Get("RESIDENCE_LOW_PATTERN", text, randomizer.Int32(num));
			num = Locale.Count("RESIDENCE_NAME");
			string arg = Locale.Get("RESIDENCE_NAME", randomizer.Int32(num));
			return StringUtils.SafeFormat(format, arg);
		}
		text = this.m_info.m_class.m_level.ToString();
		num = Locale.Count("RESIDENCE_HIGH_PATTERN", text);
		string format2 = Locale.Get("RESIDENCE_HIGH_PATTERN", text, randomizer.Int32(num));
		num = Locale.Count("RESIDENCE_NAME");
		string arg2 = Locale.Get("RESIDENCE_NAME", randomizer.Int32(num));
		return StringUtils.SafeFormat(format2, arg2);
	}

	private DistrictPolicies.Specialization SpecialPolicyNeeded()
	{
		ItemClass.SubService subService = this.m_info.m_class.m_subService;
		if (subService != ItemClass.SubService.ResidentialLowEco && subService != ItemClass.SubService.ResidentialHighEco)
		{
			return DistrictPolicies.Specialization.None;
		}
		return DistrictPolicies.Specialization.Selfsufficient;
	}
}
