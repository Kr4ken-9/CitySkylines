﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class BuildingCountMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_BUILDING_COUNT_DESC", this.m_localizationKey), new GeneratedString[]
		{
			new GeneratedString.Long((long)this.m_targetCount)
		});
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_targetCount;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)this.m_targetCount)
		{
			num = (long)this.m_targetCount;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_BUILDING_COUNT_DESC", this.m_localizationKey), this.m_targetCount);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_BUILDING_COUNT_PROG", this.m_localizationKey), new object[]
		{
			num,
			this.m_targetCount
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = (long)this.m_targetCount;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			int num = 0;
			if (this.m_requireDifferentBuildings && BuildingCountMilestone.m_tempBuildingInfoIndex == null)
			{
				BuildingCountMilestone.m_tempBuildingInfoIndex = new HashSet<int>();
			}
			ItemClass.Service service;
			ItemClass.Service service2;
			uint num2;
			if (this.m_service == ItemClass.Service.None)
			{
				service = ItemClass.Service.Road;
				service2 = ItemClass.Service.Disaster;
				num2 = 1961472u;
			}
			else
			{
				service = this.m_service;
				service2 = this.m_service;
				num2 = 1u << (int)this.m_service;
			}
			for (ItemClass.Service service3 = service; service3 <= service2; service3++)
			{
				if ((num2 & 1u << (int)service3) != 0u)
				{
					FastList<ushort> serviceBuildings = instance.GetServiceBuildings(service3);
					for (int i = 0; i < serviceBuildings.m_size; i++)
					{
						ushort num3 = serviceBuildings.m_buffer[i];
						BuildingInfo info = instance.m_buildings.m_buffer[(int)num3].Info;
						if (info != null && instance.m_buildings.m_buffer[(int)num3].m_parentBuilding == 0 && (this.m_subService == ItemClass.SubService.None || info.m_class.m_subService == this.m_subService) && (this.m_level == ItemClass.Level.None || info.m_class.m_level == this.m_level) && (!this.m_requireDifferentBuildings || !BuildingCountMilestone.m_tempBuildingInfoIndex.Contains(info.m_prefabDataIndex)))
						{
							num++;
							if (this.m_requireDifferentBuildings)
							{
								BuildingCountMilestone.m_tempBuildingInfoIndex.Add(info.m_prefabDataIndex);
							}
						}
					}
				}
			}
			if (this.m_requireDifferentBuildings)
			{
				BuildingCountMilestone.m_tempBuildingInfoIndex.Clear();
			}
			data.m_progress = (long)Mathf.Min(this.m_targetCount, num);
			data.m_passedCount = ((data.m_progress != (long)this.m_targetCount) ? 0 : 1);
		}
		return data;
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32((int)this.m_service);
		s.WriteInt32((int)this.m_subService);
		s.WriteInt32((int)this.m_level);
		s.WriteBool(this.m_requireDifferentBuildings);
		s.WriteInt32(this.m_targetCount);
		s.WriteUniqueString(this.m_localizationKey);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_service = (ItemClass.Service)s.ReadInt32();
		this.m_subService = (ItemClass.SubService)s.ReadInt32();
		this.m_level = (ItemClass.Level)s.ReadInt32();
		this.m_requireDifferentBuildings = s.ReadBool();
		this.m_targetCount = s.ReadInt32();
		this.m_localizationKey = s.ReadUniqueString();
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		if (this.m_targetCount != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)this.m_targetCount));
		}
		return num;
	}

	public ItemClass.Service m_service = ItemClass.Service.Beautification;

	public ItemClass.SubService m_subService;

	public ItemClass.Level m_level = ItemClass.Level.None;

	public bool m_requireDifferentBuildings;

	public int m_targetCount;

	public string m_localizationKey;

	public static HashSet<int> m_tempBuildingInfoIndex;
}
