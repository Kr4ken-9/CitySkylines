﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class VehicleProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		VehicleProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new VehicleProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void InitializeShaderProperties()
	{
		Shader.SetGlobalTexture("_VehicleFloorDiffuse", this.m_floorDiffuse);
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("VehicleProperties");
			Singleton<VehicleManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Texture m_floorDiffuse;

	public Shader m_undergroundShader;
}
