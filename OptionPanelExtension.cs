﻿using System;

public static class OptionPanelExtension
{
	public static void Hide(this OptionPanelBase panel)
	{
		if (panel != null)
		{
			panel.HidePanel();
		}
	}
}
