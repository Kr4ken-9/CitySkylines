﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public sealed class CitizenVehicleWorldInfoPanel : VehicleWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_CitizenOwner = base.GetComponentInChildren<CitizenWorldInfoPanel>();
		this.m_VehicleType = base.Find<UISprite>("VehicleType");
		this.m_VehicleType.add_eventSpriteNameChanged(new PropertyChangedEventHandler<string>(this.IconChanged));
	}

	private void IconChanged(UIComponent comp, string text)
	{
		UITextureAtlas.SpriteInfo spriteInfo = this.m_VehicleType.get_atlas().get_Item(text);
		if (spriteInfo != null)
		{
			this.m_VehicleType.set_size(spriteInfo.get_pixelSize());
		}
	}

	private static string GetVehicleTypeIcon(VehicleInfo.VehicleType type)
	{
		if (type == VehicleInfo.VehicleType.Car)
		{
			return "IconCitizenVehicle";
		}
		if (type != VehicleInfo.VehicleType.Bicycle)
		{
			return "IconCitizenVehicle";
		}
		return "IconCitizenBicycleVehicle";
	}

	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].Info;
				InstanceID ownerID = info.m_vehicleAI.GetOwnerID(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle]);
				this.m_CitizenOwner.SetTarget(ownerID);
			}
		}
		else if (this.m_InstanceID.Type == InstanceType.ParkedVehicle && this.m_InstanceID.ParkedVehicle != 0)
		{
			VehicleParked vehicleParked = Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)this.m_InstanceID.ParkedVehicle];
			InstanceID target = default(InstanceID);
			target.Citizen = vehicleParked.m_ownerCitizen;
			this.m_CitizenOwner.SetTarget(target);
		}
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			InstanceID instanceID = this.m_InstanceID;
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				instanceID.Vehicle = firstVehicle;
				VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].Info;
				this.m_VehicleType.set_spriteName(CitizenVehicleWorldInfoPanel.GetVehicleTypeIcon(info.m_vehicleType));
			}
		}
		else if (this.m_InstanceID.Type == InstanceType.ParkedVehicle && this.m_InstanceID.ParkedVehicle != 0)
		{
			VehicleParked vehicleParked = Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)this.m_InstanceID.ParkedVehicle];
			this.m_VehicleType.set_spriteName(CitizenVehicleWorldInfoPanel.GetVehicleTypeIcon(vehicleParked.Info.m_vehicleType));
		}
	}

	private CitizenWorldInfoPanel m_CitizenOwner;

	private UISprite m_VehicleType;
}
