﻿using System;
using ColossalFramework;
using ICities;

public class ApplicationWrapper : IApplication
{
	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	public string currentVersion
	{
		get
		{
			return BuildConfig.applicationVersionFull;
		}
	}

	public bool SupportsVersion(int a, int b, int c)
	{
		return BuildConfig.SupportsVersion(BuildConfig.MakeVersionNumber((uint)a, (uint)b, (uint)c, BuildConfig.ReleaseType.Final, 1u, BuildConfig.BuildType.Unknown));
	}

	public bool SupportsExpansion(Expansion expansion)
	{
		return expansion == -1 || Singleton<LoadingManager>.get_instance().m_supportsExpansion[expansion];
	}
}
