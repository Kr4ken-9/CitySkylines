﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class ParadoxUnlockPanel : MenuPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.m_ReferenceSprite = UIView.Find<UITextureSprite>("ModalEffect");
		if (this.m_Fireworks != null)
		{
			Renderer[] componentsInChildren = this.m_Fireworks.GetComponentsInChildren<Renderer>();
			this.m_FireworkMaterials = new Material[componentsInChildren.Length];
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				this.m_FireworkMaterials[i] = componentsInChildren[i].get_material();
			}
		}
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Ok");
			uIButton.Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (this.m_Fireworks != null)
			{
				this.m_Fireworks.Play();
			}
			base.get_component().Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && (p.get_keycode() == 27 || p.get_keycode() == 13))
		{
			this.OnClosed();
			p.Use();
		}
	}

	private void SetFireWorksRenderQueue(int rdrq)
	{
		for (int i = 0; i < this.m_FireworkMaterials.Length; i++)
		{
			this.m_FireworkMaterials[i].set_renderQueue(rdrq);
		}
	}

	private void Update()
	{
		if (this.m_Fireworks != null && this.m_Fireworks.get_isPlaying())
		{
			this.SetFireWorksRenderQueue(this.m_ReferenceSprite.get_renderMaterial().get_renderQueue() + 1);
		}
	}

	private Material[] m_FireworkMaterials;

	public ParticleSystem m_Fireworks;

	private UITextureSprite m_ReferenceSprite;
}
