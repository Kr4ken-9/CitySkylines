﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class PublicTransportStopButton : UICustomControl
{
	public static CameraController cameraController
	{
		get
		{
			if (PublicTransportStopButton.m_CameraController == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					PublicTransportStopButton.m_CameraController = gameObject.GetComponent<CameraController>();
				}
			}
			return PublicTransportStopButton.m_CameraController;
		}
	}

	private void Awake()
	{
		base.get_component().add_eventMouseDown(new MouseEventHandler(this.OnMouseDown));
	}

	private void OnMouseDown(UIComponent component, UIMouseEventParameter eventParam)
	{
		UIButton uIButton = component as UIButton;
		ushort num = (ushort)uIButton.get_objectUserData();
		Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position;
		InstanceID empty = InstanceID.Empty;
		empty.NetNode = num;
		if (PublicTransportStopButton.cameraController != null)
		{
			PublicTransportStopButton.cameraController.SetTarget(empty, position, false);
		}
		PublicTransportWorldInfoPanel.ResetScrollPosition();
		UIView.SetFocus(null);
	}

	private static CameraController m_CameraController;
}
