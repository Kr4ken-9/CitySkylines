﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class DistrictPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		if (base.category == "DistrictSpecializationPaint")
		{
			for (int i = 0; i < this.kModes.Length; i++)
			{
				this.SpawnEntry(this.kModes[i].get_enumName(), true, DistrictPolicies.Policies.None);
			}
		}
		else if (base.category == "DistrictSpecializationIndustrial")
		{
			for (int j = 0; j < this.kIndustryPolicies.Length; j++)
			{
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(this.kIndustryPolicies[j].get_enumValue()))
				{
					this.SpawnEntry("Specialization" + this.kIndustryPolicies[j].get_enumName(), ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization) && ToolsModifierControl.IsUnlocked(this.kIndustryPolicies[j].get_enumValue()), this.kIndustryPolicies[j].get_enumValue());
				}
			}
			this.SpawnEntry("SpecializationNone", ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization), DistrictPolicies.Policies.None);
		}
		else if (base.category == "DistrictSpecializationCommercial")
		{
			for (int k = 0; k < this.kCommercialPolicies.Length; k++)
			{
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(this.kCommercialPolicies[k].get_enumValue()))
				{
					this.SpawnEntry("Specialization" + this.kCommercialPolicies[k].get_enumName(), ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization) && ToolsModifierControl.IsUnlocked(this.kCommercialPolicies[k].get_enumValue()), this.kCommercialPolicies[k].get_enumValue());
				}
			}
			this.SpawnEntry("SpecializationCommercialNone", ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization), DistrictPolicies.Policies.None);
		}
		else if (base.category == "DistrictSpecializationResidential")
		{
			for (int l = 0; l < this.kResidentialPolicies.Length; l++)
			{
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(this.kResidentialPolicies[l].get_enumValue()))
				{
					this.SpawnEntry("Specialization" + this.kResidentialPolicies[l].get_enumName(), ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization) && ToolsModifierControl.IsUnlocked(this.kResidentialPolicies[l].get_enumValue()), this.kResidentialPolicies[l].get_enumValue());
				}
			}
			this.SpawnEntry("SpecializationResidentialNone", ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization), DistrictPolicies.Policies.None);
		}
		else if (base.category == "DistrictSpecializationOffice")
		{
			for (int m = 0; m < this.kOfficePolicies.Length; m++)
			{
				if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(this.kOfficePolicies[m].get_enumValue()))
				{
					this.SpawnEntry("Specialization" + this.kOfficePolicies[m].get_enumName(), ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization) && ToolsModifierControl.IsUnlocked(this.kOfficePolicies[m].get_enumValue()), this.kOfficePolicies[m].get_enumValue());
				}
			}
			this.SpawnEntry("SpecializationOfficeNone", ToolsModifierControl.IsUnlocked(DistrictPolicies.Types.Specialization), DistrictPolicies.Policies.None);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		if (base.category == "DistrictSpecializationPaint")
		{
			if (comp.get_zOrder() == 0)
			{
				DistrictTool districtTool = ToolsModifierControl.SetTool<DistrictTool>();
				if (districtTool != null)
				{
					base.ShowDistrictOptionPanel();
					districtTool.m_mode = DistrictTool.Mode.Paint;
				}
			}
			else if (comp.get_zOrder() == 1)
			{
				DistrictTool districtTool2 = ToolsModifierControl.SetTool<DistrictTool>();
				if (districtTool2 != null)
				{
					base.ShowDistrictOptionPanel();
					districtTool2.m_mode = DistrictTool.Mode.Erase;
				}
			}
		}
		else if (base.category == "DistrictSpecializationIndustrial")
		{
			DistrictPolicies.Policies policies = (DistrictPolicies.Policies)comp.get_objectUserData();
			DistrictTool districtTool3 = ToolsModifierControl.SetTool<DistrictTool>();
			if (districtTool3 != null)
			{
				base.HideDistrictOptionPanel();
				if (policies != DistrictPolicies.Policies.None)
				{
					districtTool3.m_mode = DistrictTool.Mode.Specialize;
					districtTool3.m_specialization = policies;
				}
				else
				{
					districtTool3.m_mode = DistrictTool.Mode.Unspecialize;
					districtTool3.m_specialization = DistrictPolicies.Policies.Forest;
				}
			}
		}
		else if (base.category == "DistrictSpecializationCommercial")
		{
			DistrictPolicies.Policies policies2 = (DistrictPolicies.Policies)comp.get_objectUserData();
			DistrictTool districtTool4 = ToolsModifierControl.SetTool<DistrictTool>();
			if (districtTool4 != null)
			{
				base.HideDistrictOptionPanel();
				if (policies2 != DistrictPolicies.Policies.None)
				{
					districtTool4.m_mode = DistrictTool.Mode.Specialize;
					districtTool4.m_specialization = policies2;
				}
				else
				{
					districtTool4.m_mode = DistrictTool.Mode.Unspecialize;
					districtTool4.m_specialization = DistrictPolicies.Policies.Leisure;
				}
			}
		}
		else if (base.category == "DistrictSpecializationResidential")
		{
			DistrictPolicies.Policies policies3 = (DistrictPolicies.Policies)comp.get_objectUserData();
			DistrictTool districtTool5 = ToolsModifierControl.SetTool<DistrictTool>();
			if (districtTool5 != null)
			{
				base.HideDistrictOptionPanel();
				if (policies3 != DistrictPolicies.Policies.None)
				{
					districtTool5.m_mode = DistrictTool.Mode.Specialize;
					districtTool5.m_specialization = policies3;
				}
				else
				{
					districtTool5.m_mode = DistrictTool.Mode.Unspecialize;
					districtTool5.m_specialization = DistrictPolicies.Policies.Selfsufficient;
				}
			}
		}
		else if (base.category == "DistrictSpecializationOffice")
		{
			DistrictPolicies.Policies policies4 = (DistrictPolicies.Policies)comp.get_objectUserData();
			DistrictTool districtTool6 = ToolsModifierControl.SetTool<DistrictTool>();
			if (districtTool6 != null)
			{
				base.HideDistrictOptionPanel();
				if (policies4 != DistrictPolicies.Policies.None)
				{
					districtTool6.m_mode = DistrictTool.Mode.Specialize;
					districtTool6.m_specialization = policies4;
				}
				else
				{
					districtTool6.m_mode = DistrictTool.Mode.Unspecialize;
					districtTool6.m_specialization = DistrictPolicies.Policies.Hightech;
				}
			}
		}
	}

	private void SpawnEntry(string name, bool enabled, DistrictPolicies.Policies policy)
	{
		string text = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Title,
			Locale.Get("DISTRICT_TITLE", name),
			LocaleFormatter.Sprite,
			name,
			LocaleFormatter.Text,
			Locale.Get("DISTRICT_DESC", name),
			LocaleFormatter.Locked,
			(!enabled).ToString()
		});
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestone = null;
			if (policy != DistrictPolicies.Policies.None)
			{
				milestone = Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
			}
			string text2;
			string text3;
			string text4;
			string text5;
			string text6;
			ToolsModifierControl.GetUnlockingInfo(milestone, out text2, out text3, out text4, out text5, out text6);
			string text7 = TooltipHelper.Format(new string[]
			{
				LocaleFormatter.LockedInfo,
				text6,
				LocaleFormatter.UnlockDesc,
				text2,
				LocaleFormatter.UnlockPopulationProgressText,
				text5,
				LocaleFormatter.UnlockPopulationTarget,
				text4,
				LocaleFormatter.UnlockPopulationCurrent,
				text3
			});
			text = TooltipHelper.Append(text, text7);
		}
		this.SpawnEntry(name, text, "District" + name, null, GeneratedPanel.tooltipBox, enabled).set_objectUserData(policy);
	}

	public override void OnTooltipHover(UIComponent comp, UIMouseEventParameter p)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			UIButton uIButton = p.get_source() as UIButton;
			if (uIButton != null && uIButton.get_tooltipBox().get_isVisible())
			{
				DistrictPolicies.Policies policies = (DistrictPolicies.Policies)uIButton.get_objectUserData();
				MilestoneInfo milestone = null;
				if (policies != DistrictPolicies.Policies.None)
				{
					milestone = Singleton<UnlockManager>.get_instance().m_properties.m_SpecializationMilestones[(int)(policies & (DistrictPolicies.Policies)31)];
				}
				string text;
				string text2;
				string text3;
				string text4;
				string text5;
				ToolsModifierControl.GetUnlockingInfo(milestone, out text, out text2, out text3, out text4, out text5);
				uIButton.set_tooltip(TooltipHelper.Replace(uIButton.get_tooltip(), new string[]
				{
					LocaleFormatter.UnlockDesc,
					text,
					LocaleFormatter.Locked,
					text5,
					LocaleFormatter.LockedInfo,
					text5,
					LocaleFormatter.UnlockPopulationProgressText,
					text4,
					LocaleFormatter.UnlockPopulationCurrent,
					text2
				}));
				uIButton.RefreshTooltip();
			}
		}
	}

	private readonly PositionData<DistrictTool.Mode>[] kModes = Utils.GetOrderedEnumData<DistrictTool.Mode>();

	private readonly PositionData<DistrictPolicies.Policies>[] kIndustryPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Industry");

	private readonly PositionData<DistrictPolicies.Policies>[] kCommercialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Commercial");

	private readonly PositionData<DistrictPolicies.Policies>[] kResidentialPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Residential");

	private readonly PositionData<DistrictPolicies.Policies>[] kOfficePolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>("Office");
}
