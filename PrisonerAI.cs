﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class PrisonerAI : HumanAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
	}

	public override void ReleaseAI()
	{
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.CrimeRate)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort instanceID, ref CitizenInstance data, Vector3 position, Quaternion rotation, Vector3 velocity, Color color, bool underground)
	{
		if ((data.m_flags & CitizenInstance.Flags.AtTarget) != CitizenInstance.Flags.None)
		{
			if ((data.m_flags & CitizenInstance.Flags.SittingDown) != CitizenInstance.Flags.None)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 2, underground);
				return;
			}
			if ((data.m_flags & (CitizenInstance.Flags.Panicking | CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) == CitizenInstance.Flags.Panicking)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 1, underground);
				return;
			}
			if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating | CitizenInstance.Flags.Cheering)) == CitizenInstance.Flags.Cheering)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 5, underground);
				return;
			}
		}
		if ((data.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None)
		{
			this.m_info.SetRenderParameters(position, rotation, velocity, color, 3, underground);
		}
		else if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			this.m_info.SetRenderParameters(position, rotation, Vector3.get_zero(), color, 1, underground);
		}
		else
		{
			this.m_info.SetRenderParameters(position, rotation, velocity, color, (int)(instanceID & 4), underground);
		}
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		ushort targetBuilding = data.m_targetBuilding;
		if (targetBuilding != 0)
		{
			target = InstanceID.Empty;
			target.Building = targetBuilding;
			return Locale.Get("CITIZEN_STATUS_VISITING");
		}
		target = InstanceID.Empty;
		return Locale.Get("CITIZEN_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(uint citizenID, ref Citizen data, out InstanceID target)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort instance2 = data.m_instance;
		if (instance2 != 0)
		{
			return this.GetLocalizedStatus(instance2, ref instance.m_instances.m_buffer[(int)instance2], out target);
		}
		if (data.m_visitBuilding != 0)
		{
			target = InstanceID.Empty;
			target.Building = data.m_visitBuilding;
			return Locale.Get("CITIZEN_STATUS_VISITING");
		}
		target = InstanceID.Empty;
		return Locale.Get("CITIZEN_STATUS_CONFUSED");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (data.m_targetBuilding == 0)
		{
			instance.ReleaseCitizenInstance(instanceID);
			return;
		}
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
			}
		}
	}

	protected override bool StartPathFind(ushort instanceID, ref CitizenInstance citizenData)
	{
		return true;
	}
}
