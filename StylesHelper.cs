﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;

public static class StylesHelper
{
	public static void RenameStyle(Package.Asset asset, string name)
	{
		DistrictStyleMetaData districtStyleMetaData = asset.Instantiate<DistrictStyleMetaData>();
		districtStyleMetaData.name = name;
		if (!asset.get_isWorkshopAsset())
		{
			PackageManager.DisableEvents();
			PackageManager.Remove(asset.get_package(), true);
			File.Delete(asset.get_package().get_packagePath());
			PackageManager.EnabledEvents();
		}
		StylesHelper.SaveStyle(districtStyleMetaData, name, true, null);
	}

	private static string GetStylePathName(string saveName)
	{
		string path = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		string text = Path.Combine(DataLocation.get_addonsPath(), "Styles");
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		return Path.Combine(text, path);
	}

	private static bool StyleExists(string name)
	{
		string stylePathName = StylesHelper.GetStylePathName(name);
		return File.Exists(stylePathName);
	}

	public static void SaveStyle(DistrictStyleMetaData style, string name, bool uniqueFileName, string previewPath = null)
	{
		int num = 1;
		string text = name;
		while (uniqueFileName && StylesHelper.StyleExists(text))
		{
			text = string.Concat(new object[]
			{
				name,
				"(",
				num,
				")"
			});
			style.name = text;
			num++;
		}
		PackageManager.DisableEvents();
		string stylePathName = StylesHelper.GetStylePathName(text);
		Package package = new Package(text);
		package.set_packageMainAsset(style.name);
		if (PlatformService.get_active())
		{
			package.set_packageAuthor("steamid:" + PlatformService.get_user().get_userID().get_AsUInt64().ToString());
		}
		style.timeStamp = DateTime.Now;
		if (previewPath != null && File.Exists(previewPath))
		{
			Image image = new Image(previewPath);
			style.steamPreviewRef = package.AddAsset(style.name + "_SteamPreview", image, false, 0, false, false);
			image.Resize(400, 224);
			style.imageRef = package.AddAsset(style.name + "_Snapshot", image, false, 0, false, false);
		}
		package.AddAsset(style.name, style, UserAssetType.DistrictStyleMetaData, false);
		PackageManager.EnabledEvents();
		package.Save(stylePathName, false);
		PackageManager.Update(stylePathName);
	}

	public static List<string> GetStyles()
	{
		List<string> list = new List<string>();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.DistrictStyleMetaData
		}))
		{
			if (current != null)
			{
				DistrictStyleMetaData districtStyleMetaData = current.Instantiate<DistrictStyleMetaData>();
				if (districtStyleMetaData != null)
				{
					list.Add(districtStyleMetaData.name);
				}
			}
		}
		list.Sort();
		return list;
	}
}
