﻿using System;
using ColossalFramework;
using UnityEngine;

public class ParticleSystemSpeed : MonoBehaviour
{
	private void Awake()
	{
		this.m_system = base.GetComponent<ParticleSystem>();
	}

	private void LateUpdate()
	{
		if (this.m_system != null)
		{
			this.m_system.get_main().set_simulationSpeed(Mathf.Max(0.001f, Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed));
		}
	}

	[NonSerialized]
	private ParticleSystem m_system;
}
