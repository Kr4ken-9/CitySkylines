﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class AgeInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized && Singleton<LoadingManager>.get_exists())
		{
			if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				return;
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
		}
	}

	private bool m_initialized;
}
