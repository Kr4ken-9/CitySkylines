﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct TransportLine
{
	public void RenderLine(RenderManager.CameraInfo cameraInfo, int layerMask, int typeMask, ushort lineID)
	{
		if (this.m_flags == TransportLine.Flags.None)
		{
			return;
		}
		TransportInfo info = this.Info;
		Material material = info.m_lineMaterial2;
		if ((1 << info.m_prefabDataLayer & layerMask) == 0)
		{
			if (info.m_secondaryLayer == -1 || (1 << info.m_secondaryLayer & layerMask) == 0)
			{
				return;
			}
			material = info.m_secondaryLineMaterial2;
		}
		if ((1 << (int)info.m_transportType & typeMask) == 0)
		{
			return;
		}
		TransportManager instance = Singleton<TransportManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		Mesh[] array = instance.m_lineMeshes[(int)lineID];
		if (array != null)
		{
			int num = array.Length;
			for (int i = 0; i < num; i++)
			{
				Mesh mesh = array[i];
				if (mesh != null && cameraInfo.Intersect(mesh.get_bounds()))
				{
					material.set_color(this.GetColor());
					material.SetFloat(instance.ID_StartOffset, -1000f);
					if (info.m_requireSurfaceLine)
					{
						instance2.SetWaterMaterialProperties(mesh.get_bounds().get_center(), material);
					}
					if (material.SetPass(0))
					{
						TransportManager expr_10D_cp_0 = instance;
						expr_10D_cp_0.m_drawCallData.m_overlayCalls = expr_10D_cp_0.m_drawCallData.m_overlayCalls + 1;
						Graphics.DrawMeshNow(mesh, Matrix4x4.get_identity());
					}
				}
			}
		}
	}

	public TransportInfo Info
	{
		get
		{
			return PrefabCollection<TransportInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public bool Complete
	{
		get
		{
			return (this.m_flags & TransportLine.Flags.Complete) != TransportLine.Flags.None;
		}
	}

	public void SetActive(bool day, bool night)
	{
		if (day)
		{
			this.m_flags &= ~TransportLine.Flags.DisabledDay;
		}
		else
		{
			this.m_flags |= TransportLine.Flags.DisabledDay;
		}
		if (night)
		{
			this.m_flags &= ~TransportLine.Flags.DisabledNight;
		}
		else
		{
			this.m_flags |= TransportLine.Flags.DisabledNight;
		}
	}

	public void CheckCompletionMilestone()
	{
		TransportLine.Flags flags = this.m_flags & (TransportLine.Flags.Complete | TransportLine.Flags.CompleteSet);
		if ((this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
		{
			flags &= ~TransportLine.Flags.Complete;
		}
		if (flags == TransportLine.Flags.Complete)
		{
			TransportInfo info = this.Info;
			if (info != null && info.m_completedPassMilestone != null)
			{
				info.m_completedPassMilestone.Unlock();
				this.m_flags |= TransportLine.Flags.CompleteSet;
			}
		}
		else if (flags == TransportLine.Flags.CompleteSet)
		{
			TransportInfo info2 = this.Info;
			if (info2 != null && info2.m_completedPassMilestone != null)
			{
				info2.m_completedPassMilestone.Relock();
				this.m_flags &= ~TransportLine.Flags.CompleteSet;
			}
		}
	}

	public void AddVehicle(ushort vehicleID, ref Vehicle data, bool findTargetStop)
	{
		data.m_nextLineVehicle = this.m_vehicles;
		this.m_vehicles = vehicleID;
		if (findTargetStop)
		{
			Vector3 lastFramePosition = data.GetLastFramePosition();
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			NetManager instance2 = Singleton<NetManager>.get_instance();
			ushort stops = this.m_stops;
			ushort num = stops;
			ushort targetBuilding = stops;
			float num2 = 1000000f;
			int num3 = 0;
			while (num != 0)
			{
				Vector3 position = instance2.m_nodes.m_buffer[(int)num].m_position;
				float num4 = Vector3.Distance(position, lastFramePosition) + (float)instance.m_randomizer.Int32(100u);
				if (num4 < num2)
				{
					targetBuilding = num;
					num2 = num4;
				}
				num = TransportLine.GetNextStop(num);
				if (num == stops)
				{
					break;
				}
				if (++num3 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_targetBuilding = targetBuilding;
		}
	}

	public void RemoveVehicle(ushort vehicleID, ref Vehicle data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = 0;
		ushort num2 = this.m_vehicles;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == vehicleID)
			{
				if (num != 0)
				{
					instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle = data.m_nextLineVehicle;
				}
				else
				{
					this.m_vehicles = data.m_nextLineVehicle;
				}
				data.m_nextLineVehicle = 0;
				data.m_targetBuilding = 0;
				return;
			}
			num = num2;
			num2 = instance.m_vehicles.m_buffer[(int)num2].m_nextLineVehicle;
			if (++num3 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public int CountStops(ushort lineID)
	{
		int num = 0;
		ushort stops = this.m_stops;
		ushort num2 = stops;
		int num3 = 0;
		while (num2 != 0)
		{
			num++;
			num2 = TransportLine.GetNextStop(num2);
			if (num2 == stops)
			{
				break;
			}
			if (++num3 >= 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num;
	}

	public ushort GetStop(int index)
	{
		if (index == -1)
		{
			return this.GetLastStop();
		}
		ushort stops = this.m_stops;
		ushort num = stops;
		int num2 = 0;
		while (num != 0)
		{
			if (index-- == 0)
			{
				return num;
			}
			num = TransportLine.GetNextStop(num);
			if (num == stops)
			{
				break;
			}
			if (++num2 >= 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public int CountVehicles(ushort lineID)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = this.m_vehicles;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			num2++;
			num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
			if (++num3 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	public ushort GetVehicle(int index)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = this.m_vehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (index-- == 0)
			{
				return num;
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public void AddWaveEvent(Vector3 position1, Vector3 position2, int accumulation1, int accumulation2)
	{
		if ((this.m_flags & TransportLine.Flags.Temporary) == TransportLine.Flags.None && (accumulation1 != 0 || accumulation2 != 0))
		{
			TransportInfo info = this.Info;
			NetInfo netInfo = info.m_netInfo;
			int num;
			float num2;
			netInfo.m_netAI.GetTransportAccumulation(out num, out num2);
			int num3 = num * accumulation1 / 2;
			int num4 = num * accumulation2 / 2;
			if (num3 != 0 || num4 != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position1, (num3 <= 0) ? NotificationEvent.Type.Sad : NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PublicTransport, (float)num3, (num3 == 0) ? 0f : num2, position2, (num4 <= 0) ? NotificationEvent.Type.Sad : NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PublicTransport, (float)num4, (num4 == 0) ? 0f : num2);
			}
		}
	}

	public bool AddStop(ushort lineID, int index, Vector3 position, bool fixedPlatform)
	{
		ushort num = 0;
		ushort num2 = 0;
		if (this.m_stops != 0)
		{
			if (index == -1)
			{
				if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
				{
					num = 0;
					num2 = this.GetLastStop();
				}
				else
				{
					num = this.m_stops;
					num2 = TransportLine.GetPrevStop(this.m_stops);
				}
			}
			else
			{
				num = this.m_stops;
				num2 = TransportLine.GetPrevStop(this.m_stops);
				for (int i = 0; i < index; i++)
				{
					num2 = num;
					num = TransportLine.GetNextStop(num);
					if (num == this.m_stops)
					{
						break;
					}
				}
			}
		}
		if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None && num2 != 0 && num == 0 && this.m_stops != 0 && num2 != this.m_stops && Vector3.SqrMagnitude(position - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_stops].m_position) < 6.25f)
		{
			ushort num3;
			if (this.CreateSegment(out num3, num2, this.m_stops))
			{
				this.m_flags |= TransportLine.Flags.Complete;
				this.CheckCompletionMilestone();
				Singleton<TransportManager>.get_instance().UpdateLine(lineID);
				this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num2].m_position, 1, 1);
				return true;
			}
			return false;
		}
		else
		{
			ushort num4;
			if (!this.CreateNode(lineID, out num4, position, fixedPlatform))
			{
				return false;
			}
			bool flag = false;
			if (num2 != 0 && num != 0)
			{
				ushort prevSegment = TransportLine.GetPrevSegment(num);
				if (prevSegment != 0)
				{
					Singleton<NetManager>.get_instance().ReleaseSegment(prevSegment, true);
					Singleton<TransportManager>.get_instance().UpdateLine(lineID);
					flag = true;
				}
			}
			ushort num5;
			if (num2 != 0 && !this.CreateSegment(out num5, num2, num4))
			{
				this.ReleaseNode(num4);
				if (flag)
				{
					this.m_flags &= ~TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
				}
				return false;
			}
			ushort num6;
			if (num != 0 && !this.CreateSegment(out num6, num4, num))
			{
				this.ReleaseNode(num4);
				if (flag)
				{
					this.m_flags &= ~TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
				}
				return false;
			}
			if (this.m_stops == 0 || (num == this.m_stops && num2 == 0))
			{
				this.m_stops = num4;
			}
			Singleton<TransportManager>.get_instance().UpdateLine(lineID);
			if (num2 != 0 && num != 0)
			{
				this.AddWaveEvent(position, position, 2, 0);
			}
			else if (num2 != 0)
			{
				this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num2].m_position, 1, 1);
			}
			else if (num != 0)
			{
				this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position, 1, 1);
			}
			return true;
		}
	}

	public bool CanAddStop(ushort lineID, int addIndex, Vector3 position)
	{
		ushort num = 0;
		ushort num2 = 0;
		if (this.m_stops != 0)
		{
			if (addIndex == -1)
			{
				if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
				{
					num = 0;
					num2 = this.GetLastStop();
				}
				else
				{
					num = this.m_stops;
					num2 = TransportLine.GetPrevStop(this.m_stops);
				}
			}
			else
			{
				num = this.m_stops;
				num2 = TransportLine.GetPrevStop(this.m_stops);
				for (int i = 0; i < addIndex; i++)
				{
					num2 = num;
					num = TransportLine.GetNextStop(num);
					if (num == this.m_stops)
					{
						break;
					}
				}
			}
		}
		return ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None && num2 != 0 && num == 0 && this.m_stops != 0 && num2 != this.m_stops && Vector3.SqrMagnitude(position - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_stops].m_position) < 1f) || ((num2 == 0 || Vector3.SqrMagnitude(position - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num2].m_position) >= 1f) && (num == 0 || Vector3.SqrMagnitude(position - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position) >= 1f));
	}

	public bool CanMoveStop(ushort lineID, int index, Vector3 newPos)
	{
		if (this.m_stops == 0)
		{
			return false;
		}
		ushort num = this.m_stops;
		for (int i = 0; i < index; i++)
		{
			num = TransportLine.GetNextStop(num);
			if (num == this.m_stops)
			{
				break;
			}
		}
		if (num == 0)
		{
			return false;
		}
		ushort prevStop = TransportLine.GetPrevStop(num);
		ushort nextStop = TransportLine.GetNextStop(num);
		if (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position == newPos)
		{
			return true;
		}
		if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
		{
			if (num != this.m_stops && prevStop != 0 && nextStop == 0 && prevStop != this.m_stops && Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_stops].m_position) < 1f)
			{
				return true;
			}
			if (num == this.m_stops && prevStop == 0 && nextStop != 0)
			{
				ushort lastStop = this.GetLastStop();
				if (lastStop != 0 && lastStop != nextStop && Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)lastStop].m_position) < 1f)
				{
					return true;
				}
			}
		}
		return (prevStop == 0 || Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)prevStop].m_position) >= 1f) && (nextStop == 0 || Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)nextStop].m_position) >= 1f);
	}

	public bool CheckPrevPath(int stopIndex, out bool failed)
	{
		failed = false;
		if (this.m_stops != 0)
		{
			ushort num;
			if (stopIndex == -1)
			{
				if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
				{
					num = this.GetLastStop();
				}
				else
				{
					num = this.m_stops;
				}
			}
			else
			{
				num = this.m_stops;
				for (int i = 0; i < stopIndex; i++)
				{
					num = TransportLine.GetNextStop(num);
					if (num == this.m_stops)
					{
						break;
					}
				}
			}
			if (num == 0)
			{
				return false;
			}
			ushort prevSegment = TransportLine.GetPrevSegment(num);
			if (prevSegment == 0)
			{
				return true;
			}
			NetManager instance = Singleton<NetManager>.get_instance();
			if ((this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None && (instance.m_segments.m_buffer[(int)prevSegment].m_flags & NetSegment.Flags.WaitingPath) != NetSegment.Flags.None)
			{
				return false;
			}
			if ((instance.m_segments.m_buffer[(int)prevSegment].m_flags & NetSegment.Flags.PathFailed) != NetSegment.Flags.None)
			{
				failed = true;
				return false;
			}
			if (instance.m_segments.m_buffer[(int)prevSegment].m_path == 0u)
			{
				failed = true;
				return false;
			}
		}
		return true;
	}

	public bool CheckNextPath(int stopIndex, out bool failed)
	{
		failed = false;
		if (this.m_stops != 0)
		{
			ushort num;
			if (stopIndex == -1)
			{
				if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
				{
					num = this.GetLastStop();
				}
				else
				{
					num = this.m_stops;
				}
			}
			else
			{
				num = this.m_stops;
				for (int i = 0; i < stopIndex; i++)
				{
					num = TransportLine.GetNextStop(num);
					if (num == this.m_stops)
					{
						break;
					}
				}
			}
			if (num == 0)
			{
				return false;
			}
			ushort nextSegment = TransportLine.GetNextSegment(num);
			if (nextSegment == 0)
			{
				return true;
			}
			NetManager instance = Singleton<NetManager>.get_instance();
			if ((this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None && (instance.m_segments.m_buffer[(int)nextSegment].m_flags & NetSegment.Flags.WaitingPath) != NetSegment.Flags.None)
			{
				return false;
			}
			if ((instance.m_segments.m_buffer[(int)nextSegment].m_flags & NetSegment.Flags.PathFailed) != NetSegment.Flags.None)
			{
				failed = true;
				return false;
			}
			if (instance.m_segments.m_buffer[(int)nextSegment].m_path == 0u)
			{
				failed = true;
				return false;
			}
		}
		return true;
	}

	public void CopyMissingPaths(ushort source)
	{
		if (source != 0)
		{
			TransportManager instance = Singleton<TransportManager>.get_instance();
			NetManager instance2 = Singleton<NetManager>.get_instance();
			PathManager instance3 = Singleton<PathManager>.get_instance();
			ushort stops = instance.m_lines.m_buffer[(int)source].m_stops;
			ushort stops2 = this.m_stops;
			ushort num = stops;
			ushort num2 = stops2;
			int num3 = 0;
			while (num != 0 && num2 != 0)
			{
				ushort nextStop = TransportLine.GetNextStop(num);
				ushort nextStop2 = TransportLine.GetNextStop(num2);
				ushort nextSegment = TransportLine.GetNextSegment(num2);
				if (nextSegment != 0 && instance2.m_segments.m_buffer[(int)nextSegment].m_path == 0u)
				{
					ushort nextSegment2 = TransportLine.GetNextSegment(num);
					if (nextSegment2 != 0)
					{
						NetSegment.Flags flags = instance2.m_segments.m_buffer[(int)nextSegment2].m_flags;
						if ((flags & NetSegment.Flags.WaitingPath) == NetSegment.Flags.None)
						{
							uint path = instance2.m_segments.m_buffer[(int)nextSegment2].m_path;
							PathUnit.Position pathPos;
							if (instance3.m_pathUnits.m_buffer[(int)((UIntPtr)path)].GetPosition(0, out pathPos))
							{
								TransportLineAI.CheckNodePosition(instance2.m_segments.m_buffer[(int)nextSegment].m_startNode, pathPos);
							}
							if (instance3.m_pathUnits.m_buffer[(int)((UIntPtr)path)].GetLastPosition(out pathPos))
							{
								TransportLineAI.CheckNodePosition(instance2.m_segments.m_buffer[(int)nextSegment].m_endNode, pathPos);
							}
							if (instance3.AddPathReference(path))
							{
								instance2.m_segments.m_buffer[(int)nextSegment].m_path = path;
								if ((flags & NetSegment.Flags.PathFailed) == NetSegment.Flags.None)
								{
									NetSegment[] expr_195_cp_0 = instance2.m_segments.m_buffer;
									ushort expr_195_cp_1 = nextSegment;
									expr_195_cp_0[(int)expr_195_cp_1].m_flags = (expr_195_cp_0[(int)expr_195_cp_1].m_flags & ~NetSegment.Flags.PathFailed);
								}
							}
						}
					}
				}
				num = nextStop;
				num2 = nextStop2;
				if (num == stops)
				{
					break;
				}
				if (num2 == stops2)
				{
					break;
				}
				if (++num3 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.m_totalLength = instance.m_lines.m_buffer[(int)source].m_totalLength;
		}
	}

	public void ClearStops(ushort lineID)
	{
		if (this.m_stops != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = this.m_stops;
			int num2 = 0;
			while (num != 0 && (instance.m_nodes.m_buffer[(int)num].m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) == NetNode.Flags.Created)
			{
				ushort nextStop = TransportLine.GetNextStop(num);
				this.ReleaseNode(num);
				num = nextStop;
				if (num == this.m_stops)
				{
					break;
				}
				if (++num2 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.SetVehicleTargetStop(lineID, 0, 0);
			this.m_stops = 0;
			this.m_flags &= ~TransportLine.Flags.Complete;
			this.CheckCompletionMilestone();
			Singleton<TransportManager>.get_instance().UpdateLine(lineID);
		}
	}

	public void SetVehicleTargetStop(ushort lineID, ushort oldStop, ushort newStop)
	{
		if (this.m_vehicles != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num = this.m_vehicles;
			int num2 = 0;
			while (num != 0)
			{
				if (oldStop == 0 || oldStop == instance.m_vehicles.m_buffer[(int)num].m_targetBuilding)
				{
					VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
					info.m_vehicleAI.SetTarget(num, ref instance.m_vehicles.m_buffer[(int)num], newStop);
				}
				num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public bool MoveStop(ushort lineID, int index, Vector3 newPos, bool fixedPlatform)
	{
		Vector3 vector;
		return this.MoveStop(lineID, index, newPos, fixedPlatform, out vector);
	}

	public bool MoveStop(ushort lineID, int index, Vector3 newPos, bool fixedPlatform, out Vector3 oldPos)
	{
		if (this.m_stops == 0)
		{
			oldPos = newPos;
			return false;
		}
		ushort num = this.m_stops;
		for (int i = 0; i < index; i++)
		{
			num = TransportLine.GetNextStop(num);
			if (num == this.m_stops)
			{
				break;
			}
		}
		if (num == 0)
		{
			oldPos = newPos;
			return false;
		}
		ushort prevStop = TransportLine.GetPrevStop(num);
		ushort nextStop = TransportLine.GetNextStop(num);
		oldPos = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position;
		if (oldPos == newPos)
		{
			return true;
		}
		ushort lastStop = this.GetLastStop();
		this.ReleaseNode(num);
		if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
		{
			if (num != this.m_stops && prevStop != 0 && nextStop == 0 && prevStop != this.m_stops && Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_stops].m_position) < 1f)
			{
				this.SetVehicleTargetStop(lineID, num, this.m_stops);
				ushort num2;
				if (this.CreateSegment(out num2, prevStop, this.m_stops))
				{
					this.m_flags |= TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
					Singleton<TransportManager>.get_instance().UpdateLine(lineID);
					this.AddWaveEvent(newPos, oldPos, 1, -1);
					return true;
				}
				Singleton<TransportManager>.get_instance().UpdateLine(lineID);
				return false;
			}
			else if (num == this.m_stops && prevStop == 0 && nextStop != 0 && lastStop != 0 && lastStop != nextStop && Vector3.SqrMagnitude(newPos - Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)lastStop].m_position) < 1f)
			{
				this.SetVehicleTargetStop(lineID, num, nextStop);
				this.m_stops = nextStop;
				ushort num3;
				if (this.CreateSegment(out num3, lastStop, nextStop))
				{
					this.m_flags |= TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
					Singleton<TransportManager>.get_instance().UpdateLine(lineID);
					this.AddWaveEvent(newPos, oldPos, 1, -1);
					return true;
				}
				Singleton<TransportManager>.get_instance().UpdateLine(lineID);
				return false;
			}
		}
		ushort num4;
		if (this.CreateNode(lineID, out num4, newPos, fixedPlatform))
		{
			ushort num5;
			if (prevStop != 0 && !this.CreateSegment(out num5, prevStop, num4))
			{
				this.m_flags &= ~TransportLine.Flags.Complete;
				this.CheckCompletionMilestone();
			}
			ushort num6;
			if (nextStop != 0 && !this.CreateSegment(out num6, num4, nextStop))
			{
				this.m_flags &= ~TransportLine.Flags.Complete;
				this.CheckCompletionMilestone();
			}
		}
		if (num == this.m_stops)
		{
			if (num4 != 0)
			{
				this.m_stops = num4;
			}
			else if (nextStop != 0)
			{
				this.m_stops = nextStop;
			}
			else
			{
				this.m_stops = prevStop;
			}
		}
		this.SetVehicleTargetStop(lineID, num, num4);
		Singleton<TransportManager>.get_instance().UpdateLine(lineID);
		this.AddWaveEvent(newPos, oldPos, 1, -1);
		return true;
	}

	public bool RemoveStop(ushort lineID, int index)
	{
		Vector3 vector;
		return this.RemoveStop(lineID, index, out vector);
	}

	public bool RemoveStop(ushort lineID, int index, out Vector3 position)
	{
		if (this.m_stops == 0)
		{
			position = Vector3.get_zero();
			return false;
		}
		ushort num = 0;
		if (index == -1)
		{
			if ((this.m_flags & TransportLine.Flags.Complete) == TransportLine.Flags.None)
			{
				num = this.GetLastStop();
			}
			else
			{
				ushort prevSegment = TransportLine.GetPrevSegment(this.m_stops);
				if (prevSegment != 0)
				{
					ushort prevStop = TransportLine.GetPrevStop(this.m_stops);
					position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_stops].m_position;
					Singleton<NetManager>.get_instance().ReleaseSegment(prevSegment, true);
					this.m_flags &= ~TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
					Singleton<TransportManager>.get_instance().UpdateLine(lineID);
					this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)prevStop].m_position, -1, -1);
					return true;
				}
			}
		}
		else
		{
			num = this.m_stops;
			for (int i = 0; i < index; i++)
			{
				num = TransportLine.GetNextStop(num);
				if (num == this.m_stops)
				{
					break;
				}
			}
		}
		if (num == 0)
		{
			position = Vector3.get_zero();
			return false;
		}
		ushort prevStop2 = TransportLine.GetPrevStop(num);
		ushort nextStop = TransportLine.GetNextStop(num);
		if (num == this.m_stops)
		{
			if (nextStop != 0)
			{
				this.m_stops = nextStop;
			}
			else
			{
				this.m_stops = prevStop2;
			}
		}
		position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num].m_position;
		this.ReleaseNode(num);
		if (prevStop2 != 0 && nextStop != 0 && prevStop2 != nextStop)
		{
			ushort num2;
			if (!this.CreateSegment(out num2, prevStop2, nextStop))
			{
				this.m_flags &= ~TransportLine.Flags.Complete;
				this.CheckCompletionMilestone();
			}
		}
		else
		{
			this.m_flags &= ~TransportLine.Flags.Complete;
			this.CheckCompletionMilestone();
		}
		this.SetVehicleTargetStop(lineID, num, nextStop);
		Singleton<TransportManager>.get_instance().UpdateLine(lineID);
		if (prevStop2 != 0 && nextStop != 0 && prevStop2 != nextStop)
		{
			this.AddWaveEvent(position, position, -2, 0);
		}
		else if (prevStop2 != 0)
		{
			this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)prevStop2].m_position, -1, -1);
		}
		else if (nextStop != 0)
		{
			this.AddWaveEvent(position, Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)nextStop].m_position, -1, -1);
		}
		return true;
	}

	public void CloneLine(ushort lineID, ushort source)
	{
		this.ClearStops(lineID);
		if (source != 0)
		{
			TransportManager instance = Singleton<TransportManager>.get_instance();
			NetManager instance2 = Singleton<NetManager>.get_instance();
			PathManager instance3 = Singleton<PathManager>.get_instance();
			ushort stops = instance.m_lines.m_buffer[(int)source].m_stops;
			ushort num = stops;
			ushort num2 = 0;
			int num3 = 0;
			while (num != 0)
			{
				ushort nextStop = TransportLine.GetNextStop(num);
				Vector3 position = instance2.m_nodes.m_buffer[(int)num].m_position;
				bool fixedPlatform = (instance2.m_nodes.m_buffer[(int)num].m_flags & NetNode.Flags.Fixed) != NetNode.Flags.None;
				ushort num4;
				if (this.CreateNode(lineID, out num4, position, fixedPlatform))
				{
					if (num2 == 0)
					{
						this.m_stops = num4;
					}
					else
					{
						ushort prevSegment = TransportLine.GetPrevSegment(num);
						ushort num5;
						if (this.CreateSegment(out num5, num2, num4) && prevSegment != 0)
						{
							NetSegment.Flags flags = instance2.m_segments.m_buffer[(int)prevSegment].m_flags;
							if ((flags & NetSegment.Flags.WaitingPath) == NetSegment.Flags.None)
							{
								uint path = instance2.m_segments.m_buffer[(int)prevSegment].m_path;
								if (instance3.AddPathReference(path))
								{
									instance2.m_segments.m_buffer[(int)num5].m_path = path;
									if ((flags & NetSegment.Flags.PathFailed) == NetSegment.Flags.None)
									{
										NetSegment[] expr_156_cp_0 = instance2.m_segments.m_buffer;
										ushort expr_156_cp_1 = num5;
										expr_156_cp_0[(int)expr_156_cp_1].m_flags = (expr_156_cp_0[(int)expr_156_cp_1].m_flags & ~NetSegment.Flags.PathFailed);
									}
								}
							}
						}
					}
				}
				else
				{
					num4 = num2;
				}
				num2 = num4;
				num = nextStop;
				if (num == stops)
				{
					break;
				}
				if (++num3 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.m_totalLength = instance.m_lines.m_buffer[(int)source].m_totalLength;
			if (instance.m_lines.m_buffer[(int)source].Complete && num2 != 0 && this.m_stops != 0 && num2 != this.m_stops)
			{
				ushort prevSegment2 = TransportLine.GetPrevSegment(stops);
				ushort num6;
				if (this.CreateSegment(out num6, num2, this.m_stops))
				{
					this.m_flags |= TransportLine.Flags.Complete;
					this.CheckCompletionMilestone();
					if (prevSegment2 != 0)
					{
						NetSegment.Flags flags2 = instance2.m_segments.m_buffer[(int)prevSegment2].m_flags;
						if ((flags2 & NetSegment.Flags.WaitingPath) == NetSegment.Flags.None)
						{
							uint path2 = instance2.m_segments.m_buffer[(int)prevSegment2].m_path;
							if (instance3.AddPathReference(path2))
							{
								instance2.m_segments.m_buffer[(int)num6].m_path = path2;
								if ((flags2 & NetSegment.Flags.PathFailed) == NetSegment.Flags.None)
								{
									NetSegment[] expr_2C9_cp_0 = instance2.m_segments.m_buffer;
									ushort expr_2C9_cp_1 = num6;
									expr_2C9_cp_0[(int)expr_2C9_cp_1].m_flags = (expr_2C9_cp_0[(int)expr_2C9_cp_1].m_flags & ~NetSegment.Flags.PathFailed);
								}
							}
						}
					}
				}
			}
			if (this.m_stops != 0)
			{
				Singleton<TransportManager>.get_instance().UpdateLine(lineID);
			}
		}
	}

	private bool CreateNode(ushort lineID, out ushort node, Vector3 position, bool fixedPlatform)
	{
		TransportInfo info = this.Info;
		NetManager instance = Singleton<NetManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)lineID);
		if (instance.CreateNode(out node, ref randomizer, info.m_netInfo, position, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			NetNode[] expr_46_cp_0 = instance.m_nodes.m_buffer;
			ushort expr_46_cp_1 = node;
			expr_46_cp_0[(int)expr_46_cp_1].m_flags = (expr_46_cp_0[(int)expr_46_cp_1].m_flags | NetNode.Flags.Untouchable);
			instance.m_nodes.m_buffer[(int)node].m_transportLine = lineID;
			if (fixedPlatform)
			{
				NetNode[] expr_88_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_88_cp_1 = node;
				expr_88_cp_0[(int)expr_88_cp_1].m_flags = (expr_88_cp_0[(int)expr_88_cp_1].m_flags | NetNode.Flags.Fixed);
			}
			if ((this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
			{
				NetNode[] expr_B8_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_B8_cp_1 = node;
				expr_B8_cp_0[(int)expr_B8_cp_1].m_flags = (expr_B8_cp_0[(int)expr_B8_cp_1].m_flags | NetNode.Flags.Temporary);
			}
			else
			{
				Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			}
			if (this.m_building != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				instance.m_nodes.m_buffer[(int)node].m_nextBuildingNode = instance2.m_buildings.m_buffer[(int)this.m_building].m_netNode;
				instance2.m_buildings.m_buffer[(int)this.m_building].m_netNode = node;
			}
			return true;
		}
		node = 0;
		return false;
	}

	public void ReleaseNode(ushort node)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if (this.m_building != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			ushort num = instance2.m_buildings.m_buffer[(int)this.m_building].m_netNode;
			ushort num2 = 0;
			int num3 = 0;
			while (num != 0)
			{
				ushort nextBuildingNode = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				if (num == node)
				{
					if (num2 != 0)
					{
						instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode = nextBuildingNode;
					}
					else
					{
						instance2.m_buildings.m_buffer[(int)this.m_building].m_netNode = nextBuildingNode;
					}
					break;
				}
				num2 = num;
				num = nextBuildingNode;
				if (++num3 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		instance.ReleaseNode(node);
	}

	private bool CreateSegment(out ushort segment, ushort startNode, ushort endNode)
	{
		TransportInfo info = this.Info;
		NetManager instance = Singleton<NetManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)(startNode ^ endNode));
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = position2 - position;
		vector = VectorUtils.NormalizeXZ(vector);
		if (instance.CreateSegment(out segment, ref randomizer, info.m_netInfo, startNode, endNode, vector, -vector, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, false))
		{
			if ((this.m_flags & TransportLine.Flags.Temporary) == TransportLine.Flags.None)
			{
				Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
			}
			return true;
		}
		segment = 0;
		return false;
	}

	public ushort GetLastStop()
	{
		if (this.Complete)
		{
			return this.m_stops;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = this.m_stops;
		int num2 = 0;
		while (true)
		{
			bool flag = false;
			int i = 0;
			while (i < 8)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == num)
				{
					num = instance.m_segments.m_buffer[(int)segment].m_endNode;
					if (num == this.m_stops)
					{
						return num;
					}
					flag = true;
					break;
				}
				else
				{
					i++;
				}
			}
			if (++num2 >= 32768)
			{
				goto Block_5;
			}
			if (!flag)
			{
				return num;
			}
		}
		return num;
		Block_5:
		CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
		return num;
	}

	public static ushort GetNextStop(ushort stop)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)stop].GetSegment(i);
			if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == stop)
			{
				return instance.m_segments.m_buffer[(int)segment].m_endNode;
			}
		}
		return 0;
	}

	public static ushort GetPrevStop(ushort stop)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)stop].GetSegment(i);
			if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_endNode == stop)
			{
				return instance.m_segments.m_buffer[(int)segment].m_startNode;
			}
		}
		return 0;
	}

	public static ushort GetNextSegment(ushort stop)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)stop].GetSegment(i);
			if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == stop)
			{
				return segment;
			}
		}
		return 0;
	}

	public static ushort GetPrevSegment(ushort stop)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)stop].GetSegment(i);
			if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_endNode == stop)
			{
				return segment;
			}
		}
		return 0;
	}

	public void GetStopProgress(ushort targetID, out float min, out float max, out float total)
	{
		min = 0f;
		max = 0f;
		total = 0f;
		if (this.m_stops != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort stops = this.m_stops;
			ushort num = stops;
			int num2 = 0;
			while (num != 0)
			{
				ushort num3 = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
					if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == num)
					{
						num3 = instance.m_segments.m_buffer[(int)segment].m_endNode;
						if (num3 == targetID)
						{
							min = total;
						}
						total += instance.m_segments.m_buffer[(int)segment].m_averageLength;
						if (num3 == targetID)
						{
							max = total;
						}
						break;
					}
				}
				num = num3;
				if (num == stops)
				{
					break;
				}
				if (++num2 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public int CalculatePassengerCount(ushort stop)
	{
		if (stop == 0)
		{
			return 0;
		}
		ushort nextStop = TransportLine.GetNextStop(stop);
		if (nextStop == 0)
		{
			return 0;
		}
		TransportInfo info = this.Info;
		float num = (info.m_transportType != TransportInfo.TransportType.Bus) ? 64f : 32f;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector3 position = instance2.m_nodes.m_buffer[(int)stop].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		int num2 = Mathf.Max((int)((position.x - num) / 8f + 1080f), 0);
		int num3 = Mathf.Max((int)((position.z - num) / 8f + 1080f), 0);
		int num4 = Mathf.Min((int)((position.x + num) / 8f + 1080f), 2159);
		int num5 = Mathf.Min((int)((position.z + num) / 8f + 1080f), 2159);
		int num6 = 0;
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num7 = instance.m_citizenGrid[i * 2160 + j];
				int num8 = 0;
				while (num7 != 0)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num7].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num7].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num7].m_targetPos;
						float num9 = Vector3.SqrMagnitude(vector - position);
						if (num9 < num * num)
						{
							CitizenInfo info2 = instance.m_instances.m_buffer[(int)num7].Info;
							if (info2.m_citizenAI.TransportArriveAtSource(num7, ref instance.m_instances.m_buffer[(int)num7], position, position2))
							{
								num6++;
							}
						}
					}
					num7 = nextGridInstance;
					if (++num8 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num6;
	}

	public void SimulationStep(ushort lineID)
	{
		TransportInfo info = this.Info;
		if (this.Complete)
		{
			int num = 0;
			int num2 = 0;
			if (this.m_vehicles != 0)
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				ushort num3 = this.m_vehicles;
				int num4 = 0;
				while (num3 != 0)
				{
					ushort nextLineVehicle = instance.m_vehicles.m_buffer[(int)num3].m_nextLineVehicle;
					num++;
					if ((instance.m_vehicles.m_buffer[(int)num3].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
					{
						num2++;
					}
					num3 = nextLineVehicle;
					if (++num4 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			bool flag;
			if (Singleton<SimulationManager>.get_instance().m_isNightTime)
			{
				flag = ((this.m_flags & TransportLine.Flags.DisabledNight) == TransportLine.Flags.None);
			}
			else
			{
				flag = ((this.m_flags & TransportLine.Flags.DisabledDay) == TransportLine.Flags.None);
			}
			uint num5 = 0u;
			float num6 = 0f;
			int num7 = 0;
			bool flag2 = false;
			if (this.m_stops != 0)
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				ushort stops = this.m_stops;
				ushort num8 = stops;
				int num9 = 0;
				while (num8 != 0)
				{
					ushort num10 = 0;
					if (flag)
					{
						NetNode[] expr_139_cp_0 = instance2.m_nodes.m_buffer;
						ushort expr_139_cp_1 = num8;
						expr_139_cp_0[(int)expr_139_cp_1].m_flags = (expr_139_cp_0[(int)expr_139_cp_1].m_flags & ~NetNode.Flags.Disabled);
					}
					else
					{
						NetNode[] expr_15F_cp_0 = instance2.m_nodes.m_buffer;
						ushort expr_15F_cp_1 = num8;
						expr_15F_cp_0[(int)expr_15F_cp_1].m_flags = (expr_15F_cp_0[(int)expr_15F_cp_1].m_flags | NetNode.Flags.Disabled);
					}
					for (int i = 0; i < 8; i++)
					{
						ushort segment = instance2.m_nodes.m_buffer[(int)num8].GetSegment(i);
						if (segment != 0 && instance2.m_segments.m_buffer[(int)segment].m_startNode == num8)
						{
							num7 += Mathf.Max((int)instance2.m_segments.m_buffer[(int)segment].m_trafficLightState0, (int)instance2.m_segments.m_buffer[(int)segment].m_trafficLightState1);
							num6 += instance2.m_segments.m_buffer[(int)segment].m_averageLength;
							num10 = instance2.m_segments.m_buffer[(int)segment].m_endNode;
							if ((instance2.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.PathLength) == NetSegment.Flags.None)
							{
								flag2 = true;
							}
							break;
						}
					}
					num5 += 1u;
					num8 = num10;
					if (num8 == stops)
					{
						break;
					}
					if (++num9 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (!flag2)
			{
				this.m_totalLength = num6;
			}
			if (num5 != 0u)
			{
				this.m_averageInterval = (byte)Mathf.Min(255f, (float)(((long)num7 + (long)((ulong)(num5 >> 1))) / (long)((ulong)num5)));
			}
			int num11 = num * info.m_maintenanceCostPerVehicle / 100;
			if (num11 != 0)
			{
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num11, info.m_class);
			}
			TransferManager.TransferReason vehicleReason = info.m_vehicleReason;
			if (vehicleReason != TransferManager.TransferReason.None)
			{
				int num12;
				if (flag)
				{
					if (flag2)
					{
						num12 = num2;
					}
					else
					{
						num12 = this.CalculateTargetVehicleCount();
					}
				}
				else
				{
					num12 = 0;
				}
				if (num5 != 0u && num < num12)
				{
					int index = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num5);
					ushort stop = this.GetStop(index);
					if (vehicleReason != TransferManager.TransferReason.None && stop != 0)
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = num12 - num + 1;
						offer.TransportLine = lineID;
						offer.Position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)stop].m_position;
						offer.Amount = 1;
						offer.Active = false;
						Singleton<TransferManager>.get_instance().AddIncomingOffer(vehicleReason, offer);
					}
				}
				else if (num2 > num12)
				{
					int index2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num2);
					ushort activeVehicle = this.GetActiveVehicle(index2);
					if (activeVehicle != 0)
					{
						VehicleManager instance3 = Singleton<VehicleManager>.get_instance();
						if ((instance3.m_vehicles.m_buffer[(int)activeVehicle].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
						{
							VehicleInfo info2 = instance3.m_vehicles.m_buffer[(int)activeVehicle].Info;
							info2.m_vehicleAI.SetTransportLine(activeVehicle, ref instance3.m_vehicles.m_buffer[(int)activeVehicle], 0);
						}
					}
				}
			}
		}
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			this.m_passengers.Update();
			Singleton<TransportManager>.get_instance().m_passengers[(int)info.m_transportType].Add(ref this.m_passengers);
			this.m_passengers.Reset();
		}
	}

	public bool CanLeaveStop(ushort nextStop, int waitTime)
	{
		if (nextStop == 0)
		{
			return true;
		}
		ushort prevSegment = TransportLine.GetPrevSegment(nextStop);
		if (prevSegment == 0)
		{
			return true;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		int trafficLightState = (int)instance.m_segments.m_buffer[(int)prevSegment].m_trafficLightState0;
		int num = ((int)this.m_averageInterval - trafficLightState + 2) / 4;
		return num <= 0 || waitTime >= 4;
	}

	public void LeaveStop(ushort nextStop)
	{
		if (nextStop == 0)
		{
			return;
		}
		ushort prevSegment = TransportLine.GetPrevSegment(nextStop);
		if (prevSegment == 0)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_segments.m_buffer[(int)prevSegment].m_trafficLightState1 = instance.m_segments.m_buffer[(int)prevSegment].m_trafficLightState0;
		instance.m_segments.m_buffer[(int)prevSegment].m_trafficLightState0 = 0;
	}

	public int CalculateTargetVehicleCount()
	{
		TransportInfo info = this.Info;
		float num = this.m_totalLength;
		if (num == 0f && this.m_stops != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort stops = this.m_stops;
			ushort num2 = stops;
			int num3 = 0;
			while (num2 != 0)
			{
				ushort num4 = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num2].GetSegment(i);
					if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == num2)
					{
						num += instance.m_segments.m_buffer[(int)segment].m_averageLength;
						num4 = instance.m_segments.m_buffer[(int)segment].m_endNode;
						break;
					}
				}
				num2 = num4;
				if (num2 == stops)
				{
					break;
				}
				if (++num3 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		int num5 = Singleton<EconomyManager>.get_instance().GetBudget(info.m_class);
		num5 = (num5 * (int)this.m_budget + 50) / 100;
		return Mathf.CeilToInt((float)num5 * num / (info.m_defaultVehicleDistance * 100f));
	}

	private ushort GetActiveVehicle(int index)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = this.m_vehicles;
		int num2 = 0;
		while (num != 0)
		{
			if ((instance.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && index-- == 0)
			{
				return num;
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public bool UpdatePaths(ushort lineID)
	{
		bool flag = true;
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort stops = this.m_stops;
		ushort num = stops;
		int num2 = 0;
		while (num != 0)
		{
			ushort num3 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == num)
				{
					if (!this.UpdateSegment(lineID, segment, ref instance.m_segments.m_buffer[(int)segment]))
					{
						flag = false;
					}
					num3 = instance.m_segments.m_buffer[(int)segment].m_endNode;
					break;
				}
			}
			num = num3;
			if (num == stops)
			{
				break;
			}
			if (!flag)
			{
				break;
			}
			if (++num2 >= 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return flag;
	}

	private bool UpdateSegment(ushort lineID, ushort segmentID, ref NetSegment data)
	{
		TransportInfo info = this.Info;
		return TransportLineAI.UpdatePath(segmentID, ref data, info.m_netService, info.m_secondaryNetService, info.m_vehicleType, (this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None);
	}

	private bool StartPathFind(ushort lineID, ushort segmentID, ref NetSegment data)
	{
		TransportInfo info = this.Info;
		return TransportLineAI.StartPathFind(segmentID, ref data, info.m_netService, info.m_secondaryNetService, info.m_vehicleType, (this.m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None);
	}

	public bool UpdateMeshData(ushort lineID)
	{
		TransportManager instance = Singleton<TransportManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		PathManager instance3 = Singleton<PathManager>.get_instance();
		TerrainManager instance4 = Singleton<TerrainManager>.get_instance();
		TransportInfo info = this.Info;
		TransportLine.TempUpdateMeshData[] array;
		if (info.m_requireSurfaceLine)
		{
			array = new TransportLine.TempUpdateMeshData[81];
		}
		else
		{
			array = new TransportLine.TempUpdateMeshData[1];
		}
		bool flag = true;
		int num = 0;
		int num2 = 0;
		float num3 = 0f;
		ushort stops = this.m_stops;
		ushort num4 = stops;
		int num5 = 0;
		while (num4 != 0)
		{
			ushort num6 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance2.m_nodes.m_buffer[(int)num4].GetSegment(i);
				if (segment != 0 && instance2.m_segments.m_buffer[(int)segment].m_startNode == num4)
				{
					uint path = instance2.m_segments.m_buffer[(int)segment].m_path;
					if (path != 0u)
					{
						byte pathFindFlags = instance3.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_pathFindFlags;
						if ((pathFindFlags & 4) != 0)
						{
							Vector3 zero = Vector3.get_zero();
							if (!TransportLine.CalculatePathSegmentCount(path, 0, NetInfo.LaneType.All, VehicleInfo.VehicleType.All, ref array, ref num2, ref num3, ref zero))
							{
								this.StartPathFind(lineID, segment, ref instance2.m_segments.m_buffer[(int)segment]);
								flag = false;
							}
						}
						else if ((pathFindFlags & 8) == 0)
						{
							flag = false;
						}
					}
					num6 = instance2.m_segments.m_buffer[(int)segment].m_endNode;
					break;
				}
			}
			if (info.m_requireSurfaceLine)
			{
				TransportLine.TempUpdateMeshData[] expr_1A6_cp_0 = array;
				int expr_1A6_cp_1 = instance4.GetPatchIndex(instance2.m_nodes.m_buffer[(int)num4].m_position);
				expr_1A6_cp_0[expr_1A6_cp_1].m_pathSegmentCount = expr_1A6_cp_0[expr_1A6_cp_1].m_pathSegmentCount + 1;
			}
			else
			{
				TransportLine.TempUpdateMeshData[] expr_1C0_cp_0 = array;
				int expr_1C0_cp_1 = 0;
				expr_1C0_cp_0[expr_1C0_cp_1].m_pathSegmentCount = expr_1C0_cp_0[expr_1C0_cp_1].m_pathSegmentCount + 1;
			}
			num++;
			num4 = num6;
			if (num4 == stops)
			{
				break;
			}
			if (!flag)
			{
				break;
			}
			if (++num5 >= 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!flag)
		{
			return flag;
		}
		int num7 = 0;
		for (int j = 0; j < array.Length; j++)
		{
			int pathSegmentCount = array[j].m_pathSegmentCount;
			if (pathSegmentCount != 0)
			{
				RenderGroup.MeshData meshData = new RenderGroup.MeshData();
				meshData.m_vertices = new Vector3[pathSegmentCount * 8];
				meshData.m_normals = new Vector3[pathSegmentCount * 8];
				meshData.m_tangents = new Vector4[pathSegmentCount * 8];
				meshData.m_uvs = new Vector2[pathSegmentCount * 8];
				meshData.m_uvs2 = new Vector2[pathSegmentCount * 8];
				meshData.m_colors = new Color32[pathSegmentCount * 8];
				meshData.m_triangles = new int[pathSegmentCount * 30];
				array[j].m_meshData = meshData;
				num7++;
			}
		}
		TransportManager.LineSegment[] array2 = new TransportManager.LineSegment[num];
		Bezier3[] array3 = new Bezier3[num2];
		int num8 = 0;
		int num9 = 0;
		float lengthScale = Mathf.Ceil(num3 / 64f) / num3;
		float num10 = 0f;
		num4 = stops;
		Vector3 vector;
		vector..ctor(100000f, 100000f, 100000f);
		Vector3 vector2;
		vector2..ctor(-100000f, -100000f, -100000f);
		num5 = 0;
		while (num4 != 0)
		{
			ushort num11 = 0;
			for (int k = 0; k < 8; k++)
			{
				ushort segment2 = instance2.m_nodes.m_buffer[(int)num4].GetSegment(k);
				if (segment2 != 0 && instance2.m_segments.m_buffer[(int)segment2].m_startNode == num4)
				{
					uint path2 = instance2.m_segments.m_buffer[(int)segment2].m_path;
					if (path2 != 0u && (instance3.m_pathUnits.m_buffer[(int)((UIntPtr)path2)].m_pathFindFlags & 4) != 0)
					{
						array2[num8].m_curveStart = num9;
						Vector3 vector3;
						Vector3 vector4;
						TransportLine.FillPathSegments(path2, 0, NetInfo.LaneType.All, VehicleInfo.VehicleType.All, ref array, array3, null, ref num9, ref num10, lengthScale, out vector3, out vector4, info.m_requireSurfaceLine, true);
						vector = Vector3.Min(vector, vector3);
						vector2 = Vector3.Max(vector2, vector4);
						array2[num8].m_bounds.SetMinMax(vector3, vector4);
						array2[num8].m_curveEnd = num9;
					}
					num11 = instance2.m_segments.m_buffer[(int)segment2].m_endNode;
					break;
				}
			}
			if (info.m_requireSurfaceLine)
			{
				int patchIndex = instance4.GetPatchIndex(instance2.m_nodes.m_buffer[(int)num4].m_position);
				TransportLine.FillPathNode(instance2.m_nodes.m_buffer[(int)num4].m_position, array[patchIndex].m_meshData, array[patchIndex].m_pathSegmentIndex, 4f, 20f, true);
				TransportLine.TempUpdateMeshData[] expr_4FA_cp_0 = array;
				int expr_4FA_cp_1 = patchIndex;
				expr_4FA_cp_0[expr_4FA_cp_1].m_pathSegmentIndex = expr_4FA_cp_0[expr_4FA_cp_1].m_pathSegmentIndex + 1;
			}
			else
			{
				TransportLine.FillPathNode(instance2.m_nodes.m_buffer[(int)num4].m_position, array[0].m_meshData, array[0].m_pathSegmentIndex, 4f, 5f, false);
				TransportLine.TempUpdateMeshData[] expr_555_cp_0 = array;
				int expr_555_cp_1 = 0;
				expr_555_cp_0[expr_555_cp_1].m_pathSegmentIndex = expr_555_cp_0[expr_555_cp_1].m_pathSegmentIndex + 1;
			}
			num8++;
			num4 = num11;
			if (num4 == stops)
			{
				break;
			}
			if (++num5 >= 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		RenderGroup.MeshData[] array4 = new RenderGroup.MeshData[num7];
		int num12 = 0;
		for (int l = 0; l < array.Length; l++)
		{
			if (array[l].m_meshData != null)
			{
				array[l].m_meshData.UpdateBounds();
				if (info.m_requireSurfaceLine)
				{
					Vector3 min = array[l].m_meshData.m_bounds.get_min();
					Vector3 max = array[l].m_meshData.m_bounds.get_max();
					max.y += 1024f;
					array[l].m_meshData.m_bounds.SetMinMax(min, max);
				}
				array4[num12++] = array[l].m_meshData;
			}
		}
		while (!Monitor.TryEnter(instance.m_lineMeshData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			instance.m_lineMeshData[(int)lineID] = array4;
			instance.m_lineSegments[(int)lineID] = array2;
			instance.m_lineCurves[(int)lineID] = array3;
			this.m_bounds.SetMinMax(vector, vector2);
		}
		finally
		{
			Monitor.Exit(instance.m_lineMeshData);
		}
		return flag;
	}

	public static bool CalculatePathSegmentCount(uint path, int startIndex, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, ref TransportLine.TempUpdateMeshData[] data, ref int curveCount, ref float totalLength, ref Vector3 position)
	{
		bool flag = true;
		Vector3 vector = Vector3.get_zero();
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		TerrainManager instance3 = Singleton<TerrainManager>.get_instance();
		int num = 0;
		while (path != 0u)
		{
			uint nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_nextPathUnit;
			int positionCount = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_positionCount;
			for (int i = startIndex; i < positionCount; i++)
			{
				PathUnit.Position pathPos;
				if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].GetPosition(i, out pathPos))
				{
					return false;
				}
				NetInfo info = instance2.m_segments.m_buffer[(int)pathPos.m_segment].Info;
				if (info == null || info.m_lanes == null || info.m_lanes.Length <= (int)pathPos.m_lane)
				{
					return true;
				}
				if (!info.m_lanes[(int)pathPos.m_lane].CheckType(laneTypes, vehicleTypes))
				{
					return true;
				}
				uint laneID = PathManager.GetLaneID(pathPos);
				Vector3 vector2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
				position = vector2;
				if (flag)
				{
					vector = vector2;
					flag = false;
				}
				else
				{
					Vector3 vector3;
					float num2;
					instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetClosestPosition(vector, out vector3, out num2);
					float num3 = Vector3.Distance(vector3, vector);
					float num4 = Vector3.Distance(vector2, vector3);
					if (num3 > 1f)
					{
						int num5 = 0;
						if (data.Length > 1)
						{
							num5 = instance3.GetPatchIndex((vector + vector3) * 0.5f);
						}
						TransportLine.TempUpdateMeshData[] expr_19D_cp_0 = data;
						int expr_19D_cp_1 = num5;
						expr_19D_cp_0[expr_19D_cp_1].m_pathSegmentCount = expr_19D_cp_0[expr_19D_cp_1].m_pathSegmentCount + 1;
						curveCount++;
						totalLength += num3;
						vector = vector3;
					}
					if (num4 > 1f)
					{
						int num6 = 0;
						if (data.Length > 1)
						{
							num6 = instance3.GetPatchIndex((vector + vector2) * 0.5f);
						}
						TransportLine.TempUpdateMeshData[] expr_1FD_cp_0 = data;
						int expr_1FD_cp_1 = num6;
						expr_1FD_cp_0[expr_1FD_cp_1].m_pathSegmentCount = expr_1FD_cp_0[expr_1FD_cp_1].m_pathSegmentCount + 1;
						curveCount++;
						totalLength += num4;
						vector = vector2;
					}
				}
			}
			path = nextPathUnit;
			startIndex = 0;
			if (++num >= 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return true;
	}

	public static void FillPathSegments(uint path, int startIndex, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, ref TransportLine.TempUpdateMeshData[] data, Bezier3[] curves, Vector2[] curveOffsets, ref int curveIndex, ref float currentLength, float lengthScale, out Vector3 minPos, out Vector3 maxPos, bool ignoreY, bool useStopOffset)
	{
		bool flag = true;
		bool flag2 = true;
		bool flag3 = false;
		PathUnit.Position position = default(PathUnit.Position);
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		minPos..ctor(100000f, 100000f, 100000f);
		maxPos..ctor(-100000f, -100000f, -100000f);
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		TerrainManager instance3 = Singleton<TerrainManager>.get_instance();
		int num = 0;
		while (path != 0u)
		{
			uint nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_nextPathUnit;
			int positionCount = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_positionCount;
			for (int i = startIndex; i < positionCount; i++)
			{
				bool flag4 = nextPathUnit == 0u && i == positionCount - 1;
				PathUnit.Position position2;
				if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].GetPosition(i, out position2))
				{
					return;
				}
				NetInfo info = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
				if (info == null || info.m_lanes == null || info.m_lanes.Length <= (int)position2.m_lane)
				{
					return;
				}
				if (!info.m_lanes[(int)position2.m_lane].CheckType(laneTypes, vehicleTypes))
				{
					return;
				}
				NetInfo.LaneType laneType = info.m_lanes[(int)position2.m_lane].m_laneType;
				VehicleInfo.VehicleType vehicleType = info.m_lanes[(int)position2.m_lane].m_vehicleType;
				bool flag5 = laneType == NetInfo.LaneType.Pedestrian || (laneType == NetInfo.LaneType.Vehicle && vehicleType == VehicleInfo.VehicleType.Bicycle);
				uint laneID = PathManager.GetLaneID(position2);
				float num2 = (float)position2.m_offset * 0.003921569f;
				Vector3 vector3;
				Vector3 vector4;
				instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection(num2, out vector3, out vector4);
				minPos = Vector3.Min(minPos, vector3 - new Vector3(4f, 4f, 4f));
				maxPos = Vector3.Max(maxPos, vector3 + new Vector3(4f, 4f, 4f));
				if (flag)
				{
					vector = vector3;
					vector2 = vector4;
					flag = false;
				}
				else
				{
					Vector3 vector5;
					float num3;
					instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetClosestPosition(vector, out vector5, out num3);
					Vector3 vector6 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculateDirection(num3);
					minPos = Vector3.Min(minPos, vector5 - new Vector3(4f, 4f, 4f));
					maxPos = Vector3.Max(maxPos, vector5 + new Vector3(4f, 4f, 4f));
					float num4 = Vector3.Distance(vector5, vector);
					float num5 = Vector3.Distance(vector3, vector5);
					if (num5 > 1f)
					{
						if (num2 < num3)
						{
							vector6 = -vector6;
							vector4 = -vector4;
						}
					}
					else if (num2 > 0.5f)
					{
						vector6 = -vector6;
						vector4 = -vector4;
					}
					if (num4 > 1f)
					{
						int num6 = 0;
						if (data.Length > 1)
						{
							num6 = instance3.GetPatchIndex((vector + vector5) * 0.5f);
						}
						float num7 = currentLength + num4;
						Bezier3 bezier = default(Bezier3);
						if ((flag5 || flag3) && position.m_segment == position2.m_segment)
						{
							Vector3 vector7 = VectorUtils.NormalizeXZ(vector5 - vector);
							bezier.a = vector - vector7;
							bezier.b = vector * 0.7f + vector5 * 0.3f;
							bezier.c = vector5 * 0.7f + vector * 0.3f;
							bezier.d = vector5 + vector7;
						}
						else
						{
							bezier.a = vector;
							bezier.b = vector + vector2.get_normalized() * (num4 * 0.5f);
							bezier.c = vector5 - vector6.get_normalized() * (num4 * 0.5f);
							bezier.d = vector5;
						}
						TransportLine.FillPathSegment(bezier, data[num6].m_meshData, curves, data[num6].m_pathSegmentIndex, curveIndex, currentLength * lengthScale, num7 * lengthScale, 4f, (!ignoreY) ? 5f : 20f, ignoreY);
						if (curveOffsets != null)
						{
							curveOffsets[curveIndex] = new Vector2(currentLength * lengthScale, num7 * lengthScale);
						}
						TransportLine.TempUpdateMeshData[] expr_4E0_cp_0 = data;
						int expr_4E0_cp_1 = num6;
						expr_4E0_cp_0[expr_4E0_cp_1].m_pathSegmentIndex = expr_4E0_cp_0[expr_4E0_cp_1].m_pathSegmentIndex + 1;
						curveIndex++;
						currentLength = num7;
						vector = vector5;
						vector2 = vector6;
						flag2 = false;
					}
					if (num5 > 1f)
					{
						int num8 = 0;
						if (data.Length > 1)
						{
							num8 = instance3.GetPatchIndex((vector + vector3) * 0.5f);
						}
						float num9 = currentLength + num5;
						Bezier3 subCurve = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetSubCurve(num3, num2);
						if ((flag2 || flag4) && useStopOffset)
						{
							float num10 = instance2.m_segments.m_buffer[(int)position2.m_segment].Info.m_lanes[(int)position2.m_lane].m_stopOffset;
							if ((instance2.m_segments.m_buffer[(int)position2.m_segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
							{
								num10 = -num10;
							}
							if (num2 < num3)
							{
								num10 = -num10;
							}
							if (flag2)
							{
								subCurve.a += Vector3.Cross(Vector3.get_up(), vector6).get_normalized() * num10;
							}
							if (flag4)
							{
								subCurve.d += Vector3.Cross(Vector3.get_up(), vector4).get_normalized() * num10;
							}
						}
						TransportLine.FillPathSegment(subCurve, data[num8].m_meshData, curves, data[num8].m_pathSegmentIndex, curveIndex, currentLength * lengthScale, num9 * lengthScale, 4f, (!ignoreY) ? 5f : 20f, ignoreY);
						if (curveOffsets != null)
						{
							curveOffsets[curveIndex] = new Vector2(currentLength * lengthScale, num9 * lengthScale);
						}
						TransportLine.TempUpdateMeshData[] expr_6C9_cp_0 = data;
						int expr_6C9_cp_1 = num8;
						expr_6C9_cp_0[expr_6C9_cp_1].m_pathSegmentIndex = expr_6C9_cp_0[expr_6C9_cp_1].m_pathSegmentIndex + 1;
						curveIndex++;
						currentLength = num9;
						vector = vector3;
						vector2 = vector4;
						flag2 = false;
					}
				}
				flag3 = flag5;
				position = position2;
			}
			path = nextPathUnit;
			startIndex = 0;
			if (++num >= 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public Color GetColor()
	{
		Color result;
		if ((this.m_flags & TransportLine.Flags.CustomColor) != TransportLine.Flags.None)
		{
			result = this.m_color;
		}
		else
		{
			TransportInfo info = this.Info;
			result = Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)info.m_transportType];
		}
		if ((this.m_flags & (TransportLine.Flags.Temporary | TransportLine.Flags.Selected | TransportLine.Flags.Highlighted)) != TransportLine.Flags.None)
		{
			float num = Mathf.Sin(Singleton<SimulationManager>.get_instance().m_realTimer * 3.14159274f * 2f) * 0.5f + 1f;
			result.r *= num;
			result.g *= num;
			result.b *= num;
		}
		return result;
	}

	public void GetActive(out bool day, out bool night)
	{
		day = ((this.m_flags & TransportLine.Flags.DisabledDay) == TransportLine.Flags.None);
		night = ((this.m_flags & TransportLine.Flags.DisabledNight) == TransportLine.Flags.None);
	}

	private static Color32 CalculateVertexColor(Vector3 vertex, Bezier3 bezier)
	{
		Color32 result;
		result.r = (byte)Mathf.Clamp(Mathf.RoundToInt(bezier.a.y - vertex.y + 127.5f), 0, 255);
		result.g = (byte)Mathf.Clamp(Mathf.RoundToInt(bezier.b.y - vertex.y + 127.5f), 0, 255);
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(bezier.c.y - vertex.y + 127.5f), 0, 255);
		result.a = (byte)Mathf.Clamp(Mathf.RoundToInt(bezier.d.y - vertex.y + 127.5f), 0, 255);
		return result;
	}

	private static Color32 CalculateVertexColor(Vector3 vertex, Vector3 position)
	{
		Color32 result;
		result.r = (byte)Mathf.Clamp(Mathf.RoundToInt(position.y - vertex.y + 127.5f), 0, 255);
		result.g = result.r;
		result.b = result.r;
		result.a = result.r;
		return result;
	}

	public static void FillPathSegment(Bezier3 bezier, RenderGroup.MeshData meshData, Bezier3[] curves, int segmentIndex, int curveIndex, float startOffset, float endOffset, float halfWidth, float halfHeight, bool ignoreY)
	{
		if (curves != null)
		{
			curves[curveIndex] = bezier;
		}
		if (ignoreY)
		{
			bezier.a.y = 0f;
			bezier.b.y = 0f;
			bezier.c.y = 0f;
			bezier.d.y = 0f;
		}
		int num = segmentIndex * 8;
		int num2 = segmentIndex * 30;
		float num3 = Mathf.Abs(bezier.a.y - bezier.d.y) * 0.5f;
		Vector3 vector;
		vector..ctor(0f, Mathf.Max(Mathf.Max(0f, bezier.b.y - bezier.a.y) + halfHeight, num3), 0f);
		Vector3 vector2;
		vector2..ctor(0f, Mathf.Min(Mathf.Min(0f, bezier.b.y - bezier.a.y) - halfHeight, -num3), 0f);
		Vector3 vector3;
		vector3..ctor(0f, Mathf.Max(Mathf.Max(0f, bezier.c.y - bezier.d.y) + halfHeight, num3), 0f);
		Vector3 vector4;
		vector4..ctor(0f, Mathf.Min(Mathf.Min(0f, bezier.c.y - bezier.d.y) - halfHeight, -num3), 0f);
		Vector3 vector5;
		vector5..ctor(startOffset, endOffset, -halfWidth);
		Vector4 vector6;
		vector6..ctor(bezier.a.x, bezier.b.x, bezier.c.x, bezier.d.x);
		Vector2 vector7;
		vector7..ctor(bezier.a.z, bezier.b.z);
		Vector2 vector8;
		vector8..ctor(bezier.c.z, bezier.d.z);
		Vector3 normalized = (bezier.d - bezier.a).get_normalized();
		Vector3 normalized2 = Vector3.Cross(Vector3.get_up(), normalized).get_normalized();
		float num4 = Vector3.Dot(normalized2, bezier.b - bezier.a);
		float num5 = Vector3.Dot(normalized2, bezier.c - bezier.a);
		float num6 = Mathf.Min(0f, Mathf.Min(num4, num5)) - halfWidth;
		float num7 = Mathf.Max(0f, Mathf.Max(num4, num5)) + halfWidth;
		Vector3 vector9 = bezier.a + normalized2 * num6 - normalized * 4f;
		Vector3 vector10 = bezier.a + normalized2 * num7 - normalized * 4f;
		Vector3 vector11 = bezier.d + normalized2 * num6 + normalized * 4f;
		Vector3 vector12 = bezier.d + normalized2 * num7 + normalized * 4f;
		meshData.m_vertices[num] = vector9 + vector2;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector9 + vector;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector10 + vector2;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector10 + vector;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector11 + vector4;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector11 + vector3;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector12 + vector4;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_vertices[num] = vector12 + vector3;
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], bezier);
		meshData.m_normals[num] = vector5;
		meshData.m_tangents[num] = vector6;
		meshData.m_uvs[num] = vector7;
		meshData.m_uvs2[num] = vector8;
		num++;
		meshData.m_triangles[num2++] = num - 8;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 8;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 3;
	}

	public static void FillPathNode(Vector3 position, RenderGroup.MeshData meshData, int segmentIndex, float halfWidth, float halfHeight, bool ignoreY)
	{
		if (ignoreY)
		{
			position.y = 0f;
		}
		int num = segmentIndex * 8;
		int num2 = segmentIndex * 30;
		Vector3 vector;
		vector..ctor(0f, 0f, halfWidth);
		Vector4 vector2;
		vector2..ctor(position.x, position.x, position.x, position.x);
		Vector2 vector3;
		vector3..ctor(position.z, position.z);
		Vector2 vector4;
		vector4..ctor(position.z, position.z);
		Vector3 vector5 = position + new Vector3(-halfWidth, 0f, -halfWidth);
		Vector3 vector6 = position + new Vector3(halfWidth, 0f, -halfWidth);
		Vector3 vector7 = position + new Vector3(-halfWidth, 0f, halfWidth);
		Vector3 vector8 = position + new Vector3(halfWidth, 0f, halfWidth);
		meshData.m_vertices[num] = vector5 + new Vector3(0f, -halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector5 + new Vector3(0f, halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector6 + new Vector3(0f, -halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector6 + new Vector3(0f, halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector7 + new Vector3(0f, -halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector7 + new Vector3(0f, halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector8 + new Vector3(0f, -halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_vertices[num] = vector8 + new Vector3(0f, halfHeight, 0f);
		meshData.m_colors[num] = TransportLine.CalculateVertexColor(meshData.m_vertices[num], position);
		meshData.m_normals[num] = vector;
		meshData.m_tangents[num] = vector2;
		meshData.m_uvs[num] = vector3;
		meshData.m_uvs2[num] = vector4;
		num++;
		meshData.m_triangles[num2++] = num - 8;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 6;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 5;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 8;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 7;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 3;
		meshData.m_triangles[num2++] = num - 2;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 4;
		meshData.m_triangles[num2++] = num - 1;
		meshData.m_triangles[num2++] = num - 3;
	}

	public TransportPassengerData m_passengers;

	public Bounds m_bounds;

	public TransportLine.Flags m_flags;

	public Color32 m_color;

	public float m_totalLength;

	public ushort m_lineNumber;

	public ushort m_stops;

	public ushort m_vehicles;

	public ushort m_infoIndex;

	public ushort m_building;

	public ushort m_budget;

	public byte m_averageInterval;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Complete = 4,
		Temporary = 8,
		Hidden = 16,
		Selected = 32,
		Invalid = 64,
		CompleteSet = 128,
		CustomColor = 256,
		CustomName = 512,
		DisabledDay = 4096,
		DisabledNight = 8192,
		Highlighted = 65536,
		All = -1
	}

	public struct TempUpdateMeshData
	{
		public int m_pathSegmentCount;

		public int m_pathSegmentIndex;

		public RenderGroup.MeshData m_meshData;
	}
}
