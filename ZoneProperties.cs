﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class ZoneProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		ZoneProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new ZoneProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void InitializeShaderProperties()
	{
		for (int i = 0; i < this.m_zoneColors.Length; i++)
		{
			Shader.SetGlobalColor("_ZoneColor" + i, this.m_zoneColors[i].get_linear());
		}
		Shader.SetGlobalColor("_ZoneFillColor", this.m_fillColor.get_linear());
		Shader.SetGlobalColor("_ZoneEdgeColor", this.m_edgeColor.get_linear());
		Shader.SetGlobalColor("_ZoneEdgeColorOccupied", this.m_edgeColorOccupied.get_linear());
		Shader.SetGlobalColor("_ZoneFillColorInfo", this.m_fillColorInfo.get_linear());
		Shader.SetGlobalColor("_ZoneEdgeColorInfo", this.m_edgeColorInfo.get_linear());
		Shader.SetGlobalColor("_ZoneEdgeColorOccupiedInfo", this.m_edgeColorOccupiedInfo.get_linear());
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("ZoneProperties");
			Singleton<ZoneManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Shader m_zoneShader;

	public Color[] m_zoneColors;

	public Color m_fillColor = new Color(1f, 1f, 1f, 0.75f);

	public Color m_edgeColor = new Color(0.73f, 0.73f, 0.73f, 0.6f);

	public Color m_edgeColorOccupied = new Color(0.26f, 0.26f, 0.26f, 0.8f);

	public Color m_fillColorInfo = new Color(1f, 1f, 1f, 0.75f);

	public Color m_edgeColorInfo = new Color(0.73f, 0.73f, 0.73f, 1f);

	public Color m_edgeColorOccupiedInfo = new Color(0.35f, 0.35f, 0.35f, 0.7f);

	public Color m_unzoneColor = new Color(0.5f, 0.5f, 0.5f, 1f);

	public Color m_activeColor = new Color(1f, 1f, 1f, 1f);

	public EffectInfo m_fillEffect;
}
