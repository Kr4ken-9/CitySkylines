﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

internal class SubscribePresetStatusPanel : UICustomControl
{
	private void Awake()
	{
		this.m_StatusBar = base.Find<UIProgressBar>("StatusBar");
		this.m_StatusLabel = base.Find<UILabel>("StatusLabel");
		this.m_Cancel = base.Find<UIButton>("Cancel");
		this.m_Cancel.add_eventClick(new MouseEventHandler(this.OnCancelClick));
		this.m_LogLabel = base.Find<UILabel>("Log");
		this.m_Ids = new Dictionary<PublishedFileId, SubscribePresetStatusPanel.ItemStatus>();
		this.m_Failed = new HashSet<PublishedFileId>();
		this.m_Status = SubscribePresetStatusPanel.Status.Done;
	}

	public void BeginSubscribe(IEnumerable<PublishedFileId> ids, Action callback = null)
	{
		this.Reset();
		base.get_component().Show();
		foreach (PublishedFileId current in ids)
		{
			if (current != PublishedFileId.invalid)
			{
				this.m_Ids[current] = SubscribePresetStatusPanel.ItemStatus.DetailsPending;
			}
		}
		base.StartCoroutine(this.SubscribeCoroutine(callback));
	}

	private void OnCancelClick(UIComponent c, UIMouseEventParameter e)
	{
		this.Close();
	}

	private void Close()
	{
		this.Reset();
		base.get_component().Hide();
	}

	private void Reset()
	{
		if (this.m_Status != SubscribePresetStatusPanel.Status.Done)
		{
			base.StopAllCoroutines();
			this.UnRegisterEvents();
			this.EnablePackageEvents();
		}
		this.m_StatusBar.set_value(0f);
		this.m_StatusLabel.set_suffix(string.Empty);
		this.m_LogLabel.set_text(string.Empty);
		this.m_Cancel.set_localeID("CANCEL");
		this.m_Status = SubscribePresetStatusPanel.Status.Done;
		this.m_Ids.Clear();
		this.m_Failed.Clear();
		this.m_DetailsReceived = 0;
		this.m_ItemsInstalled = 0;
	}

	private void Update()
	{
		switch (this.m_Status)
		{
		case SubscribePresetStatusPanel.Status.Done:
			this.m_StatusBar.set_value(1f);
			break;
		case SubscribePresetStatusPanel.Status.DetailsPending:
			this.m_StatusBar.set_value((float)this.m_DetailsReceived / (float)this.m_Ids.Count * 0.3f);
			break;
		case SubscribePresetStatusPanel.Status.WaitingInstallation:
			this.m_StatusBar.set_value(0.3f + (float)this.m_ItemsInstalled / (float)(this.m_Ids.Count - this.m_Failed.Count) * 0.7f);
			break;
		default:
			this.m_StatusBar.set_value(0f);
			break;
		}
	}

	[DebuggerHidden]
	private IEnumerator SubscribeCoroutine(Action callback = null)
	{
		SubscribePresetStatusPanel.<SubscribeCoroutine>c__Iterator0 <SubscribeCoroutine>c__Iterator = new SubscribePresetStatusPanel.<SubscribeCoroutine>c__Iterator0();
		<SubscribeCoroutine>c__Iterator.callback = callback;
		<SubscribeCoroutine>c__Iterator.$this = this;
		return <SubscribeCoroutine>c__Iterator;
	}

	private void HandleTimeout()
	{
		foreach (KeyValuePair<PublishedFileId, SubscribePresetStatusPanel.ItemStatus> current in this.m_Ids)
		{
			if (current.Value < SubscribePresetStatusPanel.ItemStatus.DetailsReceived)
			{
				this.m_Failed.Add(current.Key);
				this.m_LogLabel.set_text(StringUtils.SafeFormat("{0}{1}: {2}\n", new object[]
				{
					this.m_LogLabel.get_text(),
					StringUtils.SafeFormat(Locale.Get("CONTENT_PRESET_FAILED"), current.Key),
					Locale.Get("CONTENT_PRESET_DOESNT_EXIST")
				}));
			}
		}
	}

	private void OnDetailsReceived(UGCDetails details, bool ioError)
	{
		if (Dispatcher.get_currentSafe() == ThreadHelper.get_dispatcher())
		{
			this.HandleDetails(details);
		}
		else
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.HandleDetails(details);
			});
		}
	}

	private void OnWorkshopItemInstalled(PublishedFileId id)
	{
		if (Dispatcher.get_currentSafe() == ThreadHelper.get_dispatcher())
		{
			this.HandleInstalled(id);
		}
		else
		{
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.HandleInstalled(id);
			});
		}
	}

	private void HandleDetails(UGCDetails details)
	{
		if (this.m_Status == SubscribePresetStatusPanel.Status.DetailsPending)
		{
			PublishedFileId publishedFileId = details.publishedFileId;
			if (this.m_Ids.ContainsKey(publishedFileId) && this.m_Ids[publishedFileId] < SubscribePresetStatusPanel.ItemStatus.DetailsReceived)
			{
				this.m_LastDetailsReceived = Time.get_time();
				Dictionary<PublishedFileId, SubscribePresetStatusPanel.ItemStatus> ids;
				PublishedFileId key;
				(ids = this.m_Ids)[key = publishedFileId] = ids[key] + 1;
				this.m_DetailsReceived++;
				if (details.result != 1)
				{
					this.m_Failed.Add(publishedFileId);
					this.m_LogLabel.set_text(StringUtils.SafeFormat("{0}{1}: {2}\n", new object[]
					{
						this.m_LogLabel.get_text(),
						StringUtils.SafeFormat(Locale.Get("CONTENT_PRESET_FAILED"), publishedFileId),
						details.result
					}));
				}
				this.m_StatusLabel.set_suffix(StringUtils.SafeFormat(" ({0}/{1})", new object[]
				{
					this.m_DetailsReceived,
					this.m_Ids.Count
				}));
			}
		}
	}

	private void HandleInstalled(PublishedFileId id)
	{
		if (this.m_Status == SubscribePresetStatusPanel.Status.WaitingInstallation && this.m_Ids.ContainsKey(id) && this.m_Ids[id] < SubscribePresetStatusPanel.ItemStatus.Installed)
		{
			Dictionary<PublishedFileId, SubscribePresetStatusPanel.ItemStatus> ids;
			(ids = this.m_Ids)[id] = ids[id] + 1;
			this.m_ItemsInstalled++;
			this.m_StatusLabel.set_suffix(StringUtils.SafeFormat(" ({0}/{1})", new object[]
			{
				this.m_ItemsInstalled,
				this.m_Ids.Count - this.m_Failed.Count
			}));
		}
	}

	private void DisablePackageEvents()
	{
		PackageManager.DisableEvents();
		PluginManager.DisableEvents();
	}

	private void EnablePackageEvents()
	{
		PackageManager.EnabledEvents();
		PluginManager.EnabledEvents();
		PackageManager.ForcePackagesChanged();
		Singleton<PluginManager>.get_instance().ForcePluginsChanged();
	}

	private void RegisterEvents()
	{
		this.UnRegisterEvents();
		PlatformService.get_workshop().add_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
		PlatformService.get_workshop().add_eventWorkshopItemInstalled(new Workshop.WorkshopItemInstalledHandler(this.OnWorkshopItemInstalled));
	}

	private void UnRegisterEvents()
	{
		PlatformService.get_workshop().remove_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
		PlatformService.get_workshop().remove_eventWorkshopItemInstalled(new Workshop.WorkshopItemInstalledHandler(this.OnWorkshopItemInstalled));
	}

	private const float kTimeout = 5f;

	private UIProgressBar m_StatusBar;

	private UILabel m_StatusLabel;

	private UIButton m_Cancel;

	private UILabel m_LogLabel;

	private Dictionary<PublishedFileId, SubscribePresetStatusPanel.ItemStatus> m_Ids;

	private HashSet<PublishedFileId> m_Failed;

	private int m_DetailsReceived;

	private int m_ItemsInstalled;

	private float m_LastDetailsReceived;

	private SubscribePresetStatusPanel.Status m_Status;

	public enum Status
	{
		Done,
		DetailsPending,
		WaitingInstallation
	}

	public enum ItemStatus
	{
		DetailsPending,
		DetailsReceived,
		Installed
	}
}
