﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class CitizenWorldInfoPanel : HumanWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_AgeEducation = base.Find<UILabel>("AgeEducation");
		this.m_Residence = base.Find<UIButton>("Residence");
		this.m_Residence.add_eventClick(new MouseEventHandler(base.OnTargetClick));
		this.m_Home = base.Find<UILabel>("Home");
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (this.m_InstanceID.Type == InstanceType.Citizen && this.m_InstanceID.Citizen != 0u)
			{
				uint citizen = this.m_InstanceID.Citizen;
				ushort homeBuilding = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
				this.m_AgeEducation.set_text(HumanWorldInfoPanel.GetAgeAndEducation(instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].EducationLevel, Citizen.GetAgeGroup(instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Age)));
				if (homeBuilding != 0 && Singleton<BuildingManager>.get_exists())
				{
					InstanceID instanceID = default(InstanceID);
					instanceID.Building = homeBuilding;
					this.m_Residence.set_objectUserData(instanceID);
					this.m_Home.set_text(Locale.Get("CITIZEN_OCCUPATION_RESIDENCE"));
					this.m_Residence.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(homeBuilding, this.m_InstanceID));
					base.ShortenTextToFitParent(this.m_Residence);
					this.m_Residence.get_parent().set_isVisible(true);
				}
				else
				{
					this.m_Residence.get_parent().set_isVisible(false);
				}
			}
		}
	}

	private UILabel m_AgeEducation;

	private UILabel m_Home;

	private UIButton m_Residence;
}
