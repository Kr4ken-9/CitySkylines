﻿using System;
using ColossalFramework.Globalization;

public class LocaleID
{
	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string BUILDING_TITLE = "BUILDING_TITLE";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string BUILDING_DESC = "BUILDING_DESC";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string BUILDING_SHORT_DESC = "BUILDING_SHORT_DESC";

	[Locale(file = "en-patch1.0.9", category = "Buildings", hideInEditor = true)]
	public const string PROPS_TITLE = "PROPS_TITLE";

	[Locale(file = "en-patch1.0.9", category = "Buildings", hideInEditor = true)]
	public const string PROPS_DESC = "PROPS_DESC";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string TREE_TITLE = "TREE_TITLE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string TREE_DESC = "TREE_DESC";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = true)]
	public const string DECORATIONEDITOR_TOOL = "DECORATIONEDITOR_TOOL";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string DECORATION_SELECTOR_TITLE = "DECORATION_SELECTOR_TITLE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string DECORATION_SELECTOR_USEEXISTINGDECORATION = "DECORATION_SELECTOR_USEEXISTINGDECORATION";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string DECORATIONEDITOR_LIGHTROTATION = "DECORATIONEDITOR_LIGHTROTATION";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = true)]
	public const string SURFACE_TITLE = "SURFACE_TITLE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = true)]
	public const string SURFACE_DESC = "SURFACE_DESC";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string CHIRPHEADER_TREESNPROPS = "CHIRPHEADER_TREESNPROPS";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = true)]
	public const string CONFIRM_RETURNDECORATIONEDITOR = "CONFIRM_RETURNDECORATIONEDITOR";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = true)]
	public const string CONFIRM_EXITDECORATIONEDITOR = "CONFIRM_EXITDECORATIONEDITOR";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_TITLE = "ASSETIMPORTER_TITLE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_SHADED = "ASSETIMPORTER_SHADED";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_LIGHTINGONLY = "ASSETIMPORTER_LIGHTINGONLY";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_GRID = "ASSETIMPORTER_GRID";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_MAXIMIZE = "ASSETIMPORTER_MAXIMIZE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_MINIMIZE = "ASSETIMPORTER_MINIMIZE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_ROTATION = "ASSETIMPORTER_ROTATION";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_SCALE = "ASSETIMPORTER_SCALE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_PIVOTBOTTOMCENTER = "ASSETIMPORTER_PIVOTBOTTOMCENTER";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_BACKTOMAINMENU = "ASSETIMPORTER_BACKTOMAINMENU";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_STATE = "ASSETIMPORTER_STATE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_SELECTASSET = "ASSETIMPORTER_SELECTASSET";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSEIMPORTER_BACK = "ASSEIMPORTER_BACK";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETIMPORTER_CONTINUE = "ASSETIMPORTER_CONTINUE";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = true)]
	public const string DECORATIONSETTINGS_TITLE = "DECORATIONSETTINGS_TITLE";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = true)]
	public const string DECORATIONSETTINGS_DESC = "DECORATIONSETTINGS_DESC";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSET_NAME = "ASSET_NAME";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_BUILDING = "ASSETTYPE_BUILDING";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_PROP = "ASSETTYPE_PROP";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_TREE = "ASSETTYPE_TREE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_VEHICLE = "ASSETTYPE_VEHICLE";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_CITIZEN = "ASSETTYPE_CITIZEN";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_INTERSECTION = "ASSETTYPE_INTERSECTION";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_PARK = "ASSETTYPE_PARK";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTYPE_DESCRIPTION = "ASSETTYPE_DESCRIPTION";

	[Locale(file = "en-editors", category = "AssetEditor", hideInEditor = false)]
	public const string ASSETTEMPLATE_DESCRIPTION = "ASSETTEMPLATE_DESCRIPTION";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_BRUSHSIZE = "MAPEDITOR_BRUSHSIZE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_BRUSHSTRENGTH = "MAPEDITOR_BRUSHSTRENGTH";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_WATERCAPACITY = "MAPEDITOR_WATERCAPACITY";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_SELECTBRUSH = "MAPEDITOR_SELECTBRUSH";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_SEALEVEL = "MAPEDITOR_SEALEVEL";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_TERRAINLEVEL = "MAPEDITOR_TERRAINLEVEL";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_RESET_WATER = "MAPEDITOR_RESET_WATER";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_UNDO_TERRAIN = "MAPEDITOR_UNDO_TERRAIN";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string HEIGHTMAP_IMPORT = "HEIGHTMAP_IMPORT";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string HEIGHTMAP_EXPORT = "HEIGHTMAP_EXPORT";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string HEIGHTMAP_LOADERROR = "HEIGHTMAP_LOADERROR";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string CONFIRM_HEIGHTMAPOVERRIDE = "CONFIRM_HEIGHTMAPOVERRIDE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string CONFIRM_RETURNMAPEDITOR = "CONFIRM_RETURNMAPEDITOR";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string CONFIRM_EXITMAPEDITOR = "CONFIRM_EXITMAPEDITOR";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string CONFIRM_NEWMAP = "CONFIRM_NEWMAP";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPEDITOR_TOOL = "MAPEDITOR_TOOL";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPEDITOR_INFOVIEWS = "MAPEDITOR_INFOVIEWS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPEDITOR_NET_TITLE = "MAPEDITOR_NET_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPEDITOR_NET_DESC = "MAPEDITOR_NET_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_TREES = "CHIRPHEADER_TREES";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_PROPS = "CHIRPHEADER_PROPS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_SHIP_CONNECTIONS = "CHIRPHEADER_SHIP_CONNECTIONS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_PLANE_CONNECTIONS = "CHIRPHEADER_PLANE_CONNECTIONS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_TRAIN_CONNECTIONS = "CHIRPHEADER_TRAIN_CONNECTIONS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string CHIRPHEADER_ROAD_CONNECTIONS = "CHIRPHEADER_ROAD_CONNECTIONS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS = "EDITORCHIRPER_REQUIREMENTS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_WATER = "EDITORCHIRPER_REQUIREMENTS_WATER";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_SHIP = "EDITORCHIRPER_REQUIREMENTS_SHIP";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_PLANE = "EDITORCHIRPER_REQUIREMENTS_PLANE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_TRAIN = "EDITORCHIRPER_REQUIREMENTS_TRAIN";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_ROADIN = "EDITORCHIRPER_REQUIREMENTS_ROADIN";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_ROADOUT = "EDITORCHIRPER_REQUIREMENTS_ROADOUT";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_RECOMMENDED = "EDITORCHIRPER_REQUIREMENTS_RECOMMENDED";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_OIL = "EDITORCHIRPER_REQUIREMENTS_OIL";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_FOREST = "EDITORCHIRPER_REQUIREMENTS_FOREST";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_ORE = "EDITORCHIRPER_REQUIREMENTS_ORE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string EDITORCHIRPER_REQUIREMENTS_FERTILITY = "EDITORCHIRPER_REQUIREMENTS_FERTILITY";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAPEDITOR_UNFITREQUIREMENTS = "MAPEDITOR_UNFITREQUIREMENTS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string TERRAIN_TITLE = "TERRAIN_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string TERRAIN_DESC = "TERRAIN_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPSETTINGS_TITLE = "MAPSETTINGS_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string MAPSETTINGS_DESC = "MAPSETTINGS_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string WATER_TITLE = "WATER_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string WATER_DESC = "WATER_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string RESOURCE_TITLE = "RESOURCE_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string RESOURCE_DESC = "RESOURCE_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string ENVIRONMENT_TITLE = "ENVIRONMENT_TITLE";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = true)]
	public const string ENVIRONMENT_DESC = "ENVIRONMENT_DESC";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string NO_SNAPSHOTS = "NO_SNAPSHOTS";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string PUBLISH_MAP = "PUBLISH_MAP";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string MAP_NAME = "MAP_NAME";

	[Locale(file = "en-editors", category = "MapEditor", hideInEditor = false)]
	public const string TOOL_SINGLE = "TOOL_SINGLE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string NET_TITLE = "NET_TITLE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string NET_DESC = "NET_DESC";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string VEHICLE_TITLE = "VEHICLE_TITLE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = true)]
	public const string NOTIFICATION_TITLE = "NOTIFICATION_TITLE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = true)]
	public const string NOTIFICATION_NORMAL = "NOTIFICATION_NORMAL";

	[Locale(file = "en-steamachievements", category = "Menus", hideInEditor = true)]
	public const string ACH_TITLE = "ACH_TITLE";

	[Locale(file = "en-steamachievements", category = "Menus", hideInEditor = true)]
	public const string ACH_DESC = "ACH_DESC";

	[Locale(file = "en-main", category = "Policies", hideInEditor = true)]
	public const string POLICIES = "POLICIES";

	[Locale(file = "en-main", category = "Policies", hideInEditor = true)]
	public const string POLICIES_DETAIL = "POLICIES_DETAIL";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = true)]
	public const string SPECIALIZATION_TITLE = "SPECIALIZATION_TITLE";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = true)]
	public const string SPECIALIZATION_DESC = "SPECIALIZATION_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string POLICYTYPE_DESC = "POLICYTYPE_DESC";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_TAXIDEPOT_VEHICLES = "AIINFO_TAXIDEPOT_VEHICLES";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_TAXISTAND_VEHICLES = "AIINFO_TAXISTAND_VEHICLES";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_POLICESTATION_CRIMINALS = "AIINFO_POLICESTATION_CRIMINALS";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_PRISON_CRIMINALS = "AIINFO_PRISON_CRIMINALS";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_PRISON_CARS = "AIINFO_PRISON_CARS";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string AIINFO_PRISONCAR_COUNT = "AIINFO_PRISONCAR_COUNT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_PICKINGUP = "VEHICLE_STATUS_TAXI_PICKINGUP";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_TRANSPORTING = "VEHICLE_STATUS_TAXI_TRANSPORTING";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_PLANNING = "VEHICLE_STATUS_TAXI_PLANNING";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_WAIT = "VEHICLE_STATUS_TAXI_WAIT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_HEADING = "VEHICLE_STATUS_TAXI_HEADING";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_TAXI_RETURN = "VEHICLE_STATUS_TAXI_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string VEHICLE_BUFFER = "VEHICLE_BUFFER";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string CITIZEN_STATUS_WAITING_TAXI = "CITIZEN_STATUS_WAITING_TAXI";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string MAIN_TOOL = "MAIN_TOOL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = true)]
	public const string BUDGET_INFO = "BUDGET_INFO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string MAIN_CATEGORY = "MAIN_CATEGORY";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string SUBSERVICE_DESC = "SUBSERVICE_DESC";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_PRISON_PICKINGUP = "VEHICLE_STATUS_PRISON_PICKINGUP";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_PRISON_WAIT = "VEHICLE_STATUS_PRISON_WAIT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_PRISON_RETURN = "VEHICLE_STATUS_PRISON_RETURN";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_ANYBOAT_WAIT = "VEHICLE_STATUS_ANYBOAT_WAIT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_ANYBOAT_HEADING = "VEHICLE_STATUS_ANYBOAT_HEADING";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_ANYBOAT_RETURN = "VEHICLE_STATUS_ANYBOAT_RETURN";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_ANYBOAT_PARKED = "VEHICLE_STATUS_ANYBOAT_PARKED";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string GUI_TOOLTIP = "GUI_TOOLTIP";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string INFO_RES_BEACH = "INFO_RES_BEACH";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = true)]
	public const string TRANS_BUDGET_INFO = "TRANS_BUDGET_INFO";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = true)]
	public const string TRANS_TUTORIAL_ADVISER = "TRANS_TUTORIAL_ADVISER";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string TRANSPORT_LINE_ACTIVITY = "TRANSPORT_LINE_ACTIVITY";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string TRANSPORT_LINE_DAY = "TRANSPORT_LINE_DAY";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string TRANSPORT_LINE_NIGHT = "TRANSPORT_LINE_NIGHT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string TRANSPORT_LINE_DAYNNIGHT = "TRANSPORT_LINE_DAYNNIGHT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string INFO_CRIMERATE_PRISONAVAILABILITY = "INFO_CRIMERATE_PRISONAVAILABILITY";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string INFO_CRIMERATE_CRIMES = "INFO_CRIMERATE_CRIMES";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string INFO_CRIMERATE_INMATES = "INFO_CRIMERATE_INMATES";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string STYLES_SELECT = "STYLES_SELECT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string STYLES_SELECT_TOOLTIP = "STYLES_SELECT_TOOLTIP";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string STYLES_EUROPEAN = "STYLES_EUROPEAN";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string STYLES_DEFAULT = "STYLES_DEFAULT";

	[Locale(file = "en-expansion1", category = "Policies and misc", hideInEditor = false)]
	public const string STYLES_MISSING_TOOLTIP = "STYLES_MISSING_TOOLTIP";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string DISTRICT_TITLE = "DISTRICT_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string DISTRICT_DESC = "DISTRICT_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_LATEST_PROG = "MILESTONE_STAT_LATEST_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_LATEST_DESC = "MILESTONE_STAT_LATEST_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_TOTAL_PROG = "MILESTONE_STAT_TOTAL_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_TOTAL_DESC = "MILESTONE_STAT_TOTAL_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string UNLOCK_MONUMENT_TITLE = "UNLOCK_MONUMENT_TITLE";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS1 = "CREDITS1";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS3 = "CREDITS3";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS5 = "CREDITS5";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS7 = "CREDITS7";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS9 = "CREDITS9";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS10 = "CREDITS10";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS12 = "CREDITS12";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS14 = "CREDITS14";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS15 = "CREDITS15";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS17 = "CREDITS17";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS19 = "CREDITS19";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS21 = "CREDITS21";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS23 = "CREDITS23";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS24 = "CREDITS24";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS25 = "CREDITS25";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS26 = "CREDITS26";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS27 = "CREDITS27";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS28 = "CREDITS28";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS29 = "CREDITS29";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS30 = "CREDITS30";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS32 = "CREDITS32";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS34 = "CREDITS34";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS36 = "CREDITS36";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS38 = "CREDITS38";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS39 = "CREDITS39";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS41 = "CREDITS41";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS43 = "CREDITS43";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS45 = "CREDITS45";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS47 = "CREDITS47";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS49 = "CREDITS49";

	[Locale(file = "en-expansion1", category = "Credits", hideInEditor = false)]
	public const string CREDITS51 = "CREDITS51";

	[Locale(file = "en-modderbuildings", category = "Misc", hideInEditor = true)]
	public const string BUILDING_NAME = "BUILDING_NAME";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_PRISON = "CHIRP_FIRST_PRISON";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TAXIDEPOT = "CHIRP_FIRST_TAXIDEPOT";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BUSTATION = "CHIRP_FIRST_BUSTATION";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_LARGEAIRPORT = "CHIRP_FIRST_LARGEAIRPORT";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CARGOHUB = "CHIRP_FIRST_CARGOHUB";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_STABLE = "CHIRP_FIRST_STABLE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_POLICY = "CHIRP_POLICY";

	[Locale(file = "en-expansion1", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NO_PRISONS = "CHIRP_NO_PRISONS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RANDOM = "CHIRP_RANDOM";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_AD_DISABLED = "WORKSHOP_AD_DISABLED";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_TYPE = "OPTIONS_DOF_TYPE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_AMOUNT = "OPTIONS_DOF_AMOUNT";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_DISABLED = "OPTIONS_DOF_DISABLED";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_TILTSHIFT = "OPTIONS_DOF_TILTSHIFT";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_NORMAL = "OPTIONS_DOF_NORMAL";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_DOF_HIGH = "OPTIONS_DOF_HIGH";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_MOUSELIGHTINTENSITY = "OPTIONS_MOUSELIGHTINTENSITY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_ENABLE_DAY_NIGHT = "OPTIONS_ENABLE_DAY_NIGHT";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TAXI = "INFO_PUBLICTRANSPORT_TAXI";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string TIMECONTROL = "TIMECONTROL";

	[Locale(file = "en-main", category = "Props", hideInEditor = true)]
	public const string PROPS_CATEGORY = "PROPS_CATEGORY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string LINE_SCHEDULE = "LINE_SCHEDULE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string LINE_SCHEDULE_DAY = "LINE_SCHEDULE_DAY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string LINE_SCHEDULE_NIGHT = "LINE_SCHEDULE_NIGHT";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string LINE_SCHEDULE_BOTH = "LINE_SCHEDULE_BOTH";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string INFO_CRIME_JAIL_AVAILABILITY = "INFO_CRIME_JAIL_AVAILABILITY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string INFO_CRIME_PRISONERS = "INFO_CRIME_PRISONERS";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string INFO_CRIME_JAILCAPACITY = "INFO_CRIME_JAILCAPACITY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_ADDSTYLE = "WORKSHOP_ADDSTYLE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_ADDSTYLE_TOOLTIP = "WORKSHOP_ADDSTYLE_TOOLTIP";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_SHOWSTYLEASSETS = "CONTENTMANAGER_SHOWSTYLEASSETS";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLEASSETREMOVE_TOOLTIP = "CONTENTMANAGER_STYLEASSETREMOVE_TOOLTIP";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_NEWSTYLE = "WORKSHOP_NEWSTYLE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_STYLES = "WORKSHOP_STYLES";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLES = "CONTENTMANAGER_STYLES";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_ADDTOSTYLE = "CONTENTMANAGER_ADDTOSTYLE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_ADDEDTOSTYLE = "CONTENTMANAGER_ADDEDTOSTYLE";

	[Locale(file = "en-expansion5", category = "Specializations", hideInEditor = true)]
	public const string DISTRICT_CATEGORY = "DISTRICT_CATEGORY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string UNLOCK_SPECIALIZATION_COMMERCIAL = "UNLOCK_SPECIALIZATION_COMMERCIAL";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string DISTRICT_SPECIALIZATION = "DISTRICT_SPECIALIZATION";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string DISTRICT_SPECIALIZATION_COMMERCIAL = "DISTRICT_SPECIALIZATION_COMMERCIAL";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string DISTRICT_SPECIALIZATION_INDUSTRIAL = "DISTRICT_SPECIALIZATION_INDUSTRIAL";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLEREPLACES = "CONTENTMANAGER_STYLEREPLACES";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLEINCOMPLETE = "CONTENTMANAGER_STYLEINCOMPLETE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_ASSET_STYLECOUNT = "CONTENTMANAGER_ASSET_STYLECOUNT";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string ECONOMY_LEISURE = "ECONOMY_LEISURE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string ECONOMY_TOURIST = "ECONOMY_TOURIST";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSTYLE = "WORKSHOP_PUBLISHSTYLE";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLELEGACY = "CONTENTMANAGER_STYLELEGACY";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLESUBSCRIBEALL = "CONTENTMANAGER_STYLESUBSCRIBEALL";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_STYLELEVEL = "CONTENTMANAGER_STYLELEVEL";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string FEATURES = "FEATURES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string FEATURES_DESC = "FEATURES_DESC";

	[Locale(file = "en-expansion1", category = "Menu", hideInEditor = true)]
	public const string DISCLAIMER_MESSAGE = "DISCLAIMER_MESSAGE";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TRAMDEPOT = "CHIRP_FIRST_TRAMDEPOT";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SAUNA = "CHIRP_FIRST_SAUNA";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SKIRESORT = "CHIRP_FIRST_SKIRESORT";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SPAHOTEL = "CHIRP_FIRST_SPAHOTEL";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BOILERSTATION = "CHIRP_FIRST_BOILERSTATION";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SNOWDUMP = "CHIRP_FIRST_SNOWDUMP";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ROADMAINTENANCEDEPOT = "CHIRP_FIRST_ROADMAINTENANCEDEPOT";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_GEOTHERMALPLANT = "CHIRP_FIRST_GEOTHERMALPLANT";

	[Locale(file = "en-expansion2", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RANDOM_THEME = "CHIRP_RANDOM_THEME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CANAL_OPTION_STRAIGHT = "CANAL_OPTION_STRAIGHT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CANAL_OPTION_CURVED = "CANAL_OPTION_CURVED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CANAL_OPTION_FREEFORM = "CANAL_OPTION_FREEFORM";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string SNOW_SERVICE_INFO_VIEW_AS_GARBAGE_TAB = "SNOW_SERVICE_INFO_VIEW_AS_GARBAGE_TAB";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string SNOW_INFO_VIEW_BUTTON_MOUSEOVER = "SNOW_INFO_VIEW_BUTTON_MOUSEOVER";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string ROAD_MAINTENANCE_INFO_VIEW = "ROAD_MAINTENANCE_INFO_VIEW";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string MOUSEOVER_TEXT_FOR_THERMOMETER = "MOUSEOVER_TEXT_FOR_THERMOMETER";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string UPDATE_WATER_PIPES_TOOL_MOUSEOVER = "UPDATE_WATER_PIPES_TOOL_MOUSEOVER";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string UPDATE_WATER_PIPES_TOOL_DESCRIPTION = "UPDATE_WATER_PIPES_TOOL_DESCRIPTION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_1 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_1";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_2 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_2";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_3 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_3";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_4 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_4";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_5 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_5";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_6 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_6";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_7 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_7";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_8 = "EXTENDED_PUBLIC_TRANSPORT_UI_ITEM_8";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string TRANSPORT_TITLE = "TRANSPORT_TITLE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string TRANSPORT_DESC = "TRANSPORT_DESC";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string TRANSPORT_LINE_PATTERN = "TRANSPORT_LINE_PATTERN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string TRANSPORT_LINE = "TRANSPORT_LINE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string AIINFO_TRAMDEPOT_TRAMCOUNT = "AIINFO_TRAMDEPOT_TRAMCOUNT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string VEHICLE_STATUS_TRAM_STOPPED = "VEHICLE_STATUS_TRAM_STOPPED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string VEHICLE_STATUS_TRAM_ROUTE = "VEHICLE_STATUS_TRAM_ROUTE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string VEHICLE_STATUS_TRAM_RETURN = "VEHICLE_STATUS_TRAM_RETURN";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TRAM = "INFO_PUBLICTRANSPORT_TRAM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string INFOVIEWS = "INFOVIEWS";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string AIINFO_HEATING_PRODUCTION = "AIINFO_HEATING_PRODUCTION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string AIINFO_HEATING_PRODUCTION_RANGE = "AIINFO_HEATING_PRODUCTION_RANGE";

	[Locale(file = "en-main", category = "Tools", hideInEditor = true)]
	public const string TOOL_ERROR = "TOOL_ERROR";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_ENABLE_WEATHER = "OPTIONS_ENABLE_WEATHER";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CANAL_OPTION_UPGRADE = "CANAL_OPTION_UPGRADE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string FLOODWALL_OPTION_CURVED = "FLOODWALL_OPTION_CURVED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string FLOODWALL_OPTION_STRAIGHT = "FLOODWALL_OPTION_STRAIGHT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string FLOODWALL_OPTION_FREEFORM = "FLOODWALL_OPTION_FREEFORM";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string QUAY_OPTION_CURVED = "QUAY_OPTION_CURVED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string QUAY_OPTION_STRAIGHT = "QUAY_OPTION_STRAIGHT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string QUAY_OPTION_FREEFORM = "QUAY_OPTION_FREEFORM";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string PIPE_OPTION_UPGRADE = "PIPE_OPTION_UPGRADE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string PIPE_OPTION_STRAIGHT = "PIPE_OPTION_STRAIGHT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_UNITGROUP = "OPTIONS_UNITGROUP";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_TEMP_F = "OPTIONS_TEMP_F";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_TEMP_C = "OPTIONS_TEMP_C";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_TEMPERATUREUNIT = "OPTIONS_TEMPERATUREUNIT";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string MAIN_TEMPERATURE = "MAIN_TEMPERATURE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CHIRPER_SELECTIMAGE = "CHIRPER_SELECTIMAGE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string STYLES_EUROPEAN_NORMAL = "STYLES_EUROPEAN_NORMAL";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string STYLES_NORMAL = "STYLES_NORMAL";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_TITLE = "DLCPANEL_TITLE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_OWNED = "DLCPANEL_OWNED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_AVAILABLE = "DLCPANEL_AVAILABLE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_DELUXEEDITION = "DLCPANEL_DELUXEEDITION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_DELUXEEDITIONUP = "DLCPANEL_DELUXEEDITIONUP";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_AFTERDARK = "DLCPANEL_AFTERDARK";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string DLCPANEL_WINTERWONDERLAND = "DLCPANEL_WINTERWONDERLAND";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_TITLE = "INFO_HEATING_TITLE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_AVAILABILITY = "INFO_HEATING_AVAILABILITY";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_CONSUMPTION = "INFO_HEATING_CONSUMPTION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_PRODUCTION = "INFO_HEATING_PRODUCTION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_HEATED = "INFO_HEATING_HEATED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_UNHEATED = "INFO_HEATING_UNHEATED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_HEATING_FACILITIES = "INFO_HEATING_FACILITIES";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_MAINTENANCE_TITLE = "INFO_MAINTENANCE_TITLE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_MAINTENANCE_COVERAGE = "INFO_MAINTENANCE_COVERAGE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_MAINTENANCE_CONDITION = "INFO_MAINTENANCE_CONDITION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_MAINTENANCE_BUILDINGS = "INFO_MAINTENANCE_BUILDINGS";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_SNOW_TITLE = "INFO_SNOW_TITLE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_SNOW_COVERAGE = "INFO_SNOW_COVERAGE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_SNOW_CONDITION = "INFO_SNOW_CONDITION";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_SNOW_BUILDINGS = "INFO_SNOW_BUILDINGS";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_CONDITION_GOOD = "INFO_CONDITION_GOOD";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_CONDITION_POOR = "INFO_CONDITION_POOR";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string HOUSE_INFO_PANEL_HEATING1 = "HOUSE_INFO_PANEL_HEATING1";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string HOUSE_INFO_PANEL_HEATING2 = "HOUSE_INFO_PANEL_HEATING2";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string WORKSHOP_MAPTHEMES = "WORKSHOP_MAPTHEMES";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CONTENTMANAGER_MAPTHEMES = "CONTENTMANAGER_MAPTHEMES";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CONTENT_SF_REQUIRED = "CONTENT_SF_REQUIRED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string CONTENT_SF_BUY = "CONTENT_SF_BUY";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = true)]
	public const string CONFIRM_MAPTHEME_MISSING = "CONFIRM_MAPTHEME_MISSING";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string LOADMAP_THEMEOVERRIDE = "LOADMAP_THEMEOVERRIDE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string LOADMAP_THEMEOVERRIDE_TOOLTIP = "LOADMAP_THEMEOVERRIDE_TOOLTIP";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = true)]
	public const string NEWMAP_UNPUBLISHED_THEME = "NEWMAP_UNPUBLISHED_THEME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = true)]
	public const string SHAREMAP_UNPUBLISHED_THEME = "SHAREMAP_UNPUBLISHED_THEME";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string THEME_NAME = "THEME_NAME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string LINE_DELETE = "LINE_DELETE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string UNLOCK_PIPES = "UNLOCK_PIPES";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string UNLOCK_CANALS = "UNLOCK_CANALS";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string ASSETEDITOR_TOGGLE_SNOW = "ASSETEDITOR_TOGGLE_SNOW";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_CONDITION_NORMAL = "INFO_CONDITION_NORMAL";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string INFO_CONDITION_BOOSTED = "INFO_CONDITION_BOOSTED";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string MAPTHEME_NONE = "MAPTHEME_NONE";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string NEWGAME_BASETHEME = "NEWGAME_BASETHEME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string LOADGAME_BASETHEME = "LOADGAME_BASETHEME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string LOADMAP_BASETHEME = "LOADMAP_BASETHEME";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string MAPTHEME_CUSTOM = "MAPTHEME_CUSTOM";

	[Locale(file = "en-expansion2", category = "Menu", hideInEditor = false)]
	public const string OPTIONS_KM_THEMEEDITOR = "OPTIONS_KM_THEMEEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string KEYMAPPING = "KEYMAPPING";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_SNOWTRUCK_CAPACITY = "AIINFO_SNOWTRUCK_CAPACITY";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_SNOW_TRUCKS = "AIINFO_SNOW_TRUCKS";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_SNOW_COLLECT = "VEHICLE_STATUS_SNOW_COLLECT";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_SNOW_WAIT = "VEHICLE_STATUS_SNOW_WAIT";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_SNOW_TRANSFER = "VEHICLE_STATUS_SNOW_TRANSFER";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_SNOW_UNLOAD = "VEHICLE_STATUS_SNOW_UNLOAD";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_SNOW_RETURN = "VEHICLE_STATUS_SNOW_RETURN";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_EXPANSION2_TITLE = "LOADING_TIP_EXPANSION2_TITLE";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_EXPANSION2_TEXT = "LOADING_TIP_EXPANSION2_TEXT";

	[Locale(file = "en-tips", category = "Sheet1", hideInEditor = true)]
	public const string LOADING_TIP_TITLE = "LOADING_TIP_TITLE";

	[Locale(file = "en-tips", category = "Sheet1", hideInEditor = true)]
	public const string LOADING_TIP_TEXT = "LOADING_TIP_TEXT";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_WINTER_TITLE = "LOADING_TIP_WINTER_TITLE";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_WINTER_TEXT = "LOADING_TIP_WINTER_TEXT";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_NONWINTER_TITLE = "LOADING_TIP_NONWINTER_TITLE";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = true)]
	public const string LOADING_TIP_NONWINTER_TEXT = "LOADING_TIP_NONWINTER_TEXT";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = true)]
	public const string MAPNAME = "MAPNAME";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_MAINTENANCE_MAINTAIN = "VEHICLE_STATUS_MAINTENANCE_MAINTAIN";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_MAINTENANCE_WAIT = "VEHICLE_STATUS_MAINTENANCE_WAIT";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string VEHICLE_STATUS_MAINTENANCE_RETURN = "VEHICLE_STATUS_MAINTENANCE_RETURN";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_MAINTENANCETRUCK_CAPACITY = "AIINFO_MAINTENANCETRUCK_CAPACITY";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_MAINTENANCE_TRUCKS = "AIINFO_MAINTENANCE_TRUCKS";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_VISITOR_CAPACITY = "AIINFO_VISITOR_CAPACITY";

	[Locale(file = "en-expansion2", category = "Policies_and_misc", hideInEditor = false)]
	public const string AIINFO_SUN_INTENSITY = "AIINFO_SUN_INTENSITY";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string ANIMAL_TITLE = "ANIMAL_TITLE";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_MANUAL_NUM_PROG = "MILESTONE_MANUAL_NUM_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_MANUAL_NUM_DESC = "MILESTONE_MANUAL_NUM_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_COMBINED_CUSTOM_PROG = "MILESTONE_COMBINED_CUSTOM_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_COMBINED_CUSTOM_DESC = "MILESTONE_COMBINED_CUSTOM_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_MANUAL_PROG = "MILESTONE_MANUAL_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_MANUAL_DESC = "MILESTONE_MANUAL_DESC";

	[Locale(file = "en-main", category = "Tutorial", hideInEditor = true)]
	public const string TUTORIAL_TITLE = "TUTORIAL_TITLE";

	[Locale(file = "en-main", category = "Tutorial", hideInEditor = true)]
	public const string TUTORIAL_TEXT = "TUTORIAL_TEXT";

	[Locale(file = "en-main", category = "Tutorial", hideInEditor = true)]
	public const string TUTORIAL_ADVISER_TITLE = "TUTORIAL_ADVISER_TITLE";

	[Locale(file = "en-main", category = "Tutorial", hideInEditor = true)]
	public const string TUTORIAL_ADVISER = "TUTORIAL_ADVISER";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = true)]
	public const string NOTIFICATION_MAJOR = "NOTIFICATION_MAJOR";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = true)]
	public const string NOTIFICATION_FATAL = "NOTIFICATION_FATAL";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_TITLE = "PUBLICTRANSPORT_TITLE";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_BUSLINES = "PUBLICTRANSPORT_BUSLINES";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_TRAMLINES = "PUBLICTRANSPORT_TRAMLINES";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_METROLINES = "PUBLICTRANSPORT_METROLINES";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_TRAINLINES = "PUBLICTRANSPORT_TRAINLINES";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_LINENAME = "PUBLICTRANSPORT_LINENAME";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_LINECOLOR = "PUBLICTRANSPORT_LINECOLOR";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_LINESTOPS = "PUBLICTRANSPORT_LINESTOPS";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_VEHICLES = "PUBLICTRANSPORT_VEHICLES";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_PASSENGERS = "PUBLICTRANSPORT_PASSENGERS";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_SHOWALL = "PUBLICTRANSPORT_SHOWALL";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_HIDEALL = "PUBLICTRANSPORT_HIDEALL";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_SHOWLINE = "PUBLICTRANSPORT_SHOWLINE";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = false)]
	public const string PUBLICTRANSPORT_HIDELINE = "PUBLICTRANSPORT_HIDELINE";

	[Locale(file = "en-expansion2", category = "PublicTransportPanel", hideInEditor = true)]
	public const string CONFIRM_LINEDELETE = "CONFIRM_LINEDELETE";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string THEMEEDITOR_TOOL = "THEMEEDITOR_TOOL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = true)]
	public const string THEMESETTINGS_TITLE = "THEMESETTINGS_TITLE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = true)]
	public const string THEMESETTINGS_DESC = "THEMESETTINGS_DESC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string MENUITEM = "MENUITEM";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string TOOLSMENU_THEMEEDITOR = "TOOLSMENU_THEMEEDITOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_DIFFUSE = "THEME_TERRAIN_DIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GRASS = "THEME_TERRAIN_GRASS";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_RUINED = "THEME_TERRAIN_RUINED";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_PAVEMENT = "THEME_TERRAIN_PAVEMENT";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GRAVEL = "THEME_TERRAIN_GRAVEL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OIL = "THEME_TERRAIN_OIL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_ORE = "THEME_TERRAIN_ORE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_SAND = "THEME_TERRAIN_SAND";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_CLIFF = "THEME_TERRAIN_CLIFF";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_CLIFFSAND = "THEME_TERRAIN_CLIFFSAND";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_NORMAL = "THEME_TERRAIN_NORMAL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_DIFFUSETILING = "THEME_TERRAIN_DIFFUSETILING";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_NORMALTILING = "THEME_TERRAIN_NORMALTILING";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEMEEDITOR_SELECTTEXTURE = "THEMEEDITOR_SELECTTEXTURE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string TEXTUREPICKER_LOADERROR = "TEXTUREPICKER_LOADERROR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WATER_NORMAL = "THEME_WATER_NORMAL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WATER_FOAM = "THEME_WATER_FOAM";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WATER_CLEANCOLOR = "THEME_WATER_CLEANCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WATER_DIRTYCOLOR = "THEME_WATER_DIRTYCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WATER_UNDERCOLOR = "THEME_WATER_UNDERCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_LONGITUDE = "THEME_ATMOSPHERE_LONGITUDE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_LATITUDE = "THEME_ATMOSPHERE_LATITUDE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_RAYLEIGHT = "THEME_ATMOSPHERE_RAYLEIGHT";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_MIE = "THEME_ATMOSPHERE_MIE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_EXPOSURE = "THEME_ATMOSPHERE_EXPOSURE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_SUN = "THEME_ATMOSPHERE_SUN";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_SKY = "THEME_ATMOSPHERE_SKY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_SIZE = "THEME_ATMOSPHERE_SIZE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_ANISOTROPY = "THEME_ATMOSPHERE_ANISOTROPY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_MOON = "THEME_ATMOSPHERE_MOON";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_TEXTURE = "THEME_ATMOSPHERE_TEXTURE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_INNERCORONA = "THEME_ATMOSPHERE_INNERCORONA";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_OUTERCORONA = "THEME_ATMOSPHERE_OUTERCORONA";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_NIGHTHORIZONCOLOR = "THEME_ATMOSPHERE_NIGHTHORIZONCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_EARLYNIGHTZENITHCOLOR = "THEME_ATMOSPHERE_EARLYNIGHTZENITHCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_LATENIGHTZENITHCOLOR = "THEME_ATMOSPHERE_LATENIGHTZENITHCOLOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_SKYTINT = "THEME_ATMOSPHERE_SKYTINT";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_STARSINTENSITY = "THEME_ATMOSPHERE_STARSINTENSITY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_ATMOSPHERE_OUTERSPACEINTENSITY = "THEME_ATMOSPHERE_OUTERSPACEINTENSITY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_UPWARDROADDIFFUSE = "THEME_STRUCTURES_UPWARDROADDIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_DOWNWARDROADDIFFUSE = "THEME_STRUCTURES_DOWNWARDROADDIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_BUILDINGBASEDIFFUSE = "THEME_STRUCTURES_BUILDINGBASEDIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_BUILDINGBASENORMAL = "THEME_STRUCTURES_BUILDINGBASENORMAL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_BUILDINGFLOOR = "THEME_STRUCTURES_BUILDINGFLOOR";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_BUILDINGBURNTDIFFUSE = "THEME_STRUCTURES_BUILDINGBURNTDIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_BUILDINGABANDONEDDIFFUSE = "THEME_STRUCTURES_BUILDINGABANDONEDDIFFUSE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_STRUCTURES_LIGHTCOLORPALETTE = "THEME_STRUCTURES_LIGHTCOLORPALETTE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GEOMETRYDETAIL = "THEME_TERRAIN_GEOMETRYDETAIL";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GEOMETRYGRASS = "THEME_TERRAIN_GEOMETRYGRASS";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GEOMETRYROCKS = "THEME_TERRAIN_GEOMETRYROCKS";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_GEOMETRYFERTILE = "THEME_TERRAIN_GEOMETRYFERTILE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_TEMPERATURE = "THEME_WORLD_TEMPERATURE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_MIN = "THEME_WORLD_MIN";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_MAX = "THEME_WORLD_MAX";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_DAY = "THEME_WORLD_DAY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_NIGHT = "THEME_WORLD_NIGHT";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_RAIN = "THEME_WORLD_RAIN";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_FOG = "THEME_WORLD_FOG";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_WEATHER = "THEME_WORLD_WEATHER";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_DAYRAINPROB = "THEME_WORLD_DAYRAINPROB";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_NIGHTRAINPROB = "THEME_WORLD_NIGHTRAINPROB";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_DAYFOGPROB = "THEME_WORLD_DAYFOGPROB";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_NIGHTFOGPROB = "THEME_WORLD_NIGHTFOGPROB";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_WORLD_NORTHERNLIGHTSPROB = "THEME_WORLD_NORTHERNLIGHTSPROB";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = true)]
	public const string DEFAULTSAVENAMES = "DEFAULTSAVENAMES";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_BUILTIN = "THEME_BUILTIN";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_USER = "THEME_USER";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string NEWTHEME_TITLE = "NEWTHEME_TITLE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string LOADTHEME_TITLE = "LOADTHEME_TITLE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string LOADTHEME_NAME = "LOADTHEME_NAME";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string LOADTHEME_THEME = "LOADTHEME_THEME";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string SAVETHEME_TITLE = "SAVETHEME_TITLE";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string PUBLISH_THEME = "PUBLISH_THEME";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OFFSET_POLLUTION = "THEME_TERRAIN_OFFSET_POLLUTION";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OFFSET_FIELD = "THEME_TERRAIN_OFFSET_FIELD";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OFFSET_FERTILITY = "THEME_TERRAIN_OFFSET_FERTILITY";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OFFSET_FOREST = "THEME_TERRAIN_OFFSET_FOREST";

	[Locale(file = "en-expansion2", category = "ThemeEditor", hideInEditor = false)]
	public const string THEME_TERRAIN_OFFSET_LABEL = "THEME_TERRAIN_OFFSET_LABEL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string TOOL_WATER_ESTIMATE = "TOOL_WATER_ESTIMATE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string TOOL_SHORELINE_RECOMMENDED = "TOOL_SHORELINE_RECOMMENDED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string BUILDING_STATUS_COLLAPSED = "BUILDING_STATUS_COLLAPSED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NOTIFICATION_COLLAPSED = "NOTIFICATION_COLLAPSED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NOTIFICATION_VISITED = "NOTIFICATION_VISITED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string TOOLSMENU_SCENARIOEDITOR = "TOOLSMENU_SCENARIOEDITOR";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWSCENARIO_TITLE = "NEWSCENARIO_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SCENARIONAME_CITY = "SCENARIONAME_CITY";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string SCENARIOEDITOR_TOOL = "SCENARIOEDITOR_TOOL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string TRIGGERPANEL_TITLE = "TRIGGERPANEL_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SCENARIOSETTINGSPANEL_TITLE = "SCENARIOSETTINGSPANEL_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERLIST_TITLE = "DISASTERLIST_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERLIST_STARTINGAFTER = "DISASTERLIST_STARTINGAFTER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERLIST_DISASTERTYPE = "DISASTERLIST_DISASTERTYPE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERLIST_WEEKS = "DISASTERLIST_WEEKS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_TITLE = "GOALSPANEL_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_WINCONDITIONS = "GOALSPANEL_WINCONDITIONS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_LOSINGCONDITIONS = "GOALSPANEL_LOSINGCONDITIONS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_AMOUNT = "GOALSPANEL_AMOUNT";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_ADD = "GOALSPANEL_ADD";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_CHOOSESCENARIOTAB = "NEWGAME_CHOOSESCENARIOTAB";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_CHOOSEMAPTAB = "NEWGAME_CHOOSEMAPTAB";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_RESOURCES = "NEWGAME_RESOURCES";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_GOALS = "NEWGAME_GOALS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_DISASTERS = "NEWGAME_DISASTERS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_DESCRIPTION = "NEWGAME_DESCRIPTION";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWGAME_NOSCENARIOS = "NEWGAME_NOSCENARIOS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SAVESCENARIO_TITLE = "SAVESCENARIO_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SAVESCENARIO_PUBLISH = "SAVESCENARIO_PUBLISH";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SAVESCENARIO_DESCRIPTION = "SAVESCENARIO_DESCRIPTION";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SAVESCENARIO_NAME = "SAVESCENARIO_NAME";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SAVESCENARIO_NEEDS_MORE_CONDITIONS = "SAVESCENARIO_NEEDS_MORE_CONDITIONS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string CONFIRM_NEWSCENARIO = "CONFIRM_NEWSCENARIO";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string DISASTERWARNING = "DISASTERWARNING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERCLOCK = "DISASTERCLOCK";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERSEVERITY = "DISASTERSEVERITY";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SCENEDITOR_TOOLTIP = "SCENEDITOR_TOOLTIP";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = true)]
	public const string DISASTERACTIVE = "DISASTERACTIVE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EVACUATE_UI4 = "EVACUATE_UI4";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EVACUATE_UI5 = "EVACUATE_UI5";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RADIO_UI1 = "RADIO_UI1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RADIO_UI2 = "RADIO_UI2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RADIO_UI3 = "RADIO_UI3";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RADIO_UI4 = "RADIO_UI4";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SHELTEREVACUATEUI1 = "SHELTEREVACUATEUI1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SHELTEREVACUATEUI2 = "SHELTEREVACUATEUI2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SHELTEREVACUATEUI3 = "SHELTEREVACUATEUI3";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI0 = "DISASTER_UI0";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI1 = "DISASTER_UI1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI2 = "DISASTER_UI2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI3 = "DISASTER_UI3";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI4 = "DISASTER_UI4";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTER_UI5 = "DISASTER_UI5";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOWINDOW1 = "DISASTERINFOWINDOW1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOWINDOW2 = "DISASTERINFOWINDOW2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOWINDOW3 = "DISASTERINFOWINDOW3";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOWINDOWTITLE = "DISASTERINFOWINDOWTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFOWINDOWLOST = "INFOWINDOWLOST";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFOWINDOWREBUILD = "INFOWINDOWREBUILD";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WATERINFOPANEL_TANK = "WATERINFOPANEL_TANK";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = true)]
	public const string SERVICEPERSON_TITLE = "SERVICEPERSON_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RESCUE_ANIMAL_SEARCHING = "RESCUE_ANIMAL_SEARCHING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string RESCUE_WORKER_SEARCHING = "RESCUE_WORKER_SEARCHING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string METEORINFOVIEWPANEL_SPEED = "METEORINFOVIEWPANEL_SPEED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CONTENTMANAGER_SCENARIOS = "CONTENTMANAGER_SCENARIOS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WORKSHOP_SCENARIOS = "WORKSHOP_SCENARIOS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_TRIANGLE = "ASSET_WARNING_TRIANGLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_LOD_TRIANGLE = "ASSET_WARNING_LOD_TRIANGLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_TEXTURE = "ASSET_WARNING_TEXTURE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_LOD_TEXTURE = "ASSET_WARNING_LOD_TEXTURE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_TOOLTIP = "ASSET_WARNING_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_WARNING_SAVING = "ASSET_WARNING_SAVING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_RANDOM_DISASTERS_ENABLED = "OPTIONS_RANDOM_DISASTERS_ENABLED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_RANDOM_DISASTERS_PROBABILITY = "OPTIONS_RANDOM_DISASTERS_PROBABILITY";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTMONEYGAINEDTITLE = "DEFAULTMONEYGAINEDTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTMONEYGAINEDMESSAGE = "DEFAULTMONEYGAINEDMESSAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTMONEYLOSTTITLE = "DEFAULTMONEYLOSTTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTMONEYLOSTMESSAGE = "DEFAULTMONEYLOSTMESSAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTGOALMETTITLE = "DEFAULTGOALMETTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTGOALMETMESSAGE = "DEFAULTGOALMETMESSAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTSCENARIOWONTITLE = "DEFAULTSCENARIOWONTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTSCENARIOWONMESSAGE = "DEFAULTSCENARIOWONMESSAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTSCENARIOLOSTTITLE = "DEFAULTSCENARIOLOSTTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DEFAULTSCENARIOLOSTMESSAGE = "DEFAULTSCENARIOLOSTMESSAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERPANEL_WARNINGNOTRIGGERS = "DISASTERPANEL_WARNINGNOTRIGGERS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string WARNINGPANEL_INCOMINGDISASTER = "WARNINGPANEL_INCOMINGDISASTER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string WARNINGPANEL_INCOMINGDISASTER_CUSTOMNAME = "WARNINGPANEL_INCOMINGDISASTER_CUSTOMNAME";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_EVACUATEALL = "WARNINGPANEL_EVACUATEALL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_EVACUATEALL_TOOLTIP = "WARNINGPANEL_EVACUATEALL_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_EVACUATING = "WARNINGPANEL_EVACUATING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_RELEASEALL = "WARNINGPANEL_RELEASEALL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_RELEASEALL_TOOLTIP = "WARNINGPANEL_RELEASEALL_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string MAIN_TOOL_ND = "MAIN_TOOL_ND";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string BUDGET_INFO_ND = "BUDGET_INFO_ND";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EXPENSES_TT_FIRE_ND = "EXPENSES_TT_FIRE_ND";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string CONFIRM_TRIGGERWARNING = "CONFIRM_TRIGGERWARNING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string KEYMAPPING_CATEGORY = "KEYMAPPING_CATEGORY";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_KM_SCENARIOEDITOR = "OPTIONS_KM_SCENARIOEDITOR";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string NOTIFICATION_TITLE_PLAYER = "NOTIFICATION_TITLE_PLAYER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string NOTIFICATION_NORMAL_PLAYER = "NOTIFICATION_NORMAL_PLAYER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string NOTIFICATION_MAJOR_PLAYER = "NOTIFICATION_MAJOR_PLAYER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string NOTIFICATION_FATAL_PLAYER = "NOTIFICATION_FATAL_PLAYER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_THEN = "GOALSPANEL_THEN";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_TO_WIN = "GOALSPANEL_TO_WIN";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_TO_LOSE = "GOALSPANEL_TO_LOSE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_GOODGOALS = "GOALSPANEL_GOODGOALS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_BADGOALS = "GOALSPANEL_BADGOALS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INGAME_LOSINGCONDITIONS_TITLE = "INGAME_LOSINGCONDITIONS_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INGAME_WINCONDITIONS_TITLE = "INGAME_WINCONDITIONS_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ICONFORSHELTEREVACUTING = "ICONFORSHELTEREVACUTING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERSMENUADVISORTITLE = "DISASTERSMENUADVISORTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERSMENUADVISORTEXT = "DISASTERSMENUADVISORTEXT";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string REBUILDBUTTON = "REBUILDBUTTON";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string REBUILDBUTTONTEXT = "REBUILDBUTTONTEXT";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EMERGENCYSERVICES_ADVISORTITLE = "EMERGENCYSERVICES_ADVISORTITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EMERGENCYSERVICES_ADVISORTEXT = "EMERGENCYSERVICES_ADVISORTEXT";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_NATURALDISASTERS = "DLCPANEL_NATURALDISASTERS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT = "PANEL_SORT";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_NAME = "PANEL_SORT_NAME";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_MODIFIED = "PANEL_SORT_MODIFIED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string TOOL_REPAIR_COST = "TOOL_REPAIR_COST";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string STORYMESSAGE_TITLE = "STORYMESSAGE_TITLE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOVIEWLEGEND1 = "DISASTERINFOVIEWLEGEND1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOVIEWLEGEND2 = "DISASTERINFOVIEWLEGEND2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERINFOVIEWLEGEND3 = "DISASTERINFOVIEWLEGEND3";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string BUILDESCAPEROUTE = "BUILDESCAPEROUTE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string EXITROUTE = "EXITROUTE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SHELTERINFOVIEWLEGEND1 = "SHELTERINFOVIEWLEGEND1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string SHELTERINFOVIEWLEGEND2 = "SHELTERINFOVIEWLEGEND2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_AFTERMATH = "DISASTERREPORT_AFTERMATH";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_UNIT_METERS = "DISASTERREPORT_UNIT_METERS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_NODETECTIONSERVICE = "WARNINGPANEL_NODETECTIONSERVICE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_INCOMING = "WARNINGPANEL_INCOMING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = true)]
	public const string NOTIFICATION_FATAL_ND = "NOTIFICATION_FATAL_ND";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWSOUNDSETTING1 = "NEWSOUNDSETTING1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWSOUNDSETTING2 = "NEWSOUNDSETTING2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPANEL_DISASTEROVER = "WARNINGPANEL_DISASTEROVER";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT1 = "CITYSTAT1";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT2 = "CITYSTAT2";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT4 = "CITYSTAT4";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT5 = "CITYSTAT5";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_TRACKSDESTROYED = "DISASTERREPORT_TRACKSDESTROYED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_POWERLINESDESTROYED = "DISASTERREPORT_POWERLINESDESTROYED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_BUILDINGFIRES = "DISASTERREPORT_BUILDINGFIRES";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_FORESTFIRES = "DISASTERREPORT_FORESTFIRES";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_BUILDINGSUPGRADED = "DISASTERREPORT_BUILDINGSUPGRADED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_NOCASUALTIES = "DISASTERREPORT_NOCASUALTIES";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_NODAMAGE = "DISASTERREPORT_NODAMAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERREPORT_DISMISS = "DISASTERREPORT_DISMISS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT1_TOOLTIP = "CITYSTAT1_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT2_TOOLTIP = "CITYSTAT2_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT4_TOOLTIP = "CITYSTAT4_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CITYSTAT5_TOOLTIP = "CITYSTAT5_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DISASTERAFTERMATHPANEL = "DISASTERAFTERMATHPANEL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string BUILDING_STATUS_EVACUATED = "BUILDING_STATUS_EVACUATED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_HIGHTECH = "DLCPANEL_HIGHTECH";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_RELAXATIONSTATION = "DLCPANEL_RELAXATIONSTATION";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WHYLOST = "WHYLOST";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string ASSET_EDITOR_SUB_BUILDING = "ASSET_EDITOR_SUB_BUILDING";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_ESCAPE_SHELTERS = "INFO_ESCAPE_SHELTERS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_RADIO_SIGNAL = "INFO_RADIO_SIGNAL";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_RADIO_BUILDINGS = "INFO_RADIO_BUILDINGS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_DETECTION_COVERAGE = "INFO_DETECTION_COVERAGE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_DETECTION_BUILDINGS = "INFO_DETECTION_BUILDINGS";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string INFO_DISASTERRISK_DISASTERRISK = "INFO_DISASTERRISK_DISASTERRISK";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string WARNINGPHASEPANEL_DISASTERREPORTSBUTTON_TOOLTIP = "WARNINGPHASEPANEL_DISASTERREPORTSBUTTON_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string NEWSCENARIO_UPDATEMAPORSAVE = "NEWSCENARIO_UPDATEMAPORSAVE";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CONTENT_ND_REQUIRED = "CONTENT_ND_REQUIRED";

	[Locale(file = "en-expansion3", category = "Misc", hideInEditor = false)]
	public const string CONTENT_ND_BUY = "CONTENT_ND_BUY";

	[Locale(file = "en-expansion3", category = "NDPatch", hideInEditor = false)]
	public const string THEME_WORLD_DISASTER = "THEME_WORLD_DISASTER";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_HELICOPTERS = "AIINFO_HELICOPTERS";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_HELICOPTER_CAPACITY = "AIINFO_HELICOPTER_CAPACITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_SHELTER_CITIZENS = "AIINFO_SHELTER_CITIZENS";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_SHELTER_CAPACITY = "AIINFO_SHELTER_CAPACITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_SHELTER_VEHICLE_COUNT = "AIINFO_SHELTER_VEHICLE_COUNT";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_RADIO_TRANSMITTER_POWER = "AIINFO_RADIO_TRANSMITTER_POWER";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_RADIO_MAST_HEIGHT = "AIINFO_RADIO_MAST_HEIGHT";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_FIREWATCH_VISIBILITY_RADIUS = "AIINFO_FIREWATCH_VISIBILITY_RADIUS";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_FIREWATCH_TOWER_HEIGHT = "AIINFO_FIREWATCH_TOWER_HEIGHT";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_VEHICLES = "AIINFO_VEHICLES";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_VEHICLE_CAPACITY = "AIINFO_VEHICLE_CAPACITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AINFO_WATER_STORAGE_CAPACITY = "AINFO_WATER_STORAGE_CAPACITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AINFO_WATER_IO_CAPACITY = "AINFO_WATER_IO_CAPACITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AINFO_WATER_STORAGE_STATUS = "AINFO_WATER_STORAGE_STATUS";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_SPACE_RADAR_RANGE = "AIINFO_SPACE_RADAR_RANGE";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_WEATHER_RADAR_RANGE = "AIINFO_WEATHER_RADAR_RANGE";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_TSUNAMI_BUOY_SENSITIVITY = "AIINFO_TSUNAMI_BUOY_SENSITIVITY";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_TSUNAMI_BUOY_DETECTION_OFF = "AIINFO_TSUNAMI_BUOY_DETECTION_OFF";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string AIINFO_EARTHQUAKE_SENSOR_RANGE = "AIINFO_EARTHQUAKE_SENSOR_RANGE";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_WATER = "SHELTER_WATER";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_FOOD = "SHELTER_FOOD";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_POWER = "SHELTER_POWER";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_DRAWROUTE_TOOLTIP = "SHELTER_DRAWROUTE_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_DELETEROUTE_TOOLTIP = "SHELTER_DELETEROUTE_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_EVACUATE = "SHELTER_EVACUATE";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_EVACUATE_TOOLTIP = "SHELTER_EVACUATE_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_RELEASE = "SHELTER_RELEASE";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string SHELTER_RELEASE_TOOLTIP = "SHELTER_RELEASE_TOOLTIP";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string DISASTERMENUNAME = "DISASTERMENUNAME";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string DISASTERMENUNAMETAB1 = "DISASTERMENUNAMETAB1";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string DISASTERMENUNAMETAB2 = "DISASTERMENUNAMETAB2";

	[Locale(file = "en-expansion3", category = "Buildings", hideInEditor = false)]
	public const string CITYSERVICE_TOOLTIP_DISASTERSERVICEREQUIRED = "CITYSERVICE_TOOLTIP_DISASTERSERVICEREQUIRED";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_COPTER_EMERGENCY = "VEHICLE_STATUS_AMBULANCE_COPTER_EMERGENCY";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_COPTER_WAIT = "VEHICLE_STATUS_AMBULANCE_COPTER_WAIT";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_COPTER_WAIT2 = "VEHICLE_STATUS_AMBULANCE_COPTER_WAIT2";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_COPTER_RETURN = "VEHICLE_STATUS_AMBULANCE_COPTER_RETURN";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_COPTER_CARRYING = "VEHICLE_STATUS_AMBULANCE_COPTER_CARRYING";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_COPTER_PATROL = "VEHICLE_STATUS_POLICE_COPTER_PATROL";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_COPTER_PATROL_WAIT = "VEHICLE_STATUS_POLICE_COPTER_PATROL_WAIT";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_COPTER_EMERGENCY = "VEHICLE_STATUS_POLICE_COPTER_EMERGENCY";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_COPTER_RETURN = "VEHICLE_STATUS_POLICE_COPTER_RETURN";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_EMERGENCY = "VEHICLE_STATUS_FIRE_COPTER_EMERGENCY";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_EMERGENCY2 = "VEHICLE_STATUS_FIRE_COPTER_EMERGENCY2";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_EXTINGUISH = "VEHICLE_STATUS_FIRE_COPTER_EXTINGUISH";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_WAIT = "VEHICLE_STATUS_FIRE_COPTER_WAIT";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_FILLING = "VEHICLE_STATUS_FIRE_COPTER_FILLING";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRE_COPTER_RETURN = "VEHICLE_STATUS_FIRE_COPTER_RETURN";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_DISASTER_RESPONSE_RETURN = "VEHICLE_STATUS_DISASTER_RESPONSE_RETURN";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_DISASTER_RESPONSE_WAIT = "VEHICLE_STATUS_DISASTER_RESPONSE_WAIT";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_DISASTER_RESPONSE_SEARCHING = "VEHICLE_STATUS_DISASTER_RESPONSE_SEARCHING";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_DISASTER_RESPONSE_RESPONDING = "VEHICLE_STATUS_DISASTER_RESPONSE_RESPONDING";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_WATER_SEARCH = "VEHICLE_STATUS_WATER_SEARCH";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_WATER_PUMP = "VEHICLE_STATUS_WATER_PUMP";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_WATER_WAIT = "VEHICLE_STATUS_WATER_WAIT";

	[Locale(file = "en-expansion3", category = "Vehicles", hideInEditor = false)]
	public const string VEHICLE_STATUS_WATER_RETURN = "VEHICLE_STATUS_WATER_RETURN";

	[Locale(file = "en-expansion3", category = "Disasters", hideInEditor = true)]
	public const string DISASTER_TITLE = "DISASTER_TITLE";

	[Locale(file = "en-expansion3", category = "Disasters", hideInEditor = true)]
	public const string DISASTER_DESC = "DISASTER_DESC";

	[Locale(file = "en-expansion3", category = "Disasters", hideInEditor = true)]
	public const string METEOR_TITLE = "METEOR_TITLE";

	[Locale(file = "en-expansion3", category = "NDPatch", hideInEditor = false)]
	public const string DISASTERNAME_CHIRPYNADO = "DISASTERNAME_CHIRPYNADO";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SHELTER = "CHIRP_FIRST_SHELTER";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_POLICEHELI = "CHIRP_FIRST_POLICEHELI";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FIREHELI = "CHIRP_FIRST_FIREHELI";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_MEDICALHELI = "CHIRP_FIRST_MEDICALHELI";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FIREWATCH = "CHIRP_FIRST_FIREWATCH";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_PUMPING = "CHIRP_FIRST_PUMPING";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_DISASTERRESPONSE = "CHIRP_FIRST_DISASTERRESPONSE";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TSUNAMIBOYO = "CHIRP_FIRST_TSUNAMIBOYO";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_WEATHERSTATION = "CHIRP_FIRST_WEATHERSTATION";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_EARTHQUAKESENSOR = "CHIRP_FIRST_EARTHQUAKESENSOR";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_DISASTERMEMORIAL = "CHIRP_FIRST_DISASTERMEMORIAL";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_DOOMSDAYVAULT = "CHIRP_FIRST_DOOMSDAYVAULT";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_METEORITEPARK = "CHIRP_FIRST_METEORITEPARK";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_UB1 = "CHIRP_FIRST_UB1";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_UB2 = "CHIRP_FIRST_UB2";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_UB3 = "CHIRP_FIRST_UB3";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_WATERTANK = "CHIRP_FIRST_WATERTANK";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_DISASTER = "CHIRP_DISASTER";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RANDOM_DISASTERS = "CHIRP_RANDOM_DISASTERS";

	[Locale(file = "en-expansion3", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_SURVIVORFOUND = "CHIRP_SURVIVORFOUND";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string MODNOTIFICATION = "MODNOTIFICATION";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string MODWARNING = "MODWARNING";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string STREAMINGWARNING = "STREAMINGWARNING";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = false)]
	public const string TOOLTIPFORRANDOMDISASTERS = "TOOLTIPFORRANDOMDISASTERS";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string TEXTFORRANDOMDISASTERS = "TEXTFORRANDOMDISASTERS";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = false)]
	public const string BUTTON1 = "BUTTON1";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = false)]
	public const string BUTTON2 = "BUTTON2";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string DISASTERINFOVIEWADVISOR = "DISASTERINFOVIEWADVISOR";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string SHELTERTUTORIAL = "SHELTERTUTORIAL";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string SHELTERTUTORIALBUILD = "SHELTERTUTORIALBUILD";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = true)]
	public const string RELEASESHELTER = "RELEASESHELTER";

	[Locale(file = "en-expansion3", category = "Tutorial", hideInEditor = false)]
	public const string CITYVALUETUTOOLTIP = "CITYVALUETUTOOLTIP";

	[Locale(file = "en-expansion3", category = "NDPatch", hideInEditor = false)]
	public const string INFO_WATER_TANKUSAGE = "INFO_WATER_TANKUSAGE";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TIME_PROG = "MILESTONE_TIME_PROG";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TIME_DESC = "MILESTONE_TIME_DESC";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_GAMEAREAS_PROG = "MILESTONE_GAMEAREAS_PROG";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_GAMEAREAS_DESC = "MILESTONE_GAMEAREAS_DESC";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_WRAPPER_PROG = "MILESTONE_WRAPPER_PROG";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_WRAPPER_DESC = "MILESTONE_WRAPPER_DESC";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TRIGGER_PROG = "MILESTONE_TRIGGER_PROG";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TRIGGER_DESC = "MILESTONE_TRIGGER_DESC";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string UPDATEB1 = "UPDATEB1";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string UPDATEB2 = "UPDATEB2";

	[Locale(file = "en-expansion3", category = "Milestones", hideInEditor = false)]
	public const string UPDATEB3 = "UPDATEB3";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_LATEST_INV_PROG = "MILESTONE_STAT_LATEST_INV_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string MILESTONE_STAT_LATEST_INV_DESC = "MILESTONE_STAT_LATEST_INV_DESC";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string RADIO_CHANNEL_TITLE = "RADIO_CHANNEL_TITLE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_CONDITIONORDER = "TRIGGERPANEL_CONDITIONORDER";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = true)]
	public const string CONDITIONORDER = "CONDITIONORDER";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_CANREPEAT = "TRIGGERPANEL_CANREPEAT";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_HOWMANYTIMES = "TRIGGERPANEL_HOWMANYTIMES";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_COOLDOWN = "TRIGGERPANEL_COOLDOWN";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_CONDITIONS = "TRIGGERPANEL_CONDITIONS";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_EFFECTS = "TRIGGERPANEL_EFFECTS";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_TRIGGERS = "TRIGGERPANEL_TRIGGERS";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_TRIGGER = "TRIGGERPANEL_TRIGGER";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_SELECTED = "TRIGGERPANEL_SELECTED";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_SELECTFROMMAP = "TRIGGERPANEL_SELECTFROMMAP";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_CREATENEW = "TRIGGERPANEL_CREATENEW";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_DEFAULTMESSAGE = "TRIGGERPANEL_DEFAULTMESSAGE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_DEFAULTCHIRP = "TRIGGERPANEL_DEFAULTCHIRP";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_PLAYERWINS = "TRIGGERPANEL_PLAYERWINS";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_PLAYERLOSES = "TRIGGERPANEL_PLAYERLOSES";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_ADDTRIGGER = "TRIGGERPANEL_ADDTRIGGER";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_NONE = "TRIGGERPANEL_NONE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_NODISASTERSAVAILABLE = "TRIGGERPANEL_NODISASTERSAVAILABLE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string CONDITION_TRIGGERTRIGGERED = "CONDITION_TRIGGERTRIGGERED";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string CONDITION_MAPTILES = "CONDITION_MAPTILES";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string CONDITIONITEM_GAMEAREA = "CONDITIONITEM_GAMEAREA";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = true)]
	public const string TRIGGEREFFECT = "TRIGGEREFFECT";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string PANEL_SORT_THEME = "PANEL_SORT_THEME";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string PANEL_SORT_BUILDABLE = "PANEL_SORT_BUILDABLE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string PANEL_SORT_POPULATION = "PANEL_SORT_POPULATION";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string SCENARIOSETTINGS_STARTINGMONEY = "SCENARIOSETTINGS_STARTINGMONEY";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string SCENARIOSETTINGS_BUILTINMODS = "SCENARIOSETTINGS_BUILTINMODS";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string SCENARIOSETTINGS_SNAPSHOTTOOL = "SCENARIOSETTINGS_SNAPSHOTTOOL";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string SCENARIOSETTINGS_TITLE = "SCENARIOSETTINGS_TITLE";

	[Locale(file = "en-expansion3", category = "ScenarioEditor", hideInEditor = false)]
	public const string TRIGGERPANEL_BUILDINGSBUILT = "TRIGGERPANEL_BUILDINGSBUILT";

	[Locale(file = "en-expansion3", category = "NDPatch", hideInEditor = false)]
	public const string WINLOSECONDITIONS_OR = "WINLOSECONDITIONS_OR";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string DISASTER_TRIGGEREDBY = "DISASTER_TRIGGEREDBY";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string SCENARIOEDITOR_SELECTINGDISASTERFOR = "SCENARIOEDITOR_SELECTINGDISASTERFOR";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string SCENARIOEDITOR_ADDINGDISASTERFOR = "SCENARIOEDITOR_ADDINGDISASTERFOR";

	[Locale(file = "en-expansion5", category = "Scenarios", hideInEditor = true)]
	public const string SCENARIO_NAME = "SCENARIO_NAME";

	[Locale(file = "en-expansion5", category = "Scenarios", hideInEditor = true)]
	public const string SCENARIO_DESC = "SCENARIO_DESC";

	[Locale(file = "en-expansion5", category = "Scenarios", hideInEditor = true)]
	public const string SCENARIO_MESSAGE_TEXT = "SCENARIO_MESSAGE_TEXT";

	[Locale(file = "en-expansion5", category = "Scenarios", hideInEditor = true)]
	public const string SCENARIO_WIN_TEXT = "SCENARIO_WIN_TEXT";

	[Locale(file = "en-expansion3", category = "Scenarios", hideInEditor = false)]
	public const string SCENARIO_WIN_CONTINUE = "SCENARIO_WIN_CONTINUE";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_FERRY_STOPPED = "VEHICLE_STATUS_FERRY_STOPPED";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_FERRY_ROUTE = "VEHICLE_STATUS_FERRY_ROUTE";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_FERRY_RETURN = "VEHICLE_STATUS_FERRY_RETURN";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_CABLECAR_SEARCH = "VEHICLE_STATUS_CABLECAR_SEARCH";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_CABLECAR_LOAD = "VEHICLE_STATUS_CABLECAR_LOAD";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_CABLECAR_WAIT = "VEHICLE_STATUS_CABLECAR_WAIT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_SEARCH = "VEHICLE_STATUS_BLIMP_SEARCH";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_LOAD = "VEHICLE_STATUS_BLIMP_LOAD";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_WAIT = "VEHICLE_STATUS_BLIMP_WAIT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_RETURN = "VEHICLE_STATUS_BLIMP_RETURN";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_MONORAIL_SEARCH = "VEHICLE_STATUS_MONORAIL_SEARCH";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_MONORAIL_LOAD = "VEHICLE_STATUS_MONORAIL_LOAD";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_MONORAIL_WAIT = "VEHICLE_STATUS_MONORAIL_WAIT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string AIINFO_FERRYDEPOT_FERRYCOUNT = "AIINFO_FERRYDEPOT_FERRYCOUNT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string AIINFO_BLIMPDEPOT_BLIMPCOUNT = "AIINFO_BLIMPDEPOT_BLIMPCOUNT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_STOPPED = "VEHICLE_STATUS_BLIMP_STOPPED";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_STATUS_BLIMP_ROUTE = "VEHICLE_STATUS_BLIMP_ROUTE";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_SHOWROUTE = "VEHICLE_SHOWROUTE";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string VEHICLE_HIDEROUTE = "VEHICLE_HIDEROUTE";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string PUBLICTRANSPORT_FERRYLINES = "PUBLICTRANSPORT_FERRYLINES";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string PUBLICTRANSPORT_BLIMPLINES = "PUBLICTRANSPORT_BLIMPLINES";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string PUBLICTRANSPORT_MONORAILLINES = "PUBLICTRANSPORT_MONORAILLINES";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_CABLECAR = "INFO_PUBLICTRANSPORT_CABLECAR";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_MONORAIL = "INFO_PUBLICTRANSPORT_MONORAIL";

	[Locale(file = "en-fixes1.4.x", category = "Sheet1", hideInEditor = true)]
	public const string PUBLICTRANSPORT_LINECOUNT = "PUBLICTRANSPORT_LINECOUNT";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string ROAD_SHOWROUTES = "ROAD_SHOWROUTES";

	[Locale(file = "en-expansion4", category = "Vehicles and transport types", hideInEditor = false)]
	public const string ROAD_HIDEROUTES = "ROAD_HIDEROUTES";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string ROAD_NAME_PATTERN = "ROAD_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string STREET_NAME_PATTERN = "STREET_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string AVENUE_NAME_PATTERN = "AVENUE_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string HIGHWAY_NAME_PATTERN = "HIGHWAY_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string BRIDGE_NAME_PATTERN = "BRIDGE_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Roads, canals and tracks", hideInEditor = true)]
	public const string TUNNEL_NAME_PATTERN = "TUNNEL_NAME_PATTERN";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_TITLE = "INFO_TRAFFICROUTES_TITLE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string WHATSNEWPANEL_TITLE = "WHATSNEWPANEL_TITLE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string WHATSNEWPANEL_GOTIT = "WHATSNEWPANEL_GOTIT";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX1 = "ROUTECHECKBOX1";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX2 = "ROUTECHECKBOX2";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX3 = "ROUTECHECKBOX3";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX4 = "ROUTECHECKBOX4";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX5 = "ROUTECHECKBOX5";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTECHECKBOX6 = "ROUTECHECKBOX6";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTEMENUITEMS1 = "ROUTEMENUITEMS1";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTEMENUITEMS2 = "ROUTEMENUITEMS2";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROUTEMENUITEMS3 = "ROUTEMENUITEMS3";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADINFOPANEL1 = "ROADINFOPANEL1";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADINFOPANEL2 = "ROADINFOPANEL2";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_MODIFYVEHICLECOUNT = "LINEINFOPANEL_MODIFYVEHICLECOUNT";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_MODIFYVEHICLECOUNT_TOOLTIP = "LINEINFOPANEL_MODIFYVEHICLECOUNT_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_TRIPSSAVED_TOOLTIP = "LINEINFOPANEL_TRIPSSAVED_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADINFOPANEL_PRIORITYROAD = "ROADINFOPANEL_PRIORITYROAD";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADINFOPANEL_SHOWROUTES = "ROADINFOPANEL_SHOWROUTES";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADINFOPANEL_HIDEROUTES = "ROADINFOPANEL_HIDEROUTES";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFIC_AVGTRAFFICFLOW = "INFO_TRAFFIC_AVGTRAFFICFLOW";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFIC_AVGTRAFFICFLOW_TOOLTIP = "INFO_TRAFFIC_AVGTRAFFICFLOW_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string BUDGETTOOLTIPMONORAIL = "BUDGETTOOLTIPMONORAIL";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string BUDGETTOOLTIPCABLECAR = "BUDGETTOOLTIPCABLECAR";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_TRANSPORTTYPES = "INFO_TRAFFICROUTES_TRANSPORTTYPES";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_ROUTES = "INFO_TRAFFICROUTES_ROUTES";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_JUNCTIONSETTINGS = "INFO_TRAFFICROUTES_JUNCTIONSETTINGS";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_TOGGLELIGHTS = "INFO_TRAFFICROUTES_TOGGLELIGHTS";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFO_TRAFFICROUTES_TOGGLESTOPSIGNS = "INFO_TRAFFICROUTES_TOGGLESTOPSIGNS";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADSNAPPING_SNAPTO = "ROADSNAPPING_SNAPTO";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADSNAPPING_ANGLE = "ROADSNAPPING_ANGLE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADSNAPPING_LENGTH = "ROADSNAPPING_LENGTH";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADSNAPPING_GRID = "ROADSNAPPING_GRID";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string ROADSNAPPING_HELPERLINE = "ROADSNAPPING_HELPERLINE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINESOVERVIEW_TITLE = "LINESOVERVIEW_TITLE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINESOVERVIEW_TEXT = "LINESOVERVIEW_TEXT";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string CURSORLIGHT_TITLE = "CURSORLIGHT_TITLE";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string CURSORLIGHT_TEXT = "CURSORLIGHT_TEXT";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_PASSENGERS = "LINEINFOPANEL_PASSENGERS";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_STOPS = "LINEINFOPANEL_STOPS";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string LINEINFOPANEL_VEHICLES = "LINEINFOPANEL_VEHICLES";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string INFO_ADJUSTROAD = "INFO_ADJUSTROAD";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string INFO_ADJUSTROADS = "INFO_ADJUSTROADS";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string INFO_ROADADJUSTMENT = "INFO_ROADADJUSTMENT";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_LARGETRAINSTATION = "CHIRP_FIRST_LARGETRAINSTATION";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ENDOFLINETRAINSTATION = "CHIRP_FIRST_ENDOFLINETRAINSTATION";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FERRYLINE = "CHIRP_FIRST_FERRYLINE";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_MONORAILLINE = "CHIRP_FIRST_MONORAILLINE";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CABLECARLINE = "CHIRP_FIRST_CABLECARLINE";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BLIMPLINE = "CHIRP_FIRST_BLIMPLINE";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TRAFFICPARK = "CHIRP_FIRST_TRAFFICPARK";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_STEAMTRAIN = "CHIRP_FIRST_STEAMTRAIN";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BOATMUSEUM = "CHIRP_FIRST_BOATMUSEUM";

	[Locale(file = "en-expansion4", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RANDOM_INMOTION = "CHIRP_RANDOM_INMOTION";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_SUBSCRIBEALL_TOOLTIP = "CONTENT_SUBSCRIBEALL_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_SUBSCRIBEALL_TOOLTIP_DISABLED = "CONTENT_SUBSCRIBEALL_TOOLTIP_DISABLED";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_ENABLEALL_TOOLTIP = "CONTENT_ENABLEALL_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_ENABLEALL_TOOLTIP_MISSING = "CONTENT_ENABLEALL_TOOLTIP_MISSING";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_ASSETS_USED = "CONTENT_ASSETS_USED";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_SUBSCRIBEALL = "CONTENT_SUBSCRIBEALL";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string GOALSPANEL_SEGMENTS = "GOALSPANEL_SEGMENTS";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CITYSERVICE_ACCEPTINTERCITYTRAINS = "CITYSERVICE_ACCEPTINTERCITYTRAINS";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CITYSERVICE_ACCEPTINTERCITYTRAINS_TOOLTIP = "CITYSERVICE_ACCEPTINTERCITYTRAINS_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string PUBLICTRANPORTTAB1 = "PUBLICTRANPORTTAB1";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string PUBLICTRANPORTTAB2 = "PUBLICTRANPORTTAB2";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string PUBLICTRANPORTTAB3 = "PUBLICTRANPORTTAB3";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string PUBLICTRANPORTTAB4 = "PUBLICTRANPORTTAB4";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_ROAD_NAMES_VISIBLE = "OPTIONS_ROAD_NAMES_VISIBLE";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TITLE1 = "FREEWHATSNEW_TITLE1";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TEXT1 = "FREEWHATSNEW_TEXT1";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TITLE2 = "FREEWHATSNEW_TITLE2";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TEXT2 = "FREEWHATSNEW_TEXT2";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TITLE3 = "FREEWHATSNEW_TITLE3";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TEXT3 = "FREEWHATSNEW_TEXT3";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TITLE4 = "FREEWHATSNEW_TITLE4";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TEXT4 = "FREEWHATSNEW_TEXT4";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TITLE5 = "FREEWHATSNEW_TITLE5";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string FREEWHATSNEW_TEXT5 = "FREEWHATSNEW_TEXT5";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TITLE1 = "EXPANSIONWHATSNEW_TITLE1";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TEXT1 = "EXPANSIONWHATSNEW_TEXT1";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TITLE2 = "EXPANSIONWHATSNEW_TITLE2";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TEXT2 = "EXPANSIONWHATSNEW_TEXT2";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TITLE3 = "EXPANSIONWHATSNEW_TITLE3";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TEXT3 = "EXPANSIONWHATSNEW_TEXT3";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TITLE4 = "EXPANSIONWHATSNEW_TITLE4";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TEXT4 = "EXPANSIONWHATSNEW_TEXT4";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TITLE5 = "EXPANSIONWHATSNEW_TITLE5";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_TEXT5 = "EXPANSIONWHATSNEW_TEXT5";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW_CHECKBOX = "EXPANSIONWHATSNEW_CHECKBOX";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_MASSTRANSIT = "DLCPANEL_MASSTRANSIT";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string KEYMAPPING_PRESSANYKEY = "KEYMAPPING_PRESSANYKEY";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string ERRORFORDEPOTCONNECTEDTOLINE = "ERRORFORDEPOTCONNECTEDTOLINE";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string BULLDOZER_UNDERGROUND_TOOLTIP = "BULLDOZER_UNDERGROUND_TOOLTIP";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string MOD_NAME = "MOD_NAME";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string MOD_DESCRIPTION = "MOD_DESCRIPTION";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_NOT_AVAILABLE_TOOLTIP = "CONTENT_PRESET_NOT_AVAILABLE_TOOLTIP";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_STATUS_PENDING = "CONTENT_PRESET_STATUS_PENDING";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_STATUS_INSTALLING = "CONTENT_PRESET_STATUS_INSTALLING";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_FAILED = "CONTENT_PRESET_FAILED";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_DOESNT_EXIST = "CONTENT_PRESET_DOESNT_EXIST";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_PRESET_DONE = "CONTENT_PRESET_DONE";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_AUTOSAVE_TIME_GAME = "OPTIONS_AUTOSAVE_TIME_GAME";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string OPTIONS_AUTOSAVE_TIME_EDITORS = "OPTIONS_AUTOSAVE_TIME_EDITORS";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_MT_REQUIRED = "CONTENT_MT_REQUIRED";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string CONTENT_MT_BUY = "CONTENT_MT_BUY";

	[Locale(file = "en-expansion4", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string INFOPANEL_TOGGLETRAFFICRROUTESVIEW = "INFOPANEL_TOGGLETRAFFICRROUTESVIEW";

	[Locale(file = "en-expansion4", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_RADIOROCKCITY = "DLCPANEL_RADIOROCKCITY";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TITLE1 = "FREEWHATSNEW5_TITLE1";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TEXT1 = "FREEWHATSNEW5_TEXT1";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TITLE2 = "FREEWHATSNEW5_TITLE2";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TEXT2 = "FREEWHATSNEW5_TEXT2";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TITLE3 = "FREEWHATSNEW5_TITLE3";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TEXT3 = "FREEWHATSNEW5_TEXT3";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TITLE4 = "FREEWHATSNEW5_TITLE4";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TEXT4 = "FREEWHATSNEW5_TEXT4";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TITLE5 = "FREEWHATSNEW5_TITLE5";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string FREEWHATSNEW5_TEXT5 = "FREEWHATSNEW5_TEXT5";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TITLE1 = "EXPANSIONWHATSNEW5_TITLE1";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TEXT1 = "EXPANSIONWHATSNEW5_TEXT1";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TITLE2 = "EXPANSIONWHATSNEW5_TITLE2";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TEXT2 = "EXPANSIONWHATSNEW5_TEXT2";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TITLE3 = "EXPANSIONWHATSNEW5_TITLE3";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TEXT3 = "EXPANSIONWHATSNEW5_TEXT3";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TITLE4 = "EXPANSIONWHATSNEW5_TITLE4";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW5_TEXT4 = "EXPANSIONWHATSNEW5_TEXT4";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TITLE3 = "EXPANSIONWHATSNEW6_TITLE3";

	[Locale(file = "en-expansion5", category = "Tutorial messages, info views a", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TEXT3 = "EXPANSIONWHATSNEW6_TEXT3";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ULTIMATE_RECYCLING_PLANT = "CHIRP_FIRST_ULTIMATE_RECYCLING_PLANT";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CENTRAL_PARK = "CHIRP_FIRST_CENTRAL_PARK";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FLOATING_GARDENS = "CHIRP_FIRST_FLOATING_GARDENS";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ZIGGURAT_GARDEN = "CHIRP_FIRST_ZIGGURAT_GARDEN";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_LUNGS_OF_THE_CITY = "CHIRP_FIRST_LUNGS_OF_THE_CITY";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CLIMATE_RESEARCH_STATION = "CHIRP_FIRST_CLIMATE_RESEARCH_STATION";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BIRD_AND_BEE_HAVEN = "CHIRP_FIRST_BIRD_AND_BEE_HAVEN";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_GEOTHERMAL_POWER_PLANT = "CHIRP_FIRST_GEOTHERMAL_POWER_PLANT";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_OCEAN_THERMAL_ENERGY_CONVERSION_PLANT = "CHIRP_FIRST_OCEAN_THERMAL_ENERGY_CONVERSION_PLANT";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SOLAR_UPDRAFT_TOWER = "CHIRP_FIRST_SOLAR_UPDRAFT_TOWER";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ECO_WATER_OUTLET = "CHIRP_FIRST_ECO_WATER_OUTLET";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ECO_WATER_TREATMENT_PLANT = "CHIRP_FIRST_ECO_WATER_TREATMENT_PLANT";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_RECYCLING_CENTER = "CHIRP_FIRST_RECYCLING_CENTER";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FLOATING_GARBAGE_COLLECTOR = "CHIRP_FIRST_FLOATING_GARBAGE_COLLECTOR";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_COMMUNITY_POOL = "CHIRP_FIRST_COMMUNITY_POOL";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_COMMUNITY_POOL_WINTER = "CHIRP_FIRST_COMMUNITY_POOL_WINTER";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_SPORTS_HALL_AND_GYMNASIUM = "CHIRP_FIRST_SPORTS_HALL_AND_GYMNASIUM";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_YOGA_GARDEN = "CHIRP_FIRST_YOGA_GARDEN";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_YOGA_GARDEN_WINTER = "CHIRP_FIRST_YOGA_GARDEN_WINTER";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_COMMUNITY_SCHOOL = "CHIRP_FIRST_COMMUNITY_SCHOOL";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_UNIVERSITY_OF_CREATIVE_ARTS = "CHIRP_FIRST_UNIVERSITY_OF_CREATIVE_ARTS";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_MODERN_TECHNOLOGY_INSTITUTE = "CHIRP_FIRST_MODERN_TECHNOLOGY_INSTITUTE";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BIOFUEL_BUS_DEPOT = "CHIRP_FIRST_BIOFUEL_BUS_DEPOT";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TROPICAL_GARDEN = "CHIRP_FIRST_TROPICAL_GARDEN";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FLOATING_CAFE = "CHIRP_FIRST_FLOATING_CAFE";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FISHING_ISLAND = "CHIRP_FIRST_FISHING_ISLAND";

	[Locale(file = "en-expansion5", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RANDOM_EXP5 = "CHIRP_RANDOM_EXP5";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_BUILDING_COUNT_PROG = "MILESTONE_BUILDING_COUNT_PROG";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_BUILDING_COUNT_DESC = "MILESTONE_BUILDING_COUNT_DESC";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string OPTION_FADE_AWAY_NOTIFICATIONS = "OPTION_FADE_AWAY_NOTIFICATIONS";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string ASSETTYPE_ROAD = "ASSETTYPE_ROAD";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_GREENCITIES = "DLCPANEL_GREENCITIES";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string CONTENT_GC_REQUIRED = "CONTENT_GC_REQUIRED";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string CONTENT_GC_BUY = "CONTENT_GC_BUY";

	[Locale(file = "en-fixes1.4.x", category = "Sheet1", hideInEditor = true)]
	public const string ASSETIMPORTER_CATEGORY = "ASSETIMPORTER_CATEGORY";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string CUSTOM_ROAD_TOOLTIP_TITLE = "CUSTOM_ROAD_TOOLTIP_TITLE";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string CUSTOM_ROAD_TOOLTIP_DESC = "CUSTOM_ROAD_TOOLTIP_DESC";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string LINEINFOPANEL_LINEINCOMPLETE = "LINEINFOPANEL_LINEINCOMPLETE";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_EUROCONTENTPACK = "DLCPANEL_EUROCONTENTPACK";

	[Locale(file = "en-expansion5", category = "Misc", hideInEditor = false)]
	public const string STYLES_EUROPEANSUBURBIA = "STYLES_EUROPEANSUBURBIA";

	[Locale(file = "en-expansion5", category = "Specializations", hideInEditor = false)]
	public const string UNLOCK_SPECIALIZATION_RESIDENTIAL = "UNLOCK_SPECIALIZATION_RESIDENTIAL";

	[Locale(file = "en-expansion5", category = "Specializations", hideInEditor = false)]
	public const string UNLOCK_SPECIALIZATION_OFFICE = "UNLOCK_SPECIALIZATION_OFFICE";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_OVERVIEW_TOOLTIP = "ECONOMYPANEL_OVERVIEW_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_RESIDENTIAL_TOOLTIP = "ECONOMYPANEL_RESIDENTIAL_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_COMMERCIAL_TOOLTIP = "ECONOMYPANEL_COMMERCIAL_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_WORK_TOOLTIP = "ECONOMYPANEL_WORK_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_PUBLICTRANSPORT_TOOLTIP = "ECONOMYPANEL_PUBLICTRANSPORT_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_EVENTS_TOOLTIP = "ECONOMYPANEL_EVENTS_TOOLTIP";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_LOWDENSITY = "ECONOMYPANEL_LOWDENSITY";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_HIGHDENSITY = "ECONOMYPANEL_HIGHDENSITY";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_IPC_RESIDENTIAL = "ECONOMYPANEL_IPC_RESIDENTIAL";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_IPC_COMMERCIAL = "ECONOMYPANEL_IPC_COMMERCIAL";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_IPC_WORK = "ECONOMYPANEL_IPC_WORK";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_IPC_PUBLICTRANSPORTINCOME = "ECONOMYPANEL_IPC_PUBLICTRANSPORTINCOME";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_IPC_PUBLICTRANSPORTEXPENSES = "ECONOMYPANEL_IPC_PUBLICTRANSPORTEXPENSES";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_LEISURE = "ECONOMYPANEL_LEISURE";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_TOURISM = "ECONOMYPANEL_TOURISM";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_ORGANIC = "ECONOMYPANEL_ORGANIC";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_SELFSUFFICIENT = "ECONOMYPANEL_SELFSUFFICIENT";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_FOREST = "ECONOMYPANEL_FOREST";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_OIL = "ECONOMYPANEL_OIL";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_ORE = "ECONOMYPANEL_ORE";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_FARMING = "ECONOMYPANEL_FARMING";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_HIGHTECH = "ECONOMYPANEL_HIGHTECH";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_OFFICE = "ECONOMYPANEL_OFFICE";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_INDUSTRY = "ECONOMYPANEL_INDUSTRY";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_PROFIT = "ECONOMYPANEL_PROFIT";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_TOTALPROFIT = "ECONOMYPANEL_TOTALPROFIT";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string INCOME_TT_TOTAL_RESIDENTIAL = "INCOME_TT_TOTAL_RESIDENTIAL";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string INCOME_TT_TOTAL_COMMERCIAL = "INCOME_TT_TOTAL_COMMERCIAL";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string INCOME_TT_TOTAL_WORK = "INCOME_TT_TOTAL_WORK";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string PROFIT_TT_TOTAL_PUBTRANS = "PROFIT_TT_TOTAL_PUBTRANS";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_TITLE_PUBTRANS = "ECONOMYPANEL_TITLE_PUBTRANS";

	[Locale(file = "en-expansion5", category = "EconomyPanel", hideInEditor = false)]
	public const string ECONOMYPANEL_ALL = "ECONOMYPANEL_ALL";

	[Locale(file = "en-festivals-batch2", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_MILESTONEPASSED = "FESTIVALPANEL_MILESTONEPASSED";

	[Locale(file = "en-festivals-batch2", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_UPGRADECOST = "FESTIVALPANEL_UPGRADECOST";

	[Locale(file = "en-festivals-batch2", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_CONDITIONS = "FESTIVALPANEL_CONDITIONS";

	[Locale(file = "en-festivals-batch2", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_CONCERTS = "DLCPANEL_CONCERTS";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_ARTIST = "FESTIVALPANEL_ARTIST";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_POPULARITY = "FESTIVALPANEL_POPULARITY";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_FUTURECONCERTS = "FESTIVALPANEL_FUTURECONCERTS";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_NOWPLAYING = "FESTIVALPANEL_NOWPLAYING";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_PASTCONCERTS = "FESTIVALPANEL_PASTCONCERTS";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_COLOR = "FESTIVALPANEL_COLOR";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_TICKETPRICE = "FESTIVALPANEL_TICKETPRICE";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_TICKETINCOME = "FESTIVALPANEL_TICKETINCOME";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_POPULARITYCHANGE = "FESTIVALPANEL_POPULARITYCHANGE";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_SECURITYBUDGET = "FESTIVALPANEL_SECURITYBUDGET";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = false)]
	public const string FESTIVALPANEL_UPGRADES = "FESTIVALPANEL_UPGRADES";

	[Locale(file = "en-festivals", category = "FestivalPanel", hideInEditor = true)]
	public const string FESTIVALMILESTONEDESCR = "FESTIVALMILESTONEDESCR";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PDXLOGIN_ERROR = "PDXLOGIN_ERROR";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string LOADSCENARIO_TITLE = "LOADSCENARIO_TITLE";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string CONCERT_BAND_NAME = "CONCERT_BAND_NAME";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_EVENT_TICKETS_PROG = "MILESTONE_EVENT_TICKETS_PROG";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_EVENT_TICKETS_DESC = "MILESTONE_EVENT_TICKETS_DESC";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_EVENT_POPULARITY_PROG = "MILESTONE_EVENT_POPULARITY_PROG";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string MILESTONE_EVENT_POPULARITY_DESC = "MILESTONE_EVENT_POPULARITY_DESC";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_ASCENDING = "PANEL_SORT_ASCENDING";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_DESCENDING = "PANEL_SORT_DESCENDING";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_ORDER = "PANEL_SORT_ORDER";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_ACTIVE = "PANEL_SORT_ACTIVE";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_SUBSCRIBED = "PANEL_SORT_SUBSCRIBED";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string PANEL_SORT_PATH = "PANEL_SORT_PATH";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string CONTENT_SELECT_ALL = "CONTENT_SELECT_ALL";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string CONTENT_DESELECT_ALL = "CONTENT_DESELECT_ALL";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string CONTENT_SELECTED_COUNT = "CONTENT_SELECTED_COUNT";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = false)]
	public const string ROADSNAPPING_TOGGLEALL = "ROADSNAPPING_TOGGLEALL";

	[Locale(file = "en-festivals", category = "Misc", hideInEditor = true)]
	public const string CONFIRM_UNSUBSCRIBESELECTED = "CONFIRM_UNSUBSCRIBESELECTED";

	[Locale(file = "en-festivals", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_BAND_NESTOR = "CHIRP_BAND_NESTOR";

	[Locale(file = "en-festivals", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_BAND_LILY = "CHIRP_BAND_LILY";

	[Locale(file = "en-festivals", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_BAND_MOTI = "CHIRP_BAND_MOTI";

	[Locale(file = "en-festivals", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_FESTIVALAREA = "CHIRP_NEW_FESTIVALAREA";

	[Locale(file = "en-festivals", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_UPGRADE_FESTIVALAREA = "CHIRP_UPGRADE_FESTIVALAREA";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string FREEWHATSNEW6_TITLE1 = "FREEWHATSNEW6_TITLE1";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string FREEWHATSNEW6_TEXT1 = "FREEWHATSNEW6_TEXT1";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TITLE1 = "EXPANSIONWHATSNEW6_TITLE1";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TEXT1 = "EXPANSIONWHATSNEW6_TEXT1";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TITLE2 = "EXPANSIONWHATSNEW6_TITLE2";

	[Locale(file = "en-festivals", category = "Whats_new", hideInEditor = false)]
	public const string EXPANSIONWHATSNEW6_TEXT2 = "EXPANSIONWHATSNEW6_TEXT2";

	[Locale(file = "en-fixes1.4.x", category = "Sheet1", hideInEditor = true)]
	public const string CONFIRM_UNSUBSCRIBEALL = "CONFIRM_UNSUBSCRIBEALL";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string SPORT_TEAM_NAME = "SPORT_TEAM_NAME";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string SPORT_OPPONENT_NAME = "SPORT_OPPONENT_NAME";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO1 = "STADIUMINFO1";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO2 = "STADIUMINFO2";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO3 = "STADIUMINFO3";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO4 = "STADIUMINFO4";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO5 = "STADIUMINFO5";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO6 = "STADIUMINFO6";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO7 = "STADIUMINFO7";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO8 = "STADIUMINFO8";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO9 = "STADIUMINFO9";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO10 = "STADIUMINFO10";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO11 = "STADIUMINFO11";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string STADIUMINFO12 = "STADIUMINFO12";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string FOOTBALLCHIRP_WIN = "FOOTBALLCHIRP_WIN";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string FOOTBALLCHIRP_LOSE = "FOOTBALLCHIRP_LOSE";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string FOOTBALLCHIRP_GENERIC = "FOOTBALLCHIRP_GENERIC";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_VISITORS = "SPORT_VISITORS";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_WON = "SPORT_WON";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_LOST = "SPORT_LOST";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_CANCELLED = "SPORT_CANCELLED";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_TOTAL = "SPORT_TOTAL";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_NEXTMATCH = "SPORT_NEXTMATCH";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_CURRENTMATCH = "SPORT_CURRENTMATCH";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string SPORT_EXPENSES = "SPORT_EXPENSES";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string DATETIME_WEEKLOWERCASE = "DATETIME_WEEKLOWERCASE";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string ERRORMESSAGE = "ERRORMESSAGE";

	[Locale(file = "en-football", category = "Misc", hideInEditor = true)]
	public const string FOOTBALLCHIRP_FIRST_FSTADIUM = "FOOTBALLCHIRP_FIRST_FSTADIUM";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_MATCHDAY = "DLCPANEL_MATCHDAY";

	[Locale(file = "en-football", category = "Misc", hideInEditor = false)]
	public const string DLCPANEL_STADIUMS = "DLCPANEL_STADIUMS";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string TOOL_LANDSCAPING_COST = "TOOL_LANDSCAPING_COST";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string INFO_LANDSCAPING_DIRT = "INFO_LANDSCAPING_DIRT";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string INFO_LANDSCAPING_TITLE = "INFO_LANDSCAPING_TITLE";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string AIINFO_COSTPERCUBICCELL = "AIINFO_COSTPERCUBICCELL";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string LANDSCAPING_OPTION_BRUSHSTRENGTH = "LANDSCAPING_OPTION_BRUSHSTRENGTH";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string ROAD_OPTION_ELEVATIONSTEP = "ROAD_OPTION_ELEVATIONSTEP";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = true)]
	public const string LANDSCAPING_CATEGORY = "LANDSCAPING_CATEGORY";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = true)]
	public const string LANDSCAPING_TITLE = "LANDSCAPING_TITLE";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = true)]
	public const string LANDSCAPING_DESC = "LANDSCAPING_DESC";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = true)]
	public const string MAIN_TOOL_WW = "MAIN_TOOL_WW";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = true)]
	public const string BUDGET_INFO_WW = "BUDGET_INFO_WW";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string EXPENSES_TT_WATER_WW = "EXPENSES_TT_WATER_WW";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string VEHICLE_MODIFYLINE = "VEHICLE_MODIFYLINE";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string VEHICLE_LINESOVERVIEW = "VEHICLE_LINESOVERVIEW";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string HEIGHTMAP_PRECISION = "HEIGHTMAP_PRECISION";

	[Locale(file = "en-landscaping", category = "Misc", hideInEditor = false)]
	public const string HEIGHTMAP_FORMAT = "HEIGHTMAP_FORMAT";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_CONGRATULATIONS = "UNLOCK_CONGRATULATIONS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_SERVICES = "UNLOCK_SERVICES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_FEATURES = "UNLOCK_FEATURES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_BUILDINGS = "UNLOCK_BUILDINGS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_ROADS = "UNLOCK_ROADS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_ZONES = "UNLOCK_ZONES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_SPECIALIZATION = "UNLOCK_SPECIALIZATION";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_AREAS = "UNLOCK_AREAS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_AREA_TITLE = "UNLOCK_AREA_TITLE";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_POLICIES = "UNLOCK_POLICIES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_MILESTONES = "UNLOCK_MILESTONES";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_MONUMENTS = "UNLOCK_MONUMENTS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_WONDERS = "UNLOCK_WONDERS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_ACHIEVEMENTS = "UNLOCK_ACHIEVEMENTS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_UNLOCKS = "UNLOCK_UNLOCKS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string UNLOCK_MILESTONE_TITLE = "UNLOCK_MILESTONE_TITLE";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_REQUIREMENTS = "UNLOCK_REQUIREMENTS";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string UNLOCK_REQUIREMENTS_HIDDEN = "UNLOCK_REQUIREMENTS_HIDDEN";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_POPULATION_PROG = "MILESTONE_POPULATION_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_POPULATION_DESC = "MILESTONE_POPULATION_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TAXES_MAX_PROG = "MILESTONE_TAXES_MAX_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_TAXES_MAX_DESC = "MILESTONE_TAXES_MAX_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_OLDEST_PROG = "MILESTONE_OLDEST_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_OLDEST_DESC = "MILESTONE_OLDEST_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_PASSED_PROG = "MILESTONE_COMBINED_PASSED_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_PASSED_DESC = "MILESTONE_COMBINED_PASSED_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_TIME_PROG = "MILESTONE_COMBINED_TIME_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_TIME_DESC = "MILESTONE_COMBINED_TIME_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_PASSED_TIME_PROG = "MILESTONE_COMBINED_PASSED_TIME_PROG";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = false)]
	public const string MILESTONE_COMBINED_PASSED_TIME_DESC = "MILESTONE_COMBINED_PASSED_TIME_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string SERVICE_DESC = "SERVICE_DESC";

	[Locale(file = "en-main", category = "Milestones", hideInEditor = true)]
	public const string AREA_DESC = "AREA_DESC";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string NO_SPECIALIZATION = "NO_SPECIALIZATION";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICIES_CITYCAPTION = "POLICIES_CITYCAPTION";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICIES_DISTRICTCAPTION = "POLICIES_DISTRICTCAPTION";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICY_INDUSTRY = "POLICY_INDUSTRY";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICY_SERVICES = "POLICY_SERVICES";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICY_TAXATION = "POLICY_TAXATION";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICY_CITYPLANNING = "POLICY_CITYPLANNING";

	[Locale(file = "en-main", category = "Policies", hideInEditor = false)]
	public const string POLICY_SPECIAL = "POLICY_SPECIAL";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_COSTPERCELL = "AIINFO_COSTPERCELL";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_COST = "AIINFO_COST";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_CUMULATIVECOST = "AIINFO_CUMULATIVECOST";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_UPKEEP = "AIINFO_UPKEEP";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_CUMULATIVEUPKEEP = "AIINFO_CUMULATIVEUPKEEP";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_UPKEEPPERCELL = "AIINFO_UPKEEPPERCELL";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_SPEED = "AIINFO_SPEED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_DISTANCE_COST = "AIINFO_DISTANCE_COST";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_POLE_UPKEEP = "AIINFO_POLE_UPKEEP";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_POLLUTION = "AIINFO_POLLUTION";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_NOISEPOLLUTION = "AIINFO_NOISEPOLLUTION";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_ELECTRICITY_PRODUCTION = "AIINFO_ELECTRICITY_PRODUCTION";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_ELECTRICITY_PRODUCTION_RANGE = "AIINFO_ELECTRICITY_PRODUCTION_RANGE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_ELECTRICITY_CONSUMPTION = "AIINFO_ELECTRICITY_CONSUMPTION";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_CONSUMPTION = "AIINFO_WATER_CONSUMPTION";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_INTAKE = "AIINFO_WATER_INTAKE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_OUTLET = "AIINFO_WATER_OUTLET";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_CAPACITY = "AIINFO_CAPACITY";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_GARBAGETRUCK_CAPACITY = "AIINFO_GARBAGETRUCK_CAPACITY";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_PROCESSING_RATE = "AIINFO_PROCESSING_RATE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_PATIENT_CAPACITY = "AIINFO_PATIENT_CAPACITY";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_POLICECAR_COUNT = "AIINFO_POLICECAR_COUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_FIRETRUCK_COUNT = "AIINFO_FIRETRUCK_COUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_CUSTOMER_CAPACITY = "AIINFO_CUSTOMER_CAPACITY";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_STUDENT_COUNT = "AIINFO_STUDENT_COUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_GOVERNMENT_REQUIREMENT = "AIINFO_GOVERNMENT_REQUIREMENT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WINDSPEED = "AIINFO_WINDSPEED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_ELECTRICITY_PRODUCED = "AIINFO_ELECTRICITY_PRODUCED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_COAL_STORED = "AIINFO_COAL_STORED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_OIL_STORED = "AIINFO_OIL_STORED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_STORED = "AIINFO_WATER_STORED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_PUMPED = "AIINFO_WATER_PUMPED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_DRAINED = "AIINFO_WATER_DRAINED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_WATER_TREATED = "AIINFO_WATER_TREATED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_STUDENTS = "AIINFO_STUDENTS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_UNIVERSITY_STUDENTCOUNT = "AIINFO_UNIVERSITY_STUDENTCOUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_HIGHSCHOOL_STUDENTCOUNT = "AIINFO_HIGHSCHOOL_STUDENTCOUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_ELEMENTARY_STUDENTCOUNT = "AIINFO_ELEMENTARY_STUDENTCOUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_PASSENGERS_SERVICED = "AIINFO_PASSENGERS_SERVICED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_BUSDEPOT_BUSCOUNT = "AIINFO_BUSDEPOT_BUSCOUNT";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_FIRES_EXTINGUISHED = "AIINFO_FIRES_EXTINGUISHED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_FIRE_ENGINES = "AIINFO_FIRE_ENGINES";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_POLICE_CARS = "AIINFO_POLICE_CARS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_VISITORS = "AIINFO_VISITORS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_TOURISTS = "AIINFO_TOURISTS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_PATIENTS = "AIINFO_PATIENTS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_PATIENTS_TREATED = "AIINFO_PATIENTS_TREATED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_AMBULANCES = "AIINFO_AMBULANCES";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_DECEASED_STORED = "AIINFO_DECEASED_STORED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_DECEASED_PROCESSED = "AIINFO_DECEASED_PROCESSED";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_FULL = "AIINFO_FULL";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_GARBAGE_RESERVES = "AIINFO_GARBAGE_RESERVES";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_CAPACITY_IN_USE = "AIINFO_CAPACITY_IN_USE";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_HEARSES = "AIINFO_HEARSES";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string AIINFO_GARBAGE_TRUCKS = "AIINFO_GARBAGE_TRUCKS";

	[Locale(file = "en-main", category = "Prefabs Collection", hideInEditor = false)]
	public const string MONUMENT_SHORT_DESC = "MONUMENT_SHORT_DESC";

	[Locale(file = "en-main", category = "Office", hideInEditor = true)]
	public const string OFFICE_NAME = "OFFICE_NAME";

	[Locale(file = "en-main", category = "Industrial", hideInEditor = true)]
	public const string INDUSTRIAL_NAME = "INDUSTRIAL_NAME";

	[Locale(file = "en-main", category = "Industrial", hideInEditor = true)]
	public const string EXTRACTOR_NAME = "EXTRACTOR_NAME";

	[Locale(file = "en-main", category = "Commercial", hideInEditor = true)]
	public const string COMMERCIAL_LOW_NAME = "COMMERCIAL_LOW_NAME";

	[Locale(file = "en-main", category = "Commercial", hideInEditor = true)]
	public const string COMMERCIAL_HIGH_NAME = "COMMERCIAL_HIGH_NAME";

	[Locale(file = "en-main", category = "Residential", hideInEditor = true)]
	public const string RESIDENCE_LOW_PATTERN = "RESIDENCE_LOW_PATTERN";

	[Locale(file = "en-main", category = "Residential", hideInEditor = true)]
	public const string RESIDENCE_HIGH_PATTERN = "RESIDENCE_HIGH_PATTERN";

	[Locale(file = "en-main", category = "Residential", hideInEditor = true)]
	public const string RESIDENCE_NAME = "RESIDENCE_NAME";

	[Locale(file = "en-main", category = "Districts", hideInEditor = true)]
	public const string DISTRICT_PATTERN = "DISTRICT_PATTERN";

	[Locale(file = "en-main", category = "Districts", hideInEditor = true)]
	public const string DISTRICT_NAME = "DISTRICT_NAME";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = false)]
	public const string CHIRPER_NAME = "CHIRPER_NAME";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_DEFAULT = "CHIRP_DEFAULT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_WATER_PUMP = "CHIRP_FIRST_WATER_PUMP";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_DRAIN_PIPE = "CHIRP_FIRST_DRAIN_PIPE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_POWER_PLANT = "CHIRP_FIRST_POWER_PLANT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_COAL_OR_OIL_PLANT = "CHIRP_FIRST_COAL_OR_OIL_PLANT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_NUCLEAR_PLANT = "CHIRP_FIRST_NUCLEAR_PLANT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_LANDFILL_SITE = "CHIRP_FIRST_LANDFILL_SITE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_COMBUSTION_PLANT = "CHIRP_FIRST_COMBUSTION_PLANT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CLINIC = "CHIRP_FIRST_CLINIC";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FIRE_HOUSE = "CHIRP_FIRST_FIRE_HOUSE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_FIRE_STATION = "CHIRP_FIRST_FIRE_STATION";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_POLICE_STATION = "CHIRP_FIRST_POLICE_STATION";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_POLICE_HQ = "CHIRP_FIRST_POLICE_HQ";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TOWN_HALL = "CHIRP_FIRST_TOWN_HALL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CITY_HALL = "CHIRP_FIRST_CITY_HALL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_ELEMENTARY_SCHOOL = "CHIRP_FIRST_ELEMENTARY_SCHOOL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_HIGH_SCHOOL = "CHIRP_FIRST_HIGH_SCHOOL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_UNIVERSITY = "CHIRP_FIRST_UNIVERSITY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BUS_LINE = "CHIRP_FIRST_BUS_LINE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_TRAIN_LINE = "CHIRP_FIRST_TRAIN_LINE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_METRO_LINE = "CHIRP_FIRST_METRO_LINE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_HARBOR = "CHIRP_FIRST_HARBOR";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_AIRPORT = "CHIRP_FIRST_AIRPORT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_MONUMENT = "CHIRP_FIRST_MONUMENT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_WONDER = "CHIRP_FIRST_WONDER";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CEMETERY = "CHIRP_FIRST_CEMETERY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CREMATORY = "CHIRP_FIRST_CREMATORY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_BUS_DEPOT = "CHIRP_FIRST_BUS_DEPOT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CARGO_CENTER = "CHIRP_FIRST_CARGO_CENTER";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRST_CARGO_HARBOR = "CHIRP_FIRST_CARGO_HARBOR";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_WIND_OR_SOLAR_PLANT = "CHIRP_NEW_WIND_OR_SOLAR_PLANT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_HOSPITAL = "CHIRP_NEW_HOSPITAL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_UNIVERSITY = "CHIRP_NEW_UNIVERSITY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_POLICE_HQ = "CHIRP_NEW_POLICE_HQ";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_FIRE_STATION = "CHIRP_NEW_FIRE_STATION";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_PARK = "CHIRP_NEW_PARK";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_PLAZA = "CHIRP_NEW_PLAZA";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_MONUMENT = "CHIRP_NEW_MONUMENT";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_ORGANIC_FARMING = "CHIRP_ORGANIC_FARMING";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_PUBLIC_TRANSPORT_EFFICIENCY = "CHIRP_PUBLIC_TRANSPORT_EFFICIENCY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_DAYCARE_SERVICE = "CHIRP_DAYCARE_SERVICE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_STUDENT_LODGING = "CHIRP_STUDENT_LODGING";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_ASSISTIVE_TECHNOLOGIES = "CHIRP_ASSISTIVE_TECHNOLOGIES";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_CHEAP_FLOWERS = "CHIRP_CHEAP_FLOWERS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_MILESTONE_REACHED = "CHIRP_MILESTONE_REACHED";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_MAP_TILE = "CHIRP_NEW_MAP_TILE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEW_TILE_PLACED = "CHIRP_NEW_TILE_PLACED";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NO_SCHOOLS = "CHIRP_NO_SCHOOLS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NO_HEALTHCARE = "CHIRP_NO_HEALTHCARE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_LOW_CRIME = "CHIRP_LOW_CRIME";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_HIGH_CRIME = "CHIRP_HIGH_CRIME";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_LOW_HEALTH = "CHIRP_LOW_HEALTH";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_TRASH_PILING_UP = "CHIRP_TRASH_PILING_UP";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NO_WATER = "CHIRP_NO_WATER";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_SEWAGE = "CHIRP_SEWAGE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NO_ELECTRICITY = "CHIRP_NO_ELECTRICITY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_LOW_HAPPINESS = "CHIRP_LOW_HAPPINESS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_HAPPY_PEOPLE = "CHIRP_HAPPY_PEOPLE";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_ATTRACTIVE_CITY = "CHIRP_ATTRACTIVE_CITY";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_INDUSTRIAL_DEMAND = "CHIRP_INDUSTRIAL_DEMAND";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_COMMERCIAL_DEMAND = "CHIRP_COMMERCIAL_DEMAND";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_RESIDENTIAL_DEMAND = "CHIRP_RESIDENTIAL_DEMAND";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_FIRE_HAZARD = "CHIRP_FIRE_HAZARD";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_DEAD_PILING_UP = "CHIRP_DEAD_PILING_UP";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_HIGH_TECH_LEVEL = "CHIRP_HIGH_TECH_LEVEL";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_ABANDONED_BUILDINGS = "CHIRP_ABANDONED_BUILDINGS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NEED_MORE_PARKS = "CHIRP_NEED_MORE_PARKS";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_POLLUTION = "CHIRP_POLLUTION";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_NOISEPOLLUTION = "CHIRP_NOISEPOLLUTION";

	[Locale(file = "en-main", category = "Chirps", hideInEditor = true)]
	public const string CHIRP_POISONED = "CHIRP_POISONED";

	[Locale(file = "en-main", category = "Citizens", hideInEditor = true)]
	public const string NAME_MALE_FIRST = "NAME_MALE_FIRST";

	[Locale(file = "en-main", category = "Citizens", hideInEditor = true)]
	public const string NAME_MALE_LAST = "NAME_MALE_LAST";

	[Locale(file = "en-main", category = "Citizens", hideInEditor = true)]
	public const string NAME_FEMALE_FIRST = "NAME_FEMALE_FIRST";

	[Locale(file = "en-main", category = "Citizens", hideInEditor = true)]
	public const string NAME_FEMALE_LAST = "NAME_FEMALE_LAST";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_TITLE = "BAILOUT_TITLE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_MESSAGE = "BAILOUT_MESSAGE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_ACCEPT = "BAILOUT_ACCEPT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_REJECT = "BAILOUT_REJECT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_NAME = "BAILOUT_NAME";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_CONTENT = "BAILOUT_CONTENT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string BAILOUT_DETAILS = "BAILOUT_DETAILS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = true)]
	public const string BANKNAME = "BANKNAME";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_AMOUNT = "LOAN_AMOUNT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_PAYMENTPLAN = "LOAN_PAYMENTPLAN";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_INTEREST = "LOAN_INTEREST";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_WEEKLYCOST = "LOAN_WEEKLYCOST";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_TOTAL = "LOAN_TOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_PAYMENTLEFT = "LOAN_PAYMENTLEFT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_PAYMENTTIMELEFT = "LOAN_PAYMENTTIMELEFT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_PAYMENTFORMAT = "LOAN_PAYMENTFORMAT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_TAKE = "LOAN_TAKE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_PAY = "LOAN_PAY";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_MONTHS = "DATETIME_MONTHS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_YEARS = "DATETIME_YEARS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_MONTH = "DATETIME_MONTH";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_YEAR = "DATETIME_YEAR";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_WEEKS = "DATETIME_WEEKS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string DATETIME_WEEK = "DATETIME_WEEK";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string LOAN_DESC = "LOAN_DESC";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_TITLE = "ECONOMY_TITLE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_TAXES = "ECONOMY_TAXES";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_BUDGET = "ECONOMY_BUDGET";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LOANS = "ECONOMY_LOANS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_INCOME_EXPENSES = "ECONOMY_INCOME_EXPENSES";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_BUS = "ECONOMY_BUS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_METRO = "ECONOMY_METRO";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_TRAIN = "ECONOMY_TRAIN";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_SHIP = "ECONOMY_SHIP";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_PLANE = "ECONOMY_PLANE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LEVEL1 = "ECONOMY_LEVEL1";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LEVEL2 = "ECONOMY_LEVEL2";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LEVEL3 = "ECONOMY_LEVEL3";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LEVEL4 = "ECONOMY_LEVEL4";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_LEVEL5 = "ECONOMY_LEVEL5";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_FARMING = "ECONOMY_FARMING";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_FORESTRY = "ECONOMY_FORESTRY";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_OIL = "ECONOMY_OIL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string ECONOMY_ORE = "ECONOMY_ORE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_RESIDENTIALLOW = "INCOME_TT_RESIDENTIALLOW";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_RESIDENTIALHIGH = "INCOME_TT_RESIDENTIALHIGH";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_RESIDENTIALTOTAL = "INCOME_TT_RESIDENTIALTOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALLOW = "INCOME_TT_COMMERCIALLOW";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALHIGH = "INCOME_TT_COMMERCIALHIGH";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALCITIZEN = "INCOME_TT_COMMERCIALCITIZEN";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALTOURIST = "INCOME_TT_COMMERCIALTOURIST";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALTOTAL = "INCOME_TT_COMMERCIALTOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_INDUSTRIAL = "INCOME_TT_INDUSTRIAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_OFFICE = "INCOME_TT_OFFICE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_TOTAL = "INCOME_TT_TOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TT_PUBLICTRANSPORT = "INCOME_TT_PUBLICTRANSPORT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_RESIDENTIALTOTAL = "INCOME_RESIDENTIALTOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_COMMERCIALTOTAL = "INCOME_COMMERCIALTOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TOTAL = "INCOME_TOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_CITIZEN = "INCOME_CITIZEN";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string INCOME_TOURIST = "INCOME_TOURIST";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TOTAL = "EXPENSES_TOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TITLE = "EXPENSES_TITLE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_TOTAL = "EXPENSES_TT_TOTAL";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_ROADS = "EXPENSES_TT_ROADS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_ELECTRICITY = "EXPENSES_TT_ELECTRICITY";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_WATER = "EXPENSES_TT_WATER";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_GARBAGE = "EXPENSES_TT_GARBAGE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_MONUMENTS = "EXPENSES_TT_MONUMENTS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_HEALTHCARE = "EXPENSES_TT_HEALTHCARE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_EDUCATION = "EXPENSES_TT_EDUCATION";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_POLICE = "EXPENSES_TT_POLICE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_FIRE = "EXPENSES_TT_FIRE";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_PARK = "EXPENSES_TT_PARK";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_PUBLICTRANSPORT = "EXPENSES_TT_PUBLICTRANSPORT";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_LOANS = "EXPENSES_TT_LOANS";

	[Locale(file = "en-main", category = "Economy", hideInEditor = false)]
	public const string EXPENSES_TT_POLICIES = "EXPENSES_TT_POLICIES";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_NONE = "NOTIFICATION_NONE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_ABANDONED = "NOTIFICATION_ABANDONED";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_BURNED_DOWN = "NOTIFICATION_BURNED_DOWN";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_MULTIPLE = "NOTIFICATION_MULTIPLE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_UB_TITLE1 = "NOTIFICATION_CONGRATS_UB_TITLE1";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_UB = "NOTIFICATION_CONGRATS_UB";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_UB_TITLE2 = "NOTIFICATION_CONGRATS_UB_TITLE2";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_UB_TITLE3 = "NOTIFICATION_CONGRATS_UB_TITLE3";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_MONUMENT_TITLE = "NOTIFICATION_CONGRATS_MONUMENT_TITLE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_MONUMENT = "NOTIFICATION_CONGRATS_MONUMENT";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_MONUMENT_BUILT_TITLE = "NOTIFICATION_CONGRATS_MONUMENT_BUILT_TITLE";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string NOTIFICATION_CONGRATS_MONUMENT_BUILT = "NOTIFICATION_CONGRATS_MONUMENT_BUILT";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_1 = "STATS_1";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_2 = "STATS_2";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_3 = "STATS_3";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_4 = "STATS_4";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_5 = "STATS_5";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_6 = "STATS_6";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_7 = "STATS_7";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_8 = "STATS_8";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_9 = "STATS_9";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_10 = "STATS_10";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_11 = "STATS_11";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_12 = "STATS_12";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_13 = "STATS_13";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_14 = "STATS_14";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_15 = "STATS_15";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_16 = "STATS_16";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_17 = "STATS_17";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_18 = "STATS_18";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_19 = "STATS_19";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_20 = "STATS_20";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_21 = "STATS_21";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_1_TOOLTIP = "STATS_1_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_2_TOOLTIP = "STATS_2_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_3_TOOLTIP = "STATS_3_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_4_TOOLTIP = "STATS_4_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_5_TOOLTIP = "STATS_5_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_6_TOOLTIP = "STATS_6_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_7_TOOLTIP = "STATS_7_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_8_TOOLTIP = "STATS_8_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_9_TOOLTIP = "STATS_9_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_10_TOOLTIP = "STATS_10_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_11_TOOLTIP = "STATS_11_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_12_TOOLTIP = "STATS_12_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_13_TOOLTIP = "STATS_13_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_14_TOOLTIP = "STATS_14_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_15_TOOLTIP = "STATS_15_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_16_TOOLTIP = "STATS_16_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_17_TOOLTIP = "STATS_17_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_18_TOOLTIP = "STATS_18_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_19_TOOLTIP = "STATS_19_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_20_TOOLTIP = "STATS_20_TOOLTIP";

	[Locale(file = "en-main", category = "Notifications", hideInEditor = false)]
	public const string STATS_21_TOOLTIP = "STATS_21_TOOLTIP";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_CONSTRUCTION_COST = "TOOL_CONSTRUCTION_COST";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_UPGRADE_COST = "TOOL_UPGRADE_COST";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_ELECTRICITY_ESTIMATE = "TOOL_ELECTRICITY_ESTIMATE";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_REFUND_AMOUNT = "TOOL_REFUND_AMOUNT";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_RELOCATE_COST = "TOOL_RELOCATE_COST";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_NEW_LINE = "TOOL_NEW_LINE";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_ADD_STOP = "TOOL_ADD_STOP";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_MOVE_STOP = "TOOL_MOVE_STOP";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_CLOSE_LINE = "TOOL_CLOSE_LINE";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_DRAG_STOP = "TOOL_DRAG_STOP";

	[Locale(file = "en-main", category = "Tools", hideInEditor = false)]
	public const string TOOL_DRAG_LINE = "TOOL_DRAG_LINE";

	[Locale(file = "en-main", category = "Connections", hideInEditor = true)]
	public const string CONNECTIONS_PATTERN = "CONNECTIONS_PATTERN";

	[Locale(file = "en-main", category = "Connections", hideInEditor = true)]
	public const string CONNECTIONS_NAME = "CONNECTIONS_NAME";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_FILMGRAINAMOUNT = "OPTIONS_FILMGRAINAMOUNT";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_CHIRPERVOLUME = "OPTIONS_CHIRPERVOLUME";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string LOADING = "LOADING";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_TOOLTIP_IMAGE = "SAVEASSET_TOOLTIP_IMAGE";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_ICON = "SAVEASSET_THUMBNAIL_ICON";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_STATE = "SAVEASSET_THUMBNAIL_STATE";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_NORMAL = "SAVEASSET_THUMBNAIL_NORMAL";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_FOCUSED = "SAVEASSET_THUMBNAIL_FOCUSED";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_HOVERED = "SAVEASSET_THUMBNAIL_HOVERED";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_PRESSED = "SAVEASSET_THUMBNAIL_PRESSED";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string SAVEASSET_THUMBNAIL_DISABLED = "SAVEASSET_THUMBNAIL_DISABLED";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string MODS_DISCLAIMER = "MODS_DISCLAIMER";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string MODS_UNDERSTOOD = "MODS_UNDERSTOOD";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string MODS_DECLINE = "MODS_DECLINE";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string MODS_DONTSHOW = "MODS_DONTSHOW";

	[Locale(file = "en-patch1.0.6", category = "Menus", hideInEditor = false)]
	public const string ASSET_DESC = "ASSET_DESC";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_VSYNC_OFF = "OPTIONS_VSYNC_OFF";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_VSYNC_VBLANK = "OPTIONS_VSYNC_VBLANK";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_VSYNC_SECONDVBLANK = "OPTIONS_VSYNC_SECONDVBLANK";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_INVERTY = "OPTIONS_INVERTY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_COLORCORRECTIONS = "CONTENTMANAGER_COLORCORRECTIONS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_SELECTCATEGORY = "CONTENTMANAGER_SELECTCATEGORY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATED = "CONTENTMANAGER_LASTUPDATED";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDSECOND = "CONTENTMANAGER_LASTUPDATEDSECOND";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDSECONDS = "CONTENTMANAGER_LASTUPDATEDSECONDS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDMINUTE = "CONTENTMANAGER_LASTUPDATEDMINUTE";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDMINUTES = "CONTENTMANAGER_LASTUPDATEDMINUTES";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDHOUR = "CONTENTMANAGER_LASTUPDATEDHOUR";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDHOURS = "CONTENTMANAGER_LASTUPDATEDHOURS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDDAY = "CONTENTMANAGER_LASTUPDATEDDAY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDDAYS = "CONTENTMANAGER_LASTUPDATEDDAYS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDWEEK = "CONTENTMANAGER_LASTUPDATEDWEEK";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDWEEKS = "CONTENTMANAGER_LASTUPDATEDWEEKS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDMONTH = "CONTENTMANAGER_LASTUPDATEDMONTH";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDMONTHS = "CONTENTMANAGER_LASTUPDATEDMONTHS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDYEAR = "CONTENTMANAGER_LASTUPDATEDYEAR";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_LASTUPDATEDYEARS = "CONTENTMANAGER_LASTUPDATEDYEARS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_TIMEUPDATED = "CONTENTMANAGER_TIMEUPDATED";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_TIMECREATED = "CONTENTMANAGER_TIMECREATED";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_USINGCLOUD = "CONTENTMANAGER_USINGCLOUD";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string WORKSHOP_COLORCORRECTIONS = "WORKSHOP_COLORCORRECTIONS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_SUBS = "CONTENTMANAGER_SUBS";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_ADDCATEGORY = "CONTENTMANAGER_ADDCATEGORY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_RENAMECATEGORY = "CONTENTMANAGER_RENAMECATEGORY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_REMOVECATEGORY = "CONTENTMANAGER_REMOVECATEGORY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = true)]
	public const string CONFIRM_REMOVECATEGORY = "CONFIRM_REMOVECATEGORY";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string ASSETEDITOR_SELECTREFERENCE_DESC = "ASSETEDITOR_SELECTREFERENCE_DESC";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_ASSETWARNING = "CONTENTMANAGER_ASSETWARNING";

	[Locale(file = "en-patch1.0.8", category = "Menus", hideInEditor = false)]
	public const string CONTENTMANAGER_ASSETVERSIONWARNING = "CONTENTMANAGER_ASSETVERSIONWARNING";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = false)]
	public const string NEWGAME_THEME = "NEWGAME_THEME";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = false)]
	public const string LOADMAP_THEME = "LOADMAP_THEME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string BUILTIN_COLORCORRECTION = "BUILTIN_COLORCORRECTION";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = false)]
	public const string ASSETEDITOR_PROPERTIES = "ASSETEDITOR_PROPERTIES";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = true)]
	public const string CONFIRM_RESETUNIQUEBUILDINGS = "CONFIRM_RESETUNIQUEBUILDINGS";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = false)]
	public const string ASSETIMPORTER_TEMPLATE = "ASSETIMPORTER_TEMPLATE";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = true)]
	public const string EXCEPTIONTITLE = "EXCEPTIONTITLE";

	[Locale(file = "en-patch1.0.9", category = "Menus", hideInEditor = true)]
	public const string EXCEPTIONMESSAGE = "EXCEPTIONMESSAGE";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_MAPTHEME = "LOADPANEL_MAPTHEME";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHAV = "LOADPANEL_ACHAV";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_ENABLED = "LOADPANEL_ACHSTATUS_ENABLED";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_DISABLED = "LOADPANEL_ACHSTATUS_DISABLED";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_MODSACTIVE = "LOADPANEL_ACHSTATUS_MODSACTIVE";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_WORKSHOP = "LOADPANEL_ACHSTATUS_WORKSHOP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_PREVMAP = "LOADPANEL_ACHSTATUS_PREVMAP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string CONTENT_UNSUBALL = "CONTENT_UNSUBALL";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string CONTENT_ENABLEALL = "CONTENT_ENABLEALL";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string CONTENT_DISABLEALL = "CONTENT_DISABLEALL";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = true)]
	public const string PLEASEWAIT_DIALOG = "PLEASEWAIT_DIALOG";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string NO_TOOLTIPIMAGES = "NO_TOOLTIPIMAGES";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_AUTOSAVE = "OPTIONS_AUTOSAVE";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_AUTOSAVE_TIME = "OPTIONS_AUTOSAVE_TIME";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_MISC_RESETKEYMAPPING = "OPTIONS_MISC_RESETKEYMAPPING";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = true)]
	public const string CONFIRM_RESETKEYMAPPING = "CONFIRM_RESETKEYMAPPING";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_DISPLAYGROUP = "OPTIONS_DISPLAYGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_QUALITYGROUP = "OPTIONS_QUALITYGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_PREFERENCEGROUP = "OPTIONS_PREFERENCEGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_SYSTEMSETTINGS = "OPTIONS_SYSTEMSETTINGS";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_MODSSETTINGS = "OPTIONS_MODSSETTINGS";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_AUDIOGROUP = "OPTIONS_AUDIOGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_CONTROLSGROUP = "OPTIONS_CONTROLSGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_KEYMAPPINGGROUP = "OPTIONS_KEYMAPPINGGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_INGAMEGROUP = "OPTIONS_INGAMEGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_LANGUAGEGROUP = "OPTIONS_LANGUAGEGROUP";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_SHOWTUTORIALMESSAGES = "OPTIONS_SHOWTUTORIALMESSAGES";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_KEYMAPPING = "OPTIONS_KEYMAPPING";

	[Locale(file = "en-patch1.1.1", category = "Menus", hideInEditor = false)]
	public const string OPTIONS_MISCGROUP = "OPTIONS_MISCGROUP";

	[Locale(file = "en-patch1.2.1", category = "Menus", hideInEditor = false)]
	public const string INCOME_TT_COMMERCIALSPEC = "INCOME_TT_COMMERCIALSPEC";

	[Locale(file = "en-patch1.2.1", category = "Menus", hideInEditor = false)]
	public const string CONTENT_AD_REQUIRED = "CONTENT_AD_REQUIRED";

	[Locale(file = "en-patch1.2.1", category = "Menus", hideInEditor = false)]
	public const string CONTENT_AD_BUY = "CONTENT_AD_BUY";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWSFEED_REFRESHING = "NEWSFEED_REFRESHING";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWSFEED_ERROR = "NEWSFEED_ERROR";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWSFEED_WELCOME = "NEWSFEED_WELCOME";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string DLCPANEL_ORIENTALBUILDINGS = "DLCPANEL_ORIENTALBUILDINGS";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string PDX_NEWSLETTER = "PDX_NEWSLETTER";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWGAME_NOMAPS = "NEWGAME_NOMAPS";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string INFO_ESCAPE_BUILDINGS = "INFO_ESCAPE_BUILDINGS";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string INFO_ESCAPE_EVACUATED = "INFO_ESCAPE_EVACUATED";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string INFO_ESCAPE_NOTEVACUATED = "INFO_ESCAPE_NOTEVACUATED";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string STORYMESSAGE_RESTART = "STORYMESSAGE_RESTART";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string WARNINGPANEL_NOSHELTERS_TOOLTIP = "WARNINGPANEL_NOSHELTERS_TOOLTIP";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWSCENARIO_SAVEGAME = "NEWSCENARIO_SAVEGAME";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string NEWSCENARIO_MAP = "NEWSCENARIO_MAP";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string STORYMESSAGE_CONTINUE = "STORYMESSAGE_CONTINUE";

	[Locale(file = "en-postExpansion3", category = "Menus", hideInEditor = false)]
	public const string PANEL_SORT_AUTHOR = "PANEL_SORT_AUTHOR";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string STADIUMNAME = "STADIUMNAME";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string STADIUMYEAR = "STADIUMYEAR";

	[Locale(file = "en-stadiums", category = "Sheet1", hideInEditor = true)]
	public const string STADIUMCAPACITY = "STADIUMCAPACITY";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADING_TIP_TITLE_TGP = "LOADING_TIP_TITLE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADING_TIP_TEXT_TGP = "LOADING_TIP_TEXT_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_WORKSHOP_TGP = "WORKSHOP_WORKSHOP_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_ACHIEVEMENTS_TGP = "CONTENTMANAGER_ACHIEVEMENTS_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMOD_TGP = "WORKSHOP_PUBLISHMOD_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSAVE_TGP = "WORKSHOP_PUBLISHSAVE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMAP_TGP = "WORKSHOP_PUBLISHMAP_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHASSET_TGP = "WORKSHOP_PUBLISHASSET_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_STEAMWORKSHOP_TGP = "CONTENTMANAGER_STEAMWORKSHOP_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string SAVEMAP_USECLOUD_TGP = "SAVEMAP_USECLOUD_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string SAVEGAME_CLOUDUNSUPPORTED_TGP = "SAVEGAME_CLOUDUNSUPPORTED_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_CONFIRM_WORKSHOPDELETE_TGP = "CONTENT_CONFIRM_WORKSHOPDELETE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED_UPDATE_TGP = "CONTENT_MAPPUBLISHED_UPDATE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED_TGP = "CONTENT_MAPPUBLISHED_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_VIEW_TGP = "CONTENT_VIEW_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_TITLE_TGP = "WORKSHOP_TITLE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_DESC_TGP = "WORKSHOP_DESC_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_USINGCLOUD_TGP = "CONTENTMANAGER_USINGCLOUD_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_AD_BUY_TGP = "CONTENT_AD_BUY_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_WORKSHOP_TGP = "LOADPANEL_ACHSTATUS_WORKSHOP_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONFIRM_UNSUBSCRIBEALL_TGP = "CONFIRM_UNSUBSCRIBEALL_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_SUBS_TGP = "CONTENTMANAGER_SUBS_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSTYLE_TGP = "WORKSHOP_PUBLISHSTYLE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string DISCLAIMER_MESSAGE_TGP = "DISCLAIMER_MESSAGE_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_SF_BUY_TGP = "CONTENT_SF_BUY_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string NEWMAP_UNPUBLISHED_THEME_TGP = "NEWMAP_UNPUBLISHED_THEME_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_ND_BUY_TGP = "CONTENT_ND_BUY_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADING_TIP_TITLE_QQ = "LOADING_TIP_TITLE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADING_TIP_TEXT_QQ = "LOADING_TIP_TEXT_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_WORKSHOP_QQ = "WORKSHOP_WORKSHOP_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_ACHIEVEMENTS_QQ = "CONTENTMANAGER_ACHIEVEMENTS_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMOD_QQ = "WORKSHOP_PUBLISHMOD_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSAVE_QQ = "WORKSHOP_PUBLISHSAVE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMAP_QQ = "WORKSHOP_PUBLISHMAP_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHASSET_QQ = "WORKSHOP_PUBLISHASSET_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_STEAMWORKSHOP_QQ = "CONTENTMANAGER_STEAMWORKSHOP_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string SAVEMAP_USECLOUD_QQ = "SAVEMAP_USECLOUD_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string SAVEGAME_CLOUDUNSUPPORTED_QQ = "SAVEGAME_CLOUDUNSUPPORTED_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_CONFIRM_WORKSHOPDELETE_QQ = "CONTENT_CONFIRM_WORKSHOPDELETE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED_UPDATE_QQ = "CONTENT_MAPPUBLISHED_UPDATE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED_QQ = "CONTENT_MAPPUBLISHED_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_VIEW_QQ = "CONTENT_VIEW_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_TITLE_QQ = "WORKSHOP_TITLE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_DESC_QQ = "WORKSHOP_DESC_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_USINGCLOUD_QQ = "CONTENTMANAGER_USINGCLOUD_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_AD_BUY_QQ = "CONTENT_AD_BUY_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string LOADPANEL_ACHSTATUS_WORKSHOP_QQ = "LOADPANEL_ACHSTATUS_WORKSHOP_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONFIRM_UNSUBSCRIBEALL_QQ = "CONFIRM_UNSUBSCRIBEALL_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENTMANAGER_SUBS_QQ = "CONTENTMANAGER_SUBS_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSTYLE_QQ = "WORKSHOP_PUBLISHSTYLE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string DISCLAIMER_MESSAGE_QQ = "DISCLAIMER_MESSAGE_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_SF_BUY_QQ = "CONTENT_SF_BUY_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string NEWMAP_UNPUBLISHED_THEME_QQ = "NEWMAP_UNPUBLISHED_THEME_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_ND_BUY_QQ = "CONTENT_ND_BUY_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_GC_BUY_TGP = "CONTENT_GC_BUY_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MT_BUY_TGP = "CONTENT_MT_BUY_TGP";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_GC_BUY_QQ = "CONTENT_GC_BUY_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string CONTENT_MT_BUY_QQ = "CONTENT_MT_BUY_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string NEWSFEED_ERROR_QQ = "NEWSFEED_ERROR_QQ";

	[Locale(file = "en-tencent", category = "Sheet1", hideInEditor = false)]
	public const string NEWSFEED_ERROR_TGP = "NEWSFEED_ERROR_TGP";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_EMAIL = "PDXACCOUNT_EMAIL";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_PASSWORD = "PDXACCOUNT_PASSWORD";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_DATEOFBIRTH = "PDXACCOUNT_DATEOFBIRTH";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_COUNTRY = "PDXACCOUNT_COUNTRY";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_LANGUAGE = "PDXACCOUNT_LANGUAGE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_FIRSTNAME = "PDXACCOUNT_FIRSTNAME";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_LASTNAME = "PDXACCOUNT_LASTNAME";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_ADDRESS = "PDXACCOUNT_ADDRESS";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_CITY = "PDXACCOUNT_CITY";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_ZIPCODE = "PDXACCOUNT_ZIPCODE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_STATE = "PDXACCOUNT_STATE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_PHONE = "PDXACCOUNT_PHONE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_REQUIRED = "PDXACCOUNT_REQUIRED";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_CREATE = "PDXACCOUNT_CREATE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_CAPTION = "PDXACCOUNT_CAPTION";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_CAPTION = "PDXLOGIN_CAPTION";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_USERNAME = "PDXLOGIN_USERNAME";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_PASSWORD = "PDXLOGIN_PASSWORD";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_LOGIN = "PDXLOGIN_LOGIN";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_CREATE = "PDXLOGIN_CREATE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXLOGIN_NEVERFORGET = "PDXLOGIN_NEVERFORGET";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDX_LEGAL = "PDX_LEGAL";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDX_LEGAL_AND = "PDX_LEGAL_AND";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDX_TERMSOFUSE = "PDX_TERMSOFUSE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDX_PRIVACY = "PDX_PRIVACY";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_CONGRATS_TITLE = "PDXACCOUNT_CONGRATS_TITLE";

	[Locale(file = "en-ui", category = "Paradox Account", hideInEditor = false)]
	public const string PDXACCOUNT_CONGRATS = "PDXACCOUNT_CONGRATS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LANGUAGE = "LANGUAGE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LANGUAGE_ENGLISH = "LANGUAGE_ENGLISH";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string LOADSTATUS = "LOADSTATUS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string PAUSEMENU_TITLE = "PAUSEMENU_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string TOOLSMENU_MAPEDITOR = "TOOLSMENU_MAPEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string TOOLSMENU_ASSETEDITOR = "TOOLSMENU_ASSETEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string TOOLSMENU_BACK = "TOOLSMENU_BACK";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string TOOLSMENU_LOAD = "TOOLSMENU_LOAD";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string TOOLSMENU_NEW = "TOOLSMENU_NEW";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_VIEW = "CONTENT_VIEW";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_ONOFF = "CONTENT_ONOFF";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_TOGGLEALL = "CONTENT_TOGGLEALL";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_SHARE = "CONTENT_SHARE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_UPDATE = "CONTENT_UPDATE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED_UPDATE = "CONTENT_MAPPUBLISHED_UPDATE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_MAPPUBLISHED = "CONTENT_MAPPUBLISHED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_MAPUNPUBLISHED = "CONTENT_MAPUNPUBLISHED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_DELETE = "CONTENT_DELETE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENT_UNSUBSCRIBE = "CONTENT_UNSUBSCRIBE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONTENT_CONFIRM_DELETE = "CONTENT_CONFIRM_DELETE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONTENT_CONFIRM_WORKSHOPDELETE = "CONTENT_CONFIRM_WORKSHOPDELETE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_SAVEDELETE = "CONFIRM_SAVEDELETE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_SAVEOVERRIDE = "CONFIRM_SAVEOVERRIDE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_BUILDINGDELETE = "CONFIRM_BUILDINGDELETE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_REBINDKEY = "CONFIRM_REBINDKEY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string MAINMENU_CONFIRM_EXITGAME = "MAINMENU_CONFIRM_EXITGAME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRMEXIT_ASSETEDITOR = "CONFIRMEXIT_ASSETEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRMEXIT_MAPEDITOR = "CONFIRMEXIT_MAPEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRMEXIT = "CONFIRMEXIT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONFIRMEXIT_TOMAINMENU = "CONFIRMEXIT_TOMAINMENU";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONFIRMEXIT_TODESKTOP = "CONFIRMEXIT_TODESKTOP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_NEWRESOLUTION = "CONFIRM_NEWRESOLUTION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string CONFIRM_RESETCONFIGURATION = "CONFIRM_RESETCONFIGURATION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVEGAME_CLOUDUNSUPPORTED = "SAVEGAME_CLOUDUNSUPPORTED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVEGAME_TITLE = "SAVEGAME_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LOADGAME_TITLE = "LOADGAME_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string PACKAGEAUTHOR = "PACKAGEAUTHOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWGAME_INVERTTRAFFIC = "NEWGAME_INVERTTRAFFIC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWGAME_TITLE = "NEWGAME_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWGAME_CHOOSEMAP = "NEWGAME_CHOOSEMAP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVEMAP_TITLE = "SAVEMAP_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LOADMAP_TITLE = "LOADMAP_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVEMAP_USECLOUD = "SAVEMAP_USECLOUD";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWMAP_TITLE = "NEWMAP_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWMAP_CHOOSETHEME = "NEWMAP_CHOOSETHEME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWMAP_NOTHEME = "NEWMAP_NOTHEME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVEASSET_TITLE = "SAVEASSET_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LOADASSET_TITLE = "LOADASSET_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWASSET_TITLE = "NEWASSET_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CITY_NAME = "CITY_NAME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string START = "START";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string YES = "YES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NO = "NO";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CANCEL = "CANCEL";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SELECT = "SELECT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CREATE = "CREATE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string LOAD = "LOAD";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string SAVE = "SAVE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string EXCEPTION_TITLE = "EXCEPTION_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string EXCEPTION_OK = "EXCEPTION_OK";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string EXCEPTION_COPY = "EXCEPTION_COPY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CLOSE = "CLOSE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string STATS_TITLE = "STATS_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_TITLE = "CONTENTMANAGER_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_CONTENT = "CONTENTMANAGER_CONTENT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_PACKAGES = "CONTENTMANAGER_PACKAGES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_SAVEGAMES = "CONTENTMANAGER_SAVEGAMES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_MAPS = "CONTENTMANAGER_MAPS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_MODS = "CONTENTMANAGER_MODS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_STEAMWORKSHOP = "CONTENTMANAGER_STEAMWORKSHOP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_ASSETS = "CONTENTMANAGER_ASSETS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_INCLUDESOURCE = "WORKSHOP_INCLUDESOURCE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMOD = "WORKSHOP_PUBLISHMOD";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHSAVE = "WORKSHOP_PUBLISHSAVE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHMAP = "WORKSHOP_PUBLISHMAP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_PUBLISHASSET = "WORKSHOP_PUBLISHASSET";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_TITLE = "WORKSHOP_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_DESC = "WORKSHOP_DESC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_UPDATE = "WORKSHOP_UPDATE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_SHARE = "WORKSHOP_SHARE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string WORKSHOP_UPDATESTATUS = "WORKSHOP_UPDATESTATUS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_CHANGENOTE = "WORKSHOP_CHANGENOTE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_LEGAL = "WORKSHOP_LEGAL";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_LEGALLINK = "WORKSHOP_LEGALLINK";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string DEBUG_CAPTION = "DEBUG_CAPTION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string DEBUG_CLEAR = "DEBUG_CLEAR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_TITLE = "OPTIONS_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MISC_RESETUNLOCKING = "OPTIONS_MISC_RESETUNLOCKING";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MISC_RESETCONF = "OPTIONS_MISC_RESETCONF";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MISC_LOGOUTPDX = "OPTIONS_MISC_LOGOUTPDX";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_LANGUAGE = "OPTIONS_LANGUAGE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_AUTOEXPAND_CHIRPER = "OPTIONS_AUTOEXPAND_CHIRPER";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_TUTORIAL_MESSAGES = "OPTIONS_TUTORIAL_MESSAGES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_GRAPHICS = "OPTIONS_GRAPHICS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_GAMEPLAY = "OPTIONS_GAMEPLAY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MISC = "OPTIONS_MISC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_AUDIO = "OPTIONS_AUDIO";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_ASPECTRATIO = "OPTIONS_ASPECTRATIO";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_RESOLUTION = "OPTIONS_RESOLUTION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_DISPLAYMODE = "OPTIONS_DISPLAYMODE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_APPLY = "OPTIONS_APPLY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_EDGESCROLLING = "OPTIONS_EDGESCROLLING";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_EDGESCROLLINGSENSITIVITY = "OPTIONS_EDGESCROLLINGSENSITIVITY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MOUSESENSITIVITY = "OPTIONS_MOUSESENSITIVITY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_KM_SHARED = "OPTIONS_KM_SHARED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_KM_GAME = "OPTIONS_KM_GAME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_KM_MAPEDITOR = "OPTIONS_KM_MAPEDITOR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_KM_DECORATION = "OPTIONS_KM_DECORATION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MAINVOLUME = "OPTIONS_MAINVOLUME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_AMBIENTVOLUME = "OPTIONS_AMBIENTVOLUME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MUSICVOLUME = "OPTIONS_MUSICVOLUME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_EFFECTVOLUME = "OPTIONS_EFFECTVOLUME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_UIVOLUME = "OPTIONS_UIVOLUME";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_TILTSHIFTAMOUNT = "OPTIONS_TILTSHIFTAMOUNT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_COLORCORRECTION = "OPTIONS_COLORCORRECTION";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_SHADOWSQUALITY = "OPTIONS_SHADOWSQUALITY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_SHADOWSDISTANCE = "OPTIONS_SHADOWSDISTANCE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_ANTIALIASING = "OPTIONS_ANTIALIASING";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_TEXTURESQUALITY = "OPTIONS_TEXTURESQUALITY";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_TEXTURESANISO = "OPTIONS_TEXTURESANISO";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_BRIGHTNESS = "OPTIONS_BRIGHTNESS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_VSYNC = "OPTIONS_VSYNC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_LOD = "OPTIONS_LOD";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_ENABLED = "OPTIONS_ENABLED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_DISABLED = "OPTIONS_DISABLED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_LOW = "OPTIONS_LOW";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_MEDIUM = "OPTIONS_MEDIUM";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_HIGH = "OPTIONS_HIGH";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_VERYHIGH = "OPTIONS_VERYHIGH";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_VERYSHORT = "OPTIONS_VERYSHORT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_SHORT = "OPTIONS_SHORT";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_FAR = "OPTIONS_FAR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string OPTIONS_VERYFAR = "OPTIONS_VERYFAR";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string ASPECTRATIO_NORMAL = "ASPECTRATIO_NORMAL";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string ASPECTRATIO_WIDESCREEN = "ASPECTRATIO_WIDESCREEN";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string ASPECTRATIO_WIDESCREEN2 = "ASPECTRATIO_WIDESCREEN2";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string ASPECTRATIO_WIDESCREEN3 = "ASPECTRATIO_WIDESCREEN3";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string DISPLAYMODE_WINDOWED = "DISPLAYMODE_WINDOWED";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string DISPLAYMODE_FULLSCREEN = "DISPLAYMODE_FULLSCREEN";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string KEYMAPPING_MULTIPLE = "KEYMAPPING_MULTIPLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string NEWSFEED_TITLE = "NEWSFEED_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string STORE_TITLE = "STORE_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_GUIDE = "CONTENTMANAGER_GUIDE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_ACHIEVEMENTS = "CONTENTMANAGER_ACHIEVEMENTS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_MAPS = "WORKSHOP_MAPS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_SAVES = "WORKSHOP_SAVES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_REPLACE = "CONTENTMANAGER_REPLACE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string CONTENTMANAGER_REPLACE_TOOLTIP = "CONTENTMANAGER_REPLACE_TOOLTIP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_ASSETS = "WORKSHOP_ASSETS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_MODS = "WORKSHOP_MODS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string OPERATION_IN_PROGRESS = "OPERATION_IN_PROGRESS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string WORKSHOP_WORKSHOP = "WORKSHOP_WORKSHOP";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string MOD_ENABLED_STATUS = "MOD_ENABLED_STATUS";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string MOD_TITLE = "MOD_TITLE";

	[Locale(file = "en-ui", category = "Main", hideInEditor = true)]
	public const string MOD_DESC = "MOD_DESC";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string MOD_INTERFACES = "MOD_INTERFACES";

	[Locale(file = "en-ui", category = "Main", hideInEditor = false)]
	public const string MOD_INTERFACES_CONFLICT = "MOD_INTERFACES_CONFLICT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MONEY_FORMAT = "MONEY_FORMAT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MONEY_FORMATNOCENTS = "MONEY_FORMATNOCENTS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MONEY_LOCALE = "MONEY_LOCALE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MONEY_CURRENCY = "MONEY_CURRENCY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string VALUE_PERCENTAGE = "VALUE_PERCENTAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_MONEYINFO = "MAIN_MONEYINFO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_MONEYDELTA = "MAIN_MONEYDELTA";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_PLAYPAUSE = "MAIN_PLAYPAUSE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_DATE = "MAIN_DATE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_SPEED = "MAIN_SPEED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_AREAS = "MAIN_AREAS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_UNLOCKING = "MAIN_UNLOCKING";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_CITYINFO = "MAIN_CITYINFO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_ZONING_DEMAND = "MAIN_ZONING_DEMAND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string MAIN_RESIDENTIAL_DEMAND = "MAIN_RESIDENTIAL_DEMAND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string MAIN_COMMERCIAL_DEMAND = "MAIN_COMMERCIAL_DEMAND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string MAIN_INDUSTRYOFFICE_DEMAND = "MAIN_INDUSTRYOFFICE_DEMAND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_HAPPINESS = "MAIN_HAPPINESS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_POPULATION = "MAIN_POPULATION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_POPULATIONDELTA = "MAIN_POPULATIONDELTA";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_FREECAMERA = "MAIN_FREECAMERA";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_INFOVIEWS = "MAIN_INFOVIEWS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string MAIN_ADVISOR = "MAIN_ADVISOR";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ROAD_OPTION_STRAIGHT = "ROAD_OPTION_STRAIGHT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ROAD_OPTION_CURVED = "ROAD_OPTION_CURVED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ROAD_OPTION_FREEFORM = "ROAD_OPTION_FREEFORM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ROAD_OPTION_UPGRADE = "ROAD_OPTION_UPGRADE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ROAD_OPTION_TOGGLESNAPPING = "ROAD_OPTION_TOGGLESNAPPING";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string DISTRICT_OPTION_BRUSHSMALL = "DISTRICT_OPTION_BRUSHSMALL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string DISTRICT_OPTION_BRUSHMEDIUM = "DISTRICT_OPTION_BRUSHMEDIUM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string DISTRICT_OPTION_BRUSHLARGE = "DISTRICT_OPTION_BRUSHLARGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ZONING_OPTION_FILL = "ZONING_OPTION_FILL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ZONING_OPTION_MARQUEE = "ZONING_OPTION_MARQUEE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ZONING_OPTION_BRUSHSMALL = "ZONING_OPTION_BRUSHSMALL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ZONING_OPTION_BRUSHMEDIUM = "ZONING_OPTION_BRUSHMEDIUM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string ZONING_OPTION_BRUSHLARGE = "ZONING_OPTION_BRUSHLARGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string PATH_OPTION_STRAIGHT = "PATH_OPTION_STRAIGHT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string PATH_OPTION_CURVED = "PATH_OPTION_CURVED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string PATH_OPTION_FREEFORM = "PATH_OPTION_FREEFORM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string PATH_OPTION_UPGRADE = "PATH_OPTION_UPGRADE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TUNNEL_OPTION_STRAIGHT = "TUNNEL_OPTION_STRAIGHT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TUNNEL_OPTION_CURVED = "TUNNEL_OPTION_CURVED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TUNNEL_OPTION_FREEFORM = "TUNNEL_OPTION_FREEFORM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TUNNEL_OPTION_UPGRADE = "TUNNEL_OPTION_UPGRADE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TRACK_OPTION_STRAIGHT = "TRACK_OPTION_STRAIGHT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TRACK_OPTION_CURVED = "TRACK_OPTION_CURVED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TRACK_OPTION_FREEFORM = "TRACK_OPTION_FREEFORM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string TRACK_OPTION_UPGRADE = "TRACK_OPTION_UPGRADE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string ZONING_TITLE = "ZONING_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = true)]
	public const string ZONING_DESC = "ZONING_DESC";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CRIMERATE_TITLE = "INFO_CRIMERATE_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CRIMERATE_METER = "INFO_CRIMERATE_METER";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CRIMERATE_COVERAGE = "INFO_CRIMERATE_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CRIMERATE_BUILDINGS = "INFO_CRIMERATE_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CRIMERATE_RATELEGEND = "INFO_CRIMERATE_RATELEGEND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEGEND = "INFO_LEGEND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HIGH = "INFO_HIGH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LOW = "INFO_LOW";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ACTIVE = "INFO_ACTIVE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_INACTIVE = "INFO_INACTIVE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPINESS_TITLE = "INFO_HAPPINESS_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPINESS_RESIDENTIAL = "INFO_HAPPINESS_RESIDENTIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPINESS_COMMERCIAL = "INFO_HAPPINESS_COMMERCIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPINESS_OFFICE = "INFO_HAPPINESS_OFFICE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPINESS_INDUSTRIAL = "INFO_HAPPINESS_INDUSTRIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HAPPY = "INFO_HAPPY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_UNHAPPY = "INFO_UNHAPPY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POLLUTION_TITLE = "INFO_POLLUTION_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POLLUTION_GROUND = "INFO_POLLUTION_GROUND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POLLUTION_WATER = "INFO_POLLUTION_WATER";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_NOISEPOLLUTION_TITLE = "INFO_NOISEPOLLUTION_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_NOISEPOLLUTION_AVERAGE = "INFO_NOISEPOLLUTION_AVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WIND_TITLE = "INFO_WIND_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WIND_CALM = "INFO_WIND_CALM";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WIND_STRONG = "INFO_WIND_STRONG";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WIND_BUILDINGS = "INFO_WIND_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WIND_STRENGTH = "INFO_WIND_STRENGTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_TRAFFIC_TITLE = "INFO_TRAFFIC_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LANDVALUE_TITLE = "INFO_LANDVALUE_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LANDVALUE_AVERAGE = "INFO_LANDVALUE_AVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_TITLE = "INFO_POPULATION_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_POPULATION = "INFO_POPULATION_POPULATION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_WORKERS = "INFO_POPULATION_WORKERS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_WORKPLACES = "INFO_POPULATION_WORKPLACES";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_UNEMPLOYMENT = "INFO_POPULATION_UNEMPLOYMENT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_DENSITY = "INFO_POPULATION_DENSITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_CHILD = "INFO_POPULATION_CHILD";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_TEEN = "INFO_POPULATION_TEEN";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_YOUNG = "INFO_POPULATION_YOUNG";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_ADULT = "INFO_POPULATION_ADULT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_SENIOR = "INFO_POPULATION_SENIOR";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_BIRTHS = "INFO_POPULATION_BIRTHS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_DEATHS = "INFO_POPULATION_DEATHS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_FAMILIES = "INFO_POPULATION_FAMILIES";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_SENIORS = "INFO_POPULATION_SENIORS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_POPULATION_ADULTS = "INFO_POPULATION_ADULTS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_TITLE = "INFO_RES_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_AVAILABLE = "INFO_RES_AVAILABLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_USED = "INFO_RES_USED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_OIL = "INFO_RES_OIL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_OILAMOUNT = "INFO_RES_OILAMOUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_OILUSED = "INFO_RES_OILUSED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_ORE = "INFO_RES_ORE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_OREAMOUNT = "INFO_RES_OREAMOUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_OREUSED = "INFO_RES_OREUSED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FOREST = "INFO_RES_FOREST";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FORESTAMOUNT = "INFO_RES_FORESTAMOUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FORESTUSED = "INFO_RES_FORESTUSED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FERTILELAND = "INFO_RES_FERTILELAND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FERTILELANDAMOUNT = "INFO_RES_FERTILELANDAMOUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_RES_FERTILELANDUSED = "INFO_RES_FERTILELANDUSED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_TITLE = "INFO_HEALTH_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_HEALTHCARE = "INFO_HEALTH_HEALTHCARE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_DEATHCARE = "INFO_HEALTH_DEATHCARE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_AVERAGE = "INFO_HEALTH_AVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_HEALTH = "INFO_HEALTH_HEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_COVERAGE = "INFO_HEALTH_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_BUILDINGS = "INFO_HEALTH_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_SICKS = "INFO_HEALTH_SICKS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_HEALTHCARE_AVAILABILITY = "INFO_HEALTH_HEALTHCARE_AVAILABILITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_HEALCAPACITY = "INFO_HEALTH_HEALCAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_CEMETARYUSAGE = "INFO_HEALTH_CEMETARYUSAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_DEADS = "INFO_HEALTH_DEADS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_CEMETARYCAPACITY = "INFO_HEALTH_CEMETARYCAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_CREMATORIUMAVAILABILITY = "INFO_HEALTH_CREMATORIUMAVAILABILITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_DECEASED = "INFO_HEALTH_DECEASED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_HEALTH_CREMATORIUMCAPACITY = "INFO_HEALTH_CREMATORIUMCAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_TITLE = "INFO_ELECTRICITY_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_AVAILABILITY = "INFO_ELECTRICITY_AVAILABILITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_CONSUMPTION = "INFO_ELECTRICITY_CONSUMPTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_PRODUCTION = "INFO_ELECTRICITY_PRODUCTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_PLANTS = "INFO_ELECTRICITY_PLANTS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_BUILDINGS = "INFO_ELECTRICITY_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_CONNECTED = "INFO_ELECTRICITY_CONNECTED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ELECTRICITY_DISCONNECTED = "INFO_ELECTRICITY_DISCONNECTED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_TITLE = "INFO_WATER_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_WATERAVAILABILITY = "INFO_WATER_WATERAVAILABILITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_SEWAGEAVAILABILITY = "INFO_WATER_SEWAGEAVAILABILITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_POLLUTION = "INFO_WATER_POLLUTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_WATERFACILITIES = "INFO_WATER_WATERFACILITIES";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_SEWAGEFACILITIES = "INFO_WATER_SEWAGEFACILITIES";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_BUILDINGS = "INFO_WATER_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_WATERANDSEWAGE = "INFO_WATER_WATERANDSEWAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_WATERONLY = "INFO_WATER_WATERONLY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_NOWATERNORSEWAGE = "INFO_WATER_NOWATERNORSEWAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_CONSUMPTION = "INFO_WATER_CONSUMPTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_PRODUCTION = "INFO_WATER_PRODUCTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_SEWAGECAPACITY = "INFO_WATER_SEWAGECAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_WATER_SEWAGEACCUMULATION = "INFO_WATER_SEWAGEACCUMULATION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_TITLE = "INFO_EDUCATION_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_ELEMENTARY = "INFO_EDUCATION_ELEMENTARY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_HIGH = "INFO_EDUCATION_HIGH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_UNIVERSITY = "INFO_EDUCATION_UNIVERSITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_AVAILABILITY1 = "INFO_EDUCATION_AVAILABILITY1";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_AVAILABILITY2 = "INFO_EDUCATION_AVAILABILITY2";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_AVAILABILITY3 = "INFO_EDUCATION_AVAILABILITY3";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_ELIGIBLE = "INFO_EDUCATION_ELIGIBLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_CAPACITY = "INFO_EDUCATION_CAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_GRADUATED = "INFO_EDUCATION_GRADUATED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_SCHOOLS = "INFO_EDUCATION_SCHOOLS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_COVERAGE = "INFO_EDUCATION_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_EDUCATION_GRADUATEPERCENT = "INFO_EDUCATION_GRADUATEPERCENT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_FIRE_TITLE = "INFO_FIRE_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_FIRE_METER = "INFO_FIRE_METER";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_FIRE_SAFETY = "INFO_FIRE_SAFETY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_FIRE_COVERAGE = "INFO_FIRE_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_FIRE_FIRESTATIONS = "INFO_FIRE_FIRESTATIONS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_TITLE = "INFO_GARBAGE_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_LANDFILL = "INFO_GARBAGE_LANDFILL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_INCINERATOR = "INFO_GARBAGE_INCINERATOR";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_LANDFILLSTORAGE = "INFO_GARBAGE_LANDFILLSTORAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_LANDFILLCAPACITY = "INFO_GARBAGE_LANDFILLCAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_INCINERATORCAPACITY = "INFO_GARBAGE_INCINERATORCAPACITY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_GARBAGEPRODUCTION = "INFO_GARBAGE_GARBAGEPRODUCTION";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_BUILDINGS = "INFO_GARBAGE_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_COVERAGE = "INFO_GARBAGE_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_GARBAGE_AMOUNT = "INFO_GARBAGE_AMOUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_TITLE = "INFO_CONNECTIONS_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_TOURISTS = "INFO_CONNECTIONS_TOURISTS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_IMPORT = "INFO_CONNECTIONS_IMPORT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_EXPORT = "INFO_CONNECTIONS_EXPORT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_IMPORTTOTAL = "INFO_CONNECTIONS_IMPORTTOTAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_EXPORTTOTAL = "INFO_CONNECTIONS_EXPORTTOTAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_OIL = "INFO_CONNECTIONS_OIL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_ORE = "INFO_CONNECTIONS_ORE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_FORESTRY = "INFO_CONNECTIONS_FORESTRY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_GOODS = "INFO_CONNECTIONS_GOODS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_AGRICULTURE = "INFO_CONNECTIONS_AGRICULTURE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_IMPORTRATIO = "INFO_CONNECTIONS_IMPORTRATIO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_EXPORTRATIO = "INFO_CONNECTIONS_EXPORTRATIO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_TOURISTSTOTAL = "INFO_CONNECTIONS_TOURISTSTOTAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_TOURISTSWEALTH = "INFO_CONNECTIONS_TOURISTSWEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_LOWWEALTH = "INFO_CONNECTIONS_LOWWEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_MEDIUMWEALTH = "INFO_CONNECTIONS_MEDIUMWEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_CONNECTIONS_HIGHWEALTH = "INFO_CONNECTIONS_HIGHWEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_TITLE = "INFO_AGE_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_CHILD = "INFO_AGE_CHILD";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_TEEN = "INFO_AGE_TEEN";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_YOUNG = "INFO_AGE_YOUNG";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_ADULT = "INFO_AGE_ADULT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_AGE_SENIOR = "INFO_AGE_SENIOR";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TITLE = "INFO_PUBLICTRANSPORT_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_CITIZENS = "INFO_PUBLICTRANSPORT_CITIZENS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TOURISTS = "INFO_PUBLICTRANSPORT_TOURISTS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_BUS = "INFO_PUBLICTRANSPORT_BUS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_METRO = "INFO_PUBLICTRANSPORT_METRO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TRAIN = "INFO_PUBLICTRANSPORT_TRAIN";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_SHIP = "INFO_PUBLICTRANSPORT_SHIP";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_PLANE = "INFO_PUBLICTRANSPORT_PLANE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_COUNT = "INFO_PUBLICTRANSPORT_COUNT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_PUBLICTRANSPORT_TOTAL = "INFO_PUBLICTRANSPORT_TOTAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEVELS_TITLE = "INFO_LEVELS_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEVELS_RESIDENTIAL = "INFO_LEVELS_RESIDENTIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEVELS_COMMERCIAL = "INFO_LEVELS_COMMERCIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEVELS_OFFICES = "INFO_LEVELS_OFFICES";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_LEVELS_INDUSTRIAL = "INFO_LEVELS_INDUSTRIAL";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_LOWTECH = "LEVELUP_LOWTECH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_LOWLANDVALUE = "LEVELUP_LOWLANDVALUE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_DISTRESS = "LEVELUP_DISTRESS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_RESIDENTIAL_HAPPY = "LEVELUP_RESIDENTIAL_HAPPY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_SERVICES_NEEDED = "LEVELUP_SERVICES_NEEDED";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_WORKERS_HAPPY = "LEVELUP_WORKERS_HAPPY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_COMMERCIAL_HAPPY = "LEVELUP_COMMERCIAL_HAPPY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_IMPOSSIBLE = "LEVELUP_IMPOSSIBLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_NOINFO = "LEVELUP_NOINFO";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_HIGHRISE_BAN = "LEVELUP_HIGHRISE_BAN";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_LOWWEALTH = "LEVELUP_LOWWEALTH";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string LEVELUP_SPECIAL_INDUSTRY = "LEVELUP_SPECIAL_INDUSTRY";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_TITLE = "INFO_ENTERTAINMENT_TITLE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_BUILDINGS = "INFO_ENTERTAINMENT_BUILDINGS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_PARKS = "INFO_ENTERTAINMENT_PARKS";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_UBVISIT = "INFO_ENTERTAINMENT_UBVISIT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_PARKVISIT = "INFO_ENTERTAINMENT_PARKVISIT";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAINMENT_LEGEND = "INFO_ENTERTAINMENT_LEGEND";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string INFO_ENTERTAIMMENT_COVERAGE = "INFO_ENTERTAIMMENT_COVERAGE";

	[Locale(file = "en-ui", category = "UI Main", hideInEditor = false)]
	public const string DLCPANEL_ARTDECO = "DLCPANEL_ARTDECO";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string TOURIST_AGEWEALTH = "TOURIST_AGEWEALTH";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string CITIZEN_AGEEDUCATION = "CITIZEN_AGEEDUCATION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_OCCUPATION_TOURIST = "CITIZEN_OCCUPATION_TOURIST";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_OCCUPATION_UNEMPLOYED = "CITIZEN_OCCUPATION_UNEMPLOYED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_OCCUPATION_RESIDENCE = "CITIZEN_OCCUPATION_RESIDENCE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_OCCUPATION_POSITION = "CITIZEN_OCCUPATION_POSITION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string CITIZEN_SCHOOL_LEVEL = "CITIZEN_SCHOOL_LEVEL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_VISITING = "CITIZEN_STATUS_VISITING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_AT_HOME = "CITIZEN_STATUS_AT_HOME";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_AT_WORK = "CITIZEN_STATUS_AT_WORK";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_AT_SCHOOL = "CITIZEN_STATUS_AT_SCHOOL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_GOINGTO = "CITIZEN_STATUS_GOINGTO";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_GOINGTO_HOME = "CITIZEN_STATUS_GOINGTO_HOME";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_GOINGTO_WORK = "CITIZEN_STATUS_GOINGTO_WORK";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_GOINGTO_SCHOOL = "CITIZEN_STATUS_GOINGTO_SCHOOL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_GOINGTO_OUTSIDE = "CITIZEN_STATUS_GOINGTO_OUTSIDE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_DRIVINGTO = "CITIZEN_STATUS_DRIVINGTO";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_DRIVINGTO_HOME = "CITIZEN_STATUS_DRIVINGTO_HOME";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_DRIVINGTO_WORK = "CITIZEN_STATUS_DRIVINGTO_WORK";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_DRIVINGTO_SCHOOL = "CITIZEN_STATUS_DRIVINGTO_SCHOOL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_DRIVINGTO_OUTSIDE = "CITIZEN_STATUS_DRIVINGTO_OUTSIDE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_TRAVELLINGTO = "CITIZEN_STATUS_TRAVELLINGTO";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_TRAVELLINGTO_HOME = "CITIZEN_STATUS_TRAVELLINGTO_HOME";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_TRAVELLINGTO_WORK = "CITIZEN_STATUS_TRAVELLINGTO_WORK";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_TRAVELLINGTO_SCHOOL = "CITIZEN_STATUS_TRAVELLINGTO_SCHOOL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_TRAVELLINGTO_OUTSIDE = "CITIZEN_STATUS_TRAVELLINGTO_OUTSIDE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITIZEN_STATUS_CONFUSED = "CITIZEN_STATUS_CONFUSED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ANIMAL_STATUS_FLYING = "ANIMAL_STATUS_FLYING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ANIMAL_STATUS_WANDERING = "ANIMAL_STATUS_WANDERING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ANIMAL_STATUS_EATING = "ANIMAL_STATUS_EATING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ANIMAL_STATUS_FOLLOWING = "ANIMAL_STATUS_FOLLOWING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ANIMAL_STATUS_WAITING = "ANIMAL_STATUS_WAITING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string PARAMEDIC_STATUS = "PARAMEDIC_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string HEARSEDRIVER_STATUS = "HEARSEDRIVER_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CRIMINAL_STATUS = "CRIMINAL_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string FIREMAN_STATUS = "FIREMAN_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string PARKWORKER_STATUS = "PARKWORKER_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string POLICEOFFICER_STATUS = "POLICEOFFICER_STATUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_OWNER = "VEHICLE_OWNER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_PASSENGERS = "VEHICLE_PASSENGERS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_DISTANCETRAVELED = "VEHICLE_DISTANCETRAVELED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PARKING = "VEHICLE_STATUS_PARKING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PARKED = "VEHICLE_STATUS_PARKED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CONFUSED = "VEHICLE_STATUS_CONFUSED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GOINGTO = "VEHICLE_STATUS_GOINGTO";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_LEAVING = "VEHICLE_STATUS_LEAVING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string VEHICLE_STATUS_CARGOTRUCK_DELIVER = "VEHICLE_STATUS_CARGOTRUCK_DELIVER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string VEHICLE_STATUS_CARGOTRUCK_IMPORT = "VEHICLE_STATUS_CARGOTRUCK_IMPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string VEHICLE_STATUS_CARGOTRUCK_EXPORT = "VEHICLE_STATUS_CARGOTRUCK_EXPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOTRUCK_UNLOAD = "VEHICLE_STATUS_CARGOTRUCK_UNLOAD";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOTRUCK_RETURN = "VEHICLE_STATUS_CARGOTRUCK_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GARBAGE_COLLECT = "VEHICLE_STATUS_GARBAGE_COLLECT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GARBAGE_WAIT = "VEHICLE_STATUS_GARBAGE_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GARBAGE_TRANSFER = "VEHICLE_STATUS_GARBAGE_TRANSFER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GARBAGE_UNLOAD = "VEHICLE_STATUS_GARBAGE_UNLOAD";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_GARBAGE_RETURN = "VEHICLE_STATUS_GARBAGE_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_PATROL = "VEHICLE_STATUS_POLICE_PATROL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_STOPPED = "VEHICLE_STATUS_POLICE_STOPPED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_PATROL_WAIT = "VEHICLE_STATUS_POLICE_PATROL_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_STOP_WAIT = "VEHICLE_STATUS_POLICE_STOP_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_EMERGENCY = "VEHICLE_STATUS_POLICE_EMERGENCY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_POLICE_RETURN = "VEHICLE_STATUS_POLICE_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_EMERGENCY = "VEHICLE_STATUS_AMBULANCE_EMERGENCY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_WAIT = "VEHICLE_STATUS_AMBULANCE_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_RETURN_EMPTY = "VEHICLE_STATUS_AMBULANCE_RETURN_EMPTY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AMBULANCE_RETURN_FULL = "VEHICLE_STATUS_AMBULANCE_RETURN_FULL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_HEARSE_COLLECT = "VEHICLE_STATUS_HEARSE_COLLECT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_HEARSE_WAIT = "VEHICLE_STATUS_HEARSE_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_HEARSE_TRANSFER = "VEHICLE_STATUS_HEARSE_TRANSFER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_HEARSE_UNLOAD = "VEHICLE_STATUS_HEARSE_UNLOAD";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_HEARSE_RETURN = "VEHICLE_STATUS_HEARSE_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRETRUCK_EMERGENCY = "VEHICLE_STATUS_FIRETRUCK_EMERGENCY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRETRUCK_EXTINGUISH = "VEHICLE_STATUS_FIRETRUCK_EXTINGUISH";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRETRUCK_WAIT = "VEHICLE_STATUS_FIRETRUCK_WAIT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_FIRETRUCK_RETURN = "VEHICLE_STATUS_FIRETRUCK_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_BUS_STOPPED = "VEHICLE_STATUS_BUS_STOPPED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_BUS_ROUTE = "VEHICLE_STATUS_BUS_ROUTE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_BUS_RETURN = "VEHICLE_STATUS_BUS_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_METRO_STOPPED = "VEHICLE_STATUS_METRO_STOPPED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_METRO_ROUTE = "VEHICLE_STATUS_METRO_ROUTE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_METRO_RETURN = "VEHICLE_STATUS_METRO_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERTRAIN_STOPPED = "VEHICLE_STATUS_PASSENGERTRAIN_STOPPED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERTRAIN_ROUTE = "VEHICLE_STATUS_PASSENGERTRAIN_ROUTE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERTRAIN_TRANSPORT = "VEHICLE_STATUS_PASSENGERTRAIN_TRANSPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERTRAIN_RETURN = "VEHICLE_STATUS_PASSENGERTRAIN_RETURN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOTRAIN_LOADING = "VEHICLE_STATUS_CARGOTRAIN_LOADING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOTRAIN_UNLOADING = "VEHICLE_STATUS_CARGOTRAIN_UNLOADING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOTRAIN_TRANSPORT = "VEHICLE_STATUS_CARGOTRAIN_TRANSPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERSHIP_STOPPED = "VEHICLE_STATUS_PASSENGERSHIP_STOPPED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_PASSENGERSHIP_TRANSPORT = "VEHICLE_STATUS_PASSENGERSHIP_TRANSPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOSHIP_LOADING = "VEHICLE_STATUS_CARGOSHIP_LOADING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOSHIP_UNLOADING = "VEHICLE_STATUS_CARGOSHIP_UNLOADING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_CARGOSHIP_TRANSPORT = "VEHICLE_STATUS_CARGOSHIP_TRANSPORT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AIRPLANE_BOARDING = "VEHICLE_STATUS_AIRPLANE_BOARDING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AIRPLANE_TAXIING = "VEHICLE_STATUS_AIRPLANE_TAXIING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AIRPLANE_TAKING_OFF = "VEHICLE_STATUS_AIRPLANE_TAKING_OFF";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AIRPLANE_FLYING = "VEHICLE_STATUS_AIRPLANE_FLYING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string VEHICLE_STATUS_AIRPLANE_LANDING = "VEHICLE_STATUS_AIRPLANE_LANDING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string TRANSPORT_LINE_COLOR = "TRANSPORT_LINE_COLOR";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string TRANSPORT_LINE_PASSENGERS = "TRANSPORT_LINE_PASSENGERS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string TRANSPORT_LINE_VEHICLECOUNT = "TRANSPORT_LINE_VEHICLECOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string TRANSPORT_LINE_TRIPSAVED = "TRANSPORT_LINE_TRIPSAVED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_UNDER_CONSTRUCTION = "BUILDING_STATUS_UNDER_CONSTRUCTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_ABANDONED = "BUILDING_STATUS_ABANDONED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_BURNED = "BUILDING_STATUS_BURNED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_DEFAULT = "BUILDING_STATUS_DEFAULT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_REDUCED = "BUILDING_STATUS_REDUCED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_NOT_OPERATING = "BUILDING_STATUS_NOT_OPERATING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_EMPTYING = "BUILDING_STATUS_EMPTYING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_OFF = "BUILDING_STATUS_OFF";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_FULL = "BUILDING_STATUS_FULL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_UPGRADING = "BUILDING_STATUS_UPGRADING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_STATUS_HOUSEHOLDS = "BUILDING_STATUS_HOUSEHOLDS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string BUILDING_MOVING = "BUILDING_MOVING";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NEWTILE = "AREA_NEWTILE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_BUY = "AREA_BUY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NATURALRESOURCES = "AREA_NATURALRESOURCES";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_WATER = "AREA_WATER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_CONNECTIONS = "AREA_CONNECTIONS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_BUILDABLE = "AREA_BUILDABLE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_PRICE = "AREA_PRICE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_OILAMOUNT = "AREA_OILAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_OREAMOUNT = "AREA_OREAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_FORESTRYAMOUNT = "AREA_FORESTRYAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_FARMINGAMOUNT = "AREA_FARMINGAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_YES_WATER = "AREA_YES_WATER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NO_WATER = "AREA_NO_WATER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_YES_HIGHWAYCONNECTION = "AREA_YES_HIGHWAYCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_YES_TRAINCONNECTION = "AREA_YES_TRAINCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_YES_SHIPCONNECTION = "AREA_YES_SHIPCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_YES_PLANECONNECTION = "AREA_YES_PLANECONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NO_HIGHWAYCONNECTION = "AREA_NO_HIGHWAYCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NO_TRAINCONNECTION = "AREA_NO_TRAINCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NO_SHIPCONNECTION = "AREA_NO_SHIPCONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_NO_PLANECONNECTION = "AREA_NO_PLANECONNECTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_OWNEDTILE = "AREA_OWNEDTILE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string AREA_WATERAMOUNT = "AREA_WATERAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string INFOPANEL_FOCUS = "INFOPANEL_FOCUS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string INFOPANEL_FOLLOW = "INFOPANEL_FOLLOW";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITYSERVICE_ONOFF = "CITYSERVICE_ONOFF";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITYSERVICE_MOVE = "CITYSERVICE_MOVE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITYSERVICE_EMPTY = "CITYSERVICE_EMPTY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITYSERVICE_STOPEMPTY = "CITYSERVICE_STOPEMPTY";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITYSERVICE_BUDGET = "CITYSERVICE_BUDGET";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = true)]
	public const string ZONEDBUILDING_TITLE = "ZONEDBUILDING_TITLE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_UNEDUCATED = "ZONEDBUILDING_UNEDUCATED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EDUCATED = "ZONEDBUILDING_EDUCATED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_WELLEDUCATED = "ZONEDBUILDING_WELLEDUCATED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_HIGHLYEDUCATED = "ZONEDBUILDING_HIGHLYEDUCATED";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_CHILDREN = "ZONEDBUILDING_CHILDREN";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_TEENS = "ZONEDBUILDING_TEENS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_YOUNGS = "ZONEDBUILDING_YOUNGS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_ADULTS = "ZONEDBUILDING_ADULTS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_SENIORS = "ZONEDBUILDING_SENIORS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_WORKERS = "ZONEDBUILDING_WORKERS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_OVEREDUCATEDWORKERS = "ZONEDBUILDING_OVEREDUCATEDWORKERS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_OVEREDUCATEDWORKER = "ZONEDBUILDING_OVEREDUCATEDWORKER";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL0_WORKERCOUNT = "ZONEDBUILDING_EL0_WORKERCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL1_WORKERCOUNT = "ZONEDBUILDING_EL1_WORKERCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL2_WORKERCOUNT = "ZONEDBUILDING_EL2_WORKERCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL3_WORKERCOUNT = "ZONEDBUILDING_EL3_WORKERCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL0_WORKPLACECOUNT = "ZONEDBUILDING_EL0_WORKPLACECOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL1_WORKPLACECOUNT = "ZONEDBUILDING_EL1_WORKPLACECOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL2_WORKPLACECOUNT = "ZONEDBUILDING_EL2_WORKPLACECOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EL3_WORKPLACECOUNT = "ZONEDBUILDING_EL3_WORKPLACECOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_JOBSAVAIL = "ZONEDBUILDING_JOBSAVAIL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_WORKERCHART = "ZONEDBUILDING_WORKERCHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EDUCATIONCHART = "ZONEDBUILDING_EDUCATIONCHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_AGECHART = "ZONEDBUILDING_AGECHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_CHILD_RESIDENTCOUNT = "ZONEDBUILDING_CHILD_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_TEEN_RESIDENTCOUNT = "ZONEDBUILDING_TEEN_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_YOUNG_RESIDENTCOUNT = "ZONEDBUILDING_YOUNG_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_ADULT_RESIDENTCOUNT = "ZONEDBUILDING_ADULT_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_SENIOR_RESIDENTCOUNT = "ZONEDBUILDING_SENIOR_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_UNEDUCATED_RESIDENTCOUNT = "ZONEDBUILDING_UNEDUCATED_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_EDUCATED_RESIDENTCOUNT = "ZONEDBUILDING_EDUCATED_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_WELLEDUCATED_RESIDENTCOUNT = "ZONEDBUILDING_WELLEDUCATED_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string ZONEDBUILDING_HIGHLYEDUCATED_RESIDENTCOUNT = "ZONEDBUILDING_HIGHLYEDUCATED_RESIDENTCOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_LANDVALUE_AVERAGE = "DISTRICT_LANDVALUE_AVERAGE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_POLICIES = "DISTRICT_POLICIES";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_AVGLEVELS = "DISTRICT_AVGLEVELS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_DISTRIBUTION = "DISTRICT_DISTRIBUTION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_AVGVALUE = "DISTRICT_AVGVALUE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_CHILDAMOUNT = "DISTRICT_CHILDAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_CHILDAMOUNT = "CITY_CHILDAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_TEENAMOUNT = "DISTRICT_TEENAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_TEENAMOUNT = "CITY_TEENAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_YOUNGAMOUNT = "DISTRICT_YOUNGAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_YOUNGAMOUNT = "CITY_YOUNGAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_ADULTAMOUNT = "DISTRICT_ADULTAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_ADULTAMOUNT = "CITY_ADULTAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_SENIORAMOUNT = "DISTRICT_SENIORAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_SENIORAMOUNT = "CITY_SENIORAMOUNT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_AGECHART = "DISTRICT_AGECHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_AGECHART = "CITY_AGECHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_RESIDENTIAL = "DISTRICT_RESIDENTIAL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_COMMERCIAL = "DISTRICT_COMMERCIAL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_INDUSTRIAL = "DISTRICT_INDUSTRIAL";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_OFFICE = "DISTRICT_OFFICE";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_DISTRIBUTIONCHART = "CITY_DISTRIBUTIONCHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_DISTRIBUTIONCHART = "DISTRICT_DISTRIBUTIONCHART";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_POPULATION = "DISTRICT_POPULATION";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_HOUSEHOLDS = "DISTRICT_HOUSEHOLDS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_TOURISTS = "DISTRICT_TOURISTS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_WORKERS = "DISTRICT_WORKERS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_COUNTFORMAT = "DISTRICT_COUNTFORMAT";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string DISTRICT_NORESIDENTS = "DISTRICT_NORESIDENTS";

	[Locale(file = "en-ui", category = "InfoPanels", hideInEditor = false)]
	public const string CITY_NORESIDENTS = "CITY_NORESIDENTS";

	[Locale(file = "en-ui", category = "Keymapping", hideInEditor = true)]
	public const string KEYNAME = "KEYNAME";

	[Locale(file = "", category = "", hideInEditor = false)]
	public const string LANGUAGE_APPID = "LANGUAGE_APPID";

	[Locale(file = "en-credits-stadiums", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_STADIUMS = "GAME_STADIUMS";

	[Locale(file = "en-credits", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITS = "GAME_CREDITS";

	[Locale(file = "en-creditsAD", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITSAD = "GAME_CREDITSAD";

	[Locale(file = "en-creditsGB", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITSGB = "GAME_CREDITSGB";

	[Locale(file = "en-creditsMT", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITSMT = "GAME_CREDITSMT";

	[Locale(file = "en-creditsND", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITSND = "GAME_CREDITSND";

	[Locale(file = "en-creditsWW", category = "INTERNAL__Credits", hideInEditor = false)]
	public const string GAME_CREDITSWW = "GAME_CREDITSWW";
}
