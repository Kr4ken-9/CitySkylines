﻿using System;
using ColossalFramework;

public struct CitizenUnit
{
	public Citizen.Location GetLocation()
	{
		if ((ushort)(this.m_flags & (CitizenUnit.Flags.Work | CitizenUnit.Flags.Student)) != 0)
		{
			return Citizen.Location.Work;
		}
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Visit) != 0)
		{
			return Citizen.Location.Visit;
		}
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Vehicle) != 0)
		{
			return Citizen.Location.Moving;
		}
		return Citizen.Location.Home;
	}

	public uint GetCitizen(int index)
	{
		switch (index)
		{
		case 0:
			return this.m_citizen0;
		case 1:
			return this.m_citizen1;
		case 2:
			return this.m_citizen2;
		case 3:
			return this.m_citizen3;
		case 4:
			return this.m_citizen4;
		default:
			return 0u;
		}
	}

	public void SetCitizen(int index, uint citizen)
	{
		switch (index)
		{
		case 0:
			this.m_citizen0 = citizen;
			return;
		case 1:
			this.m_citizen1 = citizen;
			return;
		case 2:
			this.m_citizen2 = citizen;
			return;
		case 3:
			this.m_citizen3 = citizen;
			return;
		case 4:
			this.m_citizen4 = citizen;
			return;
		default:
			return;
		}
	}

	public void SimulationStep(uint unitID)
	{
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Home) != 0)
		{
			for (int i = 0; i < 5; i++)
			{
				uint citizen = this.GetCitizen(i);
				if (citizen != 0u)
				{
					CitizenManager instance = Singleton<CitizenManager>.get_instance();
					CitizenInfo citizenInfo = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
					if (citizenInfo != null)
					{
						citizenInfo.m_citizenAI.SimulationStep(unitID, ref this);
					}
					break;
				}
			}
		}
	}

	public void SetBuildingAfterLoading(uint unitID, ushort building, uint version)
	{
		Citizen[] buffer = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer;
		this.m_building = building;
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Home) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].m_homeBuilding = building;
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].m_homeBuilding = building;
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].m_homeBuilding = building;
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].m_homeBuilding = building;
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].m_homeBuilding = building;
			}
			if (version < 190u)
			{
				if (this.m_citizen0 != 0u)
				{
					buffer[(int)((UIntPtr)this.m_citizen0)].m_family = (byte)unitID;
				}
				if (this.m_citizen1 != 0u)
				{
					buffer[(int)((UIntPtr)this.m_citizen1)].m_family = (byte)unitID;
				}
				if (this.m_citizen2 != 0u)
				{
					buffer[(int)((UIntPtr)this.m_citizen2)].m_family = (byte)unitID;
				}
				if (this.m_citizen3 != 0u)
				{
					buffer[(int)((UIntPtr)this.m_citizen3)].m_family = (byte)unitID;
				}
				if (this.m_citizen4 != 0u)
				{
					buffer[(int)((UIntPtr)this.m_citizen4)].m_family = (byte)unitID;
				}
			}
		}
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Work) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].m_workBuilding = building;
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].m_workBuilding = building;
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].m_workBuilding = building;
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].m_workBuilding = building;
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].m_workBuilding = building;
			}
		}
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Student) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].m_workBuilding = building;
				Citizen[] expr_244_cp_0 = buffer;
				UIntPtr expr_244_cp_1 = (UIntPtr)this.m_citizen0;
				expr_244_cp_0[(int)expr_244_cp_1].m_flags = (expr_244_cp_0[(int)expr_244_cp_1].m_flags | Citizen.Flags.Student);
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].m_workBuilding = building;
				Citizen[] expr_27D_cp_0 = buffer;
				UIntPtr expr_27D_cp_1 = (UIntPtr)this.m_citizen1;
				expr_27D_cp_0[(int)expr_27D_cp_1].m_flags = (expr_27D_cp_0[(int)expr_27D_cp_1].m_flags | Citizen.Flags.Student);
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].m_workBuilding = building;
				Citizen[] expr_2B6_cp_0 = buffer;
				UIntPtr expr_2B6_cp_1 = (UIntPtr)this.m_citizen2;
				expr_2B6_cp_0[(int)expr_2B6_cp_1].m_flags = (expr_2B6_cp_0[(int)expr_2B6_cp_1].m_flags | Citizen.Flags.Student);
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].m_workBuilding = building;
				Citizen[] expr_2EF_cp_0 = buffer;
				UIntPtr expr_2EF_cp_1 = (UIntPtr)this.m_citizen3;
				expr_2EF_cp_0[(int)expr_2EF_cp_1].m_flags = (expr_2EF_cp_0[(int)expr_2EF_cp_1].m_flags | Citizen.Flags.Student);
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].m_workBuilding = building;
				Citizen[] expr_328_cp_0 = buffer;
				UIntPtr expr_328_cp_1 = (UIntPtr)this.m_citizen4;
				expr_328_cp_0[(int)expr_328_cp_1].m_flags = (expr_328_cp_0[(int)expr_328_cp_1].m_flags | Citizen.Flags.Student);
			}
		}
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Visit) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].m_visitBuilding = building;
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].m_visitBuilding = building;
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].m_visitBuilding = building;
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].m_visitBuilding = building;
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].m_visitBuilding = building;
			}
		}
	}

	public void SetVehicleAfterLoading(ushort vehicle)
	{
		Citizen[] buffer = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer;
		this.m_vehicle = vehicle;
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Vehicle) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].m_vehicle = vehicle;
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].m_vehicle = vehicle;
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].m_vehicle = vehicle;
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].m_vehicle = vehicle;
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].m_vehicle = vehicle;
			}
		}
	}

	public void SetWealthLevel(Citizen.Wealth wealthLevel)
	{
		Citizen[] buffer = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer;
		if ((ushort)(this.m_flags & CitizenUnit.Flags.Home) != 0)
		{
			if (this.m_citizen0 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen0)].WealthLevel = wealthLevel;
			}
			if (this.m_citizen1 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen1)].WealthLevel = wealthLevel;
			}
			if (this.m_citizen2 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen2)].WealthLevel = wealthLevel;
			}
			if (this.m_citizen3 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen3)].WealthLevel = wealthLevel;
			}
			if (this.m_citizen4 != 0u)
			{
				buffer[(int)((UIntPtr)this.m_citizen4)].WealthLevel = wealthLevel;
			}
		}
	}

	public void GetCitizenHomeBehaviour(ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (this.m_citizen0 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen0)].GetCitizenHomeBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen1 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen1)].GetCitizenHomeBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen2 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen2)].GetCitizenHomeBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen3 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen3)].GetCitizenHomeBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen4 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen4)].GetCitizenHomeBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
	}

	public void GetCitizenWorkBehaviour(ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (this.m_citizen0 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen0)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen1 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen1)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen2 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen2)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen3 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen3)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen4 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen4)].GetCitizenWorkBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
	}

	public void GetCitizenStudentBehaviour(ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (this.m_citizen0 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen0)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen1 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen1)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen2 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen2)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen3 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen3)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen4 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen4)].GetCitizenStudentBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
	}

	public void GetCitizenVisitBehaviour(ref Citizen.BehaviourData behaviour, ref int aliveCount, ref int totalCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (this.m_citizen0 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen0)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen1 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen1)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen2 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen2)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen3 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen3)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
		if (this.m_citizen4 != 0u)
		{
			instance.m_citizens.m_buffer[(int)((UIntPtr)this.m_citizen4)].GetCitizenVisitBehaviour(ref behaviour, ref aliveCount, ref totalCount);
		}
	}

	public bool ContainsCitizen(uint citizen)
	{
		return this.m_citizen0 == citizen || this.m_citizen1 == citizen || this.m_citizen2 == citizen || this.m_citizen3 == citizen || this.m_citizen4 == citizen;
	}

	public bool Empty()
	{
		return this.m_citizen0 == 0u && this.m_citizen1 == 0u && this.m_citizen2 == 0u && this.m_citizen3 == 0u && this.m_citizen4 == 0u;
	}

	public bool Full()
	{
		return this.m_citizen0 != 0u && this.m_citizen1 != 0u && this.m_citizen2 != 0u && this.m_citizen3 != 0u && this.m_citizen4 != 0u;
	}

	public uint m_citizen0;

	public uint m_citizen1;

	public uint m_citizen2;

	public uint m_citizen3;

	public uint m_citizen4;

	public uint m_nextUnit;

	public CitizenUnit.Flags m_flags;

	public ushort m_building;

	public ushort m_vehicle;

	public ushort m_goods;

	[Flags]
	public enum Flags : ushort
	{
		None = 0,
		Created = 1,
		Home = 16,
		Work = 32,
		Visit = 64,
		Vehicle = 128,
		Student = 256,
		All = 65535
	}
}
