﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class MaintenanceTruckAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Maintenance)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_MAINTENANCE_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_MAINTENANCE_WAIT");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_MAINTENANCE_MAINTAIN");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Maintenance";
		current = (int)data.m_transferSize;
		max = this.m_maintenanceCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0 && data.m_transferSize != 0)
		{
			int transferSize = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			if (info != null)
			{
				info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
			}
		}
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0 && this.CheckTargetSegment(vehicleID, ref data))
		{
			NetSegment[] expr_32_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
			ushort expr_32_cp_1 = data.m_targetBuilding;
			expr_32_cp_0[(int)expr_32_cp_1].m_flags = (expr_32_cp_0[(int)expr_32_cp_1].m_flags | NetSegment.Flags.AccessFailed);
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding == data.m_targetBuilding)
		{
			if (data.m_path == 0u)
			{
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			else
			{
				this.TrySpawn(vehicleID, ref data);
			}
		}
		else
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if (targetBuilding != 0 && !this.CheckTargetSegment(vehicleID, ref data))
			{
				targetBuilding = 0;
				data.m_targetBuilding = 0;
			}
			if (targetBuilding == 0)
			{
				if ((int)data.m_transferSize < this.m_maintenanceCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Vehicle = vehicleID;
					if (data.m_sourceBuilding != 0)
					{
						offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
					}
					else
					{
						offer.Position = data.GetLastFramePosition();
					}
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
					data.m_flags |= Vehicle.Flags.WaitingTarget;
				}
				else
				{
					data.m_flags |= Vehicle.Flags.GoingBack;
				}
			}
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding && (data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.NetSegment);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_maintenanceCapacity;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		data.m_targetBuilding = 0;
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		bool flag = this.TryMaintainRoad(vehicleID, ref vehicleData);
		if ((int)vehicleData.m_transferSize >= this.m_maintenanceCapacity && (vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0)
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
		else if ((vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0 && !this.CheckTargetSegment(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
		if (flag)
		{
			vehicleData.m_flags |= Vehicle.Flags.Emergency1;
		}
		else
		{
			vehicleData.m_flags &= ~Vehicle.Flags.Emergency1;
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_productionRate == 0 || (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None) && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && leaderData.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 targetPos;
			info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
			return;
		}
	}

	private bool TryMaintainRoad(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_path != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = vehicleData.m_pathPositionIndex;
			if (b == 255)
			{
				b = 0;
			}
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position position;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(b >> 1, out position))
			{
				return this.TryMaintainRoad(vehicleID, ref vehicleData, position.m_segment, ref instance.m_segments.m_buffer[(int)position.m_segment], false);
			}
		}
		return false;
	}

	private bool TryMaintainRoad(ushort vehicleID, ref Vehicle vehicleData, ushort segmentID, ref NetSegment segment, bool fullMaintenance)
	{
		if (segment.m_condition == 255)
		{
			return false;
		}
		vehicleData.m_transferSize = (ushort)Mathf.Min(this.m_maintenanceCapacity, (int)(vehicleData.m_transferSize + (ushort)(255 - segment.m_condition)));
		segment.m_condition = 255;
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.RoadBoosted);
		statisticBase.Add(Mathf.Max(1, Mathf.RoundToInt(segment.m_averageLength)));
		return true;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		base.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position, laneID, offset, prevPos, prevLaneID, prevOffset, index, out pos, out dir, out maxSpeed);
		if (index <= 0 && prevPos.m_segment != 0)
		{
			this.TryMaintainRoad(vehicleID, ref vehicleData, prevPos.m_segment, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)prevPos.m_segment], true);
		}
	}

	private bool CheckTargetSegment(ushort vehicleID, ref Vehicle vehicleData)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		return vehicleData.m_targetBuilding < 36864 && (instance.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created && instance.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_condition < 192;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0 && this.CheckTargetSegment(vehicleID, ref vehicleData))
		{
			NetManager instance2 = Singleton<NetManager>.get_instance();
			NetInfo info2 = instance2.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			if (info2.m_lanes == null)
			{
				return false;
			}
			int num = 0;
			for (int i = 0; i < info2.m_lanes.Length; i++)
			{
				if (info2.m_lanes[i].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car))
				{
					num++;
				}
			}
			if (num == 0)
			{
				return false;
			}
			Vector3 endPos2 = Vector3.get_zero();
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num);
			uint num2 = instance2.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_lanes;
			int num3 = 0;
			while (num3 < info2.m_lanes.Length && num2 != 0u)
			{
				if (info2.m_lanes[num3].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car) && num-- == 0)
				{
					endPos2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)num2)].CalculatePosition(0.5f);
					break;
				}
				num2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
				num3++;
			}
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.NetSegment = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public override int GetNoiseLevel()
	{
		return 9;
	}

	[CustomizableProperty("Maintenance capacity")]
	public int m_maintenanceCapacity = 1;
}
