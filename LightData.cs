﻿using System;
using UnityEngine;

public struct LightData
{
	public LightData(InstanceID id, Vector3 position, Color color, float intensity, float range)
	{
		this.m_id = id;
		this.m_type = 2;
		this.m_position = position;
		this.m_rotation = Quaternion.get_identity();
		this.m_color = color;
		this.m_intensity = intensity;
		this.m_range = range;
		this.m_spotAngle = 10f;
		this.m_fadeSpeed = 10f;
	}

	public LightData(InstanceID id, Vector3 position, Color color, float intensity, float range, float fadeSpeed)
	{
		this.m_id = id;
		this.m_type = 2;
		this.m_position = position;
		this.m_rotation = Quaternion.get_identity();
		this.m_color = color;
		this.m_intensity = intensity;
		this.m_range = range;
		this.m_spotAngle = 10f;
		this.m_fadeSpeed = fadeSpeed;
	}

	public LightData(InstanceID id, LightType type, Vector3 position, Quaternion rotation, Color color, float intensity, float range, float spotAngle)
	{
		this.m_id = id;
		this.m_type = type;
		this.m_position = position;
		this.m_rotation = rotation;
		this.m_color = color;
		this.m_intensity = intensity;
		this.m_range = range;
		this.m_spotAngle = spotAngle;
		this.m_fadeSpeed = 10f;
	}

	public LightData(InstanceID id, LightType type, Vector3 position, Quaternion rotation, Color color, float intensity, float range, float spotAngle, float fadeSpeed)
	{
		this.m_id = id;
		this.m_type = type;
		this.m_position = position;
		this.m_rotation = rotation;
		this.m_color = color;
		this.m_intensity = intensity;
		this.m_range = range;
		this.m_spotAngle = spotAngle;
		this.m_fadeSpeed = fadeSpeed;
	}

	public InstanceID m_id;

	public LightType m_type;

	public Vector3 m_position;

	public Quaternion m_rotation;

	public Color m_color;

	public float m_intensity;

	public float m_range;

	public float m_spotAngle;

	public float m_fadeSpeed;
}
