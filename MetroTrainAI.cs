﻿using System;
using ColossalFramework.Globalization;

public class MetroTrainAI : PassengerTrainAI
{
	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_METRO_STOPPED");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_METRO_RETURN");
		}
		if (data.m_transportLine != 0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_METRO_ROUTE");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_METRO_STOPPED");
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.Underground;
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.Underground;
	}
}
