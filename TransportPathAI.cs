﻿using System;
using ColossalFramework;
using UnityEngine;

public class TransportPathAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void ReleaseNode(ushort nodeID, ref NetNode data)
	{
		if (data.m_lane != 0u)
		{
			this.RemoveLaneConnection(nodeID, ref data);
		}
		base.ReleaseNode(nodeID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if (this.m_info.m_canDisable)
		{
			return 100f;
		}
		return 200f;
	}

	public override void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
		Singleton<NetManager>.get_instance().UpdateNode(nodeID, 0, 10);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & (NetNode.Flags.Temporary | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.None)
		{
			uint num = 0u;
			byte offset = 0;
			float num2 = 1E+10f;
			PathUnit.Position pathPos;
			PathUnit.Position position;
			float num3;
			float num4;
			if (this.m_connectService1 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService1, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, (VehicleInfo.VehicleType)65503, false, true, 32f, out pathPos, out position, out num3, out num4) && num3 < num2)
			{
				num2 = num3;
				num = PathManager.GetLaneID(pathPos);
				offset = pathPos.m_offset;
			}
			PathUnit.Position pathPos2;
			PathUnit.Position position2;
			float num5;
			float num6;
			if (this.m_connectService2 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService2, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, (VehicleInfo.VehicleType)65503, false, true, 32f, out pathPos2, out position2, out num5, out num6) && num5 < num2)
			{
				num = PathManager.GetLaneID(pathPos2);
				offset = pathPos2.m_offset;
			}
			if (num != data.m_lane)
			{
				if (data.m_lane != 0u)
				{
					this.RemoveLaneConnection(nodeID, ref data);
				}
				if (num != 0u)
				{
					this.AddLaneConnection(nodeID, ref data, num, offset);
				}
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public ItemClass.Service m_connectService1 = ItemClass.Service.Road;

	public ItemClass.Service m_connectService2 = ItemClass.Service.PublicTransport;
}
