﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class RadioChannelCollection : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(RadioChannelCollection.InitializePrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading(base.get_gameObject().get_name());
		PrefabCollection<RadioChannelInfo>.DestroyPrefabs(base.get_gameObject().get_name(), this.m_prefabs, this.m_replacedNames);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private static IEnumerator InitializePrefabs(string name, RadioChannelInfo[] prefabs, string[] replaces)
	{
		RadioChannelCollection.<InitializePrefabs>c__Iterator0 <InitializePrefabs>c__Iterator = new RadioChannelCollection.<InitializePrefabs>c__Iterator0();
		<InitializePrefabs>c__Iterator.name = name;
		<InitializePrefabs>c__Iterator.prefabs = prefabs;
		<InitializePrefabs>c__Iterator.replaces = replaces;
		return <InitializePrefabs>c__Iterator;
	}

	public RadioChannelInfo[] m_prefabs;

	public string[] m_replacedNames;
}
