﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class RunwayAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.None)
		{
			Color color = this.m_info.m_color;
			color.a = (float)(255 - data.m_wetness) * 0.003921569f;
			return color;
		}
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
			int num2 = this.m_noiseAccumulation * num / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num2 * 1.25f);
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(segmentID, ref data, infoMode);
		}
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.None)
		{
			int num = 0;
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_wetness;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
			Color color = this.m_info.m_color;
			color.a = (float)(255 - num) * 0.003921569f;
			return color;
		}
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num3 = 0;
			if (this.m_noiseAccumulation != 0)
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				int num4 = 0;
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = data.GetSegment(j);
					if (segment2 != 0)
					{
						num3 += (int)instance2.m_segments.m_buffer[(int)segment2].m_noiseDensity;
						num4++;
					}
				}
				if (num4 != 0)
				{
					num3 /= num4;
				}
			}
			int num5 = 100 - (num3 - 100) * (num3 - 100) / 100;
			int num6 = this.m_noiseAccumulation * num5 / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num6 * 1.25f);
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(nodeID, ref data, infoMode);
		}
		NetManager instance3 = Singleton<NetManager>.get_instance();
		bool flag = false;
		for (int k = 0; k < 8; k++)
		{
			ushort segment3 = data.GetSegment(k);
			if (segment3 != 0 && (instance3.m_segments.m_buffer[(int)segment3].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				flag = true;
			}
		}
		if (flag)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + 1000f;
	}

	public override bool DisplayTempSegment()
	{
		return false;
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetNode.Flags flags = data.m_flags & ~NetNode.Flags.Transition;
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null && info.m_createPavement)
				{
					flags |= NetNode.Flags.Transition;
				}
			}
		}
		data.m_flags = flags;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		float num = 0f;
		uint num2 = data.m_lanes;
		int num3 = 0;
		while (num3 < this.m_info.m_lanes.Length && num2 != 0u)
		{
			NetInfo.Lane lane = this.m_info.m_lanes[num3];
			if (lane.m_laneType == NetInfo.LaneType.Vehicle)
			{
				num += instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_length;
			}
			num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
			num3++;
		}
		int num4 = Mathf.RoundToInt(num) << 4;
		int num5 = 0;
		int num6 = 0;
		if (num4 != 0)
		{
			num5 = (int)((byte)Mathf.Min((int)(data.m_trafficBuffer * 100) / num4, 100));
			num6 = (int)((byte)Mathf.Min((int)(data.m_noiseBuffer * 250) / num4, 100));
		}
		data.m_trafficBuffer = 0;
		data.m_noiseBuffer = 0;
		if (num5 > (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Min((int)(data.m_trafficDensity + 5), num5);
		}
		else if (num5 < (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Max((int)(data.m_trafficDensity - 5), num5);
		}
		if (num6 > (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Min((int)(data.m_noiseDensity + 2), num6);
		}
		else if (num6 < (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Max((int)(data.m_noiseDensity - 2), num6);
		}
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		bool flag = false;
		float num7 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
		if (num7 > vector.y)
		{
			flag = true;
		}
		int num8 = (int)data.m_wetness;
		if (!instance.m_treatWetAsSnow)
		{
			if (flag)
			{
				num8 = 255;
			}
			else
			{
				int num9 = -(num8 + 63 >> 5);
				float num10 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
				if (num10 != 0f)
				{
					int num11 = Mathf.RoundToInt(Mathf.Min(num10 * 4000f, 1000f));
					num9 += Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num11, num11 + 99) / 100;
				}
				num8 = Mathf.Clamp(num8 + num9, 0, 255);
			}
		}
		if (num8 != (int)data.m_wetness)
		{
			if (Mathf.Abs((int)data.m_wetness - num8) > 10)
			{
				data.m_wetness = (byte)num8;
				InstanceID empty = InstanceID.Empty;
				empty.NetSegment = segmentID;
				instance.AddSmoothColor(empty);
				empty.NetNode = data.m_startNode;
				instance.AddSmoothColor(empty);
				empty.NetNode = data.m_endNode;
				instance.AddSmoothColor(empty);
			}
			else
			{
				data.m_wetness = (byte)num8;
				instance.m_wetnessChanged = 256;
			}
		}
		int num12 = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
		int num13 = this.m_noiseAccumulation * num12 / 100;
		if (num13 != 0)
		{
			float num14 = Vector3.Distance(position, position2);
			int num15 = Mathf.FloorToInt(num14 / this.m_noiseRadius);
			for (int i = 0; i < num15; i++)
			{
				Vector3 position3 = Vector3.Lerp(position, position2, (float)(i + 1) / (float)(num15 + 1));
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num13, position3, this.m_noiseRadius);
			}
		}
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		int num = 0;
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
		}
		int num3 = 100 - (num - 100) * (num - 100) / 100;
		int num4 = this.m_noiseAccumulation * num3 / 100;
		if (num4 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num4, data.m_position, this.m_noiseRadius);
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & (NetNode.Flags.End | NetNode.Flags.Temporary)) == NetNode.Flags.End)
		{
			uint num = 0u;
			byte offset = 0;
			if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				ushort num2 = 0;
				float num3 = 1E+10f;
				FastList<ushort> serviceSegments = Singleton<NetManager>.get_instance().GetServiceSegments(this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
				for (int i = 0; i < serviceSegments.m_size; i++)
				{
					ushort num4 = serviceSegments.m_buffer[i];
					NetInfo info = instance.m_segments.m_buffer[(int)num4].Info;
					if (info.m_class.m_layer == ItemClass.Layer.AirplanePaths)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)num4].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)num4].m_endNode;
						Segment3 segment;
						segment.a = instance.m_nodes.m_buffer[(int)startNode].m_position;
						segment.b = instance.m_nodes.m_buffer[(int)endNode].m_position;
						float num5 = segment.DistanceSqr(data.m_position);
						if (num5 < num3)
						{
							num2 = num4;
							num3 = num5;
						}
					}
				}
				Vector3 vector;
				int num6;
				float num7;
				if (num2 != 0 && instance.m_segments.m_buffer[(int)num2].GetClosestLanePosition(data.m_position, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Plane, out vector, out num, out num6, out num7))
				{
					offset = (byte)Mathf.Clamp(Mathf.RoundToInt(num7 * 255f), 0, 255);
				}
			}
			if (num != data.m_lane)
			{
				if (data.m_lane != 0u)
				{
					this.RemoveLaneConnection(nodeID, ref data);
				}
				if (num != 0u)
				{
					this.AddLaneConnection(nodeID, ref data, num, offset);
				}
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		return demolish && base.CollapseSegment(segmentID, ref data, group, demolish);
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		result.g = 0;
		result.a = 255;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		result.g = 0;
		result.a = 255;
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public int m_noiseAccumulation = 10;

	public float m_noiseRadius = 40f;
}
