﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class GuideManager : SimulationManagerBase<GuideManager, GuideController>, ISimulationManager
{
	public bool TutorialDisabled
	{
		get
		{
			return this.m_tutorialDisabled;
		}
		set
		{
			this.m_tutorialDisabled = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_serviceNotUsed = new ServiceTypeGuide[12];
		this.m_serviceNeeded = new ServiceTypeGuide[12];
		this.m_TutorialMessages = new SavedBool(Settings.tutorialMessages, Settings.gameSettingsFile, DefaultSettings.tutorialMessages, true);
	}

	public override void InitializeProperties(GuideController properties)
	{
		base.InitializeProperties(properties);
		this.m_currentTrigger = null;
	}

	public override void DestroyProperties(GuideController properties)
	{
		if (properties == this.m_properties)
		{
			this.m_currentTrigger = null;
		}
		base.DestroyProperties(properties);
	}

	private void Update()
	{
		if (this.m_tutorialPanel != null)
		{
			if (!this.m_tutorialPanel.get_isVisibleSelf())
			{
				this.m_tutorialPanel = null;
			}
		}
		else if (this.m_tutorialTimeout > 0f)
		{
			this.m_tutorialTimeout -= Time.get_deltaTime();
		}
		else
		{
			this.m_canShowTutorial = true;
		}
		this.m_tutorialMessages = this.m_TutorialMessages.get_value();
	}

	private void ShowTutorialPanel(GuideTriggerBase trigger, GuideTriggerBase.ActivationInfo activationInfo)
	{
		string name = activationInfo.GetName();
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().GuideMessage(name);
		}
		string icon = activationInfo.GetIcon();
		string text = Locale.Get("TUTORIAL_TITLE", name);
		string text2 = activationInfo.FormatText(Locale.Get("TUTORIAL_TEXT", name));
		string spriteName = TutorialAdvisorPanel.GetSpriteName(name);
		IUITag tag = activationInfo.GetTag();
		if (tag != null)
		{
			string info = TooltipHelper.Format(new string[]
			{
				"title",
				text,
				"icon",
				icon,
				"text",
				text2,
				"sprite",
				spriteName
			});
			this.m_tutorialPanel = TutorialPanel.Show(tag, info, this.m_properties.m_tutorialHideTimeout);
			this.m_tutorialTimeout = this.m_properties.m_tutorialNextTimeout;
			this.m_canShowTutorial = false;
			this.m_tutorialOpened = true;
		}
		else
		{
			this.m_canShowTutorial = true;
			this.m_tutorialOpened = true;
		}
	}

	private void HideTutorialPanel(GuideTriggerBase trigger)
	{
		TutorialPanel.Hide();
	}

	public void InfoViewUsed()
	{
		GenericGuide infoviewNotUsed = this.m_infoviewNotUsed;
		if (infoviewNotUsed != null && !infoviewNotUsed.m_disabled)
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.DisableGuide(infoviewNotUsed));
		}
	}

	[DebuggerHidden]
	private IEnumerator DisableGuide(GenericGuide guide)
	{
		GuideManager.<DisableGuide>c__Iterator0 <DisableGuide>c__Iterator = new GuideManager.<DisableGuide>c__Iterator0();
		<DisableGuide>c__Iterator.guide = guide;
		return <DisableGuide>c__Iterator;
	}

	public bool CanActivate(GuideInfo guideInfo)
	{
		return this.m_currentTrigger == null && this.m_canShowTutorial && !this.m_tutorialDisabled && (this.m_tutorialMessages || guideInfo.m_overrideOptions);
	}

	public void Activate(GuideTriggerBase trigger, GuideTriggerBase.ActivationInfo activationInfo)
	{
		this.m_currentTrigger = trigger;
		this.m_tutorialOpened = false;
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.ShowTutorialPanel(trigger, activationInfo);
		});
	}

	public void Deactivate(GuideTriggerBase trigger)
	{
		if (this.m_currentTrigger == trigger)
		{
			this.m_currentTrigger = null;
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.HideTutorialPanel(trigger);
			});
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_currentTrigger != null)
		{
			if (this.m_tutorialOpened && this.m_canShowTutorial)
			{
				this.m_currentTrigger = null;
			}
			else if (!this.m_currentTrigger.Validate())
			{
				this.m_currentTrigger = null;
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.HideTutorialPanel(this.m_currentTrigger);
				});
			}
		}
		GuideController properties = this.m_properties;
		if (subStep <= 1 && properties != null)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num2 = num * 20 >> 10;
			int num3 = ((num + 1) * 20 >> 10) - 1;
			for (int i = num2; i <= num3; i++)
			{
				ItemClass.Service service = i + ItemClass.Service.Residential;
				if (Singleton<UnlockManager>.get_instance().Unlocked(service))
				{
					this.ServiceStep(properties, service);
				}
			}
		}
		if (subStep <= 1 && properties != null)
		{
			int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (num4 != 10)
			{
				if (num4 != 20)
				{
					if (num4 != 30)
					{
						if (num4 != 40)
						{
							if (num4 != 50)
							{
								if (num4 != 60)
								{
									if (num4 != 70)
									{
										if (num4 != 80)
										{
											if (num4 != 90)
											{
												if (num4 != 100)
												{
													if (num4 != 110)
													{
														if (num4 == 120)
														{
															InstanceID currentInstanceID = WorldInfoPanel.GetCurrentInstanceID();
															if (currentInstanceID.Building != 0)
															{
																ushort eventIndex = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)currentInstanceID.Building].m_eventIndex;
																if (eventIndex != 0 && Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].m_building == currentInstanceID.Building)
																{
																	EventInfo info = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].Info;
																	if (info != null)
																	{
																		SportMatchAI sportMatchAI = info.m_eventAI as SportMatchAI;
																		if (sportMatchAI != null && sportMatchAI.m_allowChangeColor)
																		{
																			this.m_teamColorNotUsed.Activate(properties.m_teamColorNotUsed);
																		}
																	}
																}
															}
															return;
														}
														if (num4 == 130)
														{
															InstanceID currentInstanceID2 = WorldInfoPanel.GetCurrentInstanceID();
															if (currentInstanceID2.Building != 0)
															{
																BuildingInfo info2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)currentInstanceID2.Building].Info;
																if (info2 != null)
																{
																	TransportStationAI transportStationAI = info2.m_buildingAI as TransportStationAI;
																	if (transportStationAI != null && transportStationAI.m_transportInfo.m_transportType == TransportInfo.TransportType.Train)
																	{
																		this.m_localOnlyStation.Activate(properties.m_localOnlyStation);
																	}
																}
															}
															return;
														}
														if (num4 == 140)
														{
															InstanceID currentInstanceID3 = WorldInfoPanel.GetCurrentInstanceID();
															if (currentInstanceID3.Citizen != 0u)
															{
																this.m_routeButton.Activate(properties.m_routeButton);
															}
															else if (currentInstanceID3.Vehicle != 0)
															{
																VehicleInfo info3 = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)currentInstanceID3.Vehicle].Info;
																if (info3 != null && info3.m_class.m_service != ItemClass.Service.PublicTransport)
																{
																	this.m_routeButton.Activate(properties.m_routeButton);
																}
															}
															return;
														}
														if (num4 == 150)
														{
															InstanceID currentInstanceID4 = WorldInfoPanel.GetCurrentInstanceID();
															if (currentInstanceID4.NetSegment != 0)
															{
																NetInfo info4 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)currentInstanceID4.NetSegment].Info;
																if (info4 != null && info4.m_class.m_service == ItemClass.Service.Road)
																{
																	this.m_roadNamesOpen.Activate(properties.m_roadNamesOpen);
																}
															}
															return;
														}
														if (num4 == 160)
														{
															bool flag = false;
															InstanceID currentInstanceID5 = WorldInfoPanel.GetCurrentInstanceID();
															if (currentInstanceID5.Building != 0)
															{
																ushort eventIndex2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)currentInstanceID5.Building].m_eventIndex;
																if (eventIndex2 != 0 && Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex2].m_building == currentInstanceID5.Building)
																{
																	EventInfo info5 = Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex2].Info;
																	if (info5 != null)
																	{
																		ConcertAI concertAI = info5.m_eventAI as ConcertAI;
																		if (concertAI != null)
																		{
																			flag = true;
																			this.m_ticketPricesNotUsed.Activate(properties.m_ticketPricesNotUsed);
																		}
																	}
																}
															}
															if (flag && !this.m_festivalPanelShowing)
															{
																this.m_festivalColorNotUsed.Activate(properties.m_festivalColorNotUsed);
															}
															this.m_festivalPanelShowing = flag;
															return;
														}
														if (num4 != 240)
														{
															return;
														}
													}
													InstanceID currentInstanceID6 = WorldInfoPanel.GetCurrentInstanceID();
													if (currentInstanceID6.Building != 0)
													{
														BuildingInfo info6 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)currentInstanceID6.Building].Info;
														if (info6 != null && info6.m_class.m_service <= ItemClass.Service.Office)
														{
															this.m_zonedBuildingInfo.Activate(properties.m_zonedBuildingInfo);
														}
													}
												}
												else if (!WorldInfoPanel.GetCurrentInstanceID().IsEmpty)
												{
													this.m_renameNotUsed.Activate(properties.m_renameNotUsed);
												}
											}
											else
											{
												DistrictManager instance = Singleton<DistrictManager>.get_instance();
												if (instance.m_districts.m_buffer[0].m_playerData.m_finalBuildingCount != 0 || instance.m_districts.m_buffer[0].m_residentialData.m_finalBuildingCount != 0)
												{
													this.m_worldInfoNotUsed.Activate(properties.m_worldInfoNotUsed);
												}
											}
										}
										else if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Bulldozer))
										{
											this.m_bulldozerNotUsed.Activate(properties.m_bulldozerNotUsed);
										}
									}
									else if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.InfoViews))
									{
										this.m_infoviewNotUsed.Activate(properties.m_infoviewNotUsed);
									}
								}
								else if (Singleton<SimulationManager>.get_instance().SimulationPaused && !Singleton<SimulationManager>.get_instance().ForcedSimulationPaused)
								{
									this.m_speedNotUsed2.Activate(properties.m_speedNotUsed2);
								}
							}
							else
							{
								UnlockController properties2 = Singleton<UnlockManager>.get_instance().m_properties;
								if (properties2 != null && properties2.m_progressionMilestones != null && properties2.m_progressionMilestones.Length != 0 && Singleton<UnlockManager>.get_instance().Unlocked(properties2.m_progressionMilestones[0]))
								{
									this.m_speedNotUsed.Activate(properties.m_speedNotUsed);
								}
							}
						}
						else if (!SteamController.hasController)
						{
							this.m_cameraNotTilted.Activate(properties.m_cameraNotTilted);
						}
					}
					else if (!SteamController.hasController)
					{
						this.m_cameraNotRotated.Activate(properties.m_cameraNotRotated);
					}
				}
				else if (!SteamController.hasController)
				{
					this.m_cameraNotMoved.Activate(properties.m_cameraNotMoved);
				}
			}
			else if (!SteamController.hasController)
			{
				this.m_cameraNotZoomed.Activate(properties.m_cameraNotZoomed);
			}
		}
	}

	private void ServiceStep(GuideController guideController, ItemClass.Service service)
	{
		if (service == ItemClass.Service.Disaster)
		{
			return;
		}
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex == -1)
		{
			return;
		}
		this.m_serviceNotUsed[publicServiceIndex].Activate(guideController.m_serviceNotUsed, service);
		switch (service)
		{
		case ItemClass.Service.Electricity:
		case ItemClass.Service.Water:
		case ItemClass.Service.Garbage:
		case ItemClass.Service.Education:
		{
			int activationCount = (int)this.m_serviceNeeded[publicServiceIndex].m_activationCount;
			if (activationCount > 0 && activationCount < 255)
			{
				this.m_serviceNeeded[publicServiceIndex].Deactivate();
			}
			break;
		}
		case ItemClass.Service.Beautification:
			if (Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount != 0u)
			{
				int num;
				Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.Wellbeing, out num);
				if (num < 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Activate(guideController.m_serviceNeeded, service);
				}
				else if (num > 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Deactivate();
				}
			}
			break;
		case ItemClass.Service.HealthCare:
			if (Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount != 0u)
			{
				int finalHealth = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHealth;
				if (finalHealth < 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Activate(guideController.m_serviceNeeded, service);
				}
				else if (finalHealth > 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Deactivate();
				}
			}
			break;
		case ItemClass.Service.PoliceDepartment:
			if (Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount != 0u)
			{
				int finalCrimeRate = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalCrimeRate;
				if (finalCrimeRate > 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Activate(guideController.m_serviceNeeded, service);
				}
				else if (finalCrimeRate < 50)
				{
					this.m_serviceNeeded[publicServiceIndex].Deactivate();
				}
			}
			break;
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new GuideManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("GuideManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 0; i < 12; i++)
		{
			if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_serviceNotUsed[i] == null)
			{
				this.m_serviceNotUsed[i] = new ServiceTypeGuide();
			}
			if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_serviceNeeded[i] == null)
			{
				this.m_serviceNeeded[i] = new ServiceTypeGuide();
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cameraNotZoomed == null)
		{
			this.m_cameraNotZoomed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cameraNotMoved == null)
		{
			this.m_cameraNotMoved = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cameraNotRotated == null)
		{
			this.m_cameraNotRotated = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cameraNotTilted == null)
		{
			this.m_cameraNotTilted = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_speedNotUsed == null)
		{
			this.m_speedNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_speedNotUsed2 == null)
		{
			this.m_speedNotUsed2 = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_infoviewNotUsed == null)
		{
			this.m_infoviewNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_notEnoughMoney == null)
		{
			this.m_notEnoughMoney = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_deathCareNeeded == null)
		{
			this.m_deathCareNeeded = new BuildingTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_bulldozerNotUsed == null)
		{
			this.m_bulldozerNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_worldInfoNotUsed == null)
		{
			this.m_worldInfoNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_renameNotUsed == null)
		{
			this.m_renameNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_zonedBuildingInfo == null)
		{
			this.m_zonedBuildingInfo = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_teamColorNotUsed == null)
		{
			this.m_teamColorNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_localOnlyStation == null)
		{
			this.m_localOnlyStation = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_routeButton == null)
		{
			this.m_routeButton = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_undergroundBulldozer == null)
		{
			this.m_undergroundBulldozer = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadNamesOpen == null)
		{
			this.m_roadNamesOpen = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_festivalColorNotUsed == null)
		{
			this.m_festivalColorNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_ticketPricesNotUsed == null)
		{
			this.m_ticketPricesNotUsed = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	private UIComponent m_tutorialPanel;

	private GuideTriggerBase m_currentTrigger;

	private float m_tutorialTimeout;

	private volatile bool m_canShowTutorial;

	private volatile bool m_tutorialOpened;

	private volatile bool m_tutorialDisabled;

	private volatile bool m_tutorialMessages;

	private SavedBool m_TutorialMessages;

	private bool m_festivalPanelShowing;

	[NonSerialized]
	public ServiceTypeGuide[] m_serviceNotUsed;

	[NonSerialized]
	public ServiceTypeGuide[] m_serviceNeeded;

	[NonSerialized]
	public GenericGuide m_cameraNotZoomed;

	[NonSerialized]
	public GenericGuide m_cameraNotMoved;

	[NonSerialized]
	public GenericGuide m_cameraNotRotated;

	[NonSerialized]
	public GenericGuide m_cameraNotTilted;

	[NonSerialized]
	public GenericGuide m_speedNotUsed;

	[NonSerialized]
	public GenericGuide m_speedNotUsed2;

	[NonSerialized]
	public GenericGuide m_infoviewNotUsed;

	[NonSerialized]
	public GenericGuide m_notEnoughMoney;

	[NonSerialized]
	public BuildingTypeGuide m_deathCareNeeded;

	[NonSerialized]
	public GenericGuide m_bulldozerNotUsed;

	[NonSerialized]
	public GenericGuide m_worldInfoNotUsed;

	[NonSerialized]
	public GenericGuide m_renameNotUsed;

	[NonSerialized]
	public GenericGuide m_zonedBuildingInfo;

	[NonSerialized]
	public GenericGuide m_teamColorNotUsed;

	[NonSerialized]
	public GenericGuide m_localOnlyStation;

	[NonSerialized]
	public GenericGuide m_routeButton;

	[NonSerialized]
	public GenericGuide m_undergroundBulldozer;

	[NonSerialized]
	public GenericGuide m_roadNamesOpen;

	[NonSerialized]
	public GenericGuide m_festivalColorNotUsed;

	[NonSerialized]
	public GenericGuide m_ticketPricesNotUsed;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "GuideManager");
			GuideManager instance = Singleton<GuideManager>.get_instance();
			for (int i = 0; i < 12; i++)
			{
				s.WriteObject<ServiceTypeGuide>(instance.m_serviceNotUsed[i]);
				s.WriteObject<ServiceTypeGuide>(instance.m_serviceNeeded[i]);
			}
			s.WriteObject<GenericGuide>(instance.m_cameraNotZoomed);
			s.WriteObject<GenericGuide>(instance.m_cameraNotMoved);
			s.WriteObject<GenericGuide>(instance.m_cameraNotRotated);
			s.WriteObject<GenericGuide>(instance.m_cameraNotTilted);
			s.WriteObject<GenericGuide>(instance.m_speedNotUsed);
			s.WriteObject<GenericGuide>(instance.m_speedNotUsed2);
			s.WriteObject<GenericGuide>(instance.m_infoviewNotUsed);
			s.WriteObject<GenericGuide>(instance.m_notEnoughMoney);
			s.WriteObject<BuildingTypeGuide>(instance.m_deathCareNeeded);
			s.WriteObject<GenericGuide>(instance.m_bulldozerNotUsed);
			s.WriteObject<GenericGuide>(instance.m_worldInfoNotUsed);
			s.WriteObject<GenericGuide>(instance.m_renameNotUsed);
			s.WriteObject<GenericGuide>(instance.m_zonedBuildingInfo);
			s.WriteObject<GenericGuide>(instance.m_teamColorNotUsed);
			s.WriteObject<GenericGuide>(instance.m_localOnlyStation);
			s.WriteObject<GenericGuide>(instance.m_routeButton);
			s.WriteObject<GenericGuide>(instance.m_undergroundBulldozer);
			s.WriteObject<GenericGuide>(instance.m_roadNamesOpen);
			s.WriteObject<GenericGuide>(instance.m_festivalColorNotUsed);
			s.WriteObject<GenericGuide>(instance.m_ticketPricesNotUsed);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "GuideManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "GuideManager");
			GuideManager instance = Singleton<GuideManager>.get_instance();
			if (s.get_version() >= 82u)
			{
				for (int i = 0; i < 12; i++)
				{
					instance.m_serviceNotUsed[i] = s.ReadObject<ServiceTypeGuide>();
					instance.m_serviceNeeded[i] = s.ReadObject<ServiceTypeGuide>();
				}
			}
			else
			{
				for (int j = 0; j < 12; j++)
				{
					instance.m_serviceNotUsed[j] = null;
					instance.m_serviceNeeded[j] = null;
				}
			}
			if (s.get_version() >= 84u)
			{
				instance.m_cameraNotZoomed = s.ReadObject<GenericGuide>();
				instance.m_cameraNotMoved = s.ReadObject<GenericGuide>();
				instance.m_cameraNotRotated = s.ReadObject<GenericGuide>();
				instance.m_cameraNotTilted = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_cameraNotZoomed = null;
				instance.m_cameraNotMoved = null;
				instance.m_cameraNotRotated = null;
				instance.m_cameraNotTilted = null;
			}
			if (s.get_version() >= 94u)
			{
				instance.m_speedNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_speedNotUsed = null;
			}
			if (s.get_version() >= 174u)
			{
				instance.m_speedNotUsed2 = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_speedNotUsed2 = null;
			}
			if (s.get_version() >= 107u)
			{
				instance.m_infoviewNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_infoviewNotUsed = null;
			}
			if (s.get_version() >= 114u)
			{
				instance.m_notEnoughMoney = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_notEnoughMoney = null;
			}
			if (s.get_version() >= 134u)
			{
				instance.m_deathCareNeeded = s.ReadObject<BuildingTypeGuide>();
				instance.m_bulldozerNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_deathCareNeeded = null;
				instance.m_bulldozerNotUsed = null;
			}
			if (s.get_version() >= 156u)
			{
				instance.m_worldInfoNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_worldInfoNotUsed = null;
			}
			if (s.get_version() >= 160u)
			{
				instance.m_renameNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_renameNotUsed = null;
			}
			if (s.get_version() >= 198u)
			{
				instance.m_zonedBuildingInfo = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_zonedBuildingInfo = null;
			}
			if (s.get_version() >= 253u)
			{
				instance.m_teamColorNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_teamColorNotUsed = null;
			}
			if (s.get_version() >= 316u)
			{
				instance.m_localOnlyStation = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_localOnlyStation = null;
			}
			if (s.get_version() >= 318u)
			{
				instance.m_routeButton = s.ReadObject<GenericGuide>();
				instance.m_undergroundBulldozer = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_routeButton = null;
				instance.m_undergroundBulldozer = null;
			}
			if (s.get_version() >= 109009u || (s.get_version() >= 108009u && s.get_version() < 109000u))
			{
				instance.m_roadNamesOpen = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_roadNamesOpen = null;
			}
			if (s.get_version() >= 109011u || (s.get_version() >= 108011u && s.get_version() < 109000u))
			{
				instance.m_festivalColorNotUsed = s.ReadObject<GenericGuide>();
				instance.m_ticketPricesNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_festivalColorNotUsed = null;
				instance.m_ticketPricesNotUsed = null;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "GuideManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "GuideManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "GuideManager");
		}
	}
}
