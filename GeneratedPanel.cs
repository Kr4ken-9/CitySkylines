﻿using System;
using System.Collections.Generic;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;

public abstract class GeneratedPanel : ToolsModifierControl
{
	public static UIComponent tooltipBox
	{
		get
		{
			if (GeneratedPanel.m_Tooltip == null)
			{
				GeneratedPanel.m_Tooltip = UIView.Find("InfoTooltip");
			}
			return GeneratedPanel.m_Tooltip;
		}
	}

	public static UIComponent roadTooltipBox
	{
		get
		{
			if (GeneratedPanel.m_RoadTooltip == null)
			{
				GeneratedPanel.m_RoadTooltip = UIView.Find("InfoRoadTooltip");
			}
			return GeneratedPanel.m_RoadTooltip;
		}
	}

	public static UIComponent landscapingTooltipBox
	{
		get
		{
			if (GeneratedPanel.m_LandscapingTooltip == null)
			{
				GeneratedPanel.m_LandscapingTooltip = UIView.Find("InfoLandscapingTooltip");
			}
			return GeneratedPanel.m_LandscapingTooltip;
		}
	}

	public static UIComponent advancedTooltipBox
	{
		get
		{
			if (GeneratedPanel.m_AdvancedTooltip == null)
			{
				GeneratedPanel.m_AdvancedTooltip = UIView.Find("InfoAdvancedTooltip");
			}
			return GeneratedPanel.m_AdvancedTooltip;
		}
	}

	public static UIComponent advancedDetailTooltipBox
	{
		get
		{
			if (GeneratedPanel.m_AdvancedTooltipDetail == null)
			{
				GeneratedPanel.m_AdvancedTooltipDetail = UIView.Find("InfoAdvancedTooltipDetail");
			}
			return GeneratedPanel.m_AdvancedTooltipDetail;
		}
	}

	public virtual void CleanPanel()
	{
	}

	public virtual void RefreshPanel()
	{
	}

	protected virtual void Awake()
	{
		GeneratedPanel.tooltipBox.Hide();
		GeneratedPanel.roadTooltipBox.Hide();
		GeneratedPanel.advancedTooltipBox.Hide();
		GeneratedPanel.advancedDetailTooltipBox.Hide();
		GeneratedPanel.landscapingTooltipBox.Hide();
		this.ComputeTooltipHashMap();
	}

	private void ComputeTooltipHashMap()
	{
		if (GeneratedPanel.m_TooltipHashMap.Count == 0)
		{
			this.ComputeTooltipHashMap(GeneratedPanel.tooltipBox);
			this.ComputeTooltipHashMap(GeneratedPanel.roadTooltipBox);
			this.ComputeTooltipHashMap(GeneratedPanel.advancedTooltipBox);
			this.ComputeTooltipHashMap(GeneratedPanel.advancedDetailTooltipBox);
			this.ComputeTooltipHashMap(GeneratedPanel.landscapingTooltipBox);
		}
	}

	private void ComputeTooltipHashMap(UIComponent comp)
	{
		BindPropertyByKey component = comp.GetComponent<BindPropertyByKey>();
		GeneratedPanel.m_TooltipHashMap.Add(component.GetHashCode(), comp);
	}

	public static UIComponent GetTooltipBox(int hash)
	{
		UIComponent result;
		if ((ToolsModifierControl.toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None && GeneratedPanel.m_TooltipHashMap.TryGetValue(hash, out result))
		{
			return result;
		}
		return GeneratedPanel.tooltipBox;
	}

	protected virtual void OnDestroy()
	{
		GeneratedPanel.m_TooltipHashMap.Clear();
		GeneratedPanel.m_Tooltip = null;
		GeneratedPanel.m_RoadTooltip = null;
		GeneratedPanel.m_AdvancedTooltip = null;
		GeneratedPanel.m_AdvancedTooltipDetail = null;
		GeneratedPanel.m_LandscapingTooltip = null;
		GeneratedPanel.m_IsRefreshing = false;
	}

	protected virtual void OnEnable()
	{
	}

	protected virtual void OnDisable()
	{
	}

	protected static bool m_IsRefreshing;

	private static Dictionary<int, UIComponent> m_TooltipHashMap = new Dictionary<int, UIComponent>();

	private static UIComponent m_Tooltip;

	private static UIComponent m_RoadTooltip;

	private static UIComponent m_AdvancedTooltip;

	private static UIComponent m_AdvancedTooltipDetail;

	private static UIComponent m_LandscapingTooltip;
}
