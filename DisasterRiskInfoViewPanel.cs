﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class DisasterRiskInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Gradient = base.Find<UITextureSprite>("Gradient");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UISprite uISprite3 = base.Find<UISprite>("ColorEvacuated");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[29].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[29].m_inactiveColor);
					uISprite3.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[29].m_negativeColor);
					this.m_Gradient.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[29].m_targetColor);
					this.m_Gradient.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					this.m_Gradient.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					this.m_Gradient.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					this.m_Gradient.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.m_initialized = true;
		}
	}

	private UITextureSprite m_Gradient;

	private bool m_initialized;
}
