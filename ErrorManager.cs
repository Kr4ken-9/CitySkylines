﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class ErrorManager : Singleton<ErrorManager>
{
	private void Awake()
	{
		this.m_errorMessages = new string[32];
		this.m_repeatCounts = new int[32];
		this.m_errorIndex = -1;
		this.m_bufferLock = new object();
		Application.add_logMessageReceivedThreaded(new Application.LogCallback(this.ApplicationLog));
	}

	private void OnDestroy()
	{
		Application.remove_logMessageReceivedThreaded(new Application.LogCallback(this.ApplicationLog));
	}

	private void ApplicationLog(string message, string stackTrace, LogType type)
	{
		if (type == null || type == 4 || type == 1)
		{
			string text = message + "\n" + stackTrace;
			while (!Monitor.TryEnter(this.m_bufferLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_totalCount++;
				if (this.m_lastError == text && this.m_errorIndex != -1)
				{
					this.m_repeatCounts[this.m_errorIndex]++;
				}
				else
				{
					this.m_lastError = text;
					if (++this.m_errorIndex == this.m_errorMessages.Length)
					{
						this.m_errorIndex = this.m_errorMessages.Length >> 1;
					}
					this.m_errorMessages[this.m_errorIndex] = message;
					this.m_repeatCounts[this.m_errorIndex] = 0;
				}
			}
			finally
			{
				Monitor.Exit(this.m_bufferLock);
			}
		}
	}

	public string m_lastError;

	public int m_errorIndex;

	public int m_totalCount;

	public string[] m_errorMessages;

	public int[] m_repeatCounts;

	public object m_bufferLock;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "ErrorManager");
			ErrorManager instance = Singleton<ErrorManager>.get_instance();
			while (!Monitor.TryEnter(instance.m_bufferLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				int num = instance.m_errorMessages.Length;
				int num2 = num >> 1;
				s.WriteUniqueString(instance.m_lastError);
				s.WriteInt32(instance.m_totalCount);
				s.WriteInt32((instance.m_errorIndex >= num2) ? (num - 1) : instance.m_errorIndex);
				s.WriteInt32(num);
				for (int i = 0; i < num; i++)
				{
					int num3;
					if (i < num2)
					{
						num3 = i;
					}
					else
					{
						num3 = num2 + (i + instance.m_errorIndex + num2 + 1) % num2;
					}
					s.WriteUniqueString(instance.m_errorMessages[num3]);
					s.WriteInt32(instance.m_repeatCounts[num3]);
				}
			}
			finally
			{
				Monitor.Exit(instance.m_bufferLock);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "ErrorManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "ErrorManager");
			ErrorManager instance = Singleton<ErrorManager>.get_instance();
			while (!Monitor.TryEnter(instance.m_bufferLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				instance.m_lastError = s.ReadUniqueString();
				instance.m_totalCount = s.ReadInt32();
				instance.m_errorIndex = s.ReadInt32();
				int num = s.ReadInt32();
				if (instance.m_errorMessages.Length != num)
				{
					instance.m_errorMessages = new string[num];
					instance.m_repeatCounts = new int[num];
				}
				for (int i = 0; i < num; i++)
				{
					instance.m_errorMessages[i] = s.ReadUniqueString();
					instance.m_repeatCounts[i] = s.ReadInt32();
				}
			}
			finally
			{
				Monitor.Exit(instance.m_bufferLock);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "ErrorManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "ErrorManager");
			ErrorManager instance = Singleton<ErrorManager>.get_instance();
			if (instance.m_totalCount != 0)
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Loaded data contains error logs!");
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "ErrorManager");
		}
	}
}
