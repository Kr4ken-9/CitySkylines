﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct PropInstance
{
	public void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort propID, int layerMask)
	{
		if ((this.m_flags & 68) != 0)
		{
			return;
		}
		PropInfo info = this.Info;
		Vector3 position = this.Position;
		if (!cameraInfo.CheckRenderDistance(position, info.m_maxRenderDistance))
		{
			return;
		}
		if (!cameraInfo.Intersect(position, info.m_generatedInfo.m_size.y * info.m_maxScale))
		{
			return;
		}
		float angle = this.Angle;
		Randomizer randomizer;
		randomizer..ctor((int)propID);
		float scale = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		Color color = info.GetColor(ref randomizer);
		Vector4 objectIndex;
		objectIndex..ctor(0.001953125f, 0.00260416674f, 0f, 0f);
		InstanceID id = default(InstanceID);
		id.Prop = propID;
		if (info.m_requireHeightMap)
		{
			Texture heightMap;
			Vector4 heightMapping;
			Vector4 surfaceMapping;
			Singleton<TerrainManager>.get_instance().GetHeightMapping(position, out heightMap, out heightMapping, out surfaceMapping);
			PropInstance.RenderInstance(cameraInfo, info, id, position, scale, angle, color, objectIndex, true, heightMap, heightMapping, surfaceMapping);
		}
		else
		{
			PropInstance.RenderInstance(cameraInfo, info, id, position, scale, angle, color, objectIndex, true);
		}
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, PropInfo info, InstanceID id, Vector3 position, float scale, float angle, Color color, Vector4 objectIndex, bool active)
	{
		if (info.m_prefabInitialized)
		{
			if (info.m_hasEffects && (active || info.m_alwaysActive))
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
				float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
				for (int i = 0; i < info.m_effects.Length; i++)
				{
					Vector3 position2 = matrix4x.MultiplyPoint(info.m_effects[i].m_position);
					Vector3 direction = matrix4x.MultiplyVector(info.m_effects[i].m_direction);
					EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position2, direction, 0f);
					info.m_effects[i].m_effect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, simulationTimeDelta, cameraInfo);
				}
			}
			if (info.m_hasRenderer && (cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) != 0)
			{
				if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
				{
					if (!active && !info.m_alwaysActive)
					{
						objectIndex.z = 0f;
					}
					else if (info.m_illuminationOffRange.x < 1000f || info.m_illuminationBlinkType != LightEffect.BlinkType.None)
					{
						LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
						Randomizer randomizer;
						randomizer..ctor(id.Index);
						float num = info.m_illuminationOffRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (info.m_illuminationOffRange.y - info.m_illuminationOffRange.x);
						objectIndex.z = MathUtils.SmoothStep(num + 0.01f, num - 0.01f, lightSystem.DayLightIntensity);
						if (info.m_illuminationBlinkType != LightEffect.BlinkType.None)
						{
							Vector4 blinkVector = LightEffect.GetBlinkVector(info.m_illuminationBlinkType);
							float num2 = num * 3.71f + Singleton<SimulationManager>.get_instance().m_simulationTimer / blinkVector.w;
							num2 = (num2 - Mathf.Floor(num2)) * blinkVector.w;
							float num3 = MathUtils.SmoothStep(blinkVector.x, blinkVector.y, num2);
							float num4 = MathUtils.SmoothStep(blinkVector.w, blinkVector.z, num2);
							objectIndex.z *= 1f - num3 * num4;
						}
					}
					else
					{
						objectIndex.z = 1f;
					}
				}
				if (cameraInfo == null || cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
				{
					Matrix4x4 matrix4x2 = default(Matrix4x4);
					matrix4x2.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
					PropManager instance = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock = instance.m_materialBlock;
					materialBlock.Clear();
					materialBlock.SetColor(instance.ID_Color, color);
					materialBlock.SetVector(instance.ID_ObjectIndex, objectIndex);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_33A_cp_0 = instance;
					expr_33A_cp_0.m_drawCallData.m_defaultCalls = expr_33A_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_mesh, matrix4x2, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
				}
				else if (info.m_lodMaterialCombined == null)
				{
					Matrix4x4 matrix4x3 = default(Matrix4x4);
					matrix4x3.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
					PropManager instance2 = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock2 = instance2.m_materialBlock;
					materialBlock2.Clear();
					materialBlock2.SetColor(instance2.ID_Color, color);
					materialBlock2.SetVector(instance2.ID_ObjectIndex, objectIndex);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance2.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance2.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_420_cp_0 = instance2;
					expr_420_cp_0.m_drawCallData.m_defaultCalls = expr_420_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_lodMesh, matrix4x3, info.m_lodMaterial, info.m_prefabDataLayer, null, 0, materialBlock2);
				}
				else
				{
					objectIndex.w = scale;
					info.m_lodLocations[info.m_lodCount] = new Vector4(position.x, position.y, position.z, angle);
					info.m_lodObjectIndices[info.m_lodCount] = objectIndex;
					info.m_lodColors[info.m_lodCount] = color.get_linear();
					info.m_lodMin = Vector3.Min(info.m_lodMin, position);
					info.m_lodMax = Vector3.Max(info.m_lodMax, position);
					if (++info.m_lodCount == info.m_lodLocations.Length)
					{
						PropInstance.RenderLod(cameraInfo, info);
					}
				}
			}
		}
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, PropInfo info, InstanceID id, Matrix4x4 matrix, Vector3 position, float scale, float angle, Color color, Vector4 objectIndex, bool active)
	{
		if (info.m_prefabInitialized)
		{
			if (info.m_hasEffects && (active || info.m_alwaysActive))
			{
				float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
				for (int i = 0; i < info.m_effects.Length; i++)
				{
					Vector3 position2 = matrix.MultiplyPoint(info.m_effects[i].m_position);
					Vector3 direction = matrix.MultiplyVector(info.m_effects[i].m_direction);
					EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position2, direction, 0f);
					info.m_effects[i].m_effect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, simulationTimeDelta, cameraInfo);
				}
			}
			if (info.m_hasRenderer && (cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) != 0)
			{
				if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
				{
					if (!active && !info.m_alwaysActive)
					{
						objectIndex.z = 0f;
					}
					else if (info.m_illuminationOffRange.x < 1000f || info.m_illuminationBlinkType != LightEffect.BlinkType.None)
					{
						LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
						Randomizer randomizer;
						randomizer..ctor(id.Index);
						float num = info.m_illuminationOffRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (info.m_illuminationOffRange.y - info.m_illuminationOffRange.x);
						objectIndex.z = MathUtils.SmoothStep(num + 0.01f, num - 0.01f, lightSystem.DayLightIntensity);
						if (info.m_illuminationBlinkType != LightEffect.BlinkType.None)
						{
							Vector4 blinkVector = LightEffect.GetBlinkVector(info.m_illuminationBlinkType);
							float num2 = num * 3.71f + Singleton<SimulationManager>.get_instance().m_simulationTimer / blinkVector.w;
							num2 = (num2 - Mathf.Floor(num2)) * blinkVector.w;
							float num3 = MathUtils.SmoothStep(blinkVector.x, blinkVector.y, num2);
							float num4 = MathUtils.SmoothStep(blinkVector.w, blinkVector.z, num2);
							objectIndex.z *= 1f - num3 * num4;
						}
					}
					else
					{
						objectIndex.z = 1f;
					}
				}
				if (cameraInfo == null || cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
				{
					PropManager instance = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock = instance.m_materialBlock;
					materialBlock.Clear();
					materialBlock.SetColor(instance.ID_Color, color);
					materialBlock.SetVector(instance.ID_ObjectIndex, objectIndex);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_2DF_cp_0 = instance;
					expr_2DF_cp_0.m_drawCallData.m_defaultCalls = expr_2DF_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_mesh, matrix, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
				}
				else if (info.m_lodMaterialCombined == null)
				{
					PropManager instance2 = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock2 = instance2.m_materialBlock;
					materialBlock2.Clear();
					materialBlock2.SetColor(instance2.ID_Color, color);
					materialBlock2.SetVector(instance2.ID_ObjectIndex, objectIndex);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance2.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance2.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_397_cp_0 = instance2;
					expr_397_cp_0.m_drawCallData.m_defaultCalls = expr_397_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_lodMesh, matrix, info.m_lodMaterial, info.m_prefabDataLayer, null, 0, materialBlock2);
				}
				else
				{
					objectIndex.w = scale;
					info.m_lodLocations[info.m_lodCount] = new Vector4(position.x, position.y, position.z, angle);
					info.m_lodObjectIndices[info.m_lodCount] = objectIndex;
					info.m_lodColors[info.m_lodCount] = color.get_linear();
					info.m_lodMin = Vector3.Min(info.m_lodMin, position);
					info.m_lodMax = Vector3.Max(info.m_lodMax, position);
					if (++info.m_lodCount == info.m_lodLocations.Length)
					{
						PropInstance.RenderLod(cameraInfo, info);
					}
				}
			}
		}
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, PropInfo info, InstanceID id, Vector3 position, float scale, float angle, Color color, Vector4 objectIndex, bool active, Texture heightMap, Vector4 heightMapping, Vector4 surfaceMapping)
	{
		if (info.m_prefabInitialized)
		{
			if (info.m_hasEffects && (active || info.m_alwaysActive))
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
				float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
				for (int i = 0; i < info.m_effects.Length; i++)
				{
					Vector3 position2 = matrix4x.MultiplyPoint(info.m_effects[i].m_position);
					Vector3 direction = matrix4x.MultiplyVector(info.m_effects[i].m_direction);
					EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position2, direction, 0f);
					info.m_effects[i].m_effect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, simulationTimeDelta, cameraInfo);
				}
			}
			if (info.m_hasRenderer && (cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) != 0)
			{
				if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
				{
					if (!active && !info.m_alwaysActive)
					{
						objectIndex.z = 0f;
					}
					else if (info.m_illuminationOffRange.x < 1000f || info.m_illuminationBlinkType != LightEffect.BlinkType.None)
					{
						LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
						Randomizer randomizer;
						randomizer..ctor(id.Index);
						float num = info.m_illuminationOffRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (info.m_illuminationOffRange.y - info.m_illuminationOffRange.x);
						objectIndex.z = MathUtils.SmoothStep(num + 0.01f, num - 0.01f, lightSystem.DayLightIntensity);
						if (info.m_illuminationBlinkType != LightEffect.BlinkType.None)
						{
							Vector4 blinkVector = LightEffect.GetBlinkVector(info.m_illuminationBlinkType);
							float num2 = num * 3.71f + Singleton<SimulationManager>.get_instance().m_simulationTimer / blinkVector.w;
							num2 = (num2 - Mathf.Floor(num2)) * blinkVector.w;
							float num3 = MathUtils.SmoothStep(blinkVector.x, blinkVector.y, num2);
							float num4 = MathUtils.SmoothStep(blinkVector.w, blinkVector.z, num2);
							objectIndex.z *= 1f - num3 * num4;
						}
					}
					else
					{
						objectIndex.z = 1f;
					}
				}
				if (cameraInfo == null || cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
				{
					Matrix4x4 matrix4x2 = default(Matrix4x4);
					matrix4x2.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
					PropManager instance = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock = instance.m_materialBlock;
					materialBlock.Clear();
					materialBlock.SetColor(instance.ID_Color, color);
					materialBlock.SetTexture(instance.ID_HeightMap, heightMap);
					materialBlock.SetVector(instance.ID_HeightMapping, heightMapping);
					materialBlock.SetVector(instance.ID_SurfaceMapping, surfaceMapping);
					materialBlock.SetVector(instance.ID_ObjectIndex, objectIndex);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_36A_cp_0 = instance;
					expr_36A_cp_0.m_drawCallData.m_defaultCalls = expr_36A_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_mesh, matrix4x2, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
				}
				else
				{
					if (heightMap != info.m_lodHeightMap)
					{
						if (info.m_lodCount != 0)
						{
							PropInstance.RenderLod(cameraInfo, info);
						}
						info.m_lodHeightMap = heightMap;
						info.m_lodHeightMapping = heightMapping;
						info.m_lodSurfaceMapping = surfaceMapping;
					}
					objectIndex.w = scale;
					info.m_lodLocations[info.m_lodCount] = new Vector4(position.x, position.y, position.z, angle);
					info.m_lodObjectIndices[info.m_lodCount] = objectIndex;
					info.m_lodColors[info.m_lodCount] = color.get_linear();
					info.m_lodMin = Vector3.Min(info.m_lodMin, position);
					info.m_lodMax = Vector3.Max(info.m_lodMax, position);
					if (++info.m_lodCount == info.m_lodLocations.Length)
					{
						PropInstance.RenderLod(cameraInfo, info);
					}
				}
			}
		}
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, PropInfo info, InstanceID id, Vector3 position, float scale, float angle, Color color, Vector4 objectIndex, bool active, Texture heightMap, Vector4 heightMapping, Vector4 surfaceMapping, Texture waterHeightMap, Vector4 waterHeightMapping, Vector4 waterSurfaceMapping)
	{
		if (info.m_prefabInitialized)
		{
			if (info.m_hasEffects && (active || info.m_alwaysActive))
			{
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
				float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
				for (int i = 0; i < info.m_effects.Length; i++)
				{
					Vector3 position2 = matrix4x.MultiplyPoint(info.m_effects[i].m_position);
					Vector3 direction = matrix4x.MultiplyVector(info.m_effects[i].m_direction);
					EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position2, direction, 0f);
					info.m_effects[i].m_effect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, simulationTimeDelta, cameraInfo);
				}
			}
			if (info.m_hasRenderer && (cameraInfo.m_layerMask & 1 << info.m_prefabDataLayer) != 0)
			{
				if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
				{
					if (!active && !info.m_alwaysActive)
					{
						objectIndex.z = 0f;
					}
					else if (info.m_illuminationOffRange.x < 1000f || info.m_illuminationBlinkType != LightEffect.BlinkType.None)
					{
						LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
						Randomizer randomizer;
						randomizer..ctor(id.Index);
						float num = info.m_illuminationOffRange.x + (float)randomizer.Int32(100000u) * 1E-05f * (info.m_illuminationOffRange.y - info.m_illuminationOffRange.x);
						objectIndex.z = MathUtils.SmoothStep(num + 0.01f, num - 0.01f, lightSystem.DayLightIntensity);
						if (info.m_illuminationBlinkType != LightEffect.BlinkType.None)
						{
							Vector4 blinkVector = LightEffect.GetBlinkVector(info.m_illuminationBlinkType);
							float num2 = num * 3.71f + Singleton<SimulationManager>.get_instance().m_simulationTimer / blinkVector.w;
							num2 = (num2 - Mathf.Floor(num2)) * blinkVector.w;
							float num3 = MathUtils.SmoothStep(blinkVector.x, blinkVector.y, num2);
							float num4 = MathUtils.SmoothStep(blinkVector.w, blinkVector.z, num2);
							objectIndex.z *= 1f - num3 * num4;
						}
					}
					else
					{
						objectIndex.z = 1f;
					}
				}
				if (cameraInfo == null || cameraInfo.CheckRenderDistance(position, info.m_lodRenderDistance))
				{
					Matrix4x4 matrix4x2 = default(Matrix4x4);
					matrix4x2.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
					PropManager instance = Singleton<PropManager>.get_instance();
					MaterialPropertyBlock materialBlock = instance.m_materialBlock;
					materialBlock.Clear();
					materialBlock.SetColor(instance.ID_Color, color);
					materialBlock.SetTexture(instance.ID_HeightMap, heightMap);
					materialBlock.SetVector(instance.ID_HeightMapping, heightMapping);
					materialBlock.SetVector(instance.ID_SurfaceMapping, surfaceMapping);
					materialBlock.SetVector(instance.ID_ObjectIndex, objectIndex);
					materialBlock.SetTexture(instance.ID_WaterHeightMap, waterHeightMap);
					materialBlock.SetVector(instance.ID_WaterHeightMapping, waterHeightMapping);
					materialBlock.SetVector(instance.ID_WaterSurfaceMapping, waterSurfaceMapping);
					if (info.m_rollLocation != null)
					{
						info.m_material.SetVectorArray(instance.ID_RollLocation, info.m_rollLocation);
						info.m_material.SetVectorArray(instance.ID_RollParams, info.m_rollParams);
					}
					PropManager expr_39A_cp_0 = instance;
					expr_39A_cp_0.m_drawCallData.m_defaultCalls = expr_39A_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(info.m_mesh, matrix4x2, info.m_material, info.m_prefabDataLayer, null, 0, materialBlock);
				}
				else
				{
					if (heightMap != info.m_lodHeightMap || waterHeightMap != info.m_lodWaterHeightMap)
					{
						if (info.m_lodCount != 0)
						{
							PropInstance.RenderLod(cameraInfo, info);
						}
						info.m_lodHeightMap = heightMap;
						info.m_lodHeightMapping = heightMapping;
						info.m_lodSurfaceMapping = surfaceMapping;
						info.m_lodWaterHeightMap = waterHeightMap;
						info.m_lodWaterHeightMapping = waterHeightMapping;
						info.m_lodWaterSurfaceMapping = waterSurfaceMapping;
					}
					objectIndex.w = scale;
					info.m_lodLocations[info.m_lodCount] = new Vector4(position.x, position.y, position.z, angle);
					info.m_lodObjectIndices[info.m_lodCount] = objectIndex;
					info.m_lodColors[info.m_lodCount] = color.get_linear();
					info.m_lodMin = Vector3.Min(info.m_lodMin, position);
					info.m_lodMax = Vector3.Max(info.m_lodMax, position);
					if (++info.m_lodCount == info.m_lodLocations.Length)
					{
						PropInstance.RenderLod(cameraInfo, info);
					}
				}
			}
		}
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, PropInfo info)
	{
		PropManager instance = Singleton<PropManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_lodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_lodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else if (info.m_lodCount <= 8)
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMeshCombined16;
			num = 16;
		}
		for (int i = info.m_lodCount; i < num; i++)
		{
			info.m_lodLocations[i] = cameraInfo.m_forward * -100000f;
			info.m_lodObjectIndices[i] = Vector4.get_zero();
			info.m_lodColors[i] = new Color(0f, 0f, 0f, 0f);
		}
		materialBlock.SetVectorArray(instance.ID_PropLocation, info.m_lodLocations);
		materialBlock.SetVectorArray(instance.ID_PropObjectIndex, info.m_lodObjectIndices);
		materialBlock.SetVectorArray(instance.ID_PropColor, info.m_lodColors);
		if (info.m_requireHeightMap)
		{
			materialBlock.SetTexture(instance.ID_HeightMap, info.m_lodHeightMap);
			materialBlock.SetVector(instance.ID_HeightMapping, info.m_lodHeightMapping);
			materialBlock.SetVector(instance.ID_SurfaceMapping, info.m_lodSurfaceMapping);
		}
		if (info.m_requireWaterMap)
		{
			materialBlock.SetTexture(instance.ID_WaterHeightMap, info.m_lodWaterHeightMap);
			materialBlock.SetVector(instance.ID_WaterHeightMapping, info.m_lodWaterHeightMapping);
			materialBlock.SetVector(instance.ID_WaterSurfaceMapping, info.m_lodWaterSurfaceMapping);
		}
		if (info.m_rollLocation != null)
		{
			info.m_lodMaterialCombined.SetVectorArray(instance.ID_RollLocation, info.m_rollLocation);
			info.m_lodMaterialCombined.SetVectorArray(instance.ID_RollParams, info.m_rollParams);
		}
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_lodMin - new Vector3(100f, 100f, 100f), info.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			PropManager expr_280_cp_0 = instance;
			expr_280_cp_0.m_drawCallData.m_lodCalls = expr_280_cp_0.m_drawCallData.m_lodCalls + 1;
			PropManager expr_293_cp_0 = instance;
			expr_293_cp_0.m_drawCallData.m_batchedCalls = expr_293_cp_0.m_drawCallData.m_batchedCalls + (info.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_lodMaterialCombined, info.m_prefabDataLayer, null, 0, materialBlock);
		}
		info.m_lodCount = 0;
	}

	public PropInfo Info
	{
		get
		{
			return PrefabCollection<PropInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public bool Single
	{
		get
		{
			return (this.m_flags & 16) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 16;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -17);
			}
		}
	}

	public bool FixedHeight
	{
		get
		{
			return (this.m_flags & 32) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 32;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -33);
			}
		}
	}

	public bool Blocked
	{
		get
		{
			return (this.m_flags & 64) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 64;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -65);
			}
		}
	}

	public bool Hidden
	{
		get
		{
			return (this.m_flags & 4) != 0;
		}
		set
		{
			if (value)
			{
				this.m_flags |= 4;
			}
			else
			{
				this.m_flags = (ushort)((int)this.m_flags & -5);
			}
		}
	}

	public Vector3 Position
	{
		get
		{
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				Vector3 result;
				result.x = (float)this.m_posX * 0.0164794922f;
				result.y = (float)this.m_posY * 0.015625f;
				result.z = (float)this.m_posZ * 0.0164794922f;
				return result;
			}
			Vector3 result2;
			result2.x = (float)this.m_posX * 0.263671875f;
			result2.y = (float)this.m_posY * 0.015625f;
			result2.z = (float)this.m_posZ * 0.263671875f;
			return result2;
		}
		set
		{
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				this.m_posX = (short)Mathf.Clamp(Mathf.RoundToInt(value.x * 60.68148f), -32767, 32767);
				this.m_posZ = (short)Mathf.Clamp(Mathf.RoundToInt(value.z * 60.68148f), -32767, 32767);
				this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(value.y * 64f), 0, 65535);
			}
			else
			{
				this.m_posX = (short)Mathf.Clamp(Mathf.RoundToInt(value.x * 3.79259253f), -32767, 32767);
				this.m_posZ = (short)Mathf.Clamp(Mathf.RoundToInt(value.z * 3.79259253f), -32767, 32767);
				this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(value.y * 64f), 0, 65535);
			}
		}
	}

	public float Angle
	{
		get
		{
			return (float)this.m_angle * 9.58738E-05f;
		}
		set
		{
			this.m_angle = (ushort)Mathf.RoundToInt(value * 10430.3779f);
		}
	}

	public void CalculateProp(ushort propID)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if ((this.m_flags & 32) == 0)
		{
			Vector3 position = this.Position;
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			this.m_posY = (ushort)Mathf.Clamp(Mathf.RoundToInt(position.y * 64f), 0, 65535);
		}
		this.CheckOverlap(propID);
	}

	private void CheckOverlap(ushort propID)
	{
		PropInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		ItemClass.CollisionType collisionType;
		if ((this.m_flags & 32) == 0)
		{
			collisionType = ItemClass.CollisionType.Terrain;
		}
		else
		{
			collisionType = ItemClass.CollisionType.Elevated;
		}
		Randomizer randomizer;
		randomizer..ctor((int)propID);
		float num = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num2 = info.m_generatedInfo.m_size.y * num;
		Vector3 position = this.Position;
		float y = position.y;
		float maxY = position.y + num2;
		float num3 = (!this.Single) ? 4.5f : 0.3f;
		Quad2 quad = default(Quad2);
		Vector2 vector = VectorUtils.XZ(position);
		quad.a = vector + new Vector2(-num3, -num3);
		quad.b = vector + new Vector2(-num3, num3);
		quad.c = vector + new Vector2(num3, num3);
		quad.d = vector + new Vector2(num3, -num3);
		bool blocked = false;
		if (info.m_class != null)
		{
			if (Singleton<NetManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0))
			{
				blocked = true;
			}
			if (Singleton<BuildingManager>.get_instance().OverlapQuad(quad, y, maxY, collisionType, info.m_class.m_layer, 0, 0, 0))
			{
				blocked = true;
			}
		}
		this.Blocked = blocked;
	}

	public void UpdateProp(ushort propID)
	{
		if ((this.m_flags & 1) == 0)
		{
			return;
		}
		PropInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		if (info.m_createRuining)
		{
			Vector3 position = this.Position;
			float minX = position.x - 4f;
			float minZ = position.z - 4f;
			float maxX = position.x + 4f;
			float maxZ = position.z + 4f;
			TerrainModify.UpdateArea(minX, minZ, maxX, maxZ, false, true, false);
		}
	}

	public void TerrainUpdated(ushort propID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if (!this.Blocked)
		{
			PropInstance.TerrainUpdated(this.Info, this.Position);
		}
	}

	public static void TerrainUpdated(PropInfo info, Vector3 position)
	{
		if (info == null)
		{
			return;
		}
		if (info.m_createRuining)
		{
			Vector3 a = position + new Vector3(-4.5f, 0f, -4.5f);
			Vector3 b = position + new Vector3(-4.5f, 0f, 4.5f);
			Vector3 c = position + new Vector3(4.5f, 0f, 4.5f);
			Vector3 d = position + new Vector3(4.5f, 0f, -4.5f);
			TerrainModify.Edges edges = TerrainModify.Edges.All;
			TerrainModify.Heights heights = TerrainModify.Heights.None;
			TerrainModify.Surface surface = TerrainModify.Surface.Ruined;
			TerrainModify.ApplyQuad(a, b, c, d, edges, heights, surface);
		}
	}

	public void AfterTerrainUpdated(ushort propID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & 3) != 1)
		{
			return;
		}
		if ((this.m_flags & 32) == 0)
		{
			Vector3 position = this.Position;
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			ushort num = (ushort)Mathf.Clamp(Mathf.RoundToInt(position.y * 64f), 0, 65535);
			if (num != this.m_posY)
			{
				bool blocked = this.Blocked;
				this.m_posY = num;
				this.CheckOverlap(propID);
				bool blocked2 = this.Blocked;
				if (blocked2 != blocked)
				{
					Singleton<PropManager>.get_instance().UpdateProp(propID);
				}
				else if (!blocked2)
				{
					Singleton<PropManager>.get_instance().UpdatePropRenderer(propID, true);
				}
			}
		}
	}

	public bool OverlapQuad(ushort propID, Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType)
	{
		if (this.Hidden)
		{
			return false;
		}
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		float num = 0.3f;
		Vector3 position = this.Position;
		if (position.x - num > vector2.x || position.x + num < vector.x)
		{
			return false;
		}
		if (position.z - num > vector2.y || position.z + num < vector.y)
		{
			return false;
		}
		PropInfo info = this.Info;
		Randomizer randomizer;
		randomizer..ctor((int)propID);
		float num2 = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num3 = info.m_generatedInfo.m_size.y * num2;
		ItemClass.CollisionType collisionType2 = ItemClass.CollisionType.Terrain;
		if ((this.m_flags & 32) != 0)
		{
			collisionType2 = ItemClass.CollisionType.Elevated;
		}
		float y = position.y;
		float maxY2 = position.y + num3;
		if (!ItemClass.CheckCollisionType(minY, maxY, y, maxY2, collisionType, collisionType2))
		{
			return false;
		}
		Vector2 vector3 = VectorUtils.XZ(position);
		Quad2 quad2 = default(Quad2);
		quad2.a = vector3 + new Vector2(-num, num);
		quad2.b = vector3 + new Vector2(num, num);
		quad2.c = vector3 + new Vector2(num, -num);
		quad2.d = vector3 + new Vector2(-num, -num);
		return quad.Intersect(quad2);
	}

	public bool RayCast(ushort propID, Segment3 ray, out float t, out float targetSqr)
	{
		t = 2f;
		targetSqr = 0f;
		if (this.Blocked)
		{
			return false;
		}
		PropInfo info = this.Info;
		Randomizer randomizer;
		randomizer..ctor((int)propID);
		float num = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float num2 = info.m_generatedInfo.m_size.y * num;
		float num3 = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * num * 0.5f;
		Vector3 position = this.Position;
		Bounds bounds;
		bounds..ctor(new Vector3(position.x, position.y + num2 * 0.5f, position.z), new Vector3(num3, num2, num3));
		if (!bounds.IntersectRay(new Ray(ray.a, ray.b - ray.a)))
		{
			return false;
		}
		float num4 = (info.m_generatedInfo.m_size.x + info.m_generatedInfo.m_size.z) * num * 0.125f;
		float num5 = Mathf.Min(num4, num2 * 0.45f);
		Segment3 segment;
		segment..ctor(position, position);
		segment.a.y = segment.a.y + num5;
		segment.b.y = segment.b.y + (num2 - num5);
		bool result = false;
		float num7;
		float num8;
		float num6 = ray.DistanceSqr(segment, ref num7, ref num8);
		if (num6 < num4 * num4)
		{
			t = num7;
			targetSqr = num6;
			result = true;
		}
		if (Segment1.Intersect(ray.a.y, ray.b.y, position.y, ref num7))
		{
			num4 = num3;
			num6 = Vector3.SqrMagnitude(ray.Position(num7) - position);
			if (num6 < num4 * num4 && num7 < t)
			{
				t = num7;
				targetSqr = num6;
				result = true;
			}
		}
		return result;
	}

	public bool CalculateGroupData(ushort propID, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		if (this.Blocked)
		{
			return false;
		}
		PropInfo info = this.Info;
		return (info.m_prefabDataLayer == layer || info.m_effectLayer == layer) && PropInstance.CalculateGroupData(info, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
	}

	public static bool CalculateGroupData(PropInfo info, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
		if (info.m_prefabDataLayer == layer)
		{
			return true;
		}
		if (info.m_effectLayer == layer || (info.m_effectLayer == lightSystem.m_lightLayer && layer == lightSystem.m_lightLayerFloating))
		{
			bool result = false;
			for (int i = 0; i < info.m_effects.Length; i++)
			{
				if (info.m_effects[i].m_effect.CalculateGroupData(layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
				{
					result = true;
				}
			}
			return result;
		}
		return false;
	}

	public void PopulateGroupData(ushort propID, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if (this.Blocked)
		{
			return;
		}
		PropInfo info = this.Info;
		if (info.m_prefabDataLayer == layer || info.m_effectLayer == layer)
		{
			Vector3 position = this.Position;
			Randomizer randomizer;
			randomizer..ctor((int)propID);
			float scale = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
			float angle = this.Angle;
			Color color = info.GetColor(ref randomizer);
			PropInstance.PopulateGroupData(info, layer, new InstanceID
			{
				Prop = propID
			}, position, scale, angle, color, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
	}

	public static void PopulateGroupData(PropInfo info, int layer, InstanceID id, Vector3 position, float scale, float angle, Color color, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
		if (info.m_prefabDataLayer == layer)
		{
			float num = info.m_generatedInfo.m_size.y * scale;
			float num2 = Mathf.Max(info.m_generatedInfo.m_size.x, info.m_generatedInfo.m_size.z) * scale * 0.5f;
			min = Vector3.Min(min, position - new Vector3(num2, 0f, num2));
			max = Vector3.Max(max, position + new Vector3(num2, num, num2));
			maxRenderDistance = Mathf.Max(maxRenderDistance, info.m_maxRenderDistance);
			maxInstanceDistance = Mathf.Max(maxInstanceDistance, info.m_maxRenderDistance);
		}
		else if (info.m_effectLayer == layer || (info.m_effectLayer == lightSystem.m_lightLayer && layer == lightSystem.m_lightLayerFloating))
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(position, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), new Vector3(scale, scale, scale));
			for (int i = 0; i < info.m_effects.Length; i++)
			{
				Vector3 pos = matrix4x.MultiplyPoint(info.m_effects[i].m_position);
				Vector3 dir = matrix4x.MultiplyVector(info.m_effects[i].m_direction);
				info.m_effects[i].m_effect.PopulateGroupData(layer, id, pos, dir, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
			}
		}
	}

	public ushort m_nextGridProp;

	public short m_posX;

	public short m_posZ;

	public ushort m_posY;

	public ushort m_angle;

	public ushort m_flags;

	public ushort m_infoIndex;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Hidden = 4,
		Single = 16,
		FixedHeight = 32,
		Blocked = 64,
		All = 65535
	}
}
