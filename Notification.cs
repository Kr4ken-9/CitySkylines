﻿using System;
using System.Runtime.InteropServices;
using ColossalFramework;
using UnityEngine;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct Notification
{
	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, Notification.Problem problems, Vector3 position, float scale)
	{
		NotificationManager instance = Singleton<NotificationManager>.get_instance();
		int num = 0;
		for (int i = 0; i < 39; i++)
		{
			if ((problems & (Notification.Problem)(1L << i)) != Notification.Problem.None)
			{
				num++;
			}
		}
		if (num != 0)
		{
			Vector4 @params;
			@params..ctor(0.1f, 1f, 5f, 0f);
			int num2 = 0;
			if ((problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				num2 = 78;
				@params.x = 0.2f;
				@params.z = 6f;
			}
			else if ((problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
			{
				num2 = 39;
				@params.x = 0.2f;
				@params.z = 6f;
			}
			if (instance.FadeAwayNotifications)
			{
				float num3 = Vector3.SqrMagnitude(cameraInfo.m_position - position) * 1E-06f;
				@params.y = Mathf.Max(0f, 1f - num3 * num3);
			}
			num = (int)Time.get_realtimeSinceStartup() % num;
			for (int j = 0; j < 39; j++)
			{
				if ((problems & (Notification.Problem)(1L << j)) != Notification.Problem.None && num-- == 0)
				{
					NotificationManager.BufferedItem item;
					item.m_position = new Vector4(position.x, position.y, position.z, scale);
					item.m_params = @params;
					item.m_distanceSqr = Vector3.SqrMagnitude(position - cameraInfo.m_position);
					item.m_regionIndex = num2 + j;
					instance.m_bufferedItems.Add(item);
					return;
				}
			}
		}
	}

	public static bool CalculateGroupData(ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return true;
	}

	public static void PopulateGroupData(Notification.Problem problems, Vector3 position, float scale, int groupX, int groupZ, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		min = Vector3.Min(min, position - new Vector3(10f, 10f, 10f));
		max = Vector3.Max(max, position + new Vector3(10f, 10f, 10f));
		maxRenderDistance = Mathf.Max(maxRenderDistance, 100000f);
		maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
		if (Singleton<NotificationManager>.get_instance().FadeAwayNotifications)
		{
			return;
		}
		int num = 25;
		int num2 = 45 - num >> 1;
		groupX -= num2;
		groupZ -= num2;
		if (groupX >= 0 && groupZ >= 0 && groupX < num && groupZ < num)
		{
			int num3 = (groupZ * num + groupX) * 39;
			for (int i = 0; i < 39; i++)
			{
				Notification.Problem problem = (Notification.Problem)(1L << i);
				if ((problems & problem) != Notification.Problem.None)
				{
					problem |= (problems & (Notification.Problem.MajorProblem | Notification.Problem.FatalProblem));
					NotificationManager.GroupData groupData = Singleton<NotificationManager>.get_instance().m_groupData[num3 + i];
					groupData.m_problems = Notification.AddProblems(groupData.m_problems, problem);
					groupData.m_size += scale;
					groupData.m_minPos = Vector3.Min(groupData.m_minPos, position);
					groupData.m_maxPos = Vector3.Max(groupData.m_maxPos, position);
					Singleton<NotificationManager>.get_instance().m_groupData[num3 + i] = groupData;
				}
			}
		}
	}

	public static Notification.Problem AddProblems(Notification.Problem problems1, Notification.Problem problems2)
	{
		if ((problems2 & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			if ((problems1 & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				return problems1 | problems2;
			}
			return problems2;
		}
		else if ((problems2 & Notification.Problem.MajorProblem) != Notification.Problem.None)
		{
			if ((problems1 & Notification.Problem.FatalProblem) != Notification.Problem.None)
			{
				return problems1;
			}
			if ((problems1 & Notification.Problem.MajorProblem) != Notification.Problem.None)
			{
				return problems1 | problems2;
			}
			return problems2;
		}
		else
		{
			if ((problems1 & (Notification.Problem.MajorProblem | Notification.Problem.FatalProblem)) != Notification.Problem.None)
			{
				return problems1;
			}
			return problems1 | problems2;
		}
	}

	public static Notification.Problem RemoveProblems(Notification.Problem problems1, Notification.Problem problems2)
	{
		problems2 &= ~(Notification.Problem.MajorProblem | Notification.Problem.FatalProblem);
		problems1 &= ~problems2;
		if ((problems1 & ~(Notification.Problem.MajorProblem | Notification.Problem.FatalProblem)) == Notification.Problem.None)
		{
			problems1 = Notification.Problem.None;
		}
		return problems1;
	}

	public const int ProblemCount = 39;

	[Flags]
	public enum Problem : ulong
	{
		None = 0uL,
		[EnumPosition("Normal", "BuildingNotificationGarbagefirst", 0), EnumPosition("Major", "BuildingNotificationGarbage", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Garbage", 0)]
		Garbage = 1uL,
		[EnumPosition("Normal", "BuildingNotificationElectricityFirst", 0), EnumPosition("Major", "BuildingNotificationElectricity", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Electricity", 0)]
		Electricity = 2uL,
		[EnumPosition("Normal", "BuildingNotificationWaterFirst", 0), EnumPosition("Major", "BuildingNotificationWater", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Water", 0)]
		Water = 4uL,
		[EnumPosition("Normal", "BuildingNotificationFire", 0), EnumPosition("Major", "BuildingNotificationFire", 0), EnumPosition("Fatal", "BuildingNotificationBurnedDown", 0), EnumPosition("Text", "Fire", 0)]
		Fire = 8uL,
		[EnumPosition("Normal", "BuildingNotificationSicknessDirtyWater", 0), EnumPosition("Major", "BuildingNotificationHealthcare", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "DirtyWater", 0)]
		DirtyWater = 16uL,
		[EnumPosition("Normal", "BuildingNotificationCrime", 0), EnumPosition("Major", "BuildingNotificationPolice", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Crime", 0)]
		Crime = 32uL,
		[EnumPosition("Normal", "BuildingNotificationSicknessGroundPollution", 0), EnumPosition("Major", "BuildingNotificationHealthcare", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Pollution", 0)]
		Pollution = 64uL,
		[EnumPosition("Normal", "BuildingNotificationOff", 0), EnumPosition("Major", "BuildingNotificationOff", 0), EnumPosition("Fatal", "BuildingNotificationOff", 0), EnumPosition("Text", "TurnedOff", 0)]
		TurnedOff = 128uL,
		[EnumPosition("Normal", "BuildingNotificationToofewServices", 0), EnumPosition("Major", "BuildingNotificationAbouttoDowngrade", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "ToofewServices", 0)]
		TooFewServices = 256uL,
		[EnumPosition("Normal", "BuildingNotificationLandValueLow", 0), EnumPosition("Major", "BuildingNotificationAbouttoDowngrade", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "LandValueLow", 0)]
		LandValueLow = 512uL,
		[EnumPosition("Normal", "BuildingNotificationElectricityNotConnected", 0), EnumPosition("Major", "BuildingNotificationElectricityNotConnected", 0), EnumPosition("Fatal", "BuildingNotificationElectricityNotConnected", 0), EnumPosition("Text", "ElectricityNotConnected", 0)]
		ElectricityNotConnected = 1024uL,
		[EnumPosition("Normal", "BuildingNotificationNoFuelFirst", 0), EnumPosition("Major", "BuildingNotificationNoFuel", 0), EnumPosition("Fatal", "BuildingNotificationNoFuel", 0), EnumPosition("Text", "NoFuel", 0)]
		NoFuel = 2048uL,
		[EnumPosition("Normal", "BuildingNotificationRoadNotConnected", 0), EnumPosition("Major", "BuildingNotificationRoadNotConnected", 0), EnumPosition("Fatal", "BuildingNotificationRoadNotConnected", 0), EnumPosition("Text", "RoadNotConnected", 0)]
		RoadNotConnected = 4096uL,
		[EnumPosition("Normal", "BuildingNotificationWaterNotConnected", 0), EnumPosition("Major", "BuildingNotificationWaterNotConnected", 0), EnumPosition("Fatal", "BuildingNotificationWaterNotConnected", 0), EnumPosition("Text", "WaterNotConnected", 0)]
		WaterNotConnected = 8192uL,
		[EnumPosition("Normal", "BuildingNotificationSewageFirst", 0), EnumPosition("Major", "BuildingNotificationSewage", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Sewage", 0)]
		Sewage = 16384uL,
		[EnumPosition("Normal", "BuildingNotificationSomeoneDied", 0), EnumPosition("Major", "BuildingNotificationDead", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Death", 0)]
		Death = 32768uL,
		[EnumPosition("Normal", "BuildingNotificationLandfillFull", 0), EnumPosition("Major", "BuildingNotificationLandfillFull", 0), EnumPosition("Fatal", "BuildingNotificationLandfillFull", 0), EnumPosition("Text", "LandfillFull", 0)]
		LandfillFull = 65536uL,
		[EnumPosition("Normal", "NotificationLineNotConnected", 0), EnumPosition("Major", "NotificationLineNotConnected", 0), EnumPosition("Fatal", "NotificationLineNotConnected", 0), EnumPosition("Text", "LineNotConnected", 0)]
		LineNotConnected = 131072uL,
		[EnumPosition("Normal", "BuildingNotificationNoCustomers", 0), EnumPosition("Major", "BuildingNotificationNoCustomers", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoCustomers", 0)]
		NoCustomers = 262144uL,
		[EnumPosition("Normal", "BuildingNotificationNoResources", 0), EnumPosition("Major", "BuildingNotificationNoResources", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoResources", 0)]
		NoResources = 524288uL,
		[EnumPosition("Normal", "BuildingNotificationNotEnoughGoodsCommercial", 0), EnumPosition("Major", "BuildingNotificationNotEnoughGoodsCommercial", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoGoods", 0)]
		NoGoods = 1048576uL,
		[EnumPosition("Normal", "BuildingNotificationNoPlaceforGoods", 0), EnumPosition("Major", "BuildingNotificationNoPlaceforGoods", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoPlaceforGoods", 0)]
		NoPlaceforGoods = 2097152uL,
		[EnumPosition("Normal", "BuildingNotificationNoWorkersIndustrial", 0), EnumPosition("Major", "BuildingNotificationNoWorkersIndustrial", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoWorkers", 0)]
		NoWorkers = 4194304uL,
		[EnumPosition("Normal", "BuildingNotificationNoEducatedWorkersCommercial", 0), EnumPosition("Major", "BuildingNotificationNoEducatedWorkersCommercial", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoEducatedWorkers", 0)]
		NoEducatedWorkers = 8388608uL,
		[EnumPosition("Normal", "BuildingNotificationSicknessNoise", 0), EnumPosition("Major", "BuildingNotificationHealthcare", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Noise", 0)]
		Noise = 16777216uL,
		[EnumPosition("Normal", "BuildingNotificationLandfillEmptying", 0), EnumPosition("Major", "BuildingNotificationLandfillEmptying", 0), EnumPosition("Fatal", "BuildingNotificationLandfillEmptying", 0), EnumPosition("Text", "Emptying", 0)]
		Emptying = 33554432uL,
		[EnumPosition("Normal", "BuildingNotificationTaxesFirst", 0), EnumPosition("Major", "BuildingNotificationTaxes", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "TaxesTooHigh", 0)]
		TaxesTooHigh = 67108864uL,
		[EnumPosition("Normal", "BuildingNotificationEmptyingFinished", 0), EnumPosition("Major", "BuildingNotificationEmptyingFinished", 0), EnumPosition("Fatal", "BuildingNotificationEmptyingFinished", 0), EnumPosition("Text", "EmptyingFinished", 0)]
		EmptyingFinished = 134217728uL,
		[EnumPosition("Normal", "BuildingNotificationFloodedFirst", 0), EnumPosition("Major", "BuildingNotificationFlooded", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Flood", 0)]
		Flood = 268435456uL,
		[EnumPosition("Normal", "BuildingNotificationSnow", 0), EnumPosition("Major", "BuildingNotificationSnow", 0), EnumPosition("Fatal", "BuildingNotificationSnow", 0), EnumPosition("Text", "Snow", 0)]
		Snow = 536870912uL,
		[EnumPosition("Normal", "BuildingNotificationDepotNotConnected", 0), EnumPosition("Major", "BuildingNotificationDepotNotConnected", 0), EnumPosition("Fatal", "BuildingNotificationDepotNotConnected", 0), EnumPosition("Text", "DepotNotConnected", 0)]
		DepotNotConnected = 1073741824uL,
		[EnumPosition("Normal", "BuildingNotificationHeatingNotConnected", 0), EnumPosition("Major", "BuildingNotificationHeatingNotConnected", 0), EnumPosition("Fatal", "BuildingNotificationHeatingNotConnected", 0), EnumPosition("Text", "HeatingNotConnected", 0)]
		HeatingNotConnected = 2147483648uL,
		[EnumPosition("Normal", "BuildingNotificationHeating", 0), EnumPosition("Major", "BuildingNotificationHeatingCritical", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "Heating", 0)]
		Heating = 4294967296uL,
		[EnumPosition("Normal", "BuildingNotificationNoTracks", 0), EnumPosition("Major", "BuildingNotificationNoTracks", 0), EnumPosition("Fatal", "BuildingNotificationNoTracks", 0), EnumPosition("Text", "TrackNotConnected", 0)]
		TrackNotConnected = 8589934592uL,
		[EnumPosition("Normal", "BuildingNotificationFire", 0), EnumPosition("Major", "BuildingNotificationFire", 0), EnumPosition("Fatal", "BuildingNotificationCollapsed", 0), EnumPosition("Text", "StructureDamaged", 0)]
		StructureDamaged = 17179869184uL,
		[EnumPosition("Normal", "BuildingNotificationFire", 0), EnumPosition("Major", "BuildingNotificationFire", 0), EnumPosition("Fatal", "BuildingNotificationResponded", 0), EnumPosition("Text", "ReadyForRebuild", 0)]
		StructureVisited = 34359738368uL,
		[EnumPosition("Normal", "BuildingNotificationNotEnoughGoodsCommercial", 0), EnumPosition("Major", "BuildingNotificationNotEnoughGoodsShelter", 0), EnumPosition("Fatal", "BuildingNotificationAbandoned", 0), EnumPosition("Text", "NoFood", 0)]
		NoFood = 68719476736uL,
		[EnumPosition("Normal", "BuildingNotificationEvacuating", 0), EnumPosition("Major", "BuildingNotificationEvacuating", 0), EnumPosition("Fatal", "BuildingNotificationEvacuating", 0), EnumPosition("Text", "Evacuating", 0)]
		Evacuating = 137438953472uL,
		[EnumPosition("Normal", "BuildingNotificationFire", 0), EnumPosition("Major", "BuildingNotificationFire", 0), EnumPosition("Fatal", "BuildingNotificationRespondedService", 0), EnumPosition("Text", "ReadyForRebuild", 0)]
		StructureVisitedService = 274877906944uL,
		MajorProblem = 4611686018427387904uL,
		FatalProblem = 9223372036854775808uL
	}
}
