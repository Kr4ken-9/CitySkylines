﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Security;
using ColossalFramework.UI;
using UnityEngine;

public class ParadoxAccountLogin : UICustomControl
{
	private void OnLogout()
	{
		UICheckBox uICheckBox = base.Find<UICheckBox>("RememberMe");
		UIButton uIButton = base.Find<UIButton>("Login");
		UIButton uIButton2 = base.Find<UIButton>("Create");
		UITextField uITextField = base.Find<UITextField>("UsernameField");
		UITextField uITextField2 = base.Find<UITextField>("PasswordField");
		uITextField.set_text("email@email.com");
		uITextField2.set_text("password");
		uICheckBox.set_isEnabled(true);
		uIButton.set_isEnabled(true);
		uIButton2.set_isEnabled(true);
		uITextField.set_isEnabled(true);
		uITextField2.set_isEnabled(true);
		this.m_SavedUsername.Delete();
		this.m_SavedPassword.Delete();
		this.m_RememberMe.set_value(false);
		uICheckBox.set_isChecked(false);
		this.Show();
	}

	private void OnEnable()
	{
		Singleton<PopsManager>.get_instance().EventLoginChanged += new PopsManager.OnLoginChangedHandler(this.OnLoginChanged);
	}

	private void OnDisable()
	{
		Singleton<PopsManager>.get_instance().EventLoginChanged -= new PopsManager.OnLoginChangedHandler(this.OnLoginChanged);
		ValueAnimator.Cancel(base.GetType().ToString());
	}

	private void OnLoginChanged()
	{
		if (!Singleton<PopsManager>.get_instance().LoggedIn)
		{
			this.OnLogout();
		}
	}

	private static string Encode(string str)
	{
		if (string.IsNullOrEmpty(str))
		{
			return str;
		}
		SimpleAES simpleAES = new SimpleAES();
		return simpleAES.Encrypt(str);
	}

	private static string Decode(string str)
	{
		if (string.IsNullOrEmpty(str))
		{
			return str;
		}
		SimpleAES simpleAES = new SimpleAES();
		return simpleAES.Decrypt(str);
	}

	private void Awake()
	{
		base.get_component().Hide();
		string text = this.m_SavedUsername;
		base.Find<UITextField>("PasswordField").set_text(this.m_SavedPassword);
		if (this.m_SavedUsername.get_exists() && this.m_SavedPassword.get_exists())
		{
			Singleton<PopsManager>.get_instance().LogInWithAuthToken(ParadoxAccountLogin.Decode(this.m_SavedPassword), new Action<PopsManager.AccountActionResult>(this.OnAutoLogin));
		}
		else
		{
			base.Invoke("Show", this.m_TimeToAppear);
		}
		Vector2 vector = base.get_component().get_relativePosition();
		this.m_HiddenPosition = new Vector2(vector.x, (float)(base.get_component().GetUIView().get_fixedHeight() + 20));
		this.m_NormalPosition = new Vector2(vector.x, (float)base.get_component().GetUIView().get_fixedHeight() - base.get_component().get_size().y);
		this.m_CollapsedPosition = new Vector2(vector.x, (float)base.get_component().GetUIView().get_fixedHeight() - base.Find<UIButton>("CaptionButton").get_size().y);
		base.Find<UICheckBox>("RememberMe").set_isChecked(this.m_RememberMe);
		base.Find<UITextField>("UsernameField").set_text(text);
	}

	private void OnAutoLogin(PopsManager.AccountActionResult result)
	{
		if (result.Success)
		{
			string str = Singleton<PopsManager>.get_instance().AuthTokenRetrieve();
			this.m_LoginUsed.set_value(true);
			this.m_SavedPassword.set_value(ParadoxAccountLogin.Encode(str));
			CODebugBase<LogChannel>.Log(LogChannel.HTTP, "Paradox account successfully authenticated");
		}
		else
		{
			CODebugBase<LogChannel>.Log(LogChannel.HTTP, "Paradox account session has expired");
			UITextField uITextField = base.Find<UITextField>("PasswordField");
			uITextField.set_text(string.Empty);
			this.Show();
			uITextField.Focus();
			this.Show();
		}
	}

	public void OnCaptionClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_State == ParadoxAccountLogin.State.Normal)
		{
			this.Collapse();
		}
		else
		{
			this.Show();
		}
	}

	public void Show()
	{
		if (this.m_State != ParadoxAccountLogin.State.Normal)
		{
			if (base.get_component() != null)
			{
				base.get_component().Show();
			}
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				if (base.get_component() != null)
				{
					Vector3 relativePosition = base.get_component().get_relativePosition();
					relativePosition.y = val;
					base.get_component().set_relativePosition(relativePosition);
				}
			}, new AnimatedFloat((this.m_State != ParadoxAccountLogin.State.Collapsed) ? this.m_HiddenPosition.y : this.m_CollapsedPosition.y, this.m_NormalPosition.y, this.m_ShowHideTime, this.m_ShowEasingType));
			this.m_State = ParadoxAccountLogin.State.Normal;
		}
	}

	public void Hide()
	{
		base.CancelInvoke("Show");
		if (this.m_State != ParadoxAccountLogin.State.Hidden)
		{
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				if (base.get_component() != null)
				{
					Vector3 relativePosition = base.get_component().get_relativePosition();
					relativePosition.y = val;
					base.get_component().set_relativePosition(relativePosition);
				}
			}, new AnimatedFloat((this.m_State != ParadoxAccountLogin.State.Collapsed) ? this.m_NormalPosition.y : this.m_CollapsedPosition.y, this.m_HiddenPosition.y, this.m_ShowHideTime, this.m_ShowEasingType), delegate
			{
				if (base.get_component() != null)
				{
					base.get_component().Hide();
				}
			});
			this.m_State = ParadoxAccountLogin.State.Hidden;
		}
	}

	public void Collapse()
	{
		base.CancelInvoke("Show");
		if (base.get_component().get_isVisible())
		{
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				if (base.get_component() != null)
				{
					Vector3 relativePosition = base.get_component().get_relativePosition();
					relativePosition.y = val;
					base.get_component().set_relativePosition(relativePosition);
				}
			}, new AnimatedFloat(this.m_NormalPosition.y, this.m_CollapsedPosition.y, this.m_ShowHideTime, this.m_ShowEasingType));
			this.m_State = ParadoxAccountLogin.State.Collapsed;
		}
	}

	public void OnLogin()
	{
		this.LogMeIn(false);
	}

	public void LogMeIn(bool subscribeToNewsletter)
	{
		UICheckBox uICheckBox = base.Find<UICheckBox>("RememberMe");
		UIButton uIButton = base.Find<UIButton>("Login");
		UIButton uIButton2 = base.Find<UIButton>("Create");
		UITextField textName = base.Find<UITextField>("UsernameField");
		UITextField textPass = base.Find<UITextField>("PasswordField");
		string text = textName.get_text();
		string text2 = textPass.get_text();
		uICheckBox.set_isEnabled(false);
		uIButton.set_isEnabled(false);
		uIButton2.set_isEnabled(false);
		textName.set_isEnabled(false);
		textPass.set_isEnabled(false);
		ValueAnimator.Animate("PdxLogin", delegate(float val)
		{
			textName.set_opacity(val);
			textPass.set_opacity(val);
		}, new AnimatedFloat(1f, 0.4f, 0.3f));
		Singleton<PopsManager>.get_instance().Login(text, text2, subscribeToNewsletter, new Action<PopsManager.AccountActionResult>(this.OnLoginEnded));
	}

	private void OnLoginEnded(PopsManager.AccountActionResult result)
	{
		UICheckBox uICheckBox = base.Find<UICheckBox>("RememberMe");
		UIButton uIButton = base.Find<UIButton>("Login");
		UIButton uIButton2 = base.Find<UIButton>("Create");
		UITextField textName = base.Find<UITextField>("UsernameField");
		UITextField textPass = base.Find<UITextField>("PasswordField");
		uICheckBox.set_isEnabled(true);
		uIButton.set_isEnabled(true);
		uIButton2.set_isEnabled(true);
		textName.set_isEnabled(true);
		textPass.set_isEnabled(true);
		if (result.Success)
		{
			if (!this.m_LoginUsed.get_value())
			{
				this.m_LoginUsed.set_value(true);
				UIView.get_library().ShowModal<ParadoxUnlockPanel>("ParadoxUnlockPanel", true);
			}
			CODebugBase<LogChannel>.Log(LogChannel.HTTP, "Paradox account successfully authenticated");
			if (uICheckBox.get_isChecked())
			{
				this.m_RememberMe.set_value(true);
				this.m_SavedUsername.set_value(textName.get_text());
				this.m_SavedPassword.set_value(ParadoxAccountLogin.Encode(Singleton<PopsManager>.get_instance().AuthTokenRetrieve()));
			}
			this.Hide();
		}
		else
		{
			ValueAnimator.Animate("PdxLogin", delegate(float val)
			{
				textName.set_opacity(val);
				textPass.set_opacity(val);
			}, new AnimatedFloat(0.4f, 1f, 0.3f));
			string arg = result.StatusMessage;
			if (Locale.Exists("ERRORMESSAGE", result.StatusMessage))
			{
				arg = Locale.Get("ERRORMESSAGE", result.StatusMessage);
			}
			UIView.get_library().ShowModal<ExceptionPanel>("ExceptionPanel").SetMessage(Locale.Get("EXCEPTIONTITLE", "PdxAccountError"), StringUtils.SafeFormat(Locale.Get("PDXLOGIN_ERROR"), arg), true);
		}
	}

	public void OnCreate()
	{
		UIView.get_library().ShowModal("ParadoxAccountCreate", delegate(UIComponent c, int r)
		{
			if (r == 1)
			{
				if (Singleton<TelemetryManager>.get_exists())
				{
					Singleton<TelemetryManager>.get_instance().ParadoxAccountCreated();
				}
				UITextField uITextField = base.Find<UITextField>("UsernameField");
				UITextField uITextField2 = base.Find<UITextField>("PasswordField");
				ParadoxAccountCreate component = c.GetComponent<ParadoxAccountCreate>();
				uITextField.set_text(component.m_Email.get_text());
				uITextField2.set_text(component.m_Password.get_text());
				this.LogMeIn(component.m_SubscribeNewsletter);
			}
		});
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 13)
			{
				this.OnLogin();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	public UIComponent[] m_TabFocusList;

	public float m_TimeToAppear = 3f;

	public float m_ShowHideTime = 0.8f;

	public EasingType m_ShowEasingType = 13;

	private ParadoxAccountLogin.State m_State;

	private Vector2 m_HiddenPosition;

	private Vector2 m_CollapsedPosition;

	private Vector2 m_NormalPosition;

	private SavedString m_SavedUsername = new SavedString(Settings.pdx_acc_nm, Settings.gameSettingsFile, "email@email.com");

	private SavedString m_SavedPassword = new SavedString(Settings.pdx_acc_ps, Settings.gameSettingsFile, "password");

	private SavedBool m_RememberMe = new SavedBool(Settings.pdx_acc_rm, Settings.gameSettingsFile, true);

	private SavedBool m_LoginUsed = new SavedBool(Settings.pdxLoginUsed, Settings.userGameState, false);

	private enum State
	{
		Hidden,
		Collapsed,
		Normal
	}
}
