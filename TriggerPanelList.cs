﻿using System;
using ColossalFramework.UI;

public abstract class TriggerPanelList : UICustomControl
{
	public TriggerMilestone currentTrigger
	{
		get
		{
			return this.m_triggerMilestone;
		}
		set
		{
			this.m_triggerMilestone = value;
			this.OnTriggerChanged();
		}
	}

	protected abstract void OnTriggerChanged();

	protected TriggerMilestone m_triggerMilestone;
}
