﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class NetAI : PrefabAI
{
	public virtual Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		return this.m_info.m_color;
	}

	public virtual Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		return this.m_info.m_color;
	}

	public virtual bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return false;
	}

	public virtual void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public virtual string GetConstructionInfo(int productionRate)
	{
		return null;
	}

	public virtual void GetNodeState(ushort nodeID, ref NetNode nodeData, ushort segmentID, ref NetSegment segmentData, out NetNode.Flags flags, out Color color)
	{
		flags = nodeData.m_flags;
		color = Color.get_white();
	}

	public virtual void InitializePrefab()
	{
	}

	public virtual void DestroyPrefab()
	{
	}

	public virtual void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public virtual void RenderNode(ushort nodeID, ref NetNode nodeData, RenderManager.CameraInfo cameraInfo)
	{
	}

	public virtual void CreateSegment(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual void SegmentLoaded(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual void SimulationStep(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual void CreateNode(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void ReleaseNode(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
	}

	public virtual void ManualActivation(ushort segmentID, ref NetSegment data, NetInfo oldInfo)
	{
	}

	public virtual void ManualDeactivation(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual int RayCastNodeButton(ushort nodeID, ref NetNode data, Segment3 ray)
	{
		return 0;
	}

	public virtual void ClickNodeButton(ushort nodeID, ref NetNode data, int index)
	{
	}

	public virtual void GetNoiseAccumulation(out int noiseAccumulation, out float noiseRadius)
	{
		noiseAccumulation = 0;
		noiseRadius = 0f;
	}

	public virtual void GetTransportAccumulation(out int publicTransportAccumulation, out float publicTransportRadius)
	{
		publicTransportAccumulation = 0;
		publicTransportRadius = 0f;
	}

	public virtual void AfterSplitOrMove(ushort nodeID, ref NetNode data, ushort origNodeID1, ushort origNodeID2)
	{
	}

	public virtual void TrafficDirectionUpdated(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void AfterTerrainUpdate(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void AfterTerrainUpdate(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual void SimulationStep(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		building = null;
		heightOffset = 0f;
	}

	public virtual float GetNodeBuildingAngle(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		float[] angleBuffer = instance.m_angleBuffer;
		int num = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
				Vector3 vector;
				if (nodeID == startNode)
				{
					vector = instance.m_segments.m_buffer[(int)segment].m_startDirection;
				}
				else
				{
					vector = instance.m_segments.m_buffer[(int)segment].m_endDirection;
				}
				float num2 = Mathf.Atan2(vector.x, -vector.z) * 0.159154937f + 1.25f;
				angleBuffer[num++] = num2 - Mathf.Floor(num2);
				num2 += 0.5f;
				angleBuffer[num++] = num2 - Mathf.Floor(num2);
			}
		}
		float result = 0f;
		if (num != 0)
		{
			if (num == 2)
			{
				result = angleBuffer[0] + 0.75f;
			}
			else
			{
				bool flag;
				float num3;
				float num4;
				do
				{
					flag = true;
					for (int j = 1; j < num; j++)
					{
						num3 = angleBuffer[j - 1];
						num4 = angleBuffer[j];
						if (num3 > num4)
						{
							angleBuffer[j - 1] = num4;
							angleBuffer[j] = num3;
							flag = false;
						}
					}
				}
				while (!flag);
				num3 = angleBuffer[num - 1];
				num4 = angleBuffer[0];
				float num5 = num4 + 1f - num3;
				result = (num3 + num4) * 0.5f;
				for (int k = 1; k < num; k++)
				{
					num3 = angleBuffer[k - 1];
					num4 = angleBuffer[k];
					float num6 = num4 - num3;
					if (num6 > num5)
					{
						num5 = num6;
						result = (num3 + num4) * 0.5f;
					}
				}
			}
		}
		return result;
	}

	public virtual float GetLengthSnap()
	{
		return 8f;
	}

	public virtual void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 0;
	}

	public virtual NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		return this.m_info;
	}

	public virtual float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return 0f;
	}

	public virtual bool RequireDoubleSegments()
	{
		return false;
	}

	public virtual bool FlattenOnlyOneDoubleSegment()
	{
		return false;
	}

	public virtual bool CanModify()
	{
		return true;
	}

	public virtual bool WantTrafficLights()
	{
		return false;
	}

	public virtual void UpdateNode(ushort nodeID, ref NetNode data)
	{
	}

	public virtual int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		return 0;
	}

	public virtual int GetMaintenanceCost(Vector3 startPos, Vector3 endPos)
	{
		return 0;
	}

	public virtual bool IsUnderground()
	{
		return false;
	}

	public virtual bool IsOverground()
	{
		return false;
	}

	public virtual bool BuildUnderground()
	{
		return false;
	}

	public virtual bool UseMinHeight()
	{
		return false;
	}

	public virtual bool SupportUnderground()
	{
		return false;
	}

	public virtual bool BuildOnWater()
	{
		return false;
	}

	public virtual bool IgnoreWater()
	{
		return false;
	}

	public virtual bool DisplayTempSegment()
	{
		return true;
	}

	public virtual bool Snap(ushort segmentID, ref NetSegment data, NetInfo info, Vector3 refPos, float elevation, out float distance, out Vector3 snapPos, out Vector3 snapDir)
	{
		distance = 0f;
		snapPos = refPos;
		snapDir = Vector3.get_forward();
		return false;
	}

	public virtual float HeightSampleDistance()
	{
		return this.m_info.m_halfWidth;
	}

	public virtual bool LinearMiddleHeight()
	{
		return false;
	}

	public virtual bool SnapShoreLine()
	{
		return false;
	}

	public virtual bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float left, ref float right, ref float leftY, ref float rightY)
	{
		return index == 0;
	}

	public virtual bool SegmentModifyMask(ushort segmentID, ref NetSegment data, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float left, ref float right, ref float leftStartY, ref float rightStartY, ref float leftEndY, ref float rightEndY)
	{
		return index == 0;
	}

	public virtual void GetTerrainModifyRange(out float start, out float end)
	{
		start = 0f;
		end = 1f;
	}

	public virtual void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = this.m_info.m_maxHeight;
	}

	public virtual float GetCollisionHalfWidth()
	{
		return this.m_info.m_halfWidth;
	}

	public virtual ItemClass.CollisionType GetCollisionType()
	{
		if (this.m_info.m_flattenTerrain)
		{
			return ItemClass.CollisionType.Terrain;
		}
		if (this.IsUnderground())
		{
			return ItemClass.CollisionType.Underground;
		}
		return ItemClass.CollisionType.Elevated;
	}

	public virtual void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
	}

	public virtual bool IsCombatible(NetInfo with)
	{
		return this == with || (with.m_class.m_service == this.m_info.m_class.m_service && with.m_class.m_subService == this.m_info.m_class.m_subService && with.m_class.m_level == this.m_info.m_class.m_level && with.m_halfWidth == this.m_info.m_halfWidth && with.m_pavementWidth == this.m_info.m_pavementWidth && with.m_clipTerrain == this.m_info.m_clipTerrain && with.m_enableMiddleNodes == this.m_info.m_enableMiddleNodes);
	}

	public virtual void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True;
		uint num = 0u;
		uint num2 = data.m_lanes;
		Vector3 vector;
		Vector3 vector2;
		bool smoothStart;
		data.CalculateCorner(segmentID, true, true, true, out vector, out vector2, out smoothStart);
		Vector3 vector3;
		Vector3 vector4;
		bool smoothEnd;
		data.CalculateCorner(segmentID, true, false, true, out vector3, out vector4, out smoothEnd);
		Vector3 vector5;
		Vector3 vector6;
		data.CalculateCorner(segmentID, true, true, false, out vector5, out vector6, out smoothStart);
		Vector3 vector7;
		Vector3 vector8;
		data.CalculateCorner(segmentID, true, false, false, out vector7, out vector8, out smoothEnd);
		if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
		{
			data.m_cornerAngleStart = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector5.z - vector.z, vector5.x - vector.x) * 40.7436638f) & 255);
			data.m_cornerAngleEnd = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector3.z - vector7.z, vector3.x - vector7.x) * 40.7436638f) & 255);
		}
		else
		{
			data.m_cornerAngleStart = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector.z - vector5.z, vector.x - vector5.x) * 40.7436638f) & 255);
			data.m_cornerAngleEnd = (byte)(Mathf.RoundToInt(Mathf.Atan2(vector7.z - vector3.z, vector7.x - vector3.x) * 40.7436638f) & 255);
		}
		NetLane.Flags flags = NetLane.Flags.None;
		if ((data.m_flags & NetSegment.Flags.YieldStart) != NetSegment.Flags.None)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.YieldStart : NetLane.Flags.YieldEnd);
		}
		if ((data.m_flags & NetSegment.Flags.YieldEnd) != NetSegment.Flags.None)
		{
			flags |= (((data.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None) ? NetLane.Flags.YieldEnd : NetLane.Flags.YieldStart);
		}
		float num3 = 0f;
		float num4 = 0f;
		for (int i = 0; i < this.m_info.m_lanes.Length; i++)
		{
			if (num2 == 0u)
			{
				if (!Singleton<NetManager>.get_instance().CreateLanes(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, segmentID, 1))
				{
					break;
				}
				if (num != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane = num2;
				}
				else
				{
					data.m_lanes = num2;
				}
			}
			NetInfo.Lane lane = this.m_info.m_lanes[i];
			float num5 = lane.m_position / (this.m_info.m_halfWidth * 2f) + 0.5f;
			if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				num5 = 1f - num5;
			}
			Vector3 vector9 = vector + (vector5 - vector) * num5;
			Vector3 startDir = Vector3.Lerp(vector2, vector6, num5);
			Vector3 vector10 = vector7 + (vector3 - vector7) * num5;
			Vector3 endDir = Vector3.Lerp(vector8, vector4, num5);
			vector9.y += lane.m_verticalOffset;
			vector10.y += lane.m_verticalOffset;
			Vector3 vector11;
			Vector3 vector12;
			NetSegment.CalculateMiddlePoints(vector9, startDir, vector10, endDir, smoothStart, smoothEnd, out vector11, out vector12);
			NetLane.Flags flags2 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_flags;
			NetLane.Flags flags3 = flags;
			flags2 &= ~(NetLane.Flags.YieldStart | NetLane.Flags.YieldEnd);
			if ((byte)(lane.m_finalDirection & NetInfo.Direction.Both) == 2)
			{
				flags3 &= ~NetLane.Flags.YieldEnd;
			}
			if ((byte)(lane.m_finalDirection & NetInfo.Direction.Both) == 1)
			{
				flags3 &= ~NetLane.Flags.YieldStart;
			}
			flags2 |= flags3;
			if (flag)
			{
				flags2 |= NetLane.Flags.Inverted;
			}
			else
			{
				flags2 &= ~NetLane.Flags.Inverted;
			}
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_bezier = new Bezier3(vector9, vector11, vector12, vector10);
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_segment = segmentID;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_flags = (ushort)flags2;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_firstTarget = 0;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_lastTarget = 255;
			num3 += instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].UpdateLength();
			num4 += 1f;
			num = num2;
			num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
		}
		if (num4 != 0f)
		{
			data.m_averageLength = num3 / num4;
		}
		else
		{
			data.m_averageLength = 0f;
		}
	}

	public virtual void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
	}

	public virtual NetSegment.Flags UpdateSegmentFlags(ref NetNode startNode, ref NetNode endNode)
	{
		return NetSegment.Flags.Created;
	}

	public virtual void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
	}

	public virtual float MaxTransportWaitDistance()
	{
		return 0f;
	}

	public virtual ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ownerBuilding = null;
		ownerPosition = Vector3.get_zero();
		ownerDirection = Vector3.get_forward();
		productionRate = 0;
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if (test)
		{
			ushort num = middlePoint.m_segment;
			if (startPoint.m_segment == num || endPoint.m_segment == num)
			{
				num = 0;
			}
			if (num != 0 && Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info == this.m_info && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) == NetSegment.Flags.None)
			{
				toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
			}
		}
		if (autofix && this.m_info.m_enableBendingSegments)
		{
			Vector3 vector = middlePoint.m_direction;
			Vector3 vector2 = -endPoint.m_direction;
			Vector3 vector3 = middlePoint.m_position;
			float minNodeDistance = this.m_info.GetMinNodeDistance();
			for (int i = 0; i < 3; i++)
			{
				bool flag = false;
				bool flag2 = false;
				if (startPoint.m_node != 0)
				{
					if (NetAI.ForceValidDirection(this.m_info, ref vector, startPoint.m_node, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startPoint.m_node]))
					{
						flag = true;
					}
				}
				else if (startPoint.m_segment != 0 && NetAI.ForceValidDirection(this.m_info, startPoint.m_position, ref vector, startPoint.m_segment, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)startPoint.m_segment]))
				{
					flag = true;
				}
				if (flag)
				{
					Line2 line = Line2.XZ(startPoint.m_position, startPoint.m_position + vector);
					Line2 line2 = Line2.XZ(endPoint.m_position, endPoint.m_position + vector2);
					bool flag3 = true;
					float num2;
					float num3;
					if (line.Intersect(line2, ref num2, ref num3) && num2 >= minNodeDistance && num3 >= minNodeDistance)
					{
						vector3 = startPoint.m_position + vector * num2;
						flag3 = false;
					}
					if (flag3)
					{
						Vector3 vector4 = endPoint.m_position - startPoint.m_position;
						Vector3 vector5 = vector;
						vector4.y = 0f;
						vector5.y = 0f;
						float num4 = Vector3.SqrMagnitude(vector4);
						vector4 = Vector3.Normalize(vector4);
						float num5 = Mathf.Min(1.17809725f, Mathf.Acos(Vector3.Dot(vector4, vector5)));
						float num6 = Mathf.Sqrt(0.5f * num4 / Mathf.Max(0.001f, 1f - Mathf.Cos(3.14159274f - 2f * num5)));
						vector3 = startPoint.m_position + vector5 * num6;
						vector2 = vector3 - endPoint.m_position;
						vector2.y = 0f;
						vector2.Normalize();
					}
				}
				if (endPoint.m_node != 0)
				{
					if (NetAI.ForceValidDirection(this.m_info, ref vector2, endPoint.m_node, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endPoint.m_node]))
					{
						flag2 = true;
					}
				}
				else if (endPoint.m_segment != 0 && NetAI.ForceValidDirection(this.m_info, endPoint.m_position, ref vector2, endPoint.m_segment, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)endPoint.m_segment]))
				{
					flag2 = true;
				}
				if (flag2)
				{
					Line2 line3 = Line2.XZ(startPoint.m_position, startPoint.m_position + vector);
					Line2 line4 = Line2.XZ(endPoint.m_position, endPoint.m_position + vector2);
					bool flag4 = true;
					float num7;
					float num8;
					if (line3.Intersect(line4, ref num7, ref num8) && num7 >= minNodeDistance && num8 >= minNodeDistance)
					{
						vector3 = endPoint.m_position + vector2 * num8;
						flag4 = false;
					}
					if (flag4)
					{
						Vector3 vector6 = startPoint.m_position - endPoint.m_position;
						Vector3 vector7 = vector2;
						vector6.y = 0f;
						vector7.y = 0f;
						float num9 = Vector3.SqrMagnitude(vector6);
						vector6 = Vector3.Normalize(vector6);
						float num10 = Mathf.Min(1.17809725f, Mathf.Acos(Vector3.Dot(vector6, vector7)));
						float num11 = Mathf.Sqrt(0.5f * num9 / Mathf.Max(0.001f, 1f - Mathf.Cos(3.14159274f - 2f * num10)));
						vector3 = endPoint.m_position + vector7 * num11;
						vector = vector3 - startPoint.m_position;
						vector.y = 0f;
						vector.Normalize();
					}
				}
				if (!flag && !flag2)
				{
					middlePoint.m_direction = vector;
					endPoint.m_direction = -vector2;
					middlePoint.m_position = vector3;
					break;
				}
			}
		}
		return toolErrors;
	}

	public virtual ToolBase.ToolErrors CanConnectTo(ushort node, ushort segment, ulong[] segmentMask)
	{
		return ToolBase.ToolErrors.None;
	}

	private static bool ForceValidDirection(NetInfo info, ref Vector3 direction, ushort nodeID, ref NetNode node)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ItemClass connectionClass = info.GetConnectionClass();
		float num = 0.01f - Mathf.Cos(2.3561945f);
		float num2 = 2f;
		float num3 = -2f;
		Vector3 vector = direction;
		Vector3 vector2 = direction;
		float num4 = 0.01f - info.m_maxBuildAngleCos;
		float num5 = info.m_maxBuildAngle;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = node.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
				ItemClass connectionClass2 = info2.GetConnectionClass();
				Vector3 vector3;
				if (instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID)
				{
					vector3 = instance.m_segments.m_buffer[(int)segment].m_startDirection;
				}
				else
				{
					vector3 = instance.m_segments.m_buffer[(int)segment].m_endDirection;
				}
				float num6 = vector3.x * direction.x + vector3.z * direction.z;
				if (num6 >= num && num6 > num3 && num6 < 0.999f)
				{
					num3 = num6;
					vector2 = vector3;
				}
				if (connectionClass2.m_service == connectionClass.m_service)
				{
					if (num6 < num2)
					{
						num2 = num6;
						vector = vector3;
						num4 = 0.01f - Mathf.Min(info.m_maxBuildAngleCos, info2.m_maxBuildAngleCos);
						num5 = Mathf.Max(info.m_maxBuildAngle, info2.m_maxBuildAngle);
					}
				}
			}
		}
		if (num3 > -1.5f)
		{
			vector2.y = 0f;
			Vector3 vector4;
			vector4..ctor(direction.x, 0f, direction.z);
			vector4 = Vector3.RotateTowards(-vector2, vector4, 2.3561945f, 0f);
			direction.x = vector4.x;
			direction.z = vector4.z;
			return true;
		}
		if (num2 < 1.5f && num2 >= num4)
		{
			vector.y = 0f;
			Vector3 vector5;
			vector5..ctor(direction.x, 0f, direction.z);
			vector5 = Vector3.RotateTowards(-vector, vector5, num5 * 0.0174532924f, 0f);
			direction.x = vector5.x;
			direction.z = vector5.z;
			return true;
		}
		return false;
	}

	private static bool ForceValidDirection(NetInfo info, Vector3 position, ref Vector3 direction, ushort segmentID, ref NetSegment segment)
	{
		NetInfo info2 = segment.Info;
		float num = 0.01f - Mathf.Min(info.m_maxBuildAngleCos, info2.m_maxBuildAngleCos);
		float num2 = 0.01f - Mathf.Cos(2.3561945f);
		Vector3 vector;
		Vector3 vector2;
		segment.GetClosestPositionAndDirection(position, out vector, out vector2);
		float num3 = vector2.x * direction.x + vector2.z * direction.z;
		if ((num3 >= num2 && num3 < 0.999f) || (-num3 >= num2 && num3 > -0.999f))
		{
			if (num3 > 0f)
			{
				vector2 = -vector2;
			}
			vector2.y = 0f;
			Vector3 vector3;
			vector3..ctor(direction.x, 0f, direction.z);
			vector3 = Vector3.RotateTowards(vector2, vector3, 2.3561945f, 0f);
			direction.x = vector3.x;
			direction.z = vector3.z;
			return true;
		}
		if (info2.GetConnectionClass().m_service != info.GetConnectionClass().m_service)
		{
			return false;
		}
		if (num3 >= num && -num3 >= num)
		{
			if (num3 < 0f)
			{
				vector2 = -vector2;
			}
			vector2.y = 0f;
			Vector3 vector4;
			vector4..ctor(direction.x, 0f, direction.z);
			vector4 = Vector3.RotateTowards(vector2, vector4, Mathf.Max(info.m_maxBuildAngle, info2.m_maxBuildAngle) * 0.0174532924f, 0f);
			direction.x = vector4.x;
			direction.z = vector4.z;
			return true;
		}
		return false;
	}

	public virtual void ConnectionSucceeded(ushort nodeID, ref NetNode data)
	{
	}

	public virtual void UpgradeFailed()
	{
	}

	public virtual void UpgradeSucceeded()
	{
	}

	public virtual void ConstructSucceeded()
	{
	}

	public virtual void BulldozeSucceeded()
	{
	}

	public virtual bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		return false;
	}

	public virtual void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
	}

	public virtual void UpdateGuide(GuideController guideController)
	{
	}

	public virtual bool CanUpgradeTo(NetInfo info)
	{
		return info != this.m_info;
	}

	public virtual Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			return data.m_colors[vertexIndex];
		}
		return new Color32(255, 255, 255, 255);
	}

	public virtual Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			return data.m_colors[vertexIndex];
		}
		return new Color32(255, 255, 255, 255);
	}

	public virtual NetSegment.Flags GetBendFlags(ushort nodeID, ref NetNode nodeData)
	{
		return NetSegment.Flags.Bend;
	}

	public virtual int GetConstructionCost()
	{
		return 0;
	}

	public virtual int GetMaintenanceCost()
	{
		return 0;
	}

	public override string GetLocalizedTooltip()
	{
		int maintenanceCost = this.GetMaintenanceCost();
		return TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Cost,
			LocaleFormatter.FormatCost(this.GetConstructionCost(), true),
			LocaleFormatter.Upkeep,
			LocaleFormatter.FormatUpkeep(maintenanceCost, true),
			LocaleFormatter.IsUpkeepVisible,
			(maintenanceCost != 0).ToString()
		});
	}

	public override bool WorksAsNet()
	{
		return true;
	}

	public virtual float GetTerrainLowerOffset()
	{
		return -1.5f;
	}

	public virtual bool FlattenGroundNodes()
	{
		return false;
	}

	public virtual bool CanClipNodes()
	{
		return true;
	}

	public virtual bool CanIntersect(NetInfo with)
	{
		return true;
	}

	public virtual float GetVScale()
	{
		return 0.05f;
	}

	public virtual ItemClass.Layer GetCollisionLayers()
	{
		return this.m_info.m_class.m_layer;
	}

	public virtual bool ShowTerrainTopography()
	{
		return false;
	}

	public virtual bool CanConnect(NetInfo other)
	{
		return true;
	}

	public virtual bool RaiseTerrain()
	{
		return false;
	}

	public virtual float GetEndRadius()
	{
		return Mathf.Min(13f, this.m_info.m_halfWidth);
	}

	public virtual string GenerateName(ushort segmentID, ref NetSegment data)
	{
		return string.Empty;
	}

	public virtual float GetMinSegmentLength()
	{
		return (this.m_info.m_placementStyle != ItemClass.Placement.Manual) ? 3f : 7f;
	}

	public virtual float GetSnapElevation()
	{
		return 0f;
	}

	[NonSerialized]
	public NetInfo m_info;
}
