﻿using System;
using System.IO;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class RadioContentInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
	}

	public override void DestroyPrefab()
	{
		base.DestroyPrefab();
	}

	public override string GetLocalizedTitle()
	{
		if (!string.IsNullOrEmpty(this.m_displayName))
		{
			return this.m_displayName;
		}
		return base.get_gameObject().get_name();
	}

	public WWW ObtainClip()
	{
		string text = Path.Combine(DataLocation.get_gameContentPath(), "Radio");
		text = Path.Combine(text, this.m_contentType.ToString());
		text = Path.Combine(text, this.m_folderName);
		text = Path.Combine(text, this.m_fileName);
		return new WWW("file:///" + text);
	}

	public void CheckUnlocking()
	{
		MilestoneInfo unlockMilestone = this.m_UnlockMilestone;
		if (unlockMilestone != null)
		{
			Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
		}
	}

	public const int CONTENT_TYPE_COUNT = 5;

	public RadioContentInfo.ContentType m_contentType;

	public MilestoneInfo m_UnlockMilestone;

	public RadioChannelInfo[] m_radioChannels;

	public string m_folderName;

	public string m_fileName;

	public string m_displayName;

	public int m_lengthSeconds;

	public float m_volume = 1f;

	public int m_groupIndex;

	[NonSerialized]
	public int m_cooldown;

	public enum ContentType
	{
		Music,
		Blurb,
		Talk,
		Commercial,
		Broadcast
	}
}
