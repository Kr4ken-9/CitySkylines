﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class WorkshopAdPanel : UICustomControl
{
	private void Awake()
	{
		this.m_RefreshingSprite = base.Find<UITextureSprite>("LoadingWorkshopItems");
		this.m_RefreshingSprite.Show();
		this.m_DisabledLabel = base.Find<UILabel>("DisabledLabel");
		this.SetModsString();
		if (WorkshopAdPanel.dontInitialize)
		{
			this.m_RefreshingSprite.set_isVisible(false);
			this.m_DisabledLabel.set_isVisible(true);
			base.get_component().set_isEnabled(false);
			base.set_enabled(false);
			return;
		}
		this.m_DisabledLabel.set_isVisible(false);
		PlatformService.get_workshop().QueryItems();
		this.m_LastTime = Time.get_realtimeSinceStartup();
		this.m_ScrollContainer = base.Find<UIScrollablePanel>("Container");
	}

	private void SetModsString()
	{
		this.m_ModsButton.set_text(LocaleFormatter.FormatGeneric("MOD_ENABLED_STATUS", new object[]
		{
			Singleton<PluginManager>.get_instance().get_enabledModCount(),
			Singleton<PluginManager>.get_instance().get_modCount()
		}));
	}

	private void OnPluginsChanged()
	{
		this.SetModsString();
	}

	private void OnLocaleChanged()
	{
		this.SetModsString();
	}

	private void OnEnable()
	{
		PlatformService.get_workshop().add_eventUGCQueryCompleted(new Workshop.UGCDetailsHandler(this.OnQueryCompleted));
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.OnPluginsChanged));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.OnPluginsChanged));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		PlatformService.get_workshop().remove_eventUGCQueryCompleted(new Workshop.UGCDetailsHandler(this.OnQueryCompleted));
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.OnPluginsChanged));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.OnPluginsChanged));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	[DebuggerHidden]
	private static IEnumerator FetchPreviewFromURL(string url, UITextureSprite sprite, UITextureSprite loading)
	{
		WorkshopAdPanel.<FetchPreviewFromURL>c__Iterator0 <FetchPreviewFromURL>c__Iterator = new WorkshopAdPanel.<FetchPreviewFromURL>c__Iterator0();
		<FetchPreviewFromURL>c__Iterator.url = url;
		<FetchPreviewFromURL>c__Iterator.loading = loading;
		<FetchPreviewFromURL>c__Iterator.sprite = sprite;
		return <FetchPreviewFromURL>c__Iterator;
	}

	private void OnQueryCompleted(UGCDetails result, bool ioError)
	{
		this.m_RefreshingSprite.Hide();
		try
		{
			if (result.result == 1)
			{
				UIComponent uIComponent = this.m_ScrollContainer.AttachUIComponent(UITemplateManager.GetAsGameObject(WorkshopAdPanel.kWorkshopAdTemplate));
				string text = result.title;
				UILabel uILabel = uIComponent.Find<UILabel>("Title");
				using (UIFontRenderer uIFontRenderer = uILabel.ObtainRenderer())
				{
					float num = uILabel.GetUIView().PixelsToUnits();
					float[] characterWidths = uIFontRenderer.GetCharacterWidths(text);
					float num2 = 0f;
					for (int i = 0; i < characterWidths.Length; i++)
					{
						num2 += characterWidths[i] / num;
						if (num2 > uILabel.get_width())
						{
							text = text.Substring(0, i - 3) + "...";
							break;
						}
					}
				}
				uILabel.set_text(string.Concat(new string[]
				{
					text,
					Environment.NewLine,
					"<color #50869a>",
					result.tags,
					"</color>"
				}));
				UITextureSprite uITextureSprite = uIComponent.Find<UITextureSprite>("Image");
				UITextureSprite uITextureSprite2 = uIComponent.Find<UITextureSprite>("LoadingImage");
				if (result.image != null)
				{
					uITextureSprite2.Hide();
					result.image.set_wrapMode(1);
					uITextureSprite.set_texture(result.image);
				}
				else
				{
					uITextureSprite2.Show();
					base.StartCoroutine(WorkshopAdPanel.FetchPreviewFromURL(result.imageURL, uITextureSprite, uITextureSprite2));
				}
				UIProgressBar uIProgressBar = uIComponent.Find<UIProgressBar>("Rating");
				uIProgressBar.set_isVisible(result.score >= 0f);
				uIProgressBar.set_value(result.score);
				uIComponent.Find<UIButton>("ClickableArea").add_eventClick(delegate(UIComponent c, UIMouseEventParameter p)
				{
					if (PlatformService.IsOverlayEnabled())
					{
						PlatformService.ActivateGameOverlayToWorkshopItem(result.publishedFileId);
					}
				});
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, string.Concat(new object[]
				{
					"Workshop item: ",
					result.publishedFileId,
					" error: ",
					result.result
				}));
			}
		}
		catch (Exception ex)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, ex.ToString());
		}
	}

	public void OpenWorkshopInSteam()
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlay(6);
		}
	}

	private void Update()
	{
		if (this.m_RefreshingSprite.get_isVisible())
		{
			this.m_RefreshingSprite.get_transform().Rotate(Vector3.get_back(), 245f * Time.get_deltaTime());
		}
		if (this.m_AutoScroll && Time.get_realtimeSinceStartup() - this.m_LastTime > (float)this.m_AutoScrollInterval)
		{
			UIComponent uIComponent = UITemplateManager.Peek(WorkshopAdPanel.kWorkshopAdTemplate);
			if (!ValueAnimator.IsAnimating("WorkshopScroll"))
			{
				ValueAnimator.Animate("WorkshopScroll", delegate(float val)
				{
					if (this.m_ScrollContainer != null)
					{
						this.m_ScrollContainer.set_scrollPosition(new Vector2(this.m_ScrollContainer.get_scrollPosition().x, val));
					}
				}, new AnimatedFloat(this.m_ScrollContainer.get_scrollPosition().y, this.m_ScrollContainer.get_scrollPosition().y + uIComponent.get_height(), 2f, this.m_EasingType));
				this.m_LastTime = Time.get_realtimeSinceStartup();
			}
		}
	}

	private static readonly string kWorkshopAdTemplate = "WorkshopAdTemplate";

	internal static bool dontInitialize;

	public bool m_AutoScroll = true;

	public int m_AutoScrollInterval = 20;

	public EasingType m_EasingType = 1;

	private float m_LastTime;

	private UIScrollablePanel m_ScrollContainer;

	public UIButton m_ModsButton;

	private UILabel m_DisabledLabel;

	private UITextureSprite m_RefreshingSprite;
}
