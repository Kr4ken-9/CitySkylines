﻿using System;
using ColossalFramework;

public class MonumentsGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Monument;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Building;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		switch (name)
		{
		case "MonumentLandmarks":
			return 0;
		case "MonumentExpansion1":
			return 1;
		case "MonumentExpansion2":
			return 2;
		case "MonumentFootball":
			return 50;
		case "MonumentConcerts":
			return 80;
		case "MonumentCategory1":
			return 100;
		case "MonumentCategory2":
			return 101;
		case "MonumentCategory3":
			return 102;
		case "MonumentCategory4":
			return 103;
		case "MonumentCategory5":
			return 104;
		case "MonumentCategory6":
			return 105;
		case "MonumentModderPack":
			return 200;
		}
		return -1;
	}

	private UnlockManager.Feature GetCategoryFeature(string name)
	{
		switch (name)
		{
		case "MonumentExpansion1":
			return UnlockManager.Feature.CommercialSpecialization;
		case "MonumentFootball":
			return UnlockManager.Feature.Football;
		case "MonumentConcerts":
			return UnlockManager.Feature.Concerts;
		case "MonumentCategory2":
			return UnlockManager.Feature.MonumentLevel2;
		case "MonumentCategory3":
			return UnlockManager.Feature.MonumentLevel3;
		case "MonumentCategory4":
			return UnlockManager.Feature.MonumentLevel4;
		case "MonumentCategory5":
			return UnlockManager.Feature.MonumentLevel5;
		case "MonumentCategory6":
			return UnlockManager.Feature.MonumentLevel6;
		}
		return UnlockManager.Feature.None;
	}

	protected override GeneratedGroupPanel.GroupInfo CreateGroupInfo(string name, PrefabInfo info)
	{
		return new MonumentsGroupPanel.PTGroupInfo(name, this.GetCategoryOrder(name), info.GetService(), this.GetCategoryFeature(name));
	}

	public class PTGroupInfo : GeneratedGroupPanel.GroupInfo
	{
		public PTGroupInfo(string name, int order, ItemClass.Service service, UnlockManager.Feature feature) : base(name, order)
		{
			this.m_Service = service;
			this.m_Feature = feature;
		}

		public override string serviceName
		{
			get
			{
				return EnumExtensions.Name<ItemClass.Service>(this.m_Service);
			}
		}

		public override string unlockText
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return base.unlockText;
				}
				return GeneratedGroupPanel.GetUnlockText(this.m_Feature);
			}
		}

		public override bool isUnlocked
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return base.isUnlocked;
				}
				return ToolsModifierControl.IsUnlocked(this.m_Feature);
			}
		}

		private ItemClass.Service m_Service;

		private UnlockManager.Feature m_Feature;
	}
}
