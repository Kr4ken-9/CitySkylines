﻿using System;
using ColossalFramework.Packaging;

[Serializable]
public class DistrictStyleMetaData : MetaData
{
	public override bool getBuiltin
	{
		get
		{
			return this.builtin;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public override string[] getTags
	{
		get
		{
			return this.steamTags;
		}
	}

	public bool builtin;

	public string name;

	public string[] steamTags;

	public string[] assets;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;
}
