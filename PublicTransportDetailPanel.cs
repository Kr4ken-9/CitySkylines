﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class PublicTransportDetailPanel : UICustomControl
{
	public void SetPosition(Vector3 position)
	{
		base.get_component().set_relativePosition(position);
	}

	private void Awake()
	{
		this.m_LineCount = base.Find<UILabel>("LabelLineCount");
		this.m_BusLinesContainer = base.Find("BusDetail").Find("Container");
		this.m_TramLinesContainer = base.Find("TramDetail").Find("Container");
		this.m_MetroLinesContainer = base.Find("MetroDetail").Find("Container");
		this.m_TrainLinesContainer = base.Find("TrainDetail").Find("Container");
		this.m_FerryLinesContainer = base.Find("FerryDetail").Find("Container");
		this.m_BlimpLinesContainer = base.Find("BlimpDetail").Find("Container");
		this.m_MonorailLinesContainer = base.Find("MonorailDetail").Find("Container");
		this.m_Strip = base.Find<UITabstrip>("Tabstrip");
		this.m_ToggleAllState = new bool[this.m_Strip.get_tabCount()];
		this.m_Strip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTabChanged));
		this.m_ToggleAll = base.Find<UICheckBox>("ToggleAll");
		this.m_ToggleAll.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.CheckChangedFunction));
		for (int i = 0; i < this.m_ToggleAllState.Length; i++)
		{
			this.m_ToggleAllState[i] = this.m_ToggleAll.get_isChecked();
		}
		base.Find<UIButton>("ColorTitle").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnColorSort();
		});
		base.Find<UIButton>("NameTitle").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnNameSort();
		});
		base.Find<UIButton>("StopsTitle").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnStopSort();
		});
		base.Find<UIButton>("VehiclesTitle").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnVehicleSort();
		});
		base.Find<UIButton>("PassengersTitle").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnPassengerSort();
		});
		base.Find<UIButton>("DayButton").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnDaySort();
		});
		base.Find<UIButton>("NightButton").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnNightSort();
		});
		base.Find<UIButton>("DayNightButton").add_eventClick(delegate(UIComponent c, UIMouseEventParameter r)
		{
			this.OnDayAndNightSort();
		});
		this.SetActiveTab(0);
		this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.DEFAULT;
	}

	private void CheckChangedFunction(UIComponent c, bool r)
	{
		this.OnChangeVisibleAll(r);
	}

	private void OnColorSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.COLOR)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByColor();
	}

	private void SortByColor()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cache0 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache0 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareColor);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cache0;
			if (PublicTransportDetailPanel.<>f__mg$cache1 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache1 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareColorInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cache1);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.COLOR;
			uIComponent.Invalidate();
		}
	}

	private void OnDaySort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.DAY)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByDay();
	}

	private void SortByDay()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cache2 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache2 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareDay);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cache2;
			if (PublicTransportDetailPanel.<>f__mg$cache3 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache3 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareDayInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cache3);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.DAY;
			uIComponent.Invalidate();
		}
	}

	private void OnNightSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.NIGHT)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByNight();
	}

	private void SortByNight()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cache4 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache4 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareNight);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cache4;
			if (PublicTransportDetailPanel.<>f__mg$cache5 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache5 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareNightInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cache5);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.NIGHT;
			uIComponent.Invalidate();
		}
	}

	private void OnDayAndNightSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.DAYANDNIGHT)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByDayAndNight();
	}

	private void SortByDayAndNight()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cache6 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache6 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareDayAndNight);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cache6;
			if (PublicTransportDetailPanel.<>f__mg$cache7 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache7 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareDayAndNightInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cache7);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.DAYANDNIGHT;
			uIComponent.Invalidate();
		}
	}

	private void OnNameSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.NAME)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByNames();
	}

	private void SortByNames()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cache8 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache8 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareNames);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cache8;
			if (PublicTransportDetailPanel.<>f__mg$cache9 == null)
			{
				PublicTransportDetailPanel.<>f__mg$cache9 = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareNamesInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cache9);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.NAME;
			uIComponent.Invalidate();
		}
	}

	private void OnStopSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.STOP)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByStops();
	}

	private void SortByStops()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cacheA == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheA = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareStops);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cacheA;
			if (PublicTransportDetailPanel.<>f__mg$cacheB == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheB = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareStopsInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cacheB);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.STOP;
			uIComponent.Invalidate();
		}
	}

	private void OnVehicleSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.VEHICLE)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByVehicles();
	}

	private void SortByVehicles()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cacheC == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheC = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareVehicles);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cacheC;
			if (PublicTransportDetailPanel.<>f__mg$cacheD == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheD = new Comparison<UIComponent>(PublicTransportDetailPanel.CompareVehiclesInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cacheD);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.VEHICLE;
			uIComponent.Invalidate();
		}
	}

	private void OnPassengerSort()
	{
		if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.PASSENGER)
		{
			this.m_InvertList = !this.m_InvertList;
		}
		this.SortByPassengers();
	}

	private void SortByPassengers()
	{
		UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
		if (uIComponent.get_components().Count > 0)
		{
			IList<UIComponent> arg_7D_1 = uIComponent.get_components();
			if (PublicTransportDetailPanel.<>f__mg$cacheE == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheE = new Comparison<UIComponent>(PublicTransportDetailPanel.ComparePassengers);
			}
			Comparison<UIComponent> arg_7D_2 = PublicTransportDetailPanel.<>f__mg$cacheE;
			if (PublicTransportDetailPanel.<>f__mg$cacheF == null)
			{
				PublicTransportDetailPanel.<>f__mg$cacheF = new Comparison<UIComponent>(PublicTransportDetailPanel.ComparePassengersInverse);
			}
			this.QuicksortOrInverseQuicksort(arg_7D_1, arg_7D_2, PublicTransportDetailPanel.<>f__mg$cacheF);
			this.m_LastSortCriterion = PublicTransportDetailPanel.LineSortCriterion.PASSENGER;
			uIComponent.Invalidate();
		}
	}

	private void OnChangeVisibleAll(bool visible)
	{
		if (this.m_Strip.get_selectedIndex() > -1 && this.m_Strip.get_selectedIndex() < this.m_Strip.get_tabContainer().get_components().Count)
		{
			this.m_ToggleAllState[this.m_Strip.get_selectedIndex()] = visible;
			UIComponent uIComponent = this.m_Strip.get_tabContainer().get_components()[this.m_Strip.get_selectedIndex()].Find("Container");
			if (uIComponent != null)
			{
				for (int i = 0; i < uIComponent.get_components().Count; i++)
				{
					UIComponent uIComponent2 = uIComponent.get_components()[i];
					if (uIComponent2 != null)
					{
						UICheckBox uICheckBox = uIComponent2.Find<UICheckBox>("LineVisible");
						uICheckBox.set_isChecked(visible);
					}
				}
			}
			this.RefreshLines();
		}
	}

	private void QuicksortOrInverseQuicksort(IList<UIComponent> elements, Comparison<UIComponent> comp, Comparison<UIComponent> compInverse)
	{
		if (this.m_InvertList)
		{
			PublicTransportDetailPanel.Quicksort(elements, compInverse);
		}
		else
		{
			PublicTransportDetailPanel.Quicksort(elements, comp);
		}
	}

	public static void Quicksort(IList<UIComponent> elements, Comparison<UIComponent> comp)
	{
		PublicTransportDetailPanel.Quicksort(elements, 0, elements.Count - 1, comp);
	}

	private void OnEnable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.m_Ready = true;
		this.m_Strip.get_tabs().get_Item(1).set_isVisible(Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Tram));
		bool isVisible = SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC);
		this.m_Strip.get_tabs().get_Item(4).set_isVisible(isVisible);
		this.m_Strip.get_tabs().get_Item(5).set_isVisible(isVisible);
		this.m_Strip.get_tabs().get_Item(6).set_isVisible(isVisible);
		this.RefreshLines();
		this.SortByNames();
	}

	private void Update()
	{
		if (Singleton<TransportManager>.get_exists() && this.m_Ready && this.m_LastLineCount != Singleton<TransportManager>.get_instance().m_lineCount)
		{
			this.RefreshLines();
			this.m_LastLineCount = Singleton<TransportManager>.get_instance().m_lineCount;
		}
		if (this.m_LinesUpdated)
		{
			this.m_LinesUpdated = false;
			if (this.m_LastSortCriterion != PublicTransportDetailPanel.LineSortCriterion.DEFAULT)
			{
				if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.NAME)
				{
					this.SortByNames();
				}
				else if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.PASSENGER)
				{
					this.SortByPassengers();
				}
				else if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.STOP)
				{
					this.SortByStops();
				}
				else if (this.m_LastSortCriterion == PublicTransportDetailPanel.LineSortCriterion.VEHICLE)
				{
					this.SortByVehicles();
				}
			}
		}
	}

	private static int NaturalCompare(string left, string right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		int length = left.Length;
		int length2 = right.Length;
		int num = 0;
		int num2 = 0;
		while (num < length && num2 < length2)
		{
			char c = left[num];
			char c2 = right[num2];
			char[] array = new char[length];
			int num3 = 0;
			char[] array2 = new char[length2];
			int num4 = 0;
			do
			{
				array[num3++] = c;
				num++;
				if (num >= length)
				{
					break;
				}
				c = left[num];
			}
			while (char.IsDigit(c) == char.IsDigit(array[0]));
			do
			{
				IL_8C:
				array2[num4++] = c2;
				num2++;
				if (num2 >= length2)
				{
					break;
				}
				c2 = right[num2];
			}
			while (char.IsDigit(c2) == char.IsDigit(array2[0]));
			IL_CB:
			string text = new string(array);
			string text2 = new string(array2);
			int num6;
			if (char.IsDigit(array[0]) && char.IsDigit(array2[0]))
			{
				int num5 = int.Parse(text);
				int value = int.Parse(text2);
				num6 = num5.CompareTo(value);
			}
			else
			{
				num6 = text.CompareTo(text2);
			}
			if (num6 != 0)
			{
				return num6;
			}
			continue;
			goto IL_CB;
			goto IL_8C;
		}
		return length - length2;
	}

	private static int CompareNames(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		return PublicTransportDetailPanel.NaturalCompare(component.lineName, component2.lineName);
	}

	private static int CompareNamesInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareNames(right, left);
	}

	private static int CompareStops(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		return PublicTransportDetailPanel.NaturalCompare(component.stopCounts, component2.stopCounts);
	}

	private static int CompareStopsInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareStops(right, left);
	}

	private static int CompareVehicles(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		return PublicTransportDetailPanel.NaturalCompare(component.vehicleCounts, component2.vehicleCounts);
	}

	private static int CompareVehiclesInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareVehicles(right, left);
	}

	private static int ComparePassengers(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		return component.passengerCountsInt.CompareTo(component2.passengerCountsInt);
	}

	private static int ComparePassengersInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.ComparePassengers(right, left);
	}

	private static int CompareDay(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		bool isDay = component.isDay;
		bool isDay2 = component2.isDay;
		if (isDay && !isDay2)
		{
			return 1;
		}
		if (!isDay && isDay2)
		{
			return -1;
		}
		return PublicTransportDetailPanel.ComparePassengers(left, right);
	}

	private static int CompareDayInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareDay(right, left);
	}

	private static int CompareNight(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		bool isNight = component.isNight;
		bool isNight2 = component2.isNight;
		if (isNight && !isNight2)
		{
			return 1;
		}
		if (!isNight && isNight2)
		{
			return -1;
		}
		return PublicTransportDetailPanel.ComparePassengers(left, right);
	}

	private static int CompareNightInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareNight(right, left);
	}

	private static int CompareDayAndNight(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		bool isDayAndNight = component.isDayAndNight;
		bool isDayAndNight2 = component2.isDayAndNight;
		if (isDayAndNight && !isDayAndNight2)
		{
			return 1;
		}
		if (!isDayAndNight && isDayAndNight2)
		{
			return -1;
		}
		return PublicTransportDetailPanel.ComparePassengers(left, right);
	}

	private static int CompareDayAndNightInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareDayAndNight(right, left);
	}

	private static int CompareColor(UIComponent left, UIComponent right)
	{
		if (left == null || right == null)
		{
			return 0;
		}
		PublicTransportLineInfo component = left.GetComponent<PublicTransportLineInfo>();
		PublicTransportLineInfo component2 = right.GetComponent<PublicTransportLineInfo>();
		Color lineColor = Singleton<TransportManager>.get_instance().GetLineColor(component.lineID);
		Color lineColor2 = Singleton<TransportManager>.get_instance().GetLineColor(component2.lineID);
		if (lineColor.r > lineColor2.r)
		{
			return 1;
		}
		if (lineColor.r < lineColor2.r)
		{
			return -1;
		}
		if (lineColor.g > lineColor2.g)
		{
			return 1;
		}
		if (lineColor.g < lineColor2.g)
		{
			return -1;
		}
		if (lineColor.b > lineColor2.b)
		{
			return 1;
		}
		if (lineColor.b < lineColor2.b)
		{
			return -1;
		}
		return PublicTransportDetailPanel.ComparePassengers(left, right);
	}

	private static int CompareColorInverse(UIComponent left, UIComponent right)
	{
		return PublicTransportDetailPanel.CompareColor(right, left);
	}

	public static void Quicksort(IList<UIComponent> elements, int left, int right, Comparison<UIComponent> comp)
	{
		int i = left;
		int num = right;
		UIComponent y = elements[(left + right) / 2];
		while (i <= num)
		{
			while (comp(elements[i], y) < 0)
			{
				i++;
			}
			while (comp(elements[num], y) > 0)
			{
				num--;
			}
			if (i <= num)
			{
				UIComponent value = elements[i];
				elements[i] = elements[num];
				elements[i].set_forceZOrder(i);
				elements[num] = value;
				elements[num].set_forceZOrder(num);
				i++;
				num--;
			}
		}
		if (left < num)
		{
			PublicTransportDetailPanel.Quicksort(elements, left, num, comp);
		}
		if (i < right)
		{
			PublicTransportDetailPanel.Quicksort(elements, i, right, comp);
		}
	}

	public void RefreshLines()
	{
		if (Singleton<TransportManager>.get_exists())
		{
			this.m_BusCount = 0;
			this.m_TramCount = 0;
			this.m_MetroCount = 0;
			this.m_TrainCount = 0;
			this.m_FerryCount = 0;
			this.m_BlimpCount = 0;
			this.m_MonorailCount = 0;
			for (ushort num = 1; num < 256; num += 1)
			{
				if ((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num].m_flags & (TransportLine.Flags.Created | TransportLine.Flags.Temporary)) == TransportLine.Flags.Created)
				{
					TransportInfo info = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)num].Info;
					if (info.m_transportType == TransportInfo.TransportType.Bus)
					{
						PublicTransportLineInfo publicTransportLineInfo;
						if (this.m_BusCount >= this.m_BusLinesContainer.get_components().Count)
						{
							publicTransportLineInfo = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_BusLinesContainer.AttachUIComponent(publicTransportLineInfo.get_gameObject());
						}
						else
						{
							publicTransportLineInfo = this.m_BusLinesContainer.get_components()[this.m_BusCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo.lineID = num;
						publicTransportLineInfo.RefreshData(true, false);
						this.m_BusCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Tram)
					{
						PublicTransportLineInfo publicTransportLineInfo2;
						if (this.m_TramCount >= this.m_TramLinesContainer.get_components().Count)
						{
							publicTransportLineInfo2 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_TramLinesContainer.AttachUIComponent(publicTransportLineInfo2.get_gameObject());
						}
						else
						{
							publicTransportLineInfo2 = this.m_TramLinesContainer.get_components()[this.m_TramCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo2.lineID = num;
						publicTransportLineInfo2.RefreshData(true, false);
						this.m_TramCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Metro)
					{
						PublicTransportLineInfo publicTransportLineInfo3;
						if (this.m_MetroCount >= this.m_MetroLinesContainer.get_components().Count)
						{
							publicTransportLineInfo3 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_MetroLinesContainer.AttachUIComponent(publicTransportLineInfo3.get_gameObject());
						}
						else
						{
							publicTransportLineInfo3 = this.m_MetroLinesContainer.get_components()[this.m_MetroCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo3.lineID = num;
						publicTransportLineInfo3.RefreshData(true, false);
						this.m_MetroCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Train)
					{
						PublicTransportLineInfo publicTransportLineInfo4;
						if (this.m_TrainCount >= this.m_TrainLinesContainer.get_components().Count)
						{
							publicTransportLineInfo4 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_TrainLinesContainer.AttachUIComponent(publicTransportLineInfo4.get_gameObject());
						}
						else
						{
							publicTransportLineInfo4 = this.m_TrainLinesContainer.get_components()[this.m_TrainCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo4.lineID = num;
						publicTransportLineInfo4.RefreshData(true, false);
						this.m_TrainCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Ship)
					{
						PublicTransportLineInfo publicTransportLineInfo5;
						if (this.m_FerryCount >= this.m_FerryLinesContainer.get_components().Count)
						{
							publicTransportLineInfo5 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_FerryLinesContainer.AttachUIComponent(publicTransportLineInfo5.get_gameObject());
						}
						else
						{
							publicTransportLineInfo5 = this.m_FerryLinesContainer.get_components()[this.m_FerryCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo5.lineID = num;
						publicTransportLineInfo5.RefreshData(true, false);
						this.m_FerryCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Airplane)
					{
						PublicTransportLineInfo publicTransportLineInfo6;
						if (this.m_BlimpCount >= this.m_BlimpLinesContainer.get_components().Count)
						{
							publicTransportLineInfo6 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_BlimpLinesContainer.AttachUIComponent(publicTransportLineInfo6.get_gameObject());
						}
						else
						{
							publicTransportLineInfo6 = this.m_BlimpLinesContainer.get_components()[this.m_BlimpCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo6.lineID = num;
						publicTransportLineInfo6.RefreshData(true, false);
						this.m_BlimpCount++;
					}
					else if (info.m_transportType == TransportInfo.TransportType.Monorail)
					{
						PublicTransportLineInfo publicTransportLineInfo7;
						if (this.m_MonorailCount >= this.m_MonorailLinesContainer.get_components().Count)
						{
							publicTransportLineInfo7 = UITemplateManager.Get<PublicTransportLineInfo>(PublicTransportDetailPanel.kLineTemplate);
							this.m_MonorailLinesContainer.AttachUIComponent(publicTransportLineInfo7.get_gameObject());
						}
						else
						{
							publicTransportLineInfo7 = this.m_MonorailLinesContainer.get_components()[this.m_MonorailCount].GetComponent<PublicTransportLineInfo>();
						}
						publicTransportLineInfo7.lineID = num;
						publicTransportLineInfo7.RefreshData(true, false);
						this.m_MonorailCount++;
					}
				}
			}
			this.RefreshLineCount(this.m_Strip.get_selectedIndex(), new int[]
			{
				this.m_BusCount,
				this.m_TramCount,
				this.m_MetroCount,
				this.m_TrainCount,
				this.m_FerryCount,
				this.m_BlimpCount,
				this.m_MonorailCount
			});
			while (this.m_BusLinesContainer.get_components().Count > this.m_BusCount)
			{
				UIComponent uIComponent = this.m_BusLinesContainer.get_components()[this.m_BusCount];
				this.m_BusLinesContainer.RemoveUIComponent(uIComponent);
				Object.Destroy(uIComponent.get_gameObject());
			}
			while (this.m_TramLinesContainer.get_components().Count > this.m_TramCount)
			{
				UIComponent uIComponent2 = this.m_TramLinesContainer.get_components()[this.m_TramCount];
				this.m_TramLinesContainer.RemoveUIComponent(uIComponent2);
				Object.Destroy(uIComponent2.get_gameObject());
			}
			while (this.m_MetroLinesContainer.get_components().Count > this.m_MetroCount)
			{
				UIComponent uIComponent3 = this.m_MetroLinesContainer.get_components()[this.m_MetroCount];
				this.m_MetroLinesContainer.RemoveUIComponent(uIComponent3);
				Object.Destroy(uIComponent3.get_gameObject());
			}
			while (this.m_TrainLinesContainer.get_components().Count > this.m_TrainCount)
			{
				UIComponent uIComponent4 = this.m_TrainLinesContainer.get_components()[this.m_TrainCount];
				this.m_TrainLinesContainer.RemoveUIComponent(uIComponent4);
				Object.Destroy(uIComponent4.get_gameObject());
			}
			while (this.m_FerryLinesContainer.get_components().Count > this.m_FerryCount)
			{
				UIComponent uIComponent5 = this.m_FerryLinesContainer.get_components()[this.m_FerryCount];
				this.m_FerryLinesContainer.RemoveUIComponent(uIComponent5);
				Object.Destroy(uIComponent5.get_gameObject());
			}
			while (this.m_BlimpLinesContainer.get_components().Count > this.m_BlimpCount)
			{
				UIComponent uIComponent6 = this.m_BlimpLinesContainer.get_components()[this.m_BlimpCount];
				this.m_BlimpLinesContainer.RemoveUIComponent(uIComponent6);
				Object.Destroy(uIComponent6.get_gameObject());
			}
			while (this.m_MonorailLinesContainer.get_components().Count > this.m_MonorailCount)
			{
				UIComponent uIComponent7 = this.m_MonorailLinesContainer.get_components()[this.m_MonorailCount];
				this.m_MonorailLinesContainer.RemoveUIComponent(uIComponent7);
				Object.Destroy(uIComponent7.get_gameObject());
			}
			this.m_LinesUpdated = true;
		}
	}

	private void RefreshLineCount(int tabIndex, params int[] lineCounts)
	{
		string arg = Locale.Get("PUBLICTRANSPORT_LINECOUNT", tabIndex);
		this.m_LineCount.set_text(arg + ": " + lineCounts[tabIndex]);
	}

	private void OnLocaleChanged()
	{
		this.RefreshLineCount(this.m_Strip.get_selectedIndex(), new int[]
		{
			this.m_BusCount,
			this.m_TramCount,
			this.m_MetroCount,
			this.m_TrainCount,
			this.m_FerryCount,
			this.m_BlimpCount,
			this.m_MonorailCount
		});
	}

	private void OnTabChanged(UIComponent c, int idx)
	{
		this.m_ToggleAll.remove_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.CheckChangedFunction));
		this.m_ToggleAll.set_isChecked(this.m_ToggleAllState[idx]);
		this.m_ToggleAll.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.CheckChangedFunction));
		this.RefreshLineCount(idx, new int[]
		{
			this.m_BusCount,
			this.m_TramCount,
			this.m_MetroCount,
			this.m_TrainCount,
			this.m_FerryCount,
			this.m_BlimpCount,
			this.m_MonorailCount
		});
	}

	public void SetActiveTab(int idx)
	{
		this.m_Strip.set_selectedIndex(idx);
	}

	public void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	private static readonly string kLineTemplate = "LineTemplate";

	private UITabstrip m_Strip;

	private UIComponent m_BusLinesContainer;

	private UIComponent m_TramLinesContainer;

	private UIComponent m_MetroLinesContainer;

	private UIComponent m_TrainLinesContainer;

	private UIComponent m_FerryLinesContainer;

	private UIComponent m_BlimpLinesContainer;

	private UIComponent m_MonorailLinesContainer;

	private UICheckBox m_ToggleAll;

	private UILabel m_LineCount;

	private int m_LastLineCount;

	private bool m_Ready;

	private bool m_LinesUpdated;

	private bool[] m_ToggleAllState;

	private bool m_InvertList;

	private int m_BusCount;

	private int m_TramCount;

	private int m_MetroCount;

	private int m_TrainCount;

	private int m_FerryCount;

	private int m_BlimpCount;

	private int m_MonorailCount;

	private PublicTransportDetailPanel.LineSortCriterion m_LastSortCriterion;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache0;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache1;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache2;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache3;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache4;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache5;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache6;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache7;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache8;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cache9;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheA;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheB;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheC;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheD;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheE;

	[CompilerGenerated]
	private static Comparison<UIComponent> <>f__mg$cacheF;

	private enum LineSortCriterion
	{
		DEFAULT,
		NAME,
		STOP,
		VEHICLE,
		PASSENGER,
		DAY,
		NIGHT,
		DAYANDNIGHT,
		COLOR
	}
}
