﻿using System;
using ColossalFramework.Globalization;

public class NetTypeGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, NetInfo netInfo)
	{
		if (base.CanActivate(guideInfo) && netInfo.m_prefabDataIndex != -1)
		{
			GuideTriggerBase.ActivationInfo activationInfo = new NetTypeGuide.NetTypeActivationInfo(guideInfo, netInfo);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class NetTypeActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public NetTypeActivationInfo(GuideInfo guideInfo, NetInfo netInfo) : base(guideInfo)
		{
			this.m_netInfo = netInfo;
		}

		public override string GetName()
		{
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<NetInfo>.PrefabName((uint)this.m_netInfo.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			string icon = this.m_guideInfo.m_icon;
			string thumbnail = this.m_netInfo.m_Thumbnail;
			return StringUtils.SafeFormat(icon, thumbnail);
		}

		public override string FormatText(string pattern)
		{
			string text = PrefabCollection<NetInfo>.PrefabName((uint)this.m_netInfo.m_prefabDataIndex);
			string arg = Locale.Get("NET_TITLE", text);
			return StringUtils.SafeFormat(pattern, arg);
		}

		public override IUITag GetTag()
		{
			string text = this.m_guideInfo.m_tag;
			string arg = PrefabCollection<NetInfo>.PrefabName((uint)this.m_netInfo.m_prefabDataIndex);
			text = StringUtils.SafeFormat(text, arg);
			return MonoTutorialTag.Find(text);
		}

		protected NetInfo m_netInfo;
	}
}
