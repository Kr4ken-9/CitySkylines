﻿using System;
using System.Collections.Generic;
using ColossalFramework.Threading;

public class MultiAsyncTaskWrapper : AsyncTaskBase
{
	public MultiAsyncTaskWrapper(List<string> taskNames, List<Task[]> tasks) : base(null)
	{
		this.m_Tasks = tasks;
		this.m_TaskNames = taskNames;
	}

	public override string name
	{
		get
		{
			for (int i = 0; i < this.m_Tasks.Count; i++)
			{
				for (int j = 0; j < this.m_Tasks[i].Length; j++)
				{
					if (this.m_Tasks[i][j] != null && !this.m_Tasks[i][j].get_hasEnded())
					{
						return this.m_TaskNames[i];
					}
				}
			}
			return null;
		}
	}

	public override bool completedOrFailed
	{
		get
		{
			bool flag = true;
			for (int i = 0; i < this.m_Tasks.Count; i++)
			{
				for (int j = 0; j < this.m_Tasks[i].Length; j++)
				{
					if (this.m_Tasks[i][j] != null)
					{
						flag &= this.m_Tasks[i][j].get_hasEnded();
					}
				}
			}
			return flag;
		}
	}

	public override bool isExecuting
	{
		get
		{
			for (int i = 0; i < this.m_Tasks.Count; i++)
			{
				for (int j = 0; j < this.m_Tasks[i].Length; j++)
				{
					if (this.m_Tasks[i][j] != null && !this.m_Tasks[i][j].get_hasEnded())
					{
						return true;
					}
				}
			}
			return false;
		}
	}

	public void SetTask(int index1, int index2, Task task)
	{
		if (index1 >= 0 && index2 >= 0 && this.m_Tasks.Count > index1 && this.m_Tasks[index1].Length > index2)
		{
			this.m_Tasks[index1][index2] = task;
		}
	}

	public override void Execute()
	{
		throw new NotImplementedException();
	}

	private List<Task[]> m_Tasks;

	private List<string> m_TaskNames;
}
