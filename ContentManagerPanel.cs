﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;

public class ContentManagerPanel : MenuPanel
{
	protected override void Awake()
	{
		base.Awake();
		if (ContentManagerPanel.<>f__mg$cache0 == null)
		{
			ContentManagerPanel.<>f__mg$cache0 = new PackageManager.DefaultEnabledHandler(ContentManagerPanel.AssetEnabledDefault);
		}
		PackageManager.SetEnabledDefault(ContentManagerPanel.<>f__mg$cache0);
	}

	private static bool AssetEnabledDefault(Package.Asset asset)
	{
		return !(asset.get_type() == UserAssetType.DistrictStyleMetaData) || !(asset.get_name() == DistrictStyle.kEuropeanStyleName);
	}

	public void OpenWorkshopInSteam()
	{
		if (PlatformService.IsOverlayEnabled())
		{
			PlatformService.ActivateGameOverlay(6);
		}
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		PackageManager.add_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.Refresh));
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
		PlatformService.get_workshop().add_eventWorkshopSubscriptionChanged(new Workshop.WorkshopSubscriptionChangedHandler(this.OnWorkshopSubscriptionChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		PackageManager.remove_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.Refresh));
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
		PlatformService.get_workshop().remove_eventWorkshopSubscriptionChanged(new Workshop.WorkshopSubscriptionChangedHandler(this.OnWorkshopSubscriptionChanged));
	}

	private void OnLocaleChanged()
	{
		this.CreateCategories();
		this.Refresh();
		this.RefreshPlugins();
	}

	public void SetCategory(int index)
	{
		this.m_Categories.set_selectedIndex(index);
	}

	private void SetContainerCategory(int index)
	{
		this.m_CategoriesContainer.set_selectedIndex(index);
	}

	private void BindEnableDisableAll(UIComponent rootCategory, string disclaimerID = null)
	{
		UIButton uIButton = rootCategory.Find<UIButton>("EnableAll");
		if (disclaimerID != null)
		{
			uIButton.add_eventClick(delegate(UIComponent c, UIMouseEventParameter check)
			{
				SavedBool agreed = new SavedBool(disclaimerID, Settings.gameSettingsFile, false);
				if (!agreed)
				{
					DisclaimerPanel.ShowModal(disclaimerID, agreed, delegate(UIComponent cp, int r)
					{
						if (r > 0)
						{
							agreed.set_value(true);
							this.ToggleActiveCategory(true);
							if (r == 1)
							{
								agreed.Delete();
							}
						}
					});
				}
				else
				{
					this.ToggleActiveCategory(true);
				}
			});
		}
		else
		{
			uIButton.add_eventClick(new MouseEventHandler(this.OnEnableAll));
		}
		UIButton uIButton2 = rootCategory.Find<UIButton>("DisableAll");
		uIButton2.add_eventClick(new MouseEventHandler(this.OnDisableAll));
	}

	protected override void Initialize()
	{
		ContentManagerPanel.subscribedItemsTable = new HashSet<PublishedFileId>(PlatformService.get_workshop().GetSubscribedItems());
		this.m_Categories = base.Find<UIListBox>("Categories");
		this.m_CategoriesContainer = base.Find<UITabContainer>("CategoryContainer");
		UIComponent uIComponent = this.m_CategoriesContainer.Find("SteamWorkshop");
		UIComponent uIComponent2 = this.m_CategoriesContainer.Find("Maps");
		UIComponent uIComponent3 = this.m_CategoriesContainer.Find("Saves");
		UIComponent uIComponent4 = this.m_CategoriesContainer.Find("Assets");
		UIComponent uIComponent5 = this.m_CategoriesContainer.Find("ColorCorrections");
		UIComponent uIComponent6 = this.m_CategoriesContainer.Find("Mods");
		UIComponent uIComponent7 = this.m_CategoriesContainer.Find("Styles");
		UIComponent uIComponent8 = this.m_CategoriesContainer.Find("MapThemes");
		UIComponent uIComponent9 = this.m_CategoriesContainer.Find("Scenarios");
		this.m_MapsContainer = uIComponent2.Find("Content");
		this.m_SavesContainer = uIComponent3.Find("Content");
		this.m_AssetsContainer = uIComponent4.Find("Content");
		this.m_ColorCorrectionsContainer = uIComponent5.Find("Content");
		this.m_ModsContainer = uIComponent6.Find("Content");
		this.m_StylesContainer = uIComponent7.Find("Content");
		this.m_MapThemesContainer = uIComponent8.Find("Content");
		this.m_ScenariosContainer = uIComponent9.Find("Content");
		this.m_SteamWorkshopContainer = uIComponent.Find("Content");
		this.m_SearchField = base.Find<UITextField>("SearchField");
		this.m_SearchField.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnSearchChanged));
		UIButton uIButton = uIComponent.Find<UIButton>("UnsubAll");
		uIButton.add_eventClick(new MouseEventHandler(this.OnUnsbuscribeAll));
		UIButton uIButton2 = uIComponent7.Find<UIButton>("AddStyle");
		uIButton2.add_eventClick(new MouseEventHandler(this.OnAddStyle));
		this.m_SubscribePresetStatusPanel = base.Find<UIPanel>("SubscribePresetStatusPanel").GetComponent<SubscribePresetStatusPanel>();
		this.m_SubscribePresetStatusPanel.get_component().Hide();
		this.BindEnableDisableAll(uIComponent2, null);
		this.BindEnableDisableAll(uIComponent3, null);
		this.BindEnableDisableAll(uIComponent4, null);
		this.BindEnableDisableAll(uIComponent5, null);
		this.BindEnableDisableAll(uIComponent6, Settings.agreedModsDisclaimer);
		this.BindEnableDisableAll(uIComponent7, Settings.agreedStylesDisclaimer);
		this.BindEnableDisableAll(uIComponent8, null);
		this.BindEnableDisableAll(uIComponent9, null);
		this.m_ResetSearchField = this.m_SearchField.Find<UIButton>("Reset");
		this.m_ResetSearchField.Hide();
		this.m_ResetSearchField.add_eventClick(new MouseEventHandler(this.OnResetSearchClicked));
		this.CreateCategories();
		this.m_MapsContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kMapEntryTemplate, UserAssetType.MapMetaData, false);
		this.m_SavesContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kSaveEntryTemplate, UserAssetType.SaveGameMetaData, false);
		this.m_AssetsContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kAssetEntryTemplate, UserAssetType.CustomAssetMetaData, true);
		this.m_ColorCorrectionsContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kAssetEntryTemplate, UserAssetType.ColorCorrection, false);
		this.m_ModsContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kModEntryTemplate, null, false);
		this.m_StylesContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kStyleEntryTemplate, UserAssetType.DistrictStyleMetaData, false);
		this.m_MapThemesContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kMapThemeEntryTemplate, UserAssetType.MapThemeMetaData, false);
		this.m_ScenariosContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kScenarioEntryTemplate, UserAssetType.ScenarioMetaData, false);
		this.m_SteamWorkshopContainer.GetComponent<CategoryContentPanel>().Initialize(ContentManagerPanel.kWorkshopEntryTemplate, null, false);
		this.Refresh();
		this.RefreshCategory(this.m_ModsContainer);
		this.RefreshCategory(this.m_SteamWorkshopContainer);
	}

	private void OnResetSearchClicked(UIComponent c, UIMouseEventParameter p)
	{
		this.m_SearchField.set_text(string.Empty);
		p.Use();
	}

	private void OnUnsbuscribeAll(UIComponent c, UIMouseEventParameter p)
	{
		PublishedFileId[] selectedIds = this.m_SteamWorkshopContainer.GetComponent<CategoryContentPanel>().selectedWorkshopItems;
		if (selectedIds.Length > 0)
		{
			string title = Locale.Get("CONFIRM_UNSUBSCRIBESELECTED", "Title");
			string message = StringUtils.SafeFormat(Locale.Get("CONFIRM_UNSUBSCRIBESELECTED", "Message"), selectedIds.Length);
			ConfirmPanel.ShowModal(title, message, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.MassUnsubscribe(selectedIds);
				}
			});
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_UNSUBSCRIBEALL", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					PublishedFileId[] subscribedItems = PlatformService.get_workshop().GetSubscribedItems();
					this.MassUnsubscribe(subscribedItems);
				}
			});
		}
	}

	private void OnAddStyle(UIComponent c, UIMouseEventParameter p)
	{
		string name = Locale.Get("WORKSHOP_NEWSTYLE");
		StylesHelper.SaveStyle(new DistrictStyleMetaData
		{
			assets = new string[0],
			builtin = false,
			name = name,
			timeStamp = DateTime.Now,
			steamTags = new string[]
			{
				"District Style"
			}
		}, name, true, null);
	}

	private void ToggleActiveCategory(bool active)
	{
		UIComponent uIComponent = this.m_CategoriesContainer.get_components()[this.m_CategoriesContainer.get_selectedIndex()].Find("Content");
		if (uIComponent != null)
		{
			CategoryContentPanel component = uIComponent.GetComponent<CategoryContentPanel>();
			if (component != null)
			{
				component.SetActiveAll(active);
			}
		}
	}

	private void OnEnableAll(UIComponent c, UIMouseEventParameter p)
	{
		this.DisablePackageEvents();
		this.ToggleActiveCategory(true);
		this.EnablePackageEvents(false);
	}

	private void OnDisableAll(UIComponent c, UIMouseEventParameter p)
	{
		this.DisablePackageEvents();
		this.ToggleActiveCategory(false);
		this.EnablePackageEvents(false);
	}

	private void OnSearchChanged(UIComponent c, string search)
	{
		this.m_ResetSearchField.set_isVisible(!string.IsNullOrEmpty(search));
		this.PerformSearch(search);
	}

	private void PerformSearch(string search)
	{
		UIComponent uIComponent = this.m_CategoriesContainer.get_components()[this.m_CategoriesContainer.get_selectedIndex()].Find("Content");
		if (uIComponent != null)
		{
			CategoryContentPanel component = uIComponent.GetComponent<CategoryContentPanel>();
			if (component != null)
			{
				component.SetSearch(search);
			}
		}
	}

	private void AddCategory(string name, UIComponent container)
	{
		List<string> list;
		if (this.m_Categories.get_items() != null)
		{
			list = new List<string>(this.m_Categories.get_items());
		}
		else
		{
			list = new List<string>();
		}
		list.Add(name);
		if (container == null)
		{
			container = this.m_CategoriesContainer.AddUIComponent<UIPanel>();
		}
		container.set_zOrder(list.Count - 1);
		this.m_Categories.set_items(list.ToArray());
	}

	private void CreateCategories()
	{
		this.m_Categories.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnCategoryChanged));
		this.m_Categories.set_items(new string[0]);
		this.m_CategoriesContainer.set_selectedIndex(-1);
		this.AddCategory(Locale.Get("CONTENTMANAGER_MAPS"), this.m_MapsContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_SAVEGAMES"), this.m_SavesContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_ASSETS"), this.m_AssetsContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_COLORCORRECTIONS"), this.m_ColorCorrectionsContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_MODS"), this.m_ModsContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_STYLES"), this.m_StylesContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_MAPTHEMES"), this.m_MapThemesContainer.get_parent().get_parent());
		this.AddCategory(Locale.Get("CONTENTMANAGER_SCENARIOS"), this.m_ScenariosContainer.get_parent().get_parent());
		if (!ContentManagerPanel.noWorkshop)
		{
			this.AddCategory(string.Empty, null);
			this.AddCategory(Locale.Get("CONTENTMANAGER_STEAMWORKSHOP"), this.m_SteamWorkshopContainer.get_parent().get_parent());
			this.m_Categories.set_filteredItems(new int[]
			{
				8
			});
		}
		this.m_Categories.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnCategoryChanged));
		this.m_Categories.set_selectedIndex(0);
	}

	private void OnCategoryChanged(UIComponent c, int index)
	{
		this.SetContainerCategory(index);
		this.PerformSearch(this.m_SearchField.get_text());
	}

	private void Refresh()
	{
		using (AutoProfile.Start("ContentManagerPanel.Refresh()"))
		{
			if (base.get_component().get_isVisible())
			{
				base.get_component().Focus();
			}
			this.RefreshCategory(this.m_MapsContainer);
			this.RefreshCategory(this.m_SavesContainer);
			this.RefreshCategory(this.m_AssetsContainer);
			this.RefreshCategory(this.m_ColorCorrectionsContainer);
			this.RefreshCategory(this.m_StylesContainer);
			this.RefreshCategory(this.m_MapThemesContainer);
			this.RefreshCategory(this.m_ScenariosContainer);
		}
	}

	private void RefreshCategory(UIComponent container)
	{
		CategoryContentPanel component = container.GetComponent<CategoryContentPanel>();
		if (component != null)
		{
			component.Refresh();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
		else
		{
			MainMenu.SetFocus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 286)
			{
				this.RefreshCategory(this.m_ModsContainer);
				p.Use();
			}
			else if (p.get_keycode() == 27)
			{
				UITextField uITextField = UIView.get_activeComponent() as UITextField;
				if (uITextField == null)
				{
					this.OnClosed();
					p.Use();
				}
			}
		}
	}

	private void RefreshPlugins()
	{
		this.RefreshCategory(this.m_ModsContainer);
	}

	private void RefreshWorkshopItems()
	{
		this.RefreshCategory(this.m_SteamWorkshopContainer);
	}

	private void OnWorkshopSubscriptionChanged(PublishedFileId fileID, bool subscribed)
	{
		if (subscribed)
		{
			ContentManagerPanel.subscribedItemsTable.Add(fileID);
		}
		else
		{
			ContentManagerPanel.subscribedItemsTable.Remove(fileID);
		}
		this.RefreshWorkshopItems();
		this.RefreshCategory(this.m_SavesContainer);
	}

	private void RefreshSubscriptionTable()
	{
		ContentManagerPanel.subscribedItemsTable = new HashSet<PublishedFileId>(PlatformService.get_workshop().GetSubscribedItems());
		this.RefreshCategory(this.m_SavesContainer);
	}

	public void MassSubscribe(IEnumerable<PublishedFileId> ids)
	{
		this.m_SubscribePresetStatusPanel.BeginSubscribe(ids, new Action(this.RefreshSubscriptionTable));
	}

	public void MassUnsubscribe(PublishedFileId[] ids)
	{
		base.StartCoroutine(this.MassUnsubscribeCoroutine(ids, new Action(this.RefreshSubscriptionTable)));
	}

	private void DisablePackageEvents()
	{
		PackageManager.DisableEvents();
		PluginManager.DisableEvents();
	}

	private void EnablePackageEvents(bool refresh = false)
	{
		PackageManager.EnabledEvents();
		PluginManager.EnabledEvents();
		if (refresh)
		{
			PackageManager.ForcePackagesChanged();
			Singleton<PluginManager>.get_instance().ForcePluginsChanged();
		}
	}

	[DebuggerHidden]
	private IEnumerator MassUnsubscribeCoroutine(PublishedFileId[] ids, Action callback = null)
	{
		ContentManagerPanel.<MassUnsubscribeCoroutine>c__Iterator0 <MassUnsubscribeCoroutine>c__Iterator = new ContentManagerPanel.<MassUnsubscribeCoroutine>c__Iterator0();
		<MassUnsubscribeCoroutine>c__Iterator.ids = ids;
		<MassUnsubscribeCoroutine>c__Iterator.callback = callback;
		<MassUnsubscribeCoroutine>c__Iterator.$this = this;
		return <MassUnsubscribeCoroutine>c__Iterator;
	}

	private static readonly string kWorkshopEntryTemplate = "WorkshopEntryTemplate";

	private static readonly string kMapEntryTemplate = "MapEntryTemplate";

	private static readonly string kModEntryTemplate = "ModEntryTemplate";

	private static readonly string kSaveEntryTemplate = "SaveEntryTemplate";

	private static readonly string kAssetEntryTemplate = "AssetEntryTemplate";

	private static readonly string kStyleEntryTemplate = "StyleEntryTemplate";

	private static readonly string kMapThemeEntryTemplate = "MapThemeEntryTemplate";

	private static readonly string kScenarioEntryTemplate = "ScenarioEntryTemplate";

	internal static bool noWorkshop;

	private UIComponent m_MapsContainer;

	private UIComponent m_SavesContainer;

	private UIComponent m_AssetsContainer;

	private UIComponent m_ColorCorrectionsContainer;

	private UIComponent m_ModsContainer;

	private UIComponent m_SteamWorkshopContainer;

	private UIComponent m_StylesContainer;

	private UIComponent m_MapThemesContainer;

	private UIComponent m_ScenariosContainer;

	private UIListBox m_Categories;

	private UITabContainer m_CategoriesContainer;

	private UITextField m_SearchField;

	private UIButton m_ResetSearchField;

	private SubscribePresetStatusPanel m_SubscribePresetStatusPanel;

	public static HashSet<PublishedFileId> subscribedItemsTable;

	[CompilerGenerated]
	private static PackageManager.DefaultEnabledHandler <>f__mg$cache0;
}
