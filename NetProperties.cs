﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

[ExecuteInEditMode]
public class NetProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	public static void EnsureTextureSettings(Texture texture)
	{
		texture.set_wrapMode(0);
		texture.set_filterMode(2);
		texture.set_anisoLevel(8);
	}

	private void OverrideFromMapTheme()
	{
		MapThemeMetaData mapThemeMetaData = Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData;
		if (mapThemeMetaData != null)
		{
			if (mapThemeMetaData.upwardRoadDiffuse != null)
			{
				this.m_upwardDiffuse = mapThemeMetaData.upwardRoadDiffuse.Instantiate<Texture2D>();
				NetProperties.EnsureTextureSettings(this.m_upwardDiffuse);
			}
			if (mapThemeMetaData.downwardRoadDiffuse != null)
			{
				this.m_downwardDiffuse = mapThemeMetaData.downwardRoadDiffuse.Instantiate<Texture2D>();
				NetProperties.EnsureTextureSettings(this.m_downwardDiffuse);
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		NetProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new NetProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	public void InitializeShaderProperties()
	{
		Shader.SetGlobalTexture("_RoadUpwardDiffuse", this.m_upwardDiffuse);
		Shader.SetGlobalTexture("_RoadUpwardWetXYS", this.m_upwardWetXYS);
		Shader.SetGlobalTexture("_RoadDownwardDiffuse", this.m_downwardDiffuse);
		Shader.SetGlobalTexture("_RoadDirectionArrow", this.m_directionArrow);
		Matrix4x4 matrix4x = NetSegment.CalculateControlMatrix(new Vector3(-10f, 0f, 20f), new Vector3(-10f, 0f, 10f), new Vector3(-10f, 0f, -10f), new Vector3(-10f, 0f, -20f), new Vector3(10f, 0f, 20f), new Vector3(10f, 0f, 10f), new Vector3(10f, 0f, -10f), new Vector3(10f, 0f, -20f), Vector3.get_zero(), 0.05f);
		Matrix4x4 matrix4x2 = NetSegment.CalculateControlMatrix(new Vector3(10f, 0f, 20f), new Vector3(10f, 0f, 10f), new Vector3(10f, 0f, -10f), new Vector3(10f, 0f, -20f), new Vector3(-10f, 0f, 20f), new Vector3(-10f, 0f, 10f), new Vector3(-10f, 0f, -10f), new Vector3(-10f, 0f, -20f), Vector3.get_zero(), 0.05f);
		Shader.SetGlobalMatrix("_LeftMatrix", matrix4x);
		Shader.SetGlobalMatrix("_RightMatrix", matrix4x2);
		Shader.SetGlobalVector("_MeshScale", new Vector4(0.05f, 0.05f, 1f, 1f));
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("NetProperties");
			Singleton<NetManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Texture m_upwardDiffuse;

	public Texture m_upwardWetXYS;

	public Texture m_downwardDiffuse;

	public EffectInfo m_placementEffect;

	public EffectInfo m_bulldozeEffect;

	public AudioInfo m_drawSound;

	public Texture m_directionArrow;

	public UIFont m_nameFont;

	public Shader m_nameShader;

	public Material m_adjustMaterial;

	public Material m_adjustMaterialSurface;

	public Color m_nameColor = Color.get_white();
}
