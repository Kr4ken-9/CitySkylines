﻿using System;
using ColossalFramework.UI;

public class TextResizer : UICustomControl
{
	private void Awake()
	{
		this.m_label = base.GetComponent<UILabel>();
		this.m_label.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.OnTextChanged));
		this.m_parent = this.m_label.get_parent();
	}

	private void OnTextChanged(UIComponent component, string value)
	{
		this.m_label.set_textScale(1f);
		float num = 1f;
		while (num > 0f)
		{
			float num2 = this.m_label.get_relativePosition().x + this.m_label.get_width();
			if (num2 <= this.m_parent.get_width())
			{
				break;
			}
			num -= 0.02f;
			this.m_label.set_textScale(num);
		}
	}

	private UILabel m_label;

	private UIComponent m_parent;
}
