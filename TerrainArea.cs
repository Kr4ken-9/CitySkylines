﻿using System;
using UnityEngine;

public class TerrainArea
{
	public TerrainArea(int x, int z, int resolution)
	{
		this.m_x = x;
		this.m_z = z;
		this.m_resolution = resolution;
		this.m_limits = new short[resolution * 4];
		this.Reset();
	}

	public void Reset()
	{
		this.m_minX = 10000;
		this.m_minZ = 10000;
		this.m_maxX = -10000;
		this.m_maxZ = -10000;
	}

	public void UpdateBounds()
	{
		this.m_min.x = ((float)this.m_minX - 540f) * 16f - 16f;
		this.m_min.y = 0f;
		this.m_min.z = ((float)this.m_minZ - 540f) * 16f - 16f;
		this.m_max.x = ((float)this.m_maxX - 540f) * 16f + 16f;
		this.m_max.y = 1024f;
		this.m_max.z = ((float)this.m_maxZ - 540f) * 16f + 16f;
	}

	public void GetLimitsX(int z, out int minX, out int maxX)
	{
		int num = this.m_resolution * 2 - this.m_z;
		int num2 = this.m_resolution * 3 - this.m_z;
		minX = (int)this.m_limits[z + num];
		maxX = (int)this.m_limits[z + num2];
	}

	public void GetLimitsZ(int x, out int minZ, out int maxZ)
	{
		int num = -this.m_x;
		int num2 = this.m_resolution - this.m_x;
		minZ = (int)this.m_limits[x + num];
		maxZ = (int)this.m_limits[x + num2];
	}

	public void AddArea(int minX, int minZ, int maxX, int maxZ)
	{
		if (minX < this.m_x)
		{
			minX = this.m_x;
		}
		if (minZ < this.m_z)
		{
			minZ = this.m_z;
		}
		if (maxX >= this.m_x + this.m_resolution)
		{
			maxX = this.m_x + this.m_resolution - 1;
		}
		if (maxZ >= this.m_z + this.m_resolution)
		{
			maxZ = this.m_z + this.m_resolution - 1;
		}
		if (minX > maxX || minZ > maxZ)
		{
			return;
		}
		int num = -this.m_x;
		int num2 = this.m_resolution - this.m_x;
		int num3 = this.m_resolution * 2 - this.m_z;
		int num4 = this.m_resolution * 3 - this.m_z;
		if (this.m_minX > this.m_maxX || this.m_minZ > this.m_maxZ)
		{
			for (int i = minX; i <= maxX; i++)
			{
				this.m_limits[i + num] = (short)minZ;
				this.m_limits[i + num2] = (short)maxZ;
			}
			for (int j = minZ; j <= maxZ; j++)
			{
				this.m_limits[j + num3] = (short)minX;
				this.m_limits[j + num4] = (short)maxX;
			}
			this.m_minX = minX;
			this.m_minZ = minZ;
			this.m_maxX = maxX;
			this.m_maxZ = maxZ;
		}
		else
		{
			int num5 = Mathf.Max(minX, this.m_minX);
			int num6 = Mathf.Max(minZ, this.m_minZ);
			int num7 = Mathf.Min(maxX, this.m_maxX);
			int num8 = Mathf.Min(maxZ, this.m_maxZ);
			for (int k = num5; k <= num7; k++)
			{
				this.m_limits[k + num] = (short)Mathf.Min((int)this.m_limits[k + num], minZ);
				this.m_limits[k + num2] = (short)Mathf.Max((int)this.m_limits[k + num2], maxZ);
			}
			for (int l = num6; l <= num8; l++)
			{
				this.m_limits[l + num3] = (short)Mathf.Min((int)this.m_limits[l + num3], minX);
				this.m_limits[l + num4] = (short)Mathf.Max((int)this.m_limits[l + num4], maxX);
			}
			while (minX < this.m_minX)
			{
				this.m_minX--;
				if (this.m_minX > maxX)
				{
					this.m_limits[this.m_minX + num] = (short)(this.m_z + this.m_resolution);
					this.m_limits[this.m_minX + num2] = (short)(this.m_z - 1);
				}
				else
				{
					this.m_limits[this.m_minX + num] = (short)minZ;
					this.m_limits[this.m_minX + num2] = (short)maxZ;
				}
			}
			while (maxX > this.m_maxX)
			{
				this.m_maxX++;
				if (this.m_maxX < minX)
				{
					this.m_limits[this.m_maxX + num] = (short)(this.m_z + this.m_resolution);
					this.m_limits[this.m_maxX + num2] = (short)(this.m_z - 1);
				}
				else
				{
					this.m_limits[this.m_maxX + num] = (short)minZ;
					this.m_limits[this.m_maxX + num2] = (short)maxZ;
				}
			}
			while (minZ < this.m_minZ)
			{
				this.m_minZ--;
				if (this.m_minZ > maxZ)
				{
					this.m_limits[this.m_minZ + num3] = (short)(this.m_x + this.m_resolution);
					this.m_limits[this.m_minZ + num4] = (short)(this.m_x - 1);
				}
				else
				{
					this.m_limits[this.m_minZ + num3] = (short)minX;
					this.m_limits[this.m_minZ + num4] = (short)maxX;
				}
			}
			while (maxZ > this.m_maxZ)
			{
				this.m_maxZ++;
				if (this.m_maxZ < minZ)
				{
					this.m_limits[this.m_maxZ + num3] = (short)(this.m_x + this.m_resolution);
					this.m_limits[this.m_maxZ + num4] = (short)(this.m_x - 1);
				}
				else
				{
					this.m_limits[this.m_maxZ + num3] = (short)minX;
					this.m_limits[this.m_maxZ + num4] = (short)maxX;
				}
			}
		}
	}

	public void AddArea(TerrainArea area)
	{
		int i = area.m_minX;
		int j = area.m_maxX;
		int k = area.m_minZ;
		int l = area.m_maxZ;
		if (i < this.m_x)
		{
			i = this.m_x;
		}
		if (k < this.m_z)
		{
			k = this.m_z;
		}
		if (j >= this.m_x + this.m_resolution)
		{
			j = this.m_x + this.m_resolution - 1;
		}
		if (l >= this.m_z + this.m_resolution)
		{
			l = this.m_z + this.m_resolution - 1;
		}
		if (i > j || k > l)
		{
			return;
		}
		int num = -this.m_x;
		int num2 = this.m_resolution - this.m_x;
		int num3 = this.m_resolution * 2 - this.m_z;
		int num4 = this.m_resolution * 3 - this.m_z;
		int num5 = -area.m_x;
		int num6 = area.m_resolution - area.m_x;
		int num7 = area.m_resolution * 2 - area.m_z;
		int num8 = area.m_resolution * 3 - area.m_z;
		if (this.m_minX > this.m_maxX || this.m_minZ > this.m_maxZ)
		{
			for (int m = i; m <= j; m++)
			{
				this.m_limits[m + num] = area.m_limits[m + num5];
				this.m_limits[m + num2] = area.m_limits[m + num6];
			}
			for (int n = k; n <= l; n++)
			{
				this.m_limits[n + num3] = area.m_limits[n + num7];
				this.m_limits[n + num4] = area.m_limits[n + num8];
			}
			this.m_minX = i;
			this.m_minZ = k;
			this.m_maxX = j;
			this.m_maxZ = l;
		}
		else
		{
			int num9 = Mathf.Max(i, this.m_minX);
			int num10 = Mathf.Max(k, this.m_minZ);
			int num11 = Mathf.Min(j, this.m_maxX);
			int num12 = Mathf.Min(l, this.m_maxZ);
			for (int num13 = num9; num13 <= num11; num13++)
			{
				this.m_limits[num13 + num] = (short)Mathf.Min((int)this.m_limits[num13 + num], (int)area.m_limits[num13 + num5]);
				this.m_limits[num13 + num2] = (short)Mathf.Max((int)this.m_limits[num13 + num2], (int)area.m_limits[num13 + num6]);
			}
			for (int num14 = num10; num14 <= num12; num14++)
			{
				this.m_limits[num14 + num3] = (short)Mathf.Min((int)this.m_limits[num14 + num3], (int)area.m_limits[num14 + num7]);
				this.m_limits[num14 + num4] = (short)Mathf.Max((int)this.m_limits[num14 + num4], (int)area.m_limits[num14 + num8]);
			}
			while (i < this.m_minX)
			{
				this.m_minX--;
				if (this.m_minX > j)
				{
					this.m_limits[this.m_minX + num] = (short)(this.m_z + this.m_resolution);
					this.m_limits[this.m_minX + num2] = (short)(this.m_z - 1);
				}
				else
				{
					this.m_limits[this.m_minX + num] = area.m_limits[this.m_minX + num5];
					this.m_limits[this.m_minX + num2] = area.m_limits[this.m_minX + num6];
				}
			}
			while (j > this.m_maxX)
			{
				this.m_maxX++;
				if (this.m_maxX < i)
				{
					this.m_limits[this.m_maxX + num] = (short)(this.m_z + this.m_resolution);
					this.m_limits[this.m_maxX + num2] = (short)(this.m_z - 1);
				}
				else
				{
					this.m_limits[this.m_maxX + num] = area.m_limits[this.m_maxX + num5];
					this.m_limits[this.m_maxX + num2] = area.m_limits[this.m_maxX + num6];
				}
			}
			while (k < this.m_minZ)
			{
				this.m_minZ--;
				if (this.m_minZ > l)
				{
					this.m_limits[this.m_minZ + num3] = (short)(this.m_x + this.m_resolution);
					this.m_limits[this.m_minZ + num4] = (short)(this.m_x - 1);
				}
				else
				{
					this.m_limits[this.m_minZ + num3] = area.m_limits[this.m_minZ + num7];
					this.m_limits[this.m_minZ + num4] = area.m_limits[this.m_minZ + num8];
				}
			}
			while (l > this.m_maxZ)
			{
				this.m_maxZ++;
				if (this.m_maxZ < k)
				{
					this.m_limits[this.m_maxZ + num3] = (short)(this.m_x + this.m_resolution);
					this.m_limits[this.m_maxZ + num4] = (short)(this.m_x - 1);
				}
				else
				{
					this.m_limits[this.m_maxZ + num3] = area.m_limits[this.m_maxZ + num7];
					this.m_limits[this.m_maxZ + num4] = area.m_limits[this.m_maxZ + num8];
				}
			}
		}
	}

	public int m_x;

	public int m_z;

	public int m_resolution;

	public int m_minX;

	public int m_maxX;

	public int m_minZ;

	public int m_maxZ;

	public Vector3 m_min;

	public Vector3 m_max;

	public short[] m_limits;
}
