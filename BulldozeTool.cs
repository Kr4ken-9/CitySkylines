﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class BulldozeTool : DefaultTool
{
	public event BulldozeTool.BuildingDeleted m_BuildingDeleted
	{
		add
		{
			BulldozeTool.BuildingDeleted buildingDeleted = this.m_BuildingDeleted;
			BulldozeTool.BuildingDeleted buildingDeleted2;
			do
			{
				buildingDeleted2 = buildingDeleted;
				buildingDeleted = Interlocked.CompareExchange<BulldozeTool.BuildingDeleted>(ref this.m_BuildingDeleted, (BulldozeTool.BuildingDeleted)Delegate.Combine(buildingDeleted2, value), buildingDeleted);
			}
			while (buildingDeleted != buildingDeleted2);
		}
		remove
		{
			BulldozeTool.BuildingDeleted buildingDeleted = this.m_BuildingDeleted;
			BulldozeTool.BuildingDeleted buildingDeleted2;
			do
			{
				buildingDeleted2 = buildingDeleted;
				buildingDeleted = Interlocked.CompareExchange<BulldozeTool.BuildingDeleted>(ref this.m_BuildingDeleted, (BulldozeTool.BuildingDeleted)Delegate.Remove(buildingDeleted2, value), buildingDeleted);
			}
			while (buildingDeleted != buildingDeleted2);
		}
	}

	private UIMultiStateButton bulldozerUndergroundToggle
	{
		get
		{
			if (this.m_bulldozerUndergroundToggle == null)
			{
				this.m_bulldozerUndergroundToggle = UIView.Find<UIMultiStateButton>("BulldozerUndergroundToggle");
			}
			return this.m_bulldozerUndergroundToggle;
		}
	}

	public BulldozeTool.Layer BulldozeLayer
	{
		get
		{
			InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
			if (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating)
			{
				return BulldozeTool.Layer.Pipes;
			}
			if (currentMode != InfoManager.InfoMode.Underground)
			{
				return BulldozeTool.Layer.Default;
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
			{
				return BulldozeTool.Layer.Tunnels;
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WaterPower)
			{
				return BulldozeTool.Layer.Pipes;
			}
			return BulldozeTool.Layer.Default;
		}
		set
		{
			if (value != this.BulldozeLayer)
			{
				if (value != BulldozeTool.Layer.Default)
				{
					if (value != BulldozeTool.Layer.Tunnels)
					{
						if (value == BulldozeTool.Layer.Pipes)
						{
							this.m_manualUndergroundMode = true;
							Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.Underground, InfoManager.SubInfoMode.WaterPower);
						}
					}
					else
					{
						this.m_manualUndergroundMode = true;
						Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.Underground, InfoManager.SubInfoMode.Default);
					}
				}
				else
				{
					Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
				}
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_buildElevationUp = new SavedInputKey(Settings.buildElevationUp, Settings.gameSettingsFile, DefaultSettings.buildElevationUp, true);
		this.m_buildElevationDown = new SavedInputKey(Settings.buildElevationDown, Settings.gameSettingsFile, DefaultSettings.buildElevationDown, true);
	}

	protected override void OnToolGUI(Event e)
	{
		bool isInsideUI = this.m_toolController.IsInsideUI;
		if (e.get_type() == null)
		{
			if (!isInsideUI && e.get_button() == 0)
			{
				InstanceID hoverInstance = this.m_hoverInstance;
				InstanceID hoverInstance2 = this.m_hoverInstance2;
				if (this.m_selectErrors == ToolBase.ToolErrors.None)
				{
					InstanceType type = hoverInstance.Type;
					switch (type)
					{
					case InstanceType.NetNode:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteNode(hoverInstance.NetNode));
						goto IL_15F;
					case InstanceType.NetSegment:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteSegment(hoverInstance.NetSegment, hoverInstance2.NetSegment));
						goto IL_15F;
					case InstanceType.ParkedVehicle:
					case InstanceType.CitizenInstance:
					case InstanceType.Event:
					case InstanceType.NetLane:
					case InstanceType.BuildingProp:
					case InstanceType.NetLaneProp:
						IL_81:
						if (type != InstanceType.Building)
						{
							goto IL_15F;
						}
						Singleton<SimulationManager>.get_instance().AddAction(this.TryDeleteBuilding(hoverInstance.Building));
						goto IL_15F;
					case InstanceType.TransportLine:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteLine(hoverInstance.TransportLine));
						goto IL_15F;
					case InstanceType.Prop:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteProp(hoverInstance.Prop));
						goto IL_15F;
					case InstanceType.Tree:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteTree(hoverInstance.Tree));
						goto IL_15F;
					case InstanceType.Disaster:
						Singleton<SimulationManager>.get_instance().AddAction(this.DeleteDisaster(hoverInstance.Disaster));
						goto IL_15F;
					}
					goto IL_81;
				}
			}
			IL_15F:;
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.CancelBulldoze());
			}
		}
		else if (this.m_buildElevationUp.IsPressed(e))
		{
			if (this.BulldozeLayer == BulldozeTool.Layer.Tunnels && this.bulldozerUndergroundToggle != null)
			{
				this.bulldozerUndergroundToggle.SimulateClick();
			}
		}
		else if (this.m_buildElevationDown.IsPressed(e) && this.BulldozeLayer == BulldozeTool.Layer.Default && this.bulldozerUndergroundToggle != null)
		{
			this.bulldozerUndergroundToggle.SimulateClick();
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_bulldozingMode = BulldozeTool.Mode.Select;
		this.m_bulldozingService = ItemClass.Service.None;
		this.m_bulldozingLayers = ItemClass.Layer.None;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		Singleton<SimulationManager>.get_instance().AddAction(this.CancelBulldoze());
	}

	protected override void OnToolUpdate()
	{
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			ushort building = this.m_hoverInstance.Building;
			ushort netSegment = this.m_hoverInstance.NetSegment;
			ushort netSegment2 = this.m_hoverInstance2.NetSegment;
			uint tree = this.m_hoverInstance.Tree;
			int refundAmount = this.m_refundAmount;
			string text = null;
			if (refundAmount != 0)
			{
				text = StringUtils.SafeFormat(Locale.Get("TOOL_REFUND_AMOUNT"), refundAmount / 100);
			}
			if (building != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				Vector3 position = instance.m_buildings.m_buffer[(int)building].m_position;
				base.ShowToolInfo(true, text, position);
			}
			else if (netSegment != 0)
			{
				Vector3 vector = this.GetSegmentPosition(netSegment);
				if (netSegment2 != 0)
				{
					vector = (vector + this.GetSegmentPosition(netSegment2)) * 0.5f;
				}
				base.ShowToolInfo(true, text, vector);
			}
			else if (tree != 0u)
			{
				TreeManager instance2 = Singleton<TreeManager>.get_instance();
				Vector3 position2 = instance2.m_trees.m_buffer[(int)((UIntPtr)tree)].Position;
				base.ShowToolInfo(true, text, position2);
			}
			else
			{
				base.ShowToolInfo(false, null, Vector3.get_zero());
			}
		}
		else
		{
			base.ShowToolInfo(false, null, Vector3.get_zero());
		}
		CursorInfo cursorInfo = null;
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		if (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating)
		{
			cursorInfo = this.m_undergroundCursor;
		}
		if (cursorInfo == null)
		{
			cursorInfo = this.m_cursor;
		}
		base.ToolCursor = cursorInfo;
	}

	private Vector3 GetSegmentPosition(ushort segment)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		return (position + position2) * 0.5f;
	}

	protected override void OnToolLateUpdate()
	{
		base.OnToolLateUpdate();
		if (this.m_deleteTimer > 0f)
		{
			this.m_deleteTimer = Mathf.Max(0f, this.m_deleteTimer - Time.get_deltaTime());
		}
	}

	protected override bool EnableMouseLight()
	{
		return false;
	}

	[DebuggerHidden]
	private IEnumerator CancelBulldoze()
	{
		BulldozeTool.<CancelBulldoze>c__Iterator0 <CancelBulldoze>c__Iterator = new BulldozeTool.<CancelBulldoze>c__Iterator0();
		<CancelBulldoze>c__Iterator.$this = this;
		return <CancelBulldoze>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator DeleteNode(ushort node)
	{
		BulldozeTool.<DeleteNode>c__Iterator1 <DeleteNode>c__Iterator = new BulldozeTool.<DeleteNode>c__Iterator1();
		<DeleteNode>c__Iterator.node = node;
		<DeleteNode>c__Iterator.$this = this;
		return <DeleteNode>c__Iterator;
	}

	private void DeleteNodeImpl(ushort node)
	{
		if (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_flags != NetNode.Flags.None)
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].Info;
			this.m_bulldozingMode = BulldozeTool.Mode.NodeOrSegment;
			this.m_bulldozingService = info.m_class.m_service;
			this.m_bulldozingLayers = info.m_class.m_layer;
			this.m_deleteTimer = 0.1f;
			Singleton<NetManager>.get_instance().ReleaseNode(node);
		}
		if (node == this.m_hoverInstance.NetNode)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
		if (node == this.m_lastInstance.NetNode)
		{
			this.m_lastInstance = InstanceID.Empty;
		}
	}

	[DebuggerHidden]
	private IEnumerator DeleteSegment(ushort segment, ushort segment2)
	{
		BulldozeTool.<DeleteSegment>c__Iterator2 <DeleteSegment>c__Iterator = new BulldozeTool.<DeleteSegment>c__Iterator2();
		<DeleteSegment>c__Iterator.segment = segment;
		<DeleteSegment>c__Iterator.segment2 = segment2;
		<DeleteSegment>c__Iterator.$this = this;
		return <DeleteSegment>c__Iterator;
	}

	private void DeleteSegmentImpl(ushort segment)
	{
		if (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_flags != NetSegment.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
			this.m_bulldozingMode = BulldozeTool.Mode.NodeOrSegment;
			this.m_bulldozingService = info.m_class.m_service;
			this.m_bulldozingLayers = info.m_class.m_layer;
			this.m_deleteTimer = 0.1f;
			ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			Vector3 startDirection = instance.m_segments.m_buffer[(int)segment].m_startDirection;
			Vector3 endDirection = instance.m_segments.m_buffer[(int)segment].m_endDirection;
			this.m_lastNetInfo = info;
			this.m_lastStartPos = position;
			this.m_lastEndPos = position2;
			this.m_lastStartDir = startDirection;
			this.m_lastEndDir = endDirection;
			int segmentRefundAmount = this.GetSegmentRefundAmount(segment);
			if (segmentRefundAmount != 0)
			{
				Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.RefundAmount, segmentRefundAmount, info.m_class);
			}
			bool smoothStart = (instance.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
			bool smoothEnd = (instance.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
			Vector3 middlePos;
			Vector3 middlePos2;
			NetSegment.CalculateMiddlePoints(position, startDirection, position2, endDirection, smoothStart, smoothEnd, out middlePos, out middlePos2);
			info.m_netAI.ManualDeactivation(segment, ref instance.m_segments.m_buffer[(int)segment]);
			info.m_netAI.BulldozeSucceeded();
			instance.ReleaseSegment(segment, false);
			if (info.m_class.m_service == ItemClass.Service.Road)
			{
				Singleton<CoverageManager>.get_instance().CoverageUpdated(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None);
			}
			NetTool.DispatchPlacementEffect(position, middlePos, middlePos2, position2, info.m_halfWidth, true);
		}
		if (segment == this.m_hoverInstance.NetSegment)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
		if (segment == this.m_hoverInstance2.NetSegment)
		{
			this.m_hoverInstance2 = InstanceID.Empty;
		}
		if (segment == this.m_lastInstance.NetSegment)
		{
			this.m_lastInstance = InstanceID.Empty;
		}
		if (segment == this.m_lastInstance2.NetSegment)
		{
			this.m_lastInstance2 = InstanceID.Empty;
		}
	}

	[DebuggerHidden]
	private IEnumerator TryDeleteBuilding(ushort building)
	{
		BulldozeTool.<TryDeleteBuilding>c__Iterator3 <TryDeleteBuilding>c__Iterator = new BulldozeTool.<TryDeleteBuilding>c__Iterator3();
		<TryDeleteBuilding>c__Iterator.building = building;
		<TryDeleteBuilding>c__Iterator.$this = this;
		return <TryDeleteBuilding>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator DeleteBuilding(ushort building)
	{
		BulldozeTool.<DeleteBuilding>c__Iterator4 <DeleteBuilding>c__Iterator = new BulldozeTool.<DeleteBuilding>c__Iterator4();
		<DeleteBuilding>c__Iterator.building = building;
		<DeleteBuilding>c__Iterator.$this = this;
		return <DeleteBuilding>c__Iterator;
	}

	private void DeleteBuildingImpl(ushort building)
	{
		if (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_flags != Building.Flags.None)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			if (info.m_buildingAI.CheckBulldozing(building, ref instance.m_buildings.m_buffer[(int)building]) != ToolBase.ToolErrors.None)
			{
				return;
			}
			if (info.m_placementStyle == ItemClass.Placement.Automatic)
			{
				this.m_bulldozingMode = BulldozeTool.Mode.Building;
				this.m_bulldozingService = info.m_class.m_service;
				this.m_bulldozingLayers = info.m_class.m_layer;
				this.m_deleteTimer = 0.1f;
			}
			int buildingRefundAmount = this.GetBuildingRefundAmount(building);
			if (buildingRefundAmount != 0)
			{
				Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.RefundAmount, buildingRefundAmount, info.m_class);
			}
			Vector3 position = instance.m_buildings.m_buffer[(int)building].m_position;
			float angle = instance.m_buildings.m_buffer[(int)building].m_angle;
			int width = instance.m_buildings.m_buffer[(int)building].Width;
			int length = instance.m_buildings.m_buffer[(int)building].Length;
			bool collapsed = (instance.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.Collapsed) != Building.Flags.None;
			instance.ReleaseBuilding(building);
			int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
			if (publicServiceIndex != -1)
			{
				Singleton<CoverageManager>.get_instance().CoverageUpdated(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			}
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_BuildingDeleted != null)
				{
					this.m_BuildingDeleted(building);
				}
			});
			BuildingTool.DispatchPlacementEffect(info, building, position, angle, width, length, true, collapsed);
		}
		if (building == this.m_hoverInstance.Building)
		{
			this.m_hoverInstance.Building = 0;
		}
		if (building == this.m_lastInstance.Building)
		{
			this.m_lastInstance.Building = 0;
		}
	}

	[DebuggerHidden]
	private IEnumerator DeleteProp(ushort prop)
	{
		BulldozeTool.<DeleteProp>c__Iterator5 <DeleteProp>c__Iterator = new BulldozeTool.<DeleteProp>c__Iterator5();
		<DeleteProp>c__Iterator.prop = prop;
		<DeleteProp>c__Iterator.$this = this;
		return <DeleteProp>c__Iterator;
	}

	private void DeletePropImpl(ushort prop)
	{
		if (Singleton<PropManager>.get_instance().m_props.m_buffer[(int)prop].m_flags != 0)
		{
			PropManager instance = Singleton<PropManager>.get_instance();
			this.m_bulldozingMode = BulldozeTool.Mode.PropOrTree;
			this.m_bulldozingService = ItemClass.Service.None;
			this.m_bulldozingLayers = ItemClass.Layer.None;
			this.m_deleteTimer = 0.1f;
			instance.ReleaseProp(prop);
			PropTool.DispatchPlacementEffect(this.m_mousePosition, true);
		}
		if (prop == this.m_hoverInstance.Prop)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
		if (prop == this.m_lastInstance.Prop)
		{
			this.m_lastInstance = InstanceID.Empty;
		}
	}

	[DebuggerHidden]
	private IEnumerator DeleteTree(uint tree)
	{
		BulldozeTool.<DeleteTree>c__Iterator6 <DeleteTree>c__Iterator = new BulldozeTool.<DeleteTree>c__Iterator6();
		<DeleteTree>c__Iterator.tree = tree;
		<DeleteTree>c__Iterator.$this = this;
		return <DeleteTree>c__Iterator;
	}

	private void DeleteTreeImpl(uint tree)
	{
		if (Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)tree)].m_flags != 0)
		{
			if ((Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)tree)].m_flags & 128) != 0)
			{
				return;
			}
			TreeManager instance = Singleton<TreeManager>.get_instance();
			this.m_bulldozingMode = BulldozeTool.Mode.PropOrTree;
			this.m_bulldozingService = ItemClass.Service.None;
			this.m_bulldozingLayers = ItemClass.Layer.None;
			this.m_deleteTimer = 0.1f;
			instance.ReleaseTree(tree);
			TreeTool.DispatchPlacementEffect(this.m_mousePosition, true);
		}
		if (tree == this.m_hoverInstance.Tree)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
		if (tree == this.m_lastInstance.Tree)
		{
			this.m_lastInstance = InstanceID.Empty;
		}
	}

	[DebuggerHidden]
	private IEnumerator DeleteLine(ushort line)
	{
		BulldozeTool.<DeleteLine>c__Iterator7 <DeleteLine>c__Iterator = new BulldozeTool.<DeleteLine>c__Iterator7();
		<DeleteLine>c__Iterator.line = line;
		<DeleteLine>c__Iterator.$this = this;
		return <DeleteLine>c__Iterator;
	}

	private void DeleteLineImpl(ushort line)
	{
		if (Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)line].m_flags != TransportLine.Flags.None)
		{
			this.m_bulldozingMode = BulldozeTool.Mode.Select;
			this.m_bulldozingService = ItemClass.Service.None;
			this.m_bulldozingLayers = ItemClass.Layer.None;
			this.m_deleteTimer = 0.1f;
			Singleton<TransportManager>.get_instance().ReleaseLine(line);
		}
		if (line == this.m_hoverInstance.TransportLine)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
	}

	[DebuggerHidden]
	private IEnumerator DeleteDisaster(ushort disaster)
	{
		BulldozeTool.<DeleteDisaster>c__Iterator8 <DeleteDisaster>c__Iterator = new BulldozeTool.<DeleteDisaster>c__Iterator8();
		<DeleteDisaster>c__Iterator.disaster = disaster;
		<DeleteDisaster>c__Iterator.$this = this;
		return <DeleteDisaster>c__Iterator;
	}

	private void DeleteDisasterImpl(ushort disaster)
	{
		if (Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)disaster].m_flags != DisasterData.Flags.None)
		{
			this.m_bulldozingMode = BulldozeTool.Mode.Select;
			this.m_bulldozingService = ItemClass.Service.None;
			this.m_bulldozingLayers = ItemClass.Layer.None;
			this.m_deleteTimer = 0.1f;
			Singleton<DisasterManager>.get_instance().ReleaseDisaster(disaster);
			DisasterTool.DispatchPlacementEffect(this.m_mousePosition, true);
		}
		if (disaster == this.m_hoverInstance.Disaster)
		{
			this.m_hoverInstance = InstanceID.Empty;
		}
	}

	public override bool GetTerrainIgnore()
	{
		return true;
	}

	public override NetNode.Flags GetNodeIgnoreFlags()
	{
		return NetNode.Flags.All;
	}

	public override NetSegment.Flags GetSegmentIgnoreFlags(out bool nameOnly)
	{
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.ScenarioEditor) != ItemClass.Availability.None)
		{
			nameOnly = false;
			return NetSegment.Flags.All;
		}
		nameOnly = false;
		return NetSegment.Flags.None;
	}

	public override Building.Flags GetBuildingIgnoreFlags()
	{
		return Building.Flags.None;
	}

	public override TreeInstance.Flags GetTreeIgnoreFlags()
	{
		return TreeInstance.Flags.None;
	}

	public override PropInstance.Flags GetPropIgnoreFlags()
	{
		return PropInstance.Flags.None;
	}

	public override Vehicle.Flags GetVehicleIgnoreFlags()
	{
		return Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding | Vehicle.Flags.LeftHandDrive;
	}

	public override VehicleParked.Flags GetParkedVehicleIgnoreFlags()
	{
		return VehicleParked.Flags.All;
	}

	public override CitizenInstance.Flags GetCitizenIgnoreFlags()
	{
		return CitizenInstance.Flags.All;
	}

	public override TransportLine.Flags GetTransportIgnoreFlags()
	{
		return TransportLine.Flags.All;
	}

	public override District.Flags GetDistrictIgnoreFlags()
	{
		return District.Flags.All;
	}

	public override void SimulationStep()
	{
		base.SimulationStep();
		GenericGuide genericGuide = Singleton<GuideManager>.get_instance().m_bulldozerNotUsed;
		if (genericGuide != null && !genericGuide.m_disabled)
		{
			genericGuide.Disable();
		}
		if (this.m_manualUndergroundMode)
		{
			genericGuide = Singleton<GuideManager>.get_instance().m_undergroundBulldozer;
			if (genericGuide != null && !genericGuide.m_disabled)
			{
				genericGuide.Disable();
			}
		}
		else
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<GuideManager>.get_instance().m_undergroundBulldozer.Activate(properties.m_undergroundBulldozer);
			}
		}
		BulldozeTool.Mode bulldozingMode = this.m_bulldozingMode;
		if (bulldozingMode != BulldozeTool.Mode.NodeOrSegment)
		{
			if (bulldozingMode != BulldozeTool.Mode.Building)
			{
				if (bulldozingMode == BulldozeTool.Mode.PropOrTree)
				{
					if (this.m_lastInstance.Prop != 0)
					{
						if (this.m_hoverInstance.Prop != this.m_lastInstance.Prop || this.m_deleteTimer == 0f)
						{
							this.DeletePropImpl(this.m_lastInstance.Prop);
						}
					}
					else if (this.m_lastInstance.Tree != 0u && (this.m_hoverInstance.Tree != this.m_lastInstance.Tree || this.m_deleteTimer == 0f))
					{
						this.DeleteTreeImpl(this.m_lastInstance.Tree);
					}
				}
			}
			else if (this.m_lastInstance.Building != 0 && (this.m_hoverInstance.Building != this.m_lastInstance.Building || this.m_deleteTimer == 0f) && this.CheckBulldozingClass(Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_lastInstance.Building].Info.m_class))
			{
				this.DeleteBuildingImpl(this.m_lastInstance.Building);
			}
		}
		else if (this.m_lastInstance.NetNode != 0)
		{
			if ((this.m_hoverInstance.NetNode != this.m_lastInstance.NetNode || this.m_deleteTimer == 0f) && this.CheckBulldozingClass(Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_lastInstance.NetNode].Info.m_class))
			{
				this.DeleteNodeImpl(this.m_lastInstance.NetNode);
			}
		}
		else if (this.m_lastInstance.NetSegment != 0 && ((this.m_hoverInstance.NetSegment != this.m_lastInstance.NetSegment && this.m_hoverInstance.NetSegment != this.m_lastInstance2.NetSegment) || this.m_deleteTimer == 0f) && this.CheckBulldozingClass(Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_lastInstance.NetSegment].Info.m_class))
		{
			this.DeleteSegmentImpl(this.m_lastInstance.NetSegment);
			if (this.m_lastInstance2.NetSegment != 0)
			{
				this.DeleteSegmentImpl(this.m_lastInstance2.NetSegment);
			}
		}
		this.m_lastInstance = this.m_hoverInstance;
		this.m_lastInstance2 = this.m_hoverInstance2;
		int num = 0;
		if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			if (this.m_lastInstance.Building != 0)
			{
				num += this.GetBuildingRefundAmount(this.m_lastInstance.Building);
			}
			if (this.m_lastInstance.NetSegment != 0)
			{
				num += this.GetSegmentRefundAmount(this.m_lastInstance.NetSegment);
			}
			if (this.m_lastInstance2.NetSegment != 0)
			{
				num += this.GetSegmentRefundAmount(this.m_lastInstance2.NetSegment);
			}
		}
		this.m_refundAmount = num;
	}

	private bool CheckBulldozingClass(ItemClass klass)
	{
		return klass.m_service == this.m_bulldozingService && (klass.m_layer & this.m_bulldozingLayers) != ItemClass.Layer.None;
	}

	private int GetBuildingRefundAmount(ushort building)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (Singleton<SimulationManager>.get_instance().IsRecentBuildIndex(instance.m_buildings.m_buffer[(int)building].m_buildIndex))
		{
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			return info.m_buildingAI.GetRefundAmount(building, ref instance.m_buildings.m_buffer[(int)building]);
		}
		return 0;
	}

	private int GetSegmentRefundAmount(ushort segment)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if (Singleton<SimulationManager>.get_instance().IsRecentBuildIndex(instance.m_segments.m_buffer[(int)segment].m_buildIndex))
		{
			NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
			ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			float num = (float)instance.m_nodes.m_buffer[(int)startNode].m_elevation;
			float num2 = (float)instance.m_nodes.m_buffer[(int)endNode].m_elevation;
			if (info.m_netAI.IsUnderground())
			{
				num = -num;
				num2 = -num2;
			}
			int constructionCost = info.m_netAI.GetConstructionCost(position, position2, num, num2);
			int result = constructionCost * 3 / 4;
			Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetRefundAmount(constructionCost, ref result, info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			return result;
		}
		return 0;
	}

	protected override bool CheckBuilding(ushort building, ref ToolBase.ToolErrors errors)
	{
		if (base.CheckBuilding(building, ref errors))
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			errors |= info.m_buildingAI.CheckBulldozing(building, ref instance.m_buildings.m_buffer[(int)building]);
			return true;
		}
		return false;
	}

	protected override bool CheckTree(uint tree, ref ToolBase.ToolErrors errors)
	{
		if (base.CheckTree(tree, ref errors))
		{
			if ((Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)tree)].m_flags & 128) != 0)
			{
				errors |= ToolBase.ToolErrors.ObjectOnFire;
			}
			return true;
		}
		return false;
	}

	private BulldozeTool.Mode m_bulldozingMode;

	private ItemClass.Service m_bulldozingService;

	private ItemClass.Layer m_bulldozingLayers;

	private InstanceID m_lastInstance;

	private InstanceID m_lastInstance2;

	private float m_deleteTimer;

	private int m_refundAmount;

	private bool m_manualUndergroundMode;

	private SavedInputKey m_buildElevationUp;

	private SavedInputKey m_buildElevationDown;

	[NonSerialized]
	public NetInfo m_lastNetInfo;

	[NonSerialized]
	public Vector3 m_lastStartPos;

	[NonSerialized]
	public Vector3 m_lastEndPos;

	[NonSerialized]
	public Vector3 m_lastStartDir;

	[NonSerialized]
	public Vector3 m_lastEndDir;

	private UIMultiStateButton m_bulldozerUndergroundToggle;

	public enum Layer
	{
		Default,
		Tunnels,
		Pipes
	}

	public enum Mode
	{
		Select,
		NodeOrSegment,
		Building,
		PropOrTree
	}

	public delegate void BuildingDeleted(ushort building);
}
