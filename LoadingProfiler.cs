﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework.IO;
using UnityEngine;

public class LoadingProfiler
{
	public LoadingProfiler()
	{
		this.m_events = new FastList<LoadingProfiler.Event>();
		this.m_stopwatch = new Stopwatch();
	}

	public void Reset()
	{
		this.m_events.Clear();
		this.m_stopwatch.Reset();
		this.m_stopwatch.Start();
	}

	public void BeginLoading(string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.BeginLoading, name, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void PauseLoading()
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.PauseLoading, null, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void ContinueLoading()
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.ContinueLoading, null, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void EndLoading()
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.EndLoading, null, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void BeginSerialize(DataSerializer s, string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.BeginSerialize, name, this.m_stopwatch.ElapsedMilliseconds));
		s.WriteSharedString(name);
	}

	public void EndSerialize(DataSerializer s, string name)
	{
		s.WriteSharedString(name);
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.EndSerialize, null, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void BeginDeserialize(DataSerializer s, string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.BeginDeserialize, name, this.m_stopwatch.ElapsedMilliseconds));
		if (s.get_version() >= 130u)
		{
			string text = s.ReadSharedString();
			if (text != name)
			{
				throw new Exception("BeginDeserialize(" + name + ") tag mismatch: " + text);
			}
		}
	}

	public void EndDeserialize(DataSerializer s, string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.EndDeserialize, null, this.m_stopwatch.ElapsedMilliseconds));
		if (s.get_version() >= 130u)
		{
			string text = s.ReadSharedString();
			if ((s.get_version() >= 154u || text != null) && text != name)
			{
				throw new Exception("EndDeserialize(" + name + ") tag mismatch: " + text);
			}
		}
	}

	public void BeginDeserialize(DataSerializer s, string name1, string name2)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.BeginDeserialize, name1, this.m_stopwatch.ElapsedMilliseconds));
		if (s.get_version() >= 130u)
		{
			string text = s.ReadSharedString();
			if (text != name1 && text != name2)
			{
				throw new Exception("BeginDeserialize(" + name1 + ") tag mismatch: " + text);
			}
		}
	}

	public void EndDeserialize(DataSerializer s, string name1, string name2)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.EndDeserialize, null, this.m_stopwatch.ElapsedMilliseconds));
		if (s.get_version() >= 130u)
		{
			string text = s.ReadSharedString();
			if ((s.get_version() >= 154u || text != null) && text != name1 && text != name2)
			{
				throw new Exception("EndDeserialize(" + name1 + ") tag mismatch: " + text);
			}
		}
	}

	public void BeginAfterDeserialize(DataSerializer s, string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.BeginAfterDeserialize, name, this.m_stopwatch.ElapsedMilliseconds));
	}

	public void EndAfterDeserialize(DataSerializer s, string name)
	{
		this.m_events.Add(new LoadingProfiler.Event(LoadingProfiler.Type.EndAfterDeserialize, null, this.m_stopwatch.ElapsedMilliseconds));
	}

	public long GetMaxTime()
	{
		int size = this.m_events.m_size;
		if (size == 0)
		{
			return 0L;
		}
		return this.m_events.m_buffer[size - 1].m_time;
	}

	public string DrawGUI(Rect rect, long maxTime)
	{
		if (LoadingProfiler._staticRectTexture1 == null)
		{
			LoadingProfiler._staticRectTexture1 = new Texture2D(1, 1);
			LoadingProfiler._staticRectTexture1.SetPixel(0, 0, Color.get_white());
			LoadingProfiler._staticRectTexture1.Apply();
		}
		if (LoadingProfiler._staticRectStyle1 == null)
		{
			LoadingProfiler._staticRectStyle1 = new GUIStyle();
			LoadingProfiler._staticRectStyle1.get_normal().set_background(LoadingProfiler._staticRectTexture1);
		}
		if (LoadingProfiler._staticRectTexture2 == null)
		{
			LoadingProfiler._staticRectTexture2 = new Texture2D(1, 1);
			LoadingProfiler._staticRectTexture2.SetPixel(0, 0, Color.get_yellow());
			LoadingProfiler._staticRectTexture2.Apply();
		}
		if (LoadingProfiler._staticRectStyle2 == null)
		{
			LoadingProfiler._staticRectStyle2 = new GUIStyle();
			LoadingProfiler._staticRectStyle2.get_normal().set_background(LoadingProfiler._staticRectTexture2);
		}
		if (LoadingProfiler._staticRectTexture3 == null)
		{
			LoadingProfiler._staticRectTexture3 = new Texture2D(1, 1);
			LoadingProfiler._staticRectTexture3.SetPixel(0, 0, Color.get_red());
			LoadingProfiler._staticRectTexture3.Apply();
		}
		if (LoadingProfiler._staticRectStyle3 == null)
		{
			LoadingProfiler._staticRectStyle3 = new GUIStyle();
			LoadingProfiler._staticRectStyle3.get_normal().set_background(LoadingProfiler._staticRectTexture3);
		}
		int num = 0;
		long num2 = 0L;
		bool flag = false;
		string text = null;
		string text2 = null;
		int size = this.m_events.m_size;
		Vector3 mousePosition = Input.get_mousePosition();
		mousePosition.y = (float)Screen.get_height() - mousePosition.y;
		string result = null;
		for (int i = 0; i < size; i++)
		{
			string name = this.m_events.m_buffer[i].m_name;
			LoadingProfiler.Type type = this.m_events.m_buffer[i].m_type;
			long time = this.m_events.m_buffer[i].m_time;
			int num3 = Mathf.RoundToInt(rect.get_width() * (float)time / (float)maxTime);
			if (num3 > num)
			{
				if (mousePosition.y >= rect.get_y() && mousePosition.y < rect.get_y() + rect.get_height() && mousePosition.x >= rect.get_x() + (float)num && mousePosition.x < rect.get_x() + (float)num3)
				{
					if (text2 != null && text != null)
					{
						result = StringUtils.SafeFormat("{0}: {1}\n({2} ms)", new object[]
						{
							text,
							text2,
							time - num2
						});
					}
					else if (text2 != null)
					{
						result = StringUtils.SafeFormat("{0}\n({1} ms)", new object[]
						{
							text2,
							time - num2
						});
					}
					else if (text != null)
					{
						result = StringUtils.SafeFormat("{0}\n({1} ms)", new object[]
						{
							text,
							time - num2
						});
					}
				}
				Rect rect2;
				rect2..ctor(rect.get_x() + (float)num, rect.get_y(), 1f, rect.get_height());
				GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle1);
				num++;
				if (num3 > num)
				{
					if (text2 != null || text != null)
					{
						rect2..ctor(rect.get_x() + (float)num, rect.get_y(), (float)(num3 - num), rect.get_height());
						if (flag)
						{
							GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle2);
						}
						else
						{
							GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle3);
						}
					}
					num = num3;
				}
			}
			num2 = time;
			switch (type)
			{
			case LoadingProfiler.Type.BeginLoading:
				text = name;
				break;
			case LoadingProfiler.Type.PauseLoading:
				flag = true;
				break;
			case LoadingProfiler.Type.ContinueLoading:
				flag = false;
				break;
			case LoadingProfiler.Type.EndLoading:
				text = null;
				break;
			case LoadingProfiler.Type.BeginSerialize:
			case LoadingProfiler.Type.BeginDeserialize:
			case LoadingProfiler.Type.BeginAfterDeserialize:
				text2 = name;
				break;
			case LoadingProfiler.Type.EndSerialize:
			case LoadingProfiler.Type.EndDeserialize:
			case LoadingProfiler.Type.EndAfterDeserialize:
				text2 = null;
				break;
			}
		}
		return result;
	}

	public string DrawColoredGUI(Rect rect, long maxTime)
	{
		if (LoadingProfiler._staticRectTexture1 == null)
		{
			LoadingProfiler._staticRectTexture1 = new Texture2D(1, 1);
			LoadingProfiler._staticRectTexture1.SetPixel(0, 0, Color.get_white());
			LoadingProfiler._staticRectTexture1.Apply();
		}
		if (LoadingProfiler._staticRectStyle1 == null)
		{
			LoadingProfiler._staticRectStyle1 = new GUIStyle();
			LoadingProfiler._staticRectStyle1.get_normal().set_background(LoadingProfiler._staticRectTexture1);
		}
		int num = 0;
		long num2 = 0L;
		bool flag = false;
		string text = null;
		string text2 = null;
		int size = this.m_events.m_size;
		Vector3 mousePosition = Input.get_mousePosition();
		mousePosition.y = (float)Screen.get_height() - mousePosition.y;
		string result = null;
		int num3 = size / 2;
		List<long> list = new List<long>(num3);
		for (int i = 0; i < num3; i += 2)
		{
			if (this.m_events.m_buffer[i].m_type == LoadingProfiler.Type.BeginLoading)
			{
				list.Add(this.m_events.m_buffer[i + 1].m_time - this.m_events.m_buffer[i].m_time);
			}
		}
		long num4 = (list.Count <= 0) ? 0L : list[list.Count / 2];
		for (int j = 0; j < size; j++)
		{
			string name = this.m_events.m_buffer[j].m_name;
			LoadingProfiler.Type type = this.m_events.m_buffer[j].m_type;
			long time = this.m_events.m_buffer[j].m_time;
			int num5 = Mathf.RoundToInt(rect.get_width() * (float)time / (float)maxTime);
			if (num5 > num)
			{
				if (mousePosition.y >= rect.get_y() && mousePosition.y < rect.get_y() + rect.get_height() && mousePosition.x >= rect.get_x() + (float)num && mousePosition.x < rect.get_x() + (float)num5)
				{
					if (text2 != null && text != null)
					{
						result = StringUtils.SafeFormat("{0}: {1}\n({2} ms)", new object[]
						{
							text,
							text2,
							time - num2
						});
					}
					else if (text2 != null)
					{
						result = StringUtils.SafeFormat("{0}\n({1} ms)", new object[]
						{
							text2,
							time - num2
						});
					}
					else if (text != null)
					{
						result = StringUtils.SafeFormat("{0}\n({1} ms)", new object[]
						{
							text,
							time - num2
						});
					}
				}
				Rect rect2;
				rect2..ctor(rect.get_x() + (float)num, rect.get_y(), 1f, rect.get_height());
				GUI.set_color(Color.get_white());
				GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle1);
				num++;
				if (num5 > num)
				{
					if (text2 != null || text != null)
					{
						rect2..ctor(rect.get_x() + (float)num, rect.get_y(), (float)(num5 - num), rect.get_height());
						if (flag)
						{
							GUI.set_color(Color.get_gray());
							GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle1);
						}
						else
						{
							float num6 = (float)Math.Max(time - num2 - num4, 0L) / (float)num4;
							GUI.set_color(Color.Lerp(Color.get_green(), Color.get_red(), num6));
							GUI.Box(rect2, GUIContent.none, LoadingProfiler._staticRectStyle1);
						}
					}
					num = num5;
				}
			}
			num2 = time;
			switch (type)
			{
			case LoadingProfiler.Type.BeginLoading:
				text = name;
				break;
			case LoadingProfiler.Type.PauseLoading:
				flag = true;
				break;
			case LoadingProfiler.Type.ContinueLoading:
				flag = false;
				break;
			case LoadingProfiler.Type.EndLoading:
				text = null;
				break;
			case LoadingProfiler.Type.BeginSerialize:
			case LoadingProfiler.Type.BeginDeserialize:
			case LoadingProfiler.Type.BeginAfterDeserialize:
				text2 = name;
				break;
			case LoadingProfiler.Type.EndSerialize:
			case LoadingProfiler.Type.EndDeserialize:
			case LoadingProfiler.Type.EndAfterDeserialize:
				text2 = null;
				break;
			}
		}
		return result;
	}

	[NonSerialized]
	private FastList<LoadingProfiler.Event> m_events;

	[NonSerialized]
	private Stopwatch m_stopwatch;

	private static Texture2D _staticRectTexture1;

	private static Texture2D _staticRectTexture2;

	private static Texture2D _staticRectTexture3;

	private static GUIStyle _staticRectStyle1;

	private static GUIStyle _staticRectStyle2;

	private static GUIStyle _staticRectStyle3;

	public enum Type
	{
		BeginLoading,
		PauseLoading,
		ContinueLoading,
		EndLoading,
		BeginSerialize,
		EndSerialize,
		BeginDeserialize,
		EndDeserialize,
		BeginAfterDeserialize,
		EndAfterDeserialize
	}

	public struct Event
	{
		public Event(LoadingProfiler.Type type, string name, long time)
		{
			this.m_type = type;
			this.m_name = name;
			this.m_time = time;
		}

		public LoadingProfiler.Type m_type;

		public string m_name;

		public long m_time;
	}
}
