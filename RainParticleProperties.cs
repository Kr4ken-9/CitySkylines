﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class RainParticleProperties : MonoBehaviour
{
	private void OnEnable()
	{
		RainParticleProperties.ID_RainIntensity = Shader.PropertyToID("_RainIntensity");
		RainParticleProperties.ID_RainGridPosition = Shader.PropertyToID("_RainGridPosition");
		RainParticleProperties.ID_CameraForward = Shader.PropertyToID("_CameraForward");
		RainParticleProperties.ID_RainLightColor = Shader.PropertyToID("_RainLightColor");
		RainParticleProperties.ID_SimSpeed = Shader.PropertyToID("_SimSpeed");
		this.m_DayNightProperties = base.GetComponent<DayNightProperties>();
		if (this.m_RainMaterialX != null)
		{
			this.m_RainMaterialX.EnableKeyword("RAINPARTICLE_X");
			this.m_RainMaterialX.DisableKeyword("RAINPARTICLE_Y");
			this.m_RainMaterialX.DisableKeyword("RAINPARTICLE_Z");
		}
		if (this.m_RainMaterialY != null)
		{
			this.m_RainMaterialY.EnableKeyword("RAINPARTICLE_Y");
			this.m_RainMaterialY.DisableKeyword("RAINPARTICLE_X");
			this.m_RainMaterialY.DisableKeyword("RAINPARTICLE_Z");
		}
		if (this.m_RainMaterialZ != null)
		{
			this.m_RainMaterialZ.EnableKeyword("RAINPARTICLE_Z");
			this.m_RainMaterialZ.DisableKeyword("RAINPARTICLE_X");
			this.m_RainMaterialZ.DisableKeyword("RAINPARTICLE_Y");
		}
		if (Singleton<SimulationManager>.get_exists())
		{
			if (Singleton<SimulationManager>.get_instance().m_metaData != null)
			{
				this.m_IsWinter = (Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Winter");
			}
			if (this.m_IsWinter)
			{
				this.m_RainMaterialX.EnableKeyword("SNOWPARTICLE");
				this.m_RainMaterialX.DisableKeyword("RAINPARTICLE");
				this.m_RainMaterialY.EnableKeyword("SNOWPARTICLE");
				this.m_RainMaterialY.DisableKeyword("RAINPARTICLE");
				this.m_RainMaterialZ.EnableKeyword("SNOWPARTICLE");
				this.m_RainMaterialZ.DisableKeyword("RAINPARTICLE");
			}
			else
			{
				this.m_RainMaterialX.EnableKeyword("RAINPARTICLE");
				this.m_RainMaterialX.DisableKeyword("SNOWPARTICLE");
				this.m_RainMaterialY.EnableKeyword("RAINPARTICLE");
				this.m_RainMaterialY.DisableKeyword("SNOWPARTICLE");
				this.m_RainMaterialZ.EnableKeyword("RAINPARTICLE");
				this.m_RainMaterialZ.DisableKeyword("SNOWPARTICLE");
			}
		}
		if (this.m_IsWinter)
		{
			this.CreateNoiseSum();
		}
		this.InitMesh();
	}

	private float colorsum(float orig, float dir, float normalizer)
	{
		return orig + dir / normalizer;
	}

	private void CreateNoiseSum()
	{
		if (this.m_RainMaterialX == null || this.m_RainMaterialY == null || this.m_RainMaterialZ == null)
		{
			return;
		}
		Texture2D texture2D = this.m_RainMaterialX.GetTexture("_RainNoise") as Texture2D;
		Color[] pixels = texture2D.GetPixels();
		Color[] array = new Color[texture2D.get_width() * texture2D.get_height()];
		float[] array2 = new float[texture2D.get_width()];
		for (int i = 0; i < texture2D.get_height(); i++)
		{
			for (int j = 0; j < texture2D.get_width(); j++)
			{
				array2[j] += pixels[j + i * texture2D.get_width()].r;
			}
		}
		for (int k = 0; k < texture2D.get_height(); k++)
		{
			for (int l = 0; l < texture2D.get_width(); l++)
			{
				int num = l + texture2D.get_width() * k;
				if (k == 0)
				{
					array[num] = new Color(0f, 0f, 0f, 0f);
				}
				else
				{
					Color color = array[num - texture2D.get_width()];
					Color color2 = pixels[num];
					array[num] = new Color(this.colorsum(color.r, color2.r, array2[l]), this.colorsum(color.g, color2.g, array2[l]), this.colorsum(color.b, color2.b, array2[l]), this.colorsum(color.a, color2.a, array2[l]));
				}
			}
		}
		this.m_SumNoiseTexture = new Texture2D(texture2D.get_width(), texture2D.get_height(), 5, true, true);
		this.m_SumNoiseTexture.SetPixels(array);
		this.m_SumNoiseTexture.Apply(true, true);
		this.m_RainMaterialX.SetTexture("_RainNoiseSum", this.m_SumNoiseTexture);
		this.m_RainMaterialY.SetTexture("_RainNoiseSum", this.m_SumNoiseTexture);
		this.m_RainMaterialZ.SetTexture("_RainNoiseSum", this.m_SumNoiseTexture);
	}

	private void OnDisable()
	{
		if (this.m_RainParticleMesh)
		{
			Object.DestroyImmediate(this.m_RainParticleMesh);
			this.m_RainParticleMesh = null;
		}
		if (this.m_SumNoiseTexture)
		{
			Object.DestroyImmediate(this.m_SumNoiseTexture);
			this.m_SumNoiseTexture = null;
		}
	}

	private void InitMesh()
	{
		Mesh mesh = new Mesh();
		int num = 15;
		int num2 = 15;
		this.m_GridSpacing = 2f;
		int num3 = Mathf.RoundToInt(this.m_GridSpacing);
		int num4 = 10;
		int num5 = (2 * num / num3 + 1) * (2 * num2 / num3 + 1) * 3 * num4;
		Vector3[] array = new Vector3[num5];
		int[] array2 = new int[num5];
		Vector2[] array3 = new Vector2[num5];
		int num6 = 0;
		for (float num7 = (float)(-(float)num); num7 < (float)num + 0.1f; num7 += (float)num3)
		{
			for (float num8 = (float)(-(float)num2); num8 < (float)num2 + 0.1f; num8 += (float)num3)
			{
				for (float num9 = 0.15f; num9 < (float)num4; num9 += 0.333333343f)
				{
					int num10 = num6 % 3;
					array[num6] = new Vector3(num7, num9, num8);
					if (num10 == 0)
					{
						array3[num6] = new Vector2(0f, 0f);
					}
					else if (num10 == 1)
					{
						array3[num6] = new Vector2(1f, 0f);
					}
					else if (num10 == 2)
					{
						array3[num6] = new Vector2(0f, 1f);
					}
					array2[num6] = num6;
					num6++;
				}
			}
		}
		mesh.set_vertices(array);
		mesh.set_triangles(array2);
		mesh.set_uv(array3);
		mesh.UploadMeshData(true);
		this.m_RainParticleMesh = mesh;
	}

	private Camera GetCamera()
	{
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			return gameObject.GetComponent<Camera>();
		}
		return null;
	}

	private Vector3 cameraPosition
	{
		get
		{
			if (this.m_Camera == null)
			{
				this.m_Camera = this.GetCamera();
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_transform().get_position();
			}
			return default(Vector3);
		}
	}

	private float cameraNearClipPlane
	{
		get
		{
			if (this.m_Camera == null)
			{
				this.m_Camera = this.GetCamera();
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_nearClipPlane();
			}
			return 0f;
		}
	}

	private Vector3 cameraForward
	{
		get
		{
			if (this.m_Camera == null && this.m_Camera == null)
			{
				this.m_Camera = this.GetCamera();
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_transform().get_forward();
			}
			return Vector3.get_forward();
		}
	}

	private void LateUpdate()
	{
		if (Singleton<WeatherManager>.get_exists())
		{
			this.m_RainControl = Singleton<WeatherManager>.get_instance().m_currentRain;
		}
		if (this.m_RainControl > 0f)
		{
			Color currentLightColor;
			if (this.m_DayNightProperties != null)
			{
				currentLightColor = this.m_DayNightProperties.currentLightColor;
				currentLightColor.r = Mathf.Sqrt(currentLightColor.r);
				currentLightColor.g = Mathf.Sqrt(currentLightColor.g);
				currentLightColor.b = Mathf.Sqrt(currentLightColor.b);
				currentLightColor.a = 1f;
			}
			else
			{
				currentLightColor..ctor(1f, 1f, 1f, 1f);
			}
			if (!this.ForceRainMotionBlur && Singleton<SimulationManager>.get_exists())
			{
				this.m_RainMaterialX.SetFloat(RainParticleProperties.ID_SimSpeed, Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
				this.m_RainMaterialY.SetFloat(RainParticleProperties.ID_SimSpeed, Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
				this.m_RainMaterialZ.SetFloat(RainParticleProperties.ID_SimSpeed, Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
			}
			else
			{
				this.m_RainMaterialX.SetFloat(RainParticleProperties.ID_SimSpeed, 1f);
				this.m_RainMaterialY.SetFloat(RainParticleProperties.ID_SimSpeed, 1f);
				this.m_RainMaterialZ.SetFloat(RainParticleProperties.ID_SimSpeed, 1f);
			}
			this.m_RainMaterialX.SetColor(RainParticleProperties.ID_RainLightColor, currentLightColor);
			this.m_RainMaterialY.SetColor(RainParticleProperties.ID_RainLightColor, currentLightColor);
			this.m_RainMaterialZ.SetColor(RainParticleProperties.ID_RainLightColor, currentLightColor);
			this.m_RainMaterialX.SetFloat(RainParticleProperties.ID_RainIntensity, Mathf.Lerp(0f, this.m_MaxRainIntensity, this.m_RainControl));
			this.m_RainMaterialY.SetFloat(RainParticleProperties.ID_RainIntensity, Mathf.Lerp(0f, this.m_MaxRainIntensity, this.m_RainControl));
			this.m_RainMaterialZ.SetFloat(RainParticleProperties.ID_RainIntensity, Mathf.Lerp(0f, this.m_MaxRainIntensity, this.m_RainControl));
			this.m_RainMaterialX.SetVector(RainParticleProperties.ID_CameraForward, this.cameraForward);
			this.m_RainMaterialY.SetVector(RainParticleProperties.ID_CameraForward, this.cameraForward);
			this.m_RainMaterialZ.SetVector(RainParticleProperties.ID_CameraForward, this.cameraForward);
			Vector3 vector = this.cameraPosition + (8f + this.cameraNearClipPlane) * this.cameraForward;
			Vector3 vector2;
			vector2..ctor(this.m_GridSpacing * Mathf.Round(vector.x / this.m_GridSpacing), Mathf.Floor(vector.y), this.m_GridSpacing * Mathf.Round(vector.z / this.m_GridSpacing));
			this.m_RainMaterialX.SetVector(RainParticleProperties.ID_RainGridPosition, vector2);
			this.m_RainMaterialY.SetVector(RainParticleProperties.ID_RainGridPosition, vector2);
			this.m_RainMaterialZ.SetVector(RainParticleProperties.ID_RainGridPosition, vector2);
			Graphics.DrawMesh(this.m_RainParticleMesh, vector2, Quaternion.get_identity(), this.m_RainMaterialX, this.m_RainLayer);
			Graphics.DrawMesh(this.m_RainParticleMesh, vector2, Quaternion.get_identity(), this.m_RainMaterialY, this.m_RainLayer);
			Graphics.DrawMesh(this.m_RainParticleMesh, vector2, Quaternion.get_identity(), this.m_RainMaterialZ, this.m_RainLayer);
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.set_color(Color.get_red());
		Gizmos.DrawLine(new Vector3(-RainParticleProperties.kTerrainRange, 0f, -RainParticleProperties.kTerrainRange), new Vector3(-RainParticleProperties.kTerrainRange, 0f, RainParticleProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(-RainParticleProperties.kTerrainRange, 0f, RainParticleProperties.kTerrainRange), new Vector3(RainParticleProperties.kTerrainRange, 0f, RainParticleProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(RainParticleProperties.kTerrainRange, 0f, RainParticleProperties.kTerrainRange), new Vector3(RainParticleProperties.kTerrainRange, 0f, -RainParticleProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(RainParticleProperties.kTerrainRange, 0f, -RainParticleProperties.kTerrainRange), new Vector3(-RainParticleProperties.kTerrainRange, 0f, -RainParticleProperties.kTerrainRange));
	}

	private Mesh m_RainParticleMesh;

	[Range(0f, 1f)]
	public float m_RainControl;

	[Header("Rain Objects")]
	public Material m_RainMaterialX;

	public Material m_RainMaterialY;

	public Material m_RainMaterialZ;

	public int m_RainLayer = 29;

	[Header("Rain Settings"), Range(0f, 1f)]
	public float m_MaxRainIntensity = 0.6f;

	[Header("Debugging")]
	public bool ForceRainMotionBlur;

	private static float kTerrainRange = 8640f;

	private static int ID_RainIntensity;

	private static int ID_RainGridPosition;

	private static int ID_CameraForward;

	private static int ID_RainLightColor;

	private static int ID_SimSpeed;

	private bool m_IsWinter;

	private float m_GridSpacing;

	private DayNightProperties m_DayNightProperties;

	private Texture2D m_SumNoiseTexture;

	private Camera m_Camera;
}
