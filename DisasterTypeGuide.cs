﻿using System;

public class DisasterTypeGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, DisasterInfo disasterInfo)
	{
		if (base.CanActivate(guideInfo) && disasterInfo.m_prefabDataIndex != -1)
		{
			GuideTriggerBase.ActivationInfo activationInfo = new DisasterTypeGuide.DisasterTypeActivationInfo(guideInfo, disasterInfo);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class DisasterTypeActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public DisasterTypeActivationInfo(GuideInfo guideInfo, DisasterInfo disasterInfo) : base(guideInfo)
		{
			this.m_disasterInfo = disasterInfo;
		}

		public override string GetName()
		{
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<DisasterInfo>.PrefabName((uint)this.m_disasterInfo.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			string icon = this.m_guideInfo.m_icon;
			string icon2 = this.m_disasterInfo.m_Icon;
			return StringUtils.SafeFormat(icon, icon2);
		}

		public override string FormatText(string pattern)
		{
			return pattern;
		}

		public override IUITag GetTag()
		{
			if (string.IsNullOrEmpty(this.m_guideInfo.m_tag))
			{
				return new GenericTutorialTag();
			}
			return MonoTutorialTag.Find(this.m_guideInfo.m_tag);
		}

		protected DisasterInfo m_disasterInfo;
	}
}
