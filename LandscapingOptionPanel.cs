﻿using System;
using ColossalFramework.UI;

public class LandscapingOptionPanel : OptionPanelBase
{
	private LandscapingOptionPanel.BrushInfo GetBrushInfo(TerrainTool terrainTool)
	{
		switch (terrainTool.m_mode)
		{
		case TerrainTool.Mode.Shift:
			return this.m_ShiftBrushInfo;
		case TerrainTool.Mode.Level:
			return this.m_LevelBrushInfo;
		case TerrainTool.Mode.Soften:
			return this.m_SoftenBrushInfo;
		case TerrainTool.Mode.Slope:
			return this.m_SlopeBrushInfo;
		default:
			return null;
		}
	}

	private void Awake()
	{
		this.Hide();
		TerrainTool terrainTool = ToolsModifierControl.GetTool<TerrainTool>();
		if (terrainTool != null)
		{
			this.m_SizeTabStrip = base.Find<UITabstrip>("BrushSizeStrip");
			this.m_BrushStrengthButton = base.Find<UIMultiStateButton>("BrushStrength");
			if (this.m_BrushStrengthButton != null)
			{
				this.m_BrushStrengthButton.set_activeStateIndex(0);
				this.m_BrushStrengthButton.add_eventActiveStateIndexChanged(delegate(UIComponent component, int index)
				{
					this.UpdateTerrainToolSettings(terrainTool);
				});
			}
			if (this.m_SizeTabStrip != null)
			{
				this.m_SizeTabStrip.set_selectedIndex(0);
				this.m_SizeTabStrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int index)
				{
					this.UpdateTerrainToolSettings(terrainTool);
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						this.UpdateTerrainToolSettings(terrainTool);
					}
				});
			}
		}
	}

	private void UpdateTerrainToolSettings(TerrainTool terrainTool)
	{
		int activeStateIndex = this.m_BrushStrengthButton.get_activeStateIndex();
		int selectedIndex = this.m_SizeTabStrip.get_selectedIndex();
		if (selectedIndex != 0)
		{
			if (selectedIndex != 1)
			{
				if (selectedIndex == 2)
				{
					this.ApplyTerrainToolSettingsLarge(terrainTool, activeStateIndex);
				}
			}
			else
			{
				this.ApplyTerrainToolSettingsMedium(terrainTool, activeStateIndex);
			}
		}
		else
		{
			this.ApplyTerrainToolSettingsSmall(terrainTool, activeStateIndex);
		}
	}

	protected override void Refresh()
	{
		base.Refresh();
		TerrainTool tool = ToolsModifierControl.GetTool<TerrainTool>();
		if (tool != null)
		{
			this.UpdateTerrainToolSettings(tool);
		}
	}

	private void ApplyTerrainToolSettingsSmall(TerrainTool terrainTool, int strengthIndex)
	{
		LandscapingOptionPanel.BrushInfo brushInfo = this.GetBrushInfo(terrainTool);
		terrainTool.m_brushSize = brushInfo.m_BrushSizeSmall;
		terrainTool.m_strength = brushInfo.m_BrushStrengthSmall[strengthIndex];
	}

	private void ApplyTerrainToolSettingsMedium(TerrainTool terrainTool, int strengthIndex)
	{
		LandscapingOptionPanel.BrushInfo brushInfo = this.GetBrushInfo(terrainTool);
		terrainTool.m_brushSize = brushInfo.m_BrushSizeMedium;
		terrainTool.m_strength = brushInfo.m_BrushStrengthMedium[strengthIndex];
	}

	private void ApplyTerrainToolSettingsLarge(TerrainTool terrainTool, int strengthIndex)
	{
		LandscapingOptionPanel.BrushInfo brushInfo = this.GetBrushInfo(terrainTool);
		terrainTool.m_brushSize = brushInfo.m_BrushSizeLarge;
		terrainTool.m_strength = brushInfo.m_BrushStrengthLarge[strengthIndex];
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	public LandscapingOptionPanel.BrushInfo m_ShiftBrushInfo;

	public LandscapingOptionPanel.BrushInfo m_LevelBrushInfo;

	public LandscapingOptionPanel.BrushInfo m_SoftenBrushInfo;

	public LandscapingOptionPanel.BrushInfo m_SlopeBrushInfo;

	private UIMultiStateButton m_BrushStrengthButton;

	private UITabstrip m_SizeTabStrip;

	[Serializable]
	public class BrushInfo
	{
		public float m_BrushSizeSmall = 50f;

		public float m_BrushSizeMedium = 100f;

		public float m_BrushSizeLarge = 200f;

		public float[] m_BrushStrengthSmall = new float[]
		{
			0.005f,
			0.01f,
			0.02f
		};

		public float[] m_BrushStrengthMedium = new float[]
		{
			0.005f,
			0.01f,
			0.02f
		};

		public float[] m_BrushStrengthLarge = new float[]
		{
			0.005f,
			0.01f,
			0.02f
		};
	}
}
