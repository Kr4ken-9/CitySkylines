﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PassengerBlimpAI : BlimpAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.None || infoMode == InfoManager.InfoMode.Transport)
		{
			ushort transportLine = data.m_transportLine;
			if (transportLine != 0)
			{
				return Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].GetColor();
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(vehicleID, ref data, infoMode);
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_BLIMP_STOPPED");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_BLIMP_RETURN");
		}
		if (data.m_transportLine != 0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_BLIMP_ROUTE");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_BLIMP_STOPPED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_passengerCapacity;
	}

	public override bool GetProgressStatus(ushort vehicleID, ref Vehicle data, out float current, out float max)
	{
		ushort transportLine = data.m_transportLine;
		ushort targetBuilding = data.m_targetBuilding;
		if (transportLine != 0 && targetBuilding != 0)
		{
			float num;
			float max2;
			float num2;
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].GetStopProgress(targetBuilding, out num, out max2, out num2);
			uint path = data.m_path;
			bool result;
			if (path == 0u || (data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
			{
				current = num;
				result = false;
			}
			else
			{
				current = PassengerBlimpAI.GetPathProgress(path, (int)data.m_pathPositionIndex, num, max2, out result);
			}
			max = num2;
			return result;
		}
		current = 0f;
		max = 0f;
		return true;
	}

	public static float GetPathProgress(uint path, int pathPos, float min, float max, out bool valid)
	{
		valid = false;
		if (pathPos == 255)
		{
			pathPos = 0;
		}
		else
		{
			pathPos >>= 1;
		}
		PathManager instance = Singleton<PathManager>.get_instance();
		if ((instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_pathFindFlags & 4) == 0)
		{
			return min;
		}
		float length = instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_length;
		uint nextPathUnit = instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_nextPathUnit;
		float num = 0f;
		if (nextPathUnit != 0u)
		{
			num = instance.m_pathUnits.m_buffer[(int)((UIntPtr)nextPathUnit)].m_length;
		}
		int positionCount = (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_positionCount;
		if (positionCount == 0)
		{
			return min;
		}
		float num2 = length + (num - length) * (float)pathPos / (float)positionCount;
		valid = true;
		return Mathf.Clamp(max - num2, min, max);
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_passengerCapacity, 0);
		this.RandomizeBillboard(vehicleID, ref data);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveSource(vehicleID, ref data);
		this.RemoveLine(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].AddVehicle(vehicleID, ref data, false);
		}
		if ((data.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2)) == (Vehicle.Flags)0)
		{
			this.RandomizeBillboard(vehicleID, ref data);
		}
	}

	private void RandomizeBillboard(ushort vehicleID, ref Vehicle data)
	{
		Vehicle.Flags flags = data.m_flags;
		int num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u);
		if (num != 0)
		{
			if (num != 1)
			{
				if (num == 2)
				{
					flags |= (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2);
				}
			}
			else
			{
				flags = ((flags & ~Vehicle.Flags.Emergency1) | Vehicle.Flags.Emergency2);
			}
		}
		else
		{
			flags = ((flags & ~Vehicle.Flags.Emergency2) | Vehicle.Flags.Emergency1);
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(data.GetLastFramePosition());
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		if ((servicePolicies & DistrictPolicies.Services.EducationalBlimps) != DistrictPolicies.Services.None)
		{
			flags |= Vehicle.Flags.WaitingCargo;
		}
		else
		{
			flags &= ~Vehicle.Flags.WaitingCargo;
		}
		data.m_flags = flags;
	}

	protected override void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingAI.PathFindType pathFindType = ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingTransport : BuildingAI.PathFindType.EnteringTransport;
			if (pathFindType == BuildingAI.PathFindType.EnteringTransport || (data.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.PathfindSuccess(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], pathFindType);
			}
		}
		base.PathfindSuccess(vehicleID, ref data);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingAI.PathFindType pathFindType = ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingTransport : BuildingAI.PathFindType.EnteringTransport;
			if (pathFindType == BuildingAI.PathFindType.EnteringTransport || (data.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.PathfindFailure(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], pathFindType);
			}
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			data.Unspawn(vehicleID);
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector2;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = data.m_targetPos0;
			data.m_targetPos2 = data.m_targetPos0;
			data.m_targetPos3 = data.m_targetPos0;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		data.m_targetBuilding = targetBuilding;
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && building == data.m_sourceBuilding)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void SetTransportLine(ushort vehicleID, ref Vehicle data, ushort transportLine)
	{
		this.RemoveLine(vehicleID, ref data);
		data.m_transportLine = transportLine;
		if (transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].AddVehicle(vehicleID, ref data, true);
		}
		else
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == (TransferManager.TransferReason)data.m_transferType)
		{
			ushort transportLine = offer.TransportLine;
			this.SetTransportLine(vehicleID, ref data, transportLine);
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, reason, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = this.m_passengerCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			vehicleData.m_waitCounter += 1;
			if (this.CanLeave(vehicleID, ref vehicleData))
			{
				vehicleData.m_flags &= ~(Vehicle.Flags.Stopped | Vehicle.Flags.Landing);
				vehicleData.m_flags |= Vehicle.Flags.Leaving;
				vehicleData.m_waitCounter = 0;
				if (vehicleData.m_transportLine != 0)
				{
					Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)vehicleData.m_transportLine].LeaveStop(vehicleData.m_targetBuilding);
				}
				this.RandomizeBillboard(vehicleID, ref vehicleData);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTransportLine(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveLine(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].RemoveVehicle(vehicleID, ref data);
			data.m_transportLine = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		ushort num = 0;
		if (data.m_transportLine != 0)
		{
			num = TransportLine.GetNextStop(data.m_targetBuilding);
		}
		ushort targetBuilding = data.m_targetBuilding;
		this.UnloadPassengers(vehicleID, ref data, targetBuilding, num);
		if (num == 0)
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				return true;
			}
			data.m_flags &= ~Vehicle.Flags.Arriving;
			data.m_flags |= Vehicle.Flags.Stopped;
			data.m_waitCounter = 0;
		}
		else
		{
			data.m_targetBuilding = num;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				return true;
			}
			this.LoadPassengers(vehicleID, ref data, targetBuilding, num);
			data.m_flags &= ~Vehicle.Flags.Arriving;
			data.m_flags |= Vehicle.Flags.Stopped;
			data.m_waitCounter = 0;
		}
		return false;
	}

	private void UnloadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		TransportManager instance2 = Singleton<TransportManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 targetPos = Vector3.get_zero();
		if (nextStop != 0)
		{
			targetPos = instance.m_nodes.m_buffer[(int)nextStop].m_position;
		}
		int num = 0;
		if (data.m_transportLine != 0)
		{
			BusAI.TransportArriveAtTarget(vehicleID, ref data, position, targetPos, ref num, ref instance2.m_lines.m_buffer[(int)data.m_transportLine].m_passengers, nextStop == 0);
		}
		else
		{
			BusAI.TransportArriveAtTarget(vehicleID, ref data, position, targetPos, ref num, ref instance2.m_passengers[(int)this.m_transportInfo.m_transportType], nextStop == 0);
		}
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticArray>(StatisticType.PassengerCount);
		statisticBase.Acquire<StatisticInt32>((int)this.m_transportInfo.m_transportType, 10).Add(num);
		num += (int)instance.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		instance.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num, 65535);
	}

	private void LoadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0 || nextStop == 0)
		{
			return;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector3 position = instance2.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		instance2.m_nodes.m_buffer[(int)currentStop].m_maxWaitTime = 0;
		int num = Mathf.Max((int)((position.x - 64f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - 64f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + 64f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + 64f) / 8f + 1080f), 2159);
		int num5 = (int)instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		int num6 = (int)data.m_transferSize;
		bool flag = false;
		int num7 = num2;
		while (num7 <= num4 && !flag)
		{
			int num8 = num;
			while (num8 <= num3 && !flag)
			{
				ushort num9 = instance.m_citizenGrid[num7 * 2160 + num8];
				int num10 = 0;
				while (num9 != 0 && !flag)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num9].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num9].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num9].m_targetPos;
						float num11 = Vector3.SqrMagnitude(vector - position);
						if (num11 < 4096f)
						{
							CitizenInfo info = instance.m_instances.m_buffer[(int)num9].Info;
							if (info.m_citizenAI.TransportArriveAtSource(num9, ref instance.m_instances.m_buffer[(int)num9], position, position2))
							{
								if (info.m_citizenAI.SetCurrentVehicle(num9, ref instance.m_instances.m_buffer[(int)num9], vehicleID, 0u, position))
								{
									num5++;
									num6++;
								}
								else
								{
									flag = true;
								}
							}
						}
					}
					num9 = nextGridInstance;
					if (++num10 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				num8++;
			}
			num7++;
		}
		instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num5, 65535);
		data.m_transferSize = (ushort)num6;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
				return;
			}
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Leaving | Vehicle.Flags.Arriving)) != (Vehicle.Flags)0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
			if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
			{
				NetInfo.Lane lane = info.m_lanes[(int)position.m_lane];
				float num = lane.m_stopOffset;
				if ((instance.m_segments.m_buffer[(int)position.m_segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
				{
					num = -num;
				}
				instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculateStopPositionAndDirection((float)offset * 0.003921569f, num, out pos, out dir);
				maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
				return;
			}
		}
		base.CalculateSegmentPosition(vehicleID, ref vehicleData, position, laneID, offset, out pos, out dir, out maxSpeed);
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				Vector3 endPos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].CalculateSidewalkPosition();
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, position);
		}
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		return vehicleData.m_waitCounter >= 12 && base.CanLeave(vehicleID, ref vehicleData) && (vehicleData.m_transportLine == 0 || Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)vehicleData.m_transportLine].CanLeaveStop(vehicleData.m_targetBuilding, vehicleData.m_waitCounter >> 4));
	}

	public override int GetTicketPrice(ushort vehicleID, ref Vehicle vehicleData)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(vehicleData.m_targetPos3);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Event @event = instance.m_districts.m_buffer[(int)district].m_eventPolicies & Singleton<EventManager>.get_instance().GetEventPolicyMask();
		if ((servicePolicies & DistrictPolicies.Services.FreeTransport) != DistrictPolicies.Services.None)
		{
			District[] expr_6E_cp_0 = instance.m_districts.m_buffer;
			byte expr_6E_cp_1 = district;
			expr_6E_cp_0[(int)expr_6E_cp_1].m_servicePoliciesEffect = (expr_6E_cp_0[(int)expr_6E_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.FreeTransport);
			return 0;
		}
		if ((@event & DistrictPolicies.Event.ComeOneComeAll) != DistrictPolicies.Event.None)
		{
			District[] expr_9A_cp_0 = instance.m_districts.m_buffer;
			byte expr_9A_cp_1 = district;
			expr_9A_cp_0[(int)expr_9A_cp_1].m_eventPoliciesEffect = (expr_9A_cp_0[(int)expr_9A_cp_1].m_eventPoliciesEffect | DistrictPolicies.Event.ComeOneComeAll);
			return 0;
		}
		if ((servicePolicies & DistrictPolicies.Services.HighTicketPrices) != DistrictPolicies.Services.None)
		{
			District[] expr_C6_cp_0 = instance.m_districts.m_buffer;
			byte expr_C6_cp_1 = district;
			expr_C6_cp_0[(int)expr_C6_cp_1].m_servicePoliciesEffect = (expr_C6_cp_0[(int)expr_C6_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.HighTicketPrices);
			return this.m_ticketPrice * 5 / 4;
		}
		return this.m_ticketPrice;
	}

	public override InstanceID GetOwnerID(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (this.m_info.m_class.m_service == ItemClass.Service.PublicTransport)
		{
			return default(InstanceID);
		}
		InstanceID empty = InstanceID.Empty;
		empty.Building = vehicleData.m_sourceBuilding;
		return empty;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.NetNode = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public TransportInfo m_transportInfo;

	[CustomizableProperty("Passenger capacity")]
	public int m_passengerCapacity = 30;

	public int m_ticketPrice = 100;
}
