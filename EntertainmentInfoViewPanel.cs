﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class EntertainmentInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				if (Singleton<InfoManager>.get_exists())
				{
					UISprite uISprite = base.Find<UISprite>("ColorActive");
					UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[20].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[20].m_inactiveColor);
					UITextureSprite uITextureSprite = base.Find<UITextureSprite>("RateGradient");
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[20].m_negativeColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[20].m_targetColor);
				}
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
		}
	}

	private bool m_initialized;
}
