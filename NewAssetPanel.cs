﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class NewAssetPanel : UICustomControl
{
	public void SetMode(SimulationManager.UpdateMode mode)
	{
		UIPanel uIPanel = base.Find<UIPanel>("BuiltinContainer");
		if (uIPanel != null)
		{
			NewAssetBuiltinThemePanel component = uIPanel.GetComponent<NewAssetBuiltinThemePanel>();
			if (component != null)
			{
				component.SetMode(mode);
			}
		}
		uIPanel = base.Find<UIPanel>("UserContainer");
		if (uIPanel != null)
		{
			NewAssetUserThemePanel component2 = uIPanel.GetComponent<NewAssetUserThemePanel>();
			if (component2 != null)
			{
				component2.SetMode(mode);
			}
		}
	}

	private void Awake()
	{
		base.Find<UITabstrip>("Tabstrip").set_selectedIndex(-1);
	}

	public void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			ToolsMenu.SetFocus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}
}
