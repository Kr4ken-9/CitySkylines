﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class InstanceManager : SimulationManagerBase<InstanceManager, InstanceProperties>, ISimulationManager
{
	public event InstanceManager.ChangeAction m_instanceChanged
	{
		add
		{
			InstanceManager.ChangeAction changeAction = this.m_instanceChanged;
			InstanceManager.ChangeAction changeAction2;
			do
			{
				changeAction2 = changeAction;
				changeAction = Interlocked.CompareExchange<InstanceManager.ChangeAction>(ref this.m_instanceChanged, (InstanceManager.ChangeAction)Delegate.Combine(changeAction2, value), changeAction);
			}
			while (changeAction != changeAction2);
		}
		remove
		{
			InstanceManager.ChangeAction changeAction = this.m_instanceChanged;
			InstanceManager.ChangeAction changeAction2;
			do
			{
				changeAction2 = changeAction;
				changeAction = Interlocked.CompareExchange<InstanceManager.ChangeAction>(ref this.m_instanceChanged, (InstanceManager.ChangeAction)Delegate.Remove(changeAction2, value), changeAction);
			}
			while (changeAction != changeAction2);
		}
	}

	protected override void Awake()
	{
		base.Awake();
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Combine(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		this.m_names = new Dictionary<InstanceID, string>();
		this.m_groups = new Dictionary<InstanceID, InstanceManager.Group>();
		this.m_nameData = new Dictionary<string, InstanceManager.NameData>();
		this.m_lock = new object();
	}

	public override void InitializeProperties(InstanceProperties properties)
	{
		base.InitializeProperties(properties);
		Font.add_textureRebuilt(new Action<Font>(this.UpdateNamesCallback));
	}

	public override void DestroyProperties(InstanceProperties properties)
	{
		if (this.m_properties == properties)
		{
			Font.remove_textureRebuilt(new Action<Font>(this.UpdateNamesCallback));
			foreach (KeyValuePair<string, InstanceManager.NameData> current in this.m_nameData)
			{
				Mesh mesh = current.Value.m_mesh;
				if (mesh != null)
				{
					current.Value.m_mesh = null;
					Object.Destroy(mesh);
				}
			}
			this.m_nameData.Clear();
		}
		base.DestroyProperties(properties);
	}

	private void RequestCharacterInfo()
	{
		foreach (KeyValuePair<string, InstanceManager.NameData> current in this.m_nameData)
		{
			UIDynamicFont uIDynamicFont = current.Value.m_font as UIDynamicFont;
			if (uIDynamicFont != null && UIFontManager.IsDirty(uIDynamicFont))
			{
				uIDynamicFont.AddCharacterRequest(current.Key, 2, 0);
			}
		}
	}

	private void UpdateNamesCallback(Font font)
	{
		foreach (KeyValuePair<string, InstanceManager.NameData> current in this.m_nameData)
		{
			if (current.Value.m_font.get_baseFont() == font)
			{
				current.Value.m_dirty = true;
			}
		}
	}

	public InstanceManager.NameData GetNameData(string name, UIFont font, bool canCreate)
	{
		if (StringExtensions.IsNullOrWhiteSpace(name))
		{
			return null;
		}
		InstanceManager.NameData nameData;
		if (!this.m_nameData.TryGetValue(name, out nameData))
		{
			if (canCreate)
			{
				nameData = new InstanceManager.NameData();
				nameData.m_name = name;
				nameData.m_font = font;
				nameData.m_dirty = true;
				this.m_nameData.Add(name, nameData);
			}
			else
			{
				nameData = null;
			}
		}
		return nameData;
	}

	public static void RefreshNameData(InstanceManager.NameData data)
	{
		UIFontManager.Invalidate(data.m_font);
		UIRenderData uIRenderData = UIRenderData.Obtain();
		try
		{
			uIRenderData.Clear();
			PoolList<Vector3> vertices = uIRenderData.get_vertices();
			PoolList<Color32> colors = uIRenderData.get_colors();
			PoolList<Vector2> uvs = uIRenderData.get_uvs();
			PoolList<int> triangles = uIRenderData.get_triangles();
			using (UIFontRenderer uIFontRenderer = data.m_font.ObtainRenderer())
			{
				uIFontRenderer.set_defaultColor(new Color32(255, 255, 255, 64));
				uIFontRenderer.set_textScale(2f);
				uIFontRenderer.set_pixelRatio(1f);
				uIFontRenderer.set_processMarkup(true);
				uIFontRenderer.set_multiLine(false);
				uIFontRenderer.set_wordWrap(false);
				uIFontRenderer.set_textAlign(1);
				uIFontRenderer.set_maxSize(new Vector2(1000f, 100f));
				uIFontRenderer.set_shadow(false);
				uIFontRenderer.set_shadowColor(Color.get_black());
				uIFontRenderer.set_shadowOffset(Vector2.get_one());
				Vector2 size = uIFontRenderer.MeasureString(data.m_name);
				data.m_size = size;
				uIFontRenderer.set_vectorOffset(new Vector3(-500f, size.y * 0.5f, 0f));
				uIFontRenderer.Render(data.m_name, uIRenderData);
			}
			if (data.m_mesh == null)
			{
				data.m_mesh = new Mesh();
			}
			data.m_mesh.Clear();
			data.m_mesh.set_vertices(vertices.ToArray());
			data.m_mesh.set_colors32(colors.ToArray());
			data.m_mesh.set_uv(uvs.ToArray());
			data.m_mesh.set_triangles(triangles.ToArray());
		}
		finally
		{
			uIRenderData.Release();
			data.m_dirty = false;
		}
	}

	public void ReleaseInstance(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_selectedInstance == id || this.m_followedInstance == id)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.m_instanceChanged(id, InstanceID.Empty);
				});
			}
			this.m_names.Remove(id);
			InstanceManager.Group group;
			if (this.m_groups.TryGetValue(id, out group))
			{
				if (group.m_ownerInstance == id)
				{
					group.m_ownerInstance = InstanceID.Empty;
				}
				group.m_refCount--;
				this.m_groups.Remove(id);
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
	}

	public void ChangeInstance(InstanceID oldID, InstanceID newID)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_selectedInstance == oldID || this.m_followedInstance == oldID)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.m_instanceChanged(oldID, newID);
				});
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
	}

	public void SetName(InstanceID id, string newName)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (newName == null)
			{
				this.m_names.Remove(id);
			}
			else
			{
				this.m_names[id] = newName;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
	}

	public void SetGroup(InstanceID id, InstanceManager.Group newGroup)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			InstanceManager.Group group;
			if (this.m_groups.TryGetValue(id, out group))
			{
				if (group != newGroup)
				{
					if (group.m_ownerInstance == id)
					{
						group.m_ownerInstance = InstanceID.Empty;
					}
					group.m_refCount--;
					if (newGroup == null)
					{
						this.m_groups.Remove(id);
					}
					else
					{
						newGroup.m_refCount++;
						this.m_groups[id] = newGroup;
					}
				}
			}
			else if (newGroup != null)
			{
				newGroup.m_refCount++;
				this.m_groups[id] = newGroup;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
	}

	public void CopyGroup(InstanceID srcID, InstanceID dstID)
	{
		InstanceManager.Group newGroup;
		if (this.m_groups.TryGetValue(srcID, out newGroup))
		{
			this.SetGroup(dstID, newGroup);
		}
		else
		{
			this.SetGroup(dstID, null);
		}
	}

	public void ResetData()
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_names.Clear();
			this.m_groups.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new InstanceManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		base.UpdateData(mode);
		this.m_selectedInstance = InstanceID.Empty;
	}

	public bool SelectInstance(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		bool result;
		try
		{
			if (InstanceManager.IsValid(id))
			{
				this.m_selectedInstance = id;
				result = true;
			}
			else
			{
				result = false;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
		return result;
	}

	public InstanceID GetSelectedInstance()
	{
		InstanceID selectedInstance = this.m_selectedInstance;
		if (InstanceManager.IsValid(selectedInstance))
		{
			return selectedInstance;
		}
		return InstanceID.Empty;
	}

	public bool FollowInstance(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		bool result;
		try
		{
			if (InstanceManager.IsValid(id))
			{
				this.m_followedInstance = id;
				result = true;
			}
			else
			{
				result = false;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
		return result;
	}

	public string GetName(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		string result;
		try
		{
			string text;
			if (this.m_names.TryGetValue(id, out text))
			{
				result = text;
			}
			else
			{
				result = null;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
		return result;
	}

	public InstanceManager.Group GetGroup(InstanceID id)
	{
		while (!Monitor.TryEnter(this.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		InstanceManager.Group result;
		try
		{
			InstanceManager.Group group;
			if (this.m_groups.TryGetValue(id, out group))
			{
				result = group;
			}
			else
			{
				result = null;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lock);
		}
		return result;
	}

	public static InstanceID GetLocation(InstanceID id)
	{
		if (id.Citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_instance;
			if (instance2 != 0 && (instance.m_instances.m_buffer[(int)instance2].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
			{
				id.CitizenInstance = instance2;
			}
			else
			{
				switch (instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].CurrentLocation)
				{
				case Citizen.Location.Home:
					id.Building = instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_homeBuilding;
					break;
				case Citizen.Location.Work:
					id.Building = instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_workBuilding;
					break;
				case Citizen.Location.Visit:
					id.Building = instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_visitBuilding;
					break;
				case Citizen.Location.Moving:
				{
					ushort vehicle = instance.m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_vehicle;
					if (vehicle != 0)
					{
						id.Vehicle = vehicle;
					}
					else if (instance2 != 0)
					{
						id.CitizenInstance = instance2;
					}
					break;
				}
				}
			}
		}
		if (id.CitizenInstance != 0)
		{
			CitizenManager instance3 = Singleton<CitizenManager>.get_instance();
			if ((instance3.m_instances.m_buffer[(int)id.CitizenInstance].m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None)
			{
				Vector3 pos = instance3.m_instances.m_buffer[(int)id.CitizenInstance].m_targetPos;
				ushort num = Singleton<BuildingManager>.get_instance().FindBuilding(pos, 100f, ItemClass.Service.None, ItemClass.SubService.None, Building.Flags.Created, Building.Flags.Deleted);
				if (num != 0)
				{
					id.Building = num;
				}
			}
		}
		else if (id.Vehicle != 0)
		{
			VehicleManager instance4 = Singleton<VehicleManager>.get_instance();
			if ((instance4.m_vehicles.m_buffer[(int)id.Vehicle].m_flags & Vehicle.Flags.InsideBuilding) != (Vehicle.Flags)0)
			{
				Vector3 pos2 = instance4.m_vehicles.m_buffer[(int)id.Vehicle].m_targetPos0;
				ushort num2 = Singleton<BuildingManager>.get_instance().FindBuilding(pos2, 100f, ItemClass.Service.None, ItemClass.SubService.None, Building.Flags.Created, Building.Flags.Deleted);
				if (num2 != 0)
				{
					id.Building = num2;
				}
			}
		}
		return id;
	}

	public static bool GetPosition(InstanceID id, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		id = InstanceManager.GetLocation(id);
		if (!id.IsEmpty)
		{
			switch (id.Type)
			{
			case InstanceType.Building:
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				instance.m_buildings.m_buffer[(int)id.Building].GetTotalPosition(out position, out rotation, out size);
				return true;
			}
			case InstanceType.Vehicle:
			{
				VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)id.Vehicle].Info;
				instance2.m_vehicles.m_buffer[(int)id.Vehicle].GetSmoothPosition(id.Vehicle, out position, out rotation);
				size = info.m_generatedInfo.m_size;
				return true;
			}
			case InstanceType.District:
			{
				DistrictManager instance3 = Singleton<DistrictManager>.get_instance();
				position = instance3.m_districts.m_buffer[(int)id.District].m_nameLocation;
				rotation = Quaternion.get_identity();
				size..ctor(80f, 0f, 80f);
				return true;
			}
			case InstanceType.NetNode:
			{
				NetManager instance4 = Singleton<NetManager>.get_instance();
				position = instance4.m_nodes.m_buffer[(int)id.NetNode].m_position;
				rotation = Quaternion.get_identity();
				size = instance4.m_nodes.m_buffer[(int)id.NetNode].m_bounds.get_size();
				return true;
			}
			case InstanceType.NetSegment:
			{
				NetManager instance5 = Singleton<NetManager>.get_instance();
				ushort startNode = instance5.m_segments.m_buffer[(int)id.NetSegment].m_startNode;
				ushort endNode = instance5.m_segments.m_buffer[(int)id.NetSegment].m_endNode;
				if (startNode != 0 && endNode != 0)
				{
					Vector3 position2 = instance5.m_nodes.m_buffer[(int)startNode].m_position;
					Vector3 position3 = instance5.m_nodes.m_buffer[(int)endNode].m_position;
					position = (position2 + position3) * 0.5f;
					rotation = Quaternion.get_identity();
					size = instance5.m_segments.m_buffer[(int)id.NetSegment].m_bounds.get_size();
					return true;
				}
				position = Vector3.get_zero();
				rotation = Quaternion.get_identity();
				size = Vector3.get_zero();
				return false;
			}
			case InstanceType.ParkedVehicle:
			{
				VehicleManager instance6 = Singleton<VehicleManager>.get_instance();
				VehicleInfo info2 = instance6.m_vehicles.m_buffer[(int)id.Vehicle].Info;
				position = instance6.m_parkedVehicles.m_buffer[(int)id.ParkedVehicle].m_position;
				rotation = instance6.m_parkedVehicles.m_buffer[(int)id.ParkedVehicle].m_rotation;
				size = info2.m_generatedInfo.m_size;
				return true;
			}
			case InstanceType.CitizenInstance:
			{
				CitizenManager instance7 = Singleton<CitizenManager>.get_instance();
				CitizenInfo info3 = instance7.m_instances.m_buffer[(int)id.CitizenInstance].Info;
				instance7.m_instances.m_buffer[(int)id.CitizenInstance].GetSmoothPosition(id.CitizenInstance, out position, out rotation);
				size..ctor(info3.m_radius * 2f, info3.m_height, info3.m_radius * 2f);
				return true;
			}
			case InstanceType.Prop:
			{
				PropManager instance8 = Singleton<PropManager>.get_instance();
				position = instance8.m_props.m_buffer[(int)id.Prop].Position;
				rotation = Quaternion.get_identity();
				size = instance8.m_props.m_buffer[(int)id.Prop].Info.m_generatedInfo.m_size;
				return true;
			}
			case InstanceType.Tree:
			{
				TreeManager instance9 = Singleton<TreeManager>.get_instance();
				position = instance9.m_trees.m_buffer[(int)((UIntPtr)id.Tree)].Position;
				rotation = Quaternion.get_identity();
				size = instance9.m_trees.m_buffer[(int)((UIntPtr)id.Tree)].Info.m_generatedInfo.m_size;
				return true;
			}
			case InstanceType.Disaster:
			{
				DisasterManager instance10 = Singleton<DisasterManager>.get_instance();
				DisasterInfo info4 = instance10.m_disasters.m_buffer[(int)id.Disaster].Info;
				return info4.m_disasterAI.GetPosition(id.Disaster, ref instance10.m_disasters.m_buffer[(int)id.Disaster], out position, out rotation, out size);
			}
			}
		}
		position = Vector3.get_zero();
		rotation = Quaternion.get_identity();
		size = Vector3.get_zero();
		return false;
	}

	public static void GetAllGroupInstances(InstanceID id, FastList<InstanceID> list)
	{
		InstanceManager instance = Singleton<InstanceManager>.get_instance();
		InstanceManager.Group group = instance.GetGroup(id);
		if (group != null)
		{
			while (!Monitor.TryEnter(instance.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				foreach (KeyValuePair<InstanceID, InstanceManager.Group> current in instance.m_groups)
				{
					if (current.Value == group)
					{
						Dictionary<InstanceID, InstanceManager.Group>.Enumerator enumerator;
						KeyValuePair<InstanceID, InstanceManager.Group> current2 = enumerator.Current;
						list.Add(current2.Key);
					}
				}
			}
			finally
			{
				Monitor.Exit(instance.m_lock);
			}
		}
	}

	public static bool IsValid(InstanceID id)
	{
		if (id.IsEmpty)
		{
			return false;
		}
		switch (id.Type)
		{
		case InstanceType.Building:
			return Singleton<BuildingManager>.get_exists() && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)id.Building].m_flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created;
		case InstanceType.Vehicle:
			return Singleton<VehicleManager>.get_exists() && (Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)id.Vehicle].m_flags & (Vehicle.Flags.Created | Vehicle.Flags.Deleted)) == Vehicle.Flags.Created;
		case InstanceType.District:
			return Singleton<DistrictManager>.get_exists() && (Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)id.District].m_flags & District.Flags.Created) == District.Flags.Created;
		case InstanceType.Citizen:
			return Singleton<CitizenManager>.get_exists() && (Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].m_flags & Citizen.Flags.Created) == Citizen.Flags.Created;
		case InstanceType.NetNode:
			return Singleton<NetManager>.get_exists() && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)id.NetNode].m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) == NetNode.Flags.Created;
		case InstanceType.NetSegment:
			return Singleton<NetManager>.get_exists() && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)id.NetSegment].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created;
		case InstanceType.ParkedVehicle:
			return Singleton<VehicleManager>.get_exists() && (Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)id.ParkedVehicle].m_flags & 3) == 1;
		case InstanceType.TransportLine:
			return Singleton<TransportManager>.get_exists() && (Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)id.TransportLine].m_flags & (TransportLine.Flags.Created | TransportLine.Flags.Deleted)) == TransportLine.Flags.Created;
		case InstanceType.CitizenInstance:
			return Singleton<CitizenManager>.get_exists() && (Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)id.CitizenInstance].m_flags & (CitizenInstance.Flags.Created | CitizenInstance.Flags.Deleted)) == CitizenInstance.Flags.Created;
		case InstanceType.Disaster:
			return Singleton<DisasterManager>.get_exists() && (Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)id.Disaster].m_flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted)) == DisasterData.Flags.Created;
		}
		return false;
	}

	public static PrefabInfo GetPrefabInfo(InstanceID id)
	{
		if (id.IsEmpty)
		{
			return null;
		}
		switch (id.Type)
		{
		case InstanceType.Building:
			return Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)id.Building].Info;
		case InstanceType.Vehicle:
			return Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)id.Vehicle].Info;
		case InstanceType.Citizen:
			return Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)id.Citizen)].GetCitizenInfo(id.Citizen);
		case InstanceType.NetNode:
			return Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)id.NetNode].Info;
		case InstanceType.NetSegment:
			return Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)id.NetSegment].Info;
		case InstanceType.ParkedVehicle:
			return Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)id.ParkedVehicle].Info;
		case InstanceType.TransportLine:
			return Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)id.TransportLine].Info;
		case InstanceType.CitizenInstance:
			return Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)id.CitizenInstance].Info;
		case InstanceType.Disaster:
			return Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)id.Disaster].Info;
		}
		return null;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	private InstanceID m_selectedInstance;

	private InstanceID m_followedInstance;

	private Dictionary<InstanceID, string> m_names;

	private Dictionary<InstanceID, InstanceManager.Group> m_groups;

	private Dictionary<string, InstanceManager.NameData> m_nameData;

	private object m_lock;

	public delegate void ChangeAction(InstanceID oldId, InstanceID newID);

	public class Group : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			this.m_ownerInstance.Serialize(s);
		}

		public void Deserialize(DataSerializer s)
		{
			this.m_ownerInstance.Deserialize(s);
		}

		public void AfterDeserialize(DataSerializer s)
		{
		}

		public InstanceID m_ownerInstance;

		public int m_refCount;
	}

	public class NameData
	{
		public string m_name;

		public UIFont m_font;

		public Mesh m_mesh;

		public Vector2 m_size;

		public bool m_dirty;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "InstanceManager");
			InstanceManager instance = Singleton<InstanceManager>.get_instance();
			s.WriteUInt32((uint)instance.m_names.Count);
			foreach (KeyValuePair<InstanceID, string> current in instance.m_names)
			{
				current.Key.Serialize(s);
				s.WriteUniqueString(current.Value);
			}
			s.WriteUInt32((uint)instance.m_groups.Count);
			foreach (KeyValuePair<InstanceID, InstanceManager.Group> current2 in instance.m_groups)
			{
				current2.Key.Serialize(s);
				s.WriteObject<InstanceManager.Group>(current2.Value);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "InstanceManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "InstanceManager");
			InstanceManager instance = Singleton<InstanceManager>.get_instance();
			while (!Monitor.TryEnter(instance.m_lock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
				if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
				{
					instance.m_names.Clear();
					instance.m_groups.Clear();
					uint num = s.ReadUInt32();
					for (uint num2 = 0u; num2 < num; num2 += 1u)
					{
						InstanceID key = default(InstanceID);
						key.Deserialize(s);
						string value = s.ReadUniqueString();
						instance.m_names.Add(key, value);
					}
					if (s.get_version() >= 258u)
					{
						num = s.ReadUInt32();
						for (uint num3 = 0u; num3 < num; num3 += 1u)
						{
							InstanceID key2 = default(InstanceID);
							key2.Deserialize(s);
							InstanceManager.Group group = s.ReadObject<InstanceManager.Group>();
							group.m_refCount++;
							instance.m_groups.Add(key2, group);
						}
					}
				}
				else
				{
					Dictionary<InstanceID, string> dictionary = new Dictionary<InstanceID, string>();
					Dictionary<InstanceID, InstanceManager.Group> dictionary2 = new Dictionary<InstanceID, InstanceManager.Group>();
					foreach (KeyValuePair<InstanceID, string> current in instance.m_names)
					{
						if (current.Key.Disaster != 0)
						{
							dictionary.Add(current.Key, current.Value);
						}
					}
					foreach (KeyValuePair<InstanceID, InstanceManager.Group> current2 in instance.m_groups)
					{
						if (current2.Key.Disaster != 0)
						{
							dictionary2.Add(current2.Key, current2.Value);
						}
					}
					instance.m_names = dictionary;
					instance.m_groups = dictionary2;
					uint num4 = s.ReadUInt32();
					for (uint num5 = 0u; num5 < num4; num5 += 1u)
					{
						InstanceID key3 = default(InstanceID);
						key3.Deserialize(s);
						string value2 = s.ReadUniqueString();
						if (key3.Disaster == 0)
						{
							instance.m_names.Add(key3, value2);
						}
					}
					if (s.get_version() >= 258u)
					{
						num4 = s.ReadUInt32();
						for (uint num6 = 0u; num6 < num4; num6 += 1u)
						{
							default(InstanceID).Deserialize(s);
							s.ReadObject<InstanceManager.Group>();
						}
					}
				}
			}
			finally
			{
				Monitor.Exit(instance.m_lock);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "InstanceManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "InstanceManager");
			InstanceManager instance = Singleton<InstanceManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			if (s.get_version() < 126u)
			{
				foreach (KeyValuePair<InstanceID, string> current in instance.m_names)
				{
					if (current.Key.District != 0)
					{
						District[] expr_83_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
						byte expr_83_cp_1 = current.Key.District;
						expr_83_cp_0[(int)expr_83_cp_1].m_flags = (expr_83_cp_0[(int)expr_83_cp_1].m_flags | District.Flags.CustomName);
					}
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "InstanceManager");
		}
	}
}
