﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class SportMatchAI : EventAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
	}

	public override void ReleaseAI()
	{
		base.ReleaseAI();
	}

	public override string GetDebugString(ushort eventID, ref EventData data)
	{
		string text = base.GetDebugString(eventID, ref data);
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			int winProbability = this.GetWinProbability(eventID, ref data);
			text += StringUtils.SafeFormat("\nWin probability: {0}%", winProbability);
		}
		return text;
	}

	protected override string GetEventName(ushort eventID, ref EventData data)
	{
		string eventName = base.GetEventName(eventID, ref data);
		string teamName = this.GetTeamName(eventID, ref data);
		string opponentName = this.GetOpponentName(eventID, ref data);
		return StringUtils.SafeFormat("{0}\n{1} vs {2}", new object[]
		{
			eventName,
			teamName,
			opponentName
		});
	}

	public override void GetAnimationState(ushort eventID, ref EventData data, ref float active, ref int state, ref bool playActiveState, ref Color color1, ref Color color2)
	{
		Randomizer randomizer;
		randomizer..ctor(data.m_startFrame ^ (uint)data.m_building ^ (uint)eventID);
		int num = randomizer.Int32((uint)this.m_opponentColors.Length);
		if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
		{
			color1 = data.m_color;
			color2 = this.m_opponentColors[num];
			Color color3 = this.m_opponentColors[(num + this.m_opponentColors.Length / 2) % this.m_opponentColors.Length];
			float num2 = Mathf.Abs(color2.r - color1.r) + Mathf.Abs(color2.g - color1.g) + Mathf.Abs(color2.b - color1.b);
			float num3 = Mathf.Abs(color3.r - color1.r) + Mathf.Abs(color3.g - color1.g) + Mathf.Abs(color3.b - color1.b);
			if (num3 > num2)
			{
				color2 = color3;
			}
		}
		if (active < 0.1f || (data.m_flags & (EventData.Flags.Active | EventData.Flags.Disorganizing)) == EventData.Flags.None)
		{
			active = 0f;
			state = 0;
			return;
		}
		if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
		{
			uint num4 = (uint)Mathf.RoundToInt(this.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint num5 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - data.m_startFrame;
			uint num6 = num5 * (uint)this.m_goalSteps / num4;
			int num7 = Mathf.Max(1, randomizer.Int32(this.m_minGoalCount, this.m_maxGoalCount));
			int num8 = Mathf.Max(1, this.m_goalSteps / num7);
			if ((ulong)num6 % (ulong)((long)num8) == (ulong)((long)randomizer.Int32((uint)num8)))
			{
				state = (((data.m_flags & EventData.Flags.Success) == EventData.Flags.None) ? -2 : 2);
			}
			else
			{
				state = ((randomizer.Int32(2u) != 0) ? -1 : 1);
			}
		}
		else
		{
			state = 0;
			playActiveState = false;
		}
	}

	public override bool GetAudioState(ushort eventID, ref EventData data, ref float volume, ref float maxDistance, ref AudioInfo clip)
	{
		maxDistance = 200f;
		return (data.m_flags & EventData.Flags.Active) != EventData.Flags.None;
	}

	public override void CreateEvent(ushort eventID, ref EventData data)
	{
		base.CreateEvent(eventID, ref data);
		EventManager instance = Singleton<EventManager>.get_instance();
		if (this.m_syncronizeStart)
		{
			ushort similarEvent = this.GetSimilarEvent(eventID, ref data);
			if (similarEvent != 0)
			{
				uint num = data.m_expireFrame - data.m_startFrame;
				uint startFrame = instance.m_events.m_buffer[(int)similarEvent].m_startFrame;
				if (startFrame < data.m_startFrame)
				{
					uint num2 = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
					uint dAYTIME_FRAMES = SimulationManager.DAYTIME_FRAMES;
					uint expireFrame = instance.m_events.m_buffer[(int)similarEvent].m_expireFrame;
					if (expireFrame > data.m_startFrame - num2)
					{
						data.m_startFrame += (expireFrame - (data.m_startFrame - num2) + dAYTIME_FRAMES - 1u) / dAYTIME_FRAMES * dAYTIME_FRAMES;
					}
				}
				else
				{
					data.m_startFrame = startFrame;
				}
				data.m_expireFrame = data.m_startFrame + num;
			}
		}
		if (data.m_nextBuildingEvent != 0)
		{
			data.m_totalReward = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_totalReward;
			data.m_totalTicket = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_totalTicket;
			data.m_successCount = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_successCount;
			data.m_failureCount = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_failureCount;
			data.m_ticketPrice = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_ticketPrice;
			data.m_color = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_color;
			if (data.m_color.a != 255)
			{
				data.m_color = this.m_defaultTeamColor;
				if (data.m_building != 0)
				{
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
				}
			}
		}
		else
		{
			data.m_color = this.m_defaultTeamColor;
			if (data.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
			}
		}
		this.GenerateCustomSeed(eventID, ref data);
	}

	public override void ReleaseEvent(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
		}
		base.ReleaseEvent(eventID, ref data);
	}

	public override void SimulationStep(ushort eventID, ref EventData data)
	{
		base.SimulationStep(eventID, ref data);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (data.m_building != 0 && instance.m_buildings.m_buffer[(int)data.m_building].m_eventIndex == eventID)
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)data.m_building].m_position);
			DistrictPolicies.Event eventPolicies = instance2.m_districts.m_buffer[(int)district].m_eventPolicies;
			District[] expr_8A_cp_0 = instance2.m_districts.m_buffer;
			byte expr_8A_cp_1 = district;
			expr_8A_cp_0[(int)expr_8A_cp_1].m_eventPoliciesEffect = (expr_8A_cp_0[(int)expr_8A_cp_1].m_eventPoliciesEffect | (eventPolicies & (DistrictPolicies.Event.SubsidizedYouth | DistrictPolicies.Event.MatchSecurity)));
			if ((eventPolicies & DistrictPolicies.Event.SubsidizedYouth) != DistrictPolicies.Event.None)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_building].Info;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 56250, info.m_class);
			}
			if ((eventPolicies & DistrictPolicies.Event.MatchSecurity) != DistrictPolicies.Event.None)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_building].Info;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 3125, info2.m_class);
			}
			if ((data.m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
			{
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<EventManager>.get_instance().m_matchPreparing.Activate(properties.m_matchPreparing, data.m_building);
				}
			}
		}
		if ((data.m_flags & EventData.Flags.CustomSeed) == EventData.Flags.None)
		{
			this.GenerateCustomSeed(eventID, ref data);
		}
	}

	private void GenerateCustomSeed(ushort eventID, ref EventData data)
	{
		Randomizer randomizer;
		randomizer..ctor((long)((ulong)data.m_startFrame ^ (ulong)((long)((long)data.m_building << 8)) ^ (ulong)((long)((long)eventID << 16))));
		EventManager instance = Singleton<EventManager>.get_instance();
		FastList<ushort> similarEvents = this.GetSimilarEvents(eventID, ref data, true);
		for (int i = 0; i < 1000; i++)
		{
			bool flag = true;
			for (int j = 0; j < similarEvents.m_size; j++)
			{
				ushort num = similarEvents.m_buffer[j];
				if (eventID != num)
				{
					Randomizer r = default(Randomizer);
					r.seed = instance.m_events.m_buffer[(int)num].m_customSeed;
					if (SportMatchAI.CompareOpponentCityName(randomizer, r))
					{
						flag = false;
						break;
					}
				}
			}
			if (flag)
			{
				Randomizer randomizer2 = randomizer;
				string a = SportMatchAI.GenerateOpponentCityName(ref randomizer2);
				flag = (a != Singleton<SimulationManager>.get_instance().m_metaData.m_CityName);
			}
			if (flag)
			{
				break;
			}
			randomizer.Int32(1000u);
		}
		data.m_flags |= EventData.Flags.CustomSeed;
		data.m_customSeed = randomizer.seed;
	}

	protected override void BeginPreparing(ushort eventID, ref EventData data)
	{
		base.BeginPreparing(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
	}

	protected override void BeginEvent(ushort eventID, ref EventData data)
	{
		int winProbability = this.GetWinProbability(eventID, ref data);
		if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < winProbability)
		{
			data.m_flags |= EventData.Flags.Success;
		}
		else
		{
			data.m_flags |= EventData.Flags.Failure;
		}
		base.BeginEvent(eventID, ref data);
	}

	private int GetWinProbability(ushort eventID, ref EventData data)
	{
		int num;
		int num2;
		int num3;
		base.CountVisitors(eventID, ref data, out num, out num2, out num3);
		int num4 = Mathf.Clamp(num * 117 / Mathf.Max(1, num3), 30, 70);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (data.m_building != 0)
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)data.m_building].m_position);
			DistrictPolicies.Event eventPolicies = instance2.m_districts.m_buffer[(int)district].m_eventPolicies;
			if ((eventPolicies & DistrictPolicies.Event.SubsidizedYouth) != DistrictPolicies.Event.None)
			{
				num4 += 10;
			}
		}
		int ticketPrice = this.GetTicketPrice(eventID, ref data);
		int num5 = Mathf.Clamp((ticketPrice * 100 + (this.m_ticketPrice >> 1)) / Mathf.Max(1, this.m_ticketPrice) - 100, -30, 30);
		if (num5 < -20)
		{
			num4 += 7 - (20 + num5) / 2;
		}
		else if (num5 < -10)
		{
			num4 += 5 - (10 + num5) / 5;
		}
		else if (num5 < 10)
		{
			num4 -= num5 / 2;
		}
		else if (num5 < 20)
		{
			num4 -= 5 + (num5 - 10) / 5;
		}
		else
		{
			num4 -= 7 + (num5 - 20) / 2;
		}
		return Mathf.Clamp(num4, 0, 100);
	}

	protected override void EndEvent(ushort eventID, ref EventData data)
	{
		base.EndEvent(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
		if (data.m_ticketMoney != 0uL)
		{
			Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PublicIncome, (int)data.m_ticketMoney, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
			data.m_totalTicket += data.m_ticketMoney;
		}
		if ((data.m_flags & EventData.Flags.Success) != EventData.Flags.None)
		{
			int num = Mathf.Max(1, this.GetSimilarEvents(eventID, ref data, true).m_size);
			int num2 = this.GetPrizeSum(eventID, ref data) / num;
			if (num2 >= 10)
			{
				if (num2 < 100)
				{
					num2 = num2 / 10 * 10;
				}
				else if (num2 < 1000)
				{
					num2 = num2 / 100 * 100;
				}
				else if (num2 < 10000)
				{
					num2 = num2 / 1000 * 1000;
				}
				else
				{
					num2 = num2 / 10000 * 10000;
				}
			}
			num2 *= 100;
			data.m_rewardMoney = (ulong)((long)num2);
			data.m_totalReward += data.m_rewardMoney;
			data.m_successCount = (ushort)Mathf.Min((int)(data.m_successCount + 1), 65535);
			if (num2 > 0)
			{
				Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.RewardAmount, num2, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
			}
			string teamName = this.GetTeamName(eventID, ref data);
			Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_onWinMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID(data.m_building, CitizenUnit.Flags.Visit), teamName);
			if (data.m_building != 0)
			{
				Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].m_position;
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Wellbeing, 0.2f, 3000f);
			}
		}
		else if ((data.m_flags & EventData.Flags.Failure) != EventData.Flags.None)
		{
			data.m_failureCount = (ushort)Mathf.Min((int)(data.m_failureCount + 1), 65535);
			string teamName2 = this.GetTeamName(eventID, ref data);
			Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_onLoseMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID(data.m_building, CitizenUnit.Flags.Visit), teamName2);
			if (data.m_building != 0)
			{
				Vector3 position2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].m_position;
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position2, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.Wellbeing, -0.2f, 3000f);
			}
		}
	}

	protected override void EndDisorganizing(ushort eventID, ref EventData data)
	{
		base.EndDisorganizing(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
	}

	protected override void Cancel(ushort eventID, ref EventData data)
	{
		base.Cancel(eventID, ref data);
		EventManager instance = Singleton<EventManager>.get_instance();
		data.m_ticketMoney = 0uL;
		instance.m_globalEventDataDirty = true;
		ushort nextBuildingEvent = data.m_nextBuildingEvent;
		if (nextBuildingEvent != 0 && (instance.m_events.m_buffer[(int)nextBuildingEvent].m_flags & EventData.Flags.Cancelled) != EventData.Flags.None)
		{
			EventInfo info = instance.m_events.m_buffer[(int)nextBuildingEvent].Info;
			uint num = (uint)Mathf.RoundToInt(info.m_eventAI.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint num2 = instance.m_events.m_buffer[(int)nextBuildingEvent].m_startFrame + num;
			if (currentFrameIndex < num2)
			{
				Singleton<EventManager>.get_instance().ReleaseEvent(eventID);
			}
		}
	}

	private int GetPrizeSum(ushort eventID, ref EventData data)
	{
		int finalCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount;
		if (finalCount <= 2000)
		{
			return this.m_minPrize;
		}
		if (finalCount <= 6000)
		{
			return this.m_minPrize + this.m_prizeStep;
		}
		if (finalCount <= 10000)
		{
			return this.m_minPrize + this.m_prizeStep * 2;
		}
		if (finalCount <= 20000)
		{
			return this.m_minPrize + this.m_prizeStep * 3;
		}
		if (finalCount <= 35000)
		{
			return this.m_minPrize + this.m_prizeStep * 4;
		}
		if (finalCount <= 45000)
		{
			return this.m_minPrize + this.m_prizeStep * 5;
		}
		if (finalCount <= 60000)
		{
			return this.m_minPrize + this.m_prizeStep * 6;
		}
		if (finalCount <= 99999)
		{
			return this.m_minPrize + this.m_prizeStep * 7;
		}
		return this.m_minPrize + this.m_prizeStep * 9 + (finalCount - 100000) / 10000 * this.m_prizeStep;
	}

	private ushort GetSimilarEvent(ushort eventID, ref EventData data)
	{
		ushort result = 0;
		if (data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			EventManager instance2 = Singleton<EventManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_building].Info;
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(info.m_class.m_service);
			uint num = 0u;
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num2 = serviceBuildings.m_buffer[i];
				if (num2 != data.m_building)
				{
					BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num2].Info;
					if (info2.m_buildingAI.SupportEvents(this.m_info.m_type, (EventManager.EventGroup)(-1)))
					{
						ushort eventIndex = instance.m_buildings.m_buffer[(int)num2].m_eventIndex;
						if (eventIndex != 0 && instance2.m_events.m_buffer[(int)eventIndex].m_startFrame >= num)
						{
							result = eventIndex;
							num = instance2.m_events.m_buffer[(int)eventIndex].m_startFrame;
						}
					}
				}
			}
		}
		return result;
	}

	private FastList<ushort> GetSimilarEvents(ushort eventID, ref EventData data, bool requireSameStartTime)
	{
		if (SportMatchAI.m_tempSimilarEventsBuffer == null)
		{
			SportMatchAI.m_tempSimilarEventsBuffer = new FastList<ushort>();
		}
		FastList<ushort> tempSimilarEventsBuffer = SportMatchAI.m_tempSimilarEventsBuffer;
		tempSimilarEventsBuffer.Clear();
		if (data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			EventManager instance2 = Singleton<EventManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_building].Info;
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(info.m_class.m_service);
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num = serviceBuildings.m_buffer[i];
				if (num != data.m_building)
				{
					BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num].Info;
					if (info2.m_buildingAI.SupportEvents(this.m_info.m_type, (EventManager.EventGroup)(-1)))
					{
						ushort eventIndex = instance.m_buildings.m_buffer[(int)num].m_eventIndex;
						if (eventIndex != 0 && (instance2.m_events.m_buffer[(int)eventIndex].m_flags & EventData.Flags.Expired) == EventData.Flags.None && (!requireSameStartTime || instance2.m_events.m_buffer[(int)eventIndex].m_startFrame == data.m_startFrame))
						{
							tempSimilarEventsBuffer.Add(eventIndex);
						}
					}
				}
			}
		}
		return tempSimilarEventsBuffer;
	}

	public override void SetColor(ushort eventID, ref EventData data, Color32 newColor)
	{
		if (newColor.r != data.m_color.r || newColor.g != data.m_color.g || newColor.b != data.m_color.b)
		{
			base.SetColor(eventID, ref data, newColor);
			if (data.m_nextBuildingEvent != 0)
			{
				Singleton<EventManager>.get_instance().m_events.m_buffer[(int)data.m_nextBuildingEvent].m_color = newColor;
			}
			Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
			if (data.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
				Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(data.m_building, false);
			}
			Singleton<GuideManager>.get_instance().m_teamColorNotUsed.Disable();
		}
	}

	public override void VisitorEnter(ushort eventID, ref EventData data, ushort buildingID, uint citizenID)
	{
		base.VisitorEnter(eventID, ref data, buildingID, citizenID);
		if (buildingID == data.m_building && (data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			data.m_ticketMoney += (ulong)((long)this.GetTicketPrice(eventID, ref data));
		}
	}

	public override int GetEventPriority(int freeSpace, int maxSpace)
	{
		return 8;
	}

	public override Color32 GetCitizenColor(ushort eventID, ref EventData data, uint citizen)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None || citizen != 0u)
		{
			return data.m_color;
		}
		return new Color32(0, 0, 0, 0);
	}

	public override Color32 GetBuildingColor(ushort eventID, ref EventData data)
	{
		if (this.m_allowChangeColor)
		{
			return data.m_color;
		}
		return base.GetBuildingColor(eventID, ref data);
	}

	public override DistrictPolicies.Event GetEventPolicyMask(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active | EventData.Flags.Disorganizing)) != EventData.Flags.None)
		{
			return DistrictPolicies.Event.SubsidizedYouth | DistrictPolicies.Event.ComeOneComeAll | DistrictPolicies.Event.MatchSecurity;
		}
		return DistrictPolicies.Event.SubsidizedYouth | DistrictPolicies.Event.MatchSecurity;
	}

	public override int GetBudget(ushort eventID, ref EventData data)
	{
		return 100;
	}

	public override int GetCapacity(ushort eventID, ref EventData data, int orig, int max)
	{
		return max;
	}

	public override int GetCrimeAccumulation(ushort eventID, ref EventData data, int orig)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			if (data.m_building != 0)
			{
				DistrictManager instance = Singleton<DistrictManager>.get_instance();
				byte district = instance.GetDistrict(Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].m_position);
				DistrictPolicies.Event eventPolicies = instance.m_districts.m_buffer[(int)district].m_eventPolicies;
				if ((eventPolicies & DistrictPolicies.Event.MatchSecurity) != DistrictPolicies.Event.None)
				{
					return orig * 3 >> 1;
				}
			}
			return orig << 1;
		}
		return orig;
	}

	public string GetTeamName(ushort eventID, ref EventData data)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		string text = PrefabCollection<EventInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.Count("SPORT_TEAM_NAME", text);
		if (num != 0u)
		{
			Randomizer randomizer;
			randomizer..ctor((int)data.m_building);
			string format = Locale.Get("SPORT_TEAM_NAME", text, randomizer.Int32(num));
			string text2 = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			if (text2 == null)
			{
				text2 = "Cities: Skylines";
			}
			return StringUtils.SafeFormat(format, text2);
		}
		return text;
	}

	public string GetOpponentName(ushort eventID, ref EventData data)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		string text = PrefabCollection<EventInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.Count("SPORT_OPPONENT_NAME", text);
		if (num != 0u)
		{
			Randomizer randomizer = default(Randomizer);
			randomizer.seed = data.m_customSeed;
			string text2 = SportMatchAI.GenerateOpponentCityName(ref randomizer);
			if (text2 == null)
			{
				text2 = "Cities: Skylines";
			}
			string format = Locale.Get("SPORT_OPPONENT_NAME", text, randomizer.Int32(num));
			return StringUtils.SafeFormat(format, text2);
		}
		return text;
	}

	private static string GenerateOpponentCityName(ref Randomizer r)
	{
		string result = null;
		CityNameGroups.Environment cityNameGroups = Singleton<BuildingManager>.get_instance().m_cityNameGroups;
		if (cityNameGroups != null)
		{
			int num = cityNameGroups.m_closeDistance.Length;
			if (num != 0)
			{
				string text = cityNameGroups.m_closeDistance[r.Int32((uint)num)];
				uint num2 = Locale.Count("CONNECTIONS_PATTERN", text);
				int num3 = r.Int32(num2);
				string format = Locale.Get("CONNECTIONS_PATTERN", text, num3);
				uint num4 = Locale.Count("CONNECTIONS_NAME", text);
				int num5 = r.Int32(num4);
				string arg = Locale.Get("CONNECTIONS_NAME", text, num5);
				result = StringUtils.SafeFormat(format, arg);
			}
		}
		return result;
	}

	private static bool CompareOpponentCityName(Randomizer r1, Randomizer r2)
	{
		CityNameGroups.Environment cityNameGroups = Singleton<BuildingManager>.get_instance().m_cityNameGroups;
		if (cityNameGroups != null)
		{
			int num = cityNameGroups.m_closeDistance.Length;
			if (num != 0)
			{
				string text = cityNameGroups.m_closeDistance[r1.Int32((uint)num)];
				if (text != cityNameGroups.m_closeDistance[r2.Int32((uint)num)])
				{
					return false;
				}
				uint num2 = Locale.Count("CONNECTIONS_PATTERN", text);
				int num3 = r1.Int32(num2);
				if (num3 != r2.Int32(num2))
				{
					return false;
				}
				uint num4 = Locale.Count("CONNECTIONS_NAME", text);
				int num5 = r1.Int32(num4);
				return num5 == r2.Int32(num4);
			}
		}
		return false;
	}

	public Color GetOpponentColor(ushort eventID, ref EventData data)
	{
		Randomizer randomizer;
		randomizer..ctor(data.m_startFrame ^ (uint)data.m_building ^ (uint)eventID);
		int num = randomizer.Int32((uint)this.m_opponentColors.Length);
		Color color = data.m_color;
		Color result = this.m_opponentColors[num];
		Color color2 = this.m_opponentColors[(num + this.m_opponentColors.Length / 2) % this.m_opponentColors.Length];
		float num2 = Mathf.Abs(result.r - color.r) + Mathf.Abs(result.g - color.g) + Mathf.Abs(result.b - color.b);
		float num3 = Mathf.Abs(color2.r - color.r) + Mathf.Abs(color2.g - color.g) + Mathf.Abs(color2.b - color.b);
		if (num3 > num2)
		{
			result = color2;
		}
		return result;
	}

	public Color m_defaultTeamColor = Color.get_red();

	public Color[] m_opponentColors = new Color[]
	{
		new Color(1f, 0f, 0f, 1f),
		new Color(1f, 1f, 0f, 1f),
		new Color(0f, 1f, 0f, 1f),
		new Color(0f, 1f, 1f, 1f),
		new Color(0f, 0f, 1f, 1f),
		new Color(1f, 0f, 1f, 1f)
	};

	public int m_minPrize = 60000;

	public int m_prizeStep = 20000;

	public bool m_syncronizeStart = true;

	public bool m_allowChangeColor = true;

	public int m_goalSteps = 10;

	public int m_minGoalCount = 2;

	public int m_maxGoalCount = 5;

	public MessageInfo m_onWinMessage;

	public MessageInfo m_onLoseMessage;

	private static FastList<ushort> m_tempSimilarEventsBuffer;
}
