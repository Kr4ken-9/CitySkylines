﻿using System;
using ColossalFramework;
using ColossalFramework.IO;

public class DisasterEffect : TriggerEffect
{
	public void SetDisaster(ushort disaster)
	{
		InstanceID empty = InstanceID.Empty;
		empty.Disaster = disaster;
		this.m_group = Singleton<InstanceManager>.get_instance().GetGroup(empty);
	}

	public override void Activate()
	{
		base.Activate();
		ushort disaster = this.GetDisaster();
		if (disaster != 0)
		{
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			if ((instance.m_disasters.m_buffer[(int)disaster].m_flags & DisasterData.Flags.Created) != DisasterData.Flags.None)
			{
				DisasterInfo info = instance.m_disasters.m_buffer[(int)disaster].Info;
				info.m_disasterAI.StartNow(disaster, ref instance.m_disasters.m_buffer[(int)disaster]);
			}
		}
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteObject<InstanceManager.Group>(this.m_group);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_group = s.ReadObject<InstanceManager.Group>();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
		ushort disaster = this.GetDisaster();
		if (disaster != 0)
		{
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			DisasterData[] expr_2B_cp_0 = instance.m_disasters.m_buffer;
			ushort expr_2B_cp_1 = disaster;
			expr_2B_cp_0[(int)expr_2B_cp_1].m_flags = (expr_2B_cp_0[(int)expr_2B_cp_1].m_flags | DisasterData.Flags.Persistent);
		}
	}

	public ushort GetDisaster()
	{
		if (this.m_group != null)
		{
			return this.m_group.m_ownerInstance.Disaster;
		}
		return 0;
	}

	private InstanceManager.Group m_group;
}
