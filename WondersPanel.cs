﻿using System;
using ColossalFramework.UI;

public sealed class WondersPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Wonder);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
