﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;

public class ExceptionPanel : MenuPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.m_Binding = base.GetComponent<BindPropertyByKey>();
	}

	public void SetMessage(string title, string message, bool error)
	{
		if (this.m_Binding != null)
		{
			string text = (!error) ? "IconWarning" : "IconError";
			this.m_Binding.SetProperties(TooltipHelper.Format(new string[]
			{
				"title",
				title,
				"message",
				message,
				"img",
				text
			}));
		}
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Ok");
			uIButton.Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && (p.get_keycode() == 27 || p.get_keycode() == 13))
		{
			this.OnClosed();
			p.Use();
		}
	}

	public void OnCopy()
	{
		Clipboard.set_text(base.Find<UILabel>("Message").get_text());
	}

	public void OnMessageChanged()
	{
		UIScrollablePanel uIScrollablePanel = base.Find<UIScrollablePanel>("Scrollable Panel");
		if (uIScrollablePanel != null)
		{
			uIScrollablePanel.Reset();
		}
	}

	private BindPropertyByKey m_Binding;
}
