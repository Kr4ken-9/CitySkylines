﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class ElectricityManager : SimulationManagerBase<ElectricityManager, ElectricityProperties>, ISimulationManager, IRenderableManager
{
	public bool ElectricityMapVisible
	{
		get
		{
			return this.m_electricityMapVisible;
		}
		set
		{
			if (this.m_electricityMapVisible != value)
			{
				this.m_electricityMapVisible = value;
				this.UpdateElectricityMapping();
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_electricityGrid = new ElectricityManager.Cell[65536];
		this.m_pulseGroups = new ElectricityManager.PulseGroup[1024];
		this.m_pulseUnits = new ElectricityManager.PulseUnit[32768];
		this.m_nodeGroups = new ushort[32768];
		this.m_electricityTexture = new Texture2D(256, 256, 4, false, true);
		this.m_electricityTexture.set_filterMode(0);
		this.m_electricityTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_ElectricityTexture", this.m_electricityTexture);
		this.UpdateElectricityMapping();
		this.m_modifiedX1 = 0;
		this.m_modifiedZ1 = 0;
		this.m_modifiedX2 = 255;
		this.m_modifiedZ2 = 255;
	}

	private void OnDestroy()
	{
		if (this.m_electricityTexture != null)
		{
			Object.Destroy(this.m_electricityTexture);
			this.m_electricityTexture = null;
		}
	}

	private void UpdateElectricityMapping()
	{
		Vector4 vector;
		vector.z = 0.000102124184f;
		vector.x = 0.5f;
		vector.y = 0.5f;
		vector.w = 0.00390625f;
		Shader.SetGlobalVector("_ElectricityMapping", vector);
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_electricityMapVisible && this.m_modifiedX2 >= this.m_modifiedX1 && this.m_modifiedZ2 >= this.m_modifiedZ1)
		{
			this.UpdateTexture();
		}
	}

	private void UpdateTexture()
	{
		while (!Monitor.TryEnter(this.m_electricityGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		int modifiedX;
		int modifiedZ;
		int modifiedX2;
		int modifiedZ2;
		try
		{
			modifiedX = this.m_modifiedX1;
			modifiedZ = this.m_modifiedZ1;
			modifiedX2 = this.m_modifiedX2;
			modifiedZ2 = this.m_modifiedZ2;
			this.m_modifiedX1 = 10000;
			this.m_modifiedZ1 = 10000;
			this.m_modifiedX2 = -10000;
			this.m_modifiedZ2 = -10000;
		}
		finally
		{
			Monitor.Exit(this.m_electricityGrid);
		}
		for (int i = modifiedZ; i <= modifiedZ2; i++)
		{
			for (int j = modifiedX; j <= modifiedX2; j++)
			{
				ElectricityManager.Cell cell = this.m_electricityGrid[i * 256 + j];
				Color color;
				if (cell.m_conductivity >= 64 && j != 0 && i != 0 && j != 255 && i != 255)
				{
					color.r = Mathf.Min(1f, 0.7f + (float)(cell.m_conductivity - 64) * 0.00208333344f);
					color.g = 0f;
					color.b = 0f;
					color.a = 0f;
				}
				else
				{
					color.r = 0f;
					color.g = 0f;
					color.b = 0f;
					color.a = 0f;
				}
				this.m_electricityTexture.SetPixel(j, i, color);
			}
		}
		this.m_electricityTexture.Apply();
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ)
	{
		while (!Monitor.TryEnter(this.m_electricityGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_modifiedX1 = Mathf.Min(this.m_modifiedX1, minX);
			this.m_modifiedZ1 = Mathf.Min(this.m_modifiedZ1, minZ);
			this.m_modifiedX2 = Mathf.Max(this.m_modifiedX2, maxX);
			this.m_modifiedZ2 = Mathf.Max(this.m_modifiedZ2, maxZ);
		}
		finally
		{
			Monitor.Exit(this.m_electricityGrid);
		}
	}

	public int TryDumpElectricity(Vector3 pos, int rate, int max)
	{
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		if (max > 15000)
		{
			int num3 = (max + 14999) / 15000;
			int num4 = Mathf.RoundToInt(Mathf.Sqrt((float)num3));
			int num5 = num3 / num4;
			int num6 = Mathf.Max(0, num - (num4 >> 1));
			int num7 = Mathf.Max(0, num2 - (num5 >> 1));
			int num8 = Mathf.Min(255, num + (num4 >> 1));
			int num9 = Mathf.Min(255, num2 + (num5 >> 1));
			num3 = (num8 - num6 + 1) * (num9 - num7 + 1);
			rate /= num3;
			max /= num3;
			int num10 = 0;
			for (int i = num7; i <= num9; i++)
			{
				for (int j = num6; j <= num8; j++)
				{
					num10 += this.TryDumpElectricity(j, i, rate, max);
				}
			}
			return num10;
		}
		return this.TryDumpElectricity(num, num2, rate, max);
	}

	private int TryDumpElectricity(int x, int z, int rate, int max)
	{
		int num = z * 256 + x;
		ElectricityManager.Cell cell = this.m_electricityGrid[num];
		if (cell.m_extraCharge != 0)
		{
			int num2 = Mathf.Min(rate, (int)cell.m_extraCharge);
			cell.m_currentCharge += (short)num2;
			cell.m_extraCharge -= (ushort)num2;
			rate -= num2;
		}
		rate = Mathf.Min(Mathf.Min(rate, max), (int)(32767 - cell.m_currentCharge));
		cell.m_currentCharge += (short)rate;
		this.m_electricityGrid[num] = cell;
		return rate;
	}

	public int TryFetchElectricity(Vector3 pos, int rate, int max)
	{
		if (max == 0)
		{
			return 0;
		}
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int num3 = num2 * 256 + num;
		ElectricityManager.Cell cell = this.m_electricityGrid[num3];
		if (cell.m_electrified)
		{
			rate = Mathf.Min(Mathf.Min(rate, max), 32768 + (int)cell.m_currentCharge);
			cell.m_currentCharge -= (short)rate;
		}
		else
		{
			rate = 0;
		}
		this.m_electricityGrid[num3] = cell;
		return rate;
	}

	public void CheckElectricity(Vector3 pos, out bool electricity)
	{
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int num3 = num2 * 256 + num;
		electricity = this.m_electricityGrid[num3].m_electrified;
	}

	public bool CheckConductivity(Vector3 pos)
	{
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int num3 = num2 * 256 + num;
		int conductivity = (int)this.m_electricityGrid[num3].m_conductivity;
		if (conductivity >= 1)
		{
			if (conductivity < 64)
			{
				bool flag = true;
				bool flag2 = true;
				if (num > 0 && this.m_electricityGrid[num3 - 1].m_conductivity >= 64)
				{
					flag = false;
				}
				if (num < 255 && this.m_electricityGrid[num3 + 1].m_conductivity >= 64)
				{
					flag = false;
				}
				if (num2 > 0 && this.m_electricityGrid[num3 - 256].m_conductivity >= 64)
				{
					flag2 = false;
				}
				if (num2 < 255 && this.m_electricityGrid[num3 + 256].m_conductivity >= 64)
				{
					flag2 = false;
				}
				if (flag || flag2)
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private ushort GetRootGroup(ushort group)
	{
		for (ushort mergeIndex = this.m_pulseGroups[(int)group].m_mergeIndex; mergeIndex != 65535; mergeIndex = this.m_pulseGroups[(int)group].m_mergeIndex)
		{
			group = mergeIndex;
		}
		return group;
	}

	private void MergeGroups(ushort root, ushort merged)
	{
		ElectricityManager.PulseGroup pulseGroup = this.m_pulseGroups[(int)root];
		ElectricityManager.PulseGroup pulseGroup2 = this.m_pulseGroups[(int)merged];
		pulseGroup.m_origCharge += pulseGroup2.m_origCharge;
		if (pulseGroup2.m_mergeCount != 0)
		{
			for (int i = 0; i < this.m_pulseGroupCount; i++)
			{
				if (this.m_pulseGroups[i].m_mergeIndex == merged)
				{
					this.m_pulseGroups[i].m_mergeIndex = root;
					pulseGroup2.m_origCharge -= this.m_pulseGroups[i].m_origCharge;
				}
			}
			pulseGroup.m_mergeCount += pulseGroup2.m_mergeCount;
			pulseGroup2.m_mergeCount = 0;
		}
		pulseGroup.m_curCharge += pulseGroup2.m_curCharge;
		pulseGroup2.m_curCharge = 0u;
		pulseGroup.m_mergeCount += 1;
		pulseGroup2.m_mergeIndex = root;
		this.m_pulseGroups[(int)root] = pulseGroup;
		this.m_pulseGroups[(int)merged] = pulseGroup2;
	}

	private void ConductToCell(ref ElectricityManager.Cell cell, ushort group, int x, int z, int limit)
	{
		if ((int)cell.m_conductivity >= limit)
		{
			if (cell.m_conductivity < 64)
			{
				bool flag = true;
				bool flag2 = true;
				int num = z * 256 + x;
				if (x > 0 && this.m_electricityGrid[num - 1].m_conductivity >= 64)
				{
					flag = false;
				}
				if (x < 255 && this.m_electricityGrid[num + 1].m_conductivity >= 64)
				{
					flag = false;
				}
				if (z > 0 && this.m_electricityGrid[num - 256].m_conductivity >= 64)
				{
					flag2 = false;
				}
				if (z < 255 && this.m_electricityGrid[num + 256].m_conductivity >= 64)
				{
					flag2 = false;
				}
				if (flag || flag2)
				{
					return;
				}
			}
			if (cell.m_pulseGroup == 65535)
			{
				ElectricityManager.PulseUnit pulseUnit;
				pulseUnit.m_group = group;
				pulseUnit.m_node = 0;
				pulseUnit.m_x = (byte)x;
				pulseUnit.m_z = (byte)z;
				this.m_pulseUnits[this.m_pulseUnitEnd] = pulseUnit;
				if (++this.m_pulseUnitEnd == this.m_pulseUnits.Length)
				{
					this.m_pulseUnitEnd = 0;
				}
				cell.m_pulseGroup = group;
				this.m_canContinue = true;
			}
			else
			{
				ushort rootGroup = this.GetRootGroup(cell.m_pulseGroup);
				if (rootGroup != group)
				{
					this.MergeGroups(group, rootGroup);
					cell.m_pulseGroup = group;
					this.m_canContinue = true;
				}
			}
		}
	}

	private void ConductToCells(ushort group, float worldX, float worldZ)
	{
		int num = (int)(worldX / 38.25f + 128f);
		int num2 = (int)(worldZ / 38.25f + 128f);
		if (num >= 0 && num < 256 && num2 >= 0 && num2 < 256)
		{
			int num3 = num2 * 256 + num;
			this.ConductToCell(ref this.m_electricityGrid[num3], group, num, num2, 1);
		}
	}

	private void ConductToNode(ushort nodeIndex, ref NetNode node, ushort group, float minX, float minZ, float maxX, float maxZ)
	{
		if (node.m_position.x >= minX && node.m_position.z >= minZ && node.m_position.x <= maxX && node.m_position.z <= maxZ)
		{
			NetInfo info = node.Info;
			if (info.m_class.m_service == ItemClass.Service.Electricity)
			{
				if (this.m_nodeGroups[(int)nodeIndex] == 65535)
				{
					ElectricityManager.PulseUnit pulseUnit;
					pulseUnit.m_group = group;
					pulseUnit.m_node = nodeIndex;
					pulseUnit.m_x = 0;
					pulseUnit.m_z = 0;
					this.m_pulseUnits[this.m_pulseUnitEnd] = pulseUnit;
					if (++this.m_pulseUnitEnd == this.m_pulseUnits.Length)
					{
						this.m_pulseUnitEnd = 0;
					}
					this.m_nodeGroups[(int)nodeIndex] = group;
					this.m_canContinue = true;
				}
				else
				{
					ushort rootGroup = this.GetRootGroup(this.m_nodeGroups[(int)nodeIndex]);
					if (rootGroup != group)
					{
						this.MergeGroups(group, rootGroup);
						this.m_nodeGroups[(int)nodeIndex] = group;
						this.m_canContinue = true;
					}
				}
			}
		}
	}

	private void ConductToNodes(ushort group, int cellX, int cellZ)
	{
		float num = ((float)cellX - 128f) * 38.25f;
		float num2 = ((float)cellZ - 128f) * 38.25f;
		float num3 = num + 38.25f;
		float num4 = num2 + 38.25f;
		int num5 = Mathf.Max((int)(num / 64f + 135f), 0);
		int num6 = Mathf.Max((int)(num2 / 64f + 135f), 0);
		int num7 = Mathf.Min((int)(num3 / 64f + 135f), 269);
		int num8 = Mathf.Min((int)(num4 / 64f + 135f), 269);
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_nodeGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					this.ConductToNode(num9, ref instance.m_nodes.m_buffer[(int)num9], group, num, num2, num3, num4);
					num9 = instance.m_nodes.m_buffer[(int)num9].m_nextGridNode;
					if (++num10 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private void UpdateNodeElectricity(int nodeID, int value)
	{
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		NetNode.Flags flags = instance.m_nodes.m_buffer[nodeID].m_flags;
		if ((flags & NetNode.Flags.Transition) != NetNode.Flags.None)
		{
			NetNode[] expr_47_cp_0 = instance.m_nodes.m_buffer;
			expr_47_cp_0[nodeID].m_flags = (expr_47_cp_0[nodeID].m_flags & ~NetNode.Flags.Transition);
			return;
		}
		ushort building = instance.m_nodes.m_buffer[nodeID].m_building;
		if (building != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_electricityBuffer != value)
			{
				instance2.m_buildings.m_buffer[(int)building].m_electricityBuffer = (ushort)value;
				flag = (currentMode == InfoManager.InfoMode.Electricity);
			}
			if (flag)
			{
				instance2.UpdateBuildingColors(building);
			}
		}
		NetNode.Flags flags2 = flags & ~NetNode.Flags.Electricity;
		if (value != 0)
		{
			flags2 |= NetNode.Flags.Electricity;
		}
		if (flags2 != flags)
		{
			instance.m_nodes.m_buffer[nodeID].m_flags = flags2;
			flag = (currentMode == InfoManager.InfoMode.Electricity);
		}
		if (flag)
		{
			instance.UpdateNodeColors((ushort)nodeID);
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[nodeID].GetSegment(i);
				if (segment != 0)
				{
					instance.UpdateSegmentColors(segment);
				}
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			int num = (int)(currentFrameIndex & 255u);
			if (num < 128)
			{
				if (num == 0)
				{
					this.m_pulseGroupCount = 0;
					this.m_pulseUnitStart = 0;
					this.m_pulseUnitEnd = 0;
					this.m_processedCells = 0;
					this.m_conductiveCells = 0;
					this.m_canContinue = true;
				}
				NetManager instance = Singleton<NetManager>.get_instance();
				int num2 = num * 32768 >> 7;
				int num3 = ((num + 1) * 32768 >> 7) - 1;
				for (int i = num2; i <= num3; i++)
				{
					if (instance.m_nodes.m_buffer[i].m_flags != NetNode.Flags.None)
					{
						NetInfo info = instance.m_nodes.m_buffer[i].Info;
						if (info.m_class.m_service == ItemClass.Service.Electricity)
						{
							this.UpdateNodeElectricity(i, (this.m_nodeGroups[i] == 65535) ? 0 : 1);
							this.m_conductiveCells++;
						}
					}
					this.m_nodeGroups[i] = 65535;
				}
				int num4 = num * 256 >> 7;
				int num5 = ((num + 1) * 256 >> 7) - 1;
				for (int j = num4; j <= num5; j++)
				{
					int num6 = j * 256;
					for (int k = 0; k < 256; k++)
					{
						ElectricityManager.Cell cell = this.m_electricityGrid[num6];
						if (cell.m_currentCharge > 0)
						{
							if (this.m_pulseGroupCount < 1024)
							{
								ElectricityManager.PulseGroup pulseGroup;
								pulseGroup.m_origCharge = (uint)cell.m_currentCharge;
								pulseGroup.m_curCharge = (uint)cell.m_currentCharge;
								pulseGroup.m_mergeCount = 0;
								pulseGroup.m_mergeIndex = 65535;
								pulseGroup.m_x = (byte)k;
								pulseGroup.m_z = (byte)j;
								ElectricityManager.PulseUnit pulseUnit;
								pulseUnit.m_group = (ushort)this.m_pulseGroupCount;
								pulseUnit.m_node = 0;
								pulseUnit.m_x = (byte)k;
								pulseUnit.m_z = (byte)j;
								cell.m_pulseGroup = (ushort)this.m_pulseGroupCount;
								this.m_pulseGroups[this.m_pulseGroupCount++] = pulseGroup;
								this.m_pulseUnits[this.m_pulseUnitEnd] = pulseUnit;
								if (++this.m_pulseUnitEnd == this.m_pulseUnits.Length)
								{
									this.m_pulseUnitEnd = 0;
								}
							}
							else
							{
								cell.m_pulseGroup = 65535;
							}
							cell.m_currentCharge = 0;
							this.m_conductiveCells++;
						}
						else
						{
							cell.m_pulseGroup = 65535;
							if (cell.m_conductivity >= 64)
							{
								this.m_conductiveCells++;
							}
						}
						if (cell.m_tmpElectrified != cell.m_electrified)
						{
							cell.m_electrified = cell.m_tmpElectrified;
						}
						cell.m_tmpElectrified = (cell.m_pulseGroup != 65535);
						this.m_electricityGrid[num6] = cell;
						num6++;
					}
				}
			}
			else
			{
				int num7 = (num - 127) * this.m_conductiveCells >> 7;
				if (num == 255)
				{
					num7 = 1000000000;
				}
				while (this.m_canContinue && this.m_processedCells < num7)
				{
					this.m_canContinue = false;
					int pulseUnitEnd = this.m_pulseUnitEnd;
					while (this.m_pulseUnitStart != pulseUnitEnd)
					{
						ElectricityManager.PulseUnit pulseUnit2 = this.m_pulseUnits[this.m_pulseUnitStart];
						if (++this.m_pulseUnitStart == this.m_pulseUnits.Length)
						{
							this.m_pulseUnitStart = 0;
						}
						pulseUnit2.m_group = this.GetRootGroup(pulseUnit2.m_group);
						uint num8 = this.m_pulseGroups[(int)pulseUnit2.m_group].m_curCharge;
						if (pulseUnit2.m_node == 0)
						{
							int num9 = (int)pulseUnit2.m_z * 256 + (int)pulseUnit2.m_x;
							ElectricityManager.Cell cell2 = this.m_electricityGrid[num9];
							if (cell2.m_conductivity != 0 && !cell2.m_tmpElectrified && num8 != 0u)
							{
								int num10 = Mathf.Clamp((int)(-(int)cell2.m_currentCharge), 0, (int)num8);
								num8 -= (uint)num10;
								cell2.m_currentCharge += (short)num10;
								if (cell2.m_currentCharge == 0)
								{
									cell2.m_tmpElectrified = true;
								}
								this.m_electricityGrid[num9] = cell2;
								this.m_pulseGroups[(int)pulseUnit2.m_group].m_curCharge = num8;
							}
							if (num8 != 0u)
							{
								int limit = (cell2.m_conductivity < 64) ? 64 : 1;
								this.m_processedCells++;
								if (pulseUnit2.m_z > 0)
								{
									this.ConductToCell(ref this.m_electricityGrid[num9 - 256], pulseUnit2.m_group, (int)pulseUnit2.m_x, (int)(pulseUnit2.m_z - 1), limit);
								}
								if (pulseUnit2.m_x > 0)
								{
									this.ConductToCell(ref this.m_electricityGrid[num9 - 1], pulseUnit2.m_group, (int)(pulseUnit2.m_x - 1), (int)pulseUnit2.m_z, limit);
								}
								if (pulseUnit2.m_z < 255)
								{
									this.ConductToCell(ref this.m_electricityGrid[num9 + 256], pulseUnit2.m_group, (int)pulseUnit2.m_x, (int)(pulseUnit2.m_z + 1), limit);
								}
								if (pulseUnit2.m_x < 255)
								{
									this.ConductToCell(ref this.m_electricityGrid[num9 + 1], pulseUnit2.m_group, (int)(pulseUnit2.m_x + 1), (int)pulseUnit2.m_z, limit);
								}
								this.ConductToNodes(pulseUnit2.m_group, (int)pulseUnit2.m_x, (int)pulseUnit2.m_z);
							}
							else
							{
								this.m_pulseUnits[this.m_pulseUnitEnd] = pulseUnit2;
								if (++this.m_pulseUnitEnd == this.m_pulseUnits.Length)
								{
									this.m_pulseUnitEnd = 0;
								}
							}
						}
						else if (num8 != 0u)
						{
							this.m_processedCells++;
							NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)pulseUnit2.m_node];
							if (netNode.m_flags != NetNode.Flags.None && netNode.m_buildIndex < (currentFrameIndex & 4294967168u))
							{
								this.ConductToCells(pulseUnit2.m_group, netNode.m_position.x, netNode.m_position.z);
								for (int l = 0; l < 8; l++)
								{
									ushort segment = netNode.GetSegment(l);
									if (segment != 0)
									{
										ushort startNode = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_startNode;
										ushort endNode = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_endNode;
										ushort num11 = (startNode != pulseUnit2.m_node) ? startNode : endNode;
										this.ConductToNode(num11, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num11], pulseUnit2.m_group, -100000f, -100000f, 100000f, 100000f);
									}
								}
							}
						}
						else
						{
							this.m_pulseUnits[this.m_pulseUnitEnd] = pulseUnit2;
							if (++this.m_pulseUnitEnd == this.m_pulseUnits.Length)
							{
								this.m_pulseUnitEnd = 0;
							}
						}
					}
				}
				if (num == 255)
				{
					for (int m = 0; m < this.m_pulseGroupCount; m++)
					{
						ElectricityManager.PulseGroup pulseGroup2 = this.m_pulseGroups[m];
						if (pulseGroup2.m_mergeIndex != 65535)
						{
							ElectricityManager.PulseGroup pulseGroup3 = this.m_pulseGroups[(int)pulseGroup2.m_mergeIndex];
							pulseGroup2.m_curCharge = (uint)((ulong)pulseGroup3.m_curCharge * (ulong)pulseGroup2.m_origCharge / (ulong)pulseGroup3.m_origCharge);
							pulseGroup3.m_curCharge -= pulseGroup2.m_curCharge;
							pulseGroup3.m_origCharge -= pulseGroup2.m_origCharge;
							this.m_pulseGroups[(int)pulseGroup2.m_mergeIndex] = pulseGroup3;
							this.m_pulseGroups[m] = pulseGroup2;
						}
					}
					for (int n = 0; n < this.m_pulseGroupCount; n++)
					{
						ElectricityManager.PulseGroup pulseGroup4 = this.m_pulseGroups[n];
						if (pulseGroup4.m_curCharge != 0u)
						{
							int num12 = (int)pulseGroup4.m_z * 256 + (int)pulseGroup4.m_x;
							ElectricityManager.Cell cell3 = this.m_electricityGrid[num12];
							if (cell3.m_conductivity != 0)
							{
								cell3.m_extraCharge += (ushort)Mathf.Min((int)pulseGroup4.m_curCharge, (int)(32767 - cell3.m_extraCharge));
							}
							this.m_electricityGrid[num12] = cell3;
						}
					}
				}
			}
		}
	}

	public void UpdateGrid(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)(minX / 38.25f + 128f), 0);
		int num2 = Mathf.Max((int)(minZ / 38.25f + 128f), 0);
		int num3 = Mathf.Min((int)(maxX / 38.25f + 128f), 255);
		int num4 = Mathf.Min((int)(maxZ / 38.25f + 128f), 255);
		for (int i = num2; i <= num4; i++)
		{
			int num5 = i * 256 + num;
			for (int j = num; j <= num3; j++)
			{
				this.m_electricityGrid[num5].m_conductivity = 0;
				num5++;
			}
		}
		int num6 = Mathf.Max((int)((((float)num - 128f) * 38.25f - 96f) / 64f + 135f), 0);
		int num7 = Mathf.Max((int)((((float)num2 - 128f) * 38.25f - 96f) / 64f + 135f), 0);
		int num8 = Mathf.Min((int)((((float)num3 - 128f + 1f) * 38.25f + 96f) / 64f + 135f), 269);
		int num9 = Mathf.Min((int)((((float)num4 - 128f + 1f) * 38.25f + 96f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		for (int k = num7; k <= num9; k++)
		{
			for (int l = num6; l <= num8; l++)
			{
				ushort num10 = buildingGrid[k * 270 + l];
				int num11 = 0;
				while (num10 != 0)
				{
					Building.Flags flags = buildings.m_buffer[(int)num10].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created)
					{
						BuildingInfo buildingInfo;
						int num12;
						int num13;
						buildings.m_buffer[(int)num10].GetInfoWidthLength(out buildingInfo, out num12, out num13);
						if (buildingInfo != null)
						{
							float num14 = buildingInfo.m_buildingAI.ElectricityGridRadius();
							if (num14 > 0.1f)
							{
								Vector3 position = buildings.m_buffer[(int)num10].m_position;
								float angle = buildings.m_buffer[(int)num10].m_angle;
								Vector3 vector;
								vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
								Vector3 vector2;
								vector2..ctor(vector.z, 0f, -vector.x);
								Vector3 vector3 = position - (float)(num12 * 4) * vector - (float)(num13 * 4) * vector2;
								Vector3 vector4 = position + (float)(num12 * 4) * vector - (float)(num13 * 4) * vector2;
								Vector3 vector5 = position + (float)(num12 * 4) * vector + (float)(num13 * 4) * vector2;
								Vector3 vector6 = position - (float)(num12 * 4) * vector + (float)(num13 * 4) * vector2;
								minX = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x)) - num14;
								maxX = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x)) + num14;
								minZ = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z)) - num14;
								maxZ = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z)) + num14;
								int num15 = Mathf.Max(num, (int)(minX / 38.25f + 128f));
								int num16 = Mathf.Min(num3, (int)(maxX / 38.25f + 128f));
								int num17 = Mathf.Max(num2, (int)(minZ / 38.25f + 128f));
								int num18 = Mathf.Min(num4, (int)(maxZ / 38.25f + 128f));
								for (int m = num17; m <= num18; m++)
								{
									for (int n = num15; n <= num16; n++)
									{
										Vector3 vector7;
										vector7.x = ((float)n + 0.5f - 128f) * 38.25f;
										vector7.y = position.y;
										vector7.z = ((float)m + 0.5f - 128f) * 38.25f;
										float num19 = Mathf.Max(0f, Mathf.Abs(Vector3.Dot(vector, vector7 - position)) - (float)(num12 * 4));
										float num20 = Mathf.Max(0f, Mathf.Abs(Vector3.Dot(vector2, vector7 - position)) - (float)(num13 * 4));
										float num21 = Mathf.Sqrt(num19 * num19 + num20 * num20);
										if (num21 < num14 + 19.125f)
										{
											float num22 = (num14 - num21) * 0.0130718956f + 0.25f;
											int num23 = Mathf.Min(255, Mathf.RoundToInt(num22 * 255f));
											int num24 = m * 256 + n;
											if (num23 > (int)this.m_electricityGrid[num24].m_conductivity)
											{
												this.m_electricityGrid[num24].m_conductivity = (byte)num23;
											}
										}
									}
								}
							}
						}
					}
					num10 = buildings.m_buffer[(int)num10].m_nextGridBuilding;
					if (++num11 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int num25 = num2; num25 <= num4; num25++)
		{
			int num26 = num25 * 256 + num;
			for (int num27 = num; num27 <= num3; num27++)
			{
				ElectricityManager.Cell cell = this.m_electricityGrid[num26];
				if (cell.m_conductivity == 0)
				{
					cell.m_currentCharge = 0;
					cell.m_extraCharge = 0;
					cell.m_pulseGroup = 65535;
					cell.m_tmpElectrified = false;
					cell.m_electrified = false;
					this.m_electricityGrid[num26] = cell;
				}
				num26++;
			}
		}
		this.AreaModified(num, num2, num3, num4);
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new ElectricityManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("ElectricityManager.UpdateData");
		base.UpdateData(mode);
		if (this.m_refreshGrid)
		{
			this.UpdateGrid(-100000f, -100000f, 100000f, 100000f);
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float ELECTRICITYGRID_CELL_SIZE = 38.25f;

	public const int ELECTRICITYGRID_RESOLUTION = 256;

	public const int MAX_PULSE_GROUPS = 1024;

	public const int CONDUCTIVITY_LIMIT = 64;

	[NonSerialized]
	public ushort[] m_nodeGroups;

	private ElectricityManager.Cell[] m_electricityGrid;

	private ElectricityManager.PulseGroup[] m_pulseGroups;

	private ElectricityManager.PulseUnit[] m_pulseUnits;

	private Texture2D m_electricityTexture;

	private int m_pulseGroupCount;

	private int m_pulseUnitStart;

	private int m_pulseUnitEnd;

	private int m_processedCells;

	private int m_conductiveCells;

	private bool m_canContinue;

	private bool m_refreshGrid;

	private int m_modifiedX1;

	private int m_modifiedZ1;

	private int m_modifiedX2;

	private int m_modifiedZ2;

	private bool m_electricityMapVisible;

	public struct Cell
	{
		public short m_currentCharge;

		public ushort m_extraCharge;

		public ushort m_pulseGroup;

		public byte m_conductivity;

		public bool m_tmpElectrified;

		public bool m_electrified;
	}

	public struct PulseGroup
	{
		public uint m_origCharge;

		public uint m_curCharge;

		public ushort m_mergeIndex;

		public ushort m_mergeCount;

		public byte m_x;

		public byte m_z;
	}

	public struct PulseUnit
	{
		public ushort m_group;

		public ushort m_node;

		public byte m_x;

		public byte m_z;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "ElectricityManager");
			ElectricityManager instance = Singleton<ElectricityManager>.get_instance();
			ElectricityManager.Cell[] electricityGrid = instance.m_electricityGrid;
			int num = electricityGrid.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				@byte.Write(electricityGrid[i].m_conductivity);
			}
			@byte.EndWrite();
			EncodedArray.Short @short = EncodedArray.Short.BeginWrite(s);
			for (int j = 0; j < num; j++)
			{
				if (electricityGrid[j].m_conductivity != 0)
				{
					@short.Write(electricityGrid[j].m_currentCharge);
				}
			}
			@short.EndWrite();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int k = 0; k < num; k++)
			{
				if (electricityGrid[k].m_conductivity != 0)
				{
					uShort.Write(electricityGrid[k].m_extraCharge);
				}
			}
			uShort.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int l = 0; l < num; l++)
			{
				if (electricityGrid[l].m_conductivity != 0)
				{
					uShort2.Write(electricityGrid[l].m_pulseGroup);
				}
			}
			uShort2.EndWrite();
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginWrite(s);
			for (int m = 0; m < num; m++)
			{
				if (electricityGrid[m].m_conductivity != 0)
				{
					@bool.Write(electricityGrid[m].m_electrified);
				}
			}
			@bool.EndWrite();
			EncodedArray.Bool bool2 = EncodedArray.Bool.BeginWrite(s);
			for (int n = 0; n < num; n++)
			{
				if (electricityGrid[n].m_conductivity != 0)
				{
					bool2.Write(electricityGrid[n].m_tmpElectrified);
				}
			}
			bool2.EndWrite();
			s.WriteUInt16((uint)instance.m_pulseGroupCount);
			for (int num2 = 0; num2 < instance.m_pulseGroupCount; num2++)
			{
				s.WriteUInt32(instance.m_pulseGroups[num2].m_origCharge);
				s.WriteUInt32(instance.m_pulseGroups[num2].m_curCharge);
				s.WriteUInt16((uint)instance.m_pulseGroups[num2].m_mergeIndex);
				s.WriteUInt16((uint)instance.m_pulseGroups[num2].m_mergeCount);
				s.WriteUInt8((uint)instance.m_pulseGroups[num2].m_x);
				s.WriteUInt8((uint)instance.m_pulseGroups[num2].m_z);
			}
			int num3 = instance.m_pulseUnitEnd - instance.m_pulseUnitStart;
			if (num3 < 0)
			{
				num3 += instance.m_pulseUnits.Length;
			}
			s.WriteUInt16((uint)num3);
			int num4 = instance.m_pulseUnitStart;
			while (num4 != instance.m_pulseUnitEnd)
			{
				s.WriteUInt16((uint)instance.m_pulseUnits[num4].m_group);
				s.WriteUInt16((uint)instance.m_pulseUnits[num4].m_node);
				s.WriteUInt8((uint)instance.m_pulseUnits[num4].m_x);
				s.WriteUInt8((uint)instance.m_pulseUnits[num4].m_z);
				if (++num4 >= instance.m_pulseUnits.Length)
				{
					num4 = 0;
				}
			}
			EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginWrite(s);
			for (int num5 = 0; num5 < 32768; num5++)
			{
				uShort3.Write(instance.m_nodeGroups[num5]);
			}
			uShort3.EndWrite();
			s.WriteInt32(instance.m_processedCells);
			s.WriteInt32(instance.m_conductiveCells);
			s.WriteBool(instance.m_canContinue);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "ElectricityManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "ElectricityManager");
			ElectricityManager instance = Singleton<ElectricityManager>.get_instance();
			ElectricityManager.Cell[] electricityGrid = instance.m_electricityGrid;
			int num = electricityGrid.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				electricityGrid[i].m_conductivity = @byte.Read();
			}
			@byte.EndRead();
			if (s.get_version() >= 8u && s.get_version() <= 75u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int j = 0; j < num; j++)
				{
					electricityGrid[j].m_tmpElectrified = (byte2.Read() != 0);
				}
				byte2.EndRead();
			}
			else if (s.get_version() < 8u)
			{
				for (int k = 0; k < num; k++)
				{
					electricityGrid[k].m_tmpElectrified = false;
				}
			}
			EncodedArray.Short @short = EncodedArray.Short.BeginRead(s);
			for (int l = 0; l < num; l++)
			{
				if (electricityGrid[l].m_conductivity != 0)
				{
					electricityGrid[l].m_currentCharge = @short.Read();
				}
				else
				{
					electricityGrid[l].m_currentCharge = 0;
				}
			}
			@short.EndRead();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int m = 0; m < num; m++)
			{
				if (electricityGrid[m].m_conductivity != 0)
				{
					electricityGrid[m].m_extraCharge = uShort.Read();
				}
				else
				{
					electricityGrid[m].m_extraCharge = 0;
				}
			}
			uShort.EndRead();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int n = 0; n < num; n++)
			{
				if (electricityGrid[n].m_conductivity != 0)
				{
					electricityGrid[n].m_pulseGroup = uShort2.Read();
				}
				else
				{
					electricityGrid[n].m_pulseGroup = 65535;
				}
			}
			uShort2.EndRead();
			if (s.get_version() == 75u)
			{
				EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginRead(s);
				for (int num2 = 0; num2 < num; num2++)
				{
					if (electricityGrid[num2].m_conductivity != 0)
					{
						uShort3.Read();
					}
				}
				uShort3.EndRead();
			}
			instance.m_refreshGrid = (s.get_version() < 76u);
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginRead(s);
			for (int num3 = 0; num3 < num; num3++)
			{
				if (electricityGrid[num3].m_conductivity != 0 || (s.get_version() < 76u && electricityGrid[num3].m_tmpElectrified))
				{
					electricityGrid[num3].m_electrified = @bool.Read();
				}
				else
				{
					electricityGrid[num3].m_electrified = false;
				}
			}
			@bool.EndRead();
			EncodedArray.Bool bool2 = EncodedArray.Bool.BeginRead(s);
			for (int num4 = 0; num4 < num; num4++)
			{
				if (electricityGrid[num4].m_conductivity != 0 || (s.get_version() < 76u && electricityGrid[num4].m_tmpElectrified))
				{
					electricityGrid[num4].m_tmpElectrified = bool2.Read();
				}
				else
				{
					electricityGrid[num4].m_tmpElectrified = false;
				}
			}
			bool2.EndRead();
			instance.m_pulseGroupCount = (int)s.ReadUInt16();
			for (int num5 = 0; num5 < instance.m_pulseGroupCount; num5++)
			{
				instance.m_pulseGroups[num5].m_origCharge = s.ReadUInt32();
				instance.m_pulseGroups[num5].m_curCharge = s.ReadUInt32();
				instance.m_pulseGroups[num5].m_mergeIndex = (ushort)s.ReadUInt16();
				instance.m_pulseGroups[num5].m_mergeCount = (ushort)s.ReadUInt16();
				instance.m_pulseGroups[num5].m_x = (byte)s.ReadUInt8();
				instance.m_pulseGroups[num5].m_z = (byte)s.ReadUInt8();
			}
			int num6 = (int)s.ReadUInt16();
			instance.m_pulseUnitStart = 0;
			instance.m_pulseUnitEnd = num6 % instance.m_pulseUnits.Length;
			for (int num7 = 0; num7 < num6; num7++)
			{
				instance.m_pulseUnits[num7].m_group = (ushort)s.ReadUInt16();
				if (s.get_version() >= 8u)
				{
					instance.m_pulseUnits[num7].m_node = (ushort)s.ReadUInt16();
				}
				else
				{
					instance.m_pulseUnits[num7].m_node = 0;
				}
				instance.m_pulseUnits[num7].m_x = (byte)s.ReadUInt8();
				instance.m_pulseUnits[num7].m_z = (byte)s.ReadUInt8();
			}
			if (s.get_version() >= 202u)
			{
				EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginRead(s);
				for (int num8 = 0; num8 < 32768; num8++)
				{
					instance.m_nodeGroups[num8] = uShort4.Read();
				}
				uShort4.EndRead();
			}
			else if (s.get_version() >= 8u)
			{
				EncodedArray.UShort uShort5 = EncodedArray.UShort.BeginRead(s);
				for (int num9 = 0; num9 < 16384; num9++)
				{
					instance.m_nodeGroups[num9] = uShort5.Read();
				}
				for (int num10 = 16384; num10 < 32768; num10++)
				{
					instance.m_nodeGroups[num10] = 0;
				}
				uShort5.EndRead();
			}
			else
			{
				for (int num11 = 0; num11 < 32768; num11++)
				{
					instance.m_nodeGroups[num11] = 65535;
				}
			}
			instance.m_processedCells = s.ReadInt32();
			instance.m_conductiveCells = s.ReadInt32();
			instance.m_canContinue = s.ReadBool();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "ElectricityManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "ElectricityManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			ElectricityManager instance = Singleton<ElectricityManager>.get_instance();
			instance.AreaModified(0, 0, 255, 255);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "ElectricityManager");
		}
	}
}
