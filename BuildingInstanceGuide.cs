﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class BuildingInstanceGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, ushort buildingID)
	{
		if (this.m_buildingID != 0 && !this.Validate())
		{
			this.Deactivate();
		}
		if (this.m_buildingID == 0 || this.m_buildingID == buildingID)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			this.m_buildingID = buildingID;
			this.m_buildIndex = instance.m_buildings.m_buffer[(int)this.m_buildingID].m_buildIndex;
			if (base.CanActivate(guideInfo))
			{
				Vector3 position = instance.m_buildings.m_buffer[(int)this.m_buildingID].m_position;
				if (Singleton<SimulationManager>.get_instance().m_simulationView.Intersect(position, -10f))
				{
					GuideTriggerBase.ActivationInfo activationInfo = new BuildingInstanceGuide.BuildingInstanceActivationInfo(guideInfo, buildingID);
					base.Activate(guideInfo, activationInfo);
				}
			}
		}
	}

	public void Deactivate(ushort buildingID, bool soft)
	{
		if (this.m_buildingID == buildingID)
		{
			if (soft)
			{
				this.m_buildingID = 0;
				this.m_buildIndex = 0u;
				this.m_activationFrame = 0u;
				this.m_activationCount = 0;
			}
			else
			{
				this.Deactivate();
			}
		}
	}

	public override void Deactivate()
	{
		this.m_buildingID = 0;
		this.m_buildIndex = 0u;
		base.Deactivate();
	}

	public override void Disable()
	{
		this.m_buildingID = 0;
		this.m_buildIndex = 0u;
		base.Disable();
	}

	public override bool Validate()
	{
		if (this.m_buildingID == 0)
		{
			return false;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		return instance.m_buildings.m_buffer[(int)this.m_buildingID].m_flags != Building.Flags.None && instance.m_buildings.m_buffer[(int)this.m_buildingID].m_buildIndex == this.m_buildIndex && base.Validate();
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteUInt32(this.m_buildIndex);
		s.WriteUInt16((uint)this.m_buildingID);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_buildIndex = s.ReadUInt32();
		this.m_buildingID = (ushort)s.ReadUInt16();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public uint m_buildIndex;

	public ushort m_buildingID;

	public class BuildingInstanceActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public BuildingInstanceActivationInfo(GuideInfo guideInfo, ushort buildingID) : base(guideInfo)
		{
			this.m_buildingID = buildingID;
		}

		public override string GetName()
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_buildingID].Info;
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<BuildingInfo>.PrefabName((uint)info.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_buildingID].Info;
			string icon = this.m_guideInfo.m_icon;
			string thumbnail = info.m_Thumbnail;
			return StringUtils.SafeFormat(icon, thumbnail);
		}

		public override string FormatText(string pattern)
		{
			string buildingName = Singleton<BuildingManager>.get_instance().GetBuildingName(this.m_buildingID, InstanceID.Empty);
			return StringUtils.SafeFormat(pattern, buildingName);
		}

		public override IUITag GetTag()
		{
			return new BuildingTutorialTag(this.m_buildingID);
		}

		protected ushort m_buildingID;
	}
}
