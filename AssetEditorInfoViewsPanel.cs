﻿using System;
using ColossalFramework;

public class AssetEditorInfoViewsPanel : InfoViewsPanel
{
	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < AssetEditorInfoViewsPanel.kResources.Length; i++)
		{
			base.SpawnButtonEntry(AssetEditorInfoViewsPanel.kResources[i].get_enumName(), "InfoIcon", "INFOVIEWS", i, true);
		}
	}

	protected override void ShowSelectedIndex()
	{
		if (Singleton<InfoManager>.get_exists())
		{
			if (Singleton<InfoManager>.get_instance().NextMode != this.m_cachedMode)
			{
				this.m_cachedMode = Singleton<InfoManager>.get_instance().NextMode;
				this.m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(this.m_cachedMode, "AssetEditor");
			}
			base.selectedIndex = this.m_cachedIndex;
		}
		base.ShowSelectedIndex();
	}

	protected override void OnButtonClicked(int index)
	{
		if (index >= 0 && index < AssetEditorInfoViewsPanel.kResources.Length)
		{
			base.CloseToolbar();
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(AssetEditorInfoViewsPanel.kResources[index].get_enumValue(), InfoManager.SubInfoMode.Default);
			}
		}
		else
		{
			base.CloseToolbar();
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
			}
		}
	}

	private static readonly PositionData<InfoManager.InfoMode>[] kResources = Utils.GetOrderedEnumData<InfoManager.InfoMode>("AssetEditor");

	private InfoManager.InfoMode m_cachedMode;

	private int m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(InfoManager.InfoMode.None, "AssetEditor");
}
