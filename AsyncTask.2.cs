﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class AsyncTask<T> : AsyncTaskBase
{
	public AsyncTask(IEnumerator<T> action, string taskName = null) : base(taskName)
	{
		this.m_Action = action;
	}

	public T returnValue
	{
		get
		{
			return this.m_ReturnValue;
		}
	}

	public override void Execute()
	{
		try
		{
			this.m_Progress = 0f;
			while (this.m_Action.MoveNext())
			{
				this.m_ReturnValue = this.m_Action.Current;
				this.m_Progress += this.m_CachedStepCount;
			}
			if (this.m_Progress != 1f)
			{
				Debug.LogError(string.Concat(new object[]
				{
					"Progress steps did not match the amount of iterations for 'task ",
					this.name,
					"' [",
					this.m_Action,
					"]"
				}));
			}
			this.m_Progress = 1f;
		}
		catch (Exception ex)
		{
			this.m_Progress = 100f;
			UIView.ForwardException(ex);
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Tool error: " + ex.Message + "\n" + ex.StackTrace);
		}
		finally
		{
			this.m_Action.Dispose();
		}
	}

	private IEnumerator<T> m_Action;

	private T m_ReturnValue;
}
