﻿using System;
using ColossalFramework.IO;
using UnityEngine;

public struct WaterSource
{
	public void Serialize(DataSerializer s)
	{
		s.WriteVector3(this.m_inputPosition);
		s.WriteVector3(this.m_outputPosition);
		s.WriteUInt16((uint)this.m_type);
		s.WriteUInt16((uint)this.m_target);
		s.WriteUInt24(this.m_water);
		s.WriteUInt24(this.m_pollution);
		s.WriteUInt24(this.m_inputRate);
		s.WriteUInt24(this.m_outputRate);
		s.WriteUInt24(this.m_flow);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_inputPosition = s.ReadVector3();
		if (s.get_version() >= 184u)
		{
			this.m_outputPosition = s.ReadVector3();
		}
		else
		{
			this.m_outputPosition = this.m_inputPosition;
		}
		this.m_type = (ushort)s.ReadUInt16();
		if (s.get_version() < 184u)
		{
			if (s.get_version() >= 95u)
			{
				this.m_inputRate = s.ReadUInt24();
			}
			else
			{
				this.m_inputRate = s.ReadUInt16();
			}
			if (this.m_type != 1)
			{
				this.m_inputRate = 0u;
			}
			this.m_outputRate = this.m_inputRate;
		}
		this.m_target = (ushort)s.ReadUInt16();
		if (s.get_version() >= 184u)
		{
			this.m_water = s.ReadUInt24();
			this.m_pollution = s.ReadUInt24();
			this.m_inputRate = s.ReadUInt24();
			this.m_outputRate = s.ReadUInt24();
			this.m_flow = s.ReadUInt24();
		}
		else if (s.get_version() >= 95u)
		{
			s.ReadUInt24();
			s.ReadUInt24();
			s.ReadUInt24();
			s.ReadUInt24();
			s.ReadUInt24();
			this.m_water = 0u;
			this.m_pollution = 0u;
			this.m_flow = 0u;
		}
		else
		{
			s.ReadUInt16();
			s.ReadUInt16();
			s.ReadUInt16();
			s.ReadUInt16();
			s.ReadUInt16();
			this.m_water = 0u;
			this.m_pollution = 0u;
			this.m_flow = 0u;
		}
	}

	public const ushort TYPE_NONE = 0;

	public const ushort TYPE_NATURAL = 1;

	public const ushort TYPE_FACILITY = 2;

	public const ushort TYPE_CLEANER = 3;

	public Vector3 m_inputPosition;

	public Vector3 m_outputPosition;

	public ushort m_type;

	public ushort m_target;

	public uint m_water;

	public uint m_pollution;

	public uint m_inputRate;

	public uint m_outputRate;

	public uint m_flow;
}
