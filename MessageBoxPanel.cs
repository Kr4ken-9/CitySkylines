﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class MessageBoxPanel : MenuPanel
{
	public static void ShowModal(string localeID, UIView.ModalPoppedReturnCallback callback, bool showClose = false, params string[] format)
	{
		MessageBoxPanel messageBoxPanel = UIView.get_library().ShowModal<MessageBoxPanel>("MessageBoxPanel", callback);
		messageBoxPanel.SetValues(localeID, showClose, format);
	}

	private void SetValues(string localeID, bool showClose, params string[] format)
	{
		base.Find<UILabel>("Message").set_text(StringUtils.SafeFormat(Locale.Get(localeID, "Message"), format));
		base.Find<UILabel>("Caption").set_text(Locale.Get(localeID, "Title"));
		UIButton uIButton = base.Find<UIButton>("Close");
		if (uIButton != null)
		{
			uIButton.set_isVisible(showClose);
		}
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Ok");
			uIButton.Focus();
		}
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	protected virtual void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				p.Use();
				this.OnClosed();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnOK();
			}
		}
	}

	public virtual void OnOK()
	{
		UIView.get_library().Hide(base.GetType().Name, 0);
	}
}
