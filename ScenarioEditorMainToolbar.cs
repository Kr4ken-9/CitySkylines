﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class ScenarioEditorMainToolbar : MainToolbar
{
	public static ScenarioSettingsPanel scenarioSettingsPanel
	{
		get
		{
			if (ScenarioEditorMainToolbar.m_ScenarioSettingsPanel == null)
			{
				UIComponent uIComponent = UIView.Find("ScenarioSettingsPanel");
				if (uIComponent != null)
				{
					ScenarioEditorMainToolbar.m_ScenarioSettingsPanel = uIComponent.GetComponent<ScenarioSettingsPanel>();
				}
			}
			return ScenarioEditorMainToolbar.m_ScenarioSettingsPanel;
		}
	}

	public static TriggerPanel triggerPanel
	{
		get
		{
			if (ScenarioEditorMainToolbar.m_TriggerPanel == null)
			{
				UIComponent uIComponent = UIView.Find("TriggerPanel");
				if (uIComponent != null)
				{
					ScenarioEditorMainToolbar.m_TriggerPanel = uIComponent.GetComponent<TriggerPanel>();
				}
			}
			return ScenarioEditorMainToolbar.m_TriggerPanel;
		}
	}

	private void Start()
	{
		this.ForcePauseSimulation();
	}

	private void ForcePauseSimulation()
	{
		if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = true;
			});
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHideSpecialPanels));
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHideSpecialPanels));
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		UITabstrip strip = base.get_component().Find<UITabstrip>("MainToolstrip");
		base.SpawnSubEntry(strip, "Terrain", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "Forest", "MAPEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSeparator(strip);
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			base.SpawnSubEntry(strip, "Disasters", "SCENARIOEDITOR_TOOL", null, "ToolbarIcon", true);
		}
		UIButton uIButton = base.SpawnButtonEntry(strip, "Trigger", "SCENARIOEDITOR_TOOL", "ToolbarIcon", true);
		this.m_TriggerPanelIndex = uIButton.get_zOrder();
		base.SpawnSeparator(strip);
		UIButton uIButton2 = base.SpawnButtonEntry(strip, "ScenarioSettings", "SCENARIOEDITOR_TOOL", "ToolbarIcon", true);
		this.m_ScenarioSettingsIndex = uIButton2.get_zOrder();
	}

	private void ShowHideSpecialPanels(UIComponent comp, int index)
	{
		UITabstrip uITabstrip = comp as UITabstrip;
		if (index == this.m_ScenarioSettingsIndex)
		{
			ScenarioEditorMainToolbar.scenarioSettingsPanel.Show();
			ScenarioEditorMainToolbar.triggerPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
			AddingDisasterPanel.instance.Hide();
			SelectingDisasterPanel.instance.Hide();
		}
		else if (index == this.m_TriggerPanelIndex)
		{
			ScenarioEditorMainToolbar.scenarioSettingsPanel.Hide();
			ScenarioEditorMainToolbar.triggerPanel.Show();
			uITabstrip.get_tabContainer().Hide();
			AddingDisasterPanel.instance.Hide();
			SelectingDisasterPanel.instance.Hide();
		}
		else
		{
			ScenarioEditorMainToolbar.scenarioSettingsPanel.Hide();
			ScenarioEditorMainToolbar.triggerPanel.Hide();
			uITabstrip.get_tabContainer().Show();
		}
	}

	private static ScenarioSettingsPanel m_ScenarioSettingsPanel;

	private static TriggerPanel m_TriggerPanel;

	private int m_ScenarioSettingsIndex;

	private int m_TriggerPanelIndex;
}
