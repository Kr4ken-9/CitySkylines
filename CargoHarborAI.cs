﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

[Serializable]
public class CargoHarborAI : CargoStationAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		float num = this.m_info.m_generatedInfo.m_max.z - 7f;
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				if (this.m_info.m_paths[i].m_netInfo != null && this.m_info.m_paths[i].m_netInfo.m_class.m_service == ItemClass.Service.Road && this.m_info.m_paths[i].m_nodes != null)
				{
					for (int j = 0; j < this.m_info.m_paths[i].m_nodes.Length; j++)
					{
						num = Mathf.Min(num, -16f - this.m_info.m_paths[i].m_netInfo.m_halfWidth - this.m_info.m_paths[i].m_nodes[j].z);
					}
				}
			}
		}
		this.m_quayOffset = num;
	}

	public override void RenderBuildOverlay(RenderManager.CameraInfo cameraInfo, Color color, Vector3 position, float angle, Segment3 connectionSegment)
	{
		if (this.m_transportInfo != null && this.m_transportInfo.m_transportType == TransportInfo.TransportType.Ship)
		{
			Material connectionMaterial = this.m_transportInfo.m_connectionMaterial2;
			if (connectionMaterial != null)
			{
				Segment3 segment;
				segment.a = Building.CalculatePosition(position, angle, this.m_spawnPosition);
				segment.a.y = connectionSegment.a.y;
				segment.b = connectionSegment.a;
				HarborAI.RenderHarborPath(cameraInfo, color, connectionMaterial, segment);
				HarborAI.RenderHarborPath(cameraInfo, color, connectionMaterial, connectionSegment);
			}
		}
		else if (this.m_transportInfo2 != null && this.m_transportInfo2.m_transportType == TransportInfo.TransportType.Ship)
		{
			Material connectionMaterial2 = this.m_transportInfo2.m_connectionMaterial2;
			if (connectionMaterial2 != null)
			{
				Segment3 segment2;
				segment2.a = Building.CalculatePosition(position, angle, this.m_spawnPosition2);
				segment2.a.y = connectionSegment.a.y;
				segment2.b = connectionSegment.a;
				HarborAI.RenderHarborPath(cameraInfo, color, connectionMaterial2, segment2);
				HarborAI.RenderHarborPath(cameraInfo, color, connectionMaterial2, connectionSegment);
			}
		}
		base.RenderBuildOverlay(cameraInfo, color, position, angle, connectionSegment);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		if (this.CountAnimals(buildingID, ref buildingData) < this.m_animalCount)
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		Vector3 vector;
		Vector3 vector2;
		bool flag;
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline && BuildingTool.SnapToCanal(position, out vector, out vector2, out flag, 40f, false))
		{
			angle = Mathf.Atan2(vector2.x, -vector2.z);
			vector += vector2 * this.m_quayOffset;
			position.x = vector.x;
			position.z = vector.z;
			if (!flag)
			{
				toolErrors |= ToolBase.ToolErrors.ShoreNotFound;
			}
		}
		toolErrors |= base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		if (position.y - waterHeight > 32f)
		{
			toolErrors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		Vector3 vector3 = Building.CalculatePosition(position, angle, this.m_connectionOffset);
		Vector3 b = vector3;
		uint num;
		byte b2;
		if (ShipDockAI.FindConnectionPath(ref b, out num, out b2))
		{
			vector3.y = b.y;
			connectionSegment.a = vector3;
			connectionSegment.b = b;
			if (!Singleton<TerrainManager>.get_instance().HasWater(Segment2.XZ(connectionSegment), 50f, false))
			{
				toolErrors |= ToolBase.ToolErrors.CannotConnect;
			}
		}
		else
		{
			toolErrors |= ToolBase.ToolErrors.CannotConnect;
		}
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			if ((toolErrors & ToolBase.ToolErrors.CannotConnect) != ToolBase.ToolErrors.None)
			{
				Singleton<BuildingManager>.get_instance().m_harborPlacement.Activate(properties.m_harborPlacement);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_harborPlacement.Deactivate();
			}
		}
		return toolErrors;
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline)
		{
			min = 20f / Mathf.Max(22f, (float)this.m_info.m_cellLength * 8f);
			max = 1f;
			return true;
		}
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	public override void PlacementFailed()
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_buildNextToWater.Activate(properties.m_buildNextToWater);
		}
	}

	public override void PlacementSucceeded()
	{
		Singleton<BuildingManager>.get_instance().m_buildNextToWater.Deactivate();
		Singleton<BuildingManager>.get_instance().m_harborPlacement.Disable();
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num3);
			target.z += Mathf.Sin(num3);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 300;
			int num2 = data.Length * 300;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = this.m_info.m_size.y + (float)randomizer.Int32(1000u) * 0.1f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, ItemClass.Service.Garbage, ItemClass.SubService.None);
		ushort num;
		if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num2++;
			}
			num = nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	public Vector3 m_connectionOffset;

	private int m_animalCount = 5;

	[NonSerialized]
	protected float m_quayOffset;
}
