﻿using System;

public class EditRoadPlacementGroupPanel : GeneratedGroupPanel
{
	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("EditRoadPlacement");
		return true;
	}
}
