﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct ZoneBlock
{
	public int RowCount
	{
		get
		{
			return (int)((this.m_flags & 65280u) >> 8);
		}
		set
		{
			this.m_flags = ((this.m_flags & 4294902015u) | (uint)((uint)Mathf.Clamp(value, 0, 8) << 8));
		}
	}

	private ulong OverlapQuad(Quad2 quad)
	{
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = quad.Min();
		Vector2 vector4 = quad.Max();
		Vector2 vector5 = VectorUtils.XZ(this.m_position);
		ulong num = 0uL;
		for (int i = 0; i < rowCount; i++)
		{
			Vector2 vector6 = ((float)i - 3.9f) * vector2;
			Vector2 vector7 = ((float)i - 3.1f) * vector2;
			int num2 = 0;
			while ((long)num2 < 4L)
			{
				Vector2 vector8 = ((float)num2 - 3.9f) * vector;
				Vector2 vector9 = ((float)num2 - 3.1f) * vector;
				Vector2 vector10 = vector5 + (vector9 + vector8 + vector7 + vector6) * 0.5f;
				if (vector3.x <= vector10.x + 6f && vector3.y <= vector10.y + 6f && vector10.x - 6f <= vector4.x && vector10.y - 6f <= vector4.y)
				{
					Quad2 quad2 = default(Quad2);
					quad2.a = vector5 + vector8 + vector6;
					quad2.b = vector5 + vector9 + vector6;
					quad2.c = vector5 + vector9 + vector7;
					quad2.d = vector5 + vector8 + vector7;
					if (quad.Intersect(quad2))
					{
						num |= 1uL << (i << 3 | num2);
					}
				}
				num2++;
			}
		}
		return num;
	}

	private void CalculateImplementation1(ushort blockID, ushort segmentID, ref NetSegment data, ref ulong valid, float minX, float minZ, float maxX, float maxZ)
	{
		if (data.m_blockStartLeft == blockID || data.m_blockStartRight == blockID)
		{
			return;
		}
		if (data.m_blockEndLeft == blockID || data.m_blockEndRight == blockID)
		{
			return;
		}
		NetInfo info = data.Info;
		if (!info.m_canCollide)
		{
			return;
		}
		float collisionHalfWidth = info.m_netAI.GetCollisionHalfWidth();
		NetNode[] buffer = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
		Bezier3 bezier = default(Bezier3);
		bezier.a = buffer[(int)data.m_startNode].m_position;
		bezier.d = buffer[(int)data.m_endNode].m_position;
		NetSegment.CalculateMiddlePoints(bezier.a, data.m_startDirection, bezier.d, data.m_endDirection, true, true, out bezier.b, out bezier.c);
		Bezier2 bezier2 = Bezier2.XZ(bezier);
		Vector2 vector = bezier2.Min() + new Vector2(-collisionHalfWidth, -collisionHalfWidth);
		Vector2 vector2 = bezier2.Max() + new Vector2(collisionHalfWidth, collisionHalfWidth);
		if (vector.x <= maxX && vector.y <= maxZ && minX <= vector2.x && minZ <= vector2.y)
		{
			int rowCount = this.RowCount;
			Vector2 vector3 = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
			Vector2 vector4;
			vector4..ctor(vector3.y, -vector3.x);
			Vector2 vector5 = VectorUtils.XZ(this.m_position);
			Quad2 quad = default(Quad2);
			quad.a = vector5 - 4f * vector3 - 4f * vector4;
			quad.b = vector5 + 4f * vector3 - 4f * vector4;
			quad.c = vector5 + 4f * vector3 + (float)(rowCount - 4) * vector4;
			quad.d = vector5 - 4f * vector3 + (float)(rowCount - 4) * vector4;
			int num = 8;
			float num2;
			float num3;
			info.m_netAI.GetTerrainModifyRange(out num2, out num3);
			num2 *= 0.5f;
			num3 = 1f - (1f - num3) * 0.5f;
			float num4 = num2;
			Vector2 vector6 = bezier2.Position(num4);
			Vector2 vector7 = bezier2.Tangent(num4);
			Vector2 vector8;
			vector8..ctor(-vector7.y, vector7.x);
			vector7 = vector8.get_normalized() * collisionHalfWidth;
			Quad2 quad2;
			quad2.a = vector6 + vector7;
			quad2.d = vector6 - vector7;
			float endRadius = info.m_netAI.GetEndRadius();
			if (info.m_clipSegmentEnds && endRadius != 0f && num2 == 0f && (buffer[(int)data.m_startNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) != NetNode.Flags.None)
			{
				Vector2 vector9 = vector6;
				vector9.x += vector7.x * 0.8f - vector7.y * 0.6f * endRadius / collisionHalfWidth;
				vector9.y += vector7.y * 0.8f + vector7.x * 0.6f * endRadius / collisionHalfWidth;
				Vector2 vector10 = vector6;
				vector10.x -= vector7.x * 0.8f + vector7.y * 0.6f * endRadius / collisionHalfWidth;
				vector10.y -= vector7.y * 0.8f - vector7.x * 0.6f * endRadius / collisionHalfWidth;
				Vector2 vector11 = vector6;
				vector11.x += vector7.x * 0.3f - vector7.y * endRadius / collisionHalfWidth;
				vector11.y += vector7.y * 0.3f + vector7.x * endRadius / collisionHalfWidth;
				Vector2 vector12 = vector6;
				vector12.x -= vector7.x * 0.3f + vector7.y * endRadius / collisionHalfWidth;
				vector12.y -= vector7.y * 0.3f - vector7.x * endRadius / collisionHalfWidth;
				Quad2 quad3;
				quad3..ctor(quad2.a, quad2.d, vector10, vector9);
				Quad2 quad4;
				quad4..ctor(vector9, vector10, vector12, vector11);
				Vector2 vector13 = quad3.Min();
				Vector2 vector14 = quad3.Max();
				Vector2 vector15 = quad4.Min();
				Vector2 vector16 = quad4.Max();
				if (vector13.x <= maxX && vector13.y <= maxZ && minX <= vector14.x && minZ <= vector14.y && quad.Intersect(quad3))
				{
					valid &= ~this.OverlapQuad(quad3);
				}
				if (vector15.x <= maxX && vector15.y <= maxZ && minX <= vector16.x && minZ <= vector16.y && quad.Intersect(quad4))
				{
					valid &= ~this.OverlapQuad(quad4);
				}
			}
			for (int i = 1; i <= num; i++)
			{
				num4 = num2 + (num3 - num2) * (float)i / (float)num;
				vector6 = bezier2.Position(num4);
				vector7 = bezier2.Tangent(num4);
				Vector2 vector17;
				vector17..ctor(-vector7.y, vector7.x);
				vector7 = vector17.get_normalized() * collisionHalfWidth;
				quad2.b = vector6 + vector7;
				quad2.c = vector6 - vector7;
				Vector2 vector18 = quad2.Min();
				Vector2 vector19 = quad2.Max();
				if (vector18.x <= maxX && vector18.y <= maxZ && minX <= vector19.x && minZ <= vector19.y && quad.Intersect(quad2))
				{
					valid &= ~this.OverlapQuad(quad2);
				}
				quad2.a = quad2.b;
				quad2.d = quad2.c;
			}
			if (info.m_clipSegmentEnds && endRadius != 0f && num3 == 1f && (buffer[(int)data.m_endNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) != NetNode.Flags.None)
			{
				Vector2 vector20 = vector6;
				vector20.x += vector7.x * 0.8f + vector7.y * 0.6f * endRadius / collisionHalfWidth;
				vector20.y += vector7.y * 0.8f - vector7.x * 0.6f * endRadius / collisionHalfWidth;
				Vector2 vector21 = vector6;
				vector21.x -= vector7.x * 0.8f - vector7.y * 0.6f * endRadius / collisionHalfWidth;
				vector21.y -= vector7.y * 0.8f + vector7.x * 0.6f * endRadius / collisionHalfWidth;
				Vector2 vector22 = vector6;
				vector22.x += vector7.x * 0.3f + vector7.y * endRadius / collisionHalfWidth;
				vector22.y += vector7.y * 0.3f - vector7.x * endRadius / collisionHalfWidth;
				Vector2 vector23 = vector6;
				vector23.x -= vector7.x * 0.3f - vector7.y * endRadius / collisionHalfWidth;
				vector23.y -= vector7.y * 0.3f + vector7.x * endRadius / collisionHalfWidth;
				Quad2 quad5;
				quad5..ctor(quad2.a, vector20, vector21, quad2.d);
				Quad2 quad6;
				quad6..ctor(vector20, vector22, vector23, vector21);
				Vector2 vector24 = quad5.Min();
				Vector2 vector25 = quad5.Max();
				Vector2 vector26 = quad6.Min();
				Vector2 vector27 = quad6.Max();
				if (vector24.x <= maxX && vector24.y <= maxZ && minX <= vector25.x && minZ <= vector25.y && quad.Intersect(quad5))
				{
					valid &= ~this.OverlapQuad(quad5);
				}
				if (vector26.x <= maxX && vector26.y <= maxZ && minX <= vector27.x && minZ <= vector27.y && quad.Intersect(quad6))
				{
					valid &= ~this.OverlapQuad(quad6);
				}
			}
		}
	}

	public void CalculateBlock1(ushort blockID)
	{
		if ((this.m_flags & 3u) != 1u)
		{
			return;
		}
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - 4f * vector - 4f * vector2;
		quad.b = vector3 + 0f * vector - 4f * vector2;
		quad.c = vector3 + 0f * vector + (float)(rowCount - 4) * vector2;
		quad.d = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		Vector2 vector4 = quad.Min();
		Vector2 vector5 = quad.Max();
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = Mathf.Max((int)((vector4.x - 64f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector4.y - 64f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector5.x + 64f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector5.y + 64f) / 64f + 135f), 269);
		ulong num5 = 18446744073709551615uL;
		bool flag = Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad);
		for (int i = 0; i < rowCount; i++)
		{
			Vector2 vector6 = ((float)i - 3.5f) * vector2;
			Vector2 vector7 = ((float)i - 3.9f) * vector2;
			Vector2 vector8 = ((float)i - 3.1f) * vector2;
			Vector2 vector9 = vector3 + vector6 - 5f * vector;
			float num6 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(VectorUtils.X_Y(vector9));
			int num7 = 0;
			while ((long)num7 < 4L)
			{
				Vector2 vector10 = ((float)num7 - 3.9f) * vector;
				Vector2 vector11 = ((float)num7 - 3.1f) * vector;
				Vector2 vector12 = vector3 + vector6 + vector11;
				float num8 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(VectorUtils.X_Y(vector12));
				if (Mathf.Abs(num8 - num6) > 8f)
				{
					num5 &= ~(1uL << (i << 3 | num7));
				}
				else if (flag)
				{
					Quad2 quad2 = default(Quad2);
					quad2.a = vector3 + vector10 + vector7;
					quad2.b = vector3 + vector11 + vector7;
					quad2.c = vector3 + vector11 + vector8;
					quad2.d = vector3 + vector10 + vector8;
					if (Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad2))
					{
						num5 &= ~(1uL << (i << 3 | num7));
					}
				}
				num7++;
			}
		}
		for (int j = num2; j <= num4; j++)
		{
			for (int k = num; k <= num3; k++)
			{
				ushort num9 = instance.m_segmentGrid[j * 270 + k];
				int num10 = 0;
				while (num9 != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)num9].Info;
					if (info.m_class.m_layer == ItemClass.Layer.Default)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)num9].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)num9].m_endNode;
						Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
						float num11 = Mathf.Max(Mathf.Max(vector4.x - 64f - position.x, vector4.y - 64f - position.z), Mathf.Max(position.x - vector5.x - 64f, position.z - vector5.y - 64f));
						float num12 = Mathf.Max(Mathf.Max(vector4.x - 64f - position2.x, vector4.y - 64f - position2.z), Mathf.Max(position2.x - vector5.x - 64f, position2.z - vector5.y - 64f));
						if (num11 < 0f || num12 < 0f)
						{
							this.CalculateImplementation1(blockID, num9, ref instance.m_segments.m_buffer[(int)num9], ref num5, vector4.x, vector4.y, vector5.x, vector5.y);
						}
					}
					num9 = instance.m_segments.m_buffer[(int)num9].m_nextGridSegment;
					if (++num10 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		ulong num13 = 144680345676153346uL;
		for (int l = 0; l < 7; l++)
		{
			num5 = ((num5 & ~num13) | (num5 & num5 << 1 & num13));
			num13 <<= 1;
		}
		this.m_valid = num5;
		this.m_shared = 0uL;
	}

	private void CalculateImplementation2(ushort blockID, ref ZoneBlock other, ref ulong valid, ref ulong shared, float minX, float minZ, float maxX, float maxZ)
	{
		if ((other.m_flags & 1u) == 0u)
		{
			return;
		}
		if (Mathf.Abs(other.m_position.x - this.m_position.x) >= 92f || Mathf.Abs(other.m_position.z - this.m_position.z) >= 92f)
		{
			return;
		}
		bool flag = (other.m_flags & 2u) != 0u;
		int rowCount = this.RowCount;
		int rowCount2 = other.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = new Vector2(Mathf.Cos(other.m_angle), Mathf.Sin(other.m_angle)) * 8f;
		Vector2 vector4;
		vector4..ctor(vector3.y, -vector3.x);
		Vector2 vector5 = VectorUtils.XZ(other.m_position);
		Quad2 quad = default(Quad2);
		quad.a = vector5 - 4f * vector3 - 4f * vector4;
		quad.b = vector5 + 0f * vector3 - 4f * vector4;
		quad.c = vector5 + 0f * vector3 + (float)(rowCount2 - 4) * vector4;
		quad.d = vector5 - 4f * vector3 + (float)(rowCount2 - 4) * vector4;
		Vector2 vector6 = quad.Min();
		Vector2 vector7 = quad.Max();
		if (vector6.x <= maxX && vector6.y <= maxZ && minX <= vector7.x && minZ <= vector7.y)
		{
			Vector2 vector8 = VectorUtils.XZ(this.m_position);
			Quad2 quad2 = default(Quad2);
			quad2.a = vector8 - 4f * vector - 4f * vector2;
			quad2.b = vector8 + 0f * vector - 4f * vector2;
			quad2.c = vector8 + 0f * vector + (float)(rowCount - 4) * vector2;
			quad2.d = vector8 - 4f * vector + (float)(rowCount - 4) * vector2;
			if (!quad2.Intersect(quad))
			{
				return;
			}
			for (int i = 0; i < rowCount; i++)
			{
				Vector2 vector9 = ((float)i - 3.99f) * vector2;
				Vector2 vector10 = ((float)i - 3.01f) * vector2;
				quad2.a = vector8 - 4f * vector + vector9;
				quad2.b = vector8 + 0f * vector + vector9;
				quad2.c = vector8 + 0f * vector + vector10;
				quad2.d = vector8 - 4f * vector + vector10;
				if (quad2.Intersect(quad))
				{
					int num = 0;
					while ((long)num < 4L)
					{
						if ((valid & 1uL << (i << 3 | num)) == 0uL)
						{
							break;
						}
						Vector2 vector11 = ((float)num - 3.99f) * vector;
						Vector2 vector12 = ((float)num - 3.01f) * vector;
						Vector2 vector13 = vector8 + (vector12 + vector11 + vector10 + vector9) * 0.5f;
						if (Quad2.Intersect(quad.a - vector3 - vector4, quad.b + vector3 - vector4, quad.c + vector3 + vector4, quad.d - vector3 + vector4, vector13))
						{
							Quad2 quad3 = default(Quad2);
							quad3.a = vector8 + vector11 + vector9;
							quad3.b = vector8 + vector12 + vector9;
							quad3.c = vector8 + vector12 + vector10;
							quad3.d = vector8 + vector11 + vector10;
							bool flag2 = true;
							bool flag3 = false;
							int num2 = 0;
							while (num2 < rowCount2 && flag2)
							{
								Vector2 vector14 = ((float)num2 - 3.99f) * vector4;
								Vector2 vector15 = ((float)num2 - 3.01f) * vector4;
								int num3 = 0;
								while ((long)num3 < 4L && flag2)
								{
									if ((other.m_valid & ~other.m_shared & 1uL << (num2 << 3 | num3)) != 0uL)
									{
										Vector2 vector16 = ((float)num3 - 3.99f) * vector3;
										Vector2 vector17 = ((float)num3 - 3.01f) * vector3;
										Vector2 vector18 = vector5 + (vector17 + vector16 + vector15 + vector14) * 0.5f;
										float num4 = Vector2.SqrMagnitude(vector18 - vector13);
										if (num4 < 144f)
										{
											if (!flag)
											{
												float num5 = Mathf.Abs(other.m_angle - this.m_angle) * 0.636619747f;
												num5 -= Mathf.Floor(num5);
												if (num4 < 0.01f && (num5 < 0.01f || num5 > 0.99f))
												{
													if (num < num3 || (num == num3 && this.m_buildIndex < other.m_buildIndex))
													{
														other.m_shared |= 1uL << (num2 << 3 | num3);
													}
													else
													{
														flag3 = true;
													}
												}
												else
												{
													Quad2 quad4 = default(Quad2);
													quad4.a = vector5 + vector16 + vector14;
													quad4.b = vector5 + vector17 + vector14;
													quad4.c = vector5 + vector17 + vector15;
													quad4.d = vector5 + vector16 + vector15;
													if (quad3.Intersect(quad4))
													{
														if ((num3 >= 4 && num >= 4) || (num3 < 4 && num < 4))
														{
															if ((num3 >= 2 && num >= 2) || (num3 < 2 && num < 2))
															{
																if (this.m_buildIndex < other.m_buildIndex)
																{
																	other.m_valid &= ~(1uL << (num2 << 3 | num3));
																}
																else
																{
																	flag2 = false;
																}
															}
															else if (num3 < 2)
															{
																flag2 = false;
															}
															else
															{
																other.m_valid &= ~(1uL << (num2 << 3 | num3));
															}
														}
														else if (num3 < 4)
														{
															flag2 = false;
														}
														else
														{
															other.m_valid &= ~(1uL << (num2 << 3 | num3));
														}
													}
												}
											}
											if (num4 < 36f && num < 4 && num3 < 4)
											{
												ItemClass.Zone zone = this.GetZone(num, i);
												ItemClass.Zone zone2 = other.GetZone(num3, num2);
												if (zone == ItemClass.Zone.Unzoned)
												{
													this.SetZone(num, i, zone2);
												}
												else if (zone2 == ItemClass.Zone.Unzoned && !flag)
												{
													other.SetZone(num3, num2, zone);
												}
											}
										}
									}
									num3++;
								}
								num2++;
							}
							if (!flag2)
							{
								valid &= ~(1uL << (i << 3 | num));
								break;
							}
							if (flag3)
							{
								shared |= 1uL << (i << 3 | num);
							}
						}
						num++;
					}
				}
			}
		}
	}

	public void CalculateBlock2(ushort blockID)
	{
		if ((this.m_flags & 3u) != 1u)
		{
			return;
		}
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Vector2 vector4 = vector3 - 4f * vector - 4f * vector2;
		Vector2 vector5 = vector3 + 0f * vector - 4f * vector2;
		Vector2 vector6 = vector3 + 0f * vector + (float)(rowCount - 4) * vector2;
		Vector2 vector7 = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		float num = Mathf.Min(Mathf.Min(vector4.x, vector5.x), Mathf.Min(vector6.x, vector7.x));
		float num2 = Mathf.Min(Mathf.Min(vector4.y, vector5.y), Mathf.Min(vector6.y, vector7.y));
		float num3 = Mathf.Max(Mathf.Max(vector4.x, vector5.x), Mathf.Max(vector6.x, vector7.x));
		float num4 = Mathf.Max(Mathf.Max(vector4.y, vector5.y), Mathf.Max(vector6.y, vector7.y));
		ulong num5 = this.m_valid;
		ulong shared = 0uL;
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		for (int i = 0; i < instance.m_cachedBlocks.m_size; i++)
		{
			this.CalculateImplementation2(blockID, ref instance.m_cachedBlocks.m_buffer[i], ref num5, ref shared, num, num2, num3, num4);
		}
		int num6 = Mathf.Max((int)((num - 46f) / 64f + 75f), 0);
		int num7 = Mathf.Max((int)((num2 - 46f) / 64f + 75f), 0);
		int num8 = Mathf.Min((int)((num3 + 46f) / 64f + 75f), 149);
		int num9 = Mathf.Min((int)((num4 + 46f) / 64f + 75f), 149);
		for (int j = num7; j <= num9; j++)
		{
			for (int k = num6; k <= num8; k++)
			{
				ushort num10 = instance.m_zoneGrid[j * 150 + k];
				int num11 = 0;
				while (num10 != 0)
				{
					Vector3 position = instance.m_blocks.m_buffer[(int)num10].m_position;
					float num12 = Mathf.Max(Mathf.Max(num - 46f - position.x, num2 - 46f - position.z), Mathf.Max(position.x - num3 - 46f, position.z - num4 - 46f));
					if (num12 < 0f && num10 != blockID)
					{
						this.CalculateImplementation2(blockID, ref instance.m_blocks.m_buffer[(int)num10], ref num5, ref shared, num, num2, num3, num4);
					}
					num10 = instance.m_blocks.m_buffer[(int)num10].m_nextGridBlock;
					if (++num11 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		ulong num13 = 144680345676153346uL;
		for (int l = 0; l < 7; l++)
		{
			num5 = ((num5 & ~num13) | (num5 & num5 << 1 & num13));
			num13 <<= 1;
		}
		this.m_valid = num5;
		this.m_shared = shared;
	}

	private void CalculateImplementation3(ushort blockID, BuildingInfo info, ref Building building, ref ulong occupied1, ref ulong occupied2, ref ulong zone1, ref ulong zone2, float minX, float minZ, float maxX, float maxZ)
	{
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		int width = building.Width;
		int length = building.Length;
		Vector2 vector3;
		vector3..ctor(Mathf.Cos(building.m_angle), Mathf.Sin(building.m_angle));
		Vector2 vector4;
		vector4..ctor(vector3.y, -vector3.x);
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			vector3 *= (float)width * 4f - 0.8f;
			vector4 *= (float)length * 4f - 0.8f;
		}
		else
		{
			vector3 *= (float)width * 4f;
			vector4 *= (float)length * 4f;
		}
		if (info.m_circular)
		{
			vector3 *= 0.7f;
			vector4 *= 0.7f;
		}
		Vector2 vector5 = VectorUtils.XZ(building.m_position);
		Quad2 quad = default(Quad2);
		quad.a = vector5 - vector3 - vector4;
		quad.b = vector5 + vector3 - vector4;
		quad.c = vector5 + vector3 + vector4;
		quad.d = vector5 - vector3 + vector4;
		Vector2 vector6 = quad.Min();
		Vector2 vector7 = quad.Max();
		if (vector6.x <= maxX && vector6.y <= maxZ && minX <= vector7.x && minZ <= vector7.y)
		{
			Vector2 vector8 = VectorUtils.XZ(this.m_position);
			Quad2 quad2 = default(Quad2);
			quad2.a = vector8 - 4f * vector - 4f * vector2;
			quad2.b = vector8 + 0f * vector - 4f * vector2;
			quad2.c = vector8 + 0f * vector + (float)(rowCount - 4) * vector2;
			quad2.d = vector8 - 4f * vector + (float)(rowCount - 4) * vector2;
			if (!quad2.Intersect(quad))
			{
				return;
			}
			ulong num = this.OverlapQuad(quad);
			if (info.m_buildingAI.ClearOccupiedZoning())
			{
				occupied2 |= num;
				ulong num2 = (num & 72340172838076673uL) | (num & 144680345676153346uL) << 3;
				num2 |= num2 << 1;
				num2 |= num2 << 2;
				zone1 &= ~num2;
				num2 = ((num & 289360691352306692uL) >> 2 | (num & 578721382704613384uL) << 1);
				num2 |= num2 << 1;
				num2 |= num2 << 2;
				zone2 &= ~num2;
			}
			else
			{
				occupied1 |= num;
			}
		}
	}

	public void CalculateBlock3(ushort blockID)
	{
		if ((this.m_flags & 3u) != 1u)
		{
			return;
		}
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Vector2 vector4 = vector3 - 4f * vector - 4f * vector2;
		Vector2 vector5 = vector3 + 0f * vector - 4f * vector2;
		Vector2 vector6 = vector3 + 0f * vector + (float)(rowCount - 4) * vector2;
		Vector2 vector7 = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		float num = Mathf.Min(Mathf.Min(vector4.x, vector5.x), Mathf.Min(vector6.x, vector7.x));
		float num2 = Mathf.Min(Mathf.Min(vector4.y, vector5.y), Mathf.Min(vector6.y, vector7.y));
		float num3 = Mathf.Max(Mathf.Max(vector4.x, vector5.x), Mathf.Max(vector6.x, vector7.x));
		float num4 = Mathf.Max(Mathf.Max(vector4.y, vector5.y), Mathf.Max(vector6.y, vector7.y));
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		ulong occupied = 0uL;
		ulong occupied2 = 0uL;
		ulong zone = this.m_zone1;
		ulong zone2 = this.m_zone2;
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					BuildingInfo buildingInfo;
					int num11;
					int num12;
					instance.m_buildings.m_buffer[(int)num9].GetInfoWidthLength(out buildingInfo, out num11, out num12);
					if (buildingInfo.m_class.m_layer == ItemClass.Layer.Default)
					{
						Vector3 position = instance.m_buildings.m_buffer[(int)num9].m_position;
						float num13 = Mathf.Min(72f, (float)(num11 + num12) * 4f);
						float num14 = Mathf.Max(Mathf.Max(num - num13 - position.x, num2 - num13 - position.z), Mathf.Max(position.x - num3 - num13, position.z - num4 - num13));
						if (num14 < 0f)
						{
							this.CalculateImplementation3(blockID, buildingInfo, ref instance.m_buildings.m_buffer[(int)num9], ref occupied, ref occupied2, ref zone, ref zone2, num, num2, num3, num4);
						}
					}
					num9 = instance.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		this.m_occupied1 = occupied;
		this.m_occupied2 = occupied2;
		this.m_zone1 = zone;
		this.m_zone2 = zone2;
	}

	public void UpdateBlock(ushort blockID)
	{
		if ((this.m_flags & 1u) == 0u)
		{
			return;
		}
		int rowCount = this.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(this.m_angle), 0f, Mathf.Sin(this.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Vector3 vector3 = this.m_position - 4f * vector - 4f * vector2;
		Vector3 vector4 = this.m_position + 0f * vector - 4f * vector2;
		Vector3 vector5 = this.m_position + 0f * vector + (float)(rowCount - 4) * vector2;
		Vector3 vector6 = this.m_position - 4f * vector + (float)(rowCount - 4) * vector2;
		float minX = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x));
		float minZ = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z));
		float maxX = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x));
		float maxZ = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z));
		TerrainModify.UpdateArea(minX, minZ, maxX, maxZ, false, false, true);
		Singleton<BuildingManager>.get_instance().ZonesUpdated(minX, minZ, maxX, maxZ);
	}

	public void RefreshZoning(ushort blockID)
	{
		this.CalculateBlock3(blockID);
		this.UpdateBlock(blockID);
	}

	public float PointDistanceSq(Vector3 point, float minDistanceSq)
	{
		int rowCount = this.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(this.m_angle), 0f, Mathf.Sin(this.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		float num = Mathf.Sqrt(minDistanceSq);
		Vector3 vector3 = this.m_position - 4f * vector - 4f * vector2;
		Vector3 vector4 = this.m_position + 0f * vector - 4f * vector2;
		Vector3 vector5 = this.m_position + 0f * vector + (float)(rowCount - 4) * vector2;
		Vector3 vector6 = this.m_position - 4f * vector + (float)(rowCount - 4) * vector2;
		float num2 = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x)) - num;
		float num3 = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z)) - num;
		float num4 = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x)) + num;
		float num5 = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z)) + num;
		if (point.x <= num4 && point.z <= num5 && num2 <= point.x && num3 <= point.z)
		{
			for (int i = 0; i < rowCount; i++)
			{
				Vector3 vector7 = this.m_position - point + ((float)i - 3.5f) * vector2;
				int num6 = 0;
				while ((long)num6 < 4L)
				{
					if ((this.m_valid & 1uL << (i << 3 | num6)) != 0uL)
					{
						Vector3 vector8 = ((float)num6 - 3.5f) * vector;
						Vector3 vector9 = vector7 + vector8;
						if ((this.m_shared & 1uL << (i << 3 | num6)) != 0uL)
						{
							vector9.y = 4f;
						}
						else
						{
							vector9.y = 0f;
						}
						float num7 = Vector3.SqrMagnitude(vector9);
						if (num7 < minDistanceSq)
						{
							minDistanceSq = num7;
						}
					}
					num6++;
				}
			}
		}
		return minDistanceSq;
	}

	public bool SetZone(int x, int z, ItemClass.Zone zone)
	{
		if (zone == ItemClass.Zone.Distant)
		{
			zone = ItemClass.Zone.Unzoned;
		}
		int num = z << 3 | (x & 1) << 2;
		ulong num2 = ~(15uL << num);
		if (x < 2)
		{
			ulong num3 = (this.m_zone1 & num2) | (ulong)((ulong)((long)zone) << num);
			if (num3 != this.m_zone1)
			{
				this.m_zone1 = num3;
				return true;
			}
		}
		else if (x < 4)
		{
			ulong num4 = (this.m_zone2 & num2) | (ulong)((ulong)((long)zone) << num);
			if (num4 != this.m_zone2)
			{
				this.m_zone2 = num4;
				return true;
			}
		}
		return false;
	}

	public ItemClass.Zone GetZone(int x, int z)
	{
		if (x >= 4)
		{
			return ItemClass.Zone.Distant;
		}
		int num = z << 3 | (x & 1) << 2;
		if (x < 2)
		{
			return (ItemClass.Zone)(this.m_zone1 >> num & 15uL);
		}
		return (ItemClass.Zone)(this.m_zone2 >> num & 15uL);
	}

	public bool IsOccupied1(int x, int z)
	{
		return (this.m_occupied1 & 1uL << (z << 3 | x)) != 0uL;
	}

	public bool IsOccupied2(int x, int z)
	{
		return (this.m_occupied2 & 1uL << (z << 3 | x)) != 0uL;
	}

	public void ZonesUpdated(ushort blockID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & 3u) != 1u)
		{
			return;
		}
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Vector2 vector4 = vector3 - 4f * vector - 4f * vector2;
		Vector2 vector5 = vector3 + 0f * vector - 4f * vector2;
		Vector2 vector6 = vector3 + 0f * vector + (float)(rowCount - 4) * vector2;
		Vector2 vector7 = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		float num = Mathf.Min(Mathf.Min(vector4.x, vector5.x), Mathf.Min(vector6.x, vector7.x));
		float num2 = Mathf.Min(Mathf.Min(vector4.y, vector5.y), Mathf.Min(vector6.y, vector7.y));
		float num3 = Mathf.Max(Mathf.Max(vector4.x, vector5.x), Mathf.Max(vector6.x, vector7.x));
		float num4 = Mathf.Max(Mathf.Max(vector4.y, vector5.y), Mathf.Max(vector6.y, vector7.y));
		if (num3 >= minX && num <= maxX && num4 >= minZ && num2 <= maxZ)
		{
			bool flag = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None;
			Vector2 pos = vector3 + vector * 0.5f + vector2 * 0.5f;
			ulong num5 = this.m_valid & ~this.m_shared;
			ulong num6 = this.m_occupied1 | this.m_occupied2;
			for (int i = 0; i < rowCount; i++)
			{
				int num7 = i;
				Vector2 vector8 = ((float)i - 4f) * vector2;
				while (i + 1 < rowCount)
				{
					int num8 = 0;
					while ((long)num8 < 4L)
					{
						ulong num9 = 1uL << (num7 << 3 | num8);
						ulong num10 = 1uL << (i + 1 << 3 | num8);
						if ((num5 & num9) != 0uL != ((num5 & num10) != 0uL))
						{
							break;
						}
						if ((num6 & num9) != 0uL != ((num6 & num10) != 0uL))
						{
							break;
						}
						if (!flag && this.GetZone(num8, i + 1) != this.GetZone(num8, num7))
						{
							break;
						}
						num8++;
					}
					if ((long)num8 < 4L)
					{
						break;
					}
					i++;
				}
				Vector2 vector9 = ((float)i - 3f) * vector2;
				int num11 = 0;
				while ((long)num11 < 4L)
				{
					ulong num12 = 1uL << (num7 << 3 | num11);
					if ((num5 & num12) != 0uL)
					{
						bool flag2 = (num6 & num12) != 0uL;
						int num13 = num11;
						ItemClass.Zone zone = (!flag) ? this.GetZone(num11, num7) : ItemClass.Zone.ResidentialLow;
						Vector2 vector10 = ((float)num11 - 4f) * vector;
						while (num11 != 3 && num11 + 1 < 8)
						{
							num12 = 1uL << (num7 << 3 | num11 + 1);
							if ((num5 & num12) == 0uL)
							{
								break;
							}
							if ((num6 & num12) != 0uL != flag2)
							{
								break;
							}
							if (!flag && this.GetZone(num11 + 1, num7) != zone)
							{
								break;
							}
							num11++;
						}
						Vector2 vector11 = ((float)num11 - 3f) * vector;
						Vector2 a = vector3 + vector10 + vector8;
						Vector2 b = vector3 + vector11 + vector8;
						Vector2 c = vector3 + vector11 + vector9;
						Vector2 d = vector3 + vector10 + vector9;
						TerrainModify.ApplyQuad(a, b, c, d, zone, flag2, this.m_angle, pos, vector, vector2, 4 - num11, 4 - num13, 4 - i, 4 - num7);
					}
					num11++;
				}
			}
		}
	}

	private void CheckBlock(ref ZoneBlock other, int[] xBuffer, ItemClass.Zone zone, Vector2 startPos, Vector2 xDir, Vector2 zDir, Quad2 quad)
	{
		float num = Mathf.Abs(other.m_angle - this.m_angle) * 0.636619747f;
		num -= Mathf.Floor(num);
		if (num >= 0.01f && num <= 0.99f)
		{
			return;
		}
		int rowCount = other.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(other.m_angle), Mathf.Sin(other.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		ulong num2 = other.m_valid & ~(other.m_occupied1 | other.m_occupied2);
		Vector2 vector3 = VectorUtils.XZ(other.m_position);
		Quad2 quad2 = default(Quad2);
		quad2.a = vector3 - 4f * vector - 4f * vector2;
		quad2.b = vector3 - 4f * vector2;
		quad2.c = vector3 + (float)(rowCount - 4) * vector2;
		quad2.d = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
		if (!quad.Intersect(quad2))
		{
			return;
		}
		for (int i = 0; i < rowCount; i++)
		{
			Vector2 vector4 = ((float)i - 3.5f) * vector2;
			for (int j = 0; j < 4; j++)
			{
				if ((num2 & 1uL << (i << 3 | j)) != 0uL)
				{
					if (other.GetZone(j, i) == zone)
					{
						Vector2 vector5 = ((float)j - 3.5f) * vector;
						Vector2 vector6 = vector3 + vector5 + vector4 - startPos;
						float num3 = (vector6.x * xDir.x + vector6.y * xDir.y) * 0.015625f;
						float num4 = (vector6.x * zDir.x + vector6.y * zDir.y) * 0.015625f;
						int num5 = Mathf.RoundToInt(num3);
						int num6 = Mathf.RoundToInt(num4);
						if (num5 >= 0 && num5 <= 6 && num6 >= -6 && num6 <= 6)
						{
							if (Mathf.Abs(num3 - (float)num5) < 0.0125f && Mathf.Abs(num4 - (float)num6) < 0.0125f)
							{
								if (j == 0 || num5 != 0)
								{
									xBuffer[num6 + 6] |= 1 << num5;
									if (j == 0)
									{
										xBuffer[num6 + 6] |= 1 << num5 + 16;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private bool IsGoodPlace(Vector2 position)
	{
		int num = Mathf.Max((int)((position.x - 104f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.y - 104f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + 104f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.y + 104f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Building.Flags flags = buildings.m_buffer[(int)num5].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created)
					{
						BuildingInfo buildingInfo;
						int num7;
						int num8;
						buildings.m_buffer[(int)num5].GetInfoWidthLength(out buildingInfo, out num7, out num8);
						if (buildingInfo != null)
						{
							float num9 = buildingInfo.m_buildingAI.ElectricityGridRadius();
							if (num9 > 0.1f || buildingInfo.m_class.m_service == ItemClass.Service.Electricity)
							{
								Vector2 vector = VectorUtils.XZ(buildings.m_buffer[(int)num5].m_position);
								num9 = Mathf.Max(8f, num9) + 32f;
								if (Vector2.SqrMagnitude(position - vector) < num9 * num9)
								{
									return true;
								}
							}
						}
					}
					num5 = buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return false;
	}

	public void SimulationStep(ushort blockID)
	{
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		int rowCount = this.RowCount;
		Vector2 vector = new Vector2(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle)) * 8f;
		Vector2 vector2;
		vector2..ctor(vector.y, -vector.x);
		ulong num = this.m_valid & ~(this.m_occupied1 | this.m_occupied2);
		int num2 = 0;
		ItemClass.Zone zone = ItemClass.Zone.Unzoned;
		int num3 = 0;
		while (num3 < 4 && zone == ItemClass.Zone.Unzoned)
		{
			num2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)rowCount);
			if ((num & 1uL << (num2 << 3)) != 0uL)
			{
				zone = this.GetZone(0, num2);
			}
			num3++;
		}
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(this.m_position);
		int num4;
		switch (zone)
		{
		case ItemClass.Zone.ResidentialLow:
			num4 = instance.m_actualResidentialDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateResidentialLowDemandOffset();
			break;
		case ItemClass.Zone.ResidentialHigh:
			num4 = instance.m_actualResidentialDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateResidentialHighDemandOffset();
			break;
		case ItemClass.Zone.CommercialLow:
			num4 = instance.m_actualCommercialDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateCommercialLowDemandOffset();
			break;
		case ItemClass.Zone.CommercialHigh:
			num4 = instance.m_actualCommercialDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateCommercialHighDemandOffset();
			break;
		case ItemClass.Zone.Industrial:
			num4 = instance.m_actualWorkplaceDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateIndustrialDemandOffset();
			break;
		case ItemClass.Zone.Office:
			num4 = instance.m_actualWorkplaceDemand;
			num4 += instance2.m_districts.m_buffer[(int)district].CalculateOfficeDemandOffset();
			break;
		default:
			return;
		}
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Vector2 vector4 = vector3 - 3.5f * vector + ((float)num2 - 3.5f) * vector2;
		int[] tmpXBuffer = instance.m_tmpXBuffer;
		for (int i = 0; i < 13; i++)
		{
			tmpXBuffer[i] = 0;
		}
		Quad2 quad = default(Quad2);
		quad.a = vector3 - 4f * vector + ((float)num2 - 10f) * vector2;
		quad.b = vector3 + 3f * vector + ((float)num2 - 10f) * vector2;
		quad.c = vector3 + 3f * vector + ((float)num2 + 2f) * vector2;
		quad.d = vector3 - 4f * vector + ((float)num2 + 2f) * vector2;
		Vector2 vector5 = quad.Min();
		Vector2 vector6 = quad.Max();
		int num5 = Mathf.Max((int)((vector5.x - 46f) / 64f + 75f), 0);
		int num6 = Mathf.Max((int)((vector5.y - 46f) / 64f + 75f), 0);
		int num7 = Mathf.Min((int)((vector6.x + 46f) / 64f + 75f), 149);
		int num8 = Mathf.Min((int)((vector6.y + 46f) / 64f + 75f), 149);
		for (int j = num6; j <= num8; j++)
		{
			for (int k = num5; k <= num7; k++)
			{
				ushort num9 = instance.m_zoneGrid[j * 150 + k];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = instance.m_blocks.m_buffer[(int)num9].m_position;
					float num11 = Mathf.Max(Mathf.Max(vector5.x - 46f - position.x, vector5.y - 46f - position.z), Mathf.Max(position.x - vector6.x - 46f, position.z - vector6.y - 46f));
					if (num11 < 0f)
					{
						this.CheckBlock(ref instance.m_blocks.m_buffer[(int)num9], tmpXBuffer, zone, vector4, vector, vector2, quad);
					}
					num9 = instance.m_blocks.m_buffer[(int)num9].m_nextGridBlock;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int l = 0; l < 13; l++)
		{
			uint num12 = (uint)tmpXBuffer[l];
			int num13 = 0;
			bool flag = (num12 & 196608u) == 196608u;
			bool flag2 = false;
			while ((num12 & 1u) != 0u)
			{
				num13++;
				flag2 = ((num12 & 65536u) != 0u);
				num12 >>= 1;
			}
			if (num13 == 5 || num13 == 6)
			{
				if (flag2)
				{
					num13 -= Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) + 2;
				}
				else
				{
					num13 = 4;
				}
				num13 |= 131072;
			}
			else if (num13 == 7)
			{
				num13 = 4;
				num13 |= 131072;
			}
			if (flag)
			{
				num13 |= 65536;
			}
			tmpXBuffer[l] = num13;
		}
		int num14 = tmpXBuffer[6] & 65535;
		if (num14 == 0)
		{
			return;
		}
		bool flag3 = this.IsGoodPlace(vector4);
		if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) >= num4)
		{
			if (flag3)
			{
				instance.m_goodAreaFound[(int)zone] = 1024;
			}
			return;
		}
		if (!flag3 && instance.m_goodAreaFound[(int)zone] > -1024)
		{
			if (instance.m_goodAreaFound[(int)zone] == 0)
			{
				instance.m_goodAreaFound[(int)zone] = -1;
			}
			return;
		}
		int num15 = 6;
		int num16 = 6;
		bool flag4 = true;
		while (true)
		{
			if (flag4)
			{
				while (num15 != 0)
				{
					if ((tmpXBuffer[num15 - 1] & 65535) != num14)
					{
						break;
					}
					num15--;
				}
				while (num16 != 12)
				{
					if ((tmpXBuffer[num16 + 1] & 65535) != num14)
					{
						break;
					}
					num16++;
				}
			}
			else
			{
				while (num15 != 0)
				{
					if ((tmpXBuffer[num15 - 1] & 65535) < num14)
					{
						break;
					}
					num15--;
				}
				while (num16 != 12)
				{
					if ((tmpXBuffer[num16 + 1] & 65535) < num14)
					{
						break;
					}
					num16++;
				}
			}
			int num17 = num15;
			int num18 = num16;
			while (num17 != 0)
			{
				if ((tmpXBuffer[num17 - 1] & 65535) < 2)
				{
					break;
				}
				num17--;
			}
			while (num18 != 12)
			{
				if ((tmpXBuffer[num18 + 1] & 65535) < 2)
				{
					break;
				}
				num18++;
			}
			bool flag5 = num17 != 0 && num17 == num15 - 1;
			bool flag6 = num18 != 12 && num18 == num16 + 1;
			if (flag5 && flag6)
			{
				if (num16 - num15 > 2)
				{
					break;
				}
				if (num14 <= 2)
				{
					if (!flag4)
					{
						goto Block_34;
					}
				}
				else
				{
					num14--;
				}
			}
			else if (flag5)
			{
				if (num16 - num15 > 1)
				{
					goto Block_36;
				}
				if (num14 <= 2)
				{
					if (!flag4)
					{
						goto Block_38;
					}
				}
				else
				{
					num14--;
				}
			}
			else if (flag6)
			{
				if (num16 - num15 > 1)
				{
					goto Block_40;
				}
				if (num14 <= 2)
				{
					if (!flag4)
					{
						goto Block_42;
					}
				}
				else
				{
					num14--;
				}
			}
			else
			{
				if (num15 != num16)
				{
					goto IL_882;
				}
				if (num14 <= 2)
				{
					if (!flag4)
					{
						goto Block_45;
					}
				}
				else
				{
					num14--;
				}
			}
			flag4 = false;
		}
		num15++;
		num16--;
		Block_34:
		goto IL_88F;
		Block_36:
		num15++;
		Block_38:
		goto IL_88F;
		Block_40:
		num16--;
		Block_42:
		Block_45:
		IL_882:
		IL_88F:
		int num19;
		int num20;
		if (num14 == 1 && num16 - num15 >= 1)
		{
			num15 += Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)(num16 - num15));
			num16 = num15 + 1;
			num19 = num15 + Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u);
			num20 = num19;
		}
		else
		{
			do
			{
				num19 = num15;
				num20 = num16;
				if (num16 - num15 == 2)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						num20--;
					}
					else
					{
						num19++;
					}
				}
				else if (num16 - num15 == 3)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						num20 -= 2;
					}
					else
					{
						num19 += 2;
					}
				}
				else if (num16 - num15 == 4)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						num16 -= 2;
						num20 -= 3;
					}
					else
					{
						num15 += 2;
						num19 += 3;
					}
				}
				else if (num16 - num15 == 5)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						num16 -= 3;
						num20 -= 2;
					}
					else
					{
						num15 += 3;
						num19 += 2;
					}
				}
				else if (num16 - num15 >= 6)
				{
					if (num15 == 0 || num16 == 12)
					{
						if (num15 == 0)
						{
							num15 = 3;
							num19 = 2;
						}
						if (num16 == 12)
						{
							num16 = 9;
							num20 = 10;
						}
					}
					else if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						num16 = num15 + 3;
						num20 = num19 + 2;
					}
					else
					{
						num15 = num16 - 3;
						num19 = num20 - 2;
					}
				}
			}
			while (num16 - num15 > 3 || num20 - num19 > 3);
		}
		int num21 = 4;
		int num22 = num16 - num15 + 1;
		BuildingInfo.ZoningMode zoningMode = BuildingInfo.ZoningMode.Straight;
		bool flag7 = true;
		for (int m = num15; m <= num16; m++)
		{
			num21 = Mathf.Min(num21, tmpXBuffer[m] & 65535);
			if ((tmpXBuffer[m] & 131072) == 0)
			{
				flag7 = false;
			}
		}
		if (num16 > num15)
		{
			if ((tmpXBuffer[num15] & 65536) != 0)
			{
				zoningMode = BuildingInfo.ZoningMode.CornerLeft;
				num20 = num15 + num20 - num19;
				num19 = num15;
			}
			if ((tmpXBuffer[num16] & 65536) != 0 && (zoningMode != BuildingInfo.ZoningMode.CornerLeft || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0))
			{
				zoningMode = BuildingInfo.ZoningMode.CornerRight;
				num19 = num16 + num19 - num20;
				num20 = num16;
			}
		}
		int num23 = 4;
		int num24 = num20 - num19 + 1;
		BuildingInfo.ZoningMode zoningMode2 = BuildingInfo.ZoningMode.Straight;
		bool flag8 = true;
		for (int n = num19; n <= num20; n++)
		{
			num23 = Mathf.Min(num23, tmpXBuffer[n] & 65535);
			if ((tmpXBuffer[n] & 131072) == 0)
			{
				flag8 = false;
			}
		}
		if (num20 > num19)
		{
			if ((tmpXBuffer[num19] & 65536) != 0)
			{
				zoningMode2 = BuildingInfo.ZoningMode.CornerLeft;
			}
			if ((tmpXBuffer[num20] & 65536) != 0 && (zoningMode2 != BuildingInfo.ZoningMode.CornerLeft || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0))
			{
				zoningMode2 = BuildingInfo.ZoningMode.CornerRight;
			}
		}
		ItemClass.Service service = ItemClass.Service.None;
		ItemClass.SubService subService = ItemClass.SubService.None;
		ItemClass.Level level = ItemClass.Level.Level1;
		switch (zone)
		{
		case ItemClass.Zone.ResidentialLow:
			service = ItemClass.Service.Residential;
			subService = ItemClass.SubService.ResidentialLow;
			break;
		case ItemClass.Zone.ResidentialHigh:
			service = ItemClass.Service.Residential;
			subService = ItemClass.SubService.ResidentialHigh;
			break;
		case ItemClass.Zone.CommercialLow:
			service = ItemClass.Service.Commercial;
			subService = ItemClass.SubService.CommercialLow;
			break;
		case ItemClass.Zone.CommercialHigh:
			service = ItemClass.Service.Commercial;
			subService = ItemClass.SubService.CommercialHigh;
			break;
		case ItemClass.Zone.Industrial:
			service = ItemClass.Service.Industrial;
			break;
		case ItemClass.Zone.Office:
			service = ItemClass.Service.Office;
			break;
		default:
			return;
		}
		BuildingInfo buildingInfo = null;
		Vector3 vector7 = Vector3.get_zero();
		int num25 = 0;
		int num26 = 0;
		int num27 = 0;
		BuildingInfo.ZoningMode zoningMode3 = BuildingInfo.ZoningMode.Straight;
		int num28 = 0;
		while (num28 < 6)
		{
			switch (num28)
			{
			case 0:
				if (zoningMode != BuildingInfo.ZoningMode.Straight)
				{
					num25 = num15 + num16 + 1;
					num26 = num21;
					num27 = num22;
					zoningMode3 = zoningMode;
					goto IL_D5D;
				}
				break;
			case 1:
				if (zoningMode2 != BuildingInfo.ZoningMode.Straight)
				{
					num25 = num19 + num20 + 1;
					num26 = num23;
					num27 = num24;
					zoningMode3 = zoningMode2;
					goto IL_D5D;
				}
				break;
			case 2:
				if (zoningMode != BuildingInfo.ZoningMode.Straight)
				{
					if (num21 >= 4)
					{
						num25 = num15 + num16 + 1;
						num26 = ((!flag7) ? 2 : 3);
						num27 = num22;
						zoningMode3 = zoningMode;
						goto IL_D5D;
					}
				}
				break;
			case 3:
				if (zoningMode2 != BuildingInfo.ZoningMode.Straight)
				{
					if (num23 >= 4)
					{
						num25 = num19 + num20 + 1;
						num26 = ((!flag8) ? 2 : 3);
						num27 = num24;
						zoningMode3 = zoningMode2;
						goto IL_D5D;
					}
				}
				break;
			case 4:
				num25 = num15 + num16 + 1;
				num26 = num21;
				num27 = num22;
				zoningMode3 = BuildingInfo.ZoningMode.Straight;
				goto IL_D5D;
			case 5:
				num25 = num19 + num20 + 1;
				num26 = num23;
				num27 = num24;
				zoningMode3 = BuildingInfo.ZoningMode.Straight;
				goto IL_D5D;
			default:
				goto IL_D5D;
			}
			IL_E9C:
			num28++;
			continue;
			IL_D5D:
			vector7 = this.m_position + VectorUtils.X_Y(((float)num26 * 0.5f - 4f) * vector + ((float)num25 * 0.5f + (float)num2 - 10f) * vector2);
			if (zone == ItemClass.Zone.Industrial)
			{
				ZoneBlock.GetIndustryType(vector7, out subService, out level);
			}
			else if (zone == ItemClass.Zone.CommercialLow || zone == ItemClass.Zone.CommercialHigh)
			{
				ZoneBlock.GetCommercialType(vector7, zone, num27, num26, out subService, out level);
			}
			else if (zone == ItemClass.Zone.ResidentialLow || zone == ItemClass.Zone.ResidentialHigh)
			{
				ZoneBlock.GetResidentialType(vector7, zone, num27, num26, out subService, out level);
			}
			else if (zone == ItemClass.Zone.Office)
			{
				ZoneBlock.GetOfficeType(vector7, zone, num27, num26, out subService, out level);
			}
			byte district2 = instance2.GetDistrict(vector7);
			ushort style = instance2.m_districts.m_buffer[(int)district2].m_Style;
			if (Singleton<BuildingManager>.get_instance().m_BuildingWrapper != null)
			{
				Singleton<BuildingManager>.get_instance().m_BuildingWrapper.OnCalculateSpawn(vector7, ref service, ref subService, ref level, ref style);
			}
			buildingInfo = Singleton<BuildingManager>.get_instance().GetRandomBuildingInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, service, subService, level, num27, num26, zoningMode3, (int)style);
			if (buildingInfo != null)
			{
				break;
			}
			goto IL_E9C;
		}
		if (buildingInfo == null)
		{
			return;
		}
		float num29 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector7));
		if (num29 > vector7.y)
		{
			return;
		}
		if (Singleton<DisasterManager>.get_instance().IsEvacuating(vector7))
		{
			return;
		}
		float num30 = this.m_angle + 1.57079637f;
		if (zoningMode3 == BuildingInfo.ZoningMode.CornerLeft && buildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerRight)
		{
			num30 -= 1.57079637f;
			num26 = num27;
		}
		else if (zoningMode3 == BuildingInfo.ZoningMode.CornerRight && buildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft)
		{
			num30 += 1.57079637f;
			num26 = num27;
		}
		ushort num31;
		if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num31, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingInfo, vector7, num30, num26, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			switch (service)
			{
			case ItemClass.Service.Residential:
				instance.m_actualResidentialDemand = Mathf.Max(0, instance.m_actualResidentialDemand - 5);
				break;
			case ItemClass.Service.Commercial:
				instance.m_actualCommercialDemand = Mathf.Max(0, instance.m_actualCommercialDemand - 5);
				break;
			case ItemClass.Service.Industrial:
				instance.m_actualWorkplaceDemand = Mathf.Max(0, instance.m_actualWorkplaceDemand - 5);
				break;
			case ItemClass.Service.Office:
				instance.m_actualWorkplaceDemand = Mathf.Max(0, instance.m_actualWorkplaceDemand - 5);
				break;
			}
			if (zone == ItemClass.Zone.ResidentialHigh || zone == ItemClass.Zone.CommercialHigh)
			{
				Building[] expr_103B_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
				ushort expr_103B_cp_1 = num31;
				expr_103B_cp_0[(int)expr_103B_cp_1].m_flags = (expr_103B_cp_0[(int)expr_103B_cp_1].m_flags | Building.Flags.HighDensity);
			}
		}
		instance.m_goodAreaFound[(int)zone] = 1024;
	}

	public static void GetIndustryType(Vector3 position, out ItemClass.SubService subService, out ItemClass.Level level)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(position);
		DistrictPolicies.Specialization specializationPolicies = instance.m_districts.m_buffer[(int)district].m_specializationPolicies;
		if ((specializationPolicies & DistrictPolicies.Specialization.Forest) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.IndustrialForestry;
			int num = Singleton<NaturalResourceManager>.get_instance().CountResource(NaturalResourceManager.Resource.Forest, position, 0f);
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u) <= num)
			{
				level = ItemClass.Level.Level2;
			}
			else
			{
				level = ItemClass.Level.Level1;
			}
		}
		else if ((specializationPolicies & DistrictPolicies.Specialization.Farming) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.IndustrialFarming;
			int num2 = Singleton<NaturalResourceManager>.get_instance().CountResource(NaturalResourceManager.Resource.Fertility, position, 0f);
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u) <= num2)
			{
				level = ItemClass.Level.Level2;
			}
			else
			{
				level = ItemClass.Level.Level1;
			}
		}
		else if ((specializationPolicies & DistrictPolicies.Specialization.Oil) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.IndustrialOil;
			int num3 = Singleton<NaturalResourceManager>.get_instance().CountResource(NaturalResourceManager.Resource.Oil, position, 0f);
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u) <= num3)
			{
				level = ItemClass.Level.Level2;
			}
			else
			{
				level = ItemClass.Level.Level1;
			}
		}
		else if ((specializationPolicies & DistrictPolicies.Specialization.Ore) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.IndustrialOre;
			int num4 = Singleton<NaturalResourceManager>.get_instance().CountResource(NaturalResourceManager.Resource.Ore, position, 0f);
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u) <= num4)
			{
				level = ItemClass.Level.Level2;
			}
			else
			{
				level = ItemClass.Level.Level1;
			}
		}
		else
		{
			subService = ItemClass.SubService.IndustrialGeneric;
			level = ItemClass.Level.Level1;
		}
	}

	public static void GetCommercialType(Vector3 position, ItemClass.Zone zone, int width, int len, out ItemClass.SubService subService, out ItemClass.Level level)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(position);
		DistrictPolicies.Specialization specializationPolicies = instance.m_districts.m_buffer[(int)district].m_specializationPolicies;
		if ((specializationPolicies & DistrictPolicies.Specialization.Leisure) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.CommercialLeisure;
			level = ItemClass.Level.Level1;
		}
		else if ((specializationPolicies & DistrictPolicies.Specialization.Tourist) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.CommercialTourist;
			float num = (float)(width + len) * 4f + 40f;
			Rect area;
			area..ctor(position.x - num * 0.5f, position.z - num * 0.5f, num, num);
			float num2;
			float num3;
			Singleton<NaturalResourceManager>.get_instance().AveragePollutionAndWater(area, out num2, out num3);
			if (num3 > 0.001f)
			{
				level = ItemClass.Level.Level2;
			}
			else
			{
				level = ItemClass.Level.Level1;
			}
		}
		else if ((specializationPolicies & DistrictPolicies.Specialization.Organic) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.CommercialEco;
			level = ItemClass.Level.Level1;
		}
		else if (zone == ItemClass.Zone.CommercialHigh)
		{
			subService = ItemClass.SubService.CommercialHigh;
			level = ItemClass.Level.Level1;
		}
		else
		{
			subService = ItemClass.SubService.CommercialLow;
			level = ItemClass.Level.Level1;
		}
	}

	public static void GetResidentialType(Vector3 position, ItemClass.Zone zone, int width, int len, out ItemClass.SubService subService, out ItemClass.Level level)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(position);
		DistrictPolicies.Specialization specializationPolicies = instance.m_districts.m_buffer[(int)district].m_specializationPolicies;
		if ((specializationPolicies & DistrictPolicies.Specialization.Selfsufficient) != DistrictPolicies.Specialization.None)
		{
			if (zone == ItemClass.Zone.ResidentialHigh)
			{
				subService = ItemClass.SubService.ResidentialHighEco;
			}
			else
			{
				subService = ItemClass.SubService.ResidentialLowEco;
			}
			level = ItemClass.Level.Level1;
		}
		else
		{
			if (zone == ItemClass.Zone.ResidentialHigh)
			{
				subService = ItemClass.SubService.ResidentialHigh;
			}
			else
			{
				subService = ItemClass.SubService.ResidentialLow;
			}
			level = ItemClass.Level.Level1;
		}
	}

	public static void GetOfficeType(Vector3 position, ItemClass.Zone zone, int width, int len, out ItemClass.SubService subService, out ItemClass.Level level)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(position);
		DistrictPolicies.Specialization specializationPolicies = instance.m_districts.m_buffer[(int)district].m_specializationPolicies;
		if ((specializationPolicies & DistrictPolicies.Specialization.Hightech) != DistrictPolicies.Specialization.None)
		{
			subService = ItemClass.SubService.OfficeHightech;
			level = ItemClass.Level.Level1;
		}
		else
		{
			subService = ItemClass.SubService.OfficeGeneric;
			level = ItemClass.Level.Level1;
		}
	}

	public const uint FLAG_CREATED = 1u;

	public const uint FLAG_DELETED = 2u;

	public const uint FLAG_COLLAPSED = 4u;

	public const uint FLAG_ROWS = 65280u;

	public const uint COLUMN_COUNT = 4u;

	public uint m_flags;

	public uint m_buildIndex;

	public Vector3 m_position;

	public float m_angle;

	public ulong m_valid;

	public ulong m_shared;

	public ulong m_occupied1;

	public ulong m_occupied2;

	public ulong m_zone1;

	public ulong m_zone2;

	public ushort m_nextGridBlock;
}
