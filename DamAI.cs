﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class DamAI : RoadBaseAI
{
	public override string GetConstructionInfo(int productionRate)
	{
		BuildingAI buildingAI = this.m_powerHouse.m_buildingAI;
		if (buildingAI != null)
		{
			int num;
			int num2;
			buildingAI.GetElectricityProduction(out num, out num2);
			return StringUtils.SafeFormat(Locale.Get("TOOL_ELECTRICITY_ESTIMATE"), num2 * productionRate * 16 / 100000);
		}
		return null;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Electricity;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		float num2 = (Mathf.Max(0f, startHeight) + Mathf.Max(0f, endHeight)) * 0.5f;
		int num3 = Mathf.RoundToInt(num / 8f + 0.1f);
		int num4 = Mathf.RoundToInt(num2 / 4f + 0.1f);
		int result = this.m_constructionCost * num3 + this.m_elevationCost * num4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return 20000f + this.m_info.m_halfWidth;
	}

	public override bool DisplayTempSegment()
	{
		return true;
	}

	public override bool BuildOnWater()
	{
		return true;
	}

	public override bool LinearMiddleHeight()
	{
		return true;
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (maxElevation > 255f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Middle) != NetNode.Flags.None)
		{
			building = this.m_nodeBuilding;
			heightOffset = -100f;
		}
		else
		{
			building = null;
			heightOffset = -100f;
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if ((startPoint.m_node != 0 || startPoint.m_segment != 0) && (endPoint.m_node != 0 || endPoint.m_segment != 0))
		{
			float y = Mathf.Min(startPoint.m_position.y, endPoint.m_position.y);
			if (Mathf.Abs(startPoint.m_position.y - endPoint.m_position.y) > 0.1f)
			{
				startPoint.m_node = 0;
				startPoint.m_segment = 0;
				startPoint.m_position.y = y;
				endPoint.m_node = 0;
				endPoint.m_segment = 0;
				endPoint.m_position.y = y;
			}
		}
		else if (startPoint.m_node != 0 || startPoint.m_segment != 0)
		{
			float num = Mathf.Min(startPoint.m_position.y, endPoint.m_position.y);
			if (startPoint.m_position.y - num > 1f)
			{
				startPoint.m_node = 0;
				startPoint.m_segment = 0;
				startPoint.m_position.y = num;
			}
			else if (startPoint.m_position.y - num > 0.1f)
			{
				num = startPoint.m_position.y - 0.1f;
			}
			endPoint.m_position.y = num;
		}
		else if (endPoint.m_node != 0 || endPoint.m_segment != 0)
		{
			float num2 = Mathf.Min(startPoint.m_position.y, endPoint.m_position.y);
			if (endPoint.m_position.y - num2 > 1f)
			{
				endPoint.m_node = 0;
				endPoint.m_segment = 0;
				endPoint.m_position.y = num2;
			}
			else if (endPoint.m_position.y - num2 > 0.1f)
			{
				num2 = endPoint.m_position.y - 0.1f;
			}
			startPoint.m_position.y = num2;
		}
		else
		{
			float num3 = Mathf.Max(startPoint.m_position.y, endPoint.m_position.y);
			num3 = Mathf.Min(num3, Mathf.Min(startPoint.m_position.y + 1f, endPoint.m_position.y + 1f));
			startPoint.m_position.y = num3;
			endPoint.m_position.y = num3;
		}
		float num4 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(startPoint.m_position);
		float num5 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(endPoint.m_position);
		if (num4 > startPoint.m_position.y + 8f || num5 > endPoint.m_position.y + 8f)
		{
			toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
		}
		middlePoint.m_position = (startPoint.m_position + endPoint.m_position) * 0.5f;
		Segment2 segment = Segment2.XZ(startPoint.m_position, endPoint.m_position);
		Vector3 vector;
		Vector3 vector2;
		if (Singleton<TerrainManager>.get_instance().CalculateWaterFlow(segment, 32f, out vector, out vector2))
		{
			float num6;
			Vector3 vector3 = VectorUtils.NormalizeXZ(endPoint.m_position - startPoint.m_position, ref num6);
			float num7 = num6 * Mathf.Min(200f, num6) * 0.00125f;
			float num8 = vector2.x * vector3.z - vector2.z * vector3.x;
			if (num8 > 0f)
			{
				vector3.x = -vector3.x;
				vector3.z = -vector3.z;
			}
			middlePoint.m_position.x = middlePoint.m_position.x + vector3.z * num7;
			middlePoint.m_position.z = middlePoint.m_position.z - vector3.x * num7;
			middlePoint.m_direction = VectorUtils.NormalizeXZ(middlePoint.m_position - startPoint.m_position);
			endPoint.m_direction = VectorUtils.NormalizeXZ(endPoint.m_position - middlePoint.m_position);
			Bezier3 bezier;
			bezier.a = startPoint.m_position;
			bezier.d = endPoint.m_position;
			NetSegment.CalculateMiddlePoints(bezier.a, middlePoint.m_direction, bezier.d, -endPoint.m_direction, true, true, out bezier.b, out bezier.c);
			ownerBuilding = this.m_powerHouse;
			ownerPosition = bezier.Position(0.5f);
			ownerDirection..ctor(-vector3.z, 0f, vector3.x);
			float num9 = Mathf.Max(this.m_powerHouse.m_size.y, ownerPosition.y - vector.y);
			float num10 = this.m_powerHouseOffsetTop + (this.m_powerHouseOffsetBottom - this.m_powerHouseOffsetTop) * (num9 / -this.m_info.m_minHeight);
			ownerPosition.x -= vector3.z * num10;
			ownerPosition.y -= num9;
			ownerPosition.z += vector3.x * num10;
			float num11 = middlePoint.m_position.y - vector.y - 8f;
			productionRate = Mathf.Clamp(Mathf.CeilToInt(Mathf.Abs(num8) * num6 * 448f * num11 * 1E-05f), 0, 100);
			float num12 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(middlePoint.m_position);
			if (num4 < startPoint.m_position.y - 255f || num12 < middlePoint.m_position.y - 255f || num5 < endPoint.m_position.y - 255f)
			{
				toolErrors |= ToolBase.ToolErrors.HeightTooHigh;
			}
		}
		else
		{
			ownerBuilding = null;
			ownerPosition = Vector3.get_zero();
			ownerDirection = Vector3.get_forward();
			productionRate = 0;
			toolErrors |= ToolBase.ToolErrors.WaterNotFound;
		}
		return toolErrors;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	protected override bool HasMaintenanceCost(ushort segmentID, ref NetSegment data)
	{
		return (data.m_flags & NetSegment.Flags.Original) == NetSegment.Flags.None;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			ushort num = 0;
			if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
			{
				num = NetSegment.FindOwnerBuilding(segmentID, 363f);
				int num2 = 0;
				while (num != 0 && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Untouchable) != Building.Flags.None)
				{
					num = instance.m_buildings.m_buffer[(int)num].m_parentBuilding;
					if (++num2 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (num != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if ((instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Demolishing) == Building.Flags.None && !info.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, true, true, 0))
				{
					return false;
				}
			}
			Singleton<NetManager>.get_instance().ReleaseSegment(segmentID, false);
			if (num != 0)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num].Info;
				info2.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, false, true, 0);
			}
			return true;
		}
		return false;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
	}

	public override bool WorksAsBuilding()
	{
		return true;
	}

	public override bool WorksAsNet()
	{
		return false;
	}

	public override bool ShowTerrainTopography()
	{
		return true;
	}

	public override string GetLocalizedTooltip()
	{
		BuildingAI buildingAI = this.m_powerHouse.m_buildingAI;
		if (buildingAI != null)
		{
			int num;
			int num2;
			buildingAI.GetElectricityProduction(out num, out num2);
			string text;
			if (num2 > num)
			{
				text = LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_PRODUCTION_RANGE", new object[]
				{
					(num * 16 + 999) / 1000,
					(num2 * 16 + 999) / 1000
				});
			}
			else
			{
				text = LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_PRODUCTION", new object[]
				{
					(num2 * 16 + 999) / 1000
				});
			}
			int num3;
			int num4;
			buildingAI.GetPollutionAccumulation(out num3, out num4);
			int maintenanceCost = buildingAI.GetMaintenanceCost();
			return TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
				{
					buildingAI.GetWaterConsumption() * 16
				}),
				LocaleFormatter.Info2,
				text,
				LocaleFormatter.Pollution,
				LocaleFormatter.FormatGeneric("AIINFO_POLLUTION", new object[]
				{
					num3
				}),
				LocaleFormatter.NoisePollution,
				LocaleFormatter.FormatGeneric("AIINFO_NOISEPOLLUTION", new object[]
				{
					num4
				}),
				LocaleFormatter.Cost,
				LocaleFormatter.FormatCost(this.GetConstructionCost(), true),
				LocaleFormatter.Upkeep,
				LocaleFormatter.FormatUpkeep(maintenanceCost, false),
				LocaleFormatter.IsUpkeepVisible,
				(maintenanceCost != 0).ToString()
			});
		}
		return null;
	}

	public override string GenerateName(ushort segmentID, ref NetSegment data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)data.m_nameSeed);
		string text = null;
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
		{
			text = "ROAD_NAME_PATTERN";
		}
		if (text == null)
		{
			return string.Empty;
		}
		uint num = Locale.Count(text);
		string format = Locale.Get(text, randomizer.Int32(num));
		string arg = base.GenerateStreetName(ref randomizer);
		return StringUtils.SafeFormat(format, arg);
	}

	public BuildingInfo m_powerHouse;

	public float m_powerHouseOffsetBottom;

	public float m_powerHouseOffsetTop;

	public BuildingInfo m_nodeBuilding;

	public int m_elevationCost = 2000;
}
