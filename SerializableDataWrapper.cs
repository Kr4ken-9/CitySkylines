﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Packaging;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class SerializableDataWrapper : ISerializableData
{
	public SerializableDataWrapper(SimulationManager simulationManager)
	{
		this.m_simulationManager = simulationManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnSerializableDataExtensionsReleased();
		this.m_SerializableDataExtensions = Singleton<PluginManager>.get_instance().GetImplementations<ISerializableDataExtension>();
		this.OnSerializableDataExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnSerializableDataExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnSerializableDataExtensionsCreated()
	{
		for (int i = 0; i < this.m_SerializableDataExtensions.Count; i++)
		{
			try
			{
				this.m_SerializableDataExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_SerializableDataExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnSerializableDataExtensionsReleased()
	{
		for (int i = 0; i < this.m_SerializableDataExtensions.Count; i++)
		{
			try
			{
				this.m_SerializableDataExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_SerializableDataExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnLoadData()
	{
		for (int i = 0; i < this.m_SerializableDataExtensions.Count; i++)
		{
			try
			{
				this.m_SerializableDataExtensions[i].OnLoadData();
			}
			catch (Exception ex2)
			{
				Type type = this.m_SerializableDataExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnSaveData()
	{
		for (int i = 0; i < this.m_SerializableDataExtensions.Count; i++)
		{
			try
			{
				this.m_SerializableDataExtensions[i].OnSaveData();
			}
			catch (Exception ex2)
			{
				Type type = this.m_SerializableDataExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public string[] EnumerateData()
	{
		while (!Monitor.TryEnter(this.m_simulationManager.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		string[] result;
		try
		{
			Dictionary<string, byte[]>.KeyCollection keys = this.m_simulationManager.m_serializableDataStorage.Keys;
			string[] array = new string[keys.Count];
			keys.CopyTo(array, 0);
			result = array;
		}
		finally
		{
			Monitor.Exit(this.m_simulationManager.m_serializableDataStorage);
		}
		return result;
	}

	public byte[] LoadData(string id)
	{
		while (!Monitor.TryEnter(this.m_simulationManager.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		byte[] result;
		try
		{
			byte[] array;
			if (this.m_simulationManager.m_serializableDataStorage.TryGetValue(id, out array))
			{
				result = array;
			}
			else
			{
				result = null;
			}
		}
		finally
		{
			Monitor.Exit(this.m_simulationManager.m_serializableDataStorage);
		}
		return result;
	}

	public void SaveData(string id, byte[] data)
	{
		while (!Monitor.TryEnter(this.m_simulationManager.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_simulationManager.m_serializableDataStorage[id] = data;
		}
		finally
		{
			Monitor.Exit(this.m_simulationManager.m_serializableDataStorage);
		}
	}

	public void EraseData(string id)
	{
		while (!Monitor.TryEnter(this.m_simulationManager.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_simulationManager.m_serializableDataStorage.ContainsKey(id))
			{
				this.m_simulationManager.m_serializableDataStorage.Remove(id);
			}
		}
		finally
		{
			Monitor.Exit(this.m_simulationManager.m_serializableDataStorage);
		}
	}

	public bool SaveGame(string saveName)
	{
		SavePanel savePanel = UIView.get_library().Get<SavePanel>("SavePanel");
		return savePanel != null && savePanel.SaveGame(saveName);
	}

	public bool LoadGame(string saveName)
	{
		if (!SavePanel.isSaving && Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			Package package = PackageManager.GetPackage(saveName);
			foreach (Package.Asset current in package.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.SaveGameMetaData
			}))
			{
				if (!PackageHelper.IsDemoModeSave(current))
				{
					if (current != null && current.get_isEnabled())
					{
						SaveGameMetaData saveGameMetaData = current.Instantiate<SaveGameMetaData>();
						SimulationMetaData simulationMetaData = new SimulationMetaData
						{
							m_CityName = saveGameMetaData.cityName,
							m_updateMode = SimulationManager.UpdateMode.LoadGame
						};
						if (Singleton<PluginManager>.get_instance().get_enabledModCount() > 0)
						{
							simulationMetaData.m_disableAchievements = SimulationMetaData.MetaBool.True;
						}
						Singleton<LoadingManager>.get_instance().LoadLevel(saveGameMetaData.assetRef, "Game", "InGame", simulationMetaData);
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	private SimulationManager m_simulationManager;

	private List<ISerializableDataExtension> m_SerializableDataExtensions = new List<ISerializableDataExtension>();
}
