﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class ExpandableItem : UICustomControl
{
	public bool isEnabled
	{
		get
		{
			return this.m_button.get_isEnabled();
		}
		set
		{
			this.m_button.set_isEnabled(value);
		}
	}

	private void Awake()
	{
		this.m_button = base.Find<UIMultiStateButton>("ExpandButton");
		this.m_button.add_eventActiveStateIndexChanged(new PropertyChangedEventHandler<int>(this.OnButtonStateIndexChanged));
		this.m_contentPanel = base.Find<UIPanel>("ContentPanel");
		this.m_contentPanel.add_eventFitChildren(new FitChildrenEventHandler(this.OnContentSizeChanged));
		this.ForceCollapse(0f);
	}

	public void OnContentSizeChanged()
	{
		if (this.isExpanded)
		{
			this.m_expandedHeight = this.m_button.get_height() + this.m_contentPanel.get_height();
			base.get_component().set_height(this.m_expandedHeight);
		}
	}

	private void OnButtonStateIndexChanged(UIComponent component, int index)
	{
		if (index == 0)
		{
			this.Collapse(0.2f);
		}
		else
		{
			this.Expand(0.2f);
		}
	}

	private void Expand(float time)
	{
		this.m_expandedHeight = this.m_button.get_height() + this.m_contentPanel.get_height();
		this.isExpanded = true;
		ValueAnimator.Animate(delegate(float val)
		{
			base.get_component().set_height(val);
		}, new AnimatedFloat(this.m_button.get_height(), this.m_expandedHeight, time), delegate
		{
			base.get_component().set_height(this.m_expandedHeight);
		});
	}

	private void Collapse(float time)
	{
		this.isExpanded = false;
		ValueAnimator.Animate(delegate(float val)
		{
			base.get_component().set_height(val);
		}, new AnimatedFloat(this.m_expandedHeight, this.m_button.get_height(), time), delegate
		{
			base.get_component().set_height(this.m_button.get_height());
		});
	}

	public void ForceCollapse(float time = 0f)
	{
		this.Collapse(time);
		this.m_button.set_activeStateIndex(0);
	}

	public void ForceExpand(float time = 0f)
	{
		this.Expand(time);
		this.m_button.set_activeStateIndex(1);
	}

	public void RemoveTitle()
	{
		this.m_contentPanel.remove_eventFitChildren(new FitChildrenEventHandler(this.OnContentSizeChanged));
		this.m_button.set_isVisible(false);
		base.get_component().set_height(this.m_contentPanel.get_height());
	}

	private UIMultiStateButton m_button;

	private UIPanel m_contentPanel;

	public ExpandableItem m_parentExpandableItem;

	private float m_expandedHeight;

	private bool isExpanded;
}
