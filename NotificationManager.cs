﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class NotificationManager : SimulationManagerBase<NotificationManager, NotificationProperties>, IRenderableManager
{
	public bool NotificationsVisible
	{
		get
		{
			return this.m_notificationsVisible;
		}
		set
		{
			this.m_notificationsVisible = value;
		}
	}

	public bool FadeAwayNotifications
	{
		get
		{
			return this.m_fadeAwayNotifications;
		}
		set
		{
			if (value != this.m_fadeAwayNotifications)
			{
				this.m_fadeAwayNotifications = value;
				if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					Singleton<RenderManager>.get_instance().UpdateGroups(this.m_notificationLayer);
				}
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.ID_AtlasRegions = Shader.PropertyToID("_AtlasRegions");
		this.ID_Positions = Shader.PropertyToID("_Positions");
		this.ID_Params = Shader.PropertyToID("_Params");
		this.m_AtlasRegions = new Vector4[16];
		this.m_Positions = new Vector4[16];
		this.m_Params = new Vector4[16];
		this.m_bufferedItems = new FastList<NotificationManager.BufferedItem>();
		this.m_itemComparer = new NotificationManager.ItemComparer();
		this.m_events = new FastList<NotificationEvent>();
		this.m_spriteAtlasRegions = new Vector4[143];
		int num = 25;
		this.m_groupData = new NotificationManager.GroupData[num * num * 39];
		this.m_groupData2 = new NotificationManager.GroupData[num * num * 39];
		this.m_groupDataCount = new byte[num * num];
		SavedBool savedBool = new SavedBool(Settings.fadeAwayNotifications, Settings.gameSettingsFile, DefaultSettings.fadeAwayNotifications);
		this.m_fadeAwayNotifications = savedBool.get_value();
		this.m_notificationLayer = LayerMask.NameToLayer("Notifications");
		this.m_notificationsVisible = true;
		this.GenerateNotificationMesh();
	}

	private void OnDestroy()
	{
		if (this.m_notificationMesh != null)
		{
			Object.Destroy(this.m_notificationMesh);
			this.m_notificationMesh = null;
		}
		if (this.m_notificationMaterial != null)
		{
			Object.Destroy(this.m_notificationMaterial);
			this.m_notificationMaterial = null;
		}
	}

	private void Update()
	{
		float deltaTime = Time.get_deltaTime();
		if (this.m_notificationsVisible)
		{
			this.m_notificationAlpha = Mathf.Min(1f, this.m_notificationAlpha + deltaTime * 2f);
		}
		else
		{
			this.m_notificationAlpha = Mathf.Max(0f, this.m_notificationAlpha - deltaTime * 2f);
		}
		while (!Monitor.TryEnter(this.m_events, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			int i = 0;
			while (i < this.m_events.m_size)
			{
				if (this.m_events.m_buffer[i].UpdateEvent(deltaTime))
				{
					i++;
				}
				else
				{
					this.m_events.m_buffer[i] = this.m_events.m_buffer[--this.m_events.m_size];
				}
			}
		}
		finally
		{
			Monitor.Exit(this.m_events);
		}
	}

	public override void InitializeProperties(NotificationProperties properties)
	{
		base.InitializeProperties(properties);
		for (int i = 0; i < 39; i++)
		{
			Notification.Problem problem = (Notification.Problem)(1L << i);
			string text = EnumExtensions.Name<Notification.Problem>(problem, "Normal");
			string text2 = EnumExtensions.Name<Notification.Problem>(problem, "Major");
			string text3 = EnumExtensions.Name<Notification.Problem>(problem, "Fatal");
			if (!string.IsNullOrEmpty(text))
			{
				UITextureAtlas.SpriteInfo spriteInfo = properties.m_notificationAtlas.get_Item(text);
				if (spriteInfo != null)
				{
					Rect region = spriteInfo.get_region();
					this.m_spriteAtlasRegions[i] = new Vector4(region.get_xMin(), region.get_yMin(), region.get_xMax(), region.get_yMax());
				}
			}
			if (!string.IsNullOrEmpty(text2))
			{
				UITextureAtlas.SpriteInfo spriteInfo2 = properties.m_notificationAtlas.get_Item(text2);
				if (spriteInfo2 != null)
				{
					Rect region2 = spriteInfo2.get_region();
					this.m_spriteAtlasRegions[39 + i] = new Vector4(region2.get_xMin(), region2.get_yMin(), region2.get_xMax(), region2.get_yMax());
				}
			}
			if (!string.IsNullOrEmpty(text3))
			{
				UITextureAtlas.SpriteInfo spriteInfo3 = properties.m_notificationAtlas.get_Item(text3);
				if (spriteInfo3 != null)
				{
					Rect region3 = spriteInfo3.get_region();
					this.m_spriteAtlasRegions[78 + i] = new Vector4(region3.get_xMin(), region3.get_yMin(), region3.get_xMax(), region3.get_yMax());
				}
			}
		}
		for (int j = 0; j < 26; j++)
		{
			NotificationEvent.Type type = (NotificationEvent.Type)j;
			string text4 = EnumExtensions.Name<NotificationEvent.Type>(type);
			UITextureAtlas.SpriteInfo spriteInfo4 = properties.m_notificationAtlas.get_Item(text4);
			if (spriteInfo4 != null)
			{
				Rect region4 = spriteInfo4.get_region();
				this.m_spriteAtlasRegions[117 + j] = new Vector4(region4.get_xMin(), region4.get_yMin(), region4.get_xMax(), region4.get_yMax());
			}
		}
		this.m_notificationMaterial = new Material(this.m_properties.m_notificationAtlas.get_material());
		this.m_notificationMaterial.set_shader(this.m_properties.m_notificationShader);
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
	}

	public override void DestroyProperties(NotificationProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_notificationMaterial != null)
			{
				Object.Destroy(this.m_notificationMaterial);
				this.m_notificationMaterial = null;
			}
			this.m_cameraController = null;
		}
		base.DestroyProperties(properties);
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		FastList<RenderGroup> renderedGroups = Singleton<RenderManager>.get_instance().m_renderedGroups;
		for (int i = 0; i < renderedGroups.m_size; i++)
		{
			RenderGroup renderGroup = renderedGroups.m_buffer[i];
			if ((renderGroup.m_layersRendered & ~(renderGroup.m_instanceMask != 0) & 1 << this.m_notificationLayer) != 0)
			{
				int num = 25;
				int num2 = 45 - num >> 1;
				int num3 = renderGroup.m_x - num2;
				int num4 = renderGroup.m_z - num2;
				if (num3 >= 0 && num4 >= 0 && num3 < num && num4 < num)
				{
					int num5 = num4 * num + num3;
					int num6 = (int)this.m_groupDataCount[num5];
					num5 *= 39;
					for (int j = 0; j < num6; j++)
					{
						NotificationManager.GroupData groupData = this.m_groupData2[num5 + j];
						if (groupData.m_problems != Notification.Problem.None)
						{
							Vector3 position;
							position.x = (groupData.m_minPos.x + groupData.m_maxPos.x) * 0.5f;
							position.y = groupData.m_maxPos.y;
							position.z = (groupData.m_minPos.z + groupData.m_maxPos.z) * 0.5f;
							Notification.RenderInstance(cameraInfo, groupData.m_problems, position, groupData.m_size);
						}
					}
				}
			}
		}
		for (int k = 0; k < this.m_events.m_size; k++)
		{
			this.m_events.m_buffer[k].RenderInstance(cameraInfo);
		}
		if (this.m_cameraController != null)
		{
			InstanceID target = this.m_cameraController.GetTarget();
			Vector3 position2;
			Quaternion quaternion;
			Vector3 vector;
			if (!target.IsEmpty && target.Type != InstanceType.Disaster && target != WorldInfoPanel.GetCurrentInstanceID() && InstanceManager.GetPosition(target, out position2, out quaternion, out vector))
			{
				position2.y += vector.y * 0.85f + 2f;
				NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.LocationMarker, position2, 0.75f, 1f);
			}
		}
	}

	protected override void EndOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_bufferedItems.m_size == 0)
		{
			return;
		}
		if (this.m_notificationAlpha >= 0.001f && this.m_notificationMaterial != null)
		{
			NotificationManager.SortItems(this.m_bufferedItems.m_buffer, 0, this.m_bufferedItems.m_size - 1);
			int num = 0;
			int i = 0;
			while (i < this.m_bufferedItems.m_size)
			{
				int num2 = Mathf.Min(16, this.m_bufferedItems.m_size - i);
				for (int j = 0; j < num2; j++)
				{
					NotificationManager.BufferedItem bufferedItem = this.m_bufferedItems.m_buffer[i++];
					Vector4 @params = bufferedItem.m_params;
					@params.y *= this.m_notificationAlpha;
					this.m_AtlasRegions[j] = this.m_spriteAtlasRegions[bufferedItem.m_regionIndex];
					this.m_Positions[j] = bufferedItem.m_position;
					this.m_Params[j] = @params;
				}
				for (int k = num2; k < 16; k++)
				{
					this.m_AtlasRegions[k] = Vector4.get_zero();
					this.m_Positions[k] = cameraInfo.m_forward * -100000f;
					this.m_Params[k] = Vector4.get_zero();
				}
				this.m_notificationMaterial.SetVectorArray(this.ID_AtlasRegions, this.m_AtlasRegions);
				this.m_notificationMaterial.SetVectorArray(this.ID_Positions, this.m_Positions);
				this.m_notificationMaterial.SetVectorArray(this.ID_Params, this.m_Params);
				if (this.m_notificationMaterial.SetPass(0))
				{
					num++;
					Graphics.DrawMeshNow(this.m_notificationMesh, Matrix4x4.get_identity());
				}
			}
			this.m_drawCallData.m_overlayCalls = this.m_drawCallData.m_overlayCalls + num;
		}
		this.m_bufferedItems.Clear();
	}

	private static void SortItems(NotificationManager.BufferedItem[] buffer, int first, int last)
	{
		if (first < last)
		{
			int num = NotificationManager.SortItemsPartition(buffer, first, last);
			NotificationManager.SortItems(buffer, first, num);
			NotificationManager.SortItems(buffer, num + 1, last);
		}
	}

	private static int SortItemsPartition(NotificationManager.BufferedItem[] buffer, int first, int last)
	{
		float distanceSqr = buffer[first + last >> 1].m_distanceSqr;
		first--;
		last++;
		while (true)
		{
			last--;
			if (distanceSqr <= buffer[last].m_distanceSqr)
			{
				do
				{
					first++;
				}
				while (distanceSqr < buffer[first].m_distanceSqr);
				if (first < last)
				{
					NotificationManager.BufferedItem bufferedItem = buffer[first];
					buffer[first] = buffer[last];
					buffer[last] = bufferedItem;
				}
				if (first >= last)
				{
					break;
				}
			}
		}
		return last;
	}

	private void GenerateNotificationMesh()
	{
		Vector3[] array = new Vector3[64];
		Vector2[] array2 = new Vector2[64];
		Vector2[] array3 = new Vector2[64];
		int[] array4 = new int[96];
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < 16; i++)
		{
			float num3 = (float)i;
			array[num] = new Vector3(-1f, -1f, num3);
			array2[num] = new Vector2(0f, 0f);
			array3[num] = new Vector2(-1f, -1f);
			num++;
			array[num] = new Vector3(-1f, 1f, num3);
			array2[num] = new Vector2(0f, 1f);
			array3[num] = new Vector2(-1f, 1f);
			num++;
			array[num] = new Vector3(1f, 1f, num3);
			array2[num] = new Vector2(1f, 1f);
			array3[num] = new Vector2(1f, 1f);
			num++;
			array[num] = new Vector3(1f, -1f, num3);
			array2[num] = new Vector2(1f, 0f);
			array3[num] = new Vector2(1f, -1f);
			num++;
			array4[num2++] = num - 4;
			array4[num2++] = num - 3;
			array4[num2++] = num - 2;
			array4[num2++] = num - 2;
			array4[num2++] = num - 1;
			array4[num2++] = num - 4;
		}
		this.m_notificationMesh = new Mesh();
		this.m_notificationMesh.set_vertices(array);
		this.m_notificationMesh.set_uv(array2);
		this.m_notificationMesh.set_uv2(array3);
		this.m_notificationMesh.set_triangles(array4);
	}

	public void AddWaveEvent(Vector3 position, NotificationEvent.Type type, ImmaterialResourceManager.Resource resource, float amount, float radius)
	{
		this.AddWaveEvent(position, type, resource, NaturalResourceManager.Resource.None, amount, radius, position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.None, NaturalResourceManager.Resource.None, 0f, 0f);
	}

	public void AddWaveEvent(Vector3 position1, NotificationEvent.Type type1, ImmaterialResourceManager.Resource resource1, float amount1, float radius1, Vector3 position2, NotificationEvent.Type type2, ImmaterialResourceManager.Resource resource2, float amount2, float radius2)
	{
		this.AddWaveEvent(position1, type1, resource1, NaturalResourceManager.Resource.None, amount1, radius1, position2, type2, resource2, NaturalResourceManager.Resource.None, amount2, radius2);
	}

	public void AddWaveEvent(Vector3 position1, NotificationEvent.Type type1, ImmaterialResourceManager.Resource resource1, float amount1, float radius1, Vector3 position2, NotificationEvent.Type type2, NaturalResourceManager.Resource resource2, float amount2, float radius2)
	{
		this.AddWaveEvent(position1, type1, resource1, NaturalResourceManager.Resource.None, amount1, radius1, position2, type2, ImmaterialResourceManager.Resource.None, resource2, amount2, radius2);
	}

	public void AddWaveEvent(Vector3 position, NotificationEvent.Type type, NaturalResourceManager.Resource resource, float amount, float radius)
	{
		this.AddWaveEvent(position, type, ImmaterialResourceManager.Resource.None, resource, amount, radius, position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.None, NaturalResourceManager.Resource.None, 0f, 0f);
	}

	private void AddWaveEvent(Vector3 position1, NotificationEvent.Type type1, ImmaterialResourceManager.Resource resourceA1, NaturalResourceManager.Resource resourceB1, float amount1, float radius1, Vector3 position2, NotificationEvent.Type type2, ImmaterialResourceManager.Resource resourceA2, NaturalResourceManager.Resource resourceB2, float amount2, float radius2)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		float num = Mathf.Min(position1.x - radius1, position2.x - radius2);
		float num2 = Mathf.Min(position1.z - radius1, position2.z - radius2);
		float num3 = Mathf.Max(position1.x + radius1, position2.x + radius2);
		float num4 = Mathf.Max(position1.z + radius1, position2.z + radius2);
		int num5 = Mathf.Max((int)(num / 64f + 135f), 0);
		int num6 = Mathf.Max((int)(num2 / 64f + 135f), 0);
		int num7 = Mathf.Min((int)(num3 / 64f + 135f), 269);
		int num8 = Mathf.Min((int)(num4 / 64f + 135f), 269);
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					num9 = this.AddWaveEvent(num9, ref instance.m_buildings.m_buffer[(int)num9], position1, type1, resourceA1, resourceB1, amount1, radius1, position2, type2, resourceA2, resourceB2, amount2, radius2);
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private ushort AddWaveEvent(ushort buildingID, ref Building buildingData, Vector3 position1, NotificationEvent.Type type1, ImmaterialResourceManager.Resource resourceA1, NaturalResourceManager.Resource resourceB1, float amount1, float radius1, Vector3 position2, NotificationEvent.Type type2, ImmaterialResourceManager.Resource resourceA2, NaturalResourceManager.Resource resourceB2, float amount2, float radius2)
	{
		float num = Vector3.Distance(buildingData.m_position, position1);
		float num2 = Vector3.Distance(buildingData.m_position, position2);
		bool flag = num < radius1;
		bool flag2 = num2 < radius2;
		if (flag || flag2)
		{
			BuildingInfo info = buildingData.Info;
			if (info != null)
			{
				if (flag)
				{
					amount1 *= 1f - num / radius1;
					if (resourceA1 != ImmaterialResourceManager.Resource.None)
					{
						amount1 = info.m_buildingAI.GetEventImpact(buildingID, ref buildingData, resourceA1, amount1);
					}
					else
					{
						amount1 = info.m_buildingAI.GetEventImpact(buildingID, ref buildingData, resourceB1, amount1);
					}
					amount1 = Mathf.Abs(amount1);
				}
				if (flag2)
				{
					amount2 *= 1f - num2 / radius2;
					if (resourceA2 != ImmaterialResourceManager.Resource.None)
					{
						amount2 = info.m_buildingAI.GetEventImpact(buildingID, ref buildingData, resourceA2, amount2);
					}
					else
					{
						amount2 = info.m_buildingAI.GetEventImpact(buildingID, ref buildingData, resourceB2, amount2);
					}
					amount2 = Mathf.Abs(amount2);
					if (!flag || amount2 > amount1)
					{
						amount1 = amount2;
						type1 = type2;
						num = num2;
					}
				}
				if (amount1 > 0.01f)
				{
					Vector3 position3 = buildingData.m_position;
					position3.y += info.m_size.y;
					if (amount1 >= 0.2f)
					{
						float scale = 1f + (amount1 - 0.2f) * 1.25f;
						this.AddEvent(type1, position3, scale, num * 0.004f);
					}
					else if ((float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < amount1 * 500f)
					{
						this.AddEvent(type1, position3, 1f, num * 0.004f);
					}
				}
			}
		}
		return buildingData.m_nextGridBuilding;
	}

	public void AddEvent(NotificationEvent.Type type, Vector3 position, float scale)
	{
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		if (properties != null && properties.m_mode == ItemClass.Availability.AssetEditor)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_events, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_events.Add(new NotificationEvent(type, position, scale));
		}
		finally
		{
			Monitor.Exit(this.m_events);
		}
	}

	public void AddEvent(NotificationEvent.Type type, Vector3 position, float scale, float delay)
	{
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		if (properties != null && properties.m_mode == ItemClass.Availability.AssetEditor)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_events, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_events.Add(new NotificationEvent(type, position, scale, delay));
		}
		finally
		{
			Monitor.Exit(this.m_events);
		}
	}

	public void AddRemovalEvent(Notification.Problem notifications, Vector3 position, float scale)
	{
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		if (properties != null && properties.m_mode == ItemClass.Availability.AssetEditor)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_events, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_events.Add(new NotificationEvent(NotificationEvent.Type.NotificationRemoval, notifications, position, scale));
		}
		finally
		{
			Monitor.Exit(this.m_events);
		}
	}

	public override bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		if (layer == this.m_notificationLayer)
		{
			int num = 25;
			int num2 = 45 - num >> 1;
			groupX -= num2;
			groupZ -= num2;
			if (groupX >= 0 && groupZ >= 0 && groupX < num && groupZ < num)
			{
				int num3 = (groupZ * num + groupX) * 39;
				for (int i = 0; i < 39; i++)
				{
					NotificationManager.GroupData groupData;
					groupData.m_problems = Notification.Problem.None;
					groupData.m_size = 0f;
					groupData.m_minPos = new Vector3(100000f, 100000f, 100000f);
					groupData.m_maxPos = new Vector3(-100000f, -100000f, -100000f);
					this.m_groupData[num3 + i] = groupData;
				}
			}
		}
		return false;
	}

	public override void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		if (layer == this.m_notificationLayer)
		{
			int num = 25;
			int num2 = 45 - num >> 1;
			groupX -= num2;
			groupZ -= num2;
			if (groupX >= 0 && groupZ >= 0 && groupX < num && groupZ < num)
			{
				int num3 = (groupZ * num + groupX) * 39;
				int num4 = 0;
				for (int i = 0; i < 39; i++)
				{
					NotificationManager.GroupData groupData = this.m_groupData[num3 + i];
					if (groupData.m_problems != Notification.Problem.None)
					{
						bool flag = false;
						for (int j = 0; j < num4; j++)
						{
							NotificationManager.GroupData groupData2 = this.m_groupData[num3 + j];
							if (groupData2.m_minPos == groupData.m_minPos && groupData2.m_maxPos == groupData.m_maxPos)
							{
								groupData2.m_problems = Notification.AddProblems(groupData2.m_problems, groupData.m_problems);
								groupData2.m_size += groupData.m_size;
								this.m_groupData[num3 + j] = groupData2;
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							this.m_groupData[num3 + num4] = groupData;
							num4++;
						}
					}
				}
				for (int k = 0; k < num4; k++)
				{
					NotificationManager.GroupData groupData3 = this.m_groupData[num3 + k];
					groupData3.m_size = Mathf.Min(2f, Mathf.Sqrt(groupData3.m_size));
					this.m_groupData2[num3 + k] = groupData3;
				}
				for (int l = num4; l < 39; l++)
				{
					this.m_groupData2[num3 + l] = default(NotificationManager.GroupData);
				}
				this.m_groupDataCount[groupZ * num + groupX] = (byte)num4;
			}
		}
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	[NonSerialized]
	public int ID_AtlasRegions;

	[NonSerialized]
	public int ID_Positions;

	[NonSerialized]
	public int ID_Params;

	[NonSerialized]
	public Vector4[] m_spriteAtlasRegions;

	[NonSerialized]
	public FastList<NotificationManager.BufferedItem> m_bufferedItems;

	[NonSerialized]
	public NotificationManager.ItemComparer m_itemComparer;

	[NonSerialized]
	public NotificationManager.GroupData[] m_groupData;

	[NonSerialized]
	private NotificationManager.GroupData[] m_groupData2;

	[NonSerialized]
	private byte[] m_groupDataCount;

	[NonSerialized]
	public Mesh m_notificationMesh;

	[NonSerialized]
	public Material m_notificationMaterial;

	[NonSerialized]
	public int m_notificationLayer;

	private bool m_notificationsVisible;

	private bool m_fadeAwayNotifications;

	private float m_notificationAlpha;

	private FastList<NotificationEvent> m_events;

	private CameraController m_cameraController;

	private Vector4[] m_AtlasRegions;

	private Vector4[] m_Positions;

	private Vector4[] m_Params;

	public struct BufferedItem
	{
		public Vector4 m_position;

		public Vector4 m_params;

		public float m_distanceSqr;

		public int m_regionIndex;
	}

	public struct GroupData
	{
		public Notification.Problem m_problems;

		public float m_size;

		public Vector3 m_minPos;

		public Vector3 m_maxPos;
	}

	public class ItemComparer : IComparer<NotificationManager.BufferedItem>
	{
		public int Compare(NotificationManager.BufferedItem x, NotificationManager.BufferedItem y)
		{
			if (x.m_distanceSqr > y.m_distanceSqr)
			{
				return -1;
			}
			if (x.m_distanceSqr < y.m_distanceSqr)
			{
				return 1;
			}
			return 0;
		}
	}
}
