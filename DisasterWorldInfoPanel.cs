﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class DisasterWorldInfoPanel : WorldInfoPanel
{
	public static DisasterWorldInfoPanel instance
	{
		get
		{
			if (DisasterWorldInfoPanel.m_instance == null)
			{
				DisasterWorldInfoPanel.m_instance = UIView.get_library().Get<DisasterWorldInfoPanel>("DisasterWorldInfoPanel");
			}
			return DisasterWorldInfoPanel.m_instance;
		}
	}

	public byte intensity
	{
		get
		{
			return this.m_intensity;
		}
		set
		{
			this.m_intensity = value;
			this.m_intensityNumberLabel.set_text(((float)value / 10f).ToString("F1"));
		}
	}

	public ushort disasterId
	{
		get
		{
			return this.m_disasterId;
		}
	}

	private static UIButton triggerPanelButton
	{
		get
		{
			if (DisasterWorldInfoPanel.m_triggerPanelButton == null)
			{
				DisasterWorldInfoPanel.m_triggerPanelButton = UIView.Find<UIButton>("Trigger");
			}
			return DisasterWorldInfoPanel.m_triggerPanelButton;
		}
	}

	protected override void Start()
	{
		base.Start();
		this.m_nameField = base.Find<UITextField>("NameTextField");
		this.m_nameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_description = base.Find<UILabel>("Description");
		this.m_intensitySlider = base.Find<UISlider>("Slider");
		this.m_intensitySlider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		this.m_intensityNumberLabel = base.Find<UILabel>("SeverityNumber");
		this.m_listBox = base.Find<UIListBox>("TriggerListBox");
		this.m_listBox.add_eventItemClicked(new PropertyChangedEventHandler<int>(this.OnTriggerClicked));
		this.m_thumbnail = base.Find<UISprite>("Thumbnail");
		this.m_icon = base.Find<UISprite>("DisasterIcon");
		this.m_panelTriggers = base.Find<UIPanel>("PanelTriggers");
		this.m_panelNoTriggers = base.Find<UIPanel>("PanelNoTriggers");
	}

	private void OnSliderValueChanged(UIComponent comp, float value)
	{
		this.intensity = (byte)value;
		Singleton<SimulationManager>.get_instance().AddAction(delegate
		{
			if (Singleton<DisasterManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Disaster && this.m_InstanceID.Disaster != 0)
			{
				ushort disaster = this.m_InstanceID.Disaster;
				Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)disaster].m_intensity = (byte)value;
			}
		});
	}

	protected override void OnSetTarget()
	{
		this.m_nameField.set_text(this.GetName());
		this.m_disasterId = this.m_InstanceID.Disaster;
		if (Singleton<DisasterManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Disaster && this.m_InstanceID.Disaster != 0)
		{
			ushort disaster = this.m_InstanceID.Disaster;
			DisasterData disasterData = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)disaster];
			DisasterInfo info = disasterData.Info;
			this.m_description.set_text(info.GetLocalizedDescription());
			this.m_intensitySlider.set_value((float)disasterData.m_intensity);
			this.m_thumbnail.set_spriteName(info.m_Thumbnail);
			this.m_icon.set_spriteName(info.m_Icon);
			this.RefreshTriggers(disaster);
		}
	}

	private void OnTriggerClicked(UIComponent comp, int index)
	{
		DisasterWorldInfoPanel.triggerPanelButton.SimulateClick();
		TriggerPanel.instance.SelectTrigger(this.m_triggerIndices[index]);
	}

	private void RefreshTriggers(ushort disasterID)
	{
		List<string> list = new List<string>();
		List<int> list2 = new List<int>();
		int num = 0;
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
		{
			TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
			for (int i = 0; i < scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = scenarioTriggers[i];
				if (triggerMilestone.m_effects != null)
				{
					TriggerEffect[] effects = triggerMilestone.m_effects;
					for (int j = 0; j < effects.Length; j++)
					{
						TriggerEffect triggerEffect = effects[j];
						DisasterEffect disasterEffect = triggerEffect as DisasterEffect;
						if (disasterEffect != null && disasterEffect.GetDisaster() == disasterID)
						{
							list.Add(triggerMilestone.GetLocalizedName());
							list2.Add(num);
						}
					}
				}
				num++;
			}
		}
		this.m_listBox.set_items(list.ToArray());
		this.m_triggerIndices = list2.ToArray();
		this.m_panelTriggers.set_isVisible(list.Count > 0);
		this.m_panelNoTriggers.set_isVisible(!this.m_panelTriggers.get_isVisible());
	}

	public static int CountTriggers(ushort disasterID)
	{
		int num = 0;
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
		{
			TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
			for (int i = 0; i < scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = scenarioTriggers[i];
				if (triggerMilestone.m_effects != null)
				{
					TriggerEffect[] effects = triggerMilestone.m_effects;
					for (int j = 0; j < effects.Length; j++)
					{
						TriggerEffect triggerEffect = effects[j];
						DisasterEffect disasterEffect = triggerEffect as DisasterEffect;
						if (disasterEffect != null && disasterEffect.GetDisaster() == disasterID)
						{
							num++;
						}
					}
				}
			}
		}
		return num;
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
	}

	public void TempHide()
	{
		if (base.get_component().get_isVisible())
		{
			ToolsModifierControl.cameraController.ClearTarget();
			ValueAnimator.Animate("Relocating", delegate(float val)
			{
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(1f, 0f, 0.2f), delegate
			{
				UIView.get_library().Hide(base.GetType().Name);
			});
			DisasterWorldInfoPanel.m_isTempHidden = true;
		}
	}

	public void TempShow(InstanceID instanceID)
	{
		if (DisasterWorldInfoPanel.m_isTempHidden)
		{
			Vector3 targetPosition = Singleton<DisasterManager>.get_instance().m_disasters[(int)instanceID.Disaster].m_targetPosition;
			WorldInfoPanel.Show<DisasterWorldInfoPanel>(targetPosition, instanceID);
			DisasterWorldInfoPanel.m_isTempHidden = false;
			ValueAnimator.Animate("Relocating", delegate(float val)
			{
				base.get_component().set_opacity(val);
			}, new AnimatedFloat(0f, 1f, 0.2f));
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		DisasterWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new DisasterWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		if (this.m_InstanceID.Type == InstanceType.Disaster && this.m_InstanceID.Disaster != 0)
		{
			return Singleton<DisasterManager>.get_instance().GetDisasterName(this.m_InstanceID.Disaster);
		}
		return string.Empty;
	}

	private UITextField m_nameField;

	private UILabel m_description;

	private UILabel m_intensityNumberLabel;

	private UISlider m_intensitySlider;

	private UISprite m_thumbnail;

	private UISprite m_icon;

	private UIPanel m_panelTriggers;

	private UIPanel m_panelNoTriggers;

	private UIListBox m_listBox;

	private int[] m_triggerIndices;

	private static bool m_isTempHidden;

	private static DisasterWorldInfoPanel m_instance;

	private byte m_intensity;

	private ushort m_disasterId;

	private static UIButton m_triggerPanelButton;
}
