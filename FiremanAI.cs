﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class FiremanAI : ServicePersonAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_waterEffect != null)
		{
			this.m_waterEffect.InitializeEffect();
		}
		this.m_hasHose = (this.m_hoseBone != null);
	}

	public override void ReleaseAI()
	{
		this.m_hasHose = false;
		if (this.m_waterEffect != null)
		{
			this.m_waterEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort instanceID, ref CitizenInstance data, Vector3 position, Quaternion rotation, Vector3 velocity, Color color, bool underground)
	{
		this.m_info.SetRenderParameters(position, rotation, velocity, color, 0, underground);
		if (this.m_hoseBone != null)
		{
			ushort vehicle = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_vehicle;
			if (vehicle != 0)
			{
				Vector3 smoothPosition = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetSmoothPosition(vehicle);
				this.m_hoseBone.set_position(smoothPosition);
			}
		}
		if (data.m_targetBuilding != 0 && data.m_waitCounter >= 8 && this.m_waterEffect != null && this.m_waterBone != null)
		{
			InstanceID id = default(InstanceID);
			id.CitizenInstance = instanceID;
			Vector3 direction = this.m_waterBone.TransformDirection(Vector3.get_forward());
			EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(this.m_waterBone.get_position(), direction, 0f);
			Vector3 velocity2 = velocity * 3.75f;
			this.m_waterEffect.RenderEffect(id, area, velocity2, 0f, 1f, -1f, Singleton<SimulationManager>.get_instance().m_simulationTimeDelta, cameraInfo);
		}
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.FireSafety)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		target = InstanceID.Empty;
		return Locale.Get("FIREMAN_STATUS");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void ReleaseInstance(ushort instanceID, ref CitizenInstance data)
	{
		if (data.m_citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_instance = 0;
			instance.ReleaseCitizen(data.m_citizen);
			data.m_citizen = 0u;
		}
		base.ReleaseInstance(instanceID, ref data);
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_vehicle == 0)
		{
			instance.ReleaseCitizenInstance(instanceID);
			return;
		}
		if (data.m_waitCounter < 255)
		{
			data.m_waitCounter += 1;
		}
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
				data.m_waitCounter = 0;
			}
			else
			{
				data.m_flags |= CitizenInstance.Flags.EnteringVehicle;
				data.Spawn(instanceID);
			}
		}
	}

	protected override void GetBuildingTargetPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		if (citizenData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			if (!this.m_hasHose)
			{
				Randomizer randomizer;
				randomizer..ctor((int)instanceID);
				Vector3 vector;
				Vector3 vector2;
				Vector2 targetDir;
				CitizenInstance.Flags flags;
				info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref randomizer, this.m_info, instanceID, out vector, out vector2, out targetDir, out flags);
				citizenData.m_targetPos = new Vector4(vector.x, vector.y, vector.z, 1f);
				citizenData.m_targetDir = targetDir;
				return;
			}
			ushort vehicle = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_vehicle;
			if (vehicle != 0)
			{
				Vector3 lastFramePosition = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetLastFramePosition();
				Vector3 vector3;
				Quaternion quaternion;
				instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].CalculateMeshPosition(out vector3, out quaternion);
				Vector3 vector4 = quaternion * new Vector3(0f, 0f, info.m_size.z * 0.5f);
				Vector3 vector5 = quaternion * new Vector3(info.m_size.x * 0.5f, 0f, 0f);
				Vector3 vector6 = vector3 - vector4 - vector5;
				Vector3 vector7 = vector3 - vector4 + vector5;
				Vector3 vector8 = vector3 + vector4 + vector5;
				Vector3 vector9 = vector3 + vector4 - vector5;
				float num2;
				float num = Segment3.DistanceSqr(vector6, vector7, lastFramePosition, ref num2);
				Vector3 vector10 = Vector3.Lerp(vector6, vector7, num2);
				float num3 = Segment3.DistanceSqr(vector7, vector8, lastFramePosition, ref num2);
				if (num3 < num)
				{
					num = num3;
					vector10 = Vector3.Lerp(vector7, vector8, num2);
				}
				num3 = Segment3.DistanceSqr(vector8, vector9, lastFramePosition, ref num2);
				if (num3 < num)
				{
					num = num3;
					vector10 = Vector3.Lerp(vector8, vector9, num2);
				}
				num3 = Segment3.DistanceSqr(vector9, vector6, lastFramePosition, ref num2);
				if (num3 < num)
				{
					vector10 = Vector3.Lerp(vector9, vector6, num2);
				}
				Vector3 vector11 = vector10 - lastFramePosition;
				float magnitude = vector11.get_magnitude();
				if (magnitude > 1f)
				{
					float num4 = Mathf.Max(magnitude - 5f, magnitude * 0.5f);
					vector10 = lastFramePosition + vector11 * (num4 / magnitude);
				}
				citizenData.m_targetPos = new Vector4(vector10.x, vector10.y, vector10.z, 1f);
				citizenData.m_targetDir = VectorUtils.XZ(vector11);
				return;
			}
		}
		citizenData.m_targetDir = Vector2.get_zero();
	}

	protected override bool EnterVehicle(ushort instanceID, ref CitizenInstance citizenData)
	{
		citizenData.Unspawn(instanceID);
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
		}
		return true;
	}

	protected override bool ArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData)
	{
		if (!this.m_hasHose)
		{
			citizenData.Unspawn(instanceID);
		}
		return false;
	}

	protected override void ArriveAtDestination(ushort instanceID, ref CitizenInstance citizenData, bool success)
	{
	}

	public EffectInfo m_waterEffect;

	public Transform m_hoseBone;

	public Transform m_waterBone;

	[NonSerialized]
	private bool m_hasHose;
}
