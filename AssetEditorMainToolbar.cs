﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class AssetEditorMainToolbar : MainToolbar
{
	protected override void Awake()
	{
		base.Awake();
		this.m_SubBuildingButton = UIView.Find<UIButton>("SubBuildingButton");
		this.m_SubBuildingButton.add_eventClicked(new MouseEventHandler(this.OnSubBuildingClick));
		this.m_SubBuildingButton.Hide();
		this.m_SelectRef = GameObject.Find("SelectReference").GetComponent<UIPanel>();
		this.m_SelectRef.set_isVisible(false);
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		if (ToolsModifierControl.toolController != null)
		{
			ToolsModifierControl.toolController.eventEditPrefabChanged += new ToolController.EditPrefabChanged(this.RefreshTarget);
		}
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		if (ToolsModifierControl.toolController != null)
		{
			ToolsModifierControl.toolController.eventEditPrefabChanged -= new ToolController.EditPrefabChanged(this.RefreshTarget);
		}
	}

	protected override void OnLocaleChanged()
	{
		base.OnLocaleChanged();
		this.RefreshTarget(ToolsModifierControl.toolController.m_editPrefabInfo);
	}

	private void RefreshTarget(PrefabInfo info)
	{
		base.ResetPanel();
		this.m_SubBuildingButton.Hide();
		BuildingInfo buildingInfo = info as BuildingInfo;
		if (buildingInfo != null)
		{
			if (buildingInfo.GetAI() is IntersectionAI)
			{
				this.RefreshForIntersection();
			}
			else
			{
				this.RefreshForPropping();
			}
		}
		TreeInfo treeInfo = info as TreeInfo;
		if (treeInfo != null)
		{
			this.RefreshForTree();
		}
		NetInfo netInfo = info as NetInfo;
		if (netInfo != null)
		{
			this.RefreshForRoad();
		}
	}

	private void RefreshForRoad()
	{
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(-1);
		base.RefreshPanel();
		base.SpawnSubEntry(uITabstrip, "EditRoadPlacement", "MAIN_TOOL", null, "ToolbarIcon", true);
		base.SpawnSeparator(uITabstrip);
		base.SpawnSubEntry(uITabstrip, "Roads", "MAIN_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(uITabstrip, "AssetEditorSettings", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	private void RefreshForTree()
	{
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(-1);
		base.RefreshPanel();
		base.SpawnSubEntry(uITabstrip, "AssetEditorSettings", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	private void RefreshForIntersection()
	{
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(-1);
		base.RefreshPanel();
		base.SpawnSubEntry(uITabstrip, "Roads", "MAIN_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(uITabstrip, "Beautification", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSeparator(uITabstrip);
		base.SpawnSubEntry(uITabstrip, "AssetEditorSettings", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	private void RefreshForPropping()
	{
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(-1);
		base.RefreshPanel();
		base.SpawnSubEntry(uITabstrip, "Roads", "MAIN_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(uITabstrip, "Beautification", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(uITabstrip, "Surface", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSeparator(uITabstrip);
		this.AddMainToolbarProps(uITabstrip);
		base.SpawnSeparator(uITabstrip);
		base.SpawnSubEntry(uITabstrip, "AssetEditorSettings", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		this.m_SubBuildingButton.Show();
	}

	private void AddMainToolbarProps(UITabstrip strip)
	{
		base.SpawnSubEntry(strip, "PropsBillboards", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsSpecialBillboards", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsIndustrial", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsParks", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsCommon", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsResidential", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
		base.SpawnSubEntry(strip, "PropsMarkers", "DECORATIONEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	public string decorationWidth
	{
		get
		{
			if (ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo != null)
			{
				return ToolsModifierControl.toolController.m_editPrefabInfo.GetWidth().ToString();
			}
			return "N/A";
		}
		set
		{
			if (ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo != null)
			{
				int num = 1;
				int num2;
				if (int.TryParse(value, out num2))
				{
					num = num2;
				}
				int num3;
				int num4;
				ToolsModifierControl.toolController.m_editPrefabInfo.GetWidthRange(out num3, out num4);
				ToolsModifierControl.toolController.m_editPrefabInfo.SetWidth(Mathf.Clamp(num, num3, num4));
			}
		}
	}

	public string decorationLength
	{
		get
		{
			if (ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo != null)
			{
				return ToolsModifierControl.toolController.m_editPrefabInfo.GetLength().ToString();
			}
			return "N/A";
		}
		set
		{
			if (ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.m_editPrefabInfo != null)
			{
				int num = 1;
				int num2;
				if (int.TryParse(value, out num2))
				{
					num = num2;
				}
				int num3;
				int num4;
				ToolsModifierControl.toolController.m_editPrefabInfo.GetLengthRange(out num3, out num4);
				ToolsModifierControl.toolController.m_editPrefabInfo.SetLength(Mathf.Clamp(num, num3, num4));
			}
		}
	}

	private void OnSubBuildingClick(UIComponent component, UIMouseEventParameter eventParam)
	{
		if (this.m_SelectRef.get_isVisible() || ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<BuildingTool>())
		{
			this.m_SelectRef.set_isVisible(false);
			ToolsModifierControl.SetTool<DefaultTool>();
			return;
		}
		AssetImporterAssetTemplate component2 = this.m_SelectRef.GetComponent<AssetImporterAssetTemplate>();
		component2.ReferenceCallback = new AssetImporterAssetTemplate.ReferenceCallbackDelegate(this.OnConfirmReference);
		component2.Reset();
		component2.RefreshWithFilter(AssetImporterAssetTemplate.Filter.Buildings);
		this.m_SelectRef.set_isVisible(true);
	}

	private void OnConfirmReference(PrefabInfo template)
	{
		this.m_SelectRef.set_isVisible(false);
		BuildingInfo buildingInfo = template as BuildingInfo;
		if (buildingInfo == null)
		{
			return;
		}
		BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
		if (buildingTool != null)
		{
			buildingTool.m_prefab = buildingInfo;
		}
	}

	private UIButton m_SubBuildingButton;

	private UIPanel m_SelectRef;
}
