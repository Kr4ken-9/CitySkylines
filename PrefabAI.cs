﻿using System;
using UnityEngine;

public class PrefabAI : MonoBehaviour
{
	public virtual string GetLocalizedTooltip()
	{
		return null;
	}

	public virtual bool WorksAsBuilding()
	{
		return false;
	}

	public virtual bool WorksAsNet()
	{
		return false;
	}
}
