﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class EventManager : SimulationManagerBase<EventManager, EventProperties>, ISimulationManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_events = new FastList<EventData>();
		this.m_events.Add(default(EventData));
		this.m_randomEventColors = new FastList<Color32>();
		this.m_globalEventDataDirty = true;
		this.m_eventLocations = new Dictionary<ushort, HashSet<ushort>>();
	}

	private void OnDestroy()
	{
	}

	public bool CreateEvent(out ushort eventIndex, ushort building, EventInfo info)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		EventData eventData = default(EventData);
		eventData.m_flags = EventData.Flags.Created;
		eventData.m_startFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		eventData.m_ticketPrice = (ushort)info.m_eventAI.m_ticketPrice;
		eventData.m_securityBudget = (ushort)info.m_eventAI.m_securityBudget;
		if (building != 0)
		{
			eventData.m_building = building;
			eventData.m_nextBuildingEvent = instance.m_buildings.m_buffer[(int)building].m_eventIndex;
		}
		eventData.Info = info;
		for (int i = 1; i < this.m_events.m_size; i++)
		{
			if (this.m_events.m_buffer[i].m_flags == EventData.Flags.None)
			{
				eventIndex = (ushort)i;
				this.m_events.m_buffer[i] = eventData;
				this.m_eventCount++;
				info.m_eventAI.CreateEvent(eventIndex, ref this.m_events.m_buffer[(int)eventIndex]);
				instance.m_buildings.m_buffer[(int)building].m_eventIndex = eventIndex;
				return true;
			}
		}
		if (this.m_events.m_size < 256)
		{
			eventIndex = (ushort)this.m_events.m_size;
			this.m_events.Add(eventData);
			this.m_eventCount++;
			info.m_eventAI.CreateEvent(eventIndex, ref this.m_events.m_buffer[(int)eventIndex]);
			instance.m_buildings.m_buffer[(int)building].m_eventIndex = eventIndex;
			return true;
		}
		eventIndex = 0;
		return false;
	}

	public void ReleaseEvent(ushort eventIndex)
	{
		if (eventIndex != 0 && this.m_events.m_buffer[(int)eventIndex].m_flags != EventData.Flags.None)
		{
			EventInfo info = this.m_events.m_buffer[(int)eventIndex].Info;
			InstanceID id = default(InstanceID);
			id.Event = eventIndex;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			EventData[] expr_64_cp_0 = this.m_events.m_buffer;
			expr_64_cp_0[(int)eventIndex].m_flags = (expr_64_cp_0[(int)eventIndex].m_flags | EventData.Flags.Deleted);
			if (info != null)
			{
				info.m_eventAI.ReleaseEvent(eventIndex, ref this.m_events.m_buffer[(int)eventIndex]);
			}
			HashSet<ushort> hashSet;
			if (this.m_eventLocations.TryGetValue(eventIndex, out hashSet))
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				foreach (ushort current in hashSet)
				{
					instance.m_buildings.m_buffer[(int)current].m_eventIndex = 0;
				}
				this.m_eventLocations.Remove(eventIndex);
			}
			ushort building = this.m_events.m_buffer[(int)eventIndex].m_building;
			if (building != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				int num = 0;
				ushort num2 = 0;
				ushort num3 = instance2.m_buildings.m_buffer[(int)building].m_eventIndex;
				while (num3 != 0)
				{
					ushort nextBuildingEvent = this.m_events.m_buffer[(int)num3].m_nextBuildingEvent;
					if (num3 == eventIndex)
					{
						if (num2 != 0)
						{
							this.m_events.m_buffer[(int)num2].m_nextBuildingEvent = nextBuildingEvent;
						}
						else
						{
							instance2.m_buildings.m_buffer[(int)building].m_eventIndex = nextBuildingEvent;
						}
						break;
					}
					num2 = num3;
					num3 = nextBuildingEvent;
					if (++num > 256)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			this.m_events.m_buffer[(int)eventIndex] = default(EventData);
			this.m_eventCount--;
			while (this.m_events.m_size > 1)
			{
				if (this.m_events.m_buffer[this.m_events.m_size - 1].m_flags != EventData.Flags.None)
				{
					break;
				}
				this.m_events.m_size--;
			}
		}
	}

	public void ClearAll()
	{
		this.m_events.Clear();
		this.m_events.Add(default(EventData));
		this.m_eventCount = 0;
		this.m_bandPopularityBonus = 0;
		this.m_finalBandPopularityBonus = 0;
		this.m_eventLocations.Clear();
		this.m_adCampaignModifierTemp = 0;
		this.m_concertTicketSaleModifierTemp = 0;
		this.m_concertSuccessModifierTemp = 0;
		this.m_adCampaignModifierFinal = 0;
		this.m_concertTicketSaleModifierFinal = 0;
		this.m_concertSuccessModifierFinal = 0;
	}

	public void AddEventLocation(ushort eventIndex, ushort building)
	{
		HashSet<ushort> hashSet;
		if (this.m_eventLocations.TryGetValue(eventIndex, out hashSet))
		{
			hashSet.Add(building);
		}
		else
		{
			hashSet = new HashSet<ushort>();
			hashSet.Add(building);
			this.m_eventLocations.Add(eventIndex, hashSet);
		}
	}

	public void RemoveEventLocation(ushort eventIndex, ushort building)
	{
		HashSet<ushort> hashSet;
		if (this.m_eventLocations.TryGetValue(eventIndex, out hashSet))
		{
			hashSet.Remove(building);
			if (hashSet.Count == 0)
			{
				this.m_eventLocations.Remove(eventIndex);
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num2 = num;
			int num3 = num + 1 - 1;
			if ((num & 3) == 0 && this.m_bandPopularityBonus > 0)
			{
				this.m_bandPopularityBonus--;
			}
			if (num != 77)
			{
				if (num != 131)
				{
					if (num != 209)
					{
						if (num == 243)
						{
							this.m_finalBandPopularityBonus = this.m_bandPopularityBonus;
						}
					}
					else
					{
						this.m_concertSuccessModifierFinal = this.m_concertSuccessModifierTemp;
						this.m_concertSuccessModifierTemp = 0;
					}
				}
				else
				{
					this.m_concertTicketSaleModifierFinal = this.m_concertTicketSaleModifierTemp;
					this.m_concertTicketSaleModifierTemp = 0;
				}
			}
			else
			{
				this.m_adCampaignModifierFinal = this.m_adCampaignModifierTemp;
				this.m_adCampaignModifierTemp = 0;
			}
			for (int i = num2; i <= num3; i++)
			{
				if (i < this.m_events.m_size)
				{
					EventData.Flags flags = this.m_events.m_buffer[i].m_flags;
					if ((flags & EventData.Flags.Created) != EventData.Flags.None)
					{
						EventInfo info = this.m_events.m_buffer[i].Info;
						info.m_eventAI.SimulationStep((ushort)i, ref this.m_events.m_buffer[i]);
					}
				}
			}
		}
		if (subStep <= 1)
		{
			int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num5 = num4 * PrefabCollection<EventInfo>.PrefabCount() >> 10;
			int num6 = ((num4 + 1) * PrefabCollection<EventInfo>.PrefabCount() >> 10) - 1;
			for (int j = num5; j <= num6; j++)
			{
				EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)j);
				if (prefab != null)
				{
					prefab.CheckUnlocking();
				}
			}
		}
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<EventInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				prefab.CheckUnlocking();
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<EventInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	public ushort FindEvent(EventManager.EventType types, EventManager.EventGroup groups)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		for (int i = 1; i < this.m_events.m_size; i++)
		{
			EventData.Flags flags = this.m_events.m_buffer[i].m_flags;
			if ((flags & EventData.Flags.Created) != EventData.Flags.None)
			{
				EventInfo info = this.m_events.m_buffer[i].Info;
				if (info != null && (info.m_type & types) != (EventManager.EventType)0 && (info.m_group & groups) != (EventManager.EventGroup)0)
				{
					ushort building = this.m_events.m_buffer[i].m_building;
					if (building != 0 && (int)instance.m_buildings.m_buffer[(int)building].m_eventIndex == i)
					{
						return (ushort)i;
					}
				}
			}
		}
		return 0;
	}

	public EventInfo FindEventInfo(EventManager.EventType types, EventManager.EventGroup groups, ref Randomizer r)
	{
		return this.FindEventInfo(types, groups, ref r, null);
	}

	public EventInfo FindEventInfo(EventManager.EventType types, EventManager.EventGroup groups, ref Randomizer r, EventInfo avoidInfo)
	{
		int num = PrefabCollection<EventInfo>.PrefabCount();
		uint num2 = 0u;
		uint num3 = 0u;
		for (int i = 0; i < num; i++)
		{
			EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)i);
			if (prefab != null && (prefab.m_type & types) != (EventManager.EventType)0 && (prefab.m_group & groups) != (EventManager.EventGroup)0)
			{
				if (prefab != avoidInfo)
				{
					num2 += 1u;
				}
				num3 += 1u;
			}
		}
		if (num3 == 0u)
		{
			return null;
		}
		if (num2 == 0u)
		{
			num2 = num3;
			avoidInfo = null;
		}
		num2 = r.UInt32(num2);
		for (int j = 0; j < num; j++)
		{
			EventInfo prefab2 = PrefabCollection<EventInfo>.GetPrefab((uint)j);
			if (prefab2 != null && (prefab2.m_type & types) != (EventManager.EventType)0 && (prefab2.m_group & groups) != (EventManager.EventGroup)0 && prefab2 != avoidInfo && num2-- == 0u)
			{
				return prefab2;
			}
		}
		return null;
	}

	public int GetFutureEvents(ushort buildingID, EventManager.FutureEvent[] targetBuffer, int count)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		if (buildingID == 0)
		{
			return 0;
		}
		BuildingInfo info = instance2.m_buildings.m_buffer[(int)buildingID].Info;
		if (info == null)
		{
			return 0;
		}
		EventInfo prevEvent = null;
		uint prevExpireFrame = 0u;
		ushort eventIndex = instance2.m_buildings.m_buffer[(int)buildingID].m_eventIndex;
		if (eventIndex != 0)
		{
			prevEvent = this.m_events.m_buffer[(int)eventIndex].Info;
			prevExpireFrame = this.m_events.m_buffer[(int)eventIndex].m_expireFrame;
		}
		for (int i = 0; i < count; i++)
		{
			EventInfo eventInfo;
			uint num;
			if (!info.m_buildingAI.FindNextEventInfo(buildingID, prevEvent, prevExpireFrame, out eventInfo, out num))
			{
				return i;
			}
			targetBuffer[i].m_info = eventInfo;
			targetBuffer[i].m_startTime = Singleton<SimulationManager>.get_instance().FrameToTime(num);
			prevEvent = eventInfo;
			prevExpireFrame = eventInfo.m_eventAI.CalculateExpireFrame(num);
		}
		return count;
	}

	public Color32 GetEventCitizenColor(ushort eventIndex, uint citizen)
	{
		if (eventIndex != 0)
		{
			EventInfo info = this.m_events.m_buffer[(int)eventIndex].Info;
			return info.m_eventAI.GetCitizenColor(eventIndex, ref this.m_events.m_buffer[(int)eventIndex], citizen);
		}
		this.RefreshGlobalEventData();
		if (this.m_randomEventColors.m_size != 0)
		{
			Randomizer randomizer;
			randomizer..ctor(citizen);
			return this.m_randomEventColors.m_buffer[randomizer.Int32((uint)this.m_randomEventColors.m_size)];
		}
		return new Color32(0, 0, 0, 0);
	}

	public DistrictPolicies.Event GetEventPolicyMask()
	{
		this.RefreshGlobalEventData();
		return this.m_eventPolicyMask;
	}

	public EventManager.EventType GetEventTypeMask()
	{
		this.RefreshGlobalEventData();
		return this.m_eventTypeMask;
	}

	private void RefreshGlobalEventData()
	{
		if (this.m_globalEventDataDirty)
		{
			PrefabCollection<EventInfo>.BindPrefabs();
			this.m_randomEventColors.Clear();
			this.m_eventPolicyMask = DistrictPolicies.Event.None;
			this.m_eventTypeMask = (EventManager.EventType)0;
			for (int i = 1; i < this.m_events.m_size; i++)
			{
				EventData.Flags flags = this.m_events.m_buffer[i].m_flags;
				if ((flags & EventData.Flags.Created) != EventData.Flags.None)
				{
					EventInfo info = this.m_events.m_buffer[i].Info;
					if (info != null)
					{
						Color32 citizenColor = info.m_eventAI.GetCitizenColor((ushort)i, ref this.m_events.m_buffer[i], 0u);
						if (citizenColor.a == 255)
						{
							this.m_randomEventColors.Add(citizenColor);
						}
						this.m_eventPolicyMask |= info.m_eventAI.GetEventPolicyMask((ushort)i, ref this.m_events.m_buffer[i]);
						this.m_eventTypeMask |= info.m_type;
					}
				}
			}
			this.m_globalEventDataDirty = false;
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("EventManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < this.m_events.m_size; i++)
		{
			if (this.m_events.m_buffer[i].m_flags != EventData.Flags.None && this.m_events.m_buffer[i].Info == null)
			{
				this.ReleaseEvent((ushort)i);
			}
		}
		int num = PrefabCollection<EventInfo>.PrefabCount();
		this.m_infoCount = num;
		for (int j = 0; j < num; j++)
		{
			EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)j);
			if (prefab != null && prefab.m_UnlockMilestone != null)
			{
				switch (mode)
				{
				case SimulationManager.UpdateMode.NewMap:
				case SimulationManager.UpdateMode.LoadMap:
				case SimulationManager.UpdateMode.NewAsset:
				case SimulationManager.UpdateMode.LoadAsset:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.NewGameFromMap:
				case SimulationManager.UpdateMode.NewScenarioFromMap:
				case SimulationManager.UpdateMode.UpdateScenarioFromMap:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.LoadGame:
				case SimulationManager.UpdateMode.NewScenarioFromGame:
				case SimulationManager.UpdateMode.LoadScenario:
				case SimulationManager.UpdateMode.NewGameFromScenario:
				case SimulationManager.UpdateMode.UpdateScenarioFromGame:
					prefab.m_UnlockMilestone.Reset(true);
					break;
				}
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_matchPreparing == null)
		{
			this.m_matchPreparing = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_bandPreparing == null)
		{
			this.m_bandPreparing = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_bandPopularity == null)
		{
			this.m_bandPopularity = new BuildingInstanceGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new EventManager.Data());
	}

	public int CountEvents(EventManager.EventType types, EventManager.EventGroup groups, bool onePerBuilding)
	{
		int num = 0;
		int num2 = this.m_events.m_size;
		EventData[] buffer = this.m_events.m_buffer;
		if (buffer != null)
		{
			num2 = Mathf.Min(num2, buffer.Length);
		}
		else
		{
			num2 = 0;
		}
		for (int i = 1; i < num2; i++)
		{
			EventData.Flags flags = buffer[i].m_flags;
			if ((flags & (EventData.Flags.Created | EventData.Flags.Deleted)) == EventData.Flags.Created)
			{
				if (onePerBuilding)
				{
					ushort building = buffer[i].m_building;
					if (building == 0 || Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_eventIndex != (ushort)i)
					{
						goto IL_C7;
					}
				}
				EventInfo info = buffer[i].Info;
				if (info != null && (info.m_type & types) != (EventManager.EventType)0 && (info.m_group & groups) != (EventManager.EventGroup)0)
				{
					num++;
				}
			}
			IL_C7:;
		}
		return num;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const int MAX_EVENT_COUNT = 256;

	public int m_eventCount;

	public int m_infoCount;

	public const EventManager.EventGroup AllEventGroups = (EventManager.EventGroup)(-1);

	[NonSerialized]
	public FastList<EventData> m_events;

	[NonSerialized]
	public bool m_globalEventDataDirty;

	[NonSerialized]
	public int m_bandPopularityBonus;

	[NonSerialized]
	public int m_finalBandPopularityBonus;

	[NonSerialized]
	private FastList<Color32> m_randomEventColors;

	[NonSerialized]
	private DistrictPolicies.Event m_eventPolicyMask;

	[NonSerialized]
	private EventManager.EventType m_eventTypeMask;

	[NonSerialized]
	public Dictionary<ushort, HashSet<ushort>> m_eventLocations;

	[NonSerialized]
	public int m_adCampaignModifierTemp;

	[NonSerialized]
	public int m_concertTicketSaleModifierTemp;

	[NonSerialized]
	public int m_concertSuccessModifierTemp;

	[NonSerialized]
	public int m_adCampaignModifierFinal;

	[NonSerialized]
	public int m_concertTicketSaleModifierFinal;

	[NonSerialized]
	public int m_concertSuccessModifierFinal;

	[NonSerialized]
	public BuildingInstanceGuide m_matchPreparing;

	[NonSerialized]
	public BuildingInstanceGuide m_bandPreparing;

	[NonSerialized]
	public BuildingInstanceGuide m_bandPopularity;

	public enum EventType
	{
		Football = 1,
		Concert,
		SecondaryLocation = -2147483648
	}

	public enum EventGroup
	{
		Group01 = 1,
		Group02,
		Group03 = 4,
		Group04 = 8,
		Group05 = 16,
		Group06 = 32,
		Group07 = 64,
		Group08 = 128,
		Group09 = 256,
		Group10 = 512,
		Group11 = 1024,
		Group12 = 2048,
		Group13 = 4096,
		Group14 = 8192,
		Group15 = 16384,
		Group16 = 32768,
		Group17 = 65536,
		Group18 = 131072,
		Group19 = 262144,
		Group20 = 524288,
		Group21 = 1048576,
		Group22 = 2097152,
		Group23 = 4194304,
		Group24 = 8388608,
		Group25 = 16777216,
		Group26 = 33554432,
		Group27 = 67108864,
		Group28 = 134217728,
		Group29 = 268435456,
		Group30 = 536870912,
		Group31 = 1073741824,
		Group32 = -2147483648
	}

	public struct FutureEvent
	{
		public EventInfo m_info;

		public DateTime m_startTime;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "EventManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			EventManager instance2 = Singleton<EventManager>.get_instance();
			EventData[] buffer = instance2.m_events.m_buffer;
			int size = instance2.m_events.m_size;
			s.WriteUInt16((uint)(size - 1));
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < size; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			int num = PrefabCollection<EventInfo>.PrefabCount();
			uint num2 = 0u;
			uint num3 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int j = 0; j < num; j++)
				{
					EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)j);
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num2 += 1u;
					}
				}
				for (int k = 0; k < num; k++)
				{
					EventInfo prefab2 = PrefabCollection<EventInfo>.GetPrefab((uint)k);
					if (prefab2 != null && prefab2.HasModifiedTypeData() && prefab2.m_prefabDataIndex != -1)
					{
						num3 += 1u;
					}
				}
			}
			s.WriteUInt16(num2);
			s.WriteUInt16(num3);
			try
			{
				PrefabCollection<EventInfo>.BeginSerialize(s);
				for (int l = 1; l < size; l++)
				{
					if (buffer[l].m_flags != EventData.Flags.None)
					{
						PrefabCollection<EventInfo>.Serialize((uint)buffer[l].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int m = 0; m < num; m++)
					{
						EventInfo prefab3 = PrefabCollection<EventInfo>.GetPrefab((uint)m);
						if (prefab3 != null && prefab3.m_UnlockMilestone != null && prefab3.m_prefabDataIndex != -1)
						{
							PrefabCollection<EventInfo>.Serialize((uint)prefab3.m_prefabDataIndex);
						}
					}
					for (int n = 0; n < num; n++)
					{
						EventInfo prefab4 = PrefabCollection<EventInfo>.GetPrefab((uint)n);
						if (prefab4 != null && prefab4.HasModifiedTypeData() && prefab4.m_prefabDataIndex != -1)
						{
							PrefabCollection<EventInfo>.Serialize((uint)prefab4.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<EventInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int num4 = 0; num4 < num; num4++)
				{
					EventInfo prefab5 = PrefabCollection<EventInfo>.GetPrefab((uint)num4);
					if (prefab5 != null && prefab5.m_UnlockMilestone != null && prefab5.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab5.m_UnlockMilestone.GetData());
					}
				}
				for (int num5 = 0; num5 < num; num5++)
				{
					EventInfo prefab6 = PrefabCollection<EventInfo>.GetPrefab((uint)num5);
					if (prefab6 != null && prefab6.HasModifiedTypeData() && prefab6.m_prefabDataIndex != -1)
					{
						s.WriteInt8(prefab6.m_popularityOffset);
						s.WriteInt24(prefab6.m_ticketPriceOffset);
					}
				}
			}
			for (int num6 = 1; num6 < size; num6++)
			{
				if (buffer[num6].m_flags != EventData.Flags.None)
				{
					s.WriteUInt16((uint)buffer[num6].m_building);
					s.WriteUInt32(buffer[num6].m_startFrame);
					s.WriteUInt32(buffer[num6].m_expireFrame);
					s.WriteUInt16((uint)buffer[num6].m_ticketPrice);
					s.WriteUInt8((uint)buffer[num6].m_color.r);
					s.WriteUInt8((uint)buffer[num6].m_color.g);
					s.WriteUInt8((uint)buffer[num6].m_color.b);
					s.WriteULong64(buffer[num6].m_ticketMoney);
					s.WriteULong64(buffer[num6].m_totalTicket);
					s.WriteULong64(buffer[num6].m_rewardMoney);
					s.WriteULong64(buffer[num6].m_totalReward);
					s.WriteUInt16((uint)buffer[num6].m_successCount);
					s.WriteUInt16((uint)buffer[num6].m_failureCount);
					s.WriteInt16((int)buffer[num6].m_popularityDelta);
					s.WriteUInt16((uint)buffer[num6].m_securityBudget);
					if ((buffer[num6].m_flags & EventData.Flags.CustomSeed) != EventData.Flags.None)
					{
						s.WriteULong64(buffer[num6].m_customSeed);
					}
				}
			}
			s.WriteObject<BuildingInstanceGuide>(instance2.m_matchPreparing);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_bandPreparing);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_bandPopularity);
			s.WriteInt16(instance2.m_bandPopularityBonus);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "EventManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "EventManager");
			EventManager instance = Singleton<EventManager>.get_instance();
			int num = (int)(s.ReadUInt16() + 1u);
			if (s.get_version() < 254u && num == 65536)
			{
				num = 1;
			}
			instance.m_events.Clear();
			instance.m_events.EnsureCapacity(num);
			instance.m_events.Add(default(EventData));
			instance.m_eventCount = 0;
			EventData[] buffer = instance.m_events.m_buffer;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
			for (int i = 1; i < num; i++)
			{
				EventData item = default(EventData);
				item.m_flags = (EventData.Flags)uInt.Read();
				instance.m_events.Add(item);
				if (item.m_flags != EventData.Flags.None)
				{
					instance.m_eventCount++;
				}
			}
			uInt.EndRead();
			uint num2 = s.ReadUInt16();
			uint num3 = 0u;
			if (s.get_version() >= 109002u || (s.get_version() >= 108002u && s.get_version() < 109000u))
			{
				num3 = s.ReadUInt16();
			}
			this.m_milestoneInfoIndex = new uint[num2];
			this.m_UnlockMilestones = new MilestoneInfo.Data[num2];
			this.m_typeDataInfoIndex = new uint[num3];
			this.m_typeData = new EventManager.Data.TypeData[num3];
			PrefabCollection<EventInfo>.BeginDeserialize(s);
			for (int j = 1; j < num; j++)
			{
				if (buffer[j].m_flags != EventData.Flags.None)
				{
					buffer[j].m_infoIndex = (ushort)PrefabCollection<EventInfo>.Deserialize(true);
				}
			}
			for (uint num4 = 0u; num4 < num2; num4 += 1u)
			{
				this.m_milestoneInfoIndex[(int)((UIntPtr)num4)] = PrefabCollection<EventInfo>.Deserialize(false);
			}
			for (uint num5 = 0u; num5 < num3; num5 += 1u)
			{
				this.m_typeDataInfoIndex[(int)((UIntPtr)num5)] = PrefabCollection<EventInfo>.Deserialize(false);
			}
			PrefabCollection<EventInfo>.EndDeserialize(s);
			for (uint num6 = 0u; num6 < num2; num6 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num6)] = s.ReadObject<MilestoneInfo.Data>();
			}
			for (uint num7 = 0u; num7 < num3; num7 += 1u)
			{
				this.m_typeData[(int)((UIntPtr)num7)].m_popularityOffset = s.ReadInt8();
				if (s.get_version() >= 109004u || (s.get_version() >= 108004u && s.get_version() < 109000u))
				{
					this.m_typeData[(int)((UIntPtr)num7)].m_ticketPriceOffset = s.ReadInt24();
				}
			}
			if (s.get_version() >= 210u)
			{
				for (int k = 1; k < num; k++)
				{
					if (buffer[k].m_flags != EventData.Flags.None)
					{
						buffer[k].m_building = (ushort)s.ReadUInt16();
						buffer[k].m_startFrame = s.ReadUInt32();
						if (s.get_version() >= 246u)
						{
							buffer[k].m_expireFrame = s.ReadUInt32();
						}
						else
						{
							buffer[k].m_expireFrame = 0u;
						}
						if (s.get_version() >= 249u)
						{
							buffer[k].m_ticketPrice = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer[k].m_ticketPrice = 3000;
						}
						if (s.get_version() >= 245u && s.get_version() < 252u)
						{
							buffer[k].m_nextBuildingEvent = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer[k].m_nextBuildingEvent = 0;
						}
						if (s.get_version() >= 245u)
						{
							buffer[k].m_color.r = (byte)s.ReadUInt8();
							buffer[k].m_color.g = (byte)s.ReadUInt8();
							buffer[k].m_color.b = (byte)s.ReadUInt8();
							buffer[k].m_color.a = 255;
						}
						else
						{
							s.ReadUInt32();
							s.ReadUInt32();
							s.ReadUInt32();
							s.ReadUInt32();
							buffer[k].m_color = default(Color32);
						}
						if (s.get_version() >= 255u)
						{
							buffer[k].m_ticketMoney = s.ReadULong64();
							buffer[k].m_totalTicket = s.ReadULong64();
						}
						else
						{
							buffer[k].m_ticketMoney = 0uL;
							buffer[k].m_totalTicket = 0uL;
						}
						if (s.get_version() >= 250u)
						{
							buffer[k].m_rewardMoney = s.ReadULong64();
							buffer[k].m_totalReward = s.ReadULong64();
							buffer[k].m_successCount = (ushort)s.ReadUInt16();
							buffer[k].m_failureCount = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer[k].m_rewardMoney = 0uL;
							buffer[k].m_totalReward = 0uL;
							buffer[k].m_successCount = 0;
							buffer[k].m_failureCount = 0;
						}
						if (s.get_version() >= 109005u || (s.get_version() >= 108005u && s.get_version() < 109000u))
						{
							buffer[k].m_popularityDelta = (short)s.ReadInt16();
						}
						else
						{
							buffer[k].m_popularityDelta = 0;
						}
						if (s.get_version() >= 109008u || (s.get_version() >= 108008u && s.get_version() < 109000u))
						{
							buffer[k].m_securityBudget = (ushort)s.ReadInt16();
						}
						else
						{
							buffer[k].m_securityBudget = 0;
						}
						if ((buffer[k].m_flags & EventData.Flags.CustomSeed) != EventData.Flags.None)
						{
							buffer[k].m_customSeed = s.ReadULong64();
						}
						else
						{
							buffer[k].m_customSeed = 0uL;
						}
					}
				}
			}
			if (s.get_version() >= 253u)
			{
				instance.m_matchPreparing = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_matchPreparing = null;
			}
			if (s.get_version() >= 109011u || (s.get_version() >= 108011u && s.get_version() < 109000u))
			{
				instance.m_bandPreparing = s.ReadObject<BuildingInstanceGuide>();
				instance.m_bandPopularity = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_bandPreparing = null;
				instance.m_bandPopularity = null;
			}
			if (s.get_version() >= 109003u || (s.get_version() >= 108003u && s.get_version() < 109000u))
			{
				instance.m_bandPopularityBonus = s.ReadInt16();
			}
			else
			{
				instance.m_bandPopularityBonus = 0;
			}
			instance.m_globalEventDataDirty = true;
			instance.m_eventLocations.Clear();
			instance.m_adCampaignModifierTemp = 0;
			instance.m_concertTicketSaleModifierTemp = 0;
			instance.m_concertSuccessModifierTemp = 0;
			instance.m_adCampaignModifierFinal = 0;
			instance.m_concertTicketSaleModifierFinal = 0;
			instance.m_concertSuccessModifierFinal = 0;
			instance.m_finalBandPopularityBonus = instance.m_bandPopularityBonus;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "EventManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "EventManager");
			EventManager instance = Singleton<EventManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<EventInfo>.BindPrefabs();
			EventData[] buffer = instance.m_events.m_buffer;
			int size = instance.m_events.m_size;
			int num = PrefabCollection<EventInfo>.PrefabCount();
			for (int i = 0; i < num; i++)
			{
				EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)i);
				if (prefab != null)
				{
					prefab.m_popularityOffset = 0;
					prefab.m_ticketPriceOffset = 0;
				}
			}
			if (this.m_milestoneInfoIndex != null)
			{
				int num2 = this.m_milestoneInfoIndex.Length;
				for (int j = 0; j < num2; j++)
				{
					EventInfo prefab2 = PrefabCollection<EventInfo>.GetPrefab(this.m_milestoneInfoIndex[j]);
					if (prefab2 != null)
					{
						MilestoneInfo unlockMilestone = prefab2.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[j]);
						}
					}
				}
			}
			if (this.m_typeDataInfoIndex != null)
			{
				int num3 = this.m_typeDataInfoIndex.Length;
				for (int k = 0; k < num3; k++)
				{
					EventInfo prefab3 = PrefabCollection<EventInfo>.GetPrefab(this.m_typeDataInfoIndex[k]);
					if (prefab3 != null)
					{
						prefab3.m_popularityOffset = this.m_typeData[k].m_popularityOffset;
						prefab3.m_ticketPriceOffset = this.m_typeData[k].m_ticketPriceOffset;
					}
				}
			}
			for (int l = 1; l < size; l++)
			{
				if (buffer[l].m_flags != EventData.Flags.None)
				{
					EventInfo info = buffer[l].Info;
					if (info != null)
					{
						buffer[l].m_infoIndex = (ushort)info.m_prefabDataIndex;
						if (buffer[l].m_building != 0)
						{
							this.AddEventSorted((ushort)l, buffer[l].m_building);
						}
					}
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "EventManager");
		}

		private void AddEventSorted(ushort eventID, ushort buildingID)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			uint startFrame = instance.m_events.m_buffer[(int)eventID].m_startFrame;
			ushort num = 0;
			if ((instance.m_events.m_buffer[(int)eventID].m_flags & EventData.Flags.Expired) != EventData.Flags.None)
			{
				ushort num2 = instance2.m_buildings.m_buffer[(int)buildingID].m_eventIndex;
				int num3 = 0;
				while (num2 != 0)
				{
					if ((instance.m_events.m_buffer[(int)num2].m_flags & EventData.Flags.Expired) != EventData.Flags.None && instance.m_events.m_buffer[(int)num2].m_startFrame < startFrame)
					{
						break;
					}
					num = num2;
					num2 = instance.m_events.m_buffer[(int)num2].m_nextBuildingEvent;
					if (++num3 > 256)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (num != 0)
			{
				instance.m_events.m_buffer[(int)eventID].m_nextBuildingEvent = instance.m_events.m_buffer[(int)num].m_nextBuildingEvent;
				instance.m_events.m_buffer[(int)num].m_nextBuildingEvent = eventID;
			}
			else
			{
				instance.m_events.m_buffer[(int)eventID].m_nextBuildingEvent = instance2.m_buildings.m_buffer[(int)buildingID].m_eventIndex;
				instance2.m_buildings.m_buffer[(int)buildingID].m_eventIndex = eventID;
			}
		}

		private uint[] m_milestoneInfoIndex;

		private MilestoneInfo.Data[] m_UnlockMilestones;

		private uint[] m_typeDataInfoIndex;

		private EventManager.Data.TypeData[] m_typeData;

		private struct TypeData
		{
			public int m_popularityOffset;

			public int m_ticketPriceOffset;
		}
	}
}
