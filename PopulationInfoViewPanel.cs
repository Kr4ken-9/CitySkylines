﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class PopulationInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Population = base.Find<UILabel>("Population");
		this.m_Workers = base.Find<UILabel>("Workers");
		this.m_Workplaces = base.Find<UILabel>("Workplaces");
		this.m_Unemployment = base.Find<UILabel>("Unemployment");
		this.m_AgeChart = base.Find<UIRadialChart>("AgeChart");
		UIRadialChart.SliceSettings arg_81_0 = this.m_AgeChart.GetSlice(0);
		Color32 color = this.m_ChildColor;
		this.m_AgeChart.GetSlice(0).set_outterColor(color);
		arg_81_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_AC_0 = this.m_AgeChart.GetSlice(1);
		color = this.m_TeenColor;
		this.m_AgeChart.GetSlice(1).set_outterColor(color);
		arg_AC_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_D7_0 = this.m_AgeChart.GetSlice(2);
		color = this.m_YoungColor;
		this.m_AgeChart.GetSlice(2).set_outterColor(color);
		arg_D7_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_102_0 = this.m_AgeChart.GetSlice(3);
		color = this.m_AdultColor;
		this.m_AgeChart.GetSlice(3).set_outterColor(color);
		arg_102_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_12D_0 = this.m_AgeChart.GetSlice(4);
		color = this.m_SeniorColor;
		this.m_AgeChart.GetSlice(4).set_outterColor(color);
		arg_12D_0.set_innerColor(color);
		base.Find<UISprite>("ChildLegend").set_color(this.m_ChildColor);
		base.Find<UISprite>("TeenLegend").set_color(this.m_TeenColor);
		base.Find<UISprite>("YoungLegend").set_color(this.m_YoungColor);
		base.Find<UISprite>("AdultLegend").set_color(this.m_AdultColor);
		base.Find<UISprite>("SeniorLegend").set_color(this.m_SeniorColor);
		this.m_ChildLegend = base.Find<UILabel>("ChildLabel");
		this.m_TeenLegend = base.Find<UILabel>("TeenLabel");
		this.m_YoungLegend = base.Find<UILabel>("YoungLabel");
		this.m_AdultLegend = base.Find<UILabel>("AdultLabel");
		this.m_SeniorLegend = base.Find<UILabel>("SeniorLabel");
		this.m_BirthLabel = base.Find<UILabel>("BirthLabel");
		this.m_DeathLabel = base.Find<UILabel>("DeathLabel");
	}

	public int population
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount;
			}
			return 0;
		}
	}

	public int workers
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWorkerCount();
			}
			return 0;
		}
	}

	public int workplaces
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWorkplaceCount();
			}
			return 0;
		}
	}

	public int unemployed
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetUnemployment();
			}
			return 0;
		}
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.FloorToInt(num * 100f), 0, 100);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				if (Singleton<InfoManager>.get_exists())
				{
					UISprite uISprite = base.Find<UISprite>("FamiliesColor");
					UISprite uISprite2 = base.Find<UISprite>("AdultsColor");
					UISprite uISprite3 = base.Find<UISprite>("SeniorsColor");
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[6].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[6].m_activeColorB);
					uISprite3.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[6].m_negativeColor);
				}
			}
			this.m_initialized = true;
		}
		this.m_Population.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_POPULATION"), this.population));
		this.m_Workers.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_WORKERS"), this.workers));
		this.m_Workplaces.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_WORKPLACES"), this.workplaces));
		this.m_Unemployment.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_UNEMPLOYMENT"), this.unemployed));
		if (Singleton<DistrictManager>.get_exists())
		{
			int value = PopulationInfoViewPanel.GetValue((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_childData.m_finalCount, this.population);
			int value2 = PopulationInfoViewPanel.GetValue((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_teenData.m_finalCount, this.population);
			int value3 = PopulationInfoViewPanel.GetValue((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_youngData.m_finalCount, this.population);
			int num = PopulationInfoViewPanel.GetValue((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_adultData.m_finalCount, this.population);
			int value4 = PopulationInfoViewPanel.GetValue((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_seniorData.m_finalCount, this.population);
			int num2 = value + value2 + value3 + num + value4;
			if (num2 != 0 && num2 != 100)
			{
				num = 100 - (value + value2 + value3 + value4);
			}
			this.m_ChildLegend.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_CHILD"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), value)));
			this.m_TeenLegend.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_TEEN"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), value2)));
			this.m_YoungLegend.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_YOUNG"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), value3)));
			this.m_AdultLegend.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_ADULT"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), num)));
			this.m_SeniorLegend.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_SENIOR"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), value4)));
			this.m_AgeChart.SetValues(new int[]
			{
				value,
				value2,
				value3,
				num,
				value4
			});
			this.m_BirthLabel.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_BIRTHS"), Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_birthData.m_finalCount));
			this.m_DeathLabel.set_text(StringUtils.SafeFormat(Locale.Get("INFO_POPULATION_DEATHS"), Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_deathData.m_finalCount));
		}
	}

	public Color32 m_ChildColor;

	public Color32 m_TeenColor;

	public Color32 m_YoungColor;

	public Color32 m_AdultColor;

	public Color32 m_SeniorColor;

	private UILabel m_BirthLabel;

	private UILabel m_DeathLabel;

	private UILabel m_ChildLegend;

	private UILabel m_TeenLegend;

	private UILabel m_YoungLegend;

	private UILabel m_AdultLegend;

	private UILabel m_SeniorLegend;

	private UILabel m_Population;

	private UILabel m_Workers;

	private UILabel m_Workplaces;

	private UILabel m_Unemployment;

	private UIRadialChart m_AgeChart;

	private bool m_initialized;
}
