﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public sealed class PublicTransportWorldInfoPanel : WorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		UIComponent uIComponent = base.Find("ActivityPanel");
		this.m_DayLine = uIComponent.Find<UICheckBox>("Day");
		this.m_DayLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.GetLineID();
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetDayOnly(lineID));
			}
		});
		this.m_NightLine = uIComponent.Find<UICheckBox>("Night");
		this.m_NightLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.GetLineID();
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetNightOnly(lineID));
			}
		});
		this.m_BothLine = uIComponent.Find<UICheckBox>("Both");
		this.m_BothLine.add_eventClicked(delegate(UIComponent comp, UIMouseEventParameter c)
		{
			ushort lineID = this.GetLineID();
			if (Singleton<SimulationManager>.get_exists() && lineID != 0)
			{
				this.m_LineOperation = Singleton<SimulationManager>.get_instance().AddAction(this.SetAllDay(lineID));
			}
		});
		this.m_NameField = base.Find<UITextField>("LineName");
		if (this.m_NameField != null)
		{
			this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		}
		this.m_VehicleType = base.Find<UISprite>("VehicleType");
		this.m_VehicleType.add_eventSpriteNameChanged(new PropertyChangedEventHandler<string>(this.IconChanged));
		this.m_ColorField = base.Find<UIColorField>("ColorField");
		this.m_ColorField.add_eventSelectedColorReleased(new PropertyChangedEventHandler<Color>(this.OnColorChanged));
		this.m_ColorFieldButton = this.m_ColorField.Find<UIButton>("Button");
		this.m_ColorField.add_eventColorPickerOpen(delegate(UIColorField field, UIColorPicker picker, ref bool overridden)
		{
			this.m_ColorFieldButton.set_isInteractive(false);
		});
		this.m_ColorField.add_eventColorPickerClose(delegate(UIColorField field, UIColorPicker picker, ref bool overridden)
		{
			this.m_ColorFieldButton.set_isInteractive(true);
		});
		this.m_Passengers = base.Find<UILabel>("Passengers");
		this.m_VehicleAmount = base.Find<UILabel>("VehicleAmount");
		this.m_Type = base.Find<UILabel>("Type");
		this.m_TripSaved = base.Find<UILabel>("TripSaved");
		this.m_AgeChart = base.Find<UIRadialChart>("AgeChart");
		UIRadialChart.SliceSettings arg_1D2_0 = this.m_AgeChart.GetSlice(0);
		Color32 color = this.m_ChildColor;
		this.m_AgeChart.GetSlice(0).set_outterColor(color);
		arg_1D2_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_1FD_0 = this.m_AgeChart.GetSlice(1);
		color = this.m_TeenColor;
		this.m_AgeChart.GetSlice(1).set_outterColor(color);
		arg_1FD_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_228_0 = this.m_AgeChart.GetSlice(2);
		color = this.m_YoungColor;
		this.m_AgeChart.GetSlice(2).set_outterColor(color);
		arg_228_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_253_0 = this.m_AgeChart.GetSlice(3);
		color = this.m_AdultColor;
		this.m_AgeChart.GetSlice(3).set_outterColor(color);
		arg_253_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_27E_0 = this.m_AgeChart.GetSlice(4);
		color = this.m_SeniorColor;
		this.m_AgeChart.GetSlice(4).set_outterColor(color);
		arg_27E_0.set_innerColor(color);
		this.m_ChildLegend = base.Find<UILabel>("ChildAmount");
		this.m_TeenLegend = base.Find<UILabel>("TeenAmount");
		this.m_YoungLegend = base.Find<UILabel>("YoungAmount");
		this.m_AdultLegend = base.Find<UILabel>("AdultAmount");
		this.m_SeniorLegend = base.Find<UILabel>("SeniorAmount");
		this.m_ChildLegend.set_color(this.m_ChildColor);
		this.m_TeenLegend.set_color(this.m_TeenColor);
		this.m_YoungLegend.set_color(this.m_YoungColor);
		this.m_AdultLegend.set_color(this.m_AdultColor);
		this.m_SeniorLegend.set_color(this.m_SeniorColor);
		this.m_vehicleCountModifier = base.Find<UISlider>("SliderModifyVehicleCount");
		this.m_vehicleCountModifier.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnVehicleCountModifierChanged));
		this.m_vehicleCountModifierLabel = base.Find<UILabel>("VehicleCountPercent");
		this.m_stopsContainer = base.Find<UIPanel>("StopsPanel");
		this.m_stopButtons = new UITemplateList<UIButton>(this.m_stopsContainer, "StopButton");
		this.m_vehicleButtons = new UITemplateList<UIButton>(this.m_stopsContainer, "VehicleButton");
		this.m_stopsLineSprite = base.Find<UISprite>("StopsLineSprite");
		this.m_lineEnd = base.Find<UISprite>("LineEnd");
		PublicTransportWorldInfoPanel.m_scrollPanel = base.Find<UIScrollablePanel>("ScrollablePanel");
		PublicTransportWorldInfoPanel.m_scrollPanel.add_eventGotFocus(new FocusEventHandler(this.OnGotFocus));
	}

	private void OnGotFocus(UIComponent component, UIFocusEventParameter eventParam)
	{
		PublicTransportWorldInfoPanel.m_cachedScrollPosition = PublicTransportWorldInfoPanel.m_scrollPanel.get_scrollPosition();
	}

	public static void ResetScrollPosition()
	{
		PublicTransportWorldInfoPanel.m_scrollPanel.set_scrollPosition(PublicTransportWorldInfoPanel.m_cachedScrollPosition);
	}

	private void OnVehicleCountModifierChanged(UIComponent component, float value)
	{
		ushort lineID = this.GetLineID();
		Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_budget = (ushort)value;
		this.m_vehicleCountModifierLabel.set_text(LocaleFormatter.FormatPercentage(Mathf.RoundToInt(value)));
	}

	private void OnEnable()
	{
		Singleton<TransportManager>.get_instance().eventLineColorChanged += new TransportManager.LineColorChangedHandler(this.OnLineColorChanged);
		Singleton<TransportManager>.get_instance().eventLineNameChanged += new TransportManager.LineNameChangedHandler(this.OnLineNameChanged);
	}

	private void OnDisable()
	{
		Singleton<TransportManager>.get_instance().eventLineColorChanged -= new TransportManager.LineColorChangedHandler(this.OnLineColorChanged);
		Singleton<TransportManager>.get_instance().eventLineNameChanged -= new TransportManager.LineNameChangedHandler(this.OnLineNameChanged);
	}

	private void OnLineColorChanged(ushort id)
	{
		if (id == this.GetLineID())
		{
			this.m_ColorField.set_selectedColor(Singleton<TransportManager>.get_instance().GetLineColor(id));
		}
	}

	private void OnLineNameChanged(ushort id)
	{
		if (id == this.GetLineID())
		{
			this.m_NameField.set_text(Singleton<TransportManager>.get_instance().GetLineName(id));
		}
	}

	private void IconChanged(UIComponent comp, string text)
	{
		UITextureAtlas.SpriteInfo spriteInfo = this.m_VehicleType.get_atlas().get_Item(text);
		if (spriteInfo != null)
		{
			this.m_VehicleType.set_size(spriteInfo.get_pixelSize());
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.RenameCoroutine(this.GetLineID(), text));
	}

	private void OnColorChanged(UIComponent comp, Color color)
	{
		base.StartCoroutine(this.ChangeColorCoroutine(this.GetLineID(), color));
	}

	[DebuggerHidden]
	private IEnumerator ChangeColorCoroutine(ushort id, Color color)
	{
		PublicTransportWorldInfoPanel.<ChangeColorCoroutine>c__Iterator0 <ChangeColorCoroutine>c__Iterator = new PublicTransportWorldInfoPanel.<ChangeColorCoroutine>c__Iterator0();
		<ChangeColorCoroutine>c__Iterator.id = id;
		<ChangeColorCoroutine>c__Iterator.color = color;
		<ChangeColorCoroutine>c__Iterator.$this = this;
		return <ChangeColorCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator RenameCoroutine(ushort id, string newName)
	{
		PublicTransportWorldInfoPanel.<RenameCoroutine>c__Iterator1 <RenameCoroutine>c__Iterator = new PublicTransportWorldInfoPanel.<RenameCoroutine>c__Iterator1();
		<RenameCoroutine>c__Iterator.id = id;
		<RenameCoroutine>c__Iterator.newName = newName;
		<RenameCoroutine>c__Iterator.$this = this;
		return <RenameCoroutine>c__Iterator;
	}

	protected override void OnSetTarget()
	{
		ushort lineID = this.GetLineID();
		if (lineID != 0)
		{
			this.m_NameField.set_text(Singleton<TransportManager>.get_instance().GetLineName(lineID));
			this.m_ColorField.set_selectedColor(Singleton<TransportManager>.get_instance().GetLineColor(lineID));
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].CountStops(lineID);
			float[] array = new float[num];
			float num2 = 3.40282347E+38f;
			float num3 = 0f;
			UIButton[] array2 = this.m_stopButtons.SetItemCount(num);
			ushort stops = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_stops;
			ushort num4 = stops;
			int num5 = 0;
			while (num4 != 0 && num5 < array2.Length)
			{
				array2[num5].set_objectUserData(num4);
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num4].GetSegment(i);
					if (segment != 0 && instance.m_segments.m_buffer[(int)segment].m_startNode == num4)
					{
						num4 = instance.m_segments.m_buffer[(int)segment].m_endNode;
						float num6 = instance.m_segments.m_buffer[(int)segment].m_averageLength;
						if (num6 == 0f)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Two transport line stops have zero distance");
							num6 = 100f;
						}
						array[num5] = num6;
						if (num6 < num2)
						{
							num2 = num6;
						}
						num3 += array[num5];
						break;
					}
				}
				if (num > 2 && num4 == stops)
				{
					break;
				}
				if (num == 2 && num5 > 0)
				{
					break;
				}
				if (num == 1)
				{
					break;
				}
				if (++num5 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			float num7 = this.kminStopDistance / num2;
			this.m_UILineLength = num7 * num3;
			if (this.m_UILineLength < this.kminUILineLength)
			{
				this.m_UILineLength = this.kminUILineLength;
				num7 = this.m_UILineLength / num3;
			}
			else if (this.m_UILineLength > this.kmaxUILineLength)
			{
				this.m_UILineLength = this.kmaxUILineLength;
				num7 = this.m_UILineLength / num3;
			}
			if (num <= 2)
			{
				this.m_UILineOffset = num7 * array[array.Length - 1] - 30f;
			}
			if (this.m_UILineOffset < 20f || num > 2)
			{
				this.m_UILineOffset = num7 * array[array.Length - 1] / 2f;
			}
			this.m_stopsLineSprite.set_height(this.m_UILineLength);
			this.m_stopsContainer.set_height(this.m_UILineLength + this.kvehicleButtonHeight);
			Vector3 relativePosition = this.m_lineEnd.get_relativePosition();
			relativePosition.y = this.m_UILineLength + 6f;
			this.m_lineEnd.set_relativePosition(relativePosition);
			float num8 = 0f;
			for (int j = 0; j < num; j++)
			{
				Vector3 relativePosition2 = this.m_stopButtons.items[j].get_relativePosition();
				relativePosition2.x = this.kstopsX;
				relativePosition2.y = this.ShiftVerticalPosition(num8);
				this.m_stopButtons.items[j].set_relativePosition(relativePosition2);
				num8 += array[j] * num7;
			}
			this.RefreshVehicleButtons(lineID);
			this.m_vehicleCountModifier.set_value((float)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_budget);
			if ((Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_flags & TransportLine.Flags.Complete) != TransportLine.Flags.None)
			{
				base.Find<UILabel>("LabelLineIncomplete").set_isVisible(false);
				this.m_stopsContainer.set_isVisible(true);
			}
			else
			{
				base.Find<UILabel>("LabelLineIncomplete").set_isVisible(true);
				this.m_stopsContainer.set_isVisible(false);
			}
		}
		this.UpdateBindings();
	}

	private float ShiftVerticalPosition(float y)
	{
		y += this.m_UILineOffset;
		if (y > this.m_UILineLength + 5f)
		{
			y -= this.m_UILineLength;
		}
		return y;
	}

	public void OnBudgetClicked()
	{
		if (ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy))
		{
			ToolsModifierControl.mainToolbar.ShowEconomyPanel(1);
			WorldInfoPanel.Hide<PublicTransportWorldInfoPanel>();
		}
	}

	private void RefreshVehicleButtons(ushort lineID)
	{
		this.m_vehicleButtons.SetItemCount(Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].CountVehicles(lineID));
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_vehicles;
		int num2 = 0;
		while (num != 0)
		{
			Vector3 relativePosition = this.m_vehicleButtons.items[num2].get_relativePosition();
			relativePosition.x = this.kvehiclesX;
			this.m_vehicleButtons.items[num2].set_relativePosition(relativePosition);
			this.m_vehicleButtons.items[num2].set_objectUserData(num);
			num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private ushort GetLineID()
	{
		if (this.m_InstanceID.Type == InstanceType.TransportLine)
		{
			return this.m_InstanceID.TransportLine;
		}
		if (this.m_InstanceID.Type == InstanceType.Vehicle)
		{
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)this.m_InstanceID.Vehicle].GetFirstVehicle(this.m_InstanceID.Vehicle);
			if (firstVehicle != 0)
			{
				return Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].m_transportLine;
			}
		}
		return 0;
	}

	public static string GetVehicleTypeIcon(TransportInfo.TransportType type)
	{
		if (type < TransportInfo.TransportType.Bus || type >= (TransportInfo.TransportType)PublicTransportWorldInfoPanel.sLineType.Length)
		{
			return "ToolbarIconPublicTransportFocused";
		}
		return PublicTransportWorldInfoPanel.sLineType[(int)type];
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.FloorToInt(num * 100f), 0, 100);
	}

	[DebuggerHidden]
	private IEnumerator SetDayOnly(ushort id)
	{
		PublicTransportWorldInfoPanel.<SetDayOnly>c__Iterator2 <SetDayOnly>c__Iterator = new PublicTransportWorldInfoPanel.<SetDayOnly>c__Iterator2();
		<SetDayOnly>c__Iterator.id = id;
		return <SetDayOnly>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetNightOnly(ushort id)
	{
		PublicTransportWorldInfoPanel.<SetNightOnly>c__Iterator3 <SetNightOnly>c__Iterator = new PublicTransportWorldInfoPanel.<SetNightOnly>c__Iterator3();
		<SetNightOnly>c__Iterator.id = id;
		return <SetNightOnly>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetAllDay(ushort id)
	{
		PublicTransportWorldInfoPanel.<SetAllDay>c__Iterator4 <SetAllDay>c__Iterator = new PublicTransportWorldInfoPanel.<SetAllDay>c__Iterator4();
		<SetAllDay>c__Iterator.id = id;
		return <SetAllDay>c__Iterator;
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		ushort lineID = this.GetLineID();
		if (lineID != 0)
		{
			TransportInfo info = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].Info;
			if (this.m_LineOperation == null || this.m_LineOperation.completedOrFailed)
			{
				bool flag;
				bool flag2;
				Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].GetActive(out flag, out flag2);
				this.m_DayLine.set_isChecked(flag && !flag2);
				this.m_NightLine.set_isChecked(flag2 && !flag);
				this.m_BothLine.set_isChecked(flag && flag2);
			}
			this.m_VehicleType.set_spriteName(PublicTransportWorldInfoPanel.GetVehicleTypeIcon(info.m_transportType));
			this.m_Type.set_text(Locale.Get("TRANSPORT_LINE", EnumExtensions.Name<TransportInfo.TransportType>(info.m_transportType)));
			int averageCount = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_touristPassengers.m_averageCount;
			int averageCount2 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_residentPassengers.m_averageCount;
			int averageCount3 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_childPassengers.m_averageCount;
			int averageCount4 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_teenPassengers.m_averageCount;
			int averageCount5 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_youngPassengers.m_averageCount;
			int averageCount6 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_adultPassengers.m_averageCount;
			int averageCount7 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_seniorPassengers.m_averageCount;
			int averageCount8 = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_passengers.m_carOwningPassengers.m_averageCount;
			int num = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].CountVehicles(lineID);
			int num2 = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].CalculateTargetVehicleCount();
			string text = (num2 <= 0) ? num.ToString() : (num.ToString() + " / " + num2.ToString());
			int value = PublicTransportWorldInfoPanel.GetValue(averageCount3, averageCount2);
			int value2 = PublicTransportWorldInfoPanel.GetValue(averageCount4, averageCount2);
			int value3 = PublicTransportWorldInfoPanel.GetValue(averageCount5, averageCount2);
			int num3 = PublicTransportWorldInfoPanel.GetValue(averageCount6, averageCount2);
			int value4 = PublicTransportWorldInfoPanel.GetValue(averageCount7, averageCount2);
			int num4 = value + value2 + value3 + num3 + value4;
			if (num4 != 0 && num4 != 100)
			{
				num3 = 100 - (value + value2 + value3 + value4);
			}
			this.m_AgeChart.SetValues(new int[]
			{
				value,
				value2,
				value3,
				num3,
				value4
			});
			this.m_Passengers.set_text(LocaleFormatter.FormatGeneric("TRANSPORT_LINE_PASSENGERS", new object[]
			{
				averageCount2,
				averageCount
			}));
			this.m_VehicleAmount.set_text(LocaleFormatter.FormatGeneric("TRANSPORT_LINE_VEHICLECOUNT", new object[]
			{
				text
			}));
			int num5 = 0;
			int num6 = 0;
			if (averageCount2 + averageCount != 0)
			{
				num6 += averageCount3 * 0;
				num6 += averageCount4 * 5;
				num6 += averageCount5 * ((15 * averageCount2 + 20 * averageCount + (averageCount2 + averageCount >> 1)) / (averageCount2 + averageCount));
				num6 += averageCount6 * ((20 * averageCount2 + 20 * averageCount + (averageCount2 + averageCount >> 1)) / (averageCount2 + averageCount));
				num6 += averageCount7 * ((10 * averageCount2 + 20 * averageCount + (averageCount2 + averageCount >> 1)) / (averageCount2 + averageCount));
			}
			if (num6 != 0)
			{
				num5 = (int)(((long)averageCount8 * 10000L + (long)(num6 >> 1)) / (long)num6);
				num5 = Mathf.Clamp(num5, 0, 100);
			}
			this.m_TripSaved.set_text(LocaleFormatter.FormatGeneric("TRANSPORT_LINE_TRIPSAVED", new object[]
			{
				num5
			}));
			this.UpdateVehicleButtons(lineID);
			this.UpdateStopButtons(lineID);
		}
	}

	private void UpdateVehicleButtons(ushort lineID)
	{
		if (this.m_vehicleCountMismatch)
		{
			this.RefreshVehicleButtons(lineID);
			this.m_vehicleCountMismatch = false;
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_vehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (num2 > this.m_vehicleButtons.items.Count - 1)
			{
				this.m_vehicleCountMismatch = true;
				break;
			}
			VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num].Info;
			float num3;
			float num4;
			if (info.m_vehicleAI.GetProgressStatus(num, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num], out num3, out num4))
			{
				float num5 = num3 / num4;
				float y = this.m_UILineLength * num5;
				Vector3 relativePosition = this.m_vehicleButtons.items[num2].get_relativePosition();
				relativePosition.y = this.ShiftVerticalPosition(y);
				this.m_vehicleButtons.items[num2].set_relativePosition(relativePosition);
			}
			string text;
			int num6;
			int num7;
			info.m_vehicleAI.GetBufferStatus(num, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num], out text, out num6, out num7);
			this.m_vehicleButtons.items[num2].Find<UILabel>("PassengerCount").set_text(num6.ToString() + "/" + num7.ToString());
			num = instance.m_vehicles.m_buffer[(int)num].m_nextLineVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (num2 < this.m_vehicleButtons.items.Count - 1)
		{
			this.m_vehicleCountMismatch = true;
		}
	}

	private void UpdateStopButtons(ushort lineID)
	{
		ushort stop = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].m_stops;
		foreach (UIButton current in this.m_stopButtons.items)
		{
			UILabel uILabel = current.Find<UILabel>("PassengerCount");
			uILabel.set_text(Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)lineID].CalculatePassengerCount(stop).ToString());
			stop = TransportLine.GetNextStop(stop);
		}
	}

	public void OnLinesOverviewClicked()
	{
		UIView.get_library().Show("PublicTransportDetailPanel", true, true);
	}

	public void OnDeleteLine()
	{
		ushort id = this.GetLineID();
		if (id != 0)
		{
			ConfirmPanel.ShowModal("CONFIRM_LINEDELETE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					Singleton<SimulationManager>.get_instance().AddAction(delegate
					{
						Singleton<TransportManager>.get_instance().ReleaseLine(id);
					});
					this.OnCloseButton();
				}
			});
		}
	}

	private static readonly string[] sLineType = new string[]
	{
		"SubBarPublicTransportBus",
		"SubBarPublicTransportMetro",
		"SubBarPublicTransportTrain",
		"SubBarPublicTransportShip",
		"SubBarPublicTransportPlane",
		"SubBarPublicTransportTaxi",
		"SubBarPublicTransportTram",
		"SubBarPublicTransportCableCar",
		"SubBarPublicTransportMonorail"
	};

	public Color32 m_ChildColor;

	public Color32 m_TeenColor;

	public Color32 m_YoungColor;

	public Color32 m_AdultColor;

	public Color32 m_SeniorColor;

	private UIRadialChart m_AgeChart;

	private UILabel m_ChildLegend;

	private UILabel m_TeenLegend;

	private UILabel m_YoungLegend;

	private UILabel m_AdultLegend;

	private UILabel m_SeniorLegend;

	private UISprite m_VehicleType;

	private UITextField m_NameField;

	private UIColorField m_ColorField;

	private UIButton m_ColorFieldButton;

	private UILabel m_Passengers;

	private UILabel m_VehicleAmount;

	private UILabel m_Type;

	private UILabel m_TripSaved;

	private UICheckBox m_DayLine;

	private UICheckBox m_NightLine;

	private UICheckBox m_BothLine;

	private AsyncTask m_LineOperation;

	private UISlider m_vehicleCountModifier;

	private UILabel m_vehicleCountModifierLabel;

	private UISprite m_stopsLineSprite;

	private UISprite m_lineEnd;

	private UIPanel m_stopsContainer;

	private float m_UILineLength;

	private float m_UILineOffset;

	private bool m_vehicleCountMismatch;

	private UITemplateList<UIButton> m_stopButtons;

	private UITemplateList<UIButton> m_vehicleButtons;

	private readonly float kstopsX = 78f;

	private readonly float kvehiclesX = 49f;

	private readonly float kminStopDistance = 30f;

	private readonly float kvehicleButtonHeight = 36f;

	private readonly float kminUILineLength = 370f;

	private readonly float kmaxUILineLength = 2000f;

	public static UIScrollablePanel m_scrollPanel;

	private static Vector2 m_cachedScrollPosition;
}
