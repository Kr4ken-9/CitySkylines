﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ShipAI : VehicleAI
{
	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Vector3 vector = rotation * new Vector3(0f, 0f, this.m_info.m_generatedInfo.m_size.z * 0.25f);
		Vector3 vector2 = rotation * new Vector3(this.m_info.m_generatedInfo.m_size.x * -0.5f, 0f, 0f);
		Vector3 vector3 = position + vector + vector2;
		Vector3 vector4 = position + vector - vector2;
		Vector3 vector5 = position - vector + vector2;
		Vector3 vector6 = position - vector - vector2;
		vector3.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector3, true, 0f);
		vector4.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector4, true, 0f);
		vector5.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector5, true, 0f);
		vector6.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector6, true, 0f);
		Vector3 vector7 = vector3 + vector4 - vector5 - vector6;
		Vector3 vector8 = vector3 + vector5 - vector4 - vector6;
		Vector3 vector9 = Vector3.Cross(vector8, vector7);
		rotation = Quaternion.LookRotation(vector7, vector9);
		position.y = (vector3.y + vector4.y + vector5.y + vector6.y) * 0.25f;
		Quaternion quaternion = Quaternion.Euler(swayPosition.z * 57.29578f, 0f, swayPosition.x * -57.29578f);
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation * quaternion, scale);
		result.m13 += swayPosition.y;
		return result;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		return Matrix4x4.get_identity();
	}

	public override RenderGroup.MeshData GetEffectMeshData()
	{
		if (this.m_info.m_lodMeshData != null && this.m_underwaterMeshData == null)
		{
			Vector3[] vertices = this.m_info.m_lodMeshData.m_vertices;
			int[] triangles = this.m_info.m_lodMeshData.m_triangles;
			if (triangles != null)
			{
				int[] array = new int[vertices.Length];
				for (int i = 0; i < vertices.Length; i++)
				{
					array[i] = -1;
				}
				int num = 0;
				int num2 = 0;
				for (int j = 0; j < triangles.Length; j += 3)
				{
					int num3 = triangles[j];
					int num4 = triangles[j + 1];
					int num5 = triangles[j + 2];
					Vector3 vector = vertices[num3];
					Vector3 vector2 = vertices[num4];
					Vector3 vector3 = vertices[num5];
					if (vector.y < -2f || vector2.y < -2f || vector3.y < -2f)
					{
						num2 += 3;
						if (array[num3] == -1)
						{
							array[num3] = num++;
						}
						if (array[num4] == -1)
						{
							array[num4] = num++;
						}
						if (array[num5] == -1)
						{
							array[num5] = num++;
						}
					}
				}
				this.m_underwaterMeshData = new RenderGroup.MeshData();
				this.m_underwaterMeshData.m_vertices = new Vector3[num];
				this.m_underwaterMeshData.m_triangles = new int[num2];
				num2 = 0;
				for (int k = 0; k < triangles.Length; k += 3)
				{
					int num6 = triangles[k];
					int num7 = triangles[k + 1];
					int num8 = triangles[k + 2];
					Vector3 vector4 = vertices[num6];
					Vector3 vector5 = vertices[num7];
					Vector3 vector6 = vertices[num8];
					if (vector4.y < -2f || vector5.y < -2f || vector6.y < -2f)
					{
						vector4.y = Mathf.Min(vector4.y, -2f);
						vector5.y = Mathf.Min(vector5.y, -2f);
						vector6.y = Mathf.Min(vector6.y, -2f);
						num6 = array[num6];
						num7 = array[num7];
						num8 = array[num8];
						this.m_underwaterMeshData.m_triangles[num2++] = num6;
						this.m_underwaterMeshData.m_triangles[num2++] = num7;
						this.m_underwaterMeshData.m_triangles[num2++] = num8;
						this.m_underwaterMeshData.m_vertices[num6] = vector4;
						this.m_underwaterMeshData.m_vertices[num7] = vector5;
						this.m_underwaterMeshData.m_vertices[num8] = vector6;
					}
				}
				this.m_underwaterMeshData.UpdateBounds();
			}
		}
		return this.m_underwaterMeshData;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		bool flag;
		data.m_frame0.m_position.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(data.m_frame0.m_position, false, 0f, out flag);
		data.m_frame1.m_position.y = data.m_frame0.m_position.y;
		data.m_frame2.m_position.y = data.m_frame0.m_position.y;
		data.m_frame3.m_position.y = data.m_frame0.m_position.y;
		if (flag)
		{
			data.m_flags2 |= Vehicle.Flags2.Floating;
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				data.Unspawn(vehicleID);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, 0);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, 0);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo)) == (Vehicle.Flags)0 || data.m_blockCounter == 255)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		float num = this.m_info.m_generatedInfo.m_size.z * 0.5f;
		Vector3 vector = frameData.m_rotation * Vector3.get_forward();
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num2 = -Mathf.Atan2(vector.x, vector.z);
		num2 += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num2 * 57.29578f, Vector3.get_down());
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, num);
		Vector3 vector3 = frameData.m_position + vector2;
		Vector3 vector4 = frameData.m_position - vector2;
		float acceleration = this.m_info.m_acceleration;
		float num3 = this.m_info.m_braking;
		Vector3 vector5;
		vector5..ctor(vector3.z - vector4.z, 0f, vector4.x - vector3.x);
		vector5 += new Vector3(vehicleData.m_targetPos1.z - vehicleData.m_targetPos0.z, 0f, vehicleData.m_targetPos0.x - vehicleData.m_targetPos1.x);
		if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
		{
			vector5 = -vector5;
		}
		vector5 = vector5.get_normalized() * 20f;
		Vector3 vector6 = vehicleData.m_targetPos0 - vector4;
		Vector3 vector7 = vehicleData.m_targetPos1 - vector3;
		if (vehicleData.m_targetPos1.w > 10f)
		{
			vector6 += vector5;
			vector7 += vector5;
		}
		else
		{
			float num4 = Mathf.Min(1f, vector7.get_magnitude() * 0.01f);
			vector6 += vector5 * num4;
			vector7 += vector5 * num4;
		}
		if (vehicleData.m_targetPos0.w > 10f)
		{
			leaderData.m_flags &= ~Vehicle.Flags.Leaving;
		}
		float magnitude = frameData.m_velocity.get_magnitude();
		float sqrMagnitude = vector6.get_sqrMagnitude();
		float sqrMagnitude2 = (vehicleData.m_targetPos1 - vector4).get_sqrMagnitude();
		float num5 = Mathf.Max(magnitude + magnitude * magnitude * 0.1f + acceleration, num);
		float num6 = num5 + num * 2f;
		float num7 = (magnitude + acceleration) * (0.5f + 0.5f * (magnitude + acceleration) / num3) + num * 2f;
		float num8 = Mathf.Max((num7 - num5) / 2f, 5f);
		float num9 = num5 * num5;
		float num10 = num6 * num6;
		float minSqrDistanceC = num8 * num8;
		int i = 0;
		bool flag = false;
		if ((sqrMagnitude < num9 || sqrMagnitude2 < num10 || vehicleData.m_targetPos0.w < 0.01f || vehicleData.m_targetPos1.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				this.UpdatePathTargetPositions(vehicleID, ref vehicleData, vector4, leaderID, ref leaderData, ref i, num9, num10, minSqrDistanceC);
			}
			if (i < 4)
			{
				if (leaderData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(leaderData.m_path);
					leaderData.m_path = 0u;
				}
				i = 0;
				flag = true;
				this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, frameData.m_position, leaderID, ref leaderData, ref i, num9);
			}
			while (i < 4)
			{
				vehicleData.SetTargetPos(i, vehicleData.GetTargetPos(i - 1));
				i++;
			}
			vector6 = vehicleData.m_targetPos0 - vector4;
			vector7 = vehicleData.m_targetPos1 - vector3;
			if (vehicleData.m_targetPos1.w > 10f)
			{
				vector6 += vector5;
				vector7 += vector5;
			}
			else
			{
				float num11 = Mathf.Min(1f, vector7.get_magnitude() * 0.01f);
				vector6 += vector5 * num11;
				vector7 += vector5 * num11;
			}
		}
		this.ReserveSpace(vehicleID, ref vehicleData, ref frameData, magnitude > 0.1f);
		float num12 = Mathf.Clamp(Vector3.Dot(vector7, vector6) / Mathf.Max(1f, vector6.get_sqrMagnitude()), -1f, 1f);
		Vector3 vector8;
		if (num12 < 0f)
		{
			vector8 = vector7 + (vector6 - vector7) * (0.5f + 0.5f * num12 * num12);
		}
		else
		{
			vector8 = (vector6 + vector7) * 0.5f;
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector8 = quaternion * vector8;
		Vector3 vector9 = quaternion * frameData.m_velocity;
		Vector3 vector10 = Vector3.get_forward();
		float magnitude2 = vector8.get_magnitude();
		float num13 = 0f;
		bool flag2 = false;
		Vector3 vector11 = vehicleData.m_targetPos1 * 1.5f - vehicleData.m_targetPos0 - frameData.m_position * 0.5f;
		if (vehicleData.m_targetPos1.w > 10f)
		{
			vector11 += vector5 * 0.5f;
		}
		else
		{
			float num14 = Mathf.Min(1f, vector7.get_magnitude() * 0.01f);
			vector11 += vector5 * (num14 * 0.5f);
		}
		float num15;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			num15 = 0f;
		}
		else
		{
			float num16 = -Mathf.Atan2(vector11.x, vector11.z);
			num15 = Mathf.DeltaAngle(num2 * 57.29578f, num16 * 57.29578f) * 0.0174532924f;
		}
		if (magnitude2 > 1f)
		{
			vector10 = vector8 / Mathf.Max(1f, magnitude2);
			float num17;
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 || (leaderData.m_flags2 & Vehicle.Flags2.Floating) == (Vehicle.Flags2)0)
			{
				num17 = 0f;
			}
			else
			{
				num17 = this.m_info.m_maxSpeed;
				float curve = 1.57079637f * (1f - Mathf.Min(num12, vector10.z));
				num17 = Mathf.Min(num17, this.CalculateTargetSpeed(vehicleID, ref vehicleData, vehicleData.m_targetPos0.w, curve));
				Vector3 vector12 = vehicleData.m_targetPos3 - vehicleData.m_targetPos0;
				vector12 = Vector3.Normalize(quaternion * vector12);
				curve = 1.57079637f * (1f - Vector3.Dot(vector10, vector12));
				float num18 = magnitude2;
				num17 = Mathf.Min(num17, ShipAI.CalculateMaxSpeed(num18, Mathf.Min(vehicleData.m_targetPos2.w, this.CalculateTargetSpeed(vehicleID, ref vehicleData, vehicleData.m_targetPos1.w, curve)), num3));
				num18 += Vector3.Magnitude(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
				num17 = Mathf.Min(num17, ShipAI.CalculateMaxSpeed(num18, vehicleData.m_targetPos3.w, num3));
				num18 += Vector3.Magnitude(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
				num17 = Mathf.Min(num17, ShipAI.CalculateMaxSpeed(num18, 0f, num3));
				this.CheckOtherVehicles(vehicleID, ref vehicleData, ref frameData, ref num17, ref flag2, num7, num3 * 0.9f, lodPhysics);
			}
			if (num17 < magnitude)
			{
				num13 = Mathf.Max(num17, magnitude - num3);
			}
			else
			{
				num13 = Mathf.Min(num17, magnitude + acceleration);
			}
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num17 < 0.1f)
			{
				flag2 = true;
			}
		}
		else if (magnitude < 0.1f && flag && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			return;
		}
		if (flag2)
		{
			leaderData.m_blockCounter = (byte)Mathf.Min((int)(leaderData.m_blockCounter + 1), 255);
		}
		else
		{
			leaderData.m_blockCounter = 0;
		}
		num15 = Mathf.Clamp(num15, num13 / num * -2f, num13 / num * 2f);
		if (num15 > frameData.m_angleVelocity)
		{
			frameData.m_angleVelocity = Mathf.Min(0.2f, Mathf.Min(frameData.m_angleVelocity * 0.9f + acceleration / num, num15));
		}
		else
		{
			frameData.m_angleVelocity = Mathf.Max(-0.2f, Mathf.Max(frameData.m_angleVelocity * 0.9f - acceleration / num, num15));
		}
		Vector3 vector13 = vector10 * num13;
		if ((leaderData.m_flags & Vehicle.Flags.Leaving) != (Vehicle.Flags)0)
		{
			vector13.x -= frameData.m_angleVelocity * num;
			vector13 = Vector3.ClampMagnitude(vector13, num13);
		}
		Vector3 vector14 = vector13 - vector9;
		bool flag3;
		float num19 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(frameData.m_position + frameData.m_velocity, false, 0f, out flag3);
		if (flag3)
		{
			leaderData.m_flags2 |= Vehicle.Flags2.Floating;
		}
		else
		{
			leaderData.m_flags2 &= ~Vehicle.Flags2.Floating;
			num3 = Mathf.Max(num3, Mathf.Max(4f, num19 - frameData.m_position.y));
			num13 = Mathf.Max(0f, num13 - num3);
			vector13 = Vector3.ClampMagnitude(vector13, num13);
			vector14 = vector13 - vector9;
		}
		frameData.m_velocity = Vector3.MoveTowards(frameData.m_velocity, frameData.m_rotation * vector13, num3);
		frameData.m_position += frameData.m_velocity * 0.5f;
		num2 += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num2 * 57.29578f, Vector3.get_down());
		frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector14 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
		frameData.m_swayVelocity = Vector3.ClampMagnitude(frameData.m_swayVelocity, 1f);
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = 0f;
		frameData.m_travelDistance += vector13.z;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = 5f;
		frameData.m_lightIntensity.w = 5f;
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 10f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	private void ReserveSpace(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, bool moving)
	{
		uint num = vehicleData.m_path;
		if (num != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			PathManager instance2 = Singleton<PathManager>.get_instance();
			byte b = vehicleData.m_pathPositionIndex;
			byte lastPathOffset = vehicleData.m_lastPathOffset;
			if (b == 255)
			{
				b = 0;
			}
			b = (byte)(b >> 1);
			for (int i = 0; i < 2; i++)
			{
				PathUnit.Position pathPos;
				if (!instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetPosition((int)b, out pathPos))
				{
					return;
				}
				ushort startNode = instance.m_segments.m_buffer[(int)pathPos.m_segment].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)pathPos.m_segment].m_endNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				if (Vector3.SqrMagnitude(frameData.m_position - position) >= 90000f && Vector3.SqrMagnitude(frameData.m_position - position2) >= 90000f)
				{
					return;
				}
				if (i == 0)
				{
					instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(this.m_info.m_generatedInfo.m_size.z * 3f), this.GetNoiseLevel());
					if ((b & 1) == 0 || lastPathOffset == 0 || (vehicleData.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
					{
						if ((instance.m_segments.m_buffer[(int)pathPos.m_segment].m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
						{
							return;
						}
						uint laneID = PathManager.GetLaneID(pathPos);
						if (laneID != 0u)
						{
							instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(1000f, vehicleID);
						}
						if (!moving || (vehicleData.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
						{
							return;
						}
					}
				}
				else
				{
					if ((instance.m_segments.m_buffer[(int)pathPos.m_segment].m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
					{
						return;
					}
					uint laneID2 = PathManager.GetLaneID(pathPos);
					if (laneID2 != 0u && !instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(1000f, vehicleID))
					{
						return;
					}
					if (!moving)
					{
						return;
					}
				}
				if ((b += 1) >= instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_positionCount)
				{
					num = instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
					b = 0;
					if (num == 0u)
					{
						return;
					}
				}
			}
		}
	}

	private void CheckOtherVehicles(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, float maxDistance, float maxBraking, int lodPhysics)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = vehicleData.m_targetPos3 - frameData.m_position;
		Vector3 vector2 = frameData.m_position + Vector3.ClampMagnitude(vector, maxDistance);
		Vector3 min = Vector3.Min(vehicleData.m_segment.Min(), vector2);
		Vector3 max = Vector3.Max(vehicleData.m_segment.Max(), vector2);
		int num = Mathf.Max((int)((min.x - 100f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((min.z - 100f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((max.x + 100f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((max.z + 100f) / 320f + 27f), 53);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = this.CheckOtherVehicle(vehicleID, ref vehicleData, ref frameData, ref maxSpeed, ref blocked, maxBraking, num5, ref instance.m_vehicles.m_buffer[(int)num5], min, max, lodPhysics);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private ushort CheckOtherVehicle(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, float maxBraking, ushort otherID, ref Vehicle otherData, Vector3 min, Vector3 max, int lodPhysics)
	{
		if (otherID != vehicleID && vehicleData.m_leadingVehicle != otherID && vehicleData.m_trailingVehicle != otherID)
		{
			Vector3 vector;
			Vector3 vector2;
			if (lodPhysics >= 1)
			{
				vector = otherData.m_segment.Min();
				vector2 = otherData.m_segment.Max();
			}
			else
			{
				vector = Vector3.Min(otherData.m_segment.Min(), otherData.m_targetPos3);
				vector2 = Vector3.Max(otherData.m_segment.Max(), otherData.m_targetPos3);
			}
			if (min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
			{
				Vehicle.Frame lastFrameData = otherData.GetLastFrameData();
				VehicleInfo info = otherData.Info;
				float num = frameData.m_velocity.get_magnitude() + 0.01f;
				float num2 = lastFrameData.m_velocity.get_magnitude();
				float num3 = num2 * (0.5f + 0.5f * num2 / info.m_braking) + info.m_generatedInfo.m_size.z * Mathf.Min(0.5f, num2 * 0.1f);
				num2 += 0.01f;
				float num4 = 0f;
				Vector3 vector3 = frameData.m_position;
				Vector3 vector4 = vehicleData.m_targetPos3 - frameData.m_position;
				for (int i = 1; i < 4; i++)
				{
					Vector3 vector5 = vehicleData.GetTargetPos(i);
					Vector3 vector6 = vector5 - vector3;
					if (Vector3.Dot(vector4, vector6) > 0f)
					{
						float magnitude = vector6.get_magnitude();
						Segment3 segment;
						segment..ctor(vector3, vector5);
						min = segment.Min();
						max = segment.Max();
						segment.a.y = segment.a.y * 0.5f;
						segment.b.y = segment.b.y * 0.5f;
						if (magnitude > 0.01f && min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
						{
							Vector3 a = otherData.m_segment.a;
							a.y *= 0.5f;
							float num5;
							if (segment.DistanceSqr(a, ref num5) < 400f)
							{
								float num6 = Vector3.Dot(lastFrameData.m_velocity, vector6) / magnitude;
								float num7 = num4 + magnitude * num5;
								if (num7 >= 0.01f)
								{
									num7 -= num6 + 30f;
									float num8 = Mathf.Max(0f, ShipAI.CalculateMaxSpeed(num7, num6, maxBraking));
									if (num8 < 0.01f)
									{
										blocked = true;
									}
									Vector3 vector7 = Vector3.Normalize(otherData.m_targetPos3 - otherData.GetLastFramePosition());
									float num9 = 1.2f - 1f / ((float)vehicleData.m_blockCounter * 0.02f + 0.5f);
									if (Vector3.Dot(vector6, vector7) > num9 * magnitude)
									{
										maxSpeed = Mathf.Min(maxSpeed, num8);
									}
								}
								break;
							}
							if (lodPhysics == 0)
							{
								float num10 = 0f;
								float num11 = num3;
								Vector3 vector8 = otherData.GetLastFramePosition();
								Vector3 vector9 = otherData.m_targetPos3 - vector8;
								bool flag = false;
								int num12 = 1;
								while (num12 < 4 && num11 > 0.1f)
								{
									Vector3 vector10 = otherData.GetTargetPos(num12);
									Vector3 vector11 = Vector3.ClampMagnitude(vector10 - vector8, num11);
									if (Vector3.Dot(vector9, vector11) > 0f)
									{
										vector10 = vector8 + vector11;
										float magnitude2 = vector11.get_magnitude();
										num11 -= magnitude2;
										Segment3 segment2;
										segment2..ctor(vector8, vector10);
										segment2.a.y = segment2.a.y * 0.5f;
										segment2.b.y = segment2.b.y * 0.5f;
										if (magnitude2 > 0.01f)
										{
											float num14;
											float num15;
											float num13;
											if (otherID < vehicleID)
											{
												num13 = segment2.DistanceSqr(segment, ref num14, ref num15);
											}
											else
											{
												num13 = segment.DistanceSqr(segment2, ref num15, ref num14);
											}
											if (num13 < 400f)
											{
												float num16 = num4 + magnitude * num15;
												float num17 = num10 + magnitude2 * num14 + 0.1f;
												if (num16 >= 0.01f && num16 * num2 > num17 * num)
												{
													float num18 = Vector3.Dot(lastFrameData.m_velocity, vector6) / magnitude;
													if (num16 >= 0.01f)
													{
														num16 -= num18 + 10f + otherData.Info.m_generatedInfo.m_size.z;
														float num19 = Mathf.Max(0f, ShipAI.CalculateMaxSpeed(num16, num18, maxBraking));
														if (num19 < 0.01f)
														{
															blocked = true;
														}
														maxSpeed = Mathf.Min(maxSpeed, num19);
													}
												}
												flag = true;
												break;
											}
										}
										vector9 = vector11;
										num10 += magnitude2;
										vector8 = vector10;
									}
									num12++;
								}
								if (flag)
								{
									break;
								}
							}
						}
						vector4 = vector6;
						num4 += magnitude;
						vector3 = vector5;
					}
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	public override bool CanSpawnAt(Vector3 pos)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((pos.x - 300f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((pos.z - 300f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((pos.x + 300f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((pos.z + 300f) / 320f + 27f), 53);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if (Vector3.SqrMagnitude(instance.m_vehicles.m_buffer[(int)num5].GetLastFramePosition() - pos) < 90000f)
					{
						return false;
					}
					num5 = instance.m_vehicles.m_buffer[(int)num5].m_nextGridVehicle;
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return true;
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 100f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((vector.z - 100f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 100f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((vector2.z + 100f) / 320f + 27f), 53);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = ShipAI.CheckOverlap(segment, ignoreVehicle, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 400f)
		{
			overlap = true;
		}
		return otherData.m_nextGridVehicle;
	}

	protected void UpdatePathTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistanceA, float minSqrDistanceB, float minSqrDistanceC)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = vehicleData.m_targetPos0;
		vector.w = 1000f;
		float num = minSqrDistanceA;
		uint num2 = vehicleData.m_path;
		byte b = vehicleData.m_pathPositionIndex;
		byte b2 = vehicleData.m_lastPathOffset;
		if (b == 255)
		{
			b = 0;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = 0;
			}
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)num2)].CalculatePathPositionOffset(b >> 1, vector, out b2))
			{
				this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
				return;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].GetPosition(b >> 1, out position))
		{
			this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
			return;
		}
		uint num3 = PathManager.GetLaneID(position);
		Segment3 segment;
		while (true)
		{
			if ((b & 1) == 0)
			{
				bool flag = true;
				while (b2 != position.m_offset)
				{
					if (flag)
					{
						flag = false;
					}
					else
					{
						float num4 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
						int num5;
						if (num4 < 0f)
						{
							num5 = 4;
						}
						else
						{
							num5 = 4 + Mathf.CeilToInt(num4 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_length + 1f));
						}
						if (b2 > position.m_offset)
						{
							b2 = (byte)Mathf.Max((int)b2 - num5, (int)position.m_offset);
						}
						else if (b2 < position.m_offset)
						{
							b2 = (byte)Mathf.Min((int)b2 + num5, (int)position.m_offset);
						}
					}
					Vector3 vector2;
					Vector3 vector3;
					float num6;
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, b2, out vector2, out vector3, out num6);
					vector.Set(vector2.x, vector2.y, vector2.z, Mathf.Min(vector.w, num6));
					float sqrMagnitude = (vector2 - refPos).get_sqrMagnitude();
					if (sqrMagnitude >= num)
					{
						if (index <= 0)
						{
							vehicleData.m_lastPathOffset = b2;
						}
						vehicleData.SetTargetPos(index++, vector);
						if (index == 1)
						{
							num = minSqrDistanceB;
						}
						else
						{
							num = minSqrDistanceC;
							refPos = vector;
						}
						vector.w = 1000f;
						if (index == 4)
						{
							return;
						}
					}
				}
				b += 1;
				b2 = 0;
				if (index <= 0)
				{
					vehicleData.m_pathPositionIndex = b;
					vehicleData.m_lastPathOffset = b2;
				}
			}
			int num7 = (b >> 1) + 1;
			uint num8 = num2;
			if (num7 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_positionCount)
			{
				num7 = 0;
				num8 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_nextPathUnit;
				if (num8 == 0u)
				{
					goto Block_17;
				}
			}
			PathUnit.Position position2;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].GetPosition(num7, out position2))
			{
				goto Block_19;
			}
			NetInfo info = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
			if (info.m_lanes.Length <= (int)position2.m_lane)
			{
				goto Block_20;
			}
			uint laneID = PathManager.GetLaneID(position2);
			NetInfo.Lane lane = info.m_lanes[(int)position2.m_lane];
			if (lane.m_laneType != NetInfo.LaneType.Vehicle)
			{
				goto Block_21;
			}
			byte b3 = 0;
			if (num3 != laneID)
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
				segment = default(Segment3);
				Vector3 vector4;
				float num9;
				this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, position.m_offset, out segment.a, out vector4, out num9);
				bool flag2 = b2 == 0;
				if (flag2)
				{
					if ((leaderData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
					{
						flag2 = (vehicleData.m_trailingVehicle == 0);
					}
					else
					{
						flag2 = (vehicleData.m_leadingVehicle == 0);
					}
				}
				float num10;
				if (flag2)
				{
					PathUnit.Position nextPosition;
					if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].GetNextPosition(num7, out nextPosition))
					{
						nextPosition = default(PathUnit.Position);
					}
					Vector3 vector5;
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position2, laneID, b3, position, num3, position.m_offset, index, out segment.b, out vector5, out num10);
				}
				else
				{
					Vector3 vector5;
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position2, laneID, b3, out segment.b, out vector5, out num10);
				}
				if (num10 < 0.01f)
				{
					goto Block_27;
				}
				float num11 = segment.Length();
				if (num11 > 1f)
				{
					ushort num12;
					if (b3 == 0)
					{
						num12 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					}
					else if (b3 == 255)
					{
						num12 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
					}
					else
					{
						num12 = 0;
					}
					while (b2 < 255)
					{
						float num13 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
						int num14;
						if (num13 < 0f)
						{
							num14 = 8;
						}
						else
						{
							num14 = 8 + Mathf.CeilToInt(num13 * 256f / (num11 + 1f));
						}
						b2 = (byte)Mathf.Min((int)b2 + num14, 255);
						Vector3 vector6 = segment.Position((float)b2 * 0.003921569f);
						vector.Set(vector6.x, vector6.y, vector6.z, Mathf.Min(vector.w, num10));
						float sqrMagnitude2 = (vector6 - refPos).get_sqrMagnitude();
						if (sqrMagnitude2 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							if (num12 != 0)
							{
								this.UpdateNodeTargetPos(vehicleID, ref vehicleData, num12, ref instance2.m_nodes.m_buffer[(int)num12], ref vector, index);
							}
							vehicleData.SetTargetPos(index++, vector);
							if (index == 1)
							{
								num = minSqrDistanceB;
							}
							else
							{
								num = minSqrDistanceC;
								refPos = vector;
							}
							vector.w = 1000f;
							if (index == 4)
							{
								return;
							}
						}
					}
				}
			}
			else
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
			}
			if (index <= 0)
			{
				if (num7 == 0)
				{
					Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
				}
				if (num7 >= (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].m_positionCount - 1) && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].m_nextPathUnit == 0u && leaderID != 0)
				{
					this.ArrivingToDestination(leaderID, ref leaderData);
				}
			}
			num2 = num8;
			b = (byte)(num7 << 1);
			b2 = b3;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = b;
				vehicleData.m_lastPathOffset = b2;
				vehicleData.m_flags = ((vehicleData.m_flags & ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition)) | info.m_setVehicleFlags);
			}
			position = position2;
			num3 = laneID;
		}
		return;
		Block_17:
		if (index <= 0)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		vector.w = 1f;
		vehicleData.SetTargetPos(index++, vector);
		return;
		Block_19:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		Block_20:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		Block_21:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		Block_27:
		if (index <= 0)
		{
			vehicleData.m_lastPathOffset = b2;
		}
		vector = segment.a;
		vector.w = 0f;
		while (index < 4)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		bool flag = false;
		bool flag2 = false;
		NetInfo info = instance.m_segments.m_buffer[(int)prevPos.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)prevPos.m_lane)
		{
			float speedLimit = info.m_lanes[(int)prevPos.m_lane].m_speedLimit;
			if (speedLimit < 2f)
			{
				flag = true;
			}
		}
		info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			float speedLimit2 = info.m_lanes[(int)position.m_lane].m_speedLimit;
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, speedLimit2, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
			if (speedLimit2 < 2f)
			{
				flag2 = true;
			}
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
		pos.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(pos, false, 0f);
		if (!flag && flag2)
		{
			if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
			{
				maxSpeed = 0f;
				return;
			}
			ushort startNode = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position3 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			if (ShipAI.CheckOverlap(new Segment3(position2, position3), vehicleID))
			{
				maxSpeed = 0f;
				return;
			}
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			float speedLimit = info.m_lanes[(int)position.m_lane].m_speedLimit;
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
		pos.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(pos, false, 0f);
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		if (Vector3.SqrMagnitude(endPos - startPos) < 100f)
		{
			if (vehicleData.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				vehicleData.m_path = 0u;
			}
			vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
			vehicleData.m_targetPos0 = endPos;
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
			this.TrySpawn(vehicleID, ref vehicleData);
			return true;
		}
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true);
	}

	protected bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays)
	{
		VehicleInfo info = this.m_info;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (ShipAI.CheckOverlap(vehicleData.m_segment, 0))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		if (vehicleData.m_path != 0u)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(0, out pathPos))
			{
				uint laneID = PathManager.GetLaneID(pathPos);
				if (laneID != 0u && !Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
				{
					vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
					return false;
				}
			}
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	public TransportInfo m_transportInfo;

	[NonSerialized]
	private RenderGroup.MeshData m_underwaterMeshData;
}
