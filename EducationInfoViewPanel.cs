﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class EducationInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
		this.m_ElementaryGraduated = base.Find<UILabel>("ElementaryGraduated");
		this.m_ElementaryEligible = base.Find<UILabel>("ElementaryEligible");
		this.m_ElementaryCapacity = base.Find<UILabel>("ElementaryCapacity");
		this.m_ElementaryMeter = base.Find<UISlider>("ElementaryMeter");
		this.m_HighGraduated = base.Find<UILabel>("HighGraduated");
		this.m_HighEligible = base.Find<UILabel>("HighEligible");
		this.m_HighCapacity = base.Find<UILabel>("HighCapacity");
		this.m_HighMeter = base.Find<UISlider>("HighMeter");
		this.m_UnivGraduated = base.Find<UILabel>("UnivGraduated");
		this.m_UnivEligible = base.Find<UILabel>("UnivEligible");
		this.m_UnivCapacity = base.Find<UILabel>("UnivCapacity");
		this.m_UnivMeter = base.Find<UISlider>("UnivMeter");
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.RoundToInt(num * 100f), 0, 100);
	}

	private static float GetPercentageV(int value, int total)
	{
		return (float)value / (float)total;
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("RateGradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("CoverageGradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[19].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[19].m_inactiveColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[19].m_negativeColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[19].m_targetColor);
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
				this.m_EducationChart = base.Find<UIRadialChart>("EducationChart");
				UIRadialChart.SliceSettings arg_174_0 = this.m_EducationChart.GetSlice(0);
				Color32 color = this.m_UneducatedColor;
				this.m_EducationChart.GetSlice(0).set_outterColor(color);
				arg_174_0.set_innerColor(color);
				UIRadialChart.SliceSettings arg_1A2_0 = this.m_EducationChart.GetSlice(1);
				color = this.m_EducatedColor;
				this.m_EducationChart.GetSlice(1).set_outterColor(color);
				arg_1A2_0.set_innerColor(color);
				UIRadialChart.SliceSettings arg_1D0_0 = this.m_EducationChart.GetSlice(2);
				color = this.m_WellEducatedColor;
				this.m_EducationChart.GetSlice(2).set_outterColor(color);
				arg_1D0_0.set_innerColor(color);
				UIRadialChart.SliceSettings arg_1FE_0 = this.m_EducationChart.GetSlice(3);
				color = this.m_HighlyEducatedColor;
				this.m_EducationChart.GetSlice(3).set_outterColor(color);
				arg_1FE_0.set_innerColor(color);
				this.m_UneducatedLegend = base.Find<UILabel>("UneducatedAmount");
				this.m_EducatedLegend = base.Find<UILabel>("EducatedAmount");
				this.m_WellEducatedLegend = base.Find<UILabel>("WellEducatedAmount");
				this.m_HighlyEducatedLegend = base.Find<UILabel>("HighlyEducatedAmount");
				this.m_UneducatedLegend.set_color(this.m_UneducatedColor);
				this.m_EducatedLegend.set_color(this.m_EducatedColor);
				this.m_WellEducatedLegend.set_color(this.m_WellEducatedColor);
				this.m_HighlyEducatedLegend.set_color(this.m_HighlyEducatedColor);
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			int num7 = 0;
			int num8 = 0;
			int num9 = 0;
			if (Singleton<DistrictManager>.get_exists())
			{
				int finalCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_educated0Data.m_finalCount;
				int finalCount2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_educated1Data.m_finalCount;
				int finalCount3 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_educated2Data.m_finalCount;
				int finalCount4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_educated3Data.m_finalCount;
				int total = finalCount + finalCount2 + finalCount3 + finalCount4;
				int num10 = EducationInfoViewPanel.GetValue(finalCount, total);
				int value = EducationInfoViewPanel.GetValue(finalCount2, total);
				int value2 = EducationInfoViewPanel.GetValue(finalCount3, total);
				int value3 = EducationInfoViewPanel.GetValue(finalCount4, total);
				int num11 = num10 + value + value2 + value3;
				if (num11 != 0 && num11 != 100)
				{
					num10 = 100 - (value + value2 + value3);
				}
				this.m_EducationChart.SetValues(new float[]
				{
					EducationInfoViewPanel.GetPercentageV(finalCount, total),
					EducationInfoViewPanel.GetPercentageV(finalCount2, total),
					EducationInfoViewPanel.GetPercentageV(finalCount3, total),
					EducationInfoViewPanel.GetPercentageV(finalCount4, total)
				});
				this.m_UneducatedLegend.set_text(LocaleFormatter.FormatPercentage(num10));
				this.m_EducatedLegend.set_text(LocaleFormatter.FormatPercentage(value));
				this.m_WellEducatedLegend.set_text(LocaleFormatter.FormatPercentage(value2));
				this.m_HighlyEducatedLegend.set_text(LocaleFormatter.FormatPercentage(value3));
				num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation1Rate();
				num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation1Capacity();
				num3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation1Need();
				num4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation2Rate();
				num5 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation2Capacity();
				num6 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation2Need();
				num7 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation3Rate();
				num8 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation3Capacity();
				num9 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetEducation3Need();
			}
			this.m_ElementaryGraduated.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_GRADUATEPERCENT"), num));
			this.m_ElementaryEligible.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_ELIGIBLE"), num3));
			this.m_ElementaryCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_CAPACITY"), num2));
			this.m_ElementaryMeter.set_value((float)base.GetPercentage(num2, num3));
			this.m_HighGraduated.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_GRADUATEPERCENT"), num4));
			this.m_HighEligible.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_ELIGIBLE"), num6));
			this.m_HighCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_CAPACITY"), num5));
			this.m_HighMeter.set_value((float)base.GetPercentage(num5, num6));
			this.m_UnivGraduated.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_GRADUATEPERCENT"), num7));
			this.m_UnivEligible.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_ELIGIBLE"), num9));
			this.m_UnivCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_EDUCATION_CAPACITY"), num8));
			this.m_UnivMeter.set_value((float)base.GetPercentage(num8, num9));
		}
	}

	public Color32 m_UneducatedColor;

	public Color32 m_EducatedColor;

	public Color32 m_WellEducatedColor;

	public Color32 m_HighlyEducatedColor;

	private UIRadialChart m_EducationChart;

	private UILabel m_UneducatedLegend;

	private UILabel m_EducatedLegend;

	private UILabel m_WellEducatedLegend;

	private UILabel m_HighlyEducatedLegend;

	private UILabel m_ElementaryGraduated;

	private UILabel m_ElementaryEligible;

	private UILabel m_ElementaryCapacity;

	private UISlider m_ElementaryMeter;

	private UILabel m_HighGraduated;

	private UILabel m_HighEligible;

	private UILabel m_HighCapacity;

	private UISlider m_HighMeter;

	private UILabel m_UnivGraduated;

	private UILabel m_UnivEligible;

	private UILabel m_UnivCapacity;

	private UISlider m_UnivMeter;

	private bool m_initialized;
}
