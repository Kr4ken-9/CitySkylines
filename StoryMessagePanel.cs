﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class StoryMessagePanel : ToolsModifierControl
{
	private void Awake()
	{
		this.m_milestoneBuffer = new List<TriggerMilestone>();
		this.m_messagePanel = base.Find<UIPanel>("PanelMessage");
		this.m_winPanel = base.Find<UIPanel>("PanelWin");
		this.m_losePanel = base.Find<UIPanel>("PanelLose");
		this.m_finePanel = base.Find<UIPanel>("PanelFine");
		this.m_rewardPanel = base.Find<UIPanel>("PanelReward");
		this.m_reasonPanel = base.Find<UIPanel>("PanelReason");
		this.m_OKPanel = base.Find<UIPanel>("PanelOK");
		this.m_restartPanel = base.Find<UIPanel>("PanelRestart");
		this.m_wonPanel = base.Find<UIPanel>("PanelWon");
		this.m_scrollbar = base.Find<UIScrollbar>("Scrollbar");
		this.m_scrollbarOfReason = base.Find<UIScrollbar>("ScrollbarOfReason");
		this.m_title = base.Find<UILabel>("Caption");
		this.m_congrats = base.Find<UITextureSprite>("Congrats");
		Renderer[] componentsInChildren = this.m_fireworks.GetComponentsInChildren<Renderer>();
		this.m_fireworkMaterials = new Material[componentsInChildren.Length];
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			this.m_fireworkMaterials[i] = componentsInChildren[i].get_material();
		}
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void SetFireWorksRenderQueue(int rdrq)
	{
		for (int i = 0; i < this.m_fireworkMaterials.Length; i++)
		{
			this.m_fireworkMaterials[i].set_renderQueue(rdrq);
		}
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		base.get_component().set_isVisible(false);
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnCloseButton();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnOkayButton();
				p.Use();
			}
		}
	}

	private void HideAllComponents()
	{
		this.m_messagePanel.set_isVisible(false);
		this.m_winPanel.set_isVisible(false);
		this.m_losePanel.set_isVisible(false);
		this.m_finePanel.set_isVisible(false);
		this.m_rewardPanel.set_isVisible(false);
		this.m_reasonPanel.set_isVisible(false);
	}

	public void AddMilestoneToBuffer(TriggerMilestone milestone)
	{
		this.m_milestoneBuffer.Add(milestone);
		this.CheckBuffer();
	}

	private void CheckBuffer()
	{
		bool flag = false;
		StoryMessagePanel.ButtonPanelType buttonPanelType = StoryMessagePanel.ButtonPanelType.OK;
		while (!base.get_component().get_isVisible() && this.m_milestoneBuffer.Count > 0 && !flag)
		{
			this.HideAllComponents();
			TriggerMilestone triggerMilestone = this.m_milestoneBuffer[0];
			this.m_milestoneBuffer.RemoveAt(0);
			int num = 0;
			if (triggerMilestone.m_effects != null)
			{
				TriggerEffect[] effects = triggerMilestone.m_effects;
				for (int i = 0; i < effects.Length; i++)
				{
					TriggerEffect triggerEffect = effects[i];
					MessageEffect messageEffect = triggerEffect as MessageEffect;
					FeeEffect feeEffect = triggerEffect as FeeEffect;
					RewardEffect rewardEffect = triggerEffect as RewardEffect;
					WinEffect winEffect = triggerEffect as WinEffect;
					LoseEffect loseEffect = triggerEffect as LoseEffect;
					if (messageEffect != null)
					{
						this.m_messagePanel.set_isVisible(true);
						string scenarioAsset = Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset;
						string text = null;
						if (!string.IsNullOrEmpty(scenarioAsset) && triggerMilestone.m_triggerName != null)
						{
							text = StringUtils.SafeFormat("{0}/{1}", new object[]
							{
								scenarioAsset,
								triggerMilestone.m_triggerName
							});
						}
						if (text != null && Locale.Exists("SCENARIO_MESSAGE_TEXT", text))
						{
							this.m_messagePanel.Find<UILabel>("MessageText").set_text(Locale.Get("SCENARIO_MESSAGE_TEXT", text));
						}
						else
						{
							this.m_messagePanel.Find<UILabel>("MessageText").set_text(messageEffect.m_message);
						}
						flag = true;
						if (num < 2)
						{
							this.m_title.set_text(Locale.Get("STORYMESSAGE_TITLE"));
							num = 2;
						}
					}
					else if (feeEffect != null)
					{
						this.m_finePanel.set_isVisible(true);
						this.m_finePanel.Find<UILabel>("Label").set_text(StringUtils.SafeFormat(Locale.Get("DEFAULTMONEYLOSTMESSAGE"), feeEffect.m_moneyAmount.ToString()));
						flag = true;
						if (num < 1)
						{
							this.m_title.set_text(Locale.Get("DEFAULTMONEYLOSTTITLE"));
							num = 1;
						}
					}
					else if (rewardEffect != null)
					{
						this.m_rewardPanel.set_isVisible(true);
						this.m_rewardPanel.Find<UILabel>("Label").set_text(StringUtils.SafeFormat(Locale.Get("DEFAULTMONEYGAINEDMESSAGE"), rewardEffect.m_moneyAmount.ToString()));
						flag = true;
						if (num < 1)
						{
							this.m_title.set_text(Locale.Get("DEFAULTMONEYGAINEDTITLE"));
							num = 1;
						}
					}
					else if (winEffect != null)
					{
						this.m_winPanel.set_isVisible(true);
						buttonPanelType = StoryMessagePanel.ButtonPanelType.Won;
						string scenarioAsset2 = Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset;
						if (!string.IsNullOrEmpty(scenarioAsset2) && Locale.Exists("SCENARIO_WIN_TEXT", scenarioAsset2))
						{
							this.m_winPanel.Find<UILabel>("Label").set_text(Locale.Get("SCENARIO_WIN_TEXT", scenarioAsset2));
						}
						else
						{
							this.m_winPanel.Find<UILabel>("Label").set_text(StringUtils.SafeFormat(Locale.Get("DEFAULTSCENARIOWONMESSAGE"), Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioName));
						}
						flag = true;
						if (num < 3)
						{
							this.m_title.set_text(Locale.Get("DEFAULTSCENARIOWONTITLE"));
							num = 3;
						}
						this.m_reasonPanel.set_isVisible(true);
						this.m_reasonPanel.Find<UILabel>("MessageText").set_text(this.GetWinningOrLosingReason(triggerMilestone));
					}
					else if (loseEffect != null)
					{
						this.m_losePanel.set_isVisible(true);
						buttonPanelType = StoryMessagePanel.ButtonPanelType.Lost;
						flag = true;
						if (num < 3)
						{
							this.m_title.set_text(Locale.Get("DEFAULTSCENARIOLOSTTITLE"));
							num = 3;
						}
						this.m_losePanel.Find<UILabel>("Label").set_text(Locale.Get("DEFAULTSCENARIOLOSTMESSAGE"));
						this.m_reasonPanel.set_isVisible(true);
						this.m_reasonPanel.Find<UILabel>("MessageText").set_text(this.GetWinningOrLosingReason(triggerMilestone));
					}
				}
			}
			this.m_OKPanel.set_isVisible(buttonPanelType == StoryMessagePanel.ButtonPanelType.OK);
			this.m_restartPanel.set_isVisible(buttonPanelType == StoryMessagePanel.ButtonPanelType.Lost);
			this.m_wonPanel.set_isVisible(buttonPanelType == StoryMessagePanel.ButtonPanelType.Won);
			this.m_congrats.set_isVisible(buttonPanelType == StoryMessagePanel.ButtonPanelType.Won);
			if (buttonPanelType == StoryMessagePanel.ButtonPanelType.Won)
			{
				this.m_fireworks.Play();
			}
			if (flag)
			{
				this.Show();
			}
		}
	}

	private string GetWinningOrLosingReason(TriggerMilestone tm)
	{
		List<string> list = new List<string>();
		this.AddReasons(tm, list);
		string text = string.Empty;
		int num = 0;
		foreach (string current in list)
		{
			text += current;
			if (num < list.Count - 1)
			{
				text += ", ";
			}
			num++;
		}
		return text;
	}

	private void AddReasons(TriggerMilestone tm, List<string> reasons)
	{
		MilestoneInfo[] conditions = tm.m_conditions;
		for (int i = 0; i < conditions.Length; i++)
		{
			MilestoneInfo milestoneInfo = conditions[i];
			reasons.Add(milestoneInfo.GetLocalizedProgress().m_description);
		}
	}

	private void Show()
	{
		this.m_scrollbar.set_value(0f);
		this.m_scrollbarOfReason.set_value(0f);
		base.get_component().set_isVisible(true);
		base.get_component().FitChildrenVertically();
		base.get_component().CenterToParent();
	}

	private void Hide()
	{
		this.m_fireworks.Stop();
		base.get_component().set_isVisible(false);
		this.CheckBuffer();
	}

	public void OnCloseButton()
	{
		this.Hide();
	}

	public void OnOkayButton()
	{
		this.Hide();
	}

	public void OnLoadButton()
	{
		UIView.get_library().Show("LoadPanel");
	}

	public void OnExitToMainMenu()
	{
		this.Hide();
		Singleton<LoadingManager>.get_instance().UnloadLevel();
	}

	public void OnRestartButton()
	{
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().Send(PopsTelemetryEventFormatting.Playthrough(Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier, Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode, Singleton<SimulationManager>.get_instance().m_metaData.m_MapName, Singleton<SimulationManager>.get_instance().m_metaData.m_currentDateTime - Singleton<SimulationManager>.get_instance().m_metaData.m_startingDateTime, false));
		}
		base.CloseEverything();
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.ScenarioMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				try
				{
					ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
					if ((scenarioMetaData.environment != "winter" || flag) && scenarioMetaData.isPublished)
					{
						Package.Asset assetRef = scenarioMetaData.assetRef;
						string b = StringUtils.SafeFormat("{0}.{1}", new object[]
						{
							assetRef.get_package().get_packageName(),
							assetRef.get_name()
						});
						if (metaData.m_ScenarioAsset == b)
						{
							this.Hide();
							this.StartNewGameRoutine(scenarioMetaData);
							return;
						}
					}
				}
				catch (Exception ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
				}
			}
		}
		CODebugBase<LogChannel>.Error(LogChannel.None, "Could not find scenario asset for restarting scenario.");
	}

	private void StartNewGameRoutine(ScenarioMetaData scenarioMetaData)
	{
		Package.Asset assetRef = scenarioMetaData.assetRef;
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		SimulationMetaData simulationMetaData = new SimulationMetaData
		{
			m_CityName = metaData.m_CityName,
			m_gameInstanceIdentifier = Guid.NewGuid().ToString(),
			m_invertTraffic = metaData.m_invertTraffic,
			m_disableAchievements = ((Singleton<PluginManager>.get_instance().get_enabledModCount() <= 0) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True),
			m_startingDateTime = DateTime.Now,
			m_currentDateTime = DateTime.Now,
			m_newGameAppVersion = 172090642u,
			m_updateMode = SimulationManager.UpdateMode.NewGameFromScenario,
			m_ScenarioAsset = StringUtils.SafeFormat("{0}.{1}", new object[]
			{
				assetRef.get_package().get_packageName(),
				assetRef.get_name()
			}),
			m_ScenarioName = scenarioMetaData.scenarioName,
			m_MapName = scenarioMetaData.scenarioName
		};
		if (scenarioMetaData != null && scenarioMetaData.mapThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(scenarioMetaData.mapThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		Singleton<LoadingManager>.get_instance().LoadLevel(assetRef, "Game", "InGame", simulationMetaData);
	}

	private Material[] m_fireworkMaterials;

	public ParticleSystem m_fireworks;

	public int m_renderQueueOffset = 1;

	private UITextureSprite m_congrats;

	private List<TriggerMilestone> m_milestoneBuffer;

	private UILabel m_title;

	private UIPanel m_messagePanel;

	private UIPanel m_winPanel;

	private UIPanel m_losePanel;

	private UIPanel m_finePanel;

	private UIPanel m_rewardPanel;

	private UIPanel m_reasonPanel;

	private UIPanel m_OKPanel;

	private UIPanel m_restartPanel;

	private UIPanel m_wonPanel;

	private UIScrollbar m_scrollbar;

	private UIScrollbar m_scrollbarOfReason;

	private enum ButtonPanelType
	{
		OK,
		Lost,
		Won
	}
}
