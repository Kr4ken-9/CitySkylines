﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class AudioProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		AudioProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new AudioProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("AudioProperties");
			Singleton<AudioManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public AudioReverbZone m_reverbZone;

	public AudioInfo[] m_ambients;

	public AudioInfo[] m_ambientsNight;

	public AudioInfo m_changeRadioChannelSound;

	public string[] m_musicFiles;

	public string[] m_musicFilesNight;

	public float m_musicVolume = 1f;

	public Vector3 m_listenerOffset;
}
