﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PoliceCarAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.CrimeRate)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_PRISON_RETURN");
			}
			if ((data.m_flags & (Vehicle.Flags.Stopped | Vehicle.Flags.WaitingTarget)) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_PRISON_WAIT");
			}
			if (data.m_targetBuilding != 0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_PRISON_PICKINGUP");
			}
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CONFUSED");
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_POLICE_RETURN");
			}
			if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_POLICE_STOPPED");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				if ((data.m_flags & Vehicle.Flags.Leaving) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_POLICE_STOP_WAIT");
				}
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_POLICE_PATROL_WAIT");
			}
			else
			{
				if ((data.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_POLICE_PATROL");
				}
				if (data.m_targetBuilding != 0)
				{
					target = InstanceID.Empty;
					target.Building = data.m_targetBuilding;
					return Locale.Get("VEHICLE_STATUS_POLICE_EMERGENCY");
				}
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_CONFUSED");
			}
		}
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			localeKey = "PrisonVan";
		}
		else
		{
			localeKey = "Police";
		}
		current = (int)data.m_transferSize;
		max = this.m_criminalCapacity;
		if ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && current == max)
		{
			current = max - 1;
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_policeCount + this.m_criminalCapacity, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.UnloadCriminals(vehicleID, ref data);
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~(Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingTarget);
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		base.EnsureCitizenUnits(vehicleID, ref data, this.m_policeCount + this.m_criminalCapacity);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			if (this.m_info.m_class.m_level < ItemClass.Level.Level4)
			{
				if (PoliceCarAI.CountCriminals(targetBuilding) != 0)
				{
					data.m_flags |= Vehicle.Flags.Emergency2;
				}
				else
				{
					data.m_flags &= ~Vehicle.Flags.Emergency2;
				}
			}
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else
		{
			data.m_flags &= ~Vehicle.Flags.Emergency2;
			if ((int)data.m_transferSize < this.m_criminalCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 7;
				offer.Vehicle = vehicleID;
				if (data.m_sourceBuilding != 0)
				{
					offer.Position = data.GetLastFramePosition() * 0.25f + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position * 0.75f;
				}
				else
				{
					offer.Position = data.GetLastFramePosition();
				}
				offer.Amount = 1;
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
				data.m_flags |= Vehicle.Flags.WaitingTarget;
			}
			else
			{
				data.m_flags |= Vehicle.Flags.GoingBack;
			}
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public static int CountCriminals(ushort building)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		uint num = instance.m_buildings.m_buffer[(int)building].m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Criminal && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == building)
				{
					num3++;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num3;
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_criminalCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
			if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 && this.CanLeave(vehicleID, ref vehicleData))
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
				vehicleData.m_flags |= Vehicle.Flags.Leaving;
			}
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
		else
		{
			frameData.m_blinkState = (((vehicleData.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0) ? 0f : 10f);
			this.TryCollectCrime(vehicleID, ref vehicleData, ref frameData);
			base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
			if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
			{
				if (this.CanLeave(vehicleID, ref vehicleData))
				{
					vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
					vehicleData.m_flags |= Vehicle.Flags.Leaving;
				}
			}
			else if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
			{
				this.ArriveAtTarget(vehicleID, ref vehicleData);
			}
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			this.ArrestCriminals(vehicleID, ref data, data.m_targetBuilding);
			data.m_flags |= Vehicle.Flags.Stopped;
		}
		else
		{
			int num = -this.m_crimeCapacity;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
			if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
			{
				this.ArrestCriminals(vehicleID, ref data, data.m_targetBuilding);
				for (int i = 0; i < this.m_policeCount; i++)
				{
					this.CreatePolice(vehicleID, ref data, Citizen.AgePhase.Adult0);
				}
				data.m_flags |= Vehicle.Flags.Stopped;
			}
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private void ArrestCriminals(ushort vehicleID, ref Vehicle vehicleData, ushort building)
	{
		if ((int)vehicleData.m_transferSize >= this.m_criminalCapacity)
		{
			return;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		uint num = instance.m_buildings.m_buffer[(int)building].m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Criminal || instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested) && !instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == building)
				{
					instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, vehicleID, 0u);
					if (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle != vehicleID)
					{
						vehicleData.m_transferSize = (ushort)this.m_criminalCapacity;
						return;
					}
					instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested = true;
					ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance3 != 0)
					{
						instance2.ReleaseCitizenInstance(instance3);
					}
					instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Moving;
					if ((int)(vehicleData.m_transferSize += 1) >= this.m_criminalCapacity)
					{
						return;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		bool flag = true;
		bool flag2 = false;
		uint num = vehicleData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						CitizenInfo info = instance.m_instances.m_buffer[(int)instance2].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service)
						{
							if ((instance.m_instances.m_buffer[(int)instance2].m_flags & CitizenInstance.Flags.EnteringVehicle) == CitizenInstance.Flags.None && (instance.m_instances.m_buffer[(int)instance2].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
							{
								flag2 = true;
							}
							flag = false;
						}
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!flag2 && !flag)
		{
			num = vehicleData.m_citizenUnits;
			num2 = 0;
			while (num != 0u)
			{
				uint nextUnit2 = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				for (int j = 0; j < 5; j++)
				{
					uint citizen2 = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(j);
					if (citizen2 != 0u)
					{
						ushort instance3 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen2)].m_instance;
						if (instance3 != 0 && (instance.m_instances.m_buffer[(int)instance3].m_flags & CitizenInstance.Flags.EnteringVehicle) == CitizenInstance.Flags.None)
						{
							CitizenInfo info2 = instance.m_instances.m_buffer[(int)instance3].Info;
							info2.m_citizenAI.SetTarget(instance3, ref instance.m_instances.m_buffer[(int)instance3], 0);
						}
					}
				}
				num = nextUnit2;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return flag;
	}

	private void CreatePolice(ushort vehicleID, ref Vehicle data, Citizen.AgePhase agePhase)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		CitizenInfo groupCitizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Male, Citizen.SubCulture.Generic, agePhase);
		if (groupCitizenInfo != null)
		{
			int family = instance.m_randomizer.Int32(256u);
			uint num = 0u;
			if (instance2.CreateCitizen(out num, 90, family, ref instance.m_randomizer, groupCitizenInfo.m_gender))
			{
				ushort num2;
				if (instance2.CreateCitizenInstance(out num2, ref instance.m_randomizer, groupCitizenInfo, num))
				{
					Vector3 randomDoorPosition = data.GetRandomDoorPosition(ref instance.m_randomizer, VehicleInfo.DoorType.Exit);
					groupCitizenInfo.m_citizenAI.SetCurrentVehicle(num2, ref instance2.m_instances.m_buffer[(int)num2], 0, 0u, randomDoorPosition);
					groupCitizenInfo.m_citizenAI.SetTarget(num2, ref instance2.m_instances.m_buffer[(int)num2], data.m_targetBuilding);
					instance2.m_citizens.m_buffer[(int)((UIntPtr)num)].SetVehicle(num, vehicleID, 0u);
				}
				else
				{
					instance2.ReleaseCitizen(num);
				}
			}
		}
	}

	private void SpawnPrisoner(ushort vehicleID, ref Vehicle data, uint citizen)
	{
		if (data.m_sourceBuilding == 0)
		{
			return;
		}
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		Citizen.Gender gender = Citizen.GetGender(citizen);
		CitizenInfo groupCitizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, gender, Citizen.SubCulture.Generic, Citizen.AgePhase.Young0);
		ushort num;
		if (groupCitizenInfo != null && instance2.CreateCitizenInstance(out num, ref instance.m_randomizer, groupCitizenInfo, citizen))
		{
			Vector3 randomDoorPosition = data.GetRandomDoorPosition(ref instance.m_randomizer, VehicleInfo.DoorType.Exit);
			groupCitizenInfo.m_citizenAI.SetCurrentVehicle(num, ref instance2.m_instances.m_buffer[(int)num], 0, 0u, randomDoorPosition);
			groupCitizenInfo.m_citizenAI.SetTarget(num, ref instance2.m_instances.m_buffer[(int)num], data.m_sourceBuilding);
		}
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		int transferSize = (int)data.m_transferSize;
		BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
		info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
		data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
		this.UnloadCriminals(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		return true;
	}

	private void UnloadCriminals(ushort vehicleID, ref Vehicle data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Moving && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						instance.ReleaseCitizenInstance(instance2);
					}
					instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, data.m_sourceBuilding, 0u);
					if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding != 0)
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Visit;
						if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
						{
							this.SpawnPrisoner(vehicleID, ref data, citizen);
						}
					}
					else
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Home;
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested = false;
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_transferSize = 0;
		if (num3 != 0 && data.m_sourceBuilding != 0)
		{
			BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
			DistrictManager instance4 = Singleton<DistrictManager>.get_instance();
			byte district = instance4.GetDistrict(instance3.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position);
			District[] expr_212_cp_0_cp_0 = instance4.m_districts.m_buffer;
			byte expr_212_cp_0_cp_1 = district;
			expr_212_cp_0_cp_0[(int)expr_212_cp_0_cp_1].m_productionData.m_tempCriminalExtra = expr_212_cp_0_cp_0[(int)expr_212_cp_0_cp_1].m_productionData.m_tempCriminalExtra + (uint)num3;
		}
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
				return;
			}
		}
		else if (((leaderData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0 || this.m_info.m_class.m_level >= ItemClass.Level.Level4) && leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 targetPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out targetPos2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, 2f));
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				Vector3 endPos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].CalculateSidewalkPosition();
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			Vector3 endPos2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].CalculateSidewalkPosition();
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	protected override float CalculateTargetSpeed(ushort vehicleID, ref Vehicle data, float speedLimit, float curve)
	{
		if ((data.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0)
		{
			return base.CalculateTargetSpeed(vehicleID, ref data, speedLimit, curve);
		}
		float num = 1000f / (1f + curve * 500f / this.m_info.m_turning) + 2f;
		float num2 = 16f * speedLimit;
		return Mathf.Min(Mathf.Min(num, num2), this.m_info.m_maxSpeed * 1.5f);
	}

	private void TryCollectCrime(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) == Vehicle.Flags.Underground)
		{
			return;
		}
		Vector3 position = frameData.m_position;
		float num = position.x - 32f;
		float num2 = position.z - 32f;
		float num3 = position.x + 32f;
		float num4 = position.z + 32f;
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					this.TryCollectCrime(vehicleID, ref vehicleData, ref frameData, num9, ref instance.m_buildings.m_buffer[(int)num9]);
					num9 = instance.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private void TryCollectCrime(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort buildingID, ref Building building)
	{
		Vector3 vector = building.CalculateSidewalkPosition();
		if (Vector3.SqrMagnitude(vector - frameData.m_position) < 1024f)
		{
			int num = -this.m_crimeCapacity;
			BuildingInfo info = building.Info;
			info.m_buildingAI.ModifyMaterialBuffer(buildingID, ref building, TransferManager.TransferReason.Crime, ref num);
		}
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, undergroundTarget, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, default(PathUnit.Position), NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, 20000f, this.IsHeavyVehicle(), this.IgnoreBlocked(vehicleID, ref vehicleData), false, false, false, false, this.CombustionEngine()))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public int m_policeCount = 2;

	[CustomizableProperty("Crime capacity")]
	public int m_crimeCapacity = 50000;

	[CustomizableProperty("Criminal capacity")]
	public int m_criminalCapacity = 3;
}
