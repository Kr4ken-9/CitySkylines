﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class IndustrialExtractorAI : PrivateBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_info.m_class.m_service != ItemClass.Service.Industrial)
		{
			throw new PrefabException(this.m_info, "IndustrialExtractorAI used with other service type (" + this.m_info.m_class.m_service + ")");
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.NaturalResources:
			if (this.ShowConsumption(buildingID, ref data))
			{
				return IndustrialBuildingAI.GetResourceColor(this.m_info.m_class.m_subService);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.LandValue:
		case InfoManager.InfoMode.Districts:
			IL_19:
			if (infoMode != InfoManager.InfoMode.BuildingLevel)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (this.m_info.m_class.m_subService == ItemClass.SubService.IndustrialGeneric)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f, 0.333f + (float)this.m_info.m_class.m_level * 0.333f);
			}
			return Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f;
		case InfoManager.InfoMode.Connections:
		{
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode != InfoManager.SubInfoMode.WaterPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
			if (outgoingTransferReason != TransferManager.TransferReason.None && (data.m_tempExport != 0 || data.m_finalExport != 0))
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)outgoingTransferReason];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		}
		goto IL_19;
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		if (data.m_fireIntensity != 0 || !Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data);
			return;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		if (randomizer.Int32(5u) != 0)
		{
			return;
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data, 0.25f);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		if (resource == NaturalResourceManager.Resource.Pollution)
		{
			return 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return 15;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.PrivateIncome)
		{
			int width = data.Width;
			int length = data.Length;
			int num = width * length;
			int num2 = (100 * num + 99) / 100;
			return num2 * 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		return (100 * num + 99) / 100;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		return StringUtils.SafeFormat("{0}\n{1}: {2}\n{3}: {4}", new object[]
		{
			base.GetDebugString(buildingID, ref data),
			this.GetExtractedResourceType().ToString(),
			data.m_customBuffer1,
			this.GetOutgoingTransferReason().ToString(),
			data.m_customBuffer2
		});
	}

	public override string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		if ((data.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_IMPOSSIBLE");
		}
		if (this.m_info.m_class.m_subService != ItemClass.SubService.IndustrialGeneric)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_SPECIAL_INDUSTRY");
		}
		return base.GetLevelUpInfo(buildingID, ref data, out progress);
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
		if (specialization != DistrictPolicies.Specialization.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			District[] expr_39_cp_0 = instance.m_districts.m_buffer;
			byte expr_39_cp_1 = district;
			expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect = (expr_39_cp_0[(int)expr_39_cp_1].m_specializationPoliciesEffect | specialization);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		if (version < 187u)
		{
			int width = data.Width;
			int length = data.Length;
			int num = this.MaxOutgoingLoadSize();
			int num2 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
			int num3 = Mathf.Max(num2 * 500, num);
			int num4 = Mathf.Max(num2 * 500, num * 2);
			data.m_customBuffer2 = (ushort)((int)data.m_customBuffer2 + num4 - num3);
		}
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainTaxes, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseTaxes, position, 1.5f);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == this.GetOutgoingTransferReason())
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, false, true))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
					ushort building = offer.Building;
					if (building != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
					{
						int amount;
						int num2;
						randomVehicleInfo.m_vehicleAI.GetSize(num, ref vehicles.m_buffer[(int)num], out amount, out num2);
						CommonBuildingAI.ExportResource(buildingID, ref data, material, amount);
					}
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == this.GetOutgoingTransferReason())
		{
			int customBuffer = (int)data.m_customBuffer1;
			amountDelta = Mathf.Clamp(amountDelta, -customBuffer, 0);
			data.m_customBuffer1 = (ushort)(customBuffer + amountDelta);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(outgoingTransferReason, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			instance2.m_districts.m_buffer[(int)district].AddIndustrialData(buildingData.Width * buildingData.Length, (buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255, this.m_info.m_class.m_subService);
		}
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0 && instance.m_randomizer.Int32(10u) == 0)
		{
			DistrictPolicies.Specialization specializationPolicies = instance2.m_districts.m_buffer[(int)district].m_specializationPolicies;
			DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
			if (specialization != DistrictPolicies.Specialization.None)
			{
				if ((specializationPolicies & specialization) == DistrictPolicies.Specialization.None)
				{
					if (Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
					{
						buildingData.m_flags |= Building.Flags.Demolishing;
						instance.m_currentBuildIndex += 1u;
					}
				}
				else
				{
					District[] expr_175_cp_0 = instance2.m_districts.m_buffer;
					byte expr_175_cp_1 = district;
					expr_175_cp_0[(int)expr_175_cp_1].m_specializationPoliciesEffect = (expr_175_cp_0[(int)expr_175_cp_1].m_specializationPoliciesEffect | specialization);
				}
			}
			else if ((specializationPolicies & (DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore)) != DistrictPolicies.Specialization.None && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
			{
				buildingData.m_flags |= Building.Flags.Demolishing;
				instance.m_currentBuildIndex += 1u;
			}
		}
		uint num = (instance.m_currentFrameIndex & 3840u) >> 8;
		if (num == 15u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempExport = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		District[] expr_52_cp_0 = instance.m_districts.m_buffer;
		byte expr_52_cp_1 = district;
		expr_52_cp_0[(int)expr_52_cp_1].m_servicePoliciesEffect = (expr_52_cp_0[(int)expr_52_cp_1].m_servicePoliciesEffect | (servicePolicies & (DistrictPolicies.Services.PowerSaving | DistrictPolicies.Services.WaterSaving | DistrictPolicies.Services.SmokeDetectors | DistrictPolicies.Services.Recycling | DistrictPolicies.Services.ExtraInsulation | DistrictPolicies.Services.NoElectricity | DistrictPolicies.Services.OnlyElectricity)));
		District[] expr_76_cp_0 = instance.m_districts.m_buffer;
		byte expr_76_cp_1 = district;
		expr_76_cp_0[(int)expr_76_cp_1].m_cityPlanningPoliciesEffect = (expr_76_cp_0[(int)expr_76_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods));
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = base.HandleWorkers(buildingID, ref buildingData, ref behaviourData, ref num, ref num2, ref num3);
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num4 = 0;
		}
		if (Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			num4 = num4 + 1 >> 1;
		}
		int width = buildingData.Width;
		int length = buildingData.Length;
		int num5 = this.MaxOutgoingLoadSize();
		int num6 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
		int num7 = num6 * 500;
		int num8 = Mathf.Max(num7, num5 * 2);
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		if (num4 != 0)
		{
			int num9 = (num8 - (int)buildingData.m_customBuffer1) / 100;
			num4 = Mathf.Max(0, Mathf.Min(num4, (num9 * 20000 + num8 - 1) / num8));
			int num10 = (num4 * num6 + Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1000u)) / 1000;
			if (num9 > 0 && num10 != 0)
			{
				num10 = Singleton<NaturalResourceManager>.get_instance().TryFetchResource(this.GetExtractedResourceType(), num10, num9, buildingData.m_position, 50f);
				buildingData.m_customBuffer1 += (ushort)(num10 * 100);
				if (num10 != 0)
				{
					buildingData.m_incomingProblemTimer = 0;
					IndustrialBuildingAI.ProductType productType = IndustrialBuildingAI.GetProductType(outgoingTransferReason);
					if (productType != IndustrialBuildingAI.ProductType.None)
					{
						StatisticsManager instance2 = Singleton<StatisticsManager>.get_instance();
						StatisticBase statisticBase = instance2.Acquire<StatisticArray>(StatisticType.GoodsProduced);
						statisticBase.Acquire<StatisticInt32>((int)productType, 5).Add(num10 * 100);
					}
				}
				else if ((int)buildingData.m_customBuffer1 < num5)
				{
					buildingData.m_flags |= Building.Flags.Downgrading;
				}
			}
			num4 = num10 * 10;
		}
		int num11;
		int waterConsumption;
		int sewageAccumulation;
		int num12;
		int num13;
		this.GetConsumptionRates(new Randomizer((int)buildingID), num4, out num11, out waterConsumption, out sewageAccumulation, out num12, out num13);
		int heatingConsumption = 0;
		if (num11 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num11 * 3 + 8 >> 4);
				num13 = num13 * 95 / 100;
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num11 + 2 >> 2);
			}
		}
		if (num12 != 0 && (servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
		{
			num12 = Mathf.Max(1, num12 * 85 / 100);
			num13 = num13 * 95 / 100;
		}
		if (Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			num13 <<= 1;
		}
		if (num4 != 0)
		{
			int num14 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num11, ref heatingConsumption, ref waterConsumption, ref sewageAccumulation, ref num12, servicePolicies);
			num4 = (num4 * num14 + 99) / 100;
			if (num4 != 0)
			{
				if (num13 != 0)
				{
					Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PrivateIncome, num13, this.m_info.m_class);
				}
				int num15;
				int num16;
				this.GetPollutionRates(num4, cityPlanningPolicies, out num15, out num16);
				if (num15 != 0)
				{
					if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.FilterIndustrialWaste) != DistrictPolicies.CityPlanning.None)
					{
						District[] expr_353_cp_0 = instance.m_districts.m_buffer;
						byte expr_353_cp_1 = district;
						expr_353_cp_0[(int)expr_353_cp_1].m_cityPlanningPoliciesEffect = (expr_353_cp_0[(int)expr_353_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.FilterIndustrialWaste);
						Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 13, this.m_info.m_class);
						if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(9u) == 0)
						{
							Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num15, num15, buildingData.m_position, 60f);
						}
					}
					else if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
					{
						Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num15, num15, buildingData.m_position, 60f);
					}
				}
				if (num16 != 0)
				{
					Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num16, buildingData.m_position, 60f);
				}
				if (num14 < 100)
				{
					buildingData.m_flags |= Building.Flags.RateReduced;
				}
				else
				{
					buildingData.m_flags &= ~Building.Flags.RateReduced;
				}
				buildingData.m_flags |= Building.Flags.Active;
			}
			else
			{
				buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
			}
		}
		else
		{
			num11 = 0;
			heatingConsumption = 0;
			waterConsumption = 0;
			sewageAccumulation = 0;
			num12 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
		}
		int num17 = 0;
		int wellbeing = 0;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (behaviourData.m_healthAccumulation != 0)
		{
			if (num != 0)
			{
				num17 = (behaviourData.m_healthAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Health, behaviourData.m_healthAccumulation, buildingData.m_position, radius);
		}
		if (behaviourData.m_wellbeingAccumulation != 0)
		{
			if (num != 0)
			{
				wellbeing = (behaviourData.m_wellbeingAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Wellbeing, behaviourData.m_wellbeingAccumulation, buildingData.m_position, radius);
		}
		int num18 = Citizen.GetHappiness(num17, wellbeing) * 15 / 100;
		if (num3 != 0)
		{
			num18 += num * 40 / num3;
		}
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) == Notification.Problem.None)
		{
			num18 += 20;
		}
		if (buildingData.m_problems == Notification.Problem.None)
		{
			num18 += 25;
		}
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(this.m_info.m_class);
		int num19 = (int)((ItemClass.Level)9 - this.m_info.m_class.m_level);
		int num20 = (int)((ItemClass.Level)11 - this.m_info.m_class.m_level);
		if (taxRate < num19)
		{
			num18 += num19 - taxRate;
		}
		if (taxRate > num20)
		{
			num18 -= taxRate - num20;
		}
		if (taxRate >= num20 + 4)
		{
			if (buildingData.m_taxProblemTimer != 0 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
			{
				int num21 = taxRate - num20 >> 2;
				buildingData.m_taxProblemTimer = (byte)Mathf.Min(255, (int)buildingData.m_taxProblemTimer + num21);
				if (buildingData.m_taxProblemTimer >= 96)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_taxProblemTimer >= 32)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
				}
			}
		}
		else
		{
			buildingData.m_taxProblemTimer = (byte)Mathf.Max(0, (int)(buildingData.m_taxProblemTimer - 1));
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
		}
		num18 = Mathf.Clamp(num18, 0, 100);
		buildingData.m_health = (byte)num17;
		buildingData.m_happiness = (byte)num18;
		buildingData.m_citizenCount = (byte)num;
		base.HandleDead(buildingID, ref buildingData, ref behaviourData, num2);
		int num22 = behaviourData.m_crimeAccumulation / 10;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num22 = num22 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num22, num);
		int num23 = (int)buildingData.m_crimeBuffer;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num, buildingData.m_position, radius);
			int num24 = behaviourData.m_educated0Count * 100 + behaviourData.m_educated1Count * 50 + behaviourData.m_educated2Count * 30;
			num24 = num24 / num + 100;
			buildingData.m_fireHazard = (byte)num24;
			num23 = (num23 + (num >> 1)) / num;
		}
		else
		{
			buildingData.m_fireHazard = 0;
			num23 = 0;
		}
		int num25 = 0;
		int num26 = 0;
		int num27 = 0;
		int num28 = 0;
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			base.CalculateOwnVehicles(buildingID, ref buildingData, outgoingTransferReason, ref num25, ref num26, ref num27, ref num28);
			buildingData.m_tempExport = (byte)Mathf.Clamp(num28, (int)buildingData.m_tempExport, 255);
		}
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoResources | Notification.Problem.NoPlaceforGoods);
			if ((int)buildingData.m_customBuffer1 > num8 - (num7 >> 1))
			{
				buildingData.m_outgoingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_outgoingProblemTimer + 1));
				if (buildingData.m_outgoingProblemTimer >= 192)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_outgoingProblemTimer >= 128)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods);
				}
			}
			else
			{
				buildingData.m_outgoingProblemTimer = 0;
			}
			buildingData.m_problems = problem;
			instance.m_districts.m_buffer[(int)district].AddIndustrialData(ref behaviourData, num17, num18, num23, num3, num, Mathf.Max(0, num3 - num2), (int)this.m_info.m_class.m_level, num11, heatingConsumption, waterConsumption, sewageAccumulation, num12, num13, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255), 0, (int)buildingData.m_finalExport, this.m_info.m_class.m_subService);
			if (buildingData.m_fireIntensity == 0 && outgoingTransferReason != TransferManager.TransferReason.None)
			{
				int num29 = Mathf.Max(1, num6 / 6);
				int customBuffer = (int)buildingData.m_customBuffer1;
				if (customBuffer >= num5 && num25 < num29)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = Mathf.Max(1, customBuffer * 8 / num8);
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = Mathf.Min(customBuffer / num5, num29 - num25);
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(outgoingTransferReason, offer);
				}
			}
			base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		}
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 255;
		fireTolerance = 10;
		return true;
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
		case ImmaterialResourceManager.Resource.PoliceDepartment:
		case ImmaterialResourceManager.Resource.DeathCare:
		{
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num);
			int num2 = ImmaterialResourceManager.CalculateResourceEffect(num, 100, 500, 50, 100);
			int num3 = ImmaterialResourceManager.CalculateResourceEffect(num + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num3 - num2) / 250f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FireDepartment:
		{
			int num4;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num4);
			int num5 = ImmaterialResourceManager.CalculateResourceEffect(num4, 100, 500, 50, 100);
			int num6 = ImmaterialResourceManager.CalculateResourceEffect(num4 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num6 - num5) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.EducationElementary:
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		case ImmaterialResourceManager.Resource.EducationUniversity:
		case ImmaterialResourceManager.Resource.Entertainment:
		{
			int num7;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num7);
			int num8 = ImmaterialResourceManager.CalculateResourceEffect(num7, 100, 500, 50, 100);
			int num9 = ImmaterialResourceManager.CalculateResourceEffect(num7 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num9 - num8) / 400f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PublicTransport:
		{
			int num10;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num10);
			int num11 = ImmaterialResourceManager.CalculateResourceEffect(num10, 100, 500, 50, 100);
			int num12 = ImmaterialResourceManager.CalculateResourceEffect(num10 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num12 - num11) / 150f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.NoisePollution:
		{
			int num13;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num13);
			int num14 = ImmaterialResourceManager.CalculateResourceEffect(num13, 10, 100, 0, 100);
			int num15 = ImmaterialResourceManager.CalculateResourceEffect(num13 + Mathf.RoundToInt(amount), 10, 100, 0, 100);
			return Mathf.Clamp((float)(num15 - num14) / 350f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Abandonment:
		{
			int num16;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num16);
			int num17 = ImmaterialResourceManager.CalculateResourceEffect(num16, 15, 50, 10, 20);
			int num18 = ImmaterialResourceManager.CalculateResourceEffect(num16 + Mathf.RoundToInt(amount), 15, 50, 10, 20);
			return Mathf.Clamp((float)(num18 - num17) / 350f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.CargoTransport:
		{
			int num19;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num19);
			int num20 = ImmaterialResourceManager.CalculateResourceEffect(num19, 100, 500, 50, 100);
			int num21 = ImmaterialResourceManager.CalculateResourceEffect(num19 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num21 - num20) / 50f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.RadioCoverage:
		case ImmaterialResourceManager.Resource.DisasterCoverage:
		{
			int num22;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num22);
			int num23 = ImmaterialResourceManager.CalculateResourceEffect(num22, 50, 100, 20, 25);
			int num24 = ImmaterialResourceManager.CalculateResourceEffect(num22 + Mathf.RoundToInt(amount), 50, 100, 20, 25);
			return Mathf.Clamp((float)(num24 - num23) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FirewatchCoverage:
		{
			int num25;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num25);
			int num26 = ImmaterialResourceManager.CalculateResourceEffect(num25, 100, 1000, 0, 25);
			int num27 = ImmaterialResourceManager.CalculateResourceEffect(num25 + Mathf.RoundToInt(amount), 100, 1000, 0, 25);
			return Mathf.Clamp((float)(num27 - num26) / 100f, -1f, 1f);
		}
		}
		return base.GetEventImpact(buildingID, ref data, resource, amount);
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return base.GetEventImpact(buildingID, ref data, resource, amount);
		}
		byte b;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out b);
		int num = ImmaterialResourceManager.CalculateResourceEffect((int)b, 50, 255, 50, 100);
		int num2 = ImmaterialResourceManager.CalculateResourceEffect((int)b + Mathf.RoundToInt(amount), 50, 255, 50, 100);
		return Mathf.Clamp((float)(num2 - num) / 300f, -1f, 1f);
	}

	private NaturalResourceManager.Resource GetExtractedResourceType()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return NaturalResourceManager.Resource.Forest;
		case ItemClass.SubService.IndustrialFarming:
			return NaturalResourceManager.Resource.Fertility;
		case ItemClass.SubService.IndustrialOil:
			return NaturalResourceManager.Resource.Oil;
		case ItemClass.SubService.IndustrialOre:
			return NaturalResourceManager.Resource.Ore;
		default:
			return NaturalResourceManager.Resource.None;
		}
	}

	private TransferManager.TransferReason GetOutgoingTransferReason()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return TransferManager.TransferReason.Logs;
		case ItemClass.SubService.IndustrialFarming:
			return TransferManager.TransferReason.Grain;
		case ItemClass.SubService.IndustrialOil:
			return TransferManager.TransferReason.Oil;
		case ItemClass.SubService.IndustrialOre:
			return TransferManager.TransferReason.Ore;
		default:
			return TransferManager.TransferReason.None;
		}
	}

	private int MaxOutgoingLoadSize()
	{
		return 8000;
	}

	private DistrictPolicies.Specialization SpecialPolicyNeeded()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return DistrictPolicies.Specialization.Forest;
		case ItemClass.SubService.IndustrialFarming:
			return DistrictPolicies.Specialization.Farming;
		case ItemClass.SubService.IndustrialOil:
			return DistrictPolicies.Specialization.Oil;
		case ItemClass.SubService.IndustrialOre:
			return DistrictPolicies.Specialization.Ore;
		default:
			return DistrictPolicies.Specialization.None;
		}
	}

	public override int CalculateHomeCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		level0 = 0;
		level1 = 0;
		level2 = 0;
		level3 = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 100;
				level0 = 100;
				level1 = 0;
				level2 = 0;
				level3 = 0;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 150;
				level0 = 20;
				level1 = 60;
				level2 = 20;
				level3 = 0;
			}
			else
			{
				num = 200;
				level0 = 5;
				level1 = 15;
				level2 = 30;
				level3 = 50;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			num = 100;
			level0 = 100;
			level1 = 0;
			level2 = 0;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			num = 100;
			level0 = 100;
			level1 = 0;
			level2 = 0;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			num = 150;
			level0 = 20;
			level1 = 60;
			level2 = 20;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			num = 150;
			level0 = 20;
			level1 = 60;
			level2 = 20;
			level3 = 0;
		}
		if (num != 0)
		{
			num = Mathf.Max(200, width * length * num + r.Int32(100u)) / 100;
			int num2 = level0 + level1 + level2 + level3;
			if (num2 != 0)
			{
				level0 = (num * level0 + r.Int32((uint)num2)) / num2;
				num -= level0;
			}
			num2 = level1 + level2 + level3;
			if (num2 != 0)
			{
				level1 = (num * level1 + r.Int32((uint)num2)) / num2;
				num -= level1;
			}
			num2 = level2 + level3;
			if (num2 != 0)
			{
				level2 = (num * level2 + r.Int32((uint)num2)) / num2;
				num -= level2;
			}
			level3 = num;
		}
	}

	public override int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 100;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 140;
			}
			else
			{
				num = 160;
			}
		}
		else
		{
			num = 100;
		}
		if (num != 0)
		{
			num = Mathf.Max(100, width * length * num + r.Int32(100u)) / 100;
		}
		return num;
	}

	public override void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		ItemClass @class = this.m_info.m_class;
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			ItemClass.Level level = @class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						electricityConsumption = 250;
						waterConsumption = 160;
						sewageAccumulation = 160;
						garbageAccumulation = 100;
						incomeAccumulation = 240;
					}
				}
				else
				{
					electricityConsumption = 200;
					waterConsumption = 130;
					sewageAccumulation = 130;
					garbageAccumulation = 150;
					incomeAccumulation = 200;
				}
			}
			else
			{
				electricityConsumption = 150;
				waterConsumption = 100;
				sewageAccumulation = 100;
				garbageAccumulation = 200;
				incomeAccumulation = 160;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			electricityConsumption = 300;
			waterConsumption = 250;
			sewageAccumulation = 250;
			garbageAccumulation = 150;
			incomeAccumulation = 300;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			electricityConsumption = 350;
			waterConsumption = 200;
			sewageAccumulation = 200;
			garbageAccumulation = 200;
			incomeAccumulation = 360;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			electricityConsumption = 90;
			waterConsumption = 60;
			sewageAccumulation = 60;
			garbageAccumulation = 100;
			incomeAccumulation = 140;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			electricityConsumption = 110;
			waterConsumption = 350;
			sewageAccumulation = 350;
			garbageAccumulation = 150;
			incomeAccumulation = 180;
		}
		if (electricityConsumption != 0)
		{
			electricityConsumption = Mathf.Max(100, productionRate * electricityConsumption + r.Int32(100u)) / 100;
		}
		if (waterConsumption != 0)
		{
			int num = r.Int32(100u);
			waterConsumption = Mathf.Max(100, productionRate * waterConsumption + num) / 100;
			if (sewageAccumulation != 0)
			{
				sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + num) / 100;
			}
		}
		else if (sewageAccumulation != 0)
		{
			sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + r.Int32(100u)) / 100;
		}
		if (garbageAccumulation != 0)
		{
			garbageAccumulation = Mathf.Max(100, productionRate * garbageAccumulation + r.Int32(100u)) / 100;
		}
		if (incomeAccumulation != 0)
		{
			incomeAccumulation = productionRate * incomeAccumulation;
		}
	}

	public override void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		ItemClass @class = this.m_info.m_class;
		groundPollution = 0;
		noisePollution = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			ItemClass.Level level = @class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						groundPollution = 150;
						noisePollution = 150;
					}
				}
				else
				{
					groundPollution = 200;
					noisePollution = 200;
				}
			}
			else
			{
				groundPollution = 300;
				noisePollution = 300;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			groundPollution = 400;
			noisePollution = 500;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			groundPollution = 500;
			noisePollution = 400;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			groundPollution = 0;
			noisePollution = 200;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			groundPollution = 0;
			noisePollution = 200;
		}
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.FilterIndustrialWaste) != DistrictPolicies.CityPlanning.None)
		{
			groundPollution = groundPollution + 1 >> 1;
		}
		groundPollution = (productionRate * groundPollution + 99) / 100;
		noisePollution = (productionRate * noisePollution + 99) / 100;
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.CountUnchecked("BUILDING_NAME", text);
		if (num != 0u)
		{
			return Locale.Get("BUILDING_NAME", text, randomizer.Int32(num));
		}
		text = this.m_info.m_class.m_subService.ToString();
		num = Locale.Count("EXTRACTOR_NAME", text);
		return Locale.Get("EXTRACTOR_NAME", text, randomizer.Int32(num));
	}
}
