﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TouristAI : HumanAI
{
	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Connections)
		{
			return base.GetColor(instanceID, ref data, infoMode);
		}
		InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
		if (currentSubMode == InfoManager.SubInfoMode.WindPower)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort instanceID, ref CitizenInstance data, Vector3 position, Quaternion rotation, Vector3 velocity, Color color, bool underground)
	{
		if ((data.m_flags & CitizenInstance.Flags.AtTarget) != CitizenInstance.Flags.None)
		{
			if ((data.m_flags & CitizenInstance.Flags.SittingDown) != CitizenInstance.Flags.None)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 2, underground);
				return;
			}
			if ((data.m_flags & (CitizenInstance.Flags.Panicking | CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) == CitizenInstance.Flags.Panicking)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 1, underground);
				return;
			}
			if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating | CitizenInstance.Flags.Cheering)) == CitizenInstance.Flags.Cheering)
			{
				this.m_info.SetRenderParameters(position, rotation, velocity, color, 5, underground);
				return;
			}
		}
		if ((data.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None)
		{
			this.m_info.SetRenderParameters(position, rotation, velocity, color, 3, underground);
		}
		else if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			this.m_info.SetRenderParameters(position, rotation, Vector3.get_zero(), color, 1, underground);
		}
		else
		{
			this.m_info.SetRenderParameters(position, rotation, velocity, color, (int)(instanceID & 4), underground);
		}
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint citizen = data.m_citizen;
		ushort num = 0;
		if (citizen != 0u)
		{
			num = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
		}
		ushort targetBuilding = data.m_targetBuilding;
		if (targetBuilding == 0)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		bool flag = (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None;
		bool flag2 = data.m_path == 0u && (data.m_flags & CitizenInstance.Flags.HangAround) != CitizenInstance.Flags.None;
		if (num != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
			if (info.m_class.m_service == ItemClass.Service.Residential && info.m_vehicleType != VehicleInfo.VehicleType.Bicycle)
			{
				if (info.m_vehicleAI.GetOwnerID(num, ref instance2.m_vehicles.m_buffer[(int)num]).Citizen == citizen)
				{
					if (flag)
					{
						target = InstanceID.Empty;
						return Locale.Get("CITIZEN_STATUS_DRIVINGTO_OUTSIDE");
					}
					target = InstanceID.Empty;
					target.Building = targetBuilding;
					return Locale.Get("CITIZEN_STATUS_DRIVINGTO");
				}
			}
			else if (info.m_class.m_service == ItemClass.Service.PublicTransport || info.m_class.m_service == ItemClass.Service.Disaster)
			{
				if (flag)
				{
					target = InstanceID.Empty;
					return Locale.Get("CITIZEN_STATUS_TRAVELLINGTO_OUTSIDE");
				}
				target = InstanceID.Empty;
				target.Building = targetBuilding;
				return Locale.Get("CITIZEN_STATUS_TRAVELLINGTO");
			}
		}
		if (flag)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_GOINGTO_OUTSIDE");
		}
		if (flag2)
		{
			target = InstanceID.Empty;
			target.Building = targetBuilding;
			return Locale.Get("CITIZEN_STATUS_VISITING");
		}
		target = InstanceID.Empty;
		target.Building = targetBuilding;
		return Locale.Get("CITIZEN_STATUS_GOINGTO");
	}

	public override string GetLocalizedStatus(uint citizenID, ref Citizen data, out InstanceID target)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort instance2 = data.m_instance;
		if (instance2 != 0)
		{
			return this.GetLocalizedStatus(instance2, ref instance.m_instances.m_buffer[(int)instance2], out target);
		}
		Citizen.Location currentLocation = data.CurrentLocation;
		ushort visitBuilding = data.m_visitBuilding;
		if (currentLocation == Citizen.Location.Visit && visitBuilding != 0)
		{
			target = InstanceID.Empty;
			target.Building = visitBuilding;
			return Locale.Get("CITIZEN_STATUS_VISITING");
		}
		target = InstanceID.Empty;
		return Locale.Get("CITIZEN_STATUS_CONFUSED");
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddSourceCitizen(instanceID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void SimulationStep(uint citizenID, ref Citizen data)
	{
		this.UpdateLocation(citizenID, ref data);
	}

	public override void StartTransfer(uint citizenID, ref Citizen data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (data.m_flags == Citizen.Flags.None || data.Dead || data.Sick)
		{
			return;
		}
		switch (material)
		{
		case TransferManager.TransferReason.ShoppingB:
		case TransferManager.TransferReason.ShoppingC:
		case TransferManager.TransferReason.ShoppingD:
		case TransferManager.TransferReason.ShoppingE:
		case TransferManager.TransferReason.ShoppingF:
		case TransferManager.TransferReason.ShoppingG:
		case TransferManager.TransferReason.ShoppingH:
		case TransferManager.TransferReason.EntertainmentB:
		case TransferManager.TransferReason.EntertainmentC:
		case TransferManager.TransferReason.EntertainmentD:
			break;
		default:
			switch (material)
			{
			case TransferManager.TransferReason.EvacuateA:
			case TransferManager.TransferReason.EvacuateB:
			case TransferManager.TransferReason.EvacuateC:
			case TransferManager.TransferReason.EvacuateD:
			case TransferManager.TransferReason.EvacuateVipA:
			case TransferManager.TransferReason.EvacuateVipB:
			case TransferManager.TransferReason.EvacuateVipC:
			case TransferManager.TransferReason.EvacuateVipD:
				data.m_flags |= Citizen.Flags.Evacuating;
				if (base.StartMoving(citizenID, ref data, data.m_visitBuilding, offer.Building))
				{
					data.SetVisitplace(citizenID, offer.Building, 0u);
				}
				else
				{
					data.SetVisitplace(citizenID, offer.Building, 0u);
					if (data.m_visitBuilding == offer.Building)
					{
						data.CurrentLocation = Citizen.Location.Visit;
					}
				}
				return;
			default:
				switch (material)
				{
				case TransferManager.TransferReason.Shopping:
				case TransferManager.TransferReason.Entertainment:
					goto IL_A6;
				case TransferManager.TransferReason.LeaveCity0:
				case TransferManager.TransferReason.LeaveCity1:
				case TransferManager.TransferReason.LeaveCity2:
					data.m_flags &= ~Citizen.Flags.Evacuating;
					if (base.StartMoving(citizenID, ref data, data.m_visitBuilding, offer.Building))
					{
						data.SetVisitplace(citizenID, 0, 0u);
					}
					return;
				}
				return;
			}
			break;
		}
		IL_A6:
		data.m_flags &= ~Citizen.Flags.Evacuating;
		if (base.StartMoving(citizenID, ref data, data.m_visitBuilding, offer.Building))
		{
			data.SetVisitplace(citizenID, offer.Building, 0u);
		}
	}

	private bool DoRandomMove()
	{
		uint vehicleCount = (uint)Singleton<VehicleManager>.get_instance().m_vehicleCount;
		uint instanceCount = (uint)Singleton<CitizenManager>.get_instance().m_instanceCount;
		if (vehicleCount * 65536u > instanceCount * 16384u)
		{
			return Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(16384u) > vehicleCount;
		}
		return Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(65536u) > instanceCount;
	}

	private TransferManager.TransferReason GetShoppingReason()
	{
		switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8u))
		{
		case 0:
			return TransferManager.TransferReason.Shopping;
		case 1:
			return TransferManager.TransferReason.ShoppingB;
		case 2:
			return TransferManager.TransferReason.ShoppingC;
		case 3:
			return TransferManager.TransferReason.ShoppingD;
		case 4:
			return TransferManager.TransferReason.ShoppingE;
		case 5:
			return TransferManager.TransferReason.ShoppingF;
		case 6:
			return TransferManager.TransferReason.ShoppingG;
		case 7:
			return TransferManager.TransferReason.ShoppingH;
		default:
			return TransferManager.TransferReason.Shopping;
		}
	}

	private TransferManager.TransferReason GetEntertainmentReason()
	{
		switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
		{
		case 0:
			return TransferManager.TransferReason.Entertainment;
		case 1:
			return TransferManager.TransferReason.EntertainmentB;
		case 2:
			return TransferManager.TransferReason.EntertainmentC;
		case 3:
			return TransferManager.TransferReason.EntertainmentD;
		default:
			return TransferManager.TransferReason.Entertainment;
		}
	}

	private TransferManager.TransferReason GetEvacuationReason(ushort sourceBuilding)
	{
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)sourceBuilding].m_position);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.VIPArea) != DistrictPolicies.CityPlanning.None)
			{
				switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
				{
				case 0:
					return TransferManager.TransferReason.EvacuateVipA;
				case 1:
					return TransferManager.TransferReason.EvacuateVipB;
				case 2:
					return TransferManager.TransferReason.EvacuateVipC;
				case 3:
					return TransferManager.TransferReason.EvacuateVipD;
				default:
					return TransferManager.TransferReason.EvacuateVipA;
				}
			}
		}
		switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
		{
		case 0:
			return TransferManager.TransferReason.EvacuateA;
		case 1:
			return TransferManager.TransferReason.EvacuateB;
		case 2:
			return TransferManager.TransferReason.EvacuateC;
		case 3:
			return TransferManager.TransferReason.EvacuateD;
		default:
			return TransferManager.TransferReason.EvacuateA;
		}
	}

	private void UpdateLocation(uint citizenID, ref Citizen data)
	{
		if (data.m_homeBuilding == 0 && data.m_workBuilding == 0 && data.m_visitBuilding == 0 && data.m_instance == 0)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			return;
		}
		switch (data.CurrentLocation)
		{
		case Citizen.Location.Home:
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			return;
		case Citizen.Location.Work:
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			return;
		case Citizen.Location.Visit:
			if (data.Dead || data.Sick || data.m_visitBuilding == 0)
			{
				Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			}
			else if (!data.Collapsed)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				ushort eventIndex = instance.m_buildings.m_buffer[(int)data.m_visitBuilding].m_eventIndex;
				ItemClass.Service service = instance.m_buildings.m_buffer[(int)data.m_visitBuilding].Info.m_class.m_service;
				if (service == ItemClass.Service.Disaster)
				{
					if ((instance.m_buildings.m_buffer[(int)data.m_visitBuilding].m_flags & Building.Flags.Downgrading) != Building.Flags.None)
					{
						int num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u);
						if (num == 0)
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, base.GetLeavingReason(citizenID, ref data));
						}
						else if (num == 1)
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetShoppingReason());
						}
						else
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetEntertainmentReason());
						}
					}
				}
				else if ((instance.m_buildings.m_buffer[(int)data.m_visitBuilding].m_flags & Building.Flags.Evacuating) != Building.Flags.None)
				{
					base.FindEvacuationPlace(citizenID, data.m_visitBuilding, this.GetEvacuationReason(data.m_visitBuilding));
				}
				else if (eventIndex != 0)
				{
					if ((Singleton<EventManager>.get_instance().m_events.m_buffer[(int)eventIndex].m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) == EventData.Flags.None)
					{
						int num2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(5u);
						if (num2 == 0)
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, base.GetLeavingReason(citizenID, ref data));
						}
						else if (num2 == 1)
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetShoppingReason());
						}
						else
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetEntertainmentReason());
						}
					}
					else
					{
						int num3 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10u);
						if (num3 < 5)
						{
							BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_visitBuilding].Info;
							int num4 = -100;
							info.m_buildingAI.ModifyMaterialBuffer(data.m_visitBuilding, ref instance.m_buildings.m_buffer[(int)data.m_visitBuilding], TransferManager.TransferReason.Shopping, ref num4);
							this.AddTouristVisit(citizenID, data.m_visitBuilding);
						}
					}
				}
				else
				{
					int num5 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10u);
					if (num5 == 0)
					{
						base.FindVisitPlace(citizenID, data.m_visitBuilding, base.GetLeavingReason(citizenID, ref data));
					}
					else if (num5 == 1)
					{
						if (data.m_instance != 0 || this.DoRandomMove())
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetShoppingReason());
						}
						else
						{
							BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_visitBuilding].Info;
							int num6 = -100;
							info2.m_buildingAI.ModifyMaterialBuffer(data.m_visitBuilding, ref instance.m_buildings.m_buffer[(int)data.m_visitBuilding], TransferManager.TransferReason.Shopping, ref num6);
							this.AddTouristVisit(citizenID, data.m_visitBuilding);
						}
					}
					else if (num5 <= 4)
					{
						if (data.m_instance != 0 || this.DoRandomMove())
						{
							base.FindVisitPlace(citizenID, data.m_visitBuilding, this.GetEntertainmentReason());
						}
						else
						{
							BuildingInfo info3 = instance.m_buildings.m_buffer[(int)data.m_visitBuilding].Info;
							int num7 = -100;
							info3.m_buildingAI.ModifyMaterialBuffer(data.m_visitBuilding, ref instance.m_buildings.m_buffer[(int)data.m_visitBuilding], TransferManager.TransferReason.Shopping, ref num7);
							this.AddTouristVisit(citizenID, data.m_visitBuilding);
						}
					}
				}
			}
			return;
		case Citizen.Location.Moving:
			if (data.Dead || data.Sick)
			{
				Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			}
			else if (data.m_vehicle == 0 && data.m_instance == 0)
			{
				Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizenID);
			}
			return;
		default:
			return;
		}
	}

	public override void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
		if (sourceBuilding != data.m_sourceBuilding)
		{
			if (data.m_sourceBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveSourceCitizen(instanceID, ref data);
			}
			data.m_sourceBuilding = sourceBuilding;
			if (data.m_sourceBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddSourceCitizen(instanceID, ref data);
			}
		}
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(instanceID);
			Randomizer randomizer;
			randomizer..ctor((int)instanceID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0.m_velocity = Vector3.get_zero();
			data.m_frame0.m_position = vector;
			data.m_frame0.m_rotation = rotation;
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos = new Vector4(vector2.x, vector2.y, vector2.z, 1f);
			Color32 eventCitizenColor = Singleton<EventManager>.get_instance().GetEventCitizenColor(instance.m_buildings.m_buffer[(int)sourceBuilding].m_eventIndex, data.m_citizen);
			if (eventCitizenColor.a == 255)
			{
				data.m_color = eventCitizenColor;
				data.m_flags |= CitizenInstance.Flags.CustomColor;
			}
		}
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
				data.m_targetSeed = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u);
			}
		}
		if (this.IsRoadConnection(targetBuilding) || this.IsRoadConnection(data.m_sourceBuilding))
		{
			data.m_flags |= CitizenInstance.Flags.BorrowCar;
		}
		else
		{
			data.m_flags &= ~CitizenInstance.Flags.BorrowCar;
		}
		if (targetBuilding != 0 && (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			Color32 eventCitizenColor = Singleton<EventManager>.get_instance().GetEventCitizenColor(Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_eventIndex, data.m_citizen);
			if (eventCitizenColor.a == 255)
			{
				data.m_color = eventCitizenColor;
				data.m_flags |= CitizenInstance.Flags.CustomColor;
			}
		}
		if (!this.StartPathFind(instanceID, ref data))
		{
			data.Unspawn(instanceID);
		}
	}

	public override void BuildingRelocated(ushort instanceID, ref CitizenInstance data, ushort building)
	{
		base.BuildingRelocated(instanceID, ref data, building);
		if (building == data.m_targetBuilding)
		{
			base.InvalidPath(instanceID, ref data);
		}
	}

	private bool IsRoadConnection(ushort building)
	{
		if (building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None && instance.m_buildings.m_buffer[(int)building].Info.m_class.m_service == ItemClass.Service.Road)
			{
				return true;
			}
		}
		return false;
	}

	protected override bool SpawnVehicle(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position pathPos)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		float num = 20f;
		int num2 = Mathf.Max((int)((citizenData.m_targetPos.x - num) / 32f + 270f), 0);
		int num3 = Mathf.Max((int)((citizenData.m_targetPos.z - num) / 32f + 270f), 0);
		int num4 = Mathf.Min((int)((citizenData.m_targetPos.x + num) / 32f + 270f), 539);
		int num5 = Mathf.Min((int)((citizenData.m_targetPos.z + num) / 32f + 270f), 539);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = instance.m_vehicleGrid[i * 540 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					if (this.TryJoinVehicle(instanceID, ref citizenData, num6, ref instance.m_vehicles.m_buffer[(int)num6]))
					{
						citizenData.m_flags |= CitizenInstance.Flags.EnteringVehicle;
						citizenData.m_flags &= ~CitizenInstance.Flags.TryingSpawnVehicle;
						citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
						citizenData.m_waitCounter = 0;
						return true;
					}
					num6 = instance.m_vehicles.m_buffer[(int)num6].m_nextGridVehicle;
					if (++num7 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		NetManager instance2 = Singleton<NetManager>.get_instance();
		CitizenManager instance3 = Singleton<CitizenManager>.get_instance();
		Vector3 vector = Vector3.get_zero();
		Quaternion rotation = Quaternion.get_identity();
		ushort num8 = instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_parkedVehicle;
		if (num8 != 0)
		{
			vector = instance.m_parkedVehicles.m_buffer[(int)num8].m_position;
			rotation = instance.m_parkedVehicles.m_buffer[(int)num8].m_rotation;
		}
		VehicleInfo vehicleInfo = this.GetVehicleInfo(instanceID, ref citizenData, false);
		if (vehicleInfo == null || vehicleInfo.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
		{
			instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetParkedVehicle(citizenData.m_citizen, 0);
			if ((citizenData.m_flags & CitizenInstance.Flags.TryingSpawnVehicle) == CitizenInstance.Flags.None)
			{
				citizenData.m_flags |= CitizenInstance.Flags.TryingSpawnVehicle;
				citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
				citizenData.m_waitCounter = 0;
			}
			return true;
		}
		if (vehicleInfo.m_class.m_subService == ItemClass.SubService.PublicTransportTaxi)
		{
			instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetParkedVehicle(citizenData.m_citizen, 0);
			if ((citizenData.m_flags & CitizenInstance.Flags.WaitingTaxi) == CitizenInstance.Flags.None)
			{
				citizenData.m_flags |= CitizenInstance.Flags.WaitingTaxi;
				citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
				citizenData.m_waitCounter = 0;
			}
			return true;
		}
		uint laneID = PathManager.GetLaneID(pathPos);
		Vector3 vector2 = citizenData.m_targetPos;
		if (num8 != 0 && Vector3.SqrMagnitude(vector - vector2) < 1024f)
		{
			vector2 = vector;
		}
		else
		{
			num8 = 0;
		}
		Vector3 vector3;
		float num9;
		instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetClosestPosition(vector2, out vector3, out num9);
		byte lastPathOffset = (byte)Mathf.Clamp(Mathf.RoundToInt(num9 * 255f), 0, 255);
		vector3 = vector2 + Vector3.ClampMagnitude(vector3 - vector2, 5f);
		ushort num10;
		if (instance.CreateVehicle(out num10, ref Singleton<SimulationManager>.get_instance().m_randomizer, vehicleInfo, vector2, TransferManager.TransferReason.None, false, false))
		{
			Vehicle.Frame frame = instance.m_vehicles.m_buffer[(int)num10].m_frame0;
			if (num8 != 0)
			{
				frame.m_rotation = rotation;
			}
			else
			{
				Vector3 vector4 = vector3 - citizenData.GetLastFrameData().m_position;
				if (vector4.get_sqrMagnitude() > 0.01f)
				{
					frame.m_rotation = Quaternion.LookRotation(vector4);
				}
			}
			instance.m_vehicles.m_buffer[(int)num10].m_frame0 = frame;
			instance.m_vehicles.m_buffer[(int)num10].m_frame1 = frame;
			instance.m_vehicles.m_buffer[(int)num10].m_frame2 = frame;
			instance.m_vehicles.m_buffer[(int)num10].m_frame3 = frame;
			vehicleInfo.m_vehicleAI.FrameDataUpdated(num10, ref instance.m_vehicles.m_buffer[(int)num10], ref frame);
			instance.m_vehicles.m_buffer[(int)num10].m_targetPos0 = new Vector4(vector3.x, vector3.y, vector3.z, 2f);
			Vehicle[] expr_4E5_cp_0 = instance.m_vehicles.m_buffer;
			ushort expr_4E5_cp_1 = num10;
			expr_4E5_cp_0[(int)expr_4E5_cp_1].m_flags = (expr_4E5_cp_0[(int)expr_4E5_cp_1].m_flags | Vehicle.Flags.Stopped);
			instance.m_vehicles.m_buffer[(int)num10].m_path = citizenData.m_path;
			instance.m_vehicles.m_buffer[(int)num10].m_pathPositionIndex = citizenData.m_pathPositionIndex;
			instance.m_vehicles.m_buffer[(int)num10].m_lastPathOffset = lastPathOffset;
			instance.m_vehicles.m_buffer[(int)num10].m_transferSize = (ushort)(citizenData.m_citizen & 65535u);
			vehicleInfo.m_vehicleAI.TrySpawn(num10, ref instance.m_vehicles.m_buffer[(int)num10]);
			if (num8 != 0)
			{
				InstanceID empty = InstanceID.Empty;
				empty.ParkedVehicle = num8;
				InstanceID empty2 = InstanceID.Empty;
				empty2.Vehicle = num10;
				Singleton<InstanceManager>.get_instance().ChangeInstance(empty, empty2);
			}
			citizenData.m_path = 0u;
			instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetParkedVehicle(citizenData.m_citizen, 0);
			instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, num10, 0u);
			citizenData.m_flags |= CitizenInstance.Flags.EnteringVehicle;
			citizenData.m_flags &= ~CitizenInstance.Flags.TryingSpawnVehicle;
			citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
			citizenData.m_waitCounter = 0;
			return true;
		}
		instance3.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetParkedVehicle(citizenData.m_citizen, 0);
		if ((citizenData.m_flags & CitizenInstance.Flags.TryingSpawnVehicle) == CitizenInstance.Flags.None)
		{
			citizenData.m_flags |= CitizenInstance.Flags.TryingSpawnVehicle;
			citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
			citizenData.m_waitCounter = 0;
		}
		return true;
	}

	protected override bool SpawnBicycle(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position pathPos)
	{
		VehicleInfo vehicleInfo = this.GetVehicleInfo(instanceID, ref citizenData, false);
		if (vehicleInfo != null && vehicleInfo.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			CitizenInstance.Frame lastFrameData = citizenData.GetLastFrameData();
			ushort num;
			if (instance2.CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, vehicleInfo, lastFrameData.m_position, TransferManager.TransferReason.None, false, false))
			{
				Vehicle.Frame frame = instance2.m_vehicles.m_buffer[(int)num].m_frame0;
				frame.m_rotation = lastFrameData.m_rotation;
				instance2.m_vehicles.m_buffer[(int)num].m_frame0 = frame;
				instance2.m_vehicles.m_buffer[(int)num].m_frame1 = frame;
				instance2.m_vehicles.m_buffer[(int)num].m_frame2 = frame;
				instance2.m_vehicles.m_buffer[(int)num].m_frame3 = frame;
				vehicleInfo.m_vehicleAI.FrameDataUpdated(num, ref instance2.m_vehicles.m_buffer[(int)num], ref frame);
				vehicleInfo.m_vehicleAI.TrySpawn(num, ref instance2.m_vehicles.m_buffer[(int)num]);
				instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetParkedVehicle(citizenData.m_citizen, 0);
				instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, num, 0u);
				citizenData.m_flags |= CitizenInstance.Flags.RidingBicycle;
				return true;
			}
		}
		return false;
	}

	private bool TryJoinVehicle(ushort instanceID, ref CitizenInstance citizenData, ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0)
		{
			return false;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = vehicleData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0 && instance.m_instances.m_buffer[(int)instance2].m_targetBuilding == citizenData.m_targetBuilding)
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, vehicleID, 0u);
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_vehicle == vehicleID)
						{
							if (citizenData.m_path != 0u)
							{
								Singleton<PathManager>.get_instance().ReleasePath(citizenData.m_path);
								citizenData.m_path = 0u;
							}
							return true;
						}
					}
					break;
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return false;
	}

	protected override void SwitchBuildingTargetPos(ushort instanceID, ref CitizenInstance citizenData)
	{
		if (citizenData.m_path == 0u && citizenData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			if (info.m_hasPedestrianPaths)
			{
				Randomizer randomizer;
				randomizer..ctor((int)instanceID << 8 | (int)citizenData.m_targetSeed);
				Vector3 vector;
				Vector3 vector2;
				Vector2 vector3;
				CitizenInstance.Flags flags;
				info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref randomizer, this.m_info, instanceID, out vector, out vector2, out vector3, out flags);
				float num = Vector3.Distance(citizenData.m_targetPos, vector2);
				if (num > 10f)
				{
					base.StartPathFind(instanceID, ref citizenData, citizenData.m_targetPos, vector2, null);
				}
			}
		}
	}

	protected override bool StartPathFind(ushort instanceID, ref CitizenInstance citizenData)
	{
		if (citizenData.m_citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort vehicle = instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_vehicle;
			if (vehicle != 0)
			{
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)vehicle].Info;
				if (info != null)
				{
					uint citizen = info.m_vehicleAI.GetOwnerID(vehicle, ref instance2.m_vehicles.m_buffer[(int)vehicle]).Citizen;
					if (citizen == citizenData.m_citizen)
					{
						info.m_vehicleAI.SetTarget(vehicle, ref instance2.m_vehicles.m_buffer[(int)vehicle], 0);
						return false;
					}
				}
				instance.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, 0, 0u);
				return false;
			}
		}
		if (citizenData.m_targetBuilding != 0)
		{
			VehicleInfo vehicleInfo = this.GetVehicleInfo(instanceID, ref citizenData, false);
			BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance3.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)instanceID << 8 | (int)citizenData.m_targetSeed);
			Vector3 vector;
			Vector3 endPos;
			Vector2 vector2;
			CitizenInstance.Flags flags;
			info2.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance3.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref randomizer, this.m_info, instanceID, out vector, out endPos, out vector2, out flags);
			return base.StartPathFind(instanceID, ref citizenData, citizenData.m_targetPos, endPos, vehicleInfo);
		}
		return false;
	}

	protected override VehicleInfo GetVehicleInfo(ushort instanceID, ref CitizenInstance citizenData, bool forceProbability)
	{
		if (citizenData.m_citizen == 0u)
		{
			return null;
		}
		int num;
		int num2;
		if (forceProbability || (citizenData.m_flags & CitizenInstance.Flags.BorrowCar) != CitizenInstance.Flags.None)
		{
			num = 100;
			num2 = 0;
		}
		else
		{
			num = this.GetCarProbability();
			num2 = this.GetBikeProbability();
		}
		Randomizer randomizer;
		randomizer..ctor(citizenData.m_citizen);
		bool flag = randomizer.Int32(100u) < num;
		bool flag2 = randomizer.Int32(100u) < num2;
		bool flag3;
		bool flag4;
		if (flag)
		{
			Citizen.Wealth wealthLevel = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].WealthLevel;
			int electricCarProbability = this.GetElectricCarProbability(wealthLevel);
			flag3 = false;
			flag4 = (randomizer.Int32(100u) < electricCarProbability);
		}
		else
		{
			int taxiProbability = this.GetTaxiProbability();
			flag3 = (randomizer.Int32(100u) < taxiProbability);
			flag4 = false;
		}
		ItemClass.Service service = ItemClass.Service.Residential;
		ItemClass.SubService subService = (!flag4) ? ItemClass.SubService.ResidentialLow : ItemClass.SubService.ResidentialLowEco;
		if (!flag && flag3)
		{
			service = ItemClass.Service.PublicTransport;
			subService = ItemClass.SubService.PublicTransportTaxi;
		}
		VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref randomizer, service, subService, ItemClass.Level.Level1);
		VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref randomizer, ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.Level2);
		if (flag2 && randomVehicleInfo2 != null)
		{
			return randomVehicleInfo2;
		}
		if ((flag || flag3) && randomVehicleInfo != null)
		{
			return randomVehicleInfo;
		}
		return null;
	}

	protected override void ArriveAtDestination(ushort instanceID, ref CitizenInstance citizenData, bool success)
	{
		if (success && citizenData.m_citizen != 0u && citizenData.m_targetBuilding != 0)
		{
			this.AddTouristVisit(citizenData.m_citizen, citizenData.m_targetBuilding);
		}
		base.ArriveAtDestination(instanceID, ref citizenData, success);
	}

	private void AddTouristVisit(uint citizenID, ushort buildingID)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		if ((instance.m_citizens.m_buffer[(int)((UIntPtr)citizenID)].m_flags & Citizen.Flags.DummyTraffic) == Citizen.Flags.None)
		{
			Citizen.Wealth wealthLevel = instance.m_citizens.m_buffer[(int)((UIntPtr)citizenID)].WealthLevel;
			byte district = instance2.GetDistrict(Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)buildingID].m_position);
			if (wealthLevel != Citizen.Wealth.Low)
			{
				if (wealthLevel != Citizen.Wealth.Medium)
				{
					if (wealthLevel == Citizen.Wealth.High)
					{
						District[] expr_EF_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
						byte expr_EF_cp_0_cp_1 = district;
						expr_EF_cp_0_cp_0[(int)expr_EF_cp_0_cp_1].m_tourist3Data.m_tempCount = expr_EF_cp_0_cp_0[(int)expr_EF_cp_0_cp_1].m_tourist3Data.m_tempCount + 1u;
					}
				}
				else
				{
					District[] expr_C3_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
					byte expr_C3_cp_0_cp_1 = district;
					expr_C3_cp_0_cp_0[(int)expr_C3_cp_0_cp_1].m_tourist2Data.m_tempCount = expr_C3_cp_0_cp_0[(int)expr_C3_cp_0_cp_1].m_tourist2Data.m_tempCount + 1u;
				}
			}
			else
			{
				District[] expr_97_cp_0_cp_0 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer;
				byte expr_97_cp_0_cp_1 = district;
				expr_97_cp_0_cp_0[(int)expr_97_cp_0_cp_1].m_tourist1Data.m_tempCount = expr_97_cp_0_cp_0[(int)expr_97_cp_0_cp_1].m_tourist1Data.m_tempCount + 1u;
			}
		}
	}

	private int GetCarProbability()
	{
		return 20;
	}

	private int GetBikeProbability()
	{
		return 20;
	}

	private int GetTaxiProbability()
	{
		return 20;
	}

	private int GetElectricCarProbability(Citizen.Wealth wealth)
	{
		switch (wealth)
		{
		case Citizen.Wealth.Low:
			return 10;
		case Citizen.Wealth.Medium:
			return 15;
		case Citizen.Wealth.High:
			return 20;
		default:
			return 0;
		}
	}

	public const int CAR_PROBABILITY = 20;

	public const int BIKE_PROBABILITY = 20;

	public const int TAXI_PROBABILITY = 20;

	public const int ELECTRIC_CAR_PROBABILITY_LOW = 10;

	public const int ELECTRIC_CAR_PROBABILITY_MEDIUM = 15;

	public const int ELECTRIC_CAR_PROBABILITY_HIGH = 20;
}
