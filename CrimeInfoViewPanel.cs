﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class CrimeInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_SafetyMeter = base.Find<UISlider>("SafetyMeter");
		this.m_SafetyLabel = base.Find<UILabel>("SafetyLabel");
		this.m_JailMeter = base.Find<UISlider>("JailMeter");
		this.m_Prisoners = base.Find<UILabel>("Prisoners");
		this.m_JailCapacity = base.Find<UILabel>("JailCapacity");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("RateGradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("CoverageGradient");
				UITextureSprite uITextureSprite3 = base.Find<UITextureSprite>("SafetyGradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[3].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[3].m_inactiveColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[3].m_targetColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[3].m_negativeColor);
					uITextureSprite3.get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
			}
			this.m_initialized = true;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		if (Singleton<DistrictManager>.get_exists())
		{
			num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetCriminalCapacity();
			num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetCriminalAmount();
			num3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetExtraCriminals();
		}
		this.m_JailMeter.set_value((float)base.GetPercentage(num, num2 + num3));
		this.m_Prisoners.set_text(StringUtils.SafeFormat(Locale.Get("INFO_CRIME_PRISONERS"), num2));
		this.m_JailCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_CRIME_JAILCAPACITY"), num));
		int num4 = 0;
		if (Singleton<DistrictManager>.get_exists())
		{
			num4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_finalCrimeRate;
		}
		this.m_SafetyLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_CRIMERATE_METER"),
			" - ",
			num4,
			"%"
		}));
		this.m_SafetyMeter.set_value((float)num4);
	}

	private UISlider m_SafetyMeter;

	private UILabel m_SafetyLabel;

	private UISlider m_JailMeter;

	private UILabel m_Prisoners;

	private UILabel m_JailCapacity;

	private bool m_initialized;
}
