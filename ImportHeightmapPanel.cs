﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class ImportHeightmapPanel : HeightmapPanel
{
	public void OpenFolder()
	{
		Utils.OpenInFileBrowser(HeightmapPanel.heightMapsPath);
	}

	private void OnDestroy()
	{
		for (int i = 0; i < ImportHeightmapPanel.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.m_FileList.Focus();
		}
	}

	private void OnFileListVisibilityChanged(UIComponent comp, bool visible)
	{
		if (comp == this.m_FileList)
		{
			if (visible)
			{
				this.Refresh();
				for (int i = 0; i < ImportHeightmapPanel.m_Extensions.Length; i++)
				{
					if (this.m_FileSystemReporter[i] != null)
					{
						this.m_FileSystemReporter[i].Start();
					}
				}
			}
			else
			{
				for (int j = 0; j < ImportHeightmapPanel.m_Extensions.Length; j++)
				{
					if (this.m_FileSystemReporter[j] != null)
					{
						this.m_FileSystemReporter[j].Stop();
					}
				}
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.Refresh();
		});
	}

	private void Awake()
	{
		this.m_InfoLabel = base.Find<UILabel>("BitsLabel");
		this.m_FileList = base.Find<UIListBox>("FileList");
		this.m_ApplyButton = base.Find<UIButton>("Apply");
		this.m_PreviewSprite = base.Find<UITextureSprite>("Preview");
		this.m_ErrorLabel = base.Find<UILabel>("ErrorLabel");
		this.m_LoadingLabel = base.Find<UILabel>("LoadingLabel");
		this.m_LoadingProgress = this.m_LoadingLabel.Find<UISprite>("ProgressSprite").get_transform();
		this.m_FileList.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnFileListVisibilityChanged));
	}

	private void Start()
	{
		base.get_component().Hide();
		this.m_ErrorLabel.set_isVisible(false);
		this.m_InfoLabel.Hide();
		this.m_LoadingLabel.Hide();
		this.m_ApplyButton.set_isEnabled(false);
		this.Refresh();
		for (int i = 0; i < ImportHeightmapPanel.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + ImportHeightmapPanel.m_Extensions[i], HeightmapPanel.heightMapsPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
		}
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	private void Refresh()
	{
		base.get_component().Focus();
		List<string> list = new List<string>();
		DirectoryInfo directoryInfo = new DirectoryInfo(HeightmapPanel.heightMapsPath);
		if (directoryInfo.Exists)
		{
			FileInfo[] array = null;
			try
			{
				array = directoryInfo.GetFiles();
				base.Sort(array, HeightmapPanel.SortingType.Timestamp);
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			if (array != null)
			{
				FileInfo[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					FileInfo fileInfo = array2[i];
					for (int j = 0; j < ImportHeightmapPanel.m_Extensions.Length; j++)
					{
						if (string.Compare(Path.GetExtension(fileInfo.Name), ImportHeightmapPanel.m_Extensions[j]) == 0)
						{
							list.Add(fileInfo.Name);
						}
					}
				}
			}
			this.m_FileList.set_items(list.ToArray());
			if (list.Count > 0)
			{
				this.m_FileList.set_selectedIndex(0);
				this.m_ApplyButton.set_isEnabled(true);
			}
			else
			{
				this.m_ApplyButton.set_isEnabled(false);
			}
		}
	}

	private void AssignPreview(Image image)
	{
		if (this.m_PreviewSprite.get_texture() != null)
		{
			Object.DestroyImmediate(this.m_PreviewSprite.get_texture());
		}
		this.m_PreviewSprite.set_texture(image.CreateTexture());
		this.m_PreviewSprite.get_texture().set_wrapMode(1);
		ValueAnimator.Animate("LoadingHeightmap", delegate(float val)
		{
			this.m_PreviewSprite.set_opacity(val);
			this.m_InfoLabel.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f, 7));
		if (this.m_LastHeightmap8 != null || this.m_LastHeightmap16 != null)
		{
			this.m_ApplyButton.set_isEnabled(true);
			this.m_InfoLabel.Show();
			string text = string.Empty;
			if (this.m_LastHeightmap8 != null)
			{
				text += "8-bits map";
			}
			else if (this.m_LastHeightmap16 != null)
			{
				text += "16-bits map";
			}
			if (this.m_Rescaled)
			{
				text += "\nRescaled";
			}
			this.m_InfoLabel.set_text(text);
		}
		this.m_LoadingLabel.Hide();
	}

	private void LoadImageThreaded(string path, uint flags)
	{
		Stopwatch stopwatch = new Stopwatch();
		try
		{
			stopwatch.Start();
			Image image = new Image(path, flags, 164);
			if (image.get_format() >= Image.kFormatAlpha16)
			{
				image.Convert(Image.kFormatAlpha16);
				if (image.get_width() != 1081 || image.get_height() != 1081)
				{
					if (!image.Resize(1081, 1081))
					{
						throw new Exception(string.Concat(new object[]
						{
							"Resize not supported: ",
							image.get_format(),
							"-",
							image.get_width(),
							"x",
							image.get_height(),
							" Expected: ",
							1081,
							"x",
							1081
						}));
					}
					this.m_Rescaled = true;
				}
				this.m_LastHeightmap16 = image.GetPixels();
				image.Convert(3);
			}
			else
			{
				image.Convert(1);
				if (image.get_width() != 1081 || image.get_height() != 1081)
				{
					if (!image.Resize(1081, 1081))
					{
						throw new Exception(string.Concat(new object[]
						{
							"Resize not supported: ",
							image.get_format(),
							"-",
							image.get_width(),
							"x",
							image.get_height(),
							" Expected: ",
							1081,
							"x",
							1081
						}));
					}
					this.m_Rescaled = true;
				}
				this.m_LastHeightmap8 = image.GetPixels();
				image.Convert(3);
			}
			stopwatch.Stop();
			if (stopwatch.ElapsedMilliseconds < 200L)
			{
				Thread.Sleep(200 - (int)stopwatch.ElapsedMilliseconds);
			}
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.AssignPreview(image);
			});
		}
		catch (Exception ex)
		{
			stopwatch.Stop();
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				"[",
				path,
				"]\n",
				Environment.StackTrace
			}));
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.ShowError(path);
			});
		}
	}

	private void ShowError(string path)
	{
		this.m_LoadingLabel.Hide();
		this.m_ErrorLabel.Show();
		this.m_ErrorLabel.set_text(StringUtils.SafeFormat(Locale.Get("HEIGHTMAP_LOADERROR"), Path.GetFileName(path)));
		ValueAnimator.Animate("ErrorLoadingHeightmap", delegate(float val)
		{
			this.m_ErrorLabel.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f, 7));
	}

	public void LoadSelected()
	{
		this.m_LastHeightmap8 = null;
		this.m_LastHeightmap16 = null;
		this.m_Rescaled = false;
		this.m_ApplyButton.set_isEnabled(false);
		ValueAnimator.Animate("ErrorLoadingHeightmap", delegate(float val)
		{
			this.m_ErrorLabel.set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.2f, 7), delegate
		{
			this.m_ErrorLabel.Hide();
		});
		ValueAnimator.Animate("LoadingHeightmap", delegate(float val)
		{
			this.m_PreviewSprite.set_opacity(val);
			this.m_InfoLabel.set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.2f, 7), delegate
		{
			this.m_PreviewSprite.set_texture(null);
			this.m_InfoLabel.Hide();
		});
		if (this.m_FileList.get_selectedIndex() >= 0 && this.m_FileList.get_selectedIndex() < this.m_FileList.get_items().Length)
		{
			string path = Path.Combine(HeightmapPanel.heightMapsPath, this.m_FileList.get_items()[this.m_FileList.get_selectedIndex()]);
			this.m_LoadingLabel.Show();
			ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				this.LoadImageThreaded(path, 1081u);
			});
		}
	}

	public void OnHeightmapSelectionChanged(UIComponent comp, int index)
	{
		if (base.get_component().get_isVisible())
		{
			this.LoadSelected();
		}
	}

	private void Update()
	{
		if (this.m_LoadingLabel.get_isVisible())
		{
			this.m_LoadingProgress.Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
		}
	}

	public void OnApply()
	{
		if (Singleton<SimulationManager>.get_exists())
		{
			ToolsModifierControl.GetTool<TerrainTool>().ResetUndoBuffer();
			if (this.m_LastHeightmap8 != null)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.LoadHeightMap8(this.m_LastHeightmap8, 65536));
			}
			else if (this.m_LastHeightmap16 != null)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.LoadHeightMap16(this.m_LastHeightmap16));
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator LoadHeightMap8(byte[] heightmap, int scale)
	{
		ImportHeightmapPanel.<LoadHeightMap8>c__Iterator0 <LoadHeightMap8>c__Iterator = new ImportHeightmapPanel.<LoadHeightMap8>c__Iterator0();
		<LoadHeightMap8>c__Iterator.heightmap = heightmap;
		<LoadHeightMap8>c__Iterator.scale = scale;
		return <LoadHeightMap8>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoadHeightMap16(byte[] heightmap)
	{
		ImportHeightmapPanel.<LoadHeightMap16>c__Iterator1 <LoadHeightMap16>c__Iterator = new ImportHeightmapPanel.<LoadHeightMap16>c__Iterator1();
		<LoadHeightMap16>c__Iterator.heightmap = heightmap;
		return <LoadHeightMap16>c__Iterator;
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private bool m_Rescaled;

	private byte[] m_LastHeightmap8;

	private byte[] m_LastHeightmap16;

	private static readonly string[] m_Extensions = Image.GetExtensions(164);

	public float m_RotationSpeed = 280f;

	private UILabel m_LoadingLabel;

	private UIListBox m_FileList;

	private UIButton m_ApplyButton;

	private UITextureSprite m_PreviewSprite;

	private UILabel m_ErrorLabel;

	private UILabel m_InfoLabel;

	private Transform m_LoadingProgress;

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[ImportHeightmapPanel.m_Extensions.Length];
}
