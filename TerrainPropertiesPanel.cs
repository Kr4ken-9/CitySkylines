﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class TerrainPropertiesPanel : UICustomControl
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.UnBindActions();
		this.m_DiffuseGrass.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_grassDiffuse);
		this.m_DiffuseRuined.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_ruinedDiffuse);
		this.m_DiffusePavement.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_pavementDiffuse);
		this.m_DiffuseGravel.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_gravelDiffuse);
		this.m_DiffuseCliff.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_cliffDiffuse);
		this.m_DiffuseSand.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_sandDiffuse);
		this.m_DiffuseOil.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_oilDiffuse);
		this.m_DiffuseOre.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_oreDiffuse);
		this.m_NormalSand.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal);
		this.m_NormalCliff.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal);
		this.m_DiffuseTilingGrass.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling));
		this.m_DiffuseTilingRuined.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling));
		this.m_DiffuseTilingPavement.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling));
		this.m_DiffuseTilingGravel.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling));
		this.m_DiffuseTilingCliff.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling));
		this.m_DiffuseTilingSand.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling));
		this.m_DiffuseTilingOil.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling));
		this.m_DiffuseTilingOre.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling));
		this.m_NormalTilingCliffSand.set_value(ThemeEditorMainToolbar.TilingNonlinearize(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling));
		this.m_DiffuseTilingGrassValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingRuinedValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingPavementValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingGravelValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingCliffValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingSandValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingOilValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling.ToString(this.m_Tilingformat));
		this.m_DiffuseTilingOreValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling.ToString(this.m_Tilingformat));
		this.m_NormalTilingCliffSandValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling.ToString(this.m_Tilingformat));
		this.m_GrassGeometryEnabled.set_isChecked(Singleton<TerrainManager>.get_instance().m_properties.m_useGrassDecorations);
		this.m_FertileGeometryEnabled.set_isChecked(Singleton<TerrainManager>.get_instance().m_properties.m_useFertileDecorations);
		this.m_RocksGeometryEnabled.set_isChecked(Singleton<TerrainManager>.get_instance().m_properties.m_useCliffDecorations);
		this.m_GrassOffsetPollutionRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.x.ToString(this.m_Offsetformat));
		this.m_GrassOffsetPollutionGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.y.ToString(this.m_Offsetformat));
		this.m_GrassOffsetPollutionBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.z.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFieldRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.x.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFieldGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.y.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFieldBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.z.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFertilityRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.x.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFertilityGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.y.ToString(this.m_Offsetformat));
		this.m_GrassOffsetFertilityBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.z.ToString(this.m_Offsetformat));
		this.m_GrassOffsetForestRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.x.ToString(this.m_Offsetformat));
		this.m_GrassOffsetForestGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.y.ToString(this.m_Offsetformat));
		this.m_GrassOffsetForestBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.z.ToString(this.m_Offsetformat));
		this.m_Defaults.Store<Texture>("grassTex", this.m_DiffuseGrass.get_texture());
		this.m_Defaults.Store<Texture>("ruinedTex", this.m_DiffuseRuined.get_texture());
		this.m_Defaults.Store<Texture>("pavementTex", this.m_DiffusePavement.get_texture());
		this.m_Defaults.Store<Texture>("gravelTex", this.m_DiffuseGravel.get_texture());
		this.m_Defaults.Store<Texture>("cliffTex", this.m_DiffuseCliff.get_texture());
		this.m_Defaults.Store<Texture>("sandTex", this.m_DiffuseSand.get_texture());
		this.m_Defaults.Store<Texture>("oilTex", this.m_DiffuseOil.get_texture());
		this.m_Defaults.Store<Texture>("oreTex", this.m_DiffuseOre.get_texture());
		this.m_Defaults.Store<Texture>("normalcliffsandTex", this.m_NormalSand.get_texture());
		this.m_Defaults.Store<float>("grassTiling", Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling);
		this.m_Defaults.Store<float>("ruinedTiling", Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling);
		this.m_Defaults.Store<float>("pavementTiling", Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling);
		this.m_Defaults.Store<float>("gravelTiling", Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling);
		this.m_Defaults.Store<float>("cliffTiling", Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling);
		this.m_Defaults.Store<float>("sandTiling", Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling);
		this.m_Defaults.Store<float>("oilTiling", Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling);
		this.m_Defaults.Store<float>("oreTiling", Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling);
		this.m_Defaults.Store<float>("cliffsandTiling", Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling);
		this.m_Defaults.Store<bool>("grassGeometryEnabled", this.m_GrassGeometryEnabled.get_isChecked());
		this.m_Defaults.Store<bool>("fertileGeometryEnabled", this.m_FertileGeometryEnabled.get_isChecked());
		this.m_Defaults.Store<bool>("rocksGeometryEnabled", this.m_RocksGeometryEnabled.get_isChecked());
		this.m_Defaults.Store<Vector3>("grassOffsetPollution", Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset);
		this.m_Defaults.Store<Vector3>("grassOffsetField", Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset);
		this.m_Defaults.Store<Vector3>("grassOffsetFertility", Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset);
		this.m_Defaults.Store<Vector3>("grassOffsetForest", Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset);
		this.BindActions();
	}

	public void ResetAll()
	{
		this.ResetDiffuseTextures(null, null);
		this.ResetDiffuseTiling(null, null);
		this.ResetNormalTextures(null, null);
		this.ResetNormalTiling(null, null);
		this.ResetGeometry(null, null);
		this.ResetOffsets(null, null);
	}

	public void ResetDiffuseTextures(UIComponent c, UIMouseEventParameter p)
	{
		this.m_DiffuseGrass.set_texture(this.m_Defaults.Get<Texture>("grassTex"));
		this.m_DiffuseRuined.set_texture(this.m_Defaults.Get<Texture>("ruinedTex"));
		this.m_DiffusePavement.set_texture(this.m_Defaults.Get<Texture>("pavementTex"));
		this.m_DiffuseGravel.set_texture(this.m_Defaults.Get<Texture>("gravelTex"));
		this.m_DiffuseCliff.set_texture(this.m_Defaults.Get<Texture>("cliffTex"));
		this.m_DiffuseSand.set_texture(this.m_Defaults.Get<Texture>("sandTex"));
		this.m_DiffuseOil.set_texture(this.m_Defaults.Get<Texture>("oilTex"));
		this.m_DiffuseOre.set_texture(this.m_Defaults.Get<Texture>("oreTex"));
	}

	public void ResetDiffuseTiling(UIComponent c, UIMouseEventParameter p)
	{
		this.OnAssignTilingGrassValue(c, this.m_Defaults.Get<float>("grassTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingRuinedValue(c, this.m_Defaults.Get<float>("ruinedTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingPavementValue(c, this.m_Defaults.Get<float>("pavementTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingGravelValue(c, this.m_Defaults.Get<float>("gravelTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingCliffValue(c, this.m_Defaults.Get<float>("cliffTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingSandValue(c, this.m_Defaults.Get<float>("sandTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingOilValue(c, this.m_Defaults.Get<float>("oilTiling").ToString(this.m_Tilingformat));
		this.OnAssignTilingOreValue(c, this.m_Defaults.Get<float>("oreTiling").ToString(this.m_Tilingformat));
	}

	public void ResetNormalTextures(UIComponent c, UIMouseEventParameter p)
	{
		Texture2D texture2D = this.m_Defaults.Get<Texture2D>("normalcliffsandTex");
		if (texture2D != null)
		{
			this.m_NormalCliff.set_texture(texture2D);
			this.m_NormalSand.set_texture(texture2D);
			Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal = texture2D;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
		}
	}

	public void ResetNormalTiling(UIComponent c, UIMouseEventParameter p)
	{
		this.m_NormalTilingCliffSand.set_value(ThemeEditorMainToolbar.TilingNonlinearize(this.m_Defaults.Get<float>("cliffsandTiling")));
	}

	public void ResetGeometry(UIComponent c, UIMouseEventParameter p)
	{
		this.m_GrassGeometryEnabled.set_isChecked(this.m_Defaults.Get<bool>("grassGeometryEnabled"));
		this.m_FertileGeometryEnabled.set_isChecked(this.m_Defaults.Get<bool>("fertileGeometryEnabled"));
		this.m_RocksGeometryEnabled.set_isChecked(this.m_Defaults.Get<bool>("rocksGeometryEnabled"));
	}

	public void ResetOffsets(UIComponent c, UIMouseEventParameter p)
	{
		Vector3 vector = this.m_Defaults.Get<Vector3>("grassOffsetPollution");
		this.OnGrassOffsetPollutionRedChanged(this.m_GrassOffsetPollutionRed, vector.x.ToString(this.m_Offsetformat));
		this.OnGrassOffsetPollutionGreenChanged(this.m_GrassOffsetPollutionGreen, vector.y.ToString(this.m_Offsetformat));
		this.OnGrassOffsetPollutionBlueChanged(this.m_GrassOffsetPollutionBlue, vector.z.ToString(this.m_Offsetformat));
		vector = this.m_Defaults.Get<Vector3>("grassOffsetField");
		this.OnGrassOffsetFieldRedChanged(this.m_GrassOffsetPollutionRed, vector.x.ToString(this.m_Offsetformat));
		this.OnGrassOffsetFieldGreenChanged(this.m_GrassOffsetPollutionGreen, vector.y.ToString(this.m_Offsetformat));
		this.OnGrassOffsetFieldBlueChanged(this.m_GrassOffsetPollutionBlue, vector.z.ToString(this.m_Offsetformat));
		vector = this.m_Defaults.Get<Vector3>("grassOffsetFertility");
		this.OnGrassOffsetFertilityRedChanged(this.m_GrassOffsetPollutionRed, vector.x.ToString(this.m_Offsetformat));
		this.OnGrassOffsetFertilityGreenChanged(this.m_GrassOffsetPollutionGreen, vector.y.ToString(this.m_Offsetformat));
		this.OnGrassOffsetFertilityBlueChanged(this.m_GrassOffsetPollutionBlue, vector.z.ToString(this.m_Offsetformat));
		vector = this.m_Defaults.Get<Vector3>("grassOffsetForest");
		this.OnGrassOffsetForestRedChanged(this.m_GrassOffsetPollutionRed, vector.x.ToString(this.m_Offsetformat));
		this.OnGrassOffsetForestGreenChanged(this.m_GrassOffsetPollutionGreen, vector.y.ToString(this.m_Offsetformat));
		this.OnGrassOffsetForestBlueChanged(this.m_GrassOffsetPollutionBlue, vector.z.ToString(this.m_Offsetformat));
	}

	private void OnGrassOffsetPollutionRedChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset = new Vector3(num, Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.y, Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.z);
		}
		this.m_GrassOffsetPollutionRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.x.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetPollutionGreenChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.x, num, Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.z);
		}
		this.m_GrassOffsetPollutionGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.y.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetPollutionBlueChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.x, Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.y, num);
		}
		this.m_GrassOffsetPollutionBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassPollutionColorOffset.z.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFieldRedChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset = new Vector3(num, Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.y, Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.z);
		}
		this.m_GrassOffsetFieldRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.x.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFieldGreenChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.x, num, Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.z);
		}
		this.m_GrassOffsetFieldGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.y.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFieldBlueChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.x, Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.y, num);
		}
		this.m_GrassOffsetFieldBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFieldColorOffset.z.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFertilityRedChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset = new Vector3(num, Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.y, Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.z);
		}
		this.m_GrassOffsetFertilityRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.x.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFertilityGreenChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.x, num, Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.z);
		}
		this.m_GrassOffsetFertilityGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.y.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetFertilityBlueChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.x, Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.y, num);
		}
		this.m_GrassOffsetFertilityBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassFertilityColorOffset.z.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetForestRedChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset = new Vector3(num, Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.y, Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.z);
		}
		this.m_GrassOffsetForestRed.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.x.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetForestGreenChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.x, num, Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.z);
		}
		this.m_GrassOffsetForestGreen.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.y.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassOffsetForestBlueChanged(UIComponent c, string value)
	{
		float num;
		if (float.TryParse(value, out num))
		{
			num = Mathf.Clamp(num, -1f, 1f);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset = new Vector3(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.x, Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.y, num);
		}
		this.m_GrassOffsetForestBlue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassForestColorOffset.z.ToString(this.m_Offsetformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnGrassGeometryChanged(UIComponent c, bool t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_useGrassDecorations = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnFertileGeometryChanged(UIComponent c, bool t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_useFertileDecorations = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnRocksGeometryChanged(UIComponent c, bool t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_useCliffDecorations = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseGrass(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_grassDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_grassDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseRuined(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_ruinedDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_ruinedDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffusePavement(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_pavementDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_pavementDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseGravel(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_gravelDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_gravelDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseCliff(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_cliffDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_cliffDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseSand(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_sandDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_sandDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseOil(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_oilDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_oilDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDiffuseOre(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_oreDiffuse = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_oreDiffuse);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void ToggleNormalCliffSandEvents(bool value)
	{
		UIComponent uIComponent = base.Find("NormalTextures");
		this.m_NormalCliff = uIComponent.Find("Cliff").Find<UITextureSprite>("Texture");
		if (value)
		{
			this.m_NormalCliff.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			this.m_NormalCliff.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			this.m_NormalCliff.add_eventClick(new MouseEventHandler(this.OnNormalTextureClicked));
			this.m_NormalSand.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			this.m_NormalSand.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			this.m_NormalSand.add_eventClick(new MouseEventHandler(this.OnNormalTextureClicked));
			this.m_NormalCliff.set_tooltip(null);
			this.m_NormalSand.set_tooltip(null);
		}
		else
		{
			this.m_NormalCliff.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			this.m_NormalCliff.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			this.m_NormalCliff.remove_eventClick(new MouseEventHandler(this.OnNormalTextureClicked));
			this.m_NormalSand.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			this.m_NormalSand.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			this.m_NormalSand.remove_eventClick(new MouseEventHandler(this.OnNormalTextureClicked));
			this.m_NormalCliff.set_tooltip(Locale.Get("LOADSTATUS", "Compressing"));
			this.m_NormalSand.set_tooltip(Locale.Get("LOADSTATUS", "Compressing"));
		}
	}

	private void SetCliffSandNormalTextureAsync(Texture2D cliffNTex, Texture2D sandNTex, bool useSandBA)
	{
		this.ToggleNormalCliffSandEvents(false);
		Image imgA = new Image(cliffNTex);
		Image imgB = new Image(sandNTex);
		try
		{
			Task task = ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				try
				{
					int width = imgA.get_width();
					int height = imgA.get_height();
					if (imgA.get_width() != imgB.get_width() || imgA.get_height() != imgB.get_height())
					{
						if (imgA.get_width() > imgB.get_width())
						{
							imgB.Resize(imgA.get_width(), imgA.get_height());
							width = imgA.get_width();
							height = imgA.get_height();
						}
						else
						{
							imgA.Resize(imgB.get_width(), imgB.get_height());
							width = imgB.get_width();
							height = imgB.get_height();
						}
					}
					imgA.Uncompress();
					imgB.Uncompress();
					Color32[] colors = imgA.GetColors32();
					Color32[] colors2 = imgB.GetColors32();
					if (colors.Length != colors2.Length)
					{
						UIView.get_library().ShowModal<ExceptionPanel>("ExceptionPanel").SetMessage("Error!", "Something went wrong while processing textures.", true);
					}
					else
					{
						Color32[] array = new Color32[colors2.Length];
						for (int i = 0; i < colors2.Length; i++)
						{
							if (useSandBA)
							{
								array[i] = new Color32(colors[i].r, colors[i].g, colors2[i].b, colors2[i].a);
							}
							else
							{
								array[i] = new Color32(colors[i].r, colors[i].g, colors2[i].r, colors2[i].g);
							}
						}
						Image finalImage = new Image(width, height, 4, array);
						finalImage.Compress(true, false);
						Task task2 = ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							Texture2D texture2D = finalImage.CreateTexture(true);
							Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal = texture2D;
							this.m_NormalCliff.set_texture(texture2D);
							this.m_NormalSand.set_texture(texture2D);
							TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal);
							Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
							this.ToggleNormalCliffSandEvents(true);
						});
						task2.Wait();
					}
				}
				catch (Exception ex2)
				{
					this.ToggleNormalCliffSandEvents(true);
					Debug.LogError("An exception occured " + ex2);
					UIView.ForwardException(ex2);
				}
			});
			if (LoadSaveStatus.activeTask != null)
			{
				AsyncTaskWrapper asyncTaskWrapper = LoadSaveStatus.activeTask as AsyncTaskWrapper;
				if (asyncTaskWrapper != null)
				{
					asyncTaskWrapper.AddTask(task);
				}
			}
			else
			{
				AsyncTaskWrapper activeTask = new AsyncTaskWrapper("Compressing", new Task[]
				{
					task
				});
				LoadSaveStatus.activeTask = activeTask;
			}
		}
		catch (Exception ex)
		{
			this.ToggleNormalCliffSandEvents(true);
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				Environment.StackTrace
			}));
		}
	}

	private void OnAssignTilingGrass(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingGrassValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingGrassValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_grassTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingGrass.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGrass));
			this.m_DiffuseTilingGrass.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingGrass.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGrass));
			this.m_DiffuseTilingGrassValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingRuined(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingRuinedValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingRuinedValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_ruinedTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingRuined.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingRuined));
			this.m_DiffuseTilingRuined.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingRuined.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingRuined));
			this.m_DiffuseTilingRuinedValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingPavement(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingPavementValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingPavementValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_pavementTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingPavement.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingPavement));
			this.m_DiffuseTilingPavement.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingPavement.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingPavement));
			this.m_DiffuseTilingPavementValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingGravel(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingGravelValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingGravelValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_gravelTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingGravel.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGravel));
			this.m_DiffuseTilingGravel.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingGravel.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGravel));
			this.m_DiffuseTilingGravelValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingCliff(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingCliffValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingCliffValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_cliffTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingCliff.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliff));
			this.m_DiffuseTilingCliff.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingCliff.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliff));
			this.m_DiffuseTilingCliffValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingSand(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingSandValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingSandValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_sandTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingSand.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingSand));
			this.m_DiffuseTilingSand.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingSand.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingSand));
			this.m_DiffuseTilingSandValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingOil(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingOilValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingOilValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_oilTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingOil.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOil));
			this.m_DiffuseTilingOil.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingOil.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOil));
			this.m_DiffuseTilingOilValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingOre(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_DiffuseTilingOreValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingOreValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_oreTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_DiffuseTilingOre.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOre));
			this.m_DiffuseTilingOre.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_DiffuseTilingOre.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOre));
			this.m_DiffuseTilingOreValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void OnAssignTilingCliffSandNormal(UIComponent c, float v)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling = ThemeEditorMainToolbar.TilingLinearize(v);
		this.m_NormalTilingCliffSandValue.set_text(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling.ToString(this.m_Tilingformat));
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignTilingCliffSandNormalValue(UIComponent c, string s)
	{
		float num;
		if (float.TryParse(s, out num))
		{
			num = Mathf.Clamp(num, ThemeEditorMainToolbar.TilingLinearMin, ThemeEditorMainToolbar.TilingLinearMax);
			Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormalTiling = num;
			Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
			this.m_NormalTilingCliffSand.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliffSandNormal));
			this.m_NormalTilingCliffSand.set_value(ThemeEditorMainToolbar.TilingNonlinearize(num));
			this.m_NormalTilingCliffSand.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliffSandNormal));
			this.m_NormalTilingCliffSandValue.set_text(num.ToString(this.m_Tilingformat));
		}
	}

	private void UnBindActions()
	{
		UIComponent uIComponent = base.Find("DiffuseTextures");
		this.m_ResetDiffuseTexButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetDiffuseTexButton.remove_eventClick(new MouseEventHandler(this.ResetDiffuseTextures));
		this.m_DiffuseGrass = uIComponent.Find("Grass").Find<UITextureSprite>("Texture");
		this.m_DiffuseGrass.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseGrass.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseGrass.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseGrass.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseGrass));
		this.m_DiffuseRuined = uIComponent.Find("Ruined").Find<UITextureSprite>("Texture");
		this.m_DiffuseRuined.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseRuined.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseRuined.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseRuined.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseRuined));
		this.m_DiffusePavement = uIComponent.Find("Pavement").Find<UITextureSprite>("Texture");
		this.m_DiffusePavement.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffusePavement.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffusePavement.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffusePavement.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffusePavement));
		this.m_DiffuseGravel = uIComponent.Find("Gravel").Find<UITextureSprite>("Texture");
		this.m_DiffuseGravel.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseGravel.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseGravel.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseGravel.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseGravel));
		this.m_DiffuseCliff = uIComponent.Find("Cliff").Find<UITextureSprite>("Texture");
		this.m_DiffuseCliff.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseCliff.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseCliff.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseCliff.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseCliff));
		this.m_DiffuseSand = uIComponent.Find("Sand").Find<UITextureSprite>("Texture");
		this.m_DiffuseSand.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseSand.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseSand.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseSand.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseSand));
		this.m_DiffuseOil = uIComponent.Find("Oil").Find<UITextureSprite>("Texture");
		this.m_DiffuseOil.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseOil.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseOil.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseOil.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseOil));
		this.m_DiffuseOre = uIComponent.Find("Ore").Find<UITextureSprite>("Texture");
		this.m_DiffuseOre.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseOre.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseOre.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseOre.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseOre));
		UIComponent uIComponent2 = base.Find("NormalTextures");
		this.m_ResetNormalTexButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetNormalTexButton.remove_eventClick(new MouseEventHandler(this.ResetNormalTextures));
		this.m_NormalCliff = uIComponent2.Find("Cliff").Find<UITextureSprite>("Texture");
		this.m_NormalSand = uIComponent2.Find("Sand").Find<UITextureSprite>("Texture");
		this.ToggleNormalCliffSandEvents(false);
		UIComponent uIComponent3 = base.Find("TilingDiffuse");
		this.m_ResetDiffuseTilingButton = uIComponent3.Find<UIButton>("Reset");
		this.m_ResetDiffuseTilingButton.remove_eventClick(new MouseEventHandler(this.ResetDiffuseTiling));
		this.m_DiffuseTilingGrass = uIComponent3.Find("Grass").Find<UISlider>("Slider");
		this.m_DiffuseTilingGrass.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGrass));
		this.m_DiffuseTilingRuined = uIComponent3.Find("Ruined").Find<UISlider>("Slider");
		this.m_DiffuseTilingRuined.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingRuined));
		this.m_DiffuseTilingPavement = uIComponent3.Find("Pavement").Find<UISlider>("Slider");
		this.m_DiffuseTilingPavement.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingPavement));
		this.m_DiffuseTilingGravel = uIComponent3.Find("Gravel").Find<UISlider>("Slider");
		this.m_DiffuseTilingGravel.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGravel));
		this.m_DiffuseTilingCliff = uIComponent3.Find("Cliff").Find<UISlider>("Slider");
		this.m_DiffuseTilingCliff.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliff));
		this.m_DiffuseTilingSand = uIComponent3.Find("Sand").Find<UISlider>("Slider");
		this.m_DiffuseTilingSand.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingSand));
		this.m_DiffuseTilingOil = uIComponent3.Find("Oil").Find<UISlider>("Slider");
		this.m_DiffuseTilingOil.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOil));
		this.m_DiffuseTilingOre = uIComponent3.Find("Ore").Find<UISlider>("Slider");
		this.m_DiffuseTilingOre.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOre));
		this.m_DiffuseTilingGrassValue = uIComponent3.Find("Grass").Find<UITextField>("Value");
		this.m_DiffuseTilingGrassValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingGrassValue));
		this.m_DiffuseTilingRuinedValue = uIComponent3.Find("Ruined").Find<UITextField>("Value");
		this.m_DiffuseTilingRuinedValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingRuinedValue));
		this.m_DiffuseTilingPavementValue = uIComponent3.Find("Pavement").Find<UITextField>("Value");
		this.m_DiffuseTilingPavementValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingPavementValue));
		this.m_DiffuseTilingGravelValue = uIComponent3.Find("Gravel").Find<UITextField>("Value");
		this.m_DiffuseTilingGravelValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingGravelValue));
		this.m_DiffuseTilingCliffValue = uIComponent3.Find("Cliff").Find<UITextField>("Value");
		this.m_DiffuseTilingCliffValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingCliffValue));
		this.m_DiffuseTilingSandValue = uIComponent3.Find("Sand").Find<UITextField>("Value");
		this.m_DiffuseTilingSandValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingSandValue));
		this.m_DiffuseTilingOilValue = uIComponent3.Find("Oil").Find<UITextField>("Value");
		this.m_DiffuseTilingOilValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingOilValue));
		this.m_DiffuseTilingOreValue = uIComponent3.Find("Ore").Find<UITextField>("Value");
		this.m_DiffuseTilingOreValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingOreValue));
		this.m_NormalTilingCliffSandValue = base.Find("TilingNormal").Find("CliffSand").Find<UITextField>("Value");
		this.m_NormalTilingCliffSandValue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingCliffSandNormalValue));
		UIComponent uIComponent4 = base.Find("TilingNormal");
		this.m_NormalTilingCliffSand = uIComponent4.Find("CliffSand").Find<UISlider>("Slider");
		this.m_NormalTilingCliffSand.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliffSandNormal));
		this.m_ResetNormalTilingButton = uIComponent4.Find<UIButton>("Reset");
		this.m_ResetNormalTilingButton.remove_eventClick(new MouseEventHandler(this.ResetNormalTiling));
		UIComponent uIComponent5 = base.Find("GeometryDetail");
		this.m_ResetGeometryButton = uIComponent5.Find<UIButton>("Reset");
		this.m_ResetGeometryButton.remove_eventClick(new MouseEventHandler(this.ResetGeometry));
		this.m_GrassGeometryEnabled = uIComponent5.Find("GrassDetail").Find<UICheckBox>("Value");
		this.m_GrassGeometryEnabled.remove_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnGrassGeometryChanged));
		this.m_FertileGeometryEnabled = uIComponent5.Find("FertileDetail").Find<UICheckBox>("Value");
		this.m_FertileGeometryEnabled.remove_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnFertileGeometryChanged));
		this.m_RocksGeometryEnabled = uIComponent5.Find("RockDetail").Find<UICheckBox>("Value");
		this.m_RocksGeometryEnabled.remove_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnRocksGeometryChanged));
		UIComponent uIComponent6 = base.Find("Offsets");
		this.m_ResetOffsetsButton = uIComponent6.Find<UIButton>("Reset");
		this.m_ResetOffsetsButton.remove_eventClick(new MouseEventHandler(this.ResetOffsets));
		this.m_GrassOffsetPollutionRed = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueRed");
		this.m_GrassOffsetPollutionRed.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionRedChanged));
		this.m_GrassOffsetPollutionGreen = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueGreen");
		this.m_GrassOffsetPollutionGreen.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionGreenChanged));
		this.m_GrassOffsetPollutionBlue = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueBlue");
		this.m_GrassOffsetPollutionBlue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionBlueChanged));
		this.m_GrassOffsetFieldRed = uIComponent6.Find<UITextField>("GrassFieldOffsetValueRed");
		this.m_GrassOffsetFieldRed.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldRedChanged));
		this.m_GrassOffsetFieldGreen = uIComponent6.Find<UITextField>("GrassFieldOffsetValueGreen");
		this.m_GrassOffsetFieldGreen.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldGreenChanged));
		this.m_GrassOffsetFieldBlue = uIComponent6.Find<UITextField>("GrassFieldOffsetValueBlue");
		this.m_GrassOffsetFieldBlue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldBlueChanged));
		this.m_GrassOffsetFertilityRed = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueRed");
		this.m_GrassOffsetFertilityRed.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityRedChanged));
		this.m_GrassOffsetFertilityGreen = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueGreen");
		this.m_GrassOffsetFertilityGreen.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityGreenChanged));
		this.m_GrassOffsetFertilityBlue = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueBlue");
		this.m_GrassOffsetFertilityBlue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityBlueChanged));
		this.m_GrassOffsetForestRed = uIComponent6.Find<UITextField>("GrassForestOffsetValueRed");
		this.m_GrassOffsetForestRed.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestRedChanged));
		this.m_GrassOffsetForestGreen = uIComponent6.Find<UITextField>("GrassForestOffsetValueGreen");
		this.m_GrassOffsetForestGreen.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestGreenChanged));
		this.m_GrassOffsetForestBlue = uIComponent6.Find<UITextField>("GrassForestOffsetValueBlue");
		this.m_GrassOffsetForestBlue.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestBlueChanged));
	}

	private void BindActions()
	{
		UIComponent uIComponent = base.Find("DiffuseTextures");
		this.m_ResetDiffuseTexButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetDiffuseTexButton.add_eventClick(new MouseEventHandler(this.ResetDiffuseTextures));
		this.m_DiffuseGrass = uIComponent.Find("Grass").Find<UITextureSprite>("Texture");
		this.m_DiffuseGrass.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseGrass.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseGrass.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseGrass.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseGrass));
		this.m_DiffuseRuined = uIComponent.Find("Ruined").Find<UITextureSprite>("Texture");
		this.m_DiffuseRuined.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseRuined.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseRuined.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseRuined.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseRuined));
		this.m_DiffusePavement = uIComponent.Find("Pavement").Find<UITextureSprite>("Texture");
		this.m_DiffusePavement.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffusePavement.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffusePavement.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffusePavement.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffusePavement));
		this.m_DiffuseGravel = uIComponent.Find("Gravel").Find<UITextureSprite>("Texture");
		this.m_DiffuseGravel.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseGravel.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseGravel.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseGravel.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseGravel));
		this.m_DiffuseCliff = uIComponent.Find("Cliff").Find<UITextureSprite>("Texture");
		this.m_DiffuseCliff.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseCliff.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseCliff.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseCliff.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseCliff));
		this.m_DiffuseSand = uIComponent.Find("Sand").Find<UITextureSprite>("Texture");
		this.m_DiffuseSand.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseSand.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseSand.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseSand.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseSand));
		this.m_DiffuseOil = uIComponent.Find("Oil").Find<UITextureSprite>("Texture");
		this.m_DiffuseOil.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseOil.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseOil.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseOil.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseOil));
		this.m_DiffuseOre = uIComponent.Find("Ore").Find<UITextureSprite>("Texture");
		this.m_DiffuseOre.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DiffuseOre.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DiffuseOre.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DiffuseOre.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDiffuseOre));
		UIComponent uIComponent2 = base.Find("NormalTextures");
		this.m_ResetNormalTexButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetNormalTexButton.add_eventClick(new MouseEventHandler(this.ResetNormalTextures));
		this.m_NormalCliff = uIComponent2.Find("Cliff").Find<UITextureSprite>("Texture");
		this.m_NormalSand = uIComponent2.Find("Sand").Find<UITextureSprite>("Texture");
		this.ToggleNormalCliffSandEvents(true);
		UIComponent uIComponent3 = base.Find("TilingDiffuse");
		this.m_ResetDiffuseTilingButton = uIComponent3.Find<UIButton>("Reset");
		this.m_ResetDiffuseTilingButton.add_eventClick(new MouseEventHandler(this.ResetDiffuseTiling));
		this.m_DiffuseTilingGrass = uIComponent3.Find("Grass").Find<UISlider>("Slider");
		this.m_DiffuseTilingGrass.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGrass));
		this.m_DiffuseTilingRuined = uIComponent3.Find("Ruined").Find<UISlider>("Slider");
		this.m_DiffuseTilingRuined.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingRuined));
		this.m_DiffuseTilingPavement = uIComponent3.Find("Pavement").Find<UISlider>("Slider");
		this.m_DiffuseTilingPavement.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingPavement));
		this.m_DiffuseTilingGravel = uIComponent3.Find("Gravel").Find<UISlider>("Slider");
		this.m_DiffuseTilingGravel.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingGravel));
		this.m_DiffuseTilingCliff = uIComponent3.Find("Cliff").Find<UISlider>("Slider");
		this.m_DiffuseTilingCliff.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliff));
		this.m_DiffuseTilingSand = uIComponent3.Find("Sand").Find<UISlider>("Slider");
		this.m_DiffuseTilingSand.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingSand));
		this.m_DiffuseTilingOil = uIComponent3.Find("Oil").Find<UISlider>("Slider");
		this.m_DiffuseTilingOil.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOil));
		this.m_DiffuseTilingOre = uIComponent3.Find("Ore").Find<UISlider>("Slider");
		this.m_DiffuseTilingOre.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingOre));
		this.m_DiffuseTilingGrassValue = uIComponent3.Find("Grass").Find<UITextField>("Value");
		this.m_DiffuseTilingGrassValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingGrassValue));
		this.m_DiffuseTilingRuinedValue = uIComponent3.Find("Ruined").Find<UITextField>("Value");
		this.m_DiffuseTilingRuinedValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingRuinedValue));
		this.m_DiffuseTilingPavementValue = uIComponent3.Find("Pavement").Find<UITextField>("Value");
		this.m_DiffuseTilingPavementValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingPavementValue));
		this.m_DiffuseTilingGravelValue = uIComponent3.Find("Gravel").Find<UITextField>("Value");
		this.m_DiffuseTilingGravelValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingGravelValue));
		this.m_DiffuseTilingCliffValue = uIComponent3.Find("Cliff").Find<UITextField>("Value");
		this.m_DiffuseTilingCliffValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingCliffValue));
		this.m_DiffuseTilingSandValue = uIComponent3.Find("Sand").Find<UITextField>("Value");
		this.m_DiffuseTilingSandValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingSandValue));
		this.m_DiffuseTilingOilValue = uIComponent3.Find("Oil").Find<UITextField>("Value");
		this.m_DiffuseTilingOilValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingOilValue));
		this.m_DiffuseTilingOreValue = uIComponent3.Find("Ore").Find<UITextField>("Value");
		this.m_DiffuseTilingOreValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingOreValue));
		UIComponent uIComponent4 = base.Find("TilingNormal");
		this.m_ResetNormalTilingButton = uIComponent4.Find<UIButton>("Reset");
		this.m_ResetNormalTilingButton.add_eventClick(new MouseEventHandler(this.ResetNormalTiling));
		this.m_NormalTilingCliffSandValue = uIComponent4.Find("CliffSand").Find<UITextField>("Value");
		this.m_NormalTilingCliffSandValue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnAssignTilingCliffSandNormalValue));
		this.m_NormalTilingCliffSand = base.Find("TilingNormal").Find("CliffSand").Find<UISlider>("Slider");
		this.m_NormalTilingCliffSand.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignTilingCliffSandNormal));
		UIComponent uIComponent5 = base.Find("GeometryDetail");
		this.m_ResetGeometryButton = uIComponent5.Find<UIButton>("Reset");
		this.m_ResetGeometryButton.add_eventClick(new MouseEventHandler(this.ResetGeometry));
		this.m_GrassGeometryEnabled = uIComponent5.Find("GrassDetail").Find<UICheckBox>("Value");
		this.m_GrassGeometryEnabled.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnGrassGeometryChanged));
		this.m_FertileGeometryEnabled = uIComponent5.Find("FertileDetail").Find<UICheckBox>("Value");
		this.m_FertileGeometryEnabled.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnFertileGeometryChanged));
		this.m_RocksGeometryEnabled = uIComponent5.Find("RockDetail").Find<UICheckBox>("Value");
		this.m_RocksGeometryEnabled.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnRocksGeometryChanged));
		UIComponent uIComponent6 = base.Find("Offsets");
		this.m_ResetOffsetsButton = uIComponent6.Find<UIButton>("Reset");
		this.m_ResetOffsetsButton.add_eventClick(new MouseEventHandler(this.ResetOffsets));
		this.m_GrassOffsetPollutionRed = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueRed");
		this.m_GrassOffsetPollutionRed.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionRedChanged));
		this.m_GrassOffsetPollutionGreen = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueGreen");
		this.m_GrassOffsetPollutionGreen.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionGreenChanged));
		this.m_GrassOffsetPollutionBlue = uIComponent6.Find<UITextField>("GrassPollutionOffsetValueBlue");
		this.m_GrassOffsetPollutionBlue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetPollutionBlueChanged));
		this.m_GrassOffsetFieldRed = uIComponent6.Find<UITextField>("GrassFieldOffsetValueRed");
		this.m_GrassOffsetFieldRed.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldRedChanged));
		this.m_GrassOffsetFieldGreen = uIComponent6.Find<UITextField>("GrassFieldOffsetValueGreen");
		this.m_GrassOffsetFieldGreen.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldGreenChanged));
		this.m_GrassOffsetFieldBlue = uIComponent6.Find<UITextField>("GrassFieldOffsetValueBlue");
		this.m_GrassOffsetFieldBlue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFieldBlueChanged));
		this.m_GrassOffsetFertilityRed = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueRed");
		this.m_GrassOffsetFertilityRed.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityRedChanged));
		this.m_GrassOffsetFertilityGreen = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueGreen");
		this.m_GrassOffsetFertilityGreen.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityGreenChanged));
		this.m_GrassOffsetFertilityBlue = uIComponent6.Find<UITextField>("GrassFertilityOffsetValueBlue");
		this.m_GrassOffsetFertilityBlue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetFertilityBlueChanged));
		this.m_GrassOffsetForestRed = uIComponent6.Find<UITextField>("GrassForestOffsetValueRed");
		this.m_GrassOffsetForestRed.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestRedChanged));
		this.m_GrassOffsetForestGreen = uIComponent6.Find<UITextField>("GrassForestOffsetValueGreen");
		this.m_GrassOffsetForestGreen.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestGreenChanged));
		this.m_GrassOffsetForestBlue = uIComponent6.Find<UITextField>("GrassForestOffsetValueBlue");
		this.m_GrassOffsetForestBlue.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnGrassOffsetForestBlueChanged));
	}

	private void OnHoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_HoverColor);
	}

	private void OnUnhoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_NormalColor);
	}

	private void Awake()
	{
	}

	private void ToggleTextureEvents(UITextureSprite s, bool value)
	{
		if (value)
		{
			s.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(null);
		}
		else
		{
			s.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(Locale.Get("LOADSTATUS", "Compressing"));
		}
	}

	private void OnNormalTextureClicked(UIComponent c, UIMouseEventParameter p)
	{
		UITextureSprite s = p.get_source() as UITextureSprite;
		if (s != null)
		{
			TexturePicker tp = UIView.get_library().Get<TexturePicker>("TexturePicker");
			tp.ShowModal(s, TexturePicker.Type.ThemeTextures, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					if (s == this.m_NormalCliff)
					{
						this.SetCliffSandNormalTextureAsync(tp.selectedTexture as Texture2D, Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal, true);
					}
					else
					{
						this.SetCliffSandNormalTextureAsync(Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal, tp.selectedTexture as Texture2D, false);
					}
				}
			});
		}
	}

	private void OnTextureClicked(UIComponent c, UIMouseEventParameter p)
	{
		UITextureSprite s = p.get_source() as UITextureSprite;
		if (s != null)
		{
			TexturePicker tp = UIView.get_library().Get<TexturePicker>("TexturePicker");
			tp.ShowModal(s, TexturePicker.Type.ThemeTextures, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					s.set_texture(tp.selectedTexture);
					this.ToggleTextureEvents(s, false);
					this.CompressThreaded(s, tp.selectedImage);
				}
			});
		}
	}

	private void CompressThreaded(UITextureSprite s, Image im)
	{
		try
		{
			Task task = ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				im.Compress(true, true);
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					s.set_texture(im.CreateTexture());
					this.ToggleTextureEvents(s, true);
				}).Wait();
			});
			if (LoadSaveStatus.activeTask != null)
			{
				AsyncTaskWrapper asyncTaskWrapper = LoadSaveStatus.activeTask as AsyncTaskWrapper;
				if (asyncTaskWrapper != null)
				{
					asyncTaskWrapper.AddTask(task);
				}
			}
			else
			{
				AsyncTaskWrapper activeTask = new AsyncTaskWrapper("Compressing", new Task[]
				{
					task
				});
				LoadSaveStatus.activeTask = activeTask;
			}
		}
		catch (Exception ex)
		{
			this.ToggleTextureEvents(s, true);
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				Environment.StackTrace
			}));
		}
	}

	private void Start()
	{
		base.get_component().Hide();
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	public Color32 m_NormalColor;

	public Color32 m_HoverColor;

	private ValueStorage m_Defaults;

	private UITextureSprite m_DiffuseGrass;

	private UITextureSprite m_DiffuseRuined;

	private UITextureSprite m_DiffusePavement;

	private UITextureSprite m_DiffuseGravel;

	private UITextureSprite m_DiffuseCliff;

	private UITextureSprite m_DiffuseSand;

	private UITextureSprite m_DiffuseOil;

	private UITextureSprite m_DiffuseOre;

	private UISlider m_DiffuseTilingGrass;

	private UISlider m_DiffuseTilingRuined;

	private UISlider m_DiffuseTilingPavement;

	private UISlider m_DiffuseTilingGravel;

	private UISlider m_DiffuseTilingCliff;

	private UISlider m_DiffuseTilingSand;

	private UISlider m_DiffuseTilingOil;

	private UISlider m_DiffuseTilingOre;

	private readonly string m_Tilingformat = "0.0000";

	private UITextField m_DiffuseTilingGrassValue;

	private UITextField m_DiffuseTilingRuinedValue;

	private UITextField m_DiffuseTilingPavementValue;

	private UITextField m_DiffuseTilingGravelValue;

	private UITextField m_DiffuseTilingCliffValue;

	private UITextField m_DiffuseTilingSandValue;

	private UITextField m_DiffuseTilingOilValue;

	private UITextField m_DiffuseTilingOreValue;

	private UITextField m_NormalTilingCliffSandValue;

	private UITextureSprite m_NormalSand;

	private UITextureSprite m_NormalCliff;

	private UISlider m_NormalTilingCliffSand;

	private UICheckBox m_GrassGeometryEnabled;

	private UICheckBox m_FertileGeometryEnabled;

	private UICheckBox m_RocksGeometryEnabled;

	private readonly string m_Offsetformat = "0.000";

	private UITextField m_GrassOffsetPollutionRed;

	private UITextField m_GrassOffsetPollutionGreen;

	private UITextField m_GrassOffsetPollutionBlue;

	private UITextField m_GrassOffsetFieldRed;

	private UITextField m_GrassOffsetFieldGreen;

	private UITextField m_GrassOffsetFieldBlue;

	private UITextField m_GrassOffsetFertilityRed;

	private UITextField m_GrassOffsetFertilityGreen;

	private UITextField m_GrassOffsetFertilityBlue;

	private UITextField m_GrassOffsetForestRed;

	private UITextField m_GrassOffsetForestGreen;

	private UITextField m_GrassOffsetForestBlue;

	private UIButton m_ResetDiffuseTexButton;

	private UIButton m_ResetDiffuseTilingButton;

	private UIButton m_ResetNormalTexButton;

	private UIButton m_ResetNormalTilingButton;

	private UIButton m_ResetGeometryButton;

	private UIButton m_ResetOffsetsButton;
}
