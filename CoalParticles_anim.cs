﻿using System;
using UnityEngine;

public class CoalParticles_anim : MonoBehaviour
{
	private void Update()
	{
		float num = Time.get_time() * this.textureSpeed;
		base.GetComponent<Renderer>().get_material().SetTextureOffset("_MainTex", new Vector2(0f, num));
	}

	public float textureSpeed = 10f;
}
