﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class ThemeEditorMainToolbar : MainToolbar
{
	public TerrainPropertiesPanel terrainPropertiesPanel
	{
		get
		{
			if (this.m_TerrainPropertiesPanel == null)
			{
				this.m_TerrainPropertiesPanel = UIView.Find<UIPanel>("TerrainPropertiesPanel").GetComponent<TerrainPropertiesPanel>();
			}
			return this.m_TerrainPropertiesPanel;
		}
	}

	public AtmospherePropertiesPanel atmospherePropertiesPanel
	{
		get
		{
			if (this.m_AtmospherePropertiesPanel == null)
			{
				this.m_AtmospherePropertiesPanel = UIView.Find<UIPanel>("AtmospherePropertiesPanel").GetComponent<AtmospherePropertiesPanel>();
			}
			return this.m_AtmospherePropertiesPanel;
		}
	}

	public WaterPropertiesPanel waterPropertiesPanel
	{
		get
		{
			if (this.m_WaterPropertiesPanel == null)
			{
				this.m_WaterPropertiesPanel = UIView.Find<UIPanel>("WaterPropertiesPanel").GetComponent<WaterPropertiesPanel>();
			}
			return this.m_WaterPropertiesPanel;
		}
	}

	public StructuresPropertiesPanel structuresPropertiesPanel
	{
		get
		{
			if (this.m_StructuresPropertiesPanel == null)
			{
				this.m_StructuresPropertiesPanel = UIView.Find<UIPanel>("StructuresPropertiesPanel").GetComponent<StructuresPropertiesPanel>();
			}
			return this.m_StructuresPropertiesPanel;
		}
	}

	public WorldPropertiesPanel worldPropertiesPanel
	{
		get
		{
			if (this.m_WorldPropertiesPanel == null)
			{
				this.m_WorldPropertiesPanel = UIView.Find<UIPanel>("WorldPropertiesPanel").GetComponent<WorldPropertiesPanel>();
			}
			return this.m_WorldPropertiesPanel;
		}
	}

	public DisasterPropertiesPanel disasterPropertiesPanel
	{
		get
		{
			if (this.m_DisasterPropertiesPanel == null)
			{
				this.m_DisasterPropertiesPanel = UIView.Find<UIPanel>("DisasterPropertiesPanel").GetComponent<DisasterPropertiesPanel>();
			}
			return this.m_DisasterPropertiesPanel;
		}
	}

	public static float TilingNonlinearize(float linear)
	{
		float num = (linear - ThemeEditorMainToolbar.TilingLinearMin) / (ThemeEditorMainToolbar.TilingLinearMax - ThemeEditorMainToolbar.TilingLinearMin);
		return Mathf.Sqrt(num) * (ThemeEditorMainToolbar.TilingNonlinearMax - ThemeEditorMainToolbar.TilingNonlinearMin) + ThemeEditorMainToolbar.TilingNonlinearMin;
	}

	public static float TilingLinearize(float nonlinear)
	{
		float num = (nonlinear - ThemeEditorMainToolbar.TilingNonlinearMin) / (ThemeEditorMainToolbar.TilingNonlinearMax - ThemeEditorMainToolbar.TilingNonlinearMin);
		float num2 = num * num;
		return num2 * (ThemeEditorMainToolbar.TilingLinearMax - ThemeEditorMainToolbar.TilingLinearMin) + ThemeEditorMainToolbar.TilingLinearMin;
	}

	public void ResetToDefaults()
	{
		this.m_TerrainPropertiesPanel.ResetAll();
		this.m_WaterPropertiesPanel.ResetAll();
		this.m_WorldPropertiesPanel.ResetAll();
		this.m_AtmospherePropertiesPanel.ResetAll();
		this.m_StructuresPropertiesPanel.ResetAll();
		this.m_DisasterPropertiesPanel.ResetAll();
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHidePanels));
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		UITabstrip uITabstrip = base.get_component().Find<UITabstrip>("MainToolstrip");
		uITabstrip.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.ShowHidePanels));
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		UITabstrip strip = base.get_component().Find<UITabstrip>("MainToolstrip");
		UIButton uIButton = base.SpawnButtonEntry(strip, "TerrainProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_TerrainPropertiesIndex = uIButton.get_zOrder();
		UIButton uIButton2 = base.SpawnButtonEntry(strip, "WaterProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_WaterPropertiesIndex = uIButton2.get_zOrder();
		UIButton uIButton3 = base.SpawnButtonEntry(strip, "AtmosphereProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_AtmospherePropertiesIndex = uIButton3.get_zOrder();
		UIButton uIButton4 = base.SpawnButtonEntry(strip, "StructuresProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_StructuresPropertiesIndex = uIButton4.get_zOrder();
		UIButton uIButton5 = base.SpawnButtonEntry(strip, "WorldProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_WorldPropertiesIndex = uIButton5.get_zOrder();
		UIButton uIButton6 = base.SpawnButtonEntry(strip, "DisasterProperties", "THEMEEDITOR_TOOL", "ToolbarIcon", true);
		this.m_DisasterPropertiesIndex = uIButton6.get_zOrder();
		if (!SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			uIButton6.Disable();
		}
		base.SpawnSubEntry(strip, "ThemeSettings", "THEMEEDITOR_TOOL", null, "ToolbarIcon", true);
	}

	private void ShowHidePanels(UIComponent comp, int index)
	{
		UITabstrip uITabstrip = comp as UITabstrip;
		if (index == this.m_TerrainPropertiesIndex)
		{
			this.terrainPropertiesPanel.Show();
			this.atmospherePropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_AtmospherePropertiesIndex)
		{
			this.terrainPropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.atmospherePropertiesPanel.Show();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_WaterPropertiesIndex)
		{
			this.terrainPropertiesPanel.Hide();
			this.waterPropertiesPanel.Show();
			this.atmospherePropertiesPanel.Hide();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_WorldPropertiesIndex)
		{
			this.terrainPropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.atmospherePropertiesPanel.Hide();
			this.worldPropertiesPanel.Show();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_StructuresPropertiesIndex)
		{
			this.terrainPropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.atmospherePropertiesPanel.Hide();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Show();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Hide();
		}
		else if (index == this.m_DisasterPropertiesIndex)
		{
			this.terrainPropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.atmospherePropertiesPanel.Hide();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Show();
			uITabstrip.get_tabContainer().Hide();
		}
		else
		{
			this.terrainPropertiesPanel.Hide();
			this.atmospherePropertiesPanel.Hide();
			this.waterPropertiesPanel.Hide();
			this.worldPropertiesPanel.Hide();
			this.structuresPropertiesPanel.Hide();
			this.disasterPropertiesPanel.Hide();
			uITabstrip.get_tabContainer().Show();
		}
	}

	private int m_TerrainPropertiesIndex;

	private int m_AtmospherePropertiesIndex;

	private int m_WaterPropertiesIndex;

	private int m_StructuresPropertiesIndex;

	private int m_WorldPropertiesIndex;

	private int m_DisasterPropertiesIndex;

	private TerrainPropertiesPanel m_TerrainPropertiesPanel;

	private AtmospherePropertiesPanel m_AtmospherePropertiesPanel;

	private WaterPropertiesPanel m_WaterPropertiesPanel;

	private StructuresPropertiesPanel m_StructuresPropertiesPanel;

	private WorldPropertiesPanel m_WorldPropertiesPanel;

	private DisasterPropertiesPanel m_DisasterPropertiesPanel;

	public static readonly float TilingLinearMin = 0.0005f;

	public static readonly float TilingLinearMax = 0.1f;

	public static readonly float TilingNonlinearMin = 0.001f;

	public static readonly float TilingNonlinearMax = 0.08f;
}
