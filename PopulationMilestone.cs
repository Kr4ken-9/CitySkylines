﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class PopulationMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int num;
		if (this.m_autoAdjustTarget)
		{
			num = Singleton<GameAreaManager>.get_instance().ScalePopulationTarget(this.m_targetPopulation);
			num = Singleton<UnlockManager>.get_instance().m_MilestonesWrapper.OnGetPopulationTarget(this.m_targetPopulation, num);
		}
		else
		{
			num = this.m_targetPopulation;
		}
		info.m_min = 0f;
		info.m_max = (float)num;
		long num2;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num2 = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num2 = 0L;
		}
		if (num2 > (long)num)
		{
			num2 = (long)num;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_POPULATION_DESC"), num);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_POPULATION_PROG"), new object[]
		{
			num2,
			num
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		return this.m_targetPopulation;
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		int num;
		if (this.m_autoAdjustTarget)
		{
			num = Singleton<GameAreaManager>.get_instance().ScalePopulationTarget(this.m_targetPopulation);
			num = Singleton<UnlockManager>.get_instance().m_MilestonesWrapper.OnGetPopulationTarget(this.m_targetPopulation, num);
		}
		else
		{
			num = this.m_targetPopulation;
		}
		if (forceUnlock)
		{
			data.m_progress = (long)num;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			data.m_progress = (long)Mathf.Min(num, (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount);
			data.m_passedCount = ((data.m_progress != (long)num) ? 0 : 1);
		}
		return data;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		int num2;
		if (this.m_autoAdjustTarget)
		{
			num2 = Singleton<GameAreaManager>.get_instance().ScalePopulationTarget(this.m_targetPopulation);
			num2 = Singleton<UnlockManager>.get_instance().m_MilestonesWrapper.OnGetPopulationTarget(this.m_targetPopulation, num2);
		}
		else
		{
			num2 = this.m_targetPopulation;
		}
		if (num2 != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)num2));
		}
		return num;
	}

	public int m_targetPopulation = 1000;

	public bool m_autoAdjustTarget = true;
}
