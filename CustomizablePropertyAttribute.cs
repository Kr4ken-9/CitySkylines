﻿using System;

[AttributeUsage(AttributeTargets.Field)]
public class CustomizablePropertyAttribute : Attribute
{
	public CustomizablePropertyAttribute(string name)
	{
		this.m_Name = name;
	}

	public CustomizablePropertyAttribute(string name, string group)
	{
		this.m_Name = name;
		this.m_Group = group;
		this.m_Priority = 0;
	}

	public CustomizablePropertyAttribute(string name, string group, int priority)
	{
		this.m_Name = name;
		this.m_Group = group;
		this.m_Priority = priority;
	}

	public string name
	{
		get
		{
			return this.m_Name;
		}
		set
		{
			this.m_Name = value;
		}
	}

	public string group
	{
		get
		{
			return this.m_Group;
		}
		set
		{
			this.m_Group = value;
		}
	}

	public int priority
	{
		get
		{
			return this.m_Priority;
		}
		set
		{
			this.m_Priority = value;
		}
	}

	private string m_Name;

	private string m_Group;

	private int m_Priority;
}
