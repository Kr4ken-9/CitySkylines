﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class BrushOptionPanel : ToolsModifierControl
{
	public int selectedIndex
	{
		get
		{
			return this.m_SelectedIndex;
		}
		set
		{
			if (value != this.m_SelectedIndex)
			{
				this.SelectByIndex(value);
			}
		}
	}

	public void OpenFolder()
	{
		Utils.OpenInFileBrowser(BrushOptionPanel.brushesPath);
	}

	private void SelectByIndex(int value)
	{
		value = Mathf.Max(Mathf.Min(value, this.m_BrushesContainer.get_childCount() - 1), 0);
		if (value == this.m_SelectedIndex)
		{
			return;
		}
		this.m_SelectedIndex = value;
		for (int i = 0; i < this.m_BrushesContainer.get_childCount(); i++)
		{
			UIButton uIButton = this.m_BrushesContainer.get_components()[i] as UIButton;
			if (!(uIButton == null))
			{
				if (i == value)
				{
					uIButton.set_state(1);
				}
				else
				{
					uIButton.set_state(0);
				}
			}
		}
	}

	private void ProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (eventType != 4)
		{
			return;
		}
		if (this.m_IncreaseBrushSize.IsPressed(eventType, keyCode, modifiers))
		{
			BrushOptionPanel.SetSliderValue(this.m_BrushSizeSlider, BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider) + BrushOptionPanel.kBrushSizeInterval);
		}
		else if (this.m_DecreaseBrushSize.IsPressed(eventType, keyCode, modifiers))
		{
			BrushOptionPanel.SetSliderValue(this.m_BrushSizeSlider, BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider) - BrushOptionPanel.kBrushSizeInterval);
		}
		else if (this.m_IncreaseBrushStrength.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_BrushStrengthSlider.set_value(this.m_BrushStrengthSlider.get_value() + BrushOptionPanel.kBrushStrengthInterval);
		}
		else if (this.m_DecreaseBrushStrength.IsPressed(eventType, keyCode, modifiers))
		{
			this.m_BrushStrengthSlider.set_value(this.m_BrushStrengthSlider.get_value() - BrushOptionPanel.kBrushStrengthInterval);
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible() && this.selectedIndex >= 0 && this.selectedIndex <= this.m_BrushesContainer.get_childCount() - 1)
		{
			UIButton uIButton = this.m_BrushesContainer.get_components()[this.selectedIndex] as UIButton;
			if (uIButton != null && !uIButton.get_containsMouse())
			{
				uIButton.set_state(1);
			}
		}
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	private void SetBrushSize(float val)
	{
		PropTool currentTool = ToolsModifierControl.GetCurrentTool<PropTool>();
		if (currentTool != null)
		{
			currentTool.m_brushSize = val;
			if (currentTool.m_brushSize == this.m_BrushSizeSlider.get_minValue())
			{
				currentTool.m_mode = PropTool.Mode.Single;
			}
			else
			{
				currentTool.m_mode = PropTool.Mode.Brush;
			}
		}
		TerrainTool currentTool2 = ToolsModifierControl.GetCurrentTool<TerrainTool>();
		if (currentTool2 != null)
		{
			currentTool2.m_brushSize = val;
		}
		TreeTool currentTool3 = ToolsModifierControl.GetCurrentTool<TreeTool>();
		if (currentTool3 != null)
		{
			currentTool3.m_brushSize = val;
			if (currentTool3.m_brushSize == this.m_BrushSizeSlider.get_minValue())
			{
				currentTool3.m_mode = TreeTool.Mode.Single;
			}
			else
			{
				currentTool3.m_mode = TreeTool.Mode.Brush;
			}
		}
		ResourceTool currentTool4 = ToolsModifierControl.GetCurrentTool<ResourceTool>();
		if (currentTool4 != null)
		{
			currentTool4.m_brushSize = val;
		}
	}

	private void SetBrushStrength(float val)
	{
		PropTool currentTool = ToolsModifierControl.GetCurrentTool<PropTool>();
		if (currentTool != null)
		{
			currentTool.m_strength = val;
		}
		TerrainTool currentTool2 = ToolsModifierControl.GetCurrentTool<TerrainTool>();
		if (currentTool2 != null)
		{
			currentTool2.m_strength = val;
		}
		TreeTool currentTool3 = ToolsModifierControl.GetCurrentTool<TreeTool>();
		if (currentTool3 != null)
		{
			currentTool3.m_strength = val;
		}
		ResourceTool currentTool4 = ToolsModifierControl.GetCurrentTool<ResourceTool>();
		if (currentTool4 != null)
		{
			currentTool4.m_strength = val;
		}
	}

	private void Start()
	{
		this.Refresh();
		for (int i = 0; i < BrushOptionPanel.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + BrushOptionPanel.m_Extensions[i], BrushOptionPanel.brushesPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
		}
	}

	private static float GetSliderValue(UISlider slider)
	{
		float minValue = slider.get_minValue();
		float maxValue = slider.get_maxValue();
		float num = Mathf.Clamp01((slider.get_value() - minValue) / Mathf.Max(1E-05f, maxValue - minValue));
		return minValue + Mathf.Pow(num, 2f) * (maxValue - minValue);
	}

	private static void SetSliderValue(UISlider slider, float value)
	{
		float minValue = slider.get_minValue();
		float maxValue = slider.get_maxValue();
		value = Mathf.Clamp01((value - minValue) / Mathf.Max(1E-05f, maxValue - minValue));
		slider.set_value(minValue + Mathf.Pow(value, 0.5f) * (maxValue - minValue));
	}

	private bool SupportsSingle()
	{
		PropTool currentTool = ToolsModifierControl.GetCurrentTool<PropTool>();
		if (currentTool != null)
		{
			return true;
		}
		TreeTool currentTool2 = ToolsModifierControl.GetCurrentTool<TreeTool>();
		return currentTool2 != null;
	}

	public static string brushesPath
	{
		get
		{
			if (BrushOptionPanel.m_BrushesPath == null)
			{
				BrushOptionPanel.m_BrushesPath = Path.Combine(Path.Combine(DataLocation.get_addonsPath(), "MapEditor"), "Brushes");
			}
			if (!Directory.Exists(BrushOptionPanel.m_BrushesPath))
			{
				Directory.CreateDirectory(BrushOptionPanel.m_BrushesPath);
			}
			return BrushOptionPanel.m_BrushesPath;
		}
	}

	private void Awake()
	{
		this.m_IncreaseBrushSize = new SavedInputKey(Settings.mapEditorIncreaseBrushSize, Settings.inputSettingsFile, DefaultSettings.mapEditorIncreaseBrushSize, true);
		this.m_DecreaseBrushSize = new SavedInputKey(Settings.mapEditorDecreaseBrushSize, Settings.inputSettingsFile, DefaultSettings.mapEditorDecreaseBrushSize, true);
		this.m_IncreaseBrushStrength = new SavedInputKey(Settings.mapEditorIncreaseBrushStrength, Settings.inputSettingsFile, DefaultSettings.mapEditorIncreaseBrushStrength, true);
		this.m_DecreaseBrushStrength = new SavedInputKey(Settings.mapEditorDecreaseBrushStrength, Settings.inputSettingsFile, DefaultSettings.mapEditorDecreaseBrushStrength, true);
		this.Hide();
		this.m_BrushSizeSlider = base.Find<UISlider>("BrushSize");
		this.m_BrushSizeTextfield = base.Find<UITextField>("BrushSize");
		this.m_BrushStrengthSlider = base.Find<UISlider>("BrushStrength");
		this.m_BrushStrengthTextfield = base.Find<UITextField>("BrushStrength");
		this.m_BrushesContainer = base.Find<UIScrollablePanel>("BrushesContainer");
		float sliderValue = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
		if (this.SupportsSingle())
		{
			if (sliderValue == this.m_BrushSizeSlider.get_minValue())
			{
				this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
			}
			else
			{
				this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue).ToString());
			}
		}
		else
		{
			this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue).ToString());
		}
		this.m_BrushStrengthTextfield.set_text(this.m_BrushStrengthSlider.get_value().ToString());
		this.m_BrushSizeSlider.add_eventValueChanged(delegate(UIComponent sender, float value)
		{
			value = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
			if (this.SupportsSingle())
			{
				if (value == this.m_BrushSizeSlider.get_minValue())
				{
					this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
				}
				else
				{
					this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(value).ToString());
				}
			}
			else
			{
				this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(value).ToString());
			}
			this.SetBrushSize(value);
		});
		this.m_BrushSizeTextfield.add_eventTextSubmitted(delegate(UIComponent sender, string s)
		{
			int num;
			if (int.TryParse(s, out num))
			{
				BrushOptionPanel.SetSliderValue(this.m_BrushSizeSlider, (float)num);
				float sliderValue2 = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
				if (this.SupportsSingle())
				{
					if (sliderValue2 == this.m_BrushSizeSlider.get_minValue())
					{
						this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
					}
					else
					{
						this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue2).ToString());
					}
				}
				else
				{
					this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue2).ToString());
				}
				this.SetBrushSize(sliderValue2);
			}
			else if (string.Compare(s, Locale.Get("TOOL_SINGLE"), StringComparison.OrdinalIgnoreCase) == 0)
			{
				BrushOptionPanel.SetSliderValue(this.m_BrushSizeSlider, this.m_BrushSizeSlider.get_minValue());
				float sliderValue3 = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
				if (this.SupportsSingle())
				{
					if (sliderValue3 == this.m_BrushSizeSlider.get_minValue())
					{
						this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
					}
					else
					{
						this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue3).ToString());
					}
				}
				else
				{
					this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue3).ToString());
				}
				this.SetBrushSize(sliderValue3);
			}
			else
			{
				float sliderValue4 = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
				if (this.SupportsSingle())
				{
					if (sliderValue4 == this.m_BrushSizeSlider.get_minValue())
					{
						this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
					}
					else
					{
						this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue4).ToString());
					}
				}
				else
				{
					this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue4).ToString());
				}
			}
		});
		this.m_BrushSizeTextfield.add_eventTextCancelled(delegate(UIComponent sender, string s)
		{
			this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider)).ToString());
		});
		this.m_BrushStrengthSlider.add_eventValueChanged(delegate(UIComponent sender, float value)
		{
			this.m_BrushStrengthTextfield.set_text(value.ToString("0.00"));
			this.SetBrushStrength(value);
		});
		this.m_BrushStrengthTextfield.add_eventTextSubmitted(delegate(UIComponent sender, string s)
		{
			float num;
			if (float.TryParse(s, out num))
			{
				if (num < 0f)
				{
					num = 0f;
					this.m_BrushStrengthTextfield.set_text(num.ToString("0.00"));
				}
				else if (num > 1f)
				{
					num = 1f;
					this.m_BrushStrengthTextfield.set_text(num.ToString("0.00"));
				}
				this.m_BrushStrengthSlider.set_value(num);
				this.SetBrushStrength(num);
			}
			else
			{
				this.m_BrushStrengthTextfield.set_text(this.m_BrushStrengthSlider.get_value().ToString("0.00"));
			}
		});
		this.m_BrushStrengthTextfield.add_eventTextCancelled(delegate(UIComponent sender, string s)
		{
			this.m_BrushStrengthTextfield.set_text(this.m_BrushStrengthSlider.get_value().ToString("0.00"));
		});
	}

	private void OnDestroy()
	{
		for (int i = 0; i < BrushOptionPanel.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.Refresh();
		});
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIInput.add_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
			float sliderValue = BrushOptionPanel.GetSliderValue(this.m_BrushSizeSlider);
			if (this.SupportsSingle())
			{
				if (sliderValue == this.m_BrushSizeSlider.get_minValue())
				{
					this.m_BrushSizeTextfield.set_text(Locale.Get("TOOL_SINGLE"));
				}
				else
				{
					this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue).ToString());
				}
			}
			else
			{
				this.m_BrushSizeTextfield.set_text(Mathf.CeilToInt(sliderValue).ToString());
			}
			this.SetBrushSize(sliderValue);
			this.SetBrushStrength(this.m_BrushStrengthSlider.get_value());
			for (int i = 0; i < BrushOptionPanel.m_Extensions.Length; i++)
			{
				if (this.m_FileSystemReporter[i] != null)
				{
					this.m_FileSystemReporter[i].Start();
				}
			}
			this.Refresh();
		}
		else
		{
			UIInput.remove_eventProcessKeyEvent(new UIInput.ProcessKeyEventHandler(this.ProcessKeyEvent));
			for (int j = 0; j < BrushOptionPanel.m_Extensions.Length; j++)
			{
				if (this.m_FileSystemReporter[j] != null)
				{
					this.m_FileSystemReporter[j].Stop();
				}
			}
		}
	}

	public void ClearBrushes()
	{
		while (this.m_BrushesContainer.get_components().Count > 0)
		{
			UIComponent uIComponent = this.m_BrushesContainer.get_components()[0];
			this.m_BrushesContainer.RemoveUIComponent(uIComponent);
			Object.DestroyImmediate(uIComponent.get_gameObject());
		}
	}

	public bool AlreadyContains(List<Texture2D> list, Texture2D item)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (string.IsNullOrEmpty(item.get_name()) || string.IsNullOrEmpty(list[i].get_name()))
			{
				return false;
			}
			if (string.Compare(list[i].get_name(), item.get_name(), StringComparison.OrdinalIgnoreCase) == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void GetSortData(string str, out string name, out int number)
	{
		number = 2147483647;
		string[] array = str.Split(new char[]
		{
			'_'
		});
		name = array[0];
		int num;
		if (int.TryParse(array[array.Length - 1], out num))
		{
			number = num;
		}
	}

	private int BrushesSort(Texture2D a, Texture2D b)
	{
		string text;
		int num;
		this.GetSortData(a.get_name(), out text, out num);
		string strB;
		int value;
		this.GetSortData(b.get_name(), out strB, out value);
		int num2 = text.CompareTo(strB);
		return (num2 != 0) ? num2 : num.CompareTo(value);
	}

	private void Refresh()
	{
		this.m_ScrollPosition = this.m_BrushesContainer.get_scrollPosition();
		this.ClearBrushes();
		base.get_component().Focus();
		List<Texture2D> list = new List<Texture2D>();
		List<string> list2 = new List<string>();
		DirectoryInfo directoryInfo = new DirectoryInfo(BrushOptionPanel.brushesPath);
		if (directoryInfo.Exists)
		{
			FileInfo[] array = null;
			try
			{
				array = directoryInfo.GetFiles();
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			if (array != null)
			{
				FileInfo[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					FileInfo fileInfo = array2[i];
					for (int j = 0; j < BrushOptionPanel.m_Extensions.Length; j++)
					{
						if (string.Compare(Path.GetExtension(fileInfo.Name), BrushOptionPanel.m_Extensions[j]) == 0)
						{
							list2.Add(fileInfo.FullName);
						}
					}
				}
			}
			foreach (string current in list2)
			{
				try
				{
					Image image = new Image(current);
					Texture2D texture2D = new Texture2D(image.get_width(), image.get_height());
					texture2D.set_name(Path.GetFileNameWithoutExtension(current));
					texture2D.SetPixels(image.GetColors());
					texture2D.Apply();
					list.Add(texture2D);
				}
				catch (Exception ex2)
				{
					Debug.LogError(string.Concat(new object[]
					{
						ex2.GetType(),
						" ",
						ex2.Message,
						"\n",
						Environment.StackTrace
					}));
					UIView.ForwardException(ex2);
				}
			}
		}
		Texture2D[] builtinBrushes = this.m_BuiltinBrushes;
		for (int k = 0; k < builtinBrushes.Length; k++)
		{
			Texture2D item = builtinBrushes[k];
			if (!this.AlreadyContains(list, item))
			{
				list.Add(item);
			}
		}
		list.Sort(new Comparison<Texture2D>(this.BrushesSort));
		foreach (Texture2D current2 in list)
		{
			UIButton uIButton = this.m_BrushesContainer.AddUIComponent<UIButton>();
			uIButton.set_text(string.Empty);
			uIButton.set_normalBgSprite("BrushBackground");
			uIButton.set_disabledBgSprite("BrushBackgroundDisabled");
			uIButton.set_hoveredBgSprite("BrushBackgroundHovered");
			uIButton.set_focusedBgSprite("BrushBackgroundFocused");
			uIButton.set_pressedBgSprite("BrushBackgroundPressed");
			uIButton.set_objectUserData(current2);
			uIButton.set_tabStrip(true);
			uIButton.set_size(new Vector2((float)(BrushOptionPanel.sBrushPreviewSize + 4), (float)(BrushOptionPanel.sBrushPreviewSize + 4)));
			UITextureSprite uITextureSprite = uIButton.AddUIComponent<UITextureSprite>();
			uITextureSprite.set_pivot(4);
			uITextureSprite.set_anchor(64);
			uITextureSprite.set_anchor(128);
			uITextureSprite.set_isInteractive(false);
			uITextureSprite.set_texture(current2);
			uITextureSprite.set_size(new Vector2((float)BrushOptionPanel.sBrushPreviewSize, (float)BrushOptionPanel.sBrushPreviewSize));
		}
		this.m_BrushesContainer.set_scrollPosition(this.m_ScrollPosition);
	}

	protected int GetByIndex(UIComponent comp)
	{
		for (int i = 0; i < this.m_BrushesContainer.get_childCount(); i++)
		{
			if (this.m_BrushesContainer.get_components()[i] == comp)
			{
				return i;
			}
		}
		return -1;
	}

	public void OnMouseDown(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		int byIndex = this.GetByIndex(uIButton);
		if (byIndex != -1)
		{
			this.SelectByIndex(byIndex);
			Texture2D texture2D = uIButton.get_objectUserData() as Texture2D;
			if (uIButton.get_parent() == this.m_BrushesContainer && texture2D != null)
			{
				TerrainTool tool = ToolsModifierControl.GetTool<TerrainTool>();
				if (tool != null)
				{
					tool.m_brush = texture2D;
				}
				TreeTool tool2 = ToolsModifierControl.GetTool<TreeTool>();
				if (tool2 != null)
				{
					tool2.m_brush = texture2D;
				}
				ResourceTool tool3 = ToolsModifierControl.GetTool<ResourceTool>();
				if (tool3 != null)
				{
					tool3.m_brush = texture2D;
				}
				PropTool tool4 = ToolsModifierControl.GetTool<PropTool>();
				if (tool4 != null)
				{
					tool4.m_brush = texture2D;
				}
			}
		}
	}

	public static readonly int sBrushPreviewSize = 32;

	private static readonly float kBrushSizeInterval = 50f;

	private static readonly float kBrushStrengthInterval = 0.1f;

	private SavedInputKey m_IncreaseBrushSize;

	private SavedInputKey m_DecreaseBrushSize;

	private SavedInputKey m_IncreaseBrushStrength;

	private SavedInputKey m_DecreaseBrushStrength;

	public static string m_BrushesPath;

	private static string[] m_Extensions = Image.GetExtensions(14);

	public Texture2D[] m_BuiltinBrushes;

	private UIScrollablePanel m_BrushesContainer;

	private UISlider m_BrushSizeSlider;

	private UITextField m_BrushSizeTextfield;

	private UISlider m_BrushStrengthSlider;

	private UITextField m_BrushStrengthTextfield;

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[BrushOptionPanel.m_Extensions.Length];

	protected int m_SelectedIndex;

	private Vector2 m_ScrollPosition;

	private const float SLIDER_POWER = 2f;
}
