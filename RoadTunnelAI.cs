﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RoadTunnelAI : RoadBaseAI
{
	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Pollution || base.ColorizeProps(infoMode);
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.TrafficRoutes && infoMode != InfoManager.InfoMode.Underground && infoMode != InfoManager.InfoMode.Transport)
		{
			if (infoMode == InfoManager.InfoMode.Traffic)
			{
				return base.GetColor(segmentID, ref data, infoMode) * 0.2f;
			}
			if (infoMode != InfoManager.InfoMode.EscapeRoutes)
			{
				return base.GetColor(segmentID, ref data, infoMode);
			}
		}
		return new Color(0.2f, 0.2f, 0.2f, 1f);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.TrafficRoutes && infoMode != InfoManager.InfoMode.Underground && infoMode != InfoManager.InfoMode.Transport)
		{
			if (infoMode == InfoManager.InfoMode.Traffic)
			{
				return base.GetColor(nodeID, ref data, infoMode) * 0.2f;
			}
			if (infoMode != InfoManager.InfoMode.EscapeRoutes)
			{
				return base.GetColor(nodeID, ref data, infoMode);
			}
		}
		return new Color(0.2f, 0.2f, 0.2f, 1f);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if (this.m_info.m_flattenTerrain)
		{
			return this.m_info.m_halfWidth + 100f;
		}
		return this.m_info.m_halfWidth + 200f;
	}

	public override bool IsUnderground()
	{
		return true;
	}

	public override bool BuildUnderground()
	{
		return !this.m_info.m_flattenTerrain;
	}

	public override bool CanModify()
	{
		return this.m_canModify;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		return demolish && base.CollapseSegment(segmentID, ref data, group, demolish);
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
	}

	public override void GetTerrainModifyRange(out float start, out float end)
	{
		start = ((!this.m_info.m_flattenTerrain) ? 0f : 0.25f);
		end = 1f;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = ((!this.m_info.m_flattenTerrain) ? -3 : 0);
		max = 0;
	}

	public override bool CanIntersect(NetInfo with)
	{
		return false;
	}

	public override bool RaiseTerrain()
	{
		return true;
	}

	public override string GenerateName(ushort segmentID, ref NetSegment data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)data.m_nameSeed);
		string text = null;
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None && (!this.m_highwayRules || this.m_info.m_forwardVehicleLaneCount + this.m_info.m_backwardVehicleLaneCount >= 2))
		{
			text = "TUNNEL_NAME_PATTERN";
		}
		if (text == null)
		{
			return string.Empty;
		}
		uint num = Locale.Count(text);
		string format = Locale.Get(text, randomizer.Int32(num));
		string arg = base.GenerateStreetName(ref randomizer);
		return StringUtils.SafeFormat(format, arg);
	}

	[CustomizableProperty("Can Modify", "Properties")]
	public bool m_canModify = true;
}
