﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SaveThemePanel : LoadSavePanelBase<MapThemeMetaData>
{
	public static bool isSaving
	{
		get
		{
			return SaveThemePanel.m_IsSaving;
		}
	}

	public static string lastLoadedName
	{
		set
		{
			SaveThemePanel.m_LastSaveName = value;
		}
	}

	public static string lastLoadedAsset
	{
		set
		{
			SaveThemePanel.m_LastMapName = value;
		}
	}

	public static bool lastPublished
	{
		set
		{
			SaveThemePanel.m_LastPublished = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		SaveThemePanel.m_IsSaving = false;
		this.OnLocaleChanged();
		this.m_MapName = base.Find<UITextField>("MapName");
		this.m_SaveName = base.Find<UITextField>("SaveName");
		this.m_Publish = base.Find<UICheckBox>("Publish");
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_CurrentSnapShot = base.Find<UILabel>("CurrentSnapShot");
		this.m_FileList = base.Find<UIListBox>("SaveList");
		UIButton uIButton = base.Find<UIButton>("Previous");
		uIButton.add_eventClick(new MouseEventHandler(this.PreviousSnapshot));
		uIButton.add_eventDoubleClick(new MouseEventHandler(this.PreviousSnapshot));
		UIButton uIButton2 = base.Find<UIButton>("Next");
		uIButton2.add_eventClick(new MouseEventHandler(this.NextSnapshot));
		uIButton2.add_eventDoubleClick(new MouseEventHandler(this.NextSnapshot));
		this.m_SaveButton = base.Find<UIButton>("Save");
		this.m_SaveButton.set_isEnabled(false);
		this.m_MapName.set_text(string.Empty);
	}

	protected override void OnLocaleChanged()
	{
		SaveThemePanel.m_LastSaveName = Locale.Get("DEFAULTSAVENAMES", "NewTheme");
		SaveThemePanel.m_LastMapName = Locale.Get("DEFAULTSAVENAMES", "New Theme");
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
			if (tool != null)
			{
				string snapShotPath = tool.snapShotPath;
				if (!string.IsNullOrEmpty(snapShotPath))
				{
					for (int i = 0; i < SaveThemePanel.m_Extensions.Length; i++)
					{
						this.m_FileSystemReporter[i] = new FileSystemReporter("*" + SaveThemePanel.m_Extensions[i], snapShotPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
						if (this.m_FileSystemReporter[i] != null)
						{
							this.m_FileSystemReporter[i].Start();
						}
					}
				}
			}
			this.m_Publish.set_isChecked(SaveThemePanel.m_LastPublished);
			this.m_MapName.set_text(SaveThemePanel.m_LastMapName);
			this.m_SaveName.set_text(SaveThemePanel.m_LastSaveName);
			this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
			this.m_SaveName.SelectAll();
		}
		else
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
			}
			for (int j = 0; j < SaveThemePanel.m_Extensions.Length; j++)
			{
				if (this.m_FileSystemReporter[j] != null)
				{
					this.m_FileSystemReporter[j].Stop();
					this.m_FileSystemReporter[j].Dispose();
					this.m_FileSystemReporter[j] = null;
				}
			}
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveName.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnSave();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.FetchSnapshots();
		});
	}

	private void FetchSnapshots()
	{
		string b = (this.m_SnapshotPaths.Count <= 0) ? null : this.m_SnapshotPaths[this.m_CurrentSnapshot];
		this.m_CurrentSnapshot = 0;
		this.m_SnapshotPaths.Clear();
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null)
		{
			string snapShotPath = tool.snapShotPath;
			if (snapShotPath != null)
			{
				FileInfo[] fileInfo = SaveHelper.GetFileInfo(snapShotPath);
				if (fileInfo != null)
				{
					FileInfo[] array = fileInfo;
					for (int i = 0; i < array.Length; i++)
					{
						FileInfo fileInfo2 = array[i];
						if (string.Compare(Path.GetExtension(fileInfo2.Name), ".png", StringComparison.OrdinalIgnoreCase) == 0)
						{
							this.m_SnapshotPaths.Add(fileInfo2.FullName);
							if (fileInfo2.FullName == b)
							{
								this.m_CurrentSnapshot = this.m_SnapshotPaths.Count - 1;
							}
						}
					}
				}
				UIComponent arg_10C_0 = base.Find("Previous");
				bool isEnabled = this.m_SnapshotPaths.Count > 1;
				base.Find("Next").set_isEnabled(isEnabled);
				arg_10C_0.set_isEnabled(isEnabled);
				this.RefreshSnapshot();
			}
		}
	}

	private void RefreshSnapshot()
	{
		if (this.m_SnapShotSprite.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShotSprite.get_texture());
		}
		if (this.m_SnapshotPaths.Count > 0)
		{
			Image image = new Image(this.m_SnapshotPaths[this.m_CurrentSnapshot]);
			image.Resize(400, 224);
			this.m_SnapShotSprite.set_texture(image.CreateTexture());
			this.m_CurrentSnapShot.set_text(this.m_CurrentSnapshot + 1 + " / " + this.m_SnapshotPaths.Count);
		}
		else
		{
			this.m_SnapShotSprite.set_texture(Object.Instantiate<Texture>(this.m_DefaultThemePreviewTexture) as Texture2D);
			this.m_CurrentSnapShot.set_text(Locale.Get("NO_SNAPSHOTS"));
		}
	}

	private void PreviousSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot - 1) % this.m_SnapshotPaths.Count;
		if (this.m_CurrentSnapshot < 0)
		{
			this.m_CurrentSnapshot += this.m_SnapshotPaths.Count;
		}
		this.RefreshSnapshot();
	}

	private void NextSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot + 1) % this.m_SnapshotPaths.Count;
		this.RefreshSnapshot();
	}

	protected override void Refresh()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
		this.FetchSnapshots();
		base.ClearListing();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapThemeMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				try
				{
					MapThemeMetaData mapThemeMetaData = current.Instantiate<MapThemeMetaData>();
					mapThemeMetaData.SetSelfRef(current);
					base.AddToListing(current.get_name(), mapThemeMetaData.timeStamp, current, mapThemeMetaData, true);
				}
				catch (Exception ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
				}
			}
		}
		this.m_FileList.set_items(base.GetListingItems());
		this.m_SaveName.set_text(SaveThemePanel.m_LastSaveName);
		this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
		this.m_SaveName.SelectAll();
	}

	public void OnSaveNameChanged(UIComponent comp, string text)
	{
		this.m_FileList.set_selectedIndex(base.FindIndexOf(text));
	}

	public void OnSaveSelectionChanged(UIComponent comp, int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_SaveName.set_text(base.GetListingName(index));
			this.m_SaveName.MoveToEnd();
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.m_SaveName.set_text(base.GetListingName(sel));
			this.OnSave();
		}
	}

	private bool CheckValid()
	{
		return !StringExtensions.IsNullOrWhiteSpace(this.m_SaveName.get_text()) && !StringExtensions.IsNullOrWhiteSpace(this.m_MapName.get_text());
	}

	public void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_SaveButton.set_isEnabled(this.CheckValid());
		}
	}

	public void OnSave()
	{
		if (!this.CheckValid())
		{
			return;
		}
		string savePathName = SaveThemePanel.GetSavePathName(this.m_SaveName.get_text());
		if (!File.Exists(savePathName))
		{
			this.SaveRoutine(this.m_SaveName.get_text(), this.m_MapName.get_text(), this.m_Publish.get_isChecked() && this.m_Publish.get_isEnabled());
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_SAVEOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveRoutine(this.m_SaveName.get_text(), this.m_MapName.get_text(), this.m_Publish.get_isChecked() && this.m_Publish.get_isEnabled());
				}
			});
		}
	}

	private void SaveRoutine(string fileName, string mapName, bool publish)
	{
		SaveThemePanel.m_LastSaveName = fileName;
		SaveThemePanel.m_LastMapName = mapName;
		SaveThemePanel.m_LastPublished = publish;
		base.StartCoroutine(this.SaveMap(fileName, mapName, publish));
	}

	public void AutoSave(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SaveThemePanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Auto save requested");
				SaveThemePanel.m_IsSaving = true;
				base.StartCoroutine(this.SaveMap(savename, string.IsNullOrEmpty(SaveThemePanel.m_LastMapName) ? "AutoSavedMap" : SaveThemePanel.m_LastMapName, false));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Auto save request");
			}
		}
	}

	private Task<Package.Asset>[] SaveTextures(Package p)
	{
		return new Task<Package.Asset>[]
		{
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("GrassDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_grassDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("RuinedDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_ruinedDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("PavementDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_pavementDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("GravelDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_gravelDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("CliffDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_cliffDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("SandDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_sandDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("OilDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_oilDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("OreDiffuseTexture", Singleton<TerrainManager>.get_instance().m_properties.m_oreDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("CliffSandNormalTexture", Singleton<TerrainManager>.get_instance().m_properties.m_cliffSandNormal, false, 4, true, true)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("WaterFoam", Singleton<TerrainManager>.get_instance().m_properties.m_waterFoam, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("WaterNormal", Singleton<TerrainManager>.get_instance().m_properties.m_waterNormal, false, 4, true, true)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("MoonTexture", DayNightProperties.instance.m_MoonTexture, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("UpwardRoadDiffuse", Singleton<NetManager>.get_instance().m_properties.m_upwardDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("DownwardRoadDiffuse", Singleton<NetManager>.get_instance().m_properties.m_downwardDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("FloorDiffuse", Singleton<BuildingManager>.get_instance().m_properties.m_floorDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("BaseDiffuse", Singleton<BuildingManager>.get_instance().m_properties.m_baseDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("BaseNormal", Singleton<BuildingManager>.get_instance().m_properties.m_baseNormal, false, 4, true, true)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("BurntDiffuse", Singleton<BuildingManager>.get_instance().m_properties.m_burnedDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("AbandonedDiffuse", Singleton<BuildingManager>.get_instance().m_properties.m_abandonedDiffuse, false)),
			ThreadHelper.get_dispatcher().Dispatch<Package.Asset>(() => p.AddAsset("LightColorPalette", Singleton<BuildingManager>.get_instance().m_properties.m_lightColorPalette, false))
		};
	}

	[DebuggerHidden]
	private IEnumerator SaveMap(string saveName, string mapName, bool publish)
	{
		SaveThemePanel.<SaveMap>c__Iterator0 <SaveMap>c__Iterator = new SaveThemePanel.<SaveMap>c__Iterator0();
		<SaveMap>c__Iterator.saveName = saveName;
		<SaveMap>c__Iterator.mapName = mapName;
		<SaveMap>c__Iterator.publish = publish;
		<SaveMap>c__Iterator.$this = this;
		return <SaveMap>c__Iterator;
	}

	public void OpenSnapshotFolders()
	{
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null && !string.IsNullOrEmpty(tool.snapShotPath))
		{
			Utils.OpenInFileBrowser(tool.snapShotPath);
		}
	}

	private static string GetSavePathName(string saveName)
	{
		string path = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		return Path.Combine(DataLocation.get_mapThemesPath(), path);
	}

	private static string GetTempSavePath()
	{
		return Path.Combine(DataLocation.get_mapThemesPath(), "Temp");
	}

	public const int kPreviewWidth = 400;

	public const int kPreviewHeight = 224;

	public Texture m_DefaultThemePreviewTexture;

	public UIComponent[] m_TabFocusList;

	private static string m_LastSaveName = "NewTheme";

	private static string m_LastMapName = "New Theme";

	private static bool m_LastPublished;

	private static readonly string[] m_Extensions = Image.GetExtensions(94);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[SaveThemePanel.m_Extensions.Length];

	private List<string> m_SnapshotPaths = new List<string>();

	private UITextureSprite m_SnapShotSprite;

	private UILabel m_CurrentSnapShot;

	private int m_CurrentSnapshot;

	private UIButton m_SaveButton;

	private UITextField m_MapName;

	private UIListBox m_FileList;

	private UITextField m_SaveName;

	private UICheckBox m_Publish;

	private Task m_PackageSaveTask;

	private static volatile bool m_IsSaving;
}
