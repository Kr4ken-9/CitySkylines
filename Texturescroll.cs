﻿using System;
using UnityEngine;

public class Texturescroll : MonoBehaviour
{
	private void LateUpdate()
	{
		this.uvOffset += this.uvAnimationRate * Time.get_deltaTime();
		Renderer component = base.GetComponent<Renderer>();
		if (component.get_enabled())
		{
			for (int i = 0; i < this.textureName.Length; i++)
			{
				component.get_material().SetTextureOffset(this.textureName[i], this.uvOffset);
			}
		}
	}

	public Vector2 uvAnimationRate = new Vector2(1f, 0f);

	public string[] textureName = new string[]
	{
		"_MainTex",
		"_BumpMap"
	};

	private Vector2 uvOffset = Vector2.get_zero();
}
