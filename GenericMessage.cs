﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;

public class GenericMessage : MessageBase
{
	public GenericMessage(string senderID, string messageID, uint randomID)
	{
		this.m_messageID = messageID;
		this.m_senderID = senderID;
		this.m_randomID = randomID;
	}

	public override uint GetSenderID()
	{
		return this.m_randomID;
	}

	public override string GetSenderName()
	{
		return Locale.Get(this.m_senderID);
	}

	public override string GetText()
	{
		uint num = Locale.Count(this.m_messageID);
		Randomizer randomizer;
		randomizer..ctor(this.m_randomID);
		return Locale.Get(this.m_messageID, randomizer.Int32(num));
	}

	public override bool IsSimilarMessage(MessageBase other)
	{
		GenericMessage genericMessage = other as GenericMessage;
		return genericMessage != null && genericMessage.m_messageID == this.m_messageID;
	}

	public override void Serialize(DataSerializer s)
	{
		s.WriteSharedString(this.m_messageID);
		s.WriteSharedString(this.m_senderID);
		s.WriteUInt32(this.m_randomID);
	}

	public override void Deserialize(DataSerializer s)
	{
		this.m_messageID = s.ReadSharedString();
		if (s.get_version() >= 85u)
		{
			this.m_senderID = s.ReadSharedString();
		}
		if (s.get_version() >= 162u)
		{
			this.m_randomID = s.ReadUInt32();
		}
	}

	public override void AfterDeserialize(DataSerializer s)
	{
	}

	public string m_messageID;

	public string m_senderID;

	public uint m_randomID;
}
