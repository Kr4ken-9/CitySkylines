﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class GameAreaMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		int targetTiles = this.m_targetTiles;
		return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_GAMEAREAS_DESC"), new GeneratedString[]
		{
			new GeneratedString.Long((long)targetTiles)
		});
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int targetTiles = this.m_targetTiles;
		info.m_min = 0f;
		info.m_max = (float)targetTiles;
		int num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = (int)data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0;
		}
		if (num > targetTiles)
		{
			num = targetTiles;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_GAMEAREAS_DESC"), targetTiles);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_GAMEAREAS_PROG"), new object[]
		{
			num,
			targetTiles
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override int GetComparisonValue()
	{
		return this.m_targetTiles;
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		int targetTiles = this.m_targetTiles;
		if (forceUnlock)
		{
			data.m_progress = (long)targetTiles;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			data.m_progress = (long)Mathf.Min(targetTiles, Singleton<GameAreaManager>.get_instance().m_areaCount);
			data.m_passedCount = ((data.m_progress != (long)targetTiles) ? 0 : 1);
		}
		return data;
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32(this.m_targetTiles);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_targetTiles = s.ReadInt32();
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		int targetTiles = this.m_targetTiles;
		if (targetTiles != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)targetTiles));
		}
		return num;
	}

	public int m_targetTiles = 2;
}
