﻿using System;
using ColossalFramework.UI;

public class TextFieldOverlay : UICustomControl
{
	private void Awake()
	{
		this.m_done = base.Find<UIButton>("Done");
		this.m_cancel = base.Find<UIButton>("Cancel");
		this.m_textField = base.Find<UITextField>("TextField");
		this.m_done.add_eventClicked(new MouseEventHandler(this.OnDoneClicked));
		this.m_cancel.add_eventClicked(new MouseEventHandler(this.OnCancelClicked));
	}

	public void ShowTextField(EffectItemMessage effectItem, string text)
	{
		this.m_textField.set_text(text);
		this.m_textField.SelectAll();
		this.m_currentEffectItem = effectItem;
		base.get_component().CenterToParent();
	}

	private void OnDoneClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.m_currentEffectItem.OnTextOverLaySubmitted(this.m_textField.get_text());
	}

	private void OnCancelClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.m_currentEffectItem.OnTextOverLaySubmitted(string.Empty);
	}

	private UIButton m_done;

	private UIButton m_cancel;

	private UITextField m_textField;

	private EffectItemMessage m_currentEffectItem;
}
