﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class CargoTruckAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Connections)
		{
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			TransferManager.TransferReason transferType = (TransferManager.TransferReason)data.m_transferType;
			if (currentSubMode == InfoManager.SubInfoMode.Default && (data.m_flags & Vehicle.Flags.Importing) != (Vehicle.Flags)0 && transferType != TransferManager.TransferReason.None)
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)transferType];
			}
			if (currentSubMode == InfoManager.SubInfoMode.WaterPower && (data.m_flags & Vehicle.Flags.Exporting) != (Vehicle.Flags)0 && transferType != TransferManager.TransferReason.None)
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)transferType];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(vehicleID, ref data, infoMode);
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[4];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			ushort targetBuilding = data.m_targetBuilding;
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_RETURN");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_UNLOAD");
			}
			if (targetBuilding != 0)
			{
				Building.Flags flags = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags;
				TransferManager.TransferReason transferType = (TransferManager.TransferReason)data.m_transferType;
				if ((data.m_flags & Vehicle.Flags.Exporting) != (Vehicle.Flags)0 || (flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_EXPORT", transferType.ToString());
				}
				if ((data.m_flags & Vehicle.Flags.Importing) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					target.Building = targetBuilding;
					return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_IMPORT", transferType.ToString());
				}
				target = InstanceID.Empty;
				target.Building = targetBuilding;
				return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_DELIVER", transferType.ToString());
			}
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.Congestion) != (Vehicle.Flags)0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
			{
				this.RemoveOffers(vehicleID, ref data);
				data.m_flags &= ~Vehicle.Flags.WaitingTarget;
				data.m_flags |= Vehicle.Flags.GoingBack;
				data.m_waitCounter = 0;
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
		}
	}

	protected override void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (data.m_sourceBuilding != 0)
		{
			BuildingAI.PathFindType type = ((data.m_flags & Vehicle.Flags.DummyTraffic) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingCargo : BuildingAI.PathFindType.LeavingDummy;
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.PathfindSuccess(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], type);
		}
		if (data.m_targetBuilding != 0)
		{
			BuildingAI.PathFindType type2 = ((data.m_flags & Vehicle.Flags.DummyTraffic) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.EnteringCargo : BuildingAI.PathFindType.EnteringDummy;
			BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info2.m_buildingAI.PathfindSuccess(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], type2);
		}
		base.PathfindSuccess(vehicleID, ref data);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (data.m_sourceBuilding != 0)
		{
			BuildingAI.PathFindType type = ((data.m_flags & Vehicle.Flags.DummyTraffic) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.LeavingCargo : BuildingAI.PathFindType.LeavingDummy;
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.PathfindFailure(data.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)data.m_sourceBuilding], type);
		}
		if (data.m_targetBuilding != 0)
		{
			BuildingAI.PathFindType type2 = ((data.m_flags & Vehicle.Flags.DummyTraffic) == (Vehicle.Flags)0) ? BuildingAI.PathFindType.EnteringCargo : BuildingAI.PathFindType.EnteringDummy;
			BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info2.m_buildingAI.PathfindFailure(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], type2);
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				int num = Mathf.Min(0, (int)data.m_transferSize - this.m_cargoCapacity);
				info.m_buildingAI.ModifyMaterialBuffer(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
				num = Mathf.Max(0, -num);
				data.m_transferSize += (ushort)num;
			}
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
			if ((instance.m_buildings.m_buffer[(int)sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Importing;
				}
				else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Exporting;
				}
			}
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding == data.m_targetBuilding)
		{
			if (data.m_path == 0u)
			{
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			else
			{
				this.TrySpawn(vehicleID, ref data);
			}
		}
		else
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if (targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
				if ((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Exporting;
					}
					else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Importing;
					}
				}
			}
			else
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					if (data.m_transferSize > 0)
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = 7;
						offer.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer.Position = data.GetLastFramePosition();
						}
						offer.Amount = 1;
						offer.Active = true;
						Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
				if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					if ((int)data.m_transferSize < this.m_cargoCapacity)
					{
						TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
						offer2.Priority = 7;
						offer2.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer2.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer2.Position = data.GetLastFramePosition();
						}
						offer2.Amount = 1;
						offer2.Active = true;
						Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer2);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
			}
			if (data.m_cargoParent != 0)
			{
				if (data.m_path != 0u)
				{
					if (data.m_path != 0u)
					{
						Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
					}
					data.m_path = 0u;
				}
			}
			else if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
			else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		int num = 0;
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			num = (int)data.m_transferSize;
		}
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			num = Mathf.Min(0, (int)data.m_transferSize - this.m_cargoCapacity);
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
		info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
		}
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			data.m_transferSize += (ushort)Mathf.Max(0, -num);
		}
		if (data.m_sourceBuilding != 0 && (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.Outgoing)
		{
			BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			ushort num2 = instance.FindBuilding(instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position, 200f, info2.m_class.m_service, ItemClass.SubService.None, Building.Flags.Incoming, Building.Flags.Outgoing);
			if (num2 != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
				data.m_sourceBuilding = num2;
				instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
			}
		}
		if ((instance.m_buildings.m_buffer[(int)data.m_targetBuilding].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.Incoming)
		{
			ushort num3 = instance.FindBuilding(instance.m_buildings.m_buffer[(int)data.m_targetBuilding].m_position, 200f, info.m_class.m_service, ItemClass.SubService.None, Building.Flags.Outgoing, Building.Flags.Incoming);
			if (num3 != 0)
			{
				data.Unspawn(vehicleID);
				BuildingInfo info3 = instance.m_buildings.m_buffer[(int)num3].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 vector2;
				info3.m_buildingAI.CalculateSpawnPosition(num3, ref instance.m_buildings.m_buffer[(int)num3], ref randomizer, this.m_info, out vector, out vector2);
				Quaternion rotation = Quaternion.get_identity();
				Vector3 vector3 = vector2 - vector;
				if (vector3.get_sqrMagnitude() > 0.01f)
				{
					rotation = Quaternion.LookRotation(vector3);
				}
				data.m_frame0 = new Vehicle.Frame(vector, rotation);
				data.m_frame1 = data.m_frame0;
				data.m_frame2 = data.m_frame0;
				data.m_frame3 = data.m_frame0;
				data.m_targetPos0 = vector;
				data.m_targetPos0.w = 2f;
				data.m_targetPos1 = vector2;
				data.m_targetPos1.w = 2f;
				data.m_targetPos2 = data.m_targetPos1;
				data.m_targetPos3 = data.m_targetPos1;
				this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
				this.SetTarget(vehicleID, ref data, 0);
				return true;
			}
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		int num = 0;
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			num = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 targetPos;
				Vector3 vector;
				info.m_buildingAI.CalculateUnspawnPosition(leaderData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out targetPos, out vector);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 4f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 targetPos2;
			Vector3 vector2;
			info2.m_buildingAI.CalculateUnspawnPosition(leaderData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out targetPos2, out vector2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, 4f));
			return;
		}
	}

	protected override bool NeedChangeVehicleType(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint laneID, VehicleInfo.VehicleType laneVehicleType, ref Vector4 targetPos)
	{
		if ((laneVehicleType & VehicleInfo.VehicleType.Car) == VehicleInfo.VehicleType.None && (laneVehicleType & (VehicleInfo.VehicleType.Train | VehicleInfo.VehicleType.Ship)) != VehicleInfo.VehicleType.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			NetInfo info = instance.m_segments.m_buffer[(int)pathPos.m_segment].Info;
			byte b;
			PathUnit.CalculatePathPositionOffset(laneID, targetPos, out b);
			Vector3 pos = instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)b * 0.003921569f);
			ushort num = instance2.FindBuilding(pos, 100f, info.m_class.m_service, ItemClass.SubService.None, Building.Flags.None, Building.Flags.None);
			if (num != 0 && num != vehicleData.m_sourceBuilding)
			{
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)num].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 vector2;
				info2.m_buildingAI.CalculateUnspawnPosition(num, ref instance2.m_buildings.m_buffer[(int)num], ref randomizer, this.m_info, out vector, out vector2);
				targetPos = vector;
				targetPos.w = 4f;
			}
			return true;
		}
		return false;
	}

	protected override bool ChangeVehicleType(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint laneID)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.TransferToSource | Vehicle.Flags.GoingBack)) != (Vehicle.Flags)0)
		{
			return false;
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
		NetInfo info = instance2.m_segments.m_buffer[(int)pathPos.m_segment].Info;
		Vector3 vector = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition(0.5f);
		Vector3 pos = vector;
		if (!CargoTruckAI.SkipNonCarPaths(ref vehicleData.m_path, ref vehicleData.m_pathPositionIndex, ref vehicleData.m_lastPathOffset, ref pos))
		{
			return false;
		}
		ushort num = instance3.FindBuilding(vector, 100f, info.m_class.m_service, ItemClass.SubService.None, Building.Flags.None, Building.Flags.None);
		ushort num2 = instance3.FindBuilding(pos, 100f, info.m_class.m_service, ItemClass.SubService.None, Building.Flags.None, Building.Flags.None);
		if (num2 == num)
		{
			return true;
		}
		bool flag = false;
		if (num != 0 && (instance3.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			flag = true;
		}
		bool flag2 = false;
		if (num2 != 0 && (instance3.m_buildings.m_buffer[(int)num2].m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			flag2 = true;
		}
		ushort num3;
		if (flag && flag2 && instance.CreateVehicle(out num3, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, vector, (TransferManager.TransferReason)vehicleData.m_transferType, false, true))
		{
			if (vehicleData.m_targetBuilding != 0)
			{
				instance.m_vehicles.m_buffer[(int)num3].m_targetBuilding = vehicleData.m_targetBuilding;
				Vehicle[] expr_197_cp_0 = instance.m_vehicles.m_buffer;
				ushort expr_197_cp_1 = num3;
				expr_197_cp_0[(int)expr_197_cp_1].m_flags = (expr_197_cp_0[(int)expr_197_cp_1].m_flags & ~Vehicle.Flags.WaitingTarget);
				instance3.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].AddGuestVehicle(num3, ref instance.m_vehicles.m_buffer[(int)num3]);
			}
			instance.m_vehicles.m_buffer[(int)num3].m_transferSize = vehicleData.m_transferSize;
			instance.m_vehicles.m_buffer[(int)num3].m_path = vehicleData.m_path;
			instance.m_vehicles.m_buffer[(int)num3].m_pathPositionIndex = vehicleData.m_pathPositionIndex;
			instance.m_vehicles.m_buffer[(int)num3].m_lastPathOffset = vehicleData.m_lastPathOffset;
			Vehicle[] expr_25D_cp_0 = instance.m_vehicles.m_buffer;
			ushort expr_25D_cp_1 = num3;
			expr_25D_cp_0[(int)expr_25D_cp_1].m_flags = (expr_25D_cp_0[(int)expr_25D_cp_1].m_flags | (vehicleData.m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)));
			vehicleData.m_path = 0u;
			ushort num4 = CargoTruckAI.FindCargoParent(num, num2, info.m_class.m_service, info.m_class.m_subService);
			VehicleInfo vehicleInfo;
			if (num4 != 0)
			{
				vehicleInfo = instance.m_vehicles.m_buffer[(int)num4].Info;
			}
			else
			{
				vehicleInfo = instance.GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, info.m_class.m_service, info.m_class.m_subService, ItemClass.Level.Level4);
				if (vehicleInfo != null && instance.CreateVehicle(out num4, ref Singleton<SimulationManager>.get_instance().m_randomizer, vehicleInfo, vector, TransferManager.TransferReason.None, false, true))
				{
					vehicleInfo.m_vehicleAI.SetSource(num4, ref instance.m_vehicles.m_buffer[(int)num4], num);
					vehicleInfo.m_vehicleAI.SetTarget(num4, ref instance.m_vehicles.m_buffer[(int)num4], num2);
				}
			}
			if (num4 != 0)
			{
				int num5;
				int num6;
				vehicleInfo.m_vehicleAI.GetSize(num4, ref instance.m_vehicles.m_buffer[(int)num4], out num5, out num6);
				instance.m_vehicles.m_buffer[(int)num3].m_cargoParent = num4;
				instance.m_vehicles.m_buffer[(int)num3].m_nextCargo = instance.m_vehicles.m_buffer[(int)num4].m_firstCargo;
				instance.m_vehicles.m_buffer[(int)num4].m_firstCargo = num3;
				instance.m_vehicles.m_buffer[(int)num4].m_transferSize = (ushort)(++num5);
				if (num5 >= num6 && vehicleInfo.m_vehicleAI.CanSpawnAt(vector))
				{
					Vehicle[] expr_430_cp_0 = instance.m_vehicles.m_buffer;
					ushort expr_430_cp_1 = num4;
					expr_430_cp_0[(int)expr_430_cp_1].m_flags = (expr_430_cp_0[(int)expr_430_cp_1].m_flags & ~Vehicle.Flags.WaitingCargo);
					instance.m_vehicles.m_buffer[(int)num4].m_waitCounter = 0;
					vehicleInfo.m_vehicleAI.SetTarget(num4, ref instance.m_vehicles.m_buffer[(int)num4], num2);
				}
			}
			else
			{
				instance.ReleaseVehicle(num3);
			}
		}
		vehicleData.m_transferSize = 0;
		if (num != 0)
		{
			vehicleData.Unspawn(vehicleID);
			BuildingInfo info2 = instance3.m_buildings.m_buffer[(int)num].Info;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 vector3;
			info2.m_buildingAI.CalculateSpawnPosition(num, ref instance3.m_buildings.m_buffer[(int)num], ref randomizer, this.m_info, out vector2, out vector3);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector4 = vector3 - vector2;
			if (vector4.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector4);
			}
			vehicleData.m_frame0 = new Vehicle.Frame(vector2, rotation);
			vehicleData.m_frame1 = vehicleData.m_frame0;
			vehicleData.m_frame2 = vehicleData.m_frame0;
			vehicleData.m_frame3 = vehicleData.m_frame0;
			vehicleData.m_targetPos0 = vector2;
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = vector3;
			vehicleData.m_targetPos1.w = 2f;
			vehicleData.m_targetPos2 = vehicleData.m_targetPos1;
			vehicleData.m_targetPos3 = vehicleData.m_targetPos1;
			if (num == vehicleData.m_sourceBuilding)
			{
				this.RemoveTarget(vehicleID, ref vehicleData);
				vehicleData.m_flags &= ~Vehicle.Flags.WaitingTarget;
				vehicleData.m_flags |= Vehicle.Flags.GoingBack;
				vehicleData.m_waitCounter = 0;
				return true;
			}
		}
		else
		{
			vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
		}
		this.SetTarget(vehicleID, ref vehicleData, 0);
		return true;
	}

	public static ushort FindNextCargoParent(ushort sourceBuilding, ItemClass.Service service, ItemClass.SubService subService)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		ushort num = instance.m_buildings.m_buffer[(int)sourceBuilding].m_ownVehicles;
		ushort result = 0;
		int num2 = -1;
		int num3 = 0;
		while (num != 0)
		{
			if ((instance2.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
			{
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				if (info.m_class.m_service == service && info.m_class.m_subService == subService)
				{
					int num4;
					int num5;
					info.m_vehicleAI.GetSize(num, ref instance2.m_vehicles.m_buffer[(int)num], out num4, out num5);
					int num6 = Mathf.Max(num4 * 255 / Mathf.Max(1, num5), (int)instance2.m_vehicles.m_buffer[(int)num].m_waitCounter);
					if (num6 > num2)
					{
						result = num;
						num2 = num6;
					}
				}
			}
			num = instance2.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num3 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return result;
	}

	public static void SwitchCargoParent(ushort source, ushort target)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = instance.m_vehicles.m_buffer[(int)source].m_firstCargo;
		instance.m_vehicles.m_buffer[(int)source].m_firstCargo = 0;
		instance.m_vehicles.m_buffer[(int)target].m_firstCargo = num;
		instance.m_vehicles.m_buffer[(int)target].m_transferSize = instance.m_vehicles.m_buffer[(int)source].m_transferSize;
		int num2 = 0;
		while (num != 0)
		{
			instance.m_vehicles.m_buffer[(int)num].m_cargoParent = target;
			num = instance.m_vehicles.m_buffer[(int)num].m_nextCargo;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		instance.ReleaseVehicle(source);
	}

	private static ushort FindCargoParent(ushort sourceBuilding, ushort targetBuilding, ItemClass.Service service, ItemClass.SubService subService)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		ushort num = instance.m_buildings.m_buffer[(int)sourceBuilding].m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (instance2.m_vehicles.m_buffer[(int)num].m_targetBuilding == targetBuilding && (instance2.m_vehicles.m_buffer[(int)num].m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
			{
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				if (info.m_class.m_service == service && info.m_class.m_subService == subService)
				{
					int num3;
					int num4;
					info.m_vehicleAI.GetSize(num, ref instance2.m_vehicles.m_buffer[(int)num], out num3, out num4);
					if (num3 < num4)
					{
						return num;
					}
				}
			}
			num = instance2.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 >= 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	private static bool SkipNonCarPaths(ref uint path, ref byte pathPositionIndex, ref byte lastPathOffset, ref Vector3 lastPos)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		int num = 0;
		PathUnit.Position pathPos = default(PathUnit.Position);
		while (path != 0u)
		{
			PathUnit.Position position;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].GetPosition(pathPositionIndex >> 1, out position))
			{
				return false;
			}
			NetInfo info = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
			if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane && (info.m_lanes[(int)position.m_lane].m_vehicleType & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
			{
				if (pathPos.m_segment != 0)
				{
					uint laneID = PathManager.GetLaneID(pathPos);
					uint laneID2 = PathManager.GetLaneID(position);
					if (laneID != 0u && laneID2 != 0u)
					{
						lastPos = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition(0.5f);
						PathUnit.CalculatePathPositionOffset(laneID2, lastPos, out lastPathOffset);
						return true;
					}
				}
				return false;
			}
			pathPos = position;
			int num2 = (pathPositionIndex >> 1) + 1;
			if (num2 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)path)].m_positionCount)
			{
				num2 = 0;
				Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref path);
			}
			pathPositionIndex = (byte)(num2 << 1);
			if (++num >= 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (pathPos.m_segment != 0)
		{
			uint laneID3 = PathManager.GetLaneID(pathPos);
			if (laneID3 != 0u)
			{
				lastPos = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID3)].CalculatePosition(0.5f);
				return true;
			}
		}
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 endPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.TransferToSource | Vehicle.Flags.GoingBack)) != (Vehicle.Flags)0)
		{
			return base.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, startBothWays, endBothWays, undergroundTarget);
		}
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		bool flag = PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2);
		PathUnit.Position position;
		PathUnit.Position position2;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Train | VehicleInfo.VehicleType.Ship, allowUnderground, false, 32f, out position, out position2, out num3, out num4))
		{
			if (!flag || num3 < num)
			{
				startPosA = position;
				startPosB = position2;
				num = num3;
				num2 = num4;
			}
			flag = true;
		}
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num5;
		float num6;
		bool flag2 = PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, undergroundTarget, false, 32f, out endPosA, out endPosB, out num5, out num6);
		PathUnit.Position position3;
		PathUnit.Position position4;
		float num7;
		float num8;
		if (PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Train | VehicleInfo.VehicleType.Ship, undergroundTarget, false, 32f, out position3, out position4, out num7, out num8))
		{
			if (!flag2 || num7 < num5)
			{
				endPosA = position3;
				endPosB = position4;
				num5 = num7;
				num6 = num8;
			}
			flag2 = true;
		}
		if (flag && flag2)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num5 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			NetInfo.LaneType laneTypes = NetInfo.LaneType.Vehicle | NetInfo.LaneType.CargoVehicle;
			VehicleInfo.VehicleType vehicleTypes = VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Train | VehicleInfo.VehicleType.Ship;
			uint path;
			if (instance.CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, default(PathUnit.Position), laneTypes, vehicleTypes, 20000f, this.IsHeavyVehicle(), this.IgnoreBlocked(vehicleID, ref vehicleData), false, false, false, false, this.CombustionEngine()))
			{
				if (vehicleData.m_path != 0u)
				{
					instance.ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public override int GetNoiseLevel()
	{
		return (!this.IsHeavyVehicle()) ? 6 : 9;
	}

	protected override bool IsHeavyVehicle()
	{
		return this.m_isHeavyVehicle;
	}

	protected override bool CombustionEngine()
	{
		return true;
	}

	[CustomizableProperty("Cargo Capacity")]
	public int m_cargoCapacity = 1;

	public bool m_isHeavyVehicle;
}
