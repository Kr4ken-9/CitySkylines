﻿using System;
using UnityEngine;

public class CityNameGroups : ScriptableObject
{
	public CityNameGroups.Environment FindGroup(string name)
	{
		if (this.m_environments != null)
		{
			for (int i = 0; i < this.m_environments.Length; i++)
			{
				if (this.m_environments[i].m_name == name)
				{
					return this.m_environments[i];
				}
			}
			if (this.m_environments.Length != 0)
			{
				return this.m_environments[0];
			}
		}
		return null;
	}

	public CityNameGroups.Environment[] m_environments;

	[Serializable]
	public class Environment
	{
		public string m_name;

		public string[] m_closeDistance;

		public string[] m_mediumDistance;

		public string[] m_farDistance;
	}
}
