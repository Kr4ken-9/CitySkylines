﻿using System;

[Flags]
public enum LogChannel
{
	None = 0,
	Core = 1,
	CommandLine = 2,
	Serialization = 4,
	Editor = 8,
	HTTP = 16,
	Modding = 32,
	AssetImporter = 64,
	All = 127
}
