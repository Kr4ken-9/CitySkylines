﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class DecorationWallAI : NetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		int result = this.m_constructionCost * Mathf.RoundToInt(num / 8f + 0.1f);
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + this.m_info.m_maxHeight + 1000f;
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (maxElevation > 8f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override bool UseMinHeight()
	{
		return false;
	}

	public override bool IgnoreWater()
	{
		return true;
	}

	public override bool DisplayTempSegment()
	{
		return true;
	}

	public override bool IsCombatible(NetInfo with)
	{
		return base.IsCombatible(with);
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null && info.m_maxHeight != this.m_info.m_maxHeight)
				{
					flag = true;
				}
			}
		}
		if (flag)
		{
			data.m_flags |= NetNode.Flags.Transition;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.Transition;
		}
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		NetNode.Flags flags = instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags;
		NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags;
		if ((flags & NetNode.Flags.Transition) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingStart;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingStart;
		}
		if ((flags2 & NetNode.Flags.Transition) != NetNode.Flags.None)
		{
			data.m_flags |= NetSegment.Flags.CrossingEnd;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.CrossingEnd;
		}
		if ((flags & flags2 & NetNode.Flags.End) != NetNode.Flags.None)
		{
			data.m_flags &= ~(NetSegment.Flags.TrafficStart | NetSegment.Flags.TrafficEnd);
		}
		else
		{
			if ((flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				data.m_flags |= NetSegment.Flags.TrafficStart;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.TrafficStart;
			}
			if ((flags2 & NetNode.Flags.End) != NetNode.Flags.None)
			{
				data.m_flags |= NetSegment.Flags.TrafficEnd;
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.TrafficEnd;
			}
		}
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			ushort num = 0;
			if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
			{
				num = NetSegment.FindOwnerBuilding(segmentID, 363f);
				int num2 = 0;
				while (num != 0 && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Untouchable) != Building.Flags.None)
				{
					num = instance.m_buildings.m_buffer[(int)num].m_parentBuilding;
					if (++num2 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (num != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if ((instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Collapsed) == Building.Flags.None && !info.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, true, false, 0))
				{
					return false;
				}
			}
			Singleton<NetManager>.get_instance().ReleaseSegment(segmentID, false);
			if (num != 0)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num].Info;
				info2.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, false, false, 0);
			}
			return true;
		}
		return base.CollapseSegment(segmentID, ref data, group, demolish);
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		if (startPoint.m_node != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)startPoint.m_node].Info;
			if (info.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (startPoint.m_segment != 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)startPoint.m_segment].Info;
			if (info2.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		if (endPoint.m_node != 0)
		{
			NetInfo info3 = instance.m_nodes.m_buffer[(int)endPoint.m_node].Info;
			if (info3.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (endPoint.m_segment != 0)
		{
			NetInfo info4 = instance.m_segments.m_buffer[(int)endPoint.m_segment].Info;
			if (info4.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		Vector3 vector;
		Vector3 vector2;
		NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, true, true, out vector, out vector2);
		Bezier2 bezier;
		bezier.a = VectorUtils.XZ(startPoint.m_position);
		bezier.b = VectorUtils.XZ(vector);
		bezier.c = VectorUtils.XZ(vector2);
		bezier.d = VectorUtils.XZ(endPoint.m_position);
		int num = Mathf.CeilToInt(Vector2.Distance(bezier.a, bezier.d) * 0.005f) + 3;
		Segment2 segment;
		segment.a = bezier.a;
		for (int i = 1; i <= num; i++)
		{
			segment.b = bezier.Position((float)i / (float)num);
			if (instance2.HasWater(segment, 12f, true))
			{
				toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
			}
			segment.a = segment.b;
		}
		return toolErrors;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 255;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 255;
		return result;
	}

	public override NetSegment.Flags GetBendFlags(ushort nodeID, ref NetNode nodeData)
	{
		if ((nodeData.m_flags & NetNode.Flags.Transition) != NetNode.Flags.None)
		{
			return NetSegment.Flags.Bend | NetSegment.Flags.CrossingStart | NetSegment.Flags.CrossingEnd;
		}
		return NetSegment.Flags.Bend;
	}

	public override int GetConstructionCost()
	{
		int constructionCost = this.m_constructionCost;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref constructionCost, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return constructionCost;
	}

	public override bool CanConnect(NetInfo other)
	{
		return !other.m_hasPedestrianLanes;
	}

	[CustomizableProperty("Construction Cost", "Properties")]
	public int m_constructionCost = 1000;
}
