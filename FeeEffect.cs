﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public class FeeEffect : TriggerEffect
{
	public override void Activate()
	{
		base.Activate();
		int amount = Mathf.Clamp(this.m_moneyAmount, 0, 10000000) * 100;
		Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.FeePayment, amount, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteInt32(this.m_moneyAmount);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_moneyAmount = s.ReadInt32();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public int m_moneyAmount;
}
