﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TaxiAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport || infoMode == InfoManager.InfoMode.None)
		{
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
		{
			return base.GetColor(vehicleID, ref data, infoMode);
		}
		InstanceID empty = InstanceID.Empty;
		empty.Vehicle = vehicleID;
		if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3];
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		ushort passengerInstance = this.GetPassengerInstance(vehicleID, ref data);
		if (passengerInstance != 0)
		{
			if ((Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)passengerInstance].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
			{
				target = InstanceID.Empty;
				target.Citizen = Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)passengerInstance].m_citizen;
				return Locale.Get("VEHICLE_STATUS_TAXI_PICKINGUP");
			}
			target = InstanceID.Empty;
			target.Building = Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)passengerInstance].m_targetBuilding;
			return Locale.Get("VEHICLE_STATUS_TAXI_TRANSPORTING");
		}
		else
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_TAXI_PLANNING");
			}
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_TAXI_RETURN");
			}
			if (data.m_targetBuilding == 0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_CONFUSED");
			}
			if ((data.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				return Locale.Get("VEHICLE_STATUS_TAXI_WAIT");
			}
			target = InstanceID.Empty;
			target.Building = data.m_targetBuilding;
			return Locale.Get("VEHICLE_STATUS_TAXI_HEADING");
		}
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_BUS_STOPPED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Taxi";
		current = (int)data.m_transferSize;
		max = this.m_travelCapacity;
		if ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && current == max)
		{
			current = max - 1;
		}
	}

	public override bool GetProgressStatus(ushort vehicleID, ref Vehicle data, out float current, out float max)
	{
		current = 0f;
		max = 0f;
		return true;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_passengerCapacity, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			data.Unspawn(vehicleID);
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector2;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = data.m_targetPos0;
			data.m_targetPos2 = data.m_targetPos0;
			data.m_targetPos3 = data.m_targetPos0;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		if (targetBuilding == data.m_sourceBuilding)
		{
			data.m_transferSize = (ushort)this.m_travelCapacity;
			targetBuilding = 0;
		}
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.GoingBack;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else if ((int)data.m_transferSize < this.m_travelCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = 2;
			offer.Vehicle = vehicleID;
			offer.Position = data.GetLastFramePosition();
			offer.Amount = 1;
			offer.Active = true;
			Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			data.m_flags |= Vehicle.Flags.WaitingTarget;
		}
		else
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	private void SetCitizen(ushort vehicleID, ref Vehicle data, uint citizen)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_flags &= ~Vehicle.Flags.GoingBack;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (citizen != 0u)
		{
			ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
			if (instance2 != 0 && (instance.m_instances.m_buffer[(int)instance2].m_flags & (CitizenInstance.Flags.Character | CitizenInstance.Flags.WaitingTaxi)) == (CitizenInstance.Flags.Character | CitizenInstance.Flags.WaitingTaxi))
			{
				instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, vehicleID, 0u);
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
				return;
			}
		}
		data.m_flags |= Vehicle.Flags.GoingBack;
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	private ushort GetPassengerInstance(ushort vehicleID, ref Vehicle data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						return instance2;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == (TransferManager.TransferReason)data.m_transferType)
		{
			if (this.GetPassengerInstance(vehicleID, ref data) == 0)
			{
				if (offer.Building != 0)
				{
					this.SetTarget(vehicleID, ref data, offer.Building);
				}
				else if (offer.Citizen != 0u)
				{
					this.SetCitizen(vehicleID, ref data, offer.Citizen);
				}
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, reason, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = this.m_passengerCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			vehicleData.m_waitCounter += 1;
			if (this.CanLeave(vehicleID, ref vehicleData))
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
				vehicleData.m_flags |= Vehicle.Flags.Leaving;
				vehicleData.m_waitCounter = 0;
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if (this.GetPassengerInstance(vehicleID, ref vehicleData) == 0)
		{
			bool flag = this.ShouldReturnToSource(vehicleID, ref vehicleData);
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && flag)
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
			if ((int)vehicleData.m_transferSize < this.m_travelCapacity && !flag)
			{
				if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) == (Vehicle.Flags)0 && (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 15u) == (ulong)((long)(vehicleID & 15)))
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = ((vehicleData.m_targetBuilding == 0) ? 2 : 1);
					offer.Vehicle = vehicleID;
					offer.Position = frameData.m_position;
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Taxi, offer);
				}
				if (vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0 && vehicleData.m_blockCounter > 140)
				{
					vehicleData.m_blockCounter = 140;
				}
			}
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Vehicle = vehicleID;
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if ((data.m_flags & Vehicle.Flags.Parking) != (Vehicle.Flags)0)
		{
			data.m_flags &= ~Vehicle.Flags.Parking;
			data.m_transferSize += 1;
			this.UnloadPassengers(vehicleID, ref data, ref Singleton<TransportManager>.get_instance().m_passengers[(int)this.m_transportInfo.m_transportType]);
			if (data.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
			}
			this.SetTarget(vehicleID, ref data, 0);
		}
		else
		{
			ushort passengerInstance = this.GetPassengerInstance(vehicleID, ref data);
			if (passengerInstance == 0 || (instance.m_instances.m_buffer[(int)passengerInstance].m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
			{
				return data.m_targetBuilding == 0;
			}
			if (data.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
			}
			data.m_path = instance.m_instances.m_buffer[(int)passengerInstance].m_path;
			instance.m_instances.m_buffer[(int)passengerInstance].m_path = 0u;
			data.m_pathPositionIndex = instance.m_instances.m_buffer[(int)passengerInstance].m_pathPositionIndex;
			if (data.m_path != 0u)
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				PathManager instance3 = Singleton<PathManager>.get_instance();
				byte b = data.m_pathPositionIndex;
				if (b == 255)
				{
					b = 0;
				}
				PathUnit.Position pathPos;
				if (instance3.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].GetPosition(b >> 1, out pathPos))
				{
					uint laneID = PathManager.GetLaneID(pathPos);
					if (laneID != 0u)
					{
						Vector3 vector;
						float num;
						instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].GetClosestPosition(data.m_targetPos3, out vector, out num);
						data.m_lastPathOffset = (byte)Mathf.Clamp(Mathf.RoundToInt(num * 255f), 0, 255);
					}
				}
			}
			data.m_flags |= Vehicle.Flags.Stopped;
			CitizenInstance[] expr_1F9_cp_0 = instance.m_instances.m_buffer;
			ushort expr_1F9_cp_1 = passengerInstance;
			expr_1F9_cp_0[(int)expr_1F9_cp_1].m_flags = (expr_1F9_cp_0[(int)expr_1F9_cp_1].m_flags & ~CitizenInstance.Flags.WaitingTaxi);
			CitizenInstance[] expr_21B_cp_0 = instance.m_instances.m_buffer;
			ushort expr_21B_cp_1 = passengerInstance;
			expr_21B_cp_0[(int)expr_21B_cp_1].m_flags = (expr_21B_cp_0[(int)expr_21B_cp_1].m_flags | CitizenInstance.Flags.EnteringVehicle);
		}
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	private void UnloadPassengers(ushort vehicleID, ref Vehicle data, ref TransportPassengerData passengerData)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Vector3 lastFramePosition = data.GetLastFramePosition();
		int num = 0;
		uint num2 = data.m_citizenUnits;
		int num3 = 0;
		while (num2 != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num2)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						Vector3 lastFramePosition2 = instance.m_instances.m_buffer[(int)instance2].GetLastFramePosition();
						CitizenInfo info = instance.m_instances.m_buffer[(int)instance2].Info;
						info.m_citizenAI.SetCurrentVehicle(instance2, ref instance.m_instances.m_buffer[(int)instance2], 0, 0u, data.m_targetPos0);
						int num4 = Mathf.RoundToInt((float)this.m_pricePerKilometer * Vector3.Distance(lastFramePosition2, lastFramePosition) * 0.001f);
						if (num4 != 0)
						{
							Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PublicIncome, num4, this.m_info.m_class);
						}
						num++;
						if ((instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.Tourist) != Citizen.Flags.None)
						{
							passengerData.m_touristPassengers.m_tempCount = passengerData.m_touristPassengers.m_tempCount + 1u;
						}
						else
						{
							passengerData.m_residentPassengers.m_tempCount = passengerData.m_residentPassengers.m_tempCount + 1u;
						}
						switch (Citizen.GetAgeGroup(instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Age))
						{
						case Citizen.AgeGroup.Child:
							passengerData.m_childPassengers.m_tempCount = passengerData.m_childPassengers.m_tempCount + 1u;
							break;
						case Citizen.AgeGroup.Teen:
							passengerData.m_teenPassengers.m_tempCount = passengerData.m_teenPassengers.m_tempCount + 1u;
							break;
						case Citizen.AgeGroup.Young:
							passengerData.m_youngPassengers.m_tempCount = passengerData.m_youngPassengers.m_tempCount + 1u;
							break;
						case Citizen.AgeGroup.Adult:
							passengerData.m_adultPassengers.m_tempCount = passengerData.m_adultPassengers.m_tempCount + 1u;
							break;
						case Citizen.AgeGroup.Senior:
							passengerData.m_seniorPassengers.m_tempCount = passengerData.m_seniorPassengers.m_tempCount + 1u;
							break;
						}
					}
				}
			}
			num2 = nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticArray>(StatisticType.PassengerCount);
		statisticBase.Acquire<StatisticInt32>((int)this.m_transportInfo.m_transportType, 10).Add(num);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
				return;
			}
		}
		else if (leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 vector3;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out vector3);
			float num;
			if ((vehicleData.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0 || (index == 0 && Segment2.DistanceSqr(VectorUtils.XZ(vector3), VectorUtils.XZ(vector2), VectorUtils.XZ(refPos), ref num) < 4f))
			{
				vehicleData.m_flags |= Vehicle.Flags.WaitingCargo;
				Vector4 vector4 = base.CalculateTargetPoint(refPos, vector2, minSqrDistance, 2f);
				vector4.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector4);
				vehicleData.SetTargetPos(index++, vector4);
			}
			else
			{
				Vector4 vector5 = base.CalculateTargetPoint(refPos, vector3, minSqrDistance, 2f);
				vector5.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector5);
				vehicleData.SetTargetPos(index++, vector5);
			}
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
		{
			vehicleData.m_targetPos0 = vehicleData.GetLastFramePosition();
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
			vehicleData.m_flags &= ~Vehicle.Flags.WaitingCargo;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else
		{
			if (vehicleData.m_targetBuilding != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
				Randomizer randomizer2;
				randomizer2..ctor((int)vehicleID);
				Vector3 vector2;
				Vector3 endPos2;
				info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
			}
			CitizenManager instance3 = Singleton<CitizenManager>.get_instance();
			ushort passengerInstance = this.GetPassengerInstance(vehicleID, ref vehicleData);
			if (passengerInstance != 0)
			{
				if ((instance3.m_instances.m_buffer[(int)passengerInstance].m_flags & (CitizenInstance.Flags.Character | CitizenInstance.Flags.WaitingTaxi)) == (CitizenInstance.Flags.Character | CitizenInstance.Flags.WaitingTaxi))
				{
					Vector3 lastFramePosition = instance3.m_instances.m_buffer[(int)passengerInstance].GetLastFramePosition();
					return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, lastFramePosition, true, false, false);
				}
				ushort targetBuilding = instance3.m_instances.m_buffer[(int)passengerInstance].m_targetBuilding;
				if (targetBuilding != 0)
				{
					BuildingManager instance4 = Singleton<BuildingManager>.get_instance();
					BuildingInfo info3 = instance4.m_buildings.m_buffer[(int)targetBuilding].Info;
					Randomizer randomizer3;
					randomizer3..ctor((int)vehicleID);
					Vector3 vector3;
					Vector3 endPos3;
					info3.m_buildingAI.CalculateUnspawnPosition(targetBuilding, ref instance4.m_buildings.m_buffer[(int)targetBuilding], ref randomizer3, this.m_info, out vector3, out endPos3);
					return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos3);
				}
			}
		}
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort passengerInstance = this.GetPassengerInstance(vehicleID, ref vehicleData);
		if (passengerInstance == 0 || (instance.m_instances.m_buffer[(int)passengerInstance].m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
		{
			return base.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, startBothWays, endBothWays, undergroundTarget);
		}
		VehicleInfo info = this.m_info;
		CitizenInfo info2 = instance.m_instances.m_buffer[(int)passengerInstance].Info;
		NetInfo.LaneType laneType = NetInfo.LaneType.Vehicle | NetInfo.LaneType.Pedestrian | NetInfo.LaneType.TransportVehicle;
		VehicleInfo.VehicleType vehicleType = this.m_info.m_vehicleType;
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && info2.m_citizenAI.FindPathPosition(passengerInstance, ref instance.m_instances.m_buffer[(int)passengerInstance], endPos, laneType, vehicleType, undergroundTarget, out endPosA))
		{
			if ((instance.m_instances.m_buffer[(int)passengerInstance].m_flags & CitizenInstance.Flags.CannotUseTransport) == CitizenInstance.Flags.None)
			{
				laneType |= NetInfo.LaneType.PublicTransport;
				uint citizen = instance.m_instances.m_buffer[(int)passengerInstance].m_citizen;
				if (citizen != 0u && (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.Evacuating) != Citizen.Flags.None)
				{
					laneType |= NetInfo.LaneType.EvacuationTransport;
				}
			}
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			PathUnit.Position endPosB = default(PathUnit.Position);
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref instance2.m_randomizer, instance2.m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, laneType, vehicleType, 20000f))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		return vehicleData.m_waitCounter >= 4 && base.CanLeave(vehicleID, ref vehicleData);
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	protected override bool ParkVehicle(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint nextPath, int nextPositionIndex, out byte segmentOffset)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		segmentOffset = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1, 254);
		uint num = vehicleData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance3 != 0 && instance.AddPathReference(nextPath))
					{
						if (instance2.m_instances.m_buffer[(int)instance3].m_path != 0u)
						{
							instance.ReleasePath(instance2.m_instances.m_buffer[(int)instance3].m_path);
						}
						instance2.m_instances.m_buffer[(int)instance3].m_path = nextPath;
						instance2.m_instances.m_buffer[(int)instance3].m_pathPositionIndex = (byte)nextPositionIndex;
						instance2.m_instances.m_buffer[(int)instance3].m_lastPathOffset = segmentOffset;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return true;
	}

	public TransportInfo m_transportInfo;

	[CustomizableProperty("Passenger capacity")]
	public int m_passengerCapacity = 4;

	public int m_pricePerKilometer = 1000;

	public int m_travelCapacity = 10;
}
