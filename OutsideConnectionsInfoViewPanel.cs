﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class OutsideConnectionsInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
		this.m_TouristsTotal = base.Find<UILabel>("TouristsTotal");
		this.m_TouristWealthChart = base.Find<UIRadialChart>("TouristWealthChart");
		this.m_TotalImport = base.Find<UILabel>("ImportTotal");
		this.m_ImportChart = base.Find<UIRadialChart>("ImportChart");
		this.m_TotalExport = base.Find<UILabel>("ExportTotal");
		this.m_ExportChart = base.Find<UIRadialChart>("ExportChart");
	}

	private void SetupLegend(string legendUIName, UIRadialChart chart)
	{
		UIPanel uIPanel = base.Find<UIPanel>(legendUIName);
		UISprite uISprite = uIPanel.Find<UISprite>("OilColor");
		UISprite uISprite2 = uIPanel.Find<UISprite>("OreColor");
		UISprite uISprite3 = uIPanel.Find<UISprite>("ForestryColor");
		UISprite uISprite4 = uIPanel.Find<UISprite>("GoodsColor");
		UISprite uISprite5 = uIPanel.Find<UISprite>("AgricultureColor");
		if (Singleton<TransferManager>.get_exists())
		{
			uISprite.set_color(Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[13]);
			uISprite2.set_color(Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[14]);
			uISprite3.set_color(Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[15]);
			uISprite4.set_color(Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[17]);
			uISprite5.set_color(Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[16]);
			UIRadialChart.SliceSettings slice = chart.GetSlice(0);
			UIRadialChart.SliceSettings arg_12E_0 = slice;
			Color32 color = uISprite.get_color();
			slice.set_outterColor(color);
			arg_12E_0.set_innerColor(color);
			UIRadialChart.SliceSettings slice2 = chart.GetSlice(1);
			UIRadialChart.SliceSettings arg_151_0 = slice2;
			color = uISprite2.get_color();
			slice2.set_outterColor(color);
			arg_151_0.set_innerColor(color);
			UIRadialChart.SliceSettings slice3 = chart.GetSlice(2);
			UIRadialChart.SliceSettings arg_174_0 = slice3;
			color = uISprite3.get_color();
			slice3.set_outterColor(color);
			arg_174_0.set_innerColor(color);
			UIRadialChart.SliceSettings slice4 = chart.GetSlice(3);
			UIRadialChart.SliceSettings arg_198_0 = slice4;
			color = uISprite4.get_color();
			slice4.set_outterColor(color);
			arg_198_0.set_innerColor(color);
			UIRadialChart.SliceSettings slice5 = chart.GetSlice(4);
			UIRadialChart.SliceSettings arg_1BC_0 = slice5;
			color = uISprite5.get_color();
			slice5.set_outterColor(color);
			arg_1BC_0.set_innerColor(color);
		}
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.CeilToInt(num * 100f);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("TourismColor");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[13].m_targetColor);
				}
				this.SetupLegend("ImportLegend", this.m_ImportChart);
				this.SetupLegend("ExportLegend", this.m_ExportChart);
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		int num = (int)((instance.m_districts.m_buffer[0].m_importData.m_averageAgricultural + 99u) / 100u);
		int num2 = (int)((instance.m_districts.m_buffer[0].m_importData.m_averageForestry + 99u) / 100u);
		int num3 = (int)((instance.m_districts.m_buffer[0].m_importData.m_averageGoods + 99u) / 100u);
		int num4 = (int)((instance.m_districts.m_buffer[0].m_importData.m_averageOil + 99u) / 100u);
		int num5 = (int)((instance.m_districts.m_buffer[0].m_importData.m_averageOre + 99u) / 100u);
		int num6 = num + num2 + num3 + num4 + num5;
		this.m_TotalImport.set_text(StringUtils.SafeFormat(Locale.Get(this.m_TotalImport.get_localeID()), num6));
		this.m_ImportChart.SetValues(new int[]
		{
			OutsideConnectionsInfoViewPanel.GetValue(num4, num6),
			OutsideConnectionsInfoViewPanel.GetValue(num5, num6),
			OutsideConnectionsInfoViewPanel.GetValue(num2, num6),
			OutsideConnectionsInfoViewPanel.GetValue(num3, num6),
			OutsideConnectionsInfoViewPanel.GetValue(num, num6)
		});
		int num7 = (int)((instance.m_districts.m_buffer[0].m_exportData.m_averageAgricultural + 99u) / 100u);
		int num8 = (int)((instance.m_districts.m_buffer[0].m_exportData.m_averageForestry + 99u) / 100u);
		int num9 = (int)((instance.m_districts.m_buffer[0].m_exportData.m_averageGoods + 99u) / 100u);
		int num10 = (int)((instance.m_districts.m_buffer[0].m_exportData.m_averageOil + 99u) / 100u);
		int num11 = (int)((instance.m_districts.m_buffer[0].m_exportData.m_averageOre + 99u) / 100u);
		int num12 = num7 + num8 + num9 + num10 + num11;
		this.m_TotalExport.set_text(StringUtils.SafeFormat(Locale.Get(this.m_TotalExport.get_localeID()), num12));
		this.m_ExportChart.SetValues(new int[]
		{
			OutsideConnectionsInfoViewPanel.GetValue(num10, num12),
			OutsideConnectionsInfoViewPanel.GetValue(num11, num12),
			OutsideConnectionsInfoViewPanel.GetValue(num8, num12),
			OutsideConnectionsInfoViewPanel.GetValue(num9, num12),
			OutsideConnectionsInfoViewPanel.GetValue(num7, num12)
		});
		int averageCount = (int)instance.m_districts.m_buffer[0].m_tourist1Data.m_averageCount;
		int averageCount2 = (int)instance.m_districts.m_buffer[0].m_tourist2Data.m_averageCount;
		int averageCount3 = (int)instance.m_districts.m_buffer[0].m_tourist3Data.m_averageCount;
		int num13 = averageCount + averageCount2 + averageCount3;
		this.m_TouristsTotal.set_text(StringUtils.SafeFormat(Locale.Get(this.m_TouristsTotal.get_localeID()), num13));
		this.m_TouristWealthChart.SetValues(new int[]
		{
			OutsideConnectionsInfoViewPanel.GetValue(averageCount, num13),
			OutsideConnectionsInfoViewPanel.GetValue(averageCount2, num13),
			OutsideConnectionsInfoViewPanel.GetValue(averageCount3, num13)
		});
	}

	private UILabel m_TotalImport;

	private UIRadialChart m_ImportChart;

	private UILabel m_TotalExport;

	private UIRadialChart m_ExportChart;

	private UILabel m_TouristsTotal;

	private UIRadialChart m_TouristWealthChart;

	private bool m_initialized;
}
