﻿using System;
using ColossalFramework.UI;
using ICities;

public class UIHelper : UIHelperBase
{
	public UIHelper(UIComponent panel)
	{
		this.m_Root = panel;
	}

	public object AddCheckbox(string text, bool defaultValue, OnCheckChanged eventCallback)
	{
		if (eventCallback != null && !string.IsNullOrEmpty(text))
		{
			UICheckBox uICheckBox = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kCheckBoxTemplate)) as UICheckBox;
			uICheckBox.set_text(text);
			uICheckBox.set_isChecked(defaultValue);
			uICheckBox.add_eventCheckChanged(delegate(UIComponent c, bool isChecked)
			{
				eventCallback.Invoke(isChecked);
			});
			return uICheckBox;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create checkbox with no name or no event");
		return null;
	}

	public object AddSlider(string text, float min, float max, float step, float defaultValue, OnValueChanged eventCallback)
	{
		if (eventCallback != null && !string.IsNullOrEmpty(text))
		{
			UIPanel uIPanel = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kSliderTemplate)) as UIPanel;
			uIPanel.Find<UILabel>("Label").set_text(text);
			UISlider uISlider = uIPanel.Find<UISlider>("Slider");
			uISlider.set_minValue(min);
			uISlider.set_maxValue(max);
			uISlider.set_stepSize(step);
			uISlider.set_value(defaultValue);
			uISlider.add_eventValueChanged(delegate(UIComponent c, float val)
			{
				eventCallback.Invoke(val);
			});
			return uISlider;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create slider with no name or no event");
		return null;
	}

	public object AddDropdown(string text, string[] options, int defaultSelection, OnDropdownSelectionChanged eventCallback)
	{
		if (eventCallback != null && !string.IsNullOrEmpty(text))
		{
			UIPanel uIPanel = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kDropdownTemplate)) as UIPanel;
			uIPanel.Find<UILabel>("Label").set_text(text);
			UIDropDown uIDropDown = uIPanel.Find<UIDropDown>("Dropdown");
			uIDropDown.set_items(options);
			uIDropDown.set_selectedIndex(defaultSelection);
			uIDropDown.add_eventSelectedIndexChanged(delegate(UIComponent c, int sel)
			{
				eventCallback.Invoke(sel);
			});
			return uIDropDown;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create dropdown with no name or no event");
		return null;
	}

	public object AddTextfield(string text, string defaultContent, OnTextChanged eventChangedCallback, OnTextSubmitted eventSubmittedCallback)
	{
		if (eventChangedCallback != null && !string.IsNullOrEmpty(text))
		{
			UIPanel uIPanel = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kTextfieldTemplate)) as UIPanel;
			uIPanel.Find<UILabel>("Label").set_text(text);
			UITextField uITextField = uIPanel.Find<UITextField>("Text Field");
			uITextField.set_text(defaultContent);
			uITextField.add_eventTextChanged(delegate(UIComponent c, string sel)
			{
				eventChangedCallback.Invoke(sel);
			});
			uITextField.add_eventTextSubmitted(delegate(UIComponent c, string sel)
			{
				if (eventSubmittedCallback != null)
				{
					eventSubmittedCallback.Invoke(sel);
				}
			});
			return uITextField;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create dropdown with no name or no event");
		return null;
	}

	public UIHelperBase AddGroup(string text)
	{
		if (!string.IsNullOrEmpty(text))
		{
			UIPanel uIPanel = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kGroupTemplate)) as UIPanel;
			uIPanel.Find<UILabel>("Label").set_text(text);
			return new UIHelper(uIPanel.Find("Content"));
		}
		DebugOutputPanel.AddMessage(1, "Cannot create group with no name");
		return null;
	}

	public object AddSpace(int height)
	{
		if (height > 0)
		{
			UIPanel uIPanel = this.m_Root.AddUIComponent<UIPanel>();
			uIPanel.set_name("Space");
			uIPanel.set_isInteractive(false);
			uIPanel.set_height((float)height);
			return uIPanel;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create space of " + height + " height");
		return null;
	}

	public object AddButton(string text, OnButtonClicked eventCallback)
	{
		if (eventCallback != null && !string.IsNullOrEmpty(text))
		{
			UIButton uIButton = this.m_Root.AttachUIComponent(UITemplateManager.GetAsGameObject(UIHelper.kButtonTemplate)) as UIButton;
			uIButton.set_text(text);
			uIButton.add_eventClick(delegate(UIComponent c, UIMouseEventParameter sel)
			{
				eventCallback.Invoke();
			});
			return uIButton;
		}
		DebugOutputPanel.AddMessage(1, "Cannot create button with no name or no event");
		return null;
	}

	public object self
	{
		get
		{
			return this.m_Root;
		}
	}

	private static readonly string kCheckBoxTemplate = "OptionsCheckBoxTemplate";

	private static readonly string kSliderTemplate = "OptionsSliderTemplate";

	private static readonly string kDropdownTemplate = "OptionsDropdownTemplate";

	private static readonly string kTextfieldTemplate = "OptionsTextfieldTemplate";

	private static readonly string kButtonTemplate = "OptionsButtonTemplate";

	private static readonly string kGroupTemplate = "OptionsGroupTemplate";

	private UIComponent m_Root;
}
