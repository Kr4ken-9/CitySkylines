﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class AnimalMonumentAI : MonumentAI
{
	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (this.m_animalCount != 0 && finalProductionRate != 0 && this.CountAnimals(buildingID, ref buildingData) < this.m_animalCount)
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			position.x = Mathf.Lerp(this.m_animalArea.get_xMin(), this.m_animalArea.get_xMax(), (float)randomizer.Int32(0, 1000) * 0.001f);
			position.y = 0f;
			position.z = Mathf.Lerp(this.m_animalArea.get_yMin(), this.m_animalArea.get_yMax(), (float)randomizer.Int32(0, 1000) * 0.001f);
			position = data.CalculatePosition(position);
			float num = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num);
			target.z += Mathf.Sin(num);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			position.x = Mathf.Lerp(this.m_animalArea.get_xMin(), this.m_animalArea.get_xMax(), (float)randomizer.Int32(0, 1000) * 0.001f);
			position.y = 0f;
			position.z = Mathf.Lerp(this.m_animalArea.get_yMin(), this.m_animalArea.get_yMax(), (float)randomizer.Int32(0, 1000) * 0.001f);
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		ushort num;
		if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		int num3 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num2++;
			}
			num = nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num2;
	}

	[CustomizableProperty("Animal Count")]
	public int m_animalCount;

	[CustomizableProperty("Animal Area")]
	public Rect m_animalArea = new Rect(-10f, -10f, 20f, 20f);
}
