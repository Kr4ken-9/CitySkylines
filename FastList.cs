﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class FastList<T>
{
	[DebuggerHidden]
	public IEnumerator<T> GetEnumerator()
	{
		FastList<T>.<GetEnumerator>c__Iterator0 <GetEnumerator>c__Iterator = new FastList<T>.<GetEnumerator>c__Iterator0();
		<GetEnumerator>c__Iterator.$this = this;
		return <GetEnumerator>c__Iterator;
	}

	public T this[int i]
	{
		get
		{
			return this.m_buffer[i];
		}
		set
		{
			this.m_buffer[i] = value;
		}
	}

	public void Trim()
	{
		this.SetCapacity(this.m_size);
	}

	public void EnsureCapacity(int capacity)
	{
		if (capacity > 0 && (this.m_buffer == null || capacity > this.m_buffer.Length))
		{
			this.m_size = Mathf.Min(this.m_size, capacity);
			T[] array = new T[capacity];
			for (int i = 0; i < this.m_size; i++)
			{
				array[i] = this.m_buffer[i];
			}
			this.m_buffer = array;
		}
	}

	public void SetCapacity(int capacity)
	{
		if (capacity > 0)
		{
			if (this.m_buffer == null || capacity != this.m_buffer.Length)
			{
				this.m_size = Mathf.Min(this.m_size, capacity);
				T[] array = new T[capacity];
				for (int i = 0; i < this.m_size; i++)
				{
					array[i] = this.m_buffer[i];
				}
				this.m_buffer = array;
			}
		}
		else
		{
			this.m_buffer = null;
		}
	}

	public void Clear()
	{
		this.m_size = 0;
	}

	public void Release()
	{
		this.m_size = 0;
		this.m_buffer = null;
	}

	public void Add(T item)
	{
		if (this.m_buffer == null || this.m_size == this.m_buffer.Length)
		{
			this.SetCapacity((this.m_buffer != null) ? Mathf.Max(this.m_buffer.Length << 1, 32) : 32);
		}
		this.m_buffer[this.m_size++] = item;
	}

	public void Remove(T item)
	{
		if (this.m_buffer != null)
		{
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			for (int i = 0; i < this.m_size; i++)
			{
				if (@default.Equals(this.m_buffer[i], item))
				{
					this.m_size--;
					for (int j = i; j < this.m_size; j++)
					{
						this.m_buffer[j] = this.m_buffer[j + 1];
					}
					this.m_buffer[this.m_size] = default(T);
					return;
				}
			}
		}
	}

	public void RemoveAt(int index)
	{
		if (this.m_buffer != null && index < this.m_size)
		{
			this.m_size--;
			for (int i = index; i < this.m_size; i++)
			{
				this.m_buffer[i] = this.m_buffer[i + 1];
			}
			this.m_buffer[this.m_size] = default(T);
		}
	}

	public T[] ToArray()
	{
		this.Trim();
		return this.m_buffer;
	}

	public T[] m_buffer;

	public int m_size;
}
