﻿using System;
using System.Threading;
using ColossalFramework;
using UnityEngine;
using UnityEngine.Rendering;

public class RenderGroup
{
	public RenderGroup(int x, int z)
	{
		this.m_x = x;
		this.m_z = z;
		this.m_position.x = ((float)this.m_x + 0.5f - 22.5f) * 384f;
		this.m_position.y = 0f;
		this.m_position.z = ((float)this.m_z + 0.5f - 22.5f) * 384f;
		this.m_matrix.SetTRS(this.m_position, Quaternion.get_identity(), Vector3.get_one());
		this.m_bounds = new Bounds(this.m_position, Vector3.get_zero());
		this.m_bounds2 = this.m_bounds;
	}

	public void ReleaseGroup()
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			RenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			while (meshLayer.m_mesh != null)
			{
				if (meshLayer.m_mesh.m_mesh != null)
				{
					Object.Destroy(meshLayer.m_mesh.m_mesh);
					meshLayer.m_mesh.m_mesh = null;
				}
				meshLayer.m_mesh = meshLayer.m_mesh.m_next;
			}
			meshLayer.m_surfaceTexA = null;
			meshLayer.m_surfaceTexB = null;
			meshLayer = nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void Refresh()
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			RenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if (meshLayer.m_meshDirty)
			{
				this.UpdateMesh(meshLayer);
			}
			meshLayer = nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public bool Render(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_boundsDirty)
		{
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_bounds2 = this.m_tempBounds;
				this.m_boundsDirty = false;
			}
			finally
			{
				Monitor.Exit(this);
			}
			this.m_bounds.Encapsulate(this.m_bounds2);
			this.m_boundsDirty2 = true;
		}
		this.m_layersRendered = 0;
		this.m_instanceMask = 0;
		if (cameraInfo.Intersect(this.m_bounds))
		{
			if (this.m_boundsDirty2)
			{
				this.m_bounds = this.m_bounds2;
				this.m_boundsDirty2 = false;
			}
			RenderGroup.MeshLayer meshLayer = this.m_layers;
			int num = 0;
			while (meshLayer != null)
			{
				RenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
				if ((cameraInfo.m_layerMask & 1 << meshLayer.m_layer) != 0)
				{
					if (meshLayer.m_meshDirty)
					{
						this.UpdateMesh(meshLayer);
					}
					if (cameraInfo.Intersect(meshLayer.m_bounds))
					{
						Vector3 vector = Vector3.Max(meshLayer.m_bounds.get_min() - cameraInfo.m_position, cameraInfo.m_position - meshLayer.m_bounds.get_max());
						float sqrMagnitude = Vector3.Max(Vector3.get_zero(), vector).get_sqrMagnitude();
						if (sqrMagnitude < meshLayer.m_maxRenderDistance * meshLayer.m_maxRenderDistance)
						{
							if (sqrMagnitude < meshLayer.m_maxInstanceDistance * meshLayer.m_maxInstanceDistance)
							{
								this.m_instanceMask |= 1 << meshLayer.m_layer;
							}
							this.m_layersRendered |= 1 << meshLayer.m_layer;
						}
					}
				}
				meshLayer = nextLayer;
				if (++num >= 32)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (this.m_layersRendered != 0)
			{
				return true;
			}
		}
		return false;
	}

	public void Render(int groupMask)
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = groupMask & this.m_layersRendered & ~this.m_instanceMask;
		int num2 = 0;
		while (meshLayer != null)
		{
			RenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if ((num & 1 << meshLayer.m_layer) != 0)
			{
				this.RenderMesh(meshLayer);
			}
			meshLayer = nextLayer;
			if (++num2 >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void UpdateMesh(RenderGroup.MeshLayer layer)
	{
		RenderGroup.MeshData meshData = null;
		bool flag = false;
		bool flag2 = false;
		bool hasWaterHeightMap = false;
		while (!Monitor.TryEnter(layer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			meshData = layer.m_tempData;
			flag = layer.m_requireSurfaceMaps;
			flag2 = layer.m_requireHeightMaps;
			hasWaterHeightMap = layer.m_requireWaterHeightMaps;
			layer.m_tempData = null;
			layer.m_meshDirty = false;
			layer.m_requireSurfaceMaps = false;
			layer.m_requireHeightMaps = false;
			layer.m_requireWaterHeightMaps = false;
			layer.m_bounds = layer.m_tempBounds;
			layer.m_vertexCount = layer.m_tempVertexCount;
			layer.m_triangleCount = layer.m_tempTriangleCount;
			layer.m_objectCount = layer.m_tempObjectCount;
		}
		finally
		{
			Monitor.Exit(layer);
		}
		if (meshData == null)
		{
			while (layer.m_mesh != null)
			{
				if (layer.m_mesh.m_mesh != null)
				{
					Object.Destroy(layer.m_mesh.m_mesh);
					layer.m_mesh.m_mesh = null;
				}
				layer.m_mesh = layer.m_mesh.m_next;
			}
		}
		else
		{
			if (layer.m_mesh == null)
			{
				layer.m_mesh = new RenderGroup.MeshWrapper();
			}
			meshData.PopulateMesh(layer.m_mesh);
			Bounds bounds;
			bounds..ctor(layer.m_bounds.get_center() - this.m_position, layer.m_bounds.get_size());
			for (RenderGroup.MeshWrapper meshWrapper = layer.m_mesh; meshWrapper != null; meshWrapper = meshWrapper.m_next)
			{
				if (meshWrapper.m_mesh != null)
				{
					meshWrapper.m_mesh.set_bounds(bounds);
				}
			}
		}
		if (flag && meshData != null)
		{
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(this.m_position, out layer.m_surfaceTexA, out layer.m_surfaceTexB, out layer.m_surfaceMapping);
		}
		else
		{
			layer.m_surfaceTexA = null;
			layer.m_surfaceTexB = null;
		}
		if (flag2 && meshData != null)
		{
			Singleton<TerrainManager>.get_instance().GetHeightMapping(this.m_position, out layer.m_heightMap, out layer.m_heightMapping, out layer.m_surfaceMapping);
		}
		else
		{
			layer.m_heightMap = null;
		}
		layer.m_hasWaterHeightMap = hasWaterHeightMap;
		RenderManager instance = Singleton<RenderManager>.get_instance();
		if (layer.m_layer == instance.lightSystem.m_lightLayer)
		{
			LightSystem lightSystem = instance.lightSystem;
			layer.m_commandBuffer = lightSystem.m_lightBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = lightSystem.m_lightMaterialVolumeGroup;
			layer.m_shadows = false;
		}
		else if (layer.m_layer == instance.lightSystem.m_lightLayerFloating)
		{
			LightSystem lightSystem2 = instance.lightSystem;
			layer.m_commandBuffer = lightSystem2.m_lightBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = lightSystem2.m_lightFloatingMaterialVolumeGroup;
			layer.m_shadows = false;
		}
		else if (layer.m_layer == Singleton<NetManager>.get_instance().m_arrowLayer)
		{
			layer.m_commandBuffer = instance.m_overlayBuffer;
			layer.m_bufferMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_renderMaterial = null;
			layer.m_shadows = false;
		}
		else
		{
			layer.m_commandBuffer = null;
			layer.m_bufferMaterial = null;
			layer.m_renderMaterial = instance.m_groupLayerMaterials[layer.m_layer];
			layer.m_shadows = true;
		}
		int num = 5;
		Singleton<RenderManager>.get_instance().UpdateMegaGroup(this.m_x / num, this.m_z / num, layer.m_layer);
	}

	private void RenderMesh(RenderGroup.MeshLayer layer)
	{
		if (layer.m_mesh != null)
		{
			int num = 0;
			if (layer.m_surfaceTexA != null || layer.m_heightMap != null)
			{
				TerrainManager instance = Singleton<TerrainManager>.get_instance();
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				instance.m_materialBlock1.Clear();
				if (layer.m_surfaceTexA != null)
				{
					instance.m_materialBlock1.SetTexture(instance.ID_SurfaceTexA, layer.m_surfaceTexA);
					instance.m_materialBlock1.SetTexture(instance.ID_SurfaceTexB, layer.m_surfaceTexB);
				}
				if (layer.m_heightMap != null)
				{
					instance.m_materialBlock1.SetTexture(instance2.ID_HeightMap, layer.m_heightMap);
					instance.m_materialBlock1.SetVector(instance2.ID_HeightMapping, layer.m_heightMapping);
				}
				if (layer.m_hasWaterHeightMap)
				{
					Texture texture;
					Vector4 vector;
					Vector4 vector2;
					Singleton<TerrainManager>.get_instance().GetWaterMapping(this.m_position, out texture, out vector, out vector2);
					instance.m_materialBlock1.SetTexture(instance2.ID_WaterHeightMap, texture);
					instance.m_materialBlock1.SetVector(instance2.ID_WaterHeightMapping, vector);
					instance.m_materialBlock1.SetVector(instance2.ID_WaterSurfaceMapping, vector2);
				}
				instance.m_materialBlock1.SetVector(instance.ID_SurfaceMapping, layer.m_surfaceMapping);
				for (RenderGroup.MeshWrapper meshWrapper = layer.m_mesh; meshWrapper != null; meshWrapper = meshWrapper.m_next)
				{
					if (meshWrapper.m_mesh != null)
					{
						if (layer.m_bufferMaterial != null)
						{
							num++;
							layer.m_commandBuffer.DrawMesh(meshWrapper.m_mesh, this.m_matrix, layer.m_bufferMaterial, 0, -1, instance.m_materialBlock1);
						}
						if (layer.m_renderMaterial != null)
						{
							num++;
							Graphics.DrawMesh(meshWrapper.m_mesh, this.m_matrix, layer.m_renderMaterial, layer.m_layer, null, 0, instance.m_materialBlock1, layer.m_shadows, layer.m_shadows);
						}
					}
				}
			}
			else
			{
				for (RenderGroup.MeshWrapper meshWrapper2 = layer.m_mesh; meshWrapper2 != null; meshWrapper2 = meshWrapper2.m_next)
				{
					if (meshWrapper2.m_mesh != null)
					{
						if (layer.m_bufferMaterial != null)
						{
							num++;
							layer.m_commandBuffer.DrawMesh(meshWrapper2.m_mesh, this.m_matrix, layer.m_bufferMaterial);
						}
						if (layer.m_renderMaterial != null)
						{
							num++;
							Graphics.DrawMesh(meshWrapper2.m_mesh, this.m_matrix, layer.m_renderMaterial, layer.m_layer, null, 0, null, layer.m_shadows, layer.m_shadows);
						}
					}
				}
			}
			RenderManager instance3 = Singleton<RenderManager>.get_instance();
			RenderManager expr_29C_cp_0 = instance3;
			expr_29C_cp_0.m_drawCallData.m_lodCalls = expr_29C_cp_0.m_drawCallData.m_lodCalls + num;
			RenderManager expr_2B0_cp_0 = instance3;
			expr_2B0_cp_0.m_drawCallData.m_batchedCalls = expr_2B0_cp_0.m_drawCallData.m_batchedCalls + (layer.m_objectCount - num);
		}
	}

	public RenderGroup.MeshLayer GetLayer(int layerIndex)
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			if (meshLayer.m_layer == layerIndex)
			{
				return meshLayer;
			}
			meshLayer = meshLayer.m_nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return null;
	}

	public void SetAllLayersDirty()
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		int num = 0;
		while (meshLayer != null)
		{
			meshLayer.m_dataDirty = true;
			meshLayer = meshLayer.m_nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	public void SetLayerDataDirty(int layerIndex)
	{
		this.SetLayerDataDirty(layerIndex, true);
	}

	public void SetLayerDataDirty(int layerIndex, bool canCreate)
	{
		RenderGroup.MeshLayer meshLayer = null;
		RenderGroup.MeshLayer meshLayer2 = this.m_layers;
		int num = 0;
		while (meshLayer2 != null)
		{
			if (meshLayer2.m_layer == layerIndex)
			{
				meshLayer2.m_dataDirty = true;
				return;
			}
			meshLayer = meshLayer2;
			meshLayer2 = meshLayer2.m_nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!canCreate)
		{
			return;
		}
		meshLayer2 = new RenderGroup.MeshLayer();
		meshLayer2.m_layer = layerIndex;
		meshLayer2.m_dataDirty = true;
		if (meshLayer != null)
		{
			meshLayer.m_nextLayer = meshLayer2;
		}
		else
		{
			this.m_layers = meshLayer2;
		}
	}

	public void UpdateMeshData()
	{
		RenderGroup.MeshLayer meshLayer = this.m_layers;
		Vector3 vector;
		vector..ctor(1000000f, 1000000f, 1000000f);
		Vector3 vector2;
		vector2..ctor(-1000000f, -1000000f, -1000000f);
		int num = 0;
		while (meshLayer != null)
		{
			RenderGroup.MeshLayer nextLayer = meshLayer.m_nextLayer;
			if (meshLayer.m_dataDirty)
			{
				if (!this.UpdateMeshData(meshLayer, ref vector, ref vector2))
				{
				}
			}
			else
			{
				vector = Vector3.Min(vector, meshLayer.m_tempBounds.get_min());
				vector2 = Vector3.Max(vector2, meshLayer.m_tempBounds.get_max());
			}
			meshLayer = nextLayer;
			if (++num >= 32)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_tempBounds.SetMinMax(vector, vector2);
			this.m_boundsDirty = true;
		}
		finally
		{
			Monitor.Exit(this);
		}
	}

	private bool UpdateMeshData(RenderGroup.MeshLayer layer, ref Vector3 min, ref Vector3 max)
	{
		int num = 0;
		int num2 = 0;
		int tempObjectCount = 0;
		RenderGroup.VertexArrays vertexArrays = (RenderGroup.VertexArrays)0;
		Vector3 vector;
		vector..ctor(1000000f, 1000000f, 1000000f);
		Vector3 vector2;
		vector2..ctor(-1000000f, -1000000f, -1000000f);
		bool requireSurfaceMaps = false;
		bool requireHeightMaps = false;
		bool requireWaterHeightMaps = false;
		if (RenderManager.Managers_CalculateGroupData(this.m_x, this.m_z, layer.m_layer, ref num, ref num2, ref tempObjectCount, ref vertexArrays))
		{
			RenderGroup.MeshData meshData = null;
			if (num != 0)
			{
				meshData = new RenderGroup.MeshData(vertexArrays, num, num2);
				num = 0;
				num2 = 0;
			}
			float maxRenderDistance = 0f;
			float maxInstanceDistance = 0f;
			RenderManager.Managers_PopulateGroupData(this.m_x, this.m_z, layer.m_layer, ref num, ref num2, this.m_position, meshData, ref vector, ref vector2, ref maxRenderDistance, ref maxInstanceDistance, ref requireSurfaceMaps);
			if (layer.m_layer == Singleton<RenderManager>.get_instance().lightSystem.m_lightLayerFloating)
			{
				requireHeightMaps = true;
				requireWaterHeightMaps = true;
			}
			while (!Monitor.TryEnter(layer, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				layer.m_tempData = meshData;
				layer.m_requireSurfaceMaps = requireSurfaceMaps;
				layer.m_requireHeightMaps = requireHeightMaps;
				layer.m_requireWaterHeightMaps = requireWaterHeightMaps;
				layer.m_tempBounds.SetMinMax(vector, vector2);
				layer.m_meshDirty = true;
				layer.m_maxRenderDistance = maxRenderDistance;
				layer.m_maxInstanceDistance = maxInstanceDistance;
				layer.m_vertexArrays = vertexArrays;
				layer.m_tempVertexCount = num;
				layer.m_tempTriangleCount = num2;
				layer.m_tempObjectCount = tempObjectCount;
			}
			finally
			{
				Monitor.Exit(layer);
			}
			layer.m_dataDirty = false;
			min = Vector3.Min(min, vector);
			max = Vector3.Max(max, vector2);
			return true;
		}
		while (!Monitor.TryEnter(layer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			layer.m_tempData = null;
			layer.m_requireSurfaceMaps = false;
			layer.m_requireHeightMaps = false;
			layer.m_requireWaterHeightMaps = false;
			layer.m_tempBounds.SetMinMax(vector, vector2);
			layer.m_meshDirty = true;
			layer.m_tempVertexCount = num;
			layer.m_tempTriangleCount = num2;
			layer.m_tempObjectCount = 0;
		}
		finally
		{
			Monitor.Exit(layer);
		}
		layer.m_dataDirty = false;
		return false;
	}

	public int m_x;

	public int m_z;

	public int m_layersRendered;

	public int m_instanceMask;

	public Bounds m_bounds;

	public Bounds m_bounds2;

	public Vector3 m_position;

	public Matrix4x4 m_matrix;

	[NonSerialized]
	public RenderGroup.MeshLayer m_layers;

	private Bounds m_tempBounds;

	private bool m_boundsDirty;

	private bool m_boundsDirty2;

	[Flags]
	public enum VertexArrays
	{
		Vertices = 1,
		Normals = 2,
		Tangents = 4,
		Uvs = 8,
		Uvs2 = 16,
		Colors = 32,
		Uvs3 = 64,
		Uvs4 = 128
	}

	public class MeshWrapper
	{
		public Mesh m_mesh;

		public RenderGroup.MeshWrapper m_next;
	}

	public class MeshData
	{
		public MeshData()
		{
		}

		public MeshData(RenderGroup.VertexArrays vertexArrays, int vertexCount, int triangleCount)
		{
			if ((vertexArrays & RenderGroup.VertexArrays.Vertices) != (RenderGroup.VertexArrays)0)
			{
				this.m_vertices = new Vector3[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
			{
				this.m_normals = new Vector3[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
			{
				this.m_tangents = new Vector4[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
			{
				this.m_uvs = new Vector2[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs2) != (RenderGroup.VertexArrays)0)
			{
				this.m_uvs2 = new Vector2[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs3) != (RenderGroup.VertexArrays)0)
			{
				this.m_uvs3 = new Vector2[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs4) != (RenderGroup.VertexArrays)0)
			{
				this.m_uvs4 = new Vector2[vertexCount];
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Colors) != (RenderGroup.VertexArrays)0)
			{
				this.m_colors = new Color32[vertexCount];
			}
			this.m_triangles = new int[triangleCount];
		}

		public MeshData(Mesh mesh)
		{
			this.m_vertices = mesh.get_vertices();
			this.m_normals = mesh.get_normals();
			this.m_tangents = mesh.get_tangents();
			this.m_uvs = mesh.get_uv();
			this.m_colors = mesh.get_colors32();
			this.m_triangles = mesh.get_triangles();
		}

		public void UpdateBounds()
		{
			if (this.m_vertices != null)
			{
				Vector3 vector;
				vector..ctor(1E+07f, 1E+07f, 1E+07f);
				Vector3 vector2;
				vector2..ctor(-1E+07f, -1E+07f, -1E+07f);
				for (int i = 0; i < this.m_vertices.Length; i++)
				{
					vector = Vector3.Min(vector, this.m_vertices[i]);
					vector2 = Vector3.Max(vector2, this.m_vertices[i]);
				}
				this.m_bounds.SetMinMax(vector, vector2);
			}
		}

		public void PopulateMesh(Mesh mesh)
		{
			mesh.Clear();
			mesh.set_vertices(this.m_vertices);
			mesh.set_normals(this.m_normals);
			mesh.set_tangents(this.m_tangents);
			mesh.set_uv(this.m_uvs);
			mesh.set_uv2(this.m_uvs2);
			mesh.set_uv3(this.m_uvs3);
			mesh.set_uv4(this.m_uvs4);
			mesh.set_colors32(this.m_colors);
			mesh.set_triangles(this.m_triangles);
		}

		public void PopulateMesh(RenderGroup.MeshWrapper meshWrapper)
		{
			int num = 0;
			int num2 = Mathf.Min(64999, this.m_vertices.Length - 1);
			int num3 = 0;
			Mesh mesh;
			while (true)
			{
				mesh = meshWrapper.m_mesh;
				if (mesh == null)
				{
					mesh = new Mesh();
					meshWrapper.m_mesh = mesh;
				}
				mesh.Clear();
				if (num == 0 && num3 == 0 && num2 == this.m_vertices.Length - 1)
				{
					goto IL_426;
				}
				if (this.m_vertices != null)
				{
					Vector3[] array = new Vector3[num2 - num + 1];
					for (int i = num; i <= num2; i++)
					{
						array[i - num] = this.m_vertices[i];
					}
					mesh.set_vertices(array);
				}
				if (this.m_normals != null)
				{
					Vector3[] array2 = new Vector3[num2 - num + 1];
					for (int j = num; j <= num2; j++)
					{
						array2[j - num] = this.m_normals[j];
					}
					mesh.set_normals(array2);
				}
				if (this.m_tangents != null)
				{
					Vector4[] array3 = new Vector4[num2 - num + 1];
					for (int k = num; k <= num2; k++)
					{
						array3[k - num] = this.m_tangents[k];
					}
					mesh.set_tangents(array3);
				}
				if (this.m_uvs != null)
				{
					Vector2[] array4 = new Vector2[num2 - num + 1];
					for (int l = num; l <= num2; l++)
					{
						array4[l - num] = this.m_uvs[l];
					}
					mesh.set_uv(array4);
				}
				if (this.m_uvs2 != null)
				{
					Vector2[] array5 = new Vector2[num2 - num + 1];
					for (int m = num; m <= num2; m++)
					{
						array5[m - num] = this.m_uvs2[m];
					}
					mesh.set_uv2(array5);
				}
				if (this.m_uvs3 != null)
				{
					Vector2[] array6 = new Vector2[num2 - num + 1];
					for (int n = num; n <= num2; n++)
					{
						array6[n - num] = this.m_uvs3[n];
					}
					mesh.set_uv3(array6);
				}
				if (this.m_uvs4 != null)
				{
					Vector2[] array7 = new Vector2[num2 - num + 1];
					for (int num4 = num; num4 <= num2; num4++)
					{
						array7[num4 - num] = this.m_uvs4[num4];
					}
					mesh.set_uv4(array7);
				}
				if (this.m_colors != null)
				{
					Color32[] array8 = new Color32[num2 - num + 1];
					for (int num5 = num; num5 <= num2; num5++)
					{
						array8[num5 - num] = this.m_colors[num5];
					}
					mesh.set_colors32(array8);
				}
				int num6 = 0;
				for (int num7 = num3; num7 < this.m_triangles.Length; num7 += 3)
				{
					if (this.m_triangles[num7] > num2)
					{
						break;
					}
					if (this.m_triangles[num7 + 1] > num2)
					{
						break;
					}
					if (this.m_triangles[num7 + 2] > num2)
					{
						break;
					}
					num6 += 3;
				}
				int[] array9 = new int[num6];
				for (int num8 = 0; num8 < num6; num8++)
				{
					array9[num8] = this.m_triangles[num3 + num8] - num;
				}
				mesh.set_triangles(array9);
				num3 += num6;
				if (num3 >= this.m_triangles.Length)
				{
					break;
				}
				num = this.m_vertices.Length;
				for (int num9 = num3; num9 < this.m_triangles.Length; num9++)
				{
					if (this.m_triangles[num9] < num)
					{
						num = this.m_triangles[num9];
					}
				}
				num2 = Mathf.Min(num + 64999, this.m_vertices.Length - 1);
				if (meshWrapper.m_next == null)
				{
					meshWrapper.m_next = new RenderGroup.MeshWrapper();
				}
				meshWrapper = meshWrapper.m_next;
			}
			goto IL_4BA;
			IL_426:
			mesh.set_vertices(this.m_vertices);
			mesh.set_normals(this.m_normals);
			mesh.set_tangents(this.m_tangents);
			mesh.set_uv(this.m_uvs);
			mesh.set_uv2(this.m_uvs2);
			mesh.set_uv3(this.m_uvs3);
			mesh.set_uv4(this.m_uvs4);
			mesh.set_colors32(this.m_colors);
			mesh.set_triangles(this.m_triangles);
			IL_4BA:
			while (meshWrapper.m_next != null)
			{
				if (meshWrapper.m_next.m_mesh != null)
				{
					Object.Destroy(meshWrapper.m_next.m_mesh);
					meshWrapper.m_next.m_mesh = null;
				}
				meshWrapper.m_next = meshWrapper.m_next.m_next;
			}
		}

		public RenderGroup.VertexArrays VertexArrayMask()
		{
			RenderGroup.VertexArrays vertexArrays = RenderGroup.VertexArrays.Vertices;
			if (this.m_normals != null && this.m_normals.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Normals;
			}
			if (this.m_tangents != null && this.m_tangents.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Tangents;
			}
			if (this.m_uvs != null && this.m_uvs.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Uvs;
			}
			if (this.m_uvs2 != null && this.m_uvs2.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Uvs2;
			}
			if (this.m_uvs3 != null && this.m_uvs3.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Uvs3;
			}
			if (this.m_uvs4 != null && this.m_uvs4.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Uvs4;
			}
			if (this.m_colors != null && this.m_colors.Length != 0)
			{
				vertexArrays |= RenderGroup.VertexArrays.Colors;
			}
			return vertexArrays;
		}

		public Vector3[] m_vertices;

		public Vector3[] m_normals;

		public Vector4[] m_tangents;

		public Vector2[] m_uvs;

		public Vector2[] m_uvs2;

		public Vector2[] m_uvs3;

		public Vector2[] m_uvs4;

		public Color32[] m_colors;

		public int[] m_triangles;

		public Bounds m_bounds;
	}

	public class MeshLayer
	{
		public CommandBuffer m_commandBuffer;

		public Material m_bufferMaterial;

		public Material m_renderMaterial;

		public RenderGroup.MeshLayer m_nextLayer;

		public RenderGroup.MeshData m_tempData;

		public Bounds m_tempBounds;

		public RenderGroup.MeshWrapper m_mesh;

		public Bounds m_bounds;

		public RenderGroup.VertexArrays m_vertexArrays;

		public Texture m_surfaceTexA;

		public Texture m_surfaceTexB;

		public Texture m_heightMap;

		public Vector4 m_surfaceMapping;

		public Vector4 m_heightMapping;

		public bool m_hasWaterHeightMap;

		public float m_maxRenderDistance;

		public float m_maxInstanceDistance;

		public int m_layer;

		public int m_tempVertexCount;

		public int m_tempTriangleCount;

		public int m_tempObjectCount;

		public int m_vertexCount;

		public int m_triangleCount;

		public int m_objectCount;

		public bool m_dataDirty;

		public bool m_meshDirty;

		public bool m_requireSurfaceMaps;

		public bool m_requireHeightMaps;

		public bool m_requireWaterHeightMaps;

		public bool m_shadows;
	}
}
