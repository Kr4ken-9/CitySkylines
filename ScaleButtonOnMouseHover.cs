﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class ScaleButtonOnMouseHover : UICustomControl
{
	private void Awake()
	{
		this.m_Component = base.GetComponent<UIButton>();
		this.m_Size = this.m_Component.get_size();
	}

	public void OnMouseEnter(UIComponent component, UIMouseEventParameter p)
	{
		ValueAnimator.Animate(base.GetInstanceID().ToString(), delegate(float val)
		{
			this.m_Component.set_size(this.m_Size * val);
		}, new AnimatedFloat(1f, 1.2f, this.m_InterpTime, this.m_EasingType));
	}

	public void OnMouseLeave(UIComponent component, UIMouseEventParameter p)
	{
		ValueAnimator.Animate(base.GetInstanceID().ToString(), delegate(float val)
		{
			this.m_Component.set_size(this.m_Size * val);
		}, new AnimatedFloat(1.2f, 1f, this.m_InterpTime, this.m_EasingType));
	}

	private UIButton m_Component;

	private float m_InterpTime = 0.3f;

	private EasingType m_EasingType = 27;

	private Vector2 m_Size;
}
