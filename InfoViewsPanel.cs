﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class InfoViewsPanel : ToolsModifierControl
{
	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	public int selectedIndex
	{
		get
		{
			return this.m_SelectedIndex;
		}
		set
		{
			if (value != this.m_SelectedIndex)
			{
				this.SelectByIndex(value, true);
			}
		}
	}

	protected virtual void ShowSelectedIndex()
	{
		if (this.selectedIndex >= 0 && this.selectedIndex <= this.m_ChildContainer.get_childCount() - 1)
		{
			UIButton uIButton = this.m_ChildContainer.get_components()[this.selectedIndex] as UIButton;
			if (uIButton != null)
			{
				uIButton.set_state(1);
			}
		}
	}

	protected void SelectByIndex(int value, bool focusAdvisor)
	{
		value = Mathf.Max(Mathf.Min(value, this.m_ChildContainer.get_childCount() - 1), -1);
		if (focusAdvisor)
		{
			this.SetAdvisorFocus(value);
		}
		if (value == this.m_SelectedIndex)
		{
			return;
		}
		this.m_SelectedIndex = value;
		for (int i = 0; i < this.m_ChildContainer.get_childCount(); i++)
		{
			UIButton uIButton = this.m_ChildContainer.get_components()[i] as UIButton;
			if (!(uIButton == null))
			{
				if (i == value)
				{
					uIButton.set_state(1);
				}
				else
				{
					uIButton.set_state(0);
				}
			}
		}
	}

	protected void OnMouseUp(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_ChildContainer.get_components().Contains(p.get_source()))
		{
			p.get_source().Unfocus();
		}
	}

	private void SetAdvisorFocus(int idx)
	{
		if (idx >= 0 && idx < this.m_ChildContainer.get_childCount())
		{
			this.m_AdvisorFocus = this.m_ChildContainer.get_components()[idx];
		}
		else
		{
			this.m_AdvisorFocus = null;
		}
		this.UpdateAdvisor();
	}

	private void UpdateAdvisor()
	{
		if (ToolsModifierControl.advisorPanel != null)
		{
			if (this.m_AdvisorFocus != null)
			{
				string text = "InfoView" + this.m_AdvisorFocus.get_name();
				string icon = "ToolbarIconZoomOutCity";
				string spriteName = TutorialAdvisorPanel.GetSpriteName(text);
				if (this.m_AdvisorFocus is UIButton)
				{
					icon = ((UIButton)this.m_AdvisorFocus).get_normalFgSprite();
				}
				SavedBool savedBool = new SavedBool(text, Settings.userGameState, true);
				if (savedBool)
				{
					ToolsModifierControl.advisorPanel.Show(text, icon, spriteName, 0f, false, true);
					savedBool.set_value(false);
				}
				else
				{
					ToolsModifierControl.advisorPanel.Refresh(text, icon, spriteName);
				}
			}
			else
			{
				ToolsModifierControl.advisorPanel.Refresh(null);
			}
		}
	}

	protected void OnEnable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.RefreshPanel);
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.RefreshPanel));
	}

	protected void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.RefreshPanel);
		}
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.RefreshPanel));
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.RefreshPanel();
	}

	public void CleanPanel()
	{
		this.m_ObjectIndex = 0;
	}

	public virtual void RefreshPanel()
	{
		this.CleanPanel();
	}

	protected UIButton SpawnButtonEntry(string name, string spriteBase, string localeID, int index, bool enabled)
	{
		UIButton uIButton;
		if (this.m_ChildContainer.get_childCount() > this.m_ObjectIndex)
		{
			uIButton = (this.m_ChildContainer.get_components()[this.m_ObjectIndex] as UIButton);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(InfoViewsPanel.kInfoViewButtonTemplate);
			uIButton = (this.m_ChildContainer.AttachUIComponent(asGameObject) as UIButton);
		}
		TutorialUITag tutorialUITag = uIButton.get_gameObject().GetComponent<TutorialUITag>();
		if (tutorialUITag == null)
		{
			tutorialUITag = uIButton.get_gameObject().AddComponent<TutorialUITag>();
		}
		tutorialUITag.tutorialTag = "Info" + name;
		tutorialUITag.m_ParentOverride = this.m_ParentButton;
		uIButton.set_pivot(1);
		uIButton.set_atlas(this.m_Atlas);
		uIButton.set_text(string.Empty);
		uIButton.set_playAudioEvents(true);
		uIButton.set_name(name);
		uIButton.set_tooltipAnchor(2);
		uIButton.set_tabStrip(true);
		uIButton.set_horizontalAlignment(1);
		uIButton.set_verticalAlignment(1);
		uIButton.set_zOrder(index);
		uIButton.set_isEnabled(enabled);
		string text = spriteBase + name;
		uIButton.set_normalFgSprite(text);
		uIButton.set_focusedFgSprite(text + "Focused");
		uIButton.set_hoveredFgSprite(text + "Hovered");
		uIButton.set_pressedFgSprite(text + "Pressed");
		uIButton.set_disabledFgSprite(text + "Disabled");
		uIButton.set_tooltip(Locale.Get(localeID, name));
		uIButton.set_group(base.get_component());
		this.m_ObjectIndex++;
		return uIButton;
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (!visible && (ToolsModifierControl.policiesPanel == null || !ToolsModifierControl.policiesPanel.isVisible))
		{
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
			}
			this.selectedIndex = -1;
		}
	}

	protected virtual void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		if (uIButton != null && uIButton.get_parent() == this.m_ChildContainer)
		{
			int num = uIButton.get_zOrder();
			if (num == this.selectedIndex)
			{
				num = -1;
				uIButton.Unfocus();
			}
			this.OnButtonClicked(num);
			this.SelectByIndex(num, true);
		}
	}

	protected virtual void OnButtonClicked(int index)
	{
	}

	private void Update()
	{
		this.ShowSelectedIndex();
	}

	private void Awake()
	{
		this.RefreshPanel();
	}

	private static readonly string kInfoViewButtonTemplate = "InfoViewButtonTemplate";

	public UITextureAtlas m_Atlas;

	public UIComponent m_ChildContainer;

	public UIComponent m_ParentButton;

	private int m_ObjectIndex;

	protected int m_SelectedIndex = -1;

	private UIComponent m_AdvisorFocus;
}
