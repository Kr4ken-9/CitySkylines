﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SaveScenarioPanel : LoadSavePanelBase<ScenarioMetaData>
{
	public static string lastLoadedName
	{
		set
		{
			SaveScenarioPanel.m_LastSaveName = value;
		}
	}

	public static bool isSaving
	{
		get
		{
			return SaveScenarioPanel.m_IsSaving;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		SaveScenarioPanel.m_IsSaving = false;
		this.OnLocaleChanged();
		this.m_SaveName = base.Find<UITextField>("SaveName");
		this.m_FileList = base.Find<UIListBox>("SaveList");
		this.m_SnapShotSprite = base.Find<UITextureSprite>("SnapShot");
		this.m_CurrentSnapShot = base.Find<UILabel>("CurrentSnapShot");
		this.m_SaveButton = base.Find<UIButton>("Save");
		this.m_SaveButton.set_isEnabled(false);
		this.m_Publish = base.Find<UICheckBox>("Publish");
		this.m_ScenarioName = base.Find<UITextField>("ScenarioName");
		this.m_ScenarioDescr = base.Find<UITextField>("ScenarioDescr");
		this.m_CityName = base.Find<UITextField>("CityName");
		UIButton uIButton = base.Find<UIButton>("Previous");
		uIButton.add_eventClick(new MouseEventHandler(this.PreviousSnapshot));
		uIButton.add_eventDoubleClick(new MouseEventHandler(this.PreviousSnapshot));
		UIButton uIButton2 = base.Find<UIButton>("Next");
		uIButton2.add_eventClick(new MouseEventHandler(this.NextSnapshot));
		uIButton2.add_eventDoubleClick(new MouseEventHandler(this.NextSnapshot));
	}

	protected override void OnLocaleChanged()
	{
		if (string.IsNullOrEmpty(SaveScenarioPanel.m_LastSaveName))
		{
			SaveScenarioPanel.m_LastSaveName = Locale.Get("DEFAULTSAVENAMES", "NewSave");
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Pause();
			}
			this.Refresh();
			base.get_component().CenterToParent();
			string text = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			if (text == null)
			{
				text = "Unknown";
			}
			this.m_CityName.set_text(text);
		}
		else if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().autoSaveTimer.UnPause();
		}
	}

	private void FetchSnapshots()
	{
		string b = (this.m_SnapshotPaths.Count <= 0) ? null : this.m_SnapshotPaths[this.m_CurrentSnapshot];
		this.m_CurrentSnapshot = 0;
		this.m_SnapshotPaths.Clear();
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null)
		{
			string snapShotPath = tool.snapShotPath;
			if (snapShotPath != null)
			{
				FileInfo[] fileInfo = SaveHelper.GetFileInfo(snapShotPath);
				if (fileInfo != null)
				{
					FileInfo[] array = fileInfo;
					for (int i = 0; i < array.Length; i++)
					{
						FileInfo fileInfo2 = array[i];
						if (string.Compare(Path.GetExtension(fileInfo2.Name), ".png", StringComparison.OrdinalIgnoreCase) == 0)
						{
							this.m_SnapshotPaths.Add(fileInfo2.FullName);
							if (fileInfo2.FullName == b)
							{
								this.m_CurrentSnapshot = this.m_SnapshotPaths.Count - 1;
							}
						}
					}
				}
				UIComponent arg_10C_0 = base.Find("Previous");
				bool isEnabled = this.m_SnapshotPaths.Count > 1;
				base.Find("Next").set_isEnabled(isEnabled);
				arg_10C_0.set_isEnabled(isEnabled);
				this.RefreshSnapshot();
			}
		}
	}

	private void RefreshSnapshot()
	{
		if (this.m_SnapShotSprite.get_texture() != null)
		{
			Object.Destroy(this.m_SnapShotSprite.get_texture());
		}
		if (this.m_SnapshotPaths.Count > 0)
		{
			Image image = new Image(this.m_SnapshotPaths[this.m_CurrentSnapshot]);
			image.Resize(400, 224);
			this.m_SnapShotSprite.set_texture(image.CreateTexture());
			this.m_CurrentSnapShot.set_text(this.m_CurrentSnapshot + 1 + " / " + this.m_SnapshotPaths.Count);
		}
		else
		{
			this.m_SnapShotSprite.set_texture(Object.Instantiate<Texture>(this.m_DefaultMapPreviewTexture) as Texture2D);
			this.m_CurrentSnapShot.set_text(Locale.Get("NO_SNAPSHOTS"));
		}
	}

	private void PreviousSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot - 1) % this.m_SnapshotPaths.Count;
		if (this.m_CurrentSnapshot < 0)
		{
			this.m_CurrentSnapshot += this.m_SnapshotPaths.Count;
		}
		this.RefreshSnapshot();
	}

	private void NextSnapshot(UIComponent c, UIMouseEventParameter p)
	{
		this.m_CurrentSnapshot = (this.m_CurrentSnapshot + 1) % this.m_SnapshotPaths.Count;
		this.RefreshSnapshot();
	}

	public void OpenSnapshotFolders()
	{
		SnapshotTool tool = ToolsModifierControl.GetTool<SnapshotTool>();
		if (tool != null && !string.IsNullOrEmpty(tool.snapShotPath))
		{
			Utils.OpenInFileBrowser(tool.snapShotPath);
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_SaveName.Focus();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnSave();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
		}
	}

	protected override void Refresh()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
		this.FetchSnapshots();
		base.ClearListing();
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.ScenarioMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				try
				{
					ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
					if (!scenarioMetaData.IsBuiltinScenario(current.get_package().get_packagePath()))
					{
						base.AddToListing(current.get_name(), scenarioMetaData.timeStamp, current, scenarioMetaData, true);
					}
				}
				catch (Exception ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
				}
			}
		}
		this.m_FileList.set_items(base.GetListingItems());
		this.m_SaveName.set_text(SaveScenarioPanel.m_LastSaveName);
		this.m_ScenarioName.set_text(SaveScenarioPanel.m_LastScenarioName);
		this.m_ScenarioDescr.set_text(SaveScenarioPanel.m_LastScenarioDescription);
		if (this.m_SaveName.get_text() != "NewScenario")
		{
			this.m_FileList.set_selectedIndex(base.FindIndexOf(this.m_SaveName.get_text()));
			this.m_SaveName.SelectAll();
			this.UpdateFromSelected(this.m_FileList.get_selectedIndex());
		}
	}

	public void OnSaveNameChanged(UIComponent comp, string text)
	{
		this.m_FileList.set_selectedIndex(base.FindIndexOf(text));
	}

	public void OnSaveSelectionChanged(UIComponent comp, int index)
	{
		this.UpdateFromSelected(index);
	}

	private void UpdateFromSelected(int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_SaveName.set_text(base.GetListingName(index));
			this.m_SaveName.MoveToEnd();
			ScenarioMetaData listingMetaData = base.GetListingMetaData(index);
			this.m_ScenarioName.set_text(listingMetaData.scenarioName);
			this.m_ScenarioDescr.set_text(listingMetaData.scenarioDescription);
			this.m_CityName.set_text(listingMetaData.cityName);
			this.m_Publish.set_isChecked(listingMetaData.isPublished);
		}
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.m_SaveName.set_text(base.GetListingName(sel));
			this.OnSave();
		}
	}

	public void AutoSave(string savename)
	{
		if (Singleton<LoadingManager>.get_exists() && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (!SaveScenarioPanel.m_IsSaving)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Auto save requested");
				SaveScenarioPanel.m_IsSaving = true;
				base.StartCoroutine(this.SaveGame(savename, SaveScenarioPanel.m_LastScenarioName, SaveScenarioPanel.m_LastScenarioDescription, false, null, null));
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Save already in progress. Ignoring Auto save request");
			}
		}
	}

	private bool CheckValid()
	{
		return !StringExtensions.IsNullOrWhiteSpace(this.m_SaveName.get_text()) && !StringExtensions.IsNullOrWhiteSpace(this.m_ScenarioName.get_text());
	}

	public void Update()
	{
		if (base.get_component().get_isVisible())
		{
			this.m_SaveButton.set_isEnabled(this.CheckValid());
		}
	}

	public void OnSave()
	{
		if (!this.CheckValid())
		{
			return;
		}
		string savePathName = SaveScenarioPanel.GetSavePathName(this.m_SaveName.get_text(), false);
		if (!File.Exists(savePathName))
		{
			this.CheckForDisastersWithoutTriggers();
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_SAVEOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					base.Invoke("CheckForDisastersWithoutTriggers", 0.1f);
				}
			});
		}
	}

	private void CheckForDisastersWithoutTriggers()
	{
		if (this.m_Publish.get_isChecked() && this.DisastersWithoutTriggersExist())
		{
			ConfirmPanel.ShowModal("CONFIRM_TRIGGERWARNING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveRoutine(this.m_SaveName.get_text(), this.m_ScenarioName.get_text(), this.m_ScenarioDescr.get_text(), this.m_Publish.get_isChecked());
				}
			});
		}
		else
		{
			this.SaveRoutine(this.m_SaveName.get_text(), this.m_ScenarioName.get_text(), this.m_ScenarioDescr.get_text(), this.m_Publish.get_isChecked());
		}
	}

	private bool DisastersWithoutTriggersExist()
	{
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		ushort num = 1;
		while ((int)num < instance.m_disasters.m_size)
		{
			if ((instance.m_disasters.m_buffer[(int)num].m_flags & DisasterData.Flags.Created) != DisasterData.Flags.None && DisasterWorldInfoPanel.CountTriggers(num) == 0)
			{
				return true;
			}
			num += 1;
		}
		return false;
	}

	private void SaveRoutine(string fileName, string scenarioName, string scenarioDescr, bool publish)
	{
		SaveScenarioPanel.m_LastSaveName = fileName;
		SaveScenarioPanel.m_LastScenarioName = scenarioName;
		SaveScenarioPanel.m_LastScenarioDescription = scenarioDescr;
		byte[] winConditions = this.GenerateConditions(true);
		byte[] losingConditions = this.GenerateConditions(false);
		base.StartCoroutine(this.SaveGame(fileName, scenarioName, scenarioDescr, publish, winConditions, losingConditions));
	}

	[DebuggerHidden]
	private IEnumerator SaveGame(string saveName, string scenarioName, string scenarioDescr, bool publish, byte[] winConditions, byte[] losingConditions)
	{
		SaveScenarioPanel.<SaveGame>c__Iterator0 <SaveGame>c__Iterator = new SaveScenarioPanel.<SaveGame>c__Iterator0();
		<SaveGame>c__Iterator.saveName = saveName;
		<SaveGame>c__Iterator.publish = publish;
		<SaveGame>c__Iterator.winConditions = winConditions;
		<SaveGame>c__Iterator.losingConditions = losingConditions;
		<SaveGame>c__Iterator.scenarioName = scenarioName;
		<SaveGame>c__Iterator.scenarioDescr = scenarioDescr;
		<SaveGame>c__Iterator.$this = this;
		return <SaveGame>c__Iterator;
	}

	private static string GetSavePathName(string saveName, bool useCloud)
	{
		string text = PathUtils.AddExtension(PathEscaper.Escape(saveName), PackageManager.packageExtension);
		if (useCloud)
		{
			return text;
		}
		return Path.Combine(DataLocation.get_scenarioLocation(), text);
	}

	private static string GetTempSavePath()
	{
		return Path.Combine(DataLocation.get_scenarioLocation(), "Temp");
	}

	private byte[] GenerateConditions(bool winConditions)
	{
		List<GeneratedString> list = new List<GeneratedString>();
		if (Singleton<UnlockManager>.get_instance().m_scenarioTriggers != null)
		{
			TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
			for (int i = 0; i < scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = scenarioTriggers[i];
				if (triggerMilestone.m_effects != null)
				{
					TriggerEffect[] effects = triggerMilestone.m_effects;
					for (int j = 0; j < effects.Length; j++)
					{
						TriggerEffect triggerEffect = effects[j];
						bool flag = false;
						if (winConditions)
						{
							WinEffect winEffect = triggerEffect as WinEffect;
							if (winEffect != null)
							{
								flag = true;
							}
						}
						else
						{
							LoseEffect loseEffect = triggerEffect as LoseEffect;
							if (loseEffect != null)
							{
								flag = true;
							}
						}
						if (flag)
						{
							if (list.Count > 0)
							{
								list.Add(new GeneratedString.Locale("WINLOSECONDITIONS_OR"));
							}
							this.AddConditions(triggerMilestone, list);
						}
					}
				}
			}
		}
		GeneratedString[] obj = list.ToArray();
		return GeneratedString.SerializeArrayToByteArray(obj);
	}

	private void AddConditions(TriggerMilestone triggerMilestone, List<GeneratedString> description)
	{
		MilestoneInfo[] conditions = triggerMilestone.m_conditions;
		for (int i = 0; i < conditions.Length; i++)
		{
			MilestoneInfo milestoneInfo = conditions[i];
			TriggerMilestone triggerMilestone2 = milestoneInfo as TriggerMilestone;
			if (triggerMilestone2 != null)
			{
				this.AddConditions(triggerMilestone2, description);
			}
			else
			{
				GeneratedString description2 = milestoneInfo.GetDescription();
				if (description2 != null)
				{
					description.Add(description2);
				}
			}
		}
	}

	public const int kPreviewWidth = 400;

	public const int kPreviewHeight = 224;

	public UIComponent[] m_TabFocusList;

	public Texture m_DefaultMapPreviewTexture;

	public static string m_LastSaveName = "NewScenario";

	public static string m_LastScenarioName = "New Scenario";

	public static string m_LastScenarioDescription = string.Empty;

	private UIListBox m_FileList;

	private UITextField m_SaveName;

	private List<string> m_SnapshotPaths = new List<string>();

	private UITextureSprite m_SnapShotSprite;

	private UILabel m_CurrentSnapShot;

	private int m_CurrentSnapshot;

	private UIButton m_SaveButton;

	private Task m_PackageSaveTask;

	private object m_ImagesLock = new object();

	private static volatile bool m_IsSaving;

	private UICheckBox m_Publish;

	private UITextField m_ScenarioName;

	private UITextField m_ScenarioDescr;

	private UITextField m_CityName;
}
