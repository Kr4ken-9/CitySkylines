﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class WorldInfoPanel : ToolsModifierControl
{
	protected bool IsResidentialZone(ItemClass.Zone zoneType)
	{
		return zoneType == ItemClass.Zone.ResidentialLow || zoneType == ItemClass.Zone.ResidentialHigh;
	}

	protected bool IsIndustrialZone(ItemClass.Zone zoneType)
	{
		return zoneType == ItemClass.Zone.Industrial;
	}

	protected bool IsCommercialZone(ItemClass.Zone zoneType)
	{
		return zoneType == ItemClass.Zone.CommercialLow || zoneType == ItemClass.Zone.CommercialHigh;
	}

	protected bool IsOfficeZone(ItemClass.Zone zoneType)
	{
		return zoneType == ItemClass.Zone.Office;
	}

	private static IEnumerable<Type> GetEnumerableOfType<OutType>() where OutType : class
	{
		return from t in Assembly.GetAssembly(typeof(OutType)).GetTypes()
		where t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(OutType))
		select t;
	}

	protected bool isRoutesViewOn
	{
		get
		{
			return Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default;
		}
	}

	protected virtual void Start()
	{
		WorldInfoPanel.m_LastVisibleInfoPanel = null;
		if (!this.m_IsEmbbeded)
		{
			this.m_FullscreenContainer = UIView.Find("FullScreenContainer");
			this.m_FullscreenContainer.AttachUIComponent(base.get_gameObject());
			WorldInfoPanel.m_WorldInfoPanels = WorldInfoPanel.GetEnumerableOfType<WorldInfoPanel>();
			base.get_component().set_zOrder(0);
		}
		this.m_LocationMarker = base.Find<UIMultiStateButton>("LocationMarker");
		if (this.m_LocationMarker != null)
		{
			this.m_LocationMarker.add_eventClick(new MouseEventHandler(this.OnLocationMarkerClicked));
		}
		this.m_ShowHideRoutesButton = base.Find<UIMultiStateButton>("ShowHideRoutesButton");
		if (this.m_ShowHideRoutesButton != null)
		{
			this.m_ShowHideRoutesButton.add_eventClick(new MouseEventHandler(this.OnShowHideRoutesButtonClicked));
			this.RefreshShowHideButtonTooltip();
		}
		if (Camera.get_main() != null)
		{
			this.m_CameraTransform = Camera.get_main().get_transform();
		}
	}

	private void OnShowHideRoutesButtonClicked(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.isRoutesViewOn)
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		}
		else
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.TrafficRoutes, InfoManager.SubInfoMode.Default);
		}
	}

	protected virtual void RefreshShowHideButtonTooltip()
	{
		this.m_ShowHideRoutesButton.set_tooltip(Locale.Get("INFOPANEL_TOGGLETRAFFICRROUTESVIEW"));
	}

	private void OnLocationMarkerClicked(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() == this.m_LocationMarker)
		{
			ToolsModifierControl.cameraController.SetTarget((this.m_LocationMarker.get_activeStateIndex() != 1) ? InstanceID.Empty : this.m_InstanceID, this.m_WorldMousePosition, true);
		}
	}

	protected bool ShouldHide()
	{
		return !this.m_IsEmbbeded && (!this.m_Assigned || (this.m_Assigned && !this.IsValidTarget()));
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool isVisible)
	{
		if (!this.m_IsEmbbeded && comp == base.get_component())
		{
			if (!isVisible)
			{
				WorldInfoPanel.m_LastVisibleInfoPanel = null;
				this.m_Assigned = false;
			}
			else if (isVisible)
			{
				this.HideOtherWorldInfos();
				WorldInfoPanel.m_LastVisibleInfoPanel = this;
			}
		}
	}

	public void OnCloseButton()
	{
		if (!this.m_IsEmbbeded)
		{
			this.Hide();
		}
	}

	protected void HideOtherWorldInfos()
	{
		if (!this.m_IsEmbbeded)
		{
			foreach (Type current in WorldInfoPanel.m_WorldInfoPanels)
			{
				if (current != base.GetType())
				{
					UIView.get_library().Hide(current.Name);
				}
			}
		}
	}

	public static void HideAllWorldInfoPanels()
	{
		if (WorldInfoPanel.m_WorldInfoPanels != null)
		{
			foreach (Type current in WorldInfoPanel.m_WorldInfoPanels)
			{
				UIComponent uIComponent = UIView.get_library().Get(current.Name);
				if (uIComponent != null && uIComponent.get_isVisible())
				{
					uIComponent.Hide();
					WorldInfoPanel component = uIComponent.GetComponent<WorldInfoPanel>();
					if (component != null)
					{
						component.OnHide();
					}
				}
			}
		}
	}

	protected virtual void OnHide()
	{
	}

	public static bool AnyWorldInfoPanelOpen()
	{
		if (WorldInfoPanel.m_WorldInfoPanels != null)
		{
			foreach (Type current in WorldInfoPanel.m_WorldInfoPanels)
			{
				UIComponent uIComponent = UIView.get_library().Get(current.Name);
				if (uIComponent != null && uIComponent.get_isVisible())
				{
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public static InstanceID GetCurrentInstanceID()
	{
		WorldInfoPanel lastVisibleInfoPanel = WorldInfoPanel.m_LastVisibleInfoPanel;
		if (lastVisibleInfoPanel != null)
		{
			return lastVisibleInfoPanel.m_InstanceID;
		}
		return InstanceID.Empty;
	}

	public static void ChangeInstanceID(InstanceID oldID, InstanceID newID)
	{
		if (WorldInfoPanel.m_LastVisibleInfoPanel != null)
		{
			InstanceID instanceID = WorldInfoPanel.m_LastVisibleInfoPanel.m_InstanceID;
			if (!instanceID.IsEmpty && instanceID == oldID)
			{
				DefaultTool.OpenWorldInfoPanel(newID, WorldInfoPanel.m_LastVisibleInfoPanel.m_WorldMousePosition);
			}
		}
	}

	public static void Show<TPanel>(Vector3 worldMousePosition, InstanceID instanceID) where TPanel : WorldInfoPanel
	{
		TPanel tPanel = UIView.get_library().Show<TPanel>(typeof(TPanel).Name, false);
		tPanel.SetTarget(worldMousePosition, instanceID);
		tPanel.get_component().set_opacity(1f);
	}

	public static void Hide<TPanel>() where TPanel : WorldInfoPanel
	{
		TPanel tPanel = UIView.get_library().Hide<TPanel>(typeof(TPanel).Name);
		if (tPanel != null)
		{
			tPanel.ClearTarget();
		}
	}

	public void ClearTarget()
	{
		this.m_InstanceID = InstanceID.Empty;
	}

	public void Hide()
	{
		UIView.get_library().Hide(base.GetType().Name);
		this.ClearTarget();
	}

	protected bool IsValidTarget()
	{
		return InstanceManager.IsValid(this.m_InstanceID);
	}

	protected virtual void UpdateBindings()
	{
		if (this.m_LocationMarker != null)
		{
			this.m_LocationMarker.set_activeStateIndex((!ToolsModifierControl.cameraController.HasTarget(this.m_InstanceID)) ? 0 : 1);
		}
		if (this.m_ShowHideRoutesButton != null)
		{
			this.m_ShowHideRoutesButton.set_activeStateIndex((!this.isRoutesViewOn) ? 0 : 1);
		}
	}

	internal void SetTarget(InstanceID id)
	{
		this.SetTarget(Vector3.get_zero(), id);
	}

	internal void SetTarget(Vector3 worldMousePosition, InstanceID id)
	{
		this.m_WorldMousePosition = worldMousePosition;
		this.m_InstanceID = id;
		if (this.IsValidTarget())
		{
			this.m_Assigned = true;
			if (this.m_IsEmbbeded)
			{
				base.get_component().Show();
			}
		}
		else
		{
			this.Hide();
		}
		if (this.m_ShowHideRoutesButton != null)
		{
			this.RefreshShowHideButtonTooltip();
		}
		this.OnSetTarget();
		this.LateUpdate();
	}

	protected virtual void OnSetTarget()
	{
	}

	private void UpdatePosition()
	{
		if (this.m_CameraTransform != null)
		{
			Vector3 vector;
			if (this.m_InstanceID.Type != InstanceType.Disaster)
			{
				Quaternion quaternion;
				Vector3 vector2;
				if (InstanceManager.GetPosition(this.m_InstanceID, out vector, out quaternion, out vector2))
				{
					vector.y += vector2.y * 0.8f;
				}
				else
				{
					vector = this.m_WorldMousePosition;
				}
			}
			else
			{
				vector = Singleton<DisasterManager>.get_instance().m_disasters[(int)this.m_InstanceID.Disaster].m_targetPosition;
				vector.y += 70f;
			}
			Vector3 vector3 = Camera.get_main().WorldToScreenPoint(vector) * Mathf.Sign(Vector3.Dot(vector - this.m_CameraTransform.get_position(), this.m_CameraTransform.get_forward()));
			UIView uIView = base.get_component().GetUIView();
			Vector2 vector4 = (!(this.m_FullscreenContainer != null)) ? uIView.GetScreenResolution() : this.m_FullscreenContainer.get_size();
			vector3 /= uIView.get_inputScale();
			Vector3 vector5 = UIPivotExtensions.UpperLeftToTransform(base.get_component().get_pivot(), base.get_component().get_size(), base.get_component().get_arbitraryPivotOffset());
			Vector3 relativePosition = uIView.ScreenPointToGUI(vector3) + new Vector2(vector5.x, vector5.y);
			if (relativePosition.x < 0f)
			{
				relativePosition.x = 0f;
			}
			if (relativePosition.y < 0f)
			{
				relativePosition.y = 0f;
			}
			if (relativePosition.x + base.get_component().get_width() > vector4.x)
			{
				relativePosition.x = vector4.x - base.get_component().get_width();
			}
			if (relativePosition.y + base.get_component().get_height() > vector4.y)
			{
				relativePosition.y = vector4.y - base.get_component().get_height();
			}
			base.get_component().set_relativePosition(relativePosition);
		}
	}

	private void LateUpdate()
	{
		if (!base.get_component().get_isVisible())
		{
			return;
		}
		if (this.ShouldHide())
		{
			this.Hide();
		}
		else
		{
			if (!this.m_IsEmbbeded)
			{
				this.UpdatePosition();
			}
			this.UpdateBindings();
		}
	}

	protected void ShortenTextToFitParent(UIButton button)
	{
		float num = button.get_parent().get_width() - button.get_relativePosition().x;
		if (button.get_width() > num)
		{
			button.set_tooltip(button.get_text());
			string text = button.get_text();
			while (button.get_width() > num && text.Length > 5)
			{
				text = text.Substring(0, text.Length - 4);
				text = text.Trim();
				text += "...";
				button.set_text(text);
			}
		}
		else
		{
			button.set_tooltip(string.Empty);
		}
	}

	private UIMultiStateButton m_LocationMarker;

	protected UIMultiStateButton m_ShowHideRoutesButton;

	protected InstanceID m_InstanceID;

	public bool m_IsEmbbeded;

	protected Vector3 m_WorldMousePosition;

	protected bool m_Assigned;

	protected UIComponent m_FullscreenContainer;

	private static IEnumerable<Type> m_WorldInfoPanels;

	private static WorldInfoPanel m_LastVisibleInfoPanel;

	private Transform m_CameraTransform;
}
