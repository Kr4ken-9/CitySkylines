﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class CargoStationAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.NoisePollution;
		radius1 = this.m_noiseRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			TransportInfo transportInfo = this.m_transportInfo;
			if (this.m_transportInfo2 != null && this.m_transportInfo2.m_class.m_subService == this.m_info.m_class.m_subService)
			{
				transportInfo = this.m_transportInfo2;
			}
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)transportInfo.m_transportType];
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[(int)transportInfo.m_transportType];
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, Vector3 position, Quaternion rotation, Vector4 buildingState, Vector4 objectIndex, Color color)
	{
		int state = 0;
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			if ((data.m_flags & Building.Flags.Loading1) != Building.Flags.None)
			{
				state = 1;
			}
			else if ((data.m_flags & Building.Flags.Loading2) != Building.Flags.None)
			{
				state = 2;
			}
			else if ((data.m_flags & Building.Flags.SecondaryLoading) != Building.Flags.None)
			{
				state = 3;
			}
		}
		this.m_info.SetRenderParameters(position, rotation, buildingState, objectIndex, state, color);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Transport;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_cargoTransportAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (this.m_cargoTransportAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.CargoTransport, (float)this.m_cargoTransportAccumulation, this.m_cargoTransportRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			if (this.m_cargoTransportAccumulation != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (this.m_cargoTransportAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.CargoTransport, (float)(-(float)this.m_cargoTransportAccumulation), this.m_cargoTransportRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Vector3 vector = buildingData.CalculatePosition(this.m_spawnPosition);
		Vector3 vector2 = vector - buildingData.m_position;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = buildingData.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			Vehicle.Flags flags = instance.m_vehicles.m_buffer[(int)num].m_flags;
			flags &= (Vehicle.Flags.Spawned | Vehicle.Flags.Stopped | Vehicle.Flags.WaitingLoading);
			if (flags == (Vehicle.Flags.Spawned | Vehicle.Flags.Stopped | Vehicle.Flags.WaitingLoading))
			{
				if (instance.m_vehicles.m_buffer[(int)num].Info.m_class.m_subService == this.m_transportInfo.m_class.m_subService)
				{
					Vector3 lastFramePosition = instance.m_vehicles.m_buffer[(int)num].GetLastFramePosition();
					Vector3 vector3 = lastFramePosition - vector;
					if (vector3.x * vector2.x + vector3.z * vector2.z < 0f)
					{
						flag = true;
					}
					else
					{
						flag2 = true;
					}
				}
				else
				{
					flag3 = true;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		num = buildingData.m_guestVehicles;
		num2 = 0;
		while (num != 0)
		{
			Vehicle.Flags flags2 = instance.m_vehicles.m_buffer[(int)num].m_flags;
			flags2 &= (Vehicle.Flags.Spawned | Vehicle.Flags.Stopped | Vehicle.Flags.WaitingLoading);
			if (flags2 == (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingLoading))
			{
				if (instance.m_vehicles.m_buffer[(int)num].Info.m_class.m_subService == this.m_transportInfo.m_class.m_subService)
				{
					Vector3 lastFramePosition2 = instance.m_vehicles.m_buffer[(int)num].GetLastFramePosition();
					Vector3 vector4 = lastFramePosition2 - vector;
					if (vector4.x * vector2.x + vector4.z * vector2.z < 0f)
					{
						flag = true;
					}
					else
					{
						flag2 = true;
					}
				}
				else
				{
					flag3 = true;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextGuestVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (flag)
		{
			buildingData.m_flags |= Building.Flags.Loading1;
		}
		else
		{
			buildingData.m_flags &= ~Building.Flags.Loading1;
		}
		if (flag2)
		{
			buildingData.m_flags |= Building.Flags.Loading2;
		}
		else
		{
			buildingData.m_flags &= ~Building.Flags.Loading2;
		}
		if (flag3)
		{
			buildingData.m_flags |= Building.Flags.SecondaryLoading;
		}
		else
		{
			buildingData.m_flags &= ~Building.Flags.SecondaryLoading;
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = productionRate * this.m_cargoTransportAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.CargoTransport, num, buildingData.m_position, this.m_cargoTransportRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num2 = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num2 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num2, buildingData.m_position, this.m_noiseRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == VehicleInfo.VehicleType.Car)
		{
			if (info.m_class.m_service == ItemClass.Service.Industrial)
			{
				if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
				{
					position = data.CalculatePosition(this.m_truckUnspawnPosition);
					target = data.CalculateSidewalkPosition(this.m_truckUnspawnPosition.x, 0f);
				}
				else
				{
					position = data.CalculatePosition(this.m_truckSpawnPosition);
					target = data.CalculateSidewalkPosition(this.m_truckSpawnPosition.x, 0f);
				}
			}
			else
			{
				base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
			}
		}
		else if (this.m_transportInfo != null && info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			position = data.CalculatePosition(this.m_spawnPosition);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(this.m_spawnPosition * 2f - this.m_spawnTarget);
			}
			else
			{
				target = data.CalculatePosition(this.m_spawnTarget);
			}
		}
		else if (this.m_transportInfo2 != null && info.m_vehicleType == this.m_transportInfo2.m_vehicleType)
		{
			position = data.CalculatePosition(this.m_spawnPosition2);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(this.m_spawnPosition2 * 2f - this.m_spawnTarget2);
			}
			else
			{
				target = data.CalculatePosition(this.m_spawnTarget2);
			}
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == VehicleInfo.VehicleType.Car)
		{
			if (info.m_class.m_service == ItemClass.Service.Industrial)
			{
				if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
				{
					position = data.CalculatePosition(this.m_truckSpawnPosition);
					target = data.CalculateSidewalkPosition(this.m_truckSpawnPosition.x, 0f);
				}
				else
				{
					position = data.CalculatePosition(this.m_truckUnspawnPosition);
					target = data.CalculateSidewalkPosition(this.m_truckUnspawnPosition.x, 0f);
				}
			}
			else
			{
				base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
			}
		}
		else if (this.m_transportInfo != null && info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			position = data.CalculatePosition(this.m_spawnPosition);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic != SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(this.m_spawnPosition * 2f - this.m_spawnTarget);
			}
			else
			{
				target = data.CalculatePosition(this.m_spawnTarget);
			}
		}
		else if (this.m_transportInfo2 != null && info.m_vehicleType == this.m_transportInfo2.m_vehicleType)
		{
			position = data.CalculatePosition(this.m_spawnPosition2);
			if (this.m_canInvertTarget && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic != SimulationMetaData.MetaBool.True)
			{
				target = data.CalculatePosition(this.m_spawnPosition2 * 2f - this.m_spawnTarget2);
			}
			else
			{
				target = data.CalculatePosition(this.m_spawnTarget2);
			}
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			string.Empty
		}));
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	public int m_workPlaceCount0 = 3;

	public int m_workPlaceCount1 = 18;

	public int m_workPlaceCount2 = 21;

	public int m_workPlaceCount3 = 18;

	public int m_noiseAccumulation = 100;

	public float m_noiseRadius = 100f;

	public Vector3 m_spawnPosition;

	public Vector3 m_spawnTarget;

	public Vector3 m_spawnPosition2;

	public Vector3 m_spawnTarget2;

	public bool m_canInvertTarget;

	public Vector3 m_truckSpawnPosition;

	public Vector3 m_truckUnspawnPosition;

	public TransportInfo m_transportInfo;

	public TransportInfo m_transportInfo2;

	public int m_cargoTransportAccumulation = 100;

	public float m_cargoTransportRadius = 500f;
}
