﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class EventPopularityMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_targetPopularity;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)this.m_targetPopularity)
		{
			num = (long)this.m_targetPopularity;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_EVENT_POPULARITY_DESC", this.m_localizationKey), this.m_targetPopularity);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_EVENT_POPULARITY_PROG", this.m_localizationKey), new object[]
		{
			num,
			this.m_targetPopularity
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = (long)this.m_targetPopularity;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			int num = 0;
			int num2 = PrefabCollection<EventInfo>.PrefabCount();
			for (int i = 0; i < num2; i++)
			{
				EventInfo prefab = PrefabCollection<EventInfo>.GetPrefab((uint)i);
				if (prefab != null && prefab.m_prefabDataIndex != -1 && (this.m_supportEvents & prefab.m_type) != (EventManager.EventType)0 && (this.m_supportGroups & prefab.m_group) != (EventManager.EventGroup)0)
				{
					num = Mathf.Max(num, ConcertAI.GetPopularity(prefab));
				}
			}
			data.m_progress = (long)Mathf.Min(this.m_targetPopularity, num);
			data.m_passedCount = ((data.m_progress != (long)this.m_targetPopularity) ? 0 : 1);
		}
		return data;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		if (this.m_targetPopularity != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)this.m_targetPopularity));
		}
		return num;
	}

	public int m_targetPopularity = 100;

	[BitMask]
	public EventManager.EventType m_supportEvents;

	[BitMask]
	public EventManager.EventGroup m_supportGroups;

	public string m_localizationKey;
}
