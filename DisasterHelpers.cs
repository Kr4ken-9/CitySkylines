﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public static class DisasterHelpers
{
	public static void SplashWater(Vector2 position, float radius, float depth)
	{
		int num = Mathf.CeilToInt(radius / 16f);
		int num2 = Mathf.Clamp(Mathf.CeilToInt(depth * 65536f / 1024f), -32767, 32767);
		WaterWave waveData;
		waveData.m_origX = (ushort)Mathf.Clamp((int)((position.x + 8640f) / 16f + 0.5f), 0, 1080);
		waveData.m_origZ = (ushort)Mathf.Clamp((int)((position.y + 8640f) / 16f + 0.5f), 0, 1080);
		waveData.m_dirX = 0;
		waveData.m_dirZ = 0;
		waveData.m_minX = (ushort)Mathf.Max((int)waveData.m_origX - num, 0);
		waveData.m_minZ = (ushort)Mathf.Max((int)waveData.m_origZ - num, 0);
		waveData.m_maxX = (ushort)Mathf.Min((int)waveData.m_origX + num, 1080);
		waveData.m_maxZ = (ushort)Mathf.Min((int)waveData.m_origZ + num, 1080);
		waveData.m_type = 2;
		waveData.m_delta = (short)num2;
		waveData.m_duration = 256;
		waveData.m_currentTime = 0;
		ushort num3;
		Singleton<TerrainManager>.get_instance().WaterSimulation.CreateWaterWave(out num3, waveData);
	}

	public static void MakeCrater(Vector2 position, float radius, float depth, bool raiseEdges)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainModify.RefreshAllModifications();
		float num = 16f;
		int num2 = 1080;
		ushort[] rawHeights = instance.RawHeights;
		float num3 = 0.015625f;
		float num4 = 64f;
		int num5 = Mathf.Max((int)((position.x - radius) / num + (float)num2 * 0.5f), 0);
		int num6 = Mathf.Max((int)((position.y - radius) / num + (float)num2 * 0.5f), 0);
		int num7 = Mathf.Min((int)((position.x + radius) / num + (float)num2 * 0.5f) + 1, num2);
		int num8 = Mathf.Min((int)((position.y + radius) / num + (float)num2 * 0.5f) + 1, num2);
		for (int i = num6; i <= num8; i++)
		{
			Vector2 vector;
			vector.y = ((float)i - (float)num2 * 0.5f) * num;
			for (int j = num5; j <= num7; j++)
			{
				vector.x = ((float)j - (float)num2 * 0.5f) * num;
				int num9 = (int)rawHeights[i * (num2 + 1) + j];
				float num10 = (float)num9 * num3;
				float num11 = num10;
				float num12 = Vector2.Distance(position, vector);
				if (raiseEdges)
				{
					if (num12 < radius * 0.75f)
					{
						float num13 = num12 / (radius * 0.75f);
						num11 += (num13 * num13 * num13 * num13 - 0.7f) * depth;
					}
					else if (num12 < radius)
					{
						float num14 = (num12 - radius) / (radius * 0.25f);
						num11 += num14 * num14 * 0.3f * depth;
					}
				}
				else if (num12 < radius * 0.73f)
				{
					float num15 = num12 / (radius * 0.837f);
					num11 += (num15 * num15 * num15 * num15 - 1f) * depth;
				}
				else if (num12 < radius)
				{
					float num16 = num12 / (radius * 0.837f) - 1.197f;
					num11 -= num16 * num16 * 4f * depth;
				}
				int num17 = Mathf.Clamp(Mathf.RoundToInt(num11 * num4), 0, 65535);
				if (num17 != num9)
				{
					rawHeights[i * (num2 + 1) + j] = (ushort)num17;
				}
			}
		}
		TerrainModify.UpdateArea(num5 - 2, num6 - 2, num7 + 2, num8 + 2, true, false, false);
	}

	public static void MakeCrack(Vector2 startPos, Vector2 endPos, float width, float depth)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainModify.RefreshAllModifications();
		float num = 16f;
		int num2 = 1080;
		ushort[] rawHeights = instance.RawHeights;
		float num3 = 0.015625f;
		float num4 = 64f;
		Vector2 vector = Vector2.Min(startPos, endPos);
		Vector2 vector2 = Vector2.Max(startPos, endPos);
		int num5 = Mathf.Max((int)((vector.x - width) / num + (float)num2 * 0.5f), 0);
		int num6 = Mathf.Max((int)((vector.y - width) / num + (float)num2 * 0.5f), 0);
		int num7 = Mathf.Min((int)((vector2.x + width) / num + (float)num2 * 0.5f) + 1, num2);
		int num8 = Mathf.Min((int)((vector2.y + width) / num + (float)num2 * 0.5f) + 1, num2);
		for (int i = num6; i <= num8; i++)
		{
			Vector2 vector3;
			vector3.y = ((float)i - (float)num2 * 0.5f) * num;
			for (int j = num5; j <= num7; j++)
			{
				vector3.x = ((float)j - (float)num2 * 0.5f) * num;
				int num9 = (int)rawHeights[i * (num2 + 1) + j];
				float num10 = (float)num9 * num3;
				float num11 = num10;
				float num13;
				float num12 = Mathf.Sqrt(Segment2.DistanceSqr(startPos, endPos, vector3, ref num13));
				if (num12 < width * 0.5f)
				{
					num11 -= (1f - num12 / (width * 0.5f)) * depth;
				}
				int num14 = Mathf.Clamp(Mathf.RoundToInt(num11 * num4), 0, 65535);
				if (num14 != num9)
				{
					rawHeights[i * (num2 + 1) + j] = (ushort)num14;
				}
			}
		}
		TerrainModify.UpdateArea(num5 - 2, num6 - 2, num7 + 2, num8 + 2, true, false, false);
	}

	public static void DestroyStuff(int seed, InstanceManager.Group group, Vector3 position, float totalRadius, float preRadius, float removeRadius, float destructionRadiusMin, float destructionRadiusMax, float burnRadiusMin, float burnRadiusMax)
	{
		DisasterHelpers.DestroyBuildings(seed, group, position, preRadius, removeRadius, destructionRadiusMin, destructionRadiusMax, burnRadiusMin, burnRadiusMax, 1f);
		DisasterHelpers.DestroyNetSegments(seed, group, position, totalRadius, removeRadius, destructionRadiusMin, destructionRadiusMax);
		DisasterHelpers.DestroyTrees(seed, group, position, totalRadius, removeRadius, destructionRadiusMin, destructionRadiusMax, burnRadiusMin, burnRadiusMax);
		DisasterHelpers.DestroyProps(seed, position, totalRadius, removeRadius, destructionRadiusMin, destructionRadiusMax);
	}

	public static void DestroyStuff(InstanceManager.Group group, Segment3 ray, float radius, uint startFrame1, uint startFrame2)
	{
		DisasterHelpers.DestroyBuildings(group, ray, radius, startFrame1, startFrame2);
	}

	public static void DestroyBuildings(InstanceManager.Group group, Segment3 ray, float radius, uint startFrame1, uint startFrame2)
	{
		Vector3 vector = ray.Min();
		Vector3 vector2 = ray.Max();
		int num = Mathf.Max((int)((vector.x - radius - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.z - radius - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + radius + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.z + radius + 72f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					ushort nextGridBuilding = buildings.m_buffer[(int)num5].m_nextGridBuilding;
					Building.Flags flags = buildings.m_buffer[(int)num5].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Demolishing)) == Building.Flags.Created)
					{
						BuildingInfo info = buildings.m_buffer[(int)num5].Info;
						Vector3 position = buildings.m_buffer[(int)num5].m_position;
						float num7 = (float)buildings.m_buffer[(int)num5].Width * 4f;
						float num8 = (float)buildings.m_buffer[(int)num5].Length * 4f;
						float num9 = info.m_generatedInfo.m_size.y * 0.5f;
						position.y += num9;
						float num10 = Mathf.Sqrt(ray.DistanceSqr(position));
						float num11 = Mathf.Sqrt(num7 * num7 + num8 * num8 + num9 * num9);
						if (num10 < radius + num11)
						{
							Vector3 vector3 = Vector3.get_zero();
							double num12 = 0.0;
							int num13 = 0;
							Vector3 vector4 = Vector3.Normalize(ray.b - ray.a);
							Vector3 vector5;
							Vector3 vector6;
							if (Mathf.Abs(vector4.y) > 0.7071f)
							{
								vector5 = Vector3.Normalize(Vector3.Cross(Vector3.get_right(), vector4));
								vector6 = Vector3.Normalize(Vector3.Cross(vector5, vector4));
							}
							else
							{
								vector5 = Vector3.Normalize(Vector3.Cross(Vector3.get_up(), vector4));
								vector6 = Vector3.Normalize(Vector3.Cross(vector5, vector4));
							}
							Segment3 ray2;
							ray2..ctor(ray.a + vector5, ray.b + vector6);
							Segment3 ray3;
							ray3..ctor(ray.a - vector5, ray.b - vector6);
							Segment3 ray4;
							ray4..ctor(ray.a + vector6, ray.b + vector6);
							Segment3 ray5;
							ray5..ctor(ray.a - vector6, ray.b - vector6);
							float num14;
							if (buildings.m_buffer[(int)num5].RayCast(num5, ray, out num14))
							{
								vector3 += ray.Position(num14);
								num12 += startFrame1 * (double)(1f - num14) + startFrame2 * (double)num14;
								num13++;
							}
							if (buildings.m_buffer[(int)num5].RayCast(num5, ray2, out num14))
							{
								vector3 += ray2.Position(num14) - vector5 * 0.5f;
								num12 += startFrame1 * (double)(1f - num14) + startFrame2 * (double)num14;
								num13++;
							}
							if (buildings.m_buffer[(int)num5].RayCast(num5, ray3, out num14))
							{
								vector3 += ray3.Position(num14) + vector5 * 0.5f;
								num12 += startFrame1 * (double)(1f - num14) + startFrame2 * (double)num14;
								num13++;
							}
							if (buildings.m_buffer[(int)num5].RayCast(num5, ray4, out num14))
							{
								vector3 += ray4.Position(num14) - vector6 * 0.5f;
								num12 += startFrame1 * (double)(1f - num14) + startFrame2 * (double)num14;
								num13++;
							}
							if (buildings.m_buffer[(int)num5].RayCast(num5, ray5, out num14))
							{
								vector3 += ray5.Position(num14) + vector6 * 0.5f;
								num12 += startFrame1 * (double)(1f - num14) + startFrame2 * (double)num14;
								num13++;
							}
							if (num13 != 0)
							{
								vector3 /= (float)num13;
								num12 /= (double)num13;
								DisasterManager instance = Singleton<DisasterManager>.get_instance();
								if (instance.m_properties != null && instance.m_properties.m_mediumExplosion != null)
								{
									EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(vector3, Vector3.get_up(), 0f);
									InstanceID empty = InstanceID.Empty;
									empty.Building = num5;
									Singleton<EffectManager>.get_instance().DispatchEffect(instance.m_properties.m_mediumExplosion, empty, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().AmbientGroup, (uint)num12);
								}
								info.m_buildingAI.CollapseBuilding(num5, ref buildings.m_buffer[(int)num5], group, false, false, 0);
							}
						}
					}
					num5 = nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public static void DestroyBuildings(int seed, InstanceManager.Group group, Vector3 position, float preRadius, float removeRadius, float destructionRadiusMin, float destructionRadiusMax, float burnRadiusMin, float burnRadiusMax, float probability)
	{
		int num = Mathf.Max((int)((position.x - preRadius - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - preRadius - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + preRadius + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + preRadius + 72f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					ushort nextGridBuilding = buildings.m_buffer[(int)num5].m_nextGridBuilding;
					Building.Flags flags = buildings.m_buffer[(int)num5].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Demolishing)) == Building.Flags.Created)
					{
						Vector3 position2 = buildings.m_buffer[(int)num5].m_position;
						float num7 = VectorUtils.LengthXZ(position2 - position);
						if (num7 < preRadius)
						{
							Randomizer randomizer;
							randomizer..ctor((int)num5 | seed << 16);
							float num8 = (destructionRadiusMax - num7) / Mathf.Max(1f, destructionRadiusMax - destructionRadiusMin);
							float num9 = (burnRadiusMax - num7) / Mathf.Max(1f, burnRadiusMax - burnRadiusMin);
							bool flag = false;
							bool flag2 = (float)randomizer.Int32(10000u) < num8 * probability * 10000f;
							bool flag3 = (float)randomizer.Int32(10000u) < num9 * probability * 10000f;
							if (removeRadius != 0f && num7 - 72f < removeRadius)
							{
								BuildingInfo info = buildings.m_buffer[(int)num5].Info;
								if (info.m_circular)
								{
									float num10 = (float)(buildings.m_buffer[(int)num5].Width + buildings.m_buffer[(int)num5].Length) * 2f;
									flag = (num7 < removeRadius + num10);
								}
								else
								{
									float angle = buildings.m_buffer[(int)num5].m_angle;
									Vector2 vector = VectorUtils.XZ(position2);
									Vector2 vector2;
									vector2..ctor(Mathf.Cos(angle), Mathf.Sin(angle));
									Vector2 vector3;
									vector3..ctor(vector2.y, -vector2.x);
									vector2 *= (float)buildings.m_buffer[(int)num5].Width * 4f;
									vector3 *= (float)buildings.m_buffer[(int)num5].Length * 4f;
									Quad2 quad = default(Quad2);
									quad.a = vector - vector2 - vector3;
									quad.b = vector + vector2 - vector3;
									quad.c = vector + vector2 + vector3;
									quad.d = vector - vector2 + vector3;
									float num11 = VectorUtils.LengthXZ(position - new Vector3(quad.a.x, position2.y, quad.a.y));
									float num12 = VectorUtils.LengthXZ(position - new Vector3(quad.b.x, position2.y, quad.b.y));
									float num13 = VectorUtils.LengthXZ(position - new Vector3(quad.c.x, position2.y, quad.c.y));
									float num14 = VectorUtils.LengthXZ(position - new Vector3(quad.d.x, position2.y, quad.d.y));
									flag = (quad.Intersect(VectorUtils.XZ(position)) || num11 < removeRadius || num12 < removeRadius || num13 < removeRadius || num14 < removeRadius);
								}
							}
							if (flag)
							{
								BuildingInfo info2 = buildings.m_buffer[(int)num5].Info;
								info2.m_buildingAI.CollapseBuilding(num5, ref buildings.m_buffer[(int)num5], group, false, true, Mathf.RoundToInt(num9 * 255f));
							}
							else if (flag2)
							{
								BuildingInfo info3 = buildings.m_buffer[(int)num5].Info;
								info3.m_buildingAI.CollapseBuilding(num5, ref buildings.m_buffer[(int)num5], group, false, false, Mathf.RoundToInt(num9 * 255f));
							}
							else if (flag3 && (flags & Building.Flags.Collapsed) == Building.Flags.None && buildings.m_buffer[(int)num5].m_fireIntensity == 0)
							{
								BuildingInfo info4 = buildings.m_buffer[(int)num5].Info;
								info4.m_buildingAI.BurnBuilding(num5, ref buildings.m_buffer[(int)num5], group, false);
							}
						}
					}
					num5 = nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public static void UpgradeBuildings(int seed, InstanceManager.Group group, Vector3 position, float preRadius, float upgradeRadiusMin, float upgradeRadiusMax, float probability)
	{
		int num = Mathf.Max((int)((position.x - preRadius - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - preRadius - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + preRadius + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + preRadius + 72f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		int num5 = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort nextGridBuilding = buildings.m_buffer[(int)num6].m_nextGridBuilding;
					Building.Flags flags = buildings.m_buffer[(int)num6].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Completed | Building.Flags.Abandoned | Building.Flags.Demolishing | Building.Flags.Collapsed | Building.Flags.Upgrading)) == (Building.Flags.Created | Building.Flags.Completed))
					{
						Vector3 position2 = buildings.m_buffer[(int)num6].m_position;
						float num8 = VectorUtils.LengthXZ(position2 - position);
						if (num8 < preRadius)
						{
							Randomizer randomizer;
							randomizer..ctor((int)num6 | seed << 16);
							float num9 = (upgradeRadiusMax - num8) / Mathf.Max(1f, upgradeRadiusMax - upgradeRadiusMin);
							bool flag = (float)randomizer.Int32(10000u) < num9 * probability * 10000f;
							if (flag)
							{
								BuildingInfo info = buildings.m_buffer[(int)num6].Info;
								PrivateBuildingAI privateBuildingAI = info.m_buildingAI as PrivateBuildingAI;
								if (privateBuildingAI != null && privateBuildingAI.GetUpgradeInfo(num6, ref buildings.m_buffer[(int)num6]) != null)
								{
									privateBuildingAI.StartUpgrading(num6, ref buildings.m_buffer[(int)num6]);
									num5++;
								}
							}
						}
					}
					num6 = nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (num5 != 0 && group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_273_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_273_cp_1 = disaster;
				expr_273_cp_0[(int)expr_273_cp_1].m_upgradedCount = expr_273_cp_0[(int)expr_273_cp_1].m_upgradedCount + (ushort)num5;
			}
		}
	}

	public static void DestroyNetSegments(int seed, InstanceManager.Group group, Vector3 position, float totalRadius, float removeRadius, float destructionRadiusMin, float destructionRadiusMax)
	{
		int num = Mathf.Max((int)((position.x - totalRadius - 64f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - totalRadius - 64f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + totalRadius + 64f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + totalRadius + 64f) / 64f + 135f), 269);
		Array16<NetSegment> segments = Singleton<NetManager>.get_instance().m_segments;
		Array16<NetNode> nodes = Singleton<NetManager>.get_instance().m_nodes;
		ushort[] segmentGrid = Singleton<NetManager>.get_instance().m_segmentGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = segmentGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					ushort nextGridSegment = segments.m_buffer[(int)num5].m_nextGridSegment;
					NetSegment.Flags flags = segments.m_buffer[(int)num5].m_flags;
					if ((flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created)
					{
						ushort startNode = segments.m_buffer[(int)num5].m_startNode;
						ushort endNode = segments.m_buffer[(int)num5].m_endNode;
						Vector3 position2 = nodes.m_buffer[(int)startNode].m_position;
						Vector3 position3 = nodes.m_buffer[(int)endNode].m_position;
						Vector3 middlePosition = segments.m_buffer[(int)num5].m_middlePosition;
						float num7 = VectorUtils.LengthXZ(middlePosition - position);
						num7 = Mathf.Min(num7, VectorUtils.LengthXZ(position2 - position));
						num7 = Mathf.Min(num7, VectorUtils.LengthXZ(position3 - position));
						if (num7 < totalRadius)
						{
							NetInfo info = segments.m_buffer[(int)num5].Info;
							if ((info.m_class.m_layer & (ItemClass.Layer.Default | ItemClass.Layer.WaterPipes | ItemClass.Layer.MetroTunnels)) != ItemClass.Layer.None)
							{
								Randomizer randomizer;
								randomizer..ctor((int)num5 | seed << 16);
								float num8 = (destructionRadiusMax - num7) / Mathf.Max(1f, destructionRadiusMax - destructionRadiusMin);
								bool flag = false;
								bool flag2 = (float)randomizer.Int32(1000u) < num8 * 1000f;
								if (removeRadius != 0f && num7 - 32f < removeRadius)
								{
									Vector3 vector;
									Vector3 vector2;
									segments.m_buffer[(int)num5].GetClosestPositionAndDirection(position, out vector, out vector2);
									flag = (VectorUtils.LengthXZ(position - vector) < removeRadius + info.m_halfWidth);
								}
								if (flag || flag2)
								{
									ushort num9 = DefaultTool.FindSecondarySegment(num5);
									DisasterHelpers.DestroySegment(num5, num9, group, flag, flag2);
									if (num9 != 0)
									{
										if (nextGridSegment == num9)
										{
											nextGridSegment = segments.m_buffer[(int)num9].m_nextGridSegment;
										}
										DisasterHelpers.DestroySegment(num9, num5, group, flag, flag2);
									}
								}
							}
						}
					}
					num5 = nextGridSegment;
					if (++num6 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private static void DestroySegment(ushort segmentIndex, ushort ignoreSegment, InstanceManager.Group group, bool remove, bool collapse)
	{
		if (remove)
		{
			Array16<NetSegment> segments = Singleton<NetManager>.get_instance().m_segments;
			Array16<NetNode> nodes = Singleton<NetManager>.get_instance().m_nodes;
			NetInfo info = segments.m_buffer[(int)segmentIndex].Info;
			NetSegment.Flags flags = segments.m_buffer[(int)segmentIndex].m_flags;
			ushort startNode = segments.m_buffer[(int)segmentIndex].m_startNode;
			ushort endNode = segments.m_buffer[(int)segmentIndex].m_endNode;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = nodes.m_buffer[(int)startNode].GetSegment(i);
				if (segment != 0 && segment != segmentIndex && segment != ignoreSegment)
				{
					ushort num = DefaultTool.FindSecondarySegment(segment);
					NetInfo info2 = segments.m_buffer[(int)segment].Info;
					info2.m_netAI.CollapseSegment(segment, ref segments.m_buffer[(int)segment], group, false);
					if (num != 0 && num != segmentIndex && num != ignoreSegment)
					{
						NetInfo info3 = segments.m_buffer[(int)num].Info;
						info3.m_netAI.CollapseSegment(num, ref segments.m_buffer[(int)num], group, false);
					}
				}
			}
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = nodes.m_buffer[(int)endNode].GetSegment(j);
				if (segment2 != 0 && segment2 != segmentIndex && segment2 != ignoreSegment)
				{
					ushort num2 = DefaultTool.FindSecondarySegment(segment2);
					NetInfo info4 = segments.m_buffer[(int)segment2].Info;
					info4.m_netAI.CollapseSegment(segment2, ref segments.m_buffer[(int)segment2], group, false);
					if (num2 != 0 && num2 != segmentIndex && num2 != ignoreSegment)
					{
						NetInfo info5 = segments.m_buffer[(int)num2].Info;
						info5.m_netAI.CollapseSegment(num2, ref segments.m_buffer[(int)num2], group, false);
					}
				}
			}
			if ((flags & NetSegment.Flags.Collapsed) == NetSegment.Flags.None && group != null && (info.m_class.m_service == ItemClass.Service.Road || info.m_class.m_service == ItemClass.Service.PublicTransport))
			{
				ushort disaster = group.m_ownerInstance.Disaster;
				if (disaster != 0)
				{
					DisasterData[] expr_250_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
					ushort expr_250_cp_1 = disaster;
					expr_250_cp_0[(int)expr_250_cp_1].m_destroyedRoadLength = expr_250_cp_0[(int)expr_250_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(segments.m_buffer[(int)segmentIndex].m_averageLength);
				}
			}
			info.m_netAI.CollapseSegment(segmentIndex, ref segments.m_buffer[(int)segmentIndex], group, true);
		}
		else if (collapse)
		{
			Array16<NetSegment> segments2 = Singleton<NetManager>.get_instance().m_segments;
			NetInfo info6 = segments2.m_buffer[(int)segmentIndex].Info;
			info6.m_netAI.CollapseSegment(segmentIndex, ref segments2.m_buffer[(int)segmentIndex], group, false);
		}
	}

	public static void DestroyTrees(int seed, InstanceManager.Group group, Vector3 position, float totalRadius, float removeRadius, float destructionRadiusMin, float destructionRadiusMax, float burnRadiusMin, float burnRadiusMax)
	{
		int num = Mathf.Max((int)((position.x - totalRadius) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((position.z - totalRadius) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((position.x + totalRadius) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((position.z + totalRadius) / 32f + 270f), 539);
		Array32<TreeInstance> trees = Singleton<TreeManager>.get_instance().m_trees;
		uint[] treeGrid = Singleton<TreeManager>.get_instance().m_treeGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num5 = treeGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0u)
				{
					uint nextGridTree = trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
					TreeInstance.Flags flags = (TreeInstance.Flags)trees.m_buffer[(int)((UIntPtr)num5)].m_flags;
					if ((flags & (TreeInstance.Flags.Created | TreeInstance.Flags.Deleted)) == TreeInstance.Flags.Created)
					{
						Vector3 position2 = trees.m_buffer[(int)((UIntPtr)num5)].Position;
						float num7 = VectorUtils.LengthXZ(position2 - position);
						if (num7 < totalRadius)
						{
							Randomizer randomizer;
							randomizer..ctor(num5 | (uint)((uint)seed << 16));
							float num8 = (burnRadiusMax - num7) / Mathf.Max(1f, burnRadiusMax - burnRadiusMin);
							bool flag = num7 < removeRadius;
							bool flag2 = (float)randomizer.Int32(1000u) < num8 * 1000f;
							if (flag)
							{
								Singleton<TreeManager>.get_instance().ReleaseTree(num5);
							}
							else if (flag2 && (flags & TreeInstance.Flags.FireDamage) == TreeInstance.Flags.None)
							{
								Singleton<TreeManager>.get_instance().BurnTree(num5, group, 128);
							}
						}
					}
					num5 = nextGridTree;
					if (++num6 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public static void DestroyProps(int seed, Vector3 position, float totalRadius, float removeRadius, float destructionRadiusMin, float destructionRadiusMax)
	{
		float num = Mathf.Min(totalRadius, removeRadius);
		int num2 = Mathf.Max((int)((position.x - num) / 64f + 135f), 0);
		int num3 = Mathf.Max((int)((position.z - num) / 64f + 135f), 0);
		int num4 = Mathf.Min((int)((position.x + num) / 64f + 135f), 269);
		int num5 = Mathf.Min((int)((position.z + num) / 64f + 135f), 269);
		Array16<PropInstance> props = Singleton<PropManager>.get_instance().m_props;
		ushort[] propGrid = Singleton<PropManager>.get_instance().m_propGrid;
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = propGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort nextGridProp = props.m_buffer[(int)num6].m_nextGridProp;
					PropInstance.Flags flags = (PropInstance.Flags)props.m_buffer[(int)num6].m_flags;
					if ((flags & (PropInstance.Flags.Created | PropInstance.Flags.Deleted)) == PropInstance.Flags.Created)
					{
						Vector3 position2 = props.m_buffer[(int)num6].Position;
						float num8 = VectorUtils.LengthXZ(position2 - position);
						if (num8 < num)
						{
							Singleton<PropManager>.get_instance().ReleaseProp(num6);
						}
					}
					num6 = nextGridProp;
					if (++num7 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public static void BurnGround(Vector2 position, float radius, float intensity)
	{
		NaturalResourceManager instance = Singleton<NaturalResourceManager>.get_instance();
		float num = 33.75f;
		int num2 = 512;
		radius += num * 0.51f;
		intensity *= 255f;
		int num3 = Mathf.Max((int)((position.x - radius) / num + (float)num2 * 0.5f), 0);
		int num4 = Mathf.Max((int)((position.y - radius) / num + (float)num2 * 0.5f), 0);
		int num5 = Mathf.Min((int)((position.x + radius) / num + (float)num2 * 0.5f), num2 - 1);
		int num6 = Mathf.Min((int)((position.y + radius) / num + (float)num2 * 0.5f), num2 - 1);
		for (int i = num4; i <= num6; i++)
		{
			Vector2 vector;
			vector.y = ((float)i - (float)num2 * 0.5f + 0.5f) * num;
			for (int j = num3; j <= num5; j++)
			{
				vector.x = ((float)j - (float)num2 * 0.5f + 0.5f) * num;
				float num7 = Vector2.Distance(vector, position);
				if (num7 < radius)
				{
					int num8 = (int)instance.m_naturalResources[i * num2 + j].m_burned;
					num8 = Mathf.Max(num8, Mathf.Clamp(Mathf.RoundToInt(intensity * (1f - num7 / radius)), 0, 255));
					instance.m_naturalResources[i * num2 + j].m_burned = (byte)num8;
				}
			}
		}
		instance.AreaModifiedB(num3, num4, num5, num6);
	}

	public static void RemovePeople(InstanceManager.Group group, ushort buildingID, ref uint units, int killPercentage, ref Randomizer r)
	{
		if (units != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = units;
			uint num2 = 0u;
			int num3 = 5;
			int num4 = 0;
			int num5 = 0;
			while (num != 0u)
			{
				CitizenUnit.Flags flags = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags;
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u)
					{
						instance.m_units.m_buffer[(int)((UIntPtr)num)].SetCitizen(i, 0u);
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags != Citizen.Flags.None)
						{
							Citizen.Location currentLocation = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation;
							Citizen.Location location = Citizen.Location.Home;
							if ((ushort)(flags & CitizenUnit.Flags.Home) != 0)
							{
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding = 0;
								location = Citizen.Location.Home;
							}
							else if ((ushort)(flags & (CitizenUnit.Flags.Work | CitizenUnit.Flags.Student)) != 0)
							{
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding = 0;
								Citizen[] expr_11C_cp_0 = instance.m_citizens.m_buffer;
								UIntPtr expr_11C_cp_1 = (UIntPtr)citizen;
								expr_11C_cp_0[(int)expr_11C_cp_1].m_flags = (expr_11C_cp_0[(int)expr_11C_cp_1].m_flags & ~Citizen.Flags.Student);
								location = Citizen.Location.Work;
							}
							else if ((ushort)(flags & CitizenUnit.Flags.Visit) != 0)
							{
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding = 0;
								location = Citizen.Location.Visit;
							}
							else if ((ushort)(flags & CitizenUnit.Flags.Vehicle) != 0)
							{
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle = 0;
								location = Citizen.Location.Moving;
							}
							if (currentLocation == location)
							{
								if (r.Int32(100u) < killPercentage || buildingID == 0)
								{
									Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizen);
									num4++;
								}
								else
								{
									if (num3 == 5)
									{
										if (num2 == 0u)
										{
											num2 = units;
										}
										else
										{
											num2 = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
										}
										instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_flags = (CitizenUnit.Flags.Created | CitizenUnit.Flags.Visit);
										num3 = 0;
									}
									instance.m_units.m_buffer[(int)((UIntPtr)num2)].SetCitizen(num3++, citizen);
									instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding = buildingID;
									instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Visit;
									instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Collapsed = true;
								}
							}
						}
					}
				}
				num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				if (++num5 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (num2 == 0u)
			{
				Singleton<CitizenManager>.get_instance().ReleaseUnits(units);
				units = 0u;
			}
			else
			{
				num = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
				if (num != 0u)
				{
					Singleton<CitizenManager>.get_instance().ReleaseUnits(num);
					instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit = 0u;
				}
			}
			if (group != null && num4 != 0)
			{
				ushort disaster = group.m_ownerInstance.Disaster;
				if (disaster != 0)
				{
					DisasterData[] expr_357_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
					ushort expr_357_cp_1 = disaster;
					expr_357_cp_0[(int)expr_357_cp_1].m_casualtiesCount = expr_357_cp_0[(int)expr_357_cp_1].m_casualtiesCount + (uint)num4;
					StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.CasualtyCount);
					statisticBase.Add(num4);
				}
			}
		}
	}

	public static int SavePeople(ushort buildingID, ref uint units)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = units;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			CitizenUnit.Flags flags = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == buildingID || (instance2 != 0 && instance.m_instances.m_buffer[(int)instance2].m_targetBuilding == buildingID))
					{
						ushort num4 = 0;
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding == buildingID)
						{
							num4 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding;
						}
						else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding == buildingID)
						{
							num4 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
						}
						else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding == buildingID)
						{
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested)
							{
								instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested = false;
								if (instance2 != 0)
								{
									instance.ReleaseCitizenInstance(instance2);
								}
							}
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, 0, 0u);
							num4 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
						}
						if (num4 != 0)
						{
							CitizenInfo citizenInfo = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
							HumanAI humanAI = citizenInfo.m_citizenAI as HumanAI;
							if (humanAI != null)
							{
								Citizen[] expr_218_cp_0 = instance.m_citizens.m_buffer;
								UIntPtr expr_218_cp_1 = (UIntPtr)citizen;
								expr_218_cp_0[(int)expr_218_cp_1].m_flags = (expr_218_cp_0[(int)expr_218_cp_1].m_flags & ~Citizen.Flags.Evacuating);
								humanAI.StartMoving(citizen, ref instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)], buildingID, num4);
							}
						}
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Collapsed)
						{
							num2++;
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Collapsed = false;
						}
					}
					instance.m_units.m_buffer[(int)((UIntPtr)num)].SetCitizen(i, 0u);
					if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags != Citizen.Flags.None)
					{
						if ((ushort)(flags & CitizenUnit.Flags.Home) != 0)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding = 0;
						}
						else if ((ushort)(flags & (CitizenUnit.Flags.Work | CitizenUnit.Flags.Student)) != 0)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding = 0;
							Citizen[] expr_31D_cp_0 = instance.m_citizens.m_buffer;
							UIntPtr expr_31D_cp_1 = (UIntPtr)citizen;
							expr_31D_cp_0[(int)expr_31D_cp_1].m_flags = (expr_31D_cp_0[(int)expr_31D_cp_1].m_flags & ~Citizen.Flags.Student);
						}
						else if ((ushort)(flags & CitizenUnit.Flags.Visit) != 0)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding = 0;
						}
						else if ((ushort)(flags & CitizenUnit.Flags.Vehicle) != 0)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle = 0;
						}
					}
				}
			}
			num = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		Singleton<CitizenManager>.get_instance().ReleaseUnits(units);
		units = 0u;
		return num2;
	}

	public static void AddWind(Vector3 position, float radius, Vector3 directionalWind, float rotationalWind, float radialWind, InstanceManager.Group group)
	{
		DisasterHelpers.AddWindCitizens(position, radius, directionalWind, rotationalWind, radialWind, group);
		DisasterHelpers.AddWindVehicles(position, radius, directionalWind, rotationalWind, radialWind, group);
	}

	public static void AddWindCitizens(Vector3 position, float radius, Vector3 directionalWind, float rotationalWind, float radialWind, InstanceManager.Group group)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		int num = Mathf.Max((int)((position.x - radius) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - radius) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + radius) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + radius) / 8f + 1080f), 2159);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_citizenGrid[i * 2160 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					CitizenInfo info = instance.m_instances.m_buffer[(int)num5].Info;
					Vector3 vector = instance.m_instances.m_buffer[(int)num5].GetLastFramePosition() - position;
					float magnitude = vector.get_magnitude();
					if (magnitude < radius)
					{
						Vector3 vector2 = directionalWind;
						vector2.x -= vector.z * rotationalWind;
						vector2.z += vector.x * rotationalWind;
						vector2 += vector * radialWind / Mathf.Max(1f, magnitude);
						vector2 *= 1f - magnitude / radius;
						info.m_citizenAI.AddWind(num5, ref instance.m_instances.m_buffer[(int)num5], vector2, group);
					}
					num5 = instance.m_instances.m_buffer[(int)num5].m_nextGridInstance;
					if (++num6 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public static void AddWindVehicles(Vector3 position, float radius, Vector3 directionalWind, float rotationalWind, float radialWind, InstanceManager.Group group)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((position.x - radius) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((position.z - radius) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((position.x + radius) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((position.z + radius) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					VehicleInfo info = instance.m_vehicles.m_buffer[(int)num5].Info;
					Vector3 vector = instance.m_vehicles.m_buffer[(int)num5].GetLastFramePosition() - position;
					float magnitude = vector.get_magnitude();
					if (magnitude < radius)
					{
						Vector3 vector2 = directionalWind;
						vector2.x -= vector.z * rotationalWind;
						vector2.z += vector.x * rotationalWind;
						vector2 += vector * radialWind / Mathf.Max(1f, magnitude);
						vector2 *= 1f - magnitude / radius;
						info.m_vehicleAI.AddWind(num5, ref instance.m_vehicles.m_buffer[(int)num5], vector2, group);
					}
					num5 = instance.m_vehicles.m_buffer[(int)num5].m_nextGridVehicle;
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = num2; k <= num4; k++)
		{
			for (int l = num; l <= num3; l++)
			{
				ushort num7 = instance.m_parkedGrid[k * 540 + l];
				int num8 = 0;
				while (num7 != 0)
				{
					ushort nextGridParked = instance.m_parkedVehicles.m_buffer[(int)num7].m_nextGridParked;
					VehicleInfo info2 = instance.m_parkedVehicles.m_buffer[(int)num7].Info;
					Vector3 vector3 = instance.m_parkedVehicles.m_buffer[(int)num7].m_position - position;
					float magnitude2 = vector3.get_magnitude();
					if (magnitude2 < radius)
					{
						Vector3 vector4 = directionalWind;
						vector4.x -= vector3.z * rotationalWind;
						vector4.z += vector3.x * rotationalWind;
						vector4 += vector3 * radialWind / Mathf.Max(1f, magnitude2);
						vector4 *= 1f - magnitude2 / radius;
						info2.m_vehicleAI.AddWind(num7, ref instance.m_parkedVehicles.m_buffer[(int)num7], vector4, group);
					}
					num7 = nextGridParked;
					if (++num8 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		int num9 = Mathf.Max((int)((position.x - radius) / 320f + 27f), 0);
		int num10 = Mathf.Max((int)((position.z - radius) / 320f + 27f), 0);
		int num11 = Mathf.Min((int)((position.x + radius) / 320f + 27f), 53);
		int num12 = Mathf.Min((int)((position.z + radius) / 320f + 27f), 53);
		for (int m = num10; m <= num12; m++)
		{
			for (int n = num9; n <= num11; n++)
			{
				ushort num13 = instance.m_vehicleGrid2[m * 54 + n];
				int num14 = 0;
				while (num13 != 0)
				{
					VehicleInfo info3 = instance.m_vehicles.m_buffer[(int)num13].Info;
					Vector3 vector5 = instance.m_vehicles.m_buffer[(int)num13].GetLastFramePosition() - position;
					float magnitude3 = vector5.get_magnitude();
					if (magnitude3 < radius)
					{
						Vector3 vector6 = directionalWind;
						vector6.x -= vector5.z * rotationalWind;
						vector6.z += vector5.x * rotationalWind;
						vector6 += vector5 * radialWind / Mathf.Max(1f, magnitude3);
						vector6 *= 1f - magnitude3 / radius;
						info3.m_vehicleAI.AddWind(num13, ref instance.m_vehicles.m_buffer[(int)num13], vector6, group);
					}
					num13 = instance.m_vehicles.m_buffer[(int)num13].m_nextGridVehicle;
					if (++num14 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}
}
