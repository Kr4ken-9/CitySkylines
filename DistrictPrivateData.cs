﻿using System;
using ColossalFramework.IO;

public struct DistrictPrivateData
{
	public void Add(ref DistrictPrivateData data)
	{
		this.m_tempHomeOrWorkCount += data.m_tempHomeOrWorkCount;
		this.m_tempAliveCount += data.m_tempAliveCount;
		this.m_tempEmptyCount += data.m_tempEmptyCount;
		this.m_tempHealth += data.m_tempHealth;
		this.m_tempHappiness += data.m_tempHappiness;
		this.m_tempCrimeRate += data.m_tempCrimeRate;
		this.m_tempLevel += data.m_tempLevel;
		this.m_tempLevel1 += data.m_tempLevel1;
		this.m_tempLevel2 += data.m_tempLevel2;
		this.m_tempLevel3 += data.m_tempLevel3;
		this.m_tempLevel4 += data.m_tempLevel4;
		this.m_tempLevel5 += data.m_tempLevel5;
		this.m_tempBuildingArea += data.m_tempBuildingArea;
		this.m_tempBuildingCount += data.m_tempBuildingCount;
		this.m_tempAbandonedCount += data.m_tempAbandonedCount;
		this.m_tempBurnedCount += data.m_tempBurnedCount;
		this.m_tempCollapsedCount += data.m_tempCollapsedCount;
	}

	public void Update()
	{
		this.m_finalBuildingArea = this.m_tempBuildingArea;
		this.m_finalBuildingCount = this.m_tempBuildingCount;
		this.m_finalAbandonedCount = this.m_tempAbandonedCount;
		this.m_finalBurnedCount = this.m_tempBurnedCount;
		this.m_finalCollapsedCount = this.m_tempCollapsedCount;
		this.m_finalHomeOrWorkCount = this.m_tempHomeOrWorkCount;
		this.m_finalAliveCount = this.m_tempAliveCount;
		this.m_finalEmptyCount = this.m_tempEmptyCount;
		if (this.m_tempHomeOrWorkCount != 0u)
		{
			this.m_finalLevel = (byte)((this.m_tempLevel * 50u + (this.m_tempHomeOrWorkCount >> 1)) / this.m_tempHomeOrWorkCount);
			this.m_finalHealth = (byte)((this.m_tempHealth + (this.m_tempHomeOrWorkCount >> 1)) / this.m_tempHomeOrWorkCount);
			this.m_finalHappiness = (byte)((this.m_tempHappiness + (this.m_tempHomeOrWorkCount >> 1)) / this.m_tempHomeOrWorkCount);
			this.m_finalCrimeRate = (byte)((this.m_tempCrimeRate + (this.m_tempHomeOrWorkCount >> 1)) / this.m_tempHomeOrWorkCount);
		}
		else
		{
			this.m_finalLevel = 0;
			this.m_finalHealth = 0;
			this.m_finalHappiness = 0;
			this.m_finalCrimeRate = 0;
		}
		uint num = this.m_tempLevel1 + this.m_tempLevel2 + this.m_tempLevel3 + this.m_tempLevel4 + this.m_tempLevel5;
		int num2 = 100;
		if (num != 0u)
		{
			this.m_finalLevel1 = (byte)(((ulong)this.m_tempLevel1 * (ulong)((long)num2) + (ulong)(num >> 1)) / (ulong)num);
			num2 -= (int)this.m_finalLevel1;
			num -= this.m_tempLevel1;
		}
		else
		{
			this.m_finalLevel1 = (byte)num2;
			num2 = 0;
		}
		if (num != 0u)
		{
			this.m_finalLevel2 = (byte)(((ulong)this.m_tempLevel2 * (ulong)((long)num2) + (ulong)(num >> 1)) / (ulong)num);
			num2 -= (int)this.m_finalLevel2;
			num -= this.m_tempLevel2;
		}
		else
		{
			this.m_finalLevel2 = 0;
		}
		if (num != 0u)
		{
			this.m_finalLevel3 = (byte)(((ulong)this.m_tempLevel3 * (ulong)((long)num2) + (ulong)(num >> 1)) / (ulong)num);
			num2 -= (int)this.m_finalLevel3;
			num -= this.m_tempLevel3;
		}
		else
		{
			this.m_finalLevel3 = 0;
		}
		if (num != 0u)
		{
			this.m_finalLevel4 = (byte)(((ulong)this.m_tempLevel4 * (ulong)((long)num2) + (ulong)(num >> 1)) / (ulong)num);
			num2 -= (int)this.m_finalLevel4;
			num -= this.m_tempLevel4;
		}
		else
		{
			this.m_finalLevel4 = 0;
		}
		if (num != 0u)
		{
			this.m_finalLevel5 = (byte)(((ulong)this.m_tempLevel5 * (ulong)((long)num2) + (ulong)(num >> 1)) / (ulong)num);
		}
		else
		{
			this.m_finalLevel5 = 0;
		}
	}

	public void Reset()
	{
		this.m_tempHomeOrWorkCount = 0u;
		this.m_tempAliveCount = 0u;
		this.m_tempEmptyCount = 0u;
		this.m_tempHealth = 0u;
		this.m_tempHappiness = 0u;
		this.m_tempCrimeRate = 0u;
		this.m_tempLevel = 0u;
		this.m_tempLevel1 = 0u;
		this.m_tempLevel2 = 0u;
		this.m_tempLevel3 = 0u;
		this.m_tempLevel4 = 0u;
		this.m_tempLevel5 = 0u;
		this.m_tempBuildingArea = 0u;
		this.m_tempBuildingCount = 0;
		this.m_tempAbandonedCount = 0;
		this.m_tempBurnedCount = 0;
		this.m_tempCollapsedCount = 0;
	}

	public void GetLevelPercentages(int[] buffer)
	{
		int num = buffer.Length;
		if (num >= 1)
		{
			buffer[0] = (int)this.m_finalLevel1;
		}
		if (num >= 2)
		{
			buffer[1] = (int)this.m_finalLevel2;
		}
		if (num >= 3)
		{
			buffer[2] = (int)this.m_finalLevel3;
		}
		if (num >= 4)
		{
			buffer[3] = (int)this.m_finalLevel4;
		}
		if (num >= 5)
		{
			buffer[4] = (int)this.m_finalLevel5;
		}
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempHomeOrWorkCount);
		s.WriteUInt24(this.m_tempEmptyCount);
		s.WriteUInt32(this.m_tempHappiness);
		s.WriteUInt24(this.m_tempLevel);
		s.WriteUInt24(this.m_finalHomeOrWorkCount);
		s.WriteUInt24(this.m_finalEmptyCount);
		s.WriteUInt8((uint)this.m_finalHappiness);
		s.WriteUInt8((uint)this.m_finalLevel);
		s.WriteUInt24(this.m_tempLevel1);
		s.WriteUInt24(this.m_tempLevel2);
		s.WriteUInt24(this.m_tempLevel3);
		s.WriteUInt24(this.m_tempLevel4);
		s.WriteUInt24(this.m_tempLevel5);
		s.WriteUInt8((uint)this.m_finalLevel1);
		s.WriteUInt8((uint)this.m_finalLevel2);
		s.WriteUInt8((uint)this.m_finalLevel3);
		s.WriteUInt8((uint)this.m_finalLevel4);
		s.WriteUInt8((uint)this.m_finalLevel5);
		s.WriteUInt32(this.m_tempHealth);
		s.WriteUInt8((uint)this.m_finalHealth);
		s.WriteUInt24(this.m_tempBuildingArea);
		s.WriteUInt24(this.m_finalBuildingArea);
		s.WriteUInt16((uint)this.m_tempBuildingCount);
		s.WriteUInt16((uint)this.m_tempAbandonedCount);
		s.WriteUInt16((uint)this.m_tempBurnedCount);
		s.WriteUInt16((uint)this.m_finalBuildingCount);
		s.WriteUInt16((uint)this.m_finalAbandonedCount);
		s.WriteUInt16((uint)this.m_finalBurnedCount);
		s.WriteUInt24(this.m_tempAliveCount);
		s.WriteUInt24(this.m_finalAliveCount);
		s.WriteUInt32(this.m_tempCrimeRate);
		s.WriteUInt8((uint)this.m_finalCrimeRate);
		s.WriteUInt16((uint)this.m_tempCollapsedCount);
		s.WriteUInt16((uint)this.m_finalCollapsedCount);
	}

	public void Deserialize(DataSerializer s)
	{
		if (s.get_version() >= 67u)
		{
			this.m_tempHomeOrWorkCount = s.ReadUInt24();
			this.m_tempEmptyCount = s.ReadUInt24();
			this.m_tempHappiness = s.ReadUInt32();
			this.m_tempLevel = s.ReadUInt24();
			this.m_finalHomeOrWorkCount = s.ReadUInt24();
			this.m_finalEmptyCount = s.ReadUInt24();
			this.m_finalHappiness = (byte)s.ReadUInt8();
			this.m_finalLevel = (byte)s.ReadUInt8();
		}
		else if (s.get_version() >= 65u)
		{
			this.m_tempHomeOrWorkCount = s.ReadUInt24();
			this.m_tempEmptyCount = 0u;
			this.m_tempHappiness = s.ReadUInt32();
			this.m_tempLevel = s.ReadUInt24();
			this.m_finalHomeOrWorkCount = s.ReadUInt24();
			this.m_finalEmptyCount = 0u;
			this.m_finalHappiness = (byte)s.ReadUInt8();
			this.m_finalLevel = (byte)s.ReadUInt8();
		}
		else
		{
			if (s.get_version() >= 64u)
			{
				this.m_tempHappiness = s.ReadUInt24();
				this.m_finalHappiness = (byte)s.ReadUInt24();
			}
			else
			{
				this.m_tempHappiness = 0u;
				this.m_finalHappiness = 0;
			}
			this.m_tempHomeOrWorkCount = s.ReadUInt24();
			this.m_finalHomeOrWorkCount = s.ReadUInt24();
			this.m_tempLevel = s.ReadUInt24();
			this.m_finalLevel = (byte)s.ReadUInt24();
			this.m_tempEmptyCount = 0u;
			this.m_finalEmptyCount = 0u;
		}
		if (s.get_version() >= 117u)
		{
			this.m_tempLevel1 = s.ReadUInt24();
			this.m_tempLevel2 = s.ReadUInt24();
			this.m_tempLevel3 = s.ReadUInt24();
			this.m_tempLevel4 = s.ReadUInt24();
			this.m_tempLevel5 = s.ReadUInt24();
			this.m_finalLevel1 = (byte)s.ReadUInt8();
			this.m_finalLevel2 = (byte)s.ReadUInt8();
			this.m_finalLevel3 = (byte)s.ReadUInt8();
			this.m_finalLevel4 = (byte)s.ReadUInt8();
			this.m_finalLevel5 = (byte)s.ReadUInt8();
		}
		else
		{
			this.m_tempLevel1 = 0u;
			this.m_tempLevel2 = 0u;
			this.m_tempLevel3 = 0u;
			this.m_tempLevel4 = 0u;
			this.m_tempLevel5 = 0u;
			this.m_finalLevel1 = 0;
			this.m_finalLevel2 = 0;
			this.m_finalLevel3 = 0;
			this.m_finalLevel4 = 0;
			this.m_finalLevel5 = 0;
		}
		if (s.get_version() >= 142u)
		{
			this.m_tempHealth = s.ReadUInt32();
			this.m_finalHealth = (byte)s.ReadUInt8();
		}
		else
		{
			this.m_tempHealth = 0u;
			this.m_finalHealth = 0;
		}
		if (s.get_version() >= 143u)
		{
			this.m_tempBuildingArea = s.ReadUInt24();
			this.m_finalBuildingArea = s.ReadUInt24();
			this.m_tempBuildingCount = (ushort)s.ReadUInt16();
			this.m_tempAbandonedCount = (ushort)s.ReadUInt16();
			this.m_tempBurnedCount = (ushort)s.ReadUInt16();
			this.m_finalBuildingCount = (ushort)s.ReadUInt16();
			this.m_finalAbandonedCount = (ushort)s.ReadUInt16();
			this.m_finalBurnedCount = (ushort)s.ReadUInt16();
		}
		else
		{
			this.m_tempBuildingArea = 0u;
			this.m_finalBuildingArea = 0u;
			this.m_tempBuildingCount = 0;
			this.m_tempAbandonedCount = 0;
			this.m_tempBurnedCount = 0;
			this.m_finalBuildingCount = 0;
			this.m_finalAbandonedCount = 0;
			this.m_finalBurnedCount = 0;
		}
		if (s.get_version() >= 145u)
		{
			this.m_tempAliveCount = s.ReadUInt24();
			this.m_finalAliveCount = s.ReadUInt24();
		}
		else
		{
			this.m_tempAliveCount = 0u;
			this.m_finalAliveCount = 0u;
		}
		if (s.get_version() >= 181u)
		{
			this.m_tempCrimeRate = s.ReadUInt32();
			this.m_finalCrimeRate = (byte)s.ReadUInt8();
		}
		else
		{
			this.m_tempCrimeRate = 0u;
			this.m_finalCrimeRate = 0;
		}
		if (s.get_version() >= 256u)
		{
			this.m_tempCollapsedCount = (ushort)s.ReadUInt16();
			this.m_finalCollapsedCount = (ushort)s.ReadUInt16();
		}
		else
		{
			this.m_tempCollapsedCount = 0;
			this.m_finalCollapsedCount = 0;
		}
	}

	public uint m_tempHomeOrWorkCount;

	public uint m_tempAliveCount;

	public uint m_tempEmptyCount;

	public uint m_tempHealth;

	public uint m_tempHappiness;

	public uint m_tempCrimeRate;

	public uint m_tempLevel;

	public uint m_tempLevel1;

	public uint m_tempLevel2;

	public uint m_tempLevel3;

	public uint m_tempLevel4;

	public uint m_tempLevel5;

	public uint m_tempBuildingArea;

	public uint m_finalHomeOrWorkCount;

	public uint m_finalAliveCount;

	public uint m_finalEmptyCount;

	public uint m_finalBuildingArea;

	public ushort m_tempBuildingCount;

	public ushort m_tempAbandonedCount;

	public ushort m_tempBurnedCount;

	public ushort m_tempCollapsedCount;

	public ushort m_finalBuildingCount;

	public ushort m_finalAbandonedCount;

	public ushort m_finalBurnedCount;

	public ushort m_finalCollapsedCount;

	public byte m_finalHealth;

	public byte m_finalHappiness;

	public byte m_finalCrimeRate;

	public byte m_finalLevel;

	public byte m_finalLevel1;

	public byte m_finalLevel2;

	public byte m_finalLevel3;

	public byte m_finalLevel4;

	public byte m_finalLevel5;
}
