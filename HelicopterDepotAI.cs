﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class HelicopterDepotAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		InfoManager.InfoMode infoMode2;
		InfoManager.SubInfoMode subInfoMode;
		this.GetPlacementInfoMode(out infoMode2, out subInfoMode, 0f);
		if (infoMode == infoMode2)
		{
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != subInfoMode)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		switch (this.m_info.m_class.m_service)
		{
		case ItemClass.Service.HealthCare:
			mode = InfoManager.InfoMode.Health;
			subMode = InfoManager.SubInfoMode.Default;
			return;
		case ItemClass.Service.PoliceDepartment:
			mode = InfoManager.InfoMode.CrimeRate;
			subMode = InfoManager.SubInfoMode.Default;
			return;
		case ItemClass.Service.FireDepartment:
			mode = InfoManager.InfoMode.FireSafety;
			subMode = InfoManager.SubInfoMode.Default;
			return;
		}
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		TransferManager.TransferReason transferReason = this.GetTransferReason1();
		TransferManager.TransferReason transferReason2 = this.GetTransferReason2();
		if (material != TransferManager.TransferReason.None && (material == transferReason || material == transferReason2))
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level, VehicleInfo.VehicleType.Helicopter);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		TransferManager.TransferReason transferReason = this.GetTransferReason1();
		TransferManager.TransferReason transferReason2 = this.GetTransferReason2();
		if (transferReason != TransferManager.TransferReason.None)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(transferReason, offer);
		}
		if (transferReason2 != TransferManager.TransferReason.None)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(transferReason2, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num, buildingData.m_position, this.m_noiseRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
		TransferManager.TransferReason transferReason = this.GetTransferReason1();
		TransferManager.TransferReason transferReason2 = this.GetTransferReason2();
		if ((transferReason == TransferManager.TransferReason.ForestFire || transferReason2 == TransferManager.TransferReason.ForestFire) && finalProductionRate != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.FirewatchCoverage, finalProductionRate);
		}
		if (transferReason != TransferManager.TransferReason.None)
		{
			int num2 = (finalProductionRate * this.m_helicopterCount + 99) / 100;
			int num3 = 0;
			int num4 = 0;
			ushort num5 = 0;
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num6 = buildingData.m_ownVehicles;
			int num7 = 0;
			while (num6 != 0)
			{
				TransferManager.TransferReason transferType = (TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num6].m_transferType;
				if (transferType == transferReason || (transferType == transferReason2 && transferReason2 != TransferManager.TransferReason.None))
				{
					VehicleInfo info = instance.m_vehicles.m_buffer[(int)num6].Info;
					int num8;
					int num9;
					info.m_vehicleAI.GetSize(num6, ref instance.m_vehicles.m_buffer[(int)num6], out num8, out num9);
					num3++;
					if ((instance.m_vehicles.m_buffer[(int)num6].m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
					{
						num4++;
					}
					else if ((instance.m_vehicles.m_buffer[(int)num6].m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
					{
						num5 = num6;
					}
				}
				num6 = instance.m_vehicles.m_buffer[(int)num6].m_nextOwnVehicle;
				if (++num7 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (this.m_helicopterCount < 16384 && num3 - num4 > num2 && num5 != 0)
			{
				VehicleInfo info2 = instance.m_vehicles.m_buffer[(int)num5].Info;
				info2.m_vehicleAI.SetTarget(num5, ref instance.m_vehicles.m_buffer[(int)num5], buildingID);
			}
			if (num3 < num2)
			{
				int num10 = num2 - num3;
				bool flag = transferReason2 != TransferManager.TransferReason.None && (num10 >= 2 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0);
				bool flag2 = num10 >= 2 || !flag;
				if (flag2 && flag)
				{
					num10 = num10 + 1 >> 1;
				}
				if (flag2)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 6;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = Mathf.Min(2, num10);
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(transferReason, offer);
				}
				if (flag)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					offer2.Priority = 6;
					offer2.Building = buildingID;
					offer2.Position = buildingData.m_position;
					offer2.Amount = Mathf.Min(2, num10);
					offer2.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(transferReason2, offer2);
				}
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_HELICOPTER_CAPACITY", new object[]
			{
				this.m_helicopterCount
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num = (productionRate * this.m_helicopterCount + 99) / 100;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		TransferManager.TransferReason transferReason = this.GetTransferReason1();
		TransferManager.TransferReason transferReason2 = this.GetTransferReason2();
		if (transferReason != TransferManager.TransferReason.None)
		{
			base.CalculateOwnVehicles(buildingID, ref data, transferReason, ref num2, ref num3, ref num4, ref num5);
		}
		if (transferReason2 != TransferManager.TransferReason.None)
		{
			base.CalculateOwnVehicles(buildingID, ref data, transferReason2, ref num2, ref num3, ref num4, ref num5);
		}
		return LocaleFormatter.FormatGeneric("AIINFO_HELICOPTERS", new object[]
		{
			num2,
			num
		});
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	private TransferManager.TransferReason GetTransferReason1()
	{
		switch (this.m_info.m_class.m_service)
		{
		case ItemClass.Service.HealthCare:
			return TransferManager.TransferReason.Sick2;
		case ItemClass.Service.PoliceDepartment:
			return TransferManager.TransferReason.Crime;
		case ItemClass.Service.FireDepartment:
			return TransferManager.TransferReason.ForestFire;
		}
		return TransferManager.TransferReason.None;
	}

	private TransferManager.TransferReason GetTransferReason2()
	{
		ItemClass.Service service = this.m_info.m_class.m_service;
		if (service != ItemClass.Service.FireDepartment)
		{
			return TransferManager.TransferReason.None;
		}
		return TransferManager.TransferReason.Fire2;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 3;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 18;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 21;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 18;

	[CustomizableProperty("Helicopter Count")]
	public int m_helicopterCount = 10;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 100f;
}
