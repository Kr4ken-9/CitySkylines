﻿using System;
using ColossalFramework;
using UnityEngine;

public class LightSource : MonoBehaviour
{
	private void LateUpdate()
	{
		if (--this.m_used < 0)
		{
			float num = this.m_light.get_intensity() - Mathf.Max(0.01f, this.m_data.m_fadeSpeed) * Time.get_deltaTime();
			if (num < 0.01f)
			{
				Singleton<RenderManager>.get_instance().lightSystem.m_tempLights.Remove(this.m_data.m_id);
				Singleton<RenderManager>.get_instance().lightSystem.ReleaseLightSource(this);
			}
			else
			{
				this.m_light.set_intensity(num);
			}
		}
	}

	[NonSerialized]
	public LightData m_data;

	[NonSerialized]
	public Light m_light;

	[NonSerialized]
	public Transform m_transform;

	[NonSerialized]
	public LightSource m_nextSource;

	[NonSerialized]
	public int m_used;
}
