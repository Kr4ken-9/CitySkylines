﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class ZoneManager : SimulationManagerBase<ZoneManager, ZoneProperties>, ISimulationManager, ITerrainManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_blocks = new Array16<ZoneBlock>(49152u);
		this.m_cachedBlocks = new FastList<ZoneBlock>();
		this.m_updatedBlocks = new ulong[768];
		this.m_zoneGrid = new ushort[22500];
		this.m_tmpXBuffer = new int[13];
		this.m_zoneNotUsed = new ZoneTypeGuide[8];
		this.m_goodAreaFound = new short[8];
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
		ushort num;
		this.m_blocks.CreateItem(out num);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (this.m_DemandWrapper == null)
		{
			this.m_DemandWrapper = new DemandWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_DemandWrapper != null)
		{
			this.m_DemandWrapper.Release();
			this.m_DemandWrapper = null;
		}
	}

	public override void InitializeProperties(ZoneProperties properties)
	{
		base.InitializeProperties(properties);
		this.m_zoneMaterial = new Material(this.m_properties.m_zoneShader);
	}

	public override void DestroyProperties(ZoneProperties properties)
	{
		if (properties == this.m_properties && this.m_zoneMaterial != null)
		{
			Object.Destroy(this.m_zoneMaterial);
			this.m_zoneMaterial = null;
		}
		base.DestroyProperties(properties);
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (this.m_blockCount >= 16384)
			{
				return false;
			}
		}
		else if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_blockCount >= 4096)
			{
				return false;
			}
		}
		else if (this.m_blockCount >= 48640)
		{
			return false;
		}
		return true;
	}

	public bool CreateBlock(out ushort block, ref Randomizer randomizer, Vector3 position, float angle, int rows, uint buildIndex)
	{
		ushort num;
		if (this.m_blocks.CreateItem(out num, ref randomizer))
		{
			block = num;
			if (rows >= 8)
			{
				rows = 8;
			}
			this.m_blocks.m_buffer[(int)block].m_valid = 0uL;
			this.m_blocks.m_buffer[(int)block].m_shared = 0uL;
			this.m_blocks.m_buffer[(int)block].m_zone1 = 0uL;
			this.m_blocks.m_buffer[(int)block].m_zone2 = 0uL;
			this.m_blocks.m_buffer[(int)block].m_occupied1 = 0uL;
			this.m_blocks.m_buffer[(int)block].m_occupied2 = 0uL;
			this.m_blocks.m_buffer[(int)block].m_flags = 1u;
			this.m_blocks.m_buffer[(int)block].RowCount = rows;
			this.m_blocks.m_buffer[(int)block].m_buildIndex = buildIndex;
			this.m_blocks.m_buffer[(int)block].m_angle = angle;
			this.m_blocks.m_buffer[(int)block].m_position = position;
			this.InitializeBlock(block, ref this.m_blocks.m_buffer[(int)block]);
			this.UpdateBlock(block);
			this.m_blockCount = (int)(this.m_blocks.ItemCount() - 1u);
			return true;
		}
		block = 0;
		return false;
	}

	private void InitializeBlock(ushort block, ref ZoneBlock data)
	{
		int num = Mathf.Clamp((int)(data.m_position.x / 64f + 75f), 0, 149);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 75f), 0, 149);
		int num3 = num2 * 150 + num;
		while (!Monitor.TryEnter(this.m_zoneGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_blocks.m_buffer[(int)block].m_nextGridBlock = this.m_zoneGrid[num3];
			this.m_zoneGrid[num3] = block;
		}
		finally
		{
			Monitor.Exit(this.m_zoneGrid);
		}
	}

	public void ReleaseBlock(ushort block)
	{
		this.ReleaseBlockImplementation(block, ref this.m_blocks.m_buffer[(int)block]);
	}

	private void ReleaseBlockImplementation(ushort block, ref ZoneBlock data)
	{
		if (data.m_flags != 0u)
		{
			data.m_flags |= 2u;
			this.m_cachedBlocks.Add(data);
			int rowCount = data.RowCount;
			float angle = data.m_angle;
			Vector2 vector = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * 8f;
			Vector2 vector2;
			vector2..ctor(vector.y, -vector.x);
			Vector2 vector3 = VectorUtils.XZ(data.m_position);
			Quad2 quad = default(Quad2);
			quad.a = vector3 - 4f * vector - 4f * vector2;
			quad.b = vector3 + 0f * vector - 4f * vector2;
			quad.c = vector3 + 0f * vector + (float)(rowCount - 4) * vector2;
			quad.d = vector3 - 4f * vector + (float)(rowCount - 4) * vector2;
			this.UpdateBlocks(quad);
			data.m_flags = 0u;
			int num = Mathf.Clamp((int)(data.m_position.x / 64f + 75f), 0, 149);
			int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 75f), 0, 149);
			int num3 = num2 * 150 + num;
			while (!Monitor.TryEnter(this.m_zoneGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				ushort num4 = 0;
				ushort num5 = this.m_zoneGrid[num3];
				int num6 = 0;
				while (num5 != 0)
				{
					if (num5 == block)
					{
						if (num4 == 0)
						{
							this.m_zoneGrid[num3] = data.m_nextGridBlock;
						}
						else
						{
							this.m_blocks.m_buffer[(int)num4].m_nextGridBlock = data.m_nextGridBlock;
						}
						break;
					}
					num4 = num5;
					num5 = this.m_blocks.m_buffer[(int)num5].m_nextGridBlock;
					if (++num6 > 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				data.m_nextGridBlock = 0;
			}
			finally
			{
				Monitor.Exit(this.m_zoneGrid);
			}
			this.m_blocks.ReleaseItem(block);
			this.m_blockCount = (int)(this.m_blocks.ItemCount() - 1u);
		}
	}

	public void UpdateBlock(ushort block)
	{
		this.m_updatedBlocks[block >> 6] |= 1uL << (int)block;
		this.m_blocksUpdated = true;
	}

	public void UpdateBlocks(Quad2 quad)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		int num = Mathf.Max((int)((vector.x - 46f) / 64f + 75f), 0);
		int num2 = Mathf.Max((int)((vector.y - 46f) / 64f + 75f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 46f) / 64f + 75f), 149);
		int num4 = Mathf.Min((int)((vector2.y + 46f) / 64f + 75f), 149);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_zoneGrid[i * 150 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_blocks.m_buffer[(int)num5].m_position;
					float num7 = Mathf.Max(Mathf.Max(vector.x - 46f - position.x, vector.y - 46f - position.z), Mathf.Max(position.x - vector2.x - 46f, position.z - vector2.y - 46f));
					if (num7 < 0f && (this.m_blocks.m_buffer[(int)num5].m_flags & 3u) == 1u)
					{
						int rowCount = this.m_blocks.m_buffer[(int)num5].RowCount;
						float angle = this.m_blocks.m_buffer[(int)num5].m_angle;
						Vector2 vector3 = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * 8f;
						Vector2 vector4;
						vector4..ctor(vector3.y, -vector3.x);
						Vector2 vector5 = VectorUtils.XZ(position);
						Quad2 quad2 = default(Quad2);
						quad2.a = vector5 - 4f * vector3 - 4f * vector4;
						quad2.b = vector5 + 0f * vector3 - 4f * vector4;
						quad2.c = vector5 + 0f * vector3 + (float)(rowCount - 4) * vector4;
						quad2.d = vector5 - 4f * vector3 + (float)(rowCount - 4) * vector4;
						if (quad.Intersect(quad2))
						{
							this.m_updatedBlocks[num5 >> 6] |= 1uL << (int)num5;
							this.m_blocksUpdated = true;
						}
					}
					num5 = this.m_blocks.m_buffer[(int)num5].m_nextGridBlock;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void CheckSpace(ushort block, Vector3 position, float angle, int width, int length, ref ulong space1, ref ulong space2, ref ulong space3, ref ulong space4)
	{
		ZoneBlock zoneBlock = this.m_blocks.m_buffer[(int)block];
		float num = Mathf.Abs(zoneBlock.m_angle - angle) * 0.636619747f;
		num -= Mathf.Floor(num);
		if (num >= 0.02f && num <= 0.98f)
		{
			return;
		}
		float num2 = Mathf.Min(72f, (float)(width + length) * 4f) + 6f;
		float num3 = position.x - num2;
		float num4 = position.z - num2;
		float num5 = position.x + num2;
		float num6 = position.z + num2;
		if (zoneBlock.m_position.x + 46f < num3 || zoneBlock.m_position.x - 46f > num5 || zoneBlock.m_position.z + 46f < num4 || zoneBlock.m_position.z - 46f > num6)
		{
			return;
		}
		int rowCount = zoneBlock.RowCount;
		Vector3 vector = new Vector3(Mathf.Cos(zoneBlock.m_angle), 0f, Mathf.Sin(zoneBlock.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Vector3 vector3 = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)) * 8f;
		Vector3 vector4;
		vector4..ctor(vector3.z, 0f, -vector3.x);
		for (int i = 0; i < rowCount; i++)
		{
			Vector3 vector5 = ((float)i - 3.5f) * vector2;
			int num7 = 0;
			while ((long)num7 < 4L)
			{
				if ((zoneBlock.m_valid & ~zoneBlock.m_shared & ~(zoneBlock.m_occupied1 | zoneBlock.m_occupied2) & 1uL << (i << 3 | num7)) != 0uL)
				{
					Vector3 vector6 = ((float)num7 - 3.5f) * vector;
					Vector3 vector7 = zoneBlock.m_position + vector6 + vector5;
					if (Mathf.Abs(position.x - vector7.x) < num2 && Mathf.Abs(position.z - vector7.z) < num2)
					{
						bool flag = false;
						int num8 = 0;
						while (num8 < length && !flag)
						{
							Vector3 vector8 = ((float)num8 - (float)length * 0.5f + 0.5f) * vector4;
							int num9 = 0;
							while (num9 < width && !flag)
							{
								Vector3 vector9 = ((float)num9 - (float)width * 0.5f + 0.5f) * vector3;
								Vector3 vector10 = position + vector9 + vector8;
								if (Mathf.Abs(vector10.x - vector7.x) < 0.2f && Mathf.Abs(vector10.z - vector7.z) < 0.2f)
								{
									flag = true;
									if (num8 < 4)
									{
										space1 |= 1uL << (num8 << 4 | num9);
									}
									else if (num8 < 8)
									{
										space2 |= 1uL << (num8 - 4 << 4 | num9);
									}
									else if (num8 < 12)
									{
										space3 |= 1uL << (num8 - 8 << 4 | num9);
									}
									else
									{
										space4 |= 1uL << (num8 - 12 << 4 | num9);
									}
								}
								num9++;
							}
							num8++;
						}
					}
				}
				num7++;
			}
		}
	}

	public bool CheckSpace(Vector3 position, float angle, int width, int length, out int offset)
	{
		float num = Mathf.Min(72f, (float)(width + length) * 4f) + 6f;
		float num2 = position.x - num;
		float num3 = position.z - num;
		float num4 = position.x + num;
		float num5 = position.z + num;
		ulong num6 = 0uL;
		ulong num7 = 0uL;
		ulong num8 = 0uL;
		ulong num9 = 0uL;
		int num10 = Mathf.Max((int)((num2 - 46f) / 64f + 75f), 0);
		int num11 = Mathf.Max((int)((num3 - 46f) / 64f + 75f), 0);
		int num12 = Mathf.Min((int)((num4 + 46f) / 64f + 75f), 149);
		int num13 = Mathf.Min((int)((num5 + 46f) / 64f + 75f), 149);
		for (int i = num11; i <= num13; i++)
		{
			for (int j = num10; j <= num12; j++)
			{
				ushort num14 = this.m_zoneGrid[i * 150 + j];
				int num15 = 0;
				while (num14 != 0)
				{
					Vector3 position2 = this.m_blocks.m_buffer[(int)num14].m_position;
					float num16 = Mathf.Max(Mathf.Max(num2 - 46f - position2.x, num3 - 46f - position2.z), Mathf.Max(position2.x - num4 - 46f, position2.z - num5 - 46f));
					if (num16 < 0f)
					{
						this.CheckSpace(num14, position, angle, width, length, ref num6, ref num7, ref num8, ref num9);
					}
					num14 = this.m_blocks.m_buffer[(int)num14].m_nextGridBlock;
					if (++num15 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		bool result = true;
		bool flag = false;
		bool flag2 = false;
		for (int k = 0; k < length; k++)
		{
			for (int l = 0; l < width; l++)
			{
				bool flag3;
				if (k < 4)
				{
					flag3 = ((num6 & 1uL << (k << 4 | l)) != 0uL);
				}
				else if (k < 8)
				{
					flag3 = ((num7 & 1uL << (k - 4 << 4 | l)) != 0uL);
				}
				else if (k < 12)
				{
					flag3 = ((num8 & 1uL << (k - 8 << 4 | l)) != 0uL);
				}
				else
				{
					flag3 = ((num9 & 1uL << (k - 12 << 4 | l)) != 0uL);
				}
				if (!flag3)
				{
					result = false;
					if (l < width >> 1)
					{
						flag = true;
					}
					if (l >= width + 1 >> 1)
					{
						flag2 = true;
					}
				}
			}
		}
		if (flag == flag2)
		{
			offset = 0;
		}
		else if (flag)
		{
			offset = 1;
		}
		else if (flag2)
		{
			offset = -1;
		}
		else
		{
			offset = 0;
		}
		return result;
	}

	public int GetIncomingResidentDemand()
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		int num = this.CalculateIncomingResidentDemand(ref instance.m_districts.m_buffer[0]);
		return num + instance.m_districts.m_buffer[0].CalculateResidentialDemandOffset();
	}

	private int CalculateIncomingResidentDemand(ref District districtData)
	{
		int num = (int)(districtData.m_commercialData.m_finalHomeOrWorkCount + districtData.m_industrialData.m_finalHomeOrWorkCount + districtData.m_officeData.m_finalHomeOrWorkCount + districtData.m_playerData.m_finalHomeOrWorkCount);
		int num2 = (int)(districtData.m_commercialData.m_finalEmptyCount + districtData.m_industrialData.m_finalEmptyCount + districtData.m_officeData.m_finalEmptyCount + districtData.m_playerData.m_finalEmptyCount);
		int finalHomeOrWorkCount = (int)districtData.m_residentialData.m_finalHomeOrWorkCount;
		int finalEmptyCount = (int)districtData.m_residentialData.m_finalEmptyCount;
		int num3 = (int)(districtData.m_educated0Data.m_finalUnemployed + districtData.m_educated1Data.m_finalUnemployed + districtData.m_educated2Data.m_finalUnemployed + districtData.m_educated3Data.m_finalUnemployed);
		int num4 = (int)(districtData.m_educated0Data.m_finalHomeless + districtData.m_educated1Data.m_finalHomeless + districtData.m_educated2Data.m_finalHomeless + districtData.m_educated3Data.m_finalHomeless);
		int num5 = Mathf.Clamp(100 - finalHomeOrWorkCount, 50, 100);
		num5 += Mathf.Clamp((num2 * 200 - num3 * 200) / Mathf.Max(num, 100), -50, 50);
		num5 += Mathf.Clamp((finalEmptyCount * 200 - num4 * 200) / Mathf.Max(finalHomeOrWorkCount, 100), -50, 50);
		this.m_DemandWrapper.OnCalculateResidentialDemand(ref num5);
		return Mathf.Clamp(num5, 0, 100);
	}

	private int CalculateResidentialDemand(ref District districtData)
	{
		int num = (int)(districtData.m_commercialData.m_finalHomeOrWorkCount + districtData.m_industrialData.m_finalHomeOrWorkCount + districtData.m_officeData.m_finalHomeOrWorkCount + districtData.m_playerData.m_finalHomeOrWorkCount);
		int num2 = (int)(districtData.m_commercialData.m_finalEmptyCount + districtData.m_industrialData.m_finalEmptyCount + districtData.m_officeData.m_finalEmptyCount + districtData.m_playerData.m_finalEmptyCount);
		int finalHomeOrWorkCount = (int)districtData.m_residentialData.m_finalHomeOrWorkCount;
		int finalEmptyCount = (int)districtData.m_residentialData.m_finalEmptyCount;
		int num3 = (int)(districtData.m_educated0Data.m_finalUnemployed + districtData.m_educated1Data.m_finalUnemployed + districtData.m_educated2Data.m_finalUnemployed + districtData.m_educated3Data.m_finalUnemployed);
		int num4 = (int)(districtData.m_educated0Data.m_finalHomeless + districtData.m_educated1Data.m_finalHomeless + districtData.m_educated2Data.m_finalHomeless + districtData.m_educated3Data.m_finalHomeless);
		int num5 = Mathf.Clamp(100 - finalHomeOrWorkCount, 50, 100);
		num5 += Mathf.Clamp((num2 * 200 - num3 * 200) / Mathf.Max(num, 100), -50, 50);
		num5 += Mathf.Clamp((num4 * 200 - finalEmptyCount * 200) / Mathf.Max(finalHomeOrWorkCount, 100), -50, 50);
		this.m_DemandWrapper.OnCalculateResidentialDemand(ref num5);
		return Mathf.Clamp(num5, 0, 100);
	}

	private int CalculateCommercialDemand(ref District districtData)
	{
		int num = (int)(districtData.m_commercialData.m_finalHomeOrWorkCount - districtData.m_commercialData.m_finalEmptyCount);
		int num2 = (int)(districtData.m_residentialData.m_finalHomeOrWorkCount - districtData.m_residentialData.m_finalEmptyCount);
		int finalHomeOrWorkCount = (int)districtData.m_visitorData.m_finalHomeOrWorkCount;
		int finalEmptyCount = (int)districtData.m_visitorData.m_finalEmptyCount;
		int num3 = Mathf.Clamp(num2, 0, 50);
		num = num * 10 * 16 / 100;
		num2 = num2 * 20 / 100;
		num3 += Mathf.Clamp((num2 * 200 - num * 200) / Mathf.Max(num, 100), -50, 50);
		num3 += Mathf.Clamp((finalHomeOrWorkCount * 100 - finalEmptyCount * 300) / Mathf.Max(finalHomeOrWorkCount, 100), -50, 50);
		this.m_DemandWrapper.OnCalculateCommercialDemand(ref num3);
		return Mathf.Clamp(num3, 0, 100);
	}

	private int CalculateWorkplaceDemand(ref District districtData)
	{
		int num = (int)(districtData.m_residentialData.m_finalHomeOrWorkCount - districtData.m_residentialData.m_finalEmptyCount);
		int num2 = (int)(districtData.m_commercialData.m_finalHomeOrWorkCount + districtData.m_industrialData.m_finalHomeOrWorkCount + districtData.m_officeData.m_finalHomeOrWorkCount + districtData.m_playerData.m_finalHomeOrWorkCount);
		int num3 = (int)(districtData.m_commercialData.m_finalEmptyCount + districtData.m_industrialData.m_finalEmptyCount + districtData.m_officeData.m_finalEmptyCount + districtData.m_playerData.m_finalEmptyCount);
		int num4 = (int)(districtData.m_educated0Data.m_finalUnemployed + districtData.m_educated1Data.m_finalUnemployed + districtData.m_educated2Data.m_finalUnemployed + districtData.m_educated3Data.m_finalUnemployed);
		int num5 = Mathf.Clamp(num, 0, 50);
		num5 += Mathf.Clamp((num4 * 200 - num3 * 200) / Mathf.Max(num2, 100), -50, 50);
		this.m_DemandWrapper.OnCalculateWorkplaceDemand(ref num5);
		return Mathf.Clamp(num5, 0, 100);
	}

	private void UpdateDemand(ref int demand, int target)
	{
		int num = demand;
		int num2 = demand;
		if (target > num)
		{
			num2 = Mathf.Min(num + 2, target);
		}
		else if (target < demand)
		{
			num2 = Mathf.Max(num - 2, target);
		}
		this.m_DemandWrapper.OnUpdateDemand(num, ref num2, target);
		demand = num2;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_blocksUpdated)
		{
			int num = this.m_updatedBlocks.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedBlocks[i];
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							ushort num3 = (ushort)(i << 6 | j);
							this.m_blocks.m_buffer[(int)num3].CalculateBlock1(num3);
						}
					}
				}
			}
			for (int k = 0; k < num; k++)
			{
				ulong num4 = this.m_updatedBlocks[k];
				if (num4 != 0uL)
				{
					for (int l = 0; l < 64; l++)
					{
						if ((num4 & 1uL << l) != 0uL)
						{
							ushort num5 = (ushort)(k << 6 | l);
							this.m_blocks.m_buffer[(int)num5].CalculateBlock2(num5);
						}
					}
				}
			}
			for (int m = 0; m < num; m++)
			{
				ulong num6 = this.m_updatedBlocks[m];
				if (num6 != 0uL)
				{
					for (int n = 0; n < 64; n++)
					{
						if ((num6 & 1uL << n) != 0uL)
						{
							ushort num7 = (ushort)(m << 6 | n);
							this.m_blocks.m_buffer[(int)num7].CalculateBlock3(num7);
						}
					}
				}
			}
			this.m_blocksUpdated = false;
			for (int num8 = 0; num8 < num; num8++)
			{
				ulong num9 = this.m_updatedBlocks[num8];
				if (num9 != 0uL)
				{
					this.m_updatedBlocks[num8] = 0uL;
					for (int num10 = 0; num10 < 64; num10++)
					{
						if ((num9 & 1uL << num10) != 0uL)
						{
							ushort num11 = (ushort)(num8 << 6 | num10);
							this.m_blocks.m_buffer[(int)num11].UpdateBlock(num11);
						}
					}
				}
			}
			for (int num12 = 0; num12 < this.m_cachedBlocks.m_size; num12++)
			{
				this.m_cachedBlocks.m_buffer[num12].UpdateBlock(0);
			}
			this.m_cachedBlocks.Clear();
		}
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep != 0)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			if (instance.m_currentBuildIndex == this.m_lastBuildIndex)
			{
				int num13 = (int)(instance.m_currentFrameIndex & 1023u);
				int num14 = num13 * 48;
				int num15 = (num13 + 1) * 48 - 1;
				for (int num16 = num14; num16 <= num15; num16++)
				{
					if ((this.m_blocks.m_buffer[num16].m_flags & 5u) == 1u)
					{
						this.m_blocks.m_buffer[num16].SimulationStep((ushort)num16);
						if (instance.m_currentBuildIndex != this.m_lastBuildIndex)
						{
							break;
						}
					}
				}
				for (int num17 = 0; num17 < 8; num17++)
				{
					int num18 = (int)this.m_goodAreaFound[num17];
					if (num18 != 0 && num18 > -1024)
					{
						this.m_goodAreaFound[num17] = (short)(num18 - 1);
					}
				}
			}
			if ((instance.m_currentFrameIndex & 7u) == 0u)
			{
				this.m_lastBuildIndex = instance.m_currentBuildIndex;
			}
			if ((instance.m_currentFrameIndex & 255u) == 0u)
			{
				if (this.m_fullDemand)
				{
					this.m_actualResidentialDemand = 100;
					this.m_actualCommercialDemand = 100;
					this.m_actualWorkplaceDemand = 100;
					this.m_residentialDemand = 100;
					this.m_commercialDemand = 100;
					this.m_workplaceDemand = 100;
				}
				else
				{
					DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
					int target = this.CalculateResidentialDemand(ref instance2.m_districts.m_buffer[0]);
					int target2 = this.CalculateCommercialDemand(ref instance2.m_districts.m_buffer[0]);
					int target3 = this.CalculateWorkplaceDemand(ref instance2.m_districts.m_buffer[0]);
					this.UpdateDemand(ref this.m_actualResidentialDemand, target);
					this.UpdateDemand(ref this.m_actualCommercialDemand, target2);
					this.UpdateDemand(ref this.m_actualWorkplaceDemand, target3);
					target = this.m_actualResidentialDemand + instance2.m_districts.m_buffer[0].CalculateResidentialDemandOffset();
					target2 = this.m_actualCommercialDemand + instance2.m_districts.m_buffer[0].CalculateCommercialDemandOffset();
					target3 = this.m_actualWorkplaceDemand + instance2.m_districts.m_buffer[0].CalculateWorkplaceDemandOffset();
					this.UpdateDemand(ref this.m_residentialDemand, target);
					this.UpdateDemand(ref this.m_commercialDemand, target2);
					this.UpdateDemand(ref this.m_workplaceDemand, target3);
				}
			}
		}
		if (subStep <= 1 && properties != null)
		{
			int num19 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			if (num19 != 100)
			{
				if (num19 != 200)
				{
					if (num19 != 300)
					{
						if (num19 == 400)
						{
							if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Zoning))
							{
								this.m_zonesNotUsed.Activate(properties.m_zoningNotUsed1);
							}
						}
					}
					else if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Zone.Industrial))
					{
						if (this.m_workplaceDemand > 80)
						{
							if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
							{
								this.m_zoneDemandWorkplace.Activate(properties.m_generalDemand, ItemClass.Zone.ResidentialLow);
							}
							else
							{
								this.m_zoneDemandWorkplace.Activate(properties.m_zoningDemand, ItemClass.Zone.Industrial);
							}
						}
						else if (this.m_workplaceDemand < 80)
						{
							this.m_zoneDemandWorkplace.Deactivate();
						}
					}
				}
				else if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Zone.CommercialLow))
				{
					if (this.m_commercialDemand > 80)
					{
						if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
						{
							this.m_zoneDemandCommercial.Activate(properties.m_generalDemand, ItemClass.Zone.ResidentialLow);
						}
						else
						{
							this.m_zoneDemandCommercial.Activate(properties.m_zoningDemand, ItemClass.Zone.CommercialLow);
						}
					}
					else if (this.m_commercialDemand < 80)
					{
						this.m_zoneDemandCommercial.Deactivate();
					}
				}
			}
			else if (Singleton<UnlockManager>.get_instance().Unlocked(ItemClass.Zone.ResidentialLow))
			{
				if (this.m_residentialDemand > 80)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
					{
						this.m_zoneDemandResidential.Activate(properties.m_generalDemand, ItemClass.Zone.ResidentialLow);
					}
					else
					{
						this.m_zoneDemandResidential.Activate(properties.m_zoningDemand, ItemClass.Zone.ResidentialLow);
					}
				}
				else if (this.m_residentialDemand < 80)
				{
					this.m_zoneDemandResidential.Deactivate();
				}
			}
		}
		if (subStep <= 1 && properties != null)
		{
			int num20 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num21 = num20 * 8 >> 10;
			int num22 = ((num20 + 1) * 8 >> 10) - 1;
			for (int num23 = num21; num23 <= num22; num23++)
			{
				if (Singleton<UnlockManager>.get_instance().Unlocked((ItemClass.Zone)num23))
				{
					this.m_zoneNotUsed[num23].Activate(properties.m_zoningNotUsed2, (ItemClass.Zone)num23);
				}
			}
		}
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float x = zoneArea.m_min.x;
		float z = zoneArea.m_min.z;
		float x2 = zoneArea.m_max.x;
		float z2 = zoneArea.m_max.z;
		int num = Mathf.Max((int)((x - 46f) / 64f + 75f), 0);
		int num2 = Mathf.Max((int)((z - 46f) / 64f + 75f), 0);
		int num3 = Mathf.Min((int)((x2 + 46f) / 64f + 75f), 149);
		int num4 = Mathf.Min((int)((z2 + 46f) / 64f + 75f), 149);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_zoneGrid[i * 150 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_blocks.m_buffer[(int)num5].m_position;
					float num7 = Mathf.Max(Mathf.Max(x - 46f - position.x, z - 46f - position.z), Mathf.Max(position.x - x2 - 46f, position.z - z2 - 46f));
					if (num7 < 0f)
					{
						this.m_blocks.m_buffer[(int)num5].ZonesUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_blocks.m_buffer[(int)num5].m_nextGridBlock;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new ZoneManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("ZoneManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			this.m_residentialDemand = this.CalculateResidentialDemand(ref instance.m_districts.m_buffer[0]);
			this.m_commercialDemand = this.CalculateCommercialDemand(ref instance.m_districts.m_buffer[0]);
			this.m_workplaceDemand = this.CalculateWorkplaceDemand(ref instance.m_districts.m_buffer[0]);
			this.m_actualResidentialDemand = this.m_residentialDemand;
			this.m_actualCommercialDemand = this.m_commercialDemand;
			this.m_actualWorkplaceDemand = this.m_workplaceDemand;
		}
		for (int i = 0; i < 8; i++)
		{
			if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_zoneNotUsed[i] == null)
			{
				this.m_zoneNotUsed[i] = new ZoneTypeGuide();
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_zoneDemandResidential == null)
		{
			this.m_zoneDemandResidential = new ZoneTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_zoneDemandCommercial == null)
		{
			this.m_zoneDemandCommercial = new ZoneTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_zoneDemandWorkplace == null)
		{
			this.m_zoneDemandWorkplace = new ZoneTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_optionsNotUsed == null)
		{
			this.m_optionsNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_zonesNotUsed == null)
		{
			this.m_zonesNotUsed = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const int MAX_BLOCK_COUNT = 49152;

	public const int MAX_MAP_BLOCKS = 16384;

	public const int MAX_ASSET_BLOCKS = 4096;

	public const float ZONEGRID_CELL_SIZE = 64f;

	public const int ZONEGRID_RESOLUTION = 150;

	public int m_blockCount;

	public bool m_fullDemand;

	[NonSerialized]
	public Array16<ZoneBlock> m_blocks;

	[NonSerialized]
	public FastList<ZoneBlock> m_cachedBlocks;

	[NonSerialized]
	public ulong[] m_updatedBlocks;

	[NonSerialized]
	public bool m_blocksUpdated;

	[NonSerialized]
	public ushort[] m_zoneGrid;

	[NonSerialized]
	public int[] m_tmpXBuffer;

	[NonSerialized]
	public Material m_zoneMaterial;

	[NonSerialized]
	public int m_residentialDemand;

	[NonSerialized]
	public int m_commercialDemand;

	[NonSerialized]
	public int m_workplaceDemand;

	[NonSerialized]
	public int m_actualResidentialDemand;

	[NonSerialized]
	public int m_actualCommercialDemand;

	[NonSerialized]
	public int m_actualWorkplaceDemand;

	[NonSerialized]
	public short[] m_goodAreaFound;

	[NonSerialized]
	public uint m_lastBuildIndex;

	[NonSerialized]
	public ZoneTypeGuide[] m_zoneNotUsed;

	[NonSerialized]
	public ZoneTypeGuide m_zoneDemandResidential;

	[NonSerialized]
	public ZoneTypeGuide m_zoneDemandCommercial;

	[NonSerialized]
	public ZoneTypeGuide m_zoneDemandWorkplace;

	[NonSerialized]
	public GenericGuide m_optionsNotUsed;

	[NonSerialized]
	public GenericGuide m_zonesNotUsed;

	[NonSerialized]
	public DemandWrapper m_DemandWrapper;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "ZoneManager");
			ZoneManager instance = Singleton<ZoneManager>.get_instance();
			ZoneBlock[] buffer = instance.m_blocks.m_buffer;
			int num = buffer.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uInt.Write(buffer[i].m_flags);
			}
			uInt.EndWrite();
			for (int j = 1; j < num; j++)
			{
				if (buffer[j].m_flags != 0u)
				{
					s.WriteUInt32(buffer[j].m_buildIndex);
					s.WriteVector3(buffer[j].m_position);
					s.WriteFloat(buffer[j].m_angle);
					s.WriteULong64(buffer[j].m_valid);
					s.WriteULong64(buffer[j].m_shared);
					s.WriteULong64(buffer[j].m_occupied1);
					s.WriteULong64(buffer[j].m_occupied2);
					s.WriteULong64(buffer[j].m_zone1);
					s.WriteULong64(buffer[j].m_zone2);
				}
			}
			s.WriteInt8(instance.m_residentialDemand);
			s.WriteInt8(instance.m_commercialDemand);
			s.WriteInt8(instance.m_workplaceDemand);
			s.WriteInt8(instance.m_actualResidentialDemand);
			s.WriteInt8(instance.m_actualCommercialDemand);
			s.WriteInt8(instance.m_actualWorkplaceDemand);
			for (int k = 0; k < 8; k++)
			{
				s.WriteInt16((int)instance.m_goodAreaFound[k]);
			}
			for (int l = 0; l < 8; l++)
			{
				s.WriteObject<ZoneTypeGuide>(instance.m_zoneNotUsed[l]);
			}
			s.WriteObject<ZoneTypeGuide>(instance.m_zoneDemandResidential);
			s.WriteObject<ZoneTypeGuide>(instance.m_zoneDemandCommercial);
			s.WriteObject<ZoneTypeGuide>(instance.m_zoneDemandWorkplace);
			s.WriteObject<GenericGuide>(instance.m_optionsNotUsed);
			s.WriteObject<GenericGuide>(instance.m_zonesNotUsed);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "ZoneManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "ZoneManager");
			ZoneManager instance = Singleton<ZoneManager>.get_instance();
			ZoneBlock[] buffer = instance.m_blocks.m_buffer;
			ushort[] zoneGrid = instance.m_zoneGrid;
			int num = buffer.Length;
			int num2 = zoneGrid.Length;
			instance.m_blocks.ClearUnused();
			for (int i = 0; i < num2; i++)
			{
				zoneGrid[i] = 0;
			}
			if (s.get_version() >= 222u)
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int j = 1; j < num; j++)
				{
					buffer[j].m_flags = uInt.Read();
				}
				uInt.EndRead();
			}
			else if (s.get_version() >= 205u)
			{
				EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginRead(s);
				for (int k = 1; k < 32768; k++)
				{
					buffer[k].m_flags = uInt2.Read();
				}
				for (int l = 32768; l < num; l++)
				{
					buffer[l].m_flags = 0u;
				}
				uInt2.EndRead();
			}
			else
			{
				EncodedArray.UInt uInt3 = EncodedArray.UInt.BeginRead(s);
				for (int m = 1; m < 16384; m++)
				{
					buffer[m].m_flags = uInt3.Read();
				}
				for (int n = 16384; n < num; n++)
				{
					buffer[n].m_flags = 0u;
				}
				uInt3.EndRead();
			}
			for (int num3 = 1; num3 < num; num3++)
			{
				buffer[num3].m_nextGridBlock = 0;
				if (buffer[num3].m_flags != 0u)
				{
					buffer[num3].m_buildIndex = s.ReadUInt32();
					buffer[num3].m_position = s.ReadVector3();
					buffer[num3].m_angle = s.ReadFloat();
					buffer[num3].m_valid = s.ReadULong64();
					buffer[num3].m_shared = s.ReadULong64();
					buffer[num3].m_occupied1 = s.ReadULong64();
					if (s.get_version() >= 138u)
					{
						buffer[num3].m_occupied2 = s.ReadULong64();
					}
					else
					{
						buffer[num3].m_occupied2 = 0uL;
					}
					if (s.get_version() >= 4u)
					{
						buffer[num3].m_zone1 = s.ReadULong64();
						buffer[num3].m_zone2 = s.ReadULong64();
					}
					else
					{
						s.ReadUInt32();
						s.ReadUInt32();
						buffer[num3].m_zone1 = 0uL;
						buffer[num3].m_zone2 = 0uL;
					}
					instance.InitializeBlock((ushort)num3, ref buffer[num3]);
				}
				else
				{
					buffer[num3].m_buildIndex = 0u;
					buffer[num3].m_position = Vector3.get_zero();
					buffer[num3].m_angle = 0f;
					buffer[num3].m_valid = 0uL;
					buffer[num3].m_shared = 0uL;
					buffer[num3].m_occupied1 = 0uL;
					buffer[num3].m_occupied2 = 0uL;
					buffer[num3].m_zone1 = 0uL;
					buffer[num3].m_zone2 = 0uL;
					instance.m_blocks.ReleaseItem((ushort)num3);
				}
			}
			if (s.get_version() >= 74u)
			{
				instance.m_residentialDemand = s.ReadInt8();
				instance.m_commercialDemand = s.ReadInt8();
				instance.m_workplaceDemand = s.ReadInt8();
			}
			else
			{
				instance.m_residentialDemand = 50;
				instance.m_commercialDemand = 50;
				instance.m_workplaceDemand = 50;
			}
			if (s.get_version() >= 168u)
			{
				instance.m_actualResidentialDemand = s.ReadInt8();
				instance.m_actualCommercialDemand = s.ReadInt8();
				instance.m_actualWorkplaceDemand = s.ReadInt8();
			}
			else
			{
				instance.m_actualResidentialDemand = instance.m_residentialDemand;
				instance.m_actualCommercialDemand = instance.m_commercialDemand;
				instance.m_actualWorkplaceDemand = instance.m_workplaceDemand;
			}
			if (s.get_version() >= 194u)
			{
				for (int num4 = 0; num4 < 8; num4++)
				{
					instance.m_goodAreaFound[num4] = (short)s.ReadInt16();
				}
			}
			else
			{
				for (int num5 = 0; num5 < 8; num5++)
				{
					instance.m_goodAreaFound[num5] = 0;
				}
			}
			if (s.get_version() >= 82u)
			{
				for (int num6 = 0; num6 < 8; num6++)
				{
					instance.m_zoneNotUsed[num6] = s.ReadObject<ZoneTypeGuide>();
				}
				instance.m_zoneDemandResidential = s.ReadObject<ZoneTypeGuide>();
				instance.m_zoneDemandCommercial = s.ReadObject<ZoneTypeGuide>();
				instance.m_zoneDemandWorkplace = s.ReadObject<ZoneTypeGuide>();
			}
			else
			{
				for (int num7 = 0; num7 < 8; num7++)
				{
					instance.m_zoneNotUsed[num7] = null;
				}
				instance.m_zoneDemandResidential = null;
				instance.m_zoneDemandCommercial = null;
				instance.m_zoneDemandWorkplace = null;
			}
			if (s.get_version() >= 134u)
			{
				instance.m_optionsNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_optionsNotUsed = null;
			}
			if (s.get_version() >= 160u)
			{
				instance.m_zonesNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_zonesNotUsed = null;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "ZoneManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "ZoneManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			ZoneManager instance = Singleton<ZoneManager>.get_instance();
			instance.m_lastBuildIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex;
			instance.m_blockCount = (int)(instance.m_blocks.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "ZoneManager");
		}
	}
}
