﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class PedestrianTunnelAI : PlayerNetAI
{
	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Pollution || base.ColorizeProps(infoMode);
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.TrafficRoutes && infoMode != InfoManager.InfoMode.Underground)
		{
			if (infoMode == InfoManager.InfoMode.None)
			{
				Color color = this.m_info.m_color;
				color.a = (float)(255 - data.m_wetness) * 0.003921569f;
				return color;
			}
			if (infoMode != InfoManager.InfoMode.Transport)
			{
				if (infoMode == InfoManager.InfoMode.Traffic)
				{
					return base.GetColor(segmentID, ref data, infoMode) * 0.2f;
				}
				if (infoMode != InfoManager.InfoMode.EscapeRoutes)
				{
					return base.GetColor(segmentID, ref data, infoMode);
				}
			}
		}
		return new Color(0.2f, 0.2f, 0.2f, 1f);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.TrafficRoutes && infoMode != InfoManager.InfoMode.Underground)
		{
			if (infoMode == InfoManager.InfoMode.None)
			{
				int num = 0;
				NetManager instance = Singleton<NetManager>.get_instance();
				int num2 = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0)
					{
						num += (int)instance.m_segments.m_buffer[(int)segment].m_wetness;
						num2++;
					}
				}
				if (num2 != 0)
				{
					num /= num2;
				}
				Color color = this.m_info.m_color;
				color.a = (float)(255 - num) * 0.003921569f;
				return color;
			}
			if (infoMode != InfoManager.InfoMode.Transport)
			{
				if (infoMode == InfoManager.InfoMode.Traffic)
				{
					return base.GetColor(nodeID, ref data, infoMode) * 0.2f;
				}
				if (infoMode != InfoManager.InfoMode.EscapeRoutes)
				{
					return base.GetColor(nodeID, ref data, infoMode);
				}
			}
		}
		return new Color(0.2f, 0.2f, 0.2f, 1f);
	}

	public override void ReleaseNode(ushort nodeID, ref NetNode data)
	{
		if (data.m_lane != 0u)
		{
			this.RemoveLaneConnection(nodeID, ref data);
		}
		base.ReleaseNode(nodeID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Flood);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		bool flag = false;
		if ((this.m_info.m_setCitizenFlags & CitizenInstance.Flags.Underground) == CitizenInstance.Flags.None)
		{
			float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
			if (num > vector.y + 1f && num > 0f)
			{
				flag = true;
				if ((data.m_flags & NetSegment.Flags.Flooded) == NetSegment.Flags.None)
				{
					data.m_flags |= NetSegment.Flags.Flooded;
					data.m_modifiedIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex++;
				}
				problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
			}
			else
			{
				data.m_flags &= ~NetSegment.Flags.Flooded;
				if (num > vector.y && num > 0f)
				{
					flag = true;
					problem = Notification.AddProblems(problem, Notification.Problem.Flood);
				}
			}
			int num2 = (int)data.m_wetness;
			if (!instance.m_treatWetAsSnow)
			{
				if (flag)
				{
					num2 = 255;
				}
				else
				{
					int num3 = -(num2 + 63 >> 5);
					float num4 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
					if (num4 != 0f)
					{
						int num5 = Mathf.RoundToInt(Mathf.Min(num4 * 4000f, 1000f));
						num3 += Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num5, num5 + 99) / 100;
					}
					num2 = Mathf.Clamp(num2 + num3, 0, 255);
				}
			}
			if (num2 != (int)data.m_wetness)
			{
				if (Mathf.Abs((int)data.m_wetness - num2) > 10)
				{
					data.m_wetness = (byte)num2;
					InstanceID empty = InstanceID.Empty;
					empty.NetSegment = segmentID;
					instance.AddSmoothColor(empty);
					empty.NetNode = data.m_startNode;
					instance.AddSmoothColor(empty);
					empty.NetNode = data.m_endNode;
					instance.AddSmoothColor(empty);
				}
				else
				{
					data.m_wetness = (byte)num2;
					instance.m_wetnessChanged = 256;
				}
			}
		}
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(vector);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.BikeBan) != DistrictPolicies.CityPlanning.None)
		{
			data.m_flags |= NetSegment.Flags.BikeBan;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.BikeBan;
		}
		data.m_problems = problem;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if (this.m_info.m_flattenTerrain)
		{
			return this.m_info.m_halfWidth + 1000f;
		}
		return this.m_info.m_halfWidth + 2000f;
	}

	public override bool IsUnderground()
	{
		return true;
	}

	public override bool BuildUnderground()
	{
		return !this.m_info.m_flattenTerrain;
	}

	public override bool CanModify()
	{
		return this.m_canModify;
	}

	public override void GetTerrainModifyRange(out float start, out float end)
	{
		start = ((!this.m_info.m_flattenTerrain) ? 0f : 0.25f);
		end = 1f;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = ((!this.m_info.m_flattenTerrain) ? -3 : 0);
		max = 0;
	}

	public override void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
		Singleton<NetManager>.get_instance().UpdateNode(nodeID, 0, 10);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & (NetNode.Flags.Temporary | NetNode.Flags.OnGround | NetNode.Flags.Underground)) == NetNode.Flags.OnGround)
		{
			uint num = 0u;
			byte offset = 0;
			float num2 = 1E+10f;
			if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
			{
				PathUnit.Position pathPos;
				PathUnit.Position position;
				float num3;
				float num4;
				if (this.m_connectService1 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService1, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, false, true, 16f, out pathPos, out position, out num3, out num4) && num3 < num2)
				{
					num2 = num3;
					num = PathManager.GetLaneID(pathPos);
					offset = pathPos.m_offset;
				}
				PathUnit.Position pathPos2;
				PathUnit.Position position2;
				float num5;
				float num6;
				if (this.m_connectService2 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService2, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, false, true, 16f, out pathPos2, out position2, out num5, out num6) && num5 < num2)
				{
					num = PathManager.GetLaneID(pathPos2);
					offset = pathPos2.m_offset;
				}
			}
			if (num != data.m_lane)
			{
				if (data.m_lane != 0u)
				{
					this.RemoveLaneConnection(nodeID, ref data);
				}
				if (num != 0u)
				{
					this.AddLaneConnection(nodeID, ref data, num, offset);
				}
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].CalculatePosition((float)offset * 0.003921569f);
		if (vector.y != data.m_position.y)
		{
			data.m_position.y = vector.y;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
		float num4 = (float)data.m_elevation;
		if (this.IsUnderground())
		{
			num4 = -num4;
		}
		float num5 = NetSegment.SampleTerrainHeight(this.m_info, data.m_position, false, num4);
		if (num5 != data.m_position.y)
		{
			data.m_position.y = num5;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (vector2.y > -0.5f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 0;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (vector2.y > -0.5f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 0;
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	public override bool FlattenGroundNodes()
	{
		return true;
	}

	public override bool CanClipNodes()
	{
		return false;
	}

	public override bool RaiseTerrain()
	{
		return true;
	}

	public ItemClass.Service m_connectService1 = ItemClass.Service.Road;

	public ItemClass.Service m_connectService2 = ItemClass.Service.PublicTransport;

	[CustomizableProperty("Can Modify", "Properties")]
	public bool m_canModify = true;
}
