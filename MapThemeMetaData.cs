﻿using System;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using UnityEngine;

[Serializable]
public class MapThemeMetaData : MetaData
{
	public bool isPublished
	{
		get
		{
			return this.publishedHash == this.name.GetHashCode();
		}
	}

	public void SetSelfRef(Package.Asset asset)
	{
		PublishedFileId publishedFileID = asset.get_package().GetPublishedFileID();
		if (publishedFileID != PublishedFileId.invalid)
		{
			this.mapThemeRef = publishedFileID.ToString() + "." + asset.get_name();
		}
	}

	public override string getEnvironment
	{
		get
		{
			return this.environment;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public string name;

	public string mapThemeRef;

	public string environment;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;

	public ModInfo[] mods;

	public int publishedHash;

	public Package.Asset grassDiffuseAsset;

	public float grassTiling;

	public Package.Asset ruinedDiffuseAsset;

	public float ruinedTiling;

	public Package.Asset pavementDiffuseAsset;

	public float pavementTiling;

	public Package.Asset gravelDiffuseAsset;

	public float gravelTiling;

	public Package.Asset oilDiffuseAsset;

	public float oilTiling;

	public Package.Asset oreDiffuseAsset;

	public float oreTiling;

	public Package.Asset sandDiffuseAsset;

	public float sandDiffuseTiling;

	public Package.Asset cliffDiffuseAsset;

	public float cliffDiffuseTiling;

	public Package.Asset cliffSandNormalAsset;

	public float cliffSandNormalTiling;

	public Vector3 grassPollutionColorOffset;

	public Vector3 grassFieldColorOffset;

	public Vector3 grassFertilityColorOffset;

	public Vector3 grassForestColorOffset;

	public bool grassDetailEnabled;

	public bool fertileDetailEnabled;

	public bool rocksDetailEnabled;

	public Color waterClean;

	public Color waterDirty;

	public Color waterUnder;

	public Package.Asset waterNormalAsset;

	public Package.Asset waterFoamAsset;

	public float longitude;

	public float latitude;

	public float sunSize;

	public float sunAnisotropy;

	public Package.Asset moonTexture;

	public float moonSize;

	public Color moonInnerCorona;

	public Color moonOuterCorona;

	public float rayleight;

	public float mie;

	public float exposure;

	public float starsIntensity;

	public float outerSpaceIntensity;

	public Color skyTint;

	public Color nightHorizonColor;

	public Color earlyNightZenithColor;

	public Color lateNightZenithColor;

	public Package.Asset upwardRoadDiffuse;

	public Package.Asset downwardRoadDiffuse;

	public Package.Asset buildingFloorDiffuse;

	public Package.Asset buildingBaseDiffuse;

	public Package.Asset buildingBaseNormal;

	public Package.Asset buildingBurntDiffuse;

	public Package.Asset buildingAbandonedDiffuse;

	public Package.Asset lightColorPalette;

	public float minTemperatureDay;

	public float maxTemperatureDay;

	public float minTemperatureNight;

	public float maxTemperatureNight;

	public float minTemperatureRain;

	public float maxTemperatureRain;

	public float minTemperatureFog;

	public float maxTemperatureFog;

	public int rainProbabilityDay;

	public int rainProbabilityNight;

	public int fogProbabilityDay;

	public int fogProbabilityNight;

	public int northernLightsProbability;

	public DisasterProperties.DisasterSettings[] disasterSettings;
}
