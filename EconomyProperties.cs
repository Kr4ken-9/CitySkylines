﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class EconomyProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		EconomyProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new EconomyProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("EconomyProperties");
			Singleton<EconomyManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public EconomyManager.Bank[] m_banks = new EconomyManager.Bank[3];

	public int m_maxBailoutCount = 1;

	public int m_bailoutRepeatWeeks = 4;

	public int m_bailoutLimit = -1000000;

	public int m_bailoutAmount = 2000000;
}
