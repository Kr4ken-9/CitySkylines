﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class UnlockManager : SimulationManagerBase<UnlockManager, UnlockController>, ISimulationManager
{
	public event UnlockManager.MilestoneUnlockedHandler EventMilestoneUnlocked
	{
		add
		{
			UnlockManager.MilestoneUnlockedHandler milestoneUnlockedHandler = this.EventMilestoneUnlocked;
			UnlockManager.MilestoneUnlockedHandler milestoneUnlockedHandler2;
			do
			{
				milestoneUnlockedHandler2 = milestoneUnlockedHandler;
				milestoneUnlockedHandler = Interlocked.CompareExchange<UnlockManager.MilestoneUnlockedHandler>(ref this.EventMilestoneUnlocked, (UnlockManager.MilestoneUnlockedHandler)Delegate.Combine(milestoneUnlockedHandler2, value), milestoneUnlockedHandler);
			}
			while (milestoneUnlockedHandler != milestoneUnlockedHandler2);
		}
		remove
		{
			UnlockManager.MilestoneUnlockedHandler milestoneUnlockedHandler = this.EventMilestoneUnlocked;
			UnlockManager.MilestoneUnlockedHandler milestoneUnlockedHandler2;
			do
			{
				milestoneUnlockedHandler2 = milestoneUnlockedHandler;
				milestoneUnlockedHandler = Interlocked.CompareExchange<UnlockManager.MilestoneUnlockedHandler>(ref this.EventMilestoneUnlocked, (UnlockManager.MilestoneUnlockedHandler)Delegate.Remove(milestoneUnlockedHandler2, value), milestoneUnlockedHandler);
			}
			while (milestoneUnlockedHandler != milestoneUnlockedHandler2);
		}
	}

	public event Action m_milestonesUpdated
	{
		add
		{
			Action action = this.m_milestonesUpdated;
			Action action2;
			do
			{
				action2 = action;
				action = Interlocked.CompareExchange<Action>(ref this.m_milestonesUpdated, (Action)Delegate.Combine(action2, value), action);
			}
			while (action != action2);
		}
		remove
		{
			Action action = this.m_milestonesUpdated;
			Action action2;
			do
			{
				action2 = action;
				action = Interlocked.CompareExchange<Action>(ref this.m_milestonesUpdated, (Action)Delegate.Remove(action2, value), action);
			}
			while (action != action2);
		}
	}

	public void RegisterUnlockingPanel(UnlockingPanel panel)
	{
		this.m_UnlockingPanel = panel;
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_checkMilestones = new FastList<MilestoneInfo>();
		this.m_allMilestones = new Dictionary<string, MilestoneInfo>();
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (this.m_MilestonesWrapper == null)
		{
			this.m_MilestonesWrapper = new MilestonesWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_MilestonesWrapper != null)
		{
			this.m_MilestonesWrapper.Release();
			this.m_MilestonesWrapper = null;
		}
	}

	public override void InitializeProperties(UnlockController properties)
	{
		base.InitializeProperties(properties);
	}

	public override void DestroyProperties(UnlockController properties)
	{
		base.DestroyProperties(properties);
	}

	public void MilestonesUpdated()
	{
		if (this.m_milestonesUpdated != null)
		{
			this.m_milestonesUpdated();
		}
	}

	public void MilestonCheckNeeded(MilestoneInfo milestone)
	{
		while (!Monitor.TryEnter(this.m_checkMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			milestone.m_checkNeeded = true;
			this.m_checkMilestones.Add(milestone);
		}
		finally
		{
			Monitor.Exit(this.m_checkMilestones);
		}
	}

	public void ClearMonumentUnlock()
	{
		if (Singleton<ToolManager>.get_instance().m_properties == null)
		{
			return;
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
		{
			return;
		}
		if (this.m_properties != null)
		{
			int num = PrefabCollection<BuildingInfo>.LoadedCount();
			for (int i = 0; i < num; i++)
			{
				BuildingInfo loaded = PrefabCollection<BuildingInfo>.GetLoaded((uint)i);
				if (loaded != null)
				{
					if (loaded.m_UnlockMilestone != null && loaded.m_UnlockMilestone.m_isGlobal)
					{
						SavedBool unlocked = loaded.m_unlocked;
						if (unlocked != null && unlocked.get_value())
						{
							unlocked.set_value(false);
							loaded.m_alreadyUnlocked = false;
						}
					}
				}
			}
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				this.ClearMonumentUnlockImpl();
			});
		}
	}

	public void RefreshScenarioMilestones()
	{
		if (Singleton<ToolManager>.get_instance().m_properties == null)
		{
			return;
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
		{
			return;
		}
		if (this.m_properties != null && this.m_properties.m_ScenarioMilestones != null)
		{
			int[] passedCount = new int[this.m_properties.m_ScenarioMilestones.Length];
			bool flag = false;
			for (int i = 0; i < this.m_properties.m_ScenarioMilestones.Length; i++)
			{
				ManualMilestone manualMilestone = this.m_properties.m_ScenarioMilestones[i] as ManualMilestone;
				if (manualMilestone != null)
				{
					string name = manualMilestone.get_name();
					string text = "ScenarioWinCount[" + name + "]";
					SavedInt savedInt = new SavedInt(text, Settings.userGameState, 0, false);
					passedCount[i] = savedInt.get_value();
					if (manualMilestone.m_data == null || manualMilestone.m_data.m_progress != (long)passedCount[i])
					{
						flag = true;
					}
				}
			}
			if (flag)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					this.RefreshScenarioMilestonesImpl(passedCount);
				});
			}
		}
	}

	public TriggerMilestone AddScenarioTrigger()
	{
		return UnlockManager.AddScenarioTrigger(ref this.m_scenarioTriggers);
	}

	private static TriggerMilestone AddScenarioTrigger(ref TriggerMilestone[] target)
	{
		TriggerMilestone triggerMilestone = ScriptableObject.CreateInstance<TriggerMilestone>();
		triggerMilestone.m_triggerIndex = 1;
		int num = 0;
		if (target != null)
		{
			num = target.Length;
		}
		TriggerMilestone[] array = new TriggerMilestone[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = target[i];
			triggerMilestone.m_triggerIndex = Mathf.Max(triggerMilestone.m_triggerIndex, target[i].m_triggerIndex + 1);
		}
		array[num] = triggerMilestone;
		target = array;
		return triggerMilestone;
	}

	public void RemoveScenarioTrigger(TriggerMilestone info)
	{
		if (this.m_scenarioTriggers == null)
		{
			return;
		}
		for (int i = 0; i < this.m_scenarioTriggers.Length; i++)
		{
			if (this.m_scenarioTriggers[i] == info)
			{
				this.RemoveScenarioTrigger(i);
				return;
			}
		}
	}

	public void RemoveScenarioTrigger(int index)
	{
		if (this.m_scenarioTriggers == null)
		{
			return;
		}
		int num = this.m_scenarioTriggers.Length;
		if (index < 0 || index >= num)
		{
			return;
		}
		TriggerMilestone triggerMilestone = this.m_scenarioTriggers[index];
		TriggerMilestone[] array = new TriggerMilestone[num - 1];
		for (int i = 0; i < index; i++)
		{
			this.m_scenarioTriggers[i].RemoveCondition(triggerMilestone);
			array[i] = this.m_scenarioTriggers[i];
		}
		for (int j = index + 1; j < num; j++)
		{
			this.m_scenarioTriggers[j].RemoveCondition(triggerMilestone);
			array[j - 1] = this.m_scenarioTriggers[j];
		}
		this.m_scenarioTriggers = array;
		if (triggerMilestone != null)
		{
			Object.Destroy(triggerMilestone);
		}
	}

	[Obsolete("Use AddScenarioTrigger instead")]
	public T AddWinCondition<T>() where T : MilestoneInfo
	{
		T t = ScriptableObject.CreateInstance<T>();
		int num = 0;
		if (this.m_scenarioWinConditions != null)
		{
			num = this.m_scenarioWinConditions.Length;
		}
		MilestoneInfo[] array = new MilestoneInfo[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = this.m_scenarioWinConditions[i];
		}
		array[num] = t;
		this.m_scenarioWinConditions = array;
		return t;
	}

	[Obsolete("Use RemoveScenarioTrigger instead")]
	public void RemoveWinCondition(int index)
	{
		if (this.m_scenarioWinConditions == null)
		{
			return;
		}
		int num = this.m_scenarioWinConditions.Length;
		if (index < 0 || index >= num)
		{
			return;
		}
		MilestoneInfo milestoneInfo = this.m_scenarioWinConditions[index];
		MilestoneInfo[] array = new MilestoneInfo[num - 1];
		for (int i = 0; i < index; i++)
		{
			array[i] = this.m_scenarioWinConditions[i];
		}
		for (int j = index + 1; j < num; j++)
		{
			array[j - 1] = this.m_scenarioWinConditions[j];
		}
		this.m_scenarioWinConditions = array;
		if (milestoneInfo != null)
		{
			Object.Destroy(milestoneInfo);
		}
	}

	[Obsolete("Use AddScenarioTrigger instead")]
	public T AddLoseCondition<T>() where T : MilestoneInfo
	{
		T t = ScriptableObject.CreateInstance<T>();
		int num = 0;
		if (this.m_scenarioLoseConditions != null)
		{
			num = this.m_scenarioLoseConditions.Length;
		}
		MilestoneInfo[] array = new MilestoneInfo[num + 1];
		for (int i = 0; i < num; i++)
		{
			array[i] = this.m_scenarioLoseConditions[i];
		}
		array[num] = t;
		this.m_scenarioLoseConditions = array;
		return t;
	}

	[Obsolete("Use RemoveScenarioTrigger instead")]
	public void RemoveLoseCondition(int index)
	{
		if (this.m_scenarioLoseConditions == null)
		{
			return;
		}
		int num = this.m_scenarioLoseConditions.Length;
		if (index < 0 || index >= num)
		{
			return;
		}
		MilestoneInfo milestoneInfo = this.m_scenarioLoseConditions[index];
		MilestoneInfo[] array = new MilestoneInfo[num - 1];
		for (int i = 0; i < index; i++)
		{
			array[i] = this.m_scenarioLoseConditions[i];
		}
		for (int j = index + 1; j < num; j++)
		{
			array[j - 1] = this.m_scenarioLoseConditions[j];
		}
		this.m_scenarioLoseConditions = array;
		if (milestoneInfo != null)
		{
			Object.Destroy(milestoneInfo);
		}
	}

	public void UnlockAllProgressionMilestones()
	{
		if (this.m_properties == null)
		{
			return;
		}
		if (this.m_checkingMilestones)
		{
			for (int i = 0; i < this.m_properties.m_progressionMilestones.Length; i++)
			{
				this.CheckMilestoneImpl(this.m_properties.m_progressionMilestones[i], true, false);
			}
		}
		else
		{
			try
			{
				this.m_checkingMilestones = true;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
				for (int j = 0; j < this.m_properties.m_progressionMilestones.Length; j++)
				{
					this.CheckMilestoneImpl(this.m_properties.m_progressionMilestones[j], true, false);
				}
				int num = 0;
				while (this.m_milestoneChanged)
				{
					this.m_milestoneChanged = false;
					this.CheckAllMilestones();
					if (++num >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
					}
				}
				if (this.m_referencedMilestoneChanged)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.MilestonesUpdated();
					});
				}
			}
			finally
			{
				this.m_checkingMilestones = false;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
			}
		}
	}

	public void UnlockAllMonumentMilestones()
	{
		if (this.m_properties == null)
		{
			return;
		}
		if (this.m_checkingMilestones)
		{
			uint num = 0u;
			while ((ulong)num < (ulong)((long)PrefabCollection<BuildingInfo>.PrefabCount()))
			{
				BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab(num);
				if (prefab != null)
				{
					ItemClass @class = prefab.m_class;
					if (@class != null && @class.m_service == ItemClass.Service.Monument)
					{
						MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							this.CheckMilestoneImpl(unlockMilestone, true, false);
						}
					}
				}
				num += 1u;
			}
		}
		else
		{
			try
			{
				this.m_checkingMilestones = true;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
				uint num2 = 0u;
				while ((ulong)num2 < (ulong)((long)PrefabCollection<BuildingInfo>.PrefabCount()))
				{
					BuildingInfo prefab2 = PrefabCollection<BuildingInfo>.GetPrefab(num2);
					if (prefab2 != null)
					{
						ItemClass class2 = prefab2.m_class;
						if (class2 != null && class2.m_service == ItemClass.Service.Monument)
						{
							MilestoneInfo unlockMilestone2 = prefab2.m_UnlockMilestone;
							if (unlockMilestone2 != null)
							{
								this.CheckMilestoneImpl(unlockMilestone2, true, false);
							}
						}
					}
					num2 += 1u;
				}
				int num3 = 0;
				while (this.m_milestoneChanged)
				{
					this.m_milestoneChanged = false;
					this.CheckAllMilestones();
					if (++num3 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
					}
				}
				if (this.m_referencedMilestoneChanged)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.MilestonesUpdated();
					});
				}
			}
			finally
			{
				this.m_checkingMilestones = false;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
			}
		}
	}

	public void UnlockAllWonderMilestones()
	{
		if (this.m_properties == null)
		{
			return;
		}
		if (this.m_checkingMilestones)
		{
			uint num = 0u;
			while ((ulong)num < (ulong)((long)PrefabCollection<BuildingInfo>.PrefabCount()))
			{
				BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab(num);
				if (prefab != null && prefab.m_buildingAI.IsWonder())
				{
					MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
					if (unlockMilestone != null)
					{
						this.CheckMilestoneImpl(unlockMilestone, true, false);
					}
				}
				num += 1u;
			}
		}
		else
		{
			try
			{
				this.m_checkingMilestones = true;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
				uint num2 = 0u;
				while ((ulong)num2 < (ulong)((long)PrefabCollection<BuildingInfo>.PrefabCount()))
				{
					BuildingInfo prefab2 = PrefabCollection<BuildingInfo>.GetPrefab(num2);
					if (prefab2 != null && prefab2.m_buildingAI.IsWonder())
					{
						MilestoneInfo unlockMilestone2 = prefab2.m_UnlockMilestone;
						if (unlockMilestone2 != null)
						{
							this.CheckMilestoneImpl(unlockMilestone2, true, false);
						}
					}
					num2 += 1u;
				}
				int num3 = 0;
				while (this.m_milestoneChanged)
				{
					this.m_milestoneChanged = false;
					this.CheckAllMilestones();
					if (++num3 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
					}
				}
				if (this.m_referencedMilestoneChanged)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.MilestonesUpdated();
					});
				}
			}
			finally
			{
				this.m_checkingMilestones = false;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
			}
		}
	}

	public int CheckMilestone(MilestoneInfo milestone, bool forceUnlock, bool forceRelock)
	{
		if (milestone == null || this.m_properties == null)
		{
			return 0;
		}
		if (this.m_checkingMilestones)
		{
			return this.CheckMilestoneImpl(milestone, forceUnlock, forceRelock);
		}
		int result;
		try
		{
			this.m_checkingMilestones = true;
			this.m_milestoneChanged = false;
			this.m_referencedMilestoneChanged = false;
			int num = this.CheckMilestoneImpl(milestone, forceUnlock, forceRelock);
			int num2 = 0;
			while (this.m_milestoneChanged)
			{
				this.m_milestoneChanged = false;
				this.CheckAllMilestones();
				if (++num2 >= 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
				}
			}
			if (this.m_referencedMilestoneChanged)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.MilestonesUpdated();
				});
			}
			result = num;
		}
		finally
		{
			this.m_checkingMilestones = false;
			this.m_milestoneChanged = false;
		}
		return result;
	}

	private void ClearMonumentUnlockImpl()
	{
		if (this.m_properties == null)
		{
			return;
		}
		if (!this.m_checkingMilestones)
		{
			try
			{
				this.m_checkingMilestones = true;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
				int num = PrefabCollection<BuildingInfo>.PrefabCount();
				for (int i = 0; i < num; i++)
				{
					BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)i);
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_UnlockMilestone.m_isGlobal)
					{
						MilestoneInfo.Data data = prefab.m_UnlockMilestone.GetData();
						if (prefab.m_alreadyUnlocked)
						{
							if (data.m_passedCount == 0)
							{
								data.m_passedCount = 1;
								this.m_milestoneChanged = true;
								this.m_referencedMilestoneChanged = true;
							}
						}
						else if (data.m_passedCount == 1)
						{
							data.m_passedCount = 0;
							this.m_milestoneChanged = true;
							this.m_referencedMilestoneChanged = true;
						}
					}
				}
				int num2 = 0;
				while (this.m_milestoneChanged)
				{
					this.m_milestoneChanged = false;
					this.CheckAllMilestones();
					if (++num2 >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
					}
				}
				if (this.m_referencedMilestoneChanged)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.MilestonesUpdated();
					});
				}
			}
			finally
			{
				this.m_checkingMilestones = false;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
			}
		}
	}

	private void RefreshScenarioMilestonesImpl(int[] passedCount)
	{
		if (this.m_properties == null)
		{
			return;
		}
		if (!this.m_checkingMilestones)
		{
			try
			{
				this.m_checkingMilestones = true;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
				for (int i = 0; i < this.m_properties.m_ScenarioMilestones.Length; i++)
				{
					ManualMilestone manualMilestone = this.m_properties.m_ScenarioMilestones[i] as ManualMilestone;
					if (manualMilestone != null)
					{
						manualMilestone.SetPassedCount(passedCount[i]);
					}
				}
				int num = 0;
				while (this.m_milestoneChanged)
				{
					this.m_milestoneChanged = false;
					this.CheckAllMilestones();
					if (++num >= 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
					}
				}
				if (this.m_referencedMilestoneChanged)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.MilestonesUpdated();
					});
				}
			}
			finally
			{
				this.m_checkingMilestones = false;
				this.m_milestoneChanged = false;
				this.m_referencedMilestoneChanged = false;
			}
		}
	}

	private int CheckMilestoneImpl(MilestoneInfo milestone, bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = milestone.GetData();
		int passedCount = data.m_passedCount;
		if (milestone.m_checkDisabled)
		{
			return passedCount;
		}
		data = milestone.CheckPassed(forceUnlock, forceRelock);
		if (data.m_passedCount != passedCount)
		{
			if (data.m_passedCount != 0 && passedCount == 0)
			{
				int rewardCash = milestone.m_rewardCash;
				if (rewardCash != 0)
				{
					Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.RewardAmount, rewardCash * 100, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
				}
				if (!string.IsNullOrEmpty(milestone.m_steamAchievementName) && (milestone.m_isGlobal || Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True))
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						if (!PlatformService.get_achievements().get_Item(milestone.m_steamAchievementName).get_achieved())
						{
							PlatformService.get_achievements().get_Item(milestone.m_steamAchievementName).Unlock();
						}
					});
				}
				Singleton<MessageManager>.get_instance().TryCreateMessage(milestone.m_reachMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID());
				if (Singleton<TelemetryManager>.get_exists() && milestone.m_directReference)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						Singleton<TelemetryManager>.get_instance().MilestoneUnlocked(milestone);
					});
				}
				if (milestone.m_directReference)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						if (this.EventMilestoneUnlocked != null)
						{
							this.EventMilestoneUnlocked(milestone);
						}
					});
				}
				if (milestone.m_openUnlockPanel)
				{
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						if (this.m_UnlockingPanel != null)
						{
							this.m_UnlockingPanel.ShowModal();
						}
					});
				}
			}
			this.m_milestoneChanged = true;
			if (milestone.m_directReference)
			{
				this.m_referencedMilestoneChanged = true;
			}
		}
		milestone.m_checkNeeded = false;
		return data.m_passedCount;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		UnlockController properties = this.m_properties;
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None && subStep <= 1)
		{
			int frame = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (properties != null)
			{
				this.CheckMilestones(properties.m_progressionMilestones, frame);
				this.CheckMilestones(properties.m_FeatureMilestones, frame);
				this.CheckMilestones(properties.m_AreaMilestones, frame);
				this.CheckMilestones(properties.m_ZoneMilestones, frame);
				this.CheckMilestones(properties.m_ServiceMilestones, frame);
				this.CheckMilestones(properties.m_SubServiceMilestones, frame);
				this.CheckMilestones(properties.m_PolicyTypeMilestones, frame);
				this.CheckMilestones(properties.m_SpecializationMilestones, frame);
				this.CheckMilestones(properties.m_ServicePolicyMilestones, frame);
				this.CheckMilestones(properties.m_TaxationPolicyMilestones, frame);
				this.CheckMilestones(properties.m_CityPlanningPolicyMilestones, frame);
				this.CheckMilestones(properties.m_SpecialPolicyMilestones, frame);
				this.CheckMilestones(properties.m_EventPolicyMilestones, frame);
				this.CheckMilestones(properties.m_InfoModeMilestones, frame);
				this.CheckMilestones(properties.m_AchievementMilestones, frame);
			}
			this.CheckTriggers(this.m_scenarioTriggers, frame);
		}
		GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep <= 1 && properties2 != null)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (num != 173)
			{
				if (num == 210)
				{
					if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Zoning))
					{
						this.m_milestonesNotUsedGuide.Activate(properties2.m_milestonesNotUsed);
					}
				}
			}
			else if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Zoning) && !string.IsNullOrEmpty(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset))
			{
				this.m_scenarioGoalsNotUsedGuide.Activate(properties2.m_scenarioGoalsNotUsed);
			}
		}
		if (this.m_checkMilestones.m_size != 0)
		{
			while (!Monitor.TryEnter(this.m_checkMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				for (int i = 0; i < this.m_checkMilestones.m_size; i++)
				{
					if (this.m_checkMilestones.m_buffer[i].m_checkNeeded)
					{
						this.CheckMilestone(this.m_checkMilestones.m_buffer[i], false, false);
					}
				}
				this.m_checkMilestones.Clear();
			}
			finally
			{
				Monitor.Exit(this.m_checkMilestones);
			}
		}
	}

	private void CheckMilestones(MilestoneInfo[] infos, int frame)
	{
		if (infos != null)
		{
			int num = frame * infos.Length >> 8;
			int num2 = ((frame + 1) * infos.Length >> 8) - 1;
			for (int i = num; i <= num2; i++)
			{
				this.CheckMilestone(infos[i], false, false);
			}
		}
	}

	private void CheckTriggers(TriggerMilestone[] infos, int frame)
	{
		if (infos != null)
		{
			int num = frame * infos.Length >> 8;
			int num2 = ((frame + 1) * infos.Length >> 8) - 1;
			for (int i = num; i <= num2; i++)
			{
				TriggerMilestone.TriggerData triggerData = infos[i].GetData() as TriggerMilestone.TriggerData;
				if (triggerData.m_passedCount >= 1 && triggerData.m_repeatFrameIndex != 0u && triggerData.m_repeatFrameIndex <= Singleton<SimulationManager>.get_instance().m_currentFrameIndex)
				{
					this.CheckMilestone(infos[i], false, true);
				}
				this.CheckMilestone(infos[i], false, false);
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		this.ResetWrittenStatus();
	}

	public void ResetWrittenStatus()
	{
		UnlockController properties = this.m_properties;
		if (properties != null)
		{
			this.ResetWrittenStatus(properties.m_progressionMilestones);
			this.ResetWrittenStatus(properties.m_FeatureMilestones);
			this.ResetWrittenStatus(properties.m_AreaMilestones);
			this.ResetWrittenStatus(properties.m_ZoneMilestones);
			this.ResetWrittenStatus(properties.m_ServiceMilestones);
			this.ResetWrittenStatus(properties.m_SubServiceMilestones);
			this.ResetWrittenStatus(properties.m_PolicyTypeMilestones);
			this.ResetWrittenStatus(properties.m_SpecializationMilestones);
			this.ResetWrittenStatus(properties.m_ServicePolicyMilestones);
			this.ResetWrittenStatus(properties.m_TaxationPolicyMilestones);
			this.ResetWrittenStatus(properties.m_CityPlanningPolicyMilestones);
			this.ResetWrittenStatus(properties.m_SpecialPolicyMilestones);
			this.ResetWrittenStatus(properties.m_EventPolicyMilestones);
			this.ResetWrittenStatus(properties.m_InfoModeMilestones);
			this.ResetWrittenStatus(properties.m_AchievementMilestones);
		}
		this.ResetWrittenStatus(this.m_scenarioTriggers);
	}

	private void CheckAllMilestones()
	{
		UnlockController properties = this.m_properties;
		if (properties != null)
		{
			this.CheckMilestones(properties.m_progressionMilestones);
			this.CheckMilestones(properties.m_FeatureMilestones);
			this.CheckMilestones(properties.m_AreaMilestones);
			this.CheckMilestones(properties.m_ZoneMilestones);
			this.CheckMilestones(properties.m_ServiceMilestones);
			this.CheckMilestones(properties.m_SubServiceMilestones);
			this.CheckMilestones(properties.m_PolicyTypeMilestones);
			this.CheckMilestones(properties.m_SpecializationMilestones);
			this.CheckMilestones(properties.m_ServicePolicyMilestones);
			this.CheckMilestones(properties.m_TaxationPolicyMilestones);
			this.CheckMilestones(properties.m_CityPlanningPolicyMilestones);
			this.CheckMilestones(properties.m_SpecialPolicyMilestones);
			this.CheckMilestones(properties.m_EventPolicyMilestones);
			this.CheckMilestones(properties.m_InfoModeMilestones);
			this.CheckMilestones(properties.m_AchievementMilestones);
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			this.CheckMilestones(this.m_scenarioTriggers);
		}
		Singleton<BuildingManager>.get_instance().CheckAllMilestones();
		Singleton<NetManager>.get_instance().CheckAllMilestones();
		Singleton<TransportManager>.get_instance().CheckAllMilestones();
		Singleton<EventManager>.get_instance().CheckAllMilestones();
		Singleton<DisasterManager>.get_instance().CheckAllMilestones();
		Singleton<AudioManager>.get_instance().CheckAllMilestones();
	}

	private void ResetWrittenStatus(MilestoneInfo[] infos)
	{
		if (infos != null)
		{
			for (int i = 0; i < infos.Length; i++)
			{
				MilestoneInfo milestoneInfo = infos[i];
				if (milestoneInfo != null)
				{
					milestoneInfo.ResetWrittenStatus();
				}
			}
		}
	}

	private void CheckMilestones(MilestoneInfo[] infos)
	{
		if (infos != null)
		{
			for (int i = 0; i < infos.Length; i++)
			{
				this.CheckMilestone(infos[i], false, false);
			}
		}
	}

	public void DisableTriggers()
	{
		if (this.m_scenarioTriggers != null)
		{
			for (int i = 0; i < this.m_scenarioTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone = this.m_scenarioTriggers[i];
				triggerMilestone.m_checkDisabled = true;
				if (triggerMilestone.m_conditions != null)
				{
					for (int j = 0; j < triggerMilestone.m_conditions.Length; j++)
					{
						triggerMilestone.m_conditions[j].m_checkDisabled = true;
					}
				}
			}
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new UnlockManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("UnlockManager.UpdateData");
		base.UpdateData(mode);
		bool onlyUnwritten = false;
		switch (mode)
		{
		case SimulationManager.UpdateMode.NewMap:
		case SimulationManager.UpdateMode.LoadMap:
		case SimulationManager.UpdateMode.NewAsset:
		case SimulationManager.UpdateMode.LoadAsset:
			onlyUnwritten = false;
			break;
		case SimulationManager.UpdateMode.NewGameFromMap:
		case SimulationManager.UpdateMode.NewScenarioFromMap:
		case SimulationManager.UpdateMode.UpdateScenarioFromMap:
			onlyUnwritten = false;
			break;
		case SimulationManager.UpdateMode.LoadGame:
		case SimulationManager.UpdateMode.NewScenarioFromGame:
		case SimulationManager.UpdateMode.LoadScenario:
		case SimulationManager.UpdateMode.NewGameFromScenario:
		case SimulationManager.UpdateMode.UpdateScenarioFromGame:
			onlyUnwritten = true;
			break;
		}
		this.ResetMilestones(onlyUnwritten);
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_milestonesNotUsedGuide == null)
		{
			this.m_milestonesNotUsedGuide = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || this.m_scenarioGoalsNotUsedGuide == null)
		{
			this.m_scenarioGoalsNotUsedGuide = new GenericGuide();
		}
		while (!Monitor.TryEnter(this.m_checkMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_checkMilestones.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_checkMilestones);
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public void ResetMilestones(bool onlyUnwritten)
	{
		if (this.m_properties != null)
		{
			UnlockManager.ResetMilestones(this.m_properties.m_progressionMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_FeatureMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_AreaMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_ZoneMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_ServiceMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_SubServiceMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_PolicyTypeMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_SpecializationMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_ServicePolicyMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_TaxationPolicyMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_CityPlanningPolicyMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_SpecialPolicyMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_EventPolicyMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_InfoModeMilestones, onlyUnwritten);
			UnlockManager.ResetMilestones(this.m_properties.m_AchievementMilestones, onlyUnwritten);
		}
		UnlockManager.ResetMilestones(this.m_scenarioTriggers, onlyUnwritten);
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("UnlockManager.LateUpdateData");
		base.LateUpdateData(mode);
		try
		{
			this.m_checkingMilestones = true;
			this.m_milestoneChanged = false;
			this.m_referencedMilestoneChanged = false;
			this.m_MilestonesWrapper.OnRefreshMilestones();
			if (this.m_fixMilestones && (mode == SimulationManager.UpdateMode.LoadScenario || mode == SimulationManager.UpdateMode.NewGameFromScenario))
			{
				this.FixMilestones();
			}
			int num = 0;
			while (this.m_milestoneChanged)
			{
				this.m_milestoneChanged = false;
				this.CheckAllMilestones();
				if (++num >= 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Internal error!\n" + Environment.StackTrace);
				}
			}
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.MilestonesUpdated();
				this.RefreshScenarioMilestones();
			});
		}
		finally
		{
			this.m_checkingMilestones = false;
			this.m_milestoneChanged = false;
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public void FixMilestones()
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 1; i < 36864; i++)
		{
			if ((instance.m_segments.m_buffer[i].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Original)) == NetSegment.Flags.Created)
			{
				NetInfo info = instance.m_segments.m_buffer[i].Info;
				if (info != null)
				{
					PlayerNetAI playerNetAI = info.m_netAI as PlayerNetAI;
					if (playerNetAI != null && playerNetAI.m_createPassMilestone != null)
					{
						playerNetAI.m_createPassMilestone.Unlock();
					}
				}
			}
		}
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		for (int j = 1; j < 49152; j++)
		{
			if ((instance2.m_buildings.m_buffer[j].m_flags & (Building.Flags.Created | Building.Flags.Original)) == Building.Flags.Created)
			{
				BuildingInfo info2 = instance2.m_buildings.m_buffer[j].Info;
				if (info2 != null)
				{
					PlayerBuildingAI playerBuildingAI = info2.m_buildingAI as PlayerBuildingAI;
					if (playerBuildingAI != null && playerBuildingAI.m_createPassMilestone != null)
					{
						playerBuildingAI.m_createPassMilestone.Unlock();
					}
					if (playerBuildingAI != null && playerBuildingAI.m_createPassMilestone2 != null)
					{
						playerBuildingAI.m_createPassMilestone2.Unlock();
					}
					if (playerBuildingAI != null && playerBuildingAI.m_createPassMilestone3 != null)
					{
						playerBuildingAI.m_createPassMilestone3.Unlock();
					}
				}
			}
		}
	}

	public static void ResetMilestones(MilestoneInfo[] infos, bool onlyUnwritten)
	{
		if (infos != null)
		{
			uint num = (uint)infos.Length;
			for (uint num2 = 0u; num2 < num; num2 += 1u)
			{
				if (infos[(int)((UIntPtr)num2)] != null)
				{
					infos[(int)((UIntPtr)num2)].Reset(onlyUnwritten);
				}
			}
		}
	}

	public static void SerializeMilestones(DataSerializer s, MilestoneInfo[] infos)
	{
		uint num;
		if (infos != null)
		{
			num = (uint)infos.Length;
		}
		else
		{
			num = 0u;
		}
		s.WriteUInt16(num);
		for (uint num2 = 0u; num2 < num; num2 += 1u)
		{
			if (infos[(int)((UIntPtr)num2)] != null)
			{
				MilestoneInfo.Data data = infos[(int)((UIntPtr)num2)].GetData();
				s.WriteObject<MilestoneInfo.Data>(data);
			}
			else
			{
				s.WriteObject<MilestoneInfo.Data>(null);
			}
		}
	}

	public static MilestoneInfo.Data[] DeserializeMilestones(DataSerializer s)
	{
		uint num = s.ReadUInt16();
		MilestoneInfo.Data[] array = new MilestoneInfo.Data[num];
		for (uint num2 = 0u; num2 < num; num2 += 1u)
		{
			array[(int)((UIntPtr)num2)] = s.ReadObject<MilestoneInfo.Data>();
		}
		return array;
	}

	public static void AfterDeserializeMilestones(DataSerializer s, MilestoneInfo[] infos, MilestoneInfo.Data[] data)
	{
		if (data != null && infos != null)
		{
			int num = Mathf.Min(data.Length, infos.Length);
			for (int i = 0; i < num; i++)
			{
				if (infos[i] != null)
				{
					infos[i].SetData(data[i]);
				}
			}
		}
	}

	public bool Unlocked(MilestoneInfo milestone)
	{
		if (this.m_properties == null || milestone == null)
		{
			return true;
		}
		MilestoneInfo.Data data = milestone.m_data;
		return data != null && data.m_passedCount != 0;
	}

	public MilestoneInfo GetCurrentMilestone()
	{
		if (this.m_properties != null)
		{
			for (int i = 0; i < this.m_properties.m_progressionMilestones.Length; i++)
			{
				if (!this.Unlocked(this.m_properties.m_progressionMilestones[i]))
				{
					return this.m_properties.m_progressionMilestones[i];
				}
			}
		}
		return null;
	}

	public bool Unlocked(UnlockManager.Feature feature)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_FeatureMilestones[(int)feature]);
		}
		return this.Unlocked(null);
	}

	public bool Unlocked(int areaIndex)
	{
		if (this.m_properties != null)
		{
			MilestoneInfo[] areaMilestones = this.m_properties.m_AreaMilestones;
			return this.Unlocked(areaMilestones[Mathf.Clamp(areaIndex, 0, areaMilestones.Length - 1)]);
		}
		return this.Unlocked(null);
	}

	public bool Unlocked(ItemClass.Zone zone)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_ZoneMilestones[(int)zone]);
		}
		return this.Unlocked(null);
	}

	public bool Unlocked(ItemClass.Service service)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_ServiceMilestones[(int)service]);
		}
		return this.Unlocked(null);
	}

	public bool Unlocked(ItemClass.SubService subService)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_SubServiceMilestones[(int)subService]);
		}
		return this.Unlocked(null);
	}

	public bool Unlocked(DistrictPolicies.Types type)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_PolicyTypeMilestones[(int)type]);
		}
		return this.Unlocked(null);
	}

	public MilestoneInfo GetMilestone(DistrictPolicies.Policies policy)
	{
		if (this.m_properties == null)
		{
			return null;
		}
		switch (policy >> 5)
		{
		case DistrictPolicies.Policies.Forest:
			return this.m_properties.m_SpecializationMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		case DistrictPolicies.Policies.Farming:
			return this.m_properties.m_ServicePolicyMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		case DistrictPolicies.Policies.Oil:
			return this.m_properties.m_TaxationPolicyMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		case DistrictPolicies.Policies.Ore:
			return this.m_properties.m_CityPlanningPolicyMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		case DistrictPolicies.Policies.Leisure:
			return this.m_properties.m_SpecialPolicyMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		case DistrictPolicies.Policies.Tourist:
			return this.m_properties.m_EventPolicyMilestones[(int)(policy & (DistrictPolicies.Policies)31)];
		default:
			return null;
		}
	}

	public bool Unlocked(DistrictPolicies.Policies policy)
	{
		return this.Unlocked(this.GetMilestone(policy));
	}

	public bool Unlocked(InfoManager.InfoMode infoMode)
	{
		if (this.m_properties != null)
		{
			return this.Unlocked(this.m_properties.m_InfoModeMilestones[(int)infoMode]);
		}
		return this.Unlocked(null);
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public TriggerMilestone[] m_scenarioTriggers;

	[Obsolete("Use m_scenarioTriggers instead")]
	public MilestoneInfo[] m_scenarioWinConditions;

	[Obsolete("Use m_scenarioTriggers instead")]
	public MilestoneInfo[] m_scenarioLoseConditions;

	private bool m_checkingMilestones;

	private bool m_milestoneChanged;

	private bool m_referencedMilestoneChanged;

	private bool m_fixMilestones;

	[NonSerialized]
	public MilestonesWrapper m_MilestonesWrapper;

	[NonSerialized]
	public GenericGuide m_milestonesNotUsedGuide;

	[NonSerialized]
	public GenericGuide m_scenarioGoalsNotUsedGuide;

	[NonSerialized]
	public FastList<MilestoneInfo> m_checkMilestones;

	[NonSerialized]
	public Dictionary<string, MilestoneInfo> m_allMilestones;

	private UnlockingPanel m_UnlockingPanel;

	public enum Feature
	{
		Zoning,
		[EnumPosition("General", "Districts", 0)]
		Districts,
		[EnumPosition("General", "Policies", 1)]
		Policies,
		Economy,
		Budget,
		[EnumPosition("General", "Taxes", 2)]
		Taxes,
		[EnumPosition("General", "Loans", 3)]
		Loans,
		GameAreas,
		Bulldozer,
		TaxDetails,
		DeathCare,
		[EnumPosition("General", "Wonders", 4)]
		Wonders,
		[EnumPosition("General", "SecondLoan", 5)]
		SecondLoan,
		[EnumPosition("General", "ThirdLoan", 6)]
		ThirdLoan,
		[EnumPosition("Services", "MonumentLevel2", 6)]
		MonumentLevel2,
		[EnumPosition("Services", "MonumentLevel3", 6)]
		MonumentLevel3,
		[EnumPosition("Services", "MonumentLevel4", 6)]
		MonumentLevel4,
		[EnumPosition("Services", "MonumentLevel5", 6)]
		MonumentLevel5,
		[EnumPosition("Services", "MonumentLevel6", 6)]
		MonumentLevel6,
		InfoViews,
		EducationLevel2,
		EducationLevel3,
		IndustrySpecializations,
		[EnumPosition("Services", "MonumentExpansion1", 6)]
		CommercialSpecialization,
		Snowplow,
		[EnumPosition("General", "Landscaping", 7)]
		Landscaping,
		[EnumPosition("Services", "MonumentFootball", 6)]
		Football,
		WaterPumping,
		DisasterResponse,
		[EnumPosition("Services", "Ferry", 6)]
		Ferry,
		[EnumPosition("Services", "Blimp", 6)]
		Blimp,
		[EnumPosition("Services", "MonumentConcerts", 6)]
		Concerts,
		OfficeSpecializations,
		ResidentialSpecializations,
		CommercialSpecializationGC,
		None = -1
	}

	public delegate void MilestoneUnlockedHandler(MilestoneInfo info);

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "UnlockManager");
			UnlockManager instance = Singleton<UnlockManager>.get_instance();
			TriggerMilestone[] scenarioTriggers = instance.m_scenarioTriggers;
			if (scenarioTriggers != null)
			{
				s.WriteInt32(scenarioTriggers.Length);
			}
			else
			{
				s.WriteInt32(0);
			}
			if (instance.m_properties != null)
			{
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_progressionMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_FeatureMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_AreaMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_ZoneMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_ServiceMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_SubServiceMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_PolicyTypeMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_SpecializationMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_ServicePolicyMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_TaxationPolicyMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_CityPlanningPolicyMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_SpecialPolicyMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_EventPolicyMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_InfoModeMilestones);
				UnlockManager.SerializeMilestones(s, instance.m_properties.m_AchievementMilestones);
			}
			else
			{
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
				UnlockManager.SerializeMilestones(s, null);
			}
			if (scenarioTriggers != null)
			{
				for (int i = 0; i < scenarioTriggers.Length; i++)
				{
					TriggerMilestone triggerMilestone = scenarioTriggers[i];
					if (triggerMilestone != null)
					{
						triggerMilestone.Serialize(s);
					}
				}
			}
			UnlockManager.SerializeMilestones(s, scenarioTriggers);
			s.WriteObject<GenericGuide>(instance.m_milestonesNotUsedGuide);
			s.WriteObject<GenericGuide>(instance.m_scenarioGoalsNotUsedGuide);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "UnlockManager");
		}

		private void RefreshScenarioConditions(int triggerCount, ref TriggerMilestone[] target, bool disableTriggers, Type[] winConditionTypes, Type[] loseConditionTypes)
		{
			TriggerMilestone[] array = target;
			target = null;
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					Object.Destroy(array[i]);
				}
				array = null;
			}
			if (triggerCount != 0)
			{
				array = new TriggerMilestone[triggerCount];
				for (int j = 0; j < array.Length; j++)
				{
					array[j] = (ScriptableObject.CreateInstance(typeof(TriggerMilestone)) as TriggerMilestone);
					array[j].m_checkDisabled = disableTriggers;
				}
			}
			target = array;
			if (winConditionTypes != null && winConditionTypes.Length != 0)
			{
				this.m_winConditions = new MilestoneInfo[winConditionTypes.Length];
				for (int k = 0; k < winConditionTypes.Length; k++)
				{
					Type type = winConditionTypes[k];
					if (type != null)
					{
						MilestoneInfo milestoneInfo = ScriptableObject.CreateInstance(type) as MilestoneInfo;
						this.m_winConditions[k] = milestoneInfo;
					}
				}
				TriggerMilestone triggerMilestone = UnlockManager.AddScenarioTrigger(ref target);
				triggerMilestone.m_conditions = this.m_winConditions;
				triggerMilestone.m_checkDisabled = disableTriggers;
				triggerMilestone.AddEffect<WinEffect>();
			}
			if (loseConditionTypes != null && loseConditionTypes.Length != 0)
			{
				this.m_loseConditions = new MilestoneInfo[loseConditionTypes.Length];
				for (int l = 0; l < loseConditionTypes.Length; l++)
				{
					Type type2 = loseConditionTypes[l];
					if (type2 != null)
					{
						MilestoneInfo milestoneInfo2 = ScriptableObject.CreateInstance(type2) as MilestoneInfo;
						this.m_loseConditions[l] = milestoneInfo2;
					}
				}
				TriggerMilestone triggerMilestone2 = UnlockManager.AddScenarioTrigger(ref target);
				triggerMilestone2.m_conditions = this.m_loseConditions;
				triggerMilestone2.m_checkDisabled = disableTriggers;
				triggerMilestone2.AddEffect<LoseEffect>();
			}
		}

		private void DestroyTriggers(ref TriggerMilestone[] target)
		{
			TriggerMilestone[] array = target;
			target = null;
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					Object.Destroy(array[i]);
				}
			}
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "UnlockManager");
			UnlockManager u = Singleton<UnlockManager>.get_instance();
			int triggerCount = 0;
			Type[] winConditionTypes = null;
			Type[] loseConditionTypes = null;
			if (s.get_version() >= 279u)
			{
				triggerCount = s.ReadInt32();
			}
			else if (s.get_version() >= 262u)
			{
				int num = (int)s.ReadUInt16();
				if (num != 0)
				{
					winConditionTypes = new Type[num];
					for (int i = 0; i < num; i++)
					{
						winConditionTypes[i] = s.ReadSharedType();
					}
				}
				int num2 = (int)s.ReadUInt16();
				if (num2 != 0)
				{
					loseConditionTypes = new Type[num2];
					for (int j = 0; j < num2; j++)
					{
						loseConditionTypes[j] = s.ReadSharedType();
					}
				}
			}
			Task task = null;
			TriggerMilestone[] target = null;
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
			{
				target = u.m_scenarioTriggers;
			}
			bool disableTriggers = false;
			if (updateMode == SimulationManager.UpdateMode.LoadGame)
			{
				disableTriggers = (Singleton<SimulationManager>.get_instance().m_metaData.m_scenarioWon || Singleton<SimulationManager>.get_instance().m_metaData.m_scenarioFailed);
			}
			if (triggerCount != 0 || (target != null && target.Length != 0) || (winConditionTypes != null && winConditionTypes.Length != 0) || (loseConditionTypes != null && loseConditionTypes.Length != 0))
			{
				if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
				{
					task = ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.RefreshScenarioConditions(triggerCount, ref u.m_scenarioTriggers, disableTriggers, winConditionTypes, loseConditionTypes);
						target = u.m_scenarioTriggers;
					});
				}
				else
				{
					task = ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.RefreshScenarioConditions(triggerCount, ref target, disableTriggers, winConditionTypes, loseConditionTypes);
					});
				}
			}
			if (s.get_version() >= 127u)
			{
				this.m_progressionMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_FeatureMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_AreaMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_ZoneMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_ServiceMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_SubServiceMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_PolicyTypeMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_IndustryPolicyMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_ServicePolicyMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_TaxationPolicyMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_CityPlanningPolicyMilestones = UnlockManager.DeserializeMilestones(s);
				this.m_SpecialPolicyMilestones = UnlockManager.DeserializeMilestones(s);
				if (s.get_version() >= 248u)
				{
					this.m_EventPolicyMilestones = UnlockManager.DeserializeMilestones(s);
				}
				this.m_InfoModeMilestones = UnlockManager.DeserializeMilestones(s);
				if (s.get_version() >= 177u)
				{
					this.m_AchievementMilestones = UnlockManager.DeserializeMilestones(s);
				}
			}
			else if (s.get_version() >= 93u)
			{
				this.m_progressionMilestones = new MilestoneInfo.Data[13];
				for (int k = 0; k < 27; k++)
				{
					if (k >= 1 && k <= 13)
					{
						this.m_progressionMilestones[k - 1] = new MilestoneInfo.Data();
						this.m_progressionMilestones[k - 1].m_passedCount = ((s.ReadUInt32() != 4294967295u) ? 0 : 1);
					}
					else
					{
						s.ReadUInt32();
					}
				}
				s.ReadUInt32();
			}
			else if (s.get_version() >= 91u)
			{
				this.m_progressionMilestones = new MilestoneInfo.Data[13];
				for (int l = 0; l < 24; l++)
				{
					if (l >= 1 && l <= 13)
					{
						this.m_progressionMilestones[l - 1] = new MilestoneInfo.Data();
						this.m_progressionMilestones[l - 1].m_passedCount = ((s.ReadUInt32() != 4294967295u) ? 0 : 1);
					}
					else
					{
						s.ReadUInt32();
					}
				}
				s.ReadUInt32();
			}
			else if (s.get_version() >= 88u)
			{
				this.m_progressionMilestones = new MilestoneInfo.Data[13];
				for (int m = 0; m < 18; m++)
				{
					if (m >= 1 && m <= 13)
					{
						this.m_progressionMilestones[m - 1] = new MilestoneInfo.Data();
						this.m_progressionMilestones[m - 1].m_passedCount = ((s.ReadUInt32() != 4294967295u) ? 0 : 1);
					}
					else
					{
						s.ReadUInt32();
					}
				}
			}
			else if (s.get_version() >= 83u)
			{
				this.m_progressionMilestones = new MilestoneInfo.Data[13];
				for (int n = 0; n < 14; n++)
				{
					if (n >= 1 && n <= 13)
					{
						this.m_progressionMilestones[n - 1] = new MilestoneInfo.Data();
						this.m_progressionMilestones[n - 1].m_passedCount = ((s.ReadUInt32() != 4294967295u) ? 0 : 1);
					}
					else
					{
						s.ReadUInt32();
					}
				}
			}
			else
			{
				this.m_progressionMilestones = new MilestoneInfo.Data[13];
				for (int num3 = 0; num3 < 14; num3++)
				{
					if (num3 >= 1 && num3 <= 13)
					{
						this.m_progressionMilestones[num3 - 1] = new MilestoneInfo.Data();
						this.m_progressionMilestones[num3 - 1].m_passedCount = ((s.ReadUInt32() != 4294967295u) ? 0 : 1);
					}
					else
					{
						s.ReadBool();
					}
				}
			}
			if (task != null)
			{
				Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.PauseLoading();
				task.Wait();
				Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.ContinueLoading();
			}
			Dictionary<int, TriggerMilestone> dictionary = new Dictionary<int, TriggerMilestone>();
			for (int num4 = 0; num4 < triggerCount; num4++)
			{
				TriggerMilestone triggerMilestone = target[num4];
				if (triggerMilestone != null)
				{
					triggerMilestone.Deserialize(s);
					dictionary[triggerMilestone.m_triggerIndex] = triggerMilestone;
				}
			}
			for (int num5 = 0; num5 < triggerCount; num5++)
			{
				TriggerMilestone triggerMilestone2 = target[num5];
				if (triggerMilestone2 != null)
				{
					triggerMilestone2.AssignTriggers(dictionary);
				}
			}
			if ((updateMode == SimulationManager.UpdateMode.UpdateScenarioFromGame || updateMode == SimulationManager.UpdateMode.UpdateScenarioFromMap) && target != null && target.Length != 0)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					this.DestroyTriggers(ref target);
				});
			}
			if (this.m_winConditions != null)
			{
				for (int num6 = 0; num6 < this.m_winConditions.Length; num6++)
				{
					MilestoneInfo milestoneInfo = this.m_winConditions[num6];
					if (milestoneInfo != null)
					{
						milestoneInfo.Deserialize(s);
					}
				}
			}
			if (this.m_loseConditions != null)
			{
				for (int num7 = 0; num7 < this.m_loseConditions.Length; num7++)
				{
					MilestoneInfo milestoneInfo2 = this.m_loseConditions[num7];
					if (milestoneInfo2 != null)
					{
						milestoneInfo2.Deserialize(s);
					}
				}
			}
			if (s.get_version() >= 279u)
			{
				this.m_scenarioTriggers = UnlockManager.DeserializeMilestones(s);
			}
			else if (s.get_version() >= 262u)
			{
				this.m_scenarioWinConditions = UnlockManager.DeserializeMilestones(s);
				this.m_scenarioLoseConditions = UnlockManager.DeserializeMilestones(s);
			}
			if (s.get_version() >= 134u)
			{
				u.m_milestonesNotUsedGuide = s.ReadObject<GenericGuide>();
			}
			else
			{
				u.m_milestonesNotUsedGuide = null;
			}
			if (s.get_version() >= 295u)
			{
				u.m_scenarioGoalsNotUsedGuide = s.ReadObject<GenericGuide>();
			}
			else
			{
				u.m_scenarioGoalsNotUsedGuide = null;
			}
			while (!Monitor.TryEnter(u.m_allMilestones, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				u.m_allMilestones.Clear();
			}
			finally
			{
				Monitor.Exit(u.m_allMilestones);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "UnlockManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "UnlockManager");
			UnlockManager instance = Singleton<UnlockManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			if (instance.m_properties != null)
			{
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_progressionMilestones, this.m_progressionMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_FeatureMilestones, this.m_FeatureMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_AreaMilestones, this.m_AreaMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_ZoneMilestones, this.m_ZoneMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_ServiceMilestones, this.m_ServiceMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_SubServiceMilestones, this.m_SubServiceMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_PolicyTypeMilestones, this.m_PolicyTypeMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_SpecializationMilestones, this.m_IndustryPolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_ServicePolicyMilestones, this.m_ServicePolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_TaxationPolicyMilestones, this.m_TaxationPolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_CityPlanningPolicyMilestones, this.m_CityPlanningPolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_SpecialPolicyMilestones, this.m_SpecialPolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_EventPolicyMilestones, this.m_EventPolicyMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_InfoModeMilestones, this.m_InfoModeMilestones);
				UnlockManager.AfterDeserializeMilestones(s, instance.m_properties.m_AchievementMilestones, this.m_AchievementMilestones);
			}
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
			{
				UnlockManager.AfterDeserializeMilestones(s, instance.m_scenarioTriggers, this.m_scenarioTriggers);
				UnlockManager.AfterDeserializeMilestones(s, this.m_winConditions, this.m_scenarioWinConditions);
				UnlockManager.AfterDeserializeMilestones(s, this.m_loseConditions, this.m_scenarioLoseConditions);
			}
			instance.m_fixMilestones = (s.get_version() < 299u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "UnlockManager");
		}

		private MilestoneInfo.Data[] m_progressionMilestones;

		private MilestoneInfo.Data[] m_FeatureMilestones;

		private MilestoneInfo.Data[] m_AreaMilestones;

		private MilestoneInfo.Data[] m_ZoneMilestones;

		private MilestoneInfo.Data[] m_ServiceMilestones;

		private MilestoneInfo.Data[] m_SubServiceMilestones;

		private MilestoneInfo.Data[] m_PolicyTypeMilestones;

		private MilestoneInfo.Data[] m_IndustryPolicyMilestones;

		private MilestoneInfo.Data[] m_ServicePolicyMilestones;

		private MilestoneInfo.Data[] m_TaxationPolicyMilestones;

		private MilestoneInfo.Data[] m_CityPlanningPolicyMilestones;

		private MilestoneInfo.Data[] m_SpecialPolicyMilestones;

		private MilestoneInfo.Data[] m_EventPolicyMilestones;

		private MilestoneInfo.Data[] m_InfoModeMilestones;

		private MilestoneInfo.Data[] m_AchievementMilestones;

		private MilestoneInfo.Data[] m_scenarioTriggers;

		private MilestoneInfo.Data[] m_scenarioWinConditions;

		private MilestoneInfo.Data[] m_scenarioLoseConditions;

		private MilestoneInfo[] m_winConditions;

		private MilestoneInfo[] m_loseConditions;
	}
}
