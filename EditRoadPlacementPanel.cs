﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class EditRoadPlacementPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		PrefabInfo editPrefabInfo = ToolsModifierControl.toolController.m_editPrefabInfo;
		string tooltip = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Title,
			Locale.Get("CUSTOM_ROAD_TOOLTIP_TITLE"),
			LocaleFormatter.Sprite,
			"Basic Road",
			LocaleFormatter.Text,
			Locale.Get("CUSTOM_ROAD_TOOLTIP_DESC")
		});
		this.SpawnEntry(editPrefabInfo.get_name(), tooltip, editPrefabInfo.m_Thumbnail, editPrefabInfo.m_Atlas, GeneratedPanel.tooltipBox, true).set_objectUserData(editPrefabInfo);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		NetInfo netInfo = comp.get_objectUserData() as NetInfo;
		if (netInfo != null)
		{
			base.ShowRoadsOptionPanel();
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				netTool.Prefab = netInfo;
			}
		}
	}
}
