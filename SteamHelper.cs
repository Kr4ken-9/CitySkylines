﻿using System;
using System.Collections.Generic;
using ColossalFramework.PlatformServices;

public static class SteamHelper
{
	public static SteamHelper.DLC_BitMask GetOwnedDLCMask()
	{
		SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)0;
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.DeluxeDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.DeluxeDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.AfterDarkDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.SnowFallDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.NaturalDisastersDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.InMotionDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.GreenCitiesDLC;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.Football;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football2))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.Football2345;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football3))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.Football2345;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football4))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.Football2345;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.Football5))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.Football2345;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation1))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.RadioStation1;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.RadioStation2))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.RadioStation2;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.MusicFestival))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.MusicFestival;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack1))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.ModderPack1;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack2))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.ModderPack2;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack3))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.ModderPack3;
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.OrientalBuildings))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.OrientalBuildings;
		}
		return dLC_BitMask;
	}

	public static bool IsDLCOwned(SteamHelper.DLC dlc)
	{
		return dlc != SteamHelper.DLC.NotAllowed && (dlc == SteamHelper.DLC.None || PlatformService.IsDlcInstalled((uint)dlc));
	}

	public static bool IsDLCAvailable(SteamHelper.DLC dlc)
	{
		return dlc != SteamHelper.DLC.NotAllowed && (dlc == SteamHelper.DLC.None || PlatformService.IsDlcAvailable((uint)dlc));
	}

	public static bool IsAppOwned(uint app)
	{
		return PlatformService.IsAppOwned(app);
	}

	public static bool IsSomeStadiumDLCOwned()
	{
		return SteamHelper.IsDLCOwned(SteamHelper.DLC.Football2) || SteamHelper.IsDLCOwned(SteamHelper.DLC.Football3) || SteamHelper.IsDLCOwned(SteamHelper.DLC.Football4) || SteamHelper.IsDLCOwned(SteamHelper.DLC.Football5);
	}

	private static string ServiceToTag(ItemClass.Service service, PrefabAI ai)
	{
		switch (service)
		{
		case ItemClass.Service.Residential:
			return "Residential";
		case ItemClass.Service.Commercial:
			return "Commercial";
		case ItemClass.Service.Industrial:
			return "Industrial";
		case ItemClass.Service.Office:
			return "Office";
		case ItemClass.Service.Electricity:
			return "Electricity";
		case ItemClass.Service.Water:
			return "Water & Sewage";
		case ItemClass.Service.Garbage:
			return "Garbage";
		case ItemClass.Service.HealthCare:
			if (ai is HospitalAI)
			{
				return "Healthcare";
			}
			if (ai is CemeteryAI)
			{
				return "Deathcare";
			}
			return null;
		case ItemClass.Service.PoliceDepartment:
			return "Police Department";
		case ItemClass.Service.Education:
			return "Education";
		case ItemClass.Service.Monument:
			return "Unique Building";
		case ItemClass.Service.FireDepartment:
		case ItemClass.Service.Disaster:
			return "Fire Department";
		case ItemClass.Service.PublicTransport:
			return "Transport";
		}
		return null;
	}

	private static string SubServiceToTag(ItemClass.SubService service)
	{
		switch (service)
		{
		case ItemClass.SubService.ResidentialLow:
			return "Residential Low";
		case ItemClass.SubService.ResidentialHigh:
			return "Residential High";
		case ItemClass.SubService.CommercialLow:
			return "Commercial Low";
		case ItemClass.SubService.CommercialHigh:
			return "Commercial High";
		case ItemClass.SubService.IndustrialGeneric:
			return "Industrial Generic";
		case ItemClass.SubService.IndustrialForestry:
			return "Industrial Forestry";
		case ItemClass.SubService.IndustrialFarming:
			return "Industrial Farming";
		case ItemClass.SubService.IndustrialOil:
			return "Industrial Oil";
		case ItemClass.SubService.IndustrialOre:
			return "Industrial Ore";
		case ItemClass.SubService.PublicTransportBus:
			return "Transport Bus";
		case ItemClass.SubService.PublicTransportMetro:
			return "Transport Metro";
		case ItemClass.SubService.PublicTransportTrain:
			return "Transport Train";
		case ItemClass.SubService.PublicTransportShip:
			return "Transport Ship";
		case ItemClass.SubService.PublicTransportPlane:
			return "Transport Plane";
		case ItemClass.SubService.PublicTransportTaxi:
			return "Transport Taxi";
		case ItemClass.SubService.PublicTransportTram:
			return "Transport Tram";
		case ItemClass.SubService.CommercialLeisure:
			return "Commercial Leisure";
		case ItemClass.SubService.CommercialTourist:
			return "Commercial Tourism";
		case ItemClass.SubService.PublicTransportMonorail:
			return "Transport Monorail";
		case ItemClass.SubService.PublicTransportCableCar:
			return "Transport Cable Car";
		}
		return null;
	}

	public static string[] GetSteamTags(this PrefabInfo info)
	{
		List<string> list = new List<string>();
		if (info == null)
		{
			return list.ToArray();
		}
		if (info is BuildingInfo)
		{
			PrefabAI aI = info.GetAI();
			if (aI is IntersectionAI)
			{
				list.Add("Intersection");
			}
			else if (aI is ParkAI)
			{
				list.Add("Park");
			}
			else
			{
				list.Add("Building");
				BuildingAI buildingAI = aI as BuildingAI;
				if (buildingAI != null && buildingAI.IsWonder())
				{
					list.Add("Monument");
				}
				string text = SteamHelper.ServiceToTag(info.GetService(), aI);
				if (text != null)
				{
					list.Add(text);
				}
				string text2 = SteamHelper.SubServiceToTag(info.GetSubService());
				if (text2 != null)
				{
					list.Add(text2);
				}
			}
		}
		else if (info is PropInfo)
		{
			list.Add("Prop");
		}
		else if (info is TreeInfo)
		{
			list.Add("Tree");
		}
		else if (info is VehicleInfo)
		{
			list.Add("Vehicle");
		}
		else if (info is CitizenInfo)
		{
			list.Add("Citizen");
		}
		else if (info is NetInfo)
		{
			list.Add("Road");
			PrefabAI aI2 = info.GetAI();
			string text3 = SteamHelper.ServiceToTag(info.GetService(), aI2);
			if (text3 != null)
			{
				list.Add(text3);
			}
			string text4 = SteamHelper.SubServiceToTag(info.GetSubService());
			if (text4 != null)
			{
				list.Add(text4);
			}
		}
		return list.ToArray();
	}

	public const uint kDesiredAppID = 255710u;

	public const uint kPreorderAppID = 340160u;

	public const uint kDeluxeAppID = 346791u;

	public const uint kMagicka2AppID = 238370u;

	public const uint kAfterDLCAppID = 369150u;

	public const uint kWinterDLCAppID = 420610u;

	public const uint kNaturalDisastersDLCAppID = 515191u;

	public const uint kMotionDLCAppID = 547502u;

	public const uint kGreenDLCAppID = 614580u;

	public const uint kFootballAppID = 456200u;

	public const uint kFootball2345AppID = 536610u;

	public const uint kFootball2AppID = 525940u;

	public const uint kFootball3AppID = 526610u;

	public const uint kFootball4AppID = 526611u;

	public const uint kFootball5AppID = 526612u;

	public const uint kStation1AppID = 547501u;

	public const uint kStation2AppID = 614582u;

	public const uint kFestivalAppID = 614581u;

	public const uint kModderPack1AppID = 515190u;

	public const uint kModderPack2AppID = 547500u;

	public const uint kModderPack3AppID = 715190u;

	public const uint kOrientalBuildingsAppID = 563850u;

	public const string kSteamTagMap = "Map";

	public const string kSteamTagMod = "Mod";

	public const string kSteamTagSaveGame = "SaveGame";

	public const string kSteamTagStyle = "District Style";

	public const string kSteamTagBuilding = "Building";

	public const string kSteamTagProp = "Prop";

	public const string kSteamTagTree = "Tree";

	public const string kSteamTagIntersection = "Intersection";

	public const string kSteamTagPark = "Park";

	public const string kSteamTagElectricity = "Electricity";

	public const string kSteamTagWaterAndSewage = "Water & Sewage";

	public const string kSteamTagGarbage = "Garbage";

	public const string kSteamTagHealthcare = "Healthcare";

	public const string kSteamTagDeathcare = "Deathcare";

	public const string kSteamTagFireDepartment = "Fire Department";

	public const string kSteamTagPoliceDepartment = "Police Department";

	public const string kSteamTagEducation = "Education";

	public const string kSteamTagTransport = "Transport";

	public const string kSteamTagTransportBus = "Transport Bus";

	public const string kSteamTagTransportMetro = "Transport Metro";

	public const string kSteamTagTransportTrain = "Transport Train";

	public const string kSteamTagTransportShip = "Transport Ship";

	public const string kSteamTagTransportPlane = "Transport Plane";

	public const string kSteamTagTransportTaxi = "Transport Taxi";

	public const string kSteamTagTransportTram = "Transport Tram";

	public const string kSteamTagTransportMonorail = "Transport Monorail";

	public const string kSteamTagTransportCableCar = "Transport Cable Car";

	public const string kSteamTagUniqueBuilding = "Unique Building";

	public const string kSteamTagMonument = "Monument";

	public const string kSteamTagResidential = "Residential";

	public const string kSteamTagResidentialLow = "Residential Low";

	public const string kSteamTagResidentialHigh = "Residential High";

	public const string kSteamTagCommercial = "Commercial";

	public const string kSteamTagCommercialHigh = "Commercial High";

	public const string kSteamTagCommercialLow = "Commercial Low";

	public const string kSteamTagIndustrial = "Industrial";

	public const string kSteamTagIndustrialGeneric = "Industrial Generic";

	public const string kSteamTagIndustrialFarming = "Industrial Farming";

	public const string kSteamTagIndustrialOre = "Industrial Ore";

	public const string kSteamTagIndustrialOil = "Industrial Oil";

	public const string kSteamTagIndustrialForestry = "Industrial Forestry";

	public const string kSteamTagOffice = "Office";

	public const string kSteamTagColorCorrectionLUT = "Color Correction LUT";

	public const string kSteamTagVehicle = "Vehicle";

	public const string kSteamTagCitizen = "Citizen";

	public const string kSteamTagRoad = "Road";

	public const string kSteamTagCommercialTourist = "Commercial Tourism";

	public const string kSteamTagCommercialLeisure = "Commercial Leisure";

	public const string kSteamTagMapTheme = "Map Theme";

	public const string kSteamTagScenario = "Scenario";

	public enum DLC
	{
		None,
		DeluxeDLC = 346791,
		AfterDarkDLC = 369150,
		SnowFallDLC = 420610,
		NaturalDisastersDLC = 515191,
		InMotionDLC = 547502,
		GreenCitiesDLC = 614580,
		Football = 456200,
		Football2345 = 536610,
		Football2 = 525940,
		Football3 = 526610,
		Football4,
		Football5,
		RadioStation1 = 547501,
		RadioStation2 = 614582,
		MusicFestival = 614581,
		ModderPack1 = 515190,
		ModderPack2 = 547500,
		ModderPack3 = 715190,
		OrientalBuildings = 563850,
		NotAllowed = 100
	}

	[Flags]
	public enum DLC_BitMask
	{
		DeluxeDLC = 1,
		AfterDarkDLC = 2,
		SnowFallDLC = 4,
		NaturalDisastersDLC = 8,
		InMotionDLC = 16,
		GreenCitiesDLC = 32,
		Football = 65536,
		Football2345 = 131072,
		RadioStation1 = 262144,
		OrientalBuildings = 524288,
		RadioStation2 = 1048576,
		MusicFestival = 2097152,
		ModderPack1 = 16777216,
		ModderPack2 = 33554432,
		ModderPack3 = 67108864
	}
}
