﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;
using UnityEngine;

public class PrefabInfo : MonoBehaviour
{
	public string category
	{
		get
		{
			string category = (!string.IsNullOrEmpty(this.m_UICategory)) ? this.m_UICategory : PrefabInfo.kDefaultCategory;
			if (Singleton<ToolManager>.get_exists())
			{
				bool flag = Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor);
				bool flag2 = Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.MapEditor);
				if (flag)
				{
					return this.editorCategory;
				}
				if (flag2)
				{
					return this.mapEditorCategory;
				}
			}
			return GeneratedGroupPanel.ResolveLegacyCategoryName(this.GetService(), category);
		}
	}

	public string editorCategory
	{
		get
		{
			if (string.IsNullOrEmpty(this.m_UIEditorCategory) || string.Compare(this.m_UIEditorCategory, PrefabInfo.kSameAsGameCategory) == 0)
			{
				return GeneratedGroupPanel.ResolveLegacyCategoryName(this.GetService(), (!string.IsNullOrEmpty(this.m_UICategory)) ? this.m_UICategory : PrefabInfo.kDefaultCategory);
			}
			return GeneratedGroupPanel.ResolveLegacyCategoryName(this.GetService(), this.m_UIEditorCategory);
		}
	}

	public string mapEditorCategory
	{
		get
		{
			if (string.IsNullOrEmpty(this.m_UIMapEditorCategory) || string.Compare(this.m_UIMapEditorCategory, PrefabInfo.kSameAsGameCategory) == 0)
			{
				return GeneratedGroupPanel.ResolveLegacyCategoryName(this.GetService(), (!string.IsNullOrEmpty(this.m_UICategory)) ? this.m_UICategory : PrefabInfo.kDefaultCategory);
			}
			return GeneratedGroupPanel.ResolveLegacyCategoryName(this.GetService(), this.m_UIMapEditorCategory);
		}
	}

	public virtual void InitializePrefab()
	{
		this.m_prefabDataLayer = base.get_gameObject().get_layer();
	}

	public virtual void TempInitializePrefab()
	{
	}

	public virtual void DestroyPrefab()
	{
		this.m_prefabDataLayer = -1;
		this.ReleaseInstancePool();
	}

	public virtual void InitializePrefabInstance(PrefabInfo prefabInfo)
	{
	}

	public virtual void DestroyPrefabInstance()
	{
	}

	public void UpdatePrefabInstances()
	{
		if (this.m_instancePool != null)
		{
			int num = this.m_instancePool.m_targetCount;
			int count = this.m_instancePool.m_buffer.Count;
			int size = this.m_instancePool.m_freeInstances.m_size;
			num = Mathf.Max((int)((long)num * 16383L >> 14) + count, count << 14);
			this.m_instancePool.m_targetCount = num;
			int num2 = num + 16383 >> 14;
			if (num2 < count + size && size != 0)
			{
				this.m_instancePool.TryDestroyFreeInstance();
			}
		}
	}

	protected virtual void LateUpdate()
	{
		if (this.m_instanceNeeded)
		{
			this.m_instanceNeeded = false;
		}
		else
		{
			this.ReleasePrefabInstance();
		}
	}

	public T ObtainPrefabInstance<T>(InstanceID id, int maxCount) where T : PrefabInfo
	{
		if (this.m_instancePool == null)
		{
			this.CreateInstancePool();
		}
		return this.m_instancePool.ObtainPrefabInstance<T>(id, maxCount);
	}

	public T GetPrefabInstance<T>(InstanceID id) where T : PrefabInfo
	{
		if (this.m_instancePool == null)
		{
			return (T)((object)null);
		}
		return this.m_instancePool.GetPrefabInstance<T>(id);
	}

	protected void ReleasePrefabInstance()
	{
		if (this.m_instancePool != null)
		{
			this.m_instancePool.ReleasePrefabInstance(this);
		}
	}

	private void CreateInstancePool()
	{
		if (this.m_instancePool == null)
		{
			Transform instancePoolRoot = PrefabInfo.GetInstancePoolRoot();
			GameObject gameObject = new GameObject(base.get_gameObject().get_name());
			gameObject.get_transform().set_parent(instancePoolRoot);
			this.m_instancePool = gameObject.AddComponent<PrefabPool>();
			this.m_instancePool.Initialize(this);
		}
	}

	private void ReleaseInstancePool()
	{
		if (this.m_instancePool != null)
		{
			this.m_instancePool.Release();
			Object.Destroy(this.m_instancePool.get_gameObject());
			this.m_instancePool = null;
		}
	}

	private static Transform GetInstancePoolRoot()
	{
		if (PrefabInfo.m_instancePoolRoot == null)
		{
			PrefabInfo.m_instancePoolRoot = new GameObject("Instance Pool");
			Object.DontDestroyOnLoad(PrefabInfo.m_instancePoolRoot);
		}
		return PrefabInfo.m_instancePoolRoot.get_transform();
	}

	public virtual void ReadMesh(out Vector3[] vertices, out int[] triangles)
	{
		vertices = null;
		triangles = null;
	}

	public virtual void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
	}

	public virtual string GetLocalizedTitle()
	{
		return base.get_gameObject().get_name();
	}

	public virtual GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.String(base.get_gameObject().get_name());
	}

	public virtual string GetUncheckedLocalizedTitle()
	{
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public virtual string GetLocalizedDescription()
	{
		return base.get_gameObject().get_name();
	}

	public virtual string GetLocalizedDescriptionShort()
	{
		return base.get_gameObject().get_name();
	}

	public virtual string GetLocalizedTooltip()
	{
		string text = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Title,
			this.GetLocalizedTitle(),
			LocaleFormatter.Sprite,
			(!string.IsNullOrEmpty(this.m_InfoTooltipThumbnail)) ? this.m_InfoTooltipThumbnail : base.get_gameObject().get_name(),
			LocaleFormatter.Text,
			this.GetLocalizedDescription(),
			LocaleFormatter.Locked,
			(!ToolsModifierControl.IsUnlocked(this.GetUnlockMilestone())).ToString()
		});
		MilestoneInfo unlockMilestone = this.GetUnlockMilestone();
		string text2;
		string text3;
		string text4;
		string text5;
		string text6;
		ToolsModifierControl.GetUnlockingInfo(unlockMilestone, out text2, out text3, out text4, out text5, out text6);
		string text7 = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.LockedInfo,
			text6,
			LocaleFormatter.UnlockDesc,
			text2,
			LocaleFormatter.UnlockPopulationProgressText,
			text5,
			LocaleFormatter.UnlockPopulationTarget,
			text4,
			LocaleFormatter.UnlockPopulationCurrent,
			text3
		});
		text = TooltipHelper.Append(text, text7);
		PrefabAI aI = this.GetAI();
		if (aI != null)
		{
			text = TooltipHelper.Append(text, aI.GetLocalizedTooltip());
		}
		return text;
	}

	public virtual void CheckReferences()
	{
	}

	public virtual void RefreshLevelOfDetail()
	{
	}

	public virtual void LoadDecorations()
	{
	}

	public virtual void SaveDecorations()
	{
	}

	public virtual void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
	}

	public virtual void EnsureCellSurface()
	{
	}

	public virtual void SetCellSurface(int index, TerrainModify.Surface surface)
	{
	}

	public virtual int GetWidth()
	{
		return 0;
	}

	public virtual int GetLength()
	{
		return 0;
	}

	public virtual void SetWidth(int width)
	{
	}

	public virtual void SetLength(int length)
	{
	}

	public virtual void GetWidthRange(out int minWidth, out int maxWidth)
	{
		minWidth = 0;
		maxWidth = 0;
	}

	public virtual void GetLengthRange(out int minLength, out int maxLength)
	{
		minLength = 0;
		maxLength = 0;
	}

	public virtual void GetDecorationArea(out int width, out int length, out float offset)
	{
		width = 0;
		length = 0;
		offset = 0f;
	}

	public virtual void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		negX = false;
		posX = false;
		negZ = false;
		posZ = false;
	}

	public virtual PrefabAI GetAI()
	{
		return null;
	}

	public virtual ItemClass.Service GetService()
	{
		return ItemClass.Service.None;
	}

	public virtual ItemClass.SubService GetSubService()
	{
		return ItemClass.SubService.None;
	}

	public virtual ItemClass.Level GetClassLevel()
	{
		return ItemClass.Level.None;
	}

	public virtual MilestoneInfo GetUnlockMilestone()
	{
		return null;
	}

	public virtual bool CanBeBuilt()
	{
		return true;
	}

	public virtual int GetConstructionCost()
	{
		return 0;
	}

	public virtual int GetMaintenanceCost()
	{
		return 0;
	}

	[NonSerialized]
	public bool m_isCustomContent;

	[NonSerialized]
	public int m_prefabDataIndex = -1;

	[NonSerialized]
	public int m_prefabDataLayer = -1;

	[NonSerialized]
	public bool m_prefabInitialized;

	[NonSerialized]
	public bool m_instanceNeeded;

	[NonSerialized]
	public bool m_instanceChanged;

	[NonSerialized]
	public PrefabPool m_instancePool;

	[NonSerialized]
	public InstanceID m_instanceID = InstanceID.Empty;

	[BitMask]
	public SteamHelper.DLC_BitMask m_dlcRequired;

	public UITextureAtlas m_Atlas;

	[UISprite("m_Atlas")]
	public string m_Thumbnail;

	public UITextureAtlas m_InfoTooltipAtlas;

	[UISprite("m_InfoTooltipAtlas")]
	public string m_InfoTooltipThumbnail;

	public int m_UIPriority;

	[UICategory, SerializeField]
	protected string m_UICategory;

	[UIEditorCategory, SerializeField]
	protected string m_UIEditorCategory = PrefabInfo.kSameAsGameCategory;

	[UIMapEditorCategory, SerializeField]
	protected string m_UIMapEditorCategory = PrefabInfo.kSameAsGameCategory;

	public static readonly string kSameAsGameCategory = "SameAsGame";

	public static readonly string kDefaultCategory = "Default";

	private static GameObject m_instancePoolRoot;
}
