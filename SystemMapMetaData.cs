﻿using System;
using ColossalFramework.Packaging;

[Serializable]
public class SystemMapMetaData : MetaData
{
	public override string getEnvironment
	{
		get
		{
			return this.environment;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public string environment;

	public DateTime timeStamp;

	public Package.Asset imageRef;
}
