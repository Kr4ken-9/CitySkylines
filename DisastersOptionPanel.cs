﻿using System;
using ColossalFramework.UI;

public class DisastersOptionPanel : OptionPanelBase
{
	private void Awake()
	{
		this.m_slider = base.Find<UISlider>("Slider");
		this.m_slider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		this.m_label = base.Find<UILabel>("LabelIntensity");
		this.m_disasterTool = ToolsModifierControl.GetTool<DisasterTool>();
		this.m_slider.set_value((float)this.m_disasterTool.m_intensity);
		this.m_label.set_text(((float)this.m_disasterTool.m_intensity / 10f).ToString("F1"));
		this.Hide();
	}

	private void OnSliderValueChanged(UIComponent comp, float value)
	{
		this.m_label.set_text((value / 10f).ToString("F1"));
		if (this.m_disasterTool != null)
		{
			this.m_disasterTool.m_intensity = (int)value;
		}
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	private UISlider m_slider;

	private UILabel m_label;

	private DisasterTool m_disasterTool;
}
