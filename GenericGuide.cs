﻿using System;

public class GenericGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo)
	{
		if (base.CanActivate(guideInfo))
		{
			GuideTriggerBase.ActivationInfo activationInfo = new GenericGuide.GenericActivationInfo(guideInfo);
			base.Activate(guideInfo, activationInfo);
		}
	}

	public class GenericActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public GenericActivationInfo(GuideInfo guideInfo) : base(guideInfo)
		{
		}

		public override string GetName()
		{
			return this.m_guideInfo.m_name;
		}

		public override string GetIcon()
		{
			return this.m_guideInfo.m_icon;
		}

		public override string FormatText(string pattern)
		{
			return pattern;
		}

		public override IUITag GetTag()
		{
			if (string.IsNullOrEmpty(this.m_guideInfo.m_tag))
			{
				return new GenericTutorialTag();
			}
			return MonoTutorialTag.Find(this.m_guideInfo.m_tag);
		}
	}
}
