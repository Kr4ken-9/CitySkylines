﻿using System;
using ColossalFramework;
using UnityEngine;

public struct RadioContentData
{
	public RadioContentInfo Info
	{
		get
		{
			return PrefabCollection<RadioContentInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public void SimulationTick(ushort contentIndex)
	{
		if ((this.m_flags & (RadioContentData.Flags.Playing | RadioContentData.Flags.Queued)) == RadioContentData.Flags.None && (this.m_cooldown += 1) >= 500)
		{
			Singleton<AudioManager>.get_instance().ReleaseRadioContent(contentIndex);
		}
	}

	public RadioContentData.Flags m_flags;

	public ushort m_infoIndex;

	public ushort m_cooldown;

	public ushort m_channel;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Playing = 4,
		Queued = 8,
		All = -1
	}
}
