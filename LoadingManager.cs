﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Packaging;
using ColossalFramework.Plugins;
using ICities;
using UnityEngine;

public class LoadingManager : Singleton<LoadingManager>
{
	public LoadingManager.AutoSaveTimer autoSaveTimer
	{
		get
		{
			return this.m_AutoSaveTimer;
		}
	}

	public event LoadingManager.IntroLoadedHandler m_introLoaded
	{
		add
		{
			LoadingManager.IntroLoadedHandler introLoadedHandler = this.m_introLoaded;
			LoadingManager.IntroLoadedHandler introLoadedHandler2;
			do
			{
				introLoadedHandler2 = introLoadedHandler;
				introLoadedHandler = Interlocked.CompareExchange<LoadingManager.IntroLoadedHandler>(ref this.m_introLoaded, (LoadingManager.IntroLoadedHandler)Delegate.Combine(introLoadedHandler2, value), introLoadedHandler);
			}
			while (introLoadedHandler != introLoadedHandler2);
		}
		remove
		{
			LoadingManager.IntroLoadedHandler introLoadedHandler = this.m_introLoaded;
			LoadingManager.IntroLoadedHandler introLoadedHandler2;
			do
			{
				introLoadedHandler2 = introLoadedHandler;
				introLoadedHandler = Interlocked.CompareExchange<LoadingManager.IntroLoadedHandler>(ref this.m_introLoaded, (LoadingManager.IntroLoadedHandler)Delegate.Remove(introLoadedHandler2, value), introLoadedHandler);
			}
			while (introLoadedHandler != introLoadedHandler2);
		}
	}

	public event LoadingManager.LevelLoadedHandler m_levelLoaded
	{
		add
		{
			LoadingManager.LevelLoadedHandler levelLoadedHandler = this.m_levelLoaded;
			LoadingManager.LevelLoadedHandler levelLoadedHandler2;
			do
			{
				levelLoadedHandler2 = levelLoadedHandler;
				levelLoadedHandler = Interlocked.CompareExchange<LoadingManager.LevelLoadedHandler>(ref this.m_levelLoaded, (LoadingManager.LevelLoadedHandler)Delegate.Combine(levelLoadedHandler2, value), levelLoadedHandler);
			}
			while (levelLoadedHandler != levelLoadedHandler2);
		}
		remove
		{
			LoadingManager.LevelLoadedHandler levelLoadedHandler = this.m_levelLoaded;
			LoadingManager.LevelLoadedHandler levelLoadedHandler2;
			do
			{
				levelLoadedHandler2 = levelLoadedHandler;
				levelLoadedHandler = Interlocked.CompareExchange<LoadingManager.LevelLoadedHandler>(ref this.m_levelLoaded, (LoadingManager.LevelLoadedHandler)Delegate.Remove(levelLoadedHandler2, value), levelLoadedHandler);
			}
			while (levelLoadedHandler != levelLoadedHandler2);
		}
	}

	public event LoadingManager.LevelUnloadedHandler m_levelUnloaded
	{
		add
		{
			LoadingManager.LevelUnloadedHandler levelUnloadedHandler = this.m_levelUnloaded;
			LoadingManager.LevelUnloadedHandler levelUnloadedHandler2;
			do
			{
				levelUnloadedHandler2 = levelUnloadedHandler;
				levelUnloadedHandler = Interlocked.CompareExchange<LoadingManager.LevelUnloadedHandler>(ref this.m_levelUnloaded, (LoadingManager.LevelUnloadedHandler)Delegate.Combine(levelUnloadedHandler2, value), levelUnloadedHandler);
			}
			while (levelUnloadedHandler != levelUnloadedHandler2);
		}
		remove
		{
			LoadingManager.LevelUnloadedHandler levelUnloadedHandler = this.m_levelUnloaded;
			LoadingManager.LevelUnloadedHandler levelUnloadedHandler2;
			do
			{
				levelUnloadedHandler2 = levelUnloadedHandler;
				levelUnloadedHandler = Interlocked.CompareExchange<LoadingManager.LevelUnloadedHandler>(ref this.m_levelUnloaded, (LoadingManager.LevelUnloadedHandler)Delegate.Remove(levelUnloadedHandler2, value), levelUnloadedHandler);
			}
			while (levelUnloadedHandler != levelUnloadedHandler2);
		}
	}

	public event LoadingManager.LevelPreLoadedHandler m_levelPreLoaded
	{
		add
		{
			LoadingManager.LevelPreLoadedHandler levelPreLoadedHandler = this.m_levelPreLoaded;
			LoadingManager.LevelPreLoadedHandler levelPreLoadedHandler2;
			do
			{
				levelPreLoadedHandler2 = levelPreLoadedHandler;
				levelPreLoadedHandler = Interlocked.CompareExchange<LoadingManager.LevelPreLoadedHandler>(ref this.m_levelPreLoaded, (LoadingManager.LevelPreLoadedHandler)Delegate.Combine(levelPreLoadedHandler2, value), levelPreLoadedHandler);
			}
			while (levelPreLoadedHandler != levelPreLoadedHandler2);
		}
		remove
		{
			LoadingManager.LevelPreLoadedHandler levelPreLoadedHandler = this.m_levelPreLoaded;
			LoadingManager.LevelPreLoadedHandler levelPreLoadedHandler2;
			do
			{
				levelPreLoadedHandler2 = levelPreLoadedHandler;
				levelPreLoadedHandler = Interlocked.CompareExchange<LoadingManager.LevelPreLoadedHandler>(ref this.m_levelPreLoaded, (LoadingManager.LevelPreLoadedHandler)Delegate.Remove(levelPreLoadedHandler2, value), levelPreLoadedHandler);
			}
			while (levelPreLoadedHandler != levelPreLoadedHandler2);
		}
	}

	public event LoadingManager.LevelPreUnloadedHandler m_levelPreUnloaded
	{
		add
		{
			LoadingManager.LevelPreUnloadedHandler levelPreUnloadedHandler = this.m_levelPreUnloaded;
			LoadingManager.LevelPreUnloadedHandler levelPreUnloadedHandler2;
			do
			{
				levelPreUnloadedHandler2 = levelPreUnloadedHandler;
				levelPreUnloadedHandler = Interlocked.CompareExchange<LoadingManager.LevelPreUnloadedHandler>(ref this.m_levelPreUnloaded, (LoadingManager.LevelPreUnloadedHandler)Delegate.Combine(levelPreUnloadedHandler2, value), levelPreUnloadedHandler);
			}
			while (levelPreUnloadedHandler != levelPreUnloadedHandler2);
		}
		remove
		{
			LoadingManager.LevelPreUnloadedHandler levelPreUnloadedHandler = this.m_levelPreUnloaded;
			LoadingManager.LevelPreUnloadedHandler levelPreUnloadedHandler2;
			do
			{
				levelPreUnloadedHandler2 = levelPreUnloadedHandler;
				levelPreUnloadedHandler = Interlocked.CompareExchange<LoadingManager.LevelPreUnloadedHandler>(ref this.m_levelPreUnloaded, (LoadingManager.LevelPreUnloadedHandler)Delegate.Remove(levelPreUnloadedHandler2, value), levelPreUnloadedHandler);
			}
			while (levelPreUnloadedHandler != levelPreUnloadedHandler2);
		}
	}

	public event LoadingManager.MetaDataReadyHandler m_metaDataReady
	{
		add
		{
			LoadingManager.MetaDataReadyHandler metaDataReadyHandler = this.m_metaDataReady;
			LoadingManager.MetaDataReadyHandler metaDataReadyHandler2;
			do
			{
				metaDataReadyHandler2 = metaDataReadyHandler;
				metaDataReadyHandler = Interlocked.CompareExchange<LoadingManager.MetaDataReadyHandler>(ref this.m_metaDataReady, (LoadingManager.MetaDataReadyHandler)Delegate.Combine(metaDataReadyHandler2, value), metaDataReadyHandler);
			}
			while (metaDataReadyHandler != metaDataReadyHandler2);
		}
		remove
		{
			LoadingManager.MetaDataReadyHandler metaDataReadyHandler = this.m_metaDataReady;
			LoadingManager.MetaDataReadyHandler metaDataReadyHandler2;
			do
			{
				metaDataReadyHandler2 = metaDataReadyHandler;
				metaDataReadyHandler = Interlocked.CompareExchange<LoadingManager.MetaDataReadyHandler>(ref this.m_metaDataReady, (LoadingManager.MetaDataReadyHandler)Delegate.Remove(metaDataReadyHandler2, value), metaDataReadyHandler);
			}
			while (metaDataReadyHandler != metaDataReadyHandler2);
		}
	}

	public event LoadingManager.SimulationDataReadyHandler m_simulationDataReady
	{
		add
		{
			LoadingManager.SimulationDataReadyHandler simulationDataReadyHandler = this.m_simulationDataReady;
			LoadingManager.SimulationDataReadyHandler simulationDataReadyHandler2;
			do
			{
				simulationDataReadyHandler2 = simulationDataReadyHandler;
				simulationDataReadyHandler = Interlocked.CompareExchange<LoadingManager.SimulationDataReadyHandler>(ref this.m_simulationDataReady, (LoadingManager.SimulationDataReadyHandler)Delegate.Combine(simulationDataReadyHandler2, value), simulationDataReadyHandler);
			}
			while (simulationDataReadyHandler != simulationDataReadyHandler2);
		}
		remove
		{
			LoadingManager.SimulationDataReadyHandler simulationDataReadyHandler = this.m_simulationDataReady;
			LoadingManager.SimulationDataReadyHandler simulationDataReadyHandler2;
			do
			{
				simulationDataReadyHandler2 = simulationDataReadyHandler;
				simulationDataReadyHandler = Interlocked.CompareExchange<LoadingManager.SimulationDataReadyHandler>(ref this.m_simulationDataReady, (LoadingManager.SimulationDataReadyHandler)Delegate.Remove(simulationDataReadyHandler2, value), simulationDataReadyHandler);
			}
			while (simulationDataReadyHandler != simulationDataReadyHandler2);
		}
	}

	public LoadingAnimation LoadingAnimationComponent
	{
		get
		{
			return this.m_loadingAnimation;
		}
	}

	private void Awake()
	{
		this.m_loadingLock = new object();
		this.m_loadingProfilerMain = new LoadingProfiler();
		this.m_loadingProfilerSimulation = new LoadingProfiler();
		this.m_loadingProfilerScenes = new LoadingProfiler();
		this.m_loadingProfilerCustomContent = new LoadingProfiler();
		this.m_loadingProfilerCustomAsset = new LoadingProfiler();
		this.m_mainThreadQueue = new Queue<IEnumerator>();
		this.m_stopWatch = new Stopwatch();
		Camera camera = base.get_gameObject().AddComponent<Camera>();
		camera.set_eventMask(0);
		camera.set_backgroundColor(new Color(0f, 0f, 0f, 1f));
		camera.set_clearFlags(2);
		camera.set_orthographic(true);
		camera.set_orthographicSize(1f);
		camera.set_nearClipPlane(1f);
		camera.set_farClipPlane(100f);
		camera.set_cullingMask(0);
		camera.set_renderingPath(0);
		camera.set_useOcclusionCulling(false);
		camera.set_depth(1000000f);
		this.m_loadingAnimation = base.get_gameObject().AddComponent<LoadingAnimation>();
		this.m_loadingAnimation.set_enabled(false);
		this.m_supportsExpansion = new bool[5];
		this.m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		this.m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
		while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_terminated = true;
			Monitor.PulseAll(this.m_loadingLock);
		}
		finally
		{
			Monitor.Exit(this.m_loadingLock);
		}
	}

	private void CreateRelay()
	{
		if (this.m_LoadingWrapper == null)
		{
			this.m_LoadingWrapper = new LoadingWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_LoadingWrapper != null)
		{
			this.m_LoadingWrapper.Release();
			this.m_LoadingWrapper = null;
		}
	}

	private void OnApplicationQuit()
	{
		this.m_loadingAnimation.set_enabled(true);
		if (this.m_currentlyLoading || this.m_loadingComplete)
		{
			this.QuitApplication();
			Application.CancelQuit();
		}
	}

	private void Update()
	{
		if (this.m_hasQueuedActions)
		{
			this.m_stopWatch.Reset();
			this.m_stopWatch.Start();
			do
			{
				while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				IEnumerator enumerator;
				try
				{
					enumerator = this.m_mainThreadQueue.Peek();
				}
				finally
				{
					Monitor.Exit(this.m_loadingLock);
				}
				bool flag;
				for (flag = enumerator.MoveNext(); flag && this.m_stopWatch.ElapsedMilliseconds < 10L; flag = enumerator.MoveNext())
				{
				}
				if (!flag)
				{
					while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
					{
					}
					try
					{
						this.m_mainThreadQueue.Dequeue();
						this.m_hasQueuedActions = (this.m_mainThreadQueue.Count != 0);
					}
					finally
					{
						Monitor.Exit(this.m_loadingLock);
					}
				}
			}
			while (this.m_hasQueuedActions && this.m_stopWatch.ElapsedMilliseconds < 10L);
			this.m_stopWatch.Stop();
		}
	}

	public void SetSceneProgress(float progress)
	{
		while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_sceneProgress = progress;
			this.m_loadingAnimation.LoadingProgress = Mathf.Min(this.m_sceneProgress, this.m_simulationProgress);
		}
		finally
		{
			Monitor.Exit(this.m_loadingLock);
		}
	}

	public Coroutine LoadIntro()
	{
		if (!this.m_currentlyLoading && !this.m_applicationQuitting)
		{
			if (this.m_LoadingWrapper != null)
			{
				this.m_LoadingWrapper.OnLevelUnloading();
			}
			this.m_loadingAnimation.set_enabled(true);
			this.m_currentlyLoading = true;
			this.m_metaDataLoaded = false;
			this.m_simulationDataLoaded = false;
			this.m_loadingComplete = false;
			this.m_brokenAssets = string.Empty;
			this.m_loadingProfilerMain.Reset();
			this.m_loadingProfilerSimulation.Reset();
			this.m_loadingProfilerScenes.Reset();
			return base.StartCoroutine(this.LoadIntroCoroutine());
		}
		return null;
	}

	public Coroutine LoadLevel(Package.Asset asset, string playerScene, string uiScene)
	{
		return this.LoadLevel(asset, playerScene, uiScene, null);
	}

	public Coroutine LoadLevel(string filename, string playerScene, string uiScene)
	{
		return this.LoadLevel(filename, playerScene, uiScene, null);
	}

	public Coroutine LoadLevel(string filename, string playerScene, string uiScene, SimulationMetaData ngs)
	{
		Package.Asset asset = string.IsNullOrEmpty(filename) ? null : new Package.Asset(string.Empty, filename, Package.AssetType.Data, false);
		return this.LoadLevel(asset, playerScene, uiScene, ngs);
	}

	public Coroutine LoadLevel(Package.Asset asset, string playerScene, string uiScene, SimulationMetaData ngs)
	{
		if (!this.m_currentlyLoading && !this.m_applicationQuitting)
		{
			if (this.m_LoadingWrapper != null)
			{
				this.m_LoadingWrapper.OnLevelUnloading();
			}
			this.m_loadingAnimation.set_enabled(true);
			this.m_currentlyLoading = true;
			this.m_metaDataLoaded = false;
			this.m_simulationDataLoaded = false;
			this.m_loadingComplete = false;
			this.m_renderDataReady = false;
			this.m_essentialScenesLoaded = false;
			this.m_brokenAssets = string.Empty;
			this.m_sceneProgress = 0f;
			this.m_simulationProgress = 0f;
			this.m_loadingProfilerMain.Reset();
			this.m_loadingProfilerSimulation.Reset();
			this.m_loadingProfilerScenes.Reset();
			return base.StartCoroutine(this.LoadLevelCoroutine(asset, playerScene, uiScene, ngs));
		}
		return null;
	}

	public Coroutine SaveLevel(string filename)
	{
		if (this.m_simulationDataLoaded && !this.m_applicationQuitting)
		{
			this.m_loadingProfilerMain.Reset();
			this.m_loadingProfilerSimulation.Reset();
			this.m_loadingProfilerScenes.Reset();
			return base.StartCoroutine(this.SaveLevelCoroutine(filename));
		}
		return null;
	}

	public Coroutine UnloadLevel()
	{
		if (!this.m_currentlyLoading && !this.m_applicationQuitting)
		{
			if (this.m_LoadingWrapper != null)
			{
				this.m_LoadingWrapper.OnLevelUnloading();
			}
			this.m_loadingAnimation.set_enabled(true);
			this.m_currentlyLoading = true;
			this.m_metaDataLoaded = false;
			this.m_simulationDataLoaded = false;
			this.m_loadingComplete = false;
			this.m_brokenAssets = string.Empty;
			this.autoSaveTimer.Stop();
			this.m_loadingProfilerMain.Reset();
			this.m_loadingProfilerSimulation.Reset();
			this.m_loadingProfilerScenes.Reset();
			return base.StartCoroutine(this.UnloadLevelCoroutine());
		}
		return null;
	}

	public Coroutine QuitApplication()
	{
		if (!this.m_applicationQuitting)
		{
			if (this.m_LoadingWrapper != null)
			{
				this.m_LoadingWrapper.OnLevelUnloading();
			}
			this.m_loadingAnimation.set_enabled(true);
			this.m_applicationQuitting = true;
			return base.StartCoroutine(this.QuitApplicationCoroutine());
		}
		return null;
	}

	[DebuggerHidden]
	private IEnumerator LoadIntroCoroutine()
	{
		LoadingManager.<LoadIntroCoroutine>c__Iterator0 <LoadIntroCoroutine>c__Iterator = new LoadingManager.<LoadIntroCoroutine>c__Iterator0();
		<LoadIntroCoroutine>c__Iterator.$this = this;
		return <LoadIntroCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoadLevelCoroutine(Package.Asset asset, string playerScene, string uiScene, SimulationMetaData ngs)
	{
		LoadingManager.<LoadLevelCoroutine>c__Iterator1 <LoadLevelCoroutine>c__Iterator = new LoadingManager.<LoadLevelCoroutine>c__Iterator1();
		<LoadLevelCoroutine>c__Iterator.asset = asset;
		<LoadLevelCoroutine>c__Iterator.ngs = ngs;
		<LoadLevelCoroutine>c__Iterator.playerScene = playerScene;
		<LoadLevelCoroutine>c__Iterator.uiScene = uiScene;
		<LoadLevelCoroutine>c__Iterator.$this = this;
		return <LoadLevelCoroutine>c__Iterator;
	}

	private string GetLoadingScene()
	{
		string text = Singleton<SimulationManager>.get_instance().m_metaData.m_environment + "Loading";
		if (this.DLC(369150u))
		{
			int num = this.m_LoadingImageIndex.get_value() + 1;
			if (num >= 2)
			{
				num = 0;
			}
			this.m_LoadingImageIndex.set_value(num);
			if (num != 0)
			{
				text += (num + 1).ToString();
			}
		}
		return text;
	}

	private bool DLC(uint id)
	{
		if (id == 1u)
		{
			SavedBool savedBool = new SavedBool(Settings.pdxLoginUsed, Settings.userGameState, false);
			return savedBool.get_value();
		}
		return SteamHelper.IsDLCOwned((SteamHelper.DLC)id);
	}

	private bool APP(uint id)
	{
		return SteamHelper.IsAppOwned(id);
	}

	[DebuggerHidden]
	private IEnumerator SaveLevelCoroutine(string filename)
	{
		LoadingManager.<SaveLevelCoroutine>c__Iterator2 <SaveLevelCoroutine>c__Iterator = new LoadingManager.<SaveLevelCoroutine>c__Iterator2();
		<SaveLevelCoroutine>c__Iterator.filename = filename;
		<SaveLevelCoroutine>c__Iterator.$this = this;
		return <SaveLevelCoroutine>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator UnloadLevelCoroutine()
	{
		LoadingManager.<UnloadLevelCoroutine>c__Iterator3 <UnloadLevelCoroutine>c__Iterator = new LoadingManager.<UnloadLevelCoroutine>c__Iterator3();
		<UnloadLevelCoroutine>c__Iterator.$this = this;
		return <UnloadLevelCoroutine>c__Iterator;
	}

	private void DestroyAllPrefabs()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("DestroyPrefabs");
		PrefabCollection<NetInfo>.DestroyAll();
		PrefabCollection<BuildingInfo>.DestroyAll();
		PrefabCollection<PropInfo>.DestroyAll();
		PrefabCollection<TreeInfo>.DestroyAll();
		PrefabCollection<TransportInfo>.DestroyAll();
		PrefabCollection<VehicleInfo>.DestroyAll();
		PrefabCollection<CitizenInfo>.DestroyAll();
		PrefabCollection<EventInfo>.DestroyAll();
		PrefabCollection<DisasterInfo>.DestroyAll();
		PrefabCollection<RadioContentInfo>.DestroyAll();
		PrefabCollection<RadioChannelInfo>.DestroyAll();
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private IEnumerator QuitApplicationCoroutine()
	{
		LoadingManager.<QuitApplicationCoroutine>c__Iterator4 <QuitApplicationCoroutine>c__Iterator = new LoadingManager.<QuitApplicationCoroutine>c__Iterator4();
		<QuitApplicationCoroutine>c__Iterator.$this = this;
		return <QuitApplicationCoroutine>c__Iterator;
	}

	private void AddChildrenToBuiltinStyle(GameObject obj, DistrictStyle style, bool spawnNormally)
	{
		if (obj == null || style == null)
		{
			return;
		}
		BuildingCollection[] componentsInChildren = obj.GetComponentsInChildren<BuildingCollection>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			BuildingInfo[] prefabs = componentsInChildren[i].m_prefabs;
			for (int j = 0; j < prefabs.Length; j++)
			{
				style.Add(prefabs[j]);
				prefabs[j].m_dontSpawnNormally = !spawnNormally;
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator LoadCustomContent()
	{
		LoadingManager.<LoadCustomContent>c__Iterator5 <LoadCustomContent>c__Iterator = new LoadingManager.<LoadCustomContent>c__Iterator5();
		<LoadCustomContent>c__Iterator.$this = this;
		return <LoadCustomContent>c__Iterator;
	}

	private void MetaDataLoaded()
	{
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		while (!Monitor.TryEnter(metaData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
			{
				string key = StringUtils.SafeFormat("{0}/{1}", new object[]
				{
					(!current.get_isBuiltin()) ? "User" : "Builtin",
					current.get_name()
				});
				bool flag;
				if (metaData.m_modOverride != null && metaData.m_modOverride.TryGetValue(key, out flag))
				{
					current.set_overrideState((!flag) ? 1 : 2);
				}
				else
				{
					current.set_overrideState(0);
				}
			}
		}
		finally
		{
			Monitor.Exit(metaData);
		}
		if (this.m_metaDataReady != null)
		{
			this.m_metaDataReady();
		}
	}

	[DebuggerHidden]
	private IEnumerator EssentialScenesLoaded()
	{
		LoadingManager.<EssentialScenesLoaded>c__Iterator6 <EssentialScenesLoaded>c__Iterator = new LoadingManager.<EssentialScenesLoaded>c__Iterator6();
		<EssentialScenesLoaded>c__Iterator.$this = this;
		return <EssentialScenesLoaded>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator RenderDataReady()
	{
		LoadingManager.<RenderDataReady>c__Iterator7 <RenderDataReady>c__Iterator = new LoadingManager.<RenderDataReady>c__Iterator7();
		<RenderDataReady>c__Iterator.$this = this;
		return <RenderDataReady>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoadIntroComplete()
	{
		LoadingManager.<LoadIntroComplete>c__Iterator8 <LoadIntroComplete>c__Iterator = new LoadingManager.<LoadIntroComplete>c__Iterator8();
		<LoadIntroComplete>c__Iterator.$this = this;
		return <LoadIntroComplete>c__Iterator;
	}

	private void PreLoadLevel()
	{
		if (this.m_levelPreLoaded != null)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("LevelPreLoaded");
			this.m_levelPreLoaded();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	private void PreUnloadLevel()
	{
		if (this.m_levelPreUnloaded != null)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("LevelPreUnloaded");
			this.m_levelPreUnloaded();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	[DebuggerHidden]
	private IEnumerator LoadLevelComplete(SimulationManager.UpdateMode mode)
	{
		LoadingManager.<LoadLevelComplete>c__Iterator9 <LoadLevelComplete>c__Iterator = new LoadingManager.<LoadLevelComplete>c__Iterator9();
		<LoadLevelComplete>c__Iterator.mode = mode;
		<LoadLevelComplete>c__Iterator.$this = this;
		return <LoadLevelComplete>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator UnloadLevelComplete()
	{
		LoadingManager.<UnloadLevelComplete>c__IteratorA <UnloadLevelComplete>c__IteratorA = new LoadingManager.<UnloadLevelComplete>c__IteratorA();
		<UnloadLevelComplete>c__IteratorA.$this = this;
		return <UnloadLevelComplete>c__IteratorA;
	}

	[DebuggerHidden]
	private IEnumerator LoadSimulationData(Package.Asset asset, SimulationMetaData ngs)
	{
		LoadingManager.<LoadSimulationData>c__IteratorB <LoadSimulationData>c__IteratorB = new LoadingManager.<LoadSimulationData>c__IteratorB();
		<LoadSimulationData>c__IteratorB.asset = asset;
		<LoadSimulationData>c__IteratorB.ngs = ngs;
		<LoadSimulationData>c__IteratorB.$this = this;
		return <LoadSimulationData>c__IteratorB;
	}

	private static void CheckMetaData(SimulationMetaData meta)
	{
		if (!string.IsNullOrEmpty(meta.m_ScenarioAsset))
		{
			while (!Monitor.TryEnter(meta, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				if (meta.m_modOverride == null)
				{
					meta.m_modOverride = new Dictionary<string, bool>();
				}
				if (!meta.m_modOverride.ContainsKey("Builtin/HardMode"))
				{
					meta.m_modOverride.Add("Builtin/HardMode", false);
				}
				if (!meta.m_modOverride.ContainsKey("Builtin/UnlimitedMoney"))
				{
					meta.m_modOverride.Add("Builtin/UnlimitedMoney", false);
				}
				if (!meta.m_modOverride.ContainsKey("Builtin/UnlockAll"))
				{
					meta.m_modOverride.Add("Builtin/UnlockAll", false);
				}
			}
			finally
			{
				Monitor.Exit(meta);
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator SaveSimulationData(string filename)
	{
		LoadingManager.<SaveSimulationData>c__IteratorC <SaveSimulationData>c__IteratorC = new LoadingManager.<SaveSimulationData>c__IteratorC();
		<SaveSimulationData>c__IteratorC.filename = filename;
		<SaveSimulationData>c__IteratorC.$this = this;
		return <SaveSimulationData>c__IteratorC;
	}

	[DebuggerHidden]
	private IEnumerator UnloadSimulationData()
	{
		LoadingManager.<UnloadSimulationData>c__IteratorD <UnloadSimulationData>c__IteratorD = new LoadingManager.<UnloadSimulationData>c__IteratorD();
		<UnloadSimulationData>c__IteratorD.$this = this;
		return <UnloadSimulationData>c__IteratorD;
	}

	public void WaitUntilEssentialScenesLoaded()
	{
		if (!this.m_essentialScenesLoaded)
		{
			this.m_loadingProfilerSimulation.PauseLoading();
			while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				while (!this.m_essentialScenesLoaded && !this.m_terminated)
				{
					Monitor.Wait(this.m_loadingLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(this.m_loadingLock);
			}
			this.m_loadingProfilerSimulation.ContinueLoading();
		}
	}

	public void WaitUntilRenderDataReady()
	{
		if (!this.m_renderDataReady)
		{
			this.m_loadingProfilerSimulation.PauseLoading();
			while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				while (!this.m_renderDataReady && !this.m_terminated)
				{
					Monitor.Wait(this.m_loadingLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(this.m_loadingLock);
			}
			this.m_loadingProfilerSimulation.ContinueLoading();
		}
	}

	public void SetSimulationProgress(float progress)
	{
		while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_simulationProgress = progress;
			this.m_loadingAnimation.LoadingProgress = Mathf.Min(this.m_sceneProgress, this.m_simulationProgress);
		}
		finally
		{
			Monitor.Exit(this.m_loadingLock);
		}
	}

	public void QueueLoadingAction(IEnumerator action)
	{
		while (!Monitor.TryEnter(this.m_loadingLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_mainThreadQueue.Enqueue(action);
			this.m_hasQueuedActions = true;
		}
		finally
		{
			Monitor.Exit(this.m_loadingLock);
		}
	}

	public bool SupportsExpansion(Expansion expansion)
	{
		return this.m_supportsExpansion[expansion];
	}

	private static readonly int kCustomAssetLoadOrderLevels = 7;

	private LoadingManager.AutoSaveTimer m_AutoSaveTimer = new LoadingManager.AutoSaveTimer();

	public volatile bool m_currentlyLoading;

	public volatile bool m_renderDataReady;

	public volatile bool m_metaDataLoaded;

	public volatile bool m_simulationDataLoaded;

	public volatile bool m_loadingComplete;

	public volatile bool m_essentialScenesLoaded;

	public volatile bool m_applicationQuitting;

	public volatile string m_loadedEnvironment;

	public volatile string m_loadedMapTheme;

	public LoadingProfiler m_loadingProfilerMain;

	public LoadingProfiler m_loadingProfilerSimulation;

	public LoadingProfiler m_loadingProfilerScenes;

	public LoadingProfiler m_loadingProfilerCustomContent;

	public LoadingProfiler m_loadingProfilerCustomAsset;

	public string m_brokenAssets;

	private object m_loadingLock;

	private volatile bool m_terminated;

	private volatile bool m_hasQueuedActions;

	private volatile float m_sceneProgress;

	private volatile float m_simulationProgress;

	private Queue<IEnumerator> m_mainThreadQueue;

	private Stopwatch m_stopWatch;

	private LoadingAnimation m_loadingAnimation;

	[NonSerialized]
	public LoadingProfilerUI m_loadingProfilerUI;

	[NonSerialized]
	public LoadingWrapper m_LoadingWrapper;

	[NonSerialized]
	public bool[] m_supportsExpansion;

	private SavedInt m_LoadingImageIndex = new SavedInt(Settings.loadingImageIndex, Settings.userGameState, 0, false);

	public class AutoSaveTimer
	{
		[DebuggerHidden]
		private IEnumerator AutoSaveRoutine()
		{
			LoadingManager.AutoSaveTimer.<AutoSaveRoutine>c__Iterator0 <AutoSaveRoutine>c__Iterator = new LoadingManager.AutoSaveTimer.<AutoSaveRoutine>c__Iterator0();
			<AutoSaveRoutine>c__Iterator.$this = this;
			return <AutoSaveRoutine>c__Iterator;
		}

		public void Pause()
		{
			this.m_Paused++;
		}

		public void UnPause()
		{
			this.m_Paused--;
			if (this.m_Paused < 0)
			{
				this.m_Paused = 0;
			}
		}

		public void Start()
		{
			this.Stop();
			if (((Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.Game && this.m_AutoSave.get_value()) || ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Editors) != ItemClass.Availability.None && this.m_EditorAutoSave.get_value())) && Singleton<LoadingManager>.get_exists() && Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				this.m_Running = true;
				this.m_AutoSaveCoroutine = Singleton<LoadingManager>.get_instance().StartCoroutine(this.AutoSaveRoutine());
			}
		}

		public void Stop()
		{
			if (Singleton<LoadingManager>.get_exists() && this.m_AutoSaveCoroutine != null)
			{
				this.m_Running = false;
				Singleton<LoadingManager>.get_instance().StopCoroutine(this.m_AutoSaveCoroutine);
				this.m_AutoSaveCoroutine = null;
			}
		}

		private SavedBool m_AutoSave = new SavedBool(Settings.autoSave, Settings.gameSettingsFile, DefaultSettings.autoSave, true);

		private SavedInt m_AutoSaveInterval = new SavedInt(Settings.autoSaveInterval, Settings.gameSettingsFile, DefaultSettings.autoSaveInterval, true);

		private SavedBool m_EditorAutoSave = new SavedBool(Settings.editorAutoSave, Settings.gameSettingsFile, DefaultSettings.editorAutoSave, true);

		private SavedInt m_EditorAutoSaveInterval = new SavedInt(Settings.editorAutoSaveInterval, Settings.gameSettingsFile, DefaultSettings.editorAutoSaveInterval, true);

		private Coroutine m_AutoSaveCoroutine;

		private bool m_Running;

		private int m_Paused;
	}

	public delegate void IntroLoadedHandler();

	public delegate void LevelLoadedHandler(SimulationManager.UpdateMode updateMode);

	public delegate void LevelUnloadedHandler();

	public delegate void LevelPreUnloadedHandler();

	public delegate void LevelPreLoadedHandler();

	public delegate void MetaDataReadyHandler();

	public delegate void SimulationDataReadyHandler();
}
