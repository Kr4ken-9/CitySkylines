﻿using System;
using System.Collections;
using ColossalFramework;
using ColossalFramework.UI;

public class AsyncTask : AsyncTaskBase
{
	public AsyncTask(IEnumerator action, string taskName = null) : base(taskName)
	{
		this.m_Action = action;
	}

	public object returnValue
	{
		get
		{
			return this.m_ReturnValue;
		}
	}

	public override void Execute()
	{
		try
		{
			this.m_Progress = 0f;
			while (this.m_Action.MoveNext())
			{
				this.m_ReturnValue = this.m_Action.Current;
				this.m_Progress += this.m_CachedStepCount;
			}
			if (this.m_Progress != 1f)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new object[]
				{
					"Progress steps did not match the amount of iterations for 'task ",
					this.name,
					"' [",
					this.m_Action,
					"]"
				}));
			}
			this.m_Progress = 1f;
		}
		catch (Exception ex)
		{
			this.m_Progress = 100f;
			UIView.ForwardException(ex);
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Simulation error: " + ex.Message + "\n" + ex.StackTrace);
		}
	}

	private IEnumerator m_Action;

	private object m_ReturnValue;
}
