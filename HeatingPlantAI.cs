﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class HeatingPlantAI : PlayerBuildingAI
{
	public override void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		if (this.m_pollutionAccumulation != 0)
		{
			resource1 = NaturalResourceManager.Resource.Pollution;
			position1 = data.m_position;
			radius1 = this.m_pollutionRadius;
		}
		else
		{
			resource1 = NaturalResourceManager.Resource.None;
			position1 = data.m_position;
			radius1 = 0f;
		}
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Heating)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				Color activeColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
				activeColor.a = 0f;
				return activeColor;
			}
			Color inactiveColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
			inactiveColor.a = 0f;
			return inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.Pollution)
			{
				int pollutionAccumulation = this.m_pollutionAccumulation;
				return ColorUtils.LinearLerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor, Mathf.Clamp01((float)pollutionAccumulation * 0.04f));
			}
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				int noiseAccumulation = this.m_noiseAccumulation;
				return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
			}
			if (infoMode != InfoManager.InfoMode.Connections)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			TransferManager.TransferReason resourceType = this.m_resourceType;
			if (resourceType != TransferManager.TransferReason.None && (data.m_tempImport != 0 || data.m_finalImport != 0))
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)resourceType];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		if (resource == NaturalResourceManager.Resource.Pollution && this.m_pollutionAccumulation != 0)
		{
			int num = (int)data.m_productionRate;
			if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				num = 0;
			}
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			num = PlayerBuildingAI.GetProductionRate(num, budget);
			if (this.m_resourceType != TransferManager.TransferReason.None)
			{
				int customBuffer = (int)data.m_customBuffer1;
				num = Mathf.Min(num, customBuffer / (this.m_resourceCapacity / 400));
			}
			return num * this.m_pollutionAccumulation / 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetHeatingRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) == Building.Flags.Active)
		{
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			num = PlayerBuildingAI.GetProductionRate(num, budget);
			if (this.m_resourceType != TransferManager.TransferReason.None)
			{
				int customBuffer = (int)data.m_customBuffer1;
				num = Mathf.Min(num, customBuffer / (this.m_resourceCapacity / 400) + 10);
			}
		}
		else
		{
			num = 0;
		}
		int num2;
		int num3;
		this.GetHeatingProduction(out num2, out num3);
		return num * num3 / 100;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		if (this.m_resourceType != TransferManager.TransferReason.None)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			base.CalculateGuestVehicles(buildingID, ref data, this.m_resourceType, ref num, ref num2, ref num3, ref num4);
			return StringUtils.SafeFormat("{0}: {1} (+{2})", new object[]
			{
				this.m_resourceType.ToString(),
				data.m_customBuffer1,
				num2
			});
		}
		return base.GetDebugString(buildingID, ref data);
	}

	public int GetResourceDuration(ushort buildingID, ref Building data)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num = productionRate * this.m_resourceConsumption / 100;
		int customBuffer = (int)data.m_customBuffer1;
		if (num != 0)
		{
			return (customBuffer / num + 8) / 16;
		}
		return 0;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Heating;
		subMode = InfoManager.SubInfoMode.Default;
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if (this.m_resourceType != TransferManager.TransferReason.None)
		{
			int num = (int)data.m_productionRate;
			if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				num = 0;
			}
			int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
			num = PlayerBuildingAI.GetProductionRate(num, budget);
			int customBuffer = (int)data.m_customBuffer1;
			if (customBuffer / (this.m_resourceCapacity / 400) + 10 < num)
			{
				return Locale.Get("BUILDING_STATUS_REDUCED");
			}
		}
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return Locale.Get("BUILDING_STATUS_DEFAULT");
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
		if (this.m_resourceType != TransferManager.TransferReason.None)
		{
			data.m_customBuffer1 = (ushort)(this.m_resourceCapacity >> 1);
		}
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainElectricity, position, 1.5f);
		if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Sad, NaturalResourceManager.Resource.Pollution, (float)this.m_pollutionAccumulation, this.m_pollutionRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseElectricity, position, 1.5f);
			if (this.m_pollutionAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius, buildingData.m_position, NotificationEvent.Type.Happy, NaturalResourceManager.Resource.Pollution, (float)(-(float)this.m_pollutionAccumulation), this.m_pollutionRadius);
			}
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == this.m_resourceType)
		{
			int num = (int)data.m_customBuffer1;
			amountDelta = Mathf.Clamp(amountDelta, 0, this.m_resourceCapacity - num);
			num += amountDelta;
			data.m_customBuffer1 = (ushort)num;
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		if (this.m_resourceType != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(this.m_resourceType, offer);
		}
		data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.NoFuel);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if (this.m_resourceType != TransferManager.TransferReason.None && (Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_tempImport = 0;
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			int num;
			int num3;
			if (this.m_resourceType != TransferManager.TransferReason.None)
			{
				num = (int)buildingData.m_customBuffer1;
				int num2 = finalProductionRate;
				finalProductionRate = Mathf.Min(finalProductionRate, num / (this.m_resourceCapacity / 400) + 10);
				num3 = finalProductionRate * this.m_resourceConsumption / 100;
				if (num < num3)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.NoFuel | Notification.Problem.MajorProblem);
				}
				else if (finalProductionRate < num2)
				{
					if ((buildingData.m_problems & (Notification.Problem.NoFuel | Notification.Problem.MajorProblem)) == (Notification.Problem.NoFuel | Notification.Problem.MajorProblem))
					{
						buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoFuel);
					}
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.NoFuel);
				}
				else
				{
					buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoFuel);
				}
			}
			else
			{
				num = 0;
				num3 = 0;
			}
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			int num4;
			int num5;
			this.GetHeatingProduction(out num4, out num5);
			int num6 = finalProductionRate * num5 / 100;
			if (num6 != 0 && num >= num3)
			{
				num -= num3;
				buildingData.m_customBuffer1 = (ushort)num;
				int num7 = (int)(buildingData.m_customBuffer2 * 1000 + buildingData.m_heatingBuffer);
				int num8 = num5 * 2 - num7;
				if (num8 > 0)
				{
					int num9 = Mathf.Min(num6, num8);
					num7 += num9;
					buildingData.m_customBuffer2 = (ushort)(num7 / 1000);
					buildingData.m_heatingBuffer = (ushort)(num7 - (int)(buildingData.m_customBuffer2 * 1000));
				}
				int num10 = finalProductionRate * this.m_pollutionAccumulation / 100;
				if (num10 != 0)
				{
					Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num10, num10, buildingData.m_position, this.m_pollutionRadius);
				}
			}
			else
			{
				finalProductionRate = 0;
			}
			if (num6 != 0)
			{
				int num11 = (int)(buildingData.m_customBuffer2 * 1000 + buildingData.m_heatingBuffer);
				int num12 = Mathf.Min(num11, num6);
				if (num12 > 0)
				{
					int num13 = Singleton<WaterManager>.get_instance().TryDumpHeating(buildingData.m_netNode, num6, num12);
					num11 -= num13;
					buildingData.m_customBuffer2 = (ushort)(num11 / 1000);
					buildingData.m_heatingBuffer = (ushort)(num11 - (int)(buildingData.m_customBuffer2 * 1000));
				}
				District[] expr_248_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_248_cp_0_cp_1 = district;
				expr_248_cp_0_cp_0[(int)expr_248_cp_0_cp_1].m_productionData.m_tempHeatingCapacity = expr_248_cp_0_cp_0[(int)expr_248_cp_0_cp_1].m_productionData.m_tempHeatingCapacity + (uint)num6;
			}
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
			if (this.m_resourceType != TransferManager.TransferReason.None)
			{
				int num14 = 0;
				int num15 = 0;
				int num16 = 0;
				int num17 = 0;
				base.CalculateGuestVehicles(buildingID, ref buildingData, this.m_resourceType, ref num14, ref num15, ref num16, ref num17);
				buildingData.m_tempImport = (byte)Mathf.Clamp(num17, (int)buildingData.m_tempImport, 255);
				if (buildingData.m_finalImport != 0)
				{
					District[] expr_2CF_cp_0_cp_0 = instance.m_districts.m_buffer;
					byte expr_2CF_cp_0_cp_1 = district;
					expr_2CF_cp_0_cp_0[(int)expr_2CF_cp_0_cp_1].m_playerConsumption.m_finalImportAmount = expr_2CF_cp_0_cp_0[(int)expr_2CF_cp_0_cp_1].m_playerConsumption.m_finalImportAmount + (uint)buildingData.m_finalImport;
				}
				int num18 = this.m_resourceCapacity - num - num16;
				if (num18 >= 4000)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = Mathf.Max(1, num18 * 8 / this.m_resourceCapacity);
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = false;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(this.m_resourceType, offer);
				}
			}
			int num19 = finalProductionRate * this.m_noiseAccumulation / 100;
			if (num19 != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num19, buildingData.m_position, this.m_noiseRadius);
			}
		}
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_heatingPlacement.Activate(properties.m_heatingBuilt);
		}
		return result;
	}

	public override void PlacementSucceeded()
	{
		Singleton<BuildingManager>.get_instance().m_heatingPlacement.Disable();
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void UpdateGuide(GuideController guideController)
	{
		if (this.m_info.m_notUsedGuide != null && !this.m_info.m_notUsedGuide.m_disabled && Singleton<WeatherManager>.get_instance().m_currentTemperature <= -10f)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
				bool flag = false;
				for (int i = 0; i < serviceBuildings.m_size; i++)
				{
					ushort num = serviceBuildings.m_buffer[i];
					if (num != 0)
					{
						BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
						if (info.m_class.m_level == this.m_info.m_class.m_level)
						{
							flag = true;
							break;
						}
					}
				}
				if (flag)
				{
					this.m_info.m_notUsedGuide.Disable();
				}
				else
				{
					this.m_info.m_notUsedGuide.Activate(guideController.m_heatingNotUsed, this.m_info);
				}
			}
		}
	}

	public override void GetHeatingProduction(out int min, out int max)
	{
		min = this.m_heatingProduction;
		max = this.m_heatingProduction;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = this.m_pollutionAccumulation;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		int num;
		int num2;
		this.GetHeatingProduction(out num, out num2);
		string text;
		if (num2 > num)
		{
			text = LocaleFormatter.FormatGeneric("AIINFO_HEATING_PRODUCTION_RANGE", new object[]
			{
				(num * 16 + 500) / 1000,
				(num2 * 16 + 500) / 1000
			});
		}
		else
		{
			text = LocaleFormatter.FormatGeneric("AIINFO_HEATING_PRODUCTION", new object[]
			{
				(num2 * 16 + 500) / 1000
			});
		}
		string text2 = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text2,
			LocaleFormatter.Info2,
			text
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int heatingRate = this.GetHeatingRate(buildingID, ref data);
		string text = LocaleFormatter.FormatGeneric("AIINFO_HEATING_PRODUCTION", new object[]
		{
			(heatingRate * 16 + 500) / 1000
		});
		if (this.m_resourceType == TransferManager.TransferReason.Coal)
		{
			text += Environment.NewLine;
			int resourceDuration = this.GetResourceDuration(buildingID, ref data);
			text += LocaleFormatter.FormatGeneric("AIINFO_COAL_STORED", new object[]
			{
				resourceDuration
			});
		}
		else if (this.m_resourceType == TransferManager.TransferReason.Petrol)
		{
			text += Environment.NewLine;
			int resourceDuration2 = this.GetResourceDuration(buildingID, ref data);
			text += LocaleFormatter.FormatGeneric("AIINFO_OIL_STORED", new object[]
			{
				resourceDuration2
			});
		}
		return text;
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_resourceType != TransferManager.TransferReason.None;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 5;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 12;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 9;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 4;

	public TransferManager.TransferReason m_resourceType = TransferManager.TransferReason.None;

	[CustomizableProperty("Resource Capacity")]
	public int m_resourceCapacity = 10000;

	[CustomizableProperty("Resource Consumption")]
	public int m_resourceConsumption = 100;

	[CustomizableProperty("Heat Production", "Heating")]
	public int m_heatingProduction = 1000;

	[CustomizableProperty("Pollution Accumulation", "Pollution")]
	public int m_pollutionAccumulation = 100;

	[CustomizableProperty("Pollution Radius", "Pollution")]
	public float m_pollutionRadius = 100f;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation = 50;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;
}
