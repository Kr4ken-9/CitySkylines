﻿using System;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public abstract class MainToolbar : GeneratedPanel
{
	protected override void Awake()
	{
		base.Awake();
		UIComponent uIComponent = UIView.Find("MovingPanel");
		if (uIComponent != null)
		{
			uIComponent.Hide();
		}
		this.m_FullscreenContainer = UIView.Find("FullScreenContainer");
		this.m_Bar = UIView.Find("BulldozerBar");
		this.m_BulldozerButton = UIView.Find<UIMultiStateButton>("BulldozerButton");
		this.m_BulldozerUndergroundToggle = UIView.Find<UIMultiStateButton>("BulldozerUndergroundToggle");
		this.m_GameAreaButton = UIView.Find<UIMultiStateButton>("ZoomButton");
		this.m_BulldozerButton.add_eventActiveStateIndexChanged(new PropertyChangedEventHandler<int>(this.OnBulldozerStateChanged));
		this.m_BulldozerButton.add_eventClick(new MouseEventHandler(this.OnBulldozerClicked));
		if (this.m_BulldozerUndergroundToggle != null)
		{
			this.m_BulldozerUndergroundToggle.add_eventActiveStateIndexChanged(new PropertyChangedEventHandler<int>(this.OnBulldozerUndergroundStateChanged));
		}
		if (this.m_GameAreaButton != null)
		{
			this.m_GameAreaButton.add_eventClick(new MouseEventHandler(this.OnGameAreaClicked));
		}
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.add_eventSelectedIndexChanged(delegate(UIComponent p, int i)
		{
			if (i == -1 && !GeneratedPanel.m_IsRefreshing)
			{
				ToolsModifierControl.SetTool<DefaultTool>();
				if (Singleton<InfoManager>.get_exists())
				{
					Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
				}
			}
		});
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.LevelLoaded);
		}
		UIView.Show(false);
	}

	protected override void OnDestroy()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.LevelLoaded);
		}
		base.OnDestroy();
	}

	private void LevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		UIView.Show(true);
	}

	public void ResetLastTool()
	{
		this.m_LastTool = ToolsModifierControl.GetTool<DefaultTool>();
		this.m_LastInfoMode = InfoManager.InfoMode.None;
		this.m_LastSubInfoMode = InfoManager.SubInfoMode.Default;
	}

	private void OnGameAreaClicked(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_GameAreaButton.get_activeStateIndex() == 0)
		{
			ToolsModifierControl.SetTool<DefaultTool>();
			if (this.m_LastTool == ToolsModifierControl.GetTool<DefaultTool>())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(this.m_LastInfoMode, this.m_LastSubInfoMode);
			}
		}
		else if (this.m_GameAreaButton.get_activeStateIndex() == 1)
		{
			this.m_LastTool = ToolsModifierControl.toolController.CurrentTool;
			if (ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<DefaultTool>())
			{
				this.m_LastInfoMode = Singleton<InfoManager>.get_instance().NextMode;
				this.m_LastSubInfoMode = Singleton<InfoManager>.get_instance().NextSubMode;
			}
			base.CloseAllPanels();
			base.CloseToolbar();
			ToolsModifierControl.SetTool<GameAreaTool>();
		}
	}

	private void UpdateGameAreaButton()
	{
		this.m_GameAreaButton.set_activeStateIndex((!(ToolsModifierControl.GetCurrentTool<GameAreaTool>() != null)) ? 0 : 1);
		if (ToolsModifierControl.GetCurrentTool<GameAreaTool>() != null)
		{
			this.m_GameAreaButton.set_state(1);
		}
		else if (this.m_GameAreaButton.get_state() != 3 && this.m_GameAreaButton.get_state() != 2)
		{
			this.m_GameAreaButton.set_state(0);
		}
	}

	private void OnBulldozerClicked(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_BulldozerButton.get_activeStateIndex() == 1)
		{
			this.m_LastTool = ToolsModifierControl.toolController.CurrentTool;
			this.m_LastInfoMode = Singleton<InfoManager>.get_instance().NextMode;
			this.m_LastSubInfoMode = Singleton<InfoManager>.get_instance().NextSubMode;
			BulldozeTool bulldozeTool = ToolsModifierControl.SetTool<BulldozeTool>();
			if (this.m_BulldozerUndergroundToggle != null)
			{
				this.m_BulldozerUndergroundToggle.set_isVisible(true);
				if (bulldozeTool.BulldozeLayer != BulldozeTool.Layer.Pipes)
				{
					bulldozeTool.BulldozeLayer = ((this.m_BulldozerUndergroundToggle.get_activeStateIndex() != 0) ? BulldozeTool.Layer.Tunnels : BulldozeTool.Layer.Default);
				}
			}
		}
		else if (this.m_BulldozerButton.get_activeStateIndex() == 0)
		{
			if (this.m_LastTool == ToolsModifierControl.GetTool<CameraTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
			}
			else
			{
				ToolsModifierControl.toolController.CurrentTool = this.m_LastTool;
			}
			if (this.m_LastTool == ToolsModifierControl.GetTool<DefaultTool>())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(this.m_LastInfoMode, this.m_LastSubInfoMode);
			}
			if (this.m_BulldozerUndergroundToggle != null)
			{
				this.m_BulldozerUndergroundToggle.set_isVisible(false);
			}
		}
	}

	private void OnBulldozerUndergroundStateChanged(UIComponent comp, int index)
	{
		BulldozeTool tool = ToolsModifierControl.GetTool<BulldozeTool>();
		if (tool.BulldozeLayer != BulldozeTool.Layer.Pipes)
		{
			tool.BulldozeLayer = ((index != 0) ? BulldozeTool.Layer.Tunnels : BulldozeTool.Layer.Default);
		}
	}

	private void OnBulldozerStateChanged(UIComponent comp, int index)
	{
		if (index == 1)
		{
			this.m_Bar.Show();
			ValueAnimator.Animate("BulldozerBar", delegate(float val)
			{
				Vector3 relativePosition = this.m_Bar.get_relativePosition();
				relativePosition.y = val;
				this.m_Bar.set_relativePosition(relativePosition);
			}, new AnimatedFloat(this.m_FullscreenContainer.get_relativePosition().y + this.m_FullscreenContainer.get_size().y, this.m_FullscreenContainer.get_relativePosition().y + this.m_FullscreenContainer.get_size().y - this.m_Bar.get_size().y, 0.3f));
		}
		else if (index == 0)
		{
			ValueAnimator.Animate("BulldozerBar", delegate(float val)
			{
				Vector3 relativePosition = this.m_Bar.get_relativePosition();
				relativePosition.y = val;
				this.m_Bar.set_relativePosition(relativePosition);
			}, new AnimatedFloat(this.m_FullscreenContainer.get_relativePosition().y + this.m_FullscreenContainer.get_size().y - this.m_Bar.get_size().y, this.m_FullscreenContainer.get_relativePosition().y + this.m_FullscreenContainer.get_size().y, 0.3f), delegate
			{
				this.m_Bar.Hide();
			});
		}
	}

	private void UpdateBulldozerButton()
	{
		bool flag = false;
		if (ToolsModifierControl.toolController != null)
		{
			flag = (ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<BulldozeTool>());
		}
		this.m_BulldozerButton.set_activeStateIndex((!flag) ? 0 : 1);
		if (Singleton<InfoManager>.get_exists() && Singleton<ToolManager>.get_exists())
		{
			if ((Singleton<InfoManager>.get_instance().NextMode == InfoManager.InfoMode.Water || Singleton<InfoManager>.get_instance().NextMode == InfoManager.InfoMode.Heating) && Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.Game)
			{
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_normal("ToolbarIconBulldozerPipes");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_hovered("ToolbarIconBulldozerPipesHovered");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_pressed("ToolbarIconBulldozerPipesPressed");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(1).set_normal("ToolbarIconBulldozerPipesFocused");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_disabled("ToolbarIconBulldozerPipesDisabled");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(1).set_disabled("ToolbarIconBulldozerPipesDisabled");
				if (this.m_BulldozerUndergroundToggle != null)
				{
					this.m_BulldozerUndergroundToggle.set_isVisible(false);
				}
			}
			else
			{
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_normal("ToolbarIconBulldozer");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_hovered("ToolbarIconBulldozerHovered");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_pressed("ToolbarIconBulldozerPressed");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(1).set_normal("ToolbarIconBulldozerFocused");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(0).set_disabled("ToolbarIconBulldozerDisabled");
				this.m_BulldozerButton.get_foregroundSprites().get_Item(1).set_disabled("ToolbarIconBulldozerDisabled");
				if (this.m_BulldozerUndergroundToggle != null)
				{
					this.m_BulldozerUndergroundToggle.set_isVisible(flag);
				}
			}
		}
	}

	private void Update()
	{
		this.UpdateBulldozerButton();
		if (this.m_GameAreaButton != null)
		{
			this.UpdateGameAreaButton();
		}
	}

	public override void CleanPanel()
	{
	}

	protected void ResetPanel()
	{
		base.CloseEverything();
		UITemplateManager.ClearInstances(MainToolbar.kEmptyContainer);
		UITemplateManager.ClearInstances(MainToolbar.kMainToolbarSeparatorTemplate);
		UITemplateManager.ClearInstances(MainToolbar.kMainToolbarButtonTemplate);
		UITemplateManager.ClearInstances(MainToolbar.kScrollablePanelTemplate);
		UITemplateManager.ClearInstances(MainToolbar.kScrollableSubPanelTemplate);
	}

	private string GetBackgroundIconGroup(string name)
	{
		if (name == "Roads" || name == "Zoning" || name == "District")
		{
			return "Group1";
		}
		if (name == "Electricity" || name == "WaterAndSewage" || name == "Garbage")
		{
			return "Group2";
		}
		if (name == "Healthcare" || name == "FireDepartment" || name == "Police")
		{
			return "Group3";
		}
		if (name == "Government" || name == "Education" || name == "PublicTransport")
		{
			return "Group4";
		}
		if (name == "Beautification" || name == "Monuments" || name == "Wonders")
		{
			return "Group5";
		}
		if (name == "Money" || name == "Policies" || name == "Landscaping")
		{
			return "Group6";
		}
		return "Base";
	}

	private string GetBackgroundSprite(UIButton button, string spriteBase, string name, string state)
	{
		string text = spriteBase + this.GetBackgroundIconGroup(name) + state;
		string text2 = spriteBase + "Base" + state;
		return (!(button.get_atlas().get_Item(text) != null)) ? text2 : text;
	}

	protected UIButton SpawnButtonEntry(UITabstrip strip, string name, string localeID, string spriteBase, bool enabled)
	{
		return this.SpawnButtonEntry(strip, name, localeID, null, spriteBase, enabled);
	}

	protected UIButton SpawnButtonEntry(UITabstrip strip, string name, string localeID, string unlockText, string spriteBase, bool enabled)
	{
		return this.SpawnButtonEntry(strip, name, localeID, unlockText, spriteBase, enabled, false);
	}

	protected UIButton SpawnSubEntry(UITabstrip strip, string name, string localeID, string unlockText, string spriteBase, bool enabled)
	{
		string str = name + "Group";
		Type type = Type.GetType(str + "Panel");
		if (type != null && !type.IsSubclassOf(typeof(GeneratedGroupPanel)))
		{
			type = null;
		}
		if (type == null)
		{
			return null;
		}
		UIButton uIButton;
		if (strip.get_childCount() > this.m_ObjectIndex)
		{
			uIButton = (strip.get_components()[this.m_ObjectIndex] as UIButton);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(MainToolbar.kMainToolbarButtonTemplate);
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(MainToolbar.kScrollableSubPanelTemplate);
			uIButton = (strip.AddTab(name, asGameObject, asGameObject2, new Type[]
			{
				type
			}) as UIButton);
		}
		uIButton.set_isEnabled(enabled);
		uIButton.get_gameObject().GetComponent<TutorialUITag>().tutorialTag = name;
		GeneratedGroupPanel generatedGroupPanel = strip.GetComponentInContainer(uIButton, type) as GeneratedGroupPanel;
		if (generatedGroupPanel != null)
		{
			generatedGroupPanel.get_component().set_isInteractive(true);
			generatedGroupPanel.m_OptionsBar = this.m_OptionsBar;
			generatedGroupPanel.m_DefaultInfoTooltipAtlas = this.m_DefaultInfoTooltipAtlas;
			if (enabled)
			{
				generatedGroupPanel.RefreshPanel();
			}
		}
		uIButton.set_normalBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Normal"));
		uIButton.set_focusedBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Focused"));
		uIButton.set_hoveredBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Hovered"));
		uIButton.set_pressedBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Pressed"));
		uIButton.set_disabledBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Disabled"));
		string text = spriteBase + name;
		uIButton.set_normalFgSprite(text);
		uIButton.set_focusedFgSprite(text + "Focused");
		uIButton.set_hoveredFgSprite(text + "Hovered");
		uIButton.set_pressedFgSprite(text + "Pressed");
		uIButton.set_disabledFgSprite(text + "Disabled");
		if (unlockText != null)
		{
			uIButton.set_tooltip(Locale.Get(localeID, name) + " - " + unlockText);
		}
		else
		{
			uIButton.set_tooltip(Locale.Get(localeID, name));
		}
		this.m_ObjectIndex++;
		return uIButton;
	}

	protected UIButton SpawnButtonEntry(UITabstrip strip, string name, string localeID, string unlockText, string spriteBase, bool enabled, bool forceFillContainer)
	{
		Type type = Type.GetType(name + "Panel");
		if (type != null && !type.IsSubclassOf(typeof(GeneratedScrollPanel)))
		{
			type = null;
		}
		UIButton uIButton;
		if (strip.get_childCount() > this.m_ObjectIndex)
		{
			uIButton = (strip.get_components()[this.m_ObjectIndex] as UIButton);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(MainToolbar.kMainToolbarButtonTemplate);
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(MainToolbar.kScrollablePanelTemplate);
			uIButton = (strip.AddTab(name, asGameObject, asGameObject2, new Type[]
			{
				type
			}) as UIButton);
		}
		uIButton.set_isEnabled(enabled);
		uIButton.get_gameObject().GetComponent<TutorialUITag>().tutorialTag = name;
		GeneratedScrollPanel generatedScrollPanel = strip.GetComponentInContainer(uIButton, type) as GeneratedScrollPanel;
		if (generatedScrollPanel != null)
		{
			generatedScrollPanel.get_component().set_isInteractive(true);
			generatedScrollPanel.m_OptionsBar = this.m_OptionsBar;
			generatedScrollPanel.m_DefaultInfoTooltipAtlas = this.m_DefaultInfoTooltipAtlas;
			if (forceFillContainer || enabled)
			{
				generatedScrollPanel.RefreshPanel();
			}
			if (generatedScrollPanel.selectedIndex == -1)
			{
				generatedScrollPanel.selectedIndex = 0;
			}
		}
		uIButton.set_normalBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Normal"));
		uIButton.set_focusedBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Focused"));
		uIButton.set_hoveredBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Hovered"));
		uIButton.set_pressedBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Pressed"));
		uIButton.set_disabledBgSprite(this.GetBackgroundSprite(uIButton, spriteBase, name, "Disabled"));
		string text = spriteBase + name;
		uIButton.set_normalFgSprite(text);
		uIButton.set_focusedFgSprite(text + "Focused");
		uIButton.set_hoveredFgSprite(text + "Hovered");
		uIButton.set_pressedFgSprite(text + "Pressed");
		uIButton.set_disabledFgSprite(text + "Disabled");
		if (unlockText != null)
		{
			uIButton.set_tooltip(Locale.Get(localeID, name) + " - " + unlockText);
		}
		else
		{
			uIButton.set_tooltip(Locale.Get(localeID, name));
		}
		this.m_ObjectIndex++;
		return uIButton;
	}

	protected UIComponent SpawnSeparator(UITabstrip strip)
	{
		UIComponent uIComponent;
		if (strip.get_childCount() > this.m_ObjectIndex)
		{
			uIComponent = strip.get_components()[this.m_ObjectIndex];
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(MainToolbar.kMainToolbarSeparatorTemplate);
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(MainToolbar.kEmptyContainer);
			uIComponent = strip.AddTab("Separator", asGameObject, asGameObject2, new Type[0]);
			uIComponent.set_width(uIComponent.get_width() * 1.5f);
		}
		uIComponent.set_isEnabled(false);
		this.m_ObjectIndex++;
		return uIComponent;
	}

	protected UIComponent SpawnSmallSeparator(UITabstrip strip)
	{
		UIComponent uIComponent;
		if (strip.get_childCount() > this.m_ObjectIndex)
		{
			uIComponent = strip.get_components()[this.m_ObjectIndex];
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(MainToolbar.kMainToolbarSeparatorTemplate);
			GameObject asGameObject2 = UITemplateManager.GetAsGameObject(MainToolbar.kEmptyContainer);
			uIComponent = strip.AddTab("SmallSeparator", asGameObject, asGameObject2, new Type[0]);
			uIComponent.set_width(uIComponent.get_width() * 0.5f);
		}
		uIComponent.set_isEnabled(false);
		this.m_ObjectIndex++;
		return uIComponent;
	}

	private void ProfiledRefreshPanel()
	{
		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();
		this.RefreshPanel();
		stopwatch.Stop();
		Debug.Log("Refreshed panel in " + stopwatch.ElapsedMilliseconds + "ms");
	}

	public override void RefreshPanel()
	{
		this.CleanPanel();
		this.m_ObjectIndex = 0;
		if (Singleton<ToolManager>.get_exists() && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Editors) != ItemClass.Availability.None)
		{
			this.m_BulldozerButton.set_isEnabled(true);
		}
		else
		{
			this.m_BulldozerButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Bulldozer));
		}
		this.m_BulldozerButton.set_tooltip(Locale.Get("MAIN_TOOL", "Bulldozer"));
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	protected virtual void OnLocaleChanged()
	{
		this.RefreshPanel();
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.RefreshPanel();
	}

	public virtual void ShowPoliciesPanel(byte district)
	{
	}

	public virtual void ShowEconomyPanel(int tabIndex)
	{
	}

	private static readonly string kEmptyContainer = "EmptyContainer";

	private static readonly string kMainToolbarSeparatorTemplate = "MainToolbarSeparator";

	private static readonly string kMainToolbarButtonTemplate = "MainToolbarButtonTemplate";

	private static readonly string kScrollablePanelTemplate = "ScrollablePanelTemplate";

	private static readonly string kScrollableSubPanelTemplate = "ScrollableSubPanelTemplate";

	private int m_ObjectIndex;

	public UIComponent m_OptionsBar;

	public UITextureAtlas m_DefaultInfoTooltipAtlas;

	private UIComponent m_Bar;

	private UIComponent m_FullscreenContainer;

	protected UIMultiStateButton m_BulldozerButton;

	protected UIMultiStateButton m_BulldozerUndergroundToggle;

	protected UIMultiStateButton m_GameAreaButton;

	private ToolBase m_LastTool;

	private InfoManager.InfoMode m_LastInfoMode;

	private InfoManager.SubInfoMode m_LastSubInfoMode;
}
