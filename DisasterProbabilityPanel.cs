﻿using System;
using System.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class DisasterProbabilityPanel : UICustomControl
{
	public event DisasterProbabilityPanel.EventProbabilityChangedHandler EventProbabilityChanged
	{
		add
		{
			DisasterProbabilityPanel.EventProbabilityChangedHandler eventProbabilityChangedHandler = this.EventProbabilityChanged;
			DisasterProbabilityPanel.EventProbabilityChangedHandler eventProbabilityChangedHandler2;
			do
			{
				eventProbabilityChangedHandler2 = eventProbabilityChangedHandler;
				eventProbabilityChangedHandler = Interlocked.CompareExchange<DisasterProbabilityPanel.EventProbabilityChangedHandler>(ref this.EventProbabilityChanged, (DisasterProbabilityPanel.EventProbabilityChangedHandler)Delegate.Combine(eventProbabilityChangedHandler2, value), eventProbabilityChangedHandler);
			}
			while (eventProbabilityChangedHandler != eventProbabilityChangedHandler2);
		}
		remove
		{
			DisasterProbabilityPanel.EventProbabilityChangedHandler eventProbabilityChangedHandler = this.EventProbabilityChanged;
			DisasterProbabilityPanel.EventProbabilityChangedHandler eventProbabilityChangedHandler2;
			do
			{
				eventProbabilityChangedHandler2 = eventProbabilityChangedHandler;
				eventProbabilityChangedHandler = Interlocked.CompareExchange<DisasterProbabilityPanel.EventProbabilityChangedHandler>(ref this.EventProbabilityChanged, (DisasterProbabilityPanel.EventProbabilityChangedHandler)Delegate.Remove(eventProbabilityChangedHandler2, value), eventProbabilityChangedHandler);
			}
			while (eventProbabilityChangedHandler != eventProbabilityChangedHandler2);
		}
	}

	public string label
	{
		get
		{
			return this.m_NameLabel.get_text();
		}
		set
		{
			this.m_NameLabel.set_text(value);
		}
	}

	public int probability
	{
		get
		{
			return (int)this.m_Slider.get_value();
		}
		set
		{
			int num = Mathf.Clamp(value, 0, 100);
			this.UnBindEvents();
			this.m_Slider.set_value((float)num);
			this.m_Prob.set_text(num.ToString());
			this.BindEvents();
			this.OnProbabilityChanged();
		}
	}

	public void Awake()
	{
		this.m_Slider = base.get_component().Find<UISlider>("Slider");
		this.m_Slider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		this.m_Prob = base.get_component().Find<UITextField>("Prob");
		this.m_Prob.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextSubmitted));
		this.m_NameLabel = base.get_component().Find<UILabel>("Name");
	}

	private void BindEvents()
	{
		this.UnBindEvents();
		this.m_Slider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
	}

	private void UnBindEvents()
	{
		this.m_Slider.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
	}

	private void OnTextSubmitted(UIComponent comp, string value)
	{
		int probability;
		if (!int.TryParse(value, out probability))
		{
			probability = 0;
		}
		this.probability = probability;
	}

	private void OnSliderValueChanged(UIComponent comp, float value)
	{
		this.probability = Mathf.RoundToInt(value);
	}

	private void OnProbabilityChanged()
	{
		if (this.EventProbabilityChanged != null)
		{
			this.EventProbabilityChanged(this);
		}
	}

	private UISlider m_Slider;

	private UILabel m_NameLabel;

	private UITextField m_Prob;

	public delegate void EventProbabilityChangedHandler(DisasterProbabilityPanel source);
}
