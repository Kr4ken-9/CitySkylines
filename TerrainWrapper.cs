﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class TerrainWrapper : ITerrain
{
	public TerrainWrapper(TerrainManager terrainManager)
	{
		this.m_terrainManager = terrainManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnTerrainExtensionsReleased();
		this.m_TerrainExtensions = Singleton<PluginManager>.get_instance().GetImplementations<ITerrainExtension>();
		this.OnTerrainExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnTerrainExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnTerrainExtensionsCreated()
	{
		for (int i = 0; i < this.m_TerrainExtensions.Count; i++)
		{
			try
			{
				this.m_TerrainExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_TerrainExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnTerrainExtensionsReleased()
	{
		for (int i = 0; i < this.m_TerrainExtensions.Count; i++)
		{
			try
			{
				this.m_TerrainExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_TerrainExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnAfterHeightsModified(float minX, float minZ, float maxX, float maxZ)
	{
		for (int i = 0; i < this.m_TerrainExtensions.Count; i++)
		{
			this.m_TerrainExtensions[i].OnAfterHeightsModified(minX, minZ, maxX, maxZ);
		}
	}

	public int heightMapResolution
	{
		get
		{
			return 1080;
		}
	}

	public float cellSize
	{
		get
		{
			return 16f;
		}
	}

	public float RawToHeight(ushort rawHeight)
	{
		return 0.015625f * (float)rawHeight;
	}

	public ushort HeightToRaw(float height)
	{
		return (ushort)Mathf.Clamp(Mathf.RoundToInt(height * 64f), 0, 65535);
	}

	public void PositionToHeightMapCoord(float x, float z, out int heightX, out int heightZ)
	{
		heightX = Mathf.Clamp(Mathf.RoundToInt(x / 16f + 540f), 0, 1080);
		heightZ = Mathf.Clamp(Mathf.RoundToInt(z / 16f + 540f), 0, 1080);
	}

	public void HeightMapCoordToPosition(int heightX, int heightZ, out float x, out float z)
	{
		x = ((float)heightX - 540f) * 16f;
		z = ((float)heightZ - 540f) * 16f;
	}

	public void GetHeights(int heightX, int heightZ, int heightWidth, int heightLength, ushort[] rawHeights)
	{
		ushort[] finalHeights = this.m_terrainManager.FinalHeights;
		for (int i = 0; i < heightLength; i++)
		{
			for (int j = 0; j < heightWidth; j++)
			{
				ushort num = finalHeights[(i + heightZ) * 1081 + (j + heightX)];
				rawHeights[i * heightLength + j] = num;
			}
		}
	}

	public void SetHeights(int heightX, int heightZ, int heightWidth, int heightLength, ushort[] rawHeights)
	{
		ushort[] rawHeights2 = this.m_terrainManager.RawHeights;
		for (int i = 0; i < heightLength; i++)
		{
			for (int j = 0; j < heightWidth; j++)
			{
				ushort num = rawHeights[i * heightLength + j];
				rawHeights2[(i + heightZ) * 1081 + (j + heightX)] = num;
			}
		}
		heightX -= 2;
		heightZ -= 2;
		heightWidth += 3;
		heightLength += 3;
		int num2 = 120;
		if (heightWidth * heightLength > num2 * num2)
		{
			int num3 = (heightWidth + num2 - 1) / num2;
			int num4 = (heightLength + num2 - 1) / num2;
			for (int k = 0; k < num4; k++)
			{
				for (int l = 0; l < num3; l++)
				{
					TerrainModify.UpdateArea(heightX + (l * heightWidth + num3 - 1) / num3, heightZ + (k * heightLength + num4 - 1) / num4, heightX + ((l + 1) * heightWidth + num3 - 1) / num3, heightZ + ((k + 1) * heightLength + num4 - 1) / num4, true, false, false);
				}
			}
		}
		else
		{
			TerrainModify.UpdateArea(heightX, heightZ, heightX + heightWidth, heightZ + heightLength, true, false, false);
		}
	}

	public float SampleTerrainHeight(float x, float z)
	{
		return this.m_terrainManager.SampleDetailHeight(new Vector3(x, 0f, z));
	}

	public float SampleWaterHeight(float x, float z)
	{
		return this.m_terrainManager.WaterLevel(new Vector2(x, z));
	}

	private TerrainManager m_terrainManager;

	private List<ITerrainExtension> m_TerrainExtensions = new List<ITerrainExtension>();
}
