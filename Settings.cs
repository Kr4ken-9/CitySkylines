﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;

public static class Settings
{
	public static string moneyFormat
	{
		get
		{
			if (!Settings.m_Initialized && SingletonLite<LocaleManager>.get_exists())
			{
				Settings.sMoneyFormat = Locale.Get("MONEY_FORMAT");
				Settings.m_Initialized = true;
			}
			return Settings.sMoneyFormat;
		}
	}

	public static string moneyFormatNoCents
	{
		get
		{
			if (!Settings.m_Initialized && SingletonLite<LocaleManager>.get_exists())
			{
				Settings.sMoneyFormatNoCents = Locale.Get("MONEY_FORMATNOCENTS");
				Settings.m_Initialized = true;
			}
			return Settings.sMoneyFormatNoCents;
		}
	}

	private static bool m_Initialized;

	public static string sMoneyFormat = "C";

	public static string sMoneyFormatNoCents = "C0";

	internal static readonly string colossal = "colossal";

	internal static readonly string firstTime = "firstTime";

	public static readonly string gameSettingsFile = "gameSettings";

	public static readonly string inputSettingsFile = "gameSettings";

	public static readonly string devGameSettingsFile = "devFlags";

	public static readonly string userGameState = "userGameState";

	public static readonly string sharedSettings = "sharedSettings";

	public static readonly string nightPlayCount = "nightPlayCount";

	public static readonly string loadingImageIndex = "loadingImageIndex";

	public static readonly string chirperImage = "chirperImage";

	public static readonly string latestSeenWhatsNewPanelVersion = "latestSeenWhatsNewPanelVersion";

	public static readonly string hasSeenStreamingWarningGoldFM = "hasSeenStreamingWarningGoldFM";

	public static readonly string hasSetDisastersOnOff = "hasSetDisastersOnOff";

	public static readonly string agreedModsDisclaimer = "agreedModsDisclaimer";

	public static readonly string agreedStylesDisclaimer = "agreedStylesDisclaimer";

	public static readonly string autoExpandChirper = "autoExpandChirper";

	public static readonly string autoSave = "autoSave";

	public static readonly string autoSaveInterval = "autoSaveInterval";

	public static readonly string editorAutoSave = "editorAutoSave";

	public static readonly string editorAutoSaveInterval = "editorAutoSaveInterval";

	public static readonly string temperatureUnit = "temperatureUnit";

	public static readonly string localeID = "localeID";

	public static readonly string tutorialMessages = "tutorialMessages";

	public static readonly string enableDayNight = "enableDayNight";

	public static readonly string enableWeather = "enableWeather";

	public static readonly string randomDisastersEnabled = "randomDisastersEnabled";

	public static readonly string randomDisastersProbability = "randomDisastersProbability";

	public static readonly string roadNamesVisible = "roadNamesVisible";

	public static readonly string fadeAwayNotifications = "fadeAwayNotifications";

	public static readonly string screenWidth = "screenWidth";

	public static readonly string screenHeight = "screenHeight";

	public static readonly string fullscreen = "fullscreen";

	public static readonly string vsync = "vsync";

	public static readonly string shadowsDistance = "shadowsDistance";

	public static readonly string shadowsQuality = "shadowsQuality";

	public static readonly string texturesQuality = "texturesQuality";

	public static readonly string antialiasing = "antialiasing";

	public static readonly string anisotropicFiltering = "anisotropicFiltering";

	public static readonly string levelOfDetail = "levelOfDetail";

	public static readonly string dofMode = "dofMode";

	public static readonly string tiltShiftAmount = "tiltShiftAmount";

	public static readonly string filmGrainAmount = "filmGrainAmount";

	public static readonly string edgeScrolling = "edgeScrolling";

	public static readonly string invertYMouse = "invertYMouse";

	public static readonly string edgeScrollSensitivity = "edgeScrollSensitivity";

	public static readonly string mouseLightIntensity = "mouseLightIntensity";

	public static readonly string builtinLUT = "builtinLUT";

	public static readonly string userLUT = "userLUT";

	public static readonly string mainAudioVolume = "mainAudioVolume";

	public static readonly string musicAudioVolume = "musicAudioVolume";

	public static readonly string ambientAudioVolume = "ambientAudioVolume";

	public static readonly string broadcastAudioVolume = "broadcastAudioVolume";

	public static readonly string effectAudioVolume = "effectAudioVolume";

	public static readonly string uiAudioVolume = "uiAudioVolume";

	public static readonly string chirperAudioVolume = "chirperAudioVolume";

	[RebindableKey]
	public static readonly string buildElevationUp = "buildElevationUp";

	[RebindableKey]
	public static readonly string buildElevationDown = "buildElevationDown";

	[RebindableKey]
	public static readonly string sharedShortcutToggleSnappingMenu = "sharedShortcutToggleSnappingMenu";

	[RebindableKey]
	public static readonly string sharedShortcutElevationStep = "sharedShortcutElevationStep";

	[RebindableKey]
	public static readonly string sharedShortcutOptionsButton1 = "sharedShortcutOptionsButton1";

	[RebindableKey]
	public static readonly string sharedShortcutOptionsButton2 = "sharedShortcutOptionsButton2";

	[RebindableKey]
	public static readonly string sharedShortcutOptionsButton3 = "sharedShortcutOptionsButton3";

	[RebindableKey]
	public static readonly string sharedShortcutOptionsButton4 = "sharedShortcutOptionsButton4";

	[RebindableKey]
	public static readonly string inGameShortcutSnapToAngle = "inGameShortcutSnapToAngle";

	[RebindableKey]
	public static readonly string inGameShortcutSnapToLength = "inGameShortcutSnapToLength";

	[RebindableKey]
	public static readonly string inGameShortcutSnapToGrid = "inGameShortcutSnapToGrid";

	[RebindableKey]
	public static readonly string inGameShortcutSnapToHelperLines = "inGameShortcutSnapToHelperLines";

	[RebindableKey]
	public static readonly string inGameShortcutSnapToAll = "inGameShortcutSnapToAll";

	public static readonly string mouseWheelZoom = "mouseWheelZoom";

	public static readonly string mouseSensitivity = "mouseSensitivity";

	[RebindableKey]
	public static readonly string cameraMoveLeft = "cameraMoveLeft";

	[RebindableKey]
	public static readonly string cameraMoveRight = "cameraMoveRight";

	[RebindableKey]
	public static readonly string cameraMoveForward = "cameraMoveForward";

	[RebindableKey]
	public static readonly string cameraMoveBackward = "cameraMoveBackward";

	[RebindableKey]
	public static readonly string cameraMouseRotate = "cameraMouseRotate";

	[RebindableKey]
	public static readonly string cameraRotateLeft = "cameraRotateLeft";

	[RebindableKey]
	public static readonly string cameraRotateRight = "cameraRotateRight";

	[RebindableKey]
	public static readonly string cameraRotateUp = "cameraRotateUp";

	[RebindableKey]
	public static readonly string cameraRotateDown = "cameraRotateDown";

	[RebindableKey]
	public static readonly string cameraZoomCloser = "cameraZoomCloser";

	[RebindableKey]
	public static readonly string cameraZoomAway = "cameraZoomAway";

	[RebindableKey]
	public static readonly string inGameShortcutFreeCameraMode = "inGameShortcutFreeCameraMode";

	[RebindableKey]
	public static readonly string debugOuput = "debugOuput";

	[RebindableKey]
	public static readonly string screenshot = "screenshot";

	[RebindableKey]
	public static readonly string hiresScreenshot = "hiresScreenshot";

	[RebindableKey("Game")]
	public static readonly string inGameSimulationPause = "inGameSimulationPause";

	[RebindableKey("Game")]
	public static readonly string inGameSimulationSpeed1 = "inGameSimulationSpeed1";

	[RebindableKey("Game")]
	public static readonly string inGameSimulationSpeed2 = "inGameSimulationSpeed2";

	[RebindableKey("Game")]
	public static readonly string inGameSimulationSpeed3 = "inGameSimulationSpeed3";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutQuickSave = "inGameShortcutQuickSave";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutQuickLoad = "inGameShortcutQuickLoad";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoads = "inGameShortcutRoads";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoning = "inGameShortcutZoning";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutDistricts = "inGameShortcutDistricts";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutElectricity = "inGameShortcutElectricity";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutWater = "inGameShortcutWater";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutGarbage = "inGameShortcutGarbage";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutHealthcare = "inGameShortcutHealthcare";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutFireDepartment = "inGameShortcutFireDepartment";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPoliceDepartment = "inGameShortcutPoliceDepartment";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutEducation = "inGameShortcutEducation";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransport = "inGameShortcutPublicTransport";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutDecoration = "inGameShortcutDecoration";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonuments = "inGameShortcutMonuments";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutWonders = "inGameShortcutWonders";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscaping = "inGameShortcutLandscaping";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutEconomy = "inGameShortcutEconomy";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBudget = "inGameShortcutBudget";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutTaxes = "inGameShortcutTaxes";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLoans = "inGameShortcutLoans";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPolicies = "inGameShortcutPolicies";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutGameAreas = "inGameShortcutGameAreas";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBulldozer = "inGameShortcutBulldozer";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBulldozerUnderground = "inGameShortcutBulldozerUnderground";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoadsSmallGroup = "inGameShortcutRoadsSmallGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoadsMediumGroup = "inGameShortcutRoadsMediumGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoadsLargeGroup = "inGameShortcutRoadsLargeGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoadsHighwayGroup = "inGameShortcutRoadsHighwayGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutRoadsIntersectionGroup = "inGameShortcutRoadsIntersectionGroup";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutRoadsMaintenanceGroup = "inGameShortcutRoadsMaintenanceGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransportBusGroup = "inGameShortcutPublicTransportBusGroup";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutPublicTransportTramGroup = "inGameShortcutPublicTransportTramGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransportMetroGroup = "inGameShortcutPublicTransportMetroGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransportTrainGroup = "inGameShortcutPublicTransportTrainGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransportShipGroup = "inGameShortcutPublicTransportShipGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutPublicTransportPlaneGroup = "inGameShortcutPublicTransportPlaneGroup";

	[RebindableKey("Game", 369150u)]
	public static readonly string inGameShortcutPublicTransportTaxiGroup = "inGameShortcutPublicTransportTaxiGroup";

	[RebindableKey("Game", 547502u)]
	public static readonly string inGameShortcutPublicTransportMonorailGroup = "inGameShortcutPublicTransportMonorailGroup";

	[RebindableKey("Game", 547502u)]
	public static readonly string inGameShortcutPublicTransportCableCarGroup = "inGameShortcutPublicTransportCableCarGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutDistrictSpecializationPaintGroup = "inGameShortcutDistrictSpecializationPaintGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutDistrictSpecializationIndustrialGroup = "inGameShortcutDistrictSpecializationIndustrialGroup";

	[RebindableKey("Game", 369150u)]
	public static readonly string inGameShortcutDistrictSpecializationCommercialGroup = "inGameShortcutDistrictSpecializationCommercialGroup";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutWaterServicesGroup = "inGameShortcutWaterServicesGroup";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutWaterHeatingGroup = "inGameShortcutWaterHeatingGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBeautificationParksGroup = "inGameShortcutBeautificationParksGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBeautificationPlazasGroup = "inGameShortcutBeautificationPlazasGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutBeautificationOthersGroup = "inGameShortcutBeautificationOthersGroup";

	[RebindableKey("Game", 369150u)]
	public static readonly string inGameShortcutBeautificationExpansion1Group = "inGameShortcutBeautificationExpansion1Group";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutBeautificationExpansion2Group = "inGameShortcutBeautificationExpansion2Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentLandmarksGroup = "inGameShortcutMonumentLandmarksGroup";

	[RebindableKey("Game", 369150u)]
	public static readonly string inGameShortcutMonumentExpansion1Group = "inGameShortcutMonumentExpansion1Group";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutMonumentExpansion2Group = "inGameShortcutMonumentExpansion2Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory1Group = "inGameShortcutMonumentCategory1Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory2Group = "inGameShortcutMonumentCategory2Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory3Group = "inGameShortcutMonumentCategory3Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory4Group = "inGameShortcutMonumentCategory4Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory5Group = "inGameShortcutMonumentCategory5Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutMonumentCategory6Group = "inGameShortcutMonumentCategory6Group";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscapingToolsGroup = "inGameShortcutLandscapingToolsGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscapingPathsGroup = "inGameShortcutLandscapingPathsGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscapingTreesGroup = "inGameShortcutLandscapingTreesGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscapingRocksGroup = "inGameShortcutLandscapingRocksGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutLandscapingWaterStructuresGroup = "inGameShortcutLandscapingWaterStructuresGroup";

	[RebindableKey("Game", 515191u)]
	public static readonly string inGameShortcutLandscapingDisastersGroup = "inGameShortcutLandscapingDisastersGroup";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutCityInfo = "inGameShortcutCityInfo";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutUnlockingPanel = "inGameShortcutUnlockingPanel";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutAdvisor = "inGameShortcutAdvisor";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneResidentialLow = "inGameShortcutZoneResidentialLow";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneResidentialHigh = "inGameShortcutZoneResidentialHigh";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneCommercialLow = "inGameShortcutZoneCommercialLow";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneCommercialHigh = "inGameShortcutZoneCommercialHigh";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneIndustrial = "inGameShortcutZoneIndustrial";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutZoneOffices = "inGameShortcutZoneOffices";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutDezone = "inGameShortcutDezone";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoviews = "inGameShortcutInfoviews";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoElectricity = "inGameShortcutInfoElectricity";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoWater = "inGameShortcutInfoWater";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoCrime = "inGameShortcutInfoCrime";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoHealth = "inGameShortcutInfoHealth";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoHappiness = "inGameShortcutInfoHappiness";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoPopulation = "inGameShortcutInfoPopulation";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoNoisePollution = "inGameShortcutInfoNoisePollution";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoPublicTransport = "inGameShortcutInfoPublicTransport";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoPollution = "inGameShortcutInfoPollution";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoResources = "inGameShortcutInfoResources";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoLandValue = "inGameShortcutInfoLandValue";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoDistricts = "inGameShortcutInfoDistricts";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoOutsideConnections = "inGameShortcutInfoOutsideConnections";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoTrafficCongestion = "inGameShortcutInfoTrafficCongestion";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoWindSpeed = "inGameShortcutInfoWindSpeed";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoGarbage = "inGameShortcutInfoGarbage";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoLevel = "inGameShortcutInfoLevel";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoFireSafety = "inGameShortcutInfoFireSafety";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoEducation = "inGameShortcutInfoEducation";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutInfoHeating = "inGameShortcutInfoHeating";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutInfoMaintenance = "inGameShortcutInfoMaintenance";

	[RebindableKey("Game", 420610u)]
	public static readonly string inGameShortcutInfoSnow = "inGameShortcutInfoSnow";

	[RebindableKey("Game", 515191u)]
	public static readonly string inGameShortcutInfoEscapeRoutes = "inGameShortcutInfoEscapeRoutes";

	[RebindableKey("Game", 515191u)]
	public static readonly string inGameShortcutInfoRadio = "inGameShortcutInfoRadio";

	[RebindableKey("Game", 515191u)]
	public static readonly string inGameShortcutInfoDestruction = "inGameShortcutInfoDestruction";

	[RebindableKey("Game", 515191u)]
	public static readonly string inGameShortcutInfoDisasterDetection = "inGameShortcutInfoDisasterDetection";

	[RebindableKey("Game")]
	public static readonly string inGameShortcutInfoTrafficRoutes = "inGameShortcutInfoTrafficRoutes";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorSimulationPause = "mapEditorSimulationPause";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorSimulationSpeed1 = "mapEditorSimulationSpeed1";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorSimulationSpeed2 = "mapEditorSimulationSpeed2";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorSimulationSpeed3 = "mapEditorSimulationSpeed3";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorTerrainTool = "mapEditorTerrainTool";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorImportHeightmap = "mapEditorImportHeightmap";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorExportHeightmap = "mapEditorExportHeightmap";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorWaterTool = "mapEditorWaterTool";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorNaturalResourcesTool = "mapEditorNaturalResourcesTool";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorForestTool = "mapEditorForestTool";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorOutsideConnections = "mapEditorOutsideConnections";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorEnvironment = "mapEditorEnvironment";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorSettings = "mapEditorSettings";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorShortcutBulldozer = "mapEditorShortcutBulldozer";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorShortcutBulldozerUnderground = "mapEditorShortcutBulldozerUnderground";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorTerrainUndo = "mapEditorTerrainUndo";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorIncreaseBrushSize = "mapEditorIncreaseBrushSize";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorDecreaseBrushSize = "mapEditorDecreaseBrushSize";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorIncreaseBrushStrength = "mapEditorIncreaseBrushStrength";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorDecreaseBrushStrength = "mapEditorDecreaseBrushStrength";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorRaiseSeaHeight = "mapEditorRaiseSeaHeight";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorLowerSeaHeight = "mapEditorLowerSeaHeight";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorRaiseLevelHeight = "mapEditorRaiseLevelHeight";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorLowerLevelHeight = "mapEditorLowerLevelHeight";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorResetSeaLevel = "mapEditorResetSeaLevel";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorIncreaseCapacity = "mapEditorIncreaseCapacity";

	[RebindableKey("MapEditor")]
	public static readonly string mapEditorDecreaseCapacity = "mapEditorDecreaseCapacity";

	[RebindableKey("Decoration")]
	public static readonly string decorationSimulationPause = "decorationSimulationPause";

	[RebindableKey("Decoration")]
	public static readonly string decorationSimulationSpeed1 = "decorationSimulationSpeed1";

	[RebindableKey("Decoration")]
	public static readonly string decorationSimulationSpeed2 = "decorationSimulationSpeed2";

	[RebindableKey("Decoration")]
	public static readonly string decorationSimulationSpeed3 = "decorationSimulationSpeed3";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoads = "decorationRoads";

	[RebindableKey("Decoration")]
	public static readonly string decorationBeautification = "decorationBeautification";

	[RebindableKey("Decoration")]
	public static readonly string decorationSurface = "decorationSurface";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboards = "decorationPropsBillboards";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboards = "decorationPropsSpecialBillboards";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsIndustrial = "decorationPropsIndustrial";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsParks = "decorationPropsParks";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommon = "decorationPropsCommon";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsResidential = "decorationPropsResidential";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsMarkers = "decorationPropsMarkers";

	[RebindableKey("Decoration")]
	public static readonly string decorationSettings = "decorationSettings";

	[RebindableKey("Decoration")]
	public static readonly string decorationShortcutBulldozer = "decorationShortcutBulldozer";

	[RebindableKey("Decoration")]
	public static readonly string decorationShortcutBulldozerUnderground = "decorationShortcutBulldozerUnderground";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoadsSmallGroup = "decorationRoadsSmallGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoadsMediumGroup = "decorationRoadsMediumGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoadsLargeGroup = "decorationRoadsLargeGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoadsHighwayGroup = "decorationRoadsHighwayGroup";

	[RebindableKey("Decoration", 420610u)]
	public static readonly string decorationPublicTransportTramGroup = "decorationPublicTransportTramGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPublicTransportTrainGroup = "decorationPublicTransportTrainGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationRoadsIntersectionGroup = "decorationRoadsIntersectionGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationBeautificationPathsGroup = "decorationBeautificationPathsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationBeautificationPropsGroup = "decorationBeautificationPropsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboardsLogoGroup = "decorationPropsBillboardsLogoGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboardsSmallBillboardGroup = "decorationPropsBillboardsSmallBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboardsMediumBillboardGroup = "decorationPropsBillboardsMediumBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboardsLargeBillboardGroup = "decorationPropsBillboardsLargeBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsBillboardsRandomLogoGroup = "decorationPropsBillboardsRandomLogoGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboardsRandomSmallBillboardGroup = "decorationPropsSpecialBillboardsRandomSmallBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboardsRandomMediumBillboardGroup = "decorationPropsSpecialBillboardsRandomMediumBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboardsRandomLargeBillboardGroup = "decorationPropsSpecialBillboardsRandomLargeBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboards3DBillboardGroup = "decorationPropsSpecialBillboards3DBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsSpecialBillboardsAnimatedBillboardGroup = "decorationPropsSpecialBillboardsAnimatedBillboardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsIndustrialContainersGroup = "decorationPropsIndustrialContainersGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsIndustrialConstructionMaterialsGroup = "decorationPropsIndustrialConstructionMaterialsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsIndustrialStructuresGroup = "decorationPropsIndustrialStructuresGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsParksPlaygroundsGroup = "decorationPropsParksPlaygroundsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsParksFlowersAndPlantsGroup = "decorationPropsParksFlowersAndPlantsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsParksParkEquipmentGroup = "decorationPropsParksParkEquipmentGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsParksFountainsGroup = "decorationPropsParksFountainsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommonAccessoriesGroup = "decorationPropsCommonAccessoriesGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommonGarbageGroup = "decorationPropsCommonGarbageGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommonCommunicationsGroup = "decorationPropsCommonCommunicationsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommonStreetsGroup = "decorationPropsCommonStreetsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsCommonLightsGroup = "decorationPropsCommonLightsGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsResidentialHomeYardGroup = "decorationPropsResidentialHomeYardGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsResidentialGroundTilesGroup = "decorationPropsResidentialGroundTilesGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsResidentialRooftopAccessGroup = "decorationPropsResidentialRooftopAccessGroup";

	[RebindableKey("Decoration")]
	public static readonly string decorationPropsResidentialRandomRooftopAccessGroup = "decorationPropsResidentialRandomRooftopAccessGroup";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorSimulationPause = "themeEditorSimulationPause";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorSimulationSpeed1 = "themeEditorSimulationSpeed1";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorSimulationSpeed2 = "themeEditorSimulationSpeed2";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorSimulationSpeed3 = "themeEditorSimulationSpeed3";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorTerrainProperties = "themeEditorTerrainProperties";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorWaterProperties = "themeEditorWaterProperties";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorAtmosphereProperties = "themeEditorAtmosphereProperties";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorStructuresProperties = "themeEditorStructuresProperties";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorWorldProperties = "themeEditorWorldProperties";

	[RebindableKey("ThemeEditor", 515191u)]
	public static readonly string themeEditorDisasterProperties = "themeEditorDisasterProperties";

	[RebindableKey("ThemeEditor")]
	public static readonly string themeEditorSettings = "themeEditorSettings";

	[RebindableKey("ScenarioEditor")]
	public static readonly string scenarioEditorBulldozer = "scenarioEditorBulldozer";

	[RebindableKey("ScenarioEditor")]
	public static readonly string scenarioEditorBulldozerUnderground = "scenarioEditorBulldozerUnderground";

	[RebindableKey("ScenarioEditor")]
	public static readonly string scenarioEditorTerrainTool = "scenarioEditorTerrainTool";

	[RebindableKey("ScenarioEditor", 515191u)]
	public static readonly string scenarioEditorPlaceDisasters = "scenarioEditorPlaceDisasters";

	[RebindableKey("ScenarioEditor")]
	public static readonly string scenarioEditorTriggerPanel = "scenarioEditorTriggerPanel";

	[RebindableKey("ScenarioEditor")]
	public static readonly string scenarioEditorSettings = "scenarioEditorSettings";

	public static readonly string pdx_acc_nm = "pdx_acc_nm";

	public static readonly string pdx_acc_ps = "pdx_acc_ps";

	public static readonly string pdx_acc_rm = "pdx_acc_rm";

	public static readonly string pdxLoginUsed = "pdxLoginUsed";

	public static readonly string gameLogVerbose = "gameLogVerbose";

	public static readonly string gameLogChannels = "gameLogChannels";

	public static readonly string frameworkLogVerbose = "frameworkLogVerbose";

	public static readonly string frameworkLogChannels = "frameworkLogChannels";
}
