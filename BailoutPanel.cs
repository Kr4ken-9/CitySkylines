﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class BailoutPanel : UICustomControl
{
	private void Awake()
	{
		this.m_InfoLabel = base.Find<UILabel>("InfoLabel");
	}

	public void SetAmount(int amount)
	{
		if (this.m_InfoLabel != null)
		{
			this.m_InfoLabel.set_text(StringUtils.SafeFormat(Locale.Get("BAILOUT_DETAILS"), new object[]
			{
				amount.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()),
				Locale.Get("MONEY_CURRENCY")
			}));
			this.m_InfoLabel.get_parent().FitChildren();
		}
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Accept");
			uIButton.Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = true;
				});
			}
			Cursor.set_visible(true);
			Cursor.set_lockState(0);
			base.get_component().Focus();
		}
		else if (Singleton<SimulationManager>.get_exists())
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
			});
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 13)
			{
				this.OnAccept();
				p.Use();
			}
			else if (p.get_keycode() == 27)
			{
				this.OnReject();
				p.Use();
			}
		}
	}

	public void OnAccept()
	{
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	public void OnReject()
	{
		UIView.get_library().Hide(base.GetType().Name, 0);
	}

	private UILabel m_InfoLabel;
}
