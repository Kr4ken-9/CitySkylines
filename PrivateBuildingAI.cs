﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PrivateBuildingAI : CommonBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.NoisePollution:
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			int num;
			int num2;
			this.GetPollutionRates((!this.ShowConsumption(buildingID, ref data)) ? 0 : 40, cityPlanningPolicies, out num, out num2);
			if (num2 != 0)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)num2 * 0.25f);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		case InfoManager.InfoMode.Transport:
			IL_14:
			if (infoMode != InfoManager.InfoMode.Radio)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (this.ShowConsumption(buildingID, ref data))
			{
				int num3;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.RadioCoverage, data.m_position, out num3);
				return CommonBuildingAI.GetRadioCoverageColor((float)num3);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.Pollution:
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district2 = instance2.GetDistrict(data.m_position);
			DistrictPolicies.CityPlanning cityPlanningPolicies2 = instance2.m_districts.m_buffer[(int)district2].m_cityPlanningPolicies;
			int num4;
			int num5;
			this.GetPollutionRates((!this.ShowConsumption(buildingID, ref data)) ? 0 : 40, cityPlanningPolicies2, out num4, out num5);
			if (num4 != 0)
			{
				return ColorUtils.LinearLerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor, Mathf.Clamp01((float)num4 * 0.01f));
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		}
		goto IL_14;
	}

	protected override bool ShowConsumption(ushort buildingID, ref Building data)
	{
		return (data.m_flags & (Building.Flags.Completed | Building.Flags.Abandoned | Building.Flags.Collapsed)) == Building.Flags.Completed;
	}

	public override string GetLocalizedStatus(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Abandoned) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_ABANDONED");
		}
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			if (data.GetLastFrameData().m_fireDamage == 255)
			{
				return Locale.Get("BUILDING_STATUS_BURNED");
			}
			return Locale.Get("BUILDING_STATUS_COLLAPSED");
		}
		else
		{
			if ((data.m_flags & Building.Flags.Upgrading) != Building.Flags.None)
			{
				return Locale.Get("BUILDING_STATUS_UPGRADING");
			}
			if ((data.m_flags & Building.Flags.Completed) == Building.Flags.None)
			{
				return Locale.Get("BUILDING_STATUS_UNDER_CONSTRUCTION");
			}
			if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
			{
				return this.GetLocalizedStatusInactive(buildingID, ref data);
			}
			return this.GetLocalizedStatusActive(buildingID, ref data);
		}
	}

	protected virtual string GetLocalizedStatusInactive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EVACUATED");
		}
		return Locale.Get("BUILDING_STATUS_NOT_OPERATING");
	}

	protected virtual string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		return Locale.Get("BUILDING_STATUS_DEFAULT");
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int num;
		int num2;
		int num3;
		int num4;
		this.CalculateWorkplaceCount(new Randomizer((int)buildingID), data.Width, data.Length, out num, out num2, out num3, out num4);
		int workCount = num + num2 + num3 + num4;
		int homeCount = this.CalculateHomeCount(new Randomizer((int)buildingID), data.Width, data.Length);
		int visitCount = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), data.Width, data.Length);
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, homeCount, workCount, visitCount, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int num;
		int num2;
		int num3;
		int num4;
		this.CalculateWorkplaceCount(new Randomizer((int)buildingID), data.Width, data.Length, out num, out num2, out num3, out num4);
		int workCount = num + num2 + num3 + num4;
		int homeCount = this.CalculateHomeCount(new Randomizer((int)buildingID), data.Width, data.Length);
		int visitCount = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), data.Width, data.Length);
		base.EnsureCitizenUnits(buildingID, ref data, homeCount, workCount, visitCount, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override int GetConstructionTime()
	{
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			return 0;
		}
		return this.m_constructionTime;
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10u) == 0)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			ushort style = instance.m_districts.m_buffer[(int)district].m_Style;
			if (style > 0 && (int)(style - 1) < instance.m_Styles.Length)
			{
				DistrictStyle districtStyle = instance.m_Styles[(int)(style - 1)];
				if (districtStyle != null && this.m_info.m_class != null && districtStyle.AffectsService(this.m_info.GetService(), this.m_info.GetSubService(), this.m_info.m_class.m_level) && !districtStyle.Contains(this.m_info) && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == Singleton<SimulationManager>.get_instance().m_currentBuildIndex)
				{
					buildingData.m_flags |= Building.Flags.Demolishing;
					Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
				}
			}
		}
		if ((buildingData.m_flags & Building.Flags.ZonesUpdated) != Building.Flags.None && (buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None))
		{
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			if (buildingData.m_fireIntensity == 0 && instance2.m_randomizer.Int32(10u) == 0 && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance2.m_currentBuildIndex)
			{
				buildingData.m_flags &= ~Building.Flags.ZonesUpdated;
				if (!buildingData.CheckZoning(this.m_info.m_class.GetZone(), this.m_info.m_class.GetSecondaryZone(), true))
				{
					buildingData.m_flags |= Building.Flags.Demolishing;
					PrivateBuildingAI.CheckNearbyBuildingZones(buildingData.m_position);
					instance2.m_currentBuildIndex += 1u;
					return;
				}
			}
		}
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			if (buildingData.m_fireIntensity != 0)
			{
				DistrictManager instance3 = Singleton<DistrictManager>.get_instance();
				byte district2 = instance3.GetDistrict(buildingData.m_position);
				DistrictPolicies.Services servicePolicies = instance3.m_districts.m_buffer[(int)district2].m_servicePolicies;
				base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
			}
			else if ((buildingData.m_majorProblemTimer == 255 || (buildingData.m_flags & Building.Flags.Abandoned) == Building.Flags.None) && (buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None))
			{
				SimulationManager instance4 = Singleton<SimulationManager>.get_instance();
				ZoneManager instance5 = Singleton<ZoneManager>.get_instance();
				int num;
				switch (this.m_info.m_class.m_service)
				{
				case ItemClass.Service.Residential:
					num = instance5.m_actualResidentialDemand;
					goto IL_336;
				case ItemClass.Service.Commercial:
					num = instance5.m_actualCommercialDemand;
					goto IL_336;
				case ItemClass.Service.Industrial:
					num = instance5.m_actualWorkplaceDemand;
					goto IL_336;
				case ItemClass.Service.Office:
					num = instance5.m_actualWorkplaceDemand;
					goto IL_336;
				}
				num = 0;
				IL_336:
				if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
				{
					DistrictManager instance6 = Singleton<DistrictManager>.get_instance();
					byte district3 = instance6.GetDistrict(buildingData.m_position);
					DistrictPolicies.CityPlanning cityPlanningPolicies = instance6.m_districts.m_buffer[(int)district3].m_cityPlanningPolicies;
					if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.NoRebuild) != DistrictPolicies.CityPlanning.None)
					{
						District[] expr_397_cp_0 = instance6.m_districts.m_buffer;
						byte expr_397_cp_1 = district3;
						expr_397_cp_0[(int)expr_397_cp_1].m_cityPlanningPoliciesEffect = (expr_397_cp_0[(int)expr_397_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.NoRebuild);
						num = 0;
					}
				}
				if (instance4.m_randomizer.Int32(100u) < num && instance5.m_lastBuildIndex == instance4.m_currentBuildIndex)
				{
					float num2 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(buildingData.m_position));
					if (num2 <= buildingData.m_position.y && buildingData.CheckZoning(this.m_info.m_class.GetZone(), this.m_info.m_class.GetSecondaryZone(), false))
					{
						ItemClass.SubService subService = this.m_info.m_class.m_subService;
						ItemClass.Level level = ItemClass.Level.Level1;
						int width = buildingData.Width;
						int num3 = buildingData.Length;
						if (this.m_info.m_class.m_service == ItemClass.Service.Industrial)
						{
							ZoneBlock.GetIndustryType(buildingData.m_position, out subService, out level);
						}
						else if (this.m_info.m_class.m_service == ItemClass.Service.Commercial)
						{
							ZoneBlock.GetCommercialType(buildingData.m_position, this.m_info.m_class.GetZone(), width, num3, out subService, out level);
						}
						else if (this.m_info.m_class.m_service == ItemClass.Service.Residential)
						{
							ZoneBlock.GetResidentialType(buildingData.m_position, this.m_info.m_class.GetZone(), width, num3, out subService, out level);
						}
						else if (this.m_info.m_class.m_service == ItemClass.Service.Office)
						{
							ZoneBlock.GetOfficeType(buildingData.m_position, this.m_info.m_class.GetZone(), width, num3, out subService, out level);
						}
						DistrictManager instance7 = Singleton<DistrictManager>.get_instance();
						byte district4 = instance7.GetDistrict(buildingData.m_position);
						ushort style2 = instance7.m_districts.m_buffer[(int)district4].m_Style;
						BuildingInfo randomBuildingInfo = Singleton<BuildingManager>.get_instance().GetRandomBuildingInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, subService, level, width, num3, this.m_info.m_zoningMode, (int)style2);
						if (randomBuildingInfo != null)
						{
							buildingData.m_flags |= Building.Flags.Demolishing;
							float num4 = buildingData.m_angle + 1.57079637f;
							if (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft && randomBuildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerRight)
							{
								num4 -= 1.57079637f;
								num3 = width;
							}
							else if (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerRight && randomBuildingInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft)
							{
								num4 += 1.57079637f;
								num3 = width;
							}
							ushort num5;
							if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num5, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomBuildingInfo, buildingData.m_position, buildingData.m_angle, num3, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
							{
								Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
								switch (this.m_info.m_class.m_service)
								{
								case ItemClass.Service.Residential:
									instance5.m_actualResidentialDemand = Mathf.Max(0, instance5.m_actualResidentialDemand - 5);
									break;
								case ItemClass.Service.Commercial:
									instance5.m_actualCommercialDemand = Mathf.Max(0, instance5.m_actualCommercialDemand - 5);
									break;
								case ItemClass.Service.Industrial:
									instance5.m_actualWorkplaceDemand = Mathf.Max(0, instance5.m_actualWorkplaceDemand - 5);
									break;
								case ItemClass.Service.Office:
									instance5.m_actualWorkplaceDemand = Mathf.Max(0, instance5.m_actualWorkplaceDemand - 5);
									break;
								}
							}
							instance4.m_currentBuildIndex += 1u;
						}
					}
				}
			}
		}
	}

	private static void CheckNearbyBuildingZones(Vector3 position)
	{
		int num = Mathf.Max((int)((position.x - 35f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - 35f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + 35f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + 35f) / 64f + 135f), 269);
		Array16<Building> buildings = Singleton<BuildingManager>.get_instance().m_buildings;
		ushort[] buildingGrid = Singleton<BuildingManager>.get_instance().m_buildingGrid;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					ushort nextGridBuilding = buildings.m_buffer[(int)num5].m_nextGridBuilding;
					Building.Flags flags = buildings.m_buffer[(int)num5].m_flags;
					if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Demolishing | Building.Flags.ZonesUpdated | Building.Flags.Collapsed)) == (Building.Flags.Created | Building.Flags.ZonesUpdated))
					{
						BuildingInfo info = buildings.m_buffer[(int)num5].Info;
						if (info != null && info.m_placementStyle == ItemClass.Placement.Automatic)
						{
							ItemClass.Zone zone = info.m_class.GetZone();
							ItemClass.Zone secondaryZone = info.m_class.GetSecondaryZone();
							if (zone != ItemClass.Zone.None && VectorUtils.LengthSqrXZ(buildings.m_buffer[(int)num5].m_position - position) <= 1225f)
							{
								Building[] expr_18C_cp_0 = buildings.m_buffer;
								ushort expr_18C_cp_1 = num5;
								expr_18C_cp_0[(int)expr_18C_cp_1].m_flags = (expr_18C_cp_0[(int)expr_18C_cp_1].m_flags & ~Building.Flags.ZonesUpdated);
								if (!buildings.m_buffer[(int)num5].CheckZoning(zone, secondaryZone, true))
								{
									Singleton<BuildingManager>.get_instance().ReleaseBuilding(num5);
								}
							}
						}
					}
					num5 = nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
		{
			if (buildingData.m_fireIntensity == 0)
			{
				buildingData.m_majorProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_majorProblemTimer + 1));
				if (buildingData.m_majorProblemTimer >= 64 && !Singleton<BuildingManager>.get_instance().m_abandonmentDisabled)
				{
					if ((buildingData.m_flags & Building.Flags.Flooded) != Building.Flags.None)
					{
						InstanceID id = default(InstanceID);
						id.Building = buildingID;
						Singleton<InstanceManager>.get_instance().SetGroup(id, null);
						buildingData.m_flags &= ~Building.Flags.Flooded;
					}
					buildingData.m_majorProblemTimer = 192;
					buildingData.m_flags &= ~Building.Flags.Active;
					buildingData.m_flags |= Building.Flags.Abandoned;
					buildingData.m_problems = (Notification.Problem.FatalProblem | (buildingData.m_problems & ~Notification.Problem.MajorProblem));
					base.RemovePeople(buildingID, ref buildingData, 100);
					this.BuildingDeactivated(buildingID, ref buildingData);
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
				}
			}
		}
		else
		{
			buildingData.m_majorProblemTimer = 0;
		}
	}

	protected int HandleWorkers(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount)
	{
		int num = 0;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		int num2;
		int num3;
		int num4;
		int num5;
		this.CalculateWorkplaceCount(new Randomizer((int)buildingID), buildingData.Width, buildingData.Length, out num2, out num3, out num4, out num5);
		workPlaceCount = num2 + num3 + num4 + num5;
		if (buildingData.m_fireIntensity == 0)
		{
			base.HandleWorkPlaces(buildingID, ref buildingData, num2, num3, num4, num5, ref behaviour, aliveWorkerCount, totalWorkerCount);
			if (aliveWorkerCount != 0 && workPlaceCount != 0)
			{
				int num6 = (behaviour.m_efficiencyAccumulation + aliveWorkerCount - 1) / aliveWorkerCount;
				num = 2 * num6 - 200 * num6 / ((100 * aliveWorkerCount + workPlaceCount - 1) / workPlaceCount + 100);
			}
		}
		Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoWorkers | Notification.Problem.NoEducatedWorkers);
		int num7 = (num5 * 300 + num4 * 200 + num3 * 100) / (workPlaceCount + 1);
		int num8 = (behaviour.m_educated3Count * 300 + behaviour.m_educated2Count * 200 + behaviour.m_educated1Count * 100) / (aliveWorkerCount + 1);
		if (aliveWorkerCount < workPlaceCount >> 1)
		{
			buildingData.m_workerProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_workerProblemTimer + 1));
			if (buildingData.m_workerProblemTimer >= 128)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.NoWorkers | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_workerProblemTimer >= 64)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.NoWorkers);
			}
		}
		else if (num8 < num7 - 50)
		{
			buildingData.m_workerProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_workerProblemTimer + 1));
			if (buildingData.m_workerProblemTimer >= 128)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.NoEducatedWorkers | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_workerProblemTimer >= 64)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.NoEducatedWorkers);
			}
		}
		else
		{
			buildingData.m_workerProblemTimer = 0;
		}
		buildingData.m_problems = problem;
		return Mathf.Max(1, num);
	}

	public override BuildingInfo GetUpgradeInfo(ushort buildingID, ref Building data)
	{
		if (this.m_info.m_class.m_level == ItemClass.Level.Level5)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		for (int i = 0; i <= (int)this.m_info.m_class.m_level; i++)
		{
			randomizer.Int32(1000u);
		}
		ItemClass.Level level = this.m_info.m_class.m_level + 1;
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(data.m_position);
		ushort style = instance.m_districts.m_buffer[(int)district].m_Style;
		return Singleton<BuildingManager>.get_instance().GetRandomBuildingInfo(ref randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, level, data.Width, data.Length, this.m_info.m_zoningMode, (int)style);
	}

	public override void BuildingUpgraded(ushort buildingID, ref Building data)
	{
		int num;
		int num2;
		int num3;
		int num4;
		this.CalculateWorkplaceCount(new Randomizer((int)buildingID), data.Width, data.Length, out num, out num2, out num3, out num4);
		int workCount = num + num2 + num3 + num4;
		int homeCount = this.CalculateHomeCount(new Randomizer((int)buildingID), data.Width, data.Length);
		int visitCount = this.CalculateVisitplaceCount(new Randomizer((int)buildingID), data.Width, data.Length);
		base.EnsureCitizenUnits(buildingID, ref data, homeCount, workCount, visitCount, 0);
	}

	public void StartUpgrading(ushort buildingID, ref Building buildingData)
	{
		buildingData.m_frame0.m_constructState = 0;
		buildingData.m_frame1.m_constructState = 0;
		buildingData.m_frame2.m_constructState = 0;
		buildingData.m_frame3.m_constructState = 0;
		Building.Flags flags = buildingData.m_flags;
		flags |= Building.Flags.Upgrading;
		flags &= ~Building.Flags.Completed;
		flags &= ~Building.Flags.LevelUpEducation;
		flags &= ~Building.Flags.LevelUpLandValue;
		buildingData.m_flags = flags;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		instance.UpdateBuildingRenderer(buildingID, true);
		EffectInfo levelupEffect = instance.m_properties.m_levelupEffect;
		if (levelupEffect != null)
		{
			InstanceID instance2 = default(InstanceID);
			instance2.Building = buildingID;
			Vector3 vector;
			Quaternion quaternion;
			buildingData.CalculateMeshPosition(out vector, out quaternion);
			Matrix4x4 matrix = Matrix4x4.TRS(vector, quaternion, Vector3.get_one());
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(matrix, this.m_info.m_lodMeshData);
			Singleton<EffectManager>.get_instance().DispatchEffect(levelupEffect, instance2, spawnArea, Vector3.get_zero(), 0f, 1f, instance.m_audioGroup);
		}
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LevelUp, position, 1f);
		Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
	}

	public override void GetWidthRange(out int minWidth, out int maxWidth)
	{
		minWidth = 1;
		maxWidth = 4;
	}

	public override void GetLengthRange(out int minLength, out int maxLength)
	{
		minLength = 1;
		maxLength = 4;
	}

	public override void GetDecorationArea(out int width, out int length, out float offset)
	{
		width = this.m_info.m_cellWidth;
		length = ((this.m_info.m_zoningMode != BuildingInfo.ZoningMode.Straight) ? this.m_info.m_cellLength : 4);
		offset = (float)(length - this.m_info.m_cellLength) * 4f;
		if (!this.m_info.m_expandFrontYard)
		{
			offset = -offset;
		}
	}

	public override void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		negX = (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerRight);
		posX = (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft);
		negZ = false;
		posZ = true;
	}

	public override ItemClass.CollisionType GetCollisionType()
	{
		return ItemClass.CollisionType.Zoned;
	}

	public virtual int CalculateHomeCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public virtual int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public virtual void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		level0 = 0;
		level1 = 0;
		level2 = 0;
		level3 = 0;
	}

	public virtual int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		return 0;
	}

	public virtual void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
	}

	public virtual void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		groundPollution = 0;
		noisePollution = 0;
	}

	[CustomizableProperty("Construction Time")]
	public int m_constructionTime = 30;
}
