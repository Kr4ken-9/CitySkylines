﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class CitizenAI : PrefabAI
{
	public virtual void InitializeAI()
	{
	}

	public virtual void ReleaseAI()
	{
	}

	public virtual Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			if (infoMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
			{
				InstanceID empty = InstanceID.Empty;
				empty.CitizenInstance = instanceID;
				if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
				{
					uint citizen = data.m_citizen;
					if (citizen != 0u)
					{
						ushort vehicle = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
						if (vehicle != 0)
						{
							VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].Info;
							if (info != null && info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
							{
								return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[1];
							}
							return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
						}
					}
					return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[0];
				}
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if ((data.m_flags & CitizenInstance.Flags.CustomColor) != CitizenInstance.Flags.None)
		{
			return data.m_color;
		}
		Randomizer randomizer;
		randomizer..ctor((int)instanceID);
		switch (randomizer.Int32(4u))
		{
		case 0:
			return this.m_info.m_color0;
		case 1:
			return this.m_info.m_color1;
		case 2:
			return this.m_info.m_color2;
		case 3:
			return this.m_info.m_color3;
		default:
			return this.m_info.m_color0;
		}
	}

	public virtual void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort instanceID, ref CitizenInstance data, Vector3 position, Quaternion rotation, Vector3 velocity, Color color, bool underground)
	{
		this.m_info.SetRenderParameters(position, rotation, velocity, color, 0, underground);
	}

	public virtual string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return null;
	}

	public virtual string GetLocalizedStatus(uint citizenID, ref Citizen data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return null;
	}

	public virtual string GetDebugString(ushort instanceID, ref CitizenInstance data)
	{
		return null;
	}

	public virtual void SimulationStep(uint citizenID, ref Citizen data)
	{
	}

	public virtual void SimulationStep(uint homeID, ref CitizenUnit data)
	{
	}

	public virtual void StartTransfer(uint citizenID, ref Citizen data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
	}

	public virtual void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
	}

	public virtual void ReleaseInstance(ushort instanceID, ref CitizenInstance data)
	{
		this.SetSource(instanceID, ref data, 0);
		this.SetTarget(instanceID, ref data, 0);
	}

	public virtual void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
		{
			CitizenInstance.Frame lastFrameData = data.GetLastFrameData();
			int num = Mathf.Clamp((int)(lastFrameData.m_position.x / 8f + 1080f), 0, 2159);
			int num2 = Mathf.Clamp((int)(lastFrameData.m_position.z / 8f + 1080f), 0, 2159);
			bool lodPhysics = Vector3.SqrMagnitude(physicsLodRefPos - lastFrameData.m_position) >= 62500f;
			this.SimulationStep(instanceID, ref data, ref lastFrameData, lodPhysics);
			int num3 = Mathf.Clamp((int)(lastFrameData.m_position.x / 8f + 1080f), 0, 2159);
			int num4 = Mathf.Clamp((int)(lastFrameData.m_position.z / 8f + 1080f), 0, 2159);
			if ((num3 != num || num4 != num2) && (data.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
			{
				Singleton<CitizenManager>.get_instance().RemoveFromGrid(instanceID, ref data, num, num2);
				Singleton<CitizenManager>.get_instance().AddToGrid(instanceID, ref data, num3, num4);
			}
			if (data.m_flags != CitizenInstance.Flags.None)
			{
				data.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, lastFrameData);
			}
		}
	}

	public virtual void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
	}

	public virtual void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
	}

	public virtual void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
	}

	public virtual void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
	}

	public virtual void JoinTarget(ushort instanceID, ref CitizenInstance data, ushort otherInstance)
	{
	}

	public virtual void BuildingRelocated(ushort instanceID, ref CitizenInstance data, ushort building)
	{
	}

	public virtual bool TransportArriveAtSource(ushort instanceID, ref CitizenInstance citizenData, Vector3 currentPos, Vector3 nextTarget)
	{
		return false;
	}

	public virtual bool TransportArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData, Vector3 currentPos, Vector3 nextTarget, ref TransportPassengerData passengerData, bool forceUnload)
	{
		return true;
	}

	public virtual bool SetCurrentVehicle(ushort instanceID, ref CitizenInstance citizenData, ushort vehicleID, uint unitID, Vector3 position)
	{
		return false;
	}

	public virtual bool AddWind(ushort instanceID, ref CitizenInstance citizenData, Vector3 wind, InstanceManager.Group group)
	{
		return false;
	}

	protected Vector4 GetPathTargetPosition(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, float minSqrDistance)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = citizenData.m_targetPos;
		float num = VectorUtils.LengthSqrXZ(citizenData.m_targetPos - frameData.m_position);
		if (num >= minSqrDistance)
		{
			return vector;
		}
		if (citizenData.m_pathPositionIndex == 255)
		{
			citizenData.m_pathPositionIndex = 0;
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].CalculatePathPositionOffset(citizenData.m_pathPositionIndex >> 1, vector, out citizenData.m_lastPathOffset))
			{
				this.InvalidPath(instanceID, ref citizenData);
				return vector;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out position))
		{
			this.InvalidPath(instanceID, ref citizenData);
			return vector;
		}
		if ((citizenData.m_pathPositionIndex & 1) == 0)
		{
			int num2 = (citizenData.m_pathPositionIndex >> 1) + 1;
			uint num3 = citizenData.m_path;
			if (num2 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].m_positionCount)
			{
				num2 = 0;
				num3 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].m_nextPathUnit;
			}
			PathUnit.Position position2;
			if (num3 != 0u && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].GetPosition(num2, out position2) && position2.m_segment == position.m_segment)
			{
				NetInfo info = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
				if (info.m_lanes.Length > (int)position.m_lane && info.m_lanes.Length > (int)position2.m_lane)
				{
					float position3 = info.m_lanes[(int)position.m_lane].m_position;
					float position4 = info.m_lanes[(int)position2.m_lane].m_position;
					if (Mathf.Abs(position3 - position4) < 4f)
					{
						citizenData.m_pathPositionIndex = (byte)(num2 << 1);
						position = position2;
						if (num3 != citizenData.m_path)
						{
							Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref citizenData.m_path);
						}
					}
				}
			}
		}
		uint num4 = PathManager.GetLaneID(position);
		Randomizer randomizer;
		randomizer..ctor((int)instanceID);
		float num5 = (float)randomizer.Int32(-500, 500) * 0.001f;
		NetInfo info2;
		float num6;
		int num11;
		uint num12;
		PathUnit.Position position5;
		NetInfo info3;
		Bezier3 bezier;
		Vector3 vector5;
		Segment3 segment;
		Vector3 vector11;
		while (true)
		{
			info2 = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
			if (info2.m_lanes.Length <= (int)position.m_lane)
			{
				break;
			}
			float width = info2.m_lanes[(int)position.m_lane].m_width;
			num6 = Mathf.Max(0f, width - 1f) * num5;
			float num7 = (!info2.m_lanes[(int)position.m_lane].m_useTerrainHeight && (citizenData.m_flags & CitizenInstance.Flags.OnPath) != CitizenInstance.Flags.None) ? 0f : 1f;
			if ((citizenData.m_pathPositionIndex & 1) == 0)
			{
				bool flag = true;
				int num8 = (int)(position.m_offset - citizenData.m_lastPathOffset);
				while (num8 != 0)
				{
					if (flag)
					{
						flag = false;
					}
					else
					{
						float num9 = Mathf.Sqrt(minSqrDistance) - VectorUtils.LengthXZ(vector - frameData.m_position);
						int num10;
						if (num9 < 0f)
						{
							num10 = 4;
						}
						else
						{
							num10 = 4 + Mathf.CeilToInt(num9 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num4)].m_length + 1f));
						}
						if (num8 < 0)
						{
							citizenData.m_lastPathOffset = (byte)Mathf.Max((int)citizenData.m_lastPathOffset - num10, (int)position.m_offset);
						}
						else if (num8 > 0)
						{
							citizenData.m_lastPathOffset = (byte)Mathf.Min((int)citizenData.m_lastPathOffset + num10, (int)position.m_offset);
						}
					}
					Vector3 vector2;
					Vector3 vector3;
					instance2.m_lanes.m_buffer[(int)((UIntPtr)num4)].CalculatePositionAndDirection((float)citizenData.m_lastPathOffset * 0.003921569f, out vector2, out vector3);
					vector = vector2;
					vector.w = num7;
					Vector3 vector4 = Vector3.Cross(Vector3.get_up(), vector3).get_normalized() * num6;
					if (num8 > 0)
					{
						vector.x += vector4.x;
						vector.z += vector4.z;
					}
					else
					{
						vector.x -= vector4.x;
						vector.z -= vector4.z;
					}
					num = VectorUtils.LengthSqrXZ(vector - frameData.m_position);
					if (num >= minSqrDistance)
					{
						goto Block_22;
					}
					num8 = (int)(position.m_offset - citizenData.m_lastPathOffset);
					if ((citizenData.m_flags & CitizenInstance.Flags.OnPath) == CitizenInstance.Flags.None)
					{
						citizenData.m_flags |= CitizenInstance.Flags.OnPath;
						if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) == CitizenInstance.Flags.None && (instance2.m_segments.m_buffer[(int)position.m_segment].m_flags & NetSegment.Flags.BikeBan) == NetSegment.Flags.None)
						{
							this.SpawnBicycle(instanceID, ref citizenData, position);
						}
					}
				}
				citizenData.m_pathPositionIndex += 1;
				citizenData.m_lastPathOffset = 0;
			}
			num11 = (citizenData.m_pathPositionIndex >> 1) + 1;
			num12 = citizenData.m_path;
			if (num11 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].m_positionCount)
			{
				num11 = 0;
				num12 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].m_nextPathUnit;
				if (num12 == 0u)
				{
					goto Block_28;
				}
			}
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num12)].GetPosition(num11, out position5))
			{
				goto Block_29;
			}
			info3 = instance2.m_segments.m_buffer[(int)position5.m_segment].Info;
			if (info3.m_lanes.Length <= (int)position5.m_lane)
			{
				goto Block_30;
			}
			int num13 = num11 + 1;
			uint num14 = num12;
			uint num15 = 0u;
			if (num13 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num12)].m_positionCount)
			{
				num13 = 0;
				num14 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num12)].m_nextPathUnit;
			}
			PathUnit.Position position6;
			if (num14 != 0u && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num14)].GetPosition(num13, out position6) && position6.m_segment == position5.m_segment && info3.m_lanes.Length > (int)position6.m_lane)
			{
				float position7 = info3.m_lanes[(int)position5.m_lane].m_position;
				float position8 = info3.m_lanes[(int)position6.m_lane].m_position;
				if (Mathf.Abs(position7 - position8) < 4f)
				{
					num15 = PathManager.GetLaneID(position5);
					num11 = num13;
					position5 = position6;
					num12 = num14;
				}
			}
			NetInfo.LaneType laneType = info3.m_lanes[(int)position5.m_lane].m_laneType;
			uint laneID = PathManager.GetLaneID(position5);
			float num16 = (!info3.m_lanes[(int)position5.m_lane].m_useTerrainHeight) ? 0f : 1f;
			ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			ushort endNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			ushort startNode2 = instance2.m_segments.m_buffer[(int)position5.m_segment].m_startNode;
			ushort endNode2 = instance2.m_segments.m_buffer[(int)position5.m_segment].m_endNode;
			if (startNode2 != startNode && startNode2 != endNode && endNode2 != startNode && endNode2 != endNode)
			{
				uint lane = instance2.m_nodes.m_buffer[(int)startNode].m_lane;
				uint lane2 = instance2.m_nodes.m_buffer[(int)startNode2].m_lane;
				uint lane3 = instance2.m_nodes.m_buffer[(int)endNode].m_lane;
				uint lane4 = instance2.m_nodes.m_buffer[(int)endNode2].m_lane;
				if (lane != laneID && lane2 != num4 && lane3 != laneID && lane4 != num4 && (num15 == 0u || (lane != num15 && lane3 != num15)))
				{
					goto IL_8A4;
				}
				if (((instance2.m_nodes.m_buffer[(int)startNode].m_flags | instance2.m_nodes.m_buffer[(int)endNode].m_flags) & NetNode.Flags.Disabled) == NetNode.Flags.None && ((instance2.m_nodes.m_buffer[(int)startNode2].m_flags | instance2.m_nodes.m_buffer[(int)endNode2].m_flags) & NetNode.Flags.Disabled) != NetNode.Flags.None)
				{
					goto Block_49;
				}
			}
			if ((byte)(laneType & (NetInfo.LaneType.PublicTransport | NetInfo.LaneType.EvacuationTransport)) != 0)
			{
				goto Block_50;
			}
			if ((byte)(laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
			{
				if ((info3.m_lanes[(int)position5.m_lane].m_vehicleType & VehicleInfo.VehicleType.Bicycle) == VehicleInfo.VehicleType.None)
				{
					goto IL_A69;
				}
				if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) == CitizenInstance.Flags.None && !this.SpawnBicycle(instanceID, ref citizenData, position5))
				{
					goto Block_57;
				}
				citizenData.m_flags |= CitizenInstance.Flags.OnBikeLane;
			}
			else
			{
				if (laneType != NetInfo.LaneType.Pedestrian)
				{
					goto Block_60;
				}
				citizenData.m_flags &= ~CitizenInstance.Flags.OnBikeLane;
				if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None && (instance2.m_segments.m_buffer[(int)position5.m_segment].m_flags & NetSegment.Flags.BikeBan) != NetSegment.Flags.None)
				{
					if (citizenData.m_citizen != 0u)
					{
						Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, 0, 0u);
					}
					citizenData.m_flags &= ~CitizenInstance.Flags.RidingBicycle;
				}
			}
			byte b;
			PathUnit.CalculatePathPositionOffset(laneID, vector, out b);
			if (position5.m_segment != position.m_segment)
			{
				if ((instance2.m_segments.m_buffer[(int)position5.m_segment].m_flags & (NetSegment.Flags.Collapsed | NetSegment.Flags.Flooded)) != NetSegment.Flags.None)
				{
					goto Block_65;
				}
				bezier = default(Bezier3);
				instance2.m_lanes.m_buffer[(int)((UIntPtr)num4)].CalculatePositionAndDirection((float)position.m_offset * 0.003921569f, out bezier.a, out vector5);
				Vector3 vector6;
				instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)b * 0.003921569f, out bezier.d, out vector6);
				if (position.m_offset == 0)
				{
					vector5 = -vector5;
				}
				if (b < position5.m_offset)
				{
					vector6 = -vector6;
				}
				vector5.Normalize();
				vector6.Normalize();
				float num17;
				NetSegment.CalculateMiddlePoints(bezier.a, vector5, bezier.d, vector6, true, true, out bezier.b, out bezier.c, out num17);
				if (num17 >= 1f)
				{
					if (num17 > 64f)
					{
						goto Block_69;
					}
					if (citizenData.m_lastPathOffset == 0 && !this.CheckSegmentChange(instanceID, ref citizenData, position, position5, (int)position.m_offset, (int)b, bezier))
					{
						goto Block_71;
					}
					float num18 = Mathf.Min(bezier.a.y, bezier.d.y);
					float num19 = Mathf.Max(bezier.a.y, bezier.d.y);
					bezier.b.y = Mathf.Clamp(bezier.b.y, num18, num19);
					bezier.c.y = Mathf.Clamp(bezier.c.y, num18, num19);
					float width2 = info3.m_lanes[(int)position5.m_lane].m_width;
					while (citizenData.m_lastPathOffset < 255)
					{
						float num20 = Mathf.Sqrt(minSqrDistance) - VectorUtils.LengthXZ(vector - frameData.m_position);
						int num21;
						if (num20 < 0f)
						{
							num21 = 8;
						}
						else
						{
							num21 = 8 + Mathf.CeilToInt(num20 * 256f / (num17 + 1f));
						}
						citizenData.m_lastPathOffset = (byte)Mathf.Min((int)citizenData.m_lastPathOffset + num21, 255);
						float num22 = (float)citizenData.m_lastPathOffset * 0.003921569f;
						vector = bezier.Position(num22);
						vector.w = num7 + (num16 - num7) * num22;
						num6 = Mathf.Max(0f, Mathf.Lerp(width, width2, num22) - 1f) * num5;
						Vector3 vector7 = bezier.Tangent(num22);
						Vector3 vector8 = Vector3.Cross(Vector3.get_up(), vector7).get_normalized() * num6;
						vector.x += vector8.x;
						vector.z += vector8.z;
						num = VectorUtils.LengthSqrXZ(vector - frameData.m_position);
						if (num >= minSqrDistance)
						{
							goto Block_73;
						}
						if ((citizenData.m_flags & CitizenInstance.Flags.OnPath) == CitizenInstance.Flags.None)
						{
							citizenData.m_flags |= CitizenInstance.Flags.OnPath;
							if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) == CitizenInstance.Flags.None && (instance2.m_segments.m_buffer[(int)position5.m_segment].m_flags & NetSegment.Flags.BikeBan) == NetSegment.Flags.None)
							{
								this.SpawnBicycle(instanceID, ref citizenData, position);
							}
						}
					}
				}
			}
			else if (laneID != num4)
			{
				int num23 = (position.m_offset < 128) ? 0 : 255;
				int num24 = (b < 128) ? 0 : 255;
				Vector3 vector9;
				instance2.m_lanes.m_buffer[(int)((UIntPtr)num4)].CalculatePositionAndDirection((float)num23 * 0.003921569f, out segment.a, out vector9);
				if (num23 == 0)
				{
					segment.a -= vector9.get_normalized() * 1.5f;
				}
				else if (num23 == 255)
				{
					segment.a += vector9.get_normalized() * 1.5f;
				}
				Vector3 vector10;
				instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)num24 * 0.003921569f, out segment.b, out vector10);
				if (num24 == 0)
				{
					segment.b -= vector10.get_normalized() * 1.5f;
				}
				else if (num24 == 255)
				{
					segment.b += vector10.get_normalized() * 1.5f;
				}
				vector11 = Vector3.Cross(Vector3.get_up(), segment.b - segment.a).get_normalized() * num5;
				if (citizenData.m_lastPathOffset == 0 && num4 != laneID && !this.CheckLaneChange(instanceID, ref citizenData, position, position5, num23, num24))
				{
					goto Block_88;
				}
				float num25 = Mathf.Abs(info2.m_lanes[(int)position.m_lane].m_position - info3.m_lanes[(int)position5.m_lane].m_position);
				float num26 = (info2.m_halfWidth - info2.m_pavementWidth) / Mathf.Max(1f, num25);
				float num27 = info2.m_surfaceLevel - info2.m_lanes[(int)position.m_lane].m_verticalOffset;
				while (citizenData.m_lastPathOffset < 255)
				{
					float num28 = Mathf.Sqrt(minSqrDistance) - VectorUtils.LengthXZ(vector - frameData.m_position);
					int num29;
					if (num28 < 0f)
					{
						num29 = 8;
					}
					else
					{
						num29 = 8 + Mathf.CeilToInt(num28 * 256f / (num25 + 1f));
					}
					citizenData.m_lastPathOffset = (byte)Mathf.Min((int)citizenData.m_lastPathOffset + num29, 255);
					float num30 = (float)citizenData.m_lastPathOffset * 0.003921569f;
					vector = segment.Position(num30) + vector11;
					vector.w = num7 + (num16 - num7) * num30;
					if (Mathf.Abs(num30 - 0.5f) < num26)
					{
						vector.y += num27;
					}
					num = VectorUtils.LengthSqrXZ(vector - frameData.m_position);
					if (num >= minSqrDistance)
					{
						goto Block_91;
					}
					if ((citizenData.m_flags & CitizenInstance.Flags.OnPath) == CitizenInstance.Flags.None)
					{
						citizenData.m_flags |= CitizenInstance.Flags.OnPath;
						if ((instance2.m_segments.m_buffer[(int)position5.m_segment].m_flags & NetSegment.Flags.BikeBan) == NetSegment.Flags.None && (citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) == CitizenInstance.Flags.None)
						{
							this.SpawnBicycle(instanceID, ref citizenData, position);
						}
					}
				}
			}
			if (num12 != citizenData.m_path)
			{
				Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref citizenData.m_path);
			}
			citizenData.m_pathPositionIndex = (byte)(num11 << 1);
			citizenData.m_lastPathOffset = b;
			position = position5;
			num4 = laneID;
		}
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_22:
		citizenData.m_flags = ((citizenData.m_flags & ~(CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition)) | info2.m_setCitizenFlags);
		return vector;
		Block_28:
		Singleton<PathManager>.get_instance().ReleasePath(citizenData.m_path);
		citizenData.m_path = 0u;
		return vector;
		Block_29:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_30:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		IL_8A4:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_49:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_50:
		citizenData.m_flags |= CitizenInstance.Flags.WaitingTransport;
		citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
		citizenData.m_waitCounter = 0;
		if (num12 != citizenData.m_path)
		{
			Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref citizenData.m_path);
		}
		citizenData.m_pathPositionIndex = (byte)(num11 << 1);
		citizenData.m_lastPathOffset = position5.m_offset;
		citizenData.m_flags = ((citizenData.m_flags & ~(CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition | CitizenInstance.Flags.OnBikeLane)) | info2.m_setCitizenFlags);
		if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None)
		{
			if (citizenData.m_citizen != 0u)
			{
				Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, 0, 0u);
			}
			citizenData.m_flags &= ~CitizenInstance.Flags.RidingBicycle;
		}
		return vector;
		Block_57:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		IL_A69:
		if (num12 != citizenData.m_path)
		{
			Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref citizenData.m_path);
		}
		citizenData.m_pathPositionIndex = (byte)(num11 << 1);
		citizenData.m_lastPathOffset = position5.m_offset;
		if (!this.SpawnVehicle(instanceID, ref citizenData, position5))
		{
			this.InvalidPath(instanceID, ref citizenData);
		}
		return vector;
		Block_60:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_65:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_69:
		this.InvalidPath(instanceID, ref citizenData);
		return vector;
		Block_71:
		Vector3 vector12 = Vector3.Cross(Vector3.get_up(), vector5).get_normalized() * num6;
		return bezier.a + vector12;
		Block_73:
		CitizenInstance.Flags flags = citizenData.m_flags & ~(CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition);
		flags |= (info2.m_setCitizenFlags & info3.m_setCitizenFlags);
		flags |= ((info2.m_setCitizenFlags | info3.m_setCitizenFlags) & CitizenInstance.Flags.Transition);
		if ((flags & CitizenInstance.Flags.Underground) == CitizenInstance.Flags.None && ((info2.m_setCitizenFlags | info3.m_setCitizenFlags) & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None)
		{
			flags |= CitizenInstance.Flags.Transition;
		}
		citizenData.m_flags = flags;
		return vector;
		Block_88:
		return segment.a + vector11;
		Block_91:
		CitizenInstance.Flags flags2 = citizenData.m_flags & ~(CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition);
		flags2 |= (info2.m_setCitizenFlags & info3.m_setCitizenFlags);
		flags2 |= ((info2.m_setCitizenFlags | info3.m_setCitizenFlags) & CitizenInstance.Flags.Transition);
		if ((flags2 & CitizenInstance.Flags.Underground) == CitizenInstance.Flags.None && ((info2.m_setCitizenFlags | info3.m_setCitizenFlags) & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None)
		{
			flags2 |= CitizenInstance.Flags.Transition;
		}
		citizenData.m_flags = flags2;
		return vector;
	}

	protected virtual bool CheckSegmentChange(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position prevPos, PathUnit.Position nextPos, int prevOffset, int nextOffset, Bezier3 bezier)
	{
		return true;
	}

	protected virtual bool CheckLaneChange(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position prevPos, PathUnit.Position nextPos, int prevOffset, int nextOffset)
	{
		return true;
	}

	protected virtual bool SpawnVehicle(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position pathPos)
	{
		return false;
	}

	protected virtual bool SpawnBicycle(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position pathPos)
	{
		return false;
	}

	protected void CheckCollisions(ushort instanceID, ref CitizenInstance citizenData, Vector3 sourcePos, Vector3 targetPos, ushort buildingID, ref Vector3 pushAmount, ref float pushDivider)
	{
		Segment3 segment;
		segment..ctor(sourcePos, targetPos);
		Vector3 min = segment.Min();
		min.x -= this.m_info.m_radius;
		min.z -= this.m_info.m_radius;
		Vector3 max = segment.Max();
		max.x += this.m_info.m_radius;
		max.y += this.m_info.m_height;
		max.z += this.m_info.m_radius;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		int num = Mathf.Max((int)((min.x - 3f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((min.z - 3f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((max.x + 3f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((max.z + 3f) / 8f + 1080f), 2159);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_citizenGrid[i * 2160 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = this.CheckCollisions(instanceID, ref citizenData, segment, min, max, num5, ref instance.m_instances.m_buffer[(int)num5], ref pushAmount, ref pushDivider);
					if (++num6 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		int num7 = Mathf.Max((int)((min.x - 10f) / 32f + 270f), 0);
		int num8 = Mathf.Max((int)((min.z - 10f) / 32f + 270f), 0);
		int num9 = Mathf.Min((int)((max.x + 10f) / 32f + 270f), 539);
		int num10 = Mathf.Min((int)((max.z + 10f) / 32f + 270f), 539);
		for (int k = num8; k <= num10; k++)
		{
			for (int l = num7; l <= num9; l++)
			{
				ushort num11 = instance2.m_vehicleGrid[k * 540 + l];
				int num12 = 0;
				while (num11 != 0)
				{
					num11 = this.CheckCollisions(instanceID, ref citizenData, segment, min, max, num11, ref instance2.m_vehicles.m_buffer[(int)num11], ref pushAmount, ref pushDivider);
					if (++num12 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int m = num8; m <= num10; m++)
		{
			for (int n = num7; n <= num9; n++)
			{
				ushort num13 = instance2.m_parkedGrid[m * 540 + n];
				int num14 = 0;
				while (num13 != 0)
				{
					num13 = this.CheckCollisions(instanceID, ref citizenData, segment, min, max, num13, ref instance2.m_parkedVehicles.m_buffer[(int)num13], ref pushAmount, ref pushDivider);
					if (++num14 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (buildingID != 0)
		{
			BuildingManager instance3 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance3.m_buildings.m_buffer[(int)buildingID].Info;
			if (info.m_props != null)
			{
				Vector3 position = instance3.m_buildings.m_buffer[(int)buildingID].m_position;
				float angle = instance3.m_buildings.m_buffer[(int)buildingID].m_angle;
				int length = instance3.m_buildings.m_buffer[(int)buildingID].Length;
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(Building.CalculateMeshPosition(info, position, angle, length), Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
				for (int num15 = 0; num15 < info.m_props.Length; num15++)
				{
					BuildingInfo.Prop prop = info.m_props[num15];
					Randomizer randomizer;
					randomizer..ctor((int)buildingID << 6 | prop.m_index);
					if (randomizer.Int32(100u) < prop.m_probability && length >= prop.m_requiredLength)
					{
						Vector3 vector = matrix4x.MultiplyPoint(prop.m_position);
						if (vector.x >= min.x - 2f && vector.x <= max.x + 2f)
						{
							if (vector.z >= min.z - 2f && vector.z <= max.z + 2f)
							{
								PropInfo propInfo = prop.m_finalProp;
								TreeInfo treeInfo = prop.m_finalTree;
								float num16 = 0f;
								float num17 = 0f;
								if (propInfo != null)
								{
									propInfo = propInfo.GetVariation(ref randomizer);
									if (propInfo.m_isMarker || propInfo.m_isDecal || !propInfo.m_hasRenderer)
									{
										goto IL_7D3;
									}
									num16 = propInfo.m_generatedInfo.m_size.x * 0.5f;
									num17 = propInfo.m_generatedInfo.m_size.y;
								}
								else if (treeInfo != null)
								{
									treeInfo = treeInfo.GetVariation(ref randomizer);
									num16 = (treeInfo.m_generatedInfo.m_size.x + treeInfo.m_generatedInfo.m_size.z) * 0.125f;
									num17 = treeInfo.m_generatedInfo.m_size.y;
								}
								if (!prop.m_fixedHeight)
								{
									vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
								}
								else if (info.m_requireHeightMap)
								{
									vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector) + prop.m_position.y;
								}
								if (vector.y + num17 >= min.y && vector.y <= max.y)
								{
									num16 = this.m_info.m_radius + num16;
									float num19;
									float num18 = segment.DistanceSqr(vector, ref num19);
									if (num18 < num16 * num16)
									{
										float num20 = num16 - Mathf.Sqrt(num18);
										float num21 = 1f - num18 / (num16 * num16);
										Vector3 vector2 = segment.Position(num19 * 0.9f);
										vector2.y = 0f;
										vector.y = 0f;
										Vector3 vector3 = Vector3.Normalize(vector2 - vector);
										Vector3 vector4 = Vector3.Normalize(new Vector3(segment.b.x - segment.a.x, 0f, segment.b.z - segment.a.z));
										Vector3 vector5 = new Vector3(vector4.z, 0f, -vector4.x) * Mathf.Abs(Vector3.Dot(vector3, vector4) * 0.5f);
										if (Vector3.Dot(vector3, vector5) >= 0f)
										{
											vector3 += vector5;
										}
										else
										{
											vector3 -= vector5;
										}
										pushAmount += vector3 * (num20 * num21);
										pushDivider += num21;
									}
								}
							}
						}
					}
					IL_7D3:;
				}
			}
		}
	}

	private ushort CheckCollisions(ushort instanceID, ref CitizenInstance citizenData, Segment3 segment, Vector3 min, Vector3 max, ushort otherID, ref CitizenInstance otherData, ref Vector3 pushAmount, ref float pushDivider)
	{
		if (otherID == instanceID)
		{
			return otherData.m_nextGridInstance;
		}
		if (((citizenData.m_flags | otherData.m_flags) & CitizenInstance.Flags.Transition) == CitizenInstance.Flags.None && (citizenData.m_flags & CitizenInstance.Flags.Underground) != (citizenData.m_flags & CitizenInstance.Flags.Underground))
		{
			return otherData.m_nextGridInstance;
		}
		CitizenInfo info = otherData.Info;
		CitizenInstance.Frame lastFrameData = otherData.GetLastFrameData();
		Vector3 position = lastFrameData.m_position;
		Vector3 vector = lastFrameData.m_position + lastFrameData.m_velocity;
		Segment3 segment2;
		segment2..ctor(position, vector);
		Vector3 vector2 = segment2.Min();
		vector2.x -= info.m_radius;
		vector2.z -= info.m_radius;
		Vector3 vector3 = segment2.Max();
		vector3.x += info.m_radius;
		vector3.y += info.m_height;
		vector3.z += info.m_radius;
		if (min.x < vector3.x && max.x > vector2.x && min.z < vector3.z && max.z > vector2.z && min.y < vector3.y && max.y > vector2.y)
		{
			float num = this.m_info.m_radius + info.m_radius;
			float num3;
			float num4;
			float num2 = segment.DistanceSqr(segment2, ref num3, ref num4);
			if (num2 < num * num)
			{
				float num5 = num - Mathf.Sqrt(num2);
				float num6 = 1f - num2 / (num * num);
				Vector3 vector4 = segment.Position(num3 * 0.9f);
				Vector3 vector5 = segment2.Position(num4);
				vector4.y = 0f;
				vector5.y = 0f;
				Vector3 vector6 = vector4 - vector5;
				Vector3 vector7;
				vector7..ctor(segment.b.z - segment.a.z, 0f, segment.a.x - segment.b.x);
				if (Vector3.Dot(vector6, vector7) >= 0f)
				{
					vector6 += vector7;
				}
				else
				{
					vector6 -= vector7;
				}
				pushAmount += vector6.get_normalized() * (num5 * num6);
				pushDivider += num6;
			}
		}
		return otherData.m_nextGridInstance;
	}

	private ushort CheckCollisions(ushort instanceID, ref CitizenInstance citizenData, Segment3 segment, Vector3 min, Vector3 max, ushort otherID, ref Vehicle otherData, ref Vector3 pushAmount, ref float pushDivider)
	{
		VehicleInfo info = otherData.Info;
		if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
		{
			return otherData.m_nextGridVehicle;
		}
		if ((otherData.m_flags & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (citizenData.m_flags & CitizenInstance.Flags.Transition) == CitizenInstance.Flags.None && (otherData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0 != ((citizenData.m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None))
		{
			return otherData.m_nextGridVehicle;
		}
		Segment3 segment2 = otherData.m_segment;
		Vector3 vector = Vector3.Min(segment2.Min(), otherData.m_targetPos1);
		vector.x -= 1f;
		vector.z -= 1f;
		Vector3 vector2 = Vector3.Max(segment2.Max(), otherData.m_targetPos1);
		vector2.x += 1f;
		vector2.y += 1f;
		vector2.z += 1f;
		if (min.x < vector2.x && max.x > vector.x && min.z < vector2.z && max.z > vector.z && min.y < vector2.y && max.y > vector.y)
		{
			float num = this.m_info.m_radius + 1f;
			float num3;
			float num4;
			float num2 = segment.DistanceSqr(segment2, ref num3, ref num4);
			if (num2 < num * num)
			{
				float num5 = num - Mathf.Sqrt(num2);
				float num6 = 1f - num2 / (num * num);
				Vector3 vector3 = segment.Position(num3 * 0.9f);
				Vector3 vector4 = segment2.Position(num4);
				vector3.y = 0f;
				vector4.y = 0f;
				Vector3 vector5 = Vector3.Normalize(vector3 - vector4);
				Vector3 vector6 = Vector3.Normalize(new Vector3(segment.b.x - segment.a.x, 0f, segment.b.z - segment.a.z));
				Vector3 vector7 = new Vector3(vector6.z, 0f, -vector6.x) * Mathf.Abs(Vector3.Dot(vector5, vector6) * 0.5f);
				if (Vector3.Dot(vector5, vector7) >= 0f)
				{
					vector5 += vector7;
				}
				else
				{
					vector5 -= vector7;
				}
				pushAmount += vector5 * (num5 * num6);
				pushDivider += num6;
			}
			float magnitude = otherData.GetLastFrameVelocity().get_magnitude();
			if (magnitude > 0.1f)
			{
				float num7 = this.m_info.m_radius + 3f;
				segment2.a = segment2.b;
				segment2.b += Vector3.ClampMagnitude(otherData.m_targetPos1 - segment2.b, magnitude * 4f);
				num2 = segment.DistanceSqr(segment2, ref num3, ref num4);
				if (num2 > num * num && num2 < num7 * num7)
				{
					float num8 = num7 - Mathf.Sqrt(num2);
					float num9 = 1f - num2 / (num7 * num7);
					Vector3 vector8 = segment.Position(num3 * 0.9f);
					Vector3 vector9 = segment2.Position(num4);
					vector8.y = 0f;
					vector9.y = 0f;
					Vector3 vector10 = vector8 - vector9;
					pushAmount += vector10.get_normalized() * (num8 * num9);
					pushDivider += num9;
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private ushort CheckCollisions(ushort instanceID, ref CitizenInstance citizenData, Segment3 segment, Vector3 min, Vector3 max, ushort otherID, ref VehicleParked otherData, ref Vector3 pushAmount, ref float pushDivider)
	{
		VehicleInfo info = otherData.Info;
		Vector3 position = otherData.m_position;
		Vector3 vector = otherData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, info.m_generatedInfo.m_size.z * 0.5f - 1f));
		Segment3 segment2;
		segment2.a = position - vector;
		segment2.b = position + vector;
		Vector3 vector2 = segment2.Min();
		vector2.x -= 1f;
		vector2.z -= 1f;
		Vector3 vector3 = segment2.Max();
		vector3.x += 1f;
		vector3.y += 1f;
		vector3.z += 1f;
		if (min.x < vector3.x && max.x > vector2.x && min.z < vector3.z && max.z > vector2.z && min.y < vector3.y && max.y > vector2.y)
		{
			float num = this.m_info.m_radius + 1f;
			float num3;
			float num4;
			float num2 = segment.DistanceSqr(segment2, ref num3, ref num4);
			if (num2 < num * num)
			{
				float num5 = num - Mathf.Sqrt(num2);
				float num6 = 1f - num2 / (num * num);
				Vector3 vector4 = segment.Position(num3 * 0.9f);
				Vector3 vector5 = segment2.Position(num4);
				vector4.y = 0f;
				vector5.y = 0f;
				Vector3 vector6 = Vector3.Normalize(vector4 - vector5);
				Vector3 vector7 = Vector3.Normalize(new Vector3(segment.b.x - segment.a.x, 0f, segment.b.z - segment.a.z));
				Vector3 vector8 = new Vector3(vector7.z, 0f, -vector7.x) * Mathf.Abs(Vector3.Dot(vector6, vector7) * 0.5f);
				if (Vector3.Dot(vector6, vector8) >= 0f)
				{
					vector6 += vector8;
				}
				else
				{
					vector6 -= vector8;
				}
				pushAmount += vector6 * (num5 * num6);
				pushDivider += num6;
			}
		}
		return otherData.m_nextGridParked;
	}

	protected void InvalidPath(ushort instanceID, ref CitizenInstance citizenData)
	{
		if (citizenData.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(citizenData.m_path);
			citizenData.m_path = 0u;
		}
		citizenData.m_flags &= ~(CitizenInstance.Flags.WaitingTransport | CitizenInstance.Flags.EnteringVehicle | CitizenInstance.Flags.BoredOfWaiting | CitizenInstance.Flags.WaitingTaxi);
		if (!this.StartPathFind(instanceID, ref citizenData))
		{
			citizenData.Unspawn(instanceID);
		}
	}

	public virtual InstanceID GetTargetID(ushort instanceID, ref CitizenInstance citizenData)
	{
		return new InstanceID
		{
			Building = citizenData.m_targetBuilding
		};
	}

	protected virtual bool StartPathFind(ushort instanceID, ref CitizenInstance citizenData)
	{
		return false;
	}

	protected bool StartPathFind(ushort instanceID, ref CitizenInstance citizenData, Vector3 startPos, Vector3 endPos, VehicleInfo vehicleInfo)
	{
		NetInfo.LaneType laneType = NetInfo.LaneType.Pedestrian;
		VehicleInfo.VehicleType vehicleType = VehicleInfo.VehicleType.None;
		bool randomParking = false;
		bool combustionEngine = false;
		if (vehicleInfo != null)
		{
			if (vehicleInfo.m_class.m_subService == ItemClass.SubService.PublicTransportTaxi)
			{
				if ((citizenData.m_flags & CitizenInstance.Flags.CannotUseTaxi) == CitizenInstance.Flags.None && Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_productionData.m_finalTaxiCapacity != 0u)
				{
					SimulationManager instance = Singleton<SimulationManager>.get_instance();
					if (instance.m_isNightTime || instance.m_randomizer.Int32(2u) == 0)
					{
						laneType |= (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle);
						vehicleType |= vehicleInfo.m_vehicleType;
					}
				}
			}
			else
			{
				laneType |= NetInfo.LaneType.Vehicle;
				vehicleType |= vehicleInfo.m_vehicleType;
				if (citizenData.m_targetBuilding != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info.m_class.m_service > ItemClass.Service.Office)
				{
					randomParking = true;
				}
				if (vehicleInfo.m_vehicleType == VehicleInfo.VehicleType.Car)
				{
					combustionEngine = (vehicleInfo.m_class.m_subService == ItemClass.SubService.ResidentialLow);
				}
			}
		}
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		PathUnit.Position vehiclePosition = default(PathUnit.Position);
		ushort parkedVehicle = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_parkedVehicle;
		if (parkedVehicle != 0)
		{
			Vector3 position = Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)parkedVehicle].m_position;
			PathManager.FindPathPosition(position, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, false, false, 32f, out vehiclePosition);
		}
		bool allowUnderground = (citizenData.m_flags & (CitizenInstance.Flags.Underground | CitizenInstance.Flags.Transition)) != CitizenInstance.Flags.None;
		PathUnit.Position startPosA;
		PathUnit.Position endPosA;
		if (this.FindPathPosition(instanceID, ref citizenData, startPos, laneType, vehicleType, allowUnderground, out startPosA) && this.FindPathPosition(instanceID, ref citizenData, endPos, laneType, vehicleType, false, out endPosA))
		{
			if ((citizenData.m_flags & CitizenInstance.Flags.CannotUseTransport) == CitizenInstance.Flags.None)
			{
				laneType |= NetInfo.LaneType.PublicTransport;
				uint citizen = instance2.m_instances.m_buffer[(int)instanceID].m_citizen;
				if (citizen != 0u && (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.Evacuating) != Citizen.Flags.None)
				{
					laneType |= NetInfo.LaneType.EvacuationTransport;
				}
			}
			PathUnit.Position position2 = default(PathUnit.Position);
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, position2, endPosA, position2, vehiclePosition, laneType, vehicleType, 20000f, false, false, false, false, randomParking, false, combustionEngine))
			{
				if (citizenData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(citizenData.m_path);
				}
				citizenData.m_path = path;
				citizenData.m_flags |= CitizenInstance.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public virtual bool FindPathPosition(ushort instanceID, ref CitizenInstance citizenData, Vector3 pos, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, bool allowUnderground, out PathUnit.Position position)
	{
		position = default(PathUnit.Position);
		float num = 1E+10f;
		PathUnit.Position position2;
		PathUnit.Position position3;
		float num2;
		float num3;
		if (PathManager.FindPathPosition(pos, ItemClass.Service.Road, laneTypes, vehicleTypes, allowUnderground, false, 32f, out position2, out position3, out num2, out num3) && num2 < num)
		{
			num = num2;
			position = position2;
		}
		PathUnit.Position position4;
		PathUnit.Position position5;
		float num4;
		float num5;
		if (PathManager.FindPathPosition(pos, ItemClass.Service.Beautification, laneTypes, vehicleTypes, allowUnderground, false, 32f, out position4, out position5, out num4, out num5) && num4 < num)
		{
			num = num4;
			position = position4;
		}
		PathUnit.Position position6;
		PathUnit.Position position7;
		float num6;
		float num7;
		if ((citizenData.m_flags & CitizenInstance.Flags.CannotUseTransport) == CitizenInstance.Flags.None && PathManager.FindPathPosition(pos, ItemClass.Service.PublicTransport, laneTypes, vehicleTypes, allowUnderground, false, 32f, out position6, out position7, out num6, out num7) && num6 < num)
		{
			position = position6;
		}
		return position.m_segment != 0;
	}

	public virtual bool IsAnimal()
	{
		return false;
	}

	public virtual string GenerateName(ushort instanceID, bool useCitizen)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint citizen = instance.m_instances.m_buffer[(int)instanceID].m_citizen;
		if (citizen != 0u && useCitizen)
		{
			return CitizenAI.GenerateCitizenName(citizen, instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_family);
		}
		return null;
	}

	public static string GenerateCitizenName(uint citizenID, byte family)
	{
		Randomizer randomizer;
		randomizer..ctor(citizenID);
		Randomizer randomizer2;
		randomizer2..ctor((int)family);
		string text = "NAME_FEMALE_FIRST";
		string text2 = "NAME_FEMALE_LAST";
		if (Citizen.GetGender(citizenID) == Citizen.Gender.Male)
		{
			text = "NAME_MALE_FIRST";
			text2 = "NAME_MALE_LAST";
		}
		text = Locale.Get(text, randomizer.Int32(Locale.Count(text)));
		text2 = Locale.Get(text2, randomizer2.Int32(Locale.Count(text2)));
		return StringUtils.SafeFormat(text2, text);
	}

	[NonSerialized]
	public CitizenInfo m_info;
}
