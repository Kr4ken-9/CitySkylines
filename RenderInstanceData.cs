﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

[StructLayout(LayoutKind.Explicit)]
public struct RenderInstanceData
{
	public ushort GetUShort(int index)
	{
		switch (index)
		{
		case 0:
			return this.m_dataUShort0;
		case 1:
			return this.m_dataUShort1;
		case 2:
			return this.m_dataUShort2;
		case 3:
			return this.m_dataUShort3;
		case 4:
			return this.m_dataUShort4;
		case 5:
			return this.m_dataUShort5;
		case 6:
			return this.m_dataUShort6;
		case 7:
			return this.m_dataUShort7;
		case 8:
			return this.m_dataUShort8;
		case 9:
			return this.m_dataUShort9;
		case 10:
			return this.m_dataUShort10;
		case 11:
			return this.m_dataUShort11;
		case 12:
			return this.m_dataUShort12;
		case 13:
			return this.m_dataUShort13;
		case 14:
			return this.m_dataUShort14;
		case 15:
			return this.m_dataUShort15;
		case 16:
			return this.m_dataUShort16;
		case 17:
			return this.m_dataUShort17;
		case 18:
			return this.m_dataUShort18;
		case 19:
			return this.m_dataUShort19;
		case 20:
			return this.m_dataUShort20;
		case 21:
			return this.m_dataUShort21;
		case 22:
			return this.m_dataUShort22;
		case 23:
			return this.m_dataUShort23;
		case 24:
			return this.m_dataUShort24;
		case 25:
			return this.m_dataUShort25;
		case 26:
			return this.m_dataUShort26;
		case 27:
			return this.m_dataUShort27;
		case 28:
			return this.m_dataUShort28;
		case 29:
			return this.m_dataUShort29;
		case 30:
			return this.m_dataUShort30;
		case 31:
			return this.m_dataUShort31;
		case 32:
			return this.m_dataUShort32;
		case 33:
			return this.m_dataUShort33;
		case 34:
			return this.m_dataUShort34;
		case 35:
			return this.m_dataUShort35;
		case 36:
			return this.m_dataUShort36;
		case 37:
			return this.m_dataUShort37;
		case 38:
			return this.m_dataUShort38;
		case 39:
			return this.m_dataUShort39;
		case 40:
			return this.m_dataUShort40;
		case 41:
			return this.m_dataUShort41;
		case 42:
			return this.m_dataUShort42;
		case 43:
			return this.m_dataUShort43;
		case 44:
			return this.m_dataUShort44;
		case 45:
			return this.m_dataUShort45;
		case 46:
			return this.m_dataUShort46;
		case 47:
			return this.m_dataUShort47;
		case 48:
			return this.m_dataUShort48;
		case 49:
			return this.m_dataUShort49;
		case 50:
			return this.m_dataUShort50;
		case 51:
			return this.m_dataUShort51;
		case 52:
			return this.m_dataUShort52;
		case 53:
			return this.m_dataUShort53;
		case 54:
			return this.m_dataUShort54;
		case 55:
			return this.m_dataUShort55;
		case 56:
			return this.m_dataUShort56;
		case 57:
			return this.m_dataUShort57;
		case 58:
			return this.m_dataUShort58;
		case 59:
			return this.m_dataUShort59;
		case 60:
			return this.m_dataUShort60;
		case 61:
			return this.m_dataUShort61;
		case 62:
			return this.m_dataUShort62;
		case 63:
			return this.m_dataUShort63;
		case 64:
			return this.m_dataUShort64;
		case 65:
			return this.m_dataUShort65;
		case 66:
			return this.m_dataUShort66;
		case 67:
			return this.m_dataUShort67;
		case 68:
			return this.m_dataUShort68;
		case 69:
			return this.m_dataUShort69;
		case 70:
			return this.m_dataUShort70;
		case 71:
			return this.m_dataUShort71;
		default:
			return 0;
		}
	}

	public void SetUShort(int index, ushort value)
	{
		switch (index)
		{
		case 0:
			this.m_dataUShort0 = value;
			return;
		case 1:
			this.m_dataUShort1 = value;
			return;
		case 2:
			this.m_dataUShort2 = value;
			return;
		case 3:
			this.m_dataUShort3 = value;
			return;
		case 4:
			this.m_dataUShort4 = value;
			return;
		case 5:
			this.m_dataUShort5 = value;
			return;
		case 6:
			this.m_dataUShort6 = value;
			return;
		case 7:
			this.m_dataUShort7 = value;
			return;
		case 8:
			this.m_dataUShort8 = value;
			return;
		case 9:
			this.m_dataUShort9 = value;
			return;
		case 10:
			this.m_dataUShort10 = value;
			return;
		case 11:
			this.m_dataUShort11 = value;
			return;
		case 12:
			this.m_dataUShort12 = value;
			return;
		case 13:
			this.m_dataUShort13 = value;
			return;
		case 14:
			this.m_dataUShort14 = value;
			return;
		case 15:
			this.m_dataUShort15 = value;
			return;
		case 16:
			this.m_dataUShort16 = value;
			return;
		case 17:
			this.m_dataUShort17 = value;
			return;
		case 18:
			this.m_dataUShort18 = value;
			return;
		case 19:
			this.m_dataUShort19 = value;
			return;
		case 20:
			this.m_dataUShort20 = value;
			return;
		case 21:
			this.m_dataUShort21 = value;
			return;
		case 22:
			this.m_dataUShort22 = value;
			return;
		case 23:
			this.m_dataUShort23 = value;
			return;
		case 24:
			this.m_dataUShort24 = value;
			return;
		case 25:
			this.m_dataUShort25 = value;
			return;
		case 26:
			this.m_dataUShort26 = value;
			return;
		case 27:
			this.m_dataUShort27 = value;
			return;
		case 28:
			this.m_dataUShort28 = value;
			return;
		case 29:
			this.m_dataUShort29 = value;
			return;
		case 30:
			this.m_dataUShort30 = value;
			return;
		case 31:
			this.m_dataUShort31 = value;
			return;
		case 32:
			this.m_dataUShort32 = value;
			return;
		case 33:
			this.m_dataUShort33 = value;
			return;
		case 34:
			this.m_dataUShort34 = value;
			return;
		case 35:
			this.m_dataUShort35 = value;
			return;
		case 36:
			this.m_dataUShort36 = value;
			return;
		case 37:
			this.m_dataUShort37 = value;
			return;
		case 38:
			this.m_dataUShort38 = value;
			return;
		case 39:
			this.m_dataUShort39 = value;
			return;
		case 40:
			this.m_dataUShort40 = value;
			return;
		case 41:
			this.m_dataUShort41 = value;
			return;
		case 42:
			this.m_dataUShort42 = value;
			return;
		case 43:
			this.m_dataUShort43 = value;
			return;
		case 44:
			this.m_dataUShort44 = value;
			return;
		case 45:
			this.m_dataUShort45 = value;
			return;
		case 46:
			this.m_dataUShort46 = value;
			return;
		case 47:
			this.m_dataUShort47 = value;
			return;
		case 48:
			this.m_dataUShort48 = value;
			return;
		case 49:
			this.m_dataUShort49 = value;
			return;
		case 50:
			this.m_dataUShort50 = value;
			return;
		case 51:
			this.m_dataUShort51 = value;
			return;
		case 52:
			this.m_dataUShort52 = value;
			return;
		case 53:
			this.m_dataUShort53 = value;
			return;
		case 54:
			this.m_dataUShort54 = value;
			return;
		case 55:
			this.m_dataUShort55 = value;
			return;
		case 56:
			this.m_dataUShort56 = value;
			return;
		case 57:
			this.m_dataUShort57 = value;
			return;
		case 58:
			this.m_dataUShort58 = value;
			return;
		case 59:
			this.m_dataUShort59 = value;
			return;
		case 60:
			this.m_dataUShort60 = value;
			return;
		case 61:
			this.m_dataUShort61 = value;
			return;
		case 62:
			this.m_dataUShort62 = value;
			return;
		case 63:
			this.m_dataUShort63 = value;
			return;
		case 64:
			this.m_dataUShort64 = value;
			return;
		case 65:
			this.m_dataUShort65 = value;
			return;
		case 66:
			this.m_dataUShort66 = value;
			return;
		case 67:
			this.m_dataUShort67 = value;
			return;
		case 68:
			this.m_dataUShort68 = value;
			return;
		case 69:
			this.m_dataUShort69 = value;
			return;
		case 70:
			this.m_dataUShort70 = value;
			return;
		case 71:
			this.m_dataUShort71 = value;
			return;
		default:
			return;
		}
	}

	[FieldOffset(0)]
	public Matrix4x4 m_dataMatrix2;

	[FieldOffset(64)]
	public Matrix4x4 m_dataMatrix3;

	[FieldOffset(128)]
	public Vector4 m_dataVector4;

	[FieldOffset(0)]
	public ushort m_dataUShort0;

	[FieldOffset(2)]
	public ushort m_dataUShort1;

	[FieldOffset(4)]
	public ushort m_dataUShort2;

	[FieldOffset(6)]
	public ushort m_dataUShort3;

	[FieldOffset(8)]
	public ushort m_dataUShort4;

	[FieldOffset(10)]
	public ushort m_dataUShort5;

	[FieldOffset(12)]
	public ushort m_dataUShort6;

	[FieldOffset(14)]
	public ushort m_dataUShort7;

	[FieldOffset(16)]
	public ushort m_dataUShort8;

	[FieldOffset(18)]
	public ushort m_dataUShort9;

	[FieldOffset(20)]
	public ushort m_dataUShort10;

	[FieldOffset(22)]
	public ushort m_dataUShort11;

	[FieldOffset(24)]
	public ushort m_dataUShort12;

	[FieldOffset(26)]
	public ushort m_dataUShort13;

	[FieldOffset(28)]
	public ushort m_dataUShort14;

	[FieldOffset(30)]
	public ushort m_dataUShort15;

	[FieldOffset(32)]
	public ushort m_dataUShort16;

	[FieldOffset(34)]
	public ushort m_dataUShort17;

	[FieldOffset(36)]
	public ushort m_dataUShort18;

	[FieldOffset(38)]
	public ushort m_dataUShort19;

	[FieldOffset(40)]
	public ushort m_dataUShort20;

	[FieldOffset(42)]
	public ushort m_dataUShort21;

	[FieldOffset(44)]
	public ushort m_dataUShort22;

	[FieldOffset(46)]
	public ushort m_dataUShort23;

	[FieldOffset(48)]
	public ushort m_dataUShort24;

	[FieldOffset(50)]
	public ushort m_dataUShort25;

	[FieldOffset(52)]
	public ushort m_dataUShort26;

	[FieldOffset(54)]
	public ushort m_dataUShort27;

	[FieldOffset(56)]
	public ushort m_dataUShort28;

	[FieldOffset(58)]
	public ushort m_dataUShort29;

	[FieldOffset(60)]
	public ushort m_dataUShort30;

	[FieldOffset(62)]
	public ushort m_dataUShort31;

	[FieldOffset(64)]
	public ushort m_dataUShort32;

	[FieldOffset(66)]
	public ushort m_dataUShort33;

	[FieldOffset(68)]
	public ushort m_dataUShort34;

	[FieldOffset(70)]
	public ushort m_dataUShort35;

	[FieldOffset(72)]
	public ushort m_dataUShort36;

	[FieldOffset(74)]
	public ushort m_dataUShort37;

	[FieldOffset(76)]
	public ushort m_dataUShort38;

	[FieldOffset(78)]
	public ushort m_dataUShort39;

	[FieldOffset(80)]
	public ushort m_dataUShort40;

	[FieldOffset(82)]
	public ushort m_dataUShort41;

	[FieldOffset(84)]
	public ushort m_dataUShort42;

	[FieldOffset(86)]
	public ushort m_dataUShort43;

	[FieldOffset(88)]
	public ushort m_dataUShort44;

	[FieldOffset(90)]
	public ushort m_dataUShort45;

	[FieldOffset(92)]
	public ushort m_dataUShort46;

	[FieldOffset(94)]
	public ushort m_dataUShort47;

	[FieldOffset(96)]
	public ushort m_dataUShort48;

	[FieldOffset(98)]
	public ushort m_dataUShort49;

	[FieldOffset(100)]
	public ushort m_dataUShort50;

	[FieldOffset(102)]
	public ushort m_dataUShort51;

	[FieldOffset(104)]
	public ushort m_dataUShort52;

	[FieldOffset(106)]
	public ushort m_dataUShort53;

	[FieldOffset(108)]
	public ushort m_dataUShort54;

	[FieldOffset(110)]
	public ushort m_dataUShort55;

	[FieldOffset(112)]
	public ushort m_dataUShort56;

	[FieldOffset(114)]
	public ushort m_dataUShort57;

	[FieldOffset(116)]
	public ushort m_dataUShort58;

	[FieldOffset(118)]
	public ushort m_dataUShort59;

	[FieldOffset(120)]
	public ushort m_dataUShort60;

	[FieldOffset(122)]
	public ushort m_dataUShort61;

	[FieldOffset(124)]
	public ushort m_dataUShort62;

	[FieldOffset(126)]
	public ushort m_dataUShort63;

	[FieldOffset(128)]
	public ushort m_dataUShort64;

	[FieldOffset(130)]
	public ushort m_dataUShort65;

	[FieldOffset(132)]
	public ushort m_dataUShort66;

	[FieldOffset(134)]
	public ushort m_dataUShort67;

	[FieldOffset(136)]
	public ushort m_dataUShort68;

	[FieldOffset(138)]
	public ushort m_dataUShort69;

	[FieldOffset(140)]
	public ushort m_dataUShort70;

	[FieldOffset(142)]
	public ushort m_dataUShort71;
}
