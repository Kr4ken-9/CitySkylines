﻿using System;
using System.Threading;
using ColossalFramework;
using UnityEngine;

public class EffectManager : SimulationManagerBase<EffectManager, EffectProperties>, ISimulationManager, IRenderableManager, IAudibleManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_renderEventBuffer = new FastList<EffectManager.SimulationEvent>();
		this.m_audioEventBuffer = new FastList<EffectManager.SimulationEvent>();
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.InitEffectCollection);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void CreateRelay()
	{
		if (this.m_EffectsWrapper == null)
		{
			this.m_EffectsWrapper = new EffectsWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_EffectsWrapper != null)
		{
			this.m_EffectsWrapper.Release();
			this.m_EffectsWrapper = null;
		}
	}

	private void InitEffectCollection(SimulationManager.UpdateMode updateMode)
	{
		ParticleEffect.ClearAll();
		while (!Monitor.TryEnter(this.m_renderEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = 0; i < this.m_renderEventBuffer.m_size; i++)
			{
				this.m_renderEventBuffer.m_buffer[i] = default(EffectManager.SimulationEvent);
			}
			this.m_renderEventBuffer.m_size = 0;
		}
		finally
		{
			Monitor.Exit(this.m_renderEventBuffer);
		}
		while (!Monitor.TryEnter(this.m_audioEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int j = 0; j < this.m_audioEventBuffer.m_size; j++)
			{
				this.m_audioEventBuffer.m_buffer[j] = default(EffectManager.SimulationEvent);
			}
			this.m_audioEventBuffer.m_size = 0;
		}
		finally
		{
			Monitor.Exit(this.m_audioEventBuffer);
		}
		if (this.m_EffectsWrapper != null)
		{
			this.m_EffectsWrapper.InitEffectCollection();
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		int size = this.m_renderEventBuffer.m_size;
		int num = 0;
		for (int i = 0; i < size; i++)
		{
			if (this.RenderEvent(cameraInfo, ref this.m_renderEventBuffer.m_buffer[i]))
			{
				if (num != i)
				{
					this.m_renderEventBuffer.m_buffer[num] = this.m_renderEventBuffer.m_buffer[i];
					this.m_renderEventBuffer.m_buffer[i] = default(EffectManager.SimulationEvent);
				}
				num++;
			}
			else
			{
				this.m_renderEventBuffer.m_buffer[i] = default(EffectManager.SimulationEvent);
			}
		}
		while (!Monitor.TryEnter(this.m_renderEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int j = size; j < this.m_renderEventBuffer.m_size; j++)
			{
				this.m_renderEventBuffer.m_buffer[num] = this.m_renderEventBuffer.m_buffer[j];
				this.m_renderEventBuffer.m_buffer[j] = default(EffectManager.SimulationEvent);
				num++;
			}
			this.m_renderEventBuffer.m_size = num;
		}
		finally
		{
			Monitor.Exit(this.m_renderEventBuffer);
		}
	}

	private bool RenderEvent(RenderManager.CameraInfo cameraInfo, ref EffectManager.SimulationEvent e)
	{
		if (!(e.m_effectInfo != null))
		{
			return false;
		}
		if (e.m_startFrame != 0u && e.m_startFrame > Singleton<SimulationManager>.get_instance().m_referenceFrameIndex)
		{
			return true;
		}
		float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		e.m_effectInfo.RenderEffect(e.m_instance, e.m_spawnArea, e.m_velocity, e.m_acceleration, e.m_magnitude, e.m_timeOffset, simulationTimeDelta, cameraInfo);
		e.m_timeOffset += simulationTimeDelta;
		return e.m_timeOffset < e.m_renderDuration;
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		int size = this.m_audioEventBuffer.m_size;
		int num = 0;
		for (int i = 0; i < size; i++)
		{
			if (this.PlayEvent(listenerInfo, ref this.m_audioEventBuffer.m_buffer[i]))
			{
				if (num != i)
				{
					this.m_audioEventBuffer.m_buffer[num] = this.m_audioEventBuffer.m_buffer[i];
					this.m_audioEventBuffer.m_buffer[i] = default(EffectManager.SimulationEvent);
				}
			}
			else
			{
				this.m_audioEventBuffer.m_buffer[i] = default(EffectManager.SimulationEvent);
			}
		}
		while (!Monitor.TryEnter(this.m_audioEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int j = size; j < this.m_audioEventBuffer.m_size; j++)
			{
				this.m_audioEventBuffer.m_buffer[num] = this.m_audioEventBuffer.m_buffer[j];
				this.m_audioEventBuffer.m_buffer[j] = default(EffectManager.SimulationEvent);
				num++;
			}
			this.m_audioEventBuffer.m_size -= size;
		}
		finally
		{
			Monitor.Exit(this.m_audioEventBuffer);
		}
	}

	private bool PlayEvent(AudioManager.ListenerInfo listenerInfo, ref EffectManager.SimulationEvent e)
	{
		if (!(e.m_effectInfo != null))
		{
			return false;
		}
		if (e.m_startFrame != 0u && e.m_startFrame > Singleton<SimulationManager>.get_instance().m_referenceFrameIndex)
		{
			return true;
		}
		e.m_effectInfo.PlayEffect(e.m_instance, e.m_spawnArea, e.m_velocity, e.m_acceleration, e.m_magnitude, listenerInfo, e.m_audioGroup);
		return false;
	}

	public void DispatchEffect(EffectInfo effect, InstanceID instance, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup)
	{
		this.DispatchEffect(effect, instance, spawnArea, velocity, acceleration, magnitude, audioGroup, 0u, false);
	}

	public void DispatchEffect(EffectInfo effect, InstanceID instance, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup, bool avoidMultipleAudio)
	{
		this.DispatchEffect(effect, instance, spawnArea, velocity, acceleration, magnitude, audioGroup, 0u, avoidMultipleAudio);
	}

	public void DispatchEffect(EffectInfo effect, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup, uint startFrame)
	{
		this.DispatchEffect(effect, new InstanceID
		{
			RawData = this.m_nextInstanceIndex++
		}, spawnArea, velocity, acceleration, magnitude, audioGroup, startFrame, false);
	}

	public void DispatchEffect(EffectInfo effect, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup, uint startFrame, bool avoidMultipleAudio)
	{
		this.DispatchEffect(effect, new InstanceID
		{
			RawData = this.m_nextInstanceIndex++
		}, spawnArea, velocity, acceleration, magnitude, audioGroup, startFrame, avoidMultipleAudio);
	}

	public void DispatchEffect(EffectInfo effect, InstanceID instance, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup, uint startFrame)
	{
		this.DispatchEffect(effect, instance, spawnArea, velocity, acceleration, magnitude, audioGroup, startFrame, false);
	}

	public void DispatchEffect(EffectInfo effect, InstanceID instance, EffectInfo.SpawnArea spawnArea, Vector3 velocity, float acceleration, float magnitude, AudioGroup audioGroup, uint startFrame, bool avoidMultipleAudio)
	{
		EffectManager.SimulationEvent item;
		item.m_effectInfo = effect;
		item.m_instance = instance;
		item.m_spawnArea = spawnArea;
		item.m_velocity = velocity;
		item.m_acceleration = acceleration;
		item.m_magnitude = magnitude;
		item.m_renderDuration = effect.RenderDuration();
		item.m_timeOffset = 0f;
		item.m_audioGroup = audioGroup;
		item.m_startFrame = startFrame;
		if (effect.RequireRender())
		{
			while (!Monitor.TryEnter(this.m_renderEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_renderEventBuffer.Add(item);
			}
			finally
			{
				Monitor.Exit(this.m_renderEventBuffer);
			}
		}
		if (effect.RequirePlay() && (!avoidMultipleAudio || !this.m_audioEventAdded))
		{
			while (!Monitor.TryEnter(this.m_audioEventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_audioEventBuffer.Add(item);
			}
			finally
			{
				Monitor.Exit(this.m_audioEventBuffer);
			}
			if (avoidMultipleAudio)
			{
				this.m_audioEventAdded = true;
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		this.m_audioEventAdded = false;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	private FastList<EffectManager.SimulationEvent> m_renderEventBuffer;

	private FastList<EffectManager.SimulationEvent> m_audioEventBuffer;

	private uint m_nextInstanceIndex;

	private bool m_audioEventAdded;

	[NonSerialized]
	public EffectsWrapper m_EffectsWrapper;

	public struct SimulationEvent
	{
		public EffectInfo m_effectInfo;

		public InstanceID m_instance;

		public EffectInfo.SpawnArea m_spawnArea;

		public Vector3 m_velocity;

		public float m_acceleration;

		public float m_magnitude;

		public float m_renderDuration;

		public float m_timeOffset;

		public AudioGroup m_audioGroup;

		public uint m_startFrame;
	}
}
