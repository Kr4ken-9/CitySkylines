﻿using System;
using UnityEngine;

public class EngineSoundEffect : SoundEffect
{
	public override void PlayEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup)
	{
		Vector3 position = area.m_matrix.MultiplyPoint(this.m_position);
		float magnitude2 = velocity.get_magnitude();
		acceleration = Mathf.Max(0f, acceleration);
		float pitch = this.m_minPitch + magnitude2 * this.m_pitchSpeedMultiplier + acceleration * this.m_pitchAccelerationMultiplier;
		float range = Mathf.Min(this.m_minRange + magnitude2 * this.m_rangeSpeedMultiplier + acceleration + acceleration * this.m_rangeAccelerationMultiplier, this.m_range);
		base.PlaySound(id, listenerInfo, audioGroup, position, velocity, range, magnitude, pitch);
	}

	public float m_minPitch = 1f;

	public float m_pitchSpeedMultiplier = 0.02f;

	public float m_pitchAccelerationMultiplier;

	public float m_minRange = 50f;

	public float m_rangeSpeedMultiplier;

	public float m_rangeAccelerationMultiplier = 50f;
}
