﻿using System;
using UnityEngine;

public class LightBeams02_lightanim : MonoBehaviour
{
	private void Update()
	{
		float num = Time.get_time() / this.duration * 2f * 3.14159274f;
		float intensity = Mathf.Cos(num) * this.intensity1 + this.intensity2;
		base.GetComponent<Light>().set_intensity(intensity);
	}

	public float duration = 10f;

	public float intensity1 = 3f;

	public float intensity2 = 3f;
}
