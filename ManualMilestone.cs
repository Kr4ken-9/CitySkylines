﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

[Serializable]
public class ManualMilestone : MilestoneInfo
{
	public override string GetLocalizedName()
	{
		if (Locale.Exists("UNLOCK_MILESTONE_TITLE", this.m_name))
		{
			return Locale.Get("UNLOCK_MILESTONE_TITLE", this.m_name);
		}
		if (this.m_prefab != null)
		{
			return this.m_prefab.GetLocalizedTitle();
		}
		return this.m_name;
	}

	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(targetCount, ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(targetCount, ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	public override GeneratedString GetDescriptionImpl(int targetCount)
	{
		string localeKey = this.m_type.ToString();
		GeneratedString generatedString;
		if (this.m_prefab != null)
		{
			generatedString = this.m_prefab.GetGeneratedTitle();
		}
		else if (this.m_type == ManualMilestone.Type.Win)
		{
			generatedString = new GeneratedString.Locale("SCENARIO_NAME", this.m_name);
		}
		else
		{
			generatedString = new GeneratedString.String("NULL");
		}
		if (targetCount > 1)
		{
			return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_MANUAL_NUM_DESC", localeKey), new GeneratedString[]
			{
				generatedString,
				new GeneratedString.Long((long)targetCount)
			});
		}
		return new GeneratedString.Format(new GeneratedString.Locale("MILESTONE_MANUAL_DESC", localeKey), new GeneratedString[]
		{
			generatedString
		});
	}

	private void GetProgressInfo(int targetCount, ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		string text = this.m_type.ToString();
		string text2;
		if (this.m_prefab != null)
		{
			text2 = this.m_prefab.GetLocalizedTitle();
		}
		else if (this.m_type == ManualMilestone.Type.Win)
		{
			text2 = Locale.Get("SCENARIO_NAME", this.m_name);
		}
		else
		{
			text2 = "NULL";
		}
		info.m_min = 0f;
		info.m_max = (float)targetCount;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount >= targetCount);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)targetCount)
		{
			num = (long)targetCount;
		}
		if (targetCount > 1)
		{
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_MANUAL_NUM_DESC", text), new object[]
			{
				text2,
				targetCount
			});
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_MANUAL_NUM_PROG", text), new object[]
			{
				num,
				targetCount
			});
		}
		else
		{
			info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_MANUAL_DESC", text), text2);
			info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_MANUAL_PROG", text), new object[]
			{
				num,
				targetCount
			});
		}
		if (info.m_prefabs == null)
		{
			info.m_prefabs = new FastList<PrefabInfo>();
		}
		else
		{
			info.m_prefabs.Clear();
		}
		if (this.m_prefab != null)
		{
			info.m_prefabs.Add(this.m_prefab);
		}
	}

	public override int GetComparisonValue()
	{
		return 0;
	}

	public void SetPrefab(PrefabInfo prefab)
	{
		if (this.m_prefab == null)
		{
			this.m_prefab = prefab;
		}
		else
		{
			BuildingInfo buildingInfo = this.m_prefab as BuildingInfo;
			if (buildingInfo != null && buildingInfo.m_placementStyle != ItemClass.Placement.Manual)
			{
				this.m_prefab = prefab;
			}
			NetInfo netInfo = this.m_prefab as NetInfo;
			if (netInfo != null && netInfo.m_placementStyle != ItemClass.Placement.Manual)
			{
				this.m_prefab = prefab;
			}
		}
	}

	public override void SetData(MilestoneInfo.Data data)
	{
		if (data is ManualMilestone.ManualData)
		{
			MilestoneInfo.Data data2 = this.GetData();
			data2.m_passedCount = (int)((ManualMilestone.ManualData)data).m_unlockCount;
			data2.m_progress = (long)data2.m_passedCount;
			this.m_written = true;
		}
		else
		{
			base.SetData(data);
		}
	}

	public void Unlock()
	{
		MilestoneInfo.Data data = this.GetData();
		data.m_progress += 1L;
		Singleton<UnlockManager>.get_instance().CheckMilestone(this, true, false);
	}

	public void Relock()
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_progress != 0L)
		{
			data.m_progress -= 1L;
		}
		Singleton<UnlockManager>.get_instance().CheckMilestone(this, false, true);
	}

	public void SetPassedCount(int count)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_progress != (long)count)
		{
			data.m_progress = (long)count;
			Singleton<UnlockManager>.get_instance().CheckMilestone(this, true, false);
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (forceUnlock && (data.m_progress != 0L || !this.m_canRelock))
		{
			data.m_passedCount = Mathf.Max(Mathf.Min((int)data.m_progress, this.m_maxPassCount), 1);
		}
		else if (forceRelock && this.m_canRelock)
		{
			data.m_passedCount = (int)data.m_progress;
		}
		return data;
	}

	public int GetPassCount()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0;
		}
		return data.m_passedCount;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		return 0f;
	}

	public ManualMilestone.Type m_type;

	[NonSerialized]
	public PrefabInfo m_prefab;

	public enum Type
	{
		Create,
		Full,
		Complete,
		Experience,
		Win
	}

	public class ManualData : MilestoneInfo.Data
	{
		public override void Serialize(DataSerializer s)
		{
			base.Serialize(s);
			s.WriteUInt32(this.m_unlockCount);
		}

		public override void Deserialize(DataSerializer s)
		{
			base.Deserialize(s);
			this.m_unlockCount = s.ReadUInt32();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
			base.AfterDeserialize(s);
		}

		public uint m_unlockCount;
	}
}
