﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class PublicTransportVehicleButton : UICustomControl
{
	public static CameraController cameraController
	{
		get
		{
			if (PublicTransportVehicleButton.m_CameraController == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					PublicTransportVehicleButton.m_CameraController = gameObject.GetComponent<CameraController>();
				}
			}
			return PublicTransportVehicleButton.m_CameraController;
		}
	}

	private void Awake()
	{
		base.get_component().add_eventMouseDown(new MouseEventHandler(this.OnMouseDown));
	}

	private void OnMouseDown(UIComponent component, UIMouseEventParameter eventParam)
	{
		UIButton uIButton = component as UIButton;
		ushort vehicle = (ushort)uIButton.get_objectUserData();
		InstanceID empty = InstanceID.Empty;
		empty.Vehicle = vehicle;
		Vector3 position;
		Quaternion quaternion;
		Vector3 vector;
		InstanceManager.GetPosition(empty, out position, out quaternion, out vector);
		if (PublicTransportVehicleButton.cameraController != null)
		{
			PublicTransportVehicleButton.cameraController.SetTarget(empty, position, false);
		}
		PublicTransportWorldInfoPanel.ResetScrollPosition();
		UIView.SetFocus(null);
	}

	private static CameraController m_CameraController;
}
