﻿using System;
using ColossalFramework.Packaging;

[Serializable]
public class SaveGameMetaData : MetaData
{
	public override string getEnvironment
	{
		get
		{
			return this.environment;
		}
	}

	public override string getPopulation
	{
		get
		{
			return this.population;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public string cityName;

	public string mapThemeRef;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;

	public bool achievementsDisabled;

	public ModInfo[] mods;

	public ModInfo[] assets;

	public string environment;

	public string population;

	public string cash;
}
