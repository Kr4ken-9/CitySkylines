﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class EffectItemMessage : EffectSubItem
{
	public TextFieldOverlay textFieldOverlay
	{
		get
		{
			if (this.m_textFieldOverlay == null)
			{
				this.m_textFieldOverlay = UIView.get_library().Get<TextFieldOverlay>("TextFieldOverlay");
			}
			return this.m_textFieldOverlay;
		}
	}

	private string currentText
	{
		get
		{
			MessageEffect messageEffect = base.currentTriggerEffect as MessageEffect;
			if (messageEffect != null)
			{
				return messageEffect.m_message;
			}
			ChirpEffect chirpEffect = base.currentTriggerEffect as ChirpEffect;
			if (chirpEffect != null)
			{
				return chirpEffect.m_message;
			}
			Debug.LogError("Could not get current text");
			return string.Empty;
		}
		set
		{
			this.m_messageText.set_text(value);
			MessageEffect messageEffect = base.currentTriggerEffect as MessageEffect;
			if (messageEffect != null)
			{
				messageEffect.m_message = value;
				return;
			}
			ChirpEffect chirpEffect = base.currentTriggerEffect as ChirpEffect;
			if (chirpEffect != null)
			{
				chirpEffect.m_message = value;
				return;
			}
			Debug.LogError("Could not set current text");
		}
	}

	private void Awake()
	{
		this.m_messageText = base.Find<UILabel>("MessageText");
		this.m_bgButton = base.Find<UIButton>("MessageBoxBackgroundButton");
		this.m_bgButton.add_eventMouseWheel(new MouseEventHandler(this.OnBGButtonMouseWheel));
		this.m_bgButton.add_eventClicked(new MouseEventHandler(this.OnBGButtonClicked));
		this.m_scrollablePanel = base.Find<UIScrollablePanel>("ScrollablePanel");
		this.m_chirpSprite = base.Find<UISprite>("SpriteChirp");
		this.m_messageSprite = base.Find<UISprite>("SpriteMessage");
	}

	private void OnBGButtonMouseWheel(UIComponent component, UIMouseEventParameter eventParam)
	{
		UIScrollablePanel expr_06 = this.m_scrollablePanel;
		expr_06.set_scrollPosition(expr_06.get_scrollPosition() - new Vector2(0f, eventParam.get_wheelDelta() * (float)this.m_scrollablePanel.get_scrollWheelAmount()));
	}

	protected override void ShowImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.Chirp)
		{
			this.m_chirpSprite.set_isVisible(true);
			this.m_messageSprite.set_isVisible(false);
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.StoryMessage)
		{
			this.m_chirpSprite.set_isVisible(false);
			this.m_messageSprite.set_isVisible(true);
		}
	}

	protected override void SetDefaultValuesImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.Chirp)
		{
			base.currentTriggerEffect = base.currentTriggerMilestone.AddEffect<ChirpEffect>();
			this.currentText = Locale.Get("TRIGGERPANEL_DEFAULTCHIRP");
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.StoryMessage)
		{
			base.currentTriggerEffect = base.currentTriggerMilestone.AddEffect<MessageEffect>();
			this.currentText = Locale.Get("TRIGGERPANEL_DEFAULTMESSAGE");
		}
	}

	public void Load(ChirpEffect chirpEffect)
	{
		base.currentTriggerEffect = chirpEffect;
		this.m_currentEffectType = EffectItem.EffectType.Chirp;
		this.m_messageText.set_text(chirpEffect.m_message);
		base.Show(EffectItem.EffectType.Chirp);
	}

	public void Load(MessageEffect messageEffect)
	{
		base.currentTriggerEffect = messageEffect;
		this.m_currentEffectType = EffectItem.EffectType.StoryMessage;
		this.m_messageText.set_text(messageEffect.m_message);
		base.Show(EffectItem.EffectType.StoryMessage);
	}

	protected override void HideImpl()
	{
		base.currentTriggerMilestone.RemoveEffect(base.currentTriggerEffect);
	}

	private void OnBGButtonClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		UIView.get_library().ShowModal("TextFieldOverlay");
		this.textFieldOverlay.ShowTextField(this, this.currentText);
		UIView.SetFocus(this.textFieldOverlay.Find<UITextField>("TextField"));
	}

	public void OnTextOverLaySubmitted(string text)
	{
		UIView.get_library().Hide("TextFieldOverlay");
		if (!string.IsNullOrEmpty(text))
		{
			this.currentText = text;
		}
	}

	private UISprite m_chirpSprite;

	private UISprite m_messageSprite;

	private UILabel m_messageText;

	private UIButton m_bgButton;

	private UIScrollablePanel m_scrollablePanel;

	private TextFieldOverlay m_textFieldOverlay;
}
