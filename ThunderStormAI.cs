﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ThunderStormAI : WeatherDisasterAI
{
	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector3 vector = data.m_targetPosition;
			float num = 200f;
			Vector3 vector2;
			vector2.x = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector2.y = 0f;
			vector2.z = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector += vector2 * num;
			float num2 = this.m_radius * (0.25f + (float)data.m_intensity * 0.0075f);
			float num3 = num2 * 0.5f;
			float num4 = num2 + num * 2f;
			float num5 = vector.x - num4;
			float num6 = vector.z - num4;
			float num7 = vector.x + num4;
			float num8 = vector.z + num4;
			int num9 = Mathf.Max((int)(num5 / 38.4f + 128f), 2);
			int num10 = Mathf.Max((int)(num6 / 38.4f + 128f), 2);
			int num11 = Mathf.Min((int)(num7 / 38.4f + 128f), 253);
			int num12 = Mathf.Min((int)(num8 / 38.4f + 128f), 253);
			WeatherManager instance = Singleton<WeatherManager>.get_instance();
			Vector3 vector3;
			vector3.y = 0f;
			for (int i = num10; i <= num12; i++)
			{
				vector3.z = ((float)i - 128f + 0.5f) * 38.4f - vector.z;
				for (int j = num9; j <= num11; j++)
				{
					vector3.x = ((float)j - 128f + 0.5f) * 38.4f - vector.x;
					float magnitude = vector3.get_magnitude();
					if (magnitude < num4)
					{
						float num13 = Mathf.Min(0.6f, 1f - (magnitude - num3) / (num4 - num3));
						num13 = Mathf.Min(1f, num13 + instance.GetWindSpeed(VectorUtils.XZ(vector3 + vector)) * 0.3f - 0.3f);
						if (num13 > 0f)
						{
							int num14 = i * 256 + j;
							map[num14] = (byte)(255 - (int)(255 - map[num14]) * (255 - Mathf.RoundToInt(num13 * 255f)) / 255);
						}
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			if (data.m_activationFrame != 0u)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				if (currentFrameIndex + 1755u >= data.m_activationFrame)
				{
					if ((data.m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
					{
						Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, false);
					}
					if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
					{
						Singleton<WeatherManager>.get_instance().m_forceWeatherOn = 2f;
						Singleton<WeatherManager>.get_instance().m_targetFog = 0f;
						Singleton<WeatherManager>.get_instance().m_targetRain = 1f;
						Singleton<WeatherManager>.get_instance().m_targetCloud = 1f;
					}
				}
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None && (data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			Singleton<WeatherManager>.get_instance().m_forceWeatherOn = 2f;
			Singleton<WeatherManager>.get_instance().m_targetFog = 0f;
			Singleton<WeatherManager>.get_instance().m_targetRain = 1f;
			Singleton<WeatherManager>.get_instance().m_targetCloud = 1f;
			uint currentFrameIndex2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			int num = 100;
			num = Mathf.Min(num, (int)(currentFrameIndex2 - data.m_activationFrame >> 3));
			num = Mathf.Min(num, (int)(data.m_activationFrame + this.m_activeDuration - currentFrameIndex2 >> 3));
			num = (num * (int)data.m_intensity + 50) / 100;
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed ^ (ulong)(currentFrameIndex2 >> 8));
			int num2 = randomizer.Int32(Mathf.Max(1, num / 20), Mathf.Max(1, 1 + num / 10));
			float num3 = this.m_radius * (0.25f + (float)data.m_intensity * 0.0075f);
			InstanceID id = default(InstanceID);
			id.Disaster = disasterID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
			for (int i = 0; i < num2; i++)
			{
				float num4 = (float)randomizer.Int32(10000u) * 0.0006283185f;
				float num5 = Mathf.Sqrt((float)randomizer.Int32(10000u) * 0.0001f) * num3;
				uint startFrame = currentFrameIndex2 + randomizer.UInt32(256u);
				Vector3 targetPosition = data.m_targetPosition;
				targetPosition.x += Mathf.Cos(num4) * num5;
				targetPosition.z += Mathf.Sin(num4) * num5;
				targetPosition.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(targetPosition, false, 0f);
				Quaternion quaternion = Quaternion.AngleAxis((float)randomizer.Int32(360u), Vector3.get_up());
				quaternion *= Quaternion.AngleAxis((float)randomizer.Int32(-15, 15), Vector3.get_right());
				Singleton<WeatherManager>.get_instance().QueueLightningStrike(startFrame, targetPosition, quaternion, group);
			}
		}
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(data.m_targetPosition);
			data.m_activationFrame = data.m_startFrame + this.m_emergingDuration;
			data.m_flags |= DisasterData.Flags.Significant;
			Singleton<DisasterManager>.get_instance().m_randomDisasterCooldown = 0;
		}
	}

	protected override void ActivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.ActivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().FollowDisaster(disasterID);
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
		{
			Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
		}
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			Singleton<WeatherManager>.get_instance().m_targetRain = 0f;
			Singleton<WeatherManager>.get_instance().m_targetCloud = 0f;
		}
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillEmerging(disasterID, ref data))
		{
			return true;
		}
		if (data.m_activationFrame != 0u)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex >= data.m_activationFrame)
			{
				return false;
			}
		}
		return true;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num < this.m_activeDuration;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return 1500 / (8 + ((int)num >> 10));
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		position = data.m_targetPosition;
		rotation = Quaternion.get_identity();
		size..ctor(this.m_radius, this.m_radius, this.m_radius);
		return true;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.WaterPower;
		return true;
	}

	public float m_radius = 2000f;

	public uint m_emergingDuration = 4096u;

	public uint m_activeDuration = 4096u;
}
