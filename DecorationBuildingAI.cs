﻿using System;
using ColossalFramework;
using UnityEngine;

public class DecorationBuildingAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(buildingID, ref data, infoMode);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		data.m_flags |= Building.Flags.Original;
		data.m_problems = Notification.Problem.None;
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		data.m_flags |= Building.Flags.Original;
		data.m_problems = Notification.Problem.None;
	}

	public override void SimulationStep(ushort buildingID, ref Building data)
	{
		base.SimulationStep(buildingID, ref data);
		if ((data.m_flags & Building.Flags.Demolishing) != Building.Flags.None)
		{
			Singleton<BuildingManager>.get_instance().ReleaseBuilding(buildingID);
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		if (demolish)
		{
			if (!testOnly)
			{
				data.m_flags |= Building.Flags.Demolishing;
			}
			return true;
		}
		return false;
	}

	public override void GetTerrainLimits(ushort buildingID, ref Building data, Vector3 meshPosition, out float min, out float max)
	{
		min = meshPosition.y - 255f;
		max = 1000000f;
		if (this.m_info.m_requireHeightMap && (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Roadside || this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnGround))
		{
			min = meshPosition.y - this.m_info.m_maxHeightOffset;
			max = meshPosition.y + this.m_info.m_maxHeightOffset;
		}
		if ((data.m_flags & Building.Flags.FixedHeight) != Building.Flags.None)
		{
			max = meshPosition.y;
		}
	}

	public override int GetConstructionCost()
	{
		int result = 1000;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override int GetRefundAmount(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public override bool AllowOverlap(BuildingInfo other)
	{
		return this.m_allowOverlap && other.m_class.m_service == ItemClass.Service.Beautification;
	}

	public bool m_allowOverlap = true;
}
