﻿using System;
using ColossalFramework.IO;

public struct DistrictSpecializationData
{
	public void Add(ref DistrictSpecializationData data)
	{
		this.m_tempHomeOrWorkCount += data.m_tempHomeOrWorkCount;
		this.m_tempAliveCount += data.m_tempAliveCount;
		this.m_tempEmptyCount += data.m_tempEmptyCount;
		this.m_tempBuildingArea += data.m_tempBuildingArea;
	}

	public void Update()
	{
		this.m_finalBuildingArea = this.m_tempBuildingArea;
		this.m_finalHomeOrWorkCount = this.m_tempHomeOrWorkCount;
		this.m_finalAliveCount = this.m_tempAliveCount;
		this.m_finalEmptyCount = this.m_tempEmptyCount;
	}

	public void Reset()
	{
		this.m_tempHomeOrWorkCount = 0u;
		this.m_tempAliveCount = 0u;
		this.m_tempEmptyCount = 0u;
		this.m_tempBuildingArea = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempHomeOrWorkCount);
		s.WriteUInt24(this.m_tempEmptyCount);
		s.WriteUInt24(this.m_finalHomeOrWorkCount);
		s.WriteUInt24(this.m_finalEmptyCount);
		s.WriteUInt24(this.m_tempBuildingArea);
		s.WriteUInt24(this.m_finalBuildingArea);
		s.WriteUInt24(this.m_tempAliveCount);
		s.WriteUInt24(this.m_finalAliveCount);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempHomeOrWorkCount = s.ReadUInt24();
		this.m_tempEmptyCount = s.ReadUInt24();
		this.m_finalHomeOrWorkCount = s.ReadUInt24();
		this.m_finalEmptyCount = s.ReadUInt24();
		this.m_tempBuildingArea = s.ReadUInt24();
		this.m_finalBuildingArea = s.ReadUInt24();
		this.m_tempAliveCount = s.ReadUInt24();
		this.m_finalAliveCount = s.ReadUInt24();
	}

	public uint m_tempHomeOrWorkCount;

	public uint m_tempAliveCount;

	public uint m_tempEmptyCount;

	public uint m_tempBuildingArea;

	public uint m_finalHomeOrWorkCount;

	public uint m_finalAliveCount;

	public uint m_finalEmptyCount;

	public uint m_finalBuildingArea;
}
