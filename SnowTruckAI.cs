﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class SnowTruckAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Snow)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.TransferToSource) == (Vehicle.Flags)0)
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_SNOW_RETURN");
				}
				if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
				{
					target = InstanceID.Empty;
					return Locale.Get("VEHICLE_STATUS_SNOW_UNLOAD");
				}
				if (data.m_targetBuilding != 0)
				{
					target = InstanceID.Empty;
					target.Building = data.m_targetBuilding;
					return Locale.Get("VEHICLE_STATUS_SNOW_TRANSFER");
				}
			}
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CONFUSED");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_SNOW_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_SNOW_WAIT");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_SNOW_COLLECT");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0 && data.m_transferSize != 0)
		{
			int transferSize = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			if (info != null)
			{
				info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
			}
		}
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0 && data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0 && data.m_targetBuilding != 0 && this.CheckTargetSegment(vehicleID, ref data))
		{
			NetSegment[] expr_40_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
			ushort expr_40_cp_1 = data.m_targetBuilding;
			expr_40_cp_0[(int)expr_40_cp_1].m_flags = (expr_40_cp_0[(int)expr_40_cp_1].m_flags | NetSegment.Flags.AccessFailed);
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				int num = Mathf.Min(0, (int)data.m_transferSize - this.m_cargoCapacity);
				info.m_buildingAI.ModifyMaterialBuffer(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
				num = Mathf.Max(0, -num);
				data.m_transferSize += (ushort)num;
			}
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
			if ((instance.m_buildings.m_buffer[(int)sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Importing;
				}
				else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Exporting;
				}
			}
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding == data.m_targetBuilding)
		{
			if (data.m_path == 0u)
			{
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			else
			{
				this.TrySpawn(vehicleID, ref data);
			}
		}
		else
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0 && targetBuilding != 0 && !this.CheckTargetSegment(vehicleID, ref data))
			{
				targetBuilding = 0;
				data.m_targetBuilding = 0;
			}
			if (targetBuilding != 0)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
					if ((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
					{
						if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
						{
							data.m_flags |= Vehicle.Flags.Exporting;
						}
						else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
						{
							data.m_flags |= Vehicle.Flags.Importing;
						}
					}
				}
			}
			else
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					if (data.m_transferSize > 0)
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = 7;
						offer.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer.Position = data.GetLastFramePosition();
						}
						offer.Amount = 1;
						offer.Active = true;
						Singleton<TransferManager>.get_instance().AddOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
				if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					if ((int)data.m_transferSize < this.m_cargoCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
					{
						TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
						offer2.Priority = 7;
						offer2.Vehicle = vehicleID;
						if (data.m_sourceBuilding != 0)
						{
							offer2.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
						}
						else
						{
							offer2.Position = data.GetLastFramePosition();
						}
						offer2.Amount = 1;
						offer2.Active = true;
						Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer2);
						data.m_flags |= Vehicle.Flags.WaitingTarget;
					}
					else
					{
						data.m_flags |= Vehicle.Flags.GoingBack;
					}
				}
			}
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0 && building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					this.SetTarget(vehicleID, ref data, offer.Building);
				}
				else
				{
					this.SetTarget(vehicleID, ref data, offer.NetSegment);
				}
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
			else if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<TransferManager>.get_instance().RemoveOutgoingOffer((TransferManager.TransferReason)data.m_transferType, offer);
			}
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			}
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			int transferSize = (int)data.m_transferSize;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)data.m_targetBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		int num = 0;
		if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			num = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref num);
			data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - num, 0, (int)data.m_transferSize);
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	protected override bool LeftHandDrive(NetInfo.Lane lane)
	{
		return lane.m_position < 0f == ((byte)(lane.m_finalDirection & NetInfo.Direction.Backward) == 0);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		bool flag = false;
		if ((vehicleData.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
		{
			if ((int)vehicleData.m_transferSize < this.m_cargoCapacity)
			{
				flag = this.TryCollectSnow(vehicleID, ref vehicleData);
			}
			if ((int)vehicleData.m_transferSize >= this.m_cargoCapacity && (vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0)
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
			else if ((vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0 && !this.CheckTargetSegment(vehicleID, ref vehicleData))
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
		if (flag)
		{
			vehicleData.m_flags |= Vehicle.Flags.Emergency1;
		}
		else
		{
			vehicleData.m_flags &= ~Vehicle.Flags.Emergency1;
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & (Vehicle.Flags.TransferToSource | Vehicle.Flags.GoingBack)) == Vehicle.Flags.TransferToSource && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_productionRate == 0 || (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None) && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && leaderData.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 targetPos;
			info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
			return;
		}
	}

	private bool TryCollectSnow(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) == Vehicle.Flags.Underground)
		{
			return false;
		}
		if (vehicleData.m_path != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = vehicleData.m_pathPositionIndex;
			if (b == 255)
			{
				b = 0;
			}
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position position;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(b >> 1, out position))
			{
				return this.TryCollectSnow(vehicleID, ref vehicleData, position.m_segment, ref instance.m_segments.m_buffer[(int)position.m_segment], false);
			}
		}
		return false;
	}

	private bool TryCollectSnow(ushort vehicleID, ref Vehicle vehicleData, ushort segmentID, ref NetSegment segment, bool collectAll)
	{
		int num = this.m_cargoCapacity - (int)vehicleData.m_transferSize;
		if (num <= 0)
		{
			return false;
		}
		int wetness = (int)segment.m_wetness;
		int num2 = Mathf.CeilToInt((float)wetness * segment.m_averageLength * 0.01f);
		num = Mathf.Min(num, num2);
		if (num <= 0)
		{
			return wetness > 16;
		}
		if (!collectAll)
		{
			float magnitude = vehicleData.GetLastFrameVelocity().get_magnitude();
			num = Mathf.Min(num, Mathf.CeilToInt((float)wetness * magnitude * 0.01f));
			if (num <= 0)
			{
				return wetness > 16;
			}
		}
		num2 -= num;
		segment.m_wetness = (byte)Mathf.FloorToInt((float)num2 / (segment.m_averageLength * 0.01f));
		vehicleData.m_transferSize += (ushort)num;
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.SnowCollected);
		statisticBase.Add(num);
		NetManager instance = Singleton<NetManager>.get_instance();
		InstanceID empty = InstanceID.Empty;
		empty.NetSegment = segmentID;
		instance.AddSmoothColor(empty);
		empty.NetNode = segment.m_startNode;
		instance.AddSmoothColor(empty);
		empty.NetNode = segment.m_endNode;
		instance.AddSmoothColor(empty);
		return wetness > 16;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		base.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position, laneID, offset, prevPos, prevLaneID, prevOffset, index, out pos, out dir, out maxSpeed);
		if (index <= 0 && prevPos.m_segment != 0)
		{
			this.TryCollectSnow(vehicleID, ref vehicleData, prevPos.m_segment, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)prevPos.m_segment], true);
		}
	}

	private bool CheckTargetSegment(ushort vehicleID, ref Vehicle vehicleData)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		return vehicleData.m_targetBuilding < 36864 && (instance.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created && instance.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_wetness >= 64;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_targetBuilding != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
				Randomizer randomizer2;
				randomizer2..ctor((int)vehicleID);
				Vector3 vector2;
				Vector3 endPos2;
				info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
			}
		}
		else if (vehicleData.m_targetBuilding != 0 && this.CheckTargetSegment(vehicleID, ref vehicleData))
		{
			NetManager instance3 = Singleton<NetManager>.get_instance();
			NetInfo info3 = instance3.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			if (info3.m_lanes == null)
			{
				return false;
			}
			int num = 0;
			for (int i = 0; i < info3.m_lanes.Length; i++)
			{
				if (info3.m_lanes[i].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car))
				{
					num++;
				}
			}
			if (num == 0)
			{
				return false;
			}
			Vector3 endPos3 = Vector3.get_zero();
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num);
			uint num2 = instance3.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_lanes;
			int num3 = 0;
			while (num3 < info3.m_lanes.Length && num2 != 0u)
			{
				if (info3.m_lanes[num3].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car) && num-- == 0)
				{
					endPos3 = instance3.m_lanes.m_buffer[(int)((UIntPtr)num2)].CalculatePosition(0.5f);
					break;
				}
				num2 = instance3.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
				num3++;
			}
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos3, true, true, true);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		else
		{
			result.NetSegment = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public override int GetNoiseLevel()
	{
		return 10;
	}

	[CustomizableProperty("Cargo capacity")]
	public int m_cargoCapacity = 1;
}
