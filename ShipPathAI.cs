﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ShipPathAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ship)
		{
			radius = 100f;
			capped = false;
			color = Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		else
		{
			base.GetEffectRadius(out radius, out capped, out color);
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Transport;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
	}

	public override void SegmentLoaded(ushort segmentID, ref NetSegment data)
	{
		base.SegmentLoaded(segmentID, ref data);
		Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
	}

	public override void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
		Singleton<NetManager>.get_instance().RemoveServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, false);
		base.ReleaseSegment(segmentID, ref data);
	}

	public override void CreateNode(ushort nodeID, ref NetNode data)
	{
		base.CreateNode(nodeID, ref data);
		data.m_flags |= NetNode.Flags.DisableOnlyMiddle;
	}

	public override void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
		base.NodeLoaded(nodeID, ref data, version);
		data.m_flags |= NetNode.Flags.DisableOnlyMiddle;
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ship)
		{
			Singleton<NetManager>.get_instance().AddTileNode(data.m_position, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		}
	}

	public override void TrafficDirectionUpdated(ushort nodeID, ref NetNode data)
	{
		base.TrafficDirectionUpdated(nodeID, ref data);
		this.UpdateOutsideFlags(nodeID, ref data);
	}

	public override void AfterTerrainUpdate(ushort nodeID, ref NetNode data)
	{
		base.AfterTerrainUpdate(nodeID, ref data);
		this.CheckHeight(nodeID, ref data);
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (incoming || outgoing)
		{
			int num;
			int num2;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, out num, out num2);
			if ((incoming && num >= 4) || (outgoing && num2 >= 4))
			{
				errors |= ToolBase.ToolErrors.TooManyConnections;
			}
			if (this.m_connectedInfo != null)
			{
				return this.m_connectedInfo;
			}
		}
		return this.m_info;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth;
	}

	public override bool DisplayTempSegment()
	{
		return true;
	}

	public override bool BuildOnWater()
	{
		return true;
	}

	public override ItemClass.CollisionType GetCollisionType()
	{
		return ItemClass.CollisionType.Terrain;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			building = this.m_outsideConnection;
			heightOffset = 0f;
		}
		else
		{
			base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
		}
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		this.CheckHeight(nodeID, ref data);
	}

	private void CheckHeight(ushort nodeID, ref NetNode data)
	{
		float num = 8607f;
		Vector3 position = data.m_position;
		if (position.x < -num)
		{
			position.x = -num;
		}
		if (position.x > num)
		{
			position.x = num;
		}
		if (position.z < -num)
		{
			position.z = -num;
		}
		if (position.z > num)
		{
			position.z = num;
		}
		float num2 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(position, false, 0f);
		if (Mathf.Abs(num2 - data.m_position.y) > 7f)
		{
			position = data.m_position;
			position.y = num2;
			Singleton<NetManager>.get_instance().MoveNode(nodeID, position);
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
		int num = 0;
		int num2 = 0;
		data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.None, true, ref num, ref num2);
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_59_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_59_cp_1 = data.m_building;
				expr_59_cp_0[(int)expr_59_cp_1].m_flags = (expr_59_cp_0[(int)expr_59_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_85_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_85_cp_1 = data.m_building;
				expr_85_cp_0[(int)expr_85_cp_1].m_flags = (expr_85_cp_0[(int)expr_85_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_B2_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_B2_cp_1 = data.m_building;
				expr_B2_cp_0[(int)expr_B2_cp_1].m_flags = (expr_B2_cp_0[(int)expr_B2_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_DB_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_DB_cp_1 = data.m_building;
				expr_DB_cp_0[(int)expr_DB_cp_1].m_flags = (expr_DB_cp_0[(int)expr_DB_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
	}

	private void UpdateOutsideFlags(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Outside) != NetNode.Flags.None && data.m_building != 0)
		{
			int num = 0;
			int num2 = 0;
			data.CountLanes(nodeID, 0, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.None, true, ref num, ref num2);
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (num != 0)
			{
				Building[] expr_51_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_51_cp_1 = data.m_building;
				expr_51_cp_0[(int)expr_51_cp_1].m_flags = (expr_51_cp_0[(int)expr_51_cp_1].m_flags | Building.Flags.Outgoing);
			}
			else
			{
				Building[] expr_7D_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_7D_cp_1 = data.m_building;
				expr_7D_cp_0[(int)expr_7D_cp_1].m_flags = (expr_7D_cp_0[(int)expr_7D_cp_1].m_flags & ~Building.Flags.Outgoing);
			}
			if (num2 != 0)
			{
				Building[] expr_AA_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_AA_cp_1 = data.m_building;
				expr_AA_cp_0[(int)expr_AA_cp_1].m_flags = (expr_AA_cp_0[(int)expr_AA_cp_1].m_flags | Building.Flags.Incoming);
			}
			else
			{
				Building[] expr_D3_cp_0 = instance.m_buildings.m_buffer;
				ushort expr_D3_cp_1 = data.m_building;
				expr_D3_cp_0[(int)expr_D3_cp_1].m_flags = (expr_D3_cp_0[(int)expr_D3_cp_1].m_flags & ~Building.Flags.Incoming);
			}
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		Vector3 vector;
		Vector3 vector2;
		NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, true, true, out vector, out vector2);
		Bezier2 bezier;
		bezier.a = VectorUtils.XZ(startPoint.m_position);
		bezier.b = VectorUtils.XZ(vector);
		bezier.c = VectorUtils.XZ(vector2);
		bezier.d = VectorUtils.XZ(endPoint.m_position);
		int num = Mathf.CeilToInt(Vector2.Distance(bezier.a, bezier.d) * 0.005f);
		Segment2 segment;
		segment.a = bezier.a;
		float radius = this.m_info.m_halfWidth;
		if (this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ship)
		{
			radius = 100f;
		}
		for (int i = 1; i <= num; i++)
		{
			segment.b = bezier.Position((float)i / (float)num);
			if (!Singleton<TerrainManager>.get_instance().HasWater(segment, radius, false))
			{
				toolErrors |= ToolBase.ToolErrors.WaterNotFound;
			}
			segment.a = segment.b;
		}
		return toolErrors;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public NetInfo m_connectedInfo;

	public BuildingInfo m_outsideConnection;

	public TransportInfo m_transportInfo;
}
