﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ParticleEffect : EffectInfo
{
	private void Awake()
	{
	}

	public override void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
		Vector3 point = area.m_matrix.MultiplyPoint(Vector3.get_zero());
		if (cameraInfo.CheckRenderDistance(point, this.m_maxVisibilityDistance) && cameraInfo.Intersect(point, 100f))
		{
			Vector3 vector;
			vector..ctor(100000f, 100000f, 100000f);
			Vector3 vector2;
			vector2..ctor(-100000f, -100000f, -100000f);
			float particlesPerSquare;
			if (timeOffset >= 0f)
			{
				if (this.m_renderDuration != 0f)
				{
					if (timeOffset > this.m_renderDuration)
					{
						return;
					}
					magnitude *= this.m_intensityCurve.Evaluate(timeOffset / this.m_renderDuration);
					particlesPerSquare = timeDelta * magnitude * 0.01f;
				}
				else
				{
					if (timeOffset != 0f)
					{
						return;
					}
					particlesPerSquare = magnitude * 0.01f;
				}
			}
			else
			{
				particlesPerSquare = timeDelta * magnitude * 0.01f;
			}
			this.EmitParticles(id, area, velocity, particlesPerSquare, 100, ref vector, ref vector2);
		}
	}

	public void EmitParticles(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float particlesPerSquare, int probability, ref Vector3 min, ref Vector3 max)
	{
		int particleCount = this.m_particleSystem.get_particleCount();
		float num = Mathf.Clamp01((float)particleCount / (float)Mathf.Max(this.m_particleSystem.get_main().get_maxParticles(), 1));
		particlesPerSquare *= 1f - num * num;
		if (this.m_canUseMeshData && area.m_meshData != null)
		{
			this.EmitParticles(id, area.m_matrix, area.m_meshData, velocity, particlesPerSquare, probability, ref min, ref max);
		}
		else if (this.m_canUsePositions && area.m_positions != null)
		{
			this.EmitParticles(id, area.m_matrix, area.m_positions, velocity, particlesPerSquare, probability, ref min, ref max);
		}
		else if (this.m_canUseBezier && area.m_bezier.d != area.m_bezier.a)
		{
			this.EmitParticles(id, area.m_bezier, area.m_halfWidth, area.m_halfHeight, velocity, particlesPerSquare, probability, ref min, ref max);
		}
		else
		{
			Vector3 point = area.m_matrix.MultiplyPoint(Vector3.get_zero());
			Vector3 direction = area.m_matrix.MultiplyVector(Vector3.get_forward());
			this.EmitParticles(id, point, direction, area.m_halfWidth + this.m_extraRadius, area.m_halfHeight, velocity, particlesPerSquare, probability, ref min, ref max);
		}
	}

	public void EmitParticles(InstanceID id, Matrix4x4 matrix, RenderGroup.MeshData meshData, Vector3 velocity, float particlesPerSquare, int probability, ref Vector3 min, ref Vector3 max)
	{
		Randomizer randomizer;
		randomizer..ctor(id.RawData);
		particlesPerSquare *= this.m_particleSystem.get_emission().get_rateOverTime().get_constant();
		Vector3[] vertices = meshData.m_vertices;
		int[] triangles = meshData.m_triangles;
		if (triangles != null)
		{
			int num = triangles.Length;
			for (int i = 0; i < num; i += 3)
			{
				if (randomizer.Int32(100u) <= probability)
				{
					Vector3 vector = matrix.MultiplyPoint(vertices[triangles[i]]);
					Vector3 vector2 = matrix.MultiplyPoint(vertices[triangles[i + 1]]);
					Vector3 vector3 = matrix.MultiplyPoint(vertices[triangles[i + 2]]);
					float num2 = Triangle3.Area(vector, vector2, vector3);
					int num3 = Mathf.FloorToInt(num2 * particlesPerSquare + Random.get_value());
					for (int j = 0; j < num3; j++)
					{
						ParticleSystem.EmitParams emitParams = default(ParticleSystem.EmitParams);
						float value = Random.get_value();
						float num4 = (1f - value) * Random.get_value();
						emitParams.set_position(vector + (vector2 - vector) * value + (vector3 - vector) * num4);
						emitParams.set_startSize(this.m_particleSystem.get_main().get_startSize().get_constant());
						emitParams.set_startLifetime(this.m_minLifeTime + (this.m_maxLifeTime - this.m_minLifeTime) * Random.get_value());
						float num5 = this.m_minStartSpeed + (this.m_maxStartSpeed - this.m_minStartSpeed) * Random.get_value();
						emitParams.set_startColor(this.m_particleSystem.get_main().get_startColor().get_color());
						emitParams.set_velocity(velocity + Vector3.Cross(vector2 - vector, vector3 - vector).get_normalized() * num5);
						this.m_particleSystem.Emit(emitParams, 1);
					}
					min = Vector3.Min(min, vector);
					max = Vector3.Max(max, vector);
					min = Vector3.Min(min, vector2);
					max = Vector3.Max(max, vector2);
					min = Vector3.Min(min, vector3);
					max = Vector3.Max(max, vector3);
				}
			}
		}
	}

	public void EmitParticles(InstanceID id, Matrix4x4 matrix, Vector4[] positions, Vector3 velocity, float particlesPerSquare, int probability, ref Vector3 min, ref Vector3 max)
	{
		Randomizer randomizer;
		randomizer..ctor(id.RawData);
		particlesPerSquare *= this.m_particleSystem.get_emission().get_rateOverTime().get_constant();
		int num = positions.Length;
		for (int i = 0; i < num; i++)
		{
			if (randomizer.Int32(100u) <= probability)
			{
				Vector3 vector = matrix.MultiplyPoint(positions[i]);
				float w = positions[i].w;
				Vector3 vector2 = matrix.MultiplyVector(Vector3.get_right());
				Vector3 perpedicular = ParticleEffect.GetPerpedicular(vector2);
				Vector3 vector3 = Vector3.Cross(vector2, perpedicular);
				float num2 = Mathf.Max(100f, 3.14159274f * w * w);
				int num3 = Mathf.FloorToInt(num2 * particlesPerSquare + Random.get_value());
				for (int j = 0; j < num3; j++)
				{
					ParticleSystem.EmitParams emitParams = default(ParticleSystem.EmitParams);
					float num4 = Random.get_value() * 6.28318548f;
					float num5 = Mathf.Sqrt(Random.get_value());
					float num6 = Mathf.Cos(num4);
					float num7 = Mathf.Sin(num4);
					emitParams.set_position(vector + perpedicular * (num6 * num5 * w) + vector3 * (num7 * num5 * w));
					emitParams.set_startSize(this.m_particleSystem.get_main().get_startSize().get_constant());
					emitParams.set_startLifetime(this.m_minLifeTime + (this.m_maxLifeTime - this.m_minLifeTime) * Random.get_value());
					float num8 = this.m_minStartSpeed + (this.m_maxStartSpeed - this.m_minStartSpeed) * Random.get_value();
					float num9 = this.m_minSpawnAngle + (this.m_maxSpawnAngle - this.m_minSpawnAngle) * Random.get_value();
					float num10 = Mathf.Cos(num9 * 0.0174532924f);
					float num11 = Mathf.Sin(num9 * 0.0174532924f);
					emitParams.set_startColor(this.m_particleSystem.get_main().get_startColor().get_color());
					Vector3 vector4 = vector2 * num10 + perpedicular * (num6 * num11) + vector3 * (num7 * num11);
					emitParams.set_velocity(velocity + vector4 * num8);
					this.m_particleSystem.Emit(emitParams, 1);
				}
				min = Vector3.Min(min, vector - new Vector3(w, w, w));
				max = Vector3.Max(max, vector + new Vector3(w, w, w));
			}
		}
	}

	public void EmitParticles(InstanceID id, Bezier3 bezier, float halfWidth, float halfHeight, Vector3 velocity, float particlesPerSquare, int probability, ref Vector3 min, ref Vector3 max)
	{
		Randomizer randomizer;
		randomizer..ctor(id.RawData);
		particlesPerSquare *= this.m_particleSystem.get_emission().get_rateOverTime().get_constant();
		Vector3 vector = bezier.a;
		Vector3 vector2;
		vector2..ctor(bezier.b.z - bezier.a.z, 0f, bezier.a.x - bezier.b.x);
		Vector3 vector3 = vector2.get_normalized();
		for (int i = 1; i <= 32; i++)
		{
			float num = (float)i / 32f;
			Vector3 vector4 = bezier.Position(num);
			Vector3 vector5 = bezier.Tangent(num);
			Vector3 vector6;
			vector6..ctor(vector5.z, 0f, -vector5.x);
			vector5 = vector6.get_normalized() * halfWidth;
			if (randomizer.Int32(100u) <= probability)
			{
				float num2 = halfWidth * 2f * Vector3.Distance(vector, vector4);
				int num3 = Mathf.FloorToInt(num2 * particlesPerSquare + Random.get_value());
				for (int j = 0; j < num3; j++)
				{
					ParticleSystem.EmitParams emitParams = default(ParticleSystem.EmitParams);
					float num4 = Random.get_value() * 2f - 1f;
					float value = Random.get_value();
					Vector3 vector7 = vector + vector3 * num4;
					Vector3 vector8 = vector4 + vector5 * num4;
					emitParams.set_position(vector7 + (vector8 - vector7) * value);
					emitParams.set_startSize(this.m_particleSystem.get_main().get_startSize().get_constant());
					emitParams.set_startLifetime(this.m_minLifeTime + (this.m_maxLifeTime - this.m_minLifeTime) * Random.get_value());
					float num5 = this.m_minStartSpeed + (this.m_maxStartSpeed - this.m_minStartSpeed) * Random.get_value();
					emitParams.set_startColor(this.m_particleSystem.get_main().get_startColor().get_color());
					emitParams.set_velocity(velocity + new Vector3(0f, num5, 0f));
					this.m_particleSystem.Emit(emitParams, 1);
				}
				min = Vector3.Min(min, vector4);
				max = Vector3.Max(max, vector4);
				min = Vector3.Min(min, vector4 + vector5);
				max = Vector3.Max(max, vector4 + vector5);
				min = Vector3.Min(min, vector4 - vector5);
				max = Vector3.Max(max, vector4 - vector5);
			}
			vector = vector4;
			vector3 = vector5;
		}
	}

	public void EmitParticles(InstanceID id, Vector3 point, Vector3 direction, float radius, float halfHeight, Vector3 velocity, float particlesPerSquare, int probability, ref Vector3 min, ref Vector3 max)
	{
		Randomizer randomizer;
		randomizer..ctor(id.RawData);
		particlesPerSquare *= this.m_particleSystem.get_emission().get_rateOverTime().get_constant();
		if (randomizer.Int32(100u) <= probability)
		{
			Vector3 perpedicular = ParticleEffect.GetPerpedicular(direction);
			Vector3 vector = Vector3.Cross(direction, perpedicular);
			float num = Mathf.Max(100f, 3.14159274f * radius * radius);
			int num2 = Mathf.FloorToInt(num * particlesPerSquare + Random.get_value());
			for (int i = 0; i < num2; i++)
			{
				ParticleSystem.EmitParams emitParams = default(ParticleSystem.EmitParams);
				float num3 = Random.get_value() * 6.28318548f;
				float num4 = Mathf.Sqrt(Random.get_value());
				float num5 = Mathf.Cos(num3);
				float num6 = Mathf.Sin(num3);
				emitParams.set_position(point + perpedicular * (num5 * num4 * radius) + vector * (num6 * num4 * radius) + direction * (halfHeight * Random.get_value()));
				emitParams.set_startSize(this.m_particleSystem.get_main().get_startSize().get_constant());
				emitParams.set_startLifetime(this.m_minLifeTime + (this.m_maxLifeTime - this.m_minLifeTime) * Random.get_value());
				float num7 = this.m_minStartSpeed + (this.m_maxStartSpeed - this.m_minStartSpeed) * Random.get_value();
				float num8 = this.m_minSpawnAngle + (this.m_maxSpawnAngle - this.m_minSpawnAngle) * Random.get_value();
				float num9 = Mathf.Cos(num8 * 0.0174532924f);
				float num10 = Mathf.Sin(num8 * 0.0174532924f);
				emitParams.set_startColor(this.m_particleSystem.get_main().get_startColor().get_color());
				Vector3 vector2 = direction * num9 + perpedicular * (num5 * num10) + vector * (num6 * num10);
				emitParams.set_velocity(velocity + vector2 * num7);
				this.m_particleSystem.Emit(emitParams, 1);
			}
			min = Vector3.Min(min, point - new Vector3(radius, radius, radius));
			max = Vector3.Max(max, point + new Vector3(radius, radius, radius));
		}
	}

	private static Vector3 GetPerpedicular(Vector3 v)
	{
		if (v.x != 0f || v.y != 0f)
		{
			return new Vector3(-v.y, v.x, 0f);
		}
		if (v.z == 0f)
		{
			return Vector3.get_zero();
		}
		return new Vector3(0f, 1f, 0f);
	}

	private void Update()
	{
		if (this.m_useSimulationTime && this.m_particleSystems != null)
		{
			float simulationTimeSpeed = Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed;
			for (int i = 0; i < this.m_particleSystems.Length; i++)
			{
				if (simulationTimeSpeed < 1E-05f)
				{
					this.m_particleSystems[i].Pause(false);
				}
				else
				{
					this.m_particleSystems[i].get_main().set_simulationSpeed(simulationTimeSpeed);
					if (this.m_particleSystems[i].get_isPaused())
					{
						this.m_particleSystems[i].Play(false);
					}
				}
			}
		}
	}

	protected override void CreateEffect()
	{
		base.CreateEffect();
		if (this.m_effectObject == null)
		{
			Transform effectRoot = ParticleEffect.GetEffectRoot();
			GameObject gameObject = Object.Instantiate<GameObject>(base.get_gameObject());
			gameObject.get_transform().set_parent(effectRoot);
			gameObject.set_name(base.get_gameObject().get_name());
			this.m_effectObject = gameObject;
			this.m_effectComponent = gameObject.GetComponent<ParticleEffect>();
			this.m_particleSystem = gameObject.GetComponent<ParticleSystem>();
			this.m_particleSystem.get_emission().set_enabled(false);
			this.m_particleSystems = gameObject.GetComponentsInChildren<ParticleSystem>();
			this.m_effectComponent.m_particleSystem = this.m_particleSystem;
			this.m_effectComponent.m_particleSystems = this.m_particleSystems;
		}
	}

	protected override void DestroyEffect()
	{
		this.m_particleSystem = null;
		this.m_particleSystems = null;
		if (this.m_effectObject != null)
		{
			Object.Destroy(this.m_effectObject);
			this.m_effectObject = null;
		}
		base.DestroyEffect();
	}

	private static Transform GetEffectRoot()
	{
		GameObject gameObject = GameObject.Find("Particle Effects");
		if (gameObject == null)
		{
			gameObject = new GameObject("Particle Effects");
			Object.DontDestroyOnLoad(gameObject);
		}
		return gameObject.get_transform();
	}

	public static void ClearAll()
	{
		GameObject gameObject = GameObject.Find("Particle Effects");
		if (gameObject != null)
		{
			ParticleSystem[] componentsInChildren = gameObject.GetComponentsInChildren<ParticleSystem>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].Clear(false);
			}
		}
	}

	public override bool RequireRender()
	{
		return true;
	}

	public override float RenderDuration()
	{
		return this.m_renderDuration;
	}

	public bool m_useSimulationTime = true;

	public bool m_canUseMeshData = true;

	public bool m_canUsePositions = true;

	public bool m_canUseBezier = true;

	public float m_maxVisibilityDistance = 1000f;

	public float m_minLifeTime = 1f;

	public float m_maxLifeTime = 5f;

	public float m_minStartSpeed;

	public float m_maxStartSpeed;

	public float m_minSpawnAngle;

	public float m_maxSpawnAngle = 90f;

	public float m_renderDuration;

	public float m_extraRadius;

	public AnimationCurve m_intensityCurve;

	[NonSerialized]
	protected GameObject m_effectObject;

	[NonSerialized]
	protected ParticleEffect m_effectComponent;

	[NonSerialized]
	protected ParticleSystem m_particleSystem;

	[NonSerialized]
	protected ParticleSystem[] m_particleSystems;
}
