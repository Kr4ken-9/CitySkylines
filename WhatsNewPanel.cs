﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class WhatsNewPanel : UICustomControl
{
	private void Awake()
	{
		this.m_tabstrip = base.Find<UITabstrip>("Tabstrip");
		this.m_tabcontainer = base.Find<UITabContainer>("TabContainer");
		this.m_tabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTabstripIndexChanged));
		base.get_component().add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnVisChanged));
	}

	private void OnVisChanged(UIComponent component, bool visible)
	{
		if (visible)
		{
			this.HideDLCSlides();
		}
	}

	private void OnTabstripIndexChanged(UIComponent component, int value)
	{
		ValueAnimator.Animate("WhatsNewPanelContainerFadeIn", delegate(float val)
		{
			this.m_tabcontainer.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.3f), delegate
		{
			this.m_tabcontainer.set_opacity(1f);
		});
	}

	private void HideDLCSlides()
	{
		this.m_tabstrip.set_selectedIndex(-1);
		int num = 0;
		float width = this.m_tabstrip.get_components()[0].get_width();
		for (int i = this.m_tabcontainer.get_components().Count - 1; i >= 0; i--)
		{
			WhatsNewPanelSlide component = this.m_tabcontainer.get_components()[i].GetComponent<WhatsNewPanelSlide>();
			if (component != null)
			{
				if (!component.shouldShow)
				{
					this.m_tabcontainer.RemoveUIComponent(this.m_tabcontainer.get_components()[i]);
					this.m_tabstrip.RemoveUIComponent(this.m_tabstrip.get_components()[i]);
				}
				else
				{
					num++;
				}
			}
			else
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Missing WhatsNewPanelSlide script.");
			}
		}
		this.m_tabstrip.set_width(width * (float)num);
		this.m_tabstrip.set_selectedIndex(0);
		if (num < 2)
		{
			this.m_tabstrip.Hide();
		}
	}

	public void SelectNextTab()
	{
		if (this.m_tabstrip.get_selectedIndex() >= this.m_tabstrip.get_tabCount() - 1)
		{
			this.m_tabstrip.set_selectedIndex(0);
		}
		else
		{
			UITabstrip expr_34 = this.m_tabstrip;
			expr_34.set_selectedIndex(expr_34.get_selectedIndex() + 1);
		}
		base.get_component().Focus();
	}

	public void SelectPreviousTab()
	{
		if (this.m_tabstrip.get_selectedIndex() <= 0)
		{
			this.m_tabstrip.set_selectedIndex(this.m_tabstrip.get_tabCount() - 1);
		}
		else
		{
			UITabstrip expr_34 = this.m_tabstrip;
			expr_34.set_selectedIndex(expr_34.get_selectedIndex() - 1);
		}
		base.get_component().Focus();
	}

	private UITabstrip m_tabstrip;

	private UITabContainer m_tabcontainer;
}
