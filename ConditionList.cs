﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class ConditionList : TriggerPanelList
{
	private void Awake()
	{
		this.m_container = base.Find<UIScrollablePanel>("ScrollablePanel");
		this.m_conditionItems = new UITemplateList<ConditionItem>(this.m_container, "ConditionItem");
	}

	private void RefreshOptionList()
	{
		List<ConditionList.Option> list = new List<ConditionList.Option>();
		this.m_timeLimitOption = new ConditionList.Option(ConditionList.ConditionType.TimeLimit);
		list.Add(this.m_timeLimitOption);
		this.m_triggerOption = new ConditionList.Option(ConditionList.ConditionType.Trigger);
		this.m_triggerOption.m_availableTriggers = this.GetAvailableTriggers();
		list.Add(this.m_triggerOption);
		this.m_gameAreaOption = new ConditionList.Option(ConditionList.ConditionType.GameArea);
		list.Add(this.m_gameAreaOption);
		this.m_buildingsOption = new ConditionList.Option(ConditionList.ConditionType.BuildingMilestone);
		this.m_buildingsOption.m_availableBuildingMilestones = MilestoneCollection.GetManualMilestones(ManualMilestone.Type.Create);
		list.Add(this.m_buildingsOption);
		for (int i = 0; i < this.m_presets.m_statisticPresets.Length; i++)
		{
			list.Add(new ConditionList.Option(ConditionList.ConditionType.Preset)
			{
				m_statisticsPreset = this.m_presets.m_statisticPresets[i]
			});
		}
		for (int j = 0; j < this.m_presets.m_servicePresets.Length; j++)
		{
			list.Add(new ConditionList.Option(ConditionList.ConditionType.ServiceCount)
			{
				m_servicePreset = this.m_presets.m_servicePresets[j]
			});
		}
		this.m_allOptions = list.ToArray();
	}

	private TriggerMilestone[] GetAvailableTriggers()
	{
		List<TriggerMilestone> list = new List<TriggerMilestone>();
		TriggerMilestone[] scenarioTriggers = Singleton<UnlockManager>.get_instance().m_scenarioTriggers;
		for (int i = 0; i < scenarioTriggers.Length; i++)
		{
			TriggerMilestone triggerMilestone = scenarioTriggers[i];
			if (base.currentTrigger.CanAddTrigger(triggerMilestone))
			{
				list.Add(triggerMilestone);
			}
		}
		return list.ToArray();
	}

	protected override void OnTriggerChanged()
	{
		this.RefreshOptionList();
		this.RefreshConditionsFromTrigger();
	}

	private void RefreshConditionsFromTrigger()
	{
		if (base.currentTrigger.m_conditions != null)
		{
			ConditionItem[] array = this.m_conditionItems.SetItemCount(base.currentTrigger.m_conditions.Length);
			int num = 0;
			ConditionItem[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				ConditionItem conditionItem = array2[i];
				conditionItem.AssignExistingCondition(this, base.currentTrigger.m_conditions[num]);
				num++;
			}
		}
		else
		{
			this.m_conditionItems.SetItemCount(0);
		}
		base.Find<UIPanel>("PanelAddItem").set_zOrder(this.m_container.get_childCount() - 1);
	}

	public void OnAddItem()
	{
		ConditionItem conditionItem = this.m_conditionItems.AddItem();
		conditionItem.Init(this);
		base.Find<UIPanel>("PanelAddItem").set_zOrder(this.m_container.get_childCount() - 1);
		this.RefreshAllSwitchArrows();
	}

	public void DeleteItem(ConditionItem item)
	{
		item.DeleteSelectedCondition();
		this.m_conditionItems.RemoveItem(item);
	}

	public string[] GetAvailableOptions(ConditionList.Option currentlySelectedOption)
	{
		List<string> list = new List<string>();
		int num = 0;
		ConditionList.Option[] allOptions = this.m_allOptions;
		for (int i = 0; i < allOptions.Length; i++)
		{
			ConditionList.Option option = allOptions[i];
			if (this.OptionIsAvailable(option, currentlySelectedOption))
			{
				list.Add(option.GetLocalizedTitle());
				option.m_dropDownIndex = num;
				num++;
			}
		}
		return list.ToArray();
	}

	public ConditionList.Option FindOptionWithDropdownIndex(int index)
	{
		ConditionList.Option[] allOptions = this.m_allOptions;
		for (int i = 0; i < allOptions.Length; i++)
		{
			ConditionList.Option option = allOptions[i];
			if (option.m_dropDownIndex == index)
			{
				return option;
			}
		}
		Debug.LogError("Could not find option with index " + index);
		return null;
	}

	public ConditionList.Option GetCurrentDefaultOption()
	{
		ConditionList.Option[] allOptions = this.m_allOptions;
		for (int i = 0; i < allOptions.Length; i++)
		{
			ConditionList.Option option = allOptions[i];
			if (this.OptionIsNotUsedYet(option))
			{
				return option;
			}
		}
		ConditionList.Option[] allOptions2 = this.m_allOptions;
		for (int j = 0; j < allOptions2.Length; j++)
		{
			ConditionList.Option option2 = allOptions2[j];
			if (this.OptionIsAvailable(option2, null))
			{
				return option2;
			}
		}
		Debug.LogError("No options available.");
		return null;
	}

	private bool OptionIsAvailable(ConditionList.Option option, ConditionList.Option currentlySelectedOption)
	{
		return option.m_conditionType != ConditionList.ConditionType.Trigger || option.m_availableTriggers.Length > 0;
	}

	private bool OptionIsNotUsedYet(ConditionList.Option option)
	{
		if (option.m_conditionType == ConditionList.ConditionType.Trigger && option.m_availableTriggers.Length == 0)
		{
			return false;
		}
		foreach (ConditionItem current in this.m_conditionItems.items)
		{
			if (current.m_selectedOption != null && current.m_selectedOption.GetLocalizedTitle() == option.GetLocalizedTitle())
			{
				return false;
			}
		}
		return true;
	}

	public ConditionList.Option FindOptionForStatisticMilestone(StatisticMilestone statisticMilestone)
	{
		ConditionList.Option[] allOptions = this.m_allOptions;
		for (int i = 0; i < allOptions.Length; i++)
		{
			ConditionList.Option option = allOptions[i];
			if (option.m_conditionType == ConditionList.ConditionType.Preset && option.m_statisticsPreset.m_value == statisticMilestone.m_value && option.m_statisticsPreset.m_divider == statisticMilestone.m_divider && option.m_statisticsPreset.m_add == statisticMilestone.m_add && option.m_statisticsPreset.m_subtract == statisticMilestone.m_subtract && option.m_statisticsPreset.m_arrayIndex == statisticMilestone.m_arrayIndex && option.m_statisticsPreset.m_dividerIndex == statisticMilestone.m_dividerIndex && option.m_statisticsPreset.m_addIndex == statisticMilestone.m_addIndex && option.m_statisticsPreset.m_subtractIndex == statisticMilestone.m_subtractIndex && option.m_statisticsPreset.m_resultOffsetInt == statisticMilestone.m_resultOffsetInt && option.m_statisticsPreset.m_resultOffsetFloat == statisticMilestone.m_resultOffsetFloat && option.m_statisticsPreset.m_targetType == statisticMilestone.m_targetType)
			{
				return option;
			}
		}
		Debug.LogError("Could not find the right option for milestone " + statisticMilestone.GetLocalizedName());
		return null;
	}

	public ConditionList.Option FindOptionForServiceMilestone(BuildingCountMilestone serviceMilestone)
	{
		ConditionList.Option[] allOptions = this.m_allOptions;
		for (int i = 0; i < allOptions.Length; i++)
		{
			ConditionList.Option option = allOptions[i];
			if (option.m_conditionType == ConditionList.ConditionType.ServiceCount && option.m_servicePreset.m_service == serviceMilestone.m_service && option.m_servicePreset.m_subService == serviceMilestone.m_subService && option.m_servicePreset.m_level == serviceMilestone.m_level && option.m_servicePreset.m_requireDifferentBuildings == serviceMilestone.m_requireDifferentBuildings)
			{
				return option;
			}
		}
		Debug.LogError("Could not find the right option for milestone " + serviceMilestone.GetLocalizedName());
		return null;
	}

	public void SwitchConditionUp(ConditionItem item)
	{
		int conditionIndex = this.GetConditionIndex(item);
		this.SwitchConditions(conditionIndex, conditionIndex - 1);
	}

	public void SwitchConditionDown(ConditionItem item)
	{
		int conditionIndex = this.GetConditionIndex(item);
		this.SwitchConditions(conditionIndex, conditionIndex + 1);
	}

	private void SwitchConditions(int a, int b)
	{
		MilestoneInfo milestoneInfo = this.m_triggerMilestone.m_conditions[a];
		this.m_triggerMilestone.m_conditions[a] = this.m_triggerMilestone.m_conditions[b];
		this.m_triggerMilestone.m_conditions[b] = milestoneInfo;
		this.RefreshConditionsFromTrigger();
	}

	private int GetConditionIndex(ConditionItem item)
	{
		return this.m_conditionItems.items.IndexOf(item);
	}

	public void GetArrowStates(ConditionItem condition, out bool first, out bool last)
	{
		int conditionIndex = this.GetConditionIndex(condition);
		first = (conditionIndex == 0);
		last = (conditionIndex == this.m_conditionItems.items.Count - 1);
	}

	private void RefreshAllSwitchArrows()
	{
		foreach (ConditionItem current in this.m_conditionItems.items)
		{
			current.RefreshSwitchArrows();
		}
	}

	private UITemplateList<ConditionItem> m_conditionItems;

	private UIScrollablePanel m_container;

	public ScenarioGoalPresets m_presets;

	public ConditionList.Option m_triggerOption;

	public ConditionList.Option m_buildingsOption;

	public ConditionList.Option m_gameAreaOption;

	public ConditionList.Option m_timeLimitOption;

	private ConditionList.Option[] m_allOptions;

	public class Option
	{
		public Option(ConditionList.ConditionType conditionType)
		{
			this.m_conditionType = conditionType;
		}

		public string GetLocalizedTitle()
		{
			if (this.m_conditionType == ConditionList.ConditionType.Preset)
			{
				return this.m_statisticsPreset.GetDescription();
			}
			if (this.m_conditionType == ConditionList.ConditionType.TimeLimit)
			{
				return StringUtils.SafeFormat(Locale.Get("MILESTONE_TIME_DESC"), "X");
			}
			if (this.m_conditionType == ConditionList.ConditionType.Trigger)
			{
				return Locale.Get("CONDITION_TRIGGERTRIGGERED");
			}
			if (this.m_conditionType == ConditionList.ConditionType.GameArea)
			{
				return Locale.Get("CONDITIONITEM_GAMEAREA");
			}
			if (this.m_conditionType == ConditionList.ConditionType.BuildingMilestone)
			{
				return Locale.Get("TRIGGERPANEL_BUILDINGSBUILT");
			}
			if (this.m_conditionType == ConditionList.ConditionType.ServiceCount)
			{
				return this.m_servicePreset.GetDescription();
			}
			return "Undefined";
		}

		public MilestoneInfo AddToTrigger(TriggerMilestone triggerMilestone, int selectedSubDropdownIndex)
		{
			if (this.m_conditionType == ConditionList.ConditionType.Preset)
			{
				StatisticMilestone statisticMilestone = triggerMilestone.AddCondition<StatisticMilestone>();
				statisticMilestone.m_value = this.m_statisticsPreset.m_value;
				statisticMilestone.m_divider = this.m_statisticsPreset.m_divider;
				statisticMilestone.m_add = this.m_statisticsPreset.m_add;
				statisticMilestone.m_subtract = this.m_statisticsPreset.m_subtract;
				statisticMilestone.m_arrayIndex = this.m_statisticsPreset.m_arrayIndex;
				statisticMilestone.m_dividerIndex = this.m_statisticsPreset.m_dividerIndex;
				statisticMilestone.m_addIndex = this.m_statisticsPreset.m_addIndex;
				statisticMilestone.m_subtractIndex = this.m_statisticsPreset.m_subtractIndex;
				statisticMilestone.m_resultOffsetInt = this.m_statisticsPreset.m_resultOffsetInt;
				statisticMilestone.m_resultOffsetFloat = this.m_statisticsPreset.m_resultOffsetFloat;
				statisticMilestone.m_targetType = this.m_statisticsPreset.m_targetType;
				return statisticMilestone;
			}
			if (this.m_conditionType == ConditionList.ConditionType.TimeLimit)
			{
				return triggerMilestone.AddCondition<TimeMilestone>();
			}
			if (this.m_conditionType == ConditionList.ConditionType.Trigger)
			{
				triggerMilestone.AddCondition(this.m_availableTriggers[selectedSubDropdownIndex]);
				return this.m_availableTriggers[selectedSubDropdownIndex];
			}
			if (this.m_conditionType == ConditionList.ConditionType.GameArea)
			{
				return triggerMilestone.AddCondition<GameAreaMilestone>();
			}
			if (this.m_conditionType == ConditionList.ConditionType.BuildingMilestone)
			{
				WrapperMilestone wrapperMilestone = triggerMilestone.AddCondition<WrapperMilestone>();
				wrapperMilestone.m_requirePassed = this.m_availableBuildingMilestones[selectedSubDropdownIndex];
				wrapperMilestone.m_type = WrapperMilestone.WrapperType.Create;
				return wrapperMilestone;
			}
			if (this.m_conditionType == ConditionList.ConditionType.ServiceCount)
			{
				BuildingCountMilestone buildingCountMilestone = triggerMilestone.AddCondition<BuildingCountMilestone>();
				buildingCountMilestone.m_service = this.m_servicePreset.m_service;
				buildingCountMilestone.m_subService = this.m_servicePreset.m_subService;
				buildingCountMilestone.m_level = this.m_servicePreset.m_level;
				buildingCountMilestone.m_requireDifferentBuildings = this.m_servicePreset.m_requireDifferentBuildings;
				buildingCountMilestone.m_localizationKey = this.m_servicePreset.m_localeKey;
				return buildingCountMilestone;
			}
			Debug.LogError("Failed to add condition");
			return null;
		}

		public void RemoveFromTrigger(TriggerMilestone triggerMilestone, MilestoneInfo condition)
		{
			if (!triggerMilestone.RemoveCondition(condition))
			{
				Debug.LogError("Failed to remove condition");
			}
		}

		public ConditionList.ConditionType m_conditionType;

		public int m_dropDownIndex = -1;

		public ScenarioGoalPresets.StatisticPreset m_statisticsPreset;

		public ScenarioGoalPresets.ServicePreset m_servicePreset;

		public TriggerMilestone[] m_availableTriggers;

		public MilestoneInfo[] m_availableBuildingMilestones;

		private MilestoneInfo m_buildingMilestoneInfo;
	}

	public enum ConditionType
	{
		Preset,
		TimeLimit,
		Trigger,
		GameArea,
		BuildingMilestone,
		ServiceCount
	}
}
