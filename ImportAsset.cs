﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ColossalFramework.Importers;
using ColossalFramework.Threading;
using UnityEngine;

public abstract class ImportAsset
{
	public ImportAsset(GameObject template, PreviewCamera camera)
	{
		this.m_TemplateObject = template;
		this.m_Camera = camera;
		Task[] item = new Task[3];
		Task[] item2 = new Task[2];
		this.m_Tasks = new List<Task[]>();
		this.m_TaskNames = new List<string>();
		this.m_Tasks.Add(item);
		this.m_TaskNames.Add("Compressing");
		this.m_Tasks.Add(item2);
		this.m_TaskNames.Add("LOD");
	}

	protected virtual AssetImporterTextureLoader.ResultType[] textureTypes
	{
		get
		{
			return new AssetImporterTextureLoader.ResultType[]
			{
				AssetImporterTextureLoader.ResultType.RGB,
				AssetImporterTextureLoader.ResultType.XYS,
				AssetImporterTextureLoader.ResultType.ACI
			};
		}
	}

	protected virtual int textureAnisoLevel
	{
		get
		{
			return 4;
		}
	}

	public bool ReadyForEditing
	{
		get
		{
			return this.m_ReadyForEditing;
		}
	}

	public bool IsLoadingTextures
	{
		get
		{
			return this.m_TextureTask != null && !this.m_TextureTask.get_hasEnded();
		}
	}

	public bool TextureLoadingFinished
	{
		get
		{
			return this.m_TextureTask != null && this.m_TextureTask.get_hasEnded();
		}
	}

	public bool IsLoadingModel
	{
		get
		{
			return this.m_IsLoadingModel;
		}
	}

	public MultiAsyncTaskWrapper Tasks
	{
		get
		{
			return this.m_TaskWrapper;
		}
	}

	public GameObject Object
	{
		get
		{
			return this.m_Object;
		}
	}

	public GameObject TemplateObject
	{
		get
		{
			return this.m_TemplateObject;
		}
	}

	public virtual Mesh mesh
	{
		get
		{
			return null;
		}
	}

	public virtual Material material
	{
		get
		{
			return null;
		}
		set
		{
		}
	}

	public virtual Mesh lodMesh
	{
		get
		{
			return null;
		}
	}

	public virtual Material lodMaterial
	{
		get
		{
			return null;
		}
	}

	public virtual void Import(string path, string filename)
	{
		if (this.m_Object != null)
		{
			this.DestroyAsset();
			this.m_Object = null;
		}
		this.m_Path = path;
		this.m_Filename = filename;
		this.m_IsLoadingModel = true;
		this.m_IsImportedAsset = true;
		this.m_Importer = new SceneImporter();
		this.CreateLODObject();
		this.m_Importer.set_filePath(Path.Combine(path, filename));
		this.m_Importer.set_importSkinMesh(true);
		Task<GameObject> modelLoad = this.m_Importer.ImportAsync(new Action<GameObject>(this.GenerateAssetData));
		this.LoadTextures(modelLoad);
	}

	public virtual void ImportDefault()
	{
		if (this.m_Object != null)
		{
			this.DestroyAsset();
			this.m_Object = null;
		}
		this.m_IsImportedAsset = false;
		this.m_ReadyForEditing = false;
		if (this.m_TemplateObject != null)
		{
			PrefabInfo component = this.m_TemplateObject.GetComponent<PrefabInfo>();
			if (component == null)
			{
				return;
			}
			PrefabInfo prefabInfo = UnityEngine.Object.Instantiate<PrefabInfo>(component);
			prefabInfo.set_name(component.get_name());
			this.m_Object = prefabInfo.get_gameObject();
			Mesh sharedMesh = this.GetSharedMesh(this.m_Object);
			if (sharedMesh != null)
			{
				this.SetMesh(this.m_Object, sharedMesh, true);
				PrefabAI component2 = this.m_Object.GetComponent<PrefabAI>();
				if (component2 != null)
				{
					UnityEngine.Object.Destroy(component2);
				}
				UnityEngine.Object.Destroy(prefabInfo);
				this.CreateLODObject();
				this.GenerateAssetData(this.m_Object);
			}
			else
			{
				this.m_ReadyForEditing = true;
			}
		}
	}

	public virtual void DestroyAsset()
	{
		if (this.m_OriginalMesh != null)
		{
			UnityEngine.Object.DestroyImmediate(this.m_OriginalMesh);
			this.m_OriginalMesh = null;
		}
		if (this.m_Object != null)
		{
			Mesh sharedMesh = this.GetSharedMesh(this.m_Object);
			if (sharedMesh != null)
			{
				UnityEngine.Object.DestroyImmediate(sharedMesh);
			}
			if (this.m_IsImportedAsset)
			{
				Renderer[] componentsInChildren = this.m_Object.GetComponentsInChildren<MeshRenderer>(true);
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					for (int j = 0; j < componentsInChildren[i].get_sharedMaterials().Length; j++)
					{
						Material material = componentsInChildren[i].get_sharedMaterials()[j];
						if (material.HasProperty("_MainTex"))
						{
							Texture texture = material.GetTexture("_MainTex");
							if (texture != null)
							{
								UnityEngine.Object.DestroyImmediate(texture);
							}
						}
						if (material.HasProperty("_XYSMap"))
						{
							Texture texture2 = material.GetTexture("_XYSMap");
							if (texture2 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture2);
							}
						}
						if (material.HasProperty("_ACIMap"))
						{
							Texture texture3 = material.GetTexture("_ACIMap");
							if (texture3 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture3);
							}
						}
						if (material.HasProperty("_XYCAMap"))
						{
							Texture texture4 = material.GetTexture("_XYCAMap");
							if (texture4 != null)
							{
								UnityEngine.Object.DestroyImmediate(texture4);
							}
						}
					}
				}
			}
			UnityEngine.Object.DestroyImmediate(this.m_Object);
			this.m_Object = null;
			this.m_ReadyForEditing = false;
		}
	}

	public virtual void FinalizeImport()
	{
		if (this.m_TemplateObject != null && this.m_Object != null)
		{
			this.FinalizeLOD();
			if (this.m_Object.GetComponent<Renderer>() != null && this.m_IsImportedAsset)
			{
				this.CompressTextures();
			}
			this.m_TaskWrapper = new MultiAsyncTaskWrapper(this.m_TaskNames, this.m_Tasks);
			LoadSaveStatus.activeTask = this.m_TaskWrapper;
			this.InitializeObject();
			PrefabInfo component = this.m_Object.GetComponent<PrefabInfo>();
			PrefabInfo component2 = this.m_TemplateObject.GetComponent<PrefabInfo>();
			if (component2.m_Atlas != null && component2.m_Atlas.get_material() != null)
			{
				if (component.GetComponent<MeshFilter>() != null)
				{
					string fileName = (!this.m_IsImportedAsset) ? null : Path.Combine(this.m_Path, Path.GetFileNameWithoutExtension(this.m_Filename));
					AssetImporterThumbnails.CreateThumbnails(this.m_Object, fileName, this.m_Camera);
				}
				else
				{
					component.m_Atlas = component2.m_Atlas;
					component.m_Thumbnail = component2.m_Thumbnail;
					component.m_InfoTooltipAtlas = component2.m_InfoTooltipAtlas;
					component.m_InfoTooltipThumbnail = this.m_TemplateObject.get_name();
				}
			}
			else
			{
				component.m_Atlas = null;
				component.m_Thumbnail = string.Empty;
				component.m_InfoTooltipAtlas = null;
				component.m_InfoTooltipThumbnail = string.Empty;
			}
		}
	}

	public virtual void ApplyTransform(Vector3 scale, Vector3 rotation, bool bottomPivot)
	{
		if (this.m_Object == null)
		{
			return;
		}
		MeshFilter component = this.m_Object.GetComponent<MeshFilter>();
		if (component != null && this.m_OriginalMesh != null)
		{
			RuntimeMeshUtils.ProcessMeshTransform(this.m_OriginalMesh, component.get_sharedMesh(), scale, bottomPivot, rotation);
			this.CreateInfo();
			this.m_Camera.RecalculateBounds();
		}
	}

	public virtual float CalculateDefaultScale()
	{
		if (!this.m_IsImportedAsset)
		{
			return 1f;
		}
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		MeshFilter component = this.m_TemplateObject.GetComponent<MeshFilter>();
		if (component != null)
		{
			Mesh sharedMesh = component.get_sharedMesh();
			if (sharedMesh != null)
			{
				vector = sharedMesh.get_bounds().get_size();
			}
		}
		if (this.m_OriginalMesh != null)
		{
			vector2 = this.m_OriginalMesh.get_bounds().get_size();
		}
		if (vector == Vector3.get_zero() || vector2 == Vector3.get_zero())
		{
			return 1f;
		}
		return Mathf.Min(vector.x / vector2.x, vector.z / vector2.z);
	}

	protected virtual void CompressTextures()
	{
		Material material = this.m_Object.GetComponent<Renderer>().get_material();
		this.m_Tasks[0][0] = AssetImporterTextureLoader.CompressTexture(material, "_MainTex", false);
		this.m_Tasks[0][1] = AssetImporterTextureLoader.CompressTexture(material, "_XYSMap", true);
		this.m_Tasks[0][2] = AssetImporterTextureLoader.CompressTexture(material, "_ACIMap", true);
	}

	protected abstract void InitializeObject();

	protected abstract void CreateInfo();

	protected virtual void CreateLODObject()
	{
	}

	protected virtual void FinalizeLOD()
	{
	}

	protected virtual void LoadTextures(Task<GameObject> modelLoad)
	{
		this.m_TextureTask = AssetImporterTextureLoader.LoadTextures(modelLoad, null, this.textureTypes, this.m_Path, this.m_Filename, false, this.textureAnisoLevel, false, false);
	}

	protected virtual void GenerateAssetData(GameObject instance)
	{
		if (instance == null)
		{
			this.m_IsLoadingModel = false;
			this.m_ReadyForEditing = false;
			return;
		}
		this.m_Object = instance;
		this.m_Object.SetActive(false);
		instance.get_transform().set_localScale(Vector3.get_one());
		Mesh sharedMesh = this.GetSharedMesh(this.m_Object);
		if (sharedMesh == null)
		{
			this.m_IsLoadingModel = false;
			this.m_ReadyForEditing = false;
			return;
		}
		this.m_OriginalMesh = RuntimeMeshUtils.CopyMesh(sharedMesh);
		this.CreateInfo();
		this.CopyMaterialProperties();
		this.m_IsLoadingModel = false;
		this.m_ReadyForEditing = true;
	}

	protected virtual void CopyMaterialProperties()
	{
		Renderer renderer = this.GetRenderer(this.m_TemplateObject);
		Shader shader = this.GetShader(this.m_TemplateObject);
		if (shader == null)
		{
			shader = this.GetDefaultShader();
		}
		Renderer[] renderers = this.GetRenderers(this.m_Object);
		for (int i = 0; i < renderers.Length; i++)
		{
			for (int j = 0; j < renderers[i].get_sharedMaterials().Length; j++)
			{
				renderers[i].get_sharedMaterials()[j] = renderers[i].get_materials()[j];
				renderers[i].get_sharedMaterials()[j].set_shader(shader);
				if (renderer != null)
				{
					Material material = renderers[i].get_sharedMaterials()[j];
					material.CopyPropertiesFromMaterial(renderer.get_sharedMaterial());
					if (this.m_IsImportedAsset)
					{
						if (material.HasProperty("_MainTex"))
						{
							material.SetTexture("_MainTex", null);
						}
						if (material.HasProperty("_XYSMap"))
						{
							material.SetTexture("_XYSMap", null);
						}
						if (material.HasProperty("_ACIMap"))
						{
							material.SetTexture("_ACIMap", null);
						}
						if (material.HasProperty("_XYCAMap"))
						{
							material.SetTexture("_XYCAMap", null);
						}
						if (material.HasProperty("_APRMap"))
						{
							material.SetTexture("_APRMap", null);
						}
					}
				}
			}
		}
	}

	public static T CopyComponent<T>(T component, GameObject target) where T : Component
	{
		Type type = component.GetType();
		T t = target.GetComponent(type) as T;
		if (t != null)
		{
			UnityEngine.Object.DestroyImmediate(t);
		}
		T t2 = target.AddComponent(type) as T;
		FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		FieldInfo[] array = fields;
		for (int i = 0; i < array.Length; i++)
		{
			FieldInfo fieldInfo = array[i];
			if (!fieldInfo.IsNotSerialized && (fieldInfo.IsPublic || fieldInfo.FieldType.IsDefined(typeof(SerializableAttribute), false)))
			{
				object value = fieldInfo.GetValue(component);
				fieldInfo.SetValue(t2, value);
			}
		}
		return t2;
	}

	protected virtual Mesh GetSharedMesh(GameObject go)
	{
		MeshFilter component = go.GetComponent<MeshFilter>();
		if (component != null)
		{
			return component.get_sharedMesh();
		}
		return null;
	}

	protected virtual void SetMesh(GameObject go, Mesh mesh, bool instantiate = false)
	{
		Mesh mesh2 = mesh;
		if (instantiate)
		{
			mesh2 = UnityEngine.Object.Instantiate<Mesh>(mesh);
			mesh2.set_name(mesh.get_name());
		}
		MeshFilter component = go.GetComponent<MeshFilter>();
		if (component != null)
		{
			component.set_sharedMesh(mesh2);
		}
	}

	protected virtual Shader GetShader(GameObject go)
	{
		Renderer component = go.GetComponent<Renderer>();
		if (component != null && component.get_sharedMaterial() != null && component.get_sharedMaterial().get_shader() != null)
		{
			return component.get_sharedMaterial().get_shader();
		}
		return null;
	}

	protected virtual Renderer GetRenderer(GameObject go)
	{
		return go.GetComponent<MeshRenderer>();
	}

	protected Shader GetDefaultShader()
	{
		return Shader.Find("Custom/Buildings/Building/Default");
	}

	protected virtual Renderer[] GetRenderers(GameObject go)
	{
		return go.GetComponentsInChildren<MeshRenderer>(true);
	}

	protected static readonly string sLODModelSignature = "_lod";

	protected static readonly string sLODModelSignature2 = "_LOD";

	protected GameObject m_Object;

	protected GameObject m_LODObject;

	protected string m_Path;

	protected string m_Filename;

	protected GameObject m_TemplateObject;

	protected Mesh m_OriginalMesh;

	protected bool m_IsImportedAsset;

	protected bool m_ReadyForEditing;

	protected bool m_IsLoadingTextures;

	protected bool m_IsLoadingModel;

	protected List<Task[]> m_Tasks;

	protected List<string> m_TaskNames;

	protected Task m_TextureTask;

	protected MultiAsyncTaskWrapper m_TaskWrapper;

	protected PreviewCamera m_Camera;

	protected SceneImporter m_Importer;
}
