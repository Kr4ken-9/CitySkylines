﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.HTTP.Paradox;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class Starter : MonoBehaviour
{
	static Starter()
	{
		AppDomain.CurrentDomain.TypeResolve += new ResolveEventHandler(BuildConfig.CurrentDomain_AssemblyResolve);
		AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(BuildConfig.CurrentDomain_AssemblyResolve);
		Account.set_paradoxApiProject("cities");
		Account.set_paradoxApiURL("https://api.paradoxplaza.com");
		Telemetry.set_paradoxApiURL("https://opstm.paradoxplaza.com/cities");
		NewsFeedPanel.steamNewsFeedURL = "https://services.paradoxplaza.com/head/feeds/citiesskylines/content";
		ParadoxAccountCreate.emailTemplate = string.Empty;
		ParadoxAccountCreate.landingUrl = "http://www.citiesskylines.com/auth/cas";
		DataLocation.set_isEditor(false);
		DataLocation.set_productVersion(172090642u);
		DataLocation.set_productName("Cities_Skylines");
		DataLocation.set_migrateProductFrom("Cities");
		PackageManager.set_assetStateSettingsFile(Settings.userGameState);
		PluginManager.set_assetStateSettingsFile(Settings.userGameState);
		if (Starter.<>f__mg$cache0 == null)
		{
			Starter.<>f__mg$cache0 = new PackageSerializer.CustomSerializeHandler(PackageHelper.CustomSerialize);
		}
		PackageSerializer.SetCustomSerializer(Starter.<>f__mg$cache0);
		if (Starter.<>f__mg$cache1 == null)
		{
			Starter.<>f__mg$cache1 = new PackageDeserializer.CustomDeserializeHandler(PackageHelper.CustomDeserialize);
		}
		PackageDeserializer.SetCustomDeserializer(Starter.<>f__mg$cache1);
		if (Starter.<>f__mg$cache2 == null)
		{
			Starter.<>f__mg$cache2 = new PackageDeserializer.UnknownTypeHandler(PackageHelper.UnknownTypeHandler);
		}
		PackageDeserializer.SetUnknownTypeHandler(Starter.<>f__mg$cache2);
		if (Starter.<>f__mg$cache3 == null)
		{
			Starter.<>f__mg$cache3 = new PackageDeserializer.ResolveLegacyTypeHandler(PackageHelper.ResolveLegacyTypeHandler);
		}
		PackageDeserializer.SetResolveLegacyTypeHandler(Starter.<>f__mg$cache3);
		if (Starter.<>f__mg$cache4 == null)
		{
			Starter.<>f__mg$cache4 = new PackageDeserializer.ResolveLegacyMemberHandler(PackageHelper.ResolveLegacyMemberHandler);
		}
		PackageDeserializer.SetResolveLegacyMemberHandler(Starter.<>f__mg$cache4);
		if (Starter.<>f__mg$cache5 == null)
		{
			Starter.<>f__mg$cache5 = new DataSerializer.LegacyResolverHandler(BuildConfig.ResolveLegacyType);
		}
		DataSerializer.SetDefaultLegacyResolver(Starter.<>f__mg$cache5);
	}

	private void SetStartupResolution(bool windowed)
	{
		SavedInt savedInt = new SavedInt(Settings.screenWidth, Settings.gameSettingsFile, Screen.get_currentResolution().get_width());
		SavedInt savedInt2 = new SavedInt(Settings.screenHeight, Settings.gameSettingsFile, Screen.get_currentResolution().get_height());
		SavedBool savedBool = new SavedBool(Settings.fullscreen, Settings.gameSettingsFile, true);
		if (savedInt2.get_value() == savedInt.get_value())
		{
		}
		if (windowed || (savedInt.get_exists() && savedInt2.get_exists()))
		{
			if (windowed)
			{
				savedBool.set_value(false);
			}
			CODebugBase<LogChannel>.Log(LogChannel.Core, string.Concat(new object[]
			{
				"Setting initial resolution to ",
				savedInt,
				" ",
				savedInt2,
				" ",
				savedBool
			}));
			Screen.SetResolution(savedInt, savedInt2, savedBool);
		}
		else if (!windowed)
		{
			Screen.SetResolution(Screen.get_width(), Screen.get_height(), true);
		}
		int num = QualitySettings.get_names().Length;
		QualitySettings.SetQualityLevel(num);
		PlayerPrefs.SetInt("UnityGraphicsQuality", num);
		SavedInt savedInt3 = new SavedInt(Settings.vsync, Settings.gameSettingsFile, DefaultSettings.vsync, true);
		CODebugBase<LogChannel>.Log(LogChannel.Core, "Setting vsync to " + savedInt3.get_value());
		QualitySettings.set_vSyncCount(savedInt3.get_value());
	}

	private string ReportSystemInfo()
	{
		return StringUtils.SafeFormat(Starter.format, new object[]
		{
			SystemInfo.get_deviceUniqueIdentifier(),
			SystemInfo.get_deviceModel(),
			SystemInfo.get_graphicsDeviceName(),
			SystemInfo.get_graphicsDeviceVersion(),
			SystemInfo.get_graphicsMemorySize(),
			SystemInfo.get_graphicsShaderLevel(),
			SystemInfo.get_operatingSystem(),
			Application.get_systemLanguage(),
			SystemInfo.get_processorType(),
			SystemInfo.get_processorCount(),
			SystemInfo.get_systemMemorySize(),
			BuildConfig.applicationVersionFull
		});
	}

	public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
	{
		DirectoryInfo directoryInfo = new DirectoryInfo(sourceDirName);
		DirectoryInfo[] directories = directoryInfo.GetDirectories();
		if (!directoryInfo.Exists)
		{
			throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
		}
		if (!Directory.Exists(destDirName))
		{
			Directory.CreateDirectory(destDirName);
		}
		FileInfo[] files = directoryInfo.GetFiles();
		FileInfo[] array = files;
		for (int i = 0; i < array.Length; i++)
		{
			FileInfo fileInfo = array[i];
			string destFileName = Path.Combine(destDirName, fileInfo.Name);
			fileInfo.CopyTo(destFileName, true);
		}
		if (copySubDirs)
		{
			DirectoryInfo[] array2 = directories;
			for (int j = 0; j < array2.Length; j++)
			{
				DirectoryInfo directoryInfo2 = array2[j];
				string destDirName2 = Path.Combine(destDirName, directoryInfo2.Name);
				Starter.DirectoryCopy(directoryInfo2.FullName, destDirName2, copySubDirs);
			}
		}
	}

	private void DisableWorkshop()
	{
		WorkshopAdPanel.dontInitialize = true;
		PluginManager.noWorkshop = true;
		PackageManager.noWorkshop = true;
		ContentManagerPanel.noWorkshop = true;
	}

	private void InitSteamController()
	{
		string[] names = Enum.GetNames(typeof(SteamController.DigitalInput));
		for (int i = 0; i < names.Length - 1; i++)
		{
			PlatformService.RegisterDigitalInput(names[i]);
		}
		string[] names2 = Enum.GetNames(typeof(SteamController.AnalogInput));
		for (int j = 0; j < names2.Length - 1; j++)
		{
			PlatformService.RegisterAnalogInput(names2[j]);
		}
		PlatformService.ActivateActionSet("InGameControls");
	}

	private void OnPlatformShutdown()
	{
		Debug.Log("Shuting down on Platform request... BAI!");
		Singleton<LoadingManager>.get_instance().QuitApplication();
	}

	private void Awake()
	{
		Input.set_imeCompositionMode(2);
		if (!Starter.m_started)
		{
			Starter.m_started = true;
			try
			{
				PlatformService.set_desiredAppID(255710u);
				Debug.Log(this.ReportSystemInfo());
				PlatformService.add_eventSteamControllerInit(new PlatformService.SteamControllerInitHandler(this.InitSteamController));
				PlatformService.add_eventPlatformServiceShutdown(new PlatformService.PlatformServicesShutdownHandler(this.OnPlatformShutdown));
				try
				{
					BootStrapper.Preload();
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
					Singleton<TelemetryManager>.get_instance().OnExceptionForwarded(ex);
				}
				if (!PlatformService.get_active() || PlatformService.IsForceQuitting())
				{
					return;
				}
				bool disableMods = false;
				bool generateDevConf = false;
				bool verboseLog = false;
				bool windowed = false;
				bool runInBackground = true;
				int targetfps = -1;
				OptionSet optionSet = new OptionSet();
				optionSet.Add("v|verboseLog", "Verbose Log", delegate(string v)
				{
					verboseLog = (v != null);
				});
				optionSet.Add("w|windowed", "Windowed mode", delegate(string v)
				{
					windowed = (v != null);
				});
				optionSet.Add("b|disableInBG", "Don't run in background", delegate(string v)
				{
					runInBackground = (v == null);
				});
				optionSet.Add("generateDevConf", "Generate a developer configuration file", delegate(string v)
				{
					generateDevConf = (v != null);
				});
				optionSet.Add("disableMods", "Disable mods", delegate(string v)
				{
					disableMods = (v != null);
				});
				optionSet.Add("screen-fullscreen=", "Unity force fullscreen", delegate(string v)
				{
				});
				optionSet.Add("screen-height=", "Unity force height", delegate(string v)
				{
				});
				optionSet.Add("screen-width=", "Unity force width", delegate(string v)
				{
				});
				optionSet.Add<int>("limitfps=", "Set target framerate (disables vsync)", delegate(int v)
				{
					targetfps = v;
				});
				optionSet.Add("noWorkshop", "Disable Steam workshop", delegate(string v)
				{
					if (v != null)
					{
						this.DisableWorkshop();
					}
				});
				OptionSet optionSet2 = optionSet;
				try
				{
					optionSet2.Parse(CommandLine.arguments);
				}
				catch (Exception ex2)
				{
					Debug.LogException(ex2);
					UIView.ForwardException(ex2);
				}
				Application.set_runInBackground(runInBackground);
				if (targetfps > 0)
				{
					QualitySettings.set_vSyncCount(0);
					Application.set_targetFrameRate(targetfps);
				}
				SettingsFile[] expr_26F = new SettingsFile[1];
				int arg_286_1 = 0;
				SettingsFile settingsFile = new SettingsFile();
				settingsFile.set_fileName(Settings.devGameSettingsFile);
				expr_26F[arg_286_1] = settingsFile;
				GameSettings.AddSettingsFile(expr_26F);
				SavedBool savedBool = new SavedBool(Settings.gameLogVerbose, Settings.devGameSettingsFile, false);
				SavedInt savedInt = new SavedInt(Settings.gameLogChannels, Settings.devGameSettingsFile, 127);
				SavedBool savedBool2 = new SavedBool(Settings.frameworkLogVerbose, Settings.devGameSettingsFile, false);
				InternalLogChannel internalLogChannel = 32767;
				if (Application.get_isEditor())
				{
					internalLogChannel |= 32768;
				}
				SavedInt savedInt2 = new SavedInt(Settings.frameworkLogChannels, Settings.devGameSettingsFile, internalLogChannel);
				if (generateDevConf)
				{
					savedBool.set_value(true);
					savedBool2.set_value(true);
					savedInt.set_value(127);
					savedInt2.set_value(65535);
				}
				CODebugBase<LogChannel>.set_verbose(verboseLog);
				CODebugBase<LogChannel>.EnableChannels((LogChannel)savedInt.get_value());
				SettingsFile[] expr_33E = new SettingsFile[1];
				int arg_355_1 = 0;
				settingsFile = new SettingsFile();
				settingsFile.set_cloudName(Settings.sharedSettings);
				expr_33E[arg_355_1] = settingsFile;
				GameSettings.AddSettingsFile(expr_33E);
				SettingsFile[] expr_361 = new SettingsFile[1];
				int arg_378_1 = 0;
				settingsFile = new SettingsFile();
				settingsFile.set_fileName(Settings.gameSettingsFile);
				expr_361[arg_378_1] = settingsFile;
				GameSettings.AddSettingsFile(expr_361);
				this.SetStartupResolution(windowed);
				UIMarkupTokenizer.set_settingsLocaleID("KEYNAME");
				UIMarkupTokenizer.set_settingsFile(Settings.inputSettingsFile);
				UIMarkupTokenizer.set_settingsClass(typeof(DefaultSettings));
				UIMarkupTokenizer.set_settingsColor(new Color32?(new Color32(10, 33, 64, 255)));
				BootStrapper.Boot(savedInt2.get_value(), savedBool2);
				Singleton<TelemetryManager>.Ensure();
				SettingsFile[] expr_3E5 = new SettingsFile[1];
				int arg_3FC_1 = 0;
				settingsFile = new SettingsFile();
				settingsFile.set_fileName("userGameState");
				expr_3E5[arg_3FC_1] = settingsFile;
				GameSettings.AddSettingsFile(expr_3E5);
				PlatformService.get_achievements().Register(new string[]
				{
					"Pioneer",
					"Decorator",
					"Terraformer",
					"WellInformed",
					"CityPlanner",
					"Lawmaker",
					"PowerAtYourFingertips",
					"HeavenlyCity",
					"Medic",
					"AHugeHadron",
					"BeamMeUp",
					"NewEden",
					"ShortFuse",
					"IWantItAll",
					"Metropolis",
					"Distroy",
					"CityInMotion",
					"CityInMotion2",
					"ClimbingTheSocialLadder",
					"UnpopularMayor",
					"RollingInDough",
					"FreneticPlayer",
					"HappyTown",
					"ToughCity",
					"FireWatch",
					"TheSafestTown",
					"ProfessionalDumper",
					"EarthlovingCity",
					"HigherEducation",
					"SIMulatedCity",
					"SafeCity",
					"PowerToThePeople",
					"MakeThemPay",
					"LeisureSuites",
					"PlayingWithTheBoys",
					"PrisonBreak",
					"1001Nights",
					"DoesMyBumLookBigInThis",
					"SingingInThe",
					"FoggyWeather",
					"Brrr",
					"GetYourSnowshoesReady",
					"HeresATram",
					"ILoveTrams",
					"AreTheyNakedInThere",
					"ItsWintertime",
					"Speedup",
					"ThePlowmaster",
					"QuayKing",
					"WithCanalsYouCan",
					"WeNeedSnorkels",
					"ShakeItUp",
					"ItsHeadingRightForUs",
					"DropTheBase",
					"RunBambi",
					"ThunderAndLightning",
					"TwistAndShout",
					"WhatThe",
					"EternalCity",
					"Creator",
					"WeHaveAWinner",
					"TheUnderdog",
					"RejoiceAndBeFerry",
					"FerryFaerie",
					"Triorail",
					"NotSoMono",
					"ClarkCable",
					"CablesGalore",
					"BlimpBlimp",
					"PutSomeBlimpInYourBlimp",
					"ComboBreaker",
					"NomenEstOmen",
					"Centurion",
					"ItsCalledSteve",
					"TotallyInMotion",
					"Reporting",
					"SuperSelfSufficient",
					"IToTheT",
					"Organistic",
					"GreenEnergy",
					"FriendlyTeaching",
					"GreenestCity"
				});
				try
				{
					if (!disableMods)
					{
						PluginManager.set_userModType(typeof(IUserMod));
						if (Starter.<>f__mg$cache6 == null)
						{
							Starter.<>f__mg$cache6 = new PluginManager.AddMessageHandler(DebugOutputPanel.AddMessage);
						}
						PluginManager.add_eventLogMessage(Starter.<>f__mg$cache6);
						PluginManager.SetAdditionalAssemblies(new string[]
						{
							typeof(IUserMod).Assembly.Location,
							typeof(PluginManager).Assembly.Location,
							typeof(Starter).Assembly.Location
						});
						PluginManager.CompileScripts();
						Singleton<PluginManager>.get_instance().LoadPlugins();
						PluginHelper.ValidatePlugins();
					}
					else
					{
						Singleton<PluginManager>.get_instance().set_enabled(false);
					}
				}
				catch (Exception ex3)
				{
					Debug.LogError(ex3.ToString());
					Singleton<TelemetryManager>.get_instance().OnExceptionForwarded(ex3);
				}
			}
			catch (GameSettingsException ex4)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, ex4.GetType() + ": Game Settings error " + ex4.Message);
			}
			catch (Exception ex5)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, ex5.ToString());
				UIView.ForwardException(ex5);
			}
			Singleton<ErrorManager>.Ensure();
			try
			{
				SavedString savedString = new SavedString(Settings.localeID, Settings.gameSettingsFile, DefaultSettings.localeID);
				if (Starter.<>f__mg$cache7 == null)
				{
					Starter.<>f__mg$cache7 = new LocaleManager.LocaleNeedsSubstitutionHandler(LocaleSubstitution.SubstitutionRules);
				}
				LocaleManager.add_eventLocaleNeedsSubsitution(Starter.<>f__mg$cache7);
				LocaleManager.defaultLanguage = savedString;
				SingletonLite<LocaleManager>.Ensure();
				LocaleManager.set_cultureInfo(new CultureInfo(Locale.Get("MONEY_LOCALE")));
				LocaleManager.get_cultureInfo().NumberFormat.CurrencySymbol = Locale.Get("MONEY_CURRENCY");
				LocaleManager.get_cultureInfo().NumberFormat.CurrencyNegativePattern = 1;
			}
			catch (Exception ex6)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, ex6.ToString());
				UIView.ForwardException(ex6);
			}
			Singleton<LoadingManager>.Ensure();
			Singleton<SimulationManager>.Ensure();
			SimulationManager.RegisterManager(Singleton<ToolManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<GameAreaManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<TerrainManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<InstanceManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<PathManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<NetManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<BuildingManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<CitizenManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<TransferManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<NaturalResourceManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<ImmaterialResourceManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<CoverageManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<ElectricityManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<WaterManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<ZoneManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<PropManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<TreeManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<WeatherManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<VehicleManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<TransportManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<EconomyManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<NotificationManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<DistrictManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<EventManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<DisasterManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<InfoManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<RenderManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<AudioManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<StatisticsManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<UnlockManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<MessageManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<GuideManager>.get_instance());
			SimulationManager.RegisterManager(Singleton<EffectManager>.get_instance());
			Singleton<PopsManager>.Ensure();
			if (this.m_loadIntro)
			{
				if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading && !Singleton<LoadingManager>.get_instance().m_simulationDataLoaded)
				{
					Singleton<LoadingManager>.get_instance().LoadIntro();
				}
			}
			else if (this.m_autoLoad && !Singleton<LoadingManager>.get_instance().m_currentlyLoading && !Singleton<LoadingManager>.get_instance().m_simulationDataLoaded)
			{
				SimulationMetaData simulationMetaData = new SimulationMetaData();
				simulationMetaData.m_environment = this.m_environment;
				simulationMetaData.m_gameInstanceIdentifier = Guid.NewGuid().ToString();
				simulationMetaData.m_currentDateTime = DateTime.Now;
				simulationMetaData.m_updateMode = this.m_loadMode;
				Singleton<LoadingManager>.get_instance().LoadLevel(null, this.m_mainScene, this.m_uiScene, simulationMetaData);
			}
		}
		Object.Destroy(base.get_gameObject());
	}

	public bool m_autoLoad;

	public bool m_loadIntro;

	public SimulationManager.UpdateMode m_loadMode = SimulationManager.UpdateMode.NewGameFromMap;

	public string m_mainScene = "Game";

	public string m_environment = "Sunny";

	public string m_uiScene = "InGame";

	[NonSerialized]
	public static bool m_started;

	private static readonly string format = "Machine ID: {0}\r\nModel: {1}\r\nOS: {6}\r\nLanguage: {7}\r\nCPU: {8} ({9} core(s))\r\nSystem Memory: {10}\r\nGfx Device: {2}\r\nGfx Version: {3}\r\nGfx Memory: {4}\r\nGfx Shader Model: {5}\r\nGame Version: {11}\r\n";

	[CompilerGenerated]
	private static PackageSerializer.CustomSerializeHandler <>f__mg$cache0;

	[CompilerGenerated]
	private static PackageDeserializer.CustomDeserializeHandler <>f__mg$cache1;

	[CompilerGenerated]
	private static PackageDeserializer.UnknownTypeHandler <>f__mg$cache2;

	[CompilerGenerated]
	private static PackageDeserializer.ResolveLegacyTypeHandler <>f__mg$cache3;

	[CompilerGenerated]
	private static PackageDeserializer.ResolveLegacyMemberHandler <>f__mg$cache4;

	[CompilerGenerated]
	private static DataSerializer.LegacyResolverHandler <>f__mg$cache5;

	[CompilerGenerated]
	private static PluginManager.AddMessageHandler <>f__mg$cache6;

	[CompilerGenerated]
	private static LocaleManager.LocaleNeedsSubstitutionHandler <>f__mg$cache7;
}
