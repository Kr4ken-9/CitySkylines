﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class TestBindEvent : MonoBehaviour
{
	public void OnUserClickedSubmit()
	{
		UICheckBox component = base.GetComponent<UICheckBox>();
		if (component != null)
		{
			component.set_isChecked(!component.get_isChecked());
		}
		Debug.Log("Submit Clicked");
	}

	public string m_TestString;
}
