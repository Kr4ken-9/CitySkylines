﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class CategoryContentPanel : UICustomControl
{
	public bool DontRequestDetails
	{
		set
		{
			this.m_DontRequestDetails = value;
		}
	}

	public void Initialize(string templateName, Package.AssetType assetType = null, bool onlyMain = false)
	{
		if (this.m_Initialized)
		{
			return;
		}
		this.m_ScrollablePanel = (UIScrollablePanel)base.get_component();
		this.m_ScrollablePanel.set_autoLayout(false);
		this.m_ScrollablePanel.add_eventScrollPositionChanged(new PropertyChangedEventHandler<Vector2>(this.OnScrollPositionChanged));
		this.m_ScrollablePanel.add_eventSizeChanged(new PropertyChangedEventHandler<Vector2>(this.OnSizeChanged));
		this.m_TemplateName = templateName;
		this.m_AssetType = assetType;
		this.m_OnlyMain = onlyMain;
		this.m_Search = string.Empty;
		this.m_TemplateSize = UITemplateManager.Get<UIComponent>(this.m_TemplateName).get_size();
		this.m_Assets = new List<EntryData>();
		this.m_VisibleAssets = new List<EntryData>();
		this.m_DetailsPending = new Dictionary<PublishedFileId, List<EntryData>>();
		this.m_SortBy = base.get_component().get_parent().get_parent().Find<UIDropDown>("SortBy");
		if (this.m_SortBy != null)
		{
			this.m_SortBy.add_eventDropdownClose(new UIDropDown.PopupEventHandler(this.OnSortSelectionChanged));
		}
		if (CategoryContentPanel.<>f__mg$cache0 == null)
		{
			CategoryContentPanel.<>f__mg$cache0 = new Comparison<EntryData>(EntryData.SortByName);
		}
		this.m_SortImpl = CategoryContentPanel.<>f__mg$cache0;
		this.m_SortOrder = base.get_component().get_parent().get_parent().Find<UIDropDown>("SortOrder");
		if (this.m_SortOrder != null)
		{
			this.m_SortOrder.add_eventDropdownClose(new UIDropDown.PopupEventHandler(this.OnSortSelectionChanged));
		}
		this.m_SelectAll = base.get_component().get_parent().get_parent().Find<UIButton>("SelectAll");
		if (this.m_SelectAll != null)
		{
			this.m_SelectAll.add_eventClick(delegate(UIComponent comp, UIMouseEventParameter eventParam)
			{
				this.SelectAllFiltered(true);
			});
		}
		this.m_DeselectAll = base.get_component().get_parent().get_parent().Find<UIButton>("DeselectAll");
		if (this.m_DeselectAll != null)
		{
			this.m_DeselectAll.add_eventClick(delegate(UIComponent comp, UIMouseEventParameter eventParam)
			{
				this.SelectAllFiltered(false);
			});
		}
		this.m_SelectCountLabel = base.get_component().get_parent().get_parent().Find<UILabel>("SelectCountLabel");
		if (this.m_SelectCountLabel != null)
		{
			this.m_SelectCountLabel.Hide();
		}
		this.m_Initialized = true;
		this.RefreshEntryComponents();
		this.RefreshEntries();
	}

	protected void OnEnable()
	{
		PlatformService.get_workshop().add_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
	}

	protected void OnDisable()
	{
		PlatformService.get_workshop().remove_eventUGCRequestUGCDetailsCompleted(new Workshop.UGCDetailsHandler(this.OnDetailsReceived));
	}

	public void Refresh()
	{
		if (!this.m_Initialized)
		{
			return;
		}
		this.RefreshAssets();
	}

	private void RefreshAssets()
	{
		this.m_Assets = new List<EntryData>();
		if (this.m_SelectCountLabel != null)
		{
			this.m_SelectCountLabel.Hide();
		}
		if (this.m_TemplateName == CategoryContentPanel.kModEntryTemplate)
		{
			this.RefreshAssetsModImpl();
		}
		else if (this.m_TemplateName == CategoryContentPanel.kWorkshopEntryTemplate)
		{
			this.RefreshAssetsWorkshopImpl();
		}
		else
		{
			this.RefreshAssetsImpl();
		}
		if (!this.m_DontRequestDetails)
		{
			this.RequestDetails();
		}
		this.SortEntries();
		this.RefreshVisibleAssets();
		this.UpdateScrollbar();
		this.RefreshEntries();
	}

	private void RefreshAssetsImpl()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			this.m_AssetType
		}))
		{
			if (!(this.m_AssetType == UserAssetType.SaveGameMetaData) || !PackageHelper.IsDemoModeSave(current))
			{
				if (!this.m_OnlyMain || current.get_isMainAsset())
				{
					if (!(this.m_AssetType == UserAssetType.DistrictStyleMetaData) || !(current.get_name() == DistrictStyle.kEuropeanSuburbiaStyleName) || SteamHelper.IsDLCOwned(SteamHelper.DLC.ModderPack3))
					{
						try
						{
							EntryData item = new EntryData(current);
							this.m_Assets.Add(item);
						}
						catch
						{
							Debug.LogError("Failed to create content manager entry for asset " + current.get_name());
							current.set_isEnabled(false);
						}
					}
				}
			}
		}
	}

	private void RefreshAssetsModImpl()
	{
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			try
			{
				EntryData item = new EntryData(current);
				this.m_Assets.Add(item);
			}
			catch
			{
				Debug.LogError("Failed to create content manager entry for mod " + current.get_name());
				current.set_isEnabled(false);
			}
		}
	}

	private void RefreshAssetsWorkshopImpl()
	{
		PublishedFileId[] subscribedItems = PlatformService.get_workshop().GetSubscribedItems();
		if (subscribedItems != null)
		{
			PublishedFileId[] array = subscribedItems;
			for (int i = 0; i < array.Length; i++)
			{
				PublishedFileId id = array[i];
				EntryData item = new EntryData(id);
				this.m_Assets.Add(item);
			}
		}
	}

	private void RequestDetails()
	{
		this.m_DetailsPending = new Dictionary<PublishedFileId, List<EntryData>>();
		foreach (EntryData current in this.m_Assets)
		{
			PublishedFileId publishedFileId = current.publishedFileId;
			if (publishedFileId != PublishedFileId.invalid)
			{
				List<EntryData> list;
				if (this.m_DetailsPending.TryGetValue(publishedFileId, out list))
				{
					list.Add(current);
				}
				else
				{
					List<EntryData> list2 = new List<EntryData>();
					this.m_DetailsPending[publishedFileId] = list2;
					list2.Add(current);
				}
			}
		}
		base.StartCoroutine(this.RequestDetailsCoroutine());
	}

	[DebuggerHidden]
	private IEnumerator RequestDetailsCoroutine()
	{
		CategoryContentPanel.<RequestDetailsCoroutine>c__Iterator0 <RequestDetailsCoroutine>c__Iterator = new CategoryContentPanel.<RequestDetailsCoroutine>c__Iterator0();
		<RequestDetailsCoroutine>c__Iterator.$this = this;
		return <RequestDetailsCoroutine>c__Iterator;
	}

	private void OnDetailsReceived(UGCDetails details, bool ioError)
	{
		PublishedFileId publishedFileId = details.publishedFileId;
		List<EntryData> list;
		if (this.m_DetailsPending != null && publishedFileId != PublishedFileId.invalid && this.m_DetailsPending.TryGetValue(publishedFileId, out list))
		{
			foreach (EntryData current in list)
			{
				current.OnDetailsReceived(details, ioError);
			}
		}
	}

	private void RefreshVisibleAssets()
	{
		this.m_VisibleAssets = new List<EntryData>();
		CategoryContentPanel.SearchTokens tokens = new CategoryContentPanel.SearchTokens(this.m_Search);
		foreach (EntryData current in this.m_Assets)
		{
			if (current.IsMatch(tokens))
			{
				this.m_VisibleAssets.Add(current);
			}
		}
		this.RefreshSelectCountLabel();
	}

	private void UpdateScrollbar()
	{
		UIScrollbar verticalScrollbar = this.m_ScrollablePanel.get_verticalScrollbar();
		verticalScrollbar.set_maxValue(this.m_TemplateSize.y * (float)this.m_VisibleAssets.Count);
		verticalScrollbar.set_value(Mathf.Clamp(verticalScrollbar.get_value(), 0f, verticalScrollbar.get_maxValue()));
	}

	private void RefreshEntryComponents()
	{
		int num = this.MaxVisibleComponents();
		while (base.get_component().get_components().Count < num)
		{
			PackageEntry packageEntry = UITemplateManager.Get<PackageEntry>(this.m_TemplateName);
			this.m_ScrollablePanel.AttachUIComponent(packageEntry.get_gameObject());
		}
		while (this.m_ScrollablePanel.get_components().Count > num)
		{
			UIComponent uIComponent = base.get_component().get_components()[base.get_component().get_childCount() - 1];
			UITemplateManager.RemoveInstance(this.m_TemplateName, uIComponent);
		}
		foreach (UIComponent current in base.get_component().get_components())
		{
			current.set_size(this.m_TemplateSize);
		}
	}

	private void RefreshEntries()
	{
		Vector2 scrollPosition = this.m_ScrollablePanel.get_scrollPosition();
		int num = 0;
		if (this.m_TemplateSize.y != 0f)
		{
			num = Mathf.FloorToInt(scrollPosition.y / this.m_TemplateSize.y);
		}
		Vector2 vector;
		vector..ctor(scrollPosition.x, scrollPosition.y - (float)num * this.m_TemplateSize.y);
		int childCount = base.get_component().get_childCount();
		for (int i = 0; i < childCount; i++)
		{
			UIComponent uIComponent = base.get_component().get_components()[i];
			PackageEntry component = uIComponent.GetComponent<PackageEntry>();
			int num2 = num + i;
			uIComponent.set_relativePosition(new Vector2(0f, (float)i * this.m_TemplateSize.y) - vector);
			if (num2 < this.m_VisibleAssets.Count)
			{
				uIComponent.Show();
				EntryData entry = this.m_VisibleAssets[num2];
				component.SetEntry(entry);
			}
			else
			{
				component.Reset();
				uIComponent.Hide();
			}
		}
	}

	private void RecycleComponents()
	{
		float y = this.m_TemplateSize.y;
		Vector2 scrollPosition = this.m_ScrollablePanel.get_scrollPosition();
		int num = 0;
		if (y != 0f)
		{
			num = Mathf.FloorToInt(scrollPosition.y / y);
		}
		Vector2 vector;
		vector..ctor(scrollPosition.x, scrollPosition.y - (float)num * y);
		int num2 = this.MaxVisibleComponents();
		int num3 = 0;
		int num4 = 0;
		float num5 = 0f;
		if (y != 0f)
		{
			num5 = (float)Mathf.CeilToInt(base.get_component().get_height() / y) * y - base.get_component().get_size().y;
		}
		int childCount = base.get_component().get_childCount();
		for (int i = 0; i < childCount; i++)
		{
			UIComponent uIComponent = base.get_component().get_components()[i];
			PackageEntry component = uIComponent.GetComponent<PackageEntry>();
			float num6 = 0f;
			int num7 = 0;
			bool flag = false;
			if (uIComponent.get_relativePosition().y > base.get_component().get_size().y + num5)
			{
				num6 = (float)num3 * this.m_TemplateSize.y;
				num7 = num + num3;
				num3++;
				flag = true;
			}
			else if (uIComponent.get_relativePosition().y < -y)
			{
				num6 = (float)(num2 - (num4 + 1)) * y;
				num7 = num + num2 - (num4 + 1);
				num4++;
				flag = true;
			}
			if (flag)
			{
				uIComponent.set_relativePosition(new Vector2(0f, num6) - vector);
				if (num7 < this.m_VisibleAssets.Count)
				{
					uIComponent.Show();
					EntryData entry = this.m_VisibleAssets[num7];
					component.SetEntry(entry);
				}
				else
				{
					uIComponent.Hide();
					component.Reset();
				}
			}
		}
	}

	public void SetSearch(string search)
	{
		if (!this.m_Initialized)
		{
			return;
		}
		if (this.m_Search == search)
		{
			return;
		}
		this.m_Search = search;
		this.RefreshVisibleAssets();
		this.UpdateScrollbar();
		this.RefreshEntries();
	}

	public void SetActiveAll(bool enabled)
	{
		bool flag = true;
		foreach (EntryData current in this.m_VisibleAssets)
		{
			if (current.selected)
			{
				flag = false;
				break;
			}
		}
		foreach (EntryData current2 in this.m_VisibleAssets)
		{
			if (flag || current2.selected)
			{
				if (current2.IsActive() != enabled && Singleton<PopsManager>.get_exists())
				{
					Singleton<PopsManager>.get_instance().BufferUGCManaged(current2.GetNameForTelemetry(), enabled);
				}
				current2.SetActive(enabled);
			}
		}
		if (Singleton<PopsManager>.get_exists())
		{
			Singleton<PopsManager>.get_instance().Push();
		}
		this.RefreshEntries();
	}

	private void SelectAllFiltered(bool select)
	{
		foreach (EntryData current in this.m_VisibleAssets)
		{
			this.SelectAsset(current, select, false);
		}
		this.RefreshSelectCountLabel();
	}

	public void SelectAsset(EntryData data, bool select, bool refresh = false)
	{
		bool flag = false;
		if (!data.selected && select)
		{
			flag = true;
			data.selected = true;
		}
		else if (data.selected && !select)
		{
			flag = true;
			data.selected = false;
		}
		if (refresh && flag)
		{
			this.RefreshSelectCountLabel();
		}
	}

	private void RefreshSelectCountLabel()
	{
		if (this.m_SelectCountLabel != null)
		{
			int num = 0;
			foreach (EntryData current in this.m_VisibleAssets)
			{
				if (current.selected)
				{
					num++;
				}
			}
			if (num > 0)
			{
				this.m_SelectCountLabel.set_text(StringUtils.SafeFormat(Locale.Get("CONTENT_SELECTED_COUNT"), num));
				this.m_SelectCountLabel.Show();
			}
			else
			{
				this.m_SelectCountLabel.Hide();
			}
		}
	}

	private int MaxVisibleComponents()
	{
		int result = 0;
		if (this.m_TemplateSize.y != 0f)
		{
			result = Mathf.CeilToInt(this.m_ScrollablePanel.get_height() / this.m_TemplateSize.y) + 1;
		}
		return result;
	}

	private void OnScrollPositionChanged(UIComponent comp, Vector2 pos)
	{
		this.RecycleComponents();
	}

	private void OnSizeChanged(UIComponent comp, Vector2 size)
	{
		this.m_TemplateSize = new Vector2(size.x, this.m_TemplateSize.y);
		this.UpdateScrollbar();
		if (this.MaxVisibleComponents() != base.get_component().get_childCount())
		{
			this.RefreshEntryComponents();
			this.RefreshEntries();
		}
	}

	private void SortEntries()
	{
		if (this.m_SortImpl != null)
		{
			this.m_Assets.Sort(this.m_SortImpl);
			if (this.m_SortOrder != null && this.m_SortOrder.get_localizedItems()[this.m_SortOrder.get_selectedIndex()] == "PANEL_SORT_DESCENDING")
			{
				this.m_Assets.Reverse();
			}
		}
	}

	protected void OnSortSelectionChanged(UIDropDown dropdown, UIListBox popup, ref bool overridden)
	{
		string key = this.m_SortBy.get_localizedItems()[this.m_SortBy.get_selectedIndex()];
		if (!CategoryContentPanel.m_SortTypeToImplDict.TryGetValue(key, out this.m_SortImpl))
		{
			this.m_SortImpl = null;
		}
		this.SortEntries();
		this.RefreshVisibleAssets();
		this.RefreshEntries();
	}

	public PublishedFileId[] selectedWorkshopItems
	{
		get
		{
			List<PublishedFileId> list = new List<PublishedFileId>();
			foreach (EntryData current in this.m_VisibleAssets)
			{
				PublishedFileId publishedFileId = current.publishedFileId;
				if (current.selected && publishedFileId != PublishedFileId.invalid && !list.Contains(publishedFileId))
				{
					list.Add(publishedFileId);
				}
			}
			return list.ToArray();
		}
	}

	static CategoryContentPanel()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, Comparison<EntryData>> dictionary = new Dictionary<string, Comparison<EntryData>>();
		Dictionary<string, Comparison<EntryData>> arg_3D_0 = dictionary;
		string arg_3D_1 = "PANEL_SORT_NAME";
		if (CategoryContentPanel.<>f__mg$cache1 == null)
		{
			CategoryContentPanel.<>f__mg$cache1 = new Comparison<EntryData>(EntryData.SortByName);
		}
		arg_3D_0.Add(arg_3D_1, CategoryContentPanel.<>f__mg$cache1);
		Dictionary<string, Comparison<EntryData>> arg_65_0 = dictionary;
		string arg_65_1 = "PANEL_SORT_AUTHOR";
		if (CategoryContentPanel.<>f__mg$cache2 == null)
		{
			CategoryContentPanel.<>f__mg$cache2 = new Comparison<EntryData>(EntryData.SortByAuthor);
		}
		arg_65_0.Add(arg_65_1, CategoryContentPanel.<>f__mg$cache2);
		Dictionary<string, Comparison<EntryData>> arg_8D_0 = dictionary;
		string arg_8D_1 = "PANEL_SORT_ACTIVE";
		if (CategoryContentPanel.<>f__mg$cache3 == null)
		{
			CategoryContentPanel.<>f__mg$cache3 = new Comparison<EntryData>(EntryData.SortByActive);
		}
		arg_8D_0.Add(arg_8D_1, CategoryContentPanel.<>f__mg$cache3);
		Dictionary<string, Comparison<EntryData>> arg_B5_0 = dictionary;
		string arg_B5_1 = "PANEL_SORT_MODIFIED";
		if (CategoryContentPanel.<>f__mg$cache4 == null)
		{
			CategoryContentPanel.<>f__mg$cache4 = new Comparison<EntryData>(EntryData.SortByUpdated);
		}
		arg_B5_0.Add(arg_B5_1, CategoryContentPanel.<>f__mg$cache4);
		Dictionary<string, Comparison<EntryData>> arg_DD_0 = dictionary;
		string arg_DD_1 = "PANEL_SORT_SUBSCRIBED";
		if (CategoryContentPanel.<>f__mg$cache5 == null)
		{
			CategoryContentPanel.<>f__mg$cache5 = new Comparison<EntryData>(EntryData.SortBySubscribed);
		}
		arg_DD_0.Add(arg_DD_1, CategoryContentPanel.<>f__mg$cache5);
		Dictionary<string, Comparison<EntryData>> arg_105_0 = dictionary;
		string arg_105_1 = "PANEL_SORT_PATH";
		if (CategoryContentPanel.<>f__mg$cache6 == null)
		{
			CategoryContentPanel.<>f__mg$cache6 = new Comparison<EntryData>(EntryData.SortByPath);
		}
		arg_105_0.Add(arg_105_1, CategoryContentPanel.<>f__mg$cache6);
		CategoryContentPanel.m_SortTypeToImplDict = dictionary;
	}

	private static readonly string kWorkshopEntryTemplate = "WorkshopEntryTemplate";

	private static readonly string kModEntryTemplate = "ModEntryTemplate";

	private List<EntryData> m_Assets;

	private List<EntryData> m_VisibleAssets;

	private string m_TemplateName;

	private Package.AssetType m_AssetType;

	private UIScrollablePanel m_ScrollablePanel;

	private bool m_OnlyMain;

	private string m_Search;

	private Vector2 m_TemplateSize;

	private Dictionary<PublishedFileId, List<EntryData>> m_DetailsPending;

	private bool m_DontRequestDetails;

	private UIDropDown m_SortBy;

	private Comparison<EntryData> m_SortImpl;

	private static readonly Dictionary<string, Comparison<EntryData>> m_SortTypeToImplDict;

	private UIDropDown m_SortOrder;

	private UIButton m_SelectAll;

	private UIButton m_DeselectAll;

	private UILabel m_SelectCountLabel;

	private bool m_Initialized;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache0;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache1;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache2;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache3;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache4;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache5;

	[CompilerGenerated]
	private static Comparison<EntryData> <>f__mg$cache6;

	public struct SearchTokens
	{
		public SearchTokens(string search)
		{
			this.filters = new List<string>();
			this.searchTerms = new List<string>();
			if (!string.IsNullOrEmpty(search))
			{
				string[] array = search.Split(new char[]
				{
					','
				}, StringSplitOptions.RemoveEmptyEntries);
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string text = array2[i];
					string text2 = text.Trim();
					if (text2.StartsWith("t:"))
					{
						if (text2.Length > 2)
						{
							string text3 = text2.Substring(2).Trim();
							if (text3.Length > 0)
							{
								this.filters.Add(text3);
							}
						}
					}
					else if (text2.Length > 0)
					{
						this.searchTerms.Add(text2);
					}
				}
			}
		}

		public List<string> filters;

		public List<string> searchTerms;
	}
}
