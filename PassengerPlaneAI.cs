﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PassengerPlaneAI : AircraftAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_arriveEffect != null)
		{
			this.m_arriveEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_arriveEffect != null)
		{
			this.m_arriveEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			ushort transportLine = data.m_transportLine;
			if (transportLine != 0)
			{
				return Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].GetColor();
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		else if (infoMode == InfoManager.InfoMode.Connections)
		{
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode == InfoManager.SubInfoMode.WindPower && (data.m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(vehicleID, ref data, infoMode);
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_AIRPLANE_BOARDING");
		}
		if ((data.m_flags & Vehicle.Flags.Landing) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_AIRPLANE_LANDING");
		}
		if ((data.m_flags & Vehicle.Flags.TakingOff) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_AIRPLANE_TAKING_OFF");
		}
		if ((data.m_flags & Vehicle.Flags.Flying) == (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_AIRPLANE_TAXIING");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_RETURN");
		}
		if (data.m_targetBuilding != 0)
		{
			if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_AIRPLANE_FLYING");
			}
			Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_targetBuilding].m_position;
			ushort num = Singleton<BuildingManager>.get_instance().FindTransportBuilding(position, 128f, this.m_transportInfo.m_transportType);
			if (num != 0)
			{
				ushort num2 = Building.FindParentBuilding(num);
				if (num2 != 0)
				{
					num = num2;
				}
				target = InstanceID.Empty;
				target.Building = num;
				return Locale.Get("VEHICLE_STATUS_AIRPLANE_FLYING");
			}
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_AIRPLANE_BOARDING");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = (int)data.m_transferSize;
		max = this.m_passengerCapacity;
		if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			current = randomizer.Int32(max >> 1, max);
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_passengerCapacity, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		this.RemoveLine(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0 && data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].AddVehicle(vehicleID, ref data, false);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		bool flag = data.m_sourceBuilding != 0;
		if (flag)
		{
			this.RemoveSource(vehicleID, ref data);
		}
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			if (!flag)
			{
				data.Unspawn(vehicleID);
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
				Randomizer randomizer = default(Randomizer);
				randomizer.seed = (ulong)data.m_gateIndex;
				Vector3 vector;
				Vector3 vector2;
				info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
				Quaternion rotation = Quaternion.get_identity();
				Vector3 vector3 = vector2 - vector;
				if (vector3.get_sqrMagnitude() > 0.01f)
				{
					rotation = Quaternion.LookRotation(vector3);
				}
				data.m_frame0 = new Vehicle.Frame(vector, rotation);
				data.m_frame1 = data.m_frame0;
				data.m_frame2 = data.m_frame0;
				data.m_frame3 = data.m_frame0;
				data.m_targetPos0 = vector + Vector3.ClampMagnitude(vector2 - vector, 0.5f);
				data.m_targetPos0.w = 2f;
				data.m_targetPos1 = data.m_targetPos0;
				data.m_targetPos2 = data.m_targetPos0;
				data.m_targetPos3 = data.m_targetPos0;
				this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			}
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			if (targetBuilding != data.m_targetBuilding)
			{
				this.RemoveTarget(vehicleID, ref data);
				data.m_targetBuilding = targetBuilding;
				if (targetBuilding != 0)
				{
					Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
				}
			}
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		else
		{
			data.m_targetBuilding = targetBuilding;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (building == data.m_sourceBuilding)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0 && building == data.m_targetBuilding)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void SetTransportLine(ushort vehicleID, ref Vehicle data, ushort transportLine)
	{
		this.RemoveLine(vehicleID, ref data);
		data.m_transportLine = transportLine;
		if (transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].AddVehicle(vehicleID, ref data, true);
		}
		else
		{
			data.m_flags |= Vehicle.Flags.GoingBack;
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == (TransferManager.TransferReason)data.m_transferType)
		{
			if (offer.TransportLine != 0)
			{
				this.SetTransportLine(vehicleID, ref data, offer.TransportLine);
			}
			else if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0 && offer.Building != 0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, reason, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = this.m_passengerCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			vehicleData.m_waitCounter += 1;
			if (this.CanLeave(vehicleID, ref vehicleData))
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
				vehicleData.m_flags |= Vehicle.Flags.Leaving;
				vehicleData.m_waitCounter = 0;
			}
		}
		else if ((vehicleData.m_flags & (Vehicle.Flags.GoingBack | Vehicle.Flags.DummyTraffic)) == (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & NetNode.Flags.Disabled) != NetNode.Flags.None)
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0 && data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private void RemoveLine(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].RemoveVehicle(vehicleID, ref data);
			data.m_transportLine = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0 || (data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		ushort targetBuilding = data.m_targetBuilding;
		ushort num = 0;
		if (data.m_transportLine != 0)
		{
			num = TransportLine.GetNextStop(data.m_targetBuilding);
		}
		else if ((data.m_flags & (Vehicle.Flags.Importing | Vehicle.Flags.Exporting)) != (Vehicle.Flags)0)
		{
			num = TransportLine.GetNextStop(data.m_targetBuilding);
			Vector3 lastFramePosition = data.GetLastFramePosition();
			if (Mathf.Max(Mathf.Abs(lastFramePosition.x), Mathf.Abs(lastFramePosition.z)) > 4800f && this.CheckPassengers(vehicleID, ref data, targetBuilding, num) == 0)
			{
				num = 0;
			}
		}
		this.UnloadPassengers(vehicleID, ref data, targetBuilding, num);
		if (num == 0)
		{
			data.m_waitCounter = 0;
			data.m_flags |= Vehicle.Flags.WaitingLoading;
		}
		else
		{
			data.m_targetBuilding = num;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				return true;
			}
			this.LoadPassengers(vehicleID, ref data, targetBuilding, num);
			data.m_flags &= ~Vehicle.Flags.Arriving;
			data.m_flags |= Vehicle.Flags.Stopped;
			data.m_waitCounter = 0;
		}
		return false;
	}

	private void UnloadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		TransportManager instance2 = Singleton<TransportManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 targetPos = Vector3.get_zero();
		if (nextStop != 0)
		{
			targetPos = instance.m_nodes.m_buffer[(int)nextStop].m_position;
		}
		int num = 0;
		if (data.m_transportLine != 0)
		{
			BusAI.TransportArriveAtTarget(vehicleID, ref data, position, targetPos, ref num, ref instance2.m_lines.m_buffer[(int)data.m_transportLine].m_passengers, nextStop == 0);
		}
		else
		{
			BusAI.TransportArriveAtTarget(vehicleID, ref data, position, targetPos, ref num, ref instance2.m_passengers[(int)this.m_transportInfo.m_transportType], nextStop == 0);
		}
		StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticArray>(StatisticType.PassengerCount);
		statisticBase.Acquire<StatisticInt32>((int)this.m_transportInfo.m_transportType, 10).Add(num);
		num += (int)instance.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		instance.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num, 65535);
	}

	private int CheckPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0 || nextStop == 0)
		{
			return 0;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		if (instance2.m_nodes.m_buffer[(int)currentStop].m_maxWaitTime != 255)
		{
			return 0;
		}
		Vector3 position = instance2.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		int num = Mathf.Max((int)((position.x - 64f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - 64f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + 64f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + 64f) / 8f + 1080f), 2159);
		int num5 = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_citizenGrid[i * 2160 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num6].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num6].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num6].m_targetPos;
						float num8 = Vector3.SqrMagnitude(vector - position);
						if (num8 < 4096f)
						{
							CitizenInfo info = instance.m_instances.m_buffer[(int)num6].Info;
							if (info.m_citizenAI.TransportArriveAtSource(num6, ref instance.m_instances.m_buffer[(int)num6], position, position2))
							{
								num5++;
							}
						}
					}
					num6 = nextGridInstance;
					if (++num7 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num5;
	}

	private void LoadPassengers(ushort vehicleID, ref Vehicle data, ushort currentStop, ushort nextStop)
	{
		if (currentStop == 0 || nextStop == 0)
		{
			return;
		}
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector3 position = instance2.m_nodes.m_buffer[(int)currentStop].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)nextStop].m_position;
		instance2.m_nodes.m_buffer[(int)currentStop].m_maxWaitTime = 0;
		int num = Mathf.Max((int)((position.x - 64f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - 64f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + 64f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + 64f) / 8f + 1080f), 2159);
		int num5 = (int)instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter;
		bool flag = false;
		int num6 = num2;
		while (num6 <= num4 && !flag)
		{
			int num7 = num;
			while (num7 <= num3 && !flag)
			{
				ushort num8 = instance.m_citizenGrid[num6 * 2160 + num7];
				int num9 = 0;
				while (num8 != 0 && !flag)
				{
					ushort nextGridInstance = instance.m_instances.m_buffer[(int)num8].m_nextGridInstance;
					if ((instance.m_instances.m_buffer[(int)num8].m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num8].m_targetPos;
						float num10 = Vector3.SqrMagnitude(vector - position);
						if (num10 < 4096f)
						{
							CitizenInfo info = instance.m_instances.m_buffer[(int)num8].Info;
							if (info.m_citizenAI.TransportArriveAtSource(num8, ref instance.m_instances.m_buffer[(int)num8], position, position2))
							{
								if (info.m_citizenAI.SetCurrentVehicle(num8, ref instance.m_instances.m_buffer[(int)num8], vehicleID, 0u, position))
								{
									num5++;
									data.m_transferSize += 1;
								}
								else
								{
									flag = true;
								}
							}
						}
					}
					num8 = nextGridInstance;
					if (++num9 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				num7++;
			}
			num6++;
		}
		instance2.m_nodes.m_buffer[(int)currentStop].m_tempCounter = (ushort)Mathf.Min(num5, 65535);
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingLoading) == (Vehicle.Flags)0)
		{
			return this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		vehicleData.m_waitCounter = (byte)Mathf.Min((int)(vehicleData.m_waitCounter + 1), 255);
		if (vehicleData.m_waitCounter >= 16)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		return false;
	}

	protected override void ArrivingToDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		base.ArrivingToDestination(vehicleID, ref vehicleData);
		if (this.m_arriveEffect != null)
		{
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				if (vehicleData.m_sourceBuilding != 0)
				{
					Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].m_position;
					InstanceID instance = default(InstanceID);
					instance.Building = vehicleData.m_sourceBuilding;
					EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(position, Vector3.get_up(), 0f);
					Singleton<EffectManager>.get_instance().DispatchEffect(this.m_arriveEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<BuildingManager>.get_instance().m_audioGroup);
				}
			}
			else if ((vehicleData.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
			{
				if (vehicleData.m_targetBuilding != 0)
				{
					Vector3 position2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
					InstanceID instance2 = default(InstanceID);
					instance2.Building = vehicleData.m_targetBuilding;
					EffectInfo.SpawnArea spawnArea2 = new EffectInfo.SpawnArea(position2, Vector3.get_up(), 0f);
					Singleton<EffectManager>.get_instance().DispatchEffect(this.m_arriveEffect, instance2, spawnArea2, Vector3.get_zero(), 0f, 1f, Singleton<BuildingManager>.get_instance().m_audioGroup);
				}
			}
			else if (vehicleData.m_targetBuilding != 0)
			{
				Vector3 position3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
				InstanceID instance3 = default(InstanceID);
				instance3.NetNode = vehicleData.m_targetBuilding;
				EffectInfo.SpawnArea spawnArea3 = new EffectInfo.SpawnArea(position3, Vector3.get_up(), 0f);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_arriveEffect, instance3, spawnArea3, Vector3.get_zero(), 0f, 1f, Singleton<BuildingManager>.get_instance().m_audioGroup);
			}
		}
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				Vector4 pos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].m_position;
				pos.w = 2f;
				vehicleData.SetTargetPos(index++, pos);
				return;
			}
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].m_position;
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos0, position);
			}
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_targetBuilding != 0)
			{
				Vector3 position2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos0, position2);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			Vector3 position3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)vehicleData.m_targetBuilding].m_position;
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos0, position3);
		}
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		return vehicleData.m_waitCounter >= 12 && base.CanLeave(vehicleID, ref vehicleData);
	}

	public override int GetTicketPrice(ushort vehicleID, ref Vehicle vehicleData)
	{
		return this.m_ticketPrice;
	}

	public override InstanceID GetOwnerID(ushort vehicleID, ref Vehicle vehicleData)
	{
		return default(InstanceID);
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		else
		{
			result.NetNode = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public EffectInfo m_arriveEffect;

	public TransportInfo m_transportInfo;

	[CustomizableProperty("Passenger capacity")]
	public int m_passengerCapacity = 30;

	public int m_ticketPrice = 1000;
}
