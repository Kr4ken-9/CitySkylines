﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class LoadingWrapper : ILoading
{
	public LoadingWrapper(LoadingManager loadingManager)
	{
		this.m_loadingManager = loadingManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.m_LoadingExtensions = Singleton<PluginManager>.get_instance().GetImplementations<ILoadingExtension>();
		this.OnLoadingExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnLoadingExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnLoadingExtensionsCreated()
	{
		for (int i = 0; i < this.m_LoadingExtensions.Count; i++)
		{
			try
			{
				this.m_LoadingExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_LoadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnLoadingExtensionsReleased()
	{
		for (int i = 0; i < this.m_LoadingExtensions.Count; i++)
		{
			try
			{
				this.m_LoadingExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_LoadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		for (int i = 0; i < this.m_LoadingExtensions.Count; i++)
		{
			try
			{
				this.m_LoadingExtensions[i].OnLevelLoaded(mode);
			}
			catch (Exception ex2)
			{
				Type type = this.m_LoadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnLevelUnloading()
	{
		for (int i = 0; i < this.m_LoadingExtensions.Count; i++)
		{
			try
			{
				this.m_LoadingExtensions[i].OnLevelUnloading();
			}
			catch (Exception ex2)
			{
				Type type = this.m_LoadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public bool loadingComplete
	{
		get
		{
			return this.m_loadingManager.m_loadingComplete;
		}
	}

	public AppMode currentMode
	{
		get
		{
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			switch (mode)
			{
			case ItemClass.Availability.Game:
				return 0;
			case ItemClass.Availability.MapEditor:
				return 1;
			case ItemClass.Availability.GameAndMap:
			case ItemClass.Availability.GameAndAsset:
			case ItemClass.Availability.MapAndAsset:
			case ItemClass.Availability.Game | ItemClass.Availability.MapEditor | ItemClass.Availability.AssetEditor:
				IL_38:
				if (mode != ItemClass.Availability.ScenarioEditor)
				{
					return 0;
				}
				return 4;
			case ItemClass.Availability.AssetEditor:
				return 2;
			case ItemClass.Availability.ThemeEditor:
				return 3;
			}
			goto IL_38;
		}
	}

	public string currentTheme
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_metaData.m_environment;
		}
		set
		{
			Singleton<SimulationManager>.get_instance().m_metaData.m_environment = value;
		}
	}

	private LoadingManager m_loadingManager;

	private List<ILoadingExtension> m_LoadingExtensions;
}
