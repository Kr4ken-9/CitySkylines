﻿using System;
using ColossalFramework.UI;
using UnityEngine;

[RequireComponent(typeof(UITextField))]
public class PercentageSliderTextField : MonoBehaviour
{
	private void Awake()
	{
		this.m_textField = base.GetComponent<UITextField>();
		this.m_textField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextSubmitted));
		this.m_slider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		this.OnSliderValueChanged(null, 0f);
	}

	private void OnTextSubmitted(UIComponent component, string value)
	{
		this.m_slider.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		int num;
		if (int.TryParse(value, out num))
		{
			this.m_slider.set_value((float)num);
		}
		this.m_slider.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnSliderValueChanged));
		this.OnSliderValueChanged(null, 0f);
	}

	private void OnSliderValueChanged(UIComponent component, float value)
	{
		this.m_textField.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextSubmitted));
		this.m_textField.set_text(this.m_slider.get_value().ToString());
		this.m_textField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextSubmitted));
	}

	private UITextField m_textField;

	public UISlider m_slider;
}
