﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class FirewatchTowerAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.FireSafety)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.FirewatchCoverage;
		radius1 = this.m_firewatchRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.FireSafety;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_firewatchRadius != 0f)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.FirewatchCoverage, 1000f, this.m_firewatchRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_firewatchRadius != 0f)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.FirewatchCoverage, -1000f, this.m_firewatchRadius);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_firewatchRadius * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = finalProductionRate * 10;
		if (num != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_generatedInfo.m_size.y;
			Singleton<ImmaterialResourceManager>.get_instance().AddObstructedResource(ImmaterialResourceManager.Resource.FirewatchCoverage, num, position, this.m_firewatchRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override bool NearObjectInFire(ushort buildingID, ref Building data, InstanceID itemID, Vector3 itemPos)
	{
		if (itemID.Type != InstanceType.Tree)
		{
			return false;
		}
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		base.CalculateGuestVehicles(buildingID, ref data, TransferManager.TransferReason.ForestFire, ref num, ref num2, ref num3, ref num4);
		if (num < 3)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = Mathf.Max(8 - num - 1, 4);
			offer.Building = buildingID;
			offer.Position = data.m_position;
			offer.Amount = 1;
			Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ForestFire, offer);
		}
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(itemID);
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterManager instance = Singleton<DisasterManager>.get_instance();
				if ((instance.m_disasters.m_buffer[(int)disaster].m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
				{
					instance.DetectDisaster(disaster, true);
				}
			}
		}
		return true;
	}

	public override float MaxFireDetectDistance(ushort buildingID, ref Building data)
	{
		return this.GetCurrentRange(buildingID, ref data);
	}

	public override float ElectricityGridRadius()
	{
		if (this.m_electricityConsumption == 0)
		{
			return 0f;
		}
		return base.ElectricityGridRadius();
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_FIREWATCH_TOWER_HEIGHT", new object[]
			{
				Mathf.RoundToInt(this.m_info.m_generatedInfo.m_size.y)
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		int num2 = Mathf.RoundToInt((float)num * this.m_firewatchRadius * 0.01f);
		return LocaleFormatter.FormatGeneric("AIINFO_FIREWATCH_VISIBILITY_RADIUS", new object[]
		{
			num2
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 9;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 5;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 1;

	[CustomizableProperty("Firewatch Radius")]
	public float m_firewatchRadius = 1000f;
}
