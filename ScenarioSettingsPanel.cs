﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class ScenarioSettingsPanel : ToolsModifierControl
{
	private void OnEnable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.RefreshModsList));
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.RefreshModsList));
	}

	private void Start()
	{
		this.m_numberFieldMoney = base.Find<UIPanel>("NumberFieldMoney").GetComponent<NumberField>();
		this.m_numberFieldMoney.eventValueChanged += new NumberField.ValueChangedHandler(this.OnPlayerMoneyValueChanged);
		UIPanel container = base.Find<UIPanel>("BuiltinModsPanel");
		this.m_modsList = new UITemplateList<UIComponent>(container, "BuiltinModTemplate");
		this.RefreshModsList();
	}

	private void RefreshModsList()
	{
		this.m_modsList.Clear();
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			try
			{
				EntryData entryData = new EntryData(current);
				if (entryData.pluginInfo.get_isBuiltin())
				{
					string text = string.Empty;
					IUserMod[] instances = entryData.pluginInfo.GetInstances<IUserMod>();
					if (instances.Length == 1)
					{
						text = instances[0].get_Name();
					}
					string text2 = "Builtin/" + entryData.pluginInfo.get_name();
					UIComponent uIComponent = this.m_modsList.AddItem();
					UICheckBox uICheckBox = uIComponent.Find<UICheckBox>("Checkbox");
					uICheckBox.set_isChecked(this.IsModEnabled(text2));
					uICheckBox.set_objectUserData(text2);
					uICheckBox.add_eventCheckChanged(delegate(UIComponent comp, bool value)
					{
						string name = (string)comp.get_objectUserData();
						this.SetModEnabled(name, value);
					});
					UILabel uILabel = uIComponent.Find<UILabel>("Label");
					uILabel.set_text(Locale.Get("MOD_NAME", text));
					uIComponent.set_tooltip(Locale.Get("MOD_DESCRIPTION", text));
				}
			}
			catch
			{
				Debug.LogError("Failed to create entry for mod " + current.get_name());
				current.set_isEnabled(false);
			}
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		if (updateMode == SimulationManager.UpdateMode.NewScenarioFromGame)
		{
			Singleton<EconomyManager>.get_instance().StartMoney = Singleton<EconomyManager>.get_instance().LastCashAmount;
		}
	}

	private void OnPlayerMoneyValueChanged(int newValue)
	{
		long num = (long)newValue;
		num *= 100L;
		Singleton<EconomyManager>.get_instance().StartMoney = num;
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			long num = Singleton<EconomyManager>.get_instance().StartMoney / 100L;
			this.m_numberFieldMoney.value = (int)num;
			float num2 = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num2 + this.m_SafeMargin, num2 - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public void OnCloseButton()
	{
		base.CloseToolbar();
	}

	private bool IsModEnabled(string name)
	{
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		while (!Monitor.TryEnter(metaData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		bool result;
		try
		{
			bool flag;
			if (metaData.m_modOverride != null && metaData.m_modOverride.TryGetValue(name, out flag))
			{
				result = flag;
			}
			else
			{
				result = false;
			}
		}
		finally
		{
			Monitor.Exit(metaData);
		}
		return result;
	}

	private void SetModEnabled(string name, bool value)
	{
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		while (!Monitor.TryEnter(metaData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (metaData.m_modOverride == null)
			{
				metaData.m_modOverride = new Dictionary<string, bool>();
			}
			metaData.m_modOverride[name] = value;
		}
		finally
		{
			Monitor.Exit(metaData);
		}
	}

	public void OnSnapshotButton()
	{
		ToolsModifierControl.SetTool<SnapshotTool>();
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private NumberField m_numberFieldMoney;

	private UITemplateList<UIComponent> m_modsList;
}
