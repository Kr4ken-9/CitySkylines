﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class WaterTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
		this.m_levelMaterial2 = new Material(this.m_levelMaterial);
		this.m_propertyBlock = new MaterialPropertyBlock();
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_Color2 = Shader.PropertyToID("_Color2");
		this.ID_WaterLevel = Shader.PropertyToID("_WaterLevel");
		this.m_dragIndex = -1;
		this.m_heightIndex = -1;
		this.m_dragHoverIndex = -1;
		this.m_heightHoverIndex = -1;
		this.m_waitSimulationStep = true;
		this.m_height = 50f;
	}

	protected override void OnDestroy()
	{
		if (this.m_levelMaterial2 != null)
		{
			Object.Destroy(this.m_levelMaterial2);
			this.m_levelMaterial2 = null;
		}
		base.OnDestroy();
	}

	protected override void OnToolGUI(Event e)
	{
		if (!this.m_waitSimulationStep)
		{
			if (!this.m_toolController.IsInsideUI && e.get_type() == null)
			{
				if (e.get_button() == 0)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.PrimaryDown());
				}
				else if (e.get_button() == 1)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.SecondaryDown());
				}
			}
			else if (e.get_type() == 1 && e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.PrimaryUp());
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		Singleton<TerrainManager>.get_instance().TransparentWater = true;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Singleton<TerrainManager>.get_instance().TransparentWater = false;
		this.m_waterLevel = 0f;
		this.m_waitSimulationStep = true;
		this.m_mouseRayValid = false;
	}

	public override void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
		bool flag = !this.m_toolController.IsInsideUI && Cursor.get_visible();
		if (!this.m_waitSimulationStep && this.m_mode == WaterTool.Mode.WaterSource)
		{
			FastList<WaterSource> waterSources = Singleton<TerrainManager>.get_instance().WaterSimulation.m_waterSources;
			for (int i = 0; i < waterSources.m_size; i++)
			{
				if (waterSources.m_buffer[i].m_type == 1)
				{
					bool drag = flag && (i == this.m_dragIndex || i == this.m_dragHoverIndex);
					bool height = flag && (i == this.m_heightIndex || i == this.m_heightHoverIndex);
					this.DrawSource(ref waterSources.m_buffer[i], false, drag, height);
				}
			}
			if (flag && this.m_tempSource.m_type == 1)
			{
				this.DrawSource(ref this.m_tempSource, true, false, false);
			}
		}
		base.RenderGeometry(cameraInfo);
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_waterLevel != 0f && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			this.m_levelMaterial2.SetFloat(this.ID_WaterLevel, this.m_waterLevel);
			Vector3 vector;
			vector..ctor(0f, 512f, 0f);
			Vector3 vector2;
			vector2..ctor(17280f, 1026f, 17280f);
			Bounds bounds;
			bounds..ctor(vector, vector2);
			ToolManager expr_80_cp_0 = Singleton<ToolManager>.get_instance();
			expr_80_cp_0.m_drawCallData.m_overlayCalls = expr_80_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawEffect(cameraInfo, this.m_levelMaterial2, 0, bounds);
		}
		base.RenderOverlay(cameraInfo);
	}

	protected override void OnToolUpdate()
	{
		if (this.m_mode == WaterTool.Mode.SeaLevel)
		{
			if (this.m_heightIndex != -1 || this.m_heightHoverIndex != -1)
			{
				base.ToolCursor = this.m_sealevelCursor;
			}
			else
			{
				base.ToolCursor = null;
			}
		}
		else if (this.m_dragIndex != -1 || this.m_dragHoverIndex != -1)
		{
			base.ToolCursor = this.m_dragCursor;
		}
		else if (this.m_heightIndex != -1 || this.m_heightHoverIndex != -1)
		{
			base.ToolCursor = this.m_heightCursor;
		}
		else
		{
			base.ToolCursor = this.m_placementCursor;
		}
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		ToolBase.OverrideInfoMode = false;
	}

	private void DrawSource(ref WaterSource source, bool temp, bool drag, bool height)
	{
		float num = Mathf.Sqrt(Mathf.Abs(source.m_inputRate)) * 0.4f + 10f;
		float num2 = (float)source.m_target * 0.015625f - source.m_inputPosition.y;
		Vector3 vector = source.m_inputPosition + new Vector3(0f, num2 * 0.5f, 0f);
		Vector3 vector2;
		vector2..ctor(num * 2f, num2 * 0.5f, num * 2f);
		Matrix4x4 matrix4x = Matrix4x4.TRS(vector, Quaternion.get_identity(), vector2);
		this.m_propertyBlock.Clear();
		if (temp)
		{
			this.m_propertyBlock.SetColor(this.ID_Color, new Color(0.5f, 0.5f, 0.5f, 0.2f));
			this.m_propertyBlock.SetColor(this.ID_Color2, new Color(0.5f, 0.5f, 0.5f, 0.4f));
		}
		else if (drag)
		{
			this.m_propertyBlock.SetColor(this.ID_Color, new Color(0.5f, 0.5f, 0.5f, 0.4f));
		}
		else if (height)
		{
			this.m_propertyBlock.SetColor(this.ID_Color2, new Color(1f, 1f, 1f, 1f));
		}
		ToolManager expr_15F_cp_0 = Singleton<ToolManager>.get_instance();
		expr_15F_cp_0.m_drawCallData.m_defaultCalls = expr_15F_cp_0.m_drawCallData.m_defaultCalls + 1;
		Graphics.DrawMesh(this.m_sourceMesh, matrix4x, this.m_sourceMaterial, 0, null, 0, this.m_propertyBlock, false, false);
	}

	[DebuggerHidden]
	private IEnumerator PrimaryDown()
	{
		WaterTool.<PrimaryDown>c__Iterator0 <PrimaryDown>c__Iterator = new WaterTool.<PrimaryDown>c__Iterator0();
		<PrimaryDown>c__Iterator.$this = this;
		return <PrimaryDown>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator PrimaryUp()
	{
		WaterTool.<PrimaryUp>c__Iterator1 <PrimaryUp>c__Iterator = new WaterTool.<PrimaryUp>c__Iterator1();
		<PrimaryUp>c__Iterator.$this = this;
		return <PrimaryUp>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SecondaryDown()
	{
		WaterTool.<SecondaryDown>c__Iterator2 <SecondaryDown>c__Iterator = new WaterTool.<SecondaryDown>c__Iterator2();
		<SecondaryDown>c__Iterator.$this = this;
		return <SecondaryDown>c__Iterator;
	}

	private void PlaceSource()
	{
		ushort num;
		if (Singleton<TerrainManager>.get_instance().WaterSimulation.CreateWaterSource(out num, this.m_tempSource))
		{
			this.m_tempSource.m_type = 0;
		}
	}

	private void RemoveSource()
	{
		ushort source = (ushort)(((this.m_dragHoverIndex == -1) ? this.m_heightHoverIndex : this.m_dragHoverIndex) + 1);
		Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(source);
		this.m_dragHoverIndex = -1;
		this.m_heightHoverIndex = -1;
	}

	private bool RayCastSource(ref WaterSource source, out float distance, out bool top, out Vector3 position)
	{
		distance = 0f;
		top = false;
		position = Vector3.get_zero();
		source.m_inputPosition.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(source.m_inputPosition);
		source.m_outputPosition.y = source.m_inputPosition.y;
		float num = Mathf.Sqrt(Mathf.Abs(source.m_inputRate)) * 0.4f + 10f;
		float num2 = (float)source.m_target * 0.015625f - source.m_inputPosition.y;
		Segment3 segment;
		segment..ctor(this.m_mouseRay.get_origin(), this.m_mouseRay.get_origin() + this.m_mouseRay.get_direction().get_normalized() * this.m_mouseRayLength);
		Segment3 segment2 = segment;
		float num3;
		if (Segment1.Intersect(segment.a.y, segment.b.y, source.m_inputPosition.y + num2, ref num3))
		{
			segment2.a = segment.Position(num3);
			if (Vector2.SqrMagnitude(VectorUtils.XZ(segment2.a) - VectorUtils.XZ(source.m_inputPosition)) <= num * num)
			{
				distance = num3 * this.m_mouseRayLength;
				top = true;
				position = segment2.a;
				return true;
			}
		}
		else if (segment.a.y > source.m_inputPosition.y + num2)
		{
			return false;
		}
		if (Segment1.Intersect(segment.a.y, segment.b.y, source.m_inputPosition.y, ref num3))
		{
			segment2.b = segment.Position(num3);
		}
		else if (segment.a.y < source.m_inputPosition.y)
		{
			return false;
		}
		Vector3 vector = segment2.a - source.m_inputPosition;
		Vector3 vector2 = segment2.b - source.m_inputPosition;
		Vector3 vector3 = vector2 - vector;
		float num4 = 2f * (vector3.x * vector3.x + vector3.z * vector3.z);
		float num5 = 2f * (vector.x * vector3.x + vector.z * vector3.z);
		float num6 = vector.x * vector.x + vector.z * vector.z - num * num;
		float num7 = num5 * num5 - 2f * num4 * num6;
		if (num4 == 0f || num7 < 0f)
		{
			return false;
		}
		num7 = Mathf.Sqrt(num7);
		num3 = Mathf.Min((num7 - num5) / num4, -(num7 + num5) / num4);
		if (num3 >= 0f && num3 <= 1f)
		{
			position = segment2.Position(num3);
			distance = Vector3.Distance(position, segment.a);
			top = false;
			return true;
		}
		return false;
	}

	private bool RayCastSea(out Vector3 position)
	{
		Segment3 segment;
		segment..ctor(this.m_mouseRay.get_origin(), this.m_mouseRay.get_origin() + this.m_mouseRay.get_direction().get_normalized() * this.m_mouseRayLength);
		float nextSeaLevel = Singleton<TerrainManager>.get_instance().WaterSimulation.m_nextSeaLevel;
		float num;
		if (Segment1.Intersect(segment.a.y, segment.b.y, nextSeaLevel, ref num))
		{
			position = segment.Position(num);
			return true;
		}
		position = Vector3.get_zero();
		return false;
	}

	private void ModifyHeight(ref WaterSource source)
	{
		Segment3 segment;
		segment..ctor(this.m_mouseRay.get_origin(), this.m_mouseRay.get_origin() + this.m_mouseRay.get_direction().get_normalized() * this.m_mouseRayLength);
		Segment3 segment2;
		segment2..ctor(this.m_tempSource.m_inputPosition, this.m_tempSource.m_inputPosition + new Vector3(0f, this.m_mouseRayLength, 0f));
		float num;
		float num2;
		segment.DistanceSqr(segment2, ref num, ref num2);
		Vector3 vector = segment.Position(num);
		int num3 = Mathf.Clamp((int)((source.m_inputPosition.y + 1f) * 63.9990234f), 0, 65535);
		int num4 = Mathf.Clamp((int)(vector.y * 63.9990234f), num3, 65535);
		source.m_target = (ushort)num4;
	}

	private void ModifyHeight()
	{
		Segment3 segment;
		segment..ctor(this.m_mouseRay.get_origin(), this.m_mouseRay.get_origin() + this.m_mouseRay.get_direction().get_normalized() * this.m_mouseRayLength);
		Segment3 segment2;
		segment2..ctor(this.m_tempSource.m_inputPosition, this.m_tempSource.m_inputPosition + new Vector3(0f, this.m_mouseRayLength, 0f));
		float num;
		float num2;
		segment.DistanceSqr(segment2, ref num, ref num2);
		int num3 = Mathf.Clamp((int)(Mathf.Min(segment.Position(num).y, 500f) * 63.9990234f), 0, 65535);
		this.m_tempSource.m_target = (ushort)num3;
	}

	public bool Overlap(int ignoreSource, Vector3 pos, uint rate)
	{
		float num = Mathf.Sqrt(Mathf.Abs(rate)) * 0.4f + 10f;
		Quad2 quad;
		quad.a = new Vector2(pos.x - num, pos.z - num);
		quad.b = new Vector2(pos.x - num, pos.z + num);
		quad.c = new Vector2(pos.x + num, pos.z + num);
		quad.d = new Vector2(pos.x + num, pos.z - num);
		if (Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad))
		{
			return true;
		}
		FastList<WaterSource> waterSources = Singleton<TerrainManager>.get_instance().WaterSimulation.m_waterSources;
		for (int i = 0; i < waterSources.m_size; i++)
		{
			if (i != ignoreSource && waterSources.m_buffer[i].m_type == 1)
			{
				float num2 = Mathf.Sqrt(Mathf.Abs(waterSources.m_buffer[i].m_inputRate)) * 0.4f + 10f;
				Vector3 inputPosition = waterSources.m_buffer[i].m_inputPosition;
				if (VectorUtils.LengthSqrXZ(inputPosition - pos) < (num + num2) * (num + num2))
				{
					return true;
				}
			}
		}
		return false;
	}

	public override void SimulationStep()
	{
		if (this.m_mode == WaterTool.Mode.WaterSource)
		{
			FastList<WaterSource> waterSources = Singleton<TerrainManager>.get_instance().WaterSimulation.m_waterSources;
			float num = this.m_mouseRayLength;
			bool flag = false;
			int num2 = -1;
			if (this.m_dragIndex == -1 && this.m_heightIndex == -1)
			{
				for (int i = 0; i < waterSources.m_size; i++)
				{
					float num3;
					bool flag2;
					Vector3 vector;
					if (waterSources.m_buffer[i].m_type == 1 && this.RayCastSource(ref waterSources.m_buffer[i], out num3, out flag2, out vector) && num3 < num)
					{
						num = num3;
						flag = flag2;
						num2 = i;
						this.m_tempSource.m_inputPosition = vector;
						this.m_tempSource.m_outputPosition = vector;
					}
				}
			}
			if (this.m_heightIndex != -1)
			{
				if (this.m_mouseRayValid)
				{
					this.ModifyHeight(ref waterSources.m_buffer[this.m_heightIndex]);
				}
			}
			else
			{
				ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
				ToolBase.RaycastOutput raycastOutput;
				if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
				{
					if (this.m_dragIndex != -1)
					{
						if (!this.Overlap(this.m_dragIndex, raycastOutput.m_hitPos, waterSources.m_buffer[this.m_dragIndex].m_inputRate))
						{
							int num4 = Mathf.Clamp((int)((raycastOutput.m_hitPos.y + 1f) * 63.9990234f), 0, 65535);
							int num5 = Mathf.Max(num4, (int)this.m_tempSource.m_target);
							waterSources.m_buffer[this.m_dragIndex].m_inputPosition = raycastOutput.m_hitPos;
							waterSources.m_buffer[this.m_dragIndex].m_outputPosition = raycastOutput.m_hitPos;
							waterSources.m_buffer[this.m_dragIndex].m_target = (ushort)num5;
						}
					}
					else if (num2 == -1 || Vector3.Distance(raycastOutput.m_hitPos, this.m_mouseRay.get_origin()) < num)
					{
						this.m_mousePosition = raycastOutput.m_hitPos;
						uint num6 = (uint)Mathf.Clamp((int)(this.m_capacity * 65535f), 0, 65535);
						if (this.Overlap(-1, raycastOutput.m_hitPos, num6))
						{
							this.m_tempSource.m_type = 0;
						}
						else
						{
							this.m_tempSource.m_inputPosition = raycastOutput.m_hitPos;
							this.m_tempSource.m_outputPosition = raycastOutput.m_hitPos;
							this.m_tempSource.m_inputRate = num6;
							this.m_tempSource.m_outputRate = num6;
							this.m_tempSource.m_target = (ushort)Mathf.Clamp((int)((this.m_mousePosition.y + this.m_height) * 63.9990234f), 0, 65535);
							this.m_tempSource.m_type = 1;
						}
						num2 = -1;
						this.m_dragHoverIndex = -1;
						this.m_heightHoverIndex = -1;
					}
				}
			}
			if (num2 != -1)
			{
				this.m_tempSource.m_type = 0;
				if (flag)
				{
					this.m_dragHoverIndex = -1;
					this.m_heightHoverIndex = num2;
					this.m_waterLevel = (float)waterSources.m_buffer[num2].m_target * 0.015625f;
				}
				else
				{
					this.m_dragHoverIndex = num2;
					this.m_heightHoverIndex = -1;
					this.m_waterLevel = 0f;
				}
			}
			else if (this.m_heightIndex != -1)
			{
				this.m_waterLevel = (float)waterSources.m_buffer[this.m_heightIndex].m_target * 0.015625f;
			}
			else
			{
				this.m_waterLevel = 0f;
			}
		}
		else if (this.m_mode == WaterTool.Mode.SeaLevel)
		{
			if (this.m_heightIndex == -1)
			{
				Vector3 zero = Vector3.get_zero();
				if (this.RayCastSea(out zero))
				{
					this.m_heightHoverIndex = 0;
					this.m_waterLevel = Singleton<TerrainManager>.get_instance().WaterSimulation.m_nextSeaLevel;
					this.m_tempSource.m_inputPosition = zero;
					this.m_tempSource.m_outputPosition = zero;
				}
				else
				{
					this.m_heightHoverIndex = -1;
					this.m_waterLevel = 0f;
				}
			}
			else
			{
				if (this.m_mouseRayValid)
				{
					this.ModifyHeight();
				}
				this.m_waterLevel = (float)this.m_tempSource.m_target * 0.015625f;
			}
		}
		this.m_waitSimulationStep = false;
	}

	public Material m_levelMaterial;

	public Material m_sourceMaterial;

	public Mesh m_sourceMesh;

	public WaterTool.Mode m_mode;

	public float m_capacity = 0.5f;

	public CursorInfo m_placementCursor;

	public CursorInfo m_dragCursor;

	public CursorInfo m_heightCursor;

	public CursorInfo m_sealevelCursor;

	private float m_height;

	private Vector3 m_mousePosition;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private Material m_levelMaterial2;

	private WaterSource m_tempSource;

	private MaterialPropertyBlock m_propertyBlock;

	private int ID_Color;

	private int ID_Color2;

	private int ID_WaterLevel;

	private int m_dragIndex;

	private int m_heightIndex;

	private int m_dragHoverIndex;

	private int m_heightHoverIndex;

	private bool m_waitSimulationStep;

	private float m_waterLevel;

	private bool m_mouseRayValid;

	public enum Mode
	{
		WaterSource,
		SeaLevel
	}
}
