﻿using System;
using UnityEngine;

public interface IUITag
{
	bool locationLess
	{
		get;
	}

	bool isValid
	{
		get;
	}

	bool isDynamic
	{
		get;
	}

	bool isRelative
	{
		get;
	}

	Vector3 position
	{
		get;
	}
}
