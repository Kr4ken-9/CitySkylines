﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using UnityEngine;

public class NetSegmentInstanceGuide : GuideTriggerBase
{
	public void Activate(GuideInfo guideInfo, ushort segmentID, bool ignoreID)
	{
		if (this.m_segmentID != 0 && !this.Validate())
		{
			this.Deactivate();
		}
		if (this.m_segmentID == 0 || this.m_segmentID == segmentID || ignoreID)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			this.m_segmentID = segmentID;
			this.m_buildIndex = instance.m_segments.m_buffer[(int)this.m_segmentID].m_buildIndex;
			if (base.CanActivate(guideInfo))
			{
				ushort startNode = instance.m_segments.m_buffer[(int)this.m_segmentID].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)this.m_segmentID].m_endNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				Vector3 point = (position + position2) * 0.5f;
				if (Singleton<SimulationManager>.get_instance().m_simulationView.Intersect(point, -10f))
				{
					GuideTriggerBase.ActivationInfo activationInfo = new NetSegmentInstanceGuide.NetSegmentInstanceActivationInfo(guideInfo, segmentID);
					base.Activate(guideInfo, activationInfo);
				}
			}
		}
	}

	public override void Deactivate()
	{
		this.m_segmentID = 0;
		this.m_buildIndex = 0u;
		base.Deactivate();
	}

	public override void Disable()
	{
		this.m_segmentID = 0;
		this.m_buildIndex = 0u;
		base.Disable();
	}

	public override bool Validate()
	{
		if (this.m_segmentID == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		return instance.m_segments.m_buffer[(int)this.m_segmentID].m_flags != NetSegment.Flags.None && instance.m_segments.m_buffer[(int)this.m_segmentID].m_buildIndex == this.m_buildIndex && base.Validate();
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteUInt32(this.m_buildIndex);
		s.WriteUInt16((uint)this.m_segmentID);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_buildIndex = s.ReadUInt32();
		this.m_segmentID = (ushort)s.ReadUInt16();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public uint m_buildIndex;

	public ushort m_segmentID;

	public class NetSegmentInstanceActivationInfo : GuideTriggerBase.ActivationInfo
	{
		public NetSegmentInstanceActivationInfo(GuideInfo guideInfo, ushort segmentID) : base(guideInfo)
		{
			this.m_segmentID = segmentID;
		}

		public override string GetName()
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_segmentID].Info;
			string name = this.m_guideInfo.m_name;
			string arg = PrefabCollection<NetInfo>.PrefabName((uint)info.m_prefabDataIndex);
			return StringUtils.SafeFormat(name, arg);
		}

		public override string GetIcon()
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_segmentID].Info;
			string icon = this.m_guideInfo.m_icon;
			string thumbnail = info.m_Thumbnail;
			return StringUtils.SafeFormat(icon, thumbnail);
		}

		public override string FormatText(string pattern)
		{
			NetInfo info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_segmentID].Info;
			string text = PrefabCollection<NetInfo>.PrefabName((uint)info.m_prefabDataIndex);
			string arg = Locale.Get("NET_TITLE", text);
			return StringUtils.SafeFormat(pattern, arg);
		}

		public override IUITag GetTag()
		{
			return new NetSegmentTutorialTag(this.m_segmentID);
		}

		protected ushort m_segmentID;
	}
}
