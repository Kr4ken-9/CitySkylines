﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using UnityEngine;
using UnityEngine.Rendering;

public class RenderManager : SimulationManagerBase<RenderManager, RenderProperties>, ISimulationManager, IRenderableManager
{
	public OverlayEffect OverlayEffect
	{
		get
		{
			return this.m_overlay;
		}
	}

	public RenderManager.CameraInfo CurrentCameraInfo
	{
		get
		{
			return this.m_cameraInfo;
		}
	}

	public LightSystem lightSystem
	{
		get
		{
			return this.m_lightSystem;
		}
	}

	public float RequiredAspect
	{
		get
		{
			return this.m_requiredAspect;
		}
		set
		{
			this.m_requiredAspect = value;
		}
	}

	public float CameraHeight
	{
		get
		{
			return this.m_cameraHeight;
		}
		set
		{
			this.m_cameraHeight = value;
		}
	}

	public float ShadowDistance
	{
		get
		{
			return this.m_shadowDistance;
		}
		set
		{
			this.m_shadowDistance = value;
		}
	}

	public Light MainLight
	{
		get
		{
			return this.m_mainLight;
		}
		set
		{
			this.m_mainLight = value;
		}
	}

	public static int LevelOfDetail
	{
		get
		{
			return RenderManager.m_levelOfDetail;
		}
		set
		{
			RenderManager.m_levelOfDetail = value;
			switch (RenderManager.m_levelOfDetail)
			{
			case 0:
				RenderManager.m_levelOfDetailFactor = 0.5f;
				break;
			case 1:
				RenderManager.m_levelOfDetailFactor = 0.707106769f;
				break;
			case 2:
				RenderManager.m_levelOfDetailFactor = 1f;
				break;
			case 3:
				RenderManager.m_levelOfDetailFactor = 1.41421354f;
				break;
			}
			switch (RenderManager.m_levelOfDetail)
			{
			case 0:
				RenderManager.m_levelOfDetailFactor2 = 1f;
				break;
			case 1:
				RenderManager.m_levelOfDetailFactor2 = 1.587401f;
				break;
			case 2:
				RenderManager.m_levelOfDetailFactor2 = 2.51984215f;
				break;
			case 3:
				RenderManager.m_levelOfDetailFactor2 = 4f;
				break;
			}
			if (Singleton<RenderManager>.get_exists())
			{
				Singleton<RenderManager>.get_instance().RefreshLevelOfDetail();
			}
		}
	}

	public static float LevelOfDetailFactor
	{
		get
		{
			return RenderManager.m_levelOfDetailFactor;
		}
	}

	public static float LevelOfDetailFactor2
	{
		get
		{
			return RenderManager.m_levelOfDetailFactor2;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_instances = new RenderManager.Instance[2048];
		this.m_indices = new ushort[118784];
		this.m_groups = new RenderGroup[2025];
		this.m_megaGroups = new MegaRenderGroup[81];
		this.m_renderedGroups = new FastList<RenderGroup>();
		this.m_cameraInfo = new RenderManager.CameraInfo();
		this.m_groupLayerMaterials = new Material[32];
		this.m_updatedGroups1 = new ulong[32];
		for (int i = 0; i < this.m_instances.Length; i++)
		{
			this.m_instances[i].m_nextInstance = 65535;
		}
		for (int j = 0; j < 118784; j++)
		{
			this.m_indices[j] = 65535;
		}
		this.m_currentFrame = 2u;
		this.m_updateLock = new object();
		this.m_objectColorMap = new Texture2D(RenderManager.OBJECT_COLORMAP_WIDTH, RenderManager.OBJECT_COLORMAP_HEIGHT, 5, false, false);
		this.m_objectColorMap.set_filterMode(0);
		Shader.SetGlobalTexture("_ObjectColorMap", this.m_objectColorMap);
		this.m_lightSystem = base.get_gameObject().AddComponent<LightSystem>();
		this.m_overlayBuffer = new CommandBuffer();
		this.m_overlayBuffer.set_name("Batched overlay");
	}

	private void OnDestroy()
	{
		if (this.m_groupLayerMaterials != null)
		{
			for (int i = 0; i < this.m_groupLayerMaterials.Length; i++)
			{
				if (this.m_groupLayerMaterials[i] != null)
				{
					Object.Destroy(this.m_groupLayerMaterials[i]);
				}
			}
		}
		if (this.m_objectColorMap != null)
		{
			Object.Destroy(this.m_objectColorMap);
			this.m_objectColorMap = null;
		}
		if (this.m_groups != null)
		{
			for (int j = 0; j < this.m_groups.Length; j++)
			{
				RenderGroup renderGroup = this.m_groups[j];
				if (renderGroup != null)
				{
					renderGroup.ReleaseGroup();
				}
			}
			this.m_groups = null;
		}
		if (this.m_megaGroups != null)
		{
			for (int k = 0; k < this.m_megaGroups.Length; k++)
			{
				MegaRenderGroup megaRenderGroup = this.m_megaGroups[k];
				if (megaRenderGroup != null)
				{
					megaRenderGroup.ReleaseGroup();
				}
			}
			this.m_megaGroups = null;
		}
	}

	private void RefreshLevelOfDetail()
	{
		if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			this.RefreshLevelOfDetail<BuildingInfo>();
			this.RefreshLevelOfDetail<NetInfo>();
			this.RefreshLevelOfDetail<PropInfo>();
			this.RefreshLevelOfDetail<TreeInfo>();
			this.RefreshLevelOfDetail<VehicleInfo>();
			this.RefreshLevelOfDetail<CitizenInfo>();
		}
	}

	private void RefreshLevelOfDetail<T>() where T : PrefabInfo
	{
		uint num = (uint)PrefabCollection<T>.LoadedCount();
		for (uint num2 = 0u; num2 < num; num2 += 1u)
		{
			T loaded = PrefabCollection<T>.GetLoaded(num2);
			if (!(loaded == null))
			{
				loaded.RefreshLevelOfDetail();
			}
		}
	}

	public static void RegisterRenderableManager(IRenderableManager manager)
	{
		if (manager != null)
		{
			RenderManager.m_renderables.Add(manager);
		}
	}

	public static void GetManagers(out IRenderableManager[] managers, out int count)
	{
		if (RenderManager.m_renderables != null)
		{
			managers = RenderManager.m_renderables.m_buffer;
			count = RenderManager.m_renderables.m_size;
		}
		else
		{
			managers = null;
			count = 0;
		}
	}

	public override void InitializeProperties(RenderProperties properties)
	{
		base.InitializeProperties(properties);
		for (int i = 0; i < properties.m_groupLayerShaders.Length; i++)
		{
			if (properties.m_groupLayerShaders[i] != null)
			{
				if (this.m_groupLayerMaterials[i] == null)
				{
					this.m_groupLayerMaterials[i] = new Material(properties.m_groupLayerShaders[i]);
				}
				else
				{
					this.m_groupLayerMaterials[i].set_shader(properties.m_groupLayerShaders[i]);
				}
			}
			else
			{
				Object.Destroy(this.m_groupLayerMaterials[i]);
				this.m_groupLayerMaterials[i] = null;
			}
		}
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_overlay = gameObject.GetComponent<OverlayEffect>();
			this.m_mainCamera = gameObject.GetComponent<Camera>();
		}
		gameObject = GameObject.FindGameObjectWithTag("UndergroundView");
		if (gameObject != null)
		{
			this.m_undergroundCamera = gameObject.GetComponent<Camera>();
		}
		this.m_lightSystem.InitializeProperties(properties, this.m_mainCamera);
	}

	public override void DestroyProperties(RenderProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_lightSystem.DestroyProperties(properties, this.m_mainCamera);
			for (int i = 0; i < this.m_instances.Length; i++)
			{
				this.m_instances[i].m_dataTexture0 = null;
				this.m_instances[i].m_dataTexture1 = null;
				this.m_instances[i].m_nameData = null;
			}
			this.m_overlay = null;
			this.m_mainCamera = null;
			this.m_undergroundCamera = null;
			this.m_mainLight = null;
		}
		base.DestroyProperties(properties);
	}

	[DebuggerHidden]
	private IEnumerator UpdateRenderGroup(int index)
	{
		RenderManager.<UpdateRenderGroup>c__Iterator0 <UpdateRenderGroup>c__Iterator = new RenderManager.<UpdateRenderGroup>c__Iterator0();
		<UpdateRenderGroup>c__Iterator.index = index;
		<UpdateRenderGroup>c__Iterator.$this = this;
		return <UpdateRenderGroup>c__Iterator;
	}

	private void LateUpdate()
	{
		this.m_currentFrame += 1u;
		this.m_outOfInstances = false;
		PrefabPool.m_canCreateInstances = 1;
		this.m_lightSystem.m_lightBuffer.Clear();
		this.m_overlayBuffer.Clear();
		Singleton<InfoManager>.get_instance().UpdateInfoMode();
		if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			return;
		}
		this.UpdateCameraInfo();
		this.UpdateColorMap();
		try
		{
			for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
			{
				RenderManager.m_renderables.m_buffer[i].BeginRendering(this.m_cameraInfo);
			}
		}
		finally
		{
		}
		try
		{
			Vector3 min = this.m_cameraInfo.m_bounds.get_min();
			Vector3 max = this.m_cameraInfo.m_bounds.get_max();
			if (this.m_cameraInfo.m_shadowOffset.x < 0f)
			{
				max.x -= this.m_cameraInfo.m_shadowOffset.x;
			}
			else
			{
				min.x -= this.m_cameraInfo.m_shadowOffset.x;
			}
			if (this.m_cameraInfo.m_shadowOffset.z < 0f)
			{
				max.z -= this.m_cameraInfo.m_shadowOffset.z;
			}
			else
			{
				min.z -= this.m_cameraInfo.m_shadowOffset.z;
			}
			int num = Mathf.Max((int)((min.x - 128f) / 384f + 22.5f), 0);
			int num2 = Mathf.Max((int)((min.z - 128f) / 384f + 22.5f), 0);
			int num3 = Mathf.Min((int)((max.x + 128f) / 384f + 22.5f), 44);
			int num4 = Mathf.Min((int)((max.z + 128f) / 384f + 22.5f), 44);
			int num5 = 5;
			int num6 = 10000;
			int num7 = 10000;
			int num8 = -10000;
			int num9 = -10000;
			this.m_renderedGroups.Clear();
			for (int j = num2; j <= num4; j++)
			{
				for (int k = num; k <= num3; k++)
				{
					int num10 = j * 45 + k;
					RenderGroup renderGroup = this.m_groups[num10];
					if (renderGroup != null && renderGroup.Render(this.m_cameraInfo))
					{
						this.m_renderedGroups.Add(renderGroup);
						int num11 = k / num5;
						int num12 = j / num5;
						int num13 = num12 * 9 + num11;
						MegaRenderGroup megaRenderGroup = this.m_megaGroups[num13];
						if (megaRenderGroup != null)
						{
							megaRenderGroup.m_layersRendered2 |= (megaRenderGroup.m_layersRendered1 & renderGroup.m_layersRendered);
							megaRenderGroup.m_layersRendered1 |= renderGroup.m_layersRendered;
							megaRenderGroup.m_instanceMask |= renderGroup.m_instanceMask;
							num6 = Mathf.Min(num6, num11);
							num7 = Mathf.Min(num7, num12);
							num8 = Mathf.Max(num8, num11);
							num9 = Mathf.Max(num9, num12);
						}
					}
				}
			}
			for (int l = num7; l <= num9; l++)
			{
				for (int m = num6; m <= num8; m++)
				{
					int num14 = l * 9 + m;
					MegaRenderGroup megaRenderGroup2 = this.m_megaGroups[num14];
					if (megaRenderGroup2 != null)
					{
						megaRenderGroup2.Render();
					}
				}
			}
			for (int n = 0; n < this.m_renderedGroups.m_size; n++)
			{
				RenderGroup renderGroup2 = this.m_renderedGroups.m_buffer[n];
				int num15 = renderGroup2.m_x / num5;
				int num16 = renderGroup2.m_z / num5;
				int num17 = num16 * 9 + num15;
				MegaRenderGroup megaRenderGroup3 = this.m_megaGroups[num17];
				if (megaRenderGroup3 != null && megaRenderGroup3.m_groupMask != 0)
				{
					renderGroup2.Render(megaRenderGroup3.m_groupMask);
				}
			}
		}
		finally
		{
		}
		try
		{
			for (int num18 = 0; num18 < RenderManager.m_renderables.m_size; num18++)
			{
				RenderManager.m_renderables.m_buffer[num18].EndRendering(this.m_cameraInfo);
			}
		}
		finally
		{
		}
		this.m_lightSystem.EndRendering(this.m_cameraInfo);
	}

	public void UpdateCameraInfo()
	{
		this.m_cameraInfo.m_camera = this.m_mainCamera;
		Transform transform = this.m_mainCamera.get_transform();
		float num = this.m_mainCamera.get_fieldOfView() * 0.5f;
		float num2 = (float)this.m_mainCamera.get_pixelWidth() / (float)this.m_mainCamera.get_pixelHeight();
		float num3 = Mathf.Max(this.m_requiredAspect, num2);
		this.m_cameraInfo.m_near = this.m_mainCamera.get_nearClipPlane();
		this.m_cameraInfo.m_far = this.m_mainCamera.get_farClipPlane();
		this.m_cameraInfo.m_height = this.m_cameraHeight;
		this.m_cameraInfo.m_layerMask = (this.m_mainCamera.get_cullingMask() | this.m_undergroundCamera.get_cullingMask());
		this.m_cameraInfo.m_rotation = transform.get_rotation();
		this.m_cameraInfo.m_position = transform.get_position();
		this.m_cameraInfo.m_forward = transform.get_forward();
		this.m_cameraInfo.m_right = transform.get_right();
		this.m_cameraInfo.m_up = transform.get_up();
		Vector3 forward = this.m_cameraInfo.m_forward;
		Vector3 vector = this.m_cameraInfo.m_right * (Mathf.Tan(num * 0.0174532924f) * num3);
		Vector3 vector2 = this.m_cameraInfo.m_right * (Mathf.Tan(num * 0.0174532924f) * num2);
		Vector3 vector3 = this.m_cameraInfo.m_up * Mathf.Tan(num * 0.0174532924f);
		this.m_cameraInfo.m_directionA = forward + vector3 - vector;
		this.m_cameraInfo.m_directionB = forward + vector3 + vector;
		this.m_cameraInfo.m_directionC = forward - vector3 + vector;
		this.m_cameraInfo.m_directionD = forward - vector3 - vector;
		Vector3 vector4 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionA * this.m_cameraInfo.m_near;
		Vector3 vector5 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionB * this.m_cameraInfo.m_near;
		Vector3 vector6 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionC * this.m_cameraInfo.m_near;
		Vector3 vector7 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionD * this.m_cameraInfo.m_near;
		Vector3 vector8 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionA * this.m_cameraInfo.m_far;
		Vector3 vector9 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionB * this.m_cameraInfo.m_far;
		Vector3 vector10 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionC * this.m_cameraInfo.m_far;
		Vector3 vector11 = this.m_cameraInfo.m_position + this.m_cameraInfo.m_directionD * this.m_cameraInfo.m_far;
		this.m_cameraInfo.m_planeA = new Plane(vector8, vector4, vector9);
		this.m_cameraInfo.m_planeB = new Plane(vector9, vector5, vector10);
		this.m_cameraInfo.m_planeC = new Plane(vector10, vector6, vector11);
		this.m_cameraInfo.m_planeD = new Plane(vector11, vector7, vector8);
		this.m_cameraInfo.m_planeE = new Plane(vector5, vector4, vector6);
		this.m_cameraInfo.m_planeF = new Plane(vector9, vector10, vector8);
		Vector3 vector12 = Vector3.Min(Vector3.Min(Vector3.Min(vector4, vector5), Vector3.Min(vector6, vector7)), Vector3.Min(Vector3.Min(vector8, vector9), Vector3.Min(vector10, vector11)));
		Vector3 vector13 = Vector3.Max(Vector3.Max(Vector3.Max(vector4, vector5), Vector3.Max(vector6, vector7)), Vector3.Max(Vector3.Max(vector8, vector9), Vector3.Max(vector10, vector11)));
		this.m_cameraInfo.m_bounds.SetMinMax(vector12, vector13);
		Vector3 vector14 = Vector3.Min(Vector3.Min(Vector3.Min(vector4, vector5), Vector3.Min(vector6, vector7)), this.m_cameraInfo.m_position);
		Vector3 vector15 = Vector3.Max(Vector3.Max(Vector3.Max(vector4, vector5), Vector3.Max(vector6, vector7)), this.m_cameraInfo.m_position);
		this.m_cameraInfo.m_nearBounds.SetMinMax(vector14, vector15);
		SimulationManager.ViewData view;
		view.m_position = this.m_cameraInfo.m_position;
		view.m_direction = this.m_cameraInfo.m_forward;
		if (this.m_requiredAspect > num2)
		{
			Vector3 vector16 = forward + vector3 - vector2;
			Vector3 vector17 = forward + vector3 + vector2;
			Vector3 vector18 = forward - vector3 + vector2;
			Vector3 vector19 = forward - vector3 - vector2;
			Vector3 vector20 = this.m_cameraInfo.m_position + vector16 * this.m_cameraInfo.m_near;
			Vector3 vector21 = this.m_cameraInfo.m_position + vector17 * this.m_cameraInfo.m_near;
			Vector3 vector22 = this.m_cameraInfo.m_position + vector18 * this.m_cameraInfo.m_near;
			Vector3 vector23 = this.m_cameraInfo.m_position + vector19 * this.m_cameraInfo.m_near;
			Vector3 vector24 = this.m_cameraInfo.m_position + vector16 * this.m_cameraInfo.m_far;
			Vector3 vector25 = this.m_cameraInfo.m_position + vector17 * this.m_cameraInfo.m_far;
			Vector3 vector26 = this.m_cameraInfo.m_position + vector18 * this.m_cameraInfo.m_far;
			Vector3 vector27 = this.m_cameraInfo.m_position + vector19 * this.m_cameraInfo.m_far;
			view.m_planeA = new Plane(vector24, vector20, vector25);
			view.m_planeB = new Plane(vector25, vector21, vector26);
			view.m_planeC = new Plane(vector26, vector22, vector27);
			view.m_planeD = new Plane(vector27, vector23, vector24);
			view.m_planeE = new Plane(vector21, vector20, vector22);
			view.m_planeF = new Plane(vector25, vector26, vector24);
		}
		else
		{
			view.m_planeA = this.m_cameraInfo.m_planeA;
			view.m_planeB = this.m_cameraInfo.m_planeB;
			view.m_planeC = this.m_cameraInfo.m_planeC;
			view.m_planeD = this.m_cameraInfo.m_planeD;
			view.m_planeE = this.m_cameraInfo.m_planeE;
			view.m_planeF = this.m_cameraInfo.m_planeF;
		}
		Singleton<SimulationManager>.get_instance().SetView(view);
		Vector3 vector28 = Vector3.get_down();
		Color linear = RenderSettings.get_ambientSkyColor().get_linear();
		if (this.m_mainLight != null)
		{
			vector28 = this.m_mainLight.get_transform().get_forward();
			this.m_cameraInfo.m_shadowRotation = Quaternion.LookRotation(vector28);
			this.m_cameraInfo.m_shadowOffset = vector28 * this.m_shadowDistance;
			linear.a = this.m_mainLight.get_shadowStrength();
		}
		Shader.SetGlobalVector("_CameraRight", this.m_cameraInfo.m_right);
		Shader.SetGlobalVector("_CameraUp", this.m_cameraInfo.m_up);
		Shader.SetGlobalVector("_CameraForward", this.m_cameraInfo.m_forward);
		Shader.SetGlobalVector("_ViewportRight", vector2);
		Shader.SetGlobalVector("_ViewportUp", vector3);
		Shader.SetGlobalVector("_ViewportForward", forward);
		Shader.SetGlobalVector("_ShadowRight", this.m_cameraInfo.m_shadowRotation * Vector3.get_right());
		Shader.SetGlobalVector("_ShadowUp", this.m_cameraInfo.m_shadowRotation * Vector3.get_up());
		Shader.SetGlobalVector("_ShadowForward", vector28);
		Shader.SetGlobalColor("_AmbientColor", linear);
	}

	public static void Managers_RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			RenderManager.m_renderables.m_buffer[i].BeginOverlay(cameraInfo);
		}
		Graphics.ExecuteCommandBuffer(Singleton<RenderManager>.get_instance().m_overlayBuffer);
		for (int j = 0; j < RenderManager.m_renderables.m_size; j++)
		{
			RenderManager.m_renderables.m_buffer[j].EndOverlay(cameraInfo);
		}
	}

	public static void Managers_UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			RenderManager.m_renderables.m_buffer[i].UndergroundOverlay(cameraInfo);
		}
	}

	public static void Managers_CheckReferences()
	{
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			RenderManager.m_renderables.m_buffer[i].CheckReferences();
		}
	}

	public static void Managers_InitRenderData()
	{
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			RenderManager.m_renderables.m_buffer[i].InitRenderData();
		}
	}

	private void UpdateColorMap()
	{
		bool flag = false;
		if (Singleton<BuildingManager>.get_instance().UpdateColorMap(this.m_objectColorMap))
		{
			flag = true;
		}
		if (Singleton<NetManager>.get_instance().UpdateColorMap(this.m_objectColorMap))
		{
			flag = true;
		}
		if (flag)
		{
			this.m_objectColorMap.Apply(false);
		}
	}

	public bool RequireInstance(uint holder, uint count, out uint instanceIndex)
	{
		instanceIndex = (uint)this.m_indices[(int)((UIntPtr)holder)];
		if (instanceIndex != 65535u)
		{
			if ((ulong)count == (ulong)((long)this.m_instances[(int)((UIntPtr)instanceIndex)].m_instanceCount))
			{
				this.m_instances[(int)((UIntPtr)instanceIndex)].m_lastUse = this.m_currentFrame;
				return true;
			}
			this.ReleaseInstance(holder);
			instanceIndex = 65535u;
		}
		else if (this.m_outOfInstances)
		{
			return false;
		}
		uint num = 65535u;
		while (count != 0u)
		{
			int num2 = 0;
			while (this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_lastUse + 1u >= this.m_currentFrame || this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_instanceCount == -1)
			{
				if (++num2 == this.m_instances.Length && !this.ResizeInstanceBuffer())
				{
					this.m_outOfInstances = true;
					if (instanceIndex != 65535u)
					{
						this.ReleaseInstance(holder);
						instanceIndex = 65535u;
					}
					return false;
				}
				if ((ulong)(this.m_currentInstance += 1u) == (ulong)((long)this.m_instances.Length))
				{
					this.m_currentInstance = 0u;
				}
			}
			this.ReleaseInstance(this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_holder);
			this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_dirty = true;
			this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_initialized = false;
			this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_lastUse = this.m_currentFrame;
			this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_nextInstance = 65535;
			this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_holder = holder;
			if (num != 65535u)
			{
				this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_instanceCount = -1;
				this.m_instances[(int)((UIntPtr)num)].m_nextInstance = (ushort)this.m_currentInstance;
			}
			else
			{
				this.m_instances[(int)((UIntPtr)this.m_currentInstance)].m_instanceCount = (int)count;
				this.m_indices[(int)((UIntPtr)holder)] = (ushort)this.m_currentInstance;
				instanceIndex = this.m_currentInstance;
			}
			num = this.m_currentInstance;
			count -= 1u;
		}
		return true;
	}

	private bool ResizeInstanceBuffer()
	{
		if (this.m_instances.Length >= 32768)
		{
			return false;
		}
		RenderManager.Instance[] array = new RenderManager.Instance[this.m_instances.Length * 2];
		for (int i = 0; i < this.m_instances.Length; i++)
		{
			array[i] = this.m_instances[i];
		}
		for (int j = this.m_instances.Length; j < array.Length; j++)
		{
			array[j].m_nextInstance = 65535;
		}
		this.m_instances = array;
		return true;
	}

	public void ReleaseInstance(uint holder)
	{
		uint num = (uint)this.m_indices[(int)((UIntPtr)holder)];
		this.m_indices[(int)((UIntPtr)holder)] = 65535;
		while (num != 65535u)
		{
			ushort nextInstance = this.m_instances[(int)((UIntPtr)num)].m_nextInstance;
			this.m_instances[(int)((UIntPtr)num)].m_nextInstance = 65535;
			this.m_instances[(int)((UIntPtr)num)].m_lastUse = 0u;
			this.m_instances[(int)((UIntPtr)num)].m_instanceCount = 0;
			this.m_instances[(int)((UIntPtr)num)].m_holder = 0u;
			num = (uint)nextInstance;
		}
	}

	public void UpdateMegaGroup(int x, int z, int layer)
	{
		int num = z * 9 + x;
		if (this.m_megaGroups[num] == null)
		{
			this.m_megaGroups[num] = new MegaRenderGroup(x, z);
		}
		this.m_megaGroups[num].SetLayerDirty(layer);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.set_color(Color.get_magenta());
		for (int i = 0; i < this.m_groups.Length; i++)
		{
			RenderGroup renderGroup = this.m_groups[i];
			if (renderGroup != null)
			{
				if (renderGroup.m_layersRendered != 0)
				{
					Gizmos.DrawWireCube(renderGroup.m_bounds.get_center(), renderGroup.m_bounds.get_size());
				}
				renderGroup.m_layersRendered = 0;
			}
		}
	}

	public void UpdateInstance(uint holder)
	{
		uint num = (uint)this.m_indices[(int)((UIntPtr)holder)];
		if (num != 65535u)
		{
			this.m_instances[(int)((UIntPtr)num)].m_dirty = true;
		}
	}

	public bool GetInstanceIndex(uint holder, out uint instanceIndex)
	{
		instanceIndex = (uint)this.m_indices[(int)((UIntPtr)holder)];
		return instanceIndex != 65535u;
	}

	public void UpdateGroup(int x, int z, int layer)
	{
		int num = z * 45 + x;
		if (this.m_groups[num] == null)
		{
			this.m_groups[num] = new RenderGroup(x, z);
		}
		this.m_groups[num].SetLayerDataDirty(layer);
		this.m_updatedGroups1[num >> 6] |= 1uL << num;
		this.m_groupsUpdated1 = true;
	}

	public void UpdateGroups(int layer)
	{
		for (int i = 0; i < this.m_groups.Length; i++)
		{
			if (this.m_groups[i] != null)
			{
				this.m_groups[i].SetLayerDataDirty(layer, false);
				this.m_updatedGroups1[i >> 6] |= 1uL << i;
			}
		}
		this.m_groupsUpdated1 = true;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_groupsUpdated1)
		{
			this.m_groupsUpdated1 = false;
			int num = this.m_updatedGroups1.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedGroups1[i];
				this.m_updatedGroups1[i] = 0uL;
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							int num3 = i << 6 | j;
							if (this.m_groups[num3] != null)
							{
								this.m_groups[num3].UpdateMeshData();
							}
						}
					}
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("RenderManager.UpdateData");
		base.UpdateData(mode);
		int num = this.m_updatedGroups1.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_updatedGroups1[i] = 0uL;
		}
		int num2 = this.m_groups.Length;
		for (int j = 0; j < num2; j++)
		{
			if (this.m_groups[j] != null)
			{
				this.m_groups[j].SetAllLayersDirty();
			}
		}
		this.m_groupsUpdated1 = false;
		Singleton<LoadingManager>.get_instance().WaitUntilRenderDataReady();
		this.m_lastUpdatedGroup = -1;
		this.m_requiredUpdatedGroup = -1;
		for (int k = 0; k < num2; k++)
		{
			if (this.m_requiredUpdatedGroup > this.m_lastUpdatedGroup + 128)
			{
				Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.PauseLoading();
				while (!Monitor.TryEnter(this.m_updateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				try
				{
					while (this.m_requiredUpdatedGroup > this.m_lastUpdatedGroup + 128 && !Singleton<SimulationManager>.get_instance().Terminated)
					{
						Monitor.Wait(this.m_updateLock, 1);
					}
				}
				finally
				{
					Monitor.Exit(this.m_updateLock);
				}
				Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.ContinueLoading();
			}
			if (this.m_groups[k] != null)
			{
				this.m_groups[k].SetAllLayersDirty();
				this.m_groups[k].UpdateMeshData();
				this.m_requiredUpdatedGroup = k;
				Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.UpdateRenderGroup(k));
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("RenderManager.LateUpdateData");
		base.LateUpdateData(mode);
		if (this.m_lastUpdatedGroup != this.m_requiredUpdatedGroup)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.PauseLoading();
			while (!Monitor.TryEnter(this.m_updateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				while (this.m_lastUpdatedGroup != this.m_requiredUpdatedGroup && !Singleton<SimulationManager>.get_instance().Terminated)
				{
					Monitor.Wait(this.m_updateLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(this.m_updateLock);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.ContinueLoading();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public static bool Managers_CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			if (RenderManager.m_renderables.m_buffer[i].CalculateGroupData(groupX, groupZ, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
			{
				result = true;
			}
		}
		return result;
	}

	public static void Managers_PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		for (int i = 0; i < RenderManager.m_renderables.m_size; i++)
		{
			RenderManager.m_renderables.m_buffer[i].PopulateGroupData(groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance, ref requireSurfaceMaps);
		}
	}

	public static Vector4 GetColorLocation(uint instanceHolder)
	{
		Vector4 result;
		result.x = ((float)(instanceHolder % (uint)RenderManager.OBJECT_COLORMAP_WIDTH) + 0.5f) / (float)RenderManager.OBJECT_COLORMAP_WIDTH;
		result.y = ((float)(instanceHolder / (uint)RenderManager.OBJECT_COLORMAP_WIDTH) + 0.5f) / (float)RenderManager.OBJECT_COLORMAP_HEIGHT;
		result.z = 1f;
		result.w = 0f;
		return result;
	}

	public static void GetColorLocation(uint instanceHolder, out int x, out int y)
	{
		x = (int)(instanceHolder % (uint)RenderManager.OBJECT_COLORMAP_WIDTH);
		y = (int)(instanceHolder / (uint)RenderManager.OBJECT_COLORMAP_WIDTH);
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float GROUP_CELL_SIZE = 384f;

	public const int GROUP_RESOLUTION = 45;

	public const float MEGA_GROUP_CELL_SIZE = 1920f;

	public const int MEGA_GROUP_RESOLUTION = 9;

	public const int MAX_INSTANCE_HOLDERS = 118784;

	public static int OBJECT_COLORMAP_WIDTH = FixedMath.GEqualPowerOf2(FixedMath.Sqrt(118784));

	public static int OBJECT_COLORMAP_HEIGHT = FixedMath.GEqualPowerOf2(118784 / RenderManager.OBJECT_COLORMAP_WIDTH);

	public static Vector4 DefaultColorLocation = new Vector4(0.5f / (float)RenderManager.OBJECT_COLORMAP_WIDTH, 0.5f / (float)RenderManager.OBJECT_COLORMAP_HEIGHT, 0f, 0f);

	[NonSerialized]
	public ulong[] m_updatedGroups1;

	[NonSerialized]
	public bool m_groupsUpdated1;

	[NonSerialized]
	public RenderManager.Instance[] m_instances;

	[NonSerialized]
	public ushort[] m_indices;

	[NonSerialized]
	public RenderGroup[] m_groups;

	[NonSerialized]
	public MegaRenderGroup[] m_megaGroups;

	[NonSerialized]
	public FastList<RenderGroup> m_renderedGroups;

	[NonSerialized]
	public FastList<RenderGroup.MeshLayer> m_overlayGroupLayers;

	[NonSerialized]
	public FastList<MegaRenderGroup.MeshLayer> m_overlayMegaLayers;

	[NonSerialized]
	public Material[] m_groupLayerMaterials;

	[NonSerialized]
	public bool m_outOfInstances;

	[NonSerialized]
	public Texture2D m_objectColorMap;

	[NonSerialized]
	public CommandBuffer m_overlayBuffer;

	private uint m_currentFrame;

	private uint m_currentInstance;

	private volatile int m_lastUpdatedGroup;

	private volatile int m_requiredUpdatedGroup;

	private object m_updateLock;

	private float m_requiredAspect;

	private float m_cameraHeight;

	private float m_shadowDistance;

	private Camera m_mainCamera;

	private Camera m_undergroundCamera;

	private RenderManager.CameraInfo m_cameraInfo;

	private LightSystem m_lightSystem;

	private Light m_mainLight;

	private OverlayEffect m_overlay;

	private static FastList<IRenderableManager> m_renderables = new FastList<IRenderableManager>();

	private static int m_levelOfDetail = DefaultSettings.levelOfDetail;

	private static float m_levelOfDetailFactor = 1f;

	private static float m_levelOfDetailFactor2 = 1f;

	public struct Instance
	{
		public Vector3 m_position;

		public Quaternion m_rotation;

		public RenderInstanceData m_extraData;

		public Texture m_dataTexture0;

		public Texture m_dataTexture1;

		public InstanceManager.NameData m_nameData;

		public Matrix4x4 m_dataMatrix0;

		public Matrix4x4 m_dataMatrix1;

		public Matrix4x4 m_dataMatrix2;

		public Vector4 m_dataVector0;

		public Vector4 m_dataVector1;

		public Vector4 m_dataVector2;

		public Vector4 m_dataVector3;

		public Color m_dataColor0;

		public float m_dataFloat0;

		public int m_dataInt0;

		public int m_instanceCount;

		public uint m_lastUse;

		public uint m_holder;

		public ushort m_nextInstance;

		public bool m_dirty;

		public bool m_initialized;
	}

	public class CameraInfo
	{
		public bool Intersect(Bounds bounds)
		{
			Vector3 min = bounds.get_min();
			Vector3 max = bounds.get_max();
			if (this.m_shadowOffset.x > 0f)
			{
				max.x += this.m_shadowOffset.x;
			}
			else
			{
				min.x += this.m_shadowOffset.x;
			}
			if (this.m_shadowOffset.y > 0f)
			{
				max.y += this.m_shadowOffset.y;
			}
			else
			{
				min.y += this.m_shadowOffset.y;
			}
			if (this.m_shadowOffset.z > 0f)
			{
				max.z += this.m_shadowOffset.z;
			}
			else
			{
				min.z += this.m_shadowOffset.z;
			}
			Vector3 min2 = this.m_bounds.get_min();
			Vector3 max2 = this.m_bounds.get_max();
			return min.x <= max2.x && max.x >= min2.x && min.y <= max2.y && max.y >= min2.y && min.z <= max2.z && max.z >= min2.z && MathUtils.InsideFrustum(min, max, this.m_planeA, this.m_planeB, this.m_planeC, this.m_planeD, this.m_planeE, this.m_planeF);
		}

		public bool Intersect(Vector3 point, float radius)
		{
			Vector3 min;
			min..ctor(point.x - radius, point.y - radius, point.z - radius);
			Vector3 max;
			max..ctor(point.x + radius, point.y + radius, point.z + radius);
			if (this.m_shadowOffset.x > 0f)
			{
				max.x += this.m_shadowOffset.x;
			}
			else
			{
				min.x += this.m_shadowOffset.x;
			}
			if (this.m_shadowOffset.y > 0f)
			{
				max.y += this.m_shadowOffset.y;
			}
			else
			{
				min.y += this.m_shadowOffset.y;
			}
			if (this.m_shadowOffset.z > 0f)
			{
				max.z += this.m_shadowOffset.z;
			}
			else
			{
				min.z += this.m_shadowOffset.z;
			}
			Vector3 min2 = this.m_bounds.get_min();
			Vector3 max2 = this.m_bounds.get_max();
			return min.x <= max2.x && max.x >= min2.x && min.y <= max2.y && max.y >= min2.y && min.z <= max2.z && max.z >= min2.z && MathUtils.InsideFrustum(min, max, this.m_planeA, this.m_planeB, this.m_planeC, this.m_planeD, this.m_planeE, this.m_planeF);
		}

		public bool CheckRenderDistance(Vector3 point, float maxDistance)
		{
			Vector3 vector = point - this.m_position - this.m_forward * (maxDistance * 0.45f);
			return Vector3.SqrMagnitude(vector) < maxDistance * maxDistance * 0.3025f;
		}

		public Camera m_camera;

		public int m_layerMask;

		public Quaternion m_rotation;

		public Quaternion m_shadowRotation;

		public Vector3 m_position;

		public Vector3 m_right;

		public Vector3 m_up;

		public Vector3 m_forward;

		public Vector3 m_shadowOffset;

		public float m_near;

		public float m_far;

		public float m_height;

		public Bounds m_bounds;

		public Bounds m_nearBounds;

		public Plane m_planeA;

		public Plane m_planeB;

		public Plane m_planeC;

		public Plane m_planeD;

		public Plane m_planeE;

		public Plane m_planeF;

		public Vector3 m_directionA;

		public Vector3 m_directionB;

		public Vector3 m_directionC;

		public Vector3 m_directionD;
	}
}
