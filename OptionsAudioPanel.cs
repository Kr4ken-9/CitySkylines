﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class OptionsAudioPanel : UICustomControl
{
	private void Awake()
	{
		AudioListener.set_volume(this.m_MainAudioVolume);
		this.m_EnableAudio = true;
		bool isVisible = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		base.Find("Broadcast").set_isVisible(isVisible);
		this.PopulateRadioStreamWarning();
	}

	private void PopulateRadioStreamWarning()
	{
		UILabel uILabel = base.Find<UILabel>("RadioLegalTitle");
		uILabel.set_text(Locale.Get("STREAMINGWARNING", "Title"));
		UILabel uILabel2 = base.Find<UILabel>("RadioLegalMessage");
		string text = Environment.NewLine + "- " + Locale.Get("RADIO_CHANNEL_TITLE", "GoldFM");
		text = StringUtils.SafeFormat(Locale.Get("STREAMINGWARNING", "Message"), text);
		uILabel2.set_text(text);
	}

	private void OnLocaleChanged()
	{
		this.PopulateRadioStreamWarning();
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded += new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded += new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
			LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		}
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded -= new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded -= new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
			LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		}
	}

	public string mainAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.mainAudioVolume * 100f));
		}
	}

	public float mainAudioVolume
	{
		get
		{
			return this.m_MainAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, 1f, true);
			}
			this.m_MainAudioVolume.set_value(value);
			AudioListener.set_volume(value);
		}
	}

	public string effectAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.effectAudioVolume * 100f));
		}
	}

	public float effectAudioVolume
	{
		get
		{
			return this.m_EffectAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_EffectAudioVolume.set_value(value);
		}
	}

	public string uiAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.uiAudioVolume * 100f));
		}
	}

	public float uiAudioVolume
	{
		get
		{
			return this.m_UiAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_UiAudioVolume.set_value(value);
		}
	}

	public string chirperAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.chirperAudioVolume * 100f));
		}
	}

	public float chirperAudioVolume
	{
		get
		{
			return this.m_ChirperAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_ChirperAudioVolume.set_value(value);
		}
	}

	public string ambientAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.ambientAudioVolume * 100f));
		}
	}

	public float ambientAudioVolume
	{
		get
		{
			return this.m_AmbientAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_AmbientAudioVolume.set_value(value);
		}
	}

	public string musicAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.musicAudioVolume * 100f));
		}
	}

	public float musicAudioVolume
	{
		get
		{
			return this.m_MusicAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_MusicAudioVolume.set_value(value);
		}
	}

	public string broadcastAudioPercentage
	{
		get
		{
			return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Round(this.broadcastAudioVolume * 100f));
		}
	}

	public float broadcastAudioVolume
	{
		get
		{
			return this.m_BroadcastAudioVolume;
		}
		set
		{
			if (this.m_EnableAudio && this.m_VolumeChangedSound != null && Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_VolumeChangedSound, value, true);
			}
			this.m_BroadcastAudioVolume.set_value(value);
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.m_EnableAudio = true;
	}

	private void OnLevelUnloaded()
	{
		this.m_EnableAudio = false;
	}

	public AudioClip m_VolumeChangedSound;

	private bool m_EnableAudio;

	private SavedFloat m_MainAudioVolume = new SavedFloat(Settings.mainAudioVolume, Settings.gameSettingsFile, DefaultSettings.mainAudioVolume, true);

	private SavedFloat m_MusicAudioVolume = new SavedFloat(Settings.musicAudioVolume, Settings.gameSettingsFile, DefaultSettings.musicAudioVolume, true);

	private SavedFloat m_AmbientAudioVolume = new SavedFloat(Settings.ambientAudioVolume, Settings.gameSettingsFile, DefaultSettings.ambientAudioVolume, true);

	private SavedFloat m_BroadcastAudioVolume = new SavedFloat(Settings.broadcastAudioVolume, Settings.gameSettingsFile, DefaultSettings.broadcastAudioVolume, true);

	private SavedFloat m_EffectAudioVolume = new SavedFloat(Settings.effectAudioVolume, Settings.gameSettingsFile, DefaultSettings.effectAudioVolume, true);

	private SavedFloat m_UiAudioVolume = new SavedFloat(Settings.uiAudioVolume, Settings.gameSettingsFile, DefaultSettings.uiAudioVolume, true);

	private SavedFloat m_ChirperAudioVolume = new SavedFloat(Settings.chirperAudioVolume, Settings.gameSettingsFile, DefaultSettings.chirperAudioVolume, true);
}
