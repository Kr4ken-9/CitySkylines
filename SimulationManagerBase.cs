﻿using System;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public abstract class SimulationManagerBase<Manager, Properties> : Singleton<Manager> where Manager : Component where Properties : Component
{
	public string GetName()
	{
		if (base.get_gameObject() != null)
		{
			return base.get_gameObject().get_name();
		}
		return "null";
	}

	public ThreadProfiler GetSimulationProfiler()
	{
		return this.m_simulationProfiler;
	}

	public DrawCallData GetDrawCallData()
	{
		return this.m_drawCallData;
	}

	protected virtual void Awake()
	{
		this.m_simulationProfiler = new ThreadProfiler();
		this.m_sampleName = base.get_gameObject().get_name();
	}

	protected virtual void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
	}

	protected virtual void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
	}

	protected virtual void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
	}

	protected virtual void EndOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
	}

	protected virtual void UndergroundOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
	}

	protected virtual void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
	}

	public void BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		try
		{
			this.m_drawCallData.m_defaultCalls = 0;
			this.m_drawCallData.m_lodCalls = 0;
			this.m_drawCallData.m_batchedCalls = 0;
			this.BeginRenderingImpl(cameraInfo);
		}
		finally
		{
		}
	}

	public void EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		try
		{
			this.EndRenderingImpl(cameraInfo);
		}
		finally
		{
		}
	}

	public void BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		try
		{
			this.m_drawCallData.m_overlayCalls = 0;
			this.BeginOverlayImpl(cameraInfo);
		}
		finally
		{
		}
	}

	public void EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		try
		{
			this.EndOverlayImpl(cameraInfo);
		}
		finally
		{
		}
	}

	public void UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		try
		{
			this.UndergroundOverlayImpl(cameraInfo);
		}
		finally
		{
		}
	}

	public void PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		try
		{
			this.PlayAudioImpl(listenerInfo);
		}
		finally
		{
		}
	}

	public virtual void InitializeProperties(Properties properties)
	{
		this.m_properties = properties;
	}

	public virtual void DestroyProperties(Properties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_properties = (Properties)((object)null);
		}
	}

	public virtual void CheckReferences()
	{
	}

	public virtual void InitRenderData()
	{
	}

	protected virtual void SimulationStepImpl(int subStep)
	{
	}

	public void SimulationStep(int subStep)
	{
		this.m_simulationProfiler.BeginStep();
		try
		{
			this.SimulationStepImpl(subStep);
		}
		finally
		{
			this.m_simulationProfiler.EndStep();
		}
	}

	public virtual void EarlyUpdateData()
	{
	}

	public virtual void UpdateData(SimulationManager.UpdateMode mode)
	{
	}

	public virtual void LateUpdateData(SimulationManager.UpdateMode mode)
	{
	}

	public virtual void GetData(FastList<IDataContainer> data)
	{
	}

	public virtual bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return false;
	}

	public virtual void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
	}

	public virtual void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
	}

	public virtual void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
	}

	[NonSerialized]
	public ThreadProfiler m_simulationProfiler;

	[NonSerialized]
	public DrawCallData m_drawCallData;

	[NonSerialized]
	public Properties m_properties;

	[NonSerialized]
	private string m_sampleName;
}
