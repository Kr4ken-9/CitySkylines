﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class VehicleAI : PrefabAI
{
	public virtual void InitializeAI()
	{
	}

	public virtual void ReleaseAI()
	{
	}

	public virtual Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			if (infoMode == InfoManager.InfoMode.TrafficRoutes && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
			{
				InstanceID empty = InstanceID.Empty;
				empty.Vehicle = vehicleID;
				if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[5];
				}
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if (!this.m_info.m_useColorVariations)
		{
			return this.m_info.m_color0;
		}
		Randomizer randomizer;
		randomizer..ctor((int)vehicleID);
		switch (randomizer.Int32(4u))
		{
		case 0:
			return this.m_info.m_color0;
		case 1:
			return this.m_info.m_color1;
		case 2:
			return this.m_info.m_color2;
		case 3:
			return this.m_info.m_color3;
		default:
			return this.m_info.m_color0;
		}
	}

	public virtual Color GetColor(ushort parkedVehicleID, ref VehicleParked data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if (!this.m_info.m_useColorVariations)
		{
			return this.m_info.m_color0;
		}
		Randomizer randomizer;
		randomizer..ctor((int)parkedVehicleID);
		switch (randomizer.Int32(4u))
		{
		case 0:
			return this.m_info.m_color0;
		case 1:
			return this.m_info.m_color1;
		case 2:
			return this.m_info.m_color2;
		case 3:
			return this.m_info.m_color3;
		default:
			return this.m_info.m_color0;
		}
	}

	public virtual Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Quaternion quaternion = Quaternion.Euler(swayPosition.z * 57.29578f, 0f, swayPosition.x * -57.29578f);
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation * quaternion, scale);
		result.m13 += swayPosition.y;
		return result;
	}

	public virtual Matrix4x4 CalculateBodyMatrix(VehicleParked.Flags flags, ref Vector3 position, ref Quaternion rotation)
	{
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation, Vector3.get_one());
		return result;
	}

	public virtual Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, rotation, scale);
		return bodyMatrix.get_inverse() * matrix4x;
	}

	public virtual Matrix4x4 CalculateTyreMatrix(VehicleParked.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Matrix4x4 bodyMatrix)
	{
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, rotation, Vector3.get_one());
		return bodyMatrix.get_inverse() * matrix4x;
	}

	public virtual void RenderExtraStuff(RenderManager.CameraInfo cameraInfo, InstanceID id, Vector3 position, Quaternion rotation, Vector4 tyrePosition)
	{
	}

	public virtual string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return null;
	}

	public virtual string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return null;
	}

	public virtual void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = 0;
		max = 0;
	}

	public virtual bool GetProgressStatus(ushort vehicleID, ref Vehicle data, out float current, out float max)
	{
		current = 0f;
		max = 0f;
		return true;
	}

	public virtual RenderGroup.MeshData GetEffectMeshData()
	{
		return this.m_info.m_lodMeshData;
	}

	public virtual float GetAudioAcceleration(ref Vehicle.Frame frame1, ref Vehicle.Frame frame2, float t)
	{
		return frame2.m_velocity.get_magnitude() - frame1.m_velocity.get_magnitude();
	}

	public virtual string GetDebugString(ushort vehicleID, ref Vehicle data)
	{
		return StringUtils.SafeFormat("Speed: {0} km/h", Mathf.RoundToInt(data.GetLastFrameVelocity().get_magnitude() * 13.5f));
	}

	public virtual string GetDebugString(ushort parkedVehicleID, ref VehicleParked data)
	{
		return null;
	}

	public virtual float GetEditorVerticalOffset()
	{
		return 0f;
	}

	public virtual void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
	}

	public virtual void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
	}

	public virtual void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
	}

	public virtual void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
			if (this.m_info.m_isLargeVehicle)
			{
				int num = Mathf.Clamp((int)(lastFrameData.m_position.x / 320f + 27f), 0, 53);
				int num2 = Mathf.Clamp((int)(lastFrameData.m_position.z / 320f + 27f), 0, 53);
				this.SimulationStep(vehicleID, ref vehicleData, ref lastFrameData, leaderID, ref leaderData, lodPhysics);
				int num3 = Mathf.Clamp((int)(lastFrameData.m_position.x / 320f + 27f), 0, 53);
				int num4 = Mathf.Clamp((int)(lastFrameData.m_position.z / 320f + 27f), 0, 53);
				if ((num3 != num || num4 != num2) && (vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
				{
					Singleton<VehicleManager>.get_instance().RemoveFromGrid(vehicleID, ref vehicleData, true, num, num2);
					Singleton<VehicleManager>.get_instance().AddToGrid(vehicleID, ref vehicleData, true, num3, num4);
				}
			}
			else
			{
				int num5 = Mathf.Clamp((int)(lastFrameData.m_position.x / 32f + 270f), 0, 539);
				int num6 = Mathf.Clamp((int)(lastFrameData.m_position.z / 32f + 270f), 0, 539);
				this.SimulationStep(vehicleID, ref vehicleData, ref lastFrameData, leaderID, ref leaderData, lodPhysics);
				int num7 = Mathf.Clamp((int)(lastFrameData.m_position.x / 32f + 270f), 0, 539);
				int num8 = Mathf.Clamp((int)(lastFrameData.m_position.z / 32f + 270f), 0, 539);
				if ((num7 != num5 || num8 != num6) && (vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
				{
					Singleton<VehicleManager>.get_instance().RemoveFromGrid(vehicleID, ref vehicleData, false, num5, num6);
					Singleton<VehicleManager>.get_instance().AddToGrid(vehicleID, ref vehicleData, false, num7, num8);
				}
			}
			if (vehicleData.m_flags != (Vehicle.Flags)0)
			{
				this.FrameDataUpdated(vehicleID, ref vehicleData, ref lastFrameData);
				vehicleData.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, lastFrameData);
			}
		}
	}

	public virtual void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
	}

	public virtual void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
	}

	public virtual void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
	}

	public virtual void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
	}

	public virtual void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
	}

	public virtual void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
	}

	public virtual void SetTransportLine(ushort vehicleID, ref Vehicle data, ushort transportLine)
	{
	}

	public virtual void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
	}

	public virtual void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = 0;
	}

	public virtual bool AddWind(ushort vehicleID, ref Vehicle data, Vector3 wind, InstanceManager.Group group)
	{
		return false;
	}

	public virtual bool AddWind(ushort parkedID, ref VehicleParked data, Vector3 wind, InstanceManager.Group group)
	{
		return false;
	}

	protected void EnsureCitizenUnits(ushort vehicleID, ref Vehicle data, int passengerCount)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = 0u;
		uint num2 = data.m_citizenUnits;
		int num3 = 0;
		while (num2 != 0u)
		{
			CitizenUnit.Flags flags = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_flags;
			if ((ushort)(flags & CitizenUnit.Flags.Vehicle) != 0)
			{
				passengerCount -= 5;
			}
			num = num2;
			num2 = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
			if (++num3 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		passengerCount = Mathf.Max(0, passengerCount);
		if (passengerCount != 0)
		{
			uint num4 = 0u;
			if (instance.CreateUnits(out num4, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, passengerCount, 0))
			{
				if (num != 0u)
				{
					instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit = num4;
				}
				else
				{
					data.m_citizenUnits = num4;
				}
			}
		}
	}

	private static int FindBestLane(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		float num = 100000f;
		if (info != null && info.m_lanes != null)
		{
			NetInfo.Direction direction = NetInfo.Direction.Forward;
			float num2 = 0f;
			int lane = (int)position.m_lane;
			if ((int)position.m_lane < info.m_lanes.Length)
			{
				NetInfo.Lane lane2 = info.m_lanes[(int)position.m_lane];
				direction = (lane2.m_finalDirection & NetInfo.Direction.Both);
				num2 = lane2.m_position;
			}
			uint num3 = instance.m_segments.m_buffer[(int)position.m_segment].m_lanes;
			int num4 = 0;
			while (num4 < info.m_lanes.Length && num3 != 0u)
			{
				NetInfo.Lane lane3 = info.m_lanes[num4];
				if ((byte)(lane3.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) != 0 && (lane3.m_vehicleType & (VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Tram)) != VehicleInfo.VehicleType.None && (lane3.m_finalDirection & direction) == direction)
				{
					float num5 = instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].GetReservedSpace();
					if (num4 == lane)
					{
						num5 -= 1f + vehicleData.CalculateTotalLength(vehicleID);
					}
					num5 += Mathf.Abs(num2 - lane3.m_position) * 0.1f;
					if (num5 < num)
					{
						num = num5;
						position.m_lane = (byte)num4;
					}
				}
				num3 = instance.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_nextLane;
				num4++;
			}
		}
		return (int)position.m_lane;
	}

	protected void UpdatePathTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ref int index, int max, float minSqrDistanceA, float minSqrDistanceB)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = vehicleData.m_targetPos0;
		vector.w = 1000f;
		float num = minSqrDistanceA;
		uint num2 = vehicleData.m_path;
		byte b = vehicleData.m_pathPositionIndex;
		byte b2 = vehicleData.m_lastPathOffset;
		if (b == 255)
		{
			b = 0;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = 0;
			}
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)num2)].CalculatePathPositionOffset(b >> 1, vector, out b2))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
				return;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].GetPosition(b >> 1, out position))
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		NetInfo info = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes.Length <= (int)position.m_lane)
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		uint num3 = PathManager.GetLaneID(position);
		NetInfo.Lane lane = info.m_lanes[(int)position.m_lane];
		Bezier3 bezier;
		while (true)
		{
			if ((b & 1) == 0)
			{
				if (lane.m_laneType != NetInfo.LaneType.CargoVehicle)
				{
					bool flag = true;
					while (b2 != position.m_offset)
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							float num4 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
							int num5;
							if (num4 < 0f)
							{
								num5 = 4;
							}
							else
							{
								num5 = 4 + Mathf.Max(0, Mathf.CeilToInt(num4 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_length + 1f)));
							}
							if (b2 > position.m_offset)
							{
								b2 = (byte)Mathf.Max((int)b2 - num5, (int)position.m_offset);
							}
							else if (b2 < position.m_offset)
							{
								b2 = (byte)Mathf.Min((int)b2 + num5, (int)position.m_offset);
							}
						}
						Vector3 vector2;
						Vector3 vector3;
						float num6;
						this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, b2, out vector2, out vector3, out num6);
						vector.Set(vector2.x, vector2.y, vector2.z, Mathf.Min(vector.w, num6));
						float sqrMagnitude = (vector2 - refPos).get_sqrMagnitude();
						if (sqrMagnitude >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
				b += 1;
				b2 = 0;
				if (index <= 0)
				{
					vehicleData.m_pathPositionIndex = b;
					vehicleData.m_lastPathOffset = b2;
				}
			}
			int num7 = (b >> 1) + 1;
			uint num8 = num2;
			if (num7 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_positionCount)
			{
				num7 = 0;
				num8 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_nextPathUnit;
				if (num8 == 0u)
				{
					goto Block_17;
				}
			}
			PathUnit.Position position2;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].GetPosition(num7, out position2))
			{
				goto Block_19;
			}
			if ((vehicleData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0 && this.m_info.m_vehicleType == VehicleInfo.VehicleType.Car)
			{
				int num9 = VehicleAI.FindBestLane(vehicleID, ref vehicleData, position2);
				if (num9 != (int)position2.m_lane)
				{
					position2.m_lane = (byte)num9;
					instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].SetPosition(num7, position2);
				}
			}
			NetInfo info2 = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
			if (info2.m_lanes.Length <= (int)position2.m_lane)
			{
				goto Block_23;
			}
			uint laneID = PathManager.GetLaneID(position2);
			NetInfo.Lane lane2 = info2.m_lanes[(int)position2.m_lane];
			ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			ushort endNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			ushort startNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
			ushort endNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
			if (startNode2 != startNode && startNode2 != endNode && endNode2 != startNode && endNode2 != endNode && ((instance2.m_nodes.m_buffer[(int)startNode].m_flags | instance2.m_nodes.m_buffer[(int)endNode].m_flags) & NetNode.Flags.Disabled) == NetNode.Flags.None && ((instance2.m_nodes.m_buffer[(int)startNode2].m_flags | instance2.m_nodes.m_buffer[(int)endNode2].m_flags) & NetNode.Flags.Disabled) != NetNode.Flags.None)
			{
				goto Block_29;
			}
			if (lane2.m_laneType == NetInfo.LaneType.Pedestrian)
			{
				goto Block_30;
			}
			if ((byte)(lane2.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) == 0)
			{
				goto Block_36;
			}
			if (lane2.m_vehicleType != this.m_info.m_vehicleType && this.NeedChangeVehicleType(vehicleID, ref vehicleData, position2, laneID, lane2.m_vehicleType, ref vector))
			{
				goto Block_38;
			}
			if (position2.m_segment != position.m_segment && vehicleID != 0)
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Leaving;
			}
			byte b3 = 0;
			if ((vehicleData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
			{
				b3 = ((position2.m_offset < 128) ? 255 : 0);
			}
			else if (num3 != laneID && lane.m_laneType != NetInfo.LaneType.CargoVehicle)
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
				bezier = default(Bezier3);
				Vector3 vector4;
				float num10;
				this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, position.m_offset, out bezier.a, out vector4, out num10);
				bool flag2 = b2 == 0;
				if (flag2)
				{
					if ((vehicleData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
					{
						flag2 = (vehicleData.m_trailingVehicle == 0);
					}
					else
					{
						flag2 = (vehicleData.m_leadingVehicle == 0);
					}
				}
				Vector3 vector5;
				float num11;
				if (flag2)
				{
					PathUnit.Position nextPosition;
					if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].GetNextPosition(num7, out nextPosition))
					{
						nextPosition = default(PathUnit.Position);
					}
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position2, laneID, b3, position, num3, position.m_offset, index, out bezier.d, out vector5, out num11);
				}
				else
				{
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position2, laneID, b3, out bezier.d, out vector5, out num11);
				}
				if (num11 < 0.01f || (instance2.m_segments.m_buffer[(int)position2.m_segment].m_flags & (NetSegment.Flags.Collapsed | NetSegment.Flags.Flooded)) != NetSegment.Flags.None)
				{
					goto IL_8C9;
				}
				if (position.m_offset == 0)
				{
					vector4 = -vector4;
				}
				if (b3 < position2.m_offset)
				{
					vector5 = -vector5;
				}
				vector4.Normalize();
				vector5.Normalize();
				float num12;
				NetSegment.CalculateMiddlePoints(bezier.a, vector4, bezier.d, vector5, true, true, out bezier.b, out bezier.c, out num12);
				if (num12 > 1f)
				{
					ushort num13;
					if (b3 == 0)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					}
					else if (b3 == 255)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
					}
					else
					{
						num13 = 0;
					}
					float num14 = 1.57079637f * (1f + Vector3.Dot(vector4, vector5));
					if (num12 > 1f)
					{
						num14 /= num12;
					}
					num11 = Mathf.Min(num11, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14));
					while (b2 < 255)
					{
						float num15 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
						int num16;
						if (num15 < 0f)
						{
							num16 = 8;
						}
						else
						{
							num16 = 8 + Mathf.Max(0, Mathf.CeilToInt(num15 * 256f / (num12 + 1f)));
						}
						b2 = (byte)Mathf.Min((int)b2 + num16, 255);
						Vector3 vector6 = bezier.Position((float)b2 * 0.003921569f);
						vector.Set(vector6.x, vector6.y, vector6.z, Mathf.Min(vector.w, num11));
						float sqrMagnitude2 = (vector6 - refPos).get_sqrMagnitude();
						if (sqrMagnitude2 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							if (num13 != 0)
							{
								this.UpdateNodeTargetPos(vehicleID, ref vehicleData, num13, ref instance2.m_nodes.m_buffer[(int)num13], ref vector, index);
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
			}
			else
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
			}
			if (index <= 0)
			{
				if (num7 == 0)
				{
					Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
				}
				if (num7 >= (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].m_positionCount - 1) && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num8)].m_nextPathUnit == 0u && vehicleID != 0)
				{
					this.ArrivingToDestination(vehicleID, ref vehicleData);
				}
			}
			num2 = num8;
			b = (byte)(num7 << 1);
			b2 = b3;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = b;
				vehicleData.m_lastPathOffset = b2;
				vehicleData.m_flags = ((vehicleData.m_flags & ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition)) | info2.m_setVehicleFlags);
				if (this.LeftHandDrive(lane2))
				{
					vehicleData.m_flags |= Vehicle.Flags.LeftHandDrive;
				}
				else
				{
					vehicleData.m_flags &= (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding);
				}
			}
			position = position2;
			num3 = laneID;
			lane = lane2;
		}
		return;
		Block_17:
		if (index <= 0)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		vector.w = 1f;
		vehicleData.SetTargetPos(index++, vector);
		return;
		Block_19:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_23:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_29:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_30:
		if (vehicleID != 0 && (vehicleData.m_flags & Vehicle.Flags.Parking) == (Vehicle.Flags)0)
		{
			byte offset = position.m_offset;
			byte offset2 = position.m_offset;
			int num7;
			uint num8;
			if (this.ParkVehicle(vehicleID, ref vehicleData, position, num8, num7 << 1, out offset2))
			{
				if (offset2 != offset)
				{
					if (index <= 0)
					{
						vehicleData.m_pathPositionIndex = (byte)((int)vehicleData.m_pathPositionIndex & -2);
						vehicleData.m_lastPathOffset = offset;
					}
					position.m_offset = offset2;
					instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].SetPosition(b >> 1, position);
				}
				vehicleData.m_flags |= Vehicle.Flags.Parking;
			}
			else
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		return;
		Block_36:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_38:
		float sqrMagnitude3 = (vector - refPos).get_sqrMagnitude();
		if (sqrMagnitude3 >= num)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
		if (index <= 0)
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
			uint num8;
			if (num8 != vehicleData.m_path)
			{
				Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
			}
			int num7;
			vehicleData.m_pathPositionIndex = (byte)(num7 << 1);
			uint laneID;
			PathUnit.CalculatePathPositionOffset(laneID, vector, out vehicleData.m_lastPathOffset);
			PathUnit.Position position2;
			if (vehicleID != 0 && !this.ChangeVehicleType(vehicleID, ref vehicleData, position2, laneID))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		else
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
		}
		return;
		IL_8C9:
		if (index <= 0)
		{
			vehicleData.m_lastPathOffset = b2;
		}
		vector = bezier.a;
		vector.w = 0f;
		while (index < max)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
	}

	protected virtual bool LeftHandDrive(NetInfo.Lane lane)
	{
		return Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True;
	}

	protected virtual void UpdateNodeTargetPos(ushort vehicleID, ref Vehicle vehicleData, ushort nodeID, ref NetNode nodeData, ref Vector4 targetPos, int index)
	{
	}

	public virtual void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if (leaderData.m_targetBuilding != 0)
		{
			Vector4 pos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)leaderData.m_targetBuilding].m_position;
			pos.w = 2f;
			vehicleData.SetTargetPos(index++, pos);
			return;
		}
	}

	public virtual bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		return true;
	}

	protected virtual void ArrivingToDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		vehicleData.m_flags |= Vehicle.Flags.Arriving;
	}

	protected virtual bool NeedChangeVehicleType(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint laneID, VehicleInfo.VehicleType laneVehicleType, ref Vector4 target)
	{
		return false;
	}

	protected virtual bool ChangeVehicleType(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint laneID)
	{
		return false;
	}

	protected virtual void InvalidPath(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData)
	{
		if (vehicleID != 0 && vehicleData.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		if (leaderID != 0)
		{
			if (leaderData.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(leaderData.m_path);
				leaderData.m_path = 0u;
			}
			if (!this.StartPathFind(leaderID, ref leaderData))
			{
				leaderData.Unspawn(leaderID);
			}
		}
	}

	protected virtual float CalculateTargetSpeed(ushort vehicleID, ref Vehicle data, float speedLimit, float curve)
	{
		float num = 1000f / (1f + curve * 1000f / this.m_info.m_turning) + 2f;
		float num2 = 8f * speedLimit;
		return Mathf.Min(Mathf.Min(num, num2), this.m_info.m_maxSpeed);
	}

	protected virtual void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected virtual void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_targetBuilding != 0)
		{
			Vector3 endPos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].CalculateSidewalkPosition();
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
		}
		return false;
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return false;
	}

	public virtual bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = vehicleData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0 && (instance.m_instances.m_buffer[(int)instance2].m_flags & CitizenInstance.Flags.EnteringVehicle) != CitizenInstance.Flags.None)
					{
						return false;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return true;
	}

	public virtual bool CanSpawnAt(Vector3 pos)
	{
		return true;
	}

	protected virtual bool ParkVehicle(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position pathPos, uint nextPath, int nextPositionIndex, out byte segmentOffset)
	{
		segmentOffset = 0;
		return false;
	}

	public virtual void UpdateParkedVehicle(ushort parkedID, ref VehicleParked parkedData)
	{
	}

	public virtual bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	public virtual int GetTicketPrice(ushort vehicleID, ref Vehicle vehicleData)
	{
		return 0;
	}

	public virtual InstanceID GetOwnerID(ushort vehicleID, ref Vehicle vehicleData)
	{
		return new InstanceID
		{
			Building = vehicleData.m_sourceBuilding
		};
	}

	public virtual InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		return new InstanceID
		{
			Building = vehicleData.m_targetBuilding
		};
	}

	protected virtual bool IgnoreBlocked(ushort vehicleID, ref Vehicle vehicleData)
	{
		return false;
	}

	public virtual bool VerticalTrailers()
	{
		return false;
	}

	public virtual int GetNoiseLevel()
	{
		return 0;
	}

	[NonSerialized]
	public VehicleInfo m_info;
}
