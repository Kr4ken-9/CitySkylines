﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;

public abstract class LivingCreatureWorldInfoPanel : WorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_NameField = base.Find<UITextComponent>("PersonName");
		if (this.m_NameField is UITextField)
		{
			((UITextField)this.m_NameField).add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		}
		if (this.m_NameField is UIButton)
		{
			((UIButton)this.m_NameField).add_eventClick(new MouseEventHandler(this.OnTargetClick));
		}
	}

	protected void OnTargetClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != null && ToolsModifierControl.cameraController != null)
		{
			ToolsModifierControl.cameraController.SetTarget((InstanceID)p.get_source().get_objectUserData(), this.m_WorldMousePosition, false);
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		LivingCreatureWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new LivingCreatureWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		if (this.m_InstanceID.Type == InstanceType.Citizen && this.m_InstanceID.Citizen != 0u)
		{
			return Singleton<CitizenManager>.get_instance().GetCitizenName(this.m_InstanceID.Citizen);
		}
		if (this.m_InstanceID.Type == InstanceType.CitizenInstance && this.m_InstanceID.CitizenInstance != 0)
		{
			return Singleton<CitizenManager>.get_instance().GetInstanceName(this.m_InstanceID.CitizenInstance);
		}
		return string.Empty;
	}

	protected override void OnSetTarget()
	{
		this.m_NameField.set_text(this.GetName());
		if (this.m_NameField is UIButton)
		{
			this.m_NameField.set_objectUserData(this.m_InstanceID);
			base.ShortenTextToFitParent((UIButton)this.m_NameField);
		}
		this.UpdateBindings();
	}

	protected UITextComponent m_NameField;
}
