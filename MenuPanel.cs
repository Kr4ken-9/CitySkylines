﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public abstract class MenuPanel : UICustomControl
{
	protected virtual void Awake()
	{
		base.get_component().Hide();
	}

	private void Start()
	{
		this.Initialize();
	}

	protected virtual void Initialize()
	{
		if (base.get_component().get_isVisible())
		{
			base.get_component().Focus();
		}
	}

	public virtual void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	public static void GoToMenu(UIComponent comp)
	{
		MainMenu.m_CurrentlyActiveMenu.Hide();
		MainMenu.m_CurrentlyActiveMenu = comp;
		comp.Show();
		comp.Focus();
	}

	protected virtual UIComponent CreateMenuItem(string menuItemPrefab, string menuItemName, string localizedText)
	{
		return this.CreateMenuItem(menuItemPrefab, menuItemName, localizedText, base.get_component());
	}

	protected virtual UIComponent CreateMenuItem(string menuItemPrefab, string menuItemName, string localizedText, UIComponent parent)
	{
		GameObject asGameObject = UITemplateManager.GetAsGameObject(menuItemPrefab);
		asGameObject.set_name(menuItemName);
		UIComponent uIComponent = parent.AttachUIComponent(asGameObject);
		UIButton uIButton = uIComponent as UIButton;
		if (uIButton != null)
		{
			uIButton.set_text(localizedText);
			uIButton.set_stringUserData(menuItemName);
		}
		return uIComponent;
	}
}
