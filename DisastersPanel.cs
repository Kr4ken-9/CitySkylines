﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public sealed class DisastersPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(BuildingInfo info)
	{
		return false;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Disaster);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		DisasterInfo disasterInfo = objectUserData as DisasterInfo;
		if (disasterInfo != null)
		{
			DisasterTool disasterTool = ToolsModifierControl.SetTool<DisasterTool>();
			if (disasterTool != null)
			{
				disasterTool.m_prefab = disasterInfo;
			}
			if (disasterInfo.m_disasterAI.SupportIntensity() && Singleton<ToolManager>.get_exists() && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				base.ShowDisastersOptionPanel();
			}
			else
			{
				base.HideAllOptionPanels();
			}
		}
	}
}
