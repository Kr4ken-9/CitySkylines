﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class ShelterWorldInfoPanel : BuildingWorldInfoPanel
{
	public UIComponent movingPanel
	{
		get
		{
			if (this.m_MovingPanel == null)
			{
				this.m_MovingPanel = UIView.Find("MovingPanel");
				this.m_MovingPanel.Find<UIButton>("Close").add_eventClick(new MouseEventHandler(this.OnMovingPanelCloseClicked));
				this.m_MovingPanel.Hide();
			}
			return this.m_MovingPanel;
		}
	}

	public bool isCityServiceEnabled
	{
		get
		{
			return Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_productionRate != 0;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && this.m_InstanceID.Building != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleBuilding(this.m_InstanceID.Building, value));
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator ToggleBuilding(ushort id, bool value)
	{
		ShelterWorldInfoPanel.<ToggleBuilding>c__Iterator0 <ToggleBuilding>c__Iterator = new ShelterWorldInfoPanel.<ToggleBuilding>c__Iterator0();
		<ToggleBuilding>c__Iterator.id = id;
		<ToggleBuilding>c__Iterator.value = value;
		return <ToggleBuilding>c__Iterator;
	}

	protected override void Start()
	{
		base.Start();
		this.m_Type = base.Find<UILabel>("Type");
		this.m_Status = base.Find<UILabel>("Status");
		this.m_Upkeep = base.Find<UILabel>("Upkeep");
		this.m_Thumbnail = base.Find<UISprite>("Thumbnail");
		this.m_BuildingInfo = base.Find<UILabel>("Info");
		this.m_BuildingDesc = base.Find<UILabel>("Desc");
		this.m_BuildingService = base.Find<UISprite>("Service");
		this.m_ProgressLabelWater = base.Find<UILabel>("ProgressLabelWater");
		this.m_ProgressLabelFood = base.Find<UILabel>("ProgressLabelFood");
		this.m_ProgressLabelPower = base.Find<UILabel>("ProgressLabelPower");
		this.m_ProgressBarWater = base.Find<UIProgressBar>("ProgressBarWater");
		this.m_ProgressBarFood = base.Find<UIProgressBar>("ProgressBarFood");
		this.m_ProgressBarPower = base.Find<UIProgressBar>("ProgressBarPower");
		this.m_EvacButton = base.Find<UIButton>("EvacButton");
		this.m_EvacButton.add_eventClicked(new MouseEventHandler(this.OnEvacButtonClicked));
		this.m_MoveButton = base.Find<UIButton>("RelocateAction");
	}

	public bool isEvacuating
	{
		get
		{
			return Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Building != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].m_flags & Building.Flags.Downgrading) == Building.Flags.None;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists() && this.m_InstanceID.Building != 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.ToggleEmptying(this.m_InstanceID.Building, !value));
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator ToggleEmptying(ushort id, bool value)
	{
		ShelterWorldInfoPanel.<ToggleEmptying>c__Iterator1 <ToggleEmptying>c__Iterator = new ShelterWorldInfoPanel.<ToggleEmptying>c__Iterator1();
		<ToggleEmptying>c__Iterator.id = id;
		<ToggleEmptying>c__Iterator.value = value;
		return <ToggleEmptying>c__Iterator;
	}

	private void OnEvacButtonClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.isEvacuating = !this.isEvacuating;
		this.RefreshEvacButton();
	}

	private void RefreshEvacButton()
	{
		if (this.isEvacuating)
		{
			this.m_EvacButton.set_text(Locale.Get("SHELTER_RELEASE"));
			this.m_EvacButton.set_tooltip(Locale.Get("SHELTER_RELEASE_TOOLTIP"));
			this.m_EvacButton.set_textColor(this.m_ReleaseColor);
			this.m_EvacButton.set_focusedTextColor(this.m_ReleaseColor);
		}
		else
		{
			this.m_EvacButton.set_text(Locale.Get("SHELTER_EVACUATE"));
			this.m_EvacButton.set_tooltip(Locale.Get("SHELTER_EVACUATE_TOOLTIP"));
			this.m_EvacButton.set_textColor(this.m_EvacColor);
			this.m_EvacButton.set_focusedTextColor(this.m_EvacColor);
		}
	}

	private void OnMovingPanelCloseClicked(UIComponent comp, UIMouseEventParameter p)
	{
		this.m_IsRelocating = false;
		ToolsModifierControl.GetTool<BuildingTool>().CancelRelocate();
	}

	private void TempHide()
	{
		ToolsModifierControl.cameraController.ClearTarget();
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.33f), delegate
		{
			UIView.get_library().Hide(base.GetType().Name);
		});
		this.movingPanel.Find<UILabel>("MovingLabel").set_text(LocaleFormatter.FormatGeneric("BUILDING_MOVING", new object[]
		{
			base.buildingName
		}));
		this.movingPanel.Show();
	}

	public void TempShow(Vector3 worldPosition, InstanceID instanceID)
	{
		this.movingPanel.Hide();
		WorldInfoPanel.Show<ShelterWorldInfoPanel>(worldPosition, instanceID);
		ValueAnimator.Animate("Relocating", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.33f));
	}

	private void Update()
	{
		if (this.m_IsRelocating)
		{
			BuildingTool currentTool = ToolsModifierControl.GetCurrentTool<BuildingTool>();
			if (currentTool != null && base.IsValidTarget() && currentTool.m_relocate != 0 && !this.movingPanel.get_isVisible())
			{
				this.movingPanel.Show();
				return;
			}
			if (!base.IsValidTarget() || (currentTool != null && currentTool.m_relocate == 0))
			{
				ToolsModifierControl.mainToolbar.ResetLastTool();
				this.movingPanel.Hide();
				this.m_IsRelocating = false;
			}
		}
	}

	private void RelocateCompleted(InstanceID newID)
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.m_IsRelocating = false;
		if (!newID.IsEmpty)
		{
			this.m_InstanceID = newID;
		}
		if (base.IsValidTarget())
		{
			BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
				Vector3 worldPosition;
				Quaternion quaternion;
				Vector3 vector;
				if (InstanceManager.GetPosition(this.m_InstanceID, out worldPosition, out quaternion, out vector))
				{
					worldPosition.y += vector.y * 0.8f;
				}
				this.TempShow(worldPosition, this.m_InstanceID);
			}
		}
		else
		{
			this.movingPanel.Hide();
			BuildingTool tool2 = ToolsModifierControl.GetTool<BuildingTool>();
			if (tool2 == ToolsModifierControl.GetCurrentTool<BuildingTool>())
			{
				ToolsModifierControl.SetTool<DefaultTool>();
			}
			base.Hide();
		}
	}

	protected override void OnHide()
	{
		if (this.m_IsRelocating && ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted -= new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		this.movingPanel.Hide();
	}

	protected override void OnSetTarget()
	{
		this.RefreshEvacButton();
		base.OnSetTarget();
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			BuildingAI buildingAI = info.m_buildingAI;
			ShelterAI shelterAI = buildingAI as ShelterAI;
			this.m_Type.set_text(Singleton<BuildingManager>.get_instance().GetDefaultBuildingName(building, InstanceID.Empty));
			this.m_Status.set_text(buildingAI.GetLocalizedStatus(building, ref instance.m_buildings.m_buffer[(int)this.m_InstanceID.Building]));
			this.m_Upkeep.set_text(LocaleFormatter.FormatUpkeep(buildingAI.GetResourceRate(building, ref instance.m_buildings.m_buffer[(int)building], EconomyManager.Resource.Maintenance), false));
			this.m_Thumbnail.set_atlas(info.m_Atlas);
			this.m_Thumbnail.set_spriteName(info.m_Thumbnail);
			if (this.m_Thumbnail.get_atlas() != null && !string.IsNullOrEmpty(this.m_Thumbnail.get_spriteName()))
			{
				UITextureAtlas.SpriteInfo spriteInfo = this.m_Thumbnail.get_atlas().get_Item(this.m_Thumbnail.get_spriteName());
				if (spriteInfo != null)
				{
					this.m_Thumbnail.set_size(spriteInfo.get_pixelSize());
				}
			}
			this.m_BuildingDesc.set_text(info.GetLocalizedDescriptionShort());
			this.m_BuildingInfo.set_text(buildingAI.GetLocalizedStats(building, ref instance.m_buildings.m_buffer[(int)building]));
			ItemClass.Service service = info.GetService();
			if (service == ItemClass.Service.Disaster)
			{
				service = ItemClass.Service.FireDepartment;
			}
			if (service != ItemClass.Service.None)
			{
				string nameByValue = Utils.GetNameByValue<ItemClass.Service>(service, "Game");
				this.m_BuildingService.set_spriteName("ToolbarIcon" + nameByValue);
				this.m_BuildingService.set_tooltip(Locale.Get("MAIN_TOOL", nameByValue));
			}
			this.m_BuildingService.set_isVisible(service != ItemClass.Service.None);
			this.m_MoveButton.set_isEnabled(buildingAI != null && buildingAI.CanBeRelocated(building, ref instance.m_buildings.m_buffer[(int)building]));
			this.RefreshEvacButton();
			int num = 0;
			int num2 = 0;
			shelterAI.GetWaterStatus(this.m_InstanceID.Building, ref instance.m_buildings.m_buffer[(int)building], out num, out num2);
			this.m_ProgressBarWater.set_value((float)num);
			this.m_ProgressBarWater.set_maxValue((float)num2);
			this.m_ProgressLabelWater.set_text(Mathf.Round((float)num / (float)num2 * 100f).ToString());
			shelterAI.GetFoodStatus(this.m_InstanceID.Building, ref instance.m_buildings.m_buffer[(int)building], out num, out num2);
			this.m_ProgressBarFood.set_value((float)num);
			this.m_ProgressBarFood.set_maxValue((float)num2);
			this.m_ProgressLabelFood.set_text(Mathf.Round((float)num / (float)num2 * 100f).ToString());
			shelterAI.GetElectricityStatus(this.m_InstanceID.Building, ref instance.m_buildings.m_buffer[(int)building], out num, out num2);
			this.m_ProgressBarPower.set_value((float)num);
			this.m_ProgressBarPower.set_maxValue((float)num2);
			this.m_ProgressLabelPower.set_text(Mathf.Round((float)num / (float)num2 * 100f).ToString());
		}
	}

	public void OnRelocateBuilding()
	{
		if (ToolsModifierControl.GetTool<BuildingTool>() != null)
		{
			ToolsModifierControl.GetTool<BuildingTool>().m_relocateCompleted += new BuildingTool.RelocateCompleted(this.RelocateCompleted);
		}
		ToolsModifierControl.keepThisWorldInfoPanel = true;
		BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
		buildingTool.m_prefab = null;
		buildingTool.m_relocate = (int)this.m_InstanceID.Building;
		this.m_IsRelocating = true;
		this.TempHide();
	}

	public void OnDrawRoute()
	{
		TransportInfo info = Singleton<TransportManager>.get_instance().GetTransportInfo(TransportInfo.TransportType.EvacuationBus);
		ushort building = this.m_InstanceID.Building;
		if (info != null && building != 0)
		{
			TransportTool transportTool = ToolsModifierControl.SetTool<TransportTool>();
			if (transportTool != null)
			{
				transportTool.m_prefab = info;
				transportTool.m_building = (int)building;
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					transportTool.StartEditingBuildingLine(info, building);
					Singleton<BuildingManager>.get_instance().m_escapeRoutes.Deactivate();
				});
			}
		}
	}

	public void OnDeleteRoute()
	{
		ushort building = this.m_InstanceID.Building;
		if (building != 0)
		{
			Singleton<SimulationManager>.get_instance().AddAction(delegate
			{
				ushort num = TransportTool.FindBuildingLine(building);
				if (num != 0)
				{
					Singleton<TransportManager>.get_instance().ReleaseLine(num);
				}
			});
		}
	}

	private UIButton m_MoveButton;

	private UIComponent m_MovingPanel;

	private UILabel m_Type;

	private UILabel m_Status;

	private UILabel m_Upkeep;

	private UISprite m_Thumbnail;

	private UILabel m_BuildingInfo;

	private UILabel m_BuildingDesc;

	private UISprite m_BuildingService;

	private UILabel m_ProgressLabelWater;

	private UILabel m_ProgressLabelFood;

	private UILabel m_ProgressLabelPower;

	private UIProgressBar m_ProgressBarWater;

	private UIProgressBar m_ProgressBarFood;

	private UIProgressBar m_ProgressBarPower;

	private bool m_IsRelocating;

	private UIButton m_EvacButton;

	public Color m_EvacColor;

	public Color m_ReleaseColor;
}
