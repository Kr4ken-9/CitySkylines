﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class ExportHeightmapPanel : HeightmapPanel
{
	public void OpenFolder()
	{
		Utils.OpenInFileBrowser(HeightmapPanel.heightMapsPath);
	}

	private void OnDestroy()
	{
		for (int i = 0; i < ExportHeightmapPanel.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void OnFileListVisibilityChanged(UIComponent comp, bool visible)
	{
		if (comp == this.m_FileList)
		{
			if (visible)
			{
				this.Refresh();
				for (int i = 0; i < ExportHeightmapPanel.m_Extensions.Length; i++)
				{
					if (this.m_FileSystemReporter[i] != null)
					{
						this.m_FileSystemReporter[i].Start();
					}
				}
			}
			else
			{
				for (int j = 0; j < ExportHeightmapPanel.m_Extensions.Length; j++)
				{
					if (this.m_FileSystemReporter[j] != null)
					{
						this.m_FileSystemReporter[j].Stop();
					}
				}
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.Refresh();
		});
	}

	private void Awake()
	{
		this.m_FileName = base.Find<UITextField>("FileName");
		this.m_FormatDropdown = base.Find<UIDropDown>("FormatDropdown");
		this.m_PrecisionDropdown = base.Find<UIDropDown>("PrecisionDropdown");
		this.m_FileList = base.Find<UIListBox>("FileList");
		this.m_ExportButton = base.Find<UIButton>("Export");
		this.m_PreviewSprite = base.Find<UITextureSprite>("Preview");
		this.m_LoadingLabel = base.Find<UILabel>("LoadingLabel");
		this.m_LoadingProgress = base.Find<UISprite>("ProgressSprite").get_transform();
		this.m_FileList.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnFileListVisibilityChanged));
	}

	private void Start()
	{
		base.get_component().Hide();
		this.m_LoadingLabel.Hide();
		this.m_ExportButton.set_isEnabled(false);
		this.Refresh();
		for (int i = 0; i < ExportHeightmapPanel.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + ExportHeightmapPanel.m_Extensions[i], HeightmapPanel.heightMapsPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
		}
	}

	private void CreateHeightmapFromTerrain()
	{
		this.m_PreviewSprite.set_opacity(0f);
		this.m_LoadingLabel.Show();
		base.StartCoroutine(this.ReadHeightmap());
	}

	[DebuggerHidden]
	private IEnumerator<byte[]> GetTerrainHeights()
	{
		return new ExportHeightmapPanel.<GetTerrainHeights>c__Iterator0();
	}

	[DebuggerHidden]
	private IEnumerator ReadHeightmap()
	{
		ExportHeightmapPanel.<ReadHeightmap>c__Iterator1 <ReadHeightmap>c__Iterator = new ExportHeightmapPanel.<ReadHeightmap>c__Iterator1();
		<ReadHeightmap>c__Iterator.$this = this;
		return <ReadHeightmap>c__Iterator;
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.CreateHeightmapFromTerrain();
			this.m_FileList.Focus();
		}
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	private void Refresh()
	{
		base.get_component().Focus();
		List<string> list = new List<string>();
		DirectoryInfo directoryInfo = new DirectoryInfo(HeightmapPanel.heightMapsPath);
		if (directoryInfo.Exists)
		{
			FileInfo[] array = null;
			try
			{
				array = directoryInfo.GetFiles();
				base.Sort(array, HeightmapPanel.SortingType.Timestamp);
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			if (array != null)
			{
				FileInfo[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					FileInfo fileInfo = array2[i];
					for (int j = 0; j < ExportHeightmapPanel.m_Extensions.Length; j++)
					{
						if (string.Compare(Path.GetExtension(fileInfo.Name), ExportHeightmapPanel.m_Extensions[j]) == 0)
						{
							list.Add(fileInfo.Name);
						}
					}
				}
			}
			this.m_FileList.set_items(list.ToArray());
			if (list.Count > 0)
			{
				this.m_FileList.set_selectedIndex(0);
				this.m_ExportButton.set_isEnabled(true);
			}
			else
			{
				this.m_ExportButton.set_isEnabled(false);
			}
		}
	}

	private void AssignPreview()
	{
		if (this.m_PreviewSprite.get_texture() != null)
		{
			Object.DestroyImmediate(this.m_PreviewSprite.get_texture());
		}
		Image image = new Image(1081, 1081, Image.kFormatAlpha16, this.m_CurrentHeightmap);
		image.Convert(3);
		this.m_PreviewSprite.set_texture(image.CreateTexture());
		ValueAnimator.Animate("ReadingHeightmap", delegate(float val)
		{
			this.m_PreviewSprite.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f, 7));
		if (this.m_CurrentHeightmap != null)
		{
			this.m_ExportButton.set_isEnabled(true);
		}
		this.m_LoadingLabel.Hide();
	}

	public void OnHeightmapSelectionChanged(UIComponent comp, int index)
	{
		if (index >= 0 && index < this.m_FileList.get_items().Length)
		{
			this.m_FileName.set_text(this.m_FileList.get_items()[index]);
		}
		else
		{
			this.m_FileName.set_text(string.Empty);
		}
	}

	private void Update()
	{
		if (this.m_LoadingLabel.get_isVisible())
		{
			this.m_LoadingProgress.Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
		}
	}

	private string GetExtension()
	{
		int selectedIndex = this.m_FormatDropdown.get_selectedIndex();
		if (selectedIndex == 0)
		{
			return ".png";
		}
		if (selectedIndex == 1)
		{
			return ".raw";
		}
		if (selectedIndex != 2)
		{
			return string.Empty;
		}
		return ".tiff";
	}

	private Image.BufferFileFormat GetExtensionFormat()
	{
		int selectedIndex = this.m_FormatDropdown.get_selectedIndex();
		if (selectedIndex == 0)
		{
			return 0;
		}
		if (selectedIndex == 1)
		{
			return 5;
		}
		if (selectedIndex != 2)
		{
			return -1;
		}
		return 6;
	}

	private TextureFormat GetExportFormat()
	{
		int selectedIndex = this.m_PrecisionDropdown.get_selectedIndex();
		if (selectedIndex == 0)
		{
			return Image.kFormatAlpha16;
		}
		if (selectedIndex != 1)
		{
			return Image.kFormatNone;
		}
		return 1;
	}

	private void SaveHeightmap(string filePath)
	{
		Image image = new Image(1081, 1081, Image.kFormatAlpha16, this.m_CurrentHeightmap);
		image.Convert(this.GetExportFormat());
		File.WriteAllBytes(filePath, image.GetFormattedImage(this.GetExtensionFormat()));
	}

	public void OnApply()
	{
		string path = base.VerifyExtension(this.m_FileName.get_text(), this.GetExtension());
		string finalPath = Path.Combine(HeightmapPanel.heightMapsPath, path);
		if (!File.Exists(finalPath))
		{
			this.SaveHeightmap(finalPath);
		}
		else
		{
			ConfirmPanel.ShowModal("CONFIRM_HEIGHTMAPOVERRIDE", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.SaveHeightmap(finalPath);
				}
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private static readonly string[] m_Extensions = Image.GetExtensions(164);

	public float m_RotationSpeed = 280f;

	private UIDropDown m_FormatDropdown;

	private UIDropDown m_PrecisionDropdown;

	private UITextField m_FileName;

	private UILabel m_LoadingLabel;

	private UIListBox m_FileList;

	private UIButton m_ExportButton;

	private UITextureSprite m_PreviewSprite;

	private Transform m_LoadingProgress;

	private byte[] m_CurrentHeightmap;

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[ExportHeightmapPanel.m_Extensions.Length];
}
