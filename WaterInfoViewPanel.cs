﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class WaterInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_WaterProduction = base.Find<UILabel>("WaterProduction");
		this.m_WaterConsumption = base.Find<UILabel>("WaterConsumption");
		this.m_SewageCapacity = base.Find<UILabel>("SewageCapacity");
		this.m_SewageAccumulation = base.Find<UILabel>("SewageAccumulation");
		this.m_WaterMeter = base.Find<UISlider>("WaterMeter");
		this.m_SewageMeter = base.Find<UISlider>("SewageMeter");
		this.m_TankMeter = base.Find<UIProgressBar>("TankMeter");
		this.m_PanelTankMeter = base.Find<UIPanel>("PanelTankMeter");
		this.m_PanelTankValues = base.Find<UIPanel>("PanelTankValues");
		this.m_WaterReserves = base.Find<UILabel>("TankValues");
	}

	private void SetSpriteColor(UISlicedSprite sprite, Color col)
	{
		col.a = sprite.get_opacity();
		sprite.set_color(col);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UISprite uISprite3 = base.Find<UISprite>("ColorActiveB");
				UISprite uISprite4 = base.Find<UISprite>("ColorInactiveB");
				UISprite uISprite5 = base.Find<UISprite>("ColorWaterAndSewage");
				UISprite uISprite6 = base.Find<UISprite>("ColorWaterOnly");
				UISprite uISprite7 = base.Find<UISprite>("ColorNoWaterAndSewage");
				UISlicedSprite sprite = base.Find<UISlicedSprite>("SewageSprite");
				UISlicedSprite sprite2 = base.Find<UISlicedSprite>("WaterSprite");
				UISlicedSprite sprite3 = base.Find<UISlicedSprite>("TankSprite");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_inactiveColor);
					uISprite3.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_activeColorB);
					uISprite4.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_inactiveColor);
					this.SetSpriteColor(sprite, uISprite3.get_color());
					this.SetSpriteColor(sprite2, uISprite.get_color());
					this.SetSpriteColor(sprite3, uISprite.get_color());
					uISprite5.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_targetColor);
					uISprite6.set_color(Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_negativeColor, Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_targetColor, 0.5f));
					uISprite7.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_negativeColor);
				}
			}
			this.m_initialized = true;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		if (Singleton<DistrictManager>.get_exists())
		{
			num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterCapacity();
			num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterConsumption();
			num3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetSewageCapacity();
			num4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetSewageAccumulation();
		}
		this.m_WaterMeter.set_value((float)base.GetPercentage(num, num2));
		this.m_SewageMeter.set_value((float)base.GetPercentage(num3, num4));
		this.m_WaterProduction.set_text(StringUtils.SafeFormat(Locale.Get("INFO_WATER_PRODUCTION"), num));
		this.m_WaterConsumption.set_text(StringUtils.SafeFormat(Locale.Get("INFO_WATER_CONSUMPTION"), num2));
		this.m_SewageCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_WATER_SEWAGECAPACITY"), num3));
		this.m_SewageAccumulation.set_text(StringUtils.SafeFormat(Locale.Get("INFO_WATER_SEWAGEACCUMULATION"), num4));
		int num5 = 0;
		int waterStorageAmount = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterStorageAmount();
		int waterStorageCapacity = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterStorageCapacity();
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				this.m_PanelTankMeter.set_isVisible(true);
				this.m_PanelTankValues.set_isVisible(true);
				this.m_WaterReserves.set_text(StringUtils.SafeFormat(Locale.Get("WATERINFOPANEL_TANK"), new object[]
				{
					waterStorageAmount,
					waterStorageCapacity
				}));
				if (waterStorageCapacity > 0)
				{
					num5 = Mathf.RoundToInt((float)waterStorageAmount / (float)waterStorageCapacity * 100f);
				}
				this.m_TankMeter.set_value((float)num5);
			}
		}
		else
		{
			this.m_PanelTankMeter.set_isVisible(false);
			this.m_PanelTankValues.set_isVisible(false);
		}
		this.RefreshHeight();
	}

	private void RefreshHeight()
	{
		UIPanel uIPanel = (UIPanel)base.get_component();
		uIPanel.set_autoLayoutDirection(1);
		uIPanel.set_autoLayout(true);
		uIPanel.set_autoLayout(false);
		uIPanel.FitChildrenVertically();
	}

	private UILabel m_WaterProduction;

	private UILabel m_WaterConsumption;

	private UILabel m_SewageCapacity;

	private UILabel m_SewageAccumulation;

	private UIPanel m_PanelTankMeter;

	private UIPanel m_PanelTankValues;

	private UILabel m_WaterReserves;

	private UISlider m_WaterMeter;

	private UISlider m_SewageMeter;

	private UIProgressBar m_TankMeter;

	private bool m_initialized;
}
