﻿using System;
using System.Collections.Generic;
using ColossalFramework.Globalization;

public class DistrictStyle
{
	public DistrictStyle(string name, bool builtIn)
	{
		this.m_BuiltIn = builtIn;
		this.m_Name = name;
		this.m_PackageName = name;
		this.m_Infos = new HashSet<BuildingInfo>();
		this.m_AffectedServices = new HashSet<int>();
	}

	public string FullName
	{
		get
		{
			return this.m_PackageName + "." + this.m_Name;
		}
	}

	public string Name
	{
		get
		{
			return this.m_Name;
		}
	}

	public string PackageName
	{
		get
		{
			return this.m_PackageName;
		}
		set
		{
			this.m_PackageName = value;
		}
	}

	public bool BuiltIn
	{
		get
		{
			return this.m_BuiltIn;
		}
	}

	public int Count
	{
		get
		{
			return this.m_Infos.Count;
		}
	}

	public void Add(BuildingInfo info)
	{
		this.m_Infos.Add(info);
		ItemClass.Level level = (!(info.m_class != null)) ? ItemClass.Level.Level1 : info.m_class.m_level;
		this.m_AffectedServices.Add(DistrictStyle.GetServiceLevelIndex(info.GetService(), info.GetSubService(), level));
	}

	public BuildingInfo[] GetBuildingInfos()
	{
		BuildingInfo[] array = new BuildingInfo[this.m_Infos.Count];
		this.m_Infos.CopyTo(array);
		return array;
	}

	public ushort[] GetBuildingIndices(BuildingInfo[] infos)
	{
		ushort[] array = new ushort[this.Count];
		int num = PrefabCollection<BuildingInfo>.PrefabCount();
		ushort num2 = 0;
		while ((int)num2 < num)
		{
			BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)num2);
			if (this.m_Infos.Contains(prefab))
			{
				for (int i = 0; i < infos.Length; i++)
				{
					if (infos[i] == prefab)
					{
						array[i] = num2;
					}
				}
			}
			num2 += 1;
		}
		return array;
	}

	public static string ServiceLevelToLocalizedString(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		string text = string.Empty;
		if (subService != ItemClass.SubService.None)
		{
			string text2;
			string text3;
			switch (subService)
			{
			case ItemClass.SubService.ResidentialLow:
				text2 = "ZONING_TITLE";
				text3 = "ResidentialLow";
				goto IL_1A2;
			case ItemClass.SubService.ResidentialHigh:
				text2 = "ZONING_TITLE";
				text3 = "ResidentialHigh";
				goto IL_1A2;
			case ItemClass.SubService.CommercialLow:
				text2 = "ZONING_TITLE";
				text3 = "CommercialLow";
				goto IL_1A2;
			case ItemClass.SubService.CommercialHigh:
				text2 = "ZONING_TITLE";
				text3 = "CommercialHigh";
				goto IL_1A2;
			case ItemClass.SubService.IndustrialGeneric:
				text2 = "ZONING_TITLE";
				text3 = "Industrial";
				goto IL_1A2;
			case ItemClass.SubService.IndustrialForestry:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationForest";
				goto IL_1A2;
			case ItemClass.SubService.IndustrialFarming:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationFarming";
				goto IL_1A2;
			case ItemClass.SubService.IndustrialOil:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationOil";
				goto IL_1A2;
			case ItemClass.SubService.IndustrialOre:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationOre";
				goto IL_1A2;
			case ItemClass.SubService.CommercialLeisure:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationLeisure";
				goto IL_1A2;
			case ItemClass.SubService.CommercialTourist:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationTourist";
				goto IL_1A2;
			case ItemClass.SubService.OfficeGeneric:
				text2 = "ZONING_TITLE";
				text3 = "Office";
				goto IL_1A2;
			case ItemClass.SubService.OfficeHightech:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationHightech";
				goto IL_1A2;
			case ItemClass.SubService.CommercialEco:
				text2 = "DISTRICT_TITLE";
				text3 = "SpecializationOrganic";
				goto IL_1A2;
			case ItemClass.SubService.ResidentialLowEco:
				text2 = "DISTRICT_TITLE";
				text3 = "ResidentialLowEco";
				goto IL_1A2;
			case ItemClass.SubService.ResidentialHighEco:
				text2 = "DISTRICT_TITLE";
				text3 = "ResidentialHighEco";
				goto IL_1A2;
			}
			text2 = string.Empty;
			text3 = string.Empty;
			IL_1A2:
			if (text2.Length > 0)
			{
				text += Locale.Get(text2, text3);
			}
		}
		string text4 = text;
		return string.Concat(new object[]
		{
			text4,
			" ",
			Locale.Get("CONTENTMANAGER_STYLELEVEL"),
			" ",
			(int)(level + 1)
		});
	}

	public static ItemClass.Service GetPrivateServiceByIndex(int serviceIndex)
	{
		switch (serviceIndex)
		{
		case 0:
			return ItemClass.Service.Residential;
		case 1:
			return ItemClass.Service.Commercial;
		case 2:
			return ItemClass.Service.Industrial;
		case 3:
			return ItemClass.Service.Natural;
		case 4:
			return ItemClass.Service.Unused2;
		case 5:
			return ItemClass.Service.Citizen;
		case 6:
			return ItemClass.Service.Tourism;
		case 7:
			return ItemClass.Service.Office;
		default:
			return ItemClass.Service.None;
		}
	}

	public static ItemClass.SubService GetPrivateSubServiceByIndex(int subServiceIndex)
	{
		switch (subServiceIndex)
		{
		case 0:
			return ItemClass.SubService.ResidentialLow;
		case 1:
			return ItemClass.SubService.ResidentialHigh;
		case 2:
			return ItemClass.SubService.CommercialLow;
		case 3:
			return ItemClass.SubService.CommercialHigh;
		case 4:
			return ItemClass.SubService.IndustrialGeneric;
		case 5:
			return ItemClass.SubService.IndustrialForestry;
		case 6:
			return ItemClass.SubService.IndustrialFarming;
		case 7:
			return ItemClass.SubService.IndustrialOil;
		case 8:
			return ItemClass.SubService.IndustrialOre;
		case 9:
			return ItemClass.SubService.CommercialLeisure;
		case 10:
			return ItemClass.SubService.CommercialTourist;
		case 11:
			return ItemClass.SubService.CommercialEco;
		case 12:
			return ItemClass.SubService.ResidentialLowEco;
		case 13:
			return ItemClass.SubService.ResidentialHighEco;
		case 14:
			return ItemClass.SubService.OfficeGeneric;
		case 15:
			return ItemClass.SubService.OfficeHightech;
		default:
			return ItemClass.SubService.None;
		}
	}

	public static void InterpretServiceLevelIndex(int index, out ItemClass.Service service, out ItemClass.SubService subService, out ItemClass.Level level)
	{
		level = (ItemClass.Level)(index % 5);
		index /= 5;
		subService = DistrictStyle.GetPrivateSubServiceByIndex(index % 16);
		index /= 8;
		service = DistrictStyle.GetPrivateServiceByIndex(index);
	}

	public static int GetServiceLevelIndex(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		int num = ItemClass.GetPrivateServiceIndex(service);
		num *= 16;
		num += ItemClass.GetPrivateSubServiceIndex(subService);
		num *= 5;
		return (int)(num + level);
	}

	public bool AffectsService(ItemClass.Service service, ItemClass.SubService subservice, ItemClass.Level level)
	{
		return this.m_AffectedServices.Contains(DistrictStyle.GetServiceLevelIndex(service, subservice, level));
	}

	public bool Contains(BuildingInfo info)
	{
		return this.m_Infos.Contains(info);
	}

	public static readonly string kEuropeanStyleName = "EuropeanBuiltinStyle_q3oji4g";

	public static readonly string kEuropeanSuburbiaStyleName = "EuropeanSuburbiaBuiltinStyle_kgz2hs9";

	private HashSet<BuildingInfo> m_Infos;

	private HashSet<int> m_AffectedServices;

	private string m_Name;

	private string m_PackageName;

	private bool m_BuiltIn;
}
