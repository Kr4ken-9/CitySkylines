﻿using System;
using UnityEngine;

[Serializable]
public class BuildingInfoGen : ScriptableObject
{
	public Vector2[] m_baseArea;

	public Vector2[] m_baseNormals;

	public Vector3 m_size;

	public Vector3 m_min;

	public Vector3 m_max;

	public int m_walkWidth;

	public int m_walkLength;

	public float[] m_walkLevel;

	public float[] m_heights;

	public float m_triangleArea;

	public float m_uvmapArea;

	[NonSerialized]
	public BuildingInfoBase m_buildingInfo;
}
