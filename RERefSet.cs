﻿using System;
using System.Reflection;
using System.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class RERefSet : UICustomControl
{
	public event RERefSet.ReferenceSelectedHandler EventReferenceSelected
	{
		add
		{
			RERefSet.ReferenceSelectedHandler referenceSelectedHandler = this.EventReferenceSelected;
			RERefSet.ReferenceSelectedHandler referenceSelectedHandler2;
			do
			{
				referenceSelectedHandler2 = referenceSelectedHandler;
				referenceSelectedHandler = Interlocked.CompareExchange<RERefSet.ReferenceSelectedHandler>(ref this.EventReferenceSelected, (RERefSet.ReferenceSelectedHandler)Delegate.Combine(referenceSelectedHandler2, value), referenceSelectedHandler);
			}
			while (referenceSelectedHandler != referenceSelectedHandler2);
		}
		remove
		{
			RERefSet.ReferenceSelectedHandler referenceSelectedHandler = this.EventReferenceSelected;
			RERefSet.ReferenceSelectedHandler referenceSelectedHandler2;
			do
			{
				referenceSelectedHandler2 = referenceSelectedHandler;
				referenceSelectedHandler = Interlocked.CompareExchange<RERefSet.ReferenceSelectedHandler>(ref this.EventReferenceSelected, (RERefSet.ReferenceSelectedHandler)Delegate.Remove(referenceSelectedHandler2, value), referenceSelectedHandler);
			}
			while (referenceSelectedHandler != referenceSelectedHandler2);
		}
	}

	private void OnEnable()
	{
		if (this.m_SelectButton != null)
		{
			this.m_SelectButton.add_eventClick(new MouseEventHandler(this.SelectReference));
		}
		if (this.m_DeteleButton != null)
		{
			this.m_DeteleButton.add_eventClick(new MouseEventHandler(this.OnDeleteReference));
		}
	}

	private void OnDisable()
	{
		if (this.m_SelectButton != null)
		{
			this.m_SelectButton.remove_eventClick(new MouseEventHandler(this.SelectReference));
		}
		if (this.m_DeteleButton != null)
		{
			this.m_DeteleButton.remove_eventClick(new MouseEventHandler(this.OnDeleteReference));
		}
	}

	private void OnDeleteReference(UIComponent comp, UIMouseEventParameter param)
	{
		if (this.m_Filter == AssetImporterAssetTemplate.Filter.LaneProps)
		{
			this.m_Target.GetType().GetField(RERefSet.kPropField).SetValue(this.m_Target, null);
			this.m_Target.GetType().GetField(RERefSet.kTreeField).SetValue(this.m_Target, null);
			this.m_Target.GetType().GetField(RERefSet.kFinalPropField).SetValue(this.m_Target, null);
			this.m_Target.GetType().GetField(RERefSet.kFinalTreeField).SetValue(this.m_Target, null);
		}
		else
		{
			this.m_Field.SetValue(this.m_Target, null);
		}
		this.RefreshText();
		if (this.EventReferenceSelected != null)
		{
			this.EventReferenceSelected();
		}
	}

	public void Initialize(string label, object target, FieldInfo field, AssetImporterAssetTemplate.Filter filter)
	{
		if (this.m_Label != null)
		{
			this.m_Label.set_text(label);
		}
		this.m_Target = target;
		this.m_Field = field;
		this.m_Filter = filter;
		this.RefreshText();
	}

	private void RefreshText()
	{
		if (this.m_Filter == AssetImporterAssetTemplate.Filter.LaneProps)
		{
			object value = this.m_Target.GetType().GetField(RERefSet.kPropField).GetValue(this.m_Target);
			if (value != null)
			{
				this.m_SelectButton.set_text(((Object)value).get_name());
				return;
			}
			object value2 = this.m_Target.GetType().GetField(RERefSet.kTreeField).GetValue(this.m_Target);
			if (value2 != null)
			{
				this.m_SelectButton.set_text(((Object)value2).get_name());
				return;
			}
			this.m_SelectButton.set_text("New prop");
		}
		else
		{
			object value3 = this.m_Field.GetValue(this.m_Target);
			if (value3 != null)
			{
				this.m_SelectButton.set_text(((Object)value3).get_name());
			}
			else
			{
				this.m_SelectButton.set_text("New object");
			}
		}
	}

	private void SelectReference(UIComponent c = null, UIMouseEventParameter e = null)
	{
		AssetImporterAssetTemplate refSelectPanel = this.GetRefSelectPanel();
		refSelectPanel.Reset();
		refSelectPanel.RefreshWithFilter(this.m_Filter);
		refSelectPanel.ReferenceCallback = new AssetImporterAssetTemplate.ReferenceCallbackDelegate(this.OnReferenceSelected);
		refSelectPanel.get_component().Show();
		refSelectPanel.get_component().BringToFront();
	}

	private void OnReferenceSelected(PrefabInfo info)
	{
		this.GetRefSelectPanel().get_component().Hide();
		if (info != null)
		{
			if (this.m_Filter == AssetImporterAssetTemplate.Filter.LaneProps)
			{
				if (info is PropInfo)
				{
					this.m_Target.GetType().GetField(RERefSet.kPropField).SetValue(this.m_Target, info);
					this.m_Target.GetType().GetField(RERefSet.kTreeField).SetValue(this.m_Target, null);
					this.m_Target.GetType().GetField(RERefSet.kFinalPropField).SetValue(this.m_Target, null);
					this.m_Target.GetType().GetField(RERefSet.kFinalTreeField).SetValue(this.m_Target, null);
				}
				else if (info is TreeInfo)
				{
					this.m_Target.GetType().GetField(RERefSet.kPropField).SetValue(this.m_Target, null);
					this.m_Target.GetType().GetField(RERefSet.kTreeField).SetValue(this.m_Target, info);
					this.m_Target.GetType().GetField(RERefSet.kFinalPropField).SetValue(this.m_Target, null);
					this.m_Target.GetType().GetField(RERefSet.kFinalTreeField).SetValue(this.m_Target, null);
				}
			}
			else
			{
				this.m_Field.SetValue(this.m_Target, info);
			}
			this.m_SelectButton.set_text(info.get_name());
			if (this.EventReferenceSelected != null)
			{
				this.EventReferenceSelected();
			}
		}
	}

	private AssetImporterAssetTemplate GetRefSelectPanel()
	{
		UIComponent uIComponent = UIView.Find("SelectReference");
		return (!(uIComponent != null)) ? null : uIComponent.GetComponent<AssetImporterAssetTemplate>();
	}

	public UILabel m_Label;

	public UIButton m_SelectButton;

	public UIButton m_DeteleButton;

	private FieldInfo m_Field;

	private object m_Target;

	private AssetImporterAssetTemplate.Filter m_Filter;

	private static readonly string kPropField = "m_prop";

	private static readonly string kTreeField = "m_tree";

	private static readonly string kFinalPropField = "m_finalProp";

	private static readonly string kFinalTreeField = "m_finalTree";

	public delegate void ReferenceSelectedHandler();
}
