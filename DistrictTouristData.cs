﻿using System;
using ColossalFramework.IO;

public struct DistrictTouristData
{
	public void Add(ref DistrictTouristData data)
	{
		this.m_tempCount += data.m_tempCount;
	}

	public void Update()
	{
		this.m_averageCount = (this.m_averageCount * 8u + this.m_finalCount + this.m_tempCount + 8u) / 10u;
		this.m_finalCount = this.m_tempCount;
	}

	public void Reset()
	{
		this.m_tempCount = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempCount);
		s.WriteUInt24(this.m_finalCount);
		s.WriteUInt24(this.m_averageCount);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempCount = s.ReadUInt24();
		this.m_finalCount = s.ReadUInt24();
		if (s.get_version() >= 122u)
		{
			this.m_averageCount = s.ReadUInt24();
		}
		else
		{
			this.m_averageCount = this.m_finalCount;
		}
	}

	public uint m_tempCount;

	public uint m_finalCount;

	public uint m_averageCount;
}
