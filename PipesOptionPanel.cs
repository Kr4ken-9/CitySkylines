﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class PipesOptionPanel : OptionPanelBase
{
	private NetTool.Mode[] modes
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
			{
				return this.m_WWModes;
			}
			return this.m_Modes;
		}
	}

	private void Awake()
	{
		base.HidePanel();
		NetTool netTool = ToolsModifierControl.GetTool<NetTool>();
		if (netTool != null)
		{
			UITabstrip strip = base.Find<UITabstrip>("ToolMode");
			if (strip != null)
			{
				strip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int index)
				{
					netTool.m_mode = this.modes[index];
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						if (strip.get_selectedIndex() == -1)
						{
							strip.set_selectedIndex(0);
						}
						netTool.m_mode = this.modes[strip.get_selectedIndex()];
						if (this.modes.Length > 1)
						{
							strip.Show();
						}
						else
						{
							strip.Hide();
						}
					}
					else
					{
						netTool.m_mode = NetTool.Mode.Straight;
					}
				});
			}
			UIMultiStateButton snap = base.Find<UIMultiStateButton>("SnappingToggle");
			if (snap != null)
			{
				snap.set_activeStateIndex((!SnapSettingsPanel.isVisible) ? 0 : 1);
				snap.add_eventActiveStateIndexChanged(delegate(UIComponent sender, int index)
				{
					SnapSettingsPanel.ToggleVisibility(index == 1);
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (!visible)
					{
						SnapSettingsPanel.ToggleVisibility(false);
					}
					else
					{
						SnapSettingsPanel.ToggleVisibility(snap.get_activeStateIndex() == 1);
					}
				});
			}
		}
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	public NetTool.Mode[] m_WWModes = new NetTool.Mode[]
	{
		NetTool.Mode.Straight,
		NetTool.Mode.Upgrade
	};

	public NetTool.Mode[] m_Modes = new NetTool.Mode[1];
}
