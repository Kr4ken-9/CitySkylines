﻿using System;
using ColossalFramework.Packaging;

public class UserAssetType : Package.AssetType
{
	public static readonly Package.AssetType MapMetaData = Package.AssetType.User;

	public static readonly Package.AssetType SaveGameMetaData = Package.AssetType.User + 1;

	public static readonly Package.AssetType SystemMapMetaData = Package.AssetType.User + 2;

	public static readonly Package.AssetType CustomAssetMetaData = Package.AssetType.User + 3;

	public static readonly Package.AssetType ColorCorrection = Package.AssetType.User + 4;

	public static readonly Package.AssetType DistrictStyleMetaData = Package.AssetType.User + 5;

	public static readonly Package.AssetType MapThemeMetaData = Package.AssetType.User + 6;

	public static readonly Package.AssetType MapThemeMapMetaData = Package.AssetType.User + 7;

	public static readonly Package.AssetType ScenarioMetaData = Package.AssetType.User + 8;
}
