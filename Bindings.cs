﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class Bindings : MonoBehaviour
{
	public string temperature
	{
		get
		{
			if (Singleton<WeatherManager>.get_exists())
			{
				this.m_Temperature.Check(Singleton<WeatherManager>.get_instance().m_currentTemperature);
			}
			return this.m_Temperature.result;
		}
	}

	public string population
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				this.m_Population.Check(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount);
			}
			return this.m_Population.result;
		}
	}

	public string populationDelta
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				this.m_PopulationDelta.Check(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.GetWeeklyDelta());
			}
			return this.m_PopulationDelta.result;
		}
	}

	public string cash
	{
		get
		{
			if (Singleton<EconomyManager>.get_exists())
			{
				this.m_Cash.Check(Singleton<EconomyManager>.get_instance().LastCashAmount, Settings.moneyFormatNoCents);
			}
			return this.m_Cash.result;
		}
	}

	public string cashDelta
	{
		get
		{
			if (Singleton<EconomyManager>.get_exists())
			{
				this.m_CashDelta.Check(Singleton<EconomyManager>.get_instance().LastCashDelta, Settings.moneyFormatNoCents);
			}
			return this.m_CashDelta.result;
		}
	}

	public string gameTime
	{
		get
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				this.m_GameTime.Check(Singleton<SimulationManager>.get_instance().m_currentGameTime);
			}
			return this.m_GameTime.result;
		}
	}

	public string editorTime
	{
		get
		{
			return "-";
		}
	}

	public int isSimulationPaused
	{
		get
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				return (!Singleton<SimulationManager>.get_instance().SimulationPaused && !Singleton<SimulationManager>.get_instance().ForcedSimulationPaused) ? 0 : 1;
			}
			return 1;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				bool paused = value == 1;
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().SimulationPaused = paused;
				});
			}
		}
	}

	public bool isTimeNotPaused
	{
		get
		{
			return this.isSimulationPaused == 0;
		}
	}

	public bool isTimePaused
	{
		get
		{
			return this.isSimulationPaused == 1;
		}
	}

	public Color32 timeColor
	{
		get
		{
			return (this.isSimulationPaused != 1) ? this.m_PlayingColor : this.m_PausedColor;
		}
	}

	public float timeOfDay
	{
		get
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				return (float)Singleton<SimulationManager>.get_instance().m_currentGameTime.TimeOfDay.TotalSeconds / 86400f;
			}
			return 1f;
		}
	}

	public int simulationSpeed
	{
		get
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				return Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed - 1;
			}
			return 0;
		}
		set
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = value + 1;
				});
			}
		}
	}

	public void OptionsMenuSelected()
	{
		UIView.get_library().ShowModal("PauseMenu");
	}

	public Color32 m_PausedColor = Color.get_red();

	public Color32 m_PlayingColor = Color.get_green();

	private UITemperaturWrapper m_Temperature = new UITemperaturWrapper(3.40282347E+38f);

	private UIPopulationWrapper m_Population = new UIPopulationWrapper(4294967295u);

	private UIPopulationDeltaWrapper m_PopulationDelta = new UIPopulationDeltaWrapper(2147483647);

	private UICurrencyWrapper m_Cash = new UICurrencyWrapper(0L);

	private UICurrencyDeltaWrapper m_CashDelta = new UICurrencyDeltaWrapper(9223372036854775807L);

	private UIDateTimeWrapper m_GameTime = new UIDateTimeWrapper(DateTime.MaxValue);
}
