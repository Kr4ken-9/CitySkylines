﻿using System;
using System.IO;
using UnityEngine;

public static class FileSystemUtils
{
	public static void CopyDirectoryFiltered(string source, string target, Func<string, bool> includeCallback, bool recursive)
	{
		if (Directory.Exists(target))
		{
			Directory.Delete(target, true);
		}
		Directory.CreateDirectory(target);
		string[] files = Directory.GetFiles(source);
		for (int i = 0; i < files.Length; i++)
		{
			string text = files[i];
			if (includeCallback(text.Replace("\\", "/")))
			{
				File.Copy(FileSystemUtils.NiceWinPath(text), FileSystemUtils.NiceWinPath(Path.Combine(target, Path.GetFileName(text))));
			}
		}
		if (!recursive)
		{
			return;
		}
		string[] directories = Directory.GetDirectories(source);
		for (int j = 0; j < directories.Length; j++)
		{
			string text2 = directories[j];
			if (includeCallback(text2.Replace("\\", "/")))
			{
				FileSystemUtils.CopyDirectoryFiltered(text2, Path.Combine(target, Path.GetFileName(text2)), includeCallback, recursive);
			}
		}
	}

	public static string NiceWinPath(string unityPath)
	{
		if (Application.get_platform() == 7 || Application.get_platform() == 2)
		{
			return unityPath.Replace("/", "\\");
		}
		return unityPath;
	}
}
