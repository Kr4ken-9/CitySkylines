﻿using System;
using UnityEngine;

public class LightRays01_anim : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
		LineRenderer component = base.GetComponent<LineRenderer>();
		int i = 0;
		while (i < this.lengthOfLineRenderer)
		{
			Vector3 vector;
			vector..ctor(Random.Range(this.rangeMin, this.rangeMax), Random.Range(this.rangeMin, this.rangeMax), (float)(i * 2));
			component.SetPosition(i, vector);
			i++;
			float num = Time.get_time() * this.textureSpeed;
			base.GetComponent<Renderer>().get_material().SetTextureOffset("_MainTex", new Vector2(num, 0f));
		}
	}

	public int lengthOfLineRenderer = 12;

	public float rangeMin = -5f;

	public float rangeMax = 5f;

	public float textureSpeed = 10f;
}
