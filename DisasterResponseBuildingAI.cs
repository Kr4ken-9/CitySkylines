﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class DisasterResponseBuildingAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Destruction)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Destruction;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
		Singleton<BuildingManager>.get_instance().m_buildingDestroyed.Disable();
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Collapsed, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Collapsed2, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Collapsed)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level, VehicleInfo.VehicleType.Car);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else if (material == TransferManager.TransferReason.Collapsed2)
		{
			VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level, VehicleInfo.VehicleType.Helicopter);
			if (randomVehicleInfo2 != null)
			{
				Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num2;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, data.m_position, material, true, false))
				{
					randomVehicleInfo2.m_vehicleAI.SetSource(num2, ref vehicles2.m_buffer[(int)num2], buildingID);
					randomVehicleInfo2.m_vehicleAI.StartTransfer(num2, ref vehicles2.m_buffer[(int)num2], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.Collapsed || material == TransferManager.TransferReason.Collapsed2)
		{
			amountDelta = Mathf.Max(amountDelta, 0);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Collapsed || material == TransferManager.TransferReason.Collapsed2)
		{
			amount = 0;
			max = 1000000;
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * 5000f * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempExport = 0;
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num, buildingData.m_position, this.m_noiseRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Collapsed, ref num2, ref num3, ref num4, ref num5);
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Collapsed2, ref num6, ref num7, ref num8, ref num9);
		int num10 = (finalProductionRate * this.m_vehicleCount + 99) / 100;
		int num11 = (finalProductionRate * this.m_helicopterCount + 99) / 100;
		if (num2 < num10)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = (num10 - num2) * 3 / num10;
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			offer.Amount = num10 - num2;
			offer.Active = true;
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Collapsed, offer);
		}
		if (num6 < num11)
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Priority = (num11 - num6) * 3 / num11;
			offer2.Building = buildingID;
			offer2.Position = buildingData.m_position;
			offer2.Amount = num11 - num6;
			offer2.Active = true;
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Collapsed2, offer2);
		}
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_VEHICLE_CAPACITY", new object[]
			{
				this.m_vehicleCount
			}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_HELICOPTER_CAPACITY", new object[]
			{
				this.m_helicopterCount
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num = (productionRate * this.m_vehicleCount + 99) / 100;
		int num2 = (productionRate * this.m_helicopterCount + 99) / 100;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Collapsed, ref num3, ref num4, ref num5, ref num6);
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Collapsed2, ref num7, ref num8, ref num9, ref num10);
		string str = LocaleFormatter.FormatGeneric("AIINFO_VEHICLES", new object[]
		{
			num3,
			num
		}) + Environment.NewLine;
		return str + LocaleFormatter.FormatGeneric("AIINFO_HELICOPTERS", new object[]
		{
			num7,
			num2
		});
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 5;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 12;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 9;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 4;

	[CustomizableProperty("Vehicle Count")]
	public int m_vehicleCount = 10;

	[CustomizableProperty("Helicopter Count")]
	public int m_helicopterCount = 3;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 100f;
}
