﻿using System;

public abstract class AssetImporterPanelBase : ToolsModifierControl
{
	internal AssetImporterWizard owner
	{
		get;
		set;
	}

	public int index
	{
		get
		{
			return base.get_component().get_zOrder();
		}
	}
}
