﻿using System;
using ColossalFramework;
using UnityEngine;

public class LoadingProfilerUI : MonoBehaviour
{
	private void OnGUI()
	{
		if (!this.m_CustomContent)
		{
			Rect rect;
			rect..ctor(150f, 250f, (float)Screen.get_width() - 300f, (float)Screen.get_height() - 500f);
			long num = Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.GetMaxTime();
			long maxTime = Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.GetMaxTime();
			long maxTime2 = Singleton<LoadingManager>.get_instance().m_loadingProfilerScenes.GetMaxTime();
			if (maxTime > num)
			{
				num = maxTime;
			}
			if (maxTime2 > num)
			{
				num = maxTime2;
			}
			Rect rect2;
			rect2..ctor(rect.get_x(), rect.get_y(), rect.get_width(), rect.get_height() / 3f);
			string text = Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.DrawGUI(rect2, num);
			Rect rect3;
			rect3..ctor(rect.get_x(), rect.get_y() + rect.get_height() / 3f, rect.get_width(), rect.get_height() / 3f);
			string text2 = Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.DrawGUI(rect3, num);
			Rect rect4;
			rect4..ctor(rect.get_x(), rect.get_y() + rect.get_height() * 2f / 3f, rect.get_width(), rect.get_height() / 3f);
			string text3 = Singleton<LoadingManager>.get_instance().m_loadingProfilerScenes.DrawGUI(rect4, num);
			if (text2 != null)
			{
				text = text2;
			}
			if (text3 != null)
			{
				text = text3;
			}
			if (text != null)
			{
				Vector3 mousePosition = Input.get_mousePosition();
				mousePosition.y = (float)Screen.get_height() - mousePosition.y;
				Rect rect5;
				rect5..ctor(mousePosition.x, mousePosition.y, 200f, 50f);
				GUI.set_color(Color.get_black());
				GUI.Label(rect5, text);
			}
		}
		else
		{
			Rect rect6;
			rect6..ctor(150f, 250f, (float)Screen.get_width() - 300f, (float)Screen.get_height() - 500f);
			long num2 = Singleton<LoadingManager>.get_instance().m_loadingProfilerCustomContent.GetMaxTime();
			long maxTime3 = Singleton<LoadingManager>.get_instance().m_loadingProfilerCustomAsset.GetMaxTime();
			if (maxTime3 > num2)
			{
				num2 = maxTime3;
			}
			Rect rect7;
			rect7..ctor(rect6.get_x(), rect6.get_y() - 2f, rect6.get_width(), rect6.get_height() / 3f);
			string text4 = Singleton<LoadingManager>.get_instance().m_loadingProfilerCustomContent.DrawGUI(rect7, num2);
			Rect rect8;
			rect8..ctor(rect6.get_x(), rect6.get_y() + rect6.get_height() / 3f + 2f, rect6.get_width(), rect6.get_height() / 3f);
			string text5 = Singleton<LoadingManager>.get_instance().m_loadingProfilerCustomAsset.DrawColoredGUI(rect8, num2);
			if (text5 != null)
			{
				text4 = text5;
			}
			if (text4 != null)
			{
				Vector3 mousePosition2 = Input.get_mousePosition();
				mousePosition2.y = (float)Screen.get_height() - mousePosition2.y;
				Rect rect9;
				rect9..ctor(mousePosition2.x, mousePosition2.y, 200f, 50f);
				GUI.set_color(Color.get_black());
				GUI.Label(rect9, text4);
			}
		}
	}

	public bool m_CustomContent;
}
