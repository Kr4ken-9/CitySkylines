﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class ShipDockAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
			int num2 = this.m_noiseAccumulation * num / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num2 * 1.25f);
		}
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int num = 0;
			if (this.m_noiseAccumulation != 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				int num2 = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0)
					{
						num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
						num2++;
					}
				}
				if (num2 != 0)
				{
					num /= num2;
				}
			}
			int num3 = 100 - (num - 100) * (num - 100) / 100;
			int num4 = this.m_noiseAccumulation * num3 / 100;
			return CommonBuildingAI.GetNoisePollutionColor((float)num4 * 1.25f);
		}
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		if (this.m_transportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry && !this.HasLaneConnection(segmentID, ref data))
		{
			Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
		}
	}

	public override void SegmentLoaded(ushort segmentID, ref NetSegment data)
	{
		base.SegmentLoaded(segmentID, ref data);
		if (this.m_transportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry && !this.HasLaneConnection(segmentID, ref data))
		{
			Singleton<NetManager>.get_instance().AddServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, true);
		}
	}

	public override void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
		if (this.m_transportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry && !this.HasLaneConnection(segmentID, ref data))
		{
			Singleton<NetManager>.get_instance().RemoveServiceSegment(segmentID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
			Singleton<TransportManager>.get_instance().SetPatchDirty(segmentID, this.m_transportInfo, false);
		}
		base.ReleaseSegment(segmentID, ref data);
	}

	private bool HasLaneConnection(ushort segmentID, ref NetSegment data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		return instance.m_nodes.m_buffer[(int)data.m_startNode].m_lane != 0u || instance.m_nodes.m_buffer[(int)data.m_endNode].m_lane != 0u;
	}

	public override void CreateNode(ushort nodeID, ref NetNode data)
	{
		base.CreateNode(nodeID, ref data);
		data.m_flags |= NetNode.Flags.DisableOnlyMiddle;
	}

	public override void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
		base.NodeLoaded(nodeID, ref data, version);
		data.m_flags |= NetNode.Flags.DisableOnlyMiddle;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + 1000f;
	}

	public override bool DisplayTempSegment()
	{
		return false;
	}

	public override ItemClass.CollisionType GetCollisionType()
	{
		return ItemClass.CollisionType.Terrain;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		float num = 0f;
		uint num2 = data.m_lanes;
		int num3 = 0;
		while (num3 < this.m_info.m_lanes.Length && num2 != 0u)
		{
			NetInfo.Lane lane = this.m_info.m_lanes[num3];
			if (lane.m_laneType == NetInfo.LaneType.Vehicle)
			{
				num += instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_length;
			}
			num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
			num3++;
		}
		int num4 = Mathf.RoundToInt(num) << 4;
		int num5 = 0;
		int num6 = 0;
		if (num4 != 0)
		{
			num5 = (int)((byte)Mathf.Min((int)(data.m_trafficBuffer * 100) / num4, 100));
			num6 = (int)((byte)Mathf.Min((int)(data.m_noiseBuffer * 250) / num4, 100));
		}
		data.m_trafficBuffer = 0;
		data.m_noiseBuffer = 0;
		if (num5 > (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Min((int)(data.m_trafficDensity + 5), num5);
		}
		else if (num5 < (int)data.m_trafficDensity)
		{
			data.m_trafficDensity = (byte)Mathf.Max((int)(data.m_trafficDensity - 5), num5);
		}
		if (num6 > (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Min((int)(data.m_noiseDensity + 2), num6);
		}
		else if (num6 < (int)data.m_noiseDensity)
		{
			data.m_noiseDensity = (byte)Mathf.Max((int)(data.m_noiseDensity - 2), num6);
		}
		int num7 = (int)(100 - (data.m_noiseDensity - 100) * (data.m_noiseDensity - 100) / 100);
		int num8 = this.m_noiseAccumulation * num7 / 100;
		if (num8 != 0)
		{
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			float num9 = Vector3.Distance(position, position2);
			int num10 = Mathf.FloorToInt(num9 / this.m_noiseRadius);
			for (int i = 0; i < num10; i++)
			{
				Vector3 position3 = Vector3.Lerp(position, position2, (float)(i + 1) / (float)(num10 + 1));
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num8, position3, this.m_noiseRadius);
			}
		}
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		int num = 0;
		if (this.m_noiseAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num2 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					num += (int)instance.m_segments.m_buffer[(int)segment].m_noiseDensity;
					num2++;
				}
			}
			if (num2 != 0)
			{
				num /= num2;
			}
		}
		int num3 = 100 - (num - 100) * (num - 100) / 100;
		int num4 = this.m_noiseAccumulation * num3 / 100;
		if (num4 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num4, data.m_position, this.m_noiseRadius);
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None)
		{
			if (this.m_transportInfo == null || this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ship)
			{
				Vector3 position = data.m_position;
				uint num = 0u;
				byte offset = 0;
				if ((data.m_flags & (NetNode.Flags.End | NetNode.Flags.ForbidLaneConnection)) == NetNode.Flags.End)
				{
					ShipDockAI.FindConnectionPath(ref position, out num, out offset);
				}
				if (num != data.m_lane)
				{
					if (data.m_lane != 0u)
					{
						this.RemoveLaneConnection(nodeID, ref data);
					}
					if (num != 0u)
					{
						this.AddLaneConnection(nodeID, ref data, num, offset);
					}
				}
			}
			else
			{
				Vector3 position2 = data.m_position;
				uint num2 = 0u;
				byte offset2 = 0;
				if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
				{
					ShipDockAI.FindFerryPath(ref position2, out num2, out offset2);
				}
				if (num2 != data.m_lane)
				{
					if (data.m_lane != 0u)
					{
						this.RemoveLaneConnection(nodeID, ref data);
					}
					if (num2 != 0u)
					{
						this.AddLaneConnection(nodeID, ref data, num2, offset2);
					}
				}
			}
		}
	}

	public static bool FindConnectionPath(ref Vector3 position, out uint laneID, out byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 vector = position;
		ushort num = 0;
		float num2 = 1E+10f;
		FastList<ushort> serviceSegments = Singleton<NetManager>.get_instance().GetServiceSegments(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip);
		for (int i = 0; i < serviceSegments.m_size; i++)
		{
			ushort num3 = serviceSegments.m_buffer[i];
			NetInfo info = instance.m_segments.m_buffer[(int)num3].Info;
			if (info.m_class.m_layer == ItemClass.Layer.ShipPaths)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)num3].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)num3].m_endNode;
				Segment3 segment;
				segment.a = instance.m_nodes.m_buffer[(int)startNode].m_position;
				segment.b = instance.m_nodes.m_buffer[(int)endNode].m_position;
				float num4 = segment.DistanceSqr(vector);
				if (num4 < num2)
				{
					num = num3;
					num2 = num4;
				}
			}
		}
		laneID = 0u;
		offset = 0;
		Vector3 vector2;
		int num5;
		float num6;
		if (num != 0 && instance.m_segments.m_buffer[(int)num].GetClosestLanePosition(vector, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Ship, out vector2, out laneID, out num5, out num6))
		{
			offset = (byte)Mathf.Clamp(Mathf.RoundToInt(num6 * 255f), 0, 255);
			position = vector2;
		}
		return laneID != 0u;
	}

	public static bool FindFerryPath(ref Vector3 position, out uint laneID, out byte offset)
	{
		PathUnit.Position pathPos;
		PathUnit.Position position2;
		float num;
		float num2;
		if (PathManager.FindPathPosition(position, ItemClass.Service.Beautification, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Ferry, false, true, 32f, out pathPos, out position2, out num, out num2))
		{
			laneID = PathManager.GetLaneID(pathPos);
			offset = pathPos.m_offset;
			return true;
		}
		laneID = 0u;
		offset = 0;
		return false;
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if (this.m_transportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if (info != null && info.m_netAI is ShipDockAI && !this.HasLaneConnection(segment, ref instance.m_segments.m_buffer[(int)segment]))
					{
						instance.RemoveServiceSegment(segment, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
						Singleton<TransportManager>.get_instance().SetPatchDirty(segment, this.m_transportInfo, false);
					}
				}
			}
		}
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
		if (this.m_transportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if (info != null && info.m_netAI is ShipDockAI && !this.HasLaneConnection(segment, ref instance.m_segments.m_buffer[(int)segment]))
					{
						instance.AddServiceSegment(segment, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
						Singleton<TransportManager>.get_instance().SetPatchDirty(segment, this.m_transportInfo, true);
					}
				}
			}
		}
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public int m_noiseAccumulation = 10;

	public float m_noiseRadius = 40f;

	public TransportInfo m_transportInfo;
}
