﻿using System;
using System.Collections.Generic;
using ColossalFramework.UI;
using UnityEngine;

public class UITemplateList<T> where T : MonoBehaviour
{
	public UITemplateList(UIComponent container, string templateName)
	{
		this.m_templateName = templateName;
		this.m_container = container;
		if (container == null)
		{
			Debug.LogError("Created UITemplateList with null container.");
		}
		this.m_items = new List<T>();
	}

	public List<T> items
	{
		get
		{
			return this.m_items;
		}
	}

	public T[] SetItemCount(int count)
	{
		while (this.m_items.Count < count)
		{
			this.AddItem();
		}
		while (this.m_items.Count > 0 && this.m_items.Count > count)
		{
			this.RemoveItem(this.m_items.Count - 1);
		}
		return this.m_items.ToArray();
	}

	public T AddItem()
	{
		T t = UITemplateManager.Get<T>(this.m_templateName);
		this.m_container.AttachUIComponent(t.get_gameObject());
		this.m_items.Add(t);
		return t;
	}

	public void RemoveItem(int index)
	{
		T t = this.m_items[index];
		UIComponent component = t.get_gameObject().GetComponent<UIComponent>();
		if (component != null)
		{
			this.m_container.RemoveUIComponent(component);
			component.set_isVisible(false);
		}
		else
		{
			Debug.LogError("Could not find UIComponent on gameObject " + t.get_gameObject().get_name());
		}
		Object.Destroy(t.get_gameObject(), 2f);
		this.m_items.RemoveAt(index);
	}

	public void RemoveItem(T item)
	{
		int num = this.m_items.IndexOf(item);
		if (num == -1)
		{
			Debug.LogError("Could not delete item as it was not found in list.");
		}
		else
		{
			this.RemoveItem(num);
		}
	}

	public void Clear()
	{
		for (int i = this.m_items.Count - 1; i >= 0; i--)
		{
			this.RemoveItem(i);
		}
	}

	private string m_templateName;

	private List<T> m_items;

	private UIComponent m_container;
}
