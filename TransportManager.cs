﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class TransportManager : SimulationManagerBase<TransportManager, TransportProperties>, ISimulationManager, IRenderableManager
{
	public int LinesVisible
	{
		get
		{
			return this.m_linesVisible;
		}
		set
		{
			if (this.m_linesVisible != value)
			{
				this.m_linesVisible = value;
				this.UpdateVisibility();
			}
		}
	}

	public bool TunnelsVisible
	{
		get
		{
			return this.m_tunnelsVisible;
		}
		set
		{
			if (this.m_tunnelsVisible != value)
			{
				this.m_tunnelsVisible = value;
				this.UpdateVisibility();
			}
		}
	}

	public bool TunnelsVisibleInfo
	{
		get
		{
			return this.m_tunnelsVisibleInfo;
		}
		set
		{
			if (this.m_tunnelsVisibleInfo != value)
			{
				this.m_tunnelsVisibleInfo = value;
				this.UpdateVisibility();
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_lines = new Array16<TransportLine>(256u);
		this.m_lineMeshes = new Mesh[256][];
		this.m_lineMeshData = new RenderGroup.MeshData[256][];
		this.m_lineSegments = new TransportManager.LineSegment[256][];
		this.m_lineCurves = new Bezier3[256][];
		this.m_updatedLines = new ulong[4];
		this.m_metroLayer = LayerMask.NameToLayer("MetroTunnels");
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_StartOffset = Shader.PropertyToID("_StartOffset");
		this.m_patches = new TransportPatch[81];
		this.m_passengers = new TransportPassengerData[10];
		this.m_lineNumber = new ushort[10];
		this.m_transportTypeLoaded = new TransportInfo[10];
		ushort num;
		this.m_lines.CreateItem(out num);
	}

	private void OnDestroy()
	{
		if (this.m_patches != null)
		{
			int num = this.m_patches.Length;
			for (int i = 0; i < num; i++)
			{
				TransportPatch transportPatch = this.m_patches[i];
				int num2 = 0;
				while (transportPatch != null)
				{
					transportPatch.Release();
					transportPatch = transportPatch.m_nextPatch;
					if (++num2 >= 100)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				this.m_patches[i] = null;
			}
		}
	}

	public override void InitializeProperties(TransportProperties properties)
	{
		base.InitializeProperties(properties);
		GameObject gameObject = GameObject.FindGameObjectWithTag("UndergroundView");
		if (gameObject != null)
		{
			this.m_undergroundCamera = gameObject.GetComponent<Camera>();
		}
		int num = PrefabCollection<TransportInfo>.LoadedCount();
		for (int i = 0; i < num; i++)
		{
			TransportInfo loaded = PrefabCollection<TransportInfo>.GetLoaded((uint)i);
			if (!(loaded == null))
			{
				this.m_transportTypeLoaded[(int)loaded.m_transportType] = loaded;
			}
		}
	}

	public override void DestroyProperties(TransportProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_undergroundCamera = null;
			int num = this.m_patches.Length;
			for (int i = 0; i < num; i++)
			{
				TransportPatch transportPatch = this.m_patches[i];
				int num2 = 0;
				while (transportPatch != null)
				{
					transportPatch.Release();
					transportPatch = transportPatch.m_nextPatch;
					if (++num2 >= 100)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				this.m_patches[i] = null;
			}
			for (int j = 0; j < 10; j++)
			{
				this.m_transportTypeLoaded[j] = null;
			}
		}
		base.DestroyProperties(properties);
	}

	private void UpdateVisibility()
	{
		if (this.m_undergroundCamera != null)
		{
			if (this.m_linesVisible != 0 || this.m_tunnelsVisible || this.m_tunnelsVisibleInfo)
			{
				Camera expr_38 = this.m_undergroundCamera;
				expr_38.set_cullingMask(expr_38.get_cullingMask() | 1 << this.m_metroLayer);
			}
			else
			{
				Camera expr_5A = this.m_undergroundCamera;
				expr_5A.set_cullingMask(expr_5A.get_cullingMask() & ~(1 << this.m_metroLayer));
			}
		}
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_linesVisible != 0 && cameraInfo.m_camera != null)
		{
			int num = this.m_patches.Length;
			for (int i = 0; i < num; i++)
			{
				TransportPatch transportPatch = this.m_patches[i];
				int num2 = 0;
				while (transportPatch != null)
				{
					transportPatch.RenderOverlay(cameraInfo, this.m_linesVisible);
					transportPatch = transportPatch.m_nextPatch;
					if (++num2 >= 100)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (this.m_linesVisible != 0 && cameraInfo.m_camera != null)
		{
			this.RenderLines(cameraInfo, cameraInfo.m_camera.get_cullingMask(), this.m_linesVisible);
		}
	}

	protected override void UndergroundOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_linesVisible != 0 && this.m_undergroundCamera != null)
		{
			this.RenderLines(cameraInfo, this.m_undergroundCamera.get_cullingMask(), this.m_linesVisible);
		}
	}

	private void RenderLines(RenderManager.CameraInfo cameraInfo, int layerMask, int typeMask)
	{
		bool flag = false;
		for (int i = 0; i < 256; i++)
		{
			if (this.m_lines.m_buffer[i].m_flags != TransportLine.Flags.None)
			{
				if ((this.m_lines.m_buffer[i].m_flags & (TransportLine.Flags.Hidden | TransportLine.Flags.Highlighted)) != TransportLine.Flags.Hidden)
				{
					if (this.m_lineMeshData[i] != null)
					{
						this.UpdateMesh((ushort)i);
					}
					if ((this.m_lines.m_buffer[i].m_flags & (TransportLine.Flags.Temporary | TransportLine.Flags.Selected | TransportLine.Flags.Highlighted)) != TransportLine.Flags.None)
					{
						flag = true;
					}
					else
					{
						this.m_lines.m_buffer[i].RenderLine(cameraInfo, layerMask, typeMask, (ushort)i);
					}
				}
			}
			else if (this.m_lineMeshes[i] != null)
			{
				int num = this.m_lineMeshes[i].Length;
				for (int j = 0; j < num; j++)
				{
					Object.Destroy(this.m_lineMeshes[i][j]);
				}
				this.m_lineMeshes[i] = null;
			}
		}
		if (flag)
		{
			for (int k = 0; k < 256; k++)
			{
				if (this.m_lines.m_buffer[k].m_flags != TransportLine.Flags.None && (this.m_lines.m_buffer[k].m_flags & (TransportLine.Flags.Hidden | TransportLine.Flags.Highlighted)) != TransportLine.Flags.Hidden && (this.m_lines.m_buffer[k].m_flags & (TransportLine.Flags.Temporary | TransportLine.Flags.Selected | TransportLine.Flags.Highlighted)) != TransportLine.Flags.None)
				{
					this.m_lines.m_buffer[k].RenderLine(cameraInfo, layerMask, typeMask, (ushort)k);
				}
			}
		}
	}

	private void UpdateMesh(ushort lineID)
	{
		while (!Monitor.TryEnter(this.m_lineMeshData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		RenderGroup.MeshData[] array;
		try
		{
			array = this.m_lineMeshData[(int)lineID];
			this.m_lineMeshData[(int)lineID] = null;
		}
		finally
		{
			Monitor.Exit(this.m_lineMeshData);
		}
		if (array != null)
		{
			Mesh[] array2 = this.m_lineMeshes[(int)lineID];
			int num = 0;
			if (array2 != null)
			{
				num = array2.Length;
			}
			if (num != array.Length)
			{
				Mesh[] array3 = new Mesh[array.Length];
				int num2 = Mathf.Min(num, array3.Length);
				for (int i = 0; i < num2; i++)
				{
					array3[i] = array2[i];
				}
				for (int j = num2; j < array3.Length; j++)
				{
					array3[j] = new Mesh();
				}
				for (int k = num2; k < num; k++)
				{
					Object.Destroy(array2[k]);
				}
				array2 = array3;
				this.m_lineMeshes[(int)lineID] = array2;
			}
			for (int l = 0; l < array.Length; l++)
			{
				array2[l].Clear();
				array2[l].set_vertices(array[l].m_vertices);
				array2[l].set_normals(array[l].m_normals);
				array2[l].set_tangents(array[l].m_tangents);
				array2[l].set_uv(array[l].m_uvs);
				array2[l].set_uv2(array[l].m_uvs2);
				array2[l].set_colors32(array[l].m_colors);
				array2[l].set_triangles(array[l].m_triangles);
				array2[l].set_bounds(array[l].m_bounds);
			}
		}
	}

	public bool CheckLimits()
	{
		return this.m_lineCount < 251;
	}

	public bool CreateLine(out ushort lineID, ref Randomizer randomizer, TransportInfo info, bool newNumber)
	{
		if (this.m_lines.CreateItem(out lineID, ref randomizer))
		{
			this.m_lines.m_buffer[(int)lineID].m_flags = TransportLine.Flags.Created;
			this.m_lines.m_buffer[(int)lineID].Info = info;
			this.m_lines.m_buffer[(int)lineID].m_vehicles = 0;
			this.m_lines.m_buffer[(int)lineID].m_building = 0;
			this.m_lines.m_buffer[(int)lineID].m_budget = 100;
			this.m_lines.m_buffer[(int)lineID].m_totalLength = 0f;
			this.m_lines.m_buffer[(int)lineID].m_averageInterval = 0;
			this.m_lines.m_buffer[(int)lineID].m_bounds = default(Bounds);
			this.m_lines.m_buffer[(int)lineID].m_color = default(Color32);
			this.m_lines.m_buffer[(int)lineID].m_passengers = default(TransportPassengerData);
			if (newNumber)
			{
				TransportLine[] arg_152_0_cp_0 = this.m_lines.m_buffer;
				int arg_152_0_cp_1 = (int)lineID;
				ushort[] expr_149_cp_0 = this.m_lineNumber;
				TransportInfo.TransportType expr_149_cp_1 = info.m_transportType;
				arg_152_0_cp_0[arg_152_0_cp_1].m_lineNumber = (expr_149_cp_0[(int)expr_149_cp_1] = expr_149_cp_0[(int)expr_149_cp_1] + 1);
			}
			else
			{
				this.m_lines.m_buffer[(int)lineID].m_lineNumber = 0;
			}
			this.m_lineCount = (int)(this.m_lines.ItemCount() - 1u);
			if (this.m_lineCount == 20 && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (!PlatformService.get_achievements().get_Item("CityInMotion").get_achieved())
					{
						PlatformService.get_achievements().get_Item("CityInMotion").Unlock();
					}
				});
			}
			if (this.m_lineCount == 50 && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
			{
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (!PlatformService.get_achievements().get_Item("CityInMotion2").get_achieved())
					{
						PlatformService.get_achievements().get_Item("CityInMotion2").Unlock();
					}
				});
			}
			VehicleInfo.VehicleType vehicleType = info.m_vehicleType;
			if (vehicleType != VehicleInfo.VehicleType.Monorail)
			{
				if (vehicleType != VehicleInfo.VehicleType.Ferry)
				{
					if (vehicleType == VehicleInfo.VehicleType.Blimp)
					{
						this.m_blimpLine.Disable();
					}
				}
				else
				{
					this.m_ferryLine.Disable();
				}
			}
			else
			{
				this.m_monorailLine.Disable();
			}
			return true;
		}
		lineID = 0;
		return false;
	}

	public void ReleaseLine(ushort lineID)
	{
		this.ReleaseLineImplementation(lineID, ref this.m_lines.m_buffer[(int)lineID]);
	}

	private void ReleaseLineImplementation(ushort lineID, ref TransportLine data)
	{
		if (data.m_flags == TransportLine.Flags.None)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_lineMeshData, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_lineMeshData[(int)lineID] = null;
		}
		finally
		{
			Monitor.Exit(this.m_lineMeshData);
		}
		TransportInfo info = data.Info;
		InstanceID id = default(InstanceID);
		id.TransportLine = lineID;
		Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
		if (info != null)
		{
			if (data.m_lineNumber != 0 && data.m_lineNumber == this.m_lineNumber[(int)info.m_transportType])
			{
				data.m_lineNumber = 0;
				ushort num = 0;
				for (int i = 1; i < 256; i++)
				{
					if (this.m_lines.m_buffer[i].m_flags != TransportLine.Flags.None && this.m_lines.m_buffer[i].Info.m_transportType == info.m_transportType && this.m_lines.m_buffer[i].m_lineNumber > num)
					{
						num = this.m_lines.m_buffer[i].m_lineNumber;
					}
				}
				this.m_lineNumber[(int)info.m_transportType] = num;
			}
			TransferManager.TransferReason vehicleReason = info.m_vehicleReason;
			if (vehicleReason != TransferManager.TransferReason.None)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.TransportLine = lineID;
				Singleton<TransferManager>.get_instance().RemoveIncomingOffer(info.m_vehicleReason, offer);
			}
		}
		if (data.m_vehicles != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num2 = data.m_vehicles;
			int num3 = 0;
			while (num2 != 0)
			{
				ushort nextLineVehicle = instance.m_vehicles.m_buffer[(int)num2].m_nextLineVehicle;
				VehicleInfo info2 = instance.m_vehicles.m_buffer[(int)num2].Info;
				info2.m_vehicleAI.SetTransportLine(num2, ref instance.m_vehicles.m_buffer[(int)num2], 0);
				num2 = nextLineVehicle;
				if (++num3 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if (data.m_stops != 0)
		{
			NetManager instance2 = Singleton<NetManager>.get_instance();
			ushort num4 = data.m_stops;
			ushort num5 = 0;
			if (num4 != 0 && data.Complete)
			{
				num5 = TransportLine.GetPrevStop(num4);
			}
			int num6 = 0;
			while (num4 != 0 && (instance2.m_nodes.m_buffer[(int)num4].m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) == NetNode.Flags.Created && instance2.m_nodes.m_buffer[(int)num4].m_transportLine == lineID)
			{
				ushort nextStop = TransportLine.GetNextStop(num4);
				if (nextStop != 0 && num5 != 0)
				{
					Vector3 position = instance2.m_nodes.m_buffer[(int)num4].m_position;
					data.AddWaveEvent(position, position, -2, 0);
				}
				else if (num5 != 0 || nextStop != 0)
				{
					Vector3 position2 = instance2.m_nodes.m_buffer[(int)num4].m_position;
					data.AddWaveEvent(position2, position2, -1, 0);
				}
				data.ReleaseNode(num4);
				num5 = num4;
				num4 = nextStop;
				if (++num6 >= 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_stops = 0;
		}
		data.m_flags &= ~TransportLine.Flags.Complete;
		data.CheckCompletionMilestone();
		data.m_flags = TransportLine.Flags.None;
		this.m_lines.ReleaseItem(lineID);
		this.m_lineCount = (int)(this.m_lines.ItemCount() - 1u);
	}

	public void SetPatchDirty(ushort segmentID, TransportInfo info, bool canCreate)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort startNode = instance.m_segments.m_buffer[(int)segmentID].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)segmentID].m_endNode;
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((vector.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((vector.z + num * 0.5f) / num2), 0, 8);
		this.SetPatchDirty(num3, num4, info, canCreate);
		if (info.m_usePathNodes)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)startNode].GetSegment(i);
				if (segment != 0 && segment != segmentID)
				{
					ushort startNode2 = instance.m_segments.m_buffer[(int)segment].m_startNode;
					ushort endNode2 = instance.m_segments.m_buffer[(int)segment].m_endNode;
					Vector3 position3 = instance.m_nodes.m_buffer[(int)startNode2].m_position;
					Vector3 position4 = instance.m_nodes.m_buffer[(int)endNode2].m_position;
					Vector3 vector2 = (position3 + position4) * 0.5f;
					int num5 = Mathf.Clamp((int)((vector2.x + num * 0.5f) / num2), 0, 8);
					int num6 = Mathf.Clamp((int)((vector2.z + num * 0.5f) / num2), 0, 8);
					if (num5 != num3 || num6 != num4)
					{
						this.SetPatchDirty(num5, num6, info, canCreate);
					}
				}
			}
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)endNode].GetSegment(j);
				if (segment2 != 0 && segment2 != segmentID)
				{
					ushort startNode3 = instance.m_segments.m_buffer[(int)segment2].m_startNode;
					ushort endNode3 = instance.m_segments.m_buffer[(int)segment2].m_endNode;
					Vector3 position5 = instance.m_nodes.m_buffer[(int)startNode3].m_position;
					Vector3 position6 = instance.m_nodes.m_buffer[(int)endNode3].m_position;
					Vector3 vector3 = (position5 + position6) * 0.5f;
					int num7 = Mathf.Clamp((int)((vector3.x + num * 0.5f) / num2), 0, 8);
					int num8 = Mathf.Clamp((int)((vector3.z + num * 0.5f) / num2), 0, 8);
					if (num7 != num3 || num8 != num4)
					{
						this.SetPatchDirty(num7, num8, info, canCreate);
					}
				}
			}
		}
	}

	public void SetPatchDirty(int x, int z, TransportInfo info, bool canCreate)
	{
		int num = z * 9 + x;
		TransportPatch transportPatch = null;
		TransportPatch transportPatch2 = this.m_patches[num];
		int num2 = 0;
		while (transportPatch2 != null)
		{
			if (transportPatch2.m_info == info)
			{
				transportPatch2.m_isDirty = true;
				this.m_patchesDirty = true;
				return;
			}
			transportPatch = transportPatch2;
			transportPatch2 = transportPatch2.m_nextPatch;
			if (++num2 >= 100)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (canCreate && (Singleton<ToolManager>.get_instance().m_properties.m_mode & info.m_pathVisibility) != ItemClass.Availability.None)
		{
			transportPatch2 = new TransportPatch(x, z, info);
			transportPatch2.m_isDirty = true;
			this.m_patchesDirty = true;
			if (transportPatch != null)
			{
				transportPatch.m_nextPatch = transportPatch2;
			}
			else
			{
				this.m_patches[num] = transportPatch2;
			}
		}
	}

	public void UpdateLine(ushort lineID)
	{
		this.m_updatedLines[lineID >> 6] |= 1uL << (int)lineID;
		this.m_linesUpdated = true;
	}

	public bool RayCast(Ray ray, float rayLength, int transportTypes, out Vector3 hit, out ushort lineIndex, out int stopIndex, out int segmentIndex)
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		float num5 = 16f;
		float num6 = 9f;
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		Vector3 origin = ray.get_origin();
		Vector3 normalized = ray.get_direction().get_normalized();
		Vector3 vector3 = ray.get_origin() + normalized * rayLength;
		Segment3 segment;
		segment..ctor(origin, vector3);
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 1; i < 256; i++)
		{
			TransportLine.Flags flags = this.m_lines.m_buffer[i].m_flags;
			if ((flags & (TransportLine.Flags.Created | TransportLine.Flags.Temporary)) == TransportLine.Flags.Created && (flags & (TransportLine.Flags.Hidden | TransportLine.Flags.Selected)) != TransportLine.Flags.Hidden)
			{
				TransportInfo info = this.m_lines.m_buffer[i].Info;
				if ((transportTypes & 1 << (int)info.m_transportType) != 0 && this.m_lines.m_buffer[i].m_bounds.IntersectRay(ray))
				{
					TransportManager.LineSegment[] array = this.m_lineSegments[i];
					Bezier3[] array2 = this.m_lineCurves[i];
					ushort stops = this.m_lines.m_buffer[i].m_stops;
					ushort num7 = stops;
					int num8 = 0;
					while (num7 != 0)
					{
						Vector3 position = instance.m_nodes.m_buffer[(int)num7].m_position;
						float num9 = Line3.DistanceSqr(ray.get_direction(), ray.get_origin() - position);
						if (num9 < num5)
						{
							num = i;
							num3 = num8;
							num5 = num9;
							vector = position;
						}
						if (array.Length > num8 && array[num8].m_bounds.IntersectRay(ray))
						{
							int curveStart = array[num8].m_curveStart;
							int curveEnd = array[num8].m_curveEnd;
							for (int j = curveStart; j < curveEnd; j++)
							{
								Vector3 vector4 = array2[j].Min() - new Vector3(3f, 3f, 3f);
								Vector3 vector5 = array2[j].Max() + new Vector3(3f, 3f, 3f);
								Bounds bounds = default(Bounds);
								bounds.SetMinMax(vector4, vector5);
								if (bounds.IntersectRay(ray))
								{
									float num10;
									float num11;
									num9 = array2[j].DistanceSqr(segment, ref num10, ref num11);
									if (num9 < num6)
									{
										num2 = i;
										num4 = num8;
										num6 = num9;
										vector2 = array2[j].Position(num10);
									}
								}
							}
						}
						num7 = TransportLine.GetNextStop(num7);
						if (num7 == stops)
						{
							break;
						}
						if (++num8 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
		if (num != 0)
		{
			hit = vector;
			lineIndex = (ushort)num;
			stopIndex = num3;
			segmentIndex = -1;
			return true;
		}
		if (num2 != 0)
		{
			hit = vector2;
			lineIndex = (ushort)num2;
			stopIndex = -1;
			segmentIndex = num4;
			return true;
		}
		hit = Vector3.get_zero();
		lineIndex = 0;
		stopIndex = -1;
		segmentIndex = -1;
		return false;
	}

	public void UpdateLinesNow()
	{
		if (this.m_linesUpdated)
		{
			this.m_linesUpdated = false;
			int num = this.m_updatedLines.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedLines[i];
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							ushort num3 = (ushort)(i << 6 | j);
							if (this.m_lines.m_buffer[(int)num3].m_flags != TransportLine.Flags.None)
							{
								if (this.m_lines.m_buffer[(int)num3].UpdatePaths(num3) && this.m_lines.m_buffer[(int)num3].UpdateMeshData(num3))
								{
									num2 &= ~(1uL << j);
								}
							}
							else
							{
								num2 &= ~(1uL << j);
							}
						}
					}
					this.m_updatedLines[i] = num2;
					if (num2 != 0uL)
					{
						this.m_linesUpdated = true;
					}
				}
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		this.UpdateLinesNow();
		if (this.m_patchesDirty)
		{
			this.m_patchesDirty = false;
			int num = this.m_patches.Length;
			for (int i = 0; i < num; i++)
			{
				TransportPatch transportPatch = this.m_patches[i];
				int num2 = 0;
				while (transportPatch != null)
				{
					if (transportPatch.m_isDirty)
					{
						transportPatch.UpdateMeshData();
					}
					transportPatch = transportPatch.m_nextPatch;
					if (++num2 >= 100)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (subStep != 0)
		{
			int num3 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num4 = num3;
			int num5 = num3 + 1 - 1;
			for (int j = num4; j <= num5; j++)
			{
				TransportLine.Flags flags = this.m_lines.m_buffer[j].m_flags;
				if ((flags & (TransportLine.Flags.Created | TransportLine.Flags.Temporary)) == TransportLine.Flags.Created)
				{
					this.m_lines.m_buffer[j].SimulationStep((ushort)j);
				}
			}
			if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) == 0u)
			{
				StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
				StatisticBase statisticBase = instance.Acquire<StatisticArray>(StatisticType.AveragePassengers);
				for (int k = 0; k < 10; k++)
				{
					this.m_passengers[k].Update();
					this.m_passengers[k].Reset();
					statisticBase.Acquire<StatisticInt32>(k, 10).Set((int)(this.m_passengers[k].m_residentPassengers.m_averageCount + this.m_passengers[k].m_touristPassengers.m_averageCount));
				}
			}
		}
		if (subStep <= 1)
		{
			int num6 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num7 = num6 * PrefabCollection<TransportInfo>.PrefabCount() >> 10;
			int num8 = ((num6 + 1) * PrefabCollection<TransportInfo>.PrefabCount() >> 10) - 1;
			for (int l = num7; l <= num8; l++)
			{
				TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab((uint)l);
				if (prefab != null)
				{
					MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
					if (unlockMilestone != null)
					{
						Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
					}
				}
			}
		}
	}

	public event TransportManager.LineColorChangedHandler eventLineColorChanged
	{
		add
		{
			TransportManager.LineColorChangedHandler lineColorChangedHandler = this.eventLineColorChanged;
			TransportManager.LineColorChangedHandler lineColorChangedHandler2;
			do
			{
				lineColorChangedHandler2 = lineColorChangedHandler;
				lineColorChangedHandler = Interlocked.CompareExchange<TransportManager.LineColorChangedHandler>(ref this.eventLineColorChanged, (TransportManager.LineColorChangedHandler)Delegate.Combine(lineColorChangedHandler2, value), lineColorChangedHandler);
			}
			while (lineColorChangedHandler != lineColorChangedHandler2);
		}
		remove
		{
			TransportManager.LineColorChangedHandler lineColorChangedHandler = this.eventLineColorChanged;
			TransportManager.LineColorChangedHandler lineColorChangedHandler2;
			do
			{
				lineColorChangedHandler2 = lineColorChangedHandler;
				lineColorChangedHandler = Interlocked.CompareExchange<TransportManager.LineColorChangedHandler>(ref this.eventLineColorChanged, (TransportManager.LineColorChangedHandler)Delegate.Remove(lineColorChangedHandler2, value), lineColorChangedHandler);
			}
			while (lineColorChangedHandler != lineColorChangedHandler2);
		}
	}

	public event TransportManager.LineNameChangedHandler eventLineNameChanged
	{
		add
		{
			TransportManager.LineNameChangedHandler lineNameChangedHandler = this.eventLineNameChanged;
			TransportManager.LineNameChangedHandler lineNameChangedHandler2;
			do
			{
				lineNameChangedHandler2 = lineNameChangedHandler;
				lineNameChangedHandler = Interlocked.CompareExchange<TransportManager.LineNameChangedHandler>(ref this.eventLineNameChanged, (TransportManager.LineNameChangedHandler)Delegate.Combine(lineNameChangedHandler2, value), lineNameChangedHandler);
			}
			while (lineNameChangedHandler != lineNameChangedHandler2);
		}
		remove
		{
			TransportManager.LineNameChangedHandler lineNameChangedHandler = this.eventLineNameChanged;
			TransportManager.LineNameChangedHandler lineNameChangedHandler2;
			do
			{
				lineNameChangedHandler2 = lineNameChangedHandler;
				lineNameChangedHandler = Interlocked.CompareExchange<TransportManager.LineNameChangedHandler>(ref this.eventLineNameChanged, (TransportManager.LineNameChangedHandler)Delegate.Remove(lineNameChangedHandler2, value), lineNameChangedHandler);
			}
			while (lineNameChangedHandler != lineNameChangedHandler2);
		}
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetLineColor(ushort lineID, Color color)
	{
		TransportManager.<SetLineColor>c__Iterator0 <SetLineColor>c__Iterator = new TransportManager.<SetLineColor>c__Iterator0();
		<SetLineColor>c__Iterator.lineID = lineID;
		<SetLineColor>c__Iterator.color = color;
		<SetLineColor>c__Iterator.$this = this;
		return <SetLineColor>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetLineName(ushort lineID, string name)
	{
		TransportManager.<SetLineName>c__Iterator1 <SetLineName>c__Iterator = new TransportManager.<SetLineName>c__Iterator1();
		<SetLineName>c__Iterator.lineID = lineID;
		<SetLineName>c__Iterator.name = name;
		<SetLineName>c__Iterator.$this = this;
		return <SetLineName>c__Iterator;
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<TransportInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
				}
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<TransportInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("TransportManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 256; i++)
		{
			if (this.m_lines.m_buffer[i].m_flags != TransportLine.Flags.None)
			{
				TransportInfo info = this.m_lines.m_buffer[i].Info;
				if (info == null || (this.m_lines.m_buffer[i].m_flags & TransportLine.Flags.Invalid) != TransportLine.Flags.None)
				{
					this.ReleaseLine((ushort)i);
				}
				else if (this.m_lines.m_buffer[i].m_building != 0)
				{
					ushort building = this.m_lines.m_buffer[i].m_building;
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					if ((instance.m_buildings.m_buffer[(int)building].m_flags & (Building.Flags.Created | Building.Flags.Deleted)) != Building.Flags.Created || instance.m_buildings.m_buffer[(int)building].Info == null)
					{
						this.ReleaseLine((ushort)i);
					}
				}
			}
		}
		int num = PrefabCollection<TransportInfo>.PrefabCount();
		this.m_infoCount = num;
		for (int j = 0; j < num; j++)
		{
			TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab((uint)j);
			if (prefab != null && prefab.m_UnlockMilestone != null)
			{
				switch (mode)
				{
				case SimulationManager.UpdateMode.NewMap:
				case SimulationManager.UpdateMode.LoadMap:
				case SimulationManager.UpdateMode.NewAsset:
				case SimulationManager.UpdateMode.LoadAsset:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.NewGameFromMap:
				case SimulationManager.UpdateMode.NewScenarioFromMap:
				case SimulationManager.UpdateMode.UpdateScenarioFromMap:
					prefab.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.LoadGame:
				case SimulationManager.UpdateMode.NewScenarioFromGame:
				case SimulationManager.UpdateMode.LoadScenario:
				case SimulationManager.UpdateMode.NewGameFromScenario:
				case SimulationManager.UpdateMode.UpdateScenarioFromGame:
					prefab.m_UnlockMilestone.Reset(true);
					break;
				}
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_monorailLine == null)
		{
			this.m_monorailLine = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_endOfLineStation == null)
		{
			this.m_endOfLineStation = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_ferryBusHub == null)
		{
			this.m_ferryBusHub = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_ferryLine == null)
		{
			this.m_ferryLine = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_ferryDepot == null)
		{
			this.m_ferryDepot = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_blimpLine == null)
		{
			this.m_blimpLine = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_blimpDepot == null)
		{
			this.m_blimpDepot = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new TransportManager.Data());
	}

	public string GetLineName(ushort lineID)
	{
		if (this.m_lines.m_buffer[(int)lineID].m_flags != TransportLine.Flags.None)
		{
			string text = null;
			if ((this.m_lines.m_buffer[(int)lineID].m_flags & TransportLine.Flags.CustomName) != TransportLine.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.TransportLine = lineID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateName(lineID);
			}
			return text;
		}
		return null;
	}

	public Color GetLineColor(ushort lineID)
	{
		if (this.m_lines.m_buffer[(int)lineID].m_flags == TransportLine.Flags.None)
		{
			return Color.get_white();
		}
		if ((this.m_lines.m_buffer[(int)lineID].m_flags & TransportLine.Flags.CustomColor) != TransportLine.Flags.None)
		{
			return this.m_lines.m_buffer[(int)lineID].m_color;
		}
		return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_lines.m_buffer[(int)lineID].Info.m_transportType];
	}

	private string GenerateName(ushort lineID)
	{
		string text = null;
		TransportInfo info = this.m_lines.m_buffer[(int)lineID].Info;
		if (info != null)
		{
			string text2 = PrefabCollection<TransportInfo>.PrefabName((uint)info.m_prefabDataIndex);
			string format = Locale.Get("TRANSPORT_LINE_PATTERN", text2);
			return StringUtils.SafeFormat(format, this.m_lines.m_buffer[(int)lineID].m_lineNumber);
		}
		if (text == null)
		{
			text = "Invalid";
		}
		return text;
	}

	public bool TransportTypeLoaded(TransportInfo.TransportType type)
	{
		return this.m_transportTypeLoaded[(int)type] != null;
	}

	public TransportInfo GetTransportInfo(TransportInfo.TransportType type)
	{
		return this.m_transportTypeLoaded[(int)type];
	}

	public bool TransportServiceLoaded(ItemClass.SubService subService)
	{
		switch (subService)
		{
		case ItemClass.SubService.PublicTransportBus:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Bus);
		case ItemClass.SubService.PublicTransportMetro:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Metro);
		case ItemClass.SubService.PublicTransportTrain:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Train);
		case ItemClass.SubService.PublicTransportShip:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Ship);
		case ItemClass.SubService.PublicTransportPlane:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Airplane);
		case ItemClass.SubService.PublicTransportTaxi:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Taxi);
		case ItemClass.SubService.PublicTransportTram:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Tram);
		case ItemClass.SubService.PublicTransportMonorail:
			return this.TransportTypeLoaded(TransportInfo.TransportType.Monorail);
		case ItemClass.SubService.PublicTransportCableCar:
			return this.TransportTypeLoaded(TransportInfo.TransportType.CableCar);
		}
		return true;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const int MAX_LINE_COUNT = 256;

	public int m_lineCount;

	public int m_infoCount;

	[NonSerialized]
	public Array16<TransportLine> m_lines;

	[NonSerialized]
	public Mesh[][] m_lineMeshes;

	[NonSerialized]
	public RenderGroup.MeshData[][] m_lineMeshData;

	[NonSerialized]
	public TransportManager.LineSegment[][] m_lineSegments;

	[NonSerialized]
	public Bezier3[][] m_lineCurves;

	[NonSerialized]
	public ulong[] m_updatedLines;

	[NonSerialized]
	public bool m_linesUpdated;

	[NonSerialized]
	public int m_metroLayer;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_StartOffset;

	[NonSerialized]
	public Material m_patchMaterial;

	[NonSerialized]
	public TransportPassengerData[] m_passengers;

	[NonSerialized]
	public GenericGuide m_monorailLine;

	[NonSerialized]
	public BuildingInstanceGuide m_endOfLineStation;

	[NonSerialized]
	public BuildingInstanceGuide m_ferryBusHub;

	[NonSerialized]
	public GenericGuide m_ferryLine;

	[NonSerialized]
	public GenericGuide m_ferryDepot;

	[NonSerialized]
	public GenericGuide m_blimpLine;

	[NonSerialized]
	public GenericGuide m_blimpDepot;

	private ushort[] m_lineNumber;

	private TransportInfo[] m_transportTypeLoaded;

	private int m_linesVisible;

	private bool m_tunnelsVisible;

	private bool m_tunnelsVisibleInfo;

	private Camera m_undergroundCamera;

	private TransportPatch[] m_patches;

	private bool m_patchesDirty;

	public struct LineSegment
	{
		public Bounds m_bounds;

		public int m_curveStart;

		public int m_curveEnd;
	}

	public delegate void LineColorChangedHandler(ushort id);

	public delegate void LineNameChangedHandler(ushort id);

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "TransportManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			TransportManager instance2 = Singleton<TransportManager>.get_instance();
			TransportLine[] buffer = instance2.m_lines.m_buffer;
			int num = buffer.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			int num2 = PrefabCollection<TransportInfo>.PrefabCount();
			uint num3 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int j = 0; j < num2; j++)
				{
					TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab((uint)j);
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num3 += 1u;
					}
				}
			}
			s.WriteUInt16(num3);
			try
			{
				PrefabCollection<TransportInfo>.BeginSerialize(s);
				for (int k = 1; k < num; k++)
				{
					if (buffer[k].m_flags != TransportLine.Flags.None)
					{
						PrefabCollection<TransportInfo>.Serialize((uint)buffer[k].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int l = 0; l < num2; l++)
					{
						TransportInfo prefab2 = PrefabCollection<TransportInfo>.GetPrefab((uint)l);
						if (prefab2 != null && prefab2.m_UnlockMilestone != null && prefab2.m_prefabDataIndex != -1)
						{
							PrefabCollection<TransportInfo>.Serialize((uint)prefab2.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<TransportInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int m = 0; m < num2; m++)
				{
					TransportInfo prefab3 = PrefabCollection<TransportInfo>.GetPrefab((uint)m);
					if (prefab3 != null && prefab3.m_UnlockMilestone != null && prefab3.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab3.m_UnlockMilestone.GetData());
					}
				}
			}
			for (int n = 0; n < 10; n++)
			{
				s.WriteUInt16((uint)instance2.m_lineNumber[n]);
			}
			for (int num4 = 1; num4 < num; num4++)
			{
				if (buffer[num4].m_flags != TransportLine.Flags.None)
				{
					s.WriteUInt16((uint)buffer[num4].m_lineNumber);
					s.WriteUInt16((uint)buffer[num4].m_stops);
					s.WriteUInt8((uint)buffer[num4].m_color.r);
					s.WriteUInt8((uint)buffer[num4].m_color.g);
					s.WriteUInt8((uint)buffer[num4].m_color.b);
					s.WriteUInt16((uint)buffer[num4].m_building);
					s.WriteUInt16((uint)buffer[num4].m_budget);
					s.WriteFloat(buffer[num4].m_totalLength);
					s.WriteUInt8((uint)buffer[num4].m_averageInterval);
					buffer[num4].m_passengers.Serialize(s);
				}
			}
			for (int num5 = 0; num5 < 10; num5++)
			{
				instance2.m_passengers[num5].Serialize(s);
			}
			s.WriteObject<GenericGuide>(instance2.m_monorailLine);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_endOfLineStation);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_ferryBusHub);
			s.WriteObject<GenericGuide>(instance2.m_ferryLine);
			s.WriteObject<GenericGuide>(instance2.m_ferryDepot);
			s.WriteObject<GenericGuide>(instance2.m_blimpLine);
			s.WriteObject<GenericGuide>(instance2.m_blimpDepot);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "TransportManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "TransportManager");
			TransportManager instance = Singleton<TransportManager>.get_instance();
			TransportLine[] buffer = instance.m_lines.m_buffer;
			int num = buffer.Length;
			instance.m_lines.ClearUnused();
			if (s.get_version() >= 52u)
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int i = 1; i < num; i++)
				{
					buffer[i].m_flags = (TransportLine.Flags)uInt.Read();
				}
				uInt.EndRead();
			}
			else
			{
				for (int j = 1; j < num; j++)
				{
					buffer[j].m_flags = TransportLine.Flags.None;
				}
			}
			uint num2 = 0u;
			if (s.get_version() >= 151u)
			{
				num2 = s.ReadUInt16();
				this.m_milestoneInfoIndex = new uint[num2];
				this.m_UnlockMilestones = new MilestoneInfo.Data[num2];
			}
			if (s.get_version() >= 52u)
			{
				PrefabCollection<TransportInfo>.BeginDeserialize(s);
				for (int k = 1; k < num; k++)
				{
					if (buffer[k].m_flags != TransportLine.Flags.None)
					{
						buffer[k].m_infoIndex = (ushort)PrefabCollection<TransportInfo>.Deserialize(true);
					}
				}
				for (uint num3 = 0u; num3 < num2; num3 += 1u)
				{
					this.m_milestoneInfoIndex[(int)((UIntPtr)num3)] = PrefabCollection<TransportInfo>.Deserialize(false);
				}
				PrefabCollection<TransportInfo>.EndDeserialize(s);
			}
			for (uint num4 = 0u; num4 < num2; num4 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num4)] = s.ReadObject<MilestoneInfo.Data>();
			}
			if (s.get_version() >= 310u)
			{
				for (int l = 0; l < 10; l++)
				{
					instance.m_lineNumber[l] = (ushort)s.ReadUInt16();
				}
			}
			else if (s.get_version() >= 211u)
			{
				for (int m = 0; m < 8; m++)
				{
					instance.m_lineNumber[m] = (ushort)s.ReadUInt16();
				}
				for (int n = 8; n < 10; n++)
				{
					instance.m_lineNumber[n] = 0;
				}
			}
			else if (s.get_version() >= 153u)
			{
				for (int num5 = 0; num5 < 5; num5++)
				{
					instance.m_lineNumber[num5] = (ushort)s.ReadUInt16();
				}
				for (int num6 = 5; num6 < 10; num6++)
				{
					instance.m_lineNumber[num6] = 0;
				}
			}
			else
			{
				for (int num7 = 0; num7 < 10; num7++)
				{
					instance.m_lineNumber[num7] = 0;
				}
			}
			for (int num8 = 1; num8 < num; num8++)
			{
				buffer[num8].m_vehicles = 0;
				if (buffer[num8].m_flags != TransportLine.Flags.None)
				{
					if ((buffer[num8].m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
					{
						TransportLine[] expr_2D5_cp_0 = buffer;
						int expr_2D5_cp_1 = num8;
						expr_2D5_cp_0[expr_2D5_cp_1].m_flags = (expr_2D5_cp_0[expr_2D5_cp_1].m_flags | TransportLine.Flags.Hidden);
					}
					else
					{
						TransportLine[] expr_2F0_cp_0 = buffer;
						int expr_2F0_cp_1 = num8;
						expr_2F0_cp_0[expr_2F0_cp_1].m_flags = (expr_2F0_cp_0[expr_2F0_cp_1].m_flags & ~(TransportLine.Flags.Hidden | TransportLine.Flags.Selected));
					}
					TransportLine[] expr_306_cp_0 = buffer;
					int expr_306_cp_1 = num8;
					expr_306_cp_0[expr_306_cp_1].m_flags = (expr_306_cp_0[expr_306_cp_1].m_flags & ~TransportLine.Flags.Highlighted);
					if (s.get_version() >= 153u)
					{
						buffer[num8].m_lineNumber = (ushort)s.ReadUInt16();
					}
					else if ((buffer[num8].m_flags & TransportLine.Flags.Temporary) != TransportLine.Flags.None)
					{
						buffer[num8].m_lineNumber = 0;
					}
					else
					{
						TransportLine[] arg_392_0_cp_0 = buffer;
						int arg_392_0_cp_1 = num8;
						ushort[] expr_387_cp_0 = instance.m_lineNumber;
						ushort expr_387_cp_1 = buffer[num8].m_infoIndex;
						arg_392_0_cp_0[arg_392_0_cp_1].m_lineNumber = (expr_387_cp_0[(int)expr_387_cp_1] = expr_387_cp_0[(int)expr_387_cp_1] + 1);
					}
					buffer[num8].m_stops = (ushort)s.ReadUInt16();
					if (s.get_version() >= 121u)
					{
						buffer[num8].m_color.r = (byte)s.ReadUInt8();
						buffer[num8].m_color.g = (byte)s.ReadUInt8();
						buffer[num8].m_color.b = (byte)s.ReadUInt8();
						buffer[num8].m_color.a = 255;
					}
					else
					{
						buffer[num8].m_color = default(Color32);
					}
					if (s.get_version() >= 288u)
					{
						buffer[num8].m_building = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[num8].m_building = 0;
					}
					if (s.get_version() >= 314u)
					{
						buffer[num8].m_budget = (ushort)s.ReadUInt16();
						buffer[num8].m_totalLength = s.ReadFloat();
					}
					else
					{
						buffer[num8].m_budget = 100;
						buffer[num8].m_totalLength = 0f;
					}
					if (s.get_version() >= 320u)
					{
						buffer[num8].m_averageInterval = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[num8].m_averageInterval = 0;
					}
					if (s.get_version() >= 122u)
					{
						buffer[num8].m_passengers.Deserialize(s);
					}
					else
					{
						buffer[num8].m_passengers = default(TransportPassengerData);
					}
				}
				else
				{
					buffer[num8].m_lineNumber = 0;
					buffer[num8].m_stops = 0;
					buffer[num8].m_building = 0;
					buffer[num8].m_budget = 0;
					buffer[num8].m_totalLength = 0f;
					buffer[num8].m_averageInterval = 0;
					buffer[num8].m_color = default(Color32);
					buffer[num8].m_passengers = default(TransportPassengerData);
					instance.m_lines.ReleaseItem((ushort)num8);
				}
			}
			for (int num9 = 0; num9 < 10; num9++)
			{
				if (s.get_version() >= 310u || (s.get_version() >= 211u && num9 < 8) || (s.get_version() >= 122u && num9 < 5))
				{
					instance.m_passengers[num9].Deserialize(s);
				}
				else
				{
					instance.m_passengers[num9] = default(TransportPassengerData);
				}
			}
			if (s.get_version() >= 316u)
			{
				instance.m_monorailLine = s.ReadObject<GenericGuide>();
				instance.m_endOfLineStation = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_monorailLine = null;
				instance.m_endOfLineStation = null;
			}
			if (s.get_version() >= 317u)
			{
				instance.m_ferryBusHub = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_ferryBusHub = null;
			}
			if (s.get_version() >= 318u)
			{
				instance.m_ferryLine = s.ReadObject<GenericGuide>();
				instance.m_ferryDepot = s.ReadObject<GenericGuide>();
				instance.m_blimpLine = s.ReadObject<GenericGuide>();
				instance.m_blimpDepot = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_ferryLine = null;
				instance.m_ferryDepot = null;
				instance.m_blimpLine = null;
				instance.m_blimpDepot = null;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "TransportManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "TransportManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<TransportInfo>.BindPrefabs();
			TransportManager instance = Singleton<TransportManager>.get_instance();
			TransportLine[] buffer = instance.m_lines.m_buffer;
			int num = buffer.Length;
			for (int i = 1; i < num; i++)
			{
				if (buffer[i].m_flags != TransportLine.Flags.None)
				{
					TransportInfo info = buffer[i].Info;
					int num2 = -1;
					if (info != null)
					{
						buffer[i].m_infoIndex = (ushort)info.m_prefabDataIndex;
						num2 = info.m_netInfo.m_prefabDataIndex;
					}
					ushort stops = buffer[i].m_stops;
					ushort num3 = stops;
					int num4 = 0;
					while (num3 != 0)
					{
						if (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num3].m_transportLine == (ushort)i)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid transport line loop!\n" + Environment.StackTrace);
							TransportLine[] expr_E4_cp_0 = buffer;
							int expr_E4_cp_1 = i;
							expr_E4_cp_0[expr_E4_cp_1].m_flags = (expr_E4_cp_0[expr_E4_cp_1].m_flags | TransportLine.Flags.Invalid);
							break;
						}
						if (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num3].m_flags == NetNode.Flags.None || Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num3].m_transportLine != 0)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid transport line node!\n" + Environment.StackTrace);
							TransportLine[] expr_153_cp_0 = buffer;
							int expr_153_cp_1 = i;
							expr_153_cp_0[expr_153_cp_1].m_flags = (expr_153_cp_0[expr_153_cp_1].m_flags | TransportLine.Flags.Invalid);
							break;
						}
						Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num3].m_transportLine = (ushort)i;
						if (num2 != -1)
						{
							Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num3].m_infoIndex = (ushort)num2;
						}
						ushort nextSegment = TransportLine.GetNextSegment(num3);
						if (nextSegment != 0)
						{
							if (num2 != -1)
							{
								Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nextSegment].m_infoIndex = (ushort)num2;
							}
							num3 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nextSegment].m_endNode;
						}
						else
						{
							num3 = 0;
						}
						if (num3 == stops)
						{
							break;
						}
						if (++num4 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
			if (this.m_milestoneInfoIndex != null)
			{
				int num5 = this.m_milestoneInfoIndex.Length;
				for (int j = 0; j < num5; j++)
				{
					TransportInfo prefab = PrefabCollection<TransportInfo>.GetPrefab(this.m_milestoneInfoIndex[j]);
					if (prefab != null)
					{
						MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[j]);
						}
					}
				}
			}
			instance.m_linesUpdated = true;
			int num6 = instance.m_updatedLines.Length;
			for (int k = 0; k < num6; k++)
			{
				instance.m_updatedLines[k] = 18446744073709551615uL;
			}
			int num7 = instance.m_patches.Length;
			for (int l = 0; l < num7; l++)
			{
				TransportPatch transportPatch = instance.m_patches[l];
				int num8 = 0;
				while (transportPatch != null)
				{
					transportPatch.m_isDirty = true;
					transportPatch = transportPatch.m_nextPatch;
					if (++num8 >= 100)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			instance.m_patchesDirty = true;
			instance.m_lineCount = (int)(instance.m_lines.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "TransportManager");
		}

		private uint[] m_milestoneInfoIndex;

		private MilestoneInfo.Data[] m_UnlockMilestones;
	}
}
