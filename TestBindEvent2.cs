﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class TestBindEvent2 : MonoBehaviour
{
	public void OnUserClickedBack(UIComponent component, UIMouseEventParameter p)
	{
		UITabstrip component2 = base.GetComponent<UITabstrip>();
		if (component2 != null)
		{
			component2.set_selectedIndex(0);
		}
		Debug.Log("Back Clicked");
	}
}
