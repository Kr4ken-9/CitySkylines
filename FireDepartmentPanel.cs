﻿using System;
using ColossalFramework.UI;

public sealed class FireDepartmentPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.FireDepartment;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		ItemClass.Service service = info.GetService();
		return service == this.service || service == ItemClass.Service.Disaster;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Building);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
