﻿using System;
using ColossalFramework.Globalization;

public static class LocaleFormatter
{
	public static string FormatCost(int inCost, int inDistanceCost)
	{
		float num = (float)inCost * 0.01f;
		float num2 = (float)inDistanceCost * 0.01f;
		return StringUtils.SafeFormat(Locale.Get("AIINFO_CUMULATIVECOST"), new object[]
		{
			num.ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()),
			num2.ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo())
		});
	}

	public static string FormatPercentage(int p)
	{
		return StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), p);
	}

	public static string FormatGeneric(string localeID, params object[] p)
	{
		return StringUtils.SafeFormat(Locale.Get(localeID), p);
	}

	public static string FormatCost(int inCost, bool isDistanceBased)
	{
		float num = (float)inCost * 0.01f;
		return StringUtils.SafeFormat(Locale.Get((!isDistanceBased) ? "AIINFO_COST" : "AIINFO_COSTPERCELL"), num.ToString(Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()));
	}

	public static string FormatCubicCost(float cost)
	{
		return StringUtils.SafeFormat(Locale.Get("AIINFO_COSTPERCUBICCELL"), cost.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()));
	}

	public static string FormatUpkeep(int inUpkeep, int inDistanceUpkeep)
	{
		float num = (float)inUpkeep * 0.0016f;
		float num2 = (float)inDistanceUpkeep * 0.0016f;
		return StringUtils.SafeFormat(Locale.Get("AIINFO_CUMULATIVEUPKEEP"), new object[]
		{
			num.ToString((num <= 1f) ? Settings.moneyFormat : Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()),
			num2.ToString((num2 < 10f) ? Settings.moneyFormat : Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo())
		});
	}

	public static string FormatUpkeep(int inUpkeep, bool isDistanceBased)
	{
		float num = (float)inUpkeep * 0.0016f;
		return StringUtils.SafeFormat(Locale.Get((!isDistanceBased) ? "AIINFO_UPKEEP" : "AIINFO_UPKEEPPERCELL"), num.ToString((num < 10f) ? Settings.moneyFormat : Settings.moneyFormatNoCents, LocaleManager.get_cultureInfo()));
	}

	public static readonly string Title = "title";

	public static readonly string Cost = "cost";

	public static readonly string Upkeep = "upkeep";

	public static readonly string Sprite = "sprite";

	public static readonly string Text = "text";

	public static readonly string Locked = "locked";

	public static readonly string IsUpkeepVisible = "isUpkeepVisible";

	public static readonly string Speed = "speed";

	public static readonly string Info1 = "info1";

	public static readonly string Info2 = "info2";

	public static readonly string Pollution = "pollution";

	public static readonly string NoisePollution = "noisePollution";

	public static readonly string LockedInfo = "lockedInfo";

	public static readonly string UnlockDesc = "unlockDesc";

	public static readonly string UnlockPopulationProgressText = "unlockPopulationProgressText";

	public static readonly string UnlockPopulationTarget = "unlockPopulationTarget";

	public static readonly string UnlockPopulationCurrent = "unlockPopulationCurrent";
}
