﻿using System;
using UnityEngine;

public class UIPopulationDeltaWrapper : UIWrapper<int>
{
	public UIPopulationDeltaWrapper(int def) : base(def)
	{
	}

	public override void Check(int newVal)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			string text = (this.m_Value <= 0) ? ((this.m_Value >= 0) ? "<color #ff4500>" : "<color #800000>-") : "<color #006400>+";
			this.m_String = StringUtils.SafeFormat("{1}{0:N0}</color>", new object[]
			{
				Mathf.Abs(this.m_Value),
				text
			});
		}
	}

	private const string kPosPrefix = "<color #006400>+";

	private const string kNegPrefix = "<color #800000>-";

	private const string kNeutralPrefix = "<color #ff4500>";

	private const string kFormat = "{1}{0:N0}</color>";
}
