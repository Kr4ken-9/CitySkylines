﻿using System;
using ColossalFramework;
using UnityEngine;

public class NetNodeTutorialTag : IUITag
{
	public NetNodeTutorialTag(ushort id)
	{
		this.m_ID = id;
	}

	public bool isValid
	{
		get
		{
			return true;
		}
	}

	public bool isDynamic
	{
		get
		{
			return true;
		}
	}

	public bool isRelative
	{
		get
		{
			return true;
		}
	}

	public bool locationLess
	{
		get
		{
			return false;
		}
	}

	public Vector3 position
	{
		get
		{
			return Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_ID].m_position;
		}
	}

	private ushort m_ID;
}
