﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;
using UnityEngine;

public class TutorialPanel : UICustomControl
{
	private void Awake()
	{
		TutorialPanel.sInstance = this;
		this.m_HighLight = base.Find<UISlicedSprite>("HighLight");
		this.m_BackgroundSprite = base.Find<UISlicedSprite>("BackgroundSprite");
		this.m_Text = base.Find<UILabel>("Label");
		this.m_HighLight.Hide();
	}

	public static UIComponent Show(IUITag tag, string info)
	{
		return TutorialPanel.Show(tag, info, 0f);
	}

	public static TutorialPanel instance
	{
		get
		{
			return TutorialPanel.sInstance;
		}
	}

	private void OnDestroy()
	{
		TutorialPanel.sInstance = null;
	}

	private void AdjustPosition(bool locationLess, Vector3 position)
	{
		UIPivotPoint uIPivotPoint = 6;
		if (!locationLess)
		{
			UIView uIView = base.get_component().GetUIView();
			Vector2 screenResolution = uIView.GetScreenResolution();
			Vector3 vector = UIPivotExtensions.UpperLeftToTransform(6, base.get_component().get_size(), base.get_component().get_arbitraryPivotOffset());
			Vector2 vector2 = uIView.WorldPointToGUI(uIView.get_uiCamera(), position) + new Vector2(vector.x, vector.y);
			if (vector2.x + base.get_component().get_width() > screenResolution.x)
			{
				uIPivotPoint = 8;
				this.m_BackgroundSprite.set_flip(1);
			}
			else
			{
				this.m_BackgroundSprite.set_flip(0);
			}
			if (vector2.y < 0f)
			{
				uIPivotPoint = ((uIPivotPoint != 8) ? 0 : 2);
				this.m_BackgroundSprite.set_spriteName("TutorialBubbleFlipRight");
			}
			else
			{
				this.m_BackgroundSprite.set_spriteName("TutorialBubbleRight");
			}
		}
		else
		{
			uIPivotPoint = 0;
			this.m_BackgroundSprite.set_flip(0);
		}
		base.get_component().set_pivot(uIPivotPoint);
		base.get_component().get_transform().set_position(position);
	}

	public static UIComponent Show(IUITag tag, string info, float timeout)
	{
		if (tag == null)
		{
			return null;
		}
		TutorialPanel panel = UIView.get_library().Show<TutorialPanel>("TutorialPanel");
		if (tag.isRelative)
		{
			panel.m_BackgroundSprite.set_flip(0);
			Vector3 vector = Camera.get_main().WorldToScreenPoint(tag.position);
			UIView uIView = panel.get_component().GetUIView();
			vector /= uIView.get_inputScale();
			Vector3 vector2 = UIPivotExtensions.UpperLeftToTransform(panel.get_component().get_pivot(), panel.get_component().get_size(), panel.get_component().get_arbitraryPivotOffset());
			Vector3 relativePosition = uIView.ScreenPointToGUI(vector) + new Vector2(vector2.x, vector2.y);
			panel.get_component().set_pivot(6);
			panel.get_component().set_relativePosition(relativePosition);
		}
		else
		{
			panel.AdjustPosition(tag.locationLess, tag.position);
		}
		if (tag is MonoTutorialTag)
		{
			panel.m_TagString = ((MonoTutorialTag)tag).m_Tag;
		}
		else
		{
			panel.m_TagString = null;
		}
		panel.SetTimeout(timeout);
		if (tag.isDynamic)
		{
			panel.m_Tag = tag;
		}
		else
		{
			panel.m_Tag = null;
		}
		ValueAnimator.Animate("TutorialPanel", delegate(float val)
		{
			panel.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.3f));
		BindPropertyByKey component = panel.GetComponent<BindPropertyByKey>();
		component.SetProperties(info);
		panel.AdjustHeight();
		if (panel.m_NotificationSound != null)
		{
			Singleton<AudioManager>.get_instance().PlaySound(panel.m_NotificationSound, 1f);
		}
		return panel.get_component();
	}

	private void AdjustHeight()
	{
		base.get_component().set_size(new Vector2(base.get_component().get_width(), this.m_Text.get_relativePosition().y + this.m_Text.get_height() + this.m_BottomMargin));
	}

	public static UIComponent Hide()
	{
		if (UIView.get_library() != null)
		{
			UIComponent comp = UIView.get_library().Get("TutorialPanel");
			if (comp.get_isVisible())
			{
				ValueAnimator.Animate("TutorialPanel", delegate(float val)
				{
					comp.set_opacity(val);
				}, new AnimatedFloat(1f, 0f, 0.3f), delegate
				{
					UIView.get_library().Hide("TutorialPanel");
				});
			}
			return comp;
		}
		return null;
	}

	public void OnMouseHover(UIComponent comp, UIMouseEventParameter p)
	{
		if (base.get_component() == comp && !this.m_IsClosing)
		{
			this.m_IsFadingOut = false;
			this.m_LastShown = Time.get_realtimeSinceStartup();
			if (base.get_component().get_opacity() != 1f)
			{
				ValueAnimator.Animate("TutorialPanel", delegate(float val)
				{
					base.get_component().set_opacity(val);
				}, new AnimatedFloat(0f, 1f, 0.3f));
			}
		}
	}

	private void BlinkHighLight()
	{
		if (this.m_IsBlinking)
		{
			ValueAnimator.Animate("TutorialPanelBlink", delegate(float val)
			{
				if (this.m_HighLight != null)
				{
					this.m_HighLight.set_opacity(val);
				}
			}, new AnimatedFloat(0f, 1f, 0.5f * this.m_BlinkingTimeScale, 27), delegate
			{
				this.m_IsBlinking = false;
			});
		}
		else
		{
			ValueAnimator.Animate("TutorialPanelBlink", delegate(float val)
			{
				if (this.m_HighLight != null)
				{
					this.m_HighLight.set_opacity(val);
				}
			}, new AnimatedFloat(1f, 0f, 1f * this.m_BlinkingTimeScale), delegate
			{
				this.m_IsBlinking = true;
			});
		}
	}

	private void ShowHighLight(UIComponent target)
	{
		if (target == null || !target.get_isInteractive())
		{
			this.HideHighLight();
		}
		else
		{
			this.BlinkHighLight();
			this.m_HighLight.Show();
			this.m_HighLight.set_size(target.get_size());
			this.m_HighLight.set_pivot(target.get_pivot());
			this.m_HighLight.set_arbitraryPivotOffset(target.get_arbitraryPivotOffset());
			this.m_HighLight.get_transform().set_position(target.get_transform().get_position());
		}
	}

	private void HideHighLight()
	{
		this.m_HighLight.Hide();
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	public void OnClose()
	{
		TutorialPanel.Hide();
		this.m_IsClosing = true;
	}

	public void SetTimeout(float timeout)
	{
		this.HideHighLight();
		this.m_IsFadingOut = false;
		this.m_IsClosing = false;
		this.m_LastShown = Time.get_realtimeSinceStartup();
		this.m_Timeout = timeout;
	}

	public static bool IsNull(object aObj)
	{
		return aObj == null || aObj.Equals(null);
	}

	private void LateUpdate()
	{
		if (base.get_component().get_isVisible())
		{
			if ((TutorialPanel.IsNull(this.m_Tag) || (!TutorialPanel.IsNull(this.m_Tag) && !this.m_Tag.isValid)) && !string.IsNullOrEmpty(this.m_TagString))
			{
				this.m_Tag = MonoTutorialTag.Find(this.m_TagString);
			}
			if (!TutorialPanel.IsNull(this.m_Tag))
			{
				this.m_BackgroundSprite.set_spriteName((!this.m_Tag.locationLess) ? "TutorialBubbleRight" : "TutorialBubble");
				this.AdjustHeight();
				if (this.m_Tag.isRelative)
				{
					this.m_BackgroundSprite.set_flip(0);
					Vector3 vector = Camera.get_main().WorldToScreenPoint(this.m_Tag.position);
					UIView uIView = base.get_component().GetUIView();
					vector /= uIView.get_inputScale();
					Vector3 vector2 = UIPivotExtensions.UpperLeftToTransform(base.get_component().get_pivot(), base.get_component().get_size(), base.get_component().get_arbitraryPivotOffset());
					Vector3 relativePosition = uIView.ScreenPointToGUI(vector) + new Vector2(vector2.x, vector2.y);
					base.get_component().set_pivot(6);
					base.get_component().set_relativePosition(relativePosition);
					this.HideHighLight();
				}
				else
				{
					if (this.m_Tag is TutorialUITag)
					{
						TutorialUITag tutorialUITag = (TutorialUITag)this.m_Tag;
						this.ShowHighLight(tutorialUITag.target);
					}
					this.AdjustPosition(this.m_Tag.locationLess, this.m_Tag.position);
				}
			}
			if (!this.m_IsFadingOut && this.m_Timeout > 0f && Time.get_realtimeSinceStartup() - this.m_LastShown > this.m_Timeout)
			{
				TutorialPanel.Hide();
				this.m_IsFadingOut = true;
			}
		}
	}

	public AudioClip m_NotificationSound;

	public float m_BlinkingTimeScale = 1.5f;

	public float m_BottomMargin = 20f;

	private IUITag m_Tag;

	private float m_LastShown;

	private float m_Timeout;

	private bool m_IsClosing;

	private bool m_IsFadingOut;

	private string m_TagString;

	private bool m_IsBlinking;

	private UIComponent m_HighLight;

	private UISlicedSprite m_BackgroundSprite;

	private UILabel m_Text;

	private static TutorialPanel sInstance;
}
