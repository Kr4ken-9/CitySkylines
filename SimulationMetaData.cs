﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework.IO;
using ColossalFramework.PlatformServices;

[Serializable]
public class SimulationMetaData : IDataContainer
{
	public SimulationMetaData()
	{
		this.m_gameInstanceIdentifier = null;
		this.m_environment = null;
		this.m_invertTraffic = SimulationMetaData.MetaBool.Undefined;
		this.m_disableAchievements = SimulationMetaData.MetaBool.Undefined;
		this.m_currentDateTime = new DateTime(0L);
		this.m_startingDateTime = new DateTime(0L);
		this.m_currentDayHour = -1f;
		this.m_MapName = null;
		this.m_ScenarioAsset = null;
		this.m_ScenarioName = null;
		this.m_CityName = null;
		this.m_scenarioWon = false;
		this.m_scenarioFailed = false;
		this.m_WorkshopPublishedFileId = PublishedFileId.invalid;
		this.m_newMapAppVersion = 0u;
		this.m_newGameAppVersion = 0u;
		this.m_saveAppVersion = 172090642u;
		this.m_dataFormatVersion = 109011u;
		this.m_updateMode = SimulationManager.UpdateMode.Undefined;
		this.m_MapThemeMetaData = null;
	}

	public void Merge(SimulationMetaData source)
	{
		if (source != null)
		{
			if (!string.IsNullOrEmpty(source.m_gameInstanceIdentifier))
			{
				this.m_gameInstanceIdentifier = source.m_gameInstanceIdentifier;
			}
			if (!string.IsNullOrEmpty(source.m_environment))
			{
				this.m_environment = source.m_environment;
			}
			if (source.m_invertTraffic != SimulationMetaData.MetaBool.Undefined)
			{
				this.m_invertTraffic = source.m_invertTraffic;
			}
			if (source.m_disableAchievements != SimulationMetaData.MetaBool.Undefined)
			{
				this.m_disableAchievements = source.m_disableAchievements;
			}
			if (source.m_currentDateTime.Ticks != 0L)
			{
				this.m_currentDateTime = source.m_currentDateTime;
			}
			if (source.m_startingDateTime.Ticks != 0L)
			{
				this.m_startingDateTime = source.m_startingDateTime;
			}
			if (source.m_currentDayHour >= 0f)
			{
				this.m_currentDayHour = source.m_currentDayHour;
			}
			if (source.m_newMapAppVersion != 0u)
			{
				this.m_newMapAppVersion = source.m_newMapAppVersion;
			}
			if (source.m_newGameAppVersion != 0u)
			{
				this.m_newGameAppVersion = source.m_newGameAppVersion;
			}
			if (source.m_updateMode != SimulationManager.UpdateMode.Undefined)
			{
				this.m_updateMode = source.m_updateMode;
			}
			if (!string.IsNullOrEmpty(source.m_MapName))
			{
				this.m_MapName = source.m_MapName;
			}
			if (!string.IsNullOrEmpty(source.m_ScenarioAsset))
			{
				this.m_ScenarioAsset = source.m_ScenarioAsset;
			}
			if (!string.IsNullOrEmpty(source.m_ScenarioName))
			{
				this.m_ScenarioName = source.m_ScenarioName;
			}
			if (source.m_modOverride != null)
			{
				while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				try
				{
					this.m_modOverride = source.m_modOverride;
				}
				finally
				{
					Monitor.Exit(this);
				}
			}
			this.m_MapThemeMetaData = source.m_MapThemeMetaData;
			this.m_CityName = source.m_CityName;
			this.m_WorkshopPublishedFileId = source.m_WorkshopPublishedFileId;
		}
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUniqueString(this.m_gameInstanceIdentifier);
		s.WriteUniqueString(this.m_environment);
		s.WriteUniqueString(this.m_ScenarioAsset);
		s.WriteUniqueString(this.m_ScenarioName);
		s.WriteBool(this.m_scenarioWon);
		s.WriteBool(this.m_scenarioFailed);
		s.WriteLong64(this.m_startingDateTime.Ticks);
		s.WriteUniqueString(this.m_MapName);
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_modOverride != null)
			{
				s.WriteInt32(this.m_modOverride.Count);
				foreach (KeyValuePair<string, bool> current in this.m_modOverride)
				{
					s.WriteUniqueString(current.Key);
					Dictionary<string, bool>.Enumerator enumerator;
					KeyValuePair<string, bool> current2 = enumerator.Current;
					s.WriteBool(current2.Value);
				}
			}
			else
			{
				s.WriteInt32(0);
			}
		}
		finally
		{
			Monitor.Exit(this);
		}
		s.WriteUInt8((uint)((byte)this.m_invertTraffic));
		s.WriteUInt8((uint)((byte)this.m_disableAchievements));
		s.WriteLong64(this.m_currentDateTime.Ticks);
		s.WriteFloat(this.m_currentDayHour);
		s.WriteUInt32(this.m_newMapAppVersion);
		s.WriteUInt32(this.m_newGameAppVersion);
		s.WriteUInt32(172090642u);
		s.WriteUInt32(109011u);
	}

	public void Deserialize(DataSerializer s)
	{
		if (s.get_version() >= 161u)
		{
			this.m_gameInstanceIdentifier = s.ReadUniqueString();
		}
		else
		{
			this.m_gameInstanceIdentifier = Guid.NewGuid().ToString();
		}
		this.m_environment = s.ReadUniqueString();
		if (s.get_version() >= 292u)
		{
			this.m_ScenarioAsset = s.ReadUniqueString();
		}
		else
		{
			this.m_ScenarioAsset = null;
		}
		if (s.get_version() >= 290u)
		{
			this.m_ScenarioName = s.ReadUniqueString();
			this.m_scenarioWon = s.ReadBool();
			this.m_scenarioFailed = s.ReadBool();
		}
		else
		{
			this.m_ScenarioName = null;
			this.m_scenarioWon = false;
			this.m_scenarioFailed = false;
		}
		if ((s.get_version() >= 322u && s.get_version() < 109000u) || s.get_version() >= 109001u)
		{
			this.m_startingDateTime = new DateTime(s.ReadLong64());
			this.m_MapName = s.ReadUniqueString();
		}
		else
		{
			this.m_startingDateTime = new DateTime(0L);
			this.m_MapName = null;
		}
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (s.get_version() >= 298u)
			{
				int num = s.ReadInt32();
				if (num != 0)
				{
					if (this.m_modOverride == null)
					{
						this.m_modOverride = new Dictionary<string, bool>();
					}
					else
					{
						this.m_modOverride.Clear();
					}
					for (int i = 0; i < num; i++)
					{
						string key = s.ReadUniqueString();
						bool value = s.ReadBool();
						this.m_modOverride.Add(key, value);
					}
				}
				else
				{
					this.m_modOverride = null;
				}
			}
			else
			{
				this.m_modOverride = null;
			}
		}
		finally
		{
			Monitor.Exit(this);
		}
		this.m_invertTraffic = (SimulationMetaData.MetaBool)s.ReadUInt8();
		if (s.get_version() >= 192u)
		{
			this.m_disableAchievements = (SimulationMetaData.MetaBool)s.ReadUInt8();
		}
		else
		{
			this.m_disableAchievements = SimulationMetaData.MetaBool.Undefined;
		}
		if (s.get_version() >= 183u)
		{
			this.m_currentDateTime = new DateTime(s.ReadLong64());
		}
		else
		{
			this.m_currentDateTime = DateTime.Now;
		}
		if (this.m_startingDateTime.Ticks == 0L)
		{
			this.m_startingDateTime = this.m_currentDateTime;
		}
		if (s.get_version() >= 212u)
		{
			this.m_currentDayHour = s.ReadFloat();
		}
		else
		{
			this.m_currentDayHour = 12f;
		}
		if (s.get_version() >= 197u)
		{
			this.m_newMapAppVersion = s.ReadUInt32();
			this.m_newGameAppVersion = s.ReadUInt32();
		}
		else
		{
			this.m_newMapAppVersion = 1000000u;
			this.m_newGameAppVersion = 1000000u;
		}
		if (s.get_version() >= 191u)
		{
			this.m_saveAppVersion = s.ReadUInt32();
			this.m_dataFormatVersion = s.ReadUInt32();
		}
		else
		{
			this.m_saveAppVersion = 50500u;
			this.m_dataFormatVersion = s.get_version();
		}
		this.m_updateMode = SimulationManager.UpdateMode.Undefined;
	}

	public void AfterDeserialize(DataSerializer s)
	{
	}

	public bool m_SystemMap;

	public bool m_BuiltinAsset;

	public bool m_MapThemeTemplate;

	public PublishedFileId m_WorkshopPublishedFileId = PublishedFileId.invalid;

	public string m_gameInstanceIdentifier;

	public string m_environment;

	public SimulationMetaData.MetaBool m_invertTraffic;

	public SimulationMetaData.MetaBool m_disableAchievements;

	public DateTime m_currentDateTime;

	public DateTime m_startingDateTime;

	public float m_currentDayHour;

	public string m_MapName;

	public string m_ScenarioAsset;

	public string m_ScenarioName;

	public Dictionary<string, bool> m_modOverride;

	public string m_CityName;

	public bool m_scenarioWon;

	public bool m_scenarioFailed;

	public uint m_newMapAppVersion;

	public uint m_newGameAppVersion;

	public uint m_saveAppVersion;

	public uint m_dataFormatVersion;

	public SimulationManager.UpdateMode m_updateMode;

	[NonSerialized]
	public MapThemeMetaData m_MapThemeMetaData;

	public enum MetaBool
	{
		Undefined,
		False,
		True
	}
}
