﻿using System;
using ColossalFramework;

public static class DistrictPolicies
{
	public const int SPECIALIZATION_POLICY_COUNT = 9;

	public const int SERVICE_POLICY_COUNT = 20;

	public const int TAXATION_POLICY_COUNT = 11;

	public const int CITYPLANNING_POLICY_COUNT = 19;

	public const int SPECIAL_POLICY_COUNT = 1;

	public const int EVENT_POLICY_COUNT = 5;

	public const DistrictPolicies.Specialization IndustrySpecialization = DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore;

	public const DistrictPolicies.Specialization CommercialSpecialization = DistrictPolicies.Specialization.Leisure | DistrictPolicies.Specialization.Tourist | DistrictPolicies.Specialization.Organic;

	public const DistrictPolicies.Specialization ResidentialSpecialization = DistrictPolicies.Specialization.Selfsufficient;

	public const DistrictPolicies.Specialization OfficeSpecialization = DistrictPolicies.Specialization.Hightech;

	public enum Types
	{
		[EnumPosition("", 0)]
		Specialization,
		[EnumPosition("", 1)]
		Services,
		[EnumPosition("", 2)]
		Taxation,
		[EnumPosition("", 3)]
		CityPlanning,
		[EnumPosition("", 4)]
		Special,
		[EnumPosition("", 5)]
		Event
	}

	public enum Policies
	{
		None = -1,
		[EnumPosition("Industry", "", 0)]
		Forest,
		[EnumPosition("Industry", "", 1)]
		Farming,
		[EnumPosition("Industry", "", 3)]
		Oil,
		[EnumPosition("Industry", "", 2)]
		Ore,
		[EnumPosition("Commercial", "", 1)]
		Leisure,
		[EnumPosition("Commercial", "", 0)]
		Tourist,
		[EnumPosition("Commercial", "", 2)]
		Organic,
		[EnumPosition("Residential", "", 0)]
		Selfsufficient,
		[EnumPosition("Office", "", 0)]
		Hightech,
		[EnumPosition("Services", "", 4)]
		PowerSaving = 32,
		[EnumPosition("Services", "", 5)]
		WaterSaving,
		[EnumPosition("Services", "", 6)]
		SmokeDetectors,
		[EnumPosition("Services", "", 11)]
		RecreationBoost,
		[EnumPosition("Services", "", 7)]
		PetBan,
		[EnumPosition("Services", "", 8)]
		Recycling,
		[EnumPosition("Services", "", 9)]
		SmokingBan,
		[EnumPosition("Services", "", 24)]
		EducationBoost,
		[EnumPosition("Services", "", 30)]
		RecreationalUse,
		[EnumPosition("Services", "", 31)]
		FreeTransport,
		[EnumPosition("Services", "", 40)]
		SchoolsOut,
		[EnumPosition("Services", "", 47)]
		DoubleSentences,
		[EnumPosition("Services", "", 48)]
		ExtraInsulation,
		[EnumPosition("Services", "", 49)]
		NoElectricity,
		[EnumPosition("Services", "", 50)]
		OnlyElectricity,
		[EnumPosition("Services", "", 57)]
		HelicopterPriority,
		[EnumPosition("Services", "", 58)]
		PreferFerries,
		[EnumPosition("Services", "", 59)]
		HighTicketPrices,
		[EnumPosition("Services", "", 60)]
		EducationalBlimps,
		[EnumPosition("Services", "", 61)]
		RecyclePlastic,
		[EnumPosition("Taxation", "", 12)]
		TaxRaiseResLow = 64,
		[EnumPosition("Taxation", "", 13)]
		TaxRaiseResHigh,
		[EnumPosition("Taxation", "", 14)]
		TaxRaiseComLow,
		[EnumPosition("Taxation", "", 15)]
		TaxRaiseComHigh,
		[EnumPosition("Taxation", "", 16)]
		TaxRaiseOffice,
		[EnumPosition("Taxation", "", 17)]
		TaxLowerResLow,
		[EnumPosition("Taxation", "", 18)]
		TaxLowerResHigh,
		[EnumPosition("Taxation", "", 19)]
		TaxLowerComLow,
		[EnumPosition("Taxation", "", 20)]
		TaxLowerComHigh,
		[EnumPosition("Taxation", "", 21)]
		TaxLowerOffice,
		[EnumPosition("Taxation", "", 43)]
		DontTaxLeisure,
		[EnumPosition("CityPlanning", "", 22)]
		SmallBusiness = 96,
		[EnumPosition("CityPlanning", "", 23)]
		BigBusiness,
		[EnumPosition("CityPlanning", "", 25)]
		IndustrySpace,
		[EnumPosition("CityPlanning", "", 26)]
		HighTechHousing,
		[EnumPosition("CityPlanning", "", 27)]
		HighriseBan,
		[EnumPosition("CityPlanning", "", 32)]
		HeavyTrafficBan,
		[EnumPosition("CityPlanning", "", 41)]
		EncourageBiking,
		[EnumPosition("CityPlanning", "", 42)]
		BikeBan,
		[EnumPosition("CityPlanning", "", 44)]
		NoLoudNoises,
		[EnumPosition("CityPlanning", "", 45)]
		OldTown,
		[EnumPosition("CityPlanning", "", 51)]
		StuddedTires,
		[EnumPosition("CityPlanning", "", 52)]
		AntiSlip,
		[EnumPosition("CityPlanning", "", 53)]
		LightningRods,
		[EnumPosition("CityPlanning", "", 55)]
		FastRecovery,
		[EnumPosition("CityPlanning", "", 56)]
		NoRebuild,
		[EnumPosition("CityPlanning", "", 54)]
		VIPArea,
		[EnumPosition("CityPlanning", "", 62)]
		CombustionEngineBan,
		[EnumPosition("CityPlanning", "", 63)]
		ElectricCars,
		[EnumPosition("CityPlanning", "", 64)]
		FilterIndustrialWaste,
		[EnumPosition("Special", "", 0)]
		Reserved = 128,
		[EnumPosition("Event", "", 0)]
		SubsidizedYouth = 160,
		[EnumPosition("Event", "", 1)]
		ComeOneComeAll,
		[EnumPosition("Event", "", 2)]
		MatchSecurity,
		[EnumPosition("Event", "", 3)]
		BandAdvertisement,
		[EnumPosition("Event", "", 4)]
		PremiumStudio
	}

	[Flags]
	public enum Specialization
	{
		None = 0,
		Forest = 1,
		Farming = 2,
		Oil = 4,
		Ore = 8,
		Leisure = 16,
		Tourist = 32,
		Organic = 64,
		Selfsufficient = 128,
		Hightech = 256
	}

	[Flags]
	public enum Services
	{
		None = 0,
		PowerSaving = 1,
		WaterSaving = 2,
		SmokeDetectors = 4,
		RecreationBoost = 8,
		PetBan = 16,
		Recycling = 32,
		SmokingBan = 64,
		EducationBoost = 128,
		RecreationalUse = 256,
		FreeTransport = 512,
		SchoolsOut = 1024,
		DoubleSentences = 2048,
		ExtraInsulation = 4096,
		NoElectricity = 8192,
		OnlyElectricity = 16384,
		HelicopterPriority = 32768,
		PreferFerries = 65536,
		HighTicketPrices = 131072,
		EducationalBlimps = 262144,
		RecyclePlastic = 524288
	}

	[Flags]
	public enum Taxation
	{
		None = 0,
		TaxRaiseResLow = 1,
		TaxRaiseResHigh = 2,
		TaxRaiseComLow = 4,
		TaxRaiseComHigh = 8,
		TaxRaiseOffice = 16,
		TaxLowerResLow = 32,
		TaxLowerResHigh = 64,
		TaxLowerComLow = 128,
		TaxLowerComHigh = 256,
		TaxLowerOffice = 512,
		DontTaxLeisure = 1024
	}

	[Flags]
	public enum CityPlanning
	{
		None = 0,
		SmallBusiness = 1,
		BigBusiness = 2,
		IndustrySpace = 4,
		HighTechHousing = 8,
		HighriseBan = 16,
		HeavyTrafficBan = 32,
		EncourageBiking = 64,
		BikeBan = 128,
		NoLoudNoises = 256,
		OldTown = 512,
		StuddedTires = 1024,
		AntiSlip = 2048,
		LightningRods = 4096,
		FastRecovery = 8192,
		NoRebuild = 16384,
		VIPArea = 32768,
		CombustionEngineBan = 65536,
		ElectricCars = 131072,
		FilterIndustrialWaste = 262144
	}

	[Flags]
	public enum Special
	{
		None = 0,
		Reserved = 1
	}

	[Flags]
	public enum Event
	{
		None = 0,
		SubsidizedYouth = 1,
		ComeOneComeAll = 2,
		MatchSecurity = 4,
		BandAdvertisement = 8,
		PremiumStudio = 16
	}
}
