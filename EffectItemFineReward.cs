﻿using System;
using ColossalFramework.UI;

public class EffectItemFineReward : EffectSubItem
{
	private void Awake()
	{
		this.m_spriteNoMoney = base.Find<UISprite>("SpriteNo");
		this.m_numberField = base.Find<UIPanel>("NumberFieldAmount").GetComponent<NumberField>();
		this.m_numberField.eventValueChanged += new NumberField.ValueChangedHandler(this.OnValueChanged);
	}

	protected override void ShowImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.CashFine)
		{
			this.m_spriteNoMoney.set_isVisible(true);
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.CashReward)
		{
			this.m_spriteNoMoney.set_isVisible(false);
		}
	}

	protected override void SetDefaultValuesImpl()
	{
		if (this.m_currentEffectType == EffectItem.EffectType.CashFine)
		{
			FeeEffect feeEffect = base.currentTriggerMilestone.AddEffect<FeeEffect>();
			feeEffect.m_moneyAmount = this.m_numberField.value;
			base.currentTriggerEffect = feeEffect;
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.CashReward)
		{
			RewardEffect rewardEffect = base.currentTriggerMilestone.AddEffect<RewardEffect>();
			rewardEffect.m_moneyAmount = this.m_numberField.value;
			base.currentTriggerEffect = rewardEffect;
		}
	}

	public void Load(FeeEffect feeEffect)
	{
		base.currentTriggerEffect = feeEffect;
		this.m_currentEffectType = EffectItem.EffectType.CashFine;
		this.m_numberField.value = feeEffect.m_moneyAmount;
		base.Show(EffectItem.EffectType.CashFine);
	}

	public void Load(RewardEffect rewardEffect)
	{
		base.currentTriggerEffect = rewardEffect;
		this.m_currentEffectType = EffectItem.EffectType.CashReward;
		this.m_numberField.value = rewardEffect.m_moneyAmount;
		base.Show(EffectItem.EffectType.CashReward);
	}

	private void OnValueChanged(int value)
	{
		if (this.m_currentEffectType == EffectItem.EffectType.CashFine)
		{
			FeeEffect feeEffect = base.currentTriggerEffect as FeeEffect;
			feeEffect.m_moneyAmount = value;
		}
		else if (this.m_currentEffectType == EffectItem.EffectType.CashReward)
		{
			RewardEffect rewardEffect = base.currentTriggerEffect as RewardEffect;
			rewardEffect.m_moneyAmount = value;
		}
	}

	protected override void HideImpl()
	{
		base.currentTriggerMilestone.RemoveEffect(base.currentTriggerEffect);
	}

	private UISprite m_spriteNoMoney;

	private NumberField m_numberField;
}
