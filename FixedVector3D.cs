﻿using System;
using ColossalFramework.IO;
using UnityEngine;

[Serializable]
public struct FixedVector3D
{
	public FixedVector3D(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void Serialize24(DataSerializer s)
	{
		s.WriteInt24(this.x);
		s.WriteInt24(this.y);
		s.WriteInt24(this.z);
	}

	public void Deserialize24(DataSerializer s)
	{
		this.x = s.ReadInt24();
		this.y = s.ReadInt24();
		this.z = s.ReadInt24();
	}

	public static explicit operator Vector3(FixedVector3D v)
	{
		return new Vector3((float)v.x * 0.0009765625f, (float)v.y * 0.0009765625f, (float)v.z * 0.0009765625f);
	}

	public static explicit operator FixedVector3D(Vector3 v)
	{
		return new FixedVector3D((int)(v.x * 1024f), (int)(v.y * 1024f), (int)(v.z * 1024f));
	}

	public static FixedVector3D operator -(FixedVector3D a, FixedVector3D b)
	{
		return new FixedVector3D(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	public static FixedVector3D operator +(FixedVector3D a, FixedVector3D b)
	{
		return new FixedVector3D(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	public override string ToString()
	{
		return string.Concat(new object[]
		{
			"(",
			this.x,
			", ",
			this.y,
			", ",
			this.z,
			")"
		});
	}

	public int x;

	public int y;

	public int z;
}
