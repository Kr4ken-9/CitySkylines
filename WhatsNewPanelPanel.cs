﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class WhatsNewPanelPanel : UICustomControl
{
	public int selectedLogoIndex
	{
		get
		{
			return this.m_selectedIndex;
		}
		set
		{
			if (this.m_selectedIndex != value)
			{
				UIComponent c = (this.m_selectedIndex < 0 || this.m_selectedIndex >= this.m_logoContainer.get_childCount()) ? null : this.m_logoContainer.get_components()[this.m_selectedIndex];
				if (c != null)
				{
					ValueAnimator.Animate("WNPLogo" + this.m_selectedIndex, delegate(float val)
					{
						if (c != null)
						{
							c.set_size(Vector2.Lerp(this.m_minSize, this.m_maxSize, val));
						}
					}, new AnimatedFloat(1f, 0f, this.m_interpSpeed));
				}
				UIComponent c2 = (value < 0 || value >= this.m_logoContainer.get_childCount()) ? null : this.m_logoContainer.get_components()[value];
				if (c2 != null)
				{
					this.m_selectedIndex = value;
					this.m_prevPanel.set_isEnabled(this.m_selectedIndex > 0);
					this.m_nextPanel.set_isEnabled(this.m_selectedIndex < this.m_logoContainer.get_childCount() - 1);
					ValueAnimator.Animate("WNPLogo" + this.m_selectedIndex, delegate(float val)
					{
						if (c2 != null)
						{
							c2.set_size(Vector2.Lerp(this.m_minSize, this.m_maxSize, val));
						}
					}, new AnimatedFloat(0f, 1f, this.m_interpSpeed));
				}
				ValueAnimator.Animate("Select", delegate(float val)
				{
					this.m_logoContainer.set_scrollPosition(new Vector2(val, 0f));
				}, new AnimatedFloat(this.m_logoContainer.get_scrollPosition().x, this.CalculatePosition(this.m_selectedIndex) - this.m_logoContainer.get_size().x * 0.5f, this.m_interpSpeed));
			}
		}
	}

	private float CalculatePosition(int index)
	{
		return (this.m_minSize.x + (float)this.m_logoContainer.get_autoLayoutPadding().get_horizontal()) * (float)index + (this.m_maxSize.x + (float)this.m_logoContainer.get_autoLayoutPadding().get_horizontal()) * 0.5f;
	}

	public void InitCarousel()
	{
		foreach (UIComponent current in this.m_logoContainer.get_components())
		{
			current.set_size(this.m_minSize);
		}
		this.m_logoContainer.get_components()[0].set_size(this.m_maxSize);
		int num = 0;
		if (this.m_selectedIndex != num)
		{
			UIComponent uIComponent = (this.m_selectedIndex < 0 || this.m_selectedIndex >= this.m_logoContainer.get_childCount()) ? null : this.m_logoContainer.get_components()[this.m_selectedIndex];
			if (uIComponent != null)
			{
				uIComponent.set_size(this.m_minSize);
			}
		}
		UIComponent uIComponent2 = (num < 0 || num >= this.m_logoContainer.get_childCount()) ? null : this.m_logoContainer.get_components()[num];
		if (uIComponent2 != null)
		{
			this.m_selectedIndex = num;
			this.m_prevPanel.set_isEnabled(this.m_selectedIndex > 0);
			this.m_nextPanel.set_isEnabled(this.m_selectedIndex < this.m_logoContainer.get_childCount() - 1);
			uIComponent2.set_size(this.m_maxSize);
			this.m_logoContainer.set_scrollPosition(new Vector2(this.CalculatePosition(this.m_selectedIndex) - this.m_logoContainer.get_size().x * 0.5f, 0f));
		}
	}

	public void OnContainerMouseWheel(UIComponent c, UIMouseEventParameter p)
	{
		this.m_accumulatedLogoScrollMouseWheelDelta += p.get_wheelDelta();
		if (this.m_accumulatedLogoScrollMouseWheelDelta <= -1f)
		{
			this.OnNextPanel();
			this.m_accumulatedLogoScrollMouseWheelDelta = 0f;
		}
		else if (this.m_accumulatedLogoScrollMouseWheelDelta >= 1f)
		{
			this.OnPrevPanel();
			this.m_accumulatedLogoScrollMouseWheelDelta = 0f;
		}
	}

	private void Awake()
	{
		this.m_dontShowAgain = base.Find<UICheckBox>("StopBotheringMe");
		base.get_component().add_eventMouseDown(delegate(UIComponent c, UIMouseEventParameter e)
		{
			this.m_hasBeenTouched = true;
		});
		this.m_tabContainer = base.Find<UITabContainer>("PanelsContainer");
		this.m_tabContainer.set_selectedIndex(0);
		this.wnpCount = this.m_tabContainer.get_components().Count;
		this.m_currentWhatsNewPanel = this.GetCurrentWhatsNewPanel();
		this.m_tabContainer.add_eventMouseWheel(new MouseEventHandler(this.OnMouseWheeled));
		this.m_logoContainer = base.Find<UIScrollablePanel>("CarouselContainer");
		this.m_logoContainer.add_eventMouseWheel(new MouseEventHandler(this.OnContainerMouseWheel));
		this.m_prevPanel = base.Find<UIButton>("PrevPanel");
		this.m_nextPanel = base.Find<UIButton>("NextPanel");
		this.InitCarousel();
	}

	private WhatsNewPanel GetCurrentWhatsNewPanel()
	{
		if (this.m_tabContainer.get_selectedIndex() == -1)
		{
			this.m_tabContainer.set_selectedIndex(0);
		}
		foreach (UIComponent current in this.m_tabContainer.get_components())
		{
			if (current.get_isVisible())
			{
				WhatsNewPanel component = current.GetComponent<WhatsNewPanel>();
				if (component == null)
				{
					Debug.LogError("Something went wrong with finding the What's New Panel.");
				}
				return component;
			}
		}
		Debug.LogError("Could not find current What's New Panel.");
		return null;
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.Hide(0.2f);
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnGotItButton();
				p.Use();
			}
			else if (p.get_keycode() == 275 || p.get_keycode() == 100)
			{
				this.m_currentWhatsNewPanel.SelectNextTab();
				p.Use();
				this.m_hasBeenTouched = true;
			}
			else if (p.get_keycode() == 276 || p.get_keycode() == 97)
			{
				this.m_currentWhatsNewPanel.SelectPreviousTab();
				p.Use();
				this.m_hasBeenTouched = true;
			}
		}
	}

	public void Show()
	{
		base.get_component().set_isVisible(true);
		ValueAnimator.Animate("WhatsNewPanelFadeIn", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f), delegate
		{
			base.get_component().Focus();
		});
	}

	public void Hide(float fadeTime = 0.2f)
	{
		ValueAnimator.Animate("WhatsNewPanelFadeOut", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, fadeTime), delegate
		{
			base.get_component().set_isVisible(false);
		});
	}

	public void OnGotItButton()
	{
		if (this.m_dontShowAgain.get_isChecked())
		{
			SavedInt savedInt = new SavedInt(Settings.latestSeenWhatsNewPanelVersion, Settings.gameSettingsFile, 0);
			savedInt.set_value(this.m_versionNumber);
		}
		this.Hide(0.2f);
	}

	public bool ShouldShow()
	{
		SavedInt savedInt = new SavedInt(Settings.latestSeenWhatsNewPanelVersion, Settings.gameSettingsFile, 0);
		return savedInt.get_value() < this.m_versionNumber;
	}

	private void AutoSelectNext()
	{
		if (base.get_component().get_isVisible() && !this.m_hasBeenTouched)
		{
			this.m_currentWhatsNewPanel.SelectNextTab();
		}
		else
		{
			base.CancelInvoke();
		}
	}

	public void OnMouseWheeled(UIComponent c, UIMouseEventParameter p)
	{
		this.m_accumulatedMouseWheelDelta += p.get_wheelDelta();
		if (this.m_accumulatedMouseWheelDelta <= -1f)
		{
			this.m_currentWhatsNewPanel.SelectNextTab();
			this.m_hasBeenTouched = true;
			this.m_accumulatedMouseWheelDelta = 0f;
		}
		else if (this.m_accumulatedMouseWheelDelta >= 1f)
		{
			this.m_currentWhatsNewPanel.SelectPreviousTab();
			this.m_hasBeenTouched = true;
			this.m_accumulatedMouseWheelDelta = 0f;
		}
	}

	public void OnNextPanel()
	{
		if (this.m_tabContainer.get_selectedIndex() >= this.wnpCount - 1)
		{
			this.m_tabContainer.set_selectedIndex(0);
		}
		else
		{
			UITabContainer expr_2F = this.m_tabContainer;
			expr_2F.set_selectedIndex(expr_2F.get_selectedIndex() + 1);
		}
		this.selectedLogoIndex = this.m_tabContainer.get_selectedIndex();
		this.m_currentWhatsNewPanel = this.GetCurrentWhatsNewPanel();
	}

	public void OnPrevPanel()
	{
		if (this.m_tabContainer.get_selectedIndex() <= 0)
		{
			this.m_tabContainer.set_selectedIndex(this.wnpCount - 1);
		}
		else
		{
			UITabContainer expr_2F = this.m_tabContainer;
			expr_2F.set_selectedIndex(expr_2F.get_selectedIndex() - 1);
		}
		this.selectedLogoIndex = this.m_tabContainer.get_selectedIndex();
		this.m_currentWhatsNewPanel = this.GetCurrentWhatsNewPanel();
	}

	public int m_versionNumber = 5;

	public float m_timePerPage = 5.5f;

	private UICheckBox m_dontShowAgain;

	private bool m_hasBeenTouched;

	private float m_accumulatedMouseWheelDelta;

	private WhatsNewPanel m_currentWhatsNewPanel;

	private UITabContainer m_tabContainer;

	private int wnpCount;

	public float m_interpSpeed = 0.18f;

	public Vector2 m_minSize = new Vector2(96f, 21f);

	public Vector2 m_maxSize = new Vector2(287f, 64f);

	private UIScrollablePanel m_logoContainer;

	private int m_selectedIndex;

	private UIButton m_prevPanel;

	private UIButton m_nextPanel;

	private float m_accumulatedLogoScrollMouseWheelDelta;
}
