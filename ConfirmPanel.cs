﻿using System;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class ConfirmPanel : MenuPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.m_Binding = base.GetComponent<BindPropertyByKey>();
	}

	public static void ShowModal(string id, UIView.ModalPoppedReturnCallback callback)
	{
		ConfirmPanel.ShowModal<ConfirmPanel>(id, callback);
	}

	public static void ShowModal(string title, string message, UIView.ModalPoppedReturnCallback callback)
	{
		ConfirmPanel.ShowModal<ConfirmPanel>(title, message, callback);
	}

	public static void ShowModal<T>(string id, UIView.ModalPoppedReturnCallback callback) where T : ConfirmPanel
	{
		T t = UIView.get_library().ShowModal<T>(typeof(T).Name, callback);
		string message = string.Empty;
		if (Locale.Exists(id, "Message"))
		{
			message = Locale.Get(id, "Message");
		}
		else
		{
			message = Locale.Get(id, "Text");
		}
		t.SetMessage(Locale.Get(id, "Title"), message);
	}

	public static void ShowModal<T>(string title, string message, UIView.ModalPoppedReturnCallback callback) where T : ConfirmPanel
	{
		T t = UIView.get_library().ShowModal<T>(typeof(T).Name, callback);
		t.SetMessage(title, message);
	}

	public virtual void SetMessage(string title, string message)
	{
		if (this.m_Binding != null)
		{
			this.m_Binding.SetProperties(TooltipHelper.Format(new string[]
			{
				"title",
				title,
				"message",
				message
			}));
		}
	}

	protected virtual void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Yes");
			uIButton.Focus();
		}
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	protected virtual void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				p.Use();
				this.OnClosed();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnYes();
			}
		}
	}

	public virtual void OnNo()
	{
		UIView.get_library().Hide(base.GetType().Name, 0);
	}

	public virtual void OnYes()
	{
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	private BindPropertyByKey m_Binding;
}
