﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class NoisePollutionInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_NoisePollutionBar = base.Find<UISlider>("NoisePollutionBar");
		this.m_NoisePollutionLabel = base.Find<UILabel>("NoisePollutionLabel");
	}

	public float noisePollution
	{
		get
		{
			if (Singleton<ImmaterialResourceManager>.get_exists())
			{
				int num;
				Singleton<ImmaterialResourceManager>.get_instance().CheckTotalResource(ImmaterialResourceManager.Resource.NoisePollution, out num);
				return (float)num;
			}
			return 0f;
		}
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[7].m_activeColorB);
					uITextureSprite.get_renderMaterial().SetColor("_ColorC", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[7].m_activeColor);
					uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.222222224f);
					uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1.1f);
					uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0f);
					this.m_NoisePollutionBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
				}
			}
			this.m_initialized = true;
		}
		this.m_NoisePollutionBar.set_value(this.noisePollution);
		this.m_NoisePollutionLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_NOISEPOLLUTION_AVERAGE"),
			" - ",
			this.m_NoisePollutionBar.get_value(),
			"%"
		}));
	}

	private UISlider m_NoisePollutionBar;

	private UILabel m_NoisePollutionLabel;

	private bool m_initialized;
}
