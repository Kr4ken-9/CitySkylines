﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class DebugOutputPanel : UICustomControl
{
	private void OnApplicationQuit()
	{
		this.isShuttingDown = true;
	}

	private void Awake()
	{
		this.m_Container = base.Find<UIScrollablePanel>("Container");
	}

	private void OnEnable()
	{
		base.get_component().add_eventSizeChanged(new PropertyChangedEventHandler<Vector2>(this.OnResizePanel));
	}

	private void OnDisable()
	{
		base.get_component().remove_eventSizeChanged(new PropertyChangedEventHandler<Vector2>(this.OnResizePanel));
	}

	private void OnResizePanel(UIComponent c, Vector2 s)
	{
		for (int i = 0; i < this.m_Container.get_components().Count; i++)
		{
			this.m_Container.get_components()[i].FitChildrenVertically();
		}
	}

	private void Update()
	{
		if (this.m_DebugOuput.IsKeyUp())
		{
			if (base.get_component().get_isVisible())
			{
				DebugOutputPanel.Hide();
			}
			else
			{
				DebugOutputPanel.Show();
			}
		}
		if (UIView.get_library() != null && UIView.get_library().Get("DebugOutputPanel") != null)
		{
			if (DebugOutputPanel.sPendingMessages.Count > 0)
			{
				this.SyncMessages();
			}
			DebugOutputPanel.sPendingMessages.Clear();
		}
	}

	private void SyncMessages()
	{
		for (int i = 0; i < DebugOutputPanel.sPendingMessages.Count; i++)
		{
			DebugOutputPanel.AddMessage(DebugOutputPanel.sPendingMessages[i].type, DebugOutputPanel.sPendingMessages[i].message, true);
		}
		DebugOutputPanel.sPendingMessages.Clear();
	}

	public static void Show()
	{
		if (UIView.get_library() != null)
		{
			UIComponent uIComponent = UIView.get_library().Show("DebugOutputPanel");
			if (uIComponent != null && UIView.HasModalInput())
			{
				uIComponent.set_zOrder(uIComponent.GetUIView().get_panelsLibraryModalEffect().get_zOrder() - 1);
			}
		}
	}

	public static void Hide()
	{
		UIView.get_library().Hide("DebugOutputPanel");
	}

	public static void AddMessage(PluginManager.MessageType type, string message)
	{
		DebugOutputPanel.AddMessage(type, message, false);
	}

	private static void AddMessage(PluginManager.MessageType type, string message, bool syncing)
	{
		if (type == null || type == 1)
		{
			DebugOutputPanel.Show();
			DebugOutputPanel debugOutputPanel = (!(UIView.get_library() != null)) ? null : UIView.get_library().Get<DebugOutputPanel>("DebugOutputPanel");
			if (debugOutputPanel != null)
			{
				debugOutputPanel.AddMessageInternal(type, message);
			}
			else if (!syncing)
			{
				DebugOutputPanel.sPendingMessages.Add(new DebugOutputPanel.Message
				{
					type = type,
					message = message
				});
			}
		}
		else
		{
			DebugOutputPanel debugOutputPanel2 = (!(UIView.get_library() != null)) ? null : UIView.get_library().Get<DebugOutputPanel>("DebugOutputPanel");
			if (debugOutputPanel2 != null)
			{
				debugOutputPanel2.AddMessageInternal(type, message);
			}
			else if (!syncing)
			{
				DebugOutputPanel.sPendingMessages.Add(new DebugOutputPanel.Message
				{
					type = type,
					message = message
				});
			}
		}
	}

	private void AddMessageInternal(PluginManager.MessageType type, string message)
	{
		if (!this.isShuttingDown)
		{
			UIComponent uIComponent = this.m_Container.AttachUIComponent(UITemplateManager.GetAsGameObject(DebugOutputPanel.kDebugEntryTemplate));
			uIComponent.Find<UISprite>("Image").set_spriteName((type != null) ? ((type != 1) ? "IconMessage" : "IconWarning") : "IconError");
			uIComponent.Find<UILabel>("Message").set_text(message);
			uIComponent.FitChildrenVertically();
			this.m_Container.ScrollToBottom();
		}
	}

	public void OnClear()
	{
		UITemplateManager.ClearInstances(DebugOutputPanel.kDebugEntryTemplate);
	}

	public void OnClose()
	{
		DebugOutputPanel.Hide();
	}

	private static readonly string kDebugEntryTemplate = "DebugEntryTemplate";

	private SavedInputKey m_DebugOuput = new SavedInputKey(Settings.debugOuput, Settings.inputSettingsFile, DefaultSettings.debugOuput, true);

	private static List<DebugOutputPanel.Message> sPendingMessages = new List<DebugOutputPanel.Message>();

	private UIScrollablePanel m_Container;

	private bool isShuttingDown;

	private struct Message
	{
		public PluginManager.MessageType type;

		public string message;
	}
}
