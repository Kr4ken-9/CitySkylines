﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class RoadEditorPanel : UICustomControl
{
	public event RoadEditorPanel.ObjectModifiedHandler EventObjectModified
	{
		add
		{
			RoadEditorPanel.ObjectModifiedHandler objectModifiedHandler = this.EventObjectModified;
			RoadEditorPanel.ObjectModifiedHandler objectModifiedHandler2;
			do
			{
				objectModifiedHandler2 = objectModifiedHandler;
				objectModifiedHandler = Interlocked.CompareExchange<RoadEditorPanel.ObjectModifiedHandler>(ref this.EventObjectModified, (RoadEditorPanel.ObjectModifiedHandler)Delegate.Combine(objectModifiedHandler2, value), objectModifiedHandler);
			}
			while (objectModifiedHandler != objectModifiedHandler2);
		}
		remove
		{
			RoadEditorPanel.ObjectModifiedHandler objectModifiedHandler = this.EventObjectModified;
			RoadEditorPanel.ObjectModifiedHandler objectModifiedHandler2;
			do
			{
				objectModifiedHandler2 = objectModifiedHandler;
				objectModifiedHandler = Interlocked.CompareExchange<RoadEditorPanel.ObjectModifiedHandler>(ref this.EventObjectModified, (RoadEditorPanel.ObjectModifiedHandler)Delegate.Remove(objectModifiedHandler2, value), objectModifiedHandler);
			}
			while (objectModifiedHandler != objectModifiedHandler2);
		}
	}

	public event RoadEditorPanel.PanelClosedHandler EventPanelClosed
	{
		add
		{
			RoadEditorPanel.PanelClosedHandler panelClosedHandler = this.EventPanelClosed;
			RoadEditorPanel.PanelClosedHandler panelClosedHandler2;
			do
			{
				panelClosedHandler2 = panelClosedHandler;
				panelClosedHandler = Interlocked.CompareExchange<RoadEditorPanel.PanelClosedHandler>(ref this.EventPanelClosed, (RoadEditorPanel.PanelClosedHandler)Delegate.Combine(panelClosedHandler2, value), panelClosedHandler);
			}
			while (panelClosedHandler != panelClosedHandler2);
		}
		remove
		{
			RoadEditorPanel.PanelClosedHandler panelClosedHandler = this.EventPanelClosed;
			RoadEditorPanel.PanelClosedHandler panelClosedHandler2;
			do
			{
				panelClosedHandler2 = panelClosedHandler;
				panelClosedHandler = Interlocked.CompareExchange<RoadEditorPanel.PanelClosedHandler>(ref this.EventPanelClosed, (RoadEditorPanel.PanelClosedHandler)Delegate.Remove(panelClosedHandler2, value), panelClosedHandler);
			}
			while (panelClosedHandler != panelClosedHandler2);
		}
	}

	public RoadEditorMainPanel owner
	{
		get
		{
			return this.m_Owner;
		}
		set
		{
			this.m_Owner = value;
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		this.ResetToggleColors();
		if (!visible && this.m_SidePanel != null)
		{
			this.DestroySidePanel();
		}
	}

	public void Close(UIComponent c = null, UIMouseEventParameter e = null)
	{
		this.Clear();
		base.get_component().Hide();
		if (this.EventPanelClosed != null)
		{
			this.EventPanelClosed(this);
		}
	}

	public void Clear()
	{
		this.m_Target = null;
		IList<UIComponent> components = this.m_Container.get_components();
		while (this.m_Container.get_childCount() > 0)
		{
			UIComponent uIComponent = this.m_Container.get_components()[0];
			this.m_Container.RemoveUIComponent(uIComponent);
			Object.Destroy(uIComponent.get_gameObject());
		}
		this.DestroySidePanel();
	}

	public void Refresh()
	{
		foreach (UIComponent current in this.m_Container.get_components())
		{
			RoadEditorCollapsiblePanel component = current.GetComponent<RoadEditorCollapsiblePanel>();
			if (component != null)
			{
				foreach (UIComponent current2 in component.Container.get_components())
				{
					RoadEditorDynamicPropertyToggle component2 = current2.GetComponent<RoadEditorDynamicPropertyToggle>();
					if (component2 != null)
					{
						component2.Refresh();
					}
				}
			}
		}
	}

	public void Initialize(object obj)
	{
		this.Clear();
		if (obj != null)
		{
			this.m_Target = obj;
			this.RefreshCaption();
			FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
			FieldInfo[] array = fields;
			for (int i = 0; i < array.Length; i++)
			{
				FieldInfo fieldInfo = array[i];
				if (fieldInfo.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true).Length > 0)
				{
					this.CreateField(fieldInfo, this.m_Target);
				}
			}
			if (this.m_Target is NetInfo)
			{
				PrefabAI aI = ((NetInfo)this.m_Target).GetAI();
				if (aI != null)
				{
					FieldInfo[] fields2 = aI.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
					FieldInfo[] array2 = fields2;
					for (int j = 0; j < array2.Length; j++)
					{
						FieldInfo fieldInfo2 = array2[j];
						if (fieldInfo2.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true).Length > 0)
						{
							this.CreateField(fieldInfo2, aI);
						}
					}
				}
			}
			this.AddCustomFields();
		}
	}

	private void RefreshCaption()
	{
		if (this.m_Caption != null)
		{
			if (this.m_Target is NetInfo)
			{
				this.m_Caption.set_text("Road Properties");
			}
			else if (this.m_Target is NetInfo.Lane)
			{
				this.m_Caption.set_text("Lane Properties");
			}
			else if (this.m_Target is NetInfo.Segment)
			{
				this.m_Caption.set_text("Segment Properties");
			}
			else if (this.m_Target is NetInfo.Node)
			{
				this.m_Caption.set_text("Node Properties");
			}
			else if (this.m_Target is NetLaneProps.Prop)
			{
				this.m_Caption.set_text("Lane Prop Properties");
			}
		}
	}

	private void AddCustomFields()
	{
		Type type = this.m_Target.GetType();
		if (type == typeof(NetInfo.Segment))
		{
			this.AddModelImportField(true);
		}
		else if (type == typeof(NetInfo.Node))
		{
			this.AddModelImportField(false);
		}
		else if (type == typeof(NetInfo.Lane))
		{
			this.AddLanePropFields();
		}
		else if (type == typeof(NetLaneProps.Prop))
		{
			this.AddLanePropSelectField();
		}
		else if (type == typeof(NetInfo))
		{
			PrefabAI aI = ((NetInfo)this.m_Target).GetAI();
			if (aI != null)
			{
				Type type2 = aI.GetType();
				if (type2 == typeof(PedestrianBridgeAI))
				{
					this.AddPillarField(aI, "Pillar", "m_bridgePillarInfo");
				}
				else if (type2 == typeof(RoadBridgeAI) || type2 == typeof(TrainTrackBridgeAI))
				{
					this.AddPillarField(aI, "Pillar", "m_bridgePillarInfo");
					this.AddPillarField(aI, "Middle pillar", "m_middlePillarInfo");
				}
				else if (type2 == typeof(MonorailTrackAI))
				{
					this.AddPillarField(aI, "Default pillar", "m_bridgePillarInfo");
					this.AddPillarField(aI, "Corner pillar", "m_bridgePillarInfo2");
					this.AddPillarField(aI, "Intersection pillar", "m_bridgePillarInfo3");
					this.AddPillarField(aI, "Middle pillar", "m_middlePillarInfo");
				}
			}
		}
	}

	private void AddPillarField(PrefabAI ai, string label, string field)
	{
		RERefSet rERefSet = UITemplateManager.Get<RERefSet>(RoadEditorPanel.kRefSet);
		RoadEditorCollapsiblePanel groupPanel = this.GetGroupPanel("Properties");
		groupPanel.Container.AttachUIComponent(rERefSet.get_gameObject());
		this.FitToContainer(rERefSet.get_component());
		rERefSet.Initialize(label, ai, ai.GetType().GetField(field), AssetImporterAssetTemplate.Filter.Pillars);
		rERefSet.EventReferenceSelected += new RERefSet.ReferenceSelectedHandler(this.OnObjectModified);
	}

	private void AddModelImportField(bool showColorField = true)
	{
		REModelImport rEModelImport = UITemplateManager.Get<REModelImport>(RoadEditorPanel.kModelImport);
		this.m_Container.AttachUIComponent(rEModelImport.get_gameObject());
		this.FitToContainer(rEModelImport.get_component());
		rEModelImport.Initialize("Model", this.m_Target, showColorField);
		rEModelImport.EventModelChanged += new REModelImport.ModelChangedHandler(this.OnObjectModified);
	}

	private void AddLanePropFields()
	{
		FieldInfo field = typeof(NetLaneProps).GetField("m_props");
		string name = this.GetCustomizablePropertyAttribute(field).name;
		this.CreateArrayField(name, field, ((NetInfo.Lane)this.m_Target).m_laneProps);
	}

	private void AddLanePropSelectField()
	{
		RERefSet rERefSet = UITemplateManager.Get<RERefSet>(RoadEditorPanel.kRefSet);
		this.m_Container.AttachUIComponent(rERefSet.get_gameObject());
		this.FitToContainer(rERefSet.get_component());
		rERefSet.Initialize("Prop", this.m_Target, null, AssetImporterAssetTemplate.Filter.LaneProps);
		rERefSet.EventReferenceSelected += new RERefSet.ReferenceSelectedHandler(this.OnObjectModified);
	}

	private void CreateField(FieldInfo field, object target)
	{
		CustomizablePropertyAttribute customizablePropertyAttribute = this.GetCustomizablePropertyAttribute(field);
		if (customizablePropertyAttribute != null)
		{
			Type fieldType = field.FieldType;
			if (fieldType.IsArray)
			{
				this.CreateArrayField(customizablePropertyAttribute.name, field, target);
			}
			else
			{
				this.CreateGenericField(customizablePropertyAttribute.group, field, target);
			}
		}
	}

	private void CreateArrayField(string name, FieldInfo field, object target)
	{
		if (target == null)
		{
			target = this.m_Target;
		}
		RoadEditorCollapsiblePanel groupPanel = this.GetGroupPanel(name);
		RoadEditorAddButton roadEditorAddButton = UITemplateManager.Get<RoadEditorAddButton>(RoadEditorPanel.kAddButton);
		roadEditorAddButton.target = target;
		roadEditorAddButton.field = field;
		groupPanel.AddComponent(roadEditorAddButton.get_component(), true);
		roadEditorAddButton.EventObjectAdded += new RoadEditorAddButton.ObjectAddedHandler(this.OnAddArrayElement);
		CustomizablePropertyAttribute customizablePropertyAttribute = this.GetCustomizablePropertyAttribute(field);
		Array array = (Array)field.GetValue(target);
		string group = customizablePropertyAttribute.group;
		IEnumerator enumerator = array.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object current = enumerator.Current;
				if (current != null && this.CheckValid(current))
				{
					this.AddToArrayField(groupPanel, current, field, target);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	private bool CheckValid(object obj)
	{
		if (obj is NetInfo.Segment)
		{
			NetInfo.Segment segment = (NetInfo.Segment)obj;
			if (segment.m_material != null && segment.m_material.get_shader().get_name() == "Custom/Net/Metro")
			{
				return false;
			}
		}
		if (obj is NetInfo.Node)
		{
			NetInfo.Node node = (NetInfo.Node)obj;
			if (node.m_material != null && node.m_material.get_shader() != null && node.m_material.get_shader().get_name() == "Custom/Net/Metro")
			{
				return false;
			}
		}
		return true;
	}

	private void AddToArrayField(RoadEditorCollapsiblePanel panel, object element, FieldInfo field, object targetObject)
	{
		RoadEditorDynamicPropertyToggle roadEditorDynamicPropertyToggle = UITemplateManager.Get<RoadEditorDynamicPropertyToggle>(RoadEditorPanel.kToggle);
		panel.AddComponent(roadEditorDynamicPropertyToggle.get_component(), false);
		this.FitToContainer(roadEditorDynamicPropertyToggle.get_component());
		roadEditorDynamicPropertyToggle.targetElement = element;
		roadEditorDynamicPropertyToggle.field = field;
		roadEditorDynamicPropertyToggle.targetObject = ((targetObject == null) ? this.m_Target : targetObject);
		roadEditorDynamicPropertyToggle.EventDelete += new RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle(this.OnDeleteArrayElement);
		roadEditorDynamicPropertyToggle.EventToggle += new RoadEditorDynamicPropertyToggle.DynamicPropertyToggleHandle(this.OnToggle);
	}

	private void CreateGenericField(string groupName, FieldInfo field, object target)
	{
		UIComponent container = this.m_Container;
		if (!string.IsNullOrEmpty(groupName))
		{
			container = this.GetGroupPanel(groupName).Container;
		}
		REPropertySet rEPropertySet = this.CreateFieldComponent(field);
		if (rEPropertySet != null)
		{
			container.AttachUIComponent(rEPropertySet.get_gameObject());
			this.FitToContainer(rEPropertySet.get_component());
			rEPropertySet.SetTarget(target, field);
			rEPropertySet.EventPropertyChanged += new REPropertySet.PropertyChangedHandler(this.OnObjectModified);
		}
	}

	private REPropertySet CreateFieldComponent(FieldInfo field)
	{
		Type fieldType = field.FieldType;
		string text = null;
		if (fieldType == typeof(bool))
		{
			text = RoadEditorPanel.kBoolSet;
		}
		else if (fieldType == typeof(float))
		{
			text = RoadEditorPanel.kFloatSet;
		}
		else if (fieldType == typeof(int))
		{
			text = RoadEditorPanel.kIntSet;
		}
		else if (fieldType.IsEnum)
		{
			if (field.GetCustomAttributes(typeof(BitMaskAttribute), true).Length != 0)
			{
				text = RoadEditorPanel.kEnumBitmaskSet;
			}
			else
			{
				text = RoadEditorPanel.kEnumSet;
			}
		}
		else if (fieldType == typeof(bool))
		{
			text = RoadEditorPanel.kBoolSet;
		}
		else if (fieldType == typeof(Vector3))
		{
			text = RoadEditorPanel.kVectorSet;
		}
		if (text != null)
		{
			return UITemplateManager.Get<REPropertySet>(text);
		}
		return null;
	}

	private RoadEditorCollapsiblePanel GetGroupPanel(string name)
	{
		foreach (UIComponent current in this.m_Container.get_components())
		{
			RoadEditorCollapsiblePanel component = current.GetComponent<RoadEditorCollapsiblePanel>();
			if (component != null && component.LabelButton.get_text() == name)
			{
				return component;
			}
		}
		RoadEditorCollapsiblePanel roadEditorCollapsiblePanel = UITemplateManager.Get<RoadEditorCollapsiblePanel>(RoadEditorPanel.kCollapsiblePanel);
		this.m_Container.AttachUIComponent(roadEditorCollapsiblePanel.get_gameObject());
		this.FitToContainer(roadEditorCollapsiblePanel.get_component());
		roadEditorCollapsiblePanel.LabelButton.set_text(name);
		return roadEditorCollapsiblePanel;
	}

	private CustomizablePropertyAttribute GetCustomizablePropertyAttribute(FieldInfo field)
	{
		object[] customAttributes = field.GetCustomAttributes(typeof(CustomizablePropertyAttribute), true);
		if (customAttributes != null && customAttributes.Length > 0)
		{
			return (CustomizablePropertyAttribute)customAttributes[0];
		}
		return null;
	}

	private void OnAddArrayElement(RoadEditorAddButton button, object obj)
	{
		FieldInfo field = button.field;
		CustomizablePropertyAttribute customizablePropertyAttribute = this.GetCustomizablePropertyAttribute(field);
		RoadEditorCollapsiblePanel groupPanel = this.GetGroupPanel(customizablePropertyAttribute.name);
		if (groupPanel != null)
		{
			this.AddToArrayField(groupPanel, obj, field, button.target);
		}
		this.OnObjectModified();
	}

	private void OnDeleteArrayElement(RoadEditorDynamicPropertyToggle toggle)
	{
		if (this.m_SidePanel != null && this.m_SidePanel.m_Target == toggle.targetElement)
		{
			this.DestroySidePanel();
		}
		AssetEditorRoadUtils.RemoveArrayElement(toggle.targetElement, toggle.targetObject, toggle.field);
		toggle.get_component().get_parent().RemoveUIComponent(toggle.get_component());
		Object.Destroy(toggle.get_gameObject());
		this.OnObjectModified();
	}

	private void OnToggle(RoadEditorDynamicPropertyToggle toggle)
	{
		this.ResetToggleColors();
		if (this.m_SidePanel != null)
		{
			if (this.m_SidePanel.m_Target == toggle.targetElement)
			{
				this.DestroySidePanel();
			}
			else
			{
				this.m_SidePanel.Initialize(toggle.targetElement);
				toggle.ToggleColor(true);
			}
		}
		else
		{
			this.m_SidePanel = UITemplateManager.Get<RoadEditorPanel>(RoadEditorPanel.kSidePanel);
			base.get_component().GetUIView().AttachUIComponent(this.m_SidePanel.get_gameObject());
			this.m_SidePanel.get_gameObject().SetActive(true);
			this.m_SidePanel.Initialize(toggle.targetElement);
			this.m_SidePanel.EventPanelClosed += new RoadEditorPanel.PanelClosedHandler(this.OnSidePanelClosed);
			this.m_SidePanel.EventObjectModified += new RoadEditorPanel.ObjectModifiedHandler(this.OnObjectModified);
			this.DockToThis(this.m_SidePanel.get_component());
			toggle.ToggleColor(true);
		}
	}

	private void ResetToggleColors()
	{
		foreach (UIComponent current in this.m_Container.get_components())
		{
			RoadEditorCollapsiblePanel component = current.GetComponent<RoadEditorCollapsiblePanel>();
			if (component != null)
			{
				foreach (UIComponent current2 in component.Container.get_components())
				{
					RoadEditorDynamicPropertyToggle component2 = current2.GetComponent<RoadEditorDynamicPropertyToggle>();
					if (component2 != null)
					{
						component2.ToggleColor(false);
					}
				}
			}
		}
	}

	private void OnSidePanelClosed(RoadEditorPanel panel)
	{
		this.ResetToggleColors();
		if (this.m_SidePanel == panel)
		{
			this.DestroySidePanel();
		}
	}

	private void DestroySidePanel()
	{
		if (this.m_SidePanel != null)
		{
			this.m_SidePanel.Clear();
			Object.Destroy(this.m_SidePanel.get_gameObject());
			this.m_SidePanel = null;
		}
	}

	private void DockToThis(UIComponent comp)
	{
		UIComponent uIComponent = (!(this.owner != null)) ? base.get_component() : this.owner.get_component();
		comp.set_size(new Vector2(uIComponent.get_size().x, uIComponent.get_size().y));
		Vector3 vector;
		vector..ctor(comp.get_size().x + 8f, 0f, 0f);
		if (uIComponent.get_absolutePosition().x > vector.x)
		{
			vector = -vector;
		}
		comp.set_absolutePosition(uIComponent.get_absolutePosition() + vector);
	}

	private void FitToContainer(UIComponent comp)
	{
		float num = 0f;
		UIPanel uIPanel = comp.get_parent() as UIPanel;
		if (uIPanel != null)
		{
			num = (float)uIPanel.get_padding().get_horizontal();
		}
		UIScrollablePanel uIScrollablePanel = comp.get_parent() as UIScrollablePanel;
		if (uIScrollablePanel != null)
		{
			num = (float)uIScrollablePanel.get_scrollPadding().get_horizontal();
		}
		comp.set_size(new Vector2(comp.get_parent().get_size().x - num, comp.get_size().y));
	}

	private void OnObjectModified()
	{
		this.Refresh();
		if (this.EventObjectModified != null)
		{
			this.EventObjectModified();
		}
	}

	public static readonly string kCollapsiblePanel = "RoadEditorCollapsiblePanel";

	public static readonly string kToggle = "RoadEditorDynamicPropertyToggle";

	public static readonly string kAddButton = "REAddButton";

	public static readonly string kBoolSet = "REBoolSet";

	public static readonly string kFloatSet = "REFloatSet";

	public static readonly string kEnumBitmaskSet = "REEnumBitmaskSet";

	public static readonly string kEnumSet = "REEnumSet";

	public static readonly string kModelImport = "REModelImport";

	public static readonly string kRefSet = "RERefSet";

	public static readonly string kIntSet = "REIntSet";

	public static readonly string kVectorSet = "REVectorSet";

	public static readonly string kSidePanel = "RoadEditorSidePanel";

	public UIScrollablePanel m_Container;

	public UILabel m_Caption;

	private RoadEditorPanel m_SidePanel;

	private object m_Target;

	private RoadEditorMainPanel m_Owner;

	public delegate void ObjectModifiedHandler();

	public delegate void PanelClosedHandler(RoadEditorPanel panel);
}
