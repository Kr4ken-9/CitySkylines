﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CableCarBaseAI : VehicleAI
{
	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				data.m_flags &= ~Vehicle.Flags.Arriving;
				this.PathfindSuccess(vehicleID, ref data);
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(vehicleID, ref data);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		Vector3 lastFramePosition = data.GetLastFramePosition();
		int lodPhysics;
		if (Vector3.SqrMagnitude(physicsLodRefPos - lastFramePosition) >= 1210000f)
		{
			lodPhysics = 2;
		}
		else if (Vector3.SqrMagnitude(Singleton<SimulationManager>.get_instance().m_simulationView.m_position - lastFramePosition) >= 250000f)
		{
			lodPhysics = 1;
		}
		else
		{
			lodPhysics = 0;
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, lodPhysics);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, lodPhysics);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(this.m_info.m_class.m_service);
		int num3 = (privateServiceIndex == -1) ? 150 : 100;
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 && data.m_cargoParent == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
		else if ((int)data.m_blockCounter == num3)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		data.Unspawn(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float acceleration = this.m_info.m_acceleration;
		float braking = this.m_info.m_braking;
		float magnitude = frameData.m_velocity.get_magnitude();
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float sqrMagnitude = vector.get_sqrMagnitude();
		float num = (magnitude + acceleration) * (0.5f + 0.5f * (magnitude + acceleration) / braking) + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		float num2 = Mathf.Max(magnitude + acceleration, 5f);
		if (lodPhysics >= 2 && (ulong)(currentFrameIndex >> 4 & 3u) == (ulong)((long)(vehicleID & 3)))
		{
			num2 *= 2f;
		}
		float num3 = Mathf.Max((num - num2) / 3f, 1f);
		float num4 = num2 * num2;
		float num5 = num3 * num3;
		int i = 0;
		bool flag = false;
		if ((sqrMagnitude < num4 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				base.UpdatePathTargetPositions(vehicleID, ref vehicleData, frameData.m_position, ref i, 4, num4, num5);
				if ((leaderData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
				{
					frameData = vehicleData.m_frame0;
					return;
				}
			}
			if ((leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				while (i < 4)
				{
					float minSqrDistance;
					Vector3 refPos;
					if (i == 0)
					{
						minSqrDistance = num4;
						refPos = frameData.m_position;
						flag = true;
					}
					else
					{
						minSqrDistance = num5;
						refPos = vehicleData.GetTargetPos(i - 1);
					}
					int num6 = i;
					this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, refPos, leaderID, ref leaderData, ref i, minSqrDistance);
					if (i == num6)
					{
						break;
					}
				}
				if (i != 0)
				{
					Vector4 targetPos = vehicleData.GetTargetPos(i - 1);
					while (i < 4)
					{
						vehicleData.SetTargetPos(i++, targetPos);
					}
				}
			}
			vector = vehicleData.m_targetPos0 - frameData.m_position;
			sqrMagnitude = vector.get_sqrMagnitude();
		}
		if (leaderData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = leaderData.m_pathPositionIndex;
			byte lastPathOffset = leaderData.m_lastPathOffset;
			if (b == 255)
			{
				b = 0;
			}
			float num7 = 1f + leaderData.CalculateTotalLength(leaderID);
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetPosition(b >> 1, out pathPos))
			{
				if ((instance.m_segments.m_buffer[(int)pathPos.m_segment].m_flags & NetSegment.Flags.Flooded) != NetSegment.Flags.None && Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(frameData.m_position)))
				{
					leaderData.m_flags2 |= Vehicle.Flags2.Floating;
				}
				instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(num7 * 2.5f), this.GetNoiseLevel());
				bool flag2 = false;
				if ((b & 1) == 0 || lastPathOffset == 0)
				{
					uint laneID = PathManager.GetLaneID(pathPos);
					if (laneID != 0u)
					{
						Vector3 vector2 = instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
						float num8 = 0.5f * magnitude * magnitude / this.m_info.m_braking + this.m_info.m_generatedInfo.m_size.z * 0.5f;
						if (Vector3.Distance(frameData.m_position, vector2) >= num8 - 1f)
						{
							instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(num7);
							flag2 = true;
						}
					}
				}
				if (!flag2 && instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetNextPosition(b >> 1, out pathPos))
				{
					uint laneID2 = PathManager.GetLaneID(pathPos);
					if (laneID2 != 0u)
					{
						instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(num7);
					}
				}
			}
		}
		float num9;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			num9 = 0f;
		}
		else
		{
			num9 = vehicleData.m_targetPos0.w;
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector = quaternion * vector;
		Vector3 vector3 = quaternion * frameData.m_velocity;
		Vector3 vector4 = Vector3.get_forward();
		Vector3 vector5 = Vector3.get_zero();
		Vector3 zero = Vector3.get_zero();
		float num10 = 0f;
		float num11 = 0f;
		bool flag3 = false;
		float num12 = 0f;
		if (sqrMagnitude > 1f)
		{
			vector4 = VectorUtils.NormalizeXZ(vector, ref num12);
			if (num12 > 1f)
			{
				Vector3 vector6 = vector;
				num2 = Mathf.Max(magnitude, 2f);
				num4 = num2 * num2;
				if (sqrMagnitude > num4)
				{
					vector6 *= num2 / Mathf.Sqrt(sqrMagnitude);
				}
				bool flag4 = false;
				if (vector6.z < Mathf.Abs(vector6.x))
				{
					if (vector6.z < 0f)
					{
						flag4 = true;
					}
					float num13 = Mathf.Abs(vector6.x);
					if (num13 < 1f)
					{
						vector6.x = Mathf.Sign(vector6.x);
						if (vector6.x == 0f)
						{
							vector6.x = 1f;
						}
						num13 = 1f;
					}
					vector6.z = num13;
				}
				float num14;
				vector4 = VectorUtils.NormalizeXZ(vector6, ref num14);
				num12 = Mathf.Min(num12, num14);
				float num15 = 1.57079637f * (1f - vector4.z);
				if (num12 > 1f)
				{
					num15 /= num12;
				}
				float num16 = num12;
				if (vehicleData.m_targetPos0.w < 0.1f)
				{
					num9 = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num15);
					num9 = Mathf.Min(num9, CableCarBaseAI.CalculateMaxSpeed(num16, Mathf.Min(vehicleData.m_targetPos0.w, vehicleData.m_targetPos1.w), braking * 0.9f));
				}
				else
				{
					num9 = Mathf.Min(num9, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num15));
					num9 = Mathf.Min(num9, CableCarBaseAI.CalculateMaxSpeed(num16, vehicleData.m_targetPos1.w, braking * 0.9f));
				}
				num16 += VectorUtils.LengthXZ(vehicleData.m_targetPos1 - vehicleData.m_targetPos0);
				num9 = Mathf.Min(num9, CableCarBaseAI.CalculateMaxSpeed(num16, vehicleData.m_targetPos2.w, braking * 0.9f));
				num16 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
				num9 = Mathf.Min(num9, CableCarBaseAI.CalculateMaxSpeed(num16, vehicleData.m_targetPos3.w, braking * 0.9f));
				num16 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
				if (vehicleData.m_targetPos3.w < 0.01f)
				{
					num16 = Mathf.Max(0f, num16 - this.m_info.m_generatedInfo.m_size.z * 0.5f);
				}
				num9 = Mathf.Min(num9, CableCarBaseAI.CalculateMaxSpeed(num16, 0f, braking * 0.9f));
				if (!CableCarBaseAI.DisableCollisionCheck(leaderID, ref leaderData))
				{
					CableCarBaseAI.CheckOtherVehicles(vehicleID, ref vehicleData, ref frameData, ref num9, ref flag3, ref zero, num, braking * 0.9f, lodPhysics);
				}
				if (flag4)
				{
					num9 = -num9;
				}
				if (num9 < magnitude)
				{
					float num17 = Mathf.Max(acceleration, Mathf.Min(braking, magnitude));
					num10 = Mathf.Max(num9, magnitude - num17);
				}
				else
				{
					float num18 = Mathf.Max(acceleration, Mathf.Min(braking, -magnitude));
					num10 = Mathf.Min(num9, magnitude + num18);
				}
			}
		}
		else if (magnitude < 0.1f && flag && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			if (leaderID == vehicleID)
			{
				frameData = leaderData.m_frame0;
			}
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num9 < 0.1f)
		{
			flag3 = true;
		}
		if (flag3)
		{
			vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		}
		else
		{
			vehicleData.m_blockCounter = 0;
		}
		if (num12 > 1f)
		{
			num11 = Mathf.Asin(vector4.x) * Mathf.Sign(num10);
			vector5 = Vector3.ClampMagnitude(vector4 * num10, this.m_info.m_maxSpeed);
		}
		else
		{
			num10 = 0f;
			Vector3 vector7 = Vector3.ClampMagnitude(vector * 0.5f - vector3, braking);
			vector5 = Vector3.ClampMagnitude(vector3 + vector7, this.m_info.m_maxSpeed);
		}
		bool flag5 = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		Vector3 vector8 = vector5 - vector3;
		Vector3 vector9 = frameData.m_rotation * vector5;
		frameData.m_velocity = vector9 + zero;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector8 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = num11;
		frameData.m_travelDistance += vector5.z;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = ((vector8.z >= -0.1f) ? 0.5f : 5f);
		frameData.m_lightIntensity.z = ((num11 >= -0.1f || !flag5) ? 0f : 5f);
		frameData.m_lightIntensity.w = ((num11 <= 0.1f || !flag5) ? 0f : 5f);
		frameData.m_underground = ((vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
		frameData.m_transition = ((vehicleData.m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
		if ((vehicleData.m_flags & Vehicle.Flags.Parking) != (Vehicle.Flags)0 && num12 <= 1f && flag)
		{
			Vector3 vector10 = vehicleData.m_targetPos1 - vehicleData.m_targetPos0;
			if (vector10.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector10);
			}
		}
		else if (num10 > 0.1f)
		{
			if (vector9.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector9);
			}
		}
		else if (num10 < -0.1f && vector9.get_sqrMagnitude() > 0.01f)
		{
			frameData.m_rotation = Quaternion.LookRotation(-vector9);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	private static bool DisableCollisionCheck(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0)
		{
			float num = Mathf.Max(Mathf.Abs(vehicleData.m_targetPos3.x), Mathf.Abs(vehicleData.m_targetPos3.z));
			float num2 = 8640f;
			if (num > num2 - 100f)
			{
				return true;
			}
		}
		return false;
	}

	protected Vector4 CalculateTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float speed)
	{
		Vector3 vector = targetPos - refPos;
		float sqrMagnitude = vector.get_sqrMagnitude();
		Vector4 result;
		if (sqrMagnitude > maxSqrDistance)
		{
			result = refPos + vector * Mathf.Sqrt(maxSqrDistance / sqrMagnitude);
		}
		else
		{
			result = targetPos;
		}
		result.w = speed;
		return result;
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public static void CheckOtherVehicles(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxDistance, float maxBraking, int lodPhysics)
	{
		Vector3 vector = vehicleData.m_targetPos3 - frameData.m_position;
		Vector3 vector2 = frameData.m_position + Vector3.ClampMagnitude(vector, maxDistance);
		Vector3 min = Vector3.Min(vehicleData.m_segment.Min(), vector2);
		Vector3 max = Vector3.Max(vehicleData.m_segment.Max(), vector2);
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((min.x - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((min.z - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((max.x + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((max.z + 10f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = CableCarBaseAI.CheckOtherVehicle(vehicleID, ref vehicleData, ref frameData, ref maxSpeed, ref blocked, ref collisionPush, maxBraking, num5, ref instance.m_vehicles.m_buffer[(int)num5], min, max, lodPhysics);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (lodPhysics == 0)
		{
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			float num7 = 0f;
			Vector3 vector3 = vehicleData.m_segment.b;
			Vector3 vector4 = vehicleData.m_segment.b - vehicleData.m_segment.a;
			for (int k = 0; k < 4; k++)
			{
				Vector3 vector5 = vehicleData.GetTargetPos(k);
				Vector3 vector6 = vector5 - vector3;
				if (Vector3.Dot(vector4, vector6) > 0f)
				{
					float magnitude = vector6.get_magnitude();
					if (magnitude > 0.01f)
					{
						Segment3 segment;
						segment..ctor(vector3, vector5);
						min = segment.Min();
						max = segment.Max();
						int num8 = Mathf.Max((int)((min.x - 3f) / 8f + 1080f), 0);
						int num9 = Mathf.Max((int)((min.z - 3f) / 8f + 1080f), 0);
						int num10 = Mathf.Min((int)((max.x + 3f) / 8f + 1080f), 2159);
						int num11 = Mathf.Min((int)((max.z + 3f) / 8f + 1080f), 2159);
						for (int l = num9; l <= num11; l++)
						{
							for (int m = num8; m <= num10; m++)
							{
								ushort num12 = instance2.m_citizenGrid[l * 2160 + m];
								int num13 = 0;
								while (num12 != 0)
								{
									num12 = CableCarBaseAI.CheckCitizen(vehicleID, ref vehicleData, segment, num7, magnitude, ref maxSpeed, ref blocked, maxBraking, num12, ref instance2.m_instances.m_buffer[(int)num12], min, max);
									if (++num13 > 65536)
									{
										CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
										break;
									}
								}
							}
						}
					}
					vector4 = vector6;
					num7 += magnitude;
					vector3 = vector5;
				}
			}
		}
	}

	private static ushort CheckCitizen(ushort vehicleID, ref Vehicle vehicleData, Segment3 segment, float lastLen, float nextLen, ref float maxSpeed, ref bool blocked, float maxBraking, ushort otherID, ref CitizenInstance otherData, Vector3 min, Vector3 max)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (otherData.m_flags & CitizenInstance.Flags.Transition) == CitizenInstance.Flags.None && (vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0 != ((otherData.m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None))
		{
			return otherData.m_nextGridInstance;
		}
		if ((otherData.m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None)
		{
			return otherData.m_nextGridInstance;
		}
		CitizenInfo info = otherData.Info;
		CitizenInstance.Frame lastFrameData = otherData.GetLastFrameData();
		Vector3 position = lastFrameData.m_position;
		Vector3 vector = lastFrameData.m_position + lastFrameData.m_velocity;
		Segment3 segment2;
		segment2..ctor(position, vector);
		Vector3 vector2 = segment2.Min();
		vector2.x -= info.m_radius;
		vector2.z -= info.m_radius;
		Vector3 vector3 = segment2.Max();
		vector3.x += info.m_radius;
		vector3.y += info.m_height;
		vector3.z += info.m_radius;
		float num;
		float num2;
		if (min.x < vector3.x + 1f && min.y < vector3.y && min.z < vector3.z + 1f && vector2.x < max.x + 1f && vector2.y < max.y + 2f && vector2.z < max.z + 1f && segment.DistanceSqr(segment2, ref num, ref num2) < (1f + info.m_radius) * (1f + info.m_radius))
		{
			float num3 = lastLen + nextLen * num;
			if (num3 >= 0.01f)
			{
				num3 -= 2f;
				float num4 = Mathf.Max(1f, CableCarBaseAI.CalculateMaxSpeed(num3, 0f, maxBraking));
				maxSpeed = Mathf.Min(maxSpeed, num4);
			}
		}
		return otherData.m_nextGridInstance;
	}

	private static ushort CheckOtherVehicle(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxBraking, ushort otherID, ref Vehicle otherData, Vector3 min, Vector3 max, int lodPhysics)
	{
		if (otherID != vehicleID && vehicleData.m_leadingVehicle != otherID && vehicleData.m_trailingVehicle != otherID)
		{
			VehicleInfo info = otherData.Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				return otherData.m_nextGridVehicle;
			}
			if (((vehicleData.m_flags | otherData.m_flags) & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (vehicleData.m_flags & Vehicle.Flags.Underground) != (otherData.m_flags & Vehicle.Flags.Underground))
			{
				return otherData.m_nextGridVehicle;
			}
			Vector3 vector;
			Vector3 vector2;
			if (lodPhysics >= 2)
			{
				vector = otherData.m_segment.Min();
				vector2 = otherData.m_segment.Max();
			}
			else
			{
				vector = Vector3.Min(otherData.m_segment.Min(), otherData.m_targetPos3);
				vector2 = Vector3.Max(otherData.m_segment.Max(), otherData.m_targetPos3);
			}
			if (min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
			{
				Vehicle.Frame lastFrameData = otherData.GetLastFrameData();
				if (lodPhysics < 2)
				{
					float num2;
					float num3;
					float num = vehicleData.m_segment.DistanceSqr(otherData.m_segment, ref num2, ref num3);
					if (num < 4f)
					{
						Vector3 vector3 = vehicleData.m_segment.Position(0.5f);
						Vector3 vector4 = otherData.m_segment.Position(0.5f);
						Vector3 vector5 = vehicleData.m_segment.b - vehicleData.m_segment.a;
						if (Vector3.Dot(vector5, vector3 - vector4) < 0f)
						{
							collisionPush -= vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						else
						{
							collisionPush += vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						blocked = true;
					}
				}
				float num4 = frameData.m_velocity.get_magnitude() + 0.01f;
				float num5 = lastFrameData.m_velocity.get_magnitude();
				float num6 = num5 * (0.5f + 0.5f * num5 / info.m_braking) + Mathf.Min(1f, num5);
				num5 += 0.01f;
				float num7 = 0f;
				Vector3 vector6 = vehicleData.m_segment.b;
				Vector3 vector7 = vehicleData.m_segment.b - vehicleData.m_segment.a;
				int num8 = (vehicleData.Info.m_vehicleType != VehicleInfo.VehicleType.Tram) ? 0 : 1;
				for (int i = num8; i < 4; i++)
				{
					Vector3 vector8 = vehicleData.GetTargetPos(i);
					Vector3 vector9 = vector8 - vector6;
					if (Vector3.Dot(vector7, vector9) > 0f)
					{
						float magnitude = vector9.get_magnitude();
						Segment3 segment;
						segment..ctor(vector6, vector8);
						min = segment.Min();
						max = segment.Max();
						segment.a.y = segment.a.y * 0.5f;
						segment.b.y = segment.b.y * 0.5f;
						if (magnitude > 0.01f && min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
						{
							Vector3 a = otherData.m_segment.a;
							a.y *= 0.5f;
							float num9;
							if (segment.DistanceSqr(a, ref num9) < 4f)
							{
								float num10 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
								float num11 = num7 + magnitude * num9;
								if (num11 >= 0.01f)
								{
									num11 -= num10 + 3f;
									float num12 = Mathf.Max(0f, CableCarBaseAI.CalculateMaxSpeed(num11, num10, maxBraking));
									if (num12 < 0.01f)
									{
										blocked = true;
									}
									Vector3 vector10 = Vector3.Normalize(otherData.m_targetPos0 - otherData.m_segment.a);
									float num13 = 1.2f - 1f / ((float)vehicleData.m_blockCounter * 0.02f + 0.5f);
									if (Vector3.Dot(vector9, vector10) > num13 * magnitude)
									{
										maxSpeed = Mathf.Min(maxSpeed, num12);
									}
								}
								break;
							}
							if (lodPhysics < 2)
							{
								float num14 = 0f;
								float num15 = num6;
								Vector3 vector11 = otherData.m_segment.b;
								Vector3 vector12 = otherData.m_segment.b - otherData.m_segment.a;
								int num16 = (info.m_vehicleType != VehicleInfo.VehicleType.Tram) ? 0 : 1;
								bool flag = false;
								int num17 = num16;
								while (num17 < 4 && num15 > 0.1f)
								{
									Vector3 vector13;
									if (otherData.m_leadingVehicle != 0)
									{
										if (num17 != num16)
										{
											break;
										}
										vector13 = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)otherData.m_leadingVehicle].m_segment.b;
									}
									else
									{
										vector13 = otherData.GetTargetPos(num17);
									}
									Vector3 vector14 = Vector3.ClampMagnitude(vector13 - vector11, num15);
									if (Vector3.Dot(vector12, vector14) > 0f)
									{
										vector13 = vector11 + vector14;
										float magnitude2 = vector14.get_magnitude();
										num15 -= magnitude2;
										Segment3 segment2;
										segment2..ctor(vector11, vector13);
										segment2.a.y = segment2.a.y * 0.5f;
										segment2.b.y = segment2.b.y * 0.5f;
										if (magnitude2 > 0.01f)
										{
											float num19;
											float num20;
											float num18;
											if (otherID < vehicleID)
											{
												num18 = segment2.DistanceSqr(segment, ref num19, ref num20);
											}
											else
											{
												num18 = segment.DistanceSqr(segment2, ref num20, ref num19);
											}
											if (num18 < 4f)
											{
												float num21 = num7 + magnitude * num20;
												float num22 = num14 + magnitude2 * num19 + 0.1f;
												if (num21 >= 0.01f && num21 * num5 > num22 * num4)
												{
													float num23 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
													if (num21 >= 0.01f)
													{
														num21 -= num23 + 1f + otherData.Info.m_generatedInfo.m_size.z;
														float num24 = Mathf.Max(0f, CableCarBaseAI.CalculateMaxSpeed(num21, num23, maxBraking));
														if (num24 < 0.01f)
														{
															blocked = true;
														}
														maxSpeed = Mathf.Min(maxSpeed, num24);
													}
												}
												flag = true;
												break;
											}
										}
										vector12 = vector14;
										num14 += magnitude2;
										vector11 = vector13;
									}
									num17++;
								}
								if (flag)
								{
									break;
								}
							}
						}
						vector7 = vector9;
						num7 += magnitude;
						vector6 = vector8;
					}
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((vector.z - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((vector2.z + 10f) / 32f + 270f), 539);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = CableCarBaseAI.CheckOverlap(segment, ignoreVehicle, maxVelocity, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 4f)
		{
			VehicleInfo info = otherData.Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				return otherData.m_nextGridVehicle;
			}
			if (otherData.GetLastFrameData().m_velocity.get_sqrMagnitude() < maxVelocity * maxVelocity)
			{
				overlap = true;
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override void InvalidPath(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData)
	{
		vehicleData.m_targetPos0 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos3.w = 0f;
		base.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		float num = (float)offset * 0.003921569f;
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection(num, out pos, out dir);
		ushort startNode = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
		if ((instance.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			num = 0.5f + num * 0.5f;
		}
		else if ((instance.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			num *= 0.5f;
		}
		float num2 = 2f * num - 1f;
		pos.y += (num2 * num2 - 1f) * this.m_cableSuspensionOffset;
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		float num = (float)offset * 0.003921569f;
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection(num, out pos, out dir);
		ushort startNode = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
		if ((instance.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			num = 0.5f + num * 0.5f;
		}
		else if ((instance.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			num *= 0.5f;
		}
		float num2 = 2f * num - 1f;
		pos.y += (num2 * num2 - 1f) * this.m_cableSuspensionOffset;
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true, false);
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, false, this.IgnoreBlocked(vehicleID, ref vehicleData), false, false))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (CableCarBaseAI.CheckOverlap(vehicleData.m_segment, 0, 1000f))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	public float m_cableSuspensionOffset;
}
