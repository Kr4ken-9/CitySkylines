﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class NewScenarioGamePanel : LoadSavePanelBase<ScenarioMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_leftHandTraffic = base.Find<UICheckBox>("InvertTraffic");
		this.m_winConditionsList = new UITemplateList<UILabel>(base.Find<UIPanel>("WinConditionsContentPanel"), "WinConditionTemplate");
		this.m_losingConditionsList = new UITemplateList<UILabel>(base.Find<UIPanel>("LosingConditionsContentPanel"), "LosingConditionTemplate");
		this.m_ScenarioDescription = base.Find<UILabel>("ScenarioDescription");
		this.m_Author = base.Find<UILabel>("Author");
		this.m_FileList = base.Find<UIListBox>("MapList");
		this.m_FileList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnMapSelectionChanged));
		UITabstrip uITabstrip = base.Find<UITabstrip>("TabstripNewScenario");
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnMapSelectionChanged));
		this.m_StartButton = base.Find<UIButton>("Start");
		this.m_CityName = base.Find<UITextField>("MapName");
		this.m_CityName.add_eventTextChanged(delegate(UIComponent c, string t)
		{
			this.m_StartButton.set_isEnabled(this.IsValid());
		});
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_BuildableArea = base.Find<UILabel>("BuildableArea");
		this.m_Theme = base.Find<UILabel>("MapTheme");
		this.m_OilResources = base.Find<UIProgressBar>("ResourceBarOil");
		this.m_OreResources = base.Find<UIProgressBar>("ResourceBarOre");
		this.m_ForestryResources = base.Find<UIProgressBar>("ResourceBarForestry");
		this.m_FertilityResources = base.Find<UIProgressBar>("ResourceBarFarming");
		this.m_WaterResources = base.Find<UIProgressBar>("ResourceBarWater");
		this.m_OilNoResources = base.Find("ResourceOil").Find<UISprite>("NoNoNo");
		this.m_OreNoResources = base.Find("ResourceOre").Find<UISprite>("NoNoNo");
		this.m_ForestryNoResources = base.Find("ResourceForestry").Find<UISprite>("NoNoNo");
		this.m_FertilityNoResources = base.Find("ResourceFarming").Find<UISprite>("NoNoNo");
		this.m_WaterNoResources = base.Find("ResourceWater").Find<UISprite>("NoNoNo");
		this.m_Highway = base.Find<UISprite>("Highway");
		this.m_NoHighway = this.m_Highway.Find<UISprite>("NoNoNo");
		this.m_InHighway = this.m_Highway.Find<UISprite>("Incoming");
		this.m_OutHighway = this.m_Highway.Find<UISprite>("Outgoing");
		this.m_Train = base.Find<UISprite>("Train");
		this.m_NoTrain = this.m_Train.Find<UISprite>("NoNoNo");
		this.m_InTrain = this.m_Train.Find<UISprite>("Incoming");
		this.m_OutTrain = this.m_Train.Find<UISprite>("Outgoing");
		this.m_Ship = base.Find<UISprite>("Ship");
		this.m_NoShip = this.m_Ship.Find<UISprite>("NoNoNo");
		this.m_InShip = this.m_Ship.Find<UISprite>("Incoming");
		this.m_OutShip = this.m_Ship.Find<UISprite>("Outgoing");
		this.m_Plane = base.Find<UISprite>("Plane");
		this.m_NoPlane = this.m_Plane.Find<UISprite>("NoNoNo");
		this.m_InPlane = this.m_Plane.Find<UISprite>("Incoming");
		this.m_OutPlane = this.m_Plane.Find<UISprite>("Outgoing");
		this.m_ThemeOverridePanel = base.Find<UIPanel>("OverridePanel");
		this.m_Themes = new Dictionary<string, List<MapThemeMetaData>>();
		this.m_ThemeOverrideDropDown = base.Find<UIDropDown>("OverrideMapTheme");
		base.SetSorting("PANEL_SORT_THEME");
		this.Refresh();
	}

	private string GenerateMapName(string environment)
	{
		CityNameGroups.Environment environment2 = this.m_CityNameGroups.FindGroup(environment);
		string text = environment2.m_closeDistance[Random.Range(0, environment2.m_closeDistance.Length)];
		int num = (int)Locale.Count("CONNECTIONS_PATTERN", text);
		string format = Locale.Get("CONNECTIONS_PATTERN", text, Random.Range(0, num));
		int num2 = (int)Locale.Count("CONNECTIONS_NAME", text);
		string arg = Locale.Get("CONNECTIONS_NAME", text, Random.Range(0, num2));
		return StringUtils.SafeFormat(format, arg);
	}

	private void OnMapSelectionChanged(UIComponent comp, int sel)
	{
		this.OnMapSelectionChanged();
	}

	public void OnMapSelectionChanged()
	{
		int selectedIndex = this.m_FileList.get_selectedIndex();
		if (selectedIndex > -1)
		{
			Package.Asset listingData = base.GetListingData(selectedIndex);
			Package package = listingData.get_package();
			this.m_Author.set_isVisible(!string.IsNullOrEmpty(package.get_packageAuthor()));
			if (this.m_Author.get_isVisible())
			{
				this.m_Author.set_text(LocaleFormatter.FormatGeneric("PACKAGEAUTHOR", new object[]
				{
					LoadSavePanelBase<ScenarioMetaData>.ResolveName(package.get_packageAuthor())
				}));
			}
			ScenarioMetaData listingMetaData = base.GetListingMetaData(selectedIndex);
			if (this.m_SnapShot.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShot.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShot.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShot.get_texture() != null)
				{
					this.m_SnapShot.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShot.set_texture(null);
			}
			this.m_CityName.set_text((!string.IsNullOrEmpty(listingMetaData.cityName)) ? listingMetaData.cityName : this.GenerateMapName(listingMetaData.environment));
			ValueAnimator.Cancel("NewGameOil");
			ValueAnimator.Cancel("NewGameOre");
			ValueAnimator.Cancel("NewGameForest");
			ValueAnimator.Cancel("NewGameFertility");
			ValueAnimator.Cancel("NewGameWater");
			this.m_Theme.set_text(LoadSavePanelBase<ScenarioMetaData>.GetThemeString(null, listingMetaData.environment, "NEWGAME_BASETHEME"));
			this.m_SelectedEnvironment = listingMetaData.environment;
			if (this.m_ThemeOverridePanel != null)
			{
				base.RefreshOverrideThemes();
				base.SelectThemeOverride(listingMetaData.mapThemeRef);
				this.m_ThemeOverridePanel.Show();
			}
			this.m_leftHandTraffic.set_isChecked(listingMetaData.invertTraffic);
			this.m_OilResources.set_value(0f);
			this.m_OreResources.set_value(0f);
			this.m_ForestryResources.set_value(0f);
			this.m_FertilityResources.set_value(0f);
			this.m_WaterResources.set_value(0f);
			this.m_OilNoResources.set_isVisible(listingMetaData.oilAmount == 0u);
			this.m_OreNoResources.set_isVisible(listingMetaData.oreAmount == 0u);
			this.m_FertilityNoResources.set_isVisible(listingMetaData.fertileLandAmount == 0u);
			this.m_ForestryNoResources.set_isVisible(listingMetaData.forestAmount == 0u);
			this.m_WaterNoResources.set_isVisible(listingMetaData.waterAmount == 0u);
			this.m_BuildableArea.set_text(StringUtils.SafeFormat(Locale.Get("AREA_BUILDABLE"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), listingMetaData.buildableArea)));
			float maxResourceAmount = listingMetaData.maxResourceAmount;
			float num = 0f;
			float num2 = 0f;
			float num3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			if (maxResourceAmount > 0f)
			{
				num = Mathf.Pow(listingMetaData.oilAmount / maxResourceAmount, this.m_OilExponent);
				num2 = Mathf.Pow(listingMetaData.oreAmount / maxResourceAmount, this.m_OreExponent);
				num3 = Mathf.Pow(listingMetaData.forestAmount / maxResourceAmount, this.m_ForestryExponent);
				num4 = Mathf.Pow(listingMetaData.fertileLandAmount / maxResourceAmount, this.m_FarmingExponent);
				num5 = Mathf.Pow(listingMetaData.waterAmount / maxResourceAmount, this.m_WaterExponent);
			}
			ValueAnimator.Animate("NewGameOil", delegate(float val)
			{
				this.m_OilResources.set_value(val);
			}, new AnimatedFloat(0f, num, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameOre", delegate(float val)
			{
				this.m_OreResources.set_value(val);
			}, new AnimatedFloat(0f, num2, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameForest", delegate(float val)
			{
				this.m_ForestryResources.set_value(val);
			}, new AnimatedFloat(0f, num3, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameFertility", delegate(float val)
			{
				this.m_FertilityResources.set_value(val);
			}, new AnimatedFloat(0f, num4, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameWater", delegate(float val)
			{
				this.m_WaterResources.set_value(val);
			}, new AnimatedFloat(0f, num5, this.m_InterpolationTime, this.m_InterpolationEasingType));
			bool flag = listingMetaData.incomingShipPath == 0 && listingMetaData.outgoingShipPath == 0;
			this.m_Ship.set_tooltip((!flag) ? Locale.Get("AREA_YES_SHIPCONNECTION") : Locale.Get("AREA_NO_SHIPCONNECTION"));
			this.m_NoShip.set_isVisible(flag);
			this.m_InShip.set_isVisible(listingMetaData.incomingShipPath > 0);
			this.m_OutShip.set_isVisible(listingMetaData.outgoingShipPath > 0);
			bool flag2 = listingMetaData.incomingTrainTracks == 0 && listingMetaData.outgoingTrainTracks == 0;
			this.m_Train.set_tooltip((!flag2) ? Locale.Get("AREA_YES_TRAINCONNECTION") : Locale.Get("AREA_NO_TRAINCONNECTION"));
			this.m_NoTrain.set_isVisible(flag2);
			this.m_InTrain.set_isVisible(listingMetaData.incomingTrainTracks > 0);
			this.m_OutTrain.set_isVisible(listingMetaData.outgoingTrainTracks > 0);
			bool flag3 = listingMetaData.incomingPlanePath == 0 && listingMetaData.outgoingPlanePath == 0;
			this.m_Plane.set_tooltip((!flag3) ? Locale.Get("AREA_YES_PLANECONNECTION") : Locale.Get("AREA_NO_PLANECONNECTION"));
			this.m_NoPlane.set_isVisible(flag3);
			this.m_InPlane.set_isVisible(listingMetaData.incomingPlanePath > 0);
			this.m_OutPlane.set_isVisible(listingMetaData.outgoingPlanePath > 0);
			bool flag4 = listingMetaData.incomingRoads == 0 && listingMetaData.outgoingRoads == 0;
			this.m_Highway.set_tooltip((!flag4) ? Locale.Get("AREA_YES_HIGHWAYCONNECTION") : Locale.Get("AREA_NO_HIGHWAYCONNECTION"));
			this.m_NoHighway.set_isVisible(flag4);
			this.m_InHighway.set_isVisible(listingMetaData.incomingRoads > 0);
			this.m_OutHighway.set_isVisible(listingMetaData.outgoingRoads > 0);
			string text = StringUtils.SafeFormat("{0}.{1}", new object[]
			{
				listingData.get_package().get_packageName(),
				listingData.get_name()
			});
			if (Locale.Exists("SCENARIO_DESC", text))
			{
				this.m_ScenarioDescription.set_text(Locale.Get("SCENARIO_DESC", text));
			}
			else
			{
				this.m_ScenarioDescription.set_text(listingMetaData.scenarioDescription);
			}
			if (listingMetaData.winConditions != null)
			{
				GeneratedString[] array = GeneratedString.DeserializeArrayFromByteArray(listingMetaData.winConditions);
				UILabel[] array2 = this.m_winConditionsList.SetItemCount(array.Length);
				for (int i = 0; i < array.Length; i++)
				{
					string text2 = array[i].ToString();
					if (text2 != Locale.Get("WINLOSECONDITIONS_OR"))
					{
						text2 = "- " + text2;
						array2[i].set_textAlignment(0);
						array2[i].set_backgroundSprite(null);
					}
					else
					{
						array2[i].set_textAlignment(1);
						array2[i].set_backgroundSprite(this.m_separatorBackgroundSprite);
					}
					array2[i].set_text(text2);
				}
			}
			else
			{
				this.m_winConditionsList.SetItemCount(0);
			}
			if (listingMetaData.losingConditions != null)
			{
				GeneratedString[] array3 = GeneratedString.DeserializeArrayFromByteArray(listingMetaData.losingConditions);
				UILabel[] array4 = this.m_losingConditionsList.SetItemCount(array3.Length);
				for (int j = 0; j < array3.Length; j++)
				{
					string text3 = array3[j].ToString();
					if (text3 != Locale.Get("WINLOSECONDITIONS_OR"))
					{
						text3 = "- " + text3;
						array4[j].set_textAlignment(0);
						array4[j].set_backgroundSprite(null);
					}
					else
					{
						array4[j].set_textAlignment(1);
						array4[j].set_backgroundSprite(this.m_separatorBackgroundSprite);
					}
					array4[j].set_text(text3);
				}
			}
			else
			{
				this.m_losingConditionsList.SetItemCount(0);
			}
		}
		else
		{
			this.m_SnapShot.set_texture(null);
		}
	}

	private void OnDestroy()
	{
		ValueAnimator.Cancel("NewGameOil");
		ValueAnimator.Cancel("NewGameOre");
		ValueAnimator.Cancel("NewGameForest");
		ValueAnimator.Cancel("NewGameFertility");
		ValueAnimator.Cancel("NewGameWater");
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.OnStartNewGame();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_FileList.Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.Refresh();
			base.get_component().Focus();
		}
		else
		{
			this.m_FileList.set_selectedIndex(-1);
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("NewGamePanel.Refresh()"))
		{
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.ScenarioMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
						if ((scenarioMetaData.environment != "winter" || flag) && scenarioMetaData.isPublished)
						{
							string text = scenarioMetaData.scenarioName;
							string text2 = StringUtils.SafeFormat("{0}.{1}", new object[]
							{
								scenarioMetaData.assetRef.get_package().get_packageName(),
								scenarioMetaData.assetRef.get_name()
							});
							if (!scenarioMetaData.builtin || this.IsOwnedAsset(text2))
							{
								SteamHelper.DLC requiredDLC = NewScenarioGamePanel.GetRequiredDLC(text2);
								bool flag2 = scenarioMetaData.builtin && requiredDLC == SteamHelper.DLC.NaturalDisastersDLC;
								bool flag3 = scenarioMetaData.builtin && requiredDLC == SteamHelper.DLC.InMotionDLC;
								bool flag4 = scenarioMetaData.builtin && requiredDLC == SteamHelper.DLC.GreenCitiesDLC;
								if (Locale.Exists("SCENARIO_NAME", text2))
								{
									text = Locale.Get("SCENARIO_NAME", text2);
								}
								if (flag2)
								{
									text = "<sprite NaturalDisastersIcon> " + text;
								}
								if (flag3)
								{
									text = "<sprite MassTransitIcon> " + text;
								}
								if (flag4)
								{
									text = "<sprite GreenCitiesIcon> " + text;
								}
								base.AddToListing(text, current, scenarioMetaData);
							}
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			this.m_FileList.set_items(base.GetListingItems());
			if (this.m_FileList.get_items().Length > 0)
			{
				this.m_FileList.set_selectedIndex(0);
			}
			this.m_StartButton.set_isEnabled(this.IsValid());
			base.RebuildThemeDictionary();
		}
	}

	private bool IsOwnedAsset(string assetName)
	{
		SteamHelper.DLC requiredDLC = NewScenarioGamePanel.GetRequiredDLC(assetName);
		return requiredDLC == SteamHelper.DLC.None || SteamHelper.IsDLCOwned(requiredDLC);
	}

	public static SteamHelper.DLC GetRequiredDLC(string assetName)
	{
		switch (assetName)
		{
		case "Alpine Villages.Alpine Villages_Data":
		case "By The Dam.By The Dam_Data":
		case "Floodland.Floodland_Data":
		case "Island Hopping.Island Hopping_Data":
		case "Tornado Country.Tornado Country_Data":
			return SteamHelper.DLC.NaturalDisastersDLC;
		case "Ferry Empire.Ferry Empire_Data":
		case "Fix the Traffic.Fix the Traffic_Data":
		case "Trains.Trains_Data":
			return SteamHelper.DLC.InMotionDLC;
		case "City of Gardens.City of Gardens_Data":
		case "Clean Up Crew.Clean Up Crew_Data":
		case "Green Power.Green Power_Data":
			return SteamHelper.DLC.GreenCitiesDLC;
		}
		return SteamHelper.DLC.None;
	}

	private bool IsValid()
	{
		return this.m_FileList.get_items().Length > 0 && this.m_CityName.get_text() != string.Empty;
	}

	public void OnStartNewGame()
	{
		ScenarioMetaData listingMetaData = base.GetListingMetaData(this.m_FileList.get_selectedIndex());
		if (this.m_ThemeOverrideDropDown != null && this.m_Themes != null)
		{
			if (this.m_ThemeOverrideDropDown.get_items().Length > 0 && this.m_ThemeOverrideDropDown.get_selectedIndex() > 0 && !string.IsNullOrEmpty(this.m_SelectedEnvironment) && this.m_Themes.ContainsKey(this.m_SelectedEnvironment) && this.m_Themes[this.m_SelectedEnvironment].Count >= this.m_ThemeOverrideDropDown.get_selectedIndex())
			{
				listingMetaData.mapThemeRef = this.m_Themes[this.m_SelectedEnvironment][this.m_ThemeOverrideDropDown.get_selectedIndex() - 1].mapThemeRef;
			}
			else if (this.m_ThemeOverrideDropDown.get_selectedIndex() == 0)
			{
				listingMetaData.mapThemeRef = null;
			}
		}
		if (base.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.StartNewGameRoutine();
				}
			});
		}
		else
		{
			this.StartNewGameRoutine();
		}
	}

	private void StartNewGameRoutine()
	{
		Package.Asset listingData = base.GetListingData(this.m_FileList.get_selectedIndex());
		ScenarioMetaData listingMetaData = base.GetListingMetaData(this.m_FileList.get_selectedIndex());
		SimulationMetaData simulationMetaData = new SimulationMetaData
		{
			m_CityName = this.m_CityName.get_text(),
			m_gameInstanceIdentifier = Guid.NewGuid().ToString(),
			m_invertTraffic = ((!this.m_leftHandTraffic.get_isChecked()) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True),
			m_disableAchievements = ((Singleton<PluginManager>.get_instance().get_enabledModCount() <= 0) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True),
			m_startingDateTime = DateTime.Now,
			m_currentDateTime = DateTime.Now,
			m_newGameAppVersion = 172090642u,
			m_updateMode = SimulationManager.UpdateMode.NewGameFromScenario,
			m_ScenarioAsset = StringUtils.SafeFormat("{0}.{1}", new object[]
			{
				listingData.get_package().get_packageName(),
				listingData.get_name()
			}),
			m_ScenarioName = listingMetaData.scenarioName,
			m_MapName = listingMetaData.scenarioName
		};
		if (listingMetaData.mapThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(listingMetaData.mapThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		Singleton<LoadingManager>.get_instance().LoadLevel(listingData, "Game", "InGame", simulationMetaData);
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	public override void OnClosed()
	{
		NewGameMainPanel newGameMainPanel = UIView.get_library().Get<NewGameMainPanel>("NewGamePanel");
		if (newGameMainPanel != null)
		{
			newGameMainPanel.OnClosed();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnStartNewGame();
				p.Use();
			}
		}
	}

	public CityNameGroups m_CityNameGroups;

	public float m_OilExponent = 1f;

	public float m_OreExponent = 1f;

	public float m_ForestryExponent = 1f;

	public float m_FarmingExponent = 1f;

	public float m_WaterExponent = 1f;

	public float m_InterpolationTime = 0.7f;

	public EasingType m_InterpolationEasingType = 2;

	public UIComponent[] m_TabFocusList;

	private UIListBox m_FileList;

	private UITextureSprite m_SnapShot;

	private UIButton m_StartButton;

	private UILabel m_Theme;

	private UIProgressBar m_OilResources;

	private UIProgressBar m_OreResources;

	private UIProgressBar m_ForestryResources;

	private UIProgressBar m_FertilityResources;

	private UIProgressBar m_WaterResources;

	private UISprite m_OilNoResources;

	private UISprite m_OreNoResources;

	private UISprite m_ForestryNoResources;

	private UISprite m_FertilityNoResources;

	private UISprite m_WaterNoResources;

	private UISprite m_Highway;

	private UISprite m_NoHighway;

	private UISprite m_InHighway;

	private UISprite m_OutHighway;

	private UISprite m_Train;

	private UISprite m_NoTrain;

	private UISprite m_InTrain;

	private UISprite m_OutTrain;

	private UISprite m_Ship;

	private UISprite m_NoShip;

	private UISprite m_InShip;

	private UISprite m_OutShip;

	private UISprite m_Plane;

	private UISprite m_NoPlane;

	private UISprite m_InPlane;

	private UISprite m_OutPlane;

	private UILabel m_BuildableArea;

	private UITextField m_CityName;

	private UILabel m_Author;

	private UILabel m_ScenarioDescription;

	private UITemplateList<UILabel> m_winConditionsList;

	private UITemplateList<UILabel> m_losingConditionsList;

	private UICheckBox m_leftHandTraffic;

	public UIScrollbar[] m_scrollbars;

	public UIComponent[] m_scrollableContents;

	public UITextureAtlas m_atlas;

	[UISprite("m_atlas")]
	public string m_separatorBackgroundSprite;
}
