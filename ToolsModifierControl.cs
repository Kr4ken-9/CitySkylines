﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public abstract class ToolsModifierControl : UICustomControl
{
	internal static string GetHappinessString(Citizen.Happiness happinessLevel)
	{
		return "NotificationIcon" + ToolsModifierControl.sHappinessLevels[(int)happinessLevel];
	}

	public UIButton closeToolbarButton
	{
		get
		{
			if (this.m_CloseToolbarButton == null)
			{
				this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
			}
			return this.m_CloseToolbarButton;
		}
	}

	public static MainToolbar mainToolbar
	{
		get
		{
			if (ToolsModifierControl.m_MainToolbar == null)
			{
				ToolsModifierControl.m_MainToolbar = UIView.Find("MainToolstrip").GetComponent<MainToolbar>();
			}
			return ToolsModifierControl.m_MainToolbar;
		}
	}

	public static UIMultiStateButton bulldozerButton
	{
		get
		{
			if (ToolsModifierControl.m_BulldozerButton == null)
			{
				ToolsModifierControl.m_BulldozerButton = UIView.Find<UIMultiStateButton>("BulldozerButton");
			}
			return ToolsModifierControl.m_BulldozerButton;
		}
	}

	public static ToolController toolController
	{
		get
		{
			if (ToolsModifierControl.m_ToolController == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("GameController");
				if (gameObject != null)
				{
					ToolsModifierControl.m_ToolController = gameObject.GetComponent<ToolController>();
				}
			}
			return ToolsModifierControl.m_ToolController;
		}
	}

	public bool isMapEditor
	{
		get
		{
			return Singleton<ToolManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.MapEditor);
		}
	}

	public bool isAssetEditor
	{
		get
		{
			return Singleton<ToolManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor);
		}
	}

	public bool isGame
	{
		get
		{
			return !Singleton<ToolManager>.get_exists() || Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.Game);
		}
	}

	public static EconomyPanel economyPanel
	{
		get
		{
			if (ToolsModifierControl.m_EconomyPanel == null)
			{
				UIComponent uIComponent = UIView.Find("EconomyPanel");
				if (uIComponent != null)
				{
					ToolsModifierControl.m_EconomyPanel = uIComponent.GetComponent<EconomyPanel>();
				}
			}
			return ToolsModifierControl.m_EconomyPanel;
		}
	}

	public static CameraController cameraController
	{
		get
		{
			if (ToolsModifierControl.m_CameraController == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					ToolsModifierControl.m_CameraController = gameObject.GetComponent<CameraController>();
				}
			}
			return ToolsModifierControl.m_CameraController;
		}
	}

	public static UnlockingPanel unlockingPanel
	{
		get
		{
			if (ToolsModifierControl.m_UnlockingPanel == null)
			{
				UIComponent uIComponent = UIView.Find("UnlockingPanel");
				if (uIComponent != null)
				{
					ToolsModifierControl.m_UnlockingPanel = uIComponent.GetComponent<UnlockingPanel>();
				}
			}
			return ToolsModifierControl.m_UnlockingPanel;
		}
	}

	public static TutorialAdvisorPanel advisorPanel
	{
		get
		{
			if (ToolsModifierControl.m_AdvisorPanel == null)
			{
				UIComponent uIComponent = UIView.Find("TutorialAdvisorPanel");
				if (uIComponent != null)
				{
					ToolsModifierControl.m_AdvisorPanel = uIComponent.GetComponent<TutorialAdvisorPanel>();
				}
			}
			return ToolsModifierControl.m_AdvisorPanel;
		}
	}

	public static PoliciesPanel policiesPanel
	{
		get
		{
			if (ToolsModifierControl.m_PoliciesPanel == null)
			{
				UIComponent uIComponent = UIView.Find("PoliciesPanel");
				if (uIComponent != null)
				{
					ToolsModifierControl.m_PoliciesPanel = uIComponent.GetComponent<PoliciesPanel>();
				}
			}
			return ToolsModifierControl.m_PoliciesPanel;
		}
	}

	public static InfoViewsPanel infoViewsPanel
	{
		get
		{
			if (ToolsModifierControl.m_InfoViewsPanel == null)
			{
				UIComponent uIComponent = UIView.Find<UIPanel>("InfoViewsPanel");
				if (uIComponent != null)
				{
					ToolsModifierControl.m_InfoViewsPanel = uIComponent.GetComponent<InfoViewsPanel>();
				}
			}
			return ToolsModifierControl.m_InfoViewsPanel;
		}
	}

	public void CloseEverything()
	{
		this.CloseAllPanels();
		this.CloseToolbar();
		WorldInfoPanel.HideAllWorldInfoPanels();
		UIView.get_library().Hide("PublicTransportDetailPanel");
	}

	public void CloseAllPanels()
	{
		if (ToolsModifierControl.economyPanel != null)
		{
			ToolsModifierControl.economyPanel.Hide();
		}
		if (ToolsModifierControl.policiesPanel != null)
		{
			ToolsModifierControl.policiesPanel.Hide();
		}
	}

	public void CloseToolbar()
	{
		if (this.closeToolbarButton != null)
		{
			this.closeToolbarButton.SimulateClick();
		}
	}

	private static void CollectTools()
	{
		if (ToolsModifierControl.toolController != null)
		{
			ToolsModifierControl.m_Tools = new Dictionary<Type, ToolBase>();
			for (int i = 0; i < ToolsModifierControl.toolController.Tools.Length; i++)
			{
				ToolsModifierControl.m_Tools.Add(ToolsModifierControl.toolController.Tools[i].GetType(), ToolsModifierControl.toolController.Tools[i]);
			}
		}
	}

	public static bool keepThisWorldInfoPanel
	{
		get;
		set;
	}

	public static T SetTool<T>() where T : ToolBase
	{
		if (ToolsModifierControl.m_Tools == null)
		{
			ToolsModifierControl.CollectTools();
		}
		ToolBase toolBase;
		if (ToolsModifierControl.toolController != null && ToolsModifierControl.m_Tools.TryGetValue(typeof(T), out toolBase))
		{
			if (!ToolsModifierControl.keepThisWorldInfoPanel)
			{
				WorldInfoPanel.HideAllWorldInfoPanels();
			}
			GameAreaInfoPanel.Hide();
			ToolsModifierControl.keepThisWorldInfoPanel = false;
			if (ToolsModifierControl.toolController.CurrentTool != toolBase)
			{
				ToolsModifierControl.toolController.CurrentTool = toolBase;
			}
			return toolBase as T;
		}
		return (T)((object)null);
	}

	public static T GetTool<T>() where T : ToolBase
	{
		if (ToolsModifierControl.m_Tools == null)
		{
			ToolsModifierControl.CollectTools();
		}
		ToolBase toolBase;
		if (ToolsModifierControl.toolController != null && ToolsModifierControl.m_Tools.TryGetValue(typeof(T), out toolBase))
		{
			return toolBase as T;
		}
		return (T)((object)null);
	}

	public static T GetCurrentTool<T>() where T : ToolBase
	{
		if (ToolsModifierControl.m_Tools == null)
		{
			ToolsModifierControl.CollectTools();
		}
		if (ToolsModifierControl.toolController != null)
		{
			return ToolsModifierControl.toolController.CurrentTool as T;
		}
		return (T)((object)null);
	}

	private void OnDestroy()
	{
		ToolsModifierControl.m_ToolController = null;
		ToolsModifierControl.m_Tools = null;
	}

	public static bool IsUnlocked(MilestoneInfo milestone)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(milestone);
	}

	public static bool IsUnlocked(DistrictPolicies.Types policyType)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(policyType);
	}

	public static bool IsUnlocked(DistrictPolicies.Policies policy)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(policy);
	}

	public static bool GetUnlockingInfo(MilestoneInfo milestone, out string unlockDesc, out string currentValue, out string targetValue, out string progress, out string locked)
	{
		if (!ToolsModifierControl.IsUnlocked(milestone))
		{
			FastList<MilestoneInfo.ProgressInfo> fastList;
			MilestoneInfo.ProgressInfo progressInfo = milestone.GetLocalizedProgress(out fastList);
			if (fastList.m_size > 0)
			{
				for (int i = 0; i < fastList.m_size; i++)
				{
					if (!fastList[i].m_passed)
					{
						progressInfo = fastList[i];
						break;
					}
				}
			}
			progress = progressInfo.m_progress;
			if (progress == null)
			{
				progress = "Progress missing";
			}
			currentValue = progressInfo.m_current.ToString();
			targetValue = progressInfo.m_max.ToString();
			locked = true.ToString();
			unlockDesc = progressInfo.m_description;
			return true;
		}
		progress = string.Empty;
		currentValue = "0";
		targetValue = "1";
		unlockDesc = string.Empty;
		locked = false.ToString();
		return false;
	}

	public static bool IsUnlocked(UnlockManager.Feature feature)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(feature);
	}

	public static bool IsUnlocked(ItemClass.Service service)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(service);
	}

	public static bool IsUnlocked(ItemClass.Zone zone)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(zone);
	}

	public static bool IsUnlocked(ItemClass.SubService subService)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(subService);
	}

	public static bool IsUnlocked(InfoManager.InfoMode mode)
	{
		return !Singleton<UnlockManager>.get_exists() || Singleton<UnlockManager>.get_instance().Unlocked(mode);
	}

	private static CameraController m_CameraController;

	private static UnlockingPanel m_UnlockingPanel;

	private static TutorialAdvisorPanel m_AdvisorPanel;

	private static EconomyPanel m_EconomyPanel;

	private static PoliciesPanel m_PoliciesPanel;

	private static InfoViewsPanel m_InfoViewsPanel;

	private static MainToolbar m_MainToolbar;

	private static UIMultiStateButton m_BulldozerButton;

	private UIButton m_CloseToolbarButton;

	private static ToolController m_ToolController;

	private static Dictionary<Type, ToolBase> m_Tools;

	private static readonly string[] sHappinessLevels = new string[]
	{
		"VeryUnhappy",
		"Unhappy",
		"Happy",
		"VeryHappy",
		"ExtremelyHappy"
	};
}
