﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PropInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_generatedInfo == null)
		{
			throw new PrefabException(this, "Generated info missing");
		}
		this.m_hasRenderer = (base.GetComponent<Renderer>() != null);
		if (this.m_mesh == null && this.m_hasRenderer)
		{
			MeshFilter component = base.GetComponent<MeshFilter>();
			this.m_mesh = component.get_sharedMesh();
			this.m_material = base.GetComponent<Renderer>().get_sharedMaterial();
			if (this.m_mesh == null)
			{
				throw new PrefabException(this, "Mesh missing");
			}
			if (this.m_material == null)
			{
				throw new PrefabException(this, "Material missing");
			}
			if (this.m_useColorVariations)
			{
				this.m_color0 = this.m_material.GetColor("_ColorV0");
				this.m_color1 = this.m_material.GetColor("_ColorV1");
				this.m_color2 = this.m_material.GetColor("_ColorV2");
				this.m_color3 = this.m_material.GetColor("_ColorV3");
			}
			else
			{
				this.m_color0 = this.m_material.get_color();
				this.m_color1 = this.m_material.get_color();
				this.m_color2 = this.m_material.get_color();
				this.m_color3 = this.m_material.get_color();
			}
			string tag = this.m_material.GetTag("RenderType", false);
			this.m_isDecal = (tag == "Decal");
			this.m_isMarker = (tag == "Marker");
			this.m_requireWaterMap = (tag == "Floating");
			this.m_requireHeightMap = (this.m_requireWaterMap || tag == "Fence");
			if (tag == "RotatingProp")
			{
				this.m_rollLocation = new Vector4[4];
				this.m_rollLocation[0] = this.m_material.GetVector("_RollLocation0");
				this.m_rollLocation[1] = this.m_material.GetVector("_RollLocation1");
				this.m_rollLocation[2] = this.m_material.GetVector("_RollLocation2");
				this.m_rollLocation[3] = this.m_material.GetVector("_RollLocation3");
				this.m_rollParams = new Vector4[4];
				this.m_rollParams[0] = this.m_material.GetVector("_RollParams0");
				this.m_rollParams[1] = this.m_material.GetVector("_RollParams1");
				this.m_rollParams[2] = this.m_material.GetVector("_RollParams2");
				this.m_rollParams[3] = this.m_material.GetVector("_RollParams3");
			}
			if (this.m_generatedInfo.m_size == Vector3.get_zero())
			{
				throw new PrefabException(this, "Generated info has zero size");
			}
			if (this.m_generatedInfo.m_propInfo != null)
			{
				if (this.m_generatedInfo.m_propInfo.m_mesh != this.m_mesh)
				{
					throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_propInfo.get_gameObject().get_name() + ")");
				}
			}
			else
			{
				this.m_generatedInfo.m_propInfo = this;
			}
			Texture2D texture2D = this.m_material.get_mainTexture() as Texture2D;
			if (texture2D != null && texture2D.get_format() == 12)
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "Diffuse is DXT5: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
		}
		if (this.m_lodObject != null)
		{
			MeshFilter component2 = this.m_lodObject.GetComponent<MeshFilter>();
			this.m_lodMesh = component2.get_sharedMesh();
			this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
			if (this.m_lodMaterial.get_shader() != this.m_material.get_shader() != this.m_lodHasDifferentShader)
			{
				CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD shader mismatch: " + base.get_gameObject().get_name(), base.get_gameObject());
			}
		}
		else if (this.m_hasRenderer && !this.m_isDecal && !this.m_isMarker)
		{
			CODebugBase<LogChannel>.Warn(LogChannel.Core, "LOD missing: " + base.get_gameObject().get_name(), base.get_gameObject());
		}
		this.RefreshLevelOfDetail();
		this.m_effectLayer = -1;
		if (this.m_effects != null)
		{
			this.m_hasEffects = (this.m_effects.Length != 0);
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_effects[i].m_effect.InitializeEffect();
					int num = this.m_effects[i].m_effect.GroupLayer();
					if (num != -1)
					{
						this.m_effectLayer = num;
					}
				}
			}
		}
		else
		{
			this.m_hasEffects = false;
		}
		this.m_lodLocations = new Vector4[16];
		this.m_lodObjectIndices = new Vector4[16];
		this.m_lodColors = new Vector4[16];
		this.m_lodCount = 0;
		this.m_lodMin = new Vector3(100000f, 100000f, 100000f);
		this.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
	}

	public override void DestroyPrefab()
	{
		if (this.m_lodMeshCombined1 != null)
		{
			Object.Destroy(this.m_lodMeshCombined1);
			this.m_lodMeshCombined1 = null;
		}
		if (this.m_lodMeshCombined4 != null)
		{
			Object.Destroy(this.m_lodMeshCombined4);
			this.m_lodMeshCombined4 = null;
		}
		if (this.m_lodMeshCombined8 != null)
		{
			Object.Destroy(this.m_lodMeshCombined8);
			this.m_lodMeshCombined8 = null;
		}
		if (this.m_lodMeshCombined16 != null)
		{
			Object.Destroy(this.m_lodMeshCombined16);
			this.m_lodMeshCombined16 = null;
		}
		if (this.m_lodMaterialCombined != null)
		{
			Object.Destroy(this.m_lodMaterialCombined);
			this.m_lodMaterialCombined = null;
		}
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_effects[i].m_effect.ReleaseEffect();
				}
			}
		}
		this.m_mesh = null;
		this.m_material = null;
		this.m_lodMesh = null;
		this.m_lodMaterial = null;
		this.m_lodHeightMap = null;
		this.m_lodWaterHeightMap = null;
		if (this.m_generatedInfo != null && this.m_generatedInfo.m_propInfo == this)
		{
			this.m_generatedInfo.m_propInfo = null;
		}
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		float num = RenderManager.LevelOfDetailFactor * 100f;
		if (this.m_generatedInfo.m_triangleArea == 0f)
		{
			this.m_maxRenderDistance = 1000f;
		}
		else
		{
			this.m_maxRenderDistance = Mathf.Sqrt(this.m_generatedInfo.m_triangleArea) * num + 100f;
			this.m_maxRenderDistance = Mathf.Min(1000f, this.m_maxRenderDistance);
		}
		if (this.m_isDecal || this.m_isMarker)
		{
			this.m_lodRenderDistance = 0f;
		}
		else if (this.m_lodMesh != null)
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance * 0.25f;
		}
		else
		{
			this.m_lodRenderDistance = this.m_maxRenderDistance;
		}
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_maxRenderDistance = Mathf.Max(this.m_maxRenderDistance, this.m_effects[i].m_effect.RenderDistance());
				}
			}
			this.m_maxRenderDistance = Mathf.Min(1000f, this.m_maxRenderDistance);
		}
	}

	public override void CheckReferences()
	{
		if (this.m_variations != null)
		{
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				if (this.m_variations[i].m_prop != null)
				{
					if (this.m_variations[i].m_eventPolicy != DistrictPolicies.Event.None)
					{
						this.m_variations[i].m_requirePolicy = true;
					}
					if (this.m_variations[i].m_prop.m_prefabInitialized)
					{
						this.m_variations[i].m_finalProp = this.m_variations[i].m_prop;
					}
					else
					{
						this.m_variations[i].m_finalProp = PrefabCollection<PropInfo>.FindLoaded(this.m_variations[i].m_prop.get_gameObject().get_name());
					}
					if (this.m_variations[i].m_finalProp == null)
					{
						if (!this.m_variations[i].m_requirePolicy || this.m_variations[i].m_eventPolicy == DistrictPolicies.Event.None || Singleton<DistrictManager>.get_instance().IsPolicyLoaded(this.m_variations[i].m_eventPolicy))
						{
							throw new PrefabException(this, "Referenced prop is not loaded (" + this.m_variations[i].m_prop.get_gameObject().get_name() + ")");
						}
					}
				}
			}
		}
	}

	public PropInfo GetVariation(ref Randomizer r, ref District district)
	{
		if (this.m_variations != null && this.m_variations.Length != 0)
		{
			int num = r.Int32(100u);
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				int num2 = this.m_variations[i].m_probability;
				if (this.m_variations[i].m_requirePolicy && this.m_variations[i].m_eventPolicy != DistrictPolicies.Event.None && (district.m_eventPolicies & this.m_variations[i].m_eventPolicy) == DistrictPolicies.Event.None)
				{
					num2 = 0;
				}
				num -= num2;
				if (num < 0)
				{
					PropInfo finalProp = this.m_variations[i].m_finalProp;
					if (finalProp != null)
					{
						return finalProp.GetVariation(ref r, ref district);
					}
				}
			}
		}
		return this;
	}

	public PropInfo GetVariation(ref Randomizer r)
	{
		if (this.m_variations != null && this.m_variations.Length != 0)
		{
			int num = r.Int32(100u);
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				num -= this.m_variations[i].m_probability;
				if (num < 0)
				{
					PropInfo finalProp = this.m_variations[i].m_finalProp;
					if (finalProp != null)
					{
						return finalProp.GetVariation(ref r);
					}
				}
			}
		}
		return this;
	}

	public Color GetColor(ref Randomizer r)
	{
		switch (r.Int32(4u))
		{
		case 0:
			return this.m_color0;
		case 1:
			return this.m_color1;
		case 2:
			return this.m_color2;
		case 3:
			return this.m_color3;
		default:
			return this.m_color0;
		}
	}

	public void CalculateGeneratedInfo()
	{
		MeshFilter[] componentsInChildren = base.GetComponentsInChildren<MeshFilter>(true);
		this.CalculateGeneratedInfo(componentsInChildren);
	}

	public void CalculateGeneratedInfo(MeshFilter[] filters)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		float num5 = 0f;
		for (int i = 0; i < filters.Length; i++)
		{
			Vector3[] vertices = filters[i].get_sharedMesh().get_vertices();
			for (int j = 0; j < vertices.Length; j++)
			{
				num = Mathf.Min(num, vertices[j].x);
				num2 = Mathf.Max(num2, vertices[j].x);
				num3 = Mathf.Max(num3, vertices[j].y);
				num4 = Mathf.Min(num4, vertices[j].z);
				num5 = Mathf.Max(num5, vertices[j].z);
			}
		}
		this.m_generatedInfo.m_center = new Vector3(0f, num3 * 0.5f, 0f);
		this.m_generatedInfo.m_size = new Vector3(Mathf.Max(-num, num2) * 2f, num3, Mathf.Max(-num4, num5) * 2f);
		this.m_generatedInfo.m_triangleArea = 0f;
		this.m_generatedInfo.m_uvmapArea = 0f;
		Vector4[] array = new Vector4[4];
		for (int k = 0; k < filters.Length; k++)
		{
			Vector3[] vertices2 = filters[k].get_sharedMesh().get_vertices();
			Vector2[] uv = filters[k].get_sharedMesh().get_uv();
			Color32[] colors = filters[k].get_sharedMesh().get_colors32();
			int[] triangles = filters[k].get_sharedMesh().get_triangles();
			for (int l = 0; l < triangles.Length; l += 3)
			{
				int num6 = triangles[l];
				int num7 = triangles[l + 1];
				int num8 = triangles[l + 2];
				this.m_generatedInfo.m_triangleArea += Triangle3.Area(vertices2[num6], vertices2[num7], vertices2[num8]);
				this.m_generatedInfo.m_uvmapArea += Triangle2.Area(uv[num6], uv[num7], uv[num8]);
			}
			for (int m = 0; m < colors.Length; m++)
			{
				if (colors[m].b == 0)
				{
					Vector4 vector = vertices2[m];
					vector.w = 1f;
					array[Mathf.Min(3, (int)((colors[m].g + 32) / 64))] += vector;
				}
			}
		}
		for (int n = 0; n < 4; n++)
		{
			if (array[n].w != 0f)
			{
				array[n] /= array[n].w;
			}
		}
		MeshRenderer[] componentsInChildren = base.GetComponentsInChildren<MeshRenderer>(true);
		for (int num9 = 0; num9 < componentsInChildren.Length; num9++)
		{
			for (int num10 = 0; num10 < componentsInChildren[num9].get_sharedMaterials().Length; num10++)
			{
				Material material = componentsInChildren[num9].get_sharedMaterials()[num10];
				if (material.get_shader().get_name().StartsWith("Custom/Props/Prop"))
				{
					string[] shaderKeywords = material.get_shaderKeywords();
					bool flag = false;
					for (int num11 = 0; num11 < shaderKeywords.Length; num11++)
					{
						if (shaderKeywords[num11] == "OVERRIDE_PIVOTS_ON")
						{
							flag = true;
						}
					}
					if (!flag)
					{
						for (int num12 = 0; num12 < 4; num12++)
						{
							material.SetVector("_RollLocation" + num12, array[num12]);
						}
					}
				}
			}
		}
	}

	private void InitMeshDataDecal()
	{
		if (this.m_lodMeshCombined1 == null)
		{
			this.m_lodMeshCombined1 = new Mesh();
		}
		if (this.m_lodMeshCombined4 == null)
		{
			this.m_lodMeshCombined4 = new Mesh();
		}
		if (this.m_lodMeshCombined8 == null)
		{
			this.m_lodMeshCombined8 = new Mesh();
		}
		if (this.m_lodMeshCombined16 == null)
		{
			this.m_lodMeshCombined16 = new Mesh();
		}
		if (this.m_lodMaterialCombined == null)
		{
			this.m_lodMaterialCombined = new Material(this.m_material);
			this.m_lodMaterialCombined.EnableKeyword("MULTI_INSTANCE");
		}
		Vector3[] vertices = this.m_mesh.get_vertices();
		int[] triangles = this.m_mesh.get_triangles();
		int num = vertices.Length;
		int num2 = triangles.Length;
		if (num * 16 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num, num2);
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 4, num2 * 4);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 8, num2 * 8);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 16, num2 * 16);
		int num3 = 0;
		int num4 = 0;
		for (int i = 0; i < 16; i++)
		{
			Color32 color;
			color..ctor(255, 255, 255, (byte)(i * 16));
			for (int j = 0; j < num2; j++)
			{
				if (i < 1)
				{
					meshData.m_triangles[num4] = triangles[j] + num3;
				}
				if (i < 4)
				{
					meshData2.m_triangles[num4] = triangles[j] + num3;
				}
				if (i < 8)
				{
					meshData3.m_triangles[num4] = triangles[j] + num3;
				}
				if (i < 16)
				{
					meshData4.m_triangles[num4] = triangles[j] + num3;
				}
				num4++;
			}
			for (int k = 0; k < num; k++)
			{
				if (i < 1)
				{
					meshData.m_vertices[num3] = vertices[k];
					meshData.m_uvs[num3] = Vector2.get_zero();
					meshData.m_colors[num3] = color;
				}
				if (i < 4)
				{
					meshData2.m_vertices[num3] = vertices[k];
					meshData2.m_uvs[num3] = Vector2.get_zero();
					meshData2.m_colors[num3] = color;
				}
				if (i < 8)
				{
					meshData3.m_vertices[num3] = vertices[k];
					meshData3.m_uvs[num3] = Vector2.get_zero();
					meshData3.m_colors[num3] = color;
				}
				if (i < 16)
				{
					meshData4.m_vertices[num3] = vertices[k];
					meshData4.m_uvs[num3] = Vector2.get_zero();
					meshData4.m_colors[num3] = color;
				}
				num3++;
			}
		}
		meshData.PopulateMesh(this.m_lodMeshCombined1);
		meshData2.PopulateMesh(this.m_lodMeshCombined4);
		meshData3.PopulateMesh(this.m_lodMeshCombined8);
		meshData4.PopulateMesh(this.m_lodMeshCombined16);
	}

	public void InitMeshData(Rect atlasRect, Texture2D rgbAtlas, Texture2D xysAtlas, Texture2D aciAtlas)
	{
		if (this.m_isDecal || this.m_isMarker)
		{
			this.InitMeshDataDecal();
			return;
		}
		if (this.m_lodMesh == null || this.m_lodMaterial == null)
		{
			return;
		}
		if (this.m_lodMeshCombined1 == null)
		{
			this.m_lodMeshCombined1 = new Mesh();
		}
		if (this.m_lodMeshCombined4 == null)
		{
			this.m_lodMeshCombined4 = new Mesh();
		}
		if (this.m_lodMeshCombined8 == null)
		{
			this.m_lodMeshCombined8 = new Mesh();
		}
		if (this.m_lodMeshCombined16 == null)
		{
			this.m_lodMeshCombined16 = new Mesh();
		}
		if (this.m_lodMaterialCombined == null)
		{
			string[] shaderKeywords;
			if (this.m_lodMaterial != null)
			{
				this.m_lodMaterialCombined = new Material(this.m_lodMaterial);
				shaderKeywords = this.m_lodMaterial.get_shaderKeywords();
			}
			else
			{
				this.m_lodMaterialCombined = new Material(this.m_material);
				shaderKeywords = this.m_material.get_shaderKeywords();
			}
			for (int i = 0; i < shaderKeywords.Length; i++)
			{
				this.m_lodMaterialCombined.EnableKeyword(shaderKeywords[i]);
			}
			this.m_lodMaterialCombined.EnableKeyword("MULTI_INSTANCE");
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<PropManager>.get_instance().ID_MainTex))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<PropManager>.get_instance().ID_MainTex, rgbAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<PropManager>.get_instance().ID_XYSMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<PropManager>.get_instance().ID_XYSMap, xysAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<PropManager>.get_instance().ID_ACIMap))
		{
			this.m_lodMaterialCombined.SetTexture(Singleton<PropManager>.get_instance().ID_ACIMap, aciAtlas);
		}
		if (this.m_lodMaterialCombined.HasProperty(Singleton<PropManager>.get_instance().ID_AtlasRect))
		{
			this.m_lodMaterialCombined.SetVector(Singleton<PropManager>.get_instance().ID_AtlasRect, new Vector4(atlasRect.get_x(), atlasRect.get_y(), atlasRect.get_width(), atlasRect.get_height()));
		}
		Vector3[] vertices = this.m_lodMesh.get_vertices();
		Vector3[] normals = this.m_lodMesh.get_normals();
		Vector4[] tangents = this.m_lodMesh.get_tangents();
		Vector2[] uv = this.m_lodMesh.get_uv();
		Color32[] array = this.m_lodMesh.get_colors32();
		int[] triangles = this.m_lodMesh.get_triangles();
		int num = vertices.Length;
		int num2 = triangles.Length;
		if (num * 16 > 65000)
		{
			throw new PrefabException(this, "LOD has too many vertices");
		}
		if (array.Length != num)
		{
			array = new Color32[num];
			for (int j = 0; j < array.Length; j++)
			{
				array[j] = new Color32(255, 255, 255, 255);
			}
		}
		for (int k = 0; k < num; k++)
		{
			Vector2 vector = uv[k];
			vector.x = atlasRect.get_x() + atlasRect.get_width() * vector.x;
			vector.y = atlasRect.get_y() + atlasRect.get_height() * vector.y;
			uv[k] = vector;
		}
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num, num2);
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 4, num2 * 4);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 8, num2 * 8);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Tangents | RenderGroup.VertexArrays.Uvs | RenderGroup.VertexArrays.Colors, num * 16, num2 * 16);
		int num3 = 0;
		int num4 = 0;
		for (int l = 0; l < 16; l++)
		{
			byte a = (byte)(l * 16);
			for (int m = 0; m < num2; m++)
			{
				if (l < 1)
				{
					meshData.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 4)
				{
					meshData2.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 8)
				{
					meshData3.m_triangles[num4] = triangles[m] + num3;
				}
				if (l < 16)
				{
					meshData4.m_triangles[num4] = triangles[m] + num3;
				}
				num4++;
			}
			for (int n = 0; n < num; n++)
			{
				Color32 color = array[n];
				color.a = a;
				if (l < 1)
				{
					meshData.m_vertices[num3] = vertices[n];
					meshData.m_normals[num3] = normals[n];
					meshData.m_tangents[num3] = tangents[n];
					meshData.m_uvs[num3] = uv[n];
					meshData.m_colors[num3] = color;
				}
				if (l < 4)
				{
					meshData2.m_vertices[num3] = vertices[n];
					meshData2.m_normals[num3] = normals[n];
					meshData2.m_tangents[num3] = tangents[n];
					meshData2.m_uvs[num3] = uv[n];
					meshData2.m_colors[num3] = color;
				}
				if (l < 8)
				{
					meshData3.m_vertices[num3] = vertices[n];
					meshData3.m_normals[num3] = normals[n];
					meshData3.m_tangents[num3] = tangents[n];
					meshData3.m_uvs[num3] = uv[n];
					meshData3.m_colors[num3] = color;
				}
				if (l < 16)
				{
					meshData4.m_vertices[num3] = vertices[n];
					meshData4.m_normals[num3] = normals[n];
					meshData4.m_tangents[num3] = tangents[n];
					meshData4.m_uvs[num3] = uv[n];
					meshData4.m_colors[num3] = color;
				}
				num3++;
			}
		}
		meshData.PopulateMesh(this.m_lodMeshCombined1);
		meshData2.PopulateMesh(this.m_lodMeshCombined4);
		meshData3.PopulateMesh(this.m_lodMeshCombined8);
		meshData4.PopulateMesh(this.m_lodMeshCombined16);
	}

	public override void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
		InstanceID id = default(InstanceID);
		Vector3 vector;
		vector..ctor(0f, 60f, 0f);
		Vector4 colorLocation = RenderManager.GetColorLocation(0u);
		if (this.m_requireHeightMap)
		{
			Texture heightMap;
			Vector4 heightMapping;
			Vector4 surfaceMapping;
			Singleton<TerrainManager>.get_instance().GetHeightMapping(vector, out heightMap, out heightMapping, out surfaceMapping);
			PropInstance.RenderInstance(cameraInfo, this, id, vector, 1f, 0f, this.m_color0, colorLocation, true, heightMap, heightMapping, surfaceMapping);
		}
		else
		{
			PropInstance.RenderInstance(cameraInfo, this, id, vector, 1f, 0f, this.m_color0, colorLocation, true);
		}
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("PROPS_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("PROPS_TITLE", base.get_gameObject().get_name());
	}

	public override string GetUncheckedLocalizedTitle()
	{
		if (Locale.Exists("PROPS_TITLE", base.get_gameObject().get_name()))
		{
			return Locale.Get("PROPS_TITLE", base.get_gameObject().get_name());
		}
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("PROPS_DESC", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescriptionShort()
	{
		return Locale.Get("PROPS_DESC", base.get_gameObject().get_name());
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		Vector3 position;
		position..ctor(0f, 60f, 0f);
		PropInstance.TerrainUpdated(this, position);
	}

	public override int GetConstructionCost()
	{
		int result = 1000;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_class.m_service, this.m_class.m_subService, this.m_class.m_level);
		return result;
	}

	public override int GetMaintenanceCost()
	{
		return 0;
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public override string GetLocalizedTooltip()
	{
		string localizedTooltip = base.GetLocalizedTooltip();
		return TooltipHelper.Append(localizedTooltip, TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Cost,
			LocaleFormatter.FormatCost(this.GetConstructionCost(), false)
		}));
	}

	public PropInfoGen m_generatedInfo;

	public ItemClass m_class;

	public ItemClass.Placement m_placementStyle;

	[CustomizableProperty("Availability", "Prop general")]
	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	[CustomizableProperty("Min scale", "Prop general")]
	public float m_minScale = 1f;

	[CustomizableProperty("Max scale", "Prop general")]
	public float m_maxScale = 1f;

	[CustomizableProperty("Use color variations", "Prop general")]
	public bool m_useColorVariations = true;

	[CustomizableProperty("Create ruining", "Prop general")]
	public bool m_createRuining = true;

	public bool m_alwaysActive;

	public bool m_lodHasDifferentShader;

	public bool m_surviveCollapse;

	public GameObject m_lodObject;

	public PropInfo.DoorType m_doorType;

	public PropInfo.Variation[] m_variations;

	public PropInfo.ParkingSpace[] m_parkingSpaces;

	public PropInfo.SpecialPlace[] m_specialPlaces;

	public PropInfo.Effect[] m_effects;

	public Vector2 m_illuminationOffRange = new Vector2(1000f, 1000f);

	public LightEffect.BlinkType m_illuminationBlinkType;

	[NonSerialized]
	public Mesh m_mesh;

	[NonSerialized]
	public Material m_material;

	[NonSerialized]
	public Mesh m_lodMesh;

	[NonSerialized]
	public Material m_lodMaterial;

	[NonSerialized]
	public Color m_color0;

	[NonSerialized]
	public Color m_color1;

	[NonSerialized]
	public Color m_color2;

	[NonSerialized]
	public Color m_color3;

	[NonSerialized]
	public bool m_isDecal;

	[NonSerialized]
	public bool m_isMarker;

	[NonSerialized]
	public Vector4[] m_rollLocation;

	[NonSerialized]
	public Vector4[] m_rollParams;

	[NonSerialized]
	public bool m_requireHeightMap;

	[NonSerialized]
	public bool m_requireWaterMap;

	[NonSerialized]
	public float m_maxRenderDistance;

	[NonSerialized]
	public float m_lodRenderDistance;

	[NonSerialized]
	public bool m_hasRenderer;

	[NonSerialized]
	public bool m_hasEffects;

	[NonSerialized]
	public int m_effectLayer;

	[NonSerialized]
	public Mesh m_lodMeshCombined1;

	[NonSerialized]
	public Mesh m_lodMeshCombined4;

	[NonSerialized]
	public Mesh m_lodMeshCombined8;

	[NonSerialized]
	public Mesh m_lodMeshCombined16;

	[NonSerialized]
	public Material m_lodMaterialCombined;

	[NonSerialized]
	public Vector4[] m_lodLocations;

	[NonSerialized]
	public Vector4[] m_lodObjectIndices;

	[NonSerialized]
	public Vector4[] m_lodColors;

	[NonSerialized]
	public int m_lodCount;

	[NonSerialized]
	public Vector3 m_lodMin;

	[NonSerialized]
	public Vector3 m_lodMax;

	[NonSerialized]
	public Texture m_lodHeightMap;

	[NonSerialized]
	public Vector4 m_lodHeightMapping;

	[NonSerialized]
	public Vector4 m_lodSurfaceMapping;

	[NonSerialized]
	public Texture m_lodWaterHeightMap;

	[NonSerialized]
	public Vector4 m_lodWaterHeightMapping;

	[NonSerialized]
	public Vector4 m_lodWaterSurfaceMapping;

	[Flags]
	public enum DoorType
	{
		None = 0,
		Enter = 1,
		Exit = 2,
		Both = 3,
		HangAround = 17
	}

	[Flags]
	public enum ParkingFlags
	{
		RequireElectric = 1
	}

	[Serializable]
	public struct Variation
	{
		public PropInfo m_prop;

		public int m_probability;

		public DistrictPolicies.Event m_eventPolicy;

		[NonSerialized]
		public PropInfo m_finalProp;

		[NonSerialized]
		public bool m_requirePolicy;
	}

	[Serializable]
	public struct ParkingSpace
	{
		public VehicleInfo.VehicleType m_type;

		[BitMask]
		public PropInfo.ParkingFlags m_flags;

		public Vector3 m_position;

		public Vector3 m_direction;

		public Vector3 m_size;
	}

	[Serializable]
	public struct SpecialPlace
	{
		public CitizenInstance.Flags m_specialFlags;

		public Vector3 m_position;

		public Vector3 m_direction;
	}

	[Serializable]
	public struct Effect
	{
		public EffectInfo m_effect;

		public Vector3 m_position;

		public Vector3 m_direction;
	}
}
