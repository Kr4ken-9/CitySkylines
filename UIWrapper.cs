﻿using System;

public abstract class UIWrapper<T>
{
	public UIWrapper(T def)
	{
		this.m_Value = def;
		this.m_String = "N/A";
	}

	public abstract void Check(T newVal);

	public string result
	{
		get
		{
			return this.m_String;
		}
	}

	private const string kNA = "N/A";

	protected T m_Value;

	protected string m_String;
}
