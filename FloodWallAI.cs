﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class FloodWallAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + this.m_info.m_maxHeight + 1000f;
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (maxElevation > 8f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override bool UseMinHeight()
	{
		return false;
	}

	public override bool IgnoreWater()
	{
		return true;
	}

	public override bool DisplayTempSegment()
	{
		return true;
	}

	public override bool IsCombatible(NetInfo with)
	{
		return base.IsCombatible(with) && this.m_info.m_maxHeight == with.m_maxHeight;
	}

	public override bool Snap(ushort segmentID, ref NetSegment data, NetInfo info, Vector3 refPos, float elevation, out float distance, out Vector3 snapPos, out Vector3 snapDir)
	{
		distance = 0f;
		snapPos = refPos;
		snapDir = Vector3.get_forward();
		if (info.m_class.m_layer != ItemClass.Layer.Default)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		data.GetClosestPositionAndDirection(refPos, out snapPos, out snapDir);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		float num = Vector2.Dot(VectorUtils.XZ(refPos - position), VectorUtils.XZ(data.m_startDirection));
		float num2 = Vector2.Dot(VectorUtils.XZ(refPos - position2), VectorUtils.XZ(data.m_endDirection));
		snapDir.y = 0f;
		snapDir.Normalize();
		bool flag = false;
		if (num <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position - refPos);
			snapDir.y = 0f;
		}
		else if (num2 <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position2 - refPos);
			snapDir.y = 0f;
		}
		else
		{
			if (NetSegment.IsStraight(position, data.m_startDirection, position2, data.m_endDirection))
			{
				Vector3 vector = snapPos - position;
				Vector3 vector2 = snapPos - position2;
				if (vector2.get_sqrMagnitude() < vector.get_sqrMagnitude())
				{
					float num3 = Vector3.Dot(snapDir, vector2);
					num3 = Mathf.Round(num3 * 0.125f) * 8f - num3;
					snapPos += snapDir * num3;
				}
				else
				{
					float num4 = Vector3.Dot(snapDir, vector);
					num4 = Mathf.Round(num4 * 0.125f) * 8f - num4;
					snapPos += snapDir * num4;
				}
			}
			snapDir..ctor(snapDir.z, 0f, -snapDir.x);
			flag = (Vector3.Dot(snapPos - refPos, snapDir) < 0f);
			if (flag)
			{
				snapDir = -snapDir;
			}
		}
		distance = Vector3.Distance(snapPos, refPos);
		float num5 = this.m_info.m_halfWidth + Mathf.Ceil(info.m_halfWidth * 0.25f - 0.01f) * 4f;
		if (info.m_class.m_service == ItemClass.Service.Beautification)
		{
			if (info.m_class.m_level == ItemClass.Level.Level3)
			{
				if (distance < num5 + 24f)
				{
					num5 += 24f;
					snapPos -= snapDir * num5;
					snapPos.y = NetSegment.SampleTerrainHeight(info, snapPos, false, elevation);
					return true;
				}
			}
			else if (info.m_class.m_level >= ItemClass.Level.Level4 && distance < num5 + 1f)
			{
				num5 += 1f;
				snapPos -= snapDir * num5;
				snapPos.y = NetSegment.SampleTerrainHeight(info, snapPos, false, elevation);
				return true;
			}
		}
		else if (distance < num5 + 16f)
		{
			num5 += 4f;
			if (info.m_class.m_service != ItemClass.Service.Electricity && distance > num5 + 4f)
			{
				num5 += 8f;
				elevation += 1f;
			}
			snapPos -= snapDir * num5;
			snapPos.y = FloodWallAI.GetActualHeight(segmentID, ref data, snapPos, flag) + elevation;
			return true;
		}
		return false;
	}

	private static float GetActualHeight(ushort segmentID, ref NetSegment data, Vector3 refPos, bool right)
	{
		Vector3 vector;
		Vector3 startDir;
		bool smoothStart;
		data.CalculateCorner(segmentID, true, true, !right, out vector, out startDir, out smoothStart);
		Vector3 vector2;
		Vector3 endDir;
		bool smoothEnd;
		data.CalculateCorner(segmentID, true, false, right, out vector2, out endDir, out smoothEnd);
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateMiddlePoints(vector, startDir, vector2, endDir, smoothStart, smoothEnd, out vector3, out vector4);
		Bezier3 bezier;
		bezier..ctor(vector, vector3, vector4, vector2);
		float num;
		bezier.DistanceSqr(refPos, ref num);
		return bezier.Position(num).y;
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		if (index == 0)
		{
			heights = TerrainModify.Heights.SecondaryLevel;
			leftT = 0.5f / (this.m_info.m_halfWidth * 2f);
			rightT = 1f - 0.5f / (this.m_info.m_halfWidth * 2f);
			return true;
		}
		if (index != 1)
		{
			return false;
		}
		surface = TerrainModify.Surface.None;
		heights = TerrainModify.Heights.BlockHeight;
		leftT = 0.5f - 12f / (this.m_info.m_halfWidth * 2f);
		rightT = 0.5f + 12f / (this.m_info.m_halfWidth * 2f);
		leftY = this.m_waterBlockOffset;
		rightY = this.m_waterBlockOffset;
		return true;
	}

	public override bool SegmentModifyMask(ushort segmentID, ref NetSegment data, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftStartY, ref float rightStartY, ref float leftEndY, ref float rightEndY)
	{
		if (index == 0)
		{
			heights = TerrainModify.Heights.SecondaryLevel;
			leftT = 0.5f / (this.m_info.m_halfWidth * 2f);
			rightT = 1f - 0.5f / (this.m_info.m_halfWidth * 2f);
			return true;
		}
		if (index != 1)
		{
			return false;
		}
		surface = TerrainModify.Surface.None;
		heights = TerrainModify.Heights.BlockHeight;
		leftT = 0.5f - 12f / (this.m_info.m_halfWidth * 2f);
		rightT = 0.5f + 12f / (this.m_info.m_halfWidth * 2f);
		leftStartY = this.m_waterBlockOffset;
		rightStartY = this.m_waterBlockOffset;
		leftEndY = this.m_waterBlockOffset;
		rightEndY = this.m_waterBlockOffset;
		return true;
	}

	public override float GetCollisionHalfWidth()
	{
		return this.m_info.m_halfWidth - 0.5f;
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				if (info != null && info.m_maxHeight != this.m_info.m_maxHeight)
				{
					flag = true;
				}
			}
		}
		if (flag)
		{
			data.m_flags |= NetNode.Flags.Transition;
		}
		else
		{
			data.m_flags &= ~NetNode.Flags.Transition;
		}
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		if (startPoint.m_node != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)startPoint.m_node].Info;
			if (info.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (startPoint.m_segment != 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)startPoint.m_segment].Info;
			if (info2.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		if (endPoint.m_node != 0)
		{
			NetInfo info3 = instance.m_nodes.m_buffer[(int)endPoint.m_node].Info;
			if (info3.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		else if (endPoint.m_segment != 0)
		{
			NetInfo info4 = instance.m_segments.m_buffer[(int)endPoint.m_segment].Info;
			if (info4.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
		}
		Vector3 vector;
		Vector3 vector2;
		NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, true, true, out vector, out vector2);
		Bezier2 bezier;
		bezier.a = VectorUtils.XZ(startPoint.m_position);
		bezier.b = VectorUtils.XZ(vector);
		bezier.c = VectorUtils.XZ(vector2);
		bezier.d = VectorUtils.XZ(endPoint.m_position);
		int num = Mathf.CeilToInt(Vector2.Distance(bezier.a, bezier.d) * 0.005f) + 3;
		Segment2 segment;
		segment.a = bezier.a;
		for (int i = 1; i <= num; i++)
		{
			segment.b = bezier.Position((float)i / (float)num);
			if (instance2.HasWater(segment, 12f, true))
			{
				toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
			}
			segment.a = segment.b;
		}
		return toolErrors;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 255;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (vector.y > -0.31f && vector2.y < 0.8f && vector.x > 0.01f && vector.x < 0.99f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 255;
		return result;
	}

	public override float GetVScale()
	{
		return 0.02f;
	}

	public override bool CanConnect(NetInfo other)
	{
		return other.m_class.m_level == this.m_info.m_class.m_level;
	}

	[CustomizableProperty("Water Block Offset", "Properties")]
	public float m_waterBlockOffset = -20f;
}
