﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class SimulationManager : Singleton<SimulationManager>
{
	private void Awake()
	{
		this.m_simulationProfiler = new ThreadProfiler();
		this.m_simulationFrameLock = new object();
		this.m_simulationActions = new Queue<AsyncTaskBase>();
		this.m_randomizer = new Randomizer((int)DateTime.Now.Ticks);
		this.m_currentGameTime = DateTime.Now;
		this.m_currentDayTimeHour = 12f;
		this.m_timePerFrame = new TimeSpan(1476562500L);
		this.m_buildIndexHistory = new uint[32];
		this.m_viewLock = new object();
		this.m_serializableDataStorage = new Dictionary<string, byte[]>();
		SavedBool savedBool = new SavedBool(Settings.enableDayNight, Settings.gameSettingsFile, true);
		this.m_enableDayNight = savedBool.get_value();
		this.m_ApplicationWrapper = new ApplicationWrapper();
		this.m_ManagersWrapper = new ManagersWrapper();
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
		this.m_simulationThread = new Thread(new ThreadStart(this.SimulationThread));
		this.m_simulationThread.Name = "Simulation";
		this.m_simulationThread.Priority = SimulationManager.SIMULATION_PRIORITY;
		this.m_simulationThread.Start();
		if (!this.m_simulationThread.IsAlive)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Main simulation thread failed to start!");
		}
	}

	private void OnDestroy()
	{
		if (this.m_updateCounter != -1)
		{
			while (!Monitor.TryEnter(this.m_simulationFrameLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_updateCounter = -1;
				Monitor.Pulse(this.m_simulationFrameLock);
				while (this.m_inSimulationStep)
				{
					Monitor.Wait(this.m_simulationFrameLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(this.m_simulationFrameLock);
			}
		}
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (this.m_SerializableDataWrapper == null)
		{
			this.m_SerializableDataWrapper = new SerializableDataWrapper(this);
		}
		if (this.m_ThreadingWrapper == null)
		{
			this.m_ThreadingWrapper = new ThreadingWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_SerializableDataWrapper != null)
		{
			this.m_SerializableDataWrapper.Release();
			this.m_SerializableDataWrapper = null;
		}
		if (this.m_ThreadingWrapper != null)
		{
			this.m_ThreadingWrapper.Release();
			this.m_ThreadingWrapper = null;
		}
	}

	public static void RegisterManager(Object manager)
	{
		SimulationManager.RegisterSimulationManager(manager as ISimulationManager);
		RenderManager.RegisterRenderableManager(manager as IRenderableManager);
		AudioManager.RegisterAudibleManager(manager as IAudibleManager);
		TerrainManager.RegisterTerrainManager(manager as ITerrainManager);
	}

	public static void RegisterSimulationManager(ISimulationManager manager)
	{
		if (manager != null)
		{
			SimulationManager.m_managers.Add(manager);
		}
	}

	public static void GetManagers(out ISimulationManager[] managers, out int count)
	{
		if (SimulationManager.m_managers != null)
		{
			managers = SimulationManager.m_managers.m_buffer;
			count = SimulationManager.m_managers.m_size;
		}
		else
		{
			managers = null;
			count = 0;
		}
	}

	public void SetView(SimulationManager.ViewData viewData)
	{
		while (!Monitor.TryEnter(this.m_viewLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_tempView = viewData;
		}
		finally
		{
			Monitor.Exit(this.m_viewLock);
		}
	}

	private void Update()
	{
		if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			return;
		}
		int finalSimulationSpeed = this.FinalSimulationSpeed;
		float referenceTimer = this.m_referenceTimer;
		this.m_referenceTimer += Time.get_deltaTime() / Time.get_fixedDeltaTime() * (float)finalSimulationSpeed;
		float num = (float)(this.m_referenceFrameIndex - this.m_currentFrameIndex);
		float num2 = this.m_referenceTimer + num;
		float num3 = 1.57079637f / (float)this.m_maxFramesBehind;
		num2 = Mathf.Atan(num2 * num3) / num3;
		if (finalSimulationSpeed != 0)
		{
			this.m_referenceTimer = num2 - num;
		}
		if (this.m_referenceTimer < referenceTimer)
		{
			this.m_referenceTimer = referenceTimer;
		}
		this.m_simulationTimeDelta = (this.m_referenceTimer - referenceTimer) * Time.get_fixedDeltaTime();
		this.m_simulationTimer += this.m_simulationTimeDelta;
		this.m_simulationTimer2 += this.m_simulationTimeDelta;
		if (this.m_simulationTimer >= 60f)
		{
			this.m_simulationTimer -= 60f;
		}
		if (this.m_simulationTimer2 >= 3600f)
		{
			this.m_simulationTimer2 -= 3600f;
		}
		this.m_realTimer += Time.get_deltaTime();
		if (this.m_realTimer >= 60f)
		{
			this.m_realTimer -= 60f;
		}
		if (Time.get_deltaTime() != 0f)
		{
			this.m_simulationTimeSpeed = this.m_simulationTimeDelta / Time.get_deltaTime();
		}
		else
		{
			this.m_simulationTimeSpeed = 0f;
		}
		int num4 = Mathf.FloorToInt(this.m_referenceTimer);
		this.m_referenceFrameIndex = (uint)((ulong)this.m_referenceFrameIndex + (ulong)((long)num4));
		this.m_referenceTimer -= (float)num4;
		long num5 = (long)((ulong)this.m_referenceFrameIndex * (ulong)this.m_timePerFrame.Ticks);
		num5 += (long)(this.m_referenceTimer * (float)this.m_timePerFrame.Ticks);
		num5 += this.m_timeOffsetTicks;
		this.m_currentGameTime = new DateTime(num5);
		float num6 = this.m_referenceFrameIndex + this.m_dayTimeOffsetFrames & SimulationManager.DAYTIME_FRAMES - 1u;
		num6 = (num6 + this.m_referenceTimer) * SimulationManager.DAYTIME_FRAME_TO_HOUR;
		if (num6 >= 24f)
		{
			num6 -= 24f;
		}
		this.m_currentDayTimeHour = num6;
		DayNightProperties.instance.m_TimeOfDay = SimulationManager.Lagrange4(this.m_currentDayTimeHour, 0f, SimulationManager.SUNRISE_HOUR, SimulationManager.SUNSET_HOUR, 24f, 0f, 6f, 18f, 24f);
		Vector4 vector;
		vector.x = this.m_simulationTimer;
		vector.y = this.m_simulationTimer * 0.0166666675f;
		vector.z = this.m_simulationTimer * 6.28318548f;
		vector.w = this.m_simulationTimer * 0.104719758f;
		Shader.SetGlobalVector("_SimulationTime", vector);
		Vector4 vector2;
		vector2.x = this.m_simulationTimer2;
		vector2.y = this.m_simulationTimer2 * 0.000277777785f;
		vector2.z = this.m_simulationTimer2 * 6.28318548f;
		vector2.w = this.m_simulationTimer2 * 0.00174532935f;
		Shader.SetGlobalVector("_SimulationTime2", vector2);
		this.m_ThreadingWrapper.OnUpdate(Time.get_deltaTime(), this.m_simulationTimeDelta);
	}

	public static float Lagrange4(float x, float x0, float x1, float x2, float x3, float y0, float y1, float y2, float y3)
	{
		float num = x - x0;
		float num2 = x - x1;
		float num3 = x - x2;
		float num4 = x - x3;
		float num5 = num * num2;
		float num6 = num3 * num4;
		float num7 = x0 - x1;
		float num8 = x0 - x2;
		float num9 = x0 - x3;
		float num10 = x1 - x2;
		float num11 = x1 - x3;
		float num12 = x2 - x3;
		return y0 * num2 * num6 / (num7 * num8 * num9) - y1 * num * num6 / (num7 * num10 * num11) + y2 * num5 * num4 / (num8 * num10 * num12) - y3 * num5 * num3 / (num9 * num11 * num12);
	}

	public DateTime FrameToTime(uint frame)
	{
		return new DateTime((long)((ulong)frame * (ulong)this.m_timePerFrame.Ticks + (ulong)this.m_timeOffsetTicks));
	}

	public uint TimeToFrame(DateTime time)
	{
		return (uint)((time.Ticks - this.m_timeOffsetTicks) / this.m_timePerFrame.Ticks);
	}

	public AsyncAction AddAction(string name, Action action)
	{
		while (!Monitor.TryEnter(this.m_simulationActions, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		AsyncAction result;
		try
		{
			AsyncAction asyncAction = new AsyncAction(action, name);
			this.m_simulationActions.Enqueue(asyncAction);
			this.m_hasActions = true;
			result = asyncAction;
		}
		finally
		{
			Monitor.Exit(this.m_simulationActions);
		}
		return result;
	}

	public AsyncAction AddAction(Action action)
	{
		return this.AddAction(null, action);
	}

	public AsyncTask AddAction(string name, IEnumerator action)
	{
		while (!Monitor.TryEnter(this.m_simulationActions, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		AsyncTask result;
		try
		{
			AsyncTask asyncTask = new AsyncTask(action, name);
			this.m_simulationActions.Enqueue(asyncTask);
			this.m_hasActions = true;
			result = asyncTask;
		}
		finally
		{
			Monitor.Exit(this.m_simulationActions);
		}
		return result;
	}

	public AsyncTask AddAction(IEnumerator action)
	{
		return this.AddAction(null, action);
	}

	public AsyncTask<T> AddAction<T>(string name, IEnumerator<T> action)
	{
		while (!Monitor.TryEnter(this.m_simulationActions, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		AsyncTask<T> result;
		try
		{
			AsyncTask<T> asyncTask = new AsyncTask<T>(action, name);
			this.m_simulationActions.Enqueue(asyncTask);
			this.m_hasActions = true;
			result = asyncTask;
		}
		finally
		{
			Monitor.Exit(this.m_simulationActions);
		}
		return result;
	}

	public AsyncTask<T> AddAction<T>(IEnumerator<T> action)
	{
		return this.AddAction<T>(null, action);
	}

	private void FixedUpdate()
	{
		while (!Monitor.TryEnter(this.m_simulationFrameLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_updateCounter < this.m_maxFramesBehind)
			{
				this.m_updateCounter++;
				Monitor.Pulse(this.m_simulationFrameLock);
			}
		}
		finally
		{
			Monitor.Exit(this.m_simulationFrameLock);
		}
	}

	public bool Terminated
	{
		get
		{
			return this.m_updateCounter == -1;
		}
	}

	public bool SimulationPaused
	{
		get
		{
			return this.m_simulationPaused;
		}
		set
		{
			if (this.m_simulationPaused != value)
			{
				this.m_simulationPaused = value;
				if (value)
				{
					GenericGuide speedNotUsed = Singleton<GuideManager>.get_instance().m_speedNotUsed;
					if (speedNotUsed != null)
					{
						speedNotUsed.Disable();
					}
				}
				else
				{
					GenericGuide speedNotUsed2 = Singleton<GuideManager>.get_instance().m_speedNotUsed2;
					if (speedNotUsed2 != null)
					{
						speedNotUsed2.Deactivate();
					}
				}
			}
		}
	}

	public bool ForcedSimulationPaused
	{
		get
		{
			return this.m_forcedSimulationPause;
		}
		set
		{
			this.m_forcedSimulationPause = value;
		}
	}

	public int SelectedSimulationSpeed
	{
		get
		{
			return this.m_simulationSpeed;
		}
		set
		{
			if (this.m_simulationSpeed != value)
			{
				this.m_simulationSpeed = value;
				GenericGuide speedNotUsed = Singleton<GuideManager>.get_instance().m_speedNotUsed;
				if (speedNotUsed != null)
				{
					speedNotUsed.Disable();
				}
			}
			if (this.m_simulationPaused)
			{
				this.m_simulationPaused = false;
				GenericGuide speedNotUsed2 = Singleton<GuideManager>.get_instance().m_speedNotUsed2;
				if (speedNotUsed2 != null)
				{
					speedNotUsed2.Deactivate();
				}
			}
		}
	}

	public int FinalSimulationSpeed
	{
		get
		{
			if (!this.m_simulationPaused && !this.m_forcedSimulationPause && Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
				{
					if (this.m_simulationSpeed == 2)
					{
						return 3;
					}
					if (this.m_simulationSpeed == 3)
					{
						return 9;
					}
				}
				else if (this.m_simulationSpeed == 3)
				{
					return 4;
				}
				return this.m_simulationSpeed;
			}
			return 0;
		}
	}

	public bool IsRecentBuildIndex(uint buildIndex)
	{
		return this.m_buildIndexHistory[(int)((UIntPtr)(this.m_currentFrameIndex >> 8 & 31u))] <= buildIndex;
	}

	private void SimulationThread()
	{
		CODebugBase<LogChannel>.Log(LogChannel.Core, "Simulation started");
		while (true)
		{
			while (!Monitor.TryEnter(this.m_simulationFrameLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_inSimulationStep = false;
				Monitor.Pulse(this.m_simulationFrameLock);
				while (this.m_updateCounter == 0)
				{
					Monitor.Wait(this.m_simulationFrameLock, 1);
				}
				if (this.Terminated)
				{
					break;
				}
				this.m_updateCounter--;
				this.m_inSimulationStep = true;
			}
			finally
			{
				Monitor.Exit(this.m_simulationFrameLock);
			}
			try
			{
				this.m_simulationProfiler.BeginStep();
				try
				{
					this.SimulationStep();
				}
				finally
				{
					this.m_simulationProfiler.EndStep();
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Simulation error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
		CODebugBase<LogChannel>.Log(LogChannel.Core, "Simulation terminated");
	}

	private void SimulationStep()
	{
		TerrainModify.BeginUpdateArea();
		try
		{
			while (this.m_hasActions)
			{
				while (!Monitor.TryEnter(this.m_simulationActions, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				AsyncTaskBase asyncTaskBase;
				try
				{
					asyncTaskBase = this.m_simulationActions.Dequeue();
					this.m_hasActions = (this.m_simulationActions.Count != 0);
				}
				finally
				{
					Monitor.Exit(this.m_simulationActions);
				}
				asyncTaskBase.Execute();
			}
			while (!Monitor.TryEnter(this.m_viewLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_simulationView = this.m_tempView;
			}
			finally
			{
				Monitor.Exit(this.m_viewLock);
			}
			if (Singleton<LoadingManager>.get_instance().m_simulationDataLoaded)
			{
				int finalSimulationSpeed = this.FinalSimulationSpeed;
				this.m_ThreadingWrapper.OnBeforeSimulationTick();
				if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					this.m_currentTickIndex += 1u;
				}
				int num = (finalSimulationSpeed != 0) ? 1 : 0;
				for (int i = num; i <= finalSimulationSpeed; i++)
				{
					if (i != 0)
					{
						if ((this.m_currentFrameIndex & 255u) == 255u)
						{
							this.m_buildIndexHistory[(int)((UIntPtr)(this.m_currentFrameIndex >> 8 & 31u))] = this.m_currentBuildIndex;
						}
						this.m_ThreadingWrapper.OnBeforeSimulationFrame();
						this.m_currentFrameIndex += 1u;
						this.m_metaData.m_currentDateTime += this.m_timePerFrame;
						if (!this.m_enableDayNight && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
						{
							this.m_dayTimeOffsetFrames = ((SimulationManager.DAYTIME_FRAMES >> 1) - this.m_currentFrameIndex & SimulationManager.DAYTIME_FRAMES - 1u);
						}
						this.m_dayTimeFrame = (this.m_currentFrameIndex + this.m_dayTimeOffsetFrames & SimulationManager.DAYTIME_FRAMES - 1u);
						float num2 = this.m_dayTimeFrame * SimulationManager.DAYTIME_FRAME_TO_HOUR;
						this.m_metaData.m_currentDayHour = num2;
						bool flag = num2 < SimulationManager.SUNRISE_HOUR || num2 > SimulationManager.SUNSET_HOUR;
						if (flag != this.m_isNightTime)
						{
							this.m_isNightTime = flag;
							if (!flag && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
							{
								ThreadHelper.get_dispatcher().Dispatch(delegate
								{
									int num3 = this.m_nightCount.get_value() + 1;
									this.m_nightCount.set_value(num3);
									if (num3 == 1001 && !PlatformService.get_achievements().get_Item("1001Nights").get_achieved())
									{
										PlatformService.get_achievements().get_Item("1001Nights").Unlock();
									}
								});
							}
						}
					}
					for (int j = 0; j < SimulationManager.m_managers.m_size; j++)
					{
						SimulationManager.m_managers.m_buffer[j].SimulationStep(i);
					}
					if (i != 0)
					{
						this.m_ThreadingWrapper.OnAfterSimulationFrame();
					}
				}
				this.m_ThreadingWrapper.OnAfterSimulationTick();
			}
		}
		finally
		{
			TerrainModify.EndUpdateArea();
		}
	}

	public static void Managers_EarlyUpdateData()
	{
		for (int i = 0; i < SimulationManager.m_managers.m_size; i++)
		{
			SimulationManager.m_managers.m_buffer[i].EarlyUpdateData();
		}
	}

	public static void Managers_UpdateData(SimulationManager.UpdateMode mode, float minProgress, float maxProgress)
	{
		for (int i = 0; i < SimulationManager.m_managers.m_size; i++)
		{
			Singleton<LoadingManager>.get_instance().SetSimulationProgress(minProgress + (maxProgress - minProgress) * (float)i / (float)SimulationManager.m_managers.m_size);
			SimulationManager.m_managers.m_buffer[i].UpdateData(mode);
		}
	}

	public static void Managers_LateUpdateData(SimulationManager.UpdateMode mode, float minProgress, float maxProgress)
	{
		for (int i = 0; i < SimulationManager.m_managers.m_size; i++)
		{
			SimulationManager.m_managers.m_buffer[i].LateUpdateData(mode);
		}
		Singleton<SimulationManager>.get_instance().LateUpdateData(mode, minProgress, maxProgress);
	}

	private void LateUpdateData(SimulationManager.UpdateMode mode, float minProgress, float maxProgress)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("SimulationManager.LateUpdateData");
		this.m_SerializableDataWrapper.OnLoadData();
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
		{
			int num = 16384;
			for (int i = 0; i < num; i++)
			{
				if ((i & 256) == 0)
				{
					Singleton<LoadingManager>.get_instance().SetSimulationProgress(minProgress + (maxProgress - minProgress) * (float)i / (float)num);
				}
				this.m_currentFrameIndex += 1u;
				for (int j = 0; j < SimulationManager.m_managers.m_size; j++)
				{
					SimulationManager.m_managers.m_buffer[j].SimulationStep(1000);
				}
			}
			this.m_currentFrameIndex -= (uint)num;
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public static int SYNCHRONIZE_TIMEOUT = 0;

	public static uint DAYTIME_FRAMES = 65536u;

	public static float DAYTIME_FRAME_TO_HOUR = 24f / SimulationManager.DAYTIME_FRAMES;

	public static float DAYTIME_HOUR_TO_FRAME = SimulationManager.DAYTIME_FRAMES / 24f;

	public static float SUNRISE_HOUR = 5f;

	public static float SUNSET_HOUR = 20f;

	public static int RELATIVE_DAY_LENGTH = 5;

	public static int RELATIVE_NIGHT_LENGTH = 3;

	public static ThreadPriority SIMULATION_PRIORITY = ThreadPriority.Normal;

	public SimulationMetaData m_metaData;

	[NonSerialized]
	public ThreadProfiler m_simulationProfiler;

	[NonSerialized]
	public DateTime m_currentGameTime;

	[NonSerialized]
	public Randomizer m_randomizer;

	[NonSerialized]
	public uint m_currentTickIndex;

	[NonSerialized]
	public uint m_currentFrameIndex;

	[NonSerialized]
	public uint m_currentBuildIndex;

	[NonSerialized]
	public uint m_referenceFrameIndex;

	[NonSerialized]
	public float m_realTimer;

	[NonSerialized]
	public float m_simulationTimer;

	[NonSerialized]
	public float m_simulationTimer2;

	[NonSerialized]
	public float m_simulationTimeDelta;

	[NonSerialized]
	public float m_simulationTimeSpeed;

	[NonSerialized]
	public float m_referenceTimer;

	[NonSerialized]
	public SimulationManager.ViewData m_simulationView;

	[NonSerialized]
	public Dictionary<string, byte[]> m_serializableDataStorage;

	[NonSerialized]
	public Thread m_simulationThread;

	[NonSerialized]
	public TimeSpan m_timePerFrame;

	[NonSerialized]
	public long m_timeOffsetTicks;

	[NonSerialized]
	public uint m_dayTimeOffsetFrames;

	[NonSerialized]
	public uint m_dayTimeFrame;

	[NonSerialized]
	public bool m_isNightTime;

	[NonSerialized]
	public float m_currentDayTimeHour;

	[NonSerialized]
	public bool m_enableDayNight;

	[NonSerialized]
	public ApplicationWrapper m_ApplicationWrapper;

	[NonSerialized]
	public ManagersWrapper m_ManagersWrapper;

	[NonSerialized]
	public SerializableDataWrapper m_SerializableDataWrapper;

	[NonSerialized]
	public ThreadingWrapper m_ThreadingWrapper;

	private bool m_simulationPaused;

	private bool m_forcedSimulationPause;

	private int m_simulationSpeed = 1;

	private object m_simulationFrameLock;

	private int m_maxFramesBehind = 14;

	private int m_updateCounter;

	private bool m_inSimulationStep;

	private Queue<AsyncTaskBase> m_simulationActions;

	private bool m_hasActions;

	private SimulationManager.ViewData m_tempView;

	private object m_viewLock;

	private uint[] m_buildIndexHistory;

	private static FastList<ISimulationManager> m_managers = new FastList<ISimulationManager>();

	private SavedInt m_nightCount = new SavedInt(Settings.nightPlayCount, Settings.userGameState, 0);

	public enum UpdateMode
	{
		Undefined = -1,
		NewMap,
		LoadMap,
		NewGameFromMap,
		LoadGame,
		NewAsset,
		LoadAsset,
		NewTheme,
		LoadTheme,
		NewScenarioFromGame,
		NewScenarioFromMap,
		LoadScenario,
		NewGameFromScenario,
		UpdateScenarioFromGame,
		UpdateScenarioFromMap
	}

	public struct ViewData
	{
		public bool Intersect(Vector3 point, float radius)
		{
			return MathUtils.InsideFrustum(point, radius, this.m_planeA, this.m_planeB, this.m_planeC, this.m_planeD, this.m_planeE, this.m_planeF);
		}

		public Vector3 m_position;

		public Vector3 m_direction;

		public Plane m_planeA;

		public Plane m_planeB;

		public Plane m_planeC;

		public Plane m_planeD;

		public Plane m_planeE;

		public Plane m_planeF;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "SimulationManager");
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			instance.m_SerializableDataWrapper.OnSaveData();
			s.WriteUInt32(instance.m_currentTickIndex);
			s.WriteUInt32(instance.m_currentFrameIndex);
			s.WriteUInt32(instance.m_currentBuildIndex);
			s.WriteULong64(instance.m_randomizer.seed);
			s.WriteUInt32(instance.m_dayTimeOffsetFrames);
			for (int i = 0; i < instance.m_buildIndexHistory.Length; i++)
			{
				s.WriteUInt32(instance.m_buildIndexHistory[i]);
			}
			FastList<IDataContainer> fastList = new FastList<IDataContainer>();
			for (int j = 0; j < SimulationManager.m_managers.m_size; j++)
			{
				SimulationManager.m_managers.m_buffer[j].GetData(fastList);
			}
			s.WriteObjectArray<IDataContainer>(fastList.ToArray());
			while (!Monitor.TryEnter(instance.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				s.WriteUInt32((uint)instance.m_serializableDataStorage.Count);
				foreach (KeyValuePair<string, byte[]> current in instance.m_serializableDataStorage)
				{
					s.WriteUniqueString(current.Key);
					s.WriteByteArray(current.Value);
				}
			}
			finally
			{
				Monitor.Exit(instance.m_serializableDataStorage);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "SimulationManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "SimulationManager");
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			instance.m_simulationSpeed = 1;
			instance.m_simulationPaused = false;
			if (s.get_version() < 37u)
			{
				throw new Exception("File version " + s.get_version() + " is no longer supported :(");
			}
			if (s.get_version() >= 84u)
			{
				instance.m_currentTickIndex = s.ReadUInt32();
			}
			else
			{
				instance.m_currentTickIndex = 0u;
			}
			if (s.get_version() >= 5u)
			{
				instance.m_currentFrameIndex = s.ReadUInt32();
			}
			if (s.get_version() >= 13u)
			{
				instance.m_currentBuildIndex = s.ReadUInt32();
			}
			else
			{
				instance.m_currentBuildIndex = instance.m_currentFrameIndex;
			}
			if (s.get_version() >= 10u)
			{
				instance.m_randomizer.seed = s.ReadULong64();
			}
			if (s.get_version() >= 212u)
			{
				instance.m_dayTimeOffsetFrames = s.ReadUInt32();
			}
			if (!instance.m_enableDayNight || s.get_version() < 212u)
			{
				instance.m_dayTimeOffsetFrames = ((SimulationManager.DAYTIME_FRAMES >> 1) - instance.m_currentFrameIndex & SimulationManager.DAYTIME_FRAMES - 1u);
			}
			instance.m_dayTimeFrame = (instance.m_currentFrameIndex + instance.m_dayTimeOffsetFrames & SimulationManager.DAYTIME_FRAMES - 1u);
			float num = instance.m_dayTimeFrame * SimulationManager.DAYTIME_FRAME_TO_HOUR;
			instance.m_isNightTime = (num < SimulationManager.SUNRISE_HOUR || num > SimulationManager.SUNSET_HOUR);
			if (s.get_version() >= 20u && s.get_version() < 183u)
			{
				if (instance.m_metaData.m_currentDateTime.Ticks == 0L)
				{
					instance.m_metaData.m_currentDateTime = new DateTime(s.ReadLong64());
				}
				else
				{
					s.ReadLong64();
				}
			}
			instance.m_timeOffsetTicks = instance.m_metaData.m_currentDateTime.Ticks - (long)((ulong)instance.m_currentFrameIndex * (ulong)instance.m_timePerFrame.Ticks);
			if (s.get_version() >= 164u)
			{
				for (int i = 0; i < instance.m_buildIndexHistory.Length; i++)
				{
					instance.m_buildIndexHistory[i] = s.ReadUInt32();
				}
			}
			else
			{
				for (int j = 0; j < instance.m_buildIndexHistory.Length; j++)
				{
					instance.m_buildIndexHistory[j] = instance.m_currentBuildIndex;
				}
			}
			if (s.get_version() >= 41u)
			{
				s.ReadObjectArray<IDataContainer>();
			}
			else
			{
				if (s.get_version() >= 9u)
				{
					s.ReadObject<ErrorManager.Data>();
				}
				s.ReadObject<TerrainManager.Data>();
				s.ReadObject<WaterSimulation.Data>();
				if (s.get_version() >= 3u)
				{
					s.ReadObject<TransferManager.Data>();
				}
				s.ReadObject<NaturalResourceManager.Data>();
				if (s.get_version() >= 7u)
				{
					s.ReadObject<ElectricityManager.Data>();
				}
				if (s.get_version() >= 17u)
				{
					s.ReadObject<WaterManager.Data>();
				}
				s.ReadObject<ZoneManager.Data>();
				if (s.get_version() >= 34u)
				{
					s.ReadObject<PropManager.Data>();
				}
				if (s.get_version() >= 21u)
				{
					s.ReadObject<TreeManager.Data>();
				}
				s.ReadObject<NetManager.Data>();
				if (s.get_version() >= 28u)
				{
					s.ReadObject<VehicleManager.Data>();
				}
				if (s.get_version() >= 12u)
				{
					s.ReadObject<CitizenManager.Data>();
				}
				if (s.get_version() >= 3u)
				{
					s.ReadObject<BuildingManager.Data>();
				}
				if (s.get_version() >= 38u)
				{
					s.ReadObject<DistrictManager.Data>();
				}
				if (s.get_version() >= 40u)
				{
					s.ReadObject<EconomyManager.Data>();
				}
			}
			if (s.get_version() < 209u)
			{
				Singleton<EventManager>.get_instance().ClearAll();
			}
			if (s.get_version() < 257u)
			{
				Singleton<DisasterManager>.get_instance().ClearAll();
			}
			if (s.get_version() < 280u)
			{
				Singleton<AudioManager>.get_instance().ClearAll();
			}
			while (!Monitor.TryEnter(instance.m_serializableDataStorage, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				instance.m_serializableDataStorage.Clear();
				if (s.get_version() >= 186u)
				{
					uint num2 = s.ReadUInt32();
					for (uint num3 = 0u; num3 < num2; num3 += 1u)
					{
						string key = s.ReadUniqueString();
						byte[] value = s.ReadByteArray();
						instance.m_serializableDataStorage.Add(key, value);
					}
				}
			}
			finally
			{
				Monitor.Exit(instance.m_serializableDataStorage);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "SimulationManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "SimulationManager");
			Singleton<LoadingManager>.get_instance().SetSimulationProgress(0.7f);
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			instance.m_referenceFrameIndex = instance.m_currentFrameIndex;
			instance.m_referenceTimer = 0f;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "SimulationManager");
		}
	}
}
