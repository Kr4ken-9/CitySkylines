﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class GameAreaTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
	}

	protected override void OnToolGUI(Event e)
	{
		if (this.m_toolController.IsInsideUI)
		{
			return;
		}
		if (e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				if (this.m_mouseAreaIndex != -1)
				{
					if (this.m_toolController != null && (this.m_toolController.m_mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.UnlockArea(this.m_mouseAreaIndex));
					}
					else
					{
						int x;
						int z;
						Singleton<GameAreaManager>.get_instance().GetTileXZ(this.m_mouseAreaIndex, out x, out z);
						if (Singleton<GameAreaManager>.get_instance().CanUnlock(x, z) || Singleton<GameAreaManager>.get_instance().IsUnlocked(x, z))
						{
							UIInput.MouseUsed();
							GameAreaInfoPanel.Show(this.m_mouseAreaIndex);
						}
						else
						{
							GameAreaInfoPanel.Hide();
						}
					}
				}
				else
				{
					GameAreaInfoPanel.Hide();
				}
			}
			else if (e.get_button() == 1)
			{
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		Singleton<GameAreaManager>.get_instance().HighlightAreaIndex = -1;
		Singleton<GameAreaManager>.get_instance().AreasVisible = true;
		Singleton<NotificationManager>.get_instance().NotificationsVisible = false;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		Singleton<NotificationManager>.get_instance().NotificationsVisible = true;
		Singleton<GameAreaManager>.get_instance().AreasVisible = false;
		Singleton<GameAreaManager>.get_instance().HighlightAreaIndex = -1;
	}

	protected override void OnToolLateUpdate()
	{
		if (this.m_toolController.IsInsideUI || !Cursor.get_visible())
		{
			Singleton<GameAreaManager>.get_instance().HighlightAreaIndex = -1;
		}
		else
		{
			Singleton<GameAreaManager>.get_instance().HighlightAreaIndex = this.m_mouseAreaIndex;
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		ToolBase.ForceInfoMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
	}

	[DebuggerHidden]
	public IEnumerator UnlockArea(int mouseArea)
	{
		GameAreaTool.<UnlockArea>c__Iterator0 <UnlockArea>c__Iterator = new GameAreaTool.<UnlockArea>c__Iterator0();
		<UnlockArea>c__Iterator.mouseArea = mouseArea;
		<UnlockArea>c__Iterator.$this = this;
		return <UnlockArea>c__Iterator;
	}

	public override void SimulationStep()
	{
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (ToolBase.RayCast(input, out raycastOutput))
		{
			this.m_mouseAreaIndex = Singleton<GameAreaManager>.get_instance().GetAreaIndex(raycastOutput.m_hitPos);
		}
		else
		{
			this.m_mouseAreaIndex = -1;
		}
	}

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private int m_mouseAreaIndex;
}
