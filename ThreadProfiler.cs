﻿using System;
using System.Diagnostics;

[Serializable]
public class ThreadProfiler
{
	public ThreadProfiler()
	{
		this.m_stopwatch = new Stopwatch();
	}

	public void BeginStep()
	{
		this.m_stopwatch.Reset();
		this.m_stopwatch.Start();
	}

	public void PauseStep()
	{
		this.m_stopwatch.Stop();
	}

	public void ContinueStep()
	{
		this.m_stopwatch.Start();
	}

	public void EndStep()
	{
		this.m_stopwatch.Stop();
		this.m_lastStepDuration = (int)(this.m_stopwatch.ElapsedTicks / 10L);
		this.m_averageStepBuffer += this.m_lastStepDuration;
		if (this.m_lastStepDuration > this.m_peakStepBuffer)
		{
			this.m_peakStepBuffer = this.m_lastStepDuration;
		}
		if (++this.m_frameCount == 64)
		{
			this.m_averageStepDuration = (this.m_averageStepBuffer + 32) / 64;
			this.m_peakStepDuration = this.m_peakStepBuffer;
			this.m_averageStepBuffer = 0;
			this.m_peakStepBuffer = 0;
			this.m_frameCount = 0;
		}
	}

	public int m_lastStepDuration;

	public int m_averageStepDuration;

	public int m_peakStepDuration;

	[NonSerialized]
	private Stopwatch m_stopwatch;

	[NonSerialized]
	private int m_averageStepBuffer;

	[NonSerialized]
	private int m_peakStepBuffer;

	[NonSerialized]
	private int m_frameCount;
}
