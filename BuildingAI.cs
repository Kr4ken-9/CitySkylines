﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class BuildingAI : PrefabAI
{
	public virtual void RefreshInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		BuildingAI.RefreshInstance(this.m_info, cameraInfo, buildingID, ref data, layerMask, ref instance, false);
	}

	public static void RefreshInstance(BuildingInfo info, RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance, bool requireHeightMap)
	{
		instance.m_position = Building.CalculateMeshPosition(info, data.m_position, data.m_angle, data.Length);
		instance.m_rotation = Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down());
		instance.m_dataVector0.x = 0f;
		instance.m_dataVector0.y = 1000f;
		instance.m_dataVector0.z = 0f;
		instance.m_dataVector0.w = (float)data.m_baseHeight;
		instance.m_dataVector3 = RenderManager.GetColorLocation((uint)buildingID);
		instance.m_dataMatrix0.SetTRS(data.m_position, instance.m_rotation, Vector3.get_one());
		instance.m_dataMatrix1.SetTRS(instance.m_position, instance.m_rotation, Vector3.get_one());
		instance.m_dataColor0 = info.m_color0;
		instance.m_dataColor0.a = 0f;
		instance.m_dataInt0 = info.m_prefabDataIndex;
		requireHeightMap = (requireHeightMap || info.m_requireHeightMap || info.m_generatedRequireHeightMap);
		if (info.m_props != null)
		{
			for (int i = 0; i < info.m_props.Length; i++)
			{
				Vector3 vector = info.m_props[i].m_position;
				vector = instance.m_dataMatrix1.MultiplyPoint(vector);
				if (!info.m_props[i].m_fixedHeight)
				{
					vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
					instance.m_extraData.SetUShort(i, (ushort)Mathf.Clamp(Mathf.RoundToInt(vector.y * 64f), 0, 65535));
				}
				else if (info.m_requireHeightMap)
				{
					vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector) + info.m_props[i].m_position.y;
					instance.m_extraData.SetUShort(i, (ushort)Mathf.Clamp(Mathf.RoundToInt(vector.y * 64f), 0, 65535));
				}
				if (info.m_props[i].m_finalProp != null && info.m_props[i].m_finalProp.m_requireHeightMap)
				{
					requireHeightMap = true;
				}
			}
		}
		if (requireHeightMap)
		{
			Singleton<TerrainManager>.get_instance().GetHeightMapping(data.m_position, out instance.m_dataTexture0, out instance.m_dataVector1, out instance.m_dataVector2);
		}
		if (info.m_overrideMainRenderer != null)
		{
			InstanceID empty = InstanceID.Empty;
			empty.Building = buildingID;
			BuildingInfo prefabInstance = info.GetPrefabInstance<BuildingInfo>(empty);
			if (prefabInstance != null)
			{
				prefabInstance.m_instanceDirty = true;
			}
		}
	}

	public virtual void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		this.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
		this.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, true, true);
	}

	public virtual void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			this.PlayAudio(listenerInfo, buildingID, ref data, 1f);
		}
	}

	protected void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data, float volume)
	{
		float maxDistance = 50f + Mathf.Min((float)(data.Width + data.Length) * 8f, 100f);
		this.PlayAudio(listenerInfo, buildingID, ref data, volume, maxDistance);
	}

	protected void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data, float volume, float maxDistance)
	{
		AudioInfo audioInfo = this.m_info.m_customLoopSound;
		if (audioInfo == null)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (this.m_info.m_class.m_subService != ItemClass.SubService.None)
			{
				audioInfo = instance.m_properties.m_subServiceSounds[(int)this.m_info.m_class.m_subService];
			}
			else
			{
				audioInfo = instance.m_properties.m_serviceSounds[(int)this.m_info.m_class.m_service];
			}
		}
		if (audioInfo != null)
		{
			this.PlayAudio(listenerInfo, buildingID, ref data, volume, audioInfo, maxDistance);
		}
	}

	protected void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data, float volume, AudioInfo clip, float maxDistance)
	{
		Vector3 vector = Building.CalculateMeshPosition(this.m_info, data.m_position, data.m_angle, data.Length);
		vector.y += this.m_info.m_size.y;
		Vector3 vector2 = listenerInfo.m_position - vector;
		maxDistance *= 1f + Mathf.Clamp01(vector2.y / maxDistance * 0.5f);
		if (Vector3.SqrMagnitude(vector2) >= maxDistance * maxDistance)
		{
			return;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		instance.m_audioGroup.AddPlayer(listenerInfo, (int)buildingID, clip, vector, Vector3.get_zero(), maxDistance, volume, 1f);
	}

	public virtual void SetRenderParameters(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, Vector3 position, Quaternion rotation, Vector4 buildingState, Vector4 objectIndex, Color color)
	{
		float active = ((data.m_flags & Building.Flags.Active) == Building.Flags.None) ? 0f : 1f;
		this.m_info.SetRenderParameters(buildingID, position, rotation, buildingState, objectIndex, active, -1000000, true, color, color);
	}

	public virtual void RenderMeshes(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		if (this.m_info.m_mesh != null)
		{
			BuildingAI.RenderMesh(cameraInfo, buildingID, ref data, this.m_info, ref instance);
		}
		if (this.m_info.m_subMeshes != null)
		{
			for (int i = 0; i < this.m_info.m_subMeshes.Length; i++)
			{
				BuildingInfo.MeshInfo meshInfo = this.m_info.m_subMeshes[i];
				if (((meshInfo.m_flagsRequired | meshInfo.m_flagsForbidden) & data.m_flags) == meshInfo.m_flagsRequired)
				{
					BuildingInfoBase subInfo = meshInfo.m_subInfo;
					BuildingAI.RenderMesh(cameraInfo, this.m_info, subInfo, meshInfo.m_matrix, ref instance);
				}
			}
		}
	}

	public static void RenderMesh(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, BuildingInfo info, ref RenderManager.Instance instance)
	{
		if (cameraInfo.CheckRenderDistance(instance.m_position, info.m_minLodDistance))
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			if (info.m_overrideMainRenderer != null)
			{
				InstanceID empty = InstanceID.Empty;
				empty.Building = buildingID;
				BuildingInfo buildingInfo = info.ObtainPrefabInstance<BuildingInfo>(empty, 255);
				if (buildingInfo != null)
				{
					buildingInfo.m_buildingAI.SetRenderParameters(cameraInfo, buildingID, ref data, instance.m_position, instance.m_rotation, instance.m_dataVector0, instance.m_dataVector3, instance.m_dataColor0);
					if (info.m_generatedMesh != null && info.m_generatedRequireHeightMap)
					{
						buildingInfo.m_extraMaterial.SetTexture(instance2.ID_HeightMap, instance.m_dataTexture0);
						buildingInfo.m_extraMaterial.SetVector(instance2.ID_HeightMapping, instance.m_dataVector1);
						buildingInfo.m_extraMaterial.SetVector(instance2.ID_SurfaceMapping, instance.m_dataVector2);
					}
					return;
				}
			}
			instance2.m_materialBlock.Clear();
			instance2.m_materialBlock.SetVector(instance2.ID_BuildingState, instance.m_dataVector0);
			instance2.m_materialBlock.SetVector(instance2.ID_ObjectIndex, instance.m_dataVector3);
			instance2.m_materialBlock.SetColor(instance2.ID_Color, instance.m_dataColor0);
			if (info.m_requireHeightMap)
			{
				instance2.m_materialBlock.SetTexture(instance2.ID_HeightMap, instance.m_dataTexture0);
				instance2.m_materialBlock.SetVector(instance2.ID_HeightMapping, instance.m_dataVector1);
				instance2.m_materialBlock.SetVector(instance2.ID_SurfaceMapping, instance.m_dataVector2);
			}
			if (info.m_requireWaterMap)
			{
				Texture texture;
				Vector4 vector;
				Vector4 vector2;
				Singleton<TerrainManager>.get_instance().GetWaterMapping(instance.m_position, out texture, out vector, out vector2);
				instance2.m_materialBlock.SetTexture(instance2.ID_WaterHeightMap, texture);
				instance2.m_materialBlock.SetVector(instance2.ID_WaterHeightMapping, vector);
				instance2.m_materialBlock.SetVector(instance2.ID_WaterSurfaceMapping, vector2);
			}
			BuildingManager expr_1F2_cp_0 = instance2;
			expr_1F2_cp_0.m_drawCallData.m_defaultCalls = expr_1F2_cp_0.m_drawCallData.m_defaultCalls + 1;
			Bounds bounds = info.m_mesh.get_bounds();
			if (bounds.get_min().y > 0.1f - instance.m_dataVector0.w)
			{
				Vector3 min = bounds.get_min();
				min.y = -instance.m_dataVector0.w;
				bounds.set_min(min);
				info.m_mesh.set_bounds(bounds);
			}
			Graphics.DrawMesh(info.m_mesh, instance.m_dataMatrix1, info.m_material, info.m_prefabDataLayer, null, 0, instance2.m_materialBlock);
			if (info.m_generatedMesh != null)
			{
				if (info.m_generatedRequireHeightMap && !info.m_requireHeightMap)
				{
					instance2.m_materialBlock.SetTexture(instance2.ID_HeightMap, instance.m_dataTexture0);
					instance2.m_materialBlock.SetVector(instance2.ID_HeightMapping, instance.m_dataVector1);
					instance2.m_materialBlock.SetVector(instance2.ID_SurfaceMapping, instance.m_dataVector2);
				}
				Graphics.DrawMesh(info.m_generatedMesh, instance.m_dataMatrix1, info.m_extraMaterial, info.m_prefabDataLayer, null, 0, instance2.m_materialBlock);
			}
		}
		else
		{
			if (info.m_requireHeightMap && instance.m_dataTexture0 != info.m_lodHeightMap)
			{
				if (info.m_lodCount != 0)
				{
					Building.RenderLod(cameraInfo, info);
				}
				info.m_lodHeightMap = instance.m_dataTexture0;
				info.m_lodHeightMapping = instance.m_dataVector1;
				info.m_lodSurfaceMapping = instance.m_dataVector2;
			}
			if (info.m_requireWaterMap)
			{
				Texture texture2;
				Vector4 lodWaterHeightMapping;
				Vector4 lodWaterSurfaceMapping;
				Singleton<TerrainManager>.get_instance().GetWaterMapping(instance.m_position, out texture2, out lodWaterHeightMapping, out lodWaterSurfaceMapping);
				if (texture2 != info.m_lodWaterHeightMap)
				{
					if (info.m_lodCount != 0)
					{
						Building.RenderLod(cameraInfo, info);
					}
					info.m_lodWaterHeightMap = texture2;
					info.m_lodWaterHeightMapping = lodWaterHeightMapping;
					info.m_lodWaterSurfaceMapping = lodWaterSurfaceMapping;
				}
			}
			info.m_lodLocations[info.m_lodCount] = instance.m_dataMatrix1;
			info.m_lodStates[info.m_lodCount] = instance.m_dataVector0;
			info.m_lodObjectIndices[info.m_lodCount] = instance.m_dataVector3;
			info.m_lodColors[info.m_lodCount] = instance.m_dataColor0.get_linear();
			info.m_lodMin = Vector3.Min(info.m_lodMin, instance.m_position);
			info.m_lodMax = Vector3.Max(info.m_lodMax, instance.m_position);
			if (++info.m_lodCount == info.m_lodLocations.Length)
			{
				Building.RenderLod(cameraInfo, info);
			}
		}
	}

	public static void RenderMesh(RenderManager.CameraInfo cameraInfo, BuildingInfo info, BuildingInfoBase subInfo, Matrix4x4 matrix, ref RenderManager.Instance instance)
	{
		matrix = instance.m_dataMatrix1 * matrix;
		if (cameraInfo.CheckRenderDistance(instance.m_position, subInfo.m_minLodDistance))
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			instance2.m_materialBlock.Clear();
			instance2.m_materialBlock.SetVector(instance2.ID_BuildingState, instance.m_dataVector0);
			instance2.m_materialBlock.SetVector(instance2.ID_ObjectIndex, instance.m_dataVector3);
			instance2.m_materialBlock.SetColor(instance2.ID_Color, instance.m_dataColor0);
			if (subInfo.m_requireHeightMap)
			{
				instance2.m_materialBlock.SetTexture(instance2.ID_HeightMap, instance.m_dataTexture0);
				instance2.m_materialBlock.SetVector(instance2.ID_HeightMapping, instance.m_dataVector1);
				instance2.m_materialBlock.SetVector(instance2.ID_SurfaceMapping, instance.m_dataVector2);
			}
			if (subInfo.m_requireWaterMap)
			{
				Texture texture;
				Vector4 vector;
				Vector4 vector2;
				Singleton<TerrainManager>.get_instance().GetWaterMapping(instance.m_position, out texture, out vector, out vector2);
				instance2.m_materialBlock.SetTexture(instance2.ID_WaterHeightMap, texture);
				instance2.m_materialBlock.SetVector(instance2.ID_WaterHeightMapping, vector);
				instance2.m_materialBlock.SetVector(instance2.ID_WaterSurfaceMapping, vector2);
			}
			BuildingManager expr_131_cp_0 = instance2;
			expr_131_cp_0.m_drawCallData.m_defaultCalls = expr_131_cp_0.m_drawCallData.m_defaultCalls + 1;
			Bounds bounds = subInfo.m_mesh.get_bounds();
			if (bounds.get_min().y > 0.1f - instance.m_dataVector0.w)
			{
				Vector3 min = bounds.get_min();
				min.y = -instance.m_dataVector0.w;
				bounds.set_min(min);
				subInfo.m_mesh.set_bounds(bounds);
			}
			Graphics.DrawMesh(subInfo.m_mesh, matrix, subInfo.m_material, info.m_prefabDataLayer, null, 0, instance2.m_materialBlock);
			if (subInfo.m_generatedMesh != null)
			{
				if (subInfo.m_generatedRequireHeightMap && !subInfo.m_requireHeightMap)
				{
					instance2.m_materialBlock.SetTexture(instance2.ID_HeightMap, instance.m_dataTexture0);
					instance2.m_materialBlock.SetVector(instance2.ID_HeightMapping, instance.m_dataVector1);
					instance2.m_materialBlock.SetVector(instance2.ID_SurfaceMapping, instance.m_dataVector2);
				}
				Graphics.DrawMesh(subInfo.m_generatedMesh, instance.m_dataMatrix1, subInfo.m_extraMaterial, subInfo.m_prefabDataLayer, null, 0, instance2.m_materialBlock);
			}
		}
		else
		{
			if (subInfo.m_requireHeightMap && instance.m_dataTexture0 != subInfo.m_lodHeightMap)
			{
				if (subInfo.m_lodCount != 0)
				{
					Building.RenderLod(cameraInfo, subInfo);
				}
				subInfo.m_lodHeightMap = instance.m_dataTexture0;
				subInfo.m_lodHeightMapping = instance.m_dataVector1;
				subInfo.m_lodSurfaceMapping = instance.m_dataVector2;
			}
			if (subInfo.m_requireWaterMap)
			{
				Texture texture2;
				Vector4 lodWaterHeightMapping;
				Vector4 lodWaterSurfaceMapping;
				Singleton<TerrainManager>.get_instance().GetWaterMapping(instance.m_position, out texture2, out lodWaterHeightMapping, out lodWaterSurfaceMapping);
				if (texture2 != subInfo.m_lodWaterHeightMap)
				{
					if (subInfo.m_lodCount != 0)
					{
						Building.RenderLod(cameraInfo, subInfo);
					}
					subInfo.m_lodWaterHeightMap = texture2;
					subInfo.m_lodWaterHeightMapping = lodWaterHeightMapping;
					subInfo.m_lodWaterSurfaceMapping = lodWaterSurfaceMapping;
				}
			}
			subInfo.m_lodLocations[subInfo.m_lodCount] = matrix;
			subInfo.m_lodStates[subInfo.m_lodCount] = instance.m_dataVector0;
			subInfo.m_lodObjectIndices[subInfo.m_lodCount] = instance.m_dataVector3;
			subInfo.m_lodColors[subInfo.m_lodCount] = instance.m_dataColor0.get_linear();
			subInfo.m_lodMin = Vector3.Min(subInfo.m_lodMin, instance.m_position);
			subInfo.m_lodMax = Vector3.Max(subInfo.m_lodMax, instance.m_position);
			if (++subInfo.m_lodCount == subInfo.m_lodLocations.Length)
			{
				Building.RenderLod(cameraInfo, subInfo);
			}
		}
	}

	protected virtual InstanceID GetPropRenderID(ushort buildingID, int propIndex, ref Building data)
	{
		InstanceID result = default(InstanceID);
		if (this.m_info.m_randomEffectTiming)
		{
			result.SetBuildingProp(buildingID, propIndex);
		}
		else
		{
			result.Building = buildingID;
		}
		return result;
	}

	public void RenderProps(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance, bool renderFixed, bool renderNonfixed)
	{
		if (this.m_info.m_props != null && ((layerMask & this.m_info.m_treeLayers) != 0 || cameraInfo.CheckRenderDistance(instance.m_position, this.m_info.m_maxPropDistance + 72f)))
		{
			int length = data.Length;
			Texture texture = null;
			Vector4 zero = Vector4.get_zero();
			Vector4 zero2 = Vector4.get_zero();
			Matrix4x4 matrix4x = Matrix4x4.get_zero();
			bool flag = false;
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(data.m_position);
			for (int i = 0; i < this.m_info.m_props.Length; i++)
			{
				BuildingInfo.Prop prop = this.m_info.m_props[i];
				Randomizer randomizer;
				randomizer..ctor((int)buildingID << 6 | prop.m_index);
				if (randomizer.Int32(100u) < prop.m_probability && length >= prop.m_requiredLength)
				{
					PropInfo propInfo = prop.m_finalProp;
					TreeInfo treeInfo = prop.m_finalTree;
					if (propInfo != null)
					{
						propInfo = propInfo.GetVariation(ref randomizer, ref instance2.m_districts.m_buffer[(int)district]);
						float num = propInfo.m_minScale + (float)randomizer.Int32(10000u) * (propInfo.m_maxScale - propInfo.m_minScale) * 0.0001f;
						Color color = propInfo.GetColor(ref randomizer);
						if ((layerMask & 1 << propInfo.m_prefabDataLayer) != 0 || propInfo.m_hasEffects)
						{
							Vector4 dataVector = instance.m_dataVector3;
							Vector3 vector2;
							if (prop.m_fixedHeight)
							{
								if (!renderFixed)
								{
									goto IL_562;
								}
								if (this.m_info.m_isFloating)
								{
									if (!flag)
									{
										float y;
										Vector3 vector;
										Singleton<TerrainManager>.get_instance().HeightMap_sampleWaterHeightAndNormal(instance.m_position, 0.15f, out y, out vector);
										Vector3 position = instance.m_position;
										position.y = y;
										Quaternion quaternion = Quaternion.FromToRotation(Vector3.get_up(), vector) * instance.m_rotation;
										matrix4x = Matrix4x4.TRS(position, quaternion, Vector3.get_one());
										flag = true;
									}
									Matrix4x4 matrix4x2 = default(Matrix4x4);
									matrix4x2.SetTRS(prop.m_position, Quaternion.AngleAxis(prop.m_radAngle * 57.29578f, Vector3.get_down()), new Vector3(num, num, num));
									matrix4x2 = matrix4x * matrix4x2;
									vector2 = matrix4x2.MultiplyPoint(Vector3.get_zero());
									if (cameraInfo.CheckRenderDistance(vector2, propInfo.m_maxRenderDistance))
									{
										InstanceID propRenderID = this.GetPropRenderID(buildingID, i, ref data);
										PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID, matrix4x2, vector2, num, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None);
										goto IL_562;
									}
								}
								else
								{
									vector2 = instance.m_dataMatrix1.MultiplyPoint(prop.m_position);
									if (this.m_info.m_requireHeightMap)
									{
										vector2.y = (float)instance.m_extraData.GetUShort(i) * 0.015625f;
									}
								}
							}
							else
							{
								if (!renderNonfixed)
								{
									goto IL_562;
								}
								vector2 = instance.m_dataMatrix1.MultiplyPoint(prop.m_position);
								if (!this.m_info.m_isFloating)
								{
									vector2.y = (float)instance.m_extraData.GetUShort(i) * 0.015625f;
								}
								dataVector.z = 0f;
							}
							if (cameraInfo.CheckRenderDistance(vector2, propInfo.m_maxRenderDistance))
							{
								InstanceID propRenderID2 = this.GetPropRenderID(buildingID, i, ref data);
								if (propInfo.m_requireWaterMap)
								{
									if (texture == null)
									{
										Singleton<TerrainManager>.get_instance().GetWaterMapping(data.m_position, out texture, out zero, out zero2);
									}
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID2, vector2, num, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None, instance.m_dataTexture0, instance.m_dataVector1, instance.m_dataVector2, texture, zero, zero2);
								}
								else if (propInfo.m_requireHeightMap)
								{
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID2, vector2, num, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None, instance.m_dataTexture0, instance.m_dataVector1, instance.m_dataVector2);
								}
								else
								{
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID2, vector2, num, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None);
								}
							}
						}
					}
					else if (treeInfo != null)
					{
						treeInfo = treeInfo.GetVariation(ref randomizer);
						float scale = treeInfo.m_minScale + (float)randomizer.Int32(10000u) * (treeInfo.m_maxScale - treeInfo.m_minScale) * 0.0001f;
						float brightness = treeInfo.m_minBrightness + (float)randomizer.Int32(10000u) * (treeInfo.m_maxBrightness - treeInfo.m_minBrightness) * 0.0001f;
						if ((layerMask & 1 << treeInfo.m_prefabDataLayer) != 0 && ((!prop.m_fixedHeight) ? renderNonfixed : renderFixed))
						{
							Vector3 position2 = instance.m_dataMatrix1.MultiplyPoint(prop.m_position);
							if (!prop.m_fixedHeight || this.m_info.m_requireHeightMap)
							{
								position2.y = (float)instance.m_extraData.GetUShort(i) * 0.015625f;
							}
							TreeInstance.RenderInstance(cameraInfo, treeInfo, position2, scale, brightness);
						}
					}
				}
				IL_562:;
			}
		}
	}

	protected void RenderDestroyedProps(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance, bool renderFixed, bool renderNonfixed)
	{
		if (this.m_info.m_props != null && cameraInfo.CheckRenderDistance(instance.m_position, this.m_info.m_maxPropDistance + 72f))
		{
			int length = data.Length;
			Texture texture = null;
			Vector4 zero = Vector4.get_zero();
			Vector4 zero2 = Vector4.get_zero();
			BuildingProperties properties = Singleton<BuildingManager>.get_instance().m_properties;
			float num = (float)Mathf.Max(0, (int)(data.GetLastFrameData().m_fireDamage - 127)) * 0.0078125f;
			for (int i = 0; i < this.m_info.m_props.Length; i++)
			{
				BuildingInfo.Prop prop = this.m_info.m_props[i];
				Randomizer randomizer;
				randomizer..ctor((int)buildingID << 6 | prop.m_index);
				Randomizer randomizer2;
				randomizer2..ctor((int)buildingID << 6 | prop.m_index);
				if (randomizer.Int32(100u) < prop.m_probability && length >= prop.m_requiredLength)
				{
					PropInfo propInfo = prop.m_finalProp;
					if (propInfo != null)
					{
						propInfo = propInfo.GetVariation(ref randomizer);
						float scale = propInfo.m_minScale + (float)randomizer.Int32(10000u) * (propInfo.m_maxScale - propInfo.m_minScale) * 0.0001f;
						Color color = propInfo.GetColor(ref randomizer);
						if (!propInfo.m_isDecal)
						{
							propInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer2, ItemClass.Service.Disaster);
							propInfo = propInfo.GetVariation(ref randomizer2);
							scale = propInfo.m_minScale + (float)randomizer2.Int32(10000u) * (propInfo.m_maxScale - propInfo.m_minScale) * 0.0001f;
							color = propInfo.GetColor(ref randomizer2);
							if (properties != null && num != 0f)
							{
								color = Color.Lerp(color, properties.m_burnedColor, num);
							}
						}
						if ((layerMask & 1 << propInfo.m_prefabDataLayer) != 0 || propInfo.m_hasEffects)
						{
							Vector3 vector = instance.m_dataMatrix1.MultiplyPoint(prop.m_position);
							if (!prop.m_fixedHeight || this.m_info.m_requireHeightMap)
							{
								vector.y = (float)instance.m_extraData.GetUShort(i) * 0.015625f;
							}
							if (cameraInfo.CheckRenderDistance(vector, propInfo.m_maxRenderDistance) && ((!prop.m_fixedHeight) ? renderNonfixed : renderFixed))
							{
								InstanceID propRenderID = this.GetPropRenderID(buildingID, 0, ref data);
								Vector4 dataVector = instance.m_dataVector3;
								if (!prop.m_fixedHeight)
								{
									dataVector.z = 0f;
								}
								if (propInfo.m_requireWaterMap)
								{
									if (texture == null)
									{
										Singleton<TerrainManager>.get_instance().GetWaterMapping(data.m_position, out texture, out zero, out zero2);
									}
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID, vector, scale, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None, instance.m_dataTexture0, instance.m_dataVector1, instance.m_dataVector2, texture, zero, zero2);
								}
								else if (propInfo.m_requireHeightMap)
								{
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID, vector, scale, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None, instance.m_dataTexture0, instance.m_dataVector1, instance.m_dataVector2);
								}
								else
								{
									PropInstance.RenderInstance(cameraInfo, propInfo, propRenderID, vector, scale, data.m_angle + prop.m_radAngle, color, dataVector, (data.m_flags & Building.Flags.Active) != Building.Flags.None);
								}
							}
						}
					}
				}
			}
		}
	}

	public virtual void RenderBuildGeometry(RenderManager.CameraInfo cameraInfo, Vector3 position, float angle, float elevation)
	{
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
				{
					Vector3 vector = Building.CalculatePosition(position, angle, pathInfo.m_nodes[0]);
					if (!pathInfo.m_netInfo.m_useFixedHeight)
					{
						vector.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, vector, false, pathInfo.m_nodes[0].y + elevation);
					}
					Ray ray;
					ray..ctor(vector + new Vector3(0f, 8f, 0f), Vector3.get_down());
					NetTool.ControlPoint controlPoint;
					if (NetTool.MakeControlPoint(ray, 16f, pathInfo.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo.m_nodes[0].y + elevation - pathInfo.m_netInfo.m_buildHeight, true, out controlPoint))
					{
						Vector3 vector2 = controlPoint.m_position - vector;
						if (!pathInfo.m_netInfo.m_useFixedHeight)
						{
							vector2.y = 0f;
						}
						float sqrMagnitude = vector2.get_sqrMagnitude();
						if (sqrMagnitude > pathInfo.m_maxSnapDistance * pathInfo.m_maxSnapDistance)
						{
							controlPoint.m_position = vector;
							controlPoint.m_elevation = 0f;
							controlPoint.m_node = 0;
							controlPoint.m_segment = 0;
						}
						else
						{
							controlPoint.m_position.y = vector.y;
						}
					}
					else
					{
						controlPoint.m_position = vector;
					}
					if (controlPoint.m_node == 0)
					{
						ushort num;
						ushort num2;
						int num3;
						int num4;
						NetTool.CreateNode(pathInfo.m_netInfo, controlPoint, controlPoint, controlPoint, NetTool.m_nodePositionsMain, 0, false, true, false, false, pathInfo.m_invertSegments, false, 0, out num, out num2, out num3, out num4);
					}
					for (int j = 1; j < pathInfo.m_nodes.Length; j++)
					{
						vector = Building.CalculatePosition(position, angle, pathInfo.m_nodes[j]);
						if (!pathInfo.m_netInfo.m_useFixedHeight)
						{
							vector.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, vector, false, pathInfo.m_nodes[j].y + elevation);
						}
						ray..ctor(vector + new Vector3(0f, 8f, 0f), Vector3.get_down());
						NetTool.ControlPoint controlPoint2;
						if (NetTool.MakeControlPoint(ray, 16f, pathInfo.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo.m_nodes[j].y + elevation - pathInfo.m_netInfo.m_buildHeight, true, out controlPoint2))
						{
							Vector3 vector3 = controlPoint2.m_position - vector;
							if (!pathInfo.m_netInfo.m_useFixedHeight)
							{
								vector3.y = 0f;
							}
							float sqrMagnitude2 = vector3.get_sqrMagnitude();
							if (sqrMagnitude2 > pathInfo.m_maxSnapDistance * pathInfo.m_maxSnapDistance)
							{
								controlPoint2.m_position = vector;
								controlPoint2.m_elevation = 0f;
								controlPoint2.m_node = 0;
								controlPoint2.m_segment = 0;
							}
							else
							{
								controlPoint2.m_position.y = vector.y;
							}
						}
						else
						{
							controlPoint2.m_position = vector;
						}
						NetTool.ControlPoint middlePoint = controlPoint2;
						if (pathInfo.m_curveTargets != null && pathInfo.m_curveTargets.Length >= j)
						{
							middlePoint.m_position = Building.CalculatePosition(position, angle, pathInfo.m_curveTargets[j - 1]);
							if (!pathInfo.m_netInfo.m_useFixedHeight)
							{
								middlePoint.m_position.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, middlePoint.m_position, false, pathInfo.m_curveTargets[j - 1].y + elevation);
							}
						}
						else
						{
							middlePoint.m_position = (controlPoint.m_position + controlPoint2.m_position) * 0.5f;
						}
						middlePoint.m_direction = VectorUtils.NormalizeXZ(middlePoint.m_position - controlPoint.m_position);
						controlPoint2.m_direction = VectorUtils.NormalizeXZ(controlPoint2.m_position - middlePoint.m_position);
						ushort num5;
						ushort num6;
						ushort num7;
						int num8;
						int num9;
						NetTool.CreateNode(pathInfo.m_netInfo, controlPoint, middlePoint, controlPoint2, NetTool.m_nodePositionsMain, 1, false, false, true, false, false, pathInfo.m_invertSegments, false, 0, out num5, out num6, out num7, out num8, out num9);
						controlPoint = controlPoint2;
					}
				}
			}
		}
	}

	public virtual void RenderBuildOverlay(RenderManager.CameraInfo cameraInfo, Color color, Vector3 position, float angle, Segment3 connectionSegment)
	{
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length > 1 && pathInfo.m_netInfo.m_overlayVisible)
				{
					for (int j = 1; j < pathInfo.m_nodes.Length; j++)
					{
						Bezier3 bezier = default(Bezier3);
						bezier.a = Building.CalculatePosition(position, angle, pathInfo.m_nodes[j - 1]);
						bezier.d = Building.CalculatePosition(position, angle, pathInfo.m_nodes[j]);
						Vector3 startDir;
						Vector3 endDir;
						if (pathInfo.m_curveTargets != null && pathInfo.m_curveTargets.Length >= j)
						{
							Vector3 vector = Building.CalculatePosition(position, angle, pathInfo.m_curveTargets[j - 1]);
							startDir = VectorUtils.NormalizeXZ(vector - bezier.a);
							endDir = VectorUtils.NormalizeXZ(vector - bezier.d);
						}
						else
						{
							startDir = VectorUtils.NormalizeXZ(bezier.d - bezier.a);
							endDir = VectorUtils.NormalizeXZ(bezier.a - bezier.d);
						}
						NetSegment.CalculateMiddlePoints(bezier.a, startDir, bezier.d, endDir, true, true, out bezier.b, out bezier.c);
						float cutStart = -100000f;
						float cutEnd = -100000f;
						ToolManager expr_182_cp_0 = Singleton<ToolManager>.get_instance();
						expr_182_cp_0.m_drawCallData.m_overlayCalls = expr_182_cp_0.m_drawCallData.m_overlayCalls + 1;
						Singleton<RenderManager>.get_instance().OverlayEffect.DrawBezier(cameraInfo, color, bezier, pathInfo.m_netInfo.m_halfWidth * 2f, cutStart, cutEnd, position.y - 100f, position.y + 100f, false, false);
					}
				}
			}
		}
	}

	public virtual void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		resource1 = NaturalResourceManager.Resource.None;
		position1 = data.m_position;
		radius1 = 0f;
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public virtual void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.None;
		radius1 = 0f;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public virtual string GetDebugString(ushort buildingID, ref Building data)
	{
		return null;
	}

	public virtual Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if (!this.m_info.m_useColorVariations)
		{
			return this.m_info.m_color0;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		switch (randomizer.Int32(4u))
		{
		case 0:
			return this.m_info.m_color0;
		case 1:
			return this.m_info.m_color1;
		case 2:
			return this.m_info.m_color2;
		case 3:
			return this.m_info.m_color3;
		default:
			return this.m_info.m_color0;
		}
	}

	public virtual int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		return 0;
	}

	public virtual int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		return 0;
	}

	public virtual int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		return 0;
	}

	public virtual int GetElectricityRate(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public virtual int GetHeatingRate(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public virtual int GetWaterRate(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public virtual int GetGarbageRate(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public virtual int GetGarbageAmount(ushort buildingID, ref Building data)
	{
		return 0;
	}

	public virtual string GetConstructionInfo(int productionRate)
	{
		return null;
	}

	public virtual string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		progress = 0f;
		return Locale.Get("LEVELUP_NOINFO");
	}

	public virtual void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public virtual void InitializePrefab()
	{
	}

	public virtual void DestroyPrefab()
	{
	}

	public virtual string GetLocalizedStatus(ushort buildingID, ref Building data)
	{
		return null;
	}

	public virtual BuildingInfoSub FindCollapsedInfo(out int rotations)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (instance.m_common != null)
		{
			return this.FindCollapsedInfo(instance.m_common.m_collapsedHigh, out rotations);
		}
		rotations = 0;
		return null;
	}

	protected BuildingInfoSub FindCollapsedInfo(BuildingInfoSub[] list, out int rotations)
	{
		Vector3 vector = this.m_info.m_generatedInfo.m_min;
		Vector3 vector2 = this.m_info.m_generatedInfo.m_max;
		float num = (float)this.m_info.m_cellWidth * 4f;
		float num2 = (float)this.m_info.m_cellLength * 4f;
		vector = Vector3.Max(vector - new Vector3(4f, 0f, 4f), new Vector3(-num, 0f, -num2));
		vector2 = Vector3.Min(vector2 + new Vector3(4f, 0f, 4f), new Vector3(num, 0f, num2));
		Vector3 vector3 = vector2 - vector;
		return BuildingCommonCollection.FindCollapsedInfo(vector3.x, vector3.z, out rotations, list);
	}

	public virtual void CreateBuilding(ushort buildingID, ref Building data)
	{
		BuildingDecoration.LoadPaths(this.m_info, buildingID, ref data, 0f);
	}

	public virtual void ReleaseBuilding(ushort buildingID, ref Building data)
	{
	}

	public virtual void BeginRelocating(ushort buildingID, ref Building data)
	{
	}

	public virtual void EndRelocating(ushort buildingID, ref Building data)
	{
		BuildingDecoration.LoadPaths(this.m_info, buildingID, ref data, 0f);
	}

	public virtual void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
	}

	protected virtual void ManualActivation(ushort buildingID, ref Building buildingData)
	{
	}

	protected virtual void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
	}

	public virtual void SimulationStep(ushort buildingID, ref Building data)
	{
		Building.Frame lastFrameData = data.GetLastFrameData();
		this.SimulationStep(buildingID, ref data, ref lastFrameData);
		data.SetFrameData(Singleton<SimulationManager>.get_instance().m_currentFrameIndex, lastFrameData);
	}

	public virtual void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
	}

	public virtual void PathfindSuccess(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
	}

	public virtual void PathfindFailure(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
	}

	public virtual BuildingInfo GetUpgradeInfo(ushort buildingID, ref Building data)
	{
		return null;
	}

	public virtual bool CheckUnlocking()
	{
		if (Singleton<UnlockManager>.get_instance().Unlocked(this.m_info.m_class.m_service) && (this.m_info.m_class.m_subService == ItemClass.SubService.None || Singleton<UnlockManager>.get_instance().Unlocked(this.m_info.m_class.m_subService)))
		{
			MilestoneInfo unlockMilestone = this.m_info.m_UnlockMilestone;
			if (unlockMilestone != null)
			{
				Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
			}
			if (Singleton<UnlockManager>.get_instance().Unlocked(unlockMilestone))
			{
				return true;
			}
		}
		return false;
	}

	public virtual void BuildingUpgraded(ushort buildingID, ref Building data)
	{
	}

	public virtual void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
	}

	public virtual void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		amountDelta = 0;
	}

	public virtual void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		amount = 0;
		max = 0;
	}

	public virtual void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 0;
	}

	public virtual bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return !this.m_info.m_lodMissing;
	}

	public virtual Color32 GetGroupVertexColor(ushort buildingID, ref Building buildingData, BuildingInfoBase info, int vertexIndex, bool baseVertex)
	{
		return new Color32(0, 255, 0, (!baseVertex) ? 0 : 255);
	}

	public virtual ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		bool flag;
		this.GetConstructionCost(relocateID != 0, out constructionCost, out flag);
		if (this.m_info.m_paths != null)
		{
			bool flag2 = this.m_info.m_placementMode == BuildingInfo.PlacementMode.Roadside;
			int num = 0;
			while (num < this.m_info.m_paths.Length && !flag2)
			{
				BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[num];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
				{
					for (int i = 0; i < pathInfo.m_nodes.Length; i++)
					{
						float minNodeDistance = pathInfo.m_netInfo.GetMinNodeDistance();
						Vector3 vector = Building.CalculatePosition(position, angle, pathInfo.m_nodes[i]);
						if (!pathInfo.m_netInfo.m_useFixedHeight)
						{
							vector.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, vector, false, pathInfo.m_nodes[i].y + elevation);
						}
						Ray ray;
						ray..ctor(vector + new Vector3(0f, 8f, 0f), Vector3.get_down());
						NetTool.ControlPoint controlPoint;
						if (NetTool.MakeControlPoint(ray, 16f, pathInfo.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo.m_nodes[i].y + elevation - pathInfo.m_netInfo.m_buildHeight, true, out controlPoint))
						{
							Vector3 vector2 = controlPoint.m_position - vector;
							if (!pathInfo.m_netInfo.m_useFixedHeight)
							{
								vector2.y = 0f;
							}
							float sqrMagnitude = vector2.get_sqrMagnitude();
							if (sqrMagnitude > 0.0001f && sqrMagnitude < minNodeDistance * minNodeDistance)
							{
								position.x += controlPoint.m_position.x - vector.x;
								if (pathInfo.m_netInfo.m_useFixedHeight)
								{
									position.y += controlPoint.m_position.y - vector.y;
								}
								position.z += controlPoint.m_position.z - vector.z;
								int num2;
								int num3;
								if (pathInfo.m_nodes.Length > i + 1)
								{
									num2 = i + 1;
									num3 = i;
								}
								else
								{
									num2 = i - 1;
									num3 = i - 1;
								}
								if (num2 >= 0)
								{
									Vector3 offset;
									if (pathInfo.m_curveTargets != null && pathInfo.m_curveTargets.Length > num3)
									{
										offset = pathInfo.m_curveTargets[num3];
									}
									else
									{
										offset = pathInfo.m_nodes[num2];
									}
									NetTool.ControlPoint newPoint = controlPoint;
									newPoint.m_position = Building.CalculatePosition(position, angle, offset);
									if (!pathInfo.m_netInfo.m_useFixedHeight)
									{
										newPoint.m_position.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, newPoint.m_position, false, offset.y + elevation);
									}
									newPoint.m_direction = VectorUtils.NormalizeXZ(newPoint.m_position - controlPoint.m_position);
									bool flag3;
									float num4;
									NetTool.ControlPoint controlPoint2 = NetTool.SnapDirection(newPoint, controlPoint, pathInfo.m_netInfo, out flag3, out num4);
									if (flag3)
									{
										float num5 = Vector2.Angle(VectorUtils.XZ(newPoint.m_direction), VectorUtils.XZ(controlPoint2.m_direction));
										if (controlPoint2.m_direction.z * newPoint.m_direction.x - controlPoint2.m_direction.x * newPoint.m_direction.z >= 0f)
										{
											angle += num5 * 0.0174532924f;
										}
										else
										{
											angle -= num5 * 0.0174532924f;
										}
										Vector3 vector3 = Building.CalculatePosition(position, angle, pathInfo.m_nodes[i]);
										position.x += controlPoint.m_position.x - vector3.x;
										if (pathInfo.m_netInfo.m_useFixedHeight)
										{
											position.y += controlPoint.m_position.y - vector3.y;
										}
										position.z += controlPoint.m_position.z - vector3.z;
									}
								}
								flag2 = true;
								break;
							}
						}
					}
				}
				num++;
			}
			for (int j = 0; j < this.m_info.m_paths.Length; j++)
			{
				BuildingInfo.PathInfo pathInfo2 = this.m_info.m_paths[j];
				if (pathInfo2.m_netInfo != null && pathInfo2.m_nodes != null && pathInfo2.m_nodes.Length != 0)
				{
					Vector3 vector4 = Building.CalculatePosition(position, angle, pathInfo2.m_nodes[0]);
					if (!pathInfo2.m_netInfo.m_useFixedHeight)
					{
						vector4.y = NetSegment.SampleTerrainHeight(pathInfo2.m_netInfo, vector4, false, pathInfo2.m_nodes[0].y + elevation);
					}
					Ray ray2;
					ray2..ctor(vector4 + new Vector3(0f, 8f, 0f), Vector3.get_down());
					NetTool.ControlPoint controlPoint3;
					if (NetTool.MakeControlPoint(ray2, 16f, pathInfo2.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo2.m_nodes[0].y + elevation - pathInfo2.m_netInfo.m_buildHeight, true, out controlPoint3))
					{
						Vector3 vector5 = controlPoint3.m_position - vector4;
						if (!pathInfo2.m_netInfo.m_useFixedHeight)
						{
							vector5.y = 0f;
						}
						float sqrMagnitude2 = vector5.get_sqrMagnitude();
						if (sqrMagnitude2 > pathInfo2.m_maxSnapDistance * pathInfo2.m_maxSnapDistance)
						{
							controlPoint3.m_position = vector4;
							controlPoint3.m_elevation = 0f;
							controlPoint3.m_node = 0;
							controlPoint3.m_segment = 0;
						}
						else
						{
							controlPoint3.m_position.y = vector4.y;
						}
					}
					else
					{
						controlPoint3.m_position = vector4;
					}
					if (pathInfo2.m_curveTargets != null && pathInfo2.m_curveTargets.Length >= 1)
					{
						Vector3 vector6 = Building.CalculatePosition(position, angle, pathInfo2.m_curveTargets[0]);
						controlPoint3.m_direction = VectorUtils.NormalizeXZ(vector6 - controlPoint3.m_position);
					}
					else if (pathInfo2.m_nodes.Length >= 2)
					{
						Vector3 vector7 = Building.CalculatePosition(position, angle, pathInfo2.m_nodes[1]);
						controlPoint3.m_direction = VectorUtils.NormalizeXZ(vector7 - controlPoint3.m_position);
					}
					else
					{
						controlPoint3.m_direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
					}
					if (controlPoint3.m_node == 0)
					{
						ushort num6;
						ushort num7;
						int num8;
						int num9;
						toolErrors |= NetTool.CreateNode(pathInfo2.m_netInfo, controlPoint3, controlPoint3, controlPoint3, NetTool.m_nodePositionsSimulation, 0, true, false, false, false, pathInfo2.m_invertSegments, false, relocateID, out num6, out num7, out num8, out num9);
					}
					for (int k = 1; k < pathInfo2.m_nodes.Length; k++)
					{
						vector4 = Building.CalculatePosition(position, angle, pathInfo2.m_nodes[k]);
						if (!pathInfo2.m_netInfo.m_useFixedHeight)
						{
							vector4.y = NetSegment.SampleTerrainHeight(pathInfo2.m_netInfo, vector4, false, pathInfo2.m_nodes[k].y + elevation);
						}
						ray2..ctor(vector4 + new Vector3(0f, 8f, 0f), Vector3.get_down());
						NetTool.ControlPoint controlPoint4;
						if (NetTool.MakeControlPoint(ray2, 16f, pathInfo2.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo2.m_nodes[k].y + elevation - pathInfo2.m_netInfo.m_buildHeight, true, out controlPoint4))
						{
							Vector3 vector8 = controlPoint4.m_position - vector4;
							if (!pathInfo2.m_netInfo.m_useFixedHeight)
							{
								vector8.y = 0f;
							}
							float sqrMagnitude3 = vector8.get_sqrMagnitude();
							if (sqrMagnitude3 > pathInfo2.m_maxSnapDistance * pathInfo2.m_maxSnapDistance)
							{
								controlPoint4.m_position = vector4;
								controlPoint4.m_elevation = 0f;
								controlPoint4.m_node = 0;
								controlPoint4.m_segment = 0;
							}
							else
							{
								controlPoint4.m_position.y = vector4.y;
							}
						}
						else
						{
							controlPoint4.m_position = vector4;
						}
						NetTool.ControlPoint middlePoint = controlPoint4;
						if (pathInfo2.m_curveTargets != null && pathInfo2.m_curveTargets.Length >= k)
						{
							middlePoint.m_position = Building.CalculatePosition(position, angle, pathInfo2.m_curveTargets[k - 1]);
							if (!pathInfo2.m_netInfo.m_useFixedHeight)
							{
								middlePoint.m_position.y = NetSegment.SampleTerrainHeight(pathInfo2.m_netInfo, middlePoint.m_position, false, pathInfo2.m_curveTargets[k - 1].y + elevation);
							}
						}
						else
						{
							middlePoint.m_position = (controlPoint3.m_position + controlPoint4.m_position) * 0.5f;
						}
						middlePoint.m_direction = VectorUtils.NormalizeXZ(middlePoint.m_position - controlPoint3.m_position);
						controlPoint4.m_direction = VectorUtils.NormalizeXZ(controlPoint4.m_position - middlePoint.m_position);
						ushort num10;
						ushort num11;
						ushort num12;
						int num13;
						int num14;
						toolErrors |= NetTool.CreateNode(pathInfo2.m_netInfo, controlPoint3, middlePoint, controlPoint4, NetTool.m_nodePositionsSimulation, 1, true, false, false, false, false, pathInfo2.m_invertSegments, false, relocateID, out num10, out num11, out num12, out num13, out num14);
						controlPoint3 = controlPoint4;
						if (flag)
						{
							constructionCost += num13;
						}
					}
				}
			}
		}
		productionRate = 0;
		return toolErrors;
	}

	public virtual bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		min = 0f;
		max = 1f;
		return false;
	}

	protected virtual void GetConstructionCost(bool relocating, out int baseCost, out bool includePaths)
	{
		if (relocating)
		{
			baseCost = this.GetRelocationCost();
		}
		else
		{
			baseCost = this.GetConstructionCost();
		}
		includePaths = false;
	}

	public virtual ToolBase.ToolErrors CheckBulldozing(ushort buildingID, ref Building data)
	{
		return ToolBase.ToolErrors.None;
	}

	public virtual float ElectricityGridRadius()
	{
		return 0f;
	}

	public virtual bool ClearOccupiedZoning()
	{
		return false;
	}

	public virtual bool EnableNotUsedGuide()
	{
		return false;
	}

	public virtual float GetCurrentRange(ushort buildingID, ref Building data)
	{
		return 0f;
	}

	public virtual bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = 0;
		fireSize = 0;
		fireTolerance = 0;
		return false;
	}

	public virtual void BuildingDeactivated(ushort buildingID, ref Building data)
	{
	}

	public virtual void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		BuildingInfo.Prop prop;
		if (this.m_info.m_exitDoors != null && this.m_info.m_exitDoors.Length != 0)
		{
			prop = this.m_info.m_exitDoors[randomizer.Int32((uint)this.m_info.m_exitDoors.Length)];
		}
		else
		{
			prop = null;
		}
		Vector3 offset;
		float num;
		bool flag;
		if (prop != null)
		{
			offset = prop.m_position;
			offset.z = -offset.z;
			num = prop.m_finalProp.m_generatedInfo.m_size.x * 0.5f - info.m_radius;
			flag = prop.m_fixedHeight;
		}
		else
		{
			offset = Vector3.get_zero();
			num = 0f;
			flag = false;
		}
		offset.z += Building.CalculateLocalMeshOffset(this.m_info, data.Length);
		if (num >= 0.1f)
		{
			num *= Mathf.Sqrt((float)randomizer.Int32(1000u) * 0.001f);
			float num2 = (float)randomizer.Int32(1000u) * 0.00628318544f;
			offset.x += Mathf.Cos(num2) * num;
			offset.z += Mathf.Sin(num2) * num;
		}
		position = data.CalculatePosition(offset);
		if (!flag)
		{
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
		}
		else if (this.m_info.m_requireHeightMap)
		{
			position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position) + offset.y;
		}
		if (this.m_info.m_hasPedestrianPaths)
		{
			target = position;
		}
		else
		{
			target = data.CalculateSidewalkPosition(offset.x, 0f);
		}
	}

	public virtual void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == VehicleInfo.VehicleType.Helicopter && this.FindParkingSpace(buildingID, ref data, ref randomizer, info.m_vehicleType, false, out position, out target))
		{
			return;
		}
		position = data.CalculateSidewalkPosition(0f, 3f);
		target = position;
	}

	private bool FindParkingSpace(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo.VehicleType type, bool isElectric, out Vector3 position, out Vector3 target)
	{
		position = Vector3.get_zero();
		target = Vector3.get_zero();
		if ((this.m_info.m_hasParkingSpaces & type) == VehicleInfo.VehicleType.None)
		{
			return false;
		}
		if (this.m_info.m_props == null)
		{
			return false;
		}
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			return false;
		}
		int length = data.Length;
		Matrix4x4 matrix4x = default(Matrix4x4);
		bool flag = false;
		int num = -1;
		for (int i = 0; i < this.m_info.m_props.Length; i++)
		{
			BuildingInfo.Prop prop = this.m_info.m_props[i];
			Randomizer randomizer2;
			randomizer2..ctor((int)buildingID << 6 | prop.m_index);
			if (randomizer2.Int32(100u) < prop.m_probability && length >= prop.m_requiredLength)
			{
				PropInfo propInfo = prop.m_finalProp;
				if (propInfo != null)
				{
					propInfo = propInfo.GetVariation(ref randomizer2);
					if (propInfo.m_parkingSpaces != null && propInfo.m_parkingSpaces.Length != 0)
					{
						int num2 = randomizer.Int32(100u);
						if (num2 > num)
						{
							if (!flag)
							{
								flag = true;
								Vector3 vector = Building.CalculateMeshPosition(this.m_info, data.m_position, data.m_angle, length);
								Quaternion quaternion = Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down());
								matrix4x.SetTRS(vector, quaternion, Vector3.get_one());
							}
							Vector3 propPos = matrix4x.MultiplyPoint(prop.m_position);
							if (BuildingAI.FindParkingSpace(propInfo, propPos, data.m_angle + prop.m_radAngle, prop.m_fixedHeight, ref randomizer, type, isElectric, ref position, ref target))
							{
								num = num2;
							}
						}
					}
				}
			}
		}
		return num != -1;
	}

	private static bool FindParkingSpace(PropInfo info, Vector3 propPos, float angle, bool fixedHeight, ref Randomizer randomizer, VehicleInfo.VehicleType type, bool isElectric, ref Vector3 position, ref Vector3 target)
	{
		Matrix4x4 matrix4x = default(Matrix4x4);
		Quaternion quaternion = Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
		matrix4x.SetTRS(propPos, quaternion, Vector3.get_one());
		int num = -1;
		for (int i = 0; i < info.m_parkingSpaces.Length; i++)
		{
			if ((info.m_parkingSpaces[i].m_type & type) != VehicleInfo.VehicleType.None && (isElectric || (info.m_parkingSpaces[i].m_flags & PropInfo.ParkingFlags.RequireElectric) == (PropInfo.ParkingFlags)0))
			{
				int num2 = randomizer.Int32(100u);
				if (num2 > num)
				{
					position = matrix4x.MultiplyPoint(info.m_parkingSpaces[i].m_position);
					Vector3 vector = matrix4x.MultiplyVector(info.m_parkingSpaces[i].m_direction);
					target = position + vector;
					num = num2;
				}
			}
		}
		return num != -1;
	}

	private bool IsSomeBodyThere(Vector3 position, ushort ignoreInstance)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		int num = Mathf.Max((int)((position.x - 0.5f) / 8f + 1080f), 0);
		int num2 = Mathf.Max((int)((position.z - 0.5f) / 8f + 1080f), 0);
		int num3 = Mathf.Min((int)((position.x + 0.5f) / 8f + 1080f), 2159);
		int num4 = Mathf.Min((int)((position.z + 0.5f) / 8f + 1080f), 2159);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_citizenGrid[i * 2160 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if (num5 != ignoreInstance)
					{
						Vector3 vector = instance.m_instances.m_buffer[(int)num5].m_targetPos;
						if (Vector3.SqrMagnitude(vector - position) < 0.01f)
						{
							return true;
						}
					}
					num5 = instance.m_instances.m_buffer[(int)num5].m_nextGridInstance;
					if (++num6 > 65536)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return false;
	}

	public virtual void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		float num = Building.CalculateLocalMeshOffset(this.m_info, data.Length);
		bool flag = this.m_info.m_specialPlaces != null && this.m_info.m_specialPlaces.Length != 0;
		bool flag2 = this.m_info.m_enterDoors != null && this.m_info.m_enterDoors.Length != 0;
		if (flag && (!flag2 || randomizer.Int32(4u) == 0))
		{
			int num2 = randomizer.Int32((uint)this.m_info.m_specialPlaces.Length);
			BuildingInfo.Prop prop = this.m_info.m_specialPlaces[num2];
			Randomizer randomizer2;
			randomizer2..ctor((int)buildingID << 6 | prop.m_index);
			if (randomizer2.Int32(100u) < prop.m_probability && data.Length >= prop.m_requiredLength)
			{
				Vector3 position2 = prop.m_position;
				position2.z = num - position2.z;
				position = data.CalculatePosition(position2);
				int num3 = randomizer.Int32((uint)prop.m_finalProp.m_specialPlaces.Length);
				PropInfo.SpecialPlace specialPlace = prop.m_finalProp.m_specialPlaces[num3];
				float num4 = data.m_angle + prop.m_radAngle;
				float num5 = Mathf.Cos(num4);
				float num6 = Mathf.Sin(num4);
				if (!prop.m_fixedHeight)
				{
					position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
				}
				else if (this.m_info.m_requireHeightMap)
				{
					position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position) + position2.y;
				}
				position.x += specialPlace.m_position.x * num5 + specialPlace.m_position.z * num6;
				position.y += specialPlace.m_position.y;
				position.z += specialPlace.m_position.x * num6 - specialPlace.m_position.z * num5;
				if (!this.IsSomeBodyThere(position, ignoreInstance))
				{
					direction.x = specialPlace.m_direction.x * num5 + specialPlace.m_direction.z * num6;
					direction.y = specialPlace.m_direction.x * num6 - specialPlace.m_direction.z * num5;
					specialFlags = (CitizenInstance.Flags.HangAround | specialPlace.m_specialFlags);
					if (this.m_info.m_hasPedestrianPaths)
					{
						target = position;
					}
					else
					{
						target = data.CalculateSidewalkPosition(position2.x, 0f);
					}
					return;
				}
			}
		}
		if (flag2)
		{
			int num7 = randomizer.Int32((uint)this.m_info.m_enterDoors.Length);
			BuildingInfo.Prop prop2 = this.m_info.m_enterDoors[num7];
			Randomizer randomizer3;
			randomizer3..ctor((int)buildingID << 6 | prop2.m_index);
			if (randomizer3.Int32(100u) < prop2.m_probability && data.Length >= prop2.m_requiredLength)
			{
				Vector3 position3 = prop2.m_position;
				position3.z = num - position3.z;
				float num8 = prop2.m_finalProp.m_generatedInfo.m_size.x * 0.5f - info.m_radius;
				if (num8 >= 0.1f)
				{
					num8 *= Mathf.Sqrt((float)randomizer.Int32(1000u) * 0.001f);
					float num9 = (float)randomizer.Int32(1000u) * 0.00628318544f;
					position3.x += Mathf.Cos(num9) * num8;
					position3.z += Mathf.Sin(num9) * num8;
				}
				position = data.CalculatePosition(position3);
				direction = Vector2.get_zero();
				if ((prop2.m_finalProp.m_doorType & PropInfo.DoorType.HangAround) == PropInfo.DoorType.HangAround)
				{
					if (data.m_eventIndex != 0)
					{
						EventManager instance = Singleton<EventManager>.get_instance();
						EventInfo info2 = instance.m_events.m_buffer[(int)data.m_eventIndex].Info;
						specialFlags = info2.m_eventAI.GetHangAroundFlags(data.m_eventIndex, ref instance.m_events.m_buffer[(int)data.m_eventIndex], buildingID, ref position, ref direction);
					}
					else
					{
						specialFlags = CitizenInstance.Flags.HangAround;
					}
				}
				else
				{
					specialFlags = CitizenInstance.Flags.None;
				}
				if (!prop2.m_fixedHeight)
				{
					position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
				}
				else if (this.m_info.m_requireHeightMap)
				{
					position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position) + position3.y;
				}
				if (this.m_info.m_hasPedestrianPaths)
				{
					target = position;
				}
				else
				{
					target = data.CalculateSidewalkPosition(position3.x, 0f);
				}
				return;
			}
		}
		Vector3 offset;
		offset..ctor(0f, 0f, num);
		position = data.CalculatePosition(offset);
		position.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
		direction = Vector2.get_zero();
		specialFlags = CitizenInstance.Flags.None;
		if (this.m_info.m_hasPedestrianPaths)
		{
			target = position;
		}
		else
		{
			target = data.CalculateSidewalkPosition(offset.x, 0f);
		}
	}

	public virtual Vector2 GetInterestDirection(ushort buildingID, ref Building data, Vector3 pos)
	{
		return Vector2.get_zero();
	}

	public virtual void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == VehicleInfo.VehicleType.Helicopter && this.FindParkingSpace(buildingID, ref data, ref randomizer, info.m_vehicleType, false, out position, out target))
		{
			return;
		}
		position = data.CalculateSidewalkPosition(0f, 3f);
		target = position;
	}

	public virtual void SetProductionRate(ushort buildingID, ref Building data, byte rate)
	{
	}

	public virtual void PlacementFailed()
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_buildNextToRoad.Activate(properties.m_buildNextToRoad);
		}
	}

	public virtual void PlacementSucceeded()
	{
		Singleton<BuildingManager>.get_instance().m_buildNextToRoad.Deactivate();
	}

	public virtual void UpdateGuide(GuideController guideController)
	{
		if (this.m_info.m_notUsedGuide != null)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				this.m_info.m_notUsedGuide.Activate(guideController.m_buildingNotUsed, this.m_info);
			}
		}
	}

	protected void EnsureCitizenUnits(ushort buildingID, ref Building data, int homeCount, int workCount, int visitCount, int studentCount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) == Building.Flags.None)
		{
			Citizen.Wealth wealthLevel = Citizen.GetWealthLevel(this.m_info.m_class.m_level);
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = 0u;
			uint num2 = data.m_citizenUnits;
			int num3 = 0;
			while (num2 != 0u)
			{
				CitizenUnit.Flags flags = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_flags;
				if ((ushort)(flags & CitizenUnit.Flags.Home) != 0)
				{
					instance.m_units.m_buffer[(int)((UIntPtr)num2)].SetWealthLevel(wealthLevel);
					homeCount--;
				}
				if ((ushort)(flags & CitizenUnit.Flags.Work) != 0)
				{
					workCount -= 5;
				}
				if ((ushort)(flags & CitizenUnit.Flags.Visit) != 0)
				{
					visitCount -= 5;
				}
				if ((ushort)(flags & CitizenUnit.Flags.Student) != 0)
				{
					studentCount -= 5;
				}
				num = num2;
				num2 = instance.m_units.m_buffer[(int)((UIntPtr)num2)].m_nextUnit;
				if (++num3 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			homeCount = Mathf.Max(0, homeCount);
			workCount = Mathf.Max(0, workCount);
			visitCount = Mathf.Max(0, visitCount);
			studentCount = Mathf.Max(0, studentCount);
			if (homeCount != 0 || workCount != 0 || visitCount != 0 || studentCount != 0)
			{
				uint num4 = 0u;
				if (instance.CreateUnits(out num4, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, homeCount, workCount, visitCount, 0, studentCount))
				{
					if (num != 0u)
					{
						instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit = num4;
					}
					else
					{
						data.m_citizenUnits = num4;
					}
				}
			}
		}
	}

	public virtual void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
	}

	public virtual float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		return 0f;
	}

	public virtual float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		return 0f;
	}

	public virtual bool CalculateGroupData(ushort buildingID, ref Building buildingData, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		if (this.CalculatePropGroupData(buildingID, ref buildingData, true, true, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
		{
			result = true;
		}
		if (this.m_info.m_prefabDataLayer == layer)
		{
			result = true;
			if (this.m_info.m_buildingAI.CanUseGroupMesh(buildingID, ref buildingData))
			{
				if (this.m_info.m_lodMeshData != null)
				{
					vertexCount += this.m_info.m_lodMeshData.m_vertices.Length;
					triangleCount += this.m_info.m_lodMeshData.m_triangles.Length;
					objectCount++;
					vertexArrays |= (this.m_info.m_lodMeshData.VertexArrayMask() | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs2);
					if (this.m_info.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
					{
						vertexCount += this.m_info.m_lodMeshBase.m_vertices.Length;
						triangleCount += this.m_info.m_lodMeshBase.m_triangles.Length;
					}
				}
				if (this.m_info.m_subMeshes != null)
				{
					for (int i = 0; i < this.m_info.m_subMeshes.Length; i++)
					{
						BuildingInfo.MeshInfo meshInfo = this.m_info.m_subMeshes[i];
						if (((meshInfo.m_flagsRequired | meshInfo.m_flagsForbidden) & buildingData.m_flags) == meshInfo.m_flagsRequired)
						{
							BuildingInfoBase subInfo = meshInfo.m_subInfo;
							if (subInfo.m_lodMeshData != null)
							{
								vertexCount += subInfo.m_lodMeshData.m_vertices.Length;
								triangleCount += subInfo.m_lodMeshData.m_triangles.Length;
								objectCount++;
								vertexArrays |= (subInfo.m_lodMeshData.VertexArrayMask() | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs2);
								if (subInfo.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
								{
									vertexCount += subInfo.m_lodMeshBase.m_vertices.Length;
									triangleCount += subInfo.m_lodMeshBase.m_triangles.Length;
								}
							}
						}
					}
				}
			}
		}
		return result;
	}

	protected virtual bool CalculatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		if (props)
		{
			Randomizer randomizer;
			randomizer..ctor((int)buildingID);
			PropInfo randomPropInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer, ItemClass.Service.Garbage);
			if (randomPropInfo != null && randomPropInfo.m_prefabDataLayer == layer)
			{
				for (int i = 0; i < 8; i++)
				{
					if (PropInstance.CalculateGroupData(randomPropInfo, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
				}
			}
		}
		if (this.m_info.m_props != null && !this.GetPropRenderID(buildingID, 0, ref buildingData).IsEmpty)
		{
			LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
			int length = buildingData.Length;
			for (int j = 0; j < this.m_info.m_props.Length; j++)
			{
				Randomizer randomizer2;
				randomizer2..ctor((int)buildingID << 6 | this.m_info.m_props[j].m_index);
				if (randomizer2.Int32(100u) < this.m_info.m_props[j].m_probability && length >= this.m_info.m_props[j].m_requiredLength)
				{
					PropInfo propInfo = this.m_info.m_props[j].m_finalProp;
					TreeInfo treeInfo = this.m_info.m_props[j].m_finalTree;
					if (propInfo != null)
					{
						propInfo = propInfo.GetVariation(ref randomizer2);
						int num = layer;
						if (layer == lightSystem.m_lightLayer)
						{
							if (this.m_info.m_isFloating && this.m_info.m_props[j].m_fixedHeight)
							{
								goto IL_235;
							}
						}
						else if (layer == lightSystem.m_lightLayerFloating)
						{
							if (!this.m_info.m_isFloating || !this.m_info.m_props[j].m_fixedHeight)
							{
								goto IL_235;
							}
							num = lightSystem.m_lightLayer;
						}
						if ((propInfo.m_prefabDataLayer == layer || propInfo.m_effectLayer == num) && props && PropInstance.CalculateGroupData(propInfo, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
						{
							result = true;
						}
					}
					else if (treeInfo != null)
					{
						treeInfo = treeInfo.GetVariation(ref randomizer2);
						if (treeInfo.m_prefabDataLayer == layer && trees && TreeInstance.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
						{
							result = true;
						}
					}
				}
				IL_235:;
			}
		}
		return result;
	}

	public virtual void PopulateGroupData(ushort buildingID, ref Building buildingData, ref float height, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		this.PopulatePropGroupData(buildingID, ref buildingData, true, true, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		if (this.m_info.m_prefabDataLayer == layer)
		{
			int width = buildingData.Width;
			int length = buildingData.Length;
			Vector3 vector = new Vector3(Mathf.Cos(buildingData.m_angle), 0f, Mathf.Sin(buildingData.m_angle)) * 8f;
			Vector3 vector2;
			vector2..ctor(vector.z, 0f, -vector.x);
			Vector3 vector3 = buildingData.m_position - (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
			Vector3 vector4 = buildingData.m_position + (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
			Vector3 vector5 = buildingData.m_position + (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
			Vector3 vector6 = buildingData.m_position - (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
			float num = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x));
			float num2 = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z));
			float num3 = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x));
			float num4 = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z));
			min = Vector3.Min(min, new Vector3(num, buildingData.m_position.y - (float)buildingData.m_baseHeight, num2));
			max = Vector3.Max(max, new Vector3(num3, buildingData.m_position.y + this.m_info.m_size.y, num4));
			maxRenderDistance = Mathf.Max(maxRenderDistance, 20000f);
			maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
			if (this.m_info.m_buildingAI.CanUseGroupMesh(buildingID, ref buildingData))
			{
				if (this.m_info.m_lodMeshData != null)
				{
					Vector3 vector7;
					Quaternion quaternion;
					buildingData.CalculateMeshPosition(out vector7, out quaternion);
					Matrix4x4 matrix4x = Matrix4x4.TRS(vector7 - groupPosition, quaternion, Vector3.get_one());
					Vector4 colorLocation = RenderManager.GetColorLocation((uint)buildingID);
					Vector2 vector8;
					vector8..ctor(colorLocation.x, colorLocation.y);
					int[] triangles = this.m_info.m_lodMeshData.m_triangles;
					int num5 = triangles.Length;
					for (int i = 0; i < num5; i++)
					{
						data.m_triangles[triangleIndex++] = triangles[i] + vertexIndex;
					}
					RenderGroup.VertexArrays vertexArrays = this.m_info.m_lodMeshData.VertexArrayMask();
					Vector3[] vertices = this.m_info.m_lodMeshData.m_vertices;
					Vector3[] normals = this.m_info.m_lodMeshData.m_normals;
					Vector4[] tangents = this.m_info.m_lodMeshData.m_tangents;
					Vector2[] uvs = this.m_info.m_lodMeshData.m_uvs;
					Vector2[] uvs2 = this.m_info.m_lodMeshData.m_uvs3;
					Vector2[] uvs3 = this.m_info.m_lodMeshData.m_uvs4;
					Color32[] colors = this.m_info.m_lodMeshData.m_colors;
					int num6 = vertices.Length;
					for (int j = 0; j < num6; j++)
					{
						Color32 groupVertexColor = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, this.m_info, j, false);
						Vector3 vector9 = vertices[j];
						Vector3 vector10 = matrix4x.MultiplyPoint(vector9);
						if (this.m_info.m_requireHeightMap)
						{
							vector10.y = vector9.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector10 + groupPosition);
						}
						data.m_vertices[vertexIndex] = vector10;
						if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
						{
							data.m_normals[vertexIndex] = matrix4x.MultiplyVector(normals[j]);
						}
						else
						{
							data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
						}
						if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
						{
							Vector4 vector11 = tangents[j];
							Vector3 vector12 = matrix4x.MultiplyVector(vector11);
							vector11.x = vector12.x;
							vector11.y = vector12.y;
							vector11.z = vector12.z;
							data.m_tangents[vertexIndex] = vector11;
						}
						else
						{
							data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
						}
						if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
						{
							data.m_uvs[vertexIndex] = uvs[j];
						}
						else
						{
							data.m_uvs[vertexIndex] = Vector2.get_zero();
						}
						data.m_colors[vertexIndex] = groupVertexColor;
						data.m_uvs2[vertexIndex] = vector8;
						Color32 color = colors[j];
						Vector2 vector13 = uvs3[j];
						if (color.r == 255 && color.g == 255 && color.b <= 5)
						{
							vector13.x = -vector13.x;
						}
						data.m_uvs3[vertexIndex] = uvs2[j];
						data.m_uvs4[vertexIndex] = vector13;
						vertexIndex++;
					}
					if (this.m_info.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
					{
						triangles = this.m_info.m_lodMeshBase.m_triangles;
						num5 = triangles.Length;
						for (int k = 0; k < num5; k++)
						{
							data.m_triangles[triangleIndex++] = triangles[k] + vertexIndex;
						}
						vertexArrays = this.m_info.m_lodMeshBase.VertexArrayMask();
						vertices = this.m_info.m_lodMeshBase.m_vertices;
						normals = this.m_info.m_lodMeshBase.m_normals;
						tangents = this.m_info.m_lodMeshBase.m_tangents;
						uvs = this.m_info.m_lodMeshBase.m_uvs;
						uvs2 = this.m_info.m_lodMeshBase.m_uvs3;
						uvs3 = this.m_info.m_lodMeshBase.m_uvs4;
						colors = this.m_info.m_lodMeshBase.m_colors;
						num6 = vertices.Length;
						for (int l = 0; l < num6; l++)
						{
							Color32 groupVertexColor2 = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, this.m_info, l, true);
							Vector3 vector14 = vertices[l];
							vector14.y *= (float)buildingData.m_baseHeight;
							Vector3 vector15 = matrix4x.MultiplyPoint(vector14);
							if (this.m_info.m_requireHeightMap)
							{
								vector15.y = vector14.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector15 + groupPosition);
							}
							data.m_vertices[vertexIndex] = vector15;
							if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
							{
								data.m_normals[vertexIndex] = matrix4x.MultiplyVector(normals[l]);
							}
							else
							{
								data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
							}
							if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
							{
								Vector4 vector16 = tangents[l];
								Vector3 vector17 = matrix4x.MultiplyVector(vector16);
								vector16.x = vector17.x;
								vector16.y = vector17.y;
								vector16.z = vector17.z;
								data.m_tangents[vertexIndex] = vector16;
							}
							else
							{
								data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
							}
							if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
							{
								Vector2 vector18 = uvs[l];
								vector18.y *= (float)buildingData.m_baseHeight;
								data.m_uvs[vertexIndex] = vector18;
							}
							else
							{
								data.m_uvs[vertexIndex] = Vector2.get_zero();
							}
							data.m_colors[vertexIndex] = groupVertexColor2;
							data.m_uvs2[vertexIndex] = vector8;
							Color32 color2 = colors[l];
							Vector2 vector19 = uvs3[l];
							if (color2.r == 255 && color2.g == 255 && color2.b <= 5)
							{
								vector19.x = -vector19.x;
							}
							data.m_uvs3[vertexIndex] = uvs2[l];
							data.m_uvs4[vertexIndex] = vector19;
							vertexIndex++;
						}
					}
				}
				if (this.m_info.m_subMeshes != null)
				{
					for (int m = 0; m < this.m_info.m_subMeshes.Length; m++)
					{
						BuildingInfo.MeshInfo meshInfo = this.m_info.m_subMeshes[m];
						if (((meshInfo.m_flagsRequired | meshInfo.m_flagsForbidden) & buildingData.m_flags) == meshInfo.m_flagsRequired)
						{
							BuildingInfoBase subInfo = meshInfo.m_subInfo;
							if (subInfo.m_lodMeshData != null)
							{
								Vector3 vector20;
								Quaternion quaternion2;
								buildingData.CalculateMeshPosition(out vector20, out quaternion2);
								Matrix4x4 matrix4x2 = Matrix4x4.TRS(vector20 - groupPosition, quaternion2, Vector3.get_one()) * meshInfo.m_matrix;
								Vector4 colorLocation2 = RenderManager.GetColorLocation((uint)buildingID);
								Vector2 vector21;
								vector21..ctor(colorLocation2.x, colorLocation2.y);
								int[] triangles2 = subInfo.m_lodMeshData.m_triangles;
								int num7 = triangles2.Length;
								for (int n = 0; n < num7; n++)
								{
									data.m_triangles[triangleIndex++] = triangles2[n] + vertexIndex;
								}
								RenderGroup.VertexArrays vertexArrays2 = subInfo.m_lodMeshData.VertexArrayMask();
								Vector3[] vertices2 = subInfo.m_lodMeshData.m_vertices;
								Vector3[] normals2 = subInfo.m_lodMeshData.m_normals;
								Vector4[] tangents2 = subInfo.m_lodMeshData.m_tangents;
								Vector2[] uvs4 = subInfo.m_lodMeshData.m_uvs;
								Vector2[] uvs5 = subInfo.m_lodMeshData.m_uvs3;
								Vector2[] uvs6 = subInfo.m_lodMeshData.m_uvs4;
								Color32[] colors2 = subInfo.m_lodMeshData.m_colors;
								int num8 = vertices2.Length;
								for (int num9 = 0; num9 < num8; num9++)
								{
									Color32 groupVertexColor3 = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, subInfo, num9, false);
									Vector3 vector22 = vertices2[num9];
									Vector3 vector23 = matrix4x2.MultiplyPoint(vector22);
									if (subInfo.m_requireHeightMap)
									{
										vector23.y = vector22.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector23 + groupPosition);
									}
									data.m_vertices[vertexIndex] = vector23;
									if ((vertexArrays2 & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
									{
										data.m_normals[vertexIndex] = matrix4x2.MultiplyVector(normals2[num9]);
									}
									else
									{
										data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
									}
									if ((vertexArrays2 & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
									{
										Vector4 vector24 = tangents2[num9];
										Vector3 vector25 = matrix4x2.MultiplyVector(vector24);
										vector24.x = vector25.x;
										vector24.y = vector25.y;
										vector24.z = vector25.z;
										data.m_tangents[vertexIndex] = vector24;
									}
									else
									{
										data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
									}
									if ((vertexArrays2 & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
									{
										data.m_uvs[vertexIndex] = uvs4[num9];
									}
									else
									{
										data.m_uvs[vertexIndex] = Vector2.get_zero();
									}
									data.m_colors[vertexIndex] = groupVertexColor3;
									data.m_uvs2[vertexIndex] = vector21;
									Color32 color3 = colors2[num9];
									Vector2 vector26 = uvs6[num9];
									if (color3.r == 255 && color3.g == 255 && color3.b <= 5)
									{
										vector26.x = -vector26.x;
									}
									data.m_uvs3[vertexIndex] = uvs5[num9];
									data.m_uvs4[vertexIndex] = vector26;
									vertexIndex++;
								}
								if (subInfo.m_lodMeshBase != null && buildingData.m_baseHeight >= 3)
								{
									triangles2 = subInfo.m_lodMeshBase.m_triangles;
									num7 = triangles2.Length;
									for (int num10 = 0; num10 < num7; num10++)
									{
										data.m_triangles[triangleIndex++] = triangles2[num10] + vertexIndex;
									}
									vertexArrays2 = subInfo.m_lodMeshBase.VertexArrayMask();
									vertices2 = subInfo.m_lodMeshBase.m_vertices;
									normals2 = subInfo.m_lodMeshBase.m_normals;
									tangents2 = subInfo.m_lodMeshBase.m_tangents;
									uvs4 = subInfo.m_lodMeshBase.m_uvs;
									uvs5 = subInfo.m_lodMeshBase.m_uvs3;
									uvs6 = subInfo.m_lodMeshBase.m_uvs4;
									colors2 = subInfo.m_lodMeshBase.m_colors;
									num8 = vertices2.Length;
									for (int num11 = 0; num11 < num8; num11++)
									{
										Color32 groupVertexColor4 = this.m_info.m_buildingAI.GetGroupVertexColor(buildingID, ref buildingData, subInfo, num11, true);
										Vector3 vector27 = vertices2[num11];
										vector27.y *= (float)buildingData.m_baseHeight;
										Vector3 vector28 = matrix4x2.MultiplyPoint(vector27);
										if (subInfo.m_requireHeightMap)
										{
											vector28.y = vector27.y + Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector28 + groupPosition);
										}
										data.m_vertices[vertexIndex] = vector28;
										if ((vertexArrays2 & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
										{
											data.m_normals[vertexIndex] = matrix4x2.MultiplyVector(normals2[num11]);
										}
										else
										{
											data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
										}
										if ((vertexArrays2 & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
										{
											Vector4 vector29 = tangents2[num11];
											Vector3 vector30 = matrix4x2.MultiplyVector(vector29);
											vector29.x = vector30.x;
											vector29.y = vector30.y;
											vector29.z = vector30.z;
											data.m_tangents[vertexIndex] = vector29;
										}
										else
										{
											data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
										}
										if ((vertexArrays2 & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
										{
											Vector2 vector31 = uvs4[num11];
											vector31.y *= (float)buildingData.m_baseHeight;
											data.m_uvs[vertexIndex] = vector31;
										}
										else
										{
											data.m_uvs[vertexIndex] = Vector2.get_zero();
										}
										data.m_colors[vertexIndex] = groupVertexColor4;
										data.m_uvs2[vertexIndex] = vector21;
										Color32 color4 = colors2[num11];
										Vector2 vector32 = uvs6[num11];
										if (color4.r == 255 && color4.g == 255 && color4.b <= 5)
										{
											vector32.x = -vector32.x;
										}
										data.m_uvs3[vertexIndex] = uvs5[num11];
										data.m_uvs4[vertexIndex] = vector32;
										vertexIndex++;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	protected virtual void PopulatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if (props)
		{
			Randomizer randomizer;
			randomizer..ctor((int)buildingID);
			PropInfo randomPropInfo = Singleton<PropManager>.get_instance().GetRandomPropInfo(ref randomizer, ItemClass.Service.Garbage);
			if (randomPropInfo != null && randomPropInfo.m_prefabDataLayer == layer)
			{
				int width = buildingData.Width;
				int length = buildingData.Length;
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(buildingData.m_position, Quaternion.AngleAxis(buildingData.m_angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
				for (int i = 0; i < 8; i++)
				{
					PropInfo variation = randomPropInfo.GetVariation(ref randomizer);
					float scale = variation.m_minScale + (float)randomizer.Int32(10000u) * (variation.m_maxScale - variation.m_minScale) * 0.0001f;
					float angle = (float)randomizer.Int32(10000u) * 0.0006283185f;
					Color color = variation.GetColor(ref randomizer);
					Vector3 vector;
					vector.x = ((float)randomizer.Int32(10000u) * 0.0001f - 0.5f) * (float)width * 4f;
					vector.y = 0f;
					vector.z = (float)randomizer.Int32(10000u) * 0.0001f - 0.5f + (float)length * 4f;
					vector = matrix4x.MultiplyPoint(vector);
					vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
					PropInstance.PopulateGroupData(variation, layer, new InstanceID
					{
						Building = buildingID
					}, vector, scale, angle, color, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
				}
			}
		}
		if (this.m_info.m_props != null && !this.GetPropRenderID(buildingID, 0, ref buildingData).IsEmpty)
		{
			LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
			int length2 = buildingData.Length;
			Matrix4x4 matrix4x2 = default(Matrix4x4);
			matrix4x2.SetTRS(Building.CalculateMeshPosition(this.m_info, buildingData.m_position, buildingData.m_angle, length2), Quaternion.AngleAxis(buildingData.m_angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
			for (int j = 0; j < this.m_info.m_props.Length; j++)
			{
				Randomizer randomizer2;
				randomizer2..ctor((int)buildingID << 6 | this.m_info.m_props[j].m_index);
				if (randomizer2.Int32(100u) < this.m_info.m_props[j].m_probability && length2 >= this.m_info.m_props[j].m_requiredLength)
				{
					PropInfo propInfo = this.m_info.m_props[j].m_finalProp;
					TreeInfo treeInfo = this.m_info.m_props[j].m_finalTree;
					if (propInfo != null)
					{
						propInfo = propInfo.GetVariation(ref randomizer2);
						float scale2 = propInfo.m_minScale + (float)randomizer2.Int32(10000u) * (propInfo.m_maxScale - propInfo.m_minScale) * 0.0001f;
						Color color2 = propInfo.GetColor(ref randomizer2);
						int num = layer;
						if (layer == lightSystem.m_lightLayer)
						{
							if (this.m_info.m_isFloating && this.m_info.m_props[j].m_fixedHeight)
							{
								goto IL_591;
							}
						}
						else if (layer == lightSystem.m_lightLayerFloating)
						{
							if (!this.m_info.m_isFloating || !this.m_info.m_props[j].m_fixedHeight)
							{
								goto IL_591;
							}
							num = lightSystem.m_lightLayer;
						}
						if ((propInfo.m_prefabDataLayer == layer || propInfo.m_effectLayer == num) && props)
						{
							Vector3 vector2 = matrix4x2.MultiplyPoint(this.m_info.m_props[j].m_position);
							if (!this.m_info.m_props[j].m_fixedHeight)
							{
								vector2.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector2);
							}
							else if (this.m_info.m_requireHeightMap && !this.m_info.m_isFloating)
							{
								vector2.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector2) + this.m_info.m_props[j].m_position.y;
							}
							InstanceID propRenderID = this.GetPropRenderID(buildingID, j, ref buildingData);
							PropInstance.PopulateGroupData(propInfo, layer, propRenderID, vector2, scale2, buildingData.m_angle + this.m_info.m_props[j].m_radAngle, color2, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
						}
					}
					else if (treeInfo != null)
					{
						treeInfo = treeInfo.GetVariation(ref randomizer2);
						float scale3 = treeInfo.m_minScale + (float)randomizer2.Int32(10000u) * (treeInfo.m_maxScale - treeInfo.m_minScale) * 0.0001f;
						float brightness = treeInfo.m_minBrightness + (float)randomizer2.Int32(10000u) * (treeInfo.m_maxBrightness - treeInfo.m_minBrightness) * 0.0001f;
						if (treeInfo.m_prefabDataLayer == layer && trees)
						{
							Vector3 vector3 = matrix4x2.MultiplyPoint(this.m_info.m_props[j].m_position);
							if (!this.m_info.m_props[j].m_fixedHeight)
							{
								vector3.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector3);
							}
							else if (this.m_info.m_requireHeightMap)
							{
								vector3.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector3) + this.m_info.m_props[j].m_position.y;
							}
							TreeInstance.PopulateGroupData(treeInfo, vector3, scale3, brightness, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
						}
					}
				}
				IL_591:;
			}
		}
	}

	public virtual void CheckRoadAccess(ushort buildingID, ref Building data)
	{
	}

	public virtual void GetTerrainLimits(ushort buildingID, ref Building data, Vector3 meshPosition, out float min, out float max)
	{
		min = meshPosition.y - 255f;
		max = 1000000f;
	}

	public virtual bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		return false;
	}

	public virtual bool BurnBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly)
	{
		return false;
	}

	public virtual bool NearObjectInFire(ushort buildingID, ref Building data, InstanceID itemID, Vector3 itemPos)
	{
		return false;
	}

	public virtual float MaxFireDetectDistance(ushort buildingID, ref Building data)
	{
		return 0f;
	}

	public virtual bool FindNextEventInfo(ushort buildingID, EventInfo prevEvent, uint prevExpireFrame, out EventInfo nextEvent, out uint nextStartFrame)
	{
		nextEvent = null;
		nextStartFrame = 0u;
		return false;
	}

	public virtual void GetWidthRange(out int minWidth, out int maxWidth)
	{
		switch (this.m_info.m_cellLength)
		{
		case 9:
			minWidth = 1;
			maxWidth = 15;
			break;
		case 10:
			minWidth = 1;
			maxWidth = 14;
			break;
		case 11:
			minWidth = 1;
			maxWidth = 14;
			break;
		case 12:
			minWidth = 1;
			maxWidth = 13;
			break;
		case 13:
			minWidth = 1;
			maxWidth = 12;
			break;
		case 14:
			minWidth = 1;
			maxWidth = 11;
			break;
		case 15:
			minWidth = 1;
			maxWidth = 9;
			break;
		case 16:
			minWidth = 1;
			maxWidth = 8;
			break;
		default:
			minWidth = 1;
			maxWidth = 16;
			break;
		}
	}

	public virtual void GetLengthRange(out int minLength, out int maxLength)
	{
		switch (this.m_info.m_cellWidth)
		{
		case 9:
			minLength = 1;
			maxLength = 15;
			break;
		case 10:
			minLength = 1;
			maxLength = 14;
			break;
		case 11:
			minLength = 1;
			maxLength = 14;
			break;
		case 12:
			minLength = 1;
			maxLength = 13;
			break;
		case 13:
			minLength = 1;
			maxLength = 12;
			break;
		case 14:
			minLength = 1;
			maxLength = 11;
			break;
		case 15:
			minLength = 1;
			maxLength = 9;
			break;
		case 16:
			minLength = 1;
			maxLength = 8;
			break;
		default:
			minLength = 1;
			maxLength = 16;
			break;
		}
	}

	public virtual void GetDecorationArea(out int width, out int length, out float offset)
	{
		width = this.m_info.m_cellWidth;
		length = this.m_info.m_cellLength;
		offset = 0f;
	}

	public virtual void GetDecorationDirections(out bool negX, out bool posX, out bool negZ, out bool posZ)
	{
		negX = false;
		posX = false;
		negZ = false;
		posZ = false;
	}

	public virtual bool IsFull(ushort buildingID, ref Building data)
	{
		return false;
	}

	public virtual bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		return false;
	}

	public virtual bool CanBeEmptied()
	{
		return false;
	}

	public virtual bool CanBeEmptied(ushort buildingID, ref Building data)
	{
		return false;
	}

	public virtual bool IsWonder()
	{
		return false;
	}

	public virtual bool IsMarker()
	{
		return false;
	}

	public virtual TransportInfo GetTransportLineInfo()
	{
		return null;
	}

	public virtual TransportInfo GetSecondaryTransportLineInfo()
	{
		return null;
	}

	public virtual void GetElectricityProduction(out int min, out int max)
	{
		min = 0;
		max = 0;
	}

	public virtual void GetHeatingProduction(out int min, out int max)
	{
		min = 0;
		max = 0;
	}

	public virtual int GetWaterConsumption()
	{
		return 0;
	}

	public virtual int GetElectricityConsumption()
	{
		return 0;
	}

	public virtual void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = 0;
	}

	public virtual bool GetEducationLevel1()
	{
		return false;
	}

	public virtual bool GetEducationLevel2()
	{
		return false;
	}

	public virtual bool GetEducationLevel3()
	{
		return false;
	}

	public virtual bool CanBeBuilt()
	{
		return true;
	}

	public virtual int GetConstructionCost()
	{
		return 0;
	}

	public virtual int GetMaintenanceCost()
	{
		return 0;
	}

	public virtual int GetRelocationCost()
	{
		int constructionCost = this.GetConstructionCost();
		int result = constructionCost / 5;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetRelocationCost(constructionCost, ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public virtual int GetRefundAmount(ushort buildingID, ref Building data)
	{
		int constructionCost = this.GetConstructionCost();
		int result = constructionCost * 3 / 4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetRefundAmount(constructionCost, ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public virtual string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex != -1)
		{
			string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
			return Locale.Get("BUILDING_TITLE", text);
		}
		return null;
	}

	public override string GetLocalizedTooltip()
	{
		int maintenanceCost = this.GetMaintenanceCost();
		return TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Cost,
			LocaleFormatter.FormatCost(this.GetConstructionCost(), false),
			LocaleFormatter.Upkeep,
			LocaleFormatter.FormatUpkeep(maintenanceCost, false),
			LocaleFormatter.IsUpkeepVisible,
			(maintenanceCost != 0).ToString()
		});
	}

	public virtual string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		return null;
	}

	public virtual MilestoneInfo GetUnlockMilestone()
	{
		return this.m_info.m_UnlockMilestone;
	}

	public override bool WorksAsBuilding()
	{
		return true;
	}

	public virtual bool AllowOverlap(BuildingInfo other)
	{
		return false;
	}

	public virtual bool SupportEvents(EventManager.EventType types, EventManager.EventGroup groups)
	{
		return false;
	}

	public virtual ItemClass.CollisionType GetCollisionType()
	{
		return ItemClass.CollisionType.Terrain;
	}

	public virtual bool IgnoreBuildingCollision()
	{
		return false;
	}

	[NonSerialized]
	public BuildingInfo m_info;

	public enum PathFindType
	{
		EnteringCargo,
		LeavingCargo,
		EnteringHuman,
		LeavingHuman,
		EnteringDummy,
		LeavingDummy,
		EnteringTransport,
		LeavingTransport,
		VirtualCargo
	}
}
