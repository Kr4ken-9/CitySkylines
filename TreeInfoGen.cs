﻿using System;
using UnityEngine;

[Serializable]
public class TreeInfoGen : ScriptableObject
{
	public Vector3 m_center;

	public Vector3 m_size;

	public float m_triangleArea;

	public float m_uvmapArea;

	[NonSerialized]
	public TreeInfo m_treeInfo;
}
