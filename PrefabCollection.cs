﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using UnityEngine;

public static class PrefabCollection<T> where T : PrefabInfo
{
	public static int LoadedCount()
	{
		return PrefabCollection<T>.m_scenePrefabs.m_size;
	}

	public static T GetLoaded(uint index)
	{
		T prefab = PrefabCollection<T>.m_scenePrefabs.m_buffer[(int)((UIntPtr)index)].m_prefab;
		if (prefab.m_prefabInitialized)
		{
			return prefab;
		}
		return (T)((object)null);
	}

	public static T FindLoaded(string name)
	{
		if (name == string.Empty)
		{
			return (T)((object)null);
		}
		PrefabCollection<T>.PrefabData prefabData;
		if (PrefabCollection<T>.m_prefabDict.TryGetValue(name, out prefabData))
		{
			return prefabData.m_prefab;
		}
		string text = BuildConfig.ResolveLegacyPrefab(name);
		if (name != text && PrefabCollection<T>.m_prefabDict.TryGetValue(text, out prefabData))
		{
			return prefabData.m_prefab;
		}
		CODebugBase<LogChannel>.Warn(LogChannel.Serialization, "Unknown prefab: " + name);
		return (T)((object)null);
	}

	public static bool LoadedExists(string name)
	{
		PrefabCollection<T>.PrefabData prefabData;
		return !string.IsNullOrEmpty(name) && PrefabCollection<T>.m_prefabDict.TryGetValue(name, out prefabData);
	}

	public static void InitializePrefabs(string collection, T[] prefabs, string[] replaces)
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = 0; i < prefabs.Length; i++)
			{
				string replace = null;
				if (replaces != null && replaces.Length > i)
				{
					replace = replaces[i];
				}
				PrefabCollection<T>.InitializePrefabImpl(collection, prefabs[i], replace);
			}
			PrefabCollection<T>.m_bindingsDirty = true;
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	public static void InitializePrefabs(string collection, T prefab, string replace)
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			PrefabCollection<T>.InitializePrefabImpl(collection, prefab, replace);
			PrefabCollection<T>.m_bindingsDirty = true;
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	private static void InitializePrefabImpl(string collection, T prefab, string replace)
	{
		try
		{
			if (replace != null)
			{
				replace = replace.Trim();
			}
			string name = prefab.get_gameObject().get_name();
			PrefabCollection<T>.PrefabData prefabData;
			if (name != replace && PrefabCollection<T>.m_prefabDict.TryGetValue(name, out prefabData))
			{
				if (!prefabData.m_replaced)
				{
					throw new PrefabException(prefab, "Duplicate prefab name");
				}
			}
			else
			{
				if (!prefab.m_prefabInitialized)
				{
					prefab.InitializePrefab();
					prefab.m_prefabInitialized = true;
				}
				prefab.m_prefabDataIndex = -1;
				prefabData.m_name = name;
				prefabData.m_refcount = 0;
				prefabData.m_prefab = prefab;
				prefabData.m_replaced = false;
				PrefabCollection<T>.m_scenePrefabs.Add(prefabData);
				if (name != replace && !PrefabCollection<T>.m_prefabDict.ContainsKey(name))
				{
					PrefabCollection<T>.m_prefabDict.Add(name, prefabData);
				}
				if (!string.IsNullOrEmpty(replace))
				{
					prefabData.m_replaced = true;
					if (replace.IndexOf(',') != -1)
					{
						string[] array = replace.Split(new char[]
						{
							','
						});
						for (int i = 0; i < array.Length; i++)
						{
							string key = array[i].Trim();
							PrefabCollection<T>.PrefabData prefabData2;
							if (PrefabCollection<T>.m_prefabDict.TryGetValue(key, out prefabData2) && prefabData2.m_prefab.m_prefabInitialized)
							{
								prefabData2.m_prefab.m_prefabInitialized = false;
								prefabData2.m_prefab.DestroyPrefab();
							}
							PrefabCollection<T>.m_prefabDict[key] = prefabData;
						}
					}
					else
					{
						PrefabCollection<T>.PrefabData prefabData3;
						if (PrefabCollection<T>.m_prefabDict.TryGetValue(replace, out prefabData3) && prefabData3.m_prefab.m_prefabInitialized)
						{
							prefabData3.m_prefab.m_prefabInitialized = false;
							prefabData3.m_prefab.DestroyPrefab();
						}
						PrefabCollection<T>.m_prefabDict[replace] = prefabData;
					}
				}
			}
		}
		catch (PrefabException ex)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
			{
				collection,
				": ",
				ex.m_prefabInfo.get_gameObject().get_name(),
				": ",
				ex.Message,
				"\n",
				ex.StackTrace
			}), ex.m_prefabInfo.get_gameObject());
			LoadingManager expr_25E = Singleton<LoadingManager>.get_instance();
			string brokenAssets = expr_25E.m_brokenAssets;
			expr_25E.m_brokenAssets = string.Concat(new string[]
			{
				brokenAssets,
				"\n",
				collection,
				": ",
				ex.m_prefabInfo.get_gameObject().get_name(),
				": ",
				ex.Message
			});
		}
		catch (Exception ex2)
		{
			if (prefab == null)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, collection + ": NULL prefab");
				LoadingManager expr_2E3 = Singleton<LoadingManager>.get_instance();
				expr_2E3.m_brokenAssets = expr_2E3.m_brokenAssets + "\n" + collection + ": NULL";
			}
			else
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
				{
					collection,
					": ",
					prefab.get_gameObject().get_name(),
					": ",
					ex2.Message,
					"\n",
					ex2.StackTrace
				}), prefab.get_gameObject());
				LoadingManager expr_36B = Singleton<LoadingManager>.get_instance();
				string brokenAssets = expr_36B.m_brokenAssets;
				expr_36B.m_brokenAssets = string.Concat(new string[]
				{
					brokenAssets,
					"\n",
					collection,
					": ",
					prefab.get_gameObject().get_name(),
					": ",
					ex2.Message
				});
			}
		}
	}

	public static void DestroyPrefabs(string collection, T[] prefabs, string[] replaces)
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < PrefabCollection<T>.m_scenePrefabs.m_size; i++)
			{
				if (num2 < prefabs.Length && prefabs[num2] == PrefabCollection<T>.m_scenePrefabs.m_buffer[i].m_prefab)
				{
					PrefabCollection<T>.m_prefabDict.Remove(PrefabCollection<T>.m_scenePrefabs.m_buffer[i].m_name);
					num2++;
				}
				else
				{
					PrefabCollection<T>.m_scenePrefabs[num++] = PrefabCollection<T>.m_scenePrefabs[i];
				}
			}
			for (int j = num; j < PrefabCollection<T>.m_scenePrefabs.m_size; j++)
			{
				PrefabCollection<T>.m_scenePrefabs[j] = default(PrefabCollection<T>.PrefabData);
			}
			PrefabCollection<T>.m_scenePrefabs.m_size = num;
			if (replaces != null)
			{
				for (int k = 0; k < replaces.Length; k++)
				{
					string text = replaces[k];
					if (text.IndexOf(',') != -1)
					{
						string[] array = text.Split(new char[]
						{
							','
						});
						for (int l = 0; l < array.Length; l++)
						{
							PrefabCollection<T>.m_prefabDict.Remove(array[l].Trim());
						}
					}
					else
					{
						PrefabCollection<T>.m_prefabDict.Remove(text.Trim());
					}
				}
			}
			for (int m = 0; m < prefabs.Length; m++)
			{
				try
				{
					if (prefabs[m].m_prefabDataIndex != -1)
					{
						PrefabCollection<T>.m_simulationPrefabs.m_buffer[prefabs[m].m_prefabDataIndex].m_prefab = (T)((object)null);
						prefabs[m].m_prefabDataIndex = -1;
					}
					if (prefabs[m].m_prefabInitialized)
					{
						prefabs[m].m_prefabInitialized = false;
						prefabs[m].DestroyPrefab();
					}
				}
				catch (PrefabException ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
					{
						collection,
						": ",
						ex.m_prefabInfo.get_gameObject().get_name(),
						": ",
						ex.Message,
						"\n",
						ex.StackTrace
					}), ex.m_prefabInfo.get_gameObject());
				}
				catch (Exception ex2)
				{
					if (prefabs[m] == null)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, collection + ": NULL prefab");
					}
					else
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
						{
							collection,
							": ",
							prefabs[m].get_gameObject().get_name(),
							": ",
							ex2.Message,
							"\n",
							ex2.StackTrace
						}), prefabs[m].get_gameObject());
					}
				}
			}
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	public static void DestroyAll()
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			PrefabCollection<T>.m_prefabDict.Clear();
			for (int i = 0; i < PrefabCollection<T>.m_simulationPrefabs.m_size; i++)
			{
				PrefabCollection<T>.m_simulationPrefabs.m_buffer[i] = default(PrefabCollection<T>.PrefabData);
			}
			PrefabCollection<T>.m_simulationPrefabs.Clear();
			for (int j = 0; j < PrefabCollection<T>.m_scenePrefabs.m_size; j++)
			{
				try
				{
					if (PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab.m_prefabInitialized)
					{
						PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab.m_prefabInitialized = false;
						PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab.DestroyPrefab();
					}
				}
				catch (PrefabException ex)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
					{
						ex.m_prefabInfo.get_gameObject().get_name(),
						": ",
						ex.Message,
						"\n",
						ex.StackTrace
					}), ex.m_prefabInfo.get_gameObject());
				}
				catch (Exception ex2)
				{
					if (PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab != null)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new string[]
						{
							PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab.get_gameObject().get_name(),
							": ",
							ex2.Message,
							"\n",
							ex2.StackTrace
						}), PrefabCollection<T>.m_scenePrefabs.m_buffer[j].m_prefab.get_gameObject());
					}
				}
				PrefabCollection<T>.m_scenePrefabs.m_buffer[j] = default(PrefabCollection<T>.PrefabData);
			}
			PrefabCollection<T>.m_scenePrefabs.Clear();
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	public static int PrefabCount()
	{
		return PrefabCollection<T>.m_simulationPrefabs.m_size;
	}

	public static T GetPrefab(uint index)
	{
		return PrefabCollection<T>.m_simulationPrefabs.m_buffer[(int)((UIntPtr)index)].m_prefab;
	}

	public static string PrefabName(uint index)
	{
		return PrefabCollection<T>.m_simulationPrefabs.m_buffer[(int)((UIntPtr)index)].m_name;
	}

	public static void BeginSerialize(DataSerializer s)
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		PrefabCollection<T>.m_encodedArray = EncodedArray.UShort.BeginWrite(s);
		for (int i = 0; i < PrefabCollection<T>.m_simulationPrefabs.m_size; i++)
		{
			PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_refcount = 0;
		}
	}

	public static void Serialize(uint index)
	{
		PrefabCollection<T>.m_encodedArray.Write((ushort)index);
		PrefabCollection<T>.PrefabData[] expr_1D_cp_0 = PrefabCollection<T>.m_simulationPrefabs.m_buffer;
		UIntPtr expr_1D_cp_1 = (UIntPtr)index;
		expr_1D_cp_0[(int)expr_1D_cp_1].m_refcount = expr_1D_cp_0[(int)expr_1D_cp_1].m_refcount + 1;
	}

	public static void EndSerialize(DataSerializer s)
	{
		try
		{
			PrefabCollection<T>.m_encodedArray.EndWrite();
			PrefabCollection<T>.m_encodedArray = null;
			s.WriteUInt16((uint)PrefabCollection<T>.m_simulationPrefabs.m_size);
			for (int i = 0; i < PrefabCollection<T>.m_simulationPrefabs.m_size; i++)
			{
				if (PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_refcount != 0)
				{
					s.WriteUniqueString(PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_name);
				}
				else
				{
					s.WriteUniqueString(null);
				}
			}
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	public static void BeginDeserialize(DataSerializer s)
	{
		while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		PrefabCollection<T>.m_encodedArray = EncodedArray.UShort.BeginRead(s);
		for (int i = 0; i < PrefabCollection<T>.m_simulationPrefabs.m_size; i++)
		{
			PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_prefab = (T)((object)null);
			PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_name = null;
			PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_refcount = 0;
			PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_replaced = false;
		}
		PrefabCollection<T>.m_simulationPrefabs.Clear();
	}

	public static uint Deserialize(bool important)
	{
		uint num = (uint)PrefabCollection<T>.m_encodedArray.Read();
		if (num >= (uint)PrefabCollection<T>.m_simulationPrefabs.m_size)
		{
			int num2 = 0;
			if (PrefabCollection<T>.m_simulationPrefabs.m_buffer != null)
			{
				num2 = PrefabCollection<T>.m_simulationPrefabs.m_buffer.Length;
			}
			if (num >= (uint)num2)
			{
				int capacity = Mathf.Max(Mathf.Max((int)(num + 1u), 32), num2 << 1);
				PrefabCollection<T>.m_simulationPrefabs.SetCapacity(capacity);
			}
			PrefabCollection<T>.m_simulationPrefabs.m_size = (int)(num + 1u);
		}
		if (important)
		{
			PrefabCollection<T>.PrefabData[] expr_82_cp_0 = PrefabCollection<T>.m_simulationPrefabs.m_buffer;
			UIntPtr expr_82_cp_1 = (UIntPtr)num;
			expr_82_cp_0[(int)expr_82_cp_1].m_refcount = expr_82_cp_0[(int)expr_82_cp_1].m_refcount + 1;
		}
		return num;
	}

	public static void EndDeserialize(DataSerializer s)
	{
		try
		{
			PrefabCollection<T>.m_encodedArray.EndRead();
			PrefabCollection<T>.m_encodedArray = null;
			int num = (int)s.ReadUInt16();
			for (int i = 0; i < num; i++)
			{
				if (i < PrefabCollection<T>.m_simulationPrefabs.m_size)
				{
					PrefabCollection<T>.m_simulationPrefabs.m_buffer[i].m_name = s.ReadUniqueString();
				}
				else
				{
					PrefabCollection<T>.PrefabData item;
					item.m_name = s.ReadUniqueString();
					item.m_refcount = 0;
					item.m_prefab = (T)((object)null);
					item.m_replaced = false;
					PrefabCollection<T>.m_simulationPrefabs.Add(item);
				}
			}
			PrefabCollection<T>.m_bindingsDirty = true;
		}
		finally
		{
			Monitor.Exit(PrefabCollection<T>.m_prefabLock);
		}
	}

	public static void BindPrefabs()
	{
		if (PrefabCollection<T>.m_bindingsDirty)
		{
			while (!Monitor.TryEnter(PrefabCollection<T>.m_prefabLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				PrefabCollection<T>.m_bindingsDirty = false;
				for (int i = 0; i < PrefabCollection<T>.m_scenePrefabs.m_size; i++)
				{
					PrefabCollection<T>.m_scenePrefabs[i].m_prefab.m_prefabDataIndex = -1;
				}
				for (int j = 0; j < PrefabCollection<T>.m_simulationPrefabs.m_size; j++)
				{
					string name = PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_name;
					if (name != null)
					{
						PrefabCollection<T>.PrefabData prefabData;
						if (PrefabCollection<T>.m_prefabDict.TryGetValue(name, out prefabData))
						{
							prefabData.m_prefab.m_prefabDataIndex = j;
							PrefabCollection<T>.m_simulationPrefabs.m_buffer[j] = prefabData;
						}
						else
						{
							string text = BuildConfig.ResolveLegacyPrefab(name);
							if (name != text)
							{
								CODebugBase<LogChannel>.Warn(LogChannel.Serialization, "Resolving legacy prefab: " + name + " -> " + text);
								if (PrefabCollection<T>.m_prefabDict.TryGetValue(text, out prefabData))
								{
									prefabData.m_prefab.m_prefabDataIndex = j;
									PrefabCollection<T>.m_simulationPrefabs.m_buffer[j] = prefabData;
								}
								else
								{
									if (PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_refcount != 0)
									{
										CODebugBase<LogChannel>.Error(LogChannel.Serialization, "Unknown prefab: " + text);
									}
									PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_prefab = (T)((object)null);
									PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_refcount = 0;
								}
							}
							else
							{
								if (PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_refcount != 0)
								{
									CODebugBase<LogChannel>.Error(LogChannel.Serialization, "Unknown prefab: " + name);
								}
								PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_prefab = (T)((object)null);
								PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_refcount = 0;
							}
						}
					}
					else
					{
						PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_prefab = (T)((object)null);
						PrefabCollection<T>.m_simulationPrefabs.m_buffer[j].m_refcount = 0;
					}
				}
				int k = 0;
				for (int l = 0; l < PrefabCollection<T>.m_scenePrefabs.m_size; l++)
				{
					if (PrefabCollection<T>.m_scenePrefabs[l].m_prefab.m_prefabInitialized && PrefabCollection<T>.m_scenePrefabs[l].m_prefab.m_prefabDataIndex == -1)
					{
						while (k < PrefabCollection<T>.m_simulationPrefabs.m_size)
						{
							if (PrefabCollection<T>.m_simulationPrefabs.m_buffer[k].m_name == null)
							{
								PrefabCollection<T>.m_scenePrefabs[l].m_prefab.m_prefabDataIndex = k;
								PrefabCollection<T>.m_simulationPrefabs.m_buffer[k++] = PrefabCollection<T>.m_scenePrefabs[l];
								break;
							}
							k++;
						}
						if (PrefabCollection<T>.m_scenePrefabs[l].m_prefab.m_prefabDataIndex == -1)
						{
							PrefabCollection<T>.m_scenePrefabs[l].m_prefab.m_prefabDataIndex = PrefabCollection<T>.m_simulationPrefabs.m_size;
							PrefabCollection<T>.m_simulationPrefabs.Add(PrefabCollection<T>.m_scenePrefabs[l]);
						}
					}
				}
			}
			finally
			{
				Monitor.Exit(PrefabCollection<T>.m_prefabLock);
			}
		}
	}

	private static EncodedArray.UShort m_encodedArray;

	private static object m_prefabLock = new object();

	private static bool m_bindingsDirty = false;

	private static FastList<PrefabCollection<T>.PrefabData> m_simulationPrefabs = new FastList<PrefabCollection<T>.PrefabData>();

	private static FastList<PrefabCollection<T>.PrefabData> m_scenePrefabs = new FastList<PrefabCollection<T>.PrefabData>();

	private static Dictionary<string, PrefabCollection<T>.PrefabData> m_prefabDict = new Dictionary<string, PrefabCollection<T>.PrefabData>();

	public struct PrefabData
	{
		public string m_name;

		public int m_refcount;

		public T m_prefab;

		public bool m_replaced;
	}
}
