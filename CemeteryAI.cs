﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using UnityEngine;

public class CemeteryAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Health)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.WaterPower)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return StringUtils.SafeFormat("Electricity: {0}\nWater: {1} ({2}% polluted)\nSewage: {3}\nGarbage: {4}\nCrime: {5}\nDead: {6}", new object[]
		{
			data.m_electricityBuffer,
			data.m_waterBuffer,
			(int)(data.m_waterPollution * 100 / 255),
			data.m_sewageBuffer,
			data.m_garbageBuffer,
			data.m_crimeBuffer,
			num3
		});
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Health;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.SetPrefab(this.m_info);
		}
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data))
		{
			return Locale.Get("BUILDING_STATUS_FULL");
		}
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EMPTYING");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, this.m_corpseCapacity, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_corpseCapacity, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		if (this.IsFull(buildingID, ref data) && this.m_fullPassMilestone != null)
		{
			this.m_fullPassMilestone.Relock();
		}
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_corpseCapacity, 0);
		data.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
		this.CheckCapacity(buildingID, ref data);
		this.SetEmptying(buildingID, ref data, (data.m_flags & Building.Flags.Downgrading) != Building.Flags.None);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_deathCareAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.DeathCare, (float)this.m_deathCareAccumulation, this.m_deathCareRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_deathCareAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.DeathCare, (float)(-(float)this.m_deathCareAccumulation), this.m_deathCareRadius);
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			if (this.IsFull(buildingID, ref buildingData))
			{
				Singleton<BuildingManager>.get_instance().m_cemeteryFull.Activate(properties.m_cemeteryFull, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_cemeteryFull.Deactivate(buildingID, false);
			}
			if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None && this.CanBeRelocated(buildingID, ref buildingData))
			{
				Singleton<BuildingManager>.get_instance().m_cemeteryEmpty.Activate(properties.m_cemeteryEmpty, buildingID);
			}
			else
			{
				Singleton<BuildingManager>.get_instance().m_cemeteryEmpty.Deactivate(buildingID, false);
			}
		}
		this.CheckCapacity(buildingID, ref buildingData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempExport = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Notification.Problem problem = buildingData.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			if (buildingData.m_customBuffer1 == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		buildingData.m_problems = problem;
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
	}

	public override ToolBase.ToolErrors CheckBulldozing(ushort buildingID, ref Building data)
	{
		int customBuffer = (int)data.m_customBuffer1;
		if (this.m_graveCount != 0 && customBuffer != 0 && (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			return ToolBase.ToolErrors.NotEmpty;
		}
		return base.CheckBulldozing(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Dead)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else if (material == TransferManager.TransferReason.DeadMove)
		{
			VehicleInfo randomVehicleInfo2 = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo2 != null)
			{
				Array16<Vehicle> vehicles2 = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num2;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo2, data.m_position, material, false, true))
				{
					randomVehicleInfo2.m_vehicleAI.SetSource(num2, ref vehicles2.m_buffer[(int)num2], buildingID);
					randomVehicleInfo2.m_vehicleAI.StartTransfer(num2, ref vehicles2.m_buffer[(int)num2], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.DeadMove)
		{
			bool flag = this.IsFull(buildingID, ref data);
			int num = (int)data.m_customBuffer1;
			int deadCount = this.GetDeadCount(buildingID, ref data);
			if (this.m_graveCount != 0)
			{
				amountDelta = Mathf.Clamp(amountDelta, -num, this.m_graveCount - num - deadCount);
			}
			else if (this.m_burialRate != 0)
			{
				amountDelta = Mathf.Clamp(amountDelta, 0, this.m_corpseCapacity - num - deadCount);
			}
			num += amountDelta;
			data.m_customBuffer1 = (ushort)num;
			bool flag2 = this.IsFull(buildingID, ref data);
			if (flag != flag2)
			{
				if (flag2)
				{
					if (this.m_fullPassMilestone != null)
					{
						this.m_fullPassMilestone.Unlock();
					}
				}
				else if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Relock();
				}
			}
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Dead)
		{
			int customBuffer = (int)data.m_customBuffer1;
			int deadCount = this.GetDeadCount(buildingID, ref data);
			amount = customBuffer + deadCount;
			if (this.m_graveCount != 0)
			{
				max = this.m_graveCount;
			}
			else
			{
				max = this.m_corpseCapacity;
			}
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Dead, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.DeadMove, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.DeadMove, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active | Building.Flags.Downgrading)) != Building.Flags.Active || this.IsFull(buildingID, ref data))
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_deathCareRadius * 0.01f;
	}

	private int GetDeadCount(ushort buildingID, ref Building buildingData)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = buildingData.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num3;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = productionRate * this.m_deathCareAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.DeathCare, num, buildingData.m_position, this.m_deathCareRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num2 = (int)buildingData.m_customBuffer1;
		int num3 = (this.m_burialRate * finalProductionRate * 100 + this.m_corpseCapacity - 1) / this.m_corpseCapacity;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num4 = buildingData.m_citizenUnits;
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		while (num4 != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num4)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num4)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num4)].GetCitizen(i);
					if (citizen != 0u)
					{
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
						{
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
							{
								if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10000u) < num3)
								{
									instance.ReleaseCitizen(citizen);
									num7++;
									num2++;
								}
								else
								{
									num6++;
								}
							}
							else
							{
								num6++;
							}
						}
						else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick = false;
						}
					}
				}
			}
			num4 = nextUnit;
			if (++num5 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		behaviour.m_deadCount += num6;
		if (this.m_graveCount == 0)
		{
			for (int j = num7; j < num2; j++)
			{
				if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10000u) < num3)
				{
					num7++;
					num2--;
				}
				else
				{
					num6++;
				}
			}
		}
		buildingData.m_tempExport = (byte)Mathf.Min((int)buildingData.m_tempExport + num7, 255);
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		bool flag = this.IsFull(buildingID, ref buildingData);
		if (this.m_graveCount != 0)
		{
			num2 = Mathf.Min(num2, this.m_graveCount);
			buildingData.m_customBuffer1 = (ushort)num2;
			District[] expr_2E2_cp_0_cp_0 = instance2.m_districts.m_buffer;
			byte expr_2E2_cp_0_cp_1 = district;
			expr_2E2_cp_0_cp_0[(int)expr_2E2_cp_0_cp_1].m_productionData.m_tempDeadAmount = expr_2E2_cp_0_cp_0[(int)expr_2E2_cp_0_cp_1].m_productionData.m_tempDeadAmount + (uint)num2;
			District[] expr_307_cp_0_cp_0 = instance2.m_districts.m_buffer;
			byte expr_307_cp_0_cp_1 = district;
			expr_307_cp_0_cp_0[(int)expr_307_cp_0_cp_1].m_productionData.m_tempDeadCapacity = expr_307_cp_0_cp_0[(int)expr_307_cp_0_cp_1].m_productionData.m_tempDeadCapacity + (uint)this.m_graveCount;
		}
		else
		{
			buildingData.m_customBuffer1 = (ushort)num2;
			District[] expr_33E_cp_0_cp_0 = instance2.m_districts.m_buffer;
			byte expr_33E_cp_0_cp_1 = district;
			expr_33E_cp_0_cp_0[(int)expr_33E_cp_0_cp_1].m_productionData.m_tempCremateCapacity = expr_33E_cp_0_cp_0[(int)expr_33E_cp_0_cp_1].m_productionData.m_tempCremateCapacity + (uint)this.m_corpseCapacity;
		}
		bool flag2 = this.IsFull(buildingID, ref buildingData);
		if (flag != flag2)
		{
			if (flag2)
			{
				if (this.m_fullPassMilestone != null)
				{
					this.m_fullPassMilestone.Unlock();
				}
			}
			else if (this.m_fullPassMilestone != null)
			{
				this.m_fullPassMilestone.Relock();
			}
		}
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Dead, ref num8, ref num10, ref num11, ref num12);
		base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.DeadMove, ref num9, ref num10, ref num11, ref num12);
		int num13 = this.m_corpseCapacity - num6 - num11;
		int num14 = (finalProductionRate * this.m_hearseCount + 99) / 100;
		if ((buildingData.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			if (this.m_graveCount != 0)
			{
				int num15 = 0;
				int num16 = 0;
				int num17 = 0;
				int num18 = 0;
				base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.DeadMove, ref num15, ref num16, ref num17, ref num18);
				if (num2 > 0 && num15 < num14)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.DeadMove, offer);
				}
			}
		}
		else if (this.m_graveCount != 0)
		{
			num13 = Mathf.Min(num13, this.m_graveCount - num2 - num6 - num11 + 9);
			int num19 = num13 / 10;
			if (num8 != 0)
			{
				num19--;
			}
			bool flag3 = num19 >= 1 && (num19 > 1 || num8 >= num14 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0);
			bool flag4 = num19 >= 1 && num8 < num14 && (num19 > 1 || !flag3);
			if (flag3)
			{
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Priority = 0;
				offer2.Building = buildingID;
				offer2.Position = buildingData.m_position;
				offer2.Amount = Mathf.Max(1, num19 - 1);
				offer2.Active = false;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.DeadMove, offer2);
			}
			if (flag4)
			{
				TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
				offer3.Priority = 2 - num8;
				offer3.Building = buildingID;
				offer3.Position = buildingData.m_position;
				offer3.Amount = 1;
				offer3.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Dead, offer3);
			}
		}
		else
		{
			int num20 = num13 / 10;
			bool flag5 = num20 >= 1 && (num20 > 1 || num8 >= num14 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0);
			bool flag6 = num20 >= 1 && num8 < num14 && (num20 > 1 || !flag5);
			if (flag5)
			{
				TransferManager.TransferOffer offer4 = default(TransferManager.TransferOffer);
				offer4.Priority = Mathf.Max(1, num13 * 6 / this.m_corpseCapacity);
				offer4.Building = buildingID;
				offer4.Position = buildingData.m_position;
				offer4.Amount = Mathf.Max(1, num20 - 1);
				offer4.Active = false;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.DeadMove, offer4);
			}
			if (flag6)
			{
				TransferManager.TransferOffer offer5 = default(TransferManager.TransferOffer);
				offer5.Priority = 2 - num8;
				offer5.Building = buildingID;
				offer5.Position = buildingData.m_position;
				offer5.Amount = 1;
				offer5.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Dead, offer5);
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	private void CheckCapacity(ushort buildingID, ref Building buildingData)
	{
		if (this.m_graveCount != 0)
		{
			int customBuffer = (int)buildingData.m_customBuffer1;
			if (customBuffer >= this.m_graveCount)
			{
				if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityFull)
				{
					buildingData.m_flags |= Building.Flags.CapacityFull;
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
					Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				}
			}
			else if (customBuffer >= this.m_graveCount >> 1)
			{
				if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.CapacityStep1)
				{
					buildingData.m_flags = ((buildingData.m_flags & ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2)) | Building.Flags.CapacityStep1);
					buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
					Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
				}
			}
			else if ((buildingData.m_flags & Building.Flags.CapacityFull) != Building.Flags.None)
			{
				buildingData.m_flags &= ~(Building.Flags.CapacityStep1 | Building.Flags.CapacityStep2);
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.LandfillFull);
				Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			}
		}
	}

	public override void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
		Notification.Problem problem = data.m_problems;
		Notification.Problem problems = data.m_problems;
		problem = Notification.RemoveProblems(problem, Notification.Problem.Emptying | Notification.Problem.EmptyingFinished);
		if (this.m_graveCount != 0 && emptying)
		{
			data.m_flags |= Building.Flags.Downgrading;
			if (data.m_customBuffer1 == 0)
			{
				problem = Notification.AddProblems(problem, Notification.Problem.EmptyingFinished);
			}
			else
			{
				problem = Notification.AddProblems(problem, Notification.Problem.Emptying);
			}
		}
		else
		{
			data.m_flags &= ~Building.Flags.Downgrading;
		}
		data.m_problems = problem;
		if (problem != problems)
		{
			Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, problem);
			Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		}
	}

	public override void PlacementSucceeded()
	{
		BuildingTypeGuide deathCareNeeded = Singleton<GuideManager>.get_instance().m_deathCareNeeded;
		if (deathCareNeeded != null)
		{
			deathCareNeeded.Deactivate();
		}
	}

	public override void UpdateGuide(GuideController guideController)
	{
		if (this.m_graveCount != 0)
		{
			BuildingTypeGuide deathCareNeeded = Singleton<GuideManager>.get_instance().m_deathCareNeeded;
			if (deathCareNeeded != null)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				if ((instance.m_lastBuildingProblems & Notification.Problem.Death) != Notification.Problem.None)
				{
					deathCareNeeded.Activate(guideController.m_deathCareNeeded, this.m_info);
				}
				else
				{
					deathCareNeeded.Deactivate();
				}
			}
		}
		base.UpdateGuide(guideController);
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override bool IsFull(ushort buildingID, ref Building data)
	{
		return this.m_graveCount != 0 && (int)data.m_customBuffer1 >= this.m_graveCount;
	}

	public override bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		return this.m_graveCount == 0 || data.m_customBuffer1 == 0;
	}

	public override bool CanBeEmptied()
	{
		return this.m_graveCount != 0;
	}

	public override bool CanBeEmptied(ushort buildingID, ref Building data)
	{
		if (this.m_graveCount != 0)
		{
			if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
			{
				return true;
			}
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
			int size = serviceBuildings.m_size;
			ushort[] buffer = serviceBuildings.m_buffer;
			if (buffer != null && size <= buffer.Length)
			{
				for (int i = 0; i < size; i++)
				{
					ushort num = buffer[i];
					if (num != 0 && num != buildingID)
					{
						BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_level == this.m_info.m_class.m_level && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Active) != Building.Flags.None && instance.m_buildings.m_buffer[(int)num].m_productionRate != 0 && !info.m_buildingAI.IsFull(num, ref instance.m_buildings.m_buffer[(int)num]))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		int num;
		if (this.m_graveCount != 0)
		{
			num = this.m_graveCount;
		}
		else
		{
			num = this.m_corpseCapacity;
		}
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_PATIENT_CAPACITY", new object[]
			{
				num
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num4;
		int num5;
		if (this.m_graveCount != 0)
		{
			num4 = Mathf.Min(num3 + (int)data.m_customBuffer1, this.m_graveCount);
			num5 = this.m_graveCount;
		}
		else
		{
			num4 = Mathf.Min(num3 + (int)data.m_customBuffer1, this.m_corpseCapacity);
			num5 = this.m_corpseCapacity;
		}
		int finalExport = (int)data.m_finalExport;
		int num6 = (productionRate * this.m_hearseCount + 99) / 100;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None)
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.DeadMove, ref num7, ref num8, ref num9, ref num10);
		}
		else
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Dead, ref num7, ref num8, ref num9, ref num10);
		}
		string str = LocaleFormatter.FormatGeneric("AIINFO_DECEASED_STORED", new object[]
		{
			num4,
			num5
		}) + Environment.NewLine;
		str = str + LocaleFormatter.FormatGeneric("AIINFO_DECEASED_PROCESSED", new object[]
		{
			finalExport
		}) + Environment.NewLine;
		return str + LocaleFormatter.FormatGeneric("AIINFO_HEARSES", new object[]
		{
			num7,
			num6
		});
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 3;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 18;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 21;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 18;

	[CustomizableProperty("Hearse Count")]
	public int m_hearseCount = 10;

	[CustomizableProperty("Corpse Capacity")]
	public int m_corpseCapacity = 100;

	[CustomizableProperty("Burial Rate")]
	public int m_burialRate = 10;

	[CustomizableProperty("Grave Count")]
	public int m_graveCount;

	[CustomizableProperty("Deathcare Accumulation")]
	public int m_deathCareAccumulation = 100;

	[CustomizableProperty("Deathcare Radius")]
	public float m_deathCareRadius = 400f;

	public ManualMilestone m_fullPassMilestone;
}
