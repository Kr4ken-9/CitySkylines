﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public sealed class PublicTransportVehicleWorldInfoPanel : VehicleWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Line = base.Find<UILabel>("Line");
		this.m_Owner = base.Find<UIButton>("Owner");
		this.m_Owner.add_eventClick(new MouseEventHandler(base.OnTargetClick));
		this.m_VehicleType = base.Find<UISprite>("VehicleType");
		this.m_VehicleType.add_eventSpriteNameChanged(new PropertyChangedEventHandler<string>(this.IconChanged));
		this.m_Passengers = base.Find<UILabel>("Passengers");
		this.m_DistanceTraveled = base.Find<UIProgressBar>("DistanceTraveled");
		this.m_DistanceProgress = base.Find<UILabel>("DistanceProgress");
		this.m_modifyLine = base.Find<UIButton>("ModifyLine");
		this.m_linesOverview = base.Find<UIButton>("LinesOverview");
	}

	private void IconChanged(UIComponent comp, string text)
	{
		UITextureAtlas.SpriteInfo spriteInfo = this.m_VehicleType.get_atlas().get_Item(text);
		if (spriteInfo != null)
		{
			this.m_VehicleType.set_size(spriteInfo.get_pixelSize());
		}
	}

	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			InstanceID instanceID = this.m_InstanceID;
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				instanceID.Vehicle = firstVehicle;
				bool isVisible = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].m_transportLine != 0;
				this.m_modifyLine.set_isVisible(isVisible);
				this.m_linesOverview.set_isVisible(isVisible);
			}
		}
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			InstanceID instanceID = this.m_InstanceID;
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				instanceID.Vehicle = firstVehicle;
				VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].Info;
				InstanceID ownerID = info.m_vehicleAI.GetOwnerID(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle]);
				if (ownerID.Building != 0)
				{
					this.m_Owner.get_parent().set_isVisible(true);
					this.m_Owner.set_objectUserData(ownerID);
					this.m_Owner.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(ownerID.Building, instanceID));
					base.ShortenTextToFitParent(this.m_Owner);
					this.m_Owner.set_isEnabled((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)ownerID.Building].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.None);
				}
				else
				{
					this.m_Owner.get_parent().set_isVisible(false);
				}
				string text;
				int num;
				int num2;
				info.m_vehicleAI.GetBufferStatus(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle], out text, out num, out num2);
				if (num2 != 0)
				{
					this.m_Passengers.set_text(LocaleFormatter.FormatGeneric("VEHICLE_PASSENGERS", new object[]
					{
						num,
						num2
					}));
					this.m_Passengers.set_isVisible(true);
				}
				else
				{
					this.m_Passengers.set_isVisible(false);
				}
				float cachedCurrentProgress;
				float cachedTotalProgress;
				if (info.m_vehicleAI.GetProgressStatus(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle], out cachedCurrentProgress, out cachedTotalProgress) || firstVehicle != this.m_cachedProgressVehicle)
				{
					this.m_cachedCurrentProgress = cachedCurrentProgress;
					this.m_cachedTotalProgress = cachedTotalProgress;
					this.m_cachedProgressVehicle = firstVehicle;
				}
				else
				{
					cachedCurrentProgress = this.m_cachedCurrentProgress;
					cachedTotalProgress = this.m_cachedTotalProgress;
				}
				if (cachedTotalProgress != 0f)
				{
					this.m_DistanceTraveled.get_parent().Show();
					this.m_DistanceProgress.get_parent().Show();
					float num3 = cachedCurrentProgress / cachedTotalProgress;
					int p = Mathf.RoundToInt(num3 * 100f);
					this.m_DistanceTraveled.set_value(num3);
					this.m_DistanceProgress.set_text(LocaleFormatter.FormatPercentage(p));
				}
				else
				{
					this.m_DistanceTraveled.get_parent().Hide();
					this.m_DistanceProgress.get_parent().Hide();
				}
				if (Singleton<TransportManager>.get_exists())
				{
					ushort transportLine = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].m_transportLine;
					if (transportLine != 0)
					{
						this.m_Line.get_parent().set_isVisible(true);
						this.m_Line.set_text(Singleton<TransportManager>.get_instance().GetLineName(transportLine));
						TransportInfo info2 = Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)transportLine].Info;
						this.m_VehicleType.set_spriteName(PublicTransportWorldInfoPanel.GetVehicleTypeIcon(info2.m_transportType));
					}
					else
					{
						this.m_VehicleType.set_spriteName(PublicTransportVehicleWorldInfoPanel.GetVehicleIcon(info.GetService(), info.GetSubService(), info.m_vehicleType));
						this.m_Line.get_parent().set_isVisible(false);
					}
				}
			}
		}
	}

	public static string GetVehicleIcon(ItemClass.Service service, ItemClass.SubService subService, VehicleInfo.VehicleType type)
	{
		if (service == ItemClass.Service.PublicTransport)
		{
			switch (subService)
			{
			case ItemClass.SubService.PublicTransportMetro:
				return "SubBarPublicTransportMetro";
			case ItemClass.SubService.PublicTransportTrain:
				return "SubBarPublicTransportTrain";
			case ItemClass.SubService.PublicTransportShip:
				return "SubBarPublicTransportShip";
			case ItemClass.SubService.PublicTransportPlane:
				return "SubBarPublicTransportPlane";
			case ItemClass.SubService.PublicTransportTaxi:
				return "SubBarPublicTransportTaxi";
			case ItemClass.SubService.PublicTransportCableCar:
				return "SubBarPublicTransportCableCar";
			}
			return string.Empty;
		}
		return PublicTransportVehicleWorldInfoPanel.GetVehicleTypeIcon(type);
	}

	public static string GetVehicleTypeIcon(VehicleInfo.VehicleType type)
	{
		switch (type)
		{
		case VehicleInfo.VehicleType.Car:
			return "SubBarPublicTransportTaxi";
		case VehicleInfo.VehicleType.Metro:
			return "SubBarPublicTransportMetro";
		case VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Metro:
		case VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Train:
		case VehicleInfo.VehicleType.Metro | VehicleInfo.VehicleType.Train:
		case VehicleInfo.VehicleType.Car | VehicleInfo.VehicleType.Metro | VehicleInfo.VehicleType.Train:
			IL_28:
			if (type == VehicleInfo.VehicleType.Plane)
			{
				return "SubBarPublicTransportPlane";
			}
			if (type != VehicleInfo.VehicleType.Ferry)
			{
				return string.Empty;
			}
			goto IL_52;
		case VehicleInfo.VehicleType.Train:
			return "SubBarPublicTransportTrain";
		case VehicleInfo.VehicleType.Ship:
			goto IL_52;
		}
		goto IL_28;
		IL_52:
		return "SubBarPublicTransportShip";
	}

	public void OnModifyLineClicked()
	{
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				ushort transportLine = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].m_transportLine;
				InstanceID empty = InstanceID.Empty;
				empty.TransportLine = transportLine;
				WorldInfoPanel.Show<PublicTransportWorldInfoPanel>(this.m_WorldMousePosition, empty);
			}
		}
	}

	public void OnLinesOverviewClicked()
	{
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			UIView.get_library().Show("PublicTransportDetailPanel", true, true);
		}
	}

	private UIButton m_Owner;

	private UISprite m_VehicleType;

	private UILabel m_Line;

	private UILabel m_Passengers;

	private UIProgressBar m_DistanceTraveled;

	private UILabel m_DistanceProgress;

	private float m_cachedCurrentProgress;

	private float m_cachedTotalProgress;

	private ushort m_cachedProgressVehicle;

	private UIButton m_modifyLine;

	private UIButton m_linesOverview;
}
