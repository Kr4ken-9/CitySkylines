﻿using System;

public class WaterAndSewageGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Water;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (GeneratedGroupPanel.GroupFilter)3;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "WaterServices")
			{
				return 0;
			}
			if (name == "Heating")
			{
				return 1;
			}
		}
		return 2147483647;
	}
}
