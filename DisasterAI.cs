﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class DisasterAI : PrefabAI
{
	public virtual void InitializeAI()
	{
		if (this.m_experienceMilestone != null)
		{
			this.m_experienceMilestone.SetPrefab(this.m_info);
		}
	}

	public virtual void ReleaseAI()
	{
	}

	public virtual void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort disasterID, ref DisasterData data, ref Vector3 shake)
	{
	}

	public virtual void PlayInstance(AudioManager.ListenerInfo listenerInfo, ushort disasterID, ref DisasterData data)
	{
	}

	public virtual string GetDebugString(ushort disasterID, ref DisasterData data)
	{
		string disasterName = this.GetDisasterName(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Finished) != DisasterData.Flags.None)
		{
			return StringUtils.SafeFormat("{0} - Finished", disasterName);
		}
		if ((data.m_flags & DisasterData.Flags.Clearing) != DisasterData.Flags.None)
		{
			return StringUtils.SafeFormat("{0} - Clearing", disasterName);
		}
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			return StringUtils.SafeFormat("{0} - Active", disasterName);
		}
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			return StringUtils.SafeFormat("{0} - Emerging", disasterName);
		}
		return StringUtils.SafeFormat("{0} - Starting {1}", new object[]
		{
			disasterName,
			data.StartTime
		});
	}

	protected virtual string GetDisasterName(ushort disasterID, ref DisasterData data)
	{
		return PrefabCollection<DisasterInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
	}

	public virtual void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
	}

	public virtual void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		InstanceManager.Group group = new InstanceManager.Group();
		group.m_ownerInstance.Disaster = disasterID;
		Singleton<InstanceManager>.get_instance().SetGroup(group.m_ownerInstance, group);
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null)
		{
			Singleton<DisasterManager>.get_instance().m_DisasterWrapper.OnDisasterCreated(disasterID);
		}
	}

	public virtual void ReleaseDisaster(ushort disasterID, ref DisasterData data)
	{
	}

	public virtual void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Clearing) != DisasterData.Flags.None)
		{
			if (!this.IsStillClearing(disasterID, ref data))
			{
				this.EndDisaster(disasterID, ref data);
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			if (!this.IsStillActive(disasterID, ref data))
			{
				this.DeactivateDisaster(disasterID, ref data);
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			if (!this.IsStillEmerging(disasterID, ref data))
			{
				this.ActivateDisaster(disasterID, ref data);
			}
		}
		if ((data.m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None)
		{
			if ((data.m_flags & DisasterData.Flags.Warning) != DisasterData.Flags.None)
			{
				if (data.m_broadcastCooldown > 0)
				{
					data.m_broadcastCooldown -= 1;
				}
				if (data.m_broadcastCooldown == 0)
				{
					data.m_broadcastCooldown = 36;
					if (this.m_info.m_warningBroadcast != null)
					{
						Singleton<AudioManager>.get_instance().QueueBroadcast(this.m_info.m_warningBroadcast);
					}
				}
			}
			else
			{
				StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.DisasterCount);
				statisticBase.Add(16);
				InstanceID id = default(InstanceID);
				id.Disaster = disasterID;
				InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
				if (data.m_broadcastCooldown >= 200)
				{
					if (data.m_broadcastCooldown > 200)
					{
						data.m_broadcastCooldown -= 1;
					}
					if (data.m_broadcastCooldown == 200 && (group == null || group.m_refCount >= 2))
					{
						data.m_broadcastCooldown = 236;
						if (this.m_info.m_activeBroadcast != null)
						{
							Singleton<AudioManager>.get_instance().QueueBroadcast(this.m_info.m_activeBroadcast);
						}
					}
				}
				else
				{
					if (data.m_broadcastCooldown > 0)
					{
						data.m_broadcastCooldown -= 1;
					}
					if (data.m_broadcastCooldown == 0)
					{
						data.m_broadcastCooldown = 236;
						if (this.m_info.m_activeBroadcast != null)
						{
							Singleton<AudioManager>.get_instance().QueueBroadcast(this.m_info.m_activeBroadcast);
						}
					}
				}
				if (group == null || group.m_refCount >= 2)
				{
					if (data.m_chirpCooldown > 0)
					{
						data.m_chirpCooldown -= 1;
					}
					else if (this.m_info.m_prefabDataIndex != -1)
					{
						string text = PrefabCollection<DisasterInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
						if (Locale.Exists("CHIRP_DISASTER", text))
						{
							string disasterName = Singleton<DisasterManager>.get_instance().GetDisasterName(disasterID);
							Singleton<MessageManager>.get_instance().TryCreateMessage("CHIRP_DISASTER", text, Singleton<MessageManager>.get_instance().GetRandomResidentID(), disasterName);
							data.m_chirpCooldown = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8, 24);
						}
					}
				}
			}
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (this.m_info.m_disasterWarningGuide != null && properties != null)
			{
				this.m_info.m_disasterWarningGuide.Activate(properties.m_disasterWarning, this.m_info);
			}
		}
		if ((data.m_flags & DisasterData.Flags.Repeat) != DisasterData.Flags.None && ((data.m_flags & (DisasterData.Flags.Finished | DisasterData.Flags.UnReported)) == DisasterData.Flags.Finished || (data.m_flags & (DisasterData.Flags.Clearing | DisasterData.Flags.UnDetected)) == (DisasterData.Flags.Clearing | DisasterData.Flags.UnDetected)))
		{
			this.StartDisaster(disasterID, ref data);
		}
	}

	protected virtual bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		return Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null && Singleton<DisasterManager>.get_instance().m_DisasterWrapper.IsCustomDisaster(disasterID);
	}

	protected virtual bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		return Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null && Singleton<DisasterManager>.get_instance().m_DisasterWrapper.IsCustomDisaster(disasterID);
	}

	protected virtual bool IsStillClearing(ushort disasterID, ref DisasterData data)
	{
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null && Singleton<DisasterManager>.get_instance().m_DisasterWrapper.IsCustomDisaster(disasterID))
		{
			return true;
		}
		InstanceID id = default(InstanceID);
		id.Disaster = disasterID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		return group != null && group.m_refCount > 1;
	}

	protected virtual void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		data.m_flags = ((data.m_flags & ~(DisasterData.Flags.Active | DisasterData.Flags.Clearing | DisasterData.Flags.Finished | DisasterData.Flags.Significant | DisasterData.Flags.Detected | DisasterData.Flags.Follow | DisasterData.Flags.Located | DisasterData.Flags.UnDetected | DisasterData.Flags.Warning | DisasterData.Flags.Repeat | DisasterData.Flags.UnReported)) | DisasterData.Flags.Emerging);
		data.m_startFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		data.m_casualtiesCount = 0u;
		data.m_destroyedRoadLength = 0u;
		data.m_destroyedTrackLength = 0u;
		data.m_destroyedPowerLength = 0u;
		data.m_treeFireCount = 0u;
		data.m_collapsedCount = 0;
		data.m_upgradedCount = 0;
		data.m_buildingFireCount = 0;
		data.m_chirpCooldown = 0;
		data.m_broadcastCooldown = 0;
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null)
		{
			Singleton<DisasterManager>.get_instance().m_DisasterWrapper.OnDisasterStarted(disasterID);
		}
	}

	protected virtual void ActivateDisaster(ushort disasterID, ref DisasterData data)
	{
		data.m_flags = ((data.m_flags & ~DisasterData.Flags.Emerging) | DisasterData.Flags.Active);
		data.m_activationFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null)
		{
			Singleton<DisasterManager>.get_instance().m_DisasterWrapper.OnDisasterActivated(disasterID);
		}
		if ((data.m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None && this.m_experienceMilestone != null)
		{
			this.m_experienceMilestone.Unlock();
		}
	}

	protected virtual void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		data.m_flags = ((data.m_flags & ~(DisasterData.Flags.Emerging | DisasterData.Flags.Active)) | DisasterData.Flags.Clearing);
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null)
		{
			Singleton<DisasterManager>.get_instance().m_DisasterWrapper.OnDisasterDeactivated(disasterID);
		}
	}

	protected virtual void EndDisaster(ushort disasterID, ref DisasterData data)
	{
		data.m_flags = ((data.m_flags & ~(DisasterData.Flags.Emerging | DisasterData.Flags.Active | DisasterData.Flags.Clearing)) | DisasterData.Flags.Finished);
		if (Singleton<DisasterManager>.get_instance().m_DisasterWrapper != null)
		{
			Singleton<DisasterManager>.get_instance().m_DisasterWrapper.OnDisasterFinished(disasterID);
		}
	}

	public void StartNow(ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active | DisasterData.Flags.Clearing | DisasterData.Flags.Finished)) == DisasterData.Flags.None)
		{
			this.StartDisaster(disasterID, ref data);
		}
		else if ((data.m_flags & DisasterData.Flags.Persistent) != DisasterData.Flags.None)
		{
			data.m_flags |= DisasterData.Flags.Repeat;
		}
	}

	public void ActivateNow(ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			this.ActivateDisaster(disasterID, ref data);
		}
	}

	public void DeactivateNow(ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			this.DeactivateDisaster(disasterID, ref data);
		}
	}

	public virtual bool CanAffectAt(ushort disasterID, ref DisasterData data, Vector3 position, out float priority)
	{
		priority = 0f;
		return (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active | DisasterData.Flags.Clearing)) != DisasterData.Flags.None;
	}

	public virtual int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		return 100;
	}

	protected virtual float GetMinimumEdgeDistance()
	{
		return 100f;
	}

	public bool FindRandomTarget(out Vector3 target, out float angle)
	{
		GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
		SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
		if (instance.m_areaCount > 0)
		{
			int num = instance2.m_randomizer.Int32(1, instance.m_areaCount);
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					if (instance.GetArea(j, i) == num)
					{
						float num2;
						float num3;
						float num4;
						float num5;
						instance.GetAreaBounds(j, i, out num2, out num3, out num4, out num5);
						float minimumEdgeDistance = this.GetMinimumEdgeDistance();
						if (!instance.IsUnlocked(j - 1, i))
						{
							num2 += minimumEdgeDistance;
						}
						if (!instance.IsUnlocked(j, i - 1))
						{
							num3 += minimumEdgeDistance;
						}
						if (!instance.IsUnlocked(j + 1, i))
						{
							num4 -= minimumEdgeDistance;
						}
						if (!instance.IsUnlocked(j, i + 1))
						{
							num5 -= minimumEdgeDistance;
						}
						float num6 = (float)instance2.m_randomizer.Int32(0, 10000) * 0.0001f;
						float num7 = (float)instance2.m_randomizer.Int32(0, 10000) * 0.0001f;
						target.x = num2 + (num4 - num2) * num6;
						target.y = 0f;
						target.z = num3 + (num5 - num3) * num7;
						this.ClampDisasterTarget(ref target);
						target.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(target, false, 0f);
						angle = (float)instance2.m_randomizer.Int32(0, 10000) * 0.0006283185f;
						return true;
					}
				}
			}
		}
		target = Vector3.get_zero();
		angle = 0f;
		return false;
	}

	public void ClampDisasterTarget(ref Vector3 target)
	{
		GameAreaManager instance = Singleton<GameAreaManager>.get_instance();
		float minimumEdgeDistance = this.GetMinimumEdgeDistance();
		int num;
		int num2;
		instance.GetTileXZ(target, out num, out num2);
		float num3;
		float num4;
		float num5;
		float num6;
		instance.GetAreaBounds(num, num2, out num3, out num4, out num5, out num6);
		if (!instance.IsUnlocked(num - 1, num2))
		{
			float num7 = target.x - num3;
			if (num7 < minimumEdgeDistance)
			{
				target.x += minimumEdgeDistance - num7;
			}
		}
		if (!instance.IsUnlocked(num, num2 - 1))
		{
			float num8 = target.z - num4;
			if (num8 < minimumEdgeDistance)
			{
				target.z += minimumEdgeDistance - num8;
			}
		}
		if (!instance.IsUnlocked(num + 1, num2))
		{
			float num9 = num5 - target.x;
			if (num9 < minimumEdgeDistance)
			{
				target.x += num9 - minimumEdgeDistance;
			}
		}
		if (!instance.IsUnlocked(num, num2 + 1))
		{
			float num10 = num6 - target.z;
			if (num10 < minimumEdgeDistance)
			{
				target.z += num10 - minimumEdgeDistance;
			}
		}
		if (!instance.IsUnlocked(num - 1, num2 - 1))
		{
			Vector3 vector;
			vector..ctor(num3, target.y, num4);
			Vector3 vector2 = target - vector;
			if (vector2.get_sqrMagnitude() < minimumEdgeDistance * minimumEdgeDistance)
			{
				if (vector2.get_sqrMagnitude() < 1f)
				{
					vector2..ctor(1f, 0f, 1f);
				}
				target = vector + vector2.get_normalized() * minimumEdgeDistance;
			}
		}
		if (!instance.IsUnlocked(num + 1, num2 - 1))
		{
			Vector3 vector3;
			vector3..ctor(num5, target.y, num4);
			Vector3 vector4 = target - vector3;
			if (vector4.get_sqrMagnitude() < minimumEdgeDistance * minimumEdgeDistance)
			{
				if (vector4.get_sqrMagnitude() < 1f)
				{
					vector4..ctor(-1f, 0f, 1f);
				}
				target = vector3 + vector4.get_normalized() * minimumEdgeDistance;
			}
		}
		if (!instance.IsUnlocked(num - 1, num2 + 1))
		{
			Vector3 vector5;
			vector5..ctor(num3, target.y, num6);
			Vector3 vector6 = target - vector5;
			if (vector6.get_sqrMagnitude() < minimumEdgeDistance * minimumEdgeDistance)
			{
				if (vector6.get_sqrMagnitude() < 1f)
				{
					vector6..ctor(1f, 0f, -1f);
				}
				target = vector5 + vector6.get_normalized() * minimumEdgeDistance;
			}
		}
		if (!instance.IsUnlocked(num + 1, num2 + 1))
		{
			Vector3 vector7;
			vector7..ctor(num5, target.y, num6);
			Vector3 vector8 = target - vector7;
			if (vector8.get_sqrMagnitude() < minimumEdgeDistance * minimumEdgeDistance)
			{
				if (vector8.get_sqrMagnitude() < 1f)
				{
					vector8..ctor(-1f, 0f, -1f);
				}
				target = vector7 + vector8.get_normalized() * minimumEdgeDistance;
			}
		}
	}

	public virtual bool CanSelfTrigger()
	{
		return false;
	}

	public virtual bool HasDirection()
	{
		return false;
	}

	public virtual bool SupportIntensity()
	{
		return true;
	}

	public virtual string GenerateName(ushort disasterID, ref DisasterData data)
	{
		if (this.m_info.m_baseInfo != null && this.m_info.m_baseInfo.m_prefabDataIndex != -1)
		{
			string text = PrefabCollection<DisasterInfo>.PrefabName((uint)this.m_info.m_baseInfo.m_prefabDataIndex);
			return Locale.Get("DISASTER_TITLE", text);
		}
		if (this.m_info.m_prefabDataIndex != -1)
		{
			string text2 = PrefabCollection<DisasterInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
			return Locale.Get("DISASTER_TITLE", text2);
		}
		return null;
	}

	public virtual bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		InstanceID empty = InstanceID.Empty;
		empty.Disaster = disasterID;
		DisasterAI.m_tempList.Clear();
		InstanceManager.GetAllGroupInstances(empty, DisasterAI.m_tempList);
		if (DisasterAI.m_tempList.m_size > 1)
		{
			Vector3 vector;
			vector..ctor(1000000f, 1000000f, 1000000f);
			Vector3 vector2;
			vector2..ctor(-1000000f, -1000000f, -1000000f);
			for (int i = 0; i < DisasterAI.m_tempList.m_size; i++)
			{
				InstanceID id = DisasterAI.m_tempList.m_buffer[i];
				Vector3 vector3;
				Quaternion quaternion;
				Vector3 vector4;
				if (id.Disaster == 0 && InstanceManager.GetPosition(id, out vector3, out quaternion, out vector4))
				{
					vector4 = quaternion * vector4 * 0.5f;
					vector = Vector3.Min(vector, vector3 - vector4);
					vector2 = Vector3.Max(vector2, vector3 + vector4);
				}
			}
			position = (vector + vector2) * 0.5f;
			rotation = Quaternion.get_identity();
			size = Vector3.Max(vector2 - vector, new Vector3(100f, 100f, 100f));
		}
		else
		{
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			position = instance.m_disasters.m_buffer[(int)empty.Disaster].m_targetPosition;
			rotation = Quaternion.get_identity();
			size..ctor(100f, 100f, 100f);
		}
		return true;
	}

	public virtual bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.Default;
		return false;
	}

	public ManualMilestone m_experienceMilestone;

	[NonSerialized]
	public DisasterInfo m_info;

	protected static FastList<InstanceID> m_tempList = new FastList<InstanceID>();
}
