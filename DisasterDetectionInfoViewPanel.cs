﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class DisasterDetectionInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_CoverageGradient = base.Find<UITextureSprite>("Gradient");
		this.m_label = base.Find<UILabel>("Label");
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.m_label.set_text(Locale.Get("INFOVIEWS", "DisasterDetection"));
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized && Singleton<LoadingManager>.get_exists())
		{
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[28].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[28].m_inactiveColor);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[28].m_activeColorB);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorC", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[28].m_targetColor);
					this.m_CoverageGradient.get_renderMaterial().SetFloat("_Step", 0.222222224f);
					this.m_CoverageGradient.get_renderMaterial().SetFloat("_Scalar", 1.1f);
					this.m_CoverageGradient.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.OnLocaleChanged();
			this.m_initialized = true;
		}
	}

	private UITextureSprite m_CoverageGradient;

	private UILabel m_label;

	private bool m_initialized;
}
