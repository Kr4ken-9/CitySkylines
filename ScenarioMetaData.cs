﻿using System;
using System.IO;
using ColossalFramework.IO;
using ColossalFramework.Packaging;

[Serializable]
public class ScenarioMetaData : MetaData
{
	public bool IsBuiltinScenario(string path)
	{
		return this.builtin && path.StartsWith(Path.Combine(DataLocation.get_gameContentPath(), "Scenarios"));
	}

	public override bool getBuiltin
	{
		get
		{
			return this.builtin;
		}
	}

	public override string getEnvironment
	{
		get
		{
			return this.environment;
		}
	}

	public override int getBuildableArea
	{
		get
		{
			return this.buildableArea;
		}
	}

	public override string getPopulation
	{
		get
		{
			return this.population;
		}
	}

	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public bool isPublished;

	public bool builtin;

	public string cityName;

	public string mapThemeRef;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;

	public bool achievementsDisabled;

	public ModInfo[] mods;

	public ModInfo[] assets;

	public string environment;

	public string population;

	public string cash;

	public string scenarioName;

	public string scenarioDescription;

	public byte[] winConditions;

	public byte[] losingConditions;

	public int buildableArea;

	public float maxResourceAmount;

	public uint oilAmount;

	public uint oreAmount;

	public uint forestAmount;

	public uint fertileLandAmount;

	public uint waterAmount;

	public int incomingRoads;

	public int outgoingRoads;

	public int incomingTrainTracks;

	public int outgoingTrainTracks;

	public int incomingShipPath;

	public int outgoingShipPath;

	public int incomingPlanePath;

	public int outgoingPlanePath;

	public bool invertTraffic;
}
