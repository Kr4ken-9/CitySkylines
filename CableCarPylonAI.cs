﻿using System;
using ColossalFramework;
using UnityEngine;

public class CableCarPylonAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[9];
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, ref Building data, int layerMask, ref RenderManager.Instance instance)
	{
		this.RenderMeshes(cameraInfo, buildingID, ref data, layerMask, ref instance);
		base.RenderProps(cameraInfo, buildingID, ref data, layerMask, ref instance, (data.m_flags & Building.Flags.Collapsed) == Building.Flags.None, true);
	}

	public override bool ClearOccupiedZoning()
	{
		return true;
	}

	public override void GetTerrainLimits(ushort buildingID, ref Building data, Vector3 meshPosition, out float min, out float max)
	{
		min = meshPosition.y - 255f;
		max = ((!this.m_limitTerrain) ? 1000000f : meshPosition.y);
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		if (testOnly)
		{
			return false;
		}
		Building.Flags flags = data.m_flags;
		if ((flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			data.m_flags |= Building.Flags.Collapsed;
			data.m_electricityBuffer = 0;
			Notification.Problem problems = data.m_problems;
			data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
			if (data.m_problems != problems)
			{
				Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, data.m_problems);
			}
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, true);
			Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, data.m_flags ^ flags);
			return true;
		}
		return false;
	}

	protected override bool CalculatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None && base.CalculatePropGroupData(buildingID, ref buildingData, props, trees, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
	}

	protected override void PopulatePropGroupData(ushort buildingID, ref Building buildingData, bool props, bool trees, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			base.PopulatePropGroupData(buildingID, ref buildingData, props, trees, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
	}

	public bool m_limitTerrain = true;
}
