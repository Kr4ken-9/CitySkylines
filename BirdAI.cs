﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class BirdAI : AnimalAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("ANIMAL_STATUS_FLYING");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
		if (data.m_citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(data.m_citizen);
		}
		else if (data.m_targetBuilding == 0 || (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
		}
	}

	public override void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(instanceID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0.m_velocity = Vector3.get_zero();
			data.m_frame0.m_position = vector;
			data.m_frame0.m_rotation = rotation;
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos = vector2;
			data.Spawn(instanceID);
		}
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
			}
		}
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
		float sqrMagnitude = frameData.m_velocity.get_sqrMagnitude();
		if (sqrMagnitude > 0.01f)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
		}
		Vector3 vector = citizenData.m_targetPos - frameData.m_position;
		float sqrMagnitude2 = vector.get_sqrMagnitude();
		float num = Mathf.Max(sqrMagnitude * 3f, 3f);
		if (sqrMagnitude2 < num && citizenData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			Vector3 vector2;
			Vector3 vector3;
			Vector2 vector4;
			CitizenInstance.Flags flags;
			info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, instanceID, out vector2, out vector3, out vector4, out flags);
			citizenData.m_targetPos = vector2;
			vector = citizenData.m_targetPos - frameData.m_position;
			sqrMagnitude2 = vector.get_sqrMagnitude();
		}
		float num2 = this.m_info.m_walkSpeed;
		float num3 = 2f;
		if (sqrMagnitude2 < 1f)
		{
			vector = Vector3.get_zero();
		}
		else
		{
			float num4 = Mathf.Sqrt(sqrMagnitude2);
			num2 = Mathf.Min(num2, num4 * 0.5f);
			Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
			vector = quaternion * vector;
			if (vector.z < Mathf.Abs(vector.x) * 5f)
			{
				if (vector.x >= 0f)
				{
					vector.x = Mathf.Max(1f, vector.x);
				}
				else
				{
					vector.x = Mathf.Min(-1f, vector.x);
				}
				vector.z = Mathf.Abs(vector.x) * 5f;
				num2 = Mathf.Min(3f, num4 * 0.1f);
			}
			vector = Vector3.ClampMagnitude(frameData.m_rotation * vector, num2);
		}
		Vector3 vector5 = vector - frameData.m_velocity;
		float magnitude = vector5.get_magnitude();
		vector5 *= num3 / Mathf.Max(magnitude, num3);
		frameData.m_velocity += vector5;
		float sqrMagnitude3 = frameData.m_velocity.get_sqrMagnitude();
		if (sqrMagnitude3 > 0.01f)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
			frameData.m_rotation = Quaternion.LookRotation(new Vector3(frameData.m_velocity.x, 0f, frameData.m_velocity.z));
		}
		if (this.m_randomEffect != null && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(40u) == 0)
		{
			InstanceID instance2 = default(InstanceID);
			instance2.CitizenInstance = instanceID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(frameData.m_position - frameData.m_velocity * 2f, Vector3.get_up(), 0f);
			float num5 = 3.75f;
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_randomEffect, instance2, spawnArea, frameData.m_velocity * num5, 0f, 1f, Singleton<CitizenManager>.get_instance().m_audioGroup);
		}
	}

	public EffectInfo m_randomEffect;
}
