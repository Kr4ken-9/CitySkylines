﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.UI;
using UnityEngine;

public class SnapshotTool : ToolBase
{
	public SnapshotTool.SnapshotMode Mode
	{
		get
		{
			return this.m_mode;
		}
		set
		{
			if (this.m_mode != value)
			{
				this.OnDisable();
				this.m_mode = value;
				this.OnEnable();
			}
		}
	}

	public string snapShotPath
	{
		get
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				string text = Path.Combine(this.m_Path, Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier);
				Directory.CreateDirectory(text);
				return text;
			}
			return null;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_Path = Path.Combine(DataLocation.get_localApplicationData(), "Snapshots");
		this.m_LastSnapshotTime = Time.get_realtimeSinceStartup();
		this.m_mode = SnapshotTool.SnapshotMode.Snapshot;
	}

	protected override void OnToolGUI(Event e)
	{
		if (this.m_toolController.IsInsideUI)
		{
			return;
		}
		if (e.get_type() == null && e.get_button() == 0 && Time.get_realtimeSinceStartup() - this.m_LastSnapshotTime > 1f)
		{
			if (this.m_mode == SnapshotTool.SnapshotMode.Snapshot)
			{
				this.Snapshot(644, 360);
			}
			else if (this.m_mode == SnapshotTool.SnapshotMode.Thumbnail)
			{
				this.Snapshot(AssetImporterThumbnails.thumbWidth, AssetImporterThumbnails.thumbHeight);
			}
			else if (this.m_mode == SnapshotTool.SnapshotMode.Infotooltip)
			{
				this.Snapshot(SnapshotTool.tooltipWidth, SnapshotTool.tooltipHeight);
			}
			this.m_LastSnapshotTime = Time.get_realtimeSinceStartup();
		}
	}

	public void StartShot(int width, int height, Action callback)
	{
		base.StartCoroutine(this.Snapshot(width, height, callback, false));
	}

	private void Snapshot(int width, int height)
	{
		base.StartCoroutine(this.Snapshot(width, height, null, true));
	}

	[DebuggerHidden]
	private IEnumerator Snapshot(int width, int height, Action callback, bool effects)
	{
		SnapshotTool.<Snapshot>c__Iterator0 <Snapshot>c__Iterator = new SnapshotTool.<Snapshot>c__Iterator0();
		<Snapshot>c__Iterator.width = width;
		<Snapshot>c__Iterator.height = height;
		<Snapshot>c__Iterator.effects = effects;
		<Snapshot>c__Iterator.callback = callback;
		<Snapshot>c__Iterator.$this = this;
		return <Snapshot>c__Iterator;
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		base.ToolCursor = this.m_cursor;
		this.m_UICache[0] = Singleton<NotificationManager>.get_instance().NotificationsVisible;
		this.m_UICache[1] = Singleton<GameAreaManager>.get_instance().BordersVisible;
		this.m_UICache[2] = Singleton<DistrictManager>.get_instance().NamesVisible;
		this.m_UICache[3] = Singleton<PropManager>.get_instance().MarkersVisible;
		this.m_UICache[4] = Singleton<GameAreaManager>.get_instance().ThumbnailMode;
		this.m_UICache[5] = Singleton<NetManager>.get_instance().RoadNamesVisible;
		Singleton<NotificationManager>.get_instance().NotificationsVisible = false;
		Singleton<GameAreaManager>.get_instance().BordersVisible = false;
		Singleton<DistrictManager>.get_instance().NamesVisible = false;
		Singleton<PropManager>.get_instance().MarkersVisible = false;
		Singleton<DisasterManager>.get_instance().MarkersVisible = false;
		Singleton<GameAreaManager>.get_instance().ThumbnailMode = (this.m_mode == SnapshotTool.SnapshotMode.Thumbnail);
		Singleton<NetManager>.get_instance().RoadNamesVisible = false;
		Camera camera = Singleton<RenderManager>.get_instance().CurrentCameraInfo.m_camera;
		if (camera != null)
		{
			OverlayEffect component = camera.GetComponent<OverlayEffect>();
			if (component != null)
			{
				component.m_tooltipMode = (this.m_mode == SnapshotTool.SnapshotMode.Infotooltip);
			}
			FogEffect component2 = camera.GetComponent<FogEffect>();
			if (component2 != null)
			{
				this.m_FogCache = component2.get_enabled();
				component2.set_enabled(false);
			}
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None && this.m_toolController.m_editPrefabInfo != null)
		{
			CitizenInfo component3 = this.m_toolController.m_editPrefabInfo.GetComponent<CitizenInfo>();
			if (component3 != null)
			{
				Animator component4 = component3.GetComponent<Animator>();
				component4.Rebind();
				component4.set_enabled(false);
			}
		}
		UIPanel uIPanel = UIView.Find<UIPanel>("DecorationProperties");
		if (uIPanel != null)
		{
			uIPanel.set_isVisible(false);
		}
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Singleton<NotificationManager>.get_instance().NotificationsVisible = this.m_UICache[0];
		Singleton<GameAreaManager>.get_instance().BordersVisible = this.m_UICache[1];
		Singleton<DistrictManager>.get_instance().NamesVisible = this.m_UICache[2];
		Singleton<PropManager>.get_instance().MarkersVisible = this.m_UICache[3];
		Singleton<GameAreaManager>.get_instance().ThumbnailMode = this.m_UICache[4];
		Singleton<DisasterManager>.get_instance().MarkersVisible = true;
		Singleton<NetManager>.get_instance().RoadNamesVisible = this.m_UICache[5];
		Camera camera = Singleton<RenderManager>.get_instance().CurrentCameraInfo.m_camera;
		if (camera != null)
		{
			OverlayEffect component = camera.GetComponent<OverlayEffect>();
			if (component != null)
			{
				component.m_tooltipMode = false;
			}
			FogEffect component2 = camera.GetComponent<FogEffect>();
			if (component2 != null)
			{
				component2.set_enabled(this.m_FogCache);
			}
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None && this.m_toolController.m_editPrefabInfo != null)
		{
			CitizenInfo component3 = this.m_toolController.m_editPrefabInfo.GetComponent<CitizenInfo>();
			if (component3 != null)
			{
				component3.GetComponent<Animator>().set_enabled(true);
			}
		}
		UIPanel uIPanel = UIView.Find<UIPanel>("DecorationProperties");
		if (uIPanel != null)
		{
			uIPanel.GetComponent<DecorationPropertiesPanel>().RefreshVisibility(this.m_toolController.m_editPrefabInfo);
		}
	}

	protected override void OnToolLateUpdate()
	{
		base.OnToolLateUpdate();
		ToolBase.OverrideInfoMode = false;
	}

	public const int kSteamWidth = 644;

	public const int kSteamHeight = 360;

	public static readonly int tooltipWidth = 492;

	public static readonly int tooltipHeight = 147;

	public AudioClip m_shutterSound;

	private SnapshotTool.SnapshotMode m_mode;

	private string m_Path;

	public CursorInfo m_cursor;

	private bool[] m_UICache = new bool[6];

	private bool m_FogCache;

	private float m_LastSnapshotTime;

	public enum SnapshotMode
	{
		Snapshot,
		Thumbnail,
		Infotooltip
	}
}
