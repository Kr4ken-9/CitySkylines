﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class PedestrianBridgeAI : PlayerNetAI
{
	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Pollution || base.ColorizeProps(infoMode);
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.None)
		{
			Color color = this.m_info.m_color;
			if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				float num = 0.5f;
				color.r = color.r * (1f - num) + num * 0.75f;
				color.g = color.g * (1f - num) + num * 0.75f;
				color.b = color.b * (1f - num) + num * 0.75f;
			}
			color.a = (float)(255 - data.m_wetness) * 0.003921569f;
			return color;
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(segmentID, ref data, infoMode);
		}
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.None)
		{
			int num = 0;
			int num2 = 0;
			NetManager instance = Singleton<NetManager>.get_instance();
			int num3 = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					if ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
					{
						num++;
					}
					num2 += (int)instance.m_segments.m_buffer[(int)segment].m_wetness;
					num3++;
				}
			}
			if (num3 != 0)
			{
				num2 /= num3;
			}
			Color color = this.m_info.m_color;
			if (num != 0)
			{
				float num4 = (float)num / (float)num3 * 0.5f;
				color.r = color.r * (1f - num4) + num4 * 0.75f;
				color.g = color.g * (1f - num4) + num4 * 0.75f;
				color.b = color.b * (1f - num4) + num4 * 0.75f;
			}
			color.a = (float)(255 - num2) * 0.003921569f;
			return color;
		}
		if (infoMode != InfoManager.InfoMode.Destruction)
		{
			return base.GetColor(nodeID, ref data, infoMode);
		}
		NetManager instance2 = Singleton<NetManager>.get_instance();
		bool flag = false;
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = data.GetSegment(j);
			if (segment2 != 0 && (instance2.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				flag = true;
			}
		}
		if (flag)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_negativeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	public override void ReleaseNode(ushort nodeID, ref NetNode data)
	{
		if (data.m_lane != 0u)
		{
			this.RemoveLaneConnection(nodeID, ref data);
		}
		base.ReleaseNode(nodeID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		Notification.Problem problem = Notification.RemoveProblems(data.m_problems, Notification.Problem.Flood);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = (position + position2) * 0.5f;
		bool flag = false;
		float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector));
		if (num > vector.y + 1f)
		{
			flag = true;
			if ((data.m_flags & NetSegment.Flags.Flooded) == NetSegment.Flags.None)
			{
				data.m_flags |= NetSegment.Flags.Flooded;
				data.m_modifiedIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex++;
			}
			problem = Notification.AddProblems(problem, Notification.Problem.Flood | Notification.Problem.MajorProblem);
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.Flooded;
			if (num > vector.y)
			{
				flag = true;
				problem = Notification.AddProblems(problem, Notification.Problem.Flood);
			}
		}
		int num2 = (int)data.m_wetness;
		if (!instance.m_treatWetAsSnow)
		{
			if (flag)
			{
				num2 = 255;
			}
			else
			{
				int num3 = -(num2 + 63 >> 5);
				float num4 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(vector, false);
				if (num4 != 0f)
				{
					int num5 = Mathf.RoundToInt(Mathf.Min(num4 * 4000f, 1000f));
					num3 += Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num5, num5 + 99) / 100;
				}
				num2 = Mathf.Clamp(num2 + num3, 0, 255);
			}
		}
		if (num2 != (int)data.m_wetness)
		{
			if (Mathf.Abs((int)data.m_wetness - num2) > 10)
			{
				data.m_wetness = (byte)num2;
				InstanceID empty = InstanceID.Empty;
				empty.NetSegment = segmentID;
				instance.AddSmoothColor(empty);
				empty.NetNode = data.m_startNode;
				instance.AddSmoothColor(empty);
				empty.NetNode = data.m_endNode;
				instance.AddSmoothColor(empty);
			}
			else
			{
				data.m_wetness = (byte)num2;
				instance.m_wetnessChanged = 256;
			}
		}
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(vector);
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.BikeBan) != DistrictPolicies.CityPlanning.None)
		{
			data.m_flags |= NetSegment.Flags.BikeBan;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.BikeBan;
		}
		data.m_problems = problem;
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None && (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(segmentID & 15)))
		{
			int delta = Mathf.RoundToInt(data.m_averageLength);
			StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.DestroyedLength);
			statisticBase.Add(delta);
		}
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		float num2 = (Mathf.Max(0f, startHeight) + Mathf.Max(0f, endHeight)) * 0.5f;
		int num3 = Mathf.RoundToInt(num / 8f + 0.1f);
		int num4 = Mathf.RoundToInt(num2 / 4f + 0.1f);
		int result = this.m_constructionCost * num3 + this.m_elevationCost * num4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & (NetNode.Flags.Outside | NetNode.Flags.OnGround)) == NetNode.Flags.None && this.m_bridgePillarInfo != null)
		{
			building = this.m_bridgePillarInfo;
			heightOffset = this.m_bridgePillarOffset - 1f - this.m_bridgePillarInfo.m_generatedInfo.m_size.y;
			return;
		}
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + 1000f;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 2;
	}

	public override bool IsOverground()
	{
		return true;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return false;
		}
		if (DefaultTool.IsOutOfCityArea(segmentID))
		{
			return false;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_7C_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_7C_cp_1 = disaster;
				expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength = expr_7C_cp_0[(int)expr_7C_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Notification.Problem problems = data.m_problems;
		data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
		if (data.m_problems != problems)
		{
			Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
		return true;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_4B_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_4B_cp_1 = disaster;
				expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength = expr_4B_cp_0[(int)expr_4B_cp_1].m_destroyedRoadLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		if ((data.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			Notification.Problem problems = data.m_problems;
			data.m_problems = (Notification.Problem.StructureDamaged | Notification.Problem.FatalProblem);
			if (data.m_problems != problems)
			{
				Singleton<NetManager>.get_instance().UpdateSegmentNotifications(segmentID, problems, data.m_problems);
			}
		}
	}

	public override void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
		Singleton<NetManager>.get_instance().UpdateNode(nodeID, 0, 10);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & (NetNode.Flags.Temporary | NetNode.Flags.OnGround)) == NetNode.Flags.OnGround)
		{
			uint num = 0u;
			byte offset = 0;
			float num2 = 1E+10f;
			if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None)
			{
				PathUnit.Position pathPos;
				PathUnit.Position position;
				float num3;
				float num4;
				if (this.m_connectService1 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService1, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, false, true, 16f, out pathPos, out position, out num3, out num4) && num3 < num2)
				{
					num2 = num3;
					num = PathManager.GetLaneID(pathPos);
					offset = pathPos.m_offset;
				}
				PathUnit.Position pathPos2;
				PathUnit.Position position2;
				float num5;
				float num6;
				if (this.m_connectService2 != ItemClass.Service.None && PathManager.FindPathPosition(data.m_position, this.m_connectService2, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, false, true, 16f, out pathPos2, out position2, out num5, out num6) && num5 < num2)
				{
					num = PathManager.GetLaneID(pathPos2);
					offset = pathPos2.m_offset;
				}
			}
			if (num != data.m_lane)
			{
				if (data.m_lane != 0u)
				{
					this.RemoveLaneConnection(nodeID, ref data);
				}
				if (num != 0u)
				{
					this.AddLaneConnection(nodeID, ref data, num, offset);
				}
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].CalculatePosition((float)offset * 0.003921569f);
		if (vector.y != data.m_position.y)
		{
			data.m_position.y = vector.y;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				break;
			}
			num = num2;
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
		float num4 = (float)data.m_elevation;
		if (this.IsUnderground())
		{
			num4 = -num4;
		}
		float num5 = NetSegment.SampleTerrainHeight(this.m_info, data.m_position, false, num4);
		if (num5 != data.m_position.y)
		{
			data.m_position.y = num5;
			Singleton<NetManager>.get_instance().UpdateNode(nodeID);
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		base.UpdateLanes(segmentID, ref data, loading);
		if (!loading)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = Mathf.Max((int)((data.m_bounds.get_min().x - 16f) / 64f + 135f), 0);
			int num2 = Mathf.Max((int)((data.m_bounds.get_min().z - 16f) / 64f + 135f), 0);
			int num3 = Mathf.Min((int)((data.m_bounds.get_max().x + 16f) / 64f + 135f), 269);
			int num4 = Mathf.Min((int)((data.m_bounds.get_max().z + 16f) / 64f + 135f), 269);
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					ushort num5 = instance.m_nodeGrid[i * 270 + j];
					int num6 = 0;
					while (num5 != 0)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num5].Info;
						Vector3 position = instance.m_nodes.m_buffer[(int)num5].m_position;
						float num7 = Mathf.Max(Mathf.Max(data.m_bounds.get_min().x - 16f - position.x, data.m_bounds.get_min().z - 16f - position.z), Mathf.Max(position.x - data.m_bounds.get_max().x - 16f, position.z - data.m_bounds.get_max().z - 16f));
						if (num7 < 0f)
						{
							info.m_netAI.NearbyLanesUpdated(num5, ref instance.m_nodes.m_buffer[(int)num5]);
						}
						num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
						if (++num6 >= 32768)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
		}
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		if (vector2.y > -0.5f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 0;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		Vector3 vector2;
		if (data.m_normals != null && data.m_normals.Length > vertexIndex)
		{
			vector2 = data.m_normals[vertexIndex];
		}
		else
		{
			vector2 = Vector3.get_up();
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		if (vector2.y > -0.5f)
		{
			result.g = 255;
		}
		else
		{
			result.g = 0;
		}
		result.a = 0;
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 0f;
	}

	public override float GetTerrainLowerOffset()
	{
		return -0.1f;
	}

	public override bool FlattenGroundNodes()
	{
		return true;
	}

	public ItemClass.Service m_connectService1 = ItemClass.Service.Road;

	public ItemClass.Service m_connectService2 = ItemClass.Service.PublicTransport;

	public BuildingInfo m_bridgePillarInfo;

	[CustomizableProperty("Elevation Cost", "Properties")]
	public int m_elevationCost = 2000;

	[CustomizableProperty("Bridge Pillar Offset", "Properties")]
	public float m_bridgePillarOffset;
}
