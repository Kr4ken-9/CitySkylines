﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TransportStationAI : DepotAI
{
	public int GetPassengerCount(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_customBuffer1;
		ushort subBuilding = data.m_subBuilding;
		if (subBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			int num2 = 0;
			while (subBuilding != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)subBuilding].Info;
				if (info != null && info.m_buildingAI is TransportStationAI)
				{
					num += (int)instance.m_buildings.m_buffer[(int)subBuilding].m_customBuffer1;
				}
				subBuilding = instance.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
				if (++num2 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return num;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		this.CreateConnectionLines(buildingID, ref data);
		if (this.m_transportInfo != null)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				VehicleInfo.VehicleType vehicleType = this.m_transportInfo.m_vehicleType;
				if (vehicleType != VehicleInfo.VehicleType.Monorail)
				{
					if (vehicleType != VehicleInfo.VehicleType.Ferry)
					{
						if (vehicleType == VehicleInfo.VehicleType.Blimp)
						{
							Singleton<TransportManager>.get_instance().m_blimpLine.Activate(properties.m_blimpLine);
						}
					}
					else
					{
						Singleton<TransportManager>.get_instance().m_ferryLine.Activate(properties.m_ferryLine);
					}
				}
				else
				{
					Singleton<TransportManager>.get_instance().m_monorailLine.Activate(properties.m_monorailLine);
				}
			}
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseVehicles(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		this.CreateConnectionLines(buildingID, ref data);
	}

	private void CreateConnectionLines(ushort buildingID, ref Building data)
	{
		if (this.m_transportLineInfo != null && (data.m_flags & Building.Flags.Downgrading) == Building.Flags.None)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> outsideConnections = instance.GetOutsideConnections();
			int num = 0;
			for (int i = 0; i < outsideConnections.m_size; i++)
			{
				ushort num2 = outsideConnections.m_buffer[i];
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num2].Info;
				if (info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
				{
					num++;
				}
			}
			int num3 = 1;
			int num4 = 0;
			int num5 = 1;
			if (this.m_secondaryTransportInfo != null && this.m_secondaryTransportInfo.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
			{
				if (this.m_spawnPoints2 != null && this.m_spawnPoints2.Length != 0)
				{
					Randomizer randomizer;
					randomizer..ctor((int)buildingID);
					num3 = this.m_spawnPoints2.Length;
					num4 = randomizer.Int32((uint)num3);
					num5 = Mathf.Max(1, num3 / Mathf.Max(1, num));
				}
			}
			else if (this.m_spawnPoints != null && this.m_spawnPoints.Length != 0)
			{
				Randomizer randomizer2;
				randomizer2..ctor((int)buildingID);
				num3 = this.m_spawnPoints.Length;
				num4 = randomizer2.Int32((uint)num3);
				num5 = Mathf.Max(1, num3 / Mathf.Max(1, num));
			}
			for (int j = 0; j < outsideConnections.m_size; j++)
			{
				ushort num6 = outsideConnections.m_buffer[j];
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num6].Info;
				if (info2.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info2.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
				{
					this.CreateConnectionLines(buildingID, ref data, num6, ref instance.m_buildings.m_buffer[(int)num6], num4);
					num4 += num5;
					if (num4 >= num3)
					{
						num4 -= num3;
					}
				}
			}
		}
	}

	private void ReleaseLines(ushort node)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
			if (segment != 0)
			{
				instance.ReleaseSegment(segment, true);
			}
		}
	}

	private void RemoveConnectionLines(ushort buildingID, ref Building data)
	{
		if (this.m_transportLineInfo != null)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = 0;
			ushort num2 = data.m_netNode;
			int num3 = 0;
			while (num2 != 0)
			{
				NetInfo info = instance.m_nodes.m_buffer[(int)num2].Info;
				ushort nextBuildingNode = instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
				if (info.m_class.m_layer == ItemClass.Layer.PublicTransport && info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
				{
					if (num != 0)
					{
						instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode = nextBuildingNode;
					}
					else
					{
						data.m_netNode = nextBuildingNode;
					}
					this.ReleaseLines(num2);
					instance.ReleaseNode(num2);
					num2 = num;
				}
				num = num2;
				num2 = nextBuildingNode;
				if (++num3 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		int num = 0;
		float num2 = 0f;
		if (this.m_transportLineInfo != null)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> outsideConnections = instance.GetOutsideConnections();
			for (int i = 0; i < outsideConnections.m_size; i++)
			{
				ushort num3 = outsideConnections.m_buffer[i];
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num3].Info;
				if (info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService && (instance.m_buildings.m_buffer[(int)num3].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					int num4;
					float num5;
					this.m_transportLineInfo.m_netAI.GetTransportAccumulation(out num4, out num5);
					num += num4;
					num2 = Mathf.Max(num2, num5);
				}
			}
		}
		if (num != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (num != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PublicTransport, (float)num, num2, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			int num = 0;
			float num2 = 0f;
			if (this.m_transportLineInfo != null)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				FastList<ushort> outsideConnections = instance.GetOutsideConnections();
				for (int i = 0; i < outsideConnections.m_size; i++)
				{
					ushort num3 = outsideConnections.m_buffer[i];
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num3].Info;
					if (info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService && (instance.m_buildings.m_buffer[(int)num3].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
					{
						int num4;
						float num5;
						this.m_transportLineInfo.m_netAI.GetTransportAccumulation(out num4, out num5);
						num += num4;
						num2 = Mathf.Max(num2, num5);
					}
				}
			}
			if (num != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (num != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.PublicTransport, (float)(-(float)num), num2, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	private void ReleaseVehicles(ushort buildingID, ref Building data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (instance.m_vehicles.m_buffer[(int)num].m_transportLine == 0)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				if (info.m_class.m_service == this.m_info.m_class.m_service && info.m_class.m_subService == this.m_info.m_class.m_subService)
				{
					info.m_vehicleAI.SetTarget(num, ref instance.m_vehicles.m_buffer[(int)num], 0);
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private void CreateConnectionLines(ushort buildingID, ref Building data, ushort targetID, ref Building target, int gateIndex)
	{
		if ((target.m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position;
			if (this.m_secondaryTransportInfo != null && this.m_secondaryTransportInfo.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
			{
				if (this.m_spawnPoints2 != null && this.m_spawnPoints2.Length != 0)
				{
					position = data.CalculatePosition(this.m_spawnPoints2[gateIndex].m_position);
				}
				else
				{
					position = data.CalculatePosition(this.m_spawnPosition2);
				}
			}
			else if (this.m_spawnPoints != null && this.m_spawnPoints.Length != 0)
			{
				position = data.CalculatePosition(this.m_spawnPoints[gateIndex].m_position);
			}
			else
			{
				position = data.CalculatePosition(this.m_spawnPosition);
			}
			ushort num;
			if (this.CreateConnectionNode(out num, position))
			{
				if ((data.m_flags & Building.Flags.Active) == Building.Flags.None)
				{
					NetNode[] expr_103_cp_0 = instance.m_nodes.m_buffer;
					ushort expr_103_cp_1 = num;
					expr_103_cp_0[(int)expr_103_cp_1].m_flags = (expr_103_cp_0[(int)expr_103_cp_1].m_flags | NetNode.Flags.Disabled);
				}
				instance.UpdateNode(num);
				instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode = data.m_netNode;
				data.m_netNode = num;
			}
			ushort num2;
			if (this.CreateConnectionNode(out num2, target.m_position))
			{
				if ((data.m_flags & Building.Flags.Active) == Building.Flags.None)
				{
					NetNode[] expr_170_cp_0 = instance.m_nodes.m_buffer;
					ushort expr_170_cp_1 = num2;
					expr_170_cp_0[(int)expr_170_cp_1].m_flags = (expr_170_cp_0[(int)expr_170_cp_1].m_flags | NetNode.Flags.Disabled);
				}
				instance.UpdateNode(num2);
				instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode = data.m_netNode;
				data.m_netNode = num2;
			}
			if (num != 0 && num2 != 0)
			{
				ushort num3;
				if ((target.m_flags & Building.Flags.Incoming) != Building.Flags.None && this.CreateConnectionSegment(out num3, num, num2, gateIndex))
				{
					NetSegment[] expr_1E5_cp_0 = instance.m_segments.m_buffer;
					ushort expr_1E5_cp_1 = num3;
					expr_1E5_cp_0[(int)expr_1E5_cp_1].m_flags = (expr_1E5_cp_0[(int)expr_1E5_cp_1].m_flags | NetSegment.Flags.Untouchable);
					instance.UpdateSegment(num3);
				}
				ushort num4;
				if ((target.m_flags & Building.Flags.Outgoing) != Building.Flags.None && this.CreateConnectionSegment(out num4, num2, num, gateIndex))
				{
					NetSegment[] expr_230_cp_0 = instance.m_segments.m_buffer;
					ushort expr_230_cp_1 = num4;
					expr_230_cp_0[(int)expr_230_cp_1].m_flags = (expr_230_cp_0[(int)expr_230_cp_1].m_flags | NetSegment.Flags.Untouchable);
					instance.UpdateSegment(num4);
				}
			}
		}
	}

	private bool CreateConnectionNode(out ushort node, Vector3 position)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if (instance.CreateNode(out node, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo, position, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			NetNode[] expr_3F_cp_0 = instance.m_nodes.m_buffer;
			ushort expr_3F_cp_1 = node;
			expr_3F_cp_0[(int)expr_3F_cp_1].m_flags = (expr_3F_cp_0[(int)expr_3F_cp_1].m_flags | NetNode.Flags.Untouchable);
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
			return true;
		}
		node = 0;
		return false;
	}

	private bool CreateConnectionSegment(out ushort segment, ushort startNode, ushort endNode, int gateIndex)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = position2 - position;
		vector = VectorUtils.NormalizeXZ(vector);
		if (instance.CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo, startNode, endNode, vector, -vector, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, false))
		{
			instance.m_segments.m_buffer[(int)segment].m_trafficLightState0 = (byte)gateIndex;
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
			return true;
		}
		segment = 0;
		return false;
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		data.m_customBuffer1 = 0;
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		if (this.m_endOfLineStation)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<TransportManager>.get_instance().m_endOfLineStation.Activate(properties.m_endOfLineStation, buildingID);
			}
		}
		if (this.m_transportInfo != null && this.m_secondaryTransportInfo != null && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry && this.m_secondaryTransportInfo.m_transportType == TransportInfo.TransportType.Bus)
		{
			GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties2 != null)
			{
				Singleton<TransportManager>.get_instance().m_ferryBusHub.Activate(properties2.m_ferryBusHub, buildingID);
			}
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = 0;
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num2 = buildingData.m_netNode;
		int num3 = 0;
		while (num2 != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)num2].Info;
			if (info.m_class.m_layer == ItemClass.Layer.PublicTransport)
			{
				if (instance.m_nodes.m_buffer[(int)num2].m_transportLine == 0 && instance.m_nodes.m_buffer[(int)num2].m_maxWaitTime == 255)
				{
					for (int i = 0; i < 8; i++)
					{
						ushort segment = instance.m_nodes.m_buffer[(int)num2].GetSegment(i);
						if (segment != 0)
						{
							ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
							if (startNode == num2 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.PathFailed) == NetSegment.Flags.None)
							{
								ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
								float num4 = Vector3.SqrMagnitude(instance.m_nodes.m_buffer[(int)startNode].m_position - buildingData.m_position);
								float num5 = Vector3.SqrMagnitude(instance.m_nodes.m_buffer[(int)endNode].m_position - buildingData.m_position);
								if (num4 < num5)
								{
									this.CreateOutgoingVehicle(buildingID, ref buildingData, startNode, (int)instance.m_segments.m_buffer[(int)segment].m_trafficLightState0);
								}
								else if (num4 > num5)
								{
									this.CreateIncomingVehicle(buildingID, ref buildingData, startNode, (int)instance.m_segments.m_buffer[(int)segment].m_trafficLightState0);
								}
							}
						}
					}
				}
			}
			else
			{
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = instance.m_nodes.m_buffer[(int)num2].GetSegment(j);
					if (segment2 != 0)
					{
						ushort startNode2 = instance.m_segments.m_buffer[(int)segment2].m_startNode;
						if (startNode2 == num2)
						{
							TransportStationAI.CalculateLanePassengers(instance.m_segments.m_buffer[(int)segment2].m_lanes, ref num);
						}
					}
				}
			}
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		buildingData.m_customBuffer1 = (ushort)Mathf.Min(num, 65535);
		if (num != 0)
		{
			if (this.m_endOfLineStation)
			{
				Singleton<TransportManager>.get_instance().m_endOfLineStation.Disable();
			}
			if (this.m_transportInfo != null && this.m_secondaryTransportInfo != null && num != 0 && this.m_transportInfo.m_vehicleType == VehicleInfo.VehicleType.Ferry && this.m_secondaryTransportInfo.m_transportType == TransportInfo.TransportType.Bus)
			{
				Singleton<TransportManager>.get_instance().m_ferryBusHub.Disable();
			}
		}
	}

	private static void CalculateLanePassengers(uint firstLane, ref int passengerCount)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint num = firstLane;
		int num2 = 0;
		while (num != 0u)
		{
			ushort nodes = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nodes;
			if (nodes != 0)
			{
				TransportStationAI.CalculateLaneNodePassengers(nodes, ref passengerCount);
			}
			num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
			if (++num2 > 262144)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private static void CalculateLaneNodePassengers(ushort firstNode, ref int passengerCount)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = firstNode;
		int num2 = 0;
		while (num != 0)
		{
			passengerCount += (int)instance.m_nodes.m_buffer[(int)num].m_finalCounter;
			num = instance.m_nodes.m_buffer[(int)num].m_nextLaneNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private bool CreateOutgoingVehicle(ushort buildingID, ref Building buildingData, ushort startStop, int gateIndex)
	{
		if (this.m_transportLineInfo != null && this.FindConnectionVehicle(buildingID, ref buildingData, startStop, 3000f) == 0)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo.m_class.m_service, this.m_transportLineInfo.m_class.m_subService, this.m_transportLineInfo.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				Randomizer randomizer = default(Randomizer);
				randomizer.seed = (ulong)((long)gateIndex);
				Vector3 vector;
				Vector3 vector2;
				this.CalculateSpawnPosition(buildingID, ref buildingData, ref randomizer, randomVehicleInfo, out vector, out vector2);
				TransportInfo transportInfo = this.m_transportInfo;
				if (this.m_secondaryTransportInfo != null && this.m_secondaryTransportInfo.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
				{
					transportInfo = this.m_secondaryTransportInfo;
				}
				ushort num;
				if (randomVehicleInfo.m_vehicleAI.CanSpawnAt(vector) && Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, vector, transportInfo.m_vehicleReason, false, true))
				{
					vehicles.m_buffer[(int)num].m_gateIndex = (byte)gateIndex;
					Vehicle[] expr_12E_cp_0 = vehicles.m_buffer;
					ushort expr_12E_cp_1 = num;
					expr_12E_cp_0[(int)expr_12E_cp_1].m_flags = (expr_12E_cp_0[(int)expr_12E_cp_1].m_flags | (Vehicle.Flags.Importing | Vehicle.Flags.Exporting));
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.SetTarget(num, ref vehicles.m_buffer[(int)num], startStop);
					return true;
				}
			}
		}
		return false;
	}

	private bool CreateIncomingVehicle(ushort buildingID, ref Building buildingData, ushort startStop, int gateIndex)
	{
		if (this.m_transportLineInfo != null && this.FindConnectionVehicle(buildingID, ref buildingData, startStop, 3000f) == 0)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_transportLineInfo.m_class.m_service, this.m_transportLineInfo.m_class.m_subService, this.m_transportLineInfo.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				ushort num = this.FindConnectionBuilding(startStop);
				if (num != 0)
				{
					Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
					BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num].Info;
					Randomizer randomizer = default(Randomizer);
					randomizer.seed = (ulong)((long)gateIndex);
					Vector3 vector;
					Vector3 vector2;
					info.m_buildingAI.CalculateSpawnPosition(num, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num], ref randomizer, randomVehicleInfo, out vector, out vector2);
					TransportInfo transportInfo = this.m_transportInfo;
					if (this.m_secondaryTransportInfo != null && this.m_secondaryTransportInfo.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
					{
						transportInfo = this.m_secondaryTransportInfo;
					}
					ushort num2;
					if (randomVehicleInfo.m_vehicleAI.CanSpawnAt(vector) && Singleton<VehicleManager>.get_instance().CreateVehicle(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, vector, transportInfo.m_vehicleReason, true, false))
					{
						vehicles.m_buffer[(int)num2].m_gateIndex = (byte)gateIndex;
						Vehicle[] expr_172_cp_0 = vehicles.m_buffer;
						ushort expr_172_cp_1 = num2;
						expr_172_cp_0[(int)expr_172_cp_1].m_flags = (expr_172_cp_0[(int)expr_172_cp_1].m_flags | (Vehicle.Flags.Importing | Vehicle.Flags.Exporting));
						randomVehicleInfo.m_vehicleAI.SetSource(num2, ref vehicles.m_buffer[(int)num2], num);
						randomVehicleInfo.m_vehicleAI.SetSource(num2, ref vehicles.m_buffer[(int)num2], buildingID);
						randomVehicleInfo.m_vehicleAI.SetTarget(num2, ref vehicles.m_buffer[(int)num2], startStop);
						return true;
					}
				}
			}
		}
		return false;
	}

	private ushort FindConnectionBuilding(ushort stop)
	{
		if (this.m_transportLineInfo != null)
		{
			Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)stop].m_position;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> outsideConnections = instance.GetOutsideConnections();
			ushort result = 0;
			float num = 40000f;
			for (int i = 0; i < outsideConnections.m_size; i++)
			{
				ushort num2 = outsideConnections.m_buffer[i];
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num2].Info;
				if (info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService)
				{
					float num3 = VectorUtils.LengthSqrXZ(instance.m_buildings.m_buffer[(int)num2].m_position - position);
					if (num3 < num)
					{
						result = num2;
						num = num3;
					}
				}
			}
			return result;
		}
		return 0;
	}

	private ushort FindConnectionVehicle(ushort buildingID, ref Building buildingData, ushort targetStop, float maxDistance)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)targetStop].m_position;
		ushort num = buildingData.m_ownVehicles;
		int num2 = 0;
		while (num != 0)
		{
			if (instance.m_vehicles.m_buffer[(int)num].m_transportLine == 0)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				if (info.m_class.m_service == this.m_transportLineInfo.m_class.m_service && info.m_class.m_subService == this.m_transportLineInfo.m_class.m_subService && instance.m_vehicles.m_buffer[(int)num].m_targetBuilding == targetStop && Vector3.SqrMagnitude(instance.m_vehicles.m_buffer[(int)num].GetLastFramePosition() - position) < maxDistance * maxDistance)
				{
					return num;
				}
			}
			num = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public override void SetEmptying(ushort buildingID, ref Building data, bool emptying)
	{
		if ((data.m_flags & Building.Flags.Downgrading) != Building.Flags.None != emptying)
		{
			if (emptying)
			{
				data.m_flags |= Building.Flags.Downgrading;
				this.RemoveConnectionLines(buildingID, ref data);
			}
			else
			{
				data.m_flags &= ~Building.Flags.Downgrading;
				this.CreateConnectionLines(buildingID, ref data);
			}
		}
	}

	public override TransportInfo GetTransportLineInfo()
	{
		return this.m_transportInfo;
	}

	public override TransportInfo GetSecondaryTransportLineInfo()
	{
		return this.m_secondaryTransportInfo;
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string str = string.Empty;
		int passengerCount = this.GetPassengerCount(buildingID, ref data);
		str = str + LocaleFormatter.FormatGeneric("AIINFO_PASSENGERS_SERVICED", new object[]
		{
			passengerCount
		}) + Environment.NewLine;
		return str + base.GetLocalizedStats(buildingID, ref data);
	}

	public NetInfo m_transportLineInfo;

	public bool m_endOfLineStation;
}
