﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class WaterTruckAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Water)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_WATER_PUMP");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_WATER_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_WATER_WAIT");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_WATER_SEARCH");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "WaterTruck";
		current = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0 && data.m_transferSize != 0)
		{
			int transferSize = (int)data.m_transferSize;
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			if (info != null)
			{
				info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
				data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
			}
		}
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0 && (data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Building[] expr_33_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
				ushort expr_33_cp_1 = data.m_targetBuilding;
				expr_33_cp_0[(int)expr_33_cp_1].m_flags = (expr_33_cp_0[(int)expr_33_cp_1].m_flags | Building.Flags.RoadAccessFailed);
			}
			else if (this.CheckTargetSegment(vehicleID, ref data))
			{
				NetSegment[] expr_70_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
				ushort expr_70_cp_1 = data.m_targetBuilding;
				expr_70_cp_0[(int)expr_70_cp_1].m_flags = (expr_70_cp_0[(int)expr_70_cp_1].m_flags | NetSegment.Flags.AccessFailed);
			}
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(vehicleID);
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			data.m_frame0 = new Vehicle.Frame(vector, rotation);
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = vector;
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = vector2;
			data.m_targetPos1.w = 2f;
			data.m_targetPos2 = data.m_targetPos1;
			data.m_targetPos3 = data.m_targetPos1;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			instance.m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding == data.m_targetBuilding)
		{
			if (data.m_path == 0u)
			{
				if (!this.StartPathFind(vehicleID, ref data))
				{
					data.Unspawn(vehicleID);
				}
			}
			else
			{
				this.TrySpawn(vehicleID, ref data);
			}
		}
		else
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if (targetBuilding != 0)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
				}
				else if (!this.CheckTargetSegment(vehicleID, ref data))
				{
					targetBuilding = 0;
					data.m_targetBuilding = 0;
				}
			}
			if (targetBuilding == 0)
			{
				if ((int)data.m_transferSize < this.m_cargoCapacity && !this.ShouldReturnToSource(vehicleID, ref data))
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = 7;
					offer.Vehicle = vehicleID;
					if (data.m_sourceBuilding != 0)
					{
						offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
					}
					else
					{
						offer.Position = data.GetLastFramePosition();
					}
					offer.Amount = 1;
					offer.Active = true;
					Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
					data.m_flags |= Vehicle.Flags.WaitingTarget;
					data.m_waitCounter = 0;
				}
				else
				{
					data.m_flags |= Vehicle.Flags.GoingBack;
				}
			}
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0 && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
			{
				if (offer.Building != 0)
				{
					if ((data.m_flags & Vehicle.Flags.TransferToTarget) == (Vehicle.Flags)0)
					{
						this.RemoveTarget(vehicleID, ref data);
						data.m_flags |= Vehicle.Flags.TransferToTarget;
					}
					this.SetTarget(vehicleID, ref data, offer.Building);
				}
				else if (offer.NetSegment != 0)
				{
					if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
					{
						this.RemoveTarget(vehicleID, ref data);
						data.m_flags &= ~Vehicle.Flags.TransferToTarget;
					}
					this.SetTarget(vehicleID, ref data, offer.NetSegment);
				}
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			}
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			return true;
		}
		this.SetTarget(vehicleID, ref data, 0);
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		int transferSize = (int)data.m_transferSize;
		BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
		info.m_buildingAI.ModifyMaterialBuffer(data.m_sourceBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding], (TransferManager.TransferReason)data.m_transferType, ref transferSize);
		data.m_transferSize = (ushort)Mathf.Clamp((int)data.m_transferSize - transferSize, 0, (int)data.m_transferSize);
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		bool flag = false;
		if ((int)vehicleData.m_transferSize < this.m_cargoCapacity)
		{
			flag = this.TryCollectWater(vehicleID, ref vehicleData);
		}
		else if (vehicleData.m_waterSource != 0)
		{
			Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(vehicleData.m_waterSource);
			vehicleData.m_waterSource = 0;
		}
		if ((int)vehicleData.m_transferSize >= this.m_cargoCapacity && (vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0)
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
		else if ((vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0 && (vehicleData.m_flags & Vehicle.Flags.TransferToTarget) == (Vehicle.Flags)0 && !this.CheckTargetSegment(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
		if (flag)
		{
			vehicleData.m_flags |= (Vehicle.Flags.Emergency1 | Vehicle.Flags.Stopped);
			if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) == (Vehicle.Flags)0)
			{
				vehicleData.m_waitCounter = 0;
			}
		}
		else if (vehicleData.m_waitCounter < 4)
		{
			if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) == (Vehicle.Flags)0)
			{
				vehicleData.m_waitCounter += 1;
			}
		}
		else
		{
			vehicleData.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Stopped);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) == (Vehicle.Flags)0 && (vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0 && vehicleData.m_targetBuilding != 0 && (vehicleData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) == (Vehicle.Flags)0)
		{
			this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			this.SetTarget(vehicleID, ref vehicleData, 0);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_productionRate == 0 || (instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & (Building.Flags.Evacuating | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None) && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (leaderData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 targetPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
				vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
				return;
			}
		}
		else if ((leaderData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0 && leaderData.m_targetBuilding != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding].Info;
			Randomizer randomizer2;
			randomizer2..ctor((int)vehicleID);
			Vector3 vector2;
			Vector3 targetPos2;
			info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)leaderData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out targetPos2);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos2, minSqrDistance, 2f));
			return;
		}
	}

	private bool TryCollectWater(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) == Vehicle.Flags.Underground)
		{
			if (vehicleData.m_waterSource != 0)
			{
				Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(vehicleData.m_waterSource);
				vehicleData.m_waterSource = 0;
			}
			return false;
		}
		int num = this.m_cargoCapacity - (int)vehicleData.m_transferSize;
		num = Mathf.Min(num, this.m_pumpingRate);
		num = this.HandleWaterSource(vehicleID, ref vehicleData, vehicleData.m_segment.a, 50f, num);
		if (num > 0)
		{
			vehicleData.m_transferSize += (ushort)num;
			return true;
		}
		return false;
	}

	private int HandleWaterSource(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, float radius, int amount)
	{
		if (amount <= 0)
		{
			return 0;
		}
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		if (vehicleData.m_waterSource != 0)
		{
			bool flag = false;
			WaterSource sourceData = waterSimulation.LockWaterSource(vehicleData.m_waterSource);
			try
			{
				sourceData.m_inputRate = (uint)((uint)amount << 2);
				if ((long)amount > (long)((ulong)sourceData.m_water))
				{
					amount = (int)sourceData.m_water;
				}
				if (sourceData.m_water != 0u)
				{
					sourceData.m_pollution = (uint)((ulong)sourceData.m_pollution * ((ulong)sourceData.m_water - (ulong)((long)amount)) / (ulong)sourceData.m_water);
				}
				sourceData.m_water -= (uint)amount;
				sourceData.m_target = (ushort)Mathf.Clamp(Mathf.RoundToInt((refPos.y - 1f) * 64f), 0, 65535);
				Vector3 vector = sourceData.m_inputPosition;
				if (!instance.HasWater(VectorUtils.XZ(vector)))
				{
					vector = refPos;
					vector.y -= 1f;
					if (instance.GetClosestWaterPos(ref vector, radius))
					{
						sourceData.m_inputPosition = vector;
						sourceData.m_outputPosition = vector;
					}
					else
					{
						flag = true;
					}
				}
			}
			finally
			{
				waterSimulation.UnlockWaterSource(vehicleData.m_waterSource, sourceData);
			}
			if (flag)
			{
				waterSimulation.ReleaseWaterSource(vehicleData.m_waterSource);
				vehicleData.m_waterSource = 0;
			}
		}
		else
		{
			Vector3 vector2 = refPos;
			vector2.y -= 1f;
			if (instance.GetClosestWaterPos(ref vector2, radius))
			{
				waterSimulation.CreateWaterSource(out vehicleData.m_waterSource, new WaterSource
				{
					m_type = 2,
					m_inputPosition = vector2,
					m_outputPosition = vector2,
					m_inputRate = (uint)((uint)amount << 2),
					m_target = (ushort)Mathf.Clamp(Mathf.RoundToInt((refPos.y - 1f) * 64f), 0, 65535)
				});
			}
			amount = 0;
		}
		return amount;
	}

	private bool CheckTargetSegment(ushort vehicleID, ref Vehicle vehicleData)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		return vehicleData.m_targetBuilding < 36864 && (instance.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				Vector3 vector;
				Vector3 endPos;
				info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out endPos);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_targetBuilding != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
				Randomizer randomizer2;
				randomizer2..ctor((int)vehicleID);
				Vector3 vector2;
				Vector3 endPos2;
				info2.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out vector2, out endPos2);
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
			}
		}
		else if (vehicleData.m_targetBuilding != 0 && this.CheckTargetSegment(vehicleID, ref vehicleData))
		{
			NetManager instance3 = Singleton<NetManager>.get_instance();
			NetInfo info3 = instance3.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].Info;
			if (info3.m_lanes == null)
			{
				return false;
			}
			int num = 0;
			for (int i = 0; i < info3.m_lanes.Length; i++)
			{
				if (info3.m_lanes[i].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car))
				{
					num++;
				}
			}
			if (num == 0)
			{
				return false;
			}
			Vector3 endPos3 = Vector3.get_zero();
			num = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num);
			uint num2 = instance3.m_segments.m_buffer[(int)vehicleData.m_targetBuilding].m_lanes;
			int num3 = 0;
			while (num3 < info3.m_lanes.Length && num2 != 0u)
			{
				if (info3.m_lanes[num3].CheckType(NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car) && num-- == 0)
				{
					endPos3 = instance3.m_lanes.m_buffer[(int)((UIntPtr)num2)].CalculatePosition(0.5f);
					break;
				}
				num2 = instance3.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
				num3++;
			}
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos3, true, true, true);
		}
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, undergroundTarget, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, default(PathUnit.Position), NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, this.IsHeavyVehicle(), this.IgnoreBlocked(vehicleID, ref vehicleData), false, false, false, (int)vehicleData.m_transferSize < this.m_cargoCapacity, this.CombustionEngine()))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			this.PathfindFailure(vehicleID, ref vehicleData);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else if ((vehicleData.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		else
		{
			result.NetSegment = vehicleData.m_targetBuilding;
		}
		return result;
	}

	public override int GetNoiseLevel()
	{
		return 9;
	}

	[CustomizableProperty("Cargo capacity")]
	public int m_cargoCapacity = 1;

	[CustomizableProperty("Pumping rate")]
	public int m_pumpingRate = 1;
}
