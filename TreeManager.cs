﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.Threading;
using UnityEngine;

public class TreeManager : SimulationManagerBase<TreeManager, TreeProperties>, ISimulationManager, IRenderableManager, IAudibleManager, ITerrainManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_trees = new Array32<TreeInstance>(262144u);
		this.m_updatedTrees = new ulong[4096];
		this.m_treeGrid = new uint[291600];
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_XYCAMap = Shader.PropertyToID("_XYCAMap");
		this.ID_ShadowAMap = Shader.PropertyToID("_ShadowAMap");
		this.ID_ShadowMatrix = Shader.PropertyToID("_ShadowMatrix");
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_TileRect = Shader.PropertyToID("_TileRect");
		this.ID_TreeLocation = Shader.PropertyToID("_TreeLocation");
		this.ID_TreeColor = Shader.PropertyToID("_TreeColor");
		this.m_treeLayer = LayerMask.NameToLayer("Trees");
		this.m_burningTrees = new FastList<TreeManager.BurningTree>();
		this.m_renderCamera = base.get_gameObject().AddComponent<Camera>();
		this.m_renderCamera.set_eventMask(0);
		this.m_renderCamera.set_enabled(false);
		this.m_renderCamera.set_targetTexture(null);
		this.m_renderCamera.set_backgroundColor(new Color(0f, 0f, 0f, 0f));
		this.m_renderCamera.set_clearFlags(2);
		this.m_renderCamera.ResetAspect();
		this.m_renderCamera.set_orthographic(true);
		this.m_renderCamera.set_orthographicSize(0.5f);
		this.m_renderCamera.set_nearClipPlane(1f);
		this.m_renderCamera.set_farClipPlane(100f);
		this.m_renderCamera.set_cullingMask(0);
		this.m_renderCamera.set_renderingPath(0);
		this.m_renderCamera.set_useOcclusionCulling(false);
		this.m_renderDiffuseTexture = new RenderTexture(512, 512, 24, 0, 1);
		this.m_renderDiffuseTexture.set_filterMode(2);
		this.m_renderDiffuseTexture.set_autoGenerateMips(true);
		this.m_renderXycaTexture = new RenderTexture(512, 512, 24, 0, 1);
		this.m_renderXycaTexture.set_filterMode(2);
		this.m_renderXycaTexture.set_autoGenerateMips(true);
		this.m_renderShadowTexture = new RenderTexture(512, 512, 24, 0, 1);
		this.m_renderShadowTexture.set_filterMode(0);
		this.m_renderShadowTexture.set_autoGenerateMips(false);
		uint num;
		this.m_trees.CreateItem(out num);
	}

	public override void InitializeProperties(TreeProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_properties.m_placementEffect != null)
		{
			this.m_properties.m_placementEffect.InitializeEffect();
		}
		if (this.m_properties.m_bulldozeEffect != null)
		{
			this.m_properties.m_bulldozeEffect.InitializeEffect();
		}
		if (this.m_properties.m_fireEffect != null)
		{
			this.m_properties.m_fireEffect.InitializeEffect();
		}
	}

	public override void DestroyProperties(TreeProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_properties.m_placementEffect != null)
			{
				this.m_properties.m_placementEffect.ReleaseEffect();
			}
			if (this.m_properties.m_bulldozeEffect != null)
			{
				this.m_properties.m_bulldozeEffect.ReleaseEffect();
			}
			if (this.m_properties.m_fireEffect != null)
			{
				this.m_properties.m_fireEffect.ReleaseEffect();
			}
		}
		base.DestroyProperties(properties);
	}

	private void OnDestroy()
	{
		if (this.m_renderDiffuseTexture != null)
		{
			Object.Destroy(this.m_renderDiffuseTexture);
			this.m_renderDiffuseTexture = null;
		}
		if (this.m_renderXycaTexture != null)
		{
			Object.Destroy(this.m_renderXycaTexture);
			this.m_renderXycaTexture = null;
		}
		if (this.m_renderShadowTexture != null)
		{
			Object.Destroy(this.m_renderShadowTexture);
			this.m_renderShadowTexture = null;
		}
	}

	public override void CheckReferences()
	{
		base.CheckReferences();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.CheckReferencesImpl());
	}

	[DebuggerHidden]
	private IEnumerator CheckReferencesImpl()
	{
		return new TreeManager.<CheckReferencesImpl>c__Iterator0();
	}

	private void OnPostRender()
	{
		if (this.m_cameraInfo == null)
		{
			return;
		}
		int num = PrefabCollection<TreeInfo>.PrefabCount();
		int num2 = 1;
		while (num2 * num2 < num)
		{
			num2++;
		}
		Quaternion quaternion = Quaternion.Inverse((this.m_renderPass != 3) ? this.m_cameraInfo.m_rotation : this.m_cameraInfo.m_shadowRotation);
		float num3;
		if (this.m_renderCamera.get_orthographic())
		{
			num3 = 0.1f;
		}
		else
		{
			num3 = Mathf.Tan(this.m_renderCamera.get_fieldOfView() * 0.0174532924f * 0.5f);
		}
		for (int i = 0; i < num; i++)
		{
			TreeInfo prefab = PrefabCollection<TreeInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				if (prefab.m_renderMaterial == null)
				{
					prefab.m_renderMaterial = new Material(this.m_properties.m_renderShader);
					prefab.m_renderMaterial.CopyPropertiesFromMaterial(prefab.m_material);
				}
				Vector3 vector;
				vector..ctor((0.5f - (float)num2 * 0.5f + (float)(i % num2)) / (float)num2, (0.5f - (float)num2 * 0.5f + (float)(i / num2)) / (float)num2, 0.5f / num3);
				Vector3 vector2 = vector;
				vector2 -= quaternion * new Vector3(0f, prefab.m_renderOffset / (prefab.m_renderScale * (float)num2), 0f);
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(vector2, quaternion, Vector3.get_one() / (prefab.m_renderScale * (float)num2));
				Vector4 vector3;
				vector3..ctor(vector.x - 0.5f / (float)num2, vector.y - 0.5f / (float)num2, vector.x + 0.5f / (float)num2, vector.y + 0.5f / (float)num2);
				if (this.m_renderPass == 0)
				{
					Quaternion quaternion2 = Quaternion.Inverse(this.m_cameraInfo.m_shadowRotation);
					Vector3 vector4;
					vector4..ctor((0.5f + (float)(i % num2)) / (float)num2, (0.5f + (float)(i / num2)) / (float)num2, 0.5f / num3);
					vector4 -= quaternion2 * new Vector3(0f, prefab.m_renderOffset / (prefab.m_renderScale * (float)num2), 0f);
					Matrix4x4 matrix4x2 = default(Matrix4x4);
					matrix4x2.SetTRS(vector4, quaternion2, Vector3.get_one() / (prefab.m_renderScale * (float)num2));
					prefab.m_renderMaterial.SetTexture(this.ID_ShadowAMap, this.m_renderShadowTexture);
					prefab.m_renderMaterial.SetMatrix(this.ID_ShadowMatrix, matrix4x2);
				}
				prefab.m_renderMaterial.SetVector(this.ID_TileRect, vector3);
				if (prefab.m_renderMaterial.SetPass(this.m_renderPass))
				{
					TreeManager expr_2C2_cp_0 = Singleton<TreeManager>.get_instance();
					expr_2C2_cp_0.m_drawCallData.m_defaultCalls = expr_2C2_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMeshNow(prefab.m_mesh, matrix4x);
				}
				if ((this.m_renderPass == 1 || this.m_renderPass == 3) && prefab.m_renderMaterial.SetPass(2))
				{
					TreeManager expr_311_cp_0 = Singleton<TreeManager>.get_instance();
					expr_311_cp_0.m_drawCallData.m_defaultCalls = expr_311_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMeshNow(prefab.m_mesh, matrix4x);
				}
			}
		}
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		this.m_cameraInfo = cameraInfo;
		try
		{
			bool flag = Quaternion.Angle(this.m_lastShadowRotation, this.m_cameraInfo.m_shadowRotation) > 0.1f || !this.m_renderShadowTexture.IsCreated();
			bool flag2 = Quaternion.Angle(this.m_lastCameraRotation, this.m_cameraInfo.m_rotation) > 0.1f || !this.m_renderDiffuseTexture.IsCreated();
			bool flag3 = Quaternion.Angle(this.m_lastCameraRotation, this.m_cameraInfo.m_rotation) > 0.1f || !this.m_renderXycaTexture.IsCreated();
			if (flag)
			{
				this.m_renderCamera.set_targetTexture(this.m_renderShadowTexture);
				this.m_renderPass = 3;
				this.m_renderCamera.Render();
				this.m_lastShadowRotation = cameraInfo.m_shadowRotation;
			}
			if (flag3)
			{
				this.m_renderCamera.set_targetTexture(this.m_renderXycaTexture);
				this.m_renderPass = 1;
				this.m_renderCamera.Render();
				this.m_lastCameraRotation = this.m_cameraInfo.m_rotation;
			}
			if (flag || flag2)
			{
				this.m_renderCamera.set_targetTexture(this.m_renderDiffuseTexture);
				this.m_renderPass = 0;
				this.m_renderCamera.Render();
				this.m_lastCameraRotation = this.m_cameraInfo.m_rotation;
			}
			this.m_renderCamera.set_targetTexture(null);
			Singleton<RenderManager>.get_instance().m_groupLayerMaterials[this.m_treeLayer].set_mainTexture(this.m_renderDiffuseTexture);
			Singleton<RenderManager>.get_instance().m_groupLayerMaterials[this.m_treeLayer].SetTexture(this.ID_XYCAMap, this.m_renderXycaTexture);
			Singleton<RenderManager>.get_instance().m_groupLayerMaterials[this.m_treeLayer].SetTexture(this.ID_ShadowAMap, this.m_renderShadowTexture);
		}
		finally
		{
			this.m_cameraInfo = null;
		}
		int num = PrefabCollection<TreeInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			TreeInfo prefab = PrefabCollection<TreeInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				if (prefab.m_lodMeshData1 != null)
				{
					RenderGroup.MeshData lodMeshData = prefab.m_lodMeshData1;
					prefab.m_lodMeshData1 = null;
					if (prefab.m_lodMesh1 == null)
					{
						prefab.m_lodMesh1 = new Mesh();
					}
					lodMeshData.PopulateMesh(prefab.m_lodMesh1);
				}
				if (prefab.m_lodMeshData4 != null)
				{
					RenderGroup.MeshData lodMeshData2 = prefab.m_lodMeshData4;
					prefab.m_lodMeshData4 = null;
					if (prefab.m_lodMesh4 == null)
					{
						prefab.m_lodMesh4 = new Mesh();
					}
					lodMeshData2.PopulateMesh(prefab.m_lodMesh4);
				}
				if (prefab.m_lodMeshData8 != null)
				{
					RenderGroup.MeshData lodMeshData3 = prefab.m_lodMeshData8;
					prefab.m_lodMeshData8 = null;
					if (prefab.m_lodMesh8 == null)
					{
						prefab.m_lodMesh8 = new Mesh();
					}
					lodMeshData3.PopulateMesh(prefab.m_lodMesh8);
				}
				if (prefab.m_lodMeshData16 != null)
				{
					RenderGroup.MeshData lodMeshData4 = prefab.m_lodMeshData16;
					prefab.m_lodMeshData16 = null;
					if (prefab.m_lodMesh16 == null)
					{
						prefab.m_lodMesh16 = new Mesh();
					}
					lodMeshData4.PopulateMesh(prefab.m_lodMesh16);
				}
				if (prefab.m_lodMaterial == null)
				{
					Shader shader = Singleton<RenderManager>.get_instance().m_properties.m_groupLayerShaders[this.m_treeLayer];
					prefab.m_lodMaterial = new Material(shader);
					prefab.m_lodMaterial.EnableKeyword("MULTI_INSTANCE");
				}
				prefab.m_lodMaterial.set_mainTexture(this.m_renderDiffuseTexture);
				prefab.m_lodMaterial.SetTexture(this.ID_XYCAMap, this.m_renderXycaTexture);
				prefab.m_lodMaterial.SetTexture(this.ID_ShadowAMap, this.m_renderShadowTexture);
			}
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		FastList<RenderGroup> renderedGroups = Singleton<RenderManager>.get_instance().m_renderedGroups;
		for (int i = 0; i < renderedGroups.m_size; i++)
		{
			RenderGroup renderGroup = renderedGroups.m_buffer[i];
			if ((renderGroup.m_instanceMask & 1 << this.m_treeLayer) != 0)
			{
				int num = renderGroup.m_x * 540 / 45;
				int num2 = renderGroup.m_z * 540 / 45;
				int num3 = (renderGroup.m_x + 1) * 540 / 45 - 1;
				int num4 = (renderGroup.m_z + 1) * 540 / 45 - 1;
				for (int j = num2; j <= num4; j++)
				{
					for (int k = num; k <= num3; k++)
					{
						int num5 = j * 540 + k;
						uint num6 = this.m_treeGrid[num5];
						int num7 = 0;
						while (num6 != 0u)
						{
							this.m_trees.m_buffer[(int)((UIntPtr)num6)].RenderInstance(cameraInfo, num6, renderGroup.m_instanceMask);
							num6 = this.m_trees.m_buffer[(int)((UIntPtr)num6)].m_nextGridTree;
							if (++num7 >= 262144)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
		}
		int num8 = PrefabCollection<TreeInfo>.PrefabCount();
		for (int l = 0; l < num8; l++)
		{
			TreeInfo prefab = PrefabCollection<TreeInfo>.GetPrefab((uint)l);
			if (prefab != null)
			{
				if (prefab.m_lodCount != 0)
				{
					TreeInstance.RenderLod(cameraInfo, prefab);
				}
			}
		}
		if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
		{
			int size = this.m_burningTrees.m_size;
			for (int m = 0; m < size; m++)
			{
				TreeManager.BurningTree burningTree = this.m_burningTrees.m_buffer[m];
				if (burningTree.m_treeIndex != 0u)
				{
					float fireIntensity = (float)burningTree.m_fireIntensity * 0.003921569f;
					float fireDamage = (float)burningTree.m_fireDamage * 0.003921569f;
					this.RenderFireEffect(cameraInfo, burningTree.m_treeIndex, ref this.m_trees.m_buffer[(int)((UIntPtr)burningTree.m_treeIndex)], fireIntensity, fireDamage);
				}
			}
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		if (this.m_properties != null && !Singleton<LoadingManager>.get_instance().m_currentlyLoading && Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
		{
			int size = this.m_burningTrees.m_size;
			for (int i = 0; i < size; i++)
			{
				TreeManager.BurningTree burningTree = this.m_burningTrees.m_buffer[i];
				if (burningTree.m_treeIndex != 0u)
				{
					float fireIntensity = (float)burningTree.m_fireIntensity * 0.003921569f;
					float fireDamage = (float)burningTree.m_fireDamage * 0.003921569f;
					this.PlayFireEffect(listenerInfo, burningTree.m_treeIndex, ref this.m_trees.m_buffer[(int)((UIntPtr)burningTree.m_treeIndex)], fireIntensity, fireDamage);
				}
			}
		}
	}

	public void RenderFireEffect(RenderManager.CameraInfo cameraInfo, uint treeID, ref TreeInstance data, float fireIntensity, float fireDamage)
	{
		if (data.GrowState == 0)
		{
			return;
		}
		float num = (0.5f - Mathf.Abs(fireDamage - 0.5f)) * fireIntensity;
		if (num < 0.01f)
		{
			return;
		}
		TreeInfo info = data.Info;
		Vector3 position = data.Position;
		if (!cameraInfo.Intersect(position, info.m_generatedInfo.m_size.y))
		{
			return;
		}
		Randomizer randomizer;
		randomizer..ctor(treeID);
		float num2 = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
		float timeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta * num;
		InstanceID id = default(InstanceID);
		id.Tree = treeID;
		EffectInfo fireEffect = Singleton<TreeManager>.get_instance().m_properties.m_fireEffect;
		float radius = (info.m_generatedInfo.m_size.x + info.m_generatedInfo.m_size.z) * num2 * 0.05f;
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position, Vector3.get_up(), radius, info.m_generatedInfo.m_size.y * num2);
		fireEffect.RenderEffect(id, area, Vector3.get_zero(), 0f, 1f, -1f, timeDelta, cameraInfo);
	}

	public void PlayFireEffect(AudioManager.ListenerInfo listenerInfo, uint treeID, ref TreeInstance data, float fireIntensity, float fireDamage)
	{
		if (data.GrowState == 0)
		{
			return;
		}
		float num = (0.5f - Mathf.Abs(fireDamage - 0.5f)) * fireIntensity;
		if (num < 0.01f)
		{
			return;
		}
		Vector3 position = data.Position;
		if (Vector3.SqrMagnitude(position - listenerInfo.m_position) >= 10000f)
		{
			return;
		}
		InstanceID id = default(InstanceID);
		id.Tree = treeID;
		EffectInfo fireEffect = Singleton<TreeManager>.get_instance().m_properties.m_fireEffect;
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position, Vector3.get_up(), 1f);
		fireEffect.PlayEffect(id, area, Vector3.get_zero(), 0f, num, listenerInfo, Singleton<BuildingManager>.get_instance().m_audioGroup);
	}

	public float SampleSmoothHeight(Vector3 worldPos)
	{
		float num = 0f;
		int num2 = Mathf.Max((int)((worldPos.x - 32f) / 32f + 270f), 0);
		int num3 = Mathf.Max((int)((worldPos.z - 32f) / 32f + 270f), 0);
		int num4 = Mathf.Min((int)((worldPos.x + 32f) / 32f + 270f), 539);
		int num5 = Mathf.Min((int)((worldPos.z + 32f) / 32f + 270f), 539);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				uint num6 = this.m_treeGrid[i * 540 + j];
				int num7 = 0;
				while (num6 != 0u)
				{
					if (this.m_trees.m_buffer[(int)((UIntPtr)num6)].GrowState != 0)
					{
						Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num6)].Position;
						Vector3 vector = worldPos - position;
						float num8 = 1024f;
						float num9 = vector.x * vector.x + vector.z * vector.z;
						if (num9 < num8)
						{
							TreeInfo info = this.m_trees.m_buffer[(int)((UIntPtr)num6)].Info;
							float num10 = MathUtils.SmoothClamp01(1f - Mathf.Sqrt(num9 / num8));
							num10 = Mathf.Lerp(worldPos.y, position.y + info.m_generatedInfo.m_size.y * 1.25f, num10);
							if (num10 > num)
							{
								num = num10;
							}
						}
					}
					num6 = this.m_trees.m_buffer[(int)((UIntPtr)num6)].m_nextGridTree;
					if (++num7 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num;
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (this.m_treeCount >= 250000)
			{
				return false;
			}
		}
		else if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_treeCount + Singleton<PropManager>.get_instance().m_propCount >= 64)
			{
				return false;
			}
		}
		else if (this.m_treeCount >= 262139)
		{
			return false;
		}
		return true;
	}

	public bool CreateTree(out uint tree, ref Randomizer randomizer, TreeInfo info, Vector3 position, bool single)
	{
		if (!this.CheckLimits())
		{
			tree = 0u;
			return false;
		}
		uint num;
		if (this.m_trees.CreateItem(out num, ref randomizer))
		{
			tree = num;
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_flags = 1;
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].Info = info;
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].Single = single;
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].GrowState = 15;
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].Position = position;
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			this.InitializeTree(tree, ref this.m_trees.m_buffer[(int)((UIntPtr)tree)], (mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None);
			this.UpdateTree(tree);
			this.m_treeCount = (int)(this.m_trees.ItemCount() - 1u);
			return true;
		}
		tree = 0u;
		return false;
	}

	public void ReleaseTree(uint tree)
	{
		this.ReleaseTreeImplementation(tree, ref this.m_trees.m_buffer[(int)((UIntPtr)tree)]);
	}

	private void InitializeTree(uint tree, ref TreeInstance data, bool assetEditor)
	{
		int num;
		int num2;
		if (assetEditor)
		{
			num = Mathf.Clamp(((int)(data.m_posX / 16) + 32768) * 540 / 65536, 0, 539);
			num2 = Mathf.Clamp(((int)(data.m_posZ / 16) + 32768) * 540 / 65536, 0, 539);
		}
		else
		{
			num = Mathf.Clamp(((int)data.m_posX + 32768) * 540 / 65536, 0, 539);
			num2 = Mathf.Clamp(((int)data.m_posZ + 32768) * 540 / 65536, 0, 539);
		}
		int num3 = num2 * 540 + num;
		while (!Monitor.TryEnter(this.m_treeGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_nextGridTree = this.m_treeGrid[num3];
			this.m_treeGrid[num3] = tree;
		}
		finally
		{
			Monitor.Exit(this.m_treeGrid);
		}
	}

	public bool BurnTree(uint treeIndex, InstanceManager.Group group, int fireIntensity)
	{
		if (treeIndex == 0u || (this.m_trees.m_buffer[(int)((UIntPtr)treeIndex)].m_flags & 64) != 0)
		{
			return false;
		}
		if (!Singleton<LoadingManager>.get_instance().SupportsExpansion(2))
		{
			return false;
		}
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_66_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_66_cp_1 = disaster;
				expr_66_cp_0[(int)expr_66_cp_1].m_treeFireCount = expr_66_cp_0[(int)expr_66_cp_1].m_treeFireCount + 1u;
			}
		}
		TreeManager.BurningTree item;
		item.m_treeIndex = treeIndex;
		item.m_fireIntensity = (byte)fireIntensity;
		item.m_fireDamage = 4;
		InstanceID id = default(InstanceID);
		id.Tree = treeIndex;
		Singleton<InstanceManager>.get_instance().SetGroup(id, group);
		TreeInstance[] expr_BA_cp_0 = this.m_trees.m_buffer;
		UIntPtr expr_BA_cp_1 = (UIntPtr)treeIndex;
		expr_BA_cp_0[(int)expr_BA_cp_1].m_flags = (expr_BA_cp_0[(int)expr_BA_cp_1].m_flags | 192);
		this.m_burningTrees.Add(item);
		return true;
	}

	private void FinalizeTree(uint tree, ref TreeInstance data)
	{
		int num;
		int num2;
		if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
		{
			num = Mathf.Clamp(((int)(data.m_posX / 16) + 32768) * 540 / 65536, 0, 539);
			num2 = Mathf.Clamp(((int)(data.m_posZ / 16) + 32768) * 540 / 65536, 0, 539);
		}
		else
		{
			num = Mathf.Clamp(((int)data.m_posX + 32768) * 540 / 65536, 0, 539);
			num2 = Mathf.Clamp(((int)data.m_posZ + 32768) * 540 / 65536, 0, 539);
		}
		int num3 = num2 * 540 + num;
		while (!Monitor.TryEnter(this.m_treeGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			uint num4 = 0u;
			uint num5 = this.m_treeGrid[num3];
			int num6 = 0;
			while (num5 != 0u)
			{
				if (num5 == tree)
				{
					if (num4 == 0u)
					{
						this.m_treeGrid[num3] = data.m_nextGridTree;
					}
					else
					{
						this.m_trees.m_buffer[(int)((UIntPtr)num4)].m_nextGridTree = data.m_nextGridTree;
					}
					break;
				}
				num4 = num5;
				num5 = this.m_trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
				if (++num6 > 262144)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridTree = 0u;
		}
		finally
		{
			Monitor.Exit(this.m_treeGrid);
		}
		Singleton<RenderManager>.get_instance().UpdateGroup(num * 45 / 540, num2 * 45 / 540, this.m_treeLayer);
	}

	public void MoveTree(uint tree, Vector3 position)
	{
		this.MoveTreeImplementation(tree, ref this.m_trees.m_buffer[(int)((UIntPtr)tree)], position);
	}

	private void MoveTreeImplementation(uint tree, ref TreeInstance data, Vector3 position)
	{
		if (data.m_flags != 0)
		{
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			this.FinalizeTree(tree, ref data);
			data.Position = position;
			this.InitializeTree(tree, ref data, (mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None);
			this.UpdateTree(tree);
		}
	}

	private void ReleaseTreeImplementation(uint tree, ref TreeInstance data)
	{
		if (data.m_flags != 0)
		{
			InstanceID id = default(InstanceID);
			id.Tree = tree;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags |= 2;
			data.UpdateTree(tree);
			if ((data.m_flags & 64) != 0)
			{
				int num = this.m_burningTrees.m_size - 1;
				for (int i = 0; i <= num; i++)
				{
					if (this.m_burningTrees.m_buffer[i].m_treeIndex == tree)
					{
						this.m_burningTrees.m_buffer[i] = this.m_burningTrees.m_buffer[num];
						this.m_burningTrees.m_buffer[num] = default(TreeManager.BurningTree);
						this.m_burningTrees.m_size = num;
						break;
					}
				}
			}
			data.m_flags = 0;
			this.FinalizeTree(tree, ref data);
			this.m_trees.ReleaseItem(tree);
			this.m_treeCount = (int)(this.m_trees.ItemCount() - 1u);
		}
	}

	public void UpdateTree(uint tree)
	{
		this.m_updatedTrees[(int)((UIntPtr)(tree >> 6))] |= 1uL << (int)tree;
		this.m_treesUpdated = true;
	}

	public void UpdateTrees(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)((minX - 8f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((minZ - 8f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((maxX + 8f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((maxZ + 8f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num5 = this.m_treeGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0u)
				{
					Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num5)].Position;
					float num7 = Mathf.Max(Mathf.Max(minX - 8f - position.x, minZ - 8f - position.z), Mathf.Max(position.x - maxX - 8f, position.z - maxZ - 8f));
					if (num7 < 0f)
					{
						this.m_updatedTrees[(int)((UIntPtr)(num5 >> 6))] |= 1uL << (int)num5;
						this.m_treesUpdated = true;
					}
					num5 = this.m_trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
					if (++num6 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void UpdateTreeRenderer(uint tree, bool updateGroup)
	{
		if (this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_flags == 0)
		{
			return;
		}
		if (updateGroup)
		{
			int num;
			int num2;
			if (Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				num = Mathf.Clamp(((int)(this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_posX / 16) + 32768) * 540 / 65536, 0, 539);
				num2 = Mathf.Clamp(((int)(this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_posZ / 16) + 32768) * 540 / 65536, 0, 539);
			}
			else
			{
				num = Mathf.Clamp(((int)this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_posX + 32768) * 540 / 65536, 0, 539);
				num2 = Mathf.Clamp(((int)this.m_trees.m_buffer[(int)((UIntPtr)tree)].m_posZ + 32768) * 540 / 65536, 0, 539);
			}
			Singleton<RenderManager>.get_instance().UpdateGroup(num * 45 / 540, num2 * 45 / 540, this.m_treeLayer);
		}
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, int layer, uint ignoreTree)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		int num = Mathf.Max((int)((vector.x - 8f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((vector.y - 8f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 8f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((vector2.y + 8f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num5 = this.m_treeGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0u)
				{
					Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num5)].Position;
					float num7 = Mathf.Max(Mathf.Max(vector.x - 8f - position.x, vector.y - 8f - position.z), Mathf.Max(position.x - vector2.x - 8f, position.z - vector2.y - 8f));
					if (num7 < 0f && this.m_trees.m_buffer[(int)((UIntPtr)num5)].OverlapQuad(num5, quad, minY, maxY, collisionType))
					{
						return true;
					}
					num5 = this.m_trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
					if (++num6 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return false;
	}

	public bool RayCast(Segment3 ray, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers, TreeInstance.Flags ignoreFlags, out Vector3 hit, out uint treeIndex)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = (int)(ray.a.x / 32f + 270f);
			int num2 = (int)(ray.a.z / 32f + 270f);
			int num3 = (int)(ray.b.x / 32f + 270f);
			int num4 = (int)(ray.b.z / 32f + 270f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 32f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 32f / num6;
				}
			}
			float num9 = 2f;
			float num10 = 10000f;
			treeIndex = 0u;
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			int num11 = num;
			int num12 = num2;
			do
			{
				Vector3 vector4 = vector3 + vector;
				int num13;
				int num14;
				int num15;
				int num16;
				if (num7 != 0)
				{
					if ((num11 == num && num7 > 0) || (num11 == num3 && num7 < 0))
					{
						num13 = Mathf.Max((int)((vector4.x - 72f) / 32f + 270f), 0);
					}
					else
					{
						num13 = Mathf.Max(num11, 0);
					}
					if ((num11 == num && num7 < 0) || (num11 == num3 && num7 > 0))
					{
						num14 = Mathf.Min((int)((vector4.x + 72f) / 32f + 270f), 539);
					}
					else
					{
						num14 = Mathf.Min(num11, 539);
					}
					num15 = Mathf.Max((int)((Mathf.Min(vector2.z, vector4.z) - 72f) / 32f + 270f), 0);
					num16 = Mathf.Min((int)((Mathf.Max(vector2.z, vector4.z) + 72f) / 32f + 270f), 539);
				}
				else
				{
					if ((num12 == num2 && num8 > 0) || (num12 == num4 && num8 < 0))
					{
						num15 = Mathf.Max((int)((vector4.z - 72f) / 32f + 270f), 0);
					}
					else
					{
						num15 = Mathf.Max(num12, 0);
					}
					if ((num12 == num2 && num8 < 0) || (num12 == num4 && num8 > 0))
					{
						num16 = Mathf.Min((int)((vector4.z + 72f) / 32f + 270f), 539);
					}
					else
					{
						num16 = Mathf.Min(num12, 539);
					}
					num13 = Mathf.Max((int)((Mathf.Min(vector2.x, vector4.x) - 72f) / 32f + 270f), 0);
					num14 = Mathf.Min((int)((Mathf.Max(vector2.x, vector4.x) + 72f) / 32f + 270f), 539);
				}
				for (int i = num15; i <= num16; i++)
				{
					for (int j = num13; j <= num14; j++)
					{
						uint num17 = this.m_treeGrid[i * 540 + j];
						int num18 = 0;
						while (num17 != 0u)
						{
							TreeInstance.Flags flags = (TreeInstance.Flags)this.m_trees.m_buffer[(int)((UIntPtr)num17)].m_flags;
							if ((flags & ignoreFlags) == TreeInstance.Flags.None && ray.DistanceSqr(this.m_trees.m_buffer[(int)((UIntPtr)num17)].Position) < 2500f)
							{
								TreeInfo info = this.m_trees.m_buffer[(int)((UIntPtr)num17)].Info;
								float num19;
								float num20;
								if ((service == ItemClass.Service.None || info.m_class.m_service == service) && (subService == ItemClass.SubService.None || info.m_class.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_class.m_layer & itemLayers) != ItemClass.Layer.None) && this.m_trees.m_buffer[(int)((UIntPtr)num17)].RayCast(num17, ray, out num19, out num20) && (num19 < num9 - 0.0001f || (num19 < num9 + 0.0001f && num20 < num10)))
								{
									num9 = num19;
									num10 = num20;
									treeIndex = num17;
								}
							}
							num17 = this.m_trees.m_buffer[(int)((UIntPtr)num17)].m_nextGridTree;
							if (++num18 > 262144)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector2 = vector3;
				vector3 = vector4;
				num11 += num7;
				num12 += num8;
			}
			while ((num11 <= num3 || num7 <= 0) && (num11 >= num3 || num7 >= 0) && (num12 <= num4 || num8 <= 0) && (num12 >= num4 || num8 >= 0));
			if (num9 != 2f)
			{
				hit = ray.Position(num9);
				return true;
			}
		}
		hit = Vector3.get_zero();
		treeIndex = 0u;
		return false;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_treesUpdated)
		{
			int num = this.m_updatedTrees.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedTrees[i];
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							uint num3 = (uint)(i << 6 | j);
							this.m_trees.m_buffer[(int)((UIntPtr)num3)].CalculateTree(num3);
						}
					}
				}
			}
			this.m_treesUpdated = false;
			for (int k = 0; k < num; k++)
			{
				ulong num4 = this.m_updatedTrees[k];
				if (num4 != 0uL)
				{
					this.m_updatedTrees[k] = 0uL;
					for (int l = 0; l < 64; l++)
					{
						if ((num4 & 1uL << l) != 0uL)
						{
							uint num5 = (uint)(k << 6 | l);
							this.m_trees.m_buffer[(int)((UIntPtr)num5)].UpdateTree(num5);
							this.UpdateTreeRenderer(num5, true);
						}
					}
				}
			}
		}
		if (subStep != 0)
		{
			int num6 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num7 = num6 * this.m_burningTrees.m_size >> 8;
			int num8 = ((num6 + 1) * this.m_burningTrees.m_size >> 8) - 1;
			int m = num7;
			while (m <= num8)
			{
				if (this.FireSimulationStep(ref this.m_burningTrees.m_buffer[m]))
				{
					m++;
				}
				else
				{
					int num9 = this.m_burningTrees.m_size - 1;
					this.m_burningTrees.m_buffer[m] = this.m_burningTrees.m_buffer[num9];
					this.m_burningTrees.m_buffer[num9] = default(TreeManager.BurningTree);
					this.m_burningTrees.m_size = num9;
					if (num8 >= num9)
					{
						num8 = num9 - 1;
					}
				}
			}
		}
	}

	private bool FireSimulationStep(ref TreeManager.BurningTree tree)
	{
		if (tree.m_fireIntensity != 0)
		{
			int num = (int)tree.m_fireDamage + (Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)(tree.m_fireIntensity + 8)) >> 3);
			if (num >= 255)
			{
				tree.m_fireDamage = 255;
				tree.m_fireIntensity = 0;
				TreeInstance.Flags flags = (TreeInstance.Flags)this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags;
				flags &= ~TreeInstance.Flags.Burning;
				this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags = (ushort)flags;
				InstanceID id = default(InstanceID);
				id.Tree = tree.m_treeIndex;
				Singleton<InstanceManager>.get_instance().SetGroup(id, null);
				return true;
			}
			tree.m_fireDamage = (byte)num;
			if (!this.HandleFireSpread(ref tree))
			{
				tree.m_fireIntensity = 0;
				TreeInstance.Flags flags2 = (TreeInstance.Flags)this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags;
				flags2 &= ~TreeInstance.Flags.Burning;
				this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags = (ushort)flags2;
				InstanceID id2 = default(InstanceID);
				id2.Tree = tree.m_treeIndex;
				Singleton<InstanceManager>.get_instance().SetGroup(id2, null);
			}
			return true;
		}
		else
		{
			if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 256u) == 0u)
			{
				return true;
			}
			int num2 = (int)(tree.m_fireDamage - 1);
			if (num2 <= 0)
			{
				tree.m_fireDamage = 0;
				TreeInstance.Flags flags3 = (TreeInstance.Flags)this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags;
				flags3 &= ~TreeInstance.Flags.FireDamage;
				this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].m_flags = (ushort)flags3;
				return false;
			}
			tree.m_fireDamage = (byte)num2;
			return true;
		}
	}

	private bool HandleFireSpread(ref TreeManager.BurningTree tree)
	{
		Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].Position;
		float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(position));
		if (num > position.y + 1f)
		{
			return false;
		}
		if (this.m_trees.m_buffer[(int)((UIntPtr)tree.m_treeIndex)].GrowState == 0)
		{
			return true;
		}
		int num2 = tree.m_fireIntensity + 15 >> 4;
		Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Burned, num2, num2, position, 20f, true);
		float num3 = (float)((int)tree.m_fireIntensity * (128 - Mathf.Abs((int)(tree.m_fireDamage - 128))));
		InstanceID instanceID = default(InstanceID);
		instanceID.Tree = tree.m_treeIndex;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(instanceID);
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterManager instance = Singleton<DisasterManager>.get_instance();
				DisasterInfo info = instance.m_disasters.m_buffer[(int)disaster].Info;
				int fireSpreadProbability = info.m_disasterAI.GetFireSpreadProbability(disaster, ref instance.m_disasters.m_buffer[(int)disaster]);
				num3 *= (float)fireSpreadProbability * 0.01f;
			}
		}
		int num4 = Mathf.Max((int)((position.x - 32f) / 32f + 270f), 0);
		int num5 = Mathf.Max((int)((position.z - 32f) / 32f + 270f), 0);
		int num6 = Mathf.Min((int)((position.x + 32f) / 32f + 270f), 539);
		int num7 = Mathf.Min((int)((position.z + 32f) / 32f + 270f), 539);
		for (int i = num5; i <= num7; i++)
		{
			for (int j = num4; j <= num6; j++)
			{
				uint num8 = this.m_treeGrid[i * 540 + j];
				int num9 = 0;
				while (num8 != 0u)
				{
					Vector3 position2 = this.m_trees.m_buffer[(int)((UIntPtr)num8)].Position;
					float num10 = Vector3.Distance(position2, position);
					if (num10 < 32f && (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32768u) * num10 < num3)
					{
						this.BurnTree(num8, group, (int)tree.m_fireIntensity);
					}
					num8 = this.m_trees.m_buffer[(int)((UIntPtr)num8)].m_nextGridTree;
					if (++num9 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		int num11 = Mathf.Max((int)((position.x - 32f - 72f) / 64f + 135f), 0);
		int num12 = Mathf.Max((int)((position.z - 32f - 72f) / 64f + 135f), 0);
		int num13 = Mathf.Min((int)((position.x + 32f + 72f) / 64f + 135f), 269);
		int num14 = Mathf.Min((int)((position.z + 32f + 72f) / 64f + 135f), 269);
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		bool flag = false;
		for (int k = num12; k <= num14; k++)
		{
			for (int l = num11; l <= num13; l++)
			{
				ushort num15 = instance2.m_buildingGrid[k * 270 + l];
				int num16 = 0;
				while (num15 != 0)
				{
					Vector3 position3 = instance2.m_buildings.m_buffer[(int)num15].m_position;
					float num17 = VectorUtils.LengthSqrXZ(position3 - position);
					if (!flag && num17 < 10000f)
					{
						BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)num15].Info;
						float num18 = info2.m_buildingAI.MaxFireDetectDistance(num15, ref instance2.m_buildings.m_buffer[(int)num15]);
						if (num17 < num18 * num18 && info2.m_buildingAI.NearObjectInFire(num15, ref instance2.m_buildings.m_buffer[(int)num15], instanceID, position))
						{
							flag = true;
						}
					}
					if (num17 < 10816f)
					{
						float angle = instance2.m_buildings.m_buffer[(int)num15].m_angle;
						BuildingInfo buildingInfo;
						int num19;
						int num20;
						instance2.m_buildings.m_buffer[(int)num15].GetInfoWidthLength(out buildingInfo, out num19, out num20);
						Vector3 vector;
						vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
						Vector3 vector2;
						vector2..ctor(vector.z, 0f, -vector.x);
						Vector3 vector3 = position - position3;
						vector3 -= Mathf.Clamp(Vector3.Dot(vector3, vector), (float)(-(float)num19) * 4f, (float)num19 * 4f) * vector;
						float magnitude = (vector3 - Mathf.Clamp(Vector3.Dot(vector3, vector2), (float)(-(float)num20) * 4f, (float)num20 * 4f) * vector2).get_magnitude();
						if (magnitude < 32f && (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(65536u) * magnitude < num3)
						{
							TreeManager.TrySpreadFire(num15, ref instance2.m_buildings.m_buffer[(int)num15], group);
						}
					}
					num15 = instance2.m_buildings.m_buffer[(int)num15].m_nextGridBuilding;
					if (++num16 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		int num21;
		Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.FirewatchCoverage, position, out num21);
		if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < num21)
		{
			FastList<ushort> serviceBuildings = instance2.GetServiceBuildings(ItemClass.Service.FireDepartment);
			for (int m = 0; m < serviceBuildings.m_size; m++)
			{
				ushort num22 = serviceBuildings.m_buffer[m];
				Vector3 position4 = instance2.m_buildings.m_buffer[(int)num22].m_position;
				float num23 = VectorUtils.LengthSqrXZ(position4 - position);
				BuildingInfo info3 = instance2.m_buildings.m_buffer[(int)num22].Info;
				float num24 = info3.m_buildingAI.MaxFireDetectDistance(num22, ref instance2.m_buildings.m_buffer[(int)num22]);
				if (num23 < num24 * num24 && info3.m_buildingAI.NearObjectInFire(num22, ref instance2.m_buildings.m_buffer[(int)num22], instanceID, position))
				{
					break;
				}
			}
		}
		return true;
	}

	private static void TrySpreadFire(ushort buildingID, ref Building buildingData, InstanceManager.Group group)
	{
		BuildingInfo info = buildingData.Info;
		int num;
		int num2;
		int num3;
		if (info.m_buildingAI.GetFireParameters(buildingID, ref buildingData, out num, out num2, out num3) && (buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Abandoned)) == Building.Flags.Completed && buildingData.m_fireIntensity == 0 && buildingData.GetLastFrameData().m_fireDamage == 0)
		{
			if ((buildingData.m_flags & Building.Flags.Untouchable) != Building.Flags.None)
			{
				ushort num4 = Building.FindParentBuilding(buildingID);
				if (num4 != 0)
				{
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					info = instance.m_buildings.m_buffer[(int)num4].Info;
					if (info.m_buildingAI.GetFireParameters(buildingID, ref instance.m_buildings.m_buffer[(int)num4], out num, out num2, out num3) && (instance.m_buildings.m_buffer[(int)num4].m_flags & (Building.Flags.Completed | Building.Flags.Abandoned)) == Building.Flags.Completed && instance.m_buildings.m_buffer[(int)num4].m_fireIntensity == 0 && instance.m_buildings.m_buffer[(int)num4].GetLastFrameData().m_fireDamage == 0)
					{
						info.m_buildingAI.BurnBuilding(num4, ref instance.m_buildings.m_buffer[(int)num4], group, false);
					}
				}
			}
			else
			{
				info.m_buildingAI.BurnBuilding(buildingID, ref buildingData, group, false);
			}
		}
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float x = surfaceArea.m_min.x;
		float z = surfaceArea.m_min.z;
		float x2 = surfaceArea.m_max.x;
		float z2 = surfaceArea.m_max.z;
		int num = Mathf.Max((int)((x - 8f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((z - 8f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((x2 + 8f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((z2 + 8f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num5 = this.m_treeGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0u)
				{
					Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num5)].Position;
					float num7 = Mathf.Max(Mathf.Max(x - 8f - position.x, z - 8f - position.z), Mathf.Max(position.x - x2 - 8f, position.z - z2 - 8f));
					if (num7 < 0f)
					{
						this.m_trees.m_buffer[(int)((UIntPtr)num5)].TerrainUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
					if (++num6 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float x = heightArea.m_min.x;
		float z = heightArea.m_min.z;
		float x2 = heightArea.m_max.x;
		float z2 = heightArea.m_max.z;
		int num = Mathf.Max((int)((x - 8f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((z - 8f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((x2 + 8f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((z2 + 8f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				uint num5 = this.m_treeGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0u)
				{
					Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num5)].Position;
					float num7 = Mathf.Max(Mathf.Max(x - 8f - position.x, z - 8f - position.z), Mathf.Max(position.x - x2 - 8f, position.z - z2 - 8f));
					if (num7 < 0f)
					{
						this.m_trees.m_buffer[(int)((UIntPtr)num5)].AfterTerrainUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_trees.m_buffer[(int)((UIntPtr)num5)].m_nextGridTree;
					if (++num6 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void CalculateAreaHeight(float minX, float minZ, float maxX, float maxZ, out int num, out float min, out float avg, out float max)
	{
		int num2 = Mathf.Max((int)((minX - 8f) / 32f + 270f), 0);
		int num3 = Mathf.Max((int)((minZ - 8f) / 32f + 270f), 0);
		int num4 = Mathf.Min((int)((maxX + 8f) / 32f + 270f), 539);
		int num5 = Mathf.Min((int)((maxZ + 8f) / 32f + 270f), 539);
		num = 0;
		min = 1024f;
		avg = 0f;
		max = 0f;
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				uint num6 = this.m_treeGrid[i * 540 + j];
				int num7 = 0;
				while (num6 != 0u)
				{
					Vector3 position = this.m_trees.m_buffer[(int)((UIntPtr)num6)].Position;
					float num8 = Mathf.Max(Mathf.Max(minX - 8f - position.x, minZ - 8f - position.z), Mathf.Max(position.x - maxX - 8f, position.z - maxZ - 8f));
					if (num8 < 0f)
					{
						TreeInfo info = this.m_trees.m_buffer[(int)((UIntPtr)num6)].Info;
						if (info != null)
						{
							Randomizer randomizer;
							randomizer..ctor(num6);
							float num9 = info.m_minScale + (float)randomizer.Int32(10000u) * (info.m_maxScale - info.m_minScale) * 0.0001f;
							float num10 = position.y + info.m_generatedInfo.m_size.y * num9 * 2f;
							if (num10 < min)
							{
								min = num10;
							}
							avg += num10;
							if (num10 > max)
							{
								max = num10;
							}
							num++;
						}
					}
					num6 = this.m_trees.m_buffer[(int)((UIntPtr)num6)].m_nextGridTree;
					if (++num7 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (avg != 0f)
		{
			avg /= (float)num;
		}
	}

	public override bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		if (layer != this.m_treeLayer)
		{
			return result;
		}
		int num = groupX * 540 / 45;
		int num2 = groupZ * 540 / 45;
		int num3 = (groupX + 1) * 540 / 45 - 1;
		int num4 = (groupZ + 1) * 540 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 540 + j;
				uint num6 = this.m_treeGrid[num5];
				int num7 = 0;
				while (num6 != 0u)
				{
					if (this.m_trees.m_buffer[(int)((UIntPtr)num6)].CalculateGroupData(num6, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
					num6 = this.m_trees.m_buffer[(int)((UIntPtr)num6)].m_nextGridTree;
					if (++num7 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		if (layer != this.m_treeLayer)
		{
			return;
		}
		int num = groupX * 540 / 45;
		int num2 = groupZ * 540 / 45;
		int num3 = (groupX + 1) * 540 / 45 - 1;
		int num4 = (groupZ + 1) * 540 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 540 + j;
				uint num6 = this.m_treeGrid[num5];
				int num7 = 0;
				while (num6 != 0u)
				{
					this.m_trees.m_buffer[(int)((UIntPtr)num6)].PopulateGroupData(num6, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
					num6 = this.m_trees.m_buffer[(int)((UIntPtr)num6)].m_nextGridTree;
					if (++num7 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("TreeManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 262144; i++)
		{
			if (this.m_trees.m_buffer[i].m_flags != 0 && this.m_trees.m_buffer[i].Info == null)
			{
				this.ReleaseTree((uint)i);
			}
		}
		int num = PrefabCollection<TreeInfo>.PrefabCount();
		int num2 = 1;
		while (num2 * num2 < num)
		{
			num2++;
		}
		for (int j = 0; j < num; j++)
		{
			TreeInfo prefab = PrefabCollection<TreeInfo>.GetPrefab((uint)j);
			if (prefab != null)
			{
				prefab.SetRenderParameters(j, num2);
			}
		}
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.m_lastShadowRotation = default(Quaternion);
			this.m_lastCameraRotation = default(Quaternion);
		});
		this.m_infoCount = num;
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new TreeManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const float TREEGRID_CELL_SIZE = 32f;

	public const int TREEGRID_RESOLUTION = 540;

	public const int MAX_TREE_COUNT = 262144;

	public const int MAX_MAP_TREES = 250000;

	public const int MAX_ASSET_TREES = 64;

	public int m_treeCount;

	public int m_infoCount;

	public RenderTexture m_renderDiffuseTexture;

	public RenderTexture m_renderXycaTexture;

	public RenderTexture m_renderShadowTexture;

	[NonSerialized]
	public Array32<TreeInstance> m_trees;

	[NonSerialized]
	public ulong[] m_updatedTrees;

	[NonSerialized]
	public bool m_treesUpdated;

	[NonSerialized]
	public uint[] m_treeGrid;

	[NonSerialized]
	public FastList<TreeManager.BurningTree> m_burningTrees;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_XYCAMap;

	[NonSerialized]
	public int ID_ShadowAMap;

	[NonSerialized]
	public int ID_ShadowMatrix;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_TileRect;

	[NonSerialized]
	public int ID_TreeLocation;

	[NonSerialized]
	public int ID_TreeColor;

	[NonSerialized]
	public int m_treeLayer;

	private int m_renderPass;

	private Camera m_renderCamera;

	private RenderManager.CameraInfo m_cameraInfo;

	private Quaternion m_lastShadowRotation;

	private Quaternion m_lastCameraRotation;

	public struct BurningTree
	{
		public uint m_treeIndex;

		public byte m_fireIntensity;

		public byte m_fireDamage;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "TreeManager");
			TreeManager instance = Singleton<TreeManager>.get_instance();
			TreeInstance[] buffer = instance.m_trees.m_buffer;
			int num = buffer.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uShort.Write(buffer[i].m_flags);
			}
			uShort.EndWrite();
			try
			{
				PrefabCollection<TreeInfo>.BeginSerialize(s);
				for (int j = 1; j < num; j++)
				{
					if (buffer[j].m_flags != 0)
					{
						PrefabCollection<TreeInfo>.Serialize((uint)buffer[j].m_infoIndex);
					}
				}
			}
			finally
			{
				PrefabCollection<TreeInfo>.EndSerialize(s);
			}
			EncodedArray.Short @short = EncodedArray.Short.BeginWrite(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != 0)
				{
					@short.Write(buffer[k].m_posX);
				}
			}
			@short.EndWrite();
			EncodedArray.Short short2 = EncodedArray.Short.BeginWrite(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != 0)
				{
					short2.Write(buffer[l].m_posZ);
				}
			}
			short2.EndWrite();
			s.WriteUInt24((uint)instance.m_burningTrees.m_size);
			for (int m = 0; m < instance.m_burningTrees.m_size; m++)
			{
				s.WriteUInt24(instance.m_burningTrees.m_buffer[m].m_treeIndex);
				s.WriteUInt8((uint)instance.m_burningTrees.m_buffer[m].m_fireIntensity);
				s.WriteUInt8((uint)instance.m_burningTrees.m_buffer[m].m_fireDamage);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "TreeManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "TreeManager");
			TreeManager instance = Singleton<TreeManager>.get_instance();
			TreeInstance[] buffer = instance.m_trees.m_buffer;
			uint[] treeGrid = instance.m_treeGrid;
			int num = buffer.Length;
			int num2 = treeGrid.Length;
			instance.m_trees.ClearUnused();
			instance.m_burningTrees.Clear();
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			bool assetEditor = updateMode == SimulationManager.UpdateMode.NewAsset || updateMode == SimulationManager.UpdateMode.LoadAsset;
			for (int i = 0; i < num2; i++)
			{
				treeGrid[i] = 0u;
			}
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int j = 1; j < num; j++)
			{
				TreeInstance.Flags flags = (TreeInstance.Flags)uShort.Read();
				flags &= ~(TreeInstance.Flags.FireDamage | TreeInstance.Flags.Burning);
				buffer[j].m_flags = (ushort)flags;
			}
			uShort.EndRead();
			PrefabCollection<TreeInfo>.BeginDeserialize(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != 0)
				{
					buffer[k].m_infoIndex = (ushort)PrefabCollection<TreeInfo>.Deserialize(true);
				}
			}
			PrefabCollection<TreeInfo>.EndDeserialize(s);
			EncodedArray.Short @short = EncodedArray.Short.BeginRead(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != 0)
				{
					buffer[l].m_posX = @short.Read();
				}
				else
				{
					buffer[l].m_posX = 0;
				}
			}
			@short.EndRead();
			EncodedArray.Short short2 = EncodedArray.Short.BeginRead(s);
			for (int m = 1; m < num; m++)
			{
				if (buffer[m].m_flags != 0)
				{
					buffer[m].m_posZ = short2.Read();
				}
				else
				{
					buffer[m].m_posZ = 0;
				}
			}
			short2.EndRead();
			if (s.get_version() >= 266u)
			{
				int num3 = (int)s.ReadUInt24();
				instance.m_burningTrees.EnsureCapacity(num3);
				for (int n = 0; n < num3; n++)
				{
					TreeManager.BurningTree item;
					item.m_treeIndex = s.ReadUInt24();
					item.m_fireIntensity = (byte)s.ReadUInt8();
					item.m_fireDamage = (byte)s.ReadUInt8();
					if (item.m_treeIndex != 0u)
					{
						instance.m_burningTrees.Add(item);
						TreeInstance[] expr_255_cp_0 = buffer;
						UIntPtr expr_255_cp_1 = (UIntPtr)item.m_treeIndex;
						expr_255_cp_0[(int)expr_255_cp_1].m_flags = (expr_255_cp_0[(int)expr_255_cp_1].m_flags | 64);
						if (item.m_fireIntensity != 0)
						{
							TreeInstance[] expr_27E_cp_0 = buffer;
							UIntPtr expr_27E_cp_1 = (UIntPtr)item.m_treeIndex;
							expr_27E_cp_0[(int)expr_27E_cp_1].m_flags = (expr_27E_cp_0[(int)expr_27E_cp_1].m_flags | 128);
						}
					}
				}
			}
			for (int num4 = 1; num4 < num; num4++)
			{
				buffer[num4].m_nextGridTree = 0u;
				buffer[num4].m_posY = 0;
				if (buffer[num4].m_flags != 0)
				{
					instance.InitializeTree((uint)num4, ref buffer[num4], assetEditor);
				}
				else
				{
					instance.m_trees.ReleaseItem((uint)num4);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "TreeManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "TreeManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<TreeInfo>.BindPrefabs();
			TreeManager instance = Singleton<TreeManager>.get_instance();
			TreeInstance[] buffer = instance.m_trees.m_buffer;
			int num = buffer.Length;
			for (int i = 1; i < num; i++)
			{
				if (buffer[i].m_flags != 0)
				{
					TreeInfo info = buffer[i].Info;
					if (info != null)
					{
						buffer[i].m_infoIndex = (ushort)info.m_prefabDataIndex;
					}
				}
			}
			instance.m_treeCount = (int)(instance.m_trees.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "TreeManager");
		}
	}
}
