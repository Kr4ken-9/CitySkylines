﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TornadoAI : WeatherDisasterAI
{
	public override void PlayInstance(AudioManager.ListenerInfo listenerInfo, ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None && this.m_vortexSound != null)
		{
			InstanceID empty = InstanceID.Empty;
			empty.Disaster = disasterID;
			DisasterAI.m_tempList.Clear();
			InstanceManager.GetAllGroupInstances(empty, DisasterAI.m_tempList);
			if (DisasterAI.m_tempList.m_size > 1)
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				for (int i = 0; i < DisasterAI.m_tempList.m_size; i++)
				{
					InstanceID instanceID = DisasterAI.m_tempList.m_buffer[i];
					ushort vehicle = instanceID.Vehicle;
					if (vehicle != 0)
					{
						VehicleInfo info = instance.m_vehicles.m_buffer[(int)vehicle].Info;
						if (info == this.m_vortexInfo)
						{
							if ((instance.m_vehicles.m_buffer[(int)vehicle].m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
							{
								uint num = (uint)(((int)vehicle << 4) / 16384);
								uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
								Vehicle.Frame frameData = instance.m_vehicles.m_buffer[(int)vehicle].GetFrameData(num2 - 32u);
								Vehicle.Frame frameData2 = instance.m_vehicles.m_buffer[(int)vehicle].GetFrameData(num2 - 16u);
								float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
								Vector3 position = Vector3.Lerp(frameData.m_position, frameData2.m_position, num3);
								Vector3 velocity = Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num3) * 3.75f;
								float num4 = Mathf.Lerp(frameData.m_angleVelocity, frameData2.m_angleVelocity, num3);
								num4 = Mathf.Clamp01(num4 * (0.5f + (float)data.m_intensity * 0.005f));
								float pitch = Mathf.Clamp(1.04f - (float)data.m_intensity * 0.004f, 0.5f, 1f);
								Singleton<AudioManager>.get_instance().EffectGroup.AddPlayer(listenerInfo, (int)instanceID.RawData, this.m_vortexSound, position, velocity, 5000f, num4, pitch);
							}
							return;
						}
					}
				}
			}
		}
	}

	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			VortexAI vortexAI = this.m_vortexInfo.m_vehicleAI as VortexAI;
			if (vortexAI == null)
			{
				return;
			}
			float num = ((float)data.m_intensity * 10f + 400f) * 2f;
			float num2 = num * 0.25f;
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector2 vector = VectorUtils.XZ(data.m_targetPosition);
			Vector2 vector2;
			vector2.x = -Mathf.Sin(data.m_angle);
			vector2.y = Mathf.Cos(data.m_angle);
			Segment2 segment;
			segment.a = vector - vector2 * (num * 0.5f);
			segment.b = vector + vector2 * (num * 0.5f);
			float num3 = 300f;
			segment.a.x = segment.a.x + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.a.y = segment.a.y + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.b.x = segment.b.x + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			segment.b.y = segment.b.y + (float)randomizer.Int32(-1000, 1000) * 0.001f * num3;
			float num4 = 0f;
			float num5 = (float)data.m_intensity * 20f + num3 * 2f;
			Vector2 vector3 = segment.Min() - new Vector2(num5 + num2, num5 + num2);
			Vector2 vector4 = segment.Max() + new Vector2(num5 + num2, num5 + num2);
			int num6 = Mathf.Max((int)(vector3.x / 38.4f + 128f), 2);
			int num7 = Mathf.Max((int)(vector3.y / 38.4f + 128f), 2);
			int num8 = Mathf.Min((int)(vector4.x / 38.4f + 128f), 253);
			int num9 = Mathf.Min((int)(vector4.y / 38.4f + 128f), 253);
			WeatherManager instance = Singleton<WeatherManager>.get_instance();
			for (int i = num7; i <= num9; i++)
			{
				Vector2 vector5;
				vector5.y = ((float)i - 128f + 0.5f) * 38.4f;
				for (int j = num6; j <= num8; j++)
				{
					vector5.x = ((float)j - 128f + 0.5f) * 38.4f;
					float num11;
					float num10 = Mathf.Sqrt(segment.DistanceSqr(vector5, ref num11));
					float num12 = 2f * num11 - 1f;
					num10 -= num2 * Mathf.Sqrt(Mathf.Max(0f, 1f - num12 * num12));
					if (num10 < num5)
					{
						float num13 = Mathf.Min(1f, 1f - (num10 - num4) / (num5 - num4));
						num13 = Mathf.Min(1f, num13 * (instance.GetWindSpeed(vector5) * 0.25f + 0.75f));
						if (num13 > 0f)
						{
							num13 *= num13;
							int num14 = i * 256 + j;
							map[num14] = (byte)(255 - (int)(255 - map[num14]) * (255 - Mathf.RoundToInt(num13 * 255f)) / 255);
						}
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(data.m_targetPosition);
			data.m_activationFrame = data.m_startFrame + this.m_emergingDuration;
			data.m_flags |= DisasterData.Flags.Significant;
			Singleton<DisasterManager>.get_instance().m_randomDisasterCooldown = 0;
		}
	}

	protected override void ActivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.ActivateDisaster(disasterID, ref data);
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor(data.m_randomSeed);
		Vector3 vector;
		vector.x = Mathf.Sin(data.m_angle);
		vector.y = 0f;
		vector.z = -Mathf.Cos(data.m_angle);
		vector = vector.get_normalized() * ((float)data.m_intensity * 10f + 400f);
		Vector3 position = data.m_targetPosition + vector;
		Vector3 vector2 = data.m_targetPosition - vector;
		ushort num;
		if (instance.CreateVehicle(out num, ref randomizer, this.m_vortexInfo, position, TransferManager.TransferReason.None, false, false))
		{
			InstanceID srcID = default(InstanceID);
			InstanceID dstID = default(InstanceID);
			srcID.Disaster = disasterID;
			dstID.Vehicle = num;
			Singleton<InstanceManager>.get_instance().CopyGroup(srcID, dstID);
			instance.m_vehicles.m_buffer[(int)num].SetTargetPos(0, data.m_targetPosition);
			instance.m_vehicles.m_buffer[(int)num].SetTargetPos(1, vector2);
		}
		Singleton<DisasterManager>.get_instance().FollowDisaster(disasterID);
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
		Singleton<WeatherManager>.get_instance().m_targetRain = 0f;
		Singleton<WeatherManager>.get_instance().m_targetCloud = 0f;
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex + 1755u > data.m_activationFrame)
			{
				Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, false);
				Singleton<WeatherManager>.get_instance().m_forceWeatherOn = 2f;
				Singleton<WeatherManager>.get_instance().m_targetFog = 0f;
				Singleton<WeatherManager>.get_instance().m_targetRain = this.m_targetRain;
				Singleton<WeatherManager>.get_instance().m_targetCloud = 1f;
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			Singleton<WeatherManager>.get_instance().m_forceWeatherOn = 2f;
			Singleton<WeatherManager>.get_instance().m_targetFog = 0f;
			Singleton<WeatherManager>.get_instance().m_targetRain = this.m_targetRain;
			Singleton<WeatherManager>.get_instance().m_targetCloud = 1f;
		}
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		return base.IsStillEmerging(disasterID, ref data) || Singleton<SimulationManager>.get_instance().m_currentFrameIndex < data.m_activationFrame;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		InstanceID id = default(InstanceID);
		id.Disaster = disasterID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		return group != null && group.m_refCount >= 2;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return 100 / (1 + ((int)num >> 6));
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool HasDirection()
	{
		return true;
	}

	public override bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		InstanceID empty = InstanceID.Empty;
		empty.Disaster = disasterID;
		DisasterAI.m_tempList.Clear();
		InstanceManager.GetAllGroupInstances(empty, DisasterAI.m_tempList);
		if (DisasterAI.m_tempList.m_size > 1)
		{
			Vector3 vector;
			vector..ctor(1000000f, 1000000f, 1000000f);
			Vector3 vector2;
			vector2..ctor(-1000000f, -1000000f, -1000000f);
			bool flag = false;
			for (int i = 0; i < DisasterAI.m_tempList.m_size; i++)
			{
				InstanceID id = DisasterAI.m_tempList.m_buffer[i];
				Vector3 vector3;
				Quaternion quaternion;
				Vector3 vector4;
				if (id.Vehicle != 0 && Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)id.Vehicle].Info == this.m_vortexInfo && InstanceManager.GetPosition(id, out vector3, out quaternion, out vector4))
				{
					vector4 = quaternion * vector4 * 0.5f;
					vector = Vector3.Min(vector, vector3 - vector4);
					vector2 = Vector3.Max(vector2, vector3 + vector4);
					flag = true;
				}
			}
			if (flag)
			{
				position = (vector + vector2) * 0.5f;
				rotation = Quaternion.get_identity();
				size = Vector3.Max(vector2 - vector, new Vector3(1000f, 1000f, 1000f));
				return true;
			}
		}
		position = data.m_targetPosition;
		rotation = Quaternion.get_identity();
		size..ctor(1000f, 1000f, 1000f);
		return false;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.WeatherDetection;
		return true;
	}

	public uint m_emergingDuration = 4096u;

	public VehicleInfo m_vortexInfo;

	public AudioInfo m_vortexSound;

	public float m_targetRain = 1f;
}
