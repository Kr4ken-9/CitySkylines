﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class IncomePercentagePieChart
{
	public IncomePercentagePieChart(UIRadialChart radialChart, ItemClass.Service service, string localeID, bool isIncomePieChart = true, ItemClass.Service secondService = ItemClass.Service.None)
	{
		this.m_pieChart = radialChart;
		this.m_label = radialChart.Find<UILabel>("PercentageLabel");
		this.m_localeID = localeID;
		this.m_Service = service;
		this.m_SecondService = secondService;
		this.m_SubService = new ItemClass.SubService[1];
		this.m_isIncomePieChart = isIncomePieChart;
	}

	public void Update()
	{
		long num = this.PollIncomeOrExpenses(ItemClass.Service.None, this.m_isIncomePieChart);
		if (num == 0L)
		{
			num = 1L;
		}
		long num2 = this.PollIncomeOrExpenses(this.m_Service, this.m_isIncomePieChart);
		if (this.m_SecondService != ItemClass.Service.None)
		{
			num2 += this.PollIncomeOrExpenses(this.m_SecondService, this.m_isIncomePieChart);
		}
		float num3 = (float)num2 / (float)num;
		this.m_pieChart.GetSlice(0).set_endValue(num3);
		this.m_pieChart.GetSlice(1).set_startValue(num3);
		this.m_label.set_text(StringUtils.SafeFormat(Locale.Get(this.m_localeID), num3.ToString("P")));
	}

	private long PollIncomeOrExpenses(ItemClass.Service service, bool returnIncome = true)
	{
		long num = 0L;
		long num2 = 0L;
		if (Singleton<EconomyManager>.get_exists())
		{
			for (int i = 0; i < this.m_SubService.Length; i++)
			{
				long num3 = 0L;
				long num4 = 0L;
				Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(service, this.m_SubService[i], ItemClass.Level.None, out num3, out num4);
				num += num3;
				num2 += num4;
			}
			if (service == ItemClass.Service.FireDepartment)
			{
				for (int j = 0; j < this.m_SubService.Length; j++)
				{
					long num5 = 0L;
					long num6 = 0L;
					Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(ItemClass.Service.Disaster, this.m_SubService[j], ItemClass.Level.None, out num5, out num6);
					num += num5;
					num2 += num6;
				}
			}
		}
		if (returnIncome)
		{
			return num;
		}
		return num2;
	}

	private UIRadialChart m_pieChart;

	private UILabel m_label;

	private string m_localeID;

	private ItemClass.Service m_Service;

	private ItemClass.Service m_SecondService;

	private ItemClass.SubService[] m_SubService;

	private bool m_isIncomePieChart;
}
