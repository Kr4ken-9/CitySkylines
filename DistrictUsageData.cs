﻿using System;
using ColossalFramework.IO;

public struct DistrictUsageData
{
	public void Add(ref DistrictUsageData data)
	{
		this.m_tempElectricityUsage += data.m_tempElectricityUsage;
		this.m_tempHeatingUsage += data.m_tempHeatingUsage;
		this.m_tempWaterUsage += data.m_tempWaterUsage;
		this.m_tempSewageUsage += data.m_tempSewageUsage;
	}

	public void Update()
	{
		this.m_finalElectricityUsage = this.m_tempElectricityUsage;
		this.m_finalHeatingUsage = this.m_tempHeatingUsage;
		this.m_finalWaterUsage = this.m_tempWaterUsage;
		this.m_finalSewageUsage = this.m_tempSewageUsage;
	}

	public void Reset()
	{
		this.m_tempElectricityUsage = 0u;
		this.m_tempHeatingUsage = 0u;
		this.m_tempWaterUsage = 0u;
		this.m_tempSewageUsage = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempElectricityUsage);
		s.WriteUInt24(this.m_tempHeatingUsage);
		s.WriteUInt24(this.m_tempWaterUsage);
		s.WriteUInt24(this.m_tempSewageUsage);
		s.WriteUInt24(this.m_finalElectricityUsage);
		s.WriteUInt24(this.m_finalHeatingUsage);
		s.WriteUInt24(this.m_finalWaterUsage);
		s.WriteUInt24(this.m_finalSewageUsage);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempElectricityUsage = s.ReadUInt24();
		this.m_tempHeatingUsage = s.ReadUInt24();
		this.m_tempWaterUsage = s.ReadUInt24();
		this.m_tempSewageUsage = s.ReadUInt24();
		this.m_finalElectricityUsage = s.ReadUInt24();
		this.m_finalHeatingUsage = s.ReadUInt24();
		this.m_finalWaterUsage = s.ReadUInt24();
		this.m_finalSewageUsage = s.ReadUInt24();
	}

	public uint m_tempElectricityUsage;

	public uint m_tempHeatingUsage;

	public uint m_tempWaterUsage;

	public uint m_tempSewageUsage;

	public uint m_finalElectricityUsage;

	public uint m_finalHeatingUsage;

	public uint m_finalWaterUsage;

	public uint m_finalSewageUsage;
}
