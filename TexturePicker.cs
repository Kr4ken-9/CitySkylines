﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class TexturePicker : UICustomControl
{
	public Texture selectedTexture
	{
		get
		{
			return this.m_SelectedTexture;
		}
	}

	public Image selectedImage
	{
		get
		{
			return this.m_SelectedImage;
		}
	}

	public static string targetPath
	{
		get
		{
			if (TexturePicker.m_TargetPath == null)
			{
				TexturePicker.m_TargetPath = Path.Combine(Path.Combine(DataLocation.get_addonsPath(), "ThemeEditor"), "TerrainTextures");
			}
			if (!Directory.Exists(TexturePicker.m_TargetPath))
			{
				Directory.CreateDirectory(TexturePicker.m_TargetPath);
			}
			return TexturePicker.m_TargetPath;
		}
	}

	public void OpenFolder()
	{
		Utils.OpenInFileBrowser(TexturePicker.m_TargetPath);
	}

	private void SetPath(string path)
	{
		TexturePicker.m_TargetPath = path;
		if (!Directory.Exists(TexturePicker.m_TargetPath))
		{
			Directory.CreateDirectory(TexturePicker.m_TargetPath);
		}
		this.CleanUpFileWatchers();
		for (int i = 0; i < TexturePicker.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + TexturePicker.m_Extensions[i], TexturePicker.m_TargetPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
		}
	}

	public void SetTarget(UITextureSprite preview, TexturePicker.Type t)
	{
		if (t == TexturePicker.Type.ThemeTextures)
		{
			this.SetPath(Path.Combine(Path.Combine(DataLocation.get_addonsPath(), "ThemeEditor"), "ThemeTextures"));
		}
	}

	private void CleanUpFileWatchers()
	{
		for (int i = 0; i < TexturePicker.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void OnDestroy()
	{
		this.CleanUpFileWatchers();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.m_FileList.Focus();
		}
	}

	public void ShowModal(UITextureSprite s, TexturePicker.Type t, UIView.ModalPoppedReturnCallback c)
	{
		this.SetTarget(s, t);
		UIView.get_library().ShowModal(base.GetType().Name, c);
	}

	private void OnFileListVisibilityChanged(UIComponent comp, bool visible)
	{
		if (comp == this.m_FileList)
		{
			if (visible)
			{
				this.Refresh();
				for (int i = 0; i < TexturePicker.m_Extensions.Length; i++)
				{
					if (this.m_FileSystemReporter[i] != null)
					{
						this.m_FileSystemReporter[i].Start();
					}
				}
			}
			else
			{
				for (int j = 0; j < TexturePicker.m_Extensions.Length; j++)
				{
					if (this.m_FileSystemReporter[j] != null)
					{
						this.m_FileSystemReporter[j].Stop();
					}
				}
			}
		}
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			this.Refresh();
		});
	}

	private void Awake()
	{
		this.m_FileList = base.Find<UIListBox>("FileList");
		this.m_FileList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnSelectionChanged));
		this.m_SelectButton = base.Find<UIButton>("Select");
		this.m_SelectButton.add_eventClick(new MouseEventHandler(this.OnSelect));
		this.m_CancelButton = base.Find<UIButton>("Cancel");
		this.m_CancelButton.add_eventClick(new MouseEventHandler(this.OnCancel));
		this.m_PreviewSprite = base.Find<UITextureSprite>("Preview");
		this.m_ErrorLabel = base.Find<UILabel>("ErrorLabel");
		this.m_LoadingLabel = base.Find<UILabel>("LoadingLabel");
		this.m_LoadingProgress = this.m_LoadingLabel.Find<UISprite>("ProgressSprite").get_transform();
		this.m_FileList.add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnFileListVisibilityChanged));
	}

	private void Select()
	{
		if (this.m_SelectButton.get_isEnabled())
		{
			this.m_PreviewSprite.set_texture(null);
			UIView.get_library().Hide(base.GetType().Name, 1);
		}
	}

	private void OnSelect(UIComponent c, UIMouseEventParameter p)
	{
		this.Select();
	}

	private void Cancel()
	{
		Object.DestroyImmediate(this.m_PreviewSprite.get_texture());
		UIView.get_library().Hide(base.GetType().Name, 0);
	}

	private void OnCancel(UIComponent c, UIMouseEventParameter p)
	{
		this.Cancel();
	}

	private void Start()
	{
		base.get_component().Hide();
		this.m_ErrorLabel.set_isVisible(false);
		this.m_LoadingLabel.Hide();
		this.m_SelectButton.set_isEnabled(false);
		this.Refresh();
	}

	protected void Sort(FileInfo[] files, TexturePicker.SortingType type)
	{
		if (type == TexturePicker.SortingType.Timestamp)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => b.LastWriteTime.CompareTo(a.LastWriteTime));
		}
		else if (type == TexturePicker.SortingType.Name)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => a.Name.CompareTo(b.Name));
		}
		else if (type == TexturePicker.SortingType.Extension)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => a.Extension.CompareTo(b.Extension));
		}
	}

	private void Refresh()
	{
		if (TexturePicker.m_TargetPath == null)
		{
			return;
		}
		base.get_component().Focus();
		List<string> list = new List<string>();
		DirectoryInfo directoryInfo = new DirectoryInfo(TexturePicker.m_TargetPath);
		if (directoryInfo.Exists)
		{
			FileInfo[] array = null;
			try
			{
				array = directoryInfo.GetFiles();
				this.Sort(array, TexturePicker.SortingType.Timestamp);
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			if (array != null)
			{
				FileInfo[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					FileInfo fileInfo = array2[i];
					for (int j = 0; j < TexturePicker.m_Extensions.Length; j++)
					{
						if (string.Compare(Path.GetExtension(fileInfo.Name), TexturePicker.m_Extensions[j]) == 0)
						{
							list.Add(fileInfo.Name);
						}
					}
				}
			}
			this.m_FileList.set_items(list.ToArray());
			if (list.Count > 0)
			{
				this.m_FileList.set_selectedIndex(0);
				this.m_SelectButton.set_isEnabled(true);
			}
			else
			{
				this.m_SelectButton.set_isEnabled(false);
			}
		}
	}

	private void AssignPreview(Image image)
	{
		this.m_FileList.set_isEnabled(true);
		if (this.m_PreviewSprite.get_texture() != null)
		{
			Object.DestroyImmediate(this.m_PreviewSprite.get_texture());
		}
		this.m_SelectedImage = image;
		image.Convert(4);
		this.m_SelectedTexture = image.CreateTexture();
		this.m_PreviewSprite.set_texture(this.m_SelectedTexture);
		this.m_PreviewSprite.get_texture().set_wrapMode(0);
		ValueAnimator.Animate("LoadingHeightmap", delegate(float val)
		{
			this.m_PreviewSprite.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f, 7));
		if (this.m_SelectedTexture != null)
		{
			this.m_SelectButton.set_isEnabled(true);
		}
		this.m_LoadingLabel.Hide();
	}

	private bool IsPowerOfTwo(int i)
	{
		return (i & i - 1) == 0;
	}

	private int NextPowerOfTwo(int i)
	{
		i--;
		i |= i >> 1;
		i |= i >> 2;
		i |= i >> 4;
		i |= i >> 8;
		i |= i >> 16;
		i++;
		return i;
	}

	private void LoadImageThreaded(string path)
	{
		Stopwatch stopwatch = new Stopwatch();
		try
		{
			stopwatch.Start();
			Image image = new Image(path);
			if (!this.IsPowerOfTwo(image.get_width()) || !this.IsPowerOfTwo(image.get_height()))
			{
				image.Resize(this.NextPowerOfTwo(image.get_width() - 1), this.NextPowerOfTwo(image.get_height() - 1));
			}
			image.Convert(4);
			stopwatch.Stop();
			if (stopwatch.ElapsedMilliseconds < 200L)
			{
				Thread.Sleep(200 - (int)stopwatch.ElapsedMilliseconds);
			}
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.AssignPreview(image);
			});
		}
		catch (Exception ex)
		{
			stopwatch.Stop();
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				"[",
				path,
				"]\n",
				Environment.StackTrace
			}));
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.ShowError(path);
			});
		}
	}

	private void ShowError(string path)
	{
		this.m_LoadingLabel.Hide();
		this.m_ErrorLabel.Show();
		this.m_ErrorLabel.set_text(StringUtils.SafeFormat(Locale.Get("TEXTUREPICKER_LOADERROR"), Path.GetFileName(path)));
		ValueAnimator.Animate("ErrorLoadingHeightmap", delegate(float val)
		{
			this.m_ErrorLabel.set_opacity(val);
		}, new AnimatedFloat(0f, 1f, 0.2f, 7));
	}

	public void LoadSelected()
	{
		this.m_SelectButton.set_isEnabled(false);
		this.m_FileList.set_isEnabled(false);
		ValueAnimator.Animate("ErrorLoadingHeightmap", delegate(float val)
		{
			this.m_ErrorLabel.set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.2f, 7), delegate
		{
			this.m_ErrorLabel.Hide();
		});
		ValueAnimator.Animate("LoadingHeightmap", delegate(float val)
		{
			this.m_PreviewSprite.set_opacity(val);
		}, new AnimatedFloat(1f, 0f, 0.2f, 7), delegate
		{
			this.m_PreviewSprite.set_texture(null);
		});
		if (this.m_FileList.get_selectedIndex() >= 0 && this.m_FileList.get_selectedIndex() < this.m_FileList.get_items().Length)
		{
			string path = Path.Combine(TexturePicker.m_TargetPath, this.m_FileList.get_items()[this.m_FileList.get_selectedIndex()]);
			this.m_LoadingLabel.Show();
			ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				this.LoadImageThreaded(path);
			});
		}
	}

	public void OnSelectionChanged(UIComponent comp, int index)
	{
		if (base.get_component().get_isVisible())
		{
			this.LoadSelected();
		}
	}

	private void Update()
	{
		if (this.m_LoadingLabel.get_isVisible())
		{
			this.m_LoadingProgress.Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.Cancel();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.Select();
				p.Use();
			}
		}
	}

	public float m_RotationSpeed = 280f;

	private UILabel m_LoadingLabel;

	private UIListBox m_FileList;

	private UIButton m_SelectButton;

	private UIButton m_CancelButton;

	private UITextureSprite m_PreviewSprite;

	private UILabel m_ErrorLabel;

	private Transform m_LoadingProgress;

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[TexturePicker.m_Extensions.Length];

	private static readonly string[] m_Extensions = Image.GetExtensions(94);

	private Texture m_SelectedTexture;

	private Image m_SelectedImage;

	private static string m_TargetPath;

	protected enum SortingType
	{
		Name,
		Timestamp,
		Extension
	}

	public enum Type
	{
		ThemeTextures
	}
}
