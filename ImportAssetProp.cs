﻿using System;
using UnityEngine;

public class ImportAssetProp : ImportAssetLodded
{
	public ImportAssetProp(GameObject template, PreviewCamera camera) : base(template, camera)
	{
		this.m_LodTriangleTarget = 70;
	}

	protected override void InitializeObject()
	{
		PropInfo component = this.m_Object.GetComponent<PropInfo>();
		component.InitializePrefab();
		component.CheckReferences();
		component.m_prefabInitialized = true;
		this.m_Object.set_layer(LayerMask.NameToLayer("Props"));
	}

	protected override void CreateInfo()
	{
		PropInfo component = this.m_TemplateObject.GetComponent<PropInfo>();
		PropInfo propInfo = ImportAsset.CopyComponent<PropInfo>(component, this.m_Object);
		propInfo.m_generatedInfo = ScriptableObject.CreateInstance<PropInfoGen>();
		propInfo.m_generatedInfo.set_name(this.m_Object.get_name() + " (GeneratedInfo)");
		propInfo.m_lodObject = this.m_LODObject;
		propInfo.m_Atlas = null;
		propInfo.m_Thumbnail = string.Empty;
		propInfo.CalculateGeneratedInfo();
		propInfo.m_placementStyle = ItemClass.Placement.Manual;
	}

	protected override void FinalizeLOD()
	{
		base.FinalizeLOD();
		this.CreateInfo();
	}
}
