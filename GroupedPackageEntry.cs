﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class GroupedPackageEntry : PackageEntry
{
	public override Package.Asset asset
	{
		get
		{
			return (this.m_EntryData == null) ? null : this.m_EntryData.asset;
		}
		set
		{
			this.m_NameField.set_isEnabled(false);
			if (this.asset != null)
			{
				DistrictStyleMetaData districtStyleMetaData = this.asset.Instantiate<DistrictStyleMetaData>();
				this.m_NameField.set_isEnabled(!districtStyleMetaData.builtin && !this.asset.get_isWorkshopAsset());
				if (!districtStyleMetaData.builtin)
				{
					this.m_NameField.set_text(this.asset.get_name());
				}
				this.m_NamePackage.set_text(Path.GetFileName(base.package.get_packagePath()));
				this.StopWatchingPath();
				this.m_StagingPath = Path.Combine(Path.Combine(DataLocation.get_localApplicationData(), "StylesStagingArea"), base.package.get_packagePath().GetHashCode().ToString());
				this.m_PreviewPath = Path.Combine(this.m_StagingPath, "PreviewImage.png");
				if (Directory.Exists(this.m_StagingPath))
				{
					this.StartWatchingPath();
				}
			}
		}
	}

	public override void SetNameLabel(string entryName, string authorName)
	{
		if (!string.IsNullOrEmpty(authorName))
		{
			this.m_AuthorNameLabel.Show();
			this.m_AuthorNameLabel.set_text("by " + authorName);
		}
		else
		{
			this.m_AuthorNameLabel.Hide();
		}
	}

	public override void Reset()
	{
		base.Reset();
		this.StopWatchingPath();
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_FolderSelect = base.Find<UIButton>("FolderSelect");
		this.m_FolderSelect.add_eventClick(new MouseEventHandler(this.OnFolderSelect));
		this.m_AuthorNameLabel = base.Find<UILabel>("AuthorName");
		this.m_AuthorNameLabel.Hide();
		this.m_NamePackage = base.Find<UILabel>("Name2");
		this.m_NameField = base.Find<UITextField>("Name");
		if (this.m_NameField != null)
		{
			this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnNameChanged));
		}
		this.m_ExpandButton = base.Find<UIButton>("Expand");
		if (this.m_ExpandButton != null)
		{
			this.m_ExpandButton.add_eventClick(new MouseEventHandler(this.OnExpand));
		}
		this.m_SubscribeAllButton = base.Find<UIButton>("SubscribeAll");
		if (this.m_SubscribeAllButton != null)
		{
			this.m_SubscribeAllButton.add_eventClick(new MouseEventHandler(this.OnSubscribeAll));
		}
		this.m_MainPanel = base.Find<UIPanel>("MainPanel");
		if (this.m_MainPanel != null)
		{
			base.get_component().set_height(this.m_MainPanel.get_height());
		}
		this.m_Helper = base.Find<UITextureSprite>("Helper");
	}

	private void PrepareStagingArea(Texture2D previewTexture)
	{
		Directory.CreateDirectory(this.m_StagingPath);
		File.WriteAllBytes(this.m_PreviewPath, previewTexture.EncodeToPNG());
		this.StopWatchingPath();
		this.StartWatchingPath();
	}

	private void StartWatchingPath()
	{
		for (int i = 0; i < GroupedPackageEntry.m_Extensions.Length; i++)
		{
			this.m_FileSystemReporter[i] = new FileSystemReporter("*" + GroupedPackageEntry.m_Extensions[i], this.m_StagingPath, new FileSystemReporter.ReporterEventHandler(this.Refresh));
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Start();
			}
		}
	}

	private void ResumeWatchingPath()
	{
		for (int i = 0; i < GroupedPackageEntry.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Start();
			}
		}
	}

	private void SuspendWatchingPath()
	{
		for (int i = 0; i < GroupedPackageEntry.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Stop();
			}
		}
	}

	private void StopWatchingPath()
	{
		for (int i = 0; i < GroupedPackageEntry.m_Extensions.Length; i++)
		{
			if (this.m_FileSystemReporter[i] != null)
			{
				this.m_FileSystemReporter[i].Stop();
				this.m_FileSystemReporter[i].Dispose();
				this.m_FileSystemReporter[i] = null;
			}
		}
	}

	private void ReloadSavePackage()
	{
		DistrictStyleMetaData districtStyleMetaData = this.asset.Instantiate<DistrictStyleMetaData>();
		if (districtStyleMetaData != null)
		{
			StylesHelper.SaveStyle(districtStyleMetaData, this.asset.get_name(), false, this.m_PreviewPath);
		}
		this.ResumeWatchingPath();
	}

	private void Refresh(object sender, ReporterEventArgs e)
	{
		string fullPath = ((FileSystemEventArgs)e.get_arguments()).FullPath;
		if (string.Compare(Path.GetFileName(fullPath), "PreviewImage.png", StringComparison.OrdinalIgnoreCase) == 0)
		{
			this.SuspendWatchingPath();
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				this.ReloadSavePackage();
			});
		}
	}

	public void OnFolderSelect(UIComponent component, UIMouseEventParameter p)
	{
		DistrictStyleMetaData districtStyleMetaData = this.asset.Instantiate<DistrictStyleMetaData>();
		if (districtStyleMetaData != null)
		{
			Texture2D previewTexture;
			if (districtStyleMetaData.steamPreviewRef != null)
			{
				previewTexture = districtStyleMetaData.steamPreviewRef.Instantiate<Texture2D>();
			}
			else if (districtStyleMetaData.imageRef != null)
			{
				previewTexture = districtStyleMetaData.imageRef.Instantiate<Texture2D>();
			}
			else
			{
				previewTexture = (Object.Instantiate<Texture>(this.m_DefaultStylePreview) as Texture2D);
			}
			this.PrepareStagingArea(previewTexture);
			Utils.OpenInFileBrowser(this.m_StagingPath);
		}
	}

	public void OnNameChanged(UIComponent c, string text)
	{
		if (StringExtensions.IsNullOrWhiteSpace(text))
		{
			this.m_NameField.Cancel();
			return;
		}
		if (this.m_NameField.get_isEnabled() && !this.asset.get_name().Equals(text))
		{
			StylesHelper.RenameStyle(this.asset, text);
		}
	}

	private void OnExpand(UIComponent c, UIMouseEventParameter p)
	{
		GroupedPackageEntry.ExpandSubAssets(this.asset);
	}

	private void OnSubscribeAll(UIComponent c, UIMouseEventParameter p)
	{
		DistrictStyleMetaData districtStyleMetaData = this.asset.Instantiate<DistrictStyleMetaData>();
		if (this.m_SubscribeAllButton != null)
		{
			this.m_SubscribeAllButton.set_isEnabled(false);
		}
		for (int i = 0; i < districtStyleMetaData.assets.Length; i++)
		{
			int num = districtStyleMetaData.assets[i].IndexOf(".");
			if (num > 0 && num < districtStyleMetaData.assets[i].Length)
			{
				string s = districtStyleMetaData.assets[i].Substring(0, num);
				ulong num2;
				if (ulong.TryParse(s, out num2))
				{
					PublishedFileId publishedFileId;
					publishedFileId..ctor(num2);
					if (publishedFileId != PublishedFileId.invalid)
					{
						PlatformService.get_workshop().Subscribe(publishedFileId);
					}
				}
			}
		}
	}

	public static void ExpandSubAssets(Package.Asset asset)
	{
		GroupedPackageEntry.m_SubAssetContainer = UIView.Find<UIScrollablePanel>("SubAssetContent");
		if (asset != null && asset.get_type() == UserAssetType.DistrictStyleMetaData)
		{
			DistrictStyleMetaData districtStyleMetaData = asset.Instantiate<DistrictStyleMetaData>();
			if (districtStyleMetaData != null)
			{
				int i;
				for (i = 0; i < districtStyleMetaData.assets.Length; i++)
				{
					StyleAssetEntry styleAssetEntry;
					if (i >= GroupedPackageEntry.m_SubAssetContainer.get_components().Count)
					{
						styleAssetEntry = UITemplateManager.Get<StyleAssetEntry>(GroupedPackageEntry.kStyleAssetEntryTemplate);
						GroupedPackageEntry.m_SubAssetContainer.AttachUIComponent(styleAssetEntry.get_gameObject());
					}
					else
					{
						styleAssetEntry = GroupedPackageEntry.m_SubAssetContainer.get_components()[i].GetComponent<StyleAssetEntry>();
					}
					styleAssetEntry.entryName = districtStyleMetaData.assets[i];
					styleAssetEntry.asset = asset;
					Package.Asset asset2 = PackageManager.FindAssetByName(districtStyleMetaData.assets[i]);
					if (asset2 != null)
					{
						CustomAssetMetaData customAssetMetaData = asset2.Instantiate<CustomAssetMetaData>();
						if (customAssetMetaData != null && customAssetMetaData.imageRef != null)
						{
							styleAssetEntry.image = customAssetMetaData.imageRef.Instantiate<Texture2D>();
						}
					}
					string text = districtStyleMetaData.assets[i];
					int num = text.IndexOf(".");
					if (num != -1)
					{
						string s = text.Substring(0, num);
						ulong num2;
						styleAssetEntry.publishedFileId = ((!ulong.TryParse(s, out num2)) ? PublishedFileId.invalid : new PublishedFileId(num2));
					}
					else
					{
						styleAssetEntry.publishedFileId = PublishedFileId.invalid;
					}
					styleAssetEntry.get_component().set_size(new Vector2((GroupedPackageEntry.m_SubAssetContainer.get_size().x - 1f) / 2f, styleAssetEntry.get_component().get_size().y));
				}
				while (GroupedPackageEntry.m_SubAssetContainer.get_components().Count > i)
				{
					UIComponent uIComponent = GroupedPackageEntry.m_SubAssetContainer.get_components()[i];
					GroupedPackageEntry.m_SubAssetContainer.RemoveUIComponent(uIComponent);
					Object.Destroy(uIComponent.get_gameObject());
				}
				if (i > 0)
				{
					UIComponent parent = GroupedPackageEntry.m_SubAssetContainer.get_parent();
					UIComponent arg_228_0 = parent.Find<UIButton>("Back");
					if (GroupedPackageEntry.<>f__mg$cache0 == null)
					{
						GroupedPackageEntry.<>f__mg$cache0 = new MouseEventHandler(GroupedPackageEntry.OnSubAssetPanelClosed);
					}
					arg_228_0.remove_eventClicked(GroupedPackageEntry.<>f__mg$cache0);
					UIComponent arg_256_0 = parent.Find<UIButton>("Back");
					if (GroupedPackageEntry.<>f__mg$cache1 == null)
					{
						GroupedPackageEntry.<>f__mg$cache1 = new MouseEventHandler(GroupedPackageEntry.OnSubAssetPanelClosed);
					}
					arg_256_0.add_eventClicked(GroupedPackageEntry.<>f__mg$cache1);
					parent.Show(true);
				}
			}
		}
	}

	private static void OnSubAssetPanelClosed(UIComponent c, UIMouseEventParameter p)
	{
		UIComponent arg_31_0 = GroupedPackageEntry.m_SubAssetContainer.get_parent().Find<UIButton>("Back");
		if (GroupedPackageEntry.<>f__mg$cache2 == null)
		{
			GroupedPackageEntry.<>f__mg$cache2 = new MouseEventHandler(GroupedPackageEntry.OnSubAssetPanelClosed);
		}
		arg_31_0.remove_eventClicked(GroupedPackageEntry.<>f__mg$cache2);
		GroupedPackageEntry.m_SubAssetContainer.get_parent().Hide();
	}

	protected override void OnEntryDataChanged()
	{
		base.OnEntryDataChanged();
		if (this.asset != null && this.asset.get_type() == UserAssetType.DistrictStyleMetaData)
		{
			DistrictStyleMetaData districtStyleMetaData = this.asset.Instantiate<DistrictStyleMetaData>();
			if (districtStyleMetaData != null)
			{
				if (this.m_NameField != null)
				{
					this.m_NameField.set_isEnabled(!districtStyleMetaData.builtin && !this.asset.get_isWorkshopAsset());
					this.m_NamePackage.set_isVisible(true);
					if (districtStyleMetaData.builtin)
					{
						if (districtStyleMetaData.name.Equals(DistrictStyle.kEuropeanStyleName))
						{
							this.m_NameField.set_text(Locale.Get("STYLES_EUROPEAN_NORMAL"));
							this.m_NamePackage.set_isVisible(false);
						}
						else if (districtStyleMetaData.name.Equals(DistrictStyle.kEuropeanSuburbiaStyleName))
						{
							this.m_NameField.set_text(Locale.Get("STYLES_EUROPEANSUBURBIA"));
							this.m_NamePackage.set_isVisible(false);
						}
					}
				}
				if (this.m_Image != null)
				{
					if (this.m_Image.get_texture() != null)
					{
						Object.Destroy(this.m_Image.get_texture());
					}
					if (districtStyleMetaData.imageRef != null)
					{
						this.m_Image.set_texture(districtStyleMetaData.imageRef.Instantiate<Texture2D>());
					}
					else if (districtStyleMetaData.steamPreviewRef != null)
					{
						this.m_Image.set_texture(districtStyleMetaData.steamPreviewRef.Instantiate<Texture2D>());
					}
					else
					{
						this.m_Image.set_texture(Object.Instantiate<Texture>(this.m_DefaultStylePreview) as Texture2D);
					}
					if (this.m_Image.get_texture() != null)
					{
						this.m_Image.get_texture().set_wrapMode(1);
					}
				}
				if (this.m_DeleteButton != null)
				{
					this.m_DeleteButton.set_tooltipLocaleID((!(base.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UNSUBSCRIBE" : "CONTENT_DELETE");
					UIComponent arg_231_0 = this.m_DeleteButton;
					bool flag = !districtStyleMetaData.builtin;
					this.m_DeleteButton.set_isVisible(flag);
					arg_231_0.set_isEnabled(flag);
				}
				if (this.m_ViewButton != null)
				{
					UIComponent arg_26B_0 = this.m_ViewButton;
					bool flag = base.publishedFileId != PublishedFileId.invalid;
					this.m_ViewButton.set_isVisible(flag);
					arg_26B_0.set_isEnabled(flag);
					this.m_FolderSelect.set_isVisible(!this.m_ViewButton.get_isEnabled());
				}
				if (this.m_ShareButton != null)
				{
					this.m_ShareButton.set_isEnabled(!districtStyleMetaData.builtin);
					this.m_ShareButton.set_localeID((!(base.publishedFileId == PublishedFileId.invalid)) ? "CONTENT_UPDATE" : "CONTENT_SHARE");
					if (!districtStyleMetaData.builtin)
					{
						if (base.publishedFileId == PublishedFileId.invalid)
						{
							this.m_ShareButton.Show();
						}
						else if (this.m_EntryData.detailsPending)
						{
							this.m_ShareButton.Hide();
						}
						else
						{
							this.m_ShareButton.set_isVisible(PlatformService.get_userID() == this.m_WorkshopDetails.creatorID);
						}
					}
					else
					{
						UIComponent arg_367_0 = this.m_ShareButton;
						bool flag = false;
						this.m_ShareButton.set_isVisible(flag);
						arg_367_0.set_isEnabled(flag);
					}
				}
				if (this.m_ExpandButton != null)
				{
					this.m_ExpandButton.set_isVisible(districtStyleMetaData.assets.Length > 0 && !districtStyleMetaData.builtin);
				}
				bool flag2 = false;
				bool flag3 = false;
				HashSet<int> hashSet = new HashSet<int>();
				Dictionary<int, int> dictionary = new Dictionary<int, int>();
				for (int i = 0; i < districtStyleMetaData.assets.Length; i++)
				{
					Package.Asset asset = PackageManager.FindAssetByName(districtStyleMetaData.assets[i]);
					CustomAssetMetaData customAssetMetaData = (!(asset != null)) ? null : asset.Instantiate<CustomAssetMetaData>();
					if (customAssetMetaData != null)
					{
						if (customAssetMetaData.width < 1)
						{
							flag2 = true;
						}
						else if (!flag2)
						{
							int serviceLevelIndex = DistrictStyle.GetServiceLevelIndex(customAssetMetaData.service, customAssetMetaData.subService, customAssetMetaData.level);
							hashSet.Add(serviceLevelIndex);
							int num = serviceLevelIndex * 4 + (customAssetMetaData.width - 1);
							if (dictionary.ContainsKey(num))
							{
								Dictionary<int, int> dictionary2;
								int key;
								(dictionary2 = dictionary)[key = num] = dictionary2[key] + 1;
							}
							else
							{
								dictionary[num] = 1;
							}
						}
					}
					else
					{
						int num2 = districtStyleMetaData.assets[i].IndexOf(".");
						if (num2 > 0 && num2 < districtStyleMetaData.assets[i].Length)
						{
							string s = districtStyleMetaData.assets[i].Substring(0, num2);
							ulong num3;
							if (ulong.TryParse(s, out num3))
							{
								flag3 = true;
							}
						}
					}
				}
				if (this.m_Helper != null)
				{
					if (!flag2 && hashSet.Count > 0)
					{
						string text = Locale.Get("CONTENTMANAGER_STYLEREPLACES") + "\n";
						foreach (int current in hashSet)
						{
							ItemClass.Service service;
							ItemClass.SubService subService;
							ItemClass.Level level;
							DistrictStyle.InterpretServiceLevelIndex(current, out service, out subService, out level);
							text = text + DistrictStyle.ServiceLevelToLocalizedString(service, subService, level) + "\n";
						}
						this.m_Helper.set_isVisible(true);
						this.m_Helper.set_tooltip(text);
					}
					else if (flag2)
					{
						this.m_Helper.set_isVisible(true);
						this.m_Helper.set_tooltip(Locale.Get("CONTENTMANAGER_STYLELEGACY"));
					}
					else
					{
						this.m_Helper.set_isVisible(false);
					}
				}
				if (this.m_SubscribeAllButton != null)
				{
					this.m_SubscribeAllButton.set_isEnabled(PlatformService.get_active() && flag3);
					this.m_SubscribeAllButton.set_isVisible(!districtStyleMetaData.builtin);
				}
			}
		}
	}

	public Texture m_DefaultStylePreview;

	private const string kPreviewImage = "PreviewImage.png";

	private static readonly string kStyleAssetEntryTemplate = "StyleAssetEntryTemplate";

	private UIPanel m_MainPanel;

	private UITextField m_NameField;

	private UILabel m_NamePackage;

	private UILabel m_AuthorNameLabel;

	private UIButton m_ExpandButton;

	private UIButton m_SubscribeAllButton;

	private static UIScrollablePanel m_SubAssetContainer;

	private UITextureSprite m_Helper;

	private UIButton m_FolderSelect;

	private string m_StagingPath;

	private string m_PreviewPath;

	private static readonly string[] m_Extensions = Image.GetExtensions(4);

	private FileSystemReporter[] m_FileSystemReporter = new FileSystemReporter[GroupedPackageEntry.m_Extensions.Length];

	[CompilerGenerated]
	private static MouseEventHandler <>f__mg$cache0;

	[CompilerGenerated]
	private static MouseEventHandler <>f__mg$cache1;

	[CompilerGenerated]
	private static MouseEventHandler <>f__mg$cache2;
}
