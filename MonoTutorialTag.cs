﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using UnityEngine;

public abstract class MonoTutorialTag : MonoBehaviour, IUITag
{
	public virtual bool isDynamic
	{
		get
		{
			return false;
		}
	}

	public bool isValid
	{
		get
		{
			return true;
		}
	}

	public bool isRelative
	{
		get
		{
			return false;
		}
	}

	public bool locationLess
	{
		get
		{
			return this.m_LocationLess;
		}
	}

	public string tutorialTag
	{
		get
		{
			return this.m_Tag;
		}
		set
		{
			if (!string.IsNullOrEmpty(value))
			{
				MonoTutorialTag.m_TutorialTags[value] = this;
			}
			else if (!string.IsNullOrEmpty(this.m_Tag))
			{
				MonoTutorialTag.m_TutorialTags.Remove(this.m_Tag);
			}
			this.m_Tag = value;
		}
	}

	public abstract Vector3 position
	{
		get;
	}

	protected virtual void OnEnable()
	{
		if (!string.IsNullOrEmpty(this.m_Tag))
		{
			MonoTutorialTag.m_TutorialTags[this.m_Tag] = this;
		}
	}

	protected virtual void OnDisable()
	{
		if (!string.IsNullOrEmpty(this.m_Tag))
		{
			MonoTutorialTag.m_TutorialTags.Remove(this.m_Tag);
		}
	}

	public static IUITag Find(string tag)
	{
		IUITag result;
		if (MonoTutorialTag.m_TutorialTags.TryGetValue(tag, out result))
		{
			return result;
		}
		CODebugBase<LogChannel>.Warn(LogChannel.Core, "Tutorial tag missing: " + tag);
		return null;
	}

	public string m_Tag;

	public bool m_LocationLess;

	private static Dictionary<string, IUITag> m_TutorialTags = new Dictionary<string, IUITag>();
}
