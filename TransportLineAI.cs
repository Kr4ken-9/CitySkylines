﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TransportLineAI : NetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Transport;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		data.m_flags |= NetSegment.Flags.PathFailed;
	}

	public override void CreateNode(ushort nodeID, ref NetNode data)
	{
		base.CreateNode(nodeID, ref data);
	}

	public override void ReleaseNode(ushort nodeID, ref NetNode data)
	{
		if (data.m_lane != 0u)
		{
			this.RemoveLaneConnection(nodeID, ref data);
		}
		base.ReleaseNode(nodeID, ref data);
	}

	public override void GetTransportAccumulation(out int publicTransportAccumulation, out float publicTransportRadius)
	{
		publicTransportAccumulation = this.m_publicTransportAccumulation;
		publicTransportRadius = this.m_publicTransportRadius;
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None)
		{
			if (data.m_path == 0u || (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(segmentID & 15)))
			{
				TransportLineAI.StartPathFind(segmentID, ref data, this.m_netService, this.m_secondaryNetService, this.m_vehicleType, false);
			}
			else
			{
				TransportLineAI.UpdatePath(segmentID, ref data, this.m_netService, this.m_secondaryNetService, this.m_vehicleType, false);
			}
		}
		ushort transportLine = instance.m_nodes.m_buffer[(int)data.m_endNode].m_transportLine;
		if (transportLine != 0)
		{
			TransportManager instance2 = Singleton<TransportManager>.get_instance();
			if (instance2.m_lines.m_buffer[(int)transportLine].m_vehicles != 0)
			{
				data.m_trafficLightState0 = (byte)Mathf.Min(255, (int)(data.m_trafficLightState0 + 1));
			}
		}
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			data.m_finalCounter = data.m_tempCounter;
			data.m_tempCounter = 0;
		}
		if (this.m_publicTransportAccumulation != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.PathFailed) == NetSegment.Flags.None)
				{
					num += this.m_publicTransportAccumulation >> 1;
				}
			}
			if (num != 0)
			{
				int num2 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
				if (data.m_transportLine != 0)
				{
					int budget = (int)Singleton<TransportManager>.get_instance().m_lines.m_buffer[(int)data.m_transportLine].m_budget;
					num2 = (num2 * budget + 50) / 100;
				}
				int productionRate = PlayerBuildingAI.GetProductionRate(100, num2);
				num = num * productionRate / 100;
			}
			if (num != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.PublicTransport, num, data.m_position, this.m_publicTransportRadius);
			}
		}
		if ((data.m_problems & Notification.Problem.LineNotConnected) != Notification.Problem.None && data.CountSegments() <= 1 && (data.m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<NetManager>.get_instance().m_transportNodeNotConnected.Activate(properties.m_lineNotFinished, nodeID, Notification.Problem.LineNotConnected, false);
			}
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
		TransportLineAI.CheckNodeProblems(nodeID, ref data);
	}

	private static void CheckSegmentProblems(ushort segmentID, ref NetSegment data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Notification.Problem problems = instance.m_nodes.m_buffer[(int)data.m_startNode].m_problems;
		TransportLineAI.CheckNodeProblems(data.m_startNode, ref instance.m_nodes.m_buffer[(int)data.m_startNode]);
		Notification.Problem problems2 = instance.m_nodes.m_buffer[(int)data.m_startNode].m_problems;
		if (problems != problems2)
		{
			instance.UpdateNodeNotifications(data.m_startNode, problems, problems2);
		}
		Notification.Problem problems3 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_problems;
		TransportLineAI.CheckNodeProblems(data.m_endNode, ref instance.m_nodes.m_buffer[(int)data.m_endNode]);
		Notification.Problem problems4 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_problems;
		if (problems3 != problems4)
		{
			instance.UpdateNodeNotifications(data.m_endNode, problems3, problems4);
		}
	}

	private static void CheckNodeProblems(ushort nodeID, ref NetNode data)
	{
		if (data.m_transportLine != 0)
		{
			bool flag = false;
			if ((data.m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				int num = 0;
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0)
					{
						num++;
						if (instance.m_segments.m_buffer[(int)segment].m_path == 0u || (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.PathFailed) != NetSegment.Flags.None)
						{
							flag = true;
						}
					}
				}
				if (num <= 1)
				{
					flag = true;
				}
			}
			if (flag)
			{
				data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.LineNotConnected);
			}
			else
			{
				data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.LineNotConnected);
			}
		}
	}

	public override void UpdateLanes(ushort segmentID, ref NetSegment data, bool loading)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint num = 0u;
		uint num2 = data.m_lanes;
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector = Vector3.Lerp(position, position2, 0.333333343f);
		Vector3 vector2 = Vector3.Lerp(position, position2, 0.6666667f);
		if (data.m_averageLength == 0f)
		{
			data.m_averageLength = Vector3.Distance(position, position2);
			data.m_flags &= ~NetSegment.Flags.PathLength;
		}
		float costMultiplier = TransportLineAI.GetCostMultiplier(this.m_vehicleType, position);
		float length = data.m_averageLength * costMultiplier;
		for (int i = 0; i < this.m_info.m_lanes.Length; i++)
		{
			if (num2 == 0u)
			{
				if (!Singleton<NetManager>.get_instance().CreateLanes(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, segmentID, 1))
				{
					break;
				}
				if (num != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane = num2;
				}
				else
				{
					data.m_lanes = num2;
				}
			}
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_bezier = new Bezier3(position, vector, vector2, position2);
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_segment = segmentID;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_firstTarget = 0;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_lastTarget = 255;
			instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_length = length;
			num = num2;
			num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
		}
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None && !loading && data.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
			data.m_path = 0u;
		}
	}

	public static bool StartPathFind(ushort segmentID, ref NetSegment data, ItemClass.Service netService, ItemClass.Service netService2, VehicleInfo.VehicleType vehicleType, bool skipQueue)
	{
		if (data.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
			data.m_path = 0u;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Ambiguous) != NetNode.Flags.None)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)data.m_startNode].GetSegment(i);
				if (segment != 0 && segment != segmentID && instance.m_segments.m_buffer[(int)segment].m_path != 0u)
				{
					return true;
				}
			}
		}
		if ((instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags & NetNode.Flags.Ambiguous) != NetNode.Flags.None)
		{
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)data.m_endNode].GetSegment(j);
				if (segment2 != 0 && segment2 != segmentID && instance.m_segments.m_buffer[(int)segment2].m_path != 0u)
				{
					return true;
				}
			}
		}
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		if (!PathManager.FindPathPosition(position, netService, netService2, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, vehicleType, true, false, 32f, out startPosA, out startPosB, out num, out num2))
		{
			TransportLineAI.CheckSegmentProblems(segmentID, ref data);
			return true;
		}
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (!PathManager.FindPathPosition(position2, netService, netService2, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, vehicleType, true, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			TransportLineAI.CheckSegmentProblems(segmentID, ref data);
			return true;
		}
		if ((instance.m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Fixed) != NetNode.Flags.None)
		{
			startPosB = default(PathUnit.Position);
		}
		if ((instance.m_nodes.m_buffer[(int)data.m_endNode].m_flags & NetNode.Flags.Fixed) != NetNode.Flags.None)
		{
			endPosB = default(PathUnit.Position);
		}
		startPosA.m_offset = 128;
		startPosB.m_offset = 128;
		endPosA.m_offset = 128;
		endPosB.m_offset = 128;
		bool stopLane = TransportLineAI.GetStopLane(ref startPosA, vehicleType);
		bool stopLane2 = TransportLineAI.GetStopLane(ref startPosB, vehicleType);
		bool stopLane3 = TransportLineAI.GetStopLane(ref endPosA, vehicleType);
		bool stopLane4 = TransportLineAI.GetStopLane(ref endPosB, vehicleType);
		if ((!stopLane && !stopLane2) || (!stopLane3 && !stopLane4))
		{
			TransportLineAI.CheckSegmentProblems(segmentID, ref data);
			return true;
		}
		uint path;
		if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, vehicleType, 20000f, false, true, true, skipQueue))
		{
			if (startPosA.m_segment != 0 && startPosB.m_segment != 0)
			{
				NetNode[] expr_2FE_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_2FE_cp_1 = data.m_startNode;
				expr_2FE_cp_0[(int)expr_2FE_cp_1].m_flags = (expr_2FE_cp_0[(int)expr_2FE_cp_1].m_flags | NetNode.Flags.Ambiguous);
			}
			else
			{
				NetNode[] expr_32A_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_32A_cp_1 = data.m_startNode;
				expr_32A_cp_0[(int)expr_32A_cp_1].m_flags = (expr_32A_cp_0[(int)expr_32A_cp_1].m_flags & ~NetNode.Flags.Ambiguous);
			}
			if (endPosA.m_segment != 0 && endPosB.m_segment != 0)
			{
				NetNode[] expr_369_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_369_cp_1 = data.m_endNode;
				expr_369_cp_0[(int)expr_369_cp_1].m_flags = (expr_369_cp_0[(int)expr_369_cp_1].m_flags | NetNode.Flags.Ambiguous);
			}
			else
			{
				NetNode[] expr_395_cp_0 = instance.m_nodes.m_buffer;
				ushort expr_395_cp_1 = data.m_endNode;
				expr_395_cp_0[(int)expr_395_cp_1].m_flags = (expr_395_cp_0[(int)expr_395_cp_1].m_flags & ~NetNode.Flags.Ambiguous);
			}
			data.m_path = path;
			data.m_flags |= NetSegment.Flags.WaitingPath;
			return false;
		}
		TransportLineAI.CheckSegmentProblems(segmentID, ref data);
		return true;
	}

	private static bool GetStopLane(ref PathUnit.Position pos, VehicleInfo.VehicleType vehicleType)
	{
		if (pos.m_segment != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			int num;
			uint num2;
			if (instance.m_segments.m_buffer[(int)pos.m_segment].GetClosestLane((int)pos.m_lane, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, vehicleType, out num, out num2))
			{
				pos.m_lane = (byte)num;
				return true;
			}
		}
		pos = default(PathUnit.Position);
		return false;
	}

	public static bool UpdatePath(ushort segmentID, ref NetSegment data, ItemClass.Service netService, ItemClass.Service netService2, VehicleInfo.VehicleType vehicleType, bool skipQueue)
	{
		if (data.m_path == 0u)
		{
			return TransportLineAI.StartPathFind(segmentID, ref data, netService, netService2, vehicleType, skipQueue);
		}
		if ((data.m_flags & NetSegment.Flags.WaitingPath) == NetSegment.Flags.None)
		{
			return true;
		}
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
		if ((pathFindFlags & 4) != 0)
		{
			float num = 1f;
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].GetPosition(0, out pathPos))
			{
				Vector3 position = TransportLineAI.CheckNodePosition(data.m_startNode, pathPos);
				num = TransportLineAI.GetCostMultiplier(vehicleType, position);
			}
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].GetLastPosition(out pathPos))
			{
				TransportLineAI.CheckNodePosition(data.m_endNode, pathPos);
			}
			float length = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_length;
			if (length != data.m_averageLength)
			{
				data.m_averageLength = length;
				ushort transportLine = instance2.m_nodes.m_buffer[(int)data.m_startNode].m_transportLine;
				if (transportLine != 0)
				{
					Singleton<TransportManager>.get_instance().UpdateLine(transportLine);
				}
			}
			if (data.m_lanes != 0u)
			{
				instance2.m_lanes.m_buffer[(int)((UIntPtr)data.m_lanes)].m_length = data.m_averageLength * num;
			}
			data.m_flags &= ~NetSegment.Flags.WaitingPath;
			data.m_flags &= ~NetSegment.Flags.PathFailed;
			data.m_flags |= NetSegment.Flags.PathLength;
			TransportLineAI.CheckSegmentProblems(segmentID, ref data);
			return true;
		}
		if ((pathFindFlags & 8) != 0)
		{
			Vector3 position2 = instance2.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position3 = instance2.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			float num2 = Vector3.Distance(position2, position3);
			if (num2 != data.m_averageLength)
			{
				data.m_averageLength = num2;
				ushort transportLine2 = instance2.m_nodes.m_buffer[(int)data.m_startNode].m_transportLine;
				if (transportLine2 != 0)
				{
					Singleton<TransportManager>.get_instance().UpdateLine(transportLine2);
				}
			}
			data.m_flags &= ~NetSegment.Flags.WaitingPath;
			data.m_flags |= NetSegment.Flags.PathFailed;
			data.m_flags |= NetSegment.Flags.PathLength;
			TransportLineAI.CheckSegmentProblems(segmentID, ref data);
			return true;
		}
		return false;
	}

	public static Vector3 CheckNodePosition(ushort nodeID, PathUnit.Position pathPos)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		float num = instance.m_segments.m_buffer[(int)pathPos.m_segment].Info.m_lanes[(int)pathPos.m_lane].m_stopOffset;
		if ((instance.m_segments.m_buffer[(int)pathPos.m_segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
		{
			num = -num;
		}
		uint laneID = PathManager.GetLaneID(pathPos);
		Vector3 vector;
		Vector3 vector2;
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculateStopPositionAndDirection((float)pathPos.m_offset * 0.003921569f, num, out vector, out vector2);
		if (Vector3.SqrMagnitude(vector - instance.m_nodes.m_buffer[(int)nodeID].m_position) > 1f)
		{
			instance.MoveNode(nodeID, vector);
			ushort transportLine = instance.m_nodes.m_buffer[(int)nodeID].m_transportLine;
			if (transportLine != 0)
			{
				Singleton<TransportManager>.get_instance().UpdateLine(transportLine);
			}
		}
		NetNode[] expr_FC_cp_0 = instance.m_nodes.m_buffer;
		expr_FC_cp_0[(int)nodeID].m_flags = (expr_FC_cp_0[(int)nodeID].m_flags | NetNode.Flags.Fixed);
		NetNode[] expr_11E_cp_0 = instance.m_nodes.m_buffer;
		expr_11E_cp_0[(int)nodeID].m_flags = (expr_11E_cp_0[(int)nodeID].m_flags & ~NetNode.Flags.Ambiguous);
		return vector;
	}

	private static float GetCostMultiplier(VehicleInfo.VehicleType vehicleType, Vector3 position)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.Event @event = instance.m_districts.m_buffer[(int)district].m_eventPolicies & Singleton<EventManager>.get_instance().GetEventPolicyMask();
		float num = 1f;
		if ((servicePolicies & DistrictPolicies.Services.FreeTransport) != DistrictPolicies.Services.None || (@event & DistrictPolicies.Event.ComeOneComeAll) != DistrictPolicies.Event.None)
		{
			num *= 0.75f;
		}
		else if ((servicePolicies & DistrictPolicies.Services.HighTicketPrices) != DistrictPolicies.Services.None)
		{
			num /= 0.75f;
		}
		if (vehicleType == VehicleInfo.VehicleType.Ferry && (servicePolicies & DistrictPolicies.Services.PreferFerries) != DistrictPolicies.Services.None)
		{
			num *= 0.75f;
		}
		return num;
	}

	public override void NearbyLanesUpdated(ushort nodeID, ref NetNode data)
	{
		Singleton<NetManager>.get_instance().UpdateNode(nodeID, 0, 10);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Temporary) == NetNode.Flags.None)
		{
			uint num = 0u;
			byte offset = 0;
			float num2 = 1E+10f;
			PathUnit.Position pathPos;
			PathUnit.Position position;
			float num3;
			float num4;
			if ((data.m_flags & NetNode.Flags.ForbidLaneConnection) == NetNode.Flags.None && PathManager.FindPathPosition(data.m_position, this.m_netService, this.m_secondaryNetService, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, this.m_vehicleType, true, false, 32f, out pathPos, out position, out num3, out num4) && num3 < num2)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				int num5;
				uint num6;
				if (instance.m_segments.m_buffer[(int)pathPos.m_segment].GetClosestLane((int)pathPos.m_lane, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, this.m_vehicleType, out num5, out num6))
				{
					num = PathManager.GetLaneID(pathPos);
					offset = pathPos.m_offset;
				}
			}
			if (num != data.m_lane)
			{
				if (data.m_lane != 0u)
				{
					this.RemoveLaneConnection(nodeID, ref data);
				}
				if (num != 0u)
				{
					this.AddLaneConnection(nodeID, ref data, num, offset);
				}
			}
		}
	}

	private void AddLaneConnection(ushort nodeID, ref NetNode data, uint laneID, byte offset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_lane = laneID;
		data.m_laneOffset = offset;
		data.m_nextLaneNode = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = nodeID;
		NetLane.Flags flags = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_flags;
		if ((flags & this.m_stopFlag) == NetLane.Flags.None)
		{
			ushort segment = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_segment;
			instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_flags = (ushort)(flags | this.m_stopFlag);
			Singleton<NetManager>.get_instance().UpdateSegmentFlags(segment);
			Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segment, true);
			NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
			if (info.m_hasParkingSpaces)
			{
				Bounds bounds = instance.m_segments.m_buffer[(int)segment].m_bounds;
				Vector3 min = bounds.get_min();
				Vector3 max = bounds.get_max();
				Singleton<VehicleManager>.get_instance().UpdateParkedVehicles(min.x, min.z, max.x, max.z);
			}
		}
		ushort transportLine = instance.m_nodes.m_buffer[(int)nodeID].m_transportLine;
		if (transportLine != 0)
		{
			Singleton<TransportManager>.get_instance().UpdateLine(transportLine);
		}
	}

	private void RemoveLaneConnection(ushort nodeID, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		ushort num = 0;
		ushort num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == nodeID)
			{
				if (num == 0)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_nodes = data.m_nextLaneNode;
				}
				else
				{
					instance.m_nodes.m_buffer[(int)num].m_nextLaneNode = data.m_nextLaneNode;
				}
				num2 = data.m_nextLaneNode;
			}
			else
			{
				NetInfo info = instance.m_nodes.m_buffer[(int)num2].Info;
				if (info.m_class.m_service == ItemClass.Service.PublicTransport)
				{
					flag = true;
				}
				num = num2;
				num2 = instance.m_nodes.m_buffer[(int)num2].m_nextLaneNode;
			}
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if (!flag)
		{
			NetLane.Flags flags = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_flags;
			instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_flags = (ushort)(flags & ~this.m_stopFlag);
			Singleton<NetManager>.get_instance().UpdateSegmentFlags(instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_segment);
			Singleton<NetManager>.get_instance().UpdateSegmentRenderer(instance.m_lanes.m_buffer[(int)((UIntPtr)data.m_lane)].m_segment, true);
		}
		data.m_lane = 0u;
		data.m_laneOffset = 0;
		data.m_nextLaneNode = 0;
	}

	public override float MaxTransportWaitDistance()
	{
		return this.m_maxPassengerWaitingDistance;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = 0f;
		rightMin = 0f;
		max = 0f;
	}

	public float m_maxPassengerWaitingDistance = 10f;

	public int m_publicTransportAccumulation = 100;

	public float m_publicTransportRadius = 200f;

	public ItemClass.Service m_netService;

	public ItemClass.Service m_secondaryNetService;

	public VehicleInfo.VehicleType m_vehicleType = VehicleInfo.VehicleType.Car;

	public NetLane.Flags m_stopFlag = NetLane.Flags.Stop;
}
