﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.IO;

public class GuideTriggerBase : IDataContainer
{
	protected bool CanActivate(GuideInfo guideInfo)
	{
		if (guideInfo != null && !this.m_disabled)
		{
			if (!Singleton<LoadingManager>.get_instance().m_simulationDataLoaded)
			{
				return false;
			}
			if (Singleton<SimulationManager>.get_instance().ForcedSimulationPaused)
			{
				return false;
			}
			if (this.m_activationCount < 255)
			{
				this.m_activationCount += 1;
			}
			uint num;
			uint num2;
			switch (guideInfo.m_delayType)
			{
			case GuideInfo.Delay.GameWeeks:
				num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				num2 = num + (uint)((uint)guideInfo.m_displayDelay << 12);
				break;
			case GuideInfo.Delay.RealSeconds:
				num = Singleton<SimulationManager>.get_instance().m_currentTickIndex;
				num2 = num + (uint)(guideInfo.m_displayDelay * 60);
				break;
			case GuideInfo.Delay.OccurrenceCount:
				num = (uint)this.m_activationCount;
				num2 = num + (uint)guideInfo.m_displayDelay;
				break;
			default:
				num = 0u;
				num2 = 1u;
				break;
			}
			if (num2 < this.m_nextRepeat)
			{
				num2 = this.m_nextRepeat;
			}
			if (this.m_activationFrame == 0u || num2 < this.m_activationFrame)
			{
				this.m_activationFrame = num2;
			}
			if (this.m_activationFrame <= num && (guideInfo.m_maxDisplayCount == 0 || (int)this.m_displayCount < guideInfo.m_maxDisplayCount) && Singleton<GuideManager>.get_instance().CanActivate(guideInfo))
			{
				return true;
			}
		}
		return false;
	}

	protected void Activate(GuideInfo guideInfo, GuideTriggerBase.ActivationInfo activationInfo)
	{
		if (this.m_displayCount < 255)
		{
			this.m_displayCount += 1;
		}
		GuideInfo.Delay delayType = guideInfo.m_delayType;
		if (delayType != GuideInfo.Delay.GameWeeks)
		{
			if (delayType != GuideInfo.Delay.RealSeconds)
			{
				if (delayType == GuideInfo.Delay.OccurrenceCount)
				{
					this.m_nextRepeat = (uint)guideInfo.m_repeatDelay;
				}
			}
			else
			{
				this.m_nextRepeat = this.m_activationFrame + (uint)(guideInfo.m_repeatDelay * 60);
			}
		}
		else
		{
			this.m_nextRepeat = this.m_activationFrame + (uint)((uint)guideInfo.m_repeatDelay << 12);
		}
		this.m_activationFrame = 0u;
		this.m_activationCount = 0;
		Singleton<GuideManager>.get_instance().Activate(this, activationInfo);
	}

	public virtual void Deactivate()
	{
		this.m_activationFrame = 0u;
		this.m_activationCount = 0;
		Singleton<GuideManager>.get_instance().Deactivate(this);
	}

	public virtual void Disable()
	{
		if (!this.m_disabled)
		{
			this.m_disabled = true;
			Singleton<GuideManager>.get_instance().Deactivate(this);
		}
	}

	[DebuggerHidden]
	public IEnumerator Disable_Action()
	{
		GuideTriggerBase.<Disable_Action>c__Iterator0 <Disable_Action>c__Iterator = new GuideTriggerBase.<Disable_Action>c__Iterator0();
		<Disable_Action>c__Iterator.$this = this;
		return <Disable_Action>c__Iterator;
	}

	public virtual bool Validate()
	{
		return true;
	}

	public virtual void Serialize(DataSerializer s)
	{
		s.WriteUInt32(this.m_nextRepeat);
		s.WriteUInt32(this.m_activationFrame);
		s.WriteUInt8((uint)this.m_activationCount);
		s.WriteUInt8((uint)this.m_displayCount);
		s.WriteBool(this.m_disabled);
	}

	public virtual void Deserialize(DataSerializer s)
	{
		this.m_nextRepeat = s.ReadUInt32();
		this.m_activationFrame = s.ReadUInt32();
		this.m_activationCount = (byte)s.ReadUInt8();
		this.m_displayCount = (byte)s.ReadUInt8();
		this.m_disabled = s.ReadBool();
	}

	public virtual void AfterDeserialize(DataSerializer s)
	{
	}

	public uint m_nextRepeat;

	public uint m_activationFrame;

	public byte m_activationCount;

	public byte m_displayCount;

	public bool m_disabled;

	public class ActivationInfo
	{
		protected ActivationInfo(GuideInfo guideInfo)
		{
			this.m_guideInfo = guideInfo;
		}

		public virtual string GetName()
		{
			return this.m_guideInfo.m_name;
		}

		public virtual string GetIcon()
		{
			return this.m_guideInfo.m_icon;
		}

		public virtual string FormatText(string pattern)
		{
			return pattern;
		}

		public virtual IUITag GetTag()
		{
			return MonoTutorialTag.Find(this.m_guideInfo.m_tag);
		}

		protected GuideInfo m_guideInfo;
	}
}
