﻿using System;
using ColossalFramework.Math;
using UnityEngine;

public static class DecorationMesh
{
	public static void GenerateMesh()
	{
		DecorationMesh.m_mesh = DecorationMesh.GenerateMesh(100);
	}

	public static void DestroyMesh()
	{
		if (DecorationMesh.m_mesh != null)
		{
			Object.Destroy(DecorationMesh.m_mesh);
			DecorationMesh.m_mesh = null;
		}
	}

	private static Mesh GenerateMesh(int resolution)
	{
		int num = resolution * resolution;
		float num2 = 1f / (float)(resolution << 8);
		Vector3[] array = new Vector3[num * 4];
		Vector2[] array2 = new Vector2[num * 4];
		Vector2[] array3 = new Vector2[num * 4];
		int[] array4 = new int[num * 6];
		Randomizer randomizer;
		randomizer..ctor(resolution);
		int num3 = 0;
		int num4 = 0;
		for (int i = 0; i < resolution; i++)
		{
			for (int j = 0; j < resolution; j++)
			{
				int num5 = (j << 8) + randomizer.Int32(32, 224);
				int num6 = (i << 8) + randomizer.Int32(32, 224);
				int num7 = randomizer.Int32(4u);
				Vector3 vector;
				vector..ctor((float)num5 * num2 - 0.5f, 0f, (float)num6 * num2 - 0.5f);
				Vector2 vector2;
				vector2..ctor((float)num7 * 0.25f, 0f);
				array[num3] = vector + new Vector3(-0.5f, -0.5f, 0f);
				array2[num3] = vector2 + new Vector2(0f, 0f);
				array3[num3] = new Vector2(-0.5f, -0.5f);
				num3++;
				array[num3] = vector + new Vector3(-0.5f, 0.5f, 0f);
				array2[num3] = vector2 + new Vector2(0f, 0.25f);
				array3[num3] = new Vector2(-0.5f, 0.5f);
				num3++;
				array[num3] = vector + new Vector3(0.5f, 0.5f, 0f);
				array2[num3] = vector2 + new Vector2(0.25f, 0.25f);
				array3[num3] = new Vector2(0.5f, 0.5f);
				num3++;
				array[num3] = vector + new Vector3(0.5f, -0.5f, 0f);
				array2[num3] = vector2 + new Vector2(0.25f, 0f);
				array3[num3] = new Vector2(0.5f, -0.5f);
				num3++;
				array4[num4++] = num3 - 4;
				array4[num4++] = num3 - 3;
				array4[num4++] = num3 - 1;
				array4[num4++] = num3 - 1;
				array4[num4++] = num3 - 3;
				array4[num4++] = num3 - 2;
			}
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_uv(array2);
		mesh.set_uv2(array3);
		mesh.set_triangles(array4);
		return mesh;
	}

	public static Mesh m_mesh;
}
