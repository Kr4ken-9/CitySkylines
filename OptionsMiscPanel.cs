﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class OptionsMiscPanel : UICustomControl
{
	private void Awake()
	{
	}

	private void OnEnable()
	{
		Singleton<PopsManager>.get_instance().EventLoginChanged += new PopsManager.OnLoginChangedHandler(this.OnPdxAccountChange);
		this.OnPdxAccountChange();
	}

	private void OnDisable()
	{
		Singleton<PopsManager>.get_instance().EventLoginChanged -= new PopsManager.OnLoginChangedHandler(this.OnPdxAccountChange);
	}

	private void OnPdxAccountChange()
	{
		base.Find<UIButton>("Logout").set_isEnabled(Singleton<PopsManager>.get_instance().LoggedIn);
	}

	public void OnClearKeyMapping()
	{
		ConfirmPanel.ShowModal("CONFIRM_RESETKEYMAPPING", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.ResetKeyMapping();
			}
		});
	}

	public void OnClearMonumentUnlock()
	{
		ConfirmPanel.ShowModal("CONFIRM_RESETUNIQUEBUILDINGS", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				this.ResetMonumentUnlock();
			}
		});
	}

	public void OnClearConfiguration()
	{
		ConfirmPanel.ShowModal("CONFIRM_RESETCONFIGURATION", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				GameSettings.ClearAll();
				PlayerPrefs.DeleteKey(OptionsGraphicsPanel.kIsFullScreen);
				PlayerPrefs.DeleteKey(OptionsGraphicsPanel.kResolutionWidth);
				PlayerPrefs.DeleteKey(OptionsGraphicsPanel.kResolutionHeight);
			}
		});
	}

	public void OnLogout()
	{
		Singleton<PopsManager>.get_instance().Logout();
	}

	private void ResetKeyMapping()
	{
		List<SavedInputKey> list = new List<SavedInputKey>();
		FieldInfo[] fields = typeof(Settings).GetFields(BindingFlags.Static | BindingFlags.Public);
		FieldInfo[] array = fields;
		for (int i = 0; i < array.Length; i++)
		{
			FieldInfo fieldInfo = array[i];
			RebindableKeyAttribute[] array2 = fieldInfo.GetCustomAttributes(typeof(RebindableKeyAttribute), false) as RebindableKeyAttribute[];
			if (array2.Length > 0)
			{
				string text = fieldInfo.GetValue(null) as string;
				SavedInputKey savedInputKey = new SavedInputKey(text, Settings.gameSettingsFile, this.m_KeymappingPanel.GetDefaultEntry(text), true);
				FieldInfo field = typeof(DefaultSettings).GetField(text, BindingFlags.Static | BindingFlags.Public);
				if (field != null)
				{
					object value = field.GetValue(null);
					if (value is InputKey)
					{
						savedInputKey.set_value((InputKey)value);
					}
				}
			}
		}
		this.m_KeymappingPanel.RefreshKeyMapping();
		for (int j = 0; j < list.Count; j++)
		{
			list[j].Delete();
		}
	}

	private void ResetMonumentUnlock()
	{
		SettingsFile settingsFile = GameSettings.FindSettingsFileByName(Settings.userGameState);
		if (settingsFile != null)
		{
			string[] array = settingsFile.ListKeys();
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i];
				if ((text.StartsWith("BuildingUnlocked[") && text.EndsWith("]")) || (text.StartsWith("ScenarioWinCount[") && text.EndsWith("]")))
				{
					settingsFile.DeleteEntry(text);
				}
			}
		}
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().RefreshScenarioMilestones();
			Singleton<UnlockManager>.get_instance().ClearMonumentUnlock();
		}
	}

	public OptionsKeymappingPanel m_KeymappingPanel;
}
