﻿using System;
using ColossalFramework;

public class BuildingOnFireGuide : BuildingInstanceGuide
{
	public override bool Validate()
	{
		return base.Validate() && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_buildingID].m_fireIntensity != 0;
	}
}
