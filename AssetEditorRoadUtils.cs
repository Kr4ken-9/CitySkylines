﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using ColossalFramework;
using ColossalFramework.Importers;
using ColossalFramework.UI;
using UnityEngine;

public static class AssetEditorRoadUtils
{
	public static SteamHelper.DLC_BitMask GetDLCMask(NetInfo info)
	{
		SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)0;
		dLC_BitMask |= AssetEditorRoadUtils.GetElevationDLCMask(info);
		NetInfo netInfo = AssetEditorRoadUtils.TryGetElevated(info);
		if (netInfo != null)
		{
			dLC_BitMask |= AssetEditorRoadUtils.GetElevationDLCMask(netInfo);
		}
		NetInfo netInfo2 = AssetEditorRoadUtils.TryGetBridge(info);
		if (netInfo2 != null)
		{
			dLC_BitMask |= AssetEditorRoadUtils.GetElevationDLCMask(netInfo2);
		}
		NetInfo netInfo3 = AssetEditorRoadUtils.TryGetSlope(info);
		if (netInfo3 != null)
		{
			dLC_BitMask |= AssetEditorRoadUtils.GetElevationDLCMask(netInfo3);
		}
		NetInfo netInfo4 = AssetEditorRoadUtils.TryGetTunnel(info);
		if (netInfo4 != null)
		{
			dLC_BitMask |= AssetEditorRoadUtils.GetElevationDLCMask(netInfo4);
		}
		HashSet<BuildingInfo> pillars = AssetEditorRoadUtils.GetPillars(info);
		foreach (BuildingInfo current in pillars)
		{
			dLC_BitMask |= current.m_dlcRequired;
		}
		return dLC_BitMask;
	}

	private static SteamHelper.DLC_BitMask GetElevationDLCMask(NetInfo info)
	{
		SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)0;
		dLC_BitMask |= info.m_dlcRequired;
		if (info.m_lanes != null)
		{
			NetInfo.Lane[] lanes = info.m_lanes;
			for (int i = 0; i < lanes.Length; i++)
			{
				NetInfo.Lane lane = lanes[i];
				if (lane.m_laneProps != null && lane.m_laneProps.m_props != null)
				{
					NetLaneProps.Prop[] props = lane.m_laneProps.m_props;
					for (int j = 0; j < props.Length; j++)
					{
						NetLaneProps.Prop prop = props[j];
						PropInfo prop2 = prop.m_prop;
						if (prop2 != null)
						{
							dLC_BitMask |= prop2.m_dlcRequired;
						}
						TreeInfo tree = prop.m_tree;
						if (tree != null)
						{
							dLC_BitMask |= tree.m_dlcRequired;
						}
					}
				}
			}
		}
		return dLC_BitMask;
	}

	public static HashSet<BuildingInfo> GetPillars(NetInfo info)
	{
		HashSet<BuildingInfo> hashSet = new HashSet<BuildingInfo>();
		AssetEditorRoadUtils.AddElevationPillars(info, hashSet);
		NetInfo netInfo = AssetEditorRoadUtils.TryGetElevated(info);
		if (netInfo != null)
		{
			AssetEditorRoadUtils.AddElevationPillars(netInfo, hashSet);
		}
		NetInfo netInfo2 = AssetEditorRoadUtils.TryGetBridge(info);
		if (netInfo2 != null)
		{
			AssetEditorRoadUtils.AddElevationPillars(netInfo2, hashSet);
		}
		NetInfo netInfo3 = AssetEditorRoadUtils.TryGetSlope(info);
		if (netInfo3 != null)
		{
			AssetEditorRoadUtils.AddElevationPillars(netInfo3, hashSet);
		}
		NetInfo netInfo4 = AssetEditorRoadUtils.TryGetTunnel(info);
		if (netInfo4 != null)
		{
			AssetEditorRoadUtils.AddElevationPillars(netInfo4, hashSet);
		}
		return hashSet;
	}

	private static void AddElevationPillars(NetInfo info, HashSet<BuildingInfo> pillars)
	{
		NetAI netAI = info.m_netAI;
		if (netAI != null)
		{
			AssetEditorRoadUtils.TryAddPillarToList(netAI, "m_bridgePillarInfo", pillars);
			AssetEditorRoadUtils.TryAddPillarToList(netAI, "m_middlePillarInfo", pillars);
			AssetEditorRoadUtils.TryAddPillarToList(netAI, "m_bridgePillarInfo2", pillars);
			AssetEditorRoadUtils.TryAddPillarToList(netAI, "m_bridgePillarInfo3", pillars);
			AssetEditorRoadUtils.TryAddPillarToList(netAI, "m_pylonInfo", pillars);
		}
	}

	private static void TryAddPillarToList(object target, string fieldName, HashSet<BuildingInfo> pillars)
	{
		FieldInfo field = target.GetType().GetField(fieldName);
		if (field != null)
		{
			object value = field.GetValue(target);
			if (value != null)
			{
				Type type = value.GetType();
				if (type == typeof(BuildingInfo))
				{
					pillars.Add((BuildingInfo)value);
				}
				else if (type.IsArray && type.GetElementType() == typeof(BuildingInfo))
				{
					Array array = (Array)value;
					IEnumerator enumerator = array.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							object current = enumerator.Current;
							pillars.Add((BuildingInfo)current);
						}
					}
					finally
					{
						IDisposable disposable;
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
				}
			}
		}
	}

	public static string GetMeshName(object obj)
	{
		if (obj is NetInfo.Segment)
		{
			NetInfo.Segment segment = obj as NetInfo.Segment;
			return (!(segment.m_mesh != null) || string.IsNullOrEmpty(segment.m_mesh.get_name())) ? "New segment" : segment.m_mesh.get_name();
		}
		if (obj is NetInfo.Node)
		{
			NetInfo.Node node = obj as NetInfo.Node;
			return (!(node.m_mesh != null) || string.IsNullOrEmpty(node.m_mesh.get_name())) ? "New node" : node.m_mesh.get_name();
		}
		return string.Empty;
	}

	public static string GetShaderName(object obj)
	{
		if (obj is NetInfo.Segment)
		{
			NetInfo.Segment segment = obj as NetInfo.Segment;
			if (segment.m_material != null && segment.m_material.get_shader() != null)
			{
				return segment.m_material.get_shader().get_name();
			}
		}
		else if (obj is NetInfo.Node)
		{
			NetInfo.Node node = obj as NetInfo.Node;
			if (node.m_material != null && node.m_material.get_shader() != null)
			{
				return node.m_material.get_shader().get_name();
			}
		}
		return string.Empty;
	}

	public static Material GetMaterial(object obj)
	{
		if (obj is NetInfo.Segment)
		{
			return ((NetInfo.Segment)obj).m_material;
		}
		if (obj is NetInfo.Node)
		{
			return ((NetInfo.Node)obj).m_material;
		}
		return null;
	}

	public static string FormatCollapsibleLabel(Type type)
	{
		return type.Name + "s";
	}

	public static NetInfo InstantiatePrefab(NetInfo template)
	{
		NetInfo netInfo = AssetEditorRoadUtils.Instantiate(template);
		netInfo.m_availableIn |= ItemClass.Availability.Game;
		AssetEditorRoadUtils.CopyAIProperties(template, netInfo);
		PrefabCollection<NetInfo>.BindPrefabs();
		Singleton<NetManager>.get_instance().RebuildLods();
		return netInfo;
	}

	public static NetInfo TryGetElevated(NetInfo info)
	{
		return AssetEditorRoadUtils.TryGetElevation(info, "m_elevatedInfo");
	}

	public static NetInfo TryGetBridge(NetInfo info)
	{
		return AssetEditorRoadUtils.TryGetElevation(info, "m_bridgeInfo");
	}

	public static NetInfo TryGetSlope(NetInfo info)
	{
		return AssetEditorRoadUtils.TryGetElevation(info, "m_slopeInfo");
	}

	public static NetInfo TryGetTunnel(NetInfo info)
	{
		return AssetEditorRoadUtils.TryGetElevation(info, "m_tunnelInfo");
	}

	private static NetInfo TryGetElevation(NetInfo info, string fieldName)
	{
		if (info != null)
		{
			NetAI component = info.GetComponent<NetAI>();
			if (component != null)
			{
				FieldInfo field = component.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.Public);
				if (field != null)
				{
					object value = field.GetValue(component);
					if (value != null && value is NetInfo)
					{
						return value as NetInfo;
					}
				}
			}
		}
		return null;
	}

	public static object AddArrayElement(object target, FieldInfo field)
	{
		if (field.FieldType.IsArray)
		{
			Type fieldType = field.FieldType;
			Type elementType = fieldType.GetElementType();
			Array array = (Array)field.GetValue(target);
			Array array2 = Array.CreateInstance(elementType, array.Length + 1);
			for (int i = 0; i < array.Length; i++)
			{
				array2.SetValue(array.GetValue(i), i);
			}
			object obj;
			if (!AssetEditorRoadUtils.CustomCreateDummyInstance(elementType, out obj))
			{
				obj = Activator.CreateInstance(elementType);
			}
			array2.SetValue(obj, array2.Length - 1);
			field.SetValue(target, array2);
			return obj;
		}
		return null;
	}

	private static bool CustomCreateDummyInstance(Type type, out object obj)
	{
		if (type == typeof(NetInfo.Lane))
		{
			obj = new NetInfo.Lane
			{
				m_laneProps = ScriptableObject.CreateInstance<NetLaneProps>(),
				m_laneProps = 
				{
					m_props = new NetLaneProps.Prop[0]
				}
			};
			return true;
		}
		Shader shader = Shader.Find("Custom/Net/Road");
		if (type == typeof(NetInfo.Segment))
		{
			obj = new NetInfo.Segment
			{
				m_mesh = new Mesh(),
				m_material = new Material(shader),
				m_lodMesh = new Mesh(),
				m_lodMaterial = new Material(shader)
			};
			return true;
		}
		if (type == typeof(NetInfo.Node))
		{
			obj = new NetInfo.Node
			{
				m_mesh = new Mesh(),
				m_material = new Material(shader),
				m_lodMesh = new Mesh(),
				m_lodMaterial = new Material(shader)
			};
			return true;
		}
		obj = null;
		return false;
	}

	public static void RemoveArrayElement(object element, object target, FieldInfo field)
	{
		Array array = field.GetValue(target) as Array;
		Array array2 = Array.CreateInstance(array.GetType().GetElementType(), array.Length - 1);
		int num = 0;
		IEnumerator enumerator = array.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object current = enumerator.Current;
				if (current != element)
				{
					array2.SetValue(current, num);
					num++;
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		field.SetValue(target, array2);
	}

	[DebuggerHidden]
	public static IEnumerator RefreshNetsCoroutine(NetInfo info)
	{
		AssetEditorRoadUtils.<RefreshNetsCoroutine>c__Iterator0 <RefreshNetsCoroutine>c__Iterator = new AssetEditorRoadUtils.<RefreshNetsCoroutine>c__Iterator0();
		<RefreshNetsCoroutine>c__Iterator.info = info;
		return <RefreshNetsCoroutine>c__Iterator;
	}

	private static void HandleErrors(AssetEditorRoadUtils.ElevationErrors errors)
	{
		if ((errors & ~AssetEditorRoadUtils.ElevationErrors.Unknown) != AssetEditorRoadUtils.ElevationErrors.None)
		{
			List<string> list = new List<string>();
			if (EnumExtensions.IsFlagSet<AssetEditorRoadUtils.ElevationErrors>(errors, AssetEditorRoadUtils.ElevationErrors.Basic))
			{
				list.Add("basic");
			}
			if (EnumExtensions.IsFlagSet<AssetEditorRoadUtils.ElevationErrors>(errors, AssetEditorRoadUtils.ElevationErrors.Elevated))
			{
				list.Add("elevated");
			}
			if (EnumExtensions.IsFlagSet<AssetEditorRoadUtils.ElevationErrors>(errors, AssetEditorRoadUtils.ElevationErrors.Bridge))
			{
				list.Add("bridge");
			}
			if (EnumExtensions.IsFlagSet<AssetEditorRoadUtils.ElevationErrors>(errors, AssetEditorRoadUtils.ElevationErrors.Slope))
			{
				list.Add("slope");
			}
			if (EnumExtensions.IsFlagSet<AssetEditorRoadUtils.ElevationErrors>(errors, AssetEditorRoadUtils.ElevationErrors.Tunnel))
			{
				list.Add("tunnel");
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Something in the following elevation(s) is preventing road from being initialized:");
			stringBuilder.Append("\n");
			foreach (string current in list)
			{
				stringBuilder.Append("- ");
				stringBuilder.Append(current);
				stringBuilder.Append("\n");
			}
			stringBuilder.Append("This will prevent the road from working properly. Try undoing your last changes to these elevations.");
			UIView.get_library().ShowModal<ExceptionPanel>("ExceptionPanel").SetMessage("Uh oh! Something went wrong.", stringBuilder.ToString(), true);
		}
	}

	private static bool SafeInitializePrefab(NetInfo info)
	{
		if (info != null)
		{
			try
			{
				info.InitializePrefab();
				info.CheckReferences();
				info.RefreshLevelOfDetail();
				bool result = true;
				return result;
			}
			catch (Exception ex)
			{
				Debug.LogException(ex);
				bool result = false;
				return result;
			}
			return true;
		}
		return true;
	}

	private static void CopyAIProperties(NetInfo template, NetInfo target)
	{
		PlayerNetAI component = template.GetComponent<PlayerNetAI>();
		PlayerNetAI component2 = target.GetComponent<PlayerNetAI>();
		if (component != null && component2 != null && component.GetType() == component2.GetType())
		{
			AssetEditorRoadUtils.CopyElevation(component, component2, "m_elevatedInfo");
			AssetEditorRoadUtils.CopyElevation(component, component2, "m_bridgeInfo");
			AssetEditorRoadUtils.CopyElevation(component, component2, "m_slopeInfo");
			AssetEditorRoadUtils.CopyElevation(component, component2, "m_tunnelInfo");
		}
	}

	private static void CopyElevation(PlayerNetAI template, PlayerNetAI target, string fieldName)
	{
		FieldInfo field = template.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.Public);
		if (field != null)
		{
			object value = field.GetValue(template);
			if (value != null && value is NetInfo)
			{
				NetInfo netInfo = AssetEditorRoadUtils.Instantiate(value as NetInfo);
				field.SetValue(target, netInfo);
				netInfo.m_Atlas = null;
				netInfo.m_InfoTooltipAtlas = null;
			}
		}
	}

	private static NetInfo Instantiate(NetInfo template)
	{
		NetInfo netInfo = Object.Instantiate<NetInfo>(template);
		netInfo.set_name(template.get_name());
		AssetEditorRoadUtils.InstantiateLanes(netInfo);
		AssetEditorRoadUtils.InstantiateSegments(netInfo);
		AssetEditorRoadUtils.InstantiateNodes(netInfo);
		AssetEditorRoadUtils.InstantiateModel(netInfo);
		netInfo.get_gameObject().SetActive(false);
		netInfo.get_gameObject().set_name(AssetEditorRoadUtils.GetUniqueNetInfoName(netInfo.get_gameObject().get_name()));
		netInfo.m_prefabInitialized = false;
		PrefabCollection<NetInfo>.InitializePrefabs("Custom Assets", netInfo, null);
		netInfo.CheckReferences();
		netInfo.RefreshLevelOfDetail();
		return netInfo;
	}

	private static string GetUniqueNetInfoName(string name)
	{
		name = PackageHelper.StripName(name);
		int num = 0;
		for (int i = name.Length - 1; i > 0; i--)
		{
			int num2;
			if (!int.TryParse(name[i].ToString(), out num2))
			{
				break;
			}
			num = i;
		}
		if (num > 0)
		{
			name = name.Substring(0, num);
		}
		int num3 = 0;
		while (PrefabCollection<NetInfo>.LoadedExists(name + num3))
		{
			num3++;
		}
		return name + num3;
	}

	private static void InstantiateLanes(NetInfo info)
	{
		int num = (info.m_lanes == null) ? 0 : info.m_lanes.Length;
		NetInfo.Lane[] lanes = info.m_lanes;
		info.m_lanes = new NetInfo.Lane[num];
		for (int i = 0; i < num; i++)
		{
			info.m_lanes[i] = new NetInfo.Lane();
			AssetEditorRoadUtils.CopyProperties(info.m_lanes[i], lanes[i]);
			NetLaneProps laneProps = info.m_lanes[i].m_laneProps;
			if (laneProps != null)
			{
				info.m_lanes[i].m_laneProps = Object.Instantiate<NetLaneProps>(laneProps);
				int num2 = laneProps.m_props.Length;
				for (int j = 0; j < num2; j++)
				{
					info.m_lanes[i].m_laneProps.m_props[j] = new NetLaneProps.Prop();
					AssetEditorRoadUtils.CopyProperties(info.m_lanes[i].m_laneProps.m_props[j], laneProps.m_props[j]);
				}
			}
			else
			{
				info.m_lanes[i].m_laneProps = ScriptableObject.CreateInstance<NetLaneProps>();
				info.m_lanes[i].m_laneProps.m_props = new NetLaneProps.Prop[0];
			}
		}
	}

	private static void InstantiateNodes(NetInfo info)
	{
		int num = (info.m_nodes == null) ? 0 : info.m_nodes.Length;
		NetInfo.Node[] nodes = info.m_nodes;
		info.m_nodes = new NetInfo.Node[num];
		for (int i = 0; i < num; i++)
		{
			info.m_nodes[i] = new NetInfo.Node();
			AssetEditorRoadUtils.CopyProperties(info.m_nodes[i], nodes[i]);
		}
	}

	private static void InstantiateSegments(NetInfo info)
	{
		int num = (info.m_segments == null) ? 0 : info.m_segments.Length;
		NetInfo.Segment[] segments = info.m_segments;
		info.m_segments = new NetInfo.Segment[num];
		for (int i = 0; i < num; i++)
		{
			info.m_segments[i] = new NetInfo.Segment();
			AssetEditorRoadUtils.CopyProperties(info.m_segments[i], segments[i]);
		}
	}

	private static void CopyProperties(object target, object origin)
	{
		FieldInfo[] fields = target.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
		FieldInfo[] array = fields;
		for (int i = 0; i < array.Length; i++)
		{
			FieldInfo fieldInfo = array[i];
			fieldInfo.SetValue(target, fieldInfo.GetValue(origin));
		}
	}

	private static void InstantiateModel(NetInfo info)
	{
		if (info != null)
		{
			NetInfo.Segment[] segments = info.m_segments;
			NetInfo.Node[] nodes = info.m_nodes;
			if (segments != null)
			{
				NetInfo.Segment[] array = segments;
				for (int i = 0; i < array.Length; i++)
				{
					NetInfo.Segment segment = array[i];
					AssetEditorRoadUtils.InstantiateModel(segment);
				}
			}
			if (nodes != null)
			{
				NetInfo.Node[] array2 = nodes;
				for (int j = 0; j < array2.Length; j++)
				{
					NetInfo.Node node = array2[j];
					AssetEditorRoadUtils.InstantiateModel(node);
				}
			}
		}
	}

	private static void InstantiateModel(NetInfo.Segment segment)
	{
		segment.m_mesh = AssetEditorRoadUtils.CopyMesh(segment.m_mesh);
		segment.m_material = AssetEditorRoadUtils.CopyMaterial(segment.m_material);
		segment.m_lodMesh = AssetEditorRoadUtils.CopyMesh(segment.m_lodMesh);
		segment.m_lodMaterial = AssetEditorRoadUtils.CopyMaterial(segment.m_lodMaterial);
	}

	private static void InstantiateModel(NetInfo.Node node)
	{
		node.m_mesh = AssetEditorRoadUtils.CopyMesh(node.m_mesh);
		node.m_material = AssetEditorRoadUtils.CopyMaterial(node.m_material);
		node.m_lodMesh = AssetEditorRoadUtils.CopyMesh(node.m_lodMesh);
		node.m_lodMaterial = AssetEditorRoadUtils.CopyMaterial(node.m_lodMaterial);
	}

	public static Mesh CopyMesh(Mesh mesh)
	{
		if (mesh != null)
		{
			Mesh mesh2 = Object.Instantiate<Mesh>(mesh);
			mesh2.set_name(mesh.get_name());
			return mesh2;
		}
		return null;
	}

	public static Material CopyMaterial(Material material)
	{
		if (material != null)
		{
			Material material2 = Object.Instantiate<Material>(material);
			material2.set_name(material.get_name());
			if (material2.HasProperty("_MainTex"))
			{
				AssetEditorRoadUtils.InstantiateTexture(material2, "_MainTex");
			}
			if (material2.HasProperty("_XYSMap"))
			{
				AssetEditorRoadUtils.InstantiateTexture(material2, "_XYSMap");
			}
			if (material2.HasProperty("_APRMap"))
			{
				AssetEditorRoadUtils.InstantiateTexture(material2, "_APRMap");
			}
			return material2;
		}
		return null;
	}

	private static void InstantiateTexture(Material material, string sampler)
	{
		Texture texture = material.GetTexture(sampler);
		if (texture != null)
		{
			Texture texture2 = new Image(texture).CreateTexture(sampler != "_MainTex");
			texture2.set_anisoLevel(texture.get_anisoLevel());
			material.SetTexture(sampler, texture2);
		}
	}

	public static void ReleaseModel(NetInfo info)
	{
		if (info != null)
		{
			NetInfo.Segment[] segments = info.m_segments;
			NetInfo.Node[] nodes = info.m_nodes;
			if (segments != null)
			{
				NetInfo.Segment[] array = segments;
				for (int i = 0; i < array.Length; i++)
				{
					NetInfo.Segment segment = array[i];
					AssetEditorRoadUtils.ReleaseModel(segment);
				}
			}
			if (nodes != null)
			{
				NetInfo.Node[] array2 = nodes;
				for (int j = 0; j < array2.Length; j++)
				{
					NetInfo.Node node = array2[j];
					AssetEditorRoadUtils.ReleaseModel(node);
				}
			}
		}
	}

	public static void ReleaseModel(NetInfo.Node node)
	{
		if (node != null)
		{
			AssetEditorRoadUtils.ReleaseMesh(node.m_mesh);
			AssetEditorRoadUtils.ReleaseMesh(node.m_lodMesh);
			AssetEditorRoadUtils.ReleaseMaterial(node.m_material);
			AssetEditorRoadUtils.ReleaseMaterial(node.m_lodMaterial);
		}
	}

	public static void ReleaseModel(NetInfo.Segment segment)
	{
		if (segment != null)
		{
			AssetEditorRoadUtils.ReleaseMesh(segment.m_mesh);
			AssetEditorRoadUtils.ReleaseMesh(segment.m_lodMesh);
			AssetEditorRoadUtils.ReleaseMaterial(segment.m_material);
			AssetEditorRoadUtils.ReleaseMaterial(segment.m_lodMaterial);
		}
	}

	private static void ReleaseMesh(Mesh mesh)
	{
		if (mesh != null)
		{
			Object.Destroy(mesh);
		}
	}

	public static void ReleaseMaterial(Material material)
	{
		if (material != null)
		{
			if (material.HasProperty("_MainTex"))
			{
				AssetEditorRoadUtils.ReleaseTexture(material, "_MainTex");
			}
			if (material.HasProperty("_XYSMap"))
			{
				AssetEditorRoadUtils.ReleaseTexture(material, "_XYSMap");
			}
			if (material.HasProperty("_APRMap"))
			{
				AssetEditorRoadUtils.ReleaseTexture(material, "_APRMap");
			}
			Object.Destroy(material);
		}
	}

	private static void ReleaseTexture(Material material, string textureName)
	{
		if (material != null)
		{
			Texture texture = material.GetTexture(textureName);
			if (texture != null)
			{
				Object.Destroy(texture);
			}
		}
	}

	public static void ReleaseRoad(NetInfo info)
	{
		if (info != null)
		{
			List<NetInfo> list = new List<NetInfo>();
			list.Add(info);
			AssetEditorRoadUtils.ReleaseModel(info);
			NetInfo netInfo = AssetEditorRoadUtils.TryGetElevated(info);
			NetInfo netInfo2 = AssetEditorRoadUtils.TryGetBridge(info);
			NetInfo netInfo3 = AssetEditorRoadUtils.TryGetSlope(info);
			NetInfo netInfo4 = AssetEditorRoadUtils.TryGetTunnel(info);
			if (netInfo != null)
			{
				list.Add(netInfo);
				AssetEditorRoadUtils.ReleaseModel(netInfo);
			}
			if (netInfo2 != null)
			{
				list.Add(netInfo2);
				AssetEditorRoadUtils.ReleaseModel(netInfo2);
			}
			if (netInfo3 != null)
			{
				list.Add(netInfo3);
				AssetEditorRoadUtils.ReleaseModel(netInfo3);
			}
			if (netInfo4 != null)
			{
				list.Add(netInfo4);
				AssetEditorRoadUtils.ReleaseModel(netInfo4);
			}
			PrefabCollection<NetInfo>.DestroyPrefabs("Custom assets", list.ToArray(), null);
			PrefabCollection<NetInfo>.BindPrefabs();
			Object.Destroy(info.get_gameObject());
			if (netInfo != null)
			{
				Object.Destroy(netInfo.get_gameObject());
			}
			if (netInfo2 != null)
			{
				Object.Destroy(netInfo2.get_gameObject());
			}
			if (netInfo3 != null)
			{
				Object.Destroy(netInfo3.get_gameObject());
			}
			if (netInfo4 != null)
			{
				Object.Destroy(netInfo4.get_gameObject());
			}
			Singleton<NetManager>.get_instance().RebuildLods();
		}
	}

	public static readonly string kMeshField = "m_mesh";

	public static readonly string kLodMeshField = "m_lodMesh";

	public static readonly string kMaterialField = "m_material";

	public static readonly string kLodMaterialField = "m_lodMaterial";

	[Flags]
	public enum ElevationErrors
	{
		None = 0,
		Basic = 1,
		Elevated = 2,
		Bridge = 4,
		Slope = 8,
		Tunnel = 16,
		Unknown = 32
	}
}
