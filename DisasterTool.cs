﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using ColossalFramework.Threading;
using UnityEngine;

public class DisasterTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
	}

	protected override void OnToolGUI(Event e)
	{
		if (this.m_toolController.IsInsideUI)
		{
			return;
		}
		if (e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.CreateDisaster());
			}
			else if (e.get_button() == 1)
			{
				this.m_angleChanged = false;
			}
		}
		else if (e.get_type() == 1 && e.get_button() == 1 && !this.m_angleChanged)
		{
			Singleton<SimulationManager>.get_instance().AddAction(this.ToggleAngle());
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		base.ToolCursor = this.m_buildCursor;
		this.m_toolController.ClearColliding();
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		this.m_targetAcceptTimer = 0f;
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		this.m_placementErrors = ToolBase.ToolErrors.Pending;
		this.m_mouseRayValid = false;
		this.m_targetAcceptTimer = 0f;
	}

	public override void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
		DisasterInfo prefab = this.m_prefab;
		if ((this.m_toolController.m_mode & ItemClass.Availability.ScenarioEditor) != ItemClass.Availability.None && prefab != null && this.m_placementErrors != ToolBase.ToolErrors.Pending && this.m_placementErrors != ToolBase.ToolErrors.RaycastFailed && !this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			DisasterTool.DrawMarker(prefab, this.m_cachedPosition, this.m_cachedAngle);
		}
		base.RenderGeometry(cameraInfo);
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		DisasterInfo prefab = this.m_prefab;
		if ((prefab != null && this.m_placementErrors != ToolBase.ToolErrors.Pending && this.m_placementErrors != ToolBase.ToolErrors.RaycastFailed && !this.m_toolController.IsInsideUI && Cursor.get_visible()) || this.m_targetAcceptTimer != 0f)
		{
			Color toolColor = base.GetToolColor(false, this.m_placementErrors != ToolBase.ToolErrors.None);
			this.m_toolController.RenderColliding(cameraInfo, toolColor, toolColor, toolColor, toolColor, 0, 0);
			if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				float num = Mathf.Cos(this.m_targetAcceptTimer * 18.849556f) * 0.25f + 0.25f;
				toolColor..ctor(1f, 1f, 1f, num);
			}
			DisasterTool.RenderOverlay(cameraInfo, prefab, this.m_cachedPosition, this.m_cachedAngle, toolColor);
		}
		base.RenderOverlay(cameraInfo);
	}

	public static void RenderOverlay(RenderManager.CameraInfo cameraInfo, DisasterInfo info, Vector3 position, float angle, Color color)
	{
		if (info == null)
		{
			return;
		}
		ToolManager expr_17_cp_0 = Singleton<ToolManager>.get_instance();
		expr_17_cp_0.m_drawCallData.m_overlayCalls = expr_17_cp_0.m_drawCallData.m_overlayCalls + 1;
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.ScenarioEditor) != ItemClass.Availability.None)
		{
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, position, 100f, position.y - 100f, position.y + 100f, false, true);
		}
		else if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			DisasterProperties properties = Singleton<DisasterManager>.get_instance().m_properties;
			if (properties != null && properties.m_targetTexture != null)
			{
				Quad3 quad;
				quad.a = new Vector3(position.x - 200f, 0f, position.z - 200f);
				quad.b = new Vector3(position.x + 200f, 0f, position.z - 200f);
				quad.c = new Vector3(position.x + 200f, 0f, position.z + 200f);
				quad.d = new Vector3(position.x - 200f, 0f, position.z + 200f);
				Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, properties.m_targetTexture, color, quad, -10f, 1034f, false, true);
			}
		}
	}

	public static void CheckOverlayAlpha(DisasterInfo info, ref float alpha)
	{
		if (info == null)
		{
			return;
		}
		float num = 50f;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(num)));
	}

	public static void DrawMarker(DisasterInfo info, Vector3 pos, float angle)
	{
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		if (instance.m_properties == null)
		{
			return;
		}
		instance.m_materialBlock.Clear();
		instance.m_materialBlock.SetColor(instance.ID_Color, info.m_markerColor);
		Quaternion quaternion = Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
		Mesh markerMesh = info.m_markerMesh;
		ToolManager expr_5D_cp_0 = Singleton<ToolManager>.get_instance();
		expr_5D_cp_0.m_drawCallData.m_defaultCalls = expr_5D_cp_0.m_drawCallData.m_defaultCalls + 1;
		Graphics.DrawMesh(markerMesh, pos, quaternion, instance.m_properties.m_markerMaterial, 0, null, 0, instance.m_materialBlock, false, false);
	}

	protected override void OnToolUpdate()
	{
		DisasterInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			base.ShowToolInfo(true, null, this.m_cachedPosition);
		}
		else
		{
			base.ShowToolInfo(false, null, this.m_cachedPosition);
		}
		if (Input.GetKey(324))
		{
			float axis = Input.GetAxis("Mouse X");
			if (axis != 0f)
			{
				this.m_angleChanged = true;
				Singleton<SimulationManager>.get_instance().AddAction(this.DeltaAngle(axis * 10f));
			}
		}
	}

	protected override void OnToolLateUpdate()
	{
		DisasterInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		this.m_cachedPosition = this.m_mousePosition;
		this.m_cachedAngle = this.m_mouseAngle;
		if (this.m_targetAcceptTimer != 0f)
		{
			this.m_targetAcceptTimer = Mathf.Max(0f, this.m_targetAcceptTimer - Time.get_deltaTime());
		}
		ToolBase.OverrideInfoMode = false;
	}

	protected override string GetErrorString(ToolBase.ToolErrors e)
	{
		if (e == ToolBase.ToolErrors.Cooldown)
		{
			DisasterInfo prefab = this.m_prefab;
			int num = 0;
			if (prefab != null)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				uint lastSpawnFrame = prefab.m_lastSpawnFrame;
				if (lastSpawnFrame == 0u)
				{
					num = 99;
				}
				else if (lastSpawnFrame >= currentFrameIndex)
				{
					num = 0;
				}
				else
				{
					uint num2 = currentFrameIndex - lastSpawnFrame;
					if ((ulong)num2 >= (ulong)((long)prefab.m_cooldownFrames))
					{
						num = 99;
					}
					else
					{
						num = Mathf.Min(99, (int)(num2 * 100u / (uint)prefab.m_cooldownFrames));
					}
				}
			}
			return StringUtils.SafeFormat(Locale.Get("TOOL_ERROR", e.ToString()), new object[]
			{
				num,
				100
			});
		}
		return base.GetErrorString(e);
	}

	[DebuggerHidden]
	private IEnumerator CreateDisaster()
	{
		DisasterTool.<CreateDisaster>c__Iterator0 <CreateDisaster>c__Iterator = new DisasterTool.<CreateDisaster>c__Iterator0();
		<CreateDisaster>c__Iterator.$this = this;
		return <CreateDisaster>c__Iterator;
	}

	public static void DispatchPlacementEffect(Vector3 pos, bool bulldozing)
	{
		EffectInfo effectInfo;
		if (bulldozing)
		{
			effectInfo = Singleton<PropManager>.get_instance().m_properties.m_bulldozeEffect;
		}
		else
		{
			effectInfo = Singleton<PropManager>.get_instance().m_properties.m_placementEffect;
		}
		if (effectInfo != null)
		{
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(pos, Vector3.get_up(), 50f);
			Singleton<EffectManager>.get_instance().DispatchEffect(effectInfo, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
		}
	}

	private void DispatchTargetEffect()
	{
		ThreadHelper.get_dispatcher().Dispatch(delegate
		{
			DisasterProperties properties = Singleton<DisasterManager>.get_instance().m_properties;
			if (Singleton<AudioManager>.get_exists() && properties != null && properties.m_targetAcceptSound != null)
			{
				Singleton<AudioManager>.get_instance().PlaySound(properties.m_targetAcceptSound, 1f);
			}
		});
	}

	[DebuggerHidden]
	private IEnumerator DeltaAngle(float delta)
	{
		DisasterTool.<DeltaAngle>c__Iterator1 <DeltaAngle>c__Iterator = new DisasterTool.<DeltaAngle>c__Iterator1();
		<DeltaAngle>c__Iterator.delta = delta;
		<DeltaAngle>c__Iterator.$this = this;
		return <DeltaAngle>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator ToggleAngle()
	{
		DisasterTool.<ToggleAngle>c__Iterator2 <ToggleAngle>c__Iterator = new DisasterTool.<ToggleAngle>c__Iterator2();
		<ToggleAngle>c__Iterator.$this = this;
		return <ToggleAngle>c__Iterator;
	}

	public override void SimulationStep()
	{
		DisasterInfo prefab = this.m_prefab;
		if (prefab == null || this.m_targetAcceptTimer != 0f)
		{
			return;
		}
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
		{
			ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
			raycastOutput.m_hitPos.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(raycastOutput.m_hitPos, false, 0f);
			if (!Singleton<DisasterManager>.get_instance().CheckLimits())
			{
				toolErrors |= ToolBase.ToolErrors.TooManyObjects;
			}
			if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				if (Singleton<GameAreaManager>.get_instance().PointOutOfArea(raycastOutput.m_hitPos, 50f))
				{
					toolErrors |= ToolBase.ToolErrors.OutOfArea;
				}
				if (prefab.m_lastSpawnFrame != 0u)
				{
					uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
					uint lastSpawnFrame = prefab.m_lastSpawnFrame;
					if (lastSpawnFrame >= currentFrameIndex)
					{
						toolErrors |= ToolBase.ToolErrors.Cooldown;
					}
					else
					{
						uint num = currentFrameIndex - lastSpawnFrame;
						if ((ulong)num < (ulong)((long)prefab.m_cooldownFrames))
						{
							toolErrors |= ToolBase.ToolErrors.Cooldown;
						}
					}
				}
				this.m_mouseAngle = (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1000u) * 0.00628318544f;
			}
			else
			{
				this.m_mouseAngle = this.m_angle * 0.0174532924f;
			}
			this.m_mouseIntensity = this.m_intensity;
			this.m_mousePosition = raycastOutput.m_hitPos;
			this.m_placementErrors = toolErrors;
		}
		else
		{
			this.m_placementErrors = ToolBase.ToolErrors.RaycastFailed;
		}
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_placementErrors;
	}

	public DisasterInfo m_prefab;

	public float m_angle;

	public int m_intensity = 55;

	public CursorInfo m_buildCursor;

	private Vector3 m_mousePosition;

	private float m_mouseAngle;

	private int m_mouseIntensity;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private ToolBase.ToolErrors m_placementErrors;

	private Vector3 m_cachedPosition;

	private float m_cachedAngle;

	private bool m_angleChanged;

	private bool m_mouseRayValid;

	private float m_targetAcceptTimer;
}
