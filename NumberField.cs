﻿using System;
using System.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class NumberField : ToolsModifierControl
{
	public event NumberField.ValueChangedHandler eventValueChanged
	{
		add
		{
			NumberField.ValueChangedHandler valueChangedHandler = this.eventValueChanged;
			NumberField.ValueChangedHandler valueChangedHandler2;
			do
			{
				valueChangedHandler2 = valueChangedHandler;
				valueChangedHandler = Interlocked.CompareExchange<NumberField.ValueChangedHandler>(ref this.eventValueChanged, (NumberField.ValueChangedHandler)Delegate.Combine(valueChangedHandler2, value), valueChangedHandler);
			}
			while (valueChangedHandler != valueChangedHandler2);
		}
		remove
		{
			NumberField.ValueChangedHandler valueChangedHandler = this.eventValueChanged;
			NumberField.ValueChangedHandler valueChangedHandler2;
			do
			{
				valueChangedHandler2 = valueChangedHandler;
				valueChangedHandler = Interlocked.CompareExchange<NumberField.ValueChangedHandler>(ref this.eventValueChanged, (NumberField.ValueChangedHandler)Delegate.Remove(valueChangedHandler2, value), valueChangedHandler);
			}
			while (valueChangedHandler != valueChangedHandler2);
		}
	}

	public event NumberField.ValueChangedHandlerWithComponent eventValueChangedWithComponent
	{
		add
		{
			NumberField.ValueChangedHandlerWithComponent valueChangedHandlerWithComponent = this.eventValueChangedWithComponent;
			NumberField.ValueChangedHandlerWithComponent valueChangedHandlerWithComponent2;
			do
			{
				valueChangedHandlerWithComponent2 = valueChangedHandlerWithComponent;
				valueChangedHandlerWithComponent = Interlocked.CompareExchange<NumberField.ValueChangedHandlerWithComponent>(ref this.eventValueChangedWithComponent, (NumberField.ValueChangedHandlerWithComponent)Delegate.Combine(valueChangedHandlerWithComponent2, value), valueChangedHandlerWithComponent);
			}
			while (valueChangedHandlerWithComponent != valueChangedHandlerWithComponent2);
		}
		remove
		{
			NumberField.ValueChangedHandlerWithComponent valueChangedHandlerWithComponent = this.eventValueChangedWithComponent;
			NumberField.ValueChangedHandlerWithComponent valueChangedHandlerWithComponent2;
			do
			{
				valueChangedHandlerWithComponent2 = valueChangedHandlerWithComponent;
				valueChangedHandlerWithComponent = Interlocked.CompareExchange<NumberField.ValueChangedHandlerWithComponent>(ref this.eventValueChangedWithComponent, (NumberField.ValueChangedHandlerWithComponent)Delegate.Remove(valueChangedHandlerWithComponent2, value), valueChangedHandlerWithComponent);
			}
			while (valueChangedHandlerWithComponent != valueChangedHandlerWithComponent2);
		}
	}

	public int value
	{
		get
		{
			return this.m_value;
		}
		set
		{
			this.m_value = value;
			this.UpdateValue(true);
		}
	}

	private void Awake()
	{
		this.m_upArrow = base.Find<UIButton>("UpArrow");
		this.m_upArrow.add_eventMouseDown(new MouseEventHandler(this.OnUpArrow));
		this.m_downArrow = base.Find<UIButton>("DownArrow");
		this.m_downArrow.add_eventMouseDown(new MouseEventHandler(this.OnDownArrow));
		this.m_textField = base.Find<UITextField>("TextField");
		this.m_textField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnTextFieldSubmitted));
		this.m_label = base.Find<UILabel>("Label");
		this.m_value = this.m_defaultValue;
		this.UpdateValue(false);
	}

	public void SetLabel(string labelText)
	{
		this.m_label.set_text(labelText);
	}

	public void RoundToArrowStep()
	{
		float num = (float)this.m_value;
		num /= (float)this.m_arrowStep;
		this.m_value = Mathf.RoundToInt(num) * this.m_arrowStep;
	}

	private void UpdateValue(bool triggerValueChangedEvent = true)
	{
		this.m_value = Mathf.Clamp(this.m_value, this.m_minValue, this.m_maxValue);
		this.m_textField.set_text(this.m_value.ToString());
		this.UpdateArrowsActive();
		if (triggerValueChangedEvent)
		{
			if (this.eventValueChanged != null)
			{
				this.eventValueChanged(this.m_value);
			}
			if (this.eventValueChangedWithComponent != null)
			{
				this.eventValueChangedWithComponent(base.get_component(), this.m_value);
			}
		}
	}

	private void OnUpArrow(UIComponent c, UIMouseEventParameter m)
	{
		int value = this.m_value;
		this.RoundToArrowStep();
		if (this.m_value <= value)
		{
			this.m_value += this.m_arrowStep;
		}
		this.UpdateValue(true);
	}

	private void OnDownArrow(UIComponent c, UIMouseEventParameter m)
	{
		int value = this.m_value;
		this.RoundToArrowStep();
		if (this.m_value >= value)
		{
			this.m_value -= this.m_arrowStep;
		}
		this.UpdateValue(true);
	}

	private void OnTextFieldSubmitted(UIComponent c, string s)
	{
		int value = 0;
		if (int.TryParse(s, out value))
		{
			this.m_value = value;
		}
		this.UpdateValue(true);
	}

	private void UpdateArrowsActive()
	{
		if (this.m_value == this.m_minValue)
		{
			this.DisableArrow(this.m_downArrow);
		}
		else
		{
			this.EnableArrow(this.m_downArrow);
		}
		if (this.m_value == this.m_maxValue)
		{
			this.DisableArrow(this.m_upArrow);
		}
		else
		{
			this.EnableArrow(this.m_upArrow);
		}
	}

	private void DisableArrow(UIButton arrow)
	{
		if (arrow.get_isEnabled())
		{
			arrow.set_opacity(0.5f);
			arrow.Disable();
		}
	}

	private void EnableArrow(UIButton arrow)
	{
		if (!arrow.get_isEnabled())
		{
			arrow.set_opacity(1f);
			arrow.Enable();
		}
	}

	public int m_defaultValue = 5;

	public int m_minValue;

	public int m_maxValue = 99;

	public int m_arrowStep = 1;

	private UIButton m_upArrow;

	private UIButton m_downArrow;

	private UITextField m_textField;

	private UILabel m_label;

	private int m_value;

	public delegate void ValueChangedHandler(int value);

	public delegate void ValueChangedHandlerWithComponent(UIComponent comp, int value);
}
