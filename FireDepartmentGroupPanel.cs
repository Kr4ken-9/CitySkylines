﻿using System;

public class FireDepartmentGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.FireDepartment;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		ItemClass.Service service = info.GetService();
		return service == this.service || service == ItemClass.Service.Disaster;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Building;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "FireDepartmentFire")
			{
				return 0;
			}
			if (name == "FireDepartmentDisaster")
			{
				return 1;
			}
		}
		return 2147483647;
	}
}
