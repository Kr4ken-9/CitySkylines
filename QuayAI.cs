﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class QuayAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 0f;
		capped = false;
		color..ctor(0f, 0f, 0f, 0f);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 vector;
		vector..ctor(data.m_startDirection.z, 0f, -data.m_startDirection.x);
		Vector3 vector2;
		vector2..ctor(data.m_endDirection.z, 0f, -data.m_endDirection.x);
		float num = QuayAI.SampleMinHeight(position + vector * (this.m_info.m_halfWidth + 8f));
		float num2 = QuayAI.SampleMinHeight(position - vector * (this.m_info.m_halfWidth + 8f));
		float num3 = QuayAI.SampleMinHeight(position2 + vector2 * (this.m_info.m_halfWidth + 8f));
		float num4 = QuayAI.SampleMinHeight(position2 - vector2 * (this.m_info.m_halfWidth + 8f));
		if (num2 + num3 > num + num4)
		{
			data.m_flags |= NetSegment.Flags.Invert;
		}
		else
		{
			data.m_flags &= ~NetSegment.Flags.Invert;
		}
	}

	private static float SampleMinHeight(Vector3 worldPos)
	{
		float num = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos);
		float num2 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(worldPos);
		return Mathf.Min(num, num2);
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (maxElevation > -this.m_info.m_minHeight)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + 1000f;
	}

	public override bool IgnoreWater()
	{
		return true;
	}

	public override bool DisplayTempSegment()
	{
		return true;
	}

	public override bool Snap(ushort segmentID, ref NetSegment data, NetInfo info, Vector3 refPos, float elevation, out float distance, out Vector3 snapPos, out Vector3 snapDir)
	{
		distance = 0f;
		snapPos = refPos;
		snapDir = Vector3.get_forward();
		if (info.m_class.m_layer != ItemClass.Layer.Default)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		data.GetClosestPositionAndDirection(refPos, out snapPos, out snapDir);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		float num = Vector2.Dot(VectorUtils.XZ(refPos - position), VectorUtils.XZ(data.m_startDirection));
		float num2 = Vector2.Dot(VectorUtils.XZ(refPos - position2), VectorUtils.XZ(data.m_endDirection));
		snapDir.y = 0f;
		snapDir.Normalize();
		bool flag = false;
		if (num <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position - refPos);
			snapDir.y = 0f;
		}
		else if (num2 <= -4f)
		{
			snapDir = VectorUtils.NormalizeXZ(position2 - refPos);
			snapDir.y = 0f;
		}
		else
		{
			if (NetSegment.IsStraight(position, data.m_startDirection, position2, data.m_endDirection))
			{
				Vector3 vector = snapPos - position;
				Vector3 vector2 = snapPos - position2;
				if (vector2.get_sqrMagnitude() < vector.get_sqrMagnitude())
				{
					float num3 = Vector3.Dot(snapDir, vector2);
					num3 = Mathf.Round(num3 * 0.125f) * 8f - num3;
					snapPos += snapDir * num3;
				}
				else
				{
					float num4 = Vector3.Dot(snapDir, vector);
					num4 = Mathf.Round(num4 * 0.125f) * 8f - num4;
					snapPos += snapDir * num4;
				}
			}
			snapDir..ctor(snapDir.z, 0f, -snapDir.x);
			flag = (Vector3.Dot(snapPos - refPos, snapDir) < 0f);
			if (flag)
			{
				snapDir = -snapDir;
			}
		}
		distance = Vector3.Distance(snapPos, refPos);
		float num5 = this.GetCollisionHalfWidth() + Mathf.Ceil(info.m_halfWidth * 0.25f - 0.01f) * 4f;
		if (info.m_class.m_service == ItemClass.Service.Beautification && info.m_class.m_level >= ItemClass.Level.Level3)
		{
			if (distance < num5 + 1f)
			{
				num5 += 1f;
				snapPos -= snapDir * num5;
				snapPos.y = NetSegment.SampleTerrainHeight(info, snapPos, false, elevation);
				return true;
			}
		}
		else if (distance < num5 + 19f)
		{
			if (info.m_class.m_service != ItemClass.Service.Electricity && (distance > num5 + 4f || elevation < 1f))
			{
				num5 += 8f;
				elevation += 1f;
			}
			snapPos -= snapDir * num5;
			snapPos.y = QuayAI.GetActualHeight(segmentID, ref data, snapPos, info.m_halfWidth, flag) + elevation;
			return true;
		}
		return false;
	}

	private static float GetActualHeight(ushort segmentID, ref NetSegment data, Vector3 refPos, float halfWidth, bool right)
	{
		Vector3 vector;
		Vector3 startDir;
		bool smoothStart;
		data.CalculateCorner(segmentID, true, true, !right, out vector, out startDir, out smoothStart);
		Vector3 vector2;
		Vector3 endDir;
		bool smoothEnd;
		data.CalculateCorner(segmentID, true, false, right, out vector2, out endDir, out smoothEnd);
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateMiddlePoints(vector, startDir, vector2, endDir, smoothStart, smoothEnd, out vector3, out vector4);
		Bezier3 bezier;
		bezier..ctor(vector, vector3, vector4, vector2);
		float num;
		bezier.DistanceSqr(refPos, ref num);
		return bezier.Position(num).y + Mathf.Abs(VectorUtils.NormalizeXZ(bezier.Tangent(num)).y) * halfWidth * 1.5f;
	}

	public override float HeightSampleDistance()
	{
		return 5f;
	}

	public override bool LinearMiddleHeight()
	{
		return true;
	}

	public override bool SnapShoreLine()
	{
		return true;
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		if ((data.m_flags & NetNode.Flags.Bend) == NetNode.Flags.None)
		{
			return false;
		}
		switch (index)
		{
		case 0:
			heights = TerrainModify.Heights.None;
			leftT = 0.5f - 5f / (this.m_info.m_halfWidth * 2f);
			rightT = 0.5f + 5f / (this.m_info.m_halfWidth * 2f);
			return true;
		case 1:
			surface = TerrainModify.Surface.None;
			heights = (TerrainModify.Heights.PrimaryLevel | TerrainModify.Heights.BlockHeight | TerrainModify.Heights.RawHeight);
			if (QuayAI.GetInvertDirection(nodeID, segment1, segment2))
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0.5f - 5f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 1f / (this.m_info.m_halfWidth * 2f);
			}
			else
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f - 1f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 5f / (this.m_info.m_halfWidth * 2f);
			}
			return true;
		case 2:
		{
			surface = TerrainModify.Surface.None;
			heights = TerrainModify.Heights.PrimaryMax;
			if (QuayAI.GetInvertDirection(nodeID, segment1, segment2))
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f + 1f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 15f / (this.m_info.m_halfWidth * 2f);
			}
			else
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0.5f - 15f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f - 1f / (this.m_info.m_halfWidth * 2f);
			}
			float num = (float)(-(float)data.m_elevation);
			leftY = Mathf.Min(-1f, num);
			rightY = Mathf.Min(-1f, num);
			return true;
		}
		case 3:
			surface = TerrainModify.Surface.None;
			heights = (TerrainModify.Heights.SecondaryLevel | TerrainModify.Heights.BlockHeight | TerrainModify.Heights.RawHeight);
			if (QuayAI.GetInvertDirection(nodeID, segment1, segment2))
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0f;
				rightT = 0.5f - 8f / (this.m_info.m_halfWidth * 2f);
				leftY = this.SampleSlopeHeightOffset(nodeID, segment1, segment2);
				rightY = 0f;
			}
			else
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f + 8f / (this.m_info.m_halfWidth * 2f);
				rightT = 1f;
				leftY = 0f;
				rightY = this.SampleSlopeHeightOffset(nodeID, segment1, segment2);
			}
			return true;
		default:
			return false;
		}
	}

	private static bool GetInvertDirection(ushort nodeID, ushort segment1, ushort segment2)
	{
		if (segment1 == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		return instance.m_segments.m_buffer[(int)segment1].m_startNode != nodeID == ((instance.m_segments.m_buffer[(int)segment1].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None);
	}

	private float SampleSlopeHeightOffset(ushort nodeID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = 0;
		ushort segment = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment2 = instance.m_nodes.m_buffer[(int)nodeID].GetSegment(i);
			if (segment2 != 0)
			{
				if (num == 0)
				{
					num = segment2;
				}
				else
				{
					segment = segment2;
				}
			}
		}
		return this.SampleSlopeHeightOffset(nodeID, num, segment);
	}

	private float SampleSlopeHeightOffset(ushort nodeID, ushort segment1, ushort segment2)
	{
		if (segment1 == 0)
		{
			return 0f;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		bool flag = instance.m_segments.m_buffer[(int)segment1].m_startNode == nodeID;
		bool flag2 = flag == ((instance.m_segments.m_buffer[(int)segment1].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None);
		Vector3 vector;
		if (flag)
		{
			vector = instance.m_segments.m_buffer[(int)segment1].m_startDirection;
		}
		else
		{
			vector = instance.m_segments.m_buffer[(int)segment1].m_endDirection;
		}
		if (flag2)
		{
			vector..ctor(-vector.z, 0f, vector.x);
		}
		else
		{
			vector..ctor(vector.z, 0f, -vector.x);
		}
		if (segment2 != 0)
		{
			bool flag3 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID;
			Vector3 vector2;
			if (flag3)
			{
				vector2 = instance.m_segments.m_buffer[(int)segment2].m_startDirection;
			}
			else
			{
				vector2 = instance.m_segments.m_buffer[(int)segment2].m_endDirection;
			}
			if (flag2)
			{
				vector2..ctor(vector2.z, 0f, -vector2.x);
			}
			else
			{
				vector2..ctor(-vector2.z, 0f, vector2.x);
			}
			vector = (vector + vector2).get_normalized();
		}
		Vector3 position = instance.m_nodes.m_buffer[(int)nodeID].m_position;
		float num = instance2.SampleOriginalRawHeightSmooth(position + vector * (this.m_info.m_halfWidth + 16f)) - position.y;
		return num * (this.m_info.m_halfWidth - 8f) / (this.m_info.m_halfWidth + 8f);
	}

	public override bool SegmentModifyMask(ushort segmentID, ref NetSegment data, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftStartY, ref float rightStartY, ref float leftEndY, ref float rightEndY)
	{
		switch (index)
		{
		case 0:
			heights = TerrainModify.Heights.None;
			leftT = 0.5f - 5f / (this.m_info.m_halfWidth * 2f);
			rightT = 0.5f + 5f / (this.m_info.m_halfWidth * 2f);
			return true;
		case 1:
			surface = TerrainModify.Surface.None;
			heights = (TerrainModify.Heights.PrimaryLevel | TerrainModify.Heights.BlockHeight | TerrainModify.Heights.RawHeight);
			if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0.5f - 5f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 1f / (this.m_info.m_halfWidth * 2f);
			}
			else
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f - 1f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 5f / (this.m_info.m_halfWidth * 2f);
			}
			return true;
		case 2:
		{
			surface = TerrainModify.Surface.None;
			heights = TerrainModify.Heights.PrimaryMax;
			if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f + 1f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f + 15f / (this.m_info.m_halfWidth * 2f);
			}
			else
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0.5f - 15f / (this.m_info.m_halfWidth * 2f);
				rightT = 0.5f - 1f / (this.m_info.m_halfWidth * 2f);
			}
			float num = (float)(-(float)Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_startNode].m_elevation);
			float num2 = (float)(-(float)Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_endNode].m_elevation);
			leftStartY = Mathf.Min(-1f, (num + num2) * 0.5f);
			rightStartY = Mathf.Min(-1f, (num + num2) * 0.5f);
			leftEndY = leftStartY;
			rightEndY = rightStartY;
			return true;
		}
		case 3:
			surface = TerrainModify.Surface.None;
			heights = (TerrainModify.Heights.SecondaryLevel | TerrainModify.Heights.BlockHeight | TerrainModify.Heights.RawHeight);
			if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				edges = (TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.DA);
				leftT = 0f;
				rightT = 0.5f - 8f / (this.m_info.m_halfWidth * 2f);
				leftStartY = this.SampleSlopeHeightOffset(data.m_startNode);
				rightStartY = 0f;
				leftEndY = this.SampleSlopeHeightOffset(data.m_endNode);
				rightEndY = 0f;
			}
			else
			{
				edges = (TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA);
				leftT = 0.5f + 8f / (this.m_info.m_halfWidth * 2f);
				rightT = 1f;
				leftStartY = 0f;
				rightStartY = this.SampleSlopeHeightOffset(data.m_startNode);
				leftEndY = 0f;
				rightEndY = this.SampleSlopeHeightOffset(data.m_endNode);
			}
			return true;
		default:
			return false;
		}
	}

	public override float GetCollisionHalfWidth()
	{
		return 5f;
	}

	public override void UpdateNodeFlags(ushort nodeID, ref NetNode data)
	{
		base.UpdateNodeFlags(nodeID, ref data);
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		base.UpdateNode(nodeID, ref data);
	}

	public override void UpdateLaneConnection(ushort nodeID, ref NetNode data)
	{
	}

	public override void UpdateGuide(GuideController guideController)
	{
		GenericGuide quaysNotUsed = Singleton<NetManager>.get_instance().m_quaysNotUsed;
		if (quaysNotUsed != null)
		{
			int constructionCost = this.GetConstructionCost();
			if (Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) == constructionCost)
			{
				quaysNotUsed.Activate(guideController.m_quaysNotUsed);
			}
		}
	}

	public override void ConstructSucceeded()
	{
		GenericGuide quaysNotUsed = Singleton<NetManager>.get_instance().m_quaysNotUsed;
		if (quaysNotUsed != null)
		{
			quaysNotUsed.Disable();
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		NetManager instance = Singleton<NetManager>.get_instance();
		TerrainManager instance2 = Singleton<TerrainManager>.get_instance();
		if (startPoint.m_node != 0)
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)startPoint.m_node].Info;
			if (info.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
			else
			{
				float y = instance.m_nodes.m_buffer[(int)startPoint.m_node].m_position.y;
				startPoint.m_elevation = y - startPoint.m_position.y;
				startPoint.m_position.y = y;
				if (instance.m_nodes.m_buffer[(int)startPoint.m_node].CountSegments() > 1)
				{
					toolErrors |= ToolBase.ToolErrors.InvalidShape;
				}
			}
		}
		else if (startPoint.m_segment != 0)
		{
			toolErrors |= ToolBase.ToolErrors.InvalidShape;
		}
		else
		{
			float num = startPoint.m_position.y;
			Vector3 vector;
			vector..ctor(middlePoint.m_direction.z, 0f, -middlePoint.m_direction.x);
			num = Mathf.Max(num, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(startPoint.m_position + vector * (this.m_info.m_halfWidth + 8f)));
			num = Mathf.Max(num, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(startPoint.m_position - vector * (this.m_info.m_halfWidth + 8f)));
			startPoint.m_elevation = num - startPoint.m_position.y;
			startPoint.m_position.y = num;
		}
		if (endPoint.m_node != 0)
		{
			NetInfo info2 = instance.m_nodes.m_buffer[(int)endPoint.m_node].Info;
			if (info2.m_class.m_level != this.m_info.m_class.m_level)
			{
				toolErrors |= ToolBase.ToolErrors.InvalidShape;
			}
			else
			{
				float y2 = instance.m_nodes.m_buffer[(int)endPoint.m_node].m_position.y;
				endPoint.m_elevation = y2 - endPoint.m_position.y;
				endPoint.m_position.y = y2;
				if (instance.m_nodes.m_buffer[(int)endPoint.m_node].CountSegments() > 1)
				{
					toolErrors |= ToolBase.ToolErrors.InvalidShape;
				}
			}
		}
		else if (endPoint.m_segment != 0)
		{
			toolErrors |= ToolBase.ToolErrors.InvalidShape;
		}
		else
		{
			float num2 = endPoint.m_position.y;
			Vector3 vector2;
			vector2..ctor(endPoint.m_direction.z, 0f, -endPoint.m_direction.x);
			num2 = Mathf.Max(num2, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(endPoint.m_position + vector2 * (this.m_info.m_halfWidth + 8f)));
			num2 = Mathf.Max(num2, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(endPoint.m_position - vector2 * (this.m_info.m_halfWidth + 8f)));
			endPoint.m_elevation = num2 - endPoint.m_position.y;
			endPoint.m_position.y = num2;
		}
		middlePoint.m_elevation = (startPoint.m_elevation + endPoint.m_elevation) * 0.5f;
		middlePoint.m_position.y = (startPoint.m_position.y + endPoint.m_position.y) * 0.5f;
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, true, true, out vector3, out vector4);
		Bezier2 bezier;
		bezier.a = VectorUtils.XZ(startPoint.m_position);
		bezier.b = VectorUtils.XZ(vector3);
		bezier.c = VectorUtils.XZ(vector4);
		bezier.d = VectorUtils.XZ(endPoint.m_position);
		int num3 = Mathf.CeilToInt(Vector2.Distance(bezier.a, bezier.d) * 0.005f) + 3;
		Vector2 vector5;
		vector5..ctor(middlePoint.m_direction.z, -middlePoint.m_direction.x);
		Segment2 segment;
		segment.a = bezier.a + vector5 * this.m_info.m_halfWidth;
		Segment2 segment2;
		segment2.a = bezier.a - vector5 * this.m_info.m_halfWidth;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		bool flag4 = false;
		for (int i = 1; i <= num3; i++)
		{
			Vector2 vector6 = bezier.Position((float)i / (float)num3);
			Vector2 vector7 = bezier.Tangent((float)i / (float)num3);
			Vector2 vector8;
			vector8..ctor(vector7.y, -vector7.x);
			vector7 = vector8.get_normalized();
			segment.b = vector6 + vector7 * this.m_info.m_halfWidth;
			segment2.b = vector6 - vector7 * this.m_info.m_halfWidth;
			if (instance2.HasWater(segment, 12f, true))
			{
				flag = true;
			}
			else
			{
				flag2 = true;
			}
			if (instance2.HasWater(segment2, 12f, true))
			{
				flag3 = true;
			}
			else
			{
				flag4 = true;
			}
			segment.a = segment.b;
			segment2.a = segment2.b;
		}
		if ((flag == flag2 || flag3 == flag4 || flag == flag3) && !Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor))
		{
			toolErrors |= ToolBase.ToolErrors.ShoreNotFound;
		}
		return toolErrors;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.RoundToInt(vector.z * 255f);
		result.g = 255;
		result.a = 0;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.b = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		result.g = 255;
		result.a = 0;
		return result;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		if ((data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
		{
			leftMin = 0f;
			rightMin = this.m_info.m_minHeight;
		}
		else
		{
			leftMin = this.m_info.m_minHeight;
			rightMin = 0f;
		}
		max = 0f;
	}

	public override bool CanConnect(NetInfo other)
	{
		return other.m_class.m_level == this.m_info.m_class.m_level;
	}

	public override ItemClass.Layer GetCollisionLayers()
	{
		return this.m_info.m_class.m_layer | ItemClass.Layer.WaterStructures;
	}
}
