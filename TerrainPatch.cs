﻿using System;
using System.Threading;
using ColossalFramework;
using UnityEngine;

public class TerrainPatch
{
	public TerrainPatch(int x, int z)
	{
		this.m_x = x;
		this.m_z = z;
		this.m_modifiedLock = new object();
		this.m_heightMap = new Texture2D(128, 128, 5, false, true);
		this.m_heightMap.set_wrapMode(1);
		this.m_heightMap.set_filterMode(0);
		this.m_surfaceMapA = new Texture2D(128, 128, 5, false, true);
		this.m_surfaceMapA.set_wrapMode(1);
		this.m_surfaceMapA.set_filterMode(1);
		this.m_surfaceMapB = new Texture2D(128, 128, 5, false, true);
		this.m_surfaceMapB.set_wrapMode(1);
		this.m_surfaceMapB.set_filterMode(1);
		this.m_waterHeight = new TerrainPatch.WaterHeightFrame[2];
		for (int i = 0; i < 2; i++)
		{
			this.m_waterHeight[i].m_waterHeight = new Texture2D(128, 128, 5, false, true);
			this.m_waterHeight[i].m_waterHeight.set_wrapMode(1);
			this.m_waterHeight[i].m_waterHeight.set_filterMode(0);
		}
		this.m_waterSurfaceA = new Texture2D[3];
		this.m_waterSurfaceB = new Texture2D[3];
		for (int j = 0; j < 3; j++)
		{
			this.m_waterSurfaceA[j] = new Texture2D(64, 64, 3, false, true);
			this.m_waterSurfaceA[j].set_wrapMode(1);
			this.m_waterSurfaceA[j].set_filterMode(1);
			this.m_waterSurfaceB[j] = new Texture2D(64, 64, 3, false, true);
			this.m_waterSurfaceB[j].set_wrapMode(1);
			this.m_waterSurfaceB[j].set_filterMode(1);
		}
		int num = 120;
		int num2 = 128 - num >> 1;
		this.m_surfaceModified = new TerrainArea(x * num - num2, z * num - num2, 129);
		this.m_surfaceModified.AddArea(0, 0, 1080, 1080);
		this.m_zonesModified = new TerrainArea(x * num - num2, z * num - num2, 128);
		this.m_zonesModified.AddArea(0, 0, 1080, 1080);
		int num3 = 120;
		float num4 = 17280f;
		float num5 = (float)num3 * 16f;
		this.m_heightMappingRaw.x = 0.0078125f;
		this.m_heightMappingRaw.y = 1024f;
		this.m_heightMappingRaw.z = 0.0078125f;
		this.m_heightMappingRaw.w = 0f;
		this.m_surfaceMappingRaw.z = (float)num3 / (128f * num5);
		this.m_surfaceMappingRaw.x = (num4 * 0.5f - (float)this.m_x * num5) * this.m_surfaceMappingRaw.z + (float)(128 - num3) * 0.00390625f;
		this.m_surfaceMappingRaw.y = (num4 * 0.5f - (float)this.m_z * num5) * this.m_surfaceMappingRaw.z + (float)(128 - num3) * 0.00390625f;
		this.m_surfaceMappingRaw.w = 0f;
		int num6 = 480;
		float num7 = 17280f;
		float num8 = (float)num6 * 4f;
		this.m_heightMappingDetail.x = 0.001953125f;
		this.m_heightMappingDetail.y = 1024f;
		this.m_heightMappingDetail.z = 0.001953125f;
		this.m_heightMappingDetail.w = 1f;
		this.m_surfaceMappingDetail.z = (float)num6 / (512f * num8);
		this.m_surfaceMappingDetail.x = (num7 * 0.5f - (float)this.m_x * num8) * this.m_surfaceMappingDetail.z + (float)(512 - num6) * 0.0009765625f;
		this.m_surfaceMappingDetail.y = (num7 * 0.5f - (float)this.m_z * num8) * this.m_surfaceMappingDetail.z + (float)(512 - num6) * 0.0009765625f;
		this.m_surfaceMappingDetail.w = 1f;
	}

	public void DestroyPatch()
	{
		if (this.m_heightMap != null)
		{
			Object.Destroy(this.m_heightMap);
			this.m_heightMap = null;
		}
		if (this.m_surfaceMapA != null)
		{
			Object.Destroy(this.m_surfaceMapA);
			this.m_surfaceMapA = null;
		}
		if (this.m_surfaceMapB != null)
		{
			Object.Destroy(this.m_surfaceMapB);
			this.m_surfaceMapB = null;
		}
		if (this.m_zoneLayout != null)
		{
			Object.Destroy(this.m_zoneLayout);
			this.m_zoneLayout = null;
		}
		if (this.m_waterHeight != null)
		{
			for (int i = 0; i < this.m_waterHeight.Length; i++)
			{
				if (this.m_waterHeight[i].m_waterHeight != null)
				{
					Object.Destroy(this.m_waterHeight[i].m_waterHeight);
					this.m_waterHeight[i].m_waterHeight = null;
				}
			}
		}
		if (this.m_waterSurfaceA != null)
		{
			for (int j = 0; j < this.m_waterSurfaceA.Length; j++)
			{
				if (this.m_waterSurfaceA[j] != null)
				{
					Object.Destroy(this.m_waterSurfaceA[j]);
					this.m_waterSurfaceA[j] = null;
				}
			}
		}
		if (this.m_waterSurfaceB != null)
		{
			for (int k = 0; k < this.m_waterSurfaceB.Length; k++)
			{
				if (this.m_waterSurfaceB[k] != null)
				{
					Object.Destroy(this.m_waterSurfaceB[k]);
					this.m_waterSurfaceB[k] = null;
				}
			}
		}
	}

	public void ResizeControlTextures(bool hasDetails)
	{
		if (hasDetails)
		{
			this.m_heightMap.Resize(512, 512, 5, false);
			this.m_heightMap.set_wrapMode(1);
			this.m_heightMap.set_filterMode(0);
			this.m_surfaceMapA.Resize(512, 512, 5, false);
			this.m_surfaceMapA.set_wrapMode(1);
			this.m_surfaceMapA.set_filterMode(1);
			this.m_surfaceMapB.Resize(512, 512, 5, false);
			this.m_surfaceMapB.set_wrapMode(1);
			this.m_surfaceMapB.set_filterMode(1);
			if (this.m_zoneLayout == null)
			{
				this.m_zoneLayout = new Texture2D(512, 512, 5, false, true);
				this.m_zoneLayout.set_wrapMode(1);
				this.m_zoneLayout.set_filterMode(0);
			}
		}
		else
		{
			this.m_heightMap.Resize(128, 128, 5, false);
			this.m_heightMap.set_wrapMode(1);
			this.m_heightMap.set_filterMode(0);
			this.m_surfaceMapA.Resize(128, 128, 5, false);
			this.m_surfaceMapA.set_wrapMode(1);
			this.m_surfaceMapA.set_filterMode(1);
			this.m_surfaceMapB.Resize(128, 128, 5, false);
			this.m_surfaceMapB.set_wrapMode(1);
			this.m_surfaceMapB.set_filterMode(1);
			if (this.m_zoneLayout != null)
			{
				Object.Destroy(this.m_zoneLayout);
				this.m_zoneLayout = null;
			}
		}
	}

	public void Refresh(bool updateWater, uint waterFrame)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		ushort[] finalHeights = instance.FinalHeights;
		ushort[] blockHeightTargets = instance.BlockHeightTargets;
		TerrainManager.SurfaceCell[] rawSurface = instance.RawSurface;
		int num = 120;
		float num2 = 17280f;
		float num3 = (float)num * 16f;
		int num4 = num * this.m_x;
		int num5 = num * (this.m_x + 1);
		int num6 = num * this.m_z;
		int num7 = num * (this.m_z + 1);
		TerrainManager.CellBounds bounds = instance.GetBounds(this.m_x, this.m_z, 0);
		this.m_terrainMinY = (float)bounds.m_minHeight * 0.015625f;
		this.m_terrainMaxY = (float)bounds.m_maxHeight * 0.015625f;
		this.m_terrainPosition.x = ((float)this.m_x + 0.5f) * num3 - num2 * 0.5f;
		this.m_terrainPosition.y = (this.m_terrainMinY + this.m_terrainMaxY) * 0.5f;
		this.m_terrainPosition.z = ((float)this.m_z + 0.5f) * num3 - num2 * 0.5f;
		int num8 = this.m_rndDetailIndex;
		while (!Monitor.TryEnter(this.m_modifiedLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			TerrainArea terrainArea = instance.m_tmpSurfaceModified;
			instance.m_tmpSurfaceModified = this.m_surfaceModified;
			terrainArea.m_x = instance.m_tmpSurfaceModified.m_x;
			terrainArea.m_z = instance.m_tmpSurfaceModified.m_z;
			terrainArea.Reset();
			this.m_surfaceModified = terrainArea;
			terrainArea = instance.m_tmpZonesModified;
			instance.m_tmpZonesModified = this.m_zonesModified;
			terrainArea.m_x = instance.m_tmpZonesModified.m_x;
			terrainArea.m_z = instance.m_tmpZonesModified.m_z;
			terrainArea.Reset();
			this.m_zonesModified = terrainArea;
			num8 = this.m_tmpDetailIndex;
		}
		finally
		{
			Monitor.Exit(this.m_modifiedLock);
		}
		if (num8 != this.m_rndDetailIndex)
		{
			if (num8 != 0 != (this.m_rndDetailIndex != 0))
			{
				this.ResizeControlTextures(num8 != 0);
			}
			this.m_rndDetailIndex = num8;
		}
		int num9 = 128 - num >> 1;
		if (instance.m_tmpSurfaceModified.m_minX - num9 <= num5 && instance.m_tmpSurfaceModified.m_maxX >= num4 - num9 && instance.m_tmpSurfaceModified.m_minZ - num9 <= num7 && instance.m_tmpSurfaceModified.m_maxZ >= num6 - num9)
		{
			if (this.m_rndDetailIndex != 0)
			{
				num9 = 16;
				int num10 = 256 - 480 * this.m_x - 240;
				int num11 = 256 - 480 * this.m_z - 240;
				int num12 = Mathf.Max((num4 << 2) - num9, instance.m_tmpSurfaceModified.m_minX << 2);
				int num13 = Mathf.Min((num5 << 2) + num9 - 1, instance.m_tmpSurfaceModified.m_maxX << 2);
				int num14 = Mathf.Max((num6 << 2) - num9, instance.m_tmpSurfaceModified.m_minZ << 2);
				int num15 = Mathf.Min((num7 << 2) + num9 - 1, instance.m_tmpSurfaceModified.m_maxZ << 2);
				float num16 = 0.001953125f;
				for (int i = num14; i <= num15; i++)
				{
					int num17;
					int num18;
					instance.m_tmpSurfaceModified.GetLimitsX(i >> 2, out num17, out num18);
					num17 = Mathf.Max(num17 << 2, num12);
					num18 = Mathf.Min(num18 << 2, num13);
					int x = Mathf.Max(0, num17);
					int z = Mathf.Max(0, i);
					int z2 = Mathf.Min(4320, i + 1);
					float num19 = instance.GetDetailHeight(x, z);
					float num20 = instance.GetDetailHeight(x, z2);
					float num21 = instance.GetBlockHeight(x, z);
					for (int j = num17; j <= num18; j++)
					{
						int x2 = Mathf.Min(4320, j + 1);
						float detailHeight = instance.GetDetailHeight(x2, z);
						float detailHeight2 = instance.GetDetailHeight(x2, z2);
						float blockHeight = instance.GetBlockHeight(x2, z);
						int num22;
						int num23;
						instance.m_tmpSurfaceModified.GetLimitsZ(j >> 2, out num22, out num23);
						if (i >= num22 << 2 && i <= num23 << 2)
						{
							int num24 = Mathf.RoundToInt(num19 * 256f);
							int num25 = Mathf.RoundToInt(num21 * 256f);
							Vector3 vector;
							vector..ctor((num19 + num20 - detailHeight - detailHeight2) * num16, 1f, (num19 + detailHeight - num20 - detailHeight2) * num16);
							vector.Normalize();
							TerrainManager.SurfaceCell surfaceCell = instance.GetSurfaceCell(Mathf.Clamp(j, 0, 4319), Mathf.Clamp(i, 0, 4319));
							Color color;
							color.r = (float)(num24 >> 16) * 0.003921569f;
							color.g = (float)(num24 >> 8 & 255) * 0.003921569f;
							color.b = (float)(num25 >> 16) * 0.003921569f;
							color.a = (float)(num25 >> 8 & 255) * 0.003921569f;
							Color color2;
							color2.r = (float)surfaceCell.m_clipped * 0.003921569f;
							color2.g = (float)surfaceCell.m_pavementA * 0.003921569f;
							color2.b = vector.x * 0.498039216f + 0.498039216f;
							color2.a = vector.z * 0.498039216f + 0.498039216f;
							Color color3;
							color3.r = (float)surfaceCell.m_ruined * 0.003921569f;
							color3.g = (float)surfaceCell.m_pavementB * 0.003921569f;
							color3.b = (float)surfaceCell.m_gravel * 0.003921569f;
							color3.a = (float)surfaceCell.m_field * 0.003921569f;
							this.m_heightMap.SetPixel(j + num10, i + num11, color);
							this.m_surfaceMapA.SetPixel(j + num10, i + num11, color2);
							this.m_surfaceMapB.SetPixel(j + num10, i + num11, color3);
						}
						num19 = detailHeight;
						num20 = detailHeight2;
						num21 = blockHeight;
					}
				}
			}
			else
			{
				int num26 = 64 - num * this.m_x - (num >> 1);
				int num27 = 64 - num * this.m_z - (num >> 1);
				int num28 = Mathf.Max(num4 - num9, instance.m_tmpSurfaceModified.m_minX);
				int num29 = Mathf.Min(num5 + num9 - 1, instance.m_tmpSurfaceModified.m_maxX);
				int num30 = Mathf.Max(num6 - num9, instance.m_tmpSurfaceModified.m_minZ);
				int num31 = Mathf.Min(num7 + num9 - 1, instance.m_tmpSurfaceModified.m_maxZ);
				float num32 = 0.00048828125f;
				for (int k = num30; k <= num31; k++)
				{
					int num33;
					int num34;
					instance.m_tmpSurfaceModified.GetLimitsX(k, out num33, out num34);
					num33 = Mathf.Max(num33, num28);
					num34 = Mathf.Min(num34, num29);
					for (int l = num33; l <= num34; l++)
					{
						int num35;
						int num36;
						instance.m_tmpSurfaceModified.GetLimitsZ(l, out num35, out num36);
						if (k >= num35 && k <= num36)
						{
							int num37 = Mathf.Max(0, l);
							int num38 = Mathf.Min(1080, l + 1);
							int num39 = Mathf.Max(0, k);
							int num40 = Mathf.Min(1080, k + 1);
							int num41 = (int)finalHeights[num39 * 1081 + num37];
							int num42 = (int)finalHeights[num39 * 1081 + num38];
							int num43 = (int)finalHeights[num40 * 1081 + num37];
							int num44 = (int)finalHeights[num40 * 1081 + num38];
							int num45 = (int)blockHeightTargets[num39 * 1081 + num37];
							int num46 = num41 << 8;
							int num47 = num45 << 8;
							Vector3 vector2;
							vector2..ctor((float)(num41 + num43 - num42 - num44) * num32, 1f, (float)(num41 + num42 - num43 - num44) * num32);
							vector2.Normalize();
							TerrainManager.SurfaceCell surfaceCell2 = rawSurface[Mathf.Clamp(k, 0, 1079) * 1080 + Mathf.Clamp(l, 0, 1079)];
							Color color4;
							color4.r = (float)(num46 >> 16) * 0.003921569f;
							color4.g = (float)(num46 >> 8 & 255) * 0.003921569f;
							color4.b = (float)(num47 >> 16) * 0.003921569f;
							color4.a = (float)(num47 >> 8 & 255) * 0.003921569f;
							Color color5;
							color5.r = (float)surfaceCell2.m_clipped * 0.003921569f;
							color5.g = (float)surfaceCell2.m_pavementA * 0.003921569f;
							color5.b = vector2.x * 0.498039216f + 0.498039216f;
							color5.a = vector2.z * 0.498039216f + 0.498039216f;
							Color color6;
							color6.r = (float)surfaceCell2.m_ruined * 0.003921569f;
							color6.g = (float)surfaceCell2.m_pavementB * 0.003921569f;
							color6.b = (float)surfaceCell2.m_gravel * 0.003921569f;
							color6.a = (float)surfaceCell2.m_field * 0.003921569f;
							this.m_heightMap.SetPixel(l + num26, k + num27, color4);
							this.m_surfaceMapA.SetPixel(l + num26, k + num27, color5);
							this.m_surfaceMapB.SetPixel(l + num26, k + num27, color6);
						}
					}
				}
			}
			this.m_heightMap.Apply(false);
			this.m_surfaceMapA.Apply(false);
			this.m_surfaceMapB.Apply(false);
		}
		num9 = 128 - num >> 1;
		if (instance.m_tmpZonesModified.m_minX - num9 <= num5 && instance.m_tmpZonesModified.m_maxX >= num4 - num9 && instance.m_tmpZonesModified.m_minZ - num9 <= num7 && instance.m_tmpZonesModified.m_maxZ >= num6 - num9 && this.m_rndDetailIndex != 0)
		{
			num9 = 16;
			int num48 = 256 - 480 * this.m_x - 240;
			int num49 = 256 - 480 * this.m_z - 240;
			int num50 = Mathf.Max((num4 << 2) - num9, instance.m_tmpZonesModified.m_minX << 2);
			int num51 = Mathf.Min((num5 << 2) + num9 - 1, instance.m_tmpZonesModified.m_maxX << 2);
			int num52 = Mathf.Max((num6 << 2) - num9, instance.m_tmpZonesModified.m_minZ << 2);
			int num53 = Mathf.Min((num7 << 2) + num9 - 1, instance.m_tmpZonesModified.m_maxZ << 2);
			for (int m = num52; m <= num53; m++)
			{
				int z3 = Mathf.Max(0, m);
				int num54;
				int num55;
				instance.m_tmpZonesModified.GetLimitsX(m >> 2, out num54, out num55);
				num54 = Mathf.Max(num54 << 2, num50);
				num55 = Mathf.Min(num55 << 2, num51);
				for (int n = num54; n <= num55; n++)
				{
					int num56;
					int num57;
					instance.m_tmpZonesModified.GetLimitsZ(n >> 2, out num56, out num57);
					if (m >= num56 << 2 && m <= num57 << 2)
					{
						int x3 = Mathf.Max(0, n);
						TerrainManager.ZoneCell zoneCell = instance.GetZoneCell(x3, z3);
						Color color7;
						color7.r = (float)zoneCell.m_offsetX * 0.003921569f + 0.5019608f;
						color7.g = (float)zoneCell.m_angle * 0.00390625f;
						color7.b = (float)zoneCell.m_offsetZ * 0.003921569f + 0.5019608f;
						color7.a = (float)zoneCell.m_zone * 0.0322580636f;
						this.m_zoneLayout.SetPixel(n + num48, m + num49, color7);
					}
				}
			}
			this.m_zoneLayout.Apply(false);
		}
		bool flag = waterSimulation.WaterExists(num4, num6, num5, num7);
		if ((flag || this.m_waterExists != 0 || !this.m_waterInitialized) && (updateWater || !this.m_waterInitialized))
		{
			if (!this.m_waterInitialized)
			{
				for (uint num58 = 0u; num58 < 2u; num58 += 1u)
				{
					this.m_waterHeight[(int)((UIntPtr)num58)].m_waterHeight.LoadRawTextureData(waterSimulation.m_heightMaps[this.m_z * 9 + this.m_x]);
					this.m_waterHeight[(int)((UIntPtr)num58)].m_waterHeight.Apply(false);
					Vector4 vector3 = waterSimulation.m_heightBounds[this.m_z * 9 + this.m_x];
					float num59 = Mathf.Min(vector3.x, vector3.z);
					float num60 = Mathf.Max(vector3.y, vector3.w);
					Vector3 terrainPosition = this.m_terrainPosition;
					terrainPosition.y = (num59 + num60) * 0.5f;
					this.m_waterHeight[(int)((UIntPtr)num58)].m_waterPosition = terrainPosition;
					this.m_waterHeight[(int)((UIntPtr)num58)].m_waterMinY = num59;
					this.m_waterHeight[(int)((UIntPtr)num58)].m_waterMaxY = num60;
				}
				for (uint num61 = 0u; num61 < 3u; num61 += 1u)
				{
					this.m_waterSurfaceA[(int)((UIntPtr)num61)].LoadRawTextureData(waterSimulation.m_surfaceMapsA[this.m_z * 9 + this.m_x]);
					this.m_waterSurfaceA[(int)((UIntPtr)num61)].Apply(false);
					this.m_waterSurfaceB[(int)((UIntPtr)num61)].LoadRawTextureData(waterSimulation.m_surfaceMapsB[this.m_z * 9 + this.m_x]);
					this.m_waterSurfaceB[(int)((UIntPtr)num61)].Apply(false);
				}
			}
			else
			{
				uint num62 = waterFrame >> 6 & 1u;
				uint num63 = (waterFrame >> 6) % 3u;
				this.m_waterHeight[(int)((UIntPtr)num62)].m_waterHeight.LoadRawTextureData(waterSimulation.m_heightMaps[this.m_z * 9 + this.m_x]);
				this.m_waterHeight[(int)((UIntPtr)num62)].m_waterHeight.Apply(false);
				Vector4 vector4 = waterSimulation.m_heightBounds[this.m_z * 9 + this.m_x];
				float num64 = Mathf.Min(vector4.x, vector4.z);
				float num65 = Mathf.Max(vector4.y, vector4.w);
				Vector3 terrainPosition2 = this.m_terrainPosition;
				terrainPosition2.y = (num64 + num65) * 0.5f;
				this.m_waterHeight[(int)((UIntPtr)num62)].m_waterPosition = terrainPosition2;
				this.m_waterHeight[(int)((UIntPtr)num62)].m_waterMinY = num64;
				this.m_waterHeight[(int)((UIntPtr)num62)].m_waterMaxY = num65;
				this.m_waterSurfaceA[(int)((UIntPtr)num63)].LoadRawTextureData(waterSimulation.m_surfaceMapsA[this.m_z * 9 + this.m_x]);
				this.m_waterSurfaceA[(int)((UIntPtr)num63)].Apply(false);
				this.m_waterSurfaceB[(int)((UIntPtr)num63)].LoadRawTextureData(waterSimulation.m_surfaceMapsB[this.m_z * 9 + this.m_x]);
				this.m_waterSurfaceB[(int)((UIntPtr)num63)].Apply(false);
			}
			this.m_waterInitialized = true;
			if (flag)
			{
				this.m_waterExists = 3;
			}
			else
			{
				this.m_waterExists = Mathf.Max(0, this.m_waterExists - 1);
			}
		}
		uint num66 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		uint num67 = num66 - 64u >> 6 & 1u;
		float num68 = Mathf.Min(this.m_terrainMinY, this.m_waterHeight[(int)((UIntPtr)num67)].m_waterMinY);
		float num69 = Mathf.Max(this.m_terrainMaxY, this.m_waterHeight[(int)((UIntPtr)num67)].m_waterMaxY);
		Vector3 vector5;
		vector5..ctor(this.m_terrainPosition.x, (num68 + num69) * 0.5f, this.m_terrainPosition.z);
		Vector3 vector6;
		vector6..ctor(num3, num69 - num68 + 100f, num3);
		this.m_bounds = new Bounds(vector5, vector6);
	}

	public void Render(RenderManager.CameraInfo cameraInfo)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		if (cameraInfo.Intersect(this.m_bounds))
		{
			int num = 120;
			float num2 = (float)num * 16f;
			int num3 = num * this.m_x;
			int num4 = num * (this.m_x + 1);
			int num5 = num * this.m_z;
			int num6 = num * (this.m_z + 1);
			Vector3 vector;
			vector.x = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x) - num2 * 0.5f);
			vector.y = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.y - 512f) - 512f);
			vector.z = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z) - num2 * 0.5f);
			float levelOfDetailFactor = RenderManager.LevelOfDetailFactor2;
			float num7 = num2 * num2 * levelOfDetailFactor;
			float num8 = num7 * 4f;
			float num9 = Vector3.SqrMagnitude(vector);
			TerrainMesh.Flags flags;
			if (num9 > num8)
			{
				flags = TerrainMesh.Flags.HalfRes;
			}
			else if (num9 > num7)
			{
				flags = TerrainMesh.Flags.FullRes;
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z + num2) - num2 * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x + num2) - num2 * 0.5f), vector.y, vector.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosX;
				}
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z - num2) - num2 * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x - num2) - num2 * 0.5f), vector.y, vector.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegX;
				}
			}
			else
			{
				flags = TerrainMesh.Flags.SubDivide;
			}
			this.SetTerrainMaterialProperties(instance.m_materialBlock1);
			this.SetWaterMaterialProperties(instance.m_materialBlock2);
			if (flags != TerrainMesh.Flags.SubDivide)
			{
				Vector3 vector2;
				vector2..ctor(num2, num2, num2);
				Mesh mesh;
				Quaternion quaternion;
				TerrainMesh.GetLodMesh(flags, out mesh, out quaternion);
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(this.m_terrainPosition, quaternion, vector2);
				TerrainManager expr_2E7_cp_0 = instance;
				expr_2E7_cp_0.m_drawCallData.m_lodCalls = expr_2E7_cp_0.m_drawCallData.m_lodCalls + 1;
				TerrainManager expr_2FA_cp_0 = instance;
				expr_2FA_cp_0.m_drawCallData.m_batchedCalls = expr_2FA_cp_0.m_drawCallData.m_batchedCalls + 255;
				mesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(1f, (this.m_terrainMaxY - this.m_terrainMinY) / num2, 1f)));
				Graphics.DrawMesh(mesh, matrix4x, instance.m_terrainMaterial, instance.m_terrainLayer, null, 0, instance.m_materialBlock1, true, true);
				if (this.m_waterExists == 3)
				{
					uint num10 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
					uint num11 = num10 - 64u >> 6 & 1u;
					Vector3 waterPosition = this.m_waterHeight[(int)((UIntPtr)num11)].m_waterPosition;
					float waterMinY = this.m_waterHeight[(int)((UIntPtr)num11)].m_waterMinY;
					float waterMaxY = this.m_waterHeight[(int)((UIntPtr)num11)].m_waterMaxY;
					Material material = (!instance.TransparentWater) ? instance.m_waterMaterial : instance.m_waterTransparentMaterial;
					matrix4x.SetTRS(waterPosition, quaternion, vector2);
					TerrainManager expr_3EF_cp_0 = instance;
					expr_3EF_cp_0.m_drawCallData.m_lodCalls = expr_3EF_cp_0.m_drawCallData.m_lodCalls + 1;
					TerrainManager expr_402_cp_0 = instance;
					expr_402_cp_0.m_drawCallData.m_batchedCalls = expr_402_cp_0.m_drawCallData.m_batchedCalls + 255;
					mesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(1f, (waterMaxY - waterMinY) / num2, 1f)));
					Graphics.DrawMesh(mesh, matrix4x, material, instance.m_waterLayer, null, 0, instance.m_materialBlock2, false, false);
				}
			}
			else
			{
				for (int i = 0; i < 4; i++)
				{
					Vector3 position;
					int z;
					int minZ;
					int maxZ;
					if (i == 0 || i == 1)
					{
						position.z = this.m_terrainPosition.z - num2 * 0.25f;
						z = this.m_z << 1;
						minZ = num5;
						maxZ = num5 + num6 >> 1;
					}
					else
					{
						position.z = this.m_terrainPosition.z + num2 * 0.25f;
						z = (this.m_z << 1) + 1;
						minZ = num5 + num6 >> 1;
						maxZ = num6;
					}
					int x;
					int minX;
					int maxX;
					if (i == 0 || i == 2)
					{
						position.x = this.m_terrainPosition.x - num2 * 0.25f;
						x = this.m_x << 1;
						minX = num3;
						maxX = num3 + num4 >> 1;
					}
					else
					{
						position.x = this.m_terrainPosition.x + num2 * 0.25f;
						x = (this.m_x << 1) + 1;
						minX = num3 + num4 >> 1;
						maxX = num4;
					}
					position.y = this.m_terrainPosition.y;
					this.RenderSubPatch(0, x, z, cameraInfo, position, num2 * 0.5f, minX, maxX, minZ, maxZ);
				}
			}
		}
	}

	public void RenderOverlay(RenderManager.CameraInfo cameraInfo, Material material, bool requireDetails)
	{
		if ((this.m_rndDetailIndex != 0 || !requireDetails) && cameraInfo.Intersect(this.m_bounds))
		{
			int num = 120;
			float num2 = (float)num * 16f;
			int num3 = num * this.m_x;
			int num4 = num * (this.m_x + 1);
			int num5 = num * this.m_z;
			int num6 = num * (this.m_z + 1);
			Vector3 vector;
			vector.x = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x) - num2 * 0.5f);
			vector.y = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.y - 512f) - 512f);
			vector.z = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z) - num2 * 0.5f);
			float levelOfDetailFactor = RenderManager.LevelOfDetailFactor2;
			float num7 = num2 * num2 * levelOfDetailFactor;
			float num8 = num7 * 4f;
			float num9 = Vector3.SqrMagnitude(vector);
			TerrainMesh.Flags flags;
			if (num9 > num8)
			{
				flags = TerrainMesh.Flags.HalfRes;
			}
			else if (num9 > num7)
			{
				flags = TerrainMesh.Flags.FullRes;
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z + num2) - num2 * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x + num2) - num2 * 0.5f), vector.y, vector.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosX;
				}
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - this.m_terrainPosition.z - num2) - num2 * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - this.m_terrainPosition.x - num2) - num2 * 0.5f), vector.y, vector.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegX;
				}
			}
			else
			{
				flags = TerrainMesh.Flags.SubDivide;
			}
			this.SetTerrainMaterialProperties(material);
			if (flags != TerrainMesh.Flags.SubDivide)
			{
				Vector3 vector2;
				vector2..ctor(num2, num2, num2);
				Mesh mesh;
				Quaternion quaternion;
				TerrainMesh.GetLodMesh(flags, out mesh, out quaternion);
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(this.m_terrainPosition, quaternion, vector2);
				if (material.SetPass(0))
				{
					TerrainManager expr_2F0_cp_0 = Singleton<TerrainManager>.get_instance();
					expr_2F0_cp_0.m_drawCallData.m_overlayCalls = expr_2F0_cp_0.m_drawCallData.m_overlayCalls + 1;
					Graphics.DrawMeshNow(mesh, matrix4x);
				}
			}
			else
			{
				for (int i = 0; i < 4; i++)
				{
					Vector3 position;
					int z;
					int minZ;
					int maxZ;
					if (i == 0 || i == 1)
					{
						position.z = this.m_terrainPosition.z - num2 * 0.25f;
						z = this.m_z << 1;
						minZ = num5;
						maxZ = num5 + num6 >> 1;
					}
					else
					{
						position.z = this.m_terrainPosition.z + num2 * 0.25f;
						z = (this.m_z << 1) + 1;
						minZ = num5 + num6 >> 1;
						maxZ = num6;
					}
					int x;
					int minX;
					int maxX;
					if (i == 0 || i == 2)
					{
						position.x = this.m_terrainPosition.x - num2 * 0.25f;
						x = this.m_x << 1;
						minX = num3;
						maxX = num3 + num4 >> 1;
					}
					else
					{
						position.x = this.m_terrainPosition.x + num2 * 0.25f;
						x = (this.m_x << 1) + 1;
						minX = num3 + num4 >> 1;
						maxX = num4;
					}
					position.y = this.m_terrainPosition.y;
					this.RenderSubPatchOverlay(0, x, z, cameraInfo, material, position, num2 * 0.5f, minX, maxX, minZ, maxZ);
				}
			}
		}
	}

	private void RenderSubPatch(int level, int x, int z, RenderManager.CameraInfo cameraInfo, Vector3 position, float patchSize, int minX, int maxX, int minZ, int maxZ)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		TerrainManager.CellBounds bounds = instance.GetBounds(x, z, level + 1);
		float num = (float)bounds.m_minHeight * 0.015625f;
		float num2 = (float)bounds.m_maxHeight * 0.015625f;
		position.y = (num + num2) * 0.5f;
		uint num3 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		uint num4 = num3 - 64u >> 6 & 1u;
		float waterMinY = this.m_waterHeight[(int)((UIntPtr)num4)].m_waterMinY;
		float waterMaxY = this.m_waterHeight[(int)((UIntPtr)num4)].m_waterMaxY;
		float num5 = Mathf.Min(num, waterMinY);
		float num6 = Mathf.Max(num2, waterMaxY);
		Vector3 vector;
		vector..ctor(position.x, (num5 + num6) * 0.5f, position.z);
		Vector3 vector2;
		vector2..ctor(patchSize, num6 - num5, patchSize);
		Bounds bounds2;
		bounds2..ctor(vector, vector2);
		if (cameraInfo.Intersect(bounds2))
		{
			Vector3 vector3;
			vector3.x = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x) - patchSize * 0.5f);
			vector3.y = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.y - 512f) - 512f);
			vector3.z = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z) - patchSize * 0.5f);
			float levelOfDetailFactor = RenderManager.LevelOfDetailFactor2;
			float num7 = patchSize * patchSize * levelOfDetailFactor;
			float num8 = num7 * 4f;
			float num9 = Vector3.SqrMagnitude(vector3);
			TerrainMesh.Flags flags;
			if (num9 > num8)
			{
				flags = TerrainMesh.Flags.HalfRes;
			}
			else if (num9 > num7 || level == 3)
			{
				flags = TerrainMesh.Flags.FullRes;
				if (Vector3.SqrMagnitude(new Vector3(vector3.x, vector3.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z + patchSize) - patchSize * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x + patchSize) - patchSize * 0.5f), vector3.y, vector3.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectPosX;
				}
				if (Vector3.SqrMagnitude(new Vector3(vector3.x, vector3.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z - patchSize) - patchSize * 0.5f))) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x - patchSize) - patchSize * 0.5f), vector3.y, vector3.z)) > num8)
				{
					flags |= TerrainMesh.Flags.ConnectNegX;
				}
			}
			else
			{
				flags = TerrainMesh.Flags.SubDivide;
			}
			bool flag = waterSimulation.WaterExists(minX, minZ, maxX, maxZ);
			if (flags != TerrainMesh.Flags.SubDivide)
			{
				Vector3 vector4;
				vector4..ctor(patchSize, patchSize, patchSize);
				Mesh mesh;
				Quaternion quaternion;
				TerrainMesh.GetLodMesh(flags, out mesh, out quaternion);
				Matrix4x4 matrix4x = default(Matrix4x4);
				int num10 = 4 - level - 1;
				matrix4x.SetTRS(position, quaternion, vector4);
				if (num10 == 0)
				{
					TerrainManager expr_38B_cp_0 = instance;
					expr_38B_cp_0.m_drawCallData.m_defaultCalls = expr_38B_cp_0.m_drawCallData.m_defaultCalls + 1;
				}
				else
				{
					TerrainManager expr_3A3_cp_0 = instance;
					expr_3A3_cp_0.m_drawCallData.m_lodCalls = expr_3A3_cp_0.m_drawCallData.m_lodCalls + 1;
					TerrainManager expr_3B6_cp_0 = instance;
					expr_3B6_cp_0.m_drawCallData.m_batchedCalls = expr_3B6_cp_0.m_drawCallData.m_batchedCalls + ((1 << (num10 << 1)) - 1);
				}
				Bounds bounds3;
				bounds3..ctor(Vector3.get_zero(), new Vector3(1f, (num2 - num) / patchSize, 1f));
				mesh.set_bounds(bounds3);
				Graphics.DrawMesh(mesh, matrix4x, instance.m_terrainMaterial, instance.m_terrainLayer, null, 0, instance.m_materialBlock1, true, true);
				if (num10 == 0 && num9 <= num8 && RenderManager.LevelOfDetail != 0)
				{
					TerrainProperties properties = instance.m_properties;
					if (properties != null && (properties.m_useGrassDecorations || properties.m_useFertileDecorations || properties.m_useCliffDecorations))
					{
						Mesh mesh2 = DecorationMesh.m_mesh;
						matrix4x.SetTRS(position, Quaternion.get_identity(), vector4);
						TerrainManager expr_487_cp_0 = instance;
						expr_487_cp_0.m_drawCallData.m_defaultCalls = expr_487_cp_0.m_drawCallData.m_defaultCalls + 1;
						mesh2.set_bounds(bounds3);
						Graphics.DrawMesh(mesh2, matrix4x, instance.m_decorationMaterial, instance.m_decorationLayer, null, 0, instance.m_materialBlock1, false, true);
					}
				}
				if (flag)
				{
					Vector3 vector5 = position;
					vector5.y = (waterMinY + waterMaxY) * 0.5f;
					Material material = (!instance.TransparentWater) ? instance.m_waterMaterial : instance.m_waterTransparentMaterial;
					matrix4x.SetTRS(vector5, quaternion, vector4);
					if (num10 == 0)
					{
						TerrainManager expr_511_cp_0 = instance;
						expr_511_cp_0.m_drawCallData.m_defaultCalls = expr_511_cp_0.m_drawCallData.m_defaultCalls + 1;
					}
					else
					{
						TerrainManager expr_529_cp_0 = instance;
						expr_529_cp_0.m_drawCallData.m_lodCalls = expr_529_cp_0.m_drawCallData.m_lodCalls + 1;
						TerrainManager expr_53C_cp_0 = instance;
						expr_53C_cp_0.m_drawCallData.m_batchedCalls = expr_53C_cp_0.m_drawCallData.m_batchedCalls + ((1 << (num10 << 1)) - 1);
					}
					mesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(1f, (waterMaxY - waterMinY) / patchSize, 1f)));
					Graphics.DrawMesh(mesh, matrix4x, material, instance.m_waterLayer, null, 0, instance.m_materialBlock2, false, false);
				}
			}
			else
			{
				for (int i = 0; i < 4; i++)
				{
					Vector3 position2;
					int z2;
					int minZ2;
					int maxZ2;
					if (i == 0 || i == 1)
					{
						position2.z = position.z - patchSize * 0.25f;
						z2 = z << 1;
						minZ2 = minZ;
						maxZ2 = minZ + maxZ >> 1;
					}
					else
					{
						position2.z = position.z + patchSize * 0.25f;
						z2 = (z << 1) + 1;
						minZ2 = minZ + maxZ >> 1;
						maxZ2 = maxZ;
					}
					int x2;
					int minX2;
					int maxX2;
					if (i == 0 || i == 2)
					{
						position2.x = position.x - patchSize * 0.25f;
						x2 = x << 1;
						minX2 = minX;
						maxX2 = minX + maxX >> 1;
					}
					else
					{
						position2.x = position.x + patchSize * 0.25f;
						x2 = (x << 1) + 1;
						minX2 = minX + maxX >> 1;
						maxX2 = maxX;
					}
					position2.y = position.y;
					this.RenderSubPatch(level + 1, x2, z2, cameraInfo, position2, patchSize * 0.5f, minX2, maxX2, minZ2, maxZ2);
				}
			}
		}
	}

	private void RenderSubPatchOverlay(int level, int x, int z, RenderManager.CameraInfo cameraInfo, Material material, Vector3 position, float patchSize, int minX, int maxX, int minZ, int maxZ)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainManager.CellBounds bounds = instance.GetBounds(x, z, level + 1);
		float num = (float)bounds.m_minHeight * 0.015625f;
		float num2 = (float)bounds.m_maxHeight * 0.015625f;
		position.y = (num + num2) * 0.5f;
		Bounds bounds2;
		bounds2..ctor(position, new Vector3(patchSize, num2 - num, patchSize));
		if (cameraInfo.Intersect(bounds2))
		{
			Vector3 vector;
			vector.x = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x) - patchSize * 0.5f);
			vector.y = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.y - 512f) - 512f);
			vector.z = Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z) - patchSize * 0.5f);
			float levelOfDetailFactor = RenderManager.LevelOfDetailFactor2;
			float num3 = patchSize * patchSize * levelOfDetailFactor;
			float num4 = num3 * 4f;
			float num5 = Vector3.SqrMagnitude(vector);
			TerrainMesh.Flags flags;
			if (num5 > num4)
			{
				flags = TerrainMesh.Flags.HalfRes;
			}
			else if (num5 > num3 || level == 3)
			{
				flags = TerrainMesh.Flags.FullRes;
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z + patchSize) - patchSize * 0.5f))) > num4)
				{
					flags |= TerrainMesh.Flags.ConnectPosZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x + patchSize) - patchSize * 0.5f), vector.y, vector.z)) > num4)
				{
					flags |= TerrainMesh.Flags.ConnectPosX;
				}
				if (Vector3.SqrMagnitude(new Vector3(vector.x, vector.y, Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.z - position.z - patchSize) - patchSize * 0.5f))) > num4)
				{
					flags |= TerrainMesh.Flags.ConnectNegZ;
				}
				if (Vector3.SqrMagnitude(new Vector3(Mathf.Max(0f, Mathf.Abs(cameraInfo.m_position.x - position.x - patchSize) - patchSize * 0.5f), vector.y, vector.z)) > num4)
				{
					flags |= TerrainMesh.Flags.ConnectNegX;
				}
			}
			else
			{
				flags = TerrainMesh.Flags.SubDivide;
			}
			if (flags != TerrainMesh.Flags.SubDivide)
			{
				Vector3 vector2;
				vector2..ctor(patchSize, patchSize, patchSize);
				Mesh mesh;
				Quaternion quaternion;
				TerrainMesh.GetLodMesh(flags, out mesh, out quaternion);
				Matrix4x4 matrix4x = default(Matrix4x4);
				matrix4x.SetTRS(position, quaternion, vector2);
				if (material.SetPass(0))
				{
					TerrainManager expr_2F6_cp_0 = Singleton<TerrainManager>.get_instance();
					expr_2F6_cp_0.m_drawCallData.m_overlayCalls = expr_2F6_cp_0.m_drawCallData.m_overlayCalls + 1;
					Graphics.DrawMeshNow(mesh, matrix4x);
				}
			}
			else
			{
				for (int i = 0; i < 4; i++)
				{
					Vector3 position2;
					int z2;
					int minZ2;
					int maxZ2;
					if (i == 0 || i == 1)
					{
						position2.z = position.z - patchSize * 0.25f;
						z2 = z << 1;
						minZ2 = minZ;
						maxZ2 = minZ + maxZ >> 1;
					}
					else
					{
						position2.z = position.z + patchSize * 0.25f;
						z2 = (z << 1) + 1;
						minZ2 = minZ + maxZ >> 1;
						maxZ2 = maxZ;
					}
					int x2;
					int minX2;
					int maxX2;
					if (i == 0 || i == 2)
					{
						position2.x = position.x - patchSize * 0.25f;
						x2 = x << 1;
						minX2 = minX;
						maxX2 = minX + maxX >> 1;
					}
					else
					{
						position2.x = position.x + patchSize * 0.25f;
						x2 = (x << 1) + 1;
						minX2 = minX + maxX >> 1;
						maxX2 = maxX;
					}
					position2.y = position.y;
					this.RenderSubPatchOverlay(level + 1, x2, z2, cameraInfo, material, position2, patchSize * 0.5f, minX2, maxX2, minZ2, maxZ2);
				}
			}
		}
	}

	public void SetTerrainMaterialProperties(MaterialPropertyBlock materialBlock)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		materialBlock.Clear();
		materialBlock.SetTexture(instance.ID_MainTex, this.m_heightMap);
		materialBlock.SetTexture(instance.ID_SurfaceTexA, this.m_surfaceMapA);
		materialBlock.SetTexture(instance.ID_SurfaceTexB, this.m_surfaceMapB);
		if (this.m_zoneLayout != null)
		{
			materialBlock.SetTexture(instance.ID_ZoneLayout, this.m_zoneLayout);
		}
		materialBlock.SetVector(instance.ID_HeightMapping, (this.m_rndDetailIndex == 0) ? this.m_heightMappingRaw : this.m_heightMappingDetail);
		materialBlock.SetVector(instance.ID_SurfaceMapping, (this.m_rndDetailIndex == 0) ? this.m_surfaceMappingRaw : this.m_surfaceMappingDetail);
	}

	public void SetTerrainMaterialProperties(Material material)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		material.SetTexture(instance.ID_MainTex, this.m_heightMap);
		material.SetTexture(instance.ID_SurfaceTexA, this.m_surfaceMapA);
		material.SetTexture(instance.ID_SurfaceTexB, this.m_surfaceMapB);
		material.SetTexture(instance.ID_ZoneLayout, this.m_zoneLayout);
		material.SetVector(instance.ID_HeightMapping, (this.m_rndDetailIndex == 0) ? this.m_heightMappingRaw : this.m_heightMappingDetail);
		material.SetVector(instance.ID_SurfaceMapping, (this.m_rndDetailIndex == 0) ? this.m_surfaceMappingRaw : this.m_surfaceMappingDetail);
	}

	public void SetWaterMaterialProperties(MaterialPropertyBlock materialBlock)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		uint num = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		uint num2 = num - 64u >> 6 & 1u;
		uint num3 = (num - 128u >> 6) % 3u;
		uint num4 = (num - 64u >> 6) % 3u;
		materialBlock.Clear();
		materialBlock.SetTexture(instance.ID_MainTex, this.m_waterHeight[(int)((UIntPtr)num2)].m_waterHeight);
		materialBlock.SetTexture(instance.ID_SurfaceAOld, this.m_waterSurfaceA[(int)((UIntPtr)num3)]);
		materialBlock.SetTexture(instance.ID_SurfaceANew, this.m_waterSurfaceA[(int)((UIntPtr)num4)]);
		materialBlock.SetTexture(instance.ID_SurfaceBOld, this.m_waterSurfaceB[(int)((UIntPtr)num3)]);
		materialBlock.SetTexture(instance.ID_SurfaceBNew, this.m_waterSurfaceB[(int)((UIntPtr)num4)]);
		materialBlock.SetTexture(instance.ID_TerrainHeight, this.m_heightMap);
		materialBlock.SetVector(instance.ID_HeightMapping, this.m_heightMappingRaw);
		materialBlock.SetVector(instance.ID_TerrainMapping, (this.m_rndDetailIndex == 0) ? this.m_heightMappingRaw : this.m_heightMappingDetail);
		materialBlock.SetVector(instance.ID_SurfaceMapping, this.m_surfaceMappingRaw);
	}

	public void SetWaterMaterialProperties(Material material)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		uint num = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		uint num2 = num - 64u >> 6 & 1u;
		uint num3 = (num - 128u >> 6) % 3u;
		uint num4 = (num - 64u >> 6) % 3u;
		material.SetTexture(instance.ID_MainTex, this.m_waterHeight[(int)((UIntPtr)num2)].m_waterHeight);
		material.SetTexture(instance.ID_SurfaceAOld, this.m_waterSurfaceA[(int)((UIntPtr)num3)]);
		material.SetTexture(instance.ID_SurfaceANew, this.m_waterSurfaceA[(int)((UIntPtr)num4)]);
		material.SetTexture(instance.ID_SurfaceBOld, this.m_waterSurfaceB[(int)((UIntPtr)num3)]);
		material.SetTexture(instance.ID_SurfaceBNew, this.m_waterSurfaceB[(int)((UIntPtr)num4)]);
		material.SetTexture(instance.ID_TerrainHeight, this.m_heightMap);
		material.SetVector(instance.ID_HeightMapping, this.m_heightMappingRaw);
		material.SetVector(instance.ID_TerrainMapping, (this.m_rndDetailIndex == 0) ? this.m_heightMappingRaw : this.m_heightMappingDetail);
		material.SetVector(instance.ID_SurfaceMapping, this.m_surfaceMappingRaw);
	}

	public Texture2D m_heightMap;

	public Texture2D m_surfaceMapA;

	public Texture2D m_surfaceMapB;

	public Texture2D m_zoneLayout;

	public TerrainPatch.WaterHeightFrame[] m_waterHeight;

	public Texture2D[] m_waterSurfaceA;

	public Texture2D[] m_waterSurfaceB;

	public Vector4 m_heightMappingRaw;

	public Vector4 m_surfaceMappingRaw;

	public Vector4 m_heightMappingDetail;

	public Vector4 m_surfaceMappingDetail;

	public TerrainArea m_surfaceModified;

	public TerrainArea m_zonesModified;

	public Vector3 m_terrainPosition;

	public Bounds m_bounds;

	public object m_modifiedLock;

	public int m_x;

	public int m_z;

	public int m_simDetailIndex;

	public int m_tmpDetailIndex;

	public int m_rndDetailIndex;

	public int m_waterExists;

	public float m_flatness;

	public float m_terrainMinY;

	public float m_terrainMaxY;

	public bool m_waterInitialized;

	public struct WaterHeightFrame
	{
		public Texture2D m_waterHeight;

		public Vector3 m_waterPosition;

		public float m_waterMinY;

		public float m_waterMaxY;
	}
}
