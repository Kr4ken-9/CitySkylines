﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.Threading;
using UnityEngine;

public class DisasterManager : SimulationManagerBase<DisasterManager, DisasterProperties>, ISimulationManager, IRenderableManager, IAudibleManager
{
	public event DisasterManager.DetectedDisastersUpdatedHandler m_detectedDisastersUpdated
	{
		add
		{
			DisasterManager.DetectedDisastersUpdatedHandler detectedDisastersUpdatedHandler = this.m_detectedDisastersUpdated;
			DisasterManager.DetectedDisastersUpdatedHandler detectedDisastersUpdatedHandler2;
			do
			{
				detectedDisastersUpdatedHandler2 = detectedDisastersUpdatedHandler;
				detectedDisastersUpdatedHandler = Interlocked.CompareExchange<DisasterManager.DetectedDisastersUpdatedHandler>(ref this.m_detectedDisastersUpdated, (DisasterManager.DetectedDisastersUpdatedHandler)Delegate.Combine(detectedDisastersUpdatedHandler2, value), detectedDisastersUpdatedHandler);
			}
			while (detectedDisastersUpdatedHandler != detectedDisastersUpdatedHandler2);
		}
		remove
		{
			DisasterManager.DetectedDisastersUpdatedHandler detectedDisastersUpdatedHandler = this.m_detectedDisastersUpdated;
			DisasterManager.DetectedDisastersUpdatedHandler detectedDisastersUpdatedHandler2;
			do
			{
				detectedDisastersUpdatedHandler2 = detectedDisastersUpdatedHandler;
				detectedDisastersUpdatedHandler = Interlocked.CompareExchange<DisasterManager.DetectedDisastersUpdatedHandler>(ref this.m_detectedDisastersUpdated, (DisasterManager.DetectedDisastersUpdatedHandler)Delegate.Remove(detectedDisastersUpdatedHandler2, value), detectedDisastersUpdatedHandler);
			}
			while (detectedDisastersUpdatedHandler != detectedDisastersUpdatedHandler2);
		}
	}

	public ushort HighlightDisaster
	{
		get
		{
			return this.m_highlightDisaster;
		}
		set
		{
			this.m_highlightDisaster = value;
		}
	}

	public int HazardMapVisible
	{
		get
		{
			return this.m_hazardMapVisible;
		}
		set
		{
			if (this.m_hazardMapVisible != value)
			{
				this.m_hazardMapVisible = value;
				this.UpdateHazardMapping();
			}
		}
	}

	public bool MarkersVisible
	{
		get
		{
			return this.m_markersVisible;
		}
		set
		{
			this.m_markersVisible = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_Color = Shader.PropertyToID("_Color");
		this.m_markerLayer = LayerMask.NameToLayer("ScenarioMarkers");
		SavedBool savedBool = new SavedBool(Settings.randomDisastersEnabled, Settings.gameSettingsFile, DefaultSettings.randomDisastersEnabled);
		SavedFloat savedFloat = new SavedFloat(Settings.randomDisastersProbability, Settings.gameSettingsFile, DefaultSettings.randomDisastersProbability);
		this.m_randomDisastersProbability = ((!savedBool.get_value()) ? 0f : savedFloat.get_value());
		this.m_hazardAmount = new byte[65536];
		this.m_hazardMapVisible = 0;
		this.m_hazardTexture = new Texture2D(256, 256, 1, false, true);
		this.m_hazardTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_DisasterHazardTexture", this.m_hazardTexture);
		this.UpdateHazardMapping();
		this.m_markersVisible = true;
		this.m_disasters = new FastList<DisasterData>();
		this.m_disasters.Add(default(DisasterData));
		this.m_evacuationMap = new uint[4096];
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			if (this.m_DisasterWrapper == null)
			{
				this.m_DisasterWrapper = new DisasterWrapper(this);
			}
		}
		else
		{
			this.ReleaseRelay();
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_DisasterWrapper != null)
		{
			this.m_DisasterWrapper.Release();
			this.m_DisasterWrapper = null;
		}
	}

	private void UpdateHazardMapping()
	{
		if (this.m_hazardMapVisible != 0)
		{
			Vector4 vector;
			vector.z = 0.000101725258f;
			vector.x = 0.5f;
			vector.y = 0.5f;
			vector.w = 0.5f;
			Shader.SetGlobalVector("_DisasterHazardMapping", vector);
			this.m_hazardModified = 0;
			this.UpdateTexture();
		}
	}

	private void LateUpdate()
	{
		if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			return;
		}
		if (this.m_hazardMapVisible != 0 && (this.m_hazardModified & this.m_hazardMapVisible) != 0)
		{
			this.m_hazardModified = 0;
			this.UpdateTexture();
			Singleton<BuildingManager>.get_instance().UpdateBuildingColors();
		}
	}

	public Color SampleDisasterHazardMap(Vector3 pos)
	{
		pos.x /= 38.4f;
		pos.z /= 38.4f;
		int num = Mathf.Max((int)(pos.x + 128f), 0);
		int num2 = Mathf.Max((int)(pos.z + 128f), 0);
		int num3 = Mathf.Min((int)(pos.x + 1f + 128f), 255);
		int num4 = Mathf.Min((int)(pos.z + 1f + 128f), 255);
		float num5 = pos.x - ((float)num - 128f + 0.5f);
		float num6 = pos.z - ((float)num2 - 128f + 0.5f);
		float num7 = (float)this.m_hazardAmount[num2 * 256 + num];
		float num8 = (float)this.m_hazardAmount[num2 * 256 + num3];
		float num9 = (float)this.m_hazardAmount[num4 * 256 + num];
		float num10 = (float)this.m_hazardAmount[num4 * 256 + num3];
		float num11 = num7 + (num8 - num7) * num5;
		float num12 = num9 + (num10 - num9) * num5;
		float num13 = (num11 + (num12 - num11) * num6) * 0.003921569f;
		InfoProperties properties = Singleton<InfoManager>.get_instance().m_properties;
		if (num13 < 0.5f)
		{
			Color neutralColor = properties.m_neutralColor;
			Color activeColorB = properties.m_modeProperties[29].m_activeColorB;
			return ColorUtils.LinearLerp(neutralColor, activeColorB, num13 * 2f);
		}
		Color activeColorB2 = properties.m_modeProperties[29].m_activeColorB;
		Color targetColor = properties.m_modeProperties[29].m_targetColor;
		return ColorUtils.LinearLerp(activeColorB2, targetColor, num13 * 2f - 1f);
	}

	private void UpdateTexture()
	{
		for (int i = 0; i < 256; i++)
		{
			for (int j = 0; j < 256; j++)
			{
				this.m_hazardAmount[i * 256 + j] = 0;
			}
		}
		for (int k = 0; k < this.m_disasters.m_size; k++)
		{
			if ((this.m_disasters.m_buffer[k].m_flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted)) == DisasterData.Flags.Created)
			{
				DisasterInfo info = this.m_disasters.m_buffer[k].Info;
				InfoManager.SubInfoMode subInfoMode;
				if (info != null && info.m_disasterAI.GetHazardSubMode(out subInfoMode) && (this.m_hazardMapVisible & 1 << (int)subInfoMode) != 0)
				{
					info.m_disasterAI.UpdateHazardMap((ushort)k, ref this.m_disasters.m_buffer[k], this.m_hazardAmount);
				}
			}
		}
		for (int l = 0; l < 256; l++)
		{
			for (int m = 0; m < 256; m++)
			{
				int num = 0;
				if (l > 0)
				{
					if (m > 0)
					{
						num += (int)(this.m_hazardAmount[(l - 1) * 256 + (m - 1)] * 5);
					}
					num += (int)(this.m_hazardAmount[(l - 1) * 256 + m] * 7);
					if (m < 255)
					{
						num += (int)(this.m_hazardAmount[(l - 1) * 256 + (m + 1)] * 5);
					}
				}
				if (m > 0)
				{
					num += (int)(this.m_hazardAmount[l * 256 + (m - 1)] * 7);
				}
				num += (int)(this.m_hazardAmount[l * 256 + m] * 14);
				if (m < 255)
				{
					num += (int)(this.m_hazardAmount[l * 256 + (m + 1)] * 7);
				}
				if (l < 255)
				{
					if (m > 0)
					{
						num += (int)(this.m_hazardAmount[(l + 1) * 256 + (m - 1)] * 5);
					}
					num += (int)(this.m_hazardAmount[(l + 1) * 256 + m] * 7);
					if (m < 255)
					{
						num += (int)(this.m_hazardAmount[(l + 1) * 256 + (m + 1)] * 5);
					}
				}
				float num2 = Mathf.Clamp01((float)num * 6.325111E-05f);
				Color color;
				color.r = num2;
				color.g = num2;
				color.b = num2;
				color.a = num2;
				this.m_hazardTexture.SetPixel(m, l, color);
			}
		}
		this.m_hazardTexture.Apply();
	}

	public override void InitializeProperties(DisasterProperties properties)
	{
		base.InitializeProperties(properties);
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
		if (properties.m_mediumExplosion != null)
		{
			properties.m_mediumExplosion.InitializeEffect();
		}
		int num = PrefabCollection<DisasterInfo>.LoadedCount();
		for (int i = 0; i < num; i++)
		{
			DisasterInfo loaded = PrefabCollection<DisasterInfo>.GetLoaded((uint)i);
			loaded.m_finalRandomProbability = loaded.m_randomProbability;
		}
		if (properties.m_disasterSettings != null)
		{
			for (int j = 0; j < properties.m_disasterSettings.Length; j++)
			{
				DisasterInfo disasterInfo = PrefabCollection<DisasterInfo>.FindLoaded(properties.m_disasterSettings[j].m_disasterName);
				if (disasterInfo != null)
				{
					disasterInfo.m_finalRandomProbability = properties.m_disasterSettings[j].m_randomProbability;
				}
			}
		}
	}

	public override void DestroyProperties(DisasterProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_cameraController = null;
			if (properties.m_mediumExplosion != null)
			{
				properties.m_mediumExplosion.ReleaseEffect();
			}
		}
		base.DestroyProperties(properties);
	}

	private void Update()
	{
		if (this.m_markersVisible)
		{
			this.m_markerAlpha = Mathf.Min(1f, this.m_markerAlpha + Time.get_deltaTime() * 2f);
		}
		else
		{
			this.m_markerAlpha = Mathf.Max(0f, this.m_markerAlpha - Time.get_deltaTime() * 2f);
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if ((cameraInfo.m_layerMask & 1 << this.m_markerLayer) != 0)
		{
			for (int i = 0; i < this.m_disasters.m_size; i++)
			{
				if ((this.m_disasters.m_buffer[i].m_flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted | DisasterData.Flags.Hidden)) == DisasterData.Flags.Created)
				{
					DisasterInfo info = this.m_disasters.m_buffer[i].Info;
					if (info != null)
					{
						this.DrawMarker(info, this.m_disasters.m_buffer[i].m_targetPosition, this.m_disasters.m_buffer[i].m_angle, this.m_highlightDisaster == (ushort)i);
					}
				}
			}
		}
		if (this.m_cameraController != null)
		{
			Vector3 zero = Vector3.get_zero();
			for (int j = 0; j < this.m_disasters.m_size; j++)
			{
				if ((this.m_disasters.m_buffer[j].m_flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted)) == DisasterData.Flags.Created)
				{
					DisasterInfo info2 = this.m_disasters.m_buffer[j].Info;
					if (info2 != null)
					{
						info2.m_disasterAI.RenderInstance(cameraInfo, (ushort)j, ref this.m_disasters.m_buffer[j], ref zero);
					}
				}
			}
			if (!this.m_disableCameraShake)
			{
				this.m_cameraController.m_cameraShake += zero;
			}
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		if (this.m_properties != null && !Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			for (int i = 0; i < this.m_disasters.m_size; i++)
			{
				if ((this.m_disasters.m_buffer[i].m_flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted)) == DisasterData.Flags.Created)
				{
					DisasterInfo info = this.m_disasters.m_buffer[i].Info;
					if (info != null)
					{
						info.m_disasterAI.PlayInstance(listenerInfo, (ushort)i, ref this.m_disasters.m_buffer[i]);
					}
				}
			}
		}
	}

	private void DrawMarker(DisasterInfo info, Vector3 pos, float angle, bool highlight)
	{
		if (this.m_properties == null)
		{
			return;
		}
		Color color = info.m_markerColor;
		if (highlight)
		{
			color.r += (1f - color.r) * 0.5f;
			color.g += (1f - color.g) * 0.5f;
			color.b += (1f - color.b) * 0.5f;
		}
		color *= this.m_markerAlpha;
		this.m_materialBlock.Clear();
		this.m_materialBlock.SetColor(this.ID_Color, color);
		Quaternion quaternion = Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
		Mesh markerMesh = info.m_markerMesh;
		this.m_drawCallData.m_defaultCalls = this.m_drawCallData.m_defaultCalls + 1;
		Graphics.DrawMesh(markerMesh, pos, quaternion, this.m_properties.m_markerMaterial, 0, null, 0, this.m_materialBlock, false, false);
	}

	public bool IsFollowing(ushort disasterIndex)
	{
		DisasterData.Flags flags = this.m_disasters.m_buffer[(int)disasterIndex].m_flags;
		return (flags & (DisasterData.Flags.Created | DisasterData.Flags.Deleted | DisasterData.Flags.Follow)) == (DisasterData.Flags.Created | DisasterData.Flags.Follow) && this.m_followTime > Time.get_realtimeSinceStartup() - 2f;
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.ScenarioEditor) != ItemClass.Availability.None)
		{
			if (this.m_disasterCount >= 200)
			{
				return false;
			}
		}
		else if (this.m_disasterCount >= 256)
		{
			return false;
		}
		return true;
	}

	public static DisasterInfo FindDisasterInfo<T>() where T : DisasterAI
	{
		int num = PrefabCollection<DisasterInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)i);
			if (prefab != null && prefab.m_disasterAI is T)
			{
				return prefab;
			}
		}
		return null;
	}

	public bool CreateDisaster(out ushort disasterIndex, DisasterInfo info)
	{
		DisasterData disasterData = default(DisasterData);
		disasterData.m_flags = DisasterData.Flags.Created;
		disasterData.m_randomSeed = Singleton<SimulationManager>.get_instance().m_randomizer.ULong64();
		disasterData.m_intensity = 55;
		disasterData.Info = info;
		for (int i = 1; i < this.m_disasters.m_size; i++)
		{
			if (this.m_disasters.m_buffer[i].m_flags == DisasterData.Flags.None)
			{
				disasterIndex = (ushort)i;
				this.m_disasters.m_buffer[i] = disasterData;
				this.m_disasterCount++;
				info.m_disasterAI.CreateDisaster(disasterIndex, ref this.m_disasters.m_buffer[(int)disasterIndex]);
				return true;
			}
		}
		if (this.m_disasters.m_size < 256)
		{
			disasterIndex = (ushort)this.m_disasters.m_size;
			this.m_disasters.Add(disasterData);
			this.m_disasterCount++;
			info.m_disasterAI.CreateDisaster(disasterIndex, ref this.m_disasters.m_buffer[(int)disasterIndex]);
			return true;
		}
		disasterIndex = 0;
		return false;
	}

	public void ReleaseDisaster(ushort disasterIndex)
	{
		if (disasterIndex != 0 && this.m_disasters.m_buffer[(int)disasterIndex].m_flags != DisasterData.Flags.None)
		{
			bool flag = (this.m_disasters.m_buffer[(int)disasterIndex].m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None;
			DisasterInfo info = this.m_disasters.m_buffer[(int)disasterIndex].Info;
			InstanceID id = default(InstanceID);
			id.Disaster = disasterIndex;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			DisasterData[] expr_87_cp_0 = this.m_disasters.m_buffer;
			expr_87_cp_0[(int)disasterIndex].m_flags = (expr_87_cp_0[(int)disasterIndex].m_flags | DisasterData.Flags.Deleted);
			this.HazardModified(disasterIndex);
			if (info != null)
			{
				info.m_disasterAI.ReleaseDisaster(disasterIndex, ref this.m_disasters.m_buffer[(int)disasterIndex]);
			}
			ushort waveIndex = this.m_disasters.m_buffer[(int)disasterIndex].m_waveIndex;
			if (waveIndex != 0)
			{
				Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterWave(waveIndex);
			}
			this.m_disasters.m_buffer[(int)disasterIndex] = default(DisasterData);
			this.m_disasterCount--;
			while (this.m_disasters.m_size > 1)
			{
				if (this.m_disasters.m_buffer[this.m_disasters.m_size - 1].m_flags != DisasterData.Flags.None)
				{
					break;
				}
				this.m_disasters.m_size--;
			}
			if (flag)
			{
				if (--this.m_detectedDisasterCount == 0)
				{
					Singleton<BuildingManager>.get_instance().m_evacuationNotUsed.Deactivate();
				}
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (this.m_detectedDisastersUpdated != null)
					{
						this.m_detectedDisastersUpdated();
					}
				});
			}
		}
	}

	public void DetectDisaster(ushort disasterIndex, bool located)
	{
		if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & (DisasterData.Flags.Detected | DisasterData.Flags.UnDetected)) == DisasterData.Flags.None)
		{
			DisasterData[] expr_32_cp_0 = this.m_disasters.m_buffer;
			expr_32_cp_0[(int)disasterIndex].m_flags = (expr_32_cp_0[(int)disasterIndex].m_flags | (DisasterData.Flags.Detected | DisasterData.Flags.Warning));
			if (located)
			{
				DisasterData[] expr_5A_cp_0 = this.m_disasters.m_buffer;
				expr_5A_cp_0[(int)disasterIndex].m_flags = (expr_5A_cp_0[(int)disasterIndex].m_flags | DisasterData.Flags.Located);
			}
			this.m_DisasterWrapper.OnDisasterDetected(disasterIndex);
			this.HazardModified(disasterIndex);
			this.m_detectedDisasterCount++;
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_detectedDisastersUpdated != null)
				{
					this.m_detectedDisastersUpdated();
				}
			});
			DisasterInfo info = this.m_disasters.m_buffer[(int)disasterIndex].Info;
			this.m_disasters.m_buffer[(int)disasterIndex].m_broadcastCooldown = 36;
			if (info.m_warningBroadcast != null)
			{
				Singleton<AudioManager>.get_instance().QueueBroadcast(info.m_warningBroadcast);
			}
			if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & (DisasterData.Flags.Active | DisasterData.Flags.Clearing)) != DisasterData.Flags.None && info.m_disasterAI.m_experienceMilestone != null)
			{
				info.m_disasterAI.m_experienceMilestone.Unlock();
			}
		}
		else if (located && (this.m_disasters.m_buffer[(int)disasterIndex].m_flags & (DisasterData.Flags.Detected | DisasterData.Flags.Located | DisasterData.Flags.UnDetected)) == DisasterData.Flags.Detected)
		{
			DisasterData[] expr_16D_cp_0 = this.m_disasters.m_buffer;
			expr_16D_cp_0[(int)disasterIndex].m_flags = (expr_16D_cp_0[(int)disasterIndex].m_flags | DisasterData.Flags.Located);
			this.HazardModified(disasterIndex);
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_detectedDisastersUpdated != null)
				{
					this.m_detectedDisastersUpdated();
				}
			});
		}
	}

	public void UnDetectDisaster(ushort disasterIndex)
	{
		if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None)
		{
			DisasterData[] expr_32_cp_0 = this.m_disasters.m_buffer;
			expr_32_cp_0[(int)disasterIndex].m_flags = (expr_32_cp_0[(int)disasterIndex].m_flags & ~(DisasterData.Flags.Detected | DisasterData.Flags.Located | DisasterData.Flags.Warning));
			DisasterData[] expr_54_cp_0 = this.m_disasters.m_buffer;
			expr_54_cp_0[(int)disasterIndex].m_flags = (expr_54_cp_0[(int)disasterIndex].m_flags | (DisasterData.Flags.UnDetected | DisasterData.Flags.UnReported));
			this.HazardModified(disasterIndex);
			if (--this.m_detectedDisasterCount == 0)
			{
				Singleton<BuildingManager>.get_instance().m_evacuationNotUsed.Deactivate();
			}
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				if (this.m_detectedDisastersUpdated != null)
				{
					this.m_detectedDisastersUpdated();
				}
			});
			if (this.m_disasters.m_buffer[(int)disasterIndex].m_collapsedCount != 0 && this.m_disasters.m_buffer[(int)disasterIndex].m_casualtiesCount == 0u && this.m_properties != null && this.m_properties.m_safeDestructionMilestone != null)
			{
				this.m_properties.m_safeDestructionMilestone.Unlock();
			}
		}
	}

	public void FollowDisaster(ushort disasterIndex)
	{
		if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & DisasterData.Flags.Significant) != DisasterData.Flags.None)
		{
			this.DetectDisaster(disasterIndex, false);
			if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & DisasterData.Flags.Warning) != DisasterData.Flags.None)
			{
				DisasterData[] expr_5B_cp_0 = this.m_disasters.m_buffer;
				expr_5B_cp_0[(int)disasterIndex].m_flags = (expr_5B_cp_0[(int)disasterIndex].m_flags & ~DisasterData.Flags.Warning);
				this.m_disasters.m_buffer[(int)disasterIndex].m_broadcastCooldown = 2;
				this.HazardModified(disasterIndex);
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					if (this.m_detectedDisastersUpdated != null)
					{
						this.m_detectedDisastersUpdated();
					}
				});
			}
			if ((this.m_disasters.m_buffer[(int)disasterIndex].m_flags & DisasterData.Flags.Follow) == DisasterData.Flags.None)
			{
				DisasterData[] expr_D3_cp_0 = this.m_disasters.m_buffer;
				expr_D3_cp_0[(int)disasterIndex].m_flags = (expr_D3_cp_0[(int)disasterIndex].m_flags | DisasterData.Flags.Follow);
				if (!this.m_disableAutomaticFollow)
				{
					InstanceID id = InstanceID.Empty;
					id.Disaster = disasterIndex;
					Vector3 p;
					Quaternion quaternion;
					Vector3 vector;
					if (InstanceManager.GetPosition(id, out p, out quaternion, out vector) && !Singleton<GameAreaManager>.get_instance().PointOutOfArea(p))
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							this.m_followTime = Time.get_realtimeSinceStartup();
							if (this.m_cameraController != null)
							{
								this.m_cameraController.SetTarget(id, this.m_cameraController.get_transform().get_position(), true);
								Singleton<SimulationManager>.get_instance().AddAction(delegate
								{
									if (!Singleton<SimulationManager>.get_instance().SimulationPaused)
									{
										Singleton<SimulationManager>.get_instance().SelectedSimulationSpeed = 1;
									}
								});
							}
						});
					}
				}
			}
		}
	}

	public ushort FindDisaster<T>(Vector3 position) where T : DisasterAI
	{
		ushort result = 0;
		float num = -1f;
		for (int i = 1; i < this.m_disasters.m_size; i++)
		{
			if (this.m_disasters.m_buffer[i].m_flags != DisasterData.Flags.None)
			{
				DisasterInfo info = this.m_disasters.m_buffer[i].Info;
				float num2;
				if (info != null && info.m_disasterAI is T && info.m_disasterAI.CanAffectAt((ushort)i, ref this.m_disasters.m_buffer[i], position, out num2) && num2 > num)
				{
					result = (ushort)i;
					num = num2;
				}
			}
		}
		return result;
	}

	public void ClearAll()
	{
		SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
		if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
		{
			this.m_disasters.Clear();
			this.m_disasters.Add(default(DisasterData));
			this.m_disasterCount = 0;
		}
		this.m_clearAll = true;
	}

	private DisasterInfo FindRandomDisasterInfo()
	{
		UnlockManager instance = Singleton<UnlockManager>.get_instance();
		int num = PrefabCollection<DisasterInfo>.PrefabCount();
		int num2 = 0;
		for (int i = 0; i < num; i++)
		{
			DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)i);
			if (prefab != null && instance.Unlocked(prefab.m_UnlockMilestone))
			{
				num2 += prefab.m_finalRandomProbability;
			}
		}
		if (num2 == 0)
		{
			return null;
		}
		num2 = Singleton<SimulationManager>.get_instance().m_randomizer.Int32((uint)num2);
		for (int j = 0; j < num; j++)
		{
			DisasterInfo prefab2 = PrefabCollection<DisasterInfo>.GetPrefab((uint)j);
			if (prefab2 != null && instance.Unlocked(prefab2.m_UnlockMilestone))
			{
				if (num2 < prefab2.m_finalRandomProbability)
				{
					return prefab2;
				}
				num2 -= prefab2.m_finalRandomProbability;
			}
		}
		return null;
	}

	public void StartRandomDisaster()
	{
		DisasterInfo disasterInfo = this.FindRandomDisasterInfo();
		Vector3 targetPosition;
		float angle;
		ushort num;
		if (disasterInfo != null && disasterInfo.m_disasterAI.FindRandomTarget(out targetPosition, out angle) && this.CreateDisaster(out num, disasterInfo))
		{
			this.m_disasters.m_buffer[(int)num].m_targetPosition = targetPosition;
			this.m_disasters.m_buffer[(int)num].m_angle = angle;
			this.m_disasters.m_buffer[(int)num].m_intensity = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10, 100);
			DisasterData[] expr_98_cp_0 = this.m_disasters.m_buffer;
			ushort expr_98_cp_1 = num;
			expr_98_cp_0[(int)expr_98_cp_1].m_flags = (expr_98_cp_0[(int)expr_98_cp_1].m_flags | DisasterData.Flags.SelfTrigger);
			disasterInfo.m_disasterAI.StartNow(num, ref this.m_disasters.m_buffer[(int)num]);
		}
	}

	public void AddEvacuationArea(Vector3 position, float radius)
	{
		int num = Mathf.Max((int)((position.x - radius) / 38.4f + 128f), 0);
		int num2 = Mathf.Max((int)((position.z - radius) / 38.4f + 128f), 0);
		int num3 = Mathf.Min((int)((position.x + radius) / 38.4f + 128f), 255);
		int num4 = Mathf.Min((int)((position.z + radius) / 38.4f + 128f), 255);
		radius *= radius;
		for (int i = num2; i <= num4; i++)
		{
			float num5 = ((float)i - 128f + 0.5f) * 38.4f - position.z;
			num5 *= num5;
			for (int j = num; j <= num3; j++)
			{
				float num6 = ((float)j - 128f + 0.5f) * 38.4f - position.x;
				num6 *= num6;
				if (num5 + num6 < radius)
				{
					int num7 = i * 256 + j;
					this.m_evacuationMap[num7 >> 4] |= 1u << ((num7 & 15) << 1);
				}
			}
		}
	}

	public bool IsEvacuating(Vector3 position)
	{
		int num = Mathf.Clamp((int)(position.x / 38.4f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(position.z / 38.4f + 128f), 0, 255);
		int num3 = num2 * 256 + num;
		return (this.m_evacuationMap[num3 >> 4] & 3u << ((num3 & 15) << 1)) != 0u;
	}

	public void EvacuateAll(bool release)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(ItemClass.Service.Disaster);
		if (serviceBuildings != null)
		{
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num = serviceBuildings.m_buffer[i];
				if (num != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
					if (info != null)
					{
						ShelterAI shelterAI = info.m_buildingAI as ShelterAI;
						if (shelterAI != null)
						{
							shelterAI.SetEmptying(num, ref instance.m_buildings.m_buffer[(int)num], release);
						}
					}
				}
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
			if ((mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				int areaCount = Singleton<GameAreaManager>.get_instance().m_areaCount;
				float num = this.m_randomDisastersProbability;
				if (num > 0.001f)
				{
					num *= num;
					num *= (float)((areaCount > 1) ? (550 + areaCount * 50) : 500);
					int num2 = Mathf.Max(1, Mathf.RoundToInt(num));
					if (this.m_randomDisasterCooldown < 65536 - 49152 * num2 / 1000)
					{
						this.m_randomDisasterCooldown++;
					}
					else
					{
						SimulationManager instance = Singleton<SimulationManager>.get_instance();
						if (instance.m_randomizer.Int32(67108864u) < num2)
						{
							if (string.IsNullOrEmpty(instance.m_metaData.m_ScenarioAsset))
							{
								this.StartRandomDisaster();
							}
							this.m_randomDisasterCooldown = 0;
						}
					}
				}
			}
		}
		if (subStep != 0 && subStep != 1000)
		{
			int num3 = this.m_evacuationMap.Length;
			int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num5 = num4 * (num3 >> 8);
			int num6 = (num4 + 1) * (num3 >> 8) - 1;
			for (int i = num5; i <= num6; i++)
			{
				this.m_evacuationMap[i] = (this.m_evacuationMap[i] & 1431655765u) << 1;
			}
		}
		if (subStep != 0)
		{
			int num7 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num8 = num7;
			int num9 = num7 + 1 - 1;
			for (int j = num8; j <= num9; j++)
			{
				if (j < this.m_disasters.m_size)
				{
					DisasterData.Flags flags = this.m_disasters.m_buffer[j].m_flags;
					if ((flags & DisasterData.Flags.Created) != DisasterData.Flags.None)
					{
						DisasterInfo info = this.m_disasters.m_buffer[j].Info;
						info.m_disasterAI.SimulationStep((ushort)j, ref this.m_disasters.m_buffer[j]);
						if ((this.m_disasters.m_buffer[j].m_flags & (DisasterData.Flags.Finished | DisasterData.Flags.Persistent | DisasterData.Flags.UnReported)) == DisasterData.Flags.Finished)
						{
							this.ReleaseDisaster((ushort)j);
						}
					}
				}
			}
		}
		if (subStep <= 1)
		{
			int num10 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num11 = num10 * PrefabCollection<DisasterInfo>.PrefabCount() >> 10;
			int num12 = ((num10 + 1) * PrefabCollection<DisasterInfo>.PrefabCount() >> 10) - 1;
			for (int k = num11; k <= num12; k++)
			{
				DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)k);
				if (prefab != null)
				{
					prefab.CheckUnlocking();
				}
			}
		}
	}

	public bool RayCast(Segment3 ray, DisasterData.Flags ignoreFlags, out Vector3 hit, out ushort disasterIndex)
	{
		float num = 2f;
		disasterIndex = 0;
		for (int i = 1; i < this.m_disasters.m_size; i++)
		{
			DisasterData.Flags flags = this.m_disasters.m_buffer[i].m_flags;
			float num2;
			if (flags != DisasterData.Flags.None && (flags & ignoreFlags) == DisasterData.Flags.None && this.m_disasters.m_buffer[i].RayCast(ray, out num2) && num2 < num)
			{
				num = num2;
				disasterIndex = (ushort)i;
			}
		}
		if (disasterIndex != 0)
		{
			hit = ray.Position(num);
			return true;
		}
		hit = Vector3.get_zero();
		return false;
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<DisasterInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				prefab.CheckUnlocking();
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<DisasterInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetDisasterName(ushort disasterID, string name)
	{
		DisasterManager.<SetDisasterName>c__Iterator0 <SetDisasterName>c__Iterator = new DisasterManager.<SetDisasterName>c__Iterator0();
		<SetDisasterName>c__Iterator.disasterID = disasterID;
		<SetDisasterName>c__Iterator.name = name;
		<SetDisasterName>c__Iterator.$this = this;
		return <SetDisasterName>c__Iterator;
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("DisasterManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < this.m_disasters.m_size; i++)
		{
			if (this.m_disasters.m_buffer[i].m_flags != DisasterData.Flags.None && this.m_disasters.m_buffer[i].Info == null)
			{
				this.ReleaseDisaster((ushort)i);
			}
		}
		int num = PrefabCollection<DisasterInfo>.PrefabCount();
		this.m_infoCount = num;
		for (int j = 0; j < num; j++)
		{
			DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)j);
			if (prefab != null)
			{
				if (this.m_clearAll)
				{
					prefab.m_lastSpawnFrame = 0u;
				}
				if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario || prefab.m_disasterWarningGuide == null)
				{
					prefab.m_disasterWarningGuide = new DisasterTypeGuide();
				}
				if (prefab.m_UnlockMilestone != null)
				{
					switch (mode)
					{
					case SimulationManager.UpdateMode.NewMap:
					case SimulationManager.UpdateMode.LoadMap:
					case SimulationManager.UpdateMode.NewAsset:
					case SimulationManager.UpdateMode.LoadAsset:
						prefab.m_UnlockMilestone.Reset(false);
						break;
					case SimulationManager.UpdateMode.NewGameFromMap:
					case SimulationManager.UpdateMode.NewScenarioFromMap:
					case SimulationManager.UpdateMode.UpdateScenarioFromMap:
						prefab.m_UnlockMilestone.Reset(false);
						break;
					case SimulationManager.UpdateMode.LoadGame:
					case SimulationManager.UpdateMode.NewScenarioFromGame:
					case SimulationManager.UpdateMode.LoadScenario:
					case SimulationManager.UpdateMode.NewGameFromScenario:
					case SimulationManager.UpdateMode.UpdateScenarioFromGame:
						prefab.m_UnlockMilestone.Reset(true);
						break;
					}
				}
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("DisasterManager.LateUpdateData");
		base.LateUpdateData(mode);
		if (mode == SimulationManager.UpdateMode.UpdateScenarioFromGame || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
		{
			for (int i = 1; i < this.m_disasters.m_size; i++)
			{
				if (this.m_disasters.m_buffer[i].m_flags != DisasterData.Flags.None)
				{
					this.m_disasters.m_buffer[i].m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(this.m_disasters.m_buffer[i].m_targetPosition, false, 0f);
				}
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new DisasterManager.Data());
	}

	public string GetDisasterName(ushort disasterID)
	{
		if (this.m_disasters.m_buffer[(int)disasterID].m_flags != DisasterData.Flags.None)
		{
			string text = null;
			if ((this.m_disasters.m_buffer[(int)disasterID].m_flags & DisasterData.Flags.CustomName) != DisasterData.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.Disaster = disasterID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateName(disasterID);
			}
			return text;
		}
		return null;
	}

	private string GenerateName(ushort disasterID)
	{
		string text = null;
		DisasterInfo info = this.m_disasters.m_buffer[(int)disasterID].Info;
		if (info != null)
		{
			text = info.m_disasterAI.GenerateName(disasterID, ref this.m_disasters.m_buffer[(int)disasterID]);
		}
		if (text == null)
		{
			text = "Invalid";
		}
		return text;
	}

	public void HazardModified(ushort disasterID)
	{
		DisasterInfo info = this.m_disasters.m_buffer[(int)disasterID].Info;
		InfoManager.SubInfoMode subInfoMode;
		if (info != null && info.m_disasterAI.GetHazardSubMode(out subInfoMode))
		{
			this.m_hazardModified |= 1 << (int)subInfoMode;
		}
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const int MAX_DISASTER_COUNT = 256;

	public const int MAX_SCENARIO_DISASTERS = 200;

	public const float HAZARDMAP_CELL_SIZE = 38.4f;

	public const int HAZARDMAP_RESOLUTION = 256;

	public const float EVACUATIONMAP_CELL_SIZE = 38.4f;

	public const int EVACUATIONMAP_RESOLUTION = 256;

	public int m_disasterCount;

	public int m_infoCount;

	public bool m_disableAutomaticFollow;

	public bool m_disableCameraShake;

	[NonSerialized]
	public FastList<DisasterData> m_disasters;

	[NonSerialized]
	public float m_randomDisastersProbability;

	[NonSerialized]
	public int m_randomDisasterCooldown;

	[NonSerialized]
	public int m_detectedDisasterCount;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_Color;

	private int m_markerLayer;

	private ushort m_highlightDisaster;

	private CameraController m_cameraController;

	private Texture2D m_hazardTexture;

	private int m_hazardModified;

	private int m_hazardMapVisible;

	private float m_followTime;

	private byte[] m_hazardAmount;

	private uint[] m_evacuationMap;

	private bool m_clearAll;

	private bool m_markersVisible;

	private float m_markerAlpha;

	[NonSerialized]
	public DisasterWrapper m_DisasterWrapper;

	public delegate void DetectedDisastersUpdatedHandler();

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "DisasterManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			DisasterManager instance2 = Singleton<DisasterManager>.get_instance();
			DisasterData[] buffer = instance2.m_disasters.m_buffer;
			int size = instance2.m_disasters.m_size;
			s.WriteUInt16((uint)(size - 1));
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < size; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			int num = PrefabCollection<DisasterInfo>.PrefabCount();
			uint num2 = 0u;
			uint num3 = 0u;
			uint num4 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int j = 0; j < num; j++)
				{
					DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab((uint)j);
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num2 += 1u;
					}
					if (prefab != null && prefab.m_lastSpawnFrame != 0u && prefab.m_prefabDataIndex != -1)
					{
						num3 += 1u;
					}
					if (prefab != null && prefab.m_disasterWarningGuide != null && prefab.m_prefabDataIndex != -1)
					{
						num4 += 1u;
					}
				}
			}
			s.WriteUInt16(num2);
			s.WriteUInt16(num3);
			s.WriteUInt16(num4);
			try
			{
				PrefabCollection<DisasterInfo>.BeginSerialize(s);
				for (int k = 1; k < size; k++)
				{
					if (buffer[k].m_flags != DisasterData.Flags.None)
					{
						PrefabCollection<DisasterInfo>.Serialize((uint)buffer[k].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int l = 0; l < num; l++)
					{
						DisasterInfo prefab2 = PrefabCollection<DisasterInfo>.GetPrefab((uint)l);
						if (prefab2 != null && prefab2.m_UnlockMilestone != null && prefab2.m_prefabDataIndex != -1)
						{
							PrefabCollection<DisasterInfo>.Serialize((uint)prefab2.m_prefabDataIndex);
						}
					}
					for (int m = 0; m < num; m++)
					{
						DisasterInfo prefab3 = PrefabCollection<DisasterInfo>.GetPrefab((uint)m);
						if (prefab3 != null && prefab3.m_lastSpawnFrame != 0u && prefab3.m_prefabDataIndex != -1)
						{
							PrefabCollection<DisasterInfo>.Serialize((uint)prefab3.m_prefabDataIndex);
						}
					}
					for (int n = 0; n < num; n++)
					{
						DisasterInfo prefab4 = PrefabCollection<DisasterInfo>.GetPrefab((uint)n);
						if (prefab4 != null && prefab4.m_disasterWarningGuide != null && prefab4.m_prefabDataIndex != -1)
						{
							PrefabCollection<DisasterInfo>.Serialize((uint)prefab4.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<DisasterInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int num5 = 0; num5 < num; num5++)
				{
					DisasterInfo prefab5 = PrefabCollection<DisasterInfo>.GetPrefab((uint)num5);
					if (prefab5 != null && prefab5.m_UnlockMilestone != null && prefab5.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab5.m_UnlockMilestone.GetData());
					}
				}
				for (int num6 = 0; num6 < num; num6++)
				{
					DisasterInfo prefab6 = PrefabCollection<DisasterInfo>.GetPrefab((uint)num6);
					if (prefab6 != null && prefab6.m_lastSpawnFrame != 0u && prefab6.m_prefabDataIndex != -1)
					{
						s.WriteUInt32(prefab6.m_lastSpawnFrame);
					}
				}
				for (int num7 = 0; num7 < num; num7++)
				{
					DisasterInfo prefab7 = PrefabCollection<DisasterInfo>.GetPrefab((uint)num7);
					if (prefab7 != null && prefab7.m_disasterWarningGuide != null && prefab7.m_prefabDataIndex != -1)
					{
						s.WriteObject<DisasterTypeGuide>(prefab7.m_disasterWarningGuide);
					}
				}
			}
			for (int num8 = 1; num8 < size; num8++)
			{
				if (buffer[num8].m_flags != DisasterData.Flags.None)
				{
					s.WriteVector3(buffer[num8].m_targetPosition);
					s.WriteULong64(buffer[num8].m_randomSeed);
					s.WriteUInt32(buffer[num8].m_startFrame);
					s.WriteUInt32(buffer[num8].m_activationFrame);
					s.WriteUInt16((uint)buffer[num8].m_waveIndex);
					s.WriteFloat(buffer[num8].m_angle);
					s.WriteUInt8((uint)buffer[num8].m_intensity);
					s.WriteUInt32(buffer[num8].m_casualtiesCount);
					s.WriteUInt16((uint)buffer[num8].m_collapsedCount);
					s.WriteUInt32(buffer[num8].m_destroyedRoadLength);
					s.WriteUInt32(buffer[num8].m_destroyedTrackLength);
					s.WriteUInt32(buffer[num8].m_destroyedPowerLength);
					s.WriteUInt32(buffer[num8].m_treeFireCount);
					s.WriteUInt16((uint)buffer[num8].m_upgradedCount);
					s.WriteUInt16((uint)buffer[num8].m_buildingFireCount);
					s.WriteUInt8((uint)buffer[num8].m_chirpCooldown);
					s.WriteUInt8((uint)buffer[num8].m_broadcastCooldown);
				}
			}
			s.WriteUInt16((uint)instance2.m_randomDisasterCooldown);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "DisasterManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "DisasterManager");
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			int num = 0;
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			FastList<DisasterData> fastList;
			if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
			{
				fastList = instance.m_disasters;
				instance.m_disasterCount = 0;
			}
			else
			{
				fastList = new FastList<DisasterData>();
			}
			int num2 = (int)(s.ReadUInt16() + 1u);
			fastList.Clear();
			fastList.EnsureCapacity(num2);
			fastList.Add(default(DisasterData));
			DisasterData[] buffer = fastList.m_buffer;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
			for (int i = 1; i < num2; i++)
			{
				DisasterData item = default(DisasterData);
				item.m_flags = (DisasterData.Flags)(uInt.Read() & 4294934527u);
				fastList.Add(item);
				if (item.m_flags != DisasterData.Flags.None)
				{
					num++;
				}
			}
			uInt.EndRead();
			uint num3 = s.ReadUInt16();
			uint num4 = 0u;
			uint num5 = 0u;
			if (s.get_version() >= 291u)
			{
				num4 = s.ReadUInt16();
			}
			if (s.get_version() >= 293u)
			{
				num5 = s.ReadUInt16();
			}
			this.m_milestoneInfoIndex = new uint[num3];
			this.m_UnlockMilestones = new MilestoneInfo.Data[num3];
			this.m_lastSpawnFrameInfoIndex = new uint[num4];
			this.m_lastSpawnFrames = new uint[num4];
			this.m_guideInfoIndex = new uint[num5];
			this.m_disasterWarningGuides = new DisasterTypeGuide[num5];
			PrefabCollection<DisasterInfo>.BeginDeserialize(s);
			for (int j = 1; j < num2; j++)
			{
				if (buffer[j].m_flags != DisasterData.Flags.None)
				{
					buffer[j].m_infoIndex = (ushort)PrefabCollection<DisasterInfo>.Deserialize(true);
				}
			}
			for (uint num6 = 0u; num6 < num3; num6 += 1u)
			{
				this.m_milestoneInfoIndex[(int)((UIntPtr)num6)] = PrefabCollection<DisasterInfo>.Deserialize(false);
			}
			for (uint num7 = 0u; num7 < num4; num7 += 1u)
			{
				this.m_lastSpawnFrameInfoIndex[(int)((UIntPtr)num7)] = PrefabCollection<DisasterInfo>.Deserialize(false);
			}
			for (uint num8 = 0u; num8 < num5; num8 += 1u)
			{
				this.m_guideInfoIndex[(int)((UIntPtr)num8)] = PrefabCollection<DisasterInfo>.Deserialize(false);
			}
			PrefabCollection<DisasterInfo>.EndDeserialize(s);
			for (uint num9 = 0u; num9 < num3; num9 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num9)] = s.ReadObject<MilestoneInfo.Data>();
			}
			for (uint num10 = 0u; num10 < num4; num10 += 1u)
			{
				this.m_lastSpawnFrames[(int)((UIntPtr)num10)] = s.ReadUInt32();
			}
			for (uint num11 = 0u; num11 < num5; num11 += 1u)
			{
				this.m_disasterWarningGuides[(int)((UIntPtr)num11)] = s.ReadObject<DisasterTypeGuide>();
			}
			for (int k = 1; k < num2; k++)
			{
				if (buffer[k].m_flags != DisasterData.Flags.None)
				{
					if (s.get_version() >= 258u)
					{
						buffer[k].m_targetPosition = s.ReadVector3();
						buffer[k].m_randomSeed = s.ReadULong64();
						buffer[k].m_startFrame = s.ReadUInt32();
					}
					else
					{
						buffer[k].m_targetPosition = Vector3.get_zero();
						buffer[k].m_randomSeed = 0uL;
						buffer[k].m_startFrame = 0u;
					}
					if (s.get_version() >= 263u)
					{
						buffer[k].m_activationFrame = s.ReadUInt32();
					}
					else
					{
						buffer[k].m_activationFrame = 0u;
					}
					if (s.get_version() >= 259u)
					{
						buffer[k].m_waveIndex = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[k].m_waveIndex = 0;
					}
					if (s.get_version() >= 260u)
					{
						buffer[k].m_angle = s.ReadFloat();
					}
					else
					{
						buffer[k].m_angle = 0f;
					}
					if (s.get_version() >= 264u)
					{
						buffer[k].m_intensity = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[k].m_intensity = 55;
					}
					if (s.get_version() >= 289u)
					{
						buffer[k].m_casualtiesCount = s.ReadUInt32();
						buffer[k].m_collapsedCount = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[k].m_casualtiesCount = 0u;
						buffer[k].m_collapsedCount = 0;
					}
					if (s.get_version() >= 300u)
					{
						buffer[k].m_destroyedRoadLength = s.ReadUInt32();
					}
					else
					{
						buffer[k].m_destroyedRoadLength = 0u;
					}
					if (s.get_version() >= 301u)
					{
						buffer[k].m_destroyedTrackLength = s.ReadUInt32();
						buffer[k].m_destroyedPowerLength = s.ReadUInt32();
						buffer[k].m_treeFireCount = s.ReadUInt32();
						buffer[k].m_upgradedCount = (ushort)s.ReadUInt16();
						buffer[k].m_buildingFireCount = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[k].m_destroyedPowerLength = 0u;
						buffer[k].m_treeFireCount = 0u;
						buffer[k].m_upgradedCount = 0;
						buffer[k].m_buildingFireCount = 0;
					}
					if (s.get_version() >= 296u)
					{
						buffer[k].m_chirpCooldown = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[k].m_chirpCooldown = 0;
					}
					if (s.get_version() >= 297u)
					{
						buffer[k].m_broadcastCooldown = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[k].m_broadcastCooldown = 0;
					}
				}
			}
			if (s.get_version() >= 282u)
			{
				instance.m_randomDisasterCooldown = (int)s.ReadUInt16();
			}
			else
			{
				instance.m_randomDisasterCooldown = 16384;
			}
			if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
			{
				instance.m_disasterCount = num;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "DisasterManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "DisasterManager");
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<DisasterInfo>.BindPrefabs();
			DisasterData[] buffer = instance.m_disasters.m_buffer;
			int size = instance.m_disasters.m_size;
			if (this.m_milestoneInfoIndex != null)
			{
				int num = this.m_milestoneInfoIndex.Length;
				for (int i = 0; i < num; i++)
				{
					DisasterInfo prefab = PrefabCollection<DisasterInfo>.GetPrefab(this.m_milestoneInfoIndex[i]);
					if (prefab != null)
					{
						MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[i]);
						}
					}
				}
			}
			int num2 = PrefabCollection<DisasterInfo>.PrefabCount();
			for (int j = 0; j < num2; j++)
			{
				DisasterInfo prefab2 = PrefabCollection<DisasterInfo>.GetPrefab((uint)j);
				if (prefab2 != null)
				{
					prefab2.m_lastSpawnFrame = 0u;
					prefab2.m_disasterWarningGuide = null;
				}
			}
			if (this.m_lastSpawnFrameInfoIndex != null)
			{
				int num3 = this.m_lastSpawnFrameInfoIndex.Length;
				for (int k = 0; k < num3; k++)
				{
					DisasterInfo prefab3 = PrefabCollection<DisasterInfo>.GetPrefab(this.m_lastSpawnFrameInfoIndex[k]);
					if (prefab3 != null)
					{
						prefab3.m_lastSpawnFrame = this.m_lastSpawnFrames[k];
					}
				}
			}
			if (this.m_guideInfoIndex != null)
			{
				int num4 = this.m_guideInfoIndex.Length;
				for (int l = 0; l < num4; l++)
				{
					DisasterInfo prefab4 = PrefabCollection<DisasterInfo>.GetPrefab(this.m_guideInfoIndex[l]);
					if (prefab4 != null)
					{
						prefab4.m_disasterWarningGuide = this.m_disasterWarningGuides[l];
					}
				}
			}
			instance.m_detectedDisasterCount = 0;
			for (int m = 1; m < size; m++)
			{
				if (buffer[m].m_flags != DisasterData.Flags.None)
				{
					DisasterInfo info = buffer[m].Info;
					if (info != null)
					{
						buffer[m].m_infoIndex = (ushort)info.m_prefabDataIndex;
						if ((buffer[m].m_flags & DisasterData.Flags.Detected) != DisasterData.Flags.None)
						{
							instance.m_detectedDisasterCount++;
						}
					}
				}
			}
			instance.m_clearAll = false;
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "DisasterManager");
		}

		private uint[] m_milestoneInfoIndex;

		private MilestoneInfo.Data[] m_UnlockMilestones;

		private uint[] m_lastSpawnFrameInfoIndex;

		private uint[] m_lastSpawnFrames;

		private uint[] m_guideInfoIndex;

		private DisasterTypeGuide[] m_disasterWarningGuides;
	}
}
