﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class MeteorAI : VehicleAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_impactEffect != null)
		{
			this.m_impactEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_impactEffect != null)
		{
			this.m_impactEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		if (swayPosition.get_sqrMagnitude() > 0.01f)
		{
			int num = 20;
			float num2 = Singleton<SimulationManager>.get_instance().m_simulationTimer * (float)(6 * num);
			Quaternion quaternion = Quaternion.AngleAxis(num2, swayPosition);
			Matrix4x4 result = default(Matrix4x4);
			result.SetTRS(position, rotation * quaternion, scale);
			return result;
		}
		Matrix4x4 result2 = default(Matrix4x4);
		result2.SetTRS(position, rotation, scale);
		return result2;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		return Matrix4x4.get_identity();
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingSpace;
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.Flying)) == Vehicle.Flags.Spawned && data.m_waitCounter < 10)
		{
			float magnitude = (data.m_targetPos0 - data.GetLastFramePosition()).get_magnitude();
			if (magnitude <= 1f)
			{
				data.m_flags |= Vehicle.Flags.Flying;
			}
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, 0);
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		Vector3 vector = frameData.m_rotation * Vector3.get_forward();
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num = -Mathf.Atan2(vector.x, vector.z);
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		Vector3 vector2 = vehicleData.m_targetPos0 - frameData.m_position;
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector2 = quaternion * vector2;
		float magnitude = vector2.get_magnitude();
		float maxSpeed = this.m_info.m_maxSpeed;
		if (magnitude > 1f)
		{
			vehicleData.m_flags |= Vehicle.Flags.Flying;
		}
		else if (this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			return;
		}
		Vector3 vector3 = Vector3.ClampMagnitude(vector2, maxSpeed);
		frameData.m_velocity = frameData.m_rotation * vector3;
		if ((vehicleData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			this.CheckCollisions(vehicleID, ref vehicleData, frameData.m_position, frameData.m_velocity);
		}
		frameData.m_position += frameData.m_velocity * 0.5f;
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		Randomizer randomizer;
		randomizer..ctor((int)vehicleID);
		int num2 = randomizer.Int32(-1000, 1000);
		int num3 = randomizer.Int32(-1000, 1000);
		int num4 = randomizer.Int32(-1000, 1000);
		if (num2 == 0 && num3 == 0 && num4 == 0)
		{
			num3 = 1;
		}
		Vector3 vector4;
		vector4.x = (float)num2;
		vector4.y = (float)num3;
		vector4.z = (float)num4;
		frameData.m_swayVelocity = Vector3.get_zero();
		frameData.m_swayPosition = vector4.get_normalized();
		frameData.m_steerAngle = 0f;
		frameData.m_travelDistance = magnitude;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = 5f;
		frameData.m_lightIntensity.w = 5f;
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 10f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	private void CheckCollisions(ushort vehicleID, ref Vehicle vehicleData, Vector3 pos, Vector3 velocity)
	{
		InstanceID id = default(InstanceID);
		id.Vehicle = vehicleID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		Segment3 ray;
		ray..ctor(pos, pos + velocity);
		float num = (this.m_info.m_generatedInfo.m_size.x + this.m_info.m_generatedInfo.m_size.y + this.m_info.m_generatedInfo.m_size.z) / 3f;
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		DisasterHelpers.DestroyStuff(group, ray, num * 0.5f, currentFrameIndex + 16u, currentFrameIndex + 32u);
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		Vector3 vector = vehicleData.m_targetPos0;
		InstanceID instanceID = default(InstanceID);
		instanceID.Vehicle = vehicleID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(instanceID);
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		float num = 1f;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				num = 0.2f + (float)instance.m_disasters.m_buffer[(int)disaster].m_intensity * 0.0145454546f;
			}
		}
		float num2 = this.m_craterRadius * num;
		float num3 = this.m_craterDepth * num;
		float destructionRadiusMin = this.m_destructionRadiusMin * num;
		float num4 = this.m_destructionRadiusMax * num;
		float burnRadiusMin = this.m_burnRadiusMin * num;
		float num5 = this.m_burnRadiusMax * num;
		if (vehicleData.m_waitCounter == 0)
		{
			DisasterHelpers.SplashWater(VectorUtils.XZ(vector), num2 * 2f, num3 * 10f);
			if (group != null)
			{
				ushort disaster2 = group.m_ownerInstance.Disaster;
				if (disaster2 != 0)
				{
					DisasterInfo info = instance.m_disasters.m_buffer[(int)disaster2].Info;
					info.m_disasterAI.ActivateNow(disaster2, ref instance.m_disasters.m_buffer[(int)disaster2]);
				}
			}
		}
		if ((vehicleData.m_waitCounter += 1) > 10)
		{
			return true;
		}
		if (vehicleData.m_waitCounter < 2)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(vector, Vector3.get_up(), 0f);
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_impactEffect, instanceID, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<VehicleManager>.get_instance().m_audioGroup);
			vehicleData.m_flags &= ~Vehicle.Flags.Flying;
		}
		float num6 = Mathf.Max(num4, num5);
		float num7 = (float)(vehicleData.m_waitCounter - 3) * (num6 / 8f);
		float num8 = (float)(vehicleData.m_waitCounter - 2) * (num6 / 8f);
		if (num7 < num2 && (num8 >= num2 || vehicleData.m_waitCounter == 10))
		{
			DisasterHelpers.MakeCrater(VectorUtils.XZ(vector), num2, num3, true);
		}
		Vector3 directionalWind;
		directionalWind..ctor(0f, 200f, 0f);
		DisasterHelpers.AddWind(vector, num4, directionalWind, 0f, 200f, group);
		DisasterHelpers.DestroyStuff((int)vehicleID, group, vector, num8, num2 + num8 / num6 * (num6 - num2), num2, destructionRadiusMin, num4, burnRadiusMin, num5);
		DisasterHelpers.BurnGround(VectorUtils.XZ(vector), num8, 1f);
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
		vehicleData.m_targetPos0 = endPos;
		vehicleData.m_targetPos0.w = 2f;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
		this.TrySpawn(vehicleID, ref vehicleData);
		return true;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		vehicleData.m_waitCounter = 0;
		return true;
	}

	public float m_craterRadius = 75f;

	public float m_craterDepth = 25f;

	public float m_destructionRadiusMin = 125f;

	public float m_destructionRadiusMax = 175f;

	public float m_burnRadiusMin = 150f;

	public float m_burnRadiusMax = 200f;

	public EffectInfo m_impactEffect;
}
