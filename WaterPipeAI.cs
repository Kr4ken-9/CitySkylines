﻿using System;
using ColossalFramework;
using UnityEngine;

public class WaterPipeAI : PlayerNetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Water || infoMode == InfoManager.InfoMode.Heating)
		{
			NetNode.Flags flags = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_startNode].m_flags;
			NetNode.Flags flags2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_endNode].m_flags;
			Color result;
			result..ctor(0f, 0f, 0f, 0f);
			if ((flags & flags2 & NetNode.Flags.Water) != NetNode.Flags.None)
			{
				result.r = 1f;
			}
			if ((flags & flags2 & NetNode.Flags.Sewage) != NetNode.Flags.None)
			{
				result.g = 1f;
			}
			if ((flags & flags2 & NetNode.Flags.Heating) != NetNode.Flags.None)
			{
				result.b = 1f;
			}
			return result;
		}
		if (infoMode != InfoManager.InfoMode.Pollution)
		{
			return base.GetColor(segmentID, ref data, infoMode);
		}
		NetNode.Flags flags3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_startNode].m_flags;
		NetNode.Flags flags4 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_endNode].m_flags;
		if ((flags3 & flags4 & NetNode.Flags.Water) != NetNode.Flags.None)
		{
			int pollution = (int)Singleton<WaterManager>.get_instance().m_nodeData[(int)data.m_startNode].m_pollution;
			int pollution2 = (int)Singleton<WaterManager>.get_instance().m_nodeData[(int)data.m_endNode].m_pollution;
			float num = Mathf.Clamp01((float)(pollution + pollution2) * 0.005882353f);
			return new Color(num, num, num, 1f);
		}
		return new Color(0f, 0f, 0f, 0f);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Water || infoMode == InfoManager.InfoMode.Heating)
		{
			Color result;
			result..ctor(0f, 0f, 0f, 0f);
			if ((data.m_flags & NetNode.Flags.Water) != NetNode.Flags.None)
			{
				result.r = 1f;
			}
			if ((data.m_flags & NetNode.Flags.Sewage) != NetNode.Flags.None)
			{
				result.g = 1f;
			}
			if ((data.m_flags & NetNode.Flags.Heating) != NetNode.Flags.None)
			{
				result.b = 1f;
			}
			return result;
		}
		if (infoMode != InfoManager.InfoMode.Pollution)
		{
			return base.GetColor(nodeID, ref data, infoMode);
		}
		if ((data.m_flags & NetNode.Flags.Water) != NetNode.Flags.None)
		{
			float num = Mathf.Clamp01((float)Singleton<WaterManager>.get_instance().m_nodeData[(int)nodeID].m_pollution * 0.0117647061f);
			return new Color(num, num, num, 1f);
		}
		return new Color(0f, 0f, 0f, 0f);
	}

	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		radius = 90f;
		capped = false;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			color = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[22].m_activeColor;
		}
		else
		{
			color = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[2].m_activeColor;
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			mode = InfoManager.InfoMode.Heating;
			subMode = InfoManager.SubInfoMode.Default;
		}
		else
		{
			mode = InfoManager.InfoMode.Water;
			subMode = InfoManager.SubInfoMode.PipeWater;
		}
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return (float)this.m_info.m_class.m_level;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & (NetNode.Flags.End | NetNode.Flags.Junction)) != NetNode.Flags.None)
		{
			building = this.m_junctionInfo;
			heightOffset = 0f;
		}
		else
		{
			building = null;
			heightOffset = 0f;
		}
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Untouchable) != NetNode.Flags.None)
		{
			ushort num = NetNode.FindOwnerBuilding(nodeID, 64f);
			if (num != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				Notification.Problem problems = instance.m_buildings.m_buffer[(int)num].m_problems;
				bool flag = false;
				int num2 = 0;
				while (num2 < 8 && !flag)
				{
					ushort segment = data.GetSegment(num2);
					if (segment != 0)
					{
						if (info.m_class.m_level == ItemClass.Level.Level2)
						{
							NetInfo info2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].Info;
							if (info2.m_class.m_level == ItemClass.Level.Level2)
							{
								flag = true;
							}
						}
						else
						{
							flag = true;
						}
					}
					num2++;
				}
				Notification.Problem problem;
				if (flag)
				{
					problem = Notification.RemoveProblems(problems, (Notification.Problem)(-2147475456));
				}
				else if (info.m_class.m_level == ItemClass.Level.Level2)
				{
					problem = Notification.AddProblems(problems, (Notification.Problem)(-2147483648));
				}
				else
				{
					problem = Notification.AddProblems(problems, Notification.Problem.WaterNotConnected);
				}
				if (problem != problems)
				{
					instance.m_buildings.m_buffer[(int)num].m_problems = problem;
					instance.UpdateNotifications(num, problems, problem);
				}
			}
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.WaterNotConnected);
		}
		ushort building = data.m_building;
		if (building != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			bool flag2 = false;
			int num3 = ((data.m_flags & NetNode.Flags.Water) == NetNode.Flags.None) ? 0 : 1;
			int num4 = ((data.m_flags & NetNode.Flags.Sewage) == NetNode.Flags.None) ? 0 : 1;
			int num5 = ((data.m_flags & NetNode.Flags.Heating) == NetNode.Flags.None) ? 0 : 1;
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_waterBuffer != num3)
			{
				instance2.m_buildings.m_buffer[(int)building].m_waterBuffer = (ushort)num3;
				flag2 = true;
			}
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_sewageBuffer != num4)
			{
				instance2.m_buildings.m_buffer[(int)building].m_sewageBuffer = (ushort)num4;
				flag2 = true;
			}
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_heatingBuffer != num5)
			{
				instance2.m_buildings.m_buffer[(int)building].m_heatingBuffer = (ushort)num5;
				flag2 = true;
			}
			if (flag2)
			{
				instance2.UpdateBuildingColors(building);
			}
		}
		float minX = data.m_position.x - 100f;
		float maxX = data.m_position.x + 100f;
		float minZ = data.m_position.z - 100f;
		float maxZ = data.m_position.z + 100f;
		Singleton<WaterManager>.get_instance().UpdateGrid(minX, minZ, maxX, maxZ);
	}

	public override void AfterSplitOrMove(ushort nodeID, ref NetNode data, ushort origNodeID1, ushort origNodeID2)
	{
		base.AfterSplitOrMove(nodeID, ref data, origNodeID1, origNodeID2);
		WaterManager.Node[] nodeData = Singleton<WaterManager>.get_instance().m_nodeData;
		if (nodeData[(int)origNodeID1].m_waterPulseGroup != 65535 && nodeData[(int)origNodeID2].m_waterPulseGroup != 65535)
		{
			nodeData[(int)nodeID].m_waterPulseGroup = nodeData[(int)origNodeID1].m_waterPulseGroup;
		}
		else
		{
			nodeData[(int)nodeID].m_waterPulseGroup = 65535;
			if ((data.m_flags & NetNode.Flags.Water) != NetNode.Flags.None)
			{
				data.m_flags |= NetNode.Flags.Transition;
			}
		}
		if (nodeData[(int)origNodeID1].m_sewagePulseGroup != 65535 && nodeData[(int)origNodeID2].m_sewagePulseGroup != 65535)
		{
			nodeData[(int)nodeID].m_sewagePulseGroup = nodeData[(int)origNodeID1].m_sewagePulseGroup;
		}
		else
		{
			nodeData[(int)nodeID].m_sewagePulseGroup = 65535;
			if ((data.m_flags & NetNode.Flags.Sewage) != NetNode.Flags.None)
			{
				data.m_flags |= NetNode.Flags.Transition;
			}
		}
		if (nodeData[(int)origNodeID1].m_heatingPulseGroup != 65535 && nodeData[(int)origNodeID2].m_heatingPulseGroup != 65535)
		{
			nodeData[(int)nodeID].m_heatingPulseGroup = nodeData[(int)origNodeID1].m_heatingPulseGroup;
		}
		else
		{
			nodeData[(int)nodeID].m_heatingPulseGroup = 65535;
			if ((data.m_flags & NetNode.Flags.Heating) != NetNode.Flags.None)
			{
				data.m_flags |= NetNode.Flags.Transition;
			}
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.UpdateNodeColors(nodeID);
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				instance.UpdateSegmentColors(segment);
			}
		}
	}

	public override bool BuildUnderground()
	{
		return true;
	}

	public override bool SupportUnderground()
	{
		return true;
	}

	public override void ConnectionSucceeded(ushort nodeID, ref NetNode data)
	{
		base.ConnectionSucceeded(nodeID, ref data);
		if (this.m_connectionEffect != null)
		{
			InstanceID instance = default(InstanceID);
			instance.NetNode = nodeID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(data.m_position, Vector3.get_up(), this.m_info.m_halfWidth);
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_connectionEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup);
		}
	}

	public override Color32 GetGroupVertexColor(NetInfo.Segment segmentInfo, int vertexIndex)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		Vector3 vector = data.m_vertices[vertexIndex];
		vector.x = vector.x * 0.5f / this.m_info.m_halfWidth + 0.5f;
		vector.z = vector.z / this.m_info.m_segmentLength + 0.5f;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.g = (byte)Mathf.RoundToInt(vector.z * 255f);
		result.a = 255;
		return result;
	}

	public override Color32 GetGroupVertexColor(NetInfo.Node nodeInfo, int vertexIndex, float vOffset)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		Color32 result;
		if (data.m_colors != null && data.m_colors.Length > vertexIndex)
		{
			result = data.m_colors[vertexIndex];
		}
		else
		{
			result..ctor(255, 255, 255, 255);
		}
		result.g = (byte)Mathf.Clamp(Mathf.RoundToInt(vOffset * 255f), 0, 255);
		result.a = 255;
		return result;
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		if (test && this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				GenericGuide upgradeWaterPipes = Singleton<NetManager>.get_instance().m_upgradeWaterPipes;
				if (upgradeWaterPipes != null)
				{
					upgradeWaterPipes.Activate(properties.m_upgradeWaterPipes);
				}
			}
		}
		return result;
	}

	public BuildingInfo m_junctionInfo;

	public EffectInfo m_connectionEffect;
}
