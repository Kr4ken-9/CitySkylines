﻿using System;
using System.Collections;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.PlatformServices;
using ColossalFramework.Plugins;
using ICities;
using PopsApi;
using UnityEngine;

public static class PopsTelemetryEventFormatting
{
	public static string GetDlcName(SteamHelper.DLC dlc)
	{
		if (dlc == SteamHelper.DLC.None || dlc == SteamHelper.DLC.NotAllowed)
		{
			return null;
		}
		if (dlc == SteamHelper.DLC.DeluxeDLC)
		{
			return "deluxe_edition";
		}
		if (dlc == SteamHelper.DLC.AfterDarkDLC)
		{
			return "after_dark";
		}
		if (dlc == SteamHelper.DLC.SnowFallDLC)
		{
			return "snowfall";
		}
		if (dlc == SteamHelper.DLC.NaturalDisastersDLC)
		{
			return "natural_disasters";
		}
		if (dlc == SteamHelper.DLC.InMotionDLC)
		{
			return "mass_transit";
		}
		if (dlc == SteamHelper.DLC.Football)
		{
			return "match_day";
		}
		if (dlc == SteamHelper.DLC.Football2345)
		{
			return "stadiums";
		}
		if (dlc == SteamHelper.DLC.RadioStation1)
		{
			return "relaxation_station";
		}
		if (dlc == SteamHelper.DLC.ModderPack1)
		{
			return "art_deco";
		}
		if (dlc == SteamHelper.DLC.ModderPack2)
		{
			return "high_tech";
		}
		if (dlc == SteamHelper.DLC.OrientalBuildings)
		{
			return "pearls_from_the_east";
		}
		if (dlc == SteamHelper.DLC.RadioStation2)
		{
			return "rock_city";
		}
		if (dlc == SteamHelper.DLC.MusicFestival)
		{
			return "concerts";
		}
		if (dlc == SteamHelper.DLC.GreenCitiesDLC)
		{
			return "green_cities";
		}
		if (dlc == SteamHelper.DLC.ModderPack3)
		{
			return "euro_content_pack";
		}
		return null;
	}

	private static string GetGameplayMode(SimulationManager.UpdateMode mode)
	{
		string result = string.Empty;
		if (mode == SimulationManager.UpdateMode.LoadGame)
		{
			if (string.IsNullOrEmpty(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset))
			{
				result = "sandbox";
			}
			else
			{
				result = "scenario_play";
			}
		}
		else if (!PopsTelemetryEventFormatting.gameplayModeMap.TryGetValue(mode, out result))
		{
			result = "error";
		}
		return result;
	}

	private static SteamHelper.DLC GetDLC(SteamHelper.DLC_BitMask mask)
	{
		IEnumerator enumerator = Enum.GetValues(typeof(SteamHelper.DLC_BitMask)).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)enumerator.Current;
				if ((mask & dLC_BitMask) != (SteamHelper.DLC_BitMask)0)
				{
					SteamHelper.DLC dLC = SteamHelper.DLC.None;
					try
					{
						dLC = (SteamHelper.DLC)Enum.Parse(typeof(SteamHelper.DLC), dLC_BitMask.ToString());
					}
					catch
					{
						dLC = SteamHelper.DLC.None;
					}
					if (dLC != SteamHelper.DLC.None)
					{
						return dLC;
					}
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return SteamHelper.DLC.None;
	}

	private static PopsApiWrapper.TelemetryEntry CreateTelemetryEntry(string eventName, params string[] keyValuePairs)
	{
		PopsApiWrapper.TelemetryEntry result = default(PopsApiWrapper.TelemetryEntry);
		result.eventName = eventName;
		result.keyValuePairs = new List<KeyValuePair<string, string>>();
		int num = keyValuePairs.Length;
		for (int i = 0; i < num; i += 2)
		{
			string text = keyValuePairs[i];
			string text2 = (i + 1 >= num) ? null : keyValuePairs[i + 1];
			if (text == null)
			{
				text = "NULL";
			}
			if (text2 == null)
			{
				text2 = "NULL";
			}
			result.keyValuePairs.Add(new KeyValuePair<string, string>(text, text2));
		}
		return result;
	}

	public static PopsApiWrapper.TelemetryEntry StartGame()
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("start_game", new string[]
		{
			"loadtype",
			"cold"
		});
	}

	public static PopsApiWrapper.TelemetryEntry ExitGame(int sessionLength)
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("exit_game", new string[]
		{
			"exit_type",
			"exit",
			"session_length",
			sessionLength.ToString()
		});
	}

	public static PopsApiWrapper.TelemetryEntry Playthrough(string playthroughId, SimulationManager.UpdateMode mode, string mapId, TimeSpan inGameTimeSpent, bool start)
	{
		string gameplayMode = PopsTelemetryEventFormatting.GetGameplayMode(mode);
		string text = (!start) ? "playthrough_end" : "playthrough_start";
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("playthrough", new string[]
		{
			"playthrough_id",
			playthroughId,
			"gameplay_mode",
			gameplayMode,
			"map_id",
			mapId,
			"ingame_days",
			inGameTimeSpent.Days.ToString(),
			"action",
			text
		});
	}

	public static PopsApiWrapper.TelemetryEntry MilestoneReached(string playthroughId, MilestoneInfo milestone, TimeSpan inGameTimeSpent)
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("milestone_reached", new string[]
		{
			"playthrough_id",
			playthroughId,
			"milestone_id",
			milestone.get_name(),
			"ingame_days",
			inGameTimeSpent.Days.ToString()
		});
	}

	public static PopsApiWrapper.TelemetryEntry UGCManaged(bool enabled, string name)
	{
		string text = (!enabled) ? "disable" : "enable";
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("ugc_managed", new string[]
		{
			"ugc_action",
			text,
			"ugc_name",
			name
		});
	}

	public static PopsApiWrapper.TelemetryEntry GuideMessageReceived(string playthroughId, string guideMessageId)
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("guide_message_received", new string[]
		{
			"playthrough_id",
			playthroughId,
			"guide_message_id",
			guideMessageId
		});
	}

	public static PopsApiWrapper.TelemetryEntry ShortcutUsed(string playthroughId, SimulationManager.UpdateMode mode, string shortcutId)
	{
		string gameplayMode = PopsTelemetryEventFormatting.GetGameplayMode(mode);
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("shortcut_used", new string[]
		{
			"playthrough_id",
			playthroughId,
			"gameplay_mode",
			gameplayMode,
			"shortcut_id",
			shortcutId
		});
	}

	public static PopsApiWrapper.TelemetryEntry BuildingPlaced(string playthroughId, SteamHelper.DLC_BitMask dlc, bool userGenerated, SimulationManager.UpdateMode mode, TimeSpan inGameTimeSpent, string buildingId, float x, float z)
	{
		string text = string.Empty;
		if (userGenerated)
		{
			text = "user_generated";
		}
		else
		{
			text = PopsTelemetryEventFormatting.GetDlcName(PopsTelemetryEventFormatting.GetDLC(dlc));
			if (text == null)
			{
				text = "base_game";
			}
		}
		string gameplayMode = PopsTelemetryEventFormatting.GetGameplayMode(mode);
		int num = (int)((x / 17280f + 0.5f) * 100f);
		int num2 = (int)((z / 17280f + 0.5f) * 100f);
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("building_placed", new string[]
		{
			"playthrough_id",
			playthroughId,
			"building_parent",
			text,
			"gameplay_mode",
			gameplayMode,
			"ingame_days",
			inGameTimeSpent.Days.ToString(),
			"building_id",
			buildingId,
			"x_coord",
			num.ToString(),
			"y_coord",
			num2.ToString()
		});
	}

	public static PopsApiWrapper.TelemetryEntry GameLanguage(string gameLanguage, string osLanguage)
	{
		string text = (!PopsTelemetryEventFormatting.BuiltinLanguages.Contains(gameLanguage)) ? "1" : "0";
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("game_language", new string[]
		{
			"game_language",
			gameLanguage,
			"os_language",
			osLanguage,
			"is_modded",
			text
		});
	}

	public static PopsApiWrapper.TelemetryEntry DLC(SteamHelper.DLC dlc, bool active)
	{
		string text = (!active) ? "0" : "1";
		string text2 = PopsTelemetryEventFormatting.GetDlcName(dlc);
		if (text2 == null)
		{
			text2 = "error";
		}
		string arg_66_0 = "dlc";
		string[] expr_34 = new string[6];
		expr_34[0] = "active";
		expr_34[1] = text;
		expr_34[2] = "dlc_name";
		expr_34[3] = text2;
		expr_34[4] = "dlc_steam_id";
		int arg_65_1 = 5;
		int num = (int)dlc;
		expr_34[arg_65_1] = num.ToString();
		return PopsTelemetryEventFormatting.CreateTelemetryEntry(arg_66_0, expr_34);
	}

	public static PopsApiWrapper.TelemetryEntry HardwareReport()
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("hardware_report", new string[]
		{
			"os_version",
			SystemInfo.get_operatingSystem(),
			"system_mem",
			Mathf.RoundToInt((float)SystemInfo.get_systemMemorySize() / 1000f).ToString(),
			"gfx_device",
			SystemInfo.get_graphicsDeviceName(),
			"gfx_mem",
			Mathf.RoundToInt((float)SystemInfo.get_graphicsMemorySize() / 1000f).ToString(),
			"gfx_driver",
			SystemInfo.get_graphicsDeviceVersion(),
			"cpu_type",
			SystemInfo.get_processorType(),
			"logical_processors",
			SystemInfo.get_processorCount().ToString()
		});
	}

	public static PopsApiWrapper.TelemetryEntry ScenarioStarted(string playthroughId, SteamHelper.DLC dlc, bool userGenerated, TimeSpan inGameTimeSpent, string scenarioId)
	{
		string text = string.Empty;
		if (userGenerated)
		{
			text = "user_generated";
		}
		else
		{
			text = PopsTelemetryEventFormatting.GetDlcName(dlc);
			if (text == null)
			{
				text = "base_game";
			}
		}
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("scenario_started", new string[]
		{
			"playthrough_id",
			playthroughId,
			"scenario_parent",
			text,
			"ingame_days",
			inGameTimeSpent.Days.ToString(),
			"scenario_id",
			scenarioId
		});
	}

	public static PopsApiWrapper.TelemetryEntry ScenarioEnded(string playthroughId, SteamHelper.DLC dlc, TimeSpan inGameTimeSpent, string scenarioId, bool win, string triggerId)
	{
		string text = PopsTelemetryEventFormatting.GetDlcName(dlc);
		if (text == null)
		{
			text = "user_generated";
		}
		string text2 = (!win) ? "lost" : "won";
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("scenario_ended", new string[]
		{
			"playthrough_id",
			playthroughId,
			"scenario_parent",
			text,
			"ingame_days",
			inGameTimeSpent.Days.ToString(),
			"scenario_id",
			scenarioId,
			"reason",
			text2,
			"trigger_id",
			triggerId
		});
	}

	public static PopsApiWrapper.TelemetryEntry RadioChannel(string playThroughId, string channelId, int listenTime)
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("radio_channel", new string[]
		{
			"playthrough_id",
			playThroughId,
			"channel_id",
			channelId,
			"radio_channel_listen_time_seconds",
			listenTime.ToString()
		});
	}

	public static PopsApiWrapper.TelemetryEntry ModUsed(PluginManager.PluginInfo info)
	{
		return PopsTelemetryEventFormatting.CreateTelemetryEntry("mod_used", new string[]
		{
			"mod_workshop_id",
			(!(info.get_publishedFileID() != PublishedFileId.invalid)) ? null : info.get_publishedFileID().ToString(),
			"mod_name",
			PopsTelemetryEventFormatting.GetModName(info)
		});
	}

	public static string GetModName(PluginManager.PluginInfo info)
	{
		if (info != null)
		{
			string result = null;
			try
			{
				IUserMod[] instances = info.GetInstances<IUserMod>();
				if (instances.Length == 1)
				{
					result = instances[0].get_Name();
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		return null;
	}

	private static readonly Dictionary<SimulationManager.UpdateMode, string> gameplayModeMap = new Dictionary<SimulationManager.UpdateMode, string>
	{
		{
			SimulationManager.UpdateMode.NewMap,
			"map_editor"
		},
		{
			SimulationManager.UpdateMode.LoadMap,
			"map_editor"
		},
		{
			SimulationManager.UpdateMode.NewGameFromMap,
			"sandbox"
		},
		{
			SimulationManager.UpdateMode.LoadGame,
			"sandbox"
		},
		{
			SimulationManager.UpdateMode.NewAsset,
			"asset_editor"
		},
		{
			SimulationManager.UpdateMode.LoadAsset,
			"asset_editor"
		},
		{
			SimulationManager.UpdateMode.NewTheme,
			"theme_editor"
		},
		{
			SimulationManager.UpdateMode.LoadTheme,
			"theme_editor"
		},
		{
			SimulationManager.UpdateMode.NewScenarioFromGame,
			"scenario_editor"
		},
		{
			SimulationManager.UpdateMode.NewScenarioFromMap,
			"scenario_editor"
		},
		{
			SimulationManager.UpdateMode.LoadScenario,
			"scenario_editor"
		},
		{
			SimulationManager.UpdateMode.NewGameFromScenario,
			"scenario_play"
		},
		{
			SimulationManager.UpdateMode.UpdateScenarioFromGame,
			"scenario_editor"
		},
		{
			SimulationManager.UpdateMode.UpdateScenarioFromMap,
			"scenario_editor"
		}
	};

	private static readonly List<string> BuiltinLanguages = new List<string>
	{
		"de",
		"en",
		"es",
		"fr",
		"ko",
		"pl",
		"pt",
		"ru",
		"zh"
	};
}
