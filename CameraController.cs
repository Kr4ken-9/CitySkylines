﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraController : MonoBehaviour
{
	public Vector3 targetPosition
	{
		get
		{
			return this.m_targetPosition;
		}
	}

	public bool isTiltShiftDisabled
	{
		get
		{
			return this.m_DOFMode.get_value() != 1 || Mathf.Approximately(this.m_TiltShiftAmount, 0f);
		}
	}

	public bool isDepthOfFieldDisabled
	{
		get
		{
			return (this.m_DOFMode.get_value() != 2 && this.m_DOFMode.get_value() != 3) || Mathf.Approximately(this.m_TiltShiftAmount, 0f);
		}
	}

	public bool isFilmGrainDisabled
	{
		get
		{
			return Mathf.Approximately(this.m_FilmGrainAmount, 0f);
		}
	}

	private Light mainLight
	{
		get
		{
			if (this.m_Mainlight == null)
			{
				GameObject gameObject = GameObject.FindWithTag("MainLight");
				this.m_Mainlight = gameObject.GetComponent<Light>();
			}
			return this.m_Mainlight;
		}
	}

	private void Awake()
	{
		this.m_SMAA = base.GetComponent<SMAA>();
		this.m_TiltShift = base.GetComponent<TiltShiftEffect>();
		if (this.m_TiltShift != null)
		{
			this.m_DefaultMaxBlurSize = this.m_TiltShift.m_MaxBlurSize;
		}
		this.m_DepthOfField = base.GetComponent<DepthOfField>();
		this.m_FilmGrain = base.GetComponent<FilmGrainEffect>();
		this.m_camera = base.GetComponent<Camera>();
		this.m_camera.set_eventMask(0);
		this.m_overrideTimer = 1f;
		this.m_originalPosition = this.m_targetPosition;
		this.m_originalSize = this.m_targetSize;
		this.m_originalAngle = this.m_targetAngle;
		this.m_originalHeight = this.m_targetHeight;
		this.m_originalNearPlane = this.m_camera.get_nearClipPlane();
		this.m_originalFarPlane = this.m_camera.get_farClipPlane();
		Camera expr_CD = this.m_camera;
		expr_CD.set_depthTextureMode(expr_CD.get_depthTextureMode() | 1);
		this.m_edgeScrolling = new SavedBool(Settings.edgeScrolling, Settings.gameSettingsFile, DefaultSettings.edgeScrolling, true);
		this.m_invertYMouse = new SavedBool(Settings.invertYMouse, Settings.gameSettingsFile, DefaultSettings.invertYMouse, true);
		this.m_mouseWheelZoom = new SavedBool(Settings.mouseWheelZoom, Settings.gameSettingsFile, DefaultSettings.mouseWheelZoom, true);
		this.m_mouseSensitivity = new SavedFloat(Settings.mouseSensitivity, Settings.gameSettingsFile, DefaultSettings.mouseSensitivity, true);
		this.m_edgeScrollSensitivity = new SavedFloat(Settings.edgeScrollSensitivity, Settings.gameSettingsFile, DefaultSettings.edgeScrollSensitivity, true);
		this.m_cameraMoveLeft = new SavedInputKey(Settings.cameraMoveLeft, Settings.gameSettingsFile, DefaultSettings.cameraMoveLeft, true);
		this.m_cameraMoveRight = new SavedInputKey(Settings.cameraMoveRight, Settings.gameSettingsFile, DefaultSettings.cameraMoveRight, true);
		this.m_cameraMoveForward = new SavedInputKey(Settings.cameraMoveForward, Settings.gameSettingsFile, DefaultSettings.cameraMoveForward, true);
		this.m_cameraMoveBackward = new SavedInputKey(Settings.cameraMoveBackward, Settings.gameSettingsFile, DefaultSettings.cameraMoveBackward, true);
		this.m_cameraMouseRotate = new SavedInputKey(Settings.cameraMouseRotate, Settings.gameSettingsFile, DefaultSettings.cameraMouseRotate, true);
		this.m_cameraRotateLeft = new SavedInputKey(Settings.cameraRotateLeft, Settings.gameSettingsFile, DefaultSettings.cameraRotateLeft, true);
		this.m_cameraRotateRight = new SavedInputKey(Settings.cameraRotateRight, Settings.gameSettingsFile, DefaultSettings.cameraRotateRight, true);
		this.m_cameraRotateUp = new SavedInputKey(Settings.cameraRotateUp, Settings.gameSettingsFile, DefaultSettings.cameraRotateUp, true);
		this.m_cameraRotateDown = new SavedInputKey(Settings.cameraRotateDown, Settings.gameSettingsFile, DefaultSettings.cameraRotateDown, true);
		this.m_cameraZoomCloser = new SavedInputKey(Settings.cameraZoomCloser, Settings.gameSettingsFile, DefaultSettings.cameraZoomCloser, true);
		this.m_cameraZoomAway = new SavedInputKey(Settings.cameraZoomAway, Settings.gameSettingsFile, DefaultSettings.cameraZoomAway, true);
		this.m_Antialiasing = new SavedInt(Settings.antialiasing, Settings.gameSettingsFile, DefaultSettings.antialiasing, true);
		this.SetViewMode(CameraController.ViewMode.Normal);
		Singleton<InstanceManager>.get_instance().m_instanceChanged += new InstanceManager.ChangeAction(this.ChangeTarget);
	}

	private void OnDestroy()
	{
		Singleton<InstanceManager>.get_instance().m_instanceChanged -= new InstanceManager.ChangeAction(this.ChangeTarget);
	}

	public void SetViewMode(CameraController.ViewMode viewmode)
	{
		if (viewmode != this.m_viewmode)
		{
			if (viewmode == CameraController.ViewMode.Normal)
			{
				for (int i = 0; i < this.m_InfoPostChain.Length; i++)
				{
					this.m_InfoPostChain[i].set_enabled(false);
				}
				for (int j = 0; j < this.m_NormalPostChain.Length; j++)
				{
					this.m_NormalPostChain[j].set_enabled(true);
				}
				Camera expr_67 = this.m_camera;
				expr_67.set_cullingMask(expr_67.get_cullingMask() | 1 << Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer);
			}
			else if (viewmode == CameraController.ViewMode.Info)
			{
				for (int k = 0; k < this.m_NormalPostChain.Length; k++)
				{
					this.m_NormalPostChain[k].set_enabled(false);
				}
				for (int l = 0; l < this.m_InfoPostChain.Length; l++)
				{
					this.m_InfoPostChain[l].set_enabled(true);
				}
				Camera expr_E7 = this.m_camera;
				expr_E7.set_cullingMask(expr_E7.get_cullingMask() & ~(1 << Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer));
			}
			if (this.isTiltShiftDisabled && this.m_TiltShift != null)
			{
				this.m_TiltShift.set_enabled(false);
			}
			if (this.isDepthOfFieldDisabled && this.m_DepthOfField != null)
			{
				this.m_DepthOfField.set_enabled(false);
			}
			if (this.isFilmGrainDisabled && this.m_FilmGrain != null)
			{
				this.m_FilmGrain.set_enabled(false);
			}
			this.m_viewmode = viewmode;
		}
	}

	public void SetOverrideModeOn(Vector3 pos, Vector2 angle, float zoom)
	{
		if (!this.m_overrideMode)
		{
			this.m_overrideTimer = 1f - this.m_overrideTimer;
		}
		this.m_overrideMode = true;
		this.m_overridePos = pos;
		this.m_overrideAngle = angle;
		this.m_overrideZoom = zoom;
	}

	public void SetOverrideModeOff()
	{
		if (this.m_overrideMode)
		{
			this.m_overrideTimer = 1f - this.m_overrideTimer;
		}
		this.m_overrideMode = false;
	}

	public void Reset(Vector3 startOffset)
	{
		this.m_targetPosition = this.m_originalPosition + startOffset;
		this.m_currentPosition = this.m_originalPosition + startOffset;
		this.m_targetSize = this.m_originalSize;
		this.m_currentSize = this.m_originalSize;
		this.m_targetAngle = this.m_originalAngle;
		this.m_currentAngle = this.m_originalAngle;
		this.m_targetHeight = this.m_originalHeight;
		this.m_currentHeight = this.m_originalHeight;
		this.ClearTarget();
	}

	public void ClearTarget()
	{
		if (this.CanClearTarget())
		{
			this.SetTarget(InstanceID.Empty, this.m_assignedTargetPosition, true);
		}
	}

	private bool CanClearTarget()
	{
		return this.m_targetInstance.Disaster == 0 || !Singleton<DisasterManager>.get_exists() || !Singleton<DisasterManager>.get_instance().IsFollowing(this.m_targetInstance.Disaster);
	}

	public void SetTarget(InstanceID id, Vector3 position, bool zoomIn)
	{
		if (id != this.m_targetInstance)
		{
			if (Singleton<InstanceManager>.get_instance().FollowInstance(id))
			{
				this.m_targetInstance = id;
				this.m_assignedTargetPosition = position;
				this.m_targetTimer = 0f;
				this.m_cachedPosition = this.m_targetPosition;
				this.m_cachedAngle = this.m_targetAngle;
				this.m_cachedSize = this.m_targetSize;
				this.m_cachedHeight = this.m_targetHeight;
				this.FollowTarget(zoomIn);
				this.m_previousTargetRotation = this.m_currentTargetRotation;
				this.m_previousAngleOffset = Vector2.get_zero();
				if (!this.m_unlimitedCamera)
				{
					Vector3 targetPosition = this.m_targetPosition;
					Singleton<GameAreaManager>.get_instance().ClampPoint(ref this.m_targetPosition);
					if (this.m_targetPosition != targetPosition && this.CanClearTarget())
					{
						this.m_targetInstance = InstanceID.Empty;
						this.m_targetPosition = this.m_cachedPosition;
						this.m_targetAngle = this.m_cachedAngle;
						this.m_targetSize = this.m_cachedSize;
						this.m_targetHeight = this.m_cachedHeight;
					}
				}
			}
			else if (!this.m_targetInstance.IsEmpty)
			{
				this.m_targetInstance = InstanceID.Empty;
			}
		}
	}

	public void ChangeTarget(InstanceID oldID, InstanceID newID)
	{
		if (!this.m_targetInstance.IsEmpty && this.m_targetInstance == oldID)
		{
			this.SetTarget(newID, this.m_assignedTargetPosition, false);
		}
	}

	public InstanceID GetTarget()
	{
		return this.m_targetInstance;
	}

	public bool HasTarget(InstanceID id)
	{
		return this.m_targetInstance == id;
	}

	private bool FollowTarget(bool zoomIn)
	{
		if (!this.m_targetInstance.IsEmpty)
		{
			InstanceID location = InstanceManager.GetLocation(this.m_targetInstance);
			switch (location.Type)
			{
			case InstanceType.Building:
			case InstanceType.District:
			case InstanceType.NetNode:
			case InstanceType.NetSegment:
			case InstanceType.Disaster:
			{
				Vector3 targetPosition;
				Quaternion quaternion;
				Vector3 vector;
				if (InstanceManager.GetPosition(location, out targetPosition, out quaternion, out vector))
				{
					this.m_targetPosition = targetPosition;
					if (zoomIn)
					{
						this.m_targetAngle.y = 45f;
						this.m_targetSize = 40f + vector.get_magnitude();
					}
					return true;
				}
				return false;
			}
			case InstanceType.Vehicle:
			case InstanceType.ParkedVehicle:
			case InstanceType.CitizenInstance:
			{
				Vector3 targetPosition2;
				Quaternion currentTargetRotation;
				Vector3 vector2;
				if (InstanceManager.GetPosition(location, out targetPosition2, out currentTargetRotation, out vector2))
				{
					this.m_targetPosition = targetPosition2;
					this.m_currentTargetRotation = currentTargetRotation;
					if (zoomIn)
					{
						this.m_targetAngle.y = 45f;
						this.m_targetSize = 40f + vector2.get_magnitude();
					}
					return true;
				}
				return false;
			}
			case InstanceType.TransportLine:
				this.m_targetPosition = this.m_assignedTargetPosition;
				if (zoomIn)
				{
					this.m_targetAngle.y = 45f;
					this.m_targetSize = 100f;
				}
				return true;
			}
		}
		return false;
	}

	private void HandleMouseEvents(float multiplier)
	{
		Vector2 zero = Vector2.get_zero();
		if (this.m_cameraMouseRotate.IsPressed() || SteamController.GetDigitalAction(SteamController.DigitalInput.RotateMouse))
		{
			if (this.m_invertYMouse)
			{
				zero..ctor(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
			}
			else
			{
				zero..ctor(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
			}
			if (zero.get_magnitude() > 0.05f && Cursor.get_visible())
			{
				Cursor.set_visible(false);
				Cursor.set_lockState(1);
			}
		}
		else if (!Cursor.get_visible())
		{
			Cursor.set_visible(true);
			Cursor.set_lockState(0);
		}
		this.m_angleVelocity += zero * (12f * this.m_mouseSensitivity * multiplier);
		if (this.m_analogController)
		{
			float axis = Input.GetAxis("RotationHorizontalCamera");
			float num = Input.GetAxis("RotationVerticalCamera");
			if (this.m_invertYMouse)
			{
				num = -num;
			}
			if (axis != 0f || num != 0f)
			{
				Vector2 vector;
				vector..ctor(axis * this.m_GamepadAngleVelocityScalar.x, num * this.m_GamepadAngleVelocityScalar.y);
				this.m_angleVelocity += vector * multiplier;
			}
		}
		if (this.m_edgeScrolling)
		{
			Vector3 mousePosition = Input.get_mousePosition();
			float num2 = (float)Screen.get_width();
			float num3 = (float)Screen.get_height();
			float num4 = 10f;
			float num5 = 0f;
			if (mousePosition.x < num4)
			{
				num5 = Mathf.Clamp(1f - mousePosition.x / num4, num5, 1f);
			}
			if (mousePosition.x > num2 - num4)
			{
				num5 = Mathf.Clamp(1f - (num2 - mousePosition.x) / num4, num5, 1f);
			}
			if (mousePosition.y < num4)
			{
				num5 = Mathf.Clamp(1f - mousePosition.y / num4, num5, 1f);
			}
			if (mousePosition.y > num3 - num4)
			{
				num5 = Mathf.Clamp(1f - (num3 - mousePosition.y) / num4, num5, 1f);
			}
			if (num5 != 0f)
			{
				Vector3 vector2;
				vector2..ctor(mousePosition.x - num2 * 0.5f, 0f, mousePosition.y - num3 * 0.5f);
				vector2.Normalize();
				vector2 *= num5 * 2f * this.m_edgeScrollSensitivity;
				this.m_velocity += vector2 * multiplier * Time.get_deltaTime();
				this.ClearTarget();
			}
		}
	}

	private void HandleScrollWheelEvent(float multiplier)
	{
		if (this.m_mouseWheelZoom)
		{
			float axis = Input.GetAxis("Mouse ScrollWheel");
			this.m_zoomVelocity += axis * (-20f * multiplier);
		}
		if (this.m_analogController)
		{
			float axis2 = Input.GetAxis("ZoomCamera");
			if (axis2 != 0f)
			{
				this.m_zoomVelocity += axis2 * multiplier * this.m_GamepadMotionZoomScalar.y;
			}
		}
	}

	private void HandleKeyEvents(float multiplier)
	{
		Vector3 zero = Vector3.get_zero();
		Vector2 analogAction = SteamController.GetAnalogAction(SteamController.AnalogInput.CameraScroll);
		if (analogAction.get_sqrMagnitude() > 0.0001f)
		{
			zero.x = analogAction.x;
			zero.z = analogAction.y;
			this.ClearTarget();
		}
		if (this.m_cameraMoveLeft.IsPressed())
		{
			zero.x -= 1f;
			this.ClearTarget();
		}
		if (this.m_cameraMoveRight.IsPressed())
		{
			zero.x += 1f;
			this.ClearTarget();
		}
		if (this.m_cameraMoveForward.IsPressed())
		{
			zero.z += 1f;
			this.ClearTarget();
		}
		if (this.m_cameraMoveBackward.IsPressed())
		{
			zero.z -= 1f;
			this.ClearTarget();
		}
		if (this.m_analogController)
		{
			float num = Input.GetAxis("Horizontal") * this.m_GamepadMotionZoomScalar.x;
			float num2 = Input.GetAxis("Vertical") * this.m_GamepadMotionZoomScalar.z;
			if (num != 0f)
			{
				zero.x = num;
			}
			if (num2 != 0f)
			{
				zero.z = num2;
			}
		}
		this.m_velocity += zero * multiplier * Time.get_deltaTime();
		Vector2 zero2 = Vector2.get_zero();
		if (this.m_cameraRotateLeft.IsPressed())
		{
			zero2.x += 200f;
		}
		if (this.m_cameraRotateRight.IsPressed())
		{
			zero2.x -= 200f;
		}
		if (this.m_cameraRotateUp.IsPressed())
		{
			zero2.y += 200f;
		}
		if (this.m_cameraRotateDown.IsPressed())
		{
			zero2.y -= 200f;
		}
		this.m_angleVelocity += zero2 * multiplier * Time.get_deltaTime();
		float num3 = 0f;
		if (this.m_cameraZoomCloser.IsPressed() || SteamController.GetDigitalAction(SteamController.DigitalInput.ZoomIn))
		{
			num3 -= 50f;
		}
		if (this.m_cameraZoomAway.IsPressed() || SteamController.GetDigitalAction(SteamController.DigitalInput.ZoomOut))
		{
			num3 += 50f;
		}
		this.m_zoomVelocity += num3 * multiplier * Time.get_deltaTime();
	}

	private void LateUpdate()
	{
		if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			return;
		}
		this.UpdateFreeCamera();
		this.UpdateTargetPosition();
		this.UpdateCurrentPosition();
		this.UpdateTransform();
		if (this.mainLight != null)
		{
			this.mainLight.set_shadows((this.m_ShadowsQuality.get_value() == 0) ? 0 : 2);
		}
		if (this.m_SMAA != null)
		{
			this.m_SMAA.set_enabled(this.m_Antialiasing.get_value() != 0);
		}
		if (this.m_TiltShift != null)
		{
			this.m_TiltShift.set_enabled(!this.isTiltShiftDisabled);
			this.m_TiltShift.m_BlurArea = Mathf.Lerp(this.m_MaxTiltShiftArea, this.m_MinTiltShiftArea, Mathf.Clamp((this.m_currentSize - this.m_minDistance) / this.m_MaxTiltShiftDistance, 0f, 1f)) * this.m_TiltShiftAmount;
			this.m_TiltShift.m_MaxBlurSize = this.m_DefaultMaxBlurSize * this.m_TiltShiftAmount;
		}
		if (this.m_DepthOfField != null)
		{
			this.m_DepthOfField.set_enabled(!this.isDepthOfFieldDisabled);
			if (this.m_DOFMode == 2)
			{
				this.m_DepthOfField.blurType = 0;
			}
			else if (this.m_DOFMode == 3)
			{
				this.m_DepthOfField.blurType = 1;
			}
			float num = Mathf.Clamp((this.m_currentSize - this.m_minDistance) / this.m_MaxTiltShiftDistance, 0f, 1f);
			this.m_DepthOfField.focalLength = this.m_currentSize + this.m_FocalLength.Evaluate(num);
			this.m_DepthOfField.focalSize = this.m_FocalSize.Evaluate(num);
			this.m_DepthOfField.aperture = this.m_Aperture.Evaluate(num);
			this.m_DepthOfField.maxBlurSize = this.m_MaxBlurSize.Evaluate(num) * this.m_TiltShiftAmount;
		}
		if (this.m_FilmGrain != null)
		{
			this.m_FilmGrain.set_enabled(!this.isFilmGrainDisabled);
			this.m_FilmGrain.m_Amount = Mathf.Lerp(this.m_FilmGrainAmountMin, this.m_FilmGrainAmountMax, this.m_FilmGrainAmount);
			this.m_FilmGrain.m_MiddleRange = Mathf.Lerp(this.m_FilmGrainMiddleMax, this.m_FilmGrainMiddleMin, this.m_FilmGrainAmount);
		}
		this.m_cameraShake = Vector3.get_zero();
	}

	private void UpdateFreeCamera()
	{
		if (this.m_freeCamera != this.m_cachedFreeCamera)
		{
			this.m_cachedFreeCamera = this.m_freeCamera;
			UIView.Show(UIView.HasModalInput() || !this.m_freeCamera);
			Singleton<NotificationManager>.get_instance().NotificationsVisible = !this.m_freeCamera;
			Singleton<GameAreaManager>.get_instance().BordersVisible = !this.m_freeCamera;
			Singleton<DistrictManager>.get_instance().NamesVisible = !this.m_freeCamera;
			Singleton<PropManager>.get_instance().MarkersVisible = !this.m_freeCamera;
			Singleton<GuideManager>.get_instance().TutorialDisabled = this.m_freeCamera;
			Singleton<DisasterManager>.get_instance().MarkersVisible = !this.m_freeCamera;
			Singleton<NetManager>.get_instance().RoadNamesVisible = !this.m_freeCamera;
		}
		if (this.m_cachedFreeCamera)
		{
			this.m_camera.set_rect(new Rect(0f, 0f, 1f, 1f));
		}
		else
		{
			this.m_camera.set_rect(new Rect(0f, 0.105f, 1f, 0.895f));
		}
	}

	private void UpdateTargetPosition()
	{
		float num = Mathf.Max(0.001f, Time.get_deltaTime());
		float num2 = (!this.m_freeCamera) ? this.m_defaultInertia : this.m_freeCameraInertia;
		float num3 = Mathf.Pow(num2, num);
		this.m_velocity *= num3;
		this.m_angleVelocity *= num3;
		this.m_zoomVelocity *= num3;
		if (!UIView.HasModalInput() && !this.m_overrideMode)
		{
			num3 = 1f / num * (1f - num3);
			this.HandleMouseEvents(num3);
			if (!Singleton<ToolManager>.get_instance().m_properties.HasInputFocus)
			{
				this.HandleKeyEvents(num3);
			}
			if (!Singleton<ToolManager>.get_instance().m_properties.IsInsideUI)
			{
				this.HandleScrollWheelEvent(num3);
			}
		}
		Vector2 vector = Vector2.ClampMagnitude(this.m_angleVelocity * num, 360f);
		if (Mathf.Abs(this.m_angleVelocity.x) > 0.001f)
		{
			this.m_targetAngle.x = this.m_targetAngle.x + vector.x;
			if (this.m_targetAngle.x < -180f)
			{
				this.m_targetAngle.x = this.m_targetAngle.x + 360f;
			}
			if (this.m_targetAngle.x > 180f)
			{
				this.m_targetAngle.x = this.m_targetAngle.x - 360f;
			}
			GenericGuide cameraNotRotated = Singleton<GuideManager>.get_instance().m_cameraNotRotated;
			if (cameraNotRotated != null && !cameraNotRotated.m_disabled)
			{
				Singleton<SimulationManager>.get_instance().AddAction(cameraNotRotated.Disable_Action());
			}
		}
		if (Mathf.Abs(this.m_angleVelocity.y) > 0.001f)
		{
			this.m_targetAngle.y = this.m_targetAngle.y + vector.y;
			if (this.m_targetAngle.y > 90f)
			{
				this.m_targetAngle.y = 90f;
			}
			if (this.m_cachedFreeCamera)
			{
				if (this.m_targetAngle.y < -90f)
				{
					this.m_targetAngle.y = -90f;
				}
			}
			else if (this.m_targetAngle.y < 0f)
			{
				this.m_targetAngle.y = 0f;
			}
			GenericGuide cameraNotTilted = Singleton<GuideManager>.get_instance().m_cameraNotTilted;
			if (cameraNotTilted != null && !cameraNotTilted.m_disabled)
			{
				Singleton<SimulationManager>.get_instance().AddAction(cameraNotTilted.Disable_Action());
			}
		}
		float num4 = this.m_targetSize;
		if (Mathf.Abs(this.m_zoomVelocity) > 0.001f)
		{
			if (this.m_zoomVelocity < 0f)
			{
				num4 /= Mathf.Pow(1.1f, -this.m_zoomVelocity * num);
			}
			else
			{
				num4 *= Mathf.Pow(1.1f, this.m_zoomVelocity * num);
			}
			GenericGuide cameraNotZoomed = Singleton<GuideManager>.get_instance().m_cameraNotZoomed;
			if (cameraNotZoomed != null && !cameraNotZoomed.m_disabled)
			{
				Singleton<SimulationManager>.get_instance().AddAction(cameraNotZoomed.Disable_Action());
			}
		}
		this.m_targetSize = Mathf.Clamp(num4, this.m_minDistance, this.m_maxDistance);
		bool flag = false;
		if (!this.m_targetInstance.IsEmpty)
		{
			if (!InstanceManager.IsValid(this.m_targetInstance))
			{
				this.ClearTarget();
			}
			else
			{
				flag = this.FollowTarget(false);
			}
		}
		if (flag)
		{
			Vector3 vector2 = this.m_previousTargetRotation * Vector3.get_forward();
			Vector3 vector3 = this.m_currentTargetRotation * Vector3.get_forward();
			float num5 = Mathf.Clamp01(1.1f - this.m_targetSize * 0.003f);
			if (num5 > 0.001f)
			{
				num5 = 3f * num5 * num5 - 2f * num5 * num5 * num5;
				Vector3 vector4 = VectorUtils.NormalizeXZ(vector2);
				Vector3 vector5 = VectorUtils.NormalizeXZ(vector3);
				float num6 = 57.29578f * Mathf.Acos(Mathf.Clamp(vector4.x * vector5.x + vector4.z * vector5.z, -1f, 1f));
				if (vector4.x * vector5.z - vector4.z * vector5.x > 0f)
				{
					num6 = -num6;
				}
				this.m_targetAngle.x = this.m_targetAngle.x + (num6 - this.m_previousAngleOffset.x) * num5;
				this.m_previousAngleOffset.x = num6;
				if (this.m_targetAngle.x < -180f)
				{
					this.m_targetAngle.x = this.m_targetAngle.x + 360f;
				}
				if (this.m_targetAngle.x > 180f)
				{
					this.m_targetAngle.x = this.m_targetAngle.x - 360f;
				}
				num5 *= Mathf.Cos((num6 - this.m_targetAngle.x - 90f) * 0.0174532924f);
				num6 = (Mathf.Asin(vector2.y) - Mathf.Asin(vector3.y)) * 57.29578f;
				this.m_targetAngle.y = this.m_targetAngle.y + (num6 - this.m_previousAngleOffset.y) * num5;
				this.m_previousAngleOffset.y = num6;
				if (this.m_targetAngle.y > 90f)
				{
					this.m_targetAngle.y = 90f;
				}
				if (this.m_cachedFreeCamera)
				{
					if (this.m_targetAngle.y < -90f)
					{
						this.m_targetAngle.y = -90f;
					}
				}
				else if (this.m_targetAngle.y < 0f)
				{
					this.m_targetAngle.y = 0f;
				}
			}
			if (!this.m_unlimitedCamera)
			{
				Vector3 targetPosition = this.m_targetPosition;
				Singleton<GameAreaManager>.get_instance().ClampPoint(ref this.m_targetPosition);
				if (this.m_targetPosition != targetPosition)
				{
					this.ClearTarget();
				}
			}
		}
		else if (Vector3.SqrMagnitude(this.m_velocity) > 0.001f)
		{
			Vector3 vector6 = this.m_velocity;
			Vector3 right = base.get_transform().get_right();
			Quaternion quaternion = Quaternion.FromToRotation(Vector3.get_right(), right);
			vector6 = quaternion * vector6;
			vector6.y = 0f;
			this.m_targetPosition += vector6.get_normalized() * (this.m_velocity.get_magnitude() * num4 * num * 1.5f);
			GenericGuide cameraNotMoved = Singleton<GuideManager>.get_instance().m_cameraNotMoved;
			if (cameraNotMoved != null && !cameraNotMoved.m_disabled)
			{
				Singleton<SimulationManager>.get_instance().AddAction(cameraNotMoved.Disable_Action());
			}
			if (!this.m_unlimitedCamera)
			{
				Singleton<GameAreaManager>.get_instance().ClampPoint(ref this.m_targetPosition);
			}
		}
		float num7 = 0f;
		Vector3 vector7 = Vector3.get_zero();
		for (int i = 0; i < 3; i++)
		{
			if (flag)
			{
				this.m_targetHeight = this.m_targetPosition.y;
			}
			else
			{
				this.m_targetHeight = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(this.m_targetPosition, true, 2f);
			}
			this.m_targetPosition.y = this.m_targetHeight + this.m_targetSize * 0.05f + 10f;
			num7 = this.m_targetSize * (1f - this.m_targetHeight / this.m_maxDistance) / Mathf.Tan(0.0174532924f * this.m_camera.get_fieldOfView());
			float num8;
			if (this.m_cachedFreeCamera)
			{
				num8 = this.m_targetAngle.y;
			}
			else
			{
				num8 = 90f - (90f - this.m_targetAngle.y) * (this.m_maxTiltDistance * 0.5f / (this.m_maxTiltDistance * 0.5f + this.m_targetSize));
			}
			Quaternion quaternion2 = Quaternion.AngleAxis(this.m_targetAngle.x, Vector3.get_up()) * Quaternion.AngleAxis(num8, Vector3.get_right());
			Vector3 vector8 = this.m_targetPosition + quaternion2 * new Vector3(0f, 0f, -num7);
			vector7 = this.ClampCameraPosition(vector8);
			Vector3 vector9 = vector7 - vector8;
			this.m_targetPosition += vector9;
			if (Vector3.SqrMagnitude(vector9) < 0.0001f)
			{
				break;
			}
		}
		this.m_targetPosition.y = this.m_targetPosition.y + this.CalculateCameraHeightOffset(vector7, num7);
		this.m_targetPosition = this.ClampCameraPosition(this.m_targetPosition);
	}

	private void UpdateCurrentPosition()
	{
		float num;
		if (this.m_cachedFreeCamera)
		{
			num = this.m_targetAngle.y;
		}
		else
		{
			num = 90f - (90f - this.m_targetAngle.y) * (this.m_maxTiltDistance * 0.5f / (this.m_maxTiltDistance * 0.5f + this.m_targetSize));
		}
		if (this.m_overrideMode)
		{
			if (this.m_overrideTimer != 1f)
			{
				this.m_overrideTimer = Mathf.Min(1f, this.m_overrideTimer + Time.get_deltaTime() * 2f);
				float num2 = this.m_overrideTimer;
				num2 = 3f * num2 * num2 - 2f * num2 * num2 * num2;
				this.m_currentPosition = Vector3.Lerp(this.m_targetPosition, this.m_overridePos, num2);
				this.m_currentSize = Mathf.Lerp(this.m_targetSize, this.m_overrideZoom, num2);
				this.m_currentAngle.x = Mathf.LerpAngle(this.m_targetAngle.x, this.m_overrideAngle.x, num2);
				this.m_currentAngle.y = Mathf.Lerp(num, this.m_overrideAngle.y, num2);
				this.m_currentHeight = Mathf.Lerp(this.m_targetHeight, 0f, num2);
			}
			else
			{
				float num3 = Mathf.Pow(0.01f, Time.get_deltaTime());
				this.m_currentPosition = Vector3.Lerp(this.m_overridePos, this.m_currentPosition, num3);
				this.m_currentSize = Mathf.Lerp(this.m_overrideZoom, this.m_currentSize, num3);
				this.m_currentAngle.x = Mathf.LerpAngle(this.m_overrideAngle.x, this.m_currentAngle.x, num3);
				this.m_currentAngle.y = Mathf.Lerp(this.m_overrideAngle.y, this.m_currentAngle.y, num3);
				this.m_currentHeight = Mathf.Lerp(0f, this.m_currentHeight, num3);
			}
		}
		else if (this.m_overrideTimer != 1f)
		{
			this.m_overrideTimer = Mathf.Min(1f, this.m_overrideTimer + Time.get_deltaTime() * 2f);
			float num4 = this.m_overrideTimer;
			num4 = 3f * num4 * num4 - 2f * num4 * num4 * num4;
			this.m_currentPosition = Vector3.Lerp(this.m_overridePos, this.m_targetPosition, num4);
			this.m_currentSize = Mathf.Lerp(this.m_overrideZoom, this.m_targetSize, num4);
			this.m_currentAngle.x = Mathf.LerpAngle(this.m_overrideAngle.x, this.m_targetAngle.x, num4);
			this.m_currentAngle.y = Mathf.Lerp(this.m_overrideAngle.y, num, num4);
			this.m_currentHeight = Mathf.Lerp(0f, this.m_targetHeight, num4);
		}
		else if (!this.m_targetInstance.IsEmpty)
		{
			if (this.m_targetTimer != 1f)
			{
				float num5;
				if (this.m_cachedFreeCamera)
				{
					num5 = this.m_cachedAngle.y;
				}
				else
				{
					num5 = 90f - (90f - this.m_cachedAngle.y) * (this.m_maxTiltDistance * 0.5f / (this.m_maxTiltDistance * 0.5f + this.m_cachedSize));
				}
				this.m_targetTimer = Mathf.Min(1f, this.m_targetTimer + Time.get_deltaTime() * 2f);
				float num6 = this.m_targetTimer;
				num6 = 3f * num6 * num6 - 2f * num6 * num6 * num6;
				this.m_currentPosition = Vector3.Lerp(this.m_cachedPosition, this.m_targetPosition, num6);
				this.m_currentSize = Mathf.Lerp(this.m_cachedSize, this.m_targetSize, num6);
				this.m_currentAngle.x = Mathf.LerpAngle(this.m_cachedAngle.x, this.m_targetAngle.x, num6);
				this.m_currentAngle.y = Mathf.Lerp(num5, num, num6);
				this.m_currentHeight = Mathf.Lerp(this.m_cachedHeight, this.m_targetHeight, num6);
			}
			else
			{
				float num7 = Mathf.Pow(1E-08f, Time.get_deltaTime());
				float num8 = this.m_currentPosition.y - this.m_currentHeight;
				float num9 = this.m_targetPosition.y - this.m_targetHeight;
				num8 = Mathf.Lerp(num9, num8, num7);
				this.m_currentPosition = this.m_targetPosition;
				this.m_currentPosition.y = this.m_targetHeight + num8;
				this.m_currentSize = Mathf.Lerp(this.m_targetSize, this.m_currentSize, num7);
				this.m_currentAngle.x = Mathf.LerpAngle(this.m_targetAngle.x, this.m_currentAngle.x, num7);
				this.m_currentAngle.y = Mathf.Lerp(num, this.m_currentAngle.y, num7);
				this.m_currentHeight = this.m_targetHeight;
			}
		}
		else
		{
			float num10 = Mathf.Pow(1E-08f, Time.get_deltaTime());
			this.m_currentPosition = Vector3.Lerp(this.m_targetPosition, this.m_currentPosition, num10);
			this.m_currentSize = Mathf.Lerp(this.m_targetSize, this.m_currentSize, num10);
			this.m_currentAngle.x = Mathf.LerpAngle(this.m_targetAngle.x, this.m_currentAngle.x, num10);
			this.m_currentAngle.y = Mathf.Lerp(num, this.m_currentAngle.y, num10);
			this.m_currentHeight = Mathf.Lerp(this.m_targetHeight, this.m_currentHeight, num10);
		}
	}

	private void UpdateTransform()
	{
		this.m_camera.set_nearClipPlane(Mathf.Max(this.m_originalNearPlane, this.m_currentSize * 0.005f));
		this.m_camera.set_farClipPlane(Mathf.Max(this.m_originalFarPlane, this.m_currentSize * 2f));
		float num = this.m_currentSize * (1f - this.m_currentHeight / this.m_maxDistance) / Mathf.Tan(0.0174532924f * this.m_camera.get_fieldOfView());
		Quaternion quaternion = Quaternion.AngleAxis(this.m_currentAngle.x, Vector3.get_up()) * Quaternion.AngleAxis(this.m_currentAngle.y, Vector3.get_right());
		Vector3 vector = this.m_currentPosition + quaternion * new Vector3(0f, 0f, -num);
		vector.y += this.CalculateCameraHeightOffset(vector, num);
		vector = this.ClampCameraPosition(vector);
		vector += this.m_cameraShake * Mathf.Sqrt(num);
		base.get_transform().set_rotation(quaternion);
		base.get_transform().set_position(vector);
		float num2 = Mathf.Clamp01(num / this.m_maxDistance * 5f / 6f);
		float num3 = 1f;
		if (this.m_ShadowsDistance.get_value() == 0)
		{
			num3 = 0.7f;
		}
		else if (this.m_ShadowsDistance.get_value() == 1)
		{
			num3 = 1f;
		}
		else if (this.m_ShadowsDistance.get_value() == 2)
		{
			num3 = 2f;
		}
		else if (this.m_ShadowsDistance.get_value() == 3)
		{
			num3 = 4f;
		}
		num2 = Mathf.Lerp(this.m_minShadowDistance * num3, this.m_maxShadowDistance * num3, num2);
		QualitySettings.set_shadowDistance(num2);
		Singleton<RenderManager>.get_instance().CameraHeight = Mathf.Max(num * Mathf.Sin(this.m_currentAngle.y * 0.0174532924f), num * 0.168749988f);
		if (this.m_ShadowsQuality.get_value() == 0)
		{
			Singleton<RenderManager>.get_instance().ShadowDistance = 0f;
		}
		else
		{
			Singleton<RenderManager>.get_instance().ShadowDistance = 200f;
		}
	}

	private float CalculateCameraHeightOffset(Vector3 worldPos, float distance)
	{
		float num = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, true, 2f);
		float num2 = num - worldPos.y;
		distance *= 0.45f;
		num2 = Mathf.Max(num2, -distance);
		num2 += distance * 0.375f * Mathf.Pow(1f + 1f / distance, -num2);
		num = worldPos.y + Mathf.Max(0f, num2);
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.AssetEditor) == ItemClass.Availability.None)
		{
			ItemClass.Layer layer = ItemClass.Layer.Default;
			if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
			{
				layer |= ItemClass.Layer.Markers;
			}
			worldPos.y -= 5f;
			num = Mathf.Max(num, Singleton<BuildingManager>.get_instance().SampleSmoothHeight(worldPos, layer) + 5f);
			num = Mathf.Max(num, Singleton<NetManager>.get_instance().SampleSmoothHeight(worldPos) + 5f);
			num = Mathf.Max(num, Singleton<PropManager>.get_instance().SampleSmoothHeight(worldPos) + 5f);
			num = Mathf.Max(num, Singleton<TreeManager>.get_instance().SampleSmoothHeight(worldPos) + 5f);
			worldPos.y += 5f;
		}
		return num - worldPos.y;
	}

	private Vector3 ClampCameraPosition(Vector3 position)
	{
		float num = 8640f;
		if (position.x < -num)
		{
			position.x = -num;
		}
		if (position.x > num)
		{
			position.x = num;
		}
		if (position.z < -num)
		{
			position.z = -num;
		}
		if (position.z > num)
		{
			position.z = num;
		}
		return position;
	}

	public bool DiscardEvents()
	{
		if (Input.GetKey(9))
		{
			Event current = Event.get_current();
			if (current.get_isMouse() || current.get_isKey())
			{
				current.Use();
			}
			return true;
		}
		return false;
	}

	public Vector2 m_GamepadAngleVelocityScalar = new Vector2(3f, 3f);

	public Vector3 m_GamepadMotionZoomScalar = new Vector3(1f, 0.5f, 1f);

	public float m_minDistance = 40f;

	public float m_maxDistance = 3000f;

	public float m_maxTiltDistance = 5000f;

	public float m_minShadowDistance = 400f;

	public float m_maxShadowDistance = 4000f;

	public bool m_freeCamera;

	public bool m_unlimitedCamera;

	public bool m_analogController;

	public float m_defaultInertia;

	public float m_freeCameraInertia = 0.01f;

	public Vector3 m_targetPosition = new Vector3(450f, 110f, -610f);

	public Vector3 m_currentPosition = new Vector3(450f, 110f, -610f);

	private Vector3 m_originalPosition;

	public float m_targetSize = 780f;

	public float m_currentSize = 780f;

	private float m_originalSize;

	public Vector2 m_targetAngle = new Vector2(-28f, 20f);

	public Vector2 m_currentAngle = new Vector2(-28f, 20f);

	private Vector2 m_originalAngle;

	public float m_targetHeight = 60f;

	public float m_currentHeight = 60f;

	private float m_originalHeight;

	public Vector3 m_cameraShake;

	public ColorCorrectionLut m_MainLUT;

	public MonoBehaviour[] m_NormalPostChain;

	public MonoBehaviour[] m_InfoPostChain;

	[Header("Depth of field controls")]
	public AnimationCurve m_FocalLength = AnimationCurve.Linear(0f, 10f, 1f, 10f);

	public AnimationCurve m_FocalSize = AnimationCurve.Linear(0f, 0.05f, 1f, 0.05f);

	public AnimationCurve m_Aperture = AnimationCurve.Linear(0f, 11.5f, 1f, 11.5f);

	public AnimationCurve m_MaxBlurSize = AnimationCurve.Linear(0f, 2f, 1f, 2f);

	[Header("Tilt shift controls")]
	public float m_MaxTiltShiftDistance = 2000f;

	public float m_MaxTiltShiftArea = 5.55f;

	public float m_MinTiltShiftArea;

	[Header("Film grain controls")]
	public float m_FilmGrainAmountMax = 0.2f;

	public float m_FilmGrainAmountMin;

	public float m_FilmGrainMiddleMax = 0.35f;

	public float m_FilmGrainMiddleMin = 0.24f;

	private float m_DefaultMaxBlurSize;

	private TiltShiftEffect m_TiltShift;

	private DepthOfField m_DepthOfField;

	private FilmGrainEffect m_FilmGrain;

	private SMAA m_SMAA;

	private Light m_Mainlight;

	private Vector3 m_velocity;

	private Vector2 m_angleVelocity;

	private float m_zoomVelocity;

	private bool m_overrideMode;

	private Vector3 m_overridePos;

	private Vector2 m_overrideAngle;

	private float m_overrideZoom;

	private float m_overrideTimer;

	private Camera m_camera;

	private bool m_cachedFreeCamera;

	private float m_originalNearPlane;

	private float m_originalFarPlane;

	private CameraController.ViewMode m_viewmode;

	private InstanceID m_targetInstance;

	private Quaternion m_previousTargetRotation;

	private Quaternion m_currentTargetRotation;

	private Vector3 m_assignedTargetPosition;

	private Vector2 m_previousAngleOffset;

	private float m_targetTimer;

	private Vector3 m_cachedPosition;

	private Vector2 m_cachedAngle;

	private float m_cachedSize;

	private float m_cachedHeight;

	private SavedBool m_edgeScrolling;

	private SavedBool m_invertYMouse;

	private SavedBool m_mouseWheelZoom;

	private SavedFloat m_mouseSensitivity;

	private SavedFloat m_edgeScrollSensitivity;

	private SavedInputKey m_cameraMoveLeft;

	private SavedInputKey m_cameraMoveRight;

	private SavedInputKey m_cameraMoveForward;

	private SavedInputKey m_cameraMoveBackward;

	private SavedInputKey m_cameraMouseRotate;

	private SavedInputKey m_cameraRotateLeft;

	private SavedInputKey m_cameraRotateRight;

	private SavedInputKey m_cameraRotateUp;

	private SavedInputKey m_cameraRotateDown;

	private SavedInputKey m_cameraZoomCloser;

	private SavedInputKey m_cameraZoomAway;

	private SavedInt m_DOFMode = new SavedInt(Settings.dofMode, Settings.gameSettingsFile, DefaultSettings.dofMode, true);

	private SavedFloat m_TiltShiftAmount = new SavedFloat(Settings.tiltShiftAmount, Settings.gameSettingsFile, DefaultSettings.tiltShiftAmount, true);

	private SavedFloat m_FilmGrainAmount = new SavedFloat(Settings.filmGrainAmount, Settings.gameSettingsFile, DefaultSettings.filmGrainAmount, true);

	private SavedInt m_Antialiasing = new SavedInt(Settings.antialiasing, Settings.gameSettingsFile, DefaultSettings.antialiasing, true);

	private SavedInt m_ShadowsDistance = new SavedInt(Settings.shadowsDistance, Settings.gameSettingsFile, DefaultSettings.shadowsDistance, true);

	private SavedInt m_ShadowsQuality = new SavedInt(Settings.shadowsQuality, Settings.gameSettingsFile, DefaultSettings.shadowsQuality, true);

	public enum ViewMode
	{
		Undefined,
		Normal,
		Info
	}

	public enum DepthOfFieldMode
	{
		Disabled,
		Legacy,
		DX9,
		DX11
	}
}
