﻿using System;
using ColossalFramework;
using UnityEngine;

public class DamPowerHouseAI : PowerPlantAI
{
	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = num * budget / 100;
		num = (num * (int)data.GetLastFrameData().m_productionState + 99) / 100;
		int num2;
		int num3;
		this.GetElectricityProduction(out num2, out num3);
		return num * num3 / 100;
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Vector3 worldPos = buildingData.CalculatePosition(this.m_outputOffset);
		float num = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, false, 0f);
		float num2 = buildingData.m_position.y - 8f;
		ushort num3 = buildingData.FindSegment(ItemClass.Service.Electricity, ItemClass.SubService.None, ItemClass.Layer.Default);
		if (num3 != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort startNode = instance.m_segments.m_buffer[(int)num3].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)num3].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			num2 = Mathf.Max(num2, position.y * 0.5f + position2.y * 0.5f - 8f);
		}
		float num4 = num2;
		int num5 = this.HandleWaterSource(buildingID, ref buildingData, 20f, num2);
		int num6 = Mathf.Clamp(Mathf.CeilToInt((float)num5 * (num4 - num) * 1E-05f), 0, 100);
		frameData.m_productionState = (byte)num6;
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		finalProductionRate = (finalProductionRate * (int)frameData.m_productionState + 99) / 100;
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
	}

	protected override void AddProductionCapacity(ref District districtData, int electricityProduction)
	{
		districtData.m_productionData.m_tempElectricityCapacity = districtData.m_productionData.m_tempElectricityCapacity + (uint)electricityProduction;
		districtData.m_productionData.m_tempRenewableElectricity = districtData.m_productionData.m_tempRenewableElectricity + (uint)electricityProduction;
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		onlyCollapse = false;
		return false;
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		return demolish && base.CollapseBuilding(buildingID, ref data, group, testOnly, demolish, burnAmount);
	}

	private int HandleWaterSource(ushort buildingID, ref Building data, float radius, float target)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		Vector3 inputPosition = data.CalculatePosition(this.m_inputOffset);
		Vector3 outputPosition = data.CalculatePosition(this.m_outputOffset);
		if (data.m_waterSource != 0)
		{
			WaterSource sourceData = waterSimulation.LockWaterSource(data.m_waterSource);
			try
			{
				sourceData.m_inputPosition = inputPosition;
				sourceData.m_outputPosition = outputPosition;
				sourceData.m_inputRate = 10000000u;
				sourceData.m_outputRate = 10000000u;
				sourceData.m_target = (ushort)Mathf.Clamp(Mathf.RoundToInt(target * 64f), 0, 65535);
				return (int)(sourceData.m_flow * 4u);
			}
			finally
			{
				waterSimulation.UnlockWaterSource(data.m_waterSource, sourceData);
			}
		}
		inputPosition.y = 0f;
		outputPosition.y = 0f;
		if (instance.GetClosestWaterPos(ref inputPosition, radius) && instance.GetClosestWaterPos(ref outputPosition, radius))
		{
			waterSimulation.CreateWaterSource(out data.m_waterSource, new WaterSource
			{
				m_type = 2,
				m_inputPosition = inputPosition,
				m_outputPosition = outputPosition,
				m_inputRate = 10000000u,
				m_outputRate = 10000000u,
				m_target = (ushort)Mathf.Clamp(Mathf.RoundToInt(target * 64f), 0, 65535)
			});
		}
		return 0;
	}

	public override void GetElectricityProduction(out int min, out int max)
	{
		min = 0;
		max = this.m_electricityProduction;
	}

	public override bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		return false;
	}

	public override int GetRefundAmount(ushort buildingID, ref Building data)
	{
		int num = 0;
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num2 = data.m_netNode;
		int num3 = 0;
		while (num2 != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num2].GetSegment(i);
				if (segment != 0)
				{
					ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
					if (startNode == num2 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
					{
						NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
						ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
						Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
						float startHeight = (float)instance.m_nodes.m_buffer[(int)startNode].m_elevation;
						float endHeight = (float)instance.m_nodes.m_buffer[(int)endNode].m_elevation;
						num += info.m_netAI.GetConstructionCost(position, position2, startHeight, endHeight);
					}
				}
			}
			num2 = instance.m_nodes.m_buffer[(int)num2].m_nextBuildingNode;
			if (++num3 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int result = num * 3 / 4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetRefundAmount(num, ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public Vector3 m_inputOffset = Vector3.get_zero();

	public Vector3 m_outputOffset = Vector3.get_zero();
}
