﻿using System;
using System.IO;
using ColossalFramework.Globalization;
using ColossalFramework.IO;

public abstract class GeneratedString : IDataContainer
{
	public abstract object GetArg();

	public abstract void Serialize(DataSerializer s);

	public abstract void Deserialize(DataSerializer s);

	public abstract void AfterDeserialize(DataSerializer s);

	public static byte[] SerializeToByteArray(GeneratedString obj)
	{
		MemoryStream memoryStream = new MemoryStream();
		DataSerializer.Serialize(memoryStream, 1, 0u, obj);
		return memoryStream.ToArray();
	}

	public static GeneratedString DeserializeFromByteArray(byte[] array)
	{
		MemoryStream memoryStream = new MemoryStream(array);
		return DataSerializer.Deserialize<GeneratedString>(memoryStream, 1);
	}

	public static byte[] SerializeArrayToByteArray(GeneratedString[] obj)
	{
		MemoryStream memoryStream = new MemoryStream();
		DataSerializer.SerializeArray<GeneratedString>(memoryStream, 1, 0u, obj);
		return memoryStream.ToArray();
	}

	public static GeneratedString[] DeserializeArrayFromByteArray(byte[] array)
	{
		MemoryStream memoryStream = new MemoryStream(array);
		return DataSerializer.DeserializeArray<GeneratedString>(memoryStream, 1);
	}

	public class Locale : GeneratedString
	{
		public Locale()
		{
		}

		public Locale(string localeID)
		{
			this.m_localeID = localeID;
		}

		public Locale(string localeID, string localeKey)
		{
			this.m_localeID = localeID;
			this.m_localeKey = localeKey;
		}

		public Locale(string localeID, int localeIndex)
		{
			this.m_localeID = localeID;
			this.m_localeIndex = localeIndex;
		}

		public Locale(string localeID, string localeKey, int localeIndex)
		{
			this.m_localeID = localeID;
			this.m_localeKey = localeKey;
			this.m_localeIndex = localeIndex;
		}

		public override object GetArg()
		{
			return this.ToString();
		}

		public override string ToString()
		{
			return ColossalFramework.Globalization.Locale.Get(this.m_localeID, this.m_localeKey, this.m_localeIndex);
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteUniqueString(this.m_localeID);
			s.WriteUniqueString(this.m_localeKey);
			s.WriteInt32(this.m_localeIndex);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_localeID = s.ReadUniqueString();
			this.m_localeKey = s.ReadUniqueString();
			this.m_localeIndex = s.ReadInt32();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private string m_localeID;

		private string m_localeKey;

		private int m_localeIndex;
	}

	public class Format : GeneratedString
	{
		public Format()
		{
		}

		public Format(GeneratedString format, params GeneratedString[] args)
		{
			this.m_format = format;
			this.m_args = args;
		}

		public override object GetArg()
		{
			return this.ToString();
		}

		public override string ToString()
		{
			string format = this.m_format.ToString();
			object[] array = new object[this.m_args.Length];
			for (int i = 0; i < this.m_args.Length; i++)
			{
				array[i] = this.m_args[i].GetArg();
			}
			return StringUtils.SafeFormat(format, array);
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteObject<GeneratedString>(this.m_format);
			s.WriteObjectArray<GeneratedString>(this.m_args);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_format = s.ReadObject<GeneratedString>();
			this.m_args = s.ReadObjectArray<GeneratedString>();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private GeneratedString m_format;

		private GeneratedString[] m_args;
	}

	public class Join : GeneratedString
	{
		public Join()
		{
		}

		public Join(GeneratedString separator, params GeneratedString[] value)
		{
			this.m_separator = separator;
			this.m_value = value;
		}

		public override object GetArg()
		{
			return this.ToString();
		}

		public override string ToString()
		{
			string separator = this.m_separator.ToString();
			string[] array = new string[this.m_value.Length];
			for (int i = 0; i < this.m_value.Length; i++)
			{
				array[i] = this.m_value[i].ToString();
			}
			return string.Join(separator, array);
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteObject<GeneratedString>(this.m_separator);
			s.WriteObjectArray<GeneratedString>(this.m_value);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_separator = s.ReadObject<GeneratedString>();
			this.m_value = s.ReadObjectArray<GeneratedString>();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private GeneratedString m_separator;

		private GeneratedString[] m_value;
	}

	public class String : GeneratedString
	{
		public String()
		{
		}

		public String(string value)
		{
			this.m_value = value;
		}

		public override object GetArg()
		{
			return this.m_value;
		}

		public override string ToString()
		{
			return this.m_value;
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteUniqueString(this.m_value);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_value = s.ReadUniqueString();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private string m_value;
	}

	public class Long : GeneratedString
	{
		public Long()
		{
		}

		public Long(long value)
		{
			this.m_value = value;
		}

		public override object GetArg()
		{
			return this.m_value;
		}

		public override string ToString()
		{
			return this.m_value.ToString();
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteLong64(this.m_value);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_value = s.ReadLong64();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private long m_value;
	}

	public class Float : GeneratedString
	{
		public Float()
		{
		}

		public Float(float value)
		{
			this.m_value = value;
		}

		public override object GetArg()
		{
			return this.m_value;
		}

		public override string ToString()
		{
			return this.m_value.ToString();
		}

		public override void Serialize(DataSerializer s)
		{
			s.WriteFloat(this.m_value);
		}

		public override void Deserialize(DataSerializer s)
		{
			this.m_value = s.ReadFloat();
		}

		public override void AfterDeserialize(DataSerializer s)
		{
		}

		private float m_value;
	}
}
