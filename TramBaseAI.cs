﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class TramBaseAI : VehicleAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (data.m_leadingVehicle != 0 && infoMode != InfoManager.InfoMode.None)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort firstVehicle = data.GetFirstVehicle(vehicleID);
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)firstVehicle].Info;
			return info.m_vehicleAI.GetColor(firstVehicle, ref instance.m_vehicles.m_buffer[(int)firstVehicle], infoMode);
		}
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int noiseLevel = this.GetNoiseLevel();
			return CommonBuildingAI.GetNoisePollutionColor((float)noiseLevel * 2.5f);
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			byte pathFindFlags = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				this.PathfindSuccess(vehicleID, ref data);
				this.PathFindReady(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0 || data.m_path == 0u)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(vehicleID, ref data);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		VehicleInfo info = instance.m_vehicles.m_buffer[(int)vehicleID].Info;
		info.m_vehicleAI.SimulationStep(vehicleID, ref instance.m_vehicles.m_buffer[(int)vehicleID], vehicleID, ref data, 0);
		if ((data.m_flags & (Vehicle.Flags.Created | Vehicle.Flags.Deleted)) != Vehicle.Flags.Created)
		{
			return;
		}
		ushort trailingVehicle = instance.m_vehicles.m_buffer[(int)vehicleID].m_trailingVehicle;
		int num = 0;
		while (trailingVehicle != 0)
		{
			info = instance.m_vehicles.m_buffer[(int)trailingVehicle].Info;
			info.m_vehicleAI.SimulationStep(trailingVehicle, ref instance.m_vehicles.m_buffer[(int)trailingVehicle], vehicleID, ref data, 0);
			if ((data.m_flags & (Vehicle.Flags.Created | Vehicle.Flags.Deleted)) != Vehicle.Flags.Created)
			{
				return;
			}
			trailingVehicle = instance.m_vehicles.m_buffer[(int)trailingVehicle].m_trailingVehicle;
			if (++num > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo)) == (Vehicle.Flags)0 || data.m_blockCounter == 255)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		data.Unspawn(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		ushort leadingVehicle = vehicleData.m_leadingVehicle;
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		VehicleInfo info;
		if (leaderID != vehicleID)
		{
			info = leaderData.Info;
		}
		else
		{
			info = this.m_info;
		}
		TramBaseAI tramBaseAI = info.m_vehicleAI as TramBaseAI;
		if (leadingVehicle != 0)
		{
			frameData.m_position += frameData.m_velocity * 0.4f;
		}
		else
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
		}
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		Vector3 vector = frameData.m_rotation * new Vector3(0f, 0f, this.m_info.m_generatedInfo.m_wheelBase * 0.5f);
		Vector3 vector2 = frameData.m_position + vector;
		Vector3 vector3 = frameData.m_position - vector;
		float acceleration = this.m_info.m_acceleration;
		float braking = this.m_info.m_braking;
		float magnitude = frameData.m_velocity.get_magnitude();
		Vector3 vector4 = vehicleData.m_targetPos1 - vector2;
		float num = vector4.get_sqrMagnitude();
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		Vector3 vector5 = quaternion * frameData.m_velocity;
		Vector3 vector6 = Vector3.get_forward();
		Vector3 vector7 = Vector3.get_zero();
		float num2 = 0f;
		float num3 = 0.5f;
		float num4 = 0f;
		if (leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Vehicle.Frame lastFrameData = instance.m_vehicles.m_buffer[(int)leadingVehicle].GetLastFrameData();
			VehicleInfo info2 = instance.m_vehicles.m_buffer[(int)leadingVehicle].Info;
			float num5;
			if ((vehicleData.m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
			{
				num5 = this.m_info.m_attachOffsetBack - this.m_info.m_generatedInfo.m_size.z * 0.5f;
			}
			else
			{
				num5 = this.m_info.m_attachOffsetFront - this.m_info.m_generatedInfo.m_size.z * 0.5f;
			}
			float num6;
			if ((instance.m_vehicles.m_buffer[(int)leadingVehicle].m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
			{
				num6 = info2.m_attachOffsetFront - info2.m_generatedInfo.m_size.z * 0.5f;
			}
			else
			{
				num6 = info2.m_attachOffsetBack - info2.m_generatedInfo.m_size.z * 0.5f;
			}
			Vector3 vector8 = frameData.m_position - frameData.m_rotation * new Vector3(0f, 0f, num5);
			Vector3 vector9 = lastFrameData.m_position + lastFrameData.m_rotation * new Vector3(0f, 0f, num6);
			vector = lastFrameData.m_rotation * new Vector3(0f, 0f, info2.m_generatedInfo.m_wheelBase * 0.5f);
			Vector3 vector10 = lastFrameData.m_position - vector;
			if (Vector3.Dot(vehicleData.m_targetPos1 - vehicleData.m_targetPos0, vehicleData.m_targetPos0 - vector3) < 0f && vehicleData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				int num7 = -1;
				tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vehicleData.m_targetPos0, vector3, 0, ref leaderData, ref num7, 0, 0, Vector3.SqrMagnitude(vector3 - vehicleData.m_targetPos0) + 1f, 1f);
				num = 0f;
			}
			float num8 = Mathf.Max(Vector3.Distance(vector8, vector9), 2f);
			float num9 = 1f;
			float num10 = num8 * num8;
			float minSqrDistanceB = num9 * num9;
			int i = 0;
			if (num < num10)
			{
				if (vehicleData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
				{
					tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vector3, vector2, 0, ref leaderData, ref i, 1, 2, num10, minSqrDistanceB);
				}
				while (i < 4)
				{
					vehicleData.SetTargetPos(i, vehicleData.GetTargetPos(i - 1));
					i++;
				}
				vector4 = vehicleData.m_targetPos1 - vector2;
				num = vector4.get_sqrMagnitude();
			}
			vector4 = quaternion * vector4;
			float num11 = (this.m_info.m_generatedInfo.m_wheelBase + info2.m_generatedInfo.m_wheelBase) * -0.5f - num5 - num6;
			bool flag = false;
			if (vehicleData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				float num12;
				float num13;
				if (Line3.Intersect(vector2, vehicleData.m_targetPos1, vector10, num11, ref num12, ref num13))
				{
					vector7 = vector4 * Mathf.Clamp(Mathf.Min(num12, num13) / 0.6f, 0f, 2f);
				}
				else
				{
					Line3.DistanceSqr(vector2, vehicleData.m_targetPos1, vector10, ref num12);
					vector7 = vector4 * Mathf.Clamp(num12 / 0.6f, 0f, 2f);
				}
				flag = true;
			}
			if (flag)
			{
				if (Vector3.Dot(vector10 - vector2, vector2 - vector3) < 0f)
				{
					num3 = 0f;
				}
			}
			else
			{
				float num14 = Vector3.Distance(vector10, vector2);
				num3 = 0f;
				vector7 = quaternion * ((vector10 - vector2) * (Mathf.Max(0f, num14 - num11) / Mathf.Max(1f, num14 * 0.6f)));
			}
		}
		else
		{
			float num15 = (magnitude + acceleration) * (0.5f + 0.5f * (magnitude + acceleration) / braking) + (this.m_info.m_generatedInfo.m_size.z - this.m_info.m_generatedInfo.m_wheelBase) * 0.5f;
			float num16 = Mathf.Max(magnitude + acceleration, 2f);
			float num17 = Mathf.Max((num15 - num16) / 2f, 2f);
			float num18 = num16 * num16;
			float minSqrDistanceB2 = num17 * num17;
			if (Vector3.Dot(vehicleData.m_targetPos1 - vehicleData.m_targetPos0, vehicleData.m_targetPos0 - vector3) < 0f && vehicleData.m_path != 0u && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
			{
				int num19 = -1;
				tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vehicleData.m_targetPos0, vector3, leaderID, ref leaderData, ref num19, 0, 0, Vector3.SqrMagnitude(vector3 - vehicleData.m_targetPos0) + 1f, 1f);
				num = 0f;
			}
			int j = 0;
			bool flag2 = false;
			if ((num < num18 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
			{
				if (vehicleData.m_path != 0u)
				{
					tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vector3, vector2, leaderID, ref leaderData, ref j, 1, 4, num18, minSqrDistanceB2);
				}
				if (j < 4)
				{
					flag2 = true;
					while (j < 4)
					{
						vehicleData.SetTargetPos(j, vehicleData.GetTargetPos(j - 1));
						j++;
					}
				}
				vector4 = vehicleData.m_targetPos1 - vector2;
				num = vector4.get_sqrMagnitude();
			}
			if (leaderData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				byte b = leaderData.m_pathPositionIndex;
				byte lastPathOffset = leaderData.m_lastPathOffset;
				if (b == 255)
				{
					b = 0;
				}
				int noise;
				float num20 = 1f + leaderData.CalculateTotalLength(leaderID, out noise);
				PathManager instance3 = Singleton<PathManager>.get_instance();
				PathUnit.Position pathPos;
				if (instance3.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetPosition(b >> 1, out pathPos))
				{
					instance2.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(num20 * 2.5f), noise);
					bool flag3 = false;
					if ((b & 1) == 0 || lastPathOffset == 0)
					{
						uint laneID = PathManager.GetLaneID(pathPos);
						if (laneID != 0u)
						{
							Vector3 vector11 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
							float num21 = 0.5f * magnitude * magnitude / this.m_info.m_braking;
							float num22 = Vector3.Distance(vector2, vector11);
							float num23 = Vector3.Distance(vector3, vector11);
							if (Mathf.Min(num22, num23) >= num21 - 1f)
							{
								instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(num20);
								flag3 = true;
							}
						}
					}
					if (!flag3 && instance3.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetNextPosition(b >> 1, out pathPos))
					{
						uint laneID2 = PathManager.GetLaneID(pathPos);
						if (laneID2 != 0u)
						{
							instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(num20);
						}
					}
				}
				if ((ulong)(currentFrameIndex >> 4 & 15u) == (ulong)((long)(leaderID & 15)))
				{
					bool flag4 = false;
					uint path = leaderData.m_path;
					int num24 = b >> 1;
					int k = 0;
					while (k < 5)
					{
						bool flag5;
						if (PathUnit.GetNextPosition(ref path, ref num24, out pathPos, out flag5))
						{
							uint laneID3 = PathManager.GetLaneID(pathPos);
							if (laneID3 != 0u && !instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID3)].CheckSpace(num20))
							{
								k++;
								continue;
							}
						}
						if (flag5)
						{
							this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
						}
						flag4 = true;
						break;
					}
					if (!flag4)
					{
						leaderData.m_flags |= Vehicle.Flags.Congestion;
					}
				}
			}
			float num25;
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
			{
				num25 = 0f;
			}
			else
			{
				num25 = Mathf.Min(vehicleData.m_targetPos1.w, TramBaseAI.GetMaxSpeed(leaderID, ref leaderData));
			}
			vector4 = quaternion * vector4;
			Vector3 zero = Vector3.get_zero();
			bool flag6 = false;
			float num26 = 0f;
			if (num > 1f)
			{
				vector6 = VectorUtils.NormalizeXZ(vector4, ref num26);
				if (num26 > 1f)
				{
					Vector3 vector12 = vector4;
					num16 = Mathf.Max(magnitude, 2f);
					num18 = num16 * num16;
					if (num > num18)
					{
						float num27 = num16 / Mathf.Sqrt(num);
						vector12.x *= num27;
						vector12.y *= num27;
					}
					if (vector12.z < -1f)
					{
						if (vehicleData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
						{
							Vector3 vector13 = vehicleData.m_targetPos1 - vehicleData.m_targetPos0;
							if ((quaternion * vector13).z < -0.01f)
							{
								if (vector4.z < Mathf.Abs(vector4.x) * -10f)
								{
									vector12.z = 0f;
									vector4 = Vector3.get_zero();
									num25 = 0f;
								}
								else
								{
									vector2 = vector3 + Vector3.Normalize(vehicleData.m_targetPos1 - vehicleData.m_targetPos0) * this.m_info.m_generatedInfo.m_wheelBase;
									j = -1;
									tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vehicleData.m_targetPos0, vehicleData.m_targetPos1, leaderID, ref leaderData, ref j, 0, 0, Vector3.SqrMagnitude(vehicleData.m_targetPos1 - vehicleData.m_targetPos0) + 1f, 1f);
								}
							}
							else
							{
								j = -1;
								tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vehicleData.m_targetPos0, vector3, leaderID, ref leaderData, ref j, 0, 0, Vector3.SqrMagnitude(vector3 - vehicleData.m_targetPos0) + 1f, 1f);
								vehicleData.m_targetPos1 = vector2;
								vector12.z = 0f;
								vector4 = Vector3.get_zero();
								num25 = 0f;
							}
						}
						num3 = 0f;
					}
					vector6 = VectorUtils.NormalizeXZ(vector12, ref num26);
					float num28 = 1.57079637f * (1f - vector6.z);
					if (num26 > 1f)
					{
						num28 /= num26;
					}
					float num29 = num26;
					if (vehicleData.m_targetPos1.w < 0.1f)
					{
						num25 = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num28);
						num25 = Mathf.Min(num25, TramBaseAI.CalculateMaxSpeed(num29, vehicleData.m_targetPos1.w, braking * 0.9f));
					}
					else
					{
						num25 = Mathf.Min(num25, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num28));
					}
					num25 = Mathf.Min(num25, TramBaseAI.CalculateMaxSpeed(num29, vehicleData.m_targetPos2.w, braking * 0.9f));
					num29 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
					num25 = Mathf.Min(num25, TramBaseAI.CalculateMaxSpeed(num29, vehicleData.m_targetPos3.w, braking * 0.9f));
					num29 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
					if (vehicleData.m_targetPos3.w < 0.01f)
					{
						num29 = Mathf.Max(0f, num29 + (this.m_info.m_generatedInfo.m_wheelBase - this.m_info.m_generatedInfo.m_size.z) * 0.5f);
					}
					num25 = Mathf.Min(num25, TramBaseAI.CalculateMaxSpeed(num29, 0f, braking * 0.9f));
					CarAI.CheckOtherVehicles(vehicleID, ref vehicleData, ref frameData, ref num25, ref flag6, ref zero, num15, braking * 0.9f, lodPhysics);
					if (num25 < magnitude)
					{
						float num30 = Mathf.Max(acceleration, Mathf.Min(braking, magnitude));
						num2 = Mathf.Max(num25, magnitude - num30);
					}
					else
					{
						float num31 = Mathf.Max(acceleration, Mathf.Min(braking, -magnitude));
						num2 = Mathf.Min(num25, magnitude + num31);
					}
				}
			}
			else if (magnitude < 0.1f && flag2 && info.m_vehicleAI.ArriveAtDestination(leaderID, ref leaderData))
			{
				leaderData.Unspawn(leaderID);
				return;
			}
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num25 < 0.1f)
			{
				flag6 = true;
			}
			if (flag6)
			{
				leaderData.m_blockCounter = (byte)Mathf.Min((int)(leaderData.m_blockCounter + 1), 255);
			}
			else
			{
				leaderData.m_blockCounter = 0;
			}
			if (num26 > 1f)
			{
				num4 = Mathf.Asin(vector6.x) * Mathf.Sign(num2);
				vector7 = vector6 * num2;
			}
			else
			{
				Vector3 vector14 = Vector3.ClampMagnitude(vector4 * 0.5f - vector5, braking);
				vector7 = vector5 + vector14;
			}
		}
		bool flag7 = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		Vector3 vector15 = vector7 - vector5;
		Vector3 vector16 = frameData.m_rotation * vector7;
		Vector3 vector17 = Vector3.Normalize(vehicleData.m_targetPos0 - vector3) * (vector7.get_magnitude() * num3);
		vector17 -= vector16 * (Vector3.Dot(vector16, vector17) / Mathf.Max(1f, vector16.get_sqrMagnitude()));
		vector2 += vector16;
		vector3 += vector17;
		frameData.m_rotation = Quaternion.LookRotation(vector2 - vector3);
		Vector3 vector18 = vector2 - frameData.m_rotation * new Vector3(0f, 0f, this.m_info.m_generatedInfo.m_wheelBase * 0.5f);
		frameData.m_velocity = vector18 - frameData.m_position;
		if (leadingVehicle != 0)
		{
			frameData.m_position += frameData.m_velocity * 0.6f;
		}
		else
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
		}
		frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector15 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = 0f;
		frameData.m_travelDistance += vector7.z;
		if (leadingVehicle != 0)
		{
			frameData.m_lightIntensity = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)leaderID].GetLastFrameData().m_lightIntensity;
		}
		else
		{
			frameData.m_lightIntensity.x = 5f;
			frameData.m_lightIntensity.y = ((vector15.z >= -0.1f) ? 0.5f : 5f);
			frameData.m_lightIntensity.z = ((num4 >= -0.1f || !flag7) ? 0f : 5f);
			frameData.m_lightIntensity.w = ((num4 <= 0.1f || !flag7) ? 0f : 5f);
		}
		frameData.m_underground = ((vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
		frameData.m_transition = ((vehicleData.m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	protected void UpdatePathTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos1, Vector3 refPos2, ushort leaderID, ref Vehicle leaderData, ref int index, int max1, int max2, float minSqrDistanceA, float minSqrDistanceB)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = vehicleData.m_targetPos0;
		vector.w = 1000f;
		float num = minSqrDistanceA;
		float num2 = 0f;
		uint num3 = vehicleData.m_path;
		byte b = vehicleData.m_pathPositionIndex;
		byte b2 = vehicleData.m_lastPathOffset;
		if (b == 255)
		{
			b = 0;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = 0;
			}
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)num3)].CalculatePathPositionOffset(b >> 1, vector, out b2))
			{
				this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
				return;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].GetPosition(b >> 1, out position))
		{
			this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
			return;
		}
		uint num4 = PathManager.GetLaneID(position);
		Bezier3 bezier;
		while (true)
		{
			if ((b & 1) == 0)
			{
				bool flag = true;
				while (b2 != position.m_offset)
				{
					if (flag)
					{
						flag = false;
					}
					else
					{
						float num5 = Mathf.Max(Mathf.Sqrt(num) - Vector3.Distance(vector, refPos1), Mathf.Sqrt(num2) - Vector3.Distance(vector, refPos2));
						int num6;
						if (num5 < 0f)
						{
							num6 = 4;
						}
						else
						{
							num6 = 4 + Mathf.CeilToInt(num5 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num4)].m_length + 1f));
						}
						if (b2 > position.m_offset)
						{
							b2 = (byte)Mathf.Max((int)b2 - num6, (int)position.m_offset);
						}
						else if (b2 < position.m_offset)
						{
							b2 = (byte)Mathf.Min((int)b2 + num6, (int)position.m_offset);
						}
					}
					Vector3 vector2;
					Vector3 vector3;
					float num7;
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num4, b2, out vector2, out vector3, out num7);
					vector.Set(vector2.x, vector2.y, vector2.z, Mathf.Min(vector.w, num7));
					float sqrMagnitude = (vector2 - refPos1).get_sqrMagnitude();
					float sqrMagnitude2 = (vector2 - refPos2).get_sqrMagnitude();
					if (sqrMagnitude >= num && sqrMagnitude2 >= num2)
					{
						if (index <= 0)
						{
							vehicleData.m_lastPathOffset = b2;
						}
						vehicleData.SetTargetPos(index++, vector);
						if (index < max1)
						{
							num = minSqrDistanceB;
							refPos1 = vector;
						}
						else if (index == max1)
						{
							num = (refPos2 - refPos1).get_sqrMagnitude();
							num2 = minSqrDistanceA;
						}
						else
						{
							num2 = minSqrDistanceB;
							refPos2 = vector;
						}
						vector.w = 1000f;
						if (index == max2)
						{
							return;
						}
					}
				}
				b += 1;
				b2 = 0;
				if (index <= 0)
				{
					vehicleData.m_pathPositionIndex = b;
					vehicleData.m_lastPathOffset = b2;
				}
			}
			int num8 = (b >> 1) + 1;
			uint num9 = num3;
			if (num8 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].m_positionCount)
			{
				num8 = 0;
				num9 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num3)].m_nextPathUnit;
				if (num9 == 0u)
				{
					goto Block_19;
				}
			}
			PathUnit.Position position2;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetPosition(num8, out position2))
			{
				goto Block_21;
			}
			NetInfo info = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
			if (info.m_lanes.Length <= (int)position2.m_lane)
			{
				goto Block_22;
			}
			uint laneID = PathManager.GetLaneID(position2);
			NetInfo.Lane lane = info.m_lanes[(int)position2.m_lane];
			if (lane.m_laneType != NetInfo.LaneType.Vehicle)
			{
				goto Block_23;
			}
			if (position2.m_segment != position.m_segment && leaderID != 0)
			{
				leaderData.m_flags &= ~Vehicle.Flags.Leaving;
			}
			byte b3 = 0;
			if (num4 != laneID)
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
				bezier = default(Bezier3);
				Vector3 vector4;
				float num10;
				this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num4, position.m_offset, out bezier.a, out vector4, out num10);
				bool flag2 = vehicleData.m_leadingVehicle == 0;
				bool flag3 = flag2 && b2 == 0;
				Vector3 vector5;
				float num11;
				if (flag3)
				{
					PathUnit.Position nextPosition;
					if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetNextPosition(num8, out nextPosition))
					{
						nextPosition = default(PathUnit.Position);
					}
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position2, laneID, b3, position, num4, position.m_offset, index, out bezier.d, out vector5, out num11);
				}
				else
				{
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position2, laneID, b3, out bezier.d, out vector5, out num11);
				}
				if (position.m_offset == 0)
				{
					vector4 = -vector4;
				}
				if (b3 < position2.m_offset)
				{
					vector5 = -vector5;
				}
				vector4.Normalize();
				vector5.Normalize();
				float num12;
				NetSegment.CalculateMiddlePoints(bezier.a, vector4, bezier.d, vector5, true, true, out bezier.b, out bezier.c, out num12);
				if (flag2 && (num11 < 0.01f || (instance2.m_segments.m_buffer[(int)position2.m_segment].m_flags & (NetSegment.Flags.Collapsed | NetSegment.Flags.Flooded)) != NetSegment.Flags.None))
				{
					goto IL_5AD;
				}
				if (num12 > 1f)
				{
					ushort num13;
					if (b3 == 0)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					}
					else if (b3 == 255)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
					}
					else
					{
						num13 = 0;
					}
					float num14 = 1.57079637f * (1f + Vector3.Dot(vector4, vector5));
					if (num12 > 1f)
					{
						num14 /= num12;
					}
					num11 = Mathf.Min(num11, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14));
					while (b2 < 255)
					{
						float num15 = Mathf.Max(Mathf.Sqrt(num) - Vector3.Distance(vector, refPos1), Mathf.Sqrt(num2) - Vector3.Distance(vector, refPos2));
						int num16;
						if (num15 < 0f)
						{
							num16 = 8;
						}
						else
						{
							num16 = 8 + Mathf.CeilToInt(num15 * 256f / (num12 + 1f));
						}
						b2 = (byte)Mathf.Min((int)b2 + num16, 255);
						Vector3 vector6 = bezier.Position((float)b2 * 0.003921569f);
						vector.Set(vector6.x, vector6.y, vector6.z, Mathf.Min(vector.w, num11));
						float sqrMagnitude3 = (vector6 - refPos1).get_sqrMagnitude();
						float sqrMagnitude4 = (vector6 - refPos2).get_sqrMagnitude();
						if (sqrMagnitude3 >= num && sqrMagnitude4 >= num2)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							if (num13 != 0)
							{
								this.UpdateNodeTargetPos(vehicleID, ref vehicleData, num13, ref instance2.m_nodes.m_buffer[(int)num13], ref vector, index);
							}
							vehicleData.SetTargetPos(index++, vector);
							if (index < max1)
							{
								num = minSqrDistanceB;
								refPos1 = vector;
							}
							else if (index == max1)
							{
								num = (refPos2 - refPos1).get_sqrMagnitude();
								num2 = minSqrDistanceA;
							}
							else
							{
								num2 = minSqrDistanceB;
								refPos2 = vector;
							}
							vector.w = 1000f;
							if (index == max2)
							{
								return;
							}
						}
					}
				}
			}
			else
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
			}
			if (index <= 0)
			{
				if (num8 == 0)
				{
					Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
				}
				if (num8 >= (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_positionCount - 1) && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_nextPathUnit == 0u && leaderID != 0)
				{
					this.ArrivingToDestination(leaderID, ref leaderData);
				}
			}
			num3 = num9;
			b = (byte)(num8 << 1);
			b2 = b3;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = b;
				vehicleData.m_lastPathOffset = b2;
				vehicleData.m_flags = ((vehicleData.m_flags & ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition)) | info.m_setVehicleFlags);
				vehicleData.m_flags2 &= ~Vehicle.Flags2.Yielding;
			}
			position = position2;
			num4 = laneID;
		}
		return;
		Block_19:
		if (index <= 0)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		vector.w = 1f;
		vehicleData.SetTargetPos(index++, vector);
		return;
		Block_21:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		Block_22:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		Block_23:
		this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
		return;
		IL_5AD:
		if (index <= 0)
		{
			vehicleData.m_lastPathOffset = b2;
		}
		vector = bezier.a;
		vector.w = 0f;
		while (index < max2)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
	}

	private static bool CheckOverlap(ushort vehicleID, ref Vehicle vehicleData, Segment3 segment, ushort ignoreVehicle)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 min = segment.Min();
		Vector3 max = segment.Max();
		int num = Mathf.Max((int)((min.x - 30f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((min.z - 30f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((max.x + 30f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((max.z + 30f) / 32f + 270f), 539);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = TramBaseAI.CheckOverlap(vehicleID, ref vehicleData, segment, ignoreVehicle, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result, min, max);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(ushort vehicleID, ref Vehicle vehicleData, Segment3 segment, ushort ignoreVehicle, ushort otherID, ref Vehicle otherData, ref bool overlap, Vector3 min, Vector3 max)
	{
		if (ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle))
		{
			VehicleInfo info = otherData.Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				return otherData.m_nextGridVehicle;
			}
			if (((vehicleData.m_flags | otherData.m_flags) & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (vehicleData.m_flags & Vehicle.Flags.Underground) != (otherData.m_flags & Vehicle.Flags.Underground))
			{
				return otherData.m_nextGridVehicle;
			}
			Vector3 vector = Vector3.Min(otherData.m_segment.Min(), otherData.m_targetPos3);
			Vector3 vector2 = Vector3.Max(otherData.m_segment.Max(), otherData.m_targetPos3);
			if (min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
			{
				Vector3 vector3 = Vector3.Normalize(segment.b - segment.a);
				Vector3 vector4 = otherData.m_segment.a - vehicleData.m_segment.b;
				Vector3 vector5 = otherData.m_segment.b - vehicleData.m_segment.b;
				if (Vector3.Dot(vector4, vector3) >= 1f || Vector3.Dot(vector5, vector3) >= 1f)
				{
					float num2;
					float num3;
					float num = segment.DistanceSqr(otherData.m_segment, ref num2, ref num3);
					if (num < 4f)
					{
						overlap = true;
					}
					Vector3 vector6 = otherData.m_segment.b;
					segment.a.y = segment.a.y * 0.5f;
					segment.b.y = segment.b.y * 0.5f;
					for (int i = 0; i < 4; i++)
					{
						Vector3 vector7 = otherData.GetTargetPos(i);
						Segment3 segment2;
						segment2..ctor(vector6, vector7);
						segment2.a.y = segment2.a.y * 0.5f;
						segment2.b.y = segment2.b.y * 0.5f;
						if (segment2.LengthSqr() > 0.01f)
						{
							num = segment.DistanceSqr(segment2, ref num2, ref num3);
							if (num < 4f)
							{
								overlap = true;
								break;
							}
						}
						vector6 = vector7;
					}
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].CalculatePosition((float)prevOffset * 0.003921569f);
		Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
		Vector3 vector2 = lastFrameData.m_position;
		Vector3 vector3 = lastFrameData.m_position;
		Vector3 vector4 = lastFrameData.m_rotation * new Vector3(0f, 0f, this.m_info.m_generatedInfo.m_wheelBase * 0.5f);
		vector2 += vector4;
		vector3 -= vector4;
		float sqrMagnitude = lastFrameData.m_velocity.get_sqrMagnitude();
		float num = 0.5f * sqrMagnitude / this.m_info.m_braking;
		float num2 = Vector3.Distance(vector2, vector);
		float num3 = Vector3.Distance(vector3, vector);
		if (Mathf.Min(num2, num3) >= num - 1f)
		{
			Segment3 segment;
			segment.a = pos;
			ushort num4;
			ushort num5;
			if (offset < position.m_offset)
			{
				segment.b = pos + dir.get_normalized() * this.m_info.m_generatedInfo.m_size.z;
				num4 = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
				num5 = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			}
			else
			{
				segment.b = pos - dir.get_normalized() * this.m_info.m_generatedInfo.m_size.z;
				num4 = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
				num5 = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			}
			ushort num6;
			if (prevOffset == 0)
			{
				num6 = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_startNode;
			}
			else
			{
				num6 = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_endNode;
			}
			if (num4 == num6)
			{
				NetNode.Flags flags = instance.m_nodes.m_buffer[(int)num4].m_flags;
				NetLane.Flags flags2 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].m_flags;
				bool flag = (flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None;
				bool flag2 = (flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None;
				bool flag3 = (flags2 & NetLane.Flags.JoinedJunction) != NetLane.Flags.None;
				bool flag4 = (flags2 & (NetLane.Flags.YieldStart | NetLane.Flags.YieldEnd)) != NetLane.Flags.None && (flags & (NetNode.Flags.Junction | NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn)) == NetNode.Flags.Junction;
				if (flag4 && (vehicleData.m_flags2 & Vehicle.Flags2.Yielding) == (Vehicle.Flags2)0)
				{
					if (sqrMagnitude < 0.01f)
					{
						vehicleData.m_flags2 |= Vehicle.Flags2.Yielding;
					}
					maxSpeed = 0f;
					return;
				}
				if ((flags & (NetNode.Flags.Junction | NetNode.Flags.OneWayOut | NetNode.Flags.OneWayIn)) == NetNode.Flags.Junction && instance.m_nodes.m_buffer[(int)num4].CountSegments() != 2)
				{
					float len = vehicleData.CalculateTotalLength(vehicleID) + 2f;
					if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(len))
					{
						bool flag5 = false;
						if (nextPosition.m_segment != 0 && instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_length < 30f)
						{
							NetNode.Flags flags3 = instance.m_nodes.m_buffer[(int)num5].m_flags;
							if ((flags3 & (NetNode.Flags.Junction | NetNode.Flags.OneWayOut | NetNode.Flags.OneWayIn)) != NetNode.Flags.Junction || instance.m_nodes.m_buffer[(int)num5].CountSegments() == 2)
							{
								uint laneID2 = PathManager.GetLaneID(nextPosition);
								if (laneID2 != 0u)
								{
									flag5 = instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].CheckSpace(len);
								}
							}
						}
						if (!flag5)
						{
							vehicleData.m_flags2 |= Vehicle.Flags2.Yielding;
							maxSpeed = 0f;
							return;
						}
					}
				}
				if (flag && (!flag3 || flag2))
				{
					uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
					uint num7 = (uint)(((int)num6 << 8) / 32768);
					uint num8 = currentFrameIndex - num7 & 255u;
					RoadBaseAI.TrafficLightState trafficLightState;
					RoadBaseAI.TrafficLightState pedestrianLightState;
					bool flag6;
					bool pedestrians;
					RoadBaseAI.GetTrafficLightState(num6, ref instance.m_segments.m_buffer[(int)prevPos.m_segment], currentFrameIndex - num7, out trafficLightState, out pedestrianLightState, out flag6, out pedestrians);
					if (!flag6 && num8 >= 196u)
					{
						flag6 = true;
						RoadBaseAI.SetTrafficLightState(num6, ref instance.m_segments.m_buffer[(int)prevPos.m_segment], currentFrameIndex - num7, trafficLightState, pedestrianLightState, flag6, pedestrians);
					}
					if (trafficLightState != RoadBaseAI.TrafficLightState.RedToGreen)
					{
						if (trafficLightState != RoadBaseAI.TrafficLightState.GreenToRed)
						{
							if (trafficLightState == RoadBaseAI.TrafficLightState.Red)
							{
								vehicleData.m_flags2 |= Vehicle.Flags2.Yielding;
								maxSpeed = 0f;
								return;
							}
						}
						else if (num8 >= 30u)
						{
							vehicleData.m_flags2 |= Vehicle.Flags2.Yielding;
							maxSpeed = 0f;
							return;
						}
					}
					else if (num8 < 60u)
					{
						vehicleData.m_flags2 |= Vehicle.Flags2.Yielding;
						maxSpeed = 0f;
						return;
					}
				}
			}
		}
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	private static float GetMaxSpeed(ushort leaderID, ref Vehicle leaderData)
	{
		float num = 1000000f;
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num2 = leaderID;
		int num3 = 0;
		while (num2 != 0)
		{
			num = Mathf.Min(num, instance.m_vehicles.m_buffer[(int)num2].m_targetPos0.w);
			num = Mathf.Min(num, instance.m_vehicles.m_buffer[(int)num2].m_targetPos1.w);
			num2 = instance.m_vehicles.m_buffer[(int)num2].m_trailingVehicle;
			if (++num3 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num;
	}

	private static void ResetTargets(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData, bool pushPathPos)
	{
		Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
		VehicleInfo info = vehicleData.Info;
		TramBaseAI tramBaseAI = info.m_vehicleAI as TramBaseAI;
		Vector3 vector = lastFrameData.m_position;
		Vector3 vector2 = lastFrameData.m_position;
		Vector3 vector3 = lastFrameData.m_rotation * new Vector3(0f, 0f, info.m_generatedInfo.m_wheelBase * 0.5f);
		if ((leaderData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
		{
			vector -= vector3;
			vector2 += vector3;
		}
		else
		{
			vector += vector3;
			vector2 -= vector3;
		}
		vehicleData.m_targetPos0 = vector2;
		vehicleData.m_targetPos0.w = 2f;
		vehicleData.m_targetPos1 = vector;
		vehicleData.m_targetPos1.w = 2f;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos1;
		vehicleData.m_targetPos3 = vehicleData.m_targetPos1;
		if (vehicleData.m_path != 0u)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			int num = (vehicleData.m_pathPositionIndex >> 1) + 1;
			uint num2 = vehicleData.m_path;
			if (num >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_positionCount)
			{
				num = 0;
				num2 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_nextPathUnit;
			}
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(vehicleData.m_pathPositionIndex >> 1, out pathPos))
			{
				uint laneID = PathManager.GetLaneID(pathPos);
				PathUnit.Position pathPos2;
				if (num2 != 0u && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].GetPosition(num, out pathPos2))
				{
					uint laneID2 = PathManager.GetLaneID(pathPos2);
					if (laneID2 == laneID)
					{
						if (num2 != vehicleData.m_path)
						{
							instance.ReleaseFirstUnit(ref vehicleData.m_path);
						}
						vehicleData.m_pathPositionIndex = (byte)(num << 1);
					}
				}
				PathUnit.CalculatePathPositionOffset(laneID, vector2, out vehicleData.m_lastPathOffset);
			}
		}
		if (vehicleData.m_path != 0u)
		{
			int num3 = 0;
			tramBaseAI.UpdatePathTargetPositions(vehicleID, ref vehicleData, vector2, vector, 0, ref leaderData, ref num3, 1, 4, 4f, 1f);
		}
	}

	private static void InitializePath(ushort vehicleID, ref Vehicle vehicleData)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		ushort trailingVehicle = vehicleData.m_trailingVehicle;
		int num = 0;
		while (trailingVehicle != 0)
		{
			if (instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_path != 0u)
			{
				instance.ReleasePath(instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_path);
				instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_path = 0u;
			}
			if (instance.AddPathReference(vehicleData.m_path))
			{
				instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_path = vehicleData.m_path;
				instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_pathPositionIndex = 0;
			}
			TramBaseAI.ResetTargets(trailingVehicle, ref instance2.m_vehicles.m_buffer[(int)trailingVehicle], vehicleID, ref vehicleData, false);
			trailingVehicle = instance2.m_vehicles.m_buffer[(int)trailingVehicle].m_trailingVehicle;
			if (++num > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		vehicleData.m_pathPositionIndex = 0;
		TramBaseAI.ResetTargets(vehicleID, ref vehicleData, vehicleID, ref vehicleData, false);
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true);
	}

	protected bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays)
	{
		VehicleInfo info = this.m_info;
		bool allowUnderground;
		bool allowUnderground2;
		if (info.m_vehicleType == VehicleInfo.VehicleType.Metro)
		{
			allowUnderground = true;
			allowUnderground2 = true;
		}
		else
		{
			allowUnderground = ((vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0);
			allowUnderground2 = false;
		}
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle, info.m_vehicleType, allowUnderground2, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num2 > num * 1.2f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num4 > num3 * 1.2f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, false, false, true, false))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (vehicleData.m_path != 0u)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(0, out pathPos))
			{
				uint laneID = PathManager.GetLaneID(pathPos);
				if (laneID != 0u && !Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
				{
					vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
					return false;
				}
			}
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		TramBaseAI.InitializePath(vehicleID, ref vehicleData);
		return true;
	}

	protected virtual bool PathFindReady(ushort vehicleID, ref Vehicle vehicleData)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		float num = vehicleData.CalculateTotalLength(vehicleID);
		float distance = (num + this.m_info.m_generatedInfo.m_wheelBase - this.m_info.m_generatedInfo.m_size.z) * 0.5f;
		Vector3 vector = vehicleData.GetLastFramePosition();
		PathUnit.Position pathPos;
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0 && instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(0, out pathPos))
		{
			uint laneID = PathManager.GetLaneID(pathPos);
			vector = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
		}
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
		instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].MoveLastPosition(vehicleData.m_path, distance);
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			TramBaseAI.InitializePath(vehicleID, ref vehicleData);
		}
		else
		{
			int index = Mathf.Min(1, (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].m_positionCount - 1));
			PathUnit.Position pathPos2;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(index, out pathPos2))
			{
				uint laneID2 = PathManager.GetLaneID(pathPos2);
				Vector3 vector2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].CalculatePosition((float)pathPos2.m_offset * 0.003921569f);
				Vector3 vector3 = vector2 - vector;
				vehicleData.m_frame0.m_position = vector;
				if (vector3.get_sqrMagnitude() > 1f)
				{
					float length = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].m_length;
					vehicleData.m_frame0.m_position = vehicleData.m_frame0.m_position + vector3.get_normalized() * Mathf.Min(length * 0.5f, (num - this.m_info.m_generatedInfo.m_size.z) * 0.5f);
					vehicleData.m_frame0.m_rotation = Quaternion.LookRotation(vector3);
				}
				vehicleData.m_frame1 = vehicleData.m_frame0;
				vehicleData.m_frame2 = vehicleData.m_frame0;
				vehicleData.m_frame3 = vehicleData.m_frame0;
				this.FrameDataUpdated(vehicleID, ref vehicleData, ref vehicleData.m_frame0);
			}
			this.TrySpawn(vehicleID, ref vehicleData);
		}
		return true;
	}

	public override int GetNoiseLevel()
	{
		return 5;
	}

	public TransportInfo m_transportInfo;
}
