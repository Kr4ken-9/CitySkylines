﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class TerrainHeightInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Gradient = base.Find<UITextureSprite>("Gradient");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				if (Singleton<InfoManager>.get_exists())
				{
					this.m_Gradient.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					this.m_Gradient.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[21].m_inactiveColor);
					this.m_Gradient.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					this.m_Gradient.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					this.m_Gradient.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.m_initialized = true;
		}
	}

	private UITextureSprite m_Gradient;

	private bool m_initialized;
}
