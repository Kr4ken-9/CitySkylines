﻿using System;
using ColossalFramework.PlatformServices;
using UnityEngine;

public static class SteamController
{
	public static bool hasController
	{
		get
		{
			return PlatformService.IsControllerConnected();
		}
	}

	public static bool GetAnyDigitalActionDown()
	{
		for (int i = 0; i < 20; i++)
		{
			if (PlatformService.GetDigitalActionDown(i))
			{
				return true;
			}
		}
		return false;
	}

	public static ControllerActionOrigin[] GetDigitalGlyphsAction(SteamController.DigitalInput input)
	{
		return PlatformService.GetDigitalGlyphsAction((int)input);
	}

	public static bool GetDigitalActionDown(SteamController.DigitalInput input)
	{
		return PlatformService.GetDigitalActionDown((int)input);
	}

	public static bool GetDigitalActionUp(SteamController.DigitalInput input)
	{
		return PlatformService.GetDigitalActionUp((int)input);
	}

	public static bool GetDigitalAction(SteamController.DigitalInput input)
	{
		return PlatformService.GetDigitalAction((int)input);
	}

	public static Vector2 GetAnalogAction(SteamController.AnalogInput input)
	{
		return new Vector2(PlatformService.GetAnalogActionX((int)input), PlatformService.GetAnalogActionY((int)input));
	}

	public enum DigitalInput
	{
		Select,
		Cancel,
		FreeCamera,
		Economy,
		Policies,
		AreaView,
		Escape,
		PauseSimulation,
		NormalSimSpeed,
		MediumSimSpeed,
		FastSimSpeed,
		Bulldoze,
		RotateMouse,
		ZoomIn,
		ZoomOut,
		ElevationUp,
		ElevationDown,
		Unlocking,
		InfoViews,
		PauseMenu,
		Max
	}

	public enum AnalogInput
	{
		CameraScroll,
		Max
	}
}
