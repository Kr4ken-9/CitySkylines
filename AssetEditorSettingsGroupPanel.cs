﻿using System;

public sealed class AssetEditorSettingsGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("AssetEditorSettings");
		return true;
	}
}
