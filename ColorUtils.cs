﻿using System;
using UnityEngine;

public static class ColorUtils
{
	public static Color LinearLerp(Color color1, Color color2, float t)
	{
		return Color.Lerp(color1.get_linear(), color2.get_linear(), t).get_gamma();
	}

	public static Color GetBilinearFilteredPixelColor(Color[] colors, float position, int size)
	{
		position *= (float)size;
		int num = (int)Mathf.Floor(position);
		float num2 = position - (float)num;
		float num3 = 1f - num2;
		return colors[num] * num3 + colors[num + 1] * num2;
	}

	public static Color Desaturate(Color color, float amount)
	{
		Vector3 vector;
		vector..ctor(color.r, color.g, color.b);
		float num = Vector3.Dot(vector, new Vector3(0.3f, 0.6f, 0.1f));
		Vector3 vector2 = vector * (1f - amount) + new Vector3(num, num, num) * amount;
		return new Color(vector2.x, vector2.y, vector2.z, 1f);
	}
}
