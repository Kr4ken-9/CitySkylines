﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class OptionsGameplayPanel : UICustomControl
{
	public bool edgeScrolling
	{
		get
		{
			return this.m_EdgeScrolling;
		}
		set
		{
			this.m_EdgeScrolling.set_value(value);
		}
	}

	public bool invertYMouse
	{
		get
		{
			return this.m_InvertYMouse;
		}
		set
		{
			this.m_InvertYMouse.set_value(value);
		}
	}

	public float mouseSensitivity
	{
		get
		{
			return this.m_MouseSensitivity;
		}
		set
		{
			this.m_MouseSensitivity.set_value(value);
		}
	}

	public float mouseLightIntensity
	{
		get
		{
			return this.m_MouseLightIntensity;
		}
		set
		{
			this.m_MouseLightIntensity.set_value(value);
		}
	}

	public float edgeScrollSensitivity
	{
		get
		{
			return this.m_EdgeScrollSensitivity;
		}
		set
		{
			this.m_EdgeScrollSensitivity.set_value(value);
		}
	}

	public bool tutorialMessages
	{
		get
		{
			return this.m_TutorialMessages;
		}
		set
		{
			this.m_TutorialMessages.set_value(value);
		}
	}

	public bool autoExpandChirper
	{
		get
		{
			return this.m_AutoExpandChirper;
		}
		set
		{
			this.m_AutoExpandChirper.set_value(value);
		}
	}

	public bool enableDayNight
	{
		get
		{
			return this.m_EnableDayNight;
		}
		set
		{
			this.m_EnableDayNight.set_value(value);
			if (Singleton<SimulationManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().m_enableDayNight = value;
			}
		}
	}

	public bool enableWeather
	{
		get
		{
			return this.m_EnableWeather;
		}
		set
		{
			this.m_EnableWeather.set_value(value);
			if (Singleton<WeatherManager>.get_exists())
			{
				Singleton<WeatherManager>.get_instance().m_enableWeather = value;
			}
		}
	}

	public bool randomDisastersEnabled
	{
		get
		{
			return this.m_RandomDisastersEnabled;
		}
		set
		{
			this.m_RandomDisastersEnabled.set_value(value);
			if (Singleton<DisasterManager>.get_exists())
			{
				Singleton<DisasterManager>.get_instance().m_randomDisastersProbability = ((!value) ? 0f : this.m_RandomDisastersProbability.get_value());
			}
		}
	}

	public float randomDisastersProbability
	{
		get
		{
			return this.m_RandomDisastersProbability;
		}
		set
		{
			this.m_RandomDisastersProbability.set_value(value);
			if (Singleton<DisasterManager>.get_exists())
			{
				Singleton<DisasterManager>.get_instance().m_randomDisastersProbability = ((!this.m_RandomDisastersEnabled.get_value()) ? 0f : value);
			}
		}
	}

	public bool roadNamesVisible
	{
		get
		{
			return this.m_RoadNamesVisible;
		}
		set
		{
			this.m_RoadNamesVisible.set_value(value);
			if (Singleton<NetManager>.get_exists())
			{
				Singleton<NetManager>.get_instance().m_roadNamesVisibleSetting = value;
			}
		}
	}

	public bool fadeAwayNotifications
	{
		get
		{
			return this.m_FadeAwayNotifications;
		}
		set
		{
			this.m_FadeAwayNotifications.set_value(value);
			if (Singleton<NotificationManager>.get_exists())
			{
				Singleton<NotificationManager>.get_instance().FadeAwayNotifications = value;
			}
		}
	}

	public bool autoSave
	{
		get
		{
			return this.m_AutoSave;
		}
		set
		{
			this.m_AutoSave.set_value(value);
			if (Singleton<LoadingManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties != null && Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.Game)
			{
				if (value)
				{
					Singleton<LoadingManager>.get_instance().autoSaveTimer.Start();
				}
				else
				{
					Singleton<LoadingManager>.get_instance().autoSaveTimer.Stop();
				}
			}
		}
	}

	public void OnApplyAutoSaveInterval(UIComponent comp, string text)
	{
		int num;
		if (int.TryParse(text, out num))
		{
			if (num <= 0)
			{
				num = 1;
			}
			this.m_AutoSaveInterval.set_value(num);
			this.m_AutoSaveIntervalTextField.set_text(this.m_AutoSaveInterval.get_value().ToString());
			if (Singleton<LoadingManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties != null && Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.Game)
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Start();
			}
		}
		else
		{
			this.m_AutoSaveIntervalTextField.set_text(this.m_AutoSaveInterval.get_value().ToString());
		}
	}

	public bool editorAutoSave
	{
		get
		{
			return this.m_EditorAutoSave;
		}
		set
		{
			this.m_EditorAutoSave.set_value(value);
			if (Singleton<LoadingManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties != null && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Editors) != ItemClass.Availability.None)
			{
				if (value)
				{
					Singleton<LoadingManager>.get_instance().autoSaveTimer.Start();
				}
				else
				{
					Singleton<LoadingManager>.get_instance().autoSaveTimer.Stop();
				}
			}
		}
	}

	public void OnApplyEditorAutoSaveInterval(UIComponent comp, string text)
	{
		int num;
		if (int.TryParse(text, out num))
		{
			if (num <= 0)
			{
				num = 1;
			}
			this.m_EditorAutoSaveInterval.set_value(num);
			this.m_EditorAutoSaveIntervalTextField.set_text(this.m_EditorAutoSaveInterval.get_value().ToString());
			if (Singleton<LoadingManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties != null && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Editors) != ItemClass.Availability.None)
			{
				Singleton<LoadingManager>.get_instance().autoSaveTimer.Start();
			}
		}
		else
		{
			this.m_EditorAutoSaveIntervalTextField.set_text(this.m_EditorAutoSaveInterval.get_value().ToString());
		}
	}

	private void Awake()
	{
		this.m_LanguagesDropdown = base.Find<UIDropDown>("Languages");
		this.m_LanguagesDropdown.set_items(SingletonLite<LocaleManager>.get_instance().get_supportedLocales());
		this.m_LanguagesDropdown.set_selectedIndex(SingletonLite<LocaleManager>.get_instance().GetLocaleIndex(this.m_LocaleID));
		this.m_LanguagesDropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnChangedLanguage));
		this.m_AutoSaveIntervalTextField = base.Find<UITextField>("AutoSaveInterval");
		this.m_AutoSaveIntervalTextField.set_text(this.m_AutoSaveInterval.get_value().ToString());
		this.m_EditorAutoSaveIntervalTextField = base.Find<UITextField>("EditorAutoSaveInterval");
		this.m_EditorAutoSaveIntervalTextField.set_text(this.m_EditorAutoSaveInterval.get_value().ToString());
		bool isVisible = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
		base.Find("UnitsSettings").set_isVisible(isVisible);
		this.m_TemperaturesDropdown = base.Find<UIDropDown>("Temperatures");
		this.m_TemperaturesDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_TEMP_C",
			"OPTIONS_TEMP_F"
		});
		this.m_TemperaturesDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_Temperature.set_value(i);
		});
		this.m_TemperaturesDropdown.set_selectedIndex(this.m_Temperature.get_value());
		bool isVisible2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		base.Find("RandomDisastersProbability").set_isVisible(isVisible2);
		base.Find("RandomDisastersEnabled").set_isVisible(isVisible2);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			bool flag = Singleton<LoadingManager>.get_instance().m_loadingComplete && !string.IsNullOrEmpty(Singleton<SimulationManager>.get_instance().m_metaData.m_ScenarioAsset);
			base.Find("RandomDisastersProbability").set_isEnabled(!flag);
			base.Find("RandomDisastersEnabled").set_isEnabled(!flag);
		}
	}

	private void OnChangedLanguage(UIComponent comp, int sel)
	{
		base.StartCoroutine(this.ChangeLanguage(sel));
	}

	[DebuggerHidden]
	private IEnumerator ChangeLanguage(int id)
	{
		OptionsGameplayPanel.<ChangeLanguage>c__Iterator0 <ChangeLanguage>c__Iterator = new OptionsGameplayPanel.<ChangeLanguage>c__Iterator0();
		<ChangeLanguage>c__Iterator.id = id;
		<ChangeLanguage>c__Iterator.$this = this;
		return <ChangeLanguage>c__Iterator;
	}

	private SavedBool m_EdgeScrolling = new SavedBool(Settings.edgeScrolling, Settings.gameSettingsFile, DefaultSettings.edgeScrolling, true);

	private SavedBool m_InvertYMouse = new SavedBool(Settings.invertYMouse, Settings.gameSettingsFile, DefaultSettings.invertYMouse, true);

	private SavedFloat m_EdgeScrollSensitivity = new SavedFloat(Settings.edgeScrollSensitivity, Settings.gameSettingsFile, DefaultSettings.edgeScrollSensitivity, true);

	private SavedFloat m_MouseSensitivity = new SavedFloat(Settings.mouseSensitivity, Settings.gameSettingsFile, DefaultSettings.mouseSensitivity, true);

	private SavedFloat m_MouseLightIntensity = new SavedFloat(Settings.mouseLightIntensity, Settings.gameSettingsFile, DefaultSettings.mouseLightIntensity, true);

	private SavedString m_LocaleID = new SavedString(Settings.localeID, Settings.gameSettingsFile, DefaultSettings.localeID, true);

	private SavedBool m_TutorialMessages = new SavedBool(Settings.tutorialMessages, Settings.gameSettingsFile, DefaultSettings.tutorialMessages, true);

	private SavedBool m_AutoExpandChirper = new SavedBool(Settings.autoExpandChirper, Settings.gameSettingsFile, DefaultSettings.autoExpandChirper, true);

	private SavedBool m_EnableDayNight = new SavedBool(Settings.enableDayNight, Settings.gameSettingsFile, DefaultSettings.enableDayNight, true);

	private SavedBool m_EnableWeather = new SavedBool(Settings.enableWeather, Settings.gameSettingsFile, DefaultSettings.enableWeather, true);

	private SavedBool m_RandomDisastersEnabled = new SavedBool(Settings.randomDisastersEnabled, Settings.gameSettingsFile, DefaultSettings.randomDisastersEnabled, true);

	private SavedFloat m_RandomDisastersProbability = new SavedFloat(Settings.randomDisastersProbability, Settings.gameSettingsFile, DefaultSettings.randomDisastersProbability, true);

	private SavedBool m_RoadNamesVisible = new SavedBool(Settings.roadNamesVisible, Settings.gameSettingsFile, DefaultSettings.roadNamesVisible, true);

	private SavedBool m_FadeAwayNotifications = new SavedBool(Settings.fadeAwayNotifications, Settings.gameSettingsFile, DefaultSettings.fadeAwayNotifications, true);

	private SavedInt m_Temperature = new SavedInt(Settings.temperatureUnit, Settings.gameSettingsFile, DefaultSettings.temperatureUnit, true);

	private UIDropDown m_LanguagesDropdown;

	private UIDropDown m_TemperaturesDropdown;

	private SavedBool m_AutoSave = new SavedBool(Settings.autoSave, Settings.gameSettingsFile, DefaultSettings.autoSave, true);

	private SavedInt m_AutoSaveInterval = new SavedInt(Settings.autoSaveInterval, Settings.gameSettingsFile, DefaultSettings.autoSaveInterval, true);

	private SavedBool m_EditorAutoSave = new SavedBool(Settings.editorAutoSave, Settings.gameSettingsFile, DefaultSettings.editorAutoSave, true);

	private SavedInt m_EditorAutoSaveInterval = new SavedInt(Settings.editorAutoSaveInterval, Settings.gameSettingsFile, DefaultSettings.editorAutoSaveInterval, true);

	private UITextField m_AutoSaveIntervalTextField;

	private UITextField m_EditorAutoSaveIntervalTextField;
}
