﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class HadronColliderAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.NoisePollution;
		radius1 = this.m_noiseRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Education)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Education)
		{
			mode = InfoManager.InfoMode.Education;
			subMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
		}
		else
		{
			mode = InfoManager.InfoMode.Education;
			subMode = InfoManager.SubInfoMode.WindPower;
		}
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		int num = 500;
		if (num != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (num != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.EducationUniversity, (float)num, this.m_educationRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			int num = 500;
			if (num != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (num != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.PublicTransport, (float)(-(float)num), this.m_educationRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_educationRadius * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = (productionRate * 10000 + 99) / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationUniversity, num);
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationHighSchool, num);
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.EducationElementary, num);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num2 = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num2 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num2, buildingData.m_position, this.m_noiseRadius);
		}
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		District[] expr_B4_cp_0_cp_0 = instance.m_districts.m_buffer;
		byte expr_B4_cp_0_cp_1 = district;
		expr_B4_cp_0_cp_0[(int)expr_B4_cp_0_cp_1].m_productionData.m_tempEducation1Capacity = expr_B4_cp_0_cp_0[(int)expr_B4_cp_0_cp_1].m_productionData.m_tempEducation1Capacity + 1000000u;
		District[] expr_DB_cp_0_cp_0 = instance.m_districts.m_buffer;
		byte expr_DB_cp_0_cp_1 = district;
		expr_DB_cp_0_cp_0[(int)expr_DB_cp_0_cp_1].m_productionData.m_tempEducation2Capacity = expr_DB_cp_0_cp_0[(int)expr_DB_cp_0_cp_1].m_productionData.m_tempEducation2Capacity + 1000000u;
		District[] expr_102_cp_0_cp_0 = instance.m_districts.m_buffer;
		byte expr_102_cp_0_cp_1 = district;
		expr_102_cp_0_cp_0[(int)expr_102_cp_0_cp_1].m_productionData.m_tempEducation3Capacity = expr_102_cp_0_cp_0[(int)expr_102_cp_0_cp_1].m_productionData.m_tempEducation3Capacity + 1000000u;
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool IsWonder()
	{
		return true;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			string.Empty
		}));
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 100;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 150f;

	[CustomizableProperty("Education Radius")]
	public float m_educationRadius = 1500f;
}
