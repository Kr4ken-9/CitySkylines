﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class AssetImporterAssetTemplate : AssetImporterPanelBase
{
	public AssetImporterAssetTemplate.TemplateMode templateMode
	{
		get
		{
			return this.m_TemplateMode;
		}
		internal set
		{
			this.m_TemplateMode = value;
		}
	}

	public bool hasPreview
	{
		get
		{
			return this.m_TemplateMode != AssetImporterAssetTemplate.TemplateMode.NoPreview;
		}
	}

	public PrefabInfo selectedPrefabInfo
	{
		get
		{
			return this.m_SelectedPrefabInfo;
		}
		internal set
		{
			this.m_SelectedPrefabInfo = value;
		}
	}

	public bool loadExistingDecoration
	{
		get
		{
			return this.m_UseExistingDecoration.get_isChecked();
		}
	}

	public void Reset()
	{
		this.m_Selected = null;
		this.m_SelectedPrefabInfo = null;
		this.m_TemplateMode = AssetImporterAssetTemplate.TemplateMode.Default;
		if (this.m_ContinueButton != null)
		{
			this.m_ContinueButton.set_isEnabled(false);
		}
		if (this.m_SelectButton != null)
		{
			this.m_SelectButton.set_isEnabled(false);
		}
	}

	private void Awake()
	{
		this.m_LoadingProgress = base.Find<UISprite>("ProgressSprite");
		this.m_LoadingProgress.Hide();
		this.m_Groups = base.Find<UITabstrip>("Groups");
		this.m_UseExistingDecoration = base.Find<UICheckBox>("PrevDecoration");
		this.m_ContinueButton = base.Find<UIButton>("Continue");
		if (this.m_ContinueButton != null)
		{
			this.m_ContinueButton.add_eventClick(new MouseEventHandler(this.OnContinue));
		}
		this.m_BackButton = base.Find<UIButton>("Back");
		if (this.m_BackButton != null)
		{
			this.m_BackButton.add_eventClick(new MouseEventHandler(this.OnBack));
		}
		this.m_SelectButton = base.Find<UIButton>("Select");
		if (this.m_SelectButton != null)
		{
			this.m_SelectButton.add_eventClick(new MouseEventHandler(this.OnSelect));
		}
		this.m_CancelButton = base.Find<UIButton>("Cancel");
		if (this.m_CancelButton != null)
		{
			this.m_CancelButton.add_eventClick(new MouseEventHandler(this.OnCancel));
		}
		this.Reset();
	}

	private void OnEnable()
	{
		this.m_NameTooltip = UIView.Find<UILabel>("NameTooltip");
		if (this.m_NameTooltip != null)
		{
			this.m_NameTooltip.set_opacity(0f);
			this.m_NameTooltip.set_isVisible(false);
			this.m_NameTooltip.BringToFront();
		}
	}

	private void OnDisable()
	{
	}

	private void OnSelect(UIComponent comp, UIMouseEventParameter p)
	{
		this.ReferenceCallback(this.m_SelectedPrefabInfo);
	}

	private void OnCancel(UIComponent comp, UIMouseEventParameter p)
	{
		this.ReferenceCallback(null);
	}

	private void OnContinue(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.selectedPrefabInfo is NetInfo)
		{
			ToolsModifierControl.toolController.m_editPrefabInfo = AssetEditorRoadUtils.InstantiatePrefab((NetInfo)this.selectedPrefabInfo);
			ToolsModifierControl.toolController.m_templatePrefabInfo = this.selectedPrefabInfo;
			base.owner.Complete();
		}
		else
		{
			base.owner.assetImportPanel.OnEnter();
			base.owner.GoTo(base.owner.assetImportPanel);
		}
	}

	private void OnBack(UIComponent comp, UIMouseEventParameter p)
	{
		this.Reset();
		base.owner.GoBack();
	}

	public void RefreshWithFilter(AssetImporterAssetTemplate.Filter filter)
	{
		base.StopAllCoroutines();
		if (filter == AssetImporterAssetTemplate.Filter.Buildings)
		{
			base.StartCoroutine(this.RefreshBuildingsCoroutine());
		}
		else
		{
			base.StartCoroutine(this.RefreshCoroutine(filter));
		}
	}

	private bool IsZoningService(ItemClass.Service service, out ItemClass.SubService[] subServices, AssetImporterAssetTemplate.Filter filter)
	{
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC);
		bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC);
		bool flag3 = service == ItemClass.Service.Residential;
		bool flag4 = service == ItemClass.Service.Commercial;
		bool flag5 = service == ItemClass.Service.Industrial;
		bool flag6 = service == ItemClass.Service.Office;
		if (flag3)
		{
			if (flag2 || filter == AssetImporterAssetTemplate.Filter.Vehicles)
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.ResidentialLow,
					ItemClass.SubService.ResidentialHigh,
					ItemClass.SubService.ResidentialLowEco,
					ItemClass.SubService.ResidentialHighEco
				};
			}
			else
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.ResidentialLow,
					ItemClass.SubService.ResidentialHigh
				};
			}
		}
		else if (flag4)
		{
			if (flag && flag2)
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.CommercialLow,
					ItemClass.SubService.CommercialHigh,
					ItemClass.SubService.CommercialLeisure,
					ItemClass.SubService.CommercialTourist,
					ItemClass.SubService.CommercialEco
				};
			}
			else if (flag)
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.CommercialLow,
					ItemClass.SubService.CommercialHigh,
					ItemClass.SubService.CommercialLeisure,
					ItemClass.SubService.CommercialTourist
				};
			}
			else if (flag2)
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.CommercialLow,
					ItemClass.SubService.CommercialHigh,
					ItemClass.SubService.CommercialEco
				};
			}
			else
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.CommercialLow,
					ItemClass.SubService.CommercialHigh
				};
			}
		}
		else if (flag5)
		{
			subServices = new ItemClass.SubService[]
			{
				ItemClass.SubService.IndustrialGeneric,
				ItemClass.SubService.IndustrialFarming,
				ItemClass.SubService.IndustrialForestry,
				ItemClass.SubService.IndustrialOil,
				ItemClass.SubService.IndustrialOre
			};
		}
		else if (flag6)
		{
			if (flag2)
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.OfficeGeneric,
					ItemClass.SubService.OfficeHightech
				};
			}
			else
			{
				subServices = new ItemClass.SubService[]
				{
					ItemClass.SubService.OfficeGeneric
				};
			}
		}
		else
		{
			subServices = new ItemClass.SubService[1];
		}
		return flag3 || flag4 || flag5 || flag6;
	}

	public void CleanPanel()
	{
		UITemplateManager.ClearInstances(AssetImporterAssetTemplate.kImporterItemTemplate, false);
		UITemplateManager.ClearInstances(AssetImporterAssetTemplate.kImporterContainerTemplate, false);
		UITemplateManager.ClearInstances(AssetImporterAssetTemplate.kImporterGroupTemplate, false);
		UITemplateManager.ClearInstances(AssetImporterAssetTemplate.kImporterGroupSeparatorTemplate, false);
	}

	private void UpdateProgress()
	{
		this.m_LoadingProgress.Show();
		this.m_LoadingProgress.BringToFront();
		AssetImporterAssetTemplate.FitChildren(this.m_Groups);
	}

	private static void FitChildren(UIComponent comp)
	{
		Vector2 size = default(Vector2);
		foreach (UIComponent current in comp.get_components())
		{
			Vector2 vector = current.get_relativePosition() + current.get_size();
			if (vector.x > size.x)
			{
				size.x = vector.x;
			}
			if (vector.y > size.y)
			{
				size.y = vector.y;
			}
		}
		comp.set_size(size);
	}

	private void Update()
	{
		if (this.m_LoadingProgress.get_isVisible())
		{
			this.m_LoadingProgress.get_transform().Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
		}
		if (!this.m_Initialized && Singleton<LoadingManager>.get_exists())
		{
			if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				return;
			}
			this.m_Initialized = true;
		}
		this.ShowSelection();
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (this.m_ContinueButton != null)
			{
				this.m_ContinueButton.set_isEnabled(this.m_Selected != null && this.m_SelectedPrefabInfo != null);
			}
			if (this.m_SelectButton != null)
			{
				this.m_SelectButton.set_isEnabled(this.m_Selected != null && this.m_SelectedPrefabInfo != null);
			}
		}
	}

	private List<PrefabInfo> GetInfos<T>() where T : PrefabInfo
	{
		List<PrefabInfo> list = new List<PrefabInfo>();
		uint num = 0u;
		while ((ulong)num < (ulong)((long)PrefabCollection<T>.LoadedCount()))
		{
			PrefabInfo prefabInfo = PrefabCollection<T>.GetLoaded(num);
			if (prefabInfo != null)
			{
				TreeInfo treeInfo = prefabInfo as TreeInfo;
				VehicleInfo vehicleInfo = prefabInfo as VehicleInfo;
				PropInfo propInfo = prefabInfo as PropInfo;
				CitizenInfo citizenInfo = prefabInfo as CitizenInfo;
				NetInfo netInfo = prefabInfo as NetInfo;
				if ((treeInfo != null && (treeInfo.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None) || (vehicleInfo != null && (vehicleInfo.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None) || (propInfo != null && (propInfo.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None) || (citizenInfo != null && (citizenInfo.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None) || (netInfo != null && (netInfo.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None))
				{
					list.Add(prefabInfo);
				}
			}
			num += 1u;
		}
		return list;
	}

	private List<PrefabInfo> GetPillars()
	{
		List<PrefabInfo> list = new List<PrefabInfo>();
		uint num = 0u;
		while ((ulong)num < (ulong)((long)PrefabCollection<BuildingInfo>.LoadedCount()))
		{
			BuildingInfo loaded = PrefabCollection<BuildingInfo>.GetLoaded(num);
			if (loaded != null && (loaded.m_availableIn & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None && loaded.m_AssetEditorPillarTemplate)
			{
				list.Add(loaded);
			}
			num += 1u;
		}
		return list;
	}

	private static bool IsAfterDarkDLCType(PrefabInfo info)
	{
		VehicleInfo.VehicleType vehicleType = VehicleInfo.VehicleType.None;
		if (info is VehicleInfo)
		{
			vehicleType = ((VehicleInfo)info).m_vehicleType;
		}
		if (info is BuildingInfo)
		{
			BuildingInfo buildingInfo = (BuildingInfo)info;
			if (buildingInfo.m_paths != null)
			{
				for (int i = 0; i < buildingInfo.m_paths.Length; i++)
				{
					for (int j = 0; j < buildingInfo.m_paths[i].m_netInfo.m_lanes.Length; j++)
					{
						vehicleType |= buildingInfo.m_paths[i].m_netInfo.m_lanes[j].m_vehicleType;
					}
				}
			}
		}
		return AssetImporterAssetTemplate.IsAfterDarkDLCType(info.GetService(), info.GetSubService(), vehicleType);
	}

	private static bool IsSnowfallDLCType(PrefabInfo info)
	{
		VehicleInfo.VehicleType vehicleType = VehicleInfo.VehicleType.None;
		if (info is VehicleInfo)
		{
			vehicleType = ((VehicleInfo)info).m_vehicleType;
		}
		if (info is BuildingInfo)
		{
			BuildingInfo buildingInfo = (BuildingInfo)info;
			if (buildingInfo.m_paths != null)
			{
				for (int i = 0; i < buildingInfo.m_paths.Length; i++)
				{
					for (int j = 0; j < buildingInfo.m_paths[i].m_netInfo.m_lanes.Length; j++)
					{
						vehicleType |= buildingInfo.m_paths[i].m_netInfo.m_lanes[j].m_vehicleType;
					}
				}
			}
		}
		return AssetImporterAssetTemplate.IsSnowfallDLCType(info.GetService(), info.GetSubService(), vehicleType);
	}

	public static bool IsAfterDarkDLCType(ItemClass.Service service, ItemClass.SubService subService, VehicleInfo.VehicleType vtype)
	{
		return subService == ItemClass.SubService.PublicTransportTaxi || subService == ItemClass.SubService.CommercialLeisure || subService == ItemClass.SubService.CommercialTourist || (vtype & VehicleInfo.VehicleType.Bicycle) != VehicleInfo.VehicleType.None;
	}

	public static bool IsSnowfallDLCType(ItemClass.Service service, ItemClass.SubService subService, VehicleInfo.VehicleType vtype)
	{
		return subService == ItemClass.SubService.PublicTransportTram || (vtype & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None;
	}

	public static SteamHelper.DLC_BitMask GetAssetDLCMask(PrefabInfo info)
	{
		SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)0;
		if (AssetImporterAssetTemplate.IsAfterDarkDLCType(info))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.AfterDarkDLC;
		}
		if (AssetImporterAssetTemplate.IsSnowfallDLCType(info))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.SnowFallDLC;
		}
		return dLC_BitMask | info.m_dlcRequired;
	}

	public static SteamHelper.DLC_BitMask GetAssetDLCMask(CustomAssetMetaData meta)
	{
		SteamHelper.DLC_BitMask dLC_BitMask = (SteamHelper.DLC_BitMask)0;
		if (AssetImporterAssetTemplate.IsAfterDarkDLCType(meta.service, meta.subService, meta.vehicleType))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.AfterDarkDLC;
		}
		if (AssetImporterAssetTemplate.IsSnowfallDLCType(meta.service, meta.subService, meta.vehicleType))
		{
			dLC_BitMask |= SteamHelper.DLC_BitMask.SnowFallDLC;
		}
		return dLC_BitMask | meta.dlcMask;
	}

	private void SetDLCSprites(UIComponent comp, SteamHelper.DLC_BitMask dlcmask)
	{
		UISprite uISprite = comp.Find<UISprite>("DLCSprite");
		UISprite uISprite2 = comp.Find<UISprite>("DLCSprite2");
		List<string> icons = DLCIconUtils.GetIcons(dlcmask, 2);
		if (icons.Count == 0)
		{
			uISprite.set_spriteName(null);
			uISprite2.set_spriteName(null);
		}
		else if (icons.Count == 1)
		{
			uISprite.set_spriteName(icons[0]);
			uISprite2.set_spriteName(null);
		}
		else
		{
			uISprite.set_spriteName(icons[1]);
			uISprite2.set_spriteName(icons[0]);
		}
		uISprite.set_isVisible(!string.IsNullOrEmpty(uISprite.get_spriteName()));
		uISprite2.set_isVisible(!string.IsNullOrEmpty(uISprite2.get_spriteName()));
	}

	[DebuggerHidden]
	public IEnumerator RefreshCoroutine(AssetImporterAssetTemplate.Filter filter)
	{
		AssetImporterAssetTemplate.<RefreshCoroutine>c__Iterator0 <RefreshCoroutine>c__Iterator = new AssetImporterAssetTemplate.<RefreshCoroutine>c__Iterator0();
		<RefreshCoroutine>c__Iterator.filter = filter;
		<RefreshCoroutine>c__Iterator.$this = this;
		return <RefreshCoroutine>c__Iterator;
	}

	private bool CheckLaneProp(PropInfo info, HashSet<PropInfo> traversed = null)
	{
		if (traversed == null)
		{
			traversed = new HashSet<PropInfo>();
		}
		if (traversed.Contains(info))
		{
			return !info.m_requireHeightMap || info.m_requireWaterMap;
		}
		traversed.Add(info);
		if (info.m_requireHeightMap && !info.m_requireWaterMap)
		{
			return false;
		}
		if (info.m_variations != null)
		{
			for (int i = 0; i < info.m_variations.Length; i++)
			{
				if (info.m_variations[i].m_finalProp != null && !this.CheckLaneProp(info.m_variations[i].m_finalProp, traversed))
				{
					return false;
				}
			}
		}
		return true;
	}

	[DebuggerHidden]
	public IEnumerator RefreshBuildingsCoroutine()
	{
		AssetImporterAssetTemplate.<RefreshBuildingsCoroutine>c__Iterator1 <RefreshBuildingsCoroutine>c__Iterator = new AssetImporterAssetTemplate.<RefreshBuildingsCoroutine>c__Iterator1();
		<RefreshBuildingsCoroutine>c__Iterator.$this = this;
		return <RefreshBuildingsCoroutine>c__Iterator;
	}

	public void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != null && p.get_source().get_objectUserData() != null && p.get_source().get_objectUserData() != null)
		{
			if (this.m_Selected != null)
			{
				this.m_Selected.set_state(0);
			}
			this.m_Selected = (p.get_source() as UIButton);
			this.m_SelectedPrefabInfo = (p.get_source().get_objectUserData() as PrefabInfo);
			if (this.m_Selected != null && Singleton<AudioManager>.get_exists() && this.m_SelectionSound != null)
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_SelectionSound, 1f);
			}
			if (this.m_ContinueButton != null)
			{
				this.m_ContinueButton.set_isEnabled(this.m_Selected != null && this.m_SelectedPrefabInfo != null);
			}
			else if (this.m_SelectButton != null)
			{
				this.m_SelectButton.set_isEnabled(this.m_Selected != null && this.m_SelectedPrefabInfo != null);
			}
		}
	}

	private void OnLostFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_lostFocus() == this.m_Selected)
		{
			this.ShowSelection();
		}
	}

	private void ShowSelection()
	{
		if (this.m_Selected != null && !this.m_Selected.get_containsMouse())
		{
			this.m_Selected.set_state(1);
		}
	}

	protected void OnMouseUp(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != this.m_Selected && p.get_source() != UIInput.get_hoveredComponent())
		{
			p.get_source().Unfocus();
		}
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				if (this.m_BackButton != null)
				{
					this.m_BackButton.SimulateClick();
				}
				else
				{
					this.m_CancelButton.SimulateClick();
				}
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.m_ContinueButton.SimulateClick();
				p.Use();
			}
		}
	}

	public AudioClip m_SelectionSound;

	public float m_RotationSpeed = 280f;

	private UIButton m_ContinueButton;

	private UIButton m_BackButton;

	private UIButton m_SelectButton;

	private UIButton m_CancelButton;

	private UITabstrip m_Groups;

	private UISprite m_LoadingProgress;

	private UICheckBox m_UseExistingDecoration;

	protected UIButton m_Selected;

	protected PrefabInfo m_SelectedPrefabInfo;

	protected AssetImporterAssetTemplate.TemplateMode m_TemplateMode;

	private bool m_Initialized;

	private static readonly string kImporterItemTemplate = "ImporterItemTemplate";

	private static readonly string kImporterContainerTemplate = "ImporterTabPanelTemplate";

	private static readonly string kImporterGroupTemplate = "ImporterGroupTemplate";

	private static readonly string kImporterGroupSeparatorTemplate = "ImporterGroupSeparatorTemplate";

	private static readonly PositionData<ItemClass.Service>[] kServices = Utils.GetOrderedEnumData<ItemClass.Service>("AssetImporter");

	public AssetImporterAssetTemplate.ReferenceCallbackDelegate ReferenceCallback;

	private UILabel m_NameTooltip;

	public enum Filter
	{
		Buildings,
		Vehicles,
		Citizens,
		CarTrailers,
		TrainCars,
		TramTrailer,
		HelicopterTrailer,
		Props,
		Trees,
		CustomProps,
		CustomTrees,
		Roads,
		Pillars,
		LaneProps,
		Nothing
	}

	public enum TemplateMode
	{
		Default,
		NoPreview
	}

	public delegate void ReferenceCallbackDelegate(PrefabInfo reference);
}
