﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class WeatherManager : SimulationManagerBase<WeatherManager, WeatherProperties>, ISimulationManager, IRenderableManager, ITerrainManager
{
	public bool WindMapVisible
	{
		get
		{
			return this.m_windMapVisible;
		}
		set
		{
			this.m_windMapVisible = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_windGrid = new WeatherManager.WindCell[16384];
		Vector4 vector;
		vector.z = 5.787037E-05f;
		vector.x = 0.5f;
		vector.y = 1024f;
		vector.w = 0.0078125f;
		this.m_windTexture = new Texture2D(128, 128, 3, false, true);
		this.m_windTexture.set_filterMode(0);
		this.m_windTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_WindTexture", this.m_windTexture);
		Shader.SetGlobalVector("_WindMapping", vector);
		SavedBool savedBool = new SavedBool(Settings.enableWeather, Settings.gameSettingsFile, true);
		this.m_enableWeather = savedBool.get_value();
		this.m_windDirection = 45f;
		this.m_targetDirection = 45f;
		this.m_directionSpeed = 0f;
		this.m_currentTemperature = 20f;
		this.m_targetTemperature = 20f;
		this.m_temperatureSpeed = 0f;
		this.m_currentRain = 0f;
		this.m_targetRain = 0f;
		this.m_currentFog = 0f;
		this.m_targetFog = 0f;
		this.m_groundWetness = 0f;
		this.m_lightningQueue = new FastList<WeatherManager.LightningStrike>();
		this.m_modifiedX1 = new int[128];
		this.m_modifiedX2 = new int[128];
		for (int i = 0; i < 128; i++)
		{
			this.m_modifiedX1[i] = 0;
			this.m_modifiedX2[i] = 127;
		}
		this.m_modified = true;
		this.m_materialBlock = new MaterialPropertyBlock();
	}

	public override void InitializeProperties(WeatherProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_properties.m_windZone != null)
		{
			GameObject gameObject = Object.Instantiate<GameObject>(this.m_properties.m_windZone.get_gameObject());
			gameObject.set_name("Wind Zone");
			this.m_windZone = gameObject.get_transform();
		}
	}

	public override void DestroyProperties(WeatherProperties properties)
	{
		if (this.m_properties == properties && this.m_windZone != null)
		{
			Object.Destroy(this.m_windZone.get_gameObject());
			this.m_windZone = null;
		}
		base.DestroyProperties(properties);
	}

	private void OnDestroy()
	{
		if (this.m_windTexture != null)
		{
			Object.Destroy(this.m_windTexture);
			this.m_windTexture = null;
		}
	}

	private void Update()
	{
		if (this.m_windZone != null)
		{
			this.m_windZone.set_rotation(Quaternion.AngleAxis(this.m_windDirection + 180f, Vector3.get_up()));
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		float num = this.m_windDirection * 0.0174532924f;
		Vector4 vector;
		vector..ctor(Mathf.Sin(num), 0f, Mathf.Cos(num), this.GetWindSpeedFactor());
		Vector4 vector2;
		vector2..ctor(this.m_currentTemperature, this.m_currentRain, this.m_currentFog, this.m_groundWetness);
		Shader.SetGlobalVector("_WindDirection", vector);
		Shader.SetGlobalVector("_WeatherParams", vector2);
		if (this.m_properties != null && this.m_properties.m_lightningMesh != null)
		{
			float num2 = 0f;
			uint referenceFrameIndex = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex;
			for (int i = 0; i < this.m_lightningQueue.m_size; i++)
			{
				uint startFrame = this.m_lightningQueue.m_buffer[i].m_startFrame;
				if (referenceFrameIndex >= startFrame && referenceFrameIndex < startFrame + 30u)
				{
					Randomizer randomizer;
					randomizer..ctor(startFrame ^ referenceFrameIndex / 3u);
					float num3 = (float)randomizer.Int32(0, 100) * 0.01f;
					if (referenceFrameIndex - startFrame >= 2u && referenceFrameIndex - startFrame <= 4u)
					{
						num3 = Mathf.Max(0.75f, num3);
					}
					num2 += num3;
					float num4 = Mathf.Clamp01((referenceFrameIndex - startFrame) / 20f);
					float num5 = 6.75f * num4 * (num4 * (num4 - 2f) + 1f);
					num5 *= num5;
					num3 = Mathf.Lerp(num3, num5, this.m_flashingReduction);
					Vector3 vector3 = this.m_lightningQueue.m_buffer[i].m_position;
					randomizer..ctor(startFrame ^ referenceFrameIndex / 30u);
					if (randomizer.Int32(2u) == 0)
					{
						vector3.x += (float)randomizer.Int32(-1000, 1000) * 1E-05f;
						vector3.z += (float)randomizer.Int32(-1000, 1000) * 1E-05f;
					}
					Quaternion rotation = this.m_lightningQueue.m_buffer[i].m_rotation;
					Color color = this.m_properties.m_lightningMaterial.get_color();
					this.m_materialBlock.Clear();
					this.m_materialBlock.SetColor("_Color", color * num3);
					Graphics.DrawMesh(this.m_properties.m_lightningMesh, vector3, rotation, this.m_properties.m_lightningMaterial, 0, null, 0, this.m_materialBlock, false, false);
					if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
					{
						vector3 += rotation * new Vector3(0f, 5f, 0f);
						Singleton<RenderManager>.get_instance().lightSystem.DrawLight(2, vector3, Vector3.get_up(), Vector3.get_zero(), color, num3 * 8f, 300f, 0f, 0f, true);
						InstanceID empty = InstanceID.Empty;
						empty.Lightning = (byte)i;
						vector3 += rotation * new Vector3(0f, 500f, 0f);
						num3 *= 1f - this.m_flashingReduction * 0.75f * (1f - this.GetSunIntensityFactor());
						num3 = Mathf.Clamp01(num3);
						LightData data = new LightData(empty, vector3, color, num3 * 8f, 10000f, 50f);
						Singleton<RenderManager>.get_instance().lightSystem.DrawLight(data);
					}
				}
			}
			if (Singleton<SimulationManager>.get_instance().FinalSimulationSpeed != 0)
			{
				float num6 = Mathf.Abs(num2 - this.m_lastLightningIntensity);
				this.m_flashingReduction = Mathf.Clamp01(this.m_flashingReduction + num6 * 0.1f - Time.get_deltaTime() * 0.3f);
				this.m_lastLightningIntensity = num2;
			}
		}
		if (this.m_modified && this.m_windMapVisible)
		{
			this.m_modified = false;
			this.UpdateTexture();
		}
	}

	private void UpdateTexture()
	{
		for (int i = 0; i < 128; i++)
		{
			if (this.m_modifiedX2[i] >= this.m_modifiedX1[i])
			{
				while (!Monitor.TryEnter(this.m_windGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				int num;
				int num2;
				try
				{
					num = this.m_modifiedX1[i];
					num2 = this.m_modifiedX2[i];
					this.m_modifiedX1[i] = 10000;
					this.m_modifiedX2[i] = -10000;
				}
				finally
				{
					Monitor.Exit(this.m_windGrid);
				}
				for (int j = num; j <= num2; j++)
				{
					WeatherManager.WindCell windCell = this.m_windGrid[i * 128 + j];
					Color color;
					color.r = (float)(windCell.m_totalHeight >> 8 & 255) * 0.003921569f;
					color.g = (float)(windCell.m_totalHeight & 255) * 0.003921569f;
					color.b = 0f;
					color.a = 0f;
					this.m_windTexture.SetPixel(j, i, color);
				}
			}
		}
		this.m_windTexture.Apply();
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ)
	{
		minX = Mathf.Max(0, minX - 1);
		minZ = Mathf.Max(0, minZ - 1);
		maxX = Mathf.Min(127, maxX + 1);
		maxZ = Mathf.Min(127, maxZ + 1);
		while (!Monitor.TryEnter(this.m_windGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = minZ; i <= maxZ; i++)
			{
				this.m_modifiedX1[i] = Mathf.Min(this.m_modifiedX1[i], minX);
				this.m_modifiedX2[i] = Mathf.Max(this.m_modifiedX2[i], maxX);
			}
			this.m_modified = true;
		}
		finally
		{
			Monitor.Exit(this.m_windGrid);
		}
	}

	public float SampleWindSpeed(Vector3 pos, bool ignoreWeather)
	{
		int num = Mathf.RoundToInt((pos.x / 135f + 64f - 0.5f) * 256f);
		int num2 = Mathf.RoundToInt((pos.z / 135f + 64f - 0.5f) * 256f);
		int num3 = Mathf.Clamp(num >> 8, 0, 127);
		int num4 = Mathf.Clamp(num2 >> 8, 0, 127);
		int num5 = Mathf.Min(num3 + 1, 127);
		int num6 = Mathf.Min(num4 + 1, 127);
		int totalHeight = (int)this.m_windGrid[num4 * 128 + num3].m_totalHeight;
		int totalHeight2 = (int)this.m_windGrid[num4 * 128 + num5].m_totalHeight;
		int totalHeight3 = (int)this.m_windGrid[num6 * 128 + num3].m_totalHeight;
		int totalHeight4 = (int)this.m_windGrid[num6 * 128 + num5].m_totalHeight;
		int num7 = totalHeight + ((totalHeight2 - totalHeight) * (num & 255) >> 8);
		int num8 = totalHeight3 + ((totalHeight4 - totalHeight3) * (num & 255) >> 8);
		int num9 = num7 + ((num8 - num7) * (num2 & 255) >> 8);
		float num10 = pos.y - (float)num9 * 0.015625f;
		float num11 = num10 * 0.02f + 1f;
		if (ignoreWeather)
		{
			return Mathf.Clamp01(num11);
		}
		return Mathf.Clamp01(num11 * this.GetWindSpeedFactor());
	}

	private float GetWindSpeedFactor()
	{
		return 1f + this.m_currentRain * 0.5f - this.m_currentFog * 0.5f;
	}

	public float GetWindSpeed(Vector3 pos)
	{
		int num = Mathf.Clamp(Mathf.FloorToInt(pos.x / 135f + 64f - 0.5f), 0, 127);
		int num2 = Mathf.Clamp(Mathf.FloorToInt(pos.z / 135f + 64f - 0.5f), 0, 127);
		int totalHeight = (int)this.m_windGrid[num2 * 128 + num].m_totalHeight;
		float num3 = pos.y - (float)totalHeight * 0.015625f;
		return Mathf.Clamp(num3 * 0.02f + 1f, 0f, 2f);
	}

	public float GetWindSpeed(Vector2 pos)
	{
		int num = Mathf.Clamp(Mathf.FloorToInt(pos.x / 135f + 64f - 0.5f), 0, 127);
		int num2 = Mathf.Clamp(Mathf.FloorToInt(pos.y / 135f + 64f - 0.5f), 0, 127);
		int selfHeight = (int)this.m_windGrid[num2 * 128 + num].m_selfHeight;
		int totalHeight = (int)this.m_windGrid[num2 * 128 + num].m_totalHeight;
		float num3 = (float)(selfHeight - totalHeight) * 0.015625f;
		return Mathf.Clamp(num3 * 0.02f + 1f, 0f, 2f);
	}

	private float GetSunIntensityFactor()
	{
		float num = Singleton<SimulationManager>.get_instance().m_dayTimeFrame * SimulationManager.DAYTIME_FRAME_TO_HOUR;
		float num2 = Mathf.Clamp01(0.5f + Mathf.Min(num - SimulationManager.SUNRISE_HOUR, SimulationManager.SUNSET_HOUR - num) * 2f);
		return num2 * (1f - Mathf.Max(this.m_currentRain, this.m_currentFog) * 0.5f);
	}

	public float SampleSunIntensity(Vector3 pos, bool ignoreWeather)
	{
		if (ignoreWeather)
		{
			return 1f;
		}
		return Mathf.Clamp01(this.GetSunIntensityFactor());
	}

	public float SampleRainIntensity(Vector3 pos, bool ignoreWeather)
	{
		if (ignoreWeather)
		{
			return 1f;
		}
		return this.m_currentRain;
	}

	public float SampleFogIntensity(Vector3 pos, bool ignoreWeather)
	{
		if (ignoreWeather)
		{
			return 1f;
		}
		return this.m_currentFog;
	}

	public float SampleTemperature(Vector3 pos, bool ignoreWeather)
	{
		if (ignoreWeather)
		{
			return 1f;
		}
		return this.m_currentTemperature;
	}

	private float GetCloudCoverageFactor()
	{
		return Mathf.Max(Mathf.Max(this.m_currentRain * 4f, this.m_currentFog * 0.5f), Mathf.Max(this.m_currentCloud, this.m_groundWetness * 0.5f));
	}

	public float SampleCloudCoverage(Vector3 pos, bool ignoreWeather)
	{
		if (ignoreWeather)
		{
			return 0f;
		}
		return Mathf.Clamp01(this.GetCloudCoverageFactor());
	}

	public float GetRainbowVisibility()
	{
		return this.m_currentRainbow * Mathf.Clamp01(this.GetSunIntensityFactor());
	}

	public float GetNorthernLightsVisibility()
	{
		return this.m_currentNorthernLights * Mathf.Clamp01(1f - this.GetSunIntensityFactor() * 2f);
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			float num = Mathf.Abs(Mathf.DeltaAngle(this.m_targetDirection, this.m_windDirection) * 0.001f);
			this.m_directionSpeed = Mathf.Min(this.m_directionSpeed + 0.001f, num);
			this.m_windDirection = Mathf.MoveTowardsAngle(this.m_windDirection, this.m_targetDirection, this.m_directionSpeed);
			this.m_windDirection = Mathf.DeltaAngle(0f, this.m_windDirection);
			if (num < 0.0002f)
			{
				this.m_targetDirection = (float)instance.m_randomizer.Int32(10000u) * 0.036f;
			}
			float num2 = (!this.m_enableWeather) ? 0f : 1f;
			if (this.m_forceWeatherOn != 0f)
			{
				this.m_forceWeatherOn = Mathf.Max(0f, this.m_forceWeatherOn - 0.001f);
				num2 = Mathf.Max(num2, this.m_forceWeatherOn);
			}
			if (num2 >= 1f)
			{
				ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
				if ((mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					if (this.m_targetRain > this.m_currentRain)
					{
						float currentRain = this.m_currentRain;
						this.m_currentRain = Mathf.Min(this.m_targetRain, this.m_currentRain + 0.0002f);
						if (this.m_currentRain >= 0.2f && currentRain < 0.2f && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
						{
							if (this.m_properties.m_rainIsSnow)
							{
								ThreadHelper.get_dispatcher().Dispatch(delegate
								{
									if (!PlatformService.get_achievements().get_Item("GetYourSnowshoesReady").get_achieved())
									{
										PlatformService.get_achievements().get_Item("GetYourSnowshoesReady").Unlock();
									}
								});
							}
							else
							{
								ThreadHelper.get_dispatcher().Dispatch(delegate
								{
									if (!PlatformService.get_achievements().get_Item("SingingInThe").get_achieved())
									{
										PlatformService.get_achievements().get_Item("SingingInThe").Unlock();
									}
								});
							}
						}
					}
					else if (this.m_targetRain < this.m_currentRain)
					{
						this.m_currentRain = Mathf.Max(this.m_targetRain, this.m_currentRain - 0.0002f);
					}
					else if (instance.m_randomizer.Int32(20000u) == 0)
					{
						if (this.m_currentFog == 0f && this.m_targetFog == 0f)
						{
							int num3 = (!instance.m_isNightTime) ? this.m_properties.m_rainProbabilityDay : this.m_properties.m_rainProbabilityNight;
							if (instance.m_randomizer.Int32(100u) < num3)
							{
								this.m_targetRain = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
							}
							else
							{
								this.m_targetRain = 0f;
							}
						}
						else
						{
							this.m_targetRain = 0f;
						}
					}
					if (this.m_targetFog > this.m_currentFog)
					{
						float currentFog = this.m_currentFog;
						this.m_currentFog = Mathf.Min(this.m_targetFog, this.m_currentFog + 0.0002f);
						if (this.m_currentFog >= 0.2f && currentFog < 0.2f && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
						{
							ThreadHelper.get_dispatcher().Dispatch(delegate
							{
								if (!PlatformService.get_achievements().get_Item("FoggyWeather").get_achieved())
								{
									PlatformService.get_achievements().get_Item("FoggyWeather").Unlock();
								}
							});
						}
					}
					else if (this.m_targetFog < this.m_currentFog)
					{
						this.m_currentFog = Mathf.Max(this.m_targetFog, this.m_currentFog - 0.0002f);
					}
					else if (instance.m_randomizer.Int32(20000u) == 0)
					{
						if (this.m_currentRain == 0f && this.m_targetRain == 0f)
						{
							int num4 = (!instance.m_isNightTime) ? this.m_properties.m_fogProbabilityDay : this.m_properties.m_fogProbabilityNight;
							if (instance.m_randomizer.Int32(100u) < num4)
							{
								this.m_targetFog = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
							}
							else
							{
								this.m_targetFog = 0f;
							}
						}
						else
						{
							this.m_targetFog = 0f;
						}
					}
					if (this.m_targetCloud > this.m_currentCloud)
					{
						this.m_currentCloud = Mathf.Min(this.m_targetCloud, this.m_currentCloud + 0.0008f);
					}
					else if (this.m_targetCloud < this.m_currentCloud)
					{
						this.m_currentCloud = Mathf.Max(this.m_targetCloud, this.m_currentCloud - 0.0008f);
					}
					else if (instance.m_randomizer.Int32(20000u) == 0)
					{
						int num5 = (!instance.m_isNightTime) ? this.m_properties.m_cloudProbabilityDay : this.m_properties.m_cloudProbabilityNight;
						if (instance.m_randomizer.Int32(100u) < num5)
						{
							this.m_targetCloud = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
						}
						else
						{
							this.m_targetCloud = 0f;
						}
					}
				}
			}
			else
			{
				this.m_targetRain = 0f;
				this.m_currentRain = Mathf.Min(this.m_currentRain, num2);
				this.m_targetFog = 0f;
				this.m_currentFog = Mathf.Min(this.m_currentFog, num2);
				this.m_targetCloud = 0f;
				this.m_currentCloud = Mathf.Min(this.m_currentCloud, num2);
			}
			if (this.m_groundWetness == 0f && this.m_currentRain != 0f)
			{
				this.m_targetRainbow = ((instance.m_randomizer.Int32(100u) >= this.m_properties.m_rainbowProbability) ? 0f : 1f);
			}
			float num6 = this.m_groundWetness;
			num6 -= num6 * 0.00013f + 1E-05f;
			num6 += Mathf.Min(this.m_currentRain, 0.25f) * 0.00061f;
			this.m_groundWetness = Mathf.Clamp01(num6);
			float num7 = Mathf.Min(1f - this.m_currentRain * 4f, this.m_groundWetness);
			num7 = Mathf.Clamp01(Mathf.Min(num7, this.m_targetRainbow));
			if (num7 > this.m_currentRainbow)
			{
				this.m_currentRainbow = Mathf.Min(num7, this.m_currentRainbow + 0.0002f);
			}
			else if (num7 < this.m_currentRainbow)
			{
				this.m_currentRainbow = Mathf.Max(num7, this.m_currentRainbow - 0.0002f);
			}
			if (this.m_targetNorthernLights > this.m_currentNorthernLights)
			{
				this.m_currentNorthernLights = Mathf.Min(this.m_targetNorthernLights, this.m_currentNorthernLights + 0.0002f);
			}
			else if (this.m_targetNorthernLights < this.m_currentRain)
			{
				this.m_currentNorthernLights = Mathf.Max(this.m_targetNorthernLights, this.m_currentNorthernLights - 0.0002f);
			}
			else if (instance.m_randomizer.Int32(10000u) == 0)
			{
				if (instance.m_randomizer.Int32(100u) < this.m_properties.m_northernLightsProbability)
				{
					this.m_targetNorthernLights = (float)instance.m_randomizer.Int32(5000, 10000) * 0.0001f;
				}
				else
				{
					this.m_targetNorthernLights = 0f;
				}
			}
			float num8 = Mathf.Abs((this.m_targetTemperature - this.m_currentTemperature) * 0.001f);
			this.m_temperatureSpeed = Mathf.Min(this.m_temperatureSpeed + 0.0001f, num8);
			this.m_currentTemperature = Mathf.MoveTowards(this.m_currentTemperature, this.m_targetTemperature, this.m_temperatureSpeed);
			int dayTimeFrame = (int)instance.m_dayTimeFrame;
			int dAYTIME_FRAMES = (int)SimulationManager.DAYTIME_FRAMES;
			float num9 = Mathf.Abs((float)dayTimeFrame / (float)dAYTIME_FRAMES - 0.5f) * 2f;
			float num10 = Mathf.Lerp(this.m_properties.m_minTemperatureDay, this.m_properties.m_minTemperatureNight, num9);
			float num11 = Mathf.Lerp(this.m_properties.m_maxTemperatureDay, this.m_properties.m_maxTemperatureNight, num9);
			num10 = Mathf.Lerp(num10, this.m_properties.m_minTemperatureRain, this.m_currentRain);
			num11 = Mathf.Lerp(num11, this.m_properties.m_maxTemperatureRain, this.m_currentRain);
			num10 = Mathf.Lerp(num10, this.m_properties.m_minTemperatureFog, this.m_currentFog);
			num11 = Mathf.Lerp(num11, this.m_properties.m_maxTemperatureFog, this.m_currentFog);
			if (num8 < 2E-05f || this.m_targetTemperature < num10 || this.m_targetTemperature > num11)
			{
				this.m_targetTemperature = (float)instance.m_randomizer.Int32(0, 10000) * (num11 - num10) / 10000f + num10;
			}
			for (int i = 0; i < this.m_lightningQueue.m_size; i++)
			{
				uint startFrame = this.m_lightningQueue.m_buffer[i].m_startFrame;
				if (startFrame != 0u)
				{
					if (startFrame == instance.m_currentFrameIndex)
					{
						this.StrikeNow(i);
					}
					else if (startFrame + 45u < instance.m_currentFrameIndex)
					{
						InstanceID empty = InstanceID.Empty;
						empty.Lightning = (byte)i;
						Singleton<InstanceManager>.get_instance().ReleaseInstance(empty);
						this.m_lightningQueue.m_buffer[i].m_startFrame = 0u;
					}
				}
			}
			while (this.m_lightningQueue.m_size > 0 && this.m_lightningQueue.m_buffer[this.m_lightningQueue.m_size - 1].m_startFrame == 0u)
			{
				this.m_lightningQueue.m_size--;
			}
			if (this.m_currentRain > 0.8f && this.m_lightningQueue.m_size == 0)
			{
				float num12 = this.m_currentRain * 5f - 4f;
				int num13 = 5000 - Mathf.RoundToInt(num12 * 4000f);
				this.QueueLightningStrike(instance.m_randomizer.UInt32((uint)num13));
			}
		}
	}

	private bool StrikeNow(int lightningIndex)
	{
		WeatherManager.LightningStrike lightningStrike = this.m_lightningQueue.m_buffer[lightningIndex];
		if (this.m_properties != null && this.m_properties.m_lightningSound != null)
		{
			AudioInfo variation = this.m_properties.m_lightningSound.GetVariation(ref Singleton<SimulationManager>.get_instance().m_randomizer);
			Singleton<AudioManager>.get_instance().AddEvent(Singleton<AudioManager>.get_instance().EffectGroup, variation, lightningStrike.m_position, Vector3.get_zero(), 10000f, 1f, (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(600, 1100) * 0.001f, lightningIndex);
		}
		float maxDistance = 5f;
		ushort num;
		uint num2;
		this.FindStrikeTarget(lightningStrike.m_position, maxDistance, 0f, out num, out num2);
		InstanceID id = default(InstanceID);
		id.Lightning = (byte)lightningIndex;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		if (num != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
			if (!(info.m_buildingAI is PowerPoleAI))
			{
				DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
				byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)num].m_position);
				DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
				return ((cityPlanningPolicies & DistrictPolicies.CityPlanning.LightningRods) == DistrictPolicies.CityPlanning.None || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) >= 70) && info.m_buildingAI.BurnBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, false);
			}
			ushort num3 = instance.m_buildings.m_buffer[(int)num].FindParentNode(num);
			if (num3 != 0)
			{
				NetManager instance3 = Singleton<NetManager>.get_instance();
				bool flag = false;
				int num4 = instance3.m_nodes.m_buffer[(int)num3].CountSegments();
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance3.m_nodes.m_buffer[(int)num3].GetSegment(i);
					if (segment != 0)
					{
						NetInfo info2 = instance3.m_segments.m_buffer[(int)segment].Info;
						flag |= info2.m_netAI.CollapseSegment(segment, ref instance3.m_segments.m_buffer[(int)segment], group, false);
						if (num4-- == 0)
						{
							break;
						}
					}
				}
				return flag;
			}
		}
		else if (num2 != 0u)
		{
			TreeManager instance4 = Singleton<TreeManager>.get_instance();
			return instance4.BurnTree(num2, group, Singleton<SimulationManager>.get_instance().m_randomizer.Int32(140, 255));
		}
		return false;
	}

	private void FindStrikeTarget(Vector3 position, float maxDistance, float heightFactor, out ushort building, out uint tree)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		TreeManager instance2 = Singleton<TreeManager>.get_instance();
		float num = maxDistance;
		building = 0;
		tree = 0u;
		int num2 = Mathf.Max((int)((position.x - maxDistance - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Max((int)((position.z - maxDistance - 72f) / 64f + 135f), 0);
		int num4 = Mathf.Min((int)((position.x + maxDistance + 72f) / 64f + 135f), 269);
		int num5 = Mathf.Min((int)((position.z + maxDistance + 72f) / 64f + 135f), 269);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = instance.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					Vector3 position2 = instance.m_buildings.m_buffer[(int)num6].m_position;
					float angle = instance.m_buildings.m_buffer[(int)num6].m_angle;
					BuildingInfo buildingInfo;
					int num8;
					int num9;
					instance.m_buildings.m_buffer[(int)num6].GetInfoWidthLength(out buildingInfo, out num8, out num9);
					Vector3 vector;
					vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
					Vector3 vector2;
					vector2..ctor(vector.z, 0f, -vector.x);
					Vector3 vector3 = position - position2;
					vector3 -= Mathf.Clamp(Vector3.Dot(vector3, vector), (float)(-(float)num8) * 4f, (float)num8 * 4f) * vector;
					vector3 -= Mathf.Clamp(Vector3.Dot(vector3, vector2), (float)(-(float)num9) * 4f, (float)num9 * 4f) * vector2;
					float num10 = VectorUtils.LengthXZ(vector3);
					if (num10 < maxDistance)
					{
						if (buildingInfo.m_buildingAI is PowerPoleAI)
						{
							if ((instance.m_buildings.m_buffer[(int)num6].m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Collapsed)) == (Building.Flags.Created | Building.Flags.Untouchable))
							{
								num10 -= (position2.y + buildingInfo.m_generatedInfo.m_size.y - position.y + 50f) * heightFactor;
								if (num10 < num)
								{
									num = num10;
									building = num6;
									tree = 0u;
								}
							}
						}
						else if ((instance.m_buildings.m_buffer[(int)num6].m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Untouchable | Building.Flags.Collapsed)) == Building.Flags.Created && instance.m_buildings.m_buffer[(int)num6].m_fireIntensity == 0)
						{
							num10 -= (position2.y + buildingInfo.m_generatedInfo.m_size.y - position.y) * heightFactor;
							if (num10 < num && buildingInfo.m_buildingAI.BurnBuilding(num6, ref instance.m_buildings.m_buffer[(int)num6], null, true))
							{
								num = num10;
								building = num6;
								tree = 0u;
							}
						}
					}
					num6 = instance.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		int num11 = Mathf.Max((int)((position.x - maxDistance) / 32f + 270f), 0);
		int num12 = Mathf.Max((int)((position.z - maxDistance) / 32f + 270f), 0);
		int num13 = Mathf.Min((int)((position.x + maxDistance) / 32f + 270f), 539);
		int num14 = Mathf.Min((int)((position.z + maxDistance) / 32f + 270f), 539);
		for (int k = num12; k <= num14; k++)
		{
			for (int l = num11; l <= num13; l++)
			{
				uint num15 = instance2.m_treeGrid[k * 540 + l];
				int num16 = 0;
				while (num15 != 0u)
				{
					Vector3 position3 = instance2.m_trees.m_buffer[(int)((UIntPtr)num15)].Position;
					float num17 = VectorUtils.LengthXZ(position3 - position);
					if (num17 < maxDistance)
					{
						TreeInfo info = instance2.m_trees.m_buffer[(int)((UIntPtr)num15)].Info;
						num17 -= (position3.y + info.m_generatedInfo.m_size.y - position.y) * heightFactor;
						if (num17 < num && (instance2.m_trees.m_buffer[(int)((UIntPtr)num15)].m_flags & 64) == 0)
						{
							num = num17;
							building = 0;
							tree = num15;
						}
					}
					num15 = instance2.m_trees.m_buffer[(int)((UIntPtr)num15)].m_nextGridTree;
					if (++num16 >= 262144)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public bool QueueLightningStrike(uint delay)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		uint startFrame = instance.m_currentFrameIndex + delay;
		int num = Mathf.RoundToInt(8640f);
		Vector3 vector;
		vector.x = (float)instance.m_randomizer.Int32(-num, num);
		vector.y = 0f;
		vector.z = (float)instance.m_randomizer.Int32(-num, num);
		vector.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(vector, false, 0f);
		Quaternion quaternion = Quaternion.AngleAxis((float)instance.m_randomizer.Int32(360u), Vector3.get_up());
		quaternion *= Quaternion.AngleAxis((float)instance.m_randomizer.Int32(-15, 15), Vector3.get_right());
		DisasterManager instance2 = Singleton<DisasterManager>.get_instance();
		DisasterInfo disasterInfo = DisasterManager.FindDisasterInfo<ThunderStormAI>();
		ushort num2 = 0;
		if (disasterInfo != null)
		{
			for (int i = 1; i < instance2.m_disasters.m_size; i++)
			{
				if ((instance2.m_disasters.m_buffer[i].m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
				{
					DisasterInfo info = instance2.m_disasters.m_buffer[i].Info;
					if (info == disasterInfo)
					{
						num2 = (ushort)i;
					}
				}
			}
			if (num2 == 0 && instance2.CreateDisaster(out num2, disasterInfo))
			{
				instance2.m_disasters.m_buffer[(int)num2].m_intensity = 10;
				instance2.m_disasters.m_buffer[(int)num2].m_targetPosition = vector;
				disasterInfo.m_disasterAI.StartNow(num2, ref instance2.m_disasters.m_buffer[(int)num2]);
				disasterInfo.m_disasterAI.ActivateNow(num2, ref instance2.m_disasters.m_buffer[(int)num2]);
			}
		}
		InstanceManager.Group group = null;
		if (num2 != 0)
		{
			InstanceID empty = InstanceID.Empty;
			empty.Disaster = num2;
			group = Singleton<InstanceManager>.get_instance().GetGroup(empty);
		}
		return this.QueueLightningStrike(startFrame, vector, quaternion, group);
	}

	public bool QueueLightningStrike(uint startFrame, Vector3 position, Quaternion rotation, InstanceManager.Group group)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex + 15u;
		if (startFrame < num)
		{
			startFrame = num;
		}
		float maxDistance = 200f;
		ushort num2;
		uint num3;
		this.FindStrikeTarget(position, maxDistance, 3f, out num2, out num3);
		if (num2 != 0)
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num2].Info;
			position = Building.CalculateMeshPosition(info, Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num2].m_position, Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num2].m_angle, Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num2].Length);
			Segment3 ray;
			ray..ctor(position, position);
			ray.a.y = ray.a.y + info.m_generatedInfo.m_size.y;
			float num4;
			if (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num2].RayCast(num2, ray, out num4))
			{
				position = ray.Position(num4);
			}
		}
		else if (num3 != 0u)
		{
			position = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num3)].Position;
			Randomizer randomizer;
			randomizer..ctor(num3);
			TreeInfo info2 = Singleton<TreeManager>.get_instance().m_trees.m_buffer[(int)((UIntPtr)num3)].Info;
			float num5 = info2.m_minScale + (float)randomizer.Int32(10000u) * (info2.m_maxScale - info2.m_minScale) * 0.0001f;
			position.y += info2.m_generatedInfo.m_size.y * num5 * 0.5f;
		}
		WeatherManager.LightningStrike lightningStrike;
		lightningStrike.m_startFrame = startFrame;
		lightningStrike.m_position = position;
		lightningStrike.m_rotation = rotation;
		for (int i = 0; i < this.m_lightningQueue.m_size; i++)
		{
			if (this.m_lightningQueue.m_buffer[i].m_startFrame == 0u)
			{
				this.m_lightningQueue.m_buffer[i] = lightningStrike;
				if (group != null)
				{
					InstanceID empty = InstanceID.Empty;
					empty.Lightning = (byte)i;
					Singleton<InstanceManager>.get_instance().SetGroup(empty, group);
				}
				return true;
			}
		}
		if (this.m_lightningQueue.m_size < 20)
		{
			this.m_lightningQueue.Add(lightningStrike);
			if (group != null)
			{
				InstanceID empty2 = InstanceID.Empty;
				empty2.Lightning = (byte)(this.m_lightningQueue.m_size - 1);
				Singleton<InstanceManager>.get_instance().SetGroup(empty2, group);
			}
			return true;
		}
		return false;
	}

	private ushort CalculateSelfHeight(int x, int z)
	{
		float minX = ((float)x - 64f) * 135f;
		float minZ = ((float)z - 64f) * 135f;
		float maxX = ((float)(x + 1) - 64f) * 135f;
		float maxZ = ((float)(z + 1) - 64f) * 135f;
		int num;
		int num2;
		int num3;
		Singleton<TerrainManager>.get_instance().CalculateAreaHeight(minX, minZ, maxX, maxZ, out num, out num2, out num3);
		int num4;
		float num5;
		float num6;
		float num7;
		Singleton<TreeManager>.get_instance().CalculateAreaHeight(minX, minZ, maxX, maxZ, out num4, out num5, out num6, out num7);
		int num8 = Mathf.RoundToInt(num6 * 64f);
		int num9 = Mathf.RoundToInt(num7 * 64f);
		int num10 = num2 + (num8 - num2) * Mathf.Min(num4, 100) / 100;
		int num11 = Mathf.Max(num3, num9);
		return (ushort)Mathf.Clamp(num10 + num11 >> 1, 0, 65535);
	}

	private ushort CalculateTotalHeight(int x, int z)
	{
		int num = Mathf.Max(x - 2, 0);
		int num2 = Mathf.Max(z - 2, 0);
		int num3 = Mathf.Min(x + 2, 127);
		int num4 = Mathf.Min(z + 2, 127);
		int num5 = 0;
		int num6 = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				num5 += (int)this.m_windGrid[i * 128 + j].m_selfHeight;
				num6++;
			}
		}
		return (ushort)Mathf.Clamp(num5 / num6, 0, 65535);
	}

	public override void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float num = Mathf.Min(heightArea.m_min.x, surfaceArea.m_min.x);
		float num2 = Mathf.Min(heightArea.m_min.z, surfaceArea.m_min.z);
		float num3 = Mathf.Max(heightArea.m_max.x, surfaceArea.m_max.x);
		float num4 = Mathf.Max(heightArea.m_max.z, surfaceArea.m_max.z);
		int num5 = Mathf.Max((int)(num / 135f + 64f), 0);
		int num6 = Mathf.Max((int)(num2 / 135f + 64f), 0);
		int num7 = Mathf.Min((int)(num3 / 135f + 64f), 127);
		int num8 = Mathf.Min((int)(num4 / 135f + 64f), 127);
		int num9 = 10000;
		int num10 = 10000;
		int num11 = -10000;
		int num12 = -10000;
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num13 = this.CalculateSelfHeight(j, i);
				if (num13 != this.m_windGrid[i * 128 + j].m_selfHeight)
				{
					this.m_windGrid[i * 128 + j].m_selfHeight = num13;
					if (j - 2 < num9)
					{
						num9 = j - 2;
					}
					if (i - 2 < num10)
					{
						num10 = i - 2;
					}
					if (j + 2 > num11)
					{
						num11 = j + 2;
					}
					if (i + 2 > num12)
					{
						num12 = i + 2;
					}
				}
			}
		}
		num9 = Mathf.Max(num9, 0);
		num10 = Mathf.Max(num10, 0);
		num11 = Mathf.Min(num11, 127);
		num12 = Mathf.Min(num12, 127);
		int num14 = 10000;
		int num15 = 10000;
		int num16 = -10000;
		int num17 = -10000;
		for (int k = num10; k <= num12; k++)
		{
			for (int l = num9; l <= num11; l++)
			{
				ushort num18 = this.CalculateTotalHeight(l, k);
				if (num18 != this.m_windGrid[k * 128 + l].m_totalHeight)
				{
					this.m_windGrid[k * 128 + l].m_totalHeight = num18;
					if (l < num14)
					{
						num14 = l;
					}
					if (k < num15)
					{
						num15 = k;
					}
					if (l > num16)
					{
						num16 = l;
					}
					if (k > num17)
					{
						num17 = k;
					}
				}
			}
		}
		if (num16 >= num14 && num17 >= num15)
		{
			this.AreaModified(num14, num15, num16, num17);
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new WeatherManager.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float WINDGRID_CELL_SIZE = 135f;

	public const int WINDGRID_RESOLUTION = 128;

	public WeatherManager.WindCell[] m_windGrid;

	public Texture2D m_windTexture;

	public float m_windDirection;

	public float m_targetDirection;

	public float m_directionSpeed;

	public float m_currentTemperature;

	public float m_targetTemperature;

	public float m_temperatureSpeed;

	public float m_currentRain;

	public float m_targetRain;

	public float m_currentFog;

	public float m_targetFog;

	public float m_currentCloud;

	public float m_targetCloud;

	public float m_forceWeatherOn;

	public float m_groundWetness;

	public float m_currentNorthernLights;

	public float m_targetNorthernLights;

	public float m_currentRainbow;

	public float m_targetRainbow;

	[NonSerialized]
	public bool m_enableWeather;

	private int[] m_modifiedX1;

	private int[] m_modifiedX2;

	private bool m_modified;

	private bool m_windMapVisible;

	private Transform m_windZone;

	private FastList<WeatherManager.LightningStrike> m_lightningQueue;

	public float m_lastLightningIntensity;

	public float m_flashingReduction;

	private MaterialPropertyBlock m_materialBlock;

	public struct WindCell
	{
		public ushort m_selfHeight;

		public ushort m_totalHeight;
	}

	public struct LightningStrike
	{
		public uint m_startFrame;

		public Vector3 m_position;

		public Quaternion m_rotation;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "WeatherManager");
			WeatherManager instance = Singleton<WeatherManager>.get_instance();
			WeatherManager.WindCell[] windGrid = instance.m_windGrid;
			int num = windGrid.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				uShort.Write(windGrid[i].m_selfHeight);
			}
			uShort.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int j = 0; j < num; j++)
			{
				uShort2.Write(windGrid[j].m_totalHeight);
			}
			uShort2.EndWrite();
			s.WriteFloat(instance.m_windDirection);
			s.WriteFloat(instance.m_targetDirection);
			s.WriteFloat(instance.m_directionSpeed);
			s.WriteFloat(instance.m_currentTemperature);
			s.WriteFloat(instance.m_targetTemperature);
			s.WriteFloat(instance.m_temperatureSpeed);
			s.WriteFloat(instance.m_currentRain);
			s.WriteFloat(instance.m_targetRain);
			s.WriteFloat(instance.m_currentFog);
			s.WriteFloat(instance.m_targetFog);
			s.WriteFloat(instance.m_currentCloud);
			s.WriteFloat(instance.m_targetCloud);
			s.WriteFloat(instance.m_forceWeatherOn);
			s.WriteFloat(instance.m_groundWetness);
			s.WriteFloat(instance.m_currentNorthernLights);
			s.WriteFloat(instance.m_targetNorthernLights);
			s.WriteFloat(instance.m_currentRainbow);
			s.WriteFloat(instance.m_targetRainbow);
			int num2 = 0;
			for (int k = 0; k < instance.m_lightningQueue.m_size; k++)
			{
				if (instance.m_lightningQueue.m_buffer[k].m_startFrame != 0u)
				{
					num2++;
				}
			}
			s.WriteUInt8((uint)num2);
			for (int l = 0; l < instance.m_lightningQueue.m_size; l++)
			{
				if (instance.m_lightningQueue.m_buffer[l].m_startFrame != 0u)
				{
					s.WriteUInt32(instance.m_lightningQueue.m_buffer[l].m_startFrame);
					s.WriteVector3(instance.m_lightningQueue.m_buffer[l].m_position);
					s.WriteQuaternion(instance.m_lightningQueue.m_buffer[l].m_rotation);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "WeatherManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "WeatherManager", "WindManager");
			WeatherManager instance = Singleton<WeatherManager>.get_instance();
			WeatherManager.WindCell[] windGrid = instance.m_windGrid;
			int num = windGrid.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				windGrid[i].m_selfHeight = uShort.Read();
			}
			uShort.EndRead();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int j = 0; j < num; j++)
			{
				windGrid[j].m_totalHeight = uShort2.Read();
			}
			uShort2.EndRead();
			instance.m_windDirection = s.ReadFloat();
			instance.m_targetDirection = s.ReadFloat();
			instance.m_directionSpeed = s.ReadFloat();
			if (s.get_version() >= 232u)
			{
				instance.m_currentTemperature = s.ReadFloat();
				instance.m_targetTemperature = s.ReadFloat();
				instance.m_temperatureSpeed = s.ReadFloat();
			}
			else
			{
				instance.m_currentTemperature = 20f;
				instance.m_targetTemperature = 20f;
				instance.m_temperatureSpeed = 0f;
			}
			if (s.get_version() >= 233u)
			{
				instance.m_currentRain = s.ReadFloat();
				instance.m_targetRain = s.ReadFloat();
				instance.m_currentFog = s.ReadFloat();
				instance.m_targetFog = s.ReadFloat();
			}
			else
			{
				instance.m_currentRain = 0f;
				instance.m_targetRain = 0f;
				instance.m_currentFog = 0f;
				instance.m_targetFog = 0f;
			}
			if (s.get_version() >= 275u)
			{
				instance.m_currentCloud = s.ReadFloat();
				instance.m_targetCloud = s.ReadFloat();
				instance.m_forceWeatherOn = s.ReadFloat();
			}
			else
			{
				instance.m_currentCloud = 0f;
				instance.m_targetCloud = 0f;
				instance.m_forceWeatherOn = 0f;
			}
			if (s.get_version() >= 235u)
			{
				instance.m_groundWetness = s.ReadFloat();
			}
			else
			{
				instance.m_groundWetness = 0f;
			}
			if (s.get_version() >= 241u)
			{
				instance.m_currentNorthernLights = s.ReadFloat();
				instance.m_targetNorthernLights = s.ReadFloat();
				instance.m_currentRainbow = s.ReadFloat();
				instance.m_targetRainbow = s.ReadFloat();
			}
			else
			{
				instance.m_currentNorthernLights = 0f;
				instance.m_targetNorthernLights = 0f;
				instance.m_currentRainbow = 0f;
				instance.m_targetRainbow = 0f;
			}
			if (s.get_version() == 276u)
			{
				s.ReadUInt32();
				s.ReadUInt32();
				s.ReadVector3();
				s.ReadQuaternion();
			}
			int num2;
			if (s.get_version() >= 277u)
			{
				num2 = (int)s.ReadUInt8();
			}
			else
			{
				num2 = 0;
			}
			instance.m_lightningQueue.Clear();
			instance.m_lightningQueue.EnsureCapacity(num2);
			for (int k = 0; k < num2; k++)
			{
				WeatherManager.LightningStrike item;
				item.m_startFrame = s.ReadUInt32();
				item.m_position = s.ReadVector3();
				item.m_rotation = s.ReadQuaternion();
				instance.m_lightningQueue.Add(item);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "WeatherManager", "WindManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "WeatherManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			WeatherManager instance = Singleton<WeatherManager>.get_instance();
			instance.AreaModified(0, 0, 127, 127);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "WeatherManager");
		}
	}
}
