﻿using System;

public class StagingException : Exception
{
	public StagingException(string message, Exception inner) : base(message, inner)
	{
	}
}
