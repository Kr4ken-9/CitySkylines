﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class LandscapingPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	private LandscapingPanel.LandscapingInfo landscapingInfo
	{
		get
		{
			if (this.m_LandscapingInfo == null)
			{
				this.m_LandscapingInfo = new LandscapingPanel.LandscapingInfo();
			}
			return this.m_LandscapingInfo;
		}
	}

	protected override void OnHideOptionBars()
	{
		UIView.get_library().Hide("LandscapingInfoPanel");
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < LandscapingPanel.kTools.Length; i++)
		{
			this.SpawnEntry(LandscapingPanel.kTools[i].get_enumName(), true, null);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		TerrainTool terrainTool = ToolsModifierControl.SetTool<TerrainTool>();
		if (terrainTool != null)
		{
			terrainTool.m_mode = LandscapingPanel.kTools[zOrder].get_enumValue();
			base.ShowLandscapingOptionPanel();
			UIView.get_library().Show("LandscapingInfoPanel");
		}
	}

	private void SpawnEntry(string name, bool enabled, MilestoneInfo info)
	{
		this.landscapingInfo.m_DirtPrice = Singleton<TerrainManager>.get_instance().m_properties.m_dirtPrice;
		float cost = (float)this.landscapingInfo.m_DirtPrice * 65536f / 262144f * 512f / 100f;
		string text = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Title,
			Locale.Get("LANDSCAPING_TITLE", name),
			LocaleFormatter.Sprite,
			name,
			LocaleFormatter.Text,
			Locale.Get("LANDSCAPING_DESC", name),
			LocaleFormatter.Locked,
			(!enabled).ToString(),
			LocaleFormatter.Cost,
			LocaleFormatter.FormatCubicCost(cost)
		});
		if (Singleton<UnlockManager>.get_exists())
		{
			string text2;
			string text3;
			string text4;
			string text5;
			string text6;
			ToolsModifierControl.GetUnlockingInfo(info, out text2, out text3, out text4, out text5, out text6);
			string text7 = TooltipHelper.Format(new string[]
			{
				LocaleFormatter.LockedInfo,
				text6,
				LocaleFormatter.UnlockDesc,
				text2,
				LocaleFormatter.UnlockPopulationProgressText,
				text5,
				LocaleFormatter.UnlockPopulationTarget,
				text4,
				LocaleFormatter.UnlockPopulationCurrent,
				text3
			});
			text = TooltipHelper.Append(text, text7);
		}
		this.SpawnEntry(name, text, "Landscaping" + name, null, GeneratedPanel.landscapingTooltipBox, enabled).set_objectUserData(this.landscapingInfo);
	}

	private LandscapingPanel.LandscapingInfo m_LandscapingInfo;

	private static readonly PositionData<TerrainTool.Mode>[] kTools = Utils.GetOrderedEnumData<TerrainTool.Mode>();

	public class LandscapingInfo
	{
		public int m_DirtPrice;
	}
}
