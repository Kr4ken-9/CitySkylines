﻿using System;
using ColossalFramework.IO;

public class MessageEffect : TriggerEffect
{
	public override void Activate()
	{
		base.Activate();
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteUniqueString(this.m_message);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_message = s.ReadUniqueString();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public string m_message;
}
