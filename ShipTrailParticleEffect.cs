﻿using System;
using UnityEngine;

public class ShipTrailParticleEffect : MovementParticleEffect
{
	public override void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
		Vector3 point = area.m_matrix.MultiplyPoint(Vector3.get_zero());
		if (cameraInfo.CheckRenderDistance(point, this.m_maxVisibilityDistance) && cameraInfo.Intersect(point, 100f))
		{
			Vector3 vector;
			vector..ctor(100000f, 100000f, 100000f);
			Vector3 vector2;
			vector2..ctor(-100000f, -100000f, -100000f);
			magnitude = Mathf.Min(magnitude, this.m_minMagnitude + velocity.get_magnitude() * this.m_magnitudeSpeedMultiplier + acceleration * this.m_magnitudeAccelerationMultiplier);
			float particlesPerSquare = timeDelta * magnitude * 0.01f;
			base.EmitParticles(id, area, velocity, particlesPerSquare, 100, ref vector, ref vector2);
		}
	}
}
