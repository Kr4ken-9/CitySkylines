﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class HumanAI : CitizenAI
{
	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			if ((data.m_flags & CitizenInstance.Flags.WaitingTaxi) != CitizenInstance.Flags.None)
			{
				return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[5];
			}
			if (data.m_path != 0u && (data.m_flags & (CitizenInstance.Flags.WaitingTransport | CitizenInstance.Flags.EnteringVehicle)) != CitizenInstance.Flags.None)
			{
				PathUnit.Position position = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].GetPosition(data.m_pathPositionIndex >> 1);
				ushort startNode = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)position.m_segment].m_startNode;
				ushort transportLine = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startNode].m_transportLine;
				if (transportLine != 0)
				{
					TransportManager instance = Singleton<TransportManager>.get_instance();
					TransportInfo info = instance.m_lines.m_buffer[(int)transportLine].Info;
					if (info != null && info.m_transportType != TransportInfo.TransportType.EvacuationBus)
					{
						return instance.m_lines.m_buffer[(int)transportLine].GetColor();
					}
				}
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.EscapeRoutes)
			{
				if ((data.m_flags & CitizenInstance.Flags.WaitingTaxi) == CitizenInstance.Flags.None)
				{
					PathUnit.Position position2 = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].GetPosition(data.m_pathPositionIndex >> 1);
					ushort startNode2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					ushort transportLine2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startNode2].m_transportLine;
					if (transportLine2 != 0)
					{
						TransportManager instance2 = Singleton<TransportManager>.get_instance();
						TransportInfo info2 = instance2.m_lines.m_buffer[(int)transportLine2].Info;
						if (info2 != null && info2.m_transportType == TransportInfo.TransportType.EvacuationBus)
						{
							return instance2.m_lines.m_buffer[(int)transportLine2].GetColor();
						}
					}
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(instanceID, ref data, infoMode);
		}
	}

	protected TransferManager.TransferReason GetLeavingReason(uint citizenID, ref Citizen data)
	{
		switch (data.WealthLevel)
		{
		case Citizen.Wealth.Low:
			return TransferManager.TransferReason.LeaveCity0;
		case Citizen.Wealth.Medium:
			return TransferManager.TransferReason.LeaveCity1;
		case Citizen.Wealth.High:
			return TransferManager.TransferReason.LeaveCity2;
		default:
			return TransferManager.TransferReason.LeaveCity0;
		}
	}

	protected void FindVisitPlace(uint citizenID, ushort sourceBuilding, TransferManager.TransferReason reason)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Priority = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8u);
		offer.Citizen = citizenID;
		offer.Position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].m_position;
		offer.Amount = 1;
		offer.Active = true;
		Singleton<TransferManager>.get_instance().AddIncomingOffer(reason, offer);
	}

	protected void FindEvacuationPlace(uint citizenID, ushort sourceBuilding, TransferManager.TransferReason reason)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Priority = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(8u);
		offer.Citizen = citizenID;
		offer.Position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].m_position;
		offer.Amount = 1;
		offer.Active = true;
		Singleton<TransferManager>.get_instance().AddOutgoingOffer(reason, offer);
	}

	public bool StartMoving(uint citizenID, ref Citizen data, ushort sourceBuilding, ushort targetBuilding)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (targetBuilding == sourceBuilding)
		{
			return false;
		}
		if (targetBuilding == 0 || (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			return false;
		}
		if (data.m_instance != 0)
		{
			this.m_info.m_citizenAI.SetTarget(data.m_instance, ref instance.m_instances.m_buffer[(int)data.m_instance], targetBuilding);
			data.CurrentLocation = Citizen.Location.Moving;
			return true;
		}
		if (sourceBuilding == 0)
		{
			sourceBuilding = data.GetBuildingByLocation();
			if (sourceBuilding == 0)
			{
				return false;
			}
		}
		ushort num;
		if (instance.CreateCitizenInstance(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, citizenID))
		{
			this.m_info.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], sourceBuilding);
			this.m_info.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], targetBuilding);
			data.CurrentLocation = Citizen.Location.Moving;
			return true;
		}
		return false;
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None && (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			uint citizen = data.m_citizen;
			Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
			if (citizen != 0u)
			{
				Singleton<CitizenManager>.get_instance().ReleaseCitizen(citizen);
			}
			return;
		}
		if ((data.m_flags & CitizenInstance.Flags.WaitingPath) != CitizenInstance.Flags.None)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				this.Spawn(instanceID, ref data);
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~CitizenInstance.Flags.WaitingPath;
				data.m_flags &= ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering);
				this.PathfindSuccess(instanceID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~CitizenInstance.Flags.WaitingPath;
				data.m_flags &= ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering);
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(instanceID, ref data);
				return;
			}
		}
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		VehicleManager instance3 = Singleton<VehicleManager>.get_instance();
		ushort num = 0;
		if (data.m_citizen != 0u)
		{
			num = instance2.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_vehicle;
		}
		if (num != 0)
		{
			VehicleInfo info = instance3.m_vehicles.m_buffer[(int)num].Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				info.m_vehicleAI.SimulationStep(num, ref instance3.m_vehicles.m_buffer[(int)num], num, ref instance3.m_vehicles.m_buffer[(int)num], 0);
				num = 0;
			}
		}
		if (num == 0 && (data.m_flags & (CitizenInstance.Flags.Character | CitizenInstance.Flags.WaitingPath | CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) == CitizenInstance.Flags.None)
		{
			data.m_flags &= ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering);
			this.ArriveAtDestination(instanceID, ref data, false);
			instance2.ReleaseCitizenInstance(instanceID);
		}
	}

	protected virtual void PathfindSuccess(ushort instanceID, ref CitizenInstance data)
	{
		bool flag = false;
		uint citizen = data.m_citizen;
		if (citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			flag = ((instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.DummyTraffic) != Citizen.Flags.None);
			Citizen[] expr_48_cp_0 = instance.m_citizens.m_buffer;
			UIntPtr expr_48_cp_1 = (UIntPtr)citizen;
			expr_48_cp_0[(int)expr_48_cp_1].m_flags = (expr_48_cp_0[(int)expr_48_cp_1].m_flags & ~Citizen.Flags.MovingIn);
		}
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		if (data.m_sourceBuilding != 0)
		{
			BuildingAI.PathFindType type = (!flag) ? BuildingAI.PathFindType.LeavingHuman : BuildingAI.PathFindType.LeavingDummy;
			BuildingInfo info = instance2.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
			info.m_buildingAI.PathfindSuccess(data.m_sourceBuilding, ref instance2.m_buildings.m_buffer[(int)data.m_sourceBuilding], type);
		}
		if (data.m_targetBuilding != 0)
		{
			BuildingAI.PathFindType type2 = (!flag) ? BuildingAI.PathFindType.EnteringHuman : BuildingAI.PathFindType.EnteringDummy;
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
			info2.m_buildingAI.PathfindSuccess(data.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)data.m_targetBuilding], type2);
		}
	}

	protected virtual void PathfindFailure(ushort instanceID, ref CitizenInstance data)
	{
		VehicleInfo vehicleInfo = this.GetVehicleInfo(instanceID, ref data, false);
		if (vehicleInfo != null && vehicleInfo.m_vehicleType != VehicleInfo.VehicleType.Bicycle)
		{
			bool flag = false;
			uint citizen = data.m_citizen;
			if (citizen != 0u)
			{
				CitizenManager instance = Singleton<CitizenManager>.get_instance();
				flag = ((instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.DummyTraffic) != Citizen.Flags.None);
			}
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			if (data.m_sourceBuilding != 0)
			{
				BuildingAI.PathFindType type = (!flag) ? BuildingAI.PathFindType.LeavingHuman : BuildingAI.PathFindType.LeavingDummy;
				BuildingInfo info = instance2.m_buildings.m_buffer[(int)data.m_sourceBuilding].Info;
				info.m_buildingAI.PathfindFailure(data.m_sourceBuilding, ref instance2.m_buildings.m_buffer[(int)data.m_sourceBuilding], type);
			}
			if (data.m_targetBuilding != 0)
			{
				BuildingAI.PathFindType type2 = (!flag) ? BuildingAI.PathFindType.EnteringHuman : BuildingAI.PathFindType.EnteringDummy;
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)data.m_targetBuilding].Info;
				info2.m_buildingAI.PathfindFailure(data.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)data.m_targetBuilding], type2);
			}
		}
		this.ArriveAtDestination(instanceID, ref data, false);
		Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
	}

	protected virtual void Spawn(ushort instanceID, ref CitizenInstance data)
	{
		data.Spawn(instanceID);
	}

	public override bool AddWind(ushort instanceID, ref CitizenInstance citizenData, Vector3 wind, InstanceManager.Group group)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None && ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None || wind.y > 19.62f))
		{
			CitizenInstance.Frame lastFrameData = citizenData.GetLastFrameData();
			lastFrameData.m_velocity = lastFrameData.m_velocity * 0.875f + wind * 0.125f;
			wind.y = 0f;
			if (wind.get_sqrMagnitude() > 0.01f)
			{
				lastFrameData.m_rotation = Quaternion.Lerp(lastFrameData.m_rotation, Quaternion.LookRotation(wind), 0.1f);
			}
			citizenData.SetLastFrameData(lastFrameData);
			if ((citizenData.m_flags & CitizenInstance.Flags.Blown) == CitizenInstance.Flags.None)
			{
				if (group != null)
				{
					ushort disaster = group.m_ownerInstance.Disaster;
					if (disaster != 0)
					{
						DisasterData[] expr_E1_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
						ushort expr_E1_cp_1 = disaster;
						expr_E1_cp_0[(int)expr_E1_cp_1].m_casualtiesCount = expr_E1_cp_0[(int)expr_E1_cp_1].m_casualtiesCount + 1u;
						StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.CasualtyCount);
						statisticBase.Add(1);
					}
				}
				citizenData.m_flags |= CitizenInstance.Flags.Blown;
				citizenData.m_waitCounter = 0;
			}
			InstanceID empty = InstanceID.Empty;
			empty.CitizenInstance = instanceID;
			Singleton<InstanceManager>.get_instance().SetGroup(empty, group);
			return true;
		}
		return false;
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
			frameData.m_velocity.y = frameData.m_velocity.y - 2.4525f;
			frameData.m_velocity *= 0.99f;
			frameData.m_position += frameData.m_velocity * 0.5f;
			float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
			if (num > frameData.m_position.y)
			{
				frameData.m_velocity = Vector3.get_zero();
				frameData.m_position.y = num;
				citizenData.m_waitCounter = (byte)Mathf.Min((int)(citizenData.m_waitCounter + 1), 255);
				if (citizenData.m_waitCounter >= 100)
				{
					citizenData.Unspawn(instanceID);
					return;
				}
			}
			else
			{
				citizenData.m_waitCounter = 0;
			}
		}
		else if ((citizenData.m_flags & CitizenInstance.Flags.Floating) != CitizenInstance.Flags.None)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
			citizenData.m_waitCounter = (byte)Mathf.Min((int)(citizenData.m_waitCounter + 1), 255);
			float num2;
			float num3;
			Vector3 vector;
			Vector3 vector2;
			Singleton<TerrainManager>.get_instance().SampleWaterData(VectorUtils.XZ(frameData.m_position), out num2, out num3, out vector, out vector2);
			if (num3 - num2 > 1f)
			{
				vector *= 0.266666681f;
				frameData.m_velocity = Vector3.MoveTowards(frameData.m_velocity, vector, 1f);
				float num4 = Mathf.Clamp((float)citizenData.m_waitCounter * 0.05f, 1f, num3 - num2);
				frameData.m_velocity.y = frameData.m_velocity.y * 0.5f + (num3 - num4 - frameData.m_position.y) * 0.5f;
			}
			else
			{
				num2 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
				frameData.m_velocity = Vector3.MoveTowards(frameData.m_velocity, Vector3.get_zero(), 1f);
				frameData.m_velocity.y = frameData.m_velocity.y * 0.5f + (num2 - frameData.m_position.y) * 0.5f;
			}
			frameData.m_position += frameData.m_velocity * 0.5f;
			if (citizenData.m_waitCounter >= 100)
			{
				citizenData.Unspawn(instanceID);
				return;
			}
		}
		else
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			Vector3 vector3 = citizenData.m_targetPos - frameData.m_position;
			float num5;
			if (!lodPhysics && citizenData.m_targetPos.w > 0.001f)
			{
				num5 = VectorUtils.LengthSqrXZ(vector3);
			}
			else
			{
				num5 = vector3.get_sqrMagnitude();
			}
			float sqrMagnitude = frameData.m_velocity.get_sqrMagnitude();
			float num6 = Mathf.Max(sqrMagnitude * 3f, 3f);
			if (lodPhysics && (ulong)(currentFrameIndex >> 4 & 3u) == (ulong)((long)(instanceID & 3)))
			{
				num6 *= 4f;
			}
			bool flag = false;
			if ((citizenData.m_flags & CitizenInstance.Flags.TryingSpawnVehicle) != CitizenInstance.Flags.None)
			{
				bool flag2 = true;
				if ((citizenData.m_waitCounter += 1) == 255 || citizenData.m_path == 0u)
				{
					flag2 = false;
				}
				if (flag2)
				{
					PathUnit.Position pathPos;
					flag2 = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out pathPos);
					if (flag2)
					{
						flag2 = this.SpawnVehicle(instanceID, ref citizenData, pathPos);
					}
				}
				if (!flag2)
				{
					citizenData.m_flags &= ~CitizenInstance.Flags.TryingSpawnVehicle;
					citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
					citizenData.m_waitCounter = 0;
					base.InvalidPath(instanceID, ref citizenData);
				}
			}
			else if ((citizenData.m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
			{
				bool flag3 = true;
				if (citizenData.m_waitCounter < 255)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						citizenData.m_waitCounter += 1;
					}
				}
				else if ((citizenData.m_flags & CitizenInstance.Flags.BoredOfWaiting) == CitizenInstance.Flags.None)
				{
					citizenData.m_flags |= CitizenInstance.Flags.BoredOfWaiting;
					citizenData.m_waitCounter = 0;
				}
				else
				{
					citizenData.m_flags &= ~CitizenInstance.Flags.WaitingTransport;
					citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
					citizenData.m_flags |= CitizenInstance.Flags.CannotUseTransport;
					citizenData.m_waitCounter = 0;
					flag3 = false;
					base.InvalidPath(instanceID, ref citizenData);
				}
				if (flag3 && num5 < num6)
				{
					if ((ulong)(currentFrameIndex >> 4 & 7u) == (ulong)((long)(instanceID & 7)))
					{
						citizenData.m_targetPos = this.GetTransportWaitPosition(instanceID, ref citizenData, ref frameData, num6);
					}
					vector3 = citizenData.m_targetPos - frameData.m_position;
					if (!lodPhysics && citizenData.m_targetPos.w > 0.001f)
					{
						num5 = VectorUtils.LengthSqrXZ(vector3);
					}
					else
					{
						num5 = vector3.get_sqrMagnitude();
					}
				}
			}
			else if ((citizenData.m_flags & CitizenInstance.Flags.WaitingTaxi) != CitizenInstance.Flags.None)
			{
				bool flag4 = false;
				if (citizenData.m_citizen != 0u)
				{
					flag4 = (Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].m_vehicle != 0);
					if (!flag4 && (ulong)(currentFrameIndex >> 4 & 15u) == (ulong)((long)(instanceID & 15)))
					{
						TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
						offer.Priority = 7;
						offer.Citizen = citizenData.m_citizen;
						offer.Position = frameData.m_position;
						offer.Amount = 1;
						offer.Active = false;
						Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Taxi, offer);
					}
				}
				if (citizenData.m_waitCounter < 255)
				{
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
					{
						citizenData.m_waitCounter += 1;
					}
				}
				else if ((citizenData.m_flags & CitizenInstance.Flags.BoredOfWaiting) == CitizenInstance.Flags.None)
				{
					citizenData.m_flags |= CitizenInstance.Flags.BoredOfWaiting;
					citizenData.m_waitCounter = 0;
				}
				else if (!flag4)
				{
					citizenData.m_flags &= ~CitizenInstance.Flags.WaitingTaxi;
					citizenData.m_flags &= ~CitizenInstance.Flags.BoredOfWaiting;
					citizenData.m_flags |= CitizenInstance.Flags.CannotUseTaxi;
					citizenData.m_waitCounter = 0;
					base.InvalidPath(instanceID, ref citizenData);
				}
			}
			else if ((citizenData.m_flags & CitizenInstance.Flags.EnteringVehicle) != CitizenInstance.Flags.None)
			{
				if (num5 < num6)
				{
					citizenData.m_targetPos = this.GetVehicleEnterPosition(instanceID, ref citizenData, num6);
					vector3 = citizenData.m_targetPos - frameData.m_position;
					if (!lodPhysics && citizenData.m_targetPos.w > 0.001f)
					{
						num5 = VectorUtils.LengthSqrXZ(vector3);
					}
					else
					{
						num5 = vector3.get_sqrMagnitude();
					}
				}
			}
			else
			{
				if (citizenData.m_path != 0u && (citizenData.m_flags & CitizenInstance.Flags.WaitingPath) == CitizenInstance.Flags.None)
				{
					NetManager instance = Singleton<NetManager>.get_instance();
					byte b = citizenData.m_pathPositionIndex;
					if (b == 255)
					{
						b = 0;
					}
					PathManager instance2 = Singleton<PathManager>.get_instance();
					PathUnit.Position position;
					if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(b >> 1, out position) && (instance.m_segments.m_buffer[(int)position.m_segment].m_flags & NetSegment.Flags.Flooded) != NetSegment.Flags.None && Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(frameData.m_position)))
					{
						citizenData.m_flags |= CitizenInstance.Flags.Floating;
					}
				}
				if (num5 < num6)
				{
					if (citizenData.m_path != 0u)
					{
						if ((citizenData.m_flags & CitizenInstance.Flags.WaitingPath) == CitizenInstance.Flags.None)
						{
							citizenData.m_targetPos = base.GetPathTargetPosition(instanceID, ref citizenData, ref frameData, num6);
							if ((citizenData.m_flags & CitizenInstance.Flags.OnPath) == CitizenInstance.Flags.None)
							{
								citizenData.m_targetPos.w = 1f;
							}
						}
					}
					else
					{
						if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None)
						{
							if (citizenData.m_citizen != 0u)
							{
								Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizenData.m_citizen)].SetVehicle(citizenData.m_citizen, 0, 0u);
							}
							citizenData.m_flags &= ~CitizenInstance.Flags.RidingBicycle;
						}
						citizenData.m_flags &= ~(CitizenInstance.Flags.OnPath | CitizenInstance.Flags.OnBikeLane);
						if (citizenData.m_targetBuilding != 0 && ((citizenData.m_flags & CitizenInstance.Flags.AtTarget) == CitizenInstance.Flags.None || (ulong)(currentFrameIndex >> 4 & 15u) == (ulong)((long)(instanceID & 15))))
						{
							this.GetBuildingTargetPosition(instanceID, ref citizenData, num6);
						}
						if ((citizenData.m_flags & CitizenInstance.Flags.Panicking) == CitizenInstance.Flags.None)
						{
							flag = true;
						}
					}
					vector3 = citizenData.m_targetPos - frameData.m_position;
					if (!lodPhysics && citizenData.m_targetPos.w > 0.001f)
					{
						num5 = VectorUtils.LengthSqrXZ(vector3);
					}
					else
					{
						num5 = vector3.get_sqrMagnitude();
					}
				}
			}
			float num7 = this.m_info.m_walkSpeed;
			float num8 = 2f;
			if ((citizenData.m_flags & CitizenInstance.Flags.HangAround) != CitizenInstance.Flags.None)
			{
				num7 = Mathf.Max(num7 * 0.5f, 1f);
			}
			else if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) != CitizenInstance.Flags.None)
			{
				if ((citizenData.m_flags & CitizenInstance.Flags.OnBikeLane) != CitizenInstance.Flags.None)
				{
					num7 *= 2f;
				}
				else
				{
					num7 *= 1.5f;
				}
			}
			if (sqrMagnitude > 0.01f)
			{
				frameData.m_position += frameData.m_velocity * 0.5f;
			}
			if (num5 < 1f)
			{
				vector3 = Vector3.get_zero();
				if ((citizenData.m_flags & CitizenInstance.Flags.EnteringVehicle) != CitizenInstance.Flags.None)
				{
					if (this.EnterVehicle(instanceID, ref citizenData))
					{
						return;
					}
				}
				else if (flag)
				{
					if (this.ArriveAtTarget(instanceID, ref citizenData))
					{
						return;
					}
					citizenData.m_flags |= CitizenInstance.Flags.AtTarget;
					if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u) == 0)
					{
						citizenData.m_targetSeed = (byte)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(256u);
						this.SwitchBuildingTargetPos(instanceID, ref citizenData);
					}
				}
				else
				{
					citizenData.m_flags &= ~CitizenInstance.Flags.AtTarget;
				}
			}
			else
			{
				float num9 = Mathf.Sqrt(num5);
				float num10 = Mathf.Sqrt(sqrMagnitude);
				float num11 = Mathf.Max(0f, Vector3.Dot(vector3, frameData.m_velocity) / Mathf.Max(1f, num10 * num9));
				num7 = Mathf.Max(0.5f, num7 * num11 * num11 * num11);
				vector3 *= Mathf.Min(0.577f, num7 / num9);
				citizenData.m_flags &= ~CitizenInstance.Flags.AtTarget;
				if ((citizenData.m_flags & CitizenInstance.Flags.RequireSlowStart) != CitizenInstance.Flags.None && citizenData.m_waitCounter < 8)
				{
					citizenData.m_waitCounter += 1;
					frameData.m_velocity = Vector3.get_zero();
					return;
				}
			}
			frameData.m_underground = ((citizenData.m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None);
			frameData.m_insideBuilding = ((citizenData.m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None);
			frameData.m_transition = ((citizenData.m_flags & CitizenInstance.Flags.Transition) != CitizenInstance.Flags.None);
			if (num5 < 1f && flag && (citizenData.m_flags & CitizenInstance.Flags.SittingDown) != CitizenInstance.Flags.None)
			{
				citizenData.m_flags |= CitizenInstance.Flags.RequireSlowStart;
				citizenData.m_waitCounter = 0;
				frameData.m_velocity = (citizenData.m_targetPos - frameData.m_position) * 0.5f;
				frameData.m_position += frameData.m_velocity * 0.5f;
				if (citizenData.m_targetDir.get_sqrMagnitude() > 0.01f)
				{
					frameData.m_rotation = Quaternion.LookRotation(VectorUtils.X_Y(citizenData.m_targetDir));
				}
			}
			else
			{
				citizenData.m_flags &= ~CitizenInstance.Flags.RequireSlowStart;
				Vector3 vector4 = vector3 - frameData.m_velocity;
				float magnitude = vector4.get_magnitude();
				vector4 *= num8 / Mathf.Max(magnitude, num8);
				frameData.m_velocity += vector4;
				frameData.m_velocity -= Mathf.Max(0f, Vector3.Dot(frameData.m_position + frameData.m_velocity - citizenData.m_targetPos, frameData.m_velocity)) / Mathf.Max(0.01f, frameData.m_velocity.get_sqrMagnitude()) * frameData.m_velocity;
				float sqrMagnitude2 = frameData.m_velocity.get_sqrMagnitude();
				bool flag5 = !lodPhysics && citizenData.m_targetPos.w > 0.001f && (sqrMagnitude2 > 0.01f || sqrMagnitude > 0.01f);
				ushort num12;
				if (flag5)
				{
					Vector3 worldPos = frameData.m_position + frameData.m_velocity * 0.5f;
					num12 = Singleton<BuildingManager>.get_instance().GetWalkingBuilding(worldPos);
				}
				else
				{
					num12 = 0;
				}
				if (sqrMagnitude2 > 0.01f)
				{
					if (!lodPhysics)
					{
						Vector3 vector5 = Vector3.get_zero();
						float num13 = 0f;
						base.CheckCollisions(instanceID, ref citizenData, frameData.m_position, frameData.m_position + frameData.m_velocity, num12, ref vector5, ref num13);
						if (num13 > 0.01f)
						{
							vector5 *= 1f / num13;
							vector5 = Vector3.ClampMagnitude(vector5, Mathf.Sqrt(sqrMagnitude2) * 0.9f);
							frameData.m_velocity += vector5;
						}
					}
					frameData.m_position += frameData.m_velocity * 0.5f;
					Vector3 velocity = frameData.m_velocity;
					if ((citizenData.m_flags & CitizenInstance.Flags.RidingBicycle) == CitizenInstance.Flags.None)
					{
						velocity.y = 0f;
					}
					if (velocity.get_sqrMagnitude() > 0.01f)
					{
						frameData.m_rotation = Quaternion.LookRotation(velocity);
					}
				}
				else if (num5 < 1f && flag && citizenData.m_targetDir.get_sqrMagnitude() > 0.01f)
				{
					frameData.m_rotation = Quaternion.LookRotation(VectorUtils.X_Y(citizenData.m_targetDir));
				}
				if (flag5)
				{
					Vector3 position2 = frameData.m_position;
					float num14 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position2);
					if (num12 != 0)
					{
						num14 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num12].SampleWalkingHeight(num12, position2, num14);
						position2.y += (num14 - position2.y) * Mathf.Min(1f, citizenData.m_targetPos.w * 4f);
						frameData.m_position.y = position2.y;
					}
					else if (Mathf.Abs(num14 - position2.y) < 2f)
					{
						position2.y += (num14 - position2.y) * Mathf.Min(1f, citizenData.m_targetPos.w * 4f);
						frameData.m_position.y = position2.y;
					}
				}
			}
		}
	}

	protected virtual void SwitchBuildingTargetPos(ushort instanceID, ref CitizenInstance citizenData)
	{
	}

	protected override bool CheckSegmentChange(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position prevPos, PathUnit.Position nextPos, int prevOffset, int nextOffset, Bezier3 bezier)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num;
		if (prevOffset < 128)
		{
			num = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_startNode;
		}
		else
		{
			num = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_endNode;
		}
		ushort num2;
		if (nextOffset < 128)
		{
			num2 = instance.m_segments.m_buffer[(int)nextPos.m_segment].m_startNode;
		}
		else
		{
			num2 = instance.m_segments.m_buffer[(int)nextPos.m_segment].m_endNode;
		}
		if (num == num2)
		{
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)num].m_flags;
			if ((flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
			{
				Segment3 segment;
				segment..ctor(bezier.a, bezier.b);
				Segment3 segment2;
				segment2..ctor(bezier.b, bezier.c);
				Segment3 segment3;
				segment3..ctor(bezier.c, bezier.d);
				Segment3 segment4;
				segment4.a = instance.m_nodes.m_buffer[(int)num].m_position;
				for (int i = 0; i < 8; i++)
				{
					ushort segment5 = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
					if (segment5 != 0 && segment5 != prevPos.m_segment && segment5 != nextPos.m_segment)
					{
						if (instance.m_segments.m_buffer[(int)segment5].m_startNode == num)
						{
							segment4.b = segment4.a + instance.m_segments.m_buffer[(int)segment5].m_startDirection * 1000f;
						}
						else
						{
							segment4.b = segment4.a + instance.m_segments.m_buffer[(int)segment5].m_endDirection * 1000f;
						}
						float num3;
						float num4;
						if (segment4.DistanceSqr(segment, ref num3, ref num4) < 1f || segment4.DistanceSqr(segment2, ref num3, ref num4) < 1f || segment4.DistanceSqr(segment3, ref num3, ref num4) < 1f)
						{
							return this.CheckTrafficLights(num, segment5);
						}
					}
				}
			}
		}
		return true;
	}

	protected override bool CheckLaneChange(ushort instanceID, ref CitizenInstance citizenData, PathUnit.Position prevPos, PathUnit.Position nextPos, int prevOffset, int nextOffset)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num;
		if (prevOffset == 0)
		{
			num = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_startNode;
		}
		else
		{
			num = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_endNode;
		}
		NetNode.Flags flags = instance.m_nodes.m_buffer[(int)num].m_flags;
		if ((flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None)
		{
			NetInfo info = instance.m_segments.m_buffer[(int)prevPos.m_segment].Info;
			if (info.m_lanes.Length > (int)prevPos.m_lane && info.m_lanes.Length > (int)nextPos.m_lane)
			{
				float position = info.m_lanes[(int)prevPos.m_lane].m_position;
				float position2 = info.m_lanes[(int)nextPos.m_lane].m_position;
				for (int i = 0; i < info.m_lanes.Length; i++)
				{
					if (i != (int)prevPos.m_lane && i != (int)nextPos.m_lane && (byte)(info.m_lanes[i].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
					{
						float position3 = info.m_lanes[i].m_position;
						if ((position3 > position && position3 < position2) || (position3 < position && position3 > position2))
						{
							return this.CheckTrafficLights(num, prevPos.m_segment);
						}
					}
				}
			}
		}
		return true;
	}

	private bool CheckTrafficLights(ushort node, ushort segment)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		uint num = (uint)(((int)node << 8) / 32768);
		uint num2 = currentFrameIndex - num & 255u;
		RoadBaseAI.TrafficLightState vehicleLightState;
		RoadBaseAI.TrafficLightState trafficLightState;
		bool vehicles;
		bool flag;
		RoadBaseAI.GetTrafficLightState(node, ref instance.m_segments.m_buffer[(int)segment], currentFrameIndex - num, out vehicleLightState, out trafficLightState, out vehicles, out flag);
		if (trafficLightState != RoadBaseAI.TrafficLightState.RedToGreen)
		{
			if (trafficLightState == RoadBaseAI.TrafficLightState.GreenToRed || trafficLightState == RoadBaseAI.TrafficLightState.Red)
			{
				if (!flag && num2 >= 196u)
				{
					flag = true;
					RoadBaseAI.SetTrafficLightState(node, ref instance.m_segments.m_buffer[(int)segment], currentFrameIndex - num, vehicleLightState, trafficLightState, vehicles, flag);
				}
				return false;
			}
		}
		else if (num2 < 60u)
		{
			return false;
		}
		return true;
	}

	protected virtual bool EnterVehicle(ushort instanceID, ref CitizenInstance citizenData)
	{
		citizenData.m_flags &= ~CitizenInstance.Flags.EnteringVehicle;
		citizenData.Unspawn(instanceID);
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num = Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
			if (num != 0)
			{
				num = instance.m_vehicles.m_buffer[(int)num].GetFirstVehicle(num);
			}
			if (num != 0)
			{
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				int ticketPrice = info.m_vehicleAI.GetTicketPrice(num, ref instance.m_vehicles.m_buffer[(int)num]);
				if (ticketPrice != 0)
				{
					Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PublicIncome, ticketPrice, info.m_class);
				}
			}
		}
		return false;
	}

	public override bool TransportArriveAtSource(ushort instanceID, ref CitizenInstance citizenData, Vector3 currentPos, Vector3 nextTarget)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		PathUnit.Position pathPos;
		if (citizenData.m_path != 0u && instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out pathPos))
		{
			uint laneID = PathManager.GetLaneID(pathPos);
			Vector3 vector = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((citizenData.m_lastPathOffset < 128) ? 1f : 0f);
			Vector3 vector2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)citizenData.m_lastPathOffset * 0.003921569f);
			float num = Vector3.SqrMagnitude(vector - currentPos);
			float num2 = Vector3.SqrMagnitude(vector2 - nextTarget);
			if (num < 4f && num2 < 4f)
			{
				return true;
			}
		}
		return false;
	}

	public override bool TransportArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData, Vector3 currentPos, Vector3 nextTarget, ref TransportPassengerData passengerData, bool forceUnload)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		CitizenManager instance3 = Singleton<CitizenManager>.get_instance();
		ushort num = 0;
		bool flag = false;
		if (citizenData.m_path != 0u)
		{
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out pathPos))
			{
				uint laneID = PathManager.GetLaneID(pathPos);
				Vector3 vector = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
				float num2 = Vector3.SqrMagnitude(vector - currentPos);
				if (num2 >= 4f)
				{
					flag = true;
				}
				num = instance2.m_segments.m_buffer[(int)pathPos.m_segment].m_endNode;
			}
			else
			{
				flag = true;
			}
		}
		if (citizenData.m_path != 0u)
		{
			citizenData.m_pathPositionIndex += 2;
			if (citizenData.m_pathPositionIndex >> 1 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].m_positionCount)
			{
				instance.ReleaseFirstUnit(ref citizenData.m_path);
				citizenData.m_pathPositionIndex = 0;
			}
		}
		if (citizenData.m_path != 0u)
		{
			PathUnit.Position pathPos2;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out pathPos2))
			{
				citizenData.m_lastPathOffset = pathPos2.m_offset;
				uint laneID2 = PathManager.GetLaneID(pathPos2);
				Vector3 vector2 = instance2.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].CalculatePosition((float)citizenData.m_lastPathOffset * 0.003921569f);
				float num3 = Vector3.SqrMagnitude(vector2 - nextTarget);
				if (num3 < 4f)
				{
					if (!forceUnload)
					{
						return false;
					}
					flag = true;
				}
				else if (instance2.m_nodes.m_buffer[(int)num].m_lane != laneID2)
				{
					flag = true;
				}
			}
			else
			{
				flag = true;
			}
		}
		citizenData.m_targetPos = currentPos;
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			if ((instance3.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.Tourist) != Citizen.Flags.None)
			{
				passengerData.m_touristPassengers.m_tempCount = passengerData.m_touristPassengers.m_tempCount + 1u;
			}
			else
			{
				passengerData.m_residentPassengers.m_tempCount = passengerData.m_residentPassengers.m_tempCount + 1u;
			}
			VehicleInfo vehicleInfo = this.GetVehicleInfo(instanceID, ref citizenData, false);
			if (vehicleInfo != null && vehicleInfo.m_vehicleType != VehicleInfo.VehicleType.Bicycle)
			{
				passengerData.m_carOwningPassengers.m_tempCount = passengerData.m_carOwningPassengers.m_tempCount + 1u;
			}
			switch (Citizen.GetAgeGroup(instance3.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Age))
			{
			case Citizen.AgeGroup.Child:
				passengerData.m_childPassengers.m_tempCount = passengerData.m_childPassengers.m_tempCount + 1u;
				break;
			case Citizen.AgeGroup.Teen:
				passengerData.m_teenPassengers.m_tempCount = passengerData.m_teenPassengers.m_tempCount + 1u;
				break;
			case Citizen.AgeGroup.Young:
				passengerData.m_youngPassengers.m_tempCount = passengerData.m_youngPassengers.m_tempCount + 1u;
				break;
			case Citizen.AgeGroup.Adult:
				passengerData.m_adultPassengers.m_tempCount = passengerData.m_adultPassengers.m_tempCount + 1u;
				break;
			case Citizen.AgeGroup.Senior:
				passengerData.m_seniorPassengers.m_tempCount = passengerData.m_seniorPassengers.m_tempCount + 1u;
				break;
			}
		}
		if (flag && citizenData.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(citizenData.m_path);
			citizenData.m_path = 0u;
		}
		return true;
	}

	public override bool SetCurrentVehicle(ushort instanceID, ref CitizenInstance citizenData, ushort vehicleID, uint unitID, Vector3 position)
	{
		uint citizen = citizenData.m_citizen;
		if ((citizenData.m_flags & CitizenInstance.Flags.WaitingTransport) != CitizenInstance.Flags.None)
		{
			if (vehicleID == 0 && unitID == 0u)
			{
				return true;
			}
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (citizen == 0u)
			{
				citizenData.m_flags &= ~CitizenInstance.Flags.WaitingTransport;
				citizenData.m_flags |= CitizenInstance.Flags.EnteringVehicle;
				return false;
			}
			instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, vehicleID, unitID);
			if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle != 0)
			{
				citizenData.m_flags &= ~CitizenInstance.Flags.WaitingTransport;
				citizenData.m_flags |= CitizenInstance.Flags.EnteringVehicle;
				return true;
			}
			return false;
		}
		else
		{
			if (vehicleID == 0)
			{
				CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
				ushort num = 0;
				if (citizen != 0u)
				{
					num = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
					instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
				}
				if (num != 0)
				{
					Vector3 position2;
					if (this.GetNextTargetPosition(instanceID, ref citizenData, Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num].GetLastFramePosition(), out position2))
					{
						position = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num].GetClosestDoorPosition(position2, VehicleInfo.DoorType.Exit);
					}
					else
					{
						Randomizer randomizer;
						randomizer..ctor((int)(instanceID ^ num));
						position = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num].GetRandomDoorPosition(ref randomizer, VehicleInfo.DoorType.Exit);
					}
				}
				citizenData.Unspawn(instanceID);
				citizenData.m_targetPos = position;
				citizenData.m_frame0.m_velocity = Vector3.get_zero();
				citizenData.m_frame0.m_position = position;
				citizenData.m_frame0.m_rotation = Quaternion.get_identity();
				citizenData.m_frame1 = citizenData.m_frame0;
				citizenData.m_frame2 = citizenData.m_frame0;
				citizenData.m_frame3 = citizenData.m_frame0;
				if (citizenData.m_path != 0u && (citizenData.m_flags & CitizenInstance.Flags.WaitingPath) == CitizenInstance.Flags.None)
				{
					if (Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].CalculatePathPositionOffset(citizenData.m_pathPositionIndex >> 1, position, out citizenData.m_lastPathOffset))
					{
						this.Spawn(instanceID, ref citizenData);
					}
				}
				else if (this.StartPathFind(instanceID, ref citizenData))
				{
					this.Spawn(instanceID, ref citizenData);
				}
				return true;
			}
			return false;
		}
	}

	private bool GetNextTargetPosition(ushort instanceID, ref CitizenInstance citizenData, Vector3 refPos, out Vector3 pos)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		PathUnit.Position pathPos;
		if (citizenData.m_path != 0u && instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out pathPos))
		{
			uint num = PathManager.GetLaneID(pathPos);
			if (num != 0u)
			{
				NetInfo info = instance2.m_segments.m_buffer[(int)pathPos.m_segment].Info;
				if (info.m_lanes != null && info.m_lanes.Length > (int)pathPos.m_lane && info.m_lanes[(int)pathPos.m_lane].m_laneType == NetInfo.LaneType.Pedestrian)
				{
					float num2;
					instance2.m_lanes.m_buffer[(int)((UIntPtr)num)].GetClosestPosition(refPos, out pos, out num2);
					return true;
				}
			}
			ushort startNode = instance2.m_segments.m_buffer[(int)pathPos.m_segment].m_startNode;
			Vector3 position = instance2.m_nodes.m_buffer[(int)startNode].m_position;
			num = instance2.m_nodes.m_buffer[(int)startNode].m_lane;
			if (num != 0u)
			{
				uint segment = (uint)instance2.m_lanes.m_buffer[(int)((UIntPtr)num)].m_segment;
				uint num3;
				int num4;
				float num5;
				if (instance2.m_segments.m_buffer[(int)((UIntPtr)segment)].GetClosestLanePosition(position, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, out pos, out num3, out num4, out num5))
				{
					return true;
				}
			}
			pos = position;
			return true;
		}
		pos = Vector3.get_zero();
		return false;
	}

	private Vector4 GetTransportWaitPosition(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, float minSqrDistance)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)citizenData.m_path)].GetPosition(citizenData.m_pathPositionIndex >> 1, out position))
		{
			base.InvalidPath(instanceID, ref citizenData);
			return citizenData.m_targetPos;
		}
		ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
		if ((citizenData.m_flags & CitizenInstance.Flags.BoredOfWaiting) != CitizenInstance.Flags.None)
		{
			instance2.m_nodes.m_buffer[(int)startNode].m_maxWaitTime = 255;
		}
		else if (citizenData.m_waitCounter > instance2.m_nodes.m_buffer[(int)startNode].m_maxWaitTime)
		{
			instance2.m_nodes.m_buffer[(int)startNode].m_maxWaitTime = citizenData.m_waitCounter;
		}
		uint lane = instance2.m_nodes.m_buffer[(int)startNode].m_lane;
		if (lane == 0u)
		{
			return citizenData.m_targetPos;
		}
		uint segment = (uint)instance2.m_lanes.m_buffer[(int)((UIntPtr)lane)].m_segment;
		NetInfo.Lane lane2;
		if (!instance2.m_segments.m_buffer[(int)((UIntPtr)segment)].GetClosestLane(lane, NetInfo.LaneType.Pedestrian, VehicleInfo.VehicleType.None, out lane, out lane2))
		{
			return citizenData.m_targetPos;
		}
		ushort startNode2 = instance2.m_segments.m_buffer[(int)((UIntPtr)segment)].m_startNode;
		ushort endNode = instance2.m_segments.m_buffer[(int)((UIntPtr)segment)].m_endNode;
		if (((instance2.m_nodes.m_buffer[(int)startNode2].m_flags | instance2.m_nodes.m_buffer[(int)endNode].m_flags) & NetNode.Flags.Disabled) != NetNode.Flags.None)
		{
			citizenData.m_waitCounter = 255;
		}
		Randomizer randomizer;
		randomizer..ctor((uint)instanceID | lane << 16);
		float num = instance2.m_nodes.m_buffer[(int)startNode].Info.m_netAI.MaxTransportWaitDistance();
		int num2 = (int)instance2.m_nodes.m_buffer[(int)startNode].m_laneOffset << 8;
		int num3 = Mathf.RoundToInt(num * 65280f / Mathf.Max(1f, instance2.m_lanes.m_buffer[(int)((UIntPtr)lane)].m_length));
		int num4 = Mathf.Clamp(num2 - num3, 0, 65280);
		int num5 = Mathf.Clamp(num2 + num3, 0, 65280);
		int num6 = randomizer.Int32(num4, num5);
		Vector3 vector;
		Vector3 vector2;
		instance2.m_lanes.m_buffer[(int)((UIntPtr)lane)].CalculatePositionAndDirection((float)num6 * 1.53186284E-05f, out vector, out vector2);
		float num7 = Mathf.Max(0f, lane2.m_width - 1f) * (float)randomizer.Int32(-500, 500) * 0.001f;
		vector += Vector3.Cross(Vector3.get_up(), vector2).get_normalized() * num7;
		return new Vector4(vector.x, vector.y, vector.z, 0f);
	}

	protected virtual Vector4 GetVehicleEnterPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			ushort vehicle = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle;
			if (vehicle != 0)
			{
				Vector4 result = instance2.m_vehicles.m_buffer[(int)vehicle].GetClosestDoorPosition(citizenData.m_targetPos, VehicleInfo.DoorType.Enter);
				result.w = citizenData.m_targetPos.w;
				return result;
			}
		}
		return citizenData.m_targetPos;
	}

	protected virtual void GetBuildingTargetPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		if (citizenData.m_targetBuilding == 0)
		{
			citizenData.m_flags &= ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering);
			citizenData.m_targetDir = Vector2.get_zero();
			return;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].m_fireIntensity != 0)
		{
			citizenData.m_flags |= CitizenInstance.Flags.Panicking;
			citizenData.m_targetDir = Vector2.get_zero();
			return;
		}
		BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
		Randomizer randomizer;
		randomizer..ctor((int)instanceID << 8 | (int)citizenData.m_targetSeed);
		Vector3 vector;
		Vector3 vector2;
		Vector2 targetDir;
		CitizenInstance.Flags flags;
		info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref randomizer, this.m_info, instanceID, out vector, out vector2, out targetDir, out flags);
		citizenData.m_flags = ((citizenData.m_flags & ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering)) | flags);
		citizenData.m_targetPos = new Vector4(vector.x, vector.y, vector.z, 1f);
		citizenData.m_targetDir = targetDir;
	}

	protected virtual bool ArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.HangAround) != CitizenInstance.Flags.None)
		{
			uint citizen = citizenData.m_citizen;
			if (citizen != 0u)
			{
				CitizenManager instance = Singleton<CitizenManager>.get_instance();
				if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Moving)
				{
					this.ArriveAtDestination(instanceID, ref citizenData, true);
				}
				if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetBuildingByLocation() == citizenData.m_targetBuilding)
				{
					return false;
				}
			}
			citizenData.m_flags &= ~(CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Panicking | CitizenInstance.Flags.SittingDown | CitizenInstance.Flags.Cheering);
			citizenData.Unspawn(instanceID);
		}
		else
		{
			this.ArriveAtDestination(instanceID, ref citizenData, true);
		}
		return true;
	}

	protected virtual void ArriveAtDestination(ushort instanceID, ref CitizenInstance citizenData, bool success)
	{
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
			if (success)
			{
				instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetLocationByBuilding(citizen, citizenData.m_targetBuilding);
			}
			if (citizenData.m_targetBuilding != 0 && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance2.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
				int num = -100;
				info.m_buildingAI.ModifyMaterialBuffer(citizenData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], TransferManager.TransferReason.Shopping, ref num);
				if (info.m_class.m_service == ItemClass.Service.Beautification)
				{
					StatisticsManager instance3 = Singleton<StatisticsManager>.get_instance();
					StatisticBase statisticBase = instance3.Acquire<StatisticInt32>(StatisticType.ParkVisitCount);
					statisticBase.Add(1);
				}
				ushort eventIndex = instance2.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].m_eventIndex;
				if (eventIndex != 0)
				{
					EventManager instance4 = Singleton<EventManager>.get_instance();
					EventInfo info2 = instance4.m_events.m_buffer[(int)eventIndex].Info;
					info2.m_eventAI.VisitorEnter(eventIndex, ref instance4.m_events.m_buffer[(int)eventIndex], citizenData.m_targetBuilding, citizen);
				}
			}
		}
		if ((citizenData.m_flags & CitizenInstance.Flags.HangAround) == CitizenInstance.Flags.None || !success)
		{
			this.SetSource(instanceID, ref citizenData, 0);
			this.SetTarget(instanceID, ref citizenData, 0);
			citizenData.Unspawn(instanceID);
		}
	}

	protected virtual VehicleInfo GetVehicleInfo(ushort instanceID, ref CitizenInstance citizenData, bool forceProbability)
	{
		return null;
	}
}
