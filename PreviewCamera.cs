﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class PreviewCamera : MonoBehaviour
{
	public float burnedAmount
	{
		get
		{
			return this.m_InfoRenderer.burnedAmount;
		}
		set
		{
			this.m_InfoRenderer.burnedAmount = value;
		}
	}

	public bool showGrid
	{
		get
		{
			return this.m_InfoRenderer.showGrid;
		}
		set
		{
			this.m_InfoRenderer.showGrid = value;
		}
	}

	public bool alpha
	{
		set
		{
			Color backgroundColor = this.m_TargetCamera.get_backgroundColor();
			backgroundColor.a = (float)((!value) ? 1 : 0);
			this.m_TargetCamera.set_backgroundColor(backgroundColor);
		}
	}

	public UITextureSprite preview
	{
		set
		{
			if (this.m_Preview != value)
			{
				this.m_Preview = value;
				this.Setup();
			}
		}
	}

	public void SetPreview(UITextureSprite sprite, Vector2 size)
	{
		if (this.m_Preview != sprite)
		{
			this.m_Preview = sprite;
			size *= (float)PreviewCamera.kMultiplier;
			this.Setup((int)size.x, (int)size.y);
		}
	}

	public RenderTexture targetTexture
	{
		get
		{
			return this.m_RenderTexture;
		}
	}

	public GameObject target
	{
		set
		{
			if (this.m_CachedTarget != value)
			{
				this.m_CachedTarget = value;
				this.m_InfoRenderer.SetTarget(value);
				this.m_Distance = 4f;
			}
		}
	}

	public void ResetDistance()
	{
		this.m_Distance = 4f;
	}

	public void RecalculateBounds()
	{
		this.m_InfoRenderer.RecalculateBounds();
	}

	private void Awake()
	{
		this.m_TargetCamera = base.GetComponent<Camera>();
		this.m_InfoRenderer.Initialize(this.m_TargetCamera);
	}

	private void Setup()
	{
		if (this.m_Preview != null)
		{
			int width = (int)this.m_Preview.get_width() * PreviewCamera.kMultiplier;
			int height = (int)this.m_Preview.get_height() * PreviewCamera.kMultiplier;
			this.Setup(width, height);
		}
	}

	private void Setup(int width, int height)
	{
		if (this.m_Preview != null)
		{
			this.Release();
			this.m_RenderTexture = new RenderTexture(width, height, 24, 0);
			this.m_RenderTexture.set_hideFlags(61);
			this.m_TargetCamera.set_targetTexture(this.m_RenderTexture);
			this.m_TargetCamera.set_pixelRect(new Rect(0f, 0f, (float)width, (float)height));
			this.m_TargetCamera.set_fieldOfView(30f);
			this.m_Preview.set_texture(this.m_RenderTexture);
			this.m_Preview.add_eventMouseMove(new MouseEventHandler(this.MovingMouse));
		}
	}

	private void Release()
	{
		if (this.m_Preview != null)
		{
			this.m_Preview.remove_eventMouseMove(new MouseEventHandler(this.MovingMouse));
			if (this.m_RenderTexture != null)
			{
				this.m_TargetCamera.set_targetTexture(null);
				Object.DestroyImmediate(this.m_RenderTexture);
			}
		}
	}

	private void OnEnable()
	{
		this.Setup();
	}

	private void OnDisable()
	{
		this.Release();
	}

	private void MovingMouse(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_InfoRenderer.hasMesh && p.get_source() == this.m_Preview)
		{
			if (UIMouseButtonExtensions.IsFlagSet(p.get_buttons(), 1) || UIMouseButtonExtensions.IsFlagSet(p.get_buttons(), 4))
			{
				Vector2 moveDelta = p.get_moveDelta();
				moveDelta.y = -moveDelta.y;
				this.m_PreviewDir -= moveDelta / Mathf.Min(this.m_Preview.get_width(), this.m_Preview.get_height()) * this.m_Preview.GetUIView().get_ratio() * 140f;
				this.m_PreviewDir.y = Mathf.Clamp(this.m_PreviewDir.y, -90f, 90f);
				p.Use();
			}
			if (UIMouseButtonExtensions.IsFlagSet(p.get_buttons(), 2))
			{
				Vector2 moveDelta2 = p.get_moveDelta();
				moveDelta2.y = -moveDelta2.y;
				this.m_Distance += moveDelta2.y / this.m_Preview.get_height() * this.m_Preview.GetUIView().get_ratio() * 40f;
				float num = 6f;
				float magnitude = this.m_InfoRenderer.bounds.get_extents().get_magnitude();
				float num2 = magnitude + 16f;
				num *= num2 / magnitude;
				this.m_Distance = Mathf.Clamp(this.m_Distance, 4f, num);
				p.Use();
			}
		}
	}

	public void LateUpdate()
	{
		if (this.m_InfoRenderer.hasMesh)
		{
			Bounds bounds = this.m_InfoRenderer.bounds;
			float magnitude = bounds.get_extents().get_magnitude();
			float num = magnitude + 16f;
			float num2 = magnitude * this.m_Distance;
			this.m_TargetCamera.get_transform().set_position(-Vector3.get_forward() * num2);
			this.m_TargetCamera.get_transform().set_rotation(Quaternion.get_identity());
			this.m_TargetCamera.set_nearClipPlane(Mathf.Max(num2 - num * 1.5f, 0.01f));
			this.m_TargetCamera.set_farClipPlane(num2 + num * 1.5f);
			Quaternion quaternion = Quaternion.Euler(this.m_PreviewDir.y, 0f, 0f) * Quaternion.Euler(0f, this.m_PreviewDir.x, 0f);
			Vector3 vector = quaternion * -bounds.get_center();
			this.m_InfoRenderer.matrix = Matrix4x4.TRS(vector, quaternion, Vector3.get_one());
			this.m_InfoRenderer.Render();
		}
	}

	public PreviewCamera.InfoRenderer m_InfoRenderer = new PreviewCamera.InfoRenderer();

	private static readonly int kMultiplier = 1;

	private Camera m_TargetCamera;

	private RenderTexture m_RenderTexture;

	public UITextureSprite m_Preview;

	private Vector2 m_PreviewDir = new Vector2(120f, -20f);

	private float m_Distance = 4f;

	private GameObject m_CachedTarget;

	[Serializable]
	public class InfoRenderer
	{
		private BuildingInfo buildingInfo
		{
			get
			{
				if (this.m_PrefabInfo != null && this.m_PrefabInfo is BuildingInfo)
				{
					return (BuildingInfo)this.m_PrefabInfo;
				}
				return null;
			}
		}

		private VehicleInfo vehicleInfo
		{
			get
			{
				if (this.m_PrefabInfo != null && this.m_PrefabInfo is VehicleInfo)
				{
					return (VehicleInfo)this.m_PrefabInfo;
				}
				return null;
			}
		}

		public void RecalculateBounds()
		{
			this.m_Bounds = new Bounds(Vector3.get_zero(), Vector3.get_zero());
			if (this.m_Mesh != null)
			{
				Vector3[] vertices = this.m_Mesh.get_vertices();
				for (int i = 0; i < vertices.Length; i++)
				{
					this.m_Bounds.Encapsulate(vertices[i]);
				}
			}
		}

		public void SetTarget(GameObject go)
		{
			if (go != null)
			{
				MeshFilter component = go.GetComponent<MeshFilter>();
				MeshRenderer component2 = go.GetComponent<MeshRenderer>();
				SkinnedMeshRenderer componentInChildren = go.GetComponentInChildren<SkinnedMeshRenderer>();
				this.m_PrefabInfo = go.GetComponent<PrefabInfo>();
				if (component != null && component2 != null)
				{
					this.m_Mesh = component.get_sharedMesh();
					this.m_Material = component2.get_sharedMaterial();
				}
				else if (componentInChildren != null)
				{
					this.m_Mesh = componentInChildren.get_sharedMesh();
					this.m_Material = componentInChildren.get_sharedMaterial();
				}
				else
				{
					this.m_PrefabInfo = null;
					this.m_Mesh = null;
					this.m_Material = null;
				}
			}
			else
			{
				this.m_PrefabInfo = null;
				this.m_Mesh = null;
				this.m_Material = null;
			}
			this.RecalculateBounds();
		}

		public Matrix4x4 matrix
		{
			get;
			set;
		}

		public bool hasMesh
		{
			get
			{
				return this.m_Mesh != null;
			}
		}

		public Bounds bounds
		{
			get
			{
				return this.m_Bounds;
			}
		}

		public float burnedAmount
		{
			get;
			set;
		}

		public bool showGrid
		{
			get;
			set;
		}

		public void Initialize(Camera camera)
		{
			PreviewCamera.InfoRenderer.FullShading();
			this.m_MaterialBlock = new MaterialPropertyBlock();
			this.m_PreviewCube = PreviewCamera.InfoRenderer.CreatePreviewCube();
			this.m_TargetCamera = camera;
		}

		public static void FullShading()
		{
			Shader.DisableKeyword("LIGHTING_ONLY");
		}

		public static void LightingOnly()
		{
			Shader.EnableKeyword("LIGHTING_ONLY");
		}

		private static Mesh CreatePreviewCube()
		{
			Mesh mesh = new Mesh();
			Vector3[] vertices = new Vector3[]
			{
				new Vector3(-0.5f, -0.5f, -0.5f),
				new Vector3(-0.5f, 0.5f, -0.5f),
				new Vector3(0.5f, 0.5f, -0.5f),
				new Vector3(0.5f, -0.5f, -0.5f),
				new Vector3(-0.5f, -0.5f, 0.5f),
				new Vector3(-0.5f, 0.5f, 0.5f),
				new Vector3(0.5f, 0.5f, 0.5f),
				new Vector3(0.5f, -0.5f, 0.5f)
			};
			int[] triangles = new int[]
			{
				0,
				1,
				2,
				2,
				3,
				0,
				1,
				5,
				6,
				6,
				2,
				1,
				3,
				2,
				6,
				6,
				7,
				3,
				4,
				5,
				1,
				1,
				0,
				4,
				0,
				3,
				7,
				7,
				4,
				0,
				7,
				6,
				5,
				5,
				4,
				7
			};
			mesh.set_vertices(vertices);
			mesh.set_triangles(triangles);
			mesh.RecalculateNormals();
			return mesh;
		}

		private void DrawCube(Vector3 position, Vector3 scale, Color color)
		{
			this.m_MaterialBlock.Clear();
			this.m_MaterialBlock.SetColor("_Color", color);
			Matrix4x4 matrix4x = this.matrix * Matrix4x4.TRS(position, Quaternion.get_identity(), Vector3.get_one()) * Matrix4x4.TRS(Vector3.get_zero(), Quaternion.get_identity(), scale);
			Graphics.DrawMesh(this.m_PreviewCube, matrix4x, this.m_PreviewCubeMaterial, LayerMask.NameToLayer("Offscreen"), this.m_TargetCamera, 0, this.m_MaterialBlock, false, true);
		}

		public void Render()
		{
			if (this.m_Mesh != null)
			{
				this.m_MaterialBlock.Clear();
				if (this.buildingInfo != null)
				{
					Vector4 vector;
					vector..ctor(this.burnedAmount, 1000f, 0f, 0f);
					Vector4 colorLocation = RenderManager.GetColorLocation(0u);
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					this.m_MaterialBlock.SetVector(instance.ID_BuildingState, vector);
					this.m_MaterialBlock.SetVector(instance.ID_ObjectIndex, colorLocation);
				}
				else if (this.vehicleInfo != null)
				{
					VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
					this.m_MaterialBlock.SetVectorArray(instance2.ID_TyreLocation, this.vehicleInfo.m_generatedInfo.m_tyres);
				}
				else if (this.m_Material != null && this.m_Material.get_shader().get_name().StartsWith("Custom/Net"))
				{
					NetManager instance3 = Singleton<NetManager>.get_instance();
					this.m_MaterialBlock.SetVector(instance3.ID_ObjectIndex, RenderManager.DefaultColorLocation);
					this.m_MaterialBlock.SetColor(instance3.ID_Color, new Color(0.5f, 0.5f, 0.5f, 0f));
					if (this.m_Material.GetTag("NetType", false) == "TerrainSurface")
					{
						Texture texture;
						Texture texture2;
						Vector4 vector2;
						Singleton<TerrainManager>.get_instance().GetSurfaceMapping(default(Vector3), out texture, out texture2, out vector2);
						if (texture != null)
						{
							this.m_MaterialBlock.SetTexture(instance3.ID_SurfaceTexA, texture);
							this.m_MaterialBlock.SetTexture(instance3.ID_SurfaceTexB, texture2);
							this.m_MaterialBlock.SetVector(instance3.ID_SurfaceMapping, vector2);
						}
					}
				}
				Graphics.DrawMesh(this.m_Mesh, this.matrix, this.m_Material, LayerMask.NameToLayer("Offscreen"), this.m_TargetCamera, 0, this.m_MaterialBlock, true, true);
			}
			if (this.buildingInfo != null && this.showGrid)
			{
				int num = this.buildingInfo.m_cellLength;
				int num2 = 0;
				int num3 = 0;
				if (num < 4)
				{
					if (this.buildingInfo.m_expandFrontYard)
					{
						num3 = 4 - num;
					}
					else
					{
						num2 = 4 - num;
					}
					num = 4;
				}
				Color color = Color.get_white();
				for (int i = 0; i < num; i++)
				{
					for (int j = 0; j < this.buildingInfo.m_cellWidth; j++)
					{
						if (i < num2 || i >= num - num3)
						{
							color = (((j + i & 1) != 0) ? new Color(0.2f, 0.4f, 1f, 0.75f) : new Color(0.1f, 0.2f, 0.5f, 0.75f));
						}
						else if (this.buildingInfo.m_fullPavement)
						{
							color = (((j + i & 1) != 0) ? new Color(0.7f, 0.7f, 0.7f, 0.75f) : new Color(0.5f, 0.5f, 0.5f, 0.75f));
						}
						else if (this.buildingInfo.m_fullGravel)
						{
							color = (((j + i & 1) != 0) ? new Color(0.7f, 0.4f, 0.2f, 0.75f) : new Color(0.5f, 0.2f, 0.1f, 0.75f));
						}
						else
						{
							color = (((j + i & 1) != 0) ? new Color(0.4f, 1f, 0.2f, 0.75f) : new Color(0.2f, 0.5f, 0.1f, 0.75f));
						}
						this.DrawCube(new Vector3(((float)j - (float)this.buildingInfo.m_cellWidth * 0.5f + 0.5f) * 8f, -0.5f, ((float)i - (float)this.buildingInfo.m_cellLength * 0.5f - (float)num2 + 0.5f) * 8f), new Vector3(8f, 1f, 8f), color);
					}
				}
				this.DrawCube(new Vector3(0f, -0.5f, (float)this.buildingInfo.m_cellLength * 4f + (float)num3 * 8f + 1.5f), new Vector3((float)this.buildingInfo.m_cellWidth * 8f + 4f, 1f, 3f), new Color(0.6f, 0.6f, 0.6f, 0.75f));
				this.DrawCube(new Vector3(0f, -0.5f, (float)this.buildingInfo.m_cellLength * 4f + (float)num3 * 8f + 14.5f), new Vector3((float)this.buildingInfo.m_cellWidth * 8f + 4f, 1f, 3f), new Color(0.6f, 0.6f, 0.6f, 0.75f));
				this.DrawCube(new Vector3(0f, -0.5f, (float)this.buildingInfo.m_cellLength * 4f + (float)num3 * 8f + 8f), new Vector3((float)this.buildingInfo.m_cellWidth * 8f + 4f, 1f, 10f), new Color(0.2f, 0.2f, 0.2f, 0.75f));
			}
		}

		public Material m_PreviewCubeMaterial;

		private Camera m_TargetCamera;

		private MaterialPropertyBlock m_MaterialBlock;

		private Mesh m_PreviewCube;

		private Bounds m_Bounds;

		private Mesh m_Mesh;

		private Material m_Material;

		private PrefabInfo m_PrefabInfo;
	}
}
