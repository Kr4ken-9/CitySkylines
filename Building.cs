﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct Building
{
	public void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, int layerMask)
	{
		Building.Flags flags = this.m_flags;
		if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Hidden)) != Building.Flags.Created)
		{
			return;
		}
		BuildingInfo info = this.Info;
		if ((layerMask & 1 << info.m_prefabDataLayer) == 0)
		{
			return;
		}
		Vector3 position = this.m_position;
		float radius = info.m_renderSize + (float)this.m_baseHeight * 0.5f;
		position.y += (info.m_size.y - (float)this.m_baseHeight) * 0.5f;
		if (!cameraInfo.Intersect(position, radius))
		{
			return;
		}
		RenderManager instance = Singleton<RenderManager>.get_instance();
		uint num;
		if (instance.RequireInstance((uint)buildingID, 1u, out num))
		{
			this.RenderInstance(cameraInfo, buildingID, layerMask, info, ref instance.m_instances[(int)((UIntPtr)num)]);
		}
	}

	private void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort buildingID, int layerMask, BuildingInfo info, ref RenderManager.Instance data)
	{
		if (data.m_dirty)
		{
			data.m_dirty = false;
			info.m_buildingAI.RefreshInstance(cameraInfo, buildingID, ref this, layerMask, ref data);
		}
		info.m_buildingAI.RenderInstance(cameraInfo, buildingID, ref this, layerMask, ref data);
		if (this.m_problems != Notification.Problem.None && (layerMask & 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer) != 0)
		{
			ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
			if (properties != null && properties.m_mode == ItemClass.Availability.AssetEditor)
			{
				return;
			}
			if (this.m_subBuilding != 0 && info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
			{
				Vector3 position;
				Quaternion quaternion;
				Vector3 vector;
				this.GetTotalPosition(out position, out quaternion, out vector);
				position.y += Mathf.Min(vector.y, data.m_dataVector0.y);
				Notification.RenderInstance(cameraInfo, this.m_problems, position, 1f);
			}
			else
			{
				Vector3 position2 = data.m_position;
				position2.y += Mathf.Min(info.m_size.y, data.m_dataVector0.y);
				Notification.RenderInstance(cameraInfo, this.m_problems, position2, 1f);
			}
		}
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, BuildingInfoBase info)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_lodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_lodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		if (info.m_lodCount < num)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(cameraInfo.m_forward * -100000f, Quaternion.get_identity(), Vector3.get_one());
			for (int i = info.m_lodCount; i < num; i++)
			{
				info.m_lodLocations[i] = matrix4x;
				info.m_lodStates[i] = Vector4.get_zero();
				info.m_lodObjectIndices[i] = Vector4.get_zero();
				info.m_lodColors[i] = Color.get_black();
			}
		}
		materialBlock.SetMatrixArray(instance.ID_BuildingLocations, info.m_lodLocations);
		materialBlock.SetVectorArray(instance.ID_BuildingStates, info.m_lodStates);
		materialBlock.SetVectorArray(instance.ID_ObjectIndices, info.m_lodObjectIndices);
		materialBlock.SetVectorArray(instance.ID_BuildingColors, info.m_lodColors);
		if (info.m_requireHeightMap)
		{
			materialBlock.SetTexture(instance.ID_HeightMap, info.m_lodHeightMap);
			materialBlock.SetVector(instance.ID_HeightMapping, info.m_lodHeightMapping);
			materialBlock.SetVector(instance.ID_SurfaceMapping, info.m_lodSurfaceMapping);
		}
		if (info.m_requireWaterMap)
		{
			materialBlock.SetTexture(instance.ID_WaterHeightMap, info.m_lodWaterHeightMap);
			materialBlock.SetVector(instance.ID_WaterHeightMapping, info.m_lodWaterHeightMapping);
			materialBlock.SetVector(instance.ID_WaterSurfaceMapping, info.m_lodWaterSurfaceMapping);
		}
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_lodMin - new Vector3(100f, 100f, 100f), info.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			BuildingManager expr_263_cp_0 = instance;
			expr_263_cp_0.m_drawCallData.m_lodCalls = expr_263_cp_0.m_drawCallData.m_lodCalls + 1;
			BuildingManager expr_276_cp_0 = instance;
			expr_276_cp_0.m_drawCallData.m_batchedCalls = expr_276_cp_0.m_drawCallData.m_batchedCalls + (info.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_lodMaterialCombined, info.m_prefabDataLayer, null, 0, materialBlock);
		}
		info.m_lodCount = 0;
	}

	public void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID)
	{
		Building.Flags flags = this.m_flags;
		if ((flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Hidden)) != Building.Flags.Created)
		{
			return;
		}
		BuildingInfo info = this.Info;
		info.m_buildingAI.PlayAudio(listenerInfo, buildingID, ref this);
	}

	public BuildingInfo Info
	{
		get
		{
			return PrefabCollection<BuildingInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public Citizen.SubCulture SubCultureType
	{
		get
		{
			return (Citizen.SubCulture)this.m_subCulture;
		}
		set
		{
			this.m_subCulture = (byte)value;
		}
	}

	public int Width
	{
		get
		{
			return (int)this.m_width;
		}
		set
		{
			this.m_width = (byte)Mathf.Clamp(value, 0, 16);
		}
	}

	public int Length
	{
		get
		{
			return (int)this.m_length;
		}
		set
		{
			this.m_length = (byte)Mathf.Clamp(value, 0, 16);
		}
	}

	public Building.Frame GetLastFrameData()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0;
		case 1:
			return this.m_frame1;
		case 2:
			return this.m_frame2;
		case 3:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public void SetLastFrameData(Building.Frame data)
	{
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public Building.Frame GetFrameData(uint simulationFrame)
	{
		simulationFrame = (simulationFrame >> 8 & 3u);
		switch (simulationFrame)
		{
		case 0u:
			return this.m_frame0;
		case 1u:
			return this.m_frame1;
		case 2u:
			return this.m_frame2;
		case 3u:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public void SetFrameData(uint simulationFrame, Building.Frame data)
	{
		this.m_lastFrame = (byte)(simulationFrame >> 8 & 3u);
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public void GetInfoWidthLength(out BuildingInfo info, out int width, out int length)
	{
		info = this.Info;
		width = this.Width;
		length = this.Length;
	}

	public static float CalculateLocalMeshOffset(BuildingInfo info, int length)
	{
		float num = (float)(length - info.m_cellLength) * 4f;
		if (!info.m_expandFrontYard)
		{
			num = -num;
		}
		return num;
	}

	public static Vector3 CalculateMeshPosition(BuildingInfo info, Vector3 position, float angle, int length)
	{
		float num = Building.CalculateLocalMeshOffset(info, length);
		return position + new Vector3(Mathf.Sin(angle), 0f, -Mathf.Cos(angle)) * num;
	}

	public static Quaternion CalculateMeshRotation(float angle)
	{
		return Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down());
	}

	public Vector3 CalculateMeshPosition()
	{
		float num = Building.CalculateLocalMeshOffset(this.Info, this.Length);
		return this.m_position + new Vector3(Mathf.Sin(this.m_angle), 0f, -Mathf.Cos(this.m_angle)) * num;
	}

	public void CalculateMeshPosition(out Vector3 meshPosition, out Quaternion meshRotation)
	{
		float num = Building.CalculateLocalMeshOffset(this.Info, this.Length);
		meshPosition = this.m_position + new Vector3(Mathf.Sin(this.m_angle), 0f, -Mathf.Cos(this.m_angle)) * num;
		meshRotation = Quaternion.AngleAxis(this.m_angle * 57.29578f, Vector3.get_down());
	}

	public Vector3 CalculateSidewalkPosition()
	{
		return Building.CalculateSidewalkPosition(this.m_position, this.m_angle, this.Length);
	}

	public Vector3 CalculateSidewalkPosition(float xOffset, float zOffset)
	{
		return Building.CalculateSidewalkPosition(this.m_position, this.m_angle, this.Length, xOffset, zOffset);
	}

	public static Vector3 CalculateSidewalkPosition(Vector3 position, float angle, int length)
	{
		float num = (float)length * 4f;
		return position - new Vector3(Mathf.Sin(angle) * num, 0f, -Mathf.Cos(angle) * num);
	}

	public static Vector3 CalculateSidewalkPosition(Vector3 position, float angle, int length, float xOffset, float zOffset)
	{
		Vector3 vector;
		vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		position += vector * xOffset - vector2 * ((float)length * 4f + zOffset);
		return position;
	}

	public Vector3 CalculatePosition(Vector3 offset)
	{
		return Building.CalculatePosition(this.m_position, this.m_angle, offset);
	}

	public static Vector3 CalculatePosition(Vector3 position, float angle, Vector3 offset)
	{
		Vector3 vector;
		vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		position += vector * offset.x + vector2 * offset.z;
		position.y += offset.y;
		return position;
	}

	private static void SampleHeight(BuildingInfo info, Vector3 pos, ref float minY, ref float maxY, float oldWaterHeight, ref float newWaterHeight)
	{
		float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(pos);
		minY = Mathf.Min(minY, num);
		maxY = Mathf.Max(maxY, num);
		if (info.m_placementMode == BuildingInfo.PlacementMode.OnSurface || info.m_placementMode == BuildingInfo.PlacementMode.OnWater || info.m_placementMode == BuildingInfo.PlacementMode.Shoreline || info.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround)
		{
			minY = Mathf.Min(minY, Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(pos));
			maxY = Mathf.Max(maxY, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(pos));
			if (oldWaterHeight < 0f)
			{
				float num2 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(pos));
				if (num2 > 0f)
				{
					num2 += 2f;
					newWaterHeight = Mathf.Max(newWaterHeight, num2);
					maxY = Mathf.Max(maxY, num2);
				}
			}
			else
			{
				maxY = Mathf.Max(maxY, oldWaterHeight);
			}
		}
	}

	public static void SampleBuildingHeight(Vector3 worldPos, float angle, int width, int length, BuildingInfo info, out float minY, out float maxY, out float buildingY)
	{
		float num = -1f;
		Building.SampleBuildingHeight(worldPos, angle, width, length, info, out minY, out maxY, out buildingY, ref num);
	}

	public static void SampleBuildingHeight(Vector3 worldPos, float angle, int width, int length, BuildingInfo info, out float minY, out float maxY, out float buildingY, ref float buildWaterHeight)
	{
		Vector2[] baseArea = info.m_generatedInfo.m_baseArea;
		Matrix4x4 matrix4x = Matrix4x4.TRS(worldPos, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
		minY = 100000f;
		maxY = -100000f;
		float oldWaterHeight = buildWaterHeight;
		if (baseArea == null || baseArea.Length == 0)
		{
			Building.SampleHeight(info, worldPos, ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
			Building.SampleHeight(info, matrix4x.MultiplyPoint(new Vector3((float)width * -4f, 0f, (float)length * -4f)), ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
			Building.SampleHeight(info, matrix4x.MultiplyPoint(new Vector3((float)width * -4f, 0f, (float)length * 4f)), ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
			Building.SampleHeight(info, matrix4x.MultiplyPoint(new Vector3((float)width * 4f, 0f, (float)length * 4f)), ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
			Building.SampleHeight(info, matrix4x.MultiplyPoint(new Vector3((float)width * 4f, 0f, (float)length * -4f)), ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
		}
		else
		{
			for (int i = 0; i < baseArea.Length; i += 2)
			{
				Vector3 vector = matrix4x.MultiplyPoint(new Vector3(baseArea[i].x, 0f, baseArea[i].y));
				Vector3 vector2 = matrix4x.MultiplyPoint(new Vector3(baseArea[i + 1].x, 0f, baseArea[i + 1].y));
				float num = Vector3.Distance(vector, vector2);
				int num2 = Mathf.CeilToInt(num / 4f);
				for (int j = 0; j <= num2; j++)
				{
					Vector3 pos;
					if (num2 == 0)
					{
						pos = Vector3.Lerp(vector, vector2, 0.5f);
					}
					else
					{
						pos = Vector3.Lerp(vector, vector2, (float)j / (float)num2);
					}
					Building.SampleHeight(info, pos, ref minY, ref maxY, oldWaterHeight, ref buildWaterHeight);
				}
			}
		}
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			Vector3 worldPos2 = matrix4x.MultiplyPoint(new Vector3(0f, 0f, (float)length * 4f + 8f));
			buildingY = Singleton<TerrainManager>.get_instance().SampleDetailHeight(worldPos2);
			worldPos2 = matrix4x.MultiplyPoint(new Vector3(-8f, 0f, (float)length * 4f + 8f));
			minY = Mathf.Min(minY, Singleton<TerrainManager>.get_instance().SampleDetailHeight(worldPos2));
			worldPos2 = matrix4x.MultiplyPoint(new Vector3(8f, 0f, (float)length * 4f + 8f));
			minY = Mathf.Min(minY, Singleton<TerrainManager>.get_instance().SampleDetailHeight(worldPos2));
		}
		else if (info.m_placementMode == BuildingInfo.PlacementMode.Shoreline || info.m_placementMode == BuildingInfo.PlacementMode.ShorelineOrGround)
		{
			buildingY = maxY + 0.5f;
		}
		else
		{
			buildingY = maxY;
		}
	}

	public void CalculateBuilding(ushort buildingID)
	{
		if ((this.m_flags & Building.Flags.Created) == Building.Flags.None)
		{
			return;
		}
		BuildingInfo info = this.Info;
		int width = this.Width;
		int length = this.Length;
		float num = -1f;
		float num2;
		float num3;
		float y;
		Building.SampleBuildingHeight(Building.CalculateMeshPosition(info, this.m_position, this.m_angle, length), this.m_angle, width, length, info, out num2, out num3, out y, ref num);
		this.m_buildWaterHeight = (ushort)Mathf.Clamp(Mathf.RoundToInt(num * 64f), 0, 65535);
		if ((this.m_flags & Building.Flags.FixedHeight) == Building.Flags.None)
		{
			this.m_position.y = y;
		}
		else
		{
			y = this.m_position.y;
		}
		if (y - num2 < 0.04f)
		{
			this.m_baseHeight = 0;
		}
		else
		{
			this.m_baseHeight = (byte)Mathf.Clamp(Mathf.CeilToInt(y - num2) + 1, 0, 255);
		}
	}

	public void UpdateBuilding(ushort buildingID)
	{
		if ((this.m_flags & Building.Flags.Created) == Building.Flags.None)
		{
			return;
		}
		BuildingInfo info = this.Info;
		int width = this.Width;
		int length = this.Length;
		Vector2 vector = new Vector3(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle));
		Vector2 vector2 = new Vector3(vector.y, -vector.x);
		vector *= (float)width * 4f;
		vector2 *= (float)length * 4f;
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - vector - vector2;
		quad.b = vector3 + vector - vector2;
		quad.c = vector3 + vector + vector2;
		quad.d = vector3 - vector + vector2;
		Vector2 vector4 = quad.Min();
		Vector2 vector5 = quad.Max();
		float num;
		if (info == null)
		{
			num = 32f;
			TerrainModify.UpdateArea(vector4.x, vector4.y, vector5.x, vector5.y, true, true, false);
		}
		else
		{
			num = info.m_buildingAI.ElectricityGridRadius();
			TerrainModify.UpdateArea(vector4.x, vector4.y, vector5.x, vector5.y, info.m_flattenTerrain || info.m_generatedInfo.m_heights.Length != 0, true, false);
		}
		Singleton<ZoneManager>.get_instance().UpdateBlocks(quad);
		Singleton<PropManager>.get_instance().UpdateProps(vector4.x, vector4.y, vector5.x, vector5.y);
		Singleton<TreeManager>.get_instance().UpdateTrees(vector4.x, vector4.y, vector5.x, vector5.y);
		if (num > 0.1f)
		{
			vector4.x -= num;
			vector4.y -= num;
			vector5.x += num;
			vector5.y += num;
			Singleton<ElectricityManager>.get_instance().UpdateGrid(vector4.x, vector4.y, vector5.x, vector5.y);
		}
	}

	public void UpdateTerrain(bool heights, bool surface)
	{
		int width = this.Width;
		int length = this.Length;
		Vector2 vector = new Vector3(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle));
		Vector2 vector2 = new Vector3(vector.y, -vector.x);
		vector *= (float)width * 4f;
		vector2 *= (float)length * 4f;
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - vector - vector2;
		quad.b = vector3 + vector - vector2;
		quad.c = vector3 + vector + vector2;
		quad.d = vector3 - vector + vector2;
		Vector2 vector4 = quad.Min();
		Vector2 vector5 = quad.Max();
		TerrainModify.UpdateArea(vector4.x, vector4.y, vector5.x, vector5.y, heights, surface, false);
	}

	public void TerrainUpdated(ushort buildingID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Demolishing)) != Building.Flags.Created)
		{
			return;
		}
		BuildingInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		Building.TerrainUpdated(info, buildingID, this.m_position, this.m_angle, this.Width, this.Length, minX, minZ, maxX, maxZ, (this.m_flags & Building.Flags.Collapsed) != Building.Flags.None);
	}

	public void AfterTerrainUpdated(ushort buildingID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Demolishing)) != Building.Flags.Created)
		{
			return;
		}
		BuildingInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		int width = this.Width;
		int length = this.Length;
		float num = (float)this.m_buildWaterHeight * 0.015625f;
		Vector3 vector = Building.CalculateMeshPosition(info, this.m_position, this.m_angle, length);
		float num2;
		float num3;
		float y;
		Building.SampleBuildingHeight(vector, this.m_angle, width, length, info, out num2, out num3, out y, ref num);
		bool flag = false;
		if ((this.m_flags & Building.Flags.FixedHeight) == Building.Flags.None && !info.m_flattenTerrain)
		{
			if (y != this.m_position.y)
			{
				flag = true;
				this.m_position.y = y;
				vector.y = y;
				float num4;
				float num5;
				info.m_buildingAI.GetTerrainLimits(buildingID, ref this, vector, out num4, out num5);
				if (num4 > -100000f || num5 < 100000f)
				{
					Singleton<BuildingManager>.get_instance().TerrainHeightUpdateNeeded(buildingID);
				}
			}
		}
		else
		{
			y = this.m_position.y;
		}
		byte b;
		if (y - num2 < 0.04f)
		{
			b = 0;
		}
		else
		{
			b = (byte)Mathf.Clamp(Mathf.CeilToInt(y - num2) + 1, 0, 255);
		}
		if (b != this.m_baseHeight)
		{
			flag = true;
			this.m_baseHeight = b;
		}
		Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, flag);
		if (!flag && info.m_lightsOnTerrain)
		{
			int num6 = Mathf.Clamp((int)(this.m_position.x / 64f + 135f), 0, 269);
			int num7 = Mathf.Clamp((int)(this.m_position.z / 64f + 135f), 0, 269);
			int x = num6 * 45 / 270;
			int z = num7 * 45 / 270;
			Singleton<RenderManager>.get_instance().UpdateGroup(x, z, Singleton<RenderManager>.get_instance().lightSystem.m_lightLayer);
		}
	}

	public static void TerrainUpdated(BuildingInfo info, ushort buildingID, Vector3 position, float angle, int width, int length, float minX, float minZ, float maxX, float maxZ, bool collapsed)
	{
		if (info.m_buildingAI.IsMarker())
		{
			return;
		}
		Vector3 vector = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Vector3 vector3 = position - ((float)width * 0.5f + 0.5f) * vector - ((float)length * 0.5f + 0.5f) * vector2;
		Vector3 vector4 = position + ((float)width * 0.5f + 0.5f) * vector - ((float)length * 0.5f + 0.5f) * vector2;
		Vector3 vector5 = position + ((float)width * 0.5f + 0.5f) * vector + ((float)length * 0.5f + 0.5f) * vector2;
		Vector3 vector6 = position - ((float)width * 0.5f + 0.5f) * vector + ((float)length * 0.5f + 0.5f) * vector2;
		float num = Mathf.Min(Mathf.Min(vector3.x, vector4.x), Mathf.Min(vector5.x, vector6.x));
		float num2 = Mathf.Min(Mathf.Min(vector3.z, vector4.z), Mathf.Min(vector5.z, vector6.z));
		float num3 = Mathf.Max(Mathf.Max(vector3.x, vector4.x), Mathf.Max(vector5.x, vector6.x));
		float num4 = Mathf.Max(Mathf.Max(vector3.z, vector4.z), Mathf.Max(vector5.z, vector6.z));
		if (num3 >= minX && num <= maxX && num4 >= minZ && num2 <= maxZ)
		{
			bool flag = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None || info.m_placementMode != BuildingInfo.PlacementMode.Roadside;
			Vector3 vector7 = Building.CalculateMeshPosition(info, position, angle, length);
			Vector2[] baseArea = info.m_generatedInfo.m_baseArea;
			Vector2[] pavementAreas = info.m_pavementAreas;
			TerrainModify.Surface[] cellSurfaces = info.m_cellSurfaces;
			Matrix4x4 matrix4x = Matrix4x4.TRS(vector7, Quaternion.AngleAxis(angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
			if (info.m_flattenTerrain)
			{
				Vector3 vector8 = vector / 8f;
				Vector3 vector9 = vector2 / 8f;
				Vector3 vector10 = position - ((float)width * 0.5f + 0.1f) * vector - ((float)length * 0.5f + 0.1f) * vector2;
				Vector3 vector11 = position + ((float)width * 0.5f + 0.1f) * vector - ((float)length * 0.5f + 0.1f) * vector2;
				Vector3 vector12 = position + ((float)width * 0.5f + 0.1f) * vector + ((float)length * 0.5f + 0.1f) * vector2;
				Vector3 vector13 = position - ((float)width * 0.5f + 0.1f) * vector + ((float)length * 0.5f + 0.1f) * vector2;
				Vector3 vector14;
				Vector3 vector15;
				Vector3 vector16;
				Vector3 vector17;
				if (info.m_flattenFullArea)
				{
					vector14 = vector10;
					vector15 = vector11;
					vector16 = vector12;
					vector17 = vector13;
				}
				else
				{
					vector14 = vector7 + info.m_generatedInfo.m_min.x * vector8 - info.m_generatedInfo.m_max.z * vector9;
					vector15 = vector7 + info.m_generatedInfo.m_max.x * vector8 - info.m_generatedInfo.m_max.z * vector9;
					vector16 = vector7 + info.m_generatedInfo.m_max.x * vector8 - info.m_generatedInfo.m_min.z * vector9;
					vector17 = vector7 + info.m_generatedInfo.m_min.x * vector8 - info.m_generatedInfo.m_min.z * vector9;
				}
				float num5 = Mathf.Max(0f, Vector2.Dot(VectorUtils.XZ(vector9), VectorUtils.XZ(vector14 - vector10)));
				float num6 = Mathf.Max(0f, Vector2.Dot(VectorUtils.XZ(vector8), VectorUtils.XZ(vector11 - vector15)));
				float num7 = Mathf.Max(0f, Vector2.Dot(VectorUtils.XZ(vector9), VectorUtils.XZ(vector12 - vector16)));
				float num8 = Mathf.Max(0f, Vector2.Dot(VectorUtils.XZ(vector8), VectorUtils.XZ(vector17 - vector13)));
				TerrainModify.Edges edges = TerrainModify.Edges.None;
				TerrainModify.Heights heights = TerrainModify.Heights.SecondaryLevel;
				TerrainModify.Surface surface = TerrainModify.Surface.None;
				if (num5 > 4f)
				{
					Vector3 vector18 = vector14 - num5 * vector9;
					Vector3 vector19 = vector15 - num5 * vector9;
					vector18.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector18), vector14.y - num5 * 0.25f, vector14.y + num5 * 0.25f);
					vector19.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector19), vector15.y - num5 * 0.25f, vector15.y + num5 * 0.25f);
					TerrainModify.Edges edges2 = TerrainModify.Edges.AB;
					if (num6 <= 4f)
					{
						edges2 |= TerrainModify.Edges.BC;
					}
					if (num8 <= 4f)
					{
						edges2 |= TerrainModify.Edges.DA;
					}
					TerrainModify.ApplyQuad(vector18, vector19, vector15, vector14, edges2, heights, surface);
					if (num8 > 4f)
					{
						Vector3 vector20 = vector14 - num8 * vector8;
						vector20.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector20), vector14.y - num8 * 0.25f, vector14.y + num8 * 0.25f);
						vector10.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector10), vector14.y - Mathf.Max(num5, num8) * 0.25f, vector14.y + Mathf.Max(num5, num8) * 0.25f);
						TerrainModify.ApplyQuad(vector10, vector18, vector14, vector20, TerrainModify.Edges.AB | TerrainModify.Edges.DA, heights, surface);
					}
					if (num6 > 4f)
					{
						Vector3 vector21 = vector15 + num6 * vector8;
						vector21.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector21), vector15.y - num6 * 0.25f, vector15.y + num6 * 0.25f);
						vector11.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector11), vector15.y - Mathf.Max(num5, num6) * 0.25f, vector15.y + Mathf.Max(num5, num6) * 0.25f);
						TerrainModify.ApplyQuad(vector19, vector11, vector21, vector15, TerrainModify.Edges.AB | TerrainModify.Edges.BC, heights, surface);
					}
				}
				else
				{
					edges |= TerrainModify.Edges.AB;
				}
				if (num6 > 4f)
				{
					Vector3 vector22 = vector15 + num6 * vector8;
					Vector3 vector23 = vector16 + num6 * vector8;
					vector22.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector22), vector15.y - num6 * 0.25f, vector15.y + num6 * 0.25f);
					vector23.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector23), vector16.y - num6 * 0.25f, vector16.y + num6 * 0.25f);
					TerrainModify.Edges edges3 = TerrainModify.Edges.BC;
					if (num5 <= 4f)
					{
						edges3 |= TerrainModify.Edges.AB;
					}
					if (num7 <= 4f)
					{
						edges3 |= TerrainModify.Edges.CD;
					}
					TerrainModify.ApplyQuad(vector15, vector22, vector23, vector16, edges3, heights, surface);
				}
				else
				{
					edges |= TerrainModify.Edges.BC;
				}
				if (num7 > 4f)
				{
					Vector3 vector24 = vector16 + num7 * vector9;
					Vector3 vector25 = vector17 + num7 * vector9;
					vector24.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector24), vector16.y - num7 * 0.25f, vector16.y + num7 * 0.25f);
					vector25.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector25), vector17.y - num7 * 0.25f, vector17.y + num7 * 0.25f);
					TerrainModify.Edges edges4 = TerrainModify.Edges.CD;
					if (num6 <= 4f)
					{
						edges4 |= TerrainModify.Edges.BC;
					}
					if (num8 <= 4f)
					{
						edges4 |= TerrainModify.Edges.DA;
					}
					TerrainModify.ApplyQuad(vector17, vector16, vector24, vector25, edges4, heights, surface);
					if (num8 > 4f)
					{
						Vector3 vector26 = vector17 - num8 * vector8;
						vector26.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector26), vector17.y - num8 * 0.25f, vector17.y + num8 * 0.25f);
						vector13.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector13), vector17.y - Mathf.Max(num7, num8) * 0.25f, vector17.y + Mathf.Max(num7, num8) * 0.25f);
						TerrainModify.ApplyQuad(vector26, vector17, vector25, vector13, TerrainModify.Edges.CD | TerrainModify.Edges.DA, heights, surface);
					}
					if (num6 > 4f)
					{
						Vector3 vector27 = vector16 + num6 * vector8;
						vector27.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector27), vector16.y - num6 * 0.25f, vector16.y + num6 * 0.25f);
						vector12.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector12), vector16.y - Mathf.Max(num7, num6) * 0.25f, vector16.y + Mathf.Max(num7, num6) * 0.25f);
						TerrainModify.ApplyQuad(vector16, vector27, vector12, vector24, TerrainModify.Edges.BC | TerrainModify.Edges.CD, heights, surface);
					}
				}
				else
				{
					edges |= TerrainModify.Edges.CD;
				}
				if (num8 > 4f)
				{
					Vector3 vector28 = vector17 - num8 * vector8;
					Vector3 vector29 = vector14 - num8 * vector8;
					vector28.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector28), vector17.y - num8 * 0.25f, vector17.y + num8 * 0.25f);
					vector29.y = Mathf.Clamp(Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector29), vector14.y - num8 * 0.25f, vector14.y + num8 * 0.25f);
					TerrainModify.Edges edges5 = TerrainModify.Edges.DA;
					if (num5 <= 4f)
					{
						edges5 |= TerrainModify.Edges.AB;
					}
					if (num7 <= 4f)
					{
						edges5 |= TerrainModify.Edges.CD;
					}
					TerrainModify.ApplyQuad(vector29, vector14, vector17, vector28, edges5, heights, surface);
				}
				else
				{
					edges |= TerrainModify.Edges.DA;
				}
				TerrainModify.ApplyQuad(vector14, vector15, vector16, vector17, edges, heights, surface);
			}
			else if (buildingID != 0)
			{
				Vector3 vector30 = vector / 8f;
				Vector3 vector31 = vector2 / 8f;
				Vector3 a = vector7 + info.m_generatedInfo.m_min.x * vector30 - info.m_generatedInfo.m_max.z * vector31;
				Vector3 b = vector7 + info.m_generatedInfo.m_max.x * vector30 - info.m_generatedInfo.m_max.z * vector31;
				Vector3 c = vector7 + info.m_generatedInfo.m_max.x * vector30 - info.m_generatedInfo.m_min.z * vector31;
				Vector3 d = vector7 + info.m_generatedInfo.m_min.x * vector30 - info.m_generatedInfo.m_min.z * vector31;
				TerrainModify.Edges edges6 = TerrainModify.Edges.All;
				TerrainModify.Surface surface2 = TerrainModify.Surface.None;
				float num9;
				float num10;
				info.m_buildingAI.GetTerrainLimits(buildingID, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)buildingID], vector7, out num9, out num10);
				if (num9 > -100000f)
				{
					TerrainModify.Heights heights2 = TerrainModify.Heights.SecondaryMin;
					a.y = num9;
					b.y = num9;
					c.y = num9;
					d.y = num9;
					TerrainModify.ApplyQuad(a, b, c, d, edges6, heights2, surface2);
				}
				if (num10 < 100000f)
				{
					TerrainModify.Heights heights3 = TerrainModify.Heights.SecondaryMax;
					a.y = num10;
					b.y = num10;
					c.y = num10;
					d.y = num10;
					TerrainModify.ApplyQuad(a, b, c, d, edges6, heights3, surface2);
				}
			}
			if (info.m_fullPavement || info.m_fullGravel || info.m_clipTerrain)
			{
				if (info.m_fullPavement || info.m_fullGravel)
				{
					Vector3 a2 = position - ((float)width * 0.5f + 0.1f) * vector - ((float)length * 0.5f + ((!flag) ? 1f : 0.1f)) * vector2;
					Vector3 b2 = position + ((float)width * 0.5f + 0.1f) * vector - ((float)length * 0.5f + ((!flag) ? 1f : 0.1f)) * vector2;
					Vector3 c2 = position + ((float)width * 0.5f + 0.1f) * vector + ((float)length * 0.5f + 0.1f) * vector2;
					Vector3 d2 = position - ((float)width * 0.5f + 0.1f) * vector + ((float)length * 0.5f + 0.1f) * vector2;
					TerrainModify.Edges edges7 = TerrainModify.Edges.All;
					TerrainModify.Heights heights4 = TerrainModify.Heights.None;
					TerrainModify.Surface surface3 = TerrainModify.Surface.None;
					if (info.m_fullPavement)
					{
						surface3 |= TerrainModify.Surface.PavementB;
					}
					if (info.m_fullGravel)
					{
						surface3 |= TerrainModify.Surface.Gravel;
					}
					TerrainModify.ApplyQuad(a2, b2, c2, d2, edges7, heights4, surface3);
				}
				if (info.m_clipTerrain && !collapsed)
				{
					Vector3 a3 = position - ((float)width * 0.5f - 0.25f) * vector - ((float)length * 0.5f - 0.25f) * vector2;
					Vector3 b3 = position + ((float)width * 0.5f - 0.25f) * vector - ((float)length * 0.5f - 0.25f) * vector2;
					Vector3 c3 = position + ((float)width * 0.5f - 0.25f) * vector + ((float)length * 0.5f - 0.25f) * vector2;
					Vector3 d3 = position - ((float)width * 0.5f - 0.25f) * vector + ((float)length * 0.5f - 0.25f) * vector2;
					TerrainModify.Edges edges8 = TerrainModify.Edges.All;
					TerrainModify.Heights heights5 = TerrainModify.Heights.None;
					TerrainModify.Surface surface4 = TerrainModify.Surface.Clip;
					TerrainModify.ApplyQuad(a3, b3, c3, d3, edges8, heights5, surface4);
				}
			}
			else
			{
				for (int i = 0; i < baseArea.Length; i += 2)
				{
					Vector3 vector32 = matrix4x.MultiplyPoint(new Vector3(baseArea[i].x, 0f, baseArea[i].y));
					Vector3 vector33 = matrix4x.MultiplyPoint(new Vector3(baseArea[i + 1].x, 0f, baseArea[i + 1].y));
					Vector3 normalized = (vector33 - vector32).get_normalized();
					Vector3 c4 = vector33 + new Vector3(normalized.z, 0f, -normalized.x);
					Vector3 d4 = vector32 + new Vector3(normalized.z, 0f, -normalized.x);
					TerrainModify.Edges edges9 = TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.CD | TerrainModify.Edges.DA | TerrainModify.Edges.Expand;
					TerrainModify.Heights heights6 = TerrainModify.Heights.None;
					TerrainModify.Surface surface5 = (!info.m_weakTerrainRuining) ? TerrainModify.Surface.Ruined : TerrainModify.Surface.RuinedWeak;
					TerrainModify.ApplyQuad(vector32, vector33, c4, d4, edges9, heights6, surface5);
				}
				float num11 = (float)length * 4f - 0.1f;
				for (int j = 0; j < pavementAreas.Length; j += 4)
				{
					Vector3 vector34;
					vector34..ctor(pavementAreas[j].x, 0f, pavementAreas[j].y);
					Vector3 vector35;
					vector35..ctor(pavementAreas[j + 1].x, 0f, pavementAreas[j + 1].y);
					Vector3 vector36;
					vector36..ctor(pavementAreas[j + 2].x, 0f, pavementAreas[j + 2].y);
					Vector3 vector37;
					vector37..ctor(pavementAreas[j + 3].x, 0f, pavementAreas[j + 3].y);
					if (vector34.z > num11 && !flag)
					{
						vector34.z = num11 + 8f;
					}
					if (vector35.z > num11 && !flag)
					{
						vector35.z = num11 + 8f;
					}
					if (vector36.z > num11 && !flag)
					{
						vector36.z = num11 + 8f;
					}
					if (vector37.z > num11 && !flag)
					{
						vector37.z = num11 + 8f;
					}
					vector34 = matrix4x.MultiplyPoint(vector34);
					vector35 = matrix4x.MultiplyPoint(vector35);
					vector36 = matrix4x.MultiplyPoint(vector36);
					vector37 = matrix4x.MultiplyPoint(vector37);
					TerrainModify.Edges edges10 = TerrainModify.Edges.All;
					TerrainModify.Heights heights7 = TerrainModify.Heights.None;
					TerrainModify.Surface surface6 = TerrainModify.Surface.PavementB;
					TerrainModify.ApplyQuad(vector34, vector35, vector36, vector37, edges10, heights7, surface6);
				}
				if (cellSurfaces != null && cellSurfaces.Length != 0 && width != 0)
				{
					int num12 = cellSurfaces.Length / width;
					if (num12 != 0)
					{
						float num13 = (float)length * -0.5f;
						float num14 = (float)width * -0.5f;
						int num15 = 0;
						if (info.m_expandFrontYard)
						{
							num15 += num12 - length;
							num13 -= (float)(length - info.m_cellLength) * 0.5f;
						}
						else
						{
							num13 += (float)(length - info.m_cellLength) * 0.5f;
						}
						for (int k = 0; k < length; k++)
						{
							Building.TempMask[k] = 0;
						}
						for (int l = 0; l < length; l++)
						{
							int num16 = l + num15;
							if (num16 >= 0 && num16 < num12)
							{
								for (int m = 0; m < width; m++)
								{
									if (((int)Building.TempMask[l] & 1 << m) == 0)
									{
										TerrainModify.Surface surface7 = cellSurfaces[num16 * width + m];
										if (surface7 != TerrainModify.Surface.None)
										{
											TerrainModify.Edges edges11 = TerrainModify.Edges.None;
											if (l == 0 || num16 == 0 || cellSurfaces[(num16 - 1) * width + m] != surface7)
											{
												edges11 |= TerrainModify.Edges.DA;
											}
											if (l == length - 1 || num16 == num12 - 1 || cellSurfaces[(num16 + 1) * width + m] != surface7)
											{
												edges11 |= TerrainModify.Edges.BC;
											}
											int n;
											for (n = m + 1; n < width; n++)
											{
												if (cellSurfaces[num16 * width + n] != surface7)
												{
													break;
												}
												TerrainModify.Edges edges12 = TerrainModify.Edges.None;
												if (l == 0 || num16 == 0 || cellSurfaces[(num16 - 1) * width + n] != surface7)
												{
													edges12 |= TerrainModify.Edges.DA;
												}
												if (l == length - 1 || num16 == num12 - 1 || cellSurfaces[(num16 + 1) * width + n] != surface7)
												{
													edges12 |= TerrainModify.Edges.BC;
												}
												if ((edges11 & (TerrainModify.Edges.BC | TerrainModify.Edges.DA)) != edges12)
												{
													break;
												}
											}
											if (m == 0 || cellSurfaces[num16 * width + m - 1] != surface7)
											{
												edges11 |= TerrainModify.Edges.AB;
											}
											if (n == width || cellSurfaces[num16 * width + n] != surface7)
											{
												edges11 |= TerrainModify.Edges.CD;
											}
											int num17 = l + 1;
											while (num17 < length && (edges11 & TerrainModify.Edges.BC) == TerrainModify.Edges.None)
											{
												int num18 = num17 + num15;
												if (num18 >= num12)
												{
													break;
												}
												TerrainModify.Edges edges13 = TerrainModify.Edges.None;
												if (m == 0 || cellSurfaces[num18 * width + m - 1] != surface7)
												{
													edges13 |= TerrainModify.Edges.AB;
												}
												if (n == width || cellSurfaces[num18 * width + n] != surface7)
												{
													edges13 |= TerrainModify.Edges.CD;
												}
												if ((edges11 & (TerrainModify.Edges.AB | TerrainModify.Edges.CD)) != edges13)
												{
													break;
												}
												if (num17 == length - 1 || num18 == num12 - 1 || cellSurfaces[(num18 + 1) * width + m] != surface7)
												{
													edges11 |= TerrainModify.Edges.BC;
												}
												TerrainModify.Edges edges14 = TerrainModify.Edges.None;
												for (int num19 = m + 1; num19 < n; num19++)
												{
													edges14 = TerrainModify.Edges.None;
													if (num17 == length - 1 || num18 == num12 - 1 || cellSurfaces[(num18 + 1) * width + num19] != surface7)
													{
														edges14 |= TerrainModify.Edges.BC;
													}
													if (edges14 != (edges11 & TerrainModify.Edges.BC))
													{
														break;
													}
												}
												if (edges14 != (edges11 & TerrainModify.Edges.BC))
												{
													break;
												}
												for (int num20 = m; num20 < n; num20++)
												{
													ushort[] expr_15F3_cp_0 = Building.TempMask;
													int expr_15F3_cp_1 = num17;
													expr_15F3_cp_0[expr_15F3_cp_1] |= (ushort)(1 << num20);
												}
												num17++;
											}
											float num21 = (m != 0) ? ((float)m + num14) : ((float)m + num14 - 0.1f);
											float num22 = (n != width) ? ((float)n + num14) : ((float)n + num14 + 0.1f);
											float num23 = (l != 0) ? ((float)l + num13) : ((float)l + num13 - ((!flag && (surface7 & (TerrainModify.Surface.Clip | TerrainModify.Surface.Ruined | TerrainModify.Surface.Field)) == TerrainModify.Surface.None) ? 1f : 0.1f));
											float num24 = (num17 != length) ? ((float)num17 + num13) : ((float)num17 + num13 + 0.1f);
											Vector3 a4 = vector7 - num21 * vector + num23 * vector2;
											Vector3 b4 = vector7 - num21 * vector + num24 * vector2;
											Vector3 c5 = vector7 - num22 * vector + num24 * vector2;
											Vector3 d5 = vector7 - num22 * vector + num23 * vector2;
											if (collapsed)
											{
												surface7 &= ~TerrainModify.Surface.Clip;
											}
											TerrainModify.ApplyQuad(a4, b4, c5, d5, edges11, TerrainModify.Heights.None, surface7);
										}
									}
								}
							}
						}
					}
				}
			}
			if (info.m_generatedInfo.m_heights != null && info.m_generatedInfo.m_heights.Length != 0 && info.m_placementMode != BuildingInfo.PlacementMode.Shoreline && info.m_placementMode != BuildingInfo.PlacementMode.ShorelineOrGround && info.m_placementMode != BuildingInfo.PlacementMode.OnWater && !collapsed)
			{
				Vector3 a5 = matrix4x.MultiplyPoint(new Vector3(info.m_generatedInfo.m_min.x * 1.05f, 0f, info.m_generatedInfo.m_min.z * 1.05f));
				Vector3 b5 = matrix4x.MultiplyPoint(new Vector3(info.m_generatedInfo.m_min.x * 1.05f, 0f, info.m_generatedInfo.m_max.z * 1.05f));
				Vector3 c6 = matrix4x.MultiplyPoint(new Vector3(info.m_generatedInfo.m_max.x * 1.05f, 0f, info.m_generatedInfo.m_max.z * 1.05f));
				Vector3 d6 = matrix4x.MultiplyPoint(new Vector3(info.m_generatedInfo.m_max.x * 1.05f, 0f, info.m_generatedInfo.m_min.z * 1.05f));
				TerrainModify.Edges edges15 = TerrainModify.Edges.None;
				TerrainModify.Heights heights8 = TerrainModify.Heights.BlockHeight;
				TerrainModify.ApplyQuad(a5, b5, c6, d6, edges15, heights8, info.m_generatedInfo.m_heights);
			}
			if (buildingID != 0 && info.m_props != null)
			{
				for (int num25 = 0; num25 < info.m_props.Length; num25++)
				{
					Randomizer randomizer;
					randomizer..ctor((int)buildingID << 6 | info.m_props[num25].m_index);
					if (randomizer.Int32(100u) < info.m_props[num25].m_probability && length >= info.m_props[num25].m_requiredLength)
					{
						PropInfo propInfo = info.m_props[num25].m_finalProp;
						TreeInfo treeInfo = info.m_props[num25].m_finalTree;
						if (propInfo != null)
						{
							propInfo = propInfo.GetVariation(ref randomizer);
							if (propInfo.m_createRuining)
							{
								Vector3 vector38 = matrix4x.MultiplyPoint(info.m_props[num25].m_position);
								Vector3 a6 = vector38 + new Vector3(-4.5f, 0f, -4.5f);
								Vector3 b6 = vector38 + new Vector3(-4.5f, 0f, 4.5f);
								Vector3 c7 = vector38 + new Vector3(4.5f, 0f, 4.5f);
								Vector3 d7 = vector38 + new Vector3(4.5f, 0f, -4.5f);
								TerrainModify.Edges edges16 = TerrainModify.Edges.All;
								TerrainModify.Heights heights9 = TerrainModify.Heights.None;
								TerrainModify.Surface surface8 = TerrainModify.Surface.Ruined;
								TerrainModify.ApplyQuad(a6, b6, c7, d7, edges16, heights9, surface8);
							}
						}
						else if (treeInfo != null)
						{
							treeInfo = treeInfo.GetVariation(ref randomizer);
							if (treeInfo.m_createRuining)
							{
								Vector3 vector39 = matrix4x.MultiplyPoint(info.m_props[num25].m_position);
								Vector3 a7 = vector39 + new Vector3(-4.5f, 0f, -4.5f);
								Vector3 b7 = vector39 + new Vector3(-4.5f, 0f, 4.5f);
								Vector3 c8 = vector39 + new Vector3(4.5f, 0f, 4.5f);
								Vector3 d8 = vector39 + new Vector3(4.5f, 0f, -4.5f);
								TerrainModify.Edges edges17 = TerrainModify.Edges.All;
								TerrainModify.Heights heights10 = TerrainModify.Heights.None;
								TerrainModify.Surface surface9 = TerrainModify.Surface.Ruined;
								TerrainModify.ApplyQuad(a7, b7, c8, d8, edges17, heights10, surface9);
							}
						}
					}
				}
			}
		}
	}

	public bool OverlapQuad(ushort buildingID, Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		BuildingInfo info = this.Info;
		int width = this.Width;
		int length = this.Length;
		if (width == 0 || length == 0)
		{
			return false;
		}
		float num = Mathf.Min(72f, (float)(width + length) * 4f);
		if (this.m_position.x - num > vector2.x || this.m_position.x + num < vector.x)
		{
			return false;
		}
		if (this.m_position.z - num > vector2.y || this.m_position.z + num < vector.y)
		{
			return false;
		}
		ItemClass.CollisionType collisionType2 = info.m_buildingAI.GetCollisionType();
		float minY2 = this.m_position.y - (float)this.m_baseHeight;
		float maxY2 = this.m_position.y + info.m_collisionHeight;
		if (!ItemClass.CheckCollisionType(minY, maxY, minY2, maxY2, collisionType, collisionType2))
		{
			return false;
		}
		Vector2 vector3 = VectorUtils.XZ(this.m_position);
		Vector2 vector4;
		vector4..ctor(Mathf.Cos(this.m_angle), Mathf.Sin(this.m_angle));
		Vector2 vector5;
		vector5..ctor(vector4.y, -vector4.x);
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			vector4 *= (float)width * 4f - 0.8f;
			vector5 *= (float)length * 4f - 0.8f;
		}
		else
		{
			vector4 *= (float)width * 4f;
			vector5 *= (float)length * 4f;
		}
		if (info.m_circular)
		{
			vector4 *= 0.7f;
			vector5 *= 0.7f;
		}
		Quad2 quad2 = default(Quad2);
		quad2.a = vector3 - vector4 - vector5;
		quad2.b = vector3 + vector4 - vector5;
		quad2.c = vector3 + vector4 + vector5;
		quad2.d = vector3 - vector4 + vector5;
		return quad.Intersect(quad2);
	}

	public bool RayCast(ushort buildingID, Segment3 ray, out float t)
	{
		t = 2f;
		BuildingInfo info = this.Info;
		float num = (float)(this.Width << 2);
		float num2 = (float)(this.Length << 2);
		ray.a -= this.m_position;
		ray.b -= this.m_position;
		float num3 = Mathf.Cos(-this.m_angle);
		float num4 = Mathf.Sin(-this.m_angle);
		Segment3 segment;
		segment.a.x = num3 * ray.a.x - num4 * ray.a.z;
		segment.a.y = ray.a.y;
		segment.a.z = num3 * ray.a.z + num4 * ray.a.x;
		segment.b.x = num3 * ray.b.x - num4 * ray.b.z;
		segment.b.y = ray.b.y;
		segment.b.z = num3 * ray.b.z + num4 * ray.b.x;
		Quad3 quad = default(Quad3);
		quad.a = new Vector3(-num, 0f, -num2);
		quad.b = new Vector3(num, 0f, -num2);
		quad.c = new Vector3(num, 0f, num2);
		quad.d = new Vector3(-num, 0f, num2);
		Bounds bounds;
		bounds..ctor(new Vector3(0f, info.m_size.y * 0.5f, 0f), new Vector3(num * 2f, info.m_size.y, num2 * 2f));
		if (!bounds.IntersectRay(new Ray(segment.a, segment.b - segment.a)))
		{
			return false;
		}
		float num5;
		if (quad.Intersect(segment, ref num5) && num5 < t)
		{
			t = num5;
		}
		if ((this.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			BuildingInfoBase collapsedInfo = info.m_collapsedInfo;
			if (collapsedInfo != null && collapsedInfo.m_lodMeshData != null)
			{
				Randomizer randomizer;
				randomizer..ctor((int)buildingID);
				int num6 = randomizer.Int32(4u);
				if ((1 << num6 & info.m_collapsedRotations) == 0)
				{
					num6 = (num6 + 1 & 3);
				}
				Vector3 vector = info.m_generatedInfo.m_min;
				Vector3 vector2 = info.m_generatedInfo.m_max;
				float num7 = (float)this.Width * 4f;
				float num8 = (float)this.Length * 4f;
				float num9 = Building.CalculateLocalMeshOffset(info, this.Length);
				vector = Vector3.Max(vector - new Vector3(4f, 0f, 4f + num9), new Vector3(-num7, 0f, -num8));
				vector2 = Vector3.Min(vector2 + new Vector3(4f, 0f, 4f - num9), new Vector3(num7, 0f, num8));
				Vector3 vector3 = (vector + vector2) * 0.5f;
				Vector3 vector4 = vector2 - vector;
				float num10 = (((num6 & 1) != 0) ? vector4.z : vector4.x) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
				float num11 = (((num6 & 1) != 0) ? vector4.x : vector4.z) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
				float num12 = -vector3.x;
				float num13 = -vector3.z;
				Vector3[] vertices = collapsedInfo.m_lodMeshData.m_vertices;
				int[] triangles = collapsedInfo.m_lodMeshData.m_triangles;
				if (triangles != null)
				{
					segment.a.x = segment.a.x + num12;
					segment.b.x = segment.b.x + num12;
					segment.a.z = segment.a.z + num13;
					segment.b.z = segment.b.z + num13;
					if (num6 == 1)
					{
						segment.a = new Vector3(segment.a.z, segment.a.y, -segment.a.x);
						segment.b = new Vector3(segment.b.z, segment.b.y, -segment.b.x);
					}
					else if (num6 == 2)
					{
						segment.a = new Vector3(-segment.a.x, segment.a.y, -segment.a.z);
						segment.b = new Vector3(-segment.b.x, segment.b.y, -segment.b.z);
					}
					else if (num6 == 3)
					{
						segment.a = new Vector3(-segment.a.z, segment.a.y, segment.a.x);
						segment.b = new Vector3(-segment.b.z, segment.b.y, segment.b.x);
					}
					segment.a.x = segment.a.x / num10;
					segment.b.x = segment.b.x / num10;
					segment.a.z = segment.a.z / num11;
					segment.b.z = segment.b.z / num11;
					int num14 = triangles.Length;
					for (int i = 0; i < num14; i += 3)
					{
						float num15;
						float num16;
						float num17;
						if (Triangle3.Intersect(vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]], segment.a, segment.b, ref num15, ref num16, ref num17) && num17 < t)
						{
							t = num17;
						}
					}
				}
			}
		}
		else if (info.m_lodMeshData != null)
		{
			Vector3[] vertices2 = info.m_lodMeshData.m_vertices;
			int[] triangles2 = info.m_lodMeshData.m_triangles;
			if (triangles2 != null)
			{
				float num18 = Building.CalculateLocalMeshOffset(info, this.Length);
				segment.a.z = segment.a.z + num18;
				segment.b.z = segment.b.z + num18;
				int num19 = triangles2.Length;
				for (int j = 0; j < num19; j += 3)
				{
					float num20;
					float num21;
					float num22;
					if (Triangle3.Intersect(vertices2[triangles2[j]], vertices2[triangles2[j + 1]], vertices2[triangles2[j + 2]], segment.a, segment.b, ref num20, ref num21, ref num22) && num22 < t)
					{
						t = num22;
					}
				}
			}
		}
		return t != 2f;
	}

	public void AddOwnVehicle(ushort vehicleID, ref Vehicle data)
	{
		data.m_nextOwnVehicle = this.m_ownVehicles;
		this.m_ownVehicles = vehicleID;
	}

	public void RemoveOwnVehicle(ushort vehicleID, ref Vehicle data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = 0;
		ushort num2 = this.m_ownVehicles;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == vehicleID)
			{
				if (num != 0)
				{
					instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle = data.m_nextOwnVehicle;
				}
				else
				{
					this.m_ownVehicles = data.m_nextOwnVehicle;
				}
				data.m_nextOwnVehicle = 0;
				return;
			}
			num = num2;
			num2 = instance.m_vehicles.m_buffer[(int)num2].m_nextOwnVehicle;
			if (++num3 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		CODebugBase<LogChannel>.Error(LogChannel.Core, "Vehicle not found!\n" + Environment.StackTrace);
	}

	public void AddGuestVehicle(ushort vehicleID, ref Vehicle data)
	{
		data.m_nextGuestVehicle = this.m_guestVehicles;
		this.m_guestVehicles = vehicleID;
	}

	public void RemoveGuestVehicle(ushort vehicleID, ref Vehicle data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = 0;
		ushort num2 = this.m_guestVehicles;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == vehicleID)
			{
				if (num != 0)
				{
					instance.m_vehicles.m_buffer[(int)num].m_nextGuestVehicle = data.m_nextGuestVehicle;
				}
				else
				{
					this.m_guestVehicles = data.m_nextGuestVehicle;
				}
				data.m_nextGuestVehicle = 0;
				return;
			}
			num = num2;
			num2 = instance.m_vehicles.m_buffer[(int)num2].m_nextGuestVehicle;
			if (++num3 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		CODebugBase<LogChannel>.Error(LogChannel.Core, "Vehicle not found!\n" + Environment.StackTrace);
	}

	public void AddSourceCitizen(ushort instanceID, ref CitizenInstance data)
	{
		data.m_nextSourceInstance = this.m_sourceCitizens;
		this.m_sourceCitizens = instanceID;
	}

	public void AddTargetCitizen(ushort instanceID, ref CitizenInstance data)
	{
		data.m_nextTargetInstance = this.m_targetCitizens;
		this.m_targetCitizens = instanceID;
	}

	public void RemoveSourceCitizen(ushort instanceID, ref CitizenInstance data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = 0;
		ushort num2 = this.m_sourceCitizens;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == instanceID)
			{
				if (num != 0)
				{
					instance.m_instances.m_buffer[(int)num].m_nextSourceInstance = data.m_nextSourceInstance;
				}
				else
				{
					this.m_sourceCitizens = data.m_nextSourceInstance;
				}
				data.m_nextSourceInstance = 0;
				return;
			}
			num = num2;
			num2 = instance.m_instances.m_buffer[(int)num2].m_nextSourceInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		CODebugBase<LogChannel>.Error(LogChannel.Core, "Citizen not found!\n" + Environment.StackTrace);
	}

	public void RemoveTargetCitizen(ushort instanceID, ref CitizenInstance data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = 0;
		ushort num2 = this.m_targetCitizens;
		int num3 = 0;
		while (num2 != 0)
		{
			if (num2 == instanceID)
			{
				if (num != 0)
				{
					instance.m_instances.m_buffer[(int)num].m_nextTargetInstance = data.m_nextTargetInstance;
				}
				else
				{
					this.m_targetCitizens = data.m_nextTargetInstance;
				}
				data.m_nextTargetInstance = 0;
				return;
			}
			num = num2;
			num2 = instance.m_instances.m_buffer[(int)num2].m_nextTargetInstance;
			if (++num3 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		CODebugBase<LogChannel>.Error(LogChannel.Core, "Citizen not found!\n" + Environment.StackTrace);
	}

	public uint GetEmptyCitizenUnit(CitizenUnit.Flags flag)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = this.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0 && instance.m_units.m_buffer[(int)((UIntPtr)num)].Empty())
			{
				return num;
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0u;
	}

	public uint GetNotFullCitizenUnit(CitizenUnit.Flags flag)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = this.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0 && !instance.m_units.m_buffer[(int)((UIntPtr)num)].Full())
			{
				return num;
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0u;
	}

	public uint FindCitizenUnit(CitizenUnit.Flags flag, uint citizen)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = this.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & flag) != 0 && instance.m_units.m_buffer[(int)((UIntPtr)num)].ContainsCitizen(citizen))
			{
				return num;
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0u;
	}

	public ushort FindSegment(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = this.m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if ((service == ItemClass.Service.None || info.m_class.m_service == service) && (subService == ItemClass.SubService.None || info.m_class.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_class.m_layer & itemLayers) != ItemClass.Layer.None))
					{
						return segment;
					}
				}
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public ushort FindNode(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = this.m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			if ((instance.m_nodes.m_buffer[(int)num].m_flags & NetNode.Flags.Untouchable) != NetNode.Flags.None)
			{
				NetInfo info = instance.m_nodes.m_buffer[(int)num].Info;
				if ((service == ItemClass.Service.None || info.m_class.m_service == service) && (subService == ItemClass.SubService.None || info.m_class.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_class.m_layer & itemLayers) != ItemClass.Layer.None))
				{
					return num;
				}
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public bool ContainsSegment(ushort segment)
	{
		if (segment == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = this.m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
				if (segment2 == segment)
				{
					return true;
				}
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return false;
	}

	public bool ContainsNode(ushort node)
	{
		if (node == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = this.m_netNode;
		int num2 = 0;
		while (num != 0)
		{
			if (num == node)
			{
				return true;
			}
			num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
			if (++num2 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return false;
	}

	public bool ContainsBuilding(ushort building)
	{
		if (building == 0)
		{
			return false;
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		ushort subBuilding = this.m_subBuilding;
		int num = 0;
		while (subBuilding != 0)
		{
			if (subBuilding == building)
			{
				return true;
			}
			subBuilding = instance.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
			if (++num > 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return false;
	}

	public ushort FindParentNode(ushort buildingID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = Mathf.Max((int)((this.m_position.x - 16f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((this.m_position.z - 16f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((this.m_position.x + 16f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((this.m_position.z + 16f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_nodeGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if (instance.m_nodes.m_buffer[(int)num5].m_building == buildingID)
					{
						return num5;
					}
					num5 = instance.m_nodes.m_buffer[(int)num5].m_nextGridNode;
					if (++num6 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return 0;
	}

	public static ushort FindParentBuilding(ushort buildingID)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		ushort result = 0;
		ushort parentBuilding = instance.m_buildings.m_buffer[(int)buildingID].m_parentBuilding;
		int num = 0;
		while (parentBuilding != 0)
		{
			result = parentBuilding;
			parentBuilding = instance.m_buildings.m_buffer[(int)parentBuilding].m_parentBuilding;
			if (++num >= 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return result;
	}

	public bool CheckZoning(ItemClass.Zone zone1, ItemClass.Zone zone2, bool allowCollapsed)
	{
		int width = this.Width;
		int length = this.Length;
		Vector3 vector;
		vector..ctor(Mathf.Cos(this.m_angle), 0f, Mathf.Sin(this.m_angle));
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		vector *= (float)width * 4f;
		vector2 *= (float)length * 4f;
		Quad3 quad = default(Quad3);
		quad.a = this.m_position - vector - vector2;
		quad.b = this.m_position + vector - vector2;
		quad.c = this.m_position + vector + vector2;
		quad.d = this.m_position - vector + vector2;
		Vector3 vector3 = quad.Min();
		Vector3 vector4 = quad.Max();
		int num = Mathf.Max((int)((vector3.x - 46f) / 64f + 75f), 0);
		int num2 = Mathf.Max((int)((vector3.z - 46f) / 64f + 75f), 0);
		int num3 = Mathf.Min((int)((vector4.x + 46f) / 64f + 75f), 149);
		int num4 = Mathf.Min((int)((vector4.z + 46f) / 64f + 75f), 149);
		bool flag = false;
		uint num5 = 0u;
		ZoneManager instance = Singleton<ZoneManager>.get_instance();
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_zoneGrid[i * 150 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					if (allowCollapsed || (instance.m_blocks.m_buffer[(int)num6].m_flags & 4u) == 0u)
					{
						Vector3 position = instance.m_blocks.m_buffer[(int)num6].m_position;
						float num8 = Mathf.Max(Mathf.Max(vector3.x - 46f - position.x, vector3.z - 46f - position.z), Mathf.Max(position.x - vector4.x - 46f, position.z - vector4.z - 46f));
						if (num8 < 0f)
						{
							this.CheckZoning(zone1, zone2, ref num5, ref flag, ref instance.m_blocks.m_buffer[(int)num6]);
						}
					}
					num6 = instance.m_blocks.m_buffer[(int)num6].m_nextGridBlock;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int k = 0; k < length; k++)
		{
			for (int l = 0; l < width; l++)
			{
				if ((num5 & 1u << (k << 3) + l) == 0u)
				{
					return false;
				}
			}
		}
		bool flag2;
		if (flag)
		{
			flag2 = (zone2 == ItemClass.Zone.CommercialHigh || zone2 == ItemClass.Zone.ResidentialHigh);
		}
		else
		{
			flag2 = (zone1 == ItemClass.Zone.CommercialHigh || zone1 == ItemClass.Zone.ResidentialHigh);
		}
		if (flag2)
		{
			this.m_flags |= Building.Flags.HighDensity;
		}
		else
		{
			this.m_flags &= ~Building.Flags.HighDensity;
		}
		return true;
	}

	private void CheckZoning(ItemClass.Zone zone1, ItemClass.Zone zone2, ref uint validCells, ref bool secondary, ref ZoneBlock block)
	{
		BuildingInfo.ZoningMode zoningMode = this.Info.m_zoningMode;
		int width = this.Width;
		int length = this.Length;
		Vector3 vector = new Vector3(Mathf.Cos(this.m_angle), 0f, Mathf.Sin(this.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		int rowCount = block.RowCount;
		Vector3 vector3 = new Vector3(Mathf.Cos(block.m_angle), 0f, Mathf.Sin(block.m_angle)) * 8f;
		Vector3 vector4;
		vector4..ctor(vector3.z, 0f, -vector3.x);
		Vector3 vector5 = block.m_position - this.m_position + vector * ((float)width * 0.5f - 0.5f) + vector2 * ((float)length * 0.5f - 0.5f);
		for (int i = 0; i < rowCount; i++)
		{
			Vector3 vector6 = ((float)i - 3.5f) * vector4;
			int num = 0;
			while ((long)num < 4L)
			{
				if ((block.m_valid & ~block.m_shared & 1uL << (i << 3 | num)) != 0uL)
				{
					ItemClass.Zone zone3 = block.GetZone(num, i);
					bool flag = zone3 == zone1;
					if (zone3 == zone2 && zone2 != ItemClass.Zone.None)
					{
						flag = true;
						secondary = true;
					}
					if (flag)
					{
						Vector3 vector7 = ((float)num - 3.5f) * vector3;
						Vector3 vector8 = vector5 + vector7 + vector6;
						float num2 = vector.x * vector8.x + vector.z * vector8.z;
						float num3 = vector2.x * vector8.x + vector2.z * vector8.z;
						int num4 = Mathf.RoundToInt(num2 / 64f);
						int num5 = Mathf.RoundToInt(num3 / 64f);
						bool flag2 = false;
						if (zoningMode == BuildingInfo.ZoningMode.Straight)
						{
							flag2 = (num5 == 0);
						}
						else if (zoningMode == BuildingInfo.ZoningMode.CornerLeft)
						{
							flag2 = ((num5 == 0 && num4 >= width - 2) || (num5 <= 1 && num4 == width - 1));
						}
						else if (zoningMode == BuildingInfo.ZoningMode.CornerRight)
						{
							flag2 = ((num5 == 0 && num4 <= 1) || (num5 <= 1 && num4 == 0));
						}
						if ((!flag2 || num == 0) && num4 >= 0 && num5 >= 0 && num4 < width && num5 < length)
						{
							validCells |= 1u << (num5 << 3) + num4;
						}
					}
				}
				num++;
			}
		}
	}

	public float SampleWalkingHeight(ushort buildingID, Vector3 worldPos, float terrainHeight)
	{
		BuildingInfo info = this.Info;
		BuildingInfoBase buildingInfoBase = info;
		int width = this.Width;
		int length = this.Length;
		float num = Building.CalculateLocalMeshOffset(info, length);
		Vector3 vector;
		vector..ctor(Mathf.Cos(this.m_angle), 0f, Mathf.Sin(this.m_angle));
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Vector3 vector3 = this.m_position + vector2 * num;
		float num2 = 1f;
		float num3 = 0f;
		float num4 = 1f;
		float num5 = 0f;
		if ((this.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			BuildingInfoBase collapsedInfo = info.m_collapsedInfo;
			if (collapsedInfo != null)
			{
				Randomizer randomizer;
				randomizer..ctor((int)buildingID);
				int num6 = randomizer.Int32(4u);
				if ((1 << num6 & info.m_collapsedRotations) == 0)
				{
					num6 = (num6 + 1 & 3);
				}
				Vector3 vector4 = info.m_generatedInfo.m_min;
				Vector3 vector5 = info.m_generatedInfo.m_max;
				float num7 = (float)width * 4f;
				float num8 = (float)length * 4f;
				vector4 = Vector3.Max(vector4 - new Vector3(4f, 0f, 4f + num), new Vector3(-num7, 0f, -num8));
				vector5 = Vector3.Min(vector5 + new Vector3(4f, 0f, 4f - num), new Vector3(num7, 0f, num8));
				Vector3 vector6 = (vector4 + vector5) * 0.5f;
				Vector3 vector7 = vector5 - vector4;
				num2 = (((num6 & 1) != 0) ? vector7.z : vector7.x) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.x);
				num4 = (((num6 & 1) != 0) ? vector7.x : vector7.z) / Mathf.Max(1f, collapsedInfo.m_generatedInfo.m_size.z);
				buildingInfoBase = collapsedInfo;
				vector3 += vector * vector6.x;
				vector3 -= vector2 * (vector6.z + num);
				if (num6 != 1)
				{
					if (num6 != 2)
					{
						if (num6 == 3)
						{
							num3 = num2;
							num2 = 0f;
							num5 = -num4;
							num4 = 0f;
						}
					}
					else
					{
						num2 = -num2;
						num4 = -num4;
					}
				}
				else
				{
					num3 = -num2;
					num2 = 0f;
					num5 = num4;
					num4 = 0f;
				}
			}
		}
		float num9 = worldPos.x - vector3.x;
		float num10 = worldPos.z - vector3.z;
		float num11 = num9 * vector.x + num10 * vector.z;
		float num12 = num9 * vector2.x + num10 * vector2.z;
		int num13 = Mathf.RoundToInt((float)buildingInfoBase.m_generatedInfo.m_walkWidth * 0.5f + num11 * num2 + num12 * num3);
		int num14 = Mathf.RoundToInt((float)buildingInfoBase.m_generatedInfo.m_walkLength * 0.5f - num12 * num4 - num11 * num5);
		if (num13 >= 0 && num14 >= 0 && num13 < buildingInfoBase.m_generatedInfo.m_walkWidth && num14 < buildingInfoBase.m_generatedInfo.m_walkLength)
		{
			float num15 = buildingInfoBase.m_generatedInfo.m_walkLevel[num14 * buildingInfoBase.m_generatedInfo.m_walkWidth + num13];
			if (buildingInfoBase.m_requireHeightMap)
			{
				num15 += terrainHeight;
			}
			else
			{
				num15 += vector3.y;
			}
			if (Mathf.Abs(num15 - worldPos.y) < 2f)
			{
				return num15;
			}
		}
		if (Mathf.Abs(terrainHeight - worldPos.y) < 2f)
		{
			return terrainHeight;
		}
		return worldPos.y;
	}

	public bool CalculateGroupData(ushort buildingID, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		if ((this.m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Hidden)) != Building.Flags.Created)
		{
			return false;
		}
		bool result = false;
		BuildingInfo info = this.Info;
		if (info.m_buildingAI.CalculateGroupData(buildingID, ref this, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
		{
			result = true;
		}
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer)
		{
			ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
			if (properties != null && properties.m_mode != ItemClass.Availability.AssetEditor && Notification.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
			{
				result = true;
			}
		}
		return result;
	}

	public void PopulateGroupData(ushort buildingID, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if ((this.m_flags & (Building.Flags.Created | Building.Flags.Deleted | Building.Flags.Hidden)) != Building.Flags.Created)
		{
			return;
		}
		BuildingInfo info = this.Info;
		float y = info.m_size.y;
		info.m_buildingAI.PopulateGroupData(buildingID, ref this, ref y, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer)
		{
			ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
			if (properties != null && properties.m_mode != ItemClass.Availability.AssetEditor)
			{
				Vector3 position;
				Quaternion quaternion;
				Vector3 vector;
				this.GetTotalPosition(out position, out quaternion, out vector);
				if (y != info.m_size.y)
				{
					vector.y = y;
				}
				position.y += vector.y;
				Notification.PopulateGroupData(this.m_problems, position, 1f, groupX, groupZ, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
			}
		}
	}

	public void GetTotalPosition(out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		BuildingInfo info = this.Info;
		this.CalculateMeshPosition(out position, out rotation);
		size = info.m_size;
		ushort subBuilding = this.m_subBuilding;
		if (subBuilding != 0 && info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Vector3 vector = position - size * 0.5f;
			Vector3 vector2 = position + size * 0.5f;
			int num = 0;
			while (subBuilding != 0)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)subBuilding].Info;
				Vector3 vector3;
				Quaternion quaternion;
				instance.m_buildings.m_buffer[(int)subBuilding].CalculateMeshPosition(out vector3, out quaternion);
				Vector3 size2 = info2.m_size;
				vector = Vector3.Min(vector, vector3 - size2 * 0.5f);
				vector2 = Vector3.Max(vector2, vector3 + size2 * 0.5f);
				subBuilding = instance.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
				if (++num > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			position = (vector + vector2) * 0.5f;
			size = vector2 - vector;
		}
	}

	public Notification.Problem m_problems;

	public Building.Frame m_frame0;

	public Building.Frame m_frame1;

	public Building.Frame m_frame2;

	public Building.Frame m_frame3;

	public Building.Flags m_flags;

	public uint m_buildIndex;

	public uint m_citizenUnits;

	public Vector3 m_position;

	public float m_angle;

	public ushort m_netNode;

	public ushort m_subBuilding;

	public ushort m_parentBuilding;

	public ushort m_waterSource;

	public ushort m_eventIndex;

	public ushort m_nextGridBuilding;

	public ushort m_nextGridBuilding2;

	public ushort m_ownVehicles;

	public ushort m_guestVehicles;

	public ushort m_sourceCitizens;

	public ushort m_targetCitizens;

	public ushort m_electricityBuffer;

	public ushort m_waterBuffer;

	public ushort m_sewageBuffer;

	public ushort m_heatingBuffer;

	public ushort m_garbageBuffer;

	public ushort m_crimeBuffer;

	public ushort m_customBuffer1;

	public ushort m_customBuffer2;

	public ushort m_infoIndex;

	public ushort m_buildWaterHeight;

	public byte m_baseHeight;

	public byte m_productionRate;

	public byte m_waterPollution;

	public byte m_fireIntensity;

	public byte m_happiness;

	public byte m_health;

	public byte m_citizenCount;

	public byte m_lastFrame;

	public byte m_tempImport;

	public byte m_tempExport;

	public byte m_finalImport;

	public byte m_finalExport;

	public byte m_electricityProblemTimer;

	public byte m_heatingProblemTimer;

	public byte m_waterProblemTimer;

	public byte m_workerProblemTimer;

	public byte m_incomingProblemTimer;

	public byte m_outgoingProblemTimer;

	public byte m_healthProblemTimer;

	public byte m_deathProblemTimer;

	public byte m_serviceProblemTimer;

	public byte m_taxProblemTimer;

	public byte m_majorProblemTimer;

	public byte m_levelUpProgress;

	public byte m_width;

	public byte m_length;

	public byte m_education1;

	public byte m_education2;

	public byte m_education3;

	public byte m_teens;

	public byte m_youngs;

	public byte m_adults;

	public byte m_seniors;

	public byte m_fireHazard;

	public byte m_subCulture;

	private static ushort[] TempMask = new ushort[16];

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Original = 4,
		CustomName = 8,
		Untouchable = 16,
		FixedHeight = 32,
		Incoming = 64,
		Outgoing = 128,
		IncomingOutgoing = 192,
		CapacityStep1 = 256,
		CapacityStep2 = 512,
		CapacityFull = 768,
		RateReduced = 1024,
		HighDensity = 2048,
		LevelUpEducation = 4096,
		LevelUpLandValue = 8192,
		RoadAccessFailed = 16384,
		Evacuating = 32768,
		Completed = 65536,
		Active = 131072,
		Abandoned = 262144,
		Demolishing = 524288,
		ZonesUpdated = 1048576,
		Downgrading = 2097152,
		Collapsed = 4194304,
		BurnedDown = 4194304,
		Upgrading = 8388608,
		Loading1 = 16777216,
		Loading2 = 33554432,
		SecondaryLoading = 67108864,
		Hidden = 134217728,
		EventActive = 268435456,
		Flooded = 536870912,
		All = -1
	}

	public struct Frame
	{
		public byte m_fireDamage;

		public byte m_constructState;

		public byte m_productionState;
	}
}
