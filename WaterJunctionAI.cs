﻿using System;
using UnityEngine;

public class WaterJunctionAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Water || infoMode == InfoManager.InfoMode.Heating)
		{
			Color result;
			result..ctor(0f, 0f, 0f, 0f);
			if (data.m_waterBuffer != 0)
			{
				result.r = 1f;
			}
			if (data.m_sewageBuffer != 0)
			{
				result.g = 1f;
			}
			if (data.m_heatingBuffer != 0)
			{
				result.b = 1f;
			}
			return result;
		}
		if (infoMode != InfoManager.InfoMode.Pollution)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if (data.m_waterBuffer != 0)
		{
			float num = Mathf.Clamp01((float)data.m_waterPollution * 0.0117647061f);
			return new Color(num, num, num, 1f);
		}
		return new Color(0f, 0f, 0f, 0f);
	}

	public override Color32 GetGroupVertexColor(ushort buildingID, ref Building buildingData, BuildingInfoBase info, int vertexIndex, bool baseVertex)
	{
		if (baseVertex)
		{
			return base.GetGroupVertexColor(buildingID, ref buildingData, info, vertexIndex, baseVertex);
		}
		Color32 result;
		if (info.m_lodMeshData.m_colors != null && info.m_lodMeshData.m_colors.Length > vertexIndex)
		{
			result = info.m_lodMeshData.m_colors[vertexIndex];
			result.a = 0;
		}
		else
		{
			result..ctor(255, 255, 255, 0);
		}
		return result;
	}

	public override ItemClass.CollisionType GetCollisionType()
	{
		return ItemClass.CollisionType.Underground;
	}
}
