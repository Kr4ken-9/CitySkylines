﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class LoadSaveStatus : UICustomControl
{
	public static AsyncTaskBase activeTask
	{
		get
		{
			return (!(LoadSaveStatus.sInstance != null)) ? null : LoadSaveStatus.sInstance.internalActiveTask;
		}
		set
		{
			LoadSaveStatus.sActiveTask = value;
			if (LoadSaveStatus.sInstance != null)
			{
				LoadSaveStatus.sInstance.internalActiveTask = value;
			}
		}
	}

	private AsyncTaskBase internalActiveTask
	{
		get
		{
			return this.m_ActiveTask;
		}
		set
		{
			this.m_ActiveTask = value;
			if (this.m_ActiveTask != null)
			{
				if (this.m_ActiveTask.name != null)
				{
					this.m_Text.Show();
					if (this.m_ActiveTask is AsyncCounterWrapper)
					{
						this.m_Text.set_text(this.m_ActiveTask.name);
					}
					else
					{
						this.m_Text.set_text(Locale.Get("LOADSTATUS", this.m_ActiveTask.name));
					}
					float x = this.m_Text.get_relativePosition().x;
					float num = this.m_Text.get_parent().get_size().x - this.m_Text.get_size().x;
					ValueAnimator.Animate("LoadingSaveIcon", delegate(float val)
					{
						Vector3 relativePosition = this.m_Text.get_relativePosition();
						relativePosition.x = val;
						this.m_Text.set_relativePosition(relativePosition);
					}, new AnimatedFloat(x, num, 0.5f, 7));
				}
			}
			else
			{
				float x2 = this.m_Text.get_relativePosition().x;
				float x3 = this.m_Text.get_parent().get_size().x;
				ValueAnimator.Animate("LoadingSaveIcon", delegate(float val)
				{
					Vector3 relativePosition = this.m_Text.get_relativePosition();
					relativePosition.x = val;
					this.m_Text.set_relativePosition(relativePosition);
				}, new AnimatedFloat(x2, x3, 0.5f, 6), delegate
				{
					this.m_Text.Hide();
				});
			}
		}
	}

	private void Awake()
	{
		LoadSaveStatus.sInstance = this;
		this.m_Text.Hide();
	}

	private void OnEnable()
	{
		this.internalActiveTask = LoadSaveStatus.sActiveTask;
	}

	private void OnDisable()
	{
		this.m_Text.Hide();
	}

	private void Update()
	{
		if (this.m_ActiveTask != null)
		{
			if (this.m_ActiveTask.isExecuting && this.m_Text.get_isVisible())
			{
				this.m_Sprite.Rotate(Vector3.get_back(), this.m_RotationSpeed * Time.get_deltaTime());
				string text;
				if (this.m_ActiveTask is AsyncCounterWrapper)
				{
					text = this.m_ActiveTask.name;
				}
				else
				{
					text = Locale.Get("LOADSTATUS", this.m_ActiveTask.name);
				}
				if (!text.Equals(this.m_Text.get_text()))
				{
					this.m_Text.set_text(text);
				}
			}
			if (this.m_ActiveTask.completedOrFailed)
			{
				this.internalActiveTask = null;
			}
		}
	}

	private static AsyncTaskBase sActiveTask;

	private static LoadSaveStatus sInstance;

	public UILabel m_Text;

	public Transform m_Sprite;

	public float m_RotationSpeed = 280f;

	private AsyncTaskBase m_ActiveTask;
}
