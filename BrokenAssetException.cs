﻿using System;

public class BrokenAssetException : Exception
{
	public BrokenAssetException(string message) : base(message)
	{
	}
}
