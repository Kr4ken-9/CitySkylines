﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class BackgroundPanel : UICustomControl
{
	private void Awake()
	{
		this.m_Images.AddRange(this.m_NormalImages);
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
		{
			this.m_Images.AddRange(this.m_AfterDarkImages);
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
		{
			this.m_Images.AddRange(this.m_WinterWonderlandImages);
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			this.m_Images.AddRange(this.m_NaturalDisastersImages);
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
		{
			this.m_Images.AddRange(this.m_MassTransit);
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			this.m_Images.AddRange(this.m_GreenCities);
		}
		this.m_CurrentImage = Random.Range(0, this.m_Images.Count);
		this.m_Background0 = (base.get_component() as UITextureSprite);
		this.m_Background1 = base.Find<UITextureSprite>("BackgroundSprite2");
		this.m_Background1.set_opacity(0f);
		this.m_Background0.set_texture(this.m_Images[this.m_CurrentImage]);
		this.m_ImageTimeLeft = this.m_ImageIntervals;
	}

	private void Update()
	{
		this.m_ImageTimeLeft -= Time.get_deltaTime();
		if (this.m_ImageTimeLeft < 0f)
		{
			this.m_ImageTimeLeft = this.m_ImageIntervals;
			this.m_CurrentImage = (this.m_CurrentImage + 1) % this.m_Images.Count;
			this.SetImage(this.m_CurrentImage);
		}
	}

	public void SetImage(int id)
	{
		if (!ValueAnimator.IsAnimating("BackgroundImage"))
		{
			if (this.m_Background1.get_opacity() == 0f)
			{
				this.m_Background1.set_texture(this.m_Images[this.m_CurrentImage]);
				ValueAnimator.Animate("BackgroundImage", delegate(float val)
				{
					this.m_Background1.set_opacity(val);
				}, new AnimatedFloat(0f, 1f, 1.33f));
			}
			else if (this.m_Background1.get_opacity() == 1f)
			{
				this.m_Background0.set_texture(this.m_Images[this.m_CurrentImage]);
				ValueAnimator.Animate("BackgroundImage", delegate(float val)
				{
					this.m_Background1.set_opacity(val);
				}, new AnimatedFloat(1f, 0f, 1.33f));
			}
		}
	}

	public Texture2D[] m_NormalImages;

	public Texture2D[] m_AfterDarkImages;

	public Texture2D[] m_WinterWonderlandImages;

	public Texture2D[] m_NaturalDisastersImages;

	public Texture2D[] m_MassTransit;

	public Texture2D[] m_GreenCities;

	public float m_ImageIntervals = 30f;

	private List<Texture2D> m_Images = new List<Texture2D>();

	private int m_CurrentImage;

	private float m_ImageTimeLeft;

	private UITextureSprite m_Background0;

	private UITextureSprite m_Background1;
}
