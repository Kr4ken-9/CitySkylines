﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class VortexAI : VehicleAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (VortexAI.m_generatedMesh == null)
		{
			VortexAI.m_generatedMesh = this.GenerateMesh();
		}
		this.m_info.m_mesh = VortexAI.m_generatedMesh;
	}

	public override void ReleaseAI()
	{
		if (VortexAI.m_generatedMesh != null)
		{
			Object.Destroy(VortexAI.m_generatedMesh);
			VortexAI.m_generatedMesh = null;
		}
		base.ReleaseAI();
	}

	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation, scale);
		return result;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		return Matrix4x4.get_identity();
	}

	public override void RenderExtraStuff(RenderManager.CameraInfo cameraInfo, InstanceID id, Vector3 position, Quaternion rotation, Vector4 tyrePosition)
	{
		if (this.m_debrisCount != 0 && this.m_info.m_subMeshes != null && tyrePosition.x > 0.001f)
		{
			int num = this.m_info.m_subMeshes.Length;
			if (num != 0)
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				DisasterManager instance2 = Singleton<DisasterManager>.get_instance();
				SimulationManager instance3 = Singleton<SimulationManager>.get_instance();
				int num2 = this.m_debrisCount / num;
				Vector3 vector;
				vector..ctor(tyrePosition.x, tyrePosition.x, tyrePosition.x);
				InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
				float num3 = 1f;
				if (group != null)
				{
					ushort disaster = group.m_ownerInstance.Disaster;
					if (disaster != 0)
					{
						num3 = 0.2f + (float)instance2.m_disasters.m_buffer[(int)disaster].m_intensity * 0.0145454546f;
					}
				}
				float num4 = Mathf.Max(this.m_destructionRadiusMax, this.m_upgradeRadiusMax) * num3;
				float num5 = instance3.m_simulationTimer * 0.104719758f + (float)id.Vehicle * 0.0003834952f;
				Randomizer randomizer;
				randomizer..ctor((int)id.Vehicle);
				MaterialPropertyBlock materialBlock = instance.m_materialBlock;
				materialBlock.Clear();
				materialBlock.SetMatrix(instance.ID_TyreMatrix, Matrix4x4.get_identity());
				materialBlock.SetVector(instance.ID_TyrePosition, tyrePosition);
				materialBlock.SetVector(instance.ID_LightState, Vector4.get_zero());
				materialBlock.SetColor(instance.ID_Color, Color.get_white());
				for (int i = 0; i < num; i++)
				{
					VehicleInfoBase subInfo = this.m_info.m_subMeshes[i].m_subInfo;
					if (subInfo != null)
					{
						for (int j = 0; j < num2; j++)
						{
							float num6 = (float)randomizer.Int32(360u);
							float num7 = (float)randomizer.Int32(1000u);
							num7 *= num7 * 0.001f;
							float num8 = (float)randomizer.Int32(2500, 5000) * 0.01f;
							num8 *= 1f + num7 * 0.001f;
							float num9 = (float)randomizer.Int32(300, 1000) * 0.001f;
							num9 *= num9 * num3;
							float num10 = instance3.m_simulationTimer * 6f;
							num6 += num10 * Mathf.Round(600f / Mathf.Max(10f, num8 - 40f));
							num8 *= num4 / 75f;
							Quaternion quaternion = Quaternion.AngleAxis(num6, Vector3.get_down());
							Vector3 vector2 = position + quaternion * new Vector3(num8, num7, 0f);
							num7 = MathUtils.SmoothStep(0f, 2000f, num7);
							float num11 = Mathf.Sin(num7 * 5f - num5 * 7f) * (3f * num7 - 2.25f * num7 * num7) * 40000f / Mathf.Max(200f, num8);
							float num12 = Mathf.Cos(num7 * 3f + num5 * 3f);
							float num13 = Mathf.Sin(num7 * 3f + num5 * 3f);
							vector2.x += num12 * num11;
							vector2.z += num13 * num11;
							if (cameraInfo.Intersect(vector2, subInfo.m_generatedInfo.m_size.z * 0.5f + 15f))
							{
								Matrix4x4 matrix4x = default(Matrix4x4);
								matrix4x.SetTRS(vector2, quaternion, vector * num9);
								if (cameraInfo.CheckRenderDistance(vector2, subInfo.m_lodRenderDistance))
								{
									VehicleManager expr_384_cp_0 = instance;
									expr_384_cp_0.m_drawCallData.m_defaultCalls = expr_384_cp_0.m_drawCallData.m_defaultCalls + 1;
									Graphics.DrawMesh(subInfo.m_mesh, matrix4x, subInfo.m_material, subInfo.m_prefabDataLayer, null, 0, materialBlock);
								}
								else
								{
									subInfo.m_lodTransforms[subInfo.m_lodCount] = matrix4x;
									subInfo.m_lodLightStates[subInfo.m_lodCount] = Vector4.get_zero();
									subInfo.m_lodColors[subInfo.m_lodCount] = Color.get_white();
									subInfo.m_lodMin = Vector3.Min(subInfo.m_lodMin, vector2);
									subInfo.m_lodMax = Vector3.Max(subInfo.m_lodMax, vector2);
									if (++subInfo.m_lodCount == subInfo.m_lodTransforms.Length)
									{
										Vehicle.RenderLod(cameraInfo, subInfo);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private Mesh GenerateMesh()
	{
		int num = 16250;
		float num2 = 2000f;
		float num3 = 50f;
		float num4 = 200f;
		Vector3[] array = new Vector3[num * 4];
		Vector2[] array2 = new Vector2[num * 4];
		int[] array3 = new int[num * 6];
		int num5 = 0;
		int num6 = 0;
		Randomizer randomizer;
		randomizer..ctor(2975689);
		for (int i = 0; i < num; i++)
		{
			float num7 = (float)i / (float)num;
			num7 = (num7 + num7 * num7) * 0.5f;
			float num8 = (float)randomizer.Int32(10000u) * 0.0001f;
			float num9 = (float)randomizer.Int32(10000u) * 0.0001f;
			float num10 = num3 + num7 * num7 * (num4 - num3);
			float num11 = num7 * num2 / 300f;
			float num12 = num7 * num2 / 1500f;
			num11 = ((num11 >= 1f) ? 0f : ((2f * num11 * num11 - num11 * num11 * num11 * num11) * 200f));
			num12 = ((num12 <= 0.1f) ? ((1f - 10f * num12) * (1f - 10f * num12) * 100f) : ((2f * num12 * num12 - num12 * num12 * num12 * num12) * 750f));
			if (num11 > num12)
			{
				if (i % 3 == 0)
				{
					num9 = num10 + num11 * Mathf.Sqrt(Mathf.Sqrt(num9));
					num8 *= 6.28318548f;
				}
				else if (i % 3 == 2)
				{
					num9 = num10 + num12 * Mathf.Sqrt(num9);
					float num13 = Mathf.Sin(num8 * 25.1327419f);
					num8 = (num8 + 0.04f * num13 * num13) * 6.28318548f;
				}
				else
				{
					num9 = Mathf.Sqrt(num9) * num10;
					num8 *= 6.28318548f;
				}
			}
			else if (i % 3 == 0)
			{
				num9 = num10 + num12 * Mathf.Sqrt(num9);
				float num14 = Mathf.Sin(num8 * 25.1327419f);
				num8 = (num8 + 0.04f * num14 * num14) * 6.28318548f;
			}
			else
			{
				num9 = Mathf.Sqrt(num9) * num10;
				num8 *= 6.28318548f;
			}
			Vector3 vector;
			vector.x = Mathf.Cos(num8) * num9;
			vector.y = num7 * num2;
			vector.z = Mathf.Sin(num8) * num9;
			array[num5] = new Vector3(vector.x - 1f, vector.y, vector.z - 1f);
			array2[num5] = new Vector2(-1f, -1f);
			num5++;
			array[num5] = new Vector3(vector.x - 1f, vector.y, vector.z + 1f);
			array2[num5] = new Vector2(-1f, 1f);
			num5++;
			array[num5] = new Vector3(vector.x + 1f, vector.y, vector.z + 1f);
			array2[num5] = new Vector2(1f, 1f);
			num5++;
			array[num5] = new Vector3(vector.x + 1f, vector.y, vector.z - 1f);
			array2[num5] = new Vector2(1f, -1f);
			num5++;
			array3[num6++] = num5 - 4;
			array3[num6++] = num5 - 3;
			array3[num6++] = num5 - 1;
			array3[num6++] = num5 - 1;
			array3[num6++] = num5 - 3;
			array3[num6++] = num5 - 2;
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_uv(array2);
		mesh.set_triangles(array3);
		return mesh;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingSpace;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, 0);
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		InstanceID id = default(InstanceID);
		id.Vehicle = vehicleID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		DisasterManager instance = Singleton<DisasterManager>.get_instance();
		float num = 1f;
		float y = 1f;
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				num = 0.2f + (float)instance.m_disasters.m_buffer[(int)disaster].m_intensity * 0.0145454546f;
				y = (float)instance.m_disasters.m_buffer[(int)disaster].m_intensity * 0.0181818176f;
			}
		}
		frameData.m_position += frameData.m_velocity * 0.5f;
		float maxSpeed = this.m_info.m_maxSpeed;
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float num2 = VectorUtils.LengthXZ(vector);
		if (num2 < maxSpeed)
		{
			vehicleData.m_targetPos0 = vehicleData.m_targetPos1;
			vector = vehicleData.m_targetPos0 - frameData.m_position;
			num2 = VectorUtils.LengthXZ(vector);
		}
		if (num2 > 1f)
		{
			frameData.m_angleVelocity = Mathf.Min(1f, frameData.m_angleVelocity + 0.05f);
			if (frameData.m_angleVelocity > 0.95f)
			{
				vehicleData.m_flags |= Vehicle.Flags.Flying;
			}
		}
		else
		{
			frameData.m_angleVelocity = Mathf.Max(0f, frameData.m_angleVelocity - 0.05f);
			if (frameData.m_angleVelocity < 0.95f)
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Flying;
			}
			if (frameData.m_angleVelocity < 0.05f && this.ArriveAtDestination(leaderID, ref leaderData))
			{
				if (group != null)
				{
					ushort disaster2 = group.m_ownerInstance.Disaster;
					if (disaster2 != 0)
					{
						DisasterInfo info = instance.m_disasters.m_buffer[(int)disaster2].Info;
						info.m_disasterAI.DeactivateNow(disaster2, ref instance.m_disasters.m_buffer[(int)disaster2]);
					}
				}
				leaderData.Unspawn(leaderID);
				return;
			}
		}
		Randomizer randomizer;
		randomizer..ctor((uint)vehicleID ^ Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 9);
		vector.y = 0f;
		vector = Vector3.ClampMagnitude(vector, maxSpeed * 0.5f);
		vector.x += (float)randomizer.Int32(-1000, 1000) * Mathf.Min(maxSpeed * 0.5f, num2 * 0.1f) * 0.001f;
		vector.z += (float)randomizer.Int32(-1000, 1000) * Mathf.Min(maxSpeed * 0.5f, num2 * 0.1f) * 0.001f;
		Vector3 vector2 = Vector3.ClampMagnitude(vector, maxSpeed);
		Vector3 vector3 = frameData.m_velocity * 0.9f + vector2 * 0.1f;
		Vector3 worldPos = frameData.m_position + vector3;
		vector3.y = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, false, 0f) - frameData.m_position.y;
		frameData.m_velocity = vector3;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_rotation = Quaternion.get_identity();
		frameData.m_swayVelocity = Vector3.get_zero();
		frameData.m_swayPosition = Vector3.get_zero();
		frameData.m_steerAngle = frameData.m_angleVelocity;
		frameData.m_travelDistance = num2;
		frameData.m_lightIntensity.x = (float)vehicleID / 16384f;
		frameData.m_lightIntensity.y = y;
		frameData.m_lightIntensity.z = 0f;
		frameData.m_lightIntensity.w = 0f;
		if ((vehicleData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			if (this.m_destructionRadiusMax >= 1f)
			{
				float destructionRadiusMin = this.m_destructionRadiusMin * num;
				float num3 = this.m_destructionRadiusMax * num;
				Vector3 position;
				position..ctor(frameData.m_position.x, frameData.m_position.y + num3 * 0.75f, frameData.m_position.z);
				Vector3 directionalWind;
				directionalWind..ctor(frameData.m_velocity.x, 80f, frameData.m_velocity.z);
				DisasterHelpers.AddWind(position, num3 * 1.5f, directionalWind, 0.5f, -40f, group);
				DisasterHelpers.DestroyStuff((int)vehicleID, group, frameData.m_position, num3, num3, 0f, destructionRadiusMin, num3, 0f, 0f);
				DisasterHelpers.BurnGround(VectorUtils.XZ(frameData.m_position), num3, 0.7f);
			}
			if (this.m_upgradeRadiusMax >= 1f)
			{
				float upgradeRadiusMin = this.m_upgradeRadiusMin * num;
				float num4 = this.m_upgradeRadiusMax * num;
				DisasterHelpers.UpgradeBuildings((int)vehicleID, group, frameData.m_position, num4, upgradeRadiusMin, num4, 1f);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 10f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		return (vehicleData.m_waitCounter += 1) > 4;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
		vehicleData.m_targetPos0 = endPos;
		vehicleData.m_targetPos0.w = 2f;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
		this.TrySpawn(vehicleID, ref vehicleData);
		return true;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		vehicleData.m_waitCounter = 0;
		return true;
	}

	public float m_destructionRadiusMin = 30f;

	public float m_destructionRadiusMax = 60f;

	public float m_upgradeRadiusMin;

	public float m_upgradeRadiusMax;

	public int m_debrisCount = 100;

	[NonSerialized]
	private static Mesh m_generatedMesh;
}
