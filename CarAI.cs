﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class CarAI : VehicleAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.NoisePollution)
		{
			int noiseLevel = this.GetNoiseLevel();
			return CommonBuildingAI.GetNoisePollutionColor((float)noiseLevel * 2.5f);
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags2 & Vehicle.Flags2.Floating) != (Vehicle.Flags2)0)
		{
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(empty);
			DisasterHelpers.RemovePeople(group, 0, ref data.m_citizenUnits, 100, ref Singleton<SimulationManager>.get_instance().m_randomizer);
		}
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				data.m_flags &= ~Vehicle.Flags.Arriving;
				this.PathfindSuccess(vehicleID, ref data);
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(vehicleID, ref data);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		Vector3 lastFramePosition = data.GetLastFramePosition();
		int lodPhysics;
		if (Vector3.SqrMagnitude(physicsLodRefPos - lastFramePosition) >= 1210000f)
		{
			lodPhysics = 2;
		}
		else if (Vector3.SqrMagnitude(Singleton<SimulationManager>.get_instance().m_simulationView.m_position - lastFramePosition) >= 250000f)
		{
			lodPhysics = 1;
		}
		else
		{
			lodPhysics = 0;
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, lodPhysics);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, lodPhysics);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(this.m_info.m_class.m_service);
		int num3 = (privateServiceIndex == -1) ? 150 : 100;
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 && data.m_cargoParent == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
		else if ((int)data.m_blockCounter == num3)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		data.Unspawn(vehicleID);
	}

	public override bool AddWind(ushort vehicleID, ref Vehicle data, Vector3 wind, InstanceManager.Group group)
	{
		if ((data.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0 && data.m_leadingVehicle == 0 && ((data.m_flags2 & Vehicle.Flags2.Blown) != (Vehicle.Flags2)0 || wind.y > 19.62f))
		{
			Vehicle.Frame lastFrameData = data.GetLastFrameData();
			lastFrameData.m_velocity = lastFrameData.m_velocity * 0.875f + wind * 0.125f;
			wind.y = 0f;
			if (wind.get_sqrMagnitude() > 0.01f)
			{
				lastFrameData.m_rotation = Quaternion.Lerp(lastFrameData.m_rotation, Quaternion.LookRotation(wind), 0.1f);
			}
			data.SetLastFrameData(lastFrameData);
			if ((data.m_flags2 & Vehicle.Flags2.Blown) == (Vehicle.Flags2)0)
			{
				this.SetTarget(vehicleID, ref data, 0);
				data.m_flags2 |= Vehicle.Flags2.Blown;
				data.m_waitCounter = 0;
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			Singleton<InstanceManager>.get_instance().SetGroup(empty, group);
			return true;
		}
		return false;
	}

	public override bool AddWind(ushort parkedID, ref VehicleParked data, Vector3 wind, InstanceManager.Group group)
	{
		if (wind.y > 19.62f)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			ushort num;
			if (instance.CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, data.m_position, TransferManager.TransferReason.None, false, false))
			{
				Vehicle.Frame frame = instance.m_vehicles.m_buffer[(int)num].m_frame0;
				frame.m_rotation = data.m_rotation;
				frame.m_velocity = frame.m_velocity * 0.875f + wind * 0.125f;
				wind.y = 0f;
				if (wind.get_sqrMagnitude() > 0.01f)
				{
					frame.m_rotation = Quaternion.Lerp(frame.m_rotation, Quaternion.LookRotation(wind), 0.1f);
				}
				instance.m_vehicles.m_buffer[(int)num].m_frame0 = frame;
				instance.m_vehicles.m_buffer[(int)num].m_frame1 = frame;
				instance.m_vehicles.m_buffer[(int)num].m_frame2 = frame;
				instance.m_vehicles.m_buffer[(int)num].m_frame3 = frame;
				this.m_info.m_vehicleAI.FrameDataUpdated(num, ref instance.m_vehicles.m_buffer[(int)num], ref frame);
				Vehicle[] expr_15F_cp_0 = instance.m_vehicles.m_buffer;
				ushort expr_15F_cp_1 = num;
				expr_15F_cp_0[(int)expr_15F_cp_1].m_flags2 = (expr_15F_cp_0[(int)expr_15F_cp_1].m_flags2 | Vehicle.Flags2.Blown);
				uint ownerCitizen = data.m_ownerCitizen;
				instance.m_vehicles.m_buffer[(int)num].m_transferSize = (ushort)(ownerCitizen & 65535u);
				this.m_info.m_vehicleAI.TrySpawn(num, ref instance.m_vehicles.m_buffer[(int)num]);
				InstanceID empty = InstanceID.Empty;
				empty.ParkedVehicle = parkedID;
				InstanceID empty2 = InstanceID.Empty;
				empty2.Vehicle = num;
				Singleton<InstanceManager>.get_instance().ChangeInstance(empty, empty2);
				Singleton<InstanceManager>.get_instance().SetGroup(empty2, group);
				if (ownerCitizen != 0u)
				{
					instance2.m_citizens.m_buffer[(int)((UIntPtr)ownerCitizen)].SetParkedVehicle(ownerCitizen, 0);
					instance2.m_citizens.m_buffer[(int)((UIntPtr)ownerCitizen)].SetVehicle(ownerCitizen, num, 0u);
				}
				else
				{
					instance.ReleaseParkedVehicle(parkedID);
				}
				return true;
			}
		}
		return false;
	}

	private void SimulationStepBlown(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_velocity.y = frameData.m_velocity.y - 2.4525f;
		frameData.m_velocity *= 0.99f;
		frameData.m_position += frameData.m_velocity * 0.5f;
		float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
		if (num > frameData.m_position.y)
		{
			frameData.m_velocity = Vector3.get_zero();
			frameData.m_position.y = num;
			vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		}
		else
		{
			vehicleData.m_blockCounter = 0;
			frameData.m_travelDistance += 0.1f;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			float num2 = (float)randomizer.Int32(100, 1000) * 6.283185E-05f * currentFrameIndex;
			Vector3 vector;
			vector.x = Mathf.Sin((float)randomizer.Int32(1000u) * 0.00628318544f + num2);
			vector.y = Mathf.Sin((float)randomizer.Int32(1000u) * 0.00628318544f + num2);
			vector.z = Mathf.Sin((float)randomizer.Int32(1000u) * 0.00628318544f + num2);
			if (vector.get_sqrMagnitude() > 0.001f)
			{
				Quaternion quaternion = Quaternion.AngleAxis((float)randomizer.Int32(360u), vector);
				frameData.m_rotation = Quaternion.Lerp(frameData.m_rotation, quaternion, 0.2f);
			}
		}
		bool flag = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		leaderData.m_flags &= ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding);
		frameData.m_swayVelocity = Vector3.get_zero();
		frameData.m_swayPosition = Vector3.get_zero();
		frameData.m_steerAngle = 0f;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = ((!flag) ? 0f : 5f);
		frameData.m_lightIntensity.w = ((!flag) ? 0f : 5f);
		frameData.m_underground = false;
		frameData.m_transition = false;
	}

	private void SimulationStepFloating(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		Vector3 vector = frameData.m_rotation * Vector3.get_forward();
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num = -Mathf.Atan2(vector.x, vector.z);
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		float num2;
		float num3;
		Vector3 vector2;
		Vector3 vector3;
		Singleton<TerrainManager>.get_instance().SampleWaterData(VectorUtils.XZ(frameData.m_position), out num2, out num3, out vector2, out vector3);
		if (num3 - num2 > 1f)
		{
			frameData.m_angleVelocity = frameData.m_angleVelocity * 0.9f + (float)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(-1000, 1000) * 0.0001f;
			vector2 *= 0.266666681f;
			frameData.m_velocity = Vector3.MoveTowards(frameData.m_velocity, vector2, 1f);
			float num4 = Mathf.Clamp((float)vehicleData.m_blockCounter * 0.05f, 1f, num3 - num2);
			frameData.m_velocity.y = frameData.m_velocity.y * 0.5f + (num3 - num4 - frameData.m_position.y) * 0.5f;
		}
		else
		{
			num2 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
			frameData.m_angleVelocity = 0f;
			frameData.m_velocity = Vector3.MoveTowards(frameData.m_velocity, Vector3.get_zero(), 1f);
			frameData.m_velocity.y = frameData.m_velocity.y * 0.5f + (num2 - frameData.m_position.y) * 0.5f;
		}
		vector3.y = 0f;
		bool flag = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		leaderData.m_flags &= ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding);
		frameData.m_position += frameData.m_velocity * 0.5f;
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		frameData.m_swayVelocity += vector3 * 0.2f - frameData.m_swayVelocity * 0.5f - frameData.m_swayPosition * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = 0f;
		frameData.m_travelDistance = frameData.m_travelDistance;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = ((!flag) ? 0f : 5f);
		frameData.m_lightIntensity.w = ((!flag) ? 0f : 5f);
		frameData.m_underground = false;
		frameData.m_transition = false;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if ((leaderData.m_flags2 & Vehicle.Flags2.Blown) != (Vehicle.Flags2)0)
		{
			this.SimulationStepBlown(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
			return;
		}
		if ((leaderData.m_flags2 & Vehicle.Flags2.Floating) != (Vehicle.Flags2)0)
		{
			this.SimulationStepFloating(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
			return;
		}
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num = this.m_info.m_acceleration;
		float num2 = this.m_info.m_braking;
		if ((vehicleData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
		{
			num *= 2f;
			num2 *= 2f;
		}
		float magnitude = frameData.m_velocity.get_magnitude();
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float sqrMagnitude = vector.get_sqrMagnitude();
		float num3 = (magnitude + num) * (0.5f + 0.5f * (magnitude + num) / num2) + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		float num4 = Mathf.Max(magnitude + num, 5f);
		if (lodPhysics >= 2 && (ulong)(currentFrameIndex >> 4 & 3u) == (ulong)((long)(vehicleID & 3)))
		{
			num4 *= 2f;
		}
		float num5 = Mathf.Max((num3 - num4) / 3f, 1f);
		float num6 = num4 * num4;
		float num7 = num5 * num5;
		int i = 0;
		bool flag = false;
		if ((sqrMagnitude < num6 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				base.UpdatePathTargetPositions(vehicleID, ref vehicleData, frameData.m_position, ref i, 4, num6, num7);
				if ((leaderData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
				{
					frameData = vehicleData.m_frame0;
					return;
				}
			}
			if ((leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				while (i < 4)
				{
					float minSqrDistance;
					Vector3 refPos;
					if (i == 0)
					{
						minSqrDistance = num6;
						refPos = frameData.m_position;
						flag = true;
					}
					else
					{
						minSqrDistance = num7;
						refPos = vehicleData.GetTargetPos(i - 1);
					}
					int num8 = i;
					this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, refPos, leaderID, ref leaderData, ref i, minSqrDistance);
					if (i == num8)
					{
						break;
					}
				}
				if (i != 0)
				{
					Vector4 targetPos = vehicleData.GetTargetPos(i - 1);
					while (i < 4)
					{
						vehicleData.SetTargetPos(i++, targetPos);
					}
				}
			}
			vector = vehicleData.m_targetPos0 - frameData.m_position;
			sqrMagnitude = vector.get_sqrMagnitude();
		}
		if (leaderData.m_path != 0u && (leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = leaderData.m_pathPositionIndex;
			byte lastPathOffset = leaderData.m_lastPathOffset;
			if (b == 255)
			{
				b = 0;
			}
			int noise;
			float num9 = 1f + leaderData.CalculateTotalLength(leaderID, out noise);
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetPosition(b >> 1, out pathPos))
			{
				if ((instance.m_segments.m_buffer[(int)pathPos.m_segment].m_flags & NetSegment.Flags.Flooded) != NetSegment.Flags.None && Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(frameData.m_position)))
				{
					leaderData.m_flags2 |= Vehicle.Flags2.Floating;
				}
				instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(num9 * 2.5f), noise);
				bool flag2 = false;
				if ((b & 1) == 0 || lastPathOffset == 0)
				{
					uint laneID = PathManager.GetLaneID(pathPos);
					if (laneID != 0u)
					{
						Vector3 vector2 = instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition((float)pathPos.m_offset * 0.003921569f);
						float num10 = 0.5f * magnitude * magnitude / num2 + this.m_info.m_generatedInfo.m_size.z * 0.5f;
						if (Vector3.Distance(frameData.m_position, vector2) >= num10 - 1f)
						{
							instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(num9);
							flag2 = true;
						}
					}
				}
				if (!flag2 && instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetNextPosition(b >> 1, out pathPos))
				{
					uint laneID2 = PathManager.GetLaneID(pathPos);
					if (laneID2 != 0u)
					{
						instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(num9);
					}
				}
			}
			if ((ulong)(currentFrameIndex >> 4 & 15u) == (ulong)((long)(leaderID & 15)))
			{
				bool flag3 = false;
				uint path = leaderData.m_path;
				int num11 = b >> 1;
				int j = 0;
				while (j < 5)
				{
					bool flag4;
					if (PathUnit.GetNextPosition(ref path, ref num11, out pathPos, out flag4))
					{
						uint laneID3 = PathManager.GetLaneID(pathPos);
						if (laneID3 != 0u && !instance.m_lanes.m_buffer[(int)((UIntPtr)laneID3)].CheckSpace(num9))
						{
							j++;
							continue;
						}
					}
					if (flag4)
					{
						this.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
					}
					flag3 = true;
					break;
				}
				if (!flag3)
				{
					leaderData.m_flags |= Vehicle.Flags.Congestion;
				}
			}
		}
		float num12;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			num12 = 0f;
		}
		else
		{
			num12 = vehicleData.m_targetPos0.w;
			if ((leaderData.m_flags & Vehicle.Flags.DummyTraffic) == (Vehicle.Flags)0)
			{
				VehicleManager instance3 = Singleton<VehicleManager>.get_instance();
				float num13 = magnitude * 100f / Mathf.Max(1f, vehicleData.m_targetPos0.w);
				instance3.m_totalTrafficFlow += (uint)Mathf.RoundToInt(num13);
				instance3.m_maxTrafficFlow += 100u;
			}
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector = quaternion * vector;
		Vector3 vector3 = quaternion * frameData.m_velocity;
		Vector3 vector4 = Vector3.get_forward();
		Vector3 vector5 = Vector3.get_zero();
		Vector3 zero = Vector3.get_zero();
		float num14 = 0f;
		float num15 = 0f;
		bool flag5 = false;
		float num16 = 0f;
		if (sqrMagnitude > 1f)
		{
			vector4 = VectorUtils.NormalizeXZ(vector, ref num16);
			if (num16 > 1f)
			{
				Vector3 vector6 = vector;
				num4 = Mathf.Max(magnitude, 2f);
				num6 = num4 * num4;
				if (sqrMagnitude > num6)
				{
					vector6 *= num4 / Mathf.Sqrt(sqrMagnitude);
				}
				bool flag6 = false;
				if (vector6.z < Mathf.Abs(vector6.x))
				{
					if (vector6.z < 0f)
					{
						flag6 = true;
					}
					float num17 = Mathf.Abs(vector6.x);
					if (num17 < 1f)
					{
						vector6.x = Mathf.Sign(vector6.x);
						if (vector6.x == 0f)
						{
							vector6.x = 1f;
						}
						num17 = 1f;
					}
					vector6.z = num17;
				}
				float num18;
				vector4 = VectorUtils.NormalizeXZ(vector6, ref num18);
				num16 = Mathf.Min(num16, num18);
				float num19 = 1.57079637f * (1f - vector4.z);
				if (num16 > 1f)
				{
					num19 /= num16;
				}
				float num20 = num16;
				if (vehicleData.m_targetPos0.w < 0.1f)
				{
					num12 = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num19);
					num12 = Mathf.Min(num12, CarAI.CalculateMaxSpeed(num20, Mathf.Min(vehicleData.m_targetPos0.w, vehicleData.m_targetPos1.w), num2 * 0.9f));
				}
				else
				{
					num12 = Mathf.Min(num12, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num19));
					num12 = Mathf.Min(num12, CarAI.CalculateMaxSpeed(num20, vehicleData.m_targetPos1.w, num2 * 0.9f));
				}
				num20 += VectorUtils.LengthXZ(vehicleData.m_targetPos1 - vehicleData.m_targetPos0);
				num12 = Mathf.Min(num12, CarAI.CalculateMaxSpeed(num20, vehicleData.m_targetPos2.w, num2 * 0.9f));
				num20 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
				num12 = Mathf.Min(num12, CarAI.CalculateMaxSpeed(num20, vehicleData.m_targetPos3.w, num2 * 0.9f));
				num20 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
				if (vehicleData.m_targetPos3.w < 0.01f)
				{
					num20 = Mathf.Max(0f, num20 - this.m_info.m_generatedInfo.m_size.z * 0.5f);
				}
				num12 = Mathf.Min(num12, CarAI.CalculateMaxSpeed(num20, 0f, num2 * 0.9f));
				if (!CarAI.DisableCollisionCheck(leaderID, ref leaderData))
				{
					CarAI.CheckOtherVehicles(vehicleID, ref vehicleData, ref frameData, ref num12, ref flag5, ref zero, num3, num2 * 0.9f, lodPhysics);
				}
				if (flag6)
				{
					num12 = -num12;
				}
				if (num12 < magnitude)
				{
					float num21 = Mathf.Max(num, Mathf.Min(num2, magnitude));
					num14 = Mathf.Max(num12, magnitude - num21);
				}
				else
				{
					float num22 = Mathf.Max(num, Mathf.Min(num2, -magnitude));
					num14 = Mathf.Min(num12, magnitude + num22);
				}
			}
		}
		else if (magnitude < 0.1f && flag && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			if (leaderID == vehicleID)
			{
				frameData = leaderData.m_frame0;
			}
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num12 < 0.1f)
		{
			flag5 = true;
		}
		if (flag5)
		{
			vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		}
		else
		{
			vehicleData.m_blockCounter = 0;
		}
		if (num16 > 1f)
		{
			num15 = Mathf.Asin(vector4.x) * Mathf.Sign(num14);
			vector5 = vector4 * num14;
		}
		else
		{
			num14 = 0f;
			Vector3 vector7 = Vector3.ClampMagnitude(vector * 0.5f - vector3, num2);
			vector5 = vector3 + vector7;
		}
		bool flag7 = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		Vector3 vector8 = vector5 - vector3;
		Vector3 vector9 = frameData.m_rotation * vector5;
		frameData.m_velocity = vector9 + zero;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector8 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = num15;
		frameData.m_travelDistance += vector5.z;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = ((vector8.z >= -0.1f) ? 0.5f : 5f);
		frameData.m_lightIntensity.z = ((num15 >= -0.1f || !flag7) ? 0f : 5f);
		frameData.m_lightIntensity.w = ((num15 <= 0.1f || !flag7) ? 0f : 5f);
		frameData.m_underground = ((vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
		frameData.m_transition = ((vehicleData.m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
		if ((vehicleData.m_flags & Vehicle.Flags.Parking) != (Vehicle.Flags)0 && num16 <= 1f && flag)
		{
			Vector3 vector10 = vehicleData.m_targetPos1 - vehicleData.m_targetPos0;
			if (vector10.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector10);
			}
		}
		else if (num14 > 0.1f)
		{
			if (vector9.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector9);
			}
		}
		else if (num14 < -0.1f && vector9.get_sqrMagnitude() > 0.01f)
		{
			frameData.m_rotation = Quaternion.LookRotation(-vector9);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	private static bool DisableCollisionCheck(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0)
		{
			float num = Mathf.Max(Mathf.Abs(vehicleData.m_targetPos3.x), Mathf.Abs(vehicleData.m_targetPos3.z));
			float num2 = 8640f;
			if (num > num2 - 100f)
			{
				return true;
			}
		}
		return false;
	}

	protected Vector4 CalculateTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float speed)
	{
		Vector3 vector = targetPos - refPos;
		float sqrMagnitude = vector.get_sqrMagnitude();
		Vector4 result;
		if (sqrMagnitude > maxSqrDistance)
		{
			result = refPos + vector * Mathf.Sqrt(maxSqrDistance / sqrMagnitude);
		}
		else
		{
			result = targetPos;
		}
		result.w = speed;
		return result;
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public static void CheckOtherVehicles(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxDistance, float maxBraking, int lodPhysics)
	{
		Vector3 vector = vehicleData.m_targetPos3 - frameData.m_position;
		Vector3 vector2 = frameData.m_position + Vector3.ClampMagnitude(vector, maxDistance);
		Vector3 min = Vector3.Min(vehicleData.m_segment.Min(), vector2);
		Vector3 max = Vector3.Max(vehicleData.m_segment.Max(), vector2);
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((min.x - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((min.z - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((max.x + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((max.z + 10f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = CarAI.CheckOtherVehicle(vehicleID, ref vehicleData, ref frameData, ref maxSpeed, ref blocked, ref collisionPush, maxBraking, num5, ref instance.m_vehicles.m_buffer[(int)num5], min, max, lodPhysics);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		if (lodPhysics == 0)
		{
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			float num7 = 0f;
			Vector3 vector3 = vehicleData.m_segment.b;
			Vector3 vector4 = vehicleData.m_segment.b - vehicleData.m_segment.a;
			for (int k = 0; k < 4; k++)
			{
				Vector3 vector5 = vehicleData.GetTargetPos(k);
				Vector3 vector6 = vector5 - vector3;
				if (Vector3.Dot(vector4, vector6) > 0f)
				{
					float magnitude = vector6.get_magnitude();
					if (magnitude > 0.01f)
					{
						Segment3 segment;
						segment..ctor(vector3, vector5);
						min = segment.Min();
						max = segment.Max();
						int num8 = Mathf.Max((int)((min.x - 3f) / 8f + 1080f), 0);
						int num9 = Mathf.Max((int)((min.z - 3f) / 8f + 1080f), 0);
						int num10 = Mathf.Min((int)((max.x + 3f) / 8f + 1080f), 2159);
						int num11 = Mathf.Min((int)((max.z + 3f) / 8f + 1080f), 2159);
						for (int l = num9; l <= num11; l++)
						{
							for (int m = num8; m <= num10; m++)
							{
								ushort num12 = instance2.m_citizenGrid[l * 2160 + m];
								int num13 = 0;
								while (num12 != 0)
								{
									num12 = CarAI.CheckCitizen(vehicleID, ref vehicleData, segment, num7, magnitude, ref maxSpeed, ref blocked, maxBraking, num12, ref instance2.m_instances.m_buffer[(int)num12], min, max);
									if (++num13 > 65536)
									{
										CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
										break;
									}
								}
							}
						}
					}
					vector4 = vector6;
					num7 += magnitude;
					vector3 = vector5;
				}
			}
		}
	}

	private static ushort CheckCitizen(ushort vehicleID, ref Vehicle vehicleData, Segment3 segment, float lastLen, float nextLen, ref float maxSpeed, ref bool blocked, float maxBraking, ushort otherID, ref CitizenInstance otherData, Vector3 min, Vector3 max)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (otherData.m_flags & CitizenInstance.Flags.Transition) == CitizenInstance.Flags.None && (vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0 != ((otherData.m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None))
		{
			return otherData.m_nextGridInstance;
		}
		if ((otherData.m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None)
		{
			return otherData.m_nextGridInstance;
		}
		CitizenInfo info = otherData.Info;
		CitizenInstance.Frame lastFrameData = otherData.GetLastFrameData();
		Vector3 position = lastFrameData.m_position;
		Vector3 vector = lastFrameData.m_position + lastFrameData.m_velocity;
		Segment3 segment2;
		segment2..ctor(position, vector);
		Vector3 vector2 = segment2.Min();
		vector2.x -= info.m_radius;
		vector2.z -= info.m_radius;
		Vector3 vector3 = segment2.Max();
		vector3.x += info.m_radius;
		vector3.y += info.m_height;
		vector3.z += info.m_radius;
		float num;
		float num2;
		if (min.x < vector3.x + 1f && min.y < vector3.y && min.z < vector3.z + 1f && vector2.x < max.x + 1f && vector2.y < max.y + 2f && vector2.z < max.z + 1f && segment.DistanceSqr(segment2, ref num, ref num2) < (1f + info.m_radius) * (1f + info.m_radius))
		{
			float num3 = lastLen + nextLen * num;
			if (num3 >= 0.01f)
			{
				num3 -= 2f;
				float num4 = Mathf.Max(1f, CarAI.CalculateMaxSpeed(num3, 0f, maxBraking));
				maxSpeed = Mathf.Min(maxSpeed, num4);
			}
		}
		return otherData.m_nextGridInstance;
	}

	private static ushort CheckOtherVehicle(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxBraking, ushort otherID, ref Vehicle otherData, Vector3 min, Vector3 max, int lodPhysics)
	{
		if (otherID != vehicleID && vehicleData.m_leadingVehicle != otherID && vehicleData.m_trailingVehicle != otherID)
		{
			VehicleInfo info = otherData.Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				return otherData.m_nextGridVehicle;
			}
			if (((vehicleData.m_flags | otherData.m_flags) & Vehicle.Flags.Transition) == (Vehicle.Flags)0 && (vehicleData.m_flags & Vehicle.Flags.Underground) != (otherData.m_flags & Vehicle.Flags.Underground))
			{
				return otherData.m_nextGridVehicle;
			}
			Vector3 vector;
			Vector3 vector2;
			if (lodPhysics >= 2)
			{
				vector = otherData.m_segment.Min();
				vector2 = otherData.m_segment.Max();
			}
			else
			{
				vector = Vector3.Min(otherData.m_segment.Min(), otherData.m_targetPos3);
				vector2 = Vector3.Max(otherData.m_segment.Max(), otherData.m_targetPos3);
			}
			if (min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
			{
				Vehicle.Frame lastFrameData = otherData.GetLastFrameData();
				if (lodPhysics < 2)
				{
					float num2;
					float num3;
					float num = vehicleData.m_segment.DistanceSqr(otherData.m_segment, ref num2, ref num3);
					if (num < 4f)
					{
						Vector3 vector3 = vehicleData.m_segment.Position(0.5f);
						Vector3 vector4 = otherData.m_segment.Position(0.5f);
						Vector3 vector5 = vehicleData.m_segment.b - vehicleData.m_segment.a;
						if (Vector3.Dot(vector5, vector3 - vector4) < 0f)
						{
							collisionPush -= vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						else
						{
							collisionPush += vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						blocked = true;
					}
				}
				float num4 = frameData.m_velocity.get_magnitude() + 0.01f;
				float num5 = lastFrameData.m_velocity.get_magnitude();
				float num6 = num5 * (0.5f + 0.5f * num5 / info.m_braking) + Mathf.Min(1f, num5);
				num5 += 0.01f;
				float num7 = 0f;
				Vector3 vector6 = vehicleData.m_segment.b;
				Vector3 vector7 = vehicleData.m_segment.b - vehicleData.m_segment.a;
				int num8 = (vehicleData.Info.m_vehicleType != VehicleInfo.VehicleType.Tram) ? 0 : 1;
				for (int i = num8; i < 4; i++)
				{
					Vector3 vector8 = vehicleData.GetTargetPos(i);
					Vector3 vector9 = vector8 - vector6;
					if (Vector3.Dot(vector7, vector9) > 0f)
					{
						float magnitude = vector9.get_magnitude();
						Segment3 segment;
						segment..ctor(vector6, vector8);
						min = segment.Min();
						max = segment.Max();
						segment.a.y = segment.a.y * 0.5f;
						segment.b.y = segment.b.y * 0.5f;
						if (magnitude > 0.01f && min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
						{
							Vector3 a = otherData.m_segment.a;
							a.y *= 0.5f;
							float num9;
							if (segment.DistanceSqr(a, ref num9) < 4f)
							{
								float num10 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
								float num11 = num7 + magnitude * num9;
								if (num11 >= 0.01f)
								{
									num11 -= num10 + 3f;
									float num12 = Mathf.Max(0f, CarAI.CalculateMaxSpeed(num11, num10, maxBraking));
									if (num12 < 0.01f)
									{
										blocked = true;
									}
									Vector3 vector10 = Vector3.Normalize(otherData.m_targetPos0 - otherData.m_segment.a);
									float num13 = 1.2f - 1f / ((float)vehicleData.m_blockCounter * 0.02f + 0.5f);
									if (Vector3.Dot(vector9, vector10) > num13 * magnitude)
									{
										maxSpeed = Mathf.Min(maxSpeed, num12);
									}
								}
								break;
							}
							if (lodPhysics < 2)
							{
								float num14 = 0f;
								float num15 = num6;
								Vector3 vector11 = otherData.m_segment.b;
								Vector3 vector12 = otherData.m_segment.b - otherData.m_segment.a;
								int num16 = (info.m_vehicleType != VehicleInfo.VehicleType.Tram) ? 0 : 1;
								bool flag = false;
								int num17 = num16;
								while (num17 < 4 && num15 > 0.1f)
								{
									Vector3 vector13;
									if (otherData.m_leadingVehicle != 0)
									{
										if (num17 != num16)
										{
											break;
										}
										vector13 = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)otherData.m_leadingVehicle].m_segment.b;
									}
									else
									{
										vector13 = otherData.GetTargetPos(num17);
									}
									Vector3 vector14 = Vector3.ClampMagnitude(vector13 - vector11, num15);
									if (Vector3.Dot(vector12, vector14) > 0f)
									{
										vector13 = vector11 + vector14;
										float magnitude2 = vector14.get_magnitude();
										num15 -= magnitude2;
										Segment3 segment2;
										segment2..ctor(vector11, vector13);
										segment2.a.y = segment2.a.y * 0.5f;
										segment2.b.y = segment2.b.y * 0.5f;
										if (magnitude2 > 0.01f)
										{
											float num19;
											float num20;
											float num18;
											if (otherID < vehicleID)
											{
												num18 = segment2.DistanceSqr(segment, ref num19, ref num20);
											}
											else
											{
												num18 = segment.DistanceSqr(segment2, ref num20, ref num19);
											}
											if (num18 < 4f)
											{
												float num21 = num7 + magnitude * num20;
												float num22 = num14 + magnitude2 * num19 + 0.1f;
												if (num21 >= 0.01f && num21 * num5 > num22 * num4)
												{
													float num23 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
													if (num21 >= 0.01f)
													{
														num21 -= num23 + 1f + otherData.Info.m_generatedInfo.m_size.z;
														float num24 = Mathf.Max(0f, CarAI.CalculateMaxSpeed(num21, num23, maxBraking));
														if (num24 < 0.01f)
														{
															blocked = true;
														}
														maxSpeed = Mathf.Min(maxSpeed, num24);
													}
												}
												flag = true;
												break;
											}
										}
										vector12 = vector14;
										num14 += magnitude2;
										vector11 = vector13;
									}
									num17++;
								}
								if (flag)
								{
									break;
								}
							}
						}
						vector7 = vector9;
						num7 += magnitude;
						vector6 = vector8;
					}
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((vector.z - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((vector2.z + 10f) / 32f + 270f), 539);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = CarAI.CheckOverlap(segment, ignoreVehicle, maxVelocity, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 4f)
		{
			VehicleInfo info = otherData.Info;
			if (info.m_vehicleType == VehicleInfo.VehicleType.Bicycle)
			{
				return otherData.m_nextGridVehicle;
			}
			if (otherData.GetLastFrameData().m_velocity.get_sqrMagnitude() < maxVelocity * maxVelocity)
			{
				overlap = true;
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override void InvalidPath(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData)
	{
		vehicleData.m_targetPos0 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos3.w = 0f;
		base.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
	}

	protected override void UpdateNodeTargetPos(ushort vehicleID, ref Vehicle vehicleData, ushort nodeID, ref NetNode nodeData, ref Vector4 targetPos, int index)
	{
		if ((nodeData.m_flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None)
		{
			if (targetPos.w > 4f)
			{
				targetPos.w = 4f;
			}
			if (index <= 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				for (int i = 0; i < 7; i++)
				{
					ushort segment = nodeData.GetSegment(i);
					if (segment != 0)
					{
						NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
						if (info.m_class.m_service == ItemClass.Service.PublicTransport)
						{
							Vector3 vector;
							Vector3 startDir;
							float num;
							if (CarAI.CalculateCrossing(info, segment, ref instance.m_segments.m_buffer[(int)segment], nodeID, out vector, out startDir, out num))
							{
								for (int j = i + 1; j < 8; j++)
								{
									ushort segment2 = nodeData.GetSegment(j);
									if (segment2 != 0)
									{
										NetInfo info2 = instance.m_segments.m_buffer[(int)segment2].Info;
										if (info2.m_class.m_service == ItemClass.Service.PublicTransport)
										{
											Vector3 vector2;
											Vector3 endDir;
											float num2;
											if (CarAI.CalculateCrossing(info2, segment2, ref instance.m_segments.m_buffer[(int)segment2], nodeID, out vector2, out endDir, out num2))
											{
												Vector3 vector3;
												Vector3 vector4;
												NetSegment.CalculateMiddlePoints(vector, startDir, vector2, endDir, true, true, out vector3, out vector4);
												float num4;
												float num3 = Mathf.Sqrt(Bezier2.XZ(vector, vector3, vector4, vector2).DistanceSqr(VectorUtils.XZ(targetPos), ref num4));
												float num5 = num + (num2 - num) * num4;
												if (num3 < num5 + 1f)
												{
													float num6 = vector.y + (vector2.y - vector.y) * num4 + 0.1f;
													if (num3 > num5 - 1f)
													{
														num6 -= (num3 - num5 + 1f) * 0.3f;
													}
													if (num6 > targetPos.y)
													{
														targetPos.y = num6;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static bool CalculateCrossing(NetInfo segmentInfo, ushort segmentID, ref NetSegment segmentData, ushort nodeID, out Vector3 position, out Vector3 direction, out float radius)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		position = Vector3.get_zero();
		direction = Vector3.get_zero();
		uint num = segmentData.m_lanes;
		float num2 = 0f;
		radius = 0f;
		int num3 = 0;
		while (num3 < segmentInfo.m_lanes.Length && num != 0u)
		{
			if ((byte)(segmentInfo.m_lanes[num3].m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle)) != 0)
			{
				if (segmentData.m_startNode == nodeID)
				{
					position += instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.a;
					direction += instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.a - instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.b;
				}
				else
				{
					position += instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.d;
					direction += instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.d - instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_bezier.c;
				}
				num2 += 1f;
				radius = Mathf.Max(radius, Mathf.Abs(segmentInfo.m_lanes[num3].m_position) + segmentInfo.m_lanes[num3].m_width * 0.5f);
			}
			num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
			num3++;
		}
		if (num2 != 0f)
		{
			position /= num2;
			direction = VectorUtils.NormalizeXZ(direction);
			return true;
		}
		return false;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		float num = this.m_info.m_braking;
		if ((vehicleData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
		{
			num *= 2f;
		}
		Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
		Vector3 position2 = lastFrameData.m_position;
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].CalculatePosition((float)prevOffset * 0.003921569f);
		float sqrMagnitude = lastFrameData.m_velocity.get_sqrMagnitude();
		float num2 = 0.5f * sqrMagnitude / num + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		if (Vector3.Distance(position2, vector) >= num2 - 1f)
		{
			Segment3 segment;
			segment.a = pos;
			ushort num3;
			ushort num4;
			if (offset < position.m_offset)
			{
				segment.b = pos + dir.get_normalized() * this.m_info.m_generatedInfo.m_size.z;
				num3 = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
				num4 = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			}
			else
			{
				segment.b = pos - dir.get_normalized() * this.m_info.m_generatedInfo.m_size.z;
				num3 = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
				num4 = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			}
			ushort num5;
			if (prevOffset == 0)
			{
				num5 = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_startNode;
			}
			else
			{
				num5 = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_endNode;
			}
			if (num3 == num5)
			{
				NetNode.Flags flags = instance.m_nodes.m_buffer[(int)num3].m_flags;
				NetLane.Flags flags2 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].m_flags;
				bool flag = (flags & NetNode.Flags.TrafficLights) != NetNode.Flags.None;
				bool flag2 = (flags & NetNode.Flags.LevelCrossing) != NetNode.Flags.None;
				bool flag3 = (flags2 & NetLane.Flags.JoinedJunction) != NetLane.Flags.None;
				bool flag4 = (flags2 & (NetLane.Flags.YieldStart | NetLane.Flags.YieldEnd)) != NetLane.Flags.None && (flags & (NetNode.Flags.Junction | NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn)) == NetNode.Flags.Junction;
				if (flag4 && sqrMagnitude > 0.01f && (vehicleData.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0)
				{
					maxSpeed = 0f;
					return;
				}
				if ((flags & (NetNode.Flags.Junction | NetNode.Flags.OneWayOut | NetNode.Flags.OneWayIn)) == NetNode.Flags.Junction && instance.m_nodes.m_buffer[(int)num3].CountSegments() != 2 && (vehicleData.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0)
				{
					float len = vehicleData.CalculateTotalLength(vehicleID) + 2f;
					if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(len))
					{
						bool flag5 = false;
						if (nextPosition.m_segment != 0 && instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_length < 30f)
						{
							NetNode.Flags flags3 = instance.m_nodes.m_buffer[(int)num4].m_flags;
							if ((flags3 & (NetNode.Flags.Junction | NetNode.Flags.OneWayOut | NetNode.Flags.OneWayIn)) != NetNode.Flags.Junction || instance.m_nodes.m_buffer[(int)num4].CountSegments() == 2)
							{
								uint laneID2 = PathManager.GetLaneID(nextPosition);
								if (laneID2 != 0u)
								{
									flag5 = instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].CheckSpace(len);
								}
							}
						}
						if (!flag5)
						{
							maxSpeed = 0f;
							return;
						}
					}
				}
				if (flag && (!flag3 || flag2))
				{
					uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
					uint num6 = (uint)(((int)num5 << 8) / 32768);
					uint num7 = currentFrameIndex - num6 & 255u;
					NetInfo info = instance.m_nodes.m_buffer[(int)num3].Info;
					RoadBaseAI.TrafficLightState trafficLightState;
					RoadBaseAI.TrafficLightState pedestrianLightState;
					bool flag6;
					bool pedestrians;
					RoadBaseAI.GetTrafficLightState(num5, ref instance.m_segments.m_buffer[(int)prevPos.m_segment], currentFrameIndex - num6, out trafficLightState, out pedestrianLightState, out flag6, out pedestrians);
					if (!flag6 && num7 >= 196u)
					{
						flag6 = true;
						RoadBaseAI.SetTrafficLightState(num5, ref instance.m_segments.m_buffer[(int)prevPos.m_segment], currentFrameIndex - num6, trafficLightState, pedestrianLightState, flag6, pedestrians);
					}
					if ((vehicleData.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0 || info.m_class.m_service != ItemClass.Service.Road)
					{
						if (trafficLightState != RoadBaseAI.TrafficLightState.RedToGreen)
						{
							if (trafficLightState != RoadBaseAI.TrafficLightState.GreenToRed)
							{
								if (trafficLightState == RoadBaseAI.TrafficLightState.Red)
								{
									maxSpeed = 0f;
									return;
								}
							}
							else if (num7 >= 30u)
							{
								maxSpeed = 0f;
								return;
							}
						}
						else if (num7 < 60u)
						{
							maxSpeed = 0f;
							return;
						}
					}
				}
			}
		}
		NetInfo info2 = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info2.m_lanes != null && info2.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info2.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
		if (instance.m_treatWetAsSnow)
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(pos);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.StuddedTires) != DistrictPolicies.CityPlanning.None)
			{
				maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.0005882353f;
				District[] expr_641_cp_0 = instance2.m_districts.m_buffer;
				byte expr_641_cp_1 = district;
				expr_641_cp_0[(int)expr_641_cp_1].m_cityPlanningPoliciesEffect = (expr_641_cp_0[(int)expr_641_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.StuddedTires);
			}
			else
			{
				maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.00117647066f;
			}
		}
		else
		{
			maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.0005882353f;
		}
		maxSpeed *= 1f + (float)instance.m_segments.m_buffer[(int)position.m_segment].m_condition * 0.0005882353f;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
		if (instance.m_treatWetAsSnow)
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(pos);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance2.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.StuddedTires) != DistrictPolicies.CityPlanning.None)
			{
				maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.0005882353f;
				District[] expr_13C_cp_0 = instance2.m_districts.m_buffer;
				byte expr_13C_cp_1 = district;
				expr_13C_cp_0[(int)expr_13C_cp_1].m_cityPlanningPoliciesEffect = (expr_13C_cp_0[(int)expr_13C_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.StuddedTires);
			}
			else
			{
				maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.00117647066f;
			}
		}
		else
		{
			maxSpeed *= 1f - (float)instance.m_segments.m_buffer[(int)position.m_segment].m_wetness * 0.0005882353f;
		}
		maxSpeed *= 1f + (float)instance.m_segments.m_buffer[(int)position.m_segment].m_condition * 0.0005882353f;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true, false);
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, undergroundTarget, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, default(PathUnit.Position), NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, this.IsHeavyVehicle(), this.IgnoreBlocked(vehicleID, ref vehicleData), false, false, false, false, this.CombustionEngine()))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (CarAI.CheckOverlap(vehicleData.m_segment, 0, 1000f))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	public override int GetNoiseLevel()
	{
		return (!this.IsHeavyVehicle()) ? 5 : 9;
	}

	protected virtual bool IsHeavyVehicle()
	{
		return false;
	}

	protected virtual bool CombustionEngine()
	{
		return false;
	}
}
