﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class FerryAI : VehicleAI
{
	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Vector3 vector = rotation * new Vector3(0f, 0f, this.m_info.m_generatedInfo.m_size.z * 0.25f);
		Vector3 vector2 = rotation * new Vector3(this.m_info.m_generatedInfo.m_size.x * -0.5f, 0f, 0f);
		Vector3 vector3 = position + vector + vector2;
		Vector3 vector4 = position + vector - vector2;
		Vector3 vector5 = position - vector + vector2;
		Vector3 vector6 = position - vector - vector2;
		vector3.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(vector3, true, 0f);
		vector4.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(vector4, true, 0f);
		vector5.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(vector5, true, 0f);
		vector6.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(vector6, true, 0f);
		Vector3 vector7 = vector3 + vector4 - vector5 - vector6;
		Vector3 vector8 = vector3 + vector5 - vector4 - vector6;
		Vector3 vector9 = Vector3.Cross(vector8, vector7);
		rotation = Quaternion.LookRotation(vector7, vector9);
		position.y = (vector3.y + vector4.y + vector5.y + vector6.y) * 0.25f;
		Quaternion quaternion = Quaternion.Euler(swayPosition.z * 57.29578f, 0f, swayPosition.x * -57.29578f);
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation * quaternion, scale);
		result.m13 += swayPosition.y;
		return result;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		return Matrix4x4.get_identity();
	}

	public override RenderGroup.MeshData GetEffectMeshData()
	{
		if (this.m_info.m_lodMeshData != null && this.m_underwaterMeshData == null)
		{
			Vector3[] vertices = this.m_info.m_lodMeshData.m_vertices;
			int[] triangles = this.m_info.m_lodMeshData.m_triangles;
			if (triangles != null)
			{
				int[] array = new int[vertices.Length];
				for (int i = 0; i < vertices.Length; i++)
				{
					array[i] = -1;
				}
				int num = 0;
				int num2 = 0;
				for (int j = 0; j < triangles.Length; j += 3)
				{
					int num3 = triangles[j];
					int num4 = triangles[j + 1];
					int num5 = triangles[j + 2];
					Vector3 vector = vertices[num3];
					Vector3 vector2 = vertices[num4];
					Vector3 vector3 = vertices[num5];
					if (vector.y < -2f || vector2.y < -2f || vector3.y < -2f)
					{
						num2 += 3;
						if (array[num3] == -1)
						{
							array[num3] = num++;
						}
						if (array[num4] == -1)
						{
							array[num4] = num++;
						}
						if (array[num5] == -1)
						{
							array[num5] = num++;
						}
					}
				}
				this.m_underwaterMeshData = new RenderGroup.MeshData();
				this.m_underwaterMeshData.m_vertices = new Vector3[num];
				this.m_underwaterMeshData.m_triangles = new int[num2];
				num2 = 0;
				for (int k = 0; k < triangles.Length; k += 3)
				{
					int num6 = triangles[k];
					int num7 = triangles[k + 1];
					int num8 = triangles[k + 2];
					Vector3 vector4 = vertices[num6];
					Vector3 vector5 = vertices[num7];
					Vector3 vector6 = vertices[num8];
					if (vector4.y < -2f || vector5.y < -2f || vector6.y < -2f)
					{
						vector4.y = Mathf.Min(vector4.y, -2f);
						vector5.y = Mathf.Min(vector5.y, -2f);
						vector6.y = Mathf.Min(vector6.y, -2f);
						num6 = array[num6];
						num7 = array[num7];
						num8 = array[num8];
						this.m_underwaterMeshData.m_triangles[num2++] = num6;
						this.m_underwaterMeshData.m_triangles[num2++] = num7;
						this.m_underwaterMeshData.m_triangles[num2++] = num8;
						this.m_underwaterMeshData.m_vertices[num6] = vector4;
						this.m_underwaterMeshData.m_vertices[num7] = vector5;
						this.m_underwaterMeshData.m_vertices[num8] = vector6;
					}
				}
				this.m_underwaterMeshData.UpdateBounds();
			}
		}
		return this.m_underwaterMeshData;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		bool flag;
		data.m_frame0.m_position.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(data.m_frame0.m_position, false, 0f, out flag);
		data.m_frame1.m_position.y = data.m_frame0.m_position.y;
		data.m_frame2.m_position.y = data.m_frame0.m_position.y;
		data.m_frame3.m_position.y = data.m_frame0.m_position.y;
		if (flag)
		{
			data.m_flags2 |= Vehicle.Flags2.Floating;
		}
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				data.m_flags &= ~Vehicle.Flags.Arriving;
				this.PathfindSuccess(vehicleID, ref data);
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(vehicleID, ref data);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		Vector3 lastFramePosition = data.GetLastFramePosition();
		int lodPhysics;
		if (Vector3.SqrMagnitude(physicsLodRefPos - lastFramePosition) >= 1210000f)
		{
			lodPhysics = 2;
		}
		else if (Vector3.SqrMagnitude(Singleton<SimulationManager>.get_instance().m_simulationView.m_position - lastFramePosition) >= 250000f)
		{
			lodPhysics = 1;
		}
		else
		{
			lodPhysics = 0;
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, lodPhysics);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, lodPhysics);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(this.m_info.m_class.m_service);
		int num3 = (privateServiceIndex == -1) ? 150 : 100;
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 && data.m_cargoParent == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
		else if ((int)data.m_blockCounter == num3)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		data.Unspawn(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float acceleration = this.m_info.m_acceleration;
		float braking = this.m_info.m_braking;
		float num = VectorUtils.LengthXZ(frameData.m_velocity);
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float num2 = VectorUtils.LengthSqrXZ(vector);
		float num3 = (num + acceleration) * (0.5f + 0.5f * (num + acceleration) / braking) + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		float num4 = Mathf.Max(num + acceleration, 5f);
		if (lodPhysics >= 2 && (ulong)(currentFrameIndex >> 4 & 3u) == (ulong)((long)(vehicleID & 3)))
		{
			num4 *= 2f;
		}
		float num5 = Mathf.Max((num3 - num4) / 3f, 1f);
		float num6 = num4 * num4;
		float num7 = num5 * num5;
		int i = 0;
		bool flag = false;
		if ((num2 < num6 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				this.UpdatePathTargetPositions(vehicleID, ref vehicleData, frameData.m_position, ref i, 4, num6, num7);
				if ((leaderData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
				{
					frameData = vehicleData.m_frame0;
					return;
				}
			}
			if ((leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				while (i < 4)
				{
					float minSqrDistance;
					Vector3 refPos;
					if (i == 0)
					{
						minSqrDistance = num6;
						refPos = frameData.m_position;
						flag = true;
					}
					else
					{
						minSqrDistance = num7;
						refPos = vehicleData.GetTargetPos(i - 1);
					}
					int num8 = i;
					this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, refPos, leaderID, ref leaderData, ref i, minSqrDistance);
					if (i == num8)
					{
						break;
					}
				}
				if (i != 0)
				{
					Vector4 targetPos = vehicleData.GetTargetPos(i - 1);
					while (i < 4)
					{
						vehicleData.SetTargetPos(i++, targetPos);
					}
				}
			}
			vector = vehicleData.m_targetPos0 - frameData.m_position;
			num2 = VectorUtils.LengthSqrXZ(vector);
		}
		if (leaderData.m_path != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = leaderData.m_pathPositionIndex;
			byte lastPathOffset = leaderData.m_lastPathOffset;
			if (b == 255)
			{
				b = 0;
			}
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetPosition(b >> 1, out pathPos))
			{
				instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(this.m_info.m_generatedInfo.m_size.z * 3f), this.GetNoiseLevel());
				if ((b & 1) == 0 || lastPathOffset == 0 || (leaderData.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
				{
					uint laneID = PathManager.GetLaneID(pathPos);
					if (laneID != 0u)
					{
						instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(this.m_info.m_generatedInfo.m_size.z);
					}
				}
				else if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetNextPosition(b >> 1, out pathPos))
				{
					uint laneID2 = PathManager.GetLaneID(pathPos);
					if (laneID2 != 0u)
					{
						instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(this.m_info.m_generatedInfo.m_size.z);
					}
				}
			}
		}
		float num9;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 || (leaderData.m_flags2 & Vehicle.Flags2.Floating) == (Vehicle.Flags2)0)
		{
			num9 = 0f;
		}
		else
		{
			num9 = vehicleData.m_targetPos0.w;
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector = quaternion * vector;
		Vector3 vector2 = quaternion * frameData.m_velocity;
		Vector3 vector3 = Vector3.get_forward();
		Vector3 vector4 = Vector3.get_zero();
		Vector3 zero = Vector3.get_zero();
		float num10 = 0f;
		float num11 = 0f;
		bool flag2 = false;
		float num12 = 0f;
		if (num2 > 1f)
		{
			vector3 = VectorUtils.NormalizeXZ(vector, ref num12);
			if (num12 > 1f)
			{
				Vector3 vector5 = vector;
				num4 = Mathf.Max(num, 2f);
				num6 = num4 * num4;
				if (num2 > num6)
				{
					vector5 *= num4 / Mathf.Sqrt(num2);
				}
				float num13;
				vector3 = VectorUtils.NormalizeXZ(vector5, ref num13);
				num12 = Mathf.Min(num12, num13);
				float num14 = 1.57079637f * (1f - vector3.z);
				if (num12 > 1f)
				{
					num14 /= num12;
				}
				float num15 = num12;
				if (vehicleData.m_targetPos0.w < 0.1f)
				{
					num9 = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14);
					num9 = Mathf.Min(num9, FerryAI.CalculateMaxSpeed(num15, Mathf.Min(vehicleData.m_targetPos0.w, vehicleData.m_targetPos1.w), braking * 0.9f));
				}
				else
				{
					num9 = Mathf.Min(num9, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14));
					num9 = Mathf.Min(num9, FerryAI.CalculateMaxSpeed(num15, vehicleData.m_targetPos1.w, braking * 0.9f));
				}
				num15 += VectorUtils.LengthXZ(vehicleData.m_targetPos1 - vehicleData.m_targetPos0);
				num9 = Mathf.Min(num9, FerryAI.CalculateMaxSpeed(num15, vehicleData.m_targetPos2.w, braking * 0.9f));
				num15 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
				num9 = Mathf.Min(num9, FerryAI.CalculateMaxSpeed(num15, vehicleData.m_targetPos3.w, braking * 0.9f));
				num15 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
				if (vehicleData.m_targetPos3.w < 0.01f)
				{
					num15 = Mathf.Max(0f, num15 - this.m_info.m_generatedInfo.m_size.z * 0.5f);
				}
				num9 = Mathf.Min(num9, FerryAI.CalculateMaxSpeed(num15, 0f, braking * 0.9f));
				if (!FerryAI.DisableCollisionCheck(leaderID, ref leaderData))
				{
					FerryAI.CheckOtherVehicles(vehicleID, ref vehicleData, ref frameData, ref num9, ref flag2, ref zero, num3, braking * 0.9f, lodPhysics);
				}
				if (num9 < num)
				{
					float num16 = Mathf.Max(acceleration, Mathf.Min(braking, num));
					num10 = Mathf.Max(num9, num - num16);
				}
				else
				{
					float num17 = Mathf.Max(acceleration, Mathf.Min(braking, -num));
					num10 = Mathf.Min(num9, num + num17);
				}
			}
		}
		else if (num < 0.1f && flag && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			if (leaderID == vehicleID)
			{
				frameData = leaderData.m_frame0;
			}
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num9 < 0.1f)
		{
			flag2 = true;
		}
		if (flag2)
		{
			vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		}
		else
		{
			vehicleData.m_blockCounter = 0;
		}
		if (num12 > 1f)
		{
			num11 = Mathf.Asin(vector3.x) * Mathf.Sign(num10);
			vector4 = vector3 * num10;
		}
		else
		{
			num10 = 0f;
			Vector3 vector6 = vector * 0.5f - vector2;
			vector6.y = 0f;
			vector6 = Vector3.ClampMagnitude(vector6, braking);
			vector4 = vector2 + vector6;
		}
		bool flag3 = (currentFrameIndex + (uint)leaderID & 16u) != 0u;
		Vector3 vector7 = vector4 - vector2;
		vector7.y = 0f;
		Vector3 vector8 = frameData.m_rotation * vector4;
		bool flag4;
		vector8.y = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmoothWithWater(frameData.m_position + frameData.m_velocity, false, 0f, out flag4) - frameData.m_position.y;
		if (flag4)
		{
			leaderData.m_flags2 |= Vehicle.Flags2.Floating;
		}
		else
		{
			leaderData.m_flags2 &= ~Vehicle.Flags2.Floating;
		}
		frameData.m_velocity = vector8 + zero;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector7 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		frameData.m_steerAngle = num11;
		frameData.m_travelDistance += vector4.z;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = ((vector7.z >= -0.1f) ? 0.5f : 5f);
		frameData.m_lightIntensity.z = ((num11 >= -0.1f || !flag3) ? 0f : 5f);
		frameData.m_lightIntensity.w = ((num11 <= 0.1f || !flag3) ? 0f : 5f);
		frameData.m_underground = ((vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
		frameData.m_transition = ((vehicleData.m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
		if ((vehicleData.m_flags & Vehicle.Flags.Parking) != (Vehicle.Flags)0 && num12 <= 1f && flag)
		{
			Vector3 vector9 = vehicleData.m_targetPos1 - vehicleData.m_targetPos0;
			vector9.y = 0f;
			if (vector9.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.RotateTowards(frameData.m_rotation, Quaternion.LookRotation(vector9), 15f);
			}
		}
		else if (num10 > 0.1f)
		{
			vector8.y = 0f;
			if (vector8.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.RotateTowards(frameData.m_rotation, Quaternion.LookRotation(vector8), 15f);
			}
		}
		else if (num10 < -0.1f)
		{
			vector8.y = 0f;
			if (vector8.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.RotateTowards(frameData.m_rotation, Quaternion.LookRotation(-vector8), 15f);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	private static bool DisableCollisionCheck(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Arriving) != (Vehicle.Flags)0)
		{
			float num = Mathf.Max(Mathf.Abs(vehicleData.m_targetPos3.x), Mathf.Abs(vehicleData.m_targetPos3.z));
			float num2 = 8640f;
			if (num > num2 - 100f)
			{
				return true;
			}
		}
		return false;
	}

	protected Vector4 CalculateTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float speed)
	{
		Vector3 vector = targetPos - refPos;
		float sqrMagnitude = vector.get_sqrMagnitude();
		Vector4 result;
		if (sqrMagnitude > maxSqrDistance)
		{
			result = refPos + vector * Mathf.Sqrt(maxSqrDistance / sqrMagnitude);
		}
		else
		{
			result = targetPos;
		}
		result.w = speed;
		return result;
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public static void CheckOtherVehicles(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxDistance, float maxBraking, int lodPhysics)
	{
		Vector3 vector = vehicleData.m_targetPos3 - frameData.m_position;
		Vector3 vector2 = frameData.m_position + Vector3.ClampMagnitude(vector, maxDistance);
		Vector3 min = Vector3.Min(vehicleData.m_segment.Min(), vector2);
		Vector3 max = Vector3.Max(vehicleData.m_segment.Max(), vector2);
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((min.x - 10f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((min.z - 10f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((max.x + 10f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((max.z + 10f) / 320f + 27f), 53);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = FerryAI.CheckOtherVehicle(vehicleID, ref vehicleData, ref frameData, ref maxSpeed, ref blocked, ref collisionPush, maxBraking, num5, ref instance.m_vehicles.m_buffer[(int)num5], min, max, lodPhysics);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private static ushort CheckOtherVehicle(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ref float maxSpeed, ref bool blocked, ref Vector3 collisionPush, float maxBraking, ushort otherID, ref Vehicle otherData, Vector3 min, Vector3 max, int lodPhysics)
	{
		if (otherID != vehicleID && vehicleData.m_leadingVehicle != otherID && vehicleData.m_trailingVehicle != otherID)
		{
			VehicleInfo info = otherData.Info;
			Vector3 vector;
			Vector3 vector2;
			if (lodPhysics >= 2)
			{
				vector = otherData.m_segment.Min();
				vector2 = otherData.m_segment.Max();
			}
			else
			{
				vector = Vector3.Min(otherData.m_segment.Min(), otherData.m_targetPos3);
				vector2 = Vector3.Max(otherData.m_segment.Max(), otherData.m_targetPos3);
			}
			if (min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
			{
				Vehicle.Frame lastFrameData = otherData.GetLastFrameData();
				if (lodPhysics < 2)
				{
					float num2;
					float num3;
					float num = vehicleData.m_segment.DistanceSqr(otherData.m_segment, ref num2, ref num3);
					if (num < 4f)
					{
						Vector3 vector3 = vehicleData.m_segment.Position(0.5f);
						Vector3 vector4 = otherData.m_segment.Position(0.5f);
						Vector3 vector5 = vehicleData.m_segment.b - vehicleData.m_segment.a;
						if (Vector3.Dot(vector5, vector3 - vector4) < 0f)
						{
							collisionPush -= vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						else
						{
							collisionPush += vector5.get_normalized() * (0.1f - num * 0.025f);
						}
						blocked = true;
					}
				}
				float num4 = frameData.m_velocity.get_magnitude() + 0.01f;
				float num5 = lastFrameData.m_velocity.get_magnitude();
				float num6 = num5 * (0.5f + 0.5f * num5 / info.m_braking) + Mathf.Min(1f, num5);
				num5 += 0.01f;
				float num7 = 0f;
				Vector3 vector6 = vehicleData.m_segment.b;
				Vector3 vector7 = vehicleData.m_segment.b - vehicleData.m_segment.a;
				int num8 = 0;
				for (int i = num8; i < 4; i++)
				{
					Vector3 vector8 = vehicleData.GetTargetPos(i);
					Vector3 vector9 = vector8 - vector6;
					if (Vector3.Dot(vector7, vector9) > 0f)
					{
						float magnitude = vector9.get_magnitude();
						Segment3 segment;
						segment..ctor(vector6, vector8);
						min = segment.Min();
						max = segment.Max();
						segment.a.y = segment.a.y * 0.5f;
						segment.b.y = segment.b.y * 0.5f;
						if (magnitude > 0.01f && min.x < vector2.x + 2f && min.y < vector2.y + 2f && min.z < vector2.z + 2f && vector.x < max.x + 2f && vector.y < max.y + 2f && vector.z < max.z + 2f)
						{
							Vector3 a = otherData.m_segment.a;
							a.y *= 0.5f;
							float num9;
							if (segment.DistanceSqr(a, ref num9) < 4f)
							{
								float num10 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
								float num11 = num7 + magnitude * num9;
								if (num11 >= 0.01f)
								{
									num11 -= num10 + 3f;
									float num12 = Mathf.Max(0f, FerryAI.CalculateMaxSpeed(num11, num10, maxBraking));
									if (num12 < 0.01f)
									{
										blocked = true;
									}
									Vector3 vector10 = Vector3.Normalize(otherData.m_targetPos0 - otherData.m_segment.a);
									float num13 = 1.2f - 1f / ((float)vehicleData.m_blockCounter * 0.02f + 0.5f);
									if (Vector3.Dot(vector9, vector10) > num13 * magnitude)
									{
										maxSpeed = Mathf.Min(maxSpeed, num12);
									}
								}
								break;
							}
							if (lodPhysics < 2)
							{
								float num14 = 0f;
								float num15 = num6;
								Vector3 vector11 = otherData.m_segment.b;
								Vector3 vector12 = otherData.m_segment.b - otherData.m_segment.a;
								int num16 = 0;
								bool flag = false;
								int num17 = num16;
								while (num17 < 4 && num15 > 0.1f)
								{
									Vector3 vector13;
									if (otherData.m_leadingVehicle != 0)
									{
										if (num17 != num16)
										{
											break;
										}
										vector13 = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)otherData.m_leadingVehicle].m_segment.b;
									}
									else
									{
										vector13 = otherData.GetTargetPos(num17);
									}
									Vector3 vector14 = Vector3.ClampMagnitude(vector13 - vector11, num15);
									if (Vector3.Dot(vector12, vector14) > 0f)
									{
										vector13 = vector11 + vector14;
										float magnitude2 = vector14.get_magnitude();
										num15 -= magnitude2;
										Segment3 segment2;
										segment2..ctor(vector11, vector13);
										segment2.a.y = segment2.a.y * 0.5f;
										segment2.b.y = segment2.b.y * 0.5f;
										if (magnitude2 > 0.01f)
										{
											float num19;
											float num20;
											float num18;
											if (otherID < vehicleID)
											{
												num18 = segment2.DistanceSqr(segment, ref num19, ref num20);
											}
											else
											{
												num18 = segment.DistanceSqr(segment2, ref num20, ref num19);
											}
											if (num18 < 4f)
											{
												float num21 = num7 + magnitude * num20;
												float num22 = num14 + magnitude2 * num19 + 0.1f;
												if (num21 >= 0.01f && num21 * num5 > num22 * num4)
												{
													float num23 = Vector3.Dot(lastFrameData.m_velocity, vector9) / magnitude;
													if (num21 >= 0.01f)
													{
														num21 -= num23 + 1f + otherData.Info.m_generatedInfo.m_size.z;
														float num24 = Mathf.Max(0f, FerryAI.CalculateMaxSpeed(num21, num23, maxBraking));
														if (num24 < 0.01f)
														{
															blocked = true;
														}
														maxSpeed = Mathf.Min(maxSpeed, num24);
													}
												}
												flag = true;
												break;
											}
										}
										vector12 = vector14;
										num14 += magnitude2;
										vector11 = vector13;
									}
									num17++;
								}
								if (flag)
								{
									break;
								}
							}
						}
						vector7 = vector9;
						num7 += magnitude;
						vector6 = vector8;
					}
				}
			}
		}
		return otherData.m_nextGridVehicle;
	}

	protected new void UpdatePathTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ref int index, int max, float minSqrDistanceA, float minSqrDistanceB)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = vehicleData.m_targetPos0;
		vector.w = 1000f;
		float num = minSqrDistanceA;
		uint num2 = vehicleData.m_path;
		byte b = vehicleData.m_pathPositionIndex;
		byte b2 = vehicleData.m_lastPathOffset;
		if (b == 255)
		{
			b = 0;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = 0;
			}
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)num2)].CalculatePathPositionOffset(b >> 1, vector, out b2))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
				return;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].GetPosition(b >> 1, out position))
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		NetInfo info = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes.Length <= (int)position.m_lane)
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		uint num3 = PathManager.GetLaneID(position);
		NetInfo.Lane lane = info.m_lanes[(int)position.m_lane];
		Bezier3 bezier;
		while (true)
		{
			if ((b & 1) == 0)
			{
				if (lane.m_laneType != NetInfo.LaneType.CargoVehicle)
				{
					bool flag = true;
					while (b2 != position.m_offset)
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							float num4 = Mathf.Sqrt(num) - VectorUtils.LengthXZ(vector - refPos);
							int num5;
							if (num4 < 0f)
							{
								num5 = 4;
							}
							else
							{
								num5 = 4 + Mathf.Max(0, Mathf.CeilToInt(num4 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_length + 1f)));
							}
							if (b2 > position.m_offset)
							{
								b2 = (byte)Mathf.Max((int)b2 - num5, (int)position.m_offset);
							}
							else if (b2 < position.m_offset)
							{
								b2 = (byte)Mathf.Min((int)b2 + num5, (int)position.m_offset);
							}
						}
						Vector3 vector2;
						Vector3 vector3;
						float num6;
						this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, b2, out vector2, out vector3, out num6);
						vector.Set(vector2.x, vector2.y, vector2.z, Mathf.Min(vector.w, num6));
						float num7 = VectorUtils.LengthSqrXZ(vector2 - refPos);
						if (num7 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
				b += 1;
				b2 = 0;
				if (index <= 0)
				{
					vehicleData.m_pathPositionIndex = b;
					vehicleData.m_lastPathOffset = b2;
				}
			}
			int num8 = (b >> 1) + 1;
			uint num9 = num2;
			if (num8 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_positionCount)
			{
				num8 = 0;
				num9 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_nextPathUnit;
				if (num9 == 0u)
				{
					goto Block_17;
				}
			}
			PathUnit.Position position2;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetPosition(num8, out position2))
			{
				goto Block_19;
			}
			NetInfo info2 = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
			if (info2.m_lanes.Length <= (int)position2.m_lane)
			{
				goto Block_20;
			}
			uint laneID = PathManager.GetLaneID(position2);
			NetInfo.Lane lane2 = info2.m_lanes[(int)position2.m_lane];
			ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			ushort endNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			ushort startNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
			ushort endNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
			if (startNode2 != startNode && startNode2 != endNode && endNode2 != startNode && endNode2 != endNode && ((instance2.m_nodes.m_buffer[(int)startNode].m_flags | instance2.m_nodes.m_buffer[(int)endNode].m_flags) & NetNode.Flags.Disabled) == NetNode.Flags.None && ((instance2.m_nodes.m_buffer[(int)startNode2].m_flags | instance2.m_nodes.m_buffer[(int)endNode2].m_flags) & NetNode.Flags.Disabled) != NetNode.Flags.None)
			{
				goto Block_26;
			}
			if (lane2.m_laneType == NetInfo.LaneType.Pedestrian)
			{
				goto Block_27;
			}
			if ((byte)(lane2.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) == 0)
			{
				goto Block_33;
			}
			if (lane2.m_vehicleType != this.m_info.m_vehicleType && this.NeedChangeVehicleType(vehicleID, ref vehicleData, position2, laneID, lane2.m_vehicleType, ref vector))
			{
				goto Block_35;
			}
			if (position2.m_segment != position.m_segment && vehicleID != 0)
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Leaving;
			}
			byte b3 = 0;
			if ((vehicleData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
			{
				b3 = ((position2.m_offset < 128) ? 255 : 0);
			}
			else if (num3 != laneID && lane.m_laneType != NetInfo.LaneType.CargoVehicle)
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
				bezier = default(Bezier3);
				Vector3 vector4;
				float num10;
				this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, position.m_offset, out bezier.a, out vector4, out num10);
				bool flag2 = b2 == 0;
				if (flag2)
				{
					if ((vehicleData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
					{
						flag2 = (vehicleData.m_trailingVehicle == 0);
					}
					else
					{
						flag2 = (vehicleData.m_leadingVehicle == 0);
					}
				}
				Vector3 vector5;
				float num11;
				if (flag2)
				{
					PathUnit.Position nextPosition;
					if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetNextPosition(num8, out nextPosition))
					{
						nextPosition = default(PathUnit.Position);
					}
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position2, laneID, b3, position, num3, position.m_offset, index, out bezier.d, out vector5, out num11);
				}
				else
				{
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position2, laneID, b3, out bezier.d, out vector5, out num11);
				}
				if (num11 < 0.01f || (instance2.m_segments.m_buffer[(int)position2.m_segment].m_flags & (NetSegment.Flags.Collapsed | NetSegment.Flags.Flooded)) != NetSegment.Flags.None)
				{
					goto IL_865;
				}
				if (position.m_offset == 0)
				{
					vector4 = -vector4;
				}
				if (b3 < position2.m_offset)
				{
					vector5 = -vector5;
				}
				vector4.Normalize();
				vector5.Normalize();
				float num12;
				NetSegment.CalculateMiddlePoints(bezier.a, vector4, bezier.d, vector5, true, true, out bezier.b, out bezier.c, out num12);
				if (num12 > 1f)
				{
					ushort num13;
					if (b3 == 0)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					}
					else if (b3 == 255)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
					}
					else
					{
						num13 = 0;
					}
					float num14 = 1.57079637f * (1f + Vector3.Dot(vector4, vector5));
					if (num12 > 1f)
					{
						num14 /= num12;
					}
					num11 = Mathf.Min(num11, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14));
					while (b2 < 255)
					{
						float num15 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
						int num16;
						if (num15 < 0f)
						{
							num16 = 8;
						}
						else
						{
							num16 = 8 + Mathf.Max(0, Mathf.CeilToInt(num15 * 256f / (num12 + 1f)));
						}
						b2 = (byte)Mathf.Min((int)b2 + num16, 255);
						Vector3 vector6 = bezier.Position((float)b2 * 0.003921569f);
						vector.Set(vector6.x, vector6.y, vector6.z, Mathf.Min(vector.w, num11));
						float num17 = VectorUtils.LengthSqrXZ(vector6 - refPos);
						if (num17 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							if (num13 != 0)
							{
								this.UpdateNodeTargetPos(vehicleID, ref vehicleData, num13, ref instance2.m_nodes.m_buffer[(int)num13], ref vector, index);
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
			}
			else
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
			}
			if (index <= 0)
			{
				if (num8 == 0)
				{
					Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
				}
				if (num8 >= (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_positionCount - 1) && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_nextPathUnit == 0u && vehicleID != 0)
				{
					this.ArrivingToDestination(vehicleID, ref vehicleData);
				}
			}
			num2 = num9;
			b = (byte)(num8 << 1);
			b2 = b3;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = b;
				vehicleData.m_lastPathOffset = b2;
				vehicleData.m_flags = ((vehicleData.m_flags & ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition)) | info2.m_setVehicleFlags);
				if (this.LeftHandDrive(lane2))
				{
					vehicleData.m_flags |= Vehicle.Flags.LeftHandDrive;
				}
				else
				{
					vehicleData.m_flags &= (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding);
				}
			}
			position = position2;
			num3 = laneID;
			lane = lane2;
		}
		return;
		Block_17:
		if (index <= 0)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		vector.w = 1f;
		vehicleData.SetTargetPos(index++, vector);
		return;
		Block_19:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_20:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_26:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_27:
		if (vehicleID != 0 && (vehicleData.m_flags & Vehicle.Flags.Parking) == (Vehicle.Flags)0)
		{
			byte offset = position.m_offset;
			byte offset2 = position.m_offset;
			int num8;
			uint num9;
			if (this.ParkVehicle(vehicleID, ref vehicleData, position, num9, num8 << 1, out offset2))
			{
				if (offset2 != offset)
				{
					if (index <= 0)
					{
						vehicleData.m_pathPositionIndex = (byte)((int)vehicleData.m_pathPositionIndex & -2);
						vehicleData.m_lastPathOffset = offset;
					}
					position.m_offset = offset2;
					instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].SetPosition(b >> 1, position);
				}
				vehicleData.m_flags |= Vehicle.Flags.Parking;
			}
			else
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		return;
		Block_33:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_35:
		float num18 = VectorUtils.LengthSqrXZ(vector - refPos);
		if (num18 >= num)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
		if (index <= 0)
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
			uint num9;
			if (num9 != vehicleData.m_path)
			{
				Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
			}
			int num8;
			vehicleData.m_pathPositionIndex = (byte)(num8 << 1);
			uint laneID;
			PathUnit.CalculatePathPositionOffset(laneID, vector, out vehicleData.m_lastPathOffset);
			PathUnit.Position position2;
			if (vehicleID != 0 && !this.ChangeVehicleType(vehicleID, ref vehicleData, position2, laneID))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		else
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
		}
		return;
		IL_865:
		if (index <= 0)
		{
			vehicleData.m_lastPathOffset = b2;
		}
		vector = bezier.a;
		vector.w = 0f;
		while (index < max)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 10f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((vector.z - 10f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 10f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((vector2.z + 10f) / 320f + 27f), 53);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = FerryAI.CheckOverlap(segment, ignoreVehicle, maxVelocity, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, float maxVelocity, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 4f && otherData.GetLastFrameData().m_velocity.get_sqrMagnitude() < maxVelocity * maxVelocity)
		{
			overlap = true;
		}
		return otherData.m_nextGridVehicle;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override void InvalidPath(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData)
	{
		vehicleData.m_targetPos0 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos3.w = 0f;
		base.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
		Vector3 position2 = lastFrameData.m_position;
		Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].CalculatePosition((float)prevOffset * 0.003921569f);
		float num = 0.5f * lastFrameData.m_velocity.get_sqrMagnitude() / this.m_info.m_braking + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		if (VectorUtils.LengthXZ(position2 - vector) >= num - 5f && !instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
		{
			ushort startNode = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)prevPos.m_segment].m_endNode;
			uint lane = instance.m_nodes.m_buffer[(int)startNode].m_lane;
			uint lane2 = instance.m_nodes.m_buffer[(int)endNode].m_lane;
			if (lane != laneID || lane2 != laneID)
			{
				maxSpeed = 0f;
				return;
			}
		}
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true, false);
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		bool flag = PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out startPosA, out startPosB, out num, out num2);
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		bool flag2 = PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out endPosA, out endPosB, out num3, out num4);
		PathUnit.Position position;
		PathUnit.Position position2;
		float num5;
		float num6;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Beautification, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out position, out position2, out num5, out num6) && (!flag || num5 < num))
		{
			startPosA = position;
			startPosB = position2;
			num = num5;
			num2 = num6;
			flag = true;
		}
		PathUnit.Position position3;
		PathUnit.Position position4;
		float num7;
		float num8;
		if (PathManager.FindPathPosition(endPos, ItemClass.Service.Beautification, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out position3, out position4, out num7, out num8) && (!flag2 || num7 < num3))
		{
			endPosA = position3;
			endPosB = position4;
			num3 = num7;
			num4 = num8;
			flag2 = true;
		}
		if (flag && flag2)
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, false, this.IgnoreBlocked(vehicleID, ref vehicleData), false, false))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (FerryAI.CheckOverlap(vehicleData.m_segment, 0, 1000f))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	[NonSerialized]
	private RenderGroup.MeshData m_underwaterMeshData;
}
