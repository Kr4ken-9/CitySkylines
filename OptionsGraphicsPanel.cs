﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.UI;
using UnityEngine;

public class OptionsGraphicsPanel : UICustomControl
{
	public float tiltShiftAmount
	{
		get
		{
			return this.m_TiltShiftAmount;
		}
		set
		{
			this.m_TiltShiftAmount.set_value(value);
		}
	}

	public float filmGrainAmount
	{
		get
		{
			return this.m_FilmGrainAmount;
		}
		set
		{
			this.m_FilmGrainAmount.set_value(value);
		}
	}

	private void Awake()
	{
		this.m_AspectRatioDropdown = base.Find<UIDropDown>("Aspects");
		this.m_AspectRatioDropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnChangeAspectRatio));
		this.m_ResolutionDropdown = base.Find<UIDropDown>("Resolutions");
		this.m_FullscreenDropdown = base.Find<UIDropDown>("Fullscreens");
		this.m_ColorCorrectionDropdown = base.Find<UIDropDown>("ColorCorrection");
		this.m_ColorCorrectionDropdown.set_localeID("BUILTIN_COLORCORRECTION");
		this.m_ColorCorrectionDropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnChangedColorCorrection));
		this.m_VSyncDropdown = base.Find<UIDropDown>("VSync");
		this.m_VSyncDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_VSYNC_OFF",
			"OPTIONS_VSYNC_VBLANK",
			"OPTIONS_VSYNC_SECONDVBLANK"
		});
		this.m_VSyncDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_VSync.set_value(i);
			this.UpdateVSync();
		});
		this.CacheScreenResolution();
		this.InitAspectRatios();
		this.InitResolutions();
		this.InitDisplayModes();
		this.RefreshColorCorrectionLUTs();
		this.m_DOFTypeDropdown = base.Find<UIDropDown>("DOFType");
		this.m_DOFTypeDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_DOF_DISABLED",
			"OPTIONS_DOF_TILTSHIFT",
			"OPTIONS_DOF_NORMAL",
			"OPTIONS_DOF_HIGH"
		});
		this.m_DOFTypeDropdown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnChangedDOF));
		this.m_DOFTypeDropdown.set_selectedIndex(this.m_DOFMode.get_value());
		this.InitializeQualityOptions();
		this.UpdateTextureQuality();
		this.UpdateShadowsQuality();
		this.UpdateTextureAniso();
		this.UpdateLevelOfDetail();
		this.UpdateVSync();
		this.RefreshQualityOptions();
	}

	private void OnEnable()
	{
		PackageManager.add_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.RefreshColorCorrectionLUTs));
		this.OnLocaleChanged();
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		PackageManager.remove_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.RefreshColorCorrectionLUTs));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.RefreshColorCorrectionLUTs();
		float width = this.m_VSyncDropdown.CalculatePopupWidth(this.m_VSyncDropdown.get_listHeight());
		this.m_VSyncDropdown.set_width(width);
	}

	private void InitializeQualityOptions()
	{
		this.m_AntialiasingDropdown = base.Find<UIDropDown>("AntiAliasing");
		this.m_AntialiasingDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_DISABLED",
			"OPTIONS_ENABLED"
		});
		this.m_AntialiasingDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_Antialiasing.set_value(i);
		});
		this.m_TexturesQualityDropdown = base.Find<UIDropDown>("TexturesQuality");
		this.m_TexturesQualityDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_LOW",
			"OPTIONS_MEDIUM",
			"OPTIONS_HIGH"
		});
		this.m_TexturesQualityDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_TexturesQuality.set_value(i);
			this.UpdateTextureQuality();
		});
		this.m_TexturesAnisotropicDropdown = base.Find<UIDropDown>("TexturesAniso");
		this.m_TexturesAnisotropicDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_DISABLED",
			"OPTIONS_ENABLED"
		});
		this.m_TexturesAnisotropicDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_AnisotropicFiltering.set_value(i);
			this.UpdateTextureAniso();
		});
		this.m_ShadowsQualityDropdown = base.Find<UIDropDown>("ShadowsQuality");
		this.m_ShadowsQualityDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_DISABLED",
			"OPTIONS_LOW",
			"OPTIONS_MEDIUM",
			"OPTIONS_HIGH"
		});
		this.m_ShadowsQualityDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_ShadowsQuality.set_value(i);
			this.UpdateShadowsQuality();
		});
		this.m_ShadowsDistanceDropdown = base.Find<UIDropDown>("ShadowsDistance");
		this.m_ShadowsDistanceDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_VERYSHORT",
			"OPTIONS_SHORT",
			"OPTIONS_FAR",
			"OPTIONS_VERYFAR"
		});
		this.m_ShadowsDistanceDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_ShadowsDistance.set_value(i);
		});
		this.m_LevelOfDetailDropdown = base.Find<UIDropDown>("LevelOfDetail");
		this.m_LevelOfDetailDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_LOW",
			"OPTIONS_MEDIUM",
			"OPTIONS_HIGH",
			"OPTIONS_VERYHIGH"
		});
		this.m_LevelOfDetailDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_LevelOfDetail.set_value(i);
			this.UpdateLevelOfDetail();
		});
		this.m_VSyncDropdown = base.Find<UIDropDown>("VSync");
		this.m_VSyncDropdown.set_localizedItems(new string[]
		{
			"OPTIONS_VSYNC_OFF",
			"OPTIONS_VSYNC_VBLANK",
			"OPTIONS_VSYNC_SECONDVBLANK"
		});
		this.m_VSyncDropdown.add_eventSelectedIndexChanged(delegate(UIComponent c, int i)
		{
			this.m_VSync.set_value(i);
			this.UpdateVSync();
		});
	}

	private void RefreshQualityOptions()
	{
		this.m_AntialiasingDropdown.set_selectedIndex(this.m_Antialiasing.get_value());
		this.m_TexturesQualityDropdown.set_selectedIndex(this.m_TexturesQuality.get_value());
		this.m_ShadowsQualityDropdown.set_selectedIndex(this.m_ShadowsQuality.get_value());
		this.m_ShadowsDistanceDropdown.set_selectedIndex(this.m_ShadowsDistance.get_value());
		this.m_TexturesAnisotropicDropdown.set_selectedIndex(this.m_AnisotropicFiltering.get_value());
		this.m_LevelOfDetailDropdown.set_selectedIndex(this.m_LevelOfDetail.get_value());
		this.m_VSyncDropdown.set_selectedIndex(this.m_VSync.get_value());
	}

	private void UpdateTextureQuality()
	{
		int value = this.m_TexturesQuality.get_value();
		if (value == 0)
		{
			QualitySettings.set_masterTextureLimit(2);
		}
		else if (value == 1)
		{
			QualitySettings.set_masterTextureLimit(1);
		}
		else if (value == 2)
		{
			QualitySettings.set_masterTextureLimit(0);
		}
	}

	private void UpdateShadowsQuality()
	{
		int value = this.m_ShadowsQuality.get_value();
		if (value == 0)
		{
			QualitySettings.set_shadowCascades(0);
		}
		else if (value == 1)
		{
			QualitySettings.set_shadowCascades(1);
		}
		else if (value == 2)
		{
			QualitySettings.set_shadowCascades(2);
		}
		else if (value == 3)
		{
			QualitySettings.set_shadowCascades(4);
		}
	}

	private void UpdateTextureAniso()
	{
		int value = this.m_AnisotropicFiltering.get_value();
		if (value == 0)
		{
			QualitySettings.set_anisotropicFiltering(0);
		}
		else if (value == 1)
		{
			QualitySettings.set_anisotropicFiltering(1);
		}
	}

	private void UpdateLevelOfDetail()
	{
		RenderManager.LevelOfDetail = this.m_LevelOfDetail.get_value();
	}

	private void CacheScreenResolution()
	{
		this.m_CachedScreenWidth = Screen.get_width();
		this.m_CachedScreenHeight = Screen.get_height();
		this.m_CachedFullscreen = Screen.get_fullScreen();
	}

	private void OnChangeAspectRatio(UIComponent comp, int sel)
	{
		this.InitResolutions();
	}

	private void OnScreenResolutionChanged()
	{
		this.InitAspectRatios();
		this.InitResolutions();
		this.InitDisplayModes();
		this.CacheScreenResolution();
	}

	private void OnChangedColorCorrection(UIComponent comp, int sel)
	{
		SingletonResource<ColorCorrectionManager>.get_instance().currentSelection = sel;
	}

	private void OnChangedDOF(UIComponent comp, int sel)
	{
		this.m_DOFMode.set_value(sel);
	}

	public void OnApplyGraphics()
	{
		base.StartCoroutine(this.OnApplyGraphicsCoroutine());
	}

	[DebuggerHidden]
	public IEnumerator OnApplyGraphicsCoroutine()
	{
		OptionsGraphicsPanel.<OnApplyGraphicsCoroutine>c__Iterator0 <OnApplyGraphicsCoroutine>c__Iterator = new OptionsGraphicsPanel.<OnApplyGraphicsCoroutine>c__Iterator0();
		<OnApplyGraphicsCoroutine>c__Iterator.$this = this;
		return <OnApplyGraphicsCoroutine>c__Iterator;
	}

	private void SetSavedResolutionSettings(int width, int height, bool fullscreen)
	{
		this.m_ScreenWidth.set_value(width);
		this.m_ScreenHeight.set_value(height);
		this.m_Fullscreen.set_value(fullscreen);
		PlayerPrefs.SetInt(OptionsGraphicsPanel.kIsFullScreen, (!fullscreen) ? 0 : 1);
		PlayerPrefs.SetInt(OptionsGraphicsPanel.kResolutionWidth, width);
		PlayerPrefs.SetInt(OptionsGraphicsPanel.kResolutionHeight, height);
	}

	private int FindCurrentAspectRatio()
	{
		float num = (float)Screen.get_width() / (float)Screen.get_height();
		float num2 = num;
		for (int i = 0; i < this.m_SupportedAspectRatios.Count; i++)
		{
			if (Mathf.Abs(this.m_SupportedAspectRatios[i] - num2) < OptionsGraphicsPanel.RESOLUTION_TRESHOLD)
			{
				return i;
			}
		}
		return 0;
	}

	private int FindCurrentResolution()
	{
		for (int i = 0; i < this.m_SupportedResolutions.Count; i++)
		{
			Resolution resolution = this.m_SupportedResolutions[i];
			if (resolution.get_width() == Screen.get_width() && resolution.get_height() == Screen.get_height())
			{
				return i;
			}
		}
		int result = 0;
		int num = 2147483647;
		for (int j = 0; j < this.m_SupportedResolutions.Count; j++)
		{
			Resolution resolution2 = this.m_SupportedResolutions[j];
			int num2 = Math.Abs(Screen.get_width() - resolution2.get_width());
			int num3 = Math.Abs(Screen.get_height() - resolution2.get_height());
			int num4 = num2 * Screen.get_height() + num3 * Screen.get_width() - num2 * num3;
			if (num4 < num)
			{
				result = j;
				num = num4;
			}
		}
		return result;
	}

	private bool MatchAspectRatio(int width, int height, float aspect)
	{
		float num = (float)width / (float)height;
		return Mathf.Abs(num - aspect) < OptionsGraphicsPanel.RESOLUTION_TRESHOLD;
	}

	private void InitAspectRatios()
	{
		this.m_SupportedAspectRatios.Clear();
		Resolution[] resolutions = Screen.get_resolutions();
		List<string> list = new List<string>();
		for (int i = 0; i < OptionsGraphicsPanel.kAvailableAspectRatios.Length; i++)
		{
			for (int j = 0; j < resolutions.Length; j++)
			{
				Resolution resolution = resolutions[j];
				if (this.MatchAspectRatio(resolution.get_width(), resolution.get_height(), OptionsGraphicsPanel.kAvailableAspectRatios[i]))
				{
					this.m_SupportedAspectRatios.Add(OptionsGraphicsPanel.kAvailableAspectRatios[i]);
					list.Add(OptionsGraphicsPanel.kAvailableAspectRatioNames[i]);
					break;
				}
			}
		}
		if (this.m_SupportedAspectRatios.Count == 0)
		{
			this.m_SupportedAspectRatios.Add(OptionsGraphicsPanel.kAvailableAspectRatios[0]);
			list.Add(OptionsGraphicsPanel.kAvailableAspectRatioNames[0]);
		}
		this.m_AspectRatioDropdown.set_localizedItems(list.ToArray());
		this.m_AspectRatioDropdown.set_selectedIndex(this.FindCurrentAspectRatio());
	}

	private void InitResolutions()
	{
		this.m_SupportedResolutionNames.Clear();
		this.m_SupportedResolutions.Clear();
		Resolution[] resolutions = Screen.get_resolutions();
		for (int i = 0; i < resolutions.Length; i++)
		{
			Resolution item = resolutions[i];
			if (this.MatchAspectRatio(item.get_width(), item.get_height(), this.m_SupportedAspectRatios[this.m_AspectRatioDropdown.get_selectedIndex()]))
			{
				this.m_SupportedResolutions.Add(item);
				this.m_SupportedResolutionNames.Add(item.get_width() + "x" + item.get_height());
			}
		}
		this.m_ResolutionDropdown.set_items(this.m_SupportedResolutionNames.ToArray());
		this.m_ResolutionDropdown.set_selectedIndex(this.FindCurrentResolution());
	}

	private void InitDisplayModes()
	{
		this.m_FullscreenDropdown.set_localizedItems(OptionsGraphicsPanel.kAvailableDisplayModeNames);
		this.m_FullscreenDropdown.set_selectedIndex(Array.IndexOf<bool>(OptionsGraphicsPanel.kAvailableDisplayModes, Screen.get_fullScreen()));
	}

	private void RefreshColorCorrectionLUTs()
	{
		this.m_ColorCorrectionDropdown.set_localizedItems(SingletonResource<ColorCorrectionManager>.get_instance().items);
		this.m_ColorCorrectionDropdown.set_selectedIndex(SingletonResource<ColorCorrectionManager>.get_instance().lastSelection);
	}

	private void UpdateVSync()
	{
		QualitySettings.set_vSyncCount(this.m_VSync.get_value());
	}

	private void Update()
	{
		if (!Application.get_isEditor() && ((this.m_CachedScreenWidth != Screen.get_width() && this.m_CachedScreenHeight != Screen.get_height()) || Screen.get_fullScreen() != this.m_CachedFullscreen))
		{
			this.OnScreenResolutionChanged();
		}
	}

	public static readonly string kResolutionWidth = "Screenmanager Resolution Width";

	public static readonly string kResolutionHeight = "Screenmanager Resolution Height";

	public static readonly string kIsFullScreen = "Screenmanager Is Fullscreen mode";

	private static readonly float[] kAvailableAspectRatios = new float[]
	{
		1.33333337f,
		1.77777779f,
		1.6f,
		2.33333325f
	};

	private static readonly string[] kAvailableAspectRatioNames = new string[]
	{
		"ASPECTRATIO_NORMAL",
		"ASPECTRATIO_WIDESCREEN",
		"ASPECTRATIO_WIDESCREEN2",
		"ASPECTRATIO_WIDESCREEN3"
	};

	private static readonly bool[] kAvailableDisplayModes = new bool[]
	{
		default(bool),
		true
	};

	private static readonly string[] kAvailableDisplayModeNames = new string[]
	{
		"DISPLAYMODE_WINDOWED",
		"DISPLAYMODE_FULLSCREEN"
	};

	private static readonly float RESOLUTION_TRESHOLD = 0.06f;

	private List<float> m_SupportedAspectRatios = new List<float>();

	private List<Resolution> m_SupportedResolutions = new List<Resolution>();

	private List<string> m_SupportedResolutionNames = new List<string>();

	private UIDropDown m_ResolutionDropdown;

	private UIDropDown m_AspectRatioDropdown;

	private UIDropDown m_FullscreenDropdown;

	private UIDropDown m_VSyncDropdown;

	private UIDropDown m_ColorCorrectionDropdown;

	private SavedInt m_ScreenWidth = new SavedInt(Settings.screenWidth, Settings.gameSettingsFile);

	private SavedInt m_ScreenHeight = new SavedInt(Settings.screenHeight, Settings.gameSettingsFile);

	private SavedBool m_Fullscreen = new SavedBool(Settings.fullscreen, Settings.gameSettingsFile);

	private SavedInt m_VSync = new SavedInt(Settings.vsync, Settings.gameSettingsFile, DefaultSettings.vsync, true);

	private SavedFloat m_TiltShiftAmount = new SavedFloat(Settings.tiltShiftAmount, Settings.gameSettingsFile, DefaultSettings.tiltShiftAmount, true);

	private SavedFloat m_FilmGrainAmount = new SavedFloat(Settings.filmGrainAmount, Settings.gameSettingsFile, DefaultSettings.filmGrainAmount, true);

	private int m_CachedScreenWidth;

	private int m_CachedScreenHeight;

	private bool m_CachedFullscreen;

	private SavedInt m_ShadowsQuality = new SavedInt(Settings.shadowsQuality, Settings.gameSettingsFile, DefaultSettings.shadowsQuality, true);

	private SavedInt m_ShadowsDistance = new SavedInt(Settings.shadowsDistance, Settings.gameSettingsFile, DefaultSettings.shadowsDistance, true);

	private SavedInt m_TexturesQuality = new SavedInt(Settings.texturesQuality, Settings.gameSettingsFile, DefaultSettings.texturesQuality, true);

	private SavedInt m_Antialiasing = new SavedInt(Settings.antialiasing, Settings.gameSettingsFile, DefaultSettings.antialiasing, true);

	private SavedInt m_AnisotropicFiltering = new SavedInt(Settings.anisotropicFiltering, Settings.gameSettingsFile, DefaultSettings.anisotropicFiltering, true);

	private SavedInt m_LevelOfDetail = new SavedInt(Settings.levelOfDetail, Settings.gameSettingsFile, DefaultSettings.levelOfDetail, true);

	private SavedInt m_DOFMode = new SavedInt(Settings.dofMode, Settings.gameSettingsFile, DefaultSettings.dofMode, true);

	private UIDropDown m_ShadowsQualityDropdown;

	private UIDropDown m_TexturesQualityDropdown;

	private UIDropDown m_TexturesAnisotropicDropdown;

	private UIDropDown m_AntialiasingDropdown;

	private UIDropDown m_ShadowsDistanceDropdown;

	private UIDropDown m_LevelOfDetailDropdown;

	private UIDropDown m_DOFTypeDropdown;
}
