﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using UnityEngine;
using UnityEngine.Rendering;

public class LightSystem : MonoBehaviour
{
	public float DayLightIntensity
	{
		get
		{
			return this.m_DayLightIntensity;
		}
	}

	private void Awake()
	{
		this.m_tempLights = new Dictionary<InstanceID, LightSource>();
		this.m_lightEvents = new FastList<LightData>();
		this.m_lightLayer = LayerMask.NameToLayer("Lights");
		this.m_lightLayerFloating = LayerMask.NameToLayer("LightsFloating");
		this.m_lightBuffer = new CommandBuffer();
		this.m_lightBuffer.set_name("Batched lights");
		this.ID_DayLightIntensity = Shader.PropertyToID("_GiantMarshmallow");
		this.ID_LightPos = Shader.PropertyToID("_LightPos");
		this.ID_LightDir = Shader.PropertyToID("_LightDir");
		this.ID_LightVel = Shader.PropertyToID("_LightVel");
		this.ID_LightColor = Shader.PropertyToID("_LightColor");
		this.m_LightPos = new Vector4[32];
		this.m_LightDir = new Vector4[32];
		this.m_LightVel = new Vector4[32];
		this.m_LightColor = new Vector4[32];
		this.m_materialPropertyBlock = new MaterialPropertyBlock();
		this.CreateLightMesh();
	}

	private void OnDestroy()
	{
		if (this.m_lightMesh != null)
		{
			Object.Destroy(this.m_lightMesh);
			this.m_lightMesh = null;
		}
		if (this.m_lightMaterial != null)
		{
			Object.Destroy(this.m_lightMaterial);
			this.m_lightMaterial = null;
		}
		if (this.m_lightMaterialVolume != null)
		{
			Object.Destroy(this.m_lightMaterialVolume);
			this.m_lightMaterialVolume = null;
		}
	}

	public void InitializeProperties(RenderProperties properties, Camera mainCamera)
	{
		if (mainCamera != null)
		{
			mainCamera.AddCommandBuffer(7, this.m_lightBuffer);
		}
		if (properties.m_lightShader != null)
		{
			this.m_lightMaterial = new Material(properties.m_lightShader);
			this.m_lightMaterial.EnableKeyword("MULTI_INSTANCE");
		}
		if (properties.m_lightVolumeShader != null)
		{
			this.m_lightMaterialVolume = new Material(properties.m_lightVolumeShader);
			this.m_lightMaterialVolume.EnableKeyword("MULTI_INSTANCE");
			this.m_lightMaterialVolumeGroup = new Material(properties.m_lightVolumeShader);
		}
		if (properties.m_lightFloatingVolumeShader != null)
		{
			this.m_lightFloatingMaterialVolumeGroup = new Material(properties.m_lightFloatingVolumeShader);
		}
	}

	public void DestroyProperties(RenderProperties properties, Camera mainCamera)
	{
		if (mainCamera != null)
		{
			mainCamera.RemoveCommandBuffer(7, this.m_lightBuffer);
		}
		if (this.m_lightMaterial != null)
		{
			Object.Destroy(this.m_lightMaterial);
			this.m_lightMaterial = null;
		}
		if (this.m_lightMaterialVolume != null)
		{
			Object.Destroy(this.m_lightMaterialVolume);
			this.m_lightMaterialVolume = null;
		}
		if (this.m_lightMaterialVolumeGroup != null)
		{
			Object.Destroy(this.m_lightMaterialVolumeGroup);
			this.m_lightMaterialVolumeGroup = null;
		}
		if (this.m_lightFloatingMaterialVolumeGroup != null)
		{
			Object.Destroy(this.m_lightFloatingMaterialVolumeGroup);
			this.m_lightFloatingMaterialVolumeGroup = null;
		}
	}

	public void EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		while (!Monitor.TryEnter(this.m_lightEvents, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = 0; i < this.m_lightEvents.m_size; i++)
			{
				LightData data = this.m_lightEvents.m_buffer[i];
				if (cameraInfo.Intersect(data.m_position, data.m_range))
				{
					this.DrawLight(data);
				}
			}
			this.m_lightEvents.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_lightEvents);
		}
		if (this.m_drawLightIndex != 0)
		{
			RenderManager instance = Singleton<RenderManager>.get_instance();
			RenderManager expr_A0_cp_0 = instance;
			expr_A0_cp_0.m_drawCallData.m_lodCalls = expr_A0_cp_0.m_drawCallData.m_lodCalls + 1;
			RenderManager expr_B3_cp_0 = instance;
			expr_B3_cp_0.m_drawCallData.m_batchedCalls = expr_B3_cp_0.m_drawCallData.m_batchedCalls + (this.m_drawLightIndex - 1);
			while (this.m_drawLightIndex < 32)
			{
				this.m_LightPos[this.m_drawLightIndex] = Vector4.get_zero();
				this.m_LightDir[this.m_drawLightIndex] = Vector4.get_zero();
				this.m_LightVel[this.m_drawLightIndex] = Vector4.get_zero();
				this.m_LightColor[this.m_drawLightIndex] = Color.get_black();
				this.m_drawLightIndex++;
			}
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightPos, this.m_LightPos);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightDir, this.m_LightDir);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightVel, this.m_LightVel);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightColor, this.m_LightColor);
			this.m_lightBuffer.DrawMesh(this.m_lightMesh, Matrix4x4.get_identity(), this.m_lightMaterial, 0, 0, this.m_materialPropertyBlock);
			Graphics.DrawMesh(this.m_lightMesh, Matrix4x4.get_identity(), this.m_lightMaterialVolume, this.m_lightLayer, null, 0, this.m_materialPropertyBlock, false, false);
			this.m_materialPropertyBlock.Clear();
			this.m_drawLightIndex = 0;
		}
	}

	public void SetDaylightIntensity(float intensity)
	{
		this.m_DayLightIntensity = intensity;
		Shader.SetGlobalFloat(this.ID_DayLightIntensity, intensity);
	}

	public void DrawLight(LightType type, Vector3 pos, Vector3 dir, Vector3 vel, Color color, float intensity, float range, float spotAngle, float spotLeaking, bool volume)
	{
		Vector4 vector;
		vector.x = pos.x;
		vector.y = pos.y;
		vector.z = pos.z;
		vector.w = ((!volume) ? (-range) : range);
		Vector4 vector2;
		vector2.x = dir.x;
		vector2.y = dir.y;
		vector2.z = dir.z;
		vector2.w = 100000f;
		Vector4 vector3;
		vector3.x = vel.x;
		vector3.y = vel.y;
		vector3.z = vel.z;
		vector3.w = 0f;
		if (type == null)
		{
			vector2.w = -Mathf.Cos(0.0174532924f * spotAngle * 0.5f);
		}
		color = color.get_linear();
		color.r *= intensity;
		color.g *= intensity;
		color.b *= intensity;
		color.a = spotLeaking;
		this.m_LightPos[this.m_drawLightIndex] = vector;
		this.m_LightDir[this.m_drawLightIndex] = vector2;
		this.m_LightVel[this.m_drawLightIndex] = vector3;
		this.m_LightColor[this.m_drawLightIndex] = color;
		this.m_drawLightIndex++;
		if (this.m_drawLightIndex == 32)
		{
			RenderManager instance = Singleton<RenderManager>.get_instance();
			RenderManager expr_19A_cp_0 = instance;
			expr_19A_cp_0.m_drawCallData.m_lodCalls = expr_19A_cp_0.m_drawCallData.m_lodCalls + 1;
			RenderManager expr_1AD_cp_0 = instance;
			expr_1AD_cp_0.m_drawCallData.m_batchedCalls = expr_1AD_cp_0.m_drawCallData.m_batchedCalls + 31;
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightPos, this.m_LightPos);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightDir, this.m_LightDir);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightVel, this.m_LightVel);
			this.m_materialPropertyBlock.SetVectorArray(this.ID_LightColor, this.m_LightColor);
			this.m_lightBuffer.DrawMesh(this.m_lightMesh, Matrix4x4.get_identity(), this.m_lightMaterial, 0, 0, this.m_materialPropertyBlock);
			Graphics.DrawMesh(this.m_lightMesh, Matrix4x4.get_identity(), this.m_lightMaterialVolume, this.m_lightLayer, null, 0, this.m_materialPropertyBlock, false, false);
			this.m_materialPropertyBlock.Clear();
			this.m_drawLightIndex = 0;
		}
	}

	public void DrawLight(LightData data)
	{
		LightSource lightSource;
		if (!this.m_tempLights.TryGetValue(data.m_id, out lightSource))
		{
			lightSource = this.ObtainLightSource();
			lightSource.set_enabled(true);
			this.m_tempLights.Add(data.m_id, lightSource);
		}
		lightSource.m_transform.set_position(data.m_position);
		lightSource.m_transform.set_rotation(data.m_rotation);
		lightSource.m_light.set_type(data.m_type);
		lightSource.m_light.set_color(data.m_color);
		lightSource.m_light.set_intensity(data.m_intensity);
		lightSource.m_light.set_range(data.m_range);
		lightSource.m_light.set_spotAngle(data.m_spotAngle);
		lightSource.m_data = data;
		lightSource.m_used = 1;
	}

	public void KeepLight(InstanceID id)
	{
		LightSource lightSource;
		if (this.m_tempLights.TryGetValue(id, out lightSource))
		{
			lightSource.m_used = 1;
		}
	}

	public LightSource ObtainLightSource()
	{
		if (this.m_lightPool != null)
		{
			LightSource lightPool = this.m_lightPool;
			this.m_lightPool = lightPool.m_nextSource;
			lightPool.m_nextSource = null;
			lightPool.set_enabled(false);
			lightPool.m_data = default(LightData);
			lightPool.m_used = 0;
			lightPool.get_gameObject().SetActive(true);
			this.m_freeLightSources--;
			return lightPool;
		}
		if (this.m_lightPoolRoot == null)
		{
			this.m_lightPoolRoot = LightSystem.GetLightPoolRoot();
		}
		GameObject gameObject = new GameObject("Light Source");
		gameObject.get_transform().set_parent(this.m_lightPoolRoot);
		LightSource lightSource = gameObject.AddComponent<LightSource>();
		lightSource.m_light = gameObject.AddComponent<Light>();
		lightSource.m_transform = gameObject.get_transform();
		lightSource.set_enabled(false);
		this.m_lightSourcesCreated++;
		return lightSource;
	}

	public void ReleaseLightSource(LightSource source)
	{
		source.get_gameObject().SetActive(false);
		source.m_nextSource = this.m_lightPool;
		this.m_lightPool = source;
		this.m_freeLightSources++;
	}

	private static Transform GetLightPoolRoot()
	{
		GameObject gameObject = GameObject.Find("Light Pool");
		if (gameObject == null)
		{
			gameObject = new GameObject("Light Pool");
			Object.DontDestroyOnLoad(gameObject);
		}
		return gameObject.get_transform();
	}

	private void CreateLightMesh()
	{
		Vector3[] array = new Vector3[192];
		int[] array2 = new int[384];
		int num = 0;
		int num2 = 0;
		float num3 = 1f / Mathf.Cos(0.5235988f);
		float num4 = num3 * 0.5f;
		for (int i = 0; i < 32; i++)
		{
			float num5 = (float)i;
			array[num] = new Vector3(-num4, 1f, num5);
			num++;
			array[num] = new Vector3(num4, 1f, num5);
			num++;
			array[num] = new Vector3(num3, 0f, num5);
			num++;
			array[num] = new Vector3(num4, -1f, num5);
			num++;
			array[num] = new Vector3(-num4, -1f, num5);
			num++;
			array[num] = new Vector3(-num3, 0f, num5);
			num++;
			array2[num2++] = num - 6;
			array2[num2++] = num - 5;
			array2[num2++] = num - 1;
			array2[num2++] = num - 1;
			array2[num2++] = num - 5;
			array2[num2++] = num - 4;
			array2[num2++] = num - 1;
			array2[num2++] = num - 4;
			array2[num2++] = num - 2;
			array2[num2++] = num - 2;
			array2[num2++] = num - 4;
			array2[num2++] = num - 3;
		}
		this.m_lightMesh = new Mesh();
		this.m_lightMesh.set_vertices(array);
		this.m_lightMesh.set_triangles(array2);
		this.m_lightMesh.set_bounds(new Bounds(Vector3.get_zero(), new Vector3(100000f, 100000f, 100000f)));
	}

	public void AddEvent(LightData data)
	{
		while (!Monitor.TryEnter(this.m_lightEvents, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_lightEvents.Add(data);
		}
		finally
		{
			Monitor.Exit(this.m_lightEvents);
		}
	}

	[NonSerialized]
	public Transform m_lightPoolRoot;

	[NonSerialized]
	public LightSource m_lightPool;

	[NonSerialized]
	public Dictionary<InstanceID, LightSource> m_tempLights;

	[NonSerialized]
	public int m_lightSourcesCreated;

	[NonSerialized]
	public int m_freeLightSources;

	[NonSerialized]
	public Mesh m_lightMesh;

	[NonSerialized]
	public Material m_lightMaterial;

	[NonSerialized]
	public Material m_lightMaterialVolume;

	[NonSerialized]
	public Material m_lightMaterialVolumeGroup;

	[NonSerialized]
	public Material m_lightFloatingMaterialVolumeGroup;

	[NonSerialized]
	public int m_lightLayer;

	[NonSerialized]
	public int m_lightLayerFloating;

	[NonSerialized]
	public CommandBuffer m_lightBuffer;

	private FastList<LightData> m_lightEvents;

	private int ID_LightPos;

	private int ID_LightDir;

	private int ID_LightVel;

	private int ID_LightColor;

	private int m_drawLightIndex;

	private MaterialPropertyBlock m_materialPropertyBlock;

	private Vector4[] m_LightPos;

	private Vector4[] m_LightDir;

	private Vector4[] m_LightVel;

	private Vector4[] m_LightColor;

	private int ID_DayLightIntensity;

	private float m_DayLightIntensity;
}
