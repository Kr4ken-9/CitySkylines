﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class BlimpAI : VehicleAI
{
	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Quaternion quaternion = Quaternion.Euler(swayPosition.z * 57.29578f, 0f, swayPosition.x * -57.29578f);
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation * quaternion, scale);
		return result;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		if ((flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			return Matrix4x4.get_identity();
		}
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, rotation, scale);
		return bodyMatrix.get_inverse() * matrix4x;
	}

	public override float GetAudioAcceleration(ref Vehicle.Frame frame1, ref Vehicle.Frame frame2, float t)
	{
		return Mathf.Lerp(frame1.m_swayVelocity.y, frame2.m_swayVelocity.y, t);
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if ((data.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			data.m_frame0.m_swayVelocity.y = 5f;
			data.m_frame1.m_swayVelocity.y = 5f;
			data.m_frame2.m_swayVelocity.y = 5f;
			data.m_frame3.m_swayVelocity.y = 5f;
		}
		else
		{
			data.m_frame0.m_swayVelocity.y = 0f;
			data.m_frame1.m_swayVelocity.y = 0f;
			data.m_frame2.m_swayVelocity.y = 0f;
			data.m_frame3.m_swayVelocity.y = 0f;
		}
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				data.m_flags &= ~Vehicle.Flags.Arriving;
				this.PathfindSuccess(vehicleID, ref data);
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				this.PathfindFailure(vehicleID, ref data);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		Vector3 lastFramePosition = data.GetLastFramePosition();
		int lodPhysics;
		if (Vector3.SqrMagnitude(physicsLodRefPos - lastFramePosition) >= 1210000f)
		{
			lodPhysics = 2;
		}
		else if (Vector3.SqrMagnitude(Singleton<SimulationManager>.get_instance().m_simulationView.m_position - lastFramePosition) >= 250000f)
		{
			lodPhysics = 1;
		}
		else
		{
			lodPhysics = 0;
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, lodPhysics);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, lodPhysics);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(this.m_info.m_class.m_service);
		int num3 = (privateServiceIndex == -1) ? 150 : 100;
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 && data.m_cargoParent == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
		else if ((int)data.m_blockCounter == num3)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		data.Unspawn(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num = this.m_info.m_acceleration;
		float braking = this.m_info.m_braking;
		num *= Mathf.Max(0f, frameData.m_swayVelocity.y * 0.175f - 0.75f);
		float num2 = VectorUtils.LengthXZ(frameData.m_velocity);
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float num3 = VectorUtils.LengthSqrXZ(vector);
		float num4 = (num2 + num) * (0.5f + 0.5f * (num2 + num) / braking) + this.m_info.m_generatedInfo.m_size.z * 0.5f;
		float num5 = Mathf.Max(num2 + num, 5f);
		if (lodPhysics >= 2 && (ulong)(currentFrameIndex >> 4 & 3u) == (ulong)((long)(vehicleID & 3)))
		{
			num5 *= 2f;
		}
		float num6 = Mathf.Max((num4 - num5) / 3f, 1f);
		float num7 = num5 * num5;
		float num8 = num6 * num6;
		int i = 0;
		bool flag = false;
		if ((num3 < num7 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				this.UpdatePathTargetPositions(vehicleID, ref vehicleData, frameData.m_position, ref i, 4, num7, num8);
				if ((leaderData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0)
				{
					frameData = vehicleData.m_frame0;
					return;
				}
			}
			if ((leaderData.m_flags & Vehicle.Flags.WaitingPath) == (Vehicle.Flags)0)
			{
				if (i < 4)
				{
					leaderData.m_flags |= Vehicle.Flags.Landing;
				}
				while (i < 4)
				{
					float minSqrDistance;
					Vector3 refPos;
					if (i == 0)
					{
						minSqrDistance = num7;
						refPos = frameData.m_position;
						flag = true;
					}
					else
					{
						minSqrDistance = num8;
						refPos = vehicleData.GetTargetPos(i - 1);
					}
					int num9 = i;
					this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, refPos, leaderID, ref leaderData, ref i, minSqrDistance);
					if (i == num9)
					{
						break;
					}
				}
				if (i != 0)
				{
					Vector4 targetPos = vehicleData.GetTargetPos(i - 1);
					while (i < 4)
					{
						vehicleData.SetTargetPos(i++, targetPos);
					}
				}
			}
			vector = vehicleData.m_targetPos0 - frameData.m_position;
			num3 = VectorUtils.LengthSqrXZ(vector);
		}
		if (leaderData.m_path != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			byte b = leaderData.m_pathPositionIndex;
			if (b == 255)
			{
				b = 0;
			}
			PathManager instance2 = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].GetPosition(b >> 1, out pathPos))
			{
				instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(this.m_info.m_generatedInfo.m_size.z * 3f), this.GetNoiseLevel());
				uint laneID = PathManager.GetLaneID(pathPos);
				if (laneID != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(this.m_info.m_generatedInfo.m_size.z, vehicleID);
					if ((b >> 1) + 1 >= (int)instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].m_positionCount && instance2.m_pathUnits.m_buffer[(int)((UIntPtr)leaderData.m_path)].m_nextPathUnit == 0u)
					{
						leaderData.m_flags |= Vehicle.Flags.Landing;
					}
				}
			}
		}
		float num10;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			num10 = 0f;
		}
		else
		{
			num10 = vehicleData.m_targetPos0.w;
		}
		float num11;
		if ((leaderData.m_flags & Vehicle.Flags.Landing) != (Vehicle.Flags)0)
		{
			num11 = vehicleData.m_targetPos0.y;
			if (frameData.m_position.y < num11 + 0.5f)
			{
				leaderData.m_flags &= ~Vehicle.Flags.Flying;
			}
			else
			{
				leaderData.m_flags |= Vehicle.Flags.Flying;
			}
		}
		else
		{
			Vector3 vector2 = vehicleData.m_targetPos3 - frameData.m_position;
			vector2.y = 0f;
			Vector3 endPosition = frameData.m_position + Vector3.ClampMagnitude(vector2, num4);
			num11 = this.GetFlightHeight(vehicleID, frameData.m_position, endPosition);
			leaderData.m_flags |= Vehicle.Flags.Flying;
		}
		float num12 = num11 - frameData.m_position.y;
		float num13 = Mathf.Min(this.m_info.m_maxSpeed, 0.5f * Mathf.Sqrt(2f * Mathf.Abs(num12 * num)));
		float num14 = Mathf.Clamp(num12, -num13, num13);
		num10 = Mathf.Min(num10, this.m_info.m_maxSpeed - num13);
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector = quaternion * vector;
		Vector3 vector3 = quaternion * frameData.m_velocity;
		Vector3 vector4 = Vector3.get_forward();
		Vector3 vector5 = Vector3.get_zero();
		Vector3 zero = Vector3.get_zero();
		float num15 = 0f;
		bool flag2 = false;
		bool flag3 = (leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0;
		float num16 = 0f;
		if (num3 > 1f)
		{
			vector4 = VectorUtils.NormalizeXZ(vector, ref num16);
			if (num16 > 1f)
			{
				Vector3 vector6 = vector;
				num5 = Mathf.Max(num2, 2f);
				num7 = num5 * num5;
				if (num3 > num7)
				{
					vector6 *= num5 / Mathf.Sqrt(num3);
				}
				float num17;
				vector4 = VectorUtils.NormalizeXZ(vector6, ref num17);
				num16 = Mathf.Min(num16, num17);
				float num18 = 1.57079637f * (1f - vector4.z);
				if (num16 > 1f)
				{
					num18 /= num16;
				}
				float num19 = num16;
				if (vehicleData.m_targetPos0.w < 0.1f)
				{
					num10 = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num18);
					num10 = Mathf.Min(num10, BlimpAI.CalculateMaxSpeed(num19, Mathf.Min(vehicleData.m_targetPos0.w, vehicleData.m_targetPos1.w), braking * 0.9f));
				}
				else
				{
					num10 = Mathf.Min(num10, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num18));
					num10 = Mathf.Min(num10, BlimpAI.CalculateMaxSpeed(num19, vehicleData.m_targetPos1.w, braking * 0.9f));
				}
				num19 += VectorUtils.LengthXZ(vehicleData.m_targetPos1 - vehicleData.m_targetPos0);
				num10 = Mathf.Min(num10, BlimpAI.CalculateMaxSpeed(num19, vehicleData.m_targetPos2.w, braking * 0.9f));
				num19 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
				num10 = Mathf.Min(num10, BlimpAI.CalculateMaxSpeed(num19, vehicleData.m_targetPos3.w, braking * 0.9f));
				num19 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
				if (vehicleData.m_targetPos3.w < 0.01f)
				{
					num19 = Mathf.Max(0f, num19 - this.m_info.m_generatedInfo.m_size.z * 0.5f);
				}
				num10 = Mathf.Min(num10, BlimpAI.CalculateMaxSpeed(num19, 0f, braking * 0.9f));
				if (num10 < num2)
				{
					float num20 = Mathf.Max(num, Mathf.Min(braking, num2));
					num15 = Mathf.Max(num10, num2 - num20);
				}
				else
				{
					float num21 = Mathf.Max(num, Mathf.Min(braking, -num2));
					num15 = Mathf.Min(num10, num2 + num21);
				}
			}
		}
		else if (num2 < 0.1f && flag && Mathf.Abs(num11 - frameData.m_position.y) < 1f)
		{
			flag3 = true;
			if (frameData.m_swayVelocity.y < 0.1f && this.ArriveAtDestination(leaderID, ref leaderData))
			{
				leaderData.Unspawn(leaderID);
				if (leaderID == vehicleID)
				{
					frameData = leaderData.m_frame0;
				}
				return;
			}
		}
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && num10 < 0.1f)
		{
			flag2 = true;
		}
		if (flag2)
		{
			vehicleData.m_blockCounter = (byte)Mathf.Min((int)(vehicleData.m_blockCounter + 1), 255);
		}
		else
		{
			vehicleData.m_blockCounter = 0;
		}
		if (num16 > 1f)
		{
			vector5 = vector4 * num15;
		}
		else
		{
			num15 = 0f;
			Vector3 vector7 = vector * 0.5f - vector3;
			vector7.y = 0f;
			vector7 = Vector3.ClampMagnitude(vector7, braking);
			vector5 = vector3 + vector7;
		}
		Vector3 vector8 = vector5 - vector3;
		vector8.y = 0f;
		Vector3 vector9 = frameData.m_rotation * vector5;
		vector9.y = frameData.m_velocity.y + Mathf.Clamp(num14 - frameData.m_velocity.y, -this.m_info.m_acceleration, this.m_info.m_acceleration);
		frameData.m_velocity = vector9 + zero;
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayVelocity.x = frameData.m_swayVelocity.x * (1f - this.m_info.m_dampers) - vector8.x * (1f - this.m_info.m_springs) - frameData.m_swayPosition.x * this.m_info.m_springs;
		if (flag3)
		{
			frameData.m_swayVelocity.y = Mathf.Max(frameData.m_swayVelocity.y - 0.5f, 0f);
		}
		else
		{
			frameData.m_swayVelocity.y = Mathf.Min(frameData.m_swayVelocity.y + 0.5f, 10f);
		}
		frameData.m_swayVelocity.z = frameData.m_swayVelocity.z * (1f - this.m_info.m_dampers) - vector8.z * (1f - this.m_info.m_springs) - frameData.m_swayPosition.z * this.m_info.m_springs;
		frameData.m_swayPosition.x = frameData.m_swayPosition.x + frameData.m_swayVelocity.x * 0.5f;
		frameData.m_swayPosition.y = 0f;
		frameData.m_swayPosition.z = frameData.m_swayPosition.z + frameData.m_swayVelocity.z * 0.5f;
		frameData.m_steerAngle = 0f;
		frameData.m_travelDistance += frameData.m_swayVelocity.y;
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = 5f;
		frameData.m_lightIntensity.w = frameData.m_swayVelocity.y;
		if (num15 > 0.1f)
		{
			Vector3 vector10 = vehicleData.m_targetPos3 - frameData.m_position;
			vector10.y = 0f;
			if (vector10.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.RotateTowards(frameData.m_rotation, Quaternion.LookRotation(vector10), 10f);
			}
		}
		else if (num15 < -0.1f)
		{
			Vector3 vector11 = vehicleData.m_targetPos3 - frameData.m_position;
			vector11.y = 0f;
			if (vector11.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.RotateTowards(frameData.m_rotation, Quaternion.LookRotation(-vector11), 10f);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	protected Vector4 CalculateTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float speed)
	{
		Vector3 vector = targetPos - refPos;
		float sqrMagnitude = vector.get_sqrMagnitude();
		Vector4 result;
		if (sqrMagnitude > maxSqrDistance)
		{
			result = refPos + vector * Mathf.Sqrt(maxSqrDistance / sqrMagnitude);
		}
		else
		{
			result = targetPos;
		}
		result.w = speed;
		return result;
	}

	protected float GetFlightHeight(ushort vehicleID, Vector3 startPosition, Vector3 endPosition)
	{
		Randomizer randomizer;
		randomizer..ctor((int)vehicleID);
		float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight((startPosition + endPosition) * 0.5f);
		float num2 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(endPosition);
		float num3 = Mathf.Max(num, num2) + 80f;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		Segment2 segment;
		segment..ctor(VectorUtils.XZ(startPosition), VectorUtils.XZ(endPosition));
		Vector2 vector = segment.Min();
		Vector2 vector2 = segment.Max();
		int num4 = Mathf.Max((int)((vector.x - 72f) / 64f + 135f), 0);
		int num5 = Mathf.Max((int)((vector.y - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Min((int)((vector2.x + 72f) / 64f + 135f), 269);
		int num7 = Mathf.Min((int)((vector2.y + 72f) / 64f + 135f), 269);
		for (int i = num5; i <= num7; i++)
		{
			for (int j = num4; j <= num6; j++)
			{
				ushort num8 = instance.m_buildingGrid[i * 270 + j];
				int num9 = 0;
				while (num8 != 0)
				{
					BuildingInfo buildingInfo;
					int num10;
					int num11;
					instance.m_buildings.m_buffer[(int)num8].GetInfoWidthLength(out buildingInfo, out num10, out num11);
					Vector3 position = instance.m_buildings.m_buffer[(int)num8].m_position;
					float num12 = (float)(num10 * num10 + num11 * num11) * 16f;
					float num13;
					if (segment.DistanceSqr(VectorUtils.XZ(position), ref num13) < num12)
					{
						float num14 = buildingInfo.m_collisionHeight;
						Building.Flags flags = instance.m_buildings.m_buffer[(int)num8].m_flags;
						if ((flags & Building.Flags.Collapsed) != Building.Flags.None)
						{
							BuildingInfoBase collapsedInfo = buildingInfo.m_collapsedInfo;
							if (collapsedInfo != null)
							{
								num14 = collapsedInfo.m_generatedInfo.m_size.y;
							}
						}
						else if ((flags & Building.Flags.Upgrading) != Building.Flags.None)
						{
							BuildingInfo upgradeInfo = buildingInfo.m_buildingAI.GetUpgradeInfo(num8, ref instance.m_buildings.m_buffer[(int)num8]);
							if (upgradeInfo != null)
							{
								num14 = Mathf.Max(num14, upgradeInfo.m_collisionHeight);
							}
						}
						num3 = Mathf.Max(num3, position.y + num14);
					}
					num8 = instance.m_buildings.m_buffer[(int)num8].m_nextGridBuilding;
					if (++num9 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num3 + (float)randomizer.Int32(30, 50);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	protected new void UpdatePathTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ref int index, int max, float minSqrDistanceA, float minSqrDistanceB)
	{
		PathManager instance = Singleton<PathManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector4 vector = vehicleData.m_targetPos0;
		vector.w = 1000f;
		float num = minSqrDistanceA;
		uint num2 = vehicleData.m_path;
		byte b = vehicleData.m_pathPositionIndex;
		byte b2 = vehicleData.m_lastPathOffset;
		if (b == 255)
		{
			b = 0;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = 0;
			}
			if (!Singleton<PathManager>.get_instance().m_pathUnits.m_buffer[(int)((UIntPtr)num2)].CalculatePathPositionOffset(b >> 1, vector, out b2))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
				return;
			}
		}
		PathUnit.Position position;
		if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].GetPosition(b >> 1, out position))
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		NetInfo info = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes.Length <= (int)position.m_lane)
		{
			this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			return;
		}
		uint num3 = PathManager.GetLaneID(position);
		NetInfo.Lane lane = info.m_lanes[(int)position.m_lane];
		Bezier3 bezier;
		while (true)
		{
			if ((b & 1) == 0)
			{
				if (lane.m_laneType != NetInfo.LaneType.CargoVehicle)
				{
					bool flag = true;
					while (b2 != position.m_offset)
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							float num4 = Mathf.Sqrt(num) - VectorUtils.LengthXZ(vector - refPos);
							int num5;
							if (num4 < 0f)
							{
								num5 = 4;
							}
							else
							{
								num5 = 4 + Mathf.Max(0, Mathf.CeilToInt(num4 * 256f / (instance2.m_lanes.m_buffer[(int)((UIntPtr)num3)].m_length + 1f)));
							}
							if (b2 > position.m_offset)
							{
								b2 = (byte)Mathf.Max((int)b2 - num5, (int)position.m_offset);
							}
							else if (b2 < position.m_offset)
							{
								b2 = (byte)Mathf.Min((int)b2 + num5, (int)position.m_offset);
							}
						}
						Vector3 vector2;
						Vector3 vector3;
						float num6;
						this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, b2, out vector2, out vector3, out num6);
						vector.Set(vector2.x, vector2.y, vector2.z, Mathf.Min(vector.w, num6));
						float num7 = VectorUtils.LengthSqrXZ(vector2 - refPos);
						if (num7 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
				b += 1;
				b2 = 0;
				if (index <= 0)
				{
					vehicleData.m_pathPositionIndex = b;
					vehicleData.m_lastPathOffset = b2;
				}
			}
			int num8 = (b >> 1) + 1;
			uint num9 = num2;
			if (num8 >= (int)instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_positionCount)
			{
				num8 = 0;
				num9 = instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].m_nextPathUnit;
				if (num9 == 0u)
				{
					goto Block_17;
				}
			}
			PathUnit.Position position2;
			if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetPosition(num8, out position2))
			{
				goto Block_19;
			}
			NetInfo info2 = instance2.m_segments.m_buffer[(int)position2.m_segment].Info;
			if (info2.m_lanes.Length <= (int)position2.m_lane)
			{
				goto Block_20;
			}
			uint laneID = PathManager.GetLaneID(position2);
			NetInfo.Lane lane2 = info2.m_lanes[(int)position2.m_lane];
			ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
			ushort endNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_endNode;
			ushort startNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
			ushort endNode2 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
			if (startNode2 != startNode && startNode2 != endNode && endNode2 != startNode && endNode2 != endNode && ((instance2.m_nodes.m_buffer[(int)startNode].m_flags | instance2.m_nodes.m_buffer[(int)endNode].m_flags) & NetNode.Flags.Disabled) == NetNode.Flags.None && ((instance2.m_nodes.m_buffer[(int)startNode2].m_flags | instance2.m_nodes.m_buffer[(int)endNode2].m_flags) & NetNode.Flags.Disabled) != NetNode.Flags.None)
			{
				goto Block_26;
			}
			if (lane2.m_laneType == NetInfo.LaneType.Pedestrian)
			{
				goto Block_27;
			}
			if ((byte)(lane2.m_laneType & (NetInfo.LaneType.Vehicle | NetInfo.LaneType.CargoVehicle | NetInfo.LaneType.TransportVehicle)) == 0)
			{
				goto Block_33;
			}
			if (lane2.m_vehicleType != this.m_info.m_vehicleType && this.NeedChangeVehicleType(vehicleID, ref vehicleData, position2, laneID, lane2.m_vehicleType, ref vector))
			{
				goto Block_35;
			}
			if (position2.m_segment != position.m_segment && vehicleID != 0)
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Leaving;
			}
			byte b3 = 0;
			if (num3 != laneID && lane.m_laneType != NetInfo.LaneType.CargoVehicle)
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
				bezier = default(Bezier3);
				Vector3 vector4;
				float num10;
				this.CalculateSegmentPosition(vehicleID, ref vehicleData, position, num3, position.m_offset, out bezier.a, out vector4, out num10);
				bool flag2 = b2 == 0;
				if (flag2)
				{
					if ((vehicleData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
					{
						flag2 = (vehicleData.m_trailingVehicle == 0);
					}
					else
					{
						flag2 = (vehicleData.m_leadingVehicle == 0);
					}
				}
				Vector3 vector5;
				float num11;
				if (flag2)
				{
					PathUnit.Position nextPosition;
					if (!instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].GetNextPosition(num8, out nextPosition))
					{
						nextPosition = default(PathUnit.Position);
					}
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, nextPosition, position2, laneID, b3, position, num3, position.m_offset, index, out bezier.d, out vector5, out num11);
				}
				else
				{
					this.CalculateSegmentPosition(vehicleID, ref vehicleData, position2, laneID, b3, out bezier.d, out vector5, out num11);
				}
				if (num11 < 0.01f || (instance2.m_segments.m_buffer[(int)position2.m_segment].m_flags & (NetSegment.Flags.Collapsed | NetSegment.Flags.Flooded)) != NetSegment.Flags.None)
				{
					goto IL_830;
				}
				if (position.m_offset == 0)
				{
					vector4 = -vector4;
				}
				if (b3 < position2.m_offset)
				{
					vector5 = -vector5;
				}
				vector4.Normalize();
				vector5.Normalize();
				float num12;
				NetSegment.CalculateMiddlePoints(bezier.a, vector4, bezier.d, vector5, true, true, out bezier.b, out bezier.c, out num12);
				if (num12 > 1f)
				{
					ushort num13;
					if (b3 == 0)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_startNode;
					}
					else if (b3 == 255)
					{
						num13 = instance2.m_segments.m_buffer[(int)position2.m_segment].m_endNode;
					}
					else
					{
						num13 = 0;
					}
					float num14 = 1.57079637f * (1f + Vector3.Dot(vector4, vector5));
					if (num12 > 1f)
					{
						num14 /= num12;
					}
					num11 = Mathf.Min(num11, this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1000f, num14));
					while (b2 < 255)
					{
						float num15 = Mathf.Sqrt(num) - Vector3.Distance(vector, refPos);
						int num16;
						if (num15 < 0f)
						{
							num16 = 8;
						}
						else
						{
							num16 = 8 + Mathf.Max(0, Mathf.CeilToInt(num15 * 256f / (num12 + 1f)));
						}
						b2 = (byte)Mathf.Min((int)b2 + num16, 255);
						Vector3 vector6 = bezier.Position((float)b2 * 0.003921569f);
						vector.Set(vector6.x, vector6.y, vector6.z, Mathf.Min(vector.w, num11));
						float num17 = VectorUtils.LengthSqrXZ(vector6 - refPos);
						if (num17 >= num)
						{
							if (index <= 0)
							{
								vehicleData.m_lastPathOffset = b2;
							}
							if (num13 != 0)
							{
								this.UpdateNodeTargetPos(vehicleID, ref vehicleData, num13, ref instance2.m_nodes.m_buffer[(int)num13], ref vector, index);
							}
							vehicleData.SetTargetPos(index++, vector);
							num = minSqrDistanceB;
							refPos = vector;
							vector.w = 1000f;
							if (index == max)
							{
								return;
							}
						}
					}
				}
			}
			else
			{
				PathUnit.CalculatePathPositionOffset(laneID, vector, out b3);
			}
			if (index <= 0)
			{
				if (num8 == 0)
				{
					Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
				}
				if (num8 >= (int)(instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_positionCount - 1) && instance.m_pathUnits.m_buffer[(int)((UIntPtr)num9)].m_nextPathUnit == 0u && vehicleID != 0)
				{
					this.ArrivingToDestination(vehicleID, ref vehicleData);
				}
			}
			num2 = num9;
			b = (byte)(num8 << 1);
			b2 = b3;
			if (index <= 0)
			{
				vehicleData.m_pathPositionIndex = b;
				vehicleData.m_lastPathOffset = b2;
				vehicleData.m_flags = ((vehicleData.m_flags & ~(Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition)) | info2.m_setVehicleFlags);
				if (this.LeftHandDrive(lane2))
				{
					vehicleData.m_flags |= Vehicle.Flags.LeftHandDrive;
				}
				else
				{
					vehicleData.m_flags &= (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding);
				}
			}
			position = position2;
			num3 = laneID;
			lane = lane2;
		}
		return;
		Block_17:
		if (index <= 0)
		{
			Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
			vehicleData.m_path = 0u;
		}
		vector.w = 1f;
		vehicleData.SetTargetPos(index++, vector);
		return;
		Block_19:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_20:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_26:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_27:
		if (vehicleID != 0 && (vehicleData.m_flags & Vehicle.Flags.Parking) == (Vehicle.Flags)0)
		{
			byte offset = position.m_offset;
			byte offset2 = position.m_offset;
			int num8;
			uint num9;
			if (this.ParkVehicle(vehicleID, ref vehicleData, position, num9, num8 << 1, out offset2))
			{
				if (offset2 != offset)
				{
					if (index <= 0)
					{
						vehicleData.m_pathPositionIndex = (byte)((int)vehicleData.m_pathPositionIndex & -2);
						vehicleData.m_lastPathOffset = offset;
					}
					position.m_offset = offset2;
					instance.m_pathUnits.m_buffer[(int)((UIntPtr)num2)].SetPosition(b >> 1, position);
				}
				vehicleData.m_flags |= Vehicle.Flags.Parking;
			}
			else
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		return;
		Block_33:
		this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
		return;
		Block_35:
		float num18 = VectorUtils.LengthSqrXZ(vector - refPos);
		if (num18 >= num)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
		if (index <= 0)
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
			uint num9;
			if (num9 != vehicleData.m_path)
			{
				Singleton<PathManager>.get_instance().ReleaseFirstUnit(ref vehicleData.m_path);
			}
			int num8;
			vehicleData.m_pathPositionIndex = (byte)(num8 << 1);
			uint laneID;
			PathUnit.CalculatePathPositionOffset(laneID, vector, out vehicleData.m_lastPathOffset);
			PathUnit.Position position2;
			if (vehicleID != 0 && !this.ChangeVehicleType(vehicleID, ref vehicleData, position2, laneID))
			{
				this.InvalidPath(vehicleID, ref vehicleData, vehicleID, ref vehicleData);
			}
		}
		else
		{
			while (index < max)
			{
				vehicleData.SetTargetPos(index++, vector);
			}
		}
		return;
		IL_830:
		if (index <= 0)
		{
			vehicleData.m_lastPathOffset = b2;
		}
		vector = bezier.a;
		vector.w = 0f;
		while (index < max)
		{
			vehicleData.SetTargetPos(index++, vector);
		}
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle)
	{
		Segment2 segment2;
		segment2..ctor(VectorUtils.XZ(segment.a), VectorUtils.XZ(segment.b));
		return BlimpAI.CheckOverlap(segment2, ignoreVehicle);
	}

	private static bool CheckOverlap(Segment2 segment, ushort ignoreVehicle)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector2 vector = segment.Min();
		Vector2 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 10f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((vector.y - 10f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 10f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((vector2.y + 10f) / 320f + 27f), 53);
		bool flag = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = BlimpAI.CheckOverlap(segment, ignoreVehicle, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref flag);
					if (flag)
					{
						return flag;
					}
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return flag;
	}

	private static ushort CheckOverlap(Segment2 segment, ushort ignoreVehicle, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		if (ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle))
		{
			Segment2 segment2;
			segment2..ctor(VectorUtils.XZ(otherData.m_segment.a), VectorUtils.XZ(otherData.m_segment.b));
			float num;
			float num2;
			if (segment.DistanceSqr(segment2, ref num, ref num2) < 4f)
			{
				overlap = true;
			}
		}
		return otherData.m_nextGridVehicle;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(Mathf.Max(0f, num2 * num2 + 2f * targetDistance * maxBraking)) - num;
	}

	protected override void InvalidPath(ushort vehicleID, ref Vehicle vehicleData, ushort leaderID, ref Vehicle leaderData)
	{
		vehicleData.m_targetPos0 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos3;
		vehicleData.m_targetPos3.w = 0f;
		base.InvalidPath(vehicleID, ref vehicleData, leaderID, ref leaderData);
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
		{
			maxSpeed = 0f;
			return;
		}
		Vector2 vector = VectorUtils.XZ(instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition(0f));
		Vector2 vector2 = VectorUtils.XZ(instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition(0.5f));
		Vector2 vector3 = VectorUtils.XZ(instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePosition(1f));
		if (BlimpAI.CheckOverlap(new Segment2(vector, vector2), vehicleID) || BlimpAI.CheckOverlap(new Segment2(vector2, vector3), vehicleID))
		{
			maxSpeed = 0f;
			return;
		}
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
			vehicleData.m_flags &= ~Vehicle.Flags.Landing;
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, info.m_lanes[(int)position.m_lane].m_speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true, false);
	}

	protected virtual bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		bool flag = PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out startPosA, out startPosB, out num, out num2);
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		bool flag2 = PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out endPosA, out endPosB, out num3, out num4);
		PathUnit.Position position;
		PathUnit.Position position2;
		float num5;
		float num6;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Beautification, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out position, out position2, out num5, out num6) && (!flag || num5 < num))
		{
			startPosA = position;
			startPosB = position2;
			num = num5;
			num2 = num6;
			flag = true;
		}
		PathUnit.Position position3;
		PathUnit.Position position4;
		float num7;
		float num8;
		if (PathManager.FindPathPosition(endPos, ItemClass.Service.Beautification, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 64f, out position3, out position4, out num7, out num8) && (!flag2 || num7 < num3))
		{
			endPosA = position3;
			endPosB = position4;
			num3 = num7;
			num4 = num8;
			flag2 = true;
		}
		if (flag && flag2)
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f, false, this.IgnoreBlocked(vehicleID, ref vehicleData), false, false))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (BlimpAI.CheckOverlap(vehicleData.m_segment, 0))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}
}
