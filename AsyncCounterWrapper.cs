﻿using System;
using UnityEngine;

public class AsyncCounterWrapper : AsyncTaskBase
{
	public AsyncCounterWrapper(float time) : base(string.Empty)
	{
		this.m_StartTime = Time.get_realtimeSinceStartup();
		this.m_Time = time;
	}

	public override string name
	{
		get
		{
			float num = Time.get_realtimeSinceStartup() - this.m_StartTime;
			float num2 = Mathf.Abs(this.m_Time - num);
			return string.Format("Demo will reset in {0} seconds", Mathf.CeilToInt(num2));
		}
	}

	public override bool completedOrFailed
	{
		get
		{
			float num = Time.get_realtimeSinceStartup() - this.m_StartTime;
			float num2 = this.m_Time - num;
			return num2 <= 0f;
		}
	}

	public override bool isExecuting
	{
		get
		{
			float num = Time.get_realtimeSinceStartup() - this.m_StartTime;
			float num2 = this.m_Time - num;
			return num2 > 0f;
		}
	}

	public override void Execute()
	{
		throw new NotImplementedException();
	}

	private float m_Time;

	private float m_StartTime;
}
