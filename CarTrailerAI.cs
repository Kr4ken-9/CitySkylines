﻿using System;
using ColossalFramework;
using UnityEngine;

public class CarTrailerAI : VehicleAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (data.m_leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort firstVehicle = data.GetFirstVehicle(vehicleID);
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)firstVehicle].Info;
			return info.m_vehicleAI.GetColor(firstVehicle, ref instance.m_vehicles.m_buffer[(int)firstVehicle], infoMode);
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		if (vehicleData.m_leadingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort leadingVehicle = vehicleData.m_leadingVehicle;
			frameData.m_position += frameData.m_velocity * 0.4f;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
			Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
			Vector3 vector = quaternion * frameData.m_velocity;
			Vehicle.Flags flags = instance.m_vehicles.m_buffer[(int)leadingVehicle].m_flags;
			Vehicle.Frame lastFrameData = instance.m_vehicles.m_buffer[(int)leadingVehicle].GetLastFrameData();
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)leadingVehicle].Info;
			Vehicle.Flags flags2 = Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.GoingBack | Vehicle.Flags.OnGravel | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.LeftHandDrive;
			vehicleData.m_flags = ((vehicleData.m_flags & ~flags2) | (flags & flags2));
			float num;
			if ((flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
			{
				num = info.m_attachOffsetFront;
			}
			else
			{
				num = info.m_attachOffsetBack;
			}
			Vector3 vector2 = lastFrameData.m_position + lastFrameData.m_rotation * new Vector3(0f, 0f, num - info.m_generatedInfo.m_size.z * 0.5f);
			Vector3 vector3 = vector2 - frameData.m_position;
			float magnitude = vector3.get_magnitude();
			Vector3 vector4;
			if (magnitude > 0.1f)
			{
				float num2;
				if ((vehicleData.m_flags & Vehicle.Flags.Inverted) != (Vehicle.Flags)0)
				{
					num2 = this.m_info.m_attachOffsetBack;
				}
				else
				{
					num2 = this.m_info.m_attachOffsetFront;
				}
				vector4 = vector3 * (1f + (num2 - this.m_info.m_generatedInfo.m_size.z * 0.5f) / magnitude);
			}
			else
			{
				vector4 = Vector3.get_zero();
			}
			Vector3 vector5 = quaternion * (vector4 / 0.6f);
			Vector3 vector6 = vector5 - vector;
			frameData.m_velocity = frameData.m_rotation * vector5;
			frameData.m_position += frameData.m_velocity * 0.6f;
			frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector6 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
			frameData.m_swayVelocity.z = -lastFrameData.m_swayVelocity.z;
			frameData.m_swayPosition.z = -lastFrameData.m_swayPosition.z;
			frameData.m_steerAngle = 0f;
			frameData.m_travelDistance += vector5.z;
			frameData.m_lightIntensity = lastFrameData.m_lightIntensity;
			frameData.m_underground = lastFrameData.m_underground;
			frameData.m_transition = lastFrameData.m_transition;
			vehicleData.m_targetPos0 = lastFrameData.m_position;
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = leaderData.m_targetPos0;
			vehicleData.m_targetPos2 = leaderData.m_targetPos1;
			vehicleData.m_targetPos3 = leaderData.m_targetPos2;
			if (magnitude > 0.1f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector3);
			}
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.4f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public override int GetNoiseLevel()
	{
		return (this.m_info.m_class.m_service != ItemClass.Service.Industrial) ? 5 : 9;
	}
}
