﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class EffectItemDisaster : EffectSubItem
{
	private void Awake()
	{
		this.m_thumbnail = base.Find<UISprite>("Thumbnail");
		this.m_disasterName = base.Find<UIButton>("DisasterName");
		this.m_selectDisaster = base.Find<UIButton>("SelectFromMap");
		this.m_addDisaster = base.Find<UIButton>("CreateNew");
		this.m_disasterName.add_eventClick(new MouseEventHandler(this.OnDisasterNameClicked));
		this.m_selectDisaster.add_eventClick(new MouseEventHandler(this.OnSelectDisasterClicked));
		this.m_addDisaster.add_eventClick(new MouseEventHandler(this.OnAddDisasterClicked));
		base.get_component().add_eventVisibilityChanged(delegate(UIComponent c, bool visible)
		{
			if (visible)
			{
				this.ShowImpl();
			}
		});
	}

	protected override void ShowImpl()
	{
		this.RefreshShownDisaster();
	}

	private void RefreshShownDisaster()
	{
		if (this.areDisastersAvailable)
		{
			this.m_selectDisaster.set_isEnabled(true);
			this.m_selectDisaster.set_tooltip(string.Empty);
		}
		else
		{
			this.m_selectDisaster.set_isEnabled(false);
			this.m_selectDisaster.set_tooltip(Locale.Get("TRIGGERPANEL_NODISASTERSAVAILABLE"));
		}
		if (this.m_disasterID == 0)
		{
			this.m_disasterName.set_text(Locale.Get("TRIGGERPANEL_NONE"));
			this.m_disasterName.set_isEnabled(false);
			this.m_thumbnail.set_spriteName(string.Empty);
		}
		else
		{
			this.m_disasterName.set_text(Singleton<DisasterManager>.get_instance().GetDisasterName(this.m_disasterID));
			this.m_disasterName.set_isEnabled(true);
			DisasterData disasterData = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)this.m_disasterID];
			this.m_thumbnail.set_spriteName(disasterData.Info.m_Thumbnail);
		}
	}

	private bool areDisastersAvailable
	{
		get
		{
			return Singleton<DisasterManager>.get_instance().m_disasterCount > 0;
		}
	}

	protected override void SetDefaultValuesImpl()
	{
		base.currentTriggerEffect = base.currentTriggerMilestone.AddEffect<DisasterEffect>();
		this.m_disasterID = 0;
		this.RefreshShownDisaster();
	}

	public void Load(DisasterEffect disasterEffect)
	{
		base.currentTriggerEffect = disasterEffect;
		this.m_disasterID = disasterEffect.GetDisaster();
		base.Show(EffectItem.EffectType.Disaster);
	}

	protected override void HideImpl()
	{
		base.currentTriggerMilestone.RemoveEffect(base.currentTriggerEffect);
	}

	public void OnDisasterNameClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		this.ShowDisaster();
	}

	public void ShowDisaster()
	{
		TriggerPanel.instance.OnCloseButton();
		Vector3 targetPosition = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)this.m_disasterID].m_targetPosition;
		InstanceID instanceID = default(InstanceID);
		instanceID.Disaster = this.m_disasterID;
		WorldInfoPanel.Show<DisasterWorldInfoPanel>(targetPosition, instanceID);
		ToolsModifierControl.cameraController.SetTarget(instanceID, targetPosition, false);
	}

	public void OnSelectDisasterClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		TriggerPanel.instance.OnCloseButton();
		SelectingDisasterPanel.instance.Show(this);
	}

	public void OnAddDisasterClicked(UIComponent component, UIMouseEventParameter eventParam)
	{
		TriggerPanel.instance.OnCloseButton();
		AddingDisasterPanel.instance.Show(this);
		EffectItemDisaster.disasterButton.SimulateClick();
	}

	public void OnDisasterSelected(ushort disasterID)
	{
		this.m_disasterID = disasterID;
		this.RefreshShownDisaster();
		DisasterEffect disasterEffect = base.currentTriggerEffect as DisasterEffect;
		disasterEffect.SetDisaster(disasterID);
		DisasterData disasterData = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer[(int)disasterID];
		this.m_thumbnail.set_spriteName(disasterData.Info.m_Thumbnail);
		EffectItemDisaster.triggerPanelButton.SimulateClick();
	}

	private static UIButton disasterButton
	{
		get
		{
			if (EffectItemDisaster.m_disasterButton == null)
			{
				EffectItemDisaster.m_disasterButton = UIView.Find<UIButton>("Disasters");
			}
			return EffectItemDisaster.m_disasterButton;
		}
	}

	private static UIButton triggerPanelButton
	{
		get
		{
			if (EffectItemDisaster.m_triggerPanelButton == null)
			{
				EffectItemDisaster.m_triggerPanelButton = UIView.Find<UIButton>("Trigger");
			}
			return EffectItemDisaster.m_triggerPanelButton;
		}
	}

	private UIButton m_disasterName;

	private UIButton m_selectDisaster;

	private UIButton m_addDisaster;

	private UISprite m_thumbnail;

	private ushort m_disasterID;

	private static UIButton m_disasterButton;

	private static UIButton m_triggerPanelButton;
}
