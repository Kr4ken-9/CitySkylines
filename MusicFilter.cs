﻿using System;
using ColossalFramework;
using UnityEngine;

public class MusicFilter : MonoBehaviour
{
	private void Awake()
	{
	}

	private void OnAudioFilterRead(float[] data, int channels)
	{
		if (Singleton<AudioManager>.get_exists())
		{
			Singleton<AudioManager>.get_instance().ReadStreamBuffer(data, channels);
		}
	}
}
