﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class EconomyManager : SimulationManagerBase<EconomyManager, EconomyProperties>, ISimulationManager
{
	public string GetBankName(int index)
	{
		if (this.m_properties != null && index < this.m_properties.m_banks.Length)
		{
			return this.m_properties.m_banks[index].m_bankName;
		}
		return string.Empty;
	}

	public bool GetLoanInfo(int index, out EconomyManager.LoanInfo[] infos)
	{
		if (this.m_properties != null && index < this.m_properties.m_banks.Length)
		{
			infos = this.m_properties.m_banks[index].m_loanOffers;
			return true;
		}
		infos = new EconomyManager.LoanInfo[0];
		return false;
	}

	public long LastCashAmount
	{
		get
		{
			return this.m_lastCashAmount;
		}
	}

	public long LastCashDelta
	{
		get
		{
			return this.m_lastCashDelta;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
		this.m_taxRates = new int[120];
		this.m_serviceBudgetDay = new int[22];
		this.m_serviceBudgetNight = new int[22];
		this.m_expenses = new long[2414];
		this.m_loanExpenses = new long[17];
		this.m_policyExpenses = new long[17];
		this.m_totalExpenses = new long[17];
		this.m_income = new long[2414];
		this.m_totalIncome = new long[17];
		this.m_loans = new EconomyManager.Loan[3];
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
	}

	private void CreateRelay()
	{
		if (this.m_EconomyWrapper == null)
		{
			this.m_EconomyWrapper = new EconomyWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_EconomyWrapper != null)
		{
			this.m_EconomyWrapper.Release();
			this.m_EconomyWrapper = null;
		}
	}

	public override void InitializeProperties(EconomyProperties properties)
	{
		base.InitializeProperties(properties);
	}

	private void ShowBailoutWindow()
	{
		UIView.get_library().ShowModal<BailoutPanel>("BailoutPanel", delegate(UIComponent comp, int ret)
		{
			if (ret == 1)
			{
				Singleton<SimulationManager>.get_instance().AddAction<bool>(this.AcceptBailout());
			}
			else
			{
				Singleton<SimulationManager>.get_instance().AddAction<bool>(this.RejectBailout());
			}
		}).SetAmount(this.m_properties.m_bailoutAmount / 100);
	}

	public long InternalCashAmount
	{
		get
		{
			return this.m_cashAmount;
		}
	}

	public bool GetLoan(int index, out EconomyManager.Loan loan)
	{
		if (index < this.m_loans.Length && this.m_loans[index].m_length != 0)
		{
			loan = this.m_loans[index];
			return true;
		}
		loan = default(EconomyManager.Loan);
		return false;
	}

	public int CountLoans()
	{
		int num = 0;
		for (int i = 0; i < this.m_loans.Length; i++)
		{
			if (this.m_loans[i].m_length != 0)
			{
				num++;
			}
		}
		return num;
	}

	[DebuggerHidden]
	public IEnumerator<bool> TakeNewLoan(int index, int amount, int interest, int length)
	{
		EconomyManager.<TakeNewLoan>c__Iterator0 <TakeNewLoan>c__Iterator = new EconomyManager.<TakeNewLoan>c__Iterator0();
		<TakeNewLoan>c__Iterator.index = index;
		<TakeNewLoan>c__Iterator.amount = amount;
		<TakeNewLoan>c__Iterator.interest = interest;
		<TakeNewLoan>c__Iterator.length = length;
		<TakeNewLoan>c__Iterator.$this = this;
		return <TakeNewLoan>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> PayLoanNow(int index)
	{
		EconomyManager.<PayLoanNow>c__Iterator1 <PayLoanNow>c__Iterator = new EconomyManager.<PayLoanNow>c__Iterator1();
		<PayLoanNow>c__Iterator.index = index;
		<PayLoanNow>c__Iterator.$this = this;
		return <PayLoanNow>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> AcceptBailout()
	{
		EconomyManager.<AcceptBailout>c__Iterator2 <AcceptBailout>c__Iterator = new EconomyManager.<AcceptBailout>c__Iterator2();
		<AcceptBailout>c__Iterator.$this = this;
		return <AcceptBailout>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> RejectBailout()
	{
		EconomyManager.<RejectBailout>c__Iterator3 <RejectBailout>c__Iterator = new EconomyManager.<RejectBailout>c__Iterator3();
		<RejectBailout>c__Iterator.$this = this;
		return <RejectBailout>c__Iterator;
	}

	public int GetBudget(ItemClass.Service service, ItemClass.SubService subService, bool night)
	{
		int num = EconomyManager.PublicClassIndex(service, subService);
		if (num == -1)
		{
			return 0;
		}
		if (night)
		{
			return this.m_serviceBudgetNight[num];
		}
		return this.m_serviceBudgetDay[num];
	}

	public int GetBudget(ItemClass itemClass)
	{
		return this.GetBudget(itemClass.m_service, itemClass.m_subService, Singleton<SimulationManager>.get_instance().m_isNightTime);
	}

	public int GetBudget(ItemClass itemClass, bool night)
	{
		return this.GetBudget(itemClass.m_service, itemClass.m_subService, night);
	}

	public void SetBudget(ItemClass.Service service, ItemClass.SubService subService, int budget, bool night)
	{
		int num = EconomyManager.PublicClassIndex(service, subService);
		if (num != -1)
		{
			if (night)
			{
				this.m_serviceBudgetNight[num] = budget;
			}
			else
			{
				this.m_serviceBudgetDay[num] = budget;
			}
			if (subService == ItemClass.SubService.None)
			{
				if (service == ItemClass.Service.PublicTransport)
				{
					this.SetBudget(service, ItemClass.SubService.PublicTransportBus, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportMetro, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportTrain, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportShip, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportPlane, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportTaxi, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportTram, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportMonorail, budget, night);
					this.SetBudget(service, ItemClass.SubService.PublicTransportCableCar, budget, night);
				}
			}
		}
	}

	public void SetBudget(ItemClass itemClass, int budget)
	{
		this.SetBudget(itemClass.m_service, itemClass.m_subService, budget, Singleton<SimulationManager>.get_instance().m_isNightTime);
	}

	public int GetTaxRate(ItemClass itemClass)
	{
		return this.GetTaxRate(itemClass.m_service, itemClass.m_subService, itemClass.m_level, DistrictPolicies.Taxation.None);
	}

	public int GetTaxRate(ItemClass itemClass, DistrictPolicies.Taxation taxationPolicies)
	{
		return this.GetTaxRate(itemClass.m_service, itemClass.m_subService, itemClass.m_level, taxationPolicies);
	}

	public int GetTaxRate(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		return this.GetTaxRate(service, subService, level, DistrictPolicies.Taxation.None);
	}

	public int GetTaxRate(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, DistrictPolicies.Taxation taxationPolicies)
	{
		int num = EconomyManager.PrivateClassIndex(service, subService, level);
		if (num == -1)
		{
			return 0;
		}
		int num2 = this.m_taxRates[num];
		if (taxationPolicies != DistrictPolicies.Taxation.None)
		{
			if (service == ItemClass.Service.Residential)
			{
				if (subService == ItemClass.SubService.ResidentialLow || subService == ItemClass.SubService.ResidentialLowEco)
				{
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxRaiseResLow) != DistrictPolicies.Taxation.None)
					{
						num2 += 2;
					}
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxLowerResLow) != DistrictPolicies.Taxation.None)
					{
						num2 -= 2;
					}
				}
				else
				{
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxRaiseResHigh) != DistrictPolicies.Taxation.None)
					{
						num2 += 2;
					}
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxLowerResHigh) != DistrictPolicies.Taxation.None)
					{
						num2 -= 2;
					}
				}
			}
			else if (service == ItemClass.Service.Commercial)
			{
				if (subService != ItemClass.SubService.CommercialLow)
				{
					if (subService != ItemClass.SubService.CommercialHigh)
					{
						if (subService == ItemClass.SubService.CommercialLeisure)
						{
							if ((taxationPolicies & DistrictPolicies.Taxation.DontTaxLeisure) != DistrictPolicies.Taxation.None)
							{
								num2 = 0;
							}
						}
					}
					else
					{
						if ((taxationPolicies & DistrictPolicies.Taxation.TaxRaiseComHigh) != DistrictPolicies.Taxation.None)
						{
							num2 += 2;
						}
						if ((taxationPolicies & DistrictPolicies.Taxation.TaxLowerComHigh) != DistrictPolicies.Taxation.None)
						{
							num2 -= 2;
						}
					}
				}
				else
				{
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxRaiseComLow) != DistrictPolicies.Taxation.None)
					{
						num2 += 2;
					}
					if ((taxationPolicies & DistrictPolicies.Taxation.TaxLowerComLow) != DistrictPolicies.Taxation.None)
					{
						num2 -= 2;
					}
				}
			}
			else if (service == ItemClass.Service.Office)
			{
				if ((taxationPolicies & DistrictPolicies.Taxation.TaxRaiseOffice) != DistrictPolicies.Taxation.None)
				{
					num2 += 2;
				}
				if ((taxationPolicies & DistrictPolicies.Taxation.TaxLowerOffice) != DistrictPolicies.Taxation.None)
				{
					num2 -= 2;
				}
			}
		}
		return Mathf.Clamp(num2, 0, 100);
	}

	public void SetTaxRate(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, int rate)
	{
		int num = EconomyManager.PrivateClassIndex(service, subService, level);
		if (num != -1)
		{
			this.m_taxRates[num] = rate;
			if (level == ItemClass.Level.None)
			{
				this.SetTaxRate(service, subService, ItemClass.Level.Level1, rate);
				this.SetTaxRate(service, subService, ItemClass.Level.Level2, rate);
				this.SetTaxRate(service, subService, ItemClass.Level.Level3, rate);
				this.SetTaxRate(service, subService, ItemClass.Level.Level4, rate);
				this.SetTaxRate(service, subService, ItemClass.Level.Level5, rate);
				if (subService == ItemClass.SubService.None)
				{
					switch (service)
					{
					case ItemClass.Service.Residential:
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.ResidentialLow, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.ResidentialHigh, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.ResidentialLowEco, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.ResidentialHighEco, level)] = rate;
						break;
					case ItemClass.Service.Commercial:
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.CommercialLow, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.CommercialHigh, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.CommercialLeisure, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.CommercialTourist, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.CommercialEco, level)] = rate;
						break;
					case ItemClass.Service.Industrial:
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.IndustrialGeneric, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.IndustrialForestry, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.IndustrialFarming, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.IndustrialOil, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.IndustrialOre, level)] = rate;
						break;
					case ItemClass.Service.Office:
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.OfficeGeneric, level)] = rate;
						this.m_taxRates[EconomyManager.PrivateClassIndex(service, ItemClass.SubService.OfficeHightech, level)] = rate;
						break;
					}
				}
			}
			else if (subService == ItemClass.SubService.None)
			{
				switch (service)
				{
				case ItemClass.Service.Residential:
					this.SetTaxRate(service, ItemClass.SubService.ResidentialLow, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.ResidentialHigh, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.ResidentialLowEco, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.ResidentialHighEco, level, rate);
					break;
				case ItemClass.Service.Commercial:
					this.SetTaxRate(service, ItemClass.SubService.CommercialLow, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.CommercialHigh, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.CommercialLeisure, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.CommercialTourist, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.CommercialEco, level, rate);
					break;
				case ItemClass.Service.Industrial:
					this.SetTaxRate(service, ItemClass.SubService.IndustrialGeneric, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.IndustrialForestry, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.IndustrialFarming, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.IndustrialOil, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.IndustrialOre, level, rate);
					break;
				case ItemClass.Service.Office:
					this.SetTaxRate(service, ItemClass.SubService.OfficeGeneric, level, rate);
					this.SetTaxRate(service, ItemClass.SubService.OfficeHightech, level, rate);
					break;
				}
			}
		}
	}

	public void SetTaxRate(ItemClass itemClass, int rate)
	{
		this.SetTaxRate(itemClass.m_service, itemClass.m_subService, itemClass.m_level, rate);
	}

	public void GetIncomeAndExpenses(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, out long income, out long expenses)
	{
		income = 0L;
		expenses = 0L;
		this.GetIncomeAndExpensesImpl(service, subService, level, ref income, ref expenses);
	}

	public void GetIncomeAndExpenses(ItemClass itemClass, out long income, out long expenses)
	{
		income = 0L;
		expenses = 0L;
		this.GetIncomeAndExpensesImpl(itemClass.m_service, itemClass.m_subService, itemClass.m_level, ref income, ref expenses);
	}

	public long GetLoanExpenses()
	{
		long num = 0L;
		for (int i = 0; i < 16; i++)
		{
			num += this.m_loanExpenses[i];
		}
		return num;
	}

	public long GetPolicyExpenses()
	{
		long num = 0L;
		for (int i = 0; i < 16; i++)
		{
			num += this.m_policyExpenses[i];
		}
		return num;
	}

	private void GetIncomeAndExpensesImpl(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, ref long income, ref long expenses)
	{
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(service);
		if (service == ItemClass.Service.None)
		{
			for (int i = 0; i < 16; i++)
			{
				income += this.m_totalIncome[i];
				expenses += this.m_totalExpenses[i];
			}
		}
		else if (privateServiceIndex != -1 && level == ItemClass.Level.None)
		{
			this.GetIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level1, ref income, ref expenses);
			this.GetIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level2, ref income, ref expenses);
			this.GetIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level3, ref income, ref expenses);
			this.GetIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level4, ref income, ref expenses);
			this.GetIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level5, ref income, ref expenses);
		}
		else
		{
			int num = EconomyManager.ClassIndex(service, subService, level);
			if (num != -1)
			{
				for (int j = 0; j < 16; j++)
				{
					income += this.m_income[num * 17 + j];
					expenses += this.m_expenses[num * 17 + j];
				}
				if (subService == ItemClass.SubService.None)
				{
					switch (service)
					{
					case ItemClass.Service.Residential:
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialLow, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialHigh, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialLowEco, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialHighEco, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Commercial:
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialLow, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialHigh, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialLeisure, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialTourist, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialEco, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Industrial:
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialGeneric, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialForestry, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialFarming, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialOil, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialOre, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Natural:
					case ItemClass.Service.Unused2:
					case ItemClass.Service.Citizen:
					case ItemClass.Service.Tourism:
						IL_110:
						if (service != ItemClass.Service.PublicTransport)
						{
							return;
						}
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportBus, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportMetro, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTrain, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportShip, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportPlane, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTaxi, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTram, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportMonorail, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportCableCar, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Office:
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.OfficeGeneric, level, ref income, ref expenses);
						this.GetIncomeAndExpensesImpl(service, ItemClass.SubService.OfficeHightech, level, ref income, ref expenses);
						return;
					}
					goto IL_110;
				}
			}
		}
	}

	private void GetCurrentIncomeAndExpensesImpl(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, ref long income, ref long expenses)
	{
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(service);
		if (service == ItemClass.Service.None)
		{
			income += this.m_totalIncome[16];
			expenses += this.m_totalExpenses[16];
		}
		else if (privateServiceIndex != -1 && level == ItemClass.Level.None)
		{
			this.GetCurrentIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level1, ref income, ref expenses);
			this.GetCurrentIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level2, ref income, ref expenses);
			this.GetCurrentIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level3, ref income, ref expenses);
			this.GetCurrentIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level4, ref income, ref expenses);
			this.GetCurrentIncomeAndExpensesImpl(service, subService, ItemClass.Level.Level5, ref income, ref expenses);
		}
		else
		{
			int num = EconomyManager.ClassIndex(service, subService, level);
			if (num != -1)
			{
				income += this.m_income[num * 17 + 16];
				expenses += this.m_expenses[num * 17 + 16];
				if (subService == ItemClass.SubService.None)
				{
					switch (service)
					{
					case ItemClass.Service.Residential:
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialLow, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialHigh, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialLowEco, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.ResidentialHighEco, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Commercial:
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialLow, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialHigh, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialLeisure, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialTourist, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.CommercialEco, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Industrial:
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialGeneric, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialForestry, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialFarming, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialOil, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.IndustrialOre, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Natural:
					case ItemClass.Service.Unused2:
					case ItemClass.Service.Citizen:
					case ItemClass.Service.Tourism:
						IL_EE:
						if (service != ItemClass.Service.PublicTransport)
						{
							return;
						}
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportBus, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportMetro, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTrain, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportShip, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportPlane, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTaxi, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportTram, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportMonorail, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.PublicTransportCableCar, level, ref income, ref expenses);
						return;
					case ItemClass.Service.Office:
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.OfficeGeneric, level, ref income, ref expenses);
						this.GetCurrentIncomeAndExpensesImpl(service, ItemClass.SubService.OfficeHightech, level, ref income, ref expenses);
						return;
					}
					goto IL_EE;
				}
			}
		}
	}

	public int PeekResource(EconomyManager.Resource resource, int amount)
	{
		if (this.m_EconomyWrapper.OnPeekResource(resource, ref amount))
		{
			return amount;
		}
		switch (resource)
		{
		case EconomyManager.Resource.Construction:
		case EconomyManager.Resource.LoanAmount:
			goto IL_5D;
		case EconomyManager.Resource.Maintenance:
		case EconomyManager.Resource.LoanPayment:
		case EconomyManager.Resource.PolicyCost:
			return amount;
		case EconomyManager.Resource.PrivateIncome:
		case EconomyManager.Resource.CitizenIncome:
		case EconomyManager.Resource.TourismIncome:
		case EconomyManager.Resource.PublicIncome:
		case EconomyManager.Resource.RewardAmount:
			IL_43:
			switch (resource)
			{
			case EconomyManager.Resource.LandPrice:
			case EconomyManager.Resource.Landscaping:
				goto IL_5D;
			case EconomyManager.Resource.FeePayment:
				return amount;
			default:
				return 0;
			}
			break;
		}
		goto IL_43;
		IL_5D:
		return (this.m_cashAmount < (long)amount) ? 0 : amount;
	}

	public int FetchResource(EconomyManager.Resource resource, int amount, ItemClass itemClass)
	{
		return this.FetchResource(resource, amount, itemClass.m_service, itemClass.m_subService, itemClass.m_level);
	}

	public int FetchResource(EconomyManager.Resource resource, int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		int result = this.PeekResource(resource, amount);
		this.m_EconomyWrapper.OnFetchResource(resource, ref amount, service, subService, level);
		switch (resource)
		{
		case EconomyManager.Resource.Construction:
		case EconomyManager.Resource.LoanAmount:
			goto IL_64;
		case EconomyManager.Resource.Maintenance:
		{
			int num = EconomyManager.ClassIndex(service, subService, level);
			if (num != -1)
			{
				this.m_expenses[num * 17 + 16] += (long)amount;
			}
			this.m_cashAmount -= (long)amount;
			this.m_cashDelta -= (long)amount;
			return result;
		}
		case EconomyManager.Resource.LoanPayment:
			this.m_loanExpenses[16] += (long)amount;
			this.m_cashAmount -= (long)amount;
			this.m_cashDelta -= (long)amount;
			return result;
		case EconomyManager.Resource.PrivateIncome:
		case EconomyManager.Resource.CitizenIncome:
		case EconomyManager.Resource.TourismIncome:
		case EconomyManager.Resource.PublicIncome:
		case EconomyManager.Resource.RewardAmount:
			IL_4A:
			switch (resource)
			{
			case EconomyManager.Resource.LandPrice:
			case EconomyManager.Resource.Landscaping:
			case EconomyManager.Resource.FeePayment:
				goto IL_64;
			default:
				return result;
			}
			break;
		case EconomyManager.Resource.PolicyCost:
			this.m_policyExpenses[16] += (long)amount;
			this.m_cashAmount -= (long)amount;
			this.m_cashDelta -= (long)amount;
			return result;
		}
		goto IL_4A;
		IL_64:
		this.m_cashAmount -= (long)amount;
		this.m_cashDelta -= (long)amount;
		this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_lastCashAmount - (long)amount);
		return result;
	}

	public int AddResource(EconomyManager.Resource resource, int amount, ItemClass itemClass)
	{
		return this.AddResource(resource, amount, itemClass.m_service, itemClass.m_subService, itemClass.m_level, DistrictPolicies.Taxation.None);
	}

	public int AddResource(EconomyManager.Resource resource, int amount, ItemClass itemClass, DistrictPolicies.Taxation taxationPolicies)
	{
		return this.AddResource(resource, amount, itemClass.m_service, itemClass.m_subService, itemClass.m_level, taxationPolicies);
	}

	public int AddResource(EconomyManager.Resource resource, int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		return this.AddResource(resource, amount, service, subService, level, DistrictPolicies.Taxation.None);
	}

	public int AddResource(EconomyManager.Resource resource, int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, DistrictPolicies.Taxation taxationPolicies)
	{
		switch (resource)
		{
		case EconomyManager.Resource.LoanAmount:
		case EconomyManager.Resource.RewardAmount:
		case EconomyManager.Resource.BailoutAmount:
		case EconomyManager.Resource.RefundAmount:
			this.m_EconomyWrapper.OnAddResource(resource, ref amount, service, subService, level);
			this.m_cashAmount += (long)amount;
			this.m_cashDelta += (long)amount;
			this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_lastCashAmount + (long)amount);
			return amount;
		case EconomyManager.Resource.PrivateIncome:
		{
			int taxRate = this.GetTaxRate(service, subService, level, taxationPolicies);
			return this.AddPrivateIncome(amount, service, subService, level, taxRate);
		}
		case EconomyManager.Resource.CitizenIncome:
		{
			this.m_EconomyWrapper.OnAddResource(resource, ref amount, service, subService, level);
			int num = EconomyManager.ClassIndex(ItemClass.Service.Citizen, ItemClass.SubService.None, level);
			if (num != -1)
			{
				this.m_income[num * 17 + 16] += (long)amount;
			}
			return amount;
		}
		case EconomyManager.Resource.TourismIncome:
		{
			this.m_EconomyWrapper.OnAddResource(resource, ref amount, service, subService, level);
			int num2 = EconomyManager.ClassIndex(ItemClass.Service.Tourism, ItemClass.SubService.None, level);
			if (num2 != -1)
			{
				this.m_income[num2 * 17 + 16] += (long)amount;
			}
			return amount;
		}
		case EconomyManager.Resource.PublicIncome:
		{
			this.m_EconomyWrapper.OnAddResource(resource, ref amount, service, subService, level);
			int num3 = EconomyManager.ClassIndex(service, subService, level);
			if (num3 != -1)
			{
				this.m_income[num3 * 17 + 16] += (long)amount;
			}
			this.m_cashAmount += (long)amount;
			this.m_cashDelta += (long)amount;
			return amount;
		}
		}
		this.m_EconomyWrapper.OnAddResource(resource, ref amount, service, subService, level);
		return amount;
	}

	public int AddPrivateIncome(int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, int taxRate)
	{
		this.m_EconomyWrapper.OnAddResource(EconomyManager.Resource.PrivateIncome, ref amount, service, subService, level);
		amount = (int)(((long)amount * (long)taxRate * (long)this.m_taxMultiplier + 999999L) / 1000000L);
		int num = EconomyManager.ClassIndex(service, subService, level);
		if (num != -1)
		{
			this.m_income[num * 17 + 16] += (long)amount;
		}
		this.m_cashAmount += (long)amount;
		this.m_cashDelta += (long)amount;
		return amount;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint num = currentFrameIndex >> 8 & 15u;
			uint num2 = currentFrameIndex & 255u;
			if (num2 == 255u)
			{
				long num3 = 0L;
				for (int i = 0; i < 3; i++)
				{
					EconomyManager.Loan loan = this.m_loans[i];
					if (loan.m_length != 0)
					{
						int num4 = 0;
						if (loan.m_amountTaken != 0)
						{
							num4 = (int)(((long)loan.m_amountLeft * (long)(loan.m_length * 16) + (long)(loan.m_amountTaken >> 1)) / (long)loan.m_amountTaken);
						}
						if (num4 == 0)
						{
							num4 = 1;
						}
						int num5 = (int)(((long)loan.m_amountTaken * (long)loan.m_interestRate + 5000L) / 10000L);
						int num6 = num5 - loan.m_interestPaid;
						if (num6 < 0)
						{
							num6 = 0;
						}
						int num7 = (loan.m_amountLeft + num6 + Mathf.Max(0, (num4 * (int)(num + 1u) >> 4) - 1)) / num4;
						int num8 = (num6 + Mathf.Max(0, (num4 * (int)(num + 1u) >> 4) - 1)) / num4;
						num7 = this.FetchResource(EconomyManager.Resource.LoanPayment, num7, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
						if (num7 < num8)
						{
							num8 = num7;
						}
						int num9 = num7 - num8;
						if (num9 > 0)
						{
							loan.m_amountLeft -= num9;
						}
						if (num8 > 0)
						{
							loan.m_interestPaid += num8;
						}
						if (loan.m_amountLeft > 0)
						{
							num3 += (long)loan.m_amountLeft;
							this.m_loans[i] = loan;
						}
						else
						{
							this.m_loans[i] = default(EconomyManager.Loan);
						}
					}
				}
				int num10 = EconomyManager.ClassIndex(ItemClass.Service.Citizen, ItemClass.SubService.None, ItemClass.Level.Level1);
				int num11 = EconomyManager.ClassIndex(ItemClass.Service.Citizen, ItemClass.SubService.None, ItemClass.Level.Level5);
				int num12 = EconomyManager.ClassIndex(ItemClass.Service.Tourism, ItemClass.SubService.None, ItemClass.Level.Level1);
				int num13 = EconomyManager.ClassIndex(ItemClass.Service.Tourism, ItemClass.SubService.None, ItemClass.Level.Level5);
				StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
				StatisticBase statisticBase = instance.Acquire<StatisticArray>(StatisticType.ServiceIncome);
				StatisticBase statisticBase2 = instance.Acquire<StatisticArray>(StatisticType.ServiceExpenses);
				int num14 = 20;
				for (int j = 0; j < num14; j++)
				{
					long delta = 0L;
					long delta2 = 0L;
					this.GetCurrentIncomeAndExpensesImpl(j + ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.None, ref delta, ref delta2);
					statisticBase.Acquire<StatisticInt64>(j, num14).Add(delta);
					statisticBase2.Acquire<StatisticInt64>(j, num14).Add(delta2);
				}
				for (int k = 0; k < 142; k++)
				{
					if ((k < num10 || k > num11) && (k < num12 || k > num13))
					{
						this.m_totalIncome[16] += this.m_income[k * 17 + 16];
						this.m_totalExpenses[16] += this.m_expenses[k * 17 + 16];
					}
					this.m_income[(int)(checked((IntPtr)(unchecked((long)(k * 17) + (long)((ulong)num)))))] = this.m_income[k * 17 + 16];
					this.m_income[k * 17 + 16] = 0L;
					this.m_expenses[(int)(checked((IntPtr)(unchecked((long)(k * 17) + (long)((ulong)num)))))] = this.m_expenses[k * 17 + 16];
					this.m_expenses[k * 17 + 16] = 0L;
				}
				this.m_totalExpenses[16] += this.m_loanExpenses[16];
				this.m_loanExpenses[(int)((UIntPtr)num)] = this.m_loanExpenses[16];
				this.m_loanExpenses[16] = 0L;
				this.m_totalExpenses[16] += this.m_policyExpenses[16];
				this.m_policyExpenses[(int)((UIntPtr)num)] = this.m_policyExpenses[16];
				this.m_policyExpenses[16] = 0L;
				this.m_totalIncome[(int)((UIntPtr)num)] = this.m_totalIncome[16];
				this.m_totalIncome[16] = 0L;
				this.m_totalExpenses[(int)((UIntPtr)num)] = this.m_totalExpenses[16];
				this.m_totalExpenses[16] = 0L;
				long num15 = 0L;
				long num16 = 0L;
				for (int l = 0; l < 16; l++)
				{
					num15 += this.m_totalIncome[l];
					num16 += this.m_totalExpenses[l];
				}
				this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_cashAmount);
				this.m_lastCashDelta = num15 - num16;
				this.m_cashDelta = 0L;
				long num19;
				if (num16 != 0L)
				{
					long num17 = 200000L;
					long num18 = (this.m_lastCashDelta - num17) * 20000L / num16;
					if (num18 > 0L)
					{
						num19 = 5000L + 50000000L / (num18 + 10000L);
					}
					else if (num18 < 0L)
					{
						num19 = 20000L + 200000000L / (num18 - 20000L);
					}
					else
					{
						num19 = 10000L;
					}
					long num20 = this.m_cashAmount;
					long num21 = (long)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_populationData.m_finalCount;
					if (num20 < 0L)
					{
						num20 = 0L;
					}
					if (num21 < 0L)
					{
						num21 = 0L;
					}
					num19 = num19 * 1000000000L / (1000000000L + num20);
					num19 = num19 * 1000000L / (1000000L + num21);
					if (num19 < 100L)
					{
						num19 = 100L;
					}
					else if (num19 > 30000L)
					{
						num19 = 30000L;
					}
				}
				else
				{
					num19 = 10000L;
				}
				num19 = ((long)this.m_taxMultiplier * 99L + num19) / 100L;
				this.m_taxMultiplier = (int)num19;
				StatisticBase statisticBase3 = instance.Acquire<StatisticInt64>(StatisticType.PlayerMoney);
				statisticBase3.Set(this.m_lastCashAmount);
				StatisticBase statisticBase4 = instance.Acquire<StatisticInt64>(StatisticType.PlayerDebt);
				statisticBase4.Set(num3);
				if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None && this.m_lastCashAmount <= (long)this.m_properties.m_bailoutLimit && num15 < num16 && this.m_bailoutAccepted < this.m_properties.m_maxBailoutCount && !this.m_bailoutShowing && (ulong)this.m_lastBailoutFrame + (ulong)((long)((long)this.m_properties.m_bailoutRepeatWeeks << 12)) < (ulong)currentFrameIndex)
				{
					this.m_lastBailoutFrame = currentFrameIndex;
					this.m_bailoutShowing = true;
					ThreadHelper.get_dispatcher().Dispatch(delegate
					{
						this.ShowBailoutWindow();
					});
				}
			}
		}
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (subStep <= 1 && properties != null)
		{
			int num22 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
			if (num22 != 130)
			{
				if (num22 == 236)
				{
					if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Loans))
					{
						bool flag = this.m_lastCashAmount <= -100000L;
						if (flag)
						{
							int num23 = this.CountLoans();
							if (num23 >= 3)
							{
								flag = false;
							}
							else if (num23 == 2)
							{
								flag = Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.ThirdLoan);
							}
							else if (num23 == 1)
							{
								flag = Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.SecondLoan);
							}
						}
						if (flag)
						{
							this.m_loanNeededGuide.Activate(properties.m_loanNeededGuide);
						}
						else
						{
							this.m_loanNeededGuide.Deactivate();
						}
					}
				}
			}
			else if (Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.Economy))
			{
				this.m_economyNotUsedGuide.Activate(properties.m_economyNotUsed);
			}
		}
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new EconomyManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("EconomyManager.UpdateData");
		base.UpdateData(mode);
		if (mode == SimulationManager.UpdateMode.NewMap || mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || mode == SimulationManager.UpdateMode.NewAsset)
		{
			this.m_cashAmount = this.m_startMoney;
			this.m_cashDelta = 0L;
			this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_cashAmount);
			this.m_lastCashDelta = this.m_cashDelta;
			for (int i = 0; i < this.m_taxRates.Length; i++)
			{
				this.m_taxRates[i] = 9;
			}
			for (int j = 0; j < this.m_serviceBudgetDay.Length; j++)
			{
				this.m_serviceBudgetDay[j] = 100;
				this.m_serviceBudgetNight[j] = 100;
			}
			for (int k = 0; k < this.m_expenses.Length; k++)
			{
				this.m_expenses[k] = 0L;
			}
			for (int l = 0; l < this.m_loanExpenses.Length; l++)
			{
				this.m_loanExpenses[l] = 0L;
			}
			for (int m = 0; m < this.m_policyExpenses.Length; m++)
			{
				this.m_policyExpenses[m] = 0L;
			}
			for (int n = 0; n < this.m_totalExpenses.Length; n++)
			{
				this.m_totalExpenses[n] = 0L;
			}
			for (int num = 0; num < this.m_income.Length; num++)
			{
				this.m_income[num] = 0L;
			}
			for (int num2 = 0; num2 < this.m_totalIncome.Length; num2++)
			{
				this.m_totalIncome[num2] = 0L;
			}
			for (int num3 = 0; num3 < this.m_loans.Length; num3++)
			{
				this.m_loans[num3] = default(EconomyManager.Loan);
			}
			this.m_taxMultiplier = 10000;
		}
		else if (mode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.m_cashAmount = this.m_startMoney;
			this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_cashAmount);
			StatisticsManager instance = Singleton<StatisticsManager>.get_instance();
			StatisticBase statisticBase = instance.Acquire<StatisticInt64>(StatisticType.PlayerMoney);
			statisticBase.Set(this.m_lastCashAmount);
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_economyNotUsedGuide == null)
		{
			this.m_economyNotUsedGuide = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_loanNeededGuide == null)
		{
			this.m_loanNeededGuide = new GenericGuide();
		}
		if (this.m_bailoutAccepted != 0)
		{
			Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements = SimulationMetaData.MetaBool.True;
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	private static int PrivateClassIndex(ItemClass itemClass)
	{
		return EconomyManager.PrivateClassIndex(itemClass.m_service, itemClass.m_subService, itemClass.m_level);
	}

	private static int PrivateClassIndex(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(service);
		int privateSubServiceIndex = ItemClass.GetPrivateSubServiceIndex(subService);
		if (privateServiceIndex == -1)
		{
			return -1;
		}
		int num;
		if (privateSubServiceIndex != -1)
		{
			num = 8 + privateSubServiceIndex;
		}
		else
		{
			num = privateServiceIndex;
		}
		num *= 5;
		if (level != ItemClass.Level.None)
		{
			num = (int)(num + level);
		}
		return num;
	}

	private static int PublicClassIndex(ItemClass itemClass)
	{
		return EconomyManager.PublicClassIndex(itemClass.m_service, itemClass.m_subService);
	}

	private static int PublicClassIndex(ItemClass.Service service, ItemClass.SubService subService)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		int publicSubServiceIndex = ItemClass.GetPublicSubServiceIndex(subService);
		if (publicServiceIndex == -1)
		{
			return -1;
		}
		int result;
		if (publicSubServiceIndex != -1)
		{
			result = 12 + publicSubServiceIndex;
		}
		else
		{
			result = publicServiceIndex;
		}
		return result;
	}

	private static int ClassIndex(ItemClass itemClass)
	{
		return EconomyManager.ClassIndex(itemClass.m_service, itemClass.m_subService, itemClass.m_level);
	}

	private static int ClassIndex(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(service);
		if (privateServiceIndex != -1)
		{
			return EconomyManager.PrivateClassIndex(service, subService, level);
		}
		return EconomyManager.PublicClassIndex(service, subService) + 120;
	}

	public long StartMoney
	{
		get
		{
			return this.m_startMoney;
		}
		set
		{
			this.m_startMoney = value;
		}
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	public const int PrivateClassCount = 120;

	public const int PublicClassCount = 22;

	public const int TotalClassCount = 142;

	public const int MAX_LOANS = 3;

	private int[] m_taxRates;

	private int[] m_serviceBudgetDay;

	private int[] m_serviceBudgetNight;

	private int m_taxMultiplier;

	private long[] m_expenses;

	private long[] m_loanExpenses;

	private long[] m_policyExpenses;

	private long[] m_totalExpenses;

	private long[] m_income;

	private long[] m_totalIncome;

	private EconomyManager.Loan[] m_loans;

	private long m_startMoney;

	private long m_cashAmount;

	private long m_cashDelta;

	private long m_lastCashAmount;

	private long m_lastCashDelta;

	private uint m_lastBailoutFrame;

	private int m_bailoutAccepted;

	private bool m_bailoutShowing;

	[NonSerialized]
	public EconomyWrapper m_EconomyWrapper;

	[NonSerialized]
	public GenericGuide m_economyNotUsedGuide;

	[NonSerialized]
	public GenericGuide m_loanNeededGuide;

	public enum Resource
	{
		Construction,
		Maintenance,
		LoanAmount,
		LoanPayment,
		PrivateIncome,
		CitizenIncome,
		TourismIncome,
		PublicIncome,
		RewardAmount,
		PolicyCost,
		BailoutAmount,
		RefundAmount,
		LandPrice,
		Landscaping,
		FeePayment
	}

	public struct Loan
	{
		public int m_amountTaken;

		public int m_amountLeft;

		public int m_interestRate;

		public int m_interestPaid;

		public int m_length;
	}

	[Serializable]
	public struct Bank
	{
		public string m_bankName;

		public EconomyManager.LoanInfo[] m_loanOffers;
	}

	[Serializable]
	public struct LoanInfo
	{
		public int m_amount;

		public int m_interest;

		public int m_length;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "EconomyManager");
			EconomyManager instance = Singleton<EconomyManager>.get_instance();
			int num = instance.m_taxRates.Length;
			int num2 = instance.m_serviceBudgetDay.Length;
			int num3 = instance.m_income.Length;
			int num4 = instance.m_expenses.Length;
			int num5 = instance.m_loanExpenses.Length;
			int num6 = instance.m_policyExpenses.Length;
			int num7 = instance.m_totalIncome.Length;
			int num8 = instance.m_totalExpenses.Length;
			int num9 = instance.m_loans.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				@byte.Write((byte)instance.m_taxRates[i]);
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int j = 0; j < num2; j++)
			{
				byte2.Write((byte)instance.m_serviceBudgetDay[j]);
				byte2.Write((byte)instance.m_serviceBudgetNight[j]);
			}
			byte2.EndWrite();
			EncodedArray.Long @long = EncodedArray.Long.BeginWrite(s);
			for (int k = 0; k < num3; k++)
			{
				@long.Write(instance.m_income[k]);
			}
			for (int l = 0; l < num7; l++)
			{
				@long.Write(instance.m_totalIncome[l]);
			}
			@long.EndWrite();
			EncodedArray.Long long2 = EncodedArray.Long.BeginWrite(s);
			for (int m = 0; m < num4; m++)
			{
				long2.Write(instance.m_expenses[m]);
			}
			for (int n = 0; n < num5; n++)
			{
				long2.Write(instance.m_loanExpenses[n]);
			}
			for (int num10 = 0; num10 < num6; num10++)
			{
				long2.Write(instance.m_policyExpenses[num10]);
			}
			for (int num11 = 0; num11 < num8; num11++)
			{
				long2.Write(instance.m_totalExpenses[num11]);
			}
			long2.EndWrite();
			for (int num12 = 0; num12 < num9; num12++)
			{
				s.WriteInt32(instance.m_loans[num12].m_amountTaken);
				s.WriteInt32(instance.m_loans[num12].m_amountLeft);
				s.WriteInt16(instance.m_loans[num12].m_interestRate);
				s.WriteInt16(instance.m_loans[num12].m_length);
				s.WriteInt32(instance.m_loans[num12].m_interestPaid);
			}
			s.WriteLong64(instance.m_startMoney);
			s.WriteLong64(instance.m_cashAmount);
			s.WriteLong64(instance.m_cashDelta);
			s.WriteLong64(instance.m_lastCashAmount);
			s.WriteLong64(instance.m_lastCashDelta);
			s.WriteInt16(instance.m_taxMultiplier);
			s.WriteInt16(instance.m_bailoutAccepted);
			s.WriteUInt32(instance.m_lastBailoutFrame);
			s.WriteObject<GenericGuide>(instance.m_economyNotUsedGuide);
			s.WriteObject<GenericGuide>(instance.m_loanNeededGuide);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "EconomyManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "EconomyManager");
			EconomyManager instance = Singleton<EconomyManager>.get_instance();
			int num = instance.m_taxRates.Length;
			int num2 = instance.m_serviceBudgetDay.Length;
			int num3 = instance.m_loanExpenses.Length;
			int num4 = instance.m_policyExpenses.Length;
			int num5 = instance.m_totalIncome.Length;
			int num6 = instance.m_totalExpenses.Length;
			int num7 = instance.m_loans.Length;
			int num8 = 2040;
			int num9 = 374;
			int num10 = 2040;
			int num11 = 374;
			if (s.get_version() < 109000u)
			{
				num = 95;
				num10 = 1615;
			}
			if (s.get_version() < 310u)
			{
				num2 = 20;
				num11 = 340;
			}
			if (s.get_version() < 213u)
			{
				num = 85;
				num10 = 1445;
			}
			if (s.get_version() < 211u)
			{
				num2 = 17;
				num11 = 289;
			}
			if (s.get_version() < 102u)
			{
				num5 = 0;
				num6 = 0;
			}
			if (s.get_version() < 101u)
			{
				num3 = 0;
			}
			if (s.get_version() < 150u)
			{
				num4 = 0;
			}
			if (s.get_version() < 79u)
			{
				num2 = 15;
				num11 = 255;
			}
			if (s.get_version() < 53u)
			{
				num = 80;
				num2 = 9;
			}
			for (int i = num2; i < instance.m_serviceBudgetDay.Length; i++)
			{
				instance.m_serviceBudgetDay[i] = 100;
				instance.m_serviceBudgetNight[i] = 100;
			}
			for (int j = num10; j < num8; j++)
			{
				instance.m_income[j] = 0L;
				instance.m_expenses[j] = 0L;
			}
			for (int k = num11; k < num9; k++)
			{
				instance.m_income[num8 + k] = 0L;
				instance.m_expenses[num8 + k] = 0L;
			}
			for (int l = num; l < instance.m_taxRates.Length; l++)
			{
				instance.m_taxRates[l] = 9;
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int m = 0; m < num; m++)
			{
				instance.m_taxRates[m] = (int)@byte.Read();
			}
			@byte.EndRead();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
			if (s.get_version() >= 214u)
			{
				for (int n = 0; n < num2; n++)
				{
					instance.m_serviceBudgetDay[n] = (int)byte2.Read();
					instance.m_serviceBudgetNight[n] = (int)byte2.Read();
				}
			}
			else
			{
				for (int num12 = 0; num12 < num2; num12++)
				{
					instance.m_serviceBudgetDay[num12] = (int)byte2.Read();
					instance.m_serviceBudgetNight[num12] = instance.m_serviceBudgetDay[num12];
				}
			}
			byte2.EndRead();
			if (s.get_version() >= 54u)
			{
				EncodedArray.Long @long = EncodedArray.Long.BeginRead(s);
				for (int num13 = 0; num13 < num10; num13++)
				{
					instance.m_income[num13] = @long.Read();
				}
				for (int num14 = 0; num14 < num11; num14++)
				{
					instance.m_income[num8 + num14] = @long.Read();
				}
				for (int num15 = 0; num15 < num5; num15++)
				{
					instance.m_totalIncome[num15] = @long.Read();
				}
				@long.EndRead();
			}
			else
			{
				for (int num16 = 0; num16 < num8 + num9; num16++)
				{
					instance.m_income[num16] = 0L;
				}
			}
			if (s.get_version() >= 54u)
			{
				EncodedArray.Long long2 = EncodedArray.Long.BeginRead(s);
				for (int num17 = 0; num17 < num10; num17++)
				{
					instance.m_expenses[num17] = long2.Read();
				}
				for (int num18 = 0; num18 < num11; num18++)
				{
					instance.m_expenses[num8 + num18] = long2.Read();
				}
				for (int num19 = 0; num19 < num3; num19++)
				{
					instance.m_loanExpenses[num19] = long2.Read();
				}
				for (int num20 = 0; num20 < num4; num20++)
				{
					instance.m_policyExpenses[num20] = long2.Read();
				}
				for (int num21 = 0; num21 < num6; num21++)
				{
					instance.m_totalExpenses[num21] = long2.Read();
				}
				long2.EndRead();
			}
			else
			{
				for (int num22 = 0; num22 < num8 + num9; num22++)
				{
					instance.m_expenses[num22] = 0L;
				}
			}
			if (s.get_version() >= 54u)
			{
				for (int num23 = 0; num23 < num7; num23++)
				{
					instance.m_loans[num23].m_amountTaken = s.ReadInt32();
					instance.m_loans[num23].m_amountLeft = s.ReadInt32();
					instance.m_loans[num23].m_interestRate = s.ReadInt16();
					instance.m_loans[num23].m_length = s.ReadInt16();
					if (s.get_version() >= 193u)
					{
						instance.m_loans[num23].m_interestPaid = s.ReadInt32();
					}
					else
					{
						instance.m_loans[num23].m_interestPaid = (int)(((long)(instance.m_loans[num23].m_amountTaken - instance.m_loans[num23].m_amountLeft) * (long)instance.m_loans[num23].m_interestRate + 5000L) / 10000L);
					}
				}
			}
			else
			{
				for (int num24 = 0; num24 < num7; num24++)
				{
					instance.m_loans[num24] = default(EconomyManager.Loan);
				}
			}
			SimulationManager.UpdateMode updateMode = Singleton<SimulationManager>.get_instance().m_metaData.m_updateMode;
			if (updateMode != SimulationManager.UpdateMode.UpdateScenarioFromGame && updateMode != SimulationManager.UpdateMode.UpdateScenarioFromMap)
			{
				if (s.get_version() >= 287u)
				{
					instance.m_startMoney = s.ReadLong64();
				}
				else
				{
					instance.m_startMoney = 7000000L;
				}
			}
			else if (s.get_version() >= 287u)
			{
				s.ReadLong64();
			}
			instance.m_cashAmount = s.ReadLong64();
			instance.m_cashDelta = s.ReadLong64();
			instance.m_lastCashAmount = s.ReadLong64();
			instance.m_lastCashDelta = s.ReadLong64();
			if (s.get_version() >= 196u)
			{
				instance.m_taxMultiplier = s.ReadInt16();
			}
			else if (s.get_version() >= 106u)
			{
				instance.m_taxMultiplier = s.ReadInt16() * 100;
			}
			else
			{
				instance.m_taxMultiplier = 10000;
			}
			if (s.get_version() >= 152u)
			{
				instance.m_bailoutAccepted = s.ReadInt16();
			}
			else
			{
				instance.m_bailoutAccepted = 0;
			}
			if (s.get_version() >= 159u)
			{
				instance.m_lastBailoutFrame = s.ReadUInt32();
			}
			else
			{
				instance.m_lastBailoutFrame = 0u;
			}
			if (s.get_version() >= 94u)
			{
				instance.m_economyNotUsedGuide = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_economyNotUsedGuide = null;
			}
			if (s.get_version() >= 166u)
			{
				instance.m_loanNeededGuide = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_loanNeededGuide = null;
			}
			if (s.get_version() < 109000u)
			{
				for (int num25 = 0; num25 < 5; num25++)
				{
					int num26 = EconomyManager.PrivateClassIndex(ItemClass.Service.Office, ItemClass.SubService.None, (ItemClass.Level)num25);
					int num27 = EconomyManager.PrivateClassIndex(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, (ItemClass.Level)num25);
					if (num26 != -1 && num27 != -1)
					{
						instance.m_taxRates[num27] = instance.m_taxRates[num26];
						for (int num28 = 0; num28 < 17; num28++)
						{
							instance.m_income[num27 * 17 + num28] = instance.m_income[num26 * 17 + num28];
							instance.m_income[num26 * 17 + num28] = 0L;
						}
					}
					num27 = EconomyManager.PrivateClassIndex(ItemClass.Service.Office, ItemClass.SubService.OfficeHightech, (ItemClass.Level)num25);
					if (num26 != -1 && num27 != -1)
					{
						instance.m_taxRates[num27] = instance.m_taxRates[num26];
					}
					num26 = EconomyManager.PrivateClassIndex(ItemClass.Service.Commercial, ItemClass.SubService.None, (ItemClass.Level)num25);
					num27 = EconomyManager.PrivateClassIndex(ItemClass.Service.Commercial, ItemClass.SubService.CommercialEco, (ItemClass.Level)num25);
					if (num26 != -1 && num27 != -1)
					{
						instance.m_taxRates[num27] = instance.m_taxRates[num26];
					}
					num26 = EconomyManager.PrivateClassIndex(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, (ItemClass.Level)num25);
					num27 = EconomyManager.PrivateClassIndex(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, (ItemClass.Level)num25);
					if (num26 != -1 && num27 != -1)
					{
						instance.m_taxRates[num27] = instance.m_taxRates[num26];
					}
					num26 = EconomyManager.PrivateClassIndex(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, (ItemClass.Level)num25);
					num27 = EconomyManager.PrivateClassIndex(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, (ItemClass.Level)num25);
					if (num26 != -1 && num27 != -1)
					{
						instance.m_taxRates[num27] = instance.m_taxRates[num26];
					}
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "EconomyManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "EconomyManager");
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "EconomyManager");
		}
	}
}
