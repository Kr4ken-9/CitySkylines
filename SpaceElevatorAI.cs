﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class SpaceElevatorAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.NoisePollution;
		radius1 = this.m_noiseRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Connections)
		{
			if (Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.WindPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
		else if (infoMode == InfoManager.InfoMode.Transport)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		data.m_flags |= Building.Flags.IncomingOutgoing;
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, 0, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_publicTransportAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (this.m_publicTransportAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PublicTransport, (float)this.m_publicTransportAccumulation, this.m_publicTransportRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			if (this.m_publicTransportAccumulation != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (this.m_publicTransportAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.PublicTransport, (float)(-(float)this.m_publicTransportAccumulation), this.m_publicTransportRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	public override void PathfindSuccess(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		OutsideConnectionAI.PathfindSuccessImpl(buildingID, ref data, type);
		if (type == BuildingAI.PathFindType.LeavingHuman)
		{
			data.m_customBuffer1 = (ushort)Mathf.Min(65535, (int)(data.m_customBuffer1 + 1));
		}
	}

	public override void PathfindFailure(ushort buildingID, ref Building data, BuildingAI.PathFindType type)
	{
		OutsideConnectionAI.PathfindFailureImpl(buildingID, ref data, type);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (!OutsideConnectionAI.StartConnectionTransfer(buildingID, ref data, material, offer, this.m_touristFactor0, this.m_touristFactor1, this.m_touristFactor2))
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		OutsideConnectionAI.RemoveConnectionOffers(buildingID, ref data, TransferManager.TransferReason.None);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_customBuffer2 = buildingData.m_customBuffer1;
			buildingData.m_customBuffer1 = 0;
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = (productionRate * 1000 + 99) / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Attractiveness, num);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num2 = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num2 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num2, buildingData.m_position, this.m_noiseRadius);
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
		OutsideConnectionAI.AddConnectionOffers(buildingID, ref buildingData, finalProductionRate, 0, this.m_residentCapacity, this.m_touristFactor0, this.m_touristFactor1, this.m_touristFactor2, TransferManager.TransferReason.None, 0);
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool IsWonder()
	{
		return true;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			string.Empty
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string str = string.Empty;
		int customBuffer = (int)data.m_customBuffer2;
		str = str + LocaleFormatter.FormatGeneric("AIINFO_PASSENGERS_SERVICED", new object[]
		{
			customBuffer
		}) + Environment.NewLine;
		return str + base.GetLocalizedStats(buildingID, ref data);
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 3;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 18;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 21;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 18;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 150f;

	public int m_publicTransportAccumulation = 100;

	public float m_publicTransportRadius = 1500f;

	public int m_residentCapacity = 2000;

	public int m_touristFactor0 = 1000;

	public int m_touristFactor1 = 3000;

	public int m_touristFactor2 = 5000;
}
