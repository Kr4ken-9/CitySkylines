﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class RoadWorldInfoPanel : WorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_NameField = base.Find<UITextField>("RoadName");
		this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_PriorityRoad = base.Find<UICheckBox>("PriorityRoadCheckbox");
		this.m_PriorityRoad.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.OnPriorityChanged));
		this.m_Sprite = base.Find<UISprite>("Sprite");
		this.m_NetType = base.Find<UILabel>("NetType");
	}

	protected override void OnSetTarget()
	{
		this.m_NameField.set_text(this.GetName());
		if (this.m_InstanceID.Type == InstanceType.NetSegment && this.m_InstanceID.NetSegment != 0)
		{
			this.m_PriorityRoad.set_isVisible((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None || (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.m_vehicleTypes & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None);
			if (this.m_PriorityRoad.get_isVisible())
			{
				bool isEnabled;
				this.m_PriorityRoad.set_isChecked(Singleton<NetManager>.get_instance().IsPriorityRoad(this.m_InstanceID.NetSegment, out isEnabled));
				this.m_PriorityRoad.set_isEnabled(isEnabled);
			}
			bool isVisible = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.m_vehicleTypes != VehicleInfo.VehicleType.None || Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.m_hasPedestrianLanes;
			this.m_ShowHideRoutesButton.set_isVisible(isVisible);
			this.m_Sprite.set_isVisible(isVisible);
			this.m_NetType.set_text(this.GetNetTypeName(Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.get_gameObject().get_name()));
		}
		base.OnSetTarget();
	}

	private string GetNetTypeName(string gameObjectName)
	{
		if (Locale.Exists("NET_TITLE", gameObjectName))
		{
			return Locale.Get("NET_TITLE", gameObjectName);
		}
		string text = string.Empty;
		int i = gameObjectName.LastIndexOf(' ');
		if (i != -1)
		{
			text = gameObjectName.Substring(0, i);
			if (Locale.Exists("NET_TITLE", text))
			{
				return Locale.Get("NET_TITLE", text);
			}
		}
		i = gameObjectName.Length - 1;
		while (!char.IsUpper(gameObjectName[i]) && i > 0)
		{
			i--;
		}
		text = gameObjectName.Substring(0, i);
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace(" Elevated", string.Empty);
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace(" Tunnel", string.Empty);
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace(" Slope", string.Empty);
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace("Pedestrian Elevated Bicycle", "Pedestrian Pavement Bicycle");
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace("Pedestrian Slope Bicycle", "Pedestrian Pavement Bicycle");
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace("Pedestrian Slope", "Pedestrian Pavement");
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		text = gameObjectName.Replace("Pedestrian Elevated", "Pedestrian Pavement");
		if (Locale.Exists("NET_TITLE", text))
		{
			return Locale.Get("NET_TITLE", text);
		}
		for (i = gameObjectName.Length - 2; i > 0; i--)
		{
			text = gameObjectName.Substring(0, i);
			if (Locale.Exists("NET_TITLE", text))
			{
				return Locale.Get("NET_TITLE", text);
			}
		}
		return gameObjectName;
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
	}

	public void OnShowRoutesButton()
	{
		if (base.isRoutesViewOn)
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		}
		else
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.TrafficRoutes, InfoManager.SubInfoMode.Default);
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	private void OnPriorityChanged(UIComponent comp, bool isChecked)
	{
		base.StartCoroutine(this.SetPriority(isChecked));
	}

	[DebuggerHidden]
	private IEnumerator SetPriority(bool isPriorityRoad)
	{
		RoadWorldInfoPanel.<SetPriority>c__Iterator0 <SetPriority>c__Iterator = new RoadWorldInfoPanel.<SetPriority>c__Iterator0();
		<SetPriority>c__Iterator.isPriorityRoad = isPriorityRoad;
		<SetPriority>c__Iterator.$this = this;
		return <SetPriority>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		RoadWorldInfoPanel.<SetName>c__Iterator1 <SetName>c__Iterator = new RoadWorldInfoPanel.<SetName>c__Iterator1();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		string text = string.Empty;
		if (this.m_InstanceID.Type == InstanceType.NetSegment && this.m_InstanceID.NetSegment != 0)
		{
			text = Singleton<NetManager>.get_instance().GetSegmentName(this.m_InstanceID.NetSegment);
			if (string.IsNullOrEmpty(text))
			{
				text = this.GetNetTypeName(Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)this.m_InstanceID.NetSegment].Info.get_gameObject().get_name());
			}
		}
		return text;
	}

	public void OnAdjustRoadButton()
	{
		Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.TrafficRoutes, InfoManager.SubInfoMode.WindPower);
		base.Hide();
	}

	private UITextField m_NameField;

	private UICheckBox m_PriorityRoad;

	private UISprite m_Sprite;

	private UILabel m_NetType;
}
