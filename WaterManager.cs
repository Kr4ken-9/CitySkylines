﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class WaterManager : SimulationManagerBase<WaterManager, WaterProperties>, ISimulationManager, IRenderableManager
{
	public bool WaterMapVisible
	{
		get
		{
			return this.m_waterMapVisible;
		}
		set
		{
			if (this.m_waterMapVisible != value)
			{
				if (value && this.m_lastMapWasHeating)
				{
					this.AreaModified(0, 0, 255, 255);
					this.m_lastMapWasHeating = false;
				}
				this.m_waterMapVisible = value;
				this.UpdateWaterMapping();
			}
		}
	}

	public bool HeatingMapVisible
	{
		get
		{
			return this.m_heatingMapVisible;
		}
		set
		{
			if (this.m_heatingMapVisible != value)
			{
				if (value && !this.m_lastMapWasHeating)
				{
					this.AreaModified(0, 0, 255, 255);
					this.m_lastMapWasHeating = true;
				}
				this.m_heatingMapVisible = value;
				this.UpdateWaterMapping();
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_waterGrid = new WaterManager.Cell[65536];
		this.m_waterPulseGroups = new WaterManager.PulseGroup[1024];
		this.m_sewagePulseGroups = new WaterManager.PulseGroup[1024];
		this.m_heatingPulseGroups = new WaterManager.PulseGroup[1024];
		this.m_waterPulseUnits = new WaterManager.PulseUnit[32768];
		this.m_sewagePulseUnits = new WaterManager.PulseUnit[32768];
		this.m_heatingPulseUnits = new WaterManager.PulseUnit[32768];
		this.m_nodeData = new WaterManager.Node[32768];
		this.m_waterTexture = new Texture2D(256, 256, 4, false, true);
		this.m_waterTexture.set_filterMode(0);
		this.m_waterTexture.set_wrapMode(1);
		Shader.SetGlobalTexture("_WaterTexture", this.m_waterTexture);
		this.UpdateWaterMapping();
		this.m_modifiedX1 = 0;
		this.m_modifiedZ1 = 0;
		this.m_modifiedX2 = 255;
		this.m_modifiedZ2 = 255;
	}

	private void OnDestroy()
	{
		if (this.m_waterTexture != null)
		{
			Object.Destroy(this.m_waterTexture);
			this.m_waterTexture = null;
		}
	}

	public override void InitializeProperties(WaterProperties properties)
	{
		base.InitializeProperties(properties);
		GameObject gameObject = GameObject.FindGameObjectWithTag("UndergroundView");
		if (gameObject != null)
		{
			this.m_undergroundCamera = gameObject.GetComponent<Camera>();
		}
	}

	public override void DestroyProperties(WaterProperties properties)
	{
		if (this.m_properties == properties)
		{
			this.m_undergroundCamera = null;
		}
		base.DestroyProperties(properties);
	}

	private void UpdateWaterMapping()
	{
		if (this.m_undergroundCamera != null)
		{
			if (this.m_waterMapVisible || this.m_heatingMapVisible)
			{
				Camera expr_2D = this.m_undergroundCamera;
				expr_2D.set_cullingMask(expr_2D.get_cullingMask() | 1 << LayerMask.NameToLayer("WaterPipes"));
			}
			else
			{
				Camera expr_53 = this.m_undergroundCamera;
				expr_53.set_cullingMask(expr_53.get_cullingMask() & ~(1 << LayerMask.NameToLayer("WaterPipes")));
			}
		}
		Vector4 vector;
		vector.z = 0.000102124184f;
		vector.x = 0.5f;
		vector.y = 0.5f;
		vector.w = 0.00390625f;
		Shader.SetGlobalVector("_WaterMapping", vector);
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		if ((this.m_waterMapVisible || this.m_heatingMapVisible) && this.m_modifiedX2 >= this.m_modifiedX1 && this.m_modifiedZ2 >= this.m_modifiedZ1)
		{
			this.UpdateTexture();
		}
	}

	private void UpdateTexture()
	{
		while (!Monitor.TryEnter(this.m_waterGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		int modifiedX;
		int modifiedZ;
		int modifiedX2;
		int modifiedZ2;
		try
		{
			modifiedX = this.m_modifiedX1;
			modifiedZ = this.m_modifiedZ1;
			modifiedX2 = this.m_modifiedX2;
			modifiedZ2 = this.m_modifiedZ2;
			this.m_modifiedX1 = 10000;
			this.m_modifiedZ1 = 10000;
			this.m_modifiedX2 = -10000;
			this.m_modifiedZ2 = -10000;
		}
		finally
		{
			Monitor.Exit(this.m_waterGrid);
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = modifiedZ; i <= modifiedZ2; i++)
		{
			for (int j = modifiedX; j <= modifiedX2; j++)
			{
				WaterManager.Cell cell = this.m_waterGrid[i * 256 + j];
				ushort num = (!this.m_waterMapVisible) ? cell.m_closestPipeSegment2 : cell.m_closestPipeSegment;
				Color color;
				if (num != 0 && j != 0 && i != 0 && j != 255 && i != 255)
				{
					ushort startNode = instance.m_segments.m_buffer[(int)num].m_startNode;
					ushort endNode = instance.m_segments.m_buffer[(int)num].m_endNode;
					Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
					Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
					int num2 = Mathf.RoundToInt(position.x * 0.418300658f) + 2048;
					int num3 = Mathf.RoundToInt(position.z * 0.418300658f) + 2048;
					int num4 = Mathf.RoundToInt(position2.x * 0.418300658f) + 2048;
					int num5 = Mathf.RoundToInt(position2.z * 0.418300658f) + 2048;
					color.r = (float)(j * 16 + 8 - num2 + 128) * 0.003921569f;
					color.g = (float)(i * 16 + 8 - num3 + 128) * 0.003921569f;
					color.b = (float)(j * 16 + 8 - num4 + 128) * 0.003921569f;
					color.a = (float)(i * 16 + 8 - num5 + 128) * 0.003921569f;
				}
				else
				{
					color.r = 0f;
					color.g = 0f;
					color.b = 0f;
					color.a = 0f;
				}
				this.m_waterTexture.SetPixel(j, i, color);
			}
		}
		this.m_waterTexture.Apply();
	}

	public int TryDumpWater(ushort node, int rate, int max, byte waterPollution)
	{
		if (node == 0)
		{
			return 0;
		}
		int num = Mathf.Min(rate, 32767);
		WaterManager.Node node2 = this.m_nodeData[(int)node];
		if (node2.m_extraWaterPressure != 0)
		{
			int num2 = Mathf.Min(num - (int)node2.m_curWaterPressure, (int)node2.m_extraWaterPressure);
			if (num2 > 0)
			{
				node2.m_curWaterPressure += (ushort)num2;
				node2.m_extraWaterPressure -= (ushort)num2;
				rate -= num2;
			}
		}
		rate = Mathf.Max(0, Mathf.Min(Mathf.Min(rate, max), num - (int)node2.m_curWaterPressure));
		node2.m_curWaterPressure += (ushort)rate;
		if (rate != 0)
		{
			node2.m_pollution = waterPollution;
		}
		this.m_nodeData[(int)node] = node2;
		return rate;
	}

	public int TryDumpHeating(ushort node, int rate, int max)
	{
		if (node == 0)
		{
			return 0;
		}
		int num = Mathf.Min(rate, 32767);
		WaterManager.Node node2 = this.m_nodeData[(int)node];
		if (node2.m_extraHeatingPressure != 0)
		{
			int num2 = Mathf.Min(num - (int)node2.m_curHeatingPressure, (int)node2.m_extraHeatingPressure);
			if (num2 > 0)
			{
				node2.m_curHeatingPressure += (ushort)num2;
				node2.m_extraHeatingPressure -= (ushort)num2;
				rate -= num2;
			}
		}
		rate = Mathf.Max(0, Mathf.Min(Mathf.Min(rate, max), num - (int)node2.m_curHeatingPressure));
		node2.m_curHeatingPressure += (ushort)rate;
		this.m_nodeData[(int)node] = node2;
		return rate;
	}

	public int TryDumpSewage(Vector3 pos, int rate, int max)
	{
		if (max == 0)
		{
			return 0;
		}
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int result = 0;
		if (this.TryDumpSewageImpl(pos, num, num2, rate, max, ref result))
		{
			return result;
		}
		if (this.m_waterGrid[num2 * 256 + num].m_conductivity == 0)
		{
			return 0;
		}
		float num3 = ((float)num + 0.5f - 128f) * 38.25f;
		float num4 = ((float)num2 + 0.5f - 128f) * 38.25f;
		if (pos.z > num4 && num2 < 255)
		{
			if (this.TryDumpSewageImpl(pos, num, num2 + 1, rate, max, ref result))
			{
				return result;
			}
		}
		else if (pos.z < num4 && num2 > 0 && this.TryDumpSewageImpl(pos, num, num2 - 1, rate, max, ref result))
		{
			return result;
		}
		if (pos.x > num3 && num < 255)
		{
			if (this.TryDumpSewageImpl(pos, num + 1, num2, rate, max, ref result))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryDumpSewageImpl(pos, num + 1, num2 + 1, rate, max, ref result))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryDumpSewageImpl(pos, num + 1, num2 - 1, rate, max, ref result))
			{
				return result;
			}
		}
		else if (pos.x < num3 && num > 0)
		{
			if (this.TryDumpSewageImpl(pos, num - 1, num2, rate, max, ref result))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryDumpSewageImpl(pos, num - 1, num2 + 1, rate, max, ref result))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryDumpSewageImpl(pos, num - 1, num2 - 1, rate, max, ref result))
			{
				return result;
			}
		}
		return 0;
	}

	private bool TryDumpSewageImpl(Vector3 pos, int x, int z, int rate, int max, ref int result)
	{
		int num = z * 256 + x;
		WaterManager.Cell cell = this.m_waterGrid[num];
		if (cell.m_hasSewage)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment = cell.m_closestPipeSegment;
			ushort startNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_endNode;
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & flags2 & NetNode.Flags.Sewage) != NetNode.Flags.None)
			{
				Segment2 segment;
				segment.a = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)startNode].m_position);
				segment.b = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)endNode].m_position);
				float num2;
				if ((double)segment.DistanceSqr(VectorUtils.XZ(pos), ref num2) < 9025.0)
				{
					rate = Mathf.Min(Mathf.Min(rate, max), 32768 + (int)cell.m_currentSewagePressure);
					cell.m_currentSewagePressure -= (short)rate;
					this.m_waterGrid[num] = cell;
					result = rate;
					return true;
				}
			}
		}
		return false;
	}

	public int TryDumpSewage(ushort node, int rate, int max)
	{
		if (node == 0)
		{
			return 0;
		}
		WaterManager.Node node2 = this.m_nodeData[(int)node];
		int num = Mathf.Min(rate, 32767);
		int num2 = Mathf.Min(Mathf.Min(rate, max), (int)node2.m_extraSewagePressure);
		max -= num2;
		rate = Mathf.Max(0, Mathf.Min(Mathf.Min(rate, max), num - (int)node2.m_collectSewagePressure));
		node2.m_collectSewagePressure += (ushort)rate;
		node2.m_extraSewagePressure -= (ushort)num2;
		this.m_nodeData[(int)node] = node2;
		return num2;
	}

	public int TryFetchWater(Vector3 pos, int rate, int max, ref byte waterPollution)
	{
		if (max == 0)
		{
			return 0;
		}
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int result = 0;
		if (this.TryFetchWaterImpl(pos, num, num2, rate, max, ref result, ref waterPollution))
		{
			return result;
		}
		if (this.m_waterGrid[num2 * 256 + num].m_conductivity == 0)
		{
			return 0;
		}
		float num3 = ((float)num + 0.5f - 128f) * 38.25f;
		float num4 = ((float)num2 + 0.5f - 128f) * 38.25f;
		if (pos.z > num4 && num2 < 255)
		{
			if (this.TryFetchWaterImpl(pos, num, num2 + 1, rate, max, ref result, ref waterPollution))
			{
				return result;
			}
		}
		else if (pos.z < num4 && num2 > 0 && this.TryFetchWaterImpl(pos, num, num2 - 1, rate, max, ref result, ref waterPollution))
		{
			return result;
		}
		if (pos.x > num3 && num < 255)
		{
			if (this.TryFetchWaterImpl(pos, num + 1, num2, rate, max, ref result, ref waterPollution))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryFetchWaterImpl(pos, num + 1, num2 + 1, rate, max, ref result, ref waterPollution))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryFetchWaterImpl(pos, num + 1, num2 - 1, rate, max, ref result, ref waterPollution))
			{
				return result;
			}
		}
		else if (pos.x < num3 && num > 0)
		{
			if (this.TryFetchWaterImpl(pos, num - 1, num2, rate, max, ref result, ref waterPollution))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryFetchWaterImpl(pos, num - 1, num2 + 1, rate, max, ref result, ref waterPollution))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryFetchWaterImpl(pos, num - 1, num2 - 1, rate, max, ref result, ref waterPollution))
			{
				return result;
			}
		}
		return 0;
	}

	private bool TryFetchWaterImpl(Vector3 pos, int x, int z, int rate, int max, ref int result, ref byte waterPollution)
	{
		int num = z * 256 + x;
		WaterManager.Cell cell = this.m_waterGrid[num];
		if (cell.m_hasWater)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment = cell.m_closestPipeSegment;
			ushort startNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_endNode;
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & flags2 & NetNode.Flags.Water) != NetNode.Flags.None)
			{
				Segment2 segment;
				segment.a = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)startNode].m_position);
				segment.b = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)endNode].m_position);
				float num2;
				if ((double)segment.DistanceSqr(VectorUtils.XZ(pos), ref num2) < 9025.0)
				{
					rate = Mathf.Min(Mathf.Min(rate, max), 32768 + (int)cell.m_currentWaterPressure);
					cell.m_currentWaterPressure -= (short)rate;
					waterPollution = cell.m_pollution;
					this.m_waterGrid[num] = cell;
					result = rate;
					return true;
				}
			}
		}
		return false;
	}

	public int TryFetchWater(ushort node, int rate, int max, ref byte waterPollution)
	{
		if (node == 0)
		{
			return 0;
		}
		WaterManager.Node node2 = this.m_nodeData[(int)node];
		int num = Mathf.Min(rate, 32767);
		int num2 = Mathf.Min(Mathf.Min(rate, max), (int)node2.m_extraWaterPressure);
		max -= num2;
		rate = Mathf.Max(0, Mathf.Min(Mathf.Min(rate, max), num - (int)node2.m_collectWaterPressure));
		node2.m_collectWaterPressure += (ushort)rate;
		node2.m_extraWaterPressure -= (ushort)num2;
		if (num2 != 0)
		{
			waterPollution = node2.m_pollution;
		}
		this.m_nodeData[(int)node] = node2;
		return num2;
	}

	public int TryFetchHeating(Vector3 pos, int rate, int max, out bool connected)
	{
		connected = false;
		if (max == 0)
		{
			return 0;
		}
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		int result = 0;
		if (this.TryFetchHeatingImpl(pos, num, num2, rate, max, ref result, ref connected))
		{
			return result;
		}
		if (this.m_waterGrid[num2 * 256 + num].m_conductivity2 == 0)
		{
			return 0;
		}
		float num3 = ((float)num + 0.5f - 128f) * 38.25f;
		float num4 = ((float)num2 + 0.5f - 128f) * 38.25f;
		if (pos.z > num4 && num2 < 255)
		{
			if (this.TryFetchHeatingImpl(pos, num, num2 + 1, rate, max, ref result, ref connected))
			{
				return result;
			}
		}
		else if (pos.z < num4 && num2 > 0 && this.TryFetchHeatingImpl(pos, num, num2 - 1, rate, max, ref result, ref connected))
		{
			return result;
		}
		if (pos.x > num3 && num < 255)
		{
			if (this.TryFetchHeatingImpl(pos, num + 1, num2, rate, max, ref result, ref connected))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryFetchHeatingImpl(pos, num + 1, num2 + 1, rate, max, ref result, ref connected))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryFetchHeatingImpl(pos, num + 1, num2 - 1, rate, max, ref result, ref connected))
			{
				return result;
			}
		}
		else if (pos.x < num3 && num > 0)
		{
			if (this.TryFetchHeatingImpl(pos, num - 1, num2, rate, max, ref result, ref connected))
			{
				return result;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.TryFetchHeatingImpl(pos, num - 1, num2 + 1, rate, max, ref result, ref connected))
				{
					return result;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.TryFetchHeatingImpl(pos, num - 1, num2 - 1, rate, max, ref result, ref connected))
			{
				return result;
			}
		}
		return 0;
	}

	private bool TryFetchHeatingImpl(Vector3 pos, int x, int z, int rate, int max, ref int result, ref bool connected)
	{
		int num = z * 256 + x;
		WaterManager.Cell cell = this.m_waterGrid[num];
		if (cell.m_hasHeating)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment = cell.m_closestPipeSegment2;
			ushort startNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_endNode;
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & flags2 & NetNode.Flags.Heating) != NetNode.Flags.None)
			{
				Segment2 segment;
				segment.a = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)startNode].m_position);
				segment.b = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)endNode].m_position);
				float num2;
				if ((double)segment.DistanceSqr(VectorUtils.XZ(pos), ref num2) < 9025.0)
				{
					rate = Mathf.Min(Mathf.Min(rate, max), 32768 + (int)cell.m_currentHeatingPressure);
					cell.m_currentHeatingPressure -= (short)rate;
					this.m_waterGrid[num] = cell;
					result = rate;
					connected = true;
					return true;
				}
				return false;
			}
		}
		if (cell.m_closestPipeSegment2 != 0 && cell.m_conductivity2 >= 96)
		{
			NetManager instance2 = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment2 = cell.m_closestPipeSegment2;
			ushort startNode2 = instance2.m_segments.m_buffer[(int)closestPipeSegment2].m_startNode;
			ushort endNode2 = instance2.m_segments.m_buffer[(int)closestPipeSegment2].m_endNode;
			Segment2 segment2;
			segment2.a = VectorUtils.XZ(instance2.m_nodes.m_buffer[(int)startNode2].m_position);
			segment2.b = VectorUtils.XZ(instance2.m_nodes.m_buffer[(int)endNode2].m_position);
			float num3;
			if ((double)segment2.DistanceSqr(VectorUtils.XZ(pos), ref num3) < 9025.0)
			{
				connected = true;
			}
		}
		return false;
	}

	public int TryFetchSewage(ushort node, int rate, int max)
	{
		if (node == 0)
		{
			return 0;
		}
		int num = Mathf.Min(rate, 32767);
		WaterManager.Node node2 = this.m_nodeData[(int)node];
		if (node2.m_extraSewagePressure != 0)
		{
			int num2 = Mathf.Min(num - (int)node2.m_curSewagePressure, (int)node2.m_extraSewagePressure);
			if (num2 > 0)
			{
				node2.m_curSewagePressure += (ushort)num2;
				node2.m_extraSewagePressure -= (ushort)num2;
				rate -= num2;
			}
		}
		rate = Mathf.Max(0, Mathf.Min(Mathf.Min(rate, max), num - (int)node2.m_curSewagePressure));
		node2.m_curSewagePressure += (ushort)rate;
		this.m_nodeData[(int)node] = node2;
		return rate;
	}

	public void CheckWater(Vector3 pos, out bool water, out bool sewage, out byte waterPollution)
	{
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		if (this.CheckWaterImpl(pos, num, num2, out water, out sewage, out waterPollution))
		{
			return;
		}
		if (this.m_waterGrid[num2 * 256 + num].m_conductivity == 0)
		{
			return;
		}
		float num3 = ((float)num + 0.5f - 128f) * 38.25f;
		float num4 = ((float)num2 + 0.5f - 128f) * 38.25f;
		if (pos.z > num4 && num2 < 255)
		{
			if (this.CheckWaterImpl(pos, num, num2 + 1, out water, out sewage, out waterPollution))
			{
				return;
			}
		}
		else if (pos.z < num4 && num2 > 0 && this.CheckWaterImpl(pos, num, num2 - 1, out water, out sewage, out waterPollution))
		{
			return;
		}
		if (pos.x > num3 && num < 255)
		{
			if (this.CheckWaterImpl(pos, num + 1, num2, out water, out sewage, out waterPollution))
			{
				return;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.CheckWaterImpl(pos, num + 1, num2 + 1, out water, out sewage, out waterPollution))
				{
					return;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.CheckWaterImpl(pos, num + 1, num2 - 1, out water, out sewage, out waterPollution))
			{
				return;
			}
		}
		else if (pos.x < num3 && num > 0)
		{
			if (this.CheckWaterImpl(pos, num - 1, num2, out water, out sewage, out waterPollution))
			{
				return;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.CheckWaterImpl(pos, num - 1, num2 + 1, out water, out sewage, out waterPollution))
				{
					return;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.CheckWaterImpl(pos, num - 1, num2 - 1, out water, out sewage, out waterPollution))
			{
				return;
			}
		}
	}

	private bool CheckWaterImpl(Vector3 pos, int x, int z, out bool water, out bool sewage, out byte waterPollution)
	{
		int num = z * 256 + x;
		WaterManager.Cell cell = this.m_waterGrid[num];
		if (cell.m_hasWater || cell.m_hasSewage)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment = cell.m_closestPipeSegment;
			ushort startNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_endNode;
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & flags2 & (NetNode.Flags.Water | NetNode.Flags.Sewage)) != NetNode.Flags.None)
			{
				Segment2 segment;
				segment.a = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)startNode].m_position);
				segment.b = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)endNode].m_position);
				float num2;
				if ((double)segment.DistanceSqr(VectorUtils.XZ(pos), ref num2) < 9025.0)
				{
					water = (cell.m_hasWater && (flags & flags2 & NetNode.Flags.Water) != NetNode.Flags.None);
					sewage = (cell.m_hasSewage && (flags & flags2 & NetNode.Flags.Sewage) != NetNode.Flags.None);
					if (water)
					{
						waterPollution = this.m_waterGrid[num].m_pollution;
					}
					else
					{
						waterPollution = 0;
					}
					return true;
				}
			}
		}
		water = false;
		sewage = false;
		waterPollution = 0;
		return false;
	}

	public void CheckHeating(Vector3 pos, out bool heating)
	{
		int num = Mathf.Clamp((int)(pos.x / 38.25f + 128f), 0, 255);
		int num2 = Mathf.Clamp((int)(pos.z / 38.25f + 128f), 0, 255);
		if (this.CheckHeatingImpl(pos, num, num2, out heating))
		{
			return;
		}
		if (this.m_waterGrid[num2 * 256 + num].m_conductivity2 == 0)
		{
			return;
		}
		float num3 = ((float)num + 0.5f - 128f) * 38.25f;
		float num4 = ((float)num2 + 0.5f - 128f) * 38.25f;
		if (pos.z > num4 && num2 < 255)
		{
			if (this.CheckHeatingImpl(pos, num, num2 + 1, out heating))
			{
				return;
			}
		}
		else if (pos.z < num4 && num2 > 0 && this.CheckHeatingImpl(pos, num, num2 - 1, out heating))
		{
			return;
		}
		if (pos.x > num3 && num < 255)
		{
			if (this.CheckHeatingImpl(pos, num + 1, num2, out heating))
			{
				return;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.CheckHeatingImpl(pos, num + 1, num2 + 1, out heating))
				{
					return;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.CheckHeatingImpl(pos, num + 1, num2 - 1, out heating))
			{
				return;
			}
		}
		else if (pos.x < num3 && num > 0)
		{
			if (this.CheckHeatingImpl(pos, num - 1, num2, out heating))
			{
				return;
			}
			if (pos.z > num4 && num2 < 255)
			{
				if (this.CheckHeatingImpl(pos, num - 1, num2 + 1, out heating))
				{
					return;
				}
			}
			else if (pos.z < num4 && num2 > 0 && this.CheckHeatingImpl(pos, num - 1, num2 - 1, out heating))
			{
				return;
			}
		}
	}

	private bool CheckHeatingImpl(Vector3 pos, int x, int z, out bool heating)
	{
		int num = z * 256 + x;
		WaterManager.Cell cell = this.m_waterGrid[num];
		if (cell.m_hasHeating)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort closestPipeSegment = cell.m_closestPipeSegment2;
			ushort startNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)closestPipeSegment].m_endNode;
			NetNode.Flags flags = instance.m_nodes.m_buffer[(int)startNode].m_flags;
			NetNode.Flags flags2 = instance.m_nodes.m_buffer[(int)endNode].m_flags;
			if ((flags & flags2 & NetNode.Flags.Heating) != NetNode.Flags.None)
			{
				Segment2 segment;
				segment.a = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)startNode].m_position);
				segment.b = VectorUtils.XZ(instance.m_nodes.m_buffer[(int)endNode].m_position);
				float num2;
				if ((double)segment.DistanceSqr(VectorUtils.XZ(pos), ref num2) < 9025.0)
				{
					heating = true;
					return true;
				}
			}
		}
		heating = false;
		return false;
	}

	private ushort GetRootWaterGroup(ushort group)
	{
		for (ushort mergeIndex = this.m_waterPulseGroups[(int)group].m_mergeIndex; mergeIndex != 65535; mergeIndex = this.m_waterPulseGroups[(int)group].m_mergeIndex)
		{
			group = mergeIndex;
		}
		return group;
	}

	private ushort GetRootSewageGroup(ushort group)
	{
		for (ushort mergeIndex = this.m_sewagePulseGroups[(int)group].m_mergeIndex; mergeIndex != 65535; mergeIndex = this.m_sewagePulseGroups[(int)group].m_mergeIndex)
		{
			group = mergeIndex;
		}
		return group;
	}

	private ushort GetRootHeatingGroup(ushort group)
	{
		for (ushort mergeIndex = this.m_heatingPulseGroups[(int)group].m_mergeIndex; mergeIndex != 65535; mergeIndex = this.m_heatingPulseGroups[(int)group].m_mergeIndex)
		{
			group = mergeIndex;
		}
		return group;
	}

	private void MergeWaterGroups(ushort root, ushort merged)
	{
		WaterManager.PulseGroup pulseGroup = this.m_waterPulseGroups[(int)root];
		WaterManager.PulseGroup pulseGroup2 = this.m_waterPulseGroups[(int)merged];
		pulseGroup.m_origPressure += pulseGroup2.m_origPressure;
		pulseGroup.m_collectPressure += pulseGroup2.m_collectPressure;
		if (pulseGroup2.m_origPressure != 0u)
		{
			this.m_nodeData[(int)pulseGroup.m_node].m_pollution = (byte)(this.m_nodeData[(int)pulseGroup.m_node].m_pollution + this.m_nodeData[(int)pulseGroup2.m_node].m_pollution + 1 >> 1);
		}
		if (pulseGroup2.m_mergeCount != 0)
		{
			for (int i = 0; i < this.m_waterPulseGroupCount; i++)
			{
				if (this.m_waterPulseGroups[i].m_mergeIndex == merged)
				{
					this.m_waterPulseGroups[i].m_mergeIndex = root;
					pulseGroup2.m_origPressure -= this.m_waterPulseGroups[i].m_origPressure;
					pulseGroup2.m_collectPressure -= this.m_waterPulseGroups[i].m_collectPressure;
				}
			}
			pulseGroup.m_mergeCount += pulseGroup2.m_mergeCount;
			pulseGroup2.m_mergeCount = 0;
		}
		pulseGroup.m_curPressure += pulseGroup2.m_curPressure;
		pulseGroup2.m_curPressure = 0u;
		pulseGroup.m_mergeCount += 1;
		pulseGroup2.m_mergeIndex = root;
		this.m_waterPulseGroups[(int)root] = pulseGroup;
		this.m_waterPulseGroups[(int)merged] = pulseGroup2;
	}

	private void MergeSewageGroups(ushort root, ushort merged)
	{
		WaterManager.PulseGroup pulseGroup = this.m_sewagePulseGroups[(int)root];
		WaterManager.PulseGroup pulseGroup2 = this.m_sewagePulseGroups[(int)merged];
		pulseGroup.m_origPressure += pulseGroup2.m_origPressure;
		pulseGroup.m_collectPressure += pulseGroup2.m_collectPressure;
		if (pulseGroup2.m_mergeCount != 0)
		{
			for (int i = 0; i < this.m_sewagePulseGroupCount; i++)
			{
				if (this.m_sewagePulseGroups[i].m_mergeIndex == merged)
				{
					this.m_sewagePulseGroups[i].m_mergeIndex = root;
					pulseGroup2.m_origPressure -= this.m_sewagePulseGroups[i].m_origPressure;
					pulseGroup2.m_collectPressure -= this.m_sewagePulseGroups[i].m_collectPressure;
				}
			}
			pulseGroup.m_mergeCount += pulseGroup2.m_mergeCount;
			pulseGroup2.m_mergeCount = 0;
		}
		pulseGroup.m_curPressure += pulseGroup2.m_curPressure;
		pulseGroup2.m_curPressure = 0u;
		pulseGroup.m_mergeCount += 1;
		pulseGroup2.m_mergeIndex = root;
		this.m_sewagePulseGroups[(int)root] = pulseGroup;
		this.m_sewagePulseGroups[(int)merged] = pulseGroup2;
	}

	private void MergeHeatingGroups(ushort root, ushort merged)
	{
		WaterManager.PulseGroup pulseGroup = this.m_heatingPulseGroups[(int)root];
		WaterManager.PulseGroup pulseGroup2 = this.m_heatingPulseGroups[(int)merged];
		pulseGroup.m_origPressure += pulseGroup2.m_origPressure;
		if (pulseGroup2.m_mergeCount != 0)
		{
			for (int i = 0; i < this.m_heatingPulseGroupCount; i++)
			{
				if (this.m_heatingPulseGroups[i].m_mergeIndex == merged)
				{
					this.m_heatingPulseGroups[i].m_mergeIndex = root;
					pulseGroup2.m_origPressure -= this.m_heatingPulseGroups[i].m_origPressure;
				}
			}
			pulseGroup.m_mergeCount += pulseGroup2.m_mergeCount;
			pulseGroup2.m_mergeCount = 0;
		}
		pulseGroup.m_curPressure += pulseGroup2.m_curPressure;
		pulseGroup2.m_curPressure = 0u;
		pulseGroup.m_mergeCount += 1;
		pulseGroup2.m_mergeIndex = root;
		this.m_heatingPulseGroups[(int)root] = pulseGroup;
		this.m_heatingPulseGroups[(int)merged] = pulseGroup2;
	}

	private void ConductWaterToCell(ref WaterManager.Cell cell, ushort group, int x, int z)
	{
		if (cell.m_conductivity >= 96 && cell.m_waterPulseGroup == 65535)
		{
			WaterManager.PulseUnit pulseUnit;
			pulseUnit.m_group = group;
			pulseUnit.m_node = 0;
			pulseUnit.m_x = (byte)x;
			pulseUnit.m_z = (byte)z;
			this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit;
			if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
			{
				this.m_waterPulseUnitEnd = 0;
			}
			cell.m_waterPulseGroup = group;
			this.m_canContinue = true;
		}
	}

	private void ConductSewageToCell(ref WaterManager.Cell cell, ushort group, int x, int z)
	{
		if (cell.m_conductivity >= 96 && cell.m_sewagePulseGroup == 65535)
		{
			WaterManager.PulseUnit pulseUnit;
			pulseUnit.m_group = group;
			pulseUnit.m_node = 0;
			pulseUnit.m_x = (byte)x;
			pulseUnit.m_z = (byte)z;
			this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit;
			if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
			{
				this.m_sewagePulseUnitEnd = 0;
			}
			cell.m_sewagePulseGroup = group;
			this.m_canContinue = true;
		}
	}

	private void ConductHeatingToCell(ref WaterManager.Cell cell, ushort group, int x, int z)
	{
		if (cell.m_conductivity2 >= 96 && cell.m_heatingPulseGroup == 65535)
		{
			WaterManager.PulseUnit pulseUnit;
			pulseUnit.m_group = group;
			pulseUnit.m_node = 0;
			pulseUnit.m_x = (byte)x;
			pulseUnit.m_z = (byte)z;
			this.m_heatingPulseUnits[this.m_heatingPulseUnitEnd] = pulseUnit;
			if (++this.m_heatingPulseUnitEnd == this.m_heatingPulseUnits.Length)
			{
				this.m_heatingPulseUnitEnd = 0;
			}
			cell.m_heatingPulseGroup = group;
			this.m_canContinue = true;
		}
	}

	private void ConductWaterToCells(ushort group, float worldX, float worldZ, float radius)
	{
		int num = Mathf.Max((int)((worldX - radius) / 38.25f + 128f), 0);
		int num2 = Mathf.Max((int)((worldZ - radius) / 38.25f + 128f), 0);
		int num3 = Mathf.Min((int)((worldX + radius) / 38.25f + 128f), 255);
		int num4 = Mathf.Min((int)((worldZ + radius) / 38.25f + 128f), 255);
		float num5 = radius + 19.125f;
		num5 *= num5;
		for (int i = num2; i <= num4; i++)
		{
			float num6 = ((float)i + 0.5f - 128f) * 38.25f - worldZ;
			for (int j = num; j <= num3; j++)
			{
				float num7 = ((float)j + 0.5f - 128f) * 38.25f - worldX;
				if (num7 * num7 + num6 * num6 < num5)
				{
					int num8 = i * 256 + j;
					this.ConductWaterToCell(ref this.m_waterGrid[num8], group, j, i);
				}
			}
		}
	}

	private void ConductSewageToCells(ushort group, float worldX, float worldZ, float radius)
	{
		int num = Mathf.Max((int)((worldX - radius) / 38.25f + 128f), 0);
		int num2 = Mathf.Max((int)((worldZ - radius) / 38.25f + 128f), 0);
		int num3 = Mathf.Min((int)((worldX + radius) / 38.25f + 128f), 255);
		int num4 = Mathf.Min((int)((worldZ + radius) / 38.25f + 128f), 255);
		float num5 = radius + 19.125f;
		num5 *= num5;
		for (int i = num2; i <= num4; i++)
		{
			float num6 = ((float)i + 0.5f - 128f) * 38.25f - worldZ;
			for (int j = num; j <= num3; j++)
			{
				float num7 = ((float)j + 0.5f - 128f) * 38.25f - worldX;
				if (num7 * num7 + num6 * num6 < num5)
				{
					int num8 = i * 256 + j;
					this.ConductSewageToCell(ref this.m_waterGrid[num8], group, j, i);
				}
			}
		}
	}

	private void ConductHeatingToCells(ushort group, float worldX, float worldZ, float radius)
	{
		int num = Mathf.Max((int)((worldX - radius) / 38.25f + 128f), 0);
		int num2 = Mathf.Max((int)((worldZ - radius) / 38.25f + 128f), 0);
		int num3 = Mathf.Min((int)((worldX + radius) / 38.25f + 128f), 255);
		int num4 = Mathf.Min((int)((worldZ + radius) / 38.25f + 128f), 255);
		float num5 = radius + 19.125f;
		num5 *= num5;
		for (int i = num2; i <= num4; i++)
		{
			float num6 = ((float)i + 0.5f - 128f) * 38.25f - worldZ;
			for (int j = num; j <= num3; j++)
			{
				float num7 = ((float)j + 0.5f - 128f) * 38.25f - worldX;
				if (num7 * num7 + num6 * num6 < num5)
				{
					int num8 = i * 256 + j;
					this.ConductHeatingToCell(ref this.m_waterGrid[num8], group, j, i);
				}
			}
		}
	}

	private void ConductWaterToNode(ushort nodeIndex, ref NetNode node, ushort group)
	{
		NetInfo info = node.Info;
		if (info.m_class.m_service == ItemClass.Service.Water && info.m_class.m_level <= ItemClass.Level.Level2)
		{
			if (this.m_nodeData[(int)nodeIndex].m_waterPulseGroup == 65535)
			{
				WaterManager.PulseUnit pulseUnit;
				pulseUnit.m_group = group;
				pulseUnit.m_node = nodeIndex;
				pulseUnit.m_x = 0;
				pulseUnit.m_z = 0;
				this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit;
				if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
				{
					this.m_waterPulseUnitEnd = 0;
				}
				this.m_nodeData[(int)nodeIndex].m_waterPulseGroup = group;
				this.m_canContinue = true;
			}
			else
			{
				ushort rootWaterGroup = this.GetRootWaterGroup(this.m_nodeData[(int)nodeIndex].m_waterPulseGroup);
				if (rootWaterGroup != group)
				{
					this.MergeWaterGroups(group, rootWaterGroup);
					if (this.m_waterPulseGroups[(int)rootWaterGroup].m_origPressure == 0u)
					{
						WaterManager.PulseUnit pulseUnit2;
						pulseUnit2.m_group = group;
						pulseUnit2.m_node = nodeIndex;
						pulseUnit2.m_x = 0;
						pulseUnit2.m_z = 0;
						this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit2;
						if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
						{
							this.m_waterPulseUnitEnd = 0;
						}
					}
					this.m_nodeData[(int)nodeIndex].m_waterPulseGroup = group;
					this.m_canContinue = true;
				}
			}
		}
	}

	private void ConductSewageToNode(ushort nodeIndex, ref NetNode node, ushort group)
	{
		NetInfo info = node.Info;
		if (info.m_class.m_service == ItemClass.Service.Water && info.m_class.m_level <= ItemClass.Level.Level2)
		{
			if (this.m_nodeData[(int)nodeIndex].m_sewagePulseGroup == 65535)
			{
				WaterManager.PulseUnit pulseUnit;
				pulseUnit.m_group = group;
				pulseUnit.m_node = nodeIndex;
				pulseUnit.m_x = 0;
				pulseUnit.m_z = 0;
				this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit;
				if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
				{
					this.m_sewagePulseUnitEnd = 0;
				}
				this.m_nodeData[(int)nodeIndex].m_sewagePulseGroup = group;
				this.m_canContinue = true;
			}
			else
			{
				ushort rootSewageGroup = this.GetRootSewageGroup(this.m_nodeData[(int)nodeIndex].m_sewagePulseGroup);
				if (rootSewageGroup != group)
				{
					this.MergeSewageGroups(group, rootSewageGroup);
					if (this.m_sewagePulseGroups[(int)rootSewageGroup].m_origPressure == 0u)
					{
						WaterManager.PulseUnit pulseUnit2;
						pulseUnit2.m_group = group;
						pulseUnit2.m_node = nodeIndex;
						pulseUnit2.m_x = 0;
						pulseUnit2.m_z = 0;
						this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit2;
						if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
						{
							this.m_sewagePulseUnitEnd = 0;
						}
					}
					this.m_nodeData[(int)nodeIndex].m_sewagePulseGroup = group;
					this.m_canContinue = true;
				}
			}
		}
	}

	private void ConductHeatingToNode(ushort nodeIndex, ref NetNode node, ushort group)
	{
		NetInfo info = node.Info;
		if (info.m_class.m_service == ItemClass.Service.Water && info.m_class.m_level == ItemClass.Level.Level2)
		{
			if (this.m_nodeData[(int)nodeIndex].m_heatingPulseGroup == 65535)
			{
				WaterManager.PulseUnit pulseUnit;
				pulseUnit.m_group = group;
				pulseUnit.m_node = nodeIndex;
				pulseUnit.m_x = 0;
				pulseUnit.m_z = 0;
				this.m_heatingPulseUnits[this.m_heatingPulseUnitEnd] = pulseUnit;
				if (++this.m_heatingPulseUnitEnd == this.m_heatingPulseUnits.Length)
				{
					this.m_heatingPulseUnitEnd = 0;
				}
				this.m_nodeData[(int)nodeIndex].m_heatingPulseGroup = group;
				this.m_canContinue = true;
			}
			else
			{
				ushort rootHeatingGroup = this.GetRootHeatingGroup(this.m_nodeData[(int)nodeIndex].m_heatingPulseGroup);
				if (rootHeatingGroup != group)
				{
					this.MergeHeatingGroups(group, rootHeatingGroup);
					this.m_nodeData[(int)nodeIndex].m_heatingPulseGroup = group;
					this.m_canContinue = true;
				}
			}
		}
	}

	private void UpdateNodeWater(int nodeID, int water, int sewage, int heating)
	{
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = false;
		NetNode.Flags flags = instance.m_nodes.m_buffer[nodeID].m_flags;
		if ((flags & NetNode.Flags.Transition) != NetNode.Flags.None)
		{
			NetNode[] expr_47_cp_0 = instance.m_nodes.m_buffer;
			expr_47_cp_0[nodeID].m_flags = (expr_47_cp_0[nodeID].m_flags & ~NetNode.Flags.Transition);
			return;
		}
		ushort building = instance.m_nodes.m_buffer[nodeID].m_building;
		if (building != 0)
		{
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_waterBuffer != water)
			{
				instance2.m_buildings.m_buffer[(int)building].m_waterBuffer = (ushort)water;
				flag = (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating);
			}
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_sewageBuffer != sewage)
			{
				instance2.m_buildings.m_buffer[(int)building].m_sewageBuffer = (ushort)sewage;
				flag = (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating);
			}
			if ((int)instance2.m_buildings.m_buffer[(int)building].m_heatingBuffer != heating)
			{
				instance2.m_buildings.m_buffer[(int)building].m_heatingBuffer = (ushort)heating;
				flag = (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating);
			}
			if (flag)
			{
				instance2.UpdateBuildingColors(building);
			}
		}
		NetNode.Flags flags2 = flags & ~(NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.Heating);
		if (water != 0)
		{
			flags2 |= NetNode.Flags.Water;
		}
		if (sewage != 0)
		{
			flags2 |= NetNode.Flags.Sewage;
		}
		if (heating != 0)
		{
			flags2 |= NetNode.Flags.Heating;
		}
		if (flags2 != flags)
		{
			instance.m_nodes.m_buffer[nodeID].m_flags = flags2;
			flag = (currentMode == InfoManager.InfoMode.Water || currentMode == InfoManager.InfoMode.Heating);
		}
		if (flag)
		{
			instance.UpdateNodeColors((ushort)nodeID);
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[nodeID].GetSegment(i);
				if (segment != 0)
				{
					instance.UpdateSegmentColors(segment);
				}
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0 && subStep != 1000)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			int num = (int)(currentFrameIndex & 255u);
			if (num < 128)
			{
				if (num == 0)
				{
					this.m_waterPulseGroupCount = 0;
					this.m_waterPulseUnitStart = 0;
					this.m_waterPulseUnitEnd = 0;
					this.m_sewagePulseGroupCount = 0;
					this.m_sewagePulseUnitStart = 0;
					this.m_sewagePulseUnitEnd = 0;
					this.m_heatingPulseGroupCount = 0;
					this.m_heatingPulseUnitStart = 0;
					this.m_heatingPulseUnitEnd = 0;
					this.m_processedCells = 0;
					this.m_conductiveCells = 0;
					this.m_canContinue = true;
				}
				int num2 = num * 32768 >> 7;
				int num3 = ((num + 1) * 32768 >> 7) - 1;
				for (int i = num2; i <= num3; i++)
				{
					WaterManager.Node node = this.m_nodeData[i];
					NetNode.Flags flags = instance.m_nodes.m_buffer[i].m_flags;
					if (flags != NetNode.Flags.None)
					{
						NetInfo info = instance.m_nodes.m_buffer[i].Info;
						if (info.m_class.m_service == ItemClass.Service.Water && info.m_class.m_level <= ItemClass.Level.Level2)
						{
							int water = (node.m_waterPulseGroup == 65535) ? 0 : 1;
							int sewage = (node.m_sewagePulseGroup == 65535) ? 0 : 1;
							int heating = (node.m_heatingPulseGroup == 65535) ? 0 : 1;
							this.UpdateNodeWater(i, water, sewage, heating);
							this.m_conductiveCells += 2;
							node.m_waterPulseGroup = 65535;
							node.m_sewagePulseGroup = 65535;
							node.m_heatingPulseGroup = 65535;
							if ((node.m_curWaterPressure != 0 || node.m_collectWaterPressure != 0) && this.m_waterPulseGroupCount < 1024)
							{
								WaterManager.PulseGroup pulseGroup;
								pulseGroup.m_origPressure = (uint)node.m_curWaterPressure;
								pulseGroup.m_curPressure = (uint)node.m_curWaterPressure;
								pulseGroup.m_collectPressure = (uint)node.m_collectWaterPressure;
								pulseGroup.m_mergeCount = 0;
								pulseGroup.m_mergeIndex = 65535;
								pulseGroup.m_node = (ushort)i;
								node.m_waterPulseGroup = (ushort)this.m_waterPulseGroupCount;
								this.m_waterPulseGroups[this.m_waterPulseGroupCount++] = pulseGroup;
								if (pulseGroup.m_origPressure != 0u)
								{
									WaterManager.PulseUnit pulseUnit;
									pulseUnit.m_group = (ushort)(this.m_waterPulseGroupCount - 1);
									pulseUnit.m_node = (ushort)i;
									pulseUnit.m_x = 0;
									pulseUnit.m_z = 0;
									this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit;
									if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
									{
										this.m_waterPulseUnitEnd = 0;
									}
								}
							}
							if ((node.m_curSewagePressure != 0 || node.m_collectSewagePressure != 0) && this.m_sewagePulseGroupCount < 1024)
							{
								WaterManager.PulseGroup pulseGroup2;
								pulseGroup2.m_origPressure = (uint)node.m_curSewagePressure;
								pulseGroup2.m_curPressure = (uint)node.m_curSewagePressure;
								pulseGroup2.m_collectPressure = (uint)node.m_collectSewagePressure;
								pulseGroup2.m_mergeCount = 0;
								pulseGroup2.m_mergeIndex = 65535;
								pulseGroup2.m_node = (ushort)i;
								node.m_sewagePulseGroup = (ushort)this.m_sewagePulseGroupCount;
								this.m_sewagePulseGroups[this.m_sewagePulseGroupCount++] = pulseGroup2;
								if (pulseGroup2.m_origPressure != 0u)
								{
									WaterManager.PulseUnit pulseUnit2;
									pulseUnit2.m_group = (ushort)(this.m_sewagePulseGroupCount - 1);
									pulseUnit2.m_node = (ushort)i;
									pulseUnit2.m_x = 0;
									pulseUnit2.m_z = 0;
									this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit2;
									if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
									{
										this.m_sewagePulseUnitEnd = 0;
									}
								}
							}
							if (node.m_curHeatingPressure != 0 && this.m_heatingPulseGroupCount < 1024)
							{
								WaterManager.PulseGroup pulseGroup3;
								pulseGroup3.m_origPressure = (uint)node.m_curHeatingPressure;
								pulseGroup3.m_curPressure = (uint)node.m_curHeatingPressure;
								pulseGroup3.m_collectPressure = 0u;
								pulseGroup3.m_mergeCount = 0;
								pulseGroup3.m_mergeIndex = 65535;
								pulseGroup3.m_node = (ushort)i;
								WaterManager.PulseUnit pulseUnit3;
								pulseUnit3.m_group = (ushort)this.m_heatingPulseGroupCount;
								pulseUnit3.m_node = (ushort)i;
								pulseUnit3.m_x = 0;
								pulseUnit3.m_z = 0;
								node.m_heatingPulseGroup = (ushort)this.m_heatingPulseGroupCount;
								this.m_heatingPulseGroups[this.m_heatingPulseGroupCount++] = pulseGroup3;
								this.m_heatingPulseUnits[this.m_heatingPulseUnitEnd] = pulseUnit3;
								if (++this.m_heatingPulseUnitEnd == this.m_heatingPulseUnits.Length)
								{
									this.m_heatingPulseUnitEnd = 0;
								}
							}
						}
						else
						{
							node.m_waterPulseGroup = 65535;
							node.m_sewagePulseGroup = 65535;
							node.m_heatingPulseGroup = 65535;
							node.m_extraWaterPressure = 0;
							node.m_extraSewagePressure = 0;
							node.m_extraHeatingPressure = 0;
						}
					}
					else
					{
						node.m_waterPulseGroup = 65535;
						node.m_sewagePulseGroup = 65535;
						node.m_heatingPulseGroup = 65535;
						node.m_extraWaterPressure = 0;
						node.m_extraSewagePressure = 0;
						node.m_extraHeatingPressure = 0;
					}
					node.m_curWaterPressure = 0;
					node.m_curSewagePressure = 0;
					node.m_curHeatingPressure = 0;
					node.m_collectWaterPressure = 0;
					node.m_collectSewagePressure = 0;
					this.m_nodeData[i] = node;
				}
				int num4 = num * 256 >> 7;
				int num5 = ((num + 1) * 256 >> 7) - 1;
				for (int j = num4; j <= num5; j++)
				{
					int num6 = j * 256;
					for (int k = 0; k < 256; k++)
					{
						WaterManager.Cell cell = this.m_waterGrid[num6];
						cell.m_waterPulseGroup = 65535;
						cell.m_sewagePulseGroup = 65535;
						cell.m_heatingPulseGroup = 65535;
						if (cell.m_conductivity >= 96)
						{
							this.m_conductiveCells += 2;
						}
						if (cell.m_tmpHasWater != cell.m_hasWater)
						{
							cell.m_hasWater = cell.m_tmpHasWater;
						}
						if (cell.m_tmpHasSewage != cell.m_hasSewage)
						{
							cell.m_hasSewage = cell.m_tmpHasSewage;
						}
						if (cell.m_tmpHasHeating != cell.m_hasHeating)
						{
							cell.m_hasHeating = cell.m_tmpHasHeating;
						}
						cell.m_tmpHasWater = false;
						cell.m_tmpHasSewage = false;
						cell.m_tmpHasHeating = false;
						this.m_waterGrid[num6] = cell;
						num6++;
					}
				}
			}
			else
			{
				int num7 = (num - 127) * this.m_conductiveCells >> 7;
				if (num == 255)
				{
					num7 = 1000000000;
				}
				while (this.m_canContinue && this.m_processedCells < num7)
				{
					this.m_canContinue = false;
					int waterPulseUnitEnd = this.m_waterPulseUnitEnd;
					int sewagePulseUnitEnd = this.m_sewagePulseUnitEnd;
					int heatingPulseUnitEnd = this.m_heatingPulseUnitEnd;
					while (this.m_waterPulseUnitStart != waterPulseUnitEnd)
					{
						WaterManager.PulseUnit pulseUnit4 = this.m_waterPulseUnits[this.m_waterPulseUnitStart];
						if (++this.m_waterPulseUnitStart == this.m_waterPulseUnits.Length)
						{
							this.m_waterPulseUnitStart = 0;
						}
						pulseUnit4.m_group = this.GetRootWaterGroup(pulseUnit4.m_group);
						uint num8 = this.m_waterPulseGroups[(int)pulseUnit4.m_group].m_curPressure;
						if (pulseUnit4.m_node == 0)
						{
							int num9 = (int)pulseUnit4.m_z * 256 + (int)pulseUnit4.m_x;
							WaterManager.Cell cell2 = this.m_waterGrid[num9];
							if (cell2.m_conductivity != 0 && !cell2.m_tmpHasWater && num8 != 0u)
							{
								int num10 = Mathf.Clamp((int)(-(int)cell2.m_currentWaterPressure), 0, (int)num8);
								num8 -= (uint)num10;
								cell2.m_currentWaterPressure += (short)num10;
								if (cell2.m_currentWaterPressure >= 0)
								{
									cell2.m_tmpHasWater = true;
									cell2.m_pollution = this.m_nodeData[(int)this.m_waterPulseGroups[(int)pulseUnit4.m_group].m_node].m_pollution;
								}
								this.m_waterGrid[num9] = cell2;
								this.m_waterPulseGroups[(int)pulseUnit4.m_group].m_curPressure = num8;
							}
							if (num8 != 0u)
							{
								this.m_processedCells++;
							}
							else
							{
								this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit4;
								if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
								{
									this.m_waterPulseUnitEnd = 0;
								}
							}
						}
						else if (num8 != 0u)
						{
							this.m_processedCells++;
							NetNode netNode = instance.m_nodes.m_buffer[(int)pulseUnit4.m_node];
							if (netNode.m_flags != NetNode.Flags.None && netNode.m_buildIndex < (currentFrameIndex & 4294967168u))
							{
								byte pollution = this.m_nodeData[(int)this.m_waterPulseGroups[(int)pulseUnit4.m_group].m_node].m_pollution;
								this.m_nodeData[(int)pulseUnit4.m_node].m_pollution = pollution;
								if (netNode.m_building != 0)
								{
									Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)netNode.m_building].m_waterPollution = pollution;
								}
								this.ConductWaterToCells(pulseUnit4.m_group, netNode.m_position.x, netNode.m_position.z, 100f);
								for (int l = 0; l < 8; l++)
								{
									ushort segment = netNode.GetSegment(l);
									if (segment != 0)
									{
										ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
										ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
										ushort num11 = (startNode != pulseUnit4.m_node) ? startNode : endNode;
										this.ConductWaterToNode(num11, ref instance.m_nodes.m_buffer[(int)num11], pulseUnit4.m_group);
									}
								}
							}
						}
						else
						{
							this.m_waterPulseUnits[this.m_waterPulseUnitEnd] = pulseUnit4;
							if (++this.m_waterPulseUnitEnd == this.m_waterPulseUnits.Length)
							{
								this.m_waterPulseUnitEnd = 0;
							}
						}
					}
					while (this.m_sewagePulseUnitStart != sewagePulseUnitEnd)
					{
						WaterManager.PulseUnit pulseUnit5 = this.m_sewagePulseUnits[this.m_sewagePulseUnitStart];
						if (++this.m_sewagePulseUnitStart == this.m_sewagePulseUnits.Length)
						{
							this.m_sewagePulseUnitStart = 0;
						}
						pulseUnit5.m_group = this.GetRootSewageGroup(pulseUnit5.m_group);
						uint num12 = this.m_sewagePulseGroups[(int)pulseUnit5.m_group].m_curPressure;
						if (pulseUnit5.m_node == 0)
						{
							int num13 = (int)pulseUnit5.m_z * 256 + (int)pulseUnit5.m_x;
							WaterManager.Cell cell3 = this.m_waterGrid[num13];
							if (cell3.m_conductivity != 0 && !cell3.m_tmpHasSewage && num12 != 0u)
							{
								int num14 = Mathf.Clamp((int)(-(int)cell3.m_currentSewagePressure), 0, (int)num12);
								num12 -= (uint)num14;
								cell3.m_currentSewagePressure += (short)num14;
								if (cell3.m_currentSewagePressure >= 0)
								{
									cell3.m_tmpHasSewage = true;
								}
								this.m_waterGrid[num13] = cell3;
								this.m_sewagePulseGroups[(int)pulseUnit5.m_group].m_curPressure = num12;
							}
							if (num12 != 0u)
							{
								this.m_processedCells++;
							}
							else
							{
								this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit5;
								if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
								{
									this.m_sewagePulseUnitEnd = 0;
								}
							}
						}
						else if (num12 != 0u)
						{
							this.m_processedCells++;
							NetNode netNode2 = instance.m_nodes.m_buffer[(int)pulseUnit5.m_node];
							if (netNode2.m_flags != NetNode.Flags.None && netNode2.m_buildIndex < (currentFrameIndex & 4294967168u))
							{
								this.ConductSewageToCells(pulseUnit5.m_group, netNode2.m_position.x, netNode2.m_position.z, 100f);
								for (int m = 0; m < 8; m++)
								{
									ushort segment2 = netNode2.GetSegment(m);
									if (segment2 != 0)
									{
										ushort startNode2 = instance.m_segments.m_buffer[(int)segment2].m_startNode;
										ushort endNode2 = instance.m_segments.m_buffer[(int)segment2].m_endNode;
										ushort num15 = (startNode2 != pulseUnit5.m_node) ? startNode2 : endNode2;
										this.ConductSewageToNode(num15, ref instance.m_nodes.m_buffer[(int)num15], pulseUnit5.m_group);
									}
								}
							}
						}
						else
						{
							this.m_sewagePulseUnits[this.m_sewagePulseUnitEnd] = pulseUnit5;
							if (++this.m_sewagePulseUnitEnd == this.m_sewagePulseUnits.Length)
							{
								this.m_sewagePulseUnitEnd = 0;
							}
						}
					}
					while (this.m_heatingPulseUnitStart != heatingPulseUnitEnd)
					{
						WaterManager.PulseUnit pulseUnit6 = this.m_heatingPulseUnits[this.m_heatingPulseUnitStart];
						if (++this.m_heatingPulseUnitStart == this.m_heatingPulseUnits.Length)
						{
							this.m_heatingPulseUnitStart = 0;
						}
						pulseUnit6.m_group = this.GetRootHeatingGroup(pulseUnit6.m_group);
						uint num16 = this.m_heatingPulseGroups[(int)pulseUnit6.m_group].m_curPressure;
						if (pulseUnit6.m_node == 0)
						{
							int num17 = (int)pulseUnit6.m_z * 256 + (int)pulseUnit6.m_x;
							WaterManager.Cell cell4 = this.m_waterGrid[num17];
							if (cell4.m_conductivity2 != 0 && !cell4.m_tmpHasHeating && num16 != 0u)
							{
								int num18 = Mathf.Clamp((int)(-(int)cell4.m_currentHeatingPressure), 0, (int)num16);
								num16 -= (uint)num18;
								cell4.m_currentHeatingPressure += (short)num18;
								if (cell4.m_currentHeatingPressure >= 0)
								{
									cell4.m_tmpHasHeating = true;
								}
								this.m_waterGrid[num17] = cell4;
								this.m_heatingPulseGroups[(int)pulseUnit6.m_group].m_curPressure = num16;
							}
							if (num16 != 0u)
							{
								this.m_processedCells++;
							}
							else
							{
								this.m_heatingPulseUnits[this.m_heatingPulseUnitEnd] = pulseUnit6;
								if (++this.m_heatingPulseUnitEnd == this.m_heatingPulseUnits.Length)
								{
									this.m_heatingPulseUnitEnd = 0;
								}
							}
						}
						else if (num16 != 0u)
						{
							this.m_processedCells++;
							NetNode netNode3 = instance.m_nodes.m_buffer[(int)pulseUnit6.m_node];
							if (netNode3.m_flags != NetNode.Flags.None && netNode3.m_buildIndex < (currentFrameIndex & 4294967168u))
							{
								this.ConductHeatingToCells(pulseUnit6.m_group, netNode3.m_position.x, netNode3.m_position.z, 100f);
								for (int n = 0; n < 8; n++)
								{
									ushort segment3 = netNode3.GetSegment(n);
									if (segment3 != 0)
									{
										NetInfo info2 = instance.m_segments.m_buffer[(int)segment3].Info;
										if (info2.m_class.m_service == ItemClass.Service.Water && info2.m_class.m_level == ItemClass.Level.Level2)
										{
											ushort startNode3 = instance.m_segments.m_buffer[(int)segment3].m_startNode;
											ushort endNode3 = instance.m_segments.m_buffer[(int)segment3].m_endNode;
											ushort num19 = (startNode3 != pulseUnit6.m_node) ? startNode3 : endNode3;
											this.ConductHeatingToNode(num19, ref instance.m_nodes.m_buffer[(int)num19], pulseUnit6.m_group);
										}
									}
								}
							}
						}
						else
						{
							this.m_heatingPulseUnits[this.m_heatingPulseUnitEnd] = pulseUnit6;
							if (++this.m_heatingPulseUnitEnd == this.m_heatingPulseUnits.Length)
							{
								this.m_heatingPulseUnitEnd = 0;
							}
						}
					}
				}
				if (num == 255)
				{
					for (int num20 = 0; num20 < this.m_waterPulseGroupCount; num20++)
					{
						WaterManager.PulseGroup pulseGroup4 = this.m_waterPulseGroups[num20];
						if (pulseGroup4.m_mergeIndex != 65535 && pulseGroup4.m_collectPressure != 0u)
						{
							WaterManager.PulseGroup pulseGroup5 = this.m_waterPulseGroups[(int)pulseGroup4.m_mergeIndex];
							pulseGroup4.m_curPressure = (uint)((ulong)pulseGroup5.m_curPressure * (ulong)pulseGroup4.m_collectPressure / (ulong)pulseGroup5.m_collectPressure);
							if (pulseGroup4.m_collectPressure < pulseGroup4.m_curPressure)
							{
								pulseGroup4.m_curPressure = pulseGroup4.m_collectPressure;
							}
							pulseGroup5.m_curPressure -= pulseGroup4.m_curPressure;
							pulseGroup5.m_collectPressure -= pulseGroup4.m_collectPressure;
							this.m_waterPulseGroups[(int)pulseGroup4.m_mergeIndex] = pulseGroup5;
							this.m_waterPulseGroups[num20] = pulseGroup4;
						}
					}
					for (int num21 = 0; num21 < this.m_waterPulseGroupCount; num21++)
					{
						WaterManager.PulseGroup pulseGroup6 = this.m_waterPulseGroups[num21];
						if (pulseGroup6.m_mergeIndex != 65535 && pulseGroup6.m_collectPressure == 0u)
						{
							WaterManager.PulseGroup pulseGroup7 = this.m_waterPulseGroups[(int)pulseGroup6.m_mergeIndex];
							uint num22 = pulseGroup7.m_curPressure;
							if (pulseGroup7.m_collectPressure >= num22)
							{
								num22 = 0u;
							}
							else
							{
								num22 -= pulseGroup7.m_collectPressure;
							}
							pulseGroup6.m_curPressure = (uint)((ulong)num22 * (ulong)pulseGroup6.m_origPressure / (ulong)pulseGroup7.m_origPressure);
							pulseGroup7.m_curPressure -= pulseGroup6.m_curPressure;
							pulseGroup7.m_origPressure -= pulseGroup6.m_origPressure;
							this.m_waterPulseGroups[(int)pulseGroup6.m_mergeIndex] = pulseGroup7;
							this.m_waterPulseGroups[num21] = pulseGroup6;
						}
					}
					for (int num23 = 0; num23 < this.m_waterPulseGroupCount; num23++)
					{
						WaterManager.PulseGroup pulseGroup8 = this.m_waterPulseGroups[num23];
						if (pulseGroup8.m_curPressure != 0u)
						{
							WaterManager.Node node2 = this.m_nodeData[(int)pulseGroup8.m_node];
							node2.m_extraWaterPressure += (ushort)Mathf.Min((int)pulseGroup8.m_curPressure, (int)(32767 - node2.m_extraWaterPressure));
							this.m_nodeData[(int)pulseGroup8.m_node] = node2;
						}
					}
					for (int num24 = 0; num24 < this.m_sewagePulseGroupCount; num24++)
					{
						WaterManager.PulseGroup pulseGroup9 = this.m_sewagePulseGroups[num24];
						if (pulseGroup9.m_mergeIndex != 65535 && pulseGroup9.m_collectPressure != 0u)
						{
							WaterManager.PulseGroup pulseGroup10 = this.m_sewagePulseGroups[(int)pulseGroup9.m_mergeIndex];
							pulseGroup9.m_curPressure = (uint)((ulong)pulseGroup10.m_curPressure * (ulong)pulseGroup9.m_collectPressure / (ulong)pulseGroup10.m_collectPressure);
							if (pulseGroup9.m_collectPressure < pulseGroup9.m_curPressure)
							{
								pulseGroup9.m_curPressure = pulseGroup9.m_collectPressure;
							}
							pulseGroup10.m_curPressure -= pulseGroup9.m_curPressure;
							pulseGroup10.m_collectPressure -= pulseGroup9.m_collectPressure;
							this.m_sewagePulseGroups[(int)pulseGroup9.m_mergeIndex] = pulseGroup10;
							this.m_sewagePulseGroups[num24] = pulseGroup9;
						}
					}
					for (int num25 = 0; num25 < this.m_sewagePulseGroupCount; num25++)
					{
						WaterManager.PulseGroup pulseGroup11 = this.m_sewagePulseGroups[num25];
						if (pulseGroup11.m_mergeIndex != 65535 && pulseGroup11.m_collectPressure == 0u)
						{
							WaterManager.PulseGroup pulseGroup12 = this.m_sewagePulseGroups[(int)pulseGroup11.m_mergeIndex];
							uint num26 = pulseGroup12.m_curPressure;
							if (pulseGroup12.m_collectPressure < num26)
							{
								num26 -= pulseGroup12.m_collectPressure;
							}
							pulseGroup11.m_curPressure = (uint)((ulong)pulseGroup12.m_curPressure * (ulong)pulseGroup11.m_origPressure / (ulong)pulseGroup12.m_origPressure);
							pulseGroup12.m_curPressure -= pulseGroup11.m_curPressure;
							pulseGroup12.m_origPressure -= pulseGroup11.m_origPressure;
							this.m_sewagePulseGroups[(int)pulseGroup11.m_mergeIndex] = pulseGroup12;
							this.m_sewagePulseGroups[num25] = pulseGroup11;
						}
					}
					for (int num27 = 0; num27 < this.m_sewagePulseGroupCount; num27++)
					{
						WaterManager.PulseGroup pulseGroup13 = this.m_sewagePulseGroups[num27];
						if (pulseGroup13.m_curPressure != 0u)
						{
							WaterManager.Node node3 = this.m_nodeData[(int)pulseGroup13.m_node];
							node3.m_extraSewagePressure += (ushort)Mathf.Min((int)pulseGroup13.m_curPressure, (int)(32767 - node3.m_extraSewagePressure));
							this.m_nodeData[(int)pulseGroup13.m_node] = node3;
						}
					}
					for (int num28 = 0; num28 < this.m_heatingPulseGroupCount; num28++)
					{
						WaterManager.PulseGroup pulseGroup14 = this.m_heatingPulseGroups[num28];
						if (pulseGroup14.m_mergeIndex != 65535)
						{
							WaterManager.PulseGroup pulseGroup15 = this.m_heatingPulseGroups[(int)pulseGroup14.m_mergeIndex];
							pulseGroup14.m_curPressure = (uint)((ulong)pulseGroup15.m_curPressure * (ulong)pulseGroup14.m_origPressure / (ulong)pulseGroup15.m_origPressure);
							pulseGroup15.m_curPressure -= pulseGroup14.m_curPressure;
							pulseGroup15.m_origPressure -= pulseGroup14.m_origPressure;
							this.m_heatingPulseGroups[(int)pulseGroup14.m_mergeIndex] = pulseGroup15;
							this.m_heatingPulseGroups[num28] = pulseGroup14;
						}
					}
					for (int num29 = 0; num29 < this.m_heatingPulseGroupCount; num29++)
					{
						WaterManager.PulseGroup pulseGroup16 = this.m_heatingPulseGroups[num29];
						if (pulseGroup16.m_curPressure != 0u)
						{
							WaterManager.Node node4 = this.m_nodeData[(int)pulseGroup16.m_node];
							node4.m_extraHeatingPressure += (ushort)Mathf.Min((int)pulseGroup16.m_curPressure, (int)(32767 - node4.m_extraHeatingPressure));
							this.m_nodeData[(int)pulseGroup16.m_node] = node4;
						}
					}
				}
			}
		}
	}

	public void UpdateGrid(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)(minX / 38.25f + 128f), 0);
		int num2 = Mathf.Max((int)(minZ / 38.25f + 128f), 0);
		int num3 = Mathf.Min((int)(maxX / 38.25f + 128f), 255);
		int num4 = Mathf.Min((int)(maxZ / 38.25f + 128f), 255);
		for (int i = num2; i <= num4; i++)
		{
			int num5 = i * 256 + num;
			for (int j = num; j <= num3; j++)
			{
				this.m_waterGrid[num5].m_conductivity = 0;
				this.m_waterGrid[num5].m_conductivity2 = 0;
				this.m_waterGrid[num5].m_closestPipeSegment = 0;
				this.m_waterGrid[num5].m_closestPipeSegment2 = 0;
				num5++;
			}
		}
		float num6 = ((float)num - 128f) * 38.25f - 100f;
		float num7 = ((float)num2 - 128f) * 38.25f - 100f;
		float num8 = ((float)num3 - 128f + 1f) * 38.25f + 100f;
		float num9 = ((float)num4 - 128f + 1f) * 38.25f + 100f;
		int num10 = Mathf.Max((int)(num6 / 64f + 135f), 0);
		int num11 = Mathf.Max((int)(num7 / 64f + 135f), 0);
		int num12 = Mathf.Min((int)(num8 / 64f + 135f), 269);
		int num13 = Mathf.Min((int)(num9 / 64f + 135f), 269);
		float num14 = 100f;
		Array16<NetNode> nodes = Singleton<NetManager>.get_instance().m_nodes;
		Array16<NetSegment> segments = Singleton<NetManager>.get_instance().m_segments;
		ushort[] segmentGrid = Singleton<NetManager>.get_instance().m_segmentGrid;
		for (int k = num11; k <= num13; k++)
		{
			for (int l = num10; l <= num12; l++)
			{
				ushort num15 = segmentGrid[k * 270 + l];
				int num16 = 0;
				while (num15 != 0)
				{
					NetSegment.Flags flags = segments.m_buffer[(int)num15].m_flags;
					if ((flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created)
					{
						NetInfo info = segments.m_buffer[(int)num15].Info;
						if (info.m_class.m_service == ItemClass.Service.Water && info.m_class.m_level <= ItemClass.Level.Level2)
						{
							ushort startNode = segments.m_buffer[(int)num15].m_startNode;
							ushort endNode = segments.m_buffer[(int)num15].m_endNode;
							Vector2 vector = VectorUtils.XZ(nodes.m_buffer[(int)startNode].m_position);
							Vector2 vector2 = VectorUtils.XZ(nodes.m_buffer[(int)endNode].m_position);
							float num17 = Mathf.Max(Mathf.Max(num6 - vector.x, num7 - vector.y), Mathf.Max(vector.x - num8, vector.y - num9));
							float num18 = Mathf.Max(Mathf.Max(num6 - vector2.x, num7 - vector2.y), Mathf.Max(vector2.x - num8, vector2.y - num9));
							if (num17 < 0f || num18 < 0f)
							{
								int num19 = Mathf.Max((int)((Mathf.Min(vector.x, vector2.x) - num14) / 38.25f + 128f), num);
								int num20 = Mathf.Max((int)((Mathf.Min(vector.y, vector2.y) - num14) / 38.25f + 128f), num2);
								int num21 = Mathf.Min((int)((Mathf.Max(vector.x, vector2.x) + num14) / 38.25f + 128f), num3);
								int num22 = Mathf.Min((int)((Mathf.Max(vector.y, vector2.y) + num14) / 38.25f + 128f), num4);
								for (int m = num20; m <= num22; m++)
								{
									int num23 = m * 256 + num19;
									float num24 = ((float)m + 0.5f - 128f) * 38.25f;
									for (int n = num19; n <= num21; n++)
									{
										float num25 = ((float)n + 0.5f - 128f) * 38.25f;
										float num27;
										float num26 = Segment2.DistanceSqr(vector, vector2, new Vector2(num25, num24), ref num27);
										num26 = Mathf.Sqrt(num26);
										if (num26 < num14 + 19.125f)
										{
											float num28 = (num14 - num26) * 0.0130718956f + 0.25f;
											int num29 = Mathf.Min(255, Mathf.RoundToInt(num28 * 255f));
											if (num29 > (int)this.m_waterGrid[num23].m_conductivity)
											{
												this.m_waterGrid[num23].m_conductivity = (byte)num29;
												this.m_waterGrid[num23].m_closestPipeSegment = num15;
											}
											if (info.m_class.m_level == ItemClass.Level.Level2 && num29 > (int)this.m_waterGrid[num23].m_conductivity2)
											{
												this.m_waterGrid[num23].m_conductivity2 = (byte)num29;
												this.m_waterGrid[num23].m_closestPipeSegment2 = num15;
											}
										}
										num23++;
									}
								}
							}
						}
					}
					num15 = segments.m_buffer[(int)num15].m_nextGridSegment;
					if (++num16 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		for (int num30 = num2; num30 <= num4; num30++)
		{
			int num31 = num30 * 256 + num;
			for (int num32 = num; num32 <= num3; num32++)
			{
				WaterManager.Cell cell = this.m_waterGrid[num31];
				if (cell.m_conductivity == 0)
				{
					cell.m_currentWaterPressure = 0;
					cell.m_currentSewagePressure = 0;
					cell.m_currentHeatingPressure = 0;
					cell.m_waterPulseGroup = 65535;
					cell.m_sewagePulseGroup = 65535;
					cell.m_heatingPulseGroup = 65535;
					cell.m_tmpHasWater = false;
					cell.m_tmpHasSewage = false;
					cell.m_tmpHasHeating = false;
					cell.m_hasWater = false;
					cell.m_hasSewage = false;
					cell.m_hasHeating = false;
					cell.m_pollution = 0;
					this.m_waterGrid[num31] = cell;
				}
				else if (cell.m_conductivity2 == 0)
				{
					cell.m_currentHeatingPressure = 0;
					cell.m_heatingPulseGroup = 65535;
					cell.m_tmpHasHeating = false;
					cell.m_hasHeating = false;
					this.m_waterGrid[num31] = cell;
				}
				num31++;
			}
		}
		this.AreaModified(num, num2, num3, num4);
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new WaterManager.Data());
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("WaterManager.UpdateData");
		base.UpdateData(mode);
		if (this.m_refreshGrid)
		{
			this.UpdateGrid(-100000f, -100000f, 100000f, 100000f);
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_waterPumpMissingGuide == null)
		{
			this.m_waterPumpMissingGuide = new BuildingTypeGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_drainPipeMissingGuide == null)
		{
			this.m_drainPipeMissingGuide = new BuildingTypeGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public void AreaModified(int minX, int minZ, int maxX, int maxZ)
	{
		while (!Monitor.TryEnter(this.m_waterGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_modifiedX1 = Mathf.Min(this.m_modifiedX1, minX);
			this.m_modifiedZ1 = Mathf.Min(this.m_modifiedZ1, minZ);
			this.m_modifiedX2 = Mathf.Max(this.m_modifiedX2, maxX);
			this.m_modifiedZ2 = Mathf.Max(this.m_modifiedZ2, maxZ);
		}
		finally
		{
			Monitor.Exit(this.m_waterGrid);
		}
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float WATERGRID_CELL_SIZE = 38.25f;

	public const int WATERGRID_RESOLUTION = 256;

	public const int MAX_PULSE_GROUPS = 1024;

	public const int CONDUCTIVITY_LIMIT = 96;

	[NonSerialized]
	public WaterManager.Node[] m_nodeData;

	private WaterManager.Cell[] m_waterGrid;

	private WaterManager.PulseGroup[] m_waterPulseGroups;

	private WaterManager.PulseGroup[] m_sewagePulseGroups;

	private WaterManager.PulseGroup[] m_heatingPulseGroups;

	private WaterManager.PulseUnit[] m_waterPulseUnits;

	private WaterManager.PulseUnit[] m_sewagePulseUnits;

	private WaterManager.PulseUnit[] m_heatingPulseUnits;

	private Texture2D m_waterTexture;

	private int m_waterPulseGroupCount;

	private int m_waterPulseUnitStart;

	private int m_waterPulseUnitEnd;

	private int m_sewagePulseGroupCount;

	private int m_sewagePulseUnitStart;

	private int m_sewagePulseUnitEnd;

	private int m_heatingPulseGroupCount;

	private int m_heatingPulseUnitStart;

	private int m_heatingPulseUnitEnd;

	private int m_processedCells;

	private int m_conductiveCells;

	private bool m_canContinue;

	private bool m_refreshGrid;

	private int m_modifiedX1;

	private int m_modifiedZ1;

	private int m_modifiedX2;

	private int m_modifiedZ2;

	private bool m_waterMapVisible;

	private bool m_heatingMapVisible;

	private bool m_lastMapWasHeating;

	private Camera m_undergroundCamera;

	public BuildingTypeGuide m_drainPipeMissingGuide;

	public BuildingTypeGuide m_waterPumpMissingGuide;

	public struct Cell
	{
		public short m_currentWaterPressure;

		public short m_currentSewagePressure;

		public short m_currentHeatingPressure;

		public ushort m_waterPulseGroup;

		public ushort m_sewagePulseGroup;

		public ushort m_heatingPulseGroup;

		public ushort m_closestPipeSegment;

		public ushort m_closestPipeSegment2;

		public byte m_conductivity;

		public byte m_conductivity2;

		public byte m_pollution;

		public bool m_tmpHasWater;

		public bool m_tmpHasSewage;

		public bool m_tmpHasHeating;

		public bool m_hasWater;

		public bool m_hasSewage;

		public bool m_hasHeating;
	}

	public struct PulseGroup
	{
		public uint m_origPressure;

		public uint m_curPressure;

		public uint m_collectPressure;

		public ushort m_mergeIndex;

		public ushort m_mergeCount;

		public ushort m_node;
	}

	public struct PulseUnit
	{
		public ushort m_group;

		public ushort m_node;

		public byte m_x;

		public byte m_z;
	}

	public struct Node
	{
		public ushort m_waterPulseGroup;

		public ushort m_sewagePulseGroup;

		public ushort m_heatingPulseGroup;

		public ushort m_curWaterPressure;

		public ushort m_curSewagePressure;

		public ushort m_curHeatingPressure;

		public ushort m_extraWaterPressure;

		public ushort m_extraSewagePressure;

		public ushort m_extraHeatingPressure;

		public ushort m_collectWaterPressure;

		public ushort m_collectSewagePressure;

		public byte m_pollution;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "WaterManager");
			WaterManager instance = Singleton<WaterManager>.get_instance();
			WaterManager.Cell[] waterGrid = instance.m_waterGrid;
			int num = waterGrid.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				@byte.Write(waterGrid[i].m_conductivity);
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int j = 0; j < num; j++)
			{
				byte2.Write(waterGrid[j].m_conductivity2);
			}
			byte2.EndWrite();
			EncodedArray.Short @short = EncodedArray.Short.BeginWrite(s);
			for (int k = 0; k < num; k++)
			{
				if (waterGrid[k].m_conductivity != 0)
				{
					@short.Write(waterGrid[k].m_currentWaterPressure);
				}
			}
			@short.EndWrite();
			EncodedArray.Short short2 = EncodedArray.Short.BeginWrite(s);
			for (int l = 0; l < num; l++)
			{
				if (waterGrid[l].m_conductivity != 0)
				{
					short2.Write(waterGrid[l].m_currentSewagePressure);
				}
			}
			short2.EndWrite();
			EncodedArray.Short short3 = EncodedArray.Short.BeginWrite(s);
			for (int m = 0; m < num; m++)
			{
				if (waterGrid[m].m_conductivity2 != 0)
				{
					short3.Write(waterGrid[m].m_currentHeatingPressure);
				}
			}
			short3.EndWrite();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int n = 0; n < num; n++)
			{
				if (waterGrid[n].m_conductivity != 0)
				{
					uShort.Write(waterGrid[n].m_waterPulseGroup);
				}
			}
			uShort.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int num2 = 0; num2 < num; num2++)
			{
				if (waterGrid[num2].m_conductivity != 0)
				{
					uShort2.Write(waterGrid[num2].m_sewagePulseGroup);
				}
			}
			uShort2.EndWrite();
			EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginWrite(s);
			for (int num3 = 0; num3 < num; num3++)
			{
				if (waterGrid[num3].m_conductivity2 != 0)
				{
					uShort3.Write(waterGrid[num3].m_heatingPulseGroup);
				}
			}
			uShort3.EndWrite();
			EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginWrite(s);
			for (int num4 = 0; num4 < num; num4++)
			{
				if (waterGrid[num4].m_conductivity != 0)
				{
					uShort4.Write(waterGrid[num4].m_closestPipeSegment);
				}
			}
			uShort4.EndWrite();
			EncodedArray.UShort uShort5 = EncodedArray.UShort.BeginWrite(s);
			for (int num5 = 0; num5 < num; num5++)
			{
				if (waterGrid[num5].m_conductivity2 != 0)
				{
					uShort5.Write(waterGrid[num5].m_closestPipeSegment2);
				}
			}
			uShort5.EndWrite();
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginWrite(s);
			for (int num6 = 0; num6 < num; num6++)
			{
				if (waterGrid[num6].m_conductivity != 0)
				{
					@bool.Write(waterGrid[num6].m_hasWater);
				}
			}
			@bool.EndWrite();
			EncodedArray.Bool bool2 = EncodedArray.Bool.BeginWrite(s);
			for (int num7 = 0; num7 < num; num7++)
			{
				if (waterGrid[num7].m_conductivity != 0)
				{
					bool2.Write(waterGrid[num7].m_hasSewage);
				}
			}
			bool2.EndWrite();
			EncodedArray.Bool bool3 = EncodedArray.Bool.BeginWrite(s);
			for (int num8 = 0; num8 < num; num8++)
			{
				if (waterGrid[num8].m_conductivity2 != 0)
				{
					bool3.Write(waterGrid[num8].m_hasHeating);
				}
			}
			bool3.EndWrite();
			EncodedArray.Bool bool4 = EncodedArray.Bool.BeginWrite(s);
			for (int num9 = 0; num9 < num; num9++)
			{
				if (waterGrid[num9].m_conductivity != 0)
				{
					bool4.Write(waterGrid[num9].m_tmpHasWater);
				}
			}
			bool4.EndWrite();
			EncodedArray.Bool bool5 = EncodedArray.Bool.BeginWrite(s);
			for (int num10 = 0; num10 < num; num10++)
			{
				if (waterGrid[num10].m_conductivity != 0)
				{
					bool5.Write(waterGrid[num10].m_tmpHasSewage);
				}
			}
			bool5.EndWrite();
			EncodedArray.Bool bool6 = EncodedArray.Bool.BeginWrite(s);
			for (int num11 = 0; num11 < num; num11++)
			{
				if (waterGrid[num11].m_conductivity2 != 0)
				{
					bool6.Write(waterGrid[num11].m_tmpHasHeating);
				}
			}
			bool6.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int num12 = 0; num12 < num; num12++)
			{
				if (waterGrid[num12].m_conductivity != 0)
				{
					byte3.Write(waterGrid[num12].m_pollution);
				}
			}
			byte3.EndWrite();
			s.WriteUInt16((uint)instance.m_waterPulseGroupCount);
			for (int num13 = 0; num13 < instance.m_waterPulseGroupCount; num13++)
			{
				s.WriteUInt32(instance.m_waterPulseGroups[num13].m_origPressure);
				s.WriteUInt32(instance.m_waterPulseGroups[num13].m_curPressure);
				s.WriteUInt32(instance.m_waterPulseGroups[num13].m_collectPressure);
				s.WriteUInt16((uint)instance.m_waterPulseGroups[num13].m_mergeIndex);
				s.WriteUInt16((uint)instance.m_waterPulseGroups[num13].m_mergeCount);
				s.WriteUInt16((uint)instance.m_waterPulseGroups[num13].m_node);
			}
			s.WriteUInt16((uint)instance.m_sewagePulseGroupCount);
			for (int num14 = 0; num14 < instance.m_sewagePulseGroupCount; num14++)
			{
				s.WriteUInt32(instance.m_sewagePulseGroups[num14].m_origPressure);
				s.WriteUInt32(instance.m_sewagePulseGroups[num14].m_curPressure);
				s.WriteUInt32(instance.m_sewagePulseGroups[num14].m_collectPressure);
				s.WriteUInt16((uint)instance.m_sewagePulseGroups[num14].m_mergeIndex);
				s.WriteUInt16((uint)instance.m_sewagePulseGroups[num14].m_mergeCount);
				s.WriteUInt16((uint)instance.m_sewagePulseGroups[num14].m_node);
			}
			s.WriteUInt16((uint)instance.m_heatingPulseGroupCount);
			for (int num15 = 0; num15 < instance.m_heatingPulseGroupCount; num15++)
			{
				s.WriteUInt32(instance.m_heatingPulseGroups[num15].m_origPressure);
				s.WriteUInt32(instance.m_heatingPulseGroups[num15].m_curPressure);
				s.WriteUInt16((uint)instance.m_heatingPulseGroups[num15].m_mergeIndex);
				s.WriteUInt16((uint)instance.m_heatingPulseGroups[num15].m_mergeCount);
				s.WriteUInt16((uint)instance.m_heatingPulseGroups[num15].m_node);
			}
			int num16 = instance.m_waterPulseUnitEnd - instance.m_waterPulseUnitStart;
			if (num16 < 0)
			{
				num16 += instance.m_waterPulseUnits.Length;
			}
			s.WriteUInt16((uint)num16);
			int num17 = instance.m_waterPulseUnitStart;
			while (num17 != instance.m_waterPulseUnitEnd)
			{
				s.WriteUInt16((uint)instance.m_waterPulseUnits[num17].m_group);
				s.WriteUInt16((uint)instance.m_waterPulseUnits[num17].m_node);
				s.WriteUInt8((uint)instance.m_waterPulseUnits[num17].m_x);
				s.WriteUInt8((uint)instance.m_waterPulseUnits[num17].m_z);
				if (++num17 >= instance.m_waterPulseUnits.Length)
				{
					num17 = 0;
				}
			}
			int num18 = instance.m_sewagePulseUnitEnd - instance.m_sewagePulseUnitStart;
			if (num18 < 0)
			{
				num18 += instance.m_sewagePulseUnits.Length;
			}
			s.WriteUInt16((uint)num18);
			int num19 = instance.m_sewagePulseUnitStart;
			while (num19 != instance.m_sewagePulseUnitEnd)
			{
				s.WriteUInt16((uint)instance.m_sewagePulseUnits[num19].m_group);
				s.WriteUInt16((uint)instance.m_sewagePulseUnits[num19].m_node);
				s.WriteUInt8((uint)instance.m_sewagePulseUnits[num19].m_x);
				s.WriteUInt8((uint)instance.m_sewagePulseUnits[num19].m_z);
				if (++num19 >= instance.m_sewagePulseUnits.Length)
				{
					num19 = 0;
				}
			}
			int num20 = instance.m_heatingPulseUnitEnd - instance.m_heatingPulseUnitStart;
			if (num20 < 0)
			{
				num20 += instance.m_heatingPulseUnits.Length;
			}
			s.WriteUInt16((uint)num20);
			int num21 = instance.m_heatingPulseUnitStart;
			while (num21 != instance.m_heatingPulseUnitEnd)
			{
				s.WriteUInt16((uint)instance.m_heatingPulseUnits[num21].m_group);
				s.WriteUInt16((uint)instance.m_heatingPulseUnits[num21].m_node);
				s.WriteUInt8((uint)instance.m_heatingPulseUnits[num21].m_x);
				s.WriteUInt8((uint)instance.m_heatingPulseUnits[num21].m_z);
				if (++num21 >= instance.m_heatingPulseUnits.Length)
				{
					num21 = 0;
				}
			}
			EncodedArray.UShort uShort6 = EncodedArray.UShort.BeginWrite(s);
			for (int num22 = 0; num22 < 32768; num22++)
			{
				uShort6.Write(instance.m_nodeData[num22].m_waterPulseGroup);
			}
			uShort6.EndWrite();
			EncodedArray.UShort uShort7 = EncodedArray.UShort.BeginWrite(s);
			for (int num23 = 0; num23 < 32768; num23++)
			{
				uShort7.Write(instance.m_nodeData[num23].m_curWaterPressure);
			}
			uShort7.EndWrite();
			EncodedArray.UShort uShort8 = EncodedArray.UShort.BeginWrite(s);
			for (int num24 = 0; num24 < 32768; num24++)
			{
				uShort8.Write(instance.m_nodeData[num24].m_extraWaterPressure);
			}
			uShort8.EndWrite();
			EncodedArray.UShort uShort9 = EncodedArray.UShort.BeginWrite(s);
			for (int num25 = 0; num25 < 32768; num25++)
			{
				uShort9.Write(instance.m_nodeData[num25].m_sewagePulseGroup);
			}
			uShort9.EndWrite();
			EncodedArray.UShort uShort10 = EncodedArray.UShort.BeginWrite(s);
			for (int num26 = 0; num26 < 32768; num26++)
			{
				uShort10.Write(instance.m_nodeData[num26].m_curSewagePressure);
			}
			uShort10.EndWrite();
			EncodedArray.UShort uShort11 = EncodedArray.UShort.BeginWrite(s);
			for (int num27 = 0; num27 < 32768; num27++)
			{
				uShort11.Write(instance.m_nodeData[num27].m_extraSewagePressure);
			}
			uShort11.EndWrite();
			EncodedArray.UShort uShort12 = EncodedArray.UShort.BeginWrite(s);
			for (int num28 = 0; num28 < 32768; num28++)
			{
				uShort12.Write(instance.m_nodeData[num28].m_heatingPulseGroup);
			}
			uShort12.EndWrite();
			EncodedArray.UShort uShort13 = EncodedArray.UShort.BeginWrite(s);
			for (int num29 = 0; num29 < 32768; num29++)
			{
				uShort13.Write(instance.m_nodeData[num29].m_curHeatingPressure);
			}
			uShort13.EndWrite();
			EncodedArray.UShort uShort14 = EncodedArray.UShort.BeginWrite(s);
			for (int num30 = 0; num30 < 32768; num30++)
			{
				uShort14.Write(instance.m_nodeData[num30].m_extraHeatingPressure);
			}
			uShort14.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int num31 = 0; num31 < 32768; num31++)
			{
				byte4.Write(instance.m_nodeData[num31].m_pollution);
			}
			byte4.EndWrite();
			EncodedArray.UShort uShort15 = EncodedArray.UShort.BeginWrite(s);
			for (int num32 = 0; num32 < 32768; num32++)
			{
				uShort15.Write(instance.m_nodeData[num32].m_collectWaterPressure);
			}
			uShort15.EndWrite();
			EncodedArray.UShort uShort16 = EncodedArray.UShort.BeginWrite(s);
			for (int num33 = 0; num33 < 32768; num33++)
			{
				uShort16.Write(instance.m_nodeData[num33].m_collectSewagePressure);
			}
			uShort16.EndWrite();
			s.WriteInt32(instance.m_processedCells);
			s.WriteInt32(instance.m_conductiveCells);
			s.WriteBool(instance.m_canContinue);
			s.WriteObject<BuildingTypeGuide>(instance.m_waterPumpMissingGuide);
			s.WriteObject<BuildingTypeGuide>(instance.m_drainPipeMissingGuide);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "WaterManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "WaterManager");
			WaterManager instance = Singleton<WaterManager>.get_instance();
			WaterManager.Cell[] waterGrid = instance.m_waterGrid;
			int num = waterGrid.Length;
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				waterGrid[i].m_conductivity = @byte.Read();
			}
			@byte.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int j = 0; j < num; j++)
				{
					waterGrid[j].m_conductivity2 = byte2.Read();
				}
				byte2.EndRead();
			}
			else
			{
				for (int k = 0; k < num; k++)
				{
					waterGrid[k].m_conductivity2 = 0;
				}
			}
			EncodedArray.Short @short = EncodedArray.Short.BeginRead(s);
			for (int l = 0; l < num; l++)
			{
				if (waterGrid[l].m_conductivity != 0)
				{
					waterGrid[l].m_currentWaterPressure = @short.Read();
				}
				else
				{
					waterGrid[l].m_currentWaterPressure = 0;
				}
			}
			@short.EndRead();
			EncodedArray.Short short2 = EncodedArray.Short.BeginRead(s);
			for (int m = 0; m < num; m++)
			{
				if (waterGrid[m].m_conductivity != 0)
				{
					waterGrid[m].m_currentSewagePressure = short2.Read();
				}
				else
				{
					waterGrid[m].m_currentSewagePressure = 0;
				}
			}
			short2.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.Short short3 = EncodedArray.Short.BeginRead(s);
				for (int n = 0; n < num; n++)
				{
					if (waterGrid[n].m_conductivity2 != 0)
					{
						waterGrid[n].m_currentHeatingPressure = short3.Read();
					}
					else
					{
						waterGrid[n].m_currentHeatingPressure = 0;
					}
				}
				short3.EndRead();
			}
			else
			{
				for (int num2 = 0; num2 < num; num2++)
				{
					waterGrid[num2].m_currentHeatingPressure = 0;
				}
			}
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int num3 = 0; num3 < num; num3++)
			{
				if (waterGrid[num3].m_conductivity != 0)
				{
					waterGrid[num3].m_waterPulseGroup = uShort.Read();
				}
				else
				{
					waterGrid[num3].m_waterPulseGroup = 65535;
				}
			}
			uShort.EndRead();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
			for (int num4 = 0; num4 < num; num4++)
			{
				if (waterGrid[num4].m_conductivity != 0)
				{
					waterGrid[num4].m_sewagePulseGroup = uShort2.Read();
				}
				else
				{
					waterGrid[num4].m_sewagePulseGroup = 65535;
				}
			}
			uShort2.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginRead(s);
				for (int num5 = 0; num5 < num; num5++)
				{
					if (waterGrid[num5].m_conductivity2 != 0)
					{
						waterGrid[num5].m_heatingPulseGroup = uShort3.Read();
					}
					else
					{
						waterGrid[num5].m_heatingPulseGroup = 65535;
					}
				}
				uShort3.EndRead();
			}
			else
			{
				for (int num6 = 0; num6 < num; num6++)
				{
					waterGrid[num6].m_heatingPulseGroup = 65535;
				}
			}
			if (s.get_version() >= 73u)
			{
				EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginRead(s);
				for (int num7 = 0; num7 < num; num7++)
				{
					if (waterGrid[num7].m_conductivity != 0)
					{
						waterGrid[num7].m_closestPipeSegment = uShort4.Read();
					}
					else
					{
						waterGrid[num7].m_closestPipeSegment = 0;
					}
				}
				uShort4.EndRead();
				instance.m_refreshGrid = false;
			}
			else
			{
				instance.m_refreshGrid = true;
			}
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort5 = EncodedArray.UShort.BeginRead(s);
				for (int num8 = 0; num8 < num; num8++)
				{
					if (waterGrid[num8].m_conductivity2 != 0)
					{
						waterGrid[num8].m_closestPipeSegment2 = uShort5.Read();
					}
					else
					{
						waterGrid[num8].m_closestPipeSegment2 = 0;
					}
				}
				uShort5.EndRead();
			}
			else
			{
				for (int num9 = 0; num9 < num; num9++)
				{
					waterGrid[num9].m_closestPipeSegment2 = 0;
				}
			}
			EncodedArray.Bool @bool = EncodedArray.Bool.BeginRead(s);
			for (int num10 = 0; num10 < num; num10++)
			{
				if (waterGrid[num10].m_conductivity != 0)
				{
					waterGrid[num10].m_hasWater = @bool.Read();
				}
				else
				{
					waterGrid[num10].m_hasWater = false;
				}
			}
			@bool.EndRead();
			EncodedArray.Bool bool2 = EncodedArray.Bool.BeginRead(s);
			for (int num11 = 0; num11 < num; num11++)
			{
				if (waterGrid[num11].m_conductivity != 0)
				{
					waterGrid[num11].m_hasSewage = bool2.Read();
				}
				else
				{
					waterGrid[num11].m_hasSewage = false;
				}
			}
			bool2.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.Bool bool3 = EncodedArray.Bool.BeginRead(s);
				for (int num12 = 0; num12 < num; num12++)
				{
					if (waterGrid[num12].m_conductivity2 != 0)
					{
						waterGrid[num12].m_hasHeating = bool3.Read();
					}
					else
					{
						waterGrid[num12].m_hasHeating = false;
					}
				}
				bool3.EndRead();
			}
			else
			{
				for (int num13 = 0; num13 < num; num13++)
				{
					waterGrid[num13].m_hasHeating = false;
				}
			}
			EncodedArray.Bool bool4 = EncodedArray.Bool.BeginRead(s);
			for (int num14 = 0; num14 < num; num14++)
			{
				if (waterGrid[num14].m_conductivity != 0)
				{
					waterGrid[num14].m_tmpHasWater = bool4.Read();
				}
				else
				{
					waterGrid[num14].m_tmpHasWater = false;
				}
			}
			bool4.EndRead();
			EncodedArray.Bool bool5 = EncodedArray.Bool.BeginRead(s);
			for (int num15 = 0; num15 < num; num15++)
			{
				if (waterGrid[num15].m_conductivity != 0)
				{
					waterGrid[num15].m_tmpHasSewage = bool5.Read();
				}
				else
				{
					waterGrid[num15].m_tmpHasSewage = false;
				}
			}
			bool5.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.Bool bool6 = EncodedArray.Bool.BeginRead(s);
				for (int num16 = 0; num16 < num; num16++)
				{
					if (waterGrid[num16].m_conductivity2 != 0)
					{
						waterGrid[num16].m_tmpHasHeating = bool6.Read();
					}
					else
					{
						waterGrid[num16].m_tmpHasHeating = false;
					}
				}
				bool6.EndRead();
			}
			else
			{
				for (int num17 = 0; num17 < num; num17++)
				{
					waterGrid[num17].m_tmpHasHeating = false;
				}
			}
			if (s.get_version() >= 23u)
			{
				EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
				for (int num18 = 0; num18 < num; num18++)
				{
					if (waterGrid[num18].m_conductivity != 0)
					{
						waterGrid[num18].m_pollution = byte3.Read();
					}
					else
					{
						waterGrid[num18].m_pollution = 0;
					}
				}
				byte3.EndRead();
			}
			instance.m_waterPulseGroupCount = (int)s.ReadUInt16();
			for (int num19 = 0; num19 < instance.m_waterPulseGroupCount; num19++)
			{
				instance.m_waterPulseGroups[num19].m_origPressure = s.ReadUInt32();
				instance.m_waterPulseGroups[num19].m_curPressure = s.ReadUInt32();
				if (s.get_version() >= 270u)
				{
					instance.m_waterPulseGroups[num19].m_collectPressure = s.ReadUInt32();
				}
				else
				{
					instance.m_waterPulseGroups[num19].m_collectPressure = 0u;
				}
				instance.m_waterPulseGroups[num19].m_mergeIndex = (ushort)s.ReadUInt16();
				instance.m_waterPulseGroups[num19].m_mergeCount = (ushort)s.ReadUInt16();
				instance.m_waterPulseGroups[num19].m_node = (ushort)s.ReadUInt16();
			}
			instance.m_sewagePulseGroupCount = (int)s.ReadUInt16();
			for (int num20 = 0; num20 < instance.m_sewagePulseGroupCount; num20++)
			{
				instance.m_sewagePulseGroups[num20].m_origPressure = s.ReadUInt32();
				instance.m_sewagePulseGroups[num20].m_curPressure = s.ReadUInt32();
				if (s.get_version() >= 306u)
				{
					instance.m_sewagePulseGroups[num20].m_collectPressure = s.ReadUInt32();
				}
				else
				{
					instance.m_sewagePulseGroups[num20].m_collectPressure = 0u;
				}
				instance.m_sewagePulseGroups[num20].m_mergeIndex = (ushort)s.ReadUInt16();
				instance.m_sewagePulseGroups[num20].m_mergeCount = (ushort)s.ReadUInt16();
				instance.m_sewagePulseGroups[num20].m_node = (ushort)s.ReadUInt16();
			}
			if (s.get_version() >= 227u)
			{
				instance.m_heatingPulseGroupCount = (int)s.ReadUInt16();
				for (int num21 = 0; num21 < instance.m_heatingPulseGroupCount; num21++)
				{
					instance.m_heatingPulseGroups[num21].m_origPressure = s.ReadUInt32();
					instance.m_heatingPulseGroups[num21].m_curPressure = s.ReadUInt32();
					instance.m_heatingPulseGroups[num21].m_mergeIndex = (ushort)s.ReadUInt16();
					instance.m_heatingPulseGroups[num21].m_mergeCount = (ushort)s.ReadUInt16();
					instance.m_heatingPulseGroups[num21].m_node = (ushort)s.ReadUInt16();
				}
			}
			else
			{
				instance.m_heatingPulseGroupCount = 0;
			}
			int num22 = (int)s.ReadUInt16();
			instance.m_waterPulseUnitStart = 0;
			instance.m_waterPulseUnitEnd = num22 % instance.m_waterPulseUnits.Length;
			for (int num23 = 0; num23 < num22; num23++)
			{
				instance.m_waterPulseUnits[num23].m_group = (ushort)s.ReadUInt16();
				instance.m_waterPulseUnits[num23].m_node = (ushort)s.ReadUInt16();
				instance.m_waterPulseUnits[num23].m_x = (byte)s.ReadUInt8();
				instance.m_waterPulseUnits[num23].m_z = (byte)s.ReadUInt8();
			}
			int num24 = (int)s.ReadUInt16();
			instance.m_sewagePulseUnitStart = 0;
			instance.m_sewagePulseUnitEnd = num24 % instance.m_sewagePulseUnits.Length;
			for (int num25 = 0; num25 < num24; num25++)
			{
				instance.m_sewagePulseUnits[num25].m_group = (ushort)s.ReadUInt16();
				instance.m_sewagePulseUnits[num25].m_node = (ushort)s.ReadUInt16();
				instance.m_sewagePulseUnits[num25].m_x = (byte)s.ReadUInt8();
				instance.m_sewagePulseUnits[num25].m_z = (byte)s.ReadUInt8();
			}
			if (s.get_version() >= 227u)
			{
				int num26 = (int)s.ReadUInt16();
				instance.m_heatingPulseUnitStart = 0;
				instance.m_heatingPulseUnitEnd = num26 % instance.m_heatingPulseUnits.Length;
				for (int num27 = 0; num27 < num26; num27++)
				{
					instance.m_heatingPulseUnits[num27].m_group = (ushort)s.ReadUInt16();
					instance.m_heatingPulseUnits[num27].m_node = (ushort)s.ReadUInt16();
					instance.m_heatingPulseUnits[num27].m_x = (byte)s.ReadUInt8();
					instance.m_heatingPulseUnits[num27].m_z = (byte)s.ReadUInt8();
				}
			}
			else
			{
				instance.m_heatingPulseUnitStart = 0;
				instance.m_heatingPulseUnitEnd = 0;
			}
			int num28;
			if (s.get_version() < 202u)
			{
				num28 = 16384;
				for (int num29 = num28; num29 < 32768; num29++)
				{
					instance.m_nodeData[num29] = default(WaterManager.Node);
				}
			}
			else
			{
				num28 = 32768;
			}
			EncodedArray.UShort uShort6 = EncodedArray.UShort.BeginRead(s);
			for (int num30 = 0; num30 < num28; num30++)
			{
				instance.m_nodeData[num30].m_waterPulseGroup = uShort6.Read();
			}
			uShort6.EndRead();
			EncodedArray.UShort uShort7 = EncodedArray.UShort.BeginRead(s);
			for (int num31 = 0; num31 < num28; num31++)
			{
				instance.m_nodeData[num31].m_curWaterPressure = uShort7.Read();
			}
			uShort7.EndRead();
			EncodedArray.UShort uShort8 = EncodedArray.UShort.BeginRead(s);
			for (int num32 = 0; num32 < num28; num32++)
			{
				instance.m_nodeData[num32].m_extraWaterPressure = uShort8.Read();
			}
			uShort8.EndRead();
			EncodedArray.UShort uShort9 = EncodedArray.UShort.BeginRead(s);
			for (int num33 = 0; num33 < num28; num33++)
			{
				instance.m_nodeData[num33].m_sewagePulseGroup = uShort9.Read();
			}
			uShort9.EndRead();
			EncodedArray.UShort uShort10 = EncodedArray.UShort.BeginRead(s);
			for (int num34 = 0; num34 < num28; num34++)
			{
				instance.m_nodeData[num34].m_curSewagePressure = uShort10.Read();
			}
			uShort10.EndRead();
			EncodedArray.UShort uShort11 = EncodedArray.UShort.BeginRead(s);
			for (int num35 = 0; num35 < num28; num35++)
			{
				instance.m_nodeData[num35].m_extraSewagePressure = uShort11.Read();
			}
			uShort11.EndRead();
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort12 = EncodedArray.UShort.BeginRead(s);
				for (int num36 = 0; num36 < num28; num36++)
				{
					instance.m_nodeData[num36].m_heatingPulseGroup = uShort12.Read();
				}
				uShort12.EndRead();
			}
			else
			{
				for (int num37 = 0; num37 < num28; num37++)
				{
					instance.m_nodeData[num37].m_heatingPulseGroup = 65535;
				}
			}
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort13 = EncodedArray.UShort.BeginRead(s);
				for (int num38 = 0; num38 < num28; num38++)
				{
					instance.m_nodeData[num38].m_curHeatingPressure = uShort13.Read();
				}
				uShort13.EndRead();
			}
			else
			{
				for (int num39 = 0; num39 < num28; num39++)
				{
					instance.m_nodeData[num39].m_curHeatingPressure = 0;
				}
			}
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort14 = EncodedArray.UShort.BeginRead(s);
				for (int num40 = 0; num40 < num28; num40++)
				{
					instance.m_nodeData[num40].m_extraHeatingPressure = uShort14.Read();
				}
				uShort14.EndRead();
			}
			else
			{
				for (int num41 = 0; num41 < num28; num41++)
				{
					instance.m_nodeData[num41].m_extraHeatingPressure = 0;
				}
			}
			if (s.get_version() >= 23u)
			{
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int num42 = 0; num42 < num28; num42++)
				{
					instance.m_nodeData[num42].m_pollution = byte4.Read();
				}
				byte4.EndRead();
			}
			if (s.get_version() >= 271u)
			{
				EncodedArray.UShort uShort15 = EncodedArray.UShort.BeginRead(s);
				for (int num43 = 0; num43 < num28; num43++)
				{
					instance.m_nodeData[num43].m_collectWaterPressure = uShort15.Read();
				}
				uShort15.EndRead();
			}
			else if (s.get_version() >= 270u)
			{
				EncodedArray.Bool bool7 = EncodedArray.Bool.BeginRead(s);
				for (int num44 = 0; num44 < num28; num44++)
				{
					if (bool7.Read())
					{
						instance.m_nodeData[num44].m_collectWaterPressure = instance.m_nodeData[num44].m_curWaterPressure;
					}
					else
					{
						instance.m_nodeData[num44].m_collectWaterPressure = 0;
					}
				}
				bool7.EndRead();
			}
			else
			{
				for (int num45 = 0; num45 < num28; num45++)
				{
					instance.m_nodeData[num45].m_collectWaterPressure = 0;
				}
			}
			if (s.get_version() >= 272u)
			{
				EncodedArray.UShort uShort16 = EncodedArray.UShort.BeginRead(s);
				for (int num46 = 0; num46 < num28; num46++)
				{
					instance.m_nodeData[num46].m_collectSewagePressure = uShort16.Read();
				}
				uShort16.EndRead();
			}
			else
			{
				for (int num47 = 0; num47 < num28; num47++)
				{
					instance.m_nodeData[num47].m_collectSewagePressure = 0;
				}
			}
			instance.m_processedCells = s.ReadInt32();
			instance.m_conductiveCells = s.ReadInt32();
			instance.m_canContinue = s.ReadBool();
			if (s.get_version() >= 94u)
			{
				instance.m_waterPumpMissingGuide = s.ReadObject<BuildingTypeGuide>();
				instance.m_drainPipeMissingGuide = s.ReadObject<BuildingTypeGuide>();
			}
			else
			{
				instance.m_waterPumpMissingGuide = null;
				instance.m_drainPipeMissingGuide = null;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "WaterManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "WaterManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			WaterManager instance = Singleton<WaterManager>.get_instance();
			instance.AreaModified(0, 0, 255, 255);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "WaterManager");
		}
	}
}
