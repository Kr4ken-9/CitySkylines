﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class BarChartBar
{
	public BarChartBar(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, UIComponent container, Color32 color, bool overlay = false)
	{
		this.m_Service = service;
		this.m_SubService = new ItemClass.SubService[]
		{
			subService
		};
		this.m_Level = level;
		UIComponent uIComponent = UITemplateManager.Get<UIComponent>("BarChartBar");
		container.AttachUIComponent(uIComponent.get_gameObject());
		this.m_progressBar = uIComponent.Find<UIProgressBar>("ProgressBar");
		this.m_progressBar.set_progressColor(color);
		this.m_valueLabel = uIComponent.Find<UILabel>("ValueLabel");
		if (overlay)
		{
			this.m_valueLabel.Hide();
		}
		this.m_overlay = overlay;
	}

	public BarChartBar(ItemClass.Service service, ItemClass.SubService[] subService, ItemClass.Level level, UIComponent container, Color32 color, bool overlay = false)
	{
		this.m_Service = service;
		this.m_SubService = subService;
		this.m_Level = level;
		UIComponent uIComponent = UITemplateManager.Get<UIComponent>("BarChartBar");
		container.AttachUIComponent(uIComponent.get_gameObject());
		this.m_progressBar = uIComponent.Find<UIProgressBar>("ProgressBar");
		this.m_progressBar.set_progressColor(color);
		this.m_valueLabel = uIComponent.Find<UILabel>("ValueLabel");
		if (overlay)
		{
			this.m_valueLabel.Hide();
		}
		this.m_overlay = overlay;
	}

	public long PollIncome()
	{
		long num = 0L;
		if (Singleton<EconomyManager>.get_exists())
		{
			for (int i = 0; i < this.m_SubService.Length; i++)
			{
				long num2 = 0L;
				long num3 = 0L;
				Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(this.m_Service, this.m_SubService[i], this.m_Level, out num2, out num3);
				num += num2;
			}
			if (this.m_Service == ItemClass.Service.FireDepartment)
			{
				for (int j = 0; j < this.m_SubService.Length; j++)
				{
					long num4 = 0L;
					long num5 = 0L;
					Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(ItemClass.Service.Disaster, this.m_SubService[j], this.m_Level, out num4, out num5);
					num += num4;
				}
			}
		}
		return num;
	}

	public void SetValue(long value, long highestValueInGroup)
	{
		this.m_valueLabel.set_text(((double)value / 100.0).ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()));
		float num = (float)value / (float)highestValueInGroup;
		if (!this.m_overlay && num < 0.01f)
		{
			num = 0.01f;
		}
		this.m_progressBar.set_value(num);
	}

	private ItemClass.Service m_Service;

	private ItemClass.SubService[] m_SubService;

	private ItemClass.Level m_Level;

	private UIComponent m_root;

	private UIProgressBar m_progressBar;

	private UILabel m_valueLabel;

	private bool m_overlay;
}
