﻿using System;
using ColossalFramework.IO;

public abstract class StatisticBase : IDataContainer
{
	public abstract void Reset();

	public abstract StatisticBase Acquire<T>(int index, int count) where T : StatisticBase, new();

	public abstract void Add(int delta);

	public abstract void Set(int value);

	public abstract void Add(long delta);

	public abstract void Set(long value);

	public abstract void Add(float delta);

	public abstract void Set(float value);

	public abstract void Tick();

	public abstract void Serialize(DataSerializer s);

	public abstract void Deserialize(DataSerializer s);

	public abstract void AfterDeserialize(DataSerializer s);

	public abstract StatisticBase Get(int index);

	public abstract int GetLatestInt32();

	public abstract int GetTotalInt32();

	public abstract long GetLatestInt64();

	public abstract long GetTotalInt64();

	public abstract float GetLatestFloat();

	public abstract float GetTotalFloat();

	public abstract void GetValues(double[] buffer, out int count, out DateTime startTime, out DateTime endTime);

	public abstract void AddValues(double[] buffer, ref int count, ref DateTime startTime, ref DateTime endTime);
}
