﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class BuildingWrapper : IBuilding
{
	public BuildingWrapper(BuildingManager buildingManager)
	{
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.BindEvents();
	}

	private void GetImplementations()
	{
		this.OnBuildingExtensionsReleased();
		this.m_BuildingExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IBuildingExtension>();
		this.OnBuildingExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.UnBindEvents();
		this.OnBuildingExtensionsReleased();
	}

	private void BindEvents()
	{
		this.UnBindEvents();
		Singleton<BuildingManager>.get_instance().EventBuildingCreated += new BuildingManager.BuildingCreatedHandler(this.OnBuildingCreated);
		Singleton<BuildingManager>.get_instance().EventBuildingRelocated += new BuildingManager.BuildingRelocatedHandler(this.OnBuildingRelocated);
		Singleton<BuildingManager>.get_instance().EventBuildingReleased += new BuildingManager.BuildingReleasedHandler(this.OnBuildingReleased);
	}

	private void UnBindEvents()
	{
		Singleton<BuildingManager>.get_instance().EventBuildingCreated -= new BuildingManager.BuildingCreatedHandler(this.OnBuildingCreated);
		Singleton<BuildingManager>.get_instance().EventBuildingRelocated -= new BuildingManager.BuildingRelocatedHandler(this.OnBuildingRelocated);
		Singleton<BuildingManager>.get_instance().EventBuildingReleased -= new BuildingManager.BuildingReleasedHandler(this.OnBuildingReleased);
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnBuildingExtensionsCreated()
	{
		for (int i = 0; i < this.m_BuildingExtensions.Count; i++)
		{
			try
			{
				this.m_BuildingExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_BuildingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnBuildingExtensionsReleased()
	{
		for (int i = 0; i < this.m_BuildingExtensions.Count; i++)
		{
			try
			{
				this.m_BuildingExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_BuildingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	[DebuggerHidden]
	public IEnumerable<string> BuildingTypes()
	{
		BuildingWrapper.<BuildingTypes>c__Iterator0 <BuildingTypes>c__Iterator = new BuildingWrapper.<BuildingTypes>c__Iterator0();
		BuildingWrapper.<BuildingTypes>c__Iterator0 expr_07 = <BuildingTypes>c__Iterator;
		expr_07.$PC = -2;
		return expr_07;
	}

	public ushort CreateBuilding(Vector3 position, float angle, string name)
	{
		Task<BuildingInfo> task = ThreadHelper.get_dispatcher().Dispatch<BuildingInfo>(() => PrefabCollection<BuildingInfo>.FindLoaded(name));
		task.Wait();
		BuildingInfo result = task.get_result();
		if (result == null)
		{
			return 0;
		}
		ushort result2 = 0;
		if (Singleton<BuildingManager>.get_instance().CreateBuilding(out result2, ref Singleton<SimulationManager>.get_instance().m_randomizer, result, position, angle, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
		{
			Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
		}
		return result2;
	}

	public void RelocateBuilding(ushort building, Vector3 position, float angle)
	{
		Singleton<BuildingManager>.get_instance().RelocateBuilding(building, position, angle);
	}

	public void ReleaseBuilding(ushort building)
	{
		Singleton<BuildingManager>.get_instance().ReleaseBuilding(building);
	}

	public void OnCalculateSpawn(Vector3 position, ref ItemClass.Service service, ref ItemClass.SubService subService, ref ItemClass.Level level, ref ushort style)
	{
		if (this.m_BuildingExtensions != null && this.m_BuildingExtensions.Count > 0)
		{
			SpawnData spawnData = default(SpawnData);
			spawnData.service = service;
			spawnData.subService = subService;
			spawnData.level = level;
			spawnData.style = style;
			foreach (IBuildingExtension current in this.m_BuildingExtensions)
			{
				spawnData = current.OnCalculateSpawn(position, spawnData);
			}
			service = spawnData.service;
			subService = spawnData.subService;
			level = spawnData.level;
			style = spawnData.style;
		}
	}

	private void OnBuildingCreated(ushort id)
	{
		foreach (IBuildingExtension current in this.m_BuildingExtensions)
		{
			current.OnBuildingCreated(id);
		}
	}

	private void OnBuildingRelocated(ushort id)
	{
		foreach (IBuildingExtension current in this.m_BuildingExtensions)
		{
			current.OnBuildingRelocated(id);
		}
	}

	private void OnBuildingReleased(ushort id)
	{
		foreach (IBuildingExtension current in this.m_BuildingExtensions)
		{
			current.OnBuildingReleased(id);
		}
	}

	private List<IBuildingExtension> m_BuildingExtensions = new List<IBuildingExtension>();
}
