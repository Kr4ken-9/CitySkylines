﻿using System;
using UnityEngine;

namespace UnityStandardAssets.CinematicEffects
{
	[ExecuteInEditMode, ImageEffectAllowedInSceneView, RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Cinematic/Temporal Anti-aliasing")]
	public class TemporalAntiAliasing : MonoBehaviour
	{
		public Shader shader
		{
			get
			{
				if (this.m_Shader == null)
				{
					this.m_Shader = Shader.Find("Hidden/Temporal Anti-aliasing");
				}
				return this.m_Shader;
			}
		}

		public Material material
		{
			get
			{
				if (this.m_Material == null)
				{
					if (this.shader == null || !this.shader.get_isSupported())
					{
						return null;
					}
					this.m_Material = new Material(this.shader);
				}
				return this.m_Material;
			}
		}

		public Camera camera_
		{
			get
			{
				if (this.m_Camera == null)
				{
					this.m_Camera = base.GetComponent<Camera>();
				}
				return this.m_Camera;
			}
		}

		private void RenderFullScreenQuad(int pass)
		{
			GL.PushMatrix();
			GL.LoadOrtho();
			this.material.SetPass(pass);
			GL.Begin(7);
			GL.TexCoord2(0f, 0f);
			GL.Vertex3(0f, 0f, 0.1f);
			GL.TexCoord2(1f, 0f);
			GL.Vertex3(1f, 0f, 0.1f);
			GL.TexCoord2(1f, 1f);
			GL.Vertex3(1f, 1f, 0.1f);
			GL.TexCoord2(0f, 1f);
			GL.Vertex3(0f, 1f, 0.1f);
			GL.End();
			GL.PopMatrix();
		}

		private float GetHaltonValue(int index, int radix)
		{
			float num = 0f;
			float num2 = 1f / (float)radix;
			while (index > 0)
			{
				num += (float)(index % radix) * num2;
				index /= radix;
				num2 /= (float)radix;
			}
			return num;
		}

		private Vector2 GenerateRandomOffset()
		{
			Vector2 result;
			result..ctor(this.GetHaltonValue(this.m_SampleIndex & 1023, 2), this.GetHaltonValue(this.m_SampleIndex & 1023, 3));
			if (++this.m_SampleIndex >= this.settings.jitterSettings.sampleCount)
			{
				this.m_SampleIndex = 0;
			}
			return result;
		}

		private Matrix4x4 GetPerspectiveProjectionMatrix(Vector2 offset)
		{
			float num = Mathf.Tan(0.008726646f * this.camera_.get_fieldOfView());
			float num2 = num * this.camera_.get_aspect();
			offset.x *= num2 / (0.5f * (float)this.camera_.get_pixelWidth());
			offset.y *= num / (0.5f * (float)this.camera_.get_pixelHeight());
			float num3 = (offset.x - num2) * this.camera_.get_nearClipPlane();
			float num4 = (offset.x + num2) * this.camera_.get_nearClipPlane();
			float num5 = (offset.y + num) * this.camera_.get_nearClipPlane();
			float num6 = (offset.y - num) * this.camera_.get_nearClipPlane();
			Matrix4x4 result = default(Matrix4x4);
			result.set_Item(0, 0, 2f * this.camera_.get_nearClipPlane() / (num4 - num3));
			result.set_Item(0, 1, 0f);
			result.set_Item(0, 2, (num4 + num3) / (num4 - num3));
			result.set_Item(0, 3, 0f);
			result.set_Item(1, 0, 0f);
			result.set_Item(1, 1, 2f * this.camera_.get_nearClipPlane() / (num5 - num6));
			result.set_Item(1, 2, (num5 + num6) / (num5 - num6));
			result.set_Item(1, 3, 0f);
			result.set_Item(2, 0, 0f);
			result.set_Item(2, 1, 0f);
			result.set_Item(2, 2, -(this.camera_.get_farClipPlane() + this.camera_.get_nearClipPlane()) / (this.camera_.get_farClipPlane() - this.camera_.get_nearClipPlane()));
			result.set_Item(2, 3, -(2f * this.camera_.get_farClipPlane() * this.camera_.get_nearClipPlane()) / (this.camera_.get_farClipPlane() - this.camera_.get_nearClipPlane()));
			result.set_Item(3, 0, 0f);
			result.set_Item(3, 1, 0f);
			result.set_Item(3, 2, -1f);
			result.set_Item(3, 3, 0f);
			return result;
		}

		private Matrix4x4 GetOrthographicProjectionMatrix(Vector2 offset)
		{
			float orthographicSize = this.camera_.get_orthographicSize();
			float num = orthographicSize * this.camera_.get_aspect();
			offset.x *= num / (0.5f * (float)this.camera_.get_pixelWidth());
			offset.y *= orthographicSize / (0.5f * (float)this.camera_.get_pixelHeight());
			float num2 = offset.x - num;
			float num3 = offset.x + num;
			float num4 = offset.y + orthographicSize;
			float num5 = offset.y - orthographicSize;
			return Matrix4x4.Ortho(num2, num3, num5, num4, this.camera_.get_nearClipPlane(), this.camera_.get_farClipPlane());
		}

		private void OnEnable()
		{
			this.camera_.set_depthTextureMode(5);
			this.camera_.set_useJitteredProjectionMatrixForTransparentRendering(true);
		}

		private void OnDisable()
		{
			if (this.m_History != null)
			{
				RenderTexture.ReleaseTemporary(this.m_History);
				this.m_History = null;
			}
			Camera expr_29 = this.camera_;
			expr_29.set_depthTextureMode(expr_29.get_depthTextureMode() & -5);
			this.m_SampleIndex = 0;
		}

		private void OnPreCull()
		{
			Vector2 vector = this.GenerateRandomOffset();
			vector *= this.settings.jitterSettings.spread;
			this.camera_.set_nonJitteredProjectionMatrix(this.camera_.get_projectionMatrix());
			this.camera_.set_projectionMatrix((!this.camera_.get_orthographic()) ? this.GetPerspectiveProjectionMatrix(vector) : this.GetOrthographicProjectionMatrix(vector));
			vector.x /= (float)this.camera_.get_pixelWidth();
			vector.y /= (float)this.camera_.get_pixelHeight();
			this.material.SetVector("_Jitter", vector);
		}

		[ImageEffectOpaque]
		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			if (this.m_History == null || this.m_History.get_width() != source.get_width() || this.m_History.get_height() != source.get_height())
			{
				if (this.m_History)
				{
					RenderTexture.ReleaseTemporary(this.m_History);
				}
				this.m_History = RenderTexture.GetTemporary(source.get_width(), source.get_height(), 0, source.get_format(), 0);
				this.m_History.set_filterMode(1);
				this.m_History.set_hideFlags(61);
				Graphics.Blit(source, this.m_History, this.material, 2);
			}
			this.material.SetVector("_SharpenParameters", new Vector4(this.settings.sharpenFilterSettings.amount, 0f, 0f, 0f));
			this.material.SetVector("_FinalBlendParameters", new Vector4(this.settings.blendSettings.stationary, this.settings.blendSettings.moving, 100f * this.settings.blendSettings.motionAmplification, 0f));
			this.material.SetTexture("_MainTex", source);
			this.material.SetTexture("_HistoryTex", this.m_History);
			RenderTexture temporary = RenderTexture.GetTemporary(source.get_width(), source.get_height(), 0, source.get_format(), 0);
			temporary.set_filterMode(1);
			RenderTexture renderTexture = destination;
			bool flag = false;
			if (destination == null)
			{
				renderTexture = RenderTexture.GetTemporary(source.get_width(), source.get_height(), 0, source.get_format(), 0);
				renderTexture.set_filterMode(1);
				flag = true;
			}
			Graphics.SetRenderTarget(new RenderBuffer[]
			{
				renderTexture.get_colorBuffer(),
				temporary.get_colorBuffer()
			}, renderTexture.get_depthBuffer());
			this.RenderFullScreenQuad((!this.camera_.get_orthographic()) ? 0 : 1);
			RenderTexture.ReleaseTemporary(this.m_History);
			this.m_History = temporary;
			if (flag)
			{
				Graphics.Blit(renderTexture, destination);
				RenderTexture.ReleaseTemporary(renderTexture);
			}
			RenderTexture.set_active(destination);
		}

		public void OnPostRender()
		{
			this.camera_.ResetProjectionMatrix();
		}

		[SerializeField]
		public TemporalAntiAliasing.Settings settings = TemporalAntiAliasing.Settings.defaultSettings;

		private Shader m_Shader;

		private Material m_Material;

		private Camera m_Camera;

		private RenderTexture m_History;

		private int m_SampleIndex;

		public enum Sequence
		{
			Halton
		}

		[Serializable]
		public struct JitterSettings
		{
			[Tooltip("The sequence used to generate the points used as jitter offsets.")]
			public TemporalAntiAliasing.Sequence sequence;

			[Tooltip("The diameter (in texels) inside which jitter samples are spread. Smaller values result in crisper but more aliased output, while larger values result in more stable but blurrier output."), Range(0.1f, 3f)]
			public float spread;

			[Tooltip("Number of temporal samples. A larger value results in a smoother image but takes longer to converge; whereas a smaller value converges fast but allows for less subpixel information."), Range(4f, 64f)]
			public int sampleCount;
		}

		[Serializable]
		public struct SharpenFilterSettings
		{
			[Tooltip("Controls the amount of sharpening applied to the color buffer."), Range(0f, 3f)]
			public float amount;
		}

		[Serializable]
		public struct BlendSettings
		{
			[Tooltip("The blend coefficient for a stationary fragment. Controls the percentage of history sample blended into the final color."), Range(0f, 1f)]
			public float stationary;

			[Tooltip("The blend coefficient for a fragment with significant motion. Controls the percentage of history sample blended into the final color."), Range(0f, 1f)]
			public float moving;

			[Tooltip("Amount of motion amplification in percentage. A higher value will make the final blend more sensitive to smaller motion, but might result in more aliased output; while a smaller value might desensitivize the algorithm resulting in a blurry output."), Range(30f, 100f)]
			public float motionAmplification;
		}

		[Serializable]
		public struct DebugSettings
		{
			[Tooltip("Forces the game view to update automatically while not in play mode.")]
			public bool forceRepaint;
		}

		[Serializable]
		public class Settings
		{
			public static TemporalAntiAliasing.Settings defaultSettings
			{
				get
				{
					return new TemporalAntiAliasing.Settings
					{
						jitterSettings = new TemporalAntiAliasing.JitterSettings
						{
							sequence = TemporalAntiAliasing.Sequence.Halton,
							spread = 1f,
							sampleCount = 8
						},
						sharpenFilterSettings = new TemporalAntiAliasing.SharpenFilterSettings
						{
							amount = 0.25f
						},
						blendSettings = new TemporalAntiAliasing.BlendSettings
						{
							stationary = 0.98f,
							moving = 0.8f,
							motionAmplification = 60f
						},
						debugSettings = new TemporalAntiAliasing.DebugSettings
						{
							forceRepaint = false
						}
					};
				}
			}

			[TemporalAntiAliasing.Settings.LayoutAttribute]
			public TemporalAntiAliasing.JitterSettings jitterSettings;

			[TemporalAntiAliasing.Settings.LayoutAttribute]
			public TemporalAntiAliasing.SharpenFilterSettings sharpenFilterSettings;

			[TemporalAntiAliasing.Settings.LayoutAttribute]
			public TemporalAntiAliasing.BlendSettings blendSettings;

			[TemporalAntiAliasing.Settings.LayoutAttribute]
			public TemporalAntiAliasing.DebugSettings debugSettings;

			[AttributeUsage(AttributeTargets.Field)]
			public class LayoutAttribute : PropertyAttribute
			{
			}
		}
	}
}
