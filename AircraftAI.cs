﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class AircraftAI : VehicleAI
{
	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		if ((flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			return Matrix4x4.get_identity();
		}
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, rotation, scale);
		return bodyMatrix.get_inverse() * matrix4x;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			byte pathFindFlags = instance.m_pathUnits.m_buffer[(int)((UIntPtr)data.m_path)].m_pathFindFlags;
			if ((pathFindFlags & 4) != 0)
			{
				data.m_pathPositionIndex = 255;
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				this.TrySpawn(vehicleID, ref data);
			}
			else if ((pathFindFlags & 8) != 0)
			{
				data.m_flags &= ~Vehicle.Flags.WaitingPath;
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
				data.Unspawn(vehicleID);
				return;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, 0);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance2.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance2.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance2.m_vehicles.m_buffer[(int)num], vehicleID, ref data, 0);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingPath | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 || data.m_blockCounter == 255)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float acceleration = this.m_info.m_acceleration;
		float braking = this.m_info.m_braking;
		float num = frameData.m_velocity.get_magnitude();
		Vector3 vector = vehicleData.m_targetPos0 - frameData.m_position;
		float sqrMagnitude = vector.get_sqrMagnitude();
		float num2 = (num + acceleration) * (0.5f + 0.5f * (num + acceleration) / (braking * 0.9f));
		float num3 = Mathf.Max(num + num * num * 0.1f + acceleration, 2f);
		float num4 = Mathf.Max((num2 - num3) / 3f, 1f);
		float num5 = num3 * num3;
		float minSqrDistanceB = num4 * num4;
		int i = 0;
		bool flag = false;
		if ((sqrMagnitude < num5 || vehicleData.m_targetPos3.w < 0.01f) && (leaderData.m_flags & (Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped)) == (Vehicle.Flags)0)
		{
			if (leaderData.m_path != 0u)
			{
				base.UpdatePathTargetPositions(vehicleID, ref vehicleData, frameData.m_position, ref i, 4, num5, minSqrDistanceB);
			}
			if (i < 4)
			{
				if (i == 0)
				{
					flag = true;
				}
				this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, frameData.m_position, leaderID, ref leaderData, ref i, num5);
			}
			while (i < 4)
			{
				vehicleData.SetTargetPos(i, vehicleData.GetTargetPos(i - 1));
				i++;
			}
			vector = vehicleData.m_targetPos0 - frameData.m_position;
		}
		this.ReserveSpace(vehicleID, ref vehicleData, num > 0.1f);
		float num6 = VectorUtils.LengthXZ(frameData.m_velocity);
		float num7 = VectorUtils.LengthXZ(vector);
		Vehicle.Flags flags = leaderData.m_flags;
		if ((flags & Vehicle.Flags.TakingOff) != (Vehicle.Flags)0)
		{
			if (num6 > this.m_info.m_maxSpeed * 0.5f)
			{
				vector.y += num6 + num6 * num6 * 0.1f;
				flags |= Vehicle.Flags.Flying;
			}
			if (vehicleData.m_targetPos0.w > 40f)
			{
				flags &= ~Vehicle.Flags.TakingOff;
				flags |= Vehicle.Flags.Flying;
			}
		}
		else if ((flags & Vehicle.Flags.Landing) != (Vehicle.Flags)0)
		{
			if (vector.y > -0.1f)
			{
				flags &= ~Vehicle.Flags.Flying;
			}
			if (vehicleData.m_targetPos0.w < 16f)
			{
				flags &= ~Vehicle.Flags.Landing;
				flags &= ~Vehicle.Flags.Flying;
			}
		}
		else if ((flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			Vector3 vector2 = vehicleData.m_targetPos0;
			Vector3 vector3 = vehicleData.m_targetPos1;
			Vector3 vector4 = Vector3.Normalize(new Vector3(vector2.x - vector3.x, 0f, vector2.z - vector3.z));
			float magnitude = vector.get_magnitude();
			vector2 += vector4 * (magnitude * 0.9659258f);
			vector2.y = Mathf.Min(vector2.y + magnitude * 0.258819044f, Mathf.Max(vector2.y, 1500f));
			vector = vector2 - frameData.m_position;
			if (vector.get_sqrMagnitude() < 40000f)
			{
				vector2 = vehicleData.m_targetPos0;
				vector = vector2 - frameData.m_position;
				if (vehicleData.m_targetPos0.w < 40f && num7 < 500f)
				{
					flags |= Vehicle.Flags.Landing;
				}
			}
			else
			{
				num7 = VectorUtils.LengthXZ(vector);
			}
		}
		else if (vehicleData.m_targetPos0.w > 16f)
		{
			flags |= Vehicle.Flags.TakingOff;
		}
		leaderData.m_flags = flags;
		if ((leaderData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			Vector3 vector5 = frameData.m_rotation * Vector3.get_forward();
			vector5.y = 0f;
			vector5.Normalize();
			Vector3 vector6;
			vector6..ctor(vector5.z, 0f, -vector5.x);
			float num8 = VectorUtils.DotXZ(vector, vector5);
			float num9 = VectorUtils.DotXZ(vector, vector6);
			if (num8 < Mathf.Abs(num9) * 5f)
			{
				num9 = ((num9 < 0f) ? (num7 * -0.2f) : (num7 * 0.2f));
				num8 = num7;
				vector.x = vector5.x * num8 + vector6.x * num9;
				vector.z = vector5.z * num8 + vector6.z * num9;
			}
			float num10 = Mathf.Max(0.1f, num7 * (num6 / this.m_info.m_maxSpeed - 0.5f));
			float num11 = Mathf.Min(-0.1f, num7 * -0.5f);
			vector.y = Mathf.Clamp(vector.y, num11, num10);
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector = quaternion * vector;
		Vector3 vector7 = quaternion * frameData.m_velocity;
		if (vector7.z < 0f)
		{
			num = -num;
		}
		Vector3 vector8 = Vector3.get_zero();
		Vector3 vector9 = Vector3.get_forward();
		float num12 = 0f;
		float steerAngle = 0f;
		float magnitude2 = vector.get_magnitude();
		bool flag2 = false;
		if (magnitude2 > 1f)
		{
			vector9 = vector / magnitude2;
			float num13 = this.m_info.m_maxSpeed;
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
			{
				num13 = 0f;
			}
			else
			{
				if ((leaderData.m_flags & Vehicle.Flags.Landing) != (Vehicle.Flags)0)
				{
					num13 = Mathf.Min(num13, AircraftAI.CalculateMaxSpeed(magnitude2, vehicleData.m_targetPos0.w, braking));
				}
				else if ((leaderData.m_flags & Vehicle.Flags.Flying) == (Vehicle.Flags)0)
				{
					float curve = 1.57079637f * (1f - vector9.z);
					num13 = Mathf.Min(num13, this.CalculateTargetSpeed(vehicleID, ref vehicleData, vehicleData.m_targetPos0.w, curve));
				}
				if ((leaderData.m_flags & (Vehicle.Flags.Flying | Vehicle.Flags.Landing)) != Vehicle.Flags.Flying)
				{
					float num14 = magnitude2;
					num13 = Mathf.Min(num13, AircraftAI.CalculateMaxSpeed(num14, vehicleData.m_targetPos1.w, braking * 0.9f));
					num14 += VectorUtils.LengthXZ(vehicleData.m_targetPos1 - vehicleData.m_targetPos0);
					num13 = Mathf.Min(num13, AircraftAI.CalculateMaxSpeed(num14, vehicleData.m_targetPos2.w, braking * 0.9f));
					num14 += VectorUtils.LengthXZ(vehicleData.m_targetPos2 - vehicleData.m_targetPos1);
					num13 = Mathf.Min(num13, AircraftAI.CalculateMaxSpeed(num14, vehicleData.m_targetPos3.w, braking * 0.9f));
					num14 += VectorUtils.LengthXZ(vehicleData.m_targetPos3 - vehicleData.m_targetPos2);
					num13 = Mathf.Min(num13, AircraftAI.CalculateMaxSpeed(num14, 0f, braking * 0.9f));
				}
			}
			if ((leaderData.m_flags & Vehicle.Flags.Flying) == (Vehicle.Flags)0)
			{
				if (vector.z < Mathf.Abs(vector.x) * 2f)
				{
					if (vector.z < -Mathf.Abs(vector.x) * 20f)
					{
						vector9 = -vector9;
						num13 = -num13;
					}
					else
					{
						if (vector.x >= 0f)
						{
							vector.x = Mathf.Max(1f, vector.x);
						}
						else
						{
							vector.x = Mathf.Min(-1f, vector.x);
						}
						vector.z = Mathf.Abs(vector.x) * 2f;
						vector9 = vector.get_normalized();
						num13 = 0f;
					}
				}
			}
			else if (Mathf.Abs(vector9.y) > 0.1f)
			{
				vector9.y = Mathf.Clamp(vector9.y, -0.1f, 0.1f);
				vector9.Normalize();
			}
			if (num13 < num)
			{
				float num15 = Mathf.Max(acceleration, Mathf.Min(braking, num));
				num12 = Mathf.Max(num13, num - num15);
			}
			else
			{
				float num16 = Mathf.Max(acceleration, Mathf.Min(braking, -num));
				num12 = Mathf.Min(num13, num + num16);
			}
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && Mathf.Abs(num13) < 0.1f)
			{
				flag2 = true;
			}
		}
		else if (Mathf.Abs(num) < 0.1f && flag && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			return;
		}
		if (flag2)
		{
			leaderData.m_blockCounter = (byte)Mathf.Min((int)(leaderData.m_blockCounter + 1), 255);
		}
		else
		{
			leaderData.m_blockCounter = 0;
		}
		if (magnitude2 > 1f)
		{
			steerAngle = Mathf.Asin(vector9.x) * Mathf.Sign(num12);
			vector8 = vector9 * num12;
		}
		else
		{
			Vector3 vector10 = Vector3.ClampMagnitude(vector * 0.5f - vector7, braking);
			vector8 = vector7 + vector10;
		}
		vector9 = frameData.m_rotation * vector9;
		frameData.m_velocity = frameData.m_rotation * vector8;
		frameData.m_position += frameData.m_velocity * 0.5f;
		if ((leaderData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			Vector3 vector11;
			vector11.x = (vector7.x - vector8.x) * 0.5f;
			vector11.y = 0f;
			vector11.z = Mathf.Max(0f, vector9.y * -10f);
			frameData.m_swayVelocity = frameData.m_swayVelocity * 0.5f - vector11 * 0.5f - frameData.m_swayPosition * 0.5f;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
			frameData.m_steerAngle = 0f;
		}
		else
		{
			Vector3 vector12 = vector8 - vector7;
			frameData.m_swayVelocity = frameData.m_swayVelocity * (1f - this.m_info.m_dampers) - vector12 * (1f - this.m_info.m_springs) - frameData.m_swayPosition * this.m_info.m_springs;
			frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
			frameData.m_steerAngle = steerAngle;
			frameData.m_travelDistance += vector8.z;
		}
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = 5f;
		frameData.m_lightIntensity.w = 5f;
		if (magnitude2 > 1f)
		{
			frameData.m_rotation = Quaternion.LookRotation(vector9);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	private void ReserveSpace(ushort vehicleID, ref Vehicle vehicleData, bool moving)
	{
		uint num = vehicleData.m_path;
		if (num != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			PathManager instance2 = Singleton<PathManager>.get_instance();
			byte b = vehicleData.m_pathPositionIndex;
			byte lastPathOffset = vehicleData.m_lastPathOffset;
			if (b == 255)
			{
				b = 0;
			}
			b = (byte)(b >> 1);
			float num2 = 1f + vehicleData.CalculateTotalLength(vehicleID);
			for (int i = 0; i < 2; i++)
			{
				PathUnit.Position pathPos;
				if (!instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].GetPosition((int)b, out pathPos))
				{
					return;
				}
				if (i == 0)
				{
					instance.m_segments.m_buffer[(int)pathPos.m_segment].AddTraffic(Mathf.RoundToInt(num2 * 2.5f), this.GetNoiseLevel());
					if ((b & 1) == 0 || lastPathOffset == 0 || (vehicleData.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
					{
						uint laneID = PathManager.GetLaneID(pathPos);
						if (laneID != 0u)
						{
							instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].ReserveSpace(num2, vehicleID);
						}
						if (!moving || (vehicleData.m_flags & Vehicle.Flags.WaitingPath) != (Vehicle.Flags)0)
						{
							return;
						}
					}
				}
				else
				{
					uint laneID2 = PathManager.GetLaneID(pathPos);
					if (laneID2 != 0u && !instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].ReserveSpace(num2, vehicleID))
					{
						return;
					}
					if (!moving)
					{
						return;
					}
				}
				if ((b += 1) >= instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_positionCount)
				{
					num = instance2.m_pathUnits.m_buffer[(int)((UIntPtr)num)].m_nextPathUnit;
					b = 0;
					if (num == 0u)
					{
						return;
					}
				}
			}
		}
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 10f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(num2 * num2 + 2f * targetDistance * maxBraking) - num;
	}

	protected override float CalculateTargetSpeed(ushort vehicleID, ref Vehicle data, float speedLimit, float curve)
	{
		float num = 8f * speedLimit;
		if ((double)speedLimit < 2.0)
		{
			float num2 = 1000f / (1f + curve * 1000f / this.m_info.m_turning) + 2f;
			return Mathf.Min(num2, num);
		}
		return num;
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position nextPosition, PathUnit.Position position, uint laneID, byte offset, PathUnit.Position prevPos, uint prevLaneID, byte prevOffset, int index, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		bool flag = false;
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			float speedLimit = info.m_lanes[(int)position.m_lane].m_speedLimit;
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
			if (speedLimit > 5f)
			{
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				pos.x += (float)randomizer.Int32(-500, 500);
				pos.y += (float)randomizer.Int32(1300, 1700);
				pos.z += (float)randomizer.Int32(-500, 500);
			}
			else if (speedLimit < 2f)
			{
				flag = true;
			}
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
		if (flag)
		{
			Vehicle.Frame lastFrameData = vehicleData.GetLastFrameData();
			Vector3 position2 = lastFrameData.m_position;
			Vector3 vector = instance.m_lanes.m_buffer[(int)((UIntPtr)prevLaneID)].CalculatePosition((float)prevOffset * 0.003921569f);
			float num = 0.5f * lastFrameData.m_velocity.get_sqrMagnitude() / this.m_info.m_braking + this.m_info.m_generatedInfo.m_size.z * 0.5f;
			if (Vector3.Distance(position2, vector) >= num - 1f)
			{
				if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
				{
					maxSpeed = 0f;
					return;
				}
				ushort startNode = instance.m_segments.m_buffer[(int)position.m_segment].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)position.m_segment].m_endNode;
				Vector3 position3 = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position4 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				if (AircraftAI.CheckOverlap(new Segment3(position3, position4), vehicleID))
				{
					maxSpeed = 0f;
					return;
				}
			}
			if (nextPosition.m_segment != 0)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)nextPosition.m_segment].Info;
				if (info2.m_lanes != null && info2.m_lanes.Length > (int)nextPosition.m_lane)
				{
					float speedLimit2 = info.m_lanes[(int)nextPosition.m_lane].m_speedLimit;
					if (speedLimit2 <= 5f)
					{
						uint laneID2 = PathManager.GetLaneID(nextPosition);
						if (laneID2 != 0u)
						{
							if (!instance.m_lanes.m_buffer[(int)((UIntPtr)laneID2)].CheckSpace(1000f))
							{
								maxSpeed = 0f;
								return;
							}
							ushort startNode2 = instance.m_segments.m_buffer[(int)nextPosition.m_segment].m_startNode;
							ushort endNode2 = instance.m_segments.m_buffer[(int)nextPosition.m_segment].m_endNode;
							Vector3 position5 = instance.m_nodes.m_buffer[(int)startNode2].m_position;
							Vector3 position6 = instance.m_nodes.m_buffer[(int)endNode2].m_position;
							if (AircraftAI.CheckOverlap(new Segment3(position5, position6), vehicleID))
							{
								maxSpeed = 0f;
								return;
							}
						}
					}
				}
			}
		}
	}

	protected override void CalculateSegmentPosition(ushort vehicleID, ref Vehicle vehicleData, PathUnit.Position position, uint laneID, byte offset, out Vector3 pos, out Vector3 dir, out float maxSpeed)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].CalculatePositionAndDirection((float)offset * 0.003921569f, out pos, out dir);
		NetInfo info = instance.m_segments.m_buffer[(int)position.m_segment].Info;
		if (info.m_lanes != null && info.m_lanes.Length > (int)position.m_lane)
		{
			float speedLimit = info.m_lanes[(int)position.m_lane].m_speedLimit;
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, speedLimit, instance.m_lanes.m_buffer[(int)((UIntPtr)laneID)].m_curve);
			if (speedLimit > 5f)
			{
				Randomizer randomizer;
				randomizer..ctor((int)vehicleID);
				pos.x += (float)randomizer.Int32(-500, 500);
				pos.y += (float)randomizer.Int32(1300, 1700);
				pos.z += (float)randomizer.Int32(-500, 500);
			}
		}
		else
		{
			maxSpeed = this.CalculateTargetSpeed(vehicleID, ref vehicleData, 1f, 0f);
		}
	}

	public override bool CanSpawnAt(Vector3 pos)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((pos.x - 50f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((pos.z - 50f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((pos.x + 50f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((pos.z + 50f) / 320f + 27f), 53);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if (Vector3.SqrMagnitude(instance.m_vehicles.m_buffer[(int)num5].GetLastFramePosition() - pos) < 2500f)
					{
						return false;
					}
					num5 = instance.m_vehicles.m_buffer[(int)num5].m_nextGridVehicle;
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return true;
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 30f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((vector.z - 30f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 30f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((vector2.z + 30f) / 320f + 27f), 53);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = AircraftAI.CheckOverlap(segment, ignoreVehicle, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 100f)
		{
			overlap = true;
		}
		return otherData.m_nextGridVehicle;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		if (Vector3.SqrMagnitude(endPos - startPos) < 100f)
		{
			if (vehicleData.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				vehicleData.m_path = 0u;
			}
			vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
			vehicleData.m_targetPos0 = endPos;
			vehicleData.m_targetPos0.w = 2f;
			vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
			vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
			this.TrySpawn(vehicleID, ref vehicleData);
			return true;
		}
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, true, true);
	}

	protected bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays)
	{
		VehicleInfo info = this.m_info;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 16f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.PublicTransport, NetInfo.LaneType.Vehicle, info.m_vehicleType, false, false, 16f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, NetInfo.LaneType.Vehicle, info.m_vehicleType, 20000f))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		return false;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (AircraftAI.CheckOverlap(vehicleData.m_segment, 0))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		if (vehicleData.m_path != 0u)
		{
			PathManager instance = Singleton<PathManager>.get_instance();
			PathUnit.Position pathPos;
			if (instance.m_pathUnits.m_buffer[(int)((UIntPtr)vehicleData.m_path)].GetPosition(0, out pathPos))
			{
				uint laneID = PathManager.GetLaneID(pathPos);
				if (laneID != 0u && !Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)laneID)].CheckSpace(1000f, vehicleID))
				{
					vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
					return false;
				}
			}
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}
}
