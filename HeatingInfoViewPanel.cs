﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class HeatingInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_HeatingMeter = base.Find<UISlider>("HeatingMeter");
		this.m_Consumption = base.Find<UILabel>("Consumption");
		this.m_Production = base.Find<UILabel>("Production");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UISprite uISprite3 = base.Find<UISprite>("ColorConnected");
				UISprite uISprite4 = base.Find<UISprite>("ColorDisconnected");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[22].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[22].m_inactiveColor);
					uISprite3.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[22].m_targetColor);
					uISprite4.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[22].m_negativeColor);
				}
			}
			this.m_initialized = true;
		}
		int num = 0;
		int num2 = 0;
		if (Singleton<DistrictManager>.get_exists())
		{
			num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetHeatingCapacity();
			num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetHeatingConsumption();
		}
		this.m_HeatingMeter.set_value((float)base.GetPercentage(num, num2));
		this.m_Consumption.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEATING_CONSUMPTION"), num2 / 1000));
		this.m_Production.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEATING_PRODUCTION"), num / 1000));
	}

	private UISlider m_HeatingMeter;

	private UILabel m_Consumption;

	private UILabel m_Production;

	private bool m_initialized;
}
