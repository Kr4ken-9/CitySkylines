﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class TiltShiftEffect : PostProcessEffect
{
	protected override bool CheckResources()
	{
		this.CheckSupport(true);
		this.m_TiltShiftMaterial = base.CheckShaderAndCreateMaterial(this.m_TiltShiftShader, this.m_TiltShiftMaterial);
		if (!this.m_IsSupported)
		{
			base.ReportAutoDisable();
		}
		return this.m_IsSupported;
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (!this.CheckResources())
		{
			Graphics.Blit(source, destination);
			return;
		}
		this.m_TiltShiftMaterial.SetFloat("_BlurSize", (this.m_MaxBlurSize >= 0f) ? this.m_MaxBlurSize : 0f);
		this.m_TiltShiftMaterial.SetFloat("_BlurArea", this.m_BlurArea);
		source.set_filterMode(1);
		RenderTexture renderTexture = destination;
		if (this.m_Downsample)
		{
			renderTexture = RenderTexture.GetTemporary(source.get_width() >> (((!this.m_Downsample) ? 0 : 1) & 31), source.get_height() >> (((!this.m_Downsample) ? 0 : 1) & 31), 0, source.get_format());
			renderTexture.set_filterMode(1);
		}
		int num = (int)this.m_Quality;
		num *= 2;
		Graphics.Blit(source, renderTexture, this.m_TiltShiftMaterial, (this.m_Mode != TiltShiftEffect.TiltShiftMode.TiltShiftMode) ? (num + 1) : num);
		if (this.m_Downsample)
		{
			this.m_TiltShiftMaterial.SetTexture("_Blurred", renderTexture);
			Graphics.Blit(source, destination, this.m_TiltShiftMaterial, 6);
		}
		if (renderTexture != destination)
		{
			RenderTexture.ReleaseTemporary(renderTexture);
		}
	}

	public TiltShiftEffect.TiltShiftMode m_Mode;

	public TiltShiftEffect.TiltShiftQuality m_Quality = TiltShiftEffect.TiltShiftQuality.Normal;

	[Range(0f, 15f)]
	public float m_BlurArea = 1f;

	[Range(0f, 25f)]
	public float m_MaxBlurSize = 5f;

	public bool m_Downsample;

	public Shader m_TiltShiftShader;

	private Material m_TiltShiftMaterial;

	public enum TiltShiftMode
	{
		TiltShiftMode,
		IrisMode
	}

	public enum TiltShiftQuality
	{
		Preview,
		Normal,
		High
	}
}
