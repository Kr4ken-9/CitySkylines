﻿using System;

public sealed class MapSettingsGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("MapSettings");
		return true;
	}
}
