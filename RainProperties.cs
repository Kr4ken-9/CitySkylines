﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class RainProperties : MonoBehaviour
{
	private void OnEnable()
	{
		RainProperties.ID_RainIntensity = Shader.PropertyToID("_RainIntensity");
		RainProperties.ID_CameraMotionOffset = Shader.PropertyToID("_CameraMotionOffset");
		this.InitSpindle();
		if (Singleton<SimulationManager>.get_exists())
		{
			this.m_IsWinter = (Singleton<SimulationManager>.get_instance().m_metaData.m_environment == "Winter");
		}
	}

	private void OnDisable()
	{
		if (this.m_RainSpindleMesh)
		{
			Object.DestroyImmediate(this.m_RainSpindleMesh);
			this.m_RainSpindleMesh = null;
		}
	}

	private void InitSpindle()
	{
		if (this.m_RainSpindleBase != null)
		{
			Mesh mesh = new Mesh();
			Vector3[] vertices = this.m_RainSpindleBase.get_vertices();
			mesh.set_vertices(vertices);
			mesh.set_triangles(this.m_RainSpindleBase.get_triangles());
			mesh.set_normals(this.m_RainSpindleBase.get_normals());
			mesh.set_uv(this.m_RainSpindleBase.get_uv());
			mesh.set_uv2(this.m_RainSpindleBase.get_uv2());
			mesh.set_colors(this.m_RainSpindleBase.get_colors());
			mesh.set_bounds(new Bounds(Vector3.get_zero(), Vector3.get_one() * 2E+09f));
			mesh.set_hideFlags(52);
			mesh.set_name("SpindleMesh");
			this.m_RainSpindleMesh = mesh;
		}
		else
		{
			base.set_enabled(false);
		}
	}

	private Animator lightningStrike
	{
		get
		{
			if (this.m_LightningStrikeAnim == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("LightningStrikeLight");
				if (gameObject != null)
				{
					this.m_LightningStrikeAnim = gameObject.GetComponent<Animator>();
				}
			}
			return this.m_LightningStrikeAnim;
		}
	}

	private Vector3 cameraPosition
	{
		get
		{
			if (this.m_Camera == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					this.m_Camera = gameObject.get_transform();
				}
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_position();
			}
			return default(Vector3);
		}
	}

	private float cameraYRotation
	{
		get
		{
			if (this.m_Camera == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					this.m_Camera = gameObject.get_transform();
				}
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_eulerAngles().y;
			}
			return 0f;
		}
	}

	private Vector3 cameraForward
	{
		get
		{
			if (this.m_Camera == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					this.m_Camera = gameObject.get_transform();
				}
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_forward();
			}
			return Vector3.get_forward();
		}
	}

	private float cameraY
	{
		get
		{
			if (this.m_Camera == null)
			{
				GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
				if (gameObject != null)
				{
					this.m_Camera = gameObject.get_transform();
				}
			}
			if (this.m_Camera != null)
			{
				return this.m_Camera.get_position().y;
			}
			return 0f;
		}
	}

	private void Update()
	{
		if (Singleton<SimulationManager>.get_exists())
		{
			this.m_TimeToNextStrike -= Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		}
		this.m_CameraMotionOffset += (this.cameraYRotation - this.m_LastCameraY) * (1f - Mathf.Abs(Vector3.Dot(this.cameraForward, Vector3.get_up())));
		this.m_LastCameraY = this.cameraYRotation;
		if (Singleton<WeatherManager>.get_exists())
		{
			this.m_RainControl = Singleton<WeatherManager>.get_instance().m_currentRain;
		}
		if (this.m_RainControl > 0f)
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				this.lightningStrike.set_speed(Singleton<SimulationManager>.get_instance().m_simulationTimeSpeed);
			}
			if (!this.m_IsWinter && this.lightningStrike != null && this.m_TimeToNextStrike <= 0f && this.m_RainControl > this.m_LightningTreshold)
			{
				this.m_TimeToNextStrike = Random.Range(this.m_LightningMinMaxIntervals.x, this.m_LightningMinMaxIntervals.y);
				Vector3 vector;
				vector..ctor(Random.Range(-RainProperties.kTerrainRange, RainProperties.kTerrainRange), 2048f, Random.Range(-RainProperties.kTerrainRange, RainProperties.kTerrainRange));
				this.lightningStrike.get_transform().set_position(vector);
				this.lightningStrike.get_transform().LookAt(Vector3.get_zero());
				this.lightningStrike.Play("Strike", 0, 0f);
				if (this.m_ThunderSound != null)
				{
					base.StartCoroutine(this.PlayThunderSound(vector));
				}
			}
			this.m_RainMaterial.SetVector(RainProperties.ID_CameraMotionOffset, new Vector4(this.m_CameraMotionOffset * 0.005f, this.cameraY * 0.05f, 0f, 0f));
			this.m_RainMaterial.SetFloat(RainProperties.ID_RainIntensity, Mathf.Lerp(0f, this.m_MaxRainIntensity, this.m_RainControl));
			Graphics.DrawMesh(this.m_RainSpindleMesh, Vector3.get_zero(), Quaternion.get_identity(), this.m_RainMaterial, this.m_RainLayer);
		}
	}

	[DebuggerHidden]
	private IEnumerator PlayThunderSound(Vector3 strikePosition)
	{
		RainProperties.<PlayThunderSound>c__Iterator0 <PlayThunderSound>c__Iterator = new RainProperties.<PlayThunderSound>c__Iterator0();
		<PlayThunderSound>c__Iterator.strikePosition = strikePosition;
		<PlayThunderSound>c__Iterator.$this = this;
		return <PlayThunderSound>c__Iterator;
	}

	private void OnDrawGizmos()
	{
		Gizmos.set_color(Color.get_red());
		Gizmos.DrawLine(new Vector3(-RainProperties.kTerrainRange, 0f, -RainProperties.kTerrainRange), new Vector3(-RainProperties.kTerrainRange, 0f, RainProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(-RainProperties.kTerrainRange, 0f, RainProperties.kTerrainRange), new Vector3(RainProperties.kTerrainRange, 0f, RainProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(RainProperties.kTerrainRange, 0f, RainProperties.kTerrainRange), new Vector3(RainProperties.kTerrainRange, 0f, -RainProperties.kTerrainRange));
		Gizmos.DrawLine(new Vector3(RainProperties.kTerrainRange, 0f, -RainProperties.kTerrainRange), new Vector3(-RainProperties.kTerrainRange, 0f, -RainProperties.kTerrainRange));
	}

	private Mesh m_RainSpindleMesh;

	[Range(0f, 1f)]
	public float m_RainControl;

	[Header("Rain Objects")]
	public Mesh m_RainSpindleBase;

	public Material m_RainMaterial;

	public int m_RainLayer = 29;

	[Header("Rain Settings"), Range(0f, 1f)]
	public float m_MaxRainIntensity = 0.6f;

	public float m_LightningTreshold = 0.8f;

	public Vector2 m_LightningMinMaxIntervals = new Vector2(10f, 300f);

	public AudioInfo m_ThunderSound;

	private static int ID_RainIntensity;

	private static int ID_CameraMotionOffset;

	private static float kTerrainRange = 8640f;

	private bool m_IsWinter;

	[Header("Debugging")]
	public float m_TimeToNextStrike;

	private Animator m_LightningStrikeAnim;

	private Transform m_Camera;

	private float m_LastCameraY;

	private float m_CameraMotionOffset;
}
