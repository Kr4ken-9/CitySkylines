﻿using System;

public sealed class WaterGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("Water");
		return true;
	}
}
