﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class DisasterWrapper : IDisaster
{
	public DisasterWrapper(DisasterManager disasterManager)
	{
		this.m_DisasterManager = disasterManager;
		this.m_CustomDisasters = new List<ushort>();
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.m_DisasterTypeToInfoConversion = new Dictionary<DisasterType, DisasterInfo>();
		this.m_DisasterTypeToInfoConversion[1] = DisasterManager.FindDisasterInfo<EarthquakeAI>();
		this.m_DisasterTypeToInfoConversion[2] = DisasterManager.FindDisasterInfo<ForestFireAI>();
		this.m_DisasterTypeToInfoConversion[3] = DisasterManager.FindDisasterInfo<MeteorStrikeAI>();
		this.m_DisasterTypeToInfoConversion[4] = DisasterManager.FindDisasterInfo<ThunderStormAI>();
		this.m_DisasterTypeToInfoConversion[5] = DisasterManager.FindDisasterInfo<TornadoAI>();
		this.m_DisasterTypeToInfoConversion[6] = DisasterManager.FindDisasterInfo<TsunamiAI>();
		this.m_DisasterTypeToInfoConversion[7] = DisasterManager.FindDisasterInfo<StructureCollapseAI>();
		this.m_DisasterTypeToInfoConversion[8] = DisasterManager.FindDisasterInfo<StructureFireAI>();
		this.m_DisasterTypeToInfoConversion[9] = DisasterManager.FindDisasterInfo<SinkholeAI>();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnDisasterExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void GetImplementations()
	{
		this.OnDisasterExtensionsReleased();
		this.m_DisasterExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IDisasterExtension>();
		this.OnDisasterExtensionsCreated();
	}

	private void OnDisasterExtensionsCreated()
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			try
			{
				this.m_DisasterExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_DisasterExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnDisasterExtensionsReleased()
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			try
			{
				this.m_DisasterExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_DisasterExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public bool IsCustomDisaster(ushort disasterID)
	{
		return this.m_CustomDisasters.Contains(disasterID);
	}

	public void OnDisasterCreated(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterCreated(disasterID);
		}
	}

	public void OnDisasterStarted(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterStarted(disasterID);
		}
	}

	public void OnDisasterDetected(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterDetected(disasterID);
		}
	}

	public void OnDisasterActivated(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterActivated(disasterID);
		}
	}

	public void OnDisasterDeactivated(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterDeactivated(disasterID);
		}
	}

	public void OnDisasterFinished(ushort disasterID)
	{
		for (int i = 0; i < this.m_DisasterExtensions.Count; i++)
		{
			this.m_DisasterExtensions[i].OnDisasterFinished(disasterID);
		}
		if (this.IsCustomDisaster(disasterID))
		{
			this.m_CustomDisasters.Remove(disasterID);
		}
	}

	public bool DisastersEnabled()
	{
		return false;
	}

	public ushort CreateDisaster(DisasterSettings settings)
	{
		DisasterInfo disasterInfo = new DisasterInfo();
		DisasterType type = settings.type;
		if (type == null)
		{
			disasterInfo = DisasterManager.FindDisasterInfo<StructureCollapseAI>();
		}
		else
		{
			this.m_DisasterTypeToInfoConversion.TryGetValue(type, out disasterInfo);
		}
		if (disasterInfo != null)
		{
			ushort num = 0;
			if (this.m_DisasterManager.CreateDisaster(out num, disasterInfo))
			{
				this.m_DisasterManager.m_disasters.m_buffer[(int)num].m_intensity = settings.intensity;
				this.m_DisasterManager.m_disasters.m_buffer[(int)num].m_targetPosition = new Vector3(settings.targetX, 0f, settings.targetZ);
				this.m_DisasterManager.m_disasters.m_buffer[(int)num].m_angle = settings.angle;
				if (!string.IsNullOrEmpty(settings.name))
				{
					this.m_DisasterManager.SetDisasterName(num, settings.name);
				}
				if (type == null)
				{
					this.m_CustomDisasters.Add(num);
				}
				else
				{
					DisasterData[] expr_112_cp_0 = this.m_DisasterManager.m_disasters.m_buffer;
					ushort expr_112_cp_1 = num;
					expr_112_cp_0[(int)expr_112_cp_1].m_flags = (expr_112_cp_0[(int)expr_112_cp_1].m_flags | DisasterData.Flags.SelfTrigger);
					disasterInfo.m_disasterAI.StartNow(num, ref this.m_DisasterManager.m_disasters.m_buffer[(int)num]);
				}
				return num;
			}
		}
		return 0;
	}

	public DisasterSettings GetDisasterSettings(ushort disasterID)
	{
		DisasterSettings result = default(DisasterSettings);
		DisasterData disasterData = this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID];
		if (this.m_CustomDisasters.Contains(disasterID))
		{
			result.type = 0;
		}
		else
		{
			DisasterInfo info = disasterData.Info;
			result.type = this.m_DisasterTypeToInfoConversion.FirstOrDefault((KeyValuePair<DisasterType, DisasterInfo> x) => x.Value == info).Key;
		}
		result.name = this.m_DisasterManager.GetDisasterName(disasterID);
		Vector3 targetPosition = disasterData.m_targetPosition;
		result.targetX = targetPosition.x;
		result.targetY = targetPosition.y;
		result.targetZ = targetPosition.z;
		result.angle = disasterData.m_angle;
		result.intensity = disasterData.m_intensity;
		return result;
	}

	public void SetDisasterName(ushort disasterID, string name)
	{
		this.m_DisasterManager.SetDisasterName(disasterID, name);
	}

	public void StartDisaster(ushort disasterID)
	{
		this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID].Info.m_disasterAI.StartNow(disasterID, ref this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID]);
	}

	public void DetectDisaster(ushort disasterID)
	{
		this.m_DisasterManager.DetectDisaster(disasterID, true);
	}

	public void ActivateDisaster(ushort disasterID)
	{
		this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID].Info.m_disasterAI.ActivateNow(disasterID, ref this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID]);
	}

	public void DeactivateDisaster(ushort disasterID)
	{
		this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID].Info.m_disasterAI.DeactivateNow(disasterID, ref this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID]);
	}

	public void EndDisaster(ushort disasterID)
	{
		this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID].m_flags = ((this.m_DisasterManager.m_disasters.m_buffer[(int)disasterID].m_flags & ~(DisasterData.Flags.Emerging | DisasterData.Flags.Active | DisasterData.Flags.Clearing)) | DisasterData.Flags.Finished);
	}

	public void DestroyStuff(float positionX, float positionY, float positionZ, float removeRadius, float destructionRadiusMin, float destructionRadiusMax, float burnRadiusMin, float burnRadiusMax)
	{
		InstanceID id = default(InstanceID);
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		Vector3 position;
		position..ctor(positionX, positionY, positionZ);
		float num = Mathf.Max(new float[]
		{
			removeRadius,
			destructionRadiusMax,
			burnRadiusMax
		});
		DisasterHelpers.DestroyStuff(0, group, position, num, num, removeRadius, destructionRadiusMin, destructionRadiusMax, burnRadiusMin, burnRadiusMax);
	}

	private DisasterManager m_DisasterManager;

	private List<IDisasterExtension> m_DisasterExtensions = new List<IDisasterExtension>();

	private List<ushort> m_CustomDisasters;

	private Dictionary<DisasterType, DisasterInfo> m_DisasterTypeToInfoConversion;
}
