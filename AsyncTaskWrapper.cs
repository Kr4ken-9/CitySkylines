﻿using System;
using ColossalFramework.Threading;

public class AsyncTaskWrapper : AsyncTaskBase
{
	public AsyncTaskWrapper(string taskName = null, params Task[] task) : base(taskName)
	{
		this.m_Tasks = task;
	}

	public override bool completedOrFailed
	{
		get
		{
			bool flag = true;
			for (int i = 0; i < this.m_Tasks.Length; i++)
			{
				if (this.m_Tasks[i] != null)
				{
					flag &= this.m_Tasks[i].get_hasEnded();
				}
			}
			return flag;
		}
	}

	public override bool isExecuting
	{
		get
		{
			for (int i = 0; i < this.m_Tasks.Length; i++)
			{
				if (this.m_Tasks[i] != null && !this.m_Tasks[i].get_hasEnded())
				{
					return true;
				}
			}
			return false;
		}
	}

	public void AddTask(Task t)
	{
		Task[] array = new Task[this.m_Tasks.Length + 1];
		this.m_Tasks.CopyTo(array, 0);
		array[array.Length - 1] = t;
		this.m_Tasks = array;
	}

	public override void Execute()
	{
		throw new NotImplementedException();
	}

	private Task[] m_Tasks;
}
