﻿using System;
using ColossalFramework;
using UnityEngine;

public class VehicleInfoSub : VehicleInfoBase
{
	public override void InitializePrefab()
	{
		if (this.m_referenceCount++ == 0)
		{
			base.InitializePrefab();
			if (this.m_generatedInfo == null)
			{
				throw new PrefabException(this, "Generated info missing");
			}
			MeshFilter component = base.GetComponent<MeshFilter>();
			if (component != null && component.get_sharedMesh() != null)
			{
				this.m_mesh = component.get_sharedMesh();
				this.m_material = base.GetComponent<Renderer>().get_sharedMaterial();
				string tag = this.m_material.GetTag("RenderType", false);
				if (this.m_material == null)
				{
					throw new PrefabException(this, "Material missing");
				}
				if (this.m_generatedInfo.m_size == Vector3.get_zero())
				{
					throw new PrefabException(this, "Generated info has zero size");
				}
				if (this.m_generatedInfo.m_vehicleInfo != null)
				{
					if (this.m_generatedInfo.m_vehicleInfo.m_mesh != this.m_mesh)
					{
						throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_vehicleInfo.get_gameObject().get_name() + ")");
					}
				}
				else
				{
					this.m_generatedInfo.m_vehicleInfo = this;
				}
				if (this.m_generatedInfo.m_tyres == null || this.m_generatedInfo.m_tyres.Length == 0)
				{
					if (tag != "PropAnimUV")
					{
						CODebugBase<LogChannel>.Warn(LogChannel.Core, "Tyre array is null or zero size: " + base.get_gameObject().get_name(), base.get_gameObject());
					}
					this.m_generatedInfo.m_tyres = new Vector4[]
					{
						new Vector4(0f, 0f, 0f, 1f)
					};
				}
				if (this.m_lodObject != null)
				{
					component = this.m_lodObject.GetComponent<MeshFilter>();
					this.m_lodMesh = component.get_sharedMesh();
					this.m_lodMaterial = this.m_lodObject.GetComponent<Renderer>().get_sharedMaterial();
				}
				base.InitMeshInfo(this.m_mesh.get_bounds().get_size());
			}
		}
	}

	public override void DestroyPrefab()
	{
		if (--this.m_referenceCount == 0)
		{
			base.DestroyMeshInfo();
			base.DestroyPrefab();
		}
	}

	public override void RefreshLevelOfDetail()
	{
		if (this.m_mesh != null)
		{
			base.RefreshLevelOfDetail(this.m_mesh.get_bounds().get_size());
		}
	}

	[NonSerialized]
	private int m_referenceCount;
}
