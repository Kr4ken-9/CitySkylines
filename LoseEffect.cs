﻿using System;
using ColossalFramework;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;

public class LoseEffect : TriggerEffect
{
	public override void Activate()
	{
		base.Activate();
		LoseEffect.LoseScenario();
	}

	public static void LoseScenario()
	{
		SimulationMetaData metaData = Singleton<SimulationManager>.get_instance().m_metaData;
		string name = metaData.m_ScenarioAsset;
		if (!string.IsNullOrEmpty(name) && !metaData.m_scenarioWon && !metaData.m_scenarioFailed)
		{
			metaData.m_scenarioFailed = true;
			Singleton<UnlockManager>.get_instance().DisableTriggers();
			ThreadHelper.get_dispatcher().Dispatch(delegate
			{
				string text = "ScenarioLoseCount[" + name + "]";
				SavedInt savedInt = new SavedInt(text, Settings.userGameState, 0, false);
				int value = savedInt.get_value();
				savedInt.set_value(value + 1);
				if (value == 0)
				{
					text = "ScenarioLoseCount[Unique Scenarios]";
					savedInt = new SavedInt(text, Settings.userGameState, 0, false);
					value = savedInt.get_value();
					savedInt.set_value(value + 1);
					if (value == 9 && !PlatformService.get_achievements().get_Item("TheUnderdog").get_achieved())
					{
						PlatformService.get_achievements().get_Item("TheUnderdog").Unlock();
					}
				}
			});
		}
	}
}
