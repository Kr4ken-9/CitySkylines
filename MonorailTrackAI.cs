﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class MonorailTrackAI : TrainTrackBaseAI
{
	public override bool ColorizeProps(InfoManager.InfoMode infoMode)
	{
		return infoMode == InfoManager.InfoMode.Pollution || base.ColorizeProps(infoMode);
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<TransportManager>.get_instance().m_monorailLine.Activate(properties.m_monorailLine);
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		ushort building = instance.m_nodes.m_buffer[(int)data.m_startNode].m_building;
		if (building != 0 && (instance2.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Building[] expr_8E_cp_0 = instance2.m_buildings.m_buffer;
			ushort expr_8E_cp_1 = building;
			expr_8E_cp_0[(int)expr_8E_cp_1].m_flags = (expr_8E_cp_0[(int)expr_8E_cp_1].m_flags & ~Building.Flags.Collapsed);
			instance2.UpdateBuildingRenderer(building, true);
			instance2.UpdateFlags(building, Building.Flags.Collapsed);
		}
		building = instance.m_nodes.m_buffer[(int)data.m_endNode].m_building;
		if (building != 0 && (instance2.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Building[] expr_107_cp_0 = instance2.m_buildings.m_buffer;
			ushort expr_107_cp_1 = building;
			expr_107_cp_0[(int)expr_107_cp_1].m_flags = (expr_107_cp_0[(int)expr_107_cp_1].m_flags & ~Building.Flags.Collapsed);
			instance2.UpdateBuildingRenderer(building, true);
			instance2.UpdateFlags(building, Building.Flags.Collapsed);
		}
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		float num2 = (Mathf.Max(0f, startHeight) + Mathf.Max(0f, endHeight)) * 0.5f;
		int num3 = Mathf.RoundToInt(num / 8f + 0.1f);
		int num4 = Mathf.RoundToInt(num2 / 4f + 0.1f);
		int result = this.m_constructionCost * num3 + this.m_elevationCost * num4;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if ((data.m_flags & NetNode.Flags.Outside) == NetNode.Flags.None)
		{
			if (this.m_middlePillarInfo != null && (data.m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
			{
				building = this.m_middlePillarInfo;
				heightOffset = this.m_middlePillarOffset - 1f - this.m_middlePillarInfo.m_generatedInfo.m_size.y;
				return;
			}
			if (this.m_bridgePillarInfo != null)
			{
				building = this.m_bridgePillarInfo;
				if ((data.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None && this.m_bridgePillarInfo2 != null)
				{
					NetManager instance = Singleton<NetManager>.get_instance();
					ushort num = 0;
					for (int i = 0; i < 8; i++)
					{
						ushort segment = data.GetSegment(i);
						if (segment != 0)
						{
							if (num == 0)
							{
								num = segment;
							}
							else
							{
								Vector3 direction = instance.m_segments.m_buffer[(int)num].GetDirection(nodeID);
								Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(nodeID);
								if (direction.x * direction2.x + direction.z * direction2.z > -0.7f)
								{
									building = this.m_bridgePillarInfo2;
								}
							}
						}
					}
				}
				else if ((data.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None && this.m_bridgePillarInfo3 != null && data.CountSegments() >= 3)
				{
					building = this.m_bridgePillarInfo3;
				}
				heightOffset = this.m_bridgePillarOffset - 1f - building.m_generatedInfo.m_size.y;
				return;
			}
		}
		base.GetNodeBuilding(nodeID, ref data, out building, out heightOffset);
	}

	public override float GetNodeBuildingAngle(ushort nodeID, ref NetNode data)
	{
		if ((data.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = data.GetSegment(i);
				if (segment != 0)
				{
					if (num == 0)
					{
						num = segment;
					}
					else
					{
						Vector3 direction = instance.m_segments.m_buffer[(int)num].GetDirection(nodeID);
						Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(nodeID);
						if (direction.x * direction2.x + direction.z * direction2.z > -0.7f)
						{
							Vector3 vector = direction + direction2;
							return Mathf.Atan2(vector.x, -vector.z) * 0.159154937f + 1.25f;
						}
					}
				}
			}
		}
		return base.GetNodeBuildingAngle(nodeID, ref data);
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return this.m_info.m_halfWidth - 2000f;
		}
		if ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_startNode].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			return this.m_info.m_halfWidth - 3000f;
		}
		if ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_endNode].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			return this.m_info.m_halfWidth - 3000f;
		}
		if (this.m_bridgePillarInfo == null || !this.m_canModify)
		{
			return this.m_info.m_halfWidth - 6000f;
		}
		return this.m_info.m_halfWidth - 5000f;
	}

	public override ToolBase.ToolErrors CanConnectTo(ushort node, ushort segment, ulong[] segmentMask)
	{
		return ToolBase.ToolErrors.None;
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
		{
			return;
		}
		if (!this.m_info.m_useFixedHeight)
		{
			return;
		}
		Singleton<NetManager>.get_instance().ReleaseSegment(segmentID, true);
	}

	public override bool RequireDoubleSegments()
	{
		return this.m_doubleLength;
	}

	public override bool CanModify()
	{
		return this.m_canModify;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 2;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if (!base.CollapseSegment(segmentID, ref data, group, demolish))
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		ushort building = instance.m_nodes.m_buffer[(int)data.m_startNode].m_building;
		if (building != 0 && this.AllSegmentsCollapsed(data.m_startNode, ref instance.m_nodes.m_buffer[(int)data.m_startNode]))
		{
			BuildingInfo info = instance2.m_buildings.m_buffer[(int)building].Info;
			info.m_buildingAI.CollapseBuilding(building, ref instance2.m_buildings.m_buffer[(int)building], group, false, false, 255);
		}
		building = instance.m_nodes.m_buffer[(int)data.m_endNode].m_building;
		if (building != 0 && this.AllSegmentsCollapsed(data.m_endNode, ref instance.m_nodes.m_buffer[(int)data.m_endNode]))
		{
			BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)building].Info;
			info2.m_buildingAI.CollapseBuilding(building, ref instance2.m_buildings.m_buffer[(int)building], group, false, false, 255);
		}
		return true;
	}

	private bool AllSegmentsCollapsed(ushort node, ref NetNode data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Collapsed) == NetSegment.Flags.None)
			{
				return false;
			}
		}
		return true;
	}

	public override bool IsOverground()
	{
		return true;
	}

	public override void GetRayCastHeights(ushort segmentID, ref NetSegment data, out float leftMin, out float rightMin, out float max)
	{
		leftMin = this.m_info.m_minHeight;
		rightMin = this.m_info.m_minHeight;
		max = 8.5f;
	}

	public override float GetTerrainLowerOffset()
	{
		return Mathf.Max(0f, this.m_info.m_minHeight - 1f);
	}

	public override bool CanIntersect(NetInfo with)
	{
		return false;
	}

	public override bool CanConnect(NetInfo other)
	{
		return (other.m_vehicleTypes & VehicleInfo.VehicleType.Monorail) != VehicleInfo.VehicleType.None;
	}

	public override float GetSnapElevation()
	{
		return 8.5f;
	}

	public BuildingInfo m_bridgePillarInfo;

	public BuildingInfo m_bridgePillarInfo2;

	public BuildingInfo m_bridgePillarInfo3;

	public BuildingInfo m_middlePillarInfo;

	[CustomizableProperty("Elevation Cost", "Properties")]
	public int m_elevationCost = 2000;

	[CustomizableProperty("Bridge Pillar Offset", "Properties")]
	public float m_bridgePillarOffset;

	[CustomizableProperty("Middle Pillar Offset", "Properties")]
	public float m_middlePillarOffset;

	[CustomizableProperty("Double Length", "Properties")]
	public bool m_doubleLength;

	[CustomizableProperty("Can Modify", "Properties")]
	public bool m_canModify = true;
}
