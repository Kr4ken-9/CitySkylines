﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class GameAreaInfoPanel : ToolsModifierControl
{
	private void Start()
	{
		this.m_FullscreenContainer = UIView.Find("FullScreenContainer");
		if (this.m_FullscreenContainer != null)
		{
			this.m_FullscreenContainer.AttachUIComponent(base.get_gameObject());
		}
		this.m_Title = base.Find<UILabel>("Title");
		this.m_BuildableArea = base.Find<UILabel>("BuildableArea");
		this.m_Price = base.Find<UILabel>("Price");
		this.m_PurchasePanel = base.Find<UIPanel>("PurchasePanel");
		this.m_OilResources = base.Find<UIProgressBar>("ResourceBarOil");
		this.m_OreResources = base.Find<UIProgressBar>("ResourceBarOre");
		this.m_ForestryResources = base.Find<UIProgressBar>("ResourceBarForestry");
		this.m_FertilityResources = base.Find<UIProgressBar>("ResourceBarFarming");
		this.m_OilNoResources = base.Find("ResourceOil").Find<UISprite>("NoNoNo");
		this.m_OreNoResources = base.Find("ResourceOre").Find<UISprite>("NoNoNo");
		this.m_ForestryNoResources = base.Find("ResourceForestry").Find<UISprite>("NoNoNo");
		this.m_FertilityNoResources = base.Find("ResourceFarming").Find<UISprite>("NoNoNo");
		this.m_Water = base.Find<UISprite>("Water");
		this.m_NoWater = this.m_Water.Find<UISprite>("NoNoNo");
		this.m_Highway = base.Find<UISprite>("Highway");
		this.m_NoHighway = this.m_Highway.Find<UISprite>("NoNoNo");
		this.m_InHighway = this.m_Highway.Find<UISprite>("Incoming");
		this.m_OutHighway = this.m_Highway.Find<UISprite>("Outgoing");
		this.m_Train = base.Find<UISprite>("Train");
		this.m_NoTrain = this.m_Train.Find<UISprite>("NoNoNo");
		this.m_InTrain = this.m_Train.Find<UISprite>("Incoming");
		this.m_OutTrain = this.m_Train.Find<UISprite>("Outgoing");
		this.m_Ship = base.Find<UISprite>("Ship");
		this.m_NoShip = this.m_Ship.Find<UISprite>("NoNoNo");
		this.m_InShip = this.m_Ship.Find<UISprite>("Incoming");
		this.m_OutShip = this.m_Ship.Find<UISprite>("Outgoing");
		this.m_Plane = base.Find<UISprite>("Plane");
		this.m_NoPlane = this.m_Plane.Find<UISprite>("NoNoNo");
		this.m_InPlane = this.m_Plane.Find<UISprite>("Incoming");
		this.m_OutPlane = this.m_Plane.Find<UISprite>("Outgoing");
	}

	public static void Show(int areaIndex)
	{
		GameAreaInfoPanel gameAreaInfoPanel = UIView.get_library().Show<GameAreaInfoPanel>("GameAreaInfoPanel");
		if (gameAreaInfoPanel != null)
		{
			gameAreaInfoPanel.ShowInternal(areaIndex);
		}
	}

	private void LateUpdate()
	{
		if (base.get_component().get_isVisible())
		{
			this.UpdatePanel();
		}
	}

	private void UpdatePanel()
	{
		if (this.m_AreaIndex != -1)
		{
			int x;
			int z;
			Singleton<GameAreaManager>.get_instance().GetTileXZ(this.m_AreaIndex, out x, out z);
			Vector3 areaPositionSmooth = Singleton<GameAreaManager>.get_instance().GetAreaPositionSmooth(x, z);
			Vector3 vector = Camera.get_main().WorldToScreenPoint(areaPositionSmooth);
			UIView uIView = base.get_component().GetUIView();
			Vector2 vector2 = (!(this.m_FullscreenContainer != null)) ? uIView.GetScreenResolution() : this.m_FullscreenContainer.get_size();
			vector /= uIView.get_inputScale();
			Vector3 vector3 = UIPivotExtensions.UpperLeftToTransform(base.get_component().get_pivot(), base.get_component().get_size(), base.get_component().get_arbitraryPivotOffset());
			Vector3 relativePosition = uIView.ScreenPointToGUI(vector) + new Vector2(vector3.x, vector3.y);
			if (relativePosition.x < 0f)
			{
				relativePosition.x = 0f;
			}
			if (relativePosition.y < 0f)
			{
				relativePosition.y = 0f;
			}
			if (relativePosition.x + base.get_component().get_width() > vector2.x)
			{
				relativePosition.x = vector2.x - base.get_component().get_width();
			}
			if (relativePosition.y + base.get_component().get_height() > vector2.y)
			{
				relativePosition.y = vector2.y - base.get_component().get_height();
			}
			base.get_component().set_relativePosition(relativePosition);
			uint num;
			uint num2;
			uint num3;
			uint num4;
			uint num5;
			Singleton<NaturalResourceManager>.get_instance().GetTileResources(x, z, out num, out num2, out num3, out num4, out num5);
			this.m_OilNoResources.set_isVisible(num2 == 0u);
			this.m_OreNoResources.set_isVisible(num == 0u);
			this.m_ForestryNoResources.set_isVisible(num3 == 0u);
			this.m_FertilityNoResources.set_isVisible(num4 == 0u);
			bool flag = num5 != 0u;
			this.m_Water.set_tooltip((!flag) ? Locale.Get("AREA_NO_WATER") : Locale.Get("AREA_YES_WATER"));
			this.m_NoWater.set_isVisible(!flag);
			int num6;
			int num7;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, out num6, out num7);
			int tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip);
			bool flag2 = (num6 == 0 && num7 == 0) || tileNodeCount == 0;
			this.m_Ship.set_tooltip((!flag2) ? Locale.Get("AREA_YES_SHIPCONNECTION") : Locale.Get("AREA_NO_SHIPCONNECTION"));
			this.m_NoShip.set_isVisible(flag2);
			this.m_InShip.set_isVisible(num6 > 0 && tileNodeCount > 0);
			this.m_OutShip.set_isVisible(num7 > 0 && tileNodeCount > 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, out num6, out num7);
			tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain);
			bool flag3 = (num6 == 0 && num7 == 0) || tileNodeCount == 0;
			this.m_Train.set_tooltip((!flag3) ? Locale.Get("AREA_YES_TRAINCONNECTION") : Locale.Get("AREA_NO_TRAINCONNECTION"));
			this.m_NoTrain.set_isVisible(flag3);
			this.m_InTrain.set_isVisible(num6 > 0 && tileNodeCount > 0);
			this.m_OutTrain.set_isVisible(num7 > 0 && tileNodeCount > 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, out num6, out num7);
			bool flag4 = num6 == 0 && num7 == 0;
			this.m_Plane.set_tooltip((!flag4) ? Locale.Get("AREA_YES_PLANECONNECTION") : Locale.Get("AREA_NO_PLANECONNECTION"));
			this.m_NoPlane.set_isVisible(flag4);
			this.m_InPlane.set_isVisible(num6 > 0);
			this.m_OutPlane.set_isVisible(num7 > 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.Road, ItemClass.SubService.None, out num6, out num7);
			tileNodeCount = Singleton<NetManager>.get_instance().GetTileNodeCount(x, z, ItemClass.Service.Road, ItemClass.SubService.None);
			bool flag5 = (num6 == 0 && num7 == 0) || tileNodeCount == 0;
			this.m_Highway.set_tooltip((!flag5) ? Locale.Get("AREA_YES_HIGHWAYCONNECTION") : Locale.Get("AREA_NO_HIGHWAYCONNECTION"));
			this.m_NoHighway.set_isVisible(flag5);
			this.m_InHighway.set_isVisible(num6 > 0 && tileNodeCount > 0);
			this.m_OutHighway.set_isVisible(num7 > 0 && tileNodeCount > 0);
			float num8 = 3686400f;
			float num9 = 1139.0625f;
			float num10 = num8 / num9 * 255f;
			float tileFlatness = Singleton<TerrainManager>.get_instance().GetTileFlatness(x, z);
			float num11 = tileFlatness * (num10 - num5) / num10;
			int num12 = Singleton<GameAreaManager>.get_instance().CalculateTilePrice(num, num2, num3, num4, num5, !flag5, !flag3, !flag2, !flag4, tileFlatness);
			this.m_BuildableArea.set_text(StringUtils.SafeFormat(Locale.Get("AREA_BUILDABLE"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), Mathf.Max(0, Mathf.Min(Mathf.FloorToInt(num11 * 100f), 100)))));
			this.m_Price.set_text((num12 / 100).ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()));
			bool flag6 = Singleton<GameAreaManager>.get_instance().IsUnlocked(x, z);
			this.m_Title.set_text(Locale.Get((!flag6) ? "AREA_NEWTILE" : "AREA_OWNEDTILE"));
			this.m_PurchasePanel.set_isVisible(!flag6);
			this.m_PurchasePanel.set_isEnabled(Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.LandPrice, num12) == num12);
			float value = Mathf.Pow(num2 / num10, this.m_OilExponent);
			float value2 = Mathf.Pow(num / num10, this.m_OreExponent);
			float value3 = Mathf.Pow(num3 / num10, this.m_ForestryExponent);
			float value4 = Mathf.Pow(num4 / num10, this.m_FarmingExponent);
			if (!ValueAnimator.IsAnimating("Oil"))
			{
				this.m_OilResources.set_value(value);
			}
			if (!ValueAnimator.IsAnimating("Ore"))
			{
				this.m_OreResources.set_value(value2);
			}
			if (!ValueAnimator.IsAnimating("Forest"))
			{
				this.m_ForestryResources.set_value(value3);
			}
			if (!ValueAnimator.IsAnimating("Fertility"))
			{
				this.m_FertilityResources.set_value(value4);
			}
		}
	}

	private void ShowInternal(int areaIndex)
	{
		this.m_AreaIndex = areaIndex;
		int x;
		int z;
		Singleton<GameAreaManager>.get_instance().GetTileXZ(this.m_AreaIndex, out x, out z);
		uint num;
		uint num2;
		uint num3;
		uint num4;
		uint num5;
		Singleton<NaturalResourceManager>.get_instance().GetTileResources(x, z, out num, out num2, out num3, out num4, out num5);
		float num6 = 3686400f;
		float num7 = 1139.0625f;
		float num8 = num6 / num7 * 255f;
		float num9 = Mathf.Pow(num2 / num8, this.m_OilExponent);
		float num10 = Mathf.Pow(num / num8, this.m_OreExponent);
		float num11 = Mathf.Pow(num3 / num8, this.m_ForestryExponent);
		float num12 = Mathf.Pow(num4 / num8, this.m_FarmingExponent);
		ValueAnimator.Cancel("Oil");
		ValueAnimator.Cancel("Ore");
		ValueAnimator.Cancel("Forest");
		ValueAnimator.Cancel("Fertility");
		this.m_OilResources.set_value(0f);
		this.m_OreResources.set_value(0f);
		this.m_ForestryResources.set_value(0f);
		this.m_FertilityResources.set_value(0f);
		ValueAnimator.Animate("Oil", delegate(float val)
		{
			this.m_OilResources.set_value(val);
		}, new AnimatedFloat(0f, num9, this.m_InterpolationTime, this.m_InterpolationEasingType));
		ValueAnimator.Animate("Ore", delegate(float val)
		{
			this.m_OreResources.set_value(val);
		}, new AnimatedFloat(0f, num10, this.m_InterpolationTime, this.m_InterpolationEasingType));
		ValueAnimator.Animate("Forest", delegate(float val)
		{
			this.m_ForestryResources.set_value(val);
		}, new AnimatedFloat(0f, num11, this.m_InterpolationTime, this.m_InterpolationEasingType));
		ValueAnimator.Animate("Fertility", delegate(float val)
		{
			this.m_FertilityResources.set_value(val);
		}, new AnimatedFloat(0f, num12, this.m_InterpolationTime, this.m_InterpolationEasingType));
		this.UpdatePanel();
	}

	public static void Hide()
	{
		if (UIView.get_library() != null)
		{
			GameAreaInfoPanel gameAreaInfoPanel = UIView.get_library().Hide<GameAreaInfoPanel>("GameAreaInfoPanel");
			if (gameAreaInfoPanel != null)
			{
				gameAreaInfoPanel.m_AreaIndex = -1;
			}
		}
	}

	protected void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Purchase");
			uIButton.Focus();
		}
	}

	protected void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	protected void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				p.Use();
				GameAreaInfoPanel.Hide();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnUnlock();
			}
		}
	}

	public void OnClose()
	{
		GameAreaInfoPanel.Hide();
	}

	[DebuggerHidden]
	private IEnumerator UnlockAreaCoroutine(int areaIndex)
	{
		GameAreaInfoPanel.<UnlockAreaCoroutine>c__Iterator0 <UnlockAreaCoroutine>c__Iterator = new GameAreaInfoPanel.<UnlockAreaCoroutine>c__Iterator0();
		<UnlockAreaCoroutine>c__Iterator.areaIndex = areaIndex;
		<UnlockAreaCoroutine>c__Iterator.$this = this;
		return <UnlockAreaCoroutine>c__Iterator;
	}

	public void OnUnlock()
	{
		base.StartCoroutine(this.UnlockAreaCoroutine(this.m_AreaIndex));
	}

	public float m_OilExponent = 1f;

	public float m_OreExponent = 1f;

	public float m_ForestryExponent = 1f;

	public float m_FarmingExponent = 1f;

	public float m_InterpolationTime = 0.7f;

	public EasingType m_InterpolationEasingType = 2;

	private int m_AreaIndex;

	private UIComponent m_FullscreenContainer;

	private UIProgressBar m_OilResources;

	private UIProgressBar m_OreResources;

	private UIProgressBar m_ForestryResources;

	private UIProgressBar m_FertilityResources;

	private UISprite m_OilNoResources;

	private UISprite m_OreNoResources;

	private UISprite m_ForestryNoResources;

	private UISprite m_FertilityNoResources;

	private UILabel m_Title;

	private UISprite m_Water;

	private UISprite m_NoWater;

	private UISprite m_Highway;

	private UISprite m_NoHighway;

	private UISprite m_InHighway;

	private UISprite m_OutHighway;

	private UISprite m_Train;

	private UISprite m_NoTrain;

	private UISprite m_InTrain;

	private UISprite m_OutTrain;

	private UISprite m_Ship;

	private UISprite m_NoShip;

	private UISprite m_InShip;

	private UISprite m_OutShip;

	private UISprite m_Plane;

	private UISprite m_NoPlane;

	private UISprite m_InPlane;

	private UISprite m_OutPlane;

	private UILabel m_BuildableArea;

	private UILabel m_Price;

	private UIPanel m_PurchasePanel;
}
