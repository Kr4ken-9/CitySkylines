﻿using System;

public class GarbageGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Garbage;
		}
	}
}
