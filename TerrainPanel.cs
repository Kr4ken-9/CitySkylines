﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class TerrainPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	protected override void OnHideOptionBars()
	{
		this.ShowBrushOptionsPanel(false);
		this.ShowUndoTerrainOptionsPanel(false);
		this.ShowLevelHeightPanel(false);
	}

	protected override void Start()
	{
		if (this.m_OptionsBar != null)
		{
			if (this.m_OptionsBrushPanel == null)
			{
				this.m_OptionsBrushPanel = this.m_OptionsBar.Find<UIPanel>("BrushPanel");
			}
			if (this.m_OptionsLevelHeightPanel == null)
			{
				this.m_OptionsLevelHeightPanel = this.m_OptionsBar.Find<UIPanel>("LevelHeightPanel");
			}
			if (this.m_OptionsUndoTerrainPanel == null)
			{
				this.m_OptionsUndoTerrainPanel = this.m_OptionsBar.Find<UIPanel>("UndoTerrainPanel");
			}
		}
		base.Start();
	}

	private void ShowUndoTerrainOptionsPanel(bool show)
	{
		if (this.m_OptionsUndoTerrainPanel != null)
		{
			this.m_OptionsUndoTerrainPanel.set_isVisible(show);
			this.m_OptionsUndoTerrainPanel.set_zOrder(2);
		}
	}

	private void ShowBrushOptionsPanel(bool show)
	{
		if (this.m_OptionsBrushPanel != null)
		{
			this.m_OptionsBrushPanel.set_isVisible(show);
			this.m_OptionsBrushPanel.set_zOrder(1);
		}
	}

	private void ShowLevelHeightPanel(bool show)
	{
		if (this.m_OptionsLevelHeightPanel != null)
		{
			this.m_OptionsLevelHeightPanel.set_isVisible(show);
			this.m_OptionsLevelHeightPanel.set_zOrder(0);
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < TerrainPanel.kTools.Length; i++)
		{
			this.SpawnEntry(TerrainPanel.kTools[i].get_enumName(), i);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		int zOrder = comp.get_zOrder();
		TerrainTool terrainTool = ToolsModifierControl.SetTool<TerrainTool>();
		if (terrainTool != null)
		{
			this.ShowUndoTerrainOptionsPanel(true);
			this.ShowBrushOptionsPanel(true);
			if (zOrder == 1 || zOrder == 3)
			{
				this.ShowLevelHeightPanel(true);
			}
			else
			{
				this.ShowLevelHeightPanel(false);
			}
			terrainTool.m_mode = TerrainPanel.kTools[zOrder].get_enumValue();
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("TERRAIN_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("TERRAIN_DESC", name)
		});
		return base.CreateButton(name, tooltip, "Terrain" + name, index, GeneratedPanel.tooltipBox);
	}

	private static readonly PositionData<TerrainTool.Mode>[] kTools = Utils.GetOrderedEnumData<TerrainTool.Mode>();

	private UIPanel m_OptionsUndoTerrainPanel;

	private UIPanel m_OptionsBrushPanel;

	private UIPanel m_OptionsLevelHeightPanel;
}
