﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class WeatherProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	private void OverrideFromMapTheme()
	{
		MapThemeMetaData mapThemeMetaData = Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData;
		if (mapThemeMetaData != null)
		{
			this.m_minTemperatureDay = mapThemeMetaData.minTemperatureDay;
			this.m_maxTemperatureDay = mapThemeMetaData.maxTemperatureDay;
			this.m_minTemperatureNight = mapThemeMetaData.minTemperatureNight;
			this.m_maxTemperatureNight = mapThemeMetaData.maxTemperatureNight;
			this.m_minTemperatureRain = mapThemeMetaData.minTemperatureRain;
			this.m_maxTemperatureRain = mapThemeMetaData.maxTemperatureRain;
			this.m_minTemperatureFog = mapThemeMetaData.minTemperatureFog;
			this.m_maxTemperatureFog = mapThemeMetaData.maxTemperatureFog;
			this.m_rainProbabilityDay = mapThemeMetaData.rainProbabilityDay;
			this.m_rainProbabilityNight = mapThemeMetaData.rainProbabilityNight;
			this.m_fogProbabilityDay = mapThemeMetaData.fogProbabilityDay;
			this.m_fogProbabilityNight = mapThemeMetaData.fogProbabilityNight;
			this.m_northernLightsProbability = mapThemeMetaData.northernLightsProbability;
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		WeatherProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new WeatherProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private Mesh GenerateLightningMesh()
	{
		int num = 200;
		int num2 = 4;
		float num3 = 5000f;
		int num4 = 0;
		int num5 = num;
		int num6 = 1;
		for (int i = 0; i < num2; i++)
		{
			num4 += num5 * num6;
			num5 /= 2;
			num6 *= 3;
		}
		Vector3[] array = new Vector3[num4 * 4];
		Vector2[] array2 = new Vector2[num4 * 4];
		int[] array3 = new int[num4 * 6];
		int num7 = 0;
		int num8 = 0;
		num5 = num;
		num6 = 1;
		float num9 = 0f;
		for (int j = 0; j < num2; j++)
		{
			for (int k = 0; k < num6; k++)
			{
				for (int l = 0; l < num5; l++)
				{
					float num10 = (float)l / (float)num5;
					num10 *= num3;
					num10 += num9;
					array[num7] = new Vector3((float)j - 1f, num10, (float)k - 1f);
					array2[num7] = new Vector2(-1f, -1f);
					num7++;
					array[num7] = new Vector3((float)j - 1f, num10, (float)k + 1f);
					array2[num7] = new Vector2(-1f, 1f);
					num7++;
					array[num7] = new Vector3((float)j + 1f, num10, (float)k + 1f);
					array2[num7] = new Vector2(1f, 1f);
					num7++;
					array[num7] = new Vector3((float)j + 1f, num10, (float)k - 1f);
					array2[num7] = new Vector2(1f, -1f);
					num7++;
					array3[num8++] = num7 - 4;
					array3[num8++] = num7 - 3;
					array3[num8++] = num7 - 1;
					array3[num8++] = num7 - 1;
					array3[num8++] = num7 - 3;
					array3[num8++] = num7 - 2;
				}
			}
			num5 /= 2;
			num3 /= 2f;
			num6 *= 3;
			num9 += num3;
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_uv(array2);
		mesh.set_triangles(array3);
		mesh.set_bounds(new Bounds(new Vector3(0f, 2500f, 0f), new Vector3(2000f, 5000f, 2000f)));
		return mesh;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("WeatherProperties");
			Singleton<WeatherManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
		if (this.m_lightningMesh != null)
		{
			Object.Destroy(this.m_lightningMesh);
			this.m_lightningMesh = null;
		}
	}

	public Material m_lightningMaterial;

	[NonSerialized]
	public Mesh m_lightningMesh;

	public AudioInfo m_lightningSound;

	public Transform m_windZone;

	public bool m_rainIsSnow;

	public float m_minTemperatureDay = 15f;

	public float m_maxTemperatureDay = 30f;

	public float m_minTemperatureNight = 1f;

	public float m_maxTemperatureNight = 16f;

	public float m_minTemperatureRain = 5f;

	public float m_maxTemperatureRain = 15f;

	public float m_minTemperatureFog = 5f;

	public float m_maxTemperatureFog = 15f;

	public int m_rainProbabilityDay = 15;

	public int m_rainProbabilityNight = 20;

	public int m_fogProbabilityDay = 15;

	public int m_fogProbabilityNight = 20;

	public int m_cloudProbabilityDay = 10;

	public int m_cloudProbabilityNight = 10;

	public int m_northernLightsProbability;

	public int m_rainbowProbability = 100;
}
