﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class RoadsOptionPanel : OptionPanelBase
{
	private void Awake()
	{
		base.HidePanel();
		NetTool netTool = ToolsModifierControl.GetTool<NetTool>();
		if (netTool != null)
		{
			this.m_ElevationStepButton = base.Find<UIMultiStateButton>("ElevationStep");
			if (this.m_ElevationStepButton != null)
			{
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						this.UpdateElevationStep(netTool, this.m_ElevationStepButton.get_activeStateIndex());
					}
				});
				this.m_ElevationStepButton.add_eventActiveStateIndexChanged(delegate(UIComponent c, int index)
				{
					this.UpdateElevationStep(netTool, index);
				});
			}
			UITabstrip strip = base.Find<UITabstrip>("ToolMode");
			if (strip != null)
			{
				strip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int index)
				{
					netTool.m_mode = this.m_Modes[index];
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (visible)
					{
						if (strip.get_selectedIndex() == -1)
						{
							strip.set_selectedIndex(0);
						}
						netTool.m_mode = this.m_Modes[strip.get_selectedIndex()];
					}
					else
					{
						netTool.m_mode = NetTool.Mode.Straight;
					}
				});
			}
			UIMultiStateButton snap = base.Find<UIMultiStateButton>("SnappingToggle");
			if (snap != null)
			{
				snap.set_activeStateIndex((!SnapSettingsPanel.isVisible) ? 0 : 1);
				snap.add_eventActiveStateIndexChanged(delegate(UIComponent sender, int index)
				{
					bool flag = index == 1;
					if (flag)
					{
						UIComponent uIComponent = UIView.get_library().Get<UIComponent>("SnapSettingsPanel");
						Vector3 vector = snap.get_absolutePosition();
						vector -= Vector3.get_up() * uIComponent.get_height();
						uIComponent.set_absolutePosition(vector);
					}
					SnapSettingsPanel.ToggleVisibility(flag);
				});
				base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
				{
					if (!visible)
					{
						SnapSettingsPanel.ToggleVisibility(false);
					}
					else
					{
						SnapSettingsPanel.ToggleVisibility(snap.get_activeStateIndex() == 1);
					}
				});
			}
		}
	}

	private void UpdateElevationStep(NetTool netTool, int index)
	{
		netTool.m_elevationDivider = (int)Mathf.Pow(2f, (float)index);
	}

	public NetTool.Mode[] m_Modes = new NetTool.Mode[]
	{
		NetTool.Mode.Straight,
		NetTool.Mode.Curved,
		NetTool.Mode.Freeform,
		NetTool.Mode.Upgrade
	};

	private UIMultiStateButton m_ElevationStepButton;
}
