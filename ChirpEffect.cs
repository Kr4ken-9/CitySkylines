﻿using System;
using ColossalFramework;
using ColossalFramework.IO;

public class ChirpEffect : TriggerEffect
{
	public override void Activate()
	{
		base.Activate();
		uint randomResidentID = Singleton<MessageManager>.get_instance().GetRandomResidentID();
		Singleton<MessageManager>.get_instance().QueueMessage(new CustomCitizenMessage(randomResidentID, this.m_message, null));
	}

	public override void Serialize(DataSerializer s)
	{
		base.Serialize(s);
		s.WriteUniqueString(this.m_message);
	}

	public override void Deserialize(DataSerializer s)
	{
		base.Deserialize(s);
		this.m_message = s.ReadUniqueString();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
		base.AfterDeserialize(s);
	}

	public string m_message;
}
