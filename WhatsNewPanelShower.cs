﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class WhatsNewPanelShower : MonoBehaviour
{
	private void Start()
	{
		WhatsNewPanelPanel whatsNewPanelPanel = UIView.get_library().Get<WhatsNewPanelPanel>("WhatsNewPanelPanel");
		if (whatsNewPanelPanel.ShouldShow())
		{
			whatsNewPanelPanel.Show();
		}
	}
}
