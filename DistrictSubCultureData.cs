﻿using System;
using ColossalFramework.IO;

public struct DistrictSubCultureData
{
	public void Add(ref DistrictSubCultureData data)
	{
		this.m_tempCount += data.m_tempCount;
		this.m_tempHappiness += data.m_tempHappiness;
	}

	public void Add(int count, int happiness)
	{
		this.m_tempCount += (uint)count;
		this.m_tempHappiness += (uint)(happiness * count);
	}

	public void Update()
	{
		this.m_finalCount = this.m_tempCount;
		if (this.m_tempCount != 0u)
		{
			this.m_finalHappiness = (byte)((this.m_tempHappiness + (this.m_tempCount >> 1)) / this.m_tempCount);
		}
		else
		{
			this.m_finalHappiness = 0;
		}
	}

	public void Reset()
	{
		this.m_tempCount = 0u;
		this.m_tempHappiness = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempCount);
		s.WriteUInt32(this.m_tempHappiness);
		s.WriteUInt24(this.m_finalCount);
		s.WriteUInt8((uint)this.m_finalHappiness);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempCount = s.ReadUInt24();
		this.m_tempHappiness = s.ReadUInt32();
		this.m_finalCount = s.ReadUInt24();
		this.m_finalHappiness = (byte)s.ReadUInt8();
	}

	public uint m_tempCount;

	public uint m_tempHappiness;

	public uint m_finalCount;

	public byte m_finalHappiness;
}
