﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.UI;
using UnityEngine;

public class AudioManager : SimulationManagerBase<AudioManager, AudioProperties>, ISimulationManager, IAudibleManager
{
	public event AudioManager.RadioContentChangedHandler m_radioContentChanged
	{
		add
		{
			AudioManager.RadioContentChangedHandler radioContentChangedHandler = this.m_radioContentChanged;
			AudioManager.RadioContentChangedHandler radioContentChangedHandler2;
			do
			{
				radioContentChangedHandler2 = radioContentChangedHandler;
				radioContentChangedHandler = Interlocked.CompareExchange<AudioManager.RadioContentChangedHandler>(ref this.m_radioContentChanged, (AudioManager.RadioContentChangedHandler)Delegate.Combine(radioContentChangedHandler2, value), radioContentChangedHandler);
			}
			while (radioContentChangedHandler != radioContentChangedHandler2);
		}
		remove
		{
			AudioManager.RadioContentChangedHandler radioContentChangedHandler = this.m_radioContentChanged;
			AudioManager.RadioContentChangedHandler radioContentChangedHandler2;
			do
			{
				radioContentChangedHandler2 = radioContentChangedHandler;
				radioContentChangedHandler = Interlocked.CompareExchange<AudioManager.RadioContentChangedHandler>(ref this.m_radioContentChanged, (AudioManager.RadioContentChangedHandler)Delegate.Remove(radioContentChangedHandler2, value), radioContentChangedHandler);
			}
			while (radioContentChangedHandler != radioContentChangedHandler2);
		}
	}

	public bool isShowingCredits
	{
		get;
		set;
	}

	public bool isAfterDarkMenu
	{
		get;
		set;
	}

	public AudioGroup DefaultGroup
	{
		get
		{
			return this.m_defaultGroup;
		}
	}

	public AudioGroup AmbientGroup
	{
		get
		{
			return this.m_ambientGroup;
		}
	}

	public AudioGroup EffectGroup
	{
		get
		{
			return this.m_effectGroup;
		}
	}

	public float MasterVolume
	{
		get
		{
			return this.m_masterVolume;
		}
	}

	public bool MuteAll
	{
		get
		{
			return this.m_muteAll;
		}
		set
		{
			this.m_muteAll = value;
		}
	}

	public bool MuteRadio
	{
		get
		{
			return this.m_muteRadio;
		}
		set
		{
			this.m_muteRadio = value;
		}
	}

	public AudioManager.ListenerInfo CurrentListenerInfo
	{
		get
		{
			return this.m_listenerInfo;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_audioLocation = Path.Combine(DataLocation.get_gameContentPath(), "Audio");
		this.m_currentMusicFile = null;
		this.m_previousMusicFile = null;
		this.m_streamCrossFade = 0;
		this.m_listenerInfo = new AudioManager.ListenerInfo();
		this.m_defaultGroup = new AudioGroup(3, new SavedFloat(Settings.uiAudioVolume, Settings.gameSettingsFile, DefaultSettings.uiAudioVolume, true));
		this.m_ambientGroup = new AudioGroup(3, new SavedFloat(Settings.ambientAudioVolume, Settings.gameSettingsFile, DefaultSettings.ambientAudioVolume, true));
		this.m_effectGroup = new AudioGroup(3, new SavedFloat(Settings.effectAudioVolume, Settings.gameSettingsFile, DefaultSettings.effectAudioVolume, true));
		this.m_serviceProximity = new float[21];
		this.m_subServiceProximity = new float[27];
		this.m_eventBuffer = new FastList<AudioManager.SimulationEvent>();
		this.m_broadcastQueue = new FastList<RadioContentInfo>();
		this.m_mainAudioVolume = new SavedFloat(Settings.mainAudioVolume, Settings.gameSettingsFile, DefaultSettings.mainAudioVolume, true);
		this.m_musicAudioVolume = new SavedFloat(Settings.musicAudioVolume, Settings.gameSettingsFile, DefaultSettings.musicAudioVolume, true);
		this.m_broadcastAudioVolume = new SavedFloat(Settings.broadcastAudioVolume, Settings.gameSettingsFile, DefaultSettings.broadcastAudioVolume, true);
		this.m_tempBuffer1 = new byte[16384];
		this.m_tempBuffer2 = new byte[16384];
		this.m_streamBuffer = new float[65536];
		this.m_streamLock = new object();
		this.m_streamThread = new Thread(new ThreadStart(this.StreamThread));
		this.m_streamThread.Name = "Music Stream";
		this.m_streamThread.Priority = ThreadPriority.AboveNormal;
		this.m_streamThread.Start();
		if (!this.m_streamThread.IsAlive)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Audio stream thread failed to start!");
		}
		GameObject gameObject = new GameObject("Audio Listener");
		Object.DontDestroyOnLoad(gameObject);
		this.m_audioListener = gameObject.AddComponent<AudioListener>();
		this.m_audioListener.set_enabled(false);
		gameObject.AddComponent<MusicFilter>();
		this.m_radioChannels = new FastList<RadioChannelData>();
		this.m_radioChannels.Add(default(RadioChannelData));
		this.m_radioContents = new FastList<RadioContentData>();
		this.m_radioContents.Add(default(RadioContentData));
		this.m_tempRadioContentBuffer = new FastList<ushort>();
		this.m_radioContentTable = null;
	}

	private void OnDestroy()
	{
		while (!Monitor.TryEnter(this.m_streamLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_terminated = true;
			Monitor.PulseAll(this.m_streamLock);
		}
		finally
		{
			Monitor.Exit(this.m_streamLock);
		}
	}

	public static void RegisterAudibleManager(IAudibleManager manager)
	{
		if (manager != null)
		{
			AudioManager.m_audibles.Add(manager);
		}
	}

	public override void InitializeProperties(AudioProperties properties)
	{
		base.InitializeProperties(properties);
		GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
		if (gameObject != null)
		{
			this.m_cameraController = gameObject.GetComponent<CameraController>();
		}
		if (this.m_properties.m_reverbZone != null)
		{
			GameObject gameObject2 = Object.Instantiate<GameObject>(this.m_properties.m_reverbZone.get_gameObject());
			gameObject2.set_name("Reverb Zone");
			this.m_reverbZone = gameObject2.get_transform();
		}
		if (properties.m_musicFiles != null)
		{
			this.m_musicFiles = new string[properties.m_musicFiles.Length];
			for (int i = 0; i < properties.m_musicFiles.Length; i++)
			{
				this.m_musicFiles[i] = Path.Combine(this.m_audioLocation, properties.m_musicFiles[i]);
			}
		}
		if (properties.m_musicFilesNight != null)
		{
			this.m_musicFilesNight = new string[properties.m_musicFilesNight.Length];
			for (int j = 0; j < properties.m_musicFilesNight.Length; j++)
			{
				this.m_musicFilesNight[j] = Path.Combine(this.m_audioLocation, properties.m_musicFilesNight[j]);
			}
		}
	}

	public override void DestroyProperties(AudioProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_defaultGroup != null)
			{
				this.m_defaultGroup.Reset();
			}
			if (this.m_ambientGroup != null)
			{
				this.m_ambientGroup.Reset();
			}
			if (this.m_effectGroup != null)
			{
				this.m_effectGroup.Reset();
			}
			this.m_cameraController = null;
			this.m_musicFiles = null;
			this.m_musicFilesNight = null;
			while (!Monitor.TryEnter(this.m_broadcastQueue, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				for (int i = 0; i < this.m_broadcastQueue.m_size; i++)
				{
					this.m_broadcastQueue.m_buffer[i] = null;
				}
				this.m_broadcastQueue.Clear();
			}
			finally
			{
				Monitor.Exit(this.m_broadcastQueue);
			}
			if (this.m_reverbZone != null)
			{
				Object.Destroy(this.m_reverbZone.get_gameObject());
				this.m_reverbZone = null;
			}
		}
		base.DestroyProperties(properties);
	}

	public AudioManager.AudioPlayer ObtainPlayer(AudioInfo info)
	{
		if (this.m_sourcePool != null)
		{
			AudioManager.AudioPlayer sourcePool = this.m_sourcePool;
			this.m_sourcePool = sourcePool.m_nextPlayer;
			sourcePool.m_nextPlayer = null;
			sourcePool.m_info = info;
			sourcePool.m_source.get_gameObject().SetActive(true);
			sourcePool.m_source.set_clip(info.ObtainClip());
			return sourcePool;
		}
		if (this.m_sourcePoolRoot == null)
		{
			this.m_sourcePoolRoot = AudioManager.GetSourcePoolRoot();
		}
		GameObject gameObject = new GameObject("Audio Source");
		gameObject.get_transform().set_parent(this.m_sourcePoolRoot);
		AudioManager.AudioPlayer audioPlayer = new AudioManager.AudioPlayer();
		audioPlayer.m_source = gameObject.AddComponent<AudioSource>();
		audioPlayer.m_transform = gameObject.get_transform();
		audioPlayer.m_info = info;
		audioPlayer.m_source.set_playOnAwake(false);
		audioPlayer.m_source.set_clip(info.ObtainClip());
		return audioPlayer;
	}

	public AudioManager.AudioPlayer ObtainPlayer(AudioClip clip)
	{
		if (this.m_sourcePool != null)
		{
			AudioManager.AudioPlayer sourcePool = this.m_sourcePool;
			this.m_sourcePool = sourcePool.m_nextPlayer;
			sourcePool.m_nextPlayer = null;
			sourcePool.m_info = null;
			sourcePool.m_source.get_gameObject().SetActive(true);
			sourcePool.m_source.set_clip(clip);
			return sourcePool;
		}
		if (this.m_sourcePoolRoot == null)
		{
			this.m_sourcePoolRoot = AudioManager.GetSourcePoolRoot();
		}
		GameObject gameObject = new GameObject("Audio Source");
		gameObject.get_transform().set_parent(this.m_sourcePoolRoot);
		AudioManager.AudioPlayer audioPlayer = new AudioManager.AudioPlayer();
		audioPlayer.m_source = gameObject.AddComponent<AudioSource>();
		audioPlayer.m_transform = gameObject.get_transform();
		audioPlayer.m_info = null;
		audioPlayer.m_source.set_playOnAwake(false);
		audioPlayer.m_source.set_clip(clip);
		return audioPlayer;
	}

	public void ReleasePlayer(AudioManager.AudioPlayer player)
	{
		if (player.m_source != null)
		{
			player.m_source.Stop();
			player.m_source.set_clip(null);
			player.m_source.get_gameObject().SetActive(false);
		}
		if (player.m_info != null)
		{
			player.m_info.ReleaseClip();
			player.m_info = null;
		}
		player.m_nextPlayer = this.m_sourcePool;
		this.m_sourcePool = player;
	}

	public void PlaySound(AudioClip clip)
	{
		this.m_defaultGroup.AddPlayer(this.m_defaultPlayerID--, clip, 1f, false);
	}

	public void PlaySound(AudioClip clip, float volume)
	{
		this.PlaySound(clip, volume, false);
	}

	public void PlaySound(AudioClip clip, float volume, bool loop)
	{
		this.m_defaultGroup.AddPlayer(this.m_defaultPlayerID--, clip, volume, loop);
	}

	public void PlaySound(AudioInfo info)
	{
		this.m_defaultGroup.AddPlayer(this.m_defaultPlayerID--, info, 1f);
	}

	public void PlaySound(AudioInfo info, float volume)
	{
		this.m_defaultGroup.AddPlayer(this.m_defaultPlayerID--, info, volume);
	}

	private static Transform GetSourcePoolRoot()
	{
		GameObject gameObject = GameObject.Find("Audio Pool");
		if (gameObject == null)
		{
			gameObject = new GameObject("Audio Pool");
			Object.DontDestroyOnLoad(gameObject);
		}
		return gameObject.get_transform();
	}

	private void LateUpdate()
	{
		if (this.m_muteRadio)
		{
			this.m_muteRadioVolume = Mathf.Max(0f, this.m_muteRadioVolume - Time.get_deltaTime() * 2f);
		}
		else
		{
			this.m_muteRadioVolume = Mathf.Min(1f, this.m_muteRadioVolume + Time.get_deltaTime() * 2f);
		}
		float num = this.m_musicAudioVolume;
		this.m_masterVolume = this.m_mainAudioVolume;
		this.m_radioVolume = num * this.m_muteRadioVolume;
		this.m_musicVolume = this.m_masterVolume * num * (1f - this.m_broadcastVolume);
		if (this.m_cameraController != null)
		{
			Vector3 vector;
			if (this.m_properties != null)
			{
				vector = this.m_properties.m_listenerOffset;
			}
			else
			{
				vector = Vector3.get_zero();
			}
			Transform transform = this.m_cameraController.get_transform();
			Transform transform2 = this.m_audioListener.get_transform();
			transform2.set_position(transform.TransformPoint(vector));
			transform2.set_rotation(transform.get_rotation());
			this.m_audioListener.set_enabled(false);
			this.m_audioListener.set_enabled(true);
		}
		else if (!this.m_audioListener.get_enabled())
		{
			this.m_audioListener.set_enabled(true);
		}
		if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			this.m_listenerInfo.m_position = this.m_audioListener.get_transform().get_position();
			this.m_listenerInfo.m_dopplerLevel = 0.5f / (float)Mathf.Max(1, Singleton<SimulationManager>.get_instance().FinalSimulationSpeed);
		}
		int size = this.m_eventBuffer.m_size;
		for (int i = 0; i < size; i++)
		{
			AudioManager.SimulationEvent simulationEvent = this.m_eventBuffer.m_buffer[i];
			if (simulationEvent.m_info.m_is3D)
			{
				if (Vector3.SqrMagnitude(this.m_listenerInfo.m_position - simulationEvent.m_position) < simulationEvent.m_maxDistance * simulationEvent.m_maxDistance)
				{
					simulationEvent.m_group.AddPlayer(this.m_listenerInfo, simulationEvent.m_playerID, simulationEvent.m_info, simulationEvent.m_position, simulationEvent.m_velocity, simulationEvent.m_maxDistance, simulationEvent.m_volume, simulationEvent.m_pitch);
				}
			}
			else
			{
				simulationEvent.m_group.AddPlayer(simulationEvent.m_playerID, simulationEvent.m_info, simulationEvent.m_volume);
			}
			this.m_eventBuffer.m_buffer[i] = default(AudioManager.SimulationEvent);
		}
		while (!Monitor.TryEnter(this.m_eventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int j = size; j < this.m_eventBuffer.m_size; j++)
			{
				this.m_eventBuffer.m_buffer[j - size] = this.m_eventBuffer.m_buffer[j];
				this.m_eventBuffer.m_buffer[j] = default(AudioManager.SimulationEvent);
			}
			this.m_eventBuffer.m_size -= size;
		}
		finally
		{
			Monitor.Exit(this.m_eventBuffer);
		}
		try
		{
			for (int k = 0; k < AudioManager.m_audibles.m_size; k++)
			{
				AudioManager.m_audibles.m_buffer[k].PlayAudio(this.m_listenerInfo);
			}
		}
		finally
		{
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		this.m_defaultGroup.UpdatePlayers(listenerInfo, this.m_masterVolume);
		float listenerHeight;
		if (this.m_properties != null && Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			listenerHeight = Mathf.Max(0f, listenerInfo.m_position.y - Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(listenerInfo.m_position, true, 0f));
		}
		else
		{
			listenerHeight = 0f;
		}
		this.UpdateAmbient(listenerInfo, listenerHeight);
		this.UpdateMusic(listenerInfo, listenerHeight);
		float masterVolume;
		if (Singleton<LoadingManager>.get_instance().m_currentlyLoading || Singleton<SimulationManager>.get_instance().SimulationPaused || this.m_muteAll)
		{
			masterVolume = 0f;
		}
		else
		{
			masterVolume = this.m_masterVolume;
		}
		this.m_effectGroup.UpdatePlayers(listenerInfo, masterVolume);
	}

	private void UpdateAmbient(AudioManager.ListenerInfo listenerInfo, float listenerHeight)
	{
		if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading && this.m_properties != null && this.m_properties.m_ambients != null && this.m_properties.m_ambients.Length != 0 && this.m_ambientGroup.m_totalVolume >= 0.01f)
		{
			float num = Mathf.Clamp01(listenerHeight * 0.001f) - 1f;
			num *= num;
			float num2 = Singleton<NaturalResourceManager>.get_instance().CalculateForestProximity(listenerInfo.m_position, 500f - num * 400f);
			float num3 = 0f;
			float num4 = Singleton<TerrainManager>.get_instance().CalculateWaterProximity(listenerInfo.m_position, 400f - num * 300f, out num3);
			Singleton<BuildingManager>.get_instance().CalculateServiceProximity(listenerInfo.m_position, 400f - num * 300f, this.m_serviceProximity, this.m_subServiceProximity);
			float volume = 1f - num;
			float volume2 = (num2 + this.m_subServiceProximity[6]) / (1f + listenerHeight * 0.002f);
			float volume3 = num4 * (1f - num3) / (1f + listenerHeight * 0.003f);
			float volume4 = num4 * num3 / (1f + listenerHeight * 0.003f);
			float volume5 = (this.m_subServiceProximity[5] + this.m_subServiceProximity[8] + this.m_subServiceProximity[9]) / (1f + listenerHeight * 0.004f);
			float volume6 = (this.m_serviceProximity[17] + this.m_serviceProximity[12]) / (1f + listenerHeight * 0.004f);
			float volume7 = (this.m_subServiceProximity[1] + this.m_subServiceProximity[25] + this.m_subServiceProximity[3]) / (1f + listenerHeight * 0.004f);
			float volume8 = (this.m_subServiceProximity[2] + this.m_subServiceProximity[26] + this.m_subServiceProximity[4] + this.m_serviceProximity[8]) / (1f + listenerHeight * 0.004f);
			float volume9 = (this.m_subServiceProximity[7] + this.m_subServiceProximity[24]) / (1f + listenerHeight * 0.004f);
			float volume10 = this.m_subServiceProximity[18] / (1f + listenerHeight * 0.004f);
			float volume11 = this.m_subServiceProximity[19] / (1f + listenerHeight * 0.004f);
			float volume12 = Singleton<WeatherManager>.get_instance().SampleRainIntensity(listenerInfo.m_position, false) / (1f + listenerHeight * 0.002f);
			AudioInfo[] array;
			if (Singleton<SimulationManager>.get_instance().m_isNightTime && this.m_properties.m_ambientsNight != null && this.m_properties.m_ambientsNight.Length != 0)
			{
				array = this.m_properties.m_ambientsNight;
			}
			else
			{
				array = this.m_properties.m_ambients;
			}
			this.m_ambientGroup.AddPlayer(0, array[0], volume);
			this.m_ambientGroup.AddPlayer(1, array[1], volume2);
			this.m_ambientGroup.AddPlayer(2, array[2], volume3);
			this.m_ambientGroup.AddPlayer(3, array[3], volume4);
			this.m_ambientGroup.AddPlayer(4, array[4], volume5);
			this.m_ambientGroup.AddPlayer(5, array[5], volume6);
			this.m_ambientGroup.AddPlayer(6, array[6], volume7);
			this.m_ambientGroup.AddPlayer(7, array[7], volume8);
			this.m_ambientGroup.AddPlayer(8, array[8], volume9);
			this.m_ambientGroup.AddPlayer(9, array[9], volume10);
			this.m_ambientGroup.AddPlayer(10, array[10], volume11);
			this.m_ambientGroup.AddPlayer(11, array[11], volume12);
		}
		this.m_ambientGroup.UpdatePlayers(listenerInfo, (!this.m_muteAll) ? this.m_masterVolume : 0f);
	}

	private void UpdateMusic(AudioManager.ListenerInfo listenerInfo, float listenerHeight)
	{
		RadioContentInfo radioContentInfo = this.m_broadcastContent;
		RadioContentInfo radioContentInfo2 = null;
		ushort num = 0;
		if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading)
		{
			if (radioContentInfo == null && this.m_broadcastQueue.m_size != 0)
			{
				while (!Monitor.TryEnter(this.m_broadcastQueue, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				try
				{
					if (this.m_broadcastQueue.m_size != 0)
					{
						radioContentInfo = this.m_broadcastQueue.m_buffer[0];
						for (int i = 1; i < this.m_broadcastQueue.m_size; i++)
						{
							this.m_broadcastQueue.m_buffer[i - 1] = this.m_broadcastQueue.m_buffer[i];
						}
						this.m_broadcastQueue.m_buffer[--this.m_broadcastQueue.m_size] = null;
					}
				}
				finally
				{
					Monitor.Exit(this.m_broadcastQueue);
				}
			}
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				num = this.m_activeRadioContent;
				if (num != 0)
				{
					radioContentInfo2 = this.m_radioContents.m_buffer[(int)num].Info;
				}
				if (radioContentInfo2 != null)
				{
					this.m_musicFile = null;
				}
				else if (this.m_musicFiles != null && this.m_musicFiles.Length != 0)
				{
					string[] array = this.m_musicFiles;
					if (Singleton<SimulationManager>.get_instance().m_isNightTime && this.m_musicFilesNight != null && this.m_musicFilesNight.Length != 0)
					{
						array = this.m_musicFilesNight;
					}
					int finalHappiness = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_finalHappiness;
					AudioManager.MusicType musicType;
					if (finalHappiness < 25)
					{
						musicType = AudioManager.MusicType.Worst;
					}
					else if (finalHappiness < 40)
					{
						musicType = AudioManager.MusicType.Bad;
					}
					else if (finalHappiness < 55)
					{
						musicType = AudioManager.MusicType.Normal;
					}
					else if (finalHappiness < 70)
					{
						musicType = AudioManager.MusicType.Good;
					}
					else if (finalHappiness < 85)
					{
						musicType = AudioManager.MusicType.VeryWell;
					}
					else if (finalHappiness < 95)
					{
						musicType = AudioManager.MusicType.NearWonder;
					}
					else
					{
						musicType = AudioManager.MusicType.AlmostWonder;
					}
					if (listenerHeight > 1400f)
					{
						musicType += 7;
					}
					int num2 = Mathf.Min((int)musicType, array.Length - 1);
					this.m_musicFile = array[num2];
					if (this.m_properties != null)
					{
						this.m_musicFileVolume = this.m_properties.m_musicVolume;
					}
				}
				this.m_musicFileIsRadio = true;
			}
			else if (this.m_musicFiles != null && this.m_musicFiles.Length != 0)
			{
				string[] musicFiles = this.m_musicFiles;
				AudioManager.MusicType musicType2 = AudioManager.MusicType.Worst;
				if (this.isAfterDarkMenu)
				{
					musicType2 = AudioManager.MusicType.Normal;
				}
				if (this.isShowingCredits)
				{
					musicType2 = AudioManager.MusicType.Bad;
				}
				int num3 = Mathf.Min((int)musicType2, musicFiles.Length - 1);
				this.m_musicFile = musicFiles[num3];
				if (this.m_properties != null)
				{
					this.m_musicFileVolume = this.m_properties.m_musicVolume;
				}
				this.m_musicFileIsRadio = false;
			}
		}
		if (radioContentInfo != this.m_broadcastContent && this.m_broadcastAudioVolume >= 0.01f)
		{
			this.SetBroadcastContent(radioContentInfo);
		}
		if (this.m_broadcastPlayer != null)
		{
			float num4 = this.m_broadcastAudioVolume;
			if (this.m_muteAll || !Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				this.m_broadcastVolume = Mathf.Max(0f, this.m_broadcastVolume - Time.get_deltaTime() * 2f);
			}
			else
			{
				this.m_broadcastVolume = Mathf.Min(1f, this.m_broadcastVolume + Time.get_deltaTime() * 5f);
			}
			float num5 = this.m_broadcastVolume * num4;
			if (this.m_broadcastContent != null)
			{
				num5 *= this.m_broadcastContent.m_volume;
			}
			if (this.m_broadcastPlayer.m_source.get_volume() != num5)
			{
				this.m_broadcastPlayer.m_source.set_volume(num5);
			}
			if (num4 < 0.01f)
			{
				this.ReleaseBroadcastPlayer();
			}
			else if (this.m_muteAll || this.m_masterVolume < 0.01f)
			{
				if (this.m_broadcastPlaying && !this.m_broadcastPaused)
				{
					this.m_broadcastPlayer.m_source.Pause();
					this.m_broadcastPaused = true;
				}
			}
			else if (!this.m_broadcastPlayer.m_source.get_isPlaying())
			{
				if (this.m_broadcastPaused)
				{
					this.m_broadcastPlayer.m_source.UnPause();
					this.m_broadcastPaused = false;
				}
				else if (this.m_broadcastPlaying)
				{
					this.ReleaseBroadcastPlayer();
				}
				else
				{
					this.m_broadcastPlayer.m_source.Play();
					this.m_broadcastPlaying = true;
				}
			}
		}
		else
		{
			this.m_broadcastVolume = Mathf.Max(0f, this.m_broadcastVolume - Time.get_deltaTime() * 2f);
		}
		if (radioContentInfo2 != this.m_currentRadioContent)
		{
			this.SetRadioContent(radioContentInfo2);
		}
		if (this.m_previousRadioPlayer != null)
		{
			if (this.m_previousRadioVolume < 0.01f)
			{
				this.ReleasePreviousRadioPlayer();
			}
			else
			{
				this.m_previousRadioVolume = Mathf.Max(0f, this.m_previousRadioVolume - Time.get_deltaTime() * 2f);
				float num6 = this.m_previousRadioVolume * this.m_radioVolume * (1f - this.m_broadcastVolume);
				if (this.m_previousRadioContent != null)
				{
					num6 *= this.m_previousRadioContent.m_volume;
				}
				if (this.m_previousRadioPlayer.m_source.get_volume() != num6)
				{
					this.m_previousRadioPlayer.m_source.set_volume(num6);
				}
			}
		}
		if (this.m_currentRadioPlayer != null)
		{
			if (this.m_muteAll)
			{
				this.m_currentRadioVolume = Mathf.Max(0f, this.m_currentRadioVolume - Time.get_deltaTime() * 2f);
			}
			else
			{
				this.m_currentRadioVolume = Mathf.Min(1f, this.m_currentRadioVolume + Time.get_deltaTime() * 5f);
			}
			float num7 = this.m_currentRadioVolume * this.m_radioVolume * (1f - this.m_broadcastVolume);
			if (this.m_currentRadioContent != null)
			{
				num7 *= this.m_currentRadioContent.m_volume;
			}
			if (this.m_currentRadioPlayer.m_source.get_volume() != num7)
			{
				this.m_currentRadioPlayer.m_source.set_volume(num7);
			}
			if (this.m_muteAll || this.m_masterVolume < 0.01f)
			{
				if (this.m_currentRadioPlaying && !this.m_currentRadioPaused)
				{
					this.m_currentRadioPlayer.m_source.Pause();
					this.m_currentRadioPaused = true;
				}
			}
			else if (!this.m_currentRadioPlayer.m_source.get_isPlaying())
			{
				if (this.m_currentRadioPaused)
				{
					this.m_currentRadioPlayer.m_source.UnPause();
					this.m_currentRadioPaused = false;
				}
				else if (this.m_currentRadioPlaying)
				{
					this.m_changeRadioContent = num;
				}
				else
				{
					this.m_currentRadioPlayer.m_source.Play();
					this.m_currentRadioPlaying = true;
				}
			}
		}
	}

	private void ReleasePreviousRadioPlayer()
	{
		this.m_previousRadioPlayer.m_source.Stop();
		AudioClip clip = this.m_previousRadioPlayer.m_source.get_clip();
		if (clip != null)
		{
			this.m_previousRadioPlayer.m_source.set_clip(null);
			Object.Destroy(clip);
		}
		this.ReleasePlayer(this.m_previousRadioPlayer);
		this.m_previousRadioPlayer = null;
		this.m_previousRadioContent = null;
		this.m_previousRadioVolume = 0f;
	}

	private void ReleaseBroadcastPlayer()
	{
		this.m_broadcastPlayer.m_source.Stop();
		AudioClip clip = this.m_broadcastPlayer.m_source.get_clip();
		if (clip != null)
		{
			this.m_broadcastPlayer.m_source.set_clip(null);
			Object.Destroy(clip);
		}
		this.ReleasePlayer(this.m_broadcastPlayer);
		this.m_broadcastPlayer = null;
		this.m_broadcastContent = null;
		this.m_broadcastVolume = 0f;
	}

	private void SetRadioContent(RadioContentInfo radioContent)
	{
		if (this.m_currentRadioPlayer != null)
		{
			if (this.m_previousRadioPlayer != null)
			{
				if (this.m_previousRadioContent == radioContent)
				{
					AudioManager.AudioPlayer previousRadioPlayer = this.m_previousRadioPlayer;
					float previousRadioVolume = this.m_previousRadioVolume;
					bool previousRadioPlaying = this.m_previousRadioPlaying;
					bool previousRadioPaused = this.m_previousRadioPaused;
					this.m_previousRadioPlayer = this.m_currentRadioPlayer;
					this.m_previousRadioContent = this.m_currentRadioContent;
					this.m_previousRadioVolume = this.m_currentRadioVolume;
					this.m_previousRadioPlaying = this.m_currentRadioPlaying;
					this.m_previousRadioPaused = this.m_currentRadioPaused;
					this.m_currentRadioPlayer = previousRadioPlayer;
					this.m_currentRadioContent = radioContent;
					this.m_currentRadioVolume = previousRadioVolume;
					this.m_currentRadioPlaying = previousRadioPlaying;
					this.m_currentRadioPaused = previousRadioPaused;
				}
				else
				{
					this.ReleasePreviousRadioPlayer();
					this.m_previousRadioPlayer = this.m_currentRadioPlayer;
					this.m_previousRadioContent = this.m_currentRadioContent;
					this.m_previousRadioVolume = this.m_currentRadioVolume;
					this.m_previousRadioPlaying = this.m_currentRadioPlaying;
					this.m_previousRadioPaused = this.m_currentRadioPaused;
					this.m_currentRadioPlayer = null;
					this.m_currentRadioContent = null;
					this.m_currentRadioVolume = 0f;
					this.m_currentRadioPlaying = false;
					this.m_currentRadioPaused = false;
				}
			}
			else
			{
				this.m_previousRadioPlayer = this.m_currentRadioPlayer;
				this.m_previousRadioContent = this.m_currentRadioContent;
				this.m_previousRadioVolume = this.m_currentRadioVolume;
				this.m_previousRadioPlaying = this.m_currentRadioPlaying;
				this.m_previousRadioPaused = this.m_currentRadioPaused;
				this.m_currentRadioPlayer = null;
				this.m_currentRadioContent = null;
				this.m_currentRadioVolume = 0f;
				this.m_currentRadioPlaying = false;
				this.m_currentRadioPaused = false;
			}
		}
		if (radioContent != this.m_currentRadioContent)
		{
			this.m_currentRadioContent = radioContent;
			if (radioContent != null)
			{
				base.StartCoroutine(this.LoadRadioContent(radioContent));
			}
		}
		if (this.m_radioContentChanged != null)
		{
			this.m_radioContentChanged();
		}
	}

	private void SetBroadcastContent(RadioContentInfo broadcastContent)
	{
		if (broadcastContent != this.m_broadcastContent)
		{
			this.m_broadcastContent = broadcastContent;
			if (broadcastContent != null)
			{
				base.StartCoroutine(this.LoadBroadcastContent(broadcastContent));
			}
		}
	}

	[DebuggerHidden]
	private IEnumerator LoadRadioContent(RadioContentInfo radioContent)
	{
		AudioManager.<LoadRadioContent>c__Iterator0 <LoadRadioContent>c__Iterator = new AudioManager.<LoadRadioContent>c__Iterator0();
		<LoadRadioContent>c__Iterator.radioContent = radioContent;
		<LoadRadioContent>c__Iterator.$this = this;
		return <LoadRadioContent>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator LoadBroadcastContent(RadioContentInfo broadcastContent)
	{
		AudioManager.<LoadBroadcastContent>c__Iterator1 <LoadBroadcastContent>c__Iterator = new AudioManager.<LoadBroadcastContent>c__Iterator1();
		<LoadBroadcastContent>c__Iterator.broadcastContent = broadcastContent;
		<LoadBroadcastContent>c__Iterator.$this = this;
		return <LoadBroadcastContent>c__Iterator;
	}

	public void AddEvent(AudioGroup group, AudioInfo info, float volume)
	{
		this.AddEvent(group, info, Vector3.get_zero(), Vector3.get_zero(), 0f, volume, 1f, this.m_effectPlayerID--);
	}

	public void AddEvent(AudioGroup group, AudioInfo info, Vector3 position, Vector3 velocity, float maxDistance, float volume, float pitch)
	{
		this.AddEvent(group, info, position, velocity, maxDistance, volume, pitch, this.m_effectPlayerID--);
	}

	public void AddEvent(AudioGroup group, AudioInfo info, Vector3 position, Vector3 velocity, float maxDistance, float volume, float pitch, int playerID)
	{
		AudioManager.SimulationEvent item;
		item.m_group = group;
		item.m_info = info;
		item.m_position = position;
		item.m_velocity = velocity;
		item.m_maxDistance = maxDistance;
		item.m_volume = volume;
		item.m_pitch = pitch;
		item.m_playerID = playerID;
		while (!Monitor.TryEnter(this.m_eventBuffer, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_eventBuffer.Add(item);
		}
		finally
		{
			Monitor.Exit(this.m_eventBuffer);
		}
	}

	public bool CreateRadioChannel(out ushort radioChannelIndex, RadioChannelInfo info)
	{
		RadioChannelData radioChannelData = default(RadioChannelData);
		radioChannelData.m_flags = RadioChannelData.Flags.Created;
		radioChannelData.Info = info;
		for (int i = 1; i < this.m_radioChannels.m_size; i++)
		{
			if (this.m_radioChannels.m_buffer[i].m_flags == RadioChannelData.Flags.None)
			{
				radioChannelIndex = (ushort)i;
				this.m_radioChannels.m_buffer[i] = radioChannelData;
				this.m_radioChannelCount++;
				return true;
			}
		}
		if (this.m_radioChannels.m_size < 256)
		{
			radioChannelIndex = (ushort)this.m_radioChannels.m_size;
			this.m_radioChannels.Add(radioChannelData);
			this.m_radioChannelCount++;
			return true;
		}
		radioChannelIndex = 0;
		return false;
	}

	public void ReleaseRadioChannel(ushort radioChannelIndex)
	{
		if (radioChannelIndex != 0 && this.m_radioChannels.m_buffer[(int)radioChannelIndex].m_flags != RadioChannelData.Flags.None)
		{
			this.ReleaseRadioChannelImpl(radioChannelIndex);
			while (this.m_radioChannels.m_size > 1)
			{
				if (this.m_radioChannels.m_buffer[this.m_radioChannels.m_size - 1].m_flags != RadioChannelData.Flags.None)
				{
					break;
				}
				this.m_radioChannels.m_size--;
			}
		}
	}

	private void ReleaseRadioChannelImpl(ushort radioChannelIndex)
	{
		InstanceID id = default(InstanceID);
		id.RadioChannel = radioChannelIndex;
		Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
		RadioChannelData[] expr_2C_cp_0 = this.m_radioChannels.m_buffer;
		expr_2C_cp_0[(int)radioChannelIndex].m_flags = (expr_2C_cp_0[(int)radioChannelIndex].m_flags | RadioChannelData.Flags.Deleted);
		ushort currentContent = this.m_radioChannels.m_buffer[(int)radioChannelIndex].m_currentContent;
		if (currentContent != 0)
		{
			RadioContentData[] expr_67_cp_0 = this.m_radioContents.m_buffer;
			ushort expr_67_cp_1 = currentContent;
			expr_67_cp_0[(int)expr_67_cp_1].m_flags = (expr_67_cp_0[(int)expr_67_cp_1].m_flags & ~RadioContentData.Flags.Playing);
			this.m_radioContents.m_buffer[(int)currentContent].m_channel = 0;
		}
		ushort nextContent = this.m_radioChannels.m_buffer[(int)radioChannelIndex].m_nextContent;
		if (nextContent != 0)
		{
			RadioContentData[] expr_BA_cp_0 = this.m_radioContents.m_buffer;
			ushort expr_BA_cp_1 = nextContent;
			expr_BA_cp_0[(int)expr_BA_cp_1].m_flags = (expr_BA_cp_0[(int)expr_BA_cp_1].m_flags & ~RadioContentData.Flags.Queued);
			this.m_radioContents.m_buffer[(int)nextContent].m_channel = 0;
		}
		this.m_radioChannels.m_buffer[(int)radioChannelIndex] = default(RadioChannelData);
		this.m_radioChannelCount--;
	}

	public bool CreateRadioContent(out ushort radioContentIndex, RadioContentInfo info)
	{
		RadioContentData radioContentData = default(RadioContentData);
		radioContentData.m_flags = RadioContentData.Flags.Created;
		radioContentData.Info = info;
		for (int i = 1; i < this.m_radioContents.m_size; i++)
		{
			if (this.m_radioContents.m_buffer[i].m_flags == RadioContentData.Flags.None)
			{
				radioContentIndex = (ushort)i;
				this.m_radioContents.m_buffer[i] = radioContentData;
				this.m_radioContentCount++;
				return true;
			}
		}
		if (this.m_radioContents.m_size < 256)
		{
			radioContentIndex = (ushort)this.m_radioContents.m_size;
			this.m_radioContents.Add(radioContentData);
			this.m_radioContentCount++;
			return true;
		}
		radioContentIndex = 0;
		uint num = 0u;
		for (int j = 1; j < this.m_radioContents.m_size; j++)
		{
			if (this.m_radioContents.m_buffer[j].m_flags != RadioContentData.Flags.None && (uint)this.m_radioContents.m_buffer[j].m_cooldown > num)
			{
				radioContentIndex = (ushort)j;
				num = (uint)this.m_radioContents.m_buffer[j].m_cooldown;
			}
		}
		if (radioContentIndex != 0)
		{
			this.ReleaseRadioContentImpl(radioContentIndex);
			this.m_radioContents.m_buffer[(int)radioContentIndex] = radioContentData;
			this.m_radioContentCount++;
			return true;
		}
		radioContentIndex = 0;
		return false;
	}

	public void ReleaseRadioContent(ushort radioContentIndex)
	{
		if (radioContentIndex != 0 && this.m_radioContents.m_buffer[(int)radioContentIndex].m_flags != RadioContentData.Flags.None)
		{
			this.ReleaseRadioContentImpl(radioContentIndex);
			while (this.m_radioContents.m_size > 1)
			{
				if (this.m_radioContents.m_buffer[this.m_radioContents.m_size - 1].m_flags != RadioContentData.Flags.None)
				{
					break;
				}
				this.m_radioContents.m_size--;
			}
		}
	}

	private void ReleaseRadioContentImpl(ushort radioContentIndex)
	{
		InstanceID id = default(InstanceID);
		id.RadioContent = radioContentIndex;
		Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
		RadioContentData[] expr_2C_cp_0 = this.m_radioContents.m_buffer;
		expr_2C_cp_0[(int)radioContentIndex].m_flags = (expr_2C_cp_0[(int)radioContentIndex].m_flags | RadioContentData.Flags.Deleted);
		ushort channel = this.m_radioContents.m_buffer[(int)radioContentIndex].m_channel;
		if (channel != 0)
		{
			if (this.m_radioChannels.m_buffer[(int)channel].m_currentContent == radioContentIndex)
			{
				this.m_radioChannels.m_buffer[(int)channel].m_currentContent = 0;
			}
			if (this.m_radioChannels.m_buffer[(int)channel].m_nextContent == radioContentIndex)
			{
				this.m_radioChannels.m_buffer[(int)channel].m_nextContent = 0;
			}
		}
		this.m_radioContents.m_buffer[(int)radioContentIndex] = default(RadioContentData);
		this.m_radioContentCount--;
	}

	private void RefreshRadioContentTable()
	{
		int num = PrefabCollection<RadioChannelInfo>.PrefabCount();
		this.m_radioContentTable = new FastList<ushort>[5 * num];
		int num2 = PrefabCollection<RadioContentInfo>.PrefabCount();
		for (int i = 0; i < num2; i++)
		{
			RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)i);
			if (prefab != null && prefab.m_prefabDataIndex != -1 && prefab.m_radioChannels != null)
			{
				for (int j = 0; j < prefab.m_radioChannels.Length; j++)
				{
					int prefabDataIndex = prefab.m_radioChannels[j].m_prefabDataIndex;
					if (prefabDataIndex != -1)
					{
						int num3 = (int)(prefabDataIndex * 5 + prefab.m_contentType);
						FastList<ushort> fastList = this.m_radioContentTable[num3];
						if (fastList == null)
						{
							fastList = new FastList<ushort>();
							this.m_radioContentTable[num3] = fastList;
						}
						fastList.Add((ushort)prefab.m_prefabDataIndex);
					}
				}
			}
		}
	}

	public FastList<ushort> CollectRadioContentInfo(RadioContentInfo.ContentType type, RadioChannelInfo channel)
	{
		this.m_tempRadioContentBuffer.Clear();
		if (this.m_radioContentTable == null)
		{
			this.RefreshRadioContentTable();
		}
		int prefabDataIndex = channel.m_prefabDataIndex;
		if (prefabDataIndex != -1)
		{
			int num = (int)(prefabDataIndex * 5 + type);
			if (num < this.m_radioContentTable.Length)
			{
				FastList<ushort> fastList = this.m_radioContentTable[num];
				if (fastList != null)
				{
					for (int i = 0; i < fastList.m_size; i++)
					{
						ushort num2 = fastList.m_buffer[i];
						RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)num2);
						if (prefab != null && Singleton<UnlockManager>.get_instance().Unlocked(prefab.m_UnlockMilestone))
						{
							prefab.m_cooldown = 1000000;
							this.m_tempRadioContentBuffer.Add(num2);
						}
					}
				}
			}
		}
		for (int j = 0; j < this.m_radioContents.m_size; j++)
		{
			RadioContentData.Flags flags = this.m_radioContents.m_buffer[j].m_flags;
			if ((flags & RadioContentData.Flags.Created) != RadioContentData.Flags.None)
			{
				RadioContentInfo info = this.m_radioContents.m_buffer[j].Info;
				if (info != null)
				{
					info.m_cooldown = Mathf.Min(info.m_cooldown, (int)this.m_radioContents.m_buffer[j].m_cooldown);
				}
			}
		}
		return this.m_tempRadioContentBuffer;
	}

	public void ClearAll()
	{
		this.m_radioChannels.Clear();
		this.m_radioChannels.Add(default(RadioChannelData));
		this.m_radioChannelCount = 0;
		this.m_radioContents.Clear();
		this.m_radioContents.Add(default(RadioContentData));
		this.m_radioContentCount = 0;
		this.m_radioContentTable = null;
		this.m_activeRadioChannel = 0;
		this.m_activeRadioContent = 0;
		this.m_changeRadioContent = 0;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_changeRadioContent != 0)
		{
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete && this.m_activeRadioChannel != 0)
			{
				ushort currentContent = this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].m_currentContent;
				if (currentContent == this.m_changeRadioContent)
				{
					this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].ChangeContent(this.m_activeRadioChannel);
				}
			}
			this.m_changeRadioContent = 0;
		}
		if (this.m_activeRadioChannel != 0 && (this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].m_flags & (RadioChannelData.Flags.Created | RadioChannelData.Flags.Deleted | RadioChannelData.Flags.PlayDefault)) == RadioChannelData.Flags.Created)
		{
			this.m_activeRadioContent = this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].m_currentContent;
		}
		else
		{
			this.m_activeRadioContent = 0;
		}
		if (!this.m_muteAll && this.m_masterVolume >= 0.01f && Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			if (subStep <= 1)
			{
				int num = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
				int num2 = num;
				int num3 = num + 1 - 1;
				for (int i = num2; i <= num3; i++)
				{
					if (i < this.m_radioChannels.m_size)
					{
						RadioChannelData.Flags flags = this.m_radioChannels.m_buffer[i].m_flags;
						if ((flags & RadioChannelData.Flags.Created) != RadioChannelData.Flags.None)
						{
							this.m_radioChannels.m_buffer[i].SimulationTick((ushort)i);
						}
					}
				}
			}
			if (subStep <= 1)
			{
				int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 255u);
				int num5 = num4;
				int num6 = num4 + 1 - 1;
				for (int j = num5; j <= num6; j++)
				{
					if (j < this.m_radioContents.m_size)
					{
						RadioContentData.Flags flags2 = this.m_radioContents.m_buffer[j].m_flags;
						if ((flags2 & RadioContentData.Flags.Created) != RadioContentData.Flags.None)
						{
							this.m_radioContents.m_buffer[j].SimulationTick((ushort)j);
						}
					}
				}
			}
		}
		if (subStep <= 1)
		{
			int num7 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num8 = num7 * PrefabCollection<RadioContentInfo>.PrefabCount() >> 10;
			int num9 = ((num7 + 1) * PrefabCollection<RadioContentInfo>.PrefabCount() >> 10) - 1;
			for (int k = num8; k <= num9; k++)
			{
				RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)k);
				if (prefab != null)
				{
					prefab.CheckUnlocking();
				}
			}
		}
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<RadioContentInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				prefab.CheckUnlocking();
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<RadioContentInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	public void SetActiveRadioChannelInfo(RadioChannelInfo info)
	{
		for (int i = 1; i < this.m_radioChannels.m_size; i++)
		{
			if (this.m_radioChannels.m_buffer[i].m_flags != RadioChannelData.Flags.None)
			{
				RadioChannelInfo info2 = this.m_radioChannels.m_buffer[i].Info;
				if (info == info2)
				{
					this.SetActiveRadioChannel((ushort)i);
					return;
				}
			}
		}
	}

	public void SetActiveRadioChannel(ushort channelIndex)
	{
		if (this.m_activeRadioChannel != channelIndex)
		{
			if (this.m_activeRadioChannel != 0)
			{
				RadioChannelData[] expr_2D_cp_0 = this.m_radioChannels.m_buffer;
				ushort expr_2D_cp_1 = this.m_activeRadioChannel;
				expr_2D_cp_0[(int)expr_2D_cp_1].m_flags = (expr_2D_cp_0[(int)expr_2D_cp_1].m_flags & ~RadioChannelData.Flags.Active);
				this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].ChangeContent(this.m_activeRadioChannel);
			}
			this.m_activeRadioChannel = channelIndex;
			if (this.m_activeRadioChannel != 0)
			{
				RadioChannelData[] expr_84_cp_0 = this.m_radioChannels.m_buffer;
				ushort expr_84_cp_1 = this.m_activeRadioChannel;
				expr_84_cp_0[(int)expr_84_cp_1].m_flags = (expr_84_cp_0[(int)expr_84_cp_1].m_flags | RadioChannelData.Flags.Active);
			}
			if (this.m_properties != null && this.m_properties.m_changeRadioChannelSound != null)
			{
				AudioInfo variation = this.m_properties.m_changeRadioChannelSound.GetVariation(ref Singleton<SimulationManager>.get_instance().m_randomizer);
				this.AddEvent(this.m_defaultGroup, variation, 1f);
			}
		}
	}

	public void QueueBroadcast(RadioContentInfo info)
	{
		while (!Monitor.TryEnter(this.m_broadcastQueue, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if (this.m_broadcastQueue.m_size < 5)
			{
				for (int i = 0; i < this.m_broadcastQueue.m_size; i++)
				{
					if (this.m_broadcastQueue.m_buffer[i] == info)
					{
						return;
					}
				}
				this.m_broadcastQueue.Add(info);
			}
		}
		finally
		{
			Monitor.Exit(this.m_broadcastQueue);
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("AudioManager.UpdateData");
		base.UpdateData(mode);
		int num = PrefabCollection<RadioChannelInfo>.PrefabCount();
		this.m_radioChannelInfoCount = num;
		for (int i = 0; i < num; i++)
		{
			RadioChannelInfo prefab = PrefabCollection<RadioChannelInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				prefab.m_hasChannelData = false;
			}
		}
		ushort num2 = 0;
		ushort num3 = 0;
		for (int j = 1; j < this.m_radioChannels.m_size; j++)
		{
			if (this.m_radioChannels.m_buffer[j].m_flags != RadioChannelData.Flags.None)
			{
				RadioChannelInfo info = this.m_radioChannels.m_buffer[j].Info;
				if (info == null)
				{
					this.ReleaseRadioChannel((ushort)j);
				}
				else
				{
					info.m_hasChannelData = true;
					if (num3 == 0)
					{
						num3 = (ushort)j;
					}
					if (info.m_stateChain == null || info.m_stateChain.Length == 0)
					{
						num2 = (ushort)j;
					}
				}
			}
		}
		for (int k = 1; k < this.m_radioContents.m_size; k++)
		{
			if (this.m_radioContents.m_buffer[k].m_flags != RadioContentData.Flags.None && this.m_radioContents.m_buffer[k].Info == null)
			{
				this.ReleaseRadioContent((ushort)k);
			}
		}
		int num4 = PrefabCollection<RadioContentInfo>.PrefabCount();
		this.m_radioContentInfoCount = num4;
		for (int l = 0; l < num4; l++)
		{
			RadioContentInfo prefab2 = PrefabCollection<RadioContentInfo>.GetPrefab((uint)l);
			if (prefab2 != null && prefab2.m_UnlockMilestone != null)
			{
				switch (mode)
				{
				case SimulationManager.UpdateMode.NewMap:
				case SimulationManager.UpdateMode.LoadMap:
				case SimulationManager.UpdateMode.NewAsset:
				case SimulationManager.UpdateMode.LoadAsset:
					prefab2.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.NewGameFromMap:
				case SimulationManager.UpdateMode.NewScenarioFromMap:
				case SimulationManager.UpdateMode.UpdateScenarioFromMap:
					prefab2.m_UnlockMilestone.Reset(false);
					break;
				case SimulationManager.UpdateMode.LoadGame:
				case SimulationManager.UpdateMode.NewScenarioFromGame:
				case SimulationManager.UpdateMode.LoadScenario:
				case SimulationManager.UpdateMode.NewGameFromScenario:
				case SimulationManager.UpdateMode.UpdateScenarioFromGame:
					prefab2.m_UnlockMilestone.Reset(true);
					break;
				}
			}
		}
		if (mode != SimulationManager.UpdateMode.NewGameFromMap && mode != SimulationManager.UpdateMode.LoadGame && mode != SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.m_activeRadioChannel = 0;
			num3 = 0;
		}
		else
		{
			for (int m = 0; m < num; m++)
			{
				RadioChannelInfo prefab3 = PrefabCollection<RadioChannelInfo>.GetPrefab((uint)m);
				ushort num5;
				if (prefab3 != null && !prefab3.m_hasChannelData && this.CreateRadioChannel(out num5, prefab3))
				{
					if (num3 == 0)
					{
						num3 = num5;
					}
					if (prefab3.m_stateChain == null || prefab3.m_stateChain.Length == 0)
					{
						num2 = num5;
					}
				}
			}
		}
		if (this.m_activeRadioChannel == 0)
		{
			if (num2 != 0)
			{
				this.m_activeRadioChannel = num2;
			}
			else
			{
				this.m_activeRadioChannel = num3;
			}
			if (this.m_activeRadioChannel != 0)
			{
				RadioChannelData[] expr_2ED_cp_0 = this.m_radioChannels.m_buffer;
				ushort expr_2ED_cp_1 = this.m_activeRadioChannel;
				expr_2ED_cp_0[(int)expr_2ED_cp_1].m_flags = (expr_2ED_cp_0[(int)expr_2ED_cp_1].m_flags | RadioChannelData.Flags.Active);
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new AudioManager.Data());
	}

	private void FillStreamBuffer()
	{
		string musicFile = this.m_musicFile;
		string text = musicFile;
		float musicFileVolume = this.m_musicFileVolume;
		bool musicFileIsRadio = this.m_musicFileIsRadio;
		int num = 1;
		if (this.m_muteAll)
		{
			text = null;
			num = 4;
		}
		if (text != this.m_currentMusicFile)
		{
			if (text == this.m_previousMusicFile)
			{
				this.m_previousMusicFile = this.m_currentMusicFile;
				this.m_currentMusicFile = text;
				Stream currentMusicStream = this.m_currentMusicStream;
				this.m_currentMusicStream = this.m_previousMusicStream;
				this.m_previousMusicStream = currentMusicStream;
				float currentMusicVolume = this.m_currentMusicVolume;
				this.m_currentMusicVolume = this.m_previousMusicVolume;
				this.m_previousMusicVolume = currentMusicVolume;
				bool currentMusicIsRadio = this.m_currentMusicIsRadio;
				this.m_currentMusicIsRadio = this.m_previousMusicIsRadio;
				this.m_previousMusicIsRadio = currentMusicIsRadio;
				this.m_streamCrossFade = 65536 - this.m_streamCrossFade;
			}
			else if (this.m_previousMusicStream == null)
			{
				this.m_previousMusicFile = this.m_currentMusicFile;
				this.m_previousMusicStream = this.m_currentMusicStream;
				this.m_previousMusicVolume = this.m_currentMusicVolume;
				this.m_previousMusicIsRadio = this.m_currentMusicIsRadio;
				this.m_currentMusicFile = text;
				if (text == null || text == string.Empty)
				{
					this.m_currentMusicStream = null;
				}
				else
				{
					try
					{
						this.m_currentMusicStream = new FileStream(text, FileMode.Open, FileAccess.Read);
						this.m_currentMusicVolume = musicFileVolume;
						this.m_currentMusicIsRadio = musicFileIsRadio;
						if (this.m_previousMusicStream != null)
						{
							long length = this.m_previousMusicStream.Length;
							long length2 = this.m_currentMusicStream.Length;
							if (Mathf.Abs((float)(length - length2)) < 1024f)
							{
								long position = this.m_previousMusicStream.Position;
								if (position < length2)
								{
									this.m_currentMusicStream.Position = position;
								}
							}
						}
					}
					catch (Exception ex)
					{
						this.m_currentMusicStream = null;
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Failed to open audio stream: " + ex.Message + "\n" + ex.StackTrace);
					}
				}
				this.m_streamCrossFade = 65536 - this.m_streamCrossFade;
			}
		}
		else
		{
			this.m_currentMusicVolume = musicFileVolume;
			this.m_currentMusicIsRadio = musicFileIsRadio;
		}
		while ((this.m_bufferReadPos - this.m_bufferWritePos & 65535) >= 8192)
		{
			int num2 = this.m_bufferWritePos;
			this.FillTempBuffer(this.m_currentMusicStream, this.m_tempBuffer1);
			if (this.m_streamCrossFade == 65536)
			{
				int num3 = 0;
				float num4 = 3.05175781E-05f * this.m_currentMusicVolume;
				if (this.m_currentMusicIsRadio)
				{
					num4 *= this.m_muteRadioVolume;
				}
				for (int i = 0; i < 8192; i++)
				{
					short num5 = (short)this.m_tempBuffer1[num3++];
					num5 |= (short)(this.m_tempBuffer1[num3++] << 8);
					this.m_streamBuffer[num2++] = (float)num5 * num4;
					num2 &= 65535;
				}
			}
			else
			{
				this.FillTempBuffer(this.m_previousMusicStream, this.m_tempBuffer2);
				int num6 = 0;
				int num7 = 0;
				int num8 = 2;
				float num9 = (float)this.m_streamCrossFade * 4.656613E-10f * this.m_currentMusicVolume;
				float num10 = (float)(65536 - this.m_streamCrossFade) * 4.656613E-10f * this.m_previousMusicVolume;
				if (this.m_currentMusicIsRadio)
				{
					num9 *= this.m_muteRadioVolume;
				}
				if (this.m_previousMusicIsRadio)
				{
					num10 *= this.m_muteRadioVolume;
				}
				for (int j = 0; j < 8192; j += num8)
				{
					for (int k = 0; k < num8; k++)
					{
						short num11 = (short)this.m_tempBuffer1[num6++];
						num11 |= (short)(this.m_tempBuffer1[num6++] << 8);
						short num12 = (short)this.m_tempBuffer2[num7++];
						num12 |= (short)(this.m_tempBuffer2[num7++] << 8);
						this.m_streamBuffer[num2++] = (float)num11 * num9 + (float)num12 * num10;
						num2 &= 65535;
					}
					int num13 = this.m_streamCrossFade + num;
					if (num13 > 65536)
					{
						this.m_streamCrossFade = 65536;
					}
					else
					{
						this.m_streamCrossFade = num13;
					}
				}
			}
			this.m_bufferWritePos = num2;
		}
		if (this.m_streamCrossFade == 65536 && this.m_previousMusicStream != null && (this.m_currentMusicStream != null || musicFile == null))
		{
			this.m_previousMusicStream.Dispose();
			this.m_previousMusicStream = null;
			this.m_previousMusicFile = null;
		}
	}

	private void FillTempBuffer(Stream stream, byte[] buffer)
	{
		int num = buffer.Length;
		int i = 0;
		do
		{
			int num2 = num - i;
			if (stream != null)
			{
				long num3 = stream.Length - stream.Position;
				if ((long)num2 >= num3)
				{
					num2 = stream.Read(buffer, i, (int)num3);
					stream.Position = 0L;
				}
				else
				{
					num2 = stream.Read(buffer, i, num2);
				}
			}
			else
			{
				num2 = 0;
			}
			i += num2;
			if (num2 == 0)
			{
				while (i < num)
				{
					buffer[i++] = 0;
				}
			}
		}
		while (i < num);
	}

	private void StreamThread()
	{
		while (true)
		{
			while (!Monitor.TryEnter(this.m_streamLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				while (!this.m_needMoreData && !this.m_terminated)
				{
					Monitor.Wait(this.m_streamLock);
				}
				if (this.m_terminated)
				{
					if (this.m_currentMusicStream != null)
					{
						this.m_currentMusicStream.Dispose();
						this.m_currentMusicStream = null;
					}
					if (this.m_previousMusicStream != null)
					{
						this.m_previousMusicStream.Dispose();
						this.m_previousMusicStream = null;
					}
					break;
				}
				this.m_needMoreData = false;
			}
			finally
			{
				Monitor.Exit(this.m_streamLock);
			}
			try
			{
				this.FillStreamBuffer();
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Audio stream error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	public void ReadStreamBuffer(float[] target, int channels)
	{
		float musicVolume = this.m_musicVolume;
		int num = this.m_bufferReadPos;
		int num2 = target.Length;
		int num3 = this.m_bufferWritePos - this.m_bufferReadPos & 65535;
		if (num3 == 0)
		{
			num3 = 65534;
		}
		else
		{
			num3 = Mathf.Max(0, num3 - 2);
		}
		if (num3 * channels < num2 * 2)
		{
			num2 = num3 * channels / 2;
		}
		if (channels == 1)
		{
			for (int i = 0; i < num2; i++)
			{
				float num4 = this.m_streamBuffer[num++];
				float num5 = this.m_streamBuffer[num++];
				target[i] += (num4 + num5) * 0.5f * musicVolume;
				num &= 65535;
			}
		}
		else if (channels >= 2)
		{
			for (int j = 0; j < num2; j += channels)
			{
				target[j] += this.m_streamBuffer[num++] * musicVolume;
				target[j + 1] += this.m_streamBuffer[num++] * musicVolume;
				num &= 65535;
			}
		}
		this.m_bufferReadPos = num;
		num3 = (this.m_bufferWritePos - this.m_bufferReadPos & 65535);
		if (num3 < 57344)
		{
			while (!Monitor.TryEnter(this.m_streamLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_needMoreData = true;
				Monitor.Pulse(this.m_streamLock);
			}
			finally
			{
				Monitor.Exit(this.m_streamLock);
			}
		}
	}

	public ushort GetActiveRadioChannel()
	{
		return this.m_activeRadioChannel;
	}

	public RadioChannelInfo GetActiveRadioChannelInfo()
	{
		return this.m_radioChannels.m_buffer[(int)this.m_activeRadioChannel].Info;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const int STREAM_BUFFER_SIZE = 65536;

	public const int STREAM_BUFFER_LIMIT = 8192;

	public const int BYTES_PER_CHANNEL = 2;

	public const int MAX_RADIO_CHANNEL_COUNT = 256;

	public const int MAX_RADIO_CONTENT_COUNT = 256;

	public string m_currentMusicFile;

	public string m_previousMusicFile;

	public int m_streamCrossFade;

	public int m_radioChannelCount;

	public int m_radioContentCount;

	public int m_radioChannelInfoCount;

	public int m_radioContentInfoCount;

	[NonSerialized]
	public Transform m_sourcePoolRoot;

	[NonSerialized]
	public AudioManager.AudioPlayer m_sourcePool;

	[NonSerialized]
	public FastList<RadioChannelData> m_radioChannels;

	[NonSerialized]
	public FastList<RadioContentData> m_radioContents;

	private CameraController m_cameraController;

	private AudioListener m_audioListener;

	private AudioGroup m_defaultGroup;

	private AudioGroup m_ambientGroup;

	private AudioGroup m_effectGroup;

	private AudioManager.ListenerInfo m_listenerInfo;

	private float[] m_serviceProximity;

	private float[] m_subServiceProximity;

	private FastList<AudioManager.SimulationEvent> m_eventBuffer;

	private FastList<RadioContentInfo> m_broadcastQueue;

	private string m_audioLocation;

	private string[] m_musicFiles;

	private string[] m_musicFilesNight;

	private byte[] m_tempBuffer1;

	private byte[] m_tempBuffer2;

	private float[] m_streamBuffer;

	private Stream m_currentMusicStream;

	private Stream m_previousMusicStream;

	private float m_currentMusicVolume;

	private float m_previousMusicVolume;

	private bool m_currentMusicIsRadio;

	private bool m_previousMusicIsRadio;

	private object m_streamLock;

	private Thread m_streamThread;

	private RadioContentInfo m_currentRadioContent;

	private RadioContentInfo m_previousRadioContent;

	private RadioContentInfo m_broadcastContent;

	private ushort m_changeRadioContent;

	private AudioManager.AudioPlayer m_currentRadioPlayer;

	private AudioManager.AudioPlayer m_previousRadioPlayer;

	private AudioManager.AudioPlayer m_broadcastPlayer;

	private float m_currentRadioVolume;

	private float m_previousRadioVolume;

	private float m_broadcastVolume;

	private bool m_currentRadioPlaying;

	private bool m_currentRadioPaused;

	private bool m_previousRadioPlaying;

	private bool m_previousRadioPaused;

	private bool m_broadcastPlaying;

	private bool m_broadcastPaused;

	private ushort m_activeRadioChannel;

	private ushort m_activeRadioContent;

	private volatile int m_bufferWritePos;

	private volatile int m_bufferReadPos;

	private volatile bool m_terminated;

	private volatile bool m_needMoreData;

	private volatile string m_musicFile;

	private volatile float m_musicFileVolume;

	private volatile bool m_musicFileIsRadio;

	private volatile float m_musicVolume;

	private volatile float m_radioVolume;

	private volatile bool m_muteAll;

	private volatile bool m_muteRadio;

	private volatile float m_muteRadioVolume;

	private volatile float m_masterVolume;

	private SavedFloat m_mainAudioVolume;

	private SavedFloat m_musicAudioVolume;

	private SavedFloat m_broadcastAudioVolume;

	private FastList<ushort> m_tempRadioContentBuffer;

	private FastList<ushort>[] m_radioContentTable;

	private static FastList<IAudibleManager> m_audibles = new FastList<IAudibleManager>();

	private int m_defaultPlayerID = -1;

	private int m_effectPlayerID = -1;

	private Transform m_reverbZone;

	public enum AmbientType
	{
		None = -1,
		World,
		Forest,
		Sea,
		Stream,
		Industrial,
		Plaza,
		Suburban,
		City,
		Agricultural,
		Leisure,
		Tourist,
		Rain
	}

	public enum MusicType
	{
		None = -1,
		Worst,
		Bad,
		Normal,
		Good,
		VeryWell,
		NearWonder,
		AlmostWonder,
		Sky_Worst,
		Sky_Bad,
		Sky_Normal,
		Sky_Good,
		Sky_VeryWell,
		Sky_NearWonder,
		Sky_AlmostWonder
	}

	public class ListenerInfo
	{
		public Vector3 m_position;

		public float m_dopplerLevel;
	}

	public class AudioPlayer
	{
		public AudioSource m_source;

		public Transform m_transform;

		public AudioInfo m_info;

		public AudioManager.AudioPlayer m_nextPlayer;

		public Vector3 m_velocity;

		public float m_fadeSpeed;

		public int m_id;

		public bool m_is3d;

		public bool m_notReady;
	}

	public struct SimulationEvent
	{
		public AudioGroup m_group;

		public AudioInfo m_info;

		public Vector3 m_position;

		public Vector3 m_velocity;

		public float m_maxDistance;

		public float m_volume;

		public float m_pitch;

		public int m_playerID;
	}

	public delegate void RadioContentChangedHandler();

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "AudioManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			AudioManager instance2 = Singleton<AudioManager>.get_instance();
			RadioChannelData[] buffer = instance2.m_radioChannels.m_buffer;
			RadioContentData[] buffer2 = instance2.m_radioContents.m_buffer;
			int size = instance2.m_radioChannels.m_size;
			int size2 = instance2.m_radioContents.m_size;
			s.WriteUInt16((uint)(size - 1));
			s.WriteUInt16((uint)(size2 - 1));
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < size; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			try
			{
				PrefabCollection<RadioChannelInfo>.BeginSerialize(s);
				for (int j = 1; j < size; j++)
				{
					if (buffer[j].m_flags != RadioChannelData.Flags.None)
					{
						PrefabCollection<RadioChannelInfo>.Serialize((uint)buffer[j].m_infoIndex);
					}
				}
			}
			finally
			{
				PrefabCollection<RadioChannelInfo>.EndSerialize(s);
			}
			for (int k = 1; k < size; k++)
			{
				if (buffer[k].m_flags != RadioChannelData.Flags.None)
				{
					s.WriteUInt16((uint)buffer[k].m_currentContent);
					s.WriteUInt16((uint)buffer[k].m_nextContent);
					s.WriteUInt8((uint)buffer[k].m_stateChainIndex);
					s.WriteUInt8((uint)buffer[k].m_stateRepeatCount);
					s.WriteUInt16((uint)buffer[k].m_playPosition);
				}
			}
			EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginWrite(s);
			for (int l = 1; l < size2; l++)
			{
				uInt2.Write((uint)buffer2[l].m_flags);
			}
			uInt2.EndWrite();
			int num = PrefabCollection<RadioContentInfo>.PrefabCount();
			uint num2 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int m = 0; m < num; m++)
				{
					RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab((uint)m);
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num2 += 1u;
					}
				}
			}
			s.WriteUInt16(num2);
			try
			{
				PrefabCollection<RadioContentInfo>.BeginSerialize(s);
				for (int n = 1; n < size2; n++)
				{
					if (buffer2[n].m_flags != RadioContentData.Flags.None)
					{
						PrefabCollection<RadioContentInfo>.Serialize((uint)buffer2[n].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int num3 = 0; num3 < num; num3++)
					{
						RadioContentInfo prefab2 = PrefabCollection<RadioContentInfo>.GetPrefab((uint)num3);
						if (prefab2 != null && prefab2.m_UnlockMilestone != null && prefab2.m_prefabDataIndex != -1)
						{
							PrefabCollection<RadioContentInfo>.Serialize((uint)prefab2.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<RadioContentInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int num4 = 0; num4 < num; num4++)
				{
					RadioContentInfo prefab3 = PrefabCollection<RadioContentInfo>.GetPrefab((uint)num4);
					if (prefab3 != null && prefab3.m_UnlockMilestone != null && prefab3.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab3.m_UnlockMilestone.GetData());
					}
				}
			}
			for (int num5 = 1; num5 < size2; num5++)
			{
				if (buffer2[num5].m_flags != RadioContentData.Flags.None)
				{
					s.WriteUInt16((uint)buffer2[num5].m_cooldown);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "AudioManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "AudioManager");
			AudioManager instance = Singleton<AudioManager>.get_instance();
			while (!Monitor.TryEnter(instance.m_broadcastQueue, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				for (int i = 0; i < instance.m_broadcastQueue.m_size; i++)
				{
					instance.m_broadcastQueue.m_buffer[i] = null;
				}
				instance.m_broadcastQueue.Clear();
			}
			finally
			{
				Monitor.Exit(instance.m_broadcastQueue);
			}
			int num = (int)(s.ReadUInt16() + 1u);
			int num2 = (int)(s.ReadUInt16() + 1u);
			instance.m_radioChannels.Clear();
			instance.m_radioChannels.EnsureCapacity(num);
			instance.m_radioChannels.Add(default(RadioChannelData));
			instance.m_radioChannelCount = 0;
			instance.m_radioContents.Clear();
			instance.m_radioContents.EnsureCapacity(num2);
			instance.m_radioContents.Add(default(RadioContentData));
			instance.m_radioContentCount = 0;
			RadioChannelData[] buffer = instance.m_radioChannels.m_buffer;
			RadioContentData[] buffer2 = instance.m_radioContents.m_buffer;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
			for (int j = 1; j < num; j++)
			{
				RadioChannelData item = default(RadioChannelData);
				item.m_flags = (RadioChannelData.Flags)uInt.Read();
				instance.m_radioChannels.Add(item);
				if (item.m_flags != RadioChannelData.Flags.None)
				{
					instance.m_radioChannelCount++;
				}
			}
			uInt.EndRead();
			PrefabCollection<RadioChannelInfo>.BeginDeserialize(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != RadioChannelData.Flags.None)
				{
					buffer[k].m_infoIndex = (ushort)PrefabCollection<RadioChannelInfo>.Deserialize(true);
				}
			}
			PrefabCollection<RadioChannelInfo>.EndDeserialize(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != RadioChannelData.Flags.None)
				{
					buffer[l].m_currentContent = (ushort)s.ReadUInt16();
					buffer[l].m_nextContent = (ushort)s.ReadUInt16();
					buffer[l].m_stateChainIndex = (byte)s.ReadUInt8();
					buffer[l].m_stateRepeatCount = (byte)s.ReadUInt8();
					if (s.get_version() >= 281u)
					{
						buffer[l].m_playPosition = (ushort)s.ReadUInt16();
					}
				}
			}
			EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginRead(s);
			for (int m = 1; m < num2; m++)
			{
				RadioContentData item2 = default(RadioContentData);
				item2.m_flags = (RadioContentData.Flags)uInt2.Read();
				instance.m_radioContents.Add(item2);
				if (item2.m_flags != RadioContentData.Flags.None)
				{
					instance.m_radioContentCount++;
				}
			}
			uInt2.EndRead();
			uint num3 = s.ReadUInt16();
			this.m_milestoneInfoIndex = new uint[num3];
			this.m_UnlockMilestones = new MilestoneInfo.Data[num3];
			PrefabCollection<RadioContentInfo>.BeginDeserialize(s);
			for (int n = 1; n < num2; n++)
			{
				if (buffer2[n].m_flags != RadioContentData.Flags.None)
				{
					buffer2[n].m_infoIndex = (ushort)PrefabCollection<RadioContentInfo>.Deserialize(false);
				}
			}
			for (uint num4 = 0u; num4 < num3; num4 += 1u)
			{
				this.m_milestoneInfoIndex[(int)((UIntPtr)num4)] = PrefabCollection<RadioContentInfo>.Deserialize(false);
			}
			PrefabCollection<RadioContentInfo>.EndDeserialize(s);
			for (uint num5 = 0u; num5 < num3; num5 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num5)] = s.ReadObject<MilestoneInfo.Data>();
			}
			for (int num6 = 1; num6 < num2; num6++)
			{
				if (buffer2[num6].m_flags != RadioContentData.Flags.None)
				{
					buffer2[num6].m_cooldown = (ushort)s.ReadUInt16();
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "AudioManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "AudioManager");
			AudioManager instance = Singleton<AudioManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<RadioChannelInfo>.BindPrefabs();
			PrefabCollection<RadioContentInfo>.BindPrefabs();
			instance.m_radioContentTable = null;
			instance.m_activeRadioChannel = 0;
			instance.m_activeRadioContent = 0;
			instance.m_changeRadioContent = 0;
			RadioChannelData[] buffer = instance.m_radioChannels.m_buffer;
			RadioContentData[] buffer2 = instance.m_radioContents.m_buffer;
			int size = instance.m_radioChannels.m_size;
			int size2 = instance.m_radioContents.m_size;
			if (this.m_milestoneInfoIndex != null)
			{
				int num = this.m_milestoneInfoIndex.Length;
				for (int i = 0; i < num; i++)
				{
					RadioContentInfo prefab = PrefabCollection<RadioContentInfo>.GetPrefab(this.m_milestoneInfoIndex[i]);
					if (prefab != null)
					{
						MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[i]);
						}
					}
				}
			}
			for (int j = 1; j < size2; j++)
			{
				if (buffer2[j].m_flags != RadioContentData.Flags.None)
				{
					RadioContentInfo info = buffer2[j].Info;
					if (info != null)
					{
						buffer2[j].m_infoIndex = (ushort)info.m_prefabDataIndex;
						RadioContentData[] expr_12C_cp_0 = buffer2;
						int expr_12C_cp_1 = j;
						expr_12C_cp_0[expr_12C_cp_1].m_flags = (expr_12C_cp_0[expr_12C_cp_1].m_flags & ~(RadioContentData.Flags.Playing | RadioContentData.Flags.Queued));
					}
				}
			}
			for (int k = 1; k < size; k++)
			{
				if (buffer[k].m_flags != RadioChannelData.Flags.None)
				{
					RadioChannelInfo info2 = buffer[k].Info;
					if (info2 != null)
					{
						buffer[k].m_infoIndex = (ushort)info2.m_prefabDataIndex;
						if (info2.m_stateChain != null && info2.m_stateChain.Length != 0)
						{
							RadioChannelData[] expr_1B0_cp_0 = buffer;
							int expr_1B0_cp_1 = k;
							expr_1B0_cp_0[expr_1B0_cp_1].m_flags = (expr_1B0_cp_0[expr_1B0_cp_1].m_flags & ~RadioChannelData.Flags.PlayDefault);
						}
						if ((buffer[k].m_flags & RadioChannelData.Flags.Active) != RadioChannelData.Flags.None)
						{
							if (instance.m_activeRadioChannel == 0)
							{
								instance.m_activeRadioChannel = (ushort)k;
							}
							else
							{
								RadioChannelData[] expr_1F4_cp_0 = buffer;
								int expr_1F4_cp_1 = k;
								expr_1F4_cp_0[expr_1F4_cp_1].m_flags = (expr_1F4_cp_0[expr_1F4_cp_1].m_flags & ~RadioChannelData.Flags.Active);
							}
						}
					}
					ushort currentContent = buffer[k].m_currentContent;
					if (currentContent != 0)
					{
						if (buffer2[(int)currentContent].m_flags != RadioContentData.Flags.None)
						{
							buffer2[(int)currentContent].m_channel = (ushort)k;
							RadioContentData[] expr_242_cp_0 = buffer2;
							ushort expr_242_cp_1 = currentContent;
							expr_242_cp_0[(int)expr_242_cp_1].m_flags = (expr_242_cp_0[(int)expr_242_cp_1].m_flags | RadioContentData.Flags.Playing);
						}
						else
						{
							buffer[k].m_currentContent = 0;
						}
					}
					ushort nextContent = buffer[k].m_nextContent;
					if (nextContent != 0)
					{
						if (buffer2[(int)nextContent].m_flags != RadioContentData.Flags.None)
						{
							buffer2[(int)nextContent].m_channel = (ushort)k;
							RadioContentData[] expr_2A2_cp_0 = buffer2;
							ushort expr_2A2_cp_1 = nextContent;
							expr_2A2_cp_0[(int)expr_2A2_cp_1].m_flags = (expr_2A2_cp_0[(int)expr_2A2_cp_1].m_flags | RadioContentData.Flags.Queued);
						}
						else
						{
							buffer[k].m_nextContent = 0;
						}
					}
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "AudioManager");
		}

		private uint[] m_milestoneInfoIndex;

		private MilestoneInfo.Data[] m_UnlockMilestones;
	}
}
