﻿using System;
using System.Reflection;
using System.Threading;
using ColossalFramework.UI;

public class RoadEditorAddButton : UICustomControl
{
	public event RoadEditorAddButton.ObjectAddedHandler EventObjectAdded
	{
		add
		{
			RoadEditorAddButton.ObjectAddedHandler objectAddedHandler = this.EventObjectAdded;
			RoadEditorAddButton.ObjectAddedHandler objectAddedHandler2;
			do
			{
				objectAddedHandler2 = objectAddedHandler;
				objectAddedHandler = Interlocked.CompareExchange<RoadEditorAddButton.ObjectAddedHandler>(ref this.EventObjectAdded, (RoadEditorAddButton.ObjectAddedHandler)Delegate.Combine(objectAddedHandler2, value), objectAddedHandler);
			}
			while (objectAddedHandler != objectAddedHandler2);
		}
		remove
		{
			RoadEditorAddButton.ObjectAddedHandler objectAddedHandler = this.EventObjectAdded;
			RoadEditorAddButton.ObjectAddedHandler objectAddedHandler2;
			do
			{
				objectAddedHandler2 = objectAddedHandler;
				objectAddedHandler = Interlocked.CompareExchange<RoadEditorAddButton.ObjectAddedHandler>(ref this.EventObjectAdded, (RoadEditorAddButton.ObjectAddedHandler)Delegate.Remove(objectAddedHandler2, value), objectAddedHandler);
			}
			while (objectAddedHandler != objectAddedHandler2);
		}
	}

	private UIButton button
	{
		get
		{
			if (base.get_component() != null && base.get_component() is UIButton)
			{
				return base.get_component() as UIButton;
			}
			return null;
		}
	}

	private void OnEnable()
	{
		if (this.button != null)
		{
			this.button.add_eventClick(new MouseEventHandler(this.OnAddButtonClick));
		}
	}

	private void OnDisable()
	{
		if (this.button != null)
		{
			this.button.remove_eventClick(new MouseEventHandler(this.OnAddButtonClick));
		}
	}

	public object target
	{
		get
		{
			return this.m_TargetObject;
		}
		set
		{
			this.m_TargetObject = value;
		}
	}

	public FieldInfo field
	{
		get
		{
			return this.m_Field;
		}
		set
		{
			this.m_Field = value;
		}
	}

	private void OnAddButtonClick(UIComponent c, UIMouseEventParameter e)
	{
		object obj = null;
		if (this.m_TargetObject != null && this.m_Field != null)
		{
			obj = AssetEditorRoadUtils.AddArrayElement(this.m_TargetObject, this.m_Field);
		}
		if (this.EventObjectAdded != null)
		{
			this.EventObjectAdded(this, obj);
		}
	}

	private object m_TargetObject;

	private FieldInfo m_Field;

	public delegate void ObjectAddedHandler(RoadEditorAddButton button, object obj);
}
