﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class DisclaimerPanel : MenuPanel
{
	public static void ShowModal(string localeID, bool check, UIView.ModalPoppedReturnCallback callback)
	{
		DisclaimerPanel disclaimerPanel = UIView.get_library().ShowModal<DisclaimerPanel>("DisclaimerPanel", callback);
		disclaimerPanel.SetValues(localeID, check);
	}

	private void SetValues(string localeID, bool check)
	{
		base.Find<UICheckBox>("DontShow").set_isChecked(check);
		base.Find<UILabel>("Message").set_text(Locale.Get("DISCLAIMER_MESSAGE", localeID));
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			UIButton uIButton = base.Find<UIButton>("Ok");
			uIButton.Focus();
		}
	}

	protected virtual void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().Focus();
		}
	}

	protected virtual void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				p.Use();
				this.OnClosed();
			}
			else if (p.get_keycode() == 13)
			{
				p.Use();
				this.OnYes();
			}
		}
	}

	public virtual void OnNo()
	{
		UIView.get_library().Hide(base.GetType().Name, 0);
	}

	public virtual void OnYes()
	{
		if (base.Find<UICheckBox>("DontShow").get_isChecked())
		{
			UIView.get_library().Hide(base.GetType().Name, 2);
		}
		else
		{
			UIView.get_library().Hide(base.GetType().Name, 1);
		}
	}
}
