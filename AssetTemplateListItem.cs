﻿using System;
using ColossalFramework.UI;
using UnityEngine;

[ExecuteInEditMode]
public class AssetTemplateListItem : UICustomControl
{
	private void Awake()
	{
		UIButton uIButton = base.get_component() as UIButton;
		uIButton.add_eventButtonStateChanged(delegate(UIComponent c, UIButton.ButtonState s)
		{
			this.UpdateState(s);
		});
		this.m_Text.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.CutoffText));
	}

	private void CutoffText(UIComponent c, string s)
	{
		this.m_Text.remove_eventTextChanged(new PropertyChangedEventHandler<string>(this.CutoffText));
		string text = s;
		bool flag = false;
		using (UIFontRenderer uIFontRenderer = this.m_Text.ObtainRenderer())
		{
			float num = this.m_Text.GetUIView().PixelsToUnits();
			float[] characterWidths = uIFontRenderer.GetCharacterWidths(text);
			float num2 = 0f;
			for (int i = 0; i < characterWidths.Length; i++)
			{
				num2 += characterWidths[i] / num;
				if (num2 > this.m_Text.get_width() - (float)this.m_Text.get_padding().get_horizontal())
				{
					flag = true;
					text = text.Substring(0, i - 3) + "...";
					break;
				}
			}
		}
		this.m_Text.set_text(text);
		if (flag)
		{
			this.m_Text.set_tooltip(s);
			this.m_Text.set_isInteractive(true);
		}
		this.m_Text.add_eventTextChanged(new PropertyChangedEventHandler<string>(this.CutoffText));
	}

	private void Start()
	{
		UIButton uIButton = base.get_component() as UIButton;
		this.UpdateState(uIButton.get_state());
	}

	private void Update()
	{
		if (!Application.get_isPlaying())
		{
			UIButton uIButton = base.get_component() as UIButton;
			this.UpdateState(uIButton.get_state());
		}
	}

	private void UpdateState(UIButton.ButtonState s)
	{
		switch (s)
		{
		case 1:
		{
			this.m_Image.set_color(this.m_FocusedImageColor);
			this.m_Text.set_color(this.m_FocusedTextColor);
			UITextureAtlas.SpriteInfo spriteInfo = this.m_Atlas.get_Item(this.m_FocusedImage);
			if (spriteInfo != null)
			{
				this.m_Image.set_spriteName(this.m_FocusedImage);
			}
			spriteInfo = this.m_Atlas.get_Item(this.m_FocusedText);
			if (spriteInfo != null)
			{
				this.m_Text.set_backgroundSprite(this.m_FocusedText);
			}
			return;
		}
		case 2:
		{
			this.m_Image.set_color(this.m_HoveredImageColor);
			this.m_Text.set_color(this.m_PressedTextColor);
			UITextureAtlas.SpriteInfo spriteInfo2 = this.m_Atlas.get_Item(this.m_HoveredImage);
			if (spriteInfo2 != null)
			{
				this.m_Image.set_spriteName(this.m_HoveredImage);
			}
			spriteInfo2 = this.m_Atlas.get_Item(this.m_HoveredText);
			if (spriteInfo2 != null)
			{
				this.m_Text.set_backgroundSprite(this.m_HoveredText);
			}
			return;
		}
		case 3:
		{
			this.m_Image.set_color(this.m_PressedImageColor);
			this.m_Text.set_color(this.m_PressedTextColor);
			UITextureAtlas.SpriteInfo spriteInfo3 = this.m_Atlas.get_Item(this.m_PressedImage);
			if (spriteInfo3 != null)
			{
				this.m_Image.set_spriteName(this.m_PressedImage);
			}
			spriteInfo3 = this.m_Atlas.get_Item(this.m_PressedText);
			if (spriteInfo3 != null)
			{
				this.m_Text.set_backgroundSprite(this.m_PressedText);
			}
			return;
		}
		case 4:
		{
			this.m_Image.set_color(this.m_DisabledImageColor);
			this.m_Text.set_color(this.m_DisabledTextColor);
			UITextureAtlas.SpriteInfo spriteInfo4 = this.m_Atlas.get_Item(this.m_DisabledImage);
			if (spriteInfo4 != null)
			{
				this.m_Image.set_spriteName(this.m_DisabledImage);
			}
			spriteInfo4 = this.m_Atlas.get_Item(this.m_DisabledText);
			if (spriteInfo4 != null)
			{
				this.m_Text.set_backgroundSprite(this.m_DisabledText);
			}
			return;
		}
		}
		this.m_Image.set_color(this.m_NormalImageColor);
		this.m_Text.set_color(this.m_NormalTextColor);
		UITextureAtlas.SpriteInfo spriteInfo5 = this.m_Atlas.get_Item(this.m_NormalImage);
		if (spriteInfo5 != null)
		{
			this.m_Image.set_spriteName(this.m_NormalImage);
		}
		spriteInfo5 = this.m_Atlas.get_Item(this.m_NormalText);
		if (spriteInfo5 != null)
		{
			this.m_Text.set_backgroundSprite(this.m_NormalText);
		}
	}

	public UISlicedSprite m_Image;

	public UILabel m_Text;

	public UITextureAtlas m_Atlas;

	public Color32 m_NormalImageColor;

	public Color32 m_FocusedImageColor;

	public Color32 m_HoveredImageColor;

	public Color32 m_PressedImageColor;

	public Color32 m_DisabledImageColor;

	[UISprite("m_Atlas")]
	public string m_NormalImage;

	[UISprite("m_Atlas")]
	public string m_FocusedImage;

	[UISprite("m_Atlas")]
	public string m_HoveredImage;

	[UISprite("m_Atlas")]
	public string m_PressedImage;

	[UISprite("m_Atlas")]
	public string m_DisabledImage;

	public Color32 m_NormalTextColor;

	public Color32 m_FocusedTextColor;

	public Color32 m_HoveredTextColor;

	public Color32 m_PressedTextColor;

	public Color32 m_DisabledTextColor;

	[UISprite("m_Atlas")]
	public string m_NormalText;

	[UISprite("m_Atlas")]
	public string m_FocusedText;

	[UISprite("m_Atlas")]
	public string m_HoveredText;

	[UISprite("m_Atlas")]
	public string m_PressedText;

	[UISprite("m_Atlas")]
	public string m_DisabledText;
}
