﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public sealed class ResourcePanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	protected override void OnHideOptionBars()
	{
		if (this.m_OptionsBrushPanel != null)
		{
			this.m_OptionsBrushPanel.Hide();
		}
	}

	protected override void Start()
	{
		if (this.m_OptionsBar != null && this.m_OptionsBrushPanel == null)
		{
			this.m_OptionsBrushPanel = this.m_OptionsBar.Find<UIPanel>("BrushPanel");
		}
		base.Start();
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < ResourcePanel.kResources.Length; i++)
		{
			this.SpawnEntry(ResourcePanel.kResources[i].get_enumName(), i);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		ResourceTool resourceTool = ToolsModifierControl.SetTool<ResourceTool>();
		if (resourceTool != null)
		{
			if (this.m_OptionsBrushPanel != null)
			{
				this.m_OptionsBrushPanel.set_isVisible(true);
			}
			resourceTool.m_resource = ResourcePanel.kResources[comp.get_zOrder()].get_enumValue();
		}
	}

	private UIButton SpawnEntry(string name, int index)
	{
		string tooltip = TooltipHelper.Format(new string[]
		{
			"title",
			Locale.Get("RESOURCE_TITLE", name),
			"sprite",
			name,
			"text",
			Locale.Get("RESOURCE_DESC", name)
		});
		return base.CreateButton(name, tooltip, "Resource" + name, index, GeneratedPanel.tooltipBox);
	}

	private static readonly PositionData<NaturalResourceManager.Resource>[] kResources = Utils.GetOrderedEnumData<NaturalResourceManager.Resource>("Resource");

	private UIPanel m_OptionsBrushPanel;
}
