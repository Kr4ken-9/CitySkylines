﻿using System;
using ColossalFramework.IO;

public struct DistrictEducationData
{
	public void Add(ref DistrictEducationData data)
	{
		this.m_tempCount += data.m_tempCount;
		this.m_tempUnemployed += data.m_tempUnemployed;
		this.m_tempEligibleWorkers += data.m_tempEligibleWorkers;
		this.m_tempHomeless += data.m_tempHomeless;
	}

	public void Update()
	{
		this.m_finalCount = this.m_tempCount;
		this.m_finalUnemployed = this.m_tempUnemployed;
		this.m_finalEligibleWorkers = this.m_tempEligibleWorkers;
		this.m_finalHomeless = this.m_tempHomeless;
	}

	public void Reset()
	{
		this.m_tempCount = 0u;
		this.m_tempUnemployed = 0u;
		this.m_tempEligibleWorkers = 0u;
		this.m_tempHomeless = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempCount);
		s.WriteUInt24(this.m_tempUnemployed);
		s.WriteUInt24(this.m_tempHomeless);
		s.WriteUInt24(this.m_finalCount);
		s.WriteUInt24(this.m_finalUnemployed);
		s.WriteUInt24(this.m_finalHomeless);
		s.WriteUInt24(this.m_tempEligibleWorkers);
		s.WriteUInt24(this.m_finalEligibleWorkers);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempCount = s.ReadUInt24();
		this.m_tempUnemployed = s.ReadUInt24();
		this.m_tempHomeless = s.ReadUInt24();
		this.m_finalCount = s.ReadUInt24();
		this.m_finalUnemployed = s.ReadUInt24();
		this.m_finalHomeless = s.ReadUInt24();
		if (s.get_version() >= 140u)
		{
			this.m_tempEligibleWorkers = s.ReadUInt24();
			this.m_finalEligibleWorkers = s.ReadUInt24();
		}
		else
		{
			this.m_tempEligibleWorkers = 0u;
			this.m_finalEligibleWorkers = 0u;
		}
	}

	public uint m_tempCount;

	public uint m_tempUnemployed;

	public uint m_tempEligibleWorkers;

	public uint m_tempHomeless;

	public uint m_finalCount;

	public uint m_finalUnemployed;

	public uint m_finalEligibleWorkers;

	public uint m_finalHomeless;
}
