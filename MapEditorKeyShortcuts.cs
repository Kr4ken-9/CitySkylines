﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class MapEditorKeyShortcuts : KeyShortcuts
{
	private void Awake()
	{
		this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
		this.m_CloseInfoViews = UIView.Find<UIPanel>("InfoViewsPanel").Find<UIButton>("CloseButton");
	}

	private void SteamEscape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
	}

	protected void Update()
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseMenu))
			{
				UIView.get_library().ShowModal("PauseMenu");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseSimulation))
			{
				base.SimulationPause();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.NormalSimSpeed))
			{
				base.SimulationSpeed1();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.MediumSimSpeed))
			{
				base.SimulationSpeed2();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FastSimSpeed))
			{
				base.SimulationSpeed3();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Escape))
			{
				this.SteamEscape();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FreeCamera))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Bulldoze))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.InfoViews))
			{
				base.SelectUIButton("InfoPanel");
			}
		}
	}

	protected override void OnProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (this.m_ShortcutSimulationPause.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationPause();
			}
			else if (this.m_ShortcutSimulationSpeed1.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed1();
			}
			else if (this.m_ShortcutSimulationSpeed2.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed2();
			}
			else if (this.m_ShortcutSimulationSpeed3.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed3();
			}
			else if (eventType == 4 && keyCode == 27)
			{
				this.Escape();
			}
			else if (this.m_ShortcutTerrainTool.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Terrain");
			}
			else if (this.m_ShortcutImportHeightmap.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ImportHeightmap");
			}
			else if (this.m_ShortcutExportHeightmap.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ExportHeightmap");
			}
			else if (this.m_ShortcutWaterTool.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Water");
			}
			else if (this.m_ShortcutNaturalResourcesTool.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Resource");
			}
			else if (this.m_ShortcutForestTool.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Forest");
			}
			else if (this.m_ShortcutOutsideConnections.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Roads");
			}
			else if (this.m_ShortcutEnvironment.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Environment");
			}
			else if (this.m_ShortcutSettings.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("MapSettings");
			}
			else if (this.m_ShortcutBulldozer.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("Bulldozer");
			}
			else if (this.m_ShortcutBulldozerUnderground.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("BulldozerUndergroundToggle");
			}
			else if (this.m_ShortcutInGameShortcutFreeCameraMode.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FreeCamButton");
			}
			else if (this.m_ShortcutSnapToAngle.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAngles = !SnapSettingsPanel.snapToAngles;
			}
			else if (this.m_ShortcutSnapToLength.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToLength = !SnapSettingsPanel.snapToLength;
			}
			else if (this.m_ShortcutSnapToGrid.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToGrid = !SnapSettingsPanel.snapToGrid;
			}
			else if (this.m_ShortcutSnapToHelperLines.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToHelperLine = !SnapSettingsPanel.snapToHelperLine;
			}
			else if (this.m_ShortcutSnapToAll.IsPressed(eventType, keyCode, modifiers))
			{
				SnapSettingsPanel.snapToAll = !SnapSettingsPanel.snapToAll;
			}
			else if (this.m_ShortcutToggleSnappingMenu.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressSnappingMenuButton();
			}
			else if (this.m_ShortcutElevationStep.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressElevationStepButton();
			}
			else if (this.m_ShortcutOptionButton1.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(0);
			}
			else if (this.m_ShortcutOptionButton2.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(1);
			}
			else if (this.m_ShortcutOptionButton3.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(2);
			}
			else if (this.m_ShortcutOptionButton4.IsPressed(eventType, keyCode, modifiers))
			{
				base.PressToolModeButton(3);
			}
		}
	}

	private void Escape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseInfoViews != null && this.m_CloseInfoViews.get_isVisible())
		{
			this.m_CloseInfoViews.SimulateClick();
		}
		else if (ToolsModifierControl.GetCurrentTool<BulldozeTool>() != null)
		{
			base.SelectUIButton("Bulldozer");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else
		{
			UIView.get_library().ShowModal("PauseMenu");
		}
	}

	private SavedInputKey m_ShortcutSimulationPause = new SavedInputKey(Settings.mapEditorSimulationPause, Settings.inputSettingsFile, DefaultSettings.mapEditorSimulationPause, true);

	private SavedInputKey m_ShortcutSimulationSpeed1 = new SavedInputKey(Settings.mapEditorSimulationSpeed1, Settings.inputSettingsFile, DefaultSettings.mapEditorSimulationSpeed1, true);

	private SavedInputKey m_ShortcutSimulationSpeed2 = new SavedInputKey(Settings.mapEditorSimulationSpeed2, Settings.inputSettingsFile, DefaultSettings.mapEditorSimulationSpeed2, true);

	private SavedInputKey m_ShortcutSimulationSpeed3 = new SavedInputKey(Settings.mapEditorSimulationSpeed3, Settings.inputSettingsFile, DefaultSettings.mapEditorSimulationSpeed3, true);

	private SavedInputKey m_ShortcutInGameShortcutFreeCameraMode = new SavedInputKey(Settings.inGameShortcutFreeCameraMode, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFreeCameraMode, true);

	private SavedInputKey m_ShortcutTerrainTool = new SavedInputKey(Settings.mapEditorTerrainTool, Settings.inputSettingsFile, DefaultSettings.mapEditorTerrainTool, true);

	private SavedInputKey m_ShortcutImportHeightmap = new SavedInputKey(Settings.mapEditorImportHeightmap, Settings.inputSettingsFile, DefaultSettings.mapEditorImportHeightmap, true);

	private SavedInputKey m_ShortcutExportHeightmap = new SavedInputKey(Settings.mapEditorExportHeightmap, Settings.inputSettingsFile, DefaultSettings.mapEditorExportHeightmap, true);

	private SavedInputKey m_ShortcutWaterTool = new SavedInputKey(Settings.mapEditorWaterTool, Settings.inputSettingsFile, DefaultSettings.mapEditorWaterTool, true);

	private SavedInputKey m_ShortcutNaturalResourcesTool = new SavedInputKey(Settings.mapEditorNaturalResourcesTool, Settings.inputSettingsFile, DefaultSettings.mapEditorNaturalResourcesTool, true);

	private SavedInputKey m_ShortcutForestTool = new SavedInputKey(Settings.mapEditorForestTool, Settings.inputSettingsFile, DefaultSettings.mapEditorForestTool, true);

	private SavedInputKey m_ShortcutOutsideConnections = new SavedInputKey(Settings.mapEditorOutsideConnections, Settings.inputSettingsFile, DefaultSettings.mapEditorOutsideConnections, true);

	private SavedInputKey m_ShortcutEnvironment = new SavedInputKey(Settings.mapEditorEnvironment, Settings.inputSettingsFile, DefaultSettings.mapEditorEnvironment, true);

	private SavedInputKey m_ShortcutSettings = new SavedInputKey(Settings.mapEditorSettings, Settings.inputSettingsFile, DefaultSettings.mapEditorSettings, true);

	private SavedInputKey m_ShortcutBulldozer = new SavedInputKey(Settings.mapEditorShortcutBulldozer, Settings.inputSettingsFile, DefaultSettings.mapEditorShortcutBulldozer, true);

	private SavedInputKey m_ShortcutBulldozerUnderground = new SavedInputKey(Settings.mapEditorShortcutBulldozerUnderground, Settings.inputSettingsFile, DefaultSettings.mapEditorShortcutBulldozerUnderground, true);

	private SavedInputKey m_ShortcutToggleSnappingMenu = new SavedInputKey(Settings.sharedShortcutToggleSnappingMenu, Settings.inputSettingsFile, DefaultSettings.sharedShortcutToggleSnappingMenu, true);

	private SavedInputKey m_ShortcutElevationStep = new SavedInputKey(Settings.sharedShortcutElevationStep, Settings.inputSettingsFile, DefaultSettings.sharedShortcutElevationStep, true);

	private SavedInputKey m_ShortcutOptionButton1 = new SavedInputKey(Settings.sharedShortcutOptionsButton1, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton1, true);

	private SavedInputKey m_ShortcutOptionButton2 = new SavedInputKey(Settings.sharedShortcutOptionsButton2, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton2, true);

	private SavedInputKey m_ShortcutOptionButton3 = new SavedInputKey(Settings.sharedShortcutOptionsButton3, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton3, true);

	private SavedInputKey m_ShortcutOptionButton4 = new SavedInputKey(Settings.sharedShortcutOptionsButton4, Settings.inputSettingsFile, DefaultSettings.sharedShortcutOptionsButton4, true);

	private SavedInputKey m_ShortcutSnapToAngle = new SavedInputKey(Settings.inGameShortcutSnapToAngle, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAngle, true);

	private SavedInputKey m_ShortcutSnapToLength = new SavedInputKey(Settings.inGameShortcutSnapToLength, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToLength, true);

	private SavedInputKey m_ShortcutSnapToGrid = new SavedInputKey(Settings.inGameShortcutSnapToGrid, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToGrid, true);

	private SavedInputKey m_ShortcutSnapToHelperLines = new SavedInputKey(Settings.inGameShortcutSnapToHelperLines, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToHelperLines, true);

	private SavedInputKey m_ShortcutSnapToAll = new SavedInputKey(Settings.inGameShortcutSnapToAll, Settings.inputSettingsFile, DefaultSettings.inGameShortcutSnapToAll, true);

	private UIButton m_CloseToolbarButton;

	private UIButton m_CloseInfoViews;
}
