﻿using System;
using ColossalFramework.UI;

public sealed class PolicePanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.PoliceDepartment;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets(GeneratedScrollPanel.AssetFilter.Building);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
