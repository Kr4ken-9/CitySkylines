﻿using System;

public sealed class PropsSpecialBillboardsGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Prop;
		}
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsBillboardsRandomLogo", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsSpecialBillboardsRandomSmallBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsSpecialBillboardsRandomMediumBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsSpecialBillboardsRandomLargeBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsSpecialBillboards3DBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsSpecialBillboardsAnimatedBillboard", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "PropsBillboardsRandomLogo")
			{
				return 1;
			}
			if (name == "PropsSpecialBillboardsRandomSmallBillboard")
			{
				return 1;
			}
			if (name == "PropsSpecialBillboardsRandomMediumBillboard")
			{
				return 2;
			}
			if (name == "PropsSpecialBillboardsRandomLargeBillboard")
			{
				return 3;
			}
			if (name == "PropsSpecialBillboards3DBillboard")
			{
				return 4;
			}
			if (name == "PropsSpecialBillboardsAnimatedBillboard")
			{
				return 5;
			}
		}
		return 2147483647;
	}
}
