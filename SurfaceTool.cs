﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class SurfaceTool : ToolBase
{
	protected override void Awake()
	{
		base.Awake();
	}

	protected override void OnToolGUI(Event e)
	{
		if (this.m_toolController.IsInsideUI)
		{
			return;
		}
		if (e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.BeginDrawing());
			}
			else if (e.get_button() == 1)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.BeginErasing());
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.EndDrawing());
			}
			else if (e.get_button() == 1)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.EndErasing());
			}
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		this.m_validPosition = false;
		this.m_drawing = false;
		this.m_erasing = false;
		this.m_mouseRayValid = false;
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		bool drawing = this.m_drawing;
		bool erasing = this.m_erasing;
		bool validPosition = this.m_validPosition;
		if ((!drawing && !erasing && !validPosition) || !Cursor.get_visible() || this.m_toolController.IsInsideUI)
		{
			base.RenderOverlay(cameraInfo);
			return;
		}
		Color color;
		if (this.m_surface == TerrainModify.Surface.None || erasing)
		{
			color = Color.get_gray();
		}
		else
		{
			color = base.GetToolColor(false, false);
		}
		Vector3 startPosition = this.m_startPosition;
		Vector3 mousePosition = this.m_mousePosition;
		Vector3 startDirection = this.m_startDirection;
		Vector3 mouseDirection = this.m_mouseDirection;
		Vector3 vector = (!drawing && !erasing) ? mousePosition : startPosition;
		Vector3 vector2 = mousePosition;
		Vector3 vector3 = (!drawing && !erasing) ? mouseDirection : startDirection;
		Vector3 vector4;
		vector4..ctor(vector3.z, 0f, -vector3.x);
		float num = Mathf.Round(((vector2.x - vector.x) * vector3.x + (vector2.z - vector.z) * vector3.z) * 0.125f) * 8f;
		float num2 = Mathf.Round(((vector2.x - vector.x) * vector4.x + (vector2.z - vector.z) * vector4.z) * 0.125f) * 8f;
		float num3 = (num < 0f) ? -4f : 4f;
		float num4 = (num2 < 0f) ? -4f : 4f;
		Quad3 quad = default(Quad3);
		quad.a = vector - vector3 * num3 - vector4 * num4;
		quad.b = vector - vector3 * num3 + vector4 * (num2 + num4);
		quad.c = vector + vector3 * (num + num3) + vector4 * (num2 + num4);
		quad.d = vector + vector3 * (num + num3) - vector4 * num4;
		if (num3 != num4)
		{
			Vector3 b = quad.b;
			quad.b = quad.d;
			quad.d = b;
		}
		ToolManager expr_274_cp_0 = Singleton<ToolManager>.get_instance();
		expr_274_cp_0.m_drawCallData.m_overlayCalls = expr_274_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawQuad(cameraInfo, color, quad, -1f, 1025f, false, true);
		base.RenderOverlay(cameraInfo);
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		ToolBase.OverrideInfoMode = false;
	}

	[DebuggerHidden]
	private IEnumerator BeginDrawing()
	{
		SurfaceTool.<BeginDrawing>c__Iterator0 <BeginDrawing>c__Iterator = new SurfaceTool.<BeginDrawing>c__Iterator0();
		<BeginDrawing>c__Iterator.$this = this;
		return <BeginDrawing>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator BeginErasing()
	{
		SurfaceTool.<BeginErasing>c__Iterator1 <BeginErasing>c__Iterator = new SurfaceTool.<BeginErasing>c__Iterator1();
		<BeginErasing>c__Iterator.$this = this;
		return <BeginErasing>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator EndDrawing()
	{
		SurfaceTool.<EndDrawing>c__Iterator2 <EndDrawing>c__Iterator = new SurfaceTool.<EndDrawing>c__Iterator2();
		<EndDrawing>c__Iterator.$this = this;
		return <EndDrawing>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator EndErasing()
	{
		SurfaceTool.<EndErasing>c__Iterator3 <EndErasing>c__Iterator = new SurfaceTool.<EndErasing>c__Iterator3();
		<EndErasing>c__Iterator.$this = this;
		return <EndErasing>c__Iterator;
	}

	public override void SimulationStep()
	{
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
		{
			PrefabInfo editPrefabInfo = this.m_toolController.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				int num;
				int num2;
				float num3;
				editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
				int num4 = Mathf.FloorToInt(raycastOutput.m_hitPos.x / 8f + (float)num * 0.5f);
				int num5 = Mathf.FloorToInt(raycastOutput.m_hitPos.z / 8f + (float)num2 * 0.5f - num3 / 8f);
				if (num4 >= 0 && num4 < num && num5 >= 0 && num5 < num2)
				{
					this.m_mousePosition.x = ((float)num4 + 0.5f - (float)num * 0.5f) * 8f;
					this.m_mousePosition.z = ((float)num5 + 0.5f - (float)num2 * 0.5f) * 8f + num3;
					this.m_mouseDirection = Vector3.get_forward();
					this.m_validPosition = true;
					return;
				}
			}
			this.m_mousePosition = raycastOutput.m_hitPos;
			this.m_mouseDirection = Vector3.get_forward();
			this.m_validPosition = false;
		}
		else
		{
			this.m_validPosition = false;
		}
	}

	private void ApplyDrawing()
	{
		TerrainModify.Surface surface;
		if (this.m_drawing)
		{
			surface = this.m_surface;
		}
		else
		{
			if (!this.m_erasing)
			{
				return;
			}
			surface = TerrainModify.Surface.None;
		}
		PrefabInfo editPrefabInfo = this.m_toolController.m_editPrefabInfo;
		if (editPrefabInfo != null)
		{
			editPrefabInfo.EnsureCellSurface();
			int num;
			int num2;
			float num3;
			editPrefabInfo.GetDecorationArea(out num, out num2, out num3);
			int num4 = Mathf.FloorToInt(this.m_startPosition.x / 8f + (float)num * 0.5f);
			int num5 = Mathf.FloorToInt(this.m_startPosition.z / 8f + (float)num2 * 0.5f - num3 / 8f);
			int num6 = Mathf.FloorToInt(this.m_mousePosition.x / 8f + (float)num * 0.5f);
			int num7 = Mathf.FloorToInt(this.m_mousePosition.z / 8f + (float)num2 * 0.5f - num3 / 8f);
			int num8 = Mathf.Max(0, Mathf.Min(num4, num6));
			int num9 = Mathf.Max(0, Mathf.Min(num5, num7));
			int num10 = Mathf.Min(num - 1, Mathf.Max(num4, num6));
			int num11 = Mathf.Min(num2 - 1, Mathf.Max(num5, num7));
			for (int i = num9; i <= num11; i++)
			{
				for (int j = num8; j <= num10; j++)
				{
					int num12 = num - j - 1;
					int num13 = num2 - i - 1;
					editPrefabInfo.SetCellSurface(num13 * num + num12, surface);
				}
			}
			float minX = ((float)num8 - (float)num * 0.5f) * 8f;
			float minZ = ((float)num9 - (float)num2 * 0.5f) * 8f + num3;
			float maxX = ((float)num10 + 1f - (float)num * 0.5f) * 8f;
			float maxZ = ((float)num11 + 1f - (float)num2 * 0.5f) * 8f + num3;
			TerrainModify.UpdateArea(minX, minZ, maxX, maxZ, false, true, false);
		}
	}

	public TerrainModify.Surface m_surface;

	private Vector3 m_startPosition;

	private Vector3 m_startDirection;

	private Vector3 m_mousePosition;

	private Vector3 m_mouseDirection;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private bool m_drawing;

	private bool m_erasing;

	private bool m_validPosition;

	private bool m_mouseRayValid;
}
