﻿using System;
using UnityEngine;

public class LightBeams01_lightanim : MonoBehaviour
{
	private void Update()
	{
		base.get_transform().Rotate(Vector3.get_up(), Time.get_deltaTime() * (this.Speed * this.textureSpeed));
	}

	private float Speed = 360f;

	public float textureSpeed = 2f;
}
