﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class WaterCleanerAI : PlayerBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_waterConsumption != 0 || this.m_sewageAccumulation != 0)
		{
			throw new PrefabException(this.m_info, "Water cleaner cannot have water consumption or sewage accumulation");
		}
	}

	public override void GetNaturalResourceRadius(ushort buildingID, ref Building data, out NaturalResourceManager.Resource resource1, out Vector3 position1, out float radius1, out NaturalResourceManager.Resource resource2, out Vector3 position2, out float radius2)
	{
		if (this.m_maxWaterDistance != 0f)
		{
			resource1 = NaturalResourceManager.Resource.Water;
			position1 = data.CalculatePosition(this.m_waterLocationOffset);
			radius1 = this.m_maxWaterDistance;
		}
		else
		{
			resource1 = NaturalResourceManager.Resource.None;
			position1 = data.m_position;
			radius1 = 0f;
		}
		resource2 = NaturalResourceManager.Resource.None;
		position2 = data.m_position;
		radius2 = 0f;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Garbage)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				int noiseAccumulation = this.m_noiseAccumulation;
				return CommonBuildingAI.GetNoisePollutionColor((float)noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return num * this.m_cleaningRate / 100;
	}

	public override string GetConstructionInfo(int productionRate)
	{
		if (productionRate == 0)
		{
			return Locale.Get("TOOL_SHORELINE_RECOMMENDED");
		}
		return base.GetConstructionInfo(productionRate);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Garbage;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, 0, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		uint num2 = (uint)(((int)buildingID << 8) / 49152);
		uint num3 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - num2;
		Building.Frame lastFrameData = data.GetLastFrameData();
		data.SetFrameData(num3 - 512u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num3 - 256u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num3, lastFrameData);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		return base.GetCurrentRange(buildingID, ref data);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		Vector3 vector = Building.CalculatePosition(position, angle, this.m_waterLocationOffset);
		vector.y = 0f;
		if (Singleton<TerrainManager>.get_instance().GetClosestWaterPos(ref vector, this.m_maxWaterDistance * 0.5f))
		{
			productionRate = 100;
		}
		else
		{
			productionRate = 0;
		}
		return result;
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			if (this.m_cleaningRate > 0)
			{
				int rate = (finalProductionRate * this.m_cleaningRate + 99) / 100;
				int num = this.HandleWaterSource(buildingID, ref buildingData, rate, this.m_maxWaterDistance);
				finalProductionRate = Mathf.Min(finalProductionRate, (num * 100 + this.m_cleaningRate - 1) / this.m_cleaningRate);
			}
			base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
			int num2 = finalProductionRate * this.m_noiseAccumulation / 100;
			if (num2 != 0)
			{
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num2, buildingData.m_position, this.m_noiseRadius);
			}
		}
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		frameData.m_productionState = (byte)((int)frameData.m_productionState + finalProductionRate);
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override float ElectricityGridRadius()
	{
		if (this.m_electricityConsumption == 0)
		{
			return 0f;
		}
		return base.ElectricityGridRadius();
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.OnGround || this.m_info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			onlyCollapse = true;
			return true;
		}
		onlyCollapse = false;
		return false;
	}

	private int HandleWaterSource(ushort buildingID, ref Building data, int rate, float radius)
	{
		uint num = (uint)(rate >> 1);
		if (num == 0u)
		{
			return 0;
		}
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		WaterSimulation waterSimulation = instance.WaterSimulation;
		Vector3 vector = data.CalculatePosition(this.m_waterLocationOffset);
		if (data.m_waterSource != 0)
		{
			WaterSource sourceData = waterSimulation.LockWaterSource(data.m_waterSource);
			try
			{
				sourceData.m_inputPosition = vector;
				sourceData.m_outputPosition = vector;
				sourceData.m_inputRate = num + 3u >> 2;
				sourceData.m_outputRate = num + 3u >> 2;
				return (int)((int)sourceData.m_flow << 3);
			}
			finally
			{
				waterSimulation.UnlockWaterSource(data.m_waterSource, sourceData);
			}
		}
		vector.y = 0f;
		if (instance.GetClosestWaterPos(ref vector, radius))
		{
			waterSimulation.CreateWaterSource(out data.m_waterSource, new WaterSource
			{
				m_type = 3,
				m_inputPosition = vector,
				m_outputPosition = vector,
				m_inputRate = num + 3u >> 2,
				m_outputRate = num + 3u >> 2
			});
		}
		return 0;
	}

	public override void PlacementFailed()
	{
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<BuildingManager>.get_instance().m_buildNextToWater.Activate(properties.m_buildNextToWater);
		}
	}

	public override void PlacementSucceeded()
	{
		GenericGuide buildNextToWater = Singleton<BuildingManager>.get_instance().m_buildNextToWater;
		if (buildNextToWater != null)
		{
			buildNextToWater.Deactivate();
		}
	}

	public override void UpdateGuide(GuideController guideController)
	{
		base.UpdateGuide(guideController);
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AINFO_WATER_IO_CAPACITY", new object[]
			{
				this.m_cleaningRate * 16
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num = this.GetWaterRate(buildingID, ref data) * 16;
		return LocaleFormatter.FormatGeneric("AINFO_WATER_IO_CAPACITY", new object[]
		{
			num
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 10;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 10;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 10;

	[CustomizableProperty("Cleaning Rate", "Water")]
	public int m_cleaningRate = 1000;

	public Vector3 m_waterLocationOffset = Vector3.get_zero();

	[CustomizableProperty("Max Water Distance", "Water")]
	public float m_maxWaterDistance = 100f;

	[CustomizableProperty("Noise Accumulation", "Pollution")]
	public int m_noiseAccumulation = 50;

	[CustomizableProperty("Noise Radius", "Pollution")]
	public float m_noiseRadius = 100f;
}
