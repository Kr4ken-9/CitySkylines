﻿using System;
using ColossalFramework.IO;

public struct DistrictAgeData
{
	public void Add(ref DistrictAgeData data)
	{
		this.m_tempCount += data.m_tempCount;
	}

	public void Update()
	{
		this.m_finalCount = this.m_tempCount;
	}

	public void Reset()
	{
		this.m_tempCount = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt24(this.m_tempCount);
		s.WriteUInt24(this.m_finalCount);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempCount = s.ReadUInt24();
		this.m_finalCount = s.ReadUInt24();
	}

	public uint m_tempCount;

	public uint m_finalCount;
}
