﻿using System;

public sealed class ResourceGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("Resource");
		return true;
	}
}
