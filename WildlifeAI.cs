﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class WildlifeAI : AnimalAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_randomEffect != null)
		{
			this.m_randomEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("ANIMAL_STATUS_FLYING");
		}
		if ((data.m_flags & CitizenInstance.Flags.Floating) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		if (data.GetLastFrameData().m_velocity.get_sqrMagnitude() < 0.01f)
		{
			target = InstanceID.Empty;
			return Locale.Get("ANIMAL_STATUS_EATING");
		}
		target = InstanceID.Empty;
		return Locale.Get("ANIMAL_STATUS_WANDERING");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
		if (data.m_citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(data.m_citizen);
		}
		else if ((data.m_targetBuilding == 0 && (data.m_flags & CitizenInstance.Flags.Blown) == CitizenInstance.Flags.None) || (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
		}
	}

	public override void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
		if (sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)sourceBuilding].Info;
			data.Unspawn(instanceID);
			Vector3 vector;
			Vector3 vector2;
			info.m_buildingAI.CalculateSpawnPosition(sourceBuilding, ref instance.m_buildings.m_buffer[(int)sourceBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, out vector, out vector2);
			Quaternion rotation = Quaternion.get_identity();
			Vector3 vector3 = vector2 - vector;
			if (vector3.get_sqrMagnitude() > 0.01f)
			{
				rotation = Quaternion.LookRotation(vector3);
			}
			vector.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector);
			data.m_frame0.m_velocity = Vector3.get_zero();
			data.m_frame0.m_position = vector;
			data.m_frame0.m_rotation = rotation;
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos = vector2;
			if (this.IsFreePosition(vector))
			{
				data.Spawn(instanceID);
			}
		}
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
			}
		}
	}

	public override bool AddWind(ushort instanceID, ref CitizenInstance citizenData, Vector3 wind, InstanceManager.Group group)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None && ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None || wind.y > 19.62f))
		{
			CitizenInstance.Frame lastFrameData = citizenData.GetLastFrameData();
			lastFrameData.m_velocity = lastFrameData.m_velocity * 0.875f + wind * 0.125f;
			wind.y = 0f;
			if (wind.get_sqrMagnitude() > 0.01f)
			{
				lastFrameData.m_rotation = Quaternion.Lerp(lastFrameData.m_rotation, Quaternion.LookRotation(wind), 0.1f);
			}
			citizenData.SetLastFrameData(lastFrameData);
			if ((citizenData.m_flags & CitizenInstance.Flags.Blown) == CitizenInstance.Flags.None)
			{
				this.SetTarget(instanceID, ref citizenData, 0);
				citizenData.m_flags |= CitizenInstance.Flags.Blown;
				citizenData.m_waitCounter = 0;
			}
			InstanceID empty = InstanceID.Empty;
			empty.CitizenInstance = instanceID;
			Singleton<InstanceManager>.get_instance().SetGroup(empty, group);
			return true;
		}
		return false;
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
		if ((citizenData.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
			frameData.m_velocity.y = frameData.m_velocity.y - 2.4525f;
			frameData.m_velocity *= 0.99f;
			frameData.m_position += frameData.m_velocity * 0.5f;
			float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position);
			if (num > frameData.m_position.y)
			{
				frameData.m_velocity = Vector3.get_zero();
				frameData.m_position.y = num;
				citizenData.m_waitCounter = (byte)Mathf.Min((int)(citizenData.m_waitCounter + 1), 255);
				if (citizenData.m_waitCounter >= 100)
				{
					citizenData.m_flags &= ~CitizenInstance.Flags.Blown;
					citizenData.m_waitCounter = 0;
					return;
				}
			}
			else
			{
				citizenData.m_waitCounter = 0;
			}
		}
		else
		{
			float sqrMagnitude = frameData.m_velocity.get_sqrMagnitude();
			if (sqrMagnitude > 0.01f)
			{
				frameData.m_position += frameData.m_velocity * 0.5f;
			}
			Vector3 vector = citizenData.m_targetPos - frameData.m_position;
			float sqrMagnitude2 = vector.get_sqrMagnitude();
			float num2 = Mathf.Max(sqrMagnitude * 3f, 6f);
			if (sqrMagnitude2 < num2 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(20u) == 0 && citizenData.m_targetBuilding != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
				Vector3 vector2;
				Vector3 vector3;
				Vector2 vector4;
				CitizenInstance.Flags flags;
				info.m_buildingAI.CalculateUnspawnPosition(citizenData.m_targetBuilding, ref instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding], ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info, instanceID, out vector2, out vector3, out vector4, out flags);
				vector2.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(vector2);
				citizenData.m_targetPos = vector2;
				vector = citizenData.m_targetPos - frameData.m_position;
				sqrMagnitude2 = vector.get_sqrMagnitude();
			}
			float num3 = this.m_info.m_walkSpeed;
			float num4 = 2f;
			if (sqrMagnitude2 < 4f)
			{
				vector = Vector3.get_zero();
			}
			else
			{
				float num5 = Mathf.Sqrt(sqrMagnitude2);
				num3 = Mathf.Min(num3, Mathf.Sqrt(num5 * num4));
				float num6 = Mathf.Max(1f, 0.5f * sqrMagnitude / num4);
				float num7 = Mathf.Max(8f, 0.5f * sqrMagnitude / num4 + num3);
				Vector3 vector5 = frameData.m_position + vector * (num6 / num5);
				Vector3 position = frameData.m_position + vector * (num7 / num5);
				if (this.IsFreePosition(position))
				{
					Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
					vector = quaternion * vector;
					if (vector.z < Mathf.Abs(vector.x) * 1.7f)
					{
						if (vector.x >= 0f)
						{
							vector.x = Mathf.Max(1f, vector.x);
						}
						else
						{
							vector.x = Mathf.Min(-1f, vector.x);
						}
						vector.z = Mathf.Abs(vector.x) * 1.7f;
						num3 = Mathf.Min(1.5f, num5 * 0.1f);
					}
					vector = Vector3.ClampMagnitude(frameData.m_rotation * vector, num3);
				}
				else if (this.IsFreePosition(vector5))
				{
					citizenData.m_targetPos = vector5;
					vector = Vector3.get_zero();
				}
				else
				{
					if (!this.IsFreePosition(frameData.m_position))
					{
						citizenData.Unspawn(instanceID);
						return;
					}
					citizenData.m_targetPos = frameData.m_position;
					vector = Vector3.get_zero();
				}
			}
			Vector3 vector6 = vector - frameData.m_velocity;
			float magnitude = vector6.get_magnitude();
			vector6 *= num4 / Mathf.Max(magnitude, num4);
			frameData.m_velocity += vector6;
			citizenData.m_targetPos.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(frameData.m_position + frameData.m_velocity);
			frameData.m_velocity.y = citizenData.m_targetPos.y - frameData.m_position.y;
			float sqrMagnitude3 = frameData.m_velocity.get_sqrMagnitude();
			if (sqrMagnitude3 > 0.01f)
			{
				Vector3 vector7 = frameData.m_velocity;
				if (!lodPhysics)
				{
					Vector3 vector8 = Vector3.get_zero();
					float num8 = 0f;
					base.CheckCollisions(instanceID, ref citizenData, frameData.m_position, frameData.m_position + frameData.m_velocity, 0, ref vector8, ref num8);
					if (num8 > 0.01f)
					{
						vector8 *= 1f / num8;
						vector8 = Vector3.ClampMagnitude(vector8, Mathf.Sqrt(sqrMagnitude3) * 0.5f);
						frameData.m_velocity += vector8;
						vector7 += vector8 * 0.25f;
					}
				}
				frameData.m_position += frameData.m_velocity * 0.5f;
				if (vector7.get_sqrMagnitude() > 0.01f)
				{
					frameData.m_rotation = Quaternion.LookRotation(vector7);
				}
			}
		}
		if (this.m_randomEffect != null && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(40u) == 0)
		{
			InstanceID instance2 = default(InstanceID);
			instance2.CitizenInstance = instanceID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(frameData.m_position, Vector3.get_up(), 0f);
			float num9 = 3.75f;
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_randomEffect, instance2, spawnArea, frameData.m_velocity * num9, 0f, 1f, Singleton<CitizenManager>.get_instance().m_audioGroup);
		}
	}

	private bool IsFreePosition(Vector3 position)
	{
		TerrainManager.SurfaceCell surfaceCell = Singleton<TerrainManager>.get_instance().SampleDetailSurface(position);
		if (surfaceCell.m_clipped != 0 || surfaceCell.m_field != 0 || surfaceCell.m_gravel != 0 || surfaceCell.m_pavementA != 0 || surfaceCell.m_pavementB != 0)
		{
			return false;
		}
		if (Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(position)))
		{
			return false;
		}
		float num = position.x - this.m_info.m_radius;
		float num2 = position.z - this.m_info.m_radius;
		float num3 = position.x + this.m_info.m_radius;
		float num4 = position.z + this.m_info.m_radius;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = instance.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					BuildingInfo buildingInfo;
					int num11;
					int num12;
					instance.m_buildings.m_buffer[(int)num9].GetInfoWidthLength(out buildingInfo, out num11, out num12);
					if (buildingInfo.m_class.m_layer == ItemClass.Layer.Default)
					{
						Vector3 position2 = instance.m_buildings.m_buffer[(int)num9].m_position;
						float num13 = Mathf.Min(72f, (float)(num11 + num12) * 4f);
						float num14 = Mathf.Max(Mathf.Max(num - num13 - position2.x, num2 - num13 - position2.z), Mathf.Max(position2.x - num3 - num13, position2.z - num4 - num13));
						if (num14 < 0f)
						{
							Vector3 vector;
							Quaternion quaternion;
							instance.m_buildings.m_buffer[(int)num9].CalculateMeshPosition(out vector, out quaternion);
							Vector3 vector2 = quaternion * new Vector3(0f, 0f, buildingInfo.m_size.z * 0.5f + this.m_info.m_radius);
							Vector3 vector3 = quaternion * new Vector3(buildingInfo.m_size.x * 0.5f + this.m_info.m_radius, 0f, 0f);
							Quad2 quad;
							quad.a = VectorUtils.XZ(vector - vector2 - vector3);
							quad.b = VectorUtils.XZ(vector - vector2 + vector3);
							quad.c = VectorUtils.XZ(vector + vector2 + vector3);
							quad.d = VectorUtils.XZ(vector + vector2 - vector3);
							if (quad.Intersect(VectorUtils.XZ(position)))
							{
								return false;
							}
						}
					}
					num9 = instance.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return true;
	}

	public EffectInfo m_randomEffect;
}
