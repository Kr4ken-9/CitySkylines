﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public struct NetSegment
{
	public void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort segmentID, int layerMask)
	{
		if (this.m_flags == NetSegment.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		if (!cameraInfo.Intersect(this.m_bounds))
		{
			return;
		}
		if (this.m_problems != Notification.Problem.None && (layerMask & 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer) != 0)
		{
			Vector3 middlePosition = this.m_middlePosition;
			middlePosition.y += Mathf.Max(5f, info.m_maxHeight);
			Notification.RenderInstance(cameraInfo, this.m_problems, middlePosition, 1f);
		}
		if ((layerMask & (info.m_netLayers | info.m_propLayers)) == 0)
		{
			return;
		}
		RenderManager instance = Singleton<RenderManager>.get_instance();
		uint num;
		if (instance.RequireInstance((uint)(49152 + segmentID), 1u, out num))
		{
			this.RenderInstance(cameraInfo, segmentID, layerMask, info, ref instance.m_instances[(int)((UIntPtr)num)]);
			if (instance.m_instances[(int)((UIntPtr)num)].m_nameData != null)
			{
				NetManager instance2 = Singleton<NetManager>.get_instance();
				instance2.m_visibleRoadNameSegment = segmentID;
				instance2.m_nameInstanceBuffer.Add(num);
			}
		}
	}

	private void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort segmentID, int layerMask, NetInfo info, ref RenderManager.Instance data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if (data.m_dirty)
		{
			data.m_dirty = false;
			Vector3 position = instance.m_nodes.m_buffer[(int)this.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_position;
			data.m_position = (position + position2) * 0.5f;
			data.m_rotation = Quaternion.get_identity();
			data.m_dataColor0 = info.m_color;
			data.m_dataColor0.a = 0f;
			data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
			data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
			Vector4 colorLocation = RenderManager.GetColorLocation((uint)(49152 + segmentID));
			Vector4 vector = colorLocation;
			if (NetNode.BlendJunction(this.m_startNode))
			{
				colorLocation = RenderManager.GetColorLocation(86016u + (uint)this.m_startNode);
			}
			if (NetNode.BlendJunction(this.m_endNode))
			{
				vector = RenderManager.GetColorLocation(86016u + (uint)this.m_endNode);
			}
			data.m_dataVector3 = new Vector4(colorLocation.x, colorLocation.y, vector.x, vector.y);
			if (info.m_segments == null || info.m_segments.Length == 0)
			{
				if (info.m_lanes != null)
				{
					bool invert;
					if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
					{
						invert = true;
						NetInfo info2 = instance.m_nodes.m_buffer[(int)this.m_endNode].Info;
						NetNode.Flags flags;
						Color color;
						info2.m_netAI.GetNodeState(this.m_endNode, ref instance.m_nodes.m_buffer[(int)this.m_endNode], segmentID, ref this, out flags, out color);
						NetInfo info3 = instance.m_nodes.m_buffer[(int)this.m_startNode].Info;
						NetNode.Flags flags2;
						Color color2;
						info3.m_netAI.GetNodeState(this.m_startNode, ref instance.m_nodes.m_buffer[(int)this.m_startNode], segmentID, ref this, out flags2, out color2);
					}
					else
					{
						invert = false;
						NetInfo info4 = instance.m_nodes.m_buffer[(int)this.m_startNode].Info;
						NetNode.Flags flags;
						Color color;
						info4.m_netAI.GetNodeState(this.m_startNode, ref instance.m_nodes.m_buffer[(int)this.m_startNode], segmentID, ref this, out flags, out color);
						NetInfo info5 = instance.m_nodes.m_buffer[(int)this.m_endNode].Info;
						NetNode.Flags flags2;
						Color color2;
						info5.m_netAI.GetNodeState(this.m_endNode, ref instance.m_nodes.m_buffer[(int)this.m_endNode], segmentID, ref this, out flags2, out color2);
					}
					float startAngle = (float)this.m_cornerAngleStart * 0.0245436933f;
					float endAngle = (float)this.m_cornerAngleEnd * 0.0245436933f;
					int num = 0;
					uint num2 = this.m_lanes;
					int num3 = 0;
					while (num3 < info.m_lanes.Length && num2 != 0u)
					{
						instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].RefreshInstance(num2, info.m_lanes[num3], startAngle, endAngle, invert, ref data, ref num);
						num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
						num3++;
					}
				}
			}
			else
			{
				float vScale = info.m_netAI.GetVScale();
				Vector3 vector2;
				Vector3 startDir;
				bool smoothStart;
				this.CalculateCorner(segmentID, true, true, true, out vector2, out startDir, out smoothStart);
				Vector3 vector3;
				Vector3 endDir;
				bool smoothEnd;
				this.CalculateCorner(segmentID, true, false, true, out vector3, out endDir, out smoothEnd);
				Vector3 vector4;
				Vector3 startDir2;
				this.CalculateCorner(segmentID, true, true, false, out vector4, out startDir2, out smoothStart);
				Vector3 vector5;
				Vector3 endDir2;
				this.CalculateCorner(segmentID, true, false, false, out vector5, out endDir2, out smoothEnd);
				Vector3 vector6;
				Vector3 vector7;
				NetSegment.CalculateMiddlePoints(vector2, startDir, vector5, endDir2, smoothStart, smoothEnd, out vector6, out vector7);
				Vector3 vector8;
				Vector3 vector9;
				NetSegment.CalculateMiddlePoints(vector4, startDir2, vector3, endDir, smoothStart, smoothEnd, out vector8, out vector9);
				data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(vector2, vector6, vector7, vector5, vector4, vector8, vector9, vector3, data.m_position, vScale);
				data.m_dataMatrix1 = NetSegment.CalculateControlMatrix(vector4, vector8, vector9, vector3, vector2, vector6, vector7, vector5, data.m_position, vScale);
			}
			if ((this.m_flags & NetSegment.Flags.NameVisible2) != NetSegment.Flags.None)
			{
				string segmentName = instance.GetSegmentName(segmentID);
				UIFont nameFont = instance.m_properties.m_nameFont;
				data.m_nameData = Singleton<InstanceManager>.get_instance().GetNameData(segmentName, nameFont, true);
				if (data.m_nameData != null)
				{
					float snapElevation = info.m_netAI.GetSnapElevation();
					position.y += snapElevation;
					position2.y += snapElevation;
					Vector3 middlePos;
					Vector3 middlePos2;
					NetSegment.CalculateMiddlePoints(position, this.m_startDirection, position2, this.m_endDirection, true, true, out middlePos, out middlePos2);
					data.m_dataMatrix2 = NetSegment.CalculateControlMatrix(position, middlePos, middlePos2, position2, data.m_position, 1f);
				}
			}
			else
			{
				data.m_nameData = null;
			}
			if (info.m_requireSurfaceMaps)
			{
				Singleton<TerrainManager>.get_instance().GetSurfaceMapping(data.m_position, out data.m_dataTexture0, out data.m_dataTexture1, out data.m_dataVector1);
			}
		}
		if (info.m_segments != null && (layerMask & info.m_netLayers) != 0)
		{
			for (int i = 0; i < info.m_segments.Length; i++)
			{
				NetInfo.Segment segment = info.m_segments[i];
				bool flag;
				if (segment.CheckFlags(this.m_flags, out flag))
				{
					Vector4 dataVector = data.m_dataVector3;
					Vector4 dataVector2 = data.m_dataVector0;
					if (segment.m_requireWindSpeed)
					{
						dataVector.w = data.m_dataFloat0;
					}
					if (flag)
					{
						dataVector2.x = -dataVector2.x;
						dataVector2.y = -dataVector2.y;
					}
					if (cameraInfo.CheckRenderDistance(data.m_position, segment.m_lodRenderDistance))
					{
						instance.m_materialBlock.Clear();
						instance.m_materialBlock.SetMatrix(instance.ID_LeftMatrix, data.m_dataMatrix0);
						instance.m_materialBlock.SetMatrix(instance.ID_RightMatrix, data.m_dataMatrix1);
						instance.m_materialBlock.SetVector(instance.ID_MeshScale, dataVector2);
						instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, dataVector);
						instance.m_materialBlock.SetColor(instance.ID_Color, data.m_dataColor0);
						if (segment.m_requireSurfaceMaps && data.m_dataTexture0 != null)
						{
							instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexA, data.m_dataTexture0);
							instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexB, data.m_dataTexture1);
							instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, data.m_dataVector1);
						}
						NetManager expr_6A0_cp_0 = instance;
						expr_6A0_cp_0.m_drawCallData.m_defaultCalls = expr_6A0_cp_0.m_drawCallData.m_defaultCalls + 1;
						Graphics.DrawMesh(segment.m_segmentMesh, data.m_position, data.m_rotation, segment.m_segmentMaterial, segment.m_layer, null, 0, instance.m_materialBlock);
					}
					else
					{
						NetInfo.LodValue combinedLod = segment.m_combinedLod;
						if (combinedLod != null)
						{
							if (segment.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod.m_surfaceTexA)
							{
								if (combinedLod.m_lodCount != 0)
								{
									NetSegment.RenderLod(cameraInfo, combinedLod);
								}
								combinedLod.m_surfaceTexA = data.m_dataTexture0;
								combinedLod.m_surfaceTexB = data.m_dataTexture1;
								combinedLod.m_surfaceMapping = data.m_dataVector1;
							}
							combinedLod.m_leftMatrices[combinedLod.m_lodCount] = data.m_dataMatrix0;
							combinedLod.m_rightMatrices[combinedLod.m_lodCount] = data.m_dataMatrix1;
							combinedLod.m_meshScales[combinedLod.m_lodCount] = dataVector2;
							combinedLod.m_objectIndices[combinedLod.m_lodCount] = dataVector;
							combinedLod.m_meshLocations[combinedLod.m_lodCount] = data.m_position;
							combinedLod.m_lodMin = Vector3.Min(combinedLod.m_lodMin, data.m_position);
							combinedLod.m_lodMax = Vector3.Max(combinedLod.m_lodMax, data.m_position);
							if (++combinedLod.m_lodCount == combinedLod.m_leftMatrices.Length)
							{
								NetSegment.RenderLod(cameraInfo, combinedLod);
							}
						}
					}
				}
			}
		}
		if (info.m_lanes != null && ((layerMask & info.m_propLayers) != 0 || cameraInfo.CheckRenderDistance(data.m_position, info.m_maxPropDistance + 128f)))
		{
			bool invert2;
			NetNode.Flags startFlags;
			Color startColor;
			NetNode.Flags endFlags;
			Color endColor;
			if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				invert2 = true;
				NetInfo info6 = instance.m_nodes.m_buffer[(int)this.m_endNode].Info;
				info6.m_netAI.GetNodeState(this.m_endNode, ref instance.m_nodes.m_buffer[(int)this.m_endNode], segmentID, ref this, out startFlags, out startColor);
				NetInfo info7 = instance.m_nodes.m_buffer[(int)this.m_startNode].Info;
				info7.m_netAI.GetNodeState(this.m_startNode, ref instance.m_nodes.m_buffer[(int)this.m_startNode], segmentID, ref this, out endFlags, out endColor);
			}
			else
			{
				invert2 = false;
				NetInfo info8 = instance.m_nodes.m_buffer[(int)this.m_startNode].Info;
				info8.m_netAI.GetNodeState(this.m_startNode, ref instance.m_nodes.m_buffer[(int)this.m_startNode], segmentID, ref this, out startFlags, out startColor);
				NetInfo info9 = instance.m_nodes.m_buffer[(int)this.m_endNode].Info;
				info9.m_netAI.GetNodeState(this.m_endNode, ref instance.m_nodes.m_buffer[(int)this.m_endNode], segmentID, ref this, out endFlags, out endColor);
			}
			float startAngle2 = (float)this.m_cornerAngleStart * 0.0245436933f;
			float endAngle2 = (float)this.m_cornerAngleEnd * 0.0245436933f;
			Vector4 objectIndex;
			objectIndex..ctor(data.m_dataVector3.x, data.m_dataVector3.y, 1f, data.m_dataFloat0);
			Vector4 objectIndex2;
			objectIndex2..ctor(data.m_dataVector3.z, data.m_dataVector3.w, 1f, data.m_dataFloat0);
			InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
			if (currentMode != InfoManager.InfoMode.None && !info.m_netAI.ColorizeProps(currentMode))
			{
				objectIndex.z = 0f;
				objectIndex2.z = 0f;
			}
			int num4 = (info.m_segments != null && info.m_segments.Length != 0) ? -1 : 0;
			uint num5 = this.m_lanes;
			if ((this.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				int num6 = 0;
				while (num6 < info.m_lanes.Length && num5 != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)num5)].RenderDestroyedInstance(cameraInfo, segmentID, num5, info, info.m_lanes[num6], startFlags, endFlags, startColor, endColor, startAngle2, endAngle2, invert2, layerMask, objectIndex, objectIndex2, ref data, ref num4);
					num5 = instance.m_lanes.m_buffer[(int)((UIntPtr)num5)].m_nextLane;
					num6++;
				}
			}
			else
			{
				int num7 = 0;
				while (num7 < info.m_lanes.Length && num5 != 0u)
				{
					instance.m_lanes.m_buffer[(int)((UIntPtr)num5)].RenderInstance(cameraInfo, segmentID, num5, info.m_lanes[num7], startFlags, endFlags, startColor, endColor, startAngle2, endAngle2, invert2, layerMask, objectIndex, objectIndex2, ref data, ref num4);
					num5 = instance.m_lanes.m_buffer[(int)((UIntPtr)num5)].m_nextLane;
					num7++;
				}
			}
		}
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, NetInfo.LodValue lod)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (lod.m_lodCount <= 1)
		{
			mesh = lod.m_key.m_mesh.m_mesh1;
			num = 1;
		}
		else if (lod.m_lodCount <= 4)
		{
			mesh = lod.m_key.m_mesh.m_mesh4;
			num = 4;
		}
		else
		{
			mesh = lod.m_key.m_mesh.m_mesh8;
			num = 8;
		}
		for (int i = lod.m_lodCount; i < num; i++)
		{
			lod.m_leftMatrices[i] = default(Matrix4x4);
			lod.m_rightMatrices[i] = default(Matrix4x4);
			lod.m_meshScales[i] = Vector4.get_zero();
			lod.m_objectIndices[i] = Vector4.get_zero();
			lod.m_meshLocations[i] = cameraInfo.m_forward * -100000f;
		}
		materialBlock.SetMatrixArray(instance.ID_LeftMatrices, lod.m_leftMatrices);
		materialBlock.SetMatrixArray(instance.ID_RightMatrices, lod.m_rightMatrices);
		materialBlock.SetVectorArray(instance.ID_MeshScales, lod.m_meshScales);
		materialBlock.SetVectorArray(instance.ID_ObjectIndices, lod.m_objectIndices);
		materialBlock.SetVectorArray(instance.ID_MeshLocations, lod.m_meshLocations);
		if (lod.m_surfaceTexA != null)
		{
			materialBlock.SetTexture(instance.ID_SurfaceTexA, lod.m_surfaceTexA);
			materialBlock.SetTexture(instance.ID_SurfaceTexB, lod.m_surfaceTexB);
			materialBlock.SetVector(instance.ID_SurfaceMapping, lod.m_surfaceMapping);
			lod.m_surfaceTexA = null;
			lod.m_surfaceTexB = null;
		}
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(lod.m_lodMin - new Vector3(100f, 100f, 100f), lod.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			lod.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			lod.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			NetManager expr_260_cp_0 = instance;
			expr_260_cp_0.m_drawCallData.m_lodCalls = expr_260_cp_0.m_drawCallData.m_lodCalls + 1;
			NetManager expr_273_cp_0 = instance;
			expr_273_cp_0.m_drawCallData.m_batchedCalls = expr_273_cp_0.m_drawCallData.m_batchedCalls + (lod.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), lod.m_material, lod.m_key.m_layer, null, 0, materialBlock);
		}
		lod.m_lodCount = 0;
	}

	public NetInfo Info
	{
		get
		{
			return PrefabCollection<NetInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public static float SampleTerrainHeight(NetInfo info, Vector3 worldPos, bool timeLerp, float elevation)
	{
		float num = info.m_netAI.HeightSampleDistance();
		float num2 = num * 0.707106769f;
		float num3;
		if (info.m_netAI.BuildUnderground() || info.m_netAI.UseMinHeight() || elevation < -0.1f)
		{
			num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos);
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num, 0f, 0f)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num2, 0f, num2)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(0f, 0f, num)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num2, 0f, num2)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num, 0f, 0f)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num2, 0f, -num2)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(0f, 0f, -num)));
			num3 = Mathf.Min(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num2, 0f, -num2)));
		}
		else if (info.m_netAI.IgnoreWater())
		{
			num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos);
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num, 0f, 0f)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num2, 0f, num2)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(0f, 0f, num)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num2, 0f, num2)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num, 0f, 0f)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(-num2, 0f, -num2)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(0f, 0f, -num)));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos + new Vector3(num2, 0f, -num2)));
		}
		else if (info.m_netAI.BuildOnWater())
		{
			num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, timeLerp, 8f);
		}
		else
		{
			float waterOffset = 12f - info.m_minHeight;
			num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos, timeLerp, waterOffset);
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(num, 0f, 0f), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(num2, 0f, num2), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(0f, 0f, num), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(-num2, 0f, num2), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(-num, 0f, 0f), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(-num2, 0f, -num2), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(0f, 0f, -num), timeLerp, waterOffset));
			num3 = Mathf.Max(num3, Singleton<TerrainManager>.get_instance().SampleRawHeightSmoothWithWater(worldPos + new Vector3(num2, 0f, -num2), timeLerp, waterOffset));
		}
		return num3 + info.m_buildHeight + elevation;
	}

	public Vector3 FindDirection(ushort segmentID, ushort nodeID)
	{
		NetInfo info = this.Info;
		Vector3 vector = (nodeID != this.m_startNode) ? this.m_endDirection : this.m_startDirection;
		vector.y = 0f;
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)nodeID];
		if ((netNode.m_flags & NetNode.Flags.Middle) != NetNode.Flags.None)
		{
			NetNode netNode2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)((nodeID != this.m_startNode) ? this.m_startNode : this.m_endNode)];
			for (int i = 0; i < 8; i++)
			{
				ushort segment = netNode.GetSegment(i);
				if (segment != 0 && segment != segmentID)
				{
					NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
					NetNode netNode3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)((nodeID != netSegment.m_startNode) ? netSegment.m_startNode : netSegment.m_endNode)];
					Vector3 vector2 = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
					float arg_173_0 = 0.001f;
					Vector2 vector3;
					vector3..ctor(netNode2.m_position.x - netNode.m_position.x, netNode2.m_position.z - netNode.m_position.z);
					float num = Mathf.Max(arg_173_0, vector3.get_magnitude());
					float arg_1BF_0 = 0.001f;
					Vector2 vector4;
					vector4..ctor(netNode3.m_position.x - netNode.m_position.x, netNode3.m_position.z - netNode.m_position.z);
					float num2 = Mathf.Max(arg_1BF_0, vector4.get_magnitude());
					Vector3 worldPos = netNode.m_position + vector * (num / 3f);
					Vector3 worldPos2 = netNode.m_position + vector2 * (num2 / 3f);
					if (info.m_followTerrain && !info.m_netAI.IsUnderground())
					{
						worldPos.y = NetSegment.SampleTerrainHeight(info, worldPos, false, 0f);
						worldPos2.y = NetSegment.SampleTerrainHeight(info, worldPos2, false, 0f);
					}
					else
					{
						worldPos.y = Mathf.Lerp(netNode.m_position.y, netNode2.m_position.y, 0.333333343f);
						worldPos2.y = Mathf.Lerp(netNode.m_position.y, netNode3.m_position.y, 0.333333343f);
					}
					float num3 = (worldPos.y - netNode.m_position.y) * 3f / num;
					float num4 = (worldPos2.y - netNode.m_position.y) * 3f / num2;
					if (num4 > 0f)
					{
						vector.y = Mathf.Clamp(num3, -num4, 0f);
					}
					else
					{
						vector.y = Mathf.Clamp(num3, 0f, -num4);
					}
					break;
				}
			}
		}
		else if (!info.m_flatJunctions)
		{
			NetNode netNode4 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)((nodeID != this.m_startNode) ? this.m_startNode : this.m_endNode)];
			float arg_3B6_0 = 0.001f;
			Vector2 vector5;
			vector5..ctor(netNode4.m_position.x - netNode.m_position.x, netNode4.m_position.z - netNode.m_position.z);
			float num5 = Mathf.Max(arg_3B6_0, vector5.get_magnitude());
			vector.y = (netNode4.m_position.y - netNode.m_position.y) / num5;
		}
		return vector;
	}

	public void CalculateSegment(ushort segmentID)
	{
		if (this.m_flags != NetSegment.Flags.None)
		{
			this.m_startDirection = this.FindDirection(segmentID, this.m_startNode);
			this.m_endDirection = this.FindDirection(segmentID, this.m_endNode);
			NetManager instance = Singleton<NetManager>.get_instance();
			if ((instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				this.m_flags |= NetSegment.Flags.End;
			}
			else if ((instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				this.m_flags |= NetSegment.Flags.End;
			}
			else
			{
				this.m_flags &= ~NetSegment.Flags.End;
			}
			this.UpdateStartSegments(segmentID);
			this.UpdateEndSegments(segmentID);
			this.UpdateNameSeed(segmentID);
			instance.m_updateNameVisibility.Add(segmentID);
		}
	}

	public void UpdateNameSeed(ushort segmentID)
	{
		if ((Singleton<NetManager>.get_instance().m_adjustedSegments[segmentID >> 6] & 1uL << (int)segmentID) != 0uL)
		{
			return;
		}
		bool flag = false;
		uint num2;
		ushort num = NetSegment.FindNameSeed(this.m_startNode, segmentID, out num2);
		uint num4;
		ushort num3 = NetSegment.FindNameSeed(this.m_endNode, segmentID, out num4);
		if (num != 0 && num == this.m_nameSeed)
		{
			if (num3 != 0 && num3 != this.m_nameSeed)
			{
				flag = true;
			}
		}
		else if (num3 != 0 && num3 == this.m_nameSeed)
		{
			if (num != 0 && num != this.m_nameSeed)
			{
				flag = true;
			}
		}
		else if (num != 0 && (num == num3 || num3 == 0 || num2 <= num4))
		{
			this.m_nameSeed = num;
			flag = true;
		}
		else if (num3 != 0)
		{
			this.m_nameSeed = num3;
			flag = true;
		}
		if (this.m_nameSeed == 0)
		{
			this.m_nameSeed = (ushort)Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(1u, 65535u);
			flag = true;
		}
		if (flag)
		{
			for (int i = 0; i < 10; i++)
			{
				if (this.ValidateNameSeed(segmentID, this.m_nameSeed))
				{
					break;
				}
				if (i == 0 && this.m_nameSeed == num && num3 != num && num3 != 0)
				{
					this.m_nameSeed = num3;
				}
				else if (i == 0 && this.m_nameSeed == num3 && num != num3 && num != 0)
				{
					this.m_nameSeed = num;
				}
				else
				{
					this.m_nameSeed = (ushort)Singleton<SimulationManager>.get_instance().m_randomizer.UInt32(1u, 65535u);
				}
			}
		}
	}

	private bool ValidateNameSeed(ushort segmentID, ushort seed)
	{
		NetSegment.m_tempCheckedSet.Clear();
		NetSegment.m_tempCheckedSet2.Clear();
		NetSegment.ValidateNameSeedPass1(this.m_startNode, segmentID);
		NetSegment.ValidateNameSeedPass1(this.m_endNode, segmentID);
		bool result = true;
		NetSegment.ValidateNameSeedPass2(this.m_startNode, segmentID, seed, ref result);
		NetSegment.ValidateNameSeedPass2(this.m_endNode, segmentID, seed, ref result);
		return result;
	}

	private static void ValidateNameSeedPass1(ushort nodeID, ushort segmentID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		while (segmentID != 0)
		{
			NetInfo info = instance.m_segments.m_buffer[(int)segmentID].Info;
			Vector3 direction = instance.m_segments.m_buffer[(int)segmentID].GetDirection(nodeID);
			NetSegment.m_tempCheckedSet.Add(segmentID);
			segmentID = 0;
			bool flag = (instance.m_nodes.m_buffer[(int)nodeID].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)nodeID].GetSegment(i);
				if (segment != 0)
				{
					if (!NetSegment.m_tempCheckedSet.Contains(segment))
					{
						if ((instance.m_adjustedSegments[segment >> 6] & 1uL << (int)segment) == 0uL)
						{
							if (flag)
							{
								NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
								if (info.m_class.m_service != info2.m_class.m_service)
								{
									goto IL_19F;
								}
								if (info.m_class.m_subService != info2.m_class.m_subService)
								{
									goto IL_19F;
								}
								Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(nodeID);
								if (direction.x * direction2.x + direction.z * direction2.z > -0.93f)
								{
									goto IL_19F;
								}
							}
							segmentID = segment;
							nodeID = instance.m_segments.m_buffer[(int)segment].GetOtherNode(nodeID);
							break;
						}
					}
				}
				IL_19F:;
			}
		}
	}

	private static void ValidateNameSeedPass2(ushort nodeID, ushort segmentID, ushort seed, ref bool valid)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		while (segmentID != 0)
		{
			NetInfo info = instance.m_segments.m_buffer[(int)segmentID].Info;
			Vector3 direction = instance.m_segments.m_buffer[(int)segmentID].GetDirection(nodeID);
			NetSegment.m_tempCheckedSet2.Add(segmentID);
			segmentID = 0;
			bool flag = (instance.m_nodes.m_buffer[(int)nodeID].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None;
			ushort num = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)nodeID].GetSegment(i);
				if (segment != 0)
				{
					if (!NetSegment.m_tempCheckedSet2.Contains(segment))
					{
						if ((instance.m_adjustedSegments[segment >> 6] & 1uL << (int)segment) == 0uL)
						{
							bool flag2 = true;
							if (flag)
							{
								NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
								if (info.m_class.m_service != info2.m_class.m_service)
								{
									flag2 = false;
								}
								if (info.m_class.m_subService != info2.m_class.m_subService)
								{
									flag2 = false;
								}
								Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(nodeID);
								if (direction.x * direction2.x + direction.z * direction2.z > -0.93f)
								{
									flag2 = false;
								}
							}
							if (flag2)
							{
								if (segmentID == 0)
								{
									instance.m_updateNameVisibility.Add(segment);
									if (seed != instance.m_segments.m_buffer[(int)segment].m_nameSeed)
									{
										instance.m_segments.m_buffer[(int)segment].m_nameSeed = seed;
										instance.UpdateSegmentRenderer(segment, false);
									}
									segmentID = segment;
									num = instance.m_segments.m_buffer[(int)segment].GetOtherNode(nodeID);
								}
							}
							else if (!NetSegment.m_tempCheckedSet.Contains(segment) && instance.m_segments.m_buffer[(int)segment].m_nameSeed == seed)
							{
								valid = false;
							}
						}
					}
				}
			}
			nodeID = num;
		}
	}

	private static ushort FindNameSeed(ushort nodeID, ushort segmentID, out uint buildIndex)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetSegment.m_tempCheckedSet.Clear();
		while (segmentID != 0)
		{
			NetInfo info = instance.m_segments.m_buffer[(int)segmentID].Info;
			Vector3 direction = instance.m_segments.m_buffer[(int)segmentID].GetDirection(nodeID);
			NetSegment.m_tempCheckedSet.Add(segmentID);
			segmentID = 0;
			bool flag = (instance.m_nodes.m_buffer[(int)nodeID].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)nodeID].GetSegment(i);
				if (segment != 0)
				{
					if (!NetSegment.m_tempCheckedSet.Contains(segment))
					{
						if ((instance.m_adjustedSegments[segment >> 6] & 1uL << (int)segment) == 0uL)
						{
							if (flag)
							{
								NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
								if (info.m_class.m_service != info2.m_class.m_service)
								{
									goto IL_1E5;
								}
								if (info.m_class.m_subService != info2.m_class.m_subService)
								{
									goto IL_1E5;
								}
								Vector3 direction2 = instance.m_segments.m_buffer[(int)segment].GetDirection(nodeID);
								if (direction.x * direction2.x + direction.z * direction2.z > -0.93f)
								{
									goto IL_1E5;
								}
							}
							ushort nameSeed = instance.m_segments.m_buffer[(int)segment].m_nameSeed;
							if (nameSeed != 0)
							{
								buildIndex = instance.m_segments.m_buffer[(int)segment].m_buildIndex;
								return nameSeed;
							}
							segmentID = segment;
							nodeID = instance.m_segments.m_buffer[(int)segment].GetOtherNode(nodeID);
							break;
						}
					}
				}
				IL_1E5:;
			}
		}
		buildIndex = 0u;
		return 0;
	}

	public void UpdateBounds(ushort segmentID)
	{
		if (this.m_flags != NetSegment.Flags.None)
		{
			NetInfo info = this.Info;
			if (info == null)
			{
				return;
			}
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)this.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_position;
			Vector3 vector;
			Vector3 startDir;
			bool smoothStart;
			this.CalculateCorner(segmentID, true, true, true, out vector, out startDir, out smoothStart);
			Vector3 vector2;
			Vector3 endDir;
			bool smoothEnd;
			this.CalculateCorner(segmentID, true, false, true, out vector2, out endDir, out smoothEnd);
			Vector3 vector3;
			Vector3 startDir2;
			this.CalculateCorner(segmentID, true, true, false, out vector3, out startDir2, out smoothStart);
			Vector3 vector4;
			Vector3 endDir2;
			this.CalculateCorner(segmentID, true, false, false, out vector4, out endDir2, out smoothEnd);
			Vector3 vector5;
			Vector3 vector6;
			NetSegment.CalculateMiddlePoints(vector, startDir, vector4, endDir2, smoothStart, smoothEnd, out vector5, out vector6);
			Vector3 vector7;
			Vector3 vector8;
			NetSegment.CalculateMiddlePoints(vector3, startDir2, vector2, endDir, smoothStart, smoothEnd, out vector7, out vector8);
			Vector3 vector9 = Vector3.Min(Vector3.Min(Vector3.Min(vector, vector3), Vector3.Min(vector5, vector7)), Vector3.Min(Vector3.Min(vector6, vector8), Vector3.Min(vector4, vector2)));
			Vector3 vector10 = Vector3.Max(Vector3.Max(Vector3.Max(vector, vector3), Vector3.Max(vector5, vector7)), Vector3.Max(Vector3.Max(vector6, vector8), Vector3.Max(vector4, vector2)));
			vector9 = Vector3.Min(vector9, Vector3.Min(position, position2) - new Vector3(info.m_halfWidth, 0f, info.m_halfWidth));
			vector10 = Vector3.Max(vector10, Vector3.Max(position, position2) + new Vector3(info.m_halfWidth, 0f, info.m_halfWidth));
			vector9.y += info.m_minHeight;
			vector10.y += info.m_maxHeight;
			Vector3 vector11 = Bezier3.Position(vector, vector5, vector6, vector4, 0.5f);
			Vector3 vector12 = Bezier3.Position(vector3, vector7, vector8, vector2, 0.5f);
			this.m_bounds.SetMinMax(vector9, vector10);
			this.m_middlePosition = (vector11 + vector12) * 0.5f;
		}
	}

	public void UpdateSegment(ushort segmentID)
	{
		if (this.m_flags != NetSegment.Flags.None)
		{
			NetInfo info = this.Info;
			if (info == null)
			{
				return;
			}
			if (info.m_flattenTerrain || info.m_lowerTerrain || info.m_netAI.RaiseTerrain() || info.m_createPavement || info.m_createGravel || info.m_createRuining || info.m_clipTerrain)
			{
				TerrainModify.UpdateArea(this.m_bounds.get_min().x, this.m_bounds.get_min().z, this.m_bounds.get_max().x, this.m_bounds.get_max().z, info.m_flattenTerrain || info.m_lowerTerrain || info.m_netAI.RaiseTerrain(), info.m_createPavement || info.m_createGravel || info.m_createRuining || info.m_clipTerrain, false);
			}
			if (info.m_hasParkingSpaces)
			{
				Vector3 min = this.m_bounds.get_min();
				Vector3 max = this.m_bounds.get_max();
				Singleton<VehicleManager>.get_instance().UpdateParkedVehicles(min.x, min.z, max.x, max.z);
			}
		}
	}

	public void CountLanes(ushort segmentID, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, ref int forward, ref int backward)
	{
		if (this.m_flags == NetSegment.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info != null && info.m_lanes != null)
		{
			for (int i = 0; i < info.m_lanes.Length; i++)
			{
				NetInfo.Lane lane = info.m_lanes[i];
				if (lane.CheckType(laneTypes, vehicleTypes))
				{
					NetInfo.Direction direction = lane.m_finalDirection;
					if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
					{
						direction = NetInfo.InvertDirection(direction);
					}
					if ((byte)(direction & NetInfo.Direction.Forward) != 0)
					{
						forward++;
					}
					if ((byte)(direction & NetInfo.Direction.Backward) != 0)
					{
						backward++;
					}
				}
			}
		}
	}

	public void CountLanes(ushort segmentID, NetInfo.Direction directions, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, Vector3 direction, ref int left, ref int forward, ref int right, ref int left2, ref int forward2, ref int right2)
	{
		if (this.m_flags == NetSegment.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info != null && info.m_lanes != null)
		{
			for (int i = 0; i < info.m_lanes.Length; i++)
			{
				NetInfo.Lane lane = info.m_lanes[i];
				if (lane.CheckType(laneTypes, vehicleTypes))
				{
					NetInfo.Direction direction2 = lane.m_finalDirection;
					if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
					{
						direction2 = NetInfo.InvertDirection(direction2);
					}
					Vector3 vector;
					if ((byte)(directions & NetInfo.Direction.Both) == 2)
					{
						vector = this.m_endDirection;
					}
					else
					{
						vector = this.m_startDirection;
					}
					float num = vector.x * direction.z - vector.z * direction.x;
					if ((byte)(direction2 & directions) != 0)
					{
						if (num < -0.5f)
						{
							left++;
						}
						else if (num > 0.5f)
						{
							right++;
						}
						else
						{
							forward++;
						}
					}
					else if (num < -0.5f)
					{
						left2++;
					}
					else if (num > 0.5f)
					{
						right2++;
					}
					else
					{
						forward2++;
					}
				}
			}
		}
	}

	public void UpdateLanes(ushort segmentID, bool loading)
	{
		if (this.m_flags != NetSegment.Flags.None)
		{
			NetInfo info = this.Info;
			if (info == null)
			{
				return;
			}
			if (this.m_lanes != 0u || (info.m_lanes != null && info.m_lanes.Length != 0))
			{
				info.m_netAI.UpdateLanes(segmentID, ref this, loading);
			}
		}
	}

	public void UpdateZones(ushort segmentID)
	{
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		if (info.m_class.m_layer == ItemClass.Layer.Default)
		{
			Vector3 min = this.m_bounds.get_min();
			Vector3 max = this.m_bounds.get_max();
			Quad2 quad;
			quad..ctor(new Vector2(min.x, min.z), new Vector2(min.x, max.z), new Vector2(max.x, max.z), new Vector2(max.x, min.z));
			Singleton<ZoneManager>.get_instance().UpdateBlocks(quad);
			Singleton<PropManager>.get_instance().UpdateProps(this.m_bounds.get_min().x, this.m_bounds.get_min().z, this.m_bounds.get_max().x, this.m_bounds.get_max().z);
			Singleton<TreeManager>.get_instance().UpdateTrees(this.m_bounds.get_min().x, this.m_bounds.get_min().z, this.m_bounds.get_max().x, this.m_bounds.get_max().z);
		}
	}

	public void TerrainUpdated(ushort segmentID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		bool flag = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None;
		bool flag2 = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None;
		bool flag3 = info.m_createPavement && (!info.m_lowerTerrain || flag || flag2);
		bool flag4 = info.m_createGravel && !info.m_lowerTerrain;
		bool flag5 = info.m_createRuining && !info.m_lowerTerrain;
		bool flag6 = info.m_clipTerrain && !info.m_lowerTerrain;
		bool flag7 = !info.m_flattenTerrain && !info.m_lowerTerrain && info.m_netAI.RaiseTerrain();
		if (info.m_flattenTerrain || info.m_lowerTerrain || flag7 || info.m_blockWater || flag3 || flag4 || flag5 || flag6)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].GetSegment(i);
				if (segment != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
				{
					return;
				}
			}
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].GetSegment(j);
				if (segment2 != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
				{
					return;
				}
			}
			Bezier3 bezier = default(Bezier3);
			Bezier3 bezier2 = default(Bezier3);
			Vector3 startDir;
			bool smoothStart;
			this.CalculateCorner(segmentID, false, true, true, out bezier.a, out startDir, out smoothStart);
			Vector3 endDir;
			bool smoothEnd;
			this.CalculateCorner(segmentID, false, false, true, out bezier2.d, out endDir, out smoothEnd);
			Vector3 startDir2;
			this.CalculateCorner(segmentID, false, true, false, out bezier2.a, out startDir2, out smoothStart);
			Vector3 endDir2;
			this.CalculateCorner(segmentID, false, false, false, out bezier.d, out endDir2, out smoothEnd);
			Vector3 a = bezier.a;
			Vector3 a2 = bezier2.a;
			Vector3 d = bezier.d;
			Vector3 d2 = bezier2.d;
			TerrainModify.Heights heights = TerrainModify.Heights.None;
			if (info.m_flattenTerrain)
			{
				heights |= TerrainModify.Heights.PrimaryLevel;
			}
			if (info.m_lowerTerrain)
			{
				heights |= TerrainModify.Heights.PrimaryMax;
			}
			if (info.m_blockWater)
			{
				heights |= TerrainModify.Heights.BlockHeight;
			}
			if (flag7)
			{
				heights = TerrainModify.Heights.SecondaryMin;
			}
			TerrainModify.Surface surface = TerrainModify.Surface.None;
			if (flag3)
			{
				surface |= TerrainModify.Surface.PavementA;
			}
			if (flag4)
			{
				surface |= TerrainModify.Surface.Gravel;
			}
			if (flag5)
			{
				surface |= TerrainModify.Surface.Ruined;
			}
			if (flag6)
			{
				surface |= TerrainModify.Surface.Clip;
			}
			TerrainModify.Edges edges = TerrainModify.Edges.All;
			float num = 0f;
			float num2 = 1f;
			float num3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			float num6 = 0f;
			int num7 = 0;
			while (info.m_netAI.SegmentModifyMask(segmentID, ref this, num7, ref surface, ref heights, ref edges, ref num, ref num2, ref num3, ref num4, ref num5, ref num6))
			{
				if (num != 0f || num2 != 1f || num7 != 0)
				{
					bezier.a = Vector3.Lerp(a, a2, num);
					bezier2.a = Vector3.Lerp(a, a2, num2);
					bezier.d = Vector3.Lerp(d, d2, num);
					bezier2.d = Vector3.Lerp(d, d2, num2);
				}
				bezier.a.y = bezier.a.y + num3;
				bezier2.a.y = bezier2.a.y + num4;
				bezier.d.y = bezier.d.y + num5;
				bezier2.d.y = bezier2.d.y + num6;
				NetSegment.CalculateMiddlePoints(bezier.a, startDir, bezier.d, endDir2, smoothStart, smoothEnd, out bezier.b, out bezier.c);
				NetSegment.CalculateMiddlePoints(bezier2.a, startDir2, bezier2.d, endDir, smoothStart, smoothEnd, out bezier2.b, out bezier2.c);
				Vector3 vector = Vector3.Min(bezier.Min(), bezier2.Min());
				Vector3 vector2 = Vector3.Max(bezier.Max(), bezier2.Max());
				if (vector.x <= maxX && vector.z <= maxZ && minX <= vector2.x && minZ <= vector2.z)
				{
					float num8 = Vector3.Distance(bezier.a, bezier.b);
					float num9 = Vector3.Distance(bezier.b, bezier.c);
					float num10 = Vector3.Distance(bezier.c, bezier.d);
					float num11 = Vector3.Distance(bezier2.a, bezier2.b);
					float num12 = Vector3.Distance(bezier2.b, bezier2.c);
					float num13 = Vector3.Distance(bezier2.c, bezier2.d);
					Vector3 vector3 = (bezier.a - bezier.b) * (1f / Mathf.Max(0.1f, num8));
					Vector3 vector4 = (bezier.c - bezier.b) * (1f / Mathf.Max(0.1f, num9));
					Vector3 vector5 = (bezier.d - bezier.c) * (1f / Mathf.Max(0.1f, num10));
					float num14 = Mathf.Min(Vector3.Dot(vector3, vector4), Vector3.Dot(vector4, vector5));
					num8 += num9 + num10;
					num11 += num12 + num13;
					float num15;
					float num16;
					info.m_netAI.GetTerrainModifyRange(out num15, out num16);
					int num17 = Mathf.Clamp(Mathf.CeilToInt(Mathf.Min(Mathf.Max(num8, num11) * 0.25f * (num16 - num15), 100f - num14 * 100f)), 1, 16);
					float num18 = num15;
					Vector3 a3 = bezier.Position(num18);
					Vector3 d3 = bezier2.Position(num18);
					num18 = 3f * num18 * num18 - 2f * num18 * num18 * num18;
					float num19 = info.m_terrainStartOffset + (info.m_terrainEndOffset - info.m_terrainStartOffset) * num18;
					if (info.m_lowerTerrain)
					{
						num19 += info.m_netAI.GetTerrainLowerOffset();
					}
					if (flag7)
					{
						num19 += info.m_maxHeight;
					}
					a3.y += num19;
					d3.y += num19;
					for (int k = 1; k <= num17; k++)
					{
						num18 = num15 + (num16 - num15) * (float)k / (float)num17;
						Vector3 vector6 = bezier.Position(num18);
						Vector3 vector7 = bezier2.Position(num18);
						num18 = 3f * num18 * num18 - 2f * num18 * num18 * num18;
						num19 = info.m_terrainStartOffset + (info.m_terrainEndOffset - info.m_terrainStartOffset) * num18;
						if (info.m_lowerTerrain)
						{
							num19 += info.m_netAI.GetTerrainLowerOffset();
						}
						if (flag7)
						{
							num19 += info.m_maxHeight;
						}
						vector6.y += num19;
						vector7.y += num19;
						TerrainModify.Surface surface2 = surface;
						if (!info.m_createPavement || (info.m_lowerTerrain && (!flag || k != 1) && (!flag2 || k != num17)))
						{
							surface2 &= ~TerrainModify.Surface.PavementA;
						}
						if ((surface2 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
						{
							surface2 |= TerrainModify.Surface.Gravel;
						}
						TerrainModify.Edges edges2 = TerrainModify.Edges.AB | TerrainModify.Edges.CD;
						if (num15 != 0f && k == 1)
						{
							edges2 |= TerrainModify.Edges.DA;
						}
						if (num16 != 1f && k == num17)
						{
							edges2 |= TerrainModify.Edges.BC;
						}
						edges2 &= edges;
						TerrainModify.ApplyQuad(a3, vector6, vector7, d3, edges2, heights, surface2);
						a3 = vector6;
						d3 = vector7;
					}
					if (num7 == 0 && num15 != 0f && !flag7 && info.m_netAI.RaiseTerrain())
					{
						num17 = Mathf.Clamp(Mathf.CeilToInt(Mathf.Min(Mathf.Max(num8, num11) * 0.25f * num15, 100f - num14 * 100f)), 1, 16);
						a3 = bezier.a;
						d3 = bezier2.a;
						num19 = info.m_terrainStartOffset * 0.75f;
						a3.y += num19;
						d3.y += num19;
						for (int l = 1; l <= num17; l++)
						{
							num18 = num15 * (float)l / (float)num17;
							Vector3 vector8 = bezier.Position(num18);
							Vector3 vector9 = bezier2.Position(num18);
							num18 = 3f * num18 * num18 - 2f * num18 * num18 * num18;
							vector8.y += num19;
							vector9.y += num19;
							TerrainModify.Edges edges3 = TerrainModify.Edges.AB | TerrainModify.Edges.CD;
							if (l == 1)
							{
								edges3 |= TerrainModify.Edges.DA;
							}
							edges3 &= edges;
							TerrainModify.ApplyQuad(a3, vector8, vector9, d3, edges3, TerrainModify.Heights.SecondaryMin, TerrainModify.Surface.None);
							a3 = vector8;
							d3 = vector9;
						}
					}
				}
				num7++;
			}
		}
	}

	public bool OverlapQuad(ushort segmentID, Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType)
	{
		if ((this.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
		{
			return false;
		}
		NetInfo info = this.Info;
		if (!info.m_canCollide)
		{
			return false;
		}
		float collisionHalfWidth = info.m_netAI.GetCollisionHalfWidth();
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		Bezier3 bezier = default(Bezier3);
		bezier.a = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_position;
		bezier.d = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_position;
		NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, true, true, out bezier.b, out bezier.c);
		Vector3 vector3 = bezier.Min() + new Vector3(-collisionHalfWidth, info.m_minHeight, -collisionHalfWidth);
		Vector3 vector4 = bezier.Max() + new Vector3(collisionHalfWidth, info.m_maxHeight, collisionHalfWidth);
		ItemClass.CollisionType collisionType2 = info.m_netAI.GetCollisionType();
		if (vector3.x <= vector2.x && vector3.z <= vector2.y && vector.x <= vector4.x && vector.y <= vector4.z && ItemClass.CheckCollisionType(minY, maxY, vector3.y, vector4.y, collisionType, collisionType2))
		{
			int num = 16;
			float num2;
			float num3;
			info.m_netAI.GetTerrainModifyRange(out num2, out num3);
			num2 *= 0.5f;
			num3 = 1f - (1f - num3) * 0.5f;
			float num4 = num2;
			Vector3 vector5 = bezier.Position(num4);
			Vector3 vector6 = bezier.Tangent(num4);
			Vector3 vector7;
			vector7..ctor(-vector6.z, 0f, vector6.x);
			vector6 = vector7.get_normalized() * collisionHalfWidth;
			Vector3 vector8 = vector5 + vector6;
			Vector3 vector9 = vector5 - vector6;
			float endRadius = info.m_netAI.GetEndRadius();
			if (info.m_clipSegmentEnds && endRadius != 0f && num2 == 0f && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) != NetNode.Flags.None)
			{
				Vector3 vector10 = vector5;
				vector10.x += vector6.x * 0.8f - vector6.z * 0.6f * endRadius / collisionHalfWidth;
				vector10.z += vector6.z * 0.8f + vector6.x * 0.6f * endRadius / collisionHalfWidth;
				Vector3 vector11 = vector5;
				vector11.x -= vector6.x * 0.8f + vector6.z * 0.6f * endRadius / collisionHalfWidth;
				vector11.z -= vector6.z * 0.8f - vector6.x * 0.6f * endRadius / collisionHalfWidth;
				Vector3 vector12 = vector5;
				vector12.x += vector6.x * 0.3f - vector6.z * endRadius / collisionHalfWidth;
				vector12.z += vector6.z * 0.3f + vector6.x * endRadius / collisionHalfWidth;
				Vector3 vector13 = vector5;
				vector13.x -= vector6.x * 0.3f + vector6.z * endRadius / collisionHalfWidth;
				vector13.z -= vector6.z * 0.3f - vector6.x * endRadius / collisionHalfWidth;
				vector3.y = vector5.y + info.m_minHeight;
				vector4.y = vector5.y + info.m_maxHeight;
				if (ItemClass.CheckCollisionType(minY, maxY, vector3.y, vector4.y, collisionType, collisionType2))
				{
					if (quad.Intersect(Quad2.XZ(vector8, vector9, vector11, vector10)))
					{
						return true;
					}
					if (quad.Intersect(Quad2.XZ(vector10, vector11, vector13, vector12)))
					{
						return true;
					}
				}
			}
			for (int i = 1; i <= num; i++)
			{
				num4 = num2 + (num3 - num2) * (float)i / (float)num;
				vector5 = bezier.Position(num4);
				vector6 = bezier.Tangent(num4);
				Vector3 vector14;
				vector14..ctor(-vector6.z, 0f, vector6.x);
				vector6 = vector14.get_normalized() * collisionHalfWidth;
				Vector3 vector15 = vector5 + vector6;
				Vector3 vector16 = vector5 - vector6;
				vector3.y = Mathf.Min(Mathf.Min(vector8.y, vector15.y), Mathf.Min(vector16.y, vector9.y)) + info.m_minHeight;
				vector4.y = Mathf.Max(Mathf.Max(vector8.y, vector15.y), Mathf.Max(vector16.y, vector9.y)) + info.m_maxHeight;
				if (ItemClass.CheckCollisionType(minY, maxY, vector3.y, vector4.y, collisionType, collisionType2) && quad.Intersect(Quad2.XZ(vector8, vector15, vector16, vector9)))
				{
					return true;
				}
				vector8 = vector15;
				vector9 = vector16;
			}
			if (info.m_clipSegmentEnds && endRadius != 0f && num3 == 1f && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) != NetNode.Flags.None)
			{
				Vector3 vector17 = vector5;
				vector17.x += vector6.x * 0.8f + vector6.z * 0.6f * endRadius / collisionHalfWidth;
				vector17.z += vector6.z * 0.8f - vector6.x * 0.6f * endRadius / collisionHalfWidth;
				Vector3 vector18 = vector5;
				vector18.x -= vector6.x * 0.8f - vector6.z * 0.6f * endRadius / collisionHalfWidth;
				vector18.z -= vector6.z * 0.8f + vector6.x * 0.6f * endRadius / collisionHalfWidth;
				Vector3 vector19 = vector5;
				vector19.x += vector6.x * 0.3f + vector6.z * endRadius / collisionHalfWidth;
				vector19.z += vector6.z * 0.3f - vector6.x * endRadius / collisionHalfWidth;
				Vector3 vector20 = vector5;
				vector20.x -= vector6.x * 0.3f - vector6.z * endRadius / collisionHalfWidth;
				vector20.z -= vector6.z * 0.3f + vector6.x * endRadius / collisionHalfWidth;
				vector3.y = vector5.y + info.m_minHeight;
				vector4.y = vector5.y + info.m_maxHeight;
				if (ItemClass.CheckCollisionType(minY, maxY, vector3.y, vector4.y, collisionType, collisionType2))
				{
					if (quad.Intersect(Quad2.XZ(vector8, vector17, vector18, vector9)))
					{
						return true;
					}
					if (quad.Intersect(Quad2.XZ(vector17, vector19, vector20, vector18)))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public void AfterTerrainUpdate(ushort segmentID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		info.m_netAI.AfterTerrainUpdate(segmentID, ref this);
	}

	public bool RayCast(ushort segmentID, Segment3 ray, float snapElevation, bool nameOnly, out float t, out float priority)
	{
		NetInfo info = this.Info;
		t = 0f;
		priority = 0f;
		if (nameOnly && (this.m_flags & NetSegment.Flags.NameVisible2) == NetSegment.Flags.None)
		{
			return false;
		}
		Bounds bounds = this.m_bounds;
		bounds.Expand(16f);
		if (!bounds.IntersectRay(new Ray(ray.a, ray.b - ray.a)))
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		Bezier3 bezier = default(Bezier3);
		bezier.a = instance.m_nodes.m_buffer[(int)this.m_startNode].m_position;
		bezier.d = instance.m_nodes.m_buffer[(int)this.m_endNode].m_position;
		bool result = false;
		if (nameOnly)
		{
			RenderManager instance2 = Singleton<RenderManager>.get_instance();
			uint num;
			if (instance2.GetInstanceIndex((uint)(49152 + segmentID), out num))
			{
				InstanceManager.NameData nameData = instance2.m_instances[(int)((UIntPtr)num)].m_nameData;
				Vector3 position = instance2.m_instances[(int)((UIntPtr)num)].m_position;
				Matrix4x4 dataMatrix = instance2.m_instances[(int)((UIntPtr)num)].m_dataMatrix2;
				float num2 = Vector3.Distance(position, ray.a);
				if (nameData != null && num2 < 1000f)
				{
					float snapElevation2 = info.m_netAI.GetSnapElevation();
					bezier.a.y = bezier.a.y + snapElevation2;
					bezier.d.y = bezier.d.y + snapElevation2;
					NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, true, true, out bezier.b, out bezier.c);
					float num3 = Mathf.Max(1f, Mathf.Abs(dataMatrix.m33 - dataMatrix.m30));
					float num4 = num2 * 0.0002f + 0.05f / (1f + num2 * 0.001f);
					Vector2 vector = nameData.m_size * num4;
					float num5 = Mathf.Max(0f, 0.5f - vector.x / num3 * 0.5f);
					float num6 = Mathf.Min(1f, 0.5f + vector.x / num3 * 0.5f);
					bezier = bezier.Cut(num5, num6);
					float num8;
					float num9;
					float num7 = bezier.DistanceSqr(ray, ref num8, ref num9);
					if (num7 < vector.y * vector.y * 0.25f)
					{
						Vector3 vector2 = bezier.Position(num8);
						if (Segment1.Intersect(ray.a.y, ray.b.y, vector2.y, ref num8))
						{
							num7 = Vector3.SqrMagnitude(ray.Position(num8) - vector2);
							if (num7 < vector.y * vector.y * 0.25f)
							{
								t = num8;
								result = true;
							}
						}
					}
				}
			}
		}
		else
		{
			float num10;
			float num11;
			float num12;
			info.m_netAI.GetRayCastHeights(segmentID, ref this, out num10, out num11, out num12);
			bezier.a.y = bezier.a.y + num12;
			bezier.d.y = bezier.d.y + num12;
			bool flag = (instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
			bool flag2 = (instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
			NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, flag, flag2, out bezier.b, out bezier.c);
			float minNodeDistance = info.GetMinNodeDistance();
			float collisionHalfWidth = info.m_netAI.GetCollisionHalfWidth();
			float num13 = (float)instance.m_nodes.m_buffer[(int)this.m_startNode].m_elevation;
			float num14 = (float)instance.m_nodes.m_buffer[(int)this.m_endNode].m_elevation;
			if (info.m_netAI.IsUnderground())
			{
				num13 = -num13;
				num14 = -num14;
			}
			num13 += info.m_netAI.GetSnapElevation();
			num14 += info.m_netAI.GetSnapElevation();
			float num15 = Mathf.Lerp(minNodeDistance, collisionHalfWidth, Mathf.Clamp01(Mathf.Abs(snapElevation - num13) / 12f));
			float num16 = Mathf.Lerp(minNodeDistance, collisionHalfWidth, Mathf.Clamp01(Mathf.Abs(snapElevation - num14) / 12f));
			float num17 = Mathf.Min(num10, num11);
			t = 1000000f;
			priority = 1000000f;
			Segment3 segment;
			segment.a = bezier.a;
			for (int i = 1; i <= 16; i++)
			{
				segment.b = bezier.Position((float)i / 16f);
				float num19;
				float num20;
				float num18 = ray.DistanceSqr(segment, ref num19, ref num20);
				float num21 = Mathf.Lerp(num15, num16, ((float)(i - 1) + num20) / 16f);
				Vector3 vector3 = segment.Position(num20);
				if (num18 < priority && Segment1.Intersect(ray.a.y, ray.b.y, vector3.y, ref num19))
				{
					Vector3 vector4 = ray.Position(num19);
					num18 = Vector3.SqrMagnitude(vector4 - vector3);
					if (num18 < priority && num18 < num21 * num21)
					{
						if (flag && i == 1 && num20 < 0.001f)
						{
							Vector3 vector5 = segment.a - segment.b;
							num19 += Mathf.Max(0f, Vector3.Dot(vector4, vector5)) / Mathf.Max(0.001f, Mathf.Sqrt(vector5.get_sqrMagnitude() * ray.LengthSqr()));
						}
						if (flag2 && i == 16 && num20 > 0.999f)
						{
							Vector3 vector6 = segment.b - segment.a;
							num19 += Mathf.Max(0f, Vector3.Dot(vector4, vector6)) / Mathf.Max(0.001f, Mathf.Sqrt(vector6.get_sqrMagnitude() * ray.LengthSqr()));
						}
						priority = num18;
						t = num19;
						result = true;
					}
				}
				if (num17 < num12)
				{
					float num22 = vector3.y + num17 - num12;
					if (Mathf.Max(ray.a.y, ray.b.y) > num22 && Mathf.Min(ray.a.y, ray.b.y) < vector3.y)
					{
						Segment2 segment2;
						float num23;
						if (Segment1.Intersect(ray.a.y, ray.b.y, vector3.y, ref num19))
						{
							segment2.a = VectorUtils.XZ(ray.Position(num19));
							num23 = num19;
						}
						else
						{
							segment2.a = VectorUtils.XZ(ray.a);
							num23 = 0f;
						}
						float num24;
						if (Segment1.Intersect(ray.a.y, ray.b.y, num22, ref num19))
						{
							segment2.b = VectorUtils.XZ(ray.Position(num19));
							num24 = num19;
						}
						else
						{
							segment2.b = VectorUtils.XZ(ray.b);
							num24 = 1f;
						}
						num18 = segment2.DistanceSqr(VectorUtils.XZ(vector3), ref num19);
						if (num18 < priority && num18 < num21 * num21)
						{
							num19 = num23 + (num24 - num23) * num19;
							Vector3 vector7 = ray.Position(num19);
							if (flag && i == 1 && num20 < 0.001f)
							{
								Vector3 vector8 = segment.a - segment.b;
								num19 += Mathf.Max(0f, Vector3.Dot(vector7, vector8)) / Mathf.Max(0.001f, Mathf.Sqrt(vector8.get_sqrMagnitude() * ray.LengthSqr()));
							}
							if (flag2 && i == 16 && num20 > 0.999f)
							{
								Vector3 vector9 = segment.b - segment.a;
								num19 += Mathf.Max(0f, Vector3.Dot(vector7, vector9)) / Mathf.Max(0.001f, Mathf.Sqrt(vector9.get_sqrMagnitude() * ray.LengthSqr()));
							}
							priority = num18;
							t = num19;
							result = true;
						}
					}
				}
				segment.a = segment.b;
			}
			priority = Mathf.Max(0f, Mathf.Sqrt(priority) - collisionHalfWidth);
		}
		return result;
	}

	public static ushort FindOwnerBuilding(ushort segmentID, float maxDistance)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		ushort startNode = instance2.m_segments.m_buffer[(int)segmentID].m_startNode;
		ushort endNode = instance2.m_segments.m_buffer[(int)segmentID].m_endNode;
		Vector3 position = instance2.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance2.m_nodes.m_buffer[(int)endNode].m_position;
		Vector3 vector = Vector3.Min(position, position2);
		Vector3 vector2 = Vector3.Max(position, position2);
		int num = Mathf.Max((int)((vector.x - maxDistance) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.z - maxDistance) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + maxDistance) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.z + maxDistance) / 64f + 135f), 269);
		ushort result = 0;
		float num5 = maxDistance * maxDistance;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					Vector3 position3 = instance.m_buildings.m_buffer[(int)num6].m_position;
					float num8 = Mathf.Max(0f, Mathf.Max(position3.x - vector2.x, vector.x - position3.x));
					float num9 = Mathf.Max(0f, Mathf.Max(position3.z - vector2.z, vector.z - position3.z));
					float num10 = num8 * num8 + num9 * num9;
					if (num10 < num5 && instance.m_buildings.m_buffer[(int)num6].ContainsSegment(segmentID))
					{
						return num6;
					}
					num6 = instance.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public void AddTraffic(int amount, int noise)
	{
		this.m_trafficBuffer = (ushort)Mathf.Min((int)this.m_trafficBuffer + amount, 65535);
		this.m_noiseBuffer = (ushort)Mathf.Min((int)this.m_noiseBuffer + noise, 65535);
	}

	public Vector3 GetClosestPosition(Vector3 point)
	{
		Vector3 result;
		Vector3 vector;
		this.GetClosestPositionAndDirection(point, out result, out vector);
		return result;
	}

	public void GetClosestPositionAndDirection(Vector3 point, out Vector3 pos, out Vector3 dir)
	{
		Bezier3 bezier = default(Bezier3);
		bezier.a = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_position;
		bezier.d = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_position;
		bool smoothStart = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
		bool smoothEnd = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
		NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, smoothStart, smoothEnd, out bezier.b, out bezier.c);
		float num = 1E+11f;
		float num2 = 0f;
		Vector3 vector = bezier.a;
		for (int i = 1; i <= 16; i++)
		{
			Vector3 vector2 = bezier.Position((float)i / 16f);
			float num4;
			float num3 = Segment3.DistanceSqr(vector, vector2, point, ref num4);
			if (num3 < num)
			{
				num = num3;
				num2 = ((float)i - 1f + num4) / 16f;
			}
			vector = vector2;
		}
		float num5 = 0.03125f;
		for (int j = 0; j < 4; j++)
		{
			Vector3 vector3 = bezier.Position(Mathf.Max(0f, num2 - num5));
			Vector3 vector4 = bezier.Position(num2);
			Vector3 vector5 = bezier.Position(Mathf.Min(1f, num2 + num5));
			float num7;
			float num6 = Segment3.DistanceSqr(vector3, vector4, point, ref num7);
			float num9;
			float num8 = Segment3.DistanceSqr(vector4, vector5, point, ref num9);
			if (num6 < num8)
			{
				num2 = Mathf.Max(0f, num2 - num5 * (1f - num7));
			}
			else
			{
				num2 = Mathf.Min(1f, num2 + num5 * num9);
			}
			num5 *= 0.5f;
		}
		pos = bezier.Position(num2);
		dir = VectorUtils.NormalizeXZ(bezier.Tangent(num2));
	}

	public bool GetClosestLane(uint refLane, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, out uint laneID, out NetInfo.Lane laneInfo)
	{
		laneID = 0u;
		laneInfo = null;
		if (this.m_flags != NetSegment.Flags.None && this.m_lanes != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			NetInfo info = this.Info;
			if (info.m_lanes != null)
			{
				float num = 0f;
				uint num2 = this.m_lanes;
				int num3 = 0;
				while (num3 < info.m_lanes.Length && num2 != 0u)
				{
					if (num2 == refLane)
					{
						num = info.m_lanes[num3].m_position;
						break;
					}
					num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
					num3++;
				}
				float num4 = 100000f;
				num2 = this.m_lanes;
				int num5 = 0;
				while (num5 < info.m_lanes.Length && num2 != 0u)
				{
					NetInfo.Lane lane = info.m_lanes[num5];
					if (lane.CheckType(laneTypes, vehicleTypes))
					{
						float num6 = Mathf.Abs(lane.m_position - num);
						if (num6 < num4)
						{
							laneID = num2;
							laneInfo = lane;
							num4 = num6;
						}
					}
					num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
					num5++;
				}
			}
		}
		return laneID != 0u;
	}

	public bool GetClosestLane(int refIndex, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, out int laneIndex, out uint laneID)
	{
		laneID = 0u;
		laneIndex = -1;
		if (this.m_flags != NetSegment.Flags.None && this.m_lanes != 0u)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			NetInfo info = this.Info;
			if (info.m_lanes != null && refIndex >= 0 && refIndex < info.m_lanes.Length)
			{
				float position = info.m_lanes[refIndex].m_position;
				float num = 100000f;
				uint num2 = this.m_lanes;
				int num3 = 0;
				while (num3 < info.m_lanes.Length && num2 != 0u)
				{
					NetInfo.Lane lane = info.m_lanes[num3];
					if (lane.CheckType(laneTypes, vehicleTypes))
					{
						float num4 = Mathf.Abs(lane.m_position - position);
						if (num4 < num)
						{
							laneID = num2;
							laneIndex = num3;
							num = num4;
						}
					}
					num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
					num3++;
				}
			}
		}
		return laneID != 0u;
	}

	public bool GetClosestLanePosition(Vector3 point, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, out Vector3 position, out uint laneID, out int laneIndex, out float laneOffset)
	{
		position = point;
		laneID = 0u;
		laneIndex = -1;
		laneOffset = 0f;
		if (this.m_flags != NetSegment.Flags.None && this.m_lanes != 0u)
		{
			NetInfo info = this.Info;
			if (info.m_lanes != null)
			{
				float num = 1E+10f;
				uint num2 = this.m_lanes;
				int num3 = 0;
				while (num3 < info.m_lanes.Length && num2 != 0u)
				{
					NetInfo.Lane lane = info.m_lanes[num3];
					if (lane.CheckType(laneTypes, vehicleTypes))
					{
						Vector3 vector;
						float num4;
						Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num2)].GetClosestPosition(point, out vector, out num4);
						float num5 = Vector3.SqrMagnitude(point - vector);
						if (num5 < num)
						{
							num = num5;
							position = vector;
							laneID = num2;
							laneIndex = num3;
							laneOffset = num4;
						}
					}
					num2 = Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
					num3++;
				}
			}
		}
		return laneIndex != -1;
	}

	public bool GetClosestLanePosition(Vector3 point, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, VehicleInfo.VehicleType stopTypes, out Vector3 position, out uint laneID, out int laneIndex, out float laneOffset)
	{
		position = point;
		laneID = 0u;
		laneIndex = -1;
		laneOffset = 0f;
		if (this.m_flags != NetSegment.Flags.None && this.m_lanes != 0u)
		{
			NetInfo info = this.Info;
			if (info.m_lanes != null)
			{
				float num = 1E+10f;
				uint num2 = this.m_lanes;
				int num3 = 0;
				while (num3 < info.m_lanes.Length && num2 != 0u)
				{
					NetInfo.Lane lane = info.m_lanes[num3];
					if (lane.CheckType(laneTypes, vehicleTypes, stopTypes))
					{
						Vector3 vector;
						float num4;
						Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num2)].GetClosestPosition(point, out vector, out num4);
						float num5 = Vector3.SqrMagnitude(point - vector);
						if (num5 < num)
						{
							num = num5;
							position = vector;
							laneID = num2;
							laneIndex = num3;
							laneOffset = num4;
						}
					}
					num2 = Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
					num3++;
				}
			}
		}
		return laneIndex != -1;
	}

	public bool GetClosestLanePosition(Vector3 point, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, VehicleInfo.VehicleType stopTypes, bool requireConnect, out Vector3 positionA, out int laneIndexA, out float laneOffsetA, out Vector3 positionB, out int laneIndexB, out float laneOffsetB)
	{
		positionA = point;
		laneIndexA = -1;
		laneOffsetA = 0f;
		positionB = point;
		laneIndexB = -1;
		laneOffsetB = 0f;
		if (this.m_flags != NetSegment.Flags.None && this.m_lanes != 0u)
		{
			NetInfo info = this.Info;
			if (info.m_lanes != null)
			{
				float num = 1E+09f;
				float num2 = 1E+09f;
				uint num3 = this.m_lanes;
				int num4 = 0;
				while (num4 < info.m_lanes.Length && num3 != 0u)
				{
					NetInfo.Lane lane = info.m_lanes[num4];
					if (lane.CheckType(laneTypes, vehicleTypes, stopTypes) && (lane.m_allowConnect || !requireConnect))
					{
						Vector3 vector;
						float num5;
						Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num3)].GetClosestPosition(point, out vector, out num5);
						float num6 = Vector3.SqrMagnitude(point - vector);
						if (lane.m_finalDirection == NetInfo.Direction.Backward || lane.m_finalDirection == NetInfo.Direction.AvoidForward)
						{
							if (num6 < num2)
							{
								num2 = num6;
								positionB = vector;
								laneIndexB = num4;
								laneOffsetB = num5;
							}
						}
						else if (num6 < num)
						{
							num = num6;
							positionA = vector;
							laneIndexA = num4;
							laneOffsetA = num5;
						}
					}
					num3 = Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)num3)].m_nextLane;
					num4++;
				}
				if (num2 < num)
				{
					Vector3 vector2 = positionA;
					int num7 = laneIndexA;
					float num8 = laneOffsetA;
					positionA = positionB;
					laneIndexA = laneIndexB;
					laneOffsetA = laneOffsetB;
					positionB = vector2;
					laneIndexB = num7;
					laneOffsetB = num8;
				}
				if (!info.m_canCrossLanes && (vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
				{
					positionB = point;
					laneIndexB = -1;
					laneOffsetB = 0f;
				}
			}
		}
		return laneIndexA != -1;
	}

	public void GetClosestZoneBlock(Vector3 point, ref float distanceSq, ref ushort block)
	{
		if (this.m_blockStartLeft != 0)
		{
			float num = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)this.m_blockStartLeft].PointDistanceSq(point, distanceSq);
			if (num < distanceSq)
			{
				block = this.m_blockStartLeft;
				distanceSq = num;
			}
		}
		if (this.m_blockStartRight != 0)
		{
			float num2 = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)this.m_blockStartRight].PointDistanceSq(point, distanceSq);
			if (num2 < distanceSq)
			{
				block = this.m_blockStartRight;
				distanceSq = num2;
			}
		}
		if (this.m_blockEndLeft != 0)
		{
			float num3 = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)this.m_blockEndLeft].PointDistanceSq(point, distanceSq);
			if (num3 < distanceSq)
			{
				block = this.m_blockEndLeft;
				distanceSq = num3;
			}
		}
		if (this.m_blockEndRight != 0)
		{
			float num4 = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)this.m_blockEndRight].PointDistanceSq(point, distanceSq);
			if (num4 < distanceSq)
			{
				block = this.m_blockEndRight;
				distanceSq = num4;
			}
		}
	}

	public Vector3 GetClosestPosition(Vector3 point, Vector3 direction)
	{
		Bezier3 bezier = default(Bezier3);
		bezier.a = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_position;
		bezier.d = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_position;
		bool smoothStart = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
		bool smoothEnd = (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None;
		NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, smoothStart, smoothEnd, out bezier.b, out bezier.c);
		float num = 1E+11f;
		float num2 = 0f;
		if ((point - bezier.d).get_sqrMagnitude() < (point - bezier.a).get_sqrMagnitude())
		{
			num2 = 1f;
		}
		Vector2 vector = VectorUtils.XZ(point);
		Vector2 vector2 = vector + VectorUtils.XZ(direction);
		Vector2 vector3 = VectorUtils.XZ(bezier.a);
		for (int i = 1; i <= 16; i++)
		{
			Vector2 vector4 = VectorUtils.XZ(bezier.Position((float)i / 16f));
			float num3;
			float num4;
			if (Line2.Intersect(vector3, vector4, vector, vector2, ref num3, ref num4) && num3 >= 0f && num3 <= 1f && Mathf.Abs(num4) < num)
			{
				num = num4;
				num2 = ((float)i - 1f + num3) / 16f;
			}
			vector3 = vector4;
		}
		float num5 = 0.03125f;
		for (int j = 0; j < 4; j++)
		{
			Vector2 vector5 = VectorUtils.XZ(bezier.Position(Mathf.Max(0f, num2 - num5)));
			Vector2 vector6 = VectorUtils.XZ(bezier.Position(num2));
			Vector2 vector7 = VectorUtils.XZ(bezier.Position(Mathf.Min(1f, num2 + num5)));
			float num6;
			float num7;
			bool flag = Line2.Intersect(vector5, vector6, vector, vector2, ref num6, ref num7);
			float num8;
			float num9;
			bool flag2 = Line2.Intersect(vector6, vector7, vector, vector2, ref num8, ref num9);
			if (flag && num6 >= 0f && num6 <= 1f)
			{
				if (flag2 && num8 >= 0f && num8 <= 1f)
				{
					if (Mathf.Abs(num7) < Mathf.Abs(num9))
					{
						num2 = Mathf.Max(0f, num2 - num5 * (1f - num6));
					}
					else
					{
						num2 = Mathf.Min(1f, num2 + num5 * num8);
					}
				}
				else
				{
					num2 = Mathf.Max(0f, num2 - num5 * (1f - num6));
				}
			}
			else if (flag2 && num8 >= 0f && num8 <= 1f)
			{
				num2 = Mathf.Min(1f, num2 + num5 * num8);
			}
			num5 *= 0.5f;
		}
		return bezier.Position(num2);
	}

	public bool IsStraight()
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 position = instance.m_nodes.m_buffer[(int)this.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_position;
		return NetSegment.IsStraight(position, this.m_startDirection, position2, this.m_endDirection);
	}

	public static bool IsStraight(Vector3 startPos, Vector3 startDir, Vector3 endPos, Vector3 endDir)
	{
		Vector3 vector = VectorUtils.NormalizeXZ(endPos - startPos);
		float num = startDir.x * endDir.x + startDir.z * endDir.z;
		float num2 = startDir.x * vector.x + startDir.z * vector.z;
		return num < -0.999f && num2 > 0.999f;
	}

	public static bool IsStraight(Vector3 startPos, Vector3 startDir, Vector3 endPos, Vector3 endDir, out float distance)
	{
		Vector3 vector = VectorUtils.NormalizeXZ(endPos - startPos, ref distance);
		float num = startDir.x * endDir.x + startDir.z * endDir.z;
		float num2 = startDir.x * vector.x + startDir.z * vector.z;
		return num < -0.999f && num2 > 0.999f;
	}

	public static void CalculateMiddlePoints(Vector3 startPos, Vector3 startDir, Vector3 endPos, Vector3 endDir, bool smoothStart, bool smoothEnd, out Vector3 middlePos1, out Vector3 middlePos2)
	{
		float num;
		if (NetSegment.IsStraight(startPos, startDir, endPos, endDir, out num))
		{
			middlePos1 = startPos + startDir * (num * ((!smoothStart) ? 0.15f : 0.3f));
			middlePos2 = endPos + endDir * (num * ((!smoothEnd) ? 0.15f : 0.3f));
		}
		else
		{
			float num2 = startDir.x * endDir.x + startDir.z * endDir.z;
			float num3;
			float num4;
			if (num2 >= -0.999f && Line2.Intersect(VectorUtils.XZ(startPos), VectorUtils.XZ(startPos + startDir), VectorUtils.XZ(endPos), VectorUtils.XZ(endPos + endDir), ref num3, ref num4))
			{
				num3 = Mathf.Clamp(num3, num * 0.1f, num);
				num4 = Mathf.Clamp(num4, num * 0.1f, num);
				num = num3 + num4;
				middlePos1 = startPos + startDir * Mathf.Min(num3, num * 0.3f);
				middlePos2 = endPos + endDir * Mathf.Min(num4, num * 0.3f);
			}
			else
			{
				middlePos1 = startPos + startDir * (num * 0.3f);
				middlePos2 = endPos + endDir * (num * 0.3f);
			}
		}
	}

	public static void CalculateMiddlePoints(Vector3 startPos, Vector3 startDir, Vector3 endPos, Vector3 endDir, bool smoothStart, bool smoothEnd, out Vector3 middlePos1, out Vector3 middlePos2, out float distance)
	{
		if (NetSegment.IsStraight(startPos, startDir, endPos, endDir, out distance))
		{
			middlePos1 = startPos + startDir * (distance * ((!smoothStart) ? 0.15f : 0.3f));
			middlePos2 = endPos + endDir * (distance * ((!smoothEnd) ? 0.15f : 0.3f));
		}
		else
		{
			float num = startDir.x * endDir.x + startDir.z * endDir.z;
			float num2;
			float num3;
			if (num >= -0.999f && Line2.Intersect(VectorUtils.XZ(startPos), VectorUtils.XZ(startPos + startDir), VectorUtils.XZ(endPos), VectorUtils.XZ(endPos + endDir), ref num2, ref num3))
			{
				num2 = Mathf.Clamp(num2, distance * 0.1f, distance);
				num3 = Mathf.Clamp(num3, distance * 0.1f, distance);
				float num4 = num2 + num3;
				middlePos1 = startPos + startDir * Mathf.Min(num2, num4 * 0.3f);
				middlePos2 = endPos + endDir * Mathf.Min(num3, num4 * 0.3f);
			}
			else
			{
				middlePos1 = startPos + startDir * (distance * 0.3f);
				middlePos2 = endPos + endDir * (distance * 0.3f);
			}
		}
	}

	public static Matrix4x4 CalculateControlMatrix(Vector3 startPos, Vector3 middlePos1, Vector3 middlePos2, Vector3 endPos, Vector3 startPosB, Vector3 middlePosB1, Vector3 middlePosB2, Vector3 endPosB, Vector3 transform, float vScale)
	{
		Vector3 vector = middlePos1 - startPos;
		float num = Vector3.Dot(startPos - startPosB, vector) / Mathf.Max(0.001f, vector.get_magnitude()) * vScale * 0.5f;
		vector = middlePos2 - endPos;
		float num2 = Vector3.Dot(endPos - endPosB, vector) / Mathf.Max(0.001f, vector.get_magnitude()) * vScale * 0.5f;
		float num3 = Vector3.Distance(startPos + startPosB, middlePos1 + middlePosB1) * vScale * 0.5f;
		float num4 = Vector3.Distance(middlePos1 + middlePosB1, middlePos2 + middlePosB2) * vScale * 0.5f;
		float num5 = Vector3.Distance(middlePos2 + middlePosB2, endPos + endPosB) * vScale * 0.5f;
		float num6 = num3 + num4 + num5;
		float num7 = (num6 - num - num2) * Mathf.Round(num6 * 4f) / Mathf.Max(0.01f, num6 * num6 * 4f);
		num3 *= num7;
		num4 *= num7;
		num5 *= num7;
		Matrix4x4 result = default(Matrix4x4);
		result.SetColumn(0, startPos - transform);
		result.SetColumn(1, middlePos1 - transform);
		result.SetColumn(2, middlePos2 - transform);
		result.SetColumn(3, endPos - transform);
		result.m30 = num;
		result.m31 = num + num3;
		result.m32 = num + num3 + num4;
		result.m33 = num + num3 + num4 + num5;
		return result;
	}

	public static Matrix4x4 CalculateControlMatrix(Vector3 startPos, Vector3 middlePos1, Vector3 middlePos2, Vector3 endPos, Vector3 transform, float vScale)
	{
		float num = Vector3.Distance(startPos, endPos);
		float num2 = Vector3.Distance(startPos, middlePos1) * vScale;
		float num3 = Vector3.Distance(middlePos1, middlePos2) * vScale;
		float num4 = Vector3.Distance(middlePos2, endPos) * vScale;
		float num5 = num2 + num3 + num4;
		if (num5 != 0f)
		{
			float num6 = (num * vScale * 0.667f + num5 * 0.333f) / num5;
			num2 *= num6;
			num3 *= num6;
			num4 *= num6;
			num5 *= num6;
		}
		float num7 = num5 * Mathf.Round(num5 * 4f) / Mathf.Max(0.01f, num5 * num5 * 4f);
		num2 *= num7;
		num3 *= num7;
		num4 *= num7;
		Matrix4x4 result = default(Matrix4x4);
		result.SetColumn(0, startPos - transform);
		result.SetColumn(1, middlePos1 - transform);
		result.SetColumn(2, middlePos2 - transform);
		result.SetColumn(3, endPos - transform);
		result.m30 = 0f;
		result.m31 = num2;
		result.m32 = num2 + num3;
		result.m33 = num2 + num3 + num4;
		return result;
	}

	public void UpdateStartSegments(ushort segmentID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		ItemClass connectionClass = info.GetConnectionClass();
		float num = -4f;
		float num2 = -4f;
		ushort startLeftSegment = 0;
		ushort startRightSegment = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)this.m_startNode].GetSegment(i);
			if (segment != 0 && segment != segmentID)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
				if (info2 != null)
				{
					ItemClass connectionClass2 = info2.GetConnectionClass();
					if (connectionClass.m_service == connectionClass2.m_service)
					{
						Vector3 vector;
						if (this.m_startNode == instance.m_segments.m_buffer[(int)segment].m_startNode)
						{
							vector = instance.m_segments.m_buffer[(int)segment].m_startDirection;
						}
						else
						{
							vector = instance.m_segments.m_buffer[(int)segment].m_endDirection;
						}
						float num3 = this.m_startDirection.x * vector.x + this.m_startDirection.z * vector.z;
						if (vector.z * this.m_startDirection.x - vector.x * this.m_startDirection.z < 0f)
						{
							if (num3 > num)
							{
								num = num3;
								startLeftSegment = segment;
							}
							num3 = -2f - num3;
							if (num3 > num2)
							{
								num2 = num3;
								startRightSegment = segment;
							}
						}
						else
						{
							if (num3 > num2)
							{
								num2 = num3;
								startRightSegment = segment;
							}
							num3 = -2f - num3;
							if (num3 > num)
							{
								num = num3;
								startLeftSegment = segment;
							}
						}
					}
				}
			}
		}
		this.m_startLeftSegment = startLeftSegment;
		this.m_startRightSegment = startRightSegment;
	}

	public void UpdateEndSegments(ushort segmentID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		ItemClass connectionClass = info.GetConnectionClass();
		float num = -4f;
		float num2 = -4f;
		ushort endLeftSegment = 0;
		ushort endRightSegment = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)this.m_endNode].GetSegment(i);
			if (segment != 0 && segment != segmentID)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
				if (info2 != null)
				{
					ItemClass connectionClass2 = info2.GetConnectionClass();
					if (connectionClass.m_service == connectionClass2.m_service)
					{
						Vector3 vector;
						if (this.m_endNode == instance.m_segments.m_buffer[(int)segment].m_startNode)
						{
							vector = instance.m_segments.m_buffer[(int)segment].m_startDirection;
						}
						else
						{
							vector = instance.m_segments.m_buffer[(int)segment].m_endDirection;
						}
						float num3 = this.m_endDirection.x * vector.x + this.m_endDirection.z * vector.z;
						if (vector.z * this.m_endDirection.x - vector.x * this.m_endDirection.z < 0f)
						{
							if (num3 > num)
							{
								num = num3;
								endLeftSegment = segment;
							}
							num3 = -2f - num3;
							if (num3 > num2)
							{
								num2 = num3;
								endRightSegment = segment;
							}
						}
						else
						{
							if (num3 > num2)
							{
								num2 = num3;
								endRightSegment = segment;
							}
							num3 = -2f - num3;
							if (num3 > num)
							{
								num = num3;
								endLeftSegment = segment;
							}
						}
					}
				}
			}
		}
		this.m_endLeftSegment = endLeftSegment;
		this.m_endRightSegment = endRightSegment;
	}

	public void GetLeftAndRightSegments(ushort nodeID, out ushort leftSegment, out ushort rightSegment)
	{
		if (nodeID == this.m_startNode)
		{
			leftSegment = this.m_startLeftSegment;
			rightSegment = this.m_startRightSegment;
		}
		else if (nodeID == this.m_endNode)
		{
			leftSegment = this.m_endLeftSegment;
			rightSegment = this.m_endRightSegment;
		}
		else
		{
			leftSegment = 0;
			rightSegment = 0;
		}
	}

	public ushort GetLeftSegment(ushort nodeID)
	{
		if (nodeID == this.m_startNode)
		{
			return this.m_startLeftSegment;
		}
		if (nodeID == this.m_endNode)
		{
			return this.m_endLeftSegment;
		}
		return 0;
	}

	public ushort GetRightSegment(ushort nodeID)
	{
		if (nodeID == this.m_startNode)
		{
			return this.m_startRightSegment;
		}
		if (nodeID == this.m_endNode)
		{
			return this.m_endRightSegment;
		}
		return 0;
	}

	public void GetLeftAndRightLanes(ushort nodeID, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, int refIndex, bool centerOnly, out int leftIndex, out int rightIndex, out uint leftLane, out uint rightLane)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = this.Info;
		int num = info.m_lanes.Length;
		uint num2 = this.m_lanes;
		leftIndex = -1;
		rightIndex = -1;
		leftLane = 0u;
		rightLane = 0u;
		if (refIndex >= 0 && refIndex < num)
		{
			float position = info.m_lanes[refIndex].m_position;
			float num3 = 100000f;
			float num4 = 100000f;
			int num5 = 0;
			while (num5 < num && num2 != 0u)
			{
				NetInfo.Lane lane = info.m_lanes[num5];
				if (num5 != refIndex && lane.CheckType(laneTypes, vehicleTypes) && (lane.m_centerPlatform || !centerOnly) && !lane.m_elevated)
				{
					float num6 = position - lane.m_position;
					if (num6 > 0.1f && num6 < num3)
					{
						leftIndex = num5;
						leftLane = num2;
						num3 = num6;
					}
					float num7 = lane.m_position - position;
					if (num7 > 0.1f && num7 < num4)
					{
						rightIndex = num5;
						rightLane = num2;
						num4 = num7;
					}
				}
				num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
				num5++;
			}
		}
		else
		{
			float num8 = -100000f;
			float num9 = -100000f;
			int num10 = 0;
			while (num10 < num && num2 != 0u)
			{
				NetInfo.Lane lane2 = info.m_lanes[num10];
				if (num10 != refIndex && lane2.CheckType(laneTypes, vehicleTypes) && (lane2.m_centerPlatform || !centerOnly) && !lane2.m_elevated)
				{
					float num11 = -lane2.m_position;
					if (num11 > num8)
					{
						leftIndex = num10;
						leftLane = num2;
						num8 = num11;
					}
					float position2 = lane2.m_position;
					if (position2 > num9)
					{
						rightIndex = num10;
						rightLane = num2;
						num9 = position2;
					}
				}
				num2 = instance.m_lanes.m_buffer[(int)((UIntPtr)num2)].m_nextLane;
				num10++;
			}
		}
		if (nodeID == this.m_startNode != ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
		{
			int num12 = leftIndex;
			leftIndex = rightIndex;
			rightIndex = num12;
			uint num13 = leftLane;
			leftLane = rightLane;
			rightLane = num13;
		}
	}

	public Vector3 GetDirection(ushort nodeID)
	{
		if (this.m_startNode == nodeID)
		{
			return this.m_startDirection;
		}
		return this.m_endDirection;
	}

	public ushort GetOtherNode(ushort nodeID)
	{
		if (this.m_startNode == nodeID)
		{
			return this.m_endNode;
		}
		return this.m_startNode;
	}

	public ushort GetSharedNode(ushort otherSegmentID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort startNode = instance.m_segments.m_buffer[(int)otherSegmentID].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)otherSegmentID].m_endNode;
		if (this.m_startNode == startNode || this.m_startNode == endNode)
		{
			return this.m_startNode;
		}
		if (this.m_endNode == startNode || this.m_endNode == endNode)
		{
			return this.m_endNode;
		}
		return 0;
	}

	public void CalculateCorner(ushort segmentID, bool heightOffset, bool start, bool leftSide, out Vector3 cornerPos, out Vector3 cornerDirection, out bool smooth)
	{
		NetInfo info = this.Info;
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort num = (!start) ? this.m_endNode : this.m_startNode;
		ushort num2 = (!start) ? this.m_startNode : this.m_endNode;
		Vector3 position = instance.m_nodes.m_buffer[(int)num].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)num2].m_position;
		Vector3 startDir = (!start) ? this.m_endDirection : this.m_startDirection;
		Vector3 endDir = (!start) ? this.m_startDirection : this.m_endDirection;
		NetSegment.CalculateCorner(info, position, position2, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segmentID, num, heightOffset, leftSide, out cornerPos, out cornerDirection, out smooth);
	}

	public static void CalculateCorner(NetInfo info, Vector3 startPos, Vector3 endPos, Vector3 startDir, Vector3 endDir, NetInfo extraInfo1, Vector3 extraEndPos1, Vector3 extraStartDir1, Vector3 extraEndDir1, NetInfo extraInfo2, Vector3 extraEndPos2, Vector3 extraStartDir2, Vector3 extraEndDir2, ushort ignoreSegmentID, ushort startNodeID, bool heightOffset, bool leftSide, out Vector3 cornerPos, out Vector3 cornerDirection, out bool smooth)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Bezier3 bezier = default(Bezier3);
		Bezier3 bezier2 = default(Bezier3);
		NetNode.Flags flags = NetNode.Flags.End;
		ushort num = 0;
		if (startNodeID != 0)
		{
			flags = instance.m_nodes.m_buffer[(int)startNodeID].m_flags;
			num = instance.m_nodes.m_buffer[(int)startNodeID].m_building;
		}
		cornerDirection = startDir;
		float num2 = (!leftSide) ? (-info.m_halfWidth) : info.m_halfWidth;
		smooth = ((flags & NetNode.Flags.Middle) != NetNode.Flags.None);
		if (extraInfo1 != null)
		{
			if ((flags & NetNode.Flags.End) != NetNode.Flags.None && info.IsCombatible(extraInfo1) && extraInfo2 == null)
			{
				if (startDir.x * extraStartDir1.x + startDir.z * extraStartDir1.z < -0.999f)
				{
					flags = ((flags & ~NetNode.Flags.End) | NetNode.Flags.Middle);
				}
				else
				{
					flags = ((flags & ~NetNode.Flags.End) | NetNode.Flags.Bend);
				}
			}
			else
			{
				flags = ((flags & ~(NetNode.Flags.Middle | NetNode.Flags.Bend)) | NetNode.Flags.Junction);
			}
		}
		if ((flags & NetNode.Flags.Middle) != NetNode.Flags.None)
		{
			int num3 = (extraInfo1 == null) ? 0 : -1;
			int num4 = (startNodeID == 0) ? 0 : 8;
			int i = num3;
			while (i < num4)
			{
				Vector3 vector;
				if (i == -1)
				{
					vector = extraStartDir1;
				}
				else
				{
					ushort segment = instance.m_nodes.m_buffer[(int)startNodeID].GetSegment(i);
					if (segment == 0 || segment == ignoreSegmentID)
					{
						i++;
						continue;
					}
					ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
					if (startNodeID != startNode)
					{
						vector = instance.m_segments.m_buffer[(int)segment].m_endDirection;
					}
					else
					{
						vector = instance.m_segments.m_buffer[(int)segment].m_startDirection;
					}
				}
				cornerDirection = VectorUtils.NormalizeXZ(cornerDirection - vector);
				break;
			}
		}
		Vector3 vector2 = Vector3.Cross(cornerDirection, Vector3.get_up()).get_normalized();
		if (info.m_twistSegmentEnds)
		{
			if (num != 0)
			{
				float angle = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num].m_angle;
				Vector3 vector3;
				vector3..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
				vector2 = ((Vector3.Dot(vector2, vector3) < 0f) ? (-vector3) : vector3);
			}
			else if ((flags & NetNode.Flags.Junction) != NetNode.Flags.None && startNodeID != 0)
			{
				Vector3 zero = Vector3.get_zero();
				int num5 = 0;
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = instance.m_nodes.m_buffer[(int)startNodeID].GetSegment(j);
					if (segment2 != 0 && segment2 != ignoreSegmentID)
					{
						if ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
						{
							Vector3 vector4;
							if (instance.m_segments.m_buffer[(int)segment2].m_startNode != startNodeID)
							{
								vector4 = instance.m_segments.m_buffer[(int)segment2].m_endDirection;
							}
							else
							{
								vector4 = instance.m_segments.m_buffer[(int)segment2].m_startDirection;
							}
							zero..ctor(vector4.z, 0f, -vector4.x);
							num5++;
						}
					}
				}
				if (num5 == 1)
				{
					vector2 = ((Vector3.Dot(vector2, zero) < 0f) ? (-zero) : zero);
				}
			}
		}
		bezier.a = startPos + vector2 * num2;
		bezier2.a = startPos - vector2 * num2;
		cornerPos = bezier.a;
		if (((flags & NetNode.Flags.Junction) != NetNode.Flags.None && info.m_clipSegmentEnds) || (flags & (NetNode.Flags.Bend | NetNode.Flags.Outside)) != NetNode.Flags.None)
		{
			vector2 = Vector3.Cross(endDir, Vector3.get_up()).get_normalized();
			bezier.d = endPos - vector2 * num2;
			bezier2.d = endPos + vector2 * num2;
			NetSegment.CalculateMiddlePoints(bezier.a, cornerDirection, bezier.d, endDir, false, false, out bezier.b, out bezier.c);
			NetSegment.CalculateMiddlePoints(bezier2.a, cornerDirection, bezier2.d, endDir, false, false, out bezier2.b, out bezier2.c);
			Bezier2 bezier3 = Bezier2.XZ(bezier);
			Bezier2 bezier4 = Bezier2.XZ(bezier2);
			float num6 = -1f;
			float num7 = -1f;
			bool flag = false;
			int num8 = (extraInfo1 == null) ? 0 : ((extraInfo2 == null) ? -1 : -2);
			int num9 = (startNodeID == 0) ? 0 : 8;
			float num10 = info.m_halfWidth * 0.5f;
			int num11 = 0;
			int k = num8;
			while (k < num9)
			{
				NetInfo netInfo;
				if (k == -2)
				{
					netInfo = extraInfo2;
					Vector3 vector5 = extraStartDir2;
					if (!(extraEndPos2 == endPos) || !(extraEndDir2 == endDir))
					{
						goto IL_5E9;
					}
				}
				else if (k == -1)
				{
					netInfo = extraInfo1;
					Vector3 vector5 = extraStartDir1;
					if (!(extraEndPos1 == endPos) || !(extraEndDir1 == endDir))
					{
						goto IL_5E9;
					}
				}
				else
				{
					ushort segment3 = instance.m_nodes.m_buffer[(int)startNodeID].GetSegment(k);
					if (segment3 != 0 && segment3 != ignoreSegmentID)
					{
						netInfo = instance.m_segments.m_buffer[(int)segment3].Info;
						Vector3 vector5 = instance.m_segments.m_buffer[(int)segment3].GetDirection(startNodeID);
						goto IL_5E9;
					}
				}
				IL_6A0:
				k++;
				continue;
				IL_5E9:
				if (netInfo == null)
				{
					goto IL_6A0;
				}
				if (info.m_clipSegmentEnds != netInfo.m_clipSegmentEnds)
				{
					goto IL_6A0;
				}
				if (netInfo.m_netAI.GetSnapElevation() > info.m_netAI.GetSnapElevation())
				{
					float num12 = 0.01f - Mathf.Min(info.m_maxTurnAngleCos, netInfo.m_maxTurnAngleCos);
					Vector3 vector5;
					float num13 = vector5.x * startDir.x + vector5.z * startDir.z;
					if ((info.m_vehicleTypes & netInfo.m_vehicleTypes) == VehicleInfo.VehicleType.None || num13 >= num12)
					{
						goto IL_6A0;
					}
				}
				num10 = Mathf.Max(num10, netInfo.m_halfWidth * 0.5f);
				num11++;
				goto IL_6A0;
			}
			if (num11 >= 1 || (flags & NetNode.Flags.Outside) != NetNode.Flags.None)
			{
				int l = num8;
				while (l < num9)
				{
					NetInfo netInfo2;
					Vector3 vector6;
					Vector3 vector7;
					Vector3 vector8;
					if (l == -2)
					{
						netInfo2 = extraInfo2;
						vector6 = extraEndPos2;
						vector7 = extraStartDir2;
						vector8 = extraEndDir2;
						if (!(vector6 == endPos) || !(vector8 == endDir))
						{
							goto IL_82E;
						}
					}
					else if (l == -1)
					{
						netInfo2 = extraInfo1;
						vector6 = extraEndPos1;
						vector7 = extraStartDir1;
						vector8 = extraEndDir1;
						if (!(vector6 == endPos) || !(vector8 == endDir))
						{
							goto IL_82E;
						}
					}
					else
					{
						ushort segment4 = instance.m_nodes.m_buffer[(int)startNodeID].GetSegment(l);
						if (segment4 != 0 && segment4 != ignoreSegmentID)
						{
							ushort startNode2 = instance.m_segments.m_buffer[(int)segment4].m_startNode;
							ushort num14 = instance.m_segments.m_buffer[(int)segment4].m_endNode;
							vector7 = instance.m_segments.m_buffer[(int)segment4].m_startDirection;
							vector8 = instance.m_segments.m_buffer[(int)segment4].m_endDirection;
							if (startNodeID != startNode2)
							{
								ushort num15 = startNode2;
								num14 = num15;
								Vector3 vector9 = vector7;
								vector7 = vector8;
								vector8 = vector9;
							}
							netInfo2 = instance.m_segments.m_buffer[(int)segment4].Info;
							vector6 = instance.m_nodes.m_buffer[(int)num14].m_position;
							goto IL_82E;
						}
					}
					IL_C60:
					l++;
					continue;
					IL_82E:
					if (netInfo2 == null)
					{
						goto IL_C60;
					}
					if (info.m_clipSegmentEnds != netInfo2.m_clipSegmentEnds)
					{
						goto IL_C60;
					}
					if (netInfo2.m_netAI.GetSnapElevation() > info.m_netAI.GetSnapElevation())
					{
						float num16 = 0.01f - Mathf.Min(info.m_maxTurnAngleCos, netInfo2.m_maxTurnAngleCos);
						float num17 = vector7.x * startDir.x + vector7.z * startDir.z;
						if ((info.m_vehicleTypes & netInfo2.m_vehicleTypes) == VehicleInfo.VehicleType.None || num17 >= num16)
						{
							goto IL_C60;
						}
					}
					if (vector7.z * cornerDirection.x - vector7.x * cornerDirection.z > 0f == leftSide)
					{
						Bezier3 bezier5 = default(Bezier3);
						float num18 = Mathf.Max(num10, netInfo2.m_halfWidth);
						if (!leftSide)
						{
							num18 = -num18;
						}
						vector2 = Vector3.Cross(vector7, Vector3.get_up()).get_normalized();
						bezier5.a = startPos - vector2 * num18;
						vector2 = Vector3.Cross(vector8, Vector3.get_up()).get_normalized();
						bezier5.d = vector6 + vector2 * num18;
						NetSegment.CalculateMiddlePoints(bezier5.a, vector7, bezier5.d, vector8, false, false, out bezier5.b, out bezier5.c);
						Bezier2 bezier6 = Bezier2.XZ(bezier5);
						float num19;
						float num20;
						if (bezier3.Intersect(bezier6, ref num19, ref num20, 6))
						{
							num6 = Mathf.Max(num6, num19);
						}
						else if (bezier3.Intersect(bezier6.a, bezier6.a - VectorUtils.XZ(vector7) * 16f, ref num19, ref num20, 6))
						{
							num6 = Mathf.Max(num6, num19);
						}
						else if (bezier6.Intersect(bezier3.d + (bezier3.d - bezier4.d) * 0.01f, bezier4.d, ref num19, ref num20, 6))
						{
							num6 = Mathf.Max(num6, 1f);
						}
						float num21 = cornerDirection.x * vector7.x + cornerDirection.z * vector7.z;
						if (num21 >= -0.75f)
						{
							flag = true;
						}
						goto IL_C60;
					}
					Bezier3 bezier7 = default(Bezier3);
					float num22 = cornerDirection.x * vector7.x + cornerDirection.z * vector7.z;
					if (num22 >= 0f)
					{
						vector7.x -= cornerDirection.x * num22 * 2f;
						vector7.z -= cornerDirection.z * num22 * 2f;
					}
					float num23 = Mathf.Max(num10, netInfo2.m_halfWidth);
					if (!leftSide)
					{
						num23 = -num23;
					}
					vector2 = Vector3.Cross(vector7, Vector3.get_up()).get_normalized();
					bezier7.a = startPos + vector2 * num23;
					vector2 = Vector3.Cross(vector8, Vector3.get_up()).get_normalized();
					bezier7.d = vector6 - vector2 * num23;
					NetSegment.CalculateMiddlePoints(bezier7.a, vector7, bezier7.d, vector8, false, false, out bezier7.b, out bezier7.c);
					Bezier2 bezier8 = Bezier2.XZ(bezier7);
					float num24;
					float num25;
					if (bezier4.Intersect(bezier8, ref num24, ref num25, 6))
					{
						num7 = Mathf.Max(num7, num24);
						goto IL_C60;
					}
					if (bezier4.Intersect(bezier8.a, bezier8.a - VectorUtils.XZ(vector7) * 16f, ref num24, ref num25, 6))
					{
						num7 = Mathf.Max(num7, num24);
						goto IL_C60;
					}
					if (bezier8.Intersect(bezier3.d, bezier4.d + (bezier4.d - bezier3.d) * 0.01f, ref num24, ref num25, 6))
					{
						num7 = Mathf.Max(num7, 1f);
						goto IL_C60;
					}
					goto IL_C60;
				}
				if ((flags & NetNode.Flags.Junction) != NetNode.Flags.None)
				{
					if (!flag)
					{
						num6 = Mathf.Max(num6, num7);
					}
				}
				else if ((flags & NetNode.Flags.Bend) != NetNode.Flags.None && !flag)
				{
					num6 = Mathf.Max(num6, num7);
				}
				if ((flags & NetNode.Flags.Outside) != NetNode.Flags.None)
				{
					float num26 = 8640f;
					Vector2 vector10;
					vector10..ctor(-num26, -num26);
					Vector2 vector11;
					vector11..ctor(-num26, num26);
					Vector2 vector12;
					vector12..ctor(num26, num26);
					Vector2 vector13;
					vector13..ctor(num26, -num26);
					float num27;
					float num28;
					if (bezier3.Intersect(vector10, vector11, ref num27, ref num28, 6))
					{
						num6 = Mathf.Max(num6, num27);
					}
					if (bezier3.Intersect(vector11, vector12, ref num27, ref num28, 6))
					{
						num6 = Mathf.Max(num6, num27);
					}
					if (bezier3.Intersect(vector12, vector13, ref num27, ref num28, 6))
					{
						num6 = Mathf.Max(num6, num27);
					}
					if (bezier3.Intersect(vector13, vector10, ref num27, ref num28, 6))
					{
						num6 = Mathf.Max(num6, num27);
					}
					num6 = Mathf.Clamp01(num6);
				}
				else
				{
					if (num6 < 0f)
					{
						if (info.m_halfWidth < 4f)
						{
							num6 = 0f;
						}
						else
						{
							num6 = bezier3.Travel(0f, 8f);
						}
					}
					float num29 = info.m_minCornerOffset;
					if ((flags & (NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward)) != NetNode.Flags.None)
					{
						num29 = Mathf.Max(num29, 8f);
					}
					num6 = Mathf.Clamp01(num6);
					float num30 = VectorUtils.LengthXZ(bezier.Position(num6) - bezier.a);
					num6 = bezier3.Travel(num6, Mathf.Max(num29 - num30, 2f));
					if (info.m_straightSegmentEnds)
					{
						if (num7 < 0f)
						{
							if (info.m_halfWidth < 4f)
							{
								num7 = 0f;
							}
							else
							{
								num7 = bezier4.Travel(0f, 8f);
							}
						}
						num7 = Mathf.Clamp01(num7);
						num30 = VectorUtils.LengthXZ(bezier2.Position(num7) - bezier2.a);
						num7 = bezier4.Travel(num7, Mathf.Max(info.m_minCornerOffset - num30, 2f));
						num6 = Mathf.Max(num6, num7);
					}
				}
				float y = cornerDirection.y;
				cornerDirection = bezier.Tangent(num6);
				cornerDirection.y = 0f;
				cornerDirection.Normalize();
				if (!info.m_flatJunctions)
				{
					cornerDirection.y = y;
				}
				cornerPos = bezier.Position(num6);
				cornerPos.y = startPos.y;
			}
		}
		else if ((flags & NetNode.Flags.Junction) != NetNode.Flags.None && info.m_minCornerOffset >= 0.01f)
		{
			vector2 = Vector3.Cross(endDir, Vector3.get_up()).get_normalized();
			bezier.d = endPos - vector2 * num2;
			bezier2.d = endPos + vector2 * num2;
			NetSegment.CalculateMiddlePoints(bezier.a, cornerDirection, bezier.d, endDir, false, false, out bezier.b, out bezier.c);
			NetSegment.CalculateMiddlePoints(bezier2.a, cornerDirection, bezier2.d, endDir, false, false, out bezier2.b, out bezier2.c);
			Bezier2 bezier9 = Bezier2.XZ(bezier);
			Bezier2 bezier10 = Bezier2.XZ(bezier2);
			float num31;
			if (info.m_halfWidth < 4f)
			{
				num31 = 0f;
			}
			else
			{
				num31 = bezier9.Travel(0f, 8f);
			}
			num31 = Mathf.Clamp01(num31);
			float num32 = VectorUtils.LengthXZ(bezier.Position(num31) - bezier.a);
			num31 = bezier9.Travel(num31, Mathf.Max(info.m_minCornerOffset - num32, 2f));
			if (info.m_straightSegmentEnds)
			{
				float num33;
				if (info.m_halfWidth < 4f)
				{
					num33 = 0f;
				}
				else
				{
					num33 = bezier10.Travel(0f, 8f);
				}
				num33 = Mathf.Clamp01(num33);
				num32 = VectorUtils.LengthXZ(bezier2.Position(num33) - bezier2.a);
				num33 = bezier10.Travel(num33, Mathf.Max(info.m_minCornerOffset - num32, 2f));
				num31 = Mathf.Max(num31, num33);
			}
			float y2 = cornerDirection.y;
			cornerDirection = bezier.Tangent(num31);
			cornerDirection.y = 0f;
			cornerDirection.Normalize();
			if (!info.m_flatJunctions)
			{
				cornerDirection.y = y2;
			}
			cornerPos = bezier.Position(num31);
			cornerPos.y = startPos.y;
		}
		if (heightOffset && startNodeID != 0)
		{
			cornerPos.y += (float)instance.m_nodes.m_buffer[(int)startNodeID].m_heightOffset * 0.015625f;
		}
	}

	public bool CalculateGroupData(ushort segmentID, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		bool flag = false;
		NetInfo info = this.Info;
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer && Notification.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
		{
			result = true;
		}
		if (info.m_hasForwardVehicleLanes != info.m_hasBackwardVehicleLanes && layer == Singleton<NetManager>.get_instance().m_arrowLayer && NetSegment.CalculateArrowGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
		{
			result = true;
		}
		if (info.m_lanes != null)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			bool invert;
			NetNode.Flags flags;
			NetNode.Flags flags2;
			if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				invert = true;
				flags = instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags;
				flags2 = instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags;
			}
			else
			{
				invert = false;
				flags = instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags;
				flags2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags;
			}
			bool destroyed = (this.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None;
			uint num = this.m_lanes;
			int num2 = 0;
			while (num2 < info.m_lanes.Length && num != 0u)
			{
				if (instance.m_lanes.m_buffer[(int)((UIntPtr)num)].CalculateGroupData(num, info.m_lanes[num2], destroyed, flags, flags2, invert, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays, ref flag))
				{
					result = true;
				}
				num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
				num2++;
			}
		}
		if ((info.m_netLayers & 1 << layer) != 0)
		{
			bool flag2 = info.m_segments != null && info.m_segments.Length != 0;
			if (flag2 || flag)
			{
				result = true;
				if (flag2)
				{
					for (int i = 0; i < info.m_segments.Length; i++)
					{
						NetInfo.Segment segment = info.m_segments[i];
						bool flag3 = false;
						if (segment.m_layer == layer && segment.CheckFlags(this.m_flags, out flag3) && segment.m_combinedLod != null)
						{
							NetSegment.CalculateGroupData(segment, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
						}
					}
				}
			}
		}
		return result;
	}

	public static void CalculateGroupData(NetInfo.Segment segmentInfo, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		RenderGroup.MeshData data = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		vertexCount += data.m_vertices.Length;
		triangleCount += data.m_triangles.Length;
		objectCount++;
		vertexArrays |= (data.VertexArrayMask() | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Uvs4);
	}

	public static bool CalculateArrowGroupData(ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		vertexCount += 4;
		triangleCount += 6;
		objectCount++;
		vertexArrays |= (RenderGroup.VertexArrays.Vertices | RenderGroup.VertexArrays.Normals | RenderGroup.VertexArrays.Uvs);
		return true;
	}

	public void PopulateGroupData(ushort segmentID, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		bool flag = false;
		NetInfo info = this.Info;
		NetManager instance = Singleton<NetManager>.get_instance();
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer)
		{
			Vector3 middlePosition = this.m_middlePosition;
			middlePosition.y += info.m_maxHeight;
			Notification.PopulateGroupData(this.m_problems, middlePosition, 1f, groupX, groupZ, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
		if (info.m_hasForwardVehicleLanes != info.m_hasBackwardVehicleLanes && layer == Singleton<NetManager>.get_instance().m_arrowLayer)
		{
			Bezier3 bezier = default(Bezier3);
			bezier.a = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_startNode].m_position;
			bezier.d = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)this.m_endNode].m_position;
			NetSegment.CalculateMiddlePoints(bezier.a, this.m_startDirection, bezier.d, this.m_endDirection, true, true, out bezier.b, out bezier.c);
			Vector3 pos = bezier.Position(0.5f);
			pos.y += info.m_netAI.GetSnapElevation();
			Vector3 vector = VectorUtils.NormalizeXZ(bezier.Tangent(0.5f)) * (4f + info.m_halfWidth * 0.5f);
			if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None == info.m_hasForwardVehicleLanes)
			{
				vector = -vector;
			}
			NetSegment.PopulateArrowGroupData(pos, vector, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
		if (info.m_lanes != null)
		{
			bool invert;
			NetNode.Flags flags;
			NetNode.Flags flags2;
			if ((this.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
			{
				invert = true;
				flags = instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags;
				flags2 = instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags;
			}
			else
			{
				invert = false;
				flags = instance.m_nodes.m_buffer[(int)this.m_startNode].m_flags;
				flags2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_flags;
			}
			bool terrainHeight = info.m_segments == null || info.m_segments.Length == 0;
			float startAngle = (float)this.m_cornerAngleStart * 0.0245436933f;
			float endAngle = (float)this.m_cornerAngleEnd * 0.0245436933f;
			bool destroyed = (this.m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None;
			uint num = this.m_lanes;
			int num2 = 0;
			while (num2 < info.m_lanes.Length && num != 0u)
			{
				instance.m_lanes.m_buffer[(int)((UIntPtr)num)].PopulateGroupData(segmentID, num, info.m_lanes[num2], destroyed, flags, flags2, startAngle, endAngle, invert, terrainHeight, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance, ref flag);
				num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
				num2++;
			}
		}
		if ((info.m_netLayers & 1 << layer) != 0)
		{
			bool flag2 = info.m_segments != null && info.m_segments.Length != 0;
			if (flag2 || flag)
			{
				min = Vector3.Min(min, this.m_bounds.get_min());
				max = Vector3.Max(max, this.m_bounds.get_max());
				maxRenderDistance = Mathf.Max(maxRenderDistance, 30000f);
				maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
				if (flag2)
				{
					float vScale = info.m_netAI.GetVScale();
					Vector3 vector2;
					Vector3 startDir;
					bool smoothStart;
					this.CalculateCorner(segmentID, true, true, true, out vector2, out startDir, out smoothStart);
					Vector3 vector3;
					Vector3 endDir;
					bool smoothEnd;
					this.CalculateCorner(segmentID, true, false, true, out vector3, out endDir, out smoothEnd);
					Vector3 vector4;
					Vector3 startDir2;
					this.CalculateCorner(segmentID, true, true, false, out vector4, out startDir2, out smoothStart);
					Vector3 vector5;
					Vector3 endDir2;
					this.CalculateCorner(segmentID, true, false, false, out vector5, out endDir2, out smoothEnd);
					Vector3 vector6;
					Vector3 vector7;
					NetSegment.CalculateMiddlePoints(vector2, startDir, vector5, endDir2, smoothStart, smoothEnd, out vector6, out vector7);
					Vector3 vector8;
					Vector3 vector9;
					NetSegment.CalculateMiddlePoints(vector4, startDir2, vector3, endDir, smoothStart, smoothEnd, out vector8, out vector9);
					Vector3 position = instance.m_nodes.m_buffer[(int)this.m_startNode].m_position;
					Vector3 position2 = instance.m_nodes.m_buffer[(int)this.m_endNode].m_position;
					Vector4 meshScale;
					meshScale..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
					Vector4 colorLocation = RenderManager.GetColorLocation((uint)(49152 + segmentID));
					Vector4 vector10 = colorLocation;
					if (NetNode.BlendJunction(this.m_startNode))
					{
						colorLocation = RenderManager.GetColorLocation(86016u + (uint)this.m_startNode);
					}
					if (NetNode.BlendJunction(this.m_endNode))
					{
						vector10 = RenderManager.GetColorLocation(86016u + (uint)this.m_endNode);
					}
					Vector4 vector11;
					vector11..ctor(colorLocation.x, colorLocation.y, vector10.x, vector10.y);
					for (int i = 0; i < info.m_segments.Length; i++)
					{
						NetInfo.Segment segment = info.m_segments[i];
						bool flag3 = false;
						if (segment.m_layer == layer && segment.CheckFlags(this.m_flags, out flag3) && segment.m_combinedLod != null)
						{
							Vector4 objectIndex = vector11;
							if (segment.m_requireWindSpeed)
							{
								objectIndex.w = Singleton<WeatherManager>.get_instance().GetWindSpeed((position + position2) * 0.5f);
							}
							else if (flag3)
							{
								objectIndex..ctor(objectIndex.z, objectIndex.w, objectIndex.x, objectIndex.y);
							}
							Matrix4x4 leftMatrix;
							Matrix4x4 rightMatrix;
							if (flag3)
							{
								leftMatrix = NetSegment.CalculateControlMatrix(vector3, vector9, vector8, vector4, vector5, vector7, vector6, vector2, groupPosition, vScale);
								rightMatrix = NetSegment.CalculateControlMatrix(vector5, vector7, vector6, vector2, vector3, vector9, vector8, vector4, groupPosition, vScale);
							}
							else
							{
								leftMatrix = NetSegment.CalculateControlMatrix(vector2, vector6, vector7, vector5, vector4, vector8, vector9, vector3, groupPosition, vScale);
								rightMatrix = NetSegment.CalculateControlMatrix(vector4, vector8, vector9, vector3, vector2, vector6, vector7, vector5, groupPosition, vScale);
							}
							NetSegment.PopulateGroupData(info, segment, leftMatrix, rightMatrix, meshScale, objectIndex, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
						}
					}
				}
			}
		}
	}

	public static void PopulateGroupData(NetInfo info, NetInfo.Segment segmentInfo, Matrix4x4 leftMatrix, Matrix4x4 rightMatrix, Vector4 meshScale, Vector4 objectIndex, ref int vertexIndex, ref int triangleIndex, RenderGroup.MeshData data, ref bool requireSurfaceMaps)
	{
		if (segmentInfo.m_requireSurfaceMaps)
		{
			requireSurfaceMaps = true;
		}
		RenderGroup.MeshData data2 = segmentInfo.m_combinedLod.m_key.m_mesh.m_data;
		int[] triangles = data2.m_triangles;
		int num = triangles.Length;
		for (int i = 0; i < num; i++)
		{
			data.m_triangles[triangleIndex++] = triangles[i] + vertexIndex;
		}
		RenderGroup.VertexArrays vertexArrays = data2.VertexArrayMask();
		Vector3[] vertices = data2.m_vertices;
		Vector3[] normals = data2.m_normals;
		Vector4[] tangents = data2.m_tangents;
		Vector2[] uvs = data2.m_uvs;
		Vector2[] uvs2 = data2.m_uvs3;
		Color32[] colors = data2.m_colors;
		int num2 = vertices.Length;
		Vector2 vector;
		vector..ctor(objectIndex.x, objectIndex.y);
		Vector2 vector2;
		vector2..ctor(objectIndex.z, objectIndex.w);
		for (int j = 0; j < num2; j++)
		{
			Vector3 vector3 = vertices[j];
			vector3.x = vector3.x * meshScale.x + 0.5f;
			vector3.z = vector3.z * meshScale.y + 0.5f;
			Vector4 vector4;
			vector4..ctor(vector3.z, 1f - vector3.z, 3f * vector3.z, -vector3.z);
			Vector4 vector5;
			vector5..ctor(vector4.y * vector4.y * vector4.y, vector4.z * vector4.y * vector4.y, vector4.z * vector4.x * vector4.y, vector4.x * vector4.x * vector4.x);
			Vector4 vector6;
			vector6..ctor(vector4.y * (-1f - vector4.w) * 3f, vector4.y * (1f - vector4.z) * 3f, vector4.x * (2f - vector4.z) * 3f, vector4.x * -vector4.w * 3f);
			Vector4 vector7 = leftMatrix * vector5;
			Vector4 vector8 = rightMatrix * vector5;
			Vector4 vector9 = vector7 + (vector8 - vector7) * vector3.x;
			Vector4 vector10 = leftMatrix * vector6;
			Vector4 vector11 = rightMatrix * vector6;
			Vector3 vector12 = Vector3.Normalize(vector10 + (vector11 - vector10) * vector3.x);
			Vector3 vector13 = Vector3.Normalize(new Vector3(vector12.z, 0f, -vector12.x));
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetColumn(0, vector13);
			matrix4x.SetColumn(1, Vector3.Cross(vector12, vector13));
			matrix4x.SetColumn(2, vector12);
			data.m_vertices[vertexIndex] = new Vector3(vector9.x, vector9.y + vector3.y, vector9.z);
			if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
			{
				data.m_normals[vertexIndex] = matrix4x.MultiplyVector(normals[j]);
			}
			else
			{
				data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
			{
				Vector4 vector14 = tangents[j];
				Vector3 vector15 = matrix4x.MultiplyVector(vector14);
				vector14.x = vector15.x;
				vector14.y = vector15.y;
				vector14.z = vector15.z;
				data.m_tangents[vertexIndex] = vector14;
			}
			else
			{
				data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
			}
			Color32 color;
			if ((vertexArrays & RenderGroup.VertexArrays.Colors) != (RenderGroup.VertexArrays)0)
			{
				color = colors[j];
			}
			else
			{
				color..ctor(255, 255, 255, 255);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
			{
				Vector2 vector16 = uvs[j];
				vector16.y = Mathf.Lerp(vector16.y, vector9.w, (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector16;
			}
			else
			{
				Vector2 vector17 = default(Vector2);
				vector17.y = Mathf.Lerp(vector17.y, vector9.w, (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector17;
			}
			data.m_colors[vertexIndex] = info.m_netAI.GetGroupVertexColor(segmentInfo, j);
			data.m_uvs2[vertexIndex] = vector;
			data.m_uvs4[vertexIndex] = vector2;
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs3) != (RenderGroup.VertexArrays)0)
			{
				data.m_uvs3[vertexIndex] = uvs2[j];
			}
			vertexIndex++;
		}
	}

	public static void PopulateArrowGroupData(Vector3 pos, Vector3 dir, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		float num = VectorUtils.LengthXZ(dir) * 1.42f;
		Vector3 vector;
		vector..ctor(num, Mathf.Abs(dir.y), num);
		min = Vector3.Min(min, pos - vector);
		max = Vector3.Max(max, pos + vector);
		maxRenderDistance = Mathf.Max(maxRenderDistance, 20000f);
		Vector3 vector2 = pos - groupPosition;
		Vector3 vector3;
		vector3..ctor(dir.z, 0f, -dir.x);
		data.m_vertices[vertexIndex] = vector2 - dir - vector3;
		data.m_normals[vertexIndex] = pos;
		data.m_uvs[vertexIndex] = new Vector2(0f, 0f);
		vertexIndex++;
		data.m_vertices[vertexIndex] = vector2 - dir + vector3;
		data.m_normals[vertexIndex] = pos;
		data.m_uvs[vertexIndex] = new Vector2(1f, 0f);
		vertexIndex++;
		data.m_vertices[vertexIndex] = vector2 + dir + vector3;
		data.m_normals[vertexIndex] = pos;
		data.m_uvs[vertexIndex] = new Vector2(1f, 1f);
		vertexIndex++;
		data.m_vertices[vertexIndex] = vector2 + dir - vector3;
		data.m_normals[vertexIndex] = pos;
		data.m_uvs[vertexIndex] = new Vector2(0f, 1f);
		vertexIndex++;
		data.m_triangles[triangleIndex++] = vertexIndex - 4;
		data.m_triangles[triangleIndex++] = vertexIndex - 1;
		data.m_triangles[triangleIndex++] = vertexIndex - 3;
		data.m_triangles[triangleIndex++] = vertexIndex - 3;
		data.m_triangles[triangleIndex++] = vertexIndex - 1;
		data.m_triangles[triangleIndex++] = vertexIndex - 2;
	}

	public Notification.Problem m_problems;

	public Bounds m_bounds;

	public Vector3 m_middlePosition;

	public Vector3 m_startDirection;

	public Vector3 m_endDirection;

	public NetSegment.Flags m_flags;

	public float m_averageLength;

	public uint m_buildIndex;

	public uint m_modifiedIndex;

	public uint m_lanes;

	public uint m_path;

	public ushort m_startNode;

	public ushort m_endNode;

	public ushort m_blockStartLeft;

	public ushort m_blockStartRight;

	public ushort m_blockEndLeft;

	public ushort m_blockEndRight;

	public ushort m_trafficBuffer;

	public ushort m_noiseBuffer;

	public ushort m_startLeftSegment;

	public ushort m_startRightSegment;

	public ushort m_endLeftSegment;

	public ushort m_endRightSegment;

	public ushort m_infoIndex;

	public ushort m_nextGridSegment;

	public ushort m_nameSeed;

	public byte m_trafficDensity;

	public byte m_trafficLightState0;

	public byte m_trafficLightState1;

	public byte m_cornerAngleStart;

	public byte m_cornerAngleEnd;

	public byte m_fireCoverage;

	public byte m_wetness;

	public byte m_condition;

	public byte m_noiseDensity;

	private static HashSet<ushort> m_tempCheckedSet = new HashSet<ushort>();

	private static HashSet<ushort> m_tempCheckedSet2 = new HashSet<ushort>();

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Original = 4,
		Collapsed = 8,
		Invert = 16,
		Untouchable = 32,
		End = 64,
		Bend = 128,
		WaitingPath = 256,
		PathFailed = 512,
		PathLength = 1024,
		AccessFailed = 2048,
		TrafficStart = 4096,
		TrafficEnd = 8192,
		CrossingStart = 16384,
		CrossingEnd = 32768,
		StopRight = 65536,
		StopLeft = 131072,
		StopRight2 = 262144,
		StopLeft2 = 524288,
		HeavyBan = 1048576,
		Blocked = 2097152,
		Flooded = 4194304,
		BikeBan = 8388608,
		CarBan = 16777216,
		AsymForward = 33554432,
		AsymBackward = 67108864,
		CustomName = 134217728,
		NameVisible1 = 268435456,
		NameVisible2 = 536870912,
		YieldStart = 1073741824,
		YieldEnd = -2147483648,
		StopBoth = 196608,
		StopBoth2 = 786432,
		StopAll = 983040,
		CombustionBan = 256,
		All = -1
	}
}
