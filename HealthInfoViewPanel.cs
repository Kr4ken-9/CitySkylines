﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class HealthInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
		this.m_AvgHealthBar = base.Find<UISlider>("AvgHealthBar");
		this.m_AvgHealth = base.Find<UILabel>("AvgHealth");
		this.m_HealthcareMeter = base.Find<UISlider>("HealthcareMeter");
		this.m_Sicks = base.Find<UILabel>("Sicks");
		this.m_HealCapacity = base.Find<UILabel>("HealCapacity");
		this.m_ActiveColor = base.Find<UISprite>("ColorActive");
		this.m_InactiveColor = base.Find<UISprite>("ColorInactive");
		this.m_Health = base.Find<UITextureSprite>("Coverage");
		this.m_CoverageGradient = base.Find<UITextureSprite>("CoverageGradient");
		this.m_DeathcareMeter = base.Find<UISlider>("DeathcareMeter");
		this.m_CrematoriumCapacity = base.Find<UILabel>("CrematoriumCapacity");
		this.m_Deceased = base.Find<UILabel>("Deceased");
		this.m_Deads = base.Find<UILabel>("Deads");
		this.m_CemetaryCapacity = base.Find<UILabel>("CemetaryCapacity");
		this.m_CemetaryMeter = base.Find<UISlider>("CemetaryMeter");
		this.m_CemetaryUsage = base.Find<UILabel>("CemetaryUsage");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				if (Singleton<InfoManager>.get_exists())
				{
					this.m_ActiveColor.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_activeColor);
					this.m_InactiveColor.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_inactiveColor);
					this.m_Health.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_negativeColor);
					this.m_Health.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_targetColor);
					this.m_Health.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					this.m_Health.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					this.m_Health.get_renderMaterial().SetFloat("_Offset", 0f);
					this.m_AvgHealthBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(this.m_Health.get_renderMaterial());
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					this.m_CoverageGradient.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
				if (Singleton<InfoManager>.get_exists())
				{
					UITextureSprite uITextureSprite = this.m_CemetaryMeter.Find<UITextureSprite>("Background");
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_targetColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[4].m_negativeColor);
					uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.m_initialized = true;
		}
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
			int num = 0;
			int num2 = 0;
			if (Singleton<DistrictManager>.get_exists())
			{
				num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetHealCapacity();
				num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetSickCount();
			}
			this.m_HealthcareMeter.set_value((float)base.GetPercentage(num, num2));
			this.m_Sicks.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_SICKS"), num2));
			this.m_HealCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_HEALCAPACITY"), num));
			if (Singleton<DistrictManager>.get_exists())
			{
				int finalHealth = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHealth;
				this.m_AvgHealthBar.set_value((float)finalHealth);
				this.m_AvgHealth.set_text(string.Concat(new object[]
				{
					Locale.Get("INFO_HEALTH_AVERAGE"),
					" - ",
					finalHealth,
					"%"
				}));
			}
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			if (Singleton<DistrictManager>.get_exists())
			{
				num3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetDeadCapacity();
				num4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetDeadAmount();
				num5 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetCremateCapacity();
				num6 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetDeadCount();
			}
			if (num3 > 0)
			{
				this.m_CemetaryMeter.set_value((float)num4 / (float)num3 * 100f);
			}
			else
			{
				this.m_CemetaryMeter.set_value(0f);
			}
			this.m_DeathcareMeter.set_value((float)base.GetPercentage(num5, num6));
			this.m_Deads.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_DEADS"), num4));
			this.m_CemetaryCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_CEMETARYCAPACITY"), num3));
			this.m_CrematoriumCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_CREMATORIUMCAPACITY"), num5));
			this.m_Deceased.set_text(StringUtils.SafeFormat(Locale.Get("INFO_HEALTH_DECEASED"), num6));
			this.m_CemetaryUsage.set_text(string.Concat(new object[]
			{
				Locale.Get("INFO_HEALTH_CEMETARYUSAGE"),
				" - ",
				this.m_CemetaryMeter.get_value(),
				"%"
			}));
		}
	}

	private UISprite m_ActiveColor;

	private UISprite m_InactiveColor;

	private UITextureSprite m_Health;

	private UITextureSprite m_CoverageGradient;

	private UISlider m_AvgHealthBar;

	private UILabel m_AvgHealth;

	private UISlider m_HealthcareMeter;

	private UILabel m_Sicks;

	private UILabel m_HealCapacity;

	private UISlider m_DeathcareMeter;

	private UISlider m_CemetaryMeter;

	private UILabel m_CemetaryUsage;

	private UILabel m_CemetaryCapacity;

	private UILabel m_Deads;

	private UILabel m_CrematoriumCapacity;

	private UILabel m_Deceased;

	private bool m_initialized;
}
