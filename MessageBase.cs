﻿using System;
using ColossalFramework.IO;
using ICities;

public class MessageBase : IDataContainer, IChirperMessage
{
	public string senderName
	{
		get
		{
			return this.GetSenderName();
		}
	}

	public string text
	{
		get
		{
			return this.GetText();
		}
	}

	public uint senderID
	{
		get
		{
			return this.GetSenderID();
		}
	}

	public virtual string GetSenderName()
	{
		return string.Empty;
	}

	public virtual string GetText()
	{
		return string.Empty;
	}

	public virtual uint GetSenderID()
	{
		return 0u;
	}

	public virtual bool IsSimilarMessage(MessageBase other)
	{
		return false;
	}

	public virtual void Serialize(DataSerializer s)
	{
	}

	public virtual void Deserialize(DataSerializer s)
	{
	}

	public virtual void AfterDeserialize(DataSerializer s)
	{
	}
}
