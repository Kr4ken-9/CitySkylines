﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class MeteorStrikeAI : DisasterAI
{
	public override void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort disasterID, ref DisasterData data, ref Vector3 shake)
	{
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			int num = (int)(instance.m_referenceFrameIndex - data.m_activationFrame - 32u);
			if (num <= 0)
			{
				return;
			}
			if (num >= 256)
			{
				return;
			}
			Vector3 vector = cameraInfo.m_camera.get_transform().InverseTransformPoint(data.m_targetPosition);
			vector.z *= 0.25f;
			float num2 = 0.2f / (1f + vector.get_magnitude() * 0.002f);
			float num3 = (float)num + instance.m_referenceTimer;
			num2 *= 1f - num3 / 256f;
			Vector3 vector2;
			vector2.x = Mathf.Sin(num3 * 0.53f) + Mathf.Sin(num3 * 0.17f);
			vector2.y = Mathf.Sin(num3 * 0.31f) + Mathf.Sin(num3 * 0.73f);
			vector2.z = Mathf.Sin(num3 * 0.19f) + Mathf.Sin(num3 * 0.49f);
			shake += vector2 * num2;
		}
	}

	public override void PlayInstance(AudioManager.ListenerInfo listenerInfo, ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None && this.m_meteorSound != null)
		{
			InstanceID empty = InstanceID.Empty;
			empty.Disaster = disasterID;
			DisasterAI.m_tempList.Clear();
			InstanceManager.GetAllGroupInstances(empty, DisasterAI.m_tempList);
			if (DisasterAI.m_tempList.m_size > 1)
			{
				VehicleManager instance = Singleton<VehicleManager>.get_instance();
				for (int i = 0; i < DisasterAI.m_tempList.m_size; i++)
				{
					InstanceID instanceID = DisasterAI.m_tempList.m_buffer[i];
					ushort vehicle = instanceID.Vehicle;
					if (vehicle != 0)
					{
						VehicleInfo info = instance.m_vehicles.m_buffer[(int)vehicle].Info;
						if (info == this.m_meteorInfoSmall || info == this.m_meteorInfoLarge)
						{
							if ((instance.m_vehicles.m_buffer[(int)vehicle].m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
							{
								uint num = (uint)(((int)vehicle << 4) / 16384);
								uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
								Vehicle.Frame frameData = instance.m_vehicles.m_buffer[(int)vehicle].GetFrameData(num2 - 32u);
								Vehicle.Frame frameData2 = instance.m_vehicles.m_buffer[(int)vehicle].GetFrameData(num2 - 16u);
								float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
								Vector3 position = Vector3.Lerp(frameData.m_position, frameData2.m_position, num3);
								Vector3 velocity = Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num3) * 3.75f;
								float volume = Mathf.Clamp01(0.5f + (float)data.m_intensity * 0.005f);
								float pitch = Mathf.Clamp(1.04f - (float)data.m_intensity * 0.004f, 0.5f, 1f);
								Singleton<AudioManager>.get_instance().EffectGroup.AddPlayer(listenerInfo, (int)instanceID.RawData, this.m_meteorSound, position, velocity, 5000f, volume, pitch);
							}
							return;
						}
					}
				}
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None && this.m_explosionSound != null)
		{
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			int num4 = (int)(instance2.m_referenceFrameIndex - data.m_activationFrame - 32u);
			if (num4 <= 0)
			{
				return;
			}
			if (num4 >= 256)
			{
				return;
			}
			float num5 = (float)num4 + instance2.m_referenceTimer;
			float num6 = 1f - num5 / 256f;
			InstanceID empty2 = InstanceID.Empty;
			empty2.Disaster = disasterID;
			Vector3 targetPosition = data.m_targetPosition;
			float volume2 = Mathf.Sqrt(Mathf.Clamp01(num6 * (0.5f + (float)data.m_intensity * 0.005f)));
			Singleton<AudioManager>.get_instance().EffectGroup.AddPlayer(listenerInfo, (int)empty2.RawData, this.m_explosionSound, targetPosition, Vector3.get_zero(), 7500f, volume2, 1f);
		}
	}

	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			VehicleInfo vehicleInfo = (data.m_intensity >= 50) ? this.m_meteorInfoLarge : this.m_meteorInfoSmall;
			MeteorAI meteorAI = vehicleInfo.m_vehicleAI as MeteorAI;
			if (meteorAI == null)
			{
				return;
			}
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector3 normalized;
			normalized.x = Mathf.Sin(data.m_angle);
			normalized.y = (float)randomizer.Int32(500, 1000) * 0.001f;
			normalized.z = -Mathf.Cos(data.m_angle);
			normalized = normalized.get_normalized();
			Vector3 vector = data.m_targetPosition;
			float num = 100f;
			Vector3 vector2;
			vector2.x = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector2.y = 0f;
			vector2.z = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector += vector2 * (num * (1f + Mathf.Abs(Vector3.Dot(vector2, normalized))));
			float num2 = 0.2f + (float)data.m_intensity * 0.0145454546f;
			float num3 = meteorAI.m_craterRadius * num2;
			float num4 = meteorAI.m_destructionRadiusMax * num2;
			float num5 = meteorAI.m_burnRadiusMax * num2;
			float num6 = num3;
			float num7 = Mathf.Max(num3, Mathf.Max(num4, num5)) + num * 2f;
			float num8 = vector.x - num7 * (1f + Mathf.Abs(normalized.x));
			float num9 = vector.z - num7 * (1f + Mathf.Abs(normalized.z));
			float num10 = vector.x + num7 * (1f + Mathf.Abs(normalized.x));
			float num11 = vector.z + num7 * (1f + Mathf.Abs(normalized.z));
			int num12 = Mathf.Max((int)(num8 / 38.4f + 128f), 2);
			int num13 = Mathf.Max((int)(num9 / 38.4f + 128f), 2);
			int num14 = Mathf.Min((int)(num10 / 38.4f + 128f), 253);
			int num15 = Mathf.Min((int)(num11 / 38.4f + 128f), 253);
			Vector3 vector3;
			vector3.y = 0f;
			for (int i = num13; i <= num15; i++)
			{
				vector3.z = ((float)i - 128f + 0.5f) * 38.4f - vector.z;
				for (int j = num12; j <= num14; j++)
				{
					vector3.x = ((float)j - 128f + 0.5f) * 38.4f - vector.x;
					float magnitude = vector3.get_magnitude();
					float num16 = Vector3.Dot(vector3, normalized);
					magnitude = (vector3 - normalized * ((magnitude - magnitude / (1f + Mathf.Abs(num16) / Mathf.Max(1f, magnitude))) * Mathf.Sign(num16))).get_magnitude();
					if (magnitude < num7)
					{
						float num17 = Mathf.Min(1f, 1f - (magnitude - num6) / (num7 - num6));
						int num18 = i * 256 + j;
						map[num18] = (byte)(255 - (int)(255 - map[num18]) * (255 - Mathf.RoundToInt(num17 * 255f)) / 255);
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(data.m_targetPosition);
			Vector3 vector;
			vector.x = Mathf.Sin(data.m_angle);
			vector.y = (float)randomizer.Int32(500, 1000) * 0.001f;
			vector.z = -Mathf.Cos(data.m_angle);
			vector = vector.get_normalized() * 50000f;
			Vector3 position = data.m_targetPosition + vector;
			VehicleInfo info = (data.m_intensity >= 50) ? this.m_meteorInfoLarge : this.m_meteorInfoSmall;
			ushort num;
			if (instance.CreateVehicle(out num, ref randomizer, info, position, TransferManager.TransferReason.None, false, false))
			{
				InstanceID srcID = default(InstanceID);
				InstanceID dstID = default(InstanceID);
				srcID.Disaster = disasterID;
				dstID.Vehicle = num;
				Singleton<InstanceManager>.get_instance().CopyGroup(srcID, dstID);
				instance.m_vehicles.m_buffer[(int)num].SetTargetPos(0, data.m_targetPosition);
				data.m_flags |= DisasterData.Flags.Significant;
				Singleton<DisasterManager>.get_instance().m_randomDisasterCooldown = 0;
			}
		}
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint estimatedActivationFrame = this.GetEstimatedActivationFrame(disasterID, ref data);
			if (currentFrameIndex + 1755u > estimatedActivationFrame)
			{
				Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, false);
			}
			if (currentFrameIndex + 320u > estimatedActivationFrame)
			{
				Singleton<DisasterManager>.get_instance().FollowDisaster(disasterID);
			}
		}
	}

	public uint GetEstimatedActivationFrame(ushort disasterID, ref DisasterData data)
	{
		VehicleInfo vehicleInfo = (data.m_intensity >= 50) ? this.m_meteorInfoLarge : this.m_meteorInfoSmall;
		uint num = (uint)Mathf.RoundToInt(800000f / Mathf.Max(1f, vehicleInfo.m_maxSpeed));
		return data.m_startFrame + num;
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillEmerging(disasterID, ref data))
		{
			return true;
		}
		InstanceID id = default(InstanceID);
		id.Disaster = disasterID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		return group != null && group.m_refCount >= 2;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num < 1024u;
	}

	public override int GetFireSpreadProbability(ushort disasterID, ref DisasterData data)
	{
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return 1500 / (8 + ((int)num >> 10));
	}

	protected override bool IsStillClearing(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillClearing(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		float num2 = num * 0.125f - 1000f;
		Vector2 vector;
		vector.x = Mathf.Abs(data.m_targetPosition.x) + 4800f;
		vector.y = Mathf.Abs(data.m_targetPosition.z) + 4800f;
		float num3 = VectorUtils.LengthXZ(vector);
		return num3 > num2;
	}

	public override bool CanAffectAt(ushort disasterID, ref DisasterData data, Vector3 position, out float priority)
	{
		if (!base.CanAffectAt(disasterID, ref data, position, out priority))
		{
			return false;
		}
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			return false;
		}
		float num = VectorUtils.LengthXZ(position - data.m_targetPosition);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		float num3 = num2 * 0.125f - 1000f;
		float num4 = num2 * 0.125f + 100f;
		VehicleInfo vehicleInfo = (data.m_intensity >= 50) ? this.m_meteorInfoLarge : this.m_meteorInfoSmall;
		MeteorAI meteorAI = vehicleInfo.m_vehicleAI as MeteorAI;
		if (meteorAI != null)
		{
			num4 += meteorAI.m_craterRadius * 2f;
		}
		priority = Mathf.Clamp01(Mathf.Min((num4 - num) * 0.02f, (num - num3) * 0.002f));
		return num >= num3 && num <= num4;
	}

	protected override float GetMinimumEdgeDistance()
	{
		float num = 0f;
		if (this.m_meteorInfoSmall != null)
		{
			MeteorAI meteorAI = this.m_meteorInfoSmall.m_vehicleAI as MeteorAI;
			if (meteorAI != null)
			{
				num = Mathf.Max(num, meteorAI.m_craterRadius * 1.65454543f);
			}
		}
		if (this.m_meteorInfoLarge != null)
		{
			MeteorAI meteorAI2 = this.m_meteorInfoLarge.m_vehicleAI as MeteorAI;
			if (meteorAI2 != null)
			{
				num = Mathf.Max(num, meteorAI2.m_craterRadius * 1.65454543f);
			}
		}
		return num + 150f;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool HasDirection()
	{
		return true;
	}

	public override string GenerateName(ushort disasterID, ref DisasterData data)
	{
		Randomizer randomizer;
		randomizer..ctor(data.m_randomSeed);
		uint num = Locale.Count("METEOR_TITLE");
		return Locale.Get("METEOR_TITLE", randomizer.Int32(num));
	}

	public override bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		position = data.m_targetPosition;
		rotation = Quaternion.get_identity();
		float num = 400f + (float)data.m_intensity;
		size..ctor(num, num, num);
		return true;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.WindPower;
		return true;
	}

	public VehicleInfo m_meteorInfoSmall;

	public VehicleInfo m_meteorInfoLarge;

	public AudioInfo m_meteorSound;

	public AudioInfo m_explosionSound;
}
