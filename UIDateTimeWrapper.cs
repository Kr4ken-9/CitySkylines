﻿using System;

public class UIDateTimeWrapper : UIWrapper<DateTime>
{
	public UIDateTimeWrapper(DateTime def) : base(def)
	{
	}

	public override void Check(DateTime newVal)
	{
		if (this.m_Value.Year != newVal.Year || this.m_Value.Month != newVal.Month || this.m_Value.Day != newVal.Day)
		{
			this.m_Value = newVal;
			this.m_String = this.m_Value.Date.ToString("dd/MM/yyyy");
		}
	}
}
