﻿using System;
using ColossalFramework;

public class EventInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_eventAI == null)
		{
			this.m_eventAI = base.GetComponent<EventAI>();
			this.m_eventAI.m_info = this;
			this.m_eventAI.InitializeAI();
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_eventAI != null)
		{
			this.m_eventAI.ReleaseAI();
			this.m_eventAI = null;
		}
		base.DestroyPrefab();
	}

	public void CheckUnlocking()
	{
		MilestoneInfo unlockMilestone = this.m_UnlockMilestone;
		if (unlockMilestone != null)
		{
			Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
		}
	}

	public override PrefabAI GetAI()
	{
		return this.m_eventAI;
	}

	public bool HasModifiedTypeData()
	{
		return this.m_popularityOffset != 0 || this.m_ticketPriceOffset != 0;
	}

	public EventManager.EventType m_type;

	public EventManager.EventGroup m_group;

	public MilestoneInfo m_UnlockMilestone;

	[NonSerialized]
	public EventAI m_eventAI;

	[NonSerialized]
	public int m_popularityOffset;

	[NonSerialized]
	public int m_ticketPriceOffset;
}
