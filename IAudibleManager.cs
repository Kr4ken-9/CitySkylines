﻿using System;

public interface IAudibleManager
{
	void PlayAudio(AudioManager.ListenerInfo listenerInfo);
}
