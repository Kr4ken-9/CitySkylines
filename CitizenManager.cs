﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class CitizenManager : SimulationManagerBase<CitizenManager, CitizenProperties>, ISimulationManager, IRenderableManager, IAudibleManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_citizens = new Array32<Citizen>(1048576u);
		this.m_units = new Array32<CitizenUnit>(524288u);
		this.m_instances = new Array16<CitizenInstance>(65536u);
		this.m_citizenGrid = new ushort[4665600];
		this.m_renderBuffer = new ulong[1024];
		this.m_groupCitizens = new FastList<ushort>[700];
		this.m_groupAnimals = new FastList<ushort>[46];
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_Speed = Animator.StringToHash("Speed");
		this.ID_State = Animator.StringToHash("State");
		this.ID_CitizenLocation = Shader.PropertyToID("_CitizenLocation");
		this.ID_CitizenColor = Shader.PropertyToID("_CitizenColor");
		this.m_citizenLayer = LayerMask.NameToLayer("Citizens");
		this.m_undergroundLayer = LayerMask.NameToLayer("MetroTunnels");
		this.m_audioGroup = new AudioGroup(5, new SavedFloat(Settings.effectAudioVolume, Settings.gameSettingsFile, DefaultSettings.effectAudioVolume, true));
		uint num;
		this.m_citizens.CreateItem(out num);
		this.m_units.CreateItem(out num);
		ushort num2;
		this.m_instances.CreateItem(out num2);
	}

	public override void InitializeProperties(CitizenProperties properties)
	{
		base.InitializeProperties(properties);
	}

	public override void DestroyProperties(CitizenProperties properties)
	{
		if (this.m_properties == properties && this.m_audioGroup != null)
		{
			this.m_audioGroup.Reset();
		}
		base.DestroyProperties(properties);
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		float levelOfDetailFactor = RenderManager.LevelOfDetailFactor;
		float near = cameraInfo.m_near;
		float num = Mathf.Min(Mathf.Min(levelOfDetailFactor * 800f, levelOfDetailFactor * 400f + cameraInfo.m_height * 0.5f), cameraInfo.m_far);
		Vector3 vector = cameraInfo.m_position + cameraInfo.m_directionA * near;
		Vector3 vector2 = cameraInfo.m_position + cameraInfo.m_directionB * near;
		Vector3 vector3 = cameraInfo.m_position + cameraInfo.m_directionC * near;
		Vector3 vector4 = cameraInfo.m_position + cameraInfo.m_directionD * near;
		Vector3 vector5 = cameraInfo.m_position + cameraInfo.m_directionA * num;
		Vector3 vector6 = cameraInfo.m_position + cameraInfo.m_directionB * num;
		Vector3 vector7 = cameraInfo.m_position + cameraInfo.m_directionC * num;
		Vector3 vector8 = cameraInfo.m_position + cameraInfo.m_directionD * num;
		Vector3 vector9 = Vector3.Min(Vector3.Min(Vector3.Min(vector, vector2), Vector3.Min(vector3, vector4)), Vector3.Min(Vector3.Min(vector5, vector6), Vector3.Min(vector7, vector8)));
		Vector3 vector10 = Vector3.Max(Vector3.Max(Vector3.Max(vector, vector2), Vector3.Max(vector3, vector4)), Vector3.Max(Vector3.Max(vector5, vector6), Vector3.Max(vector7, vector8)));
		int num2 = Mathf.Max((int)((vector9.x - 1f) / 8f + 1080f), 0);
		int num3 = Mathf.Max((int)((vector9.z - 1f) / 8f + 1080f), 0);
		int num4 = Mathf.Min((int)((vector10.x + 1f) / 8f + 1080f), 2159);
		int num5 = Mathf.Min((int)((vector10.z + 1f) / 8f + 1080f), 2159);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = this.m_citizenGrid[i * 2160 + j];
				if (num6 != 0)
				{
					this.m_renderBuffer[num6 >> 6] |= 1uL << (int)num6;
				}
			}
		}
		int num7 = this.m_renderBuffer.Length;
		for (int k = 0; k < num7; k++)
		{
			ulong num8 = this.m_renderBuffer[k];
			if (num8 != 0uL)
			{
				for (int l = 0; l < 64; l++)
				{
					ulong num9 = 1uL << l;
					if ((num8 & num9) != 0uL)
					{
						ushort num10 = (ushort)(k << 6 | l);
						if (!this.m_instances.m_buffer[(int)num10].RenderInstance(cameraInfo, num10))
						{
							num8 &= ~num9;
						}
						ushort nextGridInstance = this.m_instances.m_buffer[(int)num10].m_nextGridInstance;
						int num11 = 0;
						while (nextGridInstance != 0)
						{
							int num12 = nextGridInstance >> 6;
							num9 = 1uL << (int)nextGridInstance;
							if (num12 == k)
							{
								if ((num8 & num9) != 0uL)
								{
									break;
								}
								num8 |= num9;
							}
							else
							{
								ulong num13 = this.m_renderBuffer[num12];
								if ((num13 & num9) != 0uL)
								{
									break;
								}
								this.m_renderBuffer[num12] = (num13 | num9);
							}
							if (nextGridInstance > num10)
							{
								break;
							}
							nextGridInstance = this.m_instances.m_buffer[(int)nextGridInstance].m_nextGridInstance;
							if (++num11 > 65536)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				this.m_renderBuffer[k] = num8;
			}
		}
		int num14 = PrefabCollection<CitizenInfo>.PrefabCount();
		for (int m = 0; m < num14; m++)
		{
			CitizenInfo prefab = PrefabCollection<CitizenInfo>.GetPrefab((uint)m);
			if (prefab != null)
			{
				prefab.UpdatePrefabInstances();
				if (prefab.m_lodCount != 0)
				{
					CitizenInstance.RenderLod(cameraInfo, prefab);
				}
				if (prefab.m_undergroundLodCount != 0)
				{
					CitizenInstance.RenderUndergroundLod(cameraInfo, prefab);
				}
			}
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		if (this.m_properties != null)
		{
			LoadingManager instance = Singleton<LoadingManager>.get_instance();
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			AudioManager instance3 = Singleton<AudioManager>.get_instance();
			float masterVolume;
			if (instance.m_currentlyLoading || instance2.SimulationPaused || instance3.MuteAll)
			{
				masterVolume = 0f;
			}
			else
			{
				masterVolume = instance3.MasterVolume;
			}
			this.m_audioGroup.UpdatePlayers(listenerInfo, masterVolume);
		}
	}

	public bool CreateUnits(out uint firstUnit, ref Randomizer randomizer, ushort building, ushort vehicle, int homeCount, int workCount, int visitCount, int passengerCount, int studentCount)
	{
		firstUnit = 0u;
		workCount = (workCount + 4) / 5;
		visitCount = (visitCount + 4) / 5;
		passengerCount = (passengerCount + 4) / 5;
		studentCount = (studentCount + 4) / 5;
		int num = homeCount + workCount + visitCount + passengerCount + studentCount;
		if (num == 0)
		{
			return true;
		}
		CitizenUnit citizenUnit = default(CitizenUnit);
		uint num2 = 0u;
		for (int i = 0; i < num; i++)
		{
			uint num3;
			if (!this.m_units.CreateItem(out num3, ref randomizer))
			{
				this.ReleaseUnits(firstUnit);
				firstUnit = 0u;
				return false;
			}
			if (i == 0)
			{
				firstUnit = num3;
			}
			else
			{
				citizenUnit.m_nextUnit = num3;
				this.m_units.m_buffer[(int)((UIntPtr)num2)] = citizenUnit;
			}
			citizenUnit = default(CitizenUnit);
			citizenUnit.m_flags = CitizenUnit.Flags.Created;
			if (i < homeCount)
			{
				citizenUnit.m_flags |= CitizenUnit.Flags.Home;
				citizenUnit.m_goods = 200;
			}
			else if (i < homeCount + workCount)
			{
				citizenUnit.m_flags |= CitizenUnit.Flags.Work;
			}
			else if (i < homeCount + workCount + visitCount)
			{
				citizenUnit.m_flags |= CitizenUnit.Flags.Visit;
			}
			else if (i < homeCount + workCount + visitCount + passengerCount)
			{
				citizenUnit.m_flags |= CitizenUnit.Flags.Vehicle;
			}
			else if (i < homeCount + workCount + visitCount + passengerCount + studentCount)
			{
				citizenUnit.m_flags |= CitizenUnit.Flags.Student;
			}
			citizenUnit.m_building = building;
			citizenUnit.m_vehicle = vehicle;
			num2 = num3;
		}
		this.m_units.m_buffer[(int)((UIntPtr)num2)] = citizenUnit;
		this.m_unitCount = (int)(this.m_units.ItemCount() - 1u);
		return true;
	}

	public void ReleaseUnits(uint firstUnit)
	{
		int num = 0;
		while (firstUnit != 0u)
		{
			uint nextUnit = this.m_units.m_buffer[(int)((UIntPtr)firstUnit)].m_nextUnit;
			this.ReleaseUnitImplementation(firstUnit, ref this.m_units.m_buffer[(int)((UIntPtr)firstUnit)]);
			firstUnit = nextUnit;
			if (++num > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		this.m_unitCount = (int)(this.m_units.ItemCount() - 1u);
	}

	private void ReleaseUnitImplementation(uint unit, ref CitizenUnit data)
	{
		this.ReleaseUnitCitizen(unit, ref data, data.m_citizen0);
		this.ReleaseUnitCitizen(unit, ref data, data.m_citizen1);
		this.ReleaseUnitCitizen(unit, ref data, data.m_citizen2);
		this.ReleaseUnitCitizen(unit, ref data, data.m_citizen3);
		this.ReleaseUnitCitizen(unit, ref data, data.m_citizen4);
		data = default(CitizenUnit);
		this.m_units.ReleaseItem(unit);
	}

	private void ReleaseUnitCitizen(uint unit, ref CitizenUnit data, uint citizen)
	{
		if (citizen != 0u)
		{
			if ((ushort)(data.m_flags & CitizenUnit.Flags.Home) != 0)
			{
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding = 0;
			}
			if ((ushort)(data.m_flags & (CitizenUnit.Flags.Work | CitizenUnit.Flags.Student)) != 0)
			{
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding = 0;
			}
			if ((ushort)(data.m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding = 0;
			}
			if ((ushort)(data.m_flags & CitizenUnit.Flags.Vehicle) != 0)
			{
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_vehicle = 0;
			}
		}
	}

	public bool CreateCitizen(out uint citizen, int age, int family, ref Randomizer r)
	{
		uint num;
		if (this.m_citizens.CreateItem(out num, ref r))
		{
			citizen = num;
			Citizen citizen2 = default(Citizen);
			citizen2.m_flags = Citizen.Flags.Created;
			citizen2.Age = age;
			citizen2.m_health = 50;
			citizen2.m_wellbeing = 50;
			citizen2.m_family = (byte)family;
			this.m_citizens.m_buffer[(int)((UIntPtr)citizen)] = citizen2;
			this.m_citizenCount = (int)(this.m_citizens.ItemCount() - 1u);
			return true;
		}
		citizen = 0u;
		return false;
	}

	public bool CreateCitizen(out uint citizen, int age, int family, ref Randomizer r, Citizen.Gender gender)
	{
		for (int i = 0; i < 1000; i++)
		{
			Randomizer randomizer = r;
			uint num = this.m_citizens.NextFreeItem(ref r);
			if (num == 0u)
			{
				break;
			}
			uint num2;
			if (Citizen.GetGender(num) == gender && this.m_citizens.CreateItem(out num2, ref randomizer))
			{
				citizen = num2;
				Citizen citizen2 = default(Citizen);
				citizen2.m_flags = Citizen.Flags.Created;
				citizen2.Age = age;
				citizen2.m_health = 50;
				citizen2.m_wellbeing = 50;
				citizen2.m_family = (byte)family;
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)] = citizen2;
				this.m_citizenCount = (int)(this.m_citizens.ItemCount() - 1u);
				return true;
			}
		}
		citizen = 0u;
		return false;
	}

	public void ReleaseCitizen(uint citizen)
	{
		this.ReleaseCitizenImplementation(citizen, ref this.m_citizens.m_buffer[(int)((UIntPtr)citizen)]);
	}

	private void ReleaseCitizenImplementation(uint citizen, ref Citizen data)
	{
		InstanceID id = default(InstanceID);
		id.Citizen = citizen;
		Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
		if (data.m_instance != 0)
		{
			this.ReleaseCitizenInstance(data.m_instance);
			data.m_instance = 0;
		}
		data.SetHome(citizen, 0, 0u);
		data.SetWorkplace(citizen, 0, 0u);
		data.SetVisitplace(citizen, 0, 0u);
		data.SetVehicle(citizen, 0, 0u);
		data.SetParkedVehicle(citizen, 0);
		data = default(Citizen);
		this.m_citizens.ReleaseItem(citizen);
		this.m_citizenCount = (int)(this.m_citizens.ItemCount() - 1u);
	}

	public bool CreateCitizenInstance(out ushort instance, ref Randomizer randomizer, CitizenInfo info, uint citizen)
	{
		ushort num;
		if (this.m_instances.CreateItem(out num, ref randomizer))
		{
			instance = num;
			CitizenInstance.Frame frame;
			frame.m_velocity = Vector3.get_zero();
			frame.m_position = Vector3.get_zero();
			frame.m_rotation = Quaternion.get_identity();
			frame.m_underground = false;
			frame.m_insideBuilding = false;
			frame.m_transition = false;
			this.m_instances.m_buffer[(int)instance].m_flags = CitizenInstance.Flags.Created;
			this.m_instances.m_buffer[(int)instance].Info = info;
			this.m_instances.m_buffer[(int)instance].m_citizen = citizen;
			this.m_instances.m_buffer[(int)instance].m_frame0 = frame;
			this.m_instances.m_buffer[(int)instance].m_frame1 = frame;
			this.m_instances.m_buffer[(int)instance].m_frame2 = frame;
			this.m_instances.m_buffer[(int)instance].m_frame3 = frame;
			this.m_instances.m_buffer[(int)instance].m_targetPos = Vector3.get_zero();
			this.m_instances.m_buffer[(int)instance].m_targetDir = Vector2.get_zero();
			this.m_instances.m_buffer[(int)instance].m_color = default(Color32);
			this.m_instances.m_buffer[(int)instance].m_sourceBuilding = 0;
			this.m_instances.m_buffer[(int)instance].m_targetBuilding = 0;
			this.m_instances.m_buffer[(int)instance].m_nextGridInstance = 0;
			this.m_instances.m_buffer[(int)instance].m_nextSourceInstance = 0;
			this.m_instances.m_buffer[(int)instance].m_nextTargetInstance = 0;
			this.m_instances.m_buffer[(int)instance].m_lastFrame = 0;
			this.m_instances.m_buffer[(int)instance].m_pathPositionIndex = 0;
			this.m_instances.m_buffer[(int)instance].m_lastPathOffset = 0;
			this.m_instances.m_buffer[(int)instance].m_waitCounter = 0;
			this.m_instances.m_buffer[(int)instance].m_targetSeed = 0;
			if (citizen != 0u)
			{
				this.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance = instance;
			}
			info.m_citizenAI.CreateInstance(instance, ref this.m_instances.m_buffer[(int)instance]);
			this.m_instanceCount = (int)(this.m_instances.ItemCount() - 1u);
			return true;
		}
		instance = 0;
		return false;
	}

	private void InitializeInstance(ushort instance, ref CitizenInstance data)
	{
		if ((data.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
		{
			this.AddToGrid(instance, ref data);
		}
	}

	public void ReleaseCitizenInstance(ushort instance)
	{
		this.ReleaseCitizenInstanceImplementation(instance, ref this.m_instances.m_buffer[(int)instance]);
	}

	private void ReleaseCitizenInstanceImplementation(ushort instance, ref CitizenInstance data)
	{
		CitizenInfo info = data.Info;
		if (info != null)
		{
			info.m_citizenAI.ReleaseInstance(instance, ref this.m_instances.m_buffer[(int)instance]);
		}
		data.Unspawn(instance);
		InstanceID id = default(InstanceID);
		id.CitizenInstance = instance;
		Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
		if (data.m_path != 0u)
		{
			Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
			data.m_path = 0u;
		}
		if (data.m_citizen != 0u)
		{
			this.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].SetVehicle(data.m_citizen, 0, 0u);
			this.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_instance = 0;
			data.m_citizen = 0u;
		}
		data.m_flags = CitizenInstance.Flags.None;
		this.m_instances.ReleaseItem(instance);
		this.m_instanceCount = (int)(this.m_instances.ItemCount() - 1u);
	}

	public void AddToGrid(ushort instance, ref CitizenInstance data)
	{
		CitizenInstance.Frame lastFrameData = data.GetLastFrameData();
		int gridX = Mathf.Clamp((int)(lastFrameData.m_position.x / 8f + 1080f), 0, 2159);
		int gridZ = Mathf.Clamp((int)(lastFrameData.m_position.z / 8f + 1080f), 0, 2159);
		this.AddToGrid(instance, ref data, gridX, gridZ);
	}

	public void AddToGrid(ushort instance, ref CitizenInstance data, int gridX, int gridZ)
	{
		int num = gridZ * 2160 + gridX;
		data.m_nextGridInstance = this.m_citizenGrid[num];
		this.m_citizenGrid[num] = instance;
	}

	public void RemoveFromGrid(ushort instance, ref CitizenInstance data)
	{
		CitizenInstance.Frame lastFrameData = data.GetLastFrameData();
		int gridX = Mathf.Clamp((int)(lastFrameData.m_position.x / 8f + 1080f), 0, 2159);
		int gridZ = Mathf.Clamp((int)(lastFrameData.m_position.z / 8f + 1080f), 0, 2159);
		this.RemoveFromGrid(instance, ref data, gridX, gridZ);
	}

	public void RemoveFromGrid(ushort instance, ref CitizenInstance data, int gridX, int gridZ)
	{
		int num = gridZ * 2160 + gridX;
		ushort num2 = 0;
		ushort num3 = this.m_citizenGrid[num];
		int num4 = 0;
		while (num3 != 0)
		{
			if (num3 == instance)
			{
				if (num2 == 0)
				{
					this.m_citizenGrid[num] = data.m_nextGridInstance;
				}
				else
				{
					this.m_instances.m_buffer[(int)num2].m_nextGridInstance = data.m_nextGridInstance;
				}
				break;
			}
			num2 = num3;
			num3 = this.m_instances.m_buffer[(int)num3].m_nextGridInstance;
			if (++num4 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_nextGridInstance = 0;
	}

	private int GetGroupIndex(ItemClass.Service service, Citizen.Gender gender, Citizen.SubCulture subCulture, Citizen.AgePhase agePhase)
	{
		int num;
		if (subCulture != Citizen.SubCulture.Generic)
		{
			num = subCulture + 20 - Citizen.SubCulture.Hippie;
		}
		else
		{
			num = service - ItemClass.Service.Residential;
		}
		num = (int)(num * 2 + gender);
		return (int)(num * 14 + agePhase);
	}

	private int GetGroupIndex(ItemClass.Service service, ItemClass.SubService subService)
	{
		int result;
		if (subService != ItemClass.SubService.None)
		{
			result = subService + 20 - ItemClass.SubService.ResidentialLow;
		}
		else
		{
			result = service - ItemClass.Service.Residential;
		}
		return result;
	}

	public CitizenInfo GetGroupCitizenInfo(ref Randomizer r, ItemClass.Service service, Citizen.Gender gender, Citizen.SubCulture subCulture, Citizen.AgePhase agePhase)
	{
		if (!this.m_citizensRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random citizens not refreshed yet!");
			return null;
		}
		int num = this.GetGroupIndex(service, gender, subCulture, agePhase);
		FastList<ushort> fastList = this.m_groupCitizens[num];
		if (fastList == null)
		{
			return null;
		}
		if (fastList.m_size == 0)
		{
			return null;
		}
		num = r.Int32((uint)fastList.m_size);
		return PrefabCollection<CitizenInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	public CitizenInfo GetGroupAnimalInfo(ref Randomizer r, ItemClass.Service service, ItemClass.SubService subService)
	{
		if (!this.m_citizensRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random citizens not refreshed yet!");
			return null;
		}
		int num = this.GetGroupIndex(service, subService);
		FastList<ushort> fastList = this.m_groupAnimals[num];
		if (fastList == null)
		{
			return null;
		}
		if (fastList.m_size == 0)
		{
			return null;
		}
		num = r.Int32((uint)fastList.m_size);
		return PrefabCollection<CitizenInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	private void RefreshGroupCitizens()
	{
		int num = this.m_groupCitizens.Length;
		int num2 = this.m_groupAnimals.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_groupCitizens[i] = null;
		}
		for (int j = 0; j < num2; j++)
		{
			this.m_groupAnimals[j] = null;
		}
		int num3 = PrefabCollection<CitizenInfo>.PrefabCount();
		for (int k = 0; k < num3; k++)
		{
			CitizenInfo prefab = PrefabCollection<CitizenInfo>.GetPrefab((uint)k);
			if (prefab != null && prefab.m_placementStyle == ItemClass.Placement.Automatic)
			{
				if (prefab.m_citizenAI.IsAnimal())
				{
					int groupIndex = this.GetGroupIndex(prefab.m_class.m_service, prefab.m_class.m_subService);
					if (this.m_groupAnimals[groupIndex] == null)
					{
						this.m_groupAnimals[groupIndex] = new FastList<ushort>();
					}
					this.m_groupAnimals[groupIndex].Add((ushort)k);
				}
				else
				{
					int groupIndex2 = this.GetGroupIndex(prefab.m_class.m_service, prefab.m_gender, prefab.m_subCulture, prefab.m_agePhase);
					if (this.m_groupCitizens[groupIndex2] == null)
					{
						this.m_groupCitizens[groupIndex2] = new FastList<ushort>();
					}
					this.m_groupCitizens[groupIndex2].Add((ushort)k);
				}
			}
		}
		int num4 = 25;
		for (int l = 0; l < num4; l++)
		{
			for (int m = 0; m < 2; m++)
			{
				for (int n = 1; n < 14; n++)
				{
					int num5 = l;
					num5 = num5 * 2 + m;
					num5 = num5 * 14 + n;
					FastList<ushort> fastList = this.m_groupCitizens[num5];
					FastList<ushort> fastList2 = this.m_groupCitizens[num5 - 1];
					if (fastList == null && fastList2 != null)
					{
						this.m_groupCitizens[num5] = fastList2;
					}
				}
			}
		}
		this.m_citizensRefreshed = true;
	}

	public bool RayCast(Segment3 ray, CitizenInstance.Flags ignoreFlags, out Vector3 hit, out ushort instanceIndex)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			Vector3 normalized = vector.get_normalized();
			float num = 2f;
			instanceIndex = 0;
			Vector3 vector2 = ray.a - normalized * 5f;
			Vector3 vector3 = ray.a + Vector3.ClampMagnitude(ray.b - ray.a, 400f) + normalized * 5f;
			int num2 = (int)(vector2.x / 8f + 1080f);
			int num3 = (int)(vector2.z / 8f + 1080f);
			int num4 = (int)(vector3.x / 8f + 1080f);
			int num5 = (int)(vector3.z / 8f + 1080f);
			float num6 = Mathf.Abs(vector.x);
			float num7 = Mathf.Abs(vector.z);
			int num8;
			int num9;
			if (num6 >= num7)
			{
				num8 = ((vector.x <= 0f) ? -1 : 1);
				num9 = 0;
				if (num6 != 0f)
				{
					vector *= 8f / num6;
				}
			}
			else
			{
				num8 = 0;
				num9 = ((vector.z <= 0f) ? -1 : 1);
				if (num7 != 0f)
				{
					vector *= 8f / num7;
				}
			}
			Vector3 vector4 = vector2;
			Vector3 vector5 = vector2;
			do
			{
				Vector3 vector6 = vector5 + vector;
				int num10;
				int num11;
				int num12;
				int num13;
				if (num8 != 0)
				{
					num10 = Mathf.Max(num2, 0);
					num11 = Mathf.Min(num2, 2159);
					num12 = Mathf.Max((int)((Mathf.Min(vector4.z, vector6.z) - 5f) / 8f + 1080f), 0);
					num13 = Mathf.Min((int)((Mathf.Max(vector4.z, vector6.z) + 5f) / 8f + 1080f), 2159);
				}
				else
				{
					num12 = Mathf.Max(num3, 0);
					num13 = Mathf.Min(num3, 2159);
					num10 = Mathf.Max((int)((Mathf.Min(vector4.x, vector6.x) - 5f) / 8f + 1080f), 0);
					num11 = Mathf.Min((int)((Mathf.Max(vector4.x, vector6.x) + 5f) / 8f + 1080f), 2159);
				}
				for (int i = num12; i <= num13; i++)
				{
					for (int j = num10; j <= num11; j++)
					{
						ushort num14 = this.m_citizenGrid[i * 2160 + j];
						int num15 = 0;
						while (num14 != 0)
						{
							float num16;
							if (this.m_instances.m_buffer[(int)num14].RayCast(num14, ray, ignoreFlags, out num16) && num16 < num)
							{
								num = num16;
								instanceIndex = num14;
							}
							num14 = this.m_instances.m_buffer[(int)num14].m_nextGridInstance;
							if (++num15 > 1048576)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector4 = vector5;
				vector5 = vector6;
				num2 += num8;
				num3 += num9;
			}
			while ((num2 <= num4 || num8 <= 0) && (num2 >= num4 || num8 >= 0) && (num3 <= num5 || num9 <= 0) && (num3 >= num5 || num9 >= 0));
			if (num != 2f)
			{
				hit = ray.a + (ray.b - ray.a) * num;
				return true;
			}
		}
		hit = Vector3.get_zero();
		instanceIndex = 0;
		return false;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep != 0)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u);
			int num2 = num * 256;
			int num3 = (num + 1) * 256 - 1;
			for (int i = num2; i <= num3; i++)
			{
				if ((this.m_citizens.m_buffer[i].m_flags & Citizen.Flags.Created) != Citizen.Flags.None)
				{
					CitizenInfo citizenInfo = this.m_citizens.m_buffer[i].GetCitizenInfo((uint)i);
					if (citizenInfo == null)
					{
						this.ReleaseCitizen((uint)i);
					}
					else
					{
						citizenInfo.m_citizenAI.SimulationStep((uint)i, ref this.m_citizens.m_buffer[i]);
					}
				}
			}
			if (num == 4095)
			{
				this.m_finalOldestOriginalResident = this.m_tempOldestOriginalResident;
				this.m_tempOldestOriginalResident = 0;
			}
		}
		if (subStep != 0)
		{
			int num4 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u);
			int num5 = num4 * 128;
			int num6 = (num4 + 1) * 128 - 1;
			for (int j = num5; j <= num6; j++)
			{
				if ((ushort)(this.m_units.m_buffer[j].m_flags & CitizenUnit.Flags.Created) != 0)
				{
					this.m_units.m_buffer[j].SimulationStep((uint)j);
				}
			}
		}
		if (subStep != 0)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			Vector3 physicsLodRefPos = instance.m_simulationView.m_position + instance.m_simulationView.m_direction * 200f;
			int num7 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 15u);
			int num8 = num7 * 4096;
			int num9 = (num7 + 1) * 4096 - 1;
			for (int k = num8; k <= num9; k++)
			{
				if ((this.m_instances.m_buffer[k].m_flags & CitizenInstance.Flags.Created) != CitizenInstance.Flags.None)
				{
					CitizenInfo info = this.m_instances.m_buffer[k].Info;
					info.m_citizenAI.SimulationStep((ushort)k, ref this.m_instances.m_buffer[k], physicsLodRefPos);
				}
			}
		}
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetCitizenName(uint citizenID, string name)
	{
		CitizenManager.<SetCitizenName>c__Iterator0 <SetCitizenName>c__Iterator = new CitizenManager.<SetCitizenName>c__Iterator0();
		<SetCitizenName>c__Iterator.citizenID = citizenID;
		<SetCitizenName>c__Iterator.name = name;
		<SetCitizenName>c__Iterator.$this = this;
		return <SetCitizenName>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetInstanceName(ushort instanceID, string name)
	{
		CitizenManager.<SetInstanceName>c__Iterator1 <SetInstanceName>c__Iterator = new CitizenManager.<SetInstanceName>c__Iterator1();
		<SetInstanceName>c__Iterator.instanceID = instanceID;
		<SetInstanceName>c__Iterator.name = name;
		<SetInstanceName>c__Iterator.$this = this;
		return <SetInstanceName>c__Iterator;
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("CitizenManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 65536; i++)
		{
			if (this.m_instances.m_buffer[i].m_flags != CitizenInstance.Flags.None && this.m_instances.m_buffer[i].Info == null)
			{
				this.ReleaseCitizenInstance((ushort)i);
			}
		}
		this.m_infoCount = PrefabCollection<CitizenInfo>.PrefabCount();
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new CitizenManager.Data());
	}

	public string GetDefaultCitizenName(uint citizenID)
	{
		return CitizenAI.GenerateCitizenName(citizenID, (byte)citizenID);
	}

	public string GetCitizenName(uint citizenID)
	{
		if (this.m_citizens.m_buffer[(int)((UIntPtr)citizenID)].m_flags != Citizen.Flags.None)
		{
			string text = null;
			if ((this.m_citizens.m_buffer[(int)((UIntPtr)citizenID)].m_flags & Citizen.Flags.CustomName) != Citizen.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.Citizen = citizenID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = CitizenAI.GenerateCitizenName(citizenID, this.m_citizens.m_buffer[(int)((UIntPtr)citizenID)].m_family);
			}
			return text;
		}
		return null;
	}

	public string GetDefaultInstanceName(ushort instanceID)
	{
		return this.GenerateInstanceName(instanceID, false);
	}

	public string GetInstanceName(ushort instanceID)
	{
		if (this.m_instances.m_buffer[(int)instanceID].m_flags != CitizenInstance.Flags.None)
		{
			string text = null;
			if ((this.m_instances.m_buffer[(int)instanceID].m_flags & CitizenInstance.Flags.CustomName) != CitizenInstance.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.CitizenInstance = instanceID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateInstanceName(instanceID, true);
			}
			return text;
		}
		return null;
	}

	private string GenerateInstanceName(ushort instanceID, bool useCitizen)
	{
		string text = null;
		CitizenInfo info = this.m_instances.m_buffer[(int)instanceID].Info;
		if (info != null)
		{
			text = info.m_citizenAI.GenerateName(instanceID, useCitizen);
		}
		if (text == null)
		{
			text = "Invalid";
		}
		return text;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const float CITIZENGRID_CELL_SIZE = 8f;

	public const int CITIZENGRID_RESOLUTION = 2160;

	public const int MAX_CITIZEN_COUNT = 1048576;

	public const int MAX_UNIT_COUNT = 524288;

	public const int MAX_INSTANCE_COUNT = 65536;

	public int m_citizenCount;

	public int m_unitCount;

	public int m_instanceCount;

	public int m_infoCount;

	[NonSerialized]
	public Array32<Citizen> m_citizens;

	[NonSerialized]
	public Array32<CitizenUnit> m_units;

	[NonSerialized]
	public Array16<CitizenInstance> m_instances;

	[NonSerialized]
	public ushort[] m_citizenGrid;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_Speed;

	[NonSerialized]
	public int ID_State;

	[NonSerialized]
	public int ID_CitizenLocation;

	[NonSerialized]
	public int ID_CitizenColor;

	[NonSerialized]
	public AudioGroup m_audioGroup;

	[NonSerialized]
	public int m_citizenLayer;

	[NonSerialized]
	public int m_undergroundLayer;

	[NonSerialized]
	public int m_tempOldestOriginalResident;

	[NonSerialized]
	public int m_finalOldestOriginalResident;

	[NonSerialized]
	public int m_fullyEducatedOriginalResidents;

	private FastList<ushort>[] m_groupCitizens;

	private FastList<ushort>[] m_groupAnimals;

	private bool m_citizensRefreshed;

	private ulong[] m_renderBuffer;

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "CitizenManager");
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			Citizen[] buffer = instance.m_citizens.m_buffer;
			CitizenUnit[] buffer2 = instance.m_units.m_buffer;
			CitizenInstance[] buffer3 = instance.m_instances.m_buffer;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			int num3 = buffer3.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int j = 1; j < num; j++)
			{
				if (buffer[j].m_flags != Citizen.Flags.None)
				{
					@byte.Write(buffer[j].m_health);
				}
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != Citizen.Flags.None)
				{
					byte2.Write(buffer[k].m_wellbeing);
				}
			}
			byte2.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != Citizen.Flags.None)
				{
					byte3.Write(buffer[l].m_age);
				}
			}
			byte3.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int m = 1; m < num; m++)
			{
				if (buffer[m].m_flags != Citizen.Flags.None)
				{
					byte4.Write(buffer[m].m_family);
				}
			}
			byte4.EndWrite();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int n = 1; n < num2; n++)
			{
				uShort.Write((ushort)buffer2[n].m_flags);
			}
			uShort.EndWrite();
			for (int num4 = 1; num4 < num2; num4++)
			{
				if (buffer2[num4].m_flags != CitizenUnit.Flags.None)
				{
					uint num5 = 0u;
					if (buffer2[num4].m_citizen0 != 0u)
					{
						num5 |= 1u;
					}
					if (buffer2[num4].m_citizen1 != 0u)
					{
						num5 |= 2u;
					}
					if (buffer2[num4].m_citizen2 != 0u)
					{
						num5 |= 4u;
					}
					if (buffer2[num4].m_citizen3 != 0u)
					{
						num5 |= 8u;
					}
					if (buffer2[num4].m_citizen4 != 0u)
					{
						num5 |= 16u;
					}
					if (buffer2[num4].m_nextUnit != 0u)
					{
						num5 |= 32u;
					}
					s.WriteUInt8(num5);
					if (buffer2[num4].m_citizen0 != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_citizen0);
					}
					if (buffer2[num4].m_citizen1 != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_citizen1);
					}
					if (buffer2[num4].m_citizen2 != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_citizen2);
					}
					if (buffer2[num4].m_citizen3 != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_citizen3);
					}
					if (buffer2[num4].m_citizen4 != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_citizen4);
					}
					if (buffer2[num4].m_nextUnit != 0u)
					{
						s.WriteUInt24(buffer2[num4].m_nextUnit);
					}
				}
			}
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int num6 = 1; num6 < num2; num6++)
			{
				if (buffer2[num6].m_flags != CitizenUnit.Flags.None)
				{
					uShort2.Write(buffer2[num6].m_goods);
				}
			}
			uShort2.EndWrite();
			EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginWrite(s);
			for (int num7 = 1; num7 < num3; num7++)
			{
				uInt2.Write((uint)buffer3[num7].m_flags);
			}
			uInt2.EndWrite();
			try
			{
				PrefabCollection<CitizenInfo>.BeginSerialize(s);
				for (int num8 = 1; num8 < num3; num8++)
				{
					if (buffer3[num8].m_flags != CitizenInstance.Flags.None)
					{
						PrefabCollection<CitizenInfo>.Serialize((uint)buffer3[num8].m_infoIndex);
					}
				}
			}
			finally
			{
				PrefabCollection<CitizenInfo>.EndSerialize(s);
			}
			for (int num9 = 1; num9 < num3; num9++)
			{
				if (buffer3[num9].m_flags != CitizenInstance.Flags.None)
				{
					CitizenInstance.Frame lastFrameData = buffer3[num9].GetLastFrameData();
					s.WriteVector3(lastFrameData.m_velocity);
					s.WriteVector3(lastFrameData.m_position);
					s.WriteVector4(buffer3[num9].m_targetPos);
					s.WriteVector2(buffer3[num9].m_targetDir);
					if ((buffer3[num9].m_flags & CitizenInstance.Flags.CustomColor) != CitizenInstance.Flags.None)
					{
						s.WriteUInt8((uint)buffer3[num9].m_color.r);
						s.WriteUInt8((uint)buffer3[num9].m_color.g);
						s.WriteUInt8((uint)buffer3[num9].m_color.b);
					}
					s.WriteUInt24(buffer3[num9].m_citizen);
					s.WriteUInt24(buffer3[num9].m_path);
					s.WriteUInt8((uint)buffer3[num9].m_pathPositionIndex);
					s.WriteUInt8((uint)buffer3[num9].m_lastPathOffset);
					s.WriteUInt8((uint)buffer3[num9].m_waitCounter);
					s.WriteUInt8((uint)buffer3[num9].m_targetSeed);
					s.WriteUInt16((uint)buffer3[num9].m_sourceBuilding);
					s.WriteUInt16((uint)buffer3[num9].m_targetBuilding);
				}
			}
			s.WriteInt16(instance.m_tempOldestOriginalResident);
			s.WriteInt16(instance.m_finalOldestOriginalResident);
			s.WriteInt32(instance.m_fullyEducatedOriginalResidents);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "CitizenManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "CitizenManager");
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			Citizen[] buffer = instance.m_citizens.m_buffer;
			CitizenUnit[] buffer2 = instance.m_units.m_buffer;
			CitizenInstance[] buffer3 = instance.m_instances.m_buffer;
			ushort[] citizenGrid = instance.m_citizenGrid;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			int num3 = buffer3.Length;
			int num4 = citizenGrid.Length;
			instance.m_citizens.ClearUnused();
			instance.m_units.ClearUnused();
			instance.m_instances.ClearUnused();
			for (int i = 0; i < num4; i++)
			{
				citizenGrid[i] = 0;
			}
			if (s.get_version() < 30u)
			{
				EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
				for (int j = 1; j < num; j++)
				{
					buffer[j].m_flags = (Citizen.Flags)@byte.Read();
					buffer[j].m_homeBuilding = 0;
					buffer[j].m_workBuilding = 0;
					buffer[j].m_visitBuilding = 0;
					buffer[j].m_vehicle = 0;
					buffer[j].m_parkedVehicle = 0;
					buffer[j].m_instance = 0;
					buffer[j].m_health = 0;
					buffer[j].m_wellbeing = 0;
					buffer[j].m_age = 0;
					buffer[j].m_family = 0;
				}
				@byte.EndRead();
			}
			else
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int k = 1; k < num; k++)
				{
					buffer[k].m_flags = (Citizen.Flags)uInt.Read();
					buffer[k].m_homeBuilding = 0;
					buffer[k].m_workBuilding = 0;
					buffer[k].m_visitBuilding = 0;
					buffer[k].m_vehicle = 0;
					buffer[k].m_parkedVehicle = 0;
					buffer[k].m_instance = 0;
					buffer[k].m_health = 0;
					buffer[k].m_wellbeing = 0;
					buffer[k].m_age = 0;
					buffer[k].m_family = 0;
					if (buffer[k].m_flags != Citizen.Flags.None)
					{
						if (s.get_version() < 157u)
						{
							buffer[k].m_age = (byte)(((buffer[k].m_flags & (Citizen.Flags.Student | Citizen.Flags.MovingIn | Citizen.Flags.DummyTraffic | Citizen.Flags.Criminal | Citizen.Flags.Arrested | Citizen.Flags.Evacuating | Citizen.Flags.Collapsed)) >> 4) * (Citizen.Flags.Created | Citizen.Flags.Tourist));
							Citizen[] expr_271_cp_0 = buffer;
							int expr_271_cp_1 = k;
							expr_271_cp_0[expr_271_cp_1].m_flags = (expr_271_cp_0[expr_271_cp_1].m_flags & ~(Citizen.Flags.Student | Citizen.Flags.MovingIn | Citizen.Flags.DummyTraffic | Citizen.Flags.Criminal | Citizen.Flags.Arrested | Citizen.Flags.Evacuating | Citizen.Flags.Collapsed));
						}
					}
					else
					{
						instance.m_citizens.ReleaseItem((uint)k);
					}
				}
				uInt.EndRead();
			}
			if (s.get_version() >= 65u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int l = 1; l < num; l++)
				{
					if (buffer[l].m_flags != Citizen.Flags.None)
					{
						buffer[l].m_health = byte2.Read();
					}
				}
				byte2.EndRead();
			}
			if (s.get_version() >= 65u)
			{
				EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
				for (int m = 1; m < num; m++)
				{
					if (buffer[m].m_flags != Citizen.Flags.None)
					{
						buffer[m].m_wellbeing = byte3.Read();
					}
				}
				byte3.EndRead();
			}
			if (s.get_version() >= 157u)
			{
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int n = 1; n < num; n++)
				{
					if (buffer[n].m_flags != Citizen.Flags.None)
					{
						buffer[n].m_age = byte4.Read();
					}
				}
				byte4.EndRead();
			}
			if (s.get_version() >= 190u)
			{
				EncodedArray.Byte byte5 = EncodedArray.Byte.BeginRead(s);
				for (int num5 = 1; num5 < num; num5++)
				{
					if (buffer[num5].m_flags != Citizen.Flags.None)
					{
						buffer[num5].m_family = byte5.Read();
					}
				}
				byte5.EndRead();
			}
			if (s.get_version() < 30u)
			{
				for (int num6 = 1; num6 < num; num6++)
				{
					if (buffer[num6].m_flags != Citizen.Flags.None)
					{
						if (s.get_version() < 23u)
						{
							s.ReadUInt8();
						}
					}
					else
					{
						instance.m_citizens.ReleaseItem((uint)num6);
					}
				}
			}
			if (s.get_version() >= 23u && s.get_version() < 30u)
			{
				EncodedArray.Byte byte6 = EncodedArray.Byte.BeginRead(s);
				for (int num7 = 1; num7 < num; num7++)
				{
					if (buffer[num7].m_flags != Citizen.Flags.None)
					{
						byte6.Read();
					}
				}
				byte6.EndRead();
			}
			if (s.get_version() >= 23u && s.get_version() < 30u)
			{
				EncodedArray.Byte byte7 = EncodedArray.Byte.BeginRead(s);
				for (int num8 = 1; num8 < num; num8++)
				{
					if (buffer[num8].m_flags != Citizen.Flags.None)
					{
						byte7.Read();
					}
				}
				byte7.EndRead();
			}
			if (s.get_version() >= 23u && s.get_version() < 30u)
			{
				EncodedArray.Byte byte8 = EncodedArray.Byte.BeginRead(s);
				for (int num9 = 1; num9 < num; num9++)
				{
					if (buffer[num9].m_flags != Citizen.Flags.None)
					{
						byte8.Read();
					}
				}
				byte8.EndRead();
			}
			if (s.get_version() >= 23u && s.get_version() < 30u)
			{
				EncodedArray.Byte byte9 = EncodedArray.Byte.BeginRead(s);
				for (int num10 = 1; num10 < num; num10++)
				{
					if (buffer[num10].m_flags != Citizen.Flags.None)
					{
						byte9.Read();
					}
				}
				byte9.EndRead();
			}
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int num11 = 1; num11 < num2; num11++)
			{
				buffer2[num11].m_flags = (CitizenUnit.Flags)uShort.Read();
			}
			uShort.EndRead();
			for (int num12 = 1; num12 < num2; num12++)
			{
				buffer2[num12].m_citizen0 = 0u;
				buffer2[num12].m_citizen1 = 0u;
				buffer2[num12].m_citizen2 = 0u;
				buffer2[num12].m_citizen3 = 0u;
				buffer2[num12].m_citizen4 = 0u;
				buffer2[num12].m_nextUnit = 0u;
				buffer2[num12].m_building = 0;
				if (buffer2[num12].m_flags != CitizenUnit.Flags.None)
				{
					if (s.get_version() >= 69u)
					{
						uint num13 = s.ReadUInt8();
						if ((num13 & 1u) != 0u)
						{
							buffer2[num12].m_citizen0 = s.ReadUInt24();
						}
						if ((num13 & 2u) != 0u)
						{
							buffer2[num12].m_citizen1 = s.ReadUInt24();
						}
						if ((num13 & 4u) != 0u)
						{
							buffer2[num12].m_citizen2 = s.ReadUInt24();
						}
						if ((num13 & 8u) != 0u)
						{
							buffer2[num12].m_citizen3 = s.ReadUInt24();
						}
						if ((num13 & 16u) != 0u)
						{
							buffer2[num12].m_citizen4 = s.ReadUInt24();
						}
						if ((num13 & 32u) != 0u)
						{
							buffer2[num12].m_nextUnit = s.ReadUInt24();
						}
					}
					else
					{
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)2048) != CitizenUnit.Flags.None)
						{
							buffer2[num12].m_citizen0 = s.ReadUInt24();
						}
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)4096) != CitizenUnit.Flags.None)
						{
							buffer2[num12].m_citizen1 = s.ReadUInt24();
						}
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)8192) != CitizenUnit.Flags.None)
						{
							buffer2[num12].m_citizen2 = s.ReadUInt24();
						}
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)16384) != CitizenUnit.Flags.None)
						{
							buffer2[num12].m_citizen3 = s.ReadUInt24();
						}
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)32768) != CitizenUnit.Flags.None)
						{
							buffer2[num12].m_citizen4 = s.ReadUInt24();
						}
						if ((buffer2[num12].m_flags & (CitizenUnit.Flags)2) == CitizenUnit.Flags.None)
						{
							buffer2[num12].m_nextUnit = s.ReadUInt24();
						}
						CitizenUnit[] expr_858_cp_0 = buffer2;
						int expr_858_cp_1 = num12;
						expr_858_cp_0[expr_858_cp_1].m_flags = (expr_858_cp_0[expr_858_cp_1].m_flags & (CitizenUnit.Flags)2045);
					}
				}
				else
				{
					instance.m_units.ReleaseItem((uint)num12);
				}
			}
			if (s.get_version() >= 69u)
			{
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
				for (int num14 = 1; num14 < num2; num14++)
				{
					if (buffer2[num14].m_flags != CitizenUnit.Flags.None)
					{
						buffer2[num14].m_goods = uShort2.Read();
					}
					else
					{
						buffer2[num14].m_goods = 0;
					}
				}
				uShort2.EndRead();
			}
			else
			{
				for (int num15 = 1; num15 < num2; num15++)
				{
					buffer2[num15].m_goods = 0;
				}
			}
			if (s.get_version() >= 204u)
			{
				EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginRead(s);
				for (int num16 = 1; num16 < num3; num16++)
				{
					buffer3[num16].m_flags = (CitizenInstance.Flags)uInt2.Read();
				}
				uInt2.EndRead();
			}
			else if (s.get_version() >= 44u)
			{
				EncodedArray.UInt uInt3 = EncodedArray.UInt.BeginRead(s);
				for (int num17 = 1; num17 < 32768; num17++)
				{
					buffer3[num17].m_flags = (CitizenInstance.Flags)uInt3.Read();
				}
				for (int num18 = 32768; num18 < num3; num18++)
				{
					buffer3[num18].m_flags = CitizenInstance.Flags.None;
				}
				uInt3.EndRead();
			}
			else
			{
				for (int num19 = 1; num19 < num3; num19++)
				{
					buffer3[num19] = default(CitizenInstance);
					instance.m_instances.ReleaseItem((ushort)num19);
				}
			}
			if (s.get_version() >= 44u)
			{
				PrefabCollection<CitizenInfo>.BeginDeserialize(s);
				for (int num20 = 1; num20 < num3; num20++)
				{
					if (buffer3[num20].m_flags != CitizenInstance.Flags.None)
					{
						buffer3[num20].m_infoIndex = (ushort)PrefabCollection<CitizenInfo>.Deserialize(true);
					}
				}
				PrefabCollection<CitizenInfo>.EndDeserialize(s);
			}
			if (s.get_version() >= 44u)
			{
				for (int num21 = 1; num21 < num3; num21++)
				{
					buffer3[num21].m_nextGridInstance = 0;
					buffer3[num21].m_nextSourceInstance = 0;
					buffer3[num21].m_nextTargetInstance = 0;
					buffer3[num21].m_lastFrame = 0;
					if (buffer3[num21].m_flags != CitizenInstance.Flags.None)
					{
						if (s.get_version() >= 46u)
						{
							buffer3[num21].m_frame0.m_velocity = s.ReadVector3();
						}
						else
						{
							buffer3[num21].m_frame0.m_velocity = Vector3.get_zero();
						}
						buffer3[num21].m_frame0.m_position = s.ReadVector3();
						buffer3[num21].m_frame0.m_rotation = Quaternion.get_identity();
						buffer3[num21].m_frame0.m_underground = ((buffer3[num21].m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None);
						buffer3[num21].m_frame0.m_transition = ((buffer3[num21].m_flags & CitizenInstance.Flags.Transition) != CitizenInstance.Flags.None);
						buffer3[num21].m_frame0.m_insideBuilding = ((buffer3[num21].m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None);
						buffer3[num21].m_frame1 = buffer3[num21].m_frame0;
						buffer3[num21].m_frame2 = buffer3[num21].m_frame0;
						buffer3[num21].m_frame3 = buffer3[num21].m_frame0;
						if (s.get_version() >= 195u)
						{
							buffer3[num21].m_targetPos = s.ReadVector4();
						}
						else if (s.get_version() >= 47u)
						{
							buffer3[num21].m_targetPos = s.ReadVector3();
						}
						else
						{
							buffer3[num21].m_targetPos = buffer3[num21].m_frame0.m_position;
						}
						if (s.get_version() >= 200u)
						{
							buffer3[num21].m_targetDir = s.ReadVector2();
						}
						else
						{
							buffer3[num21].m_targetDir = Vector2.get_zero();
						}
						if (s.get_version() >= 247u && (buffer3[num21].m_flags & CitizenInstance.Flags.CustomColor) != CitizenInstance.Flags.None)
						{
							buffer3[num21].m_color.r = (byte)s.ReadUInt8();
							buffer3[num21].m_color.g = (byte)s.ReadUInt8();
							buffer3[num21].m_color.b = (byte)s.ReadUInt8();
							buffer3[num21].m_color.a = 255;
						}
						else
						{
							buffer3[num21].m_color = default(Color32);
						}
						buffer3[num21].m_citizen = s.ReadUInt24();
						if (s.get_version() >= 46u)
						{
							buffer3[num21].m_path = s.ReadUInt24();
							buffer3[num21].m_pathPositionIndex = (byte)s.ReadUInt8();
							buffer3[num21].m_lastPathOffset = (byte)s.ReadUInt8();
						}
						else
						{
							buffer3[num21].m_path = 0u;
							buffer3[num21].m_pathPositionIndex = 0;
							buffer3[num21].m_lastPathOffset = 0;
						}
						if (s.get_version() >= 86u)
						{
							buffer3[num21].m_waitCounter = (byte)s.ReadUInt8();
						}
						else
						{
							buffer3[num21].m_waitCounter = 0;
						}
						if (s.get_version() >= 201u)
						{
							buffer3[num21].m_targetSeed = (byte)s.ReadUInt8();
						}
						else
						{
							buffer3[num21].m_targetSeed = 0;
						}
						if (s.get_version() >= 180u)
						{
							buffer3[num21].m_sourceBuilding = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer3[num21].m_sourceBuilding = 0;
						}
						buffer3[num21].m_targetBuilding = (ushort)s.ReadUInt16();
						instance.InitializeInstance((ushort)num21, ref buffer3[num21]);
					}
					else
					{
						buffer3[num21].m_frame0 = default(CitizenInstance.Frame);
						buffer3[num21].m_frame1 = default(CitizenInstance.Frame);
						buffer3[num21].m_frame2 = default(CitizenInstance.Frame);
						buffer3[num21].m_frame3 = default(CitizenInstance.Frame);
						buffer3[num21].m_targetPos = Vector3.get_zero();
						buffer3[num21].m_targetDir = Vector2.get_zero();
						buffer3[num21].m_color = default(Color32);
						buffer3[num21].m_citizen = 0u;
						buffer3[num21].m_path = 0u;
						buffer3[num21].m_pathPositionIndex = 0;
						buffer3[num21].m_lastPathOffset = 0;
						buffer3[num21].m_waitCounter = 0;
						buffer3[num21].m_targetSeed = 0;
						buffer3[num21].m_sourceBuilding = 0;
						buffer3[num21].m_targetBuilding = 0;
						instance.m_instances.ReleaseItem((ushort)num21);
					}
				}
			}
			if (s.get_version() >= 169u)
			{
				instance.m_tempOldestOriginalResident = s.ReadInt16();
				instance.m_finalOldestOriginalResident = s.ReadInt16();
			}
			else
			{
				instance.m_tempOldestOriginalResident = 0;
				instance.m_finalOldestOriginalResident = 0;
			}
			if (s.get_version() >= 179u)
			{
				instance.m_fullyEducatedOriginalResidents = s.ReadInt32();
			}
			else
			{
				instance.m_fullyEducatedOriginalResidents = 0;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "CitizenManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "CitizenManager");
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<CitizenInfo>.BindPrefabs();
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			instance.RefreshGroupCitizens();
			Citizen[] buffer = instance.m_citizens.m_buffer;
			CitizenInstance[] buffer2 = instance.m_instances.m_buffer;
			int num = buffer2.Length;
			for (int i = 1; i < num; i++)
			{
				if (buffer2[i].m_flags != CitizenInstance.Flags.None)
				{
					CitizenInfo info = buffer2[i].Info;
					if (info != null)
					{
						buffer2[i].m_infoIndex = (ushort)info.m_prefabDataIndex;
						info.m_citizenAI.LoadInstance((ushort)i, ref buffer2[i]);
					}
					uint citizen = buffer2[i].m_citizen;
					if (citizen != 0u)
					{
						if (buffer[(int)((UIntPtr)citizen)].m_instance != 0)
						{
							CODebugBase<LogChannel>.Warn(LogChannel.Core, "Citizen has a clone: " + citizen);
							buffer2[i].m_citizen = 0u;
						}
						else
						{
							buffer[(int)((UIntPtr)citizen)].m_instance = (ushort)i;
						}
					}
					if (buffer2[i].m_path != 0u)
					{
						PathUnit[] expr_140_cp_0 = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer;
						UIntPtr expr_140_cp_1 = (UIntPtr)buffer2[i].m_path;
						expr_140_cp_0[(int)expr_140_cp_1].m_referenceCount = expr_140_cp_0[(int)expr_140_cp_1].m_referenceCount + 1;
					}
				}
			}
			instance.m_citizenCount = (int)(instance.m_citizens.ItemCount() - 1u);
			instance.m_unitCount = (int)(instance.m_units.ItemCount() - 1u);
			instance.m_instanceCount = (int)(instance.m_instances.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "CitizenManager");
		}
	}
}
