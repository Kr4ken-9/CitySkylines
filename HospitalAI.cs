﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class HospitalAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Health)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return StringUtils.SafeFormat("Electricity: {0}\nWater: {1} ({2}% polluted)\nSewage: {3}\nGarbage: {4}\nCrime: {5}\nSick: {6}", new object[]
		{
			data.m_electricityBuffer,
			data.m_waterBuffer,
			(int)(data.m_waterPollution * 100 / 255),
			data.m_sewageBuffer,
			data.m_garbageBuffer,
			data.m_crimeBuffer,
			num3
		});
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Health;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, this.m_patientCapacity, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_patientCapacity, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u)
					{
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
						{
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
							{
								instance.ReleaseCitizen(citizen);
							}
						}
						else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
						{
							instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation = Citizen.Location.Home;
						}
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_patientCapacity, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		int healthcareAccumulation = this.GetHealthcareAccumulation();
		if (healthcareAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHealth, position, 1.5f);
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.HealthCare, (float)healthcareAccumulation, this.m_healthCareRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			int healthcareAccumulation = this.GetHealthcareAccumulation();
			if (healthcareAccumulation != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHealth, position, 1.5f);
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.HealthCare, (float)(-(float)healthcareAccumulation), this.m_healthCareRadius);
			}
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempExport = 0;
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Sick)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Sick, offer);
		Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.SickMove, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_healthCareRadius * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = productionRate * this.GetHealthcareAccumulation() / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.HealthCare, num, buildingData.m_position, this.m_healthCareRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int curingRate = this.GetCuringRate();
		int num2 = (curingRate * finalProductionRate * 100 + this.m_patientCapacity - 1) / this.m_patientCapacity;
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num3 = buildingData.m_citizenUnits;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		while (num3 != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num3)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num3)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num3)].GetCitizen(i);
					if (citizen != 0u)
					{
						if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead)
						{
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
							{
								instance.ReleaseCitizen(citizen);
							}
						}
						else if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick)
						{
							if (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
							{
								if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(10000u) < num2 && Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
								{
									instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick = false;
									num6++;
								}
								else
								{
									num5++;
								}
							}
							else
							{
								num5++;
							}
						}
					}
				}
			}
			num3 = nextUnit;
			if (++num4 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		behaviour.m_sickCount += num5;
		buildingData.m_tempExport = (byte)Mathf.Min((int)buildingData.m_tempExport + num6, 255);
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		District[] expr_274_cp_0_cp_0 = instance2.m_districts.m_buffer;
		byte expr_274_cp_0_cp_1 = district;
		expr_274_cp_0_cp_0[(int)expr_274_cp_0_cp_1].m_productionData.m_tempHealCapacity = expr_274_cp_0_cp_0[(int)expr_274_cp_0_cp_1].m_productionData.m_tempHealCapacity + (uint)this.m_patientCapacity;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Sick, ref num7, ref num9, ref num10, ref num11);
		base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Sick, ref num8, ref num9, ref num10, ref num11);
		int num12 = this.m_patientCapacity - num5 - num10;
		int num13 = (finalProductionRate * this.m_ambulanceCount + 99) / 100;
		if (num12 >= 2)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			offer.Position = buildingData.m_position;
			if (num7 < num13)
			{
				offer.Priority = 7;
				offer.Amount = Mathf.Min(num12 >> 1, num13 - num7);
				offer.Active = true;
			}
			else
			{
				offer.Priority = 1;
				offer.Amount = num12 >> 1;
				offer.Active = false;
			}
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Sick, offer);
		}
		if (num12 >= 1)
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Building = buildingID;
			offer2.Position = buildingData.m_position;
			offer2.Priority = 7;
			offer2.Amount = num12 + 1 >> 1;
			offer2.Active = false;
			Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.SickMove, offer2);
		}
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	protected virtual int GetHealthcareAccumulation()
	{
		return this.m_healthCareAccumulation;
	}

	protected virtual int GetCuringRate()
	{
		return this.m_curingRate;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_PATIENT_CAPACITY", new object[]
			{
				this.m_patientCapacity
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Sick)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num4 = num3;
		int patientCapacity = this.m_patientCapacity;
		int finalExport = (int)data.m_finalExport;
		int num5 = (productionRate * this.m_ambulanceCount + 99) / 100;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Sick, ref num6, ref num7, ref num8, ref num9);
		string text = LocaleFormatter.FormatGeneric("AIINFO_PATIENTS", new object[]
		{
			num4,
			patientCapacity
		});
		text = text + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_PATIENTS_TREATED", new object[]
		{
			finalExport
		});
		if (this.m_ambulanceCount != 0)
		{
			text = text + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_AMBULANCES", new object[]
			{
				num6,
				num5
			});
		}
		return text;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 3;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 18;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 21;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 18;

	[CustomizableProperty("Ambulance Count")]
	public int m_ambulanceCount = 10;

	[CustomizableProperty("Patient Capacity")]
	public int m_patientCapacity = 100;

	[CustomizableProperty("Curing Rate")]
	public int m_curingRate = 10;

	[CustomizableProperty("Healthcare Accumulation")]
	public int m_healthCareAccumulation = 100;

	[CustomizableProperty("Healthcare Radius")]
	public float m_healthCareRadius = 400f;
}
