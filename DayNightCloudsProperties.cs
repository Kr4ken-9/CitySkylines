﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class DayNightCloudsProperties : MonoBehaviour
{
	private void OnEnable()
	{
		this.ID_ColorFromSky = Shader.PropertyToID("_ColorFromSky");
		this.ID_ColorFromLight = Shader.PropertyToID("_ColorFromLight");
		this.m_DayNightProperties = base.GetComponent<DayNightProperties>();
		if (this.m_DayNightProperties == null || this.m_CloudMaterial == null)
		{
			base.set_enabled(false);
		}
		else
		{
			this.InitSkydomeMesh();
		}
	}

	private void OnDisable()
	{
		if (this.m_SkydomeMesh)
		{
			Object.DestroyImmediate(this.m_SkydomeMesh);
			this.m_SkydomeMesh = null;
		}
	}

	private void InitSkydomeMesh()
	{
		if (this.m_SkydomeBaseMesh != null)
		{
			Mesh mesh = new Mesh();
			Vector3[] vertices = this.m_SkydomeBaseMesh.get_vertices();
			for (int i = 0; i < vertices.Length; i++)
			{
				Vector3[] expr_31_cp_0 = vertices;
				int expr_31_cp_1 = i;
				expr_31_cp_0[expr_31_cp_1].y = expr_31_cp_0[expr_31_cp_1].y * 0.85f;
			}
			mesh.set_vertices(vertices);
			mesh.set_triangles(this.m_SkydomeBaseMesh.get_triangles());
			mesh.set_normals(this.m_SkydomeBaseMesh.get_normals());
			mesh.set_uv(this.m_SkydomeBaseMesh.get_uv());
			mesh.set_uv2(this.m_SkydomeBaseMesh.get_uv2());
			mesh.set_bounds(new Bounds(Vector3.get_zero(), Vector3.get_one() * 2E+09f));
			mesh.set_hideFlags(52);
			mesh.set_name("SkydomeMesh");
			this.m_SkydomeMesh = mesh;
		}
		else
		{
			base.set_enabled(false);
		}
	}

	private void Update()
	{
		bool useLinearSpace = this.m_DayNightProperties.m_UseLinearSpace;
		float num = Mathf.Max(Mathf.Pow(0.25f, (!useLinearSpace) ? 1f : 1.5f), this.m_DayNightProperties.DayTime);
		num *= Mathf.Sqrt(this.m_DayNightProperties.m_Exposure);
		this.m_CloudMaterial.SetVector(this.ID_ColorFromLight, (!useLinearSpace) ? (this.m_DayNightProperties.currentLightColor * num) : (this.m_DayNightProperties.currentLightColor.get_linear() * num));
		this.m_CloudMaterial.SetVector(this.ID_ColorFromSky, (!useLinearSpace) ? (this.m_DayNightProperties.currentSkyColor * num) : (this.m_DayNightProperties.currentSkyColor.get_linear() * num));
		Graphics.DrawMesh(this.m_SkydomeMesh, Vector3.get_zero(), Quaternion.get_identity(), this.m_CloudMaterial, this.m_CloudLayer);
	}

	private int ID_ColorFromSky;

	private int ID_ColorFromLight;

	public int m_CloudLayer = 29;

	private const float m_NightBrightness = 0.25f;

	public Material m_CloudMaterial;

	public Mesh m_SkydomeBaseMesh;

	private DayNightProperties m_DayNightProperties;

	private Mesh m_SkydomeMesh;
}
