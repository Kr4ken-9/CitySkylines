﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RescueWorkerAI : ServicePersonAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
	}

	public override void ReleaseAI()
	{
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Destruction)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & (CitizenInstance.Flags.Blown | CitizenInstance.Flags.Floating)) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		target = InstanceID.Empty;
		return Locale.Get("RESCUE_WORKER_SEARCHING");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void LoadInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.LoadInstance(instanceID, ref data);
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
		}
	}

	public override void ReleaseInstance(ushort instanceID, ref CitizenInstance data)
	{
		if (data.m_citizen != 0u)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_instance = 0;
			instance.ReleaseCitizen(data.m_citizen);
			data.m_citizen = 0u;
		}
		base.ReleaseInstance(instanceID, ref data);
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (instance.m_citizens.m_buffer[(int)((UIntPtr)data.m_citizen)].m_vehicle == 0)
		{
			instance.ReleaseCitizenInstance(instanceID);
			return;
		}
		if (data.m_waitCounter < 255)
		{
			data.m_waitCounter += 1;
		}
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveTargetCitizen(instanceID, ref data);
			}
			data.m_targetBuilding = targetBuilding;
			if (data.m_targetBuilding != 0)
			{
				instance.m_buildings.m_buffer[(int)data.m_targetBuilding].AddTargetCitizen(instanceID, ref data);
				data.m_waitCounter = 0;
			}
			else
			{
				data.m_flags |= CitizenInstance.Flags.EnteringVehicle;
				data.Spawn(instanceID);
			}
		}
	}

	protected override void GetBuildingTargetPosition(ushort instanceID, ref CitizenInstance citizenData, float minSqrDistance)
	{
		if (citizenData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].Info;
			Vector3 vector;
			Quaternion quaternion;
			instance.m_buildings.m_buffer[(int)citizenData.m_targetBuilding].CalculateMeshPosition(out vector, out quaternion);
			Randomizer randomizer;
			randomizer..ctor((int)instanceID);
			float num = (float)randomizer.Int32(-400, 400) * 0.001f;
			float num2 = (float)randomizer.Int32(-400, 400) * 0.001f;
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			Vector3 vector2 = vector + quaternion * new Vector3(info.m_size.x * num2, 0f, info.m_size.z * num);
			Vector2 targetDir;
			targetDir..ctor(Mathf.Cos(num3), Mathf.Sin(num3));
			citizenData.m_targetPos = new Vector4(vector2.x, vector2.y, vector2.z, 1f);
			citizenData.m_targetDir = targetDir;
			return;
		}
		citizenData.m_targetDir = Vector2.get_zero();
	}

	protected override void Spawn(ushort instanceID, ref CitizenInstance data)
	{
		if ((data.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
		{
			return;
		}
		data.Spawn(instanceID);
		if ((data.m_flags & CitizenInstance.Flags.CannotUseTaxi) == CitizenInstance.Flags.None)
		{
			uint citizen = data.m_citizen;
			if (citizen != 0u)
			{
				Randomizer randomizer;
				randomizer..ctor(citizen);
				CitizenManager instance = Singleton<CitizenManager>.get_instance();
				CitizenInfo groupAnimalInfo = instance.GetGroupAnimalInfo(ref randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
				ushort num;
				if (groupAnimalInfo != null && instance.CreateCitizenInstance(out num, ref randomizer, groupAnimalInfo, 0u))
				{
					groupAnimalInfo.m_citizenAI.SetSource(num, ref instance.m_instances.m_buffer[(int)num], instanceID);
					groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance.m_instances.m_buffer[(int)num], instanceID);
				}
			}
		}
	}

	protected override bool EnterVehicle(ushort instanceID, ref CitizenInstance citizenData)
	{
		citizenData.Unspawn(instanceID);
		uint citizen = citizenData.m_citizen;
		if (citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVehicle(citizen, 0, 0u);
		}
		return true;
	}

	protected override bool ArriveAtTarget(ushort instanceID, ref CitizenInstance citizenData)
	{
		return false;
	}

	protected override void ArriveAtDestination(ushort instanceID, ref CitizenInstance citizenData, bool success)
	{
	}
}
