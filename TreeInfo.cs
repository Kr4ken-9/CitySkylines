﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class TreeInfo : PrefabInfo
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_class == null)
		{
			throw new PrefabException(this, "Class missing");
		}
		if (this.m_generatedInfo == null)
		{
			throw new PrefabException(this, "Generated info missing");
		}
		if (this.m_mesh == null)
		{
			MeshFilter component = base.GetComponent<MeshFilter>();
			this.m_mesh = component.get_sharedMesh();
			this.m_material = base.GetComponent<Renderer>().get_sharedMaterial();
			if (this.m_mesh == null)
			{
				throw new PrefabException(this, "Mesh missing");
			}
			if (this.m_material == null)
			{
				throw new PrefabException(this, "Material missing");
			}
			this.m_defaultColor = this.m_material.get_color();
			this.m_renderScale = Mathf.Max(Mathf.Max(this.m_generatedInfo.m_size.x, this.m_generatedInfo.m_size.y), this.m_generatedInfo.m_size.z);
			this.m_renderOffset = this.m_generatedInfo.m_center.y;
			this.m_renderClip = Mathf.Max(this.m_generatedInfo.m_size.x, this.m_generatedInfo.m_size.z) / this.m_renderScale;
			this.m_lodLocations = new Vector4[16];
			this.m_lodColors = new Vector4[16];
			this.m_lodCount = 0;
			this.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			this.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
		}
		if (this.m_generatedInfo.m_size == Vector3.get_zero())
		{
			throw new PrefabException(this, "Generated info has zero size");
		}
		if (this.m_generatedInfo.m_treeInfo != null)
		{
			if (this.m_generatedInfo.m_treeInfo.m_mesh != this.m_mesh)
			{
				throw new PrefabException(this, "Same generated info but different mesh (" + this.m_generatedInfo.m_treeInfo.get_gameObject().get_name() + ")");
			}
		}
		else
		{
			this.m_generatedInfo.m_treeInfo = this;
		}
		this.RefreshLevelOfDetail();
	}

	public override void DestroyPrefab()
	{
		if (this.m_renderMaterial != null)
		{
			Object.Destroy(this.m_renderMaterial);
			this.m_renderMaterial = null;
		}
		if (this.m_lodMesh1 != null)
		{
			Object.Destroy(this.m_lodMesh1);
			this.m_lodMesh1 = null;
		}
		if (this.m_lodMesh4 != null)
		{
			Object.Destroy(this.m_lodMesh4);
			this.m_lodMesh4 = null;
		}
		if (this.m_lodMesh8 != null)
		{
			Object.Destroy(this.m_lodMesh8);
			this.m_lodMesh8 = null;
		}
		if (this.m_lodMesh16 != null)
		{
			Object.Destroy(this.m_lodMesh16);
			this.m_lodMesh16 = null;
		}
		if (this.m_lodMaterial != null)
		{
			Object.Destroy(this.m_lodMaterial);
			this.m_lodMaterial = null;
		}
		if (this.m_generatedInfo != null && this.m_generatedInfo.m_treeInfo == this)
		{
			this.m_generatedInfo.m_treeInfo = null;
		}
		base.DestroyPrefab();
	}

	public override void RefreshLevelOfDetail()
	{
		this.m_lodRenderDistance = RenderManager.LevelOfDetailFactor * 300f;
	}

	public override void CheckReferences()
	{
		if (this.m_variations != null)
		{
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				if (this.m_variations[i].m_tree != null)
				{
					if (this.m_variations[i].m_tree.m_prefabInitialized)
					{
						this.m_variations[i].m_finalTree = this.m_variations[i].m_tree;
					}
					else
					{
						this.m_variations[i].m_finalTree = PrefabCollection<TreeInfo>.FindLoaded(this.m_variations[i].m_tree.get_gameObject().get_name());
					}
					if (this.m_variations[i].m_finalTree == null)
					{
						throw new PrefabException(this, "Referenced tree is not loaded (" + this.m_variations[i].m_tree.get_gameObject().get_name() + ")");
					}
				}
			}
		}
	}

	public TreeInfo GetVariation(ref Randomizer r)
	{
		if (this.m_variations != null && this.m_variations.Length != 0)
		{
			int num = r.Int32(100u);
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				num -= this.m_variations[i].m_probability;
				if (num < 0)
				{
					TreeInfo finalTree = this.m_variations[i].m_finalTree;
					if (finalTree != null)
					{
						return finalTree.GetVariation(ref r);
					}
				}
			}
		}
		return this;
	}

	public void CalculateGeneratedInfo()
	{
		MeshFilter[] componentsInChildren = base.GetComponentsInChildren<MeshFilter>(true);
		this.CalculateGeneratedInfo(componentsInChildren);
	}

	public void CalculateGeneratedInfo(MeshFilter[] filters)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		float num5 = 0f;
		for (int i = 0; i < filters.Length; i++)
		{
			Vector3[] vertices = filters[i].get_sharedMesh().get_vertices();
			for (int j = 0; j < vertices.Length; j++)
			{
				num = Mathf.Min(num, vertices[j].x);
				num2 = Mathf.Max(num2, vertices[j].x);
				num3 = Mathf.Max(num3, vertices[j].y);
				num4 = Mathf.Min(num4, vertices[j].z);
				num5 = Mathf.Max(num5, vertices[j].z);
			}
		}
		this.m_generatedInfo.m_center = new Vector3(0f, num3 * 0.5f, 0f);
		this.m_generatedInfo.m_size = new Vector3(Mathf.Max(-num, num2) * 2f, num3, Mathf.Max(-num4, num5) * 2f);
		this.m_generatedInfo.m_triangleArea = 0f;
		this.m_generatedInfo.m_uvmapArea = 0f;
		if (num3 != 0f)
		{
			for (int k = 0; k < filters.Length; k++)
			{
				Vector3[] vertices2 = filters[k].get_sharedMesh().get_vertices();
				Vector2[] uv = filters[k].get_sharedMesh().get_uv();
				Color32[] array = filters[k].get_sharedMesh().get_colors32();
				int[] triangles = filters[k].get_sharedMesh().get_triangles();
				if (array.Length != vertices2.Length)
				{
					array = new Color32[vertices2.Length];
				}
				for (int l = 0; l < vertices2.Length; l++)
				{
					array[l].a = (byte)Mathf.Clamp(Mathf.RoundToInt(vertices2[l].y * 255f / num3), 0, 255);
				}
				for (int m = 0; m < triangles.Length; m += 3)
				{
					int num6 = triangles[m];
					int num7 = triangles[m + 1];
					int num8 = triangles[m + 2];
					this.m_generatedInfo.m_triangleArea += Triangle3.Area(vertices2[num6], vertices2[num7], vertices2[num8]);
					this.m_generatedInfo.m_uvmapArea += Triangle2.Area(uv[num6], uv[num7], uv[num8]);
				}
				filters[k].get_sharedMesh().set_colors32(array);
			}
		}
	}

	public override void RenderMesh(RenderManager.CameraInfo cameraInfo)
	{
		Vector3 position;
		position..ctor(0f, 60f, 0f);
		TreeInstance.RenderInstance(cameraInfo, this, position, 1f, 1f);
	}

	public override string GetLocalizedTitle()
	{
		return Locale.Get("TREE_TITLE", base.get_gameObject().get_name());
	}

	public override GeneratedString GetGeneratedTitle()
	{
		return new GeneratedString.Locale("TREE_TITLE", base.get_gameObject().get_name());
	}

	public override string GetUncheckedLocalizedTitle()
	{
		if (Locale.Exists("TREE_TITLE", base.get_gameObject().get_name()))
		{
			return Locale.Get("TREE_TITLE", base.get_gameObject().get_name());
		}
		return StringExtensions.SplitUppercase(base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescription()
	{
		return Locale.Get("TREE_DESC", base.get_gameObject().get_name());
	}

	public override string GetLocalizedDescriptionShort()
	{
		return Locale.Get("TREE_DESC", base.get_gameObject().get_name());
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		Vector3 position;
		position..ctor(0f, 60f, 0f);
		TreeInstance.TerrainUpdated(this, position);
	}

	public void SetRenderParameters(int index, int tiles)
	{
		float num = Mathf.Max(Mathf.Max(this.m_generatedInfo.m_size.x, this.m_generatedInfo.m_size.y), this.m_generatedInfo.m_size.z);
		float num2 = Mathf.Max(this.m_generatedInfo.m_size.x, this.m_generatedInfo.m_size.z) / this.m_renderScale;
		float num3 = this.m_generatedInfo.m_size.y / this.m_renderScale;
		float num4 = ((float)(index % tiles) + 0.5f - num2 * 0.5f) / (float)tiles;
		float num5 = num4 + num2 / (float)tiles;
		float num6 = (float)(index / tiles) / (float)tiles;
		float num7 = num6 + 1f / (float)tiles;
		this.m_renderUv0 = new Vector2(num4, num6);
		this.m_renderUv1 = new Vector2(num5, num6);
		this.m_renderUv2 = new Vector2(num4, num7);
		this.m_renderUv3 = new Vector2(num5, num7);
		num4 = -0.5f * num * num2;
		num5 = 0.5f * num * num2;
		num6 = -0.5f * num * num3;
		num7 = 0.5f * num * num3;
		this.m_renderUv0B = new Vector2(num4, num6);
		this.m_renderUv1B = new Vector2(num5, num6);
		this.m_renderUv2B = new Vector2(num4, num7);
		this.m_renderUv3B = new Vector2(num5, num7);
		this.GenerateLodMesh();
	}

	private void GenerateLodMesh()
	{
		RenderGroup.VertexArrays vertexArrays = (RenderGroup.VertexArrays)0;
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		TreeInstance.CalculateGroupData(ref num, ref num2, ref num3, ref vertexArrays);
		RenderGroup.MeshData meshData = new RenderGroup.MeshData(vertexArrays, num, num2);
		RenderGroup.MeshData meshData2 = new RenderGroup.MeshData(vertexArrays, num * 4, num2 * 4);
		RenderGroup.MeshData meshData3 = new RenderGroup.MeshData(vertexArrays, num * 8, num2 * 8);
		RenderGroup.MeshData meshData4 = new RenderGroup.MeshData(vertexArrays, num * 16, num2 * 16);
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		float num4 = 0f;
		float num5 = 0f;
		int num6 = 0;
		int num7 = 0;
		for (int i = 0; i < 1; i++)
		{
			Vector3 position;
			position..ctor(0f, 0f, (float)i);
			TreeInstance.PopulateGroupData(this, position, 1f, 1f, ref num6, ref num7, Vector3.get_zero(), meshData, ref zero, ref zero2, ref num4, ref num5);
		}
		num6 = 0;
		num7 = 0;
		for (int j = 0; j < 4; j++)
		{
			Vector3 position2;
			position2..ctor(0f, 0f, (float)j);
			TreeInstance.PopulateGroupData(this, position2, 1f, 1f, ref num6, ref num7, Vector3.get_zero(), meshData2, ref zero, ref zero2, ref num4, ref num5);
		}
		num6 = 0;
		num7 = 0;
		for (int k = 0; k < 8; k++)
		{
			Vector3 position3;
			position3..ctor(0f, 0f, (float)k);
			TreeInstance.PopulateGroupData(this, position3, 1f, 1f, ref num6, ref num7, Vector3.get_zero(), meshData3, ref zero, ref zero2, ref num4, ref num5);
		}
		num6 = 0;
		num7 = 0;
		for (int l = 0; l < 16; l++)
		{
			Vector3 position4;
			position4..ctor(0f, 0f, (float)l);
			TreeInstance.PopulateGroupData(this, position4, 1f, 1f, ref num6, ref num7, Vector3.get_zero(), meshData4, ref zero, ref zero2, ref num4, ref num5);
		}
		this.m_lodMeshData1 = meshData;
		this.m_lodMeshData4 = meshData2;
		this.m_lodMeshData8 = meshData3;
		this.m_lodMeshData16 = meshData4;
	}

	public override ItemClass.Service GetService()
	{
		return (this.m_class == null) ? base.GetService() : this.m_class.m_service;
	}

	public override ItemClass.SubService GetSubService()
	{
		return (this.m_class == null) ? base.GetSubService() : this.m_class.m_subService;
	}

	public override ItemClass.Level GetClassLevel()
	{
		return (this.m_class == null) ? base.GetClassLevel() : this.m_class.m_level;
	}

	public override int GetConstructionCost()
	{
		int result = 1000;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_class.m_service, this.m_class.m_subService, this.m_class.m_level);
		return result;
	}

	public override int GetMaintenanceCost()
	{
		return 0;
	}

	public override string GetLocalizedTooltip()
	{
		string localizedTooltip = base.GetLocalizedTooltip();
		return TooltipHelper.Append(localizedTooltip, TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Cost,
			LocaleFormatter.FormatCost(this.GetConstructionCost(), false)
		}));
	}

	public TreeInfoGen m_generatedInfo;

	[CustomizableProperty("Class", "Tree general")]
	public ItemClass m_class;

	public ItemClass.Placement m_placementStyle;

	[CustomizableProperty("Availability", "Tree general")]
	public ItemClass.Availability m_availableIn = ItemClass.Availability.All;

	[CustomizableProperty("Min scale", "Variation")]
	public float m_minScale = 0.75f;

	[CustomizableProperty("Max scale", "Variation")]
	public float m_maxScale = 1.25f;

	[CustomizableProperty("Min brightness", "Variation")]
	public float m_minBrightness = 0.5f;

	[CustomizableProperty("Max brightness", "Variation")]
	public float m_maxBrightness = 1f;

	[CustomizableProperty("Create ruining", "Variation")]
	public bool m_createRuining = true;

	public TreeInfo.Variation[] m_variations;

	[NonSerialized]
	public Mesh m_mesh;

	[NonSerialized]
	public Material m_material;

	[NonSerialized]
	public Color m_defaultColor;

	[NonSerialized]
	public Material m_renderMaterial;

	[NonSerialized]
	public float m_renderScale;

	[NonSerialized]
	public float m_renderOffset;

	[NonSerialized]
	public float m_renderClip;

	[NonSerialized]
	public Vector2 m_renderUv0;

	[NonSerialized]
	public Vector2 m_renderUv1;

	[NonSerialized]
	public Vector2 m_renderUv2;

	[NonSerialized]
	public Vector2 m_renderUv3;

	[NonSerialized]
	public Vector2 m_renderUv0B;

	[NonSerialized]
	public Vector2 m_renderUv1B;

	[NonSerialized]
	public Vector2 m_renderUv2B;

	[NonSerialized]
	public Vector2 m_renderUv3B;

	[NonSerialized]
	public float m_lodRenderDistance;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData1;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData4;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData8;

	[NonSerialized]
	public RenderGroup.MeshData m_lodMeshData16;

	[NonSerialized]
	public Mesh m_lodMesh1;

	[NonSerialized]
	public Mesh m_lodMesh4;

	[NonSerialized]
	public Mesh m_lodMesh8;

	[NonSerialized]
	public Mesh m_lodMesh16;

	[NonSerialized]
	public Material m_lodMaterial;

	[NonSerialized]
	public Vector4[] m_lodLocations;

	[NonSerialized]
	public Vector4[] m_lodColors;

	[NonSerialized]
	public int m_lodCount;

	[NonSerialized]
	public Vector3 m_lodMin;

	[NonSerialized]
	public Vector3 m_lodMax;

	[Serializable]
	public class Variation
	{
		public TreeInfo m_tree;

		public int m_probability = 100;

		[NonSerialized]
		public TreeInfo m_finalTree;
	}
}
