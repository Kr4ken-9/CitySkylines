﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class TrafficInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_avgTrafficFlow = base.Find<UISlider>("TrafficFlowMeter");
		this.m_avgTrafficFlowLabel = base.Find<UILabel>("TrafficFlow");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[14].m_targetColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[14].m_negativeColor);
				}
			}
			this.m_initialized = true;
		}
		if (Singleton<VehicleManager>.get_exists())
		{
			this.m_avgTrafficFlow.set_value(Singleton<VehicleManager>.get_instance().m_lastTrafficFlow);
			this.m_avgTrafficFlowLabel.set_text(LocaleFormatter.FormatGeneric("INFO_TRAFFIC_AVGTRAFFICFLOW", new object[]
			{
				Mathf.RoundToInt(this.m_avgTrafficFlow.get_value())
			}));
		}
	}

	private bool m_initialized;

	private UISlider m_avgTrafficFlow;

	private UILabel m_avgTrafficFlowLabel;
}
