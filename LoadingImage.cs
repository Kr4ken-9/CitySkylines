﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class LoadingImage : MonoBehaviour
{
	private void Awake()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			LoadingManager instance = Singleton<LoadingManager>.get_instance();
			LoadingAnimation loadingAnimationComponent = instance.LoadingAnimationComponent;
			loadingAnimationComponent.SetImage(this.m_mesh, this.m_material, this.m_scale, this.m_showAnimation);
			if (this.m_loadingTips != null && this.m_loadingTips.Length != 0)
			{
				uint num = 0u;
				for (int i = 0; i < this.m_loadingTips.Length; i++)
				{
					Expansion expansion = this.m_loadingTips[i].m_expansion;
					if (expansion == -1 || instance.m_supportsExpansion[expansion])
					{
						num += Locale.Count(this.m_loadingTips[i].m_titleID);
					}
				}
				Randomizer randomizer;
				randomizer..ctor((int)DateTime.Now.Ticks);
				uint num2 = randomizer.UInt32(num);
				string title = null;
				string text = null;
				for (int j = 0; j < this.m_loadingTips.Length; j++)
				{
					Expansion expansion2 = this.m_loadingTips[j].m_expansion;
					if (expansion2 == -1 || instance.m_supportsExpansion[expansion2])
					{
						num = Locale.Count(this.m_loadingTips[j].m_titleID);
						if (num > num2)
						{
							title = Locale.Get(this.m_loadingTips[j].m_titleID, (int)num2);
							text = Locale.Get(this.m_loadingTips[j].m_textID, (int)num2);
							break;
						}
						num2 -= num;
					}
				}
				loadingAnimationComponent.SetText(this.m_font, this.m_textColor, this.m_textSize, title, text);
			}
		}
		Object.Destroy(base.get_gameObject());
	}

	public Mesh m_mesh;

	public Material m_material;

	public float m_scale = 1f;

	public bool m_showAnimation = true;

	public UIFont m_font;

	public Color m_textColor;

	public float m_textSize = 1.5f;

	public LoadingImage.LoadingTip[] m_loadingTips;

	[Serializable]
	public struct LoadingTip
	{
		public string m_titleID;

		public string m_textID;

		public Expansion m_expansion;
	}
}
