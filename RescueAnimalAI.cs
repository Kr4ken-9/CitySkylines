﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RescueAnimalAI : AnimalAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_detectionEffect != null)
		{
			this.m_detectionEffect.InitializeEffect();
		}
	}

	public override void ReleaseAI()
	{
		if (this.m_detectionEffect != null)
		{
			this.m_detectionEffect.ReleaseEffect();
		}
		base.ReleaseAI();
	}

	public override Color GetColor(ushort instanceID, ref CitizenInstance data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Destruction)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(instanceID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort instanceID, ref CitizenInstance data, out InstanceID target)
	{
		if ((data.m_flags & CitizenInstance.Flags.Blown) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("ANIMAL_STATUS_FLYING");
		}
		if ((data.m_flags & CitizenInstance.Flags.Floating) != CitizenInstance.Flags.None)
		{
			target = InstanceID.Empty;
			return Locale.Get("CITIZEN_STATUS_CONFUSED");
		}
		target = InstanceID.Empty;
		return Locale.Get("RESCUE_ANIMAL_SEARCHING");
	}

	public override void CreateInstance(ushort instanceID, ref CitizenInstance data)
	{
		base.CreateInstance(instanceID, ref data);
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance data, Vector3 physicsLodRefPos)
	{
		base.SimulationStep(instanceID, ref data, physicsLodRefPos);
		if (data.m_citizen != 0u)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizen(data.m_citizen);
		}
		else if (data.m_targetBuilding == 0 || (data.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			Singleton<CitizenManager>.get_instance().ReleaseCitizenInstance(instanceID);
		}
	}

	public override void SetSource(ushort instanceID, ref CitizenInstance data, ushort sourceBuilding)
	{
		if (sourceBuilding != 0)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			data.Unspawn(instanceID);
			Randomizer randomizer;
			randomizer..ctor((int)instanceID);
			CitizenInstance.Frame lastFrameData = instance.m_instances.m_buffer[(int)sourceBuilding].GetLastFrameData();
			Quaternion rotation = lastFrameData.m_rotation;
			Vector3 vector = rotation * new Vector3(0.5f, 0f, 0f);
			if (randomizer.Int32(2u) == 0)
			{
				vector = -vector;
			}
			Vector3 position = lastFrameData.m_position + vector;
			Vector4 targetPos = instance.m_instances.m_buffer[(int)sourceBuilding].m_targetPos + vector;
			data.m_frame0.m_velocity = Vector3.get_zero();
			data.m_frame0.m_position = position;
			data.m_frame0.m_rotation = rotation;
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos = targetPos;
			data.Spawn(instanceID);
		}
	}

	public override void SetTarget(ushort instanceID, ref CitizenInstance data, ushort targetBuilding)
	{
		data.m_targetBuilding = targetBuilding;
	}

	public override InstanceID GetTargetID(ushort instanceID, ref CitizenInstance citizenData)
	{
		InstanceID result = default(InstanceID);
		if (citizenData.m_targetBuilding != 0)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			result.Building = instance.m_instances.m_buffer[(int)citizenData.m_targetBuilding].m_targetBuilding;
		}
		return result;
	}

	public override void SimulationStep(ushort instanceID, ref CitizenInstance citizenData, ref CitizenInstance.Frame frameData, bool lodPhysics)
	{
		float sqrMagnitude = frameData.m_velocity.get_sqrMagnitude();
		if (sqrMagnitude > 0.01f)
		{
			frameData.m_position += frameData.m_velocity * 0.5f;
		}
		CitizenInstance.Flags flags = CitizenInstance.Flags.None;
		if (citizenData.m_targetBuilding != 0)
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			CitizenInstance.Flags flags2 = instance.m_instances.m_buffer[(int)citizenData.m_targetBuilding].m_flags;
			if ((flags2 & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
			{
				Randomizer randomizer;
				randomizer..ctor((int)instanceID);
				CitizenInstance.Frame lastFrameData = instance.m_instances.m_buffer[(int)citizenData.m_targetBuilding].GetLastFrameData();
				Vector3 vector = lastFrameData.m_rotation * new Vector3(0.5f, 0f, 0f);
				if (randomizer.Int32(2u) == 0)
				{
					vector = -vector;
				}
				Vector4 targetPos = instance.m_instances.m_buffer[(int)citizenData.m_targetBuilding].m_targetPos + vector;
				if (Vector3.SqrMagnitude(lastFrameData.m_position - frameData.m_position) > 10000f)
				{
					citizenData.m_targetBuilding = 0;
				}
				else
				{
					flags = (flags2 & (CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition));
					citizenData.m_targetPos = targetPos;
				}
			}
			else
			{
				citizenData.m_targetBuilding = 0;
			}
		}
		citizenData.m_flags = ((citizenData.m_flags & ~(CitizenInstance.Flags.Underground | CitizenInstance.Flags.InsideBuilding | CitizenInstance.Flags.Transition)) | flags);
		Vector3 vector2 = citizenData.m_targetPos - frameData.m_position;
		float num;
		if (!lodPhysics && citizenData.m_targetPos.w > 0.001f)
		{
			num = VectorUtils.LengthSqrXZ(vector2);
		}
		else
		{
			num = vector2.get_sqrMagnitude();
		}
		float num2 = this.m_info.m_walkSpeed;
		float num3 = 2f;
		if (num < 1f)
		{
			vector2 = Vector3.get_zero();
			if ((citizenData.m_flags & CitizenInstance.Flags.AtTarget) == CitizenInstance.Flags.None)
			{
				citizenData.m_flags |= CitizenInstance.Flags.AtTarget;
				if (this.m_detectionEffect != null)
				{
					InstanceID instance2 = default(InstanceID);
					instance2.CitizenInstance = instanceID;
					EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(frameData.m_position, Vector3.get_up(), 0f);
					float num4 = 3.75f;
					Singleton<EffectManager>.get_instance().DispatchEffect(this.m_detectionEffect, instance2, spawnArea, frameData.m_velocity * num4, 0f, 1f, Singleton<CitizenManager>.get_instance().m_audioGroup);
				}
			}
		}
		else
		{
			citizenData.m_flags &= ~CitizenInstance.Flags.AtTarget;
			float num5 = Mathf.Sqrt(num);
			num2 = Mathf.Min(num2, num5 * 0.75f);
			Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
			vector2 = quaternion * vector2;
			if (vector2.z < Mathf.Abs(vector2.x))
			{
				if (vector2.x >= 0f)
				{
					vector2.x = Mathf.Max(1f, vector2.x);
				}
				else
				{
					vector2.x = Mathf.Min(-1f, vector2.x);
				}
				vector2.z = Mathf.Abs(vector2.x);
				num2 = Mathf.Min(0.5f, num5 * 0.1f);
			}
			vector2 = Vector3.ClampMagnitude(frameData.m_rotation * vector2, num2);
		}
		Vector3 vector3 = vector2 - frameData.m_velocity;
		float magnitude = vector3.get_magnitude();
		vector3 *= num3 / Mathf.Max(magnitude, num3);
		frameData.m_velocity += vector3;
		float sqrMagnitude2 = frameData.m_velocity.get_sqrMagnitude();
		bool flag = !lodPhysics && citizenData.m_targetPos.w > 0.001f && (sqrMagnitude2 > 0.01f || sqrMagnitude > 0.01f);
		ushort num6;
		if (flag)
		{
			Vector3 worldPos = frameData.m_position + frameData.m_velocity * 0.5f;
			num6 = Singleton<BuildingManager>.get_instance().GetWalkingBuilding(worldPos);
		}
		else
		{
			num6 = 0;
		}
		if (sqrMagnitude2 > 0.01f)
		{
			Vector3 vector4 = frameData.m_velocity;
			if (!lodPhysics)
			{
				Vector3 vector5 = Vector3.get_zero();
				float num7 = 0f;
				base.CheckCollisions(instanceID, ref citizenData, frameData.m_position, frameData.m_position + frameData.m_velocity, num6, ref vector5, ref num7);
				if (num7 > 0.01f)
				{
					vector5 *= 1f / num7;
					vector5 = Vector3.ClampMagnitude(vector5, Mathf.Sqrt(sqrMagnitude2) * 0.5f);
					frameData.m_velocity += vector5;
					vector4 += vector5 * 0.25f;
				}
			}
			frameData.m_position += frameData.m_velocity * 0.5f;
			if (vector4.get_sqrMagnitude() > 0.01f)
			{
				frameData.m_rotation = Quaternion.LookRotation(vector4);
			}
		}
		if (flag)
		{
			Vector3 position = frameData.m_position;
			float num8 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(position);
			if (num6 != 0)
			{
				num8 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num6].SampleWalkingHeight(num6, position, num8);
				position.y += (num8 - position.y) * Mathf.Min(1f, citizenData.m_targetPos.w * 4f);
				frameData.m_position.y = position.y;
			}
			else if (Mathf.Abs(num8 - position.y) < 2f)
			{
				position.y += (num8 - position.y) * Mathf.Min(1f, citizenData.m_targetPos.w * 4f);
				frameData.m_position.y = position.y;
			}
		}
		frameData.m_underground = ((citizenData.m_flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None);
		frameData.m_insideBuilding = ((citizenData.m_flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None);
		frameData.m_transition = ((citizenData.m_flags & CitizenInstance.Flags.Transition) != CitizenInstance.Flags.None);
	}

	public EffectInfo m_detectionEffect;
}
