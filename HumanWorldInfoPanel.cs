﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public abstract class HumanWorldInfoPanel : LivingCreatureWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Status = base.Find<UILabel>("Status");
		this.m_Target = base.Find<UIButton>("Target");
		if (this.m_Target != null)
		{
			this.m_Target.add_eventClick(new MouseEventHandler(base.OnTargetClick));
		}
		this.m_Occupation = base.Find<UILabel>("Occupation");
		this.m_Workplace = base.Find<UIButton>("Workplace");
		if (this.m_Workplace != null)
		{
			this.m_Workplace.add_eventClick(new MouseEventHandler(base.OnTargetClick));
		}
		this.m_Happiness = base.Find<UISprite>("Happiness");
	}

	protected static string GetAgeAndWealth(Citizen.Wealth wealth, Citizen.AgeGroup age)
	{
		return Locale.Get("TOURIST_AGEWEALTH", wealth + age);
	}

	protected static string GetAgeAndEducation(Citizen.Education education, Citizen.AgeGroup age)
	{
		return Locale.Get("CITIZEN_AGEEDUCATION", education + age);
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (this.m_InstanceID.Type == InstanceType.Citizen && this.m_InstanceID.Citizen != 0u)
			{
				uint citizen = this.m_InstanceID.Citizen;
				CitizenInfo citizenInfo = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
				int happiness = Citizen.GetHappiness((int)instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_health, (int)instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_wellbeing);
				ItemClass.Level currentSchoolLevel = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCurrentSchoolLevel(citizen);
				ushort workBuilding = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_workBuilding;
				string text = string.Empty;
				if ((instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.Tourist) != Citizen.Flags.None)
				{
					text = Locale.Get("CITIZEN_OCCUPATION_TOURIST");
				}
				else if (currentSchoolLevel != ItemClass.Level.None)
				{
					text = Locale.Get("CITIZEN_SCHOOL_LEVEL", currentSchoolLevel.ToString());
				}
				else
				{
					text = ((workBuilding == 0) ? Locale.Get("CITIZEN_OCCUPATION_UNEMPLOYED") : Locale.Get("CITIZEN_OCCUPATION_POSITION"));
				}
				this.m_Happiness.set_spriteName(ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel(happiness)));
				this.m_Occupation.set_text(text);
				if (workBuilding != 0 && Singleton<BuildingManager>.get_exists())
				{
					InstanceID instanceID = default(InstanceID);
					instanceID.Building = workBuilding;
					this.m_Workplace.set_objectUserData(instanceID);
					this.m_Workplace.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(workBuilding, this.m_InstanceID));
					this.m_Workplace.set_isVisible(true);
				}
				else
				{
					this.m_Workplace.set_text(string.Empty);
					this.m_Workplace.set_isVisible(false);
				}
				base.ShortenTextToFitParent(this.m_Workplace);
				InstanceID instanceID2;
				this.m_Status.set_text(citizenInfo.m_citizenAI.GetLocalizedStatus(citizen, ref instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)], out instanceID2));
				if (instanceID2.IsEmpty)
				{
					this.m_Target.set_isVisible(false);
				}
				else
				{
					this.m_Target.set_objectUserData(instanceID2);
					this.m_Target.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(instanceID2.Building, this.m_InstanceID));
					base.ShortenTextToFitParent(this.m_Target);
					this.m_Target.set_isVisible(true);
				}
			}
		}
	}

	protected UISprite m_Happiness;

	protected UILabel m_Occupation;

	protected UIButton m_Workplace;

	protected UILabel m_Status;

	protected UIButton m_Target;
}
