﻿using System;
using ColossalFramework;
using UnityEngine;

public class IntersectionAI : BuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		MilestoneInfo milestoneInfo = this.m_info.m_UnlockMilestone;
		int num = -10000000;
		if (milestoneInfo != null)
		{
			num = milestoneInfo.GetComparisonValue();
		}
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
				{
					MilestoneInfo unlockMilestone = pathInfo.m_netInfo.m_UnlockMilestone;
					if (unlockMilestone != null)
					{
						int comparisonValue = unlockMilestone.GetComparisonValue();
						if (comparisonValue > num)
						{
							milestoneInfo = unlockMilestone;
							num = comparisonValue;
						}
					}
				}
			}
		}
		this.m_cachedUnlockMilestone = milestoneInfo;
	}

	private void EnsureHasTunnels()
	{
		if (this.m_cachedHasTunnels < 0)
		{
			this.m_cachedHasTunnels = 0;
			if (this.m_info.m_paths != null)
			{
				for (int i = 0; i < this.m_info.m_paths.Length; i++)
				{
					BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
					if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0 && pathInfo.m_netInfo.m_netAI.IsUnderground())
					{
						this.m_cachedHasTunnels = 1;
					}
				}
			}
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		this.EnsureHasTunnels();
		if (elevation < -8f || this.m_cachedHasTunnels == 1)
		{
			mode = InfoManager.InfoMode.Underground;
			subMode = InfoManager.SubInfoMode.Default;
		}
		else
		{
			mode = InfoManager.InfoMode.None;
			subMode = InfoManager.SubInfoMode.Default;
		}
	}

	private void EnsureElevation()
	{
		if (this.m_cachedMaxElevation < 0)
		{
			this.m_cachedMinElevation = -1000;
			this.m_cachedMaxElevation = 1000;
			if (this.m_info.m_paths != null)
			{
				for (int i = 0; i < this.m_info.m_paths.Length; i++)
				{
					BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
					if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
					{
						int num;
						int num2;
						pathInfo.m_netInfo.m_netAI.GetElevationLimits(out num, out num2);
						int num3 = 1000;
						int num4 = -1000;
						for (int j = 0; j < pathInfo.m_nodes.Length; j++)
						{
							Vector3 vector = pathInfo.m_nodes[j];
							int num5 = Mathf.RoundToInt(vector.y / 12f);
							if (num5 < num3)
							{
								num3 = num5;
							}
							if (num5 > num4)
							{
								num4 = num5;
							}
						}
						if (num3 < 0)
						{
							num = Mathf.Min(0, num - num3);
						}
						if (num4 > 0)
						{
							num2 = Mathf.Max(0, num2 - num4);
						}
						if (num > this.m_cachedMinElevation)
						{
							this.m_cachedMinElevation = num;
						}
						if (num2 < this.m_cachedMaxElevation)
						{
							this.m_cachedMaxElevation = num2;
						}
					}
				}
			}
			if (this.m_cachedMinElevation == -1000)
			{
				this.m_cachedMinElevation = 0;
			}
			if (this.m_cachedMaxElevation == 1000)
			{
				this.m_cachedMaxElevation = 0;
			}
		}
	}

	protected override void GetConstructionCost(bool relocating, out int baseCost, out bool includePaths)
	{
		baseCost = 0;
		includePaths = true;
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		this.EnsureElevation();
		min = this.m_cachedMinElevation;
		max = this.m_cachedMaxElevation;
	}

	public override bool CheckUnlocking()
	{
		if (Singleton<UnlockManager>.get_instance().Unlocked(this.m_info.m_class.m_service))
		{
			MilestoneInfo cachedUnlockMilestone = this.m_cachedUnlockMilestone;
			if (cachedUnlockMilestone != null)
			{
				Singleton<UnlockManager>.get_instance().CheckMilestone(cachedUnlockMilestone, false, false);
			}
			if (Singleton<UnlockManager>.get_instance().Unlocked(cachedUnlockMilestone))
			{
				return true;
			}
		}
		return false;
	}

	private void EnsureCosts()
	{
		if (this.m_cachedConstructionCost < 0)
		{
			this.m_cachedConstructionCost = 0;
			this.m_cachedMaintenanceCost = 0;
			if (this.m_info.m_paths != null)
			{
				for (int i = 0; i < this.m_info.m_paths.Length; i++)
				{
					BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
					if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
					{
						for (int j = 1; j < pathInfo.m_nodes.Length; j++)
						{
							Vector3 startPos = pathInfo.m_nodes[j - 1];
							Vector3 endPos = pathInfo.m_nodes[j];
							this.m_cachedConstructionCost += pathInfo.m_netInfo.m_netAI.GetConstructionCost(startPos, endPos, startPos.y, endPos.y);
							this.m_cachedMaintenanceCost += pathInfo.m_netInfo.m_netAI.GetMaintenanceCost(startPos, endPos);
						}
					}
				}
			}
		}
	}

	public override int GetConstructionCost()
	{
		this.EnsureCosts();
		return this.m_cachedConstructionCost;
	}

	public override int GetMaintenanceCost()
	{
		this.EnsureCosts();
		return this.m_cachedMaintenanceCost;
	}

	public void GetActualSize(out int width, out int length)
	{
		width = 0;
		length = 0;
		if (this.m_info.m_paths != null)
		{
			for (int i = 0; i < this.m_info.m_paths.Length; i++)
			{
				BuildingInfo.PathInfo pathInfo = this.m_info.m_paths[i];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
				{
					for (int j = 0; j < pathInfo.m_nodes.Length; j++)
					{
						Vector3 vector = pathInfo.m_nodes[j];
						width = Math.Max(width, Mathf.CeilToInt(Mathf.Abs(vector.x) / 8f));
						length = Math.Max(length, Mathf.CeilToInt(Mathf.Abs(vector.z) / 8f));
					}
				}
			}
		}
		if (this.m_info.m_props != null)
		{
			for (int k = 0; k < this.m_info.m_props.Length; k++)
			{
				PropInfo prop = this.m_info.m_props[k].m_prop;
				TreeInfo tree = this.m_info.m_props[k].m_tree;
				Vector3 position = this.m_info.m_props[k].m_position;
				Vector3 vector2;
				Vector3 vector3;
				if (prop != null)
				{
					vector2 = position + prop.m_generatedInfo.m_center;
					vector3 = 0.5f * prop.m_generatedInfo.m_size;
				}
				else if (tree != null)
				{
					vector2 = position + tree.m_generatedInfo.m_center;
					vector3 = 0.5f * tree.m_generatedInfo.m_size;
				}
				else
				{
					vector2..ctor(0f, 0f, 0f);
					vector3..ctor(0f, 0f, 0f);
				}
				width = Math.Max(width, Mathf.CeilToInt(Mathf.Max(Mathf.Abs(vector2.x - vector3.x) / 8f, Mathf.Abs(vector2.x + vector3.x) / 8f)));
				length = Math.Max(length, Mathf.CeilToInt(Mathf.Max(Mathf.Abs(vector2.z - vector3.z) / 8f, Mathf.Abs(vector2.z + vector3.z) / 8f)));
			}
		}
		width *= 2;
		length *= 2;
	}

	public override void GetWidthRange(out int minWidth, out int maxWidth)
	{
		minWidth = 0;
		maxWidth = 0;
	}

	public override void GetLengthRange(out int minLength, out int maxLength)
	{
		minLength = 0;
		maxLength = 0;
	}

	public override void GetDecorationArea(out int width, out int length, out float offset)
	{
		width = 64;
		length = 64;
		offset = 0f;
	}

	public override MilestoneInfo GetUnlockMilestone()
	{
		return this.m_cachedUnlockMilestone;
	}

	public override bool WorksAsBuilding()
	{
		return false;
	}

	public override bool WorksAsNet()
	{
		return true;
	}

	public override bool IgnoreBuildingCollision()
	{
		return true;
	}

	[NonSerialized]
	private int m_cachedConstructionCost = -1;

	[NonSerialized]
	private int m_cachedMaintenanceCost = -1;

	[NonSerialized]
	private int m_cachedMinElevation = -1;

	[NonSerialized]
	private int m_cachedMaxElevation = -1;

	[NonSerialized]
	private int m_cachedHasTunnels = -1;

	[NonSerialized]
	private MilestoneInfo m_cachedUnlockMilestone;
}
