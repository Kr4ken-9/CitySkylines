﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct CitizenInstance
{
	public bool RenderInstance(RenderManager.CameraInfo cameraInfo, ushort instanceID)
	{
		if ((this.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			return false;
		}
		CitizenInfo info = this.Info;
		if (info == null)
		{
			return false;
		}
		uint num = (uint)(((int)instanceID << 4) / 65536);
		uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		CitizenInstance.Frame frameData = this.GetFrameData(num2 - 32u);
		float maxDistance = Mathf.Min(RenderManager.LevelOfDetailFactor * 800f, info.m_maxRenderDistance + cameraInfo.m_height * 0.5f);
		if (!cameraInfo.CheckRenderDistance(frameData.m_position, maxDistance))
		{
			return false;
		}
		if (!cameraInfo.Intersect(frameData.m_position, 10f))
		{
			return false;
		}
		CitizenInstance.Frame frameData2 = this.GetFrameData(num2 - 16u);
		float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		bool flag = frameData2.m_underground && frameData.m_underground;
		bool flag2 = frameData2.m_insideBuilding && frameData.m_insideBuilding;
		bool flag3 = frameData2.m_transition || frameData.m_transition;
		if ((flag2 && !flag3) || (flag && !flag3 && (cameraInfo.m_layerMask & 1 << Singleton<CitizenManager>.get_instance().m_undergroundLayer) == 0))
		{
			return false;
		}
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 vector = bezier.Position(num3);
		Quaternion quaternion = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num3);
		Color color = info.m_citizenAI.GetColor(instanceID, ref this, Singleton<InfoManager>.get_instance().CurrentMode);
		if (cameraInfo.CheckRenderDistance(vector, info.m_lodRenderDistance))
		{
			InstanceID empty = InstanceID.Empty;
			empty.CitizenInstance = instanceID;
			CitizenInfo citizenInfo = info.ObtainPrefabInstance<CitizenInfo>(empty, 255);
			if (citizenInfo != null)
			{
				Vector3 velocity = Vector3.Lerp(frameData.m_velocity, frameData2.m_velocity, num3);
				citizenInfo.m_citizenAI.SetRenderParameters(cameraInfo, instanceID, ref this, vector, quaternion, velocity, color, (flag || flag3) && (cameraInfo.m_layerMask & 1 << Singleton<CitizenManager>.get_instance().m_undergroundLayer) != 0);
				return true;
			}
		}
		if (flag || flag3)
		{
			info.m_undergroundLodLocations[info.m_undergroundLodCount].SetTRS(vector, quaternion, Vector3.get_one());
			info.m_undergroundLodColors[info.m_undergroundLodCount] = color.get_linear();
			info.m_undergroundLodMin = Vector3.Min(info.m_undergroundLodMin, vector);
			info.m_undergroundLodMax = Vector3.Max(info.m_undergroundLodMax, vector);
			if (++info.m_undergroundLodCount == info.m_undergroundLodLocations.Length)
			{
				CitizenInstance.RenderUndergroundLod(cameraInfo, info);
			}
		}
		if (!flag || flag3)
		{
			info.m_lodLocations[info.m_lodCount].SetTRS(vector, quaternion, Vector3.get_one());
			info.m_lodColors[info.m_lodCount] = color.get_linear();
			info.m_lodMin = Vector3.Min(info.m_lodMin, vector);
			info.m_lodMax = Vector3.Max(info.m_lodMax, vector);
			if (++info.m_lodCount == info.m_lodLocations.Length)
			{
				CitizenInstance.RenderLod(cameraInfo, info);
			}
		}
		return true;
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, CitizenInfo info, Vector3 position)
	{
		if (info.m_prefabInitialized)
		{
			float maxDistance = Mathf.Min(RenderManager.LevelOfDetailFactor * 800f, info.m_maxRenderDistance + cameraInfo.m_height * 0.5f);
			float num = 20f;
			Vector3 vector = position + info.m_skinRenderer.get_transform().get_position() * num;
			if (info.GetComponent<BirdAI>() != null)
			{
				vector.y += 10f;
			}
			Matrix4x4 matrix4x = default(Matrix4x4);
			if (CitizenInstance.asseteditorDrawMesh == null)
			{
				CitizenInstance.asseteditorDrawMesh = new Mesh();
			}
			if (cameraInfo.CheckRenderDistance(vector, maxDistance))
			{
				matrix4x.SetTRS(vector, info.m_skinRenderer.get_transform().get_rotation(), new Vector3(num, num, num));
				info.m_skinRenderer.BakeMesh(CitizenInstance.asseteditorDrawMesh);
				Graphics.DrawMesh(CitizenInstance.asseteditorDrawMesh, matrix4x, info.m_skinRenderer.get_sharedMaterial(), LayerMask.NameToLayer("Citizens"));
			}
			else
			{
				matrix4x.SetTRS(vector, info.m_lodObject.get_transform().get_rotation(), new Vector3(num, num, num));
				Graphics.DrawMesh(info.m_lodMesh, matrix4x, info.m_lodMaterial, LayerMask.NameToLayer("Citizens"));
			}
		}
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, CitizenInfo info)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_lodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_lodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else if (info.m_lodCount <= 8)
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMeshCombined16;
			num = 16;
		}
		for (int i = info.m_lodCount; i < num; i++)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(cameraInfo.m_forward * -100000f, Quaternion.get_identity(), Vector3.get_one());
			info.m_lodLocations[i] = matrix4x;
			info.m_lodColors[i] = new Color(0f, 0f, 0f, 0f);
		}
		materialBlock.SetMatrixArray(instance.ID_CitizenLocation, info.m_lodLocations);
		materialBlock.SetVectorArray(instance.ID_CitizenColor, info.m_lodColors);
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_lodMin - new Vector3(100f, 100f, 100f), info.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			CitizenManager expr_1B2_cp_0 = instance;
			expr_1B2_cp_0.m_drawCallData.m_lodCalls = expr_1B2_cp_0.m_drawCallData.m_lodCalls + 1;
			CitizenManager expr_1C5_cp_0 = instance;
			expr_1C5_cp_0.m_drawCallData.m_batchedCalls = expr_1C5_cp_0.m_drawCallData.m_batchedCalls + (info.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_lodMaterialCombined, info.m_prefabDataLayer, null, 0, materialBlock);
		}
		info.m_lodCount = 0;
	}

	public static void RenderUndergroundLod(RenderManager.CameraInfo cameraInfo, CitizenInfo info)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		if (info.m_undergroundLodMaterial == null)
		{
			info.m_undergroundLodMaterial = new Material(instance.m_properties.m_undergroundShader);
			info.m_undergroundLodMaterial.CopyPropertiesFromMaterial(info.m_lodMaterialCombined);
			info.m_undergroundLodMaterial.EnableKeyword("MULTI_INSTANCE");
		}
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (info.m_undergroundLodCount <= 1)
		{
			mesh = info.m_lodMeshCombined1;
			num = 1;
		}
		else if (info.m_undergroundLodCount <= 4)
		{
			mesh = info.m_lodMeshCombined4;
			num = 4;
		}
		else if (info.m_undergroundLodCount <= 8)
		{
			mesh = info.m_lodMeshCombined8;
			num = 8;
		}
		else
		{
			mesh = info.m_lodMeshCombined16;
			num = 16;
		}
		for (int i = info.m_undergroundLodCount; i < num; i++)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(cameraInfo.m_forward * -100000f, Quaternion.get_identity(), Vector3.get_one());
			info.m_undergroundLodLocations[i] = matrix4x;
			info.m_undergroundLodColors[i] = new Color(0f, 0f, 0f, 0f);
		}
		materialBlock.SetMatrixArray(instance.ID_CitizenLocation, info.m_undergroundLodLocations);
		materialBlock.SetVectorArray(instance.ID_CitizenColor, info.m_undergroundLodColors);
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(info.m_undergroundLodMin - new Vector3(100f, 100f, 100f), info.m_undergroundLodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			info.m_undergroundLodMin = new Vector3(100000f, 100000f, 100000f);
			info.m_undergroundLodMax = new Vector3(-100000f, -100000f, -100000f);
			CitizenManager expr_1FA_cp_0 = instance;
			expr_1FA_cp_0.m_drawCallData.m_lodCalls = expr_1FA_cp_0.m_drawCallData.m_lodCalls + 1;
			CitizenManager expr_20D_cp_0 = instance;
			expr_20D_cp_0.m_drawCallData.m_batchedCalls = expr_20D_cp_0.m_drawCallData.m_batchedCalls + (info.m_undergroundLodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), info.m_undergroundLodMaterial, Singleton<CitizenManager>.get_instance().m_undergroundLayer, null, 0, materialBlock);
		}
		info.m_undergroundLodCount = 0;
	}

	public void RenderOverlay(RenderManager.CameraInfo cameraInfo, ushort instanceID, Color color)
	{
		CitizenInfo info = this.Info;
		uint num = (uint)(((int)instanceID << 4) / 65536);
		uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		CitizenInstance.Frame frameData = this.GetFrameData(num2 - 32u);
		CitizenInstance.Frame frameData2 = this.GetFrameData(num2 - 16u);
		float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 center = bezier.Position(num3);
		ToolManager expr_CF_cp_0 = Singleton<ToolManager>.get_instance();
		expr_CF_cp_0.m_drawCallData.m_overlayCalls = expr_CF_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color, center, info.m_radius * 2f, center.y - 1f, center.y + info.m_height + 1f, false, true);
	}

	public void CheckOverlayAlpha(ref float alpha)
	{
		CitizenInfo info = this.Info;
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(info.m_radius)));
	}

	public Vector3 GetSmoothPosition(ushort instanceID)
	{
		uint num = (uint)(((int)instanceID << 4) / 65536);
		uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		CitizenInstance.Frame frameData = this.GetFrameData(num2 - 32u);
		CitizenInstance.Frame frameData2 = this.GetFrameData(num2 - 16u);
		float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		return bezier.Position(num3);
	}

	public void GetSmoothPosition(ushort instanceID, out Vector3 position, out Quaternion rotation)
	{
		uint num = (uint)(((int)instanceID << 4) / 65536);
		uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		CitizenInstance.Frame frameData = this.GetFrameData(num2 - 32u);
		CitizenInstance.Frame frameData2 = this.GetFrameData(num2 - 16u);
		float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		position = bezier.Position(num3);
		rotation = Quaternion.Lerp(frameData.m_rotation, frameData2.m_rotation, num3);
	}

	public CitizenInfo Info
	{
		get
		{
			return PrefabCollection<CitizenInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public CitizenInstance.Frame GetLastFrameData()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0;
		case 1:
			return this.m_frame1;
		case 2:
			return this.m_frame2;
		case 3:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public Vector3 GetLastFramePosition()
	{
		switch (this.m_lastFrame)
		{
		case 0:
			return this.m_frame0.m_position;
		case 1:
			return this.m_frame1.m_position;
		case 2:
			return this.m_frame2.m_position;
		case 3:
			return this.m_frame3.m_position;
		default:
			return this.m_frame0.m_position;
		}
	}

	public CitizenInstance.Frame GetFrameData(uint simulationFrame)
	{
		simulationFrame = (simulationFrame >> 4 & 3u);
		switch (simulationFrame)
		{
		case 0u:
			return this.m_frame0;
		case 1u:
			return this.m_frame1;
		case 2u:
			return this.m_frame2;
		case 3u:
			return this.m_frame3;
		default:
			return this.m_frame0;
		}
	}

	public void SetFrameData(uint simulationFrame, CitizenInstance.Frame data)
	{
		this.m_lastFrame = (byte)(simulationFrame >> 4 & 3u);
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public void SetLastFrameData(CitizenInstance.Frame data)
	{
		switch (this.m_lastFrame)
		{
		case 0:
			this.m_frame0 = data;
			return;
		case 1:
			this.m_frame1 = data;
			return;
		case 2:
			this.m_frame2 = data;
			return;
		case 3:
			this.m_frame3 = data;
			return;
		default:
			return;
		}
	}

	public void Spawn(ushort instanceID)
	{
		if ((this.m_flags & CitizenInstance.Flags.Character) == CitizenInstance.Flags.None)
		{
			this.m_flags |= CitizenInstance.Flags.Character;
			Singleton<CitizenManager>.get_instance().AddToGrid(instanceID, ref this);
		}
	}

	public void Unspawn(ushort instanceID)
	{
		if ((this.m_flags & CitizenInstance.Flags.Character) != CitizenInstance.Flags.None)
		{
			Singleton<CitizenManager>.get_instance().RemoveFromGrid(instanceID, ref this);
			this.m_flags &= ~(CitizenInstance.Flags.Character | CitizenInstance.Flags.EnteringVehicle);
		}
	}

	public bool RayCast(ushort instanceID, Segment3 ray, CitizenInstance.Flags ignoreFlags, out float t)
	{
		t = 2f;
		if ((this.m_flags & ignoreFlags) != CitizenInstance.Flags.None)
		{
			return false;
		}
		uint num = (uint)(((int)instanceID << 4) / 65536);
		uint num2 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex - num;
		CitizenInstance.Frame frameData = this.GetFrameData(num2 - 32u);
		CitizenInstance.Frame frameData2 = this.GetFrameData(num2 - 16u);
		float num3 = ((num2 & 15u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.0625f;
		Bezier3 bezier = default(Bezier3);
		bezier.a = frameData.m_position;
		bezier.b = frameData.m_position + frameData.m_velocity * 0.333f;
		bezier.c = frameData2.m_position - frameData2.m_velocity * 0.333f;
		bezier.d = frameData2.m_position;
		Vector3 vector = bezier.Position(num3);
		Segment3 segment;
		segment..ctor(vector, vector);
		segment.b.y = segment.b.y + 2f;
		float num5;
		float num6;
		float num4 = ray.DistanceSqr(segment, ref num5, ref num6);
		if (num4 < 0.25f)
		{
			t = num5;
			return true;
		}
		return false;
	}

	public CitizenInstance.Frame m_frame0;

	public CitizenInstance.Frame m_frame1;

	public CitizenInstance.Frame m_frame2;

	public CitizenInstance.Frame m_frame3;

	public Vector4 m_targetPos;

	public Vector2 m_targetDir;

	public CitizenInstance.Flags m_flags;

	public Color32 m_color;

	public uint m_citizen;

	public uint m_path;

	public ushort m_sourceBuilding;

	public ushort m_targetBuilding;

	public ushort m_nextGridInstance;

	public ushort m_nextSourceInstance;

	public ushort m_nextTargetInstance;

	public ushort m_infoIndex;

	public byte m_lastFrame;

	public byte m_pathPositionIndex;

	public byte m_lastPathOffset;

	public byte m_waitCounter;

	public byte m_targetSeed;

	private static Mesh asseteditorDrawMesh;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Underground = 4,
		CustomName = 8,
		Character = 16,
		BorrowCar = 32,
		HangAround = 64,
		InsideBuilding = 128,
		WaitingPath = 256,
		WaitingTransport = 512,
		TryingSpawnVehicle = 1024,
		EnteringVehicle = 2048,
		BoredOfWaiting = 4096,
		CannotUseTransport = 8192,
		Panicking = 16384,
		OnPath = 32768,
		SittingDown = 65536,
		AtTarget = 131072,
		RequireSlowStart = 262144,
		Transition = 524288,
		RidingBicycle = 1048576,
		OnBikeLane = 2097152,
		WaitingTaxi = 4194304,
		CannotUseTaxi = 8388608,
		CustomColor = 16777216,
		Blown = 33554432,
		Floating = 67108864,
		Cheering = 536870912,
		TargetFlags = 536952896,
		All = -1
	}

	public struct Frame
	{
		public Vector3 m_velocity;

		public Vector3 m_position;

		public Quaternion m_rotation;

		public bool m_underground;

		public bool m_insideBuilding;

		public bool m_transition;
	}
}
