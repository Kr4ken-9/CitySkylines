﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class AtmospherePropertiesPanel : HeightmapPanel
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.UnBindActions();
		this.m_Longitude.set_value(DayNightProperties.instance.m_Longitude);
		this.m_Latitude.set_value(DayNightProperties.instance.m_Latitude);
		this.m_SunSize.set_value(DayNightProperties.instance.m_SunSize);
		this.m_SunAnisotropy.set_value(DayNightProperties.instance.m_SunAnisotropyFactor);
		this.m_MoonSize.set_value(DayNightProperties.instance.m_MoonSize);
		this.m_MoonTexture.set_texture(DayNightProperties.instance.m_MoonTexture);
		this.m_InnerCoronaColor.set_selectedColor(DayNightProperties.instance.m_MoonInnerCorona);
		this.m_OuterCoronaColor.set_selectedColor(DayNightProperties.instance.m_MoonOuterCorona);
		this.m_InnerCoronaIntensity.set_value(DayNightProperties.instance.m_MoonInnerCorona.a);
		this.m_OuterCoronaIntensity.set_value(DayNightProperties.instance.m_MoonOuterCorona.a);
		this.m_Rayleight.set_value(DayNightProperties.instance.m_RayleighScattering);
		this.m_Mie.set_value(DayNightProperties.instance.m_MieScattering);
		this.m_Exposure.set_value(DayNightProperties.instance.m_Exposure);
		this.m_StarsIntensity.set_value(DayNightProperties.instance.m_StarsIntensity);
		this.m_OuterSpaceIntensity.set_value(DayNightProperties.instance.m_OuterSpaceIntensity);
		this.m_SkyTint.set_selectedColor(DayNightProperties.instance.m_SkyTint);
		this.m_NightHorizonColor.set_selectedColor(DayNightProperties.instance.m_NightHorizonColor);
		this.m_EarlyNightZenithColor.set_selectedColor(DayNightProperties.instance.m_NightZenithColor.get_colorKeys()[2].color);
		this.m_LateNightZenithColor.set_selectedColor(DayNightProperties.instance.m_NightZenithColor.get_colorKeys()[3].color);
		this.m_Defaults.Store<float>("longitude", DayNightProperties.instance.m_Longitude);
		this.m_Defaults.Store<float>("latitude", DayNightProperties.instance.m_Latitude);
		this.m_Defaults.Store<float>("sunSize", DayNightProperties.instance.m_SunSize);
		this.m_Defaults.Store<float>("sunAnisotropy", DayNightProperties.instance.m_SunAnisotropyFactor);
		this.m_Defaults.Store<float>("moonSize", DayNightProperties.instance.m_MoonSize);
		this.m_Defaults.Store<Texture>("moonTex", DayNightProperties.instance.m_MoonTexture);
		this.m_Defaults.Store<Color>("innerCoronaColor", DayNightProperties.instance.m_MoonInnerCorona);
		this.m_Defaults.Store<Color>("outerCoronaColor", DayNightProperties.instance.m_MoonOuterCorona);
		this.m_Defaults.Store<float>("innerCoronaIntensity", DayNightProperties.instance.m_MoonInnerCorona.a);
		this.m_Defaults.Store<float>("outerCoronaIntensity", DayNightProperties.instance.m_MoonOuterCorona.a);
		this.m_Defaults.Store<float>("rayleigh", DayNightProperties.instance.m_RayleighScattering);
		this.m_Defaults.Store<float>("mie", DayNightProperties.instance.m_MieScattering);
		this.m_Defaults.Store<float>("exposure", DayNightProperties.instance.m_Exposure);
		this.m_Defaults.Store<float>("starsIntensity", DayNightProperties.instance.m_StarsIntensity);
		this.m_Defaults.Store<float>("spaceIntensity", DayNightProperties.instance.m_OuterSpaceIntensity);
		this.m_Defaults.Store<Color>("skyTint", DayNightProperties.instance.m_SkyTint);
		this.m_Defaults.Store<Color>("nightHorizonColor", DayNightProperties.instance.m_NightHorizonColor);
		this.m_Defaults.Store<Color>("earlyNightColor", DayNightProperties.instance.m_NightZenithColor.get_colorKeys()[2].color);
		this.m_Defaults.Store<Color>("lateNightColor", DayNightProperties.instance.m_NightZenithColor.get_colorKeys()[3].color);
		this.BindActions();
	}

	public void ResetAll()
	{
		this.ResetSun(null, null);
		this.ResetMoon(null, null);
		this.ResetSky(null, null);
	}

	public void ResetSun(UIComponent c, UIMouseEventParameter p)
	{
		this.m_Longitude.set_value(this.m_Defaults.Get<float>("longitude"));
		this.m_Latitude.set_value(this.m_Defaults.Get<float>("latitude"));
		this.m_SunSize.set_value(this.m_Defaults.Get<float>("sunSize"));
		this.m_SunAnisotropy.set_value(this.m_Defaults.Get<float>("sunAnisotropy"));
	}

	public void ResetMoon(UIComponent c, UIMouseEventParameter p)
	{
		this.m_MoonSize.set_value(this.m_Defaults.Get<float>("moonSize"));
		this.m_MoonTexture.set_texture(this.m_Defaults.Get<Texture>("moonTex"));
		this.m_InnerCoronaColor.set_selectedColor(this.m_Defaults.Get<Color>("innerCoronaColor"));
		this.m_OuterCoronaColor.set_selectedColor(this.m_Defaults.Get<Color>("outerCoronaColor"));
		this.m_InnerCoronaIntensity.set_value(this.m_Defaults.Get<float>("innerCoronaIntensity"));
		this.m_OuterCoronaIntensity.set_value(this.m_Defaults.Get<float>("outerCoronaIntensity"));
	}

	public void ResetSky(UIComponent c, UIMouseEventParameter p)
	{
		this.m_Rayleight.set_value(this.m_Defaults.Get<float>("rayleigh"));
		this.m_Mie.set_value(this.m_Defaults.Get<float>("mie"));
		this.m_Exposure.set_value(this.m_Defaults.Get<float>("exposure"));
		this.m_StarsIntensity.set_value(this.m_Defaults.Get<float>("starsIntensity"));
		this.m_OuterSpaceIntensity.set_value(this.m_Defaults.Get<float>("spaceIntensity"));
		this.m_SkyTint.set_selectedColor(this.m_Defaults.Get<Color>("skyTint"));
		this.m_NightHorizonColor.set_selectedColor(this.m_Defaults.Get<Color>("nightHorizonColor"));
		this.m_EarlyNightZenithColor.set_selectedColor(this.m_Defaults.Get<Color>("earlyNightColor"));
		this.m_LateNightZenithColor.set_selectedColor(this.m_Defaults.Get<Color>("lateNightColor"));
	}

	private void OnAssignLongitude(UIComponent c, float v)
	{
		DayNightProperties.instance.m_Longitude = v;
	}

	private void OnAssignLatitude(UIComponent c, float v)
	{
		DayNightProperties.instance.m_Latitude = v;
	}

	private void OnAssignRayleight(UIComponent c, float v)
	{
		DayNightProperties.instance.m_RayleighScattering = v;
	}

	private void OnAssignMie(UIComponent c, float v)
	{
		DayNightProperties.instance.m_MieScattering = v;
	}

	private void OnAssignExposure(UIComponent c, float v)
	{
		DayNightProperties.instance.m_Exposure = v;
	}

	private void OnAssignSunSize(UIComponent c, float v)
	{
		DayNightProperties.instance.m_SunSize = v;
	}

	private void OnAssignMoonSize(UIComponent c, float v)
	{
		DayNightProperties.instance.m_MoonSize = v;
	}

	private void OnAssignSunAnisotropy(UIComponent c, float v)
	{
		DayNightProperties.instance.m_SunAnisotropyFactor = v;
	}

	private void OnAssignStarsIntensity(UIComponent c, float v)
	{
		DayNightProperties.instance.m_StarsIntensity = v;
	}

	private void OnAssignOuterSpaceIntensity(UIComponent c, float v)
	{
		DayNightProperties.instance.m_OuterSpaceIntensity = v;
	}

	private void OnAssignMoonTexture(UIComponent c, Texture t)
	{
		DayNightProperties.instance.m_MoonTexture = (Texture2D)t;
		DayNightProperties.instance.m_MoonTexture.set_wrapMode(1);
	}

	private void OnInnerCoronaChanged(UIComponent c, Color t)
	{
		DayNightProperties.instance.m_MoonInnerCorona = new Color(t.r, t.g, t.b, this.m_InnerCoronaIntensity.get_value());
	}

	private void OnInnerCoronaIntensityChanged(UIComponent c, float v)
	{
		DayNightProperties.instance.m_MoonInnerCorona = new Color(this.m_InnerCoronaColor.get_selectedColor().r, this.m_InnerCoronaColor.get_selectedColor().g, this.m_InnerCoronaColor.get_selectedColor().b, v);
	}

	private void OnOuterCoronaChanged(UIComponent c, Color t)
	{
		DayNightProperties.instance.m_MoonOuterCorona = new Color(t.r, t.g, t.b, this.m_OuterCoronaIntensity.get_value());
	}

	private void OnOuterCoronaIntensityChanged(UIComponent c, float v)
	{
		DayNightProperties.instance.m_MoonOuterCorona = new Color(this.m_OuterCoronaColor.get_selectedColor().r, this.m_OuterCoronaColor.get_selectedColor().g, this.m_OuterCoronaColor.get_selectedColor().b, v);
	}

	private void OnHorizonColorChanged(UIComponent c, Color t)
	{
		DayNightProperties.instance.m_NightHorizonColor = t;
	}

	private void OnSkyTintChanged(UIComponent c, Color t)
	{
		DayNightProperties.instance.m_SkyTint = t;
	}

	private void OnZenithEarlyNightColorChanged(UIComponent c, Color t)
	{
		GradientColorKey[] colorKeys = DayNightProperties.instance.m_NightZenithColor.get_colorKeys();
		GradientAlphaKey[] alphaKeys = DayNightProperties.instance.m_NightZenithColor.get_alphaKeys();
		colorKeys[1].color = t;
		colorKeys[2].color = t;
		DayNightProperties.instance.m_NightZenithColor.SetKeys(colorKeys, alphaKeys);
	}

	private void OnZenithLateNightColorChanged(UIComponent c, Color t)
	{
		GradientColorKey[] colorKeys = DayNightProperties.instance.m_NightZenithColor.get_colorKeys();
		GradientAlphaKey[] alphaKeys = DayNightProperties.instance.m_NightZenithColor.get_alphaKeys();
		colorKeys[0].color = t;
		colorKeys[3].color = t;
		DayNightProperties.instance.m_NightZenithColor.SetKeys(colorKeys, alphaKeys);
	}

	private void UnBindActions()
	{
		UIComponent uIComponent = base.Find("Sun");
		this.m_ResetSunButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetSunButton.remove_eventClick(new MouseEventHandler(this.ResetSun));
		this.m_Longitude = uIComponent.Find("Longitude").Find<UISlider>("Slider");
		this.m_Longitude.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignLongitude));
		this.m_Latitude = uIComponent.Find("Latitude").Find<UISlider>("Slider");
		this.m_Latitude.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignLatitude));
		this.m_SunSize = uIComponent.Find("SunSize").Find<UISlider>("Slider");
		this.m_SunSize.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignSunSize));
		this.m_SunAnisotropy = uIComponent.Find("SunAnisotropy").Find<UISlider>("Slider");
		this.m_SunAnisotropy.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignSunAnisotropy));
		UIComponent uIComponent2 = base.Find("Moon");
		this.m_ResetMoonButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetMoonButton.remove_eventClick(new MouseEventHandler(this.ResetMoon));
		this.m_MoonSize = uIComponent2.Find("MoonSize").Find<UISlider>("Slider");
		this.m_MoonSize.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignMoonSize));
		this.m_MoonTexture = uIComponent2.Find("MoonTexture").Find<UITextureSprite>("Texture");
		this.m_MoonTexture.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_MoonTexture.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_MoonTexture.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_MoonTexture.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignMoonTexture));
		this.m_InnerCoronaColor = uIComponent2.Find("InnerCorona").Find<UIColorField>("Value");
		this.m_InnerCoronaColor.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnInnerCoronaChanged));
		this.m_InnerCoronaIntensity = uIComponent2.Find("InnerCorona").Find<UISlider>("Slider");
		this.m_InnerCoronaIntensity.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnInnerCoronaIntensityChanged));
		this.m_OuterCoronaColor = uIComponent2.Find("OuterCorona").Find<UIColorField>("Value");
		this.m_OuterCoronaColor.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnOuterCoronaChanged));
		this.m_OuterCoronaIntensity = uIComponent2.Find("OuterCorona").Find<UISlider>("Slider");
		this.m_OuterCoronaIntensity.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnOuterCoronaIntensityChanged));
		UIComponent uIComponent3 = base.Find("Sky");
		this.m_ResetSkyButton = uIComponent3.Find<UIButton>("Reset");
		this.m_ResetSkyButton.remove_eventClick(new MouseEventHandler(this.ResetSky));
		this.m_Rayleight = uIComponent3.Find("Rayleight").Find<UISlider>("Slider");
		this.m_Rayleight.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRayleight));
		this.m_Mie = uIComponent3.Find("Mie").Find<UISlider>("Slider");
		this.m_Mie.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignMie));
		this.m_Exposure = uIComponent3.Find("Exposure").Find<UISlider>("Slider");
		this.m_Exposure.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignExposure));
		this.m_StarsIntensity = uIComponent3.Find("StarsIntensity").Find<UISlider>("Slider");
		this.m_StarsIntensity.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignStarsIntensity));
		this.m_OuterSpaceIntensity = uIComponent3.Find("OuterSpaceIntensity").Find<UISlider>("Slider");
		this.m_OuterSpaceIntensity.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignOuterSpaceIntensity));
		this.m_SkyTint = uIComponent3.Find("SkyTint").Find<UIColorField>("Value");
		this.m_SkyTint.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnSkyTintChanged));
		this.m_NightHorizonColor = uIComponent3.Find("NightHorizonColor").Find<UIColorField>("Value");
		this.m_NightHorizonColor.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnHorizonColorChanged));
		this.m_EarlyNightZenithColor = uIComponent3.Find("EarlyNightZenithColor").Find<UIColorField>("Value");
		this.m_EarlyNightZenithColor.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnZenithEarlyNightColorChanged));
		this.m_LateNightZenithColor = uIComponent3.Find("LateNightZenithColor").Find<UIColorField>("Value");
		this.m_LateNightZenithColor.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnZenithLateNightColorChanged));
	}

	private void BindActions()
	{
		UIComponent uIComponent = base.Find("Sun");
		this.m_ResetSunButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetSunButton.add_eventClick(new MouseEventHandler(this.ResetSun));
		this.m_Longitude = uIComponent.Find("Longitude").Find<UISlider>("Slider");
		this.m_Longitude.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignLongitude));
		this.m_Latitude = uIComponent.Find("Latitude").Find<UISlider>("Slider");
		this.m_Latitude.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignLatitude));
		this.m_SunSize = uIComponent.Find("SunSize").Find<UISlider>("Slider");
		this.m_SunSize.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignSunSize));
		this.m_SunAnisotropy = uIComponent.Find("SunAnisotropy").Find<UISlider>("Slider");
		this.m_SunAnisotropy.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignSunAnisotropy));
		UIComponent uIComponent2 = base.Find("Moon");
		this.m_ResetMoonButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetMoonButton.add_eventClick(new MouseEventHandler(this.ResetMoon));
		this.m_MoonSize = uIComponent2.Find("MoonSize").Find<UISlider>("Slider");
		this.m_MoonSize.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignMoonSize));
		this.m_MoonTexture = uIComponent2.Find("MoonTexture").Find<UITextureSprite>("Texture");
		this.m_MoonTexture.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_MoonTexture.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_MoonTexture.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_MoonTexture.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignMoonTexture));
		this.m_InnerCoronaColor = uIComponent2.Find("InnerCorona").Find<UIColorField>("Value");
		this.m_InnerCoronaColor.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnInnerCoronaChanged));
		this.m_InnerCoronaIntensity = uIComponent2.Find("InnerCorona").Find<UISlider>("Slider");
		this.m_InnerCoronaIntensity.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnInnerCoronaIntensityChanged));
		this.m_OuterCoronaColor = uIComponent2.Find("OuterCorona").Find<UIColorField>("Value");
		this.m_OuterCoronaColor.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnOuterCoronaChanged));
		this.m_OuterCoronaIntensity = uIComponent2.Find("OuterCorona").Find<UISlider>("Slider");
		this.m_OuterCoronaIntensity.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnOuterCoronaIntensityChanged));
		UIComponent uIComponent3 = base.Find("Sky");
		this.m_ResetSkyButton = uIComponent3.Find<UIButton>("Reset");
		this.m_ResetSkyButton.add_eventClick(new MouseEventHandler(this.ResetSky));
		this.m_Rayleight = uIComponent3.Find("Rayleight").Find<UISlider>("Slider");
		this.m_Rayleight.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRayleight));
		this.m_Mie = uIComponent3.Find("Mie").Find<UISlider>("Slider");
		this.m_Mie.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignMie));
		this.m_Exposure = uIComponent3.Find("Exposure").Find<UISlider>("Slider");
		this.m_Exposure.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignExposure));
		this.m_StarsIntensity = uIComponent3.Find("StarsIntensity").Find<UISlider>("Slider");
		this.m_StarsIntensity.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignStarsIntensity));
		this.m_OuterSpaceIntensity = uIComponent3.Find("OuterSpaceIntensity").Find<UISlider>("Slider");
		this.m_OuterSpaceIntensity.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignOuterSpaceIntensity));
		this.m_SkyTint = uIComponent3.Find("SkyTint").Find<UIColorField>("Value");
		this.m_SkyTint.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnSkyTintChanged));
		this.m_NightHorizonColor = uIComponent3.Find("NightHorizonColor").Find<UIColorField>("Value");
		this.m_NightHorizonColor.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnHorizonColorChanged));
		this.m_EarlyNightZenithColor = uIComponent3.Find("EarlyNightZenithColor").Find<UIColorField>("Value");
		this.m_EarlyNightZenithColor.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnZenithEarlyNightColorChanged));
		this.m_LateNightZenithColor = uIComponent3.Find("LateNightZenithColor").Find<UIColorField>("Value");
		this.m_LateNightZenithColor.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnZenithLateNightColorChanged));
	}

	private void OnHoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_HoverColor);
	}

	private void OnUnhoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_NormalColor);
	}

	private void OnTextureClicked(UIComponent c, UIMouseEventParameter p)
	{
		UITextureSprite s = p.get_source() as UITextureSprite;
		if (s != null)
		{
			TexturePicker texturePicker = UIView.get_library().Get<TexturePicker>("TexturePicker");
			texturePicker.ShowModal(s, TexturePicker.Type.ThemeTextures, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					s.set_texture(UIView.get_library().Get<TexturePicker>("TexturePicker").selectedTexture);
				}
			});
		}
	}

	private void Awake()
	{
	}

	private void Start()
	{
		base.get_component().Hide();
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	public Color32 m_NormalColor;

	public Color32 m_HoverColor;

	private ValueStorage m_Defaults;

	private UISlider m_Longitude;

	private UISlider m_Latitude;

	private UISlider m_SunSize;

	private UISlider m_SunAnisotropy;

	private UISlider m_MoonSize;

	private UITextureSprite m_MoonTexture;

	private UIColorField m_InnerCoronaColor;

	private UIColorField m_OuterCoronaColor;

	private UISlider m_InnerCoronaIntensity;

	private UISlider m_OuterCoronaIntensity;

	private UISlider m_Rayleight;

	private UISlider m_Mie;

	private UISlider m_Exposure;

	private UISlider m_StarsIntensity;

	private UISlider m_OuterSpaceIntensity;

	private UIColorField m_SkyTint;

	private UIColorField m_NightHorizonColor;

	private UIColorField m_EarlyNightZenithColor;

	private UIColorField m_LateNightZenithColor;

	private UIButton m_ResetSunButton;

	private UIButton m_ResetMoonButton;

	private UIButton m_ResetSkyButton;
}
