﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class TaxMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		int num = 0;
		if (this.m_residential)
		{
			num += 2;
		}
		if (this.m_commercial)
		{
			num += 2;
		}
		if (this.m_industrial)
		{
			num++;
		}
		if (this.m_office)
		{
			num++;
		}
		info.m_min = 0f;
		info.m_max = (float)num;
		long num2;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num2 = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num2 = 0L;
		}
		info.m_description = StringUtils.SafeFormat(Locale.Get("MILESTONE_TAXES_MAX_DESC"), this.m_maxTaxes);
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_TAXES_MAX_PROG"), new object[]
		{
			num2,
			num
		});
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = 6L;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			long num = 0L;
			int minTaxes = this.m_minTaxes;
			int maxTaxes = this.m_maxTaxes;
			if (this.m_subtractResidential)
			{
				this.CheckRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ref minTaxes, ref maxTaxes);
				this.CheckRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ref minTaxes, ref maxTaxes);
			}
			if (this.m_subtractCommercial)
			{
				this.CheckRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ref minTaxes, ref maxTaxes);
				this.CheckRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ref minTaxes, ref maxTaxes);
			}
			if (this.m_subtractIndustrial)
			{
				this.CheckRate(ItemClass.Service.Industrial, ItemClass.SubService.None, ref minTaxes, ref maxTaxes);
			}
			if (this.m_subtractOffice)
			{
				this.CheckRate(ItemClass.Service.Office, ItemClass.SubService.None, ref minTaxes, ref maxTaxes);
			}
			int num2 = 0;
			if (this.m_residential)
			{
				if (this.CheckRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				if (this.CheckRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				num2 += 2;
			}
			if (this.m_commercial)
			{
				if (this.CheckRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				if (this.CheckRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				num2 += 2;
			}
			if (this.m_industrial)
			{
				if (this.CheckRate(ItemClass.Service.Industrial, ItemClass.SubService.None, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				num2++;
			}
			if (this.m_office)
			{
				if (this.CheckRate(ItemClass.Service.Office, ItemClass.SubService.None, minTaxes, maxTaxes))
				{
					num += 1L;
				}
				num2++;
			}
			data.m_progress = num;
			data.m_passedCount = ((num != (long)num2) ? 0 : 1);
		}
		return data;
	}

	private void CheckRate(ItemClass.Service service, ItemClass.SubService subService, ref int minTaxes, ref int maxTaxes)
	{
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(service, subService, ItemClass.Level.None);
		if (this.m_minTaxes + taxRate > minTaxes)
		{
			minTaxes = this.m_minTaxes + taxRate;
		}
		if (this.m_maxTaxes + taxRate < maxTaxes)
		{
			maxTaxes = this.m_maxTaxes + taxRate;
		}
	}

	private bool CheckRate(ItemClass.Service service, ItemClass.SubService subService, int minTaxes, int maxTaxes)
	{
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(service, subService, ItemClass.Level.None);
		return taxRate >= minTaxes && taxRate <= maxTaxes;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		int num = 0;
		if (this.m_residential)
		{
			num += 2;
		}
		if (this.m_commercial)
		{
			num += 2;
		}
		if (this.m_industrial)
		{
			num++;
		}
		if (this.m_office)
		{
			num++;
		}
		return Mathf.Clamp01((float)data.m_progress / (float)num);
	}

	public bool m_residential = true;

	public bool m_commercial = true;

	public bool m_industrial = true;

	public bool m_office = true;

	public bool m_subtractResidential;

	public bool m_subtractCommercial;

	public bool m_subtractIndustrial;

	public bool m_subtractOffice;

	public int m_minTaxes;

	public int m_maxTaxes;
}
