﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class LoadingAnimation : MonoBehaviour
{
	public bool AnimationLoaded
	{
		get
		{
			return this.m_animationLoaded;
		}
	}

	public bool AnimationSkipped
	{
		get
		{
			return this.m_skipped;
		}
	}

	public float LoadingProgress
	{
		set
		{
			this.m_targetProgress = value;
		}
	}

	public void SetData(Mesh mesh, Material material, Material barBG, Material barFG)
	{
		if (this.m_animationMaterial != null)
		{
			Object.Destroy(this.m_animationMaterial);
			this.m_animationMaterial = null;
		}
		if (this.m_barBGMaterial != null)
		{
			Object.Destroy(this.m_barBGMaterial);
			this.m_barBGMaterial = null;
		}
		if (this.m_barFGMaterial != null)
		{
			Object.Destroy(this.m_barFGMaterial);
			this.m_barFGMaterial = null;
		}
		this.m_animationMesh = mesh;
		this.m_animationMaterial = new Material(material);
		this.m_barBGMaterial = new Material(barBG);
		this.m_barFGMaterial = new Material(barFG);
		this.m_animationLoaded = true;
		this.m_animationAlpha = 0f;
		this.m_progress = -1f;
		this.m_targetProgress = -1f;
	}

	public void SetImage(Mesh mesh, Material material, float scale, bool showAnimation)
	{
		if (this.m_imageLoaded && this.m_imageAlpha != 0f)
		{
			if (this.m_imageMaterial2 != null)
			{
				Object.Destroy(this.m_imageMaterial2);
				this.m_imageMaterial2 = null;
			}
			this.m_imageMesh2 = mesh;
			this.m_imageMaterial2 = new Material(material);
			this.m_imageLoaded2 = true;
			this.m_imageShowAnimation2 = showAnimation;
			this.m_imageScale2 = scale;
		}
		else
		{
			if (this.m_imageMaterial != null)
			{
				Object.Destroy(this.m_imageMaterial);
				this.m_imageMaterial = null;
			}
			this.m_imageMesh = mesh;
			this.m_imageMaterial = new Material(material);
			this.m_imageLoaded = true;
			this.m_imageShowAnimation = showAnimation;
			this.m_imageScale = scale;
			this.m_imageAlpha = 0f;
		}
	}

	public void SetText(UIFont font, Color color, float size, string title, string text)
	{
		if (this.m_textMaterial != null)
		{
			Object.Destroy(this.m_textMaterial);
			this.m_textMaterial = null;
		}
		this.m_textMaterial = new Material(font.get_material());
		this.m_font = font;
		this.m_textColor = color;
		this.m_textSize = size;
		if (title != null)
		{
			title = title.ToUpper();
		}
		this.m_title = title;
		this.m_text = text;
		this.m_textLoaded = true;
		this.m_textAlpha = 0f;
		this.GenerateTextMesh();
	}

	public void FadeImage()
	{
		this.m_fadeImage = true;
	}

	public void Complete()
	{
		this.m_fadeAway = true;
	}

	private void Awake()
	{
		this.m_camera = base.GetComponent<Camera>();
	}

	private void OnDestroy()
	{
		if (this.m_animationMaterial != null)
		{
			Object.Destroy(this.m_animationMaterial);
			this.m_animationMaterial = null;
		}
		if (this.m_barBGMaterial != null)
		{
			Object.Destroy(this.m_barBGMaterial);
			this.m_barBGMaterial = null;
		}
		if (this.m_barFGMaterial != null)
		{
			Object.Destroy(this.m_barFGMaterial);
			this.m_barFGMaterial = null;
		}
		if (this.m_imageMaterial != null)
		{
			Object.Destroy(this.m_imageMaterial);
			this.m_imageMaterial = null;
		}
		if (this.m_imageMaterial2 != null)
		{
			Object.Destroy(this.m_imageMaterial2);
			this.m_imageMaterial2 = null;
		}
		if (this.m_textMaterial != null)
		{
			Object.Destroy(this.m_textMaterial);
			this.m_textMaterial = null;
		}
		if (this.m_textMesh != null)
		{
			Object.Destroy(this.m_textMesh);
			this.m_textMesh = null;
		}
	}

	private void OnEnable()
	{
		this.m_camera.set_enabled(true);
		this.m_totalAlpha = 1f;
		this.m_fadeAway = false;
		this.m_fadeImage = false;
		this.m_skipped = false;
		this.m_progress = -1f;
		this.m_targetProgress = -1f;
		this.m_camera.set_clearFlags(2);
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Combine(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		Font.add_textureRebuilt(new Action<Font>(this.FontTextureRebuilt));
	}

	private void OnDisable()
	{
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Remove(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		Font.remove_textureRebuilt(new Action<Font>(this.FontTextureRebuilt));
		this.m_camera.set_enabled(false);
		this.m_imageMesh = null;
		this.m_imageLoaded = false;
		this.m_imageAlpha = 0f;
		this.m_imageMesh2 = null;
		this.m_imageLoaded2 = false;
		this.m_font = null;
		this.m_title = null;
		this.m_text = null;
		this.m_textLoaded = false;
		this.m_textAlpha = 0f;
		this.m_progress = -1f;
		this.m_targetProgress = -1f;
		if (this.m_imageMaterial != null)
		{
			Object.Destroy(this.m_imageMaterial);
			this.m_imageMaterial = null;
		}
		if (this.m_imageMaterial2 != null)
		{
			Object.Destroy(this.m_imageMaterial2);
			this.m_imageMaterial2 = null;
		}
		if (this.m_textMaterial != null)
		{
			Object.Destroy(this.m_textMaterial);
			this.m_textMaterial = null;
		}
		if (this.m_textMesh != null)
		{
			Object.Destroy(this.m_textMesh);
			this.m_textMesh = null;
		}
	}

	private void OnGUI()
	{
		EventType type = Event.get_current().get_type();
		if (type != 7 && type != 8)
		{
			if (type != null)
			{
				Event.get_current().Use();
			}
			else
			{
				Event.get_current().Use();
				this.m_skipped = true;
			}
		}
	}

	private void RequestCharacterInfo()
	{
		UIDynamicFont uIDynamicFont = this.m_font as UIDynamicFont;
		if (uIDynamicFont == null)
		{
			return;
		}
		if (!UIFontManager.IsDirty(uIDynamicFont))
		{
			return;
		}
		if (!string.IsNullOrEmpty(this.m_title))
		{
			uIDynamicFont.AddCharacterRequest(this.m_title, 2, 0);
		}
		if (!string.IsNullOrEmpty(this.m_text))
		{
			uIDynamicFont.AddCharacterRequest(this.m_text, 2, 0);
		}
	}

	private void FontTextureRebuilt(Font font)
	{
		if (this.m_font != null && font == this.m_font.get_baseFont())
		{
			this.GenerateTextMesh();
		}
	}

	private void GenerateTextMesh()
	{
		if (this.m_font == null)
		{
			return;
		}
		UIFontManager.Invalidate(this.m_font);
		UIRenderData uIRenderData = UIRenderData.Obtain();
		try
		{
			uIRenderData.Clear();
			PoolList<Vector3> vertices = uIRenderData.get_vertices();
			PoolList<Color32> colors = uIRenderData.get_colors();
			PoolList<Vector2> uvs = uIRenderData.get_uvs();
			PoolList<int> triangles = uIRenderData.get_triangles();
			string text = null;
			if (!string.IsNullOrEmpty(this.m_title) && !string.IsNullOrEmpty(this.m_text))
			{
				text = this.m_title + "\n" + this.m_text;
			}
			else if (!string.IsNullOrEmpty(this.m_title))
			{
				text = this.m_title;
			}
			else if (!string.IsNullOrEmpty(this.m_text))
			{
				text = this.m_text;
			}
			if (!string.IsNullOrEmpty(text))
			{
				using (UIFontRenderer uIFontRenderer = this.m_font.ObtainRenderer())
				{
					uIFontRenderer.set_defaultColor(this.m_textColor);
					uIFontRenderer.set_textScale(this.m_textSize);
					uIFontRenderer.set_pixelRatio(1f);
					uIFontRenderer.set_processMarkup(true);
					uIFontRenderer.set_multiLine(true);
					uIFontRenderer.set_wordWrap(true);
					uIFontRenderer.set_textAlign(1);
					uIFontRenderer.set_maxSize(new Vector2(700f, 500f));
					uIFontRenderer.set_shadow(true);
					uIFontRenderer.set_shadowColor(new Color32(0, 0, 0, 64));
					uIFontRenderer.set_shadowOffset(new Vector2(this.m_textSize * 0.5f, -this.m_textSize));
					uIFontRenderer.set_vectorOffset(new Vector3(-350f, 0f, 0f));
					uIFontRenderer.Render(text, uIRenderData);
				}
			}
			Color32[] array = colors.ToArray();
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i].r == 0)
				{
					array[i].a = 64;
				}
			}
			if (this.m_textMesh == null)
			{
				this.m_textMesh = new Mesh();
			}
			this.m_textMesh.Clear();
			this.m_textMesh.set_vertices(vertices.ToArray());
			this.m_textMesh.set_colors32(array);
			this.m_textMesh.set_uv(uvs.ToArray());
			this.m_textMesh.set_triangles(triangles.ToArray());
		}
		finally
		{
			uIRenderData.Release();
		}
	}

	private void Update()
	{
		if (Input.get_anyKey() || SteamController.GetAnyDigitalActionDown())
		{
			Input.ResetInputAxes();
			this.m_skipped = true;
		}
		float num = Mathf.Min(0.0333333351f, Time.get_deltaTime());
		this.m_timer += num;
		if (this.m_animationLoaded)
		{
			if (!this.m_imageLoaded || this.m_imageShowAnimation || this.m_fadeImage)
			{
				this.m_animationAlpha = Mathf.Min(1f, this.m_animationAlpha + num * 2f);
			}
			else
			{
				this.m_animationAlpha = Mathf.Max(0f, this.m_animationAlpha - num * 2f);
			}
		}
		if (this.m_targetProgress >= 0f)
		{
			if (this.m_progress < 0f)
			{
				this.m_progress = 0f;
			}
			this.m_progress += (Mathf.Clamp01(this.m_targetProgress + 0.05f) - this.m_progress) * num;
		}
		if (this.m_imageLoaded)
		{
			if (this.m_imageLoaded2 || this.m_fadeImage)
			{
				this.m_imageAlpha = Mathf.Max(0f, this.m_imageAlpha - num * 2f);
				if (this.m_imageAlpha == 0f)
				{
					if (this.m_imageMaterial != null)
					{
						Object.Destroy(this.m_imageMaterial);
						this.m_imageMaterial = null;
					}
					this.m_imageMaterial = this.m_imageMaterial2;
					this.m_imageMaterial2 = null;
					this.m_imageMesh = this.m_imageMesh2;
					this.m_imageMesh2 = null;
					this.m_imageLoaded = this.m_imageLoaded2;
					this.m_imageLoaded2 = false;
					this.m_imageShowAnimation = this.m_imageShowAnimation2;
					this.m_imageScale = this.m_imageScale2;
				}
			}
			else
			{
				this.m_imageAlpha = Mathf.Min(1f, this.m_imageAlpha + num * 2f);
			}
		}
		if (this.m_textLoaded)
		{
			this.m_textAlpha = Mathf.Min(1f, this.m_textAlpha + num * 2f);
		}
		if (this.m_fadeAway)
		{
			this.m_camera.set_clearFlags(4);
			this.m_totalAlpha = Mathf.Max(0f, this.m_totalAlpha - num * 4f);
			if (this.m_totalAlpha < 0.001f)
			{
				base.set_enabled(false);
			}
		}
		else
		{
			this.m_camera.set_clearFlags(2);
		}
	}

	private void OnPostRender()
	{
		if (this.m_imageLoaded)
		{
			Matrix4x4 matrix4x = default(Matrix4x4);
			float num = 1f;
			float num2 = 2f * this.m_imageScale;
			Texture2D texture2D = this.m_imageMaterial.get_mainTexture() as Texture2D;
			if (texture2D != null)
			{
				num = (float)texture2D.get_width() / (float)texture2D.get_height();
			}
			Vector3 vector;
			vector..ctor(0f, 0f, 10f);
			Quaternion identity = Quaternion.get_identity();
			Vector3 vector2;
			vector2..ctor(num2 * num, num2, num2);
			matrix4x.SetTRS(vector, identity, vector2);
			this.m_imageMaterial.set_color(new Color(1f, 1f, 1f, this.m_imageAlpha * this.m_totalAlpha));
			if (this.m_imageMaterial.SetPass(0))
			{
				Graphics.DrawMeshNow(this.m_imageMesh, matrix4x);
			}
		}
		if (this.m_animationLoaded)
		{
			Matrix4x4 matrix4x2 = default(Matrix4x4);
			float animationScale = this.m_animationScale;
			Vector3 vector3;
			vector3..ctor(0f, 0f, 10f);
			Quaternion quaternion = Quaternion.AngleAxis(this.m_timer * this.m_rotationSpeed, Vector3.get_back());
			Vector3 vector4;
			vector4..ctor(animationScale, animationScale, animationScale);
			matrix4x2.SetTRS(vector3, quaternion, vector4);
			this.m_animationMaterial.set_color(new Color(1f, 1f, 1f, this.m_animationAlpha * this.m_totalAlpha));
			if (this.m_animationMaterial.SetPass(0))
			{
				Graphics.DrawMeshNow(this.m_animationMesh, matrix4x2);
			}
		}
		if (this.m_animationLoaded && this.m_progress >= 0f)
		{
			Matrix4x4 matrix4x3 = default(Matrix4x4);
			float animationScale2 = this.m_animationScale;
			Vector3 vector5;
			vector5..ctor(0f, this.m_animationScale * -0.8f, 10f);
			Quaternion identity2 = Quaternion.get_identity();
			Vector3 vector6;
			vector6..ctor(animationScale2 * 2f, animationScale2 / 8f, animationScale2);
			matrix4x3.SetTRS(vector5, identity2, vector6);
			this.m_barBGMaterial.set_color(new Color(1f, 1f, 1f, this.m_animationAlpha * this.m_totalAlpha));
			if (this.m_barBGMaterial.SetPass(0))
			{
				Graphics.DrawMeshNow(this.m_animationMesh, matrix4x3);
			}
			vector6.y *= 0.8f;
			vector6.x *= 0.9875f;
			vector5.x -= vector6.x * (1f - this.m_progress) * 0.5f;
			vector6.x *= this.m_progress;
			matrix4x3.SetTRS(vector5, identity2, vector6);
			this.m_barFGMaterial.set_color(new Color(1f, 1f, 1f, this.m_animationAlpha * this.m_totalAlpha));
			if (this.m_barFGMaterial.SetPass(0))
			{
				Graphics.DrawMeshNow(this.m_animationMesh, matrix4x3);
			}
		}
		if (this.m_textLoaded)
		{
			Matrix4x4 matrix4x4 = default(Matrix4x4);
			float num3 = 0.00185185182f;
			Vector3 vector7;
			vector7..ctor(0f, this.m_animationScale * -1.2f, 10f);
			Quaternion identity3 = Quaternion.get_identity();
			Vector3 vector8;
			vector8..ctor(num3, num3, num3);
			matrix4x4.SetTRS(vector7, identity3, vector8);
			this.m_textMaterial.set_color(new Color(1f, 1f, 1f, this.m_textAlpha * this.m_totalAlpha));
			if (this.m_textMaterial.SetPass(0))
			{
				Graphics.DrawMeshNow(this.m_textMesh, matrix4x4);
			}
		}
	}

	public float m_rotationSpeed = 100f;

	public float m_animationScale = 0.25f;

	private Camera m_camera;

	private float m_timer;

	private bool m_animationLoaded;

	private float m_animationAlpha;

	private Mesh m_animationMesh;

	private Material m_animationMaterial;

	private Material m_barBGMaterial;

	private Material m_barFGMaterial;

	private bool m_imageLoaded;

	private bool m_imageShowAnimation;

	private float m_imageScale;

	private float m_imageAlpha;

	private Mesh m_imageMesh;

	private Material m_imageMaterial;

	private bool m_imageLoaded2;

	private bool m_imageShowAnimation2;

	private float m_imageScale2;

	private Mesh m_imageMesh2;

	private Material m_imageMaterial2;

	private bool m_fadeAway;

	private bool m_fadeImage;

	private bool m_skipped;

	private float m_totalAlpha;

	private UIFont m_font;

	private Color m_textColor;

	private float m_textSize;

	private string m_title;

	private string m_text;

	private Mesh m_textMesh;

	private bool m_textLoaded;

	private float m_textAlpha;

	private Material m_textMaterial;

	private float m_progress;

	private float m_targetProgress;
}
