﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class TaxiStandAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.NoisePollution;
		radius1 = this.m_noiseRadius;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Transport)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
			}
			return Singleton<TransportManager>.get_instance().m_properties.m_inactiveColors[(int)this.m_transportInfo.m_transportType];
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public int GetVehicleCount(ushort buildingID, ref Building data)
	{
		int num = 0;
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num2 = data.m_guestVehicles;
		int num3 = 0;
		while (num2 != 0)
		{
			if ((TransferManager.TransferReason)instance.m_vehicles.m_buffer[(int)num2].m_transferType == this.m_transportInfo.m_vehicleReason && (instance.m_vehicles.m_buffer[(int)num2].m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
			{
				num++;
			}
			num2 = instance.m_vehicles.m_buffer[(int)num2].m_nextGuestVehicle;
			if (++num3 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Transport;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else if (this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == this.m_transportInfo.m_vehicleReason)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				Vector3 position;
				Vector3 vector;
				this.CalculateSpawnPosition(buildingID, ref data, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, out position, out vector);
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, position, reason, false, true))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], reason, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, reason, offer);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason vehicleReason = this.m_transportInfo.m_vehicleReason;
		if (vehicleReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(vehicleReason, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		int num = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num, buildingData.m_position, this.m_noiseRadius);
		}
		TransferManager.TransferReason vehicleReason = this.m_transportInfo.m_vehicleReason;
		if (vehicleReason != TransferManager.TransferReason.None)
		{
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			base.CalculateGuestVehicles(buildingID, ref buildingData, vehicleReason, ref num2, ref num3, ref num4, ref num5);
			if (num2 < this.m_maxVehicleCount)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 0;
				offer.Building = buildingID;
				offer.Position = buildingData.m_position;
				offer.Amount = this.m_maxVehicleCount - num2;
				offer.Active = false;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(vehicleReason, offer);
			}
		}
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				position = data.CalculatePosition(this.m_queueEndPos);
				target = data.CalculatePosition(this.m_queueStartPos);
			}
			else
			{
				position = data.CalculatePosition(this.m_queueStartPos);
				target = data.CalculatePosition(this.m_queueEndPos);
			}
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, VehicleInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_vehicleType == this.m_transportInfo.m_vehicleType)
		{
			if (Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				position = data.CalculatePosition(this.m_queueEndPos);
				target = data.CalculatePosition(this.m_queueStartPos);
			}
			else
			{
				position = data.CalculatePosition(this.m_queueStartPos);
				target = data.CalculatePosition(this.m_queueEndPos);
			}
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override TransportInfo GetTransportLineInfo()
	{
		return this.m_transportInfo;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			string.Empty
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int vehicleCount = this.GetVehicleCount(buildingID, ref data);
		return LocaleFormatter.FormatGeneric("AIINFO_TAXISTAND_VEHICLES", new object[]
		{
			vehicleCount,
			this.m_maxVehicleCount
		});
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	public TransportInfo m_transportInfo;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 100f;

	public Vector3 m_queueStartPos;

	public Vector3 m_queueEndPos;

	public int m_maxVehicleCount = 5;
}
