﻿using System;
using ColossalFramework.Math;
using UnityEngine;

public class EffectInfo : MonoBehaviour
{
	public void InitializeEffect()
	{
		if (this.m_refCount++ == 0)
		{
			this.CreateEffect();
		}
	}

	public void ReleaseEffect()
	{
		if (--this.m_refCount == 0)
		{
			this.DestroyEffect();
		}
	}

	public virtual void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
	}

	public virtual void PlayEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup)
	{
	}

	protected virtual void CreateEffect()
	{
	}

	protected virtual void DestroyEffect()
	{
	}

	public virtual bool CalculateGroupData(int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		return false;
	}

	public virtual void PopulateGroupData(int layer, InstanceID id, Vector3 pos, Vector3 dir, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
	}

	public virtual bool RequireRender()
	{
		return false;
	}

	public virtual bool RequirePlay()
	{
		return false;
	}

	public virtual float RenderDistance()
	{
		return 0f;
	}

	public virtual float RenderDuration()
	{
		return 0f;
	}

	public virtual int GroupLayer()
	{
		return -1;
	}

	[NonSerialized]
	private int m_refCount;

	public struct SpawnArea
	{
		public SpawnArea(Matrix4x4 matrix, RenderGroup.MeshData meshData)
		{
			this.m_matrix = matrix;
			this.m_meshData = meshData;
			this.m_positions = null;
			this.m_positions2 = null;
			Vector3 vector = matrix.MultiplyPoint(Vector3.get_zero());
			this.m_bezier = new Bezier3(vector, vector, vector, vector);
			this.m_halfWidth = 0f;
			this.m_halfHeight = 0f;
		}

		public SpawnArea(Matrix4x4 matrix, RenderGroup.MeshData meshData, Vector4[] positions, Vector3[] positions2)
		{
			this.m_matrix = matrix;
			this.m_meshData = meshData;
			this.m_positions = positions;
			this.m_positions2 = positions2;
			Vector3 vector = matrix.MultiplyPoint(Vector3.get_zero());
			this.m_bezier = new Bezier3(vector, vector, vector, vector);
			this.m_halfWidth = 0f;
			this.m_halfHeight = 0f;
		}

		public SpawnArea(Bezier3 bezier, float halfWidth, float halfHeight)
		{
			Vector3 vector = (bezier.a + bezier.b + bezier.c + bezier.d) * 0.25f;
			Vector3 normalized = (bezier.d - bezier.a).get_normalized();
			Quaternion quaternion = Quaternion.LookRotation(normalized);
			this.m_matrix = Matrix4x4.get_identity();
			this.m_matrix.SetTRS(vector, quaternion, Vector3.get_one());
			this.m_meshData = null;
			this.m_positions = null;
			this.m_positions2 = null;
			this.m_bezier = bezier;
			this.m_halfWidth = halfWidth;
			this.m_halfHeight = halfHeight;
		}

		public SpawnArea(Vector3 position, Vector3 direction, float radius)
		{
			Quaternion quaternion;
			if (direction.get_sqrMagnitude() > 0.0001f)
			{
				quaternion = Quaternion.LookRotation(direction);
			}
			else
			{
				quaternion = Quaternion.get_identity();
			}
			this.m_matrix = default(Matrix4x4);
			this.m_matrix.SetTRS(position, quaternion, Vector3.get_one());
			this.m_meshData = null;
			this.m_positions = null;
			this.m_positions2 = null;
			this.m_bezier = new Bezier3(position, position, position, position);
			this.m_halfWidth = radius;
			this.m_halfHeight = 0f;
		}

		public SpawnArea(Vector3 position, Vector3 direction, float radius, float halfHeight)
		{
			Quaternion quaternion;
			if (direction.get_sqrMagnitude() > 0.0001f)
			{
				quaternion = Quaternion.LookRotation(direction);
			}
			else
			{
				quaternion = Quaternion.get_identity();
			}
			this.m_matrix = default(Matrix4x4);
			this.m_matrix.SetTRS(position, quaternion, Vector3.get_one());
			this.m_meshData = null;
			this.m_positions = null;
			this.m_positions2 = null;
			this.m_bezier = new Bezier3(position, position, position, position);
			this.m_halfWidth = radius;
			this.m_halfHeight = halfHeight;
		}

		public Matrix4x4 m_matrix;

		public RenderGroup.MeshData m_meshData;

		public Vector4[] m_positions;

		public Vector3[] m_positions2;

		public Bezier3 m_bezier;

		public float m_halfWidth;

		public float m_halfHeight;
	}
}
