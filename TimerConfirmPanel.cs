﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class TimerConfirmPanel : ConfirmPanel
{
	protected override void Initialize()
	{
		base.Initialize();
		this.ResetCounter();
	}

	private void ResetCounter()
	{
		this.m_CounterStart = Time.get_realtimeSinceStartup();
	}

	private float GetRemainingTime()
	{
		return this.m_TimeOut - (float)Mathf.CeilToInt(Time.get_realtimeSinceStartup() - this.m_CounterStart);
	}

	private string GetFormattedMessage()
	{
		return StringUtils.SafeFormat(this.m_Message, this.GetRemainingTime());
	}

	public override void SetMessage(string title, string message)
	{
		this.m_Title = title;
		this.m_Message = message;
		base.SetMessage(this.m_Title, this.GetFormattedMessage());
	}

	protected override void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		base.OnVisibilityChanged(comp, visible);
		if (visible)
		{
			this.ResetCounter();
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible())
		{
			base.SetMessage(this.m_Title, this.GetFormattedMessage());
			if (this.GetRemainingTime() <= 0f)
			{
				this.OnNo();
			}
		}
	}

	public float m_TimeOut = 20f;

	private float m_CounterStart;

	private string m_Message;

	private string m_Title;
}
