﻿using System;
using ColossalFramework;
using UnityEngine;

public class TsunamiAI : FloodBaseAI
{
	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active | DisasterData.Flags.Clearing)) != DisasterData.Flags.None)
		{
			int num = 1080;
			int num2 = (num + 1) * (num + 1);
			if (TsunamiAI.m_tempHazardLevel == null)
			{
				TsunamiAI.m_tempHazardLevel = new byte[num2];
			}
			else
			{
				for (int i = 0; i < num2; i++)
				{
					TsunamiAI.m_tempHazardLevel[i] = 0;
				}
			}
			if (TsunamiAI.m_tempCellPositions == null)
			{
				TsunamiAI.m_tempCellPositions = new FastList<TsunamiAI.CellPos>[256];
			}
			else
			{
				for (int j = 0; j < 256; j++)
				{
					if (TsunamiAI.m_tempCellPositions[j] != null)
					{
						TsunamiAI.m_tempCellPositions[j].Clear();
					}
				}
			}
			int num3 = Mathf.Clamp(Mathf.FloorToInt(data.m_targetPosition.x / 16f + 540f), 0, num);
			int num4 = Mathf.Clamp(Mathf.FloorToInt(data.m_targetPosition.z / 16f + 540f), 0, num);
			int num5 = (int)((ushort)Mathf.Clamp(Mathf.RoundToInt(data.m_targetPosition.y * 65536f / 1024f), 0, 65535));
			int num6 = (int)((ushort)Mathf.Clamp(Mathf.RoundToInt((data.m_targetPosition.y + this.m_height * (float)data.m_intensity / 55f) * 65536f / 1024f), 0, 65535));
			int num7 = Mathf.Max(1, num6 - num5);
			ushort[] blockHeights = Singleton<TerrainManager>.get_instance().BlockHeights;
			int num8 = num4 * (num + 1) + num3;
			int num9 = num6 - (int)blockHeights[num8];
			int num10 = Mathf.Min(255, num9 * 255 / num7);
			if (num10 > (int)TsunamiAI.m_tempHazardLevel[num8])
			{
				TsunamiAI.m_tempHazardLevel[num8] = (byte)num10;
				if (TsunamiAI.m_tempCellPositions[num10] == null)
				{
					TsunamiAI.m_tempCellPositions[num10] = new FastList<TsunamiAI.CellPos>();
				}
				TsunamiAI.m_tempCellPositions[num10].Add(new TsunamiAI.CellPos
				{
					m_x = (ushort)num3,
					m_z = (ushort)num4
				});
			}
			for (int k = 255; k > 0; k--)
			{
				FastList<TsunamiAI.CellPos> fastList = TsunamiAI.m_tempCellPositions[k];
				if (fastList != null)
				{
					int num11 = (k != 255) ? (k - 1) : 255;
					while (fastList.m_size != 0)
					{
						TsunamiAI.CellPos cellPos = fastList.m_buffer[--fastList.m_size];
						int num12 = (int)cellPos.m_z * (num + 1) + (int)cellPos.m_x;
						if ((int)TsunamiAI.m_tempHazardLevel[num12] == k)
						{
							if (cellPos.m_x > 0)
							{
								int num13 = num12 - 1;
								int num14 = num6 - (int)blockHeights[num13];
								int num15 = Mathf.Min(num11, num14 * 255 / num7);
								if (num15 > (int)TsunamiAI.m_tempHazardLevel[num13])
								{
									TsunamiAI.m_tempHazardLevel[num13] = (byte)num15;
									if (TsunamiAI.m_tempCellPositions[num15] == null)
									{
										TsunamiAI.m_tempCellPositions[num15] = new FastList<TsunamiAI.CellPos>();
									}
									TsunamiAI.m_tempCellPositions[num15].Add(new TsunamiAI.CellPos
									{
										m_x = cellPos.m_x - 1,
										m_z = cellPos.m_z
									});
								}
							}
							if (cellPos.m_z > 0)
							{
								int num16 = num12 - num - 1;
								int num17 = num6 - (int)blockHeights[num16];
								int num18 = Mathf.Min(num11, num17 * 255 / num7);
								if (num18 > (int)TsunamiAI.m_tempHazardLevel[num16])
								{
									TsunamiAI.m_tempHazardLevel[num16] = (byte)num18;
									if (TsunamiAI.m_tempCellPositions[num18] == null)
									{
										TsunamiAI.m_tempCellPositions[num18] = new FastList<TsunamiAI.CellPos>();
									}
									TsunamiAI.m_tempCellPositions[num18].Add(new TsunamiAI.CellPos
									{
										m_x = cellPos.m_x,
										m_z = cellPos.m_z - 1
									});
								}
							}
							if ((int)cellPos.m_x < num)
							{
								int num19 = num12 + 1;
								int num20 = num6 - (int)blockHeights[num19];
								int num21 = Mathf.Min(num11, num20 * 255 / num7);
								if (num21 > (int)TsunamiAI.m_tempHazardLevel[num19])
								{
									TsunamiAI.m_tempHazardLevel[num19] = (byte)num21;
									if (TsunamiAI.m_tempCellPositions[num21] == null)
									{
										TsunamiAI.m_tempCellPositions[num21] = new FastList<TsunamiAI.CellPos>();
									}
									TsunamiAI.m_tempCellPositions[num21].Add(new TsunamiAI.CellPos
									{
										m_x = cellPos.m_x + 1,
										m_z = cellPos.m_z
									});
								}
							}
							if ((int)cellPos.m_z < num)
							{
								int num22 = num12 + num + 1;
								int num23 = num6 - (int)blockHeights[num22];
								int num24 = Mathf.Min(num11, num23 * 255 / num7);
								if (num24 > (int)TsunamiAI.m_tempHazardLevel[num22])
								{
									TsunamiAI.m_tempHazardLevel[num22] = (byte)num24;
									if (TsunamiAI.m_tempCellPositions[num24] == null)
									{
										TsunamiAI.m_tempCellPositions[num24] = new FastList<TsunamiAI.CellPos>();
									}
									TsunamiAI.m_tempCellPositions[num24].Add(new TsunamiAI.CellPos
									{
										m_x = cellPos.m_x,
										m_z = cellPos.m_z + 1
									});
								}
							}
						}
					}
				}
			}
			int num25 = Mathf.Max(Mathf.CeilToInt(232.799988f), 0);
			for (int l = 2; l <= 253; l++)
			{
				int num26 = Mathf.Min(Mathf.FloorToInt(((float)l - 128f + 1f) * 2.4f + 540f), num);
				int num27 = Mathf.Max(Mathf.CeilToInt(232.799988f), 0);
				for (int m = 2; m <= 253; m++)
				{
					int num28 = Mathf.Min(Mathf.FloorToInt(((float)m - 128f + 1f) * 2.4f + 540f), num);
					int num29 = 0;
					int num30 = 0;
					for (int n = num25; n <= num26; n++)
					{
						for (int num31 = num27; num31 <= num28; num31++)
						{
							int num32 = n * (num + 1) + num31;
							num29 += (int)TsunamiAI.m_tempHazardLevel[num32];
							num30++;
						}
					}
					if (num29 != 0)
					{
						num29 /= num30;
						int num33 = l * 256 + m;
						map[num33] = (byte)(255 - (int)(255 - map[num33]) * (255 - num29) / 255);
					}
					num27 = num28 + 1;
				}
				num25 = num26 + 1;
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			if (data.m_waveIndex != 0)
			{
				Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterWave(data.m_waveIndex);
				data.m_waveIndex = 0;
			}
			WaterWave waveData;
			if (this.FindSea(disasterID, ref data, out waveData))
			{
				data.m_flags |= DisasterData.Flags.Significant;
				Singleton<TerrainManager>.get_instance().WaterSimulation.CreateWaterWave(out data.m_waveIndex, waveData);
			}
		}
	}

	protected override void EndDisaster(ushort disasterID, ref DisasterData data)
	{
		base.EndDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
	}

	private bool FindSea(ushort disasterID, ref DisasterData data, out WaterWave waveData)
	{
		waveData = default(WaterWave);
		int num = (int)((ushort)Mathf.Clamp((int)((data.m_targetPosition.x + 8640f) / 16f + 0.5f), 0, 1080));
		int num2 = (int)((ushort)Mathf.Clamp((int)((data.m_targetPosition.z + 8640f) / 16f + 0.5f), 0, 1080));
		int num3 = 1080;
		int num4 = num3 << 3;
		int num5 = 0;
		int num6 = 0;
		int num7 = 1000000000;
		int num8 = 0;
		int num9 = (int)(Singleton<TerrainManager>.get_instance().WaterSimulation.m_currentSeaLevel * 64f);
		ushort[] blockHeights = Singleton<TerrainManager>.get_instance().BlockHeights;
		int num10 = 0;
		int num11 = 1000000000;
		int num12 = 0;
		for (int i = 0; i < num4; i++)
		{
			int num13;
			int num14;
			this.GetSeaSideLocation(i, out num13, out num14);
			int num15 = num9 - (int)blockHeights[num14 * (num3 + 1) + num13];
			if (num15 < 8)
			{
				if (i - num10 >= 10 && (num11 < num7 || (num11 == num7 && i - num10 > num6)))
				{
					num5 = num10;
					num6 = i - num10;
					num7 = num11;
					num8 = num12;
				}
				num10 = i + 1;
				num11 = 1000000000;
			}
			else
			{
				int num16 = num13 - num;
				int num17 = num14 - num2;
				int num18 = num16 * num16 + num17 * num17;
				if (num18 < num11)
				{
					num11 = num18;
					num12 = i;
				}
			}
		}
		if (num4 - num10 >= 10 && (num11 < num7 || (num11 == num7 && num4 - num10 > num6)))
		{
			num5 = num10;
			num6 = num4 - num10;
			num8 = num12;
		}
		if (num6 < 10)
		{
			return false;
		}
		int num19;
		int cellIndex;
		int cellIndex2;
		if (num6 == num4)
		{
			num19 = num8;
			if (num8 < num3)
			{
				num19 += num3 << 2;
			}
			cellIndex = num19 - num3 + 1;
			cellIndex2 = num19 + num3 - 1;
		}
		else
		{
			num19 = Mathf.Clamp(num8, num5 + (num6 >> 2), num5 + num6 - (num6 >> 2));
			cellIndex = Mathf.Max(num19 - num3 + 1, num5);
			cellIndex2 = Mathf.Min(num19 + num3 - 1, num5 + num6 - 1);
		}
		int num20;
		int num21;
		this.GetSeaSideLocation(num19, out num20, out num21);
		int num22;
		int num23;
		this.GetSeaSideLocation(cellIndex, out num22, out num23);
		int num24;
		int num25;
		this.GetSeaSideLocation(cellIndex2, out num24, out num25);
		int num26 = num23 - num25;
		int num27 = num24 - num22;
		int num28 = Mathf.Max(1, FixedMath.Sqrt(num26 * num26 + num27 * num27));
		num26 = Mathf.Clamp((num26 << 15) / num28, -32767, 32767);
		num27 = Mathf.Clamp((num27 << 15) / num28, -32767, 32767);
		waveData.m_minX = (ushort)Mathf.Min(Mathf.Min(num20, num22), num24);
		waveData.m_minZ = (ushort)Mathf.Min(Mathf.Min(num21, num23), num25);
		waveData.m_maxX = (ushort)Mathf.Max(Mathf.Max(num20, num22), num24);
		waveData.m_maxZ = (ushort)Mathf.Max(Mathf.Max(num21, num23), num25);
		if (num20 == 0 || num20 == num3)
		{
			if (num27 > 0)
			{
				num21 = (int)waveData.m_minZ;
			}
			if (num27 < 0)
			{
				num21 = (int)waveData.m_maxZ;
			}
		}
		else if (num21 == 0 || num21 == num3)
		{
			if (num26 > 0)
			{
				num20 = (int)waveData.m_minX;
			}
			if (num26 < 0)
			{
				num20 = (int)waveData.m_maxX;
			}
		}
		waveData.m_dirX = (short)num26;
		waveData.m_dirZ = (short)num27;
		waveData.m_origX = (ushort)num20;
		waveData.m_origZ = (ushort)num21;
		waveData.m_type = 1;
		waveData.m_delta = (short)Mathf.Clamp(Mathf.RoundToInt(this.m_height * 65536f / 1024f * (float)data.m_intensity / 55f), -32767, 32767);
		waveData.m_duration = (ushort)Mathf.Clamp(this.m_duration << 6, 0, 65472);
		waveData.m_currentTime = 0;
		data.m_targetPosition.x = (float)waveData.m_origX * 16f - 8640f;
		data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().WaterSimulation.m_currentSeaLevel;
		data.m_targetPosition.z = (float)waveData.m_origZ * 16f - 8640f;
		data.m_angle = Mathf.Atan2((float)(-(float)num26), (float)num27);
		return true;
	}

	private void GetSeaSideLocation(int cellIndex, out int x, out int z)
	{
		int num = 1080;
		cellIndex %= num << 2;
		if (cellIndex < num)
		{
			x = cellIndex;
			z = 0;
		}
		else
		{
			cellIndex -= num;
			if (cellIndex < num)
			{
				x = num;
				z = cellIndex;
			}
			else
			{
				cellIndex -= num;
				if (cellIndex < num)
				{
					x = num - cellIndex;
					z = num;
				}
				else
				{
					cellIndex -= num;
					x = 0;
					z = num - cellIndex;
				}
			}
		}
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillEmerging(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_startFrame;
		float num2 = num * 0.125f;
		Vector3 vector;
		vector..ctor(-Mathf.Sin(data.m_angle), 0f, Mathf.Cos(data.m_angle));
		float num3 = 4800f;
		Vector3 vector2;
		vector2.x = ((vector.x <= 0f) ? num3 : (-num3));
		vector2.y = data.m_targetPosition.y;
		vector2.z = ((vector.z <= 0f) ? num3 : (-num3));
		float num4 = Vector3.Dot(vector2 - data.m_targetPosition, vector);
		return num4 > num2;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_startFrame;
		float num2 = num * 0.125f - 3000f;
		Vector3 vector;
		vector..ctor(-Mathf.Sin(data.m_angle), 0f, Mathf.Cos(data.m_angle));
		float num3 = 4800f;
		Vector3 vector2;
		vector2.x = ((vector.x <= 0f) ? num3 : (-num3));
		vector2.y = data.m_targetPosition.y;
		vector2.z = ((vector.z <= 0f) ? num3 : (-num3));
		float num4 = Vector3.Dot(vector2 - data.m_targetPosition, vector);
		return num4 > num2;
	}

	protected override bool IsStillClearing(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillClearing(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_startFrame;
		float num2 = num * 0.125f;
		Vector3 vector;
		vector..ctor(-Mathf.Sin(data.m_angle), 0f, Mathf.Cos(data.m_angle));
		float num3 = 4800f;
		Vector3 vector2;
		vector2.x = ((vector.x <= 0f) ? (-num3) : num3);
		vector2.y = data.m_targetPosition.y;
		vector2.z = ((vector.z <= 0f) ? (-num3) : num3);
		float num4 = Vector3.Dot(vector2 - data.m_targetPosition, vector);
		return num4 > num2;
	}

	public override bool CanAffectAt(ushort disasterID, ref DisasterData data, Vector3 position, out float priority)
	{
		if (!base.CanAffectAt(disasterID, ref data, position, out priority))
		{
			return false;
		}
		Vector3 vector;
		vector..ctor(-Mathf.Sin(data.m_angle), 0f, Mathf.Cos(data.m_angle));
		float num = Vector3.Dot(position - data.m_targetPosition, vector);
		uint num2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_startFrame;
		float num3 = num2 * 0.125f - 3000f;
		float num4 = num2 * 0.125f;
		priority = Mathf.Clamp01(Mathf.Min((num4 - num) * 0.01f, (num - num3) * 0.0005f));
		return num >= num3 && num <= num4;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public float m_height = 64f;

	public int m_duration = 256;

	private static byte[] m_tempHazardLevel;

	private static FastList<TsunamiAI.CellPos>[] m_tempCellPositions;

	private struct CellPos
	{
		public ushort m_x;

		public ushort m_z;
	}
}
