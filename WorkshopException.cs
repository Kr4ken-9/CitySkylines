﻿using System;

public class WorkshopException : Exception
{
	public WorkshopException(string message, Exception inner) : base(message, inner)
	{
	}
}
