﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using ColossalFramework.Math;
using UnityEngine;

public class MeshSimplification : Object
{
	public static MeshSimplification.SimplificationMesh SimplifyMesh(MeshSimplification.SimplificationMesh sourceMesh)
	{
		MeshSimplification.SimplificationParameters par = new MeshSimplification.SimplificationParameters();
		return MeshSimplification.SimplifyMesh(sourceMesh, par);
	}

	public static MeshSimplification.SimplificationMesh SimplifyMesh(MeshSimplification.SimplificationMesh sourceMesh, MeshSimplification.SimplificationParameters par)
	{
		return MeshSimplification.SimplifyBuildingInternal(sourceMesh, par);
	}

	public static void SimplificationPostProcess(MeshSimplification.SimplificationMesh generatedMesh, Mesh targetMesh)
	{
		MeshSimplification.SimplificationParameters par = new MeshSimplification.SimplificationParameters();
		MeshSimplification.SimplificationPostProcess(generatedMesh, targetMesh, par);
	}

	public static void SimplificationPostProcess(MeshSimplification.SimplificationMesh generatedMesh, Mesh targetMesh, MeshSimplification.SimplificationParameters par)
	{
		targetMesh.set_vertices(generatedMesh.vertices);
		targetMesh.set_triangles(generatedMesh.triangles);
		targetMesh.RecalculateNormals();
		if (!par.onlyPostProcess && !par.alternateUVs)
		{
			MeshSimplification.CreateCubeProjectedUV(targetMesh);
		}
		MeshSimplification.CalculateTangents(targetMesh);
	}

	private static bool ShootRay(Vector3 from, Vector3 to, List<Triangle3> tris, out float hitt)
	{
		hitt = 1f;
		bool result = false;
		foreach (Triangle3 current in tris)
		{
			float num;
			float num2;
			float num3;
			if (Triangle3.Intersect(current.a, current.b, current.c, from, to, ref num, ref num2, ref num3) && num3 < hitt)
			{
				hitt = num3;
				result = true;
			}
		}
		return result;
	}

	public static Vector2 DropComponent(Vector3 v, int i)
	{
		if (i == 0)
		{
			return new Vector2(v.y, v.z);
		}
		if (i == 1)
		{
			return new Vector2(v.x, v.z);
		}
		return new Vector2(v.x, v.y);
	}

	protected static void VoxelizeModel(MeshSimplification.Grid3D grid, MeshSimplification.SimplificationMesh mesh)
	{
		Vector3[] vertices = mesh.vertices;
		int[] triangles = mesh.triangles;
		int resolution = grid.m_resolution;
		for (int i = 0; i < 3; i++)
		{
			int dim = 1;
			int dim2 = 2;
			if (i == 1)
			{
				dim = 0;
				dim2 = 2;
			}
			else if (i == 2)
			{
				dim = 0;
				dim2 = 1;
			}
			int[,] array = new int[resolution, resolution];
			int[,] array2 = new int[resolution, resolution];
			for (int j = 0; j < resolution; j++)
			{
				for (int k = 0; k < resolution; k++)
				{
					if (i != 1)
					{
						array[j, k] = resolution;
					}
					else
					{
						array[j, k] = -1;
					}
					array2[j, k] = -1;
				}
			}
			for (int l = 0; l < triangles.Length; l += 3)
			{
				Triangle3 triangle;
				triangle..ctor(vertices[triangles[l]], vertices[triangles[l + 1]], vertices[triangles[l + 2]]);
				Triangle2 triangle2;
				triangle2..ctor(MeshSimplification.DropComponent(vertices[triangles[l]], i), MeshSimplification.DropComponent(vertices[triangles[l + 1]], i), MeshSimplification.DropComponent(vertices[triangles[l + 2]], i));
				Vector2 vector = triangle2.Min();
				Vector2 vector2 = triangle2.Max();
				vector.x = grid.ToGrid(vector.x, dim);
				vector.y = grid.ToGrid(vector.y, dim2);
				vector2.x = grid.ToGrid(vector2.x, dim);
				vector2.y = grid.ToGrid(vector2.y, dim2);
				for (float num = Mathf.Round(vector.x) - 0.25f; num <= Mathf.Round(vector2.x) + 0.25f; num += 0.5f)
				{
					for (float num2 = Mathf.Round(vector.y) - 0.25f; num2 <= Mathf.Round(vector2.y) + 0.25f; num2 += 0.5f)
					{
						Vector3 vector3 = default(Vector3);
						Vector3 vector4 = default(Vector3);
						if (i == 0)
						{
							vector3.x = -0.5f;
							vector4.x = (float)resolution - 0.5f;
							vector3.y = num;
							vector3.z = num2;
							vector4.y = num;
							vector4.z = num2;
						}
						else if (i == 1)
						{
							vector3.y = -0.5f;
							vector4.y = (float)resolution - 0.5f;
							vector3.x = num;
							vector3.z = num2;
							vector4.x = num;
							vector4.z = num2;
						}
						else
						{
							vector3.z = -0.5f;
							vector4.z = (float)resolution - 0.5f;
							vector3.x = num;
							vector3.y = num2;
							vector4.x = num;
							vector4.y = num2;
						}
						vector3 = grid.GetPos(vector3);
						vector4 = grid.GetPos(vector4);
						float num3;
						float num4;
						float num5;
						bool flag = triangle.Intersect(new Segment3(vector3, vector4), ref num3, ref num4, ref num5);
						num5 = Mathf.Min(0.99999f, num5);
						if (flag)
						{
							int val = Mathf.FloorToInt((float)resolution * num5);
							if (i != 1)
							{
								array[Mathf.RoundToInt(num), Mathf.RoundToInt(num2)] = Math.Min(array[Mathf.RoundToInt(num), Mathf.RoundToInt(num2)], val);
							}
							array2[Mathf.RoundToInt(num), Mathf.RoundToInt(num2)] = Math.Max(array2[Mathf.RoundToInt(num), Mathf.RoundToInt(num2)], val);
						}
					}
				}
			}
			for (int m = 0; m < resolution; m++)
			{
				for (int n = 0; n < resolution; n++)
				{
					for (int num6 = 0; num6 < resolution; num6++)
					{
						if (num6 < array[m, n] || num6 > array2[m, n])
						{
							int[] loc;
							if (i == 0)
							{
								loc = new int[]
								{
									num6,
									m,
									n
								};
							}
							else if (i == 1)
							{
								loc = new int[]
								{
									m,
									num6,
									n
								};
							}
							else
							{
								loc = new int[]
								{
									m,
									n,
									num6
								};
							}
							grid.SetData(loc, MeshSimplification.CellStatus.OUTSIDE);
						}
					}
				}
			}
		}
	}

	protected static List<MeshSimplification.SimplificationVolume> FitVolumes(MeshSimplification.Grid3D grid, MeshSimplification.SimplificationParameters par)
	{
		List<MeshSimplification.SimplificationVolume> list = new List<MeshSimplification.SimplificationVolume>();
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		bool flag;
		int num5;
		do
		{
			num2++;
			num++;
			MeshSimplification.SimplificationVolume simplificationVolume = null;
			flag = false;
			int num4 = 0;
			num5 = grid.FindNextCandidate().Count<MeshSimplification.SimplificationVolume>();
			int num6 = 0;
			int num7 = 1;
			if (num5 > 0)
			{
				int num8 = par.minCandidates;
				if (num5 < par.minCandidates)
				{
					num8 = num5;
				}
				else
				{
					num8 = Math.Min(num5, num8);
				}
				num7 = num5 / num8;
			}
			foreach (MeshSimplification.SimplificationVolume current in grid.FindNextCandidate())
			{
				if ((num2 + num6++) % num7 == 0)
				{
					MeshSimplification.SimplificationVolume simplificationVolume2;
					int num9 = grid.BestExpansion(current, out simplificationVolume2);
					num3++;
					if (num9 > num4)
					{
						num4 = num9;
						simplificationVolume = simplificationVolume2;
					}
				}
			}
			if (simplificationVolume != null && grid.CountInside(simplificationVolume) >= par.minVolume)
			{
				flag = true;
				grid.Mark(grid.Data, simplificationVolume, MeshSimplification.CellStatus.PROCESSED);
				list.Add(simplificationVolume);
				num = 0;
				grid.ResetCandidates();
			}
		}
		while (flag || (num5 > 500 && num < 20));
		return list;
	}

	protected static MeshSimplification.SimplificationMesh BuildMesh(List<MeshSimplification.SimplificationVolume> volumes, MeshSimplification.SimplificationMesh mesh, MeshSimplification.Grid3D grid, MeshSimplification.SimplificationParameters par)
	{
		MeshSimplification.SimplificationMesh result = default(MeshSimplification.SimplificationMesh);
		Vector3[] vertices = mesh.vertices;
		List<Vector3> list = new List<Vector3>();
		List<Vector3> list2 = new List<Vector3>();
		List<int> list3 = new List<int>();
		List<Vector2> list4 = new List<Vector2>();
		foreach (MeshSimplification.SimplificationVolume current in volumes)
		{
			if (grid.CountInside(current) >= par.minVolume && (par.maxTriangles <= 0 || list3.Count < 3 * par.maxTriangles))
			{
				int num = list.Count<Vector3>();
				List<Vector3> list5;
				List<Vector3> second;
				int[] array;
				Vector2[] second2;
				current.GetMesh(grid.Data, out list5, out second, out array, out second2);
				foreach (Vector3 current2 in list5)
				{
					list.Add(grid.GetPos(current2));
				}
				list2 = list2.Concat(second).ToList<Vector3>();
				list4 = list4.Concat(second2).ToList<Vector2>();
				int[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					int num2 = array2[i];
					list3.Add(num2 + num);
				}
			}
		}
		int num3 = 0;
		Vector3[] array3 = list.ToArray();
		int num4 = array3.Length;
		if (par.relocateCorners)
		{
			num3++;
			List<Vector3> list6 = new List<Vector3>();
			for (int j = 0; j < array3.Length; j++)
			{
				float num5 = 3f * grid.GetMaxCellSize();
				int num6 = -1;
				for (int k = 0; k < vertices.Length; k++)
				{
					float num7 = Vector3.Distance(array3[j], vertices[k]);
					if (num7 < num5)
					{
						num6 = k;
						num5 = num7;
					}
				}
				if (num6 >= 0)
				{
					array3[j] = vertices[num6];
					list6.Add(vertices[num6]);
				}
				else
				{
					list6.Add(array3[j]);
				}
			}
			for (int l = 0; l < list6.Count; l++)
			{
				list6[l] = new Vector3(list6[l].x, Mathf.Max(0f, list6[l].y), list6[l].z);
			}
			result.vertices = list6.ToArray();
			if (num4 != result.vertices.Length)
			{
				Debug.Log("vertices vanished in mesh simplification");
			}
		}
		else
		{
			result.vertices = array3;
		}
		result.uv = list4.ToArray();
		result.normals = list2.ToArray();
		result.triangles = list3.ToArray();
		return result;
	}

	protected static MeshSimplification.SimplificationMesh SimplifyBuildingInternal(MeshSimplification.SimplificationMesh mesh, MeshSimplification.SimplificationParameters par)
	{
		Vector3[] vertices = mesh.vertices;
		Vector3 vector;
		vector..ctor(vertices[0].x, vertices[0].y, vertices[0].z);
		Vector3 vector2;
		vector2..ctor(vertices[0].x, vertices[0].y, vertices[0].z);
		for (int i = 1; i < vertices.Length; i++)
		{
			vector = Vector3.Min(vector, vertices[i]);
			vector2 = Vector3.Max(vector2, vertices[i]);
		}
		MeshSimplification.Grid3D grid = new MeshSimplification.Grid3D(par.resolution, vector, vector2, par.useWedges);
		MeshSimplification.VoxelizeModel(grid, mesh);
		List<MeshSimplification.SimplificationVolume> volumes = MeshSimplification.FitVolumes(grid, par);
		return MeshSimplification.BuildMesh(volumes, mesh, grid, par);
	}

	protected static void CreateUniqueUVs(Mesh mesh, List<MeshSimplification.SimplificationVolume> volumes)
	{
		float num = 0f;
		foreach (MeshSimplification.SimplificationVolume current in volumes)
		{
			float maxDim = current.GetMaxDim();
			num += maxDim * maxDim;
		}
		Vector3[] vertices = mesh.get_vertices();
		Vector2[] uv = mesh.get_uv();
		for (float num2 = 0.95f; num2 >= 0.05f; num2 -= 0.05f)
		{
			bool flag = false;
			int num3 = 0;
			MeshSimplification.QuadTree quadTree = new MeshSimplification.QuadTree(new Vector2(0f, 0f), new Vector2(1f, 1f));
			Vector2[] array = new Vector2[vertices.Length];
			for (int i = 0; i < volumes.Count<MeshSimplification.SimplificationVolume>(); i++)
			{
				MeshSimplification.SimplificationVolume simplificationVolume = volumes[i];
				float maxDim2 = simplificationVolume.GetMaxDim();
				float num4 = maxDim2 * maxDim2 / num;
				MeshSimplification.QuadTree quadTree2 = quadTree.AllocatePos(num2 * Mathf.Sqrt(num4));
				if (quadTree2 == null)
				{
					flag = true;
					break;
				}
				int num5;
				if (simplificationVolume is MeshSimplification.SimplificationWedge)
				{
					num5 = 18;
				}
				else if (simplificationVolume is MeshSimplification.SimplificationCube)
				{
					num5 = 20;
				}
				else
				{
					num5 = 0;
				}
				for (int j = 0; j < num5; j++)
				{
					if (num3 >= vertices.Length)
					{
						Debug.Log("Warning: too many uv indices created");
					}
					Vector2 vector = Vector2.Scale(quadTree2.max - quadTree2.min, uv[num3]);
					vector += quadTree2.min;
					array[num3] = vector;
					num3++;
				}
			}
			if (!flag)
			{
				mesh.set_uv(array);
				return;
			}
		}
		Debug.Log("Warning: Unique UV generation failed.");
	}

	public static void DownscaleImage(Color32[] src, Color32[] dst, int srcW, int srcH, int dstW, int dstH)
	{
		int num = srcW / dstW;
		int num2 = srcH / dstH;
		for (int i = 0; i < dstH; i++)
		{
			for (int j = 0; j < dstW; j++)
			{
				int num3 = 0;
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				for (int k = 0; k < num2; k++)
				{
					for (int l = 0; l < num; l++)
					{
						Color32 color = src[(k + i * num2) * srcW + l + j * num];
						num3 += (int)color.r;
						num4 += (int)color.g;
						num5 += (int)color.b;
						num6 += (int)color.a;
					}
				}
				Color32 color2;
				color2.r = (byte)(num3 / (num * num2));
				color2.g = (byte)(num4 / (num * num2));
				color2.b = (byte)(num5 / (num * num2));
				color2.a = (byte)(num6 / (num * num2));
				dst[i * dstW + j] = color2;
			}
		}
	}

	public static void ForceSmoothing(Mesh mesh)
	{
		Vector3[] vertices = mesh.get_vertices();
		Vector3[] array = mesh.get_normals();
		Vector4[] array2 = mesh.get_tangents();
		if (array.Length != vertices.Length)
		{
			array = new Vector3[vertices.Length];
		}
		if (array2.Length != vertices.Length)
		{
			array2 = new Vector4[vertices.Length];
		}
		MeshSimplification.ForceSmoothing(vertices, array, array2, array, array2);
		mesh.set_normals(array);
		mesh.set_tangents(array2);
	}

	public static void ForceSmoothing(Vector3[] srcVertices, Vector3[] srcNormals, Vector4[] srcTangents, Vector3[] dstNormals, Vector4[] dstTangents)
	{
		int[] array = new int[srcVertices.Length];
		for (int i = 0; i < srcVertices.Length; i++)
		{
			Vector3 vector = srcVertices[i];
			for (int j = 0; j <= i; j++)
			{
				if (Vector3.SqrMagnitude(srcVertices[j] - vector) < 0.0001f)
				{
					if (i == j)
					{
						array[i] = j;
					}
					else
					{
						array[i] = array[j];
					}
					break;
				}
			}
		}
		for (int k = 0; k < srcVertices.Length; k++)
		{
			if (array[k] < k)
			{
				dstNormals[array[k]] += srcNormals[k];
				dstNormals[k] = Vector3.get_zero();
			}
			else
			{
				dstNormals[k] = srcNormals[k];
			}
		}
		for (int l = 0; l < srcVertices.Length; l++)
		{
			dstNormals[l] = dstNormals[array[l]].get_normalized();
			if (srcTangents != null && dstTangents != null)
			{
				Vector3 vector2 = Vector3.Cross(dstNormals[l], new Vector3(srcTangents[l].x, srcTangents[l].y, srcTangents[l].z));
				vector2 = Vector3.Cross(vector2, dstNormals[l]).get_normalized();
				dstTangents[l].x = vector2.x;
				dstTangents[l].y = vector2.y;
				dstTangents[l].z = vector2.z;
			}
		}
	}

	public static Vector4[] CalculateTangents(int[] triangles, Vector3[] vertices, Vector2[] uv, Vector3[] normals)
	{
		int num = triangles.Length;
		int num2 = vertices.Length;
		Vector3[] array = new Vector3[num2];
		Vector3[] array2 = new Vector3[num2];
		Vector4[] array3 = new Vector4[num2];
		for (long num3 = 0L; num3 < (long)num; num3 += 3L)
		{
			long num4 = (long)triangles[(int)(checked((IntPtr)num3))];
			long num5 = (long)triangles[(int)(checked((IntPtr)(unchecked(num3 + 1L))))];
			long num6 = (long)triangles[(int)(checked((IntPtr)(unchecked(num3 + 2L))))];
			Vector3 vector;
			Vector3 vector2;
			Vector3 vector3;
			Vector2 vector4;
			Vector2 vector5;
			Vector2 vector6;
			checked
			{
				vector = vertices[(int)((IntPtr)num4)];
				vector2 = vertices[(int)((IntPtr)num5)];
				vector3 = vertices[(int)((IntPtr)num6)];
				vector4 = uv[(int)((IntPtr)num4)];
				vector5 = uv[(int)((IntPtr)num5)];
				vector6 = uv[(int)((IntPtr)num6)];
			}
			float num7 = vector2.x - vector.x;
			float num8 = vector3.x - vector.x;
			float num9 = vector2.y - vector.y;
			float num10 = vector3.y - vector.y;
			float num11 = vector2.z - vector.z;
			float num12 = vector3.z - vector.z;
			float num13 = vector5.x - vector4.x;
			float num14 = vector6.x - vector4.x;
			float num15 = vector5.y - vector4.y;
			float num16 = vector6.y - vector4.y;
			float num17 = 1f / (num13 * num16 - num14 * num15);
			Vector3 vector7;
			vector7..ctor((num16 * num7 - num15 * num8) * num17, (num16 * num9 - num15 * num10) * num17, (num16 * num11 - num15 * num12) * num17);
			Vector3 vector8;
			vector8..ctor((num13 * num8 - num14 * num7) * num17, (num13 * num10 - num14 * num9) * num17, (num13 * num12 - num14 * num11) * num17);
			checked
			{
				array[(int)((IntPtr)num4)] += vector7;
				array[(int)((IntPtr)num5)] += vector7;
				array[(int)((IntPtr)num6)] += vector7;
				array2[(int)((IntPtr)num4)] += vector8;
				array2[(int)((IntPtr)num5)] += vector8;
				array2[(int)((IntPtr)num6)] += vector8;
			}
		}
		for (long num18 = 0L; num18 < (long)num2; num18 += 1L)
		{
			checked
			{
				Vector3 vector9 = normals[(int)((IntPtr)num18)];
				Vector3 vector10 = array[(int)((IntPtr)num18)];
				Vector3.OrthoNormalize(ref vector9, ref vector10);
				array3[(int)((IntPtr)num18)].x = vector10.x;
				array3[(int)((IntPtr)num18)].y = vector10.y;
				array3[(int)((IntPtr)num18)].z = vector10.z;
				array3[(int)((IntPtr)num18)].w = ((Vector3.Dot(Vector3.Cross(vector9, vector10), array2[(int)((IntPtr)num18)]) >= 0f) ? 1f : -1f);
			}
		}
		return array3;
	}

	public static void CalculateTangents(Mesh mesh)
	{
		int[] triangles = mesh.get_triangles();
		Vector3[] vertices = mesh.get_vertices();
		Vector2[] uv = mesh.get_uv();
		Vector3[] normals = mesh.get_normals();
		mesh.set_tangents(MeshSimplification.CalculateTangents(triangles, vertices, uv, normals));
	}

	public static Color32 SampleColor(Color32[] colors, int width, int height, Vector2 uv)
	{
		return MeshSimplification.SampleColor(colors, width, height, uv, new Color32(255, 255, 255, 0));
	}

	public static Color32 SampleColor(Color32[] colors, int width, int height, Vector2 uv, Color32 def)
	{
		if (colors == null)
		{
			return def;
		}
		int num = (int)(uv.x * (float)width - 0.5f) & width - 1;
		int num2 = (int)(uv.y * (float)height - 0.5f) & height - 1;
		int num3 = num + 1 & width - 1;
		int num4 = num2 + 1 & height - 1;
		Color32 color = colors[num2 * width + num];
		Color32 color2 = colors[num2 * width + num3];
		Color32 color3 = colors[num4 * width + num];
		Color32 color4 = colors[num4 * width + num3];
		int num5 = (int)(uv.x * (float)width * 65535f - 32767.5f) & 65535;
		int num6 = (int)(uv.y * (float)height * 65535f - 32767.5f) & 65535;
		int num7 = (int)color.r + (int)(color2.r - color.r) * num5 / 65535;
		int num8 = (int)color.g + (int)(color2.g - color.g) * num5 / 65535;
		int num9 = (int)color.b + (int)(color2.b - color.b) * num5 / 65535;
		int num10 = (int)color.a + (int)(color2.a - color.a) * num5 / 65535;
		int num11 = (int)color3.r + (int)(color4.r - color3.r) * num5 / 65535;
		int num12 = (int)color3.g + (int)(color4.g - color3.g) * num5 / 65535;
		int num13 = (int)color3.b + (int)(color4.b - color3.b) * num5 / 65535;
		int num14 = (int)color3.a + (int)(color4.a - color3.a) * num5 / 65535;
		Color32 result;
		result.r = (byte)(num7 + (num11 - num7) * num6 / 65535);
		result.g = (byte)(num8 + (num12 - num8) * num6 / 65535);
		result.b = (byte)(num9 + (num13 - num9) * num6 / 65535);
		result.a = (byte)(num10 + (num14 - num10) * num6 / 65535);
		return result;
	}

	public static void BakeTextures(Vector3[] sourceVertices, Vector3[] sourceNormals, Vector4[] sourceTangents, Vector2[] sourceUVS, int[] sourceTriangles, int sourceWidth, int sourceHeight, Color32[] sourceDiffuse, Color32[] sourceXYS, Color32[] sourceACI, Vector3[] targetVertices, Vector3[] targetNormals, Vector4[] targetTangents, Vector2[] targetUVS, int[] targetTriangles, out int targetWidth, out int targetHeight, out Color32[] targetDiffuse, out Color32[] targetXYS, out Color32[] targetACI, bool autoSize, bool emptyOpaque, float sizeBias, float padding, float rayLength, int minSize, int maxSize, int internalSizeBias, int threadCount)
	{
		MeshSimplification.<BakeTextures>c__AnonStorey0 <BakeTextures>c__AnonStorey = new MeshSimplification.<BakeTextures>c__AnonStorey0();
		<BakeTextures>c__AnonStorey.targetVertices = targetVertices;
		<BakeTextures>c__AnonStorey.targetNormals = targetNormals;
		<BakeTextures>c__AnonStorey.targetTangents = targetTangents;
		<BakeTextures>c__AnonStorey.targetUVS = targetUVS;
		<BakeTextures>c__AnonStorey.targetTriangles = targetTriangles;
		<BakeTextures>c__AnonStorey.threadCount = threadCount;
		<BakeTextures>c__AnonStorey.sourceDiffuse = sourceDiffuse;
		<BakeTextures>c__AnonStorey.sourceXYS = sourceXYS;
		<BakeTextures>c__AnonStorey.sourceACI = sourceACI;
		<BakeTextures>c__AnonStorey.sourceVertices = sourceVertices;
		<BakeTextures>c__AnonStorey.sourceNormals = sourceNormals;
		<BakeTextures>c__AnonStorey.sourceTangents = sourceTangents;
		<BakeTextures>c__AnonStorey.sourceUVS = sourceUVS;
		<BakeTextures>c__AnonStorey.sourceTriangles = sourceTriangles;
		<BakeTextures>c__AnonStorey.sourceWidth = sourceWidth;
		<BakeTextures>c__AnonStorey.sourceHeight = sourceHeight;
		<BakeTextures>c__AnonStorey.rayLength = rayLength;
		<BakeTextures>c__AnonStorey.smoothNormals = new Vector3[<BakeTextures>c__AnonStorey.targetNormals.Length];
		MeshSimplification.ForceSmoothing(<BakeTextures>c__AnonStorey.targetVertices, <BakeTextures>c__AnonStorey.targetNormals, null, <BakeTextures>c__AnonStorey.smoothNormals, null);
		<BakeTextures>c__AnonStorey.sourceMin = new Vector3(100000f, 100000f, 100000f);
		<BakeTextures>c__AnonStorey.sourceMax = new Vector3(-100000f, -100000f, -100000f);
		for (int i2 = 0; i2 < <BakeTextures>c__AnonStorey.sourceVertices.Length; i2++)
		{
			Vector3 vector = <BakeTextures>c__AnonStorey.sourceVertices[i2];
			<BakeTextures>c__AnonStorey.sourceMin = Vector3.Min(<BakeTextures>c__AnonStorey.sourceMin, vector);
			<BakeTextures>c__AnonStorey.sourceMax = Vector3.Max(<BakeTextures>c__AnonStorey.sourceMax, vector);
		}
		if (padding != 0f)
		{
			for (int j = 0; j < <BakeTextures>c__AnonStorey.targetUVS.Length; j++)
			{
				Vector2 vector2 = <BakeTextures>c__AnonStorey.targetUVS[j];
				vector2.x = padding + vector2.x * (1f - padding * 2f);
				vector2.y = padding + vector2.y * (1f - padding * 2f);
				<BakeTextures>c__AnonStorey.targetUVS[j] = vector2;
			}
		}
		<BakeTextures>c__AnonStorey.sourceData = new ushort[4194304];
		for (int k = 0; k < <BakeTextures>c__AnonStorey.sourceTriangles.Length; k += 3)
		{
			int num = <BakeTextures>c__AnonStorey.sourceTriangles[k];
			int num2 = <BakeTextures>c__AnonStorey.sourceTriangles[k + 1];
			int num3 = <BakeTextures>c__AnonStorey.sourceTriangles[k + 2];
			Vector3 vector3 = <BakeTextures>c__AnonStorey.sourceVertices[num];
			Vector3 vector4 = <BakeTextures>c__AnonStorey.sourceVertices[num2];
			Vector3 vector5 = <BakeTextures>c__AnonStorey.sourceVertices[num3];
			Vector3 vector6 = Vector3.Min(Vector3.Min(vector3, vector4), vector5);
			Vector3 vector7 = Vector3.Max(Vector3.Max(vector3, vector4), vector5);
			int num4 = Mathf.Clamp(Mathf.FloorToInt((vector6.x - <BakeTextures>c__AnonStorey.sourceMin.x) / (<BakeTextures>c__AnonStorey.sourceMax.x - <BakeTextures>c__AnonStorey.sourceMin.x) * 16f), 0, 15);
			int num5 = Mathf.Clamp(Mathf.FloorToInt((vector7.x - <BakeTextures>c__AnonStorey.sourceMin.x) / (<BakeTextures>c__AnonStorey.sourceMax.x - <BakeTextures>c__AnonStorey.sourceMin.x) * 16f), 0, 15);
			int num6 = Mathf.Clamp(Mathf.FloorToInt((vector6.y - <BakeTextures>c__AnonStorey.sourceMin.y) / (<BakeTextures>c__AnonStorey.sourceMax.y - <BakeTextures>c__AnonStorey.sourceMin.y) * 16f), 0, 15);
			int num7 = Mathf.Clamp(Mathf.FloorToInt((vector7.y - <BakeTextures>c__AnonStorey.sourceMin.y) / (<BakeTextures>c__AnonStorey.sourceMax.y - <BakeTextures>c__AnonStorey.sourceMin.y) * 16f), 0, 15);
			int num8 = Mathf.Clamp(Mathf.FloorToInt((vector6.z - <BakeTextures>c__AnonStorey.sourceMin.z) / (<BakeTextures>c__AnonStorey.sourceMax.z - <BakeTextures>c__AnonStorey.sourceMin.z) * 16f), 0, 15);
			int num9 = Mathf.Clamp(Mathf.FloorToInt((vector7.z - <BakeTextures>c__AnonStorey.sourceMin.z) / (<BakeTextures>c__AnonStorey.sourceMax.z - <BakeTextures>c__AnonStorey.sourceMin.z) * 16f), 0, 15);
			for (int l = num8; l <= num9; l++)
			{
				for (int m = num6; m <= num7; m++)
				{
					for (int n = num4; n <= num5; n++)
					{
						int num10 = n * 1024 + m * 16384 + l * 262144;
						int num11 = (int)<BakeTextures>c__AnonStorey.sourceData[num10];
						if (num11 < 1023)
						{
							num11++;
							<BakeTextures>c__AnonStorey.sourceData[num10 + num11] = (ushort)(k / 3);
							<BakeTextures>c__AnonStorey.sourceData[num10] = (ushort)num11;
						}
					}
				}
			}
		}
		targetWidth = 1024;
		targetHeight = 1024;
		if (autoSize)
		{
			float num12 = 0f;
			float num13 = 0f;
			for (int num14 = 0; num14 < <BakeTextures>c__AnonStorey.targetTriangles.Length; num14 += 3)
			{
				int num15 = <BakeTextures>c__AnonStorey.targetTriangles[num14];
				int num16 = <BakeTextures>c__AnonStorey.targetTriangles[num14 + 1];
				int num17 = <BakeTextures>c__AnonStorey.targetTriangles[num14 + 2];
				Vector3 vector8 = <BakeTextures>c__AnonStorey.targetVertices[num15];
				Vector3 vector9 = <BakeTextures>c__AnonStorey.targetVertices[num16];
				Vector3 vector10 = <BakeTextures>c__AnonStorey.targetVertices[num17];
				num12 += Triangle3.Area(vector8, vector9, vector10);
				Vector2 vector11 = <BakeTextures>c__AnonStorey.targetUVS[num15];
				Vector2 vector12 = <BakeTextures>c__AnonStorey.targetUVS[num16];
				Vector2 vector13 = <BakeTextures>c__AnonStorey.targetUVS[num17];
				num13 += Triangle2.Area(vector11, vector12, vector13);
			}
			if (num13 > 1f)
			{
				num12 /= num13;
			}
			targetWidth = (int)(Mathf.Sqrt(num12) * sizeBias);
			for (int num18 = 1; num18 < 31; num18++)
			{
				targetWidth &= ~(targetWidth >> num18);
			}
			targetWidth = Mathf.Clamp(targetWidth, minSize, maxSize);
			targetHeight = targetWidth;
		}
		<BakeTextures>c__AnonStorey.internalWidth = Mathf.Min(1024, targetWidth << internalSizeBias);
		<BakeTextures>c__AnonStorey.internalHeight = Mathf.Min(1024, targetHeight << internalSizeBias);
		<BakeTextures>c__AnonStorey.internalDiffuse = new Color32[<BakeTextures>c__AnonStorey.internalWidth * <BakeTextures>c__AnonStorey.internalHeight];
		<BakeTextures>c__AnonStorey.internalXYS = new Color32[<BakeTextures>c__AnonStorey.internalWidth * <BakeTextures>c__AnonStorey.internalHeight];
		<BakeTextures>c__AnonStorey.internalACI = new Color32[<BakeTextures>c__AnonStorey.internalWidth * <BakeTextures>c__AnonStorey.internalHeight];
		<BakeTextures>c__AnonStorey.fillEmptyAreas = false;
		<BakeTextures>c__AnonStorey.locks = new object[<BakeTextures>c__AnonStorey.internalHeight];
		for (int num19 = 0; num19 < <BakeTextures>c__AnonStorey.internalHeight; num19++)
		{
			<BakeTextures>c__AnonStorey.locks[num19] = new object();
		}
		Thread[] array = new Thread[<BakeTextures>c__AnonStorey.threadCount];
		<BakeTextures>c__AnonStorey.completed = new bool[<BakeTextures>c__AnonStorey.threadCount];
		int i;
		for (i = 0; i < <BakeTextures>c__AnonStorey.threadCount; i++)
		{
			ThreadStart start = delegate
			{
				MeshSimplification.BakeThread(<BakeTextures>c__AnonStorey.internalDiffuse, <BakeTextures>c__AnonStorey.internalXYS, <BakeTextures>c__AnonStorey.internalACI, <BakeTextures>c__AnonStorey.targetVertices, <BakeTextures>c__AnonStorey.targetNormals, <BakeTextures>c__AnonStorey.smoothNormals, <BakeTextures>c__AnonStorey.targetTangents, <BakeTextures>c__AnonStorey.targetUVS, <BakeTextures>c__AnonStorey.targetTriangles, <BakeTextures>c__AnonStorey.internalWidth, <BakeTextures>c__AnonStorey.internalHeight, i * 3, <BakeTextures>c__AnonStorey.threadCount * 3, <BakeTextures>c__AnonStorey.sourceDiffuse, <BakeTextures>c__AnonStorey.sourceXYS, <BakeTextures>c__AnonStorey.sourceACI, <BakeTextures>c__AnonStorey.sourceVertices, <BakeTextures>c__AnonStorey.sourceNormals, <BakeTextures>c__AnonStorey.sourceTangents, <BakeTextures>c__AnonStorey.sourceUVS, <BakeTextures>c__AnonStorey.sourceTriangles, <BakeTextures>c__AnonStorey.sourceWidth, <BakeTextures>c__AnonStorey.sourceHeight, <BakeTextures>c__AnonStorey.sourceMin, <BakeTextures>c__AnonStorey.sourceMax, <BakeTextures>c__AnonStorey.sourceData, <BakeTextures>c__AnonStorey.locks, <BakeTextures>c__AnonStorey.rayLength, ref <BakeTextures>c__AnonStorey.fillEmptyAreas, ref <BakeTextures>c__AnonStorey.completed[i]);
			};
			array[i] = new Thread(start);
			array[i].Name = "Texture Bake " + i;
			array[i].Start();
			Thread.Sleep(1);
		}
		bool flag = true;
		while (flag)
		{
			Thread.Sleep(100);
			flag = false;
			for (int num20 = 0; num20 < <BakeTextures>c__AnonStorey.threadCount; num20++)
			{
				if (array[num20] != null)
				{
					if (<BakeTextures>c__AnonStorey.completed[num20])
					{
						array[num20] = null;
					}
					else if (array[num20].IsAlive)
					{
						flag = true;
					}
					else
					{
						Debug.LogError(string.Concat(new object[]
						{
							"Thread ",
							num20,
							" not alive: ",
							array[num20].ThreadState.ToString()
						}));
						array[num20] = null;
					}
				}
			}
		}
		while (<BakeTextures>c__AnonStorey.fillEmptyAreas)
		{
			<BakeTextures>c__AnonStorey.fillEmptyAreas = false;
			for (int num21 = 0; num21 < <BakeTextures>c__AnonStorey.internalHeight; num21++)
			{
				for (int num22 = 0; num22 < <BakeTextures>c__AnonStorey.internalWidth; num22++)
				{
					int num23 = num21 * <BakeTextures>c__AnonStorey.internalWidth + num22;
					<BakeTextures>c__AnonStorey.internalXYS[num23].a = <BakeTextures>c__AnonStorey.internalDiffuse[num23].a;
				}
			}
			for (int num24 = 0; num24 < <BakeTextures>c__AnonStorey.internalHeight; num24++)
			{
				for (int num25 = 0; num25 < <BakeTextures>c__AnonStorey.internalWidth; num25++)
				{
					int num26 = num24 * <BakeTextures>c__AnonStorey.internalWidth + num25;
					byte a = <BakeTextures>c__AnonStorey.internalXYS[num26].a;
					if (a == 0)
					{
						int num27 = num24 - 1 & <BakeTextures>c__AnonStorey.internalHeight - 1;
						int num28 = num27 * <BakeTextures>c__AnonStorey.internalWidth + num25;
						Color32 color = <BakeTextures>c__AnonStorey.internalXYS[num28];
						if (color.a != 0)
						{
							color.a = a;
							<BakeTextures>c__AnonStorey.internalDiffuse[num26] = <BakeTextures>c__AnonStorey.internalDiffuse[num28];
							<BakeTextures>c__AnonStorey.internalXYS[num26] = color;
							Color32 color2 = <BakeTextures>c__AnonStorey.internalACI[num28];
							if (!emptyOpaque)
							{
								color2.r = 255;
							}
							<BakeTextures>c__AnonStorey.internalACI[num26] = color2;
						}
						else
						{
							int num29 = num25 - 1 & <BakeTextures>c__AnonStorey.internalWidth - 1;
							num28 = num24 * <BakeTextures>c__AnonStorey.internalWidth + num29;
							color = <BakeTextures>c__AnonStorey.internalXYS[num28];
							if (color.a != 0)
							{
								color.a = a;
								<BakeTextures>c__AnonStorey.internalDiffuse[num26] = <BakeTextures>c__AnonStorey.internalDiffuse[num28];
								<BakeTextures>c__AnonStorey.internalXYS[num26] = color;
								Color32 color3 = <BakeTextures>c__AnonStorey.internalACI[num28];
								if (!emptyOpaque)
								{
									color3.r = 255;
								}
								<BakeTextures>c__AnonStorey.internalACI[num26] = color3;
							}
							else
							{
								num29 = (num25 + 1 & <BakeTextures>c__AnonStorey.internalWidth - 1);
								num28 = num24 * <BakeTextures>c__AnonStorey.internalWidth + num29;
								color = <BakeTextures>c__AnonStorey.internalXYS[num28];
								if (color.a != 0)
								{
									color.a = a;
									<BakeTextures>c__AnonStorey.internalDiffuse[num26] = <BakeTextures>c__AnonStorey.internalDiffuse[num28];
									<BakeTextures>c__AnonStorey.internalXYS[num26] = color;
									Color32 color4 = <BakeTextures>c__AnonStorey.internalACI[num28];
									if (!emptyOpaque)
									{
										color4.r = 255;
									}
									<BakeTextures>c__AnonStorey.internalACI[num26] = color4;
								}
								else
								{
									num27 = (num24 + 1 & <BakeTextures>c__AnonStorey.internalHeight - 1);
									num28 = num27 * <BakeTextures>c__AnonStorey.internalWidth + num25;
									color = <BakeTextures>c__AnonStorey.internalXYS[num28];
									if (color.a != 0)
									{
										color.a = a;
										<BakeTextures>c__AnonStorey.internalDiffuse[num26] = <BakeTextures>c__AnonStorey.internalDiffuse[num28];
										<BakeTextures>c__AnonStorey.internalXYS[num26] = color;
										Color32 color5 = <BakeTextures>c__AnonStorey.internalACI[num28];
										if (!emptyOpaque)
										{
											color5.r = 255;
										}
										<BakeTextures>c__AnonStorey.internalACI[num26] = color5;
									}
									else
									{
										<BakeTextures>c__AnonStorey.fillEmptyAreas = true;
									}
								}
							}
						}
					}
				}
			}
		}
		if (targetWidth == <BakeTextures>c__AnonStorey.internalWidth && targetHeight == <BakeTextures>c__AnonStorey.internalHeight)
		{
			targetDiffuse = <BakeTextures>c__AnonStorey.internalDiffuse;
			targetXYS = <BakeTextures>c__AnonStorey.internalXYS;
			targetACI = <BakeTextures>c__AnonStorey.internalACI;
		}
		else
		{
			targetDiffuse = new Color32[targetWidth * targetHeight];
			MeshSimplification.DownscaleImage(<BakeTextures>c__AnonStorey.internalDiffuse, targetDiffuse, <BakeTextures>c__AnonStorey.internalWidth, <BakeTextures>c__AnonStorey.internalHeight, targetWidth, targetHeight);
			targetXYS = new Color32[targetWidth * targetHeight];
			MeshSimplification.DownscaleImage(<BakeTextures>c__AnonStorey.internalXYS, targetXYS, <BakeTextures>c__AnonStorey.internalWidth, <BakeTextures>c__AnonStorey.internalHeight, targetWidth, targetHeight);
			targetACI = new Color32[targetWidth * targetHeight];
			MeshSimplification.DownscaleImage(<BakeTextures>c__AnonStorey.internalACI, targetACI, <BakeTextures>c__AnonStorey.internalWidth, <BakeTextures>c__AnonStorey.internalHeight, targetWidth, targetHeight);
		}
	}

	private static void BakeThread(Color32[] targetDiffuse, Color32[] targetXYS, Color32[] targetACI, Vector3[] targetVertices, Vector3[] targetNormals, Vector3[] smoothNormals, Vector4[] targetTangents, Vector2[] targetUVS, int[] targetTriangles, int targetWidth, int targetHeight, int targetTriangleStart, int targetTriangleDelta, Color32[] sourceDiffuse, Color32[] sourceXYS, Color32[] sourceACI, Vector3[] sourceVertices, Vector3[] sourceNormals, Vector4[] sourceTangents, Vector2[] sourceUVS, int[] sourceTriangles, int sourceWidth, int sourceHeight, Vector3 sourceMin, Vector3 sourceMax, ushort[] sourceData, object[] locks, float rayLength, ref bool fillEmptyAreas, ref bool completed)
	{
		try
		{
			HashSet<int> hashSet = new HashSet<int>();
			for (int i = targetTriangleStart; i < targetTriangles.Length; i += targetTriangleDelta)
			{
				int num = targetTriangles[i];
				int num2 = targetTriangles[i + 1];
				int num3 = targetTriangles[i + 2];
				Vector3 vector = targetVertices[num];
				Vector3 vector2 = targetVertices[num2];
				Vector3 vector3 = targetVertices[num3];
				Vector3 vector4 = targetNormals[num];
				Vector3 vector5 = targetNormals[num2];
				Vector3 vector6 = targetNormals[num3];
				Vector3 vector7 = smoothNormals[num];
				Vector3 vector8 = smoothNormals[num2];
				Vector3 vector9 = smoothNormals[num3];
				Vector4 vector10 = targetTangents[num];
				Vector4 vector11 = targetTangents[num2];
				Vector4 vector12 = targetTangents[num3];
				Vector2 vector13 = targetUVS[num];
				Vector2 vector14 = targetUVS[num2];
				Vector2 vector15 = targetUVS[num3];
				int num4 = Mathf.Max(0, (int)(Mathf.Min(Mathf.Min(vector13.x, vector14.x), vector15.x) * (float)targetWidth));
				int num5 = Mathf.Min(targetWidth - 1, (int)(Mathf.Max(Mathf.Max(vector13.x, vector14.x), vector15.x) * (float)targetWidth));
				int num6 = Mathf.Max(0, (int)(Mathf.Min(Mathf.Min(vector13.y, vector14.y), vector15.y) * (float)targetHeight));
				int num7 = Mathf.Min(targetHeight - 1, (int)(Mathf.Max(Mathf.Max(vector13.y, vector14.y), vector15.y) * (float)targetHeight));
				for (int j = num6; j <= num7; j++)
				{
					for (int k = num4; k <= num5; k++)
					{
						int num8 = j * targetWidth + k;
						if (targetDiffuse[num8].a == 255)
						{
							break;
						}
						Vector2 vector16;
						vector16..ctor(((float)k + 0.5f) / (float)targetWidth, ((float)j + 0.5f) / (float)targetHeight);
						float num9;
						float num10;
						if (Triangle2.Intersect(vector13, vector14, vector15, vector16, ref num9, ref num10))
						{
							Vector3 vector17 = vector;
							vector17 += (vector2 - vector) * num9;
							vector17 += (vector3 - vector) * num10;
							Vector3 vector18 = vector4;
							vector18 += (vector5 - vector4) * num9;
							vector18 += (vector6 - vector4) * num10;
							vector18.Normalize();
							Vector3 vector19 = vector7;
							vector19 += (vector8 - vector7) * num9;
							vector19 += (vector9 - vector7) * num10;
							vector19.Normalize();
							Vector4 vector20 = vector10;
							vector20 += (vector11 - vector10) * num9;
							vector20 += (vector12 - vector10) * num10;
							Vector3 vector21;
							vector21..ctor(vector20.x, vector20.y, vector20.z);
							vector21.Normalize();
							Vector3 vector22 = Vector3.Cross(vector18, vector21);
							if (vector20.w < 0f)
							{
								vector22 = -vector22;
							}
							vector22.Normalize();
							float num11 = 2f;
							float num12 = 0f;
							Color32 color = default(Color32);
							Color32 color2 = default(Color32);
							Color32 color3 = default(Color32);
							for (int l = 0; l < 2; l++)
							{
								Vector3 vector23;
								Vector3 vector24;
								if (l == 0)
								{
									vector23 = vector17 + vector18 * rayLength;
									vector24 = vector17 - vector18 * rayLength;
								}
								else
								{
									vector23 = vector17 + vector19 * rayLength;
									vector24 = vector17 - vector19 * rayLength;
								}
								Vector3 vector25 = Vector3.Min(vector23, vector24);
								Vector3 vector26 = Vector3.Max(vector23, vector24);
								Bounds bounds = default(Bounds);
								bounds.SetMinMax(vector25, vector26);
								int num13 = Mathf.Max(0, Mathf.FloorToInt((vector25.x - sourceMin.x) / (sourceMax.x - sourceMin.x) * 16f));
								int num14 = Mathf.Min(15, Mathf.FloorToInt((vector26.x - sourceMin.x) / (sourceMax.x - sourceMin.x) * 16f));
								int num15 = Mathf.Max(0, Mathf.FloorToInt((vector25.y - sourceMin.y) / (sourceMax.y - sourceMin.y) * 16f));
								int num16 = Mathf.Min(15, Mathf.FloorToInt((vector26.y - sourceMin.y) / (sourceMax.y - sourceMin.y) * 16f));
								int num17 = Mathf.Max(0, Mathf.FloorToInt((vector25.z - sourceMin.z) / (sourceMax.z - sourceMin.z) * 16f));
								int num18 = Mathf.Min(15, Mathf.FloorToInt((vector26.z - sourceMin.z) / (sourceMax.z - sourceMin.z) * 16f));
								hashSet.Clear();
								for (int m = num17; m <= num18; m++)
								{
									for (int n = num15; n <= num16; n++)
									{
										for (int num19 = num13; num19 <= num14; num19++)
										{
											int num20 = num19 * 1024 + n * 16384 + m * 262144;
											int num21 = (int)sourceData[num20];
											for (int num22 = 1; num22 <= num21; num22++)
											{
												int num23 = (int)(sourceData[num20 + num22] * 3);
												if (!hashSet.Contains(num23))
												{
													hashSet.Add(num23);
													int num24 = sourceTriangles[num23];
													int num25 = sourceTriangles[num23 + 1];
													int num26 = sourceTriangles[num23 + 2];
													Vector3 vector27 = sourceVertices[num24];
													Vector3 vector28 = sourceVertices[num25];
													Vector3 vector29 = sourceVertices[num26];
													Vector3 vector30 = Vector3.Min(Vector3.Min(vector27, vector28), vector29);
													Vector3 vector31 = Vector3.Max(Vector3.Max(vector27, vector28), vector29);
													Bounds bounds2 = default(Bounds);
													bounds2.SetMinMax(vector30, vector31);
													float num27;
													float num28;
													float num29;
													if (bounds.Intersects(bounds2) && Triangle3.Intersect(vector27, vector28, vector29, vector23, vector24, ref num27, ref num28, ref num29) && num29 < num11)
													{
														Vector2 vector32 = sourceUVS[num24];
														Vector2 vector33 = sourceUVS[num25];
														Vector2 vector34 = sourceUVS[num26];
														Vector2 vector35 = vector32;
														vector35 += (vector33 - vector32) * num27;
														vector35 += (vector34 - vector32) * num28;
														Color32 color4 = MeshSimplification.SampleColor(sourceDiffuse, sourceWidth, sourceHeight, vector35, new Color32(128, 128, 128, 255));
														Color32 color5 = MeshSimplification.SampleColor(sourceXYS, sourceWidth, sourceHeight, vector35, new Color32(128, 128, 255, 255));
														Color32 color6 = MeshSimplification.SampleColor(sourceACI, sourceWidth, sourceHeight, vector35, new Color32(0, 0, 0, 255));
														if ((num29 < num11 && color6.r < 128) || num11 > 1f)
														{
															Vector3 vector36 = sourceNormals[num24];
															Vector3 vector37 = sourceNormals[num25];
															Vector3 vector38 = sourceNormals[num26];
															Vector4 vector39 = sourceTangents[num24];
															Vector4 vector40 = sourceTangents[num25];
															Vector4 vector41 = sourceTangents[num26];
															Vector3 vector42 = vector36;
															vector42 += (vector37 - vector36) * num27;
															vector42 += (vector38 - vector36) * num28;
															vector42.Normalize();
															float num30 = Vector3.Dot(vector42, vector18);
															if (num30 > 0f)
															{
																Vector4 vector43 = vector39;
																vector43 += (vector40 - vector39) * num27;
																vector43 += (vector41 - vector39) * num28;
																Vector3 vector44;
																vector44..ctor(vector43.x, vector43.y, vector43.z);
																vector44.Normalize();
																Vector3 vector45 = Vector3.Cross(vector42, vector44);
																if (vector43.w < 0f)
																{
																	vector45 = -vector45;
																}
																vector45.Normalize();
																float num31 = (float)color5.r / 127.5f - 1f;
																float num32 = (float)color5.g / 127.5f - 1f;
																Vector3 vector46 = vector44 * num31 + vector45 * num32 + vector42 * Mathf.Sqrt(Mathf.Max(0f, 1f - num31 * num31 - num32 * num32));
																num31 = Vector3.Dot(vector46, vector21);
																num32 = Vector3.Dot(vector46, vector22);
																color5.r = (byte)Mathf.Clamp(Mathf.RoundToInt((num31 + 1f) * 127.5f), 0, 255);
																color5.g = (byte)Mathf.Clamp(Mathf.RoundToInt((num32 + 1f) * 127.5f), 0, 255);
																color = color4;
																color2 = color5;
																color3 = color6;
																num11 = num29;
																num12 = num30;
															}
														}
													}
												}
											}
										}
									}
								}
								if (num12 > 0.9f)
								{
									break;
								}
							}
							if (num11 <= 1f)
							{
								float num33 = 0f;
								if (num9 < 0f)
								{
									num33 = Mathf.Max(num33, -num9);
								}
								if (num10 < 0f)
								{
									num33 = Mathf.Max(num33, -num10);
								}
								if (num9 + num10 > 1f)
								{
									num33 = Mathf.Max(num33, num9 + num10 - 1f);
								}
								int num34;
								if (num33 == 0f)
								{
									num34 = 255;
								}
								else
								{
									num34 = (int)(254f / (1f + num33 * 253f));
								}
								object obj = locks[j];
								lock (obj)
								{
									if (num34 > (int)targetDiffuse[num8].a)
									{
										color.a = (byte)num34;
										targetDiffuse[num8] = color;
										targetXYS[num8] = color2;
										targetACI[num8] = color3;
										fillEmptyAreas = true;
									}
								}
							}
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			Debug.LogError(ex.Message + "\n" + ex.StackTrace);
		}
		completed = true;
	}

	public static void CreateCubeProjectedUV(Mesh mesh)
	{
		Vector3[] vertices = mesh.get_vertices();
		Vector3[] normals = mesh.get_normals();
		Vector2[] array = new Vector2[mesh.get_vertices().Length];
		int[] array2 = new int[vertices.Length];
		Vector3 vector;
		vector..ctor(100000f, 100000f, 100000f);
		Vector3 vector2;
		vector2..ctor(-100000f, -100000f, -100000f);
		for (int i = 0; i < vertices.Length; i++)
		{
			array2[i] = 2;
			Vector3 vector3 = vertices[i];
			Vector3 vector4 = normals[i];
			if (Mathf.Abs(vector4.x) > Mathf.Abs(vector4.y))
			{
				if (Mathf.Abs(vector4.x) > Mathf.Abs(vector4.z))
				{
					array2[i] = 0;
				}
				else
				{
					array2[i] = 2;
				}
			}
			else if (Mathf.Abs(vector4.y) > Mathf.Abs(vector4.z))
			{
				array2[i] = 1;
			}
			if (vector4.get_Item(array2[i]) > 0f)
			{
				array2[i] += 3;
			}
			for (int j = 0; j < vertices.Length; j++)
			{
				vector = Vector3.Min(vector, vector3);
				vector2 = Vector3.Max(vector2, vector3);
			}
		}
		Vector3 vector5 = vector2 - vector;
		float num = 0.333333343f;
		Vector2[] array3 = new Vector2[]
		{
			new Vector2(0f, num),
			new Vector2(0f, 0f),
			new Vector2(num, 2f * num),
			new Vector2(2f * num, num),
			new Vector2(num, num),
			new Vector2(num, 0f)
		};
		for (int k = 0; k < vertices.Length; k++)
		{
			Vector3 vector6 = vertices[k];
			int[] array4 = new int[]
			{
				(array2[k] + 1) % 3,
				(array2[k] + 2) % 3
			};
			Vector2 vector7;
			vector7..ctor(vector6.get_Item(array4[0]), vector6.get_Item(array4[1]));
			vector7.x = (vector7.x - vector.get_Item(array4[0])) / vector5.get_Item(array4[0]) / 3f;
			vector7.y = (vector7.y - vector.get_Item(array4[1])) / vector5.get_Item(array4[1]) / 3f;
			vector7 += array3[array2[k]];
			array[k] = vector7;
		}
		mesh.set_uv(array);
	}

	public struct SimplificationMesh
	{
		public Vector3[] vertices;

		public int[] triangles;

		public Vector3[] normals;

		public Vector4[] tangents;

		public Vector2[] uv;
	}

	public class SimplificationParameters
	{
		public SimplificationParameters()
		{
			this.resolution = 21;
			this.minVolume = 10;
			this.relocateCorners = true;
			this.useWedges = true;
			this.minCandidates = 80;
			this.postProcess = false;
			this.postProcessMaxError = 10;
			this.alternateUVs = false;
			this.onlyPostProcess = false;
			this.postProcessTriangles = 100;
			this.maxTriangles = -1;
		}

		public int resolution;

		public int minVolume;

		public bool relocateCorners;

		public bool useWedges;

		public int minCandidates;

		public bool postProcess;

		public int postProcessMaxError;

		public bool onlyPostProcess;

		public int postProcessTriangles;

		public bool alternateUVs;

		public int maxTriangles;
	}

	protected delegate int ScoreCounter(int[] pos);

	protected abstract class SimplificationVolume
	{
		public abstract IEnumerable<int[]> Cells();

		public abstract bool Clamp(int min, int max);

		public abstract int GetExpansions();

		public abstract MeshSimplification.SimplificationVolume GetExpansion(int i);

		public abstract int GetTypeID();

		public abstract int ScoreDelta(MeshSimplification.SimplificationVolume vol, MeshSimplification.ScoreCounter c);

		public abstract float GetMaxDim();

		public abstract void GetMesh(MeshSimplification.CellStatus[,,] data, out List<Vector3> volumeVerts, out List<Vector3> volumeNorms, out int[] volumeTriangles, out Vector2[] volumeUVs);
	}

	protected enum CellStatus
	{
		OUTSIDE = -1,
		INSIDE,
		PROCESSED = 2
	}

	protected class SimplificationCube : MeshSimplification.SimplificationVolume
	{
		public SimplificationCube(int[,] data)
		{
			this.m_data = data;
		}

		public override int GetTypeID()
		{
			return 0;
		}

		[DebuggerHidden]
		public override IEnumerable<int[]> Cells()
		{
			MeshSimplification.SimplificationCube.<Cells>c__Iterator0 <Cells>c__Iterator = new MeshSimplification.SimplificationCube.<Cells>c__Iterator0();
			<Cells>c__Iterator.$this = this;
			MeshSimplification.SimplificationCube.<Cells>c__Iterator0 expr_0E = <Cells>c__Iterator;
			expr_0E.$PC = -2;
			return expr_0E;
		}

		public override float GetMaxDim()
		{
			int val = 1 + this.m_data[1, 0] - this.m_data[0, 0];
			int val2 = 1 + this.m_data[1, 1] - this.m_data[0, 1];
			int val3 = 1 + this.m_data[1, 2] - this.m_data[0, 2];
			return (float)Math.Max(Math.Max(val, val2), val3);
		}

		public override int ScoreDelta(MeshSimplification.SimplificationVolume vol, MeshSimplification.ScoreCounter counter)
		{
			MeshSimplification.SimplificationCube simplificationCube = (MeshSimplification.SimplificationCube)vol;
			int num;
			int num2;
			int num3;
			if (simplificationCube.m_data[0, 0] != this.m_data[0, 0] || simplificationCube.m_data[1, 0] != this.m_data[1, 0])
			{
				num = 0;
				num2 = 1;
				num3 = 2;
			}
			else if (simplificationCube.m_data[0, 1] != this.m_data[0, 1] || simplificationCube.m_data[1, 1] != this.m_data[1, 1])
			{
				num = 1;
				num2 = 0;
				num3 = 2;
			}
			else
			{
				num = 2;
				num2 = 0;
				num3 = 1;
			}
			int[] array = new int[3];
			array[num] = ((simplificationCube.m_data[0, num] != this.m_data[0, num]) ? this.m_data[0, num] : this.m_data[1, num]);
			int num4 = 0;
			array[num2] = this.m_data[0, num2];
			while (array[num2] <= this.m_data[1, num2])
			{
				array[num3] = this.m_data[0, num3];
				while (array[num3] <= this.m_data[1, num3])
				{
					num4 += counter(array);
					array[num3]++;
				}
				array[num2]++;
			}
			return num4;
		}

		public override bool Clamp(int min, int max)
		{
			bool result = false;
			for (int i = 0; i < 3; i++)
			{
				if (this.m_data[0, i] < min)
				{
					this.m_data[0, i] = min;
					result = true;
				}
				if (this.m_data[1, i] > max)
				{
					this.m_data[1, i] = max;
					result = true;
				}
			}
			return result;
		}

		public override int GetExpansions()
		{
			return 6;
		}

		public override MeshSimplification.SimplificationVolume GetExpansion(int i)
		{
			int[,] array = (int[,])this.m_data.Clone();
			array[MeshSimplification.SimplificationCube.dirLookup[i, 0], 0] += MeshSimplification.SimplificationCube.dirLookup[i, 1];
			array[MeshSimplification.SimplificationCube.dirLookup[i, 0], 1] += MeshSimplification.SimplificationCube.dirLookup[i, 2];
			array[MeshSimplification.SimplificationCube.dirLookup[i, 0], 2] += MeshSimplification.SimplificationCube.dirLookup[i, 3];
			return new MeshSimplification.SimplificationCube(array);
		}

		protected Vector3 GetCorner(int x, int y, int z)
		{
			return new Vector3((float)(this.m_data[x, 0] + x) - 0.5f, (float)(this.m_data[y, 1] + y) - 0.5f, (float)(this.m_data[z, 2] + z) - 0.5f);
		}

		protected void AddTriangle(int[,] corners, Vector3 normal, List<Vector3> verts, List<Vector3> norms, List<int> triangles)
		{
			verts.Add(this.GetCorner(corners[0, 0], corners[0, 1], corners[0, 2]));
			verts.Add(this.GetCorner(corners[1, 0], corners[1, 1], corners[1, 2]));
			verts.Add(this.GetCorner(corners[2, 0], corners[2, 1], corners[2, 2]));
			norms.Add(normal);
			norms.Add(normal);
			norms.Add(normal);
			triangles.Add(verts.Count<Vector3>() - 3);
			triangles.Add(verts.Count<Vector3>() - 2);
			triangles.Add(verts.Count<Vector3>() - 1);
		}

		protected void AddQuad(int[,] corners, Vector3 normal, List<Vector3> verts, List<Vector3> norms, List<int> triangles)
		{
			verts.Add(this.GetCorner(corners[0, 0], corners[0, 1], corners[0, 2]));
			verts.Add(this.GetCorner(corners[1, 0], corners[1, 1], corners[1, 2]));
			verts.Add(this.GetCorner(corners[2, 0], corners[2, 1], corners[2, 2]));
			verts.Add(this.GetCorner(corners[3, 0], corners[3, 1], corners[3, 2]));
			norms.Add(normal);
			norms.Add(normal);
			norms.Add(normal);
			norms.Add(normal);
			triangles.Add(verts.Count<Vector3>() - 4);
			triangles.Add(verts.Count<Vector3>() - 3);
			triangles.Add(verts.Count<Vector3>() - 2);
			triangles.Add(verts.Count<Vector3>() - 4);
			triangles.Add(verts.Count<Vector3>() - 2);
			triangles.Add(verts.Count<Vector3>() - 1);
		}

		public bool IsWallSolid(MeshSimplification.CellStatus[,,] data, int naxis, int ndir)
		{
			int[] array = new int[3];
			int num;
			int num2;
			if (naxis == 0)
			{
				num = 1;
				num2 = 2;
			}
			else if (naxis == 1)
			{
				num = 0;
				num2 = 2;
			}
			else
			{
				num = 0;
				num2 = 1;
			}
			array[num] = this.m_data[0, num];
			while (array[num] <= this.m_data[1, num])
			{
				array[num2] = this.m_data[0, num2];
				while (array[num2] <= this.m_data[1, num2])
				{
					array[naxis] = this.m_data[(ndir <= 0) ? 0 : 1, naxis] + ndir;
					if (array[naxis] < 0 || array[naxis] >= data.GetLength(0) || data[array[0], array[1], array[2]] == MeshSimplification.CellStatus.OUTSIDE)
					{
						return false;
					}
					array[num2]++;
				}
				array[num]++;
			}
			return true;
		}

		public void AddUVQuad(float umin, float vmin, float umax, float vmax, List<Vector2> uvs)
		{
			uvs.Add(new Vector2(umin, vmin));
			uvs.Add(new Vector2(umin, vmax));
			uvs.Add(new Vector2(umax, vmax));
			uvs.Add(new Vector2(umax, vmin));
		}

		public override void GetMesh(MeshSimplification.CellStatus[,,] data, out List<Vector3> volumeVerts, out List<Vector3> volumeNorms, out int[] volumeTriangles, out Vector2[] volumeUVs)
		{
			volumeVerts = new List<Vector3>();
			volumeNorms = new List<Vector3>();
			List<Vector2> list = new List<Vector2>();
			List<int> list2 = new List<int>();
			float num = 0.333333343f;
			this.AddQuad(new int[,]
			{
				{
					0,
					1,
					1
				},
				{
					0,
					0,
					1
				},
				{
					1,
					0,
					1
				},
				{
					1,
					1,
					1
				}
			}, new Vector3(0f, 0f, 1f), volumeVerts, volumeNorms, list2);
			this.AddUVQuad(num, 2f * num, 2f * num, 1f, list);
			this.AddQuad(new int[,]
			{
				{
					1,
					1,
					1
				},
				{
					1,
					0,
					1
				},
				{
					1,
					0,
					0
				},
				{
					1,
					1,
					0
				}
			}, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list2);
			this.AddUVQuad(2f * num, 2f * num, 1f, num, list);
			this.AddQuad(new int[,]
			{
				{
					1,
					1,
					0
				},
				{
					1,
					0,
					0
				},
				{
					0,
					0,
					0
				},
				{
					0,
					1,
					0
				}
			}, new Vector3(0f, 0f, -1f), volumeVerts, volumeNorms, list2);
			this.AddUVQuad(2f * num, num, num, 0f, list);
			this.AddQuad(new int[,]
			{
				{
					0,
					1,
					0
				},
				{
					0,
					0,
					0
				},
				{
					0,
					0,
					1
				},
				{
					0,
					1,
					1
				}
			}, new Vector3(-1f, 0f, 0f), volumeVerts, volumeNorms, list2);
			this.AddUVQuad(num, num, 0f, 2f * num, list);
			this.AddQuad(new int[,]
			{
				{
					0,
					1,
					0
				},
				{
					0,
					1,
					1
				},
				{
					1,
					1,
					1
				},
				{
					1,
					1,
					0
				}
			}, new Vector3(0f, 1f, 0f), volumeVerts, volumeNorms, list2);
			this.AddUVQuad(num, num, 2f * num, 2f * num, list);
			volumeTriangles = list2.ToArray();
			volumeUVs = list.ToArray();
		}

		public static bool GetCandidate(int[] root, int number, out MeshSimplification.SimplificationVolume volume)
		{
			if (number == 0)
			{
				int[,] expr_0E = new int[2, 3];
				expr_0E[0, 0] = root[0];
				expr_0E[0, 1] = root[1];
				expr_0E[0, 2] = root[2];
				expr_0E[1, 0] = root[0];
				expr_0E[1, 1] = root[1];
				expr_0E[1, 2] = root[2];
				volume = new MeshSimplification.SimplificationCube(expr_0E);
				return true;
			}
			volume = null;
			return false;
		}

		public int[,] m_data;

		public static int[,] dirLookup = new int[,]
		{
			{
				1,
				1,
				0,
				0
			},
			{
				0,
				-1,
				0,
				0
			},
			{
				1,
				0,
				1,
				0
			},
			{
				0,
				0,
				-1,
				0
			},
			{
				1,
				0,
				0,
				1
			},
			{
				0,
				0,
				0,
				-1
			}
		};
	}

	protected class SimplificationWedge : MeshSimplification.SimplificationCube
	{
		public SimplificationWedge(int thebase, int[,] data) : base(data)
		{
			this.m_base = thebase;
			this.m_axes = new int[]
			{
				Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 0]) - 1,
				Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 1]) - 1,
				Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 2]) - 1
			};
			this.m_signs = new int[]
			{
				Math.Sign(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 0]),
				Math.Sign(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 1])
			};
			if (this.m_signs[0] > 0)
			{
				this.m_uborder = (float)this.m_data[0, this.m_axes[0]];
			}
			else
			{
				this.m_uborder = (float)this.m_data[1, this.m_axes[0]] + 1f;
			}
			if (this.m_signs[1] > 0)
			{
				this.m_vborder = (float)this.m_data[0, this.m_axes[1]];
			}
			else
			{
				this.m_vborder = (float)this.m_data[1, this.m_axes[1]] + 1f;
			}
			this.m_usize = (float)(this.m_data[1, this.m_axes[0]] - this.m_data[0, this.m_axes[0]] + 1);
			this.m_vsize = (float)(this.m_data[1, this.m_axes[1]] - this.m_data[0, this.m_axes[1]] + 1);
			this.m_uvsize = this.m_usize * this.m_vsize + 0.001f;
		}

		[DebuggerHidden]
		public override IEnumerable<int[]> Cells()
		{
			MeshSimplification.SimplificationWedge.<Cells>c__Iterator0 <Cells>c__Iterator = new MeshSimplification.SimplificationWedge.<Cells>c__Iterator0();
			<Cells>c__Iterator.$this = this;
			MeshSimplification.SimplificationWedge.<Cells>c__Iterator0 expr_0E = <Cells>c__Iterator;
			expr_0E.$PC = -2;
			return expr_0E;
		}

		public override int GetExpansions()
		{
			return 6;
		}

		public override int GetTypeID()
		{
			return 20 + this.m_base;
		}

		public bool Inside(int[] pos)
		{
			float num = Mathf.Abs((float)pos[this.m_axes[0]] + 0.5f - this.m_uborder);
			float num2 = Mathf.Abs((float)pos[this.m_axes[1]] + 0.5f - this.m_vborder);
			float num3 = num * this.m_vsize + num2 * this.m_usize;
			return num3 < this.m_uvsize;
		}

		public override int ScoreDelta(MeshSimplification.SimplificationVolume vol, MeshSimplification.ScoreCounter counter)
		{
			MeshSimplification.SimplificationWedge simplificationWedge = (MeshSimplification.SimplificationWedge)vol;
			int num;
			int num2;
			int num3;
			if (simplificationWedge.m_data[0, 0] != this.m_data[0, 0] || simplificationWedge.m_data[1, 0] != this.m_data[1, 0])
			{
				num = 0;
				num2 = 1;
				num3 = 2;
			}
			else if (simplificationWedge.m_data[0, 1] != this.m_data[0, 1] || simplificationWedge.m_data[1, 1] != this.m_data[1, 1])
			{
				num = 1;
				num2 = 0;
				num3 = 2;
			}
			else
			{
				num = 2;
				num2 = 0;
				num3 = 1;
			}
			int num4 = 0;
			if (Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 0]) - 1 == 0)
			{
				if (Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[this.m_base, 0]) - 1 == 1)
				{
					num4 = 1;
				}
				else
				{
					num4 = 2;
				}
			}
			int[] array = new int[3];
			int num5 = 0;
			if (num == num4)
			{
				array[num] = ((simplificationWedge.m_data[0, num] != this.m_data[0, num]) ? this.m_data[0, num] : this.m_data[1, num]);
				array[num2] = this.m_data[0, num2];
				while (array[num2] <= this.m_data[1, num2])
				{
					array[num3] = this.m_data[0, num3];
					while (array[num3] <= this.m_data[1, num3])
					{
						if (this.Inside(array))
						{
							num5 += counter(array);
						}
						array[num3]++;
					}
					array[num2]++;
				}
				return num5;
			}
			if (num4 == 0)
			{
				num2 = 1;
				num3 = 2;
			}
			else if (num4 == 1)
			{
				num2 = 0;
				num3 = 2;
			}
			else
			{
				num2 = 0;
				num3 = 1;
			}
			array[num2] = this.m_data[0, num2];
			while (array[num2] <= this.m_data[1, num2])
			{
				array[num3] = this.m_data[0, num3];
				while (array[num3] <= this.m_data[1, num3])
				{
					array[num4] = this.m_data[0, num4];
					bool flag = this.Inside(array);
					bool flag2 = simplificationWedge.Inside(array);
					if (flag && !flag2)
					{
						array[num4] = this.m_data[0, num4];
						while (array[num4] <= this.m_data[1, num4])
						{
							num5 += counter(array);
							array[num4]++;
						}
					}
					else if (!flag && flag2)
					{
						array[num4] = this.m_data[0, num4];
						while (array[num4] <= this.m_data[1, num4])
						{
							num5 -= counter(array);
							array[num4]++;
						}
					}
					array[num3]++;
				}
				array[num2]++;
			}
			return num5;
		}

		public override MeshSimplification.SimplificationVolume GetExpansion(int i)
		{
			MeshSimplification.SimplificationCube simplificationCube = (MeshSimplification.SimplificationCube)base.GetExpansion(i);
			return new MeshSimplification.SimplificationWedge(this.m_base, simplificationCube.m_data);
		}

		public override void GetMesh(MeshSimplification.CellStatus[,,] data, out List<Vector3> volumeVerts, out List<Vector3> volumeNorms, out int[] volumeTriangles, out Vector2[] volumeUVs)
		{
			volumeVerts = new List<Vector3>();
			volumeNorms = new List<Vector3>();
			List<int> list = new List<int>();
			List<Vector2> list2 = new List<Vector2>();
			int[,] expr_22 = new int[4, 3];
			expr_22[0, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 0];
			expr_22[0, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 1];
			expr_22[0, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 2];
			expr_22[1, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 0];
			expr_22[1, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 1];
			expr_22[1, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 2];
			expr_22[2, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 0];
			expr_22[2, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 1];
			expr_22[2, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 2];
			expr_22[3, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 0];
			expr_22[3, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 1];
			expr_22[3, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 2];
			base.AddQuad(expr_22, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list);
			base.AddUVQuad(0.6666667f, 0f, 0.333333343f, 0.5f, list2);
			int[,] expr_213 = new int[4, 3];
			expr_213[0, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 0];
			expr_213[0, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 1];
			expr_213[0, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 2];
			expr_213[1, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 0];
			expr_213[1, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 1];
			expr_213[1, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 2];
			expr_213[2, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 0];
			expr_213[2, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 1];
			expr_213[2, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 2];
			expr_213[3, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 0];
			expr_213[3, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 1];
			expr_213[3, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 2];
			base.AddQuad(expr_213, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list);
			list2.Add(new Vector2(0.333333343f, 0.5f));
			list2.Add(new Vector2(0.6666667f, 0.5f));
			list2.Add(new Vector2(0.6666667f, 1f));
			list2.Add(new Vector2(0.333333343f, 1f));
			int[,] expr_43D = new int[3, 3];
			expr_43D[0, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 0];
			expr_43D[0, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 1];
			expr_43D[0, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 2];
			expr_43D[1, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 0];
			expr_43D[1, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 1];
			expr_43D[1, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 2], 2];
			expr_43D[2, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 0];
			expr_43D[2, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 1];
			expr_43D[2, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 2];
			base.AddTriangle(expr_43D, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list);
			list2.Add(new Vector2(0f, 0.5f));
			list2.Add(new Vector2(0.333333343f, 0.5f));
			list2.Add(new Vector2(0.333333343f, 1f));
			int[,] expr_5E6 = new int[3, 3];
			expr_5E6[0, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 0];
			expr_5E6[0, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 1];
			expr_5E6[0, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 2];
			expr_5E6[1, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 0];
			expr_5E6[1, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 1];
			expr_5E6[1, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 2];
			expr_5E6[2, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 0];
			expr_5E6[2, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 1];
			expr_5E6[2, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 3], 2];
			base.AddTriangle(expr_5E6, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list);
			list2.Add(new Vector2(1f, 0.5f));
			list2.Add(new Vector2(0.6666667f, 1f));
			list2.Add(new Vector2(0.6666667f, 0.5f));
			int[,] expr_78F = new int[4, 3];
			expr_78F[0, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 0];
			expr_78F[0, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 1];
			expr_78F[0, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 1], 2];
			expr_78F[1, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 0];
			expr_78F[1, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 1];
			expr_78F[1, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 4], 2];
			expr_78F[2, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 0];
			expr_78F[2, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 1];
			expr_78F[2, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 5], 2];
			expr_78F[3, 0] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 0];
			expr_78F[3, 1] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 1];
			expr_78F[3, 2] = MeshSimplification.SimplificationWedge.vertices[MeshSimplification.SimplificationWedge.orientation[this.m_base, 0], 2];
			base.AddQuad(expr_78F, new Vector3(1f, 0f, 0f), volumeVerts, volumeNorms, list);
			base.AddUVQuad(0.333333343f, 0.5f, 0.6666667f, 1f, list2);
			volumeTriangles = list.ToArray();
			volumeUVs = list2.ToArray();
		}

		public new static bool GetCandidate(int[] root, int number, out MeshSimplification.SimplificationVolume volume)
		{
			if (number < 12)
			{
				int[,] expr_0F = new int[2, 3];
				expr_0F[0, 0] = root[0];
				expr_0F[0, 1] = root[1];
				expr_0F[0, 2] = root[2];
				expr_0F[1, 0] = root[0];
				expr_0F[1, 1] = root[1];
				expr_0F[1, 2] = root[2];
				int[,] array = expr_0F;
				array[(MeshSimplification.SimplificationWedge.candidateLookup[number, 0] <= 0) ? 0 : 1, Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[number, 0]) - 1] += ((MeshSimplification.SimplificationWedge.candidateLookup[number, 0] <= 0) ? -1 : 1);
				array[(MeshSimplification.SimplificationWedge.candidateLookup[number, 1] <= 0) ? 0 : 1, Math.Abs(MeshSimplification.SimplificationWedge.candidateLookup[number, 1]) - 1] += ((MeshSimplification.SimplificationWedge.candidateLookup[number, 1] <= 0) ? -1 : 1);
				int[,] expr_F9 = new int[2, 3];
				expr_F9[0, 0] = array[0, 0];
				expr_F9[0, 1] = array[0, 1];
				expr_F9[0, 2] = array[0, 2];
				expr_F9[1, 0] = array[1, 0];
				expr_F9[1, 1] = array[1, 1];
				expr_F9[1, 2] = array[1, 2];
				volume = new MeshSimplification.SimplificationWedge(number, expr_F9);
				return true;
			}
			volume = null;
			return false;
		}

		public int m_base;

		protected int[] m_axes;

		protected int[] m_signs;

		private float m_uborder;

		private float m_vborder;

		private float m_usize;

		private float m_vsize;

		private float m_uvsize;

		public static int[,] vertices = new int[,]
		{
			{
				0,
				1,
				1
			},
			{
				0,
				0,
				1
			},
			{
				1,
				0,
				1
			},
			{
				1,
				1,
				1
			},
			{
				1,
				1,
				0
			},
			{
				1,
				0,
				0
			},
			{
				0,
				0,
				0
			},
			{
				0,
				1,
				0
			}
		};

		public static int[,] orientation = new int[,]
		{
			{
				4,
				7,
				6,
				5,
				1,
				2
			},
			{
				0,
				1,
				6,
				7,
				5,
				4
			},
			{
				7,
				0,
				1,
				6,
				2,
				5
			},
			{
				7,
				4,
				3,
				0,
				2,
				1
			},
			{
				3,
				2,
				1,
				0,
				6,
				7
			},
			{
				4,
				3,
				0,
				7,
				1,
				6
			},
			{
				0,
				3,
				2,
				1,
				5,
				6
			},
			{
				1,
				0,
				3,
				2,
				4,
				5
			},
			{
				3,
				4,
				5,
				2,
				6,
				1
			},
			{
				3,
				0,
				7,
				4,
				6,
				5
			},
			{
				2,
				3,
				4,
				5,
				7,
				6
			},
			{
				0,
				7,
				4,
				3,
				5,
				2
			}
		};

		public static int[,] candidateLookup = new int[,]
		{
			{
				2,
				3,
				1
			},
			{
				1,
				3,
				2
			},
			{
				1,
				2,
				3
			},
			{
				-2,
				-3,
				1
			},
			{
				1,
				-3,
				2
			},
			{
				1,
				-2,
				3
			},
			{
				2,
				-3,
				1
			},
			{
				-1,
				-3,
				2
			},
			{
				-1,
				2,
				3
			},
			{
				-2,
				3,
				1
			},
			{
				-1,
				3,
				2
			},
			{
				-1,
				-2,
				3
			}
		};
	}

	protected class Grid3D
	{
		public Grid3D(int resolution, Vector3 mins, Vector3 maxes, bool useWedges)
		{
			this.m_onlyCubes = !useWedges;
			this.m_resolution = resolution;
			this.m_mins = mins;
			this.m_size = maxes - mins;
			this.ResetCandidates();
			this.Data = new MeshSimplification.CellStatus[this.m_resolution, this.m_resolution, this.m_resolution];
			for (int i = 0; i < this.m_resolution; i++)
			{
				for (int j = 0; j < this.m_resolution; j++)
				{
					for (int k = 0; k < this.m_resolution; k++)
					{
						this.Data[i, j, k] = MeshSimplification.CellStatus.INSIDE;
					}
				}
			}
			this.m_processed = new MeshSimplification.CellStatus[this.m_resolution, this.m_resolution, this.m_resolution];
		}

		public float GetMaxCellSize()
		{
			return Mathf.Max(Mathf.Max(this.m_size.x, this.m_size.y), this.m_size.z) / (float)this.m_resolution;
		}

		public void SetData(int[] loc, MeshSimplification.CellStatus value)
		{
			this.Data[loc[0], loc[1], loc[2]] = value;
		}

		public void SetData(Vector3 pos, MeshSimplification.CellStatus value)
		{
			int[] array = this.ToGrid(pos);
			this.Data[array[0], array[1], array[2]] = value;
		}

		public Vector3 CenterPos(int loc0, int loc1, int loc2)
		{
			return new Vector3(this.m_mins.get_Item(0) + ((float)loc0 + 0.5f) * this.m_size.x / (float)this.m_resolution, this.m_mins.get_Item(1) + ((float)loc1 + 0.5f) * this.m_size.y / (float)this.m_resolution, this.m_mins.get_Item(2) + ((float)loc2 + 0.5f) * this.m_size.z / (float)this.m_resolution);
		}

		public Vector3 GetPos(Vector3 gridCoords)
		{
			return new Vector3(this.m_mins.get_Item(0) + (gridCoords.x + 0.5f) * this.m_size.x / (float)this.m_resolution, this.m_mins.get_Item(1) + (gridCoords.y + 0.5f) * this.m_size.y / (float)this.m_resolution, this.m_mins.get_Item(2) + (gridCoords.z + 0.5f) * this.m_size.z / (float)this.m_resolution);
		}

		public Vector3 OuterPos(int[,] volume, int x, int y, int z)
		{
			return new Vector3(this.m_mins.get_Item(0) + (float)(volume[x, 0] + x) * this.m_size.x / (float)this.m_resolution, this.m_mins.get_Item(1) + (float)(volume[y, 1] + y) * this.m_size.y / (float)this.m_resolution, this.m_mins.get_Item(2) + (float)(volume[z, 2] + z) * this.m_size.z / (float)this.m_resolution);
		}

		public float ToGrid(float pos, int dim)
		{
			return (float)this.m_resolution * (pos - this.m_mins.get_Item(dim)) / this.m_size.get_Item(dim) - 0.5f;
		}

		public int[] ToGrid(Vector3 pos)
		{
			return new int[]
			{
				Mathf.FloorToInt((float)this.m_resolution * (pos.x - this.m_mins.get_Item(0)) / this.m_size.get_Item(0)),
				Mathf.FloorToInt((float)this.m_resolution * (pos.y - this.m_mins.get_Item(1)) / this.m_size.get_Item(1)),
				Mathf.FloorToInt((float)this.m_resolution * (pos.z - this.m_mins.get_Item(2)) / this.m_size.get_Item(2))
			};
		}

		private bool CheckCandidate(MeshSimplification.SimplificationVolume volume)
		{
			if (this.Clamp(volume))
			{
				return false;
			}
			foreach (int[] current in volume.Cells())
			{
				if (this.Data[current[0], current[1], current[2]] == MeshSimplification.CellStatus.OUTSIDE)
				{
					return false;
				}
			}
			return true;
		}

		public void ResetCandidates()
		{
			this.m_candidateRoot = new int[3];
			this.m_candidateCounter = new int[2];
		}

		[DebuggerHidden]
		public IEnumerable<MeshSimplification.SimplificationVolume> FindNextCandidate()
		{
			MeshSimplification.Grid3D.<FindNextCandidate>c__Iterator0 <FindNextCandidate>c__Iterator = new MeshSimplification.Grid3D.<FindNextCandidate>c__Iterator0();
			<FindNextCandidate>c__Iterator.$this = this;
			MeshSimplification.Grid3D.<FindNextCandidate>c__Iterator0 expr_0E = <FindNextCandidate>c__Iterator;
			expr_0E.$PC = -2;
			return expr_0E;
		}

		public bool Find(MeshSimplification.CellStatus value, out int[] loc)
		{
			for (int i = 0; i < this.m_resolution; i++)
			{
				for (int j = 0; j < this.m_resolution; j++)
				{
					for (int k = 0; k < this.m_resolution; k++)
					{
						if (this.Data[i, j, k] == value)
						{
							loc = new int[]
							{
								i,
								j,
								k
							};
							return true;
						}
					}
				}
			}
			loc = new int[]
			{
				-1,
				-1,
				-1
			};
			return false;
		}

		public int ScoreFunc(int[] pos)
		{
			MeshSimplification.CellStatus cellStatus = this.Data[pos[0], pos[1], pos[2]];
			return (int)((cellStatus != MeshSimplification.CellStatus.PROCESSED) ? ((MeshSimplification.CellStatus)600 * cellStatus + 10) : MeshSimplification.CellStatus.OUTSIDE);
		}

		public int ScoreFuncWedge(int[] pos)
		{
			MeshSimplification.CellStatus cellStatus = this.Data[pos[0], pos[1], pos[2]];
			return (int)((cellStatus != MeshSimplification.CellStatus.PROCESSED) ? ((MeshSimplification.CellStatus)40 * cellStatus + 10) : MeshSimplification.CellStatus.OUTSIDE);
		}

		public int ScoreDelta(MeshSimplification.SimplificationVolume vol1, MeshSimplification.SimplificationVolume vol2)
		{
			MeshSimplification.ScoreCounter c = new MeshSimplification.ScoreCounter(this.ScoreFunc);
			if (vol1 is MeshSimplification.SimplificationWedge)
			{
				c = new MeshSimplification.ScoreCounter(this.ScoreFuncWedge);
			}
			return vol1.ScoreDelta(vol2, c);
		}

		public int Score(MeshSimplification.SimplificationVolume volume)
		{
			MeshSimplification.ScoreCounter scoreCounter = new MeshSimplification.ScoreCounter(this.ScoreFunc);
			if (volume is MeshSimplification.SimplificationWedge)
			{
				scoreCounter = new MeshSimplification.ScoreCounter(this.ScoreFuncWedge);
			}
			int num = 0;
			foreach (int[] current in volume.Cells())
			{
				num += scoreCounter(current);
			}
			return num;
		}

		public int CountInside(MeshSimplification.SimplificationVolume volume)
		{
			int num = 0;
			foreach (int[] current in volume.Cells())
			{
				if (this.Data[current[0], current[1], current[2]] >= MeshSimplification.CellStatus.INSIDE)
				{
					num++;
				}
			}
			return num;
		}

		public bool Clamp(MeshSimplification.SimplificationVolume volume)
		{
			return volume.Clamp(0, this.m_resolution - 1);
		}

		public int BestExpansion(MeshSimplification.SimplificationVolume volume, out MeshSimplification.SimplificationVolume result)
		{
			MeshSimplification.SimplificationVolume simplificationVolume = volume;
			int num;
			bool flag;
			do
			{
				num = this.Score(volume);
				flag = false;
				this.m_shuffler++;
				for (int i = 0; i < volume.GetExpansions(); i++)
				{
					MeshSimplification.SimplificationVolume expansion = volume.GetExpansion((i + this.m_shuffler) % volume.GetExpansions());
					if (!this.Clamp(expansion))
					{
						int num2 = this.ScoreDelta(expansion, volume);
						if ((volume is MeshSimplification.SimplificationCube && num2 > 0) || (volume is MeshSimplification.SimplificationWedge && num2 >= 0))
						{
							num += num2;
							simplificationVolume = expansion;
							flag = true;
							break;
						}
					}
				}
				volume = simplificationVolume;
			}
			while (flag);
			this.Mark(this.m_processed, simplificationVolume, MeshSimplification.CellStatus.PROCESSED);
			result = simplificationVolume;
			return num;
		}

		public void Mark(MeshSimplification.CellStatus[,,] which, MeshSimplification.SimplificationVolume volume, MeshSimplification.CellStatus value)
		{
			foreach (int[] current in volume.Cells())
			{
				which[current[0], current[1], current[2]] = value;
			}
		}

		private const int VOLUME_TYPES = 2;

		private readonly int[] VOLUME_CANDIDATES = new int[]
		{
			1,
			12
		};

		public int m_resolution;

		public Vector3 m_mins;

		public Vector3 m_size;

		public int[] m_candidateRoot;

		public int[] m_candidateCounter;

		public MeshSimplification.CellStatus[,,] Data;

		public MeshSimplification.CellStatus[,,] m_processed;

		public bool m_onlyCubes;

		public int m_shuffler;
	}

	protected class QuadTree
	{
		public QuadTree()
		{
			this.data = false;
			this.min = new Vector2(0f, 0f);
			this.max = new Vector2(1f, 1f);
			this.space = Math.Min(this.max.x - this.min.x, this.max.y - this.min.y);
			this.children = new MeshSimplification.QuadTree[4];
		}

		public QuadTree(Vector2 min, Vector2 max)
		{
			this.min = min;
			this.max = max;
			this.data = false;
			this.space = Math.Min(max.x - min.x, max.y - min.y);
			this.children = new MeshSimplification.QuadTree[4];
		}

		public void Split(Vector2 pos)
		{
			this.children[0] = new MeshSimplification.QuadTree(new Vector2(this.min.x, this.min.y), new Vector2(pos.x, pos.y));
			this.children[1] = new MeshSimplification.QuadTree(new Vector2(pos.x, this.min.y), new Vector2(this.max.x, pos.y));
			this.children[2] = new MeshSimplification.QuadTree(new Vector2(this.min.x, pos.y), new Vector2(pos.x, this.max.y));
			this.children[3] = new MeshSimplification.QuadTree(new Vector2(pos.x, pos.y), new Vector2(this.max.x, this.max.y));
			this.data = true;
		}

		public MeshSimplification.QuadTree AllocatePos(float spaceReq)
		{
			if (this.data)
			{
				for (int i = 0; i < 4; i++)
				{
					MeshSimplification.QuadTree quadTree = this.children[i].AllocatePos(spaceReq);
					if (quadTree != null)
					{
						return quadTree;
					}
				}
				return null;
			}
			if (this.space >= spaceReq)
			{
				this.Split(new Vector2(this.min.x + spaceReq, this.min.y + spaceReq));
				this.children[0].space = 0f;
				return this.children[0];
			}
			return null;
		}

		private MeshSimplification.QuadTree[] children;

		public Vector2 min;

		public Vector2 max;

		public bool data;

		public float space;
	}
}
