﻿using System;

[Serializable]
public struct FixedBounds3D
{
	public FixedBounds3D(bool reset)
	{
		this.minX = 2000000000;
		this.minY = 2000000000;
		this.minZ = 2000000000;
		this.maxX = -2000000000;
		this.maxY = -2000000000;
		this.maxZ = -2000000000;
	}

	public void AddPoint(FixedVector3D pos)
	{
		if (pos.x < this.minX)
		{
			this.minX = pos.x;
		}
		if (pos.y < this.minY)
		{
			this.minY = pos.y;
		}
		if (pos.z < this.minZ)
		{
			this.minZ = pos.z;
		}
		if (pos.x > this.maxX)
		{
			this.maxX = pos.x;
		}
		if (pos.y > this.maxY)
		{
			this.maxY = pos.y;
		}
		if (pos.z > this.maxZ)
		{
			this.maxZ = pos.z;
		}
	}

	public void AddPoint(int x, int y, int z)
	{
		if (x < this.minX)
		{
			this.minX = x;
		}
		if (y < this.minY)
		{
			this.minY = y;
		}
		if (z < this.minZ)
		{
			this.minZ = z;
		}
		if (x > this.maxX)
		{
			this.maxX = x;
		}
		if (y > this.maxY)
		{
			this.maxY = y;
		}
		if (z > this.maxZ)
		{
			this.maxZ = z;
		}
	}

	public void AddBounds(FixedBounds3D bounds)
	{
		if (bounds.minX < this.minX)
		{
			this.minX = bounds.minX;
		}
		if (bounds.minY < this.minY)
		{
			this.minY = bounds.minY;
		}
		if (bounds.minZ < this.minZ)
		{
			this.minZ = bounds.minZ;
		}
		if (bounds.maxX > this.maxX)
		{
			this.maxX = bounds.maxX;
		}
		if (bounds.maxY > this.maxY)
		{
			this.maxY = bounds.maxY;
		}
		if (bounds.maxZ > this.maxZ)
		{
			this.maxZ = bounds.maxZ;
		}
	}

	public bool Overlap(FixedBounds3D bounds)
	{
		return this.minX <= bounds.maxX && this.minY <= bounds.maxY && this.minZ <= bounds.maxZ && this.maxX >= bounds.minX && this.maxY >= bounds.minY && this.maxZ >= bounds.minZ;
	}

	public FixedVector3D GetCenter()
	{
		FixedVector3D result;
		result.x = this.minX + this.maxX >> 1;
		result.y = this.minY + this.maxY >> 1;
		result.z = this.minZ + this.maxZ >> 1;
		return result;
	}

	public int minX;

	public int minY;

	public int minZ;

	public int maxX;

	public int maxY;

	public int maxZ;
}
