﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class GarbageInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_LandfillCapacity = base.Find<UILabel>("LandfillCapacity");
		this.m_LandfillStorage = base.Find<UILabel>("LandfillStorage");
		this.m_IncineratorCapacity = base.Find<UILabel>("IncineratorCapacity");
		this.m_GarbageProduction = base.Find<UILabel>("GarbageProduction");
		this.m_LandfillUsage = base.Find<UILabel>("LandfillUsage");
		this.m_LandfillMeter = base.Find<UISlider>("LandfillMeter");
		this.m_IncineratorMeter = base.Find<UISlider>("IncineratorMeter");
	}

	private void SetSpriteColor(UISlicedSprite sprite, Color col)
	{
		col.a = sprite.get_opacity();
		sprite.set_color(col);
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UISlicedSprite sprite = base.Find<UISlicedSprite>("LandfillSprite");
				UISlicedSprite sprite2 = base.Find<UISlicedSprite>("IncineratorSprite");
				UISprite uISprite = base.Find<UISprite>("ColorActive");
				UISprite uISprite2 = base.Find<UISprite>("ColorInactive");
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("RateGradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("CoverageGradient");
				UITextureSprite uITextureSprite3 = base.Find<UITextureSprite>("LandfillGradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uISprite.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_activeColor);
					uISprite2.set_color(Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_inactiveColor);
					this.SetSpriteColor(sprite, uISprite.get_color());
					this.SetSpriteColor(sprite2, uISprite.get_color());
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_targetColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_negativeColor);
					uITextureSprite3.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_targetColor);
					uITextureSprite3.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[16].m_negativeColor);
				}
				if (Singleton<CoverageManager>.get_exists())
				{
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<CoverageManager>.get_instance().m_properties.m_badCoverage);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", Singleton<CoverageManager>.get_instance().m_properties.m_goodCoverage);
				}
			}
			this.m_initialized = true;
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		if (Singleton<DistrictManager>.get_exists())
		{
			num = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetGarbageCapacity();
			num2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetGarbageAmount();
			num3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetIncinerationCapacity();
			num4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetGarbageAccumulation();
		}
		if (num > 0)
		{
			this.m_LandfillMeter.set_value((float)num2 / (float)num * 100f);
		}
		else
		{
			this.m_LandfillMeter.set_value(0f);
		}
		this.m_IncineratorMeter.set_value((float)base.GetPercentage(num3, num4));
		this.m_LandfillUsage.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_GARBAGE_LANDFILL"),
			" - ",
			this.m_LandfillMeter.get_value(),
			"%"
		}));
		this.m_LandfillCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_GARBAGE_LANDFILLCAPACITY"), num));
		this.m_LandfillStorage.set_text(StringUtils.SafeFormat(Locale.Get("INFO_GARBAGE_LANDFILLSTORAGE"), num2));
		this.m_IncineratorCapacity.set_text(StringUtils.SafeFormat(Locale.Get("INFO_GARBAGE_INCINERATORCAPACITY"), num3));
		this.m_GarbageProduction.set_text(StringUtils.SafeFormat(Locale.Get("INFO_GARBAGE_GARBAGEPRODUCTION"), num4));
	}

	private UILabel m_LandfillUsage;

	private UISlider m_LandfillMeter;

	private UISlider m_IncineratorMeter;

	private UILabel m_LandfillCapacity;

	private UILabel m_LandfillStorage;

	private UILabel m_IncineratorCapacity;

	private UILabel m_GarbageProduction;

	private bool m_initialized;
}
