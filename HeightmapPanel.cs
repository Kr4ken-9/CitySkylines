﻿using System;
using System.IO;
using ColossalFramework.IO;

public abstract class HeightmapPanel : ToolsModifierControl
{
	protected void Sort(FileInfo[] files, HeightmapPanel.SortingType type)
	{
		if (type == HeightmapPanel.SortingType.Timestamp)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => b.LastWriteTime.CompareTo(a.LastWriteTime));
		}
		else if (type == HeightmapPanel.SortingType.Name)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => a.Name.CompareTo(b.Name));
		}
		else if (type == HeightmapPanel.SortingType.Extension)
		{
			Array.Sort<FileInfo>(files, (FileInfo a, FileInfo b) => a.Extension.CompareTo(b.Extension));
		}
	}

	protected string VerifyExtension(string fileName, string desiredExtension)
	{
		string extension = Path.GetExtension(fileName);
		if (string.IsNullOrEmpty(extension))
		{
			fileName += desiredExtension;
		}
		else if (extension != desiredExtension)
		{
			fileName = Path.ChangeExtension(fileName, desiredExtension);
		}
		return fileName;
	}

	public static string heightMapsPath
	{
		get
		{
			if (HeightmapPanel.m_HeightmapsPath == null)
			{
				HeightmapPanel.m_HeightmapsPath = Path.Combine(Path.Combine(DataLocation.get_addonsPath(), "MapEditor"), "Heightmaps");
			}
			if (!Directory.Exists(HeightmapPanel.m_HeightmapsPath))
			{
				Directory.CreateDirectory(HeightmapPanel.m_HeightmapsPath);
			}
			return HeightmapPanel.m_HeightmapsPath;
		}
	}

	private static string m_HeightmapsPath;

	protected enum SortingType
	{
		Name,
		Timestamp,
		Extension
	}
}
