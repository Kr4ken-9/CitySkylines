﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.UI;

public class NewScenarioPanel : ToolsModifierControl
{
	public bool updateScenario
	{
		get
		{
			return this.m_updateScenario;
		}
		set
		{
			this.m_updateScenario = value;
			this.m_title.set_text((!value) ? Locale.Get("NEWSCENARIO_TITLE") : Locale.Get("NEWSCENARIO_UPDATEMAPORSAVE"));
		}
	}

	private void Awake()
	{
		this.m_Tabstrip = base.Find<UITabstrip>("Tabstrip");
		this.m_Tabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTabIndexChanged));
		this.m_title = base.Find<UILabel>("Label");
	}

	private void OnTabIndexChanged(UIComponent comp, int sel)
	{
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			MainMenu.SetFocus();
		}
	}

	public void OnClosed()
	{
		this.updateScenario = false;
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	public void OnLoadMap()
	{
		if (this.m_loadMapPanel.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.LoadFromMap();
				}
			});
		}
		else
		{
			this.LoadFromMap();
		}
	}

	private void LoadFromMap()
	{
		base.CloseEverything();
		UIView.get_library().Hide("NewScenarioPanel");
		UIView.get_library().Hide("PauseMenu");
		Package.Asset selectedAsset = this.m_loadMapPanel.selectedAsset;
		SimulationMetaData simulationMetaData = new SimulationMetaData();
		simulationMetaData.m_invertTraffic = ((!base.Find<UICheckBox>("InvertTraffic").get_isChecked()) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True);
		if (this.updateScenario)
		{
			simulationMetaData.m_updateMode = SimulationManager.UpdateMode.UpdateScenarioFromMap;
			simulationMetaData.m_CityName = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			simulationMetaData.m_modOverride = Singleton<SimulationManager>.get_instance().m_metaData.m_modOverride;
			simulationMetaData.m_gameInstanceIdentifier = Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier;
		}
		else
		{
			simulationMetaData.m_updateMode = SimulationManager.UpdateMode.NewScenarioFromMap;
			simulationMetaData.m_CityName = this.m_loadMapPanel.selectedCityName;
			SaveScenarioPanel.m_LastScenarioName = "New Scenario";
			SaveScenarioPanel.m_LastScenarioDescription = string.Empty;
			SaveScenarioPanel.m_LastSaveName = "NewScenario";
		}
		string selectedOverrideThemeRef = this.m_loadMapPanel.selectedOverrideThemeRef;
		if (selectedOverrideThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(selectedOverrideThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		this.updateScenario = false;
		Singleton<LoadingManager>.get_instance().LoadLevel(selectedAsset, "ScenarioEditor", "InScenarioEditor", simulationMetaData);
	}

	public void OnLoadSaveGame()
	{
		if (this.m_loadPanel.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.LoadFromSaveGame();
				}
			});
		}
		else
		{
			this.LoadFromSaveGame();
		}
	}

	private void LoadFromSaveGame()
	{
		base.CloseEverything();
		UIView.get_library().Hide("NewScenarioPanel");
		UIView.get_library().Hide("PauseMenu");
		Package.Asset selectedAsset = this.m_loadPanel.selectedAsset;
		SimulationMetaData simulationMetaData = new SimulationMetaData();
		if (this.updateScenario)
		{
			simulationMetaData.m_updateMode = SimulationManager.UpdateMode.UpdateScenarioFromGame;
			simulationMetaData.m_CityName = Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
			simulationMetaData.m_modOverride = Singleton<SimulationManager>.get_instance().m_metaData.m_modOverride;
			simulationMetaData.m_gameInstanceIdentifier = Singleton<SimulationManager>.get_instance().m_metaData.m_gameInstanceIdentifier;
		}
		else
		{
			simulationMetaData.m_updateMode = SimulationManager.UpdateMode.NewScenarioFromGame;
			simulationMetaData.m_CityName = this.m_loadPanel.selectedCityName;
			SaveScenarioPanel.m_LastScenarioName = "New Scenario";
			SaveScenarioPanel.m_LastScenarioDescription = string.Empty;
			SaveScenarioPanel.m_LastSaveName = "NewScenario";
		}
		string selectedOverrideThemeRef = this.m_loadPanel.selectedOverrideThemeRef;
		if (selectedOverrideThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(selectedOverrideThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		this.updateScenario = false;
		Singleton<LoadingManager>.get_instance().LoadLevel(selectedAsset, "ScenarioEditor", "InScenarioEditor", simulationMetaData);
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
		}
	}

	public LoadPanel m_loadPanel;

	public LoadMapPanel m_loadMapPanel;

	private UITabstrip m_Tabstrip;

	private UILabel m_title;

	private bool m_updateScenario;
}
