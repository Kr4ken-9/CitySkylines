﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class MonumentAI : PlayerBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_info.m_subBuildings != null)
		{
			int num = 2000000001;
			Vector3 vector = Vector3.get_zero();
			Vector3 vector2 = Vector3.get_zero();
			for (int i = 0; i < this.m_info.m_subBuildings.Length; i++)
			{
				BuildingInfo buildingInfo = this.m_info.m_subBuildings[i].m_buildingInfo;
				if (buildingInfo != null)
				{
					DummyBuildingAI component = buildingInfo.GetComponent<DummyBuildingAI>();
					if (component != null)
					{
						int num2 = (component.m_dummyID != 0) ? component.m_dummyID : 2000000000;
						if (num2 < num)
						{
							Vector3 position = this.m_info.m_subBuildings[i].m_position;
							position.z = -position.z;
							if (num2 == num)
							{
								vector = Vector3.Min(vector, position);
								vector2 = Vector3.Max(vector2, position);
							}
							else
							{
								vector = position;
								vector2 = position;
								num = num2;
							}
						}
					}
				}
			}
			this.m_dummyPosition = (vector + vector2) * 0.5f;
		}
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_entertainmentAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.Entertainment;
			radius1 = this.m_entertainmentRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		if (this.m_noiseAccumulation != 0)
		{
			resource2 = ImmaterialResourceManager.Resource.NoisePollution;
			radius2 = this.m_noiseRadius;
		}
		else
		{
			resource2 = ImmaterialResourceManager.Resource.None;
			radius2 = 0f;
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Entertainment)
		{
			if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.WaterPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			if (infoMode != InfoManager.InfoMode.Connections)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode != InfoManager.SubInfoMode.WindPower)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (data.m_tempExport != 0 || data.m_finalExport != 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_targetColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Entertainment;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, num * 5 / 4, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, num * 5 / 4, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, num * 5 / 4, 0);
	}

	public override void BuildingUpgraded(ushort buildingID, ref Building data)
	{
		base.BuildingUpgraded(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, num * 5 / 4, 0);
	}

	public override bool CheckUnlocking()
	{
		if (this.CheckLevelUnlock())
		{
			MilestoneInfo unlockMilestone = this.m_info.m_UnlockMilestone;
			if (unlockMilestone != null)
			{
				Singleton<UnlockManager>.get_instance().CheckMilestone(unlockMilestone, false, false);
			}
			if (Singleton<UnlockManager>.get_instance().Unlocked(unlockMilestone))
			{
				return true;
			}
		}
		return false;
	}

	private bool CheckLevelUnlock()
	{
		if (this.m_customUnlockFeature != UnlockManager.Feature.None)
		{
			return Singleton<UnlockManager>.get_instance().Unlocked(this.m_customUnlockFeature);
		}
		switch (this.m_monumentLevel)
		{
		case 1:
			return Singleton<UnlockManager>.get_instance().Unlocked(this.m_info.m_class.m_service);
		case 2:
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.MonumentLevel2);
		case 3:
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.MonumentLevel3);
		case 4:
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.MonumentLevel4);
		case 5:
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.MonumentLevel5);
		case 6:
			return Singleton<UnlockManager>.get_instance().Unlocked(UnlockManager.Feature.MonumentLevel6);
		default:
			return true;
		}
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_entertainmentAccumulation != 0)
		{
			Vector3 position;
			Quaternion quaternion;
			Vector3 vector;
			buildingData.GetTotalPosition(out position, out quaternion, out vector);
			position.y += vector.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (this.m_entertainmentAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Entertainment, (float)this.m_entertainmentAccumulation, this.m_entertainmentRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			if (this.m_entertainmentAccumulation != 0)
			{
				Vector3 position;
				Quaternion quaternion;
				Vector3 vector;
				buildingData.GetTotalPosition(out position, out quaternion, out vector);
				position.y += vector.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (this.m_entertainmentAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.Entertainment, (float)(-(float)this.m_entertainmentAccumulation), this.m_entertainmentRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.Shopping, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingB, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingC, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingD, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingE, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingF, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingG, offer);
		Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.ShoppingH, offer);
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
		visitPlaceCount += this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		base.GetVisitBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveVisitorCount, ref totalVisitorCount);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4095u) >= 3840u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_customBuffer2 = buildingData.m_customBuffer1;
			buildingData.m_tempImport = 0;
			buildingData.m_tempExport = 0;
			buildingData.m_customBuffer1 = 0;
		}
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = productionRate * this.m_entertainmentAccumulation / 100;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Entertainment, num, buildingData.m_position, this.m_entertainmentRadius);
		}
		int num2 = productionRate * this.m_attractivenessAccumulation / 100;
		if (num2 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Attractiveness, num2);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num3 = finalProductionRate * this.m_noiseAccumulation / 100;
		float noiseRadius = this.m_noiseRadius;
		buildingData.m_tempExport = (byte)Mathf.Clamp(behaviour.m_touristCount * 255 / Mathf.Max(1, aliveVisitorCount), (int)buildingData.m_tempExport, 255);
		buildingData.m_customBuffer1 = (ushort)Mathf.Clamp(aliveVisitorCount, (int)buildingData.m_customBuffer1, 65535);
		if (buildingData.m_finalExport != 0)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			District[] expr_104_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_104_cp_0_cp_1 = district;
			expr_104_cp_0_cp_0[(int)expr_104_cp_0_cp_1].m_playerConsumption.m_tempExportAmount = expr_104_cp_0_cp_0[(int)expr_104_cp_0_cp_1].m_playerConsumption.m_tempExportAmount + (uint)buildingData.m_finalExport;
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount + totalVisitorCount);
		int num4 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount0 + 99) / 100, this.m_visitPlaceCount0 * 5 / 4);
		int num5 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount1 + 99) / 100, this.m_visitPlaceCount1 * 5 / 4);
		int num6 = Mathf.Min((finalProductionRate * this.m_visitPlaceCount2 + 99) / 100, this.m_visitPlaceCount2 * 5 / 4);
		if (buildingData.m_eventIndex != 0)
		{
			EventManager instance2 = Singleton<EventManager>.get_instance();
			EventInfo info = instance2.m_events.m_buffer[(int)buildingData.m_eventIndex].Info;
			info.m_eventAI.ModifyNoise(buildingData.m_eventIndex, ref instance2.m_events.m_buffer[(int)buildingData.m_eventIndex], ref num3, ref noiseRadius);
			num4 = info.m_eventAI.GetCapacity(buildingData.m_eventIndex, ref instance2.m_events.m_buffer[(int)buildingData.m_eventIndex], num4, this.m_visitPlaceCount0 * 5 / 4);
			num5 = info.m_eventAI.GetCapacity(buildingData.m_eventIndex, ref instance2.m_events.m_buffer[(int)buildingData.m_eventIndex], num5, this.m_visitPlaceCount1 * 5 / 4);
			num6 = info.m_eventAI.GetCapacity(buildingData.m_eventIndex, ref instance2.m_events.m_buffer[(int)buildingData.m_eventIndex], num6, this.m_visitPlaceCount2 * 5 / 4);
			int num7 = Mathf.Max(1, num4 + num5 + num6);
			int num8 = Mathf.Max(0, num7 - totalVisitorCount);
			if (num8 != 0 && (instance2.m_events.m_buffer[(int)buildingData.m_eventIndex].m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = Mathf.Max(1, info.m_eventAI.GetEventPriority(num8, num7));
				offer.Building = buildingID;
				offer.Position = buildingData.m_position;
				offer.Amount = Mathf.Max(1, num8 / 12);
				offer.Active = false;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Entertainment, offer);
				if (num8 >= 2)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer);
				}
				if (num8 >= 3)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer);
				}
				if (num8 >= 4)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer);
				}
				if (num8 >= 5)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Shopping, offer);
				}
				if (num8 >= 6)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingB, offer);
				}
				if (num8 >= 7)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingC, offer);
				}
				if (num8 >= 8)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingD, offer);
				}
				if (num8 >= 9)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingE, offer);
				}
				if (num8 >= 10)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingF, offer);
				}
				if (num8 >= 11)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingG, offer);
				}
				if (num8 >= 12)
				{
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.ShoppingH, offer);
				}
			}
		}
		else
		{
			int num9 = Mathf.Max(1, num4 + num5 + num6);
			int num10 = Mathf.Max(0, num9 - totalVisitorCount);
			int num11 = Mathf.Max(Mathf.Max(num4, num5), Mathf.Max(1, num6));
			int num12 = num10 * num4 / num9;
			int num13 = num10 * num5 / num9;
			int num14 = num10 * num6 / num9;
			if (num12 + num13 + num14 > 0)
			{
				int num15 = Mathf.Max(Mathf.Max(num12, num13), Mathf.Max(1, num14));
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Priority = Mathf.Max(1, num15 * 8 / num11);
				offer2.Building = buildingID;
				offer2.Position = buildingData.m_position;
				offer2.Amount = num12 + num13 + num14;
				offer2.Active = false;
				switch (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(4u))
				{
				case 0:
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.Entertainment, offer2);
					break;
				case 1:
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentB, offer2);
					break;
				case 2:
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentC, offer2);
					break;
				case 3:
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.EntertainmentD, offer2);
					break;
				}
			}
		}
		if (num3 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num3, buildingData.m_position, noiseRadius);
		}
		MonumentAI.AdditionalEffect additionalEffect = this.m_additionalEffect;
		if (additionalEffect != MonumentAI.AdditionalEffect.AdCampaign)
		{
			if (additionalEffect != MonumentAI.AdditionalEffect.ConcertTicketSale)
			{
				if (additionalEffect == MonumentAI.AdditionalEffect.ConcertSuccess)
				{
					Singleton<EventManager>.get_instance().m_concertSuccessModifierTemp += (this.m_additionalEffectFactor * finalProductionRate + 99) / 100;
				}
			}
			else
			{
				Singleton<EventManager>.get_instance().m_concertTicketSaleModifierTemp += (this.m_additionalEffectFactor * finalProductionRate + 99) / 100;
			}
		}
		else
		{
			Singleton<EventManager>.get_instance().m_adCampaignModifierTemp += (this.m_additionalEffectFactor * finalProductionRate + 99) / 100;
		}
	}

	protected override bool CanEvacuate()
	{
		return this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_visitPlaceCount0 != 0 || this.m_visitPlaceCount1 != 0 || this.m_visitPlaceCount2 != 0;
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		Vector3 vector;
		Vector3 vector2;
		bool flag;
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline && BuildingTool.SnapToCanal(position, out vector, out vector2, out flag, 40f, false))
		{
			angle = Mathf.Atan2(vector2.x, -vector2.z);
			vector += vector2 * (this.m_info.m_generatedInfo.m_max.z - 7f);
			position.x = vector.x;
			position.z = vector.z;
			if (!flag)
			{
				toolErrors |= ToolBase.ToolErrors.ShoreNotFound;
			}
		}
		toolErrors |= base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		if (relocateID == 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num = serviceBuildings.m_buffer[i];
				if (num != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
					if (info == this.m_info)
					{
						toolErrors |= ToolBase.ToolErrors.AlreadyExists;
					}
				}
			}
		}
		return toolErrors;
	}

	public override bool GetWaterStructureCollisionRange(out float min, out float max)
	{
		if (this.m_info.m_placementMode == BuildingInfo.PlacementMode.Shoreline)
		{
			min = 12f / Mathf.Max(14f, (float)this.m_info.m_cellLength * 8f);
			max = 1f;
			return true;
		}
		return base.GetWaterStructureCollisionRange(out min, out max);
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}

	public override string GetLocalizedTooltip()
	{
		int num = this.m_visitPlaceCount0 + this.m_visitPlaceCount1 + this.m_visitPlaceCount2;
		if (this.m_supportEvents != (EventManager.EventType)0)
		{
			num = num * 5 / 4;
		}
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_VISITOR_CAPACITY", new object[]
			{
				num
			})
		}));
	}

	public override Vector2 GetInterestDirection(ushort buildingID, ref Building data, Vector3 pos)
	{
		return VectorUtils.XZ(data.CalculatePosition(this.m_dummyPosition) - pos).get_normalized();
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num = (int)(((ushort)data.m_finalExport * data.m_customBuffer2 + 254) / 255);
		int customBuffer = (int)data.m_customBuffer2;
		string str = LocaleFormatter.FormatGeneric("AIINFO_VISITORS", new object[]
		{
			customBuffer
		});
		str += Environment.NewLine;
		return str + LocaleFormatter.FormatGeneric("AIINFO_TOURISTS", new object[]
		{
			num
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess() || this.m_workPlaceCount0 != 0 || this.m_workPlaceCount1 != 0 || this.m_workPlaceCount2 != 0 || this.m_workPlaceCount3 != 0 || this.m_visitPlaceCount0 != 0 || this.m_visitPlaceCount1 != 0 || this.m_visitPlaceCount2 != 0;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 10;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 10;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 10;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 10;

	[CustomizableProperty("Low Wealth Tourists", "Tourists", 0)]
	public int m_visitPlaceCount0 = 100;

	[CustomizableProperty("Medium Wealth Tourists", "Tourists", 1)]
	public int m_visitPlaceCount1 = 100;

	[CustomizableProperty("High Wealth Tourists", "Tourists", 2)]
	public int m_visitPlaceCount2 = 100;

	[CustomizableProperty("Entertainment Accumulation")]
	public int m_entertainmentAccumulation = 100;

	[CustomizableProperty("Entertainment Radius")]
	public float m_entertainmentRadius = 400f;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation = 100;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 100f;

	[CustomizableProperty("Attractiveness Accumulation")]
	public int m_attractivenessAccumulation = 10;

	[CustomizableProperty("Monument Level")]
	public int m_monumentLevel = 1;

	public UnlockManager.Feature m_customUnlockFeature = UnlockManager.Feature.None;

	public MonumentAI.AdditionalEffect m_additionalEffect;

	public int m_additionalEffectFactor;

	[NonSerialized]
	private Vector3 m_dummyPosition;

	public enum AdditionalEffect
	{
		None,
		AdCampaign,
		ConcertTicketSale,
		ConcertSuccess
	}
}
