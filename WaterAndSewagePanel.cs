﻿using System;
using ColossalFramework.UI;

public sealed class WaterAndSewagePanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Water;
		}
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		base.PopulateAssets((GeneratedScrollPanel.AssetFilter)3);
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		object objectUserData = comp.get_objectUserData();
		NetInfo netInfo = objectUserData as NetInfo;
		BuildingInfo buildingInfo = objectUserData as BuildingInfo;
		if (netInfo != null)
		{
			NetTool netTool = ToolsModifierControl.SetTool<NetTool>();
			if (netTool != null)
			{
				base.ShowPipesOptionPanel();
				netTool.Prefab = netInfo;
			}
		}
		if (buildingInfo != null)
		{
			BuildingTool buildingTool = ToolsModifierControl.SetTool<BuildingTool>();
			if (buildingTool != null)
			{
				base.HideAllOptionPanels();
				buildingTool.m_prefab = buildingInfo;
				buildingTool.m_relocate = 0;
			}
		}
	}
}
