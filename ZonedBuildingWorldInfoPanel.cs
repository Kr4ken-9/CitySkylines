﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public sealed class ZonedBuildingWorldInfoPanel : BuildingWorldInfoPanel
{
	protected override void OnSetTarget()
	{
		base.OnSetTarget();
		if (this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0 && Singleton<BuildingManager>.get_exists())
		{
			BuildingInfo info = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_InstanceID.Building].Info;
			if (base.IsResidentialZone(info.m_class.GetZone()))
			{
				this.m_ZoneTypeInfo.set_selectedIndex(0);
			}
			else
			{
				this.m_ZoneTypeInfo.set_selectedIndex(1);
			}
			string specializationName = this.GetSpecializationName(info.GetSubService());
			if (!string.IsNullOrEmpty(specializationName))
			{
				this.m_SpecializationSprite.set_isVisible(true);
				this.m_SpecializationSprite.set_spriteName("IconPolicy" + specializationName);
				this.m_SpecializationSprite.set_tooltip(Locale.Get("DISTRICT_TITLE", "Specialization" + specializationName));
			}
			else
			{
				this.m_SpecializationSprite.set_isVisible(false);
			}
		}
	}

	private Color32 MultiplyColor(Color col, float scalar)
	{
		Color color = col * scalar;
		color.a = col.a;
		return color;
	}

	protected override void Start()
	{
		base.Start();
		this.m_ZoneTypeInfo = base.Find<UITabContainer>("ZoneTypeInfo");
		this.m_Type = base.Find<UILabel>("Type");
		this.m_Status = base.Find<UILabel>("Status");
		this.m_ZoneIcon = base.Find<UISprite>("Zone");
		this.m_HappinessIcon = base.Find<UISprite>("Happiness");
		this.m_LevelProgress = base.Find<UIPanel>("LevelProgress");
		this.m_AgeChart = base.Find<UIRadialChart>("AgeChart");
		UIRadialChart.SliceSettings arg_A3_0 = this.m_AgeChart.GetSlice(0);
		Color32 color = this.m_ChildColor;
		this.m_AgeChart.GetSlice(0).set_outterColor(color);
		arg_A3_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_CE_0 = this.m_AgeChart.GetSlice(1);
		color = this.m_TeenColor;
		this.m_AgeChart.GetSlice(1).set_outterColor(color);
		arg_CE_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_F9_0 = this.m_AgeChart.GetSlice(2);
		color = this.m_YoungColor;
		this.m_AgeChart.GetSlice(2).set_outterColor(color);
		arg_F9_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_124_0 = this.m_AgeChart.GetSlice(3);
		color = this.m_AdultColor;
		this.m_AgeChart.GetSlice(3).set_outterColor(color);
		arg_124_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_14F_0 = this.m_AgeChart.GetSlice(4);
		color = this.m_SeniorColor;
		this.m_AgeChart.GetSlice(4).set_outterColor(color);
		arg_14F_0.set_innerColor(color);
		this.m_ChildLegend = base.Find<UILabel>("ChildAmount");
		this.m_TeenLegend = base.Find<UILabel>("TeenAmount");
		this.m_YoungLegend = base.Find<UILabel>("YoungAmount");
		this.m_AdultLegend = base.Find<UILabel>("AdultAmount");
		this.m_SeniorLegend = base.Find<UILabel>("SeniorAmount");
		this.m_ChildLegend.set_color(this.m_ChildColor);
		this.m_TeenLegend.set_color(this.m_TeenColor);
		this.m_YoungLegend.set_color(this.m_YoungColor);
		this.m_AdultLegend.set_color(this.m_AdultColor);
		this.m_SeniorLegend.set_color(this.m_SeniorColor);
		this.m_EducationChart = base.Find<UIRadialChart>("EducationChart");
		UIRadialChart.SliceSettings arg_235_0 = this.m_EducationChart.GetSlice(0);
		color = this.m_UneducatedColor;
		this.m_EducationChart.GetSlice(0).set_outterColor(color);
		arg_235_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_260_0 = this.m_EducationChart.GetSlice(1);
		color = this.m_EducatedColor;
		this.m_EducationChart.GetSlice(1).set_outterColor(color);
		arg_260_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_28B_0 = this.m_EducationChart.GetSlice(2);
		color = this.m_WellEducatedColor;
		this.m_EducationChart.GetSlice(2).set_outterColor(color);
		arg_28B_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_2B6_0 = this.m_EducationChart.GetSlice(3);
		color = this.m_HighlyEducatedColor;
		this.m_EducationChart.GetSlice(3).set_outterColor(color);
		arg_2B6_0.set_innerColor(color);
		this.m_UneducatedLegend = base.Find<UILabel>("UneducatedAmount");
		this.m_EducatedLegend = base.Find<UILabel>("EducatedAmount");
		this.m_WellEducatedLegend = base.Find<UILabel>("WellEducatedAmount");
		this.m_HighlyEducatedLegend = base.Find<UILabel>("HighlyEducatedAmount");
		this.m_UneducatedLegend.set_color(this.m_UneducatedColor);
		this.m_EducatedLegend.set_color(this.m_EducatedColor);
		this.m_WellEducatedLegend.set_color(this.m_WellEducatedColor);
		this.m_HighlyEducatedLegend.set_color(this.m_HighlyEducatedColor);
		for (int i = 0; i < 5; i++)
		{
			this.m_Levels[i] = base.Find<UIProgressBar>("Level" + (i + 1).ToString() + "Bar");
		}
		this.m_LevelSprite = base.Find<UISprite>("LevelSprite");
		this.m_WorkSituation = base.Find<UILabel>("WorkSituation");
		this.m_OverWorkSituation = base.Find<UILabel>("OverWorkSituation");
		this.m_UneducatedPlaces = base.Find<UILabel>("UneducatedPlaces");
		this.m_UneducatedWorkers = base.Find<UILabel>("UneducatedWorkers");
		this.m_EducatedPlaces = base.Find<UILabel>("EducatedPlaces");
		this.m_EducatedWorkers = base.Find<UILabel>("EducatedWorkers");
		this.m_WellEducatedPlaces = base.Find<UILabel>("WellEducatedPlaces");
		this.m_WellEducatedWorkers = base.Find<UILabel>("WellEducatedWorkers");
		this.m_HighlyEducatedPlaces = base.Find<UILabel>("HighlyEducatedPlaces");
		this.m_HighlyEducatedWorkers = base.Find<UILabel>("HighlyEducatedWorkers");
		this.m_JobsAvailLegend = base.Find<UILabel>("JobsAvailAmount");
		this.m_JobsAvailLegend.set_color(this.m_UnoccupiedWorkplaceColor);
		this.m_WorkPlacesEducationChart = base.Find<UIRadialChart>("WorkPlacesEducationChart");
		this.m_WorkersEducationChart = base.Find<UIRadialChart>("WorkersEducationChart");
		Color[] array = new Color[]
		{
			this.m_UneducatedColor,
			this.m_EducatedColor,
			this.m_WellEducatedColor,
			this.m_HighlyEducatedColor
		};
		for (int j = 0; j < 4; j++)
		{
			UIRadialChart.SliceSettings arg_521_0 = this.m_WorkPlacesEducationChart.GetSlice(j);
			color = array[j];
			this.m_WorkPlacesEducationChart.GetSlice(j).set_outterColor(color);
			arg_521_0.set_innerColor(color);
			UIRadialChart.SliceSettings arg_561_0 = this.m_WorkersEducationChart.GetSlice(j);
			color = this.MultiplyColor(array[j], this.m_WorkersColorScalar);
			this.m_WorkersEducationChart.GetSlice(j).set_outterColor(color);
			arg_561_0.set_innerColor(color);
		}
		UIRadialChart.SliceSettings arg_59A_0 = this.m_WorkersEducationChart.GetSlice(4);
		color = this.m_UnoccupiedWorkplaceColor;
		this.m_WorkersEducationChart.GetSlice(4).set_outterColor(color);
		arg_59A_0.set_innerColor(color);
		this.m_UneducatedPlaces.set_color(this.m_UneducatedColor);
		this.m_EducatedPlaces.set_color(this.m_EducatedColor);
		this.m_WellEducatedPlaces.set_color(this.m_WellEducatedColor);
		this.m_HighlyEducatedPlaces.set_color(this.m_HighlyEducatedColor);
		this.m_UneducatedWorkers.set_color(this.MultiplyColor(this.m_UneducatedColor, this.m_WorkersColorScalar));
		this.m_EducatedWorkers.set_color(this.MultiplyColor(this.m_EducatedColor, this.m_WorkersColorScalar));
		this.m_WellEducatedWorkers.set_color(this.MultiplyColor(this.m_WellEducatedColor, this.m_WorkersColorScalar));
		this.m_HighlyEducatedWorkers.set_color(this.MultiplyColor(this.m_HighlyEducatedColor, this.m_WorkersColorScalar));
		this.m_SpecializationSprite = base.Find<UISprite>("SpecializationPolicyIcon");
	}

	public static int GetMaxLevel(ItemClass.Zone zone)
	{
		switch (zone)
		{
		case ItemClass.Zone.ResidentialLow:
		case ItemClass.Zone.ResidentialHigh:
			return 5;
		case ItemClass.Zone.CommercialLow:
		case ItemClass.Zone.CommercialHigh:
			return 3;
		case ItemClass.Zone.Industrial:
			return 3;
		case ItemClass.Zone.Office:
			return 3;
		default:
			return 1;
		}
	}

	public static int GetMaxLevel(ItemClass klass)
	{
		switch (klass.GetZone())
		{
		case ItemClass.Zone.ResidentialLow:
		case ItemClass.Zone.ResidentialHigh:
			return 5;
		case ItemClass.Zone.CommercialLow:
		case ItemClass.Zone.CommercialHigh:
			if (klass.m_subService == ItemClass.SubService.CommercialLow || klass.m_subService == ItemClass.SubService.CommercialHigh)
			{
				return 3;
			}
			return 1;
		case ItemClass.Zone.Industrial:
			if (klass.m_subService == ItemClass.SubService.IndustrialGeneric)
			{
				return 3;
			}
			return 1;
		case ItemClass.Zone.Office:
			if (klass.m_subService == ItemClass.SubService.OfficeGeneric)
			{
				return 3;
			}
			return 1;
		default:
			return 1;
		}
	}

	private void UpdateLevels(ItemClass klass, float levelProgress, ref Building building)
	{
		float width = this.m_LevelProgress.get_width();
		int maxLevel = ZonedBuildingWorldInfoPanel.GetMaxLevel(klass);
		float width2 = width / (float)maxLevel;
		for (int i = 0; i < 5; i++)
		{
			if (i < maxLevel)
			{
				this.m_Levels[i].set_width(width2);
				this.m_Levels[i].Show();
				if ((building.m_flags & (Building.Flags.Completed | Building.Flags.Abandoned | Building.Flags.Collapsed)) == Building.Flags.Completed || (building.m_flags & Building.Flags.Upgrading) != Building.Flags.None)
				{
					if (klass.m_level >= (ItemClass.Level)i)
					{
						this.m_Levels[i].set_progressColor(this.m_LevelCompleteColor);
						this.m_Levels[i].set_value(1f);
					}
					else if (klass.m_level + 1 == (ItemClass.Level)i)
					{
						if ((building.m_flags & Building.Flags.Upgrading) != Building.Flags.None)
						{
							this.m_Levels[i].set_progressColor(Color32.Lerp(this.m_LevelCompleteColor, this.m_LevelCompleteColorBlink, 0.75f + Mathf.Cos(Time.get_time() * 3.14159274f * 1.5f) * 0.25f));
							this.m_Levels[i].set_value(1f);
						}
						else
						{
							this.m_Levels[i].set_progressColor(this.m_LevelProgressColor);
							this.m_Levels[i].set_value(levelProgress);
						}
					}
					else
					{
						this.m_Levels[i].set_value(0f);
					}
				}
				else
				{
					this.m_Levels[i].set_value(0f);
				}
			}
			else
			{
				this.m_Levels[i].Hide();
			}
		}
	}

	private void UpdateWorkers(ushort buildingID, BuildingAI ai, ref Building building)
	{
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = building.m_citizenUnits;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			int num7 = 0;
			int num8 = 0;
			int num9 = 0;
			int num10 = 0;
			int num11 = 0;
			int num12 = 0;
			PrivateBuildingAI privateBuildingAI = ai as PrivateBuildingAI;
			if (privateBuildingAI != null && (building.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) == Building.Flags.None)
			{
				privateBuildingAI.CalculateWorkplaceCount(new Randomizer((int)buildingID), building.Width, building.Length, out num5, out num6, out num7, out num8);
				num4 = num5 + num6 + num7 + num8;
			}
			while (num != 0u)
			{
				uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Work) != 0)
				{
					for (int i = 0; i < 5; i++)
					{
						uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
						if (citizen != 0u && !instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.MovingIn) == Citizen.Flags.None)
						{
							num3++;
							switch (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].EducationLevel)
							{
							case Citizen.Education.Uneducated:
								num9++;
								break;
							case Citizen.Education.OneSchool:
								num10++;
								break;
							case Citizen.Education.TwoSchools:
								num11++;
								break;
							case Citizen.Education.ThreeSchools:
								num12++;
								break;
							}
						}
					}
				}
				num = nextUnit;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.m_WorkSituation.set_text(StringUtils.SafeFormat(Locale.Get("ZONEDBUILDING_WORKERS"), new object[]
			{
				num3,
				num4
			}));
			int num13 = 0;
			int num14 = num5 - num9;
			if (num10 > num6)
			{
				num13 += Mathf.Max(0, Mathf.Min(num14, num10 - num6));
			}
			num14 += num6 - num10;
			if (num11 > num7)
			{
				num13 += Mathf.Max(0, Mathf.Min(num14, num11 - num7));
			}
			num14 += num7 - num11;
			if (num12 > num8)
			{
				num13 += Mathf.Max(0, Mathf.Min(num14, num12 - num8));
			}
			string format = Locale.Get((num13 != 1) ? "ZONEDBUILDING_OVEREDUCATEDWORKERS" : "ZONEDBUILDING_OVEREDUCATEDWORKER");
			this.m_OverWorkSituation.set_text(StringUtils.SafeFormat(format, num13));
			this.m_OverWorkSituation.set_isVisible(num13 > 0);
			this.m_UneducatedPlaces.set_text(num5.ToString());
			this.m_EducatedPlaces.set_text(num6.ToString());
			this.m_WellEducatedPlaces.set_text(num7.ToString());
			this.m_HighlyEducatedPlaces.set_text(num8.ToString());
			this.m_UneducatedWorkers.set_text(num9.ToString());
			this.m_EducatedWorkers.set_text(num10.ToString());
			this.m_WellEducatedWorkers.set_text(num11.ToString());
			this.m_HighlyEducatedWorkers.set_text(num12.ToString());
			this.m_JobsAvailLegend.set_text((num4 - (num9 + num10 + num11 + num12)).ToString());
			int num15 = ZonedBuildingWorldInfoPanel.GetValue(num5, num4);
			int value = ZonedBuildingWorldInfoPanel.GetValue(num6, num4);
			int value2 = ZonedBuildingWorldInfoPanel.GetValue(num7, num4);
			int value3 = ZonedBuildingWorldInfoPanel.GetValue(num8, num4);
			int num16 = num15 + value + value2 + value3;
			if (num16 != 0 && num16 != 100)
			{
				num15 = 100 - (value + value2 + value3);
			}
			this.m_WorkPlacesEducationChart.SetValues(new int[]
			{
				num15,
				value,
				value2,
				value3
			});
			int value4 = ZonedBuildingWorldInfoPanel.GetValue(num9, num4);
			int value5 = ZonedBuildingWorldInfoPanel.GetValue(num10, num4);
			int value6 = ZonedBuildingWorldInfoPanel.GetValue(num11, num4);
			int value7 = ZonedBuildingWorldInfoPanel.GetValue(num12, num4);
			int num17 = value4 + value5 + value6 + value7;
			int num18 = 100 - num17;
			this.m_WorkersEducationChart.SetValues(new int[]
			{
				value4,
				value5,
				value6,
				value7,
				num18
			});
		}
	}

	private void UpdateResidential(ref Building building)
	{
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			uint num = building.m_citizenUnits;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			int num7 = 0;
			int num8 = 0;
			int num9 = 0;
			int num10 = 0;
			int num11 = 0;
			int num12 = 0;
			while (num != 0u)
			{
				uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
				if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Home) != 0)
				{
					for (int i = 0; i < 5; i++)
					{
						uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
						if (citizen != 0u && !instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_flags & Citizen.Flags.MovingIn) == Citizen.Flags.None)
						{
							num12++;
							int age = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Age;
							switch (instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].EducationLevel)
							{
							case Citizen.Education.Uneducated:
								num3++;
								break;
							case Citizen.Education.OneSchool:
								num4++;
								break;
							case Citizen.Education.TwoSchools:
								num5++;
								break;
							case Citizen.Education.ThreeSchools:
								num6++;
								break;
							}
							switch (Citizen.GetAgeGroup(age))
							{
							case Citizen.AgeGroup.Child:
								num7++;
								break;
							case Citizen.AgeGroup.Teen:
								num8++;
								break;
							case Citizen.AgeGroup.Young:
								num9++;
								break;
							case Citizen.AgeGroup.Adult:
								num10++;
								break;
							case Citizen.AgeGroup.Senior:
								num11++;
								break;
							}
						}
					}
				}
				num = nextUnit;
				if (++num2 > 524288)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.m_ChildLegend.set_text(num7.ToString());
			this.m_TeenLegend.set_text(num8.ToString());
			this.m_YoungLegend.set_text(num9.ToString());
			this.m_AdultLegend.set_text(num10.ToString());
			this.m_SeniorLegend.set_text(num11.ToString());
			int value = ZonedBuildingWorldInfoPanel.GetValue(num7, num12);
			int value2 = ZonedBuildingWorldInfoPanel.GetValue(num8, num12);
			int value3 = ZonedBuildingWorldInfoPanel.GetValue(num9, num12);
			int num13 = ZonedBuildingWorldInfoPanel.GetValue(num10, num12);
			int value4 = ZonedBuildingWorldInfoPanel.GetValue(num11, num12);
			int num14 = value + value2 + value3 + num13 + value4;
			if (num14 != 0 && num14 != 100)
			{
				num13 = 100 - (value + value2 + value3 + value4);
			}
			this.m_AgeChart.SetValues(new int[]
			{
				value,
				value2,
				value3,
				num13,
				value4
			});
			this.m_UneducatedLegend.set_text(num3.ToString());
			this.m_EducatedLegend.set_text(num4.ToString());
			this.m_WellEducatedLegend.set_text(num5.ToString());
			this.m_HighlyEducatedLegend.set_text(num6.ToString());
			int num15 = ZonedBuildingWorldInfoPanel.GetValue(num3, num12);
			int value5 = ZonedBuildingWorldInfoPanel.GetValue(num4, num12);
			int value6 = ZonedBuildingWorldInfoPanel.GetValue(num5, num12);
			int value7 = ZonedBuildingWorldInfoPanel.GetValue(num6, num12);
			int num16 = num15 + value5 + value6 + value7;
			if (num16 != 0 && num16 != 100)
			{
				num15 = 100 - (value5 + value6 + value7);
			}
			this.m_EducationChart.SetValues(new int[]
			{
				num15,
				value5,
				value6,
				value7
			});
		}
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.FloorToInt(num * 100f), 0, 100);
	}

	private bool IsAbandonned(ref Building building)
	{
		return (building.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None;
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<BuildingManager>.get_exists() && this.m_InstanceID.Type == InstanceType.Building && this.m_InstanceID.Building != 0)
		{
			ushort building = this.m_InstanceID.Building;
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)building].Info;
			BuildingAI buildingAI = info.m_buildingAI;
			ItemClass.Zone zone = info.m_class.GetZone();
			string happinessString = ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)instance.m_buildings.m_buffer[(int)building].m_happiness));
			string spriteName = "Zoning" + zone.ToString() + "Hovered";
			float levelProgress = 0f;
			string levelUpInfo = info.m_buildingAI.GetLevelUpInfo(building, ref instance.m_buildings.m_buffer[(int)building], out levelProgress);
			this.m_Type.set_text(zone.GetLocalizedBuildingType());
			this.m_Status.set_text(buildingAI.GetLocalizedStatus(building, ref instance.m_buildings.m_buffer[(int)building]));
			this.m_ZoneIcon.set_spriteName(spriteName);
			this.m_HappinessIcon.set_spriteName(happinessString);
			this.m_HappinessIcon.set_isVisible(!this.IsAbandonned(ref instance.m_buildings.m_buffer[(int)building]));
			this.m_LevelSprite.set_tooltip(levelUpInfo);
			for (int i = 0; i < 5; i++)
			{
				this.m_Levels[i].set_tooltip(levelUpInfo);
			}
			if (base.IsResidentialZone(zone))
			{
				this.UpdateResidential(ref instance.m_buildings.m_buffer[(int)building]);
			}
			else
			{
				this.UpdateWorkers(building, buildingAI, ref instance.m_buildings.m_buffer[(int)building]);
			}
			this.UpdateLevels(info.m_class, levelProgress, ref instance.m_buildings.m_buffer[(int)building]);
		}
	}

	private string GetSpecializationName(ItemClass.SubService subService)
	{
		switch (subService)
		{
		case ItemClass.SubService.CommercialLeisure:
			return "Leisure";
		case ItemClass.SubService.CommercialTourist:
			return "Tourist";
		case ItemClass.SubService.PublicTransportMonorail:
		case ItemClass.SubService.PublicTransportCableCar:
		case ItemClass.SubService.OfficeGeneric:
			IL_2D:
			switch (subService)
			{
			case ItemClass.SubService.IndustrialForestry:
				return "Forest";
			case ItemClass.SubService.IndustrialFarming:
				return "Farming";
			case ItemClass.SubService.IndustrialOil:
				return "Oil";
			case ItemClass.SubService.IndustrialOre:
				return "Ore";
			default:
				return string.Empty;
			}
			break;
		case ItemClass.SubService.OfficeHightech:
			return "Hightech";
		case ItemClass.SubService.CommercialEco:
			return "Organic";
		case ItemClass.SubService.ResidentialLowEco:
		case ItemClass.SubService.ResidentialHighEco:
			return "Selfsufficient";
		}
		goto IL_2D;
	}

	public Color m_LevelProgressColor = Color.get_blue();

	public Color m_LevelCompleteColor = Color.get_green();

	public Color m_LevelCompleteColorBlink = Color.get_green();

	public Color32 m_ChildColor;

	public Color32 m_TeenColor;

	public Color32 m_YoungColor;

	public Color32 m_AdultColor;

	public Color32 m_SeniorColor;

	public Color32 m_UneducatedColor;

	public Color32 m_EducatedColor;

	public Color32 m_WellEducatedColor;

	public Color32 m_HighlyEducatedColor;

	public Color32 m_UnoccupiedWorkplaceColor;

	public float m_WorkersColorScalar = 0.5f;

	private UITabContainer m_ZoneTypeInfo;

	private UISprite m_ZoneIcon;

	private UISprite m_HappinessIcon;

	private UISprite m_LevelSprite;

	private UIPanel m_LevelProgress;

	private UIProgressBar[] m_Levels = new UIProgressBar[5];

	private UILabel m_Type;

	private UILabel m_Status;

	private UIRadialChart m_AgeChart;

	private UILabel m_ChildLegend;

	private UILabel m_TeenLegend;

	private UILabel m_YoungLegend;

	private UILabel m_AdultLegend;

	private UILabel m_SeniorLegend;

	private UIRadialChart m_EducationChart;

	private UILabel m_UneducatedLegend;

	private UILabel m_EducatedLegend;

	private UILabel m_WellEducatedLegend;

	private UILabel m_HighlyEducatedLegend;

	private UILabel m_JobsAvailLegend;

	private UIRadialChart m_WorkPlacesEducationChart;

	private UIRadialChart m_WorkersEducationChart;

	private UILabel m_WorkSituation;

	private UILabel m_UneducatedPlaces;

	private UILabel m_UneducatedWorkers;

	private UILabel m_EducatedPlaces;

	private UILabel m_EducatedWorkers;

	private UILabel m_WellEducatedPlaces;

	private UILabel m_WellEducatedWorkers;

	private UILabel m_HighlyEducatedPlaces;

	private UILabel m_HighlyEducatedWorkers;

	private UILabel m_OverWorkSituation;

	private UISprite m_SpecializationSprite;
}
