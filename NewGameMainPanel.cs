﻿using System;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class NewGameMainPanel : ToolsModifierControl
{
	private void Start()
	{
		this.m_chooseScenario = base.Find<UIButton>("TabChooseScenario");
		this.m_chooseMap = base.Find<UIButton>("TabChooseMap");
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.Refresh();
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			MainMenu.SetFocus();
		}
	}

	private void Refresh()
	{
		this.m_chooseScenario.set_isEnabled(SaveHelper.AreTherePublishedScenarios());
		if (!this.m_chooseScenario.get_isEnabled())
		{
			this.m_chooseScenario.set_tooltip(Locale.Get("NEWGAME_NOSCENARIOS"));
		}
		else
		{
			this.m_chooseScenario.set_tooltip(string.Empty);
		}
		this.m_chooseMap.set_isEnabled(SaveHelper.AreThereMaps());
		if (!this.m_chooseMap.get_isEnabled())
		{
			this.m_chooseMap.set_tooltip(Locale.Get("NEWGAME_NOMAPS"));
		}
	}

	public void OnClosed()
	{
		UIView.get_library().Hide("NewGamePanel", -1);
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	private UIButton m_chooseScenario;

	private UIButton m_chooseMap;
}
