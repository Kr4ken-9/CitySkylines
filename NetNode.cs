﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct NetNode
{
	public void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort nodeID, int layerMask)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		if (!cameraInfo.Intersect(this.m_bounds))
		{
			return;
		}
		if (this.m_problems != Notification.Problem.None && (layerMask & 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer) != 0)
		{
			Vector3 position = this.m_position;
			position.y += Mathf.Max(5f, info.m_maxHeight);
			Notification.RenderInstance(cameraInfo, this.m_problems, position, 1f);
		}
		if ((layerMask & info.m_netLayers) == 0)
		{
			return;
		}
		if ((this.m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) == NetNode.Flags.None)
		{
			return;
		}
		if ((this.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None)
		{
			if (info.m_segments == null || info.m_segments.Length == 0)
			{
				return;
			}
		}
		else if (info.m_nodes == null || info.m_nodes.Length == 0)
		{
			return;
		}
		uint count = (uint)this.CalculateRendererCount(info);
		RenderManager instance = Singleton<RenderManager>.get_instance();
		uint num;
		if (instance.RequireInstance(86016u + (uint)nodeID, count, out num))
		{
			int num2 = 0;
			while (num != 65535u)
			{
				this.RenderInstance(cameraInfo, nodeID, info, num2, this.m_flags, ref num, ref instance.m_instances[(int)((UIntPtr)num)]);
				if (++num2 > 36)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		info.m_netAI.RenderNode(nodeID, ref this, cameraInfo);
	}

	private int CalculateRendererCount(NetInfo info)
	{
		if ((this.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
		{
			int num = (int)this.m_connectCount;
			if (info.m_requireSegmentRenderers)
			{
				num += this.CountSegments();
			}
			return num;
		}
		return 1;
	}

	private void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort nodeID, NetInfo info, int iter, NetNode.Flags flags, ref uint instanceIndex, ref RenderManager.Instance data)
	{
		if (data.m_dirty)
		{
			data.m_dirty = false;
			if (iter == 0)
			{
				if ((flags & NetNode.Flags.Junction) != NetNode.Flags.None)
				{
					this.RefreshJunctionData(nodeID, info, instanceIndex);
				}
				else if ((flags & NetNode.Flags.Bend) != NetNode.Flags.None)
				{
					this.RefreshBendData(nodeID, info, instanceIndex, ref data);
				}
				else if ((flags & NetNode.Flags.End) != NetNode.Flags.None)
				{
					this.RefreshEndData(nodeID, info, instanceIndex, ref data);
				}
			}
		}
		if (data.m_initialized)
		{
			if ((flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				if ((data.m_dataInt0 & 8) != 0)
				{
					ushort segment = this.GetSegment(data.m_dataInt0 & 7);
					ushort segment2 = this.GetSegment(data.m_dataInt0 >> 4);
					if (segment != 0 && segment2 != 0)
					{
						NetManager instance = Singleton<NetManager>.get_instance();
						info = instance.m_segments.m_buffer[(int)segment].Info;
						NetInfo info2 = instance.m_segments.m_buffer[(int)segment2].Info;
						NetNode.Flags flags2 = flags;
						if (((instance.m_segments.m_buffer[(int)segment].m_flags | instance.m_segments.m_buffer[(int)segment2].m_flags) & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
						{
							flags2 |= NetNode.Flags.Collapsed;
						}
						for (int i = 0; i < info.m_nodes.Length; i++)
						{
							NetInfo.Node node = info.m_nodes[i];
							if (node.CheckFlags(flags2) && node.m_directConnect && (node.m_connectGroup == NetInfo.ConnectGroup.None || (node.m_connectGroup & info2.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None))
							{
								Vector4 dataVector = data.m_dataVector3;
								Vector4 dataVector2 = data.m_dataVector0;
								if (node.m_requireWindSpeed)
								{
									dataVector.w = data.m_dataFloat0;
								}
								if ((node.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
								{
									bool flag = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
									if (info2.m_hasBackwardVehicleLanes != info2.m_hasForwardVehicleLanes)
									{
										bool flag2 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
										if (flag == flag2)
										{
											goto IL_55E;
										}
									}
									if (flag)
									{
										if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
										{
											goto IL_55E;
										}
									}
									else
									{
										if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
										{
											goto IL_55E;
										}
										dataVector2.x = -dataVector2.x;
										dataVector2.y = -dataVector2.y;
									}
								}
								if (cameraInfo.CheckRenderDistance(data.m_position, node.m_lodRenderDistance))
								{
									instance.m_materialBlock.Clear();
									instance.m_materialBlock.SetMatrix(instance.ID_LeftMatrix, data.m_dataMatrix0);
									instance.m_materialBlock.SetMatrix(instance.ID_RightMatrix, data.m_extraData.m_dataMatrix2);
									instance.m_materialBlock.SetVector(instance.ID_MeshScale, dataVector2);
									instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, dataVector);
									instance.m_materialBlock.SetColor(instance.ID_Color, data.m_dataColor0);
									if (node.m_requireSurfaceMaps && data.m_dataTexture1 != null)
									{
										instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexA, data.m_dataTexture0);
										instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexB, data.m_dataTexture1);
										instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, data.m_dataVector1);
									}
									NetManager expr_3B1_cp_0 = instance;
									expr_3B1_cp_0.m_drawCallData.m_defaultCalls = expr_3B1_cp_0.m_drawCallData.m_defaultCalls + 1;
									Graphics.DrawMesh(node.m_nodeMesh, data.m_position, data.m_rotation, node.m_nodeMaterial, node.m_layer, null, 0, instance.m_materialBlock);
								}
								else
								{
									NetInfo.LodValue combinedLod = node.m_combinedLod;
									if (combinedLod != null)
									{
										if (node.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod.m_surfaceTexA)
										{
											if (combinedLod.m_lodCount != 0)
											{
												NetSegment.RenderLod(cameraInfo, combinedLod);
											}
											combinedLod.m_surfaceTexA = data.m_dataTexture0;
											combinedLod.m_surfaceTexB = data.m_dataTexture1;
											combinedLod.m_surfaceMapping = data.m_dataVector1;
										}
										combinedLod.m_leftMatrices[combinedLod.m_lodCount] = data.m_dataMatrix0;
										combinedLod.m_rightMatrices[combinedLod.m_lodCount] = data.m_extraData.m_dataMatrix2;
										combinedLod.m_meshScales[combinedLod.m_lodCount] = dataVector2;
										combinedLod.m_objectIndices[combinedLod.m_lodCount] = dataVector;
										combinedLod.m_meshLocations[combinedLod.m_lodCount] = data.m_position;
										combinedLod.m_lodMin = Vector3.Min(combinedLod.m_lodMin, data.m_position);
										combinedLod.m_lodMax = Vector3.Max(combinedLod.m_lodMax, data.m_position);
										if (++combinedLod.m_lodCount == combinedLod.m_leftMatrices.Length)
										{
											NetSegment.RenderLod(cameraInfo, combinedLod);
										}
									}
								}
							}
							IL_55E:;
						}
					}
				}
				else
				{
					ushort segment3 = this.GetSegment(data.m_dataInt0 & 7);
					if (segment3 != 0)
					{
						NetManager instance2 = Singleton<NetManager>.get_instance();
						info = instance2.m_segments.m_buffer[(int)segment3].Info;
						for (int j = 0; j < info.m_nodes.Length; j++)
						{
							NetInfo.Node node2 = info.m_nodes[j];
							if (node2.CheckFlags(flags) && !node2.m_directConnect)
							{
								Vector4 dataVector3 = data.m_extraData.m_dataVector4;
								if (node2.m_requireWindSpeed)
								{
									dataVector3.w = data.m_dataFloat0;
								}
								if (cameraInfo.CheckRenderDistance(data.m_position, node2.m_lodRenderDistance))
								{
									instance2.m_materialBlock.Clear();
									instance2.m_materialBlock.SetMatrix(instance2.ID_LeftMatrix, data.m_dataMatrix0);
									instance2.m_materialBlock.SetMatrix(instance2.ID_RightMatrix, data.m_extraData.m_dataMatrix2);
									instance2.m_materialBlock.SetMatrix(instance2.ID_LeftMatrixB, data.m_extraData.m_dataMatrix3);
									instance2.m_materialBlock.SetMatrix(instance2.ID_RightMatrixB, data.m_dataMatrix1);
									instance2.m_materialBlock.SetVector(instance2.ID_MeshScale, data.m_dataVector0);
									instance2.m_materialBlock.SetVector(instance2.ID_CenterPos, data.m_dataVector1);
									instance2.m_materialBlock.SetVector(instance2.ID_SideScale, data.m_dataVector2);
									instance2.m_materialBlock.SetVector(instance2.ID_ObjectIndex, dataVector3);
									instance2.m_materialBlock.SetColor(instance2.ID_Color, data.m_dataColor0);
									if (node2.m_requireSurfaceMaps && data.m_dataTexture1 != null)
									{
										instance2.m_materialBlock.SetTexture(instance2.ID_SurfaceTexA, data.m_dataTexture0);
										instance2.m_materialBlock.SetTexture(instance2.ID_SurfaceTexB, data.m_dataTexture1);
										instance2.m_materialBlock.SetVector(instance2.ID_SurfaceMapping, data.m_dataVector3);
									}
									NetManager expr_78D_cp_0 = instance2;
									expr_78D_cp_0.m_drawCallData.m_defaultCalls = expr_78D_cp_0.m_drawCallData.m_defaultCalls + 1;
									Graphics.DrawMesh(node2.m_nodeMesh, data.m_position, data.m_rotation, node2.m_nodeMaterial, node2.m_layer, null, 0, instance2.m_materialBlock);
								}
								else
								{
									NetInfo.LodValue combinedLod2 = node2.m_combinedLod;
									if (combinedLod2 != null)
									{
										if (node2.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod2.m_surfaceTexA)
										{
											if (combinedLod2.m_lodCount != 0)
											{
												NetNode.RenderLod(cameraInfo, combinedLod2);
											}
											combinedLod2.m_surfaceTexA = data.m_dataTexture0;
											combinedLod2.m_surfaceTexB = data.m_dataTexture1;
											combinedLod2.m_surfaceMapping = data.m_dataVector3;
										}
										combinedLod2.m_leftMatrices[combinedLod2.m_lodCount] = data.m_dataMatrix0;
										combinedLod2.m_leftMatricesB[combinedLod2.m_lodCount] = data.m_extraData.m_dataMatrix3;
										combinedLod2.m_rightMatrices[combinedLod2.m_lodCount] = data.m_extraData.m_dataMatrix2;
										combinedLod2.m_rightMatricesB[combinedLod2.m_lodCount] = data.m_dataMatrix1;
										combinedLod2.m_meshScales[combinedLod2.m_lodCount] = data.m_dataVector0;
										combinedLod2.m_centerPositions[combinedLod2.m_lodCount] = data.m_dataVector1;
										combinedLod2.m_sideScales[combinedLod2.m_lodCount] = data.m_dataVector2;
										combinedLod2.m_objectIndices[combinedLod2.m_lodCount] = dataVector3;
										combinedLod2.m_meshLocations[combinedLod2.m_lodCount] = data.m_position;
										combinedLod2.m_lodMin = Vector3.Min(combinedLod2.m_lodMin, data.m_position);
										combinedLod2.m_lodMax = Vector3.Max(combinedLod2.m_lodMax, data.m_position);
										if (++combinedLod2.m_lodCount == combinedLod2.m_leftMatrices.Length)
										{
											NetNode.RenderLod(cameraInfo, combinedLod2);
										}
									}
								}
							}
						}
					}
				}
			}
			else if ((flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				NetManager instance3 = Singleton<NetManager>.get_instance();
				for (int k = 0; k < info.m_nodes.Length; k++)
				{
					NetInfo.Node node3 = info.m_nodes[k];
					if (node3.CheckFlags(flags) && !node3.m_directConnect)
					{
						Vector4 dataVector4 = data.m_extraData.m_dataVector4;
						if (node3.m_requireWindSpeed)
						{
							dataVector4.w = data.m_dataFloat0;
						}
						if (cameraInfo.CheckRenderDistance(data.m_position, node3.m_lodRenderDistance))
						{
							instance3.m_materialBlock.Clear();
							instance3.m_materialBlock.SetMatrix(instance3.ID_LeftMatrix, data.m_dataMatrix0);
							instance3.m_materialBlock.SetMatrix(instance3.ID_RightMatrix, data.m_extraData.m_dataMatrix2);
							instance3.m_materialBlock.SetMatrix(instance3.ID_LeftMatrixB, data.m_extraData.m_dataMatrix3);
							instance3.m_materialBlock.SetMatrix(instance3.ID_RightMatrixB, data.m_dataMatrix1);
							instance3.m_materialBlock.SetVector(instance3.ID_MeshScale, data.m_dataVector0);
							instance3.m_materialBlock.SetVector(instance3.ID_CenterPos, data.m_dataVector1);
							instance3.m_materialBlock.SetVector(instance3.ID_SideScale, data.m_dataVector2);
							instance3.m_materialBlock.SetVector(instance3.ID_ObjectIndex, dataVector4);
							instance3.m_materialBlock.SetColor(instance3.ID_Color, data.m_dataColor0);
							if (node3.m_requireSurfaceMaps && data.m_dataTexture1 != null)
							{
								instance3.m_materialBlock.SetTexture(instance3.ID_SurfaceTexA, data.m_dataTexture0);
								instance3.m_materialBlock.SetTexture(instance3.ID_SurfaceTexB, data.m_dataTexture1);
								instance3.m_materialBlock.SetVector(instance3.ID_SurfaceMapping, data.m_dataVector3);
							}
							NetManager expr_BC8_cp_0 = instance3;
							expr_BC8_cp_0.m_drawCallData.m_defaultCalls = expr_BC8_cp_0.m_drawCallData.m_defaultCalls + 1;
							Graphics.DrawMesh(node3.m_nodeMesh, data.m_position, data.m_rotation, node3.m_nodeMaterial, node3.m_layer, null, 0, instance3.m_materialBlock);
						}
						else
						{
							NetInfo.LodValue combinedLod3 = node3.m_combinedLod;
							if (combinedLod3 != null)
							{
								if (node3.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod3.m_surfaceTexA)
								{
									if (combinedLod3.m_lodCount != 0)
									{
										NetNode.RenderLod(cameraInfo, combinedLod3);
									}
									combinedLod3.m_surfaceTexA = data.m_dataTexture0;
									combinedLod3.m_surfaceTexB = data.m_dataTexture1;
									combinedLod3.m_surfaceMapping = data.m_dataVector3;
								}
								combinedLod3.m_leftMatrices[combinedLod3.m_lodCount] = data.m_dataMatrix0;
								combinedLod3.m_leftMatricesB[combinedLod3.m_lodCount] = data.m_extraData.m_dataMatrix3;
								combinedLod3.m_rightMatrices[combinedLod3.m_lodCount] = data.m_extraData.m_dataMatrix2;
								combinedLod3.m_rightMatricesB[combinedLod3.m_lodCount] = data.m_dataMatrix1;
								combinedLod3.m_meshScales[combinedLod3.m_lodCount] = data.m_dataVector0;
								combinedLod3.m_centerPositions[combinedLod3.m_lodCount] = data.m_dataVector1;
								combinedLod3.m_sideScales[combinedLod3.m_lodCount] = data.m_dataVector2;
								combinedLod3.m_objectIndices[combinedLod3.m_lodCount] = dataVector4;
								combinedLod3.m_meshLocations[combinedLod3.m_lodCount] = data.m_position;
								combinedLod3.m_lodMin = Vector3.Min(combinedLod3.m_lodMin, data.m_position);
								combinedLod3.m_lodMax = Vector3.Max(combinedLod3.m_lodMax, data.m_position);
								if (++combinedLod3.m_lodCount == combinedLod3.m_leftMatrices.Length)
								{
									NetNode.RenderLod(cameraInfo, combinedLod3);
								}
							}
						}
					}
				}
			}
			else if ((flags & NetNode.Flags.Bend) != NetNode.Flags.None)
			{
				NetManager instance4 = Singleton<NetManager>.get_instance();
				for (int l = 0; l < info.m_segments.Length; l++)
				{
					NetInfo.Segment segment4 = info.m_segments[l];
					bool flag3;
					if (segment4.CheckFlags(info.m_netAI.GetBendFlags(nodeID, ref this), out flag3) && !segment4.m_disableBendNodes)
					{
						Vector4 dataVector5 = data.m_dataVector3;
						Vector4 dataVector6 = data.m_dataVector0;
						if (segment4.m_requireWindSpeed)
						{
							dataVector5.w = data.m_dataFloat0;
						}
						if (flag3)
						{
							dataVector6.x = -dataVector6.x;
							dataVector6.y = -dataVector6.y;
						}
						if (cameraInfo.CheckRenderDistance(data.m_position, segment4.m_lodRenderDistance))
						{
							instance4.m_materialBlock.Clear();
							instance4.m_materialBlock.SetMatrix(instance4.ID_LeftMatrix, data.m_dataMatrix0);
							instance4.m_materialBlock.SetMatrix(instance4.ID_RightMatrix, data.m_extraData.m_dataMatrix2);
							instance4.m_materialBlock.SetVector(instance4.ID_MeshScale, dataVector6);
							instance4.m_materialBlock.SetVector(instance4.ID_ObjectIndex, dataVector5);
							instance4.m_materialBlock.SetColor(instance4.ID_Color, data.m_dataColor0);
							if (segment4.m_requireSurfaceMaps && data.m_dataTexture1 != null)
							{
								instance4.m_materialBlock.SetTexture(instance4.ID_SurfaceTexA, data.m_dataTexture0);
								instance4.m_materialBlock.SetTexture(instance4.ID_SurfaceTexB, data.m_dataTexture1);
								instance4.m_materialBlock.SetVector(instance4.ID_SurfaceMapping, data.m_dataVector1);
							}
							NetManager expr_FC7_cp_0 = instance4;
							expr_FC7_cp_0.m_drawCallData.m_defaultCalls = expr_FC7_cp_0.m_drawCallData.m_defaultCalls + 1;
							Graphics.DrawMesh(segment4.m_segmentMesh, data.m_position, data.m_rotation, segment4.m_segmentMaterial, segment4.m_layer, null, 0, instance4.m_materialBlock);
						}
						else
						{
							NetInfo.LodValue combinedLod4 = segment4.m_combinedLod;
							if (combinedLod4 != null)
							{
								if (segment4.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod4.m_surfaceTexA)
								{
									if (combinedLod4.m_lodCount != 0)
									{
										NetSegment.RenderLod(cameraInfo, combinedLod4);
									}
									combinedLod4.m_surfaceTexA = data.m_dataTexture0;
									combinedLod4.m_surfaceTexB = data.m_dataTexture1;
									combinedLod4.m_surfaceMapping = data.m_dataVector1;
								}
								combinedLod4.m_leftMatrices[combinedLod4.m_lodCount] = data.m_dataMatrix0;
								combinedLod4.m_rightMatrices[combinedLod4.m_lodCount] = data.m_extraData.m_dataMatrix2;
								combinedLod4.m_meshScales[combinedLod4.m_lodCount] = dataVector6;
								combinedLod4.m_objectIndices[combinedLod4.m_lodCount] = dataVector5;
								combinedLod4.m_meshLocations[combinedLod4.m_lodCount] = data.m_position;
								combinedLod4.m_lodMin = Vector3.Min(combinedLod4.m_lodMin, data.m_position);
								combinedLod4.m_lodMax = Vector3.Max(combinedLod4.m_lodMax, data.m_position);
								if (++combinedLod4.m_lodCount == combinedLod4.m_leftMatrices.Length)
								{
									NetSegment.RenderLod(cameraInfo, combinedLod4);
								}
							}
						}
					}
				}
				for (int m = 0; m < info.m_nodes.Length; m++)
				{
					ushort segment5 = this.GetSegment(data.m_dataInt0 & 7);
					ushort segment6 = this.GetSegment(data.m_dataInt0 >> 4);
					if (((instance4.m_segments.m_buffer[(int)segment5].m_flags | instance4.m_segments.m_buffer[(int)segment6].m_flags) & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
					{
						NetNode.Flags flags3 = flags | NetNode.Flags.Collapsed;
					}
					NetInfo.Node node4 = info.m_nodes[m];
					if (node4.CheckFlags(flags) && node4.m_directConnect && (node4.m_connectGroup == NetInfo.ConnectGroup.None || (node4.m_connectGroup & info.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None))
					{
						Vector4 dataVector7 = data.m_dataVector3;
						Vector4 dataVector8 = data.m_dataVector0;
						if (node4.m_requireWindSpeed)
						{
							dataVector7.w = data.m_dataFloat0;
						}
						if ((node4.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
						{
							bool flag4 = instance4.m_segments.m_buffer[(int)segment5].m_startNode == nodeID == ((instance4.m_segments.m_buffer[(int)segment5].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
							bool flag5 = instance4.m_segments.m_buffer[(int)segment6].m_startNode == nodeID == ((instance4.m_segments.m_buffer[(int)segment6].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
							if (flag4 == flag5)
							{
								goto IL_1625;
							}
							if (flag4)
							{
								if ((node4.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
								{
									goto IL_1625;
								}
							}
							else
							{
								if ((node4.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
								{
									goto IL_1625;
								}
								dataVector8.x = -dataVector8.x;
								dataVector8.y = -dataVector8.y;
							}
						}
						if (cameraInfo.CheckRenderDistance(data.m_position, node4.m_lodRenderDistance))
						{
							instance4.m_materialBlock.Clear();
							instance4.m_materialBlock.SetMatrix(instance4.ID_LeftMatrix, data.m_dataMatrix0);
							instance4.m_materialBlock.SetMatrix(instance4.ID_RightMatrix, data.m_extraData.m_dataMatrix2);
							instance4.m_materialBlock.SetVector(instance4.ID_MeshScale, dataVector8);
							instance4.m_materialBlock.SetVector(instance4.ID_ObjectIndex, dataVector7);
							instance4.m_materialBlock.SetColor(instance4.ID_Color, data.m_dataColor0);
							if (node4.m_requireSurfaceMaps && data.m_dataTexture1 != null)
							{
								instance4.m_materialBlock.SetTexture(instance4.ID_SurfaceTexA, data.m_dataTexture0);
								instance4.m_materialBlock.SetTexture(instance4.ID_SurfaceTexB, data.m_dataTexture1);
								instance4.m_materialBlock.SetVector(instance4.ID_SurfaceMapping, data.m_dataVector1);
							}
							NetManager expr_1477_cp_0 = instance4;
							expr_1477_cp_0.m_drawCallData.m_defaultCalls = expr_1477_cp_0.m_drawCallData.m_defaultCalls + 1;
							Graphics.DrawMesh(node4.m_nodeMesh, data.m_position, data.m_rotation, node4.m_nodeMaterial, node4.m_layer, null, 0, instance4.m_materialBlock);
						}
						else
						{
							NetInfo.LodValue combinedLod5 = node4.m_combinedLod;
							if (combinedLod5 != null)
							{
								if (node4.m_requireSurfaceMaps && data.m_dataTexture0 != combinedLod5.m_surfaceTexA)
								{
									if (combinedLod5.m_lodCount != 0)
									{
										NetSegment.RenderLod(cameraInfo, combinedLod5);
									}
									combinedLod5.m_surfaceTexA = data.m_dataTexture0;
									combinedLod5.m_surfaceTexB = data.m_dataTexture1;
									combinedLod5.m_surfaceMapping = data.m_dataVector1;
								}
								combinedLod5.m_leftMatrices[combinedLod5.m_lodCount] = data.m_dataMatrix0;
								combinedLod5.m_rightMatrices[combinedLod5.m_lodCount] = data.m_extraData.m_dataMatrix2;
								combinedLod5.m_meshScales[combinedLod5.m_lodCount] = dataVector8;
								combinedLod5.m_objectIndices[combinedLod5.m_lodCount] = dataVector7;
								combinedLod5.m_meshLocations[combinedLod5.m_lodCount] = data.m_position;
								combinedLod5.m_lodMin = Vector3.Min(combinedLod5.m_lodMin, data.m_position);
								combinedLod5.m_lodMax = Vector3.Max(combinedLod5.m_lodMax, data.m_position);
								if (++combinedLod5.m_lodCount == combinedLod5.m_leftMatrices.Length)
								{
									NetSegment.RenderLod(cameraInfo, combinedLod5);
								}
							}
						}
					}
					IL_1625:;
				}
			}
		}
		instanceIndex = (uint)data.m_nextInstance;
	}

	public static void RenderLod(RenderManager.CameraInfo cameraInfo, NetInfo.LodValue lod)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		MaterialPropertyBlock materialBlock = instance.m_materialBlock;
		materialBlock.Clear();
		Mesh mesh;
		int num;
		if (lod.m_lodCount <= 1)
		{
			mesh = lod.m_key.m_mesh.m_mesh1;
			num = 1;
		}
		else if (lod.m_lodCount <= 4)
		{
			mesh = lod.m_key.m_mesh.m_mesh4;
			num = 4;
		}
		else
		{
			mesh = lod.m_key.m_mesh.m_mesh8;
			num = 8;
		}
		for (int i = lod.m_lodCount; i < num; i++)
		{
			lod.m_leftMatrices[i] = default(Matrix4x4);
			lod.m_leftMatricesB[i] = default(Matrix4x4);
			lod.m_rightMatrices[i] = default(Matrix4x4);
			lod.m_rightMatricesB[i] = default(Matrix4x4);
			lod.m_meshScales[i] = Vector4.get_zero();
			lod.m_centerPositions[i] = Vector4.get_zero();
			lod.m_sideScales[i] = Vector4.get_zero();
			lod.m_objectIndices[i] = Vector4.get_zero();
			lod.m_meshLocations[i] = cameraInfo.m_forward * -100000f;
		}
		materialBlock.SetMatrixArray(instance.ID_LeftMatrices, lod.m_leftMatrices);
		materialBlock.SetMatrixArray(instance.ID_LeftMatricesB, lod.m_leftMatricesB);
		materialBlock.SetMatrixArray(instance.ID_RightMatrices, lod.m_rightMatrices);
		materialBlock.SetMatrixArray(instance.ID_RightMatricesB, lod.m_rightMatricesB);
		materialBlock.SetVectorArray(instance.ID_MeshScales, lod.m_meshScales);
		materialBlock.SetVectorArray(instance.ID_CenterPositions, lod.m_centerPositions);
		materialBlock.SetVectorArray(instance.ID_SideScales, lod.m_sideScales);
		materialBlock.SetVectorArray(instance.ID_ObjectIndices, lod.m_objectIndices);
		materialBlock.SetVectorArray(instance.ID_MeshLocations, lod.m_meshLocations);
		if (lod.m_surfaceTexA != null)
		{
			materialBlock.SetTexture(instance.ID_SurfaceTexA, lod.m_surfaceTexA);
			materialBlock.SetTexture(instance.ID_SurfaceTexB, lod.m_surfaceTexB);
			materialBlock.SetVector(instance.ID_SurfaceMapping, lod.m_surfaceMapping);
			lod.m_surfaceTexA = null;
			lod.m_surfaceTexB = null;
		}
		if (mesh != null)
		{
			Bounds bounds = default(Bounds);
			bounds.SetMinMax(lod.m_lodMin - new Vector3(100f, 100f, 100f), lod.m_lodMax + new Vector3(100f, 100f, 100f));
			mesh.set_bounds(bounds);
			lod.m_lodMin = new Vector3(100000f, 100000f, 100000f);
			lod.m_lodMax = new Vector3(-100000f, -100000f, -100000f);
			NetManager expr_30E_cp_0 = instance;
			expr_30E_cp_0.m_drawCallData.m_lodCalls = expr_30E_cp_0.m_drawCallData.m_lodCalls + 1;
			NetManager expr_321_cp_0 = instance;
			expr_321_cp_0.m_drawCallData.m_batchedCalls = expr_321_cp_0.m_drawCallData.m_batchedCalls + (lod.m_lodCount - 1);
			Graphics.DrawMesh(mesh, Matrix4x4.get_identity(), lod.m_material, lod.m_key.m_layer, null, 0, materialBlock);
		}
		lod.m_lodCount = 0;
	}

	private void RefreshEndData(ushort nodeID, NetInfo info, uint instanceIndex, ref RenderManager.Instance data)
	{
		data.m_position = this.m_position;
		data.m_rotation = Quaternion.get_identity();
		data.m_initialized = true;
		float vScale = info.m_netAI.GetVScale() / 1.5f;
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		Vector3 zero3 = Vector3.get_zero();
		Vector3 zero4 = Vector3.get_zero();
		Vector3 vector3 = Vector3.get_zero();
		Vector3 vector4 = Vector3.get_zero();
		bool flag = false;
		ushort num = 0;
		int num2 = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
				bool start = netSegment.m_startNode == nodeID;
				bool flag2;
				netSegment.CalculateCorner(segment, true, start, false, out zero, out zero3, out flag2);
				netSegment.CalculateCorner(segment, true, start, true, out zero2, out zero4, out flag2);
				if (flag)
				{
					vector3 = -zero3;
					vector4 = -zero4;
					zero3.y = 0.25f;
					zero4.y = 0.25f;
					vector3.y = -5f;
					vector4.y = -5f;
					vector = zero - zero3 * 10f + vector3 * 10f;
					vector2 = zero2 - zero4 * 10f + vector4 * 10f;
				}
				else
				{
					vector = zero2;
					vector2 = zero;
					vector3 = zero4;
					vector4 = zero3;
				}
				num = segment;
				num2 = i;
			}
		}
		if (flag)
		{
			Vector3 vector5;
			Vector3 vector6;
			NetSegment.CalculateMiddlePoints(zero, -zero3, vector, -vector3, true, true, out vector5, out vector6);
			Vector3 vector7;
			Vector3 vector8;
			NetSegment.CalculateMiddlePoints(zero2, -zero4, vector2, -vector4, true, true, out vector7, out vector8);
			data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector5, vector6, vector, zero2, vector7, vector8, vector2, this.m_position, vScale);
			data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector7, vector8, vector2, zero, vector5, vector6, vector, this.m_position, vScale);
			data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
			Vector4 colorLocation = RenderManager.GetColorLocation(86016u + (uint)nodeID);
			data.m_dataVector3 = new Vector4(colorLocation.x, colorLocation.y, colorLocation.x, colorLocation.y);
			data.m_dataColor0 = info.m_color;
			data.m_dataColor0.a = 0f;
			data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
			data.m_dataInt0 = (8 | num2);
		}
		else
		{
			float num3 = info.m_netAI.GetEndRadius() * 1.33333337f;
			Vector3 vector9 = zero - zero3 * num3;
			Vector3 vector10 = vector - vector3 * num3;
			Vector3 vector11 = zero2 - zero4 * num3;
			Vector3 vector12 = vector2 - vector4 * num3;
			Vector3 vector13 = zero + zero3 * num3;
			Vector3 vector14 = vector + vector3 * num3;
			Vector3 vector15 = zero2 + zero4 * num3;
			Vector3 vector16 = vector2 + vector4 * num3;
			data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector9, vector10, vector, zero, vector9, vector10, vector, this.m_position, vScale);
			data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector15, vector16, vector2, zero2, vector15, vector16, vector2, this.m_position, vScale);
			data.m_extraData.m_dataMatrix3 = NetSegment.CalculateControlMatrix(zero, vector13, vector14, vector, zero, vector13, vector14, vector, this.m_position, vScale);
			data.m_dataMatrix1 = NetSegment.CalculateControlMatrix(zero2, vector11, vector12, vector2, zero2, vector11, vector12, vector2, this.m_position, vScale);
			data.m_dataMatrix0.SetRow(3, data.m_dataMatrix0.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_extraData.m_dataMatrix2.SetRow(3, data.m_extraData.m_dataMatrix2.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_extraData.m_dataMatrix3.SetRow(3, data.m_extraData.m_dataMatrix3.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_dataMatrix1.SetRow(3, data.m_dataMatrix1.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 0.5f - info.m_pavementWidth / info.m_halfWidth * 0.5f, info.m_pavementWidth / info.m_halfWidth * 0.5f);
			data.m_dataVector1 = new Vector4(0f, (float)this.m_heightOffset * 0.015625f, 0f, 0f);
			data.m_dataVector1.w = (data.m_dataMatrix0.m31 + data.m_dataMatrix0.m32 + data.m_extraData.m_dataMatrix2.m31 + data.m_extraData.m_dataMatrix2.m32 + data.m_extraData.m_dataMatrix3.m31 + data.m_extraData.m_dataMatrix3.m32 + data.m_dataMatrix1.m31 + data.m_dataMatrix1.m32) * 0.125f;
			data.m_dataVector2 = new Vector4(info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f, info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f);
			Vector4 colorLocation2 = RenderManager.GetColorLocation((uint)(49152 + num));
			data.m_extraData.m_dataVector4 = new Vector4(colorLocation2.x, colorLocation2.y, colorLocation2.x, colorLocation2.y);
			data.m_dataColor0 = info.m_color;
			data.m_dataColor0.a = 0f;
			data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
			data.m_dataInt0 = num2;
		}
		if (info.m_requireSurfaceMaps)
		{
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(data.m_position, out data.m_dataTexture0, out data.m_dataTexture1, out data.m_dataVector3);
		}
	}

	private void RefreshBendData(ushort nodeID, NetInfo info, uint instanceIndex, ref RenderManager.Instance data)
	{
		data.m_position = this.m_position;
		data.m_rotation = Quaternion.get_identity();
		data.m_initialized = true;
		float vScale = info.m_netAI.GetVScale();
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		Vector3 zero3 = Vector3.get_zero();
		Vector3 zero4 = Vector3.get_zero();
		Vector3 zero5 = Vector3.get_zero();
		Vector3 zero6 = Vector3.get_zero();
		Vector3 zero7 = Vector3.get_zero();
		Vector3 zero8 = Vector3.get_zero();
		int num = 0;
		int num2 = 0;
		bool flag = false;
		int num3 = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
				bool flag2 = ++num3 == 1;
				bool flag3 = netSegment.m_startNode == nodeID;
				bool flag4 = (netSegment.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
				if ((!flag2 && !flag) || (flag2 && flag3 == flag4))
				{
					bool flag5;
					netSegment.CalculateCorner(segment, true, flag3, false, out zero, out zero5, out flag5);
					netSegment.CalculateCorner(segment, true, flag3, true, out zero2, out zero6, out flag5);
					flag = true;
					num = i;
				}
				else
				{
					bool flag5;
					netSegment.CalculateCorner(segment, true, flag3, true, out zero3, out zero7, out flag5);
					netSegment.CalculateCorner(segment, true, flag3, false, out zero4, out zero8, out flag5);
					num2 = i;
				}
			}
		}
		Vector3 vector;
		Vector3 vector2;
		NetSegment.CalculateMiddlePoints(zero, -zero5, zero3, -zero7, true, true, out vector, out vector2);
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateMiddlePoints(zero2, -zero6, zero4, -zero8, true, true, out vector3, out vector4);
		data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector, vector2, zero3, zero2, vector3, vector4, zero4, this.m_position, vScale);
		data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector3, vector4, zero4, zero, vector, vector2, zero3, this.m_position, vScale);
		data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
		Vector4 colorLocation = RenderManager.GetColorLocation(86016u + (uint)nodeID);
		data.m_dataVector3 = new Vector4(colorLocation.x, colorLocation.y, colorLocation.x, colorLocation.y);
		data.m_dataColor0 = info.m_color;
		data.m_dataColor0.a = 0f;
		data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
		data.m_dataInt0 = (num | num2 << 4);
		if (info.m_requireSurfaceMaps)
		{
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(data.m_position, out data.m_dataTexture0, out data.m_dataTexture1, out data.m_dataVector1);
		}
	}

	private void RefreshJunctionData(ushort nodeID, NetInfo info, uint instanceIndex)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 vector = this.m_position;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
				ItemClass connectionClass = info2.GetConnectionClass();
				Vector3 vector2 = (nodeID != instance.m_segments.m_buffer[(int)segment].m_startNode) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
				float num = -1f;
				for (int j = 0; j < 8; j++)
				{
					ushort segment2 = this.GetSegment(j);
					if (segment2 != 0 && segment2 != segment)
					{
						NetInfo info3 = instance.m_segments.m_buffer[(int)segment2].Info;
						ItemClass connectionClass2 = info3.GetConnectionClass();
						if (connectionClass.m_service == connectionClass2.m_service || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None)
						{
							Vector3 vector3 = (nodeID != instance.m_segments.m_buffer[(int)segment2].m_startNode) ? instance.m_segments.m_buffer[(int)segment2].m_endDirection : instance.m_segments.m_buffer[(int)segment2].m_startDirection;
							float num2 = vector2.x * vector3.x + vector2.z * vector3.z;
							num = Mathf.Max(num, num2);
							bool flag = info2.m_requireDirectRenderers && (info2.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None);
							bool flag2 = info3.m_requireDirectRenderers && (info3.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None);
							if (j > i && (flag || flag2))
							{
								float num3 = 0.01f - Mathf.Min(info2.m_maxTurnAngleCos, info3.m_maxTurnAngleCos);
								if (num2 < num3 && instanceIndex != 65535u)
								{
									float num4;
									if (flag)
									{
										num4 = info2.m_netAI.GetNodeInfoPriority(segment, ref instance.m_segments.m_buffer[(int)segment]);
									}
									else
									{
										num4 = -1E+08f;
									}
									float num5;
									if (flag2)
									{
										num5 = info3.m_netAI.GetNodeInfoPriority(segment2, ref instance.m_segments.m_buffer[(int)segment2]);
									}
									else
									{
										num5 = -1E+08f;
									}
									if (num4 >= num5)
									{
										this.RefreshJunctionData(nodeID, i, j, info2, info3, segment, segment2, ref instanceIndex, ref Singleton<RenderManager>.get_instance().m_instances[(int)((UIntPtr)instanceIndex)]);
									}
									else
									{
										this.RefreshJunctionData(nodeID, j, i, info3, info2, segment2, segment, ref instanceIndex, ref Singleton<RenderManager>.get_instance().m_instances[(int)((UIntPtr)instanceIndex)]);
									}
								}
							}
						}
					}
				}
				vector += vector2 * (2f + num * 2f);
			}
		}
		vector.y = this.m_position.y + (float)this.m_heightOffset * 0.015625f;
		if (info.m_requireSegmentRenderers)
		{
			for (int k = 0; k < 8; k++)
			{
				ushort segment3 = this.GetSegment(k);
				if (segment3 != 0 && instanceIndex != 65535u)
				{
					this.RefreshJunctionData(nodeID, k, segment3, vector, ref instanceIndex, ref Singleton<RenderManager>.get_instance().m_instances[(int)((UIntPtr)instanceIndex)]);
				}
			}
		}
	}

	private void RefreshJunctionData(ushort nodeID, int segmentIndex, int segmentIndex2, NetInfo info, NetInfo info2, ushort nodeSegment, ushort nodeSegment2, ref uint instanceIndex, ref RenderManager.Instance data)
	{
		data.m_position = this.m_position;
		data.m_rotation = Quaternion.get_identity();
		data.m_initialized = true;
		float vScale = info.m_netAI.GetVScale();
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		Vector3 zero3 = Vector3.get_zero();
		Vector3 zero4 = Vector3.get_zero();
		Vector3 zero5 = Vector3.get_zero();
		Vector3 zero6 = Vector3.get_zero();
		bool start = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment].m_startNode == nodeID;
		bool flag;
		Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment].CalculateCorner(nodeSegment, true, start, false, out zero, out zero3, out flag);
		Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment].CalculateCorner(nodeSegment, true, start, true, out zero2, out zero4, out flag);
		start = (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment2].m_startNode == nodeID);
		Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment2].CalculateCorner(nodeSegment2, true, start, true, out vector, out zero5, out flag);
		Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)nodeSegment2].CalculateCorner(nodeSegment2, true, start, false, out vector2, out zero6, out flag);
		Vector3 vector3 = (vector2 - vector) * (info.m_halfWidth / info2.m_halfWidth * 0.5f - 0.5f);
		vector -= vector3;
		vector2 += vector3;
		Vector3 vector4;
		Vector3 vector5;
		NetSegment.CalculateMiddlePoints(zero, -zero3, vector, -zero5, true, true, out vector4, out vector5);
		Vector3 vector6;
		Vector3 vector7;
		NetSegment.CalculateMiddlePoints(zero2, -zero4, vector2, -zero6, true, true, out vector6, out vector7);
		data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector4, vector5, vector, zero2, vector6, vector7, vector2, this.m_position, vScale);
		data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector6, vector7, vector2, zero, vector4, vector5, vector, this.m_position, vScale);
		data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
		Vector4 colorLocation;
		Vector4 vector8;
		if (NetNode.BlendJunction(nodeID))
		{
			colorLocation = RenderManager.GetColorLocation(86016u + (uint)nodeID);
			vector8 = colorLocation;
		}
		else
		{
			colorLocation = RenderManager.GetColorLocation((uint)(49152 + nodeSegment));
			vector8 = RenderManager.GetColorLocation((uint)(49152 + nodeSegment2));
		}
		data.m_dataVector3 = new Vector4(colorLocation.x, colorLocation.y, vector8.x, vector8.y);
		data.m_dataInt0 = (8 | segmentIndex | segmentIndex2 << 4);
		data.m_dataColor0 = info.m_color;
		data.m_dataColor0.a = 0f;
		data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
		if (info.m_requireSurfaceMaps)
		{
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(data.m_position, out data.m_dataTexture0, out data.m_dataTexture1, out data.m_dataVector1);
		}
		instanceIndex = (uint)data.m_nextInstance;
	}

	private void RefreshJunctionData(ushort nodeID, int segmentIndex, ushort nodeSegment, Vector3 centerPos, ref uint instanceIndex, ref RenderManager.Instance data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		data.m_position = this.m_position;
		data.m_rotation = Quaternion.get_identity();
		data.m_initialized = true;
		Vector3 zero = Vector3.get_zero();
		Vector3 zero2 = Vector3.get_zero();
		Vector3 zero3 = Vector3.get_zero();
		Vector3 zero4 = Vector3.get_zero();
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		Vector3 vector3 = Vector3.get_zero();
		Vector3 vector4 = Vector3.get_zero();
		Vector3 zero5 = Vector3.get_zero();
		Vector3 zero6 = Vector3.get_zero();
		Vector3 zero7 = Vector3.get_zero();
		Vector3 zero8 = Vector3.get_zero();
		NetSegment netSegment = instance.m_segments.m_buffer[(int)nodeSegment];
		NetInfo info = netSegment.Info;
		float vScale = info.m_netAI.GetVScale();
		ItemClass connectionClass = info.GetConnectionClass();
		Vector3 vector5 = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
		float num = -4f;
		float num2 = -4f;
		ushort num3 = 0;
		ushort num4 = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0 && segment != nodeSegment)
			{
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
				ItemClass connectionClass2 = info2.GetConnectionClass();
				if (connectionClass.m_service == connectionClass2.m_service)
				{
					NetSegment netSegment2 = instance.m_segments.m_buffer[(int)segment];
					Vector3 vector6 = (nodeID != netSegment2.m_startNode) ? netSegment2.m_endDirection : netSegment2.m_startDirection;
					float num5 = vector5.x * vector6.x + vector5.z * vector6.z;
					if (vector6.z * vector5.x - vector6.x * vector5.z < 0f)
					{
						if (num5 > num)
						{
							num = num5;
							num3 = segment;
						}
						num5 = -2f - num5;
						if (num5 > num2)
						{
							num2 = num5;
							num4 = segment;
						}
					}
					else
					{
						if (num5 > num2)
						{
							num2 = num5;
							num4 = segment;
						}
						num5 = -2f - num5;
						if (num5 > num)
						{
							num = num5;
							num3 = segment;
						}
					}
				}
			}
		}
		bool start = netSegment.m_startNode == nodeID;
		bool flag;
		netSegment.CalculateCorner(nodeSegment, true, start, false, out zero, out zero3, out flag);
		netSegment.CalculateCorner(nodeSegment, true, start, true, out zero2, out zero4, out flag);
		if (num3 != 0 && num4 != 0)
		{
			float num6 = info.m_pavementWidth / info.m_halfWidth * 0.5f;
			float num7 = 1f;
			if (num3 != 0)
			{
				NetSegment netSegment3 = instance.m_segments.m_buffer[(int)num3];
				NetInfo info3 = netSegment3.Info;
				start = (netSegment3.m_startNode == nodeID);
				netSegment3.CalculateCorner(num3, true, start, true, out vector, out vector3, out flag);
				netSegment3.CalculateCorner(num3, true, start, false, out vector2, out vector4, out flag);
				float num8 = info3.m_pavementWidth / info3.m_halfWidth * 0.5f;
				num6 = (num6 + num8) * 0.5f;
				num7 = 2f * info.m_halfWidth / (info.m_halfWidth + info3.m_halfWidth);
			}
			float num9 = info.m_pavementWidth / info.m_halfWidth * 0.5f;
			float num10 = 1f;
			if (num4 != 0)
			{
				NetSegment netSegment4 = instance.m_segments.m_buffer[(int)num4];
				NetInfo info4 = netSegment4.Info;
				start = (netSegment4.m_startNode == nodeID);
				netSegment4.CalculateCorner(num4, true, start, true, out zero5, out zero7, out flag);
				netSegment4.CalculateCorner(num4, true, start, false, out zero6, out zero8, out flag);
				float num11 = info4.m_pavementWidth / info4.m_halfWidth * 0.5f;
				num9 = (num9 + num11) * 0.5f;
				num10 = 2f * info.m_halfWidth / (info.m_halfWidth + info4.m_halfWidth);
			}
			Vector3 vector7;
			Vector3 vector8;
			NetSegment.CalculateMiddlePoints(zero, -zero3, vector, -vector3, true, true, out vector7, out vector8);
			Vector3 vector9;
			Vector3 vector10;
			NetSegment.CalculateMiddlePoints(zero2, -zero4, vector2, -vector4, true, true, out vector9, out vector10);
			Vector3 vector11;
			Vector3 vector12;
			NetSegment.CalculateMiddlePoints(zero, -zero3, zero5, -zero7, true, true, out vector11, out vector12);
			Vector3 vector13;
			Vector3 vector14;
			NetSegment.CalculateMiddlePoints(zero2, -zero4, zero6, -zero8, true, true, out vector13, out vector14);
			data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector7, vector8, vector, zero, vector7, vector8, vector, this.m_position, vScale);
			data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector9, vector10, vector2, zero2, vector9, vector10, vector2, this.m_position, vScale);
			data.m_extraData.m_dataMatrix3 = NetSegment.CalculateControlMatrix(zero, vector11, vector12, zero5, zero, vector11, vector12, zero5, this.m_position, vScale);
			data.m_dataMatrix1 = NetSegment.CalculateControlMatrix(zero2, vector13, vector14, zero6, zero2, vector13, vector14, zero6, this.m_position, vScale);
			data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 0.5f - info.m_pavementWidth / info.m_halfWidth * 0.5f, info.m_pavementWidth / info.m_halfWidth * 0.5f);
			data.m_dataVector1 = centerPos - data.m_position;
			data.m_dataVector1.w = (data.m_dataMatrix0.m31 + data.m_dataMatrix0.m32 + data.m_extraData.m_dataMatrix2.m31 + data.m_extraData.m_dataMatrix2.m32 + data.m_extraData.m_dataMatrix3.m31 + data.m_extraData.m_dataMatrix3.m32 + data.m_dataMatrix1.m31 + data.m_dataMatrix1.m32) * 0.125f;
			data.m_dataVector2 = new Vector4(num6, num7, num9, num10);
		}
		else
		{
			centerPos.x = (zero.x + zero2.x) * 0.5f;
			centerPos.z = (zero.z + zero2.z) * 0.5f;
			vector = zero2;
			vector2 = zero;
			vector3 = zero4;
			vector4 = zero3;
			float num12 = info.m_netAI.GetEndRadius() * 1.33333337f;
			Vector3 vector15 = zero - zero3 * num12;
			Vector3 vector16 = vector - vector3 * num12;
			Vector3 vector17 = zero2 - zero4 * num12;
			Vector3 vector18 = vector2 - vector4 * num12;
			Vector3 vector19 = zero + zero3 * num12;
			Vector3 vector20 = vector + vector3 * num12;
			Vector3 vector21 = zero2 + zero4 * num12;
			Vector3 vector22 = vector2 + vector4 * num12;
			data.m_dataMatrix0 = NetSegment.CalculateControlMatrix(zero, vector15, vector16, vector, zero, vector15, vector16, vector, this.m_position, vScale);
			data.m_extraData.m_dataMatrix2 = NetSegment.CalculateControlMatrix(zero2, vector21, vector22, vector2, zero2, vector21, vector22, vector2, this.m_position, vScale);
			data.m_extraData.m_dataMatrix3 = NetSegment.CalculateControlMatrix(zero, vector19, vector20, vector, zero, vector19, vector20, vector, this.m_position, vScale);
			data.m_dataMatrix1 = NetSegment.CalculateControlMatrix(zero2, vector17, vector18, vector2, zero2, vector17, vector18, vector2, this.m_position, vScale);
			data.m_dataMatrix0.SetRow(3, data.m_dataMatrix0.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_extraData.m_dataMatrix2.SetRow(3, data.m_extraData.m_dataMatrix2.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_extraData.m_dataMatrix3.SetRow(3, data.m_extraData.m_dataMatrix3.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_dataMatrix1.SetRow(3, data.m_dataMatrix1.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
			data.m_dataVector0 = new Vector4(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 0.5f - info.m_pavementWidth / info.m_halfWidth * 0.5f, info.m_pavementWidth / info.m_halfWidth * 0.5f);
			data.m_dataVector1 = centerPos - data.m_position;
			data.m_dataVector1.w = (data.m_dataMatrix0.m31 + data.m_dataMatrix0.m32 + data.m_extraData.m_dataMatrix2.m31 + data.m_extraData.m_dataMatrix2.m32 + data.m_extraData.m_dataMatrix3.m31 + data.m_extraData.m_dataMatrix3.m32 + data.m_dataMatrix1.m31 + data.m_dataMatrix1.m32) * 0.125f;
			data.m_dataVector2 = new Vector4(info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f, info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f);
		}
		Vector4 colorLocation;
		Vector4 vector23;
		if (NetNode.BlendJunction(nodeID))
		{
			colorLocation = RenderManager.GetColorLocation(86016u + (uint)nodeID);
			vector23 = colorLocation;
		}
		else
		{
			colorLocation = RenderManager.GetColorLocation((uint)(49152 + nodeSegment));
			vector23 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
		}
		data.m_extraData.m_dataVector4 = new Vector4(colorLocation.x, colorLocation.y, vector23.x, vector23.y);
		data.m_dataInt0 = segmentIndex;
		data.m_dataColor0 = info.m_color;
		data.m_dataColor0.a = 0f;
		data.m_dataFloat0 = Singleton<WeatherManager>.get_instance().GetWindSpeed(data.m_position);
		if (info.m_requireSurfaceMaps)
		{
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(data.m_position, out data.m_dataTexture0, out data.m_dataTexture1, out data.m_dataVector3);
		}
		instanceIndex = (uint)data.m_nextInstance;
	}

	public NetInfo Info
	{
		get
		{
			return PrefabCollection<NetInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public bool AddSegment(ushort segment)
	{
		if (this.m_segment0 == 0)
		{
			this.m_segment0 = segment;
			return true;
		}
		if (this.m_segment1 == 0)
		{
			this.m_segment1 = segment;
			return true;
		}
		if (this.m_segment2 == 0)
		{
			this.m_segment2 = segment;
			return true;
		}
		if (this.m_segment3 == 0)
		{
			this.m_segment3 = segment;
			return true;
		}
		if (this.m_segment4 == 0)
		{
			this.m_segment4 = segment;
			return true;
		}
		if (this.m_segment5 == 0)
		{
			this.m_segment5 = segment;
			return true;
		}
		if (this.m_segment6 == 0)
		{
			this.m_segment6 = segment;
			return true;
		}
		if (this.m_segment7 == 0)
		{
			this.m_segment7 = segment;
			return true;
		}
		return false;
	}

	public bool RemoveSegment(ushort segment)
	{
		if (this.m_segment0 == segment)
		{
			this.m_segment0 = 0;
			return true;
		}
		if (this.m_segment1 == segment)
		{
			this.m_segment1 = 0;
			return true;
		}
		if (this.m_segment2 == segment)
		{
			this.m_segment2 = 0;
			return true;
		}
		if (this.m_segment3 == segment)
		{
			this.m_segment3 = 0;
			return true;
		}
		if (this.m_segment4 == segment)
		{
			this.m_segment4 = 0;
			return true;
		}
		if (this.m_segment5 == segment)
		{
			this.m_segment5 = 0;
			return true;
		}
		if (this.m_segment6 == segment)
		{
			this.m_segment6 = 0;
			return true;
		}
		if (this.m_segment7 == segment)
		{
			this.m_segment7 = 0;
			return true;
		}
		return false;
	}

	public int CountSegments()
	{
		int num = 0;
		if (this.m_segment0 != 0)
		{
			num++;
		}
		if (this.m_segment1 != 0)
		{
			num++;
		}
		if (this.m_segment2 != 0)
		{
			num++;
		}
		if (this.m_segment3 != 0)
		{
			num++;
		}
		if (this.m_segment4 != 0)
		{
			num++;
		}
		if (this.m_segment5 != 0)
		{
			num++;
		}
		if (this.m_segment6 != 0)
		{
			num++;
		}
		if (this.m_segment7 != 0)
		{
			num++;
		}
		return num;
	}

	public ushort GetSegment(int index)
	{
		switch (index)
		{
		case 0:
			return this.m_segment0;
		case 1:
			return this.m_segment1;
		case 2:
			return this.m_segment2;
		case 3:
			return this.m_segment3;
		case 4:
			return this.m_segment4;
		case 5:
			return this.m_segment5;
		case 6:
			return this.m_segment6;
		case 7:
			return this.m_segment7;
		default:
			return 0;
		}
	}

	public int CountSegments(NetSegment.Flags flags, ushort ignoreSegment)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = 0;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0 && segment != ignoreSegment && (instance.m_segments.m_buffer[(int)segment].m_flags & flags) != NetSegment.Flags.None)
			{
				num++;
			}
		}
		return num;
	}

	public bool IsConnectedTo(ushort node)
	{
		if (node == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
				if (startNode == node || endNode == node)
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool IsConnectedTo(ushort node1, ushort node2)
	{
		if (node1 == 0 && node2 == 0)
		{
			return false;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
				if (startNode == node1 || endNode == node1 || startNode == node2 || endNode == node2)
				{
					return true;
				}
			}
		}
		return false;
	}

	public void CalculateNode(ushort nodeID)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 vector = Vector3.get_zero();
		int num = 0;
		int num2 = 0;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		bool flag4 = false;
		bool flag5 = false;
		bool flag6 = false;
		bool flag7 = false;
		bool flag8 = false;
		bool flag9 = false;
		bool flag10 = true;
		bool flag11 = true;
		bool flag12 = Singleton<TerrainManager>.get_instance().HasDetailMapping(this.m_position);
		NetInfo with = null;
		int num3 = 0;
		int num4 = 0;
		NetInfo netInfo = null;
		float num5 = -1E+07f;
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				float nodeInfoPriority = info.m_netAI.GetNodeInfoPriority(segment, ref instance.m_segments.m_buffer[(int)segment]);
				if (nodeInfoPriority > num5)
				{
					netInfo = info;
					num5 = nodeInfoPriority;
				}
			}
		}
		if (netInfo == null)
		{
			netInfo = this.Info;
		}
		if (netInfo != this.Info)
		{
			this.Info = netInfo;
			Singleton<NetManager>.get_instance().UpdateNodeColors(nodeID);
			if (!netInfo.m_canDisable)
			{
				this.m_flags &= ~NetNode.Flags.Disabled;
			}
		}
		bool flag13 = false;
		for (int j = 0; j < 8; j++)
		{
			ushort segment2 = this.GetSegment(j);
			if (segment2 != 0)
			{
				num++;
				ushort startNode = instance.m_segments.m_buffer[(int)segment2].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)segment2].m_endNode;
				Vector3 startDirection = instance.m_segments.m_buffer[(int)segment2].m_startDirection;
				Vector3 endDirection = instance.m_segments.m_buffer[(int)segment2].m_endDirection;
				bool flag14 = nodeID == startNode;
				Vector3 vector2 = (!flag14) ? endDirection : startDirection;
				NetInfo info2 = instance.m_segments.m_buffer[(int)segment2].Info;
				ItemClass connectionClass = info2.GetConnectionClass();
				if (!info2.m_netAI.CanModify())
				{
					flag11 = false;
				}
				int num6;
				int num7;
				if (flag14 == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
				{
					num6 = info2.m_backwardVehicleLaneCount;
					num7 = info2.m_forwardVehicleLaneCount;
				}
				else
				{
					num6 = info2.m_forwardVehicleLaneCount;
					num7 = info2.m_backwardVehicleLaneCount;
				}
				for (int k = j + 1; k < 8; k++)
				{
					ushort segment3 = this.GetSegment(k);
					if (segment3 != 0)
					{
						NetInfo info3 = instance.m_segments.m_buffer[(int)segment3].Info;
						ItemClass connectionClass2 = info3.GetConnectionClass();
						if (connectionClass2.m_service == connectionClass.m_service || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None)
						{
							bool flag15 = nodeID == instance.m_segments.m_buffer[(int)segment3].m_startNode;
							Vector3 vector3 = (!flag15) ? instance.m_segments.m_buffer[(int)segment3].m_endDirection : instance.m_segments.m_buffer[(int)segment3].m_startDirection;
							float num8 = vector2.x * vector3.x + vector2.z * vector3.z;
							float num9 = 0.01f - Mathf.Min(info2.m_maxTurnAngleCos, info3.m_maxTurnAngleCos);
							if (num8 < num9)
							{
								if ((info2.m_requireDirectRenderers && (info2.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None)) || (info3.m_requireDirectRenderers && (info3.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None)))
								{
									num2++;
								}
							}
							else
							{
								flag6 = true;
							}
						}
						else
						{
							flag6 = true;
						}
					}
				}
				if (instance.m_nodes.m_buffer[(int)startNode].m_elevation != instance.m_nodes.m_buffer[(int)endNode].m_elevation)
				{
					flag10 = false;
				}
				Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				if (flag14)
				{
					flag12 = (flag12 && Singleton<TerrainManager>.get_instance().HasDetailMapping(position2));
				}
				else
				{
					flag12 = (flag12 && Singleton<TerrainManager>.get_instance().HasDetailMapping(position));
				}
				if (NetSegment.IsStraight(position, startDirection, position2, endDirection))
				{
					flag8 = true;
				}
				else
				{
					flag7 = true;
				}
				if (num == 1)
				{
					flag13 = flag14;
					vector = vector2;
					flag = true;
				}
				else if (num == 2 && info2.IsCombatible(with) && info2.IsCombatible(netInfo) && num6 != 0 == (num4 != 0) && num7 != 0 == (num3 != 0))
				{
					float num10 = vector.x * vector2.x + vector.z * vector2.z;
					if (num6 != num4 || num7 != num3)
					{
						if (num6 > num7)
						{
							flag4 = true;
							flag3 = true;
						}
						else
						{
							flag5 = true;
							flag3 = true;
						}
					}
					else if (num10 < -0.999f)
					{
						flag2 = true;
					}
					else
					{
						flag3 = true;
					}
					flag9 = (flag14 != flag13);
				}
				else
				{
					flag6 = true;
				}
				with = info2;
				num3 = num6;
				num4 = num7;
			}
		}
		if (!netInfo.m_enableMiddleNodes & flag2)
		{
			flag3 = true;
		}
		if (!netInfo.m_enableBendingNodes & flag3)
		{
			flag6 = true;
		}
		if (netInfo.m_requireContinuous && (this.m_flags & NetNode.Flags.Untouchable) != NetNode.Flags.None)
		{
			flag6 = true;
		}
		if (netInfo.m_requireContinuous && !flag9 && (flag2 || flag3))
		{
			flag6 = true;
		}
		NetNode.Flags flags = this.m_flags & ~(NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Junction | NetNode.Flags.Moveable | NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward);
		if ((flags & NetNode.Flags.Outside) != NetNode.Flags.None)
		{
			this.m_flags = flags;
		}
		else if (flag6)
		{
			this.m_flags = (flags | NetNode.Flags.Junction);
		}
		else if (flag3)
		{
			if (flag4)
			{
				flags |= NetNode.Flags.AsymForward;
			}
			if (flag5)
			{
				flags |= NetNode.Flags.AsymBackward;
			}
			this.m_flags = (flags | NetNode.Flags.Bend);
		}
		else if (flag2)
		{
			if ((!flag7 || !flag8) && (this.m_flags & (NetNode.Flags.Untouchable | NetNode.Flags.Double)) == NetNode.Flags.None && flag10 && flag11)
			{
				flags |= NetNode.Flags.Moveable;
			}
			this.m_flags = (flags | NetNode.Flags.Middle);
		}
		else if (flag)
		{
			if ((this.m_flags & NetNode.Flags.Untouchable) == NetNode.Flags.None && flag10 && flag11 && netInfo.m_enableMiddleNodes)
			{
				flags |= NetNode.Flags.Moveable;
			}
			this.m_flags = (flags | NetNode.Flags.End);
		}
		this.m_heightOffset = ((!flag12 && netInfo.m_requireSurfaceMaps) ? 64 : 0);
		this.m_connectCount = (byte)num2;
		BuildingInfo newBuilding;
		float heightOffset;
		netInfo.m_netAI.GetNodeBuilding(nodeID, ref this, out newBuilding, out heightOffset);
		this.UpdateBuilding(nodeID, newBuilding, heightOffset);
	}

	public void UpdateBuilding(ushort nodeID, BuildingInfo newBuilding, float heightOffset)
	{
		float num = 0f;
		if (newBuilding != null)
		{
			NetInfo info = this.Info;
			if (info != null)
			{
				num = info.m_netAI.GetNodeBuildingAngle(nodeID, ref this);
			}
		}
		BuildingInfo buildingInfo = null;
		if (this.m_building != 0)
		{
			buildingInfo = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)this.m_building].Info;
		}
		if (newBuilding != buildingInfo)
		{
			if (this.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().ReleaseBuilding(this.m_building);
				this.m_building = 0;
			}
			if (newBuilding != null)
			{
				Vector3 position = this.m_position;
				position.y += heightOffset;
				num *= 6.28318548f;
				if (buildingInfo == null && !this.TestNodeBuilding(nodeID, newBuilding, position, num))
				{
					return;
				}
				Randomizer randomizer;
				randomizer..ctor((int)nodeID);
				if (Singleton<BuildingManager>.get_instance().CreateBuilding(out this.m_building, ref randomizer, newBuilding, position, num, 0, this.m_buildIndex + 1u))
				{
					Building[] expr_FC_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
					ushort expr_FC_cp_1 = this.m_building;
					expr_FC_cp_0[(int)expr_FC_cp_1].m_flags = (expr_FC_cp_0[(int)expr_FC_cp_1].m_flags | (Building.Flags.Untouchable | Building.Flags.FixedHeight));
				}
			}
		}
		else if (this.m_building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Vector3 position2 = this.m_position;
			position2.y += heightOffset;
			num *= 6.28318548f;
			if (instance.m_buildings.m_buffer[(int)this.m_building].m_position.y != position2.y || instance.m_buildings.m_buffer[(int)this.m_building].m_angle != num)
			{
				instance.m_buildings.m_buffer[(int)this.m_building].m_position.y = position2.y;
				instance.m_buildings.m_buffer[(int)this.m_building].m_angle = num;
				instance.UpdateBuilding(this.m_building);
			}
		}
	}

	private bool TestNodeBuilding(ushort nodeID, BuildingInfo info, Vector3 position, float angle)
	{
		Vector2 vector = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle));
		Vector2 vector2 = new Vector3(vector.y, -vector.x);
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			vector *= (float)info.m_cellWidth * 4f - 0.8f;
			vector2 *= (float)info.m_cellLength * 4f - 0.8f;
		}
		else
		{
			vector *= (float)info.m_cellWidth * 4f;
			vector2 *= (float)info.m_cellLength * 4f;
		}
		if (info.m_circular)
		{
			vector *= 0.7f;
			vector2 *= 0.7f;
		}
		ItemClass.CollisionType collisionType = info.m_buildingAI.GetCollisionType();
		Vector2 vector3 = VectorUtils.XZ(position);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - vector - vector2;
		quad.b = vector3 - vector + vector2;
		quad.c = vector3 + vector + vector2;
		quad.d = vector3 + vector - vector2;
		float minY = Mathf.Min(position.y, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(position));
		float maxY = position.y + info.m_generatedInfo.m_size.y;
		return !Singleton<NetManager>.get_instance().OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, nodeID, 0, 0, null) && !Singleton<BuildingManager>.get_instance().OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, 0, nodeID, 0, null);
	}

	public void CountLanes(ushort nodeID, ushort ignoreSegment, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, bool onePerSegment, ref int forward, ref int backward)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0 && segment != ignoreSegment)
			{
				int num = 0;
				int num2 = 0;
				instance.m_segments.m_buffer[(int)segment].CountLanes(segment, laneTypes, vehicleTypes, ref num, ref num2);
				if (onePerSegment)
				{
					if (num != 0)
					{
						num = 1;
					}
					if (num2 != 0)
					{
						num2 = 1;
					}
				}
				if (instance.m_segments.m_buffer[(int)segment].m_endNode == nodeID)
				{
					forward += num2;
					backward += num;
				}
				else
				{
					forward += num;
					backward += num2;
				}
			}
		}
	}

	public void CountLanes(ushort nodeID, ushort ignoreSegment, NetInfo.Direction directions, NetInfo.LaneType laneTypes, VehicleInfo.VehicleType vehicleTypes, Vector3 direction, ref int left, ref int forward, ref int right, ref int left2, ref int forward2, ref int right2)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0 && segment != ignoreSegment)
			{
				NetInfo.Direction direction2 = directions;
				if (instance.m_segments.m_buffer[(int)segment].m_endNode == nodeID)
				{
					if (direction2 == NetInfo.Direction.Forward)
					{
						direction2 = NetInfo.Direction.Backward;
					}
					else if (direction2 == NetInfo.Direction.Backward)
					{
						direction2 = NetInfo.Direction.Forward;
					}
				}
				instance.m_segments.m_buffer[(int)segment].CountLanes(segment, direction2, laneTypes, vehicleTypes, direction, ref left, ref forward, ref right, ref left2, ref forward2, ref right2);
			}
		}
	}

	public void UpdateLaneConnection(ushort nodeID)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		info.m_netAI.UpdateLaneConnection(nodeID, ref this);
	}

	public void UpdateBounds(ushort nodeID)
	{
		if (this.m_flags != NetNode.Flags.None)
		{
			NetInfo info = this.Info;
			if (info == null)
			{
				return;
			}
			Vector3 vector = this.m_position + new Vector3(-info.m_halfWidth, info.m_minHeight, -info.m_halfWidth);
			Vector3 vector2 = this.m_position + new Vector3(info.m_halfWidth, info.m_maxHeight, info.m_halfWidth);
			if ((this.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = this.GetSegment(i);
					if (segment != 0)
					{
						NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
						if (netSegment.Info != null)
						{
							Vector3 zero = Vector3.get_zero();
							Vector3 zero2 = Vector3.get_zero();
							Vector3 zero3 = Vector3.get_zero();
							Vector3 zero4 = Vector3.get_zero();
							Vector3 vector3 = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
							float num = -4f;
							ushort num2 = 0;
							for (int j = 0; j < 8; j++)
							{
								ushort segment2 = this.GetSegment(j);
								if (segment2 != 0 && segment2 != segment)
								{
									NetSegment netSegment2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2];
									if (netSegment2.Info != null)
									{
										Vector3 vector4 = (nodeID != netSegment2.m_startNode) ? netSegment2.m_endDirection : netSegment2.m_startDirection;
										float num3 = vector3.x * vector4.x + vector3.z * vector4.z;
										if (vector4.z * vector3.x - vector4.x * vector3.z < 0f)
										{
											if (num3 > num)
											{
												num = num3;
												num2 = segment2;
											}
										}
										else
										{
											num3 = -2f - num3;
											if (num3 > num)
											{
												num = num3;
												num2 = segment2;
											}
										}
									}
								}
							}
							bool start = netSegment.m_startNode == nodeID;
							bool flag;
							netSegment.CalculateCorner(segment, true, start, false, out zero, out zero2, out flag);
							if (num2 != 0)
							{
								NetSegment netSegment3 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2];
								start = (netSegment3.m_startNode == nodeID);
								netSegment3.CalculateCorner(num2, true, start, true, out zero3, out zero4, out flag);
							}
							Vector3 vector5;
							Vector3 vector6;
							NetSegment.CalculateMiddlePoints(zero, -zero2, zero3, -zero4, true, true, out vector5, out vector6);
							vector = Vector3.Min(vector, Vector3.Min(Vector3.Min(zero, vector5), Vector3.Min(vector6, zero3)));
							vector2 = Vector3.Max(vector2, Vector3.Max(Vector3.Max(zero, vector5), Vector3.Max(vector6, zero3)));
						}
					}
				}
			}
			else if ((this.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None)
			{
				Vector3 zero5 = Vector3.get_zero();
				Vector3 zero6 = Vector3.get_zero();
				Vector3 zero7 = Vector3.get_zero();
				Vector3 zero8 = Vector3.get_zero();
				Vector3 zero9 = Vector3.get_zero();
				Vector3 zero10 = Vector3.get_zero();
				Vector3 zero11 = Vector3.get_zero();
				Vector3 zero12 = Vector3.get_zero();
				int num4 = 0;
				for (int k = 0; k < 8; k++)
				{
					ushort segment3 = this.GetSegment(k);
					if (segment3 != 0)
					{
						NetSegment netSegment4 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment3];
						bool start2 = netSegment4.m_startNode == nodeID;
						if (++num4 == 1)
						{
							bool flag2;
							netSegment4.CalculateCorner(segment3, true, start2, false, out zero5, out zero9, out flag2);
							netSegment4.CalculateCorner(segment3, true, start2, true, out zero6, out zero10, out flag2);
						}
						else
						{
							bool flag2;
							netSegment4.CalculateCorner(segment3, true, start2, true, out zero7, out zero11, out flag2);
							netSegment4.CalculateCorner(segment3, true, start2, false, out zero8, out zero12, out flag2);
						}
					}
				}
				Vector3 vector7;
				Vector3 vector8;
				NetSegment.CalculateMiddlePoints(zero5, -zero9, zero7, -zero11, true, true, out vector7, out vector8);
				Vector3 vector9;
				Vector3 vector10;
				NetSegment.CalculateMiddlePoints(zero6, -zero10, zero8, -zero12, true, true, out vector9, out vector10);
				vector = Vector3.Min(Vector3.Min(Vector3.Min(zero5, zero6), Vector3.Min(vector7, vector9)), Vector3.Min(Vector3.Min(vector8, vector10), Vector3.Min(zero7, zero8)));
				vector2 = Vector3.Max(Vector3.Max(Vector3.Max(zero5, zero6), Vector3.Max(vector7, vector9)), Vector3.Max(Vector3.Max(vector8, vector10), Vector3.Max(zero7, zero8)));
			}
			else if ((this.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				Vector3 zero13 = Vector3.get_zero();
				Vector3 zero14 = Vector3.get_zero();
				Vector3 vector11 = Vector3.get_zero();
				Vector3 vector12 = Vector3.get_zero();
				Vector3 zero15 = Vector3.get_zero();
				Vector3 zero16 = Vector3.get_zero();
				Vector3 vector13 = Vector3.get_zero();
				Vector3 vector14 = Vector3.get_zero();
				for (int l = 0; l < 8; l++)
				{
					ushort segment4 = this.GetSegment(l);
					if (segment4 != 0)
					{
						NetSegment netSegment5 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment4];
						bool start3 = netSegment5.m_startNode == nodeID;
						bool flag3;
						netSegment5.CalculateCorner(segment4, true, start3, false, out zero13, out zero15, out flag3);
						netSegment5.CalculateCorner(segment4, true, start3, true, out zero14, out zero16, out flag3);
						vector11 = zero14;
						vector12 = zero13;
						vector13 = zero16;
						vector14 = zero15;
					}
				}
				float num5 = info.m_netAI.GetEndRadius() * 1.33333337f;
				Vector3 vector15 = zero13 - zero15 * num5;
				Vector3 vector16 = vector11 - vector13 * num5;
				Vector3 vector17 = zero14 + zero16 * num5;
				Vector3 vector18 = vector12 + vector14 * num5;
				vector = Vector3.Min(Vector3.Min(Vector3.Min(zero13, zero14), Vector3.Min(vector15, vector17)), Vector3.Min(Vector3.Min(vector16, vector18), Vector3.Min(vector11, vector12)));
				vector2 = Vector3.Max(Vector3.Max(Vector3.Max(zero13, zero14), Vector3.Max(vector15, vector17)), Vector3.Max(Vector3.Max(vector16, vector18), Vector3.Max(vector11, vector12)));
			}
			this.m_bounds.SetMinMax(vector, vector2);
		}
	}

	public void UpdateNode(ushort nodeID)
	{
		if (this.m_flags == NetNode.Flags.None)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		if (info.m_flattenTerrain || info.m_lowerTerrain || info.m_netAI.RaiseTerrain() || info.m_createPavement || info.m_createGravel || info.m_createRuining || info.m_clipTerrain)
		{
			TerrainModify.UpdateArea(this.m_bounds.get_min().x, this.m_bounds.get_min().z, this.m_bounds.get_max().x, this.m_bounds.get_max().z, info.m_flattenTerrain || info.m_lowerTerrain || info.m_netAI.RaiseTerrain(), info.m_createPavement || info.m_createGravel || info.m_createRuining || info.m_clipTerrain, false);
		}
		info.m_netAI.UpdateNode(nodeID, ref this);
	}

	private void CheckHeightOffset(ushort nodeID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		bool flag = Singleton<TerrainManager>.get_instance().HasDetailMapping(this.m_position);
		for (int i = 0; i < 8; i++)
		{
			ushort segment = this.GetSegment(i);
			if (segment != 0)
			{
				ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
				if (startNode == nodeID)
				{
					Vector3 position = instance.m_nodes.m_buffer[(int)endNode].m_position;
					flag = (flag && Singleton<TerrainManager>.get_instance().HasDetailMapping(position));
				}
				else
				{
					Vector3 position2 = instance.m_nodes.m_buffer[(int)startNode].m_position;
					flag = (flag && Singleton<TerrainManager>.get_instance().HasDetailMapping(position2));
				}
			}
		}
		byte b = (!flag && info.m_requireSurfaceMaps) ? 64 : 0;
		if (b != this.m_heightOffset)
		{
			this.m_heightOffset = b;
			BuildingInfo newBuilding;
			float heightOffset;
			info.m_netAI.GetNodeBuilding(nodeID, ref this, out newBuilding, out heightOffset);
			this.UpdateBuilding(nodeID, newBuilding, heightOffset);
			instance.UpdateNodeFlags(nodeID);
			instance.UpdateNodeRenderer(nodeID, true);
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = this.GetSegment(j);
				if (segment2 != 0)
				{
					instance.m_segments.m_buffer[(int)segment2].UpdateLanes(segment2, false);
					instance.UpdateSegmentFlags(segment2);
					instance.UpdateSegmentRenderer(segment2, true);
				}
			}
		}
	}

	public void TerrainUpdated(ushort nodeID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) != NetNode.Flags.Created)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		byte b = (!Singleton<TerrainManager>.get_instance().HasDetailMapping(this.m_position) && info.m_requireSurfaceMaps) ? 64 : 0;
		if (b != this.m_heightOffset)
		{
			this.CheckHeightOffset(nodeID);
			NetManager instance = Singleton<NetManager>.get_instance();
			for (int i = 0; i < 8; i++)
			{
				ushort segment = this.GetSegment(i);
				if (segment != 0)
				{
					ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
					ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
					if (startNode == nodeID)
					{
						instance.m_nodes.m_buffer[(int)endNode].CheckHeightOffset(endNode);
					}
					else
					{
						instance.m_nodes.m_buffer[(int)startNode].CheckHeightOffset(startNode);
					}
				}
			}
		}
		bool flag;
		bool flag2;
		bool flag3;
		bool flag4;
		bool flag5;
		bool flag6;
		if ((this.m_flags & NetNode.Flags.Underground) != NetNode.Flags.None)
		{
			flag = false;
			flag2 = false;
			flag3 = false;
			flag4 = false;
			flag5 = false;
			flag6 = info.m_netAI.RaiseTerrain();
		}
		else
		{
			flag = (info.m_createPavement && (!info.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None));
			flag2 = (info.m_createGravel && (!info.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None));
			flag3 = (info.m_createRuining && (!info.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None));
			flag4 = (info.m_clipTerrain && (!info.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None) && info.m_netAI.CanClipNodes());
			flag5 = (info.m_flattenTerrain || (info.m_netAI.FlattenGroundNodes() && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None));
			flag6 = false;
		}
		if (flag5 || info.m_lowerTerrain || flag6 || flag || flag2 || flag3 || flag4)
		{
			for (int j = 0; j < 8; j++)
			{
				ushort segment2 = this.GetSegment(j);
				if (segment2 != 0)
				{
					if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
					{
						return;
					}
					ushort startNode2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_startNode;
					ushort endNode2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_endNode;
					if (startNode2 == nodeID)
					{
						for (int k = 0; k < 8; k++)
						{
							ushort segment3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endNode2].GetSegment(k);
							if (segment3 != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment3].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
							{
								return;
							}
						}
					}
					else
					{
						for (int l = 0; l < 8; l++)
						{
							ushort segment4 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startNode2].GetSegment(l);
							if (segment4 != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment4].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) != NetSegment.Flags.Created)
							{
								return;
							}
						}
					}
				}
			}
			ushort num = 0;
			if ((this.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
			{
				Vector3 vector = this.m_position;
				int num2 = 0;
				for (int m = 0; m < 8; m++)
				{
					ushort segment5 = this.GetSegment(m);
					if (segment5 != 0)
					{
						NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment5];
						NetInfo info2 = netSegment.Info;
						if (info2 != null && info2.m_netAI.GetSnapElevation() <= info.m_netAI.GetSnapElevation())
						{
							ItemClass connectionClass = info2.GetConnectionClass();
							Vector3 vector2 = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
							float num3 = -1f;
							for (int n = 0; n < 8; n++)
							{
								ushort segment6 = this.GetSegment(n);
								if (segment6 != 0 && segment6 != segment5)
								{
									NetSegment netSegment2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment6];
									NetInfo info3 = netSegment2.Info;
									if (info3 != null && info3.m_netAI.GetSnapElevation() <= info.m_netAI.GetSnapElevation())
									{
										ItemClass connectionClass2 = info3.GetConnectionClass();
										if (connectionClass.m_service == connectionClass2.m_service)
										{
											Vector3 vector3 = (nodeID != netSegment2.m_startNode) ? netSegment2.m_endDirection : netSegment2.m_startDirection;
											num3 = Mathf.Max(num3, vector2.x * vector3.x + vector2.z * vector3.z);
										}
									}
								}
							}
							vector += vector2 * (2f + num3 * 2f);
							num2++;
							num = segment5;
						}
					}
				}
				vector.y = this.m_position.y;
				if (num2 > 1)
				{
					num = 0;
					for (int num4 = 0; num4 < 8; num4++)
					{
						ushort segment7 = this.GetSegment(num4);
						if (segment7 != 0)
						{
							NetSegment netSegment3 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment7];
							NetInfo info4 = netSegment3.Info;
							if (info4 != null && info4.m_netAI.GetSnapElevation() <= info.m_netAI.GetSnapElevation())
							{
								Bezier3 bezier = default(Bezier3);
								Segment3 segment8 = default(Segment3);
								Vector3 zero = Vector3.get_zero();
								Vector3 zero2 = Vector3.get_zero();
								Vector3 vector4 = Vector3.get_zero();
								Vector3 vector5 = Vector3.get_zero();
								ItemClass connectionClass3 = info4.GetConnectionClass();
								Vector3 vector6 = (nodeID != netSegment3.m_startNode) ? netSegment3.m_endDirection : netSegment3.m_startDirection;
								float num5 = -4f;
								ushort num6 = 0;
								for (int num7 = 0; num7 < 8; num7++)
								{
									ushort segment9 = this.GetSegment(num7);
									if (segment9 != 0 && segment9 != segment7)
									{
										NetSegment netSegment4 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment9];
										NetInfo info5 = netSegment4.Info;
										if (info5 != null && info5.m_netAI.GetSnapElevation() <= info.m_netAI.GetSnapElevation())
										{
											ItemClass connectionClass4 = info5.GetConnectionClass();
											if (connectionClass3.m_service == connectionClass4.m_service)
											{
												Vector3 vector7 = (nodeID != netSegment4.m_startNode) ? netSegment4.m_endDirection : netSegment4.m_startDirection;
												float num8 = vector6.x * vector7.x + vector6.z * vector7.z;
												if (vector7.z * vector6.x - vector7.x * vector6.z < 0f)
												{
													if (num8 > num5)
													{
														num5 = num8;
														num6 = segment9;
													}
												}
												else
												{
													num8 = -2f - num8;
													if (num8 > num5)
													{
														num5 = num8;
														num6 = segment9;
													}
												}
											}
										}
									}
								}
								bool start = netSegment3.m_startNode == nodeID;
								bool flag7;
								netSegment3.CalculateCorner(segment7, false, start, false, out bezier.a, out zero, out flag7);
								netSegment3.CalculateCorner(segment7, false, start, true, out segment8.a, out zero2, out flag7);
								if (num6 != 0)
								{
									NetSegment netSegment5 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num6];
									NetInfo info6 = netSegment5.Info;
									start = (netSegment5.m_startNode == nodeID);
									netSegment5.CalculateCorner(num6, false, start, true, out bezier.d, out vector4, out flag7);
									netSegment5.CalculateCorner(num6, false, start, false, out segment8.b, out vector5, out flag7);
									NetSegment.CalculateMiddlePoints(bezier.a, -zero, bezier.d, -vector4, true, true, out bezier.b, out bezier.c);
									segment8.a = (bezier.a + segment8.a) * 0.5f;
									segment8.b = (bezier.d + segment8.b) * 0.5f;
									Vector3 vector8 = Vector3.Min(vector, Vector3.Min(bezier.Min(), segment8.Min()));
									Vector3 vector9 = Vector3.Max(vector, Vector3.Max(bezier.Max(), segment8.Max()));
									if (vector8.x <= maxX && vector8.z <= maxZ && minX <= vector9.x && minZ <= vector9.z)
									{
										float num9 = Vector3.Distance(bezier.a, bezier.b);
										float num10 = Vector3.Distance(bezier.b, bezier.c);
										float num11 = Vector3.Distance(bezier.c, bezier.d);
										Vector3 vector10 = (bezier.a - bezier.b) * (1f / Mathf.Max(0.1f, num9));
										Vector3 vector11 = (bezier.c - bezier.b) * (1f / Mathf.Max(0.1f, num10));
										Vector3 vector12 = (bezier.d - bezier.c) * (1f / Mathf.Max(0.1f, num11));
										float num12 = Mathf.Min(Vector3.Dot(vector10, vector11), Vector3.Dot(vector11, vector12));
										num9 += num10 + num11;
										int num13 = Mathf.Clamp(Mathf.CeilToInt(Mathf.Min(num9 * 0.125f, 50f - num12 * 50f)) * 2, 2, 16);
										Vector3 vector13 = bezier.a;
										Vector3 vector14 = segment8.a;
										for (int num14 = 1; num14 <= num13; num14++)
										{
											NetInfo netInfo = (num14 > num13 >> 1) ? info6 : info4;
											Vector3 vector15 = bezier.Position((float)num14 / (float)num13);
											Vector3 vector16;
											if (num14 <= num13 >> 1)
											{
												vector16 = segment8.a + (vector - segment8.a) * ((float)num14 / (float)num13 * 2f);
											}
											else
											{
												vector16 = vector + (segment8.b - vector) * ((float)num14 / (float)num13 * 2f - 1f);
											}
											bool flag8 = netInfo.m_createPavement && (!netInfo.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
											bool flag9 = netInfo.m_createGravel && (!netInfo.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
											bool flag10 = netInfo.m_createRuining && (!netInfo.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
											bool flag11 = netInfo.m_clipTerrain && (!netInfo.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None) && netInfo.m_netAI.CanClipNodes();
											bool flag12 = netInfo.m_flattenTerrain || (netInfo.m_netAI.FlattenGroundNodes() && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
											Vector3 vector17 = vector13;
											Vector3 vector18 = vector15;
											Vector3 vector19 = vector16;
											Vector3 vector20 = vector14;
											TerrainModify.Heights heights = TerrainModify.Heights.None;
											TerrainModify.Surface surface = TerrainModify.Surface.None;
											if (flag6)
											{
												heights = TerrainModify.Heights.SecondaryMin;
											}
											else
											{
												if (flag5 || flag12)
												{
													heights |= TerrainModify.Heights.PrimaryLevel;
												}
												if (info.m_lowerTerrain || netInfo.m_lowerTerrain)
												{
													heights |= TerrainModify.Heights.PrimaryMax;
												}
												if (info.m_blockWater || netInfo.m_blockWater)
												{
													heights |= TerrainModify.Heights.BlockHeight;
												}
												if (flag8)
												{
													surface |= TerrainModify.Surface.PavementA;
												}
												if (flag2 || flag9)
												{
													surface |= TerrainModify.Surface.Gravel;
												}
												if (flag3 || flag10)
												{
													surface |= TerrainModify.Surface.Ruined;
												}
												if (flag4 || flag11)
												{
													surface |= TerrainModify.Surface.Clip;
												}
											}
											TerrainModify.Edges edges = TerrainModify.Edges.All;
											float num15 = 0f;
											float num16 = 1f;
											float num17 = 0f;
											float num18 = 0f;
											int num19 = 0;
											while (netInfo.m_netAI.NodeModifyMask(nodeID, ref this, segment7, num6, num19, ref surface, ref heights, ref edges, ref num15, ref num16, ref num17, ref num18))
											{
												if (num15 < 0.5f)
												{
													TerrainModify.Edges edges2 = TerrainModify.Edges.AB;
													if (num15 != 0f || num16 != 1f || num19 != 0)
													{
														if (num15 != 0f)
														{
															float num20 = 2f * num15 * netInfo.m_halfWidth / Vector3.Distance(vector17, vector20);
															float num21 = 2f * num15 * netInfo.m_halfWidth / Vector3.Distance(vector18, vector19);
															vector13 = Vector3.Lerp(vector17, vector20, num20);
															vector15 = Vector3.Lerp(vector18, vector19, num21);
														}
														else
														{
															vector13 = vector17;
															vector15 = vector18;
														}
														if (num16 < 0.5f)
														{
															edges2 |= TerrainModify.Edges.CD;
															float num22 = 2f * num16 * netInfo.m_halfWidth / Vector3.Distance(vector17, vector20);
															float num23 = 2f * num16 * netInfo.m_halfWidth / Vector3.Distance(vector18, vector19);
															vector14 = Vector3.Lerp(vector17, vector20, num22);
															vector16 = Vector3.Lerp(vector18, vector19, num23);
														}
														else
														{
															vector14 = vector20;
															vector16 = vector19;
														}
													}
													vector13.y += num17;
													vector14.y += num18;
													vector15.y += num17;
													vector16.y += num18;
													Vector3 zero3 = Vector3.get_zero();
													Vector3 zero4 = Vector3.get_zero();
													if (flag6)
													{
														zero3.y += info.m_maxHeight;
														zero4.y += info.m_maxHeight;
													}
													else if (netInfo.m_lowerTerrain)
													{
														if (!info.m_lowerTerrain)
														{
															if (num14 == 1)
															{
																TerrainModify.Edges edges3 = edges2 | TerrainModify.Edges.DA;
																TerrainModify.ApplyQuad(vector13, vector15, vector16, vector14, edges3, TerrainModify.Heights.None, surface);
																surface = TerrainModify.Surface.None;
															}
															else if (num14 == num13)
															{
																TerrainModify.Edges edges4 = edges2 | TerrainModify.Edges.BC;
																TerrainModify.ApplyQuad(vector13, vector15, vector16, vector14, edges4, TerrainModify.Heights.None, surface);
																surface = TerrainModify.Surface.None;
															}
															zero3.y += (float)Mathf.Abs(num14 - 1 - (num13 >> 1)) * (1f / (float)num13) * netInfo.m_netAI.GetTerrainLowerOffset();
															zero4.y += (float)Mathf.Abs(num14 - (num13 >> 1)) * (1f / (float)num13) * netInfo.m_netAI.GetTerrainLowerOffset();
														}
														else
														{
															if ((this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None)
															{
																if (num14 == 1)
																{
																	edges2 |= TerrainModify.Edges.DA;
																}
																else if (num14 == num13)
																{
																	edges2 |= TerrainModify.Edges.BC;
																}
															}
															zero3.y += netInfo.m_netAI.GetTerrainLowerOffset();
															zero4.y += netInfo.m_netAI.GetTerrainLowerOffset();
														}
													}
													edges2 &= edges;
													TerrainModify.Surface surface2 = surface;
													if ((surface2 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
													{
														surface2 |= TerrainModify.Surface.Gravel;
													}
													TerrainModify.ApplyQuad(vector13 + zero3, vector15 + zero4, vector16 + zero4, vector14 + zero3, edges2, heights, surface2);
												}
												num19++;
											}
											vector13 = vector18;
											vector14 = vector19;
										}
									}
								}
								else
								{
									Vector3 vector21 = bezier.a;
									Vector3 vector22 = segment8.a;
									Vector3 vector23 = Vector3.get_zero();
									Vector3 vector24 = Vector3.get_zero();
									Vector3 vector25 = vector21;
									Vector3 vector26 = vector22;
									Vector3 vector27 = vector23;
									Vector3 vector28 = vector24;
									bool flag13 = info4.m_createPavement && (!info4.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
									bool flag14 = info4.m_createGravel && (!info4.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
									bool flag15 = info4.m_createRuining && (!info4.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
									bool flag16 = info4.m_clipTerrain && (!info4.m_lowerTerrain || (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None) && info4.m_netAI.CanClipNodes();
									bool flag17 = info4.m_flattenTerrain || (info4.m_netAI.FlattenGroundNodes() && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None);
									TerrainModify.Heights heights2 = TerrainModify.Heights.None;
									TerrainModify.Surface surface3 = TerrainModify.Surface.None;
									if (flag6)
									{
										heights2 = TerrainModify.Heights.SecondaryMin;
									}
									else
									{
										if (flag17)
										{
											heights2 |= TerrainModify.Heights.PrimaryLevel;
										}
										if (info4.m_lowerTerrain)
										{
											heights2 |= TerrainModify.Heights.PrimaryMax;
										}
										if (info4.m_blockWater)
										{
											heights2 |= TerrainModify.Heights.BlockHeight;
										}
										if (flag13)
										{
											surface3 |= TerrainModify.Surface.PavementA;
										}
										if (flag14)
										{
											surface3 |= TerrainModify.Surface.Gravel;
										}
										if (flag15)
										{
											surface3 |= TerrainModify.Surface.Ruined;
										}
										if (flag16)
										{
											surface3 |= TerrainModify.Surface.Clip;
										}
									}
									TerrainModify.Edges edges5 = TerrainModify.Edges.All;
									float num24 = 0f;
									float num25 = 1f;
									float num26 = 0f;
									float num27 = 0f;
									int num28 = 0;
									while (info4.m_netAI.NodeModifyMask(nodeID, ref this, segment7, segment7, num28, ref surface3, ref heights2, ref edges5, ref num24, ref num25, ref num26, ref num27))
									{
										if (num24 != 0f || num25 != 1f || num28 != 0)
										{
											vector21 = Vector3.Lerp(vector25, vector26, num24);
											vector22 = Vector3.Lerp(vector25, vector26, num25);
											vector23 = Vector3.Lerp(vector27, vector28, num24);
											vector24 = Vector3.Lerp(vector27, vector28, num25);
										}
										vector21.y += num26;
										vector22.y += num27;
										vector23.y += num26;
										vector24.y += num27;
										if (info4.m_halfWidth < 3.999f)
										{
											vector23 = vector21 - zero * (info4.m_halfWidth + 2f);
											vector24 = vector22 - zero2 * (info4.m_halfWidth + 2f);
											float num29 = Mathf.Min(new float[]
											{
												Mathf.Min(Mathf.Min(vector21.x, vector22.x), Mathf.Min(vector23.x, vector24.x))
											});
											float num30 = Mathf.Max(new float[]
											{
												Mathf.Max(Mathf.Max(vector21.x, vector22.x), Mathf.Max(vector23.x, vector24.x))
											});
											float num31 = Mathf.Min(new float[]
											{
												Mathf.Min(Mathf.Min(vector21.z, vector22.z), Mathf.Min(vector23.z, vector24.z))
											});
											float num32 = Mathf.Max(new float[]
											{
												Mathf.Max(Mathf.Max(vector21.z, vector22.z), Mathf.Max(vector23.z, vector24.z))
											});
											if (num29 <= maxX && num31 <= maxZ && minX <= num30 && minZ <= num32)
											{
												TerrainModify.Edges edges6 = TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.CD;
												if (info4.m_lowerTerrain && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None)
												{
													edges6 |= TerrainModify.Edges.DA;
												}
												edges6 &= edges5;
												TerrainModify.Surface surface4 = surface3;
												if ((surface4 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
												{
													surface4 |= TerrainModify.Surface.Gravel;
												}
												Vector3 zero5 = Vector3.get_zero();
												if (flag6)
												{
													zero5.y += info4.m_maxHeight;
												}
												else if (info4.m_lowerTerrain)
												{
													zero5.y += info4.m_netAI.GetTerrainLowerOffset();
												}
												TerrainModify.ApplyQuad(vector21 + zero5, vector23 + zero5, vector24 + zero5, vector22 + zero5, edges6, heights2, surface4);
											}
										}
										else
										{
											vector23 = vector22;
											vector24 = vector21;
											vector4 = zero2;
											vector5 = zero;
											float num33 = info4.m_netAI.GetEndRadius() * 1.33333337f * 1.1f;
											Vector3 vector29 = vector21 - zero * num33;
											Vector3 vector30 = vector23 - vector4 * num33;
											Vector3 vector31 = vector22 + zero2 * num33;
											Vector3 vector32 = vector24 + vector5 * num33;
											float num34 = Mathf.Min(Mathf.Min(Mathf.Min(vector21.x, vector22.x), Mathf.Min(vector29.x, vector31.x)), Mathf.Min(Mathf.Min(vector30.x, vector32.x), Mathf.Min(vector23.x, vector24.x)));
											float num35 = Mathf.Max(Mathf.Max(Mathf.Max(vector21.x, vector22.x), Mathf.Max(vector29.x, vector31.x)), Mathf.Max(Mathf.Max(vector30.x, vector32.x), Mathf.Max(vector23.x, vector24.x)));
											float num36 = Mathf.Min(Mathf.Min(Mathf.Min(vector21.z, vector22.z), Mathf.Min(vector29.z, vector31.z)), Mathf.Min(Mathf.Min(vector30.z, vector32.z), Mathf.Min(vector23.z, vector24.z)));
											float num37 = Mathf.Max(Mathf.Max(Mathf.Max(vector21.z, vector22.z), Mathf.Max(vector29.z, vector31.z)), Mathf.Max(Mathf.Max(vector30.z, vector32.z), Mathf.Max(vector23.z, vector24.z)));
											if (num34 <= maxX && num36 <= maxZ && minX <= num35 && minZ <= num37)
											{
												int num38 = Mathf.Clamp(Mathf.CeilToInt(info4.m_halfWidth * 0.4f), 2, 8);
												Vector3 vector33 = vector21;
												Vector3 vector34 = (vector21 + vector22) * 0.5f;
												for (int num39 = 1; num39 <= num38; num39++)
												{
													Vector3 vector35 = Bezier3.Position(vector21, vector29, vector30, vector23, ((float)num39 - 0.5f) / (float)num38);
													Vector3 vector36 = Bezier3.Position(vector21, vector29, vector30, vector23, (float)num39 / (float)num38);
													TerrainModify.Edges edges7 = TerrainModify.Edges.AB | TerrainModify.Edges.BC;
													edges7 &= edges5;
													TerrainModify.Surface surface5 = surface3;
													if ((surface5 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
													{
														surface5 |= TerrainModify.Surface.Gravel;
													}
													Vector3 zero6 = Vector3.get_zero();
													if (flag6)
													{
														zero6.y += info4.m_maxHeight;
													}
													else if (info4.m_lowerTerrain)
													{
														zero6.y += info4.m_netAI.GetTerrainLowerOffset();
													}
													TerrainModify.ApplyQuad(vector33 + zero6, vector35 + zero6, vector36 + zero6, vector34 + zero6, edges7, heights2, surface5);
													vector33 = vector36;
												}
											}
										}
										num28++;
									}
								}
							}
						}
					}
					if (num2 == 8)
					{
						Vector3 vector37 = vector + Vector3.get_left() * 8f;
						Vector3 vector38 = vector + Vector3.get_back() * 8f;
						Vector3 vector39 = vector + Vector3.get_right() * 8f;
						Vector3 vector40 = vector + Vector3.get_forward() * 8f;
						Vector3 vector41 = vector37;
						Vector3 vector42 = vector38;
						Vector3 vector43 = vector39;
						Vector3 vector44 = vector40;
						TerrainModify.Heights heights3 = TerrainModify.Heights.None;
						TerrainModify.Surface surface6 = TerrainModify.Surface.None;
						if (flag6)
						{
							heights3 = TerrainModify.Heights.SecondaryMin;
						}
						else
						{
							if (flag5)
							{
								heights3 |= TerrainModify.Heights.PrimaryLevel;
							}
							if (info.m_lowerTerrain)
							{
								heights3 |= TerrainModify.Heights.PrimaryMax;
							}
							if (info.m_blockWater)
							{
								heights3 |= TerrainModify.Heights.BlockHeight;
							}
							if (flag)
							{
								surface6 |= TerrainModify.Surface.PavementA;
							}
							if (flag2)
							{
								surface6 |= TerrainModify.Surface.Gravel;
							}
							if (flag3)
							{
								surface6 |= TerrainModify.Surface.Ruined;
							}
							if (flag4)
							{
								surface6 |= TerrainModify.Surface.Clip;
							}
						}
						TerrainModify.Edges edges8 = TerrainModify.Edges.All;
						float num40 = 0f;
						float num41 = 1f;
						float num42 = 0f;
						float num43 = 0f;
						int num44 = 0;
						while (info.m_netAI.NodeModifyMask(nodeID, ref this, 0, 0, num44, ref surface6, ref heights3, ref edges8, ref num40, ref num41, ref num42, ref num43))
						{
							if (num44 != 0)
							{
								vector37 = vector41;
								vector38 = vector42;
								vector39 = vector43;
								vector40 = vector44;
							}
							vector37.y += (num42 + num43) * 0.5f;
							vector38.y += (num42 + num43) * 0.5f;
							vector39.y += (num42 + num43) * 0.5f;
							vector40.y += (num42 + num43) * 0.5f;
							TerrainModify.Edges edges9 = TerrainModify.Edges.All;
							edges9 &= edges8;
							TerrainModify.Surface surface7 = surface6;
							if ((surface7 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
							{
								surface7 |= TerrainModify.Surface.Gravel;
							}
							Vector3 zero7 = Vector3.get_zero();
							if (flag6)
							{
								zero7.y += info.m_maxHeight;
							}
							else if (info.m_lowerTerrain)
							{
								zero7.y += info.m_netAI.GetTerrainLowerOffset();
							}
							TerrainModify.ApplyQuad(vector37 + zero7, vector38 + zero7, vector39 + zero7, vector40 + zero7, edges9, heights3, surface7);
							num44++;
						}
					}
				}
			}
			else if ((this.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None)
			{
				Bezier3 bezier2 = default(Bezier3);
				Bezier3 bezier3 = default(Bezier3);
				Vector3 zero8 = Vector3.get_zero();
				Vector3 zero9 = Vector3.get_zero();
				Vector3 zero10 = Vector3.get_zero();
				Vector3 zero11 = Vector3.get_zero();
				ushort segment10 = 0;
				ushort num45 = 0;
				int num46 = 0;
				for (int num47 = 0; num47 < 8; num47++)
				{
					ushort segment11 = this.GetSegment(num47);
					if (segment11 != 0)
					{
						NetSegment netSegment6 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment11];
						bool start2 = netSegment6.m_startNode == nodeID;
						if (++num46 == 1)
						{
							segment10 = segment11;
							bool flag18;
							netSegment6.CalculateCorner(segment11, false, start2, false, out bezier2.a, out zero8, out flag18);
							netSegment6.CalculateCorner(segment11, false, start2, true, out bezier3.a, out zero9, out flag18);
						}
						else
						{
							num45 = segment11;
							bool flag18;
							netSegment6.CalculateCorner(segment11, false, start2, true, out bezier2.d, out zero10, out flag18);
							netSegment6.CalculateCorner(segment11, false, start2, false, out bezier3.d, out zero11, out flag18);
						}
					}
				}
				if (num45 == 0)
				{
					return;
				}
				Vector3 a = bezier2.a;
				Vector3 a2 = bezier3.a;
				Vector3 d = bezier2.d;
				Vector3 d2 = bezier3.d;
				TerrainModify.Heights heights4 = TerrainModify.Heights.None;
				TerrainModify.Surface surface8 = TerrainModify.Surface.None;
				if (flag6)
				{
					heights4 = TerrainModify.Heights.SecondaryMin;
				}
				else
				{
					if (flag5)
					{
						heights4 |= TerrainModify.Heights.PrimaryLevel;
					}
					if (info.m_lowerTerrain)
					{
						heights4 |= TerrainModify.Heights.PrimaryMax;
					}
					if (info.m_blockWater)
					{
						heights4 |= TerrainModify.Heights.BlockHeight;
					}
					if (flag)
					{
						surface8 |= TerrainModify.Surface.PavementA;
					}
					if (flag2)
					{
						surface8 |= TerrainModify.Surface.Gravel;
					}
					if (flag3)
					{
						surface8 |= TerrainModify.Surface.Ruined;
					}
					if (flag4)
					{
						surface8 |= TerrainModify.Surface.Clip;
					}
				}
				TerrainModify.Edges edges10 = TerrainModify.Edges.All;
				float num48 = 0f;
				float num49 = 1f;
				float num50 = 0f;
				float num51 = 0f;
				int num52 = 0;
				while (info.m_netAI.NodeModifyMask(nodeID, ref this, segment10, num45, num52, ref surface8, ref heights4, ref edges10, ref num48, ref num49, ref num50, ref num51))
				{
					if (num48 != 0f || num49 != 1f || num52 != 0)
					{
						bezier2.a = Vector3.Lerp(a, a2, num48);
						bezier3.a = Vector3.Lerp(a, a2, num49);
						bezier2.d = Vector3.Lerp(d, d2, num48);
						bezier3.d = Vector3.Lerp(d, d2, num49);
					}
					bezier2.a.y = bezier2.a.y + num50;
					bezier3.a.y = bezier3.a.y + num51;
					bezier2.d.y = bezier2.d.y + num50;
					bezier3.d.y = bezier3.d.y + num51;
					NetSegment.CalculateMiddlePoints(bezier2.a, -zero8, bezier2.d, -zero10, true, true, out bezier2.b, out bezier2.c);
					NetSegment.CalculateMiddlePoints(bezier3.a, -zero9, bezier3.d, -zero11, true, true, out bezier3.b, out bezier3.c);
					Vector3 vector45 = Vector3.Min(bezier2.Min(), bezier3.Min());
					Vector3 vector46 = Vector3.Max(bezier2.Max(), bezier3.Max());
					if (vector45.x <= maxX && vector45.z <= maxZ && minX <= vector46.x && minZ <= vector46.z)
					{
						float num53 = Vector3.Distance(bezier2.a, bezier2.b);
						float num54 = Vector3.Distance(bezier2.b, bezier2.c);
						float num55 = Vector3.Distance(bezier2.c, bezier2.d);
						float num56 = Vector3.Distance(bezier3.a, bezier3.b);
						float num57 = Vector3.Distance(bezier3.b, bezier3.c);
						float num58 = Vector3.Distance(bezier3.c, bezier3.d);
						Vector3 vector47 = (bezier2.a - bezier2.b) * (1f / Mathf.Max(0.1f, num53));
						Vector3 vector48 = (bezier2.c - bezier2.b) * (1f / Mathf.Max(0.1f, num54));
						Vector3 vector49 = (bezier2.d - bezier2.c) * (1f / Mathf.Max(0.1f, num55));
						float num59 = Mathf.Min(Vector3.Dot(vector47, vector48), Vector3.Dot(vector48, vector49));
						num53 += num54 + num55;
						num56 += num57 + num58;
						int num60 = Mathf.Clamp(Mathf.CeilToInt(Mathf.Min(Mathf.Max(num53, num56) * 0.25f, 100f - num59 * 100f)), 1, 16);
						Vector3 vector50 = bezier2.a;
						Vector3 vector51 = bezier3.a;
						for (int num61 = 1; num61 <= num60; num61++)
						{
							Vector3 vector52 = bezier2.Position((float)num61 / (float)num60);
							Vector3 vector53 = bezier3.Position((float)num61 / (float)num60);
							TerrainModify.Edges edges11 = TerrainModify.Edges.AB | TerrainModify.Edges.CD;
							if (info.m_lowerTerrain && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None)
							{
								if (num61 == 1)
								{
									edges11 |= TerrainModify.Edges.DA;
								}
								else if (num61 == num60)
								{
									edges11 |= TerrainModify.Edges.BC;
								}
							}
							edges11 &= edges10;
							TerrainModify.Surface surface9 = surface8;
							if ((surface9 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
							{
								surface9 |= TerrainModify.Surface.Gravel;
							}
							Vector3 zero12 = Vector3.get_zero();
							if (flag6)
							{
								zero12.y += info.m_maxHeight;
							}
							else if (info.m_lowerTerrain)
							{
								zero12.y += info.m_netAI.GetTerrainLowerOffset();
							}
							TerrainModify.ApplyQuad(vector50 + zero12, vector52 + zero12, vector53 + zero12, vector51 + zero12, edges11, heights4, surface9);
							vector50 = vector52;
							vector51 = vector53;
						}
					}
					num52++;
				}
			}
			if ((this.m_flags & NetNode.Flags.End) != NetNode.Flags.None || num != 0)
			{
				Vector3 vector54 = Vector3.get_zero();
				Vector3 vector55 = Vector3.get_zero();
				Vector3 vector56 = Vector3.get_zero();
				Vector3 vector57 = Vector3.get_zero();
				Vector3 zero13 = Vector3.get_zero();
				Vector3 zero14 = Vector3.get_zero();
				Vector3 vector58 = Vector3.get_zero();
				Vector3 vector59 = Vector3.get_zero();
				ushort num62 = num;
				int num63 = 0;
				while (num63 < 8 && num62 == 0)
				{
					ushort segment12 = this.GetSegment(num63);
					if (segment12 != 0)
					{
						num62 = segment12;
					}
					num63++;
				}
				if (num62 == 0)
				{
					return;
				}
				bool start3 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num62].m_startNode == nodeID;
				bool flag19;
				Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num62].CalculateCorner(num62, false, start3, false, out vector54, out zero13, out flag19);
				Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num62].CalculateCorner(num62, false, start3, true, out vector55, out zero14, out flag19);
				Vector3 vector60 = vector54;
				Vector3 vector61 = vector55;
				Vector3 vector62 = vector56;
				Vector3 vector63 = vector57;
				TerrainModify.Heights heights5 = TerrainModify.Heights.None;
				TerrainModify.Surface surface10 = TerrainModify.Surface.None;
				if (flag6)
				{
					heights5 = TerrainModify.Heights.SecondaryMin;
				}
				else
				{
					if (flag5)
					{
						heights5 |= TerrainModify.Heights.PrimaryLevel;
					}
					if (info.m_lowerTerrain)
					{
						heights5 |= TerrainModify.Heights.PrimaryMax;
					}
					if (info.m_blockWater)
					{
						heights5 |= TerrainModify.Heights.BlockHeight;
					}
					if (flag)
					{
						surface10 |= TerrainModify.Surface.PavementA;
					}
					if (flag2)
					{
						surface10 |= TerrainModify.Surface.Gravel;
					}
					if (flag3)
					{
						surface10 |= TerrainModify.Surface.Ruined;
					}
					if (flag4)
					{
						surface10 |= TerrainModify.Surface.Clip;
					}
				}
				TerrainModify.Edges edges12 = TerrainModify.Edges.All;
				float num64 = 0f;
				float num65 = 1f;
				float num66 = 0f;
				float num67 = 0f;
				int num68 = 0;
				while (info.m_netAI.NodeModifyMask(nodeID, ref this, num62, num62, num68, ref surface10, ref heights5, ref edges12, ref num64, ref num65, ref num66, ref num67))
				{
					if (num64 != 0f || num65 != 1f || num68 != 0)
					{
						vector54 = Vector3.Lerp(vector60, vector61, num64);
						vector55 = Vector3.Lerp(vector60, vector61, num65);
						vector56 = Vector3.Lerp(vector62, vector63, num64);
						vector57 = Vector3.Lerp(vector62, vector63, num65);
					}
					vector54.y += num66;
					vector55.y += num67;
					vector56.y += num66;
					vector57.y += num67;
					if (info.m_halfWidth < 3.999f)
					{
						vector56 = vector54 - zero13 * (info.m_halfWidth + 2f);
						vector57 = vector55 - zero14 * (info.m_halfWidth + 2f);
						float num69 = Mathf.Min(new float[]
						{
							Mathf.Min(Mathf.Min(vector54.x, vector55.x), Mathf.Min(vector56.x, vector57.x))
						});
						float num70 = Mathf.Max(new float[]
						{
							Mathf.Max(Mathf.Max(vector54.x, vector55.x), Mathf.Max(vector56.x, vector57.x))
						});
						float num71 = Mathf.Min(new float[]
						{
							Mathf.Min(Mathf.Min(vector54.z, vector55.z), Mathf.Min(vector56.z, vector57.z))
						});
						float num72 = Mathf.Max(new float[]
						{
							Mathf.Max(Mathf.Max(vector54.z, vector55.z), Mathf.Max(vector56.z, vector57.z))
						});
						if (num69 <= maxX && num71 <= maxZ && minX <= num70 && minZ <= num72)
						{
							TerrainModify.Edges edges13 = TerrainModify.Edges.AB | TerrainModify.Edges.BC | TerrainModify.Edges.CD;
							if (info.m_lowerTerrain && (this.m_flags & NetNode.Flags.OnGround) != NetNode.Flags.None)
							{
								edges13 |= TerrainModify.Edges.DA;
							}
							edges13 &= edges12;
							TerrainModify.Surface surface11 = surface10;
							if ((surface11 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
							{
								surface11 |= TerrainModify.Surface.Gravel;
							}
							Vector3 zero15 = Vector3.get_zero();
							if (flag6)
							{
								zero15.y += info.m_maxHeight;
							}
							else if (info.m_lowerTerrain)
							{
								zero15.y += info.m_netAI.GetTerrainLowerOffset();
							}
							TerrainModify.ApplyQuad(vector54 + zero15, vector56 + zero15, vector57 + zero15, vector55 + zero15, edges13, heights5, surface11);
						}
					}
					else
					{
						vector56 = vector55;
						vector57 = vector54;
						vector58 = zero14;
						vector59 = zero13;
						float num73 = info.m_netAI.GetEndRadius() * 1.33333337f * 1.1f;
						Vector3 vector64 = vector54 - zero13 * num73;
						Vector3 vector65 = vector56 - vector58 * num73;
						Vector3 vector66 = vector55 + zero14 * num73;
						Vector3 vector67 = vector57 + vector59 * num73;
						float num74 = Mathf.Min(Mathf.Min(Mathf.Min(vector54.x, vector55.x), Mathf.Min(vector64.x, vector66.x)), Mathf.Min(Mathf.Min(vector65.x, vector67.x), Mathf.Min(vector56.x, vector57.x)));
						float num75 = Mathf.Max(Mathf.Max(Mathf.Max(vector54.x, vector55.x), Mathf.Max(vector64.x, vector66.x)), Mathf.Max(Mathf.Max(vector65.x, vector67.x), Mathf.Max(vector56.x, vector57.x)));
						float num76 = Mathf.Min(Mathf.Min(Mathf.Min(vector54.z, vector55.z), Mathf.Min(vector64.z, vector66.z)), Mathf.Min(Mathf.Min(vector65.z, vector67.z), Mathf.Min(vector56.z, vector57.z)));
						float num77 = Mathf.Max(Mathf.Max(Mathf.Max(vector54.z, vector55.z), Mathf.Max(vector64.z, vector66.z)), Mathf.Max(Mathf.Max(vector65.z, vector67.z), Mathf.Max(vector56.z, vector57.z)));
						if (num74 <= maxX && num76 <= maxZ && minX <= num75 && minZ <= num77)
						{
							int num78 = Mathf.Clamp(Mathf.CeilToInt(info.m_halfWidth * 0.4f), 2, 8);
							Vector3 vector68 = vector54;
							Vector3 vector69 = (vector54 + vector55) * 0.5f;
							for (int num79 = 1; num79 <= num78; num79++)
							{
								Vector3 vector70 = Bezier3.Position(vector54, vector64, vector65, vector56, ((float)num79 - 0.5f) / (float)num78);
								Vector3 vector71 = Bezier3.Position(vector54, vector64, vector65, vector56, (float)num79 / (float)num78);
								TerrainModify.Edges edges14 = TerrainModify.Edges.AB | TerrainModify.Edges.BC;
								edges14 &= edges12;
								TerrainModify.Surface surface12 = surface10;
								if ((surface12 & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
								{
									surface12 |= TerrainModify.Surface.Gravel;
								}
								Vector3 zero16 = Vector3.get_zero();
								if (flag6)
								{
									zero16.y += info.m_maxHeight;
								}
								else if (info.m_lowerTerrain)
								{
									zero16.y += info.m_netAI.GetTerrainLowerOffset();
								}
								TerrainModify.ApplyQuad(vector68 + zero16, vector70 + zero16, vector71 + zero16, vector69 + zero16, edges14, heights5, surface12);
								vector68 = vector71;
							}
						}
					}
					num68++;
				}
			}
			if (this.m_lane != 0u && info.m_halfWidth < 3.999f)
			{
				Vector3 vector72 = Singleton<NetManager>.get_instance().m_lanes.m_buffer[(int)((UIntPtr)this.m_lane)].CalculatePosition((float)this.m_laneOffset * 0.003921569f);
				float num80 = 0f;
				Vector3 vector73 = VectorUtils.NormalizeXZ(vector72 - this.m_position, ref num80);
				if (num80 > 1f)
				{
					Vector3 vector74 = this.m_position - new Vector3(vector73.x + vector73.z * info.m_halfWidth, 0f, vector73.z - vector73.x * info.m_halfWidth);
					Vector3 vector75 = this.m_position - new Vector3(vector73.x - vector73.z * info.m_halfWidth, 0f, vector73.z + vector73.x * info.m_halfWidth);
					Vector3 vector76 = vector72 + new Vector3(vector73.x - vector73.z * info.m_halfWidth, 0f, vector73.z + vector73.x * info.m_halfWidth);
					Vector3 vector77 = vector72 + new Vector3(vector73.x + vector73.z * info.m_halfWidth, 0f, vector73.z - vector73.x * info.m_halfWidth);
					float num81 = Mathf.Min(new float[]
					{
						Mathf.Min(Mathf.Min(vector74.x, vector75.x), Mathf.Min(vector76.x, vector77.x))
					});
					float num82 = Mathf.Max(new float[]
					{
						Mathf.Max(Mathf.Max(vector74.x, vector75.x), Mathf.Max(vector76.x, vector77.x))
					});
					float num83 = Mathf.Min(new float[]
					{
						Mathf.Min(Mathf.Min(vector74.z, vector75.z), Mathf.Min(vector76.z, vector77.z))
					});
					float num84 = Mathf.Max(new float[]
					{
						Mathf.Max(Mathf.Max(vector74.z, vector75.z), Mathf.Max(vector76.z, vector77.z))
					});
					if (num81 <= maxX && num83 <= maxZ && minX <= num82 && minZ <= num84)
					{
						TerrainModify.Edges edges15 = TerrainModify.Edges.All;
						TerrainModify.Heights heights6 = TerrainModify.Heights.None;
						TerrainModify.Surface surface13 = TerrainModify.Surface.None;
						if (flag)
						{
							surface13 |= (TerrainModify.Surface.PavementA | TerrainModify.Surface.Gravel);
						}
						if (flag2)
						{
							surface13 |= TerrainModify.Surface.Gravel;
						}
						if (flag3)
						{
							surface13 |= TerrainModify.Surface.Ruined;
						}
						Vector3 zero17 = Vector3.get_zero();
						TerrainModify.ApplyQuad(vector74 + zero17, vector76 + zero17, vector77 + zero17, vector75 + zero17, edges15, heights6, surface13);
					}
				}
			}
		}
	}

	public void AfterTerrainUpdate(ushort nodeID, float minX, float minZ, float maxX, float maxZ)
	{
		if ((this.m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) != NetNode.Flags.Created)
		{
			return;
		}
		NetInfo info = this.Info;
		if (info == null)
		{
			return;
		}
		info.m_netAI.AfterTerrainUpdate(nodeID, ref this);
	}

	public bool RayCast(Segment3 ray, float snapElevation, out float t, out float priority)
	{
		if ((this.m_flags & (NetNode.Flags.End | NetNode.Flags.Bend | NetNode.Flags.Junction)) != NetNode.Flags.None)
		{
			NetInfo info = this.Info;
			float num = (float)this.m_elevation + info.m_netAI.GetSnapElevation();
			float num2;
			if (info.m_netAI.IsUnderground())
			{
				num2 = Mathf.Clamp01(Mathf.Abs(snapElevation + num) / 12f);
			}
			else
			{
				num2 = Mathf.Clamp01(Mathf.Abs(snapElevation - num) / 12f);
			}
			float collisionHalfWidth = info.m_netAI.GetCollisionHalfWidth();
			float num3 = Mathf.Lerp(info.GetMinNodeDistance(), collisionHalfWidth, num2);
			if (Segment1.Intersect(ray.a.y, ray.b.y, this.m_position.y, ref t))
			{
				float num4 = Vector3.Distance(ray.Position(t), this.m_position);
				if (num4 < num3)
				{
					priority = Mathf.Max(0f, num4 - collisionHalfWidth);
					return true;
				}
			}
		}
		t = 0f;
		priority = 0f;
		return false;
	}

	public static ushort FindOwnerBuilding(ushort nodeID, float maxDistance)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		Vector3 position = instance2.m_nodes.m_buffer[(int)nodeID].m_position;
		int num = Mathf.Max((int)((position.x - maxDistance) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - maxDistance) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + maxDistance) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + maxDistance) / 64f + 135f), 269);
		ushort result = 0;
		float num5 = maxDistance * maxDistance;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					Vector3 position2 = instance.m_buildings.m_buffer[(int)num6].m_position;
					float num8 = position2.x - position.x;
					float num9 = position2.z - position.z;
					float num10 = num8 * num8 + num9 * num9;
					if (num10 < num5 && instance.m_buildings.m_buffer[(int)num6].ContainsNode(nodeID))
					{
						return num6;
					}
					num6 = instance.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public bool CalculateGroupData(ushort nodeID, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		NetInfo info = this.Info;
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer && Notification.CalculateGroupData(ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
		{
			result = true;
		}
		if ((this.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 vector = this.m_position;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = this.GetSegment(i);
				if (segment != 0)
				{
					NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
					ItemClass connectionClass = info2.GetConnectionClass();
					Vector3 vector2 = (nodeID != instance.m_segments.m_buffer[(int)segment].m_startNode) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
					float num = -1f;
					for (int j = 0; j < 8; j++)
					{
						ushort segment2 = this.GetSegment(j);
						if (segment2 != 0 && segment2 != segment)
						{
							NetInfo info3 = instance.m_segments.m_buffer[(int)segment2].Info;
							ItemClass connectionClass2 = info3.GetConnectionClass();
							if (((info.m_netLayers | info2.m_netLayers | info3.m_netLayers) & 1 << layer) != 0 && (connectionClass.m_service == connectionClass2.m_service || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None))
							{
								Vector3 vector3 = (nodeID != instance.m_segments.m_buffer[(int)segment2].m_startNode) ? instance.m_segments.m_buffer[(int)segment2].m_endDirection : instance.m_segments.m_buffer[(int)segment2].m_startDirection;
								float num2 = vector2.x * vector3.x + vector2.z * vector3.z;
								num = Mathf.Max(num, num2);
								bool flag = info2.m_requireDirectRenderers && (info2.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None);
								bool flag2 = info3.m_requireDirectRenderers && (info3.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None);
								if (j > i && (flag || flag2))
								{
									float num3 = 0.01f - Mathf.Min(info2.m_maxTurnAngleCos, info3.m_maxTurnAngleCos);
									if (num2 < num3)
									{
										float num4;
										if (flag)
										{
											num4 = info2.m_netAI.GetNodeInfoPriority(segment, ref instance.m_segments.m_buffer[(int)segment]);
										}
										else
										{
											num4 = -1E+08f;
										}
										float num5;
										if (flag2)
										{
											num5 = info3.m_netAI.GetNodeInfoPriority(segment2, ref instance.m_segments.m_buffer[(int)segment2]);
										}
										else
										{
											num5 = -1E+08f;
										}
										if (num4 >= num5)
										{
											if (info2.m_nodes != null && info2.m_nodes.Length != 0)
											{
												result = true;
												for (int k = 0; k < info2.m_nodes.Length; k++)
												{
													NetInfo.Node node = info2.m_nodes[k];
													if ((node.m_connectGroup == NetInfo.ConnectGroup.None || (node.m_connectGroup & info3.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node.m_layer == layer && node.CheckFlags(this.m_flags) && node.m_combinedLod != null && node.m_directConnect)
													{
														if ((node.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
														{
															bool flag3 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
															if (info3.m_hasBackwardVehicleLanes != info3.m_hasForwardVehicleLanes)
															{
																bool flag4 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
																if (flag3 == flag4)
																{
																	goto IL_4A0;
																}
															}
															if (flag3)
															{
																if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
																{
																	goto IL_4A0;
																}
															}
															else if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
															{
																goto IL_4A0;
															}
														}
														NetNode.CalculateGroupData(node, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
													}
													IL_4A0:;
												}
											}
										}
										else if (info3.m_nodes != null && info3.m_nodes.Length != 0)
										{
											result = true;
											for (int l = 0; l < info3.m_nodes.Length; l++)
											{
												NetInfo.Node node2 = info3.m_nodes[l];
												if ((node2.m_connectGroup == NetInfo.ConnectGroup.None || (node2.m_connectGroup & info2.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node2.m_layer == layer && node2.CheckFlags(this.m_flags) && node2.m_combinedLod != null && node2.m_directConnect)
												{
													if ((node2.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
													{
														bool flag5 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
														if (info2.m_hasBackwardVehicleLanes != info2.m_hasForwardVehicleLanes)
														{
															bool flag6 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
															if (flag6 == flag5)
															{
																goto IL_639;
															}
														}
														if (flag5)
														{
															if ((node2.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
															{
																goto IL_639;
															}
														}
														else if ((node2.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
														{
															goto IL_639;
														}
													}
													NetNode.CalculateGroupData(node2, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
												}
												IL_639:;
											}
										}
									}
								}
							}
						}
					}
					vector += vector2 * (2f + num * 2f);
				}
			}
			vector.y = this.m_position.y + (float)this.m_heightOffset * 0.015625f;
			if ((info.m_netLayers & 1 << layer) != 0 && info.m_requireSegmentRenderers)
			{
				for (int m = 0; m < 8; m++)
				{
					ushort segment3 = this.GetSegment(m);
					if (segment3 != 0)
					{
						NetInfo info4 = instance.m_segments.m_buffer[(int)segment3].Info;
						if (info4.m_nodes != null && info4.m_nodes.Length != 0)
						{
							result = true;
							for (int n = 0; n < info4.m_nodes.Length; n++)
							{
								NetInfo.Node node3 = info4.m_nodes[n];
								if (node3.m_layer == layer && node3.CheckFlags(this.m_flags) && node3.m_combinedLod != null && !node3.m_directConnect)
								{
									NetNode.CalculateGroupData(node3, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
								}
							}
						}
					}
				}
			}
		}
		else if ((info.m_netLayers & 1 << layer) != 0)
		{
			if ((this.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				if (info.m_nodes != null && info.m_nodes.Length != 0)
				{
					result = true;
					for (int num6 = 0; num6 < info.m_nodes.Length; num6++)
					{
						NetInfo.Node node4 = info.m_nodes[num6];
						if (node4.m_layer == layer && node4.CheckFlags(this.m_flags) && node4.m_combinedLod != null && !node4.m_directConnect)
						{
							NetNode.CalculateGroupData(node4, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
						}
					}
				}
			}
			else if ((this.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None)
			{
				if (info.m_segments != null && info.m_segments.Length != 0)
				{
					result = true;
					for (int num7 = 0; num7 < info.m_segments.Length; num7++)
					{
						NetInfo.Segment segment4 = info.m_segments[num7];
						bool flag7;
						if (segment4.m_layer == layer && segment4.CheckFlags(info.m_netAI.GetBendFlags(nodeID, ref this), out flag7) && segment4.m_combinedLod != null && !segment4.m_disableBendNodes)
						{
							NetSegment.CalculateGroupData(segment4, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
						}
					}
				}
				if (info.m_nodes != null && info.m_nodes.Length != 0)
				{
					result = true;
					for (int num8 = 0; num8 < info.m_nodes.Length; num8++)
					{
						NetInfo.Node node5 = info.m_nodes[num8];
						if ((node5.m_connectGroup == NetInfo.ConnectGroup.None || (node5.m_connectGroup & info.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node5.m_layer == layer && node5.CheckFlags(this.m_flags) && node5.m_combinedLod != null && node5.m_directConnect)
						{
							if ((node5.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
							{
								NetManager instance2 = Singleton<NetManager>.get_instance();
								ushort num9 = 0;
								ushort num10 = 0;
								bool flag8 = false;
								int num11 = 0;
								for (int num12 = 0; num12 < 8; num12++)
								{
									ushort segment5 = this.GetSegment(num12);
									if (segment5 != 0)
									{
										NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment5];
										bool flag9 = ++num11 == 1;
										bool flag10 = netSegment.m_startNode == nodeID;
										if ((!flag9 && !flag8) || (flag9 && !flag10))
										{
											flag8 = true;
											num9 = segment5;
										}
										else
										{
											num10 = segment5;
										}
									}
								}
								bool flag11 = instance2.m_segments.m_buffer[(int)num9].m_startNode == nodeID == ((instance2.m_segments.m_buffer[(int)num9].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
								bool flag12 = instance2.m_segments.m_buffer[(int)num10].m_startNode == nodeID == ((instance2.m_segments.m_buffer[(int)num10].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
								if (flag11 == flag12)
								{
									goto IL_AE4;
								}
								if (flag11)
								{
									if ((node5.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
									{
										goto IL_AE4;
									}
								}
								else if ((node5.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
								{
									goto IL_AE4;
								}
							}
							NetNode.CalculateGroupData(node5, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays);
						}
						IL_AE4:;
					}
				}
			}
		}
		return result;
	}

	public static void CalculateGroupData(NetInfo.Node nodeInfo, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		RenderGroup.MeshData data = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		vertexCount += data.m_vertices.Length;
		triangleCount += data.m_triangles.Length;
		objectCount++;
		vertexArrays |= (data.VertexArrayMask() | RenderGroup.VertexArrays.Colors | RenderGroup.VertexArrays.Uvs2 | RenderGroup.VertexArrays.Uvs4);
	}

	public void PopulateGroupData(ushort nodeID, int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		NetInfo info = this.Info;
		if (this.m_problems != Notification.Problem.None && layer == Singleton<NotificationManager>.get_instance().m_notificationLayer)
		{
			Vector3 position = this.m_position;
			position.y += info.m_maxHeight;
			Notification.PopulateGroupData(this.m_problems, position, 1f, groupX, groupZ, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
		}
		bool flag = false;
		if ((this.m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 vector = this.m_position;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = this.GetSegment(i);
				if (segment != 0)
				{
					NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
					ItemClass connectionClass = info2.GetConnectionClass();
					Vector3 vector2 = (nodeID != instance.m_segments.m_buffer[(int)segment].m_startNode) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
					float num = -1f;
					for (int j = 0; j < 8; j++)
					{
						ushort segment2 = this.GetSegment(j);
						if (segment2 != 0 && segment2 != segment)
						{
							NetInfo info3 = instance.m_segments.m_buffer[(int)segment2].Info;
							ItemClass connectionClass2 = info3.GetConnectionClass();
							if (((info.m_netLayers | info2.m_netLayers | info3.m_netLayers) & 1 << layer) != 0 && (connectionClass.m_service == connectionClass2.m_service || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None))
							{
								Vector3 vector3 = (nodeID != instance.m_segments.m_buffer[(int)segment2].m_startNode) ? instance.m_segments.m_buffer[(int)segment2].m_endDirection : instance.m_segments.m_buffer[(int)segment2].m_startDirection;
								float num2 = vector2.x * vector3.x + vector2.z * vector3.z;
								num = Mathf.Max(num, num2);
								bool flag2 = info2.m_requireDirectRenderers && (info2.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info2.m_nodeConnectGroups & info3.m_connectGroup) != NetInfo.ConnectGroup.None);
								bool flag3 = info3.m_requireDirectRenderers && (info3.m_nodeConnectGroups == NetInfo.ConnectGroup.None || (info3.m_nodeConnectGroups & info2.m_connectGroup) != NetInfo.ConnectGroup.None);
								if (j > i && (flag2 || flag3))
								{
									float num3 = 0.01f - Mathf.Min(info2.m_maxTurnAngleCos, info3.m_maxTurnAngleCos);
									if (num2 < num3)
									{
										float num4;
										if (flag2)
										{
											num4 = info2.m_netAI.GetNodeInfoPriority(segment, ref instance.m_segments.m_buffer[(int)segment]);
										}
										else
										{
											num4 = -1E+08f;
										}
										float num5;
										if (flag3)
										{
											num5 = info3.m_netAI.GetNodeInfoPriority(segment2, ref instance.m_segments.m_buffer[(int)segment2]);
										}
										else
										{
											num5 = -1E+08f;
										}
										if (num4 >= num5)
										{
											if (info2.m_nodes != null && info2.m_nodes.Length != 0)
											{
												flag = true;
												float vScale = info2.m_netAI.GetVScale();
												Vector3 zero = Vector3.get_zero();
												Vector3 zero2 = Vector3.get_zero();
												Vector3 vector4 = Vector3.get_zero();
												Vector3 vector5 = Vector3.get_zero();
												Vector3 zero3 = Vector3.get_zero();
												Vector3 zero4 = Vector3.get_zero();
												Vector3 zero5 = Vector3.get_zero();
												Vector3 zero6 = Vector3.get_zero();
												bool start = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_startNode == nodeID;
												bool flag4;
												Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].CalculateCorner(segment, true, start, false, out zero, out zero3, out flag4);
												Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].CalculateCorner(segment, true, start, true, out zero2, out zero4, out flag4);
												start = (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_startNode == nodeID);
												Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].CalculateCorner(segment2, true, start, true, out vector4, out zero5, out flag4);
												Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].CalculateCorner(segment2, true, start, false, out vector5, out zero6, out flag4);
												Vector3 vector6 = (vector5 - vector4) * (info2.m_halfWidth / info3.m_halfWidth * 0.5f - 0.5f);
												vector4 -= vector6;
												vector5 += vector6;
												Vector3 vector7;
												Vector3 vector8;
												NetSegment.CalculateMiddlePoints(zero, -zero3, vector4, -zero5, true, true, out vector7, out vector8);
												Vector3 vector9;
												Vector3 vector10;
												NetSegment.CalculateMiddlePoints(zero2, -zero4, vector5, -zero6, true, true, out vector9, out vector10);
												Matrix4x4 leftMatrix = NetSegment.CalculateControlMatrix(zero, vector7, vector8, vector4, zero2, vector9, vector10, vector5, groupPosition, vScale);
												Matrix4x4 rightMatrix = NetSegment.CalculateControlMatrix(zero2, vector9, vector10, vector5, zero, vector7, vector8, vector4, groupPosition, vScale);
												Vector4 vector11;
												vector11..ctor(0.5f / info2.m_halfWidth, 1f / info2.m_segmentLength, 1f, 1f);
												Vector4 colorLocation;
												Vector4 vector12;
												if (NetNode.BlendJunction(nodeID))
												{
													colorLocation = RenderManager.GetColorLocation(86016u + (uint)nodeID);
													vector12 = colorLocation;
												}
												else
												{
													colorLocation = RenderManager.GetColorLocation((uint)(49152 + segment));
													vector12 = RenderManager.GetColorLocation((uint)(49152 + segment2));
												}
												Vector4 vector13;
												vector13..ctor(colorLocation.x, colorLocation.y, vector12.x, vector12.y);
												for (int k = 0; k < info2.m_nodes.Length; k++)
												{
													NetInfo.Node node = info2.m_nodes[k];
													if ((node.m_connectGroup == NetInfo.ConnectGroup.None || (node.m_connectGroup & info3.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node.m_layer == layer && node.CheckFlags(this.m_flags) && node.m_combinedLod != null && node.m_directConnect)
													{
														Vector4 objectIndex = vector13;
														Vector4 meshScale = vector11;
														if (node.m_requireWindSpeed)
														{
															objectIndex.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
														}
														if ((node.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
														{
															bool flag5 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
															if (info3.m_hasBackwardVehicleLanes != info3.m_hasForwardVehicleLanes)
															{
																bool flag6 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
																if (flag5 == flag6)
																{
																	goto IL_784;
																}
															}
															if (flag5)
															{
																if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
																{
																	goto IL_784;
																}
															}
															else
															{
																if ((node.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
																{
																	goto IL_784;
																}
																meshScale.x = -meshScale.x;
																meshScale.y = -meshScale.y;
															}
														}
														NetNode.PopulateGroupData(info2, node, leftMatrix, rightMatrix, meshScale, objectIndex, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
													}
													IL_784:;
												}
											}
										}
										else if (info3.m_nodes != null && info3.m_nodes.Length != 0)
										{
											flag = true;
											float vScale2 = info3.m_netAI.GetVScale();
											Vector3 vector14 = Vector3.get_zero();
											Vector3 vector15 = Vector3.get_zero();
											Vector3 zero7 = Vector3.get_zero();
											Vector3 zero8 = Vector3.get_zero();
											Vector3 zero9 = Vector3.get_zero();
											Vector3 zero10 = Vector3.get_zero();
											Vector3 zero11 = Vector3.get_zero();
											Vector3 zero12 = Vector3.get_zero();
											bool start2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_startNode == nodeID;
											bool flag7;
											Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].CalculateCorner(segment, true, start2, false, out vector14, out zero9, out flag7);
											Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].CalculateCorner(segment, true, start2, true, out vector15, out zero10, out flag7);
											start2 = (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].m_startNode == nodeID);
											Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].CalculateCorner(segment2, true, start2, true, out zero7, out zero11, out flag7);
											Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment2].CalculateCorner(segment2, true, start2, false, out zero8, out zero12, out flag7);
											Vector3 vector16 = (vector15 - vector14) * (info3.m_halfWidth / info2.m_halfWidth * 0.5f - 0.5f);
											vector14 -= vector16;
											vector15 += vector16;
											Vector3 vector17;
											Vector3 vector18;
											NetSegment.CalculateMiddlePoints(vector14, -zero9, zero7, -zero11, true, true, out vector17, out vector18);
											Vector3 vector19;
											Vector3 vector20;
											NetSegment.CalculateMiddlePoints(vector15, -zero10, zero8, -zero12, true, true, out vector19, out vector20);
											Matrix4x4 leftMatrix2 = NetSegment.CalculateControlMatrix(vector14, vector17, vector18, zero7, vector15, vector19, vector20, zero8, groupPosition, vScale2);
											Matrix4x4 rightMatrix2 = NetSegment.CalculateControlMatrix(vector15, vector19, vector20, zero8, vector14, vector17, vector18, zero7, groupPosition, vScale2);
											Vector4 vector21;
											vector21..ctor(0.5f / info3.m_halfWidth, 1f / info3.m_segmentLength, 1f, 1f);
											Vector4 colorLocation2;
											Vector4 vector22;
											if (NetNode.BlendJunction(nodeID))
											{
												colorLocation2 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
												vector22 = colorLocation2;
											}
											else
											{
												colorLocation2 = RenderManager.GetColorLocation((uint)(49152 + segment));
												vector22 = RenderManager.GetColorLocation((uint)(49152 + segment2));
											}
											Vector4 vector23;
											vector23..ctor(colorLocation2.x, colorLocation2.y, vector22.x, vector22.y);
											for (int l = 0; l < info3.m_nodes.Length; l++)
											{
												NetInfo.Node node2 = info3.m_nodes[l];
												if ((node2.m_connectGroup == NetInfo.ConnectGroup.None || (node2.m_connectGroup & info2.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node2.m_layer == layer && node2.CheckFlags(this.m_flags) && node2.m_combinedLod != null && node2.m_directConnect)
												{
													Vector4 objectIndex2 = vector23;
													Vector4 meshScale2 = vector21;
													if (node2.m_requireWindSpeed)
													{
														objectIndex2.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
													}
													if ((node2.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
													{
														bool flag8 = instance.m_segments.m_buffer[(int)segment2].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment2].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
														if (info2.m_hasBackwardVehicleLanes != info2.m_hasForwardVehicleLanes)
														{
															bool flag9 = instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
															if (flag9 == flag8)
															{
																goto IL_BD3;
															}
														}
														if (flag8)
														{
															if ((node2.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
															{
																goto IL_BD3;
															}
															meshScale2.x = -meshScale2.x;
															meshScale2.y = -meshScale2.y;
														}
														else if ((node2.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
														{
															goto IL_BD3;
														}
													}
													NetNode.PopulateGroupData(info3, node2, leftMatrix2, rightMatrix2, meshScale2, objectIndex2, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
												}
												IL_BD3:;
											}
										}
									}
								}
							}
						}
					}
					vector += vector2 * (2f + num * 2f);
				}
			}
			vector.y = this.m_position.y + (float)this.m_heightOffset * 0.015625f;
			if ((info.m_netLayers & 1 << layer) != 0 && info.m_requireSegmentRenderers)
			{
				for (int m = 0; m < 8; m++)
				{
					ushort segment3 = this.GetSegment(m);
					if (segment3 != 0)
					{
						NetInfo info4 = instance.m_segments.m_buffer[(int)segment3].Info;
						if (info4.m_nodes != null && info4.m_nodes.Length != 0)
						{
							flag = true;
							float vScale3 = info4.m_netAI.GetVScale();
							Vector3 zero13 = Vector3.get_zero();
							Vector3 zero14 = Vector3.get_zero();
							Vector3 zero15 = Vector3.get_zero();
							Vector3 zero16 = Vector3.get_zero();
							Vector3 vector24 = Vector3.get_zero();
							Vector3 vector25 = Vector3.get_zero();
							Vector3 vector26 = Vector3.get_zero();
							Vector3 vector27 = Vector3.get_zero();
							Vector3 zero17 = Vector3.get_zero();
							Vector3 zero18 = Vector3.get_zero();
							Vector3 zero19 = Vector3.get_zero();
							Vector3 zero20 = Vector3.get_zero();
							NetSegment netSegment = instance.m_segments.m_buffer[(int)segment3];
							ItemClass connectionClass3 = info4.GetConnectionClass();
							Vector3 vector28 = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
							float num6 = -4f;
							float num7 = -4f;
							ushort num8 = 0;
							ushort num9 = 0;
							for (int n = 0; n < 8; n++)
							{
								ushort segment4 = this.GetSegment(n);
								if (segment4 != 0 && segment4 != segment3)
								{
									NetInfo info5 = instance.m_segments.m_buffer[(int)segment4].Info;
									ItemClass connectionClass4 = info5.GetConnectionClass();
									if (connectionClass3.m_service == connectionClass4.m_service)
									{
										NetSegment netSegment2 = instance.m_segments.m_buffer[(int)segment4];
										Vector3 vector29 = (nodeID != netSegment2.m_startNode) ? netSegment2.m_endDirection : netSegment2.m_startDirection;
										float num10 = vector28.x * vector29.x + vector28.z * vector29.z;
										if (vector29.z * vector28.x - vector29.x * vector28.z < 0f)
										{
											if (num10 > num6)
											{
												num6 = num10;
												num8 = segment4;
											}
											num10 = -2f - num10;
											if (num10 > num7)
											{
												num7 = num10;
												num9 = segment4;
											}
										}
										else
										{
											if (num10 > num7)
											{
												num7 = num10;
												num9 = segment4;
											}
											num10 = -2f - num10;
											if (num10 > num6)
											{
												num6 = num10;
												num8 = segment4;
											}
										}
									}
								}
							}
							bool start3 = netSegment.m_startNode == nodeID;
							bool flag10;
							netSegment.CalculateCorner(segment3, true, start3, false, out zero13, out zero15, out flag10);
							netSegment.CalculateCorner(segment3, true, start3, true, out zero14, out zero16, out flag10);
							Matrix4x4 leftMatrix3;
							Matrix4x4 rightMatrix3;
							Matrix4x4 leftMatrixB;
							Matrix4x4 rightMatrixB;
							Vector4 meshScale3;
							Vector4 centerPos;
							Vector4 sideScale;
							if (num8 != 0 && num9 != 0)
							{
								float num11 = info4.m_pavementWidth / info4.m_halfWidth * 0.5f;
								float num12 = 1f;
								if (num8 != 0)
								{
									NetSegment netSegment3 = instance.m_segments.m_buffer[(int)num8];
									NetInfo info6 = netSegment3.Info;
									start3 = (netSegment3.m_startNode == nodeID);
									netSegment3.CalculateCorner(num8, true, start3, true, out vector24, out vector26, out flag10);
									netSegment3.CalculateCorner(num8, true, start3, false, out vector25, out vector27, out flag10);
									float num13 = info6.m_pavementWidth / info6.m_halfWidth * 0.5f;
									num11 = (num11 + num13) * 0.5f;
									num12 = 2f * info4.m_halfWidth / (info4.m_halfWidth + info6.m_halfWidth);
								}
								float num14 = info4.m_pavementWidth / info4.m_halfWidth * 0.5f;
								float num15 = 1f;
								if (num9 != 0)
								{
									NetSegment netSegment4 = instance.m_segments.m_buffer[(int)num9];
									NetInfo info7 = netSegment4.Info;
									start3 = (netSegment4.m_startNode == nodeID);
									netSegment4.CalculateCorner(num9, true, start3, true, out zero17, out zero19, out flag10);
									netSegment4.CalculateCorner(num9, true, start3, false, out zero18, out zero20, out flag10);
									float num16 = info7.m_pavementWidth / info7.m_halfWidth * 0.5f;
									num14 = (num14 + num16) * 0.5f;
									num15 = 2f * info4.m_halfWidth / (info4.m_halfWidth + info7.m_halfWidth);
								}
								Vector3 vector30;
								Vector3 vector31;
								NetSegment.CalculateMiddlePoints(zero13, -zero15, vector24, -vector26, true, true, out vector30, out vector31);
								Vector3 vector32;
								Vector3 vector33;
								NetSegment.CalculateMiddlePoints(zero14, -zero16, vector25, -vector27, true, true, out vector32, out vector33);
								Vector3 vector34;
								Vector3 vector35;
								NetSegment.CalculateMiddlePoints(zero13, -zero15, zero17, -zero19, true, true, out vector34, out vector35);
								Vector3 vector36;
								Vector3 vector37;
								NetSegment.CalculateMiddlePoints(zero14, -zero16, zero18, -zero20, true, true, out vector36, out vector37);
								leftMatrix3 = NetSegment.CalculateControlMatrix(zero13, vector30, vector31, vector24, zero13, vector30, vector31, vector24, groupPosition, vScale3);
								rightMatrix3 = NetSegment.CalculateControlMatrix(zero14, vector32, vector33, vector25, zero14, vector32, vector33, vector25, groupPosition, vScale3);
								leftMatrixB = NetSegment.CalculateControlMatrix(zero13, vector34, vector35, zero17, zero13, vector34, vector35, zero17, groupPosition, vScale3);
								rightMatrixB = NetSegment.CalculateControlMatrix(zero14, vector36, vector37, zero18, zero14, vector36, vector37, zero18, groupPosition, vScale3);
								meshScale3..ctor(0.5f / info4.m_halfWidth, 1f / info4.m_segmentLength, 0.5f - info4.m_pavementWidth / info4.m_halfWidth * 0.5f, info4.m_pavementWidth / info4.m_halfWidth * 0.5f);
								centerPos = vector - groupPosition;
								centerPos.w = (leftMatrix3.m33 + rightMatrix3.m33 + leftMatrixB.m33 + rightMatrixB.m33) * 0.25f;
								sideScale..ctor(num11, num12, num14, num15);
							}
							else
							{
								vector.x = (zero13.x + zero14.x) * 0.5f;
								vector.z = (zero13.z + zero14.z) * 0.5f;
								vector24 = zero14;
								vector25 = zero13;
								vector26 = zero16;
								vector27 = zero15;
								float num17 = info.m_netAI.GetEndRadius() * 1.33333337f;
								Vector3 vector38 = zero13 - zero15 * num17;
								Vector3 vector39 = vector24 - vector26 * num17;
								Vector3 vector40 = zero14 - zero16 * num17;
								Vector3 vector41 = vector25 - vector27 * num17;
								Vector3 vector42 = zero13 + zero15 * num17;
								Vector3 vector43 = vector24 + vector26 * num17;
								Vector3 vector44 = zero14 + zero16 * num17;
								Vector3 vector45 = vector25 + vector27 * num17;
								leftMatrix3 = NetSegment.CalculateControlMatrix(zero13, vector38, vector39, vector24, zero13, vector38, vector39, vector24, groupPosition, vScale3);
								rightMatrix3 = NetSegment.CalculateControlMatrix(zero14, vector44, vector45, vector25, zero14, vector44, vector45, vector25, groupPosition, vScale3);
								leftMatrixB = NetSegment.CalculateControlMatrix(zero13, vector42, vector43, vector24, zero13, vector42, vector43, vector24, groupPosition, vScale3);
								rightMatrixB = NetSegment.CalculateControlMatrix(zero14, vector40, vector41, vector25, zero14, vector40, vector41, vector25, groupPosition, vScale3);
								leftMatrix3.SetRow(3, leftMatrix3.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
								rightMatrix3.SetRow(3, rightMatrix3.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
								leftMatrixB.SetRow(3, leftMatrixB.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
								rightMatrixB.SetRow(3, rightMatrixB.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
								meshScale3..ctor(0.5f / info4.m_halfWidth, 1f / info4.m_segmentLength, 0.5f - info4.m_pavementWidth / info4.m_halfWidth * 0.5f, info4.m_pavementWidth / info4.m_halfWidth * 0.5f);
								centerPos = vector - groupPosition;
								centerPos.w = (leftMatrix3.m33 + rightMatrix3.m33 + leftMatrixB.m33 + rightMatrixB.m33) * 0.25f;
								sideScale..ctor(info4.m_pavementWidth / info4.m_halfWidth * 0.5f, 1f, info4.m_pavementWidth / info4.m_halfWidth * 0.5f, 1f);
							}
							Vector4 colorLocation3;
							Vector4 vector46;
							if (NetNode.BlendJunction(nodeID))
							{
								colorLocation3 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
								vector46 = colorLocation3;
							}
							else
							{
								colorLocation3 = RenderManager.GetColorLocation((uint)(49152 + segment3));
								vector46 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
							}
							Vector4 vector47;
							vector47..ctor(colorLocation3.x, colorLocation3.y, vector46.x, vector46.y);
							for (int num18 = 0; num18 < info4.m_nodes.Length; num18++)
							{
								NetInfo.Node node3 = info4.m_nodes[num18];
								if (node3.m_layer == layer && node3.CheckFlags(this.m_flags) && node3.m_combinedLod != null && !node3.m_directConnect)
								{
									Vector4 objectIndex3 = vector47;
									if (node3.m_requireWindSpeed)
									{
										objectIndex3.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
									}
									NetNode.PopulateGroupData(info4, node3, leftMatrix3, rightMatrix3, leftMatrixB, rightMatrixB, meshScale3, centerPos, sideScale, objectIndex3, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
								}
							}
						}
					}
				}
			}
		}
		else if ((info.m_netLayers & 1 << layer) != 0)
		{
			if ((this.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				if (info.m_nodes != null && info.m_nodes.Length != 0)
				{
					flag = true;
					float vScale4 = info.m_netAI.GetVScale() / 1.5f;
					Vector3 zero21 = Vector3.get_zero();
					Vector3 zero22 = Vector3.get_zero();
					Vector3 vector48 = Vector3.get_zero();
					Vector3 vector49 = Vector3.get_zero();
					Vector3 zero23 = Vector3.get_zero();
					Vector3 zero24 = Vector3.get_zero();
					Vector3 vector50 = Vector3.get_zero();
					Vector3 vector51 = Vector3.get_zero();
					bool flag11 = false;
					ushort num19 = 0;
					for (int num20 = 0; num20 < 8; num20++)
					{
						ushort segment5 = this.GetSegment(num20);
						if (segment5 != 0)
						{
							NetSegment netSegment5 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment5];
							bool start4 = netSegment5.m_startNode == nodeID;
							bool flag12;
							netSegment5.CalculateCorner(segment5, true, start4, false, out zero21, out zero23, out flag12);
							netSegment5.CalculateCorner(segment5, true, start4, true, out zero22, out zero24, out flag12);
							if (flag11)
							{
								vector50 = -zero23;
								vector51 = -zero24;
								zero23.y = 0.25f;
								zero24.y = 0.25f;
								vector50.y = -5f;
								vector51.y = -5f;
								vector48 = zero21 - zero23 * 10f + vector50 * 10f;
								vector49 = zero22 - zero24 * 10f + vector51 * 10f;
							}
							else
							{
								vector48 = zero22;
								vector49 = zero21;
								vector50 = zero24;
								vector51 = zero23;
							}
							num19 = segment5;
						}
					}
					if (flag11)
					{
						Vector3 vector52;
						Vector3 vector53;
						NetSegment.CalculateMiddlePoints(zero21, -zero23, vector48, -vector50, true, true, out vector52, out vector53);
						Vector3 vector54;
						Vector3 vector55;
						NetSegment.CalculateMiddlePoints(zero22, -zero24, vector49, -vector51, true, true, out vector54, out vector55);
						Matrix4x4 leftMatrix4 = NetSegment.CalculateControlMatrix(zero21, vector52, vector53, vector48, zero22, vector54, vector55, vector49, groupPosition, vScale4);
						Matrix4x4 rightMatrix4 = NetSegment.CalculateControlMatrix(zero22, vector54, vector55, vector49, zero21, vector52, vector53, vector48, groupPosition, vScale4);
						Vector4 meshScale4;
						meshScale4..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
						Vector4 colorLocation4 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
						Vector4 vector56;
						vector56..ctor(colorLocation4.x, colorLocation4.y, colorLocation4.x, colorLocation4.y);
						if (info.m_segments != null && info.m_segments.Length != 0)
						{
							for (int num21 = 0; num21 < info.m_segments.Length; num21++)
							{
								NetInfo.Segment segment6 = info.m_segments[num21];
								bool flag13;
								if (segment6.m_layer == layer && segment6.CheckFlags(NetSegment.Flags.Bend | (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num19].m_flags & NetSegment.Flags.Collapsed), out flag13) && segment6.m_combinedLod != null)
								{
									Vector4 objectIndex4 = vector56;
									if (segment6.m_requireWindSpeed)
									{
										objectIndex4.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
									}
									NetSegment.PopulateGroupData(info, segment6, leftMatrix4, rightMatrix4, meshScale4, objectIndex4, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
								}
							}
						}
					}
					else
					{
						float num22 = info.m_netAI.GetEndRadius() * 1.33333337f;
						Vector3 vector57 = zero21 - zero23 * num22;
						Vector3 vector58 = vector48 - vector50 * num22;
						Vector3 vector59 = zero22 - zero24 * num22;
						Vector3 vector60 = vector49 - vector51 * num22;
						Vector3 vector61 = zero21 + zero23 * num22;
						Vector3 vector62 = vector48 + vector50 * num22;
						Vector3 vector63 = zero22 + zero24 * num22;
						Vector3 vector64 = vector49 + vector51 * num22;
						Matrix4x4 leftMatrix5 = NetSegment.CalculateControlMatrix(zero21, vector57, vector58, vector48, zero21, vector57, vector58, vector48, groupPosition, vScale4);
						Matrix4x4 rightMatrix5 = NetSegment.CalculateControlMatrix(zero22, vector63, vector64, vector49, zero22, vector63, vector64, vector49, groupPosition, vScale4);
						Matrix4x4 leftMatrixB2 = NetSegment.CalculateControlMatrix(zero21, vector61, vector62, vector48, zero21, vector61, vector62, vector48, groupPosition, vScale4);
						Matrix4x4 rightMatrixB2 = NetSegment.CalculateControlMatrix(zero22, vector59, vector60, vector49, zero22, vector59, vector60, vector49, groupPosition, vScale4);
						leftMatrix5.SetRow(3, leftMatrix5.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
						rightMatrix5.SetRow(3, rightMatrix5.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
						leftMatrixB2.SetRow(3, leftMatrixB2.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
						rightMatrixB2.SetRow(3, rightMatrixB2.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
						Vector4 meshScale5;
						meshScale5..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 0.5f - info.m_pavementWidth / info.m_halfWidth * 0.5f, info.m_pavementWidth / info.m_halfWidth * 0.5f);
						Vector4 centerPos2;
						centerPos2..ctor(this.m_position.x - groupPosition.x, this.m_position.y - groupPosition.y + (float)this.m_heightOffset * 0.015625f, this.m_position.z - groupPosition.z, 0f);
						centerPos2.w = (leftMatrix5.m33 + rightMatrix5.m33 + leftMatrixB2.m33 + rightMatrixB2.m33) * 0.25f;
						Vector4 sideScale2;
						sideScale2..ctor(info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f, info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f);
						Vector4 colorLocation5 = RenderManager.GetColorLocation((uint)(49152 + num19));
						Vector4 vector65;
						vector65..ctor(colorLocation5.x, colorLocation5.y, colorLocation5.x, colorLocation5.y);
						for (int num23 = 0; num23 < info.m_nodes.Length; num23++)
						{
							NetInfo.Node node4 = info.m_nodes[num23];
							if (node4.m_layer == layer && node4.CheckFlags(this.m_flags) && node4.m_combinedLod != null && !node4.m_directConnect)
							{
								Vector4 objectIndex5 = vector65;
								if (node4.m_requireWindSpeed)
								{
									objectIndex5.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
								}
								NetNode.PopulateGroupData(info, node4, leftMatrix5, rightMatrix5, leftMatrixB2, rightMatrixB2, meshScale5, centerPos2, sideScale2, objectIndex5, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
							}
						}
					}
				}
			}
			else if ((this.m_flags & NetNode.Flags.Bend) != NetNode.Flags.None && ((info.m_segments != null && info.m_segments.Length != 0) || (info.m_nodes != null && info.m_nodes.Length != 0)))
			{
				float vScale5 = info.m_netAI.GetVScale();
				Vector3 zero25 = Vector3.get_zero();
				Vector3 zero26 = Vector3.get_zero();
				Vector3 zero27 = Vector3.get_zero();
				Vector3 zero28 = Vector3.get_zero();
				Vector3 zero29 = Vector3.get_zero();
				Vector3 zero30 = Vector3.get_zero();
				Vector3 zero31 = Vector3.get_zero();
				Vector3 zero32 = Vector3.get_zero();
				ushort num24 = 0;
				ushort num25 = 0;
				bool flag14 = false;
				int num26 = 0;
				for (int num27 = 0; num27 < 8; num27++)
				{
					ushort segment7 = this.GetSegment(num27);
					if (segment7 != 0)
					{
						NetSegment netSegment6 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment7];
						bool flag15 = ++num26 == 1;
						bool flag16 = netSegment6.m_startNode == nodeID;
						if ((!flag15 && !flag14) || (flag15 && !flag16))
						{
							bool flag17;
							netSegment6.CalculateCorner(segment7, true, flag16, false, out zero25, out zero29, out flag17);
							netSegment6.CalculateCorner(segment7, true, flag16, true, out zero26, out zero30, out flag17);
							flag14 = true;
							num24 = segment7;
						}
						else
						{
							bool flag17;
							netSegment6.CalculateCorner(segment7, true, flag16, true, out zero27, out zero31, out flag17);
							netSegment6.CalculateCorner(segment7, true, flag16, false, out zero28, out zero32, out flag17);
							num25 = segment7;
						}
					}
				}
				Vector3 vector66;
				Vector3 vector67;
				NetSegment.CalculateMiddlePoints(zero25, -zero29, zero27, -zero31, true, true, out vector66, out vector67);
				Vector3 vector68;
				Vector3 vector69;
				NetSegment.CalculateMiddlePoints(zero26, -zero30, zero28, -zero32, true, true, out vector68, out vector69);
				Matrix4x4 leftMatrix6 = NetSegment.CalculateControlMatrix(zero25, vector66, vector67, zero27, zero26, vector68, vector69, zero28, groupPosition, vScale5);
				Matrix4x4 rightMatrix6 = NetSegment.CalculateControlMatrix(zero26, vector68, vector69, zero28, zero25, vector66, vector67, zero27, groupPosition, vScale5);
				Vector4 vector70;
				vector70..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
				Vector4 colorLocation6 = RenderManager.GetColorLocation(86016u + (uint)nodeID);
				Vector4 vector71;
				vector71..ctor(colorLocation6.x, colorLocation6.y, colorLocation6.x, colorLocation6.y);
				if (info.m_segments != null && info.m_segments.Length != 0)
				{
					for (int num28 = 0; num28 < info.m_segments.Length; num28++)
					{
						NetInfo.Segment segment8 = info.m_segments[num28];
						bool flag18;
						if (segment8.m_layer == layer && segment8.CheckFlags(info.m_netAI.GetBendFlags(nodeID, ref this), out flag18) && segment8.m_combinedLod != null && !segment8.m_disableBendNodes)
						{
							Vector4 objectIndex6 = vector71;
							Vector4 meshScale6 = vector70;
							if (segment8.m_requireWindSpeed)
							{
								objectIndex6.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
							}
							if (flag18)
							{
								meshScale6.x = -meshScale6.x;
								meshScale6.y = -meshScale6.y;
							}
							flag = true;
							NetSegment.PopulateGroupData(info, segment8, leftMatrix6, rightMatrix6, meshScale6, objectIndex6, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
						}
					}
				}
				if (info.m_nodes != null && info.m_nodes.Length != 0)
				{
					for (int num29 = 0; num29 < info.m_nodes.Length; num29++)
					{
						NetInfo.Node node5 = info.m_nodes[num29];
						if ((node5.m_connectGroup == NetInfo.ConnectGroup.None || (node5.m_connectGroup & info.m_connectGroup & NetInfo.ConnectGroup.AllGroups) != NetInfo.ConnectGroup.None) && node5.m_layer == layer && node5.CheckFlags(this.m_flags) && node5.m_combinedLod != null && node5.m_directConnect)
						{
							Vector4 objectIndex7 = vector71;
							Vector4 meshScale7 = vector70;
							if (node5.m_requireWindSpeed)
							{
								objectIndex7.w = Singleton<WeatherManager>.get_instance().GetWindSpeed(this.m_position);
							}
							if ((node5.m_connectGroup & NetInfo.ConnectGroup.Oneway) != NetInfo.ConnectGroup.None)
							{
								NetManager instance2 = Singleton<NetManager>.get_instance();
								bool flag19 = instance2.m_segments.m_buffer[(int)num24].m_startNode == nodeID == ((instance2.m_segments.m_buffer[(int)num24].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
								bool flag20 = instance2.m_segments.m_buffer[(int)num25].m_startNode == nodeID == ((instance2.m_segments.m_buffer[(int)num25].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None);
								if (flag19 == flag20)
								{
									goto IL_21B3;
								}
								if (flag19)
								{
									if ((node5.m_connectGroup & NetInfo.ConnectGroup.OnewayStart) == NetInfo.ConnectGroup.None)
									{
										goto IL_21B3;
									}
								}
								else
								{
									if ((node5.m_connectGroup & NetInfo.ConnectGroup.OnewayEnd) == NetInfo.ConnectGroup.None)
									{
										goto IL_21B3;
									}
									meshScale7.x = -meshScale7.x;
									meshScale7.y = -meshScale7.y;
								}
							}
							flag = true;
							NetNode.PopulateGroupData(info, node5, leftMatrix6, rightMatrix6, meshScale7, objectIndex7, ref vertexIndex, ref triangleIndex, data, ref requireSurfaceMaps);
						}
						IL_21B3:;
					}
				}
			}
		}
		if (flag)
		{
			min = Vector3.Min(min, this.m_bounds.get_min());
			max = Vector3.Max(max, this.m_bounds.get_max());
			maxRenderDistance = Mathf.Max(maxRenderDistance, 30000f);
			maxInstanceDistance = Mathf.Max(maxInstanceDistance, 1000f);
		}
	}

	public static bool BlendJunction(ushort nodeID)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if ((instance.m_nodes.m_buffer[(int)nodeID].m_flags & (NetNode.Flags.Middle | NetNode.Flags.Bend)) != NetNode.Flags.None)
		{
			return true;
		}
		if ((instance.m_nodes.m_buffer[(int)nodeID].m_flags & NetNode.Flags.Junction) != NetNode.Flags.None)
		{
			bool flag = false;
			bool flag2 = false;
			int num = 0;
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)nodeID].GetSegment(i);
				if (segment != 0)
				{
					if (++num >= 3)
					{
						return false;
					}
					NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
					if (!info.m_enableMiddleNodes || info.m_requireContinuous)
					{
						return false;
					}
					bool flag3;
					bool flag4;
					if (instance.m_segments.m_buffer[(int)segment].m_startNode == nodeID == ((instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None))
					{
						flag3 = info.m_hasForwardVehicleLanes;
						flag4 = info.m_hasBackwardVehicleLanes;
					}
					else
					{
						flag3 = info.m_hasBackwardVehicleLanes;
						flag4 = info.m_hasForwardVehicleLanes;
					}
					if (num == 2)
					{
						if (flag3 != flag2 || flag4 != flag)
						{
							return false;
						}
					}
					else
					{
						flag = flag3;
						flag2 = flag4;
					}
				}
			}
			return num == 2;
		}
		return false;
	}

	public static void PopulateGroupData(NetInfo info, NetInfo.Node nodeInfo, Matrix4x4 leftMatrix, Matrix4x4 rightMatrix, Matrix4x4 leftMatrixB, Matrix4x4 rightMatrixB, Vector4 meshScale, Vector4 centerPos, Vector4 sideScale, Vector4 objectIndex, ref int vertexIndex, ref int triangleIndex, RenderGroup.MeshData data, ref bool requireSurfaceMaps)
	{
		if (nodeInfo.m_requireSurfaceMaps)
		{
			requireSurfaceMaps = true;
		}
		RenderGroup.MeshData data2 = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		int[] triangles = data2.m_triangles;
		int num = triangles.Length;
		for (int i = 0; i < num; i++)
		{
			data.m_triangles[triangleIndex++] = triangles[i] + vertexIndex;
		}
		RenderGroup.VertexArrays vertexArrays = data2.VertexArrayMask();
		Vector3[] vertices = data2.m_vertices;
		Vector3[] normals = data2.m_normals;
		Vector4[] tangents = data2.m_tangents;
		Vector2[] uvs = data2.m_uvs;
		Vector2[] uvs2 = data2.m_uvs3;
		Color32[] colors = data2.m_colors;
		int num2 = vertices.Length;
		Vector2 vector;
		vector..ctor(objectIndex.x, objectIndex.y);
		Vector2 vector2;
		vector2..ctor(-objectIndex.z, objectIndex.w);
		for (int j = 0; j < num2; j++)
		{
			Vector3 vector3 = vertices[j];
			vector3.x = Mathf.Abs(vector3.x * meshScale.x);
			vector3.z = vector3.z * meshScale.y + 0.5f;
			Vector2 vector4 = (vertices[j].x >= 0f) ? new Vector2(sideScale.z, sideScale.w) : new Vector2(sideScale.x, sideScale.y);
			Vector3 vector5;
			vector5..ctor(0.5f - vector4.x, vector4.x, vector4.y);
			Vector3 vector6;
			vector6..ctor(Mathf.Clamp01(vector3.x / meshScale.z), Mathf.Clamp01((vector3.x - meshScale.z) / meshScale.w), Mathf.Clamp01(vector3.x - 0.5f));
			vector3.x = Mathf.Lerp(vector3.x, Vector3.Dot(vector6, vector5), vector3.z * vector3.z);
			vector3.x = vector3.x * Mathf.Sign(vertices[j].x) + 0.5f;
			vector3.z *= 0.5f;
			Vector4 vector7;
			vector7..ctor(vector3.z, 1f - vector3.z, 3f * vector3.z, -vector3.z);
			Vector4 vector8;
			vector8..ctor(vector7.y * vector7.y * vector7.y, vector7.z * vector7.y * vector7.y, vector7.z * vector7.x * vector7.y, vector7.x * vector7.x * vector7.x);
			Vector4 vector9;
			vector9..ctor(vector7.y * (-1f - vector7.w) * 3f, vector7.y * (1f - vector7.z) * 3f, vector7.x * (2f - vector7.z) * 3f, vector7.x * -vector7.w * 3f);
			float num3 = MathUtils.SmoothStep(-1f, 1f, vertices[j].x);
			float num4 = MathUtils.SmoothStep(0.3f, 0.5f, vector3.z - Mathf.Abs(num3 - 0.5f));
			Vector4 vector10 = leftMatrix * vector8;
			Vector4 vector11 = leftMatrixB * vector8;
			Vector4 vector12 = vector10 + (vector11 - vector10) * num3;
			Vector4 vector13 = rightMatrix * vector8;
			Vector4 vector14 = rightMatrixB * vector8;
			Vector4 vector15 = vector13 + (vector14 - vector13) * num3;
			Vector4 vector16 = vector12 + (vector15 - vector12) * vector3.x;
			vector16 += (centerPos - vector16) * num4;
			Vector4 vector17 = leftMatrix * vector9;
			Vector4 vector18 = leftMatrixB * vector9;
			Vector4 vector19 = vector17 + (vector18 - vector17) * num3;
			Vector4 vector20 = rightMatrix * vector9;
			Vector4 vector21 = rightMatrixB * vector9;
			Vector4 vector22 = vector20 + (vector21 - vector20) * num3;
			Vector3 vector23 = vector19 + (vector22 - vector19) * vector3.x;
			if (vector23.get_sqrMagnitude() < 0.01f)
			{
				vector23 = Vector3.get_forward();
			}
			else
			{
				vector23 = Vector3.Normalize(vector23);
			}
			Vector3 vector24 = Vector3.Normalize(new Vector3(vector23.z, 0f, -vector23.x));
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetColumn(0, vector24);
			matrix4x.SetColumn(1, Vector3.Cross(vector23, vector24));
			matrix4x.SetColumn(2, vector23);
			data.m_vertices[vertexIndex] = new Vector3(vector16.x, vector16.y + vector3.y, vector16.z);
			if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
			{
				data.m_normals[vertexIndex] = matrix4x.MultiplyVector(normals[j]);
			}
			else
			{
				data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
			{
				Vector4 vector25 = tangents[j];
				Vector3 vector26 = matrix4x.MultiplyVector(vector25);
				vector25.x = vector26.x;
				vector25.y = vector26.y;
				vector25.z = vector26.z;
				data.m_tangents[vertexIndex] = vector25;
			}
			else
			{
				data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
			}
			Color32 color;
			if ((vertexArrays & RenderGroup.VertexArrays.Colors) != (RenderGroup.VertexArrays)0)
			{
				color = colors[j];
			}
			else
			{
				color..ctor(255, 255, 255, 255);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
			{
				Vector2 vector27 = uvs[j];
				vector27.y = Mathf.Lerp(vector27.y, Mathf.Min(0.9f, vector16.w), (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector27;
			}
			else
			{
				Vector2 vector28 = default(Vector2);
				vector28.y = Mathf.Lerp(vector28.y, Mathf.Min(0.9f, vector16.w), (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector28;
			}
			data.m_colors[vertexIndex] = info.m_netAI.GetGroupVertexColor(nodeInfo, j, vector16.w);
			data.m_uvs2[vertexIndex] = vector;
			data.m_uvs4[vertexIndex] = vector2;
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs3) != (RenderGroup.VertexArrays)0)
			{
				data.m_uvs3[vertexIndex] = uvs2[j];
			}
			vertexIndex++;
		}
	}

	public static void PopulateGroupData(NetInfo info, NetInfo.Node nodeInfo, Matrix4x4 leftMatrix, Matrix4x4 rightMatrix, Vector4 meshScale, Vector4 objectIndex, ref int vertexIndex, ref int triangleIndex, RenderGroup.MeshData data, ref bool requireSurfaceMaps)
	{
		if (nodeInfo.m_requireSurfaceMaps)
		{
			requireSurfaceMaps = true;
		}
		RenderGroup.MeshData data2 = nodeInfo.m_combinedLod.m_key.m_mesh.m_data;
		int[] triangles = data2.m_triangles;
		int num = triangles.Length;
		for (int i = 0; i < num; i++)
		{
			data.m_triangles[triangleIndex++] = triangles[i] + vertexIndex;
		}
		RenderGroup.VertexArrays vertexArrays = data2.VertexArrayMask();
		Vector3[] vertices = data2.m_vertices;
		Vector3[] normals = data2.m_normals;
		Vector4[] tangents = data2.m_tangents;
		Vector2[] uvs = data2.m_uvs;
		Vector2[] uvs2 = data2.m_uvs3;
		Color32[] colors = data2.m_colors;
		int num2 = vertices.Length;
		Vector2 vector;
		vector..ctor(objectIndex.x, objectIndex.y);
		Vector2 vector2;
		vector2..ctor(objectIndex.z, objectIndex.w);
		for (int j = 0; j < num2; j++)
		{
			Vector3 vector3 = vertices[j];
			vector3.x = vector3.x * meshScale.x + 0.5f;
			vector3.z = vector3.z * meshScale.y + 0.5f;
			Vector4 vector4;
			vector4..ctor(vector3.z, 1f - vector3.z, 3f * vector3.z, -vector3.z);
			Vector4 vector5;
			vector5..ctor(vector4.y * vector4.y * vector4.y, vector4.z * vector4.y * vector4.y, vector4.z * vector4.x * vector4.y, vector4.x * vector4.x * vector4.x);
			Vector4 vector6;
			vector6..ctor(vector4.y * (-1f - vector4.w) * 3f, vector4.y * (1f - vector4.z) * 3f, vector4.x * (2f - vector4.z) * 3f, vector4.x * -vector4.w * 3f);
			Vector4 vector7 = leftMatrix * vector5;
			Vector4 vector8 = rightMatrix * vector5;
			Vector4 vector9 = vector7 + (vector8 - vector7) * vector3.x;
			Vector4 vector10 = leftMatrix * vector6;
			Vector4 vector11 = rightMatrix * vector6;
			Vector3 vector12 = Vector3.Normalize(vector10 + (vector11 - vector10) * vector3.x);
			Vector3 vector13 = Vector3.Normalize(new Vector3(vector12.z, 0f, -vector12.x));
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetColumn(0, vector13);
			matrix4x.SetColumn(1, Vector3.Cross(vector12, vector13));
			matrix4x.SetColumn(2, vector12);
			data.m_vertices[vertexIndex] = new Vector3(vector9.x, vector9.y + vector3.y, vector9.z);
			if ((vertexArrays & RenderGroup.VertexArrays.Normals) != (RenderGroup.VertexArrays)0)
			{
				Vector3 vector14 = normals[j];
				if (meshScale.x < 0f)
				{
					vector14.x = -vector14.x;
				}
				if (meshScale.y < 0f)
				{
					vector14.z = -vector14.z;
				}
				data.m_normals[vertexIndex] = matrix4x.MultiplyVector(vector14);
			}
			else
			{
				data.m_normals[vertexIndex] = new Vector3(0f, 1f, 0f);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Tangents) != (RenderGroup.VertexArrays)0)
			{
				Vector4 vector15 = tangents[j];
				if (meshScale.x < 0f)
				{
					vector15.x = -vector15.x;
				}
				if (meshScale.y < 0f)
				{
					vector15.z = -vector15.z;
				}
				Vector3 vector16 = matrix4x.MultiplyVector(vector15);
				vector15.x = vector16.x;
				vector15.y = vector16.y;
				vector15.z = vector16.z;
				data.m_tangents[vertexIndex] = vector15;
			}
			else
			{
				data.m_tangents[vertexIndex] = new Vector4(1f, 0f, 0f, 1f);
			}
			Color32 color;
			if ((vertexArrays & RenderGroup.VertexArrays.Colors) != (RenderGroup.VertexArrays)0)
			{
				color = colors[j];
			}
			else
			{
				color..ctor(255, 255, 255, 255);
			}
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs) != (RenderGroup.VertexArrays)0)
			{
				Vector2 vector17 = uvs[j];
				vector17.y = Mathf.Lerp(vector17.y, vector9.w, (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector17;
			}
			else
			{
				Vector2 vector18 = default(Vector2);
				vector18.y = Mathf.Lerp(vector18.y, vector9.w, (float)color.g * 0.003921569f);
				data.m_uvs[vertexIndex] = vector18;
			}
			data.m_colors[vertexIndex] = info.m_netAI.GetGroupVertexColor(nodeInfo, j, vector3.z);
			data.m_uvs2[vertexIndex] = vector;
			data.m_uvs4[vertexIndex] = vector2;
			if ((vertexArrays & RenderGroup.VertexArrays.Uvs3) != (RenderGroup.VertexArrays)0)
			{
				data.m_uvs3[vertexIndex] = uvs2[j];
			}
			vertexIndex++;
		}
	}

	public Notification.Problem m_problems;

	public Bounds m_bounds;

	public Vector3 m_position;

	public NetNode.Flags m_flags;

	public uint m_buildIndex;

	public uint m_lane;

	public ushort m_segment0;

	public ushort m_segment1;

	public ushort m_segment2;

	public ushort m_segment3;

	public ushort m_segment4;

	public ushort m_segment5;

	public ushort m_segment6;

	public ushort m_segment7;

	public ushort m_building;

	public ushort m_transportLine;

	public ushort m_nextLaneNode;

	public ushort m_nextBuildingNode;

	public ushort m_nextGridNode;

	public ushort m_tempCounter;

	public ushort m_finalCounter;

	public ushort m_infoIndex;

	public byte m_laneOffset;

	public byte m_elevation;

	public byte m_heightOffset;

	public byte m_connectCount;

	public byte m_maxWaitTime;

	public byte m_coverage;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Original = 4,
		Disabled = 8,
		End = 16,
		Middle = 32,
		Bend = 64,
		Junction = 128,
		Moveable = 256,
		Untouchable = 512,
		Outside = 1024,
		Temporary = 2048,
		Double = 4096,
		Fixed = 8192,
		OnGround = 16384,
		Ambiguous = 32768,
		Water = 65536,
		Sewage = 131072,
		ForbidLaneConnection = 262144,
		Underground = 524288,
		Transition = 1048576,
		LevelCrossing = 2097152,
		OneWayOut = 4194304,
		TrafficLights = 8388608,
		OneWayIn = 16777216,
		Heating = 33554432,
		Electricity = 67108864,
		Collapsed = 134217728,
		DisableOnlyMiddle = 268435456,
		AsymForward = 536870912,
		AsymBackward = 1073741824,
		CustomTrafficLights = -2147483648,
		OneWayOutTrafficLights = 12582912,
		UndergroundTransition = 1572864,
		All = -1
	}
}
