﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ColossalFramework;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class DayNightProperties : MonoBehaviour
{
	public DayNightProperties()
	{
		Gradient gradient = new Gradient();
		gradient.set_colorKeys(new GradientColorKey[]
		{
			new GradientColorKey(new Color32(50, 71, 99, 255), 0.225f),
			new GradientColorKey(new Color32(74, 107, 148, 255), 0.25f),
			new GradientColorKey(new Color32(74, 107, 148, 255), 0.75f),
			new GradientColorKey(new Color32(50, 71, 99, 255), 0.775f)
		});
		gradient.set_alphaKeys(new GradientAlphaKey[]
		{
			new GradientAlphaKey(1f, 0f),
			new GradientAlphaKey(1f, 1f)
		});
		this.m_NightZenithColor = gradient;
		this.m_NightHorizonColor = new Color(0.43f, 0.47f, 0.5f, 1f);
		this.m_SunAnisotropyFactor = 0.76f;
		this.m_SunSize = 1f;
		this.m_MoonSize = 0.15f;
		this.m_NightSky = DayNightProperties.NightSkyOptions.Rotated;
		this.m_StarsIntensity = 1f;
		this.m_OuterSpaceIntensity = 0.25f;
		this.m_MoonInnerCorona = new Color(1f, 1f, 1f, 0.5f);
		this.m_MoonOuterCorona = new Color(0.25f, 0.39f, 0.5f, 0.5f);
		this.m_UseLinearSpace = true;
		this.BetaM = new Vector3(0.004f, 0.004f, 0.004f) * 0.9f;
		gradient = new Gradient();
		gradient.set_colorKeys(new GradientColorKey[]
		{
			new GradientColorKey(new Color32(55, 66, 77, 255), 0.23f),
			new GradientColorKey(new Color32(245, 173, 84, 255), 0.26f),
			new GradientColorKey(new Color32(249, 208, 144, 255), 0.32f),
			new GradientColorKey(new Color32(252, 222, 186, 255), 0.5f),
			new GradientColorKey(new Color32(249, 208, 144, 255), 0.68f),
			new GradientColorKey(new Color32(245, 173, 84, 255), 0.74f),
			new GradientColorKey(new Color32(55, 66, 77, 255), 0.77f)
		});
		gradient.set_alphaKeys(new GradientAlphaKey[]
		{
			new GradientAlphaKey(1f, 0f),
			new GradientAlphaKey(1f, 1f)
		});
		this.m_LightColor = gradient;
		this.m_SunIntensity = 1f;
		this.m_MoonIntensity = 1f;
		this.m_bloomIntensity = AnimationCurve.Linear(0f, 0.02f, 24f, 0.02f);
		this.m_bloomThreshold = AnimationCurve.Linear(0f, 4f, 24f, 4f);
		this.m_bloomSampleDistance = AnimationCurve.Linear(0f, 6.31f, 24f, 6.31f);
		this.m_DayBokehTreshold = 4f;
		this.m_NightBokehTreshold = 0.1f;
		this.m_filmGrainAmountScalar = new AnimationCurve(new Keyframe[]
		{
			new Keyframe(0f, 0.15f),
			new Keyframe(7f, 0.9f),
			new Keyframe(12f, 1f),
			new Keyframe(17f, 0.9f),
			new Keyframe(24f, 0.15f)
		});
		base..ctor();
	}

	public static DayNightProperties instance
	{
		get
		{
			return DayNightProperties.sInstance;
		}
	}

	private Bloom bloom
	{
		get
		{
			if (this.m_Bloom == null && Camera.get_main() != null)
			{
				this.m_Bloom = Camera.get_main().GetComponent<Bloom>();
			}
			return this.m_Bloom;
		}
	}

	private FilmGrainEffect filmGrain
	{
		get
		{
			if (this.m_FilmGrainEffect == null && Camera.get_main() != null)
			{
				this.m_FilmGrainEffect = Camera.get_main().GetComponent<FilmGrainEffect>();
			}
			return this.m_FilmGrainEffect;
		}
	}

	private DepthOfField depthOfField
	{
		get
		{
			if (this.m_DepthOfField == null && Camera.get_main() != null)
			{
				this.m_DepthOfField = Camera.get_main().GetComponent<DepthOfField>();
			}
			return this.m_DepthOfField;
		}
	}

	protected Material starMaterial
	{
		get
		{
			if (this.m_StarsMaterial == null)
			{
				this.m_StarsMaterial = new Material(Shader.Find("Hidden/DayNight/Stars"));
				this.m_StarsMaterial.set_hideFlags(52);
			}
			return this.m_StarsMaterial;
		}
	}

	protected Material skyboxMaterial
	{
		get
		{
			if (this.m_SkyboxMaterial == null)
			{
				this.m_SkyboxMaterial = new Material(Shader.Find("Hidden/DayNight/Skybox"));
				this.m_SkyboxMaterial.set_hideFlags(52);
			}
			return this.m_SkyboxMaterial;
		}
	}

	public Mesh starsMesh
	{
		get
		{
			if (this.m_StarsMesh == null)
			{
				this.m_StarsMesh = DayNightProperties.Starfield.Create();
			}
			return this.m_StarsMesh;
		}
	}

	public bool nightSkyEnabled
	{
		get
		{
			return this.m_NightSky != DayNightProperties.NightSkyOptions.Disabled;
		}
	}

	public Vector3 sunDir
	{
		get
		{
			return (!(this.m_SunLight != null)) ? new Vector3(0.321f, 0.766f, -0.557f) : (-this.m_SunLight.get_forward());
		}
	}

	private Matrix4x4 moonMatrix
	{
		get
		{
			if (this.m_MoonLight == null)
			{
				return Matrix4x4.TRS(Vector3.get_zero(), new Quaternion(-0.9238795f, 8.817204E-08f, 8.817204E-08f, 0.3826835f), Vector3.get_one());
			}
			Matrix4x4 worldToLocalMatrix = this.m_MoonLight.get_worldToLocalMatrix();
			worldToLocalMatrix.SetColumn(2, Vector4.Scale(new Vector4(1f, 1f, 1f, -1f), worldToLocalMatrix.GetColumn(2)));
			return worldToLocalMatrix;
		}
	}

	private Vector3 variableRangeWavelengths
	{
		get
		{
			return new Vector3(Mathf.Lerp(this.m_WaveLengths.x + 150f, this.m_WaveLengths.x - 150f, this.m_SkyTint.r), Mathf.Lerp(this.m_WaveLengths.y + 150f, this.m_WaveLengths.y - 150f, this.m_SkyTint.g), Mathf.Lerp(this.m_WaveLengths.z + 150f, this.m_WaveLengths.z - 150f, this.m_SkyTint.b));
		}
	}

	public Vector3 BetaR
	{
		get
		{
			Vector3 vector = this.variableRangeWavelengths * 1E-09f;
			Vector3 vector2;
			vector2..ctor(Mathf.Pow(vector.x, 4f), Mathf.Pow(vector.y, 4f), Mathf.Pow(vector.z, 4f));
			Vector3 vector3 = 7.635E+25f * vector2 * 5.755f;
			float num = 8f * Mathf.Pow(3.14159274f, 3f) * Mathf.Pow(0.0006001896f, 2f) * 6.105f;
			return 1000f * new Vector3(num / vector3.x, num / vector3.y, num / vector3.z);
		}
	}

	private Vector3 betaR_RayleighOffset
	{
		get
		{
			return this.BetaR * Mathf.Max(0.001f, this.m_RayleighScattering);
		}
	}

	public float uMuS
	{
		get
		{
			return Mathf.Atan(Mathf.Max(this.sunDir.y, -0.1975f) * 5.35f) / 1.1f + 0.739f;
		}
	}

	public float DayTime
	{
		get
		{
			return Mathf.Clamp01(this.uMuS);
		}
	}

	public float SunsetTime
	{
		get
		{
			return Mathf.Clamp01((this.uMuS - 1f) * (1.5f / Mathf.Pow(this.m_RayleighScattering, 4f)));
		}
	}

	public float NightTime
	{
		get
		{
			return 1f - this.DayTime;
		}
	}

	public Vector3 skyMultiplier
	{
		get
		{
			return new Vector3(this.SunsetTime, this.m_Exposure * 4f * this.DayTime * Mathf.Sqrt(this.m_RayleighScattering), this.NightTime);
		}
	}

	public Vector3 mieConst
	{
		get
		{
			return new Vector3(1f, this.BetaR.x / this.BetaR.y, this.BetaR.x / this.BetaR.z) * this.BetaM.x * this.m_MieScattering;
		}
	}

	public Vector3 miePhase_g
	{
		get
		{
			float num = this.m_SunAnisotropyFactor * this.m_SunAnisotropyFactor;
			float num2 = (!this.m_UseLinearSpace || !this.m_Tonemapping) ? 1f : 2f;
			return new Vector3(num2 * ((1f - num) / (2f + num)), 1f + num, 2f * this.m_SunAnisotropyFactor);
		}
	}

	private Vector3 bottomTint
	{
		get
		{
			float num = (!this.m_UseLinearSpace) ? 0.02f : 0.01f;
			return new Vector3(this.betaR_RayleighOffset.x / (this.m_GroundColor.r * num), this.betaR_RayleighOffset.y / (this.m_GroundColor.g * num), this.betaR_RayleighOffset.z / (this.m_GroundColor.b * num));
		}
	}

	public Vector2 colorCorrection
	{
		get
		{
			return (!this.m_UseLinearSpace || !this.m_Tonemapping) ? ((!this.m_UseLinearSpace) ? Vector2.get_one() : new Vector2(1f, 2f)) : new Vector2(0.38317f, 1.413f);
		}
	}

	public float normalizedTimeOfDay
	{
		get
		{
			return this.m_TimeOfDay / 24f;
		}
	}

	public Color nightHorizonColor
	{
		get
		{
			return this.m_NightHorizonColor * this.NightTime;
		}
	}

	public Color nightZenithColor
	{
		get
		{
			return this.m_NightZenithColor.Evaluate(this.normalizedTimeOfDay) * 0.01f;
		}
	}

	private float moonCoronaFactor
	{
		get
		{
			float num = -this.sunDir.y;
			float result;
			if (this.m_NightSky == DayNightProperties.NightSkyOptions.Rotated)
			{
				result = this.NightTime * num;
			}
			else
			{
				result = this.NightTime;
			}
			return result;
		}
	}

	private Vector4 moonInnerCorona
	{
		get
		{
			return new Vector4(this.m_MoonInnerCorona.r * this.moonCoronaFactor, this.m_MoonInnerCorona.g * this.moonCoronaFactor, this.m_MoonInnerCorona.b * this.moonCoronaFactor, 400f / this.m_MoonInnerCorona.a);
		}
	}

	private Vector4 moonOuterCorona
	{
		get
		{
			float num = (!this.m_UseLinearSpace) ? 8f : ((!this.m_Tonemapping) ? 12f : 16f);
			return new Vector4(this.m_MoonOuterCorona.r * 0.25f * this.moonCoronaFactor, this.m_MoonOuterCorona.g * 0.25f * this.moonCoronaFactor, this.m_MoonOuterCorona.b * 0.25f * this.moonCoronaFactor, num / this.m_MoonOuterCorona.a);
		}
	}

	private float starsBrightness
	{
		get
		{
			float num = (!this.m_UseLinearSpace) ? 1.5f : 1f;
			return this.m_StarsIntensity * this.NightTime * num;
		}
	}

	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		DayNightProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new DayNightProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnEnable()
	{
		DayNightProperties.sInstance = this;
		DayNightProperties.ID_MoonSampler = Shader.PropertyToID("_MoonSampler");
		DayNightProperties.ID_OuterSpaceCube = Shader.PropertyToID("_OuterSpaceCube");
		DayNightProperties.ID_SunDir = Shader.PropertyToID("_SunDir");
		DayNightProperties.ID_MoonMat = Shader.PropertyToID("_MoonMat");
		DayNightProperties.ID_BetaR = Shader.PropertyToID("_BetaR");
		DayNightProperties.ID_BetaM = Shader.PropertyToID("_BetaM");
		DayNightProperties.ID_SkyMultiplier = Shader.PropertyToID("_SkyMultiplier");
		DayNightProperties.ID_SunSize = Shader.PropertyToID("_SunSize");
		DayNightProperties.ID_MieConst = Shader.PropertyToID("_MieConst");
		DayNightProperties.ID_MiePhase_g = Shader.PropertyToID("_MiePhase_g");
		DayNightProperties.ID_GroundColor = Shader.PropertyToID("_GroundColor");
		DayNightProperties.ID_NightHorizonColor = Shader.PropertyToID("_NightHorizonColor");
		DayNightProperties.ID_NightZenithColor = Shader.PropertyToID("_NightZenithColor");
		DayNightProperties.ID_MoonInnerCorona = Shader.PropertyToID("_MoonInnerCorona");
		DayNightProperties.ID_MoonOuterCorona = Shader.PropertyToID("_MoonOuterCorona");
		DayNightProperties.ID_MoonSize = Shader.PropertyToID("_MoonSize");
		DayNightProperties.ID_ColorCorrection = Shader.PropertyToID("_ColorCorrection");
		DayNightProperties.ID_RotationMatrix = Shader.PropertyToID("_RotationMatrix");
		DayNightProperties.ID_OuterSpaceIntensity = Shader.PropertyToID("_OuterSpaceIntensity");
		DayNightProperties.ID_StarIntensity = Shader.PropertyToID("_StarIntensity");
		DayNightProperties.ID_SimulationDeltaTime = Shader.PropertyToID("_SimulationDeltaTime");
		if (this.m_SunLight == null)
		{
			GameObject gameObject = GameObject.FindGameObjectWithTag("MainLight");
			if (gameObject != null)
			{
				this.m_SunLight = gameObject.get_transform();
			}
			else
			{
				gameObject = GameObject.Find("Directional Light");
				if (gameObject != null)
				{
					this.m_SunLight = gameObject.get_transform();
				}
				else
				{
					Debug.Log("No sun light can be found");
				}
			}
		}
		if (this.m_MoonLight == null)
		{
			GameObject gameObject2 = GameObject.Find("Moon Light");
			if (gameObject2 != null)
			{
				this.m_MoonLight = gameObject2.get_transform();
			}
			else
			{
				Debug.Log("No moon light can be found");
			}
		}
		if (RenderSettings.get_ambientMode() != 1)
		{
			RenderSettings.set_ambientMode(1);
		}
		if (Camera.get_main() != null && Camera.get_main().get_clearFlags() != 1)
		{
			Camera.get_main().set_clearFlags(1);
		}
		this.UpdateSun();
		this.UpdateMoon();
		this.UpdateSkyboxMaterial();
		this.UpdateBloom();
		this.UpdateDOF();
	}

	private void UpdateSun()
	{
		float num = this.m_TimeOfDay * 360f / 24f - 90f;
		this.m_Euler = Quaternion.Euler(new Vector3(0f, this.m_Longitude, this.m_Latitude)) * Quaternion.Euler(num, 0f, 0f);
		if (this.m_SunLight != null)
		{
			this.m_SunLight.get_transform().set_rotation(this.m_Euler);
		}
		if (Singleton<InfoManager>.get_exists() && Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.None)
		{
			Vector3 localEulerAngles = this.m_SunLight.get_localEulerAngles();
			if (localEulerAngles.x < 0f)
			{
				localEulerAngles.x = -localEulerAngles.x;
			}
			else if (localEulerAngles.x > 180f)
			{
				localEulerAngles.x = 360f - localEulerAngles.x;
			}
			localEulerAngles.x = Mathf.Clamp(localEulerAngles.x, 50f, 130f);
			this.m_SunLight.set_localEulerAngles(localEulerAngles);
		}
	}

	private void UpdateMoon()
	{
		if (this.m_NightSky == DayNightProperties.NightSkyOptions.Rotated && this.m_MoonLight != null)
		{
			this.m_MoonLight.set_forward(this.sunDir);
		}
	}

	public void UpdateSharedAtmosphericProperties()
	{
		Shader.SetGlobalVector(DayNightProperties.ID_ColorCorrection, this.colorCorrection);
		Shader.SetGlobalVector(DayNightProperties.ID_BetaR, this.betaR_RayleighOffset);
		Shader.SetGlobalVector(DayNightProperties.ID_BetaM, this.BetaM);
		Shader.SetGlobalVector(DayNightProperties.ID_MieConst, this.mieConst);
		Shader.SetGlobalVector(DayNightProperties.ID_MiePhase_g, this.miePhase_g);
		Shader.SetGlobalVector(DayNightProperties.ID_NightHorizonColor, this.nightHorizonColor);
		Shader.SetGlobalVector(DayNightProperties.ID_NightZenithColor, this.nightZenithColor);
		Shader.SetGlobalVector(DayNightProperties.ID_SunDir, this.sunDir);
		Shader.SetGlobalVector(DayNightProperties.ID_SkyMultiplier, this.skyMultiplier);
		if (this.m_Tonemapping)
		{
			Shader.DisableKeyword("HDR_OFF");
			Shader.EnableKeyword("HDR_ON");
		}
		else
		{
			Shader.EnableKeyword("HDR_OFF");
			Shader.DisableKeyword("HDR_ON");
		}
	}

	private void UpdateSkyboxMaterial()
	{
		if (RenderSettings.get_skybox() != this.skyboxMaterial)
		{
			RenderSettings.set_skybox(this.skyboxMaterial);
		}
		this.UpdateSharedAtmosphericProperties();
		this.skyboxMaterial.SetTexture(DayNightProperties.ID_MoonSampler, this.m_MoonTexture);
		this.skyboxMaterial.SetTexture(DayNightProperties.ID_OuterSpaceCube, this.m_OuterSpaceCubemap);
		this.skyboxMaterial.SetMatrix(DayNightProperties.ID_MoonMat, this.moonMatrix);
		this.skyboxMaterial.SetFloat(DayNightProperties.ID_SunSize, 32f / this.m_SunSize);
		this.skyboxMaterial.SetVector(DayNightProperties.ID_GroundColor, this.bottomTint);
		this.skyboxMaterial.SetVector(DayNightProperties.ID_MoonInnerCorona, this.moonInnerCorona);
		this.skyboxMaterial.SetVector(DayNightProperties.ID_MoonOuterCorona, this.moonOuterCorona);
		this.skyboxMaterial.SetFloat(DayNightProperties.ID_MoonSize, this.m_MoonSize);
		if (this.m_SunLight != null && this.m_NightSky == DayNightProperties.NightSkyOptions.Rotated)
		{
			this.skyboxMaterial.SetMatrix(DayNightProperties.ID_RotationMatrix, this.m_SunLight.get_worldToLocalMatrix());
		}
		else
		{
			this.skyboxMaterial.SetMatrix(DayNightProperties.ID_RotationMatrix, Matrix4x4.get_identity());
		}
		this.skyboxMaterial.SetFloat(DayNightProperties.ID_OuterSpaceIntensity, this.m_OuterSpaceIntensity);
		if (this.starMaterial != null)
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				this.starMaterial.SetFloat(DayNightProperties.ID_SimulationDeltaTime, Singleton<SimulationManager>.get_instance().m_simulationTimeDelta);
			}
			this.starMaterial.SetFloat(DayNightProperties.ID_StarIntensity, this.starsBrightness);
			if (this.m_SunLight != null && this.m_NightSky == DayNightProperties.NightSkyOptions.Rotated)
			{
				this.starMaterial.SetMatrix(DayNightProperties.ID_RotationMatrix, this.m_SunLight.get_localToWorldMatrix());
			}
			else
			{
				this.starMaterial.SetMatrix(DayNightProperties.ID_RotationMatrix, Matrix4x4.get_identity());
			}
		}
	}

	private void Update()
	{
		this.Refresh();
	}

	public void Refresh()
	{
		if (this.m_TimeOfDay >= 24f)
		{
			this.m_TimeOfDay = 0f;
		}
		if (this.m_TimeOfDay < 0f)
		{
			this.m_TimeOfDay = 24f;
		}
		this.UpdateSun();
		this.UpdateMoon();
		this.UpdateSkyboxMaterial();
		this.UpdateBloom();
		this.UpdateFilmGrain();
		this.UpdateLighting();
		if (Singleton<RenderManager>.get_exists())
		{
			if (this.m_TimeOfDay < 12f)
			{
				Singleton<RenderManager>.get_instance().lightSystem.SetDaylightIntensity(this.m_TimeOfDay - 6f);
			}
			else
			{
				Singleton<RenderManager>.get_instance().lightSystem.SetDaylightIntensity(18f - this.m_TimeOfDay);
			}
		}
	}

	private void LateUpdate()
	{
		if (this.nightSkyEnabled && this.starsMesh != null && this.starMaterial != null && this.sunDir.y < 0.2f)
		{
			Graphics.DrawMesh(this.starsMesh, Vector3.get_zero(), Quaternion.get_identity(), this.starMaterial, 0);
		}
	}

	private void OnDisable()
	{
		if (this.m_StarsMesh != null)
		{
			Object.DestroyImmediate(this.m_StarsMesh);
		}
		if (this.m_StarsMaterial != null)
		{
			Object.DestroyImmediate(this.m_StarsMaterial);
		}
		if (this.m_SkyboxMaterial != null)
		{
			Object.DestroyImmediate(this.m_SkyboxMaterial);
		}
	}

	public Light sunLightSource
	{
		get
		{
			if (this.m_SunLightSource == null && this.m_SunLight != null)
			{
				this.m_SunLightSource = this.m_SunLight.GetComponent<Light>();
			}
			return this.m_SunLightSource;
		}
	}

	public Light moonLightSource
	{
		get
		{
			if (this.m_MoonLightSource == null && this.m_MoonLight != null)
			{
				this.m_MoonLightSource = this.m_MoonLight.GetComponent<Light>();
			}
			return this.m_MoonLightSource;
		}
	}

	public Color currentSkyColor
	{
		get
		{
			return this.ApplyColorOffset(this.m_AmbientColor.skyColor.Evaluate(this.normalizedTimeOfDay), 0.15f, 0.7f, false);
		}
	}

	public Color currentEquatorColor
	{
		get
		{
			return this.ApplyColorOffset(this.m_AmbientColor.equatorColor.Evaluate(this.normalizedTimeOfDay), 0.15f, 0.9f, false);
		}
	}

	public Color currentGroundColor
	{
		get
		{
			return this.ApplyColorOffset(this.m_AmbientColor.groundColor.Evaluate(this.normalizedTimeOfDay), 0.25f, 0.85f, true);
		}
	}

	public Color currentLightColor
	{
		get
		{
			return this.m_LightColor.Evaluate(this.normalizedTimeOfDay);
		}
	}

	private Color ApplyColorOffset(Color currentColor, float offsetRange, float rayleighOffsetRange, bool IsGround)
	{
		Vector3 vector = this.BetaR * 1000f;
		Vector3 vector2;
		vector2..ctor(0.5f, 0.5f, 0.5f);
		vector2..ctor(vector.x / 5.81f * 0.5f, vector.y / 13.57f * 0.5f, vector.z / 33.13f * 0.5f);
		if (!IsGround)
		{
			vector2 = Vector3.Lerp(new Vector3(Mathf.Abs(1f - vector2.x), Mathf.Abs(1f - vector2.y), Mathf.Abs(1f - vector2.z)), vector2, this.SunsetTime);
		}
		vector2 = Vector3.Lerp(new Vector3(0.5f, 0.5f, 0.5f), vector2, this.DayTime);
		Vector3 vector3 = default(Vector3);
		vector3..ctor(Mathf.Lerp(currentColor.r - offsetRange, currentColor.r + offsetRange, vector2.x), Mathf.Lerp(currentColor.g - offsetRange, currentColor.g + offsetRange, vector2.y), Mathf.Lerp(currentColor.b - offsetRange, currentColor.b + offsetRange, vector2.z));
		Vector3 vector4 = new Vector3(vector3.x / vector.x, vector3.y / vector.y, vector3.z / vector.z) * 4f;
		vector3 = ((this.m_RayleighScattering >= 1f) ? Vector3.Lerp(vector3, vector4, Mathf.Max(0f, this.m_RayleighScattering - 1f) / 4f * rayleighOffsetRange) : Vector3.Lerp(Vector3.get_zero(), vector3, this.m_RayleighScattering));
		return new Color(vector3.x, vector3.y, vector3.z, 1f) * this.m_Exposure;
	}

	private void UpdateLighting()
	{
		float num = QualitySettings.get_shadowDistance() * 5E-05f;
		float num2 = Mathf.Max(0.01f, num * (1.2f - num) - 0.01f);
		if (QualitySettings.get_shadowCascades() <= 1)
		{
			num2 *= 2f;
		}
		else if (QualitySettings.get_shadowCascades() <= 2)
		{
			num2 *= 1.5f;
		}
		if (Singleton<InfoManager>.get_exists() && Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.None && Singleton<InfoManager>.get_instance().m_properties != null)
		{
			RenderSettings.set_ambientSkyColor(Singleton<InfoManager>.get_instance().m_properties.m_ambientColor);
			RenderSettings.set_ambientEquatorColor(Singleton<InfoManager>.get_instance().m_properties.m_ambientColor);
			RenderSettings.set_ambientGroundColor(Singleton<InfoManager>.get_instance().m_properties.m_ambientColor);
			if (this.sunLightSource != null)
			{
				float num3 = Mathf.Max(0.01f, this.sunDir.y);
				num3 = 0.8752397f / Mathf.Sqrt(num3);
				this.sunLightSource.set_shadowBias(num2);
				this.sunLightSource.set_intensity(Singleton<InfoManager>.get_instance().m_properties.m_lightIntensity * num3);
				this.sunLightSource.set_color(Singleton<InfoManager>.get_instance().m_properties.m_lightColor);
				this.sunLightSource.set_enabled(true);
			}
			if (this.moonLightSource != null)
			{
				this.moonLightSource.set_enabled(false);
			}
			if (Singleton<RenderManager>.get_exists())
			{
				Singleton<RenderManager>.get_instance().MainLight = this.sunLightSource;
			}
		}
		else
		{
			RenderSettings.set_ambientSkyColor(this.currentSkyColor);
			RenderSettings.set_ambientEquatorColor(this.currentEquatorColor);
			RenderSettings.set_ambientGroundColor(this.currentGroundColor);
			if (this.sunLightSource != null)
			{
				this.sunLightSource.set_shadowBias(num2);
				this.sunLightSource.set_intensity(this.m_Exposure * this.m_SunIntensity * this.DayTime);
				this.sunLightSource.set_color(this.currentLightColor * this.DayTime);
				this.sunLightSource.set_enabled(this.normalizedTimeOfDay >= 0.24f && this.normalizedTimeOfDay <= 0.76f);
			}
			if (this.moonLightSource != null)
			{
				this.moonLightSource.set_shadowBias(num2);
				this.moonLightSource.set_intensity(this.m_Exposure * this.m_MoonIntensity * this.NightTime);
				this.moonLightSource.set_color(this.currentLightColor * this.NightTime);
				this.moonLightSource.set_enabled(this.normalizedTimeOfDay <= 0.26f || this.normalizedTimeOfDay >= 0.74f);
			}
			if (Singleton<RenderManager>.get_exists())
			{
				if (this.normalizedTimeOfDay < 0.25f || this.normalizedTimeOfDay > 0.75f)
				{
					Singleton<RenderManager>.get_instance().MainLight = this.moonLightSource;
				}
				else
				{
					Singleton<RenderManager>.get_instance().MainLight = this.sunLightSource;
				}
			}
		}
	}

	private void UpdateBloom()
	{
		if (this.bloom != null)
		{
			this.bloom.bloomIntensity = this.m_bloomIntensity.Evaluate(this.m_TimeOfDay);
			this.bloom.bloomThreshold = this.m_bloomThreshold.Evaluate(this.m_TimeOfDay);
			this.bloom.sepBlurSpread = this.m_bloomSampleDistance.Evaluate(this.m_TimeOfDay);
		}
	}

	private void UpdateDOF()
	{
		if (this.depthOfField != null)
		{
			this.depthOfField.dx11BokehThreshold = Mathf.Lerp(this.m_DayBokehTreshold, this.m_NightBokehTreshold, this.NightTime);
		}
	}

	private void UpdateFilmGrain()
	{
		if (this.filmGrain != null)
		{
			this.filmGrain.m_AmountScalar = this.m_filmGrainAmountScalar.Evaluate(this.m_TimeOfDay);
		}
	}

	private static DayNightProperties sInstance;

	private static int ID_MoonSampler;

	private static int ID_OuterSpaceCube;

	private static int ID_SunDir;

	private static int ID_MoonMat;

	private static int ID_BetaR;

	private static int ID_BetaM;

	private static int ID_SkyMultiplier;

	private static int ID_SunSize;

	private static int ID_MieConst;

	private static int ID_MiePhase_g;

	private static int ID_GroundColor;

	private static int ID_NightHorizonColor;

	private static int ID_NightZenithColor;

	private static int ID_MoonInnerCorona;

	private static int ID_MoonOuterCorona;

	private static int ID_MoonSize;

	private static int ID_ColorCorrection;

	private static int ID_RotationMatrix;

	private static int ID_OuterSpaceIntensity;

	private static int ID_StarIntensity;

	private static int ID_SimulationDeltaTime;

	[Header("Global Settings"), Range(0f, 24f)]
	public float m_TimeOfDay;

	[Range(-180f, 180f)]
	public float m_Longitude;

	[Range(-80f, 80f)]
	public float m_Latitude;

	public Transform m_SunLight;

	public Transform m_MoonLight;

	[Header("Sky Settings"), Range(0f, 5f)]
	public float m_RayleighScattering = 1f;

	[Range(0f, 5f)]
	public float m_MieScattering = 1f;

	[Range(0f, 5f)]
	public float m_Exposure = 1f;

	public Vector3 m_WaveLengths = new Vector3(680f, 550f, 440f);

	public Color m_SkyTint = new Color(0.5f, 0.5f, 0.5f, 1f);

	public Color m_GroundColor = new Color(0.369f, 0.349f, 0.341f, 1f);

	public Gradient m_NightZenithColor;

	public Color m_NightHorizonColor;

	[Header("Sun & Moon Settings"), Range(0f, 0.9995f)]
	public float m_SunAnisotropyFactor;

	[Range(0.001f, 10f)]
	public float m_SunSize;

	[Range(0f, 1f)]
	public float m_MoonSize;

	public DayNightProperties.NightSkyOptions m_NightSky;

	[Range(0f, 1f)]
	public float m_StarsIntensity;

	[Range(0f, 1f)]
	public float m_OuterSpaceIntensity;

	public Color m_MoonInnerCorona;

	public Color m_MoonOuterCorona;

	public Texture m_MoonTexture;

	public Cubemap m_OuterSpaceCubemap;

	private Material m_SkyboxMaterial;

	public bool m_UseLinearSpace;

	public bool m_Tonemapping;

	private Quaternion m_Euler;

	private Material m_StarsMaterial;

	private Mesh m_StarsMesh;

	private Bloom m_Bloom;

	private DepthOfField m_DepthOfField;

	private FilmGrainEffect m_FilmGrainEffect;

	private readonly Vector3 BetaM;

	[Header("Lighting")]
	public DayNightProperties.AmbientColor m_AmbientColor;

	public Gradient m_LightColor;

	[Range(0f, 8f)]
	public float m_SunIntensity;

	[Range(0f, 8f)]
	public float m_MoonIntensity;

	private Light m_SunLightSource;

	private Light m_MoonLightSource;

	[Header("Bloom")]
	public AnimationCurve m_bloomIntensity;

	public AnimationCurve m_bloomThreshold;

	public AnimationCurve m_bloomSampleDistance;

	[Header("DOF")]
	public float m_DayBokehTreshold;

	public float m_NightBokehTreshold;

	[Header("Film Grain")]
	public AnimationCurve m_filmGrainAmountScalar;

	public enum NightSkyOptions
	{
		Disabled = 1,
		Fixed,
		Rotated
	}

	private static class Starfield
	{
		private static Mesh CreateQuad(float size)
		{
			Vector3[] vertices = new Vector3[]
			{
				new Vector3(1f, 1f, 0f) * size,
				new Vector3(-1f, 1f, 0f) * size,
				new Vector3(1f, -1f, 0f) * size,
				new Vector3(-1f, -1f, 0f) * size
			};
			Vector2[] uv = new Vector2[]
			{
				new Vector2(0f, 1f),
				new Vector2(1f, 1f),
				new Vector2(0f, 0f),
				new Vector2(1f, 0f)
			};
			int[] triangles = new int[]
			{
				0,
				2,
				1,
				2,
				3,
				1
			};
			Mesh mesh = new Mesh();
			mesh.set_vertices(vertices);
			mesh.set_uv(uv);
			mesh.set_triangles(triangles);
			mesh.RecalculateNormals();
			mesh.set_hideFlags(52);
			return mesh;
		}

		private static Matrix4x4 BillboardMatrix(Vector3 particlePosition)
		{
			Vector3 vector = particlePosition - Vector3.get_zero();
			vector.Normalize();
			Vector3 vector2 = Vector3.Cross(vector, Vector3.get_up());
			vector2.Normalize();
			Vector3 vector3 = Vector3.Cross(vector2, vector);
			Matrix4x4 result = default(Matrix4x4);
			result.SetColumn(0, vector2);
			result.SetColumn(1, vector3);
			result.SetColumn(2, vector);
			result.SetColumn(3, particlePosition);
			return result;
		}

		public static Mesh Create()
		{
			List<CombineInstance> list = new List<CombineInstance>();
			float num = (!(Camera.get_main() != null)) ? ((!(Camera.get_current() != null)) ? 990f : Camera.get_current().get_farClipPlane()) : (Camera.get_main().get_farClipPlane() - 10f);
			float size = num / 100f;
			TextAsset textAsset = Resources.Load<TextAsset>("StarsData");
			if (textAsset == null)
			{
				Debug.Log("Can't find or read StarsData.bytes file.");
				return null;
			}
			DayNightProperties.Starfield.Star[] array = new DayNightProperties.Starfield.Star[9110];
			using (BinaryReader binaryReader = new BinaryReader(new MemoryStream(textAsset.get_bytes())))
			{
				for (int i = 0; i < 9110; i++)
				{
					array[i].position.x = binaryReader.ReadSingle();
					array[i].position.z = binaryReader.ReadSingle();
					array[i].position.y = binaryReader.ReadSingle();
					array[i].position = Vector3.Scale(array[i].position, new Vector3(-1f, 1f, -1f));
					array[i].color.r = binaryReader.ReadSingle();
					array[i].color.g = binaryReader.ReadSingle();
					array[i].color.b = binaryReader.ReadSingle();
					float a = Vector3.Dot(new Vector3(array[i].color.r, array[i].color.g, array[i].color.b), new Vector3(0.22f, 0.707f, 0.071f));
					array[i].color.a = a;
					if (array[i].color.a >= 0.0162f && array[i].color.a <= 2.2f)
					{
						CombineInstance item = default(CombineInstance);
						item.set_mesh(DayNightProperties.Starfield.CreateQuad(size));
						item.set_transform(DayNightProperties.Starfield.BillboardMatrix(array[i].position * num));
						Color[] colors = new Color[]
						{
							array[i].color,
							array[i].color,
							array[i].color,
							array[i].color
						};
						item.get_mesh().set_colors(colors);
						list.Add(item);
					}
				}
			}
			Mesh mesh = new Mesh();
			mesh.set_name("StarFieldMesh");
			mesh.CombineMeshes(list.ToArray());
			mesh.set_bounds(new Bounds(Vector3.get_zero(), Vector3.get_one() * 2E+09f));
			mesh.set_hideFlags(52);
			return mesh;
		}

		private struct Star
		{
			public Vector3 position;

			public Color color;
		}
	}

	[Serializable]
	public class AmbientColor
	{
		public AmbientColor()
		{
			Gradient gradient = new Gradient();
			gradient.set_colorKeys(new GradientColorKey[]
			{
				new GradientColorKey(new Color32(28, 32, 40, 255), 0.225f),
				new GradientColorKey(new Color32(55, 65, 63, 255), 0.25f),
				new GradientColorKey(new Color32(148, 179, 219, 255), 0.28f),
				new GradientColorKey(new Color32(148, 179, 219, 255), 0.72f),
				new GradientColorKey(new Color32(55, 65, 63, 255), 0.75f),
				new GradientColorKey(new Color32(28, 32, 40, 255), 0.775f)
			});
			gradient.set_alphaKeys(new GradientAlphaKey[]
			{
				new GradientAlphaKey(1f, 0f),
				new GradientAlphaKey(1f, 1f)
			});
			this.m_SkyColor = gradient;
			gradient = new Gradient();
			gradient.set_colorKeys(new GradientColorKey[]
			{
				new GradientColorKey(new Color32(20, 25, 36, 255), 0.225f),
				new GradientColorKey(new Color32(80, 70, 50, 255), 0.25f),
				new GradientColorKey(new Color32(102, 138, 168, 255), 0.28f),
				new GradientColorKey(new Color32(102, 138, 168, 255), 0.72f),
				new GradientColorKey(new Color32(80, 70, 50, 255), 0.75f),
				new GradientColorKey(new Color32(20, 25, 36, 255), 0.775f)
			});
			gradient.set_alphaKeys(new GradientAlphaKey[]
			{
				new GradientAlphaKey(1f, 0f),
				new GradientAlphaKey(1f, 1f)
			});
			this.m_EquatorColor = gradient;
			gradient = new Gradient();
			gradient.set_colorKeys(new GradientColorKey[]
			{
				new GradientColorKey(new Color32(20, 20, 20, 255), 0.24f),
				new GradientColorKey(new Color32(51, 51, 51, 255), 0.27f),
				new GradientColorKey(new Color32(51, 51, 51, 255), 0.73f),
				new GradientColorKey(new Color32(20, 20, 20, 255), 0.76f)
			});
			gradient.set_alphaKeys(new GradientAlphaKey[]
			{
				new GradientAlphaKey(1f, 0f),
				new GradientAlphaKey(1f, 1f)
			});
			this.m_GroundColor = gradient;
			base..ctor();
		}

		public Gradient skyColor
		{
			get
			{
				return this.m_SkyColor;
			}
		}

		public Gradient equatorColor
		{
			get
			{
				return this.m_EquatorColor;
			}
		}

		public Gradient groundColor
		{
			get
			{
				return this.m_GroundColor;
			}
		}

		[SerializeField]
		private Gradient m_SkyColor;

		[SerializeField]
		private Gradient m_EquatorColor;

		[SerializeField]
		private Gradient m_GroundColor;
	}
}
