﻿using System;
using System.Collections.Generic;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class ThreadingWrapper : IThreading
{
	public ThreadingWrapper(SimulationManager simulationManager)
	{
		this.m_simulationManager = simulationManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnThreadingExtensionsReleased();
		this.m_ThreadingExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IThreadingExtension>();
		this.OnThreadingExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnThreadingExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnThreadingExtensionsCreated()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			try
			{
				this.m_ThreadingExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_ThreadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnThreadingExtensionsReleased()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			try
			{
				this.m_ThreadingExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_ThreadingExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnUpdate(float realTimeDelta, float simulationTimeDelta)
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			this.m_ThreadingExtensions[i].OnUpdate(realTimeDelta, simulationTimeDelta);
		}
	}

	public void OnBeforeSimulationTick()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			this.m_ThreadingExtensions[i].OnBeforeSimulationTick();
		}
	}

	public void OnBeforeSimulationFrame()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			this.m_ThreadingExtensions[i].OnBeforeSimulationFrame();
		}
	}

	public void OnAfterSimulationFrame()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			this.m_ThreadingExtensions[i].OnAfterSimulationFrame();
		}
	}

	public void OnAfterSimulationTick()
	{
		for (int i = 0; i < this.m_ThreadingExtensions.Count; i++)
		{
			this.m_ThreadingExtensions[i].OnAfterSimulationTick();
		}
	}

	public DateTime renderTime
	{
		get
		{
			return this.m_simulationManager.m_currentGameTime;
		}
	}

	public uint renderFrame
	{
		get
		{
			return this.m_simulationManager.m_referenceFrameIndex;
		}
	}

	public float renderFrameOffset
	{
		get
		{
			return this.m_simulationManager.m_referenceTimer;
		}
	}

	public float renderDayTimeHour
	{
		get
		{
			return this.m_simulationManager.m_currentDayTimeHour;
		}
	}

	public DateTime simulationTime
	{
		get
		{
			return new DateTime((long)((ulong)this.m_simulationManager.m_currentFrameIndex * (ulong)this.m_simulationManager.m_timePerFrame.Ticks + (ulong)this.m_simulationManager.m_timeOffsetTicks));
		}
	}

	public uint simulationFrame
	{
		get
		{
			return this.m_simulationManager.m_currentFrameIndex;
		}
	}

	public uint simulationTick
	{
		get
		{
			return this.m_simulationManager.m_currentTickIndex;
		}
	}

	public bool simulationPaused
	{
		get
		{
			return this.m_simulationManager.SimulationPaused;
		}
		set
		{
			this.m_simulationManager.SimulationPaused = value;
		}
	}

	public int simulationSpeed
	{
		get
		{
			return this.m_simulationManager.SelectedSimulationSpeed;
		}
		set
		{
			this.m_simulationManager.SelectedSimulationSpeed = Mathf.Clamp(value, 1, 3);
		}
	}

	public float simulationDayTimeHour
	{
		get
		{
			float num = this.m_simulationManager.m_currentFrameIndex + this.m_simulationManager.m_dayTimeOffsetFrames & SimulationManager.DAYTIME_FRAMES - 1u;
			num *= SimulationManager.DAYTIME_FRAME_TO_HOUR;
			if (num >= 24f)
			{
				num -= 24f;
			}
			return num;
		}
		set
		{
			float num = Mathf.Clamp01(value / 24f);
			uint num2 = (uint)(num * SimulationManager.DAYTIME_FRAMES);
			uint currentFrameIndex = this.m_simulationManager.m_currentFrameIndex;
			this.m_simulationManager.m_dayTimeOffsetFrames = (num2 - currentFrameIndex & SimulationManager.DAYTIME_FRAMES - 1u);
			this.m_simulationManager.m_isNightTime = (value < SimulationManager.SUNRISE_HOUR || value > SimulationManager.SUNSET_HOUR);
		}
	}

	public bool simulationNightTime
	{
		get
		{
			return this.m_simulationManager.m_isNightTime;
		}
	}

	public TimeSpan timePerFrame
	{
		get
		{
			return this.m_simulationManager.m_timePerFrame;
		}
	}

	public void QueueMainThread(Action action)
	{
		if (Dispatcher.get_currentSafe() == ThreadHelper.get_dispatcher())
		{
			action();
		}
		else
		{
			ThreadHelper.get_dispatcher().Dispatch(action);
		}
	}

	public void QueueSimulationThread(Action action)
	{
		if (Thread.CurrentThread == this.m_simulationManager.m_simulationThread)
		{
			action();
		}
		else
		{
			this.m_simulationManager.AddAction(action);
		}
	}

	private SimulationManager m_simulationManager;

	private List<IThreadingExtension> m_ThreadingExtensions = new List<IThreadingExtension>();
}
