﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class WorldPropertiesPanel : UICustomControl
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.UnBindActions();
		this.m_MinTemperatureDay.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureDay.ToString("0.0"));
		this.m_MaxTemperatureDay.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureDay.ToString("0.0"));
		this.m_MinTemperatureNight.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureNight.ToString("0.0"));
		this.m_MaxTemperatureNight.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureNight.ToString("0.0"));
		this.m_MinTemperatureRain.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureRain.ToString("0.0"));
		this.m_MaxTemperatureRain.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureRain.ToString("0.0"));
		this.m_MinTemperatureFog.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureFog.ToString("0.0"));
		this.m_MaxTemperatureFog.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureFog.ToString("0.0"));
		this.m_RainProbabilityDay.set_value((float)Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityDay);
		this.m_RainProbabilityNight.set_value((float)Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityNight);
		this.m_FogProbabilityDay.set_value((float)Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityDay);
		this.m_FogProbabilityNight.set_value((float)Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityNight);
		this.m_NorthernLightsProbability.set_value((float)Singleton<WeatherManager>.get_instance().m_properties.m_northernLightsProbability);
		this.m_Defaults.Store<float>("minTempDay", Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureDay);
		this.m_Defaults.Store<float>("maxTempDay", Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureDay);
		this.m_Defaults.Store<float>("minTempNight", Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureNight);
		this.m_Defaults.Store<float>("maxTempNight", Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureNight);
		this.m_Defaults.Store<float>("minTempRain", Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureRain);
		this.m_Defaults.Store<float>("maxTempRain", Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureRain);
		this.m_Defaults.Store<float>("minTempFog", Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureFog);
		this.m_Defaults.Store<float>("maxTempFog", Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureFog);
		this.m_Defaults.Store<int>("rainProbDay", Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityDay);
		this.m_Defaults.Store<int>("rainProbNight", Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityNight);
		this.m_Defaults.Store<int>("fogProbDay", Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityDay);
		this.m_Defaults.Store<int>("fogProbNight", Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityNight);
		this.m_Defaults.Store<int>("northernLightsProb", Singleton<WeatherManager>.get_instance().m_properties.m_northernLightsProbability);
		this.BindActions();
	}

	public void ResetAll()
	{
		this.ResetTemperatures(null, null);
		this.ResetProbabilities(null, null);
	}

	public void ResetTemperatures(UIComponent c, UIMouseEventParameter p)
	{
		this.m_MinTemperatureDay.set_text(this.m_Defaults.Get<float>("minTempDay").ToString("0.0"));
		this.m_MaxTemperatureDay.set_text(this.m_Defaults.Get<float>("maxTempDay").ToString("0.0"));
		this.m_MinTemperatureNight.set_text(this.m_Defaults.Get<float>("minTempNight").ToString("0.0"));
		this.m_MaxTemperatureNight.set_text(this.m_Defaults.Get<float>("maxTempNight").ToString("0.0"));
		this.m_MinTemperatureRain.set_text(this.m_Defaults.Get<float>("minTempRain").ToString("0.0"));
		this.m_MaxTemperatureRain.set_text(this.m_Defaults.Get<float>("maxTempRain").ToString("0.0"));
		this.m_MinTemperatureFog.set_text(this.m_Defaults.Get<float>("minTempFog").ToString("0.0"));
		this.m_MaxTemperatureFog.set_text(this.m_Defaults.Get<float>("maxTempFog").ToString("0.0"));
	}

	public void ResetProbabilities(UIComponent c, UIMouseEventParameter p)
	{
		this.m_RainProbabilityDay.set_value((float)this.m_Defaults.Get<int>("rainProbDay"));
		this.m_RainProbabilityNight.set_value((float)this.m_Defaults.Get<int>("rainProbNight"));
		this.m_FogProbabilityDay.set_value((float)this.m_Defaults.Get<int>("fogProbDay"));
		this.m_FogProbabilityNight.set_value((float)this.m_Defaults.Get<int>("fogProbNight"));
		this.m_NorthernLightsProbability.set_value((float)this.m_Defaults.Get<int>("northernLightsProb"));
	}

	private void UnBindActions()
	{
		UIComponent uIComponent = base.Find("Temperature");
		this.m_ResetTemperaturesButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetTemperaturesButton.remove_eventClick(new MouseEventHandler(this.ResetTemperatures));
		this.m_MinTemperatureDay = uIComponent.Find("TempDay").Find<UITextField>("ValueMin");
		this.m_MinTemperatureDay.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureDayChanged));
		this.m_MaxTemperatureDay = uIComponent.Find("TempDay").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureDay.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureDayChanged));
		this.m_MinTemperatureNight = uIComponent.Find("TempNight").Find<UITextField>("ValueMin");
		this.m_MinTemperatureNight.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureNightChanged));
		this.m_MaxTemperatureNight = uIComponent.Find("TempNight").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureNight.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureNightChanged));
		this.m_MinTemperatureRain = uIComponent.Find("TempRain").Find<UITextField>("ValueMin");
		this.m_MinTemperatureRain.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureRainChanged));
		this.m_MaxTemperatureRain = uIComponent.Find("TempRain").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureRain.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureRainChanged));
		this.m_MinTemperatureFog = uIComponent.Find("TempFog").Find<UITextField>("ValueMin");
		this.m_MinTemperatureFog.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureFogChanged));
		this.m_MaxTemperatureFog = uIComponent.Find("TempFog").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureFog.remove_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureFogChanged));
		UIComponent uIComponent2 = base.Find("Weather");
		this.m_ResetProbabilitiesButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetProbabilitiesButton.remove_eventClick(new MouseEventHandler(this.ResetProbabilities));
		this.m_RainProbabilityDay = uIComponent2.Find("DayRainProb").Find<UISlider>("Slider");
		this.m_RainProbabilityDay.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRainProbabilityDay));
		this.m_RainProbabilityNight = uIComponent2.Find("NightRainProb").Find<UISlider>("Slider");
		this.m_RainProbabilityNight.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRainProbabilityNight));
		this.m_FogProbabilityDay = uIComponent2.Find("DayFogProb").Find<UISlider>("Slider");
		this.m_FogProbabilityDay.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignFogProbabilityDay));
		this.m_FogProbabilityNight = uIComponent2.Find("NightFogProb").Find<UISlider>("Slider");
		this.m_FogProbabilityNight.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignFogProbabilityNight));
		this.m_NorthernLightsProbability = uIComponent2.Find("NorthernLightsProb").Find<UISlider>("Slider");
		this.m_NorthernLightsProbability.remove_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignNorthernLightsProbability));
	}

	private static void CheckMinMaxLimits(UITextField minField, UITextField maxField, bool minPriority)
	{
		float num;
		float num2;
		if (float.TryParse(minField.get_text(), out num) && float.TryParse(maxField.get_text(), out num2) && num > num2)
		{
			if (minPriority)
			{
				maxField.set_text(num.ToString("0.0"));
			}
			else
			{
				minField.set_text(num2.ToString("0.0"));
			}
		}
	}

	private void OnMinTemperatureDayChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureDay = num;
		}
		this.m_MinTemperatureDay.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureDay.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureDay, this.m_MaxTemperatureDay, true);
	}

	private void OnMaxTemperatureDayChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureDay = num;
		}
		this.m_MaxTemperatureDay.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureDay.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureDay, this.m_MaxTemperatureDay, false);
	}

	private void OnMinTemperatureNightChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureNight = num;
		}
		this.m_MinTemperatureNight.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureNight.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureNight, this.m_MaxTemperatureNight, true);
	}

	private void OnMaxTemperatureNightChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureNight = num;
		}
		this.m_MaxTemperatureNight.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureNight.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureNight, this.m_MaxTemperatureNight, false);
	}

	private void OnMinTemperatureRainChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureRain = num;
		}
		this.m_MinTemperatureRain.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureRain.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureRain, this.m_MaxTemperatureRain, true);
	}

	private void OnMaxTemperatureRainChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureRain = num;
		}
		this.m_MaxTemperatureRain.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureRain.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureRain, this.m_MaxTemperatureRain, false);
	}

	private void OnMinTemperatureFogChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureFog = num;
		}
		this.m_MinTemperatureFog.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_minTemperatureFog.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureFog, this.m_MaxTemperatureFog, true);
	}

	private void OnMaxTemperatureFogChanged(UIComponent c, string t)
	{
		float num;
		if (float.TryParse(t, out num))
		{
			num = Mathf.Clamp(num, -99f, 99f);
			Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureFog = num;
		}
		this.m_MaxTemperatureFog.set_text(Singleton<WeatherManager>.get_instance().m_properties.m_maxTemperatureFog.ToString("0.0"));
		WorldPropertiesPanel.CheckMinMaxLimits(this.m_MinTemperatureFog, this.m_MaxTemperatureFog, false);
	}

	private void OnAssignRainProbabilityDay(UIComponent c, float t)
	{
		Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityDay = (int)t;
	}

	private void OnAssignRainProbabilityNight(UIComponent c, float t)
	{
		Singleton<WeatherManager>.get_instance().m_properties.m_rainProbabilityNight = (int)t;
	}

	private void OnAssignFogProbabilityDay(UIComponent c, float t)
	{
		Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityDay = (int)t;
	}

	private void OnAssignFogProbabilityNight(UIComponent c, float t)
	{
		Singleton<WeatherManager>.get_instance().m_properties.m_fogProbabilityNight = (int)t;
	}

	private void OnAssignNorthernLightsProbability(UIComponent c, float t)
	{
		Singleton<WeatherManager>.get_instance().m_properties.m_northernLightsProbability = (int)t;
	}

	private void BindActions()
	{
		UIComponent uIComponent = base.Find("Temperature");
		this.m_ResetTemperaturesButton = uIComponent.Find<UIButton>("Reset");
		this.m_ResetTemperaturesButton.add_eventClick(new MouseEventHandler(this.ResetTemperatures));
		this.m_MinTemperatureDay = uIComponent.Find("TempDay").Find<UITextField>("ValueMin");
		this.m_MinTemperatureDay.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureDayChanged));
		this.m_MaxTemperatureDay = uIComponent.Find("TempDay").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureDay.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureDayChanged));
		this.m_MinTemperatureNight = uIComponent.Find("TempNight").Find<UITextField>("ValueMin");
		this.m_MinTemperatureNight.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureNightChanged));
		this.m_MaxTemperatureNight = uIComponent.Find("TempNight").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureNight.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureNightChanged));
		this.m_MinTemperatureRain = uIComponent.Find("TempRain").Find<UITextField>("ValueMin");
		this.m_MinTemperatureRain.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureRainChanged));
		this.m_MaxTemperatureRain = uIComponent.Find("TempRain").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureRain.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureRainChanged));
		this.m_MinTemperatureFog = uIComponent.Find("TempFog").Find<UITextField>("ValueMin");
		this.m_MinTemperatureFog.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMinTemperatureFogChanged));
		this.m_MaxTemperatureFog = uIComponent.Find("TempFog").Find<UITextField>("ValueMax");
		this.m_MaxTemperatureFog.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnMaxTemperatureFogChanged));
		UIComponent uIComponent2 = base.Find("Weather");
		this.m_ResetProbabilitiesButton = uIComponent2.Find<UIButton>("Reset");
		this.m_ResetProbabilitiesButton.add_eventClick(new MouseEventHandler(this.ResetProbabilities));
		this.m_RainProbabilityDay = uIComponent2.Find("DayRainProb").Find<UISlider>("Slider");
		this.m_RainProbabilityDay.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRainProbabilityDay));
		this.m_RainProbabilityNight = uIComponent2.Find("NightRainProb").Find<UISlider>("Slider");
		this.m_RainProbabilityNight.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignRainProbabilityNight));
		this.m_FogProbabilityDay = uIComponent2.Find("DayFogProb").Find<UISlider>("Slider");
		this.m_FogProbabilityDay.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignFogProbabilityDay));
		this.m_FogProbabilityNight = uIComponent2.Find("NightFogProb").Find<UISlider>("Slider");
		this.m_FogProbabilityNight.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignFogProbabilityNight));
		this.m_NorthernLightsProbability = uIComponent2.Find("NorthernLightsProb").Find<UISlider>("Slider");
		this.m_NorthernLightsProbability.add_eventValueChanged(new PropertyChangedEventHandler<float>(this.OnAssignNorthernLightsProbability));
	}

	private void Awake()
	{
	}

	private void Start()
	{
		base.get_component().Hide();
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private ValueStorage m_Defaults;

	private UITextField m_MinTemperatureDay;

	private UITextField m_MaxTemperatureDay;

	private UITextField m_MinTemperatureNight;

	private UITextField m_MaxTemperatureNight;

	private UITextField m_MinTemperatureRain;

	private UITextField m_MaxTemperatureRain;

	private UITextField m_MinTemperatureFog;

	private UITextField m_MaxTemperatureFog;

	private UISlider m_RainProbabilityDay;

	private UISlider m_RainProbabilityNight;

	private UISlider m_FogProbabilityDay;

	private UISlider m_FogProbabilityNight;

	private UISlider m_NorthernLightsProbability;

	private UIButton m_ResetTemperaturesButton;

	private UIButton m_ResetProbabilitiesButton;
}
