﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class DisasterProperties : MonoBehaviour
{
	protected virtual void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		DisasterProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new DisasterProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	protected virtual void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("DisasterProperties");
			Singleton<DisasterManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	private void OverrideFromMapTheme()
	{
		MapThemeMetaData mapThemeMetaData = Singleton<SimulationManager>.get_instance().m_metaData.m_MapThemeMetaData;
		if (mapThemeMetaData != null)
		{
			DisasterProperties.DisasterSettings[] disasterSettings = mapThemeMetaData.disasterSettings;
			if (disasterSettings != null)
			{
				DisasterProperties.DisasterSettings[] array = disasterSettings;
				for (int i = 0; i < array.Length; i++)
				{
					DisasterProperties.DisasterSettings s = array[i];
					this.ApplySettings(s);
				}
			}
		}
	}

	private void ApplySettings(DisasterProperties.DisasterSettings s)
	{
		string disasterName = s.m_disasterName;
		for (int i = 0; i < this.m_disasterSettings.Length; i++)
		{
			if (this.m_disasterSettings[i].m_disasterName == disasterName)
			{
				this.m_disasterSettings[i] = s;
				break;
			}
		}
	}

	public Material m_markerMaterial;

	public EffectInfo m_mediumExplosion;

	public ManualMilestone m_safeDestructionMilestone;

	public Texture2D m_targetTexture;

	public AudioClip m_targetAcceptSound;

	public DisasterProperties.DisasterSettings[] m_disasterSettings;

	[Serializable]
	public struct DisasterSettings
	{
		public string m_disasterName;

		public int m_randomProbability;
	}
}
