﻿using System;
using ColossalFramework;
using UnityEngine;

public class PowerLineAI : PlayerNetAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_connectionEffect != null)
		{
			this.m_connectionEffect.InitializeEffect();
		}
		if (this.m_breakEffect != null)
		{
			this.m_breakEffect.InitializeEffect();
		}
	}

	public override void DestroyPrefab()
	{
		if (this.m_connectionEffect != null)
		{
			this.m_connectionEffect.ReleaseEffect();
		}
		if (this.m_breakEffect != null)
		{
			this.m_breakEffect.ReleaseEffect();
		}
		base.DestroyPrefab();
	}

	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Electricity)
		{
			return base.GetColor(segmentID, ref data, infoMode);
		}
		NetNode.Flags flags = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_startNode].m_flags;
		NetNode.Flags flags2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)data.m_endNode].m_flags;
		if ((flags & flags2 & NetNode.Flags.Electricity) != NetNode.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		Color inactiveColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		inactiveColor.a = 0f;
		return inactiveColor;
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.Electricity)
		{
			return base.GetColor(nodeID, ref data, infoMode);
		}
		if ((data.m_flags & NetNode.Flags.Electricity) != NetNode.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		Color inactiveColor = Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		inactiveColor.a = 0f;
		return inactiveColor;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.Electricity;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void NodeLoaded(ushort nodeID, ref NetNode data, uint version)
	{
		base.NodeLoaded(nodeID, ref data, version);
		if (version < 230u)
		{
			ushort building = data.m_building;
			if (building != 0 && Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_electricityBuffer != 0)
			{
				data.m_flags |= NetNode.Flags.Electricity;
			}
		}
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = 0;
		max = 2;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		building = this.m_powerPoleInfo;
		heightOffset = 0f;
	}

	public override void UpdateNode(ushort nodeID, ref NetNode data)
	{
		int num = data.CountSegments();
		if (num <= 1 && !Singleton<ElectricityManager>.get_instance().CheckConductivity(data.m_position))
		{
			data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.ElectricityNotConnected);
		}
		else
		{
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.ElectricityNotConnected);
		}
		ushort building = data.m_building;
		if (building != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			bool flag = false;
			int num2 = ((data.m_flags & NetNode.Flags.Electricity) == NetNode.Flags.None) ? 0 : 1;
			if ((int)instance.m_buildings.m_buffer[(int)building].m_electricityBuffer != num2)
			{
				instance.m_buildings.m_buffer[(int)building].m_electricityBuffer = (ushort)num2;
				flag = true;
			}
			if (flag)
			{
				instance.UpdateBuildingColors(building);
				NetManager instance2 = Singleton<NetManager>.get_instance();
				instance2.UpdateNodeColors(nodeID);
				for (int i = 0; i < 8; i++)
				{
					ushort segment = data.GetSegment(i);
					if (segment != 0)
					{
						instance2.UpdateSegmentColors(segment);
					}
				}
			}
		}
	}

	public override void AfterSplitOrMove(ushort nodeID, ref NetNode data, ushort origNodeID1, ushort origNodeID2)
	{
		base.AfterSplitOrMove(nodeID, ref data, origNodeID1, origNodeID2);
		ushort[] nodeGroups = Singleton<ElectricityManager>.get_instance().m_nodeGroups;
		if (nodeGroups[(int)origNodeID1] != 65535 && nodeGroups[(int)origNodeID2] != 65535)
		{
			nodeGroups[(int)nodeID] = nodeGroups[(int)origNodeID1];
		}
		else
		{
			nodeGroups[(int)nodeID] = 65535;
			if ((data.m_flags & NetNode.Flags.Electricity) != NetNode.Flags.None)
			{
				data.m_flags |= NetNode.Flags.Transition;
			}
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		instance.UpdateNodeColors(nodeID);
		for (int i = 0; i < 8; i++)
		{
			ushort segment = data.GetSegment(i);
			if (segment != 0)
			{
				instance.UpdateSegmentColors(segment);
			}
		}
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		return false;
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		int num = data.CountSegments();
		if (num <= 1 && !Singleton<ElectricityManager>.get_instance().CheckConductivity(data.m_position))
		{
			data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.ElectricityNotConnected);
		}
		else
		{
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.ElectricityNotConnected);
		}
		base.SimulationStep(nodeID, ref data);
	}

	public override void ConnectionSucceeded(ushort nodeID, ref NetNode data)
	{
		base.ConnectionSucceeded(nodeID, ref data);
		if (this.m_connectionEffect != null)
		{
			InstanceID instance = default(InstanceID);
			instance.NetNode = nodeID;
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(data.m_position, Vector3.get_up(), this.m_info.m_halfWidth);
			Singleton<EffectManager>.get_instance().DispatchEffect(this.m_connectionEffect, instance, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup);
		}
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			return base.CollapseSegment(segmentID, ref data, group, demolish);
		}
		if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			return false;
		}
		if (group != null)
		{
			ushort disaster = group.m_ownerInstance.Disaster;
			if (disaster != 0)
			{
				DisasterData[] expr_50_cp_0 = Singleton<DisasterManager>.get_instance().m_disasters.m_buffer;
				ushort expr_50_cp_1 = disaster;
				expr_50_cp_0[(int)expr_50_cp_1].m_destroyedPowerLength = expr_50_cp_0[(int)expr_50_cp_1].m_destroyedPowerLength + (uint)Mathf.RoundToInt(data.m_averageLength);
			}
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
		if (instance.m_nodes.m_buffer[(int)data.m_startNode].CountSegments() == 1)
		{
			ushort building = instance.m_nodes.m_buffer[(int)data.m_startNode].m_building;
			if (building != 0)
			{
				BuildingInfo info = instance2.m_buildings.m_buffer[(int)building].Info;
				if (info.m_buildingAI.CollapseBuilding(building, ref instance2.m_buildings.m_buffer[(int)building], group, false, false, 255))
				{
					instance.m_nodes.m_buffer[(int)data.m_startNode].m_building = 0;
					Building[] expr_126_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_126_cp_1 = building;
					expr_126_cp_0[(int)expr_126_cp_1].m_flags = (expr_126_cp_0[(int)expr_126_cp_1].m_flags | Building.Flags.Original);
					Building[] expr_144_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_144_cp_1 = building;
					expr_144_cp_0[(int)expr_144_cp_1].m_flags = (expr_144_cp_0[(int)expr_144_cp_1].m_flags & ~Building.Flags.Untouchable);
				}
			}
		}
		if (instance.m_nodes.m_buffer[(int)data.m_endNode].CountSegments() == 1)
		{
			ushort building2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_building;
			if (building2 != 0)
			{
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)building2].Info;
				if (info2.m_buildingAI.CollapseBuilding(building2, ref instance2.m_buildings.m_buffer[(int)building2], group, false, false, 255))
				{
					instance.m_nodes.m_buffer[(int)data.m_endNode].m_building = 0;
					Building[] expr_20B_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_20B_cp_1 = building2;
					expr_20B_cp_0[(int)expr_20B_cp_1].m_flags = (expr_20B_cp_0[(int)expr_20B_cp_1].m_flags | Building.Flags.Original);
					Building[] expr_22A_cp_0 = instance2.m_buildings.m_buffer;
					ushort expr_22A_cp_1 = building2;
					expr_22A_cp_0[(int)expr_22A_cp_1].m_flags = (expr_22A_cp_0[(int)expr_22A_cp_1].m_flags & ~Building.Flags.Untouchable);
				}
			}
		}
		if (this.m_breakEffect != null)
		{
			if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0)
			{
				InstanceID instance3 = default(InstanceID);
				instance3.NetNode = data.m_startNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
				position.y += (this.m_info.m_minHeight + this.m_info.m_maxHeight) * 0.5f;
				EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(position, Vector3.get_up(), this.m_info.m_halfWidth);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_breakEffect, instance3, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup);
			}
			else
			{
				InstanceID instance4 = default(InstanceID);
				instance4.NetNode = data.m_endNode;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
				position2.y += (this.m_info.m_minHeight + this.m_info.m_maxHeight) * 0.5f;
				EffectInfo.SpawnArea spawnArea2 = new EffectInfo.SpawnArea(position2, Vector3.get_up(), this.m_info.m_halfWidth);
				Singleton<EffectManager>.get_instance().DispatchEffect(this.m_breakEffect, instance4, spawnArea2, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup);
			}
		}
		Singleton<NetManager>.get_instance().ReleaseSegment(segmentID, false);
		return true;
	}

	public override float GetTerrainLowerOffset()
	{
		return Mathf.Max(0f, this.m_info.m_minHeight - 3f);
	}

	public BuildingInfo m_powerPoleInfo;

	public EffectInfo m_connectionEffect;

	public EffectInfo m_breakEffect;
}
