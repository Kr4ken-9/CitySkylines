﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class LevelUpWrapper : ILevelUp
{
	public LevelUpWrapper(BuildingManager buildingManager)
	{
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnLevelUpExtensionsReleased();
		this.m_LevelUpExtensions = Singleton<PluginManager>.get_instance().GetImplementations<ILevelUpExtension>();
		this.OnLevelUpExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnLevelUpExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnLevelUpExtensionsCreated()
	{
		for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
		{
			try
			{
				this.m_LevelUpExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_LevelUpExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnLevelUpExtensionsReleased()
	{
		for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
		{
			try
			{
				this.m_LevelUpExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_LevelUpExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public void OnCalculateResidentialLevelUp(ref ItemClass.Level targetLevel, ref int educationProgress, ref int landValueProgress, ref bool landValueTooLow, int averageEducation, int landValue, ushort buildingID, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level currentLevel)
	{
		if (this.m_LevelUpExtensions.Count != 0)
		{
			ResidentialLevelUp residentialLevelUp;
			residentialLevelUp.targetLevel = targetLevel;
			residentialLevelUp.educationProgress = educationProgress;
			residentialLevelUp.landValueProgress = landValueProgress;
			residentialLevelUp.landValueTooLow = landValueTooLow;
			for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
			{
				residentialLevelUp = this.m_LevelUpExtensions[i].OnCalculateResidentialLevelUp(residentialLevelUp, averageEducation, landValue, buildingID, service, subService, currentLevel);
			}
			targetLevel = residentialLevelUp.targetLevel;
			educationProgress = residentialLevelUp.educationProgress;
			landValueProgress = residentialLevelUp.landValueProgress;
			landValueTooLow = residentialLevelUp.landValueTooLow;
		}
	}

	public void OnCalculateCommercialLevelUp(ref ItemClass.Level targetLevel, ref int wealthProgress, ref int landValueProgress, ref bool landValueTooLow, int averageWealth, int landValue, ushort buildingID, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level currentLevel)
	{
		if (this.m_LevelUpExtensions.Count != 0)
		{
			CommercialLevelUp commercialLevelUp;
			commercialLevelUp.targetLevel = targetLevel;
			commercialLevelUp.wealthProgress = wealthProgress;
			commercialLevelUp.landValueProgress = landValueProgress;
			commercialLevelUp.landValueTooLow = landValueTooLow;
			for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
			{
				commercialLevelUp = this.m_LevelUpExtensions[i].OnCalculateCommercialLevelUp(commercialLevelUp, averageWealth, landValue, buildingID, service, subService, currentLevel);
			}
			targetLevel = commercialLevelUp.targetLevel;
			wealthProgress = commercialLevelUp.wealthProgress;
			landValueProgress = commercialLevelUp.landValueProgress;
			landValueTooLow = commercialLevelUp.landValueTooLow;
		}
	}

	public void OnCalculateIndustrialLevelUp(ref ItemClass.Level targetLevel, ref int educationProgress, ref int serviceProgress, ref bool tooFewServices, int averageEducation, int serviceScore, ushort buildingID, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level currentLevel)
	{
		if (this.m_LevelUpExtensions.Count != 0)
		{
			IndustrialLevelUp industrialLevelUp;
			industrialLevelUp.targetLevel = targetLevel;
			industrialLevelUp.educationProgress = educationProgress;
			industrialLevelUp.serviceProgress = serviceProgress;
			industrialLevelUp.tooFewServices = tooFewServices;
			for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
			{
				industrialLevelUp = this.m_LevelUpExtensions[i].OnCalculateIndustrialLevelUp(industrialLevelUp, averageEducation, serviceScore, buildingID, service, subService, currentLevel);
			}
			targetLevel = industrialLevelUp.targetLevel;
			educationProgress = industrialLevelUp.educationProgress;
			serviceProgress = industrialLevelUp.serviceProgress;
			tooFewServices = industrialLevelUp.tooFewServices;
		}
	}

	public void OnCalculateOfficeLevelUp(ref ItemClass.Level targetLevel, ref int educationProgress, ref int serviceProgress, ref bool tooFewServices, int averageEducation, int serviceScore, ushort buildingID, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level currentLevel)
	{
		if (this.m_LevelUpExtensions.Count != 0)
		{
			OfficeLevelUp officeLevelUp;
			officeLevelUp.targetLevel = targetLevel;
			officeLevelUp.educationProgress = educationProgress;
			officeLevelUp.serviceProgress = serviceProgress;
			officeLevelUp.tooFewServices = tooFewServices;
			for (int i = 0; i < this.m_LevelUpExtensions.Count; i++)
			{
				officeLevelUp = this.m_LevelUpExtensions[i].OnCalculateOfficeLevelUp(officeLevelUp, averageEducation, serviceScore, buildingID, service, subService, currentLevel);
			}
			targetLevel = officeLevelUp.targetLevel;
			educationProgress = officeLevelUp.educationProgress;
			serviceProgress = officeLevelUp.serviceProgress;
			tooFewServices = officeLevelUp.tooFewServices;
		}
	}

	private List<ILevelUpExtension> m_LevelUpExtensions = new List<ILevelUpExtension>();
}
