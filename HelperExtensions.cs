﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;

public static class HelperExtensions
{
	public static string GetLocalizedBuildingType(this ItemClass.Zone zone)
	{
		return Locale.Get("ZONEDBUILDING_TITLE", EnumExtensions.Name<ItemClass.Zone>(zone));
	}

	public static string GetIconSpriteName(this PositionData<ItemClass.Service> service)
	{
		return "ToolbarIcon" + service.get_enumName();
	}

	public static string GetLocalizedName(this PositionData<ItemClass.Service> service)
	{
		if (service.get_enumValue() == ItemClass.Service.Water && SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
		{
			return Locale.Get("MAIN_TOOL_WW", service.get_enumName());
		}
		if (service.get_enumValue() == ItemClass.Service.FireDepartment && SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			return Locale.Get("MAIN_TOOL_ND", service.get_enumName());
		}
		return Locale.Get("MAIN_TOOL", service.get_enumName());
	}

	public static string GetLocalizedName(this PositionData<ItemClass.SubService> service)
	{
		return Locale.Get("MAIN_TOOL", service.get_enumName());
	}

	public static string GetIconSpriteName(this PositionData<ItemClass.SubService> service)
	{
		return "SubBarPublicTransport" + service.get_enumName();
	}

	public static string GetLocalizedName(this PositionData<UnlockManager.Feature> feature)
	{
		return Locale.Get("FEATURES", feature.get_enumName());
	}

	public static string GetLocalizedName(this PositionData<DistrictPolicies.Types> type)
	{
		return Locale.Get("POLICIES", type.get_enumName());
	}

	public static string GetLocalizedName(this PositionData<DistrictPolicies.Policies> type)
	{
		return Locale.Get("POLICIES", type.get_enumName());
	}

	public static string GetLocalizedName(this PositionData<ItemClass.Zone> type)
	{
		return Locale.Get("ZONING_TITLE", type.get_enumName());
	}

	public static string GetLocalizedDescription(this PositionData<ItemClass.Zone> type)
	{
		return Locale.Get("ZONING_DESC", type.get_enumName());
	}

	public static bool IsFlagSet(this GeneratedScrollPanel.AssetFilter filter, GeneratedScrollPanel.AssetFilter flag)
	{
		return (filter & flag) == flag;
	}

	public static bool IsFlagSet(this GeneratedGroupPanel.GroupFilter filter, GeneratedGroupPanel.GroupFilter flag)
	{
		return (filter & flag) == flag;
	}

	public static bool IsFlagSet(this ItemClass.Availability filter, ItemClass.Availability flag)
	{
		return (filter & flag) == flag;
	}
}
