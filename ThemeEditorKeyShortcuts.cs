﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class ThemeEditorKeyShortcuts : KeyShortcuts
{
	private void Awake()
	{
		this.m_CloseToolbarButton = UIView.Find<UIButton>("TSCloseButton");
	}

	private void SteamEscape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
	}

	protected void Update()
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseMenu))
			{
				UIView.get_library().ShowModal("PauseMenu");
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.PauseSimulation))
			{
				base.SimulationPause();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.NormalSimSpeed))
			{
				base.SimulationSpeed1();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.MediumSimSpeed))
			{
				base.SimulationSpeed2();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FastSimSpeed))
			{
				base.SimulationSpeed3();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Escape))
			{
				this.SteamEscape();
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.FreeCamera))
			{
				base.SelectUIButton("FreeCamButton");
			}
		}
	}

	protected override void OnProcessKeyEvent(EventType eventType, KeyCode keyCode, EventModifiers modifiers)
	{
		if (!UIView.HasModalInput() && !UIView.HasInputFocus())
		{
			if (this.m_ShortcutSimulationPause.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationPause();
			}
			else if (this.m_ShortcutSimulationSpeed1.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed1();
			}
			else if (this.m_ShortcutSimulationSpeed2.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed2();
			}
			else if (this.m_ShortcutSimulationSpeed3.IsPressed(eventType, keyCode, modifiers))
			{
				base.SimulationSpeed3();
			}
			else if (eventType == 4 && keyCode == 27)
			{
				this.Escape();
			}
			else if (this.m_ShortcutTerrainProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("TerrainProperties");
			}
			else if (this.m_ShortcutWaterProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("WaterProperties");
			}
			else if (this.m_ShortcutAtmosphereProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("AtmosphereProperties");
			}
			else if (this.m_ShortcutWorldProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("WorldProperties");
			}
			else if (this.m_ShortcutStructuresProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("StructuresProperties");
			}
			else if (this.m_ShortcutDisasterProperties.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("DisasterProperties");
			}
			else if (this.m_ShortcutSettings.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("ThemeSettings");
			}
			else if (this.m_ShortcutInGameShortcutFreeCameraMode.IsPressed(eventType, keyCode, modifiers))
			{
				base.SelectUIButton("FreeCamButton");
			}
		}
	}

	private void Escape()
	{
		if (ToolsModifierControl.GetCurrentTool<CameraTool>() != null)
		{
			base.SelectUIButton("FreeCamButton");
		}
		else if (this.m_CloseToolbarButton != null && this.m_CloseToolbarButton.get_isVisible())
		{
			this.m_CloseToolbarButton.SimulateClick();
		}
		else
		{
			UIView.get_library().ShowModal("PauseMenu");
		}
	}

	private SavedInputKey m_ShortcutSimulationPause = new SavedInputKey(Settings.themeEditorSimulationPause, Settings.inputSettingsFile, DefaultSettings.themeEditorSimulationPause, true);

	private SavedInputKey m_ShortcutSimulationSpeed1 = new SavedInputKey(Settings.themeEditorSimulationSpeed1, Settings.inputSettingsFile, DefaultSettings.themeEditorSimulationSpeed1, true);

	private SavedInputKey m_ShortcutSimulationSpeed2 = new SavedInputKey(Settings.themeEditorSimulationSpeed2, Settings.inputSettingsFile, DefaultSettings.themeEditorSimulationSpeed2, true);

	private SavedInputKey m_ShortcutSimulationSpeed3 = new SavedInputKey(Settings.themeEditorSimulationSpeed3, Settings.inputSettingsFile, DefaultSettings.themeEditorSimulationSpeed3, true);

	private SavedInputKey m_ShortcutInGameShortcutFreeCameraMode = new SavedInputKey(Settings.inGameShortcutFreeCameraMode, Settings.inputSettingsFile, DefaultSettings.inGameShortcutFreeCameraMode, true);

	private SavedInputKey m_ShortcutTerrainProperties = new SavedInputKey(Settings.themeEditorTerrainProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorTerrainProperties, true);

	private SavedInputKey m_ShortcutWaterProperties = new SavedInputKey(Settings.themeEditorWaterProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorWaterProperties, true);

	private SavedInputKey m_ShortcutAtmosphereProperties = new SavedInputKey(Settings.themeEditorAtmosphereProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorAtmosphereProperties, true);

	private SavedInputKey m_ShortcutWorldProperties = new SavedInputKey(Settings.themeEditorWorldProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorWorldProperties, true);

	private SavedInputKey m_ShortcutStructuresProperties = new SavedInputKey(Settings.themeEditorStructuresProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorStructuresProperties, true);

	private SavedInputKey m_ShortcutDisasterProperties = new SavedInputKey(Settings.themeEditorDisasterProperties, Settings.inputSettingsFile, DefaultSettings.themeEditorDisasterProperties, true);

	private SavedInputKey m_ShortcutSettings = new SavedInputKey(Settings.themeEditorSettings, Settings.inputSettingsFile, DefaultSettings.themeEditorSettings, true);

	private UIButton m_CloseToolbarButton;
}
