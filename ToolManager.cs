﻿using System;
using ColossalFramework;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class ToolManager : SimulationManagerBase<ToolManager, ToolController>, ISimulationManager, IRenderableManager, IAudibleManager, ITerrainManager
{
	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		ToolController properties = this.m_properties;
		if (properties != null)
		{
			PrefabInfo editPrefabInfo = properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				editPrefabInfo.RenderMesh(cameraInfo);
				VehicleInfo vehicleInfo = editPrefabInfo as VehicleInfo;
				if (vehicleInfo != null && vehicleInfo.m_trailers != null)
				{
					float num = vehicleInfo.m_generatedInfo.m_size.z * 0.5f;
					for (int i = 0; i < vehicleInfo.m_trailers.Length; i++)
					{
						VehicleInfo info = vehicleInfo.m_trailers[i].m_info;
						num += info.m_generatedInfo.m_size.z * 0.5f - info.m_attachOffsetFront;
						info.RenderMesh(cameraInfo, new Vector3(0f, 0f, -num));
						num += info.m_generatedInfo.m_size.z * 0.5f - info.m_attachOffsetBack;
					}
				}
			}
			try
			{
				ToolBase currentTool = properties.CurrentTool;
				if (currentTool != null)
				{
					currentTool.RenderGeometry(cameraInfo);
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Tool error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	protected override void EndOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		ToolController properties = this.m_properties;
		if (properties != null)
		{
			try
			{
				ToolBase currentTool = properties.CurrentTool;
				if (currentTool != null)
				{
					currentTool.RenderOverlay(cameraInfo);
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Tool error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		ToolController properties = this.m_properties;
		if (properties != null)
		{
			try
			{
				ToolBase currentTool = properties.CurrentTool;
				if (currentTool != null)
				{
					currentTool.PlayAudio(listenerInfo);
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Tool error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		ToolController properties = this.m_properties;
		if (properties != null)
		{
			PrefabInfo editPrefabInfo = properties.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				editPrefabInfo.TerrainUpdated(heightArea, surfaceArea, zoneArea);
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (Singleton<LoadingManager>.get_instance().m_loadingComplete && subStep <= 1)
		{
			try
			{
				ToolController properties = this.m_properties;
				if (properties != null)
				{
					ToolBase currentTool = properties.CurrentTool;
					if (currentTool != null)
					{
						currentTool.SimulationStep();
					}
				}
			}
			catch (Exception ex)
			{
				UIView.ForwardException(ex);
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Tool error: " + ex.Message + "\n" + ex.StackTrace);
			}
		}
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("ToolManager.LateUpdateData");
		base.LateUpdateData(mode);
		ToolController toolController = this.m_properties;
		if (toolController != null && (mode == SimulationManager.UpdateMode.NewAsset || mode == SimulationManager.UpdateMode.LoadAsset))
		{
			PrefabInfo editPrefabInfo = toolController.m_editPrefabInfo;
			if (editPrefabInfo != null)
			{
				TerrainModify.UpdateArea(-500f, -500f, 500f, 500f, true, true, true);
				editPrefabInfo.LoadDecorations();
				toolController.m_prevPrefabInfo = editPrefabInfo;
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					toolController.ReadPrefabMesh();
				});
			}
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}
}
