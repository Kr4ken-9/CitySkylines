﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public static class TerrainModify
{
	public static void UpdateArea(float minX, float minZ, float maxX, float maxZ, bool heights, bool surface, bool zones)
	{
		int minX2 = (int)(minX / 16f + 540f) - 2;
		int minZ2 = (int)(minZ / 16f + 540f) - 2;
		int maxX2 = (int)(maxX / 16f + 540f) + 2;
		int maxZ2 = (int)(maxZ / 16f + 540f) + 2;
		TerrainModify.UpdateArea(minX2, minZ2, maxX2, maxZ2, heights, surface, zones);
	}

	public static void UpdateArea(int minX, int minZ, int maxX, int maxZ, bool heights, bool surface, bool zones)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainArea heightsModified = instance.m_heightsModified;
		TerrainArea surfaceModified = instance.m_surfaceModified;
		TerrainArea zonesModified = instance.m_zonesModified;
		int num = (maxX - minX + 1) * (maxZ - minZ + 1);
		if (instance.m_modifyingLevel != 0)
		{
			if (instance.m_modifyingHeights && heights)
			{
				int num2 = (heightsModified.m_maxX - heightsModified.m_minX + 1) * (heightsModified.m_maxZ - heightsModified.m_minZ + 1);
				int num3 = (Mathf.Max(maxX, heightsModified.m_maxX) - Mathf.Min(minX, heightsModified.m_minX) + 1) * (Mathf.Max(maxZ, heightsModified.m_maxZ) - Mathf.Min(minZ, heightsModified.m_minZ) + 1);
				if (num3 > num2 + num << 1 || num3 > 10000)
				{
					TerrainModify.UpdateAreaImplementation();
				}
			}
			if (instance.m_modifyingSurface && surface)
			{
				int num4 = (surfaceModified.m_maxX - surfaceModified.m_minX + 1) * (surfaceModified.m_maxZ - surfaceModified.m_minZ + 1);
				int num5 = (Mathf.Max(maxX, surfaceModified.m_maxX) - Mathf.Min(minX, surfaceModified.m_minX) + 1) * (Mathf.Max(maxZ, surfaceModified.m_maxZ) - Mathf.Min(minZ, surfaceModified.m_minZ) + 1);
				if (num5 > num4 + num << 1 || num5 > 10000)
				{
					TerrainModify.UpdateAreaImplementation();
				}
			}
			if (instance.m_modifyingZones && zones)
			{
				int num6 = (zonesModified.m_maxX - zonesModified.m_minX + 1) * (zonesModified.m_maxZ - zonesModified.m_minZ + 1);
				int num7 = (Mathf.Max(maxX, zonesModified.m_maxX) - Mathf.Min(minX, zonesModified.m_minX) + 1) * (Mathf.Max(maxZ, zonesModified.m_maxZ) - Mathf.Min(minZ, zonesModified.m_minZ) + 1);
				if (num7 > num6 + num << 1 || num7 > 10000)
				{
					TerrainModify.UpdateAreaImplementation();
				}
			}
		}
		instance.m_modifyingHeights = (instance.m_modifyingHeights || heights);
		instance.m_modifyingSurface = (instance.m_modifyingSurface || surface);
		instance.m_modifyingZones = (instance.m_modifyingZones || zones);
		if (heights)
		{
			heightsModified.AddArea(minX, minZ, maxX, maxZ);
			int num8 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
			int num9 = (heightsModified.m_maxZ - heightsModified.m_minZ << 2) + 1;
			if (num8 * num9 > instance.m_tempHeights.Length)
			{
				heightsModified.m_maxX = Mathf.Min(heightsModified.m_maxX, heightsModified.m_minX + 120 + 8);
				heightsModified.m_maxZ = Mathf.Min(heightsModified.m_maxZ, heightsModified.m_minZ + 120 + 8);
			}
		}
		if (surface)
		{
			surfaceModified.AddArea(minX, minZ, maxX, maxZ);
			int num10 = (surfaceModified.m_maxX - surfaceModified.m_minX << 2) + 1;
			int num11 = (surfaceModified.m_maxZ - surfaceModified.m_minZ << 2) + 1;
			if (num10 * num11 > instance.m_tempSurface.Length)
			{
				surfaceModified.m_maxX = Mathf.Min(surfaceModified.m_maxX, surfaceModified.m_minX + 120 + 8);
				surfaceModified.m_maxZ = Mathf.Min(surfaceModified.m_maxZ, surfaceModified.m_minZ + 120 + 8);
			}
		}
		if (zones)
		{
			zonesModified.AddArea(minX, minZ, maxX, maxZ);
			int num12 = (zonesModified.m_maxX - zonesModified.m_minX << 2) + 1;
			int num13 = (zonesModified.m_maxZ - zonesModified.m_minZ << 2) + 1;
			if (num12 * num13 > instance.m_tempZones.Length)
			{
				zonesModified.m_maxX = Mathf.Min(zonesModified.m_maxX, zonesModified.m_minX + 120 + 8);
				zonesModified.m_maxZ = Mathf.Min(zonesModified.m_maxZ, zonesModified.m_minZ + 120 + 8);
			}
		}
		if (instance.m_modifyingLevel == 0 || num > 10000)
		{
			TerrainModify.UpdateAreaImplementation();
		}
	}

	public static void RefreshAllModifications()
	{
		TerrainModify.UpdateAreaImplementation();
	}

	public static void BeginUpdateArea()
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		instance.m_modifyingLevel++;
	}

	public static void EndUpdateArea()
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		if (--instance.m_modifyingLevel == 0 && (instance.m_modifyingHeights || instance.m_modifyingSurface || instance.m_modifyingZones))
		{
			TerrainModify.UpdateAreaImplementation();
		}
	}

	private static void UpdateAreaImplementation()
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainArea heightsModified = instance.m_heightsModified;
		TerrainArea surfaceModified = instance.m_surfaceModified;
		TerrainArea zonesModified = instance.m_zonesModified;
		TerrainModify.HeightModification[] tempHeights = instance.m_tempHeights;
		TerrainManager.SurfaceCell[] tempSurface = instance.m_tempSurface;
		TerrainModify.ZoneModification[] tempZones = instance.m_tempZones;
		ushort[] detailHeights = instance.m_detailHeights;
		ushort[] detailHeights2 = instance.m_detailHeights2;
		ushort[] rawHeights = instance.RawHeights;
		ushort[] rawHeights2 = instance.RawHeights2;
		ushort[] finalHeights = instance.FinalHeights;
		ushort[] blockHeightTargets = instance.BlockHeightTargets;
		TerrainManager.SurfaceCell[] detailSurface = instance.m_detailSurface;
		TerrainManager.ZoneCell[] detailZones = instance.m_detailZones;
		int num = Mathf.Min(Mathf.Min(heightsModified.m_minX, surfaceModified.m_minX), zonesModified.m_minX);
		int num2 = Mathf.Min(Mathf.Min(heightsModified.m_minZ, surfaceModified.m_minZ), zonesModified.m_minZ);
		int num3 = Mathf.Max(Mathf.Max(heightsModified.m_maxX, surfaceModified.m_maxX), zonesModified.m_maxX);
		int num4 = Mathf.Max(Mathf.Max(heightsModified.m_maxZ, surfaceModified.m_maxZ), zonesModified.m_maxZ);
		int num5 = 120;
		int num6 = 128 - num5 >> 1;
		int num7 = Mathf.Max(0, (num - num6) / num5);
		int num8 = Mathf.Min(8, (num3 + num6) / num5);
		int num9 = Mathf.Max(0, (num2 - num6) / num5);
		int num10 = Mathf.Min(8, (num4 + num6) / num5);
		for (int i = num9; i <= num10; i++)
		{
			for (int j = num7; j <= num8; j++)
			{
				int num11 = i * 9 + j;
				int simDetailIndex = instance.m_patches[num11].m_simDetailIndex;
				if (simDetailIndex != 0)
				{
					if (instance.m_modifyingHeights)
					{
						int num12 = Mathf.Max(heightsModified.m_minX - j * num5, 0);
						int num13 = Mathf.Min(heightsModified.m_maxX - j * num5, num5);
						int num14 = Mathf.Max(heightsModified.m_minZ - i * num5, 0);
						int num15 = Mathf.Min(heightsModified.m_maxZ - i * num5, num5);
						int num16 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
						int num17 = j * num5;
						int num18 = i * num5;
						TerrainModify.HeightModification heightModification;
						heightModification.m_divider = 1f;
						heightModification.m_primaryMin = 0f;
						heightModification.m_primaryMax = 1024f;
						heightModification.m_secondaryMin = 0f;
						heightModification.m_secondaryMax = 1024f;
						heightModification.m_blockHeight = 0f;
						heightModification.m_digHeight = 1024f;
						for (int k = num14; k <= num15; k++)
						{
							for (int l = num12; l <= num13; l++)
							{
								int num19 = l + num17;
								int num20 = k + num18;
								int num21 = Mathf.Max(num19 - 1, 0);
								int num22 = num19;
								int num23 = Mathf.Min(num19 + 1, 1080);
								int num24 = Mathf.Min(num19 + 2, 1080);
								int num25 = Mathf.Max(num20 - 1, 0) * 1081;
								int num26 = num20 * 1081;
								int num27 = Mathf.Min(num20 + 1, 1080) * 1081;
								int num28 = Mathf.Min(num20 + 2, 1080) * 1081;
								rawHeights2[num26 + num22] = rawHeights[num26 + num22];
								float h = (float)rawHeights[num25 + num21];
								float h2 = (float)rawHeights[num25 + num22];
								float h3 = (float)rawHeights[num25 + num23];
								float h4 = (float)rawHeights[num25 + num24];
								float h5 = (float)rawHeights[num26 + num21];
								float h6 = (float)rawHeights[num26 + num22];
								float h7 = (float)rawHeights[num26 + num23];
								float h8 = (float)rawHeights[num26 + num24];
								float h9 = (float)rawHeights[num27 + num21];
								float h10 = (float)rawHeights[num27 + num22];
								float h11 = (float)rawHeights[num27 + num23];
								float h12 = (float)rawHeights[num27 + num24];
								float h13 = (float)rawHeights[num28 + num21];
								float h14 = (float)rawHeights[num28 + num22];
								float h15 = (float)rawHeights[num28 + num23];
								float h16 = (float)rawHeights[num28 + num24];
								int num29 = Mathf.Min(Mathf.Min(heightsModified.m_maxX - j * num5 << 2, 480) - (l << 2) + 1, 4);
								int num30 = Mathf.Min(Mathf.Min(heightsModified.m_maxZ - i * num5 << 2, 480) - (k << 2) + 1, 4);
								for (int m = 0; m < num29; m++)
								{
									float t = (float)m * 0.25f;
									float h17 = TerrainManager.SmoothSample(h, h2, h3, h4, t);
									float h18 = TerrainManager.SmoothSample(h5, h6, h7, h8, t);
									float h19 = TerrainManager.SmoothSample(h9, h10, h11, h12, t);
									float h20 = TerrainManager.SmoothSample(h13, h14, h15, h16, t);
									for (int n = 0; n < num30; n++)
									{
										float t2 = (float)n * 0.25f;
										float num31 = TerrainManager.SmoothSample(h17, h18, h19, h20, t2);
										num31 *= 0.015625f;
										heightModification.m_target = num31;
										tempHeights[((k + i * num5 - heightsModified.m_minZ << 2) + n) * num16 + (l + j * num5 - heightsModified.m_minX << 2) + m] = heightModification;
									}
								}
							}
						}
					}
					if (instance.m_modifyingSurface)
					{
						int num32 = Mathf.Max((surfaceModified.m_minX << 2) - j * 480, 0);
						int num33 = Mathf.Min((surfaceModified.m_maxX << 2) - j * 480, 479);
						int num34 = Mathf.Max((surfaceModified.m_minZ << 2) - i * 480, 0);
						int num35 = Mathf.Min((surfaceModified.m_maxZ << 2) - i * 480, 479);
						int num36 = (surfaceModified.m_maxX - surfaceModified.m_minX << 2) + 1;
						TerrainManager.SurfaceCell surfaceCell;
						surfaceCell.m_clipped = 0;
						surfaceCell.m_ruined = 0;
						surfaceCell.m_gravel = 0;
						surfaceCell.m_field = 0;
						surfaceCell.m_pavementA = 0;
						surfaceCell.m_pavementB = 0;
						for (int num37 = num34; num37 <= num35; num37++)
						{
							for (int num38 = num32; num38 <= num33; num38++)
							{
								tempSurface[(num37 + i * 480 - (surfaceModified.m_minZ << 2)) * num36 + num38 + j * 480 - (surfaceModified.m_minX << 2)] = surfaceCell;
							}
						}
					}
					if (instance.m_modifyingZones)
					{
						int num39 = Mathf.Max((zonesModified.m_minX << 2) - j * 480, 0);
						int num40 = Mathf.Min((zonesModified.m_maxX << 2) - j * 480, 479);
						int num41 = Mathf.Max((zonesModified.m_minZ << 2) - i * 480, 0);
						int num42 = Mathf.Min((zonesModified.m_maxZ << 2) - i * 480, 479);
						int num43 = (zonesModified.m_maxX - zonesModified.m_minX << 2) + 1;
						TerrainModify.ZoneModification zoneModification;
						zoneModification.m_angle = 0;
						zoneModification.m_zone = 15;
						zoneModification.m_offsetX = 0;
						zoneModification.m_offsetZ = 0;
						zoneModification.m_minD2 = 1f;
						for (int num44 = num41; num44 <= num42; num44++)
						{
							for (int num45 = num39; num45 <= num40; num45++)
							{
								tempZones[(num44 + i * 480 - (zonesModified.m_minZ << 2)) * num43 + num45 + j * 480 - (zonesModified.m_minX << 2)] = zoneModification;
							}
						}
					}
				}
				else if (instance.m_modifyingHeights)
				{
					int num46 = Mathf.Max(heightsModified.m_minX - j * num5, 0);
					int num47 = Mathf.Min(heightsModified.m_maxX - j * num5, num5);
					int num48 = Mathf.Max(heightsModified.m_minZ - i * num5, 0);
					int num49 = Mathf.Min(heightsModified.m_maxZ - i * num5, num5);
					int num50 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
					int num51 = j * num5;
					int num52 = i * num5;
					TerrainModify.HeightModification heightModification2;
					heightModification2.m_divider = 1f;
					heightModification2.m_primaryMin = 0f;
					heightModification2.m_primaryMax = 1024f;
					heightModification2.m_secondaryMin = 0f;
					heightModification2.m_secondaryMax = 1024f;
					heightModification2.m_blockHeight = 0f;
					heightModification2.m_digHeight = 1024f;
					for (int num53 = num48; num53 <= num49; num53++)
					{
						for (int num54 = num46; num54 <= num47; num54++)
						{
							int num55 = (num52 + num53) * 1081 + num54 + num51;
							ushort num56 = rawHeights[num55];
							rawHeights2[num55] = num56;
							float num57 = (float)num56;
							num57 *= 0.015625f;
							heightModification2.m_target = num57;
							tempHeights[(num53 + i * num5 - heightsModified.m_minZ << 2) * num50 + (num54 + j * num5 - heightsModified.m_minX << 2)] = heightModification2;
						}
					}
				}
			}
		}
		heightsModified.UpdateBounds();
		surfaceModified.UpdateBounds();
		zonesModified.UpdateBounds();
		TerrainManager.Managers_TerrainUpdated(heightsModified, surfaceModified, zonesModified);
		if (instance.m_modifyingHeights)
		{
			short[] modifiedBlockMinX = instance.m_modifiedBlockMinX;
			short[] modifiedBlockMaxX = instance.m_modifiedBlockMaxX;
			for (int num58 = heightsModified.m_minZ; num58 <= heightsModified.m_maxZ; num58++)
			{
				if (heightsModified.m_minX < (int)modifiedBlockMinX[num58])
				{
					modifiedBlockMinX[num58] = (short)heightsModified.m_minX;
				}
				if (heightsModified.m_maxX > (int)modifiedBlockMaxX[num58])
				{
					modifiedBlockMaxX[num58] = (short)heightsModified.m_maxX;
				}
			}
		}
		for (int num59 = num9; num59 <= num10; num59++)
		{
			for (int num60 = num7; num60 <= num8; num60++)
			{
				int num61 = num59 * 9 + num60;
				int simDetailIndex2 = instance.m_patches[num61].m_simDetailIndex;
				if (instance.m_modifyingHeights)
				{
					int num62 = Mathf.Max(heightsModified.m_minX - num60 * num5, 0);
					int num63 = Mathf.Min(heightsModified.m_maxX - num60 * num5, num5);
					int num64 = Mathf.Max(heightsModified.m_minZ - num59 * num5, 0);
					int num65 = Mathf.Min(heightsModified.m_maxZ - num59 * num5, num5);
					int num66 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
					int num67 = num60 * num5;
					int num68 = num59 * num5;
					for (int num69 = num64; num69 <= num65; num69++)
					{
						for (int num70 = num62; num70 <= num63; num70++)
						{
							TerrainModify.HeightModification heightModification3 = tempHeights[(num69 + num59 * num5 - heightsModified.m_minZ << 2) * num66 + (num70 + num60 * num5 - heightsModified.m_minX << 2)];
							float num71 = heightModification3.m_target / heightModification3.m_divider;
							num71 = Mathf.Max(num71, heightModification3.m_secondaryMin);
							num71 = Mathf.Min(num71, heightModification3.m_secondaryMax);
							num71 = Mathf.Max(num71, heightModification3.m_primaryMin);
							num71 = Mathf.Min(num71, heightModification3.m_primaryMax);
							int num72 = (num68 + num69) * 1081 + num70 + num67;
							ushort num73 = (ushort)Mathf.Clamp((int)(num71 * 65536f / 1024f), 0, 65535);
							finalHeights[num72] = num73;
							num71 = Mathf.Max(num71, heightModification3.m_blockHeight);
							num71 = Mathf.Min(num71, heightModification3.m_digHeight);
							num71 = Mathf.Min(num71, heightModification3.m_primaryMax);
							num73 = (ushort)Mathf.Clamp((int)(num71 * 65536f / 1024f), 0, 65535);
							blockHeightTargets[num72] = num73;
						}
					}
				}
				if (simDetailIndex2 != 0)
				{
					if (instance.m_modifyingHeights)
					{
						int num74 = Mathf.Max((heightsModified.m_minX << 2) - num60 * 480, 0);
						int num75 = Mathf.Min((heightsModified.m_maxX << 2) - num60 * 480, 480);
						int num76 = Mathf.Max((heightsModified.m_minZ << 2) - num59 * 480, 0);
						int num77 = Mathf.Min((heightsModified.m_maxZ << 2) - num59 * 480, 480);
						int num78 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
						int num79 = (simDetailIndex2 - 1) * 481 * 481;
						for (int num80 = num76; num80 <= num77; num80++)
						{
							for (int num81 = num74; num81 <= num75; num81++)
							{
								TerrainModify.HeightModification heightModification4 = tempHeights[(num80 + num59 * 480 - (heightsModified.m_minZ << 2)) * num78 + num81 + num60 * 480 - (heightsModified.m_minX << 2)];
								float num82 = heightModification4.m_target / heightModification4.m_divider;
								num82 = Mathf.Max(num82, heightModification4.m_secondaryMin);
								num82 = Mathf.Min(num82, heightModification4.m_secondaryMax);
								num82 = Mathf.Max(num82, heightModification4.m_primaryMin);
								num82 = Mathf.Min(num82, heightModification4.m_primaryMax);
								float num83 = num82 * 65536f / 1024f;
								int num84 = num79 + num80 * 481 + num81;
								detailHeights[num84] = (ushort)Mathf.Clamp((int)num83, 0, 65535);
								num82 = Mathf.Max(num82, heightModification4.m_blockHeight);
								num82 = Mathf.Min(num82, heightModification4.m_digHeight);
								num82 = Mathf.Min(num82, heightModification4.m_primaryMax);
								float num85 = num82 * 65536f / 1024f;
								detailHeights2[num84] = (ushort)Mathf.Clamp((int)num85, 0, 65535);
							}
						}
					}
					if (instance.m_modifyingSurface)
					{
						int num86 = Mathf.Max((surfaceModified.m_minX << 2) - num60 * 480, 0);
						int num87 = Mathf.Min((surfaceModified.m_maxX << 2) - num60 * 480, 479);
						int num88 = Mathf.Max((surfaceModified.m_minZ << 2) - num59 * 480, 0);
						int num89 = Mathf.Min((surfaceModified.m_maxZ << 2) - num59 * 480, 479);
						int num90 = (surfaceModified.m_maxX - surfaceModified.m_minX << 2) + 1;
						int num91 = (simDetailIndex2 - 1) * 480 * 480;
						for (int num92 = num88; num92 <= num89; num92++)
						{
							for (int num93 = num86; num93 <= num87; num93++)
							{
								TerrainManager.SurfaceCell surfaceCell2 = tempSurface[(num92 + num59 * 480 - (surfaceModified.m_minZ << 2)) * num90 + num93 + num60 * 480 - (surfaceModified.m_minX << 2)];
								detailSurface[num91 + num92 * 480 + num93] = surfaceCell2;
							}
						}
					}
					if (instance.m_modifyingZones)
					{
						int num94 = Mathf.Max((zonesModified.m_minX << 2) - num60 * 480, 0);
						int num95 = Mathf.Min((zonesModified.m_maxX << 2) - num60 * 480, 479);
						int num96 = Mathf.Max((zonesModified.m_minZ << 2) - num59 * 480, 0);
						int num97 = Mathf.Min((zonesModified.m_maxZ << 2) - num59 * 480, 479);
						int num98 = (zonesModified.m_maxX - zonesModified.m_minX << 2) + 1;
						int num99 = (simDetailIndex2 - 1) * 480 * 480;
						for (int num100 = num96; num100 <= num97; num100++)
						{
							for (int num101 = num94; num101 <= num95; num101++)
							{
								TerrainModify.ZoneModification zoneModification2 = tempZones[(num100 + num59 * 480 - (zonesModified.m_minZ << 2)) * num98 + num101 + num60 * 480 - (zonesModified.m_minX << 2)];
								TerrainManager.ZoneCell zoneCell;
								zoneCell.m_angle = zoneModification2.m_angle;
								zoneCell.m_zone = zoneModification2.m_zone;
								zoneCell.m_offsetX = zoneModification2.m_offsetX;
								zoneCell.m_offsetZ = zoneModification2.m_offsetZ;
								detailZones[num99 + num100 * 480 + num101] = zoneCell;
							}
						}
					}
				}
			}
		}
		instance.UpdateBounds(heightsModified.m_minX, heightsModified.m_minZ, heightsModified.m_maxX, heightsModified.m_maxZ);
		for (int num102 = num9; num102 <= num10; num102++)
		{
			for (int num103 = num7; num103 <= num8; num103++)
			{
				int num104 = num102 * 9 + num103;
				while (!Monitor.TryEnter(instance.m_patches[num104].m_modifiedLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
				{
				}
				try
				{
					if (instance.m_modifyingHeights)
					{
						instance.m_patches[num104].m_surfaceModified.AddArea(heightsModified);
					}
					if (instance.m_modifyingSurface)
					{
						instance.m_patches[num104].m_surfaceModified.AddArea(surfaceModified);
					}
					if (instance.m_modifyingZones)
					{
						instance.m_patches[num104].m_zonesModified.AddArea(zonesModified);
					}
					instance.m_patches[num104].m_tmpDetailIndex = instance.m_patches[num104].m_simDetailIndex;
				}
				finally
				{
					Monitor.Exit(instance.m_patches[num104].m_modifiedLock);
				}
			}
		}
		TerrainManager.Managers_AfterTerrainUpdate(heightsModified, surfaceModified, zonesModified);
		if (instance.m_modifyingHeights)
		{
			instance.m_TerrainWrapper.OnAfterHeightsModified(heightsModified.m_min.x, heightsModified.m_min.z, heightsModified.m_max.x, heightsModified.m_max.z);
		}
		instance.m_modifyingHeights = false;
		instance.m_modifyingSurface = false;
		instance.m_modifyingZones = false;
		heightsModified.Reset();
		surfaceModified.Reset();
		zonesModified.Reset();
	}

	public static void ApplyQuad(Vector2 a, Vector2 b, Vector2 c, Vector2 d, ItemClass.Zone zone, bool occupied, float angle, Vector2 pos, Vector2 xDir, Vector2 zDir, int xs, int xe, int zs, int ze)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainArea zonesModified = instance.m_zonesModified;
		TerrainModify.ZoneModification[] tempZones = instance.m_tempZones;
		if (!instance.m_modifyingZones)
		{
			return;
		}
		int num = Mathf.Max((int)(Mathf.Min(Mathf.Min(a.x, b.x), Mathf.Min(c.x, d.x)) / 4f + 2160f), zonesModified.m_minX << 2);
		int num2 = Mathf.Max((int)(Mathf.Min(Mathf.Min(a.y, b.y), Mathf.Min(c.y, d.y)) / 4f + 2160f), zonesModified.m_minZ << 2);
		int num3 = Mathf.Min((int)(Mathf.Max(Mathf.Max(a.x, b.x), Mathf.Max(c.x, d.x)) / 4f + 2160f), zonesModified.m_maxX << 2);
		int num4 = Mathf.Min((int)(Mathf.Max(Mathf.Max(a.y, b.y), Mathf.Max(c.y, d.y)) / 4f + 2160f), zonesModified.m_maxZ << 2);
		float num5 = 17280f;
		int num6 = (zonesModified.m_maxX - zonesModified.m_minX << 2) + 1;
		float num7 = 4f;
		num7 *= num7;
		byte zone2 = (byte)(zone + ((!occupied) ? 0 : 16));
		byte angle2 = (byte)(Mathf.RoundToInt(angle * 162.974655f) & 255);
		for (int i = num2; i <= num4; i++)
		{
			int j = num;
			while (j <= num3)
			{
				Vector2 p;
				p.x = (float)j * 4f - num5 * 0.5f + 2f;
				p.y = (float)i * 4f - num5 * 0.5f + 2f;
				float num8 = 0f;
				if ((p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x) >= 0f)
				{
					goto IL_24D;
				}
				num8 = Mathf.Max(num8, TerrainModify.SqrOffset(a, b, p));
				if (num8 < num7)
				{
					goto IL_24D;
				}
				IL_51D:
				j++;
				continue;
				IL_24D:
				if ((p.x - b.x) * (c.y - b.y) - (p.y - b.y) * (c.x - b.x) < 0f)
				{
					num8 = Mathf.Max(num8, TerrainModify.SqrOffset(b, c, p));
					if (num8 >= num7)
					{
						goto IL_51D;
					}
				}
				if ((p.x - c.x) * (d.y - c.y) - (p.y - c.y) * (d.x - c.x) < 0f)
				{
					num8 = Mathf.Max(num8, TerrainModify.SqrOffset(c, d, p));
					if (num8 >= num7)
					{
						goto IL_51D;
					}
				}
				if ((p.x - d.x) * (a.y - d.y) - (p.y - d.y) * (a.x - d.x) < 0f)
				{
					num8 = Mathf.Max(num8, TerrainModify.SqrOffset(d, a, p));
					if (num8 >= num7)
					{
						goto IL_51D;
					}
				}
				int num9 = (i - (zonesModified.m_minZ << 2)) * num6 + (j - (zonesModified.m_minX << 2));
				TerrainModify.ZoneModification zoneModification = tempZones[num9];
				Vector2 vector;
				vector..ctor(pos.x - p.x, pos.y - p.y);
				if (num8 <= zoneModification.m_minD2)
				{
					vector..ctor(pos.x - p.x, pos.y - p.y);
					float num10 = Mathf.Clamp(Mathf.Round((vector.x * xDir.x + vector.y * xDir.y) * 0.015625f), (float)xs, (float)xe);
					float num11 = Mathf.Clamp(Mathf.Round((vector.x * zDir.x + vector.y * zDir.y) * 0.015625f), (float)zs, (float)ze);
					vector.x -= num10 * xDir.x + num11 * zDir.x;
					vector.y -= num10 * xDir.y + num11 * zDir.y;
					zoneModification.m_angle = angle2;
					zoneModification.m_zone = zone2;
					zoneModification.m_offsetX = (sbyte)Mathf.RoundToInt(vector.x * 15.9375f);
					zoneModification.m_offsetZ = (sbyte)Mathf.RoundToInt(vector.y * 15.9375f);
					zoneModification.m_minD2 = num8;
				}
				tempZones[num9] = zoneModification;
				goto IL_51D;
			}
		}
	}

	private static float GetAdjustedLength(Vector2 a, Vector2 b)
	{
		float num = a.x * b.x + a.y * b.y;
		return Mathf.Max(0.1f, Mathf.Sqrt(1f - num * num));
	}

	public static void ApplyQuad(Vector3 a, Vector3 b, Vector3 c, Vector3 d, TerrainModify.Edges edges, TerrainModify.Heights heights, TerrainModify.Surface surface)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		if (instance.m_modifyingHeights && heights != TerrainModify.Heights.None)
		{
			TerrainModify.ApplyQuad(a, b, c, d, edges, heights, null);
		}
		if (instance.m_modifyingSurface && surface != TerrainModify.Surface.None)
		{
			if (edges != TerrainModify.Edges.None && (edges & TerrainModify.Edges.Expand) == TerrainModify.Edges.None)
			{
				if ((surface & TerrainModify.Surface.Clip) != TerrainModify.Surface.None)
				{
					TerrainModify.ApplyQuad(Quad2.XZ(a, b, c, d), edges, surface & TerrainModify.Surface.Clip);
				}
				if ((surface & ~TerrainModify.Surface.Clip) != TerrainModify.Surface.None)
				{
					TerrainModify.ApplyQuad(Quad2.XZ(a, b, c, d), edges, surface & ~TerrainModify.Surface.Clip);
				}
			}
			else
			{
				TerrainModify.ApplyQuad(Quad2.XZ(a, b, c, d), edges, surface);
			}
		}
	}

	private static float SqrOffset(Vector2 a, Vector2 b, Vector2 p)
	{
		float num = b.x - a.x;
		float num2 = b.y - a.y;
		float num3 = a.x - p.x;
		float num4 = a.y - p.y;
		float num5 = num * num3 + num2 * num4;
		float num6 = num * num + num2 * num2;
		if (num6 != 0f)
		{
			num5 /= num6;
		}
		float num7 = num3 - num * num5;
		float num8 = num4 - num2 * num5;
		return num7 * num7 + num8 * num8;
	}

	private static float SqrOffset(Vector3 ab, Vector3 pa)
	{
		float num = ab.x * pa.x + ab.z * pa.z;
		float num2 = ab.x * ab.x + ab.z * ab.z;
		if (num2 != 0f)
		{
			num /= num2;
		}
		float num3 = pa.x - ab.x * num;
		float num4 = pa.z - ab.z * num;
		return num3 * num3 + num4 * num4;
	}

	private static float SolveDown(Vector3 a, Vector3 b, Vector3 r)
	{
		float num = a.x * b.z - a.z * b.x;
		if (num == 0f)
		{
			return 0f;
		}
		return MathUtils.Determinant3x3(a, b, r) / num;
	}

	public static void SolveDown(Vector3 a, Vector3 b, Vector3 r, out float u, out float v, out float t)
	{
		float num = a.x * b.z - a.z * b.x;
		if (num == 0f)
		{
			u = 0f;
			v = 0f;
			t = 0f;
			return;
		}
		num = 1f / num;
		u = (r.x * b.z - r.z * b.x) * num;
		v = (a.x * r.z - a.z * r.x) * num;
		t = MathUtils.Determinant3x3(a, b, r) * num;
	}

	public static void ApplyQuad(Vector3 a, Vector3 b, Vector3 c, Vector3 d, TerrainModify.Edges edges, TerrainModify.Heights heights, float[] heightArray)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainArea heightsModified = instance.m_heightsModified;
		TerrainModify.HeightModification[] tempHeights = instance.m_tempHeights;
		float num;
		if ((heights & TerrainModify.Heights.DigHeight) != TerrainModify.Heights.None)
		{
			num = 0f;
		}
		else if ((heights & (TerrainModify.Heights.PrimaryLevel | TerrainModify.Heights.SecondaryLevel)) == TerrainModify.Heights.SecondaryLevel)
		{
			num = 0f;
		}
		else if (instance.HasDetailMapping((a + b + c + d) * 0.25f))
		{
			num = 5.656854f;
		}
		else
		{
			num = 22.6274166f;
		}
		float num2;
		if ((heights & (TerrainModify.Heights.PrimaryLevel | TerrainModify.Heights.SecondaryLevel)) != TerrainModify.Heights.None)
		{
			num2 = num + 12f;
		}
		else
		{
			num2 = num;
		}
		Vector3 vector = a;
		Vector3 vector2 = b;
		Vector3 vector3 = c;
		Vector3 vector4 = d;
		if ((edges & (TerrainModify.Edges.AB | TerrainModify.Edges.CD)) != TerrainModify.Edges.None)
		{
			Vector3 vector5 = (c - b).get_normalized() * num2;
			Vector3 vector6 = (d - a).get_normalized() * num2;
			if ((edges & TerrainModify.Edges.AB) != TerrainModify.Edges.None)
			{
				vector -= vector6;
				vector2 -= vector5;
			}
			if ((edges & TerrainModify.Edges.CD) != TerrainModify.Edges.None)
			{
				vector4 += vector6;
				vector3 += vector5;
			}
		}
		if ((edges & (TerrainModify.Edges.BC | TerrainModify.Edges.DA)) != TerrainModify.Edges.None)
		{
			Vector3 vector7 = (b - a).get_normalized() * num2;
			Vector3 vector8 = (d - c).get_normalized() * num2;
			if ((edges & TerrainModify.Edges.BC) != TerrainModify.Edges.None)
			{
				vector2 += vector7;
				vector3 -= vector8;
			}
			if ((edges & TerrainModify.Edges.DA) != TerrainModify.Edges.None)
			{
				vector -= vector7;
				vector4 += vector8;
			}
		}
		int num3 = Mathf.Max((int)(Mathf.Min(Mathf.Min(vector.x, vector2.x), Mathf.Min(vector3.x, vector4.x)) / 4f + 2160f) + 1, heightsModified.m_minX << 2);
		int num4 = Mathf.Max((int)(Mathf.Min(Mathf.Min(vector.z, vector2.z), Mathf.Min(vector3.z, vector4.z)) / 4f + 2160f) + 1, heightsModified.m_minZ << 2);
		int num5 = Mathf.Min((int)(Mathf.Max(Mathf.Max(vector.x, vector2.x), Mathf.Max(vector3.x, vector4.x)) / 4f + 2160f), heightsModified.m_maxX << 2);
		int num6 = Mathf.Min((int)(Mathf.Max(Mathf.Max(vector.z, vector2.z), Mathf.Max(vector3.z, vector4.z)) / 4f + 2160f), heightsModified.m_maxZ << 2);
		float num7 = 17280f;
		int num8 = (heightsModified.m_maxX - heightsModified.m_minX << 2) + 1;
		num *= num;
		num2 *= num2;
		edges = ~edges;
		Vector3 vector9 = b - a;
		Vector3 vector10 = c - b;
		Vector3 vector11 = d - c;
		Vector3 vector12 = a - d;
		Vector3 vector13 = c - a;
		for (int i = num4; i <= num6; i++)
		{
			int j = num3;
			while (j <= num5)
			{
				Vector3 vector14;
				vector14.x = (float)j * 4f - num7 * 0.5f;
				vector14.y = 0f;
				vector14.z = (float)i * 4f - num7 * 0.5f;
				TerrainModify.Edges edges2 = TerrainModify.Edges.None;
				if ((vector14.x - a.x) * vector9.z - (vector14.z - a.z) * vector9.x >= 0f)
				{
					goto IL_3A8;
				}
				edges2 |= TerrainModify.Edges.AB;
				if ((edges & edges2) == TerrainModify.Edges.None)
				{
					goto IL_3A8;
				}
				IL_927:
				j++;
				continue;
				IL_3A8:
				if ((vector14.x - b.x) * vector10.z - (vector14.z - b.z) * vector10.x < 0f)
				{
					edges2 |= TerrainModify.Edges.BC;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_927;
					}
				}
				if ((vector14.x - c.x) * vector11.z - (vector14.z - c.z) * vector11.x < 0f)
				{
					edges2 |= TerrainModify.Edges.CD;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_927;
					}
				}
				if ((vector14.x - d.x) * vector12.z - (vector14.z - d.z) * vector12.x < 0f)
				{
					edges2 |= TerrainModify.Edges.DA;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_927;
					}
				}
				float num9 = 0f;
				if ((edges2 & TerrainModify.Edges.AB) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector9, a - vector14));
					if (num9 >= num2)
					{
						goto IL_927;
					}
				}
				if ((edges2 & TerrainModify.Edges.BC) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector10, b - vector14));
					if (num9 >= num2)
					{
						goto IL_927;
					}
				}
				if ((edges2 & TerrainModify.Edges.CD) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector11, c - vector14));
					if (num9 >= num2)
					{
						goto IL_927;
					}
				}
				if ((edges2 & TerrainModify.Edges.DA) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector12, d - vector14));
					if (num9 >= num2)
					{
						goto IL_927;
					}
				}
				float num21;
				if (heightArray != null)
				{
					float num10;
					float num11;
					float num12;
					TerrainModify.SolveDown(vector9, vector12, vector14 - a, out num10, out num11, out num12);
					float num13 = Mathf.Clamp(num11 * -16f, 0f, 16f);
					float num14 = Mathf.Clamp(num10 * 16f, 0f, 16f);
					int num15 = Mathf.Clamp(Mathf.FloorToInt(num13), 0, 15);
					int num16 = Mathf.Clamp(Mathf.FloorToInt(num14), 0, 15);
					float num17 = heightArray[num16 * 17 + num15];
					float num18 = heightArray[num16 * 17 + num15 + 1];
					float num19 = heightArray[(num16 + 1) * 17 + num15];
					float num20 = heightArray[(num16 + 1) * 17 + num15 + 1];
					num17 += (num18 - num17) * (num13 - (float)num15);
					num19 += (num20 - num19) * (num13 - (float)num15);
					num21 = num12 + num17 + (num19 - num17) * (num14 - (float)num16);
					if (num21 < num12 + 0.01f)
					{
						goto IL_927;
					}
				}
				else if ((vector14.x - a.x) * vector13.z - (vector14.z - a.z) * vector13.x < 0f)
				{
					num21 = TerrainModify.SolveDown(vector9, vector10, vector14 - b);
				}
				else
				{
					num21 = TerrainModify.SolveDown(vector12, vector11, vector14 - d);
				}
				int num22 = (i - (heightsModified.m_minZ << 2)) * num8 + (j - (heightsModified.m_minX << 2));
				TerrainModify.HeightModification heightModification = tempHeights[num22];
				if (num9 <= num)
				{
					if ((heights & TerrainModify.Heights.PrimaryLevel) != TerrainModify.Heights.None)
					{
						heightModification.m_primaryMin = Mathf.Max(heightModification.m_primaryMin, num21);
						heightModification.m_primaryMax = Mathf.Min(heightModification.m_primaryMax, num21);
						heightModification.m_target += num21 * 4f;
						heightModification.m_divider += 4f;
					}
					if ((heights & TerrainModify.Heights.SecondaryLevel) != TerrainModify.Heights.None)
					{
						heightModification.m_secondaryMin = Mathf.Max(heightModification.m_secondaryMin, num21);
						heightModification.m_secondaryMax = Mathf.Min(heightModification.m_secondaryMax, num21);
						heightModification.m_target += num21 * 4f;
						heightModification.m_divider += 4f;
					}
					if ((heights & TerrainModify.Heights.PrimaryMax) != TerrainModify.Heights.None)
					{
						heightModification.m_primaryMax = Mathf.Min(heightModification.m_primaryMax, num21);
					}
					if ((heights & TerrainModify.Heights.BlockHeight) != TerrainModify.Heights.None)
					{
						heightModification.m_blockHeight = Mathf.Max(heightModification.m_blockHeight, num21);
					}
					if ((heights & TerrainModify.Heights.DigHeight) != TerrainModify.Heights.None)
					{
						heightModification.m_digHeight = Mathf.Min(heightModification.m_digHeight, num21);
					}
					if ((heights & TerrainModify.Heights.SecondaryMin) != TerrainModify.Heights.None)
					{
						heightModification.m_secondaryMin = Mathf.Max(heightModification.m_secondaryMin, num21);
					}
					if ((heights & TerrainModify.Heights.SecondaryMax) != TerrainModify.Heights.None)
					{
						heightModification.m_secondaryMax = Mathf.Min(heightModification.m_secondaryMax, num21);
					}
					if ((heights & TerrainModify.Heights.RawHeight) != TerrainModify.Heights.None)
					{
						uint num23 = 3u;
						if (((long)j & (long)((ulong)num23)) == 0L && ((long)i & (long)((ulong)num23)) == 0L)
						{
							int num24 = j >> 2;
							int num25 = i >> 2;
							instance.RawHeights2[num25 * 1081 + num24] = (ushort)Mathf.Clamp((int)(num21 * 65536f / 1024f), 0, 65535);
						}
					}
				}
				else if ((heights & (TerrainModify.Heights.PrimaryLevel | TerrainModify.Heights.SecondaryLevel)) != TerrainModify.Heights.None)
				{
					float num26 = Mathf.Sqrt(num9) - 5.656854f;
					float num27 = Mathf.Sqrt(12f / Mathf.Max(0.187878788f, num26)) * 0.5f - 0.5f;
					num27 = Mathf.Max(0f, num27 * 4f);
					heightModification.m_target += num21 * num27;
					heightModification.m_divider += num27;
				}
				tempHeights[num22] = heightModification;
				goto IL_927;
			}
		}
	}

	private static void ApplyQuad(Quad2 quad, TerrainModify.Edges edges, TerrainModify.Surface surface)
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		TerrainArea surfaceModified = instance.m_surfaceModified;
		TerrainManager.SurfaceCell[] tempSurface = instance.m_tempSurface;
		float num = 8f;
		Vector2 vector = quad.a;
		Vector2 vector2 = quad.b;
		Vector2 vector3 = quad.c;
		Vector2 vector4 = quad.d;
		if ((edges & (TerrainModify.Edges.AB | TerrainModify.Edges.CD)) != TerrainModify.Edges.None)
		{
			Vector2 vector5 = (quad.c - quad.b).get_normalized() * num;
			Vector2 vector6 = (quad.d - quad.a).get_normalized() * num;
			if ((edges & TerrainModify.Edges.AB) != TerrainModify.Edges.None)
			{
				vector -= vector6;
				vector2 -= vector5;
			}
			if ((edges & TerrainModify.Edges.CD) != TerrainModify.Edges.None)
			{
				vector4 += vector6;
				vector3 += vector5;
			}
		}
		if ((edges & (TerrainModify.Edges.BC | TerrainModify.Edges.DA)) != TerrainModify.Edges.None)
		{
			Vector2 vector7 = (quad.b - quad.a).get_normalized() * num;
			Vector2 vector8 = (quad.d - quad.c).get_normalized() * num;
			if ((edges & TerrainModify.Edges.BC) != TerrainModify.Edges.None)
			{
				vector2 += vector7;
				vector3 -= vector8;
			}
			if ((edges & TerrainModify.Edges.DA) != TerrainModify.Edges.None)
			{
				vector -= vector7;
				vector4 += vector8;
			}
		}
		int num2 = Mathf.Max((int)(Mathf.Min(Mathf.Min(vector.x, vector2.x), Mathf.Min(vector3.x, vector4.x)) / 4f + 2160f + 0.5f), surfaceModified.m_minX << 2);
		int num3 = Mathf.Max((int)(Mathf.Min(Mathf.Min(vector.y, vector2.y), Mathf.Min(vector3.y, vector4.y)) / 4f + 2160f + 0.5f), surfaceModified.m_minZ << 2);
		int num4 = Mathf.Min((int)(Mathf.Max(Mathf.Max(vector.x, vector2.x), Mathf.Max(vector3.x, vector4.x)) / 4f + 2160f - 0.5f), surfaceModified.m_maxX << 2);
		int num5 = Mathf.Min((int)(Mathf.Max(Mathf.Max(vector.y, vector2.y), Mathf.Max(vector3.y, vector4.y)) / 4f + 2160f - 0.5f), surfaceModified.m_maxZ << 2);
		float num6 = 17280f;
		int num7 = (surfaceModified.m_maxX - surfaceModified.m_minX << 2) + 1;
		num *= num;
		Vector2 vector9 = quad.a;
		Vector2 vector10 = quad.b;
		Vector2 vector11 = quad.b;
		Vector2 vector12 = quad.c;
		Vector2 vector13 = quad.c;
		Vector2 vector14 = quad.d;
		Vector2 vector15 = quad.d;
		Vector2 vector16 = quad.a;
		if (edges != TerrainModify.Edges.None && (edges & TerrainModify.Edges.Expand) == TerrainModify.Edges.None)
		{
			float num8 = 4f;
			if ((surface & TerrainModify.Surface.Clip) != TerrainModify.Surface.None)
			{
				num8 = 5.5f;
			}
			Vector2 normalized = (quad.b - quad.a).get_normalized();
			Vector2 normalized2 = (quad.c - quad.b).get_normalized();
			Vector2 normalized3 = (quad.d - quad.c).get_normalized();
			Vector2 normalized4 = (quad.a - quad.d).get_normalized();
			if ((edges & TerrainModify.Edges.AB) != TerrainModify.Edges.None)
			{
				vector9 -= normalized4 * (num8 / TerrainModify.GetAdjustedLength(normalized4, normalized));
				vector10 += normalized2 * (num8 / TerrainModify.GetAdjustedLength(normalized2, normalized));
				if ((vector10.x - vector9.x) * normalized.x + (vector10.y - vector9.y) * normalized.y <= 0f)
				{
					return;
				}
			}
			if ((edges & TerrainModify.Edges.BC) != TerrainModify.Edges.None)
			{
				vector11 -= normalized * (num8 / TerrainModify.GetAdjustedLength(normalized, normalized2));
				vector12 += normalized3 * (num8 / TerrainModify.GetAdjustedLength(normalized3, normalized2));
				if ((vector12.x - vector11.x) * normalized2.x + (vector12.y - vector11.y) * normalized2.y <= 0f)
				{
					return;
				}
			}
			if ((edges & TerrainModify.Edges.CD) != TerrainModify.Edges.None)
			{
				vector13 -= normalized2 * (num8 / TerrainModify.GetAdjustedLength(normalized2, normalized3));
				vector14 += normalized4 * (num8 / TerrainModify.GetAdjustedLength(normalized4, normalized3));
				if ((vector14.x - vector13.x) * normalized3.x + (vector14.y - vector13.y) * normalized3.y <= 0f)
				{
					return;
				}
			}
			if ((edges & TerrainModify.Edges.DA) != TerrainModify.Edges.None)
			{
				vector15 -= normalized3 * (num8 / TerrainModify.GetAdjustedLength(normalized3, normalized4));
				vector16 += normalized * (num8 / TerrainModify.GetAdjustedLength(normalized, normalized4));
				if ((vector16.x - vector15.x) * normalized4.x + (vector16.y - vector15.y) * normalized4.y <= 0f)
				{
					return;
				}
			}
		}
		edges = ~edges;
		for (int i = num3; i <= num5; i++)
		{
			int j = num2;
			while (j <= num4)
			{
				Vector2 p;
				p.x = (float)j * 4f - num6 * 0.5f + 2f;
				p.y = (float)i * 4f - num6 * 0.5f + 2f;
				TerrainModify.Edges edges2 = TerrainModify.Edges.None;
				if ((p.x - vector9.x) * (vector10.y - vector9.y) - (p.y - vector9.y) * (vector10.x - vector9.x) >= 0f)
				{
					goto IL_62A;
				}
				edges2 |= TerrainModify.Edges.AB;
				if ((edges & edges2) == TerrainModify.Edges.None)
				{
					goto IL_62A;
				}
				IL_941:
				j++;
				continue;
				IL_62A:
				if ((p.x - vector11.x) * (vector12.y - vector11.y) - (p.y - vector11.y) * (vector12.x - vector11.x) < 0f)
				{
					edges2 |= TerrainModify.Edges.BC;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_941;
					}
				}
				if ((p.x - vector13.x) * (vector14.y - vector13.y) - (p.y - vector13.y) * (vector14.x - vector13.x) < 0f)
				{
					edges2 |= TerrainModify.Edges.CD;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_941;
					}
				}
				if ((p.x - vector15.x) * (vector16.y - vector15.y) - (p.y - vector15.y) * (vector16.x - vector15.x) < 0f)
				{
					edges2 |= TerrainModify.Edges.DA;
					if ((edges & edges2) != TerrainModify.Edges.None)
					{
						goto IL_941;
					}
				}
				float num9 = 0f;
				if ((edges2 & TerrainModify.Edges.AB) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector9, vector10, p));
					if (num9 >= num)
					{
						goto IL_941;
					}
				}
				if ((edges2 & TerrainModify.Edges.BC) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector11, vector12, p));
					if (num9 >= num)
					{
						goto IL_941;
					}
				}
				if ((edges2 & TerrainModify.Edges.CD) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector13, vector14, p));
					if (num9 >= num)
					{
						goto IL_941;
					}
				}
				if ((edges2 & TerrainModify.Edges.DA) != TerrainModify.Edges.None)
				{
					num9 = Mathf.Max(num9, TerrainModify.SqrOffset(vector15, vector16, p));
					if (num9 >= num)
					{
						goto IL_941;
					}
				}
				int num10 = (i - (surfaceModified.m_minZ << 2)) * num7 + (j - (surfaceModified.m_minX << 2));
				TerrainManager.SurfaceCell surfaceCell = tempSurface[num10];
				float num11 = 1f - Mathf.Sqrt(num9 / num);
				if ((surface & TerrainModify.Surface.Clip) != TerrainModify.Surface.None)
				{
					surfaceCell.m_clipped = (byte)Mathf.Max((float)surfaceCell.m_clipped, num11 * 255f);
				}
				if ((surface & TerrainModify.Surface.PavementA) != TerrainModify.Surface.None)
				{
					surfaceCell.m_pavementA = (byte)Mathf.Max((float)surfaceCell.m_pavementA, num11 * 255f);
				}
				if ((surface & TerrainModify.Surface.PavementB) != TerrainModify.Surface.None)
				{
					surfaceCell.m_pavementB = (byte)Mathf.Max((float)surfaceCell.m_pavementB, num11 * 255f);
				}
				if ((surface & TerrainModify.Surface.Ruined) != TerrainModify.Surface.None)
				{
					surfaceCell.m_ruined = (byte)Mathf.Max((float)surfaceCell.m_ruined, num11 * 255f);
				}
				if ((surface & TerrainModify.Surface.RuinedWeak) != TerrainModify.Surface.None)
				{
					surfaceCell.m_ruined = (byte)Mathf.Max((float)surfaceCell.m_ruined, num11 * 208f);
				}
				if ((surface & TerrainModify.Surface.Gravel) != TerrainModify.Surface.None)
				{
					surfaceCell.m_gravel = (byte)Mathf.Max((float)surfaceCell.m_gravel, num11 * 255f);
				}
				if ((surface & TerrainModify.Surface.Field) != TerrainModify.Surface.None)
				{
					surfaceCell.m_field = (byte)Mathf.Max((float)surfaceCell.m_field, num11 * 255f);
				}
				tempSurface[num10] = surfaceCell;
				goto IL_941;
			}
		}
	}

	[Flags]
	public enum Edges
	{
		None = 0,
		AB = 1,
		BC = 2,
		CD = 4,
		DA = 8,
		All = 15,
		Expand = 16
	}

	[Flags]
	public enum Heights
	{
		None = 0,
		PrimaryLevel = 1,
		SecondaryLevel = 2,
		PrimaryMax = 4,
		BlockHeight = 8,
		SecondaryMin = 16,
		DigHeight = 32,
		RawHeight = 64,
		SecondaryMax = 128,
		Any = 255
	}

	[Flags]
	public enum Surface
	{
		None = 0,
		[EnumPosition(4)]
		Clip = 1,
		PavementA = 2,
		[EnumPosition(0)]
		PavementB = 4,
		[EnumPosition(2)]
		Ruined = 8,
		[EnumPosition(1)]
		Gravel = 16,
		[EnumPosition(3)]
		Field = 32,
		RuinedWeak = 64,
		Any = 255
	}

	public struct HeightModification
	{
		public float m_target;

		public float m_divider;

		public float m_primaryMin;

		public float m_primaryMax;

		public float m_secondaryMin;

		public float m_secondaryMax;

		public float m_blockHeight;

		public float m_digHeight;
	}

	public struct ZoneModification
	{
		public float m_minD2;

		public byte m_angle;

		public byte m_zone;

		public sbyte m_offsetX;

		public sbyte m_offsetZ;
	}
}
