﻿using System;
using ColossalFramework;
using UnityEngine;

public struct NotificationEvent
{
	public NotificationEvent(NotificationEvent.Type type, Vector3 position, float scale)
	{
		this.m_type = type;
		this.m_problems = Notification.Problem.None;
		this.m_position = position;
		this.m_scale = scale;
		this.m_timer = 0f;
	}

	public NotificationEvent(NotificationEvent.Type type, Vector3 position, float scale, float delay)
	{
		this.m_type = type;
		this.m_problems = Notification.Problem.None;
		this.m_position = position;
		this.m_scale = scale;
		this.m_timer = -delay;
	}

	public NotificationEvent(NotificationEvent.Type type, Notification.Problem notifications, Vector3 position, float scale)
	{
		this.m_type = type;
		this.m_problems = notifications;
		this.m_position = position;
		this.m_scale = scale;
		this.m_timer = 0f;
	}

	public bool UpdateEvent(float deltaTime)
	{
		this.m_timer += deltaTime;
		if (this.m_type == NotificationEvent.Type.NotificationRemoval)
		{
			return this.m_timer < 0.5f;
		}
		return this.m_timer < 2f;
	}

	public void RenderInstance(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_timer <= 0f)
		{
			return;
		}
		NotificationEvent.Type type = this.m_type;
		if (type != NotificationEvent.Type.Happy && type != NotificationEvent.Type.Sad && type != NotificationEvent.Type.LevelUp)
		{
			if (type != NotificationEvent.Type.NotificationRemoval)
			{
				float num = Mathf.Abs(this.m_timer - 1f);
				num = 1f - num * num;
				float scale = this.m_scale * (0.75f + this.m_timer * 0.25f);
				Vector3 position = this.m_position;
				position.y += this.m_timer * 1.5f;
				NotificationEvent.RenderInstance(cameraInfo, this.m_type, position, scale, num);
			}
			else
			{
				float num2 = 1f - this.m_timer * 2f;
				float num3 = this.m_scale * (1f + this.m_timer * 2f);
				Vector3 position2 = this.m_position;
				position2.y += this.m_timer * 5f;
				NotificationManager.BufferedItem item;
				item.m_position = new Vector4(position2.x, position2.y, position2.z, num3);
				item.m_params = new Vector4(0f, num2, 0f, 0f);
				item.m_distanceSqr = Vector3.SqrMagnitude(position2 - cameraInfo.m_position);
				item.m_regionIndex = 0;
				if ((this.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
				{
					item.m_regionIndex = 78;
				}
				else if ((this.m_problems & Notification.Problem.MajorProblem) != Notification.Problem.None)
				{
					item.m_regionIndex = 39;
				}
				NotificationManager instance = Singleton<NotificationManager>.get_instance();
				for (int i = 0; i < 39; i++)
				{
					if ((this.m_problems & (Notification.Problem)(1L << i)) != Notification.Problem.None)
					{
						item.m_regionIndex += i;
						instance.m_bufferedItems.Add(item);
					}
				}
			}
		}
		else
		{
			float num4 = Mathf.Abs(this.m_timer - 1f);
			num4 = 1f - num4 * num4;
			Vector3 position3 = this.m_position;
			position3.y += this.m_timer * 10f - 5f;
			NotificationEvent.RenderInstance(cameraInfo, this.m_type, position3, this.m_scale, num4);
		}
	}

	public static void RenderInstance(RenderManager.CameraInfo cameraInfo, NotificationEvent.Type type, Vector3 position, float scale, float alpha)
	{
		NotificationManager instance = Singleton<NotificationManager>.get_instance();
		NotificationManager.BufferedItem item;
		item.m_position = new Vector4(position.x, position.y, position.z, scale);
		item.m_params = new Vector4(0f, alpha, 0f, 0f);
		item.m_distanceSqr = Vector3.SqrMagnitude(position - cameraInfo.m_position);
		item.m_regionIndex = (int)(117 + type);
		instance.m_bufferedItems.Add(item);
	}

	public const int TypeCount = 26;

	private NotificationEvent.Type m_type;

	private Notification.Problem m_problems;

	private Vector3 m_position;

	private float m_scale;

	private float m_timer;

	public enum Type
	{
		[EnumPosition("BuildingEventHappy", 0)]
		Happy,
		[EnumPosition("BuildingEventSad", 0)]
		Sad,
		[EnumPosition("BuildingEventGainElectricity", 0)]
		GainElectricity,
		[EnumPosition("BuildingEventGainElectricityNegative", 0)]
		LoseElectricity,
		[EnumPosition("BuildingEventGainHappiness", 0)]
		GainHappiness,
		[EnumPosition("BuildingEventGainHappinessNegative", 0)]
		LoseHappiness,
		[EnumPosition("BuildingEventGainHealth", 0)]
		GainHealth,
		[EnumPosition("BuildingEventGainHealthNegative", 0)]
		LoseHealth,
		[EnumPosition("BuildingEventGainPopulation", 0)]
		GainPopulation,
		[EnumPosition("BuildingEventGainPopulationNegative", 0)]
		LosePopulation,
		[EnumPosition("BuildingEventGainTaxes", 0)]
		GainTaxes,
		[EnumPosition("BuildingEventGainTaxesNegative", 0)]
		LoseTaxes,
		[EnumPosition("BuildingEventGainWater", 0)]
		GainWater,
		[EnumPosition("BuildingEventGainWaterNegative", 0)]
		LoseWater,
		[EnumPosition("BuildingEventGainGarbage", 0)]
		GainGarbage,
		[EnumPosition("BuildingEventGainGarbageNegative", 0)]
		LoseGarbage,
		[EnumPosition("BuildingEventLevelUp", 0)]
		LevelUp,
		[EnumPosition("LocationMarkerActiveNormal", 0)]
		LocationMarker,
		[EnumPosition("BuildingEventPollution", 0)]
		Pollution,
		[EnumPosition("BuildingEventNoisePollution", 0)]
		NoisePollution,
		[EnumPosition("BuildingNotificationBulldozer", 0)]
		Bulldozer,
		NotificationRemoval,
		[EnumPosition("IconJunctionTrafficLights", 0)]
		TrafficLightsOn,
		[EnumPosition("IconJunctionNoTrafficLights", 0)]
		TrafficLightsOff,
		[EnumPosition("IconJunctionStopSign", 0)]
		YieldOn,
		[EnumPosition("IconJunctionNoStopSign", 0)]
		YieldOff
	}
}
