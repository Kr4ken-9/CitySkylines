﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class ConditionItem : UICustomControl
{
	private void Awake()
	{
		this.m_dropDown = base.Find<UIDropDown>("Dropdown");
		this.m_dropDown.add_eventEnterFocus(new FocusEventHandler(this.OnDropDownEnterFocus));
		this.m_dropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionIndexChanged));
		this.m_triggerDropDown = base.Find<UIDropDown>("TriggerDropdown");
		this.m_triggerDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTriggerIndexChanged));
		this.m_buildingDropDown = base.Find<UIDropDown>("BuildingDropdown");
		this.m_buildingDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnBuildingIndexChanged));
		this.m_buildingAmountLabel = base.Find<UILabel>("BuildingAmountLabel");
		this.m_amountInfo = base.Find<UIPanel>("AmountInfo");
		this.m_triggerInfo = base.Find<UIPanel>("TriggerInfo");
		this.m_buildingInfo = base.Find<UIPanel>("BuildingInfo");
		this.m_numberField = this.m_amountInfo.get_gameObject().AddComponent<NumberField>();
		this.m_numberField.eventValueChanged += new NumberField.ValueChangedHandler(this.OnAmountValueChanged);
		this.m_buildingNumberField = this.m_buildingInfo.get_gameObject().AddComponent<NumberField>();
		this.m_buildingNumberField.m_minValue = this.m_minBuildingAmount;
		this.m_buildingNumberField.m_maxValue = this.m_maxBuildingAmount;
		this.m_buildingNumberField.value = this.m_defaultBuildingAmount;
		this.m_buildingNumberField.eventValueChanged += new NumberField.ValueChangedHandler(this.OnBuildingAmountValueChanged);
		this.m_switchUpArrow = base.Find<UIButton>("ArrowSwitchUp");
		this.m_switchDownArrow = base.Find<UIButton>("ArrowSwitchDown");
	}

	public void Init(ConditionList conditionList)
	{
		this.m_conditionList = conditionList;
		this.m_selectedOption = this.m_conditionList.GetCurrentDefaultOption();
		this.m_selectedCondition = this.m_selectedOption.AddToTrigger(this.m_conditionList.currentTrigger, 0);
		this.RefreshItemsList();
		this.RefreshUIValues(true);
		this.RefreshSwitchArrows();
	}

	public void AssignExistingCondition(ConditionList conditionList, MilestoneInfo condition)
	{
		this.m_conditionList = conditionList;
		this.m_selectedCondition = condition;
		StatisticMilestone statisticMilestone = this.m_selectedCondition as StatisticMilestone;
		TimeMilestone timeMilestone = this.m_selectedCondition as TimeMilestone;
		TriggerMilestone triggerMilestone = this.m_selectedCondition as TriggerMilestone;
		GameAreaMilestone gameAreaMilestone = this.m_selectedCondition as GameAreaMilestone;
		WrapperMilestone wrapperMilestone = this.m_selectedCondition as WrapperMilestone;
		BuildingCountMilestone buildingCountMilestone = this.m_selectedCondition as BuildingCountMilestone;
		if (statisticMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.FindOptionForStatisticMilestone(statisticMilestone);
			this.RefreshUIValues(false);
			this.GetValueFromStatisticMilestone(statisticMilestone);
		}
		else if (timeMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.m_timeLimitOption;
			this.RefreshUIValues(false);
			this.m_numberField.value = timeMilestone.m_targetGameWeeks;
		}
		else if (triggerMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.m_triggerOption;
			this.RefreshUIValues(false);
			this.m_triggerDropDown.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTriggerIndexChanged));
			this.m_triggerDropDown.set_selectedIndex(this.GetCurrentTriggerIndex(triggerMilestone));
			this.m_triggerDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnTriggerIndexChanged));
		}
		else if (gameAreaMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.m_gameAreaOption;
			this.RefreshUIValues(false);
			this.m_numberField.value = gameAreaMilestone.m_targetTiles;
		}
		else if (wrapperMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.m_buildingsOption;
			this.RefreshUIValues(false);
			this.m_buildingDropDown.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnBuildingIndexChanged));
			this.m_buildingDropDown.set_selectedIndex(this.GetCurrentBuildingIndex(wrapperMilestone));
			this.m_buildingDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnBuildingIndexChanged));
			this.m_buildingNumberField.value = wrapperMilestone.m_targetCount;
			this.RefreshBuildingAmountLabel();
		}
		else if (buildingCountMilestone != null)
		{
			this.m_selectedOption = this.m_conditionList.FindOptionForServiceMilestone(buildingCountMilestone);
			this.RefreshUIValues(false);
			this.m_numberField.value = buildingCountMilestone.m_targetCount;
		}
		this.RefreshItemsList();
		this.RefreshSwitchArrows();
	}

	private int GetCurrentTriggerIndex(TriggerMilestone currentTrigger)
	{
		int num = 0;
		TriggerMilestone[] availableTriggers = this.m_selectedOption.m_availableTriggers;
		for (int i = 0; i < availableTriggers.Length; i++)
		{
			TriggerMilestone triggerMilestone = availableTriggers[i];
			if (triggerMilestone == currentTrigger)
			{
				return num;
			}
			num++;
		}
		Debug.LogError("Trigger index not found.");
		return -1;
	}

	private int GetCurrentBuildingIndex(WrapperMilestone wrapperMilestone)
	{
		int num = 0;
		MilestoneInfo[] availableBuildingMilestones = this.m_selectedOption.m_availableBuildingMilestones;
		for (int i = 0; i < availableBuildingMilestones.Length; i++)
		{
			MilestoneInfo milestoneInfo = availableBuildingMilestones[i];
			if (wrapperMilestone.m_requirePassed.GetLocalizedName() == milestoneInfo.GetLocalizedName())
			{
				return num;
			}
			num++;
		}
		Debug.LogError("Building index not found.");
		return -1;
	}

	private void DisplayInDropdown(string optionString)
	{
		string[] items = new string[]
		{
			optionString
		};
		this.m_dropDown.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionIndexChanged));
		this.m_dropDown.set_selectedIndex(0);
		this.m_dropDown.set_items(items);
		this.m_dropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionIndexChanged));
	}

	private void OnDropDownEnterFocus(UIComponent component, UIFocusEventParameter eventParam)
	{
		this.RefreshItemsList();
	}

	private void RefreshItemsList()
	{
		List<string> list = new List<string>();
		list.AddRange(this.m_conditionList.GetAvailableOptions(this.m_selectedOption));
		this.m_dropDown.set_items(list.ToArray());
		this.m_dropDown.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionIndexChanged));
		this.m_dropDown.set_selectedIndex(this.m_selectedOption.m_dropDownIndex);
		this.m_dropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnConditionIndexChanged));
	}

	private void OnConditionIndexChanged(UIComponent comp, int index)
	{
		this.m_selectedOption.RemoveFromTrigger(this.m_conditionList.currentTrigger, this.m_selectedCondition);
		this.m_selectedOption = this.m_conditionList.FindOptionWithDropdownIndex(index);
		this.m_selectedCondition = this.m_selectedOption.AddToTrigger(this.m_conditionList.currentTrigger, 0);
		StatisticMilestone statisticMilestone = this.m_selectedCondition as StatisticMilestone;
		if (statisticMilestone != null && statisticMilestone.m_value == StatisticType.CityValue)
		{
			this.m_dropDown.set_tooltip(Locale.Get("CITYVALUETUTOOLTIP"));
		}
		else
		{
			this.m_dropDown.set_tooltip(string.Empty);
			this.m_dropDown.get_tooltipBox().Hide();
		}
		this.RefreshUIValues(true);
	}

	private void RefreshUIValues(bool alsoSetDefaultValue)
	{
		StatisticMilestone statisticMilestone = this.m_selectedCondition as StatisticMilestone;
		TimeMilestone timeMilestone = this.m_selectedCondition as TimeMilestone;
		TriggerMilestone triggerMilestone = this.m_selectedCondition as TriggerMilestone;
		GameAreaMilestone gameAreaMilestone = this.m_selectedCondition as GameAreaMilestone;
		WrapperMilestone wrapperMilestone = this.m_selectedCondition as WrapperMilestone;
		BuildingCountMilestone buildingCountMilestone = this.m_selectedCondition as BuildingCountMilestone;
		if (statisticMilestone != null)
		{
			this.ShowNumberUI();
			statisticMilestone.m_targetType = this.m_selectedOption.m_statisticsPreset.m_targetType;
			this.RefreshMinMaxFromPreset(this.m_selectedOption.m_statisticsPreset);
			int num = this.CalcDefaultValue();
			if (alsoSetDefaultValue)
			{
				this.m_numberField.value = num;
			}
			this.RefreshArrowStep(num);
			this.m_numberField.SetLabel(this.GetStatisticMilestoneUnit(statisticMilestone));
		}
		else if (timeMilestone != null)
		{
			this.ShowNumberUI();
			if (alsoSetDefaultValue)
			{
				this.m_numberField.value = this.m_defaultTimeLimit;
			}
			this.m_numberField.m_minValue = this.m_minTimeLimit;
			this.m_numberField.m_maxValue = this.m_maxTimeLimit;
			this.RefreshArrowStep(this.m_defaultTimeLimit);
			this.m_numberField.SetLabel(Locale.Get("DISASTERLIST_WEEKS"));
		}
		else if (triggerMilestone != null)
		{
			this.ShowTriggerUI();
			List<string> list = new List<string>();
			TriggerMilestone[] availableTriggers = this.m_selectedOption.m_availableTriggers;
			for (int i = 0; i < availableTriggers.Length; i++)
			{
				TriggerMilestone triggerMilestone2 = availableTriggers[i];
				list.Add(triggerMilestone2.GetLocalizedName());
			}
			this.m_triggerDropDown.set_items(list.ToArray());
			if (alsoSetDefaultValue)
			{
				this.m_triggerDropDown.set_selectedIndex(0);
			}
		}
		else if (gameAreaMilestone != null)
		{
			this.ShowNumberUI();
			if (alsoSetDefaultValue)
			{
				this.m_numberField.value = this.m_defaultMapTiles;
			}
			this.m_numberField.m_minValue = this.m_minMapTiles;
			this.m_numberField.m_maxValue = Singleton<GameAreaManager>.get_instance().MaxAreaCount;
			this.RefreshArrowStep(this.m_defaultMapTiles);
			this.m_numberField.SetLabel(Locale.Get("CONDITION_MAPTILES"));
		}
		else if (wrapperMilestone != null)
		{
			this.ShowBuildingUI();
			List<string> list2 = new List<string>();
			MilestoneInfo[] manualMilestones = MilestoneCollection.GetManualMilestones(ManualMilestone.Type.Create);
			for (int j = 0; j < manualMilestones.Length; j++)
			{
				MilestoneInfo milestoneInfo = manualMilestones[j];
				list2.Add(milestoneInfo.GetLocalizedName());
			}
			this.m_buildingDropDown.set_items(list2.ToArray());
			if (alsoSetDefaultValue)
			{
				this.m_buildingDropDown.set_selectedIndex(0);
				this.m_buildingNumberField.value = this.m_defaultBuildingAmount;
			}
			this.RefreshBuildingAmountLabel();
		}
		else if (buildingCountMilestone != null)
		{
			this.ShowNumberUI();
			if (alsoSetDefaultValue)
			{
				this.m_numberField.value = this.m_defaultBuildingAmount;
			}
			this.m_numberField.m_minValue = this.m_minBuildingAmount;
			this.m_numberField.m_maxValue = this.m_maxBuildingAmount;
			this.RefreshArrowStep(this.m_defaultBuildingAmount);
			this.m_numberField.SetLabel(Locale.Get("GOALSPANEL_AMOUNT"));
		}
	}

	private string GetStatisticMilestoneUnit(StatisticMilestone statisticMilestone)
	{
		if (this.IsMoneyCondition(statisticMilestone.m_value))
		{
			return "₡";
		}
		if (statisticMilestone.m_divider == StatisticType.None && this.IsElectricityCondition(statisticMilestone.m_value))
		{
			return "MW";
		}
		return string.Empty;
	}

	private void RefreshArrowStep(int defaultValue)
	{
		int num = defaultValue.ToString().Length - 1;
		int num2 = Mathf.RoundToInt(0.5f * Mathf.Pow(10f, (float)num));
		if (num2 < 1)
		{
			num2 = 1;
		}
		this.m_numberField.m_arrowStep = num2;
	}

	private void ShowTriggerUI()
	{
		this.m_amountInfo.set_isVisible(false);
		this.m_triggerInfo.set_isVisible(true);
		this.m_buildingInfo.set_isVisible(false);
	}

	private void ShowNumberUI()
	{
		this.m_amountInfo.set_isVisible(true);
		this.m_triggerInfo.set_isVisible(false);
		this.m_buildingInfo.set_isVisible(false);
	}

	private void ShowBuildingUI()
	{
		this.m_amountInfo.set_isVisible(false);
		this.m_triggerInfo.set_isVisible(false);
		this.m_buildingInfo.set_isVisible(true);
	}

	private void OnAmountValueChanged(int value)
	{
		StatisticMilestone statisticMilestone = this.m_selectedCondition as StatisticMilestone;
		TimeMilestone timeMilestone = this.m_selectedCondition as TimeMilestone;
		GameAreaMilestone gameAreaMilestone = this.m_selectedCondition as GameAreaMilestone;
		BuildingCountMilestone buildingCountMilestone = this.m_selectedCondition as BuildingCountMilestone;
		if (statisticMilestone != null)
		{
			this.SetValueToStatisticMilestone(statisticMilestone, value);
		}
		else if (timeMilestone != null)
		{
			timeMilestone.m_targetGameWeeks = value;
		}
		else if (gameAreaMilestone != null)
		{
			gameAreaMilestone.m_targetTiles = value;
		}
		else if (buildingCountMilestone != null)
		{
			buildingCountMilestone.m_targetCount = value;
		}
	}

	private void OnBuildingAmountValueChanged(int value)
	{
		WrapperMilestone wrapperMilestone = this.m_selectedCondition as WrapperMilestone;
		wrapperMilestone.m_targetCount = value;
		this.RefreshBuildingAmountLabel();
	}

	private void RefreshBuildingAmountLabel()
	{
		WrapperMilestone wrapperMilestone = this.m_selectedCondition as WrapperMilestone;
		string text = "GOALSPANEL_AMOUNT";
		ManualMilestone manualMilestone = wrapperMilestone.m_requirePassed as ManualMilestone;
		if (manualMilestone != null)
		{
			NetInfo netInfo = manualMilestone.m_prefab as NetInfo;
			if (netInfo != null)
			{
				text = "GOALSPANEL_SEGMENTS";
			}
		}
		this.m_buildingAmountLabel.set_text(Locale.Get(text));
	}

	private void OnBuildingIndexChanged(UIComponent comp, int index)
	{
		this.DeleteSelectedCondition();
		this.m_selectedCondition = this.m_selectedOption.AddToTrigger(this.m_conditionList.currentTrigger, index);
		this.OnBuildingAmountValueChanged(this.m_buildingNumberField.value);
	}

	private void OnTriggerIndexChanged(UIComponent comp, int index)
	{
		this.DeleteSelectedCondition();
		this.m_selectedCondition = this.m_selectedOption.AddToTrigger(this.m_conditionList.currentTrigger, index);
	}

	private void RefreshMinMaxFromPreset(ScenarioGoalPresets.StatisticPreset preset)
	{
		switch (preset.m_targetType)
		{
		case StatisticMilestone.TargetType.TotalInt_Greater:
			this.m_numberField.m_minValue = this.m_selectedOption.m_statisticsPreset.m_minTotalInt;
			this.m_numberField.m_maxValue = this.m_selectedOption.m_statisticsPreset.m_maxTotalInt;
			break;
		case StatisticMilestone.TargetType.LatestInt_Greater:
		case StatisticMilestone.TargetType.LatestInt_Less:
			this.m_numberField.m_minValue = this.m_selectedOption.m_statisticsPreset.m_minLatestInt;
			this.m_numberField.m_maxValue = this.m_selectedOption.m_statisticsPreset.m_maxLatestInt;
			break;
		case StatisticMilestone.TargetType.TotalFloat_Greater:
			this.m_numberField.m_minValue = (int)(this.m_selectedOption.m_statisticsPreset.m_minTotalFloat * 100f);
			this.m_numberField.m_maxValue = (int)(this.m_selectedOption.m_statisticsPreset.m_maxTotalFloat * 100f);
			break;
		case StatisticMilestone.TargetType.LatestFloat_Greater:
		case StatisticMilestone.TargetType.LatestFloat_Less:
			this.m_numberField.m_minValue = (int)(this.m_selectedOption.m_statisticsPreset.m_minLatestFloat * 100f);
			this.m_numberField.m_maxValue = (int)(this.m_selectedOption.m_statisticsPreset.m_maxLatestFloat * 100f);
			break;
		default:
			Debug.LogError("Could not resolve target type " + preset.m_targetType.ToString() + " for getting min an max values");
			break;
		}
		if (this.IsMoneyCondition(preset.m_value))
		{
			this.m_numberField.m_minValue /= 100;
			this.m_numberField.m_maxValue /= 100;
		}
		if (preset.m_divider == StatisticType.None && this.IsElectricityCondition(preset.m_value))
		{
			this.m_numberField.m_minValue = this.m_numberField.m_minValue * 2 / 125;
			this.m_numberField.m_maxValue = this.m_numberField.m_maxValue * 2 / 125;
		}
	}

	private int CalcDefaultValue()
	{
		return Mathf.RoundToInt((float)this.m_numberField.m_minValue + (float)(this.m_numberField.m_maxValue - this.m_numberField.m_minValue) * 0.75f);
	}

	private void GetValueFromStatisticMilestone(StatisticMilestone statisticMilestone)
	{
		switch (statisticMilestone.m_targetType)
		{
		case StatisticMilestone.TargetType.TotalInt_Greater:
			this.m_numberField.m_minValue = this.m_selectedOption.m_statisticsPreset.m_minTotalInt;
			this.m_numberField.m_maxValue = this.m_selectedOption.m_statisticsPreset.m_maxTotalInt;
			this.m_numberField.value = statisticMilestone.m_targetTotalInt;
			break;
		case StatisticMilestone.TargetType.LatestInt_Greater:
		case StatisticMilestone.TargetType.LatestInt_Less:
			this.m_numberField.m_minValue = this.m_selectedOption.m_statisticsPreset.m_minLatestInt;
			this.m_numberField.m_maxValue = this.m_selectedOption.m_statisticsPreset.m_maxLatestInt;
			this.m_numberField.value = statisticMilestone.m_targetLatestInt;
			break;
		case StatisticMilestone.TargetType.TotalFloat_Greater:
			this.m_numberField.m_minValue = (int)(this.m_selectedOption.m_statisticsPreset.m_minTotalFloat * 100f);
			this.m_numberField.m_maxValue = (int)(this.m_selectedOption.m_statisticsPreset.m_maxTotalFloat * 100f);
			this.m_numberField.value = (int)(statisticMilestone.m_targetTotalFloat * 100f);
			break;
		case StatisticMilestone.TargetType.LatestFloat_Greater:
		case StatisticMilestone.TargetType.LatestFloat_Less:
			this.m_numberField.m_minValue = (int)(this.m_selectedOption.m_statisticsPreset.m_minLatestFloat * 100f);
			this.m_numberField.m_maxValue = (int)(this.m_selectedOption.m_statisticsPreset.m_maxLatestFloat * 100f);
			this.m_numberField.value = (int)(statisticMilestone.m_targetLatestFloat * 100f);
			break;
		default:
			Debug.LogError("Could not resolve target type " + statisticMilestone.m_targetType.ToString() + " for getting value");
			break;
		}
		if (this.IsMoneyCondition(statisticMilestone.m_value))
		{
			this.m_numberField.m_minValue /= 100;
			this.m_numberField.m_maxValue /= 100;
			this.m_numberField.value /= 100;
		}
		if (statisticMilestone.m_divider == StatisticType.None && this.IsElectricityCondition(statisticMilestone.m_value))
		{
			this.m_numberField.m_minValue = this.m_numberField.m_minValue * 2 / 125;
			this.m_numberField.m_maxValue = this.m_numberField.m_maxValue * 2 / 125;
			this.m_numberField.value = this.m_numberField.value * 2 / 125;
		}
	}

	private void SetValueToStatisticMilestone(StatisticMilestone statisticMilestone, int value)
	{
		if (this.IsMoneyCondition(statisticMilestone.m_value))
		{
			value *= 100;
		}
		if (statisticMilestone.m_divider == StatisticType.None && this.IsElectricityCondition(statisticMilestone.m_value))
		{
			value = (value * 125 + 1) / 2;
		}
		switch (statisticMilestone.m_targetType)
		{
		case StatisticMilestone.TargetType.TotalInt_Greater:
			statisticMilestone.m_targetTotalInt = value;
			break;
		case StatisticMilestone.TargetType.LatestInt_Greater:
		case StatisticMilestone.TargetType.LatestInt_Less:
			statisticMilestone.m_targetLatestInt = value;
			break;
		case StatisticMilestone.TargetType.TotalFloat_Greater:
			statisticMilestone.m_targetTotalFloat = (float)value / 100f;
			break;
		case StatisticMilestone.TargetType.LatestFloat_Greater:
		case StatisticMilestone.TargetType.LatestFloat_Less:
			statisticMilestone.m_targetLatestFloat = (float)value / 100f;
			break;
		default:
			Debug.LogError("Could not resolve target type " + statisticMilestone.m_targetType.ToString() + " for setting value");
			break;
		}
	}

	private bool IsMoneyCondition(StatisticType statistictype)
	{
		return statistictype == StatisticType.PlayerMoney || statistictype == StatisticType.PlayerDebt || statistictype == StatisticType.CityValue || statistictype == StatisticType.ServiceExpenses || statistictype == StatisticType.ServiceIncome;
	}

	private bool IsElectricityCondition(StatisticType statistictype)
	{
		return statistictype == StatisticType.ElectricityCapacity || statistictype == StatisticType.RenewableElectricity || statistictype == StatisticType.PollutingElectricity;
	}

	public void OnDeleteButtonPressed()
	{
		this.m_conditionList.DeleteItem(this);
	}

	public void DeleteSelectedCondition()
	{
		this.m_selectedOption.RemoveFromTrigger(this.m_conditionList.currentTrigger, this.m_selectedCondition);
	}

	public void OnArrowSwitchUp()
	{
		this.m_conditionList.SwitchConditionUp(this);
	}

	public void OnArrowSwitchDown()
	{
		this.m_conditionList.SwitchConditionDown(this);
	}

	public void RefreshSwitchArrows()
	{
		bool flag = false;
		bool flag2 = false;
		this.m_conditionList.GetArrowStates(this, out flag, out flag2);
		this.m_switchUpArrow.set_isEnabled(!flag);
		this.m_switchDownArrow.set_isEnabled(!flag2);
	}

	private ConditionList m_conditionList;

	private UIDropDown m_dropDown;

	private UIDropDown m_triggerDropDown;

	private UIDropDown m_buildingDropDown;

	private MilestoneInfo m_selectedCondition;

	private NumberField m_numberField;

	private NumberField m_buildingNumberField;

	private UILabel m_buildingAmountLabel;

	public ConditionList.Option m_selectedOption;

	private UIPanel m_amountInfo;

	private UIPanel m_triggerInfo;

	private UIPanel m_buildingInfo;

	private UIButton m_switchUpArrow;

	private UIButton m_switchDownArrow;

	public int m_minTimeLimit;

	public int m_maxTimeLimit = 9999;

	public int m_defaultTimeLimit = 4;

	public int m_minMapTiles = 2;

	public int m_defaultMapTiles = 5;

	public int m_minBuildingAmount = 1;

	public int m_maxBuildingAmount = 99999;

	public int m_defaultBuildingAmount = 10;
}
