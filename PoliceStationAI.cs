﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using UnityEngine;

public class PoliceStationAI : PlayerBuildingAI
{
	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		if (this.m_noiseAccumulation != 0)
		{
			resource1 = ImmaterialResourceManager.Resource.NoisePollution;
			radius1 = this.m_noiseRadius;
		}
		else
		{
			resource1 = ImmaterialResourceManager.Resource.None;
			radius1 = 0f;
		}
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.CrimeRate)
		{
			if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
		}
		else
		{
			if (infoMode == InfoManager.InfoMode.NoisePollution && this.m_noiseAccumulation != 0)
			{
				return CommonBuildingAI.GetNoisePollutionColor((float)this.m_noiseAccumulation);
			}
			return base.GetColor(buildingID, ref data, infoMode);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return this.m_noiseAccumulation;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.CrimeRate;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			subMode = InfoManager.SubInfoMode.WaterPower;
		}
		else
		{
			subMode = InfoManager.SubInfoMode.Default;
		}
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingID, 0, 0, workCount, this.m_jailCapacity, 0, 0);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_jailCapacity, 0);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		int workCount = this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.EnsureCitizenUnits(buildingID, ref data, 0, workCount, this.m_jailCapacity, 0);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		if (this.m_policeDepartmentAccumulation != 0)
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
		}
		if (this.m_policeDepartmentAccumulation != 0 || this.m_noiseAccumulation != 0)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.PoliceDepartment, (float)this.m_policeDepartmentAccumulation, this.m_policeDepartmentRadius, buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.NoisePollution, (float)this.m_noiseAccumulation, this.m_noiseRadius);
		}
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			if (this.m_policeDepartmentAccumulation != 0)
			{
				Vector3 position = buildingData.m_position;
				position.y += this.m_info.m_size.y;
				Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
			}
			if (this.m_policeDepartmentAccumulation != 0 || this.m_noiseAccumulation != 0)
			{
				Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.PoliceDepartment, (float)(-(float)this.m_policeDepartmentAccumulation), this.m_policeDepartmentRadius, buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.NoisePollution, (float)(-(float)this.m_noiseAccumulation), this.m_noiseRadius);
			}
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
		offer.Building = buildingID;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.CriminalMove, offer);
		}
		else
		{
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(TransferManager.TransferReason.Crime, offer);
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(TransferManager.TransferReason.CriminalMove, offer);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == TransferManager.TransferReason.Crime || material == TransferManager.TransferReason.CriminalMove)
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, true, false))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == TransferManager.TransferReason.Crime)
		{
			amountDelta = Mathf.Max(amountDelta, 0);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void GetMaterialAmount(ushort buildingID, ref Building data, TransferManager.TransferReason material, out int amount, out int max)
	{
		if (material == TransferManager.TransferReason.Crime)
		{
			amount = 0;
			max = 1000000;
		}
		else
		{
			base.GetMaterialAmount(buildingID, ref data, material, out amount, out max);
		}
	}

	public override float GetCurrentRange(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return (float)num * this.m_policeDepartmentRadius * 0.01f;
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
		workPlaceCount += this.m_workPlaceCount0 + this.m_workPlaceCount1 + this.m_workPlaceCount2 + this.m_workPlaceCount3;
		base.GetWorkBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveWorkerCount, ref totalWorkerCount);
		base.HandleWorkPlaces(buildingID, ref buildingData, this.m_workPlaceCount0, this.m_workPlaceCount1, this.m_workPlaceCount2, this.m_workPlaceCount3, ref behaviour, aliveWorkerCount, totalWorkerCount);
		visitPlaceCount += this.m_jailCapacity;
		base.GetVisitBehaviour(buildingID, ref buildingData, ref behaviour, ref aliveVisitorCount, ref totalVisitorCount);
		behaviour.m_crimeAccumulation = 0;
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			District[] expr_62_cp_0 = instance.m_districts.m_buffer;
			byte expr_62_cp_1 = district;
			expr_62_cp_0[(int)expr_62_cp_1].m_servicePoliciesEffect = (expr_62_cp_0[(int)expr_62_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.RecreationalUse);
			int num = this.GetMaintenanceCost() / 100;
			num = (finalProductionRate * num + 666) / 667;
			if (num != 0)
			{
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num, this.m_info.m_class);
			}
		}
		int num2 = productionRate * this.m_policeDepartmentAccumulation / 100;
		if (num2 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.PoliceDepartment, num2, buildingData.m_position, this.m_policeDepartmentRadius);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		int num3 = finalProductionRate * this.m_noiseAccumulation / 100;
		if (num3 != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num3, buildingData.m_position, this.m_noiseRadius);
		}
		int num4 = this.m_sentenceWeeks;
		if ((servicePolicies & DistrictPolicies.Services.DoubleSentences) != DistrictPolicies.Services.None)
		{
			District[] expr_137_cp_0 = instance.m_districts.m_buffer;
			byte expr_137_cp_1 = district;
			expr_137_cp_0[(int)expr_137_cp_1].m_servicePoliciesEffect = (expr_137_cp_0[(int)expr_137_cp_1].m_servicePoliciesEffect | DistrictPolicies.Services.DoubleSentences);
			num4 <<= 1;
		}
		int num5 = 1000000 / Mathf.Max(1, num4 * 16);
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		uint num6 = buildingData.m_citizenUnits;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		while (num6 != 0u)
		{
			uint nextUnit = instance2.m_units.m_buffer[(int)((UIntPtr)num6)].m_nextUnit;
			if ((ushort)(instance2.m_units.m_buffer[(int)((UIntPtr)num6)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance2.m_units.m_buffer[(int)((UIntPtr)num6)].GetCitizen(i);
					if (citizen != 0u)
					{
						if (!instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Dead && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
						{
							if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(1000000u) < num5)
							{
								instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].Arrested = false;
								ushort instance3 = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
								if (instance3 != 0)
								{
									instance2.ReleaseCitizenInstance(instance3);
								}
								ushort homeBuilding = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_homeBuilding;
								if (homeBuilding != 0)
								{
									CitizenInfo citizenInfo = instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].GetCitizenInfo(citizen);
									HumanAI humanAI = citizenInfo.m_citizenAI as HumanAI;
									if (humanAI != null)
									{
										Citizen[] expr_310_cp_0 = instance2.m_citizens.m_buffer;
										UIntPtr expr_310_cp_1 = (UIntPtr)citizen;
										expr_310_cp_0[(int)expr_310_cp_1].m_flags = (expr_310_cp_0[(int)expr_310_cp_1].m_flags & ~Citizen.Flags.Evacuating);
										humanAI.StartMoving(citizen, ref instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)], buildingID, homeBuilding);
									}
								}
								if (instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation != Citizen.Location.Visit && instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_visitBuilding == buildingID)
								{
									instance2.m_citizens.m_buffer[(int)((UIntPtr)citizen)].SetVisitplace(citizen, 0, 0u);
								}
							}
							num8++;
						}
						num7++;
					}
				}
			}
			num6 = nextUnit;
			if (++num9 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount + totalVisitorCount);
		if (this.m_jailCapacity != 0)
		{
			District[] expr_41D_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_41D_cp_0_cp_1 = district;
			expr_41D_cp_0_cp_0[(int)expr_41D_cp_0_cp_1].m_productionData.m_tempCriminalAmount = expr_41D_cp_0_cp_0[(int)expr_41D_cp_0_cp_1].m_productionData.m_tempCriminalAmount + (uint)num8;
			District[] expr_441_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_441_cp_0_cp_1 = district;
			expr_441_cp_0_cp_0[(int)expr_441_cp_0_cp_1].m_productionData.m_tempCriminalCapacity = expr_441_cp_0_cp_0[(int)expr_441_cp_0_cp_1].m_productionData.m_tempCriminalCapacity + (uint)this.m_jailCapacity;
		}
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		int num13 = 0;
		int num14 = 0;
		int num15 = 0;
		int num16 = 0;
		int num17 = 0;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.CriminalMove, ref num10, ref num11, ref num12, ref num13);
			num11 = Mathf.Max(0, Mathf.Min(this.m_jailCapacity - num8, num11));
			District[] expr_4C1_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_4C1_cp_0_cp_1 = district;
			expr_4C1_cp_0_cp_0[(int)expr_4C1_cp_0_cp_1].m_productionData.m_tempCriminalAmount = expr_4C1_cp_0_cp_0[(int)expr_4C1_cp_0_cp_1].m_productionData.m_tempCriminalAmount + (uint)num11;
		}
		else
		{
			base.CalculateOwnVehicles(buildingID, ref buildingData, TransferManager.TransferReason.Crime, ref num10, ref num11, ref num12, ref num13);
			base.CalculateGuestVehicles(buildingID, ref buildingData, TransferManager.TransferReason.CriminalMove, ref num14, ref num15, ref num16, ref num17);
		}
		int num18 = (finalProductionRate * this.m_policeCarCount + 99) / 100;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			if (num10 < num18 && num12 + num7 <= this.m_jailCapacity - 20)
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 2 - num10;
				offer.Building = buildingID;
				offer.Position = buildingData.m_position;
				offer.Amount = 1;
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.CriminalMove, offer);
			}
		}
		else
		{
			if (num10 < num18)
			{
				TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
				offer2.Priority = 2 - num10;
				offer2.Building = buildingID;
				offer2.Position = buildingData.m_position;
				offer2.Amount = 1;
				offer2.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer(TransferManager.TransferReason.Crime, offer2);
			}
			if (num8 - num16 > 0)
			{
				TransferManager.TransferOffer offer3 = default(TransferManager.TransferOffer);
				offer3.Priority = (num8 - num16) * 8 / Mathf.Max(1, this.m_jailCapacity);
				offer3.Building = buildingID;
				offer3.Position = buildingData.m_position;
				offer3.Amount = 1;
				offer3.Active = false;
				Singleton<TransferManager>.get_instance().AddOutgoingOffer(TransferManager.TransferReason.CriminalMove, offer3);
			}
		}
	}

	protected override bool CanEvacuate()
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override void GetPollutionAccumulation(out int ground, out int noise)
	{
		ground = 0;
		noise = this.m_noiseAccumulation;
	}

	public override string GetLocalizedTooltip()
	{
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
			{
				this.GetWaterConsumption() * 16
			}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
			{
				this.GetElectricityConsumption() * 16
			});
			return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
			{
				LocaleFormatter.Info1,
				text,
				LocaleFormatter.Info2,
				LocaleFormatter.FormatGeneric("AIINFO_PRISONCAR_COUNT", new object[]
				{
					this.m_policeCarCount
				})
			}));
		}
		string text2 = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Info1,
			text2,
			LocaleFormatter.Info2,
			LocaleFormatter.FormatGeneric("AIINFO_POLICECAR_COUNT", new object[]
			{
				this.m_policeCarCount
			})
		}));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		int num3 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			if ((ushort)(instance.m_units.m_buffer[(int)((UIntPtr)num)].m_flags & CitizenUnit.Flags.Visit) != 0)
			{
				for (int i = 0; i < 5; i++)
				{
					uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
					if (citizen != 0u && instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].CurrentLocation == Citizen.Location.Visit)
					{
						num3++;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		int productionRate = PlayerBuildingAI.GetProductionRate(100, budget);
		int num4 = (productionRate * this.m_policeCarCount + 99) / 100;
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.CriminalMove, ref num5, ref num6, ref num7, ref num8);
		}
		else
		{
			base.CalculateOwnVehicles(buildingID, ref data, TransferManager.TransferReason.Crime, ref num5, ref num6, ref num7, ref num8);
		}
		string text;
		if (this.m_info.m_class.m_level >= ItemClass.Level.Level4)
		{
			text = LocaleFormatter.FormatGeneric("AIINFO_PRISON_CRIMINALS", new object[]
			{
				num3,
				this.m_jailCapacity
			}) + Environment.NewLine;
			text += LocaleFormatter.FormatGeneric("AIINFO_PRISON_CARS", new object[]
			{
				num5,
				num4
			});
		}
		else
		{
			text = LocaleFormatter.FormatGeneric("AIINFO_POLICESTATION_CRIMINALS", new object[]
			{
				num3,
				this.m_jailCapacity
			}) + Environment.NewLine;
			text += LocaleFormatter.FormatGeneric("AIINFO_POLICE_CARS", new object[]
			{
				num5,
				num4
			});
		}
		return text;
	}

	public override bool RequireRoadAccess()
	{
		return true;
	}

	[CustomizableProperty("Uneducated Workers", "Workers", 0)]
	public int m_workPlaceCount0 = 4;

	[CustomizableProperty("Educated Workers", "Workers", 1)]
	public int m_workPlaceCount1 = 16;

	[CustomizableProperty("Well Educated Workers", "Workers", 2)]
	public int m_workPlaceCount2 = 16;

	[CustomizableProperty("Highly Educated Workers", "Workers", 3)]
	public int m_workPlaceCount3 = 4;

	[CustomizableProperty("Police Car Count")]
	public int m_policeCarCount = 10;

	[CustomizableProperty("Jail Capacity")]
	public int m_jailCapacity = 20;

	[CustomizableProperty("Average Sentence Length")]
	public int m_sentenceWeeks = 15;

	[CustomizableProperty("Police Department Accumulation")]
	public int m_policeDepartmentAccumulation = 100;

	[CustomizableProperty("Police Department Radius")]
	public float m_policeDepartmentRadius = 500f;

	[CustomizableProperty("Noise Accumulation")]
	public int m_noiseAccumulation;

	[CustomizableProperty("Noise Radius")]
	public float m_noiseRadius = 200f;
}
