﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class HelicopterAI : VehicleAI
{
	public override Matrix4x4 CalculateBodyMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Vector3 swayPosition)
	{
		Quaternion quaternion = Quaternion.Euler(swayPosition.z * 57.29578f, 0f, swayPosition.x * -57.29578f);
		Matrix4x4 result = default(Matrix4x4);
		result.SetTRS(position, rotation * quaternion, scale);
		return result;
	}

	public override Matrix4x4 CalculateTyreMatrix(Vehicle.Flags flags, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale, ref Matrix4x4 bodyMatrix)
	{
		return Matrix4x4.get_identity();
	}

	public override float GetAudioAcceleration(ref Vehicle.Frame frame1, ref Vehicle.Frame frame2, float t)
	{
		return Mathf.Lerp(frame1.m_swayVelocity.y, frame2.m_swayVelocity.y, t);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if ((data.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			data.m_frame0.m_swayVelocity.y = 5f;
			data.m_frame1.m_swayVelocity.y = 5f;
			data.m_frame2.m_swayVelocity.y = 5f;
			data.m_frame3.m_swayVelocity.y = 5f;
		}
		else
		{
			data.m_frame0.m_swayVelocity.y = 0f;
			data.m_frame1.m_swayVelocity.y = 0f;
			data.m_frame2.m_swayVelocity.y = 0f;
			data.m_frame3.m_swayVelocity.y = 0f;
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingSpace) != (Vehicle.Flags)0)
		{
			this.TrySpawn(vehicleID, ref data);
		}
		this.SimulationStep(vehicleID, ref data, vehicleID, ref data, 0);
		if (data.m_leadingVehicle == 0 && data.m_trailingVehicle != 0)
		{
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_trailingVehicle;
			int num2 = 0;
			while (num != 0)
			{
				ushort trailingVehicle = instance.m_vehicles.m_buffer[(int)num].m_trailingVehicle;
				VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
				info.m_vehicleAI.SimulationStep(num, ref instance.m_vehicles.m_buffer[(int)num], vehicleID, ref data, 0);
				num = trailingVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if ((data.m_flags & (Vehicle.Flags.Spawned | Vehicle.Flags.WaitingSpace)) == (Vehicle.Flags)0 || data.m_blockCounter == 255)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		Vector3 vector = frameData.m_rotation * Vector3.get_forward();
		frameData.m_position += frameData.m_velocity * 0.5f;
		frameData.m_swayPosition += frameData.m_swayVelocity * 0.5f;
		float num = -Mathf.Atan2(vector.x, vector.z);
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		float num2 = this.m_info.m_acceleration;
		float num3 = frameData.m_velocity.get_magnitude();
		Vector4 targetPos = this.GetTargetPos(vehicleID, ref vehicleData);
		Vector3 vector2 = targetPos - frameData.m_position;
		float num4 = VectorUtils.LengthSqrXZ(vector2);
		float num5 = Mathf.Max((num3 + num2) * (0.5f + 0.5f * (num3 + num2) / (num2 * 0.9f)), 20f);
		float num6 = Mathf.Max(num3 + num2, 5f);
		float num7 = num6 * num6;
		int num8 = 0;
		if (num4 < num7 && (leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0)
		{
			this.UpdateBuildingTargetPositions(vehicleID, ref vehicleData, frameData.m_position, leaderID, ref leaderData, ref num8, num7);
			targetPos = this.GetTargetPos(vehicleID, ref vehicleData);
			vector2 = targetPos - frameData.m_position;
		}
		vector2.y += targetPos.w;
		vector2 = Vector3.ClampMagnitude(vector2, num5);
		Vector3 endPosition = frameData.m_position + vector2;
		Vehicle.Flags flags = leaderData.m_flags;
		if ((flags & Vehicle.Flags.TakingOff) != (Vehicle.Flags)0)
		{
			frameData.m_swayVelocity.y = Mathf.Min(frameData.m_swayVelocity.y + 0.5f, 10f);
			vector2.y = this.GetFlightHeight(vehicleID, frameData.m_position, endPosition) - frameData.m_position.y;
			if (frameData.m_velocity.y > 0.1f)
			{
				flags |= Vehicle.Flags.Flying;
			}
			if (vector2.y < 10f)
			{
				flags &= ~Vehicle.Flags.TakingOff;
				flags |= Vehicle.Flags.Flying;
			}
		}
		else if ((flags & Vehicle.Flags.Landing) != (Vehicle.Flags)0)
		{
			frameData.m_swayVelocity.y = Mathf.Min(frameData.m_swayVelocity.y + 0.5f, 10f);
			if (targetPos.w != 0f)
			{
				if ((flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
				{
					vector2 = Vector3.get_zero();
				}
			}
			else if (vector2.y > -0.1f && vector2.get_sqrMagnitude() <= 1f)
			{
				flags &= ~Vehicle.Flags.Landing;
				flags &= ~Vehicle.Flags.Flying;
			}
		}
		else if ((flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			frameData.m_swayVelocity.y = Mathf.Min(frameData.m_swayVelocity.y + 0.5f, 10f);
			vector2.y = this.GetFlightHeight(vehicleID, frameData.m_position, endPosition) - frameData.m_position.y;
			if (vector2.get_sqrMagnitude() < num5 * num5 * 0.5f)
			{
				flags |= Vehicle.Flags.Landing;
			}
		}
		else
		{
			frameData.m_swayVelocity.y = Mathf.Max(frameData.m_swayVelocity.y - 0.5f, 0f);
			if (vector2.get_sqrMagnitude() >= 1f && (leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0)
			{
				vector2.y = this.GetFlightHeight(vehicleID, frameData.m_position, frameData.m_position) - frameData.m_position.y;
				flags |= Vehicle.Flags.TakingOff;
			}
		}
		leaderData.m_flags = flags;
		float num9;
		if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 || (leaderData.m_flags & Vehicle.Flags.Flying) == (Vehicle.Flags)0 || VectorUtils.LengthSqrXZ(vector2) < 1f)
		{
			num9 = 0f;
		}
		else
		{
			float num10 = -Mathf.Atan2(vector2.x, vector2.z);
			num9 = Mathf.DeltaAngle(num * 57.29578f, num10 * 57.29578f) * 0.0174532924f;
		}
		Quaternion quaternion = Quaternion.Inverse(frameData.m_rotation);
		vector2 = quaternion * vector2;
		Vector3 vector3 = quaternion * frameData.m_velocity;
		if (vector3.z < 0f)
		{
			num3 = -num3;
		}
		Vector3 vector4 = Vector3.get_zero();
		Vector3 vector5 = Vector3.get_forward();
		float magnitude = vector2.get_magnitude();
		float num11 = this.m_info.m_maxSpeed;
		bool flag = false;
		if (magnitude > 1f)
		{
			vector5 = vector2 / magnitude;
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
			{
				num11 = 0f;
			}
			num11 = Mathf.Min(num11, HelicopterAI.CalculateMaxSpeed(magnitude, 0f, num2 * 0.9f));
			if ((flags & Vehicle.Flags.TakingOff) != (Vehicle.Flags)0)
			{
				num2 *= Mathf.Clamp01((frameData.m_swayVelocity.y - 6f) * 0.25f);
			}
			num11 *= 1f - Mathf.Abs(vector5.y) * 0.5f;
			if ((leaderData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0 && Mathf.Abs(num11) < 0.1f)
			{
				flag = true;
			}
		}
		else if (((Mathf.Abs(num3) < 0.1f && (flags & (Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing)) == (Vehicle.Flags)0 && frameData.m_swayVelocity.y < 0.1f) || targetPos.w != 0f) && (flags & Vehicle.Flags.WaitingTarget) == (Vehicle.Flags)0 && this.ArriveAtDestination(leaderID, ref leaderData))
		{
			leaderData.Unspawn(leaderID);
			return;
		}
		if (flag)
		{
			leaderData.m_blockCounter = (byte)Mathf.Min((int)(leaderData.m_blockCounter + 1), 255);
		}
		else
		{
			leaderData.m_blockCounter = 0;
		}
		if (num9 > frameData.m_angleVelocity)
		{
			frameData.m_angleVelocity = Mathf.Min(0.2f, Mathf.Min(frameData.m_angleVelocity * 0.9f + num2 * 0.2f, num9));
		}
		else
		{
			frameData.m_angleVelocity = Mathf.Max(-0.2f, Mathf.Max(frameData.m_angleVelocity * 0.9f - num2 * 0.2f, num9));
		}
		vector2 = Vector3.ClampMagnitude(vector2 * 0.5f, num11);
		Vector3 vector6 = Vector3.ClampMagnitude(vector2 - vector3, num2);
		vector4 = vector3 + vector6;
		vector5 = frameData.m_rotation * vector5;
		frameData.m_velocity = frameData.m_rotation * vector4;
		frameData.m_position += frameData.m_velocity * 0.5f;
		num += frameData.m_angleVelocity * 0.5f;
		frameData.m_rotation = Quaternion.AngleAxis(num * 57.29578f, Vector3.get_down());
		if ((leaderData.m_flags & Vehicle.Flags.Flying) != (Vehicle.Flags)0)
		{
			Vector3 vector7 = vector4 - vector3;
			frameData.m_swayVelocity.x = frameData.m_swayVelocity.x * 0.5f - vector7.x * 0.5f - frameData.m_swayPosition.x * 0.5f;
			frameData.m_swayVelocity.z = frameData.m_swayVelocity.z * 0.5f - vector7.z * 0.5f - frameData.m_swayPosition.z * 0.5f;
			frameData.m_swayPosition.x = frameData.m_swayPosition.x + frameData.m_swayVelocity.x * 0.5f;
			frameData.m_swayPosition.y = 0f;
			frameData.m_swayPosition.z = frameData.m_swayPosition.z + frameData.m_swayVelocity.z * 0.5f;
			frameData.m_steerAngle = 0f;
			frameData.m_travelDistance += frameData.m_swayVelocity.y;
		}
		else
		{
			frameData.m_swayVelocity.x = 0f;
			frameData.m_swayVelocity.z = 0f;
			frameData.m_swayPosition = Vector3.get_zero();
			frameData.m_steerAngle = 0f;
			frameData.m_travelDistance += frameData.m_swayVelocity.y;
		}
		frameData.m_lightIntensity.x = 5f;
		frameData.m_lightIntensity.y = 5f;
		frameData.m_lightIntensity.z = 5f;
		frameData.m_lightIntensity.w = frameData.m_swayVelocity.y;
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	protected virtual Vector4 GetTargetPos(ushort vehicleID, ref Vehicle vehicleData)
	{
		return vehicleData.m_targetPos0;
	}

	protected virtual float GetFlightHeight(ushort vehicleID, Vector3 startPosition, Vector3 endPosition)
	{
		Randomizer randomizer;
		randomizer..ctor((int)vehicleID);
		float num = Singleton<TerrainManager>.get_instance().SampleDetailHeight((startPosition + endPosition) * 0.5f);
		float num2 = Singleton<TerrainManager>.get_instance().SampleDetailHeight(endPosition);
		float num3 = Mathf.Max(num, num2) + 50f;
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		Segment2 segment;
		segment..ctor(VectorUtils.XZ(startPosition), VectorUtils.XZ(endPosition));
		Vector2 vector = segment.Min();
		Vector2 vector2 = segment.Max();
		int num4 = Mathf.Max((int)((vector.x - 72f) / 64f + 135f), 0);
		int num5 = Mathf.Max((int)((vector.y - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Min((int)((vector2.x + 72f) / 64f + 135f), 269);
		int num7 = Mathf.Min((int)((vector2.y + 72f) / 64f + 135f), 269);
		for (int i = num5; i <= num7; i++)
		{
			for (int j = num4; j <= num6; j++)
			{
				ushort num8 = instance.m_buildingGrid[i * 270 + j];
				int num9 = 0;
				while (num8 != 0)
				{
					BuildingInfo buildingInfo;
					int num10;
					int num11;
					instance.m_buildings.m_buffer[(int)num8].GetInfoWidthLength(out buildingInfo, out num10, out num11);
					Vector3 position = instance.m_buildings.m_buffer[(int)num8].m_position;
					float num12 = (float)(num10 * num10 + num11 * num11) * 16f;
					float num13;
					if (segment.DistanceSqr(VectorUtils.XZ(position), ref num13) < num12)
					{
						float num14 = buildingInfo.m_collisionHeight;
						Building.Flags flags = instance.m_buildings.m_buffer[(int)num8].m_flags;
						if ((flags & Building.Flags.Collapsed) != Building.Flags.None)
						{
							BuildingInfoBase collapsedInfo = buildingInfo.m_collapsedInfo;
							if (collapsedInfo != null)
							{
								num14 = collapsedInfo.m_generatedInfo.m_size.y;
							}
						}
						else if ((flags & Building.Flags.Upgrading) != Building.Flags.None)
						{
							BuildingInfo upgradeInfo = buildingInfo.m_buildingAI.GetUpgradeInfo(num8, ref instance.m_buildings.m_buffer[(int)num8]);
							if (upgradeInfo != null)
							{
								num14 = Mathf.Max(num14, upgradeInfo.m_collisionHeight);
							}
						}
						num3 = Mathf.Max(num3, position.y + num14);
					}
					num8 = instance.m_buildings.m_buffer[(int)num8].m_nextGridBuilding;
					if (++num9 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num3 + (float)randomizer.Int32(10, 100);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 10f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if (leaderData.m_targetBuilding != 0)
		{
			Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)leaderData.m_targetBuilding].m_position;
			vehicleData.SetTargetPos(index++, this.CalculateTargetPoint(refPos, position, minSqrDistance, 0f));
			return;
		}
	}

	protected Vector4 CalculateTargetPoint(Vector3 refPos, Vector3 targetPos, float maxSqrDistance, float height)
	{
		Vector4 result = targetPos;
		result.w = height;
		return result;
	}

	private static float CalculateMaxSpeed(float targetDistance, float targetSpeed, float maxBraking)
	{
		float num = 0.5f * maxBraking;
		float num2 = num + targetSpeed;
		return Mathf.Sqrt(num2 * num2 + 2f * targetDistance * maxBraking) - num;
	}

	public override bool CanSpawnAt(Vector3 pos)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		int num = Mathf.Max((int)((pos.x - 50f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((pos.z - 50f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((pos.x + 50f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((pos.z + 50f) / 320f + 27f), 53);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if (Vector3.SqrMagnitude(instance.m_vehicles.m_buffer[(int)num5].GetLastFramePosition() - pos) < 2500f)
					{
						return false;
					}
					num5 = instance.m_vehicles.m_buffer[(int)num5].m_nextGridVehicle;
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return true;
	}

	private static bool CheckOverlap(Segment3 segment, ushort ignoreVehicle)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		Vector3 vector = segment.Min();
		Vector3 vector2 = segment.Max();
		int num = Mathf.Max((int)((vector.x - 30f) / 320f + 27f), 0);
		int num2 = Mathf.Max((int)((vector.z - 30f) / 320f + 27f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 30f) / 320f + 27f), 53);
		int num4 = Mathf.Min((int)((vector2.z + 30f) / 320f + 27f), 53);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_vehicleGrid2[i * 54 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					num5 = HelicopterAI.CheckOverlap(segment, ignoreVehicle, num5, ref instance.m_vehicles.m_buffer[(int)num5], ref result);
					if (++num6 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private static ushort CheckOverlap(Segment3 segment, ushort ignoreVehicle, ushort otherID, ref Vehicle otherData, ref bool overlap)
	{
		float num;
		float num2;
		if ((ignoreVehicle == 0 || (otherID != ignoreVehicle && otherData.m_leadingVehicle != ignoreVehicle && otherData.m_trailingVehicle != ignoreVehicle)) && segment.DistanceSqr(otherData.m_segment, ref num, ref num2) < 100f)
		{
			overlap = true;
		}
		return otherData.m_nextGridVehicle;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos)
	{
		return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos, 0f);
	}

	protected bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, float height)
	{
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingPath;
		vehicleData.m_targetPos0 = endPos;
		vehicleData.m_targetPos0.w = height;
		vehicleData.m_targetPos1 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos2 = vehicleData.m_targetPos0;
		vehicleData.m_targetPos3 = vehicleData.m_targetPos0;
		this.TrySpawn(vehicleID, ref vehicleData);
		return true;
	}

	public override bool TrySpawn(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
		{
			return true;
		}
		if (HelicopterAI.CheckOverlap(vehicleData.m_segment, 0))
		{
			vehicleData.m_flags |= Vehicle.Flags.WaitingSpace;
			return false;
		}
		vehicleData.Spawn(vehicleID);
		vehicleData.m_flags &= ~Vehicle.Flags.WaitingSpace;
		return true;
	}

	public override bool VerticalTrailers()
	{
		return true;
	}
}
