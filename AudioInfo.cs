﻿using System;
using ColossalFramework.Math;
using UnityEngine;

public class AudioInfo : ScriptableObject
{
	public AudioClip ObtainClip()
	{
		return this.m_clip;
	}

	public float GetLength()
	{
		return this.m_clip.get_length();
	}

	public void ReleaseClip()
	{
	}

	public AudioInfo GetVariation(ref Randomizer r)
	{
		if (this.m_variations != null && this.m_variations.Length != 0)
		{
			int num = r.Int32(100u);
			for (int i = 0; i < this.m_variations.Length; i++)
			{
				num -= this.m_variations[i].m_probability;
				if (num < 0)
				{
					return this.m_variations[i].m_sound.GetVariation(ref r);
				}
			}
		}
		return this;
	}

	public AudioClip m_clip;

	public float m_volume = 1f;

	public float m_pitch = 1f;

	public float m_fadeLength = 0.1f;

	public bool m_loop;

	public bool m_is3D;

	public bool m_randomTime;

	public AudioInfo.Variation[] m_variations;

	[Serializable]
	public struct Variation
	{
		public AudioInfo m_sound;

		public int m_probability;
	}
}
