﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class DayNightFogEffect : MonoBehaviour
{
	public Material material
	{
		get
		{
			return this.m_FogMaterial;
		}
	}

	private void OnEnable()
	{
		DayNightFogEffect.ID_FrustumCornersWS = Shader.PropertyToID("_FrustumCornersWS");
		DayNightFogEffect.ID_CameraWS = Shader.PropertyToID("_CameraWS");
		if (!this.CheckSupport())
		{
			return;
		}
		this.m_Camera = base.GetComponent<Camera>();
		this.m_CameraTransform = this.m_Camera.get_transform();
		Camera expr_4D = this.m_Camera;
		expr_4D.set_depthTextureMode(expr_4D.get_depthTextureMode() | 1);
		this.m_FogMaterial = new Material(Shader.Find("Hidden/DayNight/Fog"));
		this.m_FogMaterial.set_hideFlags(52);
	}

	private void OnDisable()
	{
		if (this.m_FogMaterial)
		{
			Object.DestroyImmediate(this.m_FogMaterial);
			this.m_FogMaterial = null;
		}
	}

	protected bool CheckSupport()
	{
		if (!SystemInfo.get_supportsImageEffects())
		{
			base.set_enabled(false);
			return false;
		}
		if (!SystemInfo.SupportsRenderTextureFormat(1))
		{
			base.set_enabled(false);
			return false;
		}
		return true;
	}

	private void SetupFrustumPlanes()
	{
		float nearClipPlane = this.m_Camera.get_nearClipPlane();
		float farClipPlane = this.m_Camera.get_farClipPlane();
		float fieldOfView = this.m_Camera.get_fieldOfView();
		float aspect = this.m_Camera.get_aspect();
		Matrix4x4 identity = Matrix4x4.get_identity();
		float num = fieldOfView * 0.5f;
		Vector3 vector = this.m_CameraTransform.get_right() * nearClipPlane * Mathf.Tan(num * 0.0174532924f) * aspect;
		Vector3 vector2 = this.m_CameraTransform.get_up() * nearClipPlane * Mathf.Tan(num * 0.0174532924f);
		Vector3 vector3 = this.m_CameraTransform.get_forward() * nearClipPlane - vector + vector2;
		float num2 = vector3.get_magnitude() * farClipPlane / nearClipPlane;
		vector3.Normalize();
		vector3 *= num2;
		Vector3 vector4 = this.m_CameraTransform.get_forward() * nearClipPlane + vector + vector2;
		vector4.Normalize();
		vector4 *= num2;
		Vector3 vector5 = this.m_CameraTransform.get_forward() * nearClipPlane + vector - vector2;
		vector5.Normalize();
		vector5 *= num2;
		Vector3 vector6 = this.m_CameraTransform.get_forward() * nearClipPlane - vector - vector2;
		vector6.Normalize();
		vector6 *= num2;
		identity.SetRow(0, vector3);
		identity.SetRow(1, vector4);
		identity.SetRow(2, vector5);
		identity.SetRow(3, vector6);
		this.m_FogMaterial.SetMatrix(DayNightFogEffect.ID_FrustumCornersWS, identity);
		this.m_FogMaterial.SetVector(DayNightFogEffect.ID_CameraWS, this.m_CameraTransform.get_position());
	}

	[ImageEffectOpaque]
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		this.SetupFrustumPlanes();
		DayNightFogEffect.GraphicsBlitInterpolatedIndex(source, destination, this.m_FogMaterial, 0);
	}

	private static void GraphicsBlitInterpolatedIndex(RenderTexture source, RenderTexture dest, Material mat, int pass)
	{
		RenderTexture.set_active(dest);
		mat.SetTexture("_MainTex", source);
		GL.PushMatrix();
		GL.LoadOrtho();
		mat.SetPass(pass);
		GL.Begin(7);
		GL.MultiTexCoord2(0, 0f, 0f);
		GL.Vertex3(0f, 0f, 3f);
		GL.MultiTexCoord2(0, 1f, 0f);
		GL.Vertex3(1f, 0f, 2f);
		GL.MultiTexCoord2(0, 1f, 1f);
		GL.Vertex3(1f, 1f, 1f);
		GL.MultiTexCoord2(0, 0f, 1f);
		GL.Vertex3(0f, 1f, 0f);
		GL.End();
		GL.PopMatrix();
	}

	private static int ID_FrustumCornersWS;

	private static int ID_CameraWS;

	private Material m_FogMaterial;

	private Camera m_Camera;

	private Transform m_CameraTransform;
}
