﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class LandscapingInfoPanel : ToolsModifierControl
{
	private void Start()
	{
		base.get_transform().set_position(ToolsModifierControl.infoViewsPanel.get_transform().get_position());
		this.m_DesiredPosition = base.get_component().get_relativePosition();
		this.m_ViewsPanelOffset = new Vector2(ToolsModifierControl.infoViewsPanel.get_component().get_size().x, 0f);
		ToolsModifierControl.infoViewsPanel.get_component().add_eventVisibilityChanged(delegate(UIComponent c, bool v)
		{
			if (v)
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition);
			}
			else
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition - this.m_ViewsPanelOffset);
			}
		});
		this.m_DirtMeter = base.Find<UIProgressBar>("DirtMeter");
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (ToolsModifierControl.infoViewsPanel.isVisible)
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition);
			}
			else
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition - this.m_ViewsPanelOffset);
			}
		}
	}

	private void Update()
	{
		if (base.get_component().get_isVisible() && base.get_component().get_isEnabled())
		{
			this.m_DirtMeter.set_value((float)Singleton<TerrainManager>.get_instance().DirtBuffer / 524288f);
		}
	}

	public void OnCloseButton()
	{
		UIView.get_library().Hide(base.GetType().Name);
	}

	private UIProgressBar m_DirtMeter;

	private Vector2 m_DesiredPosition;

	private Vector2 m_ViewsPanelOffset;
}
