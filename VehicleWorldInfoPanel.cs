﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public abstract class VehicleWorldInfoPanel : WorldInfoPanel
{
	protected int GetPercentage(int current, int max)
	{
		float num = (float)current / (float)max * 100f;
		int result = Mathf.RoundToInt(num);
		if (num < 1f && !Mathf.Approximately(num, 0f))
		{
			result = 1;
		}
		if (num > 99f && !Mathf.Approximately(num, 100f))
		{
			result = 99;
		}
		return result;
	}

	protected override void Start()
	{
		base.Start();
		this.m_NameField = base.Find<UITextField>("VehicleName");
		if (this.m_NameField != null)
		{
			this.m_NameField.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		}
		this.m_Status = base.Find<UILabel>("Status");
		this.m_Target = base.Find<UIButton>("Target");
		this.m_Target.add_eventClick(new MouseEventHandler(this.OnTargetClick));
		this.m_Type = base.Find<UILabel>("Type");
	}

	protected void OnTargetClick(UIComponent comp, UIMouseEventParameter p)
	{
		if (p.get_source() != null && ToolsModifierControl.cameraController != null)
		{
			ToolsModifierControl.cameraController.SetTarget((InstanceID)p.get_source().get_objectUserData(), this.m_WorldMousePosition, false);
		}
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.SetName(text));
	}

	[DebuggerHidden]
	private IEnumerator SetName(string newName)
	{
		VehicleWorldInfoPanel.<SetName>c__Iterator0 <SetName>c__Iterator = new VehicleWorldInfoPanel.<SetName>c__Iterator0();
		<SetName>c__Iterator.newName = newName;
		<SetName>c__Iterator.$this = this;
		return <SetName>c__Iterator;
	}

	private string GetName()
	{
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				return Singleton<VehicleManager>.get_instance().GetVehicleName(firstVehicle);
			}
		}
		else if (this.m_InstanceID.Type == InstanceType.ParkedVehicle && this.m_InstanceID.ParkedVehicle != 0)
		{
			ushort parkedVehicle = this.m_InstanceID.ParkedVehicle;
			return Singleton<VehicleManager>.get_instance().GetParkedVehicleName(parkedVehicle);
		}
		return string.Empty;
	}

	protected override void OnSetTarget()
	{
		this.m_NameField.set_text(this.GetName());
		this.UpdateBindings();
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		InstanceID instanceID = this.m_InstanceID;
		InstanceID instanceID2 = default(InstanceID);
		string text = null;
		if (this.m_InstanceID.Type == InstanceType.Vehicle && this.m_InstanceID.Vehicle != 0)
		{
			ushort vehicle = this.m_InstanceID.Vehicle;
			ushort firstVehicle = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)vehicle].GetFirstVehicle(vehicle);
			if (firstVehicle != 0)
			{
				VehicleInfo info = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle].Info;
				text = info.m_vehicleAI.GetLocalizedStatus(firstVehicle, ref Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)firstVehicle], out instanceID2);
				this.m_Type.set_text(Singleton<VehicleManager>.get_instance().GetDefaultVehicleName(firstVehicle));
				instanceID.Vehicle = firstVehicle;
			}
		}
		else if (this.m_InstanceID.Type == InstanceType.ParkedVehicle && this.m_InstanceID.ParkedVehicle != 0)
		{
			ushort parkedVehicle = this.m_InstanceID.ParkedVehicle;
			VehicleParked vehicleParked = Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)parkedVehicle];
			text = vehicleParked.Info.m_vehicleAI.GetLocalizedStatus(parkedVehicle, ref Singleton<VehicleManager>.get_instance().m_parkedVehicles.m_buffer[(int)parkedVehicle], out instanceID2);
			this.m_Type.set_text(Singleton<VehicleManager>.get_instance().GetDefaultParkedVehicleName(parkedVehicle));
		}
		this.m_Status.get_parent().set_isVisible(!string.IsNullOrEmpty(text));
		this.m_Status.set_text(text);
		if (instanceID2.Building != 0)
		{
			this.m_Target.set_objectUserData(instanceID2);
			this.m_Target.set_text(Singleton<BuildingManager>.get_instance().GetBuildingName(instanceID2.Building, instanceID));
			base.ShortenTextToFitParent(this.m_Target);
			this.m_Target.set_isVisible(true);
			this.m_Target.set_isEnabled((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)instanceID2.Building].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.None);
		}
		else if (instanceID2.Citizen != 0u)
		{
			this.m_Target.set_objectUserData(instanceID2);
			this.m_Target.set_text(Singleton<CitizenManager>.get_instance().GetCitizenName(instanceID2.Citizen));
			base.ShortenTextToFitParent(this.m_Target);
			this.m_Target.set_isVisible(true);
			this.m_Target.set_isEnabled(true);
		}
		else
		{
			this.m_Target.set_isVisible(false);
		}
	}

	protected UITextField m_NameField;

	protected UILabel m_Status;

	protected UIButton m_Target;

	protected UILabel m_Type;
}
