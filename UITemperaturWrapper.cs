﻿using System;
using ColossalFramework;
using UnityEngine;

public class UITemperaturWrapper : UIWrapper<float>
{
	public UITemperaturWrapper(float def) : base(def)
	{
	}

	public override void Check(float newVal)
	{
		int num = this.temperatureUnit;
		if (num == 1)
		{
			newVal = newVal * 1.8f + 32f;
		}
		if (!Mathf.Approximately(this.m_Value, newVal))
		{
			this.m_Value = newVal;
			this.m_String = StringUtils.SafeFormat((num != 0) ? "{0:0.0}°F" : "{0:0.0}°C", newVal);
		}
	}

	private const string kFormatCelsius = "{0:0.0}°C";

	private const string kFormatFahrenheit = "{0:0.0}°F";

	private SavedInt temperatureUnit = new SavedInt(Settings.temperatureUnit, Settings.gameSettingsFile, DefaultSettings.temperatureUnit, true);
}
