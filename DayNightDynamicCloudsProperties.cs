﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class DayNightDynamicCloudsProperties : MonoBehaviour
{
	private void OnEnable()
	{
		this.ID_CloudPosition = Shader.PropertyToID("_CloudPosition");
		this.ID_Coverage = Shader.PropertyToID("_Coverage");
		this.ID_NoiseTiling = Shader.PropertyToID("_NoiseTiling");
		this.ID_PlanetCenterKm = Shader.PropertyToID("_PlanetCenterKm");
		this.ID_PlanetTangent = Shader.PropertyToID("_PlanetTangent");
		this.ID_PlanetBiTangent = Shader.PropertyToID("_PlanetBiTangent");
		this.ID_PlanetNormal = Shader.PropertyToID("_PlanetNormal");
		this.ID_SunDirection = Shader.PropertyToID("_SunDirection");
		this.ID_ColorFromSky = Shader.PropertyToID("_ColorFromSky");
		this.ID_ColorFromLight = Shader.PropertyToID("_ColorFromLight");
		this.m_DayNightProperties = base.GetComponent<DayNightProperties>();
		if (this.m_DayNightProperties == null || this.m_CloudMaterial == null)
		{
			base.set_enabled(false);
		}
		else
		{
			this.InitSkydomeMesh();
		}
	}

	private void OnDisable()
	{
		if (this.m_SkydomeMesh != null)
		{
			Object.DestroyImmediate(this.m_SkydomeMesh);
			this.m_SkydomeMesh = null;
		}
	}

	private void InitSkydomeMesh()
	{
		if (this.m_SkydomeBaseMesh != null)
		{
			Mesh mesh = new Mesh();
			Vector3[] vertices = this.m_SkydomeBaseMesh.get_vertices();
			mesh.set_vertices(vertices);
			mesh.set_triangles(this.m_SkydomeBaseMesh.get_triangles());
			mesh.set_normals(this.m_SkydomeBaseMesh.get_normals());
			mesh.set_uv(this.m_SkydomeBaseMesh.get_uv());
			mesh.set_uv2(this.m_SkydomeBaseMesh.get_uv2());
			mesh.set_colors(this.m_SkydomeBaseMesh.get_colors());
			mesh.set_bounds(new Bounds(Vector3.get_zero(), Vector3.get_one() * 2E+09f));
			mesh.set_hideFlags(52);
			mesh.set_name("SkydomeMesh");
			this.m_SkydomeMesh = mesh;
		}
		else
		{
			base.set_enabled(false);
		}
	}

	private void Update()
	{
		float num = 0f;
		float num2 = 0f;
		if (Singleton<WeatherManager>.get_exists())
		{
			this.m_Coverage = this.m_MaxCoverage * Singleton<WeatherManager>.get_instance().SampleCloudCoverage(Vector3.get_zero(), false);
			num = Singleton<WeatherManager>.get_instance().m_windDirection * 0.0174532924f;
			num2 = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		}
		if (this.m_Coverage > 0f)
		{
			this.m_PlanetBiTangent = Vector3.Cross(Vector3.get_right(), this.m_PlanetNormal).get_normalized();
			this.m_PlanetTangent = Vector3.Cross(this.m_PlanetNormal, this.m_PlanetBiTangent);
			Vector2 vector = this.m_WindForce * new Vector2(Mathf.Sin(num), Mathf.Cos(num));
			Vector2 vector2;
			vector2..ctor(this.m_CloudPosition.x, this.m_CloudPosition.y);
			Vector2 vector3;
			vector3..ctor(this.m_CloudPosition.z, this.m_CloudPosition.w);
			vector2 += this.m_NoiseTiling * vector * num2;
			vector3 += this.m_EvolutionSpeed * this.m_NoiseTiling * vector * num2;
			this.m_CloudPosition = new Vector4(vector2.x, vector2.y, vector3.x, vector3.y);
			bool useLinearSpace = this.m_DayNightProperties.m_UseLinearSpace;
			float num3 = Mathf.Max(Mathf.Pow(0.25f, (!useLinearSpace) ? 1f : 1.5f), this.m_DayNightProperties.DayTime);
			num3 *= Mathf.Sqrt(this.m_DayNightProperties.m_Exposure);
			this.m_CloudMaterial.SetVector(this.ID_ColorFromLight, (!useLinearSpace) ? (this.m_DayNightProperties.currentLightColor * num3) : (this.m_DayNightProperties.currentLightColor.get_linear() * num3));
			this.m_CloudMaterial.SetVector(this.ID_ColorFromSky, (!useLinearSpace) ? (this.m_DayNightProperties.currentSkyColor * num3) : (this.m_DayNightProperties.currentSkyColor.get_linear() * num3));
			this.m_CloudMaterial.SetVector(this.ID_CloudPosition, this.m_CloudPosition);
			this.m_CloudMaterial.SetFloat(this.ID_Coverage, this.m_Coverage);
			this.m_CloudMaterial.SetFloat(this.ID_NoiseTiling, this.m_NoiseTiling);
			this.m_CloudMaterial.SetVector(this.ID_PlanetCenterKm, this.m_PlanetCenterKm);
			this.m_CloudMaterial.SetVector(this.ID_PlanetNormal, this.m_PlanetNormal);
			this.m_CloudMaterial.SetVector(this.ID_PlanetTangent, this.m_PlanetTangent);
			this.m_CloudMaterial.SetVector(this.ID_PlanetBiTangent, this.m_PlanetBiTangent);
			this.m_CloudMaterial.SetVector(this.ID_SunDirection, this.m_DayNightProperties.sunDir);
			Graphics.DrawMesh(this.m_SkydomeMesh, new Vector3(0f, this.m_HorizonOffset, 0f), Quaternion.get_identity(), this.m_CloudMaterial, this.m_CloudLayer);
		}
	}

	protected const float EARTH_RADIUS = 6400f;

	private int ID_CloudPosition;

	private int ID_Coverage;

	private int ID_NoiseTiling;

	private int ID_PlanetCenterKm;

	private int ID_PlanetTangent;

	private int ID_PlanetBiTangent;

	private int ID_PlanetNormal;

	private int ID_SunDirection;

	private int ID_ColorFromSky;

	private int ID_ColorFromLight;

	public int m_CloudLayer = 29;

	public float m_WindForce;

	public float m_Coverage;

	public float m_MaxCoverage = 1f;

	public float m_NoiseTiling = 0.006f;

	public float m_EvolutionSpeed = 8f;

	public float m_HorizonOffset;

	private const float m_NightBrightness = 0.25f;

	public Vector3 m_PlanetCenterKm = -6400f * Vector3.get_up();

	public Vector3 m_PlanetNormal = Vector3.get_up();

	private Vector3 m_PlanetTangent = Vector3.get_zero();

	private Vector3 m_PlanetBiTangent = Vector3.get_zero();

	public Material m_CloudMaterial;

	public Mesh m_SkydomeBaseMesh;

	private DayNightProperties m_DayNightProperties;

	private Vector4 m_CloudPosition;

	private Mesh m_SkydomeMesh;
}
