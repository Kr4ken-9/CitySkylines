﻿using System;

public class WeatherDisasterAI : DisasterAI
{
	public virtual uint GetEstimatedActivationFrame(ushort disasterID, ref DisasterData data)
	{
		return data.m_activationFrame;
	}
}
