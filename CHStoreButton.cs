﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class CHStoreButton : UICustomControl
{
	private void Awake()
	{
	}

	public UITextureAtlas m_Atlas;

	[Header("TGP"), UISprite("m_Atlas")]
	public string m_ButtonTGPCH;

	[UISprite("m_Atlas")]
	public string m_ButtonHoveredTGPCH;

	[UISprite("m_Atlas")]
	public string m_ButtonPressedTGPCH;

	[UISprite("m_Atlas")]
	public string m_ButtonDisabledTGPCH;

	[Header("QQ"), UISprite("m_Atlas")]
	public string m_ButtonQQCH;

	[UISprite("m_Atlas")]
	public string m_ButtonHoveredQQCH;

	[UISprite("m_Atlas")]
	public string m_ButtonPressedQQCH;

	[UISprite("m_Atlas")]
	public string m_ButtonDisabledQQCH;

	public bool m_Foreground;
}
