﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class NetTool : ToolBase
{
	public NetInfo Prefab
	{
		get
		{
			return this.m_prefab;
		}
		set
		{
			if (this.m_prefab != value)
			{
				this.m_prefab = value;
				this.m_controlPointCount = 0;
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_bulldozerTool = base.GetComponent<BulldozeTool>();
		this.m_controlPoints = new NetTool.ControlPoint[3];
		this.m_cachedControlPoints = new NetTool.ControlPoint[3];
		this.m_closeSegments = new ushort[16];
		this.m_cacheLock = new object();
		this.m_upgradedSegments = new HashSet<ushort>();
		this.m_tempUpgraded = new FastList<ushort>();
		this.m_helperLineTimer = new Dictionary<int, NetTool.HelperLineTimer>();
		this.m_buildElevationUp = new SavedInputKey(Settings.buildElevationUp, Settings.gameSettingsFile, DefaultSettings.buildElevationUp, true);
		this.m_buildElevationDown = new SavedInputKey(Settings.buildElevationDown, Settings.gameSettingsFile, DefaultSettings.buildElevationDown, true);
	}

	protected override void OnToolGUI(Event e)
	{
		bool isInsideUI = this.m_toolController.IsInsideUI;
		if (e.get_type() == null)
		{
			if (!isInsideUI)
			{
				if (e.get_button() == 0)
				{
					if (this.m_cachedErrors == ToolBase.ToolErrors.None)
					{
						Singleton<SimulationManager>.get_instance().AddAction<bool>(this.CreateNode(false));
					}
					else
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.CreateFailed());
					}
				}
				else if (e.get_button() == 1)
				{
					if (this.m_mode == NetTool.Mode.Upgrade)
					{
						Singleton<SimulationManager>.get_instance().AddAction<bool>(this.CreateNode(true));
					}
					else
					{
						Singleton<SimulationManager>.get_instance().AddAction(this.CancelNode());
					}
				}
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0 || e.get_button() == 1)
			{
				Singleton<SimulationManager>.get_instance().AddAction(this.CancelUpgrading());
			}
		}
		else if (this.m_buildElevationUp.IsPressed(e))
		{
			Singleton<SimulationManager>.get_instance().AddAction<bool>(this.ChangeElevation(1));
		}
		else if (this.m_buildElevationDown.IsPressed(e))
		{
			Singleton<SimulationManager>.get_instance().AddAction<bool>(this.ChangeElevation(-1));
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_controlPointCount = 0;
		this.m_lengthChanging = false;
		this.m_lengthTimer = 0f;
		this.m_constructionCost = 0;
		this.m_productionRate = 0;
		this.m_buildErrors = ToolBase.ToolErrors.Pending;
		this.m_cachedErrors = ToolBase.ToolErrors.Pending;
		this.m_upgrading = false;
		this.m_switchingDir = false;
		while (!Monitor.TryEnter(this.m_upgradedSegments, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_repairing = false;
			this.m_upgradedSegments.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_upgradedSegments);
		}
		this.m_toolController.ClearColliding();
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Singleton<TransportManager>.get_instance().TunnelsVisible = false;
		Singleton<TerrainManager>.get_instance().RenderZones = false;
		Singleton<TerrainManager>.get_instance().RenderTopography = false;
		Singleton<NetManager>.get_instance().RenderDirectionArrows = false;
		this.m_controlPointCount = 0;
		this.m_lengthChanging = false;
		this.m_lengthTimer = 0f;
		this.m_constructionCost = 0;
		this.m_productionRate = 0;
		this.m_buildErrors = ToolBase.ToolErrors.Pending;
		this.m_cachedErrors = ToolBase.ToolErrors.Pending;
		this.m_upgrading = false;
		this.m_switchingDir = false;
		while (!Monitor.TryEnter(this.m_upgradedSegments, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_repairing = false;
			this.m_upgradedSegments.Clear();
		}
		finally
		{
			Monitor.Exit(this.m_upgradedSegments);
		}
		this.m_mouseRayValid = false;
		this.m_lastAngle = -1;
		this.m_helperLineTimer.Clear();
	}

	public override void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
		base.RenderGeometry(cameraInfo);
		NetInfo prefab = this.m_prefab;
		if (prefab != null && !this.m_toolController.IsInsideUI && Cursor.get_visible() && (this.m_cachedErrors & (ToolBase.ToolErrors.RaycastFailed | ToolBase.ToolErrors.Pending)) == ToolBase.ToolErrors.None)
		{
			if (this.m_mode != NetTool.Mode.Upgrade || this.m_cachedControlPointCount >= 2)
			{
				if (this.m_mode == NetTool.Mode.Straight && this.m_cachedControlPointCount < 1)
				{
					this.m_toolController.RenderCollidingNotifications(cameraInfo, 0, 0);
					NetTool.ControlPoint controlPoint = this.m_cachedControlPoints[0];
					controlPoint.m_direction = Vector3.get_forward();
					ushort num;
					ushort num2;
					int num3;
					int num4;
					NetTool.CreateNode(prefab, controlPoint, controlPoint, controlPoint, NetTool.m_nodePositionsMain, 0, false, true, true, false, false, false, 0, out num, out num2, out num3, out num4);
				}
				else if ((this.m_mode == NetTool.Mode.Curved || this.m_mode == NetTool.Mode.Freeform) && this.m_cachedControlPointCount < 2 && (this.m_cachedControlPointCount == 0 || (this.m_cachedControlPoints[1].m_node == 0 && this.m_cachedControlPoints[1].m_segment == 0)))
				{
					this.m_toolController.RenderCollidingNotifications(cameraInfo, 0, 0);
					NetTool.ControlPoint controlPoint2 = this.m_cachedControlPoints[0];
					controlPoint2.m_direction = Vector3.get_forward();
					ushort num5;
					ushort num6;
					int num7;
					int num8;
					NetTool.CreateNode(prefab, controlPoint2, controlPoint2, controlPoint2, NetTool.m_nodePositionsMain, 0, false, true, true, false, false, false, 0, out num5, out num6, out num7, out num8);
				}
				else
				{
					this.m_toolController.RenderCollidingNotifications(cameraInfo, 0, 0);
					NetTool.ControlPoint startPoint;
					NetTool.ControlPoint endPoint;
					NetTool.ControlPoint middlePoint;
					if (this.m_cachedControlPointCount == 1)
					{
						startPoint = this.m_cachedControlPoints[0];
						endPoint = this.m_cachedControlPoints[1];
						middlePoint = this.m_cachedControlPoints[1];
						middlePoint.m_node = 0;
						middlePoint.m_segment = 0;
						middlePoint.m_position = (this.m_cachedControlPoints[0].m_position + this.m_cachedControlPoints[1].m_position) * 0.5f;
						middlePoint.m_elevation = (this.m_cachedControlPoints[0].m_elevation + this.m_cachedControlPoints[1].m_elevation) * 0.5f;
					}
					else
					{
						startPoint = this.m_cachedControlPoints[0];
						middlePoint = this.m_cachedControlPoints[1];
						endPoint = this.m_cachedControlPoints[2];
					}
					if (middlePoint.m_direction == Vector3.get_zero())
					{
						startPoint.m_direction = Vector3.get_forward();
						middlePoint.m_direction = Vector3.get_forward();
						endPoint.m_direction = Vector3.get_forward();
					}
					ushort num9;
					ushort num10;
					int num11;
					int num12;
					NetTool.CreateNode(prefab, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsMain, 1000, false, true, true, false, false, false, 0, out num9, out num10, out num11, out num12);
					if (NetTool.GetSecondaryControlPoints(prefab, ref startPoint, ref middlePoint, ref endPoint))
					{
						NetTool.CreateNode(prefab, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsMain, 1000, false, true, true, false, false, false, 0, out num9, out num10, out num11, out num12);
					}
				}
			}
		}
	}

	public override void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.RenderOverlay(cameraInfo);
		NetInfo prefab = this.m_prefab;
		if (prefab != null && !this.m_toolController.IsInsideUI && Cursor.get_visible() && this.m_mode == NetTool.Mode.Upgrade)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Color toolColor = base.GetToolColor(false, false);
			this.m_tempUpgraded.Clear();
			while (!Monitor.TryEnter(this.m_upgradedSegments, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				foreach (ushort current in this.m_upgradedSegments)
				{
					this.m_tempUpgraded.Add(current);
				}
			}
			finally
			{
				Monitor.Exit(this.m_upgradedSegments);
			}
			for (int i = 0; i < this.m_tempUpgraded.m_size; i++)
			{
				ushort num = this.m_tempUpgraded.m_buffer[i];
				NetTool.RenderOverlay(cameraInfo, ref instance.m_segments.m_buffer[(int)num], toolColor, toolColor);
			}
		}
		if (prefab != null && !this.m_toolController.IsInsideUI && Cursor.get_visible() && (this.m_cachedErrors & (ToolBase.ToolErrors.RaycastFailed | ToolBase.ToolErrors.Pending)) == ToolBase.ToolErrors.None)
		{
			if (this.m_mode != NetTool.Mode.Upgrade || this.m_cachedControlPointCount >= 2)
			{
				NetTool.ControlPoint startPoint;
				NetTool.ControlPoint middlePoint;
				NetTool.ControlPoint endPoint;
				if (this.m_cachedControlPointCount >= 2)
				{
					startPoint = this.m_cachedControlPoints[0];
					middlePoint = this.m_cachedControlPoints[1];
					endPoint = this.m_cachedControlPoints[this.m_cachedControlPointCount];
				}
				else if (this.m_mode == NetTool.Mode.Straight || this.m_cachedControlPoints[this.m_cachedControlPointCount].m_node != 0 || this.m_cachedControlPoints[this.m_cachedControlPointCount].m_segment != 0)
				{
					startPoint = this.m_cachedControlPoints[0];
					middlePoint = this.m_cachedControlPoints[this.m_cachedControlPointCount];
					endPoint = this.m_cachedControlPoints[this.m_cachedControlPointCount];
				}
				else
				{
					startPoint = this.m_cachedControlPoints[0];
					middlePoint = this.m_cachedControlPoints[0];
					endPoint = this.m_cachedControlPoints[0];
				}
				ushort num2 = middlePoint.m_segment;
				if (startPoint.m_segment == num2 || endPoint.m_segment == num2)
				{
					num2 = 0;
				}
				Color toolColor2 = base.GetToolColor(false, this.m_cachedErrors != ToolBase.ToolErrors.None);
				Color toolColor3 = base.GetToolColor(true, false);
				this.m_toolController.RenderColliding(cameraInfo, toolColor2, toolColor3, toolColor2, toolColor3, num2, 0);
				this.RenderOverlay(cameraInfo, prefab, toolColor2, startPoint, middlePoint, endPoint);
				if (NetTool.GetSecondaryControlPoints(prefab, ref startPoint, ref middlePoint, ref endPoint))
				{
					this.RenderOverlay(cameraInfo, prefab, toolColor2, startPoint, middlePoint, endPoint);
				}
				if ((this.m_snap & NetTool.Snapping.HelperLine) != NetTool.Snapping.None)
				{
					toolColor2 = base.GetToolColor(false, false);
					this.RenderHelperLines(cameraInfo, NetTool.m_helperLines, toolColor2);
				}
			}
		}
	}

	private bool GetClosestDirection(NetTool.ControlPoint startPoint, Vector3 startDirection, out Vector3 dir)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		float num = -2f;
		dir = Vector3.get_zero();
		if (startPoint.m_node != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)startPoint.m_node].GetSegment(i);
				if (segment != 0)
				{
					Vector3 direction = instance.m_segments.m_buffer[(int)segment].GetDirection(startPoint.m_node);
					float num2 = direction.x * startDirection.x + direction.z * startDirection.z;
					if (num2 > num)
					{
						num = num2;
						dir = direction;
					}
				}
			}
		}
		if (startPoint.m_segment != 0)
		{
			Vector3 vector;
			Vector3 vector2;
			instance.m_segments.m_buffer[(int)startPoint.m_segment].GetClosestPositionAndDirection(startPoint.m_position, out vector, out vector2);
			float num3 = vector2.x * startDirection.x + vector2.z * startDirection.z;
			if (num3 < 0f)
			{
				vector2 = -vector2;
				num3 = -num3;
			}
			if (num3 > num)
			{
				num = num3;
				dir = vector2;
			}
		}
		return num != -2f;
	}

	private void RenderAngleArc(RenderManager.CameraInfo cameraInfo, Color color, Vector3 pos, Vector3 dir1, Vector3 dir2)
	{
		Vector3 vector;
		vector..ctor(dir1.z, 0f, -dir1.x);
		Vector3 vector2;
		vector2..ctor(-dir2.z, 0f, dir2.x);
		if (Vector3.Dot(vector, dir2) < 0f)
		{
			vector = -vector;
			vector2 = -vector2;
		}
		Vector3 vector3 = VectorUtils.NormalizeXZ(vector + vector2);
		float num = Mathf.Acos(Mathf.Clamp(dir1.x * dir2.x + dir1.z * dir2.z, -1f, 1f));
		if (num > 1.57079637f)
		{
			this.RenderAngleImpl(cameraInfo, color, pos, dir1, vector3, num * 0.5f);
			this.RenderAngleImpl(cameraInfo, color, pos, vector3, dir2, num * 0.5f);
		}
		else
		{
			this.RenderAngleImpl(cameraInfo, color, pos, dir1, dir2, num);
		}
		int num2 = Mathf.RoundToInt(num * 57.29578f);
		string text = null;
		if (this.m_lastAngle != num2)
		{
			this.m_lastAngle = num2;
			text = StringUtils.SafeFormat("{0}°", num2);
		}
		float num3 = Vector3.Distance(cameraInfo.m_position, pos);
		base.ShowExtraInfo(true, text, pos + vector3 * (32f + num3 * 0.012f));
		this.m_angleShown = true;
	}

	private void RenderAngleImpl(RenderManager.CameraInfo cameraInfo, Color color, Vector3 pos, Vector3 dir1, Vector3 dir2, float angle)
	{
		float num = 32f;
		Bezier3 bezier;
		bezier.a = pos + dir1 * num;
		bezier.d = pos + dir2 * num;
		Vector3 vector;
		vector..ctor(dir1.z, 0f, -dir1.x);
		Vector3 vector2;
		vector2..ctor(-dir2.z, 0f, dir2.x);
		if (Vector3.Dot(vector, dir2) < 0f)
		{
			vector = -vector;
			vector2 = -vector2;
		}
		float num2 = Mathf.Tan(angle * 0.5f) * num * 0.5522848f;
		bezier.b = bezier.a + vector * num2;
		bezier.c = bezier.d + vector2 * num2;
		ToolManager expr_DD_cp_0 = Singleton<ToolManager>.get_instance();
		expr_DD_cp_0.m_drawCallData.m_overlayCalls = expr_DD_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawBezier(cameraInfo, color, bezier, 0f, -100000f, -100000f, -1f, 1280f, false, true);
	}

	private void RenderHelperLines(RenderManager.CameraInfo cameraInfo, FastList<NetTool.HelperLine> lines, Color color)
	{
		NetTool.m_helperLineBuffer.Clear();
		while (!Monitor.TryEnter(lines, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = 0; i < lines.m_size; i++)
			{
				NetTool.HelperBufferLine item;
				item.m_line = lines.m_buffer[i];
				if (this.m_helperLineTimer.TryGetValue(item.m_line.m_id, out item.m_timer))
				{
					item.m_timer.m_nodeTimer = Mathf.Min(item.m_timer.m_nodeTimer + Time.get_deltaTime() * 3f, 1f);
					if (item.m_line.m_distance < 20f)
					{
						item.m_timer.m_lineTimer = Mathf.Min(item.m_timer.m_lineTimer + Time.get_deltaTime() * 3f, 1f);
					}
					else
					{
						item.m_timer.m_lineTimer = Mathf.Max(item.m_timer.m_lineTimer - Time.get_deltaTime() * 3f, -1f);
					}
				}
				else
				{
					item.m_timer.m_nodeTimer = -1f;
					item.m_timer.m_lineTimer = -1f;
				}
				NetTool.m_helperLineBuffer.Add(item);
			}
		}
		finally
		{
			Monitor.Exit(lines);
		}
		this.m_helperLineTimer.Clear();
		for (int j = 0; j < NetTool.m_helperLineBuffer.m_size; j++)
		{
			NetTool.HelperLine line = NetTool.m_helperLineBuffer.m_buffer[j].m_line;
			NetTool.HelperLineTimer timer = NetTool.m_helperLineBuffer.m_buffer[j].m_timer;
			Vector3 a = line.m_line.a;
			Vector3 vector = line.m_line.b - a;
			float num = VectorUtils.LengthXZ(vector);
			int num2 = Mathf.CeilToInt(num / 80f - 0.01f);
			Color color2 = color;
			float distance = line.m_distance;
			float nodeTimer = timer.m_nodeTimer;
			float lineTimer = timer.m_lineTimer;
			this.m_helperLineTimer[line.m_id] = timer;
			if (nodeTimer > 0f)
			{
				if (distance < 20f && lineTimer > 0f)
				{
					color2.a *= lineTimer;
					if (distance >= 0f)
					{
						float num3 = Mathf.Clamp01(distance / 20f);
						color2.a *= 0.5f - 0.5f * num3;
					}
					for (int k = 1; k <= num2; k++)
					{
						Segment3 segment;
						segment.a = a + vector * (((float)(k - 1) * 80f + 4f) / Mathf.Max(1f, num));
						segment.b = a + vector * (((float)k * 80f - 4f) / Mathf.Max(1f, num));
						ToolManager expr_2EC_cp_0 = Singleton<ToolManager>.get_instance();
						expr_2EC_cp_0.m_drawCallData.m_overlayCalls = expr_2EC_cp_0.m_drawCallData.m_overlayCalls + 1;
						Singleton<RenderManager>.get_instance().OverlayEffect.DrawSegment(cameraInfo, color2, segment, 0f, 8f, -1f, 1280f, false, true);
					}
				}
				else
				{
					num2 = 0;
				}
				for (int l = 0; l <= num2; l++)
				{
					Vector3 center = a + vector * ((float)l * 80f / Mathf.Max(1f, num));
					Color color3 = color;
					if (distance >= 0f)
					{
						if (l == 0)
						{
							float num4 = Mathf.Clamp01(distance / 200f);
							color3.a *= 0.5f - 0.5f * num4;
							color3.a *= nodeTimer;
						}
						else
						{
							color3 = color2;
						}
					}
					else if (l == 0)
					{
						color3.a *= nodeTimer;
					}
					else
					{
						color3.a *= lineTimer;
					}
					ToolManager expr_3FD_cp_0 = Singleton<ToolManager>.get_instance();
					expr_3FD_cp_0.m_drawCallData.m_overlayCalls = expr_3FD_cp_0.m_drawCallData.m_overlayCalls + 1;
					Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color3, center, 8f, -1f, 1280f, false, true);
				}
			}
		}
	}

	private void RenderOverlay(RenderManager.CameraInfo cameraInfo, NetInfo info, Color color, NetTool.ControlPoint startPoint, NetTool.ControlPoint middlePoint, NetTool.ControlPoint endPoint)
	{
		Vector3 position = middlePoint.m_position;
		if (middlePoint.m_segment != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)middlePoint.m_segment].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
		{
			info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)middlePoint.m_segment].Info;
		}
		BuildingInfo buildingInfo;
		Vector3 vector;
		Vector3 vector2;
		int num;
		info.m_netAI.CheckBuildPosition(false, false, true, this.m_mode != NetTool.Mode.Upgrade, ref startPoint, ref middlePoint, ref endPoint, out buildingInfo, out vector, out vector2, out num);
		bool flag = position != middlePoint.m_position;
		Bezier3 bezier;
		bezier.a = startPoint.m_position;
		bezier.d = endPoint.m_position;
		bool smoothStart = true;
		bool smoothEnd = true;
		if (this.m_mode == NetTool.Mode.Upgrade)
		{
			smoothStart = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startPoint.m_node].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None);
			smoothEnd = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endPoint.m_node].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None);
		}
		NetSegment.CalculateMiddlePoints(bezier.a, middlePoint.m_direction, bezier.d, -endPoint.m_direction, smoothStart, smoothEnd, out bezier.b, out bezier.c);
		Segment3 segment;
		bool flag2;
		Segment3 segment2;
		bool flag3;
		if ((this.m_mode == NetTool.Mode.Curved || this.m_mode == NetTool.Mode.Freeform) && this.m_cachedControlPointCount >= 2)
		{
			segment.a = startPoint.m_position;
			segment.b = middlePoint.m_position;
			flag2 = true;
			segment2.a = middlePoint.m_position;
			segment2.b = endPoint.m_position;
			flag3 = true;
		}
		else if ((this.m_mode == NetTool.Mode.Curved || this.m_mode == NetTool.Mode.Freeform) && this.m_cachedControlPointCount >= 1)
		{
			segment.a = startPoint.m_position;
			segment.b = this.m_cachedControlPoints[1].m_position;
			flag2 = true;
			segment2.a = new Vector3(-100000f, 0f, -100000f);
			segment2.b = new Vector3(-100000f, 0f, -100000f);
			flag3 = false;
		}
		else
		{
			segment.a = new Vector3(-100000f, 0f, -100000f);
			segment.b = new Vector3(-100000f, 0f, -100000f);
			flag2 = false;
			segment2.a = new Vector3(-100000f, 0f, -100000f);
			segment2.b = new Vector3(-100000f, 0f, -100000f);
			flag3 = false;
		}
		if (flag2 && flag3)
		{
			ToolManager expr_2CF_cp_0 = Singleton<ToolManager>.get_instance();
			expr_2CF_cp_0.m_drawCallData.m_overlayCalls = expr_2CF_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawSegment(cameraInfo, color, segment, segment2, info.m_halfWidth * 2f, 8f, -1f, 1280f, false, false);
		}
		else if (flag2)
		{
			ToolManager expr_324_cp_0 = Singleton<ToolManager>.get_instance();
			expr_324_cp_0.m_drawCallData.m_overlayCalls = expr_324_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawSegment(cameraInfo, color, segment, info.m_halfWidth * 2f, 8f, -1f, 1280f, false, false);
		}
		else if (flag3)
		{
			ToolManager expr_377_cp_0 = Singleton<ToolManager>.get_instance();
			expr_377_cp_0.m_drawCallData.m_overlayCalls = expr_377_cp_0.m_drawCallData.m_overlayCalls + 1;
			Singleton<RenderManager>.get_instance().OverlayEffect.DrawSegment(cameraInfo, color, segment2, info.m_halfWidth * 2f, 8f, -1f, 1280f, false, false);
		}
		ToolManager expr_3BE_cp_0 = Singleton<ToolManager>.get_instance();
		expr_3BE_cp_0.m_drawCallData.m_overlayCalls = expr_3BE_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawBezier(cameraInfo, color, bezier, info.m_halfWidth * 2f, -100000f, -100000f, -1f, 1280f, false, false);
		if (this.m_cachedErrors == ToolBase.ToolErrors.None && !flag && (this.m_snap & NetTool.Snapping.Length) != NetTool.Snapping.None)
		{
			float num2 = info.m_netAI.GetLengthSnap();
			if ((this.m_mode == NetTool.Mode.Straight || this.m_mode == NetTool.Mode.Curved) && num2 > 0.5f && this.m_cachedControlPointCount >= 1)
			{
				Vector3 position2 = this.m_cachedControlPoints[this.m_cachedControlPointCount].m_position;
				Vector3 position3 = this.m_cachedControlPoints[this.m_cachedControlPointCount - 1].m_position;
				Color color2 = color;
				color2.a *= 0.25f;
				num2 *= 10f;
				float num3;
				Vector3 vector3 = VectorUtils.NormalizeXZ(position2 - position3, ref num3);
				Vector3 vector4;
				vector4..ctor(vector3.z * (info.m_halfWidth + 48f), 0f, -vector3.x * (info.m_halfWidth + 48f));
				int num4 = Mathf.RoundToInt(num3 / num2 + 0.01f);
				if (num4 >= 1)
				{
					for (int i = 0; i <= num4; i++)
					{
						Vector3 vector5 = position3 + vector3 * (num2 * (float)i);
						Segment3 segment3;
						segment3..ctor(vector5 + vector4, vector5 - vector4);
						ToolManager expr_54F_cp_0 = Singleton<ToolManager>.get_instance();
						expr_54F_cp_0.m_drawCallData.m_overlayCalls = expr_54F_cp_0.m_drawCallData.m_overlayCalls + 1;
						Singleton<RenderManager>.get_instance().OverlayEffect.DrawSegment(cameraInfo, color2, segment3, 0f, 0f, -1f, 1280f, false, true);
					}
				}
			}
		}
		if (this.m_cachedErrors == ToolBase.ToolErrors.None)
		{
			float num5;
			bool flag4;
			Color color3;
			info.m_netAI.GetEffectRadius(out num5, out flag4, out color3);
			if (num5 > info.m_halfWidth)
			{
				if (Vector3.SqrMagnitude(bezier.d - bezier.a) >= 1f)
				{
					ToolManager expr_5EA_cp_0 = Singleton<ToolManager>.get_instance();
					expr_5EA_cp_0.m_drawCallData.m_overlayCalls = expr_5EA_cp_0.m_drawCallData.m_overlayCalls + 1;
					Singleton<RenderManager>.get_instance().OverlayEffect.DrawBezier(cameraInfo, color3, bezier, num5 * 2f, (!flag4) ? -100000f : num5, (!flag4) ? -100000f : num5, -1f, 1280f, false, true);
				}
				else
				{
					ToolManager expr_654_cp_0 = Singleton<ToolManager>.get_instance();
					expr_654_cp_0.m_drawCallData.m_overlayCalls = expr_654_cp_0.m_drawCallData.m_overlayCalls + 1;
					Singleton<RenderManager>.get_instance().OverlayEffect.DrawCircle(cameraInfo, color3, bezier.a, num5 * 2f, -1f, 1280f, false, true);
				}
			}
		}
		if (this.m_cachedErrors == ToolBase.ToolErrors.None && this.m_cachedControlPointCount == 1 && this.m_mode != NetTool.Mode.Upgrade && (this.m_snap & NetTool.Snapping.Angle) != NetTool.Snapping.None)
		{
			Vector3 direction = this.m_cachedControlPoints[1].m_direction;
			if (flag)
			{
				direction = middlePoint.m_direction;
			}
			Vector3 dir;
			if (this.GetClosestDirection(this.m_cachedControlPoints[0], direction, out dir))
			{
				this.RenderAngleArc(cameraInfo, color, this.m_cachedControlPoints[0].m_position, dir, direction);
			}
		}
	}

	public static void RenderBulldozeNotification(RenderManager.CameraInfo cameraInfo, ref NetSegment segment)
	{
		NetInfo info = segment.Info;
		if (info == null)
		{
			return;
		}
		if ((segment.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None && !info.m_overlayVisible)
		{
			return;
		}
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
		if ((privateServiceIndex != -1 || info.m_autoRemove) && (segment.m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
		{
			Vector3 position = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)segment.m_startNode].m_position;
			Vector3 position2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)segment.m_endNode].m_position;
			Vector3 position3 = (position + position2) * 0.5f;
			position3.y += info.m_maxHeight;
			NotificationEvent.RenderInstance(cameraInfo, NotificationEvent.Type.Bulldozer, position3, 1f, 1f);
		}
	}

	public static void RenderOverlay(RenderManager.CameraInfo cameraInfo, ref NetSegment segment, Color importantColor, Color nonImportantColor)
	{
		NetInfo info = segment.Info;
		if (info == null)
		{
			return;
		}
		if ((segment.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None && !info.m_overlayVisible)
		{
			return;
		}
		Bezier3 bezier;
		bezier.a = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)segment.m_startNode].m_position;
		bezier.d = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)segment.m_endNode].m_position;
		NetSegment.CalculateMiddlePoints(bezier.a, segment.m_startDirection, bezier.d, segment.m_endDirection, false, false, out bezier.b, out bezier.c);
		bool flag = false;
		bool flag2 = false;
		int privateServiceIndex = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
		Color color = ((privateServiceIndex == -1 && !info.m_autoRemove) || (segment.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None) ? importantColor : nonImportantColor;
		ToolManager expr_F3_cp_0 = Singleton<ToolManager>.get_instance();
		expr_F3_cp_0.m_drawCallData.m_overlayCalls = expr_F3_cp_0.m_drawCallData.m_overlayCalls + 1;
		Singleton<RenderManager>.get_instance().OverlayEffect.DrawBezier(cameraInfo, color, bezier, info.m_halfWidth * 2f, (!flag) ? -100000f : info.m_halfWidth, (!flag2) ? -100000f : info.m_halfWidth, -1f, 1280f, false, false);
	}

	public static void CheckOverlayAlpha(ref NetSegment segment, ref float alpha)
	{
		NetInfo info = segment.Info;
		if (info == null)
		{
			return;
		}
		if ((segment.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None && !info.m_overlayVisible)
		{
			return;
		}
		NetTool.CheckOverlayAlpha(info, ref alpha);
	}

	public static void CheckOverlayAlpha(NetInfo info, ref float alpha)
	{
		if (info == null)
		{
			return;
		}
		alpha = Mathf.Min(alpha, 2f / Mathf.Max(1f, Mathf.Sqrt(info.m_halfWidth)));
	}

	protected override void OnToolUpdate()
	{
		NetInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		while (!Monitor.TryEnter(this.m_cacheLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			for (int i = 0; i < this.m_controlPoints.Length; i++)
			{
				this.m_cachedControlPoints[i] = this.m_controlPoints[i];
			}
			this.m_cachedControlPointCount = this.m_controlPointCount;
			this.m_cachedErrors = this.m_buildErrors;
		}
		finally
		{
			Monitor.Exit(this.m_cacheLock);
		}
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible() && (this.m_cachedErrors & (ToolBase.ToolErrors.RaycastFailed | ToolBase.ToolErrors.Pending)) == ToolBase.ToolErrors.None)
		{
			Vector3 position;
			if (this.m_mode == NetTool.Mode.Upgrade && this.m_cachedControlPointCount >= 2)
			{
				position = this.m_cachedControlPoints[1].m_position;
			}
			else
			{
				position = this.m_cachedControlPoints[this.m_cachedControlPointCount].m_position;
			}
			int num = this.m_constructionCost;
			bool flag = num < 0;
			if (flag)
			{
				num = -num;
			}
			if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None && num != 0 && (this.m_mode != NetTool.Mode.Upgrade || !this.m_upgrading))
			{
				string text;
				if (this.m_mode == NetTool.Mode.Upgrade)
				{
					text = StringUtils.SafeFormat(Locale.Get((!flag) ? "TOOL_UPGRADE_COST" : "TOOL_REPAIR_COST"), num / 100);
				}
				else
				{
					text = StringUtils.SafeFormat(Locale.Get("TOOL_CONSTRUCTION_COST"), num / 100);
				}
				string constructionInfo = prefab.m_netAI.GetConstructionInfo(this.m_productionRate);
				if (constructionInfo != null)
				{
					text = text + "\n" + constructionInfo;
				}
				base.ShowToolInfo(true, text, position);
			}
			else
			{
				base.ShowToolInfo(true, null, position);
			}
		}
		else
		{
			base.ShowToolInfo(false, null, Vector3.get_zero());
		}
		if (!this.m_angleShown)
		{
			base.ShowExtraInfo(false, null, Vector3.get_zero());
		}
		this.m_angleShown = false;
		CursorInfo cursorInfo = null;
		if (this.m_mode == NetTool.Mode.Upgrade)
		{
			cursorInfo = prefab.m_upgradeCursor;
		}
		if (cursorInfo == null)
		{
			cursorInfo = prefab.m_placementCursor;
		}
		if (cursorInfo == null && this.m_mode == NetTool.Mode.Upgrade)
		{
			cursorInfo = this.m_upgradeCursor;
		}
		if (cursorInfo == null)
		{
			cursorInfo = this.m_placementCursor;
		}
		base.ToolCursor = cursorInfo;
	}

	protected override void OnToolLateUpdate()
	{
		NetInfo prefab = this.m_prefab;
		if (prefab == null)
		{
			return;
		}
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		if (this.m_lengthTimer > 0f)
		{
			this.m_lengthTimer = Mathf.Max(0f, this.m_lengthTimer - Time.get_deltaTime());
		}
		InfoManager.InfoMode infoMode;
		InfoManager.SubInfoMode subMode;
		prefab.m_netAI.GetPlacementInfoMode(out infoMode, out subMode, this.GetElevation(prefab));
		ToolBase.ForceInfoMode(infoMode, subMode);
		if (infoMode == InfoManager.InfoMode.None && (prefab.m_netAI.GetCollisionLayers() & ItemClass.Layer.MetroTunnels) != ItemClass.Layer.None)
		{
			Singleton<TransportManager>.get_instance().TunnelsVisible = true;
		}
		else
		{
			Singleton<TransportManager>.get_instance().TunnelsVisible = false;
		}
		if (prefab.m_netAI.ShowTerrainTopography())
		{
			Singleton<TerrainManager>.get_instance().RenderTopography = true;
			Singleton<TerrainManager>.get_instance().RenderZones = false;
			Singleton<NetManager>.get_instance().RenderDirectionArrows = false;
		}
		else
		{
			Singleton<TerrainManager>.get_instance().RenderTopography = false;
			Singleton<TerrainManager>.get_instance().RenderZones = true;
			Singleton<NetManager>.get_instance().RenderDirectionArrows = ((prefab.m_vehicleTypes & ~VehicleInfo.VehicleType.Bicycle) != VehicleInfo.VehicleType.None);
		}
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
		if (this.m_lengthChanging)
		{
			this.m_lengthTimer = 0.1f;
			this.m_lengthChanging = false;
		}
		if (this.m_lengthTimer != 0f)
		{
			NetProperties properties = Singleton<NetManager>.get_instance().m_properties;
			if (properties != null && properties.m_drawSound != null)
			{
				Singleton<AudioManager>.get_instance().DefaultGroup.AddPlayer(0, properties.m_drawSound, 1f);
			}
		}
	}

	private static bool GetSecondaryControlPoints(NetInfo info, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint)
	{
		ushort num = middlePoint.m_segment;
		if (startPoint.m_segment == num || endPoint.m_segment == num)
		{
			num = 0;
		}
		ushort num2 = 0;
		if (num != 0)
		{
			num2 = DefaultTool.FindSecondarySegment(num);
		}
		if (num2 != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			startPoint.m_node = instance.m_segments.m_buffer[(int)num2].m_startNode;
			startPoint.m_segment = 0;
			startPoint.m_position = instance.m_nodes.m_buffer[(int)startPoint.m_node].m_position;
			startPoint.m_direction = instance.m_segments.m_buffer[(int)num2].m_startDirection;
			startPoint.m_elevation = (float)instance.m_nodes.m_buffer[(int)startPoint.m_node].m_elevation;
			if (instance.m_nodes.m_buffer[(int)startPoint.m_node].Info.m_netAI.IsUnderground())
			{
				startPoint.m_elevation = -startPoint.m_elevation;
			}
			startPoint.m_outside = ((instance.m_nodes.m_buffer[(int)startPoint.m_node].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None);
			endPoint.m_node = instance.m_segments.m_buffer[(int)num2].m_endNode;
			endPoint.m_segment = 0;
			endPoint.m_position = instance.m_nodes.m_buffer[(int)endPoint.m_node].m_position;
			endPoint.m_direction = -instance.m_segments.m_buffer[(int)num2].m_endDirection;
			endPoint.m_elevation = (float)instance.m_nodes.m_buffer[(int)endPoint.m_node].m_elevation;
			if (instance.m_nodes.m_buffer[(int)endPoint.m_node].Info.m_netAI.IsUnderground())
			{
				endPoint.m_elevation = -endPoint.m_elevation;
			}
			endPoint.m_outside = ((instance.m_nodes.m_buffer[(int)endPoint.m_node].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None);
			if ((instance.m_segments.m_buffer[(int)num2].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				info = instance.m_segments.m_buffer[(int)num2].Info;
			}
			middlePoint.m_node = 0;
			middlePoint.m_segment = num2;
			middlePoint.m_position = startPoint.m_position + startPoint.m_direction * (info.GetMinNodeDistance() + 1f);
			middlePoint.m_direction = startPoint.m_direction;
			middlePoint.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, 0.5f);
			middlePoint.m_outside = false;
			return true;
		}
		return false;
	}

	private static void MoveMiddleNode(ref ushort node, ref Vector3 direction, Vector3 position)
	{
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node];
		uint buildIndex = netNode.m_buildIndex;
		ushort num = node;
		NetInfo info = netNode.Info;
		Vector3 vector = netNode.m_position - position;
		vector.y = 0f;
		if (vector.get_sqrMagnitude() < 2500f)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = netNode.GetSegment(i);
				if (segment != 0)
				{
					NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
					NetInfo info2 = netSegment.Info;
					Vector3 position2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_startNode].m_position;
					Vector3 position3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_endNode].m_position;
					bool flag = !NetSegment.IsStraight(position2, netSegment.m_startDirection, position3, netSegment.m_endDirection);
					bool flag2 = netSegment.m_startNode == node;
					ushort num2 = (!flag2) ? netSegment.m_startNode : netSegment.m_endNode;
					uint buildIndex2 = netSegment.m_buildIndex;
					NetNode netNode2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num2];
					vector = netNode2.m_position - position;
					vector.y = 0f;
					bool flag3 = vector.get_sqrMagnitude() >= 10000f;
					Vector3 vector2;
					Vector3 vector3;
					if (flag && flag3)
					{
						netSegment.GetClosestPositionAndDirection((position + netNode2.m_position) * 0.5f, out vector2, out vector3);
						if (flag2)
						{
							vector3 = -vector3;
						}
					}
					else
					{
						vector2 = NetTool.LerpPosition(position, netNode2.m_position, 0.5f, info.m_netAI.GetLengthSnap());
						vector3 = ((!flag2) ? netSegment.m_startDirection : netSegment.m_endDirection);
					}
					direction = vector3;
					string text = null;
					if ((netSegment.m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
					{
						InstanceID empty = InstanceID.Empty;
						empty.NetSegment = segment;
						text = Singleton<InstanceManager>.get_instance().GetName(empty);
					}
					bool flag4 = false;
					if ((Singleton<NetManager>.get_instance().m_adjustedSegments[segment >> 6] & 1uL << (int)segment) != 0uL)
					{
						flag4 = true;
					}
					Singleton<NetManager>.get_instance().ReleaseSegment(segment, true);
					Singleton<NetManager>.get_instance().ReleaseNode(node);
					if (flag3)
					{
						if (Singleton<NetManager>.get_instance().CreateNode(out node, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, vector2, buildIndex))
						{
							NetNode[] expr_2AF_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
							ushort expr_2AF_cp_1 = node;
							expr_2AF_cp_0[(int)expr_2AF_cp_1].m_flags = (expr_2AF_cp_0[(int)expr_2AF_cp_1].m_flags | (netNode.m_flags & (NetNode.Flags.Original | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.Heating | NetNode.Flags.Electricity)));
							if (info.m_netAI.IsUnderground())
							{
								Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = netNode.m_elevation;
								NetNode[] expr_310_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
								ushort expr_310_cp_1 = node;
								expr_310_cp_0[(int)expr_310_cp_1].m_flags = (expr_310_cp_0[(int)expr_310_cp_1].m_flags | NetNode.Flags.Underground);
							}
							else if (netNode.m_elevation > 0)
							{
								float num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(vector2);
								Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(vector2.y - num3), 1, 255);
							}
							else
							{
								Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = 0;
								NetNode[] expr_3AE_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
								ushort expr_3AE_cp_1 = node;
								expr_3AE_cp_0[(int)expr_3AE_cp_1].m_flags = (expr_3AE_cp_0[(int)expr_3AE_cp_1].m_flags | NetNode.Flags.OnGround);
							}
							if (flag2)
							{
								if (Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, info2, node, num2, -vector3, netSegment.m_endDirection, buildIndex2, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, (netSegment.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
								{
									Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
									NetSegment[] expr_439_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
									ushort expr_439_cp_1 = segment;
									expr_439_cp_0[(int)expr_439_cp_1].m_flags = (expr_439_cp_0[(int)expr_439_cp_1].m_flags | (netSegment.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Collapsed | NetSegment.Flags.WaitingPath | NetSegment.Flags.TrafficEnd | NetSegment.Flags.CrossingEnd | NetSegment.Flags.HeavyBan | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded | NetSegment.Flags.BikeBan | NetSegment.Flags.CarBan | NetSegment.Flags.YieldEnd)));
									Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_wetness = netSegment.m_wetness;
									Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_condition = netSegment.m_condition;
									Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_nameSeed = netSegment.m_nameSeed;
									if (text != null)
									{
										NetSegment[] expr_4D5_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
										ushort expr_4D5_cp_1 = segment;
										expr_4D5_cp_0[(int)expr_4D5_cp_1].m_flags = (expr_4D5_cp_0[(int)expr_4D5_cp_1].m_flags | NetSegment.Flags.CustomName);
										InstanceID empty2 = InstanceID.Empty;
										empty2.NetSegment = segment;
										Singleton<InstanceManager>.get_instance().SetName(empty2, text);
									}
									if (flag4)
									{
										Singleton<NetManager>.get_instance().m_adjustedSegments[segment >> 6] |= 1uL << (int)segment;
									}
								}
							}
							else if (Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, info2, num2, node, netSegment.m_startDirection, -vector3, buildIndex2, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, (netSegment.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
							{
								Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
								NetSegment[] expr_5A5_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
								ushort expr_5A5_cp_1 = segment;
								expr_5A5_cp_0[(int)expr_5A5_cp_1].m_flags = (expr_5A5_cp_0[(int)expr_5A5_cp_1].m_flags | (netSegment.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Collapsed | NetSegment.Flags.WaitingPath | NetSegment.Flags.TrafficStart | NetSegment.Flags.CrossingStart | NetSegment.Flags.HeavyBan | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded | NetSegment.Flags.BikeBan | NetSegment.Flags.CarBan | NetSegment.Flags.YieldStart)));
								Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_wetness = netSegment.m_wetness;
								Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_condition = netSegment.m_condition;
								if (text != null)
								{
									NetSegment[] expr_61F_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
									ushort expr_61F_cp_1 = segment;
									expr_61F_cp_0[(int)expr_61F_cp_1].m_flags = (expr_61F_cp_0[(int)expr_61F_cp_1].m_flags | NetSegment.Flags.CustomName);
									InstanceID empty3 = InstanceID.Empty;
									empty3.NetSegment = segment;
									Singleton<InstanceManager>.get_instance().SetName(empty3, text);
								}
								if (flag4)
								{
									Singleton<NetManager>.get_instance().m_adjustedSegments[segment >> 6] |= 1uL << (int)segment;
								}
							}
							info2.m_netAI.AfterSplitOrMove(node, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node], num, num);
						}
					}
					else
					{
						node = num2;
					}
					break;
				}
			}
		}
	}

	private static void MoveEndNode(ref ushort node, ref Vector3 direction, Vector3 position)
	{
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node];
		NetInfo info = netNode.Info;
		Vector3 vector = netNode.m_position - position;
		vector.y = 0f;
		float minNodeDistance = info.GetMinNodeDistance();
		if (vector.get_sqrMagnitude() < minNodeDistance * minNodeDistance)
		{
			Singleton<NetManager>.get_instance().ReleaseNode(node);
			node = 0;
		}
	}

	private static bool SplitSegment(ushort segment, out ushort node, Vector3 position)
	{
		NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
		NetInfo info = netSegment.Info;
		uint buildIndex = netSegment.m_buildIndex;
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_startNode];
		NetNode netNode2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_endNode];
		Vector3 vector;
		Vector3 vector2;
		netSegment.GetClosestPositionAndDirection(position, out vector, out vector2);
		string text = null;
		if ((netSegment.m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
		{
			InstanceID empty = InstanceID.Empty;
			empty.NetSegment = segment;
			text = Singleton<InstanceManager>.get_instance().GetName(empty);
		}
		bool flag = false;
		if ((Singleton<NetManager>.get_instance().m_adjustedSegments[segment >> 6] & 1uL << (int)segment) != 0uL)
		{
			flag = true;
		}
		Singleton<NetManager>.get_instance().ReleaseSegment(segment, true);
		bool flag2 = false;
		if ((netNode.m_flags & (NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == NetNode.Flags.Moveable)
		{
			if ((netNode.m_flags & NetNode.Flags.Middle) != NetNode.Flags.None)
			{
				NetTool.MoveMiddleNode(ref netSegment.m_startNode, ref netSegment.m_startDirection, position);
			}
			else if ((netNode.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				NetTool.MoveEndNode(ref netSegment.m_startNode, ref netSegment.m_startDirection, position);
			}
		}
		if ((netNode2.m_flags & (NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == NetNode.Flags.Moveable)
		{
			if ((netNode2.m_flags & NetNode.Flags.Middle) != NetNode.Flags.None)
			{
				NetTool.MoveMiddleNode(ref netSegment.m_endNode, ref netSegment.m_endDirection, position);
			}
			else if ((netNode2.m_flags & NetNode.Flags.End) != NetNode.Flags.None)
			{
				NetTool.MoveEndNode(ref netSegment.m_endNode, ref netSegment.m_endDirection, position);
			}
		}
		ushort num = 0;
		ushort num2 = 0;
		if (Singleton<NetManager>.get_instance().CreateNode(out node, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, position, buildIndex))
		{
			NetNode[] expr_1DE_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
			ushort expr_1DE_cp_1 = node;
			expr_1DE_cp_0[(int)expr_1DE_cp_1].m_flags = (expr_1DE_cp_0[(int)expr_1DE_cp_1].m_flags | (netNode.m_flags & netNode2.m_flags & (NetNode.Flags.Original | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.Heating | NetNode.Flags.Electricity)));
			if (info.m_netAI.IsUnderground())
			{
				Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = (netNode.m_elevation + netNode2.m_elevation) / 2;
				NetNode[] expr_252_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
				ushort expr_252_cp_1 = node;
				expr_252_cp_0[(int)expr_252_cp_1].m_flags = (expr_252_cp_0[(int)expr_252_cp_1].m_flags | NetNode.Flags.Underground);
			}
			else if (info.m_netAI.IsOverground())
			{
				float num3 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(position);
				Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(position.y - num3), 1, 255);
			}
			else
			{
				Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node].m_elevation = 0;
				NetNode[] expr_2F2_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
				ushort expr_2F2_cp_1 = node;
				expr_2F2_cp_0[(int)expr_2F2_cp_1].m_flags = (expr_2F2_cp_0[(int)expr_2F2_cp_1].m_flags | NetNode.Flags.OnGround);
			}
			if (netSegment.m_startNode != 0)
			{
				if (Singleton<NetManager>.get_instance().CreateSegment(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, netSegment.m_startNode, node, netSegment.m_startDirection, -vector2, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, (netSegment.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
				{
					Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
					NetSegment[] expr_385_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
					ushort expr_385_cp_1 = num;
					expr_385_cp_0[(int)expr_385_cp_1].m_flags = (expr_385_cp_0[(int)expr_385_cp_1].m_flags | (netSegment.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Collapsed | NetSegment.Flags.WaitingPath | NetSegment.Flags.TrafficStart | NetSegment.Flags.CrossingStart | NetSegment.Flags.HeavyBan | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded | NetSegment.Flags.BikeBan | NetSegment.Flags.CarBan | NetSegment.Flags.YieldStart)));
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_wetness = netSegment.m_wetness;
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_condition = netSegment.m_condition;
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_nameSeed = netSegment.m_nameSeed;
					if (text != null)
					{
						NetSegment[] expr_421_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
						ushort expr_421_cp_1 = num;
						expr_421_cp_0[(int)expr_421_cp_1].m_flags = (expr_421_cp_0[(int)expr_421_cp_1].m_flags | NetSegment.Flags.CustomName);
						InstanceID empty2 = InstanceID.Empty;
						empty2.NetSegment = num;
						Singleton<InstanceManager>.get_instance().SetName(empty2, text);
					}
					if (flag)
					{
						Singleton<NetManager>.get_instance().m_adjustedSegments[num >> 6] |= 1uL << (int)num;
					}
				}
				else
				{
					flag2 = true;
				}
			}
			if (netSegment.m_endNode != 0)
			{
				if (info.m_requireContinuous)
				{
					if (Singleton<NetManager>.get_instance().CreateSegment(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, node, netSegment.m_endNode, vector2, netSegment.m_endDirection, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, (netSegment.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None))
					{
						Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
						NetSegment[] expr_509_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
						ushort expr_509_cp_1 = num2;
						expr_509_cp_0[(int)expr_509_cp_1].m_flags = (expr_509_cp_0[(int)expr_509_cp_1].m_flags | (netSegment.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Collapsed | NetSegment.Flags.WaitingPath | NetSegment.Flags.TrafficEnd | NetSegment.Flags.CrossingEnd | NetSegment.Flags.HeavyBan | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded | NetSegment.Flags.BikeBan | NetSegment.Flags.CarBan | NetSegment.Flags.YieldEnd)));
						Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_wetness = netSegment.m_wetness;
						Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_condition = netSegment.m_condition;
						Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_nameSeed = netSegment.m_nameSeed;
						if (text != null)
						{
							NetSegment[] expr_5A5_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
							ushort expr_5A5_cp_1 = num2;
							expr_5A5_cp_0[(int)expr_5A5_cp_1].m_flags = (expr_5A5_cp_0[(int)expr_5A5_cp_1].m_flags | NetSegment.Flags.CustomName);
							InstanceID empty3 = InstanceID.Empty;
							empty3.NetSegment = num2;
							Singleton<InstanceManager>.get_instance().SetName(empty3, text);
						}
						if (flag)
						{
							Singleton<NetManager>.get_instance().m_adjustedSegments[num2 >> 6] |= 1uL << (int)num2;
						}
					}
					else
					{
						flag2 = true;
					}
				}
				else if (Singleton<NetManager>.get_instance().CreateSegment(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, info, netSegment.m_endNode, node, netSegment.m_endDirection, vector2, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, (netSegment.m_flags & NetSegment.Flags.Invert) == NetSegment.Flags.None))
				{
					Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
					NetSegment.Flags flags = netSegment.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Collapsed | NetSegment.Flags.WaitingPath | NetSegment.Flags.HeavyBan | NetSegment.Flags.Blocked | NetSegment.Flags.Flooded | NetSegment.Flags.BikeBan | NetSegment.Flags.CarBan);
					if ((netSegment.m_flags & NetSegment.Flags.CrossingEnd) != NetSegment.Flags.None)
					{
						flags |= NetSegment.Flags.CrossingStart;
					}
					if ((netSegment.m_flags & NetSegment.Flags.TrafficEnd) != NetSegment.Flags.None)
					{
						flags |= NetSegment.Flags.TrafficStart;
					}
					if ((netSegment.m_flags & NetSegment.Flags.YieldEnd) != NetSegment.Flags.None)
					{
						flags |= NetSegment.Flags.YieldStart;
					}
					NetSegment[] expr_6DB_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
					ushort expr_6DB_cp_1 = num2;
					expr_6DB_cp_0[(int)expr_6DB_cp_1].m_flags = (expr_6DB_cp_0[(int)expr_6DB_cp_1].m_flags | flags);
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_wetness = netSegment.m_wetness;
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_condition = netSegment.m_condition;
					Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num2].m_nameSeed = netSegment.m_nameSeed;
					if (text != null)
					{
						NetSegment[] expr_76C_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
						ushort expr_76C_cp_1 = num2;
						expr_76C_cp_0[(int)expr_76C_cp_1].m_flags = (expr_76C_cp_0[(int)expr_76C_cp_1].m_flags | NetSegment.Flags.CustomName);
						InstanceID empty4 = InstanceID.Empty;
						empty4.NetSegment = num2;
						Singleton<InstanceManager>.get_instance().SetName(empty4, text);
					}
					if (flag)
					{
						Singleton<NetManager>.get_instance().m_adjustedSegments[num2 >> 6] |= 1uL << (int)num2;
					}
				}
				else
				{
					flag2 = true;
				}
			}
			info.m_netAI.AfterSplitOrMove(node, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)node], netSegment.m_startNode, netSegment.m_endNode);
		}
		else
		{
			flag2 = true;
		}
		if (flag2 && node != 0)
		{
			Singleton<NetManager>.get_instance().ReleaseNode(node);
			node = 0;
		}
		return !flag2;
	}

	private static void TryMoveNode(ref ushort node, ref Vector3 direction, NetInfo segmentInfo, Vector3 endPos)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		if ((instance.m_nodes.m_buffer[(int)node].m_flags & (NetNode.Flags.End | NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == (NetNode.Flags.End | NetNode.Flags.Moveable))
		{
			NetInfo info = instance.m_nodes.m_buffer[(int)node].Info;
			if (info.IsCombatible(segmentInfo))
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
					if (segment != 0)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)segment].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)segment].m_endNode;
						Vector3 startDirection = instance.m_segments.m_buffer[(int)segment].m_startDirection;
						Vector3 endDirection = instance.m_segments.m_buffer[(int)segment].m_endDirection;
						Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
						if (NetSegment.IsStraight(position, startDirection, position2, endDirection))
						{
							Vector3 vector = (startNode != node) ? endDirection : startDirection;
							float num = direction.x * vector.x + direction.z * vector.z;
							if (num < -0.999f)
							{
								NetTool.MoveMiddleNode(ref node, ref direction, endPos);
							}
						}
						return;
					}
				}
			}
		}
	}

	private static Vector3 LerpPosition(Vector3 refPos1, Vector3 refPos2, float t, float snap)
	{
		if (snap != 0f)
		{
			Vector2 vector;
			vector..ctor(refPos2.x - refPos1.x, refPos2.z - refPos1.z);
			float magnitude = vector.get_magnitude();
			if (magnitude != 0f)
			{
				t = Mathf.Round(t * magnitude / snap + 0.01f) * (snap / magnitude);
			}
		}
		return Vector3.Lerp(refPos1, refPos2, t);
	}

	private static bool CanAddSegment(ushort nodeID, Vector3 direction, ulong[] collidingSegmentBuffer, ushort ignoreSegment)
	{
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)nodeID];
		bool flag = (netNode.m_flags & NetNode.Flags.Double) != NetNode.Flags.None && ignoreSegment == 0;
		bool result = true;
		if ((netNode.m_flags & (NetNode.Flags.Middle | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Untouchable) && netNode.CountSegments(NetSegment.Flags.Untouchable, ignoreSegment) >= 2)
		{
			flag = true;
		}
		for (int i = 0; i < 8; i++)
		{
			ushort segment = netNode.GetSegment(i);
			if (segment != 0 && segment != ignoreSegment)
			{
				NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
				Vector3 vector = (nodeID != netSegment.m_startNode) ? netSegment.m_endDirection : netSegment.m_startDirection;
				float num = direction.x * vector.x + direction.z * vector.z;
				if (flag || num > 0.75f)
				{
					if (collidingSegmentBuffer != null)
					{
						collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					}
					result = false;
				}
			}
		}
		return result;
	}

	private static bool CanAddNode(ushort segmentID, ushort nodeID, Vector3 position, Vector3 direction)
	{
		NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)nodeID];
		if ((netNode.m_flags & NetNode.Flags.Double) != NetNode.Flags.None)
		{
			return false;
		}
		if ((netNode.m_flags & (NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == NetNode.Flags.Moveable)
		{
			return true;
		}
		NetInfo info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segmentID].Info;
		NetInfo info2 = netNode.Info;
		if (!info.m_netAI.CanModify())
		{
			return false;
		}
		float minNodeDistance = info2.GetMinNodeDistance();
		Vector2 vector;
		vector..ctor(netNode.m_position.x - position.x, netNode.m_position.z - position.z);
		float magnitude = vector.get_magnitude();
		return magnitude >= minNodeDistance;
	}

	private static bool CanAddNode(ushort segmentID, Vector3 position, Vector3 direction, bool checkDirection, ulong[] collidingSegmentBuffer)
	{
		bool flag = true;
		NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segmentID];
		if ((netSegment.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
		{
			flag = false;
		}
		if (checkDirection)
		{
			Vector3 vector;
			Vector3 vector2;
			netSegment.GetClosestPositionAndDirection(position, out vector, out vector2);
			float num = direction.x * vector2.x + direction.z * vector2.z;
			if (num > 0.75f || num < -0.75f)
			{
				flag = false;
			}
		}
		if (!NetTool.CanAddNode(segmentID, netSegment.m_startNode, position, direction))
		{
			flag = false;
		}
		if (!NetTool.CanAddNode(segmentID, netSegment.m_endNode, position, direction))
		{
			flag = false;
		}
		if (!flag && collidingSegmentBuffer != null)
		{
			collidingSegmentBuffer[segmentID >> 6] |= 1uL << (int)segmentID;
		}
		return flag;
	}

	private static bool CheckStartAndEnd(ushort upgrading, ushort startSegment, ushort startNode, ushort endSegment, ushort endNode, ulong[] collidingSegmentBuffer)
	{
		bool result = true;
		if (startSegment != 0 && endSegment != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort startNode2 = instance.m_segments.m_buffer[(int)startSegment].m_startNode;
			ushort endNode2 = instance.m_segments.m_buffer[(int)startSegment].m_endNode;
			ushort startNode3 = instance.m_segments.m_buffer[(int)endSegment].m_startNode;
			ushort endNode3 = instance.m_segments.m_buffer[(int)endSegment].m_endNode;
			if (startSegment == endSegment || startNode2 == startNode3 || startNode2 == endNode3 || endNode2 == startNode3 || endNode2 == endNode3)
			{
				if (collidingSegmentBuffer != null)
				{
					collidingSegmentBuffer[startSegment >> 6] |= 1uL << (int)startSegment;
					collidingSegmentBuffer[endSegment >> 6] |= 1uL << (int)endSegment;
				}
				result = false;
			}
		}
		if (startSegment != 0 && endNode != 0)
		{
			NetManager instance2 = Singleton<NetManager>.get_instance();
			if (instance2.m_segments.m_buffer[(int)startSegment].m_startNode == endNode || instance2.m_segments.m_buffer[(int)startSegment].m_endNode == endNode)
			{
				if (collidingSegmentBuffer != null)
				{
					collidingSegmentBuffer[startSegment >> 6] |= 1uL << (int)startSegment;
				}
				result = false;
			}
		}
		if (endSegment != 0 && startNode != 0)
		{
			NetManager instance3 = Singleton<NetManager>.get_instance();
			if (instance3.m_segments.m_buffer[(int)endSegment].m_startNode == startNode || instance3.m_segments.m_buffer[(int)endSegment].m_endNode == startNode)
			{
				if (collidingSegmentBuffer != null)
				{
					collidingSegmentBuffer[endSegment >> 6] |= 1uL << (int)endSegment;
				}
				result = false;
			}
		}
		if (startNode != 0 && endNode != 0 && upgrading == 0)
		{
			NetManager instance4 = Singleton<NetManager>.get_instance();
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance4.m_nodes.m_buffer[(int)startNode].GetSegment(i);
				if (segment != 0)
				{
					ushort startNode4 = instance4.m_segments.m_buffer[(int)segment].m_startNode;
					ushort endNode4 = instance4.m_segments.m_buffer[(int)segment].m_endNode;
					if ((startNode4 == startNode && endNode4 == endNode) || (startNode4 == endNode && endNode4 == startNode))
					{
						if (collidingSegmentBuffer != null)
						{
							collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
						}
						result = false;
					}
				}
			}
		}
		return result;
	}

	private static ToolBase.ToolErrors CanCreateSegment(NetInfo segmentInfo, ushort startNode, ushort upgrading, Vector3 endPos, Vector3 startDir, Vector3 endDir, ulong[] collidingSegmentBuffer)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 8; i++)
		{
			ushort segment = instance.m_nodes.m_buffer[(int)startNode].GetSegment(i);
			if (segment != 0 && segment != upgrading)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)segment].Info;
				bool flag = instance.m_segments.m_buffer[(int)segment].m_startNode == startNode;
				ushort num = (!flag) ? instance.m_segments.m_buffer[(int)segment].m_endNode : instance.m_segments.m_buffer[(int)segment].m_startNode;
				ushort num2 = (!flag) ? instance.m_segments.m_buffer[(int)segment].m_startNode : instance.m_segments.m_buffer[(int)segment].m_endNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)num].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)num2].m_position;
				Vector3 vector = (!flag) ? instance.m_segments.m_buffer[(int)segment].m_endDirection : instance.m_segments.m_buffer[(int)segment].m_startDirection;
				Vector3 vector2 = (!flag) ? instance.m_segments.m_buffer[(int)segment].m_startDirection : instance.m_segments.m_buffer[(int)segment].m_endDirection;
				Vector3 vector3;
				Vector3 vector4;
				bool flag2;
				NetSegment.CalculateCorner(info, position, position2, vector, vector2, segmentInfo, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, num, false, true, out vector3, out vector4, out flag2);
				Vector3 vector5;
				Vector3 vector6;
				NetSegment.CalculateCorner(info, position, position2, vector, vector2, segmentInfo, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, num, false, false, out vector5, out vector6, out flag2);
				Vector3 vector7;
				Vector3 vector8;
				bool flag3;
				NetSegment.CalculateCorner(info, position2, position, vector2, vector, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, num2, false, false, out vector7, out vector8, out flag3);
				Vector3 vector9;
				Vector3 vector10;
				NetSegment.CalculateCorner(info, position2, position, vector2, vector, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, num2, false, true, out vector9, out vector10, out flag3);
				if ((vector7.x - vector3.x) * vector.x + (vector7.z - vector3.z) * vector.z < 2f)
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if ((vector3.x - vector7.x) * vector2.x + (vector3.z - vector7.z) * vector2.z < 2f)
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if ((vector9.x - vector5.x) * vector.x + (vector9.z - vector5.z) * vector.z < 2f)
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if ((vector5.x - vector9.x) * vector2.x + (vector5.z - vector9.z) * vector2.z < 2f)
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (VectorUtils.LengthSqrXZ(vector7 - vector3) * info.m_maxSlope * info.m_maxSlope * 4f < (vector7.y - vector3.y) * (vector7.y - vector3.y))
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
				}
				if (VectorUtils.LengthSqrXZ(vector9 - vector5) * info.m_maxSlope * info.m_maxSlope * 4f < (vector9.y - vector5.y) * (vector9.y - vector5.y))
				{
					collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
					toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
				}
			}
		}
		return toolErrors;
	}

	private static ToolBase.ToolErrors CanCreateSegment(ushort segment, ushort endNode, ushort otherNode, NetInfo info1, Vector3 startPos, Vector3 endPos, Vector3 startDir, Vector3 endDir, NetInfo info2, Vector3 endPos2, Vector3 startDir2, Vector3 endDir2, ulong[] collidingSegmentBuffer)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		NetManager instance = Singleton<NetManager>.get_instance();
		bool flag = true;
		if ((instance.m_nodes.m_buffer[(int)endNode].m_flags & (NetNode.Flags.Middle | NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Moveable))
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment2 = instance.m_nodes.m_buffer[(int)endNode].GetSegment(i);
				if (segment2 != 0 && segment2 != segment)
				{
					segment = segment2;
					info1 = instance.m_segments.m_buffer[(int)segment].Info;
					ushort num;
					if (instance.m_segments.m_buffer[(int)segment].m_startNode == endNode)
					{
						num = instance.m_segments.m_buffer[(int)segment].m_endNode;
						endDir = instance.m_segments.m_buffer[(int)segment].m_endDirection;
					}
					else
					{
						num = instance.m_segments.m_buffer[(int)segment].m_startNode;
						endDir = instance.m_segments.m_buffer[(int)segment].m_startDirection;
					}
					if (num == otherNode)
					{
						flag = false;
					}
					else
					{
						endNode = num;
						endPos = instance.m_nodes.m_buffer[(int)endNode].m_position;
					}
					break;
				}
			}
		}
		if (flag && (instance.m_nodes.m_buffer[(int)endNode].m_flags & (NetNode.Flags.Middle | NetNode.Flags.Moveable | NetNode.Flags.Untouchable)) == (NetNode.Flags.Middle | NetNode.Flags.Moveable))
		{
			return toolErrors;
		}
		Vector3 vector;
		Vector3 vector2;
		bool flag2;
		NetSegment.CalculateCorner(info1, startPos, endPos, startDir, endDir, info2, endPos2, startDir2, endDir2, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, 0, false, true, out vector, out vector2, out flag2);
		Vector3 vector3;
		Vector3 vector4;
		NetSegment.CalculateCorner(info1, startPos, endPos, startDir, endDir, info2, endPos2, startDir2, endDir2, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, 0, false, false, out vector3, out vector4, out flag2);
		Vector3 vector5;
		Vector3 vector6;
		bool flag3;
		NetSegment.CalculateCorner(info1, endPos, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, endNode, false, false, out vector5, out vector6, out flag3);
		Vector3 vector7;
		Vector3 vector8;
		NetSegment.CalculateCorner(info1, endPos, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), segment, endNode, false, true, out vector7, out vector8, out flag3);
		if ((vector5.x - vector.x) * startDir.x + (vector5.z - vector.z) * startDir.z < 2f)
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if ((vector.x - vector5.x) * endDir.x + (vector.z - vector5.z) * endDir.z < 2f)
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if ((vector7.x - vector3.x) * startDir.x + (vector7.z - vector3.z) * startDir.z < 2f)
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if ((vector3.x - vector7.x) * endDir.x + (vector3.z - vector7.z) * endDir.z < 2f)
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.ObjectCollision;
		}
		if (VectorUtils.LengthSqrXZ(vector5 - vector) * info1.m_maxSlope * info1.m_maxSlope * 4f < (vector5.y - vector.y) * (vector5.y - vector.y))
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
		}
		if (VectorUtils.LengthSqrXZ(vector7 - vector3) * info1.m_maxSlope * info1.m_maxSlope * 4f < (vector7.y - vector3.y) * (vector7.y - vector3.y))
		{
			collidingSegmentBuffer[segment >> 6] |= 1uL << (int)segment;
			toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
		}
		return toolErrors;
	}

	private static ToolBase.ToolErrors CanCreateSegment(NetInfo segmentInfo, ushort startNode, ushort startSegment, ushort endNode, ushort endSegment, ushort upgrading, Vector3 startPos, Vector3 endPos, Vector3 startDir, Vector3 endDir, ulong[] collidingSegmentBuffer, bool testEnds)
	{
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		NetManager instance = Singleton<NetManager>.get_instance();
		Vector3 vector3;
		Vector3 vector5;
		if (startSegment != 0 && startNode == 0)
		{
			NetInfo info = instance.m_segments.m_buffer[(int)startSegment].Info;
			Vector3 vector;
			Vector3 vector2;
			instance.m_segments.m_buffer[(int)startSegment].GetClosestPositionAndDirection(startPos, out vector, out vector2);
			vector2 = VectorUtils.NormalizeXZ(vector2);
			ushort startNode2 = instance.m_segments.m_buffer[(int)startSegment].m_startNode;
			ushort endNode2 = instance.m_segments.m_buffer[(int)startSegment].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode2].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode2].m_position;
			Vector3 startDirection = instance.m_segments.m_buffer[(int)startSegment].m_startDirection;
			Vector3 endDirection = instance.m_segments.m_buffer[(int)startSegment].m_endDirection;
			Vector3 vector4;
			bool flag;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, info, position, -vector2, startDirection, info, position2, vector2, endDirection, 0, 0, false, true, out vector3, out vector4, out flag);
			Vector3 vector6;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, info, position, -vector2, startDirection, info, position2, vector2, endDirection, 0, 0, false, false, out vector5, out vector6, out flag);
			toolErrors |= NetTool.CanCreateSegment(startSegment, startNode2, endNode, info, startPos, position, -vector2, startDirection, segmentInfo, endPos, startDir, endDir, collidingSegmentBuffer);
			toolErrors |= NetTool.CanCreateSegment(startSegment, endNode2, endNode, info, startPos, position2, vector2, endDirection, segmentInfo, endPos, startDir, endDir, collidingSegmentBuffer);
		}
		else if (upgrading != 0 || startNode == 0)
		{
			Vector3 vector4;
			bool flag;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), upgrading, startNode, false, true, out vector3, out vector4, out flag);
			Vector3 vector6;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), upgrading, startNode, false, false, out vector5, out vector6, out flag);
			if (startNode != 0)
			{
				toolErrors |= NetTool.CanCreateSegment(segmentInfo, startNode, upgrading, endPos, startDir, endDir, collidingSegmentBuffer);
			}
		}
		else
		{
			Vector3 vector4;
			bool flag;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, segmentInfo, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, startNode, false, true, out vector3, out vector4, out flag);
			Vector3 vector6;
			NetSegment.CalculateCorner(segmentInfo, startPos, endPos, startDir, endDir, segmentInfo, endPos, startDir, endDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, startNode, false, false, out vector5, out vector6, out flag);
			if (startNode != 0)
			{
				toolErrors |= NetTool.CanCreateSegment(segmentInfo, startNode, 0, endPos, startDir, endDir, collidingSegmentBuffer);
			}
		}
		Vector3 vector9;
		Vector3 vector11;
		if (endSegment != 0 && endNode == 0)
		{
			NetInfo info2 = instance.m_segments.m_buffer[(int)endSegment].Info;
			Vector3 vector7;
			Vector3 vector8;
			instance.m_segments.m_buffer[(int)endSegment].GetClosestPositionAndDirection(startPos, out vector7, out vector8);
			vector8 = VectorUtils.NormalizeXZ(vector8);
			ushort startNode3 = instance.m_segments.m_buffer[(int)endSegment].m_startNode;
			ushort endNode3 = instance.m_segments.m_buffer[(int)endSegment].m_endNode;
			Vector3 position3 = instance.m_nodes.m_buffer[(int)startNode3].m_position;
			Vector3 position4 = instance.m_nodes.m_buffer[(int)endNode3].m_position;
			Vector3 startDirection2 = instance.m_segments.m_buffer[(int)endSegment].m_startDirection;
			Vector3 endDirection2 = instance.m_segments.m_buffer[(int)endSegment].m_endDirection;
			Vector3 vector10;
			bool flag2;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, info2, position3, -vector8, startDirection2, info2, position4, vector8, endDirection2, 0, 0, false, false, out vector9, out vector10, out flag2);
			Vector3 vector12;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, info2, position3, -vector8, startDirection2, info2, position4, vector8, endDirection2, 0, 0, false, true, out vector11, out vector12, out flag2);
			toolErrors |= NetTool.CanCreateSegment(endSegment, startNode3, startNode, info2, endPos, position3, -vector8, startDirection2, segmentInfo, startPos, endDir, startDir, collidingSegmentBuffer);
			toolErrors |= NetTool.CanCreateSegment(endSegment, endNode3, startNode, info2, endPos, position4, vector8, endDirection2, segmentInfo, startPos, endDir, startDir, collidingSegmentBuffer);
		}
		else if (upgrading != 0 || endNode == 0)
		{
			Vector3 vector10;
			bool flag2;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), upgrading, endNode, false, false, out vector9, out vector10, out flag2);
			Vector3 vector12;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), upgrading, endNode, false, true, out vector11, out vector12, out flag2);
			if (endNode != 0)
			{
				toolErrors |= NetTool.CanCreateSegment(segmentInfo, endNode, upgrading, startPos, endDir, startDir, collidingSegmentBuffer);
			}
		}
		else
		{
			Vector3 vector10;
			bool flag2;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, segmentInfo, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, endNode, false, false, out vector9, out vector10, out flag2);
			Vector3 vector12;
			NetSegment.CalculateCorner(segmentInfo, endPos, startPos, endDir, startDir, segmentInfo, startPos, endDir, startDir, null, Vector3.get_zero(), Vector3.get_zero(), Vector3.get_zero(), 0, endNode, false, true, out vector11, out vector12, out flag2);
			if (endNode != 0)
			{
				toolErrors |= NetTool.CanCreateSegment(segmentInfo, endNode, 0, startPos, endDir, startDir, collidingSegmentBuffer);
			}
		}
		if ((vector9.x - vector3.x) * startDir.x + (vector9.z - vector3.z) * startDir.z < 2f)
		{
			toolErrors |= ToolBase.ToolErrors.TooShort;
		}
		if ((vector3.x - vector9.x) * endDir.x + (vector3.z - vector9.z) * endDir.z < 2f)
		{
			toolErrors |= ToolBase.ToolErrors.TooShort;
		}
		if ((vector11.x - vector5.x) * startDir.x + (vector11.z - vector5.z) * startDir.z < 2f)
		{
			toolErrors |= ToolBase.ToolErrors.TooShort;
		}
		if ((vector5.x - vector11.x) * endDir.x + (vector5.z - vector11.z) * endDir.z < 2f)
		{
			toolErrors |= ToolBase.ToolErrors.TooShort;
		}
		if (VectorUtils.LengthSqrXZ(vector9 - vector3) * segmentInfo.m_maxSlope * segmentInfo.m_maxSlope * 4f < (vector9.y - vector3.y) * (vector9.y - vector3.y))
		{
			toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
		}
		if (VectorUtils.LengthSqrXZ(vector11 - vector5) * segmentInfo.m_maxSlope * segmentInfo.m_maxSlope * 4f < (vector11.y - vector5.y) * (vector11.y - vector5.y))
		{
			toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
		}
		return toolErrors;
	}

	[DebuggerHidden]
	private IEnumerator<bool> CreateNode(bool switchDirection)
	{
		NetTool.<CreateNode>c__Iterator0 <CreateNode>c__Iterator = new NetTool.<CreateNode>c__Iterator0();
		<CreateNode>c__Iterator.switchDirection = switchDirection;
		<CreateNode>c__Iterator.$this = this;
		return <CreateNode>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator CreateFailed()
	{
		NetTool.<CreateFailed>c__Iterator1 <CreateFailed>c__Iterator = new NetTool.<CreateFailed>c__Iterator1();
		<CreateFailed>c__Iterator.$this = this;
		return <CreateFailed>c__Iterator;
	}

	private bool CreateNodeImpl(bool switchDirection)
	{
		NetInfo prefab = this.m_prefab;
		if (prefab != null)
		{
			if (this.m_mode == NetTool.Mode.Upgrade && this.m_controlPointCount < 2)
			{
				prefab.m_netAI.UpgradeFailed();
			}
			else
			{
				if (this.m_mode == NetTool.Mode.Straight && this.m_controlPointCount < 1)
				{
					int num;
					int num2;
					prefab.m_netAI.GetElevationLimits(out num, out num2);
					this.m_elevation = Mathf.Clamp(Mathf.RoundToInt(this.m_controlPoints[this.m_controlPointCount].m_elevation * 256f / 12f), num * 256, num2 * 256);
					if (this.m_elevation < 0 && this.m_elevation > -256)
					{
						this.m_elevation = -256;
					}
					this.m_controlPoints[this.m_controlPointCount + 1] = this.m_controlPoints[this.m_controlPointCount];
					this.m_controlPoints[this.m_controlPointCount + 1].m_node = 0;
					this.m_controlPoints[this.m_controlPointCount + 1].m_segment = 0;
					this.m_controlPointCount++;
					return true;
				}
				if ((this.m_mode == NetTool.Mode.Curved || this.m_mode == NetTool.Mode.Freeform) && this.m_controlPointCount < 2 && (this.m_controlPointCount == 0 || (this.m_controlPoints[1].m_node == 0 && this.m_controlPoints[1].m_segment == 0)))
				{
					int num3;
					int num4;
					prefab.m_netAI.GetElevationLimits(out num3, out num4);
					this.m_elevation = Mathf.Clamp(Mathf.RoundToInt(this.m_controlPoints[this.m_controlPointCount].m_elevation * 256f / 12f), num3 * 256, num4 * 256);
					if (this.m_elevation < 0 && this.m_elevation > -256)
					{
						this.m_elevation = -256;
					}
					this.m_controlPoints[this.m_controlPointCount + 1] = this.m_controlPoints[this.m_controlPointCount];
					this.m_controlPoints[this.m_controlPointCount + 1].m_node = 0;
					this.m_controlPoints[this.m_controlPointCount + 1].m_segment = 0;
					this.m_controlPointCount++;
					return true;
				}
				bool needMoney = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None;
				if (this.m_mode == NetTool.Mode.Upgrade)
				{
					this.m_upgrading = true;
					this.m_switchingDir = switchDirection;
				}
				NetTool.ControlPoint controlPoint;
				NetTool.ControlPoint controlPoint2;
				NetTool.ControlPoint controlPoint3;
				if (this.m_controlPointCount == 1)
				{
					controlPoint = this.m_controlPoints[0];
					controlPoint2 = this.m_controlPoints[1];
					controlPoint3 = this.m_controlPoints[1];
					controlPoint3.m_node = 0;
					controlPoint3.m_segment = 0;
					controlPoint3.m_position = (this.m_controlPoints[0].m_position + this.m_controlPoints[1].m_position) * 0.5f;
					controlPoint3.m_elevation = (this.m_controlPoints[0].m_elevation + this.m_controlPoints[1].m_elevation) * 0.5f;
				}
				else
				{
					controlPoint = this.m_controlPoints[0];
					controlPoint3 = this.m_controlPoints[1];
					controlPoint2 = this.m_controlPoints[2];
				}
				NetTool.ControlPoint startPoint = controlPoint;
				NetTool.ControlPoint middlePoint = controlPoint3;
				NetTool.ControlPoint endPoint = controlPoint2;
				bool secondaryControlPoints = NetTool.GetSecondaryControlPoints(prefab, ref startPoint, ref middlePoint, ref endPoint);
				if (this.CreateNodeImpl(prefab, needMoney, switchDirection, controlPoint, controlPoint3, controlPoint2))
				{
					if (secondaryControlPoints)
					{
						this.CreateNodeImpl(prefab, needMoney, switchDirection, startPoint, middlePoint, endPoint);
					}
					return true;
				}
			}
		}
		return false;
	}

	private bool CreateNodeImpl(NetInfo info, bool needMoney, bool switchDirection, NetTool.ControlPoint startPoint, NetTool.ControlPoint middlePoint, NetTool.ControlPoint endPoint)
	{
		bool flag = endPoint.m_node != 0 || endPoint.m_segment != 0;
		bool repairing = false;
		if (middlePoint.m_segment != 0)
		{
			repairing = ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)middlePoint.m_segment].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None);
		}
		ushort num;
		ushort num2;
		int num3;
		int num4;
		if (NetTool.CreateNode(info, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsSimulation, 1000, true, false, true, needMoney, false, switchDirection, 0, out num, out num2, out num3, out num4) == ToolBase.ToolErrors.None)
		{
			NetTool.CreateNode(info, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsSimulation, 1000, false, false, true, needMoney, false, switchDirection, 0, out num, out num2, out num3, out num4);
			NetManager instance = Singleton<NetManager>.get_instance();
			endPoint.m_segment = 0;
			endPoint.m_node = num;
			if (num2 != 0)
			{
				if (this.m_upgrading)
				{
					while (!Monitor.TryEnter(this.m_upgradedSegments, SimulationManager.SYNCHRONIZE_TIMEOUT))
					{
					}
					try
					{
						if (this.m_upgradedSegments.Count == 0)
						{
							this.m_repairing = repairing;
						}
						this.m_upgradedSegments.Add(num2);
					}
					finally
					{
						Monitor.Exit(this.m_upgradedSegments);
					}
				}
				if (instance.m_segments.m_buffer[(int)num2].m_startNode == num)
				{
					endPoint.m_direction = -instance.m_segments.m_buffer[(int)num2].m_startDirection;
				}
				else if (instance.m_segments.m_buffer[(int)num2].m_endNode == num)
				{
					endPoint.m_direction = -instance.m_segments.m_buffer[(int)num2].m_endDirection;
				}
			}
			this.m_controlPoints[0] = endPoint;
			if (!this.m_upgrading)
			{
				int num5;
				int num6;
				info.m_netAI.GetElevationLimits(out num5, out num6);
				this.m_elevation = Mathf.Clamp(Mathf.RoundToInt(endPoint.m_elevation * 256f / 12f), num5 * 256, num6 * 256);
			}
			if (num != 0 && (instance.m_nodes.m_buffer[(int)num].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None)
			{
				this.m_controlPointCount = 0;
			}
			else if (this.m_mode == NetTool.Mode.Freeform && this.m_controlPointCount == 2)
			{
				middlePoint.m_position = endPoint.m_position * 2f - middlePoint.m_position;
				middlePoint.m_elevation = endPoint.m_elevation * 2f - middlePoint.m_elevation;
				middlePoint.m_direction = endPoint.m_direction;
				middlePoint.m_node = 0;
				middlePoint.m_segment = 0;
				this.m_controlPoints[1] = middlePoint;
				this.m_controlPointCount = 2;
			}
			else
			{
				this.m_controlPointCount = 1;
			}
			int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
			if (publicServiceIndex != -1)
			{
				Singleton<GuideManager>.get_instance().m_serviceNotUsed[publicServiceIndex].Disable();
				Singleton<GuideManager>.get_instance().m_serviceNeeded[publicServiceIndex].Deactivate();
			}
			if (info.m_class.m_service == ItemClass.Service.Road)
			{
				Singleton<CoverageManager>.get_instance().CoverageUpdated(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.None);
				Singleton<NetManager>.get_instance().m_roadsNotUsed.Disable();
			}
			if ((info.m_class.m_service == ItemClass.Service.Road || info.m_class.m_service == ItemClass.Service.PublicTransport || info.m_class.m_service == ItemClass.Service.Beautification) && (info.m_hasForwardVehicleLanes || info.m_hasBackwardVehicleLanes) && (!info.m_hasForwardVehicleLanes || !info.m_hasBackwardVehicleLanes))
			{
				Singleton<NetManager>.get_instance().m_onewayRoadPlacement.Disable();
			}
			if (this.m_upgrading)
			{
				info.m_netAI.UpgradeSucceeded();
			}
			else
			{
				info.m_netAI.ConstructSucceeded();
				if (flag && num != 0)
				{
					info.m_netAI.ConnectionSucceeded(num, ref Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num]);
				}
			}
			Singleton<GuideManager>.get_instance().m_notEnoughMoney.Deactivate();
			if (Singleton<GuideManager>.get_instance().m_properties != null && !this.m_upgrading && num2 != 0 && this.m_bulldozerTool != null && this.m_bulldozerTool.m_lastNetInfo != null && this.m_bulldozerTool.m_lastNetInfo.m_netAI.CanUpgradeTo(info))
			{
				ushort startNode = instance.m_segments.m_buffer[(int)num2].m_startNode;
				ushort endNode = instance.m_segments.m_buffer[(int)num2].m_endNode;
				Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
				Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
				Vector3 startDirection = instance.m_segments.m_buffer[(int)num2].m_startDirection;
				Vector3 endDirection = instance.m_segments.m_buffer[(int)num2].m_endDirection;
				if (Vector3.SqrMagnitude(this.m_bulldozerTool.m_lastStartPos - position) < 1f && Vector3.SqrMagnitude(this.m_bulldozerTool.m_lastEndPos - position2) < 1f && Vector2.Dot(VectorUtils.XZ(this.m_bulldozerTool.m_lastStartDir), VectorUtils.XZ(startDirection)) > 0.99f && Vector2.Dot(VectorUtils.XZ(this.m_bulldozerTool.m_lastEndDir), VectorUtils.XZ(endDirection)) > 0.99f)
				{
					Singleton<NetManager>.get_instance().m_manualUpgrade.Activate(Singleton<GuideManager>.get_instance().m_properties.m_manualUpgrade, info.m_class.m_service);
				}
			}
			return true;
		}
		return false;
	}

	[DebuggerHidden]
	private IEnumerator CancelNode()
	{
		NetTool.<CancelNode>c__Iterator2 <CancelNode>c__Iterator = new NetTool.<CancelNode>c__Iterator2();
		<CancelNode>c__Iterator.$this = this;
		return <CancelNode>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator CancelUpgrading()
	{
		NetTool.<CancelUpgrading>c__Iterator3 <CancelUpgrading>c__Iterator = new NetTool.<CancelUpgrading>c__Iterator3();
		<CancelUpgrading>c__Iterator.$this = this;
		return <CancelUpgrading>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator<bool> ChangeElevation(int delta)
	{
		NetTool.<ChangeElevation>c__Iterator4 <ChangeElevation>c__Iterator = new NetTool.<ChangeElevation>c__Iterator4();
		<ChangeElevation>c__Iterator.delta = delta;
		<ChangeElevation>c__Iterator.$this = this;
		return <ChangeElevation>c__Iterator;
	}

	private float GetElevation(NetInfo info)
	{
		if (info == null)
		{
			return 0f;
		}
		int num;
		int num2;
		info.m_netAI.GetElevationLimits(out num, out num2);
		if (num == num2)
		{
			return 0f;
		}
		return (float)Mathf.Clamp(this.m_elevation, num * 256, num2 * 256) / 256f * 12f;
	}

	private static ToolBase.ToolErrors TestNodeBuilding(BuildingInfo info, Vector3 position, Vector3 direction, ushort ignoreNode, ushort ignoreSegment, ushort ignoreBuilding, bool test, ulong[] collidingSegmentBuffer, ulong[] collidingBuildingBuffer)
	{
		Vector2 vector;
		vector..ctor(direction.x, direction.z);
		Vector2 vector2;
		vector2..ctor(direction.z, -direction.x);
		if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside)
		{
			vector2 *= (float)info.m_cellWidth * 4f - 0.8f;
			vector *= (float)info.m_cellLength * 4f - 0.8f;
		}
		else
		{
			vector2 *= (float)info.m_cellWidth * 4f;
			vector *= (float)info.m_cellLength * 4f;
		}
		if (info.m_circular)
		{
			vector2 *= 0.7f;
			vector *= 0.7f;
		}
		ItemClass.CollisionType collisionType = info.m_buildingAI.GetCollisionType();
		Vector2 vector3 = VectorUtils.XZ(position);
		Quad2 quad = default(Quad2);
		quad.a = vector3 - vector2 - vector;
		quad.b = vector3 - vector2 + vector;
		quad.c = vector3 + vector2 + vector;
		quad.d = vector3 + vector2 - vector;
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		float minY = Mathf.Min(position.y, Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(position));
		float maxY = position.y + info.m_generatedInfo.m_size.y;
		Singleton<NetManager>.get_instance().OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, ignoreNode, 0, ignoreSegment, collidingSegmentBuffer);
		Singleton<BuildingManager>.get_instance().OverlapQuad(quad, minY, maxY, collisionType, info.m_class.m_layer, ignoreBuilding, ignoreNode, 0, collidingBuildingBuffer);
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			float num = 256f;
			if (quad.a.x < -num || quad.a.x > num || quad.a.y < -num || quad.a.y > num)
			{
				toolErrors |= ToolBase.ToolErrors.OutOfArea;
			}
			if (quad.b.x < -num || quad.b.x > num || quad.b.y < -num || quad.b.y > num)
			{
				toolErrors |= ToolBase.ToolErrors.OutOfArea;
			}
			if (quad.c.x < -num || quad.c.x > num || quad.c.y < -num || quad.c.y > num)
			{
				toolErrors |= ToolBase.ToolErrors.OutOfArea;
			}
			if (quad.d.x < -num || quad.d.x > num || quad.d.y < -num || quad.d.y > num)
			{
				toolErrors |= ToolBase.ToolErrors.OutOfArea;
			}
		}
		else if (Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad))
		{
			toolErrors |= ToolBase.ToolErrors.OutOfArea;
		}
		if (!Singleton<BuildingManager>.get_instance().CheckLimits())
		{
			toolErrors |= ToolBase.ToolErrors.TooManyObjects;
		}
		return toolErrors;
	}

	private static void RenderNodeBuilding(BuildingInfo info, Vector3 position, Vector3 direction)
	{
		if (!Singleton<ToolManager>.get_instance().m_properties.IsInsideUI)
		{
			if (direction.get_sqrMagnitude() < 0.5f)
			{
				return;
			}
			if (info.m_mesh == null)
			{
				return;
			}
			direction.y = 0f;
			Quaternion quaternion = Quaternion.LookRotation(direction, Vector3.get_up());
			Building building = default(Building);
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			instance.m_materialBlock.Clear();
			instance.m_materialBlock.SetVector(instance.ID_BuildingState, new Vector4(0f, 1000f, 0f, 256f));
			instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, RenderManager.DefaultColorLocation);
			instance.m_materialBlock.SetColor(instance.ID_Color, info.m_buildingAI.GetColor(0, ref building, Singleton<InfoManager>.get_instance().CurrentMode));
			ToolManager expr_DC_cp_0 = Singleton<ToolManager>.get_instance();
			expr_DC_cp_0.m_drawCallData.m_defaultCalls = expr_DC_cp_0.m_drawCallData.m_defaultCalls + 1;
			Graphics.DrawMesh(info.m_mesh, position, quaternion, info.m_material, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
		}
	}

	private static void RenderSegment(NetInfo info, NetSegment.Flags flags, Vector3 startPosition, Vector3 endPosition, Vector3 startDirection, Vector3 endDirection, bool smoothStart, bool smoothEnd)
	{
		if (info.m_segments == null)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		startPosition.y += 0.15f;
		endPosition.y += 0.15f;
		Vector3 vector = (startPosition + endPosition) * 0.5f;
		Quaternion identity = Quaternion.get_identity();
		Vector3 vector2 = new Vector3(startDirection.z, 0f, -startDirection.x) * info.m_halfWidth;
		Vector3 vector3 = startPosition - vector2;
		Vector3 vector4 = startPosition + vector2;
		vector2 = new Vector3(endDirection.z, 0f, -endDirection.x) * info.m_halfWidth;
		Vector3 vector5 = endPosition - vector2;
		Vector3 vector6 = endPosition + vector2;
		Vector3 vector7;
		Vector3 vector8;
		NetSegment.CalculateMiddlePoints(vector3, startDirection, vector5, -endDirection, smoothStart, smoothEnd, out vector7, out vector8);
		Vector3 vector9;
		Vector3 vector10;
		NetSegment.CalculateMiddlePoints(vector4, startDirection, vector6, -endDirection, smoothStart, smoothEnd, out vector9, out vector10);
		float vScale = info.m_netAI.GetVScale();
		Matrix4x4 matrix4x = NetSegment.CalculateControlMatrix(vector3, vector7, vector8, vector5, vector4, vector9, vector10, vector6, vector, vScale);
		Matrix4x4 matrix4x2 = NetSegment.CalculateControlMatrix(vector4, vector9, vector10, vector6, vector3, vector7, vector8, vector5, vector, vScale);
		Vector4 vector11;
		vector11..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 1f, 1f);
		instance.m_materialBlock.Clear();
		instance.m_materialBlock.SetMatrix(instance.ID_LeftMatrix, matrix4x);
		instance.m_materialBlock.SetMatrix(instance.ID_RightMatrix, matrix4x2);
		instance.m_materialBlock.SetVector(instance.ID_MeshScale, vector11);
		instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, RenderManager.DefaultColorLocation);
		instance.m_materialBlock.SetColor(instance.ID_Color, info.m_color);
		if (info.m_requireSurfaceMaps)
		{
			Texture texture;
			Texture texture2;
			Vector4 vector12;
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(vector, out texture, out texture2, out vector12);
			if (texture != null)
			{
				instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexA, texture);
				instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexB, texture2);
				instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, vector12);
			}
		}
		bool flag = false;
		for (int i = 0; i < info.m_segments.Length; i++)
		{
			NetInfo.Segment segment = info.m_segments[i];
			bool flag2;
			if (segment.CheckFlags(flags, out flag2))
			{
				if (flag2 != flag)
				{
					vector11.x = -vector11.x;
					vector11.y = -vector11.y;
					instance.m_materialBlock.SetVector(instance.ID_MeshScale, vector11);
					flag = flag2;
				}
				ToolManager expr_29C_cp_0 = Singleton<ToolManager>.get_instance();
				expr_29C_cp_0.m_drawCallData.m_defaultCalls = expr_29C_cp_0.m_drawCallData.m_defaultCalls + 1;
				Graphics.DrawMesh(segment.m_segmentMesh, vector, identity, segment.m_segmentMaterial, segment.m_layer, null, 0, instance.m_materialBlock);
			}
		}
	}

	private static void RenderNode(NetInfo info, Vector3 position, Vector3 direction)
	{
		if (info.m_nodes == null)
		{
			return;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		position.y += 0.15f;
		Quaternion identity = Quaternion.get_identity();
		float vScale = info.m_netAI.GetVScale();
		Vector3 vector = new Vector3(direction.z, 0f, -direction.x) * info.m_halfWidth;
		Vector3 vector2 = position + vector;
		Vector3 vector3 = position - vector;
		Vector3 vector4 = vector3;
		Vector3 vector5 = vector2;
		float num = Mathf.Min(info.m_halfWidth * 1.33333337f, 16f);
		Vector3 vector6 = vector2 - direction * num;
		Vector3 vector7 = vector4 - direction * num;
		Vector3 vector8 = vector3 - direction * num;
		Vector3 vector9 = vector5 - direction * num;
		Vector3 vector10 = vector2 + direction * num;
		Vector3 vector11 = vector4 + direction * num;
		Vector3 vector12 = vector3 + direction * num;
		Vector3 vector13 = vector5 + direction * num;
		Matrix4x4 matrix4x = NetSegment.CalculateControlMatrix(vector2, vector6, vector7, vector4, vector2, vector6, vector7, vector4, position, vScale);
		Matrix4x4 matrix4x2 = NetSegment.CalculateControlMatrix(vector3, vector12, vector13, vector5, vector3, vector12, vector13, vector5, position, vScale);
		Matrix4x4 matrix4x3 = NetSegment.CalculateControlMatrix(vector2, vector10, vector11, vector4, vector2, vector10, vector11, vector4, position, vScale);
		Matrix4x4 matrix4x4 = NetSegment.CalculateControlMatrix(vector3, vector8, vector9, vector5, vector3, vector8, vector9, vector5, position, vScale);
		matrix4x.SetRow(3, matrix4x.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
		matrix4x2.SetRow(3, matrix4x2.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
		matrix4x3.SetRow(3, matrix4x3.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
		matrix4x4.SetRow(3, matrix4x4.GetRow(3) + new Vector4(0.2f, 0.2f, 0.2f, 0.2f));
		Vector4 vector14;
		vector14..ctor(0.5f / info.m_halfWidth, 1f / info.m_segmentLength, 0.5f - info.m_pavementWidth / info.m_halfWidth * 0.5f, info.m_pavementWidth / info.m_halfWidth * 0.5f);
		Vector4 zero = Vector4.get_zero();
		zero.w = (matrix4x.m33 + matrix4x2.m33 + matrix4x3.m33 + matrix4x4.m33) * 0.25f;
		Vector4 vector15;
		vector15..ctor(info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f, info.m_pavementWidth / info.m_halfWidth * 0.5f, 1f);
		instance.m_materialBlock.Clear();
		instance.m_materialBlock.SetMatrix(instance.ID_LeftMatrix, matrix4x);
		instance.m_materialBlock.SetMatrix(instance.ID_RightMatrix, matrix4x2);
		instance.m_materialBlock.SetMatrix(instance.ID_LeftMatrixB, matrix4x3);
		instance.m_materialBlock.SetMatrix(instance.ID_RightMatrixB, matrix4x4);
		instance.m_materialBlock.SetVector(instance.ID_MeshScale, vector14);
		instance.m_materialBlock.SetVector(instance.ID_CenterPos, zero);
		instance.m_materialBlock.SetVector(instance.ID_SideScale, vector15);
		instance.m_materialBlock.SetColor(instance.ID_Color, info.m_color);
		if (info.m_requireSurfaceMaps)
		{
			Texture texture;
			Texture texture2;
			Vector4 vector16;
			Singleton<TerrainManager>.get_instance().GetSurfaceMapping(position, out texture, out texture2, out vector16);
			if (texture != null)
			{
				instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexA, texture);
				instance.m_materialBlock.SetTexture(instance.ID_SurfaceTexB, texture2);
				instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, vector16);
			}
		}
		for (int i = 0; i < info.m_nodes.Length; i++)
		{
			NetInfo.Node node = info.m_nodes[i];
			if (node.CheckFlags(NetNode.Flags.None))
			{
				ToolManager expr_41A_cp_0 = Singleton<ToolManager>.get_instance();
				expr_41A_cp_0.m_drawCallData.m_defaultCalls = expr_41A_cp_0.m_drawCallData.m_defaultCalls + 1;
				Graphics.DrawMesh(node.m_nodeMesh, position, identity, node.m_nodeMaterial, node.m_layer, null, 0, instance.m_materialBlock);
			}
		}
	}

	private static ushort GetIgnoredBuilding(NetTool.ControlPoint point)
	{
		if (point.m_node != 0)
		{
			return Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)point.m_node].m_building;
		}
		if (point.m_segment == 0)
		{
			return 0;
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort startNode = instance.m_segments.m_buffer[(int)point.m_segment].m_startNode;
		ushort endNode = instance.m_segments.m_buffer[(int)point.m_segment].m_endNode;
		Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
		if (Vector3.SqrMagnitude(position - point.m_position) < Vector3.SqrMagnitude(position2 - point.m_position))
		{
			return Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startNode].m_building;
		}
		return Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endNode].m_building;
	}

	private static ToolBase.ToolErrors CheckNodeHeights(NetInfo info, FastList<NetTool.NodePosition> nodeBuffer)
	{
		bool flag = info.m_netAI.BuildUnderground();
		bool flag2 = info.m_netAI.SupportUnderground();
		if (info.m_netAI.LinearMiddleHeight())
		{
			if (nodeBuffer.m_size >= 3)
			{
				Vector2 vector = VectorUtils.XZ(nodeBuffer.m_buffer[0].m_position);
				Vector2 vector2 = VectorUtils.XZ(nodeBuffer.m_buffer[nodeBuffer.m_size - 1].m_position);
				float y = nodeBuffer.m_buffer[0].m_position.y;
				float y2 = nodeBuffer.m_buffer[nodeBuffer.m_size - 1].m_position.y;
				for (int i = 1; i < nodeBuffer.m_size - 1; i++)
				{
					NetTool.NodePosition nodePosition = nodeBuffer.m_buffer[i];
					float num = Vector2.Distance(VectorUtils.XZ(nodePosition.m_position), vector);
					float num2 = Vector2.Distance(VectorUtils.XZ(nodePosition.m_position), vector2);
					nodePosition.m_position.y = Mathf.Lerp(y, y2, num / Mathf.Max(1f, num + num2));
					nodeBuffer.m_buffer[i] = nodePosition;
				}
			}
		}
		else
		{
			int num3 = 0;
			while (true)
			{
				bool flag3 = false;
				for (int j = 1; j < nodeBuffer.m_size; j++)
				{
					NetTool.NodePosition nodePosition2 = nodeBuffer.m_buffer[j - 1];
					NetTool.NodePosition nodePosition3 = nodeBuffer.m_buffer[j];
					float num4 = VectorUtils.LengthXZ(nodePosition3.m_position - nodePosition2.m_position);
					float num5 = num4 * info.m_maxSlope;
					nodePosition3.m_minY = Mathf.Max(nodePosition3.m_minY, nodePosition2.m_minY - num5);
					nodePosition3.m_maxY = Mathf.Min(nodePosition3.m_maxY, nodePosition2.m_maxY + num5);
					if (!flag2)
					{
						nodePosition3.m_terrainHeight = Mathf.Min(nodePosition3.m_terrainHeight, nodePosition3.m_position.y + 7.98f);
						nodePosition3.m_minY = Mathf.Max(nodePosition3.m_minY, nodePosition3.m_terrainHeight - 7.99f);
					}
					nodeBuffer.m_buffer[j] = nodePosition3;
				}
				for (int k = nodeBuffer.m_size - 2; k >= 0; k--)
				{
					NetTool.NodePosition nodePosition4 = nodeBuffer.m_buffer[k + 1];
					NetTool.NodePosition nodePosition5 = nodeBuffer.m_buffer[k];
					float num6 = VectorUtils.LengthXZ(nodePosition5.m_position - nodePosition4.m_position);
					float num7 = num6 * info.m_maxSlope;
					nodePosition5.m_minY = Mathf.Max(nodePosition5.m_minY, nodePosition4.m_minY - num7);
					nodePosition5.m_maxY = Mathf.Min(nodePosition5.m_maxY, nodePosition4.m_maxY + num7);
					nodeBuffer.m_buffer[k] = nodePosition5;
				}
				for (int l = 0; l < nodeBuffer.m_size; l++)
				{
					NetTool.NodePosition nodePosition6 = nodeBuffer.m_buffer[l];
					if (nodePosition6.m_minY > nodePosition6.m_maxY)
					{
						return ToolBase.ToolErrors.SlopeTooSteep;
					}
					if (nodePosition6.m_position.y > nodePosition6.m_maxY)
					{
						nodePosition6.m_position.y = nodePosition6.m_maxY;
						if (!flag && nodePosition6.m_elevation >= -8f)
						{
							nodePosition6.m_minY = nodePosition6.m_maxY;
						}
						flag3 = true;
					}
					else if (nodePosition6.m_position.y < nodePosition6.m_minY)
					{
						nodePosition6.m_position.y = nodePosition6.m_minY;
						if (flag || nodePosition6.m_elevation < -8f)
						{
							nodePosition6.m_maxY = nodePosition6.m_minY;
						}
						flag3 = true;
					}
					nodeBuffer.m_buffer[l] = nodePosition6;
				}
				if (num3++ == nodeBuffer.m_size << 1)
				{
					return ToolBase.ToolErrors.SlopeTooSteep;
				}
				if (!flag3)
				{
					goto Block_14;
				}
			}
			return ToolBase.ToolErrors.SlopeTooSteep;
			Block_14:
			for (int m = 1; m < nodeBuffer.m_size - 1; m++)
			{
				NetTool.NodePosition nodePosition7 = nodeBuffer.m_buffer[m - 1];
				NetTool.NodePosition nodePosition8 = nodeBuffer.m_buffer[m];
				float num8 = VectorUtils.LengthXZ(nodePosition8.m_position - nodePosition7.m_position);
				float num9 = num8 * info.m_maxSlope;
				if (flag || nodePosition8.m_elevation < -8f)
				{
					if (nodePosition8.m_position.y > nodePosition7.m_position.y + num9)
					{
						nodePosition8.m_position.y = nodePosition7.m_position.y + num9;
					}
				}
				else if (nodePosition8.m_position.y < nodePosition7.m_position.y - num9)
				{
					nodePosition8.m_position.y = nodePosition7.m_position.y - num9;
				}
				nodeBuffer.m_buffer[m] = nodePosition8;
			}
			for (int n = nodeBuffer.m_size - 2; n > 0; n--)
			{
				NetTool.NodePosition nodePosition9 = nodeBuffer.m_buffer[n + 1];
				NetTool.NodePosition nodePosition10 = nodeBuffer.m_buffer[n];
				float num10 = VectorUtils.LengthXZ(nodePosition10.m_position - nodePosition9.m_position);
				float num11 = num10 * info.m_maxSlope;
				if (flag || nodePosition10.m_elevation < -8f)
				{
					if (nodePosition10.m_position.y > nodePosition9.m_position.y + num11)
					{
						nodePosition10.m_position.y = nodePosition9.m_position.y + num11;
					}
				}
				else if (nodePosition10.m_position.y < nodePosition9.m_position.y - num11)
				{
					nodePosition10.m_position.y = nodePosition9.m_position.y - num11;
				}
				nodeBuffer.m_buffer[n] = nodePosition10;
			}
			int num12;
			int num13;
			info.m_netAI.GetElevationLimits(out num12, out num13);
			if (num13 > num12 && !flag)
			{
				int num15;
				for (int num14 = 0; num14 < nodeBuffer.m_size - 1; num14 = num15)
				{
					NetTool.NodePosition nodePosition11 = nodeBuffer.m_buffer[num14];
					num15 = num14 + 1;
					float num16 = 0f;
					bool flag4 = nodeBuffer.m_buffer[num15].m_position.y >= nodeBuffer.m_buffer[num15].m_terrainHeight + 8f;
					bool flag5 = nodeBuffer.m_buffer[num15].m_position.y <= nodeBuffer.m_buffer[num15].m_terrainHeight - 8f;
					if (!flag2)
					{
						flag5 = false;
					}
					if (flag4 || flag5)
					{
						while (num15 < nodeBuffer.m_size)
						{
							NetTool.NodePosition nodePosition12 = nodeBuffer.m_buffer[num15];
							num16 += VectorUtils.LengthXZ(nodePosition12.m_position - nodePosition11.m_position);
							if (flag4 && nodePosition12.m_position.y < nodePosition12.m_terrainHeight + 8f)
							{
								break;
							}
							if (flag5 && nodePosition12.m_position.y > nodePosition12.m_terrainHeight - 8f)
							{
								break;
							}
							nodePosition11 = nodePosition12;
							if (num15 == nodeBuffer.m_size - 1)
							{
								break;
							}
							num15++;
						}
					}
					float y3 = nodeBuffer.m_buffer[num14].m_position.y;
					float y4 = nodeBuffer.m_buffer[num15].m_position.y;
					nodePosition11 = nodeBuffer.m_buffer[num14];
					float num17 = 0f;
					num16 = Mathf.Max(1f, num16);
					for (int num18 = num14 + 1; num18 < num15; num18++)
					{
						NetTool.NodePosition nodePosition13 = nodeBuffer.m_buffer[num18];
						num17 += VectorUtils.LengthXZ(nodePosition13.m_position - nodePosition11.m_position);
						if (flag5)
						{
							nodePosition13.m_position.y = Mathf.Min(nodePosition13.m_position.y, Mathf.Lerp(y3, y4, num17 / num16));
						}
						else
						{
							nodePosition13.m_position.y = Mathf.Max(nodePosition13.m_position.y, Mathf.Lerp(y3, y4, num17 / num16));
						}
						nodeBuffer.m_buffer[num18] = nodePosition13;
						nodePosition11 = nodePosition13;
					}
				}
			}
		}
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		for (int num19 = 1; num19 < nodeBuffer.m_size - 1; num19++)
		{
			NetTool.NodePosition nodePosition14 = nodeBuffer.m_buffer[num19 - 1];
			NetTool.NodePosition nodePosition15 = nodeBuffer.m_buffer[num19 + 1];
			NetTool.NodePosition nodePosition16 = nodeBuffer.m_buffer[num19];
			if (flag)
			{
				if (nodePosition16.m_terrainHeight < nodePosition16.m_position.y)
				{
					toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
				}
			}
			else if (nodePosition16.m_elevation < -8f)
			{
				if (nodePosition16.m_terrainHeight <= nodePosition16.m_position.y + 8f)
				{
					toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
				}
			}
			else if (!flag2 && nodePosition16.m_terrainHeight > nodePosition16.m_position.y + 8f)
			{
				toolErrors |= ToolBase.ToolErrors.SlopeTooSteep;
			}
			nodePosition16.m_direction.y = VectorUtils.NormalizeXZ(nodePosition15.m_position - nodePosition14.m_position).y;
			nodeBuffer.m_buffer[num19] = nodePosition16;
		}
		return toolErrors;
	}

	public static ToolBase.ToolErrors CreateNode(NetInfo info, NetTool.ControlPoint startPoint, NetTool.ControlPoint middlePoint, NetTool.ControlPoint endPoint, FastList<NetTool.NodePosition> nodeBuffer, int maxSegments, bool test, bool visualize, bool autoFix, bool needMoney, bool invert, bool switchDir, ushort relocateBuildingID, out ushort node, out ushort segment, out int cost, out int productionRate)
	{
		ushort num;
		ushort num2;
		ToolBase.ToolErrors toolErrors = NetTool.CreateNode(info, startPoint, middlePoint, endPoint, nodeBuffer, maxSegments, test, true, visualize, autoFix, needMoney, invert, switchDir, relocateBuildingID, out num, out num2, out segment, out cost, out productionRate);
		if (toolErrors == ToolBase.ToolErrors.None)
		{
			if (num2 != 0)
			{
				node = num2;
			}
			else
			{
				node = num;
			}
		}
		else
		{
			node = 0;
		}
		return toolErrors;
	}

	public static ToolBase.ToolErrors CreateNode(NetInfo info, NetTool.ControlPoint startPoint, NetTool.ControlPoint middlePoint, NetTool.ControlPoint endPoint, FastList<NetTool.NodePosition> nodeBuffer, int maxSegments, bool test, bool testEnds, bool visualize, bool autoFix, bool needMoney, bool invert, bool switchDir, ushort relocateBuildingID, out ushort firstNode, out ushort lastNode, out ushort segment, out int cost, out int productionRate)
	{
		ushort num = middlePoint.m_segment;
		NetInfo oldInfo = null;
		if (startPoint.m_segment == num || endPoint.m_segment == num)
		{
			num = 0;
		}
		uint buildIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex;
		bool flag = invert;
		bool smoothStart = true;
		bool smoothEnd = true;
		bool enableDouble = false;
		NetSegment.Flags flags = NetSegment.Flags.None;
		ushort num2 = 0;
		string text = null;
		bool flag2 = false;
		if (num != 0)
		{
			enableDouble = (DefaultTool.FindSecondarySegment(num) != 0);
			maxSegments = Mathf.Min(1, maxSegments);
			cost = -Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info.m_netAI.GetConstructionCost(startPoint.m_position, endPoint.m_position, startPoint.m_elevation, endPoint.m_elevation);
			if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				cost /= 2;
			}
			buildIndex = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_buildIndex;
			smoothStart = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startPoint.m_node].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None);
			smoothEnd = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endPoint.m_node].m_flags & NetNode.Flags.Middle) != NetNode.Flags.None);
			autoFix = false;
			if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
			{
				info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info;
				switchDir = false;
			}
			else if (switchDir)
			{
				flag = !flag;
				info = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info;
			}
			if (!test && !visualize)
			{
				if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None)
				{
					flag = !flag;
				}
				flags = (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & (NetSegment.Flags.YieldStart | NetSegment.Flags.YieldEnd));
				if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.CustomName) != NetSegment.Flags.None)
				{
					InstanceID empty = InstanceID.Empty;
					empty.NetSegment = num;
					text = Singleton<InstanceManager>.get_instance().GetName(empty);
				}
				if ((Singleton<NetManager>.get_instance().m_adjustedSegments[num >> 6] & 1uL << (int)num) != 0uL)
				{
					flag2 = true;
				}
				num2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_nameSeed;
				if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
				{
					Singleton<NetManager>.get_instance().m_roadDestroyed.Deactivate();
					Singleton<NetManager>.get_instance().m_roadDestroyed2.Deactivate();
				}
				oldInfo = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].Info;
				Singleton<NetManager>.get_instance().ReleaseSegment(num, true);
				num = 0;
			}
		}
		else
		{
			if (autoFix && Singleton<SimulationManager>.get_instance().m_metaData.m_invertTraffic == SimulationMetaData.MetaBool.True)
			{
				flag = !flag;
			}
			cost = 0;
		}
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		ulong[] array = null;
		ulong[] array2 = null;
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if (test || !visualize)
		{
			properties.BeginColliding(out array, out array2);
		}
		ToolBase.ToolErrors result;
		try
		{
			ushort num3 = 0;
			BuildingInfo buildingInfo;
			Vector3 zero;
			Vector3 forward;
			if (num != 0 && switchDir)
			{
				buildingInfo = null;
				zero = Vector3.get_zero();
				forward = Vector3.get_forward();
				productionRate = 0;
				if (info.m_forwardVehicleLaneCount == info.m_backwardVehicleLaneCount)
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
			}
			else
			{
				toolErrors |= info.m_netAI.CheckBuildPosition(test, visualize, false, autoFix, ref startPoint, ref middlePoint, ref endPoint, out buildingInfo, out zero, out forward, out productionRate);
			}
			if (test)
			{
				Vector3 direction = middlePoint.m_direction;
				Vector3 direction2 = -endPoint.m_direction;
				if (maxSegments != 0 && num == 0 && direction.x * direction2.x + direction.z * direction2.z >= 0.8f)
				{
					toolErrors |= ToolBase.ToolErrors.InvalidShape;
				}
				if (maxSegments != 0 && !NetTool.CheckStartAndEnd(num, startPoint.m_segment, startPoint.m_node, endPoint.m_segment, endPoint.m_node, array))
				{
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (startPoint.m_node != 0)
				{
					if (maxSegments != 0 && !NetTool.CanAddSegment(startPoint.m_node, direction, array, num))
					{
						toolErrors |= ToolBase.ToolErrors.ObjectCollision;
					}
				}
				else if (startPoint.m_segment != 0 && !NetTool.CanAddNode(startPoint.m_segment, startPoint.m_position, direction, maxSegments != 0, array))
				{
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (endPoint.m_node != 0)
				{
					if (maxSegments != 0 && !NetTool.CanAddSegment(endPoint.m_node, direction2, array, num))
					{
						toolErrors |= ToolBase.ToolErrors.ObjectCollision;
					}
				}
				else if (endPoint.m_segment != 0 && !NetTool.CanAddNode(endPoint.m_segment, endPoint.m_position, direction2, maxSegments != 0, array))
				{
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (!Singleton<NetManager>.get_instance().CheckLimits())
				{
					toolErrors |= ToolBase.ToolErrors.TooManyObjects;
				}
			}
			if (buildingInfo != null)
			{
				if (visualize)
				{
					NetTool.RenderNodeBuilding(buildingInfo, zero, forward);
				}
				else if (test)
				{
					toolErrors |= NetTool.TestNodeBuilding(buildingInfo, zero, forward, 0, 0, 0, test, array, array2);
				}
				else
				{
					float angle = Mathf.Atan2(-forward.x, forward.z);
					if (Singleton<BuildingManager>.get_instance().CreateBuilding(out num3, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingInfo, zero, angle, 0, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
					{
						Building[] expr_5D8_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
						ushort expr_5D8_cp_1 = num3;
						expr_5D8_cp_0[(int)expr_5D8_cp_1].m_flags = (expr_5D8_cp_0[(int)expr_5D8_cp_1].m_flags | Building.Flags.FixedHeight);
						Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
					}
				}
			}
			bool flag3 = middlePoint.m_direction.x * endPoint.m_direction.x + middlePoint.m_direction.z * endPoint.m_direction.z <= 0.999f;
			Vector2 vector;
			vector..ctor(startPoint.m_position.x - middlePoint.m_position.x, startPoint.m_position.z - middlePoint.m_position.z);
			float magnitude = vector.get_magnitude();
			Vector2 vector2;
			vector2..ctor(middlePoint.m_position.x - endPoint.m_position.x, middlePoint.m_position.z - endPoint.m_position.z);
			float magnitude2 = vector2.get_magnitude();
			float num4 = magnitude + magnitude2;
			if (test && maxSegments != 0)
			{
				float minSegmentLength = info.m_netAI.GetMinSegmentLength();
				if (flag3 && num == 0)
				{
					if (magnitude < minSegmentLength)
					{
						toolErrors |= ToolBase.ToolErrors.TooShort;
					}
					if (magnitude2 < minSegmentLength)
					{
						toolErrors |= ToolBase.ToolErrors.TooShort;
					}
				}
				else if (num4 < minSegmentLength)
				{
					toolErrors |= ToolBase.ToolErrors.TooShort;
				}
			}
			segment = 0;
			int num5 = Mathf.Min(maxSegments, Mathf.FloorToInt(num4 / 100f) + 1);
			if (num5 >= 2)
			{
				enableDouble = true;
			}
			ushort num6 = startPoint.m_node;
			Vector3 vector3 = startPoint.m_position;
			Vector3 vector4 = middlePoint.m_direction;
			Vector3 vector5;
			Vector3 vector6;
			NetSegment.CalculateMiddlePoints(startPoint.m_position, middlePoint.m_direction, endPoint.m_position, -endPoint.m_direction, smoothStart, smoothEnd, out vector5, out vector6);
			nodeBuffer.Clear();
			NetTool.NodePosition item;
			item.m_position = vector3;
			item.m_direction = vector4;
			item.m_minY = vector3.y;
			item.m_maxY = vector3.y;
			item.m_terrainHeight = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(item.m_position);
			if (!info.m_blockWater)
			{
				float num7 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(item.m_position);
				if (num7 < item.m_terrainHeight - 15f)
				{
					item.m_terrainHeight = num7;
				}
			}
			item.m_elevation = startPoint.m_elevation;
			item.m_double = false;
			if (startPoint.m_node != 0)
			{
				item.m_double = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startPoint.m_node].m_flags & NetNode.Flags.Double) != NetNode.Flags.None);
			}
			else if (info.m_netAI.RequireDoubleSegments() && maxSegments <= 1)
			{
				item.m_double = true;
			}
			item.m_nodeInfo = null;
			nodeBuffer.Add(item);
			for (int i = 1; i <= num5; i++)
			{
				item.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, (float)i / (float)num5);
				if (i != num5 && item.m_elevation > 0.1f)
				{
					if (startPoint.m_elevation > -0.1f && startPoint.m_elevation < 0.1f)
					{
						int num8 = Mathf.Clamp(num5 - Mathf.FloorToInt((endPoint.m_elevation - startPoint.m_elevation) / 8f), 0, num5 - 1);
						item.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, Mathf.Clamp01((float)(i - num8) / (float)(num5 - num8)));
					}
					else if (endPoint.m_elevation > -0.1f && endPoint.m_elevation < 0.1f)
					{
						int num9 = Mathf.Clamp(Mathf.FloorToInt((startPoint.m_elevation - endPoint.m_elevation) / 8f), 1, num5);
						item.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, Mathf.Clamp01((float)i / (float)num9));
					}
				}
				else if (i != num5 && item.m_elevation < -0.1f)
				{
					if (startPoint.m_elevation > -0.1f && startPoint.m_elevation < 0.1f)
					{
						int num10 = Mathf.Clamp(num5 - Mathf.FloorToInt((startPoint.m_elevation - endPoint.m_elevation) / 8f), 0, num5 - 1);
						item.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, Mathf.Clamp01((float)(i - num10) / (float)(num5 - num10)));
					}
					else if (endPoint.m_elevation > -0.1f && endPoint.m_elevation < 0.1f)
					{
						int num11 = Mathf.Clamp(Mathf.FloorToInt((endPoint.m_elevation - startPoint.m_elevation) / 8f), 1, num5);
						item.m_elevation = Mathf.Lerp(startPoint.m_elevation, endPoint.m_elevation, Mathf.Clamp01((float)i / (float)num11));
					}
				}
				item.m_double = false;
				if (i == num5)
				{
					item.m_position = endPoint.m_position;
					item.m_direction = endPoint.m_direction;
					item.m_minY = endPoint.m_position.y;
					item.m_maxY = endPoint.m_position.y;
					if (endPoint.m_node != 0)
					{
						item.m_double = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endPoint.m_node].m_flags & NetNode.Flags.Double) != NetNode.Flags.None);
					}
				}
				else if (flag3)
				{
					item.m_position = Bezier3.Position(startPoint.m_position, vector5, vector6, endPoint.m_position, (float)i / (float)num5);
					item.m_direction = Bezier3.Tangent(startPoint.m_position, vector5, vector6, endPoint.m_position, (float)i / (float)num5);
					item.m_position.y = NetSegment.SampleTerrainHeight(info, item.m_position, visualize, item.m_elevation);
					item.m_direction = VectorUtils.NormalizeXZ(item.m_direction);
					item.m_minY = 0f;
					item.m_maxY = 1280f;
				}
				else
				{
					float lengthSnap = info.m_netAI.GetLengthSnap();
					item.m_position = NetTool.LerpPosition(startPoint.m_position, endPoint.m_position, (float)i / (float)num5, lengthSnap);
					item.m_direction = endPoint.m_direction;
					item.m_position.y = NetSegment.SampleTerrainHeight(info, item.m_position, visualize, item.m_elevation);
					item.m_minY = 0f;
					item.m_maxY = 1280f;
				}
				item.m_terrainHeight = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(item.m_position);
				if (!info.m_blockWater)
				{
					float num12 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(item.m_position);
					if (num12 < item.m_terrainHeight - 15f)
					{
						item.m_terrainHeight = num12;
					}
				}
				nodeBuffer.Add(item);
			}
			ToolBase.ToolErrors toolErrors2 = NetTool.CheckNodeHeights(info, nodeBuffer);
			if (toolErrors2 != ToolBase.ToolErrors.None && test)
			{
				toolErrors |= toolErrors2;
			}
			float num13 = nodeBuffer.m_buffer[0].m_position.y - nodeBuffer.m_buffer[0].m_terrainHeight;
			if (num13 > 0f && ((num5 >= 1 && nodeBuffer.m_buffer[1].m_position.y - nodeBuffer.m_buffer[1].m_terrainHeight < -8f) || (num13 < 11f && nodeBuffer.m_buffer[0].m_elevation < 1f)))
			{
				num13 = 0f;
				nodeBuffer.m_buffer[0].m_terrainHeight = nodeBuffer.m_buffer[0].m_position.y;
			}
			if (autoFix || !info.m_useFixedHeight)
			{
				nodeBuffer.m_buffer[0].m_nodeInfo = info.m_netAI.GetInfo(num13, num13, num4, startPoint.m_outside, false, flag3, enableDouble, ref toolErrors);
			}
			else
			{
				nodeBuffer.m_buffer[0].m_nodeInfo = info;
			}
			for (int j = 1; j <= num5; j++)
			{
				num13 = nodeBuffer.m_buffer[j].m_position.y - nodeBuffer.m_buffer[j].m_terrainHeight;
				if (num13 > 0f && (nodeBuffer.m_buffer[j - 1].m_position.y - nodeBuffer.m_buffer[j - 1].m_terrainHeight < -8f || (num5 > j && nodeBuffer.m_buffer[j + 1].m_position.y - nodeBuffer.m_buffer[j + 1].m_terrainHeight < -8f) || (num13 < 11f && nodeBuffer.m_buffer[j].m_elevation < 1f)))
				{
					num13 = 0f;
					nodeBuffer.m_buffer[j].m_terrainHeight = nodeBuffer.m_buffer[j].m_position.y;
				}
				if (autoFix || !info.m_useFixedHeight)
				{
					nodeBuffer.m_buffer[j].m_nodeInfo = info.m_netAI.GetInfo(num13, num13, num4, false, j == num5 && endPoint.m_outside, flag3, enableDouble, ref toolErrors);
				}
				else
				{
					nodeBuffer.m_buffer[j].m_nodeInfo = info;
				}
			}
			int k = 1;
			int num14 = 0;
			NetInfo netInfo = null;
			while (k <= num5)
			{
				NetInfo nodeInfo = nodeBuffer.m_buffer[k].m_nodeInfo;
				if (k != num5 && nodeInfo == netInfo)
				{
					num14++;
					k++;
				}
				else
				{
					if (num14 != 0 && netInfo.m_netAI.RequireDoubleSegments())
					{
						int num15 = k - num14 - 1;
						int num16 = k;
						if ((num14 & 1) == 0)
						{
							nodeBuffer.RemoveAt(k - 1);
							num5--;
							num16--;
							for (int l = num15 + 1; l < num16; l++)
							{
								int num17 = num15;
								int num18 = num16;
								if (netInfo.m_netAI.FlattenOnlyOneDoubleSegment())
								{
									num17 = l - (l - num15 & 1);
									num18 = num17 + 2;
								}
								float num19 = (float)(l - num17) / (float)(num18 - num17);
								nodeBuffer.m_buffer[l].m_position = Vector3.Lerp(nodeBuffer.m_buffer[num17].m_position, nodeBuffer.m_buffer[num18].m_position, num19);
								nodeBuffer.m_buffer[l].m_direction = VectorUtils.NormalizeXZ(Vector3.Lerp(nodeBuffer.m_buffer[num17].m_direction, nodeBuffer.m_buffer[num18].m_direction, num19));
								nodeBuffer.m_buffer[l].m_elevation = Mathf.Lerp(nodeBuffer.m_buffer[num17].m_elevation, nodeBuffer.m_buffer[num18].m_elevation, num19);
								nodeBuffer.m_buffer[l].m_terrainHeight = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(nodeBuffer.m_buffer[l].m_position);
								if (!info.m_blockWater)
								{
									float num20 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(nodeBuffer.m_buffer[l].m_position);
									if (num20 < nodeBuffer.m_buffer[l].m_terrainHeight - 15f)
									{
										nodeBuffer.m_buffer[l].m_terrainHeight = num20;
									}
								}
							}
						}
						else
						{
							for (int m = num15 + 1; m < num16; m++)
							{
								int num21 = num15;
								int num22 = num16;
								if (netInfo.m_netAI.FlattenOnlyOneDoubleSegment())
								{
									num21 = m - (m - num15 & 1);
									num22 = num21 + 2;
								}
								float num23 = (float)(m - num21) / (float)(num22 - num21);
								nodeBuffer.m_buffer[m].m_position = Vector3.Lerp(nodeBuffer.m_buffer[num21].m_position, nodeBuffer.m_buffer[num22].m_position, num23);
								nodeBuffer.m_buffer[m].m_direction = VectorUtils.NormalizeXZ(Vector3.Lerp(nodeBuffer.m_buffer[num21].m_direction, nodeBuffer.m_buffer[num22].m_direction, num23));
								nodeBuffer.m_buffer[m].m_elevation = Mathf.Lerp(nodeBuffer.m_buffer[num21].m_elevation, nodeBuffer.m_buffer[num22].m_elevation, num23);
								nodeBuffer.m_buffer[m].m_terrainHeight = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(nodeBuffer.m_buffer[m].m_position);
								if (!info.m_blockWater)
								{
									float num24 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(nodeBuffer.m_buffer[m].m_position);
									if (num24 < nodeBuffer.m_buffer[m].m_terrainHeight - 15f)
									{
										nodeBuffer.m_buffer[m].m_terrainHeight = num24;
									}
								}
							}
							k++;
						}
						for (int n = num15 + 1; n < num16; n++)
						{
							NetTool.NodePosition[] expr_13D1_cp_0 = nodeBuffer.m_buffer;
							int expr_13D1_cp_1 = n;
							expr_13D1_cp_0[expr_13D1_cp_1].m_double = (expr_13D1_cp_0[expr_13D1_cp_1].m_double | (n - num15 & 1) == 1);
						}
					}
					else
					{
						k++;
					}
					num14 = 1;
				}
				netInfo = nodeInfo;
			}
			bool flag4 = true;
			bool flag5 = true;
			if (startPoint.m_node != 0)
			{
				flag4 = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)startPoint.m_node].m_flags & NetNode.Flags.Underground) != NetNode.Flags.None);
			}
			if (endPoint.m_node != 0)
			{
				flag5 = ((Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)endPoint.m_node].m_flags & NetNode.Flags.Underground) != NetNode.Flags.None);
			}
			NetInfo nodeInfo2 = nodeBuffer[0].m_nodeInfo;
			bool flag6 = false;
			if (num6 == 0 && !test && !visualize)
			{
				if (startPoint.m_segment != 0)
				{
					if (NetTool.SplitSegment(startPoint.m_segment, out num6, vector3))
					{
						flag6 = true;
					}
					startPoint.m_segment = 0;
				}
				else if (Singleton<NetManager>.get_instance().CreateNode(out num6, ref Singleton<SimulationManager>.get_instance().m_randomizer, nodeInfo2, vector3, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
				{
					if (startPoint.m_outside)
					{
						NetNode[] expr_1532_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
						ushort expr_1532_cp_1 = num6;
						expr_1532_cp_0[(int)expr_1532_cp_1].m_flags = (expr_1532_cp_0[(int)expr_1532_cp_1].m_flags | NetNode.Flags.Outside);
					}
					if (vector3.y - nodeBuffer.m_buffer[0].m_terrainHeight < -8f && (nodeInfo2.m_netAI.SupportUnderground() || nodeInfo2.m_netAI.IsUnderground()))
					{
						NetNode[] expr_159F_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
						ushort expr_159F_cp_1 = num6;
						expr_159F_cp_0[(int)expr_159F_cp_1].m_flags = (expr_159F_cp_0[(int)expr_159F_cp_1].m_flags | NetNode.Flags.Underground);
					}
					if (nodeBuffer.m_buffer[0].m_double)
					{
						NetNode[] expr_15DD_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
						ushort expr_15DD_cp_1 = num6;
						expr_15DD_cp_0[(int)expr_15DD_cp_1].m_flags = (expr_15DD_cp_0[(int)expr_15DD_cp_1].m_flags | NetNode.Flags.Double);
					}
					if (nodeInfo2.m_netAI.IsUnderground())
					{
						Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(-nodeBuffer[0].m_elevation), 0, 255);
					}
					else if (nodeInfo2.m_netAI.IsOverground() && (nodeBuffer[0].m_elevation > 0.1f || nodeBuffer[0].m_position.y - nodeBuffer[0].m_terrainHeight > 0.1f))
					{
						Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(nodeBuffer[0].m_elevation), 1, 255);
					}
					else
					{
						NetNode[] expr_16F8_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
						ushort expr_16F8_cp_1 = num6;
						expr_16F8_cp_0[(int)expr_16F8_cp_1].m_flags = (expr_16F8_cp_0[(int)expr_16F8_cp_1].m_flags | NetNode.Flags.OnGround);
					}
					Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
					flag6 = true;
				}
				startPoint.m_node = num6;
			}
			NetNode netNode = default(NetNode);
			netNode.m_position = vector3;
			if (nodeBuffer.m_buffer[0].m_double)
			{
				netNode.m_flags |= NetNode.Flags.Double;
			}
			if (maxSegments == 0)
			{
				netNode.m_flags |= NetNode.Flags.End;
			}
			if (vector3.y - nodeBuffer.m_buffer[0].m_terrainHeight < -8f && flag4 && (nodeInfo2.m_netAI.SupportUnderground() || nodeInfo2.m_netAI.IsUnderground()))
			{
				netNode.m_flags |= NetNode.Flags.Underground;
			}
			else if (!nodeInfo2.m_netAI.IsOverground() || nodeBuffer[0].m_position.y - nodeBuffer[0].m_terrainHeight <= 0.1f)
			{
				netNode.m_flags |= NetNode.Flags.OnGround;
			}
			else
			{
				netNode.m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(nodeBuffer[0].m_elevation), 1, 255);
			}
			if (startPoint.m_outside)
			{
				netNode.m_flags |= NetNode.Flags.Outside;
			}
			BuildingInfo buildingInfo2;
			float num25;
			nodeInfo2.m_netAI.GetNodeBuilding(0, ref netNode, out buildingInfo2, out num25);
			if (visualize)
			{
				if (buildingInfo2 != null && (num6 == 0 || num != 0))
				{
					Vector3 position = vector3;
					position.y += num25;
					NetTool.RenderNodeBuilding(buildingInfo2, position, vector4);
				}
				if (nodeInfo2.m_netAI.DisplayTempSegment())
				{
					NetTool.RenderNode(nodeInfo2, vector3, vector4);
				}
			}
			else if (buildingInfo2 != null && (netNode.m_flags & NetNode.Flags.Outside) == NetNode.Flags.None)
			{
				ushort node = startPoint.m_node;
				ushort segment2 = startPoint.m_segment;
				ushort ignoredBuilding = NetTool.GetIgnoredBuilding(startPoint);
				toolErrors2 = NetTool.TestNodeBuilding(buildingInfo2, vector3, vector4, node, segment2, ignoredBuilding, test, array, array2);
				if (test && toolErrors2 != ToolBase.ToolErrors.None)
				{
					toolErrors |= toolErrors2;
				}
			}
			if (num3 != 0 && num6 != 0 && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_flags & NetNode.Flags.Untouchable) == NetNode.Flags.None)
			{
				NetNode[] expr_199A_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
				ushort expr_199A_cp_1 = num6;
				expr_199A_cp_0[(int)expr_199A_cp_1].m_flags = (expr_199A_cp_0[(int)expr_199A_cp_1].m_flags | NetNode.Flags.Untouchable);
				Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_nextBuildingNode = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num3].m_netNode;
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num3].m_netNode = num6;
			}
			for (int num26 = 1; num26 <= num5; num26++)
			{
				Vector3 position2 = nodeBuffer[num26].m_position;
				Vector3 vector7 = nodeBuffer[num26].m_direction;
				Vector3 vector8;
				Vector3 vector9;
				NetSegment.CalculateMiddlePoints(vector3, vector4, position2, -vector7, smoothStart, smoothEnd, out vector8, out vector9);
				nodeInfo2 = nodeBuffer.m_buffer[num26].m_nodeInfo;
				float num27 = nodeBuffer[num26 - 1].m_position.y - nodeBuffer[num26 - 1].m_terrainHeight;
				float num28 = nodeBuffer[num26].m_position.y - nodeBuffer[num26].m_terrainHeight;
				if (num26 == 1 && !flag4 && num27 < 0f)
				{
					num27 = 0f;
				}
				if (num26 == num5 && !flag5 && num28 < 0f)
				{
					num28 = 0f;
				}
				NetNode netNode2 = netNode;
				NetInfo netInfo2;
				if (nodeBuffer.m_buffer[num26].m_double)
				{
					netInfo2 = nodeBuffer.m_buffer[num26].m_nodeInfo;
					netNode.m_flags |= NetNode.Flags.Double;
				}
				else if (nodeBuffer.m_buffer[num26 - 1].m_double)
				{
					netInfo2 = nodeBuffer.m_buffer[num26 - 1].m_nodeInfo;
					netNode.m_flags &= ~NetNode.Flags.Double;
				}
				else
				{
					float minElevation = Mathf.Min(num28, num27);
					float num29 = Mathf.Max(num28, num27);
					if (num29 >= -8f)
					{
						for (int num30 = 1; num30 < 8; num30++)
						{
							Vector3 worldPos = Bezier3.Position(vector3, vector8, vector9, position2, (float)num30 / 8f);
							float num31 = Singleton<TerrainManager>.get_instance().SampleRawHeightSmooth(worldPos);
							if (!info.m_blockWater)
							{
								float num32 = Singleton<TerrainManager>.get_instance().SampleBlockHeightSmooth(worldPos);
								if (num32 < num31 - 15f)
								{
									num31 = num32;
								}
							}
							num13 = worldPos.y - num31;
							num29 = Mathf.Max(num29, num13);
						}
					}
					if (num28 > -0.1f && num28 < 0.1f && num27 > -0.1f && num27 < 0.1f && num29 < 11f)
					{
						num29 = 0f;
					}
					else if (num28 < 0.1f && num27 < 0.1f && num29 > 0f && num29 < 11f)
					{
						num29 = 0f;
					}
					if (autoFix || !info.m_useFixedHeight)
					{
						netInfo2 = info.m_netAI.GetInfo(minElevation, num29, num4, num26 == 1 && startPoint.m_outside, num26 == num5 && endPoint.m_outside, flag3, false, ref toolErrors);
					}
					else
					{
						netInfo2 = info;
					}
					netNode.m_flags &= ~NetNode.Flags.Double;
				}
				bool flag7 = (netNode.m_flags & NetNode.Flags.Underground) != NetNode.Flags.None;
				bool flag8 = !flag7;
				netNode.m_position = position2;
				if (num28 < -8f && (num26 != num5 || flag5) && (nodeInfo2.m_netAI.SupportUnderground() || nodeInfo2.m_netAI.IsUnderground()))
				{
					netNode.m_elevation = 0;
					netNode.m_flags |= NetNode.Flags.Underground;
					netNode.m_flags &= ~NetNode.Flags.OnGround;
					flag7 = false;
				}
				else if (!nodeInfo2.m_netAI.IsOverground() || num28 <= 0.1f)
				{
					netNode.m_elevation = 0;
					netNode.m_flags |= NetNode.Flags.OnGround;
					netNode.m_flags &= ~NetNode.Flags.Underground;
					flag8 = false;
				}
				else
				{
					netNode.m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(nodeBuffer[num26].m_elevation), 1, 255);
					netNode.m_flags &= ~NetNode.Flags.OnGround;
					netNode.m_flags &= ~NetNode.Flags.Underground;
					flag8 = false;
				}
				if (num26 == num5 && endPoint.m_outside)
				{
					netNode.m_flags |= NetNode.Flags.Outside;
				}
				else
				{
					netNode.m_flags &= ~NetNode.Flags.Outside;
				}
				nodeInfo2.m_netAI.GetNodeBuilding(0, ref netNode, out buildingInfo2, out num25);
				if (visualize)
				{
					if (buildingInfo2 != null && (num26 != num5 || endPoint.m_node == 0 || num != 0))
					{
						Vector3 position3 = position2;
						position3.y += num25;
						NetTool.RenderNodeBuilding(buildingInfo2, position3, vector7);
					}
					if (netInfo2.m_netAI.DisplayTempSegment())
					{
						if (nodeBuffer.m_buffer[num26].m_double || flag8)
						{
							NetSegment.Flags flags2 = netInfo2.m_netAI.UpdateSegmentFlags(ref netNode, ref netNode2);
							NetTool.RenderSegment(netInfo2, flags2, position2, vector3, -vector7, -vector4, smoothStart, smoothEnd);
						}
						else
						{
							NetSegment.Flags flags3 = netInfo2.m_netAI.UpdateSegmentFlags(ref netNode2, ref netNode);
							NetTool.RenderSegment(netInfo2, flags3, vector3, position2, vector4, vector7, smoothStart, smoothEnd);
						}
					}
				}
				else
				{
					if (netInfo2.m_canCollide)
					{
						ItemClass.CollisionType collisionType = netInfo2.m_netAI.GetCollisionType();
						ItemClass.Layer collisionLayers = netInfo2.m_netAI.GetCollisionLayers();
						bool flag9 = netInfo2.m_placementStyle == ItemClass.Placement.Procedural && !netInfo2.m_overlayVisible;
						float collisionHalfWidth = netInfo2.m_netAI.GetCollisionHalfWidth();
						int num33 = Mathf.Max(2, 16 / num5);
						Vector3 vector10 = new Vector3(vector4.z, 0f, -vector4.x) * collisionHalfWidth;
						Quad3 quad = default(Quad3);
						quad.a = vector3 - vector10;
						quad.d = vector3 + vector10;
						float endRadius = netInfo2.m_netAI.GetEndRadius();
						if (num26 == 1 && num == 0 && netInfo2.m_clipSegmentEnds && endRadius != 0f && startPoint.m_node == 0 && startPoint.m_segment == 0)
						{
							Vector3 vector11 = vector3;
							vector11.x -= vector10.x * 0.8f - vector10.z * 0.6f * endRadius / collisionHalfWidth;
							vector11.z -= vector10.z * 0.8f + vector10.x * 0.6f * endRadius / collisionHalfWidth;
							Vector3 vector12 = vector3;
							vector12.x += vector10.x * 0.8f + vector10.z * 0.6f * endRadius / collisionHalfWidth;
							vector12.z += vector10.z * 0.8f - vector10.x * 0.6f * endRadius / collisionHalfWidth;
							Vector3 vector13 = vector3;
							vector13.x -= vector10.x * 0.3f - vector10.z * endRadius / collisionHalfWidth;
							vector13.z -= vector10.z * 0.3f + vector10.x * endRadius / collisionHalfWidth;
							Vector3 vector14 = vector3;
							vector14.x += vector10.x * 0.3f + vector10.z * endRadius / collisionHalfWidth;
							vector14.z += vector10.z * 0.3f - vector10.x * endRadius / collisionHalfWidth;
							float minY = vector3.y + netInfo2.m_minHeight;
							float maxY = vector3.y + netInfo2.m_maxHeight;
							Quad2 quad2 = Quad2.XZ(quad.a, quad.d, vector12, vector11);
							Quad2 quad3 = Quad2.XZ(vector11, vector12, vector14, vector13);
							Singleton<NetManager>.get_instance().OverlapQuad(quad2, minY, maxY, collisionType, collisionLayers, startPoint.m_node, 0, startPoint.m_segment, array);
							Singleton<NetManager>.get_instance().OverlapQuad(quad3, minY, maxY, collisionType, collisionLayers, startPoint.m_node, 0, startPoint.m_segment, array);
							Singleton<BuildingManager>.get_instance().OverlapQuad(quad2, minY, maxY, collisionType, collisionLayers, NetTool.GetIgnoredBuilding(startPoint), startPoint.m_node, 0, array2);
							Singleton<BuildingManager>.get_instance().OverlapQuad(quad3, minY, maxY, collisionType, collisionLayers, NetTool.GetIgnoredBuilding(startPoint), startPoint.m_node, 0, array2);
							if (test)
							{
								bool flag10 = flag9 || startPoint.m_outside;
								if ((properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
								{
									float num34 = 256f;
									if (quad2.a.x < -num34 || quad2.a.x > num34 || quad2.a.y < -num34 || quad2.a.y > num34)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad2.b.x < -num34 || quad2.b.x > num34 || quad2.b.y < -num34 || quad2.b.y > num34)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad2.c.x < -num34 || quad2.c.x > num34 || quad2.c.y < -num34 || quad2.c.y > num34)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad2.d.x < -num34 || quad2.d.x > num34 || quad2.d.y < -num34 || quad2.d.y > num34)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
								}
								else if (!flag10 && Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad2))
								{
									toolErrors |= ToolBase.ToolErrors.OutOfArea;
								}
								if ((properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
								{
									float num35 = 256f;
									if (quad3.a.x < -num35 || quad3.a.x > num35 || quad3.a.y < -num35 || quad3.a.y > num35)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad3.b.x < -num35 || quad3.b.x > num35 || quad3.b.y < -num35 || quad3.b.y > num35)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad3.c.x < -num35 || quad3.c.x > num35 || quad3.c.y < -num35 || quad3.c.y > num35)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad3.d.x < -num35 || quad3.d.x > num35 || quad3.d.y < -num35 || quad3.d.y > num35)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
								}
								else if (!flag10 && Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad3))
								{
									toolErrors |= ToolBase.ToolErrors.OutOfArea;
								}
							}
						}
						for (int num36 = 1; num36 <= num33; num36++)
						{
							ushort ignoreNode = 0;
							ushort ignoreNode2 = 0;
							ushort ignoreSegment = 0;
							ushort ignoreBuilding = 0;
							bool flag11 = flag9;
							if (num != 0 && num5 == 1)
							{
								ignoreNode = startPoint.m_node;
								ignoreNode2 = endPoint.m_node;
								if (num36 << 1 > num33)
								{
									ignoreBuilding = NetTool.GetIgnoredBuilding(endPoint);
								}
								else
								{
									ignoreBuilding = NetTool.GetIgnoredBuilding(startPoint);
								}
							}
							else if (num26 == 1 && num36 - 1 << 1 < num33)
							{
								ignoreNode = startPoint.m_node;
								if (num26 == num5 && num36 << 1 >= num33)
								{
									ignoreNode2 = endPoint.m_node;
								}
								ignoreSegment = startPoint.m_segment;
								ignoreBuilding = NetTool.GetIgnoredBuilding(startPoint);
								flag11 |= startPoint.m_outside;
							}
							else if (num26 == num5 && num36 << 1 > num33)
							{
								ignoreNode = endPoint.m_node;
								if (num26 == 1 && num36 - 1 << 1 <= num33)
								{
									ignoreNode2 = startPoint.m_node;
								}
								ignoreSegment = endPoint.m_segment;
								ignoreBuilding = NetTool.GetIgnoredBuilding(endPoint);
								flag11 |= endPoint.m_outside;
							}
							else if (num36 - 1 << 1 < num33)
							{
								ignoreNode = num6;
							}
							Vector3 vector15 = Bezier3.Position(vector3, vector8, vector9, position2, (float)num36 / (float)num33);
							vector10 = Bezier3.Tangent(vector3, vector8, vector9, position2, (float)num36 / (float)num33);
							Vector3 vector16;
							vector16..ctor(vector10.z, 0f, -vector10.x);
							vector10 = vector16.get_normalized() * collisionHalfWidth;
							quad.b = vector15 - vector10;
							quad.c = vector15 + vector10;
							float minY2 = Mathf.Min(Mathf.Min(quad.a.y, quad.b.y), Mathf.Min(quad.c.y, quad.d.y)) + netInfo2.m_minHeight;
							float maxY2 = Mathf.Max(Mathf.Max(quad.a.y, quad.b.y), Mathf.Max(quad.c.y, quad.d.y)) + netInfo2.m_maxHeight;
							Quad2 quad4 = Quad2.XZ(quad);
							Singleton<NetManager>.get_instance().OverlapQuad(quad4, minY2, maxY2, collisionType, collisionLayers, ignoreNode, ignoreNode2, ignoreSegment, array);
							Singleton<BuildingManager>.get_instance().OverlapQuad(quad4, minY2, maxY2, collisionType, collisionLayers, ignoreBuilding, ignoreNode, ignoreNode2, array2);
							if (test)
							{
								if ((properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
								{
									float num37 = 256f;
									if (quad4.a.x < -num37 || quad4.a.x > num37 || quad4.a.y < -num37 || quad4.a.y > num37)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad4.b.x < -num37 || quad4.b.x > num37 || quad4.b.y < -num37 || quad4.b.y > num37)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad4.c.x < -num37 || quad4.c.x > num37 || quad4.c.y < -num37 || quad4.c.y > num37)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad4.d.x < -num37 || quad4.d.x > num37 || quad4.d.y < -num37 || quad4.d.y > num37)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
								}
								else if (!flag11 && Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad4))
								{
									toolErrors |= ToolBase.ToolErrors.OutOfArea;
								}
							}
							quad.a = quad.b;
							quad.d = quad.c;
						}
						if (num26 == num5 && num == 0 && netInfo2.m_clipSegmentEnds && endRadius != 0f && endPoint.m_node == 0 && endPoint.m_segment == 0)
						{
							Vector3 vector17 = position2;
							vector17.x -= vector10.x * 0.8f + vector10.z * 0.6f * endRadius / collisionHalfWidth;
							vector17.z -= vector10.z * 0.8f - vector10.x * 0.6f * endRadius / collisionHalfWidth;
							Vector3 vector18 = position2;
							vector18.x += vector10.x * 0.8f - vector10.z * 0.6f * endRadius / collisionHalfWidth;
							vector18.z += vector10.z * 0.8f + vector10.x * 0.6f * endRadius / collisionHalfWidth;
							Vector3 vector19 = position2;
							vector19.x -= vector10.x * 0.3f + vector10.z * endRadius / collisionHalfWidth;
							vector19.z -= vector10.z * 0.3f - vector10.x * endRadius / collisionHalfWidth;
							Vector3 vector20 = position2;
							vector20.x += vector10.x * 0.3f - vector10.z * endRadius / collisionHalfWidth;
							vector20.z += vector10.z * 0.3f + vector10.x * endRadius / collisionHalfWidth;
							float minY3 = position2.y + netInfo2.m_minHeight;
							float maxY3 = position2.y + netInfo2.m_maxHeight;
							Quad2 quad5 = Quad2.XZ(quad.a, vector17, vector18, quad.d);
							Quad2 quad6 = Quad2.XZ(vector17, vector19, vector20, vector18);
							Singleton<NetManager>.get_instance().OverlapQuad(quad5, minY3, maxY3, collisionType, collisionLayers, endPoint.m_node, 0, endPoint.m_segment, array);
							Singleton<NetManager>.get_instance().OverlapQuad(quad6, minY3, maxY3, collisionType, collisionLayers, endPoint.m_node, 0, endPoint.m_segment, array);
							Singleton<BuildingManager>.get_instance().OverlapQuad(quad5, minY3, maxY3, collisionType, collisionLayers, NetTool.GetIgnoredBuilding(endPoint), endPoint.m_node, 0, array2);
							Singleton<BuildingManager>.get_instance().OverlapQuad(quad6, minY3, maxY3, collisionType, collisionLayers, NetTool.GetIgnoredBuilding(endPoint), endPoint.m_node, 0, array2);
							if (test)
							{
								bool flag12 = flag9 || endPoint.m_outside;
								if ((properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
								{
									float num38 = 256f;
									if (quad5.a.x < -num38 || quad5.a.x > num38 || quad5.a.y < -num38 || quad5.a.y > num38)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad5.b.x < -num38 || quad5.b.x > num38 || quad5.b.y < -num38 || quad5.b.y > num38)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad5.c.x < -num38 || quad5.c.x > num38 || quad5.c.y < -num38 || quad5.c.y > num38)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad5.d.x < -num38 || quad5.d.x > num38 || quad5.d.y < -num38 || quad5.d.y > num38)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
								}
								else if (!flag12 && Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad5))
								{
									toolErrors |= ToolBase.ToolErrors.OutOfArea;
								}
								if ((properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
								{
									float num39 = 256f;
									if (quad6.a.x < -num39 || quad6.a.x > num39 || quad6.a.y < -num39 || quad6.a.y > num39)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad6.b.x < -num39 || quad6.b.x > num39 || quad6.b.y < -num39 || quad6.b.y > num39)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad6.c.x < -num39 || quad6.c.x > num39 || quad6.c.y < -num39 || quad6.c.y > num39)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
									if (quad6.d.x < -num39 || quad6.d.x > num39 || quad6.d.y < -num39 || quad6.d.y > num39)
									{
										toolErrors |= ToolBase.ToolErrors.OutOfArea;
									}
								}
								else if (!flag12 && Singleton<GameAreaManager>.get_instance().QuadOutOfArea(quad6))
								{
									toolErrors |= ToolBase.ToolErrors.OutOfArea;
								}
							}
						}
					}
					if (buildingInfo2 != null && (netNode.m_flags & NetNode.Flags.Outside) == NetNode.Flags.None)
					{
						ushort ignoreNode3 = (num26 != num5) ? 0 : endPoint.m_node;
						ushort ignoreSegment2 = (num26 != num5) ? 0 : endPoint.m_segment;
						ushort ignoreBuilding2 = (num26 != num5) ? 0 : NetTool.GetIgnoredBuilding(endPoint);
						Vector3 position4 = position2;
						position4.y += num25;
						toolErrors2 = NetTool.TestNodeBuilding(buildingInfo2, position4, vector7, ignoreNode3, ignoreSegment2, ignoreBuilding2, test, array, array2);
						if (test && toolErrors2 != ToolBase.ToolErrors.None)
						{
							toolErrors |= toolErrors2;
						}
					}
					if (test)
					{
						cost += netInfo2.m_netAI.GetConstructionCost(vector3, position2, num27, num28);
						if (needMoney && cost > 0 && Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, cost) != cost)
						{
							toolErrors |= ToolBase.ToolErrors.NotEnoughMoney;
						}
						if (!netInfo2.m_netAI.BuildUnderground() && !netInfo2.m_netAI.BuildOnWater() && !netInfo2.m_netAI.IgnoreWater())
						{
							float num40 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(vector3));
							float num41 = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(position2));
							if ((num40 > vector3.y && num40 > 0f) || (num41 > position2.y && num41 > 0f))
							{
								toolErrors |= ToolBase.ToolErrors.CannotBuildOnWater;
							}
						}
						ushort num42 = 0;
						ushort num43 = 0;
						ushort num44 = 0;
						ushort num45 = 0;
						if (num26 == 1)
						{
							num42 = startPoint.m_node;
							num43 = startPoint.m_segment;
						}
						if (num26 == num5)
						{
							num44 = endPoint.m_node;
							num45 = endPoint.m_segment;
						}
						toolErrors |= NetTool.CanCreateSegment(netInfo2, num42, num43, num44, num45, num, vector3, position2, vector4, -vector7, array, testEnds);
						toolErrors |= netInfo2.m_netAI.CanConnectTo(num42, num43, array);
						toolErrors |= netInfo2.m_netAI.CanConnectTo(num44, num45, array);
					}
					else
					{
						cost += netInfo2.m_netAI.GetConstructionCost(vector3, position2, num27, num28);
						if (needMoney && cost > 0)
						{
							cost -= Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Construction, cost, netInfo2.m_class);
							if (cost > 0)
							{
								toolErrors |= ToolBase.ToolErrors.NotEnoughMoney;
							}
						}
						bool flag13 = num6 == 0;
						bool flag14 = false;
						ushort num46 = endPoint.m_node;
						if (num26 != num5 || num46 == 0)
						{
							if (num26 == num5 && endPoint.m_segment != 0)
							{
								if (NetTool.SplitSegment(endPoint.m_segment, out num46, position2))
								{
									flag14 = true;
								}
								else
								{
									flag13 = true;
								}
								endPoint.m_segment = 0;
							}
							else if (Singleton<NetManager>.get_instance().CreateNode(out num46, ref Singleton<SimulationManager>.get_instance().m_randomizer, nodeInfo2, position2, Singleton<SimulationManager>.get_instance().m_currentBuildIndex))
							{
								if (num26 == num5 && endPoint.m_outside)
								{
									NetNode[] expr_332A_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
									ushort expr_332A_cp_1 = num46;
									expr_332A_cp_0[(int)expr_332A_cp_1].m_flags = (expr_332A_cp_0[(int)expr_332A_cp_1].m_flags | NetNode.Flags.Outside);
								}
								if (num28 < -8f && (nodeInfo2.m_netAI.SupportUnderground() || nodeInfo2.m_netAI.IsUnderground()))
								{
									NetNode[] expr_337F_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
									ushort expr_337F_cp_1 = num46;
									expr_337F_cp_0[(int)expr_337F_cp_1].m_flags = (expr_337F_cp_0[(int)expr_337F_cp_1].m_flags | NetNode.Flags.Underground);
								}
								if (nodeBuffer.m_buffer[num26].m_double)
								{
									NetNode[] expr_33BE_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
									ushort expr_33BE_cp_1 = num46;
									expr_33BE_cp_0[(int)expr_33BE_cp_1].m_flags = (expr_33BE_cp_0[(int)expr_33BE_cp_1].m_flags | NetNode.Flags.Double);
								}
								if (nodeInfo2.m_netAI.IsUnderground())
								{
									Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num46].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(-nodeBuffer[num26].m_elevation), 0, 255);
								}
								else if (nodeInfo2.m_netAI.IsOverground() && (nodeBuffer[num26].m_elevation > 0.1f || nodeBuffer[num26].m_position.y - nodeBuffer[num26].m_terrainHeight > 0.1f))
								{
									Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num46].m_elevation = (byte)Mathf.Clamp(Mathf.RoundToInt(nodeBuffer[num26].m_elevation), 1, 255);
								}
								else
								{
									NetNode[] expr_34DE_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
									ushort expr_34DE_cp_1 = num46;
									expr_34DE_cp_0[(int)expr_34DE_cp_1].m_flags = (expr_34DE_cp_0[(int)expr_34DE_cp_1].m_flags | NetNode.Flags.OnGround);
								}
								Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 1u;
								flag14 = true;
							}
							else
							{
								flag13 = true;
							}
							if (num26 == num5)
							{
								endPoint.m_node = num46;
							}
						}
						if (!flag13 && !flag3 && Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_elevation == Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num46].m_elevation)
						{
							Vector3 endPos = vector3;
							if (num26 == 1)
							{
								NetTool.TryMoveNode(ref num6, ref vector4, netInfo2, position2);
								endPos = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num6].m_position;
							}
							if (num26 == num5)
							{
								Vector3 vector21 = -vector7;
								NetTool.TryMoveNode(ref num46, ref vector21, netInfo2, endPos);
								vector7 = -vector21;
							}
						}
						if (!flag13)
						{
							if (nodeBuffer.m_buffer[num26].m_double || flag8)
							{
								flag13 = !Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, netInfo2, num46, num6, -vector7, vector4, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, !flag);
							}
							else if (nodeBuffer.m_buffer[num26 - 1].m_double || flag7)
							{
								flag13 = !Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, netInfo2, num6, num46, vector4, -vector7, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, flag);
							}
							else if ((num5 - num26 & 1) == 0 && num26 != 1 && flag3)
							{
								flag13 = !Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, netInfo2, num46, num6, -vector7, vector4, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, !flag);
							}
							else
							{
								flag13 = !Singleton<NetManager>.get_instance().CreateSegment(out segment, ref Singleton<SimulationManager>.get_instance().m_randomizer, netInfo2, num6, num46, vector4, -vector7, buildIndex, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, flag);
							}
							if (!flag13)
							{
								NetSegment[] expr_3732_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
								ushort expr_3732_cp_1 = segment;
								expr_3732_cp_0[(int)expr_3732_cp_1].m_flags = (expr_3732_cp_0[(int)expr_3732_cp_1].m_flags | flags);
								if (num2 != 0)
								{
									Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_nameSeed = num2;
								}
								if (text != null)
								{
									NetSegment[] expr_3783_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
									ushort expr_3783_cp_1 = segment;
									expr_3783_cp_0[(int)expr_3783_cp_1].m_flags = (expr_3783_cp_0[(int)expr_3783_cp_1].m_flags | NetSegment.Flags.CustomName);
									InstanceID empty2 = InstanceID.Empty;
									empty2.NetSegment = segment;
									Singleton<InstanceManager>.get_instance().SetName(empty2, text);
								}
								if (flag2)
								{
									Singleton<NetManager>.get_instance().m_adjustedSegments[segment >> 6] |= 1uL << (int)segment;
								}
								Singleton<SimulationManager>.get_instance().m_currentBuildIndex += 2u;
								buildIndex = Singleton<SimulationManager>.get_instance().m_currentBuildIndex;
								NetTool.DispatchPlacementEffect(vector3, vector8, vector9, position2, info.m_halfWidth, false);
								netInfo2.m_netAI.ManualActivation(segment, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment], oldInfo);
							}
						}
						if (flag13)
						{
							if (flag6 && num6 != 0)
							{
								Singleton<NetManager>.get_instance().ReleaseNode(num6);
								num6 = 0;
							}
							if (flag14 && num46 != 0)
							{
								Singleton<NetManager>.get_instance().ReleaseNode(num46);
								num46 = 0;
							}
						}
						if (num3 != 0 && num46 != 0 && (Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num46].m_flags & NetNode.Flags.Untouchable) == NetNode.Flags.None)
						{
							NetNode[] expr_38C1_cp_0 = Singleton<NetManager>.get_instance().m_nodes.m_buffer;
							ushort expr_38C1_cp_1 = num46;
							expr_38C1_cp_0[(int)expr_38C1_cp_1].m_flags = (expr_38C1_cp_0[(int)expr_38C1_cp_1].m_flags | NetNode.Flags.Untouchable);
							Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)num46].m_nextBuildingNode = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num3].m_netNode;
							Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num3].m_netNode = num46;
						}
						if (num3 != 0 && segment != 0 && (Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
						{
							NetSegment[] expr_396F_cp_0 = Singleton<NetManager>.get_instance().m_segments.m_buffer;
							ushort expr_396F_cp_1 = segment;
							expr_396F_cp_0[(int)expr_396F_cp_1].m_flags = (expr_396F_cp_0[(int)expr_396F_cp_1].m_flags | NetSegment.Flags.Untouchable);
						}
						num6 = num46;
					}
				}
				vector3 = position2;
				vector4 = vector7;
				flag6 = false;
			}
			if (visualize)
			{
				if (nodeInfo2.m_netAI.DisplayTempSegment())
				{
					NetTool.RenderNode(nodeInfo2, vector3, -vector4);
				}
			}
			else
			{
				BuildingTool.IgnoreRelocateSegments(relocateBuildingID, array, array2);
				if (NetTool.CheckCollidingSegments(array, array2, num) && test && (toolErrors & (ToolBase.ToolErrors.InvalidShape | ToolBase.ToolErrors.TooShort | ToolBase.ToolErrors.SlopeTooSteep | ToolBase.ToolErrors.HeightTooHigh | ToolBase.ToolErrors.TooManyConnections)) == ToolBase.ToolErrors.None)
				{
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (BuildingTool.CheckCollidingBuildings(null, array2, array) && test)
				{
					toolErrors |= ToolBase.ToolErrors.ObjectCollision;
				}
				if (!test)
				{
					NetTool.ReleaseNonImportantSegments(array);
					BuildingTool.ReleaseNonImportantBuildings(array2);
				}
			}
			for (int num47 = 0; num47 <= num5; num47++)
			{
				nodeBuffer.m_buffer[num47].m_nodeInfo = null;
			}
			firstNode = startPoint.m_node;
			lastNode = endPoint.m_node;
			result = toolErrors;
		}
		finally
		{
			if (cost < 0)
			{
				cost = 0;
			}
			if (test || !visualize)
			{
				properties.EndColliding();
			}
		}
		return result;
	}

	public static bool CheckCollidingSegments(ulong[] segmentMask, ulong[] buildingMask, ushort upgrading)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = segmentMask.Length;
		bool result = false;
		for (int i = 0; i < num; i++)
		{
			ulong num2 = segmentMask[i];
			if (num2 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num2 & 1uL << j) != 0uL)
					{
						int num3 = i << 6 | j;
						if (num3 != (int)upgrading)
						{
							NetInfo info = instance.m_segments.m_buffer[num3].Info;
							int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
							if ((publicServiceIndex != -1 && !info.m_autoRemove) || (instance.m_segments.m_buffer[num3].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
							{
								result = true;
							}
							else
							{
								NetTool.CheckCollidingNode(instance.m_segments.m_buffer[num3].m_startNode, segmentMask, buildingMask);
								NetTool.CheckCollidingNode(instance.m_segments.m_buffer[num3].m_endNode, segmentMask, buildingMask);
							}
						}
						else
						{
							segmentMask[num3 >> 6] &= ~(1uL << num3);
						}
					}
				}
			}
		}
		return result;
	}

	public static bool ReleaseNonImportantSegments(ulong[] segmentMask)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = segmentMask.Length;
		for (int i = 0; i < num; i++)
		{
			ulong num2 = segmentMask[i];
			if (num2 != 0uL)
			{
				for (int j = 0; j < 64; j++)
				{
					if ((num2 & 1uL << j) != 0uL)
					{
						int num3 = i << 6 | j;
						NetInfo info = instance.m_segments.m_buffer[num3].Info;
						int privateServiceIndex = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
						if ((privateServiceIndex != -1 || info.m_autoRemove) && (instance.m_segments.m_buffer[num3].m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
						{
							instance.ReleaseSegment((ushort)num3, false);
							segmentMask[i] &= ~(1uL << j);
						}
					}
				}
			}
		}
		return false;
	}

	private static void CheckCollidingNode(ushort node, ulong[] segmentMask, ulong[] buildingMask)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		ushort building = instance.m_nodes.m_buffer[(int)node].m_building;
		if (building != 0)
		{
			for (int i = 0; i < 8; i++)
			{
				ushort segment = instance.m_nodes.m_buffer[(int)node].GetSegment(i);
				if (segment != 0 && (segmentMask[segment >> 6] & 1uL << (int)segment) == 0uL)
				{
					return;
				}
			}
			buildingMask[building >> 6] &= ~(1uL << (int)building);
		}
	}

	public static void DispatchPlacementEffect(Vector3 startPos, Vector3 middlePos1, Vector3 middlePos2, Vector3 endPos, float halfWidth, bool bulldozing)
	{
		EffectInfo effectInfo;
		if (bulldozing)
		{
			effectInfo = Singleton<NetManager>.get_instance().m_properties.m_bulldozeEffect;
		}
		else
		{
			effectInfo = Singleton<NetManager>.get_instance().m_properties.m_placementEffect;
		}
		if (effectInfo != null)
		{
			Bezier3 bezier;
			bezier..ctor(startPos, middlePos1, middlePos2, endPos);
			EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(bezier, halfWidth, 0f);
			Singleton<EffectManager>.get_instance().DispatchEffect(effectInfo, spawnArea, Vector3.get_zero(), 0f, 1f, Singleton<AudioManager>.get_instance().DefaultGroup, 0u, true);
		}
	}

	private void Snap(NetInfo info, ref Vector3 point, ref Vector3 direction, Vector3 refPoint, float refAngle)
	{
		direction..ctor(Mathf.Cos(refAngle), 0f, Mathf.Sin(refAngle));
		Vector3 vector = direction * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		if (info.m_halfWidth <= 4f)
		{
			refPoint.x += vector.x * 0.5f + vector2.x * 0.5f;
			refPoint.z += vector.z * 0.5f + vector2.z * 0.5f;
		}
		Vector2 vector3;
		vector3..ctor(point.x - refPoint.x, point.z - refPoint.z);
		float num = Mathf.Round((vector3.x * vector.x + vector3.y * vector.z) * 0.015625f);
		float num2 = Mathf.Round((vector3.x * vector2.x + vector3.y * vector2.z) * 0.015625f);
		point.x = refPoint.x + num * vector.x + num2 * vector2.x;
		point.z = refPoint.z + num * vector.z + num2 * vector2.z;
	}

	public static bool MakeControlPoint(Ray ray, float rayLength, NetInfo info, bool ignoreTerrain, NetNode.Flags ignoreNodeFlags, NetSegment.Flags ignoreSegmentFlags, Building.Flags ignoreBuildingFlags, float elevation, bool tunnels, out NetTool.ControlPoint p)
	{
		p = default(NetTool.ControlPoint);
		p.m_elevation = elevation;
		ItemClass connectionClass = info.GetConnectionClass();
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(ray, rayLength);
		ToolBase.RaycastOutput raycastOutput = default(ToolBase.RaycastOutput);
		input.m_buildObject = info;
		input.m_netSnap = elevation + info.m_netAI.GetSnapElevation();
		input.m_ignoreNodeFlags = ignoreNodeFlags;
		input.m_ignoreSegmentFlags = ignoreSegmentFlags;
		input.m_ignoreBuildingFlags = ignoreBuildingFlags;
		bool flag;
		if (tunnels && (info.m_netAI.SupportUnderground() || info.m_netAI.IsUnderground()))
		{
			input.m_netService = new ToolBase.RaycastService(connectionClass.m_service, connectionClass.m_subService, ItemClass.Layer.MetroTunnels);
			input.m_buildingService = new ToolBase.RaycastService(connectionClass.m_service, connectionClass.m_subService, ItemClass.Layer.MetroTunnels);
			if (info.m_intersectClass != null)
			{
				input.m_netService2 = new ToolBase.RaycastService(info.m_intersectClass.m_service, info.m_intersectClass.m_subService, ItemClass.Layer.MetroTunnels);
			}
			input.m_ignoreTerrain = true;
			flag = ToolBase.RayCast(input, out raycastOutput);
		}
		else
		{
			input.m_ignoreNodeFlags |= NetNode.Flags.Underground;
			flag = false;
		}
		if (!flag)
		{
			input.m_netService = new ToolBase.RaycastService(connectionClass.m_service, connectionClass.m_subService, connectionClass.m_layer);
			input.m_buildingService = new ToolBase.RaycastService(connectionClass.m_service, connectionClass.m_subService, ItemClass.Layer.None);
			if (info.m_intersectClass != null)
			{
				input.m_netService2 = new ToolBase.RaycastService(info.m_intersectClass.m_service, info.m_intersectClass.m_subService, info.m_intersectClass.m_layer);
			}
			input.m_ignoreTerrain = ignoreTerrain;
			flag = ToolBase.RayCast(input, out raycastOutput);
		}
		if (flag)
		{
			if (raycastOutput.m_building != 0)
			{
				raycastOutput.m_netNode = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)raycastOutput.m_building].FindNode(connectionClass.m_service, connectionClass.m_subService, connectionClass.m_layer);
				raycastOutput.m_building = 0;
			}
			p.m_position = raycastOutput.m_hitPos;
			p.m_node = raycastOutput.m_netNode;
			p.m_segment = raycastOutput.m_netSegment;
			Vector3 position = p.m_position;
			if (p.m_node != 0)
			{
				NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)p.m_node];
				p.m_position = netNode.m_position;
				p.m_direction = Vector3.get_zero();
				p.m_segment = 0;
				if (netNode.Info.m_netAI.IsUnderground())
				{
					p.m_elevation = (float)(-(float)netNode.m_elevation);
				}
				else
				{
					p.m_elevation = (float)netNode.m_elevation;
				}
			}
			else if (p.m_segment != 0)
			{
				NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)p.m_segment];
				NetNode netNode2 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_startNode];
				NetNode netNode3 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)netSegment.m_endNode];
				bool flag2 = !NetSegment.IsStraight(netNode2.m_position, netSegment.m_startDirection, netNode3.m_position, netSegment.m_endDirection);
				if (flag2)
				{
					Vector3 vector;
					Vector3 direction;
					netSegment.GetClosestPositionAndDirection(p.m_position, out vector, out direction);
					if ((vector - netNode2.m_position).get_sqrMagnitude() < 64f)
					{
						p.m_position = netNode2.m_position;
						p.m_direction = netSegment.m_startDirection;
						p.m_node = netSegment.m_startNode;
						p.m_segment = 0;
					}
					else if ((vector - netNode3.m_position).get_sqrMagnitude() < 64f)
					{
						p.m_position = netNode3.m_position;
						p.m_direction = netSegment.m_endDirection;
						p.m_node = netSegment.m_endNode;
						p.m_segment = 0;
					}
					else
					{
						p.m_position = vector;
						p.m_direction = direction;
					}
				}
				else
				{
					p.m_position = netSegment.GetClosestPosition(p.m_position);
					p.m_direction = netSegment.m_startDirection;
					float num = (p.m_position.x - netNode2.m_position.x) * (netNode3.m_position.x - netNode2.m_position.x) + (p.m_position.z - netNode2.m_position.z) * (netNode3.m_position.z - netNode2.m_position.z);
					float num2 = (netNode3.m_position.x - netNode2.m_position.x) * (netNode3.m_position.x - netNode2.m_position.x) + (netNode3.m_position.z - netNode2.m_position.z) * (netNode3.m_position.z - netNode2.m_position.z);
					if (num2 != 0f)
					{
						p.m_position = NetTool.LerpPosition(netNode2.m_position, netNode3.m_position, num / num2, info.m_netAI.GetLengthSnap());
					}
				}
				float num3 = (float)netNode2.m_elevation;
				float num4 = (float)netNode3.m_elevation;
				if (netNode2.Info.m_netAI.IsUnderground())
				{
					num3 = -num3;
				}
				if (netNode3.Info.m_netAI.IsUnderground())
				{
					num4 = -num4;
				}
				p.m_elevation = Mathf.Lerp(num3, num4, 0.5f);
				if ((netNode2.m_elevation > 0 || netNode3.m_elevation > 0) && p.m_elevation != 0f)
				{
					p.m_elevation = Mathf.Max(1f, p.m_elevation);
				}
			}
			else
			{
				float num5 = 8640f;
				if (Mathf.Abs(p.m_position.x) >= Mathf.Abs(p.m_position.z))
				{
					if (p.m_position.x > num5 - info.m_halfWidth * 3f)
					{
						p.m_position.x = num5 + info.m_halfWidth * 0.8f;
						p.m_position.z = Mathf.Clamp(p.m_position.z, info.m_halfWidth - num5, num5 - info.m_halfWidth);
						p.m_outside = true;
					}
					if (p.m_position.x < info.m_halfWidth * 3f - num5)
					{
						p.m_position.x = -num5 - info.m_halfWidth * 0.8f;
						p.m_position.z = Mathf.Clamp(p.m_position.z, info.m_halfWidth - num5, num5 - info.m_halfWidth);
						p.m_outside = true;
					}
				}
				else
				{
					if (p.m_position.z > num5 - info.m_halfWidth * 3f)
					{
						p.m_position.z = num5 + info.m_halfWidth * 0.8f;
						p.m_position.x = Mathf.Clamp(p.m_position.x, info.m_halfWidth - num5, num5 - info.m_halfWidth);
						p.m_outside = true;
					}
					if (p.m_position.z < info.m_halfWidth * 3f - num5)
					{
						p.m_position.z = -num5 - info.m_halfWidth * 0.8f;
						p.m_position.x = Mathf.Clamp(p.m_position.x, info.m_halfWidth - num5, num5 - info.m_halfWidth);
						p.m_outside = true;
					}
				}
				p.m_position.y = NetSegment.SampleTerrainHeight(info, p.m_position, false, elevation);
			}
			if (p.m_node != 0)
			{
				NetNode netNode4 = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)p.m_node];
				if ((netNode4.m_flags & ignoreNodeFlags) != NetNode.Flags.None)
				{
					p.m_position = position;
					p.m_position.y = NetSegment.SampleTerrainHeight(info, p.m_position, false, elevation);
					p.m_node = 0;
					p.m_segment = 0;
					p.m_elevation = elevation;
				}
			}
			else if (p.m_segment != 0)
			{
				NetSegment netSegment2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)p.m_segment];
				if ((netSegment2.m_flags & ignoreSegmentFlags) != NetSegment.Flags.None)
				{
					p.m_position = position;
					p.m_position.y = NetSegment.SampleTerrainHeight(info, p.m_position, false, elevation);
					p.m_node = 0;
					p.m_segment = 0;
					p.m_elevation = elevation;
				}
			}
			return true;
		}
		return false;
	}

	public override void SimulationStep()
	{
		NetInfo netInfo = this.m_prefab;
		if (netInfo == null)
		{
			return;
		}
		if (this.m_mode == NetTool.Mode.Straight)
		{
			if (netInfo.m_class.m_service == ItemClass.Service.Road || netInfo.m_class.m_service == ItemClass.Service.PublicTransport || netInfo.m_class.m_service == ItemClass.Service.Beautification)
			{
				GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
				if (properties != null)
				{
					Singleton<NetManager>.get_instance().m_optionsNotUsed.Activate(properties.m_roadOptionsNotUsed, netInfo.m_class.m_service);
				}
			}
		}
		else
		{
			ServiceTypeGuide optionsNotUsed = Singleton<NetManager>.get_instance().m_optionsNotUsed;
			if (optionsNotUsed != null && !optionsNotUsed.m_disabled)
			{
				optionsNotUsed.Disable();
			}
		}
		if (this.m_elevation == 0)
		{
			int num;
			int num2;
			netInfo.m_netAI.GetElevationLimits(out num, out num2);
			if (num2 > num)
			{
				GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
				if (properties2 != null)
				{
					Singleton<NetManager>.get_instance().m_elevationNotUsed.Activate(properties2.m_elevationNotUsed, netInfo.m_class.m_service);
				}
			}
		}
		else
		{
			ServiceTypeGuide elevationNotUsed = Singleton<NetManager>.get_instance().m_elevationNotUsed;
			if (elevationNotUsed != null && !elevationNotUsed.m_disabled)
			{
				elevationNotUsed.Disable();
			}
		}
		if ((netInfo.m_hasForwardVehicleLanes || netInfo.m_hasBackwardVehicleLanes) && (!netInfo.m_hasForwardVehicleLanes || !netInfo.m_hasBackwardVehicleLanes) && (netInfo.m_class.m_service == ItemClass.Service.Road || netInfo.m_class.m_service == ItemClass.Service.PublicTransport || netInfo.m_class.m_service == ItemClass.Service.Beautification) && this.m_controlPointCount >= 1)
		{
			GuideController properties3 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties3 != null)
			{
				Singleton<NetManager>.get_instance().m_onewayRoadPlacement.Activate(properties3.m_onewayRoadPlacement);
			}
		}
		if (this.m_mode == NetTool.Mode.Upgrade)
		{
			ServiceTypeGuide manualUpgrade = Singleton<NetManager>.get_instance().m_manualUpgrade;
			manualUpgrade.Deactivate();
		}
		else if (netInfo.m_class.m_service == ItemClass.Service.Road)
		{
			GuideController properties4 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties4 != null)
			{
				Singleton<NetManager>.get_instance().m_visualAids.Activate(properties4.m_visualAids);
			}
		}
		Vector3 position = this.m_controlPoints[this.m_controlPointCount].m_position;
		bool flag = false;
		bool flag2 = false;
		if (this.m_mode == NetTool.Mode.Upgrade)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
			input.m_buildObject = netInfo;
			input.m_netService = new ToolBase.RaycastService(netInfo.m_class.m_service, netInfo.m_class.m_subService, netInfo.m_class.m_layer);
			input.m_ignoreTerrain = true;
			input.m_ignoreNodeFlags = NetNode.Flags.All;
			input.m_ignoreSegmentFlags = NetSegment.Flags.Untouchable;
			if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Transport || Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Traffic || Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes)
			{
				input.m_netService.m_itemLayers = (input.m_netService.m_itemLayers | ItemClass.Layer.MetroTunnels);
			}
			else if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Underground && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
			{
				input.m_netService.m_itemLayers = (input.m_netService.m_itemLayers | ItemClass.Layer.MetroTunnels);
			}
			ToolBase.RaycastOutput raycastOutput;
			if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
			{
				bool flag3 = false;
				if (raycastOutput.m_netSegment != 0)
				{
					if ((instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].m_flags & NetSegment.Flags.Collapsed) != NetSegment.Flags.None)
					{
						flag3 = true;
					}
					NetInfo info = instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].Info;
					if (info.m_class.m_service != netInfo.m_class.m_service || info.m_class.m_subService != netInfo.m_class.m_subService)
					{
						raycastOutput.m_netSegment = 0;
					}
					else if (this.m_upgradedSegments.Contains(raycastOutput.m_netSegment) || (this.m_upgradedSegments.Count != 0 && flag3 != this.m_repairing))
					{
						raycastOutput.m_netSegment = 0;
					}
				}
				if (raycastOutput.m_netSegment != 0)
				{
					if (flag3)
					{
						flag2 = true;
						netInfo = instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].Info;
					}
					NetTool.ControlPoint controlPoint;
					controlPoint.m_node = instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].m_startNode;
					controlPoint.m_segment = 0;
					controlPoint.m_position = instance.m_nodes.m_buffer[(int)controlPoint.m_node].m_position;
					controlPoint.m_direction = instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].m_startDirection;
					controlPoint.m_elevation = (float)instance.m_nodes.m_buffer[(int)controlPoint.m_node].m_elevation;
					if (instance.m_nodes.m_buffer[(int)controlPoint.m_node].Info.m_netAI.IsUnderground())
					{
						controlPoint.m_elevation = -controlPoint.m_elevation;
					}
					controlPoint.m_outside = ((instance.m_nodes.m_buffer[(int)controlPoint.m_node].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None);
					NetTool.ControlPoint controlPoint2;
					controlPoint2.m_node = instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].m_endNode;
					controlPoint2.m_segment = 0;
					controlPoint2.m_position = instance.m_nodes.m_buffer[(int)controlPoint2.m_node].m_position;
					controlPoint2.m_direction = -instance.m_segments.m_buffer[(int)raycastOutput.m_netSegment].m_endDirection;
					controlPoint2.m_elevation = (float)instance.m_nodes.m_buffer[(int)controlPoint2.m_node].m_elevation;
					if (instance.m_nodes.m_buffer[(int)controlPoint2.m_node].Info.m_netAI.IsUnderground())
					{
						controlPoint2.m_elevation = -controlPoint2.m_elevation;
					}
					controlPoint2.m_outside = ((instance.m_nodes.m_buffer[(int)controlPoint2.m_node].m_flags & NetNode.Flags.Outside) != NetNode.Flags.None);
					NetTool.ControlPoint controlPoint3;
					controlPoint3.m_node = 0;
					controlPoint3.m_segment = raycastOutput.m_netSegment;
					controlPoint3.m_position = controlPoint.m_position + controlPoint.m_direction * (netInfo.GetMinNodeDistance() + 1f);
					controlPoint3.m_direction = controlPoint.m_direction;
					controlPoint3.m_elevation = Mathf.Lerp(controlPoint.m_elevation, controlPoint2.m_elevation, 0.5f);
					controlPoint3.m_outside = false;
					this.m_controlPoints[0] = controlPoint;
					this.m_controlPoints[1] = controlPoint3;
					this.m_controlPoints[2] = controlPoint2;
					this.m_controlPointCount = 2;
				}
				else
				{
					this.m_controlPointCount = 0;
					this.m_controlPoints[this.m_controlPointCount] = default(NetTool.ControlPoint);
				}
			}
			else
			{
				this.m_controlPointCount = 0;
				this.m_controlPoints[this.m_controlPointCount] = default(NetTool.ControlPoint);
				flag = true;
			}
			NetTool.ClearHelperLines(NetTool.m_helperLines);
		}
		else
		{
			NetTool.ControlPoint controlPoint4 = default(NetTool.ControlPoint);
			float elevation = this.GetElevation(netInfo);
			NetNode.Flags flags;
			NetSegment.Flags ignoreSegmentFlags;
			if ((this.m_mode == NetTool.Mode.Curved || this.m_mode == NetTool.Mode.Freeform) && this.m_controlPointCount == 1)
			{
				flags = NetNode.Flags.All;
				ignoreSegmentFlags = NetSegment.Flags.All;
			}
			else
			{
				flags = NetNode.Flags.ForbidLaneConnection;
				ignoreSegmentFlags = NetSegment.Flags.Untouchable;
			}
			Building.Flags ignoreBuildingFlags;
			if (netInfo.m_snapBuildingNodes)
			{
				ignoreBuildingFlags = Building.Flags.Untouchable;
			}
			else
			{
				ignoreBuildingFlags = Building.Flags.All;
			}
			bool tunnels = Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Transport || Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Traffic || Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.TrafficRoutes;
			if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.Underground && Singleton<InfoManager>.get_instance().CurrentSubMode == InfoManager.SubInfoMode.Default)
			{
				tunnels = true;
			}
			if (this.m_mouseRayValid && NetTool.MakeControlPoint(this.m_mouseRay, this.m_mouseRayLength, netInfo, false, flags, ignoreSegmentFlags, ignoreBuildingFlags, elevation, tunnels, out controlPoint4))
			{
				Vector3 vector;
				Vector3 zero;
				if ((this.m_snap & NetTool.Snapping.HelperLine) != NetTool.Snapping.None)
				{
					bool canSnap = controlPoint4.m_node == 0 && controlPoint4.m_segment == 0 && !controlPoint4.m_outside;
					if (this.m_controlPointCount == 1)
					{
						NetTool.ControlPoint oldPoint = this.m_controlPoints[this.m_controlPointCount - 1];
						NetTool.RefreshHelperLines(NetTool.m_helperLines, oldPoint, controlPoint4, 1 << netInfo.m_prefabDataLayer, canSnap, out vector, out zero);
					}
					else
					{
						NetTool.ControlPoint oldPoint2 = default(NetTool.ControlPoint);
						oldPoint2.m_position = controlPoint4.m_position;
						NetTool.RefreshHelperLines(NetTool.m_helperLines, oldPoint2, controlPoint4, 1 << netInfo.m_prefabDataLayer, canSnap, out vector, out zero);
					}
				}
				else
				{
					vector = controlPoint4.m_position;
					zero = Vector3.get_zero();
					NetTool.ClearHelperLines(NetTool.m_helperLines);
				}
				bool flag4 = false;
				bool flag5 = false;
				bool flag6 = true;
				if (controlPoint4.m_node == 0 && controlPoint4.m_segment == 0 && !controlPoint4.m_outside)
				{
					if (this.m_snap != NetTool.Snapping.None)
					{
						if (zero != Vector3.get_zero())
						{
							vector.y = NetSegment.SampleTerrainHeight(netInfo, vector, false, elevation);
							zero.y = 0f;
							controlPoint4.m_position = vector;
							controlPoint4.m_direction = zero;
							controlPoint4.m_direction.y = 0f;
							flag4 = true;
						}
						if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
						{
							if (!flag4 && (this.m_snap & NetTool.Snapping.Grid) != NetTool.Snapping.None)
							{
								Vector3 zero2 = Vector3.get_zero();
								PrefabInfo editPrefabInfo = Singleton<ToolManager>.get_instance().m_properties.m_editPrefabInfo;
								if (editPrefabInfo != null)
								{
									if ((editPrefabInfo.GetWidth() & 1) != 0)
									{
										zero2.x += 4f;
									}
									if ((editPrefabInfo.GetLength() & 1) != 0)
									{
										zero2.z += 4f;
									}
								}
								this.Snap(netInfo, ref controlPoint4.m_position, ref controlPoint4.m_direction, zero2, 0f);
								flag4 = true;
							}
						}
						else
						{
							Singleton<NetManager>.get_instance().GetClosestSegments(controlPoint4.m_position, this.m_closeSegments, out this.m_closeSegmentCount);
							if (!flag4)
							{
								controlPoint4.m_direction = Vector3.get_zero();
							}
							if (flags != NetNode.Flags.All && (this.m_snap & NetTool.Snapping.Grid) != NetTool.Snapping.None)
							{
								Vector3 position2 = controlPoint4.m_position;
								Vector3 direction = controlPoint4.m_direction;
								float num3 = 128f;
								for (int i = 0; i < 5; i++)
								{
									for (int j = 0; j < this.m_closeSegmentCount; j++)
									{
										ushort num4 = this.m_closeSegments[j];
										NetInfo info2 = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num4].Info;
										float num5;
										Vector3 vector2;
										Vector3 vector3;
										if (info2.m_netAI.Snap(num4, ref Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num4], netInfo, controlPoint4.m_position, controlPoint4.m_elevation, out num5, out vector2, out vector3) && num5 < num3)
										{
											num3 = num5;
											position2 = vector2;
											direction = vector3;
											flag4 = true;
											flag5 = true;
										}
									}
									if (num3 >= 128f)
									{
										break;
									}
									controlPoint4.m_position = position2;
									controlPoint4.m_direction = direction;
									num3 = 128f;
								}
							}
							if (flag4)
							{
								flag6 = false;
							}
							else
							{
								Vector3 vector4;
								Vector3 vector5;
								float num6;
								if (netInfo.m_netAI.SnapShoreLine() && flags != NetNode.Flags.All && (this.m_snap & NetTool.Snapping.Grid) != NetTool.Snapping.None && Singleton<TerrainManager>.get_instance().GetShorePos(controlPoint4.m_position, 50f, out vector4, out vector5, out num6))
								{
									Vector3 refPos = vector4;
									if (Singleton<TerrainManager>.get_instance().GetShorePos(refPos, 50f, out vector4, out vector5, out num6))
									{
										controlPoint4.m_position = vector4;
										controlPoint4.m_direction = VectorUtils.NormalizeXZ(vector5);
										flag4 = true;
									}
								}
								if (!flag4 && (this.m_snap & NetTool.Snapping.Grid) != NetTool.Snapping.None)
								{
									float num7 = 256f;
									ushort num8 = 0;
									for (int k = 0; k < this.m_closeSegmentCount; k++)
									{
										ushort num9 = this.m_closeSegments[k];
										Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num9].GetClosestZoneBlock(controlPoint4.m_position, ref num7, ref num8);
									}
									if (num8 != 0)
									{
										ZoneBlock zoneBlock = Singleton<ZoneManager>.get_instance().m_blocks.m_buffer[(int)num8];
										this.Snap(netInfo, ref controlPoint4.m_position, ref controlPoint4.m_direction, zoneBlock.m_position, zoneBlock.m_angle);
										flag4 = true;
									}
								}
							}
						}
					}
					if (flag6)
					{
						controlPoint4.m_position.y = NetSegment.SampleTerrainHeight(netInfo, controlPoint4.m_position, false, controlPoint4.m_elevation);
					}
				}
				else
				{
					flag4 = true;
				}
				bool flag7 = false;
				if (this.m_controlPointCount == 2 && this.m_mode == NetTool.Mode.Freeform)
				{
					Vector3 vector6 = controlPoint4.m_position - this.m_controlPoints[0].m_position;
					Vector3 direction2 = this.m_controlPoints[1].m_direction;
					vector6.y = 0f;
					direction2.y = 0f;
					float num10 = Vector3.SqrMagnitude(vector6);
					vector6 = Vector3.Normalize(vector6);
					float num11 = Mathf.Min(1.17809725f, Mathf.Acos(Vector3.Dot(vector6, direction2)));
					float num12 = Mathf.Sqrt(0.5f * num10 / Mathf.Max(0.001f, 1f - Mathf.Cos(3.14159274f - 2f * num11)));
					this.m_controlPoints[1].m_position = this.m_controlPoints[0].m_position + direction2 * num12;
					controlPoint4.m_direction = controlPoint4.m_position - this.m_controlPoints[1].m_position;
					controlPoint4.m_direction.y = 0f;
					controlPoint4.m_direction.Normalize();
				}
				else if (this.m_controlPointCount != 0)
				{
					NetTool.ControlPoint oldPoint3 = this.m_controlPoints[this.m_controlPointCount - 1];
					controlPoint4.m_direction = controlPoint4.m_position - oldPoint3.m_position;
					controlPoint4.m_direction.y = 0f;
					controlPoint4.m_direction.Normalize();
					if (!flag5)
					{
						float num13 = netInfo.GetMinNodeDistance();
						num13 *= num13;
						float num14 = num13;
						NetTool.ControlPoint controlPoint5;
						if ((this.m_snap & NetTool.Snapping.Angle) != NetTool.Snapping.None)
						{
							controlPoint5 = NetTool.SnapDirection(controlPoint4, oldPoint3, netInfo, out flag7, out num13);
							controlPoint4 = controlPoint5;
						}
						else
						{
							controlPoint5 = controlPoint4;
						}
						if (controlPoint4.m_segment != 0 && num13 < num14)
						{
							NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)controlPoint4.m_segment];
							controlPoint5.m_position = netSegment.GetClosestPosition(controlPoint4.m_position, controlPoint4.m_direction);
						}
						else if (controlPoint4.m_segment == 0 && controlPoint4.m_node == 0 && !controlPoint4.m_outside)
						{
							bool flag8 = false;
							if (zero != Vector3.get_zero())
							{
								Line2 line;
								line..ctor(VectorUtils.XZ(oldPoint3.m_position), VectorUtils.XZ(controlPoint4.m_position));
								Line2 line2;
								line2..ctor(VectorUtils.XZ(vector), VectorUtils.XZ(vector + zero));
								float num15;
								float num16;
								if (line.Intersect(line2, ref num15, ref num16))
								{
									vector += zero * num16;
									if (VectorUtils.LengthXZ(vector - controlPoint4.m_position) < 7.5f)
									{
										controlPoint5.m_position = vector;
										flag8 = true;
									}
								}
							}
							if (!flag8 && (this.m_snap & NetTool.Snapping.Length) != NetTool.Snapping.None)
							{
								float lengthSnap = netInfo.m_netAI.GetLengthSnap();
								if (this.m_mode != NetTool.Mode.Freeform && (flag7 || !flag4) && lengthSnap != 0f)
								{
									Vector3 vector7 = controlPoint4.m_position - oldPoint3.m_position;
									Vector3 vector8;
									vector8..ctor(vector7.x, 0f, vector7.z);
									float magnitude = vector8.get_magnitude();
									if (magnitude < 0.001f)
									{
										controlPoint5.m_position = oldPoint3.m_position;
									}
									else
									{
										int num17 = Mathf.Max(1, Mathf.RoundToInt(magnitude / lengthSnap));
										controlPoint5.m_position = oldPoint3.m_position + vector7 * ((float)num17 * lengthSnap / magnitude);
										if (netInfo.m_useFixedHeight)
										{
											controlPoint5.m_position.y = controlPoint4.m_position.y;
										}
										else
										{
											controlPoint5.m_position.y = NetSegment.SampleTerrainHeight(netInfo, controlPoint5.m_position, false, controlPoint5.m_elevation);
										}
									}
								}
							}
						}
						controlPoint4 = controlPoint5;
					}
				}
			}
			else
			{
				flag = true;
				NetTool.ClearHelperLines(NetTool.m_helperLines);
			}
			this.m_controlPoints[this.m_controlPointCount] = controlPoint4;
		}
		bool flag9 = (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None;
		int num20;
		int productionRate;
		ToolBase.ToolErrors toolErrors;
		if (this.m_controlPointCount == 2)
		{
			if (Vector3.SqrMagnitude(position - this.m_controlPoints[this.m_controlPointCount].m_position) > 1f)
			{
				this.m_lengthChanging = true;
			}
			NetTool.ControlPoint startPoint = this.m_controlPoints[this.m_controlPointCount - 2];
			NetTool.ControlPoint middlePoint = this.m_controlPoints[this.m_controlPointCount - 1];
			NetTool.ControlPoint endPoint = this.m_controlPoints[this.m_controlPointCount];
			ushort num18;
			ushort num19;
			toolErrors = NetTool.CreateNode(netInfo, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsSimulation, 1000, true, false, true, flag9, false, this.m_switchingDir, 0, out num18, out num19, out num20, out productionRate);
			if (NetTool.GetSecondaryControlPoints(netInfo, ref startPoint, ref middlePoint, ref endPoint))
			{
				int num21;
				toolErrors |= NetTool.CreateNode(netInfo, startPoint, middlePoint, endPoint, NetTool.m_nodePositionsSimulation, 1000, true, false, true, flag9, false, this.m_switchingDir, 0, out num18, out num19, out num21, out productionRate);
				num20 += num21;
			}
		}
		else if (this.m_controlPointCount == 1)
		{
			if (Vector3.SqrMagnitude(position - this.m_controlPoints[this.m_controlPointCount].m_position) > 1f)
			{
				this.m_lengthChanging = true;
			}
			NetTool.ControlPoint middlePoint2 = this.m_controlPoints[1];
			if ((this.m_mode != NetTool.Mode.Curved && this.m_mode != NetTool.Mode.Freeform) || middlePoint2.m_node != 0 || middlePoint2.m_segment != 0)
			{
				middlePoint2.m_node = 0;
				middlePoint2.m_segment = 0;
				middlePoint2.m_position = (this.m_controlPoints[0].m_position + this.m_controlPoints[1].m_position) * 0.5f;
				ushort num22;
				ushort num23;
				toolErrors = NetTool.CreateNode(netInfo, this.m_controlPoints[this.m_controlPointCount - 1], middlePoint2, this.m_controlPoints[this.m_controlPointCount], NetTool.m_nodePositionsSimulation, 1000, true, false, true, flag9, false, this.m_switchingDir, 0, out num22, out num23, out num20, out productionRate);
			}
			else
			{
				this.m_toolController.ClearColliding();
				toolErrors = ToolBase.ToolErrors.None;
				num20 = 0;
				productionRate = 0;
			}
		}
		else
		{
			this.m_toolController.ClearColliding();
			toolErrors = ToolBase.ToolErrors.None;
			num20 = 0;
			productionRate = 0;
		}
		if (flag)
		{
			toolErrors |= ToolBase.ToolErrors.RaycastFailed;
		}
		while (!Monitor.TryEnter(this.m_cacheLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_buildErrors = toolErrors;
			if (flag2)
			{
				this.m_constructionCost = ((!flag9) ? 0 : (-num20));
			}
			else
			{
				this.m_constructionCost = ((!flag9) ? 0 : num20);
			}
			this.m_productionRate = productionRate;
		}
		finally
		{
			Monitor.Exit(this.m_cacheLock);
		}
		if (this.m_mode == NetTool.Mode.Upgrade && this.m_upgrading && this.m_controlPointCount == 2 && this.m_buildErrors == ToolBase.ToolErrors.None)
		{
			this.CreateNodeImpl(this.m_switchingDir);
		}
	}

	private static void TryAddSecondaryDirection(Vector3 d)
	{
		for (int i = 0; i < NetTool.m_primaryDirections.m_size; i++)
		{
			Vector3 direction = NetTool.m_primaryDirections.m_buffer[i].m_direction;
			if (direction.x * d.x + direction.z * d.z > 0.75f)
			{
				return;
			}
		}
		for (int j = 0; j < NetTool.m_secondaryDirections.m_size; j++)
		{
			Vector3 vector = NetTool.m_secondaryDirections.m_buffer[j];
			if (vector.x * d.x + vector.z * d.z > 0.995f)
			{
				return;
			}
		}
		NetTool.m_secondaryDirections.Add(d);
	}

	private static void RefreshHelperLines(FastList<NetTool.HelperLine> lines, NetTool.ControlPoint oldPoint, NetTool.ControlPoint newPoint, int layers, bool canSnap, out Vector3 snapPos, out Vector3 snapDir)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		float num = 260f;
		NetTool.m_directionBuffer.Clear();
		Vector3 position = newPoint.m_position;
		snapPos = position;
		snapDir = Vector3.get_zero();
		float num2 = position.x - num;
		float num3 = position.z - num;
		float num4 = position.x + num;
		float num5 = position.z + num;
		int num6 = Mathf.Max((int)(num2 / 64f + 135f), 0);
		int num7 = Mathf.Max((int)(num3 / 64f + 135f), 0);
		int num8 = Mathf.Min((int)(num4 / 64f + 135f), 269);
		int num9 = Mathf.Min((int)(num5 / 64f + 135f), 269);
		bool flag = (newPoint.m_position - oldPoint.m_position).get_sqrMagnitude() >= 1f;
		for (int i = num7; i <= num9; i++)
		{
			for (int j = num6; j <= num8; j++)
			{
				ushort num10 = instance.m_nodeGrid[i * 270 + j];
				int num11 = 0;
				while (num10 != 0)
				{
					NetNode.Flags flags = instance.m_nodes.m_buffer[(int)num10].m_flags;
					if (num10 != oldPoint.m_node && (flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) == NetNode.Flags.Created)
					{
						NetInfo info = instance.m_nodes.m_buffer[(int)num10].Info;
						if ((layers & 1 << info.m_prefabDataLayer) != 0)
						{
							Vector3 position2 = instance.m_nodes.m_buffer[(int)num10].m_position;
							if (VectorUtils.LengthSqrXZ(position2 - position) < num * num)
							{
								NetTool.m_primaryDirections.Clear();
								NetTool.m_secondaryDirections.Clear();
								for (int k = 0; k < 8; k++)
								{
									ushort segment = instance.m_nodes.m_buffer[(int)num10].GetSegment(k);
									if (segment != 0)
									{
										NetTool.HelperDirection item;
										item.m_direction = instance.m_segments.m_buffer[(int)segment].GetDirection(num10);
										item.m_valid = instance.m_segments.m_buffer[(int)segment].IsStraight();
										ushort otherNode = instance.m_segments.m_buffer[(int)segment].GetOtherNode(num10);
										if ((instance.m_nodes.m_buffer[(int)otherNode].m_flags & (NetNode.Flags.Middle | NetNode.Flags.Moveable)) != (NetNode.Flags.Middle | NetNode.Flags.Moveable))
										{
											Vector3 position3 = instance.m_nodes.m_buffer[(int)otherNode].m_position;
											if (VectorUtils.LengthSqrXZ(position2 - position3) < 100f)
											{
												item.m_valid = false;
											}
										}
										NetTool.m_primaryDirections.Add(item);
									}
								}
								NetTool.HelperDirection2 item2;
								item2.m_netInfo = info;
								item2.m_position = position2;
								item2.m_distance = 0f;
								item2.m_offset = 0f;
								int num12 = (int)num10 << 16;
								for (int l = 0; l < NetTool.m_primaryDirections.m_size; l++)
								{
									Vector3 direction = NetTool.m_primaryDirections.m_buffer[l].m_direction;
									if ((flags & (NetNode.Flags.Middle | NetNode.Flags.Moveable)) != (NetNode.Flags.Middle | NetNode.Flags.Moveable))
									{
										if (NetTool.m_primaryDirections.m_buffer[l].m_valid)
										{
											item2.m_direction = direction;
											item2.m_id = num12++;
											if (flag)
											{
												float num13;
												Vector3 vector = VectorUtils.NormalizeXZ(oldPoint.m_position - position2, ref num13);
												if (vector.x * item2.m_direction.x + vector.z * item2.m_direction.z < 0.995f && num13 >= 7.5f)
												{
													NetTool.m_directionBuffer.Add(item2);
												}
											}
											else
											{
												NetTool.m_directionBuffer.Add(item2);
											}
										}
										NetTool.TryAddSecondaryDirection(-direction);
									}
									NetTool.TryAddSecondaryDirection(new Vector3(direction.z, 0f, -direction.x));
									NetTool.TryAddSecondaryDirection(new Vector3(-direction.z, 0f, direction.x));
								}
								for (int m = 0; m < NetTool.m_secondaryDirections.m_size; m++)
								{
									item2.m_direction = NetTool.m_secondaryDirections.m_buffer[m];
									item2.m_id = num12++;
									if (flag)
									{
										float num14;
										Vector3 vector2 = VectorUtils.NormalizeXZ(oldPoint.m_position - position2, ref num14);
										if (vector2.x * item2.m_direction.x + vector2.z * item2.m_direction.z < 0.995f && num14 >= 7.5f)
										{
											NetTool.m_directionBuffer.Add(item2);
										}
									}
									else
									{
										NetTool.m_directionBuffer.Add(item2);
									}
								}
							}
						}
					}
					num10 = instance.m_nodes.m_buffer[(int)num10].m_nextGridNode;
					if (++num11 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		float num15 = 1000f;
		Vector3 vector3 = Vector3.get_forward();
		for (int n = 0; n < NetTool.m_directionBuffer.m_size; n++)
		{
			NetTool.HelperDirection2 helperDirection = NetTool.m_directionBuffer.m_buffer[n];
			Vector3 vector4 = helperDirection.m_position - position;
			float num16 = -helperDirection.m_direction.x * vector4.x - helperDirection.m_direction.z * vector4.z;
			float num17;
			if (num16 < 0f)
			{
				num16 = -num16;
				num17 = VectorUtils.LengthXZ(vector4);
			}
			else if (num16 > 240f)
			{
				num16 = 240f;
				num17 = VectorUtils.LengthXZ(helperDirection.m_position + helperDirection.m_direction * num16 - position);
			}
			else
			{
				num17 = Mathf.Abs(helperDirection.m_direction.x * vector4.z - helperDirection.m_direction.z * vector4.x);
			}
			if (num16 < num15 && num17 < 20f)
			{
				num15 = num16;
				vector3 = helperDirection.m_direction;
			}
			NetTool.m_directionBuffer.m_buffer[n].m_distance = num17;
			NetTool.m_directionBuffer.m_buffer[n].m_offset = num16;
		}
		float num18 = 1000f;
		float num19 = 1000f;
		float num20 = 1000f;
		for (int num21 = 0; num21 < NetTool.m_directionBuffer.m_size; num21++)
		{
			NetTool.HelperDirection2 helperDirection2 = NetTool.m_directionBuffer.m_buffer[num21];
			if (helperDirection2.m_distance < 20f)
			{
				float num22 = helperDirection2.m_direction.x * vector3.x + helperDirection2.m_direction.z * vector3.z;
				float num23 = helperDirection2.m_direction.x * vector3.z - helperDirection2.m_direction.z * vector3.x;
				if (num22 < 0f)
				{
					num18 = Mathf.Min(num18, -helperDirection2.m_offset / num22);
				}
				if (num23 > 0f)
				{
					num19 = Mathf.Min(num19, helperDirection2.m_offset / num23);
				}
				else if (num23 < 0f)
				{
					num20 = Mathf.Min(num20, -helperDirection2.m_offset / num23);
				}
			}
		}
		float num24 = 7.5f;
		float num25 = 0f;
		int num26 = -1;
		for (int num27 = 0; num27 < NetTool.m_directionBuffer.m_size; num27++)
		{
			NetTool.HelperDirection2 helperDirection3 = NetTool.m_directionBuffer.m_buffer[num27];
			float num28 = 1000f;
			float num29 = helperDirection3.m_direction.x * vector3.x + helperDirection3.m_direction.z * vector3.z;
			float num30 = helperDirection3.m_direction.x * vector3.z - helperDirection3.m_direction.z * vector3.x;
			if (num29 > 0f)
			{
				num28 = Mathf.Min(num28, num15 / num29);
			}
			else if (num29 < 0f)
			{
				num28 = Mathf.Min(num28, -num18 / num29);
			}
			if (num30 > 0f)
			{
				num28 = Mathf.Min(num28, num19 / num30);
			}
			else if (num30 < 0f)
			{
				num28 = Mathf.Min(num28, -num20 / num30);
			}
			if (helperDirection3.m_offset > num28)
			{
				float distance = VectorUtils.LengthXZ(helperDirection3.m_position + helperDirection3.m_direction * num28 - position);
				helperDirection3.m_distance = distance;
				helperDirection3.m_offset = num28;
				NetTool.m_directionBuffer.m_buffer[num27].m_distance = distance;
				NetTool.m_directionBuffer.m_buffer[num27].m_offset = num28;
			}
			if (canSnap && helperDirection3.m_distance < num24)
			{
				num24 = helperDirection3.m_distance;
				float num31 = (float)Mathf.RoundToInt(helperDirection3.m_offset / 8f) * 8f;
				snapPos = helperDirection3.m_position + helperDirection3.m_direction * num31;
				snapDir = helperDirection3.m_direction;
				num26 = num27;
				num25 = num31;
			}
		}
		if (num26 != -1)
		{
			NetTool.m_directionBuffer.m_buffer[num26].m_distance = -1f;
			NetTool.m_directionBuffer.m_buffer[num26].m_offset = num25;
			num24 = 7.5f;
			Vector3 vector5 = snapPos;
			int num32 = -1;
			float offset = 0f;
			float offset2 = 0f;
			for (int num33 = 0; num33 < NetTool.m_directionBuffer.m_size; num33++)
			{
				NetTool.HelperDirection2 helperDirection4 = NetTool.m_directionBuffer.m_buffer[num33];
				if (helperDirection4.m_distance < 7.5f)
				{
					Vector3 vector6 = helperDirection4.m_position + helperDirection4.m_direction * helperDirection4.m_offset;
					Vector3 direction2 = helperDirection4.m_direction;
					float num34 = direction2.x * snapDir.x + direction2.z * snapDir.z;
					if (Mathf.Abs(num34) < 0.995f)
					{
						Line2 line;
						line..ctor(VectorUtils.XZ(snapPos), VectorUtils.XZ(snapPos + snapDir));
						Line2 line2;
						line2..ctor(VectorUtils.XZ(vector6), VectorUtils.XZ(vector6 + direction2));
						float num35;
						float num36;
						if (line.Intersect(line2, ref num35, ref num36))
						{
							vector6 += direction2 * num36;
							float num37 = VectorUtils.LengthXZ(vector6 - position);
							if (num37 < num24)
							{
								num24 = num37;
								vector5 = vector6;
								num32 = num33;
								offset = num25 + num35;
								offset2 = helperDirection4.m_offset + num36;
							}
						}
					}
				}
			}
			if (num32 != -1)
			{
				snapPos = vector5;
				NetTool.m_directionBuffer.m_buffer[num26].m_offset = offset;
				NetTool.m_directionBuffer.m_buffer[num32].m_distance = -1f;
				NetTool.m_directionBuffer.m_buffer[num32].m_offset = offset2;
			}
		}
		while (!Monitor.TryEnter(lines, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			lines.Clear();
			for (int num38 = 0; num38 < NetTool.m_directionBuffer.m_size; num38++)
			{
				NetTool.HelperDirection2 helperDirection5 = NetTool.m_directionBuffer.m_buffer[num38];
				if (helperDirection5.m_distance < 200f)
				{
					NetTool.HelperLine item3;
					item3.m_netInfo = helperDirection5.m_netInfo;
					if (canSnap)
					{
						item3.m_line = new Line3(helperDirection5.m_position, helperDirection5.m_position + helperDirection5.m_direction * helperDirection5.m_offset);
					}
					else
					{
						item3.m_line = new Line3(helperDirection5.m_position, helperDirection5.m_position);
					}
					item3.m_distance = helperDirection5.m_distance;
					item3.m_id = helperDirection5.m_id;
					lines.Add(item3);
				}
			}
		}
		finally
		{
			Monitor.Exit(lines);
		}
	}

	private static void ClearHelperLines(FastList<NetTool.HelperLine> lines)
	{
		while (!Monitor.TryEnter(lines, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			lines.Clear();
		}
		finally
		{
			Monitor.Exit(lines);
		}
	}

	public static NetTool.ControlPoint SnapDirection(NetTool.ControlPoint newPoint, NetTool.ControlPoint oldPoint, NetInfo info, out bool success, out float minDistanceSq)
	{
		minDistanceSq = info.GetMinNodeDistance();
		minDistanceSq *= minDistanceSq;
		NetTool.ControlPoint result = newPoint;
		success = false;
		if (oldPoint.m_node != 0)
		{
			NetNode netNode = Singleton<NetManager>.get_instance().m_nodes.m_buffer[(int)oldPoint.m_node];
			for (int i = 0; i < 8; i++)
			{
				ushort segment = netNode.GetSegment(i);
				if (segment != 0)
				{
					NetSegment netSegment = Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)segment];
					Vector3 vector = (netSegment.m_startNode != oldPoint.m_node) ? netSegment.m_endDirection : netSegment.m_startDirection;
					vector.y = 0f;
					if (newPoint.m_node == 0 && !newPoint.m_outside)
					{
						Vector3 vector2 = Line2.Offset(VectorUtils.XZ(vector), VectorUtils.XZ(oldPoint.m_position - newPoint.m_position));
						float sqrMagnitude = vector2.get_sqrMagnitude();
						if (sqrMagnitude < minDistanceSq)
						{
							vector2 = newPoint.m_position + vector2 - oldPoint.m_position;
							float num = vector2.x * vector.x + vector2.z * vector.z;
							result.m_position = oldPoint.m_position + vector * num;
							if (info.m_useFixedHeight)
							{
								result.m_position.y = newPoint.m_position.y;
							}
							else
							{
								result.m_position.y = NetSegment.SampleTerrainHeight(info, result.m_position, false, result.m_elevation);
							}
							result.m_direction = ((num >= 0f) ? vector : (-vector));
							minDistanceSq = sqrMagnitude;
							success = true;
						}
						if (info.m_maxBuildAngle > 89f)
						{
							vector..ctor(vector.z, 0f, -vector.x);
							vector2 = Line2.Offset(VectorUtils.XZ(vector), VectorUtils.XZ(oldPoint.m_position - newPoint.m_position));
							sqrMagnitude = vector2.get_sqrMagnitude();
							if (sqrMagnitude < minDistanceSq)
							{
								vector2 = newPoint.m_position + vector2 - oldPoint.m_position;
								float num2 = vector2.x * vector.x + vector2.z * vector.z;
								result.m_position = oldPoint.m_position + vector * num2;
								if (info.m_useFixedHeight)
								{
									result.m_position.y = newPoint.m_position.y;
								}
								else
								{
									result.m_position.y = NetSegment.SampleTerrainHeight(info, result.m_position, false, result.m_elevation);
								}
								result.m_direction = ((num2 >= 0f) ? vector : (-vector));
								minDistanceSq = sqrMagnitude;
								success = true;
							}
						}
					}
					else
					{
						float num3 = newPoint.m_direction.x * vector.x + newPoint.m_direction.z * vector.z;
						if (num3 > 0.999f)
						{
							result.m_direction = vector;
							success = true;
						}
						if (num3 < -0.999f)
						{
							result.m_direction = -vector;
							success = true;
						}
						if (info.m_maxBuildAngle > 89f)
						{
							vector..ctor(vector.z, 0f, -vector.x);
							num3 = newPoint.m_direction.x * vector.x + newPoint.m_direction.z * vector.z;
							if (num3 > 0.999f)
							{
								result.m_direction = vector;
								success = true;
							}
							if (num3 < -0.999f)
							{
								result.m_direction = -vector;
								success = true;
							}
						}
					}
				}
			}
		}
		else if (oldPoint.m_direction.get_sqrMagnitude() > 0.5f)
		{
			Vector3 direction = oldPoint.m_direction;
			if (newPoint.m_node == 0 && !newPoint.m_outside)
			{
				Vector3 vector3 = Line2.Offset(VectorUtils.XZ(direction), VectorUtils.XZ(oldPoint.m_position - newPoint.m_position));
				float sqrMagnitude2 = vector3.get_sqrMagnitude();
				if (sqrMagnitude2 < minDistanceSq)
				{
					vector3 = newPoint.m_position + vector3 - oldPoint.m_position;
					float num4 = vector3.x * direction.x + vector3.z * direction.z;
					result.m_position = oldPoint.m_position + direction * num4;
					if (info.m_useFixedHeight)
					{
						result.m_position.y = newPoint.m_position.y;
					}
					else
					{
						result.m_position.y = NetSegment.SampleTerrainHeight(info, result.m_position, false, result.m_elevation);
					}
					result.m_direction = ((num4 >= 0f) ? direction : (-direction));
					minDistanceSq = sqrMagnitude2;
					success = true;
				}
				direction..ctor(direction.z, 0f, -direction.x);
				vector3 = Line2.Offset(VectorUtils.XZ(direction), VectorUtils.XZ(oldPoint.m_position - newPoint.m_position));
				sqrMagnitude2 = vector3.get_sqrMagnitude();
				if (sqrMagnitude2 < minDistanceSq)
				{
					vector3 = newPoint.m_position + vector3 - oldPoint.m_position;
					float num5 = vector3.x * direction.x + vector3.z * direction.z;
					result.m_position = oldPoint.m_position + direction * num5;
					if (info.m_useFixedHeight)
					{
						result.m_position.y = newPoint.m_position.y;
					}
					else
					{
						result.m_position.y = NetSegment.SampleTerrainHeight(info, result.m_position, false, result.m_elevation);
					}
					result.m_direction = ((num5 >= 0f) ? direction : (-direction));
					minDistanceSq = sqrMagnitude2;
					success = true;
				}
			}
			else
			{
				float num6 = newPoint.m_direction.x * direction.x + newPoint.m_direction.z * direction.z;
				if (num6 > 0.999f)
				{
					result.m_direction = direction;
					success = true;
				}
				if (num6 < -0.999f)
				{
					result.m_direction = -direction;
					success = true;
				}
				if (info.m_maxBuildAngle > 89f)
				{
					direction..ctor(direction.z, 0f, -direction.x);
					num6 = newPoint.m_direction.x * direction.x + newPoint.m_direction.z * direction.z;
					if (num6 > 0.999f)
					{
						result.m_direction = direction;
						success = true;
					}
					if (num6 < -0.999f)
					{
						result.m_direction = -direction;
						success = true;
					}
				}
			}
		}
		return result;
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_buildErrors;
	}

	public NetTool.Mode m_mode;

	public NetTool.Snapping m_snap = NetTool.Snapping.All;

	public int m_elevationDivider = 1;

	public NetInfo m_prefab;

	public CursorInfo m_placementCursor;

	public CursorInfo m_upgradeCursor;

	private NetTool.ControlPoint[] m_controlPoints;

	private NetTool.ControlPoint[] m_cachedControlPoints;

	private int m_controlPointCount;

	private int m_cachedControlPointCount;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private bool m_mouseRayValid;

	private ToolBase.ToolErrors m_buildErrors;

	private ToolBase.ToolErrors m_cachedErrors;

	private ushort[] m_closeSegments;

	private int m_closeSegmentCount;

	private int m_elevation;

	private float m_lengthTimer;

	private bool m_lengthChanging;

	private bool m_upgrading;

	private bool m_switchingDir;

	private bool m_repairing;

	private HashSet<ushort> m_upgradedSegments;

	private FastList<ushort> m_tempUpgraded;

	private int m_constructionCost;

	private int m_productionRate;

	private object m_cacheLock;

	private int m_lastAngle;

	private bool m_angleShown;

	private BulldozeTool m_bulldozerTool;

	private SavedInputKey m_buildElevationUp;

	private SavedInputKey m_buildElevationDown;

	private Dictionary<int, NetTool.HelperLineTimer> m_helperLineTimer;

	[NonSerialized]
	public static FastList<NetTool.NodePosition> m_nodePositionsMain = new FastList<NetTool.NodePosition>();

	[NonSerialized]
	public static FastList<NetTool.NodePosition> m_nodePositionsSimulation = new FastList<NetTool.NodePosition>();

	[NonSerialized]
	public static FastList<NetTool.HelperLine> m_helperLines = new FastList<NetTool.HelperLine>();

	private static FastList<NetTool.HelperBufferLine> m_helperLineBuffer = new FastList<NetTool.HelperBufferLine>();

	private static FastList<NetTool.HelperDirection> m_primaryDirections = new FastList<NetTool.HelperDirection>();

	private static FastList<Vector3> m_secondaryDirections = new FastList<Vector3>();

	private static FastList<NetTool.HelperDirection2> m_directionBuffer = new FastList<NetTool.HelperDirection2>();

	public enum Mode
	{
		[EnumPosition(0)]
		Straight,
		[EnumPosition(1)]
		Curved,
		[EnumPosition(2)]
		Freeform,
		[EnumPosition(3)]
		Upgrade
	}

	[Flags]
	public enum Snapping
	{
		None = 0,
		Angle = 1,
		Length = 2,
		Grid = 4,
		HelperLine = 8,
		All = 15
	}

	public struct ControlPoint
	{
		public Vector3 m_position;

		public Vector3 m_direction;

		public ushort m_node;

		public ushort m_segment;

		public float m_elevation;

		public bool m_outside;
	}

	public struct NodePosition
	{
		public NetInfo m_nodeInfo;

		public Vector3 m_position;

		public Vector3 m_direction;

		public float m_minY;

		public float m_maxY;

		public float m_terrainHeight;

		public float m_elevation;

		public bool m_double;
	}

	public struct HelperLine
	{
		public NetInfo m_netInfo;

		public Line3 m_line;

		public float m_distance;

		public int m_id;
	}

	public struct HelperLineTimer
	{
		public float m_nodeTimer;

		public float m_lineTimer;
	}

	private struct HelperBufferLine
	{
		public NetTool.HelperLine m_line;

		public NetTool.HelperLineTimer m_timer;
	}

	private struct HelperDirection
	{
		public Vector3 m_direction;

		public bool m_valid;
	}

	private struct HelperDirection2
	{
		public NetInfo m_netInfo;

		public Vector3 m_position;

		public Vector3 m_direction;

		public float m_distance;

		public float m_offset;

		public int m_id;
	}
}
