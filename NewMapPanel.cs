﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class NewMapPanel : UICustomControl
{
	private void Awake()
	{
		base.Find<UITabstrip>("Tabstrip").set_selectedIndex(-1);
	}

	public void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().CenterToParent();
			base.get_component().Focus();
		}
		else
		{
			ToolsMenu.SetFocus();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}
}
