﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class VehicleManager : SimulationManagerBase<VehicleManager, VehicleProperties>, ISimulationManager, IRenderableManager, IAudibleManager
{
	private static int GetTransferIndex(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		int num;
		if (subService != ItemClass.SubService.None)
		{
			num = 20 + subService - ItemClass.SubService.ResidentialLow;
		}
		else
		{
			num = service - ItemClass.Service.Residential;
		}
		return (int)(num * 5 + level);
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_vehicles = new Array16<Vehicle>(16384u);
		this.m_parkedVehicles = new Array16<VehicleParked>(32768u);
		this.m_updatedParked = new ulong[512];
		this.m_renderBuffer = new ulong[256];
		this.m_renderBuffer2 = new ulong[512];
		this.m_vehicleGrid = new ushort[291600];
		this.m_vehicleGrid2 = new ushort[2916];
		this.m_parkedGrid = new ushort[291600];
		this.m_transferVehicles = new FastList<ushort>[230];
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_TyreMatrix = Shader.PropertyToID("_TyreMatrix");
		this.ID_TyrePosition = Shader.PropertyToID("_TyrePosition");
		this.ID_LightState = Shader.PropertyToID("_LightState");
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_MainTex = Shader.PropertyToID("_MainTex");
		this.ID_XYSMap = Shader.PropertyToID("_XYSMap");
		this.ID_ACIMap = Shader.PropertyToID("_ACIMap");
		this.ID_AtlasRect = Shader.PropertyToID("_AtlasRect");
		this.ID_VehicleTransform = Shader.PropertyToID("_VehicleTransform");
		this.ID_VehicleLightState = Shader.PropertyToID("_VehicleLightState");
		this.ID_VehicleColor = Shader.PropertyToID("_VehicleColor");
		this.ID_TyreLocation = Shader.PropertyToID("_TyreLocation");
		this.m_audioGroup = new AudioGroup(5, new SavedFloat(Settings.effectAudioVolume, Settings.gameSettingsFile, DefaultSettings.effectAudioVolume, true));
		this.m_undergroundLayer = LayerMask.NameToLayer("MetroTunnels");
		ushort num;
		this.m_vehicles.CreateItem(out num);
		this.m_parkedVehicles.CreateItem(out num);
	}

	private void OnDestroy()
	{
		if (this.m_lodRgbAtlas != null)
		{
			Object.Destroy(this.m_lodRgbAtlas);
			this.m_lodRgbAtlas = null;
		}
		if (this.m_lodXysAtlas != null)
		{
			Object.Destroy(this.m_lodXysAtlas);
			this.m_lodXysAtlas = null;
		}
		if (this.m_lodAciAtlas != null)
		{
			Object.Destroy(this.m_lodAciAtlas);
			this.m_lodAciAtlas = null;
		}
	}

	public override void InitializeProperties(VehicleProperties properties)
	{
		base.InitializeProperties(properties);
	}

	public override void DestroyProperties(VehicleProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_audioGroup != null)
			{
				this.m_audioGroup.Reset();
			}
			if (this.m_lodRgbAtlas != null)
			{
				Object.Destroy(this.m_lodRgbAtlas);
				this.m_lodRgbAtlas = null;
			}
			if (this.m_lodXysAtlas != null)
			{
				Object.Destroy(this.m_lodXysAtlas);
				this.m_lodXysAtlas = null;
			}
			if (this.m_lodAciAtlas != null)
			{
				Object.Destroy(this.m_lodAciAtlas);
				this.m_lodAciAtlas = null;
			}
		}
		base.DestroyProperties(properties);
	}

	public override void CheckReferences()
	{
		base.CheckReferences();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.CheckReferencesImpl());
	}

	[DebuggerHidden]
	private IEnumerator CheckReferencesImpl()
	{
		return new VehicleManager.<CheckReferencesImpl>c__Iterator0();
	}

	public override void InitRenderData()
	{
		base.InitRenderData();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitRenderDataImpl());
	}

	[DebuggerHidden]
	private IEnumerator InitRenderDataImpl()
	{
		VehicleManager.<InitRenderDataImpl>c__Iterator1 <InitRenderDataImpl>c__Iterator = new VehicleManager.<InitRenderDataImpl>c__Iterator1();
		<InitRenderDataImpl>c__Iterator.$this = this;
		return <InitRenderDataImpl>c__Iterator;
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		float levelOfDetailFactor = RenderManager.LevelOfDetailFactor;
		float near = cameraInfo.m_near;
		float num = Mathf.Min(levelOfDetailFactor * 5000f, Mathf.Min(levelOfDetailFactor * 2000f + cameraInfo.m_height * 0.6f, cameraInfo.m_far));
		Vector3 vector = cameraInfo.m_position + cameraInfo.m_directionA * near;
		Vector3 vector2 = cameraInfo.m_position + cameraInfo.m_directionB * near;
		Vector3 vector3 = cameraInfo.m_position + cameraInfo.m_directionC * near;
		Vector3 vector4 = cameraInfo.m_position + cameraInfo.m_directionD * near;
		Vector3 vector5 = cameraInfo.m_position + cameraInfo.m_directionA * num;
		Vector3 vector6 = cameraInfo.m_position + cameraInfo.m_directionB * num;
		Vector3 vector7 = cameraInfo.m_position + cameraInfo.m_directionC * num;
		Vector3 vector8 = cameraInfo.m_position + cameraInfo.m_directionD * num;
		Vector3 vector9 = Vector3.Min(Vector3.Min(Vector3.Min(vector, vector2), Vector3.Min(vector3, vector4)), Vector3.Min(Vector3.Min(vector5, vector6), Vector3.Min(vector7, vector8)));
		Vector3 vector10 = Vector3.Max(Vector3.Max(Vector3.Max(vector, vector2), Vector3.Max(vector3, vector4)), Vector3.Max(Vector3.Max(vector5, vector6), Vector3.Max(vector7, vector8)));
		int num2 = Mathf.Max((int)((vector9.x - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Max((int)((vector9.z - 10f) / 32f + 270f), 0);
		int num4 = Mathf.Min((int)((vector10.x + 10f) / 32f + 270f), 539);
		int num5 = Mathf.Min((int)((vector10.z + 10f) / 32f + 270f), 539);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = this.m_vehicleGrid[i * 540 + j];
				if (num6 != 0)
				{
					this.m_renderBuffer[num6 >> 6] |= 1uL << (int)num6;
				}
			}
		}
		float near2 = cameraInfo.m_near;
		float num7 = Mathf.Min(2000f, cameraInfo.m_far);
		Vector3 vector11 = cameraInfo.m_position + cameraInfo.m_directionA * near2;
		Vector3 vector12 = cameraInfo.m_position + cameraInfo.m_directionB * near2;
		Vector3 vector13 = cameraInfo.m_position + cameraInfo.m_directionC * near2;
		Vector3 vector14 = cameraInfo.m_position + cameraInfo.m_directionD * near2;
		Vector3 vector15 = cameraInfo.m_position + cameraInfo.m_directionA * num7;
		Vector3 vector16 = cameraInfo.m_position + cameraInfo.m_directionB * num7;
		Vector3 vector17 = cameraInfo.m_position + cameraInfo.m_directionC * num7;
		Vector3 vector18 = cameraInfo.m_position + cameraInfo.m_directionD * num7;
		Vector3 vector19 = Vector3.Min(Vector3.Min(Vector3.Min(vector11, vector12), Vector3.Min(vector13, vector14)), Vector3.Min(Vector3.Min(vector15, vector16), Vector3.Min(vector17, vector18)));
		Vector3 vector20 = Vector3.Max(Vector3.Max(Vector3.Max(vector11, vector12), Vector3.Max(vector13, vector14)), Vector3.Max(Vector3.Max(vector15, vector16), Vector3.Max(vector17, vector18)));
		int num8 = Mathf.Max((int)((vector19.x - 10f) / 32f + 270f), 0);
		int num9 = Mathf.Max((int)((vector19.z - 10f) / 32f + 270f), 0);
		int num10 = Mathf.Min((int)((vector20.x + 10f) / 32f + 270f), 539);
		int num11 = Mathf.Min((int)((vector20.z + 10f) / 32f + 270f), 539);
		for (int k = num9; k <= num11; k++)
		{
			for (int l = num8; l <= num10; l++)
			{
				ushort num12 = this.m_parkedGrid[k * 540 + l];
				if (num12 != 0)
				{
					this.m_renderBuffer2[num12 >> 6] |= 1uL << (int)num12;
				}
			}
		}
		float near3 = cameraInfo.m_near;
		float num13 = Mathf.Min(10000f, cameraInfo.m_far);
		Vector3 vector21 = cameraInfo.m_position + cameraInfo.m_directionA * near3;
		Vector3 vector22 = cameraInfo.m_position + cameraInfo.m_directionB * near3;
		Vector3 vector23 = cameraInfo.m_position + cameraInfo.m_directionC * near3;
		Vector3 vector24 = cameraInfo.m_position + cameraInfo.m_directionD * near3;
		Vector3 vector25 = cameraInfo.m_position + cameraInfo.m_directionA * num13;
		Vector3 vector26 = cameraInfo.m_position + cameraInfo.m_directionB * num13;
		Vector3 vector27 = cameraInfo.m_position + cameraInfo.m_directionC * num13;
		Vector3 vector28 = cameraInfo.m_position + cameraInfo.m_directionD * num13;
		Vector3 vector29 = Vector3.Min(Vector3.Min(Vector3.Min(vector21, vector22), Vector3.Min(vector23, vector24)), Vector3.Min(Vector3.Min(vector25, vector26), Vector3.Min(vector27, vector28)));
		Vector3 vector30 = Vector3.Max(Vector3.Max(Vector3.Max(vector21, vector22), Vector3.Max(vector23, vector24)), Vector3.Max(Vector3.Max(vector25, vector26), Vector3.Max(vector27, vector28)));
		if (cameraInfo.m_shadowOffset.x < 0f)
		{
			vector30.x = Mathf.Min(cameraInfo.m_position.x + num13, vector30.x - cameraInfo.m_shadowOffset.x);
		}
		else
		{
			vector29.x = Mathf.Max(cameraInfo.m_position.x - num13, vector29.x - cameraInfo.m_shadowOffset.x);
		}
		if (cameraInfo.m_shadowOffset.z < 0f)
		{
			vector30.z = Mathf.Min(cameraInfo.m_position.z + num13, vector30.z - cameraInfo.m_shadowOffset.z);
		}
		else
		{
			vector29.z = Mathf.Max(cameraInfo.m_position.z - num13, vector29.z - cameraInfo.m_shadowOffset.z);
		}
		int num14 = Mathf.Max((int)((vector29.x - 50f) / 320f + 27f), 0);
		int num15 = Mathf.Max((int)((vector29.z - 50f) / 320f + 27f), 0);
		int num16 = Mathf.Min((int)((vector30.x + 50f) / 320f + 27f), 53);
		int num17 = Mathf.Min((int)((vector30.z + 50f) / 320f + 27f), 53);
		for (int m = num15; m <= num17; m++)
		{
			for (int n = num14; n <= num16; n++)
			{
				ushort num18 = this.m_vehicleGrid2[m * 54 + n];
				if (num18 != 0)
				{
					this.m_renderBuffer[num18 >> 6] |= 1uL << (int)num18;
				}
			}
		}
		int num19 = this.m_renderBuffer.Length;
		for (int num20 = 0; num20 < num19; num20++)
		{
			ulong num21 = this.m_renderBuffer[num20];
			if (num21 != 0uL)
			{
				for (int num22 = 0; num22 < 64; num22++)
				{
					ulong num23 = 1uL << num22;
					if ((num21 & num23) != 0uL)
					{
						ushort num24 = (ushort)(num20 << 6 | num22);
						if (!this.m_vehicles.m_buffer[(int)num24].RenderInstance(cameraInfo, num24))
						{
							num21 &= ~num23;
						}
						ushort nextGridVehicle = this.m_vehicles.m_buffer[(int)num24].m_nextGridVehicle;
						int num25 = 0;
						while (nextGridVehicle != 0)
						{
							int num26 = nextGridVehicle >> 6;
							num23 = 1uL << (int)nextGridVehicle;
							if (num26 == num20)
							{
								if ((num21 & num23) != 0uL)
								{
									break;
								}
								num21 |= num23;
							}
							else
							{
								ulong num27 = this.m_renderBuffer[num26];
								if ((num27 & num23) != 0uL)
								{
									break;
								}
								this.m_renderBuffer[num26] = (num27 | num23);
							}
							if (nextGridVehicle > num24)
							{
								break;
							}
							nextGridVehicle = this.m_vehicles.m_buffer[(int)nextGridVehicle].m_nextGridVehicle;
							if (++num25 > 16384)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				this.m_renderBuffer[num20] = num21;
			}
		}
		int num28 = this.m_renderBuffer2.Length;
		for (int num29 = 0; num29 < num28; num29++)
		{
			ulong num30 = this.m_renderBuffer2[num29];
			if (num30 != 0uL)
			{
				for (int num31 = 0; num31 < 64; num31++)
				{
					ulong num32 = 1uL << num31;
					if ((num30 & num32) != 0uL)
					{
						ushort num33 = (ushort)(num29 << 6 | num31);
						if (!this.m_parkedVehicles.m_buffer[(int)num33].RenderInstance(cameraInfo, num33))
						{
							num30 &= ~num32;
						}
						ushort nextGridParked = this.m_parkedVehicles.m_buffer[(int)num33].m_nextGridParked;
						int num34 = 0;
						while (nextGridParked != 0)
						{
							int num35 = nextGridParked >> 6;
							num32 = 1uL << (int)nextGridParked;
							if (num35 == num29)
							{
								if ((num30 & num32) != 0uL)
								{
									break;
								}
								num30 |= num32;
							}
							else
							{
								ulong num36 = this.m_renderBuffer2[num35];
								if ((num36 & num32) != 0uL)
								{
									break;
								}
								this.m_renderBuffer2[num35] = (num36 | num32);
							}
							if (nextGridParked > num33)
							{
								break;
							}
							nextGridParked = this.m_parkedVehicles.m_buffer[(int)nextGridParked].m_nextGridParked;
							if (++num34 > 32768)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				this.m_renderBuffer2[num29] = num30;
			}
		}
		int num37 = PrefabCollection<VehicleInfo>.PrefabCount();
		for (int num38 = 0; num38 < num37; num38++)
		{
			VehicleInfo prefab = PrefabCollection<VehicleInfo>.GetPrefab((uint)num38);
			if (prefab != null)
			{
				if (prefab.m_lodCount != 0)
				{
					Vehicle.RenderLod(cameraInfo, prefab);
				}
				if (prefab.m_undergroundLodCount != 0)
				{
					Vehicle.RenderUndergroundLod(cameraInfo, prefab);
				}
				if (prefab.m_subMeshes != null)
				{
					for (int num39 = 0; num39 < prefab.m_subMeshes.Length; num39++)
					{
						VehicleInfoBase subInfo = prefab.m_subMeshes[num39].m_subInfo;
						if (subInfo != null)
						{
							if (subInfo.m_lodCount != 0)
							{
								Vehicle.RenderLod(cameraInfo, subInfo);
							}
							if (subInfo.m_undergroundLodCount != 0)
							{
								Vehicle.RenderUndergroundLod(cameraInfo, subInfo);
							}
						}
					}
				}
			}
		}
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		if (this.m_properties != null)
		{
			LoadingManager instance = Singleton<LoadingManager>.get_instance();
			SimulationManager instance2 = Singleton<SimulationManager>.get_instance();
			AudioManager instance3 = Singleton<AudioManager>.get_instance();
			if (!instance.m_currentlyLoading)
			{
				int num = Mathf.Max((int)((listenerInfo.m_position.x - 150f) / 32f + 270f), 0);
				int num2 = Mathf.Max((int)((listenerInfo.m_position.z - 150f) / 32f + 270f), 0);
				int num3 = Mathf.Min((int)((listenerInfo.m_position.x + 150f) / 32f + 270f), 539);
				int num4 = Mathf.Min((int)((listenerInfo.m_position.z + 150f) / 32f + 270f), 539);
				for (int i = num2; i <= num4; i++)
				{
					for (int j = num; j <= num3; j++)
					{
						int num5 = i * 540 + j;
						ushort num6 = this.m_vehicleGrid[num5];
						int num7 = 0;
						while (num6 != 0)
						{
							this.m_vehicles.m_buffer[(int)num6].PlayAudio(listenerInfo, num6);
							num6 = this.m_vehicles.m_buffer[(int)num6].m_nextGridVehicle;
							if (++num7 >= 16384)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			if (!instance.m_currentlyLoading)
			{
				int num8 = Mathf.Max((int)((listenerInfo.m_position.x - 600f) / 320f + 27f), 0);
				int num9 = Mathf.Max((int)((listenerInfo.m_position.z - 600f) / 320f + 27f), 0);
				int num10 = Mathf.Min((int)((listenerInfo.m_position.x + 600f) / 320f + 27f), 53);
				int num11 = Mathf.Min((int)((listenerInfo.m_position.z + 600f) / 320f + 27f), 53);
				for (int k = num9; k <= num11; k++)
				{
					for (int l = num8; l <= num10; l++)
					{
						int num12 = k * 54 + l;
						ushort num13 = this.m_vehicleGrid2[num12];
						int num14 = 0;
						while (num13 != 0)
						{
							this.m_vehicles.m_buffer[(int)num13].PlayAudio(listenerInfo, num13);
							num13 = this.m_vehicles.m_buffer[(int)num13].m_nextGridVehicle;
							if (++num14 >= 16384)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			float masterVolume;
			if (instance.m_currentlyLoading || instance2.SimulationPaused || instance3.MuteAll)
			{
				masterVolume = 0f;
			}
			else
			{
				masterVolume = instance3.MasterVolume;
			}
			this.m_audioGroup.UpdatePlayers(listenerInfo, masterVolume);
		}
	}

	public bool CreateVehicle(out ushort vehicle, ref Randomizer r, VehicleInfo info, Vector3 position, TransferManager.TransferReason type, bool transferToSource, bool transferToTarget)
	{
		ushort num;
		if (this.m_vehicles.CreateItem(out num, ref r))
		{
			vehicle = num;
			Vehicle.Frame frame = new Vehicle.Frame(position, Quaternion.get_identity());
			this.m_vehicles.m_buffer[(int)vehicle].m_flags = Vehicle.Flags.Created;
			this.m_vehicles.m_buffer[(int)vehicle].m_flags2 = (Vehicle.Flags2)0;
			if (transferToSource)
			{
				Vehicle[] expr_6D_cp_0 = this.m_vehicles.m_buffer;
				ushort expr_6D_cp_1 = vehicle;
				expr_6D_cp_0[(int)expr_6D_cp_1].m_flags = (expr_6D_cp_0[(int)expr_6D_cp_1].m_flags | Vehicle.Flags.TransferToSource);
			}
			if (transferToTarget)
			{
				Vehicle[] expr_94_cp_0 = this.m_vehicles.m_buffer;
				ushort expr_94_cp_1 = vehicle;
				expr_94_cp_0[(int)expr_94_cp_1].m_flags = (expr_94_cp_0[(int)expr_94_cp_1].m_flags | Vehicle.Flags.TransferToTarget);
			}
			this.m_vehicles.m_buffer[(int)vehicle].Info = info;
			this.m_vehicles.m_buffer[(int)vehicle].m_frame0 = frame;
			this.m_vehicles.m_buffer[(int)vehicle].m_frame1 = frame;
			this.m_vehicles.m_buffer[(int)vehicle].m_frame2 = frame;
			this.m_vehicles.m_buffer[(int)vehicle].m_frame3 = frame;
			this.m_vehicles.m_buffer[(int)vehicle].m_targetPos0 = Vector4.get_zero();
			this.m_vehicles.m_buffer[(int)vehicle].m_targetPos1 = Vector4.get_zero();
			this.m_vehicles.m_buffer[(int)vehicle].m_targetPos2 = Vector4.get_zero();
			this.m_vehicles.m_buffer[(int)vehicle].m_targetPos3 = Vector4.get_zero();
			this.m_vehicles.m_buffer[(int)vehicle].m_sourceBuilding = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_targetBuilding = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_transferType = (byte)type;
			this.m_vehicles.m_buffer[(int)vehicle].m_transferSize = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_waitCounter = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_blockCounter = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_nextGridVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_nextOwnVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_nextGuestVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_nextLineVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_transportLine = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_leadingVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_trailingVehicle = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_cargoParent = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_firstCargo = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_nextCargo = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_citizenUnits = 0u;
			this.m_vehicles.m_buffer[(int)vehicle].m_path = 0u;
			this.m_vehicles.m_buffer[(int)vehicle].m_lastFrame = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_pathPositionIndex = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_lastPathOffset = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_gateIndex = 0;
			this.m_vehicles.m_buffer[(int)vehicle].m_waterSource = 0;
			info.m_vehicleAI.CreateVehicle(vehicle, ref this.m_vehicles.m_buffer[(int)vehicle]);
			info.m_vehicleAI.FrameDataUpdated(vehicle, ref this.m_vehicles.m_buffer[(int)vehicle], ref this.m_vehicles.m_buffer[(int)vehicle].m_frame0);
			this.m_vehicleCount = (int)(this.m_vehicles.ItemCount() - 1u);
			return true;
		}
		vehicle = 0;
		return false;
	}

	public void ReleaseVehicle(ushort vehicle)
	{
		this.ReleaseVehicleImplementation(vehicle, ref this.m_vehicles.m_buffer[(int)vehicle]);
	}

	private void ReleaseVehicleImplementation(ushort vehicle, ref Vehicle data)
	{
		if (data.m_flags != (Vehicle.Flags)0)
		{
			InstanceID id = default(InstanceID);
			id.Vehicle = vehicle;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags |= Vehicle.Flags.Deleted;
			data.Unspawn(vehicle);
			VehicleInfo info = data.Info;
			if (info != null)
			{
				info.m_vehicleAI.ReleaseVehicle(vehicle, ref data);
			}
			if (data.m_leadingVehicle != 0)
			{
				if (this.m_vehicles.m_buffer[(int)data.m_leadingVehicle].m_trailingVehicle == vehicle)
				{
					this.m_vehicles.m_buffer[(int)data.m_leadingVehicle].m_trailingVehicle = 0;
				}
				data.m_leadingVehicle = 0;
			}
			if (data.m_trailingVehicle != 0)
			{
				if (this.m_vehicles.m_buffer[(int)data.m_trailingVehicle].m_leadingVehicle == vehicle)
				{
					this.m_vehicles.m_buffer[(int)data.m_trailingVehicle].m_leadingVehicle = 0;
				}
				data.m_trailingVehicle = 0;
			}
			this.ReleaseWaterSource(vehicle, ref data);
			if (data.m_cargoParent != 0)
			{
				ushort num = 0;
				ushort num2 = this.m_vehicles.m_buffer[(int)data.m_cargoParent].m_firstCargo;
				int num3 = 0;
				while (num2 != 0)
				{
					if (num2 == vehicle)
					{
						if (num == 0)
						{
							this.m_vehicles.m_buffer[(int)data.m_cargoParent].m_firstCargo = data.m_nextCargo;
						}
						else
						{
							this.m_vehicles.m_buffer[(int)num].m_nextCargo = data.m_nextCargo;
						}
						break;
					}
					num = num2;
					num2 = this.m_vehicles.m_buffer[(int)num2].m_nextCargo;
					if (++num3 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				data.m_cargoParent = 0;
				data.m_nextCargo = 0;
			}
			if (data.m_firstCargo != 0)
			{
				ushort num4 = data.m_firstCargo;
				int num5 = 0;
				while (num4 != 0)
				{
					ushort nextCargo = this.m_vehicles.m_buffer[(int)num4].m_nextCargo;
					this.m_vehicles.m_buffer[(int)num4].m_cargoParent = 0;
					this.m_vehicles.m_buffer[(int)num4].m_nextCargo = 0;
					num4 = nextCargo;
					if (++num5 > 16384)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				data.m_firstCargo = 0;
			}
			if (data.m_path != 0u)
			{
				Singleton<PathManager>.get_instance().ReleasePath(data.m_path);
				data.m_path = 0u;
			}
			if (data.m_citizenUnits != 0u)
			{
				Singleton<CitizenManager>.get_instance().ReleaseUnits(data.m_citizenUnits);
				data.m_citizenUnits = 0u;
			}
			data.m_flags = (Vehicle.Flags)0;
			this.m_vehicles.ReleaseItem(vehicle);
			this.m_vehicleCount = (int)(this.m_vehicles.ItemCount() - 1u);
		}
	}

	private void ReleaseWaterSource(ushort vehicle, ref Vehicle data)
	{
		if (data.m_waterSource != 0)
		{
			Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(data.m_waterSource);
			data.m_waterSource = 0;
		}
	}

	public void AddToGrid(ushort vehicle, ref Vehicle data, bool large)
	{
		Vector3 lastFramePosition = data.GetLastFramePosition();
		if (large)
		{
			int gridX = Mathf.Clamp((int)(lastFramePosition.x / 320f + 27f), 0, 53);
			int gridZ = Mathf.Clamp((int)(lastFramePosition.z / 320f + 27f), 0, 53);
			this.AddToGrid(vehicle, ref data, large, gridX, gridZ);
		}
		else
		{
			int gridX2 = Mathf.Clamp((int)(lastFramePosition.x / 32f + 270f), 0, 539);
			int gridZ2 = Mathf.Clamp((int)(lastFramePosition.z / 32f + 270f), 0, 539);
			this.AddToGrid(vehicle, ref data, large, gridX2, gridZ2);
		}
	}

	public void AddToGrid(ushort vehicle, ref Vehicle data, bool large, int gridX, int gridZ)
	{
		if (large)
		{
			int num = gridZ * 54 + gridX;
			data.m_nextGridVehicle = this.m_vehicleGrid2[num];
			this.m_vehicleGrid2[num] = vehicle;
		}
		else
		{
			int num2 = gridZ * 540 + gridX;
			data.m_nextGridVehicle = this.m_vehicleGrid[num2];
			this.m_vehicleGrid[num2] = vehicle;
		}
	}

	public void RemoveFromGrid(ushort vehicle, ref Vehicle data, bool large)
	{
		Vector3 lastFramePosition = data.GetLastFramePosition();
		if (large)
		{
			int gridX = Mathf.Clamp((int)(lastFramePosition.x / 320f + 27f), 0, 53);
			int gridZ = Mathf.Clamp((int)(lastFramePosition.z / 320f + 27f), 0, 53);
			this.RemoveFromGrid(vehicle, ref data, large, gridX, gridZ);
		}
		else
		{
			int gridX2 = Mathf.Clamp((int)(lastFramePosition.x / 32f + 270f), 0, 539);
			int gridZ2 = Mathf.Clamp((int)(lastFramePosition.z / 32f + 270f), 0, 539);
			this.RemoveFromGrid(vehicle, ref data, large, gridX2, gridZ2);
		}
	}

	public void RemoveFromGrid(ushort vehicle, ref Vehicle data, bool large, int gridX, int gridZ)
	{
		if (large)
		{
			int num = gridZ * 54 + gridX;
			ushort num2 = 0;
			ushort num3 = this.m_vehicleGrid2[num];
			int num4 = 0;
			while (num3 != 0)
			{
				if (num3 == vehicle)
				{
					if (num2 == 0)
					{
						this.m_vehicleGrid2[num] = data.m_nextGridVehicle;
					}
					else
					{
						this.m_vehicles.m_buffer[(int)num2].m_nextGridVehicle = data.m_nextGridVehicle;
					}
					break;
				}
				num2 = num3;
				num3 = this.m_vehicles.m_buffer[(int)num3].m_nextGridVehicle;
				if (++num4 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridVehicle = 0;
		}
		else
		{
			int num5 = gridZ * 540 + gridX;
			ushort num6 = 0;
			ushort num7 = this.m_vehicleGrid[num5];
			int num8 = 0;
			while (num7 != 0)
			{
				if (num7 == vehicle)
				{
					if (num6 == 0)
					{
						this.m_vehicleGrid[num5] = data.m_nextGridVehicle;
					}
					else
					{
						this.m_vehicles.m_buffer[(int)num6].m_nextGridVehicle = data.m_nextGridVehicle;
					}
					break;
				}
				num6 = num7;
				num7 = this.m_vehicles.m_buffer[(int)num7].m_nextGridVehicle;
				if (++num8 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridVehicle = 0;
		}
	}

	public bool CreateParkedVehicle(out ushort parked, ref Randomizer r, VehicleInfo info, Vector3 position, Quaternion rotation, uint ownerCitizen)
	{
		ushort num;
		if (this.m_parkedVehicles.CreateItem(out num, ref r))
		{
			parked = num;
			this.m_parkedVehicles.m_buffer[(int)parked].m_flags = 9;
			this.m_parkedVehicles.m_buffer[(int)parked].Info = info;
			this.m_parkedVehicles.m_buffer[(int)parked].m_position = position;
			this.m_parkedVehicles.m_buffer[(int)parked].m_rotation = rotation;
			this.m_parkedVehicles.m_buffer[(int)parked].m_ownerCitizen = ownerCitizen;
			this.m_parkedVehicles.m_buffer[(int)parked].m_travelDistance = 0f;
			this.m_parkedVehicles.m_buffer[(int)parked].m_nextGridParked = 0;
			this.AddToGrid(parked, ref this.m_parkedVehicles.m_buffer[(int)parked]);
			this.m_parkedCount = (int)(this.m_parkedVehicles.ItemCount() - 1u);
			return true;
		}
		parked = 0;
		return false;
	}

	public void ReleaseParkedVehicle(ushort parked)
	{
		this.ReleaseParkedVehicleImplementation(parked, ref this.m_parkedVehicles.m_buffer[(int)parked]);
	}

	private void ReleaseParkedVehicleImplementation(ushort parked, ref VehicleParked data)
	{
		if (data.m_flags != 0)
		{
			InstanceID id = default(InstanceID);
			id.ParkedVehicle = parked;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags |= 2;
			this.RemoveFromGrid(parked, ref data);
			if (data.m_ownerCitizen != 0u)
			{
				Singleton<CitizenManager>.get_instance().m_citizens.m_buffer[(int)((UIntPtr)data.m_ownerCitizen)].m_parkedVehicle = 0;
				data.m_ownerCitizen = 0u;
			}
			data.m_flags = 0;
			this.m_parkedVehicles.ReleaseItem(parked);
			this.m_parkedCount = (int)(this.m_parkedVehicles.ItemCount() - 1u);
		}
	}

	public void AddToGrid(ushort parked, ref VehicleParked data)
	{
		int gridX = Mathf.Clamp((int)(data.m_position.x / 32f + 270f), 0, 539);
		int gridZ = Mathf.Clamp((int)(data.m_position.z / 32f + 270f), 0, 539);
		this.AddToGrid(parked, ref data, gridX, gridZ);
	}

	public void AddToGrid(ushort parked, ref VehicleParked data, int gridX, int gridZ)
	{
		int num = gridZ * 540 + gridX;
		data.m_nextGridParked = this.m_parkedGrid[num];
		this.m_parkedGrid[num] = parked;
	}

	public void RemoveFromGrid(ushort parked, ref VehicleParked data)
	{
		int gridX = Mathf.Clamp((int)(data.m_position.x / 32f + 270f), 0, 539);
		int gridZ = Mathf.Clamp((int)(data.m_position.z / 32f + 270f), 0, 539);
		this.RemoveFromGrid(parked, ref data, gridX, gridZ);
	}

	public void RemoveFromGrid(ushort parked, ref VehicleParked data, int gridX, int gridZ)
	{
		int num = gridZ * 540 + gridX;
		ushort num2 = 0;
		ushort num3 = this.m_parkedGrid[num];
		int num4 = 0;
		while (num3 != 0)
		{
			if (num3 == parked)
			{
				if (num2 == 0)
				{
					this.m_parkedGrid[num] = data.m_nextGridParked;
				}
				else
				{
					this.m_parkedVehicles.m_buffer[(int)num2].m_nextGridParked = data.m_nextGridParked;
				}
				break;
			}
			num2 = num3;
			num3 = this.m_parkedVehicles.m_buffer[(int)num3].m_nextGridParked;
			if (++num4 > 32768)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_nextGridParked = 0;
	}

	public bool RayCast(Segment3 ray, Vehicle.Flags ignoreFlags, VehicleParked.Flags ignoreFlags2, out Vector3 hit, out ushort vehicleIndex, out ushort parkedIndex)
	{
		hit = ray.b;
		vehicleIndex = 0;
		parkedIndex = 0;
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		Segment3 ray2 = ray;
		if (ray2.Clip(bounds))
		{
			Vector3 vector = ray2.b - ray2.a;
			Vector3 normalized = vector.get_normalized();
			Vector3 vector2 = ray2.a - normalized * 72f;
			Vector3 vector3 = ray2.a + Vector3.ClampMagnitude(ray2.b - ray2.a, 2000f) + normalized * 72f;
			int num = (int)(vector2.x / 32f + 270f);
			int num2 = (int)(vector2.z / 32f + 270f);
			int num3 = (int)(vector3.x / 32f + 270f);
			int num4 = (int)(vector3.z / 32f + 270f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 32f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 32f / num6;
				}
			}
			Vector3 vector4 = vector2;
			Vector3 vector5 = vector2;
			do
			{
				Vector3 vector6 = vector5 + vector;
				int num9;
				int num10;
				int num11;
				int num12;
				if (num7 != 0)
				{
					num9 = Mathf.Max(num, 0);
					num10 = Mathf.Min(num, 539);
					num11 = Mathf.Max((int)((Mathf.Min(vector4.z, vector6.z) - 72f) / 32f + 270f), 0);
					num12 = Mathf.Min((int)((Mathf.Max(vector4.z, vector6.z) + 72f) / 32f + 270f), 539);
				}
				else
				{
					num11 = Mathf.Max(num2, 0);
					num12 = Mathf.Min(num2, 539);
					num9 = Mathf.Max((int)((Mathf.Min(vector4.x, vector6.x) - 72f) / 32f + 270f), 0);
					num10 = Mathf.Min((int)((Mathf.Max(vector4.x, vector6.x) + 72f) / 32f + 270f), 539);
				}
				for (int i = num11; i <= num12; i++)
				{
					for (int j = num9; j <= num10; j++)
					{
						ushort num13 = this.m_vehicleGrid[i * 540 + j];
						int num14 = 0;
						while (num13 != 0)
						{
							float num15;
							if (this.m_vehicles.m_buffer[(int)num13].RayCast(num13, ray2, ignoreFlags, out num15))
							{
								Vector3 vector7 = ray2.Position(num15);
								if (Vector3.SqrMagnitude(vector7 - ray.a) < Vector3.SqrMagnitude(hit - ray.a))
								{
									hit = vector7;
									vehicleIndex = num13;
									parkedIndex = 0;
								}
							}
							num13 = this.m_vehicles.m_buffer[(int)num13].m_nextGridVehicle;
							if (++num14 > 16384)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
						ushort num16 = this.m_parkedGrid[i * 540 + j];
						int num17 = 0;
						while (num16 != 0)
						{
							float num18;
							if (this.m_parkedVehicles.m_buffer[(int)num16].RayCast(num16, ray2, ignoreFlags2, out num18))
							{
								Vector3 vector8 = ray2.Position(num18);
								if (Vector3.SqrMagnitude(vector8 - ray.a) < Vector3.SqrMagnitude(hit - ray.a))
								{
									hit = vector8;
									vehicleIndex = 0;
									parkedIndex = num16;
								}
							}
							num16 = this.m_parkedVehicles.m_buffer[(int)num16].m_nextGridParked;
							if (++num17 > 32768)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector4 = vector5;
				vector5 = vector6;
				num += num7;
				num2 += num8;
			}
			while ((num <= num3 || num7 <= 0) && (num >= num3 || num7 >= 0) && (num2 <= num4 || num8 <= 0) && (num2 >= num4 || num8 >= 0));
		}
		bounds..ctor(new Vector3(0f, 1512f, 0f), new Vector3(17280f, 3024f, 17280f));
		ray2 = ray;
		if (ray2.Clip(bounds))
		{
			Vector3 vector9 = ray2.b - ray2.a;
			Vector3 normalized2 = vector9.get_normalized();
			Vector3 vector10 = ray2.a - normalized2 * 112f;
			Vector3 vector11 = ray2.b + normalized2 * 112f;
			int num19 = (int)(vector10.x / 320f + 27f);
			int num20 = (int)(vector10.z / 320f + 27f);
			int num21 = (int)(vector11.x / 320f + 27f);
			int num22 = (int)(vector11.z / 320f + 27f);
			float num23 = Mathf.Abs(vector9.x);
			float num24 = Mathf.Abs(vector9.z);
			int num25;
			int num26;
			if (num23 >= num24)
			{
				num25 = ((vector9.x <= 0f) ? -1 : 1);
				num26 = 0;
				if (num23 != 0f)
				{
					vector9 *= 320f / num23;
				}
			}
			else
			{
				num25 = 0;
				num26 = ((vector9.z <= 0f) ? -1 : 1);
				if (num24 != 0f)
				{
					vector9 *= 320f / num24;
				}
			}
			Vector3 vector12 = vector10;
			Vector3 vector13 = vector10;
			do
			{
				Vector3 vector14 = vector13 + vector9;
				int num27;
				int num28;
				int num29;
				int num30;
				if (num25 != 0)
				{
					num27 = Mathf.Max(num19, 0);
					num28 = Mathf.Min(num19, 53);
					num29 = Mathf.Max((int)((Mathf.Min(vector12.z, vector14.z) - 112f) / 320f + 27f), 0);
					num30 = Mathf.Min((int)((Mathf.Max(vector12.z, vector14.z) + 112f) / 320f + 27f), 53);
				}
				else
				{
					num29 = Mathf.Max(num20, 0);
					num30 = Mathf.Min(num20, 53);
					num27 = Mathf.Max((int)((Mathf.Min(vector12.x, vector14.x) - 112f) / 320f + 27f), 0);
					num28 = Mathf.Min((int)((Mathf.Max(vector12.x, vector14.x) + 112f) / 320f + 27f), 53);
				}
				for (int k = num29; k <= num30; k++)
				{
					for (int l = num27; l <= num28; l++)
					{
						ushort num31 = this.m_vehicleGrid2[k * 54 + l];
						int num32 = 0;
						while (num31 != 0)
						{
							float num33;
							if (this.m_vehicles.m_buffer[(int)num31].RayCast(num31, ray2, ignoreFlags, out num33))
							{
								Vector3 vector15 = ray2.Position(num33);
								if (Vector3.SqrMagnitude(vector15 - ray.a) < Vector3.SqrMagnitude(hit - ray.a))
								{
									hit = vector15;
									vehicleIndex = num31;
									parkedIndex = 0;
								}
							}
							num31 = this.m_vehicles.m_buffer[(int)num31].m_nextGridVehicle;
							if (++num32 > 16384)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector12 = vector13;
				vector13 = vector14;
				num19 += num25;
				num20 += num26;
			}
			while ((num19 <= num21 || num25 <= 0) && (num19 >= num21 || num25 >= 0) && (num20 <= num22 || num26 <= 0) && (num20 >= num22 || num26 >= 0));
		}
		if (vehicleIndex != 0 || parkedIndex != 0)
		{
			return true;
		}
		hit = Vector3.get_zero();
		vehicleIndex = 0;
		parkedIndex = 0;
		return false;
	}

	public void UpdateParkedVehicles(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)((minX - 10f) / 32f + 270f), 0);
		int num2 = Mathf.Max((int)((minZ - 10f) / 32f + 270f), 0);
		int num3 = Mathf.Min((int)((maxX + 10f) / 32f + 270f), 539);
		int num4 = Mathf.Min((int)((maxZ + 10f) / 32f + 270f), 539);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_parkedGrid[i * 540 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					if ((this.m_parkedVehicles.m_buffer[(int)num5].m_flags & 4) == 0)
					{
						VehicleParked[] expr_D1_cp_0 = this.m_parkedVehicles.m_buffer;
						ushort expr_D1_cp_1 = num5;
						expr_D1_cp_0[(int)expr_D1_cp_1].m_flags = (expr_D1_cp_0[(int)expr_D1_cp_1].m_flags | 4);
						this.m_updatedParked[num5 >> 6] |= 1uL << (int)num5;
						this.m_parkedUpdated = true;
					}
					num5 = this.m_parkedVehicles.m_buffer[(int)num5].m_nextGridParked;
					if (++num6 > 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_parkedUpdated)
		{
			int num = this.m_updatedParked.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedParked[i];
				if (num2 != 0uL)
				{
					this.m_updatedParked[i] = 0uL;
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							ushort num3 = (ushort)(i << 6 | j);
							VehicleInfo info = this.m_parkedVehicles.m_buffer[(int)num3].Info;
							VehicleParked[] expr_80_cp_0 = this.m_parkedVehicles.m_buffer;
							ushort expr_80_cp_1 = num3;
							expr_80_cp_0[(int)expr_80_cp_1].m_flags = (expr_80_cp_0[(int)expr_80_cp_1].m_flags & 65531);
							info.m_vehicleAI.UpdateParkedVehicle(num3, ref this.m_parkedVehicles.m_buffer[(int)num3]);
						}
					}
				}
			}
			this.m_parkedUpdated = false;
		}
		if (subStep != 0)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			Vector3 physicsLodRefPos = instance.m_simulationView.m_position + instance.m_simulationView.m_direction * 1000f;
			int num4 = (int)(instance.m_currentFrameIndex & 15u);
			int num5 = num4 * 1024;
			int num6 = (num4 + 1) * 1024 - 1;
			for (int k = num5; k <= num6; k++)
			{
				Vehicle.Flags flags = this.m_vehicles.m_buffer[k].m_flags;
				if ((flags & Vehicle.Flags.Created) != (Vehicle.Flags)0 && this.m_vehicles.m_buffer[k].m_leadingVehicle == 0)
				{
					VehicleInfo info2 = this.m_vehicles.m_buffer[k].Info;
					info2.m_vehicleAI.SimulationStep((ushort)k, ref this.m_vehicles.m_buffer[k], physicsLodRefPos);
				}
			}
			if ((instance.m_currentFrameIndex & 255u) == 0u)
			{
				uint num7 = this.m_maxTrafficFlow / 100u;
				if (num7 == 0u)
				{
					num7 = 1u;
				}
				uint num8 = this.m_totalTrafficFlow / num7;
				if (num8 > 100u)
				{
					num8 = 100u;
				}
				this.m_lastTrafficFlow = num8;
				this.m_totalTrafficFlow = 0u;
				this.m_maxTrafficFlow = 0u;
				StatisticsManager instance2 = Singleton<StatisticsManager>.get_instance();
				StatisticBase statisticBase = instance2.Acquire<StatisticInt32>(StatisticType.TrafficFlow);
				statisticBase.Set((int)num8);
			}
		}
	}

	public VehicleInfo GetRandomVehicleInfo(ref Randomizer r, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		if (!this.m_vehiclesRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random vehicles not refreshed yet!\n" + Environment.StackTrace);
			return null;
		}
		int num = VehicleManager.GetTransferIndex(service, subService, level);
		FastList<ushort> fastList = this.m_transferVehicles[num];
		if (fastList == null)
		{
			return null;
		}
		if (fastList.m_size == 0)
		{
			return null;
		}
		num = r.Int32((uint)fastList.m_size);
		return PrefabCollection<VehicleInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	public VehicleInfo GetRandomVehicleInfo(ref Randomizer r, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, VehicleInfo.VehicleType type)
	{
		if (!this.m_vehiclesRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random vehicles not refreshed yet!\n" + Environment.StackTrace);
			return null;
		}
		int num = VehicleManager.GetTransferIndex(service, subService, level);
		FastList<ushort> fastList = this.m_transferVehicles[num];
		if (fastList == null)
		{
			return null;
		}
		int num2 = this.FindFirst(fastList.m_buffer, fastList.m_size, type);
		int num3 = this.FindFirst(fastList.m_buffer, fastList.m_size, type + 1);
		if (num3 - num2 <= 0)
		{
			return null;
		}
		num = r.Int32(num2, num3 - 1);
		return PrefabCollection<VehicleInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	private int FindFirst(ushort[] vehicles, int size, VehicleInfo.VehicleType type)
	{
		int num = 0;
		int i = size;
		while (i > num)
		{
			int num2 = i + num >> 1;
			VehicleInfo.VehicleType vehicleType = PrefabCollection<VehicleInfo>.GetPrefab((uint)vehicles[num2]).m_vehicleType;
			if (vehicleType < type)
			{
				num = num2 + 1;
			}
			else
			{
				i = num2;
			}
		}
		return i;
	}

	public void RefreshTransferVehicles()
	{
		if (this.m_vehiclesRefreshed)
		{
			return;
		}
		int num = this.m_transferVehicles.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_transferVehicles[i] = null;
		}
		int num2 = PrefabCollection<VehicleInfo>.PrefabCount();
		for (int j = 0; j < num2; j++)
		{
			VehicleInfo prefab = PrefabCollection<VehicleInfo>.GetPrefab((uint)j);
			if (prefab != null && prefab.m_class.m_service != ItemClass.Service.None && prefab.m_placementStyle == ItemClass.Placement.Automatic)
			{
				int transferIndex = VehicleManager.GetTransferIndex(prefab.m_class.m_service, prefab.m_class.m_subService, prefab.m_class.m_level);
				if (this.m_transferVehicles[transferIndex] == null)
				{
					this.m_transferVehicles[transferIndex] = new FastList<ushort>();
				}
				this.m_transferVehicles[transferIndex].Add((ushort)j);
			}
		}
		for (int k = 0; k < num; k++)
		{
			if (this.m_transferVehicles[k] != null)
			{
				Array.Sort<ushort>(this.m_transferVehicles[k].m_buffer, 0, this.m_transferVehicles[k].m_size, VehicleManager.VehicleTypeComparer.comparer);
			}
		}
		int num3 = 46;
		for (int l = 0; l < num3; l++)
		{
			for (int m = 1; m < 5; m++)
			{
				int num4 = l;
				num4 = num4 * 5 + m;
				FastList<ushort> fastList = this.m_transferVehicles[num4];
				FastList<ushort> fastList2 = this.m_transferVehicles[num4 - 1];
				if (fastList == null && fastList2 != null)
				{
					this.m_transferVehicles[num4] = fastList2;
				}
			}
		}
		this.m_vehiclesRefreshed = true;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetVehicleName(ushort vehicleID, string name)
	{
		VehicleManager.<SetVehicleName>c__Iterator2 <SetVehicleName>c__Iterator = new VehicleManager.<SetVehicleName>c__Iterator2();
		<SetVehicleName>c__Iterator.vehicleID = vehicleID;
		<SetVehicleName>c__Iterator.name = name;
		<SetVehicleName>c__Iterator.$this = this;
		return <SetVehicleName>c__Iterator;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetParkedVehicleName(ushort parkedID, string name)
	{
		VehicleManager.<SetParkedVehicleName>c__Iterator3 <SetParkedVehicleName>c__Iterator = new VehicleManager.<SetParkedVehicleName>c__Iterator3();
		<SetParkedVehicleName>c__Iterator.parkedID = parkedID;
		<SetParkedVehicleName>c__Iterator.name = name;
		<SetParkedVehicleName>c__Iterator.$this = this;
		return <SetParkedVehicleName>c__Iterator;
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("VehicleManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 16384; i++)
		{
			if (this.m_vehicles.m_buffer[i].m_flags != (Vehicle.Flags)0 && this.m_vehicles.m_buffer[i].Info == null)
			{
				this.ReleaseVehicle((ushort)i);
			}
		}
		for (int j = 1; j < 32768; j++)
		{
			if (this.m_parkedVehicles.m_buffer[j].m_flags != 0 && this.m_parkedVehicles.m_buffer[j].Info == null)
			{
				this.ReleaseParkedVehicle((ushort)j);
			}
		}
		this.m_infoCount = PrefabCollection<VehicleInfo>.PrefabCount();
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new VehicleManager.Data());
	}

	public string GetVehicleName(ushort vehicleID)
	{
		if (this.m_vehicles.m_buffer[(int)vehicleID].m_flags != (Vehicle.Flags)0)
		{
			string text = null;
			if ((this.m_vehicles.m_buffer[(int)vehicleID].m_flags & Vehicle.Flags.CustomName) != (Vehicle.Flags)0)
			{
				InstanceID id = default(InstanceID);
				id.Vehicle = vehicleID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateVehicleName(vehicleID);
			}
			return text;
		}
		return null;
	}

	public string GetParkedVehicleName(ushort parkedID)
	{
		if (this.m_parkedVehicles.m_buffer[(int)parkedID].m_flags != 0)
		{
			string text = null;
			if ((this.m_parkedVehicles.m_buffer[(int)parkedID].m_flags & 16) != 0)
			{
				InstanceID id = default(InstanceID);
				id.ParkedVehicle = parkedID;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateParkedVehicleName(parkedID);
			}
			return text;
		}
		return null;
	}

	public string GetDefaultVehicleName(ushort vehicleID)
	{
		return this.GenerateVehicleName(vehicleID);
	}

	public string GetDefaultParkedVehicleName(ushort parkedID)
	{
		return this.GenerateParkedVehicleName(parkedID);
	}

	private string GenerateVehicleName(ushort vehicleID)
	{
		VehicleInfo info = this.m_vehicles.m_buffer[(int)vehicleID].Info;
		if (info != null)
		{
			string text = PrefabCollection<VehicleInfo>.PrefabName((uint)info.m_prefabDataIndex);
			return Locale.Get("VEHICLE_TITLE", text);
		}
		return "Invalid";
	}

	private string GenerateParkedVehicleName(ushort parkedID)
	{
		VehicleInfo info = this.m_parkedVehicles.m_buffer[(int)parkedID].Info;
		if (info != null)
		{
			string text = PrefabCollection<VehicleInfo>.PrefabName((uint)info.m_prefabDataIndex);
			return Locale.Get("VEHICLE_TITLE", text);
		}
		return "Invalid";
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const float VEHICLEGRID_CELL_SIZE = 32f;

	public const int VEHICLEGRID_RESOLUTION = 540;

	public const float VEHICLEGRID_CELL_SIZE2 = 320f;

	public const int VEHICLEGRID_RESOLUTION2 = 54;

	public const int MAX_VEHICLE_COUNT = 16384;

	public const int MAX_PARKED_COUNT = 32768;

	public int m_vehicleCount;

	public int m_parkedCount;

	public int m_infoCount;

	[NonSerialized]
	public Array16<Vehicle> m_vehicles;

	[NonSerialized]
	public Array16<VehicleParked> m_parkedVehicles;

	[NonSerialized]
	public ulong[] m_updatedParked;

	[NonSerialized]
	public bool m_parkedUpdated;

	[NonSerialized]
	public ushort[] m_vehicleGrid;

	[NonSerialized]
	public ushort[] m_vehicleGrid2;

	[NonSerialized]
	public ushort[] m_parkedGrid;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_TyreMatrix;

	[NonSerialized]
	public int ID_TyrePosition;

	[NonSerialized]
	public int ID_LightState;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_MainTex;

	[NonSerialized]
	public int ID_XYSMap;

	[NonSerialized]
	public int ID_ACIMap;

	[NonSerialized]
	public int ID_AtlasRect;

	[NonSerialized]
	public int ID_VehicleTransform;

	[NonSerialized]
	public int ID_VehicleLightState;

	[NonSerialized]
	public int ID_VehicleColor;

	[NonSerialized]
	public int ID_TyreLocation;

	[NonSerialized]
	public AudioGroup m_audioGroup;

	[NonSerialized]
	public int m_undergroundLayer;

	[NonSerialized]
	public uint m_totalTrafficFlow;

	[NonSerialized]
	public uint m_maxTrafficFlow;

	[NonSerialized]
	public uint m_lastTrafficFlow;

	public Texture2D m_lodRgbAtlas;

	public Texture2D m_lodXysAtlas;

	public Texture2D m_lodAciAtlas;

	private FastList<ushort>[] m_transferVehicles;

	private bool m_vehiclesRefreshed;

	private ulong[] m_renderBuffer;

	private ulong[] m_renderBuffer2;

	public class VehicleTypeComparer : IComparer<ushort>
	{
		int IComparer<ushort>.Compare(ushort prefabIndex1, ushort prefabIndex2)
		{
			VehicleInfo prefab = PrefabCollection<VehicleInfo>.GetPrefab((uint)prefabIndex1);
			VehicleInfo prefab2 = PrefabCollection<VehicleInfo>.GetPrefab((uint)prefabIndex2);
			return prefab.m_vehicleType - prefab2.m_vehicleType;
		}

		public static VehicleManager.VehicleTypeComparer comparer = new VehicleManager.VehicleTypeComparer();
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "VehicleManager");
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Vehicle[] buffer = instance.m_vehicles.m_buffer;
			VehicleParked[] buffer2 = instance.m_parkedVehicles.m_buffer;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			for (int j = 1; j < num; j++)
			{
				uInt.Write((uint)buffer[j].m_flags2);
			}
			uInt.EndWrite();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int k = 1; k < num2; k++)
			{
				uShort.Write(buffer2[k].m_flags);
			}
			uShort.EndWrite();
			try
			{
				PrefabCollection<VehicleInfo>.BeginSerialize(s);
				for (int l = 1; l < num; l++)
				{
					if (buffer[l].m_flags != (Vehicle.Flags)0)
					{
						PrefabCollection<VehicleInfo>.Serialize((uint)buffer[l].m_infoIndex);
					}
				}
				for (int m = 1; m < num2; m++)
				{
					if (buffer2[m].m_flags != 0)
					{
						PrefabCollection<VehicleInfo>.Serialize((uint)buffer2[m].m_infoIndex);
					}
				}
			}
			finally
			{
				PrefabCollection<VehicleInfo>.EndSerialize(s);
			}
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int n = 1; n < num; n++)
			{
				if (buffer[n].m_flags != (Vehicle.Flags)0)
				{
					@byte.Write(buffer[n].m_gateIndex);
				}
			}
			@byte.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int num3 = 1; num3 < num; num3++)
			{
				if (buffer[num3].m_flags != (Vehicle.Flags)0)
				{
					uShort2.Write(buffer[num3].m_waterSource);
				}
			}
			uShort2.EndWrite();
			for (int num4 = 1; num4 < num; num4++)
			{
				if (buffer[num4].m_flags != (Vehicle.Flags)0)
				{
					Vehicle.Frame lastFrameData = buffer[num4].GetLastFrameData();
					s.WriteVector3(lastFrameData.m_velocity);
					s.WriteVector3(lastFrameData.m_position);
					s.WriteQuaternion(lastFrameData.m_rotation);
					s.WriteFloat(lastFrameData.m_angleVelocity);
					s.WriteVector4(buffer[num4].m_targetPos0);
					s.WriteVector4(buffer[num4].m_targetPos1);
					s.WriteVector4(buffer[num4].m_targetPos2);
					s.WriteVector4(buffer[num4].m_targetPos3);
					s.WriteUInt16((uint)buffer[num4].m_sourceBuilding);
					s.WriteUInt16((uint)buffer[num4].m_targetBuilding);
					s.WriteUInt16((uint)buffer[num4].m_transportLine);
					s.WriteUInt16((uint)buffer[num4].m_transferSize);
					s.WriteUInt8((uint)buffer[num4].m_transferType);
					s.WriteUInt8((uint)buffer[num4].m_waitCounter);
					s.WriteUInt8((uint)buffer[num4].m_blockCounter);
					s.WriteUInt24(buffer[num4].m_citizenUnits);
					s.WriteUInt24(buffer[num4].m_path);
					s.WriteUInt8((uint)buffer[num4].m_pathPositionIndex);
					s.WriteUInt8((uint)buffer[num4].m_lastPathOffset);
					s.WriteUInt16((uint)buffer[num4].m_trailingVehicle);
					s.WriteUInt16((uint)buffer[num4].m_cargoParent);
				}
			}
			for (int num5 = 1; num5 < num2; num5++)
			{
				if (buffer2[num5].m_flags != 0)
				{
					s.WriteVector3(buffer2[num5].m_position);
					s.WriteQuaternion(buffer2[num5].m_rotation);
					s.WriteUInt24(buffer2[num5].m_ownerCitizen);
				}
			}
			s.WriteUInt32(instance.m_totalTrafficFlow);
			s.WriteUInt32(instance.m_maxTrafficFlow);
			s.WriteUInt8(instance.m_lastTrafficFlow);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "VehicleManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "VehicleManager");
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			Vehicle[] buffer = instance.m_vehicles.m_buffer;
			VehicleParked[] buffer2 = instance.m_parkedVehicles.m_buffer;
			ushort[] vehicleGrid = instance.m_vehicleGrid;
			ushort[] vehicleGrid2 = instance.m_vehicleGrid2;
			ushort[] parkedGrid = instance.m_parkedGrid;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			int num3 = vehicleGrid.Length;
			int num4 = vehicleGrid2.Length;
			int num5 = parkedGrid.Length;
			instance.m_vehicles.ClearUnused();
			instance.m_parkedVehicles.ClearUnused();
			for (int i = 0; i < num3; i++)
			{
				vehicleGrid[i] = 0;
			}
			for (int j = 0; j < num4; j++)
			{
				vehicleGrid2[j] = 0;
			}
			for (int k = 0; k < num5; k++)
			{
				parkedGrid[k] = 0;
			}
			for (int l = 0; l < instance.m_updatedParked.Length; l++)
			{
				instance.m_updatedParked[l] = 0uL;
			}
			instance.m_parkedUpdated = false;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
			for (int m = 1; m < num; m++)
			{
				buffer[m].m_flags = (Vehicle.Flags)uInt.Read();
			}
			if (s.get_version() >= 274u)
			{
				for (int n = 1; n < num; n++)
				{
					buffer[n].m_flags2 = (Vehicle.Flags2)uInt.Read();
				}
			}
			else
			{
				for (int num6 = 1; num6 < num; num6++)
				{
					buffer[num6].m_flags2 = (Vehicle.Flags2)0;
				}
			}
			uInt.EndRead();
			if (s.get_version() >= 205u)
			{
				EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
				for (int num7 = 1; num7 < num2; num7++)
				{
					buffer2[num7].m_flags = uShort.Read();
				}
				uShort.EndRead();
			}
			else if (s.get_version() >= 115u)
			{
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
				for (int num8 = 1; num8 < 16384; num8++)
				{
					buffer2[num8].m_flags = uShort2.Read();
				}
				for (int num9 = 16384; num9 < num2; num9++)
				{
					buffer2[num9].m_flags = 0;
				}
				uShort2.EndRead();
			}
			else
			{
				for (int num10 = 1; num10 < num2; num10++)
				{
					buffer2[num10].m_flags = 0;
				}
			}
			if (s.get_version() >= 30u)
			{
				PrefabCollection<VehicleInfo>.BeginDeserialize(s);
				for (int num11 = 1; num11 < num; num11++)
				{
					if (buffer[num11].m_flags != (Vehicle.Flags)0)
					{
						buffer[num11].m_infoIndex = (ushort)PrefabCollection<VehicleInfo>.Deserialize(true);
					}
				}
				if (s.get_version() >= 115u)
				{
					for (int num12 = 1; num12 < num2; num12++)
					{
						if (buffer2[num12].m_flags != 0)
						{
							buffer2[num12].m_infoIndex = (ushort)PrefabCollection<VehicleInfo>.Deserialize(true);
						}
					}
				}
				PrefabCollection<VehicleInfo>.EndDeserialize(s);
			}
			if (s.get_version() >= 182u)
			{
				EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
				for (int num13 = 1; num13 < num; num13++)
				{
					if (buffer[num13].m_flags != (Vehicle.Flags)0)
					{
						buffer[num13].m_gateIndex = @byte.Read();
					}
					else
					{
						buffer[num13].m_gateIndex = 0;
					}
				}
				@byte.EndRead();
			}
			else
			{
				for (int num14 = 1; num14 < num; num14++)
				{
					buffer[num14].m_gateIndex = 0;
				}
			}
			if (s.get_version() >= 273u)
			{
				EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginRead(s);
				for (int num15 = 1; num15 < num; num15++)
				{
					if (buffer[num15].m_flags != (Vehicle.Flags)0)
					{
						buffer[num15].m_waterSource = uShort3.Read();
					}
					else
					{
						buffer[num15].m_waterSource = 0;
					}
				}
				uShort3.EndRead();
			}
			else
			{
				for (int num16 = 1; num16 < num; num16++)
				{
					buffer[num16].m_waterSource = 0;
				}
			}
			for (int num17 = 1; num17 < num; num17++)
			{
				buffer[num17].m_nextGridVehicle = 0;
				buffer[num17].m_nextGuestVehicle = 0;
				buffer[num17].m_nextOwnVehicle = 0;
				buffer[num17].m_nextLineVehicle = 0;
				buffer[num17].m_leadingVehicle = 0;
				buffer[num17].m_firstCargo = 0;
				buffer[num17].m_nextCargo = 0;
				buffer[num17].m_lastFrame = 0;
				if (buffer[num17].m_flags != (Vehicle.Flags)0)
				{
					buffer[num17].m_frame0 = new Vehicle.Frame(Vector3.get_zero(), Quaternion.get_identity());
					if (s.get_version() >= 47u)
					{
						buffer[num17].m_frame0.m_velocity = s.ReadVector3();
					}
					buffer[num17].m_frame0.m_position = s.ReadVector3();
					if (s.get_version() >= 78u)
					{
						buffer[num17].m_frame0.m_rotation = s.ReadQuaternion();
					}
					if (s.get_version() >= 129u)
					{
						buffer[num17].m_frame0.m_angleVelocity = s.ReadFloat();
					}
					buffer[num17].m_frame0.m_underground = ((buffer[num17].m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
					buffer[num17].m_frame0.m_transition = ((buffer[num17].m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
					buffer[num17].m_frame0.m_insideBuilding = ((buffer[num17].m_flags & Vehicle.Flags.InsideBuilding) != (Vehicle.Flags)0);
					buffer[num17].m_frame1 = buffer[num17].m_frame0;
					buffer[num17].m_frame2 = buffer[num17].m_frame0;
					buffer[num17].m_frame3 = buffer[num17].m_frame0;
					if (s.get_version() >= 68u)
					{
						buffer[num17].m_targetPos0 = s.ReadVector4();
					}
					else if (s.get_version() >= 47u)
					{
						buffer[num17].m_targetPos0 = s.ReadVector3();
						buffer[num17].m_targetPos0.w = 2f;
					}
					else
					{
						buffer[num17].m_targetPos0 = buffer[num17].m_frame0.m_position;
						buffer[num17].m_targetPos0.w = 2f;
					}
					if (s.get_version() >= 90u)
					{
						buffer[num17].m_targetPos1 = s.ReadVector4();
						buffer[num17].m_targetPos2 = s.ReadVector4();
						buffer[num17].m_targetPos3 = s.ReadVector4();
					}
					else
					{
						buffer[num17].m_targetPos1 = buffer[num17].m_targetPos0;
						buffer[num17].m_targetPos2 = buffer[num17].m_targetPos0;
						buffer[num17].m_targetPos3 = buffer[num17].m_targetPos0;
					}
					buffer[num17].m_sourceBuilding = (ushort)s.ReadUInt16();
					buffer[num17].m_targetBuilding = (ushort)s.ReadUInt16();
					if (s.get_version() >= 52u)
					{
						buffer[num17].m_transportLine = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[num17].m_transportLine = 0;
					}
					buffer[num17].m_transferSize = (ushort)s.ReadUInt16();
					buffer[num17].m_transferType = (byte)s.ReadUInt8();
					buffer[num17].m_waitCounter = (byte)s.ReadUInt8();
					if (s.get_version() >= 99u)
					{
						buffer[num17].m_blockCounter = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[num17].m_blockCounter = 0;
					}
					if (s.get_version() >= 32u)
					{
						buffer[num17].m_citizenUnits = s.ReadUInt24();
					}
					else
					{
						buffer[num17].m_citizenUnits = 0u;
					}
					if (s.get_version() >= 47u)
					{
						buffer[num17].m_path = s.ReadUInt24();
						buffer[num17].m_pathPositionIndex = (byte)s.ReadUInt8();
						buffer[num17].m_lastPathOffset = (byte)s.ReadUInt8();
					}
					else
					{
						buffer[num17].m_path = 0u;
						buffer[num17].m_pathPositionIndex = 0;
						buffer[num17].m_lastPathOffset = 0;
					}
					if (s.get_version() >= 58u)
					{
						buffer[num17].m_trailingVehicle = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[num17].m_trailingVehicle = 0;
					}
					if (s.get_version() >= 104u)
					{
						buffer[num17].m_cargoParent = (ushort)s.ReadUInt16();
					}
					else
					{
						buffer[num17].m_cargoParent = 0;
					}
				}
				else
				{
					buffer[num17].m_frame0 = new Vehicle.Frame(Vector3.get_zero(), Quaternion.get_identity());
					buffer[num17].m_frame1 = new Vehicle.Frame(Vector3.get_zero(), Quaternion.get_identity());
					buffer[num17].m_frame2 = new Vehicle.Frame(Vector3.get_zero(), Quaternion.get_identity());
					buffer[num17].m_frame3 = new Vehicle.Frame(Vector3.get_zero(), Quaternion.get_identity());
					buffer[num17].m_targetPos0 = Vector4.get_zero();
					buffer[num17].m_targetPos1 = Vector4.get_zero();
					buffer[num17].m_targetPos2 = Vector4.get_zero();
					buffer[num17].m_targetPos3 = Vector4.get_zero();
					buffer[num17].m_sourceBuilding = 0;
					buffer[num17].m_targetBuilding = 0;
					buffer[num17].m_transportLine = 0;
					buffer[num17].m_transferSize = 0;
					buffer[num17].m_transferType = 0;
					buffer[num17].m_waitCounter = 0;
					buffer[num17].m_blockCounter = 0;
					buffer[num17].m_citizenUnits = 0u;
					buffer[num17].m_path = 0u;
					buffer[num17].m_pathPositionIndex = 0;
					buffer[num17].m_lastPathOffset = 0;
					buffer[num17].m_trailingVehicle = 0;
					buffer[num17].m_cargoParent = 0;
					instance.m_vehicles.ReleaseItem((ushort)num17);
				}
			}
			if (s.get_version() >= 115u)
			{
				for (int num18 = 1; num18 < num2; num18++)
				{
					buffer2[num18].m_nextGridParked = 0;
					buffer2[num18].m_travelDistance = 0f;
					if (buffer2[num18].m_flags != 0)
					{
						buffer2[num18].m_position = s.ReadVector3();
						buffer2[num18].m_rotation = s.ReadQuaternion();
						buffer2[num18].m_ownerCitizen = s.ReadUInt24();
						if ((buffer2[num18].m_flags & 4) != 0)
						{
							instance.m_updatedParked[num18 >> 6] |= 1uL << num18;
							instance.m_parkedUpdated = true;
						}
					}
					else
					{
						buffer2[num18].m_position = Vector3.get_zero();
						buffer2[num18].m_rotation = Quaternion.get_identity();
						buffer2[num18].m_ownerCitizen = 0u;
						instance.m_parkedVehicles.ReleaseItem((ushort)num18);
					}
				}
			}
			else
			{
				for (int num19 = 1; num19 < num2; num19++)
				{
					buffer2[num19].m_nextGridParked = 0;
					buffer2[num19].m_travelDistance = 0f;
					buffer2[num19].m_position = Vector3.get_zero();
					buffer2[num19].m_rotation = Quaternion.get_identity();
					buffer2[num19].m_ownerCitizen = 0u;
					instance.m_parkedVehicles.ReleaseItem((ushort)num19);
				}
			}
			if (s.get_version() >= 315u)
			{
				instance.m_totalTrafficFlow = s.ReadUInt32();
				instance.m_maxTrafficFlow = s.ReadUInt32();
				instance.m_lastTrafficFlow = s.ReadUInt8();
			}
			else
			{
				instance.m_totalTrafficFlow = 0u;
				instance.m_maxTrafficFlow = 0u;
				instance.m_lastTrafficFlow = 0u;
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "VehicleManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "VehicleManager");
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			VehicleManager instance2 = Singleton<VehicleManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			instance2.m_vehiclesRefreshed = false;
			PrefabCollection<VehicleInfo>.BindPrefabs();
			instance2.RefreshTransferVehicles();
			Vehicle[] buffer = instance2.m_vehicles.m_buffer;
			VehicleParked[] buffer2 = instance2.m_parkedVehicles.m_buffer;
			int num = buffer.Length;
			int num2 = buffer2.Length;
			VehicleInfo vehicleInfo = null;
			VehicleInfo vehicleInfo2 = null;
			if (s.get_version() < 303u)
			{
				int num3 = PrefabCollection<VehicleInfo>.PrefabCount();
				for (int i = 0; i < num3; i++)
				{
					VehicleInfo prefab = PrefabCollection<VehicleInfo>.GetPrefab((uint)i);
					if (prefab != null && prefab.m_prefabDataIndex != -1)
					{
						string a = PrefabCollection<VehicleInfo>.PrefabName((uint)prefab.m_prefabDataIndex);
						if (a == "Evacuation Bus")
						{
							vehicleInfo = prefab;
						}
						else if (a == "Sedan")
						{
							vehicleInfo2 = prefab;
						}
					}
				}
			}
			for (int j = 1; j < num; j++)
			{
				if (buffer[j].m_flags != (Vehicle.Flags)0)
				{
					ushort trailingVehicle = buffer[j].m_trailingVehicle;
					if (trailingVehicle != 0)
					{
						if (buffer[(int)trailingVehicle].m_flags != (Vehicle.Flags)0)
						{
							buffer[(int)trailingVehicle].m_leadingVehicle = (ushort)j;
						}
						else
						{
							buffer[j].m_trailingVehicle = 0;
						}
					}
					ushort cargoParent = buffer[j].m_cargoParent;
					if (cargoParent != 0)
					{
						if (buffer[(int)cargoParent].m_flags != (Vehicle.Flags)0)
						{
							buffer[j].m_nextCargo = buffer[(int)cargoParent].m_firstCargo;
							buffer[(int)cargoParent].m_firstCargo = (ushort)j;
						}
						else
						{
							buffer[j].m_cargoParent = 0;
						}
					}
				}
			}
			for (int k = 1; k < num; k++)
			{
				if (buffer[k].m_flags != (Vehicle.Flags)0)
				{
					if (buffer[k].m_path != 0u)
					{
						PathUnit[] expr_214_cp_0 = Singleton<PathManager>.get_instance().m_pathUnits.m_buffer;
						UIntPtr expr_214_cp_1 = (UIntPtr)buffer[k].m_path;
						expr_214_cp_0[(int)expr_214_cp_1].m_referenceCount = expr_214_cp_0[(int)expr_214_cp_1].m_referenceCount + 1;
					}
					VehicleInfo vehicleInfo3 = buffer[k].Info;
					if (vehicleInfo3 != null)
					{
						if (vehicleInfo3 == vehicleInfo && vehicleInfo2 != null)
						{
							vehicleInfo3 = vehicleInfo2;
						}
						buffer[k].m_infoIndex = (ushort)vehicleInfo3.m_prefabDataIndex;
						vehicleInfo3.m_vehicleAI.LoadVehicle((ushort)k, ref buffer[k]);
						if ((buffer[k].m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0)
						{
							instance2.AddToGrid((ushort)k, ref buffer[k], vehicleInfo3.m_isLargeVehicle);
						}
					}
					uint num4 = buffer[k].m_citizenUnits;
					int num5 = 0;
					while (num4 != 0u)
					{
						instance.m_units.m_buffer[(int)((UIntPtr)num4)].SetVehicleAfterLoading((ushort)k);
						num4 = instance.m_units.m_buffer[(int)((UIntPtr)num4)].m_nextUnit;
						if (++num5 > 524288)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
			for (int l = 1; l < num2; l++)
			{
				if (buffer2[l].m_flags != 0)
				{
					VehicleInfo vehicleInfo4 = buffer2[l].Info;
					if (vehicleInfo4 != null)
					{
						if (vehicleInfo4 == vehicleInfo && vehicleInfo2 != null)
						{
							vehicleInfo4 = vehicleInfo2;
						}
						buffer2[l].m_infoIndex = (ushort)vehicleInfo4.m_prefabDataIndex;
						instance2.AddToGrid((ushort)l, ref buffer2[l]);
					}
					uint ownerCitizen = buffer2[l].m_ownerCitizen;
					if (ownerCitizen != 0u)
					{
						instance.m_citizens.m_buffer[(int)((UIntPtr)ownerCitizen)].m_parkedVehicle = (ushort)l;
					}
				}
			}
			instance2.m_vehicleCount = (int)(instance2.m_vehicles.ItemCount() - 1u);
			instance2.m_parkedCount = (int)(instance2.m_parkedVehicles.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "VehicleManager");
		}
	}
}
