﻿using System;
using ColossalFramework.UI;

public class AssetImporterAssetLOD : AssetImporterPanelBase
{
	public void Reset()
	{
		this.m_ContinueButton.set_isEnabled(false);
	}

	private void Awake()
	{
		this.m_ContinueButton = base.Find<UIButton>("Continue");
		this.m_ContinueButton.add_eventClick(new MouseEventHandler(this.OnContinue));
		this.m_ContinueButton.set_isEnabled(false);
		this.m_BackButton = base.Find<UIButton>("Back");
		this.m_BackButton.add_eventClick(new MouseEventHandler(this.OnBack));
	}

	private void OnContinue(UIComponent comp, UIMouseEventParameter p)
	{
	}

	private void OnBack(UIComponent comp, UIMouseEventParameter p)
	{
		base.owner.GoBack();
	}

	private UIButton m_ContinueButton;

	private UIButton m_BackButton;
}
