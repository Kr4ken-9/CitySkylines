﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class WildlifeSpawnPointAI : BuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		return this.m_info.m_color0;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		this.ReleaseAnimals(buildingID, ref data);
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		int num = randomizer.Int32(1, this.m_maxAnimalCount);
		int num2 = this.CountAnimals(buildingID, ref buildingData);
		if (num2 < num)
		{
			this.CreateAnimal(buildingID, ref buildingData);
		}
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return false;
	}

	public override void CalculateSpawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, out Vector3 position, out Vector3 target)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 30;
			int num2 = data.Length * 30;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			float num3 = (float)randomizer.Int32(360u) * 0.0174532924f;
			target = position;
			target.x += Mathf.Cos(num3);
			target.z += Mathf.Sin(num3);
		}
		else
		{
			base.CalculateSpawnPosition(buildingID, ref data, ref randomizer, info, out position, out target);
		}
	}

	public override void CalculateUnspawnPosition(ushort buildingID, ref Building data, ref Randomizer randomizer, CitizenInfo info, ushort ignoreInstance, out Vector3 position, out Vector3 target, out Vector2 direction, out CitizenInstance.Flags specialFlags)
	{
		if (info.m_citizenAI.IsAnimal())
		{
			int num = data.Width * 600;
			int num2 = data.Length * 600;
			position.x = (float)randomizer.Int32(-num, num) * 0.1f;
			position.y = 0f;
			position.z = (float)randomizer.Int32(-num2, num2) * 0.1f;
			position = data.CalculatePosition(position);
			target = position;
			direction = Vector2.get_zero();
			specialFlags = CitizenInstance.Flags.HangAround;
		}
		else
		{
			base.CalculateUnspawnPosition(buildingID, ref data, ref randomizer, info, ignoreInstance, out position, out target, out direction, out specialFlags);
		}
	}

	private void CreateAnimal(ushort buildingID, ref Building data)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		CitizenInfo groupAnimalInfo = instance2.GetGroupAnimalInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
		ushort num;
		if (groupAnimalInfo != null && instance2.CreateCitizenInstance(out num, ref instance.m_randomizer, groupAnimalInfo, 0u))
		{
			groupAnimalInfo.m_citizenAI.SetSource(num, ref instance2.m_instances.m_buffer[(int)num], buildingID);
			groupAnimalInfo.m_citizenAI.SetTarget(num, ref instance2.m_instances.m_buffer[(int)num], buildingID);
		}
	}

	private void ReleaseAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				instance.ReleaseCitizenInstance(num);
			}
			num = nextTargetInstance;
			if (++num2 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
	}

	private int CountAnimals(ushort buildingID, ref Building data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = data.m_targetCitizens;
		ushort num2 = 0;
		int num3 = 0;
		int num4 = 0;
		while (num != 0)
		{
			ushort nextTargetInstance = instance.m_instances.m_buffer[(int)num].m_nextTargetInstance;
			if (instance.m_instances.m_buffer[(int)num].m_flags == CitizenInstance.Flags.None)
			{
				if (num2 != 0)
				{
					instance.m_instances.m_buffer[(int)num2].m_nextTargetInstance = nextTargetInstance;
				}
				else
				{
					data.m_targetCitizens = nextTargetInstance;
				}
				instance.m_instances.m_buffer[(int)num].m_nextTargetInstance = 0;
				num = num2;
			}
			else if (instance.m_instances.m_buffer[(int)num].Info.m_citizenAI.IsAnimal())
			{
				num3++;
			}
			num2 = num;
			num = nextTargetInstance;
			if (++num4 > 65536)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return num3;
	}

	public override bool IsMarker()
	{
		return true;
	}

	public int m_maxAnimalCount = 2;
}
