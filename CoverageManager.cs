﻿using System;
using System.Threading;
using ColossalFramework;
using UnityEngine;

public class CoverageManager : SimulationManagerBase<CoverageManager, CoverageProperties>, ISimulationManager
{
	protected override void Awake()
	{
		base.Awake();
		this.m_lockObject = new object();
		this.m_nodes = new ushort[32768];
		this.m_distances = new float[32768];
		this.m_service = ItemClass.Service.None;
		this.m_subService = ItemClass.SubService.None;
		this.m_level = ItemClass.Level.None;
		this.m_buildingInfo = null;
		this.m_buildingPosition = new Vector3(-1000000f, -1000000f, -1000000f);
		this.m_buildingAngle = 0f;
		this.m_ignoreBuilding = 0;
	}

	private void Update()
	{
		if (this.m_isReady)
		{
			this.m_isReady = false;
			Singleton<NetManager>.get_instance().UpdateSegmentColors();
			Singleton<NetManager>.get_instance().UpdateNodeColors();
		}
	}

	public bool WaitingForIt()
	{
		return this.m_service != ItemClass.Service.None && this.m_pending;
	}

	public void SetMode(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, float fadeLength, bool invertDirection)
	{
		if (this.m_service != service || this.m_subService != subService || this.m_level != level)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			for (int i = 0; i < 32768; i++)
			{
				instance.m_nodes.m_buffer[i].m_coverage = 255;
			}
			while (!Monitor.TryEnter(this.m_lockObject, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_service = service;
				this.m_subService = subService;
				this.m_level = level;
				this.m_fadeLength = fadeLength;
				this.m_invertDirection = invertDirection;
				this.m_needCoverage = true;
				this.m_pending = true;
			}
			finally
			{
				Monitor.Exit(this.m_lockObject);
			}
		}
	}

	public void SetBuilding(BuildingInfo info, Vector3 position, float angle, ushort ignoreBuilding)
	{
		if (this.m_buildingInfo != info || this.m_buildingPosition != position || this.m_buildingAngle != angle || this.m_ignoreBuilding != ignoreBuilding)
		{
			while (!Monitor.TryEnter(this.m_lockObject, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_buildingInfo = info;
				this.m_buildingPosition = position;
				this.m_buildingAngle = angle;
				this.m_ignoreBuilding = ignoreBuilding;
				this.m_needCoverage = true;
			}
			finally
			{
				Monitor.Exit(this.m_lockObject);
			}
		}
	}

	public byte FindFireCoverage(Vector3 position)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		int num = Mathf.Max((int)((position.x - 64f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - 64f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + 64f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + 64f) / 64f + 135f), 269);
		float num5 = 4096f;
		byte result = 0;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = instance.m_segmentGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)num6].Info;
					if (info.m_class.m_service == ItemClass.Service.Road)
					{
						ushort startNode = instance.m_segments.m_buffer[(int)num6].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)num6].m_endNode;
						Vector3 position2 = instance.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position3 = instance.m_nodes.m_buffer[(int)endNode].m_position;
						Vector3 vector = (position2 + position3) * 0.5f;
						float num8 = Vector3.SqrMagnitude(position - vector);
						if (num8 < num5)
						{
							num5 = num8;
							result = instance.m_segments.m_buffer[(int)num6].m_fireCoverage;
						}
					}
					num6 = instance.m_segments.m_buffer[(int)num6].m_nextGridSegment;
					if (++num7 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (subStep <= 1 && this.m_service != ItemClass.Service.None && this.m_needCoverage)
		{
			this.UpdateCoverage();
		}
	}

	private void UpdateCoverage()
	{
		while (!Monitor.TryEnter(this.m_lockObject, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		ItemClass.Service service;
		ItemClass.SubService subService;
		ItemClass.Level level;
		float fadeLength;
		bool invertDirection;
		BuildingInfo buildingInfo;
		Vector3 buildingPosition;
		float buildingAngle;
		ushort ignoreBuilding;
		try
		{
			service = this.m_service;
			subService = this.m_subService;
			level = this.m_level;
			fadeLength = this.m_fadeLength;
			invertDirection = this.m_invertDirection;
			buildingInfo = this.m_buildingInfo;
			buildingPosition = this.m_buildingPosition;
			buildingAngle = this.m_buildingAngle;
			ignoreBuilding = this.m_ignoreBuilding;
			this.m_needCoverage = false;
		}
		finally
		{
			Monitor.Exit(this.m_lockObject);
		}
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		NetManager instance2 = Singleton<NetManager>.get_instance();
		FastList<ushort> serviceBuildings = instance.GetServiceBuildings(service);
		for (int i = 0; i < 32768; i++)
		{
			this.m_distances[i] = 1000000f;
		}
		this.m_firstNode = 0;
		this.m_lastNode = 0;
		for (int j = 0; j < serviceBuildings.m_size; j++)
		{
			ushort num = serviceBuildings.m_buffer[j];
			if (num != ignoreBuilding)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if ((info.m_class.m_subService == subService || subService == ItemClass.SubService.None) && (info.m_class.m_level == level || level == ItemClass.Level.None))
				{
					float currentRange = info.m_buildingAI.GetCurrentRange(num, ref instance.m_buildings.m_buffer[(int)num]);
					if (currentRange >= 10f)
					{
						Vector3 vector = instance.m_buildings.m_buffer[(int)num].CalculateSidewalkPosition();
						PathUnit.Position position;
						if (PathManager.FindPathPosition(vector, ItemClass.Service.Road, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Car, false, false, 32f, out position))
						{
							NetInfo info2 = instance2.m_segments.m_buffer[(int)position.m_segment].Info;
							bool flag;
							bool flag2;
							if ((instance2.m_segments.m_buffer[(int)position.m_segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None == invertDirection)
							{
								flag = info2.m_hasForwardVehicleLanes;
								flag2 = info2.m_hasBackwardVehicleLanes;
							}
							else
							{
								flag = info2.m_hasBackwardVehicleLanes;
								flag2 = info2.m_hasForwardVehicleLanes;
							}
							float num2 = Mathf.Max(0.001f, info2.m_averageVehicleLaneSpeed);
							ushort startNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_startNode;
							ushort endNode = instance2.m_segments.m_buffer[(int)position.m_segment].m_endNode;
							Vector3 position2 = instance2.m_nodes.m_buffer[(int)startNode].m_position;
							Vector3 position3 = instance2.m_nodes.m_buffer[(int)endNode].m_position;
							float num3 = Vector3.Distance(position2, vector) / num2 - currentRange + fadeLength * 0.5f;
							float num4 = Vector3.Distance(position3, vector) / num2 - currentRange + fadeLength * 0.5f;
							if (num3 < fadeLength)
							{
								if (flag2)
								{
									this.AddNode(startNode, num3);
								}
								else if (this.m_distances[(int)startNode] == 1000000f)
								{
									this.m_distances[(int)startNode] = 999999f;
								}
							}
							if (num4 < fadeLength)
							{
								if (flag)
								{
									this.AddNode(endNode, num4);
								}
								else if (this.m_distances[(int)endNode] == 1000000f)
								{
									this.m_distances[(int)endNode] = 999999f;
								}
							}
						}
					}
				}
			}
		}
		bool flag3 = false;
		if (buildingInfo != null && buildingPosition.x > -20000f)
		{
			Building building = default(Building);
			building.m_productionRate = 100;
			building.m_flags |= Building.Flags.Active;
			float currentRange2 = buildingInfo.m_buildingAI.GetCurrentRange(0, ref building);
			if (currentRange2 >= 10f)
			{
				Vector3 vector2 = Building.CalculateSidewalkPosition(buildingPosition, buildingAngle, buildingInfo.m_cellLength);
				PathUnit.Position position4;
				if (PathManager.FindPathPosition(vector2, ItemClass.Service.Road, NetInfo.LaneType.Vehicle, VehicleInfo.VehicleType.Car, false, false, 32f, out position4))
				{
					NetInfo info3 = instance2.m_segments.m_buffer[(int)position4.m_segment].Info;
					bool flag4;
					bool flag5;
					if ((instance2.m_segments.m_buffer[(int)position4.m_segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None == invertDirection)
					{
						flag4 = info3.m_hasForwardVehicleLanes;
						flag5 = info3.m_hasBackwardVehicleLanes;
					}
					else
					{
						flag4 = info3.m_hasBackwardVehicleLanes;
						flag5 = info3.m_hasForwardVehicleLanes;
					}
					float num5 = Mathf.Max(0.001f, info3.m_averageVehicleLaneSpeed);
					ushort startNode2 = instance2.m_segments.m_buffer[(int)position4.m_segment].m_startNode;
					ushort endNode2 = instance2.m_segments.m_buffer[(int)position4.m_segment].m_endNode;
					Vector3 position5 = instance2.m_nodes.m_buffer[(int)startNode2].m_position;
					Vector3 position6 = instance2.m_nodes.m_buffer[(int)endNode2].m_position;
					float num6 = Vector3.Distance(position5, vector2) / num5 - currentRange2 + fadeLength * 0.5f;
					float num7 = Vector3.Distance(position6, vector2) / num5 - currentRange2 + fadeLength * 0.5f;
					if (num6 < fadeLength)
					{
						if (flag5)
						{
							this.AddNode(startNode2, num6);
						}
						else if (this.m_distances[(int)startNode2] == 1000000f)
						{
							this.m_distances[(int)startNode2] = 999999f;
						}
					}
					if (num7 < fadeLength)
					{
						if (flag4)
						{
							this.AddNode(endNode2, num7);
						}
						else if (this.m_distances[(int)endNode2] == 1000000f)
						{
							this.m_distances[(int)endNode2] = 999999f;
						}
					}
					flag3 = true;
				}
			}
		}
		while (this.m_firstNode < this.m_lastNode)
		{
			ushort num8 = this.m_nodes[this.m_firstNode++];
			Vector3 position7 = instance2.m_nodes.m_buffer[(int)num8].m_position;
			float num9 = this.m_distances[(int)num8];
			for (int k = 0; k < 8; k++)
			{
				ushort segment = instance2.m_nodes.m_buffer[(int)num8].GetSegment(k);
				if (segment != 0)
				{
					NetInfo info4 = instance2.m_segments.m_buffer[(int)segment].Info;
					if (info4.m_class.m_service == ItemClass.Service.Road && (info4.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
					{
						bool flag6;
						bool flag7;
						if ((instance2.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None == invertDirection)
						{
							flag6 = info4.m_hasForwardVehicleLanes;
							flag7 = info4.m_hasBackwardVehicleLanes;
						}
						else
						{
							flag6 = info4.m_hasBackwardVehicleLanes;
							flag7 = info4.m_hasForwardVehicleLanes;
						}
						float num10 = Mathf.Max(0.001f, info4.m_averageVehicleLaneSpeed);
						ushort startNode3 = instance2.m_segments.m_buffer[(int)segment].m_startNode;
						ushort endNode3 = instance2.m_segments.m_buffer[(int)segment].m_endNode;
						if (startNode3 == num8)
						{
							Vector3 position8 = instance2.m_nodes.m_buffer[(int)endNode3].m_position;
							float num11 = num9 + Vector3.Distance(position7, position8) / num10;
							if (flag6 && num11 < fadeLength)
							{
								this.AddNode(endNode3, num11);
							}
						}
						else
						{
							Vector3 position9 = instance2.m_nodes.m_buffer[(int)startNode3].m_position;
							float num12 = num9 + Vector3.Distance(position7, position9) / num10;
							if (flag7 && num12 < fadeLength)
							{
								this.AddNode(startNode3, num12);
							}
						}
					}
				}
			}
		}
		float num13 = 254f / fadeLength;
		for (int l = 0; l < 32768; l++)
		{
			float num14 = this.m_distances[l];
			if (num14 != 1000000f)
			{
				instance2.m_nodes.m_buffer[l].m_coverage = (byte)Mathf.Clamp(Mathf.RoundToInt(num14 * num13), 0, 254);
			}
			else
			{
				instance2.m_nodes.m_buffer[l].m_coverage = 255;
			}
		}
		if (service == ItemClass.Service.FireDepartment && !flag3)
		{
			for (int m = 0; m < 36864; m++)
			{
				ushort startNode4 = instance2.m_segments.m_buffer[m].m_startNode;
				ushort endNode4 = instance2.m_segments.m_buffer[m].m_endNode;
				float num15 = this.m_distances[(int)startNode4];
				float num16 = this.m_distances[(int)endNode4];
				if (num15 == 1000000f || num16 == 1000000f)
				{
					instance2.m_segments.m_buffer[m].m_fireCoverage = 0;
				}
				else
				{
					float num17 = (num15 + num16) * 0.5f + fadeLength;
					num17 = num17 * 255f / (2f * fadeLength);
					instance2.m_segments.m_buffer[m].m_fireCoverage = (byte)Mathf.Clamp(255 - Mathf.RoundToInt(num17), 0, 255);
				}
			}
		}
		this.m_isReady = true;
		this.m_pending = false;
	}

	private void AddNode(ushort nodeID, float distance)
	{
		float num = this.m_distances[(int)nodeID];
		if (num == 1000000f)
		{
			this.m_distances[(int)nodeID] = distance;
			int i = this.m_lastNode++;
			while (i > this.m_firstNode)
			{
				ushort num2 = this.m_nodes[i - 1];
				if (this.m_distances[(int)num2] <= distance)
				{
					break;
				}
				this.m_nodes[i--] = num2;
			}
			if (i >= this.m_firstNode)
			{
				this.m_nodes[i] = nodeID;
			}
		}
		else if (distance < num)
		{
			this.m_distances[(int)nodeID] = distance;
			int j;
			for (j = this.m_lastNode - 1; j >= this.m_firstNode; j--)
			{
				if (this.m_nodes[j] == nodeID)
				{
					break;
				}
			}
			while (j > this.m_firstNode)
			{
				ushort num3 = this.m_nodes[j - 1];
				if (this.m_distances[(int)num3] <= distance)
				{
					break;
				}
				this.m_nodes[j--] = num3;
			}
			if (j >= this.m_firstNode)
			{
				this.m_nodes[j] = nodeID;
			}
		}
	}

	public void CoverageUpdated(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		while (!Monitor.TryEnter(this.m_lockObject, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			if ((service == this.m_service || service == ItemClass.Service.None) && (subService == this.m_subService || subService == ItemClass.SubService.None || this.m_subService == ItemClass.SubService.None) && (level == this.m_level || level == ItemClass.Level.None || this.m_level == ItemClass.Level.None))
			{
				this.m_needCoverage = true;
			}
		}
		finally
		{
			Monitor.Exit(this.m_lockObject);
		}
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	private ushort[] m_nodes;

	private float[] m_distances;

	private int m_firstNode;

	private int m_lastNode;

	private object m_lockObject;

	private volatile bool m_needCoverage;

	private volatile bool m_isReady;

	private volatile bool m_pending;

	private ItemClass.Service m_service;

	private ItemClass.SubService m_subService;

	private ItemClass.Level m_level;

	private float m_fadeLength;

	private bool m_invertDirection;

	private BuildingInfo m_buildingInfo;

	private Vector3 m_buildingPosition;

	private float m_buildingAngle;

	private ushort m_ignoreBuilding;
}
