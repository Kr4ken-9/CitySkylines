﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using ColossalFramework.UI;
using UnityEngine;

public class ToolBase : MonoBehaviour
{
	public static UILabel cursorInfoLabel
	{
		get
		{
			if (ToolBase.m_CursorInfoLabel == null)
			{
				ToolBase.m_CursorInfoLabel = UIView.Find<UILabel>("CursorInfo");
			}
			return ToolBase.m_CursorInfoLabel;
		}
	}

	public static UILabel extraInfoLabel
	{
		get
		{
			if (ToolBase.m_ExtraInfoLabel == null && ToolBase.cursorInfoLabel != null)
			{
				ToolBase.m_ExtraInfoLabel = Object.Instantiate<GameObject>(ToolBase.cursorInfoLabel.get_gameObject(), ToolBase.cursorInfoLabel.get_transform().get_parent()).GetComponent<UILabel>();
				ToolBase.m_ExtraInfoLabel.set_backgroundSprite(null);
			}
			return ToolBase.m_ExtraInfoLabel;
		}
	}

	public static UIComponent fullscreenContainer
	{
		get
		{
			if (ToolBase.m_FullscreenContainer == null)
			{
				ToolBase.m_FullscreenContainer = UIView.Find("FullScreenContainer");
			}
			return ToolBase.m_FullscreenContainer;
		}
	}

	public static bool OverrideInfoMode
	{
		get
		{
			return ToolBase.m_overrideInfoMode;
		}
		set
		{
			ToolBase.m_overrideInfoMode = value;
		}
	}

	protected void OnGUI()
	{
		if (Singleton<LoadingManager>.get_instance().m_loadingComplete && UIView.ModalInputCount() == 0)
		{
			Event current = Event.get_current();
			this.OnToolGUI(current);
		}
	}

	protected void Update()
	{
		if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Select))
			{
				Event @event = new Event();
				@event.set_type(0);
				@event.set_button(0);
				this.OnToolGUI(@event);
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.Cancel))
			{
				Event event2 = new Event();
				event2.set_type(0);
				event2.set_button(1);
				this.OnToolGUI(event2);
			}
			else if (SteamController.GetDigitalActionDown(SteamController.DigitalInput.ElevationUp) || SteamController.GetDigitalActionDown(SteamController.DigitalInput.ElevationDown))
			{
				Event event3 = new Event();
				event3.set_type(4);
				event3.set_keyCode(0);
				this.OnToolGUI(event3);
			}
			else if (SteamController.GetDigitalActionUp(SteamController.DigitalInput.Select))
			{
				Event event4 = new Event();
				event4.set_type(1);
				event4.set_button(0);
				this.OnToolGUI(event4);
			}
			else if (SteamController.GetDigitalActionUp(SteamController.DigitalInput.Cancel))
			{
				Event event5 = new Event();
				event5.set_type(1);
				event5.set_button(1);
				this.OnToolGUI(event5);
			}
			this.OnToolUpdate();
		}
	}

	protected void LateUpdate()
	{
		if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			this.OnToolLateUpdate();
		}
	}

	protected virtual void OnToolGUI(Event e)
	{
	}

	protected virtual void OnToolUpdate()
	{
	}

	protected virtual void OnToolLateUpdate()
	{
	}

	protected static void ForceInfoMode(InfoManager.InfoMode mode, InfoManager.SubInfoMode subMode)
	{
		if ((mode != InfoManager.InfoMode.None || ToolBase.m_forcedInfoMode != InfoManager.InfoMode.None) && (!ToolBase.m_overrideInfoMode || ToolBase.m_forcedInfoMode != mode))
		{
			ToolBase.m_forcedInfoMode = mode;
			ToolBase.m_overrideInfoMode = false;
			Singleton<InfoManager>.get_instance().SetCurrentMode(mode, subMode);
		}
	}

	protected CursorInfo ToolCursor
	{
		get
		{
			if (this.m_toolController != null)
			{
				return this.m_toolController.ToolCursor;
			}
			return null;
		}
		set
		{
			if (this.m_toolController != null)
			{
				this.m_toolController.ToolCursor = value;
			}
		}
	}

	protected Color GetToolColor(bool warning, bool error)
	{
		if (Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.None)
		{
			if (error)
			{
				return this.m_toolController.m_errorColorInfo;
			}
			if (warning)
			{
				return this.m_toolController.m_warningColorInfo;
			}
			return this.m_toolController.m_validColorInfo;
		}
		else
		{
			if (error)
			{
				return this.m_toolController.m_errorColor;
			}
			if (warning)
			{
				return this.m_toolController.m_warningColor;
			}
			return this.m_toolController.m_validColor;
		}
	}

	protected virtual void Awake()
	{
		this.m_toolController = base.GetComponent<ToolController>();
	}

	protected virtual void OnDestroy()
	{
	}

	protected virtual void OnEnable()
	{
		if (this.m_toolController.CurrentTool != this)
		{
			this.m_toolController.CurrentTool = this;
		}
	}

	protected virtual void OnDisable()
	{
		if (ToolBase.cursorInfoLabel != null && ToolBase.cursorInfoLabel.get_isVisible())
		{
			ToolBase.cursorInfoLabel.set_isVisible(false);
		}
		if (ToolBase.extraInfoLabel != null && ToolBase.extraInfoLabel.get_isVisible())
		{
			ToolBase.extraInfoLabel.set_isVisible(false);
		}
		if (this.m_toolController.NextTool == null || this.m_toolController.NextTool.GetType() != typeof(BulldozeTool))
		{
			Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
		}
	}

	public virtual void RenderGeometry(RenderManager.CameraInfo cameraInfo)
	{
	}

	public virtual void RenderOverlay(RenderManager.CameraInfo cameraInfo)
	{
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible())
		{
			this.m_toolController.RenderBrush(cameraInfo);
		}
	}

	public virtual void PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
	}

	protected void ShowToolInfo(bool show, string text, Vector3 worldPos)
	{
		if (ToolBase.cursorInfoLabel == null)
		{
			return;
		}
		ToolBase.ToolErrors errors = this.GetErrors();
		if ((errors & ToolBase.ToolErrors.VisibleErrors) != ToolBase.ToolErrors.None)
		{
			bool flag;
			if (string.IsNullOrEmpty(text))
			{
				flag = false;
				text = string.Empty;
			}
			else
			{
				flag = true;
				text += "\n";
			}
			text += ToolBase.kCursorInfoErrorColor;
			for (ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.ObjectCollision; toolErrors <= ToolBase.ToolErrors.Cooldown; toolErrors <<= 1)
			{
				if ((errors & toolErrors) != ToolBase.ToolErrors.None)
				{
					if (flag)
					{
						text += "\n";
					}
					flag = true;
					text += this.GetErrorString(toolErrors);
				}
			}
			text += ToolBase.kCursorInfoCloseColorTag;
		}
		if (!string.IsNullOrEmpty(text) && show)
		{
			text = ToolBase.kCursorInfoNormalColor + text + ToolBase.kCursorInfoCloseColorTag;
			ToolBase.cursorInfoLabel.set_isVisible(true);
			UIView uIView = ToolBase.cursorInfoLabel.GetUIView();
			Vector2 vector = (!(ToolBase.fullscreenContainer != null)) ? uIView.GetScreenResolution() : ToolBase.fullscreenContainer.get_size();
			Vector3 vector2 = Camera.get_main().WorldToScreenPoint(worldPos);
			vector2 /= uIView.get_inputScale();
			Vector3 vector3 = UIPivotExtensions.UpperLeftToTransform(ToolBase.cursorInfoLabel.get_pivot(), ToolBase.cursorInfoLabel.get_size(), ToolBase.cursorInfoLabel.get_arbitraryPivotOffset());
			Vector3 relativePosition = uIView.ScreenPointToGUI(vector2) + new Vector2(vector3.x, vector3.y);
			ToolBase.cursorInfoLabel.set_text(text);
			if (relativePosition.x < 0f)
			{
				relativePosition.x = 0f;
			}
			if (relativePosition.y < 0f)
			{
				relativePosition.y = 0f;
			}
			if (relativePosition.x + ToolBase.cursorInfoLabel.get_width() > vector.x)
			{
				relativePosition.x = vector.x - ToolBase.cursorInfoLabel.get_width();
			}
			if (relativePosition.y + ToolBase.cursorInfoLabel.get_height() > vector.y)
			{
				relativePosition.y = vector.y - ToolBase.cursorInfoLabel.get_height();
			}
			ToolBase.cursorInfoLabel.set_relativePosition(relativePosition);
		}
		else
		{
			ToolBase.cursorInfoLabel.set_isVisible(false);
		}
	}

	protected void ShowExtraInfo(bool show, string text, Vector3 worldPos)
	{
		if (ToolBase.extraInfoLabel == null)
		{
			return;
		}
		if (show)
		{
			UIView uIView = ToolBase.extraInfoLabel.GetUIView();
			Vector3 vector = Camera.get_main().WorldToScreenPoint(worldPos);
			vector /= uIView.get_inputScale();
			Vector3 relativePosition = uIView.ScreenPointToGUI(vector) - ToolBase.extraInfoLabel.get_size() * 0.5f;
			ToolBase.extraInfoLabel.set_isVisible(true);
			ToolBase.extraInfoLabel.set_textColor(this.GetToolColor(false, false));
			if (text != null)
			{
				ToolBase.extraInfoLabel.set_text(text);
			}
			ToolBase.extraInfoLabel.set_relativePosition(relativePosition);
		}
		else
		{
			ToolBase.extraInfoLabel.set_isVisible(false);
		}
	}

	protected virtual string GetErrorString(ToolBase.ToolErrors e)
	{
		if (e == ToolBase.ToolErrors.OutOfArea && (this.m_toolController.m_mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
		{
			return Locale.Get("TOOL_ERROR", "OutOfEditingArea");
		}
		return Locale.Get("TOOL_ERROR", e.ToString());
	}

	public virtual void SimulationStep()
	{
	}

	public virtual ToolBase.ToolErrors GetErrors()
	{
		return ToolBase.ToolErrors.None;
	}

	protected static bool RayCast(ToolBase.RaycastInput input, out ToolBase.RaycastOutput output)
	{
		Vector3 origin = input.m_ray.get_origin();
		Vector3 normalized = input.m_ray.get_direction().get_normalized();
		Vector3 vector = input.m_ray.get_origin() + normalized * input.m_length;
		Segment3 ray;
		ray..ctor(origin, vector);
		output.m_hitPos = vector;
		output.m_overlayButtonIndex = 0;
		output.m_netNode = 0;
		output.m_netSegment = 0;
		output.m_building = 0;
		output.m_propInstance = 0;
		output.m_treeInstance = 0u;
		output.m_vehicle = 0;
		output.m_parkedVehicle = 0;
		output.m_citizenInstance = 0;
		output.m_transportLine = 0;
		output.m_transportStopIndex = 0;
		output.m_transportSegmentIndex = 0;
		output.m_district = 0;
		output.m_disaster = 0;
		output.m_currentEditObject = false;
		bool result = false;
		float num = input.m_length;
		Vector3 vector2;
		if (!input.m_ignoreTerrain && Singleton<TerrainManager>.get_instance().RayCast(ray, out vector2))
		{
			float num2 = Vector3.Distance(vector2, origin) + 100f;
			if (num2 < num)
			{
				output.m_hitPos = vector2;
				result = true;
				num = num2;
			}
		}
		if ((input.m_ignoreNodeFlags != NetNode.Flags.All || input.m_ignoreSegmentFlags != NetSegment.Flags.All) && Singleton<NetManager>.get_instance().RayCast(input.m_buildObject as NetInfo, ray, input.m_netSnap, input.m_segmentNameOnly, input.m_netService.m_service, input.m_netService2.m_service, input.m_netService.m_subService, input.m_netService2.m_subService, input.m_netService.m_itemLayers, input.m_netService2.m_itemLayers, input.m_ignoreNodeFlags, input.m_ignoreSegmentFlags, out vector2, out output.m_netNode, out output.m_netSegment))
		{
			float num3 = Vector3.Distance(vector2, origin);
			if (num3 < num)
			{
				output.m_hitPos = vector2;
				result = true;
				num = num3;
			}
			else
			{
				output.m_netNode = 0;
				output.m_netSegment = 0;
			}
		}
		if (input.m_ignoreBuildingFlags != Building.Flags.All && Singleton<BuildingManager>.get_instance().RayCast(ray, input.m_buildingService.m_service, input.m_buildingService.m_subService, input.m_buildingService.m_itemLayers, input.m_ignoreBuildingFlags, out vector2, out output.m_building))
		{
			float num4 = Vector3.Distance(vector2, origin);
			if (num4 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				result = true;
				num = num4;
			}
			else
			{
				output.m_building = 0;
			}
		}
		if (input.m_ignoreDisasterFlags != DisasterData.Flags.All && Singleton<DisasterManager>.get_instance().RayCast(ray, input.m_ignoreDisasterFlags, out vector2, out output.m_disaster))
		{
			float num5 = Vector3.Distance(vector2, origin);
			if (num5 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				result = true;
				num = num5;
			}
			else
			{
				output.m_disaster = 0;
			}
		}
		if (input.m_currentEditObject && Singleton<ToolManager>.get_instance().m_properties.RaycastEditObject(ray, out vector2))
		{
			float num6 = Vector3.Distance(vector2, origin);
			if (num6 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_currentEditObject = true;
				result = true;
				num = num6;
			}
		}
		if (input.m_ignorePropFlags != PropInstance.Flags.All && Singleton<PropManager>.get_instance().RayCast(ray, input.m_propService.m_service, input.m_propService.m_subService, input.m_propService.m_itemLayers, input.m_ignorePropFlags, out vector2, out output.m_propInstance))
		{
			float num7 = Vector3.Distance(vector2, origin) - 0.5f;
			if (num7 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_currentEditObject = false;
				result = true;
				num = num7;
			}
			else
			{
				output.m_propInstance = 0;
			}
		}
		if (input.m_ignoreTreeFlags != TreeInstance.Flags.All && Singleton<TreeManager>.get_instance().RayCast(ray, input.m_treeService.m_service, input.m_treeService.m_subService, input.m_treeService.m_itemLayers, input.m_ignoreTreeFlags, out vector2, out output.m_treeInstance))
		{
			float num8 = Vector3.Distance(vector2, origin) - 1f;
			if (num8 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_propInstance = 0;
				output.m_currentEditObject = false;
				result = true;
				num = num8;
			}
			else
			{
				output.m_treeInstance = 0u;
			}
		}
		if ((input.m_ignoreVehicleFlags != (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding | Vehicle.Flags.LeftHandDrive) || input.m_ignoreParkedVehicleFlags != VehicleParked.Flags.All) && Singleton<VehicleManager>.get_instance().RayCast(ray, input.m_ignoreVehicleFlags, input.m_ignoreParkedVehicleFlags, out vector2, out output.m_vehicle, out output.m_parkedVehicle))
		{
			float num9 = Vector3.Distance(vector2, origin) - 0.5f;
			if (num9 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_propInstance = 0;
				output.m_treeInstance = 0u;
				output.m_currentEditObject = false;
				result = true;
				num = num9;
			}
			else
			{
				output.m_vehicle = 0;
				output.m_parkedVehicle = 0;
			}
		}
		if (input.m_ignoreCitizenFlags != CitizenInstance.Flags.All && Singleton<CitizenManager>.get_instance().RayCast(ray, input.m_ignoreCitizenFlags, out vector2, out output.m_citizenInstance))
		{
			float num10 = Vector3.Distance(vector2, origin) - 0.5f;
			if (num10 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_propInstance = 0;
				output.m_treeInstance = 0u;
				output.m_vehicle = 0;
				output.m_parkedVehicle = 0;
				output.m_currentEditObject = false;
				result = true;
				num = num10;
			}
			else
			{
				output.m_citizenInstance = 0;
			}
		}
		if (input.m_ignoreTransportFlags != TransportLine.Flags.All && Singleton<TransportManager>.get_instance().RayCast(input.m_ray, input.m_length, input.m_transportTypes, out vector2, out output.m_transportLine, out output.m_transportStopIndex, out output.m_transportSegmentIndex))
		{
			float num11 = Vector3.Distance(vector2, origin) - 2f;
			if (num11 < num)
			{
				output.m_hitPos = vector2;
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_propInstance = 0;
				output.m_treeInstance = 0u;
				output.m_vehicle = 0;
				output.m_parkedVehicle = 0;
				output.m_citizenInstance = 0;
				output.m_currentEditObject = false;
				result = true;
			}
			else
			{
				output.m_transportLine = 0;
			}
		}
		if (input.m_ignoreDistrictFlags != District.Flags.All)
		{
			if (input.m_districtNameOnly)
			{
				if (Singleton<DistrictManager>.get_instance().RayCast(ray, input.m_rayRight, out vector2, out output.m_district))
				{
					output.m_hitPos = vector2;
				}
			}
			else
			{
				output.m_district = Singleton<DistrictManager>.get_instance().SampleDistrict(output.m_hitPos);
				if ((Singleton<DistrictManager>.get_instance().m_districts.m_buffer[(int)output.m_district].m_flags & input.m_ignoreDistrictFlags) != District.Flags.None)
				{
					output.m_district = 0;
				}
			}
			if (output.m_district != 0)
			{
				output.m_netNode = 0;
				output.m_netSegment = 0;
				output.m_building = 0;
				output.m_disaster = 0;
				output.m_propInstance = 0;
				output.m_treeInstance = 0u;
				output.m_vehicle = 0;
				output.m_parkedVehicle = 0;
				output.m_citizenInstance = 0;
				output.m_transportLine = 0;
				output.m_transportStopIndex = 0;
				output.m_transportSegmentIndex = 0;
				output.m_currentEditObject = false;
				result = true;
			}
		}
		if (output.m_netNode != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			NetInfo info = instance.m_nodes.m_buffer[(int)output.m_netNode].Info;
			output.m_overlayButtonIndex = info.m_netAI.RayCastNodeButton(output.m_netNode, ref instance.m_nodes.m_buffer[(int)output.m_netNode], ray);
		}
		return result;
	}

	private static readonly string kCursorInfoErrorColor = "<color #ff7e00>";

	private static readonly string kCursorInfoNormalColor = "<color #87d3ff>";

	private static readonly string kCursorInfoCloseColorTag = "</color>";

	protected ToolController m_toolController;

	private static InfoManager.InfoMode m_forcedInfoMode;

	private static bool m_overrideInfoMode;

	private static UILabel m_CursorInfoLabel;

	private static UILabel m_ExtraInfoLabel;

	private static UIComponent m_FullscreenContainer;

	[Flags]
	public enum ToolErrors
	{
		None = 0,
		RaycastFailed = 1,
		Pending = 2,
		ObjectCollision = 16,
		OutOfArea = 32,
		NotEnoughMoney = 64,
		InvalidShape = 128,
		TooShort = 256,
		SlopeTooSteep = 512,
		WaterNotFound = 1024,
		HeightTooHigh = 2048,
		CannotConnect = 4096,
		CannotBuildOnWater = 8192,
		GridNotFound = 16384,
		ShoreNotFound = 32768,
		CannotUpgrade = 65536,
		AlreadyExists = 131072,
		TooManyConnections = 262144,
		ObjectOnFire = 524288,
		NotEmpty = 1048576,
		BurnedDown = 2097152,
		TooManyObjects = 4194304,
		CannotCrossTrack = 8388608,
		PathNotFound = 16777216,
		TooMuchDirt = 33554432,
		NotEnoughDirt = 67108864,
		CanalTooClose = 134217728,
		Collapsed = 268435456,
		Cooldown = 536870912,
		FirstVisibleError = 16,
		LastVisibleError = 536870912,
		VisibleErrors = 2147483632
	}

	public struct RaycastService
	{
		public RaycastService(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers)
		{
			this.m_service = service;
			this.m_subService = subService;
			this.m_itemLayers = itemLayers;
		}

		public ItemClass.Service m_service;

		public ItemClass.SubService m_subService;

		public ItemClass.Layer m_itemLayers;
	}

	public struct RaycastInput
	{
		public RaycastInput(Ray ray, float length)
		{
			this.m_buildObject = null;
			this.m_ray = ray;
			this.m_length = length;
			this.m_rayRight = Vector3.get_right();
			this.m_netService = new ToolBase.RaycastService(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default);
			this.m_netService2 = new ToolBase.RaycastService(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default);
			this.m_buildingService = new ToolBase.RaycastService(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default);
			this.m_propService = new ToolBase.RaycastService(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default);
			this.m_treeService = new ToolBase.RaycastService(ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default);
			this.m_netSnap = -1000f;
			this.m_ignoreTerrain = false;
			this.m_currentEditObject = false;
			this.m_districtNameOnly = false;
			this.m_segmentNameOnly = false;
			this.m_ignoreNodeFlags = NetNode.Flags.All;
			this.m_ignoreSegmentFlags = NetSegment.Flags.All;
			this.m_ignoreBuildingFlags = Building.Flags.All;
			this.m_ignorePropFlags = PropInstance.Flags.All;
			this.m_ignoreTreeFlags = TreeInstance.Flags.All;
			this.m_ignoreVehicleFlags = (Vehicle.Flags.Created | Vehicle.Flags.Deleted | Vehicle.Flags.Spawned | Vehicle.Flags.Inverted | Vehicle.Flags.TransferToTarget | Vehicle.Flags.TransferToSource | Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingPath | Vehicle.Flags.Stopped | Vehicle.Flags.Leaving | Vehicle.Flags.Arriving | Vehicle.Flags.Reversed | Vehicle.Flags.TakingOff | Vehicle.Flags.Flying | Vehicle.Flags.Landing | Vehicle.Flags.WaitingSpace | Vehicle.Flags.WaitingCargo | Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget | Vehicle.Flags.Importing | Vehicle.Flags.Exporting | Vehicle.Flags.Parking | Vehicle.Flags.CustomName | Vehicle.Flags.OnGravel | Vehicle.Flags.WaitingLoading | Vehicle.Flags.Congestion | Vehicle.Flags.DummyTraffic | Vehicle.Flags.Underground | Vehicle.Flags.Transition | Vehicle.Flags.InsideBuilding | Vehicle.Flags.LeftHandDrive);
			this.m_ignoreParkedVehicleFlags = VehicleParked.Flags.All;
			this.m_ignoreCitizenFlags = CitizenInstance.Flags.All;
			this.m_ignoreTransportFlags = TransportLine.Flags.All;
			this.m_ignoreDistrictFlags = District.Flags.All;
			this.m_ignoreDisasterFlags = DisasterData.Flags.All;
			this.m_transportTypes = -1;
		}

		public PrefabInfo m_buildObject;

		public Ray m_ray;

		public Vector3 m_rayRight;

		public ToolBase.RaycastService m_netService;

		public ToolBase.RaycastService m_netService2;

		public ToolBase.RaycastService m_buildingService;

		public ToolBase.RaycastService m_propService;

		public ToolBase.RaycastService m_treeService;

		public float m_length;

		public float m_netSnap;

		public bool m_ignoreTerrain;

		public bool m_currentEditObject;

		public bool m_districtNameOnly;

		public bool m_segmentNameOnly;

		public NetNode.Flags m_ignoreNodeFlags;

		public NetSegment.Flags m_ignoreSegmentFlags;

		public Building.Flags m_ignoreBuildingFlags;

		public PropInstance.Flags m_ignorePropFlags;

		public TreeInstance.Flags m_ignoreTreeFlags;

		public Vehicle.Flags m_ignoreVehicleFlags;

		public VehicleParked.Flags m_ignoreParkedVehicleFlags;

		public CitizenInstance.Flags m_ignoreCitizenFlags;

		public TransportLine.Flags m_ignoreTransportFlags;

		public District.Flags m_ignoreDistrictFlags;

		public DisasterData.Flags m_ignoreDisasterFlags;

		public int m_transportTypes;
	}

	public struct RaycastOutput
	{
		public Vector3 m_hitPos;

		public int m_transportStopIndex;

		public int m_transportSegmentIndex;

		public int m_overlayButtonIndex;

		public uint m_treeInstance;

		public ushort m_netNode;

		public ushort m_netSegment;

		public ushort m_building;

		public ushort m_propInstance;

		public ushort m_vehicle;

		public ushort m_parkedVehicle;

		public ushort m_citizenInstance;

		public ushort m_transportLine;

		public ushort m_disaster;

		public byte m_district;

		public bool m_currentEditObject;
	}
}
