﻿using System;
using ColossalFramework;
using UnityEngine;

public class BicycleAI : VehicleAI
{
	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, 2, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		uint citizenUnits = data.m_citizenUnits;
		if (citizenUnits == 0u || Singleton<CitizenManager>.get_instance().m_units.m_buffer[(int)((UIntPtr)citizenUnits)].m_citizen0 == 0u)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		}
	}

	protected virtual void PathfindSuccess(ushort vehicleID, ref Vehicle data)
	{
	}

	protected virtual void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		ushort num = 0;
		if (vehicleData.m_citizenUnits != 0u)
		{
			uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)vehicleData.m_citizenUnits)].m_citizen0;
			if (citizen != 0u)
			{
				num = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
			}
		}
		if (num != 0)
		{
			CitizenInstance.Flags flags = instance.m_instances.m_buffer[(int)num].m_flags;
			if ((flags & CitizenInstance.Flags.Underground) != CitizenInstance.Flags.None)
			{
				vehicleData.m_flags |= Vehicle.Flags.Underground;
			}
			else
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Underground;
			}
			if ((flags & CitizenInstance.Flags.Transition) != CitizenInstance.Flags.None)
			{
				vehicleData.m_flags |= Vehicle.Flags.Transition;
			}
			else
			{
				vehicleData.m_flags &= ~Vehicle.Flags.Transition;
			}
			if ((flags & CitizenInstance.Flags.InsideBuilding) != CitizenInstance.Flags.None)
			{
				vehicleData.m_flags |= Vehicle.Flags.InsideBuilding;
			}
			else
			{
				vehicleData.m_flags &= ~Vehicle.Flags.InsideBuilding;
			}
			CitizenInstance.Frame lastFrameData = instance.m_instances.m_buffer[(int)num].GetLastFrameData();
			frameData.m_velocity = lastFrameData.m_velocity;
			frameData.m_position = lastFrameData.m_position;
			frameData.m_rotation = lastFrameData.m_rotation;
			frameData.m_swayVelocity = Vector3.get_zero();
			frameData.m_swayPosition = Vector3.get_zero();
			frameData.m_steerAngle = 0f;
			frameData.m_travelDistance += lastFrameData.m_velocity.get_magnitude();
			frameData.m_lightIntensity = new Vector4(5f, 1f, 0f, 0f);
			frameData.m_underground = ((vehicleData.m_flags & Vehicle.Flags.Underground) != (Vehicle.Flags)0);
			frameData.m_transition = ((vehicleData.m_flags & Vehicle.Flags.Transition) != (Vehicle.Flags)0);
			frameData.m_insideBuilding = ((vehicleData.m_flags & Vehicle.Flags.InsideBuilding) != (Vehicle.Flags)0);
		}
		if ((ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 255u) == (ulong)((long)(vehicleID & 255)))
		{
			StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt32>(StatisticType.CyclistAmount);
			statisticBase.Add(1);
		}
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
	}

	public override void FrameDataUpdated(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData)
	{
		Vector3 vector = frameData.m_position + frameData.m_velocity * 0.5f;
		Vector3 vector2 = frameData.m_rotation * new Vector3(0f, 0f, Mathf.Max(0.5f, this.m_info.m_generatedInfo.m_size.z * 0.5f - 1f));
		vehicleData.m_segment.a = vector - vector2;
		vehicleData.m_segment.b = vector + vector2;
	}

	private ushort GetDriverInstance(ushort vehicleID, ref Vehicle data)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		uint num = data.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						return instance2;
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return 0;
	}

	public override InstanceID GetOwnerID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		ushort driverInstance = this.GetDriverInstance(vehicleID, ref vehicleData);
		if (driverInstance != 0)
		{
			result.Citizen = Singleton<CitizenManager>.get_instance().m_instances.m_buffer[(int)driverInstance].m_citizen;
		}
		return result;
	}

	protected virtual bool IsHeavyVehicle()
	{
		return false;
	}
}
