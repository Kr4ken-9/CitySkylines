﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public struct DisasterData
{
	public DisasterInfo Info
	{
		get
		{
			return PrefabCollection<DisasterInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public DateTime StartTime
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().FrameToTime(this.m_startFrame);
		}
		set
		{
			this.m_startFrame = Singleton<SimulationManager>.get_instance().TimeToFrame(value);
		}
	}

	public bool RayCast(Segment3 ray, out float t)
	{
		t = 2f;
		float num = 10f;
		float num2 = 50f;
		float y = this.m_targetPosition.y;
		float num3 = this.m_targetPosition.y + num;
		if (ray.a.y > num3 && ray.b.y > num3)
		{
			return false;
		}
		if (ray.a.y < y && ray.b.y < y)
		{
			return false;
		}
		float num4;
		float num5;
		if (ray.b.y > ray.a.y)
		{
			if (Segment1.Intersect(ray.a.y, ray.b.y, y, ref num4))
			{
				if (num4 < t)
				{
					Vector3 vector = ray.Position(num4);
					if (VectorUtils.LengthSqrXZ(vector - this.m_targetPosition) < num2 * num2)
					{
						t = num4;
					}
				}
			}
			else
			{
				num4 = -1f;
			}
			if (Segment1.Intersect(ray.a.y, ray.b.y, num3, ref num5))
			{
				if (num5 < t)
				{
					Vector3 vector2 = ray.Position(num5);
					if (VectorUtils.LengthSqrXZ(vector2 - this.m_targetPosition) < num2 * num2)
					{
						t = num5;
					}
				}
			}
			else
			{
				num5 = 2f;
			}
		}
		else if (ray.b.y < ray.a.y)
		{
			if (Segment1.Intersect(ray.a.y, ray.b.y, y, ref num5))
			{
				if (num5 < t)
				{
					Vector3 vector3 = ray.Position(num5);
					if (VectorUtils.LengthSqrXZ(vector3 - this.m_targetPosition) < num2 * num2)
					{
						t = num5;
					}
				}
			}
			else
			{
				num5 = -1f;
			}
			if (Segment1.Intersect(ray.a.y, ray.b.y, num3, ref num4))
			{
				if (num4 < t)
				{
					Vector3 vector4 = ray.Position(num4);
					if (VectorUtils.LengthSqrXZ(vector4 - this.m_targetPosition) < num2 * num2)
					{
						t = num4;
					}
				}
			}
			else
			{
				num4 = 2f;
			}
		}
		else
		{
			num4 = -1f;
			num5 = 2f;
		}
		Vector3 vector5 = ray.b - ray.a;
		Vector3 vector6 = ray.a - this.m_targetPosition;
		float num6 = VectorUtils.LengthSqrXZ(vector5);
		float num7 = VectorUtils.DotXZ(vector5, vector6);
		float num8 = VectorUtils.LengthSqrXZ(vector6) - num2 * num2;
		float num9 = num7 * num7 - 4f * num6 * num8;
		if (num6 == 0f || num9 < 0f)
		{
			return t <= 1f;
		}
		float num10 = Mathf.Sqrt(num9);
		float num11 = (-num7 + num10) / (2f * num6);
		float num12 = (-num7 - num10) / (2f * num6);
		if (num11 > num4 && num11 < num5 && num11 < t)
		{
			t = num11;
		}
		if (num12 > num4 && num12 < num5 && num12 < t)
		{
			t = num12;
		}
		return t <= 1f;
	}

	public DisasterData.Flags m_flags;

	public Vector3 m_targetPosition;

	public ulong m_randomSeed;

	public uint m_startFrame;

	public uint m_activationFrame;

	public uint m_casualtiesCount;

	public uint m_destroyedRoadLength;

	public uint m_destroyedTrackLength;

	public uint m_destroyedPowerLength;

	public uint m_treeFireCount;

	public float m_angle;

	public ushort m_infoIndex;

	public ushort m_waveIndex;

	public ushort m_collapsedCount;

	public ushort m_upgradedCount;

	public ushort m_buildingFireCount;

	public byte m_intensity;

	public byte m_chirpCooldown;

	public byte m_broadcastCooldown;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Emerging = 4,
		Active = 8,
		Clearing = 16,
		Finished = 32,
		SelfTrigger = 64,
		Hidden = 128,
		Significant = 256,
		Detected = 512,
		Follow = 1024,
		CustomName = 2048,
		Located = 4096,
		UnDetected = 8192,
		Warning = 16384,
		Persistent = 32768,
		Repeat = 65536,
		UnReported = 131072,
		All = -1
	}
}
