﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class PlayerNetAI : NetAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.SetPrefab(this.m_info);
		}
	}

	public override int GetConstructionCost(Vector3 startPos, Vector3 endPos, float startHeight, float endHeight)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		int result = this.m_constructionCost * Mathf.RoundToInt(num / 8f + 0.1f);
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override int GetMaintenanceCost(Vector3 startPos, Vector3 endPos)
	{
		float num = VectorUtils.LengthXZ(endPos - startPos);
		int result = this.m_maintenanceCost * Mathf.RoundToInt(num / 8f + 0.1f);
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetMaintenanceCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.Unlock();
		}
	}

	public override void ReleaseSegment(ushort segmentID, ref NetSegment data)
	{
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.Relock();
		}
		base.ReleaseSegment(segmentID, ref data);
	}

	public override void SimulationStep(ushort segmentID, ref NetSegment data)
	{
		base.SimulationStep(segmentID, ref data);
		if (this.HasMaintenanceCost(segmentID, ref data))
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
			int num = this.GetMaintenanceCost(position, position2);
			bool flag = (ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(segmentID & 15));
			if (num != 0)
			{
				if (flag)
				{
					num = num * 16 / 100 - num / 100 * 15;
				}
				else
				{
					num /= 100;
				}
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num, this.m_info.m_class);
			}
			if (flag)
			{
				float num2 = (float)instance.m_nodes.m_buffer[(int)data.m_startNode].m_elevation;
				float num3 = (float)instance.m_nodes.m_buffer[(int)data.m_endNode].m_elevation;
				if (this.IsUnderground())
				{
					num2 = -num2;
					num3 = -num3;
				}
				int constructionCost = this.GetConstructionCost(position, position2, num2, num3);
				if (constructionCost != 0)
				{
					StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt64>(StatisticType.CityValue);
					if (statisticBase != null)
					{
						statisticBase.Add(constructionCost);
					}
				}
			}
		}
	}

	protected virtual bool HasMaintenanceCost(ushort segmentID, ref NetSegment data)
	{
		return (data.m_flags & (NetSegment.Flags.Original | NetSegment.Flags.Untouchable)) == NetSegment.Flags.None;
	}

	public override void SimulationStep(ushort nodeID, ref NetNode data)
	{
		base.SimulationStep(nodeID, ref data);
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		if (demolish)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			ushort num = 0;
			if ((data.m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
			{
				num = NetSegment.FindOwnerBuilding(segmentID, 363f);
				int num2 = 0;
				while (num != 0 && (instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Untouchable) != Building.Flags.None)
				{
					num = instance.m_buildings.m_buffer[(int)num].m_parentBuilding;
					if (++num2 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
			if (num != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				if ((instance.m_buildings.m_buffer[(int)num].m_flags & Building.Flags.Collapsed) == Building.Flags.None && !info.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, true, false, 0))
				{
					return false;
				}
			}
			Singleton<NetManager>.get_instance().ReleaseSegment(segmentID, false);
			if (num != 0)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)num].Info;
				info2.m_buildingAI.CollapseBuilding(num, ref instance.m_buildings.m_buffer[(int)num], group, false, false, 0);
			}
			return true;
		}
		return base.CollapseSegment(segmentID, ref data, group, demolish);
	}

	public override int GetConstructionCost()
	{
		int constructionCost = this.m_constructionCost;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref constructionCost, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return constructionCost;
	}

	public override int GetMaintenanceCost()
	{
		int maintenanceCost = this.m_maintenanceCost;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetMaintenanceCost(ref maintenanceCost, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return maintenanceCost;
	}

	[CustomizableProperty("Construction Cost", "Properties")]
	public int m_constructionCost = 1000;

	[CustomizableProperty("Maintenance Cost", "Properties")]
	public int m_maintenanceCost = 2;

	public ManualMilestone m_createPassMilestone;
}
