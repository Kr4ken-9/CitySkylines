﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class HappinessInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_ResidentialGroup = base.Find("ResidentialGroup");
		this.m_CommercialGroup = base.Find("CommercialGroup");
		this.m_IndustrialGroup = base.Find("IndustrialGroup");
		this.m_OfficeGroup = base.Find("OfficeGroup");
		this.m_ResidentialThumb = this.m_ResidentialGroup.Find<UISprite>("ResidentialThumb");
		this.m_CommercialThumb = this.m_CommercialGroup.Find<UISprite>("CommercialThumb");
		this.m_IndustrialThumb = this.m_IndustrialGroup.Find<UISprite>("IndustrialThumb");
		this.m_OfficeThumb = this.m_OfficeGroup.Find<UISprite>("OfficeThumb");
		this.m_ResidentialBar = this.m_ResidentialGroup.Find<UISlider>("ResidentialBar");
		this.m_CommercialBar = this.m_CommercialGroup.Find<UISlider>("CommercialBar");
		this.m_IndustrialBar = this.m_IndustrialGroup.Find<UISlider>("IndustrialBar");
		this.m_OfficeBar = this.m_OfficeGroup.Find<UISlider>("OfficeBar");
		this.m_ResidentialLabel = this.m_ResidentialGroup.Find<UILabel>("ResidentialLabel");
		this.m_CommercialLabel = this.m_CommercialGroup.Find<UILabel>("CommercialLabel");
		this.m_IndustrialLabel = this.m_IndustrialGroup.Find<UILabel>("IndustrialLabel");
		this.m_OfficeLabel = this.m_OfficeGroup.Find<UILabel>("OfficeLabel");
		this.m_ResidentialSprite = this.m_ResidentialGroup.Find<UISprite>("ResidentialSprite");
		this.m_CommercialSprite = this.m_CommercialGroup.Find<UISprite>("CommercialSprite");
		this.m_IndustrialSprite = this.m_IndustrialGroup.Find<UISprite>("IndustrialSprite");
		this.m_OfficeSprite = this.m_OfficeGroup.Find<UISprite>("OfficeSprite");
	}

	protected override void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
		if (Singleton<InfoManager>.get_exists())
		{
			uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[5].m_negativeColor);
			uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[5].m_targetColor);
			uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.2f);
			uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1.25f);
			uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0f);
			this.m_ResidentialBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
			this.m_CommercialBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
			this.m_IndustrialBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
			this.m_OfficeBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
		}
	}

	public string residentialHappinessIcon
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHappiness));
			}
			return ToolsModifierControl.GetHappinessString(Citizen.Happiness.Bad);
		}
	}

	public float residentialHappiness
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHappiness;
			}
			return 0f;
		}
	}

	public string commercialHappinessIcon
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_commercialData.m_finalHappiness));
			}
			return ToolsModifierControl.GetHappinessString(Citizen.Happiness.Bad);
		}
	}

	public float commercialHappiness
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_commercialData.m_finalHappiness;
			}
			return 0f;
		}
	}

	public string industrialHappinessIcon
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_industrialData.m_finalHappiness));
			}
			return ToolsModifierControl.GetHappinessString(Citizen.Happiness.Bad);
		}
	}

	public float industrialHappiness
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_industrialData.m_finalHappiness;
			}
			return 0f;
		}
	}

	public string officeHappinessIcon
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_officeData.m_finalHappiness));
			}
			return ToolsModifierControl.GetHappinessString(Citizen.Happiness.Bad);
		}
	}

	public float officeHappiness
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_officeData.m_finalHappiness;
			}
			return 0f;
		}
	}

	public bool residentialExists
	{
		get
		{
			return Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_residentialData.m_finalHomeOrWorkCount > 0u;
		}
	}

	public bool commercialExists
	{
		get
		{
			return Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_commercialData.m_finalHomeOrWorkCount > 0u;
		}
	}

	public bool industrialExists
	{
		get
		{
			return Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_industrialData.m_finalHomeOrWorkCount > 0u;
		}
	}

	public bool officeExists
	{
		get
		{
			return Singleton<DistrictManager>.get_exists() && Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].m_officeData.m_finalHomeOrWorkCount > 0u;
		}
	}

	protected override void UpdatePanel()
	{
		this.m_ResidentialSprite.set_isVisible(this.residentialExists);
		this.m_CommercialSprite.set_isVisible(this.commercialExists);
		this.m_IndustrialSprite.set_isVisible(this.industrialExists);
		this.m_OfficeSprite.set_isVisible(this.officeExists);
		this.m_ResidentialThumb.set_spriteName((!this.residentialExists) ? "ZoningResidentialLowDisabled" : "ZoningResidentialLow");
		this.m_CommercialThumb.set_spriteName((!this.commercialExists) ? "ZoningCommercialLowDisabled" : "ZoningCommercialLow");
		this.m_IndustrialThumb.set_spriteName((!this.industrialExists) ? "ZoningIndustrialDisabled" : "ZoningIndustrial");
		this.m_OfficeThumb.set_spriteName((!this.officeExists) ? "ZoningOfficeDisabled" : "ZoningOffice");
		this.m_ResidentialBar.get_thumbObject().set_isVisible(this.residentialExists);
		this.m_CommercialBar.get_thumbObject().set_isVisible(this.commercialExists);
		this.m_IndustrialBar.get_thumbObject().set_isVisible(this.industrialExists);
		this.m_OfficeBar.get_thumbObject().set_isVisible(this.officeExists);
		this.m_ResidentialBar.set_value(this.residentialHappiness);
		this.m_CommercialBar.set_value(this.commercialHappiness);
		this.m_IndustrialBar.set_value(this.industrialHappiness);
		this.m_OfficeBar.set_value(this.officeHappiness);
		this.m_ResidentialSprite.set_spriteName(this.residentialHappinessIcon);
		this.m_CommercialSprite.set_spriteName(this.commercialHappinessIcon);
		this.m_IndustrialSprite.set_spriteName(this.industrialHappinessIcon);
		this.m_OfficeSprite.set_spriteName(this.officeHappinessIcon);
		this.m_ResidentialLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_HAPPINESS_RESIDENTIAL"),
			" - ",
			this.m_ResidentialBar.get_value(),
			"%"
		}));
		this.m_CommercialLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_HAPPINESS_COMMERCIAL"),
			" - ",
			this.m_CommercialBar.get_value(),
			"%"
		}));
		this.m_IndustrialLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_HAPPINESS_INDUSTRIAL"),
			" - ",
			this.m_IndustrialBar.get_value(),
			"%"
		}));
		this.m_OfficeLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_HAPPINESS_OFFICE"),
			" - ",
			this.m_OfficeBar.get_value(),
			"%"
		}));
	}

	private UIComponent m_ResidentialGroup;

	private UIComponent m_CommercialGroup;

	private UIComponent m_IndustrialGroup;

	private UIComponent m_OfficeGroup;

	private UISlider m_ResidentialBar;

	private UISlider m_CommercialBar;

	private UISlider m_IndustrialBar;

	private UISlider m_OfficeBar;

	private UISprite m_ResidentialThumb;

	private UISprite m_CommercialThumb;

	private UISprite m_IndustrialThumb;

	private UISprite m_OfficeThumb;

	private UISprite m_ResidentialSprite;

	private UISprite m_CommercialSprite;

	private UISprite m_IndustrialSprite;

	private UISprite m_OfficeSprite;

	private UILabel m_ResidentialLabel;

	private UILabel m_CommercialLabel;

	private UILabel m_IndustrialLabel;

	private UILabel m_OfficeLabel;
}
