﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class OldestCitizenMilestone : MilestoneInfo
{
	public override void GetLocalizedProgressImpl(int targetCount, ref MilestoneInfo.ProgressInfo totalProgress, FastList<MilestoneInfo.ProgressInfo> subProgress)
	{
		if (totalProgress.m_description == null)
		{
			this.GetProgressInfo(ref totalProgress);
		}
		else
		{
			subProgress.EnsureCapacity(subProgress.m_size + 1);
			this.GetProgressInfo(ref subProgress.m_buffer[subProgress.m_size]);
			subProgress.m_size++;
		}
	}

	private void GetProgressInfo(ref MilestoneInfo.ProgressInfo info)
	{
		MilestoneInfo.Data data = this.m_data;
		info.m_min = 0f;
		info.m_max = (float)this.m_targetAge;
		long num;
		if (data != null)
		{
			info.m_passed = (data.m_passedCount != 0);
			info.m_current = (float)data.m_progress;
			num = data.m_progress;
		}
		else
		{
			info.m_passed = false;
			info.m_current = 0f;
			num = 0L;
		}
		if (num > (long)this.m_targetAge)
		{
			num = (long)this.m_targetAge;
		}
		info.m_description = Locale.Get("MILESTONE_OLDEST_DESC");
		info.m_progress = StringUtils.SafeFormat(Locale.Get("MILESTONE_OLDEST_PROG"), num * 100L / (long)this.m_targetAge);
		if (info.m_prefabs != null)
		{
			info.m_prefabs.Clear();
		}
	}

	public override MilestoneInfo.Data CheckPassed(bool forceUnlock, bool forceRelock)
	{
		MilestoneInfo.Data data = this.GetData();
		if (data.m_passedCount != 0 && !this.m_canRelock)
		{
			return data;
		}
		if (forceUnlock)
		{
			data.m_progress = (long)this.m_targetAge;
			data.m_passedCount = Mathf.Max(1, data.m_passedCount);
		}
		else if (forceRelock)
		{
			data.m_progress = 0L;
			data.m_passedCount = 0;
		}
		else
		{
			data.m_progress = (long)Mathf.Min(this.m_targetAge, Singleton<CitizenManager>.get_instance().m_finalOldestOriginalResident);
			data.m_passedCount = ((data.m_progress != (long)this.m_targetAge) ? 0 : 1);
		}
		return data;
	}

	public override float GetProgress()
	{
		MilestoneInfo.Data data = this.m_data;
		if (data == null)
		{
			return 0f;
		}
		if (data.m_passedCount != 0)
		{
			return 1f;
		}
		float num = 1f;
		if (this.m_targetAge != 0)
		{
			num = Mathf.Min(num, Mathf.Clamp01((float)data.m_progress / (float)this.m_targetAge));
		}
		return num;
	}

	public int m_targetAge = 80;
}
