﻿using System;
using ColossalFramework;
using UnityEngine;

public class NetSegmentTutorialTag : IUITag
{
	public NetSegmentTutorialTag(ushort id)
	{
		this.m_ID = id;
	}

	public bool isValid
	{
		get
		{
			return true;
		}
	}

	public bool isDynamic
	{
		get
		{
			return true;
		}
	}

	public bool isRelative
	{
		get
		{
			return true;
		}
	}

	public bool locationLess
	{
		get
		{
			return false;
		}
	}

	public Vector3 position
	{
		get
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort startNode = instance.m_segments.m_buffer[(int)this.m_ID].m_startNode;
			ushort endNode = instance.m_segments.m_buffer[(int)this.m_ID].m_endNode;
			Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
			Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
			return (position + position2) * 0.5f;
		}
	}

	private ushort m_ID;
}
