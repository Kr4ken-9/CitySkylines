﻿using System;

public class UIHappinessWrapper : UIWrapper<byte>
{
	public UIHappinessWrapper(byte def) : base(def)
	{
		this.m_String = "Happy";
	}

	public override void Check(byte newVal)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			this.m_String = ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)this.m_Value));
		}
	}
}
