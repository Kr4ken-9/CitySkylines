﻿using System;

public class DisastersGroupPanel : GeneratedGroupPanel
{
	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("Disasters");
		return true;
	}
}
