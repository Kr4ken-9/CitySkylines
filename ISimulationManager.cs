﻿using System;
using ColossalFramework.IO;

public interface ISimulationManager
{
	string GetName();

	ThreadProfiler GetSimulationProfiler();

	void SimulationStep(int subStep);

	void EarlyUpdateData();

	void UpdateData(SimulationManager.UpdateMode mode);

	void LateUpdateData(SimulationManager.UpdateMode mode);

	void GetData(FastList<IDataContainer> data);
}
