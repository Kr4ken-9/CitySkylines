﻿using System;

public class OptionPanelBase : ToolsModifierControl
{
	protected virtual void Refresh()
	{
	}

	public void ShowPanel()
	{
		this.Refresh();
		base.get_component().Show();
	}

	public void HidePanel()
	{
		base.get_component().Hide();
	}
}
