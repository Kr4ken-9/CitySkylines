﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class SetAudioManager : MonoBehaviour
{
	private void Awake()
	{
		if (Singleton<AudioManager>.get_exists())
		{
			UIView.playSoundDelegate = new UIView.PlaySoundDelegate(Singleton<AudioManager>.get_instance().PlaySound);
		}
	}
}
