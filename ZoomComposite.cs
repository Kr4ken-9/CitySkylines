﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class ZoomComposite : UICustomControl
{
	private void Awake()
	{
		this.m_SunMoonSprite = base.Find("SunMoon").get_transform();
		this.m_Sky = base.Find<UISprite>("ZoomComposite");
		this.m_Ground = base.Find<UISprite>("Ground");
	}

	private void Update()
	{
		float num = DayNightProperties.instance.normalizedTimeOfDay * 360f - 180f;
		this.m_SunMoonSprite.set_localEulerAngles(new Vector3(0f, 0f, -num));
		Color currentSkyColor = DayNightProperties.instance.currentSkyColor;
		currentSkyColor.a = 1f;
		this.m_Sky.set_color(currentSkyColor);
		Color currentGroundColor = DayNightProperties.instance.currentGroundColor;
		currentGroundColor.a = 1f;
		this.m_Ground.set_color(currentGroundColor);
	}

	private Transform m_SunMoonSprite;

	private UISprite m_Sky;

	private UISprite m_Ground;
}
