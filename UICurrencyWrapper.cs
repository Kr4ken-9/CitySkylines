﻿using System;
using ColossalFramework.Globalization;

public class UICurrencyWrapper : UIWrapper<long>
{
	public UICurrencyWrapper(long def) : base(def)
	{
	}

	public override void Check(long newVal)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			if (this.m_Value == 9223372036854775807L)
			{
				this.m_String = "∞";
			}
			else
			{
				this.m_String = ((double)this.m_Value / 100.0).ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			}
		}
	}

	public void Check(long newVal, string specialMoneyFormat)
	{
		if (this.m_Value != newVal)
		{
			this.m_Value = newVal;
			if (this.m_Value == 9223372036854775807L)
			{
				this.m_String = "∞";
			}
			else
			{
				this.m_String = ((double)this.m_Value / 100.0).ToString(specialMoneyFormat, LocaleManager.get_cultureInfo());
			}
		}
	}
}
