﻿using System;
using ColossalFramework.Packaging;

[Serializable]
public abstract class MetaData
{
	public virtual bool getBuiltin
	{
		get
		{
			return false;
		}
	}

	public virtual string getEnvironment
	{
		get
		{
			return string.Empty;
		}
	}

	public virtual int getBuildableArea
	{
		get
		{
			return 0;
		}
	}

	public virtual string getPopulation
	{
		get
		{
			return string.Empty;
		}
	}

	public virtual DateTime getTimeStamp
	{
		get
		{
			return default(DateTime);
		}
	}

	public virtual string[] getTags
	{
		get
		{
			return new string[0];
		}
	}

	public Package.Asset assetRef;
}
