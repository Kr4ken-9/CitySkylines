﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class StructuresPropertiesPanel : UICustomControl
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.UnBindActions();
		this.m_UpwardRoadDiffuse.set_texture(Singleton<NetManager>.get_instance().m_properties.m_upwardDiffuse);
		this.m_DownwardRoadDiffuse.set_texture(Singleton<NetManager>.get_instance().m_properties.m_downwardDiffuse);
		this.m_BuildingFloorDiffuse.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_floorDiffuse);
		this.m_BuildingBaseDiffuse.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_baseDiffuse);
		this.m_BuildingBaseNormal.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_baseNormal);
		this.m_BuildingBurntDiffuse.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_burnedDiffuse);
		this.m_BuildingAbandonedDiffuse.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_abandonedDiffuse);
		this.m_LightColorPalette.set_texture(Singleton<BuildingManager>.get_instance().m_properties.m_lightColorPalette);
		this.m_Defaults.Store<Texture>("upwardRoadTex", this.m_UpwardRoadDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("downwardRoadTex", this.m_DownwardRoadDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("floorTex", this.m_BuildingFloorDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("baseDiffuseTex", this.m_BuildingBaseDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("baseNormalTex", this.m_BuildingBaseNormal.get_texture());
		this.m_Defaults.Store<Texture>("burntTex", this.m_BuildingBurntDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("abandonedTex", this.m_BuildingAbandonedDiffuse.get_texture());
		this.m_Defaults.Store<Texture>("lightPaletteTex", this.m_LightColorPalette.get_texture());
		this.BindActions();
	}

	public void ResetAll()
	{
		this.ResetStructures(null, null);
	}

	public void ResetStructures(UIComponent c, UIMouseEventParameter p)
	{
		this.m_UpwardRoadDiffuse.set_texture(this.m_Defaults.Get<Texture>("upwardRoadTex"));
		this.m_DownwardRoadDiffuse.set_texture(this.m_Defaults.Get<Texture>("downwardRoadTex"));
		this.m_BuildingFloorDiffuse.set_texture(this.m_Defaults.Get<Texture>("floorTex"));
		this.m_BuildingBaseDiffuse.set_texture(this.m_Defaults.Get<Texture>("baseDiffuseTex"));
		this.m_BuildingBaseNormal.set_texture(this.m_Defaults.Get<Texture>("baseNormalTex"));
		this.m_BuildingBurntDiffuse.set_texture(this.m_Defaults.Get<Texture>("burntTex"));
		this.m_BuildingAbandonedDiffuse.set_texture(this.m_Defaults.Get<Texture>("abandonedTex"));
		this.m_LightColorPalette.set_texture(this.m_Defaults.Get<Texture>("lightPaletteTex"));
	}

	private void OnHoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_HoverColor);
	}

	private void OnUnhoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_NormalColor);
	}

	private void OnTextureClicked(UIComponent c, UIMouseEventParameter p)
	{
		UITextureSprite s = p.get_source() as UITextureSprite;
		if (s != null)
		{
			TexturePicker tp = UIView.get_library().Get<TexturePicker>("TexturePicker");
			tp.ShowModal(s, TexturePicker.Type.ThemeTextures, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.ToggleTextureEvents(s, false);
					this.CompressThreaded(s, tp.selectedImage);
				}
			});
		}
	}

	private void OnAssignLightColorPalette(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_lightColorPalette = t;
		BuildingProperties.EnsurePaletteSettings(Singleton<BuildingManager>.get_instance().m_properties.m_lightColorPalette);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignBuildingAbandonedDiffuse(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_abandonedDiffuse = t;
		BuildingProperties.EnsureTextureSettings(Singleton<BuildingManager>.get_instance().m_properties.m_abandonedDiffuse);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignBuildingBurntDiffuse(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_burnedDiffuse = t;
		BuildingProperties.EnsureTextureSettings(Singleton<BuildingManager>.get_instance().m_properties.m_burnedDiffuse);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignBuildingBaseDiffuse(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_baseDiffuse = t;
		BuildingProperties.EnsureTextureSettings(Singleton<BuildingManager>.get_instance().m_properties.m_baseDiffuse);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignBuildingBaseNormal(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_baseNormal = t;
		BuildingProperties.EnsureTextureSettings(Singleton<BuildingManager>.get_instance().m_properties.m_baseNormal);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignFloorDiffuse(UIComponent c, Texture t)
	{
		Singleton<BuildingManager>.get_instance().m_properties.m_floorDiffuse = t;
		BuildingProperties.EnsureTextureSettings(Singleton<BuildingManager>.get_instance().m_properties.m_floorDiffuse);
		Singleton<BuildingManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignUpwardRoadDiffuse(UIComponent c, Texture t)
	{
		Singleton<NetManager>.get_instance().m_properties.m_upwardDiffuse = t;
		NetProperties.EnsureTextureSettings(Singleton<NetManager>.get_instance().m_properties.m_upwardDiffuse);
		Singleton<NetManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignDownwardRoadDiffuse(UIComponent c, Texture t)
	{
		Singleton<NetManager>.get_instance().m_properties.m_downwardDiffuse = t;
		NetProperties.EnsureTextureSettings(Singleton<NetManager>.get_instance().m_properties.m_downwardDiffuse);
		Singleton<NetManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void UnBindActions()
	{
		this.m_ResetButton = base.Find<UIButton>("Reset");
		this.m_ResetButton.remove_eventClick(new MouseEventHandler(this.ResetStructures));
		this.m_UpwardRoadDiffuse = base.Find("UpwardRoadDiffuse").Find<UITextureSprite>("Texture");
		this.m_UpwardRoadDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_UpwardRoadDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_UpwardRoadDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_UpwardRoadDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignUpwardRoadDiffuse));
		this.m_DownwardRoadDiffuse = base.Find("DownwardRoadDiffuse").Find<UITextureSprite>("Texture");
		this.m_DownwardRoadDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DownwardRoadDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DownwardRoadDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DownwardRoadDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDownwardRoadDiffuse));
		this.m_BuildingFloorDiffuse = base.Find("BuildingFloorDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingFloorDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingFloorDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingFloorDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingFloorDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignFloorDiffuse));
		this.m_BuildingBaseDiffuse = base.Find("BuildingBaseDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingBaseDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBaseDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBaseDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBaseDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBaseDiffuse));
		this.m_BuildingBaseNormal = base.Find("BuildingBaseNormal").Find<UITextureSprite>("Texture");
		this.m_BuildingBaseNormal.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBaseNormal.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBaseNormal.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBaseNormal.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBaseNormal));
		this.m_BuildingBurntDiffuse = base.Find("BuildingBurntDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingBurntDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBurntDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBurntDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBurntDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBurntDiffuse));
		this.m_BuildingAbandonedDiffuse = base.Find("BuildingAbandonedDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingAbandonedDiffuse.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingAbandonedDiffuse.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingAbandonedDiffuse.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingAbandonedDiffuse.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingAbandonedDiffuse));
		this.m_LightColorPalette = base.Find("LightColorPalette").Find<UITextureSprite>("Texture");
		this.m_LightColorPalette.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_LightColorPalette.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_LightColorPalette.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_LightColorPalette.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignLightColorPalette));
	}

	private void BindActions()
	{
		this.m_ResetButton = base.Find<UIButton>("Reset");
		this.m_ResetButton.add_eventClick(new MouseEventHandler(this.ResetStructures));
		this.m_UpwardRoadDiffuse = base.Find("UpwardRoadDiffuse").Find<UITextureSprite>("Texture");
		this.m_UpwardRoadDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_UpwardRoadDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_UpwardRoadDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_UpwardRoadDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignUpwardRoadDiffuse));
		this.m_DownwardRoadDiffuse = base.Find("DownwardRoadDiffuse").Find<UITextureSprite>("Texture");
		this.m_DownwardRoadDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_DownwardRoadDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_DownwardRoadDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_DownwardRoadDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignDownwardRoadDiffuse));
		this.m_BuildingFloorDiffuse = base.Find("BuildingFloorDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingFloorDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingFloorDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingFloorDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingFloorDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignFloorDiffuse));
		this.m_BuildingBaseDiffuse = base.Find("BuildingBaseDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingBaseDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBaseDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBaseDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBaseDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBaseDiffuse));
		this.m_BuildingBaseNormal = base.Find("BuildingBaseNormal").Find<UITextureSprite>("Texture");
		this.m_BuildingBaseNormal.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBaseNormal.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBaseNormal.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBaseNormal.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBaseNormal));
		this.m_BuildingBurntDiffuse = base.Find("BuildingBurntDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingBurntDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingBurntDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingBurntDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingBurntDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingBurntDiffuse));
		this.m_BuildingAbandonedDiffuse = base.Find("BuildingAbandonedDiffuse").Find<UITextureSprite>("Texture");
		this.m_BuildingAbandonedDiffuse.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_BuildingAbandonedDiffuse.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_BuildingAbandonedDiffuse.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_BuildingAbandonedDiffuse.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignBuildingAbandonedDiffuse));
		this.m_LightColorPalette = base.Find("LightColorPalette").Find<UITextureSprite>("Texture");
		this.m_LightColorPalette.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_LightColorPalette.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_LightColorPalette.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_LightColorPalette.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignLightColorPalette));
	}

	private void ToggleTextureEvents(UITextureSprite s, bool value)
	{
		if (value)
		{
			s.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(null);
		}
		else
		{
			s.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(Locale.Get("LOADSTATUS", "Compressing"));
		}
	}

	private void CompressThreaded(UITextureSprite s, Image im)
	{
		try
		{
			Task task = ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				im.Compress(true, true);
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					s.set_texture(im.CreateTexture());
					this.ToggleTextureEvents(s, true);
				}).Wait();
			});
			if (LoadSaveStatus.activeTask != null)
			{
				AsyncTaskWrapper asyncTaskWrapper = LoadSaveStatus.activeTask as AsyncTaskWrapper;
				if (asyncTaskWrapper != null)
				{
					asyncTaskWrapper.AddTask(task);
				}
			}
			else
			{
				AsyncTaskWrapper activeTask = new AsyncTaskWrapper("Compressing", new Task[]
				{
					task
				});
				LoadSaveStatus.activeTask = activeTask;
			}
		}
		catch (Exception ex)
		{
			this.ToggleTextureEvents(s, true);
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				Environment.StackTrace
			}));
		}
	}

	private void Awake()
	{
	}

	private void Start()
	{
		base.get_component().Hide();
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	public Color32 m_NormalColor;

	public Color32 m_HoverColor;

	private ValueStorage m_Defaults;

	private UITextureSprite m_UpwardRoadDiffuse;

	private UITextureSprite m_DownwardRoadDiffuse;

	private UITextureSprite m_BuildingFloorDiffuse;

	private UITextureSprite m_BuildingBaseDiffuse;

	private UITextureSprite m_BuildingBaseNormal;

	private UITextureSprite m_BuildingBurntDiffuse;

	private UITextureSprite m_BuildingAbandonedDiffuse;

	private UITextureSprite m_LightColorPalette;

	private UIButton m_ResetButton;
}
