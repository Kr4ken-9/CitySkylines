﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Importers;
using ColossalFramework.Threading;
using ColossalFramework.UI;
using UnityEngine;

public class WaterPropertiesPanel : UICustomControl
{
	private void OnEnable()
	{
		this.m_Defaults = new ValueStorage();
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.UnBindActions();
		this.m_WaterFoam.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_waterFoam);
		this.m_WaterNormal.set_texture(Singleton<TerrainManager>.get_instance().m_properties.m_waterNormal);
		this.m_WaterColorClean.set_selectedColor(Singleton<TerrainManager>.get_instance().m_properties.m_waterColorClean);
		this.m_WaterColorDirty.set_selectedColor(Singleton<TerrainManager>.get_instance().m_properties.m_waterColorDirty);
		this.m_WaterColorUnder.set_selectedColor(Singleton<TerrainManager>.get_instance().m_properties.m_waterColorUnder);
		this.m_Defaults.Store<Texture>("foamTex", Singleton<TerrainManager>.get_instance().m_properties.m_waterFoam);
		this.m_Defaults.Store<Texture>("waterNormalTex", Singleton<TerrainManager>.get_instance().m_properties.m_waterNormal);
		this.m_Defaults.Store<Color>("cleanColor", Singleton<TerrainManager>.get_instance().m_properties.m_waterColorClean);
		this.m_Defaults.Store<Color>("dirtyColor", Singleton<TerrainManager>.get_instance().m_properties.m_waterColorDirty);
		this.m_Defaults.Store<Color>("underColor", Singleton<TerrainManager>.get_instance().m_properties.m_waterColorUnder);
		this.BindActions();
	}

	public void ResetAll()
	{
		this.ResetWater(null, null);
	}

	public void ResetWater(UIComponent c, UIMouseEventParameter p)
	{
		this.m_WaterFoam.set_texture(this.m_Defaults.Get<Texture>("foamTex"));
		this.m_WaterNormal.set_texture(this.m_Defaults.Get<Texture>("waterNormalTex"));
		this.m_WaterColorClean.set_selectedColor(this.m_Defaults.Get<Color>("cleanColor"));
		this.m_WaterColorDirty.set_selectedColor(this.m_Defaults.Get<Color>("dirtyColor"));
		this.m_WaterColorUnder.set_selectedColor(this.m_Defaults.Get<Color>("underColor"));
	}

	private void OnAssignWaterFoam(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_waterFoam = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_waterFoam);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnAssignWaterNormal(UIComponent c, Texture t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_waterNormal = (Texture2D)t;
		TerrainProperties.EnsureTextureSettings(Singleton<TerrainManager>.get_instance().m_properties.m_waterNormal);
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnWaterColorCleanChanged(UIComponent c, Color t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_waterColorClean = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnWaterColorDirtyChanged(UIComponent c, Color t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_waterColorDirty = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void OnWaterColorUnderChanged(UIComponent c, Color t)
	{
		Singleton<TerrainManager>.get_instance().m_properties.m_waterColorUnder = t;
		Singleton<TerrainManager>.get_instance().m_properties.InitializeShaderProperties();
	}

	private void UnBindActions()
	{
		this.m_ResetButton = base.Find<UIButton>("Reset");
		this.m_ResetButton.remove_eventClick(new MouseEventHandler(this.ResetWater));
		this.m_WaterFoam = base.Find("WaterFoam").Find<UITextureSprite>("Texture");
		this.m_WaterFoam.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_WaterFoam.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_WaterFoam.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_WaterFoam.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignWaterFoam));
		this.m_WaterNormal = base.Find("WaterNormal").Find<UITextureSprite>("Texture");
		this.m_WaterNormal.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_WaterNormal.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_WaterNormal.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_WaterNormal.remove_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignWaterNormal));
		this.m_WaterColorClean = base.Find("WaterColorClean").Find<UIColorField>("Value");
		this.m_WaterColorClean.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorCleanChanged));
		this.m_WaterColorDirty = base.Find("WaterColorDirty").Find<UIColorField>("Value");
		this.m_WaterColorDirty.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorDirtyChanged));
		this.m_WaterColorUnder = base.Find("WaterColorUnder").Find<UIColorField>("Value");
		this.m_WaterColorUnder.remove_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorUnderChanged));
	}

	private void BindActions()
	{
		this.m_ResetButton = base.Find<UIButton>("Reset");
		this.m_ResetButton.add_eventClick(new MouseEventHandler(this.ResetWater));
		this.m_WaterFoam = base.Find("WaterFoam").Find<UITextureSprite>("Texture");
		this.m_WaterFoam.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_WaterFoam.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_WaterFoam.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_WaterFoam.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignWaterFoam));
		this.m_WaterNormal = base.Find("WaterNormal").Find<UITextureSprite>("Texture");
		this.m_WaterNormal.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
		this.m_WaterNormal.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
		this.m_WaterNormal.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
		this.m_WaterNormal.add_eventTextureChanged(new PropertyChangedEventHandler<Texture>(this.OnAssignWaterNormal));
		this.m_WaterColorClean = base.Find("WaterColorClean").Find<UIColorField>("Value");
		this.m_WaterColorClean.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorCleanChanged));
		this.m_WaterColorDirty = base.Find("WaterColorDirty").Find<UIColorField>("Value");
		this.m_WaterColorDirty.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorDirtyChanged));
		this.m_WaterColorUnder = base.Find("WaterColorUnder").Find<UIColorField>("Value");
		this.m_WaterColorUnder.add_eventSelectedColorChanged(new PropertyChangedEventHandler<Color>(this.OnWaterColorUnderChanged));
	}

	private void ToggleTextureEvents(UITextureSprite s, bool value)
	{
		if (value)
		{
			s.add_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.add_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.add_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(null);
		}
		else
		{
			s.remove_eventMouseEnter(new MouseEventHandler(this.OnHoverTexture));
			s.remove_eventMouseLeave(new MouseEventHandler(this.OnUnhoverTexture));
			s.remove_eventClick(new MouseEventHandler(this.OnTextureClicked));
			s.set_tooltip(Locale.Get("LOADSTATUS", "Compressing"));
		}
	}

	private void CompressThreaded(UITextureSprite s, Image im, bool linear)
	{
		try
		{
			Task task = ThreadHelper.get_taskDistributor().Dispatch(delegate
			{
				if (linear)
				{
					Color32[] colors = im.GetColors32();
					for (int i = 0; i < colors.Length; i++)
					{
						colors[i].a = colors[i].r;
						colors[i].r = colors[i].g;
						colors[i].b = colors[i].g;
					}
					im = new Image(im.get_width(), im.get_height(), im.get_format(), colors);
				}
				using (AutoProfile.Start("Compressing Texture CompressThreaded"))
				{
					im.Compress(true, true);
				}
				ThreadHelper.get_dispatcher().Dispatch(delegate
				{
					s.set_texture(im.CreateTexture(linear));
					this.ToggleTextureEvents(s, true);
				}).Wait();
			});
			if (LoadSaveStatus.activeTask != null)
			{
				AsyncTaskWrapper asyncTaskWrapper = LoadSaveStatus.activeTask as AsyncTaskWrapper;
				if (asyncTaskWrapper != null)
				{
					asyncTaskWrapper.AddTask(task);
				}
			}
			else
			{
				AsyncTaskWrapper activeTask = new AsyncTaskWrapper("Compressing", new Task[]
				{
					task
				});
				LoadSaveStatus.activeTask = activeTask;
			}
		}
		catch (Exception ex)
		{
			this.ToggleTextureEvents(s, true);
			Debug.LogError(string.Concat(new object[]
			{
				ex.GetType(),
				" ",
				ex.Message,
				Environment.StackTrace
			}));
		}
	}

	private void Awake()
	{
	}

	private void Start()
	{
		base.get_component().Hide();
	}

	public void Show()
	{
		if (!base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void Hide()
	{
		if (base.get_component().get_isVisible())
		{
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
			}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
			{
				base.get_component().Hide();
			});
		}
	}

	private void OnHoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_HoverColor);
	}

	private void OnUnhoverTexture(UIComponent c, UIMouseEventParameter p)
	{
		p.get_source().get_parent().set_color(this.m_NormalColor);
	}

	private void OnTextureClicked(UIComponent c, UIMouseEventParameter p)
	{
		UITextureSprite s = p.get_source() as UITextureSprite;
		if (s != null)
		{
			TexturePicker tp = UIView.get_library().Get<TexturePicker>("TexturePicker");
			tp.ShowModal(s, TexturePicker.Type.ThemeTextures, delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.ToggleTextureEvents(s, false);
					this.CompressThreaded(s, tp.selectedImage, s == this.m_WaterNormal);
				}
			});
		}
	}

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	public Color32 m_NormalColor;

	public Color32 m_HoverColor;

	private ValueStorage m_Defaults;

	private UITextureSprite m_WaterFoam;

	private UITextureSprite m_WaterNormal;

	private UIColorField m_WaterColorClean;

	private UIColorField m_WaterColorDirty;

	private UIColorField m_WaterColorUnder;

	private UIButton m_ResetButton;
}
