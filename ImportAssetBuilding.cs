﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ImportAssetBuilding : ImportAssetLodded
{
	public ImportAssetBuilding(GameObject template, PreviewCamera camera, bool loadExistingDecoration) : base(template, camera)
	{
		this.m_LodTriangleTarget = 150;
		this.m_LoadExistingDecoration = loadExistingDecoration;
	}

	protected override void InitializeObject()
	{
		BuildingInfo component = this.m_Object.GetComponent<BuildingInfo>();
		BuildingInfo.MeshInfo[] subMeshes = component.m_subMeshes;
		component.m_subMeshes = new BuildingInfo.MeshInfo[0];
		component.InitializePrefab();
		component.m_subMeshes = subMeshes;
		component.CheckReferences();
		component.m_prefabInitialized = true;
		this.m_Object.set_layer(LayerMask.NameToLayer("Buildings"));
	}

	protected override void CreateInfo()
	{
		BuildingInfo component = this.m_TemplateObject.GetComponent<BuildingInfo>();
		BuildingInfo buildingInfo = ImportAsset.CopyComponent<BuildingInfo>(component, this.m_Object);
		buildingInfo.m_generatedInfo = ScriptableObject.CreateInstance<BuildingInfoGen>();
		buildingInfo.m_generatedInfo.set_name(this.m_Object.get_name() + " (GeneratedInfo)");
		buildingInfo.m_InfoTooltipThumbnail = this.m_TemplateObject.get_name();
		buildingInfo.m_lodObject = this.m_LODObject;
		buildingInfo.CalculateGeneratedInfo();
		ImportAsset.CopyComponent<BuildingAI>(component.m_buildingAI, this.m_Object);
		buildingInfo.TempInitializePrefab();
		int num;
		int num2;
		buildingInfo.GetWidthRange(out num, out num2);
		int num3 = Mathf.CeilToInt((buildingInfo.m_generatedInfo.m_size.x - 0.1f) / 8f);
		int num4 = Mathf.CeilToInt((buildingInfo.m_generatedInfo.m_size.z - 0.1f) / 8f);
		if (!this.m_LoadExistingDecoration)
		{
			this.ClearDecorations(buildingInfo);
			buildingInfo.m_cellWidth = Mathf.Clamp(num3, num, num2);
			buildingInfo.GetLengthRange(out num, out num2);
			buildingInfo.m_cellLength = Mathf.Clamp(num4, num, num2);
		}
		else
		{
			buildingInfo.m_cellWidth = Mathf.Clamp(Math.Max(component.m_cellWidth, num3), num, num2);
			buildingInfo.GetLengthRange(out num, out num2);
			buildingInfo.m_cellLength = Mathf.Clamp(Math.Max(component.m_cellLength, num4), num, num2);
		}
	}

	protected void ClearDecorations(BuildingInfo instance)
	{
		List<BuildingInfo.PathInfo> list = new List<BuildingInfo.PathInfo>();
		if (instance.m_paths != null)
		{
			for (int i = 0; i < instance.m_paths.Length; i++)
			{
				if (instance.m_paths[i].m_netInfo != null && instance.m_paths[i].m_nodes != null && instance.m_paths[i].m_nodes.Length != 0 && (instance.m_paths[i].m_nodes.Length == 1 || (instance.m_paths[i].m_netInfo.m_availableIn & ItemClass.Availability.AssetEditor) == ItemClass.Availability.None))
				{
					list.Add(instance.m_paths[i]);
				}
			}
		}
		instance.m_paths = list.ToArray();
		instance.m_props = new BuildingInfo.Prop[0];
		instance.m_cellSurfaces = new TerrainModify.Surface[0];
		instance.m_pavementAreas = new Vector2[0];
		instance.m_subMeshes = new BuildingInfo.MeshInfo[0];
		instance.m_subBuildings = new BuildingInfo.SubInfo[0];
	}

	protected bool m_LoadExistingDecoration;
}
