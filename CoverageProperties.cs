﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class CoverageProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		CoverageProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new CoverageProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("CoverageProperties");
			Singleton<CoverageManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Color m_goodCoverage;

	public Color m_badCoverage;
}
