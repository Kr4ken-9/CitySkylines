﻿using System;

public sealed class DistrictGroupPanel : GeneratedGroupPanel
{
	public override string serviceName
	{
		get
		{
			return "District";
		}
	}

	protected override bool CustomRefreshPanel()
	{
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC);
		bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC);
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("DistrictSpecializationPaint", this.GetCategoryOrder("DistrictSpecializationPaint"), "District"), "DISTRICT_CATEGORY");
		base.CreateGroupItem(new DistrictGroupPanel.PTGroupInfo("DistrictSpecializationIndustrial", this.GetCategoryOrder("DistrictSpecializationIndustrial"), UnlockManager.Feature.IndustrySpecializations, "District"), "DISTRICT_CATEGORY");
		if (flag2)
		{
			base.CreateGroupItem(new DistrictGroupPanel.PTGroupInfo("DistrictSpecializationCommercial", this.GetCategoryOrder("DistrictSpecializationCommercial"), UnlockManager.Feature.CommercialSpecializationGC, "District"), "DISTRICT_CATEGORY");
		}
		else if (flag)
		{
			base.CreateGroupItem(new DistrictGroupPanel.PTGroupInfo("DistrictSpecializationCommercial", this.GetCategoryOrder("DistrictSpecializationCommercial"), UnlockManager.Feature.CommercialSpecialization, "District"), "DISTRICT_CATEGORY");
		}
		if (flag2)
		{
			base.CreateGroupItem(new DistrictGroupPanel.PTGroupInfo("DistrictSpecializationOffice", this.GetCategoryOrder("DistrictSpecializationOffice"), UnlockManager.Feature.OfficeSpecializations, "District"), "DISTRICT_CATEGORY");
			base.CreateGroupItem(new DistrictGroupPanel.PTGroupInfo("DistrictSpecializationResidential", this.GetCategoryOrder("DistrictSpecializationResidential"), UnlockManager.Feature.ResidentialSpecializations, "District"), "DISTRICT_CATEGORY");
		}
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "DistrictSpecializationPaint")
			{
				return 0;
			}
			if (name == "DistrictSpecializationResidential")
			{
				return 1;
			}
			if (name == "DistrictSpecializationCommercial")
			{
				return 2;
			}
			if (name == "DistrictSpecializationIndustrial")
			{
				return 3;
			}
			if (name == "DistrictSpecializationOffice")
			{
				return 4;
			}
		}
		return 2147483647;
	}

	public class PTGroupInfo : GeneratedGroupPanel.GroupInfo
	{
		public PTGroupInfo(string name, int order, UnlockManager.Feature feature, string subPanelType) : base(name, order, subPanelType)
		{
			this.m_Feature = feature;
		}

		public override string unlockText
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return null;
				}
				return GeneratedGroupPanel.GetUnlockText(this.m_Feature);
			}
		}

		public override bool isUnlocked
		{
			get
			{
				if (this.m_Feature == UnlockManager.Feature.None)
				{
					return base.isUnlocked;
				}
				return ToolsModifierControl.IsUnlocked(this.m_Feature);
			}
		}

		private UnlockManager.Feature m_Feature;
	}
}
