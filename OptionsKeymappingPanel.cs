﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class OptionsKeymappingPanel : UICustomControl
{
	private void Awake()
	{
		this.m_TabContainer = base.Find<UITabContainer>("KeyMappingContainer");
		this.m_DropDown = base.Find<UIDropDown>("DropDown");
		this.m_DropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnDropDownIndexChanged));
		this.m_SharedInputContainer = base.Find<UIScrollablePanel>("SharedInputContainer");
		this.m_GameInputContainer = base.Find<UIScrollablePanel>("GameInputContainer");
		this.m_MapEditorInputContainer = base.Find<UIScrollablePanel>("MapEditorInputContainer");
		this.m_DecorationInputContainer = base.Find<UIScrollablePanel>("DecorationInputContainer");
		this.m_ThemeEditorInputContainer = base.Find<UIScrollablePanel>("ThemeEditorInputContainer");
		this.m_ScenarioEditorInputContainer = base.Find<UIScrollablePanel>("ScenarioEditorInputContainer");
		this.CreateBindableInputs(this.m_SharedInputContainer, string.Empty);
		this.CreateBindableInputs(this.m_GameInputContainer, "Game");
		this.CreateBindableInputs(this.m_MapEditorInputContainer, "MapEditor");
		this.CreateBindableInputs(this.m_DecorationInputContainer, "Decoration");
		this.CreateBindableInputs(this.m_ThemeEditorInputContainer, "ThemeEditor");
		this.CreateBindableInputs(this.m_ScenarioEditorInputContainer, "ScenarioEditor");
	}

	private void Start()
	{
		this.RefreshDropdown();
	}

	private void RefreshDropdown()
	{
		string[] items = new string[]
		{
			Locale.Get("OPTIONS_KM_SHARED"),
			Locale.Get("OPTIONS_KM_GAME"),
			Locale.Get("OPTIONS_KM_MAPEDITOR"),
			Locale.Get("OPTIONS_KM_DECORATION"),
			Locale.Get("OPTIONS_KM_THEMEEDITOR"),
			Locale.Get("OPTIONS_KM_SCENARIOEDITOR")
		};
		this.m_DropDown.set_items(items);
	}

	private void OnDropDownIndexChanged(UIComponent component, int index)
	{
		this.m_TabContainer.set_selectedIndex(index);
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.RefreshBindableInputs(this.m_SharedInputContainer);
		this.RefreshBindableInputs(this.m_GameInputContainer);
		this.RefreshBindableInputs(this.m_MapEditorInputContainer);
		this.RefreshBindableInputs(this.m_DecorationInputContainer);
		this.RefreshBindableInputs(this.m_ThemeEditorInputContainer);
		this.RefreshBindableInputs(this.m_ScenarioEditorInputContainer);
		this.RefreshDropdown();
	}

	private bool IsModifierKey(KeyCode code)
	{
		return code == 306 || code == 305 || code == 304 || code == 303 || code == 308 || code == 307;
	}

	private bool IsControlDown()
	{
		return Input.GetKey(306) || Input.GetKey(305);
	}

	private bool IsShiftDown()
	{
		return Input.GetKey(304) || Input.GetKey(303);
	}

	private bool IsAltDown()
	{
		return Input.GetKey(308) || Input.GetKey(307);
	}

	private bool IsUnbindableMouseButton(UIMouseButton code)
	{
		return code == 1 || code == 2;
	}

	private bool IsAlreadyBound(SavedInputKey target, InputKey inputKey, string category, out List<SavedInputKey> currentAssigned)
	{
		currentAssigned = new List<SavedInputKey>();
		if (inputKey == SavedInputKey.Empty)
		{
			return false;
		}
		FieldInfo[] fields = typeof(Settings).GetFields(BindingFlags.Static | BindingFlags.Public);
		FieldInfo[] array = fields;
		for (int i = 0; i < array.Length; i++)
		{
			FieldInfo fieldInfo = array[i];
			RebindableKeyAttribute[] array2 = fieldInfo.GetCustomAttributes(typeof(RebindableKeyAttribute), false) as RebindableKeyAttribute[];
			if (array2.Length > 0 && (string.IsNullOrEmpty(category) || category == array2[0].get_category() || string.IsNullOrEmpty(array2[0].get_category())))
			{
				string text = fieldInfo.GetValue(null) as string;
				SavedInputKey savedInputKey = new SavedInputKey(text, Settings.gameSettingsFile, this.GetDefaultEntry(text), true);
				if (!(target == savedInputKey))
				{
					if (inputKey == savedInputKey.get_value())
					{
						currentAssigned.Add(savedInputKey);
					}
				}
			}
		}
		return currentAssigned.Count > 0;
	}

	private KeyCode ButtonToKeycode(UIMouseButton button)
	{
		if (button == 1)
		{
			return 323;
		}
		if (button == 2)
		{
			return 324;
		}
		if (button == 4)
		{
			return 325;
		}
		if (button == 8)
		{
			return 326;
		}
		if (button == 16)
		{
			return 327;
		}
		if (button == 32)
		{
			return 328;
		}
		if (button == 64)
		{
			return 329;
		}
		return 0;
	}

	private void OnBindingKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (this.m_EditingBinding != null && !this.IsModifierKey(p.get_keycode()))
		{
			p.Use();
			UIView.PopModal();
			KeyCode keycode = p.get_keycode();
			InputKey inputKey = (p.get_keycode() == 27) ? this.m_EditingBinding.get_value() : SavedInputKey.Encode(keycode, p.get_control(), p.get_shift(), p.get_alt());
			if (p.get_keycode() == 8)
			{
				inputKey = SavedInputKey.Empty;
			}
			List<SavedInputKey> currentAssigned;
			if (!this.IsAlreadyBound(this.m_EditingBinding, inputKey, this.m_EditingBindingCategory, out currentAssigned))
			{
				this.m_EditingBinding.set_value(inputKey);
				UITextComponent uITextComponent = p.get_source() as UITextComponent;
				uITextComponent.set_text(this.m_EditingBinding.ToLocalizedString("KEYNAME"));
				this.m_EditingBinding = null;
				this.m_EditingBindingCategory = string.Empty;
			}
			else
			{
				string text = (currentAssigned.Count <= 1) ? Locale.Get("KEYMAPPING", currentAssigned[0].get_name()) : Locale.Get("KEYMAPPING_MULTIPLE");
				string message = StringUtils.SafeFormat(Locale.Get("CONFIRM_REBINDKEY", "Message"), new object[]
				{
					SavedInputKey.ToLocalizedString("KEYNAME", inputKey),
					text
				});
				ConfirmPanel.ShowModal(Locale.Get("CONFIRM_REBINDKEY", "Title"), message, delegate(UIComponent c, int ret)
				{
					if (ret == 1)
					{
						this.m_EditingBinding.set_value(inputKey);
						for (int i = 0; i < currentAssigned.Count; i++)
						{
							currentAssigned[i].set_value(SavedInputKey.Empty);
						}
						this.RefreshKeyMapping();
					}
					UITextComponent uITextComponent2 = p.get_source() as UITextComponent;
					uITextComponent2.set_text(this.m_EditingBinding.ToLocalizedString("KEYNAME"));
					this.m_EditingBinding = null;
					this.m_EditingBindingCategory = string.Empty;
				});
			}
		}
	}

	private void OnBindingMouseDown(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.m_EditingBinding == null)
		{
			p.Use();
			this.m_EditingBinding = (SavedInputKey)p.get_source().get_objectUserData();
			this.m_EditingBindingCategory = p.get_source().get_stringUserData();
			UIButton uIButton = p.get_source() as UIButton;
			uIButton.set_buttonsMask(127);
			uIButton.set_text(Locale.Get("KEYMAPPING_PRESSANYKEY"));
			p.get_source().Focus();
			UIView.PushModal(p.get_source());
		}
		else if (!this.IsUnbindableMouseButton(p.get_buttons()))
		{
			p.Use();
			UIView.PopModal();
			InputKey inputKey = SavedInputKey.Encode(this.ButtonToKeycode(p.get_buttons()), this.IsControlDown(), this.IsShiftDown(), this.IsAltDown());
			List<SavedInputKey> currentAssigned;
			if (!this.IsAlreadyBound(this.m_EditingBinding, inputKey, this.m_EditingBindingCategory, out currentAssigned))
			{
				this.m_EditingBinding.set_value(inputKey);
				UIButton uIButton2 = p.get_source() as UIButton;
				uIButton2.set_text(this.m_EditingBinding.ToLocalizedString("KEYNAME"));
				uIButton2.set_buttonsMask(1);
				this.m_EditingBinding = null;
				this.m_EditingBindingCategory = string.Empty;
			}
			else
			{
				string text = (currentAssigned.Count <= 1) ? Locale.Get("KEYMAPPING", currentAssigned[0].get_name()) : Locale.Get("KEYMAPPING_MULTIPLE");
				string message = StringUtils.SafeFormat(Locale.Get("CONFIRM_REBINDKEY", "Message"), new object[]
				{
					SavedInputKey.ToLocalizedString("KEYNAME", inputKey),
					text
				});
				ConfirmPanel.ShowModal(Locale.Get("CONFIRM_REBINDKEY", "Title"), message, delegate(UIComponent c, int ret)
				{
					if (ret == 1)
					{
						this.m_EditingBinding.set_value(inputKey);
						for (int i = 0; i < currentAssigned.Count; i++)
						{
							currentAssigned[i].set_value(SavedInputKey.Empty);
						}
						this.RefreshKeyMapping();
					}
					UIButton uIButton3 = p.get_source() as UIButton;
					uIButton3.set_text(this.m_EditingBinding.ToLocalizedString("KEYNAME"));
					uIButton3.set_buttonsMask(1);
					this.m_EditingBinding = null;
					this.m_EditingBindingCategory = string.Empty;
				});
			}
		}
	}

	private void CreateBindableInputs(UIScrollablePanel panel, string category)
	{
		int num = 0;
		FieldInfo[] fields = typeof(Settings).GetFields(BindingFlags.Static | BindingFlags.Public);
		FieldInfo[] array = fields;
		for (int i = 0; i < array.Length; i++)
		{
			FieldInfo fieldInfo = array[i];
			RebindableKeyAttribute[] array2 = fieldInfo.GetCustomAttributes(typeof(RebindableKeyAttribute), false) as RebindableKeyAttribute[];
			if (array2.Length > 0 && category == array2[0].get_category() && SteamHelper.IsDLCOwned((SteamHelper.DLC)array2[0].get_appID()))
			{
				num++;
				UIPanel uIPanel = panel.AttachUIComponent(UITemplateManager.GetAsGameObject(OptionsKeymappingPanel.kKeyBindingTemplate)) as UIPanel;
				if (num % 2 == 0)
				{
					uIPanel.set_backgroundSprite(string.Empty);
				}
				UILabel uILabel = uIPanel.Find<UILabel>("Name");
				UIButton uIButton = uIPanel.Find<UIButton>("Binding");
				uIButton.add_eventKeyDown(new KeyPressHandler(this.OnBindingKeyDown));
				uIButton.add_eventMouseDown(new MouseEventHandler(this.OnBindingMouseDown));
				string text = fieldInfo.GetValue(null) as string;
				uILabel.set_stringUserData(text);
				uILabel.set_text(Locale.Get("KEYMAPPING", text));
				SavedInputKey savedInputKey = new SavedInputKey(text, Settings.gameSettingsFile, this.GetDefaultEntry(text), true);
				uIButton.set_text(savedInputKey.ToLocalizedString("KEYNAME"));
				uIButton.set_objectUserData(savedInputKey);
				uIButton.set_stringUserData(category);
			}
		}
	}

	private void RefreshBindableInputs(UIScrollablePanel panel)
	{
		foreach (UIComponent current in panel.get_components())
		{
			UITextComponent uITextComponent = current.Find<UITextComponent>("Binding");
			if (uITextComponent != null)
			{
				SavedInputKey savedInputKey = uITextComponent.get_objectUserData() as SavedInputKey;
				if (savedInputKey != null)
				{
					uITextComponent.set_text(savedInputKey.ToLocalizedString("KEYNAME"));
				}
			}
			UILabel uILabel = current.Find<UILabel>("Name");
			if (uILabel != null)
			{
				uILabel.set_text(Locale.Get("KEYMAPPING", uILabel.get_stringUserData()));
			}
		}
	}

	internal InputKey GetDefaultEntry(string entryName)
	{
		FieldInfo field = typeof(DefaultSettings).GetField(entryName, BindingFlags.Static | BindingFlags.Public);
		if (field == null)
		{
			return 0;
		}
		object value = field.GetValue(null);
		if (value is InputKey)
		{
			return (InputKey)value;
		}
		return 0;
	}

	internal void RefreshKeyMapping()
	{
		this.RefreshKeyMapping(this.m_SharedInputContainer);
		this.RefreshKeyMapping(this.m_GameInputContainer);
		this.RefreshKeyMapping(this.m_MapEditorInputContainer);
		this.RefreshKeyMapping(this.m_DecorationInputContainer);
		this.RefreshKeyMapping(this.m_ThemeEditorInputContainer);
		this.RefreshKeyMapping(this.m_ScenarioEditorInputContainer);
	}

	private void RefreshKeyMapping(UIScrollablePanel panel)
	{
		for (int i = 0; i < panel.get_childCount(); i++)
		{
			UITextComponent uITextComponent = panel.get_components()[i].Find<UITextComponent>("Binding");
			SavedInputKey savedInputKey = (SavedInputKey)uITextComponent.get_objectUserData();
			if (this.m_EditingBinding != savedInputKey)
			{
				uITextComponent.set_text(savedInputKey.ToLocalizedString("KEYNAME"));
			}
		}
	}

	private static readonly string kKeyBindingTemplate = "KeyBindingTemplate";

	private SavedInputKey m_EditingBinding;

	private string m_EditingBindingCategory;

	private UIScrollablePanel m_SharedInputContainer;

	private UIScrollablePanel m_GameInputContainer;

	private UIScrollablePanel m_MapEditorInputContainer;

	private UIScrollablePanel m_DecorationInputContainer;

	private UIScrollablePanel m_ThemeEditorInputContainer;

	private UIScrollablePanel m_ScenarioEditorInputContainer;

	private UIDropDown m_DropDown;

	private UITabContainer m_TabContainer;
}
