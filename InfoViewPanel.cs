﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public abstract class InfoViewPanel : ToolsModifierControl
{
	protected virtual void Start()
	{
		this.m_CloseButton = base.Find("Caption").Find("Close");
		this.m_Tabstrip = base.Find<UITabstrip>("Tabstrip");
		base.get_transform().set_position(ToolsModifierControl.infoViewsPanel.get_transform().get_position());
		this.m_DesiredPosition = base.get_component().get_relativePosition();
		this.m_ViewsPanelOffset = new Vector2(ToolsModifierControl.infoViewsPanel.get_component().get_size().x, 0f);
		ToolsModifierControl.infoViewsPanel.get_component().add_eventVisibilityChanged(delegate(UIComponent c, bool v)
		{
			if (v)
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition);
			}
			else
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition - this.m_ViewsPanelOffset);
			}
		});
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	protected virtual void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
	}

	protected void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (ToolsModifierControl.infoViewsPanel.isVisible)
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition);
			}
			else
			{
				base.get_component().set_relativePosition(this.m_DesiredPosition - this.m_ViewsPanelOffset);
			}
		}
	}

	public void OnCloseButton()
	{
		UIView.get_library().Hide(base.GetType().Name);
	}

	protected int GetPercentage(int capacity, int need)
	{
		return this.GetPercentage(capacity, need, 45, 55);
	}

	protected int GetPercentage(int capacity, int need, int needMin, int needMax)
	{
		if (need != 0)
		{
			float num = (float)capacity / (float)need;
			return (int)(num * (float)((needMin + needMax) / 2));
		}
		if (capacity == 0)
		{
			return 0;
		}
		return 100;
	}

	protected void Update()
	{
		if (base.get_component().get_isVisible() && base.get_component().get_isEnabled())
		{
			bool flag = ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<DefaultTool>();
			UIComponent arg_4A_0 = this.m_CloseButton;
			bool flag2 = flag;
			this.m_CloseButton.set_isEnabled(flag2);
			arg_4A_0.set_isVisible(flag2);
			this.UpdatePanel();
		}
	}

	protected virtual void UpdatePanel()
	{
	}

	private UIComponent m_CloseButton;

	protected UITabstrip m_Tabstrip;

	private Vector2 m_DesiredPosition;

	private Vector2 m_ViewsPanelOffset;
}
