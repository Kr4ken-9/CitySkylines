﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class ConcertAI : EventAI
{
	public override void InitializeAI()
	{
		base.InitializeAI();
		if (this.m_completePassMilestone != null)
		{
			this.m_completePassMilestone.SetPrefab(this.m_info);
		}
	}

	public override void ReleaseAI()
	{
		base.ReleaseAI();
	}

	public override string GetDebugString(ushort eventID, ref EventData data)
	{
		string text = base.GetDebugString(eventID, ref data);
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			int successProbability = this.GetSuccessProbability(eventID, ref data);
			text += StringUtils.SafeFormat("\nSuccess probability: {0}%", successProbability);
		}
		return text;
	}

	protected override string GetEventName(ushort eventID, ref EventData data)
	{
		string eventName = base.GetEventName(eventID, ref data);
		string bandName = this.GetBandName(eventID, ref data);
		return StringUtils.SafeFormat("{0}\n{1}", new object[]
		{
			eventName,
			bandName
		});
	}

	public override void GetAnimationState(ushort eventID, ref EventData data, ref float active, ref int state, ref bool playActiveState, ref Color color1, ref Color color2)
	{
		if (Singleton<InfoManager>.get_instance().CurrentMode == InfoManager.InfoMode.None)
		{
			color1 = data.m_color;
			color2 = color1;
		}
		if (active < 0.1f || (data.m_flags & EventData.Flags.Active) == EventData.Flags.None)
		{
			active = 0f;
			state = 0;
			playActiveState = false;
			return;
		}
		state = 1;
	}

	public override bool GetAudioState(ushort eventID, ref EventData data, ref float volume, ref float maxDistance, ref AudioInfo clip)
	{
		if (this.m_crowdAudio != null && Mathf.RoundToInt(Time.get_realtimeSinceStartup()) % 60 < 8)
		{
			clip = this.m_crowdAudio;
		}
		maxDistance = 300f;
		return (data.m_flags & EventData.Flags.Active) != EventData.Flags.None;
	}

	public override void CreateEvent(ushort eventID, ref EventData data)
	{
		base.CreateEvent(eventID, ref data);
		EventManager instance = Singleton<EventManager>.get_instance();
		if (data.m_nextBuildingEvent != 0)
		{
			data.m_totalReward = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_totalReward;
			data.m_totalTicket = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_totalTicket;
			data.m_successCount = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_successCount;
			data.m_failureCount = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_failureCount;
			data.m_securityBudget = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_securityBudget;
			data.m_color = instance.m_events.m_buffer[(int)data.m_nextBuildingEvent].m_color;
			if (data.m_color.a != 255)
			{
				data.m_color = this.m_defaultThemeColor;
				if (data.m_building != 0)
				{
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
				}
			}
		}
		else
		{
			data.m_color = this.m_defaultThemeColor;
			if (data.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
			}
		}
	}

	public override void ReleaseEvent(ushort eventID, ref EventData data)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
		}
		base.ReleaseEvent(eventID, ref data);
	}

	public override void SimulationStep(ushort eventID, ref EventData data)
	{
		base.SimulationStep(eventID, ref data);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		if (data.m_building != 0 && instance.m_buildings.m_buffer[(int)data.m_building].m_eventIndex == eventID)
		{
			DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
			byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)data.m_building].m_position);
			DistrictPolicies.Event eventPolicies = instance2.m_districts.m_buffer[(int)district].m_eventPolicies;
			District[] expr_8A_cp_0 = instance2.m_districts.m_buffer;
			byte expr_8A_cp_1 = district;
			expr_8A_cp_0[(int)expr_8A_cp_1].m_eventPoliciesEffect = (expr_8A_cp_0[(int)expr_8A_cp_1].m_eventPoliciesEffect | (eventPolicies & (DistrictPolicies.Event.BandAdvertisement | DistrictPolicies.Event.PremiumStudio)));
			if ((eventPolicies & DistrictPolicies.Event.BandAdvertisement) != DistrictPolicies.Event.None)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)data.m_building].Info;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 56250, info.m_class);
				EventManager instance3 = Singleton<EventManager>.get_instance();
				instance3.m_bandPopularityBonus = Mathf.Min(instance3.m_bandPopularityBonus + 79, 21503);
			}
			if ((eventPolicies & DistrictPolicies.Event.PremiumStudio) != DistrictPolicies.Event.None)
			{
				BuildingInfo info2 = instance.m_buildings.m_buffer[(int)data.m_building].Info;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 18750, info2.m_class);
			}
			int num = data.m_securityBudget * 100 + 8 >> 4;
			if (num > 0)
			{
				BuildingInfo info3 = instance.m_buildings.m_buffer[(int)data.m_building].Info;
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, num, info3.m_class);
			}
			if ((data.m_flags & EventData.Flags.Preparing) != EventData.Flags.None)
			{
				uint num2 = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
				if (Singleton<SimulationManager>.get_instance().m_currentFrameIndex + (num2 >> 1) >= data.m_startFrame)
				{
					GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
					if (properties != null)
					{
						Singleton<EventManager>.get_instance().m_bandPreparing.Activate(properties.m_bandPreparing, data.m_building);
					}
				}
			}
		}
		if (data.m_building != 0 && ConcertAI.GetPopularity(this.m_info) <= 35)
		{
			GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
			if (properties2 != null)
			{
				Singleton<EventManager>.get_instance().m_bandPopularity.Activate(properties2.m_bandPopularity, data.m_building);
			}
		}
	}

	protected override void BeginPreparing(ushort eventID, ref EventData data)
	{
		base.BeginPreparing(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
		ushort num = data.m_building;
		if (num != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			num = instance.m_buildings.m_buffer[(int)num].m_subBuilding;
			int num2 = 0;
			while (num != 0)
			{
				BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
				BuildingInfo buildingInfo = this.ModifySubBuildingInfo(info);
				if (info != buildingInfo)
				{
					instance.UpdateBuildingInfo(num, buildingInfo);
				}
				num = instance.m_buildings.m_buffer[(int)num].m_subBuilding;
				if (++num2 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		HashSet<ushort> hashSet;
		if (Singleton<EventManager>.get_instance().m_eventLocations.TryGetValue(eventID, out hashSet))
		{
			foreach (ushort current in hashSet)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				num = instance2.m_buildings.m_buffer[(int)current].m_subBuilding;
				int num3 = 0;
				while (num != 0)
				{
					BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)num].Info;
					BuildingInfo buildingInfo2 = this.ModifySubBuildingInfo(info2);
					if (info2 != buildingInfo2)
					{
						instance2.UpdateBuildingInfo(num, buildingInfo2);
					}
					num = instance2.m_buildings.m_buffer[(int)num].m_subBuilding;
					if (++num3 > 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		string bandName = this.GetBandName(eventID, ref data);
		Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_onPrepareMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID(data.m_building, CitizenUnit.Flags.Visit), bandName);
	}

	protected override void BeginEvent(ushort eventID, ref EventData data)
	{
		int successProbability = this.GetSuccessProbability(eventID, ref data);
		if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < successProbability)
		{
			data.m_flags |= EventData.Flags.Success;
		}
		else
		{
			data.m_flags |= EventData.Flags.Failure;
		}
		base.BeginEvent(eventID, ref data);
		if (data.m_building != 0)
		{
			Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
		}
		HashSet<ushort> hashSet;
		if (Singleton<EventManager>.get_instance().m_eventLocations.TryGetValue(eventID, out hashSet))
		{
			foreach (ushort current in hashSet)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(current);
			}
		}
	}

	private int GetSuccessProbability(ushort eventID, ref EventData data)
	{
		int num;
		int num2;
		int num3;
		base.CountVisitors(eventID, ref data, out num, out num2, out num3);
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(instance.m_buildings.m_buffer[(int)data.m_building].m_position);
		DistrictPolicies.Event eventPolicies = instance2.m_districts.m_buffer[(int)district].m_eventPolicies;
		int num4 = Singleton<EventManager>.get_instance().m_finalBandPopularityBonus * (100 + Singleton<EventManager>.get_instance().m_adCampaignModifierFinal) / 102400;
		int num5 = ((eventPolicies & DistrictPolicies.Event.PremiumStudio) == DistrictPolicies.Event.None) ? 0 : 10;
		int num6 = Mathf.Clamp((num * 100 + (num3 >> 1)) / Mathf.Max(1, num3), 0, 100);
		int num7 = num5 + Singleton<EventManager>.get_instance().m_concertSuccessModifierFinal;
		int num8 = Mathf.Clamp(50 + this.m_info.m_popularityOffset + num4, 0, 100);
		if (num8 <= 45)
		{
			num7 += num8 * 5 / 15 + 25;
		}
		else if (num6 <= 60)
		{
			num7 += num6 * 10 / 15 + 10;
		}
		else
		{
			num7 += num6 * 5 / 15 + 30;
		}
		if (num6 == 0)
		{
			num7 = 0;
		}
		else if (num6 <= 15)
		{
			num7 += num6 * 5 / 14 - 15;
		}
		else if (num6 <= 30)
		{
			num7 += num6 * 5 / 15 - 15;
		}
		else if (num6 <= 60)
		{
			num7 += num6 * 5 / 15 - 15;
		}
		else if (num6 <= 90)
		{
			num7 += num6 * 5 / 15 - 15;
		}
		else
		{
			num7 += num6 * 5 / 10 - 30;
		}
		return Mathf.Clamp(num7, 0, 100);
	}

	protected override void EndEvent(ushort eventID, ref EventData data)
	{
		base.EndEvent(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
		if (data.m_building != 0)
		{
			Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
		}
		HashSet<ushort> hashSet;
		if (Singleton<EventManager>.get_instance().m_eventLocations.TryGetValue(eventID, out hashSet))
		{
			foreach (ushort current in hashSet)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(current);
			}
		}
		if (data.m_ticketMoney != 0uL)
		{
			Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PublicIncome, (int)data.m_ticketMoney, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Level.Level1);
			data.m_totalTicket += data.m_ticketMoney;
		}
		int num = 0;
		int ticketPrice = this.GetTicketPrice(eventID, ref data);
		int num2 = Mathf.Clamp((ticketPrice * 100 + (this.m_ticketPrice >> 1)) / Mathf.Max(1, this.m_ticketPrice) - 100, -30, 30);
		if (num2 < -20)
		{
			num += num2 * 4 / 10 + 2;
		}
		else if (num2 < -10)
		{
			num += num2 * 3 / 10;
		}
		else if (num2 < 0)
		{
			num += num2 * 3 / 10;
		}
		else if (num2 < 10)
		{
			num += num2 * 2 / 10;
		}
		else if (num2 < 20)
		{
			num += num2 * 2 / 10;
		}
		else
		{
			num += num2 * 4 / 10 - 4;
		}
		if ((data.m_flags & EventData.Flags.Success) != EventData.Flags.None)
		{
			data.m_successCount = (ushort)Mathf.Min((int)(data.m_successCount + 1), 65535);
			num += Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3, 10);
			if (data.m_building != 0)
			{
				Vector3 position = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].m_position;
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Wellbeing, 0.2f, 3000f);
			}
		}
		else if ((data.m_flags & EventData.Flags.Failure) != EventData.Flags.None)
		{
			data.m_failureCount = (ushort)Mathf.Min((int)(data.m_failureCount + 1), 65535);
			num -= Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3, 10);
			if (data.m_building != 0)
			{
				Vector3 position2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_building].m_position;
				Singleton<NotificationManager>.get_instance().AddWaveEvent(position2, NotificationEvent.Type.Sad, ImmaterialResourceManager.Resource.Wellbeing, -0.2f, 3000f);
			}
		}
		int num3 = this.m_info.m_popularityOffset + 50;
		int num4 = Mathf.Clamp(num3 + num, 0, 100);
		data.m_popularityDelta = (short)(num4 - num3);
		this.m_info.m_popularityOffset = num4 - 50;
		if (this.m_completePassMilestone != null)
		{
			this.m_completePassMilestone.Unlock();
		}
	}

	protected override void EndDisorganizing(ushort eventID, ref EventData data)
	{
		base.EndDisorganizing(eventID, ref data);
		Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
	}

	protected override void Cancel(ushort eventID, ref EventData data)
	{
		base.Cancel(eventID, ref data);
		EventManager instance = Singleton<EventManager>.get_instance();
		data.m_ticketMoney = 0uL;
		instance.m_globalEventDataDirty = true;
		ushort nextBuildingEvent = data.m_nextBuildingEvent;
		if (nextBuildingEvent != 0 && (instance.m_events.m_buffer[(int)nextBuildingEvent].m_flags & EventData.Flags.Cancelled) != EventData.Flags.None)
		{
			EventInfo info = instance.m_events.m_buffer[(int)nextBuildingEvent].Info;
			uint num = (uint)Mathf.RoundToInt(info.m_eventAI.m_eventDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			uint num2 = instance.m_events.m_buffer[(int)nextBuildingEvent].m_startFrame + num;
			if (currentFrameIndex < num2)
			{
				Singleton<EventManager>.get_instance().ReleaseEvent(eventID);
			}
		}
	}

	public override void SetColor(ushort eventID, ref EventData data, Color32 newColor)
	{
		if (newColor.r != data.m_color.r || newColor.g != data.m_color.g || newColor.b != data.m_color.b)
		{
			base.SetColor(eventID, ref data, newColor);
			if (data.m_nextBuildingEvent != 0)
			{
				Singleton<EventManager>.get_instance().m_events.m_buffer[(int)data.m_nextBuildingEvent].m_color = newColor;
			}
			Singleton<EventManager>.get_instance().m_globalEventDataDirty = true;
			if (data.m_building != 0)
			{
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(data.m_building);
				Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(data.m_building, false);
			}
			HashSet<ushort> hashSet;
			if (Singleton<EventManager>.get_instance().m_eventLocations.TryGetValue(eventID, out hashSet))
			{
				foreach (ushort current in hashSet)
				{
					Singleton<BuildingManager>.get_instance().UpdateBuildingColors(current);
					Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(current, false);
				}
			}
			Singleton<GuideManager>.get_instance().m_festivalColorNotUsed.Disable();
		}
	}

	public override void SetTicketPrice(ushort eventID, ref EventData data, int newPrice)
	{
		int num = newPrice - this.m_ticketPrice;
		if (num != this.m_info.m_ticketPriceOffset)
		{
			this.m_info.m_ticketPriceOffset = num;
			Singleton<GuideManager>.get_instance().m_ticketPricesNotUsed.Disable();
		}
	}

	public override void VisitorEnter(ushort eventID, ref EventData data, ushort buildingID, uint citizenID)
	{
		base.VisitorEnter(eventID, ref data, buildingID, citizenID);
		if (buildingID == data.m_building && (data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			data.m_ticketMoney += (ulong)((long)this.GetTicketPrice(eventID, ref data));
		}
	}

	public override BuildingInfo ModifySubBuildingInfo(BuildingInfo orig)
	{
		if (this.m_customSubBuildings != null && this.m_customSubBuildings.Length != 0)
		{
			DummyBuildingAI dummyBuildingAI = orig.m_buildingAI as DummyBuildingAI;
			if (dummyBuildingAI != null)
			{
				for (int i = 0; i < this.m_customSubBuildings.Length; i++)
				{
					DummyBuildingAI dummyBuildingAI2 = this.m_customSubBuildings[i].m_buildingAI as DummyBuildingAI;
					if (dummyBuildingAI2 != null && dummyBuildingAI.m_dummyID == dummyBuildingAI2.m_dummyID)
					{
						return this.m_customSubBuildings[i];
					}
				}
			}
		}
		return orig;
	}

	public override int GetEventPriority(int freeSpace, int maxSpace)
	{
		return 8;
	}

	public override Color32 GetCitizenColor(ushort eventID, ref EventData data, uint citizen)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None || citizen != 0u)
		{
			return this.m_defaultBandColor;
		}
		return new Color32(0, 0, 0, 0);
	}

	public override Color32 GetBuildingColor(ushort eventID, ref EventData data)
	{
		return data.m_color;
	}

	public override bool IsBuildingActive(ushort eventID, ref EventData data)
	{
		return (data.m_flags & EventData.Flags.Active) != EventData.Flags.None;
	}

	public override DistrictPolicies.Event GetEventPolicyMask(ushort eventID, ref EventData data)
	{
		return DistrictPolicies.Event.BandAdvertisement | DistrictPolicies.Event.PremiumStudio;
	}

	public override int GetBudget(ushort eventID, ref EventData data)
	{
		return 100;
	}

	public override int GetTicketPrice(ushort eventID, ref EventData data)
	{
		return this.m_ticketPrice + this.m_info.m_ticketPriceOffset;
	}

	public override int GetCapacity(ushort eventID, ref EventData data, int orig, int max)
	{
		int num = Singleton<EventManager>.get_instance().m_finalBandPopularityBonus * (100 + Singleton<EventManager>.get_instance().m_adCampaignModifierFinal) / 102400;
		int num2 = Mathf.Clamp(50 + this.m_info.m_popularityOffset + num, 0, 100);
		Randomizer randomizer;
		randomizer..ctor((uint)eventID ^ data.m_startFrame ^ (uint)data.m_building);
		int num3 = 50 + num2 / 2 + randomizer.Int32(-30, 30) + Singleton<EventManager>.get_instance().m_concertTicketSaleModifierFinal;
		int ticketPrice = this.GetTicketPrice(eventID, ref data);
		int num4 = Mathf.Clamp((ticketPrice * 100 + (this.m_ticketPrice >> 1)) / Mathf.Max(1, this.m_ticketPrice) - 100, -30, 30);
		if (num4 < -20)
		{
			num3 += -(num4 * 4) / 10;
		}
		else if (num4 < -10)
		{
			num3 += 2 - num4 * 3 / 10;
		}
		else if (num4 < 0)
		{
			num3 += -(num4 * 5) / 10;
		}
		else if (num4 < 10)
		{
			num3 += -(num4 * 5) / 10;
		}
		else if (num4 < 20)
		{
			num3 += -2 - num4 * 3 / 10;
		}
		else
		{
			num3 += -(num4 * 4) / 10;
		}
		return Mathf.Clamp((max * num3 + 50) / 100, 0, max);
	}

	public override CitizenInstance.Flags GetHangAroundFlags(ushort eventID, ref EventData data, ushort buildingID, ref Vector3 position, ref Vector2 direction)
	{
		if ((data.m_flags & EventData.Flags.Active) != EventData.Flags.None)
		{
			if (buildingID != 0)
			{
				BuildingManager instance = Singleton<BuildingManager>.get_instance();
				BuildingInfo info = instance.m_buildings.m_buffer[(int)buildingID].Info;
				direction = info.m_buildingAI.GetInterestDirection(buildingID, ref instance.m_buildings.m_buffer[(int)buildingID], position);
			}
			return CitizenInstance.Flags.HangAround | CitizenInstance.Flags.Cheering;
		}
		return CitizenInstance.Flags.HangAround;
	}

	public override void ModifyNoise(ushort eventID, ref EventData data, ref int accumulation, ref float range)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) == EventData.Flags.None)
		{
			accumulation >>= 2;
			range *= 0.25f;
		}
		else if ((data.m_flags & EventData.Flags.Active) == EventData.Flags.None)
		{
			uint num = (uint)Mathf.RoundToInt(this.m_prepareDuration * SimulationManager.DAYTIME_HOUR_TO_FRAME);
			uint startFrame = data.m_startFrame;
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex + num <= startFrame)
			{
				accumulation >>= 2;
				range *= 0.25f;
			}
			else if (currentFrameIndex < startFrame)
			{
				int num2 = (int)(256u - (startFrame - currentFrameIndex) * 192u / num);
				accumulation = accumulation * num2 >> 8;
				range = range * (float)num2 / 256f;
			}
		}
	}

	public override int GetCrimeAccumulation(ushort eventID, ref EventData data, int orig)
	{
		if ((data.m_flags & (EventData.Flags.Preparing | EventData.Flags.Active)) != EventData.Flags.None)
		{
			return orig + (orig + 19) / 20;
		}
		int num = Mathf.Clamp(100 - (int)(data.m_securityBudget * 40) / Mathf.Max(1, this.m_securityBudget), 0, 100);
		return (orig * num + 50) / 100;
	}

	public string GetBandName(ushort eventID, ref EventData data)
	{
		if (this.m_info.m_prefabDataIndex != -1)
		{
			string text = PrefabCollection<EventInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
			return Locale.Get("CONCERT_BAND_NAME", text);
		}
		return null;
	}

	public static int GetPopularity(EventInfo bandInfo)
	{
		int num = Singleton<EventManager>.get_instance().m_finalBandPopularityBonus * (100 + Singleton<EventManager>.get_instance().m_adCampaignModifierFinal) / 102400;
		return Mathf.Clamp(50 + bandInfo.m_popularityOffset + num, 0, 100);
	}

	public Color m_defaultBandColor = Color.get_red();

	public Color m_defaultThemeColor = Color.get_white();

	public MessageInfo m_onPrepareMessage;

	public ManualMilestone m_completePassMilestone;

	public BuildingInfo[] m_customSubBuildings;

	public AudioInfo m_crowdAudio;
}
