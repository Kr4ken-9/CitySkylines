﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class CargoTrainAI : TrainAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (data.m_leadingVehicle == 0 && infoMode == InfoManager.InfoMode.Transport)
		{
			return Singleton<TransportManager>.get_instance().m_properties.m_transportColors[(int)this.m_transportInfo.m_transportType];
		}
		if (data.m_leadingVehicle == 0 && infoMode == InfoManager.InfoMode.Connections)
		{
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			TransferManager.TransferReason transferType = (TransferManager.TransferReason)data.m_transferType;
			if (currentSubMode == InfoManager.SubInfoMode.Default && (data.m_flags & Vehicle.Flags.Importing) != (Vehicle.Flags)0 && transferType != TransferManager.TransferReason.None)
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)transferType];
			}
			if (currentSubMode == InfoManager.SubInfoMode.WaterPower && (data.m_flags & Vehicle.Flags.Exporting) != (Vehicle.Flags)0 && transferType != TransferManager.TransferReason.None)
			{
				return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)transferType];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		else
		{
			if (infoMode != InfoManager.InfoMode.TrafficRoutes || Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default)
			{
				return base.GetColor(vehicleID, ref data, infoMode);
			}
			InstanceID empty = InstanceID.Empty;
			empty.Vehicle = vehicleID;
			if (Singleton<NetManager>.get_instance().PathVisualizer.IsPathVisible(empty))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3];
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CARGOTRAIN_LOADING");
		}
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_CARGOTRUCK_RETURN");
		}
		if (data.m_targetBuilding != 0)
		{
			target = InstanceID.Empty;
			target.Building = data.m_targetBuilding;
			return Locale.Get("VEHICLE_STATUS_CARGOTRAIN_TRANSPORT");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override string GetLocalizedStatus(ushort parkedVehicleID, ref VehicleParked data, out InstanceID target)
	{
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CARGOTRAIN_LOADING");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = 0;
		max = this.m_cargoCapacity;
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_firstCargo;
		int num2 = 0;
		while (num != 0)
		{
			current++;
			num = instance.m_vehicles.m_buffer[(int)num].m_nextCargo;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		if ((data.m_flags & Vehicle.Flags.DummyTraffic) != (Vehicle.Flags)0)
		{
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			current = randomizer.Int32(max >> 1, max);
		}
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		data.m_flags |= Vehicle.Flags.WaitingCargo;
		data.m_flags |= Vehicle.Flags.WaitingLoading;
		data.m_flags |= Vehicle.Flags.Stopped;
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			data.Unspawn(vehicleID);
			data.m_targetPos0 = data.GetLastFramePosition();
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = data.m_targetPos0;
			data.m_targetPos2 = data.m_targetPos0;
			data.m_targetPos3 = data.m_targetPos0;
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
			if ((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
			{
				if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Importing;
				}
				else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
				{
					data.m_flags |= Vehicle.Flags.Exporting;
				}
			}
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		if (targetBuilding != data.m_targetBuilding)
		{
			this.RemoveTarget(vehicleID, ref data);
			data.m_targetBuilding = targetBuilding;
			data.m_flags &= ~Vehicle.Flags.WaitingTarget;
			data.m_waitCounter = 0;
			if (targetBuilding != 0)
			{
				Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
				if ((Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
				{
					if ((data.m_flags & Vehicle.Flags.TransferToTarget) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Exporting;
					}
					else if ((data.m_flags & Vehicle.Flags.TransferToSource) != (Vehicle.Flags)0)
					{
						data.m_flags |= Vehicle.Flags.Importing;
					}
				}
			}
			else
			{
				data.m_flags |= Vehicle.Flags.GoingBack;
			}
		}
		if ((data.m_flags & Vehicle.Flags.WaitingCargo) == (Vehicle.Flags)0 && !this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason reason, TransferManager.TransferOffer offer)
	{
		if (reason == (TransferManager.TransferReason)data.m_transferType)
		{
			ushort building = offer.Building;
			if (building != 0)
			{
				this.SetTarget(vehicleID, ref data, building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, reason, offer);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = (int)data.m_transferSize;
		max = this.m_cargoCapacity;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingCargo) != (Vehicle.Flags)0)
		{
			bool flag = Singleton<SimulationManager>.get_instance().m_randomizer.Int32(2u) == 0;
			if (!flag && data.m_sourceBuilding != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None)
			{
				flag = true;
			}
			if (!flag && data.m_targetBuilding != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].m_flags & Building.Flags.Active) == Building.Flags.None)
			{
				flag = true;
			}
			if (!flag)
			{
				if ((int)data.m_transferSize >= this.m_cargoCapacity)
				{
					data.m_waitCounter = 255;
				}
				else
				{
					data.m_waitCounter = (byte)Mathf.Min((int)(data.m_waitCounter + 1), 255);
				}
				if (data.m_waitCounter == 255 && ((data.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0 || this.CanSpawnAt(data.GetLastFramePosition())))
				{
					data.m_flags &= ~Vehicle.Flags.WaitingCargo;
					data.m_waitCounter = 0;
					this.StartPathFind(vehicleID, ref data);
				}
			}
		}
		else if ((data.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0)
		{
			if ((data.m_flags & Vehicle.Flags.Spawned) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) == 16)
			{
				data.m_flags &= ~(Vehicle.Flags.Stopped | Vehicle.Flags.WaitingLoading);
				data.m_waitCounter = 0;
			}
		}
		else if ((data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0 && data.m_targetBuilding != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			this.SetTarget(vehicleID, ref data, 0);
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_firstCargo;
		data.m_firstCargo = 0;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextCargo = instance.m_vehicles.m_buffer[(int)num].m_nextCargo;
			instance.m_vehicles.m_buffer[(int)num].m_nextCargo = 0;
			instance.m_vehicles.m_buffer[(int)num].m_cargoParent = 0;
			VehicleInfo info = instance.m_vehicles.m_buffer[(int)num].Info;
			if (data.m_targetBuilding != 0)
			{
				info.m_vehicleAI.SetSource(num, ref instance.m_vehicles.m_buffer[(int)num], data.m_targetBuilding);
				info.m_vehicleAI.SetTarget(num, ref instance.m_vehicles.m_buffer[(int)num], instance.m_vehicles.m_buffer[(int)num].m_targetBuilding);
			}
			num = nextCargo;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_waitCounter = 0;
		data.m_flags |= Vehicle.Flags.WaitingLoading;
		return false;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		VehicleManager instance = Singleton<VehicleManager>.get_instance();
		ushort num = data.m_firstCargo;
		data.m_firstCargo = 0;
		int num2 = 0;
		while (num != 0)
		{
			ushort nextCargo = instance.m_vehicles.m_buffer[(int)num].m_nextCargo;
			instance.m_vehicles.m_buffer[(int)num].m_nextCargo = 0;
			instance.m_vehicles.m_buffer[(int)num].m_cargoParent = 0;
			instance.ReleaseVehicle(num);
			num = nextCargo;
			if (++num2 > 16384)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		data.m_waitCounter = 0;
		data.m_flags |= Vehicle.Flags.WaitingLoading;
		return false;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingLoading) != (Vehicle.Flags)0)
		{
			vehicleData.m_waitCounter = (byte)Mathf.Min((int)(vehicleData.m_waitCounter + 1), 255);
			if (vehicleData.m_waitCounter >= 16)
			{
				if (vehicleData.m_targetBuilding != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & Building.Flags.IncomingOutgoing) == Building.Flags.None)
				{
					ushort num = CargoTruckAI.FindNextCargoParent(vehicleData.m_targetBuilding, this.m_info.m_class.m_service, this.m_info.m_class.m_subService);
					if (num != 0)
					{
						ushort targetBuilding = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)num].m_targetBuilding;
						if (targetBuilding != 0)
						{
							CargoTruckAI.SwitchCargoParent(num, vehicleID);
							vehicleData.m_waitCounter = 0;
							vehicleData.m_flags &= ~Vehicle.Flags.WaitingLoading;
							this.SetTarget(vehicleID, ref vehicleData, targetBuilding);
							return (vehicleData.m_flags & Vehicle.Flags.Spawned) == (Vehicle.Flags)0;
						}
					}
				}
				Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
				return true;
			}
			return false;
		}
		else
		{
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				return this.ArriveAtSource(vehicleID, ref vehicleData);
			}
			return this.ArriveAtTarget(vehicleID, ref vehicleData);
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if (vehicleData.m_leadingVehicle == 0)
		{
			Vector3 startPos;
			if ((vehicleData.m_flags & Vehicle.Flags.Reversed) != (Vehicle.Flags)0)
			{
				ushort lastVehicle = vehicleData.GetLastVehicle(vehicleID);
				startPos = Singleton<VehicleManager>.get_instance().m_vehicles.m_buffer[(int)lastVehicle].m_targetPos0;
			}
			else
			{
				startPos = vehicleData.m_targetPos0;
			}
			if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				if (vehicleData.m_sourceBuilding != 0)
				{
					BuildingManager instance = Singleton<BuildingManager>.get_instance();
					BuildingInfo info = instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].Info;
					Randomizer randomizer;
					randomizer..ctor((int)vehicleID);
					Vector3 endPos;
					Vector3 vector;
					info.m_buildingAI.CalculateSpawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding], ref randomizer, this.m_info, out endPos, out vector);
					return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos);
				}
			}
			else if (vehicleData.m_targetBuilding != 0)
			{
				BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
				BuildingInfo info2 = instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].Info;
				Randomizer randomizer2;
				randomizer2..ctor((int)vehicleID);
				Vector3 endPos2;
				Vector3 vector2;
				info2.m_buildingAI.CalculateSpawnPosition(vehicleData.m_targetBuilding, ref instance2.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding], ref randomizer2, this.m_info, out endPos2, out vector2);
				return this.StartPathFind(vehicleID, ref vehicleData, startPos, endPos2);
			}
		}
		return false;
	}

	[CustomizableProperty("Cargo Capacity")]
	public int m_cargoCapacity = 1;
}
