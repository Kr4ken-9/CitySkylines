﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class GameMainToolbar : MainToolbar
{
	protected override void OnEnable()
	{
		ToolsModifierControl.SetTool<DefaultTool>();
		base.OnEnable();
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.SetAdvisorFocus));
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.SetAdvisorFocus));
	}

	public override void ShowPoliciesPanel(byte district)
	{
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(this.m_PoliciesIndex);
		ToolsModifierControl.policiesPanel.Set(district);
	}

	public override void ShowEconomyPanel(int tabIndex)
	{
		UITabstrip uITabstrip = base.get_component() as UITabstrip;
		uITabstrip.set_selectedIndex(this.m_EconomyIndex);
		ToolsModifierControl.economyPanel.Show(tabIndex);
	}

	private void ShowHidePoliciesPanel(UIComponent comp, bool visible)
	{
		if (!GeneratedPanel.m_IsRefreshing)
		{
			if (visible)
			{
				ToolsModifierControl.policiesPanel.Show(0);
			}
			else
			{
				ToolsModifierControl.policiesPanel.Hide();
			}
		}
	}

	private void ShowHideEconomyPanel(UIComponent comp, bool visible)
	{
		if (!GeneratedPanel.m_IsRefreshing)
		{
			if (visible)
			{
				ToolsModifierControl.economyPanel.Show(-1);
			}
			else
			{
				ToolsModifierControl.economyPanel.Hide();
			}
		}
	}

	private void SetAdvisorFocus(UIComponent component, int idx)
	{
		UITabstrip uITabstrip = component as UITabstrip;
		if (uITabstrip != null && idx >= 0 && idx < uITabstrip.get_tabCount())
		{
			this.m_AdvisorFocus = uITabstrip.get_components()[idx];
		}
		else
		{
			this.m_AdvisorFocus = null;
		}
		this.UpdateAdvisor();
	}

	public void UpdateAdvisor()
	{
		if (this.m_AdvisorFocus != null)
		{
			string name = this.m_AdvisorFocus.get_name();
			string icon = "ToolbarIconZoomOutCity";
			string spriteName = TutorialAdvisorPanel.GetSpriteName(name);
			if (this.m_AdvisorFocus is UIButton)
			{
				icon = ((UIButton)this.m_AdvisorFocus).get_normalFgSprite();
			}
			SavedBool savedBool = new SavedBool(name, Settings.userGameState, true);
			if (savedBool)
			{
				ToolsModifierControl.advisorPanel.Show(name, icon, spriteName, 0f, false, true);
				savedBool.set_value(false);
			}
			else
			{
				ToolsModifierControl.advisorPanel.Refresh(name, icon, spriteName);
			}
		}
		else
		{
			ToolsModifierControl.advisorPanel.Refresh(null);
		}
	}

	private string GetUnlockText(ItemClass.Service service)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_ServiceMilestones[(int)service];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	private string GetUnlockText(UnlockManager.Feature feature)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	public override void CleanPanel()
	{
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		if (this.m_EventsRegistered)
		{
			if (uITabstrip.get_tabPages() != null && uITabstrip.get_tabPages().get_components() != null)
			{
				uITabstrip.get_tabPages().get_components()[this.m_PoliciesIndex].remove_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.ShowHidePoliciesPanel));
				uITabstrip.get_tabPages().get_components()[this.m_EconomyIndex].remove_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.ShowHideEconomyPanel));
			}
			this.m_EventsRegistered = false;
		}
		base.CleanPanel();
	}

	public override void RefreshPanel()
	{
		GeneratedPanel.m_IsRefreshing = true;
		UITabstrip uITabstrip = ToolsModifierControl.mainToolbar.get_component() as UITabstrip;
		this.m_LastSelection = uITabstrip.get_selectedIndex();
		uITabstrip.set_selectedIndex(-1);
		base.RefreshPanel();
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
		bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		string unlockText = this.GetUnlockText(UnlockManager.Feature.Bulldozer);
		if (unlockText != null)
		{
			this.m_BulldozerButton.set_tooltip(Locale.Get("MAIN_TOOL", "Bulldozer") + " - " + unlockText);
		}
		int[] array = new int[]
		{
			3,
			3,
			3,
			2,
			3,
			2
		};
		int num = 1;
		int i = 0;
		int num2 = 0;
		while (i < GameMainToolbar.kServices.Length)
		{
			bool enabled = ToolsModifierControl.IsUnlocked(GameMainToolbar.kServices[i].get_enumValue());
			if (GameMainToolbar.kServices[i].get_enumValue() == ItemClass.Service.Water && flag)
			{
				base.SpawnSubEntry(uITabstrip, GameMainToolbar.kServices[i].get_enumName(), "MAIN_TOOL_WW", this.GetUnlockText(GameMainToolbar.kServices[i].get_enumValue()), "ToolbarIcon", enabled);
			}
			else if (GameMainToolbar.kServices[i].get_enumValue() == ItemClass.Service.FireDepartment && flag2)
			{
				base.SpawnSubEntry(uITabstrip, GameMainToolbar.kServices[i].get_enumName(), "MAIN_TOOL_ND", this.GetUnlockText(GameMainToolbar.kServices[i].get_enumValue()), "ToolbarIcon", enabled);
			}
			else
			{
				base.SpawnSubEntry(uITabstrip, GameMainToolbar.kServices[i].get_enumName(), "MAIN_TOOL", this.GetUnlockText(GameMainToolbar.kServices[i].get_enumValue()), "ToolbarIcon", enabled);
			}
			if (i == 0)
			{
				base.SpawnSubEntry(uITabstrip, "Zoning", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Zoning), "ToolbarIcon", ZoningPanel.IsZoningPossible());
				base.SpawnSubEntry(uITabstrip, "District", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Districts), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Districts));
				base.SpawnSeparator(uITabstrip);
			}
			else if (num2 % array[num] == 0)
			{
				base.SpawnSmallSeparator(uITabstrip);
				num++;
				num2 = 0;
			}
			i++;
			num2++;
		}
		base.SpawnSubEntry(uITabstrip, "Wonders", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Wonders), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Wonders));
		base.SpawnSeparator(uITabstrip);
		base.SpawnSubEntry(uITabstrip, "Landscaping", (!flag2) ? "MAIN_TOOL" : "MAIN_TOOL_ND", this.GetUnlockText(UnlockManager.Feature.Landscaping), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Landscaping));
		base.SpawnSeparator(uITabstrip);
		if (ToolsModifierControl.policiesPanel.m_DockingPosition == PoliciesPanel.DockingPosition.Left)
		{
			UIButton uIButton = base.SpawnButtonEntry(uITabstrip, "Policies", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Policies), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Policies));
			this.m_PoliciesIndex = uIButton.get_zOrder();
			ToolsModifierControl.policiesPanel.SetParentButton(uIButton);
			UIButton uIButton2 = base.SpawnButtonEntry(uITabstrip, "Money", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Economy), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy));
			this.m_EconomyIndex = uIButton2.get_zOrder();
			ToolsModifierControl.economyPanel.SetParentButton(uIButton2);
		}
		else
		{
			UIButton uIButton3 = base.SpawnButtonEntry(uITabstrip, "Money", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Economy), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy));
			this.m_EconomyIndex = uIButton3.get_zOrder();
			ToolsModifierControl.economyPanel.SetParentButton(uIButton3);
			UIButton uIButton4 = base.SpawnButtonEntry(uITabstrip, "Policies", "MAIN_TOOL", this.GetUnlockText(UnlockManager.Feature.Policies), "ToolbarIcon", ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Policies));
			this.m_PoliciesIndex = uIButton4.get_zOrder();
			ToolsModifierControl.policiesPanel.SetParentButton(uIButton4);
		}
		if (!this.m_EventsRegistered)
		{
			uITabstrip.get_tabPages().get_components()[this.m_PoliciesIndex].add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.ShowHidePoliciesPanel));
			uITabstrip.get_tabPages().get_components()[this.m_EconomyIndex].add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.ShowHideEconomyPanel));
		}
		this.m_EventsRegistered = true;
		if (this.m_LastSelection != -1)
		{
			uITabstrip.set_selectedIndex(this.m_LastSelection);
		}
		this.m_LastSelection = -1;
		GeneratedPanel.m_IsRefreshing = false;
	}

	public void ShowAdvisor(UIComponent comp, UIMouseEventParameter p)
	{
		UIMultiStateButton uIMultiStateButton = p.get_source() as UIMultiStateButton;
		if (uIMultiStateButton != null)
		{
			if (uIMultiStateButton.get_activeStateIndex() == 1)
			{
				ToolsModifierControl.advisorPanel.Show("Default", "ToolbarIconZoomOutGlobe", string.Empty, 0f, true, false);
			}
			else if (uIMultiStateButton.get_activeStateIndex() == 0)
			{
				ToolsModifierControl.advisorPanel.Hide();
			}
		}
	}

	public void ShowUnlockPanel()
	{
		ToolsModifierControl.unlockingPanel.Toggle();
	}

	private UIComponent m_AdvisorFocus;

	private static readonly PositionData<ItemClass.Service>[] kServices = Utils.GetOrderedEnumData<ItemClass.Service>("Game");

	protected int m_PoliciesIndex;

	protected int m_EconomyIndex;

	private int m_LastSelection = -1;

	private bool m_EventsRegistered;
}
