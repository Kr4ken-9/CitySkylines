﻿using System;

[Serializable]
public struct ModInfo
{
	public string modName;

	public ulong modWorkshopID;
}
