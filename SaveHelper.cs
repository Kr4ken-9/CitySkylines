﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework.IO;
using ColossalFramework.Packaging;
using ColossalFramework.UI;
using UnityEngine;

public static class SaveHelper
{
	public static string SaveExtension
	{
		get
		{
			return SaveHelper.m_SaveExtension;
		}
	}

	public static string MapExtension
	{
		get
		{
			return SaveHelper.m_MapExtension;
		}
	}

	public static SaveGameMetaData GetLatestSaveGame()
	{
		DateTime t = DateTime.MinValue;
		SaveGameMetaData result = null;
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.SaveGameMetaData
		}))
		{
			if (!PackageHelper.IsDemoModeSave(current))
			{
				if (current != null && current.get_isEnabled())
				{
					SaveGameMetaData saveGameMetaData = current.Instantiate<SaveGameMetaData>();
					if (saveGameMetaData != null && saveGameMetaData.timeStamp >= t)
					{
						t = saveGameMetaData.timeStamp;
						result = saveGameMetaData;
					}
				}
			}
		}
		return result;
	}

	public static List<Package.Asset> GetMapsOnDisk()
	{
		List<Package.Asset> list = new List<Package.Asset>();
		FileInfo[] fileInfo = SaveHelper.GetFileInfo(DataLocation.get_mapLocation());
		if (fileInfo != null)
		{
			FileInfo[] array = fileInfo;
			for (int i = 0; i < array.Length; i++)
			{
				FileInfo fileInfo2 = array[i];
				if (string.Compare(Path.GetExtension(fileInfo2.Name), SaveHelper.m_MapExtension, StringComparison.OrdinalIgnoreCase) == 0)
				{
					string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileInfo2.Name);
					Package.Asset item = new Package.Asset(fileNameWithoutExtension, fileInfo2.FullName);
					list.Add(item);
				}
			}
		}
		return list;
	}

	public static List<Package.Asset> GetSavesOnDisk()
	{
		List<Package.Asset> list = new List<Package.Asset>();
		FileInfo[] fileInfo = SaveHelper.GetFileInfo(DataLocation.get_saveLocation());
		if (fileInfo != null)
		{
			FileInfo[] array = fileInfo;
			for (int i = 0; i < array.Length; i++)
			{
				FileInfo fileInfo2 = array[i];
				if (string.Compare(Path.GetExtension(fileInfo2.Name), SaveHelper.m_SaveExtension, StringComparison.OrdinalIgnoreCase) == 0)
				{
					string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileInfo2.Name);
					Package.Asset item = new Package.Asset(fileNameWithoutExtension, fileInfo2.FullName);
					list.Add(item);
				}
			}
		}
		return list;
	}

	public static bool AreThereSaveGames()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.SaveGameMetaData
		}))
		{
			if (!PackageHelper.IsDemoModeSave(current))
			{
				if (current != null && current.get_isEnabled())
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool AreThereUserMaps()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				MapMetaData mapMetaData = current.Instantiate<MapMetaData>();
				if (mapMetaData != null && !mapMetaData.IsBuiltinMap(current.get_package().get_packagePath()))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool AreThereUserAssets()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.CustomAssetMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				return true;
			}
		}
		return false;
	}

	public static bool AreThereUserThemes()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapThemeMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				return true;
			}
		}
		return false;
	}

	public static bool AreThereUserScenarios()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.ScenarioMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
				if (scenarioMetaData != null && !scenarioMetaData.IsBuiltinScenario(current.get_package().get_packagePath()))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool AreThereMaps()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.MapMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				MapMetaData mapMetaData = current.Instantiate<MapMetaData>();
				if (mapMetaData != null && mapMetaData.isPublished)
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool AreThereScenarios()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.ScenarioMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				return true;
			}
		}
		return false;
	}

	public static bool AreTherePublishedScenarios()
	{
		foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
		{
			UserAssetType.ScenarioMetaData
		}))
		{
			if (current != null && current.get_isEnabled())
			{
				ScenarioMetaData scenarioMetaData = current.Instantiate<ScenarioMetaData>();
				if (scenarioMetaData != null && scenarioMetaData.isPublished)
				{
					if (!scenarioMetaData.builtin)
					{
						bool result = true;
						return result;
					}
					string assetName = StringUtils.SafeFormat("{0}.{1}", new object[]
					{
						scenarioMetaData.assetRef.get_package().get_packageName(),
						scenarioMetaData.assetRef.get_name()
					});
					bool flag = SteamHelper.IsDLCOwned(NewScenarioGamePanel.GetRequiredDLC(assetName));
					if (flag)
					{
						bool result = true;
						return result;
					}
				}
			}
		}
		return false;
	}

	public static FileInfo[] GetFileInfo(string location)
	{
		if (string.IsNullOrEmpty(location))
		{
			return null;
		}
		DirectoryInfo directoryInfo = new DirectoryInfo(location);
		if (directoryInfo.Exists)
		{
			FileInfo[] result = null;
			try
			{
				result = directoryInfo.GetFiles();
			}
			catch (Exception ex)
			{
				Debug.LogError("An exception occured " + ex);
				UIView.ForwardException(ex);
			}
			return result;
		}
		return null;
	}

	private static readonly string m_SaveExtension = ".ccs";

	private static readonly string m_MapExtension = ".cct";
}
