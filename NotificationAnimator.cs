﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class NotificationAnimator : UICustomControl
{
	private void Awake()
	{
		this.m_baseSize = this.m_sprite.get_size().x;
		base.get_component().add_eventVisibilityChanged(new PropertyChangedEventHandler<bool>(this.OnVisChanged));
		this.m_isAnimating = true;
		this.StartAnim();
	}

	private void OnVisChanged(UIComponent component, bool visible)
	{
		if (visible && !this.m_isAnimating)
		{
			this.m_isAnimating = true;
			this.StartAnim();
		}
	}

	private void StartAnim()
	{
		if (base.get_component().get_isVisible())
		{
			ValueAnimator.Animate(delegate(float val)
			{
				if (this.m_sprite != null)
				{
					this.m_sprite.set_size(new Vector2(val, val));
				}
			}, new AnimatedFloat(this.m_baseSize, this.m_baseSize * this.m_targetScale, this.m_animationTime), new Action(this.StartAnim2));
		}
		else
		{
			this.m_isAnimating = false;
		}
	}

	private void StartAnim2()
	{
		if (base.get_component().get_isVisible())
		{
			ValueAnimator.Animate(delegate(float val)
			{
				if (this.m_sprite != null)
				{
					this.m_sprite.set_size(new Vector2(val, val));
				}
			}, new AnimatedFloat(this.m_baseSize * this.m_targetScale, this.m_baseSize, this.m_animationTime), new Action(this.StartAnim));
		}
		else
		{
			this.m_isAnimating = false;
		}
	}

	public float m_animationTime = 1f;

	public float m_targetScale = 0.7f;

	public UISprite m_sprite;

	private float m_baseSize;

	private bool m_isAnimating;
}
