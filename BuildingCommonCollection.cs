﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

public class BuildingCommonCollection : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(BuildingCommonCollection.InitializePrefabs(base.get_gameObject().get_name(), this));
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading(base.get_gameObject().get_name());
		Singleton<BuildingManager>.get_instance().m_common = null;
		if (this.m_subInfos != null)
		{
			for (int i = 0; i < this.m_subInfos.m_size; i++)
			{
				this.m_subInfos.m_buffer[i].DestroyPrefab();
			}
			this.m_subInfos = null;
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	[DebuggerHidden]
	private static IEnumerator InitializePrefabs(string name, BuildingCommonCollection collection)
	{
		BuildingCommonCollection.<InitializePrefabs>c__Iterator0 <InitializePrefabs>c__Iterator = new BuildingCommonCollection.<InitializePrefabs>c__Iterator0();
		<InitializePrefabs>c__Iterator.collection = collection;
		return <InitializePrefabs>c__Iterator;
	}

	public static BuildingInfoSub FindCollapsedInfo(float width, float length, out int rotations, BuildingInfoSub[] list)
	{
		float num = 1000000f;
		float num2 = 1000000f;
		BuildingInfoSub buildingInfoSub = null;
		BuildingInfoSub buildingInfoSub2 = null;
		if (list != null)
		{
			for (int i = 0; i < list.Length; i++)
			{
				BuildingInfoSub buildingInfoSub3 = list[i];
				Vector3 size = buildingInfoSub3.m_generatedInfo.m_size;
				float num3 = Mathf.Max(1f, size.x * size.z);
				float num4 = Mathf.Max(1f, width * length);
				float num5 = Mathf.Max(num3, num4) / Mathf.Min(num3, num4) - 1f;
				float num6 = (size.x - size.z) / Mathf.Max(1f, Mathf.Min(size.x, size.z));
				float num7 = (width - length) / Mathf.Max(1f, Mathf.Min(width, length));
				float num8 = Mathf.Abs(num6 - num7) * 2f;
				float num9 = Mathf.Abs(num6 + num7) * 2f;
				float num10 = num5 + num8;
				if (num10 < num)
				{
					num = num10;
					buildingInfoSub = buildingInfoSub3;
				}
				float num11 = num5 + num9;
				if (num11 < num2)
				{
					num2 = num11;
					buildingInfoSub2 = buildingInfoSub3;
				}
			}
		}
		if (buildingInfoSub == null)
		{
			rotations = 0;
			return null;
		}
		if (buildingInfoSub == buildingInfoSub2)
		{
			rotations = 15;
			return buildingInfoSub;
		}
		if (num <= num2)
		{
			rotations = 5;
			return buildingInfoSub;
		}
		rotations = 10;
		return buildingInfoSub2;
	}

	public BuildingInfoSub m_construction;

	public BuildingInfoSub[] m_collapsedLow;

	public BuildingInfoSub[] m_collapsedHigh;

	[NonSerialized]
	public FastList<BuildingInfoSub> m_subInfos;
}
