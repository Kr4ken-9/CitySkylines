﻿using System;
using System.Collections.Generic;
using System.IO;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class CreditsScroller : UICustomControl
{
	private float currentYPosition
	{
		get
		{
			return this.m_CurrentYPosition - this.m_Panel.get_scrollPosition().y;
		}
		set
		{
			this.m_CurrentYPosition = value;
		}
	}

	private void LoadScrollText(string credits)
	{
		using (StringReader stringReader = new StringReader(credits))
		{
			while (true)
			{
				string text = stringReader.ReadLine();
				if (text == null)
				{
					break;
				}
				this.m_ScrollEntries.Add(text);
			}
		}
	}

	private void LoadScrollText()
	{
		this.m_ScrollEntries.Clear();
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			this.m_ScrollEntries.Add("[screenspace]");
			this.LoadScrollText(Locale.Get("GAME_CREDITSGB"));
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
		{
			this.m_ScrollEntries.Add("[screenspace]");
			this.LoadScrollText(Locale.Get("GAME_CREDITSMT"));
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			this.m_ScrollEntries.Add("[screenspace]");
			this.LoadScrollText(Locale.Get("GAME_CREDITSND"));
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
		{
			this.m_ScrollEntries.Add("[screenspace]");
			this.LoadScrollText(Locale.Get("GAME_CREDITSWW"));
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
		{
			this.m_ScrollEntries.Add("[screenspace]");
			this.LoadScrollText(Locale.Get("GAME_CREDITSAD"));
		}
		this.m_ScrollEntries.Add("[screenspace]");
		this.m_EndLineRead = 0;
		this.LoadScrollText(Locale.Get("GAME_CREDITS"));
		if (SteamHelper.IsSomeStadiumDLCOwned())
		{
			this.m_ScrollEntries.Add("[largespace]");
			this.LoadScrollText(Locale.Get("GAME_STADIUMS"));
		}
	}

	private void StartReadTexts()
	{
		this.currentYPosition = 0f;
		this.m_EndLineRead = 0;
		this.m_FirstItem = null;
		this.m_LastItem = null;
	}

	private bool ParseLine(string line)
	{
		if (line != null)
		{
			if (line.StartsWith("["))
			{
				int num = line.IndexOf("]");
				string tagName = line.Substring(1, num - 1);
				string text = line.Substring(num + 1);
				CreditsScroller.TagInfo tagInfo = this.GetTagInfo(tagName);
				if (tagInfo != null)
				{
					if (tagInfo.m_TagType == CreditsScroller.TagType.Text)
					{
						this.AddText(tagInfo, text);
					}
					else if (tagInfo.m_TagType == CreditsScroller.TagType.Image)
					{
						this.AddImage(tagInfo, text);
					}
					else if (tagInfo.m_TagType == CreditsScroller.TagType.Space)
					{
						this.AddSpace(tagInfo);
					}
				}
				else if (text == string.Empty)
				{
					this.AddSpace(this.m_DefaultTag);
				}
				else
				{
					this.AddText(this.m_DefaultTag, text);
				}
			}
			else if (line == string.Empty)
			{
				this.AddSpace(this.m_DefaultTag);
			}
			else
			{
				this.AddText(this.m_DefaultTag, line);
			}
			return true;
		}
		return false;
	}

	private bool ReadTexts(int lineCount = 1)
	{
		bool result = false;
		for (int i = 0; i < lineCount; i++)
		{
			if (this.m_EndLineRead < this.m_ScrollEntries.Count)
			{
				result = this.ParseLine(this.m_ScrollEntries[this.m_EndLineRead]);
				this.m_EndLineRead++;
			}
		}
		return result;
	}

	private void ClearTexts()
	{
		int num = this.m_FirstItem.get_zOrder() + 1;
		if (num < base.get_component().get_components().Count)
		{
			UIComponent firstItem = base.get_component().get_components()[num];
			UIComponent firstItem2 = this.m_FirstItem;
			base.get_component().RemoveUIComponent(firstItem2);
			Object.Destroy(firstItem2.get_gameObject());
			this.m_FirstItem = firstItem;
			this.m_FirstItem.set_zOrder(0);
		}
	}

	public void ClearCredits()
	{
		while (base.get_component().get_components().Count > 0)
		{
			UIComponent uIComponent = base.get_component().get_components()[0];
			base.get_component().RemoveUIComponent(uIComponent);
			Object.DestroyImmediate(uIComponent.get_gameObject());
		}
	}

	private void Start()
	{
		this.m_Panel = (base.get_component() as UIScrollablePanel);
		this.m_Scrolling = false;
		this.m_FirstItem = null;
		this.m_LastItem = null;
		this.m_OriginalScrollSpeed = this.m_ScrollSpeed;
		this.LoadScrollText();
	}

	private void OnEnable()
	{
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Combine(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		UIFontManager.callbackRequestCharacterInfo = (UIFontManager.CallbackRequestCharacterInfo)Delegate.Remove(UIFontManager.callbackRequestCharacterInfo, new UIFontManager.CallbackRequestCharacterInfo(this.RequestCharacterInfo));
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnLocaleChanged()
	{
		this.LoadScrollText();
	}

	private void RequestCharacterInfo()
	{
		for (int i = 0; i < base.get_component().get_components().Count; i++)
		{
			UILabel uILabel = base.get_component().get_components()[i] as UILabel;
			if (uILabel != null)
			{
				UIDynamicFont uIDynamicFont = uILabel.get_font() as UIDynamicFont;
				uIDynamicFont.AddCharacterRequest(uILabel.get_text(), Mathf.CeilToInt(uILabel.get_textScale() / (float)uILabel.get_font().get_size()), 0);
			}
		}
	}

	private void LateUpdate()
	{
		if (this.m_Scrolling)
		{
			if (this.m_LastItem == null || this.m_LastItem.get_relativePosition().y < this.m_Panel.get_size().y)
			{
				this.ReadTexts(1);
			}
			if (this.m_FirstItem != null && this.m_FirstItem.get_relativePosition().y + this.m_FirstItem.get_size().y < 0f)
			{
				this.ClearTexts();
			}
			this.m_Panel.set_scrollPosition(new Vector2(0f, this.m_Panel.get_scrollPosition().y + this.m_ScrollSpeed * Time.get_deltaTime()));
			if (this.m_LastItem == null || this.m_LastItem.get_relativePosition().y + this.m_LastItem.get_size().y < 0f)
			{
				this.StopScrolling();
			}
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			UIView.PushModal(base.get_component());
			base.get_component().Focus();
		}
		else if (UIView.GetModalComponent() == base.get_component())
		{
			UIView.PopModal();
		}
	}

	public void StartScrolling()
	{
		this.m_ScrollSpeed = this.m_OriginalScrollSpeed;
		this.m_Scrolling = true;
		this.m_Panel.set_scrollPosition(Vector2.get_zero());
		this.StartReadTexts();
		this.ReadTexts(1);
		ValueAnimator.Animate("CreditsFadein", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(0f, 1f, this.m_CreditsFadeinTime));
	}

	public void StopScrolling()
	{
		ValueAnimator.Animate("CreditsFadeout", delegate(float val)
		{
			base.get_component().set_opacity(val);
		}, new AnimatedFloat(1f, 0f, this.m_CreditsFadeoutTime), delegate
		{
			UIView.PopModal();
			this.m_Scrolling = false;
			this.m_MainMenu.CreditsEnded();
			this.ClearCredits();
		});
	}

	public void OnMouseDown(UIComponent component, UIMouseEventParameter p)
	{
		if (!p.get_used())
		{
			this.StopScrolling();
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 273)
			{
				if (!ValueAnimator.IsAnimating("CreditsScrollFaster") && this.m_ScrollSpeed > 0f)
				{
					ValueAnimator.Animate("CreditsScrollFaster", delegate(float val)
					{
						this.m_ScrollSpeed = val;
					}, new AnimatedFloat(this.m_ScrollSpeed, Mathf.Max(this.m_ScrollSpeed - this.m_OriginalScrollSpeed, 0f), 0.3f));
				}
			}
			else if (p.get_keycode() == 274)
			{
				if (!ValueAnimator.IsAnimating("CreditsScrollFaster") && this.m_ScrollSpeed < 400f)
				{
					ValueAnimator.Animate("CreditsScrollFaster", delegate(float val)
					{
						this.m_ScrollSpeed = val;
					}, new AnimatedFloat(this.m_ScrollSpeed, this.m_ScrollSpeed + this.m_OriginalScrollSpeed, 0.3f));
				}
			}
			else if (p.get_keycode() == 27 || p.get_keycode() == 13 || p.get_keycode() == 32)
			{
				this.StopScrolling();
			}
		}
	}

	private CreditsScroller.TagInfo GetTagInfo(string tagName)
	{
		for (int i = 0; i < this.m_Tags.Length; i++)
		{
			if (this.m_Tags[i].tagName == tagName)
			{
				return this.m_Tags[i];
			}
		}
		return null;
	}

	private UIComponent AddImage(CreditsScroller.TagInfo tagInfo, string content)
	{
		UISprite uISprite = base.get_component().AddUIComponent<UISprite>();
		uISprite.set_pivot(4);
		uISprite.set_anchor(65);
		if (tagInfo != null)
		{
			uISprite.set_atlas(tagInfo.m_Atlas);
		}
		uISprite.set_spriteName(content);
		UITextureAtlas.SpriteInfo spriteInfo = uISprite.get_atlas().get_Item(uISprite.get_spriteName());
		if (spriteInfo != null)
		{
			Vector2 pixelSize = spriteInfo.get_pixelSize();
			pixelSize.x = Mathf.Min(pixelSize.x, base.get_component().get_width());
			uISprite.set_size(pixelSize);
		}
		uISprite.set_relativePosition(new Vector3((uISprite.get_parent().get_size().x - uISprite.get_size().x) * 0.5f, this.currentYPosition));
		this.m_CurrentYPosition += ((tagInfo == null || !tagInfo.m_FixedHeight) ? uISprite.get_height() : ((float)tagInfo.m_Height));
		this.m_LastItem = uISprite;
		if (this.m_FirstItem == null)
		{
			this.m_FirstItem = uISprite;
		}
		return uISprite;
	}

	private UIComponent AddText(CreditsScroller.TagInfo tagInfo, string content)
	{
		UILabel uILabel = base.get_component().AddUIComponent<UILabel>();
		uILabel.set_pivot(4);
		if (tagInfo != null)
		{
			uILabel.set_font(tagInfo.m_Font);
			UIFontManager.Invalidate(uILabel.get_font());
			uILabel.set_textScale((float)tagInfo.m_FontSize / (float)uILabel.get_font().get_size());
			uILabel.set_textColor(tagInfo.m_TextColor);
			uILabel.set_textAlignment(tagInfo.m_TextAlign);
			uILabel.set_useGradient(tagInfo.m_UseGradient);
			uILabel.set_bottomColor(tagInfo.m_BottomColor);
			uILabel.set_useDropShadow(tagInfo.m_UseShadows);
			uILabel.set_dropShadowColor(tagInfo.m_ShadowsColor);
			uILabel.set_dropShadowOffset(tagInfo.m_ShadowsOffset);
			if (tagInfo.m_Atlas != null)
			{
				uILabel.set_atlas(tagInfo.m_Atlas);
			}
		}
		else
		{
			uILabel.set_textAlignment(1);
		}
		uILabel.set_autoSize(false);
		uILabel.set_width(base.get_component().get_width());
		uILabel.set_wordWrap(true);
		uILabel.set_autoHeight(true);
		uILabel.set_anchor(13);
		uILabel.set_text(content);
		uILabel.set_relativePosition(new Vector3((uILabel.get_parent().get_size().x - uILabel.get_size().x) * 0.5f, this.currentYPosition));
		this.m_CurrentYPosition += ((tagInfo == null || !tagInfo.m_FixedHeight) ? uILabel.get_height() : ((float)tagInfo.m_Height));
		this.m_LastItem = uILabel;
		if (this.m_FirstItem == null)
		{
			this.m_FirstItem = uILabel;
		}
		return uILabel;
	}

	private void AddSpace(CreditsScroller.TagInfo tagInfo)
	{
		if (tagInfo.m_FixedHeight)
		{
			this.m_CurrentYPosition += ((tagInfo.m_Height <= 0) ? (base.get_component().get_size().y * 1.1f) : ((float)tagInfo.m_Height));
		}
		else
		{
			this.m_CurrentYPosition += (float)this.m_DefaultSpaceHeight;
		}
	}

	public MainMenu m_MainMenu;

	public Color32 m_BackgroundColor;

	public float m_ScrollSpeed = 25f;

	public float m_MenuFadeoutTime = 0.5f;

	public float m_CreditsFadeinTime = 0.5f;

	public float m_MenuFadeinTime = 0.5f;

	public float m_CreditsFadeoutTime = 0.5f;

	private float m_OriginalScrollSpeed;

	private bool m_Scrolling;

	private UIComponent m_LastItem;

	private UIComponent m_FirstItem;

	private UIScrollablePanel m_Panel;

	private int m_EndLineRead;

	public int m_DefaultSpaceHeight = 20;

	public CreditsScroller.TagInfo m_DefaultTag;

	public CreditsScroller.TagInfo[] m_Tags;

	private float m_CurrentYPosition;

	private List<string> m_ScrollEntries = new List<string>();

	public enum TagType
	{
		Text,
		Image,
		Space
	}

	[Serializable]
	public class TagInfo
	{
		public string tagName;

		public CreditsScroller.TagType m_TagType;

		public UIFont m_Font;

		public int m_FontSize;

		public UIHorizontalAlignment m_TextAlign = 1;

		public Color32 m_TextColor = Color.get_white();

		public bool m_UseGradient;

		public Color32 m_BottomColor;

		public bool m_UseShadows;

		public Color32 m_ShadowsColor;

		public Vector2 m_ShadowsOffset;

		public bool m_FixedHeight;

		public int m_Height = 20;

		public UITextureAtlas m_Atlas;
	}
}
