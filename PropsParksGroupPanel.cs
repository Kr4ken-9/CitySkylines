﻿using System;

public sealed class PropsParksGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return GeneratedGroupPanel.GroupFilter.Prop;
		}
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsParksPlaygrounds", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsParksFlowersAndPlants", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsParksParkEquipment", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("PropsParksFountains", this.GetCategoryOrder(base.get_name()), "Props"), "PROPS_CATEGORY");
		return true;
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "PropsParksPlaygrounds")
			{
				return 0;
			}
			if (name == "PropsParksFlowersAndPlants")
			{
				return 1;
			}
			if (name == "PropsParksParkEquipment")
			{
				return 2;
			}
			if (name == "PropsParksFountains")
			{
				return 3;
			}
		}
		return 2147483647;
	}
}
