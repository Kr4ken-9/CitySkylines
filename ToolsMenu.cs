﻿using System;
using ColossalFramework.Packaging;
using ColossalFramework.UI;

public class ToolsMenu : MenuPanel
{
	protected override void Awake()
	{
		base.Awake();
		ToolsMenu.m_ToolsMenu = base.get_component();
		this.m_MapEditorOptions = base.Find<UIPanel>("MapEditorOptions");
		this.m_ThemeEditorOptions = base.Find<UIPanel>("ThemeEditorOptions");
		this.m_AssetEditorOptions = base.Find<UIPanel>("AssetEditorOptions");
		this.m_ScenarioEditorOptions = base.Find<UIPanel>("ScenarioEditorOptions");
		this.Refresh();
	}

	private void OnEnable()
	{
		PackageManager.add_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		PackageManager.add_eventAssetStateChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
	}

	private void OnDisable()
	{
		PackageManager.remove_eventPackagesChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
		PackageManager.remove_eventAssetStateChanged(new PackageManager.PackagesChangedHandler(this.OnPackagesChanged));
	}

	private void Refresh()
	{
		UIButton uIButton = this.m_MapEditorOptions.Find<UIButton>("Load");
		uIButton.set_isEnabled(SaveHelper.AreThereUserMaps());
		uIButton = this.m_AssetEditorOptions.Find<UIButton>("Load");
		uIButton.set_isEnabled(SaveHelper.AreThereUserAssets());
		uIButton = this.m_ThemeEditorOptions.Find<UIButton>("Load");
		uIButton.set_isEnabled(SaveHelper.AreThereUserThemes());
		uIButton = this.m_ScenarioEditorOptions.Find<UIButton>("Load");
		uIButton.set_isEnabled(SaveHelper.AreThereUserScenarios());
	}

	private void OnPackagesChanged()
	{
		this.Refresh();
		base.get_component().Focus();
	}

	public static void SetFocus()
	{
		if (ToolsMenu.m_ToolsMenu != null)
		{
			ToolsMenu.m_ToolsMenu.Focus();
		}
	}

	private void OnDestroy()
	{
		ToolsMenu.m_ToolsMenu = null;
	}

	protected override void Initialize()
	{
		base.Initialize();
		this.m_MapEditorOptions.Hide();
		this.m_AssetEditorOptions.Hide();
		this.m_ThemeEditorOptions.Hide();
		this.m_ScenarioEditorOptions.Hide();
	}

	public void ThemeEditor()
	{
		this.m_ThemeEditorOptions.Show();
		this.m_AssetEditorOptions.Hide();
		this.m_MapEditorOptions.Hide();
		this.m_ScenarioEditorOptions.Hide();
	}

	public void ThemeEditorNew()
	{
		UIView.get_library().ShowModal("NewThemePanel");
	}

	public void ThemeEditorLoad()
	{
		UIView.get_library().ShowModal("LoadThemePanel");
	}

	public void MapEditor()
	{
		this.m_MapEditorOptions.Show();
		this.m_AssetEditorOptions.Hide();
		this.m_ThemeEditorOptions.Hide();
		this.m_ScenarioEditorOptions.Hide();
	}

	public void MapEditorNew()
	{
		UIView.get_library().ShowModal("NewMapPanel");
	}

	public void MapEditorLoad()
	{
		UIView.get_library().ShowModal("LoadMapPanel");
	}

	public void AssetEditor()
	{
		this.m_AssetEditorOptions.Show();
		this.m_MapEditorOptions.Hide();
		this.m_ThemeEditorOptions.Hide();
		this.m_ScenarioEditorOptions.Hide();
	}

	public void NewAsset()
	{
		NewAssetPanel newAssetPanel = UIView.get_library().ShowModal<NewAssetPanel>("NewAssetPanel");
		if (newAssetPanel != null)
		{
			newAssetPanel.SetMode(SimulationManager.UpdateMode.NewAsset);
		}
	}

	public void LoadAsset()
	{
		NewAssetPanel newAssetPanel = UIView.get_library().ShowModal<NewAssetPanel>("NewAssetPanel");
		if (newAssetPanel != null)
		{
			newAssetPanel.SetMode(SimulationManager.UpdateMode.LoadAsset);
		}
	}

	public void ScenarioEditor()
	{
		this.m_AssetEditorOptions.Hide();
		this.m_MapEditorOptions.Hide();
		this.m_ThemeEditorOptions.Hide();
		this.m_ScenarioEditorOptions.Show();
	}

	public void ScenarioEditorNew()
	{
		UIView.get_library().ShowModal("NewScenarioPanel").Focus();
	}

	public void ScenarioEditorLoad()
	{
		UIView.get_library().ShowModal("LoadScenarioPanel").Focus();
	}

	public void Back()
	{
		MenuPanel.GoToMenu(MainMenu.m_MainMenu);
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.Back();
			p.Use();
		}
	}

	public static UIComponent m_ToolsMenu;

	private UIPanel m_MapEditorOptions;

	private UIPanel m_AssetEditorOptions;

	private UIPanel m_ThemeEditorOptions;

	private UIPanel m_ScenarioEditorOptions;
}
