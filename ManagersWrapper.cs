﻿using System;
using ColossalFramework;
using ICities;

public class ManagersWrapper : IManagers
{
	public IApplication application
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ApplicationWrapper;
		}
	}

	public IAreas areas
	{
		get
		{
			return Singleton<GameAreaManager>.get_instance().m_AreasWrapper;
		}
	}

	public IDemand demand
	{
		get
		{
			return Singleton<ZoneManager>.get_instance().m_DemandWrapper;
		}
	}

	public IEconomy economy
	{
		get
		{
			return Singleton<EconomyManager>.get_instance().m_EconomyWrapper;
		}
	}

	public ILevelUp levelUp
	{
		get
		{
			return Singleton<BuildingManager>.get_instance().m_LevelUpWrapper;
		}
	}

	public IMilestones milestones
	{
		get
		{
			return Singleton<UnlockManager>.get_instance().m_MilestonesWrapper;
		}
	}

	public ISerializableData serializableData
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_SerializableDataWrapper;
		}
	}

	public ITerrain terrain
	{
		get
		{
			return Singleton<TerrainManager>.get_instance().m_TerrainWrapper;
		}
	}

	public IThreading threading
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ThreadingWrapper;
		}
	}

	public ILoading loading
	{
		get
		{
			return Singleton<LoadingManager>.get_instance().m_LoadingWrapper;
		}
	}

	public IDisaster disaster
	{
		get
		{
			return Singleton<DisasterManager>.get_instance().m_DisasterWrapper;
		}
	}

	public IEffects effects
	{
		get
		{
			return Singleton<EffectManager>.get_instance().m_EffectsWrapper;
		}
	}

	public IBuilding building
	{
		get
		{
			return Singleton<BuildingManager>.get_instance().m_BuildingWrapper;
		}
	}

	public IResource resource
	{
		get
		{
			return Singleton<NaturalResourceManager>.get_instance().m_ResourceWrapper;
		}
	}
}
