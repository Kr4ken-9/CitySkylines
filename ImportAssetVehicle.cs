﻿using System;
using UnityEngine;

public class ImportAssetVehicle : ImportAssetLodded
{
	public ImportAssetVehicle(GameObject template, PreviewCamera camera) : base(template, camera)
	{
		this.m_LodTriangleTarget = 70;
	}

	protected override void InitializeObject()
	{
		VehicleInfo component = this.m_TemplateObject.GetComponent<VehicleInfo>();
		VehicleInfo component2 = this.m_Object.GetComponent<VehicleInfo>();
		ImportAsset.CopyComponent<VehicleAI>(component.m_vehicleAI, this.m_Object);
		component2.InitializePrefab();
		component2.CheckReferences();
		component2.m_prefabInitialized = true;
		this.m_Object.set_layer(LayerMask.NameToLayer("Vehicles"));
	}

	protected override void CreateInfo()
	{
		VehicleInfo component = this.m_TemplateObject.GetComponent<VehicleInfo>();
		VehicleInfo vehicleInfo = ImportAsset.CopyComponent<VehicleInfo>(component, this.m_Object);
		vehicleInfo.m_generatedInfo = ScriptableObject.CreateInstance<VehicleInfoGen>();
		vehicleInfo.m_generatedInfo.set_name(this.m_Object.get_name() + " (GeneratedInfo)");
		vehicleInfo.m_lodObject = this.m_LODObject;
		vehicleInfo.m_Atlas = null;
		vehicleInfo.m_Thumbnail = string.Empty;
		vehicleInfo.CalculateGeneratedInfo();
	}

	protected override void FinalizeLOD()
	{
		base.FinalizeLOD();
		ImportAssetLodded.SetLODColors(this.m_Object);
		this.CreateInfo();
	}
}
