﻿using System;

public class RoadsGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Road;
		}
	}

	protected override bool IsServiceValid(PrefabInfo info)
	{
		if (base.isMapEditor)
		{
			return info.GetService() == this.service || info.GetService() == ItemClass.Service.PublicTransport;
		}
		if (base.isAssetEditor)
		{
			BuildingInfo buildingInfo = info as BuildingInfo;
			return (info.GetService() == this.service && (buildingInfo == null || buildingInfo.m_buildingAI.WorksAsNet())) || (info.GetService() == ItemClass.Service.PublicTransport && (info.GetSubService() == ItemClass.SubService.PublicTransportTrain || info.GetSubService() == ItemClass.SubService.PublicTransportMonorail || info.GetSubService() == ItemClass.SubService.PublicTransportCableCar)) || (info.GetService() == ItemClass.Service.Beautification && info is NetInfo && ToolsModifierControl.toolController.m_editPrefabInfo is NetInfo);
		}
		NetInfo netInfo = info as NetInfo;
		return info.GetService() == this.service && (netInfo == null || (netInfo.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None);
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (GeneratedGroupPanel.GroupFilter)3;
		}
	}

	protected override int GetCategoryOrder(string name)
	{
		if (base.isMapEditor)
		{
			switch (name)
			{
			case "RoadsSmall":
				return 0;
			case "RoadsMedium":
				return 1;
			case "RoadsLarge":
				return 2;
			case "RoadsHighway":
				return 3;
			case "RoadsIntersection":
				return 4;
			case "PublicTransportBus":
				return 5;
			case "PublicTransportMetro":
				return 6;
			case "PublicTransportTrain":
				return 7;
			case "PublicTransportShip":
				return 8;
			case "PublicTransportPlane":
				return 9;
			case "PublicTransportTaxi":
				return 10;
			case "PublicTransportTram":
				return 11;
			case "PublicTransportMonorail":
				return 12;
			case "PublicTransportCableCar":
				return 13;
			}
			return 2147483647;
		}
		if (base.isAssetEditor)
		{
			switch (name)
			{
			case "RoadsSmall":
				return 0;
			case "RoadsMedium":
				return 1;
			case "RoadsLarge":
				return 2;
			case "RoadsHighway":
				return 3;
			case "PublicTransportTram":
				return 4;
			case "PublicTransportTrain":
				return 5;
			case "PublicTransportMonorail":
				return 6;
			case "PublicTransportCableCar":
				return 7;
			case "RoadsIntersection":
				return 8;
			}
			return 2147483647;
		}
		if (name != null)
		{
			if (name == "RoadsSmall")
			{
				return 0;
			}
			if (name == "RoadsMedium")
			{
				return 1;
			}
			if (name == "RoadsLarge")
			{
				return 2;
			}
			if (name == "RoadsHighway")
			{
				return 3;
			}
			if (name == "RoadsIntersection")
			{
				return 4;
			}
			if (name == "RoadsMaintenance")
			{
				return 5;
			}
		}
		return 2147483647;
	}
}
