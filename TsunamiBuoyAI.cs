﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Math;
using UnityEngine;

public class TsunamiBuoyAI : PlayerBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		if (infoMode != InfoManager.InfoMode.DisasterDetection)
		{
			return base.GetColor(buildingID, ref data, infoMode);
		}
		if (Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.Default && Singleton<InfoManager>.get_instance().CurrentSubMode != InfoManager.SubInfoMode.WaterPower)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_inactiveColor;
	}

	public override void GetImmaterialResourceRadius(ushort buildingID, ref Building data, out ImmaterialResourceManager.Resource resource1, out float radius1, out ImmaterialResourceManager.Resource resource2, out float radius2)
	{
		resource1 = ImmaterialResourceManager.Resource.None;
		radius1 = 0f;
		resource2 = ImmaterialResourceManager.Resource.None;
		radius2 = 0f;
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.DisasterDetection;
		subMode = InfoManager.SubInfoMode.WaterPower;
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainHappiness, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseHappiness, position, 1.5f);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		base.StartTransfer(buildingID, ref data, material, offer);
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		base.BuildingDeactivated(buildingID, ref data);
		InstanceID id = default(InstanceID);
		id.Building = buildingID;
		Singleton<InstanceManager>.get_instance().SetGroup(id, null);
	}

	protected override void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		if (finalProductionRate == 0)
		{
			return;
		}
		float num = Singleton<TerrainManager>.get_instance().WaterLevel(VectorUtils.XZ(buildingData.m_position));
		float num2 = Mathf.Max(1000f / Mathf.Max(0.01f, (float)finalProductionRate), 10f);
		InstanceID instanceID = default(InstanceID);
		instanceID.Building = buildingID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(instanceID);
		if (group != null)
		{
			DisasterManager instance = Singleton<DisasterManager>.get_instance();
			ushort num3 = group.m_ownerInstance.Disaster;
			if (num3 != 0)
			{
				DisasterInfo info = instance.m_disasters.m_buffer[(int)num3].Info;
				if (info.m_disasterAI is TsunamiAI)
				{
					float num4 = 0f;
					if (!info.m_disasterAI.CanAffectAt(num3, ref instance.m_disasters.m_buffer[(int)num3], buildingData.m_position, out num4))
					{
						num3 = 0;
					}
				}
				else
				{
					num3 = 0;
				}
			}
			if (num3 == 0 || num < buildingData.m_position.y + num2)
			{
				Singleton<InstanceManager>.get_instance().SetGroup(instanceID, null);
			}
		}
		if (num >= buildingData.m_position.y + num2)
		{
			DisasterManager instance2 = Singleton<DisasterManager>.get_instance();
			ushort num5 = instance2.FindDisaster<TsunamiAI>(buildingData.m_position);
			if (num5 != 0)
			{
				InstanceID srcID = default(InstanceID);
				srcID.Disaster = num5;
				Singleton<InstanceManager>.get_instance().CopyGroup(srcID, instanceID);
				instance2.DetectDisaster(num5, true);
			}
		}
		base.HandleDead(buildingID, ref buildingData, ref behaviour, totalWorkerCount);
	}

	public override bool CollapseBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly, bool demolish, int burnAmount)
	{
		return demolish && base.CollapseBuilding(buildingID, ref data, group, testOnly, demolish, burnAmount);
	}

	public override bool BurnBuilding(ushort buildingID, ref Building data, InstanceManager.Group group, bool testOnly)
	{
		return false;
	}

	public override bool EnableNotUsedGuide()
	{
		return true;
	}

	public override float ElectricityGridRadius()
	{
		if (this.m_electricityConsumption == 0)
		{
			return 0f;
		}
		return base.ElectricityGridRadius();
	}

	protected override bool CanSufferFromFlood(out bool onlyCollapse)
	{
		onlyCollapse = false;
		return false;
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return false;
	}

	public override string GetLocalizedTooltip()
	{
		string text = LocaleFormatter.FormatGeneric("AIINFO_WATER_CONSUMPTION", new object[]
		{
			this.GetWaterConsumption() * 16
		}) + Environment.NewLine + LocaleFormatter.FormatGeneric("AIINFO_ELECTRICITY_CONSUMPTION", new object[]
		{
			this.GetElectricityConsumption() * 16
		});
		string arg_72_0 = base.GetLocalizedTooltip();
		string[] expr_59 = new string[4];
		expr_59[0] = LocaleFormatter.Info1;
		expr_59[1] = text;
		expr_59[2] = LocaleFormatter.Info2;
		return TooltipHelper.Append(arg_72_0, TooltipHelper.Format(expr_59));
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
		{
			num = 0;
		}
		else if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			num = Mathf.Min(num, 50);
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		int num2 = Mathf.RoundToInt(Mathf.Max(1000f / Mathf.Max(0.01f, (float)num), 10f));
		if (num2 >= 1000)
		{
			return LocaleFormatter.FormatGeneric("AIINFO_TSUNAMI_BUOY_DETECTION_OFF", new object[]
			{
				num2
			});
		}
		return LocaleFormatter.FormatGeneric("AIINFO_TSUNAMI_BUOY_SENSITIVITY", new object[]
		{
			num2
		});
	}

	public override bool RequireRoadAccess()
	{
		return base.RequireRoadAccess();
	}
}
