﻿using System;
using UnityEngine;

public static class TerrainMesh
{
	public static void GenerateMeshes()
	{
		TerrainMesh.m_mesh4 = TerrainMesh.GenerateMesh(30, false, false, false, false);
		TerrainMesh.m_mesh4_Z0 = TerrainMesh.GenerateMesh(30, true, false, false, false);
		TerrainMesh.m_mesh4_Z0X0 = TerrainMesh.GenerateMeshL(30);
		TerrainMesh.m_mesh4_Z0X0Z1 = TerrainMesh.GenerateMeshU(30);
		TerrainMesh.m_mesh4_Z0X0Z1X1 = TerrainMesh.GenerateMesh(30, true, true, true, true);
		TerrainMesh.m_mesh8 = TerrainMesh.GenerateMesh(15, false, false, false, false);
	}

	public static void DestroyMeshes()
	{
		if (TerrainMesh.m_mesh4 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh4);
			TerrainMesh.m_mesh4 = null;
		}
		if (TerrainMesh.m_mesh4_Z0 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh4_Z0);
			TerrainMesh.m_mesh4_Z0 = null;
		}
		if (TerrainMesh.m_mesh4_Z0X0 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh4_Z0X0);
			TerrainMesh.m_mesh4_Z0X0 = null;
		}
		if (TerrainMesh.m_mesh4_Z0X0Z1 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh4_Z0X0Z1);
			TerrainMesh.m_mesh4_Z0X0Z1 = null;
		}
		if (TerrainMesh.m_mesh4_Z0X0Z1X1 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh4_Z0X0Z1X1);
			TerrainMesh.m_mesh4_Z0X0Z1X1 = null;
		}
		if (TerrainMesh.m_mesh8 != null)
		{
			Object.Destroy(TerrainMesh.m_mesh8);
			TerrainMesh.m_mesh8 = null;
		}
	}

	public static void GetLodMesh(TerrainMesh.Flags lod, out Mesh mesh, out Quaternion rotation)
	{
		if ((lod & TerrainMesh.Flags.FullRes) != (TerrainMesh.Flags)0)
		{
			switch (lod & TerrainMesh.Flags.ConnectMask)
			{
			case TerrainMesh.Flags.ConnectPosZ:
				mesh = TerrainMesh.m_mesh4_Z0;
				rotation = Quaternion.get_identity();
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosX:
				mesh = TerrainMesh.m_mesh4_Z0;
				rotation..ctor(0f, 0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosZ | TerrainMesh.Flags.ConnectPosX:
				mesh = TerrainMesh.m_mesh4_Z0X0;
				rotation = Quaternion.get_identity();
				goto IL_223;
			case TerrainMesh.Flags.ConnectNegZ:
				mesh = TerrainMesh.m_mesh4_Z0;
				rotation..ctor(0f, 1f, 0f, 0f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosX | TerrainMesh.Flags.ConnectNegZ:
				mesh = TerrainMesh.m_mesh4_Z0X0;
				rotation..ctor(0f, 0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosZ | TerrainMesh.Flags.ConnectPosX | TerrainMesh.Flags.ConnectNegZ:
				mesh = TerrainMesh.m_mesh4_Z0X0Z1;
				rotation = Quaternion.get_identity();
				goto IL_223;
			case TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0;
				rotation..ctor(0f, -0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosZ | TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0X0;
				rotation..ctor(0f, -0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosZ | TerrainMesh.Flags.ConnectPosX | TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0X0Z1;
				rotation..ctor(0f, -0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectNegZ | TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0X0;
				rotation..ctor(0f, 1f, 0f, 0f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosZ | TerrainMesh.Flags.ConnectNegZ | TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0X0Z1;
				rotation..ctor(0f, 1f, 0f, 0f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectPosX | TerrainMesh.Flags.ConnectNegZ | TerrainMesh.Flags.ConnectNegX:
				mesh = TerrainMesh.m_mesh4_Z0X0Z1;
				rotation..ctor(0f, 0.707106769f, 0f, 0.707106769f);
				goto IL_223;
			case TerrainMesh.Flags.ConnectMask:
				mesh = TerrainMesh.m_mesh4_Z0X0Z1X1;
				rotation = Quaternion.get_identity();
				goto IL_223;
			}
			mesh = TerrainMesh.m_mesh4;
			rotation = Quaternion.get_identity();
			IL_223:;
		}
		else
		{
			mesh = TerrainMesh.m_mesh8;
			rotation = Quaternion.get_identity();
		}
	}

	private static void DegenerateHalfOfTriangles(Mesh mesh)
	{
		Bounds bounds = mesh.get_bounds();
		int[] triangles = mesh.get_triangles();
		for (int i = 0; i < triangles.Length; i += 12)
		{
			triangles[i + 1] = triangles[i];
			triangles[i + 2] = triangles[i];
			if (i + 9 < triangles.Length)
			{
				triangles[i + 10] = triangles[i + 9];
				triangles[i + 11] = triangles[i + 9];
			}
		}
		mesh.set_triangles(triangles);
		mesh.set_bounds(bounds);
	}

	private static Mesh GenerateMeshL(int resolution)
	{
		int num = resolution >> 1;
		int num2 = (num + 1) * (num + 1) + num * num * 4;
		int num3 = (num * num + num + (resolution - 1) * (resolution - 1) + (resolution - 1)) * 2 * 3;
		Vector3[] array = new Vector3[num2];
		int[] array2 = new int[num3];
		int num4 = 0;
		int num5 = 0;
		float num6 = 1f / (float)resolution;
		float num7 = 0.5f / (float)resolution;
		float num8 = -0.5f;
		for (int i = 0; i <= resolution; i++)
		{
			float num9 = -0.5f;
			if (i != 0)
			{
				num9 += num7 + num6 * (float)(resolution - i + 1);
				for (int j = resolution - i + 2; j <= resolution; j++)
				{
					array[num4++] = new Vector3(num9, 0f, num8);
					num9 += num6;
				}
				num9 = -0.5f;
				num8 += num7;
			}
			for (int k = 0; k <= resolution; k++)
			{
				if ((k + i & 1) == 1 && k + i <= resolution)
				{
					num9 += num6;
				}
				else
				{
					array[num4++] = new Vector3(num9, 0f, num8);
					num9 += num6;
					if (k != 0 && i != 0)
					{
						if ((k & 1) == 0 && (i & 1) == 0)
						{
							if (k + i <= resolution)
							{
								int num10 = num4 - num - (i >> 1) * 3 - 1;
								int num11 = num4 - 1;
								int num12 = num4 - resolution - i * 3 + 2;
								array2[num5++] = num10;
								array2[num5++] = num12 - 1;
								array2[num5++] = num11 - 1;
								array2[num5++] = num10;
								array2[num5++] = num12;
								array2[num5++] = num12 - 1;
								array2[num5++] = num10;
								array2[num5++] = num11 - 1;
								array2[num5++] = num11;
								array2[num5++] = num10;
								array2[num5++] = num11;
								array2[num5++] = num12;
							}
							else if (k + i == resolution + 2)
							{
								int num13 = num4 - num - (i >> 1) * 3 - 2;
								int num14 = num4 - 1;
								int num15 = num4 - resolution - i * 3 + 1;
								array2[num5++] = num13;
								array2[num5++] = num15 - 1;
								array2[num5++] = num14 - 2;
								array2[num5++] = num13;
								array2[num5++] = num15;
								array2[num5++] = num15 - 1;
								array2[num5++] = num13;
								array2[num5++] = num14 - 2;
								array2[num5++] = num14 - 1;
								array2[num5++] = num13;
								array2[num5++] = num13 + 1;
								array2[num5++] = num15;
							}
						}
						if (k + i >= resolution + 2)
						{
							int num16 = num4 - num - (i >> 1) - 2;
							int num17 = num4 - 1;
							int num18 = num4 - num - (i >> 1) - i - 1;
							array2[num5++] = num16;
							array2[num5++] = num18 - 1;
							array2[num5++] = num17 - 1;
							array2[num5++] = num16;
							array2[num5++] = num18;
							array2[num5++] = num18 - 1;
							array2[num5++] = num16;
							array2[num5++] = num17 - 1;
							array2[num5++] = num17;
							array2[num5++] = num16;
							array2[num5++] = num17;
							array2[num5++] = num18;
						}
					}
				}
			}
			num8 += num7;
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_triangles(array2);
		return mesh;
	}

	private static Mesh GenerateMeshU(int resolution)
	{
		int num = resolution >> 1;
		int num2 = num - 1 >> 1;
		int num3 = num - 2 >> 1;
		int num4 = (num + 1) * (num + 1) + (num * num * 5 - num >> 1);
		int num5 = (num * num * 2 - num2 * num2 - num2 - num3 * num3 - num3 + (num - 1) * (num - 1) * 2 + (num - 1) * 2) * 2 * 3 + 3;
		Vector3[] array = new Vector3[num4];
		int[] array2 = new int[num5];
		int num6 = 0;
		int num7 = 0;
		float num8 = 1f / (float)resolution;
		float num9 = 0.5f / (float)resolution;
		float num10 = -0.5f;
		for (int i = 0; i <= resolution; i++)
		{
			float num11 = -0.5f;
			if (i != 0)
			{
				int num12 = Mathf.Min(i, resolution - i + 1);
				num11 += num9 + num8 * (float)(resolution - num12 + 1);
				for (int j = resolution - num12 + 2; j <= resolution; j++)
				{
					array[num6++] = new Vector3(num11, 0f, num10);
					num11 += num8;
				}
				num11 = -0.5f;
				num10 += num9;
			}
			for (int k = 0; k <= resolution; k++)
			{
				int num13 = Mathf.Min(i, resolution - i + 1);
				if ((k + i & 1) == 1 && k + num13 <= resolution)
				{
					num11 += num8;
				}
				else
				{
					array[num6++] = new Vector3(num11, 0f, num10);
					num11 += num8;
					if (k != 0 && i != 0)
					{
						if ((k & 1) == 0 && (i & 1) == 0)
						{
							if (k + num13 <= resolution)
							{
								int num14;
								int num15;
								int num16;
								if (i < num)
								{
									num14 = num6 - num - (i >> 1) * 3 - 1;
									num15 = num6 - 1;
									num16 = num6 - resolution - i * 3 + 2;
								}
								else if (i > num + 1)
								{
									num14 = num6 - num - (num13 >> 1) * 3 - 3;
									num15 = num6 - 1;
									num16 = num6 - resolution - num13 * 3 - 2;
								}
								else
								{
									num14 = num6 - num - (num13 >> 1) * 3 - 3;
									num15 = num6 - 1;
									num16 = num6 - resolution - num13 * 3;
								}
								array2[num7++] = num14;
								array2[num7++] = num16 - 1;
								array2[num7++] = num15 - 1;
								array2[num7++] = num14;
								array2[num7++] = num16;
								array2[num7++] = num16 - 1;
								array2[num7++] = num14;
								array2[num7++] = num15 - 1;
								array2[num7++] = num15;
								array2[num7++] = num14;
								array2[num7++] = num15;
								array2[num7++] = num16;
							}
							else if (k + num13 <= resolution + 2)
							{
								if (i < num)
								{
									int num17 = num6 - num - (i >> 1) * 3 - 2;
									int num18 = num6 - 1;
									int num19 = num6 - resolution - i * 3 + 1;
									array2[num7++] = num17;
									array2[num7++] = num19 - 1;
									array2[num7++] = num18 - 2;
									array2[num7++] = num17;
									array2[num7++] = num19;
									array2[num7++] = num19 - 1;
									array2[num7++] = num17;
									array2[num7++] = num18 - 2;
									array2[num7++] = num18 - 1;
									array2[num7++] = num17;
									array2[num7++] = num17 + 1;
									array2[num7++] = num19;
								}
								else if (i > num + 1)
								{
									int num17 = num6 - num - (num13 >> 1) * 3 - 3;
									int num18 = num6 - 1;
									int num19 = num6 - resolution - num13 * 3 - 1;
									array2[num7++] = num17;
									array2[num7++] = num19 - 2;
									array2[num7++] = num18 - 1;
									array2[num7++] = num17;
									array2[num7++] = num19 - 1;
									array2[num7++] = num19 - 2;
									array2[num7++] = num17;
									array2[num7++] = num18 - 1;
									array2[num7++] = num18;
									array2[num7++] = num17;
									array2[num7++] = num18;
									array2[num7++] = num17 + 1;
								}
								else
								{
									int num17 = num6 - num - (num13 >> 1) * 3 - 3;
									int num18 = num6 - 1;
									int num19 = num6 - resolution - num13 * 3;
									array2[num7++] = num17;
									array2[num7++] = num19 - 1;
									array2[num7++] = num18 - 1;
									array2[num7++] = num17;
									array2[num7++] = num19;
									array2[num7++] = num19 - 1;
									array2[num7++] = num17;
									array2[num7++] = num18 - 1;
									array2[num7++] = num18;
									array2[num7++] = num17;
									array2[num7++] = num18;
									array2[num7++] = num17 + 1;
									array2[num7++] = num17;
									array2[num7++] = num17 + 1;
									array2[num7++] = num19;
								}
							}
						}
						if (k + num13 >= resolution + 2)
						{
							int num20;
							int num21;
							int num22;
							if (i < num)
							{
								num20 = num6 - num - (i >> 1) - 2;
								num21 = num6 - 1;
								num22 = num6 - num - (i >> 1) - i - 1;
							}
							else
							{
								num20 = num6 - num - (num13 - 1 >> 1) - 2;
								num21 = num6 - 1;
								num22 = num6 - num - (num13 - 1 >> 1) - num13 - 1;
							}
							array2[num7++] = num20;
							array2[num7++] = num22 - 1;
							array2[num7++] = num21 - 1;
							array2[num7++] = num20;
							array2[num7++] = num22;
							array2[num7++] = num22 - 1;
							array2[num7++] = num20;
							array2[num7++] = num21 - 1;
							array2[num7++] = num21;
							array2[num7++] = num20;
							array2[num7++] = num21;
							array2[num7++] = num22;
						}
					}
				}
			}
			num10 += num9;
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_triangles(array2);
		return mesh;
	}

	private static Mesh GenerateMesh(int resolution, bool wideZ0, bool wideX0, bool wideZ1, bool wideX1)
	{
		int num = (resolution + 1) * (resolution + 1) + resolution * resolution;
		int num2 = resolution * resolution * 4 * 3;
		if (wideZ0)
		{
			num2 -= (resolution >> 1) * 5 * 3;
		}
		if (wideX0)
		{
			num2 -= (resolution >> 1) * 5 * 3;
		}
		if (wideZ1)
		{
			num2 -= (resolution >> 1) * 5 * 3;
		}
		if (wideX1)
		{
			num2 -= (resolution >> 1) * 5 * 3;
		}
		if (wideZ0 && wideX0)
		{
			num2 += 6;
		}
		if (wideZ0 && wideX1)
		{
			num2 += 6;
		}
		if (wideZ1 && wideX0)
		{
			num2 += 6;
		}
		if (wideZ1 && wideX1)
		{
			num2 += 6;
		}
		Vector3[] array = new Vector3[num];
		int[] array2 = new int[num2];
		int num3 = 0;
		int num4 = 0;
		float num5 = 1f / (float)resolution;
		float num6 = 0.5f / (float)resolution;
		float num7 = -0.5f;
		for (int i = 0; i <= resolution; i++)
		{
			float num8 = -0.5f;
			if (i != 0)
			{
				num8 += num6;
				for (int j = 1; j <= resolution; j++)
				{
					array[num3++] = new Vector3(num8, 0f, num7);
					num8 += num5;
				}
				num8 = -0.5f;
				num7 += num6;
			}
			for (int k = 0; k <= resolution; k++)
			{
				array[num3++] = new Vector3(num8, 0f, num7);
				num8 += num5;
				if (k != 0 && i != 0)
				{
					int num9 = num3 - resolution - 2;
					int num10 = num3 - 1;
					int num11 = num3 - (resolution << 1) - 2;
					int num12 = num3 - (resolution << 2) - 3;
					if (wideZ0 && i == 1)
					{
						if ((k & 1) == 0)
						{
							if (!wideX0 || k != 2)
							{
								array2[num4++] = num10 - 1;
								array2[num4++] = num11 - 2;
								array2[num4++] = num10 - 2;
							}
							array2[num4++] = num10 - 1;
							array2[num4++] = num11;
							array2[num4++] = num11 - 2;
							if (!wideX1 || k != resolution)
							{
								array2[num4++] = num10 - 1;
								array2[num4++] = num10;
								array2[num4++] = num11;
							}
						}
					}
					else if (wideX0 && k == 1)
					{
						if ((i & 1) == 0)
						{
							if (!wideZ0 || i != 2)
							{
								array2[num4++] = num11;
								array2[num4++] = num12;
								array2[num4++] = num12 - 1;
							}
							array2[num4++] = num11;
							array2[num4++] = num12 - 1;
							array2[num4++] = num10 - 1;
							if (!wideZ1 || i != resolution)
							{
								array2[num4++] = num11;
								array2[num4++] = num10 - 1;
								array2[num4++] = num10;
							}
						}
					}
					else if ((wideZ1 && i == resolution) || (wideX1 && k == resolution))
					{
						if (wideZ1 && i == resolution && (k & 1) == 0)
						{
							if (!wideX0 || k != 2)
							{
								array2[num4++] = num11 - 1;
								array2[num4++] = num11 - 2;
								array2[num4++] = num10 - 2;
							}
							array2[num4++] = num11 - 1;
							array2[num4++] = num10 - 2;
							array2[num4++] = num10;
							if (!wideX1 || k != resolution)
							{
								array2[num4++] = num11 - 1;
								array2[num4++] = num10;
								array2[num4++] = num11;
							}
						}
						if (wideX1 && k == resolution && (i & 1) == 0)
						{
							if (!wideZ0 || i != 2)
							{
								array2[num4++] = num11 - 1;
								array2[num4++] = num12;
								array2[num4++] = num12 - 1;
							}
							array2[num4++] = num11 - 1;
							array2[num4++] = num10;
							array2[num4++] = num12;
							if (!wideZ1 || i != resolution)
							{
								array2[num4++] = num11 - 1;
								array2[num4++] = num10 - 1;
								array2[num4++] = num10;
							}
						}
					}
					else
					{
						array2[num4++] = num9;
						array2[num4++] = num11 - 1;
						array2[num4++] = num10 - 1;
						array2[num4++] = num9;
						array2[num4++] = num11;
						array2[num4++] = num11 - 1;
						array2[num4++] = num9;
						array2[num4++] = num10 - 1;
						array2[num4++] = num10;
						array2[num4++] = num9;
						array2[num4++] = num10;
						array2[num4++] = num11;
					}
				}
			}
			num7 += num6;
		}
		Mesh mesh = new Mesh();
		mesh.set_vertices(array);
		mesh.set_triangles(array2);
		return mesh;
	}

	private static Mesh m_mesh4;

	private static Mesh m_mesh4_Z0;

	private static Mesh m_mesh4_Z0X0;

	private static Mesh m_mesh4_Z0X0Z1;

	private static Mesh m_mesh4_Z0X0Z1X1;

	private static Mesh m_mesh8;

	[Flags]
	public enum Flags
	{
		SubDivide = 512,
		FullRes = 1024,
		HalfRes = 2048,
		ConnectPosZ = 1,
		ConnectPosX = 2,
		ConnectNegZ = 4,
		ConnectNegX = 8,
		ConnectMask = 15
	}
}
