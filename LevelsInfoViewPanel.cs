﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class LevelsInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_ResidentialChart = base.Find<UIRadialChart>("ResidentialChart");
		this.m_CommercialChart = base.Find<UIRadialChart>("CommercialChart");
		this.m_OfficesChart = base.Find<UIRadialChart>("OfficesChart");
		this.m_IndustrialChart = base.Find<UIRadialChart>("IndustrialChart");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("ResidentialGradient");
				UITextureSprite uITextureSprite2 = base.Find<UITextureSprite>("CommercialGradient");
				UITextureSprite uITextureSprite3 = base.Find<UITextureSprite>("OfficesGradient");
				UITextureSprite uITextureSprite4 = base.Find<UITextureSprite>("IndustrialGradient");
				if (Singleton<InfoManager>.get_exists() && Singleton<ZoneManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					Color color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[2];
					Color color2 = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[3];
					Color color3 = Color.Lerp(color, color2, 0.5f) * 0.5f;
					for (int i = 0; i < 5; i++)
					{
						UIRadialChart.SliceSettings slice = this.m_ResidentialChart.GetSlice(i);
						Color color4 = Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, color3, 0.2f + (float)i * 0.2f);
						color4.a = 1f;
						slice.set_innerColor(color4);
						slice.set_outterColor(color4);
					}
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", color3);
					uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.2f);
					uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1f);
					uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0.2f);
					uITextureSprite2.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[4];
					color2 = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[5];
					color3 = Color.Lerp(color, color2, 0.5f) * 0.5f;
					for (int j = 0; j < 3; j++)
					{
						UIRadialChart.SliceSettings slice2 = this.m_CommercialChart.GetSlice(j);
						Color color5 = Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, color3, 0.333f + (float)j * 0.333f);
						color5.a = 1f;
						slice2.set_innerColor(color5);
						slice2.set_outterColor(color5);
					}
					uITextureSprite2.get_renderMaterial().SetColor("_ColorB", color3);
					uITextureSprite2.get_renderMaterial().SetFloat("_Step", 0.333333343f);
					uITextureSprite2.get_renderMaterial().SetFloat("_Scalar", 1f);
					uITextureSprite2.get_renderMaterial().SetFloat("_Offset", 0.33f);
					uITextureSprite3.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite3.get_renderMaterial().SetColor("_ColorB", Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7] * 0.5f);
					uITextureSprite3.get_renderMaterial().SetFloat("_Step", 0.333333343f);
					uITextureSprite3.get_renderMaterial().SetFloat("_Scalar", 1f);
					uITextureSprite3.get_renderMaterial().SetFloat("_Offset", 0.33f);
					for (int k = 0; k < 3; k++)
					{
						UIRadialChart.SliceSettings slice3 = this.m_OfficesChart.GetSlice(k);
						Color color6 = Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7] * 0.5f, 0.333f + (float)k * 0.333f);
						color6.a = 1f;
						slice3.set_innerColor(color6);
						slice3.set_outterColor(color6);
					}
					uITextureSprite4.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite4.get_renderMaterial().SetColor("_ColorB", Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f);
					uITextureSprite4.get_renderMaterial().SetFloat("_Step", 0.333333343f);
					uITextureSprite4.get_renderMaterial().SetFloat("_Scalar", 1f);
					uITextureSprite4.get_renderMaterial().SetFloat("_Offset", 0.33f);
					for (int l = 0; l < 3; l++)
					{
						UIRadialChart.SliceSettings slice4 = this.m_IndustrialChart.GetSlice(l);
						Color color7 = Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f, 0.333f + (float)l * 0.333f);
						color7.a = 1f;
						slice4.set_innerColor(color7);
						slice4.set_outterColor(color7);
					}
				}
			}
			this.m_initialized = true;
		}
		int[] array = new int[5];
		int[] array2 = new int[3];
		int[] array3 = new int[3];
		int[] array4 = new int[3];
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		instance.m_districts.m_buffer[0].m_residentialData.GetLevelPercentages(array);
		instance.m_districts.m_buffer[0].m_commercialData.GetLevelPercentages(array2);
		instance.m_districts.m_buffer[0].m_industrialData.GetLevelPercentages(array4);
		instance.m_districts.m_buffer[0].m_officeData.GetLevelPercentages(array3);
		this.m_ResidentialChart.SetValues(array);
		this.m_CommercialChart.SetValues(array2);
		this.m_OfficesChart.SetValues(array3);
		this.m_IndustrialChart.SetValues(array4);
	}

	private UIRadialChart m_ResidentialChart;

	private UIRadialChart m_CommercialChart;

	private UIRadialChart m_OfficesChart;

	private UIRadialChart m_IndustrialChart;

	private bool m_initialized;
}
