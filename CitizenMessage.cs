﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.IO;
using ColossalFramework.Math;

public class CitizenMessage : MessageBase
{
	public CitizenMessage(uint senderID, string messageID, string keyID, string tag)
	{
		this.m_messageID = messageID;
		this.m_keyID = keyID;
		this.m_senderID = senderID;
		this.m_tag = tag;
	}

	public override uint GetSenderID()
	{
		return this.m_senderID;
	}

	public override string GetSenderName()
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		string text = instance.GetCitizenName(this.m_senderID);
		if (text == null)
		{
			text = instance.GetDefaultCitizenName(this.m_senderID);
		}
		return text;
	}

	public override string GetText()
	{
		uint num = Locale.Count(this.m_messageID, this.m_keyID);
		Randomizer randomizer;
		randomizer..ctor(this.m_senderID);
		string text = Locale.Get(this.m_messageID, this.m_keyID, randomizer.Int32(num));
		if (!string.IsNullOrEmpty(this.m_tag))
		{
			string text2 = this.m_tag.Replace(" ", string.Empty);
			text = StringUtils.SafeFormat(text, new object[]
			{
				this.m_tag,
				text2
			});
		}
		return text;
	}

	public override bool IsSimilarMessage(MessageBase other)
	{
		CitizenMessage citizenMessage = other as CitizenMessage;
		return citizenMessage != null && citizenMessage.m_messageID == this.m_messageID && citizenMessage.m_keyID == this.m_keyID;
	}

	public override void Serialize(DataSerializer s)
	{
		s.WriteSharedString(this.m_messageID);
		s.WriteSharedString(this.m_keyID);
		s.WriteUniqueString(this.m_tag);
		s.WriteUInt32(this.m_senderID);
	}

	public override void Deserialize(DataSerializer s)
	{
		this.m_messageID = s.ReadSharedString();
		if (s.get_version() >= 165u)
		{
			this.m_keyID = s.ReadSharedString();
		}
		else
		{
			this.m_keyID = null;
		}
		if (s.get_version() >= 251u)
		{
			this.m_tag = s.ReadUniqueString();
		}
		else
		{
			this.m_tag = null;
		}
		this.m_senderID = s.ReadUInt32();
	}

	public override void AfterDeserialize(DataSerializer s)
	{
	}

	public string m_messageID;

	public string m_keyID;

	public string m_tag;

	public uint m_senderID;
}
