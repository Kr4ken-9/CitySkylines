﻿using System;
using UnityEngine;

public interface IRenderableManager
{
	string GetName();

	DrawCallData GetDrawCallData();

	void BeginRendering(RenderManager.CameraInfo cameraInfo);

	void EndRendering(RenderManager.CameraInfo cameraInfo);

	void BeginOverlay(RenderManager.CameraInfo cameraInfo);

	void EndOverlay(RenderManager.CameraInfo cameraInfo);

	void UndergroundOverlay(RenderManager.CameraInfo cameraInfo);

	void CheckReferences();

	void InitRenderData();

	bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays);

	void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps);
}
