﻿using System;
using ColossalFramework.UI;

public class UndoTerrainOptionPanel : ToolsModifierControl
{
	private void Awake()
	{
		this.m_TerrainTool = ToolsModifierControl.GetTool<TerrainTool>();
		this.m_ApplyButton = base.Find<UIButton>("Apply");
		this.Hide();
	}

	public void Show()
	{
		base.get_component().Show();
	}

	public void Hide()
	{
		base.get_component().Hide();
	}

	public void Update()
	{
		this.m_ApplyButton.set_isEnabled(this.m_TerrainTool != null && this.m_TerrainTool.IsUndoAvailable());
	}

	public void UndoTerrain()
	{
		if (this.m_TerrainTool != null && this.m_TerrainTool.IsUndoAvailable())
		{
			this.m_TerrainTool.Undo();
		}
	}

	private TerrainTool m_TerrainTool;

	private UIButton m_ApplyButton;
}
