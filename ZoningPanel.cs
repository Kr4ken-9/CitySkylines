﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;

public sealed class ZoningPanel : GeneratedScrollPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.None;
		}
	}

	public override UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 2;
		}
	}

	public static bool IsZoningPossible()
	{
		bool flag = ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Zoning);
		if (flag)
		{
			for (int i = 0; i < ZoningPanel.kZones.Length; i++)
			{
				if (ToolsModifierControl.IsUnlocked(ZoningPanel.kZones[i].get_enumValue()))
				{
					return true;
				}
			}
		}
		return false;
	}

	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < ZoningPanel.kZones.Length; i++)
		{
			this.SpawnEntry(ZoningPanel.kZones[i].get_enumName(), i);
		}
	}

	protected override void OnButtonClicked(UIComponent comp)
	{
		ZoneTool zoneTool = ToolsModifierControl.SetTool<ZoneTool>();
		if (zoneTool != null)
		{
			base.ShowZoningOptionPanel();
			zoneTool.m_zone = ZoningPanel.kZones[comp.get_zOrder()].get_enumValue();
		}
	}

	private void SpawnEntry(string name, int index)
	{
		bool flag = ToolsModifierControl.IsUnlocked(ZoningPanel.kZones[index].get_enumValue());
		string text = TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Title,
			ZoningPanel.kZones[index].GetLocalizedName(),
			LocaleFormatter.Sprite,
			name,
			LocaleFormatter.Text,
			ZoningPanel.kZones[index].GetLocalizedDescription(),
			LocaleFormatter.Locked,
			(!flag).ToString()
		});
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestone = Singleton<UnlockManager>.get_instance().m_properties.m_ZoneMilestones[(int)ZoningPanel.kZones[index].get_enumValue()];
			string text2;
			string text3;
			string text4;
			string text5;
			string text6;
			ToolsModifierControl.GetUnlockingInfo(milestone, out text2, out text3, out text4, out text5, out text6);
			string text7 = TooltipHelper.Format(new string[]
			{
				LocaleFormatter.LockedInfo,
				text6,
				LocaleFormatter.UnlockDesc,
				text2,
				LocaleFormatter.UnlockPopulationProgressText,
				text5,
				LocaleFormatter.UnlockPopulationTarget,
				text4,
				LocaleFormatter.UnlockPopulationCurrent,
				text3
			});
			text = TooltipHelper.Append(text, text7);
		}
		this.SpawnEntry(name, text, "Zoning" + name, null, GeneratedPanel.tooltipBox, flag).set_objectUserData(index);
	}

	public override void OnTooltipHover(UIComponent comp, UIMouseEventParameter p)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			UIButton uIButton = p.get_source() as UIButton;
			if (uIButton != null && uIButton.get_tooltipBox().get_isVisible())
			{
				int num = (int)uIButton.get_objectUserData();
				MilestoneInfo milestone = Singleton<UnlockManager>.get_instance().m_properties.m_ZoneMilestones[(int)ZoningPanel.kZones[num].get_enumValue()];
				string text;
				string text2;
				string text3;
				string text4;
				string text5;
				ToolsModifierControl.GetUnlockingInfo(milestone, out text, out text2, out text3, out text4, out text5);
				uIButton.set_tooltip(TooltipHelper.Replace(uIButton.get_tooltip(), new string[]
				{
					LocaleFormatter.UnlockDesc,
					text,
					LocaleFormatter.Locked,
					text5,
					LocaleFormatter.LockedInfo,
					text5,
					LocaleFormatter.UnlockPopulationProgressText,
					text4,
					LocaleFormatter.UnlockPopulationCurrent,
					text2
				}));
				uIButton.RefreshTooltip();
			}
		}
	}

	private static readonly PositionData<ItemClass.Zone>[] kZones = Utils.GetOrderedEnumData<ItemClass.Zone>();
}
