﻿using System;

public sealed class ForestGroupPanel : GeneratedGroupPanel
{
	protected override bool IsServiceValid(PrefabInfo info)
	{
		return true;
	}

	protected override bool CustomRefreshPanel()
	{
		base.DefaultGroup("Forest");
		return true;
	}
}
