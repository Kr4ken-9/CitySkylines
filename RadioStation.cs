﻿using System;

[Serializable]
public class RadioStation
{
	public string message
	{
		get
		{
			return this.m_message;
		}
	}

	public string name
	{
		get
		{
			return this.m_name;
		}
	}

	public string m_name;

	public string m_message;

	public string m_spriteName;
}
