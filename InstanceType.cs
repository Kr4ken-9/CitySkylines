﻿using System;

public enum InstanceType : byte
{
	Building = 1,
	Vehicle,
	District,
	Citizen,
	NetNode,
	NetSegment,
	ParkedVehicle,
	TransportLine,
	CitizenInstance,
	Prop,
	Tree,
	Event,
	NetLane,
	BuildingProp,
	NetLaneProp,
	Disaster,
	Lightning,
	RadioChannel,
	RadioContent
}
