﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using ColossalFramework.PlatformServices;
using ColossalFramework.Threading;
using UnityEngine;

public class BuildingManager : SimulationManagerBase<BuildingManager, BuildingProperties>, ISimulationManager, IRenderableManager, IAudibleManager, ITerrainManager
{
	public event BuildingManager.BuildingCreatedHandler EventBuildingCreated
	{
		add
		{
			BuildingManager.BuildingCreatedHandler buildingCreatedHandler = this.EventBuildingCreated;
			BuildingManager.BuildingCreatedHandler buildingCreatedHandler2;
			do
			{
				buildingCreatedHandler2 = buildingCreatedHandler;
				buildingCreatedHandler = Interlocked.CompareExchange<BuildingManager.BuildingCreatedHandler>(ref this.EventBuildingCreated, (BuildingManager.BuildingCreatedHandler)Delegate.Combine(buildingCreatedHandler2, value), buildingCreatedHandler);
			}
			while (buildingCreatedHandler != buildingCreatedHandler2);
		}
		remove
		{
			BuildingManager.BuildingCreatedHandler buildingCreatedHandler = this.EventBuildingCreated;
			BuildingManager.BuildingCreatedHandler buildingCreatedHandler2;
			do
			{
				buildingCreatedHandler2 = buildingCreatedHandler;
				buildingCreatedHandler = Interlocked.CompareExchange<BuildingManager.BuildingCreatedHandler>(ref this.EventBuildingCreated, (BuildingManager.BuildingCreatedHandler)Delegate.Remove(buildingCreatedHandler2, value), buildingCreatedHandler);
			}
			while (buildingCreatedHandler != buildingCreatedHandler2);
		}
	}

	public event BuildingManager.BuildingRelocatedHandler EventBuildingRelocated
	{
		add
		{
			BuildingManager.BuildingRelocatedHandler buildingRelocatedHandler = this.EventBuildingRelocated;
			BuildingManager.BuildingRelocatedHandler buildingRelocatedHandler2;
			do
			{
				buildingRelocatedHandler2 = buildingRelocatedHandler;
				buildingRelocatedHandler = Interlocked.CompareExchange<BuildingManager.BuildingRelocatedHandler>(ref this.EventBuildingRelocated, (BuildingManager.BuildingRelocatedHandler)Delegate.Combine(buildingRelocatedHandler2, value), buildingRelocatedHandler);
			}
			while (buildingRelocatedHandler != buildingRelocatedHandler2);
		}
		remove
		{
			BuildingManager.BuildingRelocatedHandler buildingRelocatedHandler = this.EventBuildingRelocated;
			BuildingManager.BuildingRelocatedHandler buildingRelocatedHandler2;
			do
			{
				buildingRelocatedHandler2 = buildingRelocatedHandler;
				buildingRelocatedHandler = Interlocked.CompareExchange<BuildingManager.BuildingRelocatedHandler>(ref this.EventBuildingRelocated, (BuildingManager.BuildingRelocatedHandler)Delegate.Remove(buildingRelocatedHandler2, value), buildingRelocatedHandler);
			}
			while (buildingRelocatedHandler != buildingRelocatedHandler2);
		}
	}

	public event BuildingManager.BuildingReleasedHandler EventBuildingReleased
	{
		add
		{
			BuildingManager.BuildingReleasedHandler buildingReleasedHandler = this.EventBuildingReleased;
			BuildingManager.BuildingReleasedHandler buildingReleasedHandler2;
			do
			{
				buildingReleasedHandler2 = buildingReleasedHandler;
				buildingReleasedHandler = Interlocked.CompareExchange<BuildingManager.BuildingReleasedHandler>(ref this.EventBuildingReleased, (BuildingManager.BuildingReleasedHandler)Delegate.Combine(buildingReleasedHandler2, value), buildingReleasedHandler);
			}
			while (buildingReleasedHandler != buildingReleasedHandler2);
		}
		remove
		{
			BuildingManager.BuildingReleasedHandler buildingReleasedHandler = this.EventBuildingReleased;
			BuildingManager.BuildingReleasedHandler buildingReleasedHandler2;
			do
			{
				buildingReleasedHandler2 = buildingReleasedHandler;
				buildingReleasedHandler = Interlocked.CompareExchange<BuildingManager.BuildingReleasedHandler>(ref this.EventBuildingReleased, (BuildingManager.BuildingReleasedHandler)Delegate.Remove(buildingReleasedHandler2, value), buildingReleasedHandler);
			}
			while (buildingReleasedHandler != buildingReleasedHandler2);
		}
	}

	private static int GetAreaIndex(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, int width, int length, BuildingInfo.ZoningMode zoningMode)
	{
		int privateSubServiceIndex = ItemClass.GetPrivateSubServiceIndex(subService);
		int num;
		if (privateSubServiceIndex != -1)
		{
			num = 8 + privateSubServiceIndex;
		}
		else
		{
			num = ItemClass.GetPrivateServiceIndex(service);
		}
		num = (int)(num * 5 + level);
		if (zoningMode == BuildingInfo.ZoningMode.CornerRight)
		{
			num = num * 4 + length - 1;
			num = num * 4 + width - 1;
			num = num * 2 + 1;
		}
		else
		{
			num = num * 4 + width - 1;
			num = num * 4 + length - 1;
			num = (int)(num * 2 + zoningMode);
		}
		return num;
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_buildings = new Array16<Building>(49152u);
		this.m_updatedBuildings = new ulong[768];
		this.m_buildingGrid = new ushort[72900];
		this.m_buildingGrid2 = new ushort[2025];
		this.m_areaBuildings = new FastList<ushort>[3840];
		this.m_serviceBuildings = new FastList<ushort>[12];
		for (int i = 0; i < 12; i++)
		{
			this.m_serviceBuildings[i] = new FastList<ushort>();
		}
		this.m_outsideConnections = new FastList<ushort>();
		this.m_roadCheckNeeded = new HashSet<ushort>();
		this.m_terrainHeightUpdateNeeded = new FastList<ushort>();
		this.m_materialBlock = new MaterialPropertyBlock();
		this.ID_BuildingState = Shader.PropertyToID("_BuildingState");
		this.ID_ObjectIndex = Shader.PropertyToID("_ObjectIndex");
		this.ID_Color = Shader.PropertyToID("_Color");
		this.ID_BuildingSize = Shader.PropertyToID("_BuildingSize");
		this.ID_MainTex = Shader.PropertyToID("_MainTex");
		this.ID_XYSMap = Shader.PropertyToID("_XYSMap");
		this.ID_ACIMap = Shader.PropertyToID("_ACIMap");
		this.ID_HeightMap = Shader.PropertyToID("_HeightMap");
		this.ID_HeightMapping = Shader.PropertyToID("_HeightMapping");
		this.ID_SurfaceMapping = Shader.PropertyToID("_SurfaceMapping");
		this.ID_WaterHeightMap = Shader.PropertyToID("_WaterHeightMap");
		this.ID_WaterHeightMapping = Shader.PropertyToID("_WaterHeightMapping");
		this.ID_WaterSurfaceMapping = Shader.PropertyToID("_WaterSurfaceMapping");
		this.ID_Speed = Animator.StringToHash("Speed");
		this.ID_State = Animator.StringToHash("State");
		this.ID_BuildingLocations = Shader.PropertyToID("_BuildingLocations");
		this.ID_BuildingStates = Shader.PropertyToID("_BuildingStates");
		this.ID_ObjectIndices = Shader.PropertyToID("_ObjectIndices");
		this.ID_BuildingColors = Shader.PropertyToID("_BuildingColors");
		this.m_buildingLayer = LayerMask.NameToLayer("Buildings");
		this.m_audioGroup = new AudioGroup(3, new SavedFloat(Settings.effectAudioVolume, Settings.gameSettingsFile, DefaultSettings.effectAudioVolume, true));
		this.m_colorUpdateLock = new object();
		this.m_colorUpdateMin = 0;
		this.m_colorUpdateMax = 0;
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
		ushort num;
		this.m_buildings.CreateItem(out num);
	}

	private void OnDestroy()
	{
		if (this.m_highlightMesh != null)
		{
			Object.Destroy(this.m_highlightMesh);
			this.m_highlightMesh = null;
		}
		if (this.m_highlightMesh2 != null)
		{
			Object.Destroy(this.m_highlightMesh2);
			this.m_highlightMesh2 = null;
		}
		if (this.m_lodRgbAtlas != null)
		{
			Object.Destroy(this.m_lodRgbAtlas);
			this.m_lodRgbAtlas = null;
		}
		if (this.m_lodXysAtlas != null)
		{
			Object.Destroy(this.m_lodXysAtlas);
			this.m_lodXysAtlas = null;
		}
		if (this.m_lodAciAtlas != null)
		{
			Object.Destroy(this.m_lodAciAtlas);
			this.m_lodAciAtlas = null;
		}
		this.ReleaseRelay();
	}

	public void InitializeStyleArray(int styleCount)
	{
		this.m_styleBuildings = new Dictionary<int, FastList<ushort>>[styleCount];
		for (int i = 0; i < styleCount; i++)
		{
			this.m_styleBuildings[i] = new Dictionary<int, FastList<ushort>>(64);
		}
	}

	private void CreateRelay()
	{
		if (this.m_LevelUpWrapper == null)
		{
			this.m_LevelUpWrapper = new LevelUpWrapper(this);
		}
		if (this.m_BuildingWrapper == null)
		{
			this.m_BuildingWrapper = new BuildingWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_LevelUpWrapper != null)
		{
			this.m_LevelUpWrapper.Release();
			this.m_LevelUpWrapper = null;
		}
		if (this.m_BuildingWrapper != null)
		{
			this.m_BuildingWrapper.Release();
			this.m_BuildingWrapper = null;
		}
	}

	public override void InitializeProperties(BuildingProperties properties)
	{
		base.InitializeProperties(properties);
		if (this.m_properties.m_placementEffect != null)
		{
			this.m_properties.m_placementEffect.InitializeEffect();
		}
		if (this.m_properties.m_bulldozeEffect != null)
		{
			this.m_properties.m_bulldozeEffect.InitializeEffect();
		}
		if (this.m_properties.m_levelupEffect != null)
		{
			this.m_properties.m_levelupEffect.InitializeEffect();
		}
		if (this.m_properties.m_fireEffect != null)
		{
			this.m_properties.m_fireEffect.InitializeEffect();
		}
		if (this.m_properties.m_collapseEffect != null)
		{
			this.m_properties.m_collapseEffect.InitializeEffect();
		}
		if (this.m_properties.m_collapseFloodedEffect != null)
		{
			this.m_properties.m_collapseFloodedEffect.InitializeEffect();
		}
		if (this.m_properties.m_highlightMaterial != null)
		{
			this.m_highlightMaterial = new Material(this.m_properties.m_highlightMaterial);
		}
		if (this.m_highlightMesh == null)
		{
			this.CreateHighlightMesh();
		}
		if (this.m_highlightMesh2 == null)
		{
			this.CreateHighlightMeshCircular();
		}
		this.m_cityNameGroups = properties.m_cityNameGroups.FindGroup(Singleton<SimulationManager>.get_instance().m_metaData.m_environment);
		Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.RefreshUnlocks);
	}

	public override void DestroyProperties(BuildingProperties properties)
	{
		if (this.m_properties == properties)
		{
			if (this.m_audioGroup != null)
			{
				this.m_audioGroup.Reset();
			}
			if (this.m_properties.m_placementEffect != null)
			{
				this.m_properties.m_placementEffect.ReleaseEffect();
			}
			if (this.m_properties.m_bulldozeEffect != null)
			{
				this.m_properties.m_bulldozeEffect.ReleaseEffect();
			}
			if (this.m_properties.m_levelupEffect != null)
			{
				this.m_properties.m_levelupEffect.ReleaseEffect();
			}
			if (this.m_properties.m_fireEffect != null)
			{
				this.m_properties.m_fireEffect.ReleaseEffect();
			}
			if (this.m_properties.m_collapseEffect != null)
			{
				this.m_properties.m_collapseEffect.ReleaseEffect();
			}
			if (this.m_properties.m_collapseFloodedEffect != null)
			{
				this.m_properties.m_collapseFloodedEffect.ReleaseEffect();
			}
			if (this.m_highlightMaterial != null)
			{
				Object.Destroy(this.m_highlightMaterial);
				this.m_highlightMaterial = null;
			}
			if (this.m_lodRgbAtlas != null)
			{
				Object.Destroy(this.m_lodRgbAtlas);
				this.m_lodRgbAtlas = null;
			}
			if (this.m_lodXysAtlas != null)
			{
				Object.Destroy(this.m_lodXysAtlas);
				this.m_lodXysAtlas = null;
			}
			if (this.m_lodAciAtlas != null)
			{
				Object.Destroy(this.m_lodAciAtlas);
				this.m_lodAciAtlas = null;
			}
			this.m_cityNameGroups = null;
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.RefreshUnlocks);
		}
		base.DestroyProperties(properties);
	}

	private void RefreshUnlocks()
	{
		ToolController properties = Singleton<ToolManager>.get_instance().m_properties;
		if (properties != null && (properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			bool flag = true;
			int num = PrefabCollection<BuildingInfo>.LoadedCount();
			for (int i = 0; i < num; i++)
			{
				BuildingInfo loaded = PrefabCollection<BuildingInfo>.GetLoaded((uint)i);
				if (loaded != null)
				{
					SavedBool unlocked = loaded.m_unlocked;
					if (unlocked != null && !unlocked.get_value())
					{
						if (Singleton<UnlockManager>.get_instance().Unlocked(loaded.GetUnlockMilestone()))
						{
							unlocked.set_value(true);
							loaded.m_alreadyUnlocked = true;
						}
						else
						{
							flag = false;
						}
					}
				}
			}
			if (flag && Singleton<SimulationManager>.get_instance().m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True && !PlatformService.get_achievements().get_Item("IWantItAll").get_achieved())
			{
				PlatformService.get_achievements().get_Item("IWantItAll").Unlock();
			}
		}
	}

	public bool UpdateColorMap(Texture2D colorMap)
	{
		if (this.m_colorUpdateMax < this.m_colorUpdateMin)
		{
			return false;
		}
		if (Singleton<CoverageManager>.get_instance().WaitingForIt())
		{
			return false;
		}
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		int colorUpdateMin;
		int colorUpdateMax;
		try
		{
			colorUpdateMin = this.m_colorUpdateMin;
			colorUpdateMax = this.m_colorUpdateMax;
			this.m_colorUpdateMin = 49152;
			this.m_colorUpdateMax = -1;
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		for (int i = colorUpdateMin; i <= colorUpdateMax; i++)
		{
			if (this.m_buildings.m_buffer[i].m_flags != Building.Flags.None)
			{
				int num;
				int num2;
				RenderManager.GetColorLocation((uint)i, out num, out num2);
				BuildingInfo info = this.m_buildings.m_buffer[i].Info;
				Color color = info.m_buildingAI.GetColor((ushort)i, ref this.m_buildings.m_buffer[i], currentMode);
				colorMap.SetPixel(num, num2, color);
			}
		}
		if (colorUpdateMin == 0)
		{
			if (currentMode != InfoManager.InfoMode.None)
			{
				colorMap.SetPixel(0, 0, Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
			}
			else
			{
				colorMap.SetPixel(0, 0, Color.get_white());
			}
		}
		return true;
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		Material material = Singleton<RenderManager>.get_instance().m_groupLayerMaterials[this.m_buildingLayer];
		if (material != null)
		{
			if (material.HasProperty(this.ID_MainTex))
			{
				material.SetTexture(this.ID_MainTex, this.m_lodRgbAtlas);
			}
			if (material.HasProperty(this.ID_XYSMap))
			{
				material.SetTexture(this.ID_XYSMap, this.m_lodXysAtlas);
			}
			if (material.HasProperty(this.ID_ACIMap))
			{
				material.SetTexture(this.ID_ACIMap, this.m_lodAciAtlas);
			}
		}
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		FastList<RenderGroup> renderedGroups = Singleton<RenderManager>.get_instance().m_renderedGroups;
		for (int i = 0; i < renderedGroups.m_size; i++)
		{
			RenderGroup renderGroup = renderedGroups.m_buffer[i];
			int num = renderGroup.m_layersRendered & ~(1 << Singleton<NotificationManager>.get_instance().m_notificationLayer);
			if (renderGroup.m_instanceMask != 0)
			{
				num &= ~renderGroup.m_instanceMask;
				int num2 = renderGroup.m_x * 270 / 45;
				int num3 = renderGroup.m_z * 270 / 45;
				int num4 = (renderGroup.m_x + 1) * 270 / 45 - 1;
				int num5 = (renderGroup.m_z + 1) * 270 / 45 - 1;
				for (int j = num3; j <= num5; j++)
				{
					for (int k = num2; k <= num4; k++)
					{
						int num6 = j * 270 + k;
						ushort num7 = this.m_buildingGrid[num6];
						int num8 = 0;
						while (num7 != 0)
						{
							this.m_buildings.m_buffer[(int)num7].RenderInstance(cameraInfo, num7, num | renderGroup.m_instanceMask);
							num7 = this.m_buildings.m_buffer[(int)num7].m_nextGridBuilding;
							if (++num8 >= 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			if (num != 0)
			{
				int num9 = renderGroup.m_z * 45 + renderGroup.m_x;
				ushort num10 = this.m_buildingGrid2[num9];
				int num11 = 0;
				while (num10 != 0)
				{
					this.m_buildings.m_buffer[(int)num10].RenderInstance(cameraInfo, num10, num);
					num10 = this.m_buildings.m_buffer[(int)num10].m_nextGridBuilding2;
					if (++num11 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		int num12 = PrefabCollection<BuildingInfo>.PrefabCount();
		for (int l = 0; l < num12; l++)
		{
			BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)l);
			if (prefab != null)
			{
				prefab.UpdatePrefabInstances();
				if (prefab.m_lodCount != 0)
				{
					Building.RenderLod(cameraInfo, prefab);
				}
				if (prefab.m_subMeshes != null)
				{
					for (int m = 0; m < prefab.m_subMeshes.Length; m++)
					{
						BuildingInfoBase subInfo = prefab.m_subMeshes[m].m_subInfo;
						if (subInfo.m_lodCount != 0)
						{
							Building.RenderLod(cameraInfo, subInfo);
						}
					}
				}
			}
		}
		if (this.m_common != null && this.m_common.m_subInfos != null)
		{
			for (int n = 0; n < this.m_common.m_subInfos.m_size; n++)
			{
				BuildingInfoBase buildingInfoBase = this.m_common.m_subInfos.m_buffer[n];
				if (buildingInfoBase.m_lodCount != 0)
				{
					Building.RenderLod(cameraInfo, buildingInfoBase);
				}
			}
		}
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		InfoManager.InfoMode currentMode = Singleton<InfoManager>.get_instance().CurrentMode;
		if (currentMode == InfoManager.InfoMode.None || this.m_highlightMesh == null || this.m_highlightMaterial == null)
		{
			return;
		}
		InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
		ItemClass.Service service = ItemClass.Service.None;
		ItemClass.SubService subService = ItemClass.SubService.None;
		int num = 0;
		switch (currentMode)
		{
		case InfoManager.InfoMode.Wind:
			service = ItemClass.Service.Electricity;
			subService = ItemClass.SubService.None;
			num = 2;
			goto IL_24D;
		case InfoManager.InfoMode.Garbage:
			service = ItemClass.Service.Garbage;
			subService = ItemClass.SubService.None;
			num = 3;
			goto IL_24D;
		case InfoManager.InfoMode.BuildingLevel:
		case InfoManager.InfoMode.TerrainHeight:
			IL_8B:
			switch (currentMode)
			{
			case InfoManager.InfoMode.Electricity:
				service = ItemClass.Service.Electricity;
				subService = ItemClass.SubService.None;
				num = 0;
				goto IL_24D;
			case InfoManager.InfoMode.Water:
				service = ItemClass.Service.Water;
				subService = ItemClass.SubService.None;
				num = 1;
				goto IL_24D;
			case InfoManager.InfoMode.CrimeRate:
				service = ItemClass.Service.PoliceDepartment;
				subService = ItemClass.SubService.None;
				num = 0;
				goto IL_24D;
			case InfoManager.InfoMode.Health:
				service = ItemClass.Service.HealthCare;
				subService = ItemClass.SubService.None;
				if (currentSubMode != InfoManager.SubInfoMode.Default)
				{
					if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
					{
						num = 2;
					}
				}
				else
				{
					num = 5;
				}
				goto IL_24D;
			case InfoManager.InfoMode.Happiness:
			case InfoManager.InfoMode.Density:
			case InfoManager.InfoMode.NoisePollution:
				goto IL_24D;
			case InfoManager.InfoMode.Transport:
				service = ItemClass.Service.PublicTransport;
				subService = ItemClass.SubService.None;
				num = 0;
				goto IL_24D;
			default:
				goto IL_24D;
			}
			break;
		case InfoManager.InfoMode.FireSafety:
			service = ItemClass.Service.FireDepartment;
			subService = ItemClass.SubService.None;
			num = 1;
			goto IL_24D;
		case InfoManager.InfoMode.Education:
			service = ItemClass.Service.Education;
			subService = ItemClass.SubService.None;
			if (currentSubMode != InfoManager.SubInfoMode.Default)
			{
				if (currentSubMode != InfoManager.SubInfoMode.WaterPower)
				{
					if (currentSubMode == InfoManager.SubInfoMode.WindPower)
					{
						num = 4;
					}
				}
				else
				{
					num = 2;
				}
			}
			else
			{
				num = 1;
			}
			goto IL_24D;
		case InfoManager.InfoMode.Entertainment:
			if (currentSubMode != InfoManager.SubInfoMode.Default)
			{
				if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
				{
					service = ItemClass.Service.Monument;
				}
			}
			else
			{
				service = ItemClass.Service.Beautification;
			}
			subService = ItemClass.SubService.None;
			num = 0;
			goto IL_24D;
		case InfoManager.InfoMode.Heating:
			service = ItemClass.Service.Water;
			subService = ItemClass.SubService.None;
			num = 2;
			goto IL_24D;
		case InfoManager.InfoMode.Maintenance:
			service = ItemClass.Service.Road;
			subService = ItemClass.SubService.None;
			if (currentSubMode != InfoManager.SubInfoMode.Default)
			{
				if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
				{
					num = 1;
				}
			}
			else
			{
				num = 2;
			}
			goto IL_24D;
		case InfoManager.InfoMode.Snow:
			service = ItemClass.Service.Road;
			subService = ItemClass.SubService.None;
			if (currentSubMode != InfoManager.SubInfoMode.Default)
			{
				if (currentSubMode == InfoManager.SubInfoMode.WaterPower)
				{
					num = 1;
				}
			}
			else
			{
				num = 8;
			}
			goto IL_24D;
		case InfoManager.InfoMode.EscapeRoutes:
			service = ItemClass.Service.Disaster;
			subService = ItemClass.SubService.None;
			num = 8;
			goto IL_24D;
		case InfoManager.InfoMode.Radio:
			service = ItemClass.Service.Disaster;
			subService = ItemClass.SubService.None;
			num = 16;
			goto IL_24D;
		case InfoManager.InfoMode.Destruction:
			service = ItemClass.Service.Disaster;
			subService = ItemClass.SubService.None;
			num = 2;
			goto IL_24D;
		case InfoManager.InfoMode.DisasterDetection:
			service = ItemClass.Service.Disaster;
			subService = ItemClass.SubService.None;
			num = 4;
			goto IL_24D;
		case InfoManager.InfoMode.DisasterHazard:
			service = ItemClass.Service.Disaster;
			subService = ItemClass.SubService.None;
			num = 8;
			goto IL_24D;
		}
		goto IL_8B;
		IL_24D:
		if (service != ItemClass.Service.None)
		{
			FastList<ushort> serviceBuildings = this.GetServiceBuildings(service);
			if (serviceBuildings != null)
			{
				for (int i = 0; i < serviceBuildings.m_size; i++)
				{
					ushort num2 = serviceBuildings.m_buffer[i];
					BuildingInfo info = this.m_buildings.m_buffer[(int)num2].Info;
					if (subService == ItemClass.SubService.None || info.m_class.m_subService == subService)
					{
						if (num == 0 || (num & 1 << (int)info.m_class.m_level) != 0)
						{
							this.m_highlightMaterial.set_color(info.m_buildingAI.GetColor(num2, ref this.m_buildings.m_buffer[(int)num2], currentMode));
							int width = this.m_buildings.m_buffer[(int)num2].Width;
							int length = this.m_buildings.m_buffer[(int)num2].Length;
							Vector3 vector;
							Quaternion quaternion;
							this.m_buildings.m_buffer[(int)num2].CalculateMeshPosition(out vector, out quaternion);
							Vector3 vector2 = info.m_generatedInfo.m_max;
							Vector3 vector3 = info.m_generatedInfo.m_min;
							float num3 = (float)width * 4f;
							float num4 = (float)length * 4f;
							vector3 = Vector3.Max(vector3, new Vector3(-num3, 0f, -num4));
							vector2 = Vector3.Min(vector2, new Vector3(num3, 0f, num4));
							ushort subBuilding = this.m_buildings.m_buffer[(int)num2].m_subBuilding;
							if (subBuilding != 0)
							{
								Quaternion quaternion2 = Quaternion.Inverse(quaternion);
								int num5 = 0;
								while (subBuilding != 0)
								{
									BuildingInfo info2 = this.m_buildings.m_buffer[(int)subBuilding].Info;
									int width2 = this.m_buildings.m_buffer[(int)subBuilding].Width;
									int length2 = this.m_buildings.m_buffer[(int)subBuilding].Length;
									Vector3 vector4;
									Quaternion quaternion3;
									this.m_buildings.m_buffer[(int)subBuilding].CalculateMeshPosition(out vector4, out quaternion3);
									Vector3 vector5 = info2.m_generatedInfo.m_max;
									Vector3 vector6 = info2.m_generatedInfo.m_min;
									float num6 = (float)width2 * 4f;
									float num7 = (float)length2 * 4f;
									vector6 = Vector3.Max(vector6, new Vector3(-num6, 0f, -num7));
									vector5 = Vector3.Min(vector5, new Vector3(num6, 0f, num7));
									Vector3 vector7 = quaternion2 * (vector4 + quaternion3 * new Vector3(vector6.x, 0f, vector6.z) - vector);
									Vector3 vector8 = quaternion2 * (vector4 + quaternion3 * new Vector3(vector6.x, 0f, vector5.z) - vector);
									Vector3 vector9 = quaternion2 * (vector4 + quaternion3 * new Vector3(vector5.x, 0f, vector6.z) - vector);
									Vector3 vector10 = quaternion2 * (vector4 + quaternion3 * new Vector3(vector5.x, 0f, vector5.z) - vector);
									vector3 = Vector3.Min(vector3, vector7);
									vector3 = Vector3.Min(vector3, vector8);
									vector3 = Vector3.Min(vector3, vector9);
									vector3 = Vector3.Min(vector3, vector10);
									vector2 = Vector3.Max(vector2, vector7);
									vector2 = Vector3.Max(vector2, vector8);
									vector2 = Vector3.Max(vector2, vector9);
									vector2 = Vector3.Max(vector2, vector10);
									subBuilding = this.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
									if (++num5 > 49152)
									{
										CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
										break;
									}
								}
							}
							Vector4 vector11 = vector2 - vector3;
							if (info.m_circular)
							{
								vector11.x = Mathf.Max(vector11.x, vector11.z);
								vector11.z = vector11.x;
							}
							Vector3 vector12 = (vector2 + vector3) * 0.5f;
							vector12 = quaternion * vector12;
							vector12.y = 0f;
							vector += vector12;
							if (vector11.x * vector11.z < (float)(width * length) * 6f)
							{
								vector = this.m_buildings.m_buffer[(int)num2].m_position;
								vector11..ctor((float)width * 8f, info.m_size.y, (float)length * 8f);
							}
							vector11.w = ((!info.m_circular) ? 0f : 1f);
							this.m_highlightMaterial.SetVector(this.ID_BuildingSize, vector11);
							if (this.m_highlightMaterial.SetPass(0))
							{
								BuildingManager expr_74A_cp_0 = Singleton<BuildingManager>.get_instance();
								expr_74A_cp_0.m_drawCallData.m_overlayCalls = expr_74A_cp_0.m_drawCallData.m_overlayCalls + 1;
								if (info.m_circular)
								{
									Graphics.DrawMeshNow(this.m_highlightMesh2, vector, quaternion);
								}
								else
								{
									Graphics.DrawMeshNow(this.m_highlightMesh, vector, quaternion);
								}
							}
						}
					}
				}
			}
		}
	}

	public override void CheckReferences()
	{
		base.CheckReferences();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.CheckReferencesImpl());
	}

	[DebuggerHidden]
	private IEnumerator CheckReferencesImpl()
	{
		return new BuildingManager.<CheckReferencesImpl>c__Iterator0();
	}

	public override void InitRenderData()
	{
		base.InitRenderData();
		Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitRenderDataImpl());
	}

	[DebuggerHidden]
	private IEnumerator InitRenderDataImpl()
	{
		BuildingManager.<InitRenderDataImpl>c__Iterator1 <InitRenderDataImpl>c__Iterator = new BuildingManager.<InitRenderDataImpl>c__Iterator1();
		<InitRenderDataImpl>c__Iterator.$this = this;
		return <InitRenderDataImpl>c__Iterator;
	}

	private void CreateHighlightMesh()
	{
		Vector3[] array = new Vector3[8];
		int[] array2 = new int[24];
		int num = 0;
		int num2 = 0;
		array[num++] = new Vector3(-0.5f, 0f, -0.5f);
		array[num++] = new Vector3(-0.5f, 0f, 0.5f);
		array[num++] = new Vector3(0.5f, 0f, 0.5f);
		array[num++] = new Vector3(0.5f, 0f, -0.5f);
		array[num++] = new Vector3(-0.5f, 1f, -0.5f);
		array[num++] = new Vector3(-0.5f, 1f, 0.5f);
		array[num++] = new Vector3(0.5f, 1f, 0.5f);
		array[num++] = new Vector3(0.5f, 1f, -0.5f);
		array2[num2++] = num - 8;
		array2[num2++] = num - 4;
		array2[num2++] = num - 7;
		array2[num2++] = num - 7;
		array2[num2++] = num - 4;
		array2[num2++] = num - 3;
		array2[num2++] = num - 7;
		array2[num2++] = num - 3;
		array2[num2++] = num - 6;
		array2[num2++] = num - 6;
		array2[num2++] = num - 3;
		array2[num2++] = num - 2;
		array2[num2++] = num - 6;
		array2[num2++] = num - 2;
		array2[num2++] = num - 5;
		array2[num2++] = num - 5;
		array2[num2++] = num - 2;
		array2[num2++] = num - 1;
		array2[num2++] = num - 5;
		array2[num2++] = num - 1;
		array2[num2++] = num - 8;
		array2[num2++] = num - 8;
		array2[num2++] = num - 1;
		array2[num2++] = num - 4;
		this.m_highlightMesh = new Mesh();
		this.m_highlightMesh.set_vertices(array);
		this.m_highlightMesh.set_triangles(array2);
		this.m_highlightMesh.set_bounds(new Bounds(new Vector3(0f, 256f, 0f), new Vector3(128f, 512f, 128f)));
	}

	private void CreateHighlightMeshCircular()
	{
		int num = 32;
		float num2 = 6.28318548f / (float)num;
		int num3 = num * 2 - 1;
		Vector3[] array = new Vector3[num * 2];
		int[] array2 = new int[num * 6];
		int num4 = 0;
		int num5 = 0;
		for (int i = 0; i < num; i++)
		{
			float num6 = (float)i * num2;
			float num7 = Mathf.Cos(num6) * 0.5f;
			float num8 = Mathf.Sin(num6) * 0.5f;
			array[num4++] = new Vector3(num7, 0f, num8);
			array[num4++] = new Vector3(num7, 1f, num8);
			array2[num5++] = num4 - 2;
			array2[num5++] = num4 - 1;
			array2[num5++] = (num4 & num3);
			array2[num5++] = (num4 & num3);
			array2[num5++] = num4 - 1;
			array2[num5++] = (num4 + 1 & num3);
		}
		this.m_highlightMesh2 = new Mesh();
		this.m_highlightMesh2.set_vertices(array);
		this.m_highlightMesh2.set_triangles(array2);
		this.m_highlightMesh2.set_bounds(new Bounds(new Vector3(0f, 256f, 0f), new Vector3(128f, 512f, 128f)));
	}

	protected override void PlayAudioImpl(AudioManager.ListenerInfo listenerInfo)
	{
		if (this.m_properties != null && !Singleton<ToolManager>.get_instance().m_properties.m_mode.IsFlagSet(ItemClass.Availability.AssetEditor))
		{
			if (!Singleton<LoadingManager>.get_instance().m_currentlyLoading)
			{
				int num = Mathf.Max((int)((listenerInfo.m_position.x - 300f) / 64f + 135f), 0);
				int num2 = Mathf.Max((int)((listenerInfo.m_position.z - 300f) / 64f + 135f), 0);
				int num3 = Mathf.Min((int)((listenerInfo.m_position.x + 300f) / 64f + 135f), 269);
				int num4 = Mathf.Min((int)((listenerInfo.m_position.z + 300f) / 64f + 135f), 269);
				for (int i = num2; i <= num4; i++)
				{
					for (int j = num; j <= num3; j++)
					{
						int num5 = i * 270 + j;
						ushort num6 = this.m_buildingGrid[num5];
						int num7 = 0;
						while (num6 != 0)
						{
							this.m_buildings.m_buffer[(int)num6].PlayAudio(listenerInfo, num6);
							num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
							if (++num7 >= 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
			}
			AudioManager instance = Singleton<AudioManager>.get_instance();
			this.m_audioGroup.UpdatePlayers(listenerInfo, (!instance.MuteAll) ? instance.MasterVolume : 0f);
		}
	}

	public void CalculateServiceProximity(Vector3 position, float radius, float[] serviceProximity, float[] subServiceProximity)
	{
		int num = Mathf.Max((int)((position.x - radius) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((position.z - radius) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((position.x + radius) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((position.z + radius) / 64f + 135f), 269);
		radius *= radius;
		float num5 = 0f;
		for (int i = 0; i < serviceProximity.Length; i++)
		{
			serviceProximity[i] = 0f;
		}
		for (int j = 0; j < subServiceProximity.Length; j++)
		{
			subServiceProximity[j] = 0f;
		}
		for (int k = num2; k <= num4; k++)
		{
			float num6 = ((float)k - 135f + 0.5f) * 64f - position.z;
			num6 *= num6;
			for (int l = num; l <= num3; l++)
			{
				float num7 = ((float)l - 135f + 0.5f) * 64f - position.x;
				num7 *= num7;
				if (num6 + num7 < radius)
				{
					float num8 = 1f - (num6 + num7) / radius;
					int num9 = k * 270 + l;
					ushort num10 = this.m_buildingGrid[num9];
					int num11 = 0;
					while (num10 != 0)
					{
						if ((this.m_buildings.m_buffer[(int)num10].m_flags & Building.Flags.Active) != Building.Flags.None)
						{
							BuildingInfo info = this.m_buildings.m_buffer[(int)num10].Info;
							int width = this.m_buildings.m_buffer[(int)num10].Width;
							int length = this.m_buildings.m_buffer[(int)num10].Length;
							float num12 = (float)(width * length) * 64f * num8;
							serviceProximity[(int)info.m_class.m_service] += num12;
							subServiceProximity[(int)info.m_class.m_subService] += num12;
						}
						num10 = this.m_buildings.m_buffer[(int)num10].m_nextGridBuilding;
						if (++num11 >= 49152)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
					num5 += 4096f * num8;
				}
			}
		}
		if (num5 != 0f)
		{
			for (int m = 0; m < serviceProximity.Length; m++)
			{
				serviceProximity[m] = Mathf.Clamp01(serviceProximity[m] / num5 * 2f);
			}
			for (int n = 0; n < subServiceProximity.Length; n++)
			{
				subServiceProximity[n] = Mathf.Clamp01(subServiceProximity[n] / num5 * 2f);
			}
		}
	}

	public float SampleSmoothHeight(Vector3 worldPos, ItemClass.Layer layers)
	{
		float num = 0f;
		int num2 = Mathf.Max((int)((worldPos.x - 94f) / 64f + 135f), 0);
		int num3 = Mathf.Max((int)((worldPos.z - 94f) / 64f + 135f), 0);
		int num4 = Mathf.Min((int)((worldPos.x + 94f) / 64f + 135f), 269);
		int num5 = Mathf.Min((int)((worldPos.z + 94f) / 64f + 135f), 269);
		for (int i = num3; i <= num5; i++)
		{
			for (int j = num2; j <= num4; j++)
			{
				ushort num6 = this.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					BuildingInfo buildingInfo;
					int num8;
					int num9;
					this.m_buildings.m_buffer[(int)num6].GetInfoWidthLength(out buildingInfo, out num8, out num9);
					if ((buildingInfo.m_class.m_layer & layers) != ItemClass.Layer.None)
					{
						Vector3 position = this.m_buildings.m_buffer[(int)num6].m_position;
						Vector3 vector = worldPos - position;
						float num10 = (float)(num8 + 4) * 4f;
						float num11 = (float)(num9 + 4) * 4f;
						float num12 = num10 * num10 + num11 * num11;
						float num13 = vector.x * vector.x + vector.z * vector.z;
						if (num13 < num12)
						{
							float angle = this.m_buildings.m_buffer[(int)num6].m_angle;
							Vector3 vector2;
							vector2..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
							Vector3 vector3;
							vector3..ctor(vector2.z, 0f, -vector2.x);
							float num14 = vector.x * vector2.x + vector.z * vector2.z;
							float num15 = vector.x * vector3.x + vector.z * vector3.z;
							if (num14 > -num10 && num14 < num10 && num15 > -num11 && num15 < num11)
							{
								float num16 = Mathf.Min(Mathf.Min(num14 + num10, num10 - num14), Mathf.Min(num15 + num11, num11 - num15));
								num16 = MathUtils.SmoothClamp01(num16 * 0.0625f);
								float num17 = buildingInfo.m_collisionHeight;
								Building.Flags flags = this.m_buildings.m_buffer[(int)num6].m_flags;
								if ((flags & Building.Flags.Collapsed) != Building.Flags.None && this.m_buildings.m_buffer[(int)num6].GetLastFrameData().m_constructState == 0)
								{
									if (buildingInfo.m_collapsedInfo != null)
									{
										num17 = buildingInfo.m_collapsedInfo.m_generatedInfo.m_size.y;
									}
								}
								else if ((flags & Building.Flags.Upgrading) != Building.Flags.None)
								{
									BuildingInfo upgradeInfo = buildingInfo.m_buildingAI.GetUpgradeInfo(num6, ref this.m_buildings.m_buffer[(int)num6]);
									if (upgradeInfo != null)
									{
										num17 = Mathf.Max(num17, upgradeInfo.m_collisionHeight);
									}
								}
								num16 = Mathf.Lerp(worldPos.y, position.y + num17, num16);
								if (num16 > num)
								{
									num = num16;
								}
							}
						}
					}
					num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return num;
	}

	public bool CheckLimits()
	{
		ItemClass.Availability mode = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		if ((mode & ItemClass.Availability.MapEditor) != ItemClass.Availability.None)
		{
			if (this.m_buildingCount >= 8192)
			{
				return false;
			}
		}
		else if ((mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
		{
			if (this.m_buildingCount >= 1024)
			{
				return false;
			}
		}
		else if (this.m_buildingCount >= 48640)
		{
			return false;
		}
		return true;
	}

	public bool CreateBuilding(out ushort building, ref Randomizer randomizer, BuildingInfo info, Vector3 position, float angle, int length, uint buildIndex)
	{
		ushort num;
		if (this.m_buildings.CreateItem(out num, ref randomizer))
		{
			if (length <= 0)
			{
				length = info.m_cellLength;
			}
			building = num;
			this.m_buildings.m_buffer[(int)building].m_flags = Building.Flags.Created;
			this.m_buildings.m_buffer[(int)building].Info = info;
			this.m_buildings.m_buffer[(int)building].Width = info.m_cellWidth;
			this.m_buildings.m_buffer[(int)building].Length = length;
			this.m_buildings.m_buffer[(int)building].m_frame0 = default(Building.Frame);
			this.m_buildings.m_buffer[(int)building].m_buildIndex = buildIndex;
			this.m_buildings.m_buffer[(int)building].m_angle = angle;
			this.m_buildings.m_buffer[(int)building].m_position = position;
			this.m_buildings.m_buffer[(int)building].m_baseHeight = 0;
			this.m_buildings.m_buffer[(int)building].m_ownVehicles = 0;
			this.m_buildings.m_buffer[(int)building].m_guestVehicles = 0;
			this.m_buildings.m_buffer[(int)building].m_sourceCitizens = 0;
			this.m_buildings.m_buffer[(int)building].m_targetCitizens = 0;
			this.m_buildings.m_buffer[(int)building].m_citizenUnits = 0u;
			this.m_buildings.m_buffer[(int)building].m_netNode = 0;
			this.m_buildings.m_buffer[(int)building].m_subBuilding = 0;
			this.m_buildings.m_buffer[(int)building].m_parentBuilding = 0;
			this.m_buildings.m_buffer[(int)building].m_waterSource = 0;
			this.m_buildings.m_buffer[(int)building].m_eventIndex = 0;
			this.m_buildings.m_buffer[(int)building].m_nextGridBuilding = 0;
			this.m_buildings.m_buffer[(int)building].m_nextGridBuilding2 = 0;
			this.m_buildings.m_buffer[(int)building].m_electricityBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_waterBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_sewageBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_heatingBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_garbageBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_crimeBuffer = 0;
			this.m_buildings.m_buffer[(int)building].m_customBuffer1 = 0;
			this.m_buildings.m_buffer[(int)building].m_customBuffer2 = 0;
			this.m_buildings.m_buffer[(int)building].m_buildWaterHeight = 0;
			this.m_buildings.m_buffer[(int)building].m_productionRate = 0;
			this.m_buildings.m_buffer[(int)building].m_waterPollution = 0;
			this.m_buildings.m_buffer[(int)building].m_fireIntensity = 0;
			this.m_buildings.m_buffer[(int)building].m_problems = Notification.Problem.None;
			this.m_buildings.m_buffer[(int)building].m_lastFrame = 0;
			this.m_buildings.m_buffer[(int)building].m_tempImport = 0;
			this.m_buildings.m_buffer[(int)building].m_tempExport = 0;
			this.m_buildings.m_buffer[(int)building].m_finalImport = 0;
			this.m_buildings.m_buffer[(int)building].m_finalExport = 0;
			this.m_buildings.m_buffer[(int)building].m_education1 = 0;
			this.m_buildings.m_buffer[(int)building].m_education2 = 0;
			this.m_buildings.m_buffer[(int)building].m_education3 = 0;
			this.m_buildings.m_buffer[(int)building].m_teens = 0;
			this.m_buildings.m_buffer[(int)building].m_youngs = 0;
			this.m_buildings.m_buffer[(int)building].m_adults = 0;
			this.m_buildings.m_buffer[(int)building].m_seniors = 0;
			this.m_buildings.m_buffer[(int)building].m_fireHazard = 0;
			this.m_buildings.m_buffer[(int)building].m_electricityProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_heatingProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_waterProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_workerProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_incomingProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_outgoingProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_healthProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_deathProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_serviceProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_taxProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_majorProblemTimer = 0;
			this.m_buildings.m_buffer[(int)building].m_levelUpProgress = 0;
			this.m_buildings.m_buffer[(int)building].m_subCulture = 0;
			info.m_buildingAI.CreateBuilding(building, ref this.m_buildings.m_buffer[(int)building]);
			this.m_buildings.m_buffer[(int)building].m_frame1 = this.m_buildings.m_buffer[(int)building].m_frame0;
			this.m_buildings.m_buffer[(int)building].m_frame2 = this.m_buildings.m_buffer[(int)building].m_frame0;
			this.m_buildings.m_buffer[(int)building].m_frame3 = this.m_buildings.m_buffer[(int)building].m_frame0;
			this.InitializeBuilding(building, ref this.m_buildings.m_buffer[(int)building]);
			this.UpdateBuilding(building);
			this.UpdateBuildingColors(building);
			this.m_buildingCount = (int)(this.m_buildings.ItemCount() - 1u);
			if (this.EventBuildingCreated != null)
			{
				this.EventBuildingCreated(building);
			}
			return true;
		}
		building = 0;
		return false;
	}

	public void ReleaseBuilding(ushort building)
	{
		this.ReleaseBuildingImplementation(building, ref this.m_buildings.m_buffer[(int)building]);
	}

	public BuildingInfo GetRandomBuildingInfo(ref Randomizer r, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, int width, int length, BuildingInfo.ZoningMode zoningMode, int style)
	{
		if (!this.m_buildingsRefreshed)
		{
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Random buildings not refreshed yet!");
			return null;
		}
		int num = BuildingManager.GetAreaIndex(service, subService, level, width, length, zoningMode);
		FastList<ushort> fastList;
		if (style > 0)
		{
			style--;
			DistrictStyle districtStyle = Singleton<DistrictManager>.get_instance().m_Styles[style];
			if (style <= this.m_styleBuildings.Length && this.m_styleBuildings[style] != null && this.m_styleBuildings[style].Count > 0 && districtStyle.AffectsService(service, subService, level))
			{
				if (this.m_styleBuildings[style].ContainsKey(num))
				{
					fastList = this.m_styleBuildings[style][num];
				}
				else
				{
					fastList = null;
				}
			}
			else
			{
				fastList = this.m_areaBuildings[num];
			}
		}
		else
		{
			fastList = this.m_areaBuildings[num];
		}
		if (fastList == null)
		{
			return null;
		}
		if (fastList.m_size == 0)
		{
			return null;
		}
		num = r.Int32((uint)fastList.m_size);
		return PrefabCollection<BuildingInfo>.GetPrefab((uint)fastList.m_buffer[num]);
	}

	public void AddServiceBuilding(ushort building, ItemClass.Service service)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			this.m_serviceBuildings[publicServiceIndex].Add(building);
		}
	}

	public void RemoveServiceBuilding(ushort building, ItemClass.Service service)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			this.m_serviceBuildings[publicServiceIndex].Remove(building);
		}
	}

	public FastList<ushort> GetServiceBuildings(ItemClass.Service service)
	{
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(service);
		if (publicServiceIndex != -1)
		{
			return this.m_serviceBuildings[publicServiceIndex];
		}
		return null;
	}

	public void AddOutsideConnection(ushort buildingID)
	{
		while (!Monitor.TryEnter(this.m_outsideConnections, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_outsideConnections.Add(buildingID);
		}
		finally
		{
			Monitor.Exit(this.m_outsideConnections);
		}
	}

	public void RemoveOutsideConnection(ushort buildingID)
	{
		while (!Monitor.TryEnter(this.m_outsideConnections, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_outsideConnections.Remove(buildingID);
		}
		finally
		{
			Monitor.Exit(this.m_outsideConnections);
		}
	}

	public FastList<ushort> GetOutsideConnections()
	{
		return this.m_outsideConnections;
	}

	private void InitializeBuilding(ushort building, ref Building data)
	{
		this.AddToGrid(building, ref data);
	}

	private void AddToGrid(ushort building, ref Building data)
	{
		int num = Mathf.Clamp((int)(data.m_position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		while (!Monitor.TryEnter(this.m_buildingGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_buildings.m_buffer[(int)building].m_nextGridBuilding = this.m_buildingGrid[num3];
			this.m_buildingGrid[num3] = building;
		}
		finally
		{
			Monitor.Exit(this.m_buildingGrid);
		}
	}

	private void AddToGrid2(ushort building, ref Building data)
	{
		int num = Mathf.Clamp((int)(data.m_position.x / 384f + 22.5f), 0, 44);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 384f + 22.5f), 0, 44);
		int num3 = num2 * 45 + num;
		while (!Monitor.TryEnter(this.m_buildingGrid2, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			ushort num4 = this.m_buildingGrid2[num3];
			int num5 = 0;
			while (num4 != 0)
			{
				if (num4 == building)
				{
					return;
				}
				num4 = this.m_buildings.m_buffer[(int)num4].m_nextGridBuilding2;
				if (++num5 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			this.m_buildings.m_buffer[(int)building].m_nextGridBuilding2 = this.m_buildingGrid2[num3];
			this.m_buildingGrid2[num3] = building;
		}
		finally
		{
			Monitor.Exit(this.m_buildingGrid2);
		}
	}

	private void RemoveFromGrid(ushort building, ref Building data)
	{
		BuildingInfo info = data.Info;
		int num = Mathf.Clamp((int)(data.m_position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 64f + 135f), 0, 269);
		int num3 = num2 * 270 + num;
		while (!Monitor.TryEnter(this.m_buildingGrid, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			ushort num4 = 0;
			ushort num5 = this.m_buildingGrid[num3];
			int num6 = 0;
			while (num5 != 0)
			{
				if (num5 == building)
				{
					if (num4 == 0)
					{
						this.m_buildingGrid[num3] = data.m_nextGridBuilding;
					}
					else
					{
						this.m_buildings.m_buffer[(int)num4].m_nextGridBuilding = data.m_nextGridBuilding;
					}
					break;
				}
				num4 = num5;
				num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
				if (++num6 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridBuilding = 0;
		}
		finally
		{
			Monitor.Exit(this.m_buildingGrid);
		}
		if (info != null)
		{
			Singleton<RenderManager>.get_instance().UpdateGroup(num * 45 / 270, num2 * 45 / 270, info.m_prefabDataLayer);
		}
	}

	private void RemoveFromGrid2(ushort building, ref Building data)
	{
		BuildingInfo info = data.Info;
		int num = Mathf.Clamp((int)(data.m_position.x / 384f + 22.5f), 0, 44);
		int num2 = Mathf.Clamp((int)(data.m_position.z / 384f + 22.5f), 0, 44);
		int num3 = num2 * 45 + num;
		while (!Monitor.TryEnter(this.m_buildingGrid2, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			ushort num4 = 0;
			ushort num5 = this.m_buildingGrid2[num3];
			int num6 = 0;
			while (num5 != 0)
			{
				if (num5 == building)
				{
					if (num4 == 0)
					{
						this.m_buildingGrid2[num3] = data.m_nextGridBuilding2;
					}
					else
					{
						this.m_buildings.m_buffer[(int)num4].m_nextGridBuilding2 = data.m_nextGridBuilding2;
					}
					break;
				}
				num4 = num5;
				num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding2;
				if (++num6 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_nextGridBuilding2 = 0;
		}
		finally
		{
			Monitor.Exit(this.m_buildingGrid2);
		}
		if (info != null)
		{
			Singleton<RenderManager>.get_instance().UpdateGroup(num, num2, info.m_prefabDataLayer);
		}
	}

	private void ReleaseBuildingImplementation(ushort building, ref Building data)
	{
		if (data.m_flags != Building.Flags.None && (data.m_flags & Building.Flags.Deleted) == Building.Flags.None)
		{
			BuildingInfo info = data.Info;
			InstanceID id = default(InstanceID);
			id.Building = building;
			Singleton<InstanceManager>.get_instance().ReleaseInstance(id);
			data.m_flags |= Building.Flags.Deleted;
			data.m_problems = Notification.Problem.None;
			data.UpdateBuilding(building);
			this.UpdateBuildingRenderer(building, true);
			if (info != null)
			{
				if (info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
				{
					this.UpdateParkingSpaces(building, ref data);
				}
				info.m_buildingAI.ReleaseBuilding(building, ref data);
			}
			if (data.m_citizenUnits != 0u)
			{
				Singleton<CitizenManager>.get_instance().ReleaseUnits(data.m_citizenUnits);
				data.m_citizenUnits = 0u;
			}
			this.ReleasePaths(building, ref data);
			this.ReleaseWaterSource(building, ref data);
			this.ReleaseEvents(building, ref data);
			this.ReleaseSubBuildings(building, ref data);
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_ownVehicles;
			int num2 = 0;
			while (num != 0)
			{
				ushort nextOwnVehicle = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
				instance.ReleaseVehicle(num);
				num = nextOwnVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			ushort num3 = data.m_guestVehicles;
			int num4 = 0;
			while (num3 != 0)
			{
				ushort nextGuestVehicle = instance.m_vehicles.m_buffer[(int)num3].m_nextGuestVehicle;
				VehicleInfo info2 = instance.m_vehicles.m_buffer[(int)num3].Info;
				if (info2 != null)
				{
					info2.m_vehicleAI.SetTarget(num3, ref instance.m_vehicles.m_buffer[(int)num3], 0);
				}
				num3 = nextGuestVehicle;
				if (++num4 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			ushort num5 = data.m_sourceCitizens;
			int num6 = 0;
			while (num5 != 0)
			{
				ushort nextSourceInstance = instance2.m_instances.m_buffer[(int)num5].m_nextSourceInstance;
				CitizenInfo info3 = instance2.m_instances.m_buffer[(int)num5].Info;
				if (info3 != null)
				{
					info3.m_citizenAI.SetSource(num5, ref instance2.m_instances.m_buffer[(int)num5], 0);
				}
				num5 = nextSourceInstance;
				if (++num6 > 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			ushort num7 = data.m_targetCitizens;
			int num8 = 0;
			while (num7 != 0)
			{
				ushort nextTargetInstance = instance2.m_instances.m_buffer[(int)num7].m_nextTargetInstance;
				CitizenInfo info4 = instance2.m_instances.m_buffer[(int)num7].Info;
				if (info4 != null)
				{
					info4.m_citizenAI.SetTarget(num7, ref instance2.m_instances.m_buffer[(int)num7], 0);
				}
				num7 = nextTargetInstance;
				if (++num8 > 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			data.m_flags = Building.Flags.None;
			this.RemoveFromGrid(building, ref data);
			this.m_buildings.ReleaseItem(building);
			this.m_buildingCount = (int)(this.m_buildings.ItemCount() - 1u);
			if (this.EventBuildingReleased != null)
			{
				this.EventBuildingReleased(building);
			}
		}
	}

	private void ReleasePaths(ushort building, ref Building data)
	{
		if (data.m_netNode != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = data.m_netNode;
			int num2 = 0;
			while (num != 0)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
					if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
					{
						instance.ReleaseSegment(segment, true);
					}
				}
				num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				if (++num2 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			num = data.m_netNode;
			data.m_netNode = 0;
			ushort num3 = 0;
			num2 = 0;
			while (num != 0)
			{
				ushort nextBuildingNode = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode = 0;
				bool flag = instance.m_nodes.m_buffer[(int)num].m_transportLine != 0;
				if (!flag)
				{
					for (int j = 0; j < 8; j++)
					{
						ushort segment2 = instance.m_nodes.m_buffer[(int)num].GetSegment(j);
						if (segment2 != 0)
						{
							flag = true;
							break;
						}
					}
				}
				if (flag)
				{
					if (instance.m_nodes.m_buffer[(int)num].m_transportLine != 0)
					{
						if (num3 != 0)
						{
							instance.m_nodes.m_buffer[(int)num3].m_nextBuildingNode = num;
						}
						else
						{
							data.m_netNode = num;
						}
						num3 = num;
					}
					else
					{
						NetNode[] expr_1D0_cp_0 = instance.m_nodes.m_buffer;
						ushort expr_1D0_cp_1 = num;
						expr_1D0_cp_0[(int)expr_1D0_cp_1].m_flags = (expr_1D0_cp_0[(int)expr_1D0_cp_1].m_flags & ~(NetNode.Flags.Untouchable | NetNode.Flags.ForbidLaneConnection));
						instance.UpdateNode(num);
					}
				}
				else
				{
					instance.ReleaseNode(num);
				}
				num = nextBuildingNode;
				if (++num2 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	private void ReleaseSubBuildings(ushort building, ref Building data)
	{
		if (data.m_parentBuilding != 0)
		{
			ushort parentBuilding = data.m_parentBuilding;
			ushort subBuilding = data.m_subBuilding;
			this.m_buildings.m_buffer[(int)parentBuilding].m_subBuilding = subBuilding;
			data.m_parentBuilding = 0;
			data.m_subBuilding = 0;
			if (subBuilding != 0)
			{
				this.m_buildings.m_buffer[(int)subBuilding].m_parentBuilding = parentBuilding;
			}
		}
		if (data.m_subBuilding != 0)
		{
			ushort num = data.m_subBuilding;
			data.m_subBuilding = 0;
			int num2 = 0;
			while (num != 0)
			{
				ushort subBuilding2 = this.m_buildings.m_buffer[(int)num].m_subBuilding;
				this.m_buildings.m_buffer[(int)num].m_subBuilding = 0;
				this.m_buildings.m_buffer[(int)num].m_parentBuilding = 0;
				this.ReleaseBuilding(num);
				num = subBuilding2;
				if (++num2 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	private void ReleaseWaterSource(ushort building, ref Building data)
	{
		if (data.m_waterSource != 0)
		{
			Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(data.m_waterSource);
			data.m_waterSource = 0;
		}
	}

	private void ReleaseEvents(ushort building, ref Building data)
	{
		if (data.m_eventIndex != 0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			ushort num = data.m_eventIndex;
			data.m_eventIndex = 0;
			int num2 = 0;
			while (num != 0)
			{
				ushort nextBuildingEvent = instance.m_events.m_buffer[(int)num].m_nextBuildingEvent;
				instance.m_events.m_buffer[(int)num].m_building = 0;
				instance.m_events.m_buffer[(int)num].m_nextBuildingEvent = 0;
				instance.ReleaseEvent(num);
				num = nextBuildingEvent;
				if (++num2 > 256)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public void RelocateBuilding(ushort building, Vector3 position, float angle)
	{
		this.RelocateBuildingImpl(building, ref this.m_buildings.m_buffer[(int)building], position, angle);
	}

	private void RelocateBuildingImpl(ushort building, ref Building data, Vector3 position, float angle)
	{
		if (data.m_flags != Building.Flags.None)
		{
			BuildingInfo info = data.Info;
			this.RemoveFromGrid(building, ref data);
			data.UpdateBuilding(building);
			this.UpdateBuildingRenderer(building, ref data, true, true);
			if (info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
			{
				this.UpdateParkingSpaces(building, ref data);
			}
			info.m_buildingAI.BeginRelocating(building, ref data);
			this.ReleasePaths(building, ref data);
			this.ReleaseWaterSource(building, ref data);
			this.ReleaseSubBuildings(building, ref data);
			data.m_position = position;
			data.m_angle = angle;
			if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				Building.Frame frame = default(Building.Frame);
				frame.m_constructState = 255;
				data.m_flags &= ~Building.Flags.Collapsed;
				data.m_problems = Notification.Problem.None;
				data.m_frame0 = frame;
				data.m_frame1 = frame;
				data.m_frame2 = frame;
				data.m_frame3 = frame;
			}
			info.m_buildingAI.EndRelocating(building, ref data);
			this.AddToGrid(building, ref data);
			data.CalculateBuilding(building);
			data.UpdateBuilding(building);
			this.UpdateBuildingRenderer(building, ref data, true, false);
			this.UpdateBuildingColors(building);
			VehicleManager instance = Singleton<VehicleManager>.get_instance();
			ushort num = data.m_ownVehicles;
			int num2 = 0;
			while (num != 0)
			{
				ushort nextOwnVehicle = instance.m_vehicles.m_buffer[(int)num].m_nextOwnVehicle;
				VehicleInfo info2 = instance.m_vehicles.m_buffer[(int)num].Info;
				info2.m_vehicleAI.BuildingRelocated(num, ref instance.m_vehicles.m_buffer[(int)num], building);
				num = nextOwnVehicle;
				if (++num2 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			ushort num3 = data.m_guestVehicles;
			int num4 = 0;
			while (num3 != 0)
			{
				ushort nextGuestVehicle = instance.m_vehicles.m_buffer[(int)num3].m_nextGuestVehicle;
				VehicleInfo info3 = instance.m_vehicles.m_buffer[(int)num3].Info;
				info3.m_vehicleAI.BuildingRelocated(num3, ref instance.m_vehicles.m_buffer[(int)num3], building);
				num3 = nextGuestVehicle;
				if (++num4 > 16384)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
			ushort num5 = data.m_targetCitizens;
			int num6 = 0;
			while (num5 != 0)
			{
				ushort nextTargetInstance = instance2.m_instances.m_buffer[(int)num5].m_nextTargetInstance;
				CitizenInfo info4 = instance2.m_instances.m_buffer[(int)num5].Info;
				info4.m_citizenAI.BuildingRelocated(num5, ref instance2.m_instances.m_buffer[(int)num5], building);
				num5 = nextTargetInstance;
				if (++num6 > 65536)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (this.EventBuildingRelocated != null)
			{
				this.EventBuildingRelocated(building);
			}
		}
	}

	public void UpdateBuilding(ushort building)
	{
		this.m_updatedBuildings[building >> 6] |= 1uL << (int)building;
		this.m_buildingsUpdated = true;
	}

	public void UpdateBuildingRenderer(ushort building, bool updateGroup)
	{
		this.UpdateBuildingRenderer(building, ref this.m_buildings.m_buffer[(int)building], updateGroup, false);
	}

	public void UpdateBuildingRenderer(ushort building, ref Building data, bool updateGroup)
	{
		this.UpdateBuildingRenderer(building, ref data, updateGroup, false);
	}

	public void UpdateBuildingRenderer(ushort building, ref Building data, bool updateGroup, bool relocating)
	{
		if (data.m_flags == Building.Flags.None)
		{
			return;
		}
		Singleton<RenderManager>.get_instance().UpdateInstance((uint)building);
		if (updateGroup)
		{
			BuildingInfo info = data.Info;
			if (info == null)
			{
				return;
			}
			if (relocating || (data.m_flags & Building.Flags.Deleted) != Building.Flags.None || info.m_buildingAI.CanUseGroupMesh(building, ref data))
			{
				this.RemoveFromGrid2(building, ref data);
			}
			else
			{
				this.AddToGrid2(building, ref data);
			}
			Vector3 position = data.m_position;
			int num = Mathf.Clamp((int)(position.x / 64f + 135f), 0, 269);
			int num2 = Mathf.Clamp((int)(position.z / 64f + 135f), 0, 269);
			int x = num * 45 / 270;
			int z = num2 * 45 / 270;
			int layers = this.GetLayers(info, building);
			for (int i = 0; i < 32; i++)
			{
				if ((layers & 1 << i) != 0)
				{
					Singleton<RenderManager>.get_instance().UpdateGroup(x, z, i);
				}
			}
		}
		else if (relocating)
		{
			this.RemoveFromGrid2(building, ref data);
		}
		if (data.m_subBuilding != 0 && data.m_parentBuilding == 0)
		{
			ushort subBuilding = data.m_subBuilding;
			int num3 = 0;
			while (subBuilding != 0)
			{
				this.UpdateBuildingRenderer(subBuilding, ref this.m_buildings.m_buffer[(int)subBuilding], updateGroup, relocating);
				subBuilding = this.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
				if (++num3 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
	}

	public bool UpgradeBuilding(ushort buildingID)
	{
		if (buildingID == 0 || (this.m_buildings.m_buffer[(int)buildingID].m_flags & Building.Flags.Created) == Building.Flags.None)
		{
			return false;
		}
		BuildingInfo info = this.m_buildings.m_buffer[(int)buildingID].Info;
		if (info == null)
		{
			return false;
		}
		BuildingInfo upgradeInfo = info.m_buildingAI.GetUpgradeInfo(buildingID, ref this.m_buildings.m_buffer[(int)buildingID]);
		if (upgradeInfo == null || upgradeInfo == info)
		{
			return false;
		}
		this.UpdateBuildingInfo(buildingID, upgradeInfo);
		upgradeInfo.m_buildingAI.BuildingUpgraded(buildingID, ref this.m_buildings.m_buffer[(int)buildingID]);
		int constructionCost = upgradeInfo.m_buildingAI.GetConstructionCost();
		Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Construction, constructionCost, upgradeInfo.m_class);
		int num = 0;
		while (buildingID != 0)
		{
			BuildingInfo info2 = this.m_buildings.m_buffer[(int)buildingID].Info;
			if (info2 != null)
			{
				Vector3 position = this.m_buildings.m_buffer[(int)buildingID].m_position;
				float angle = this.m_buildings.m_buffer[(int)buildingID].m_angle;
				BuildingTool.DispatchPlacementEffect(info2, 0, position, angle, info2.m_cellWidth, info2.m_cellLength, false, false);
			}
			buildingID = this.m_buildings.m_buffer[(int)buildingID].m_subBuilding;
			if (++num > 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return true;
	}

	public void UpdateBuildingInfo(ushort building, BuildingInfo newInfo)
	{
		this.UpdateBuildingInfo(building, ref this.m_buildings.m_buffer[(int)building], newInfo);
	}

	public void UpdateParkingSpaces(ushort building, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		Vector3 vector = new Vector3(Mathf.Cos(data.m_angle), 0f, Mathf.Sin(data.m_angle)) * 8f;
		Vector3 vector2;
		vector2..ctor(vector.z, 0f, -vector.x);
		Quad3 quad;
		quad.a = data.m_position - (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
		quad.b = data.m_position + (float)width * 0.5f * vector - (float)length * 0.5f * vector2;
		quad.c = data.m_position + (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
		quad.d = data.m_position - (float)width * 0.5f * vector + (float)length * 0.5f * vector2;
		Vector3 vector3 = quad.Min();
		Vector3 vector4 = quad.Max();
		Singleton<VehicleManager>.get_instance().UpdateParkedVehicles(vector3.x, vector3.z, vector4.x, vector4.z);
	}

	public void UpdateBuildingInfo(ushort building, ref Building data, BuildingInfo newInfo)
	{
		if (data.m_flags == Building.Flags.None)
		{
			return;
		}
		BuildingInfo info = data.Info;
		if (info.m_hasParkingSpaces != VehicleInfo.VehicleType.None)
		{
			this.UpdateParkingSpaces(building, ref data);
		}
		this.ReleasePaths(building, ref data);
		this.ReleaseWaterSource(building, ref data);
		if (data.m_parentBuilding == 0)
		{
			this.ReleaseSubBuildings(building, ref data);
		}
		data.Info = newInfo;
		BuildingDecoration.LoadPaths(newInfo, building, ref data, 0f);
		int width = data.Width;
		int length = data.Length;
		if (info.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft && newInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerRight)
		{
			data.m_angle -= 1.57079637f;
			data.Width = length;
			data.Length = width;
		}
		else if (info.m_zoningMode == BuildingInfo.ZoningMode.CornerRight && newInfo.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft)
		{
			data.m_angle += 1.57079637f;
			data.Width = length;
			data.Length = width;
		}
		data.CalculateBuilding(building);
		data.UpdateBuilding(building);
		Singleton<RenderManager>.get_instance().UpdateInstance((uint)building);
		this.UpdateBuildingColors(building);
		if ((data.m_flags & Building.Flags.Deleted) != Building.Flags.None || newInfo.m_buildingAI.CanUseGroupMesh(building, ref data))
		{
			this.RemoveFromGrid2(building, ref data);
		}
		else
		{
			this.AddToGrid2(building, ref data);
		}
		Vector3 position = data.m_position;
		int num = Mathf.Clamp((int)(position.x / 64f + 135f), 0, 269);
		int num2 = Mathf.Clamp((int)(position.z / 64f + 135f), 0, 269);
		int x = num * 45 / 270;
		int z = num2 * 45 / 270;
		int num3 = this.GetLayers(info, building) | this.GetLayers(newInfo, building);
		for (int i = 0; i < 32; i++)
		{
			if ((num3 & 1 << i) != 0)
			{
				Singleton<RenderManager>.get_instance().UpdateGroup(x, z, i);
			}
		}
		if (data.m_parentBuilding == 0 && newInfo.m_subBuildings != null && newInfo.m_subBuildings.Length != 0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			ushort eventIndex = data.m_eventIndex;
			EventInfo eventInfo = null;
			if (eventIndex != 0)
			{
				eventInfo = instance.m_events.m_buffer[(int)eventIndex].Info;
			}
			Matrix4x4 matrix4x = default(Matrix4x4);
			matrix4x.SetTRS(data.m_position, Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down()), Vector3.get_one());
			for (int j = 0; j < newInfo.m_subBuildings.Length; j++)
			{
				BuildingInfo buildingInfo = newInfo.m_subBuildings[j].m_buildingInfo;
				if (eventInfo != null)
				{
					buildingInfo = eventInfo.m_eventAI.ModifySubBuildingInfo(buildingInfo);
				}
				if (buildingInfo != null)
				{
					Vector3 position2 = matrix4x.MultiplyPoint(newInfo.m_subBuildings[j].m_position);
					float angle = newInfo.m_subBuildings[j].m_angle * 0.0174532924f + data.m_angle;
					bool fixedHeight = newInfo.m_subBuildings[j].m_fixedHeight;
					ushort num4;
					if (this.CreateBuilding(out num4, ref Singleton<SimulationManager>.get_instance().m_randomizer, buildingInfo, position2, angle, 0, data.m_buildIndex))
					{
						if (fixedHeight)
						{
							Building[] expr_31D_cp_0 = this.m_buildings.m_buffer;
							ushort expr_31D_cp_1 = num4;
							expr_31D_cp_0[(int)expr_31D_cp_1].m_flags = (expr_31D_cp_0[(int)expr_31D_cp_1].m_flags | Building.Flags.FixedHeight);
						}
						this.m_buildings.m_buffer[(int)building].m_subBuilding = num4;
						this.m_buildings.m_buffer[(int)num4].m_parentBuilding = building;
						Building[] expr_36D_cp_0 = this.m_buildings.m_buffer;
						ushort expr_36D_cp_1 = num4;
						expr_36D_cp_0[(int)expr_36D_cp_1].m_flags = (expr_36D_cp_0[(int)expr_36D_cp_1].m_flags | Building.Flags.Untouchable);
						building = num4;
					}
				}
			}
		}
	}

	private int GetLayers(BuildingInfo info, ushort building)
	{
		if (info == null)
		{
			return 0;
		}
		int num = 1 << info.m_prefabDataLayer | 1 << Singleton<NotificationManager>.get_instance().m_notificationLayer;
		if (info.m_props != null)
		{
			for (int i = 0; i < info.m_props.Length; i++)
			{
				Randomizer randomizer;
				randomizer..ctor((int)building << 6 | info.m_props[i].m_index);
				if (randomizer.Int32(100u) < info.m_props[i].m_probability)
				{
					PropInfo propInfo = info.m_props[i].m_finalProp;
					TreeInfo treeInfo = info.m_props[i].m_finalTree;
					if (propInfo != null)
					{
						propInfo = propInfo.GetVariation(ref randomizer);
						num |= 1 << propInfo.m_prefabDataLayer;
						if (propInfo.m_effectLayer != -1)
						{
							LightSystem lightSystem = Singleton<RenderManager>.get_instance().lightSystem;
							if (info.m_isFloating && info.m_props[i].m_fixedHeight && propInfo.m_effectLayer == lightSystem.m_lightLayer)
							{
								num |= 1 << lightSystem.m_lightLayerFloating;
							}
							else
							{
								num |= 1 << propInfo.m_effectLayer;
							}
						}
					}
					else if (treeInfo != null)
					{
						treeInfo = treeInfo.GetVariation(ref randomizer);
						num |= 1 << treeInfo.m_prefabDataLayer;
					}
				}
			}
		}
		return num;
	}

	public void UpdateNotifications(ushort building, Notification.Problem oldProblems, Notification.Problem newProblems)
	{
		if (this.m_updatingNotifications)
		{
			return;
		}
		try
		{
			this.m_updatingNotifications = true;
			if (this.m_buildings.m_buffer[(int)building].m_flags != Building.Flags.None)
			{
				Vector3 position = this.m_buildings.m_buffer[(int)building].m_position;
				Notification.Problem problem = Notification.Problem.None;
				if ((newProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
				{
					if ((oldProblems & Notification.Problem.FatalProblem) != Notification.Problem.None)
					{
						problem = (Notification.Problem.FatalProblem | (oldProblems & ~newProblems));
					}
				}
				else if ((newProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
				{
					if ((oldProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
					{
						problem = (Notification.Problem.MajorProblem | (oldProblems & ~newProblems));
					}
				}
				else
				{
					if ((oldProblems & Notification.Problem.MajorProblem) != Notification.Problem.None)
					{
						BuildingInfo info = this.m_buildings.m_buffer[(int)building].Info;
						if (info != null)
						{
							info.m_buildingAI.CheckRoadAccess(building, ref this.m_buildings.m_buffer[(int)building]);
							this.m_roadCheckNeeded.Remove(building);
						}
					}
					problem = (oldProblems & ~newProblems);
				}
				if (problem != Notification.Problem.None)
				{
					Vector3 position2;
					Quaternion quaternion;
					Vector3 vector;
					this.m_buildings.m_buffer[(int)building].GetTotalPosition(out position2, out quaternion, out vector);
					position2.y += vector.y;
					Singleton<NotificationManager>.get_instance().AddRemovalEvent(problem, position2, 1f);
				}
				int num = Mathf.Clamp((int)(position.x / 64f + 135f), 0, 269);
				int num2 = Mathf.Clamp((int)(position.z / 64f + 135f), 0, 269);
				int x = num * 45 / 270;
				int z = num2 * 45 / 270;
				Singleton<RenderManager>.get_instance().UpdateGroup(x, z, Singleton<NotificationManager>.get_instance().m_notificationLayer);
			}
		}
		finally
		{
			this.m_updatingNotifications = false;
		}
	}

	private void UpdateBuilding(ushort building, float minX, float minZ, float maxX, float maxZ)
	{
		if (building != 0)
		{
			Vector3 position = this.m_buildings.m_buffer[(int)building].m_position;
			if (position.x + 46f >= minX && position.x - 46f <= maxX && position.z + 46f >= minZ && position.z - 46f <= maxZ)
			{
				this.m_updatedBuildings[building >> 6] |= 1uL << (int)building;
				this.m_buildingsUpdated = true;
			}
		}
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, ItemClass.Layer layers, ushort ignoreBuilding, ushort ignoreNode1, ushort ignoreNode2)
	{
		return this.OverlapQuad(quad, minY, maxY, collisionType, layers, ignoreBuilding, ignoreNode1, ignoreNode2, null);
	}

	public bool OverlapQuad(Quad2 quad, float minY, float maxY, ItemClass.CollisionType collisionType, ItemClass.Layer layers, ushort ignoreBuilding, ushort ignoreNode1, ushort ignoreNode2, ulong[] buildingMask)
	{
		Vector2 vector = quad.Min();
		Vector2 vector2 = quad.Max();
		int num = Mathf.Max((int)((vector.x - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((vector.y - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((vector2.x + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((vector2.y + 72f) / 64f + 135f), 269);
		bool result = false;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					BuildingInfo info = this.m_buildings.m_buffer[(int)num5].Info;
					if (info != null && (layers == ItemClass.Layer.None || (info.m_class.m_layer & layers) != ItemClass.Layer.None) && !this.IgnoreOverlap(num5, ignoreBuilding, ignoreNode1, ignoreNode2) && this.m_buildings.m_buffer[(int)num5].OverlapQuad(num5, quad, minY, maxY, collisionType))
					{
						if (buildingMask == null)
						{
							return true;
						}
						buildingMask[num5 >> 6] |= 1uL << (int)num5;
						result = true;
					}
					num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public ushort GetWalkingBuilding(Vector3 worldPos)
	{
		int num = Mathf.Max((int)((worldPos.x - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((worldPos.z - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((worldPos.x + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((worldPos.z + 72f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					BuildingInfo buildingInfo;
					int num7;
					int num8;
					this.m_buildings.m_buffer[(int)num5].GetInfoWidthLength(out buildingInfo, out num7, out num8);
					Vector3 position = this.m_buildings.m_buffer[(int)num5].m_position;
					if (worldPos.y > position.y + buildingInfo.m_generatedInfo.m_min.y - 2f && worldPos.y < position.y + buildingInfo.m_generatedInfo.m_max.y + 2f)
					{
						float num9 = worldPos.x - position.x;
						float num10 = worldPos.z - position.z;
						if (num9 * num9 + num10 * num10 < (float)(num7 * num7 + num8 * num8 << 4))
						{
							float angle = this.m_buildings.m_buffer[(int)num5].m_angle;
							Vector3 vector;
							vector..ctor(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
							Vector3 vector2;
							vector2..ctor(vector.z, 0f, -vector.x);
							num9 = worldPos.x - position.x;
							num10 = worldPos.z - position.z;
							float num11 = num9 * vector.x + num10 * vector.z;
							float num12 = num9 * vector2.x + num10 * vector2.z;
							if (num11 >= (float)num7 * -4f && num11 <= (float)num7 * 4f && num12 >= (float)num8 * -4f && num12 <= (float)num8 * 4f)
							{
								return num5;
							}
						}
					}
					num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return 0;
	}

	public ushort FindBuilding(Vector3 pos, float maxDistance, ItemClass.Service service, ItemClass.SubService subService, Building.Flags flagsRequired, Building.Flags flagsForbidden)
	{
		int num = Mathf.Max((int)((pos.x - maxDistance) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((pos.z - maxDistance) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((pos.x + maxDistance) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((pos.z + maxDistance) / 64f + 135f), 269);
		ushort result = 0;
		float num5 = maxDistance * maxDistance;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = this.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					BuildingInfo info = this.m_buildings.m_buffer[(int)num6].Info;
					if ((info.m_class.m_service == service || service == ItemClass.Service.None) && (info.m_class.m_subService == subService || subService == ItemClass.SubService.None))
					{
						Building.Flags flags = this.m_buildings.m_buffer[(int)num6].m_flags;
						if ((flags & (flagsRequired | flagsForbidden)) == flagsRequired)
						{
							float num8 = Vector3.SqrMagnitude(pos - this.m_buildings.m_buffer[(int)num6].m_position);
							if (num8 < num5)
							{
								result = num6;
								num5 = num8;
							}
						}
					}
					num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public ushort FindTransportBuilding(Vector3 pos, float maxDistance, TransportInfo.TransportType type)
	{
		int num = Mathf.Max((int)((pos.x - maxDistance) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((pos.z - maxDistance) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((pos.x + maxDistance) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((pos.z + maxDistance) / 64f + 135f), 269);
		ushort result = 0;
		float num5 = maxDistance * maxDistance;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num6 = this.m_buildingGrid[i * 270 + j];
				int num7 = 0;
				while (num6 != 0)
				{
					BuildingInfo info = this.m_buildings.m_buffer[(int)num6].Info;
					TransportInfo transportLineInfo = info.m_buildingAI.GetTransportLineInfo();
					TransportInfo secondaryTransportLineInfo = info.m_buildingAI.GetSecondaryTransportLineInfo();
					if ((transportLineInfo != null && transportLineInfo.m_transportType == type) || (secondaryTransportLineInfo != null && secondaryTransportLineInfo.m_transportType == type))
					{
						float num8 = Vector3.SqrMagnitude(pos - this.m_buildings.m_buffer[(int)num6].m_position);
						if (num8 < num5)
						{
							result = num6;
							num5 = num8;
						}
					}
					num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	private bool IgnoreOverlap(ushort buildingIndex, ushort ignoreBuilding, ushort ignoreNode1, ushort ignoreNode2)
	{
		if (buildingIndex == ignoreBuilding)
		{
			return true;
		}
		if (ignoreNode1 != 0 || ignoreNode2 != 0)
		{
			ushort num = buildingIndex;
			int num2 = 0;
			while (num != 0)
			{
				NetManager instance = Singleton<NetManager>.get_instance();
				ushort num3 = this.m_buildings.m_buffer[(int)num].m_netNode;
				int num4 = 0;
				while (num3 != 0)
				{
					if (num3 == ignoreNode1 || num3 == ignoreNode2)
					{
						return true;
					}
					num3 = instance.m_nodes.m_buffer[(int)num3].m_nextBuildingNode;
					if (++num4 >= 32768)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
				num = this.m_buildings.m_buffer[(int)num].m_parentBuilding;
				if (++num2 >= 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if (ignoreBuilding != 0)
		{
			ushort subBuilding = this.m_buildings.m_buffer[(int)buildingIndex].m_subBuilding;
			int num5 = 0;
			while (subBuilding != 0)
			{
				if (subBuilding == ignoreBuilding)
				{
					return true;
				}
				subBuilding = this.m_buildings.m_buffer[(int)subBuilding].m_subBuilding;
				if (++num5 > 49152)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return false;
	}

	public bool RayCast(Segment3 ray, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Layer itemLayers, Building.Flags ignoreFlags, out Vector3 hit, out ushort buildingIndex)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1152f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = (int)(ray.a.x / 64f + 135f);
			int num2 = (int)(ray.a.z / 64f + 135f);
			int num3 = (int)(ray.b.x / 64f + 135f);
			int num4 = (int)(ray.b.z / 64f + 135f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 64f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 64f / num6;
				}
			}
			float num9 = 2f;
			buildingIndex = 0;
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			int num10 = num;
			int num11 = num2;
			do
			{
				Vector3 vector4 = vector3 + vector;
				int num12;
				int num13;
				int num14;
				int num15;
				if (num7 != 0)
				{
					if ((num10 == num && num7 > 0) || (num10 == num3 && num7 < 0))
					{
						num12 = Mathf.Max((int)((vector4.x - 72f) / 64f + 135f), 0);
					}
					else
					{
						num12 = Mathf.Max(num10, 0);
					}
					if ((num10 == num && num7 < 0) || (num10 == num3 && num7 > 0))
					{
						num13 = Mathf.Min((int)((vector4.x + 72f) / 64f + 135f), 269);
					}
					else
					{
						num13 = Mathf.Min(num10, 269);
					}
					num14 = Mathf.Max((int)((Mathf.Min(vector2.z, vector4.z) - 72f) / 64f + 135f), 0);
					num15 = Mathf.Min((int)((Mathf.Max(vector2.z, vector4.z) + 72f) / 64f + 135f), 269);
				}
				else
				{
					if ((num11 == num2 && num8 > 0) || (num11 == num4 && num8 < 0))
					{
						num14 = Mathf.Max((int)((vector4.z - 72f) / 64f + 135f), 0);
					}
					else
					{
						num14 = Mathf.Max(num11, 0);
					}
					if ((num11 == num2 && num8 < 0) || (num11 == num4 && num8 > 0))
					{
						num15 = Mathf.Min((int)((vector4.z + 72f) / 64f + 135f), 269);
					}
					else
					{
						num15 = Mathf.Min(num11, 269);
					}
					num12 = Mathf.Max((int)((Mathf.Min(vector2.x, vector4.x) - 72f) / 64f + 135f), 0);
					num13 = Mathf.Min((int)((Mathf.Max(vector2.x, vector4.x) + 72f) / 64f + 135f), 269);
				}
				for (int i = num14; i <= num15; i++)
				{
					for (int j = num12; j <= num13; j++)
					{
						ushort num16 = this.m_buildingGrid[i * 270 + j];
						int num17 = 0;
						while (num16 != 0)
						{
							Building.Flags flags = this.m_buildings.m_buffer[(int)num16].m_flags;
							if ((flags & ignoreFlags) == Building.Flags.None)
							{
								BuildingInfo info = this.m_buildings.m_buffer[(int)num16].Info;
								float num18;
								if ((service == ItemClass.Service.None || info.m_class.m_service == service) && (subService == ItemClass.SubService.None || info.m_class.m_subService == subService) && (itemLayers == ItemClass.Layer.None || (info.m_class.m_layer & itemLayers) != ItemClass.Layer.None) && this.m_buildings.m_buffer[(int)num16].RayCast(num16, ray, out num18) && num18 < num9)
								{
									num9 = num18;
									buildingIndex = num16;
								}
							}
							num16 = this.m_buildings.m_buffer[(int)num16].m_nextGridBuilding;
							if (++num17 > 49152)
							{
								CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
								break;
							}
						}
					}
				}
				vector2 = vector3;
				vector3 = vector4;
				num10 += num7;
				num11 += num8;
			}
			while ((num10 <= num3 || num7 <= 0) && (num10 >= num3 || num7 >= 0) && (num11 <= num4 || num8 <= 0) && (num11 >= num4 || num8 >= 0));
			if (num9 != 2f)
			{
				hit = ray.Position(num9);
				return true;
			}
		}
		hit = Vector3.get_zero();
		buildingIndex = 0;
		return false;
	}

	public void UpdateFlags(ushort building, Building.Flags changeMask)
	{
		if (this.m_buildings.m_buffer[(int)building].m_flags == Building.Flags.None)
		{
			return;
		}
		BuildingInfo info = this.m_buildings.m_buffer[(int)building].Info;
		if ((changeMask & Building.Flags.Active) != Building.Flags.None)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = this.m_buildings.m_buffer[(int)building].m_netNode;
			bool flag = (this.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.Active) != Building.Flags.None;
			int num2 = 0;
			while (num != 0)
			{
				if (flag)
				{
					NetNode[] expr_9E_cp_0 = instance.m_nodes.m_buffer;
					ushort expr_9E_cp_1 = num;
					expr_9E_cp_0[(int)expr_9E_cp_1].m_flags = (expr_9E_cp_0[(int)expr_9E_cp_1].m_flags & ~NetNode.Flags.Disabled);
				}
				else if (instance.m_nodes.m_buffer[(int)num].Info.m_canDisable)
				{
					NetNode[] expr_E2_cp_0 = instance.m_nodes.m_buffer;
					ushort expr_E2_cp_1 = num;
					expr_E2_cp_0[(int)expr_E2_cp_1].m_flags = (expr_E2_cp_0[(int)expr_E2_cp_1].m_flags | NetNode.Flags.Disabled);
				}
				num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				if (++num2 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		if ((changeMask & Building.Flags.Collapsed) != Building.Flags.None)
		{
			bool heights = info.m_generatedInfo.m_heights != null && info.m_generatedInfo.m_heights.Length != 0;
			bool flag2 = info.m_clipTerrain;
			if (!flag2 && info.m_cellSurfaces != null)
			{
				for (int i = 0; i < info.m_cellSurfaces.Length; i++)
				{
					if ((info.m_cellSurfaces[i] & TerrainModify.Surface.Clip) != TerrainModify.Surface.None)
					{
						flag2 = true;
						break;
					}
				}
			}
			this.m_buildings.m_buffer[(int)building].UpdateTerrain(heights, flag2);
		}
		int publicServiceIndex = ItemClass.GetPublicServiceIndex(info.m_class.m_service);
		if (publicServiceIndex != -1)
		{
			if ((changeMask & (Building.Flags.RateReduced | Building.Flags.Active | Building.Flags.Downgrading | Building.Flags.Collapsed)) != Building.Flags.None)
			{
				Singleton<CoverageManager>.get_instance().CoverageUpdated(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level);
			}
			if ((changeMask & Building.Flags.Active) != Building.Flags.None)
			{
				this.UpdateBuildingColors(building);
			}
		}
		else if ((changeMask & (Building.Flags.Completed | Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			this.UpdateBuildingColors(building);
		}
		if (info.m_subMeshes != null)
		{
			for (int j = 0; j < info.m_subMeshes.Length; j++)
			{
				BuildingInfo.MeshInfo meshInfo = info.m_subMeshes[j];
				if (((meshInfo.m_flagsRequired | meshInfo.m_flagsForbidden) & changeMask) != Building.Flags.None)
				{
					this.UpdateBuildingRenderer(building, true);
					return;
				}
			}
		}
	}

	public void RoadCheckNeeded(ushort building)
	{
		this.m_roadCheckNeeded.Add(building);
	}

	public void TerrainHeightUpdateNeeded(ushort building)
	{
		this.m_terrainHeightUpdateNeeded.Add(building);
	}

	protected override void SimulationStepImpl(int subStep)
	{
		if (this.m_buildingsUpdated)
		{
			int num = this.m_updatedBuildings.Length;
			for (int i = 0; i < num; i++)
			{
				ulong num2 = this.m_updatedBuildings[i];
				if (num2 != 0uL)
				{
					for (int j = 0; j < 64; j++)
					{
						if ((num2 & 1uL << j) != 0uL)
						{
							ushort num3 = (ushort)(i << 6 | j);
							this.m_buildings.m_buffer[(int)num3].CalculateBuilding(num3);
						}
					}
				}
			}
			this.m_buildingsUpdated = false;
			for (int k = 0; k < num; k++)
			{
				ulong num4 = this.m_updatedBuildings[k];
				if (num4 != 0uL)
				{
					this.m_updatedBuildings[k] = 0uL;
					for (int l = 0; l < 64; l++)
					{
						if ((num4 & 1uL << l) != 0uL)
						{
							ushort num5 = (ushort)(k << 6 | l);
							this.m_buildings.m_buffer[(int)num5].UpdateBuilding(num5);
							this.UpdateBuildingRenderer(num5, true);
						}
					}
				}
			}
		}
		if (this.m_roadCheckNeeded.Count != 0)
		{
			foreach (ushort current in this.m_roadCheckNeeded)
			{
				if ((this.m_buildings.m_buffer[(int)current].m_flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created)
				{
					BuildingInfo info = this.m_buildings.m_buffer[(int)current].Info;
					info.m_buildingAI.CheckRoadAccess(current, ref this.m_buildings.m_buffer[(int)current]);
				}
			}
			this.m_roadCheckNeeded.Clear();
		}
		if (this.m_terrainHeightUpdateNeeded.m_size != 0)
		{
			for (int m = 0; m < this.m_terrainHeightUpdateNeeded.m_size; m++)
			{
				ushort num6 = this.m_terrainHeightUpdateNeeded.m_buffer[m];
				if ((this.m_buildings.m_buffer[(int)num6].m_flags & (Building.Flags.Created | Building.Flags.Deleted)) == Building.Flags.Created)
				{
					this.m_buildings.m_buffer[(int)num6].UpdateTerrain(true, false);
				}
			}
			this.m_terrainHeightUpdateNeeded.Clear();
		}
		if (subStep != 0)
		{
			int num7 = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 255u);
			int num8 = num7 * 192;
			int num9 = (num7 + 1) * 192 - 1;
			bool flag = false;
			Notification.Problem problem = Notification.Problem.None;
			for (int n = num8; n <= num9; n++)
			{
				Building.Flags flags = this.m_buildings.m_buffer[n].m_flags;
				if ((flags & Building.Flags.Created) != Building.Flags.None)
				{
					Notification.Problem problems = this.m_buildings.m_buffer[n].m_problems;
					BuildingInfo info2 = this.m_buildings.m_buffer[n].Info;
					info2.m_buildingAI.SimulationStep((ushort)n, ref this.m_buildings.m_buffer[n]);
					Notification.Problem problems2 = this.m_buildings.m_buffer[n].m_problems;
					if (problems2 != problems)
					{
						this.UpdateNotifications((ushort)n, problems, problems2);
					}
					problem |= problems2;
					Building.Flags flags2 = this.m_buildings.m_buffer[n].m_flags;
					if (flags2 != flags)
					{
						this.UpdateFlags((ushort)n, flags2 ^ flags);
					}
					flag = true;
				}
			}
			if (num7 == 255)
			{
				this.m_lastBuildingProblems = this.m_currentBuildingProblems;
				this.m_currentBuildingProblems = Notification.Problem.None;
			}
			else
			{
				this.m_currentBuildingProblems |= problem;
			}
			if (flag)
			{
				switch (Singleton<InfoManager>.get_instance().CurrentMode)
				{
				case InfoManager.InfoMode.Electricity:
				case InfoManager.InfoMode.Water:
				case InfoManager.InfoMode.CrimeRate:
				case InfoManager.InfoMode.Health:
				case InfoManager.InfoMode.Happiness:
				case InfoManager.InfoMode.Density:
				case InfoManager.InfoMode.Transport:
				case InfoManager.InfoMode.Pollution:
				case InfoManager.InfoMode.Connections:
				case InfoManager.InfoMode.Wind:
				case InfoManager.InfoMode.Garbage:
				case InfoManager.InfoMode.BuildingLevel:
				case InfoManager.InfoMode.FireSafety:
				case InfoManager.InfoMode.Education:
				case InfoManager.InfoMode.Entertainment:
				case InfoManager.InfoMode.Heating:
				case InfoManager.InfoMode.EscapeRoutes:
				case InfoManager.InfoMode.Radio:
				case InfoManager.InfoMode.Destruction:
				case InfoManager.InfoMode.DisasterHazard:
					this.UpdateBuildingColors((ushort)num8, (ushort)num9);
					break;
				}
			}
		}
		if (subStep <= 1)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			int num10 = (int)(Singleton<SimulationManager>.get_instance().m_currentTickIndex & 1023u);
			int num11 = num10 * PrefabCollection<BuildingInfo>.PrefabCount() >> 10;
			int num12 = ((num10 + 1) * PrefabCollection<BuildingInfo>.PrefabCount() >> 10) - 1;
			for (int num13 = num11; num13 <= num12; num13++)
			{
				BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)num13);
				if (prefab != null && prefab.m_buildingAI.CheckUnlocking() && properties != null)
				{
					prefab.m_buildingAI.UpdateGuide(properties);
				}
			}
		}
	}

	public void CheckAllMilestones()
	{
		int num = PrefabCollection<BuildingInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				prefab.m_buildingAI.CheckUnlocking();
			}
		}
	}

	public override void EarlyUpdateData()
	{
		base.EarlyUpdateData();
		int num = PrefabCollection<BuildingInfo>.PrefabCount();
		for (int i = 0; i < num; i++)
		{
			BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)i);
			if (prefab != null)
			{
				MilestoneInfo unlockMilestone = prefab.m_UnlockMilestone;
				if (unlockMilestone != null)
				{
					unlockMilestone.ResetWrittenStatus();
				}
			}
		}
	}

	public override void TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		float num = Mathf.Min(heightArea.m_min.x, surfaceArea.m_min.x);
		float num2 = Mathf.Min(heightArea.m_min.z, surfaceArea.m_min.z);
		float num3 = Mathf.Max(heightArea.m_max.x, surfaceArea.m_max.x);
		float num4 = Mathf.Max(heightArea.m_max.z, surfaceArea.m_max.z);
		int num5 = Mathf.Max((int)((num - 72f) / 64f + 135f), 0);
		int num6 = Mathf.Max((int)((num2 - 72f) / 64f + 135f), 0);
		int num7 = Mathf.Min((int)((num3 + 72f) / 64f + 135f), 269);
		int num8 = Mathf.Min((int)((num4 + 72f) / 64f + 135f), 269);
		for (int i = num6; i <= num8; i++)
		{
			for (int j = num5; j <= num7; j++)
			{
				ushort num9 = this.m_buildingGrid[i * 270 + j];
				int num10 = 0;
				while (num9 != 0)
				{
					Vector3 position = this.m_buildings.m_buffer[(int)num9].m_position;
					int width = this.m_buildings.m_buffer[(int)num9].Width;
					int length = this.m_buildings.m_buffer[(int)num9].Length;
					float num11 = Mathf.Min(72f, (float)(width + length) * 4f);
					float num12 = Mathf.Max(Mathf.Max(num - num11 - position.x, num2 - num11 - position.z), Mathf.Max(position.x - num3 - num11, position.z - num4 - num11));
					if (num12 < 0f)
					{
						this.m_buildings.m_buffer[(int)num9].TerrainUpdated(num9, num, num2, num3, num4);
					}
					num9 = this.m_buildings.m_buffer[(int)num9].m_nextGridBuilding;
					if (++num10 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		if (!Singleton<LoadingManager>.get_instance().m_simulationDataLoaded)
		{
			return;
		}
		float x = heightArea.m_min.x;
		float z = heightArea.m_min.z;
		float x2 = heightArea.m_max.x;
		float z2 = heightArea.m_max.z;
		int num = Mathf.Max((int)((x - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((z - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((x2 + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((z2 + 72f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_buildings.m_buffer[(int)num5].m_position;
					int width = this.m_buildings.m_buffer[(int)num5].Width;
					int length = this.m_buildings.m_buffer[(int)num5].Length;
					float num7 = Mathf.Min(72f, (float)(width + length) * 4f);
					float num8 = Mathf.Max(Mathf.Max(x - num7 - position.x, z - num7 - position.z), Mathf.Max(position.x - x2 - num7, position.z - z2 - num7));
					if (num8 < 0f)
					{
						this.m_buildings.m_buffer[(int)num5].AfterTerrainUpdated(num5, x, z, x2, z2);
					}
					num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public void ZonesUpdated(float minX, float minZ, float maxX, float maxZ)
	{
		int num = Mathf.Max((int)((minX - 72f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((minZ - 72f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((maxX + 72f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((maxZ + 72f) / 64f + 135f), 269);
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = this.m_buildingGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					Vector3 position = this.m_buildings.m_buffer[(int)num5].m_position;
					int width = this.m_buildings.m_buffer[(int)num5].Width;
					int length = this.m_buildings.m_buffer[(int)num5].Length;
					float num7 = Mathf.Min(72f, (float)(width + length) * 4f);
					float num8 = Mathf.Max(Mathf.Max(minX - num7 - position.x, minZ - num7 - position.z), Mathf.Max(position.x - maxX - num7, position.z - maxZ - num7));
					if (num8 < 0f)
					{
						Building[] expr_164_cp_0 = this.m_buildings.m_buffer;
						ushort expr_164_cp_1 = num5;
						expr_164_cp_0[(int)expr_164_cp_1].m_flags = (expr_164_cp_0[(int)expr_164_cp_1].m_flags | Building.Flags.ZonesUpdated);
					}
					num5 = this.m_buildings.m_buffer[(int)num5].m_nextGridBuilding;
					if (++num6 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	private void RefreshAreaBuildings()
	{
		int num = this.m_areaBuildings.Length;
		for (int i = 0; i < num; i++)
		{
			this.m_areaBuildings[i] = null;
		}
		int num2 = PrefabCollection<BuildingInfo>.PrefabCount();
		BuildingInfo[] array = new BuildingInfo[num2];
		ushort[] array2 = new ushort[num2];
		ushort num3 = 0;
		while ((int)num3 < num2)
		{
			array[(int)num3] = PrefabCollection<BuildingInfo>.GetPrefab((uint)num3);
			array2[(int)num3] = num3;
			num3 += 1;
		}
		this.ApplyRefreshBuildings(array, array2, 0);
	}

	private void RefreshStyleBuildings()
	{
		ushort num = 0;
		while ((int)num < this.m_styleBuildings.Length)
		{
			this.m_styleBuildings[(int)num].Clear();
			DistrictStyle districtStyle = Singleton<DistrictManager>.get_instance().m_Styles[(int)num];
			BuildingInfo[] buildingInfos = districtStyle.GetBuildingInfos();
			ushort[] buildingIndices = districtStyle.GetBuildingIndices(buildingInfos);
			this.ApplyRefreshBuildingsStyles(buildingInfos, buildingIndices, (int)(num + 1));
			num += 1;
		}
	}

	private void ApplyRefreshBuildingsStyles(BuildingInfo[] infos, ushort[] indices, int style)
	{
		Dictionary<int, FastList<ushort>> dictionary = this.m_styleBuildings[style - 1];
		for (int i = 0; i < infos.Length; i++)
		{
			BuildingInfo info = infos[i];
			if (info != null && info.m_class.m_service != ItemClass.Service.None && info.m_placementStyle == ItemClass.Placement.Automatic && (!info.m_dontSpawnNormally || style > 0))
			{
				int privateServiceIndex = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
				if (privateServiceIndex != -1)
				{
					if (info.m_cellWidth < 1 || info.m_cellWidth > 4)
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new object[]
							{
								"Invalid width (",
								info.get_gameObject().get_name(),
								"): ",
								info.m_cellWidth
							}), info.get_gameObject());
						});
					}
					else if (info.m_cellLength < 1 || info.m_cellLength > 4)
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new object[]
							{
								"Invalid length (",
								info.get_gameObject().get_name(),
								"): ",
								info.m_cellLength
							}), info.get_gameObject());
						});
					}
					else
					{
						int areaIndex = BuildingManager.GetAreaIndex(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level, info.m_cellWidth, info.m_cellLength, info.m_zoningMode);
						if (!dictionary.ContainsKey(areaIndex))
						{
							dictionary[areaIndex] = new FastList<ushort>();
						}
						dictionary[areaIndex].Add(indices[i]);
					}
				}
			}
		}
		int num = 24;
		for (int j = 0; j < num; j++)
		{
			for (int k = 0; k < 5; k++)
			{
				for (int l = 0; l < 4; l++)
				{
					for (int m = 1; m < 4; m++)
					{
						int num2 = j;
						num2 = num2 * 5 + k;
						num2 = num2 * 4 + l;
						num2 = num2 * 4 + m;
						num2 *= 2;
						if (dictionary.ContainsKey(num2 - 2))
						{
							if (!dictionary.ContainsKey(num2))
							{
								dictionary[num2] = dictionary[num2 - 2];
							}
							else
							{
								FastList<ushort> fastList = dictionary[num2];
								FastList<ushort> fastList2 = dictionary[num2 - 2];
								for (int n = 0; n < fastList2.m_size; n++)
								{
									fastList.Add(fastList2.m_buffer[n]);
								}
							}
						}
					}
				}
			}
		}
		this.m_buildingsRefreshed = true;
	}

	private void ApplyRefreshBuildings(BuildingInfo[] infos, ushort[] indices, int style)
	{
		FastList<ushort>[] areaBuildings = this.m_areaBuildings;
		for (int i = 0; i < infos.Length; i++)
		{
			BuildingInfo info = infos[i];
			if (info != null && info.m_class.m_service != ItemClass.Service.None && info.m_placementStyle == ItemClass.Placement.Automatic && (!info.m_dontSpawnNormally || style > 0))
			{
				int privateServiceIndex = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
				if (privateServiceIndex != -1)
				{
					if (info.m_cellWidth < 1 || info.m_cellWidth > 4)
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new object[]
							{
								"Invalid width (",
								info.get_gameObject().get_name(),
								"): ",
								info.m_cellWidth
							}), info.get_gameObject());
						});
					}
					else if (info.m_cellLength < 1 || info.m_cellLength > 4)
					{
						ThreadHelper.get_dispatcher().Dispatch(delegate
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, string.Concat(new object[]
							{
								"Invalid length (",
								info.get_gameObject().get_name(),
								"): ",
								info.m_cellLength
							}), info.get_gameObject());
						});
					}
					else
					{
						int areaIndex = BuildingManager.GetAreaIndex(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level, info.m_cellWidth, info.m_cellLength, info.m_zoningMode);
						if (areaBuildings[areaIndex] == null)
						{
							areaBuildings[areaIndex] = new FastList<ushort>();
						}
						areaBuildings[areaIndex].Add(indices[i]);
					}
				}
			}
		}
		int num = 24;
		for (int j = 0; j < num; j++)
		{
			for (int k = 0; k < 5; k++)
			{
				for (int l = 0; l < 4; l++)
				{
					for (int m = 1; m < 4; m++)
					{
						int num2 = j;
						num2 = num2 * 5 + k;
						num2 = num2 * 4 + l;
						num2 = num2 * 4 + m;
						num2 *= 2;
						FastList<ushort> fastList = areaBuildings[num2];
						FastList<ushort> fastList2 = areaBuildings[num2 - 2];
						if (fastList2 != null)
						{
							if (fastList == null)
							{
								areaBuildings[num2] = fastList2;
							}
							else
							{
								for (int n = 0; n < fastList2.m_size; n++)
								{
									fastList.Add(fastList2.m_buffer[n]);
								}
							}
						}
					}
				}
			}
		}
		for (int num3 = 0; num3 < infos.Length; num3++)
		{
			BuildingInfo info = infos[num3];
			if (info != null && info.m_class.m_service != ItemClass.Service.None && info.m_placementStyle == ItemClass.Placement.Automatic && !info.m_dontSpawnNormally)
			{
				int privateServiceIndex2 = ItemClass.GetPrivateServiceIndex(info.m_class.m_service);
				if (privateServiceIndex2 != -1)
				{
					if (info.m_cellWidth >= 1 && info.m_cellWidth <= 4)
					{
						if (info.m_cellLength >= 1 && info.m_cellLength <= 4)
						{
							ItemClass.Level level = ItemClass.Level.Level1;
							ItemClass.Level level2 = ItemClass.Level.Level1;
							if (info.m_class.m_service == ItemClass.Service.Residential)
							{
								level2 = ItemClass.Level.Level5;
							}
							else if (info.m_class.m_service == ItemClass.Service.Commercial)
							{
								if (info.m_class.m_subService == ItemClass.SubService.CommercialLow || info.m_class.m_subService == ItemClass.SubService.CommercialHigh)
								{
									level2 = ItemClass.Level.Level3;
								}
								else
								{
									level = ItemClass.Level.Level3;
								}
							}
							else if (info.m_class.m_service == ItemClass.Service.Industrial)
							{
								if (info.m_class.m_subService == ItemClass.SubService.IndustrialGeneric)
								{
									level2 = ItemClass.Level.Level3;
								}
								else
								{
									level = ItemClass.Level.Level3;
								}
							}
							else if (info.m_class.m_service == ItemClass.Service.Office)
							{
								if (info.m_class.m_subService == ItemClass.SubService.OfficeGeneric)
								{
									level2 = ItemClass.Level.Level3;
								}
								else
								{
									level = ItemClass.Level.Level3;
								}
							}
							if (info.m_class.m_level < level2)
							{
								int areaIndex2 = BuildingManager.GetAreaIndex(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level + 1, info.m_cellWidth, info.m_cellLength, info.m_zoningMode);
								if (areaBuildings[areaIndex2] == null)
								{
									ThreadHelper.get_dispatcher().Dispatch(delegate
									{
										CODebugBase<LogChannel>.Warn(LogChannel.Core, "Building cannot upgrade to next level: " + info.get_gameObject().get_name(), info.get_gameObject());
									});
								}
							}
							if (info.m_class.m_level > level)
							{
								int areaIndex3 = BuildingManager.GetAreaIndex(info.m_class.m_service, info.m_class.m_subService, info.m_class.m_level - 1, info.m_cellWidth, info.m_cellLength, info.m_zoningMode);
								if (areaBuildings[areaIndex3] == null)
								{
									ThreadHelper.get_dispatcher().Dispatch(delegate
									{
										CODebugBase<LogChannel>.Warn(LogChannel.Core, "There is no building that would upgrade to: " + info.get_gameObject().get_name(), info.get_gameObject());
									});
								}
							}
						}
					}
				}
			}
		}
		this.m_buildingsRefreshed = true;
	}

	[DebuggerHidden]
	public IEnumerator<bool> SetBuildingName(ushort building, string name)
	{
		BuildingManager.<SetBuildingName>c__Iterator2 <SetBuildingName>c__Iterator = new BuildingManager.<SetBuildingName>c__Iterator2();
		<SetBuildingName>c__Iterator.building = building;
		<SetBuildingName>c__Iterator.name = name;
		<SetBuildingName>c__Iterator.$this = this;
		return <SetBuildingName>c__Iterator;
	}

	public override bool CalculateGroupData(int groupX, int groupZ, int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_buildingGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					if (this.m_buildings.m_buffer[(int)num6].CalculateGroupData(num6, layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
					{
						result = true;
					}
					num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return result;
	}

	public override void PopulateGroupData(int groupX, int groupZ, int layer, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance, ref bool requireSurfaceMaps)
	{
		int num = groupX * 270 / 45;
		int num2 = groupZ * 270 / 45;
		int num3 = (groupX + 1) * 270 / 45 - 1;
		int num4 = (groupZ + 1) * 270 / 45 - 1;
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				int num5 = i * 270 + j;
				ushort num6 = this.m_buildingGrid[num5];
				int num7 = 0;
				while (num6 != 0)
				{
					this.m_buildings.m_buffer[(int)num6].PopulateGroupData(num6, groupX, groupZ, layer, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
					num6 = this.m_buildings.m_buffer[(int)num6].m_nextGridBuilding;
					if (++num7 >= 49152)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("BuildingManager.UpdateData");
		base.UpdateData(mode);
		for (int i = 1; i < 49152; i++)
		{
			if (this.m_buildings.m_buffer[i].m_flags != Building.Flags.None)
			{
				if (this.m_buildings.m_buffer[i].Info == null)
				{
					default(TransferManager.TransferOffer).Building = (ushort)i;
					this.ReleaseBuilding((ushort)i);
				}
				else if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap)
				{
					Building[] expr_A0_cp_0 = this.m_buildings.m_buffer;
					int expr_A0_cp_1 = i;
					expr_A0_cp_0[expr_A0_cp_1].m_flags = (expr_A0_cp_0[expr_A0_cp_1].m_flags | Building.Flags.Original);
				}
			}
		}
		ItemClass.Availability mode2 = Singleton<ToolManager>.get_instance().m_properties.m_mode;
		int num = PrefabCollection<BuildingInfo>.PrefabCount();
		this.m_infoCount = num;
		for (int j = 0; j < num; j++)
		{
			BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)j);
			if (prefab != null)
			{
				if (prefab.m_placementStyle == ItemClass.Placement.Manual && (prefab.m_availableIn & mode2) != ItemClass.Availability.None && prefab.m_buildingAI.EnableNotUsedGuide())
				{
					if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || prefab.m_notUsedGuide == null)
					{
						prefab.m_notUsedGuide = new BuildingTypeGuide();
					}
				}
				else
				{
					prefab.m_notUsedGuide = null;
				}
				if (prefab.m_UnlockMilestone != null)
				{
					switch (mode)
					{
					case SimulationManager.UpdateMode.NewMap:
					case SimulationManager.UpdateMode.LoadMap:
					case SimulationManager.UpdateMode.NewAsset:
					case SimulationManager.UpdateMode.LoadAsset:
						prefab.m_UnlockMilestone.Reset(false);
						break;
					case SimulationManager.UpdateMode.NewGameFromMap:
					case SimulationManager.UpdateMode.NewScenarioFromMap:
					case SimulationManager.UpdateMode.UpdateScenarioFromMap:
						prefab.m_UnlockMilestone.Reset(false);
						if (prefab.m_UnlockMilestone.m_isGlobal)
						{
							MilestoneInfo.Data data = prefab.m_UnlockMilestone.GetData();
							if (prefab.m_alreadyUnlocked)
							{
								if (data.m_passedCount == 0)
								{
									data.m_passedCount = 1;
								}
							}
							else if (data.m_passedCount == 1)
							{
								data.m_passedCount = 0;
							}
						}
						break;
					case SimulationManager.UpdateMode.LoadGame:
					case SimulationManager.UpdateMode.NewScenarioFromGame:
					case SimulationManager.UpdateMode.LoadScenario:
					case SimulationManager.UpdateMode.NewGameFromScenario:
					case SimulationManager.UpdateMode.UpdateScenarioFromGame:
						prefab.m_UnlockMilestone.Reset(true);
						if (prefab.m_UnlockMilestone.m_isGlobal)
						{
							MilestoneInfo.Data data2 = prefab.m_UnlockMilestone.GetData();
							if (prefab.m_alreadyUnlocked)
							{
								if (data2.m_passedCount == 0)
								{
									data2.m_passedCount = 1;
								}
							}
							else if (data2.m_passedCount == 1)
							{
								data2.m_passedCount = 0;
							}
						}
						break;
					}
				}
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingAbandoned1 == null)
		{
			this.m_buildingAbandoned1 = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingAbandoned2 == null)
		{
			this.m_buildingAbandoned2 = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingBurned == null)
		{
			this.m_buildingBurned = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingLevelUp == null)
		{
			this.m_buildingLevelUp = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingOnFire == null)
		{
			this.m_buildingOnFire = new BuildingOnFireGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildNextToRoad == null)
		{
			this.m_buildNextToRoad = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildNextToWater == null)
		{
			this.m_buildNextToWater = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_landfillSiteFull == null)
		{
			this.m_landfillSiteFull = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cemeteryFull == null)
		{
			this.m_cemeteryFull = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_landfillSiteEmpty == null)
		{
			this.m_landfillSiteEmpty = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_cemeteryEmpty == null)
		{
			this.m_cemeteryEmpty = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_windTurbinePlacement == null)
		{
			this.m_windTurbinePlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_harborPlacement == null)
		{
			this.m_harborPlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_heatingPlacement == null)
		{
			this.m_heatingPlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_roadMaintenancePlacement == null)
		{
			this.m_roadMaintenancePlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_tramDepotPlacement == null)
		{
			this.m_tramDepotPlacement = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_depotNotConnected == null)
		{
			this.m_depotNotConnected = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_snowDumpFull == null)
		{
			this.m_snowDumpFull = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingFlooded == null)
		{
			this.m_buildingFlooded = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingFlooded2 == null)
		{
			this.m_buildingFlooded2 = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_escapeRoutes == null)
		{
			this.m_escapeRoutes = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingDestroyed == null)
		{
			this.m_buildingDestroyed = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_waterOutletOnLand == null)
		{
			this.m_waterOutletOnLand = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_evacuationNotUsed == null)
		{
			this.m_evacuationNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_citizensInShelter == null)
		{
			this.m_citizensInShelter = new BuildingInstanceGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_buildingDestroyed2 == null)
		{
			this.m_buildingDestroyed2 = new BuildingInstanceGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new BuildingManager.Data());
	}

	public void UpdateBuildingColors(ushort building)
	{
		ushort num = building;
		ushort num2 = building;
		building = this.m_buildings.m_buffer[(int)building].m_subBuilding;
		int num3 = 0;
		while (building != 0)
		{
			if (building < num)
			{
				num = building;
			}
			if (building > num2)
			{
				num2 = building;
			}
			building = this.m_buildings.m_buffer[(int)building].m_subBuilding;
			if (++num3 > 49152)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		this.UpdateBuildingColors(num, num2);
	}

	public void UpdateBuildingColors(ushort min, ushort max)
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMin = Mathf.Min(this.m_colorUpdateMin, (int)min);
			this.m_colorUpdateMax = Mathf.Max(this.m_colorUpdateMax, (int)max);
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void UpdateBuildingColors()
	{
		while (!Monitor.TryEnter(this.m_colorUpdateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			this.m_colorUpdateMin = 0;
			this.m_colorUpdateMax = 49151;
		}
		finally
		{
			Monitor.Exit(this.m_colorUpdateLock);
		}
	}

	public void CalculateOutsideConnectionCount(ItemClass.Service service, ItemClass.SubService subService, out int incoming, out int outgoing)
	{
		while (!Monitor.TryEnter(this.m_outsideConnections, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			incoming = 0;
			outgoing = 0;
			for (int i = 0; i < this.m_outsideConnections.m_size; i++)
			{
				ushort num = this.m_outsideConnections.m_buffer[i];
				BuildingInfo info = this.m_buildings.m_buffer[(int)num].Info;
				if (info != null && info.m_class.m_service == service && info.m_class.m_subService == subService)
				{
					Building.Flags flags = this.m_buildings.m_buffer[(int)num].m_flags;
					if ((flags & Building.Flags.Outgoing) != Building.Flags.None)
					{
						incoming++;
					}
					if ((flags & Building.Flags.Incoming) != Building.Flags.None)
					{
						outgoing++;
					}
				}
			}
		}
		finally
		{
			Monitor.Exit(this.m_outsideConnections);
		}
	}

	public string GetDefaultBuildingName(ushort building, InstanceID caller)
	{
		return this.GenerateName(building, caller);
	}

	public string GetBuildingName(ushort building, InstanceID caller)
	{
		if (this.m_buildings.m_buffer[(int)building].m_flags != Building.Flags.None)
		{
			string text = null;
			if ((this.m_buildings.m_buffer[(int)building].m_flags & Building.Flags.CustomName) != Building.Flags.None)
			{
				InstanceID id = default(InstanceID);
				id.Building = building;
				text = Singleton<InstanceManager>.get_instance().GetName(id);
			}
			if (text == null)
			{
				text = this.GenerateName(building, caller);
			}
			return text;
		}
		return null;
	}

	private string GenerateName(ushort building, InstanceID caller)
	{
		string text = null;
		BuildingInfo info = this.m_buildings.m_buffer[(int)building].Info;
		if (info != null)
		{
			text = info.m_buildingAI.GenerateName(building, caller);
		}
		if (text == null)
		{
			text = "Invalid";
		}
		return text;
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	void IAudibleManager.PlayAudio(AudioManager.ListenerInfo listenerInfo)
	{
		base.PlayAudio(listenerInfo);
	}

	public const float BUILDINGGRID_CELL_SIZE = 64f;

	public const int BUILDINGGRID_RESOLUTION = 270;

	public const int MAX_BUILDING_COUNT = 49152;

	public const int MAX_OUTSIDE_CONNECTIONS = 4;

	public const int MAX_MAP_BUILDINGS = 8192;

	public const int MAX_ASSET_BUILDINGS = 1024;

	public int m_buildingCount;

	public int m_infoCount;

	public bool m_abandonmentDisabled;

	public bool m_firesDisabled;

	[NonSerialized]
	public Array16<Building> m_buildings;

	[NonSerialized]
	public ulong[] m_updatedBuildings;

	[NonSerialized]
	public bool m_buildingsUpdated;

	[NonSerialized]
	public ushort[] m_buildingGrid;

	[NonSerialized]
	public ushort[] m_buildingGrid2;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock;

	[NonSerialized]
	public int ID_BuildingState;

	[NonSerialized]
	public int ID_ObjectIndex;

	[NonSerialized]
	public int ID_Color;

	[NonSerialized]
	public int ID_BuildingSize;

	[NonSerialized]
	public int ID_MainTex;

	[NonSerialized]
	public int ID_XYSMap;

	[NonSerialized]
	public int ID_ACIMap;

	[NonSerialized]
	public int ID_BuildingLocations;

	[NonSerialized]
	public int ID_BuildingStates;

	[NonSerialized]
	public int ID_ObjectIndices;

	[NonSerialized]
	public int ID_BuildingColors;

	[NonSerialized]
	public int ID_HeightMap;

	[NonSerialized]
	public int ID_HeightMapping;

	[NonSerialized]
	public int ID_SurfaceMapping;

	[NonSerialized]
	public int ID_WaterHeightMap;

	[NonSerialized]
	public int ID_WaterHeightMapping;

	[NonSerialized]
	public int ID_WaterSurfaceMapping;

	[NonSerialized]
	public int ID_Speed;

	[NonSerialized]
	public int ID_State;

	[NonSerialized]
	public AudioGroup m_audioGroup;

	[NonSerialized]
	public BuildingCommonCollection m_common;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingAbandoned1;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingAbandoned2;

	[NonSerialized]
	public BuildingOnFireGuide m_buildingOnFire;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingBurned;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingLevelUp;

	[NonSerialized]
	public GenericGuide m_buildNextToRoad;

	[NonSerialized]
	public GenericGuide m_buildNextToWater;

	[NonSerialized]
	public BuildingInstanceGuide m_landfillSiteFull;

	[NonSerialized]
	public BuildingInstanceGuide m_cemeteryFull;

	[NonSerialized]
	public BuildingInstanceGuide m_landfillSiteEmpty;

	[NonSerialized]
	public BuildingInstanceGuide m_cemeteryEmpty;

	[NonSerialized]
	public GenericGuide m_windTurbinePlacement;

	[NonSerialized]
	public GenericGuide m_harborPlacement;

	[NonSerialized]
	public GenericGuide m_heatingPlacement;

	[NonSerialized]
	public GenericGuide m_roadMaintenancePlacement;

	[NonSerialized]
	public GenericGuide m_tramDepotPlacement;

	[NonSerialized]
	public BuildingInstanceGuide m_depotNotConnected;

	[NonSerialized]
	public BuildingInstanceGuide m_snowDumpFull;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingFlooded;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingFlooded2;

	[NonSerialized]
	public BuildingInstanceGuide m_escapeRoutes;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingDestroyed;

	[NonSerialized]
	public BuildingInstanceGuide m_buildingDestroyed2;

	[NonSerialized]
	public BuildingInstanceGuide m_waterOutletOnLand;

	[NonSerialized]
	public GenericGuide m_evacuationNotUsed;

	[NonSerialized]
	public BuildingInstanceGuide m_citizensInShelter;

	[NonSerialized]
	public Notification.Problem m_lastBuildingProblems;

	[NonSerialized]
	public Notification.Problem m_currentBuildingProblems;

	[NonSerialized]
	public CityNameGroups.Environment m_cityNameGroups;

	[NonSerialized]
	public LevelUpWrapper m_LevelUpWrapper;

	[NonSerialized]
	public BuildingWrapper m_BuildingWrapper;

	public Texture2D m_lodRgbAtlas;

	public Texture2D m_lodXysAtlas;

	public Texture2D m_lodAciAtlas;

	private FastList<ushort>[] m_areaBuildings;

	private Dictionary<int, FastList<ushort>>[] m_styleBuildings;

	private FastList<ushort>[] m_serviceBuildings;

	private FastList<ushort> m_outsideConnections;

	private HashSet<ushort> m_roadCheckNeeded;

	private FastList<ushort> m_terrainHeightUpdateNeeded;

	private Mesh m_highlightMesh;

	private Mesh m_highlightMesh2;

	private Material m_highlightMaterial;

	private int m_buildingLayer;

	private int m_colorUpdateMin;

	private int m_colorUpdateMax;

	private object m_colorUpdateLock;

	private bool m_buildingsRefreshed;

	private bool m_updatingNotifications;

	public delegate void BuildingCreatedHandler(ushort building);

	public delegate void BuildingRelocatedHandler(ushort building);

	public delegate void BuildingReleasedHandler(ushort building);

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "BuildingManager");
			ToolManager instance = Singleton<ToolManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			Building[] buffer = instance2.m_buildings.m_buffer;
			int num = buffer.Length;
			EncodedArray.UInt uInt = EncodedArray.UInt.BeginWrite(s);
			for (int i = 1; i < num; i++)
			{
				uInt.Write((uint)buffer[i].m_flags);
			}
			uInt.EndWrite();
			int num2 = PrefabCollection<BuildingInfo>.PrefabCount();
			uint num3 = 0u;
			uint num4 = 0u;
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int j = 0; j < num2; j++)
				{
					BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)j);
					if (prefab != null && prefab.m_notUsedGuide != null && prefab.m_prefabDataIndex != -1)
					{
						num3 += 1u;
					}
					if (prefab != null && prefab.m_UnlockMilestone != null && prefab.m_prefabDataIndex != -1)
					{
						num4 += 1u;
					}
				}
			}
			s.WriteUInt16(num3);
			s.WriteUInt16(num4);
			try
			{
				PrefabCollection<BuildingInfo>.BeginSerialize(s);
				for (int k = 1; k < num; k++)
				{
					if (buffer[k].m_flags != Building.Flags.None)
					{
						PrefabCollection<BuildingInfo>.Serialize((uint)buffer[k].m_infoIndex);
					}
				}
				if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
				{
					for (int l = 0; l < num2; l++)
					{
						BuildingInfo prefab2 = PrefabCollection<BuildingInfo>.GetPrefab((uint)l);
						if (prefab2 != null && prefab2.m_notUsedGuide != null && prefab2.m_prefabDataIndex != -1)
						{
							PrefabCollection<BuildingInfo>.Serialize((uint)prefab2.m_prefabDataIndex);
						}
					}
					for (int m = 0; m < num2; m++)
					{
						BuildingInfo prefab3 = PrefabCollection<BuildingInfo>.GetPrefab((uint)m);
						if (prefab3 != null && prefab3.m_UnlockMilestone != null && prefab3.m_prefabDataIndex != -1)
						{
							PrefabCollection<BuildingInfo>.Serialize((uint)prefab3.m_prefabDataIndex);
						}
					}
				}
			}
			finally
			{
				PrefabCollection<BuildingInfo>.EndSerialize(s);
			}
			if ((instance.m_properties.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
			{
				for (int n = 0; n < num2; n++)
				{
					BuildingInfo prefab4 = PrefabCollection<BuildingInfo>.GetPrefab((uint)n);
					if (prefab4 != null && prefab4.m_notUsedGuide != null && prefab4.m_prefabDataIndex != -1)
					{
						s.WriteObject<BuildingTypeGuide>(prefab4.m_notUsedGuide);
					}
				}
				for (int num5 = 0; num5 < num2; num5++)
				{
					BuildingInfo prefab5 = PrefabCollection<BuildingInfo>.GetPrefab((uint)num5);
					if (prefab5 != null && prefab5.m_UnlockMilestone != null && prefab5.m_prefabDataIndex != -1)
					{
						s.WriteObject<MilestoneInfo.Data>(prefab5.m_UnlockMilestone.GetData());
					}
				}
			}
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingAbandoned1);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingAbandoned2);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingBurned);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingLevelUp);
			s.WriteObject<BuildingOnFireGuide>(instance2.m_buildingOnFire);
			s.WriteObject<GenericGuide>(instance2.m_buildNextToRoad);
			s.WriteObject<GenericGuide>(instance2.m_buildNextToWater);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_landfillSiteFull);
			s.WriteObject<GenericGuide>(instance2.m_windTurbinePlacement);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_cemeteryFull);
			s.WriteObject<GenericGuide>(instance2.m_harborPlacement);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_landfillSiteEmpty);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_cemeteryEmpty);
			s.WriteObject<GenericGuide>(instance2.m_heatingPlacement);
			s.WriteObject<GenericGuide>(instance2.m_roadMaintenancePlacement);
			s.WriteObject<GenericGuide>(instance2.m_tramDepotPlacement);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_depotNotConnected);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_snowDumpFull);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingFlooded);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingFlooded2);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_escapeRoutes);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingDestroyed);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_waterOutletOnLand);
			s.WriteObject<GenericGuide>(instance2.m_evacuationNotUsed);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_citizensInShelter);
			s.WriteObject<BuildingInstanceGuide>(instance2.m_buildingDestroyed2);
			s.WriteUInt32((uint)instance2.m_lastBuildingProblems);
			s.WriteUInt32((uint)instance2.m_currentBuildingProblems);
			EncodedArray.ULong uLong = EncodedArray.ULong.BeginWrite(s);
			for (int num6 = 1; num6 < num; num6++)
			{
				if (buffer[num6].m_flags != Building.Flags.None)
				{
					uLong.Write((ulong)buffer[num6].m_problems);
				}
			}
			uLong.EndWrite();
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int num7 = 1; num7 < num; num7++)
			{
				if (buffer[num7].m_flags != Building.Flags.None)
				{
					uShort.Write(buffer[num7].m_netNode);
				}
			}
			uShort.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int num8 = 1; num8 < num; num8++)
			{
				if (buffer[num8].m_flags != Building.Flags.None)
				{
					uShort2.Write(buffer[num8].m_subBuilding);
				}
			}
			uShort2.EndWrite();
			EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginWrite(s);
			for (int num9 = 1; num9 < num; num9++)
			{
				if (buffer[num9].m_flags != Building.Flags.None)
				{
					uShort3.Write(buffer[num9].m_waterSource);
				}
			}
			uShort3.EndWrite();
			EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginWrite(s);
			for (int num10 = 1; num10 < num; num10++)
			{
				if (buffer[num10].m_flags != Building.Flags.None)
				{
					uShort4.Write(buffer[num10].m_electricityBuffer);
				}
			}
			uShort4.EndWrite();
			EncodedArray.UShort uShort5 = EncodedArray.UShort.BeginWrite(s);
			for (int num11 = 1; num11 < num; num11++)
			{
				if (buffer[num11].m_flags != Building.Flags.None)
				{
					uShort5.Write(buffer[num11].m_waterBuffer);
				}
			}
			uShort5.EndWrite();
			EncodedArray.UShort uShort6 = EncodedArray.UShort.BeginWrite(s);
			for (int num12 = 1; num12 < num; num12++)
			{
				if (buffer[num12].m_flags != Building.Flags.None)
				{
					uShort6.Write(buffer[num12].m_sewageBuffer);
				}
			}
			uShort6.EndWrite();
			EncodedArray.UShort uShort7 = EncodedArray.UShort.BeginWrite(s);
			for (int num13 = 1; num13 < num; num13++)
			{
				if (buffer[num13].m_flags != Building.Flags.None)
				{
					uShort7.Write(buffer[num13].m_heatingBuffer);
				}
			}
			uShort7.EndWrite();
			EncodedArray.UShort uShort8 = EncodedArray.UShort.BeginWrite(s);
			for (int num14 = 1; num14 < num; num14++)
			{
				if (buffer[num14].m_flags != Building.Flags.None)
				{
					uShort8.Write(buffer[num14].m_garbageBuffer);
				}
			}
			uShort8.EndWrite();
			EncodedArray.UShort uShort9 = EncodedArray.UShort.BeginWrite(s);
			for (int num15 = 1; num15 < num; num15++)
			{
				if (buffer[num15].m_flags != Building.Flags.None)
				{
					uShort9.Write(buffer[num15].m_crimeBuffer);
				}
			}
			uShort9.EndWrite();
			EncodedArray.UShort uShort10 = EncodedArray.UShort.BeginWrite(s);
			for (int num16 = 1; num16 < num; num16++)
			{
				if (buffer[num16].m_flags != Building.Flags.None)
				{
					uShort10.Write(buffer[num16].m_customBuffer1);
				}
			}
			uShort10.EndWrite();
			EncodedArray.UShort uShort11 = EncodedArray.UShort.BeginWrite(s);
			for (int num17 = 1; num17 < num; num17++)
			{
				if (buffer[num17].m_flags != Building.Flags.None)
				{
					uShort11.Write(buffer[num17].m_customBuffer2);
				}
			}
			uShort11.EndWrite();
			EncodedArray.UShort uShort12 = EncodedArray.UShort.BeginWrite(s);
			for (int num18 = 1; num18 < num; num18++)
			{
				if (buffer[num18].m_flags != Building.Flags.None)
				{
					uShort12.Write(buffer[num18].m_buildWaterHeight);
				}
			}
			uShort12.EndWrite();
			EncodedArray.Byte @byte = EncodedArray.Byte.BeginWrite(s);
			for (int num19 = 1; num19 < num; num19++)
			{
				if (buffer[num19].m_flags != Building.Flags.None)
				{
					@byte.Write(buffer[num19].m_productionRate);
				}
			}
			@byte.EndWrite();
			EncodedArray.Byte byte2 = EncodedArray.Byte.BeginWrite(s);
			for (int num20 = 1; num20 < num; num20++)
			{
				if (buffer[num20].m_flags != Building.Flags.None)
				{
					byte2.Write(buffer[num20].m_waterPollution);
				}
			}
			byte2.EndWrite();
			EncodedArray.Byte byte3 = EncodedArray.Byte.BeginWrite(s);
			for (int num21 = 1; num21 < num; num21++)
			{
				if (buffer[num21].m_flags != Building.Flags.None)
				{
					byte3.Write(buffer[num21].GetLastFrameData().m_constructState);
				}
			}
			byte3.EndWrite();
			EncodedArray.Byte byte4 = EncodedArray.Byte.BeginWrite(s);
			for (int num22 = 1; num22 < num; num22++)
			{
				if (buffer[num22].m_flags != Building.Flags.None)
				{
					byte4.Write(buffer[num22].m_fireIntensity);
				}
			}
			byte4.EndWrite();
			EncodedArray.Byte byte5 = EncodedArray.Byte.BeginWrite(s);
			for (int num23 = 1; num23 < num; num23++)
			{
				if (buffer[num23].m_flags != Building.Flags.None)
				{
					byte5.Write(buffer[num23].GetLastFrameData().m_fireDamage);
				}
			}
			byte5.EndWrite();
			EncodedArray.Byte byte6 = EncodedArray.Byte.BeginWrite(s);
			for (int num24 = 1; num24 < num; num24++)
			{
				if (buffer[num24].m_flags != Building.Flags.None)
				{
					byte6.Write(buffer[num24].GetLastFrameData().m_productionState);
				}
			}
			byte6.EndWrite();
			EncodedArray.Byte byte7 = EncodedArray.Byte.BeginWrite(s);
			for (int num25 = 1; num25 < num; num25++)
			{
				if (buffer[num25].m_flags != Building.Flags.None)
				{
					byte7.Write(buffer[num25].m_health);
				}
			}
			byte7.EndWrite();
			EncodedArray.Byte byte8 = EncodedArray.Byte.BeginWrite(s);
			for (int num26 = 1; num26 < num; num26++)
			{
				if (buffer[num26].m_flags != Building.Flags.None)
				{
					byte8.Write(buffer[num26].m_happiness);
				}
			}
			byte8.EndWrite();
			EncodedArray.Byte byte9 = EncodedArray.Byte.BeginWrite(s);
			for (int num27 = 1; num27 < num; num27++)
			{
				if (buffer[num27].m_flags != Building.Flags.None)
				{
					byte9.Write(buffer[num27].m_citizenCount);
				}
			}
			byte9.EndWrite();
			EncodedArray.Byte byte10 = EncodedArray.Byte.BeginWrite(s);
			for (int num28 = 1; num28 < num; num28++)
			{
				if (buffer[num28].m_flags != Building.Flags.None)
				{
					byte10.Write(buffer[num28].m_tempImport);
				}
			}
			byte10.EndWrite();
			EncodedArray.Byte byte11 = EncodedArray.Byte.BeginWrite(s);
			for (int num29 = 1; num29 < num; num29++)
			{
				if (buffer[num29].m_flags != Building.Flags.None)
				{
					byte11.Write(buffer[num29].m_tempExport);
				}
			}
			byte11.EndWrite();
			EncodedArray.Byte byte12 = EncodedArray.Byte.BeginWrite(s);
			for (int num30 = 1; num30 < num; num30++)
			{
				if (buffer[num30].m_flags != Building.Flags.None)
				{
					byte12.Write(buffer[num30].m_finalImport);
				}
			}
			byte12.EndWrite();
			EncodedArray.Byte byte13 = EncodedArray.Byte.BeginWrite(s);
			for (int num31 = 1; num31 < num; num31++)
			{
				if (buffer[num31].m_flags != Building.Flags.None)
				{
					byte13.Write(buffer[num31].m_finalExport);
				}
			}
			byte13.EndWrite();
			EncodedArray.Byte byte14 = EncodedArray.Byte.BeginWrite(s);
			for (int num32 = 1; num32 < num; num32++)
			{
				if (buffer[num32].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num32].m_electricityProblemTimer);
				}
			}
			for (int num33 = 1; num33 < num; num33++)
			{
				if (buffer[num33].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num33].m_heatingProblemTimer);
				}
			}
			for (int num34 = 1; num34 < num; num34++)
			{
				if (buffer[num34].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num34].m_waterProblemTimer);
				}
			}
			for (int num35 = 1; num35 < num; num35++)
			{
				if (buffer[num35].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num35].m_workerProblemTimer);
				}
			}
			for (int num36 = 1; num36 < num; num36++)
			{
				if (buffer[num36].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num36].m_incomingProblemTimer);
				}
			}
			for (int num37 = 1; num37 < num; num37++)
			{
				if (buffer[num37].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num37].m_outgoingProblemTimer);
				}
			}
			for (int num38 = 1; num38 < num; num38++)
			{
				if (buffer[num38].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num38].m_healthProblemTimer);
				}
			}
			for (int num39 = 1; num39 < num; num39++)
			{
				if (buffer[num39].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num39].m_deathProblemTimer);
				}
			}
			for (int num40 = 1; num40 < num; num40++)
			{
				if (buffer[num40].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num40].m_serviceProblemTimer);
				}
			}
			for (int num41 = 1; num41 < num; num41++)
			{
				if (buffer[num41].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num41].m_taxProblemTimer);
				}
			}
			for (int num42 = 1; num42 < num; num42++)
			{
				if (buffer[num42].m_flags != Building.Flags.None)
				{
					byte14.Write(buffer[num42].m_majorProblemTimer);
				}
			}
			byte14.EndWrite();
			EncodedArray.Byte byte15 = EncodedArray.Byte.BeginWrite(s);
			for (int num43 = 1; num43 < num; num43++)
			{
				if (buffer[num43].m_flags != Building.Flags.None)
				{
					byte15.Write(buffer[num43].m_levelUpProgress);
				}
			}
			byte15.EndWrite();
			EncodedArray.Byte byte16 = EncodedArray.Byte.BeginWrite(s);
			for (int num44 = 1; num44 < num; num44++)
			{
				if (buffer[num44].m_flags != Building.Flags.None)
				{
					byte16.Write(buffer[num44].m_width);
				}
			}
			byte16.EndWrite();
			EncodedArray.Byte byte17 = EncodedArray.Byte.BeginWrite(s);
			for (int num45 = 1; num45 < num; num45++)
			{
				if (buffer[num45].m_flags != Building.Flags.None)
				{
					byte17.Write(buffer[num45].m_length);
				}
			}
			byte17.EndWrite();
			EncodedArray.Byte byte18 = EncodedArray.Byte.BeginWrite(s);
			for (int num46 = 1; num46 < num; num46++)
			{
				if (buffer[num46].m_flags != Building.Flags.None)
				{
					byte18.Write(buffer[num46].m_education1);
				}
			}
			byte18.EndWrite();
			EncodedArray.Byte byte19 = EncodedArray.Byte.BeginWrite(s);
			for (int num47 = 1; num47 < num; num47++)
			{
				if (buffer[num47].m_flags != Building.Flags.None)
				{
					byte19.Write(buffer[num47].m_education2);
				}
			}
			byte19.EndWrite();
			EncodedArray.Byte byte20 = EncodedArray.Byte.BeginWrite(s);
			for (int num48 = 1; num48 < num; num48++)
			{
				if (buffer[num48].m_flags != Building.Flags.None)
				{
					byte20.Write(buffer[num48].m_education3);
				}
			}
			byte20.EndWrite();
			EncodedArray.Byte byte21 = EncodedArray.Byte.BeginWrite(s);
			for (int num49 = 1; num49 < num; num49++)
			{
				if (buffer[num49].m_flags != Building.Flags.None)
				{
					byte21.Write(buffer[num49].m_teens);
				}
			}
			byte21.EndWrite();
			EncodedArray.Byte byte22 = EncodedArray.Byte.BeginWrite(s);
			for (int num50 = 1; num50 < num; num50++)
			{
				if (buffer[num50].m_flags != Building.Flags.None)
				{
					byte22.Write(buffer[num50].m_youngs);
				}
			}
			byte22.EndWrite();
			EncodedArray.Byte byte23 = EncodedArray.Byte.BeginWrite(s);
			for (int num51 = 1; num51 < num; num51++)
			{
				if (buffer[num51].m_flags != Building.Flags.None)
				{
					byte23.Write(buffer[num51].m_adults);
				}
			}
			byte23.EndWrite();
			EncodedArray.Byte byte24 = EncodedArray.Byte.BeginWrite(s);
			for (int num52 = 1; num52 < num; num52++)
			{
				if (buffer[num52].m_flags != Building.Flags.None)
				{
					byte24.Write(buffer[num52].m_seniors);
				}
			}
			byte24.EndWrite();
			EncodedArray.Byte byte25 = EncodedArray.Byte.BeginWrite(s);
			for (int num53 = 1; num53 < num; num53++)
			{
				if (buffer[num53].m_flags != Building.Flags.None)
				{
					byte25.Write(buffer[num53].m_fireHazard);
				}
			}
			byte25.EndWrite();
			EncodedArray.Byte byte26 = EncodedArray.Byte.BeginWrite(s);
			for (int num54 = 1; num54 < num; num54++)
			{
				if (buffer[num54].m_flags != Building.Flags.None)
				{
					byte26.Write(buffer[num54].m_subCulture);
				}
			}
			byte26.EndWrite();
			for (int num55 = 1; num55 < num; num55++)
			{
				if (buffer[num55].m_flags != Building.Flags.None)
				{
					s.WriteUInt32(buffer[num55].m_buildIndex);
					s.WriteVector3(buffer[num55].m_position);
					s.WriteFloat(buffer[num55].m_angle);
					s.WriteUInt8((uint)buffer[num55].m_baseHeight);
					s.WriteUInt24(buffer[num55].m_citizenUnits);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "BuildingManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "BuildingManager");
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Building[] buffer = instance.m_buildings.m_buffer;
			ushort[] buildingGrid = instance.m_buildingGrid;
			ushort[] buildingGrid2 = instance.m_buildingGrid2;
			int num = buffer.Length;
			int num2 = buildingGrid.Length;
			int num3 = buildingGrid2.Length;
			instance.m_buildings.ClearUnused();
			for (int i = 0; i < num2; i++)
			{
				buildingGrid[i] = 0;
			}
			for (int j = 0; j < num3; j++)
			{
				buildingGrid2[j] = 0;
			}
			for (int k = 0; k < 12; k++)
			{
				instance.m_serviceBuildings[k].Clear();
			}
			while (!Monitor.TryEnter(instance.m_outsideConnections, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				instance.m_outsideConnections.Clear();
			}
			finally
			{
				Monitor.Exit(instance.m_outsideConnections);
			}
			if (s.get_version() >= 222u)
			{
				EncodedArray.UInt uInt = EncodedArray.UInt.BeginRead(s);
				for (int l = 1; l < num; l++)
				{
					buffer[l].m_flags = (Building.Flags)uInt.Read();
				}
				uInt.EndRead();
			}
			else
			{
				EncodedArray.UInt uInt2 = EncodedArray.UInt.BeginRead(s);
				for (int m = 1; m < 32768; m++)
				{
					buffer[m].m_flags = (Building.Flags)uInt2.Read();
				}
				for (int n = 32768; n < num; n++)
				{
					buffer[n].m_flags = Building.Flags.None;
				}
				uInt2.EndRead();
			}
			uint num4 = 0u;
			uint num5 = 0u;
			if (s.get_version() >= 82u)
			{
				num4 = s.ReadUInt16();
				this.m_guideInfoIndex = new uint[num4];
				this.m_buildingNotUsedGuides = new BuildingTypeGuide[num4];
			}
			if (s.get_version() >= 128u)
			{
				num5 = s.ReadUInt16();
				this.m_milestoneInfoIndex = new uint[num5];
				this.m_UnlockMilestones = new MilestoneInfo.Data[num5];
			}
			if (s.get_version() >= 18u)
			{
				PrefabCollection<BuildingInfo>.BeginDeserialize(s);
				for (int num6 = 1; num6 < num; num6++)
				{
					if (buffer[num6].m_flags != Building.Flags.None)
					{
						buffer[num6].m_infoIndex = (ushort)PrefabCollection<BuildingInfo>.Deserialize(true);
					}
				}
				for (uint num7 = 0u; num7 < num4; num7 += 1u)
				{
					this.m_guideInfoIndex[(int)((UIntPtr)num7)] = PrefabCollection<BuildingInfo>.Deserialize(false);
				}
				for (uint num8 = 0u; num8 < num5; num8 += 1u)
				{
					this.m_milestoneInfoIndex[(int)((UIntPtr)num8)] = PrefabCollection<BuildingInfo>.Deserialize(false);
				}
				PrefabCollection<BuildingInfo>.EndDeserialize(s);
			}
			for (uint num9 = 0u; num9 < num4; num9 += 1u)
			{
				this.m_buildingNotUsedGuides[(int)((UIntPtr)num9)] = s.ReadObject<BuildingTypeGuide>();
			}
			for (uint num10 = 0u; num10 < num5; num10 += 1u)
			{
				this.m_UnlockMilestones[(int)((UIntPtr)num10)] = s.ReadObject<MilestoneInfo.Data>();
			}
			if (s.get_version() >= 82u)
			{
				instance.m_buildingAbandoned1 = s.ReadObject<BuildingInstanceGuide>();
				instance.m_buildingAbandoned2 = s.ReadObject<BuildingInstanceGuide>();
				instance.m_buildingBurned = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_buildingAbandoned1 = null;
				instance.m_buildingAbandoned2 = null;
				instance.m_buildingBurned = null;
			}
			if (s.get_version() >= 198u)
			{
				instance.m_buildingLevelUp = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_buildingLevelUp = null;
			}
			if (s.get_version() >= 87u)
			{
				instance.m_buildingOnFire = s.ReadObject<BuildingOnFireGuide>();
				instance.m_buildNextToRoad = s.ReadObject<GenericGuide>();
				instance.m_buildNextToWater = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_buildingOnFire = null;
				instance.m_buildNextToRoad = null;
				instance.m_buildNextToWater = null;
			}
			if (s.get_version() >= 156u)
			{
				instance.m_landfillSiteFull = s.ReadObject<BuildingInstanceGuide>();
				instance.m_windTurbinePlacement = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_landfillSiteFull = null;
				instance.m_windTurbinePlacement = null;
			}
			if (s.get_version() >= 166u)
			{
				instance.m_cemeteryFull = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_cemeteryFull = null;
			}
			if (s.get_version() >= 160u)
			{
				instance.m_harborPlacement = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_harborPlacement = null;
			}
			if (s.get_version() >= 188u)
			{
				instance.m_landfillSiteEmpty = s.ReadObject<BuildingInstanceGuide>();
				instance.m_cemeteryEmpty = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_landfillSiteEmpty = null;
				instance.m_cemeteryEmpty = null;
			}
			if (s.get_version() >= 237u)
			{
				instance.m_heatingPlacement = s.ReadObject<GenericGuide>();
				instance.m_roadMaintenancePlacement = s.ReadObject<GenericGuide>();
				instance.m_tramDepotPlacement = s.ReadObject<GenericGuide>();
				instance.m_depotNotConnected = s.ReadObject<BuildingInstanceGuide>();
				instance.m_snowDumpFull = s.ReadObject<BuildingInstanceGuide>();
				instance.m_buildingFlooded = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_heatingPlacement = null;
				instance.m_roadMaintenancePlacement = null;
				instance.m_tramDepotPlacement = null;
				instance.m_depotNotConnected = null;
				instance.m_snowDumpFull = null;
				instance.m_buildingFlooded = null;
			}
			if (s.get_version() >= 295u)
			{
				instance.m_buildingFlooded2 = s.ReadObject<BuildingInstanceGuide>();
				instance.m_escapeRoutes = s.ReadObject<BuildingInstanceGuide>();
				instance.m_buildingDestroyed = s.ReadObject<BuildingInstanceGuide>();
				instance.m_waterOutletOnLand = s.ReadObject<BuildingInstanceGuide>();
				instance.m_evacuationNotUsed = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_buildingFlooded2 = null;
				instance.m_escapeRoutes = null;
				instance.m_buildingDestroyed = null;
				instance.m_waterOutletOnLand = null;
				instance.m_evacuationNotUsed = null;
			}
			if (s.get_version() >= 304u)
			{
				instance.m_citizensInShelter = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_citizensInShelter = null;
			}
			if (s.get_version() >= 309u)
			{
				instance.m_buildingDestroyed2 = s.ReadObject<BuildingInstanceGuide>();
			}
			else
			{
				instance.m_buildingDestroyed2 = null;
			}
			if (s.get_version() >= 134u)
			{
				instance.m_lastBuildingProblems = (Notification.Problem)s.ReadUInt32();
				instance.m_currentBuildingProblems = (Notification.Problem)s.ReadUInt32();
			}
			else
			{
				instance.m_lastBuildingProblems = Notification.Problem.None;
				instance.m_currentBuildingProblems = Notification.Problem.None;
			}
			if (s.get_version() >= 221u && Singleton<SimulationManager>.get_instance().m_metaData.m_saveAppVersion >= BuildConfig.MakeVersionNumber(1u, 3u, 0u, BuildConfig.ReleaseType.Prototype, 1u, BuildConfig.BuildType.Unknown))
			{
				EncodedArray.ULong uLong = EncodedArray.ULong.BeginRead(s);
				for (int num11 = 1; num11 < num; num11++)
				{
					if (buffer[num11].m_flags != Building.Flags.None)
					{
						buffer[num11].m_problems = (Notification.Problem)uLong.Read();
					}
					else
					{
						buffer[num11].m_problems = Notification.Problem.None;
					}
				}
				uLong.EndRead();
			}
			else if (s.get_version() >= 39u)
			{
				EncodedArray.UInt uInt3 = EncodedArray.UInt.BeginRead(s);
				for (int num12 = 1; num12 < num; num12++)
				{
					if (buffer[num12].m_flags != Building.Flags.None)
					{
						ulong num13 = (ulong)uInt3.Read();
						num13 = ((num13 & (ulong)-1073741824) << 32 | (num13 & 1073741823uL));
						buffer[num12].m_problems = (Notification.Problem)num13;
					}
					else
					{
						buffer[num12].m_problems = Notification.Problem.None;
					}
				}
				uInt3.EndRead();
			}
			else
			{
				for (int num14 = 1; num14 < num; num14++)
				{
					buffer[num14].m_problems = Notification.Problem.None;
				}
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
				for (int num15 = 1; num15 < num; num15++)
				{
					if (buffer[num15].m_flags != Building.Flags.None)
					{
						buffer[num15].m_netNode = uShort.Read();
					}
					else
					{
						buffer[num15].m_netNode = 0;
					}
				}
				uShort.EndRead();
			}
			if (s.get_version() >= 208u)
			{
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
				for (int num16 = 1; num16 < num; num16++)
				{
					if (buffer[num16].m_flags != Building.Flags.None)
					{
						buffer[num16].m_subBuilding = uShort2.Read();
					}
					else
					{
						buffer[num16].m_subBuilding = 0;
					}
				}
				uShort2.EndRead();
			}
			else
			{
				for (int num17 = 1; num17 < num; num17++)
				{
					buffer[num17].m_subBuilding = 0;
				}
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort3 = EncodedArray.UShort.BeginRead(s);
				for (int num18 = 1; num18 < num; num18++)
				{
					if (buffer[num18].m_flags != Building.Flags.None)
					{
						buffer[num18].m_waterSource = uShort3.Read();
					}
					else
					{
						buffer[num18].m_waterSource = 0;
					}
				}
				uShort3.EndRead();
			}
			if (s.get_version() >= 95u && s.get_version() < 184u)
			{
				EncodedArray.UShort uShort4 = EncodedArray.UShort.BeginRead(s);
				for (int num19 = 1; num19 < num; num19++)
				{
					if (buffer[num19].m_flags != Building.Flags.None)
					{
						ushort num20 = uShort4.Read();
						if (num20 != 0)
						{
							Singleton<TerrainManager>.get_instance().WaterSimulation.ReleaseWaterSource(num20);
						}
					}
				}
				uShort4.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort5 = EncodedArray.UShort.BeginRead(s);
				for (int num21 = 1; num21 < num; num21++)
				{
					if (buffer[num21].m_flags != Building.Flags.None)
					{
						buffer[num21].m_electricityBuffer = uShort5.Read();
					}
					else
					{
						buffer[num21].m_electricityBuffer = 0;
					}
				}
				uShort5.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort6 = EncodedArray.UShort.BeginRead(s);
				for (int num22 = 1; num22 < num; num22++)
				{
					if (buffer[num22].m_flags != Building.Flags.None)
					{
						buffer[num22].m_waterBuffer = uShort6.Read();
					}
					else
					{
						buffer[num22].m_waterBuffer = 0;
					}
				}
				uShort6.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort7 = EncodedArray.UShort.BeginRead(s);
				for (int num23 = 1; num23 < num; num23++)
				{
					if (buffer[num23].m_flags != Building.Flags.None)
					{
						buffer[num23].m_sewageBuffer = uShort7.Read();
					}
					else
					{
						buffer[num23].m_sewageBuffer = 0;
					}
				}
				uShort7.EndRead();
			}
			if (s.get_version() >= 227u)
			{
				EncodedArray.UShort uShort8 = EncodedArray.UShort.BeginRead(s);
				for (int num24 = 1; num24 < num; num24++)
				{
					if (buffer[num24].m_flags != Building.Flags.None)
					{
						buffer[num24].m_heatingBuffer = uShort8.Read();
					}
					else
					{
						buffer[num24].m_heatingBuffer = 0;
					}
				}
				uShort8.EndRead();
			}
			else
			{
				for (int num25 = 1; num25 < num; num25++)
				{
					buffer[num25].m_heatingBuffer = 0;
				}
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort9 = EncodedArray.UShort.BeginRead(s);
				for (int num26 = 1; num26 < num; num26++)
				{
					if (buffer[num26].m_flags != Building.Flags.None)
					{
						buffer[num26].m_garbageBuffer = uShort9.Read();
					}
					else
					{
						buffer[num26].m_garbageBuffer = 0;
					}
				}
				uShort9.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort10 = EncodedArray.UShort.BeginRead(s);
				for (int num27 = 1; num27 < num; num27++)
				{
					if (buffer[num27].m_flags != Building.Flags.None)
					{
						buffer[num27].m_crimeBuffer = uShort10.Read();
					}
					else
					{
						buffer[num27].m_crimeBuffer = 0;
					}
				}
				uShort10.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.UShort uShort11 = EncodedArray.UShort.BeginRead(s);
				for (int num28 = 1; num28 < num; num28++)
				{
					if (buffer[num28].m_flags != Building.Flags.None)
					{
						buffer[num28].m_customBuffer1 = uShort11.Read();
					}
					else
					{
						buffer[num28].m_customBuffer1 = 0;
					}
				}
				uShort11.EndRead();
			}
			if (s.get_version() >= 66u)
			{
				EncodedArray.UShort uShort12 = EncodedArray.UShort.BeginRead(s);
				for (int num29 = 1; num29 < num; num29++)
				{
					if (buffer[num29].m_flags != Building.Flags.None)
					{
						buffer[num29].m_customBuffer2 = uShort12.Read();
					}
					else
					{
						buffer[num29].m_customBuffer2 = 0;
					}
				}
				uShort12.EndRead();
			}
			else
			{
				for (int num30 = 1; num30 < num; num30++)
				{
					buffer[num30].m_customBuffer2 = 0;
				}
			}
			if (s.get_version() >= 308u)
			{
				EncodedArray.UShort uShort13 = EncodedArray.UShort.BeginRead(s);
				for (int num31 = 1; num31 < num; num31++)
				{
					if (buffer[num31].m_flags != Building.Flags.None)
					{
						buffer[num31].m_buildWaterHeight = uShort13.Read();
					}
					else
					{
						buffer[num31].m_buildWaterHeight = 0;
					}
				}
				uShort13.EndRead();
			}
			else
			{
				for (int num32 = 1; num32 < num; num32++)
				{
					buffer[num32].m_buildWaterHeight = 0;
				}
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.Byte @byte = EncodedArray.Byte.BeginRead(s);
				for (int num33 = 1; num33 < num; num33++)
				{
					if (buffer[num33].m_flags != Building.Flags.None)
					{
						buffer[num33].m_productionRate = @byte.Read();
					}
					else
					{
						buffer[num33].m_productionRate = 0;
					}
				}
				@byte.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.Byte byte2 = EncodedArray.Byte.BeginRead(s);
				for (int num34 = 1; num34 < num; num34++)
				{
					if (buffer[num34].m_flags != Building.Flags.None)
					{
						buffer[num34].m_waterPollution = byte2.Read();
					}
					else
					{
						buffer[num34].m_waterPollution = 0;
					}
				}
				byte2.EndRead();
			}
			if (s.get_version() >= 33u)
			{
				EncodedArray.Byte byte3 = EncodedArray.Byte.BeginRead(s);
				for (int num35 = 1; num35 < num; num35++)
				{
					if (buffer[num35].m_flags != Building.Flags.None)
					{
						buffer[num35].m_frame0.m_constructState = byte3.Read();
					}
					else
					{
						buffer[num35].m_frame0.m_constructState = 0;
					}
				}
				byte3.EndRead();
			}
			if (s.get_version() >= 35u)
			{
				EncodedArray.Byte byte4 = EncodedArray.Byte.BeginRead(s);
				for (int num36 = 1; num36 < num; num36++)
				{
					if (buffer[num36].m_flags != Building.Flags.None)
					{
						buffer[num36].m_fireIntensity = byte4.Read();
					}
					else
					{
						buffer[num36].m_fireIntensity = 0;
					}
				}
				byte4.EndRead();
			}
			if (s.get_version() >= 35u)
			{
				EncodedArray.Byte byte5 = EncodedArray.Byte.BeginRead(s);
				for (int num37 = 1; num37 < num; num37++)
				{
					if (buffer[num37].m_flags != Building.Flags.None)
					{
						buffer[num37].m_frame0.m_fireDamage = byte5.Read();
						if (buffer[num37].m_frame0.m_fireDamage == 255)
						{
							Building[] expr_FA8_cp_0 = buffer;
							int expr_FA8_cp_1 = num37;
							expr_FA8_cp_0[expr_FA8_cp_1].m_flags = (expr_FA8_cp_0[expr_FA8_cp_1].m_flags | Building.Flags.Collapsed);
						}
					}
					else
					{
						buffer[num37].m_frame0.m_fireDamage = 0;
					}
				}
				byte5.EndRead();
			}
			if (s.get_version() >= 81u)
			{
				EncodedArray.Byte byte6 = EncodedArray.Byte.BeginRead(s);
				for (int num38 = 1; num38 < num; num38++)
				{
					if (buffer[num38].m_flags != Building.Flags.None)
					{
						buffer[num38].m_frame0.m_productionState = byte6.Read();
					}
					else
					{
						buffer[num38].m_frame0.m_productionState = 0;
					}
				}
				byte6.EndRead();
			}
			if (s.get_version() >= 55u)
			{
				EncodedArray.Byte byte7 = EncodedArray.Byte.BeginRead(s);
				for (int num39 = 1; num39 < num; num39++)
				{
					if (buffer[num39].m_flags != Building.Flags.None)
					{
						buffer[num39].m_health = byte7.Read();
					}
					else
					{
						buffer[num39].m_health = 0;
					}
				}
				byte7.EndRead();
			}
			if (s.get_version() >= 55u)
			{
				EncodedArray.Byte byte8 = EncodedArray.Byte.BeginRead(s);
				for (int num40 = 1; num40 < num; num40++)
				{
					if (buffer[num40].m_flags != Building.Flags.None)
					{
						buffer[num40].m_happiness = byte8.Read();
					}
					else
					{
						buffer[num40].m_happiness = 0;
					}
				}
				byte8.EndRead();
			}
			if (s.get_version() >= 55u)
			{
				EncodedArray.Byte byte9 = EncodedArray.Byte.BeginRead(s);
				for (int num41 = 1; num41 < num; num41++)
				{
					if (buffer[num41].m_flags != Building.Flags.None)
					{
						buffer[num41].m_citizenCount = byte9.Read();
					}
					else
					{
						buffer[num41].m_citizenCount = 0;
					}
				}
				byte9.EndRead();
			}
			if (s.get_version() >= 72u)
			{
				EncodedArray.Byte byte10 = EncodedArray.Byte.BeginRead(s);
				for (int num42 = 1; num42 < num; num42++)
				{
					if (buffer[num42].m_flags != Building.Flags.None)
					{
						buffer[num42].m_tempImport = byte10.Read();
					}
					else
					{
						buffer[num42].m_tempImport = 0;
					}
				}
				byte10.EndRead();
			}
			if (s.get_version() >= 72u)
			{
				EncodedArray.Byte byte11 = EncodedArray.Byte.BeginRead(s);
				for (int num43 = 1; num43 < num; num43++)
				{
					if (buffer[num43].m_flags != Building.Flags.None)
					{
						buffer[num43].m_tempExport = byte11.Read();
					}
					else
					{
						buffer[num43].m_tempExport = 0;
					}
				}
				byte11.EndRead();
			}
			if (s.get_version() >= 72u)
			{
				EncodedArray.Byte byte12 = EncodedArray.Byte.BeginRead(s);
				for (int num44 = 1; num44 < num; num44++)
				{
					if (buffer[num44].m_flags != Building.Flags.None)
					{
						buffer[num44].m_finalImport = byte12.Read();
					}
					else
					{
						buffer[num44].m_finalImport = 0;
					}
				}
				byte12.EndRead();
			}
			if (s.get_version() >= 72u)
			{
				EncodedArray.Byte byte13 = EncodedArray.Byte.BeginRead(s);
				for (int num45 = 1; num45 < num; num45++)
				{
					if (buffer[num45].m_flags != Building.Flags.None)
					{
						buffer[num45].m_finalExport = byte13.Read();
					}
					else
					{
						buffer[num45].m_finalExport = 0;
					}
				}
				byte13.EndRead();
			}
			if (s.get_version() >= 78u)
			{
				EncodedArray.Byte byte14 = EncodedArray.Byte.BeginRead(s);
				if (s.get_version() >= 132u)
				{
					for (int num46 = 1; num46 < num; num46++)
					{
						if (buffer[num46].m_flags != Building.Flags.None)
						{
							buffer[num46].m_electricityProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num46].m_electricityProblemTimer = 0;
						}
					}
					for (int num47 = 1; num47 < num; num47++)
					{
						if (buffer[num47].m_flags != Building.Flags.None && s.get_version() >= 231u)
						{
							buffer[num47].m_heatingProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num47].m_heatingProblemTimer = 0;
						}
					}
					for (int num48 = 1; num48 < num; num48++)
					{
						if (buffer[num48].m_flags != Building.Flags.None)
						{
							buffer[num48].m_waterProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num48].m_waterProblemTimer = 0;
						}
					}
					for (int num49 = 1; num49 < num; num49++)
					{
						if (buffer[num49].m_flags != Building.Flags.None)
						{
							buffer[num49].m_workerProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num49].m_workerProblemTimer = 0;
						}
					}
					for (int num50 = 1; num50 < num; num50++)
					{
						if (buffer[num50].m_flags != Building.Flags.None)
						{
							buffer[num50].m_incomingProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num50].m_incomingProblemTimer = 0;
						}
					}
					for (int num51 = 1; num51 < num; num51++)
					{
						if (buffer[num51].m_flags != Building.Flags.None)
						{
							buffer[num51].m_outgoingProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num51].m_outgoingProblemTimer = 0;
						}
					}
					for (int num52 = 1; num52 < num; num52++)
					{
						if (buffer[num52].m_flags != Building.Flags.None)
						{
							buffer[num52].m_healthProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num52].m_healthProblemTimer = 0;
						}
					}
					for (int num53 = 1; num53 < num; num53++)
					{
						if (buffer[num53].m_flags != Building.Flags.None)
						{
							buffer[num53].m_deathProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num53].m_deathProblemTimer = 0;
						}
					}
					for (int num54 = 1; num54 < num; num54++)
					{
						if (buffer[num54].m_flags != Building.Flags.None)
						{
							buffer[num54].m_serviceProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num54].m_serviceProblemTimer = 0;
						}
					}
				}
				else
				{
					for (int num55 = 1; num55 < num; num55++)
					{
						buffer[num55].m_electricityProblemTimer = 0;
						buffer[num55].m_waterProblemTimer = 0;
						buffer[num55].m_workerProblemTimer = 0;
						buffer[num55].m_incomingProblemTimer = 0;
						buffer[num55].m_outgoingProblemTimer = 0;
						buffer[num55].m_healthProblemTimer = 0;
						buffer[num55].m_deathProblemTimer = 0;
						buffer[num55].m_serviceProblemTimer = 0;
					}
				}
				if (s.get_version() >= 172u)
				{
					for (int num56 = 1; num56 < num; num56++)
					{
						if (buffer[num56].m_flags != Building.Flags.None)
						{
							buffer[num56].m_taxProblemTimer = byte14.Read();
						}
						else
						{
							buffer[num56].m_taxProblemTimer = 0;
						}
					}
				}
				else
				{
					for (int num57 = 1; num57 < num; num57++)
					{
						buffer[num57].m_taxProblemTimer = 0;
					}
				}
				for (int num58 = 1; num58 < num; num58++)
				{
					if (buffer[num58].m_flags != Building.Flags.None)
					{
						buffer[num58].m_majorProblemTimer = byte14.Read();
					}
					else
					{
						buffer[num58].m_majorProblemTimer = 0;
					}
				}
				byte14.EndRead();
			}
			if (s.get_version() >= 170u)
			{
				EncodedArray.Byte byte15 = EncodedArray.Byte.BeginRead(s);
				for (int num59 = 1; num59 < num; num59++)
				{
					if (buffer[num59].m_flags != Building.Flags.None)
					{
						buffer[num59].m_levelUpProgress = byte15.Read();
					}
					else
					{
						buffer[num59].m_levelUpProgress = 0;
					}
				}
				byte15.EndRead();
			}
			else
			{
				for (int num60 = 1; num60 < num; num60++)
				{
					buffer[num60].m_levelUpProgress = 0;
				}
			}
			if (s.get_version() >= 78u)
			{
				EncodedArray.Byte byte16 = EncodedArray.Byte.BeginRead(s);
				for (int num61 = 1; num61 < num; num61++)
				{
					if (buffer[num61].m_flags != Building.Flags.None)
					{
						buffer[num61].m_width = byte16.Read();
					}
					else
					{
						buffer[num61].m_width = 0;
					}
				}
				byte16.EndRead();
			}
			else
			{
				for (int num62 = 1; num62 < num; num62++)
				{
					uint num63 = (uint)buffer[num62].m_flags;
					buffer[num62].m_width = (byte)(((num63 & 3840u) >> 8) + 1u);
					num63 &= 4294963455u;
					buffer[num62].m_flags = (Building.Flags)num63;
				}
			}
			if (s.get_version() >= 78u)
			{
				EncodedArray.Byte byte17 = EncodedArray.Byte.BeginRead(s);
				for (int num64 = 1; num64 < num; num64++)
				{
					if (buffer[num64].m_flags != Building.Flags.None)
					{
						buffer[num64].m_length = byte17.Read();
					}
					else
					{
						buffer[num64].m_length = 0;
					}
				}
				byte17.EndRead();
			}
			else
			{
				for (int num65 = 1; num65 < num; num65++)
				{
					uint num66 = (uint)buffer[num65].m_flags;
					buffer[num65].m_length = (byte)(((num66 & 61440u) >> 12) + 1u);
					num66 &= 4294905855u;
					buffer[num65].m_flags = (Building.Flags)num66;
				}
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte18 = EncodedArray.Byte.BeginRead(s);
				for (int num67 = 1; num67 < num; num67++)
				{
					if (buffer[num67].m_flags != Building.Flags.None)
					{
						buffer[num67].m_education1 = byte18.Read();
					}
					else
					{
						buffer[num67].m_education1 = 0;
					}
				}
				byte18.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte19 = EncodedArray.Byte.BeginRead(s);
				for (int num68 = 1; num68 < num; num68++)
				{
					if (buffer[num68].m_flags != Building.Flags.None)
					{
						buffer[num68].m_education2 = byte19.Read();
					}
					else
					{
						buffer[num68].m_education2 = 0;
					}
				}
				byte19.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte20 = EncodedArray.Byte.BeginRead(s);
				for (int num69 = 1; num69 < num; num69++)
				{
					if (buffer[num69].m_flags != Building.Flags.None)
					{
						buffer[num69].m_education3 = byte20.Read();
					}
					else
					{
						buffer[num69].m_education3 = 0;
					}
				}
				byte20.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte21 = EncodedArray.Byte.BeginRead(s);
				for (int num70 = 1; num70 < num; num70++)
				{
					if (buffer[num70].m_flags != Building.Flags.None)
					{
						buffer[num70].m_teens = byte21.Read();
					}
					else
					{
						buffer[num70].m_teens = 0;
					}
				}
				byte21.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte22 = EncodedArray.Byte.BeginRead(s);
				for (int num71 = 1; num71 < num; num71++)
				{
					if (buffer[num71].m_flags != Building.Flags.None)
					{
						buffer[num71].m_youngs = byte22.Read();
					}
					else
					{
						buffer[num71].m_youngs = 0;
					}
				}
				byte22.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte23 = EncodedArray.Byte.BeginRead(s);
				for (int num72 = 1; num72 < num; num72++)
				{
					if (buffer[num72].m_flags != Building.Flags.None)
					{
						buffer[num72].m_adults = byte23.Read();
					}
					else
					{
						buffer[num72].m_adults = 0;
					}
				}
				byte23.EndRead();
			}
			if (s.get_version() >= 98u)
			{
				EncodedArray.Byte byte24 = EncodedArray.Byte.BeginRead(s);
				for (int num73 = 1; num73 < num; num73++)
				{
					if (buffer[num73].m_flags != Building.Flags.None)
					{
						buffer[num73].m_seniors = byte24.Read();
					}
					else
					{
						buffer[num73].m_seniors = 0;
					}
				}
				byte24.EndRead();
			}
			if (s.get_version() >= 175u)
			{
				EncodedArray.Byte byte25 = EncodedArray.Byte.BeginRead(s);
				for (int num74 = 1; num74 < num; num74++)
				{
					if (buffer[num74].m_flags != Building.Flags.None)
					{
						buffer[num74].m_fireHazard = byte25.Read();
					}
					else
					{
						buffer[num74].m_fireHazard = 0;
					}
				}
				byte25.EndRead();
			}
			if (s.get_version() >= 207u)
			{
				EncodedArray.Byte byte26 = EncodedArray.Byte.BeginRead(s);
				for (int num75 = 1; num75 < num; num75++)
				{
					if (buffer[num75].m_flags != Building.Flags.None)
					{
						buffer[num75].m_subCulture = byte26.Read();
					}
					else
					{
						buffer[num75].m_subCulture = 0;
					}
				}
				byte26.EndRead();
			}
			else
			{
				for (int num76 = 1; num76 < num; num76++)
				{
					buffer[num76].m_subCulture = 0;
				}
			}
			for (int num77 = 1; num77 < num; num77++)
			{
				buffer[num77].m_eventIndex = 0;
				buffer[num77].m_nextGridBuilding = 0;
				buffer[num77].m_ownVehicles = 0;
				buffer[num77].m_guestVehicles = 0;
				buffer[num77].m_sourceCitizens = 0;
				buffer[num77].m_targetCitizens = 0;
				buffer[num77].m_lastFrame = 0;
				buffer[num77].m_parentBuilding = 0;
				if (buffer[num77].m_flags != Building.Flags.None)
				{
					if (s.get_version() >= 29u)
					{
						buffer[num77].m_buildIndex = s.ReadUInt32();
						buffer[num77].m_position = s.ReadVector3();
						buffer[num77].m_angle = s.ReadFloat();
						buffer[num77].m_baseHeight = (byte)s.ReadUInt8();
						if (s.get_version() >= 33u)
						{
							buffer[num77].m_citizenUnits = s.ReadUInt24();
						}
						else
						{
							buffer[num77].m_citizenUnits = 0u;
						}
						if (s.get_version() < 31u)
						{
							s.ReadUInt32();
							s.ReadUInt32();
							s.ReadUInt32();
							s.ReadUInt32();
							s.ReadUInt32();
						}
					}
					else
					{
						buffer[num77].m_buildIndex = s.ReadUInt32();
						if (s.get_version() >= 12u)
						{
							buffer[num77].m_citizenUnits = s.ReadUInt24();
						}
						else
						{
							buffer[num77].m_citizenUnits = 0u;
						}
						buffer[num77].m_position = s.ReadVector3();
						buffer[num77].m_angle = s.ReadFloat();
						s.ReadUInt16();
						if (s.get_version() >= 15u)
						{
							buffer[num77].m_netNode = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer[num77].m_netNode = 0;
						}
						if (s.get_version() >= 15u)
						{
							buffer[num77].m_waterSource = (ushort)s.ReadUInt16();
						}
						else
						{
							buffer[num77].m_waterSource = 0;
						}
						buffer[num77].m_waterPollution = (byte)s.ReadUInt8();
						if (s.get_version() >= 25u)
						{
							buffer[num77].m_garbageBuffer = (ushort)(s.ReadUInt8() * 100u);
						}
						else
						{
							buffer[num77].m_garbageBuffer = 0;
						}
						buffer[num77].m_baseHeight = (byte)s.ReadUInt8();
						if (s.get_version() >= 11u)
						{
							buffer[num77].m_productionRate = (byte)s.ReadUInt8();
						}
						else
						{
							buffer[num77].m_productionRate = 100;
						}
						if (s.get_version() >= 14u)
						{
							buffer[num77].m_frame0.m_constructState = (byte)s.ReadUInt8();
						}
						else
						{
							buffer[num77].m_frame0.m_constructState = 255;
							Building[] expr_20F2_cp_0 = buffer;
							int expr_20F2_cp_1 = num77;
							expr_20F2_cp_0[expr_20F2_cp_1].m_flags = (expr_20F2_cp_0[expr_20F2_cp_1].m_flags | Building.Flags.Completed);
						}
					}
					buffer[num77].m_frame1 = buffer[num77].m_frame0;
					buffer[num77].m_frame2 = buffer[num77].m_frame0;
					buffer[num77].m_frame3 = buffer[num77].m_frame0;
					if (s.get_version() < 308u)
					{
						buffer[num77].m_buildWaterHeight = (ushort)Mathf.Clamp(Mathf.RoundToInt(buffer[num77].m_position.y * 64f), 0, 65535);
					}
					instance.InitializeBuilding((ushort)num77, ref buffer[num77]);
				}
				else
				{
					buffer[num77].m_frame0 = default(Building.Frame);
					buffer[num77].m_frame1 = default(Building.Frame);
					buffer[num77].m_frame2 = default(Building.Frame);
					buffer[num77].m_frame3 = default(Building.Frame);
					buffer[num77].m_buildIndex = 0u;
					buffer[num77].m_position = Vector3.get_zero();
					buffer[num77].m_angle = 0f;
					buffer[num77].m_baseHeight = 0;
					buffer[num77].m_netNode = 0;
					buffer[num77].m_waterSource = 0;
					buffer[num77].m_garbageBuffer = 0;
					buffer[num77].m_productionRate = 0;
					buffer[num77].m_citizenUnits = 0u;
					instance.m_buildings.ReleaseItem((ushort)num77);
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "BuildingManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "BuildingManager");
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			BuildingManager instance2 = Singleton<BuildingManager>.get_instance();
			Singleton<LoadingManager>.get_instance().WaitUntilEssentialScenesLoaded();
			PrefabCollection<BuildingInfo>.BindPrefabs();
			instance2.RefreshAreaBuildings();
			instance2.RefreshStyleBuildings();
			Building[] buffer = instance2.m_buildings.m_buffer;
			int num = buffer.Length;
			int num2 = PrefabCollection<BuildingInfo>.PrefabCount();
			for (int i = 0; i < num2; i++)
			{
				BuildingInfo prefab = PrefabCollection<BuildingInfo>.GetPrefab((uint)i);
				if (prefab != null && prefab.m_notUsedGuide != null)
				{
					prefab.m_notUsedGuide = null;
				}
			}
			if (this.m_guideInfoIndex != null)
			{
				int num3 = this.m_guideInfoIndex.Length;
				for (int j = 0; j < num3; j++)
				{
					BuildingInfo prefab2 = PrefabCollection<BuildingInfo>.GetPrefab(this.m_guideInfoIndex[j]);
					if (prefab2 != null)
					{
						prefab2.m_notUsedGuide = this.m_buildingNotUsedGuides[j];
					}
				}
			}
			if (this.m_milestoneInfoIndex != null)
			{
				int num4 = this.m_milestoneInfoIndex.Length;
				for (int k = 0; k < num4; k++)
				{
					BuildingInfo prefab3 = PrefabCollection<BuildingInfo>.GetPrefab(this.m_milestoneInfoIndex[k]);
					if (prefab3 != null)
					{
						MilestoneInfo unlockMilestone = prefab3.m_UnlockMilestone;
						if (unlockMilestone != null)
						{
							unlockMilestone.SetData(this.m_UnlockMilestones[k]);
						}
					}
				}
			}
			for (int l = 1; l < num; l++)
			{
				if (buffer[l].m_flags != Building.Flags.None)
				{
					BuildingInfo info = buffer[l].Info;
					if (info != null)
					{
						buffer[l].m_infoIndex = (ushort)info.m_prefabDataIndex;
						info.m_buildingAI.BuildingLoaded((ushort)l, ref buffer[l], s.get_version());
					}
					instance2.UpdateBuildingRenderer((ushort)l, true);
					if (buffer[l].m_subBuilding != 0)
					{
						buffer[(int)buffer[l].m_subBuilding].m_parentBuilding = (ushort)l;
					}
					uint num5 = buffer[l].m_citizenUnits;
					int num6 = 0;
					while (num5 != 0u)
					{
						instance.m_units.m_buffer[(int)((UIntPtr)num5)].SetBuildingAfterLoading(num5, (ushort)l, s.get_version());
						num5 = instance.m_units.m_buffer[(int)((UIntPtr)num5)].m_nextUnit;
						if (++num6 > 524288)
						{
							CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
							break;
						}
					}
				}
			}
			instance2.UpdateBuildingColors();
			instance2.m_buildingCount = (int)(instance2.m_buildings.ItemCount() - 1u);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "BuildingManager");
		}

		private uint[] m_guideInfoIndex;

		private uint[] m_milestoneInfoIndex;

		private BuildingTypeGuide[] m_buildingNotUsedGuides;

		private MilestoneInfo.Data[] m_UnlockMilestones;
	}
}
