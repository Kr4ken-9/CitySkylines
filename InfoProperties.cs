﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class InfoProperties : MonoBehaviour
{
	private void Awake()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.InitializeProperties());
		}
		else
		{
			this.InitializeShaderProperties();
		}
	}

	[DebuggerHidden]
	private IEnumerator InitializeProperties()
	{
		InfoProperties.<InitializeProperties>c__Iterator0 <InitializeProperties>c__Iterator = new InfoProperties.<InitializeProperties>c__Iterator0();
		<InitializeProperties>c__Iterator.$this = this;
		return <InitializeProperties>c__Iterator;
	}

	private void InitializeShaderProperties()
	{
		if (this.m_neutralColor.a != 0f)
		{
			this.m_neutralColor.a = 0f;
		}
		Shader.SetGlobalColor("_InfoObjectColor", this.m_neutralColor.get_linear());
		string[] names = Enum.GetNames(typeof(InfoManager.InfoMode));
		int num = Mathf.Min(this.m_modeProperties.Length, names.Length);
		for (int i = 0; i < num; i++)
		{
			Shader.SetGlobalColor("_InfoColor" + names[i], this.m_modeProperties[i].m_activeColor.get_linear());
			Shader.SetGlobalColor("_InfoColorB" + names[i], this.m_modeProperties[i].m_activeColorB.get_linear());
			Shader.SetGlobalColor("_InfoColorInactive" + names[i], this.m_modeProperties[i].m_inactiveColor.get_linear());
		}
	}

	private void OnDestroy()
	{
		if (Application.get_isPlaying())
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("InfoProperties");
			Singleton<InfoManager>.get_instance().DestroyProperties(this);
			Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
		}
	}

	public Color m_lightColor = new Color(0.847f, 0.808f, 0.753f);

	public Color m_ambientColor = new Color(0.376f, 0.502f, 0.627f);

	public float m_lightIntensity = 1f;

	public Color m_neutralColor = new Color(0.73f, 0.73f, 0.73f);

	public InfoProperties.ModeProperties[] m_modeProperties;

	public Color[] m_routeColors;

	[Serializable]
	public class ModeProperties
	{
		public Color m_activeColor;

		public Color m_activeColorB;

		public Color m_inactiveColor;

		public Color m_targetColor;

		public Color m_negativeColor;
	}

	public enum RouteType
	{
		Pedestrian,
		Cyclist,
		PrivateCar,
		PublicTransport,
		CargoTruck,
		ServiceVehicle
	}
}
