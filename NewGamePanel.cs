﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using UnityEngine;

public class NewGamePanel : LoadSavePanelBase<MapMetaData>
{
	protected override void Awake()
	{
		base.Awake();
		this.m_Author = base.Find<UILabel>("Author");
		this.m_FileList = base.Find<UIListBox>("MapList");
		this.m_FileList.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnMapSelectionChanged));
		UITabstrip uITabstrip = base.Find<UITabstrip>("TabstripNewGame");
		uITabstrip.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnMapSelectionChanged));
		this.m_StartButton = base.Find<UIButton>("Start");
		this.m_MapName = base.Find<UITextField>("MapName");
		this.m_MapName.add_eventTextChanged(delegate(UIComponent c, string t)
		{
			this.m_Virgin = false;
			this.m_StartButton.set_isEnabled(this.IsValid());
		});
		this.m_SnapShot = base.Find<UITextureSprite>("SnapShot");
		this.m_BuildableArea = base.Find<UILabel>("BuildableArea");
		this.m_Theme = base.Find<UILabel>("MapTheme");
		this.m_OilResources = base.Find<UIProgressBar>("ResourceBarOil");
		this.m_OreResources = base.Find<UIProgressBar>("ResourceBarOre");
		this.m_ForestryResources = base.Find<UIProgressBar>("ResourceBarForestry");
		this.m_FertilityResources = base.Find<UIProgressBar>("ResourceBarFarming");
		this.m_WaterResources = base.Find<UIProgressBar>("ResourceBarWater");
		this.m_OilNoResources = base.Find("ResourceOil").Find<UISprite>("NoNoNo");
		this.m_OreNoResources = base.Find("ResourceOre").Find<UISprite>("NoNoNo");
		this.m_ForestryNoResources = base.Find("ResourceForestry").Find<UISprite>("NoNoNo");
		this.m_FertilityNoResources = base.Find("ResourceFarming").Find<UISprite>("NoNoNo");
		this.m_WaterNoResources = base.Find("ResourceWater").Find<UISprite>("NoNoNo");
		this.m_Highway = base.Find<UISprite>("Highway");
		this.m_NoHighway = this.m_Highway.Find<UISprite>("NoNoNo");
		this.m_InHighway = this.m_Highway.Find<UISprite>("Incoming");
		this.m_OutHighway = this.m_Highway.Find<UISprite>("Outgoing");
		this.m_Train = base.Find<UISprite>("Train");
		this.m_NoTrain = this.m_Train.Find<UISprite>("NoNoNo");
		this.m_InTrain = this.m_Train.Find<UISprite>("Incoming");
		this.m_OutTrain = this.m_Train.Find<UISprite>("Outgoing");
		this.m_Ship = base.Find<UISprite>("Ship");
		this.m_NoShip = this.m_Ship.Find<UISprite>("NoNoNo");
		this.m_InShip = this.m_Ship.Find<UISprite>("Incoming");
		this.m_OutShip = this.m_Ship.Find<UISprite>("Outgoing");
		this.m_Plane = base.Find<UISprite>("Plane");
		this.m_NoPlane = this.m_Plane.Find<UISprite>("NoNoNo");
		this.m_InPlane = this.m_Plane.Find<UISprite>("Incoming");
		this.m_OutPlane = this.m_Plane.Find<UISprite>("Outgoing");
		this.m_ThemeOverridePanel = base.Find<UIPanel>("OverridePanel");
		this.m_Themes = new Dictionary<string, List<MapThemeMetaData>>();
		this.m_ThemeOverrideDropDown = base.Find<UIDropDown>("OverrideMapTheme");
		base.SetSorting("PANEL_SORT_THEME");
		this.Refresh();
	}

	private string GenerateMapName(string environment)
	{
		CityNameGroups.Environment environment2 = this.m_CityNameGroups.FindGroup(environment);
		string text = environment2.m_closeDistance[Random.Range(0, environment2.m_closeDistance.Length)];
		int num = (int)Locale.Count("CONNECTIONS_PATTERN", text);
		string format = Locale.Get("CONNECTIONS_PATTERN", text, Random.Range(0, num));
		int num2 = (int)Locale.Count("CONNECTIONS_NAME", text);
		string arg = Locale.Get("CONNECTIONS_NAME", text, Random.Range(0, num2));
		return StringUtils.SafeFormat(format, arg);
	}

	private void OnMapSelectionChanged(UIComponent comp, int sel)
	{
		this.OnMapSelectionChanged();
	}

	private void OnMapSelectionChanged()
	{
		int selectedIndex = this.m_FileList.get_selectedIndex();
		if (selectedIndex > -1)
		{
			Package package = base.GetListingData(selectedIndex).get_package();
			this.m_Author.set_isVisible(!string.IsNullOrEmpty(package.get_packageAuthor()));
			if (this.m_Author.get_isVisible())
			{
				this.m_Author.set_text(LocaleFormatter.FormatGeneric("PACKAGEAUTHOR", new object[]
				{
					LoadSavePanelBase<MapMetaData>.ResolveName(package.get_packageAuthor())
				}));
			}
			MapMetaData listingMetaData = base.GetListingMetaData(selectedIndex);
			if (this.m_SnapShot.get_texture() != null)
			{
				Object.Destroy(this.m_SnapShot.get_texture());
			}
			if (listingMetaData.imageRef != null)
			{
				this.m_SnapShot.set_texture(listingMetaData.imageRef.Instantiate<Texture>());
				if (this.m_SnapShot.get_texture() != null)
				{
					this.m_SnapShot.get_texture().set_wrapMode(1);
				}
			}
			else
			{
				this.m_SnapShot.set_texture(null);
			}
			if (this.m_Virgin)
			{
				this.m_MapName.set_text(this.GenerateMapName(listingMetaData.environment));
				this.m_Virgin = true;
			}
			ValueAnimator.Cancel("NewGameOil");
			ValueAnimator.Cancel("NewGameOre");
			ValueAnimator.Cancel("NewGameForest");
			ValueAnimator.Cancel("NewGameFertility");
			ValueAnimator.Cancel("NewGameWater");
			this.m_Theme.set_text(LoadSavePanelBase<MapMetaData>.GetThemeString(null, listingMetaData.environment, "NEWGAME_BASETHEME"));
			this.m_SelectedEnvironment = listingMetaData.environment;
			if (this.m_ThemeOverridePanel != null)
			{
				base.RefreshOverrideThemes();
				base.SelectThemeOverride(listingMetaData.mapThemeRef);
				this.m_ThemeOverridePanel.Show();
			}
			this.m_OilResources.set_value(0f);
			this.m_OreResources.set_value(0f);
			this.m_ForestryResources.set_value(0f);
			this.m_FertilityResources.set_value(0f);
			this.m_WaterResources.set_value(0f);
			this.m_OilNoResources.set_isVisible(listingMetaData.oilAmount == 0u);
			this.m_OreNoResources.set_isVisible(listingMetaData.oreAmount == 0u);
			this.m_FertilityNoResources.set_isVisible(listingMetaData.fertileLandAmount == 0u);
			this.m_ForestryNoResources.set_isVisible(listingMetaData.forestAmount == 0u);
			this.m_WaterNoResources.set_isVisible(listingMetaData.waterAmount == 0u);
			this.m_BuildableArea.set_text(StringUtils.SafeFormat(Locale.Get("AREA_BUILDABLE"), StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), listingMetaData.buildableArea)));
			float maxResourceAmount = listingMetaData.maxResourceAmount;
			float num = 0f;
			float num2 = 0f;
			float num3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			if (maxResourceAmount > 0f)
			{
				num = Mathf.Pow(listingMetaData.oilAmount / maxResourceAmount, this.m_OilExponent);
				num2 = Mathf.Pow(listingMetaData.oreAmount / maxResourceAmount, this.m_OreExponent);
				num3 = Mathf.Pow(listingMetaData.forestAmount / maxResourceAmount, this.m_ForestryExponent);
				num4 = Mathf.Pow(listingMetaData.fertileLandAmount / maxResourceAmount, this.m_FarmingExponent);
				num5 = Mathf.Pow(listingMetaData.waterAmount / maxResourceAmount, this.m_WaterExponent);
			}
			ValueAnimator.Animate("NewGameOil", delegate(float val)
			{
				this.m_OilResources.set_value(val);
			}, new AnimatedFloat(0f, num, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameOre", delegate(float val)
			{
				this.m_OreResources.set_value(val);
			}, new AnimatedFloat(0f, num2, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameForest", delegate(float val)
			{
				this.m_ForestryResources.set_value(val);
			}, new AnimatedFloat(0f, num3, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameFertility", delegate(float val)
			{
				this.m_FertilityResources.set_value(val);
			}, new AnimatedFloat(0f, num4, this.m_InterpolationTime, this.m_InterpolationEasingType));
			ValueAnimator.Animate("NewGameWater", delegate(float val)
			{
				this.m_WaterResources.set_value(val);
			}, new AnimatedFloat(0f, num5, this.m_InterpolationTime, this.m_InterpolationEasingType));
			bool flag = listingMetaData.incomingShipPath == 0 && listingMetaData.outgoingShipPath == 0;
			this.m_Ship.set_tooltip((!flag) ? Locale.Get("AREA_YES_SHIPCONNECTION") : Locale.Get("AREA_NO_SHIPCONNECTION"));
			this.m_NoShip.set_isVisible(flag);
			this.m_InShip.set_isVisible(listingMetaData.incomingShipPath > 0);
			this.m_OutShip.set_isVisible(listingMetaData.outgoingShipPath > 0);
			bool flag2 = listingMetaData.incomingTrainTracks == 0 && listingMetaData.outgoingTrainTracks == 0;
			this.m_Train.set_tooltip((!flag2) ? Locale.Get("AREA_YES_TRAINCONNECTION") : Locale.Get("AREA_NO_TRAINCONNECTION"));
			this.m_NoTrain.set_isVisible(flag2);
			this.m_InTrain.set_isVisible(listingMetaData.incomingTrainTracks > 0);
			this.m_OutTrain.set_isVisible(listingMetaData.outgoingTrainTracks > 0);
			bool flag3 = listingMetaData.incomingPlanePath == 0 && listingMetaData.outgoingPlanePath == 0;
			this.m_Plane.set_tooltip((!flag3) ? Locale.Get("AREA_YES_PLANECONNECTION") : Locale.Get("AREA_NO_PLANECONNECTION"));
			this.m_NoPlane.set_isVisible(flag3);
			this.m_InPlane.set_isVisible(listingMetaData.incomingPlanePath > 0);
			this.m_OutPlane.set_isVisible(listingMetaData.outgoingPlanePath > 0);
			bool flag4 = listingMetaData.incomingRoads == 0 && listingMetaData.outgoingRoads == 0;
			this.m_Highway.set_tooltip((!flag4) ? Locale.Get("AREA_YES_HIGHWAYCONNECTION") : Locale.Get("AREA_NO_HIGHWAYCONNECTION"));
			this.m_NoHighway.set_isVisible(flag4);
			this.m_InHighway.set_isVisible(listingMetaData.incomingRoads > 0);
			this.m_OutHighway.set_isVisible(listingMetaData.outgoingRoads > 0);
		}
		else
		{
			this.m_SnapShot.set_texture(null);
		}
	}

	private void OnDestroy()
	{
		ValueAnimator.Cancel("NewGameOil");
		ValueAnimator.Cancel("NewGameOre");
		ValueAnimator.Cancel("NewGameForest");
		ValueAnimator.Cancel("NewGameFertility");
		ValueAnimator.Cancel("NewGameWater");
	}

	private void OnItemDoubleClick(UIComponent comp, int sel)
	{
		if (comp == this.m_FileList && sel != -1)
		{
			this.OnStartNewGame();
		}
	}

	private void OnResolutionChanged(UIComponent comp, Vector2 previousResolution, Vector2 currentResolution)
	{
		base.get_component().CenterToParent();
	}

	private void OnEnterFocus(UIComponent comp, UIFocusEventParameter p)
	{
		if (p.get_gotFocus() == base.get_component())
		{
			this.m_FileList.Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.m_Virgin = true;
			this.Refresh();
			base.get_component().Focus();
		}
		else
		{
			this.m_FileList.set_selectedIndex(-1);
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("NewGamePanel.Refresh()"))
		{
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
			bool flag3 = SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC);
			bool flag4 = SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.MapMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					try
					{
						MapMetaData mapMetaData = current.Instantiate<MapMetaData>();
						if ((mapMetaData.environment != "Winter" || flag) && mapMetaData.isPublished)
						{
							string text = mapMetaData.mapName;
							bool flag5 = NewGamePanel.IsNDScenarioMap(text);
							bool flag6 = NewGamePanel.IsMTScenarioMap(text);
							bool flag7 = NewGamePanel.IsGCScenarioMap(text);
							bool flag8 = mapMetaData.environment == "Winter" && mapMetaData.IsBuiltinMap(current.get_package().get_packagePath());
							if ((flag3 || !mapMetaData.IsBuiltinMap(current.get_package().get_packagePath()) || !flag6) && (flag2 || !mapMetaData.IsBuiltinMap(current.get_package().get_packagePath()) || !flag5) && (flag4 || !mapMetaData.IsBuiltinMap(current.get_package().get_packagePath()) || !flag7) && !NewGamePanel.IsUnplayableNDScenarioMap(text))
							{
								if (Locale.Exists("MAPNAME", text))
								{
									text = Locale.Get("MAPNAME", text);
								}
								if (flag7)
								{
									text = "<sprite GreenCitiesIcon> " + text;
								}
								if (flag5)
								{
									text = "<sprite NaturalDisastersIcon> " + text;
								}
								if (flag6)
								{
									text = "<sprite MassTransitIcon> " + text;
								}
								if (flag8)
								{
									text = "<sprite WWIcon> " + text;
								}
								base.AddToListing(text, current, mapMetaData);
							}
						}
					}
					catch (Exception ex)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Serialization, "'" + current.get_name() + "' failed to load.\n" + ex.ToString());
					}
				}
			}
			this.m_FileList.set_items(base.GetListingItems());
			if (this.m_FileList.get_items().Length > 0)
			{
				this.m_FileList.set_selectedIndex(0);
			}
			this.m_StartButton.set_isEnabled(this.IsValid());
			base.RebuildThemeDictionary();
		}
	}

	public static bool IsGCScenarioMap(string mapName)
	{
		string[] array = NewGamePanel.kGCScenarioMaps;
		for (int i = 0; i < array.Length; i++)
		{
			string a = array[i];
			if (a == mapName)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsMTScenarioMap(string mapName)
	{
		string[] array = NewGamePanel.kMTScenarioMaps;
		for (int i = 0; i < array.Length; i++)
		{
			string a = array[i];
			if (a == mapName)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsNDScenarioMap(string mapName)
	{
		string[] array = NewGamePanel.kNDScenarioMaps;
		for (int i = 0; i < array.Length; i++)
		{
			string a = array[i];
			if (a == mapName)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsUnplayableNDScenarioMap(string mapName)
	{
		string[] array = NewGamePanel.kUnplayableNDScenarioMaps;
		for (int i = 0; i < array.Length; i++)
		{
			string a = array[i];
			if (a == mapName)
			{
				return true;
			}
		}
		return false;
	}

	public bool IsValid()
	{
		return this.m_FileList.get_items().Length > 0 && this.m_MapName.get_text() != string.Empty;
	}

	public void OnStartNewGame()
	{
		MapMetaData listingMetaData = base.GetListingMetaData(this.m_FileList.get_selectedIndex());
		if (this.m_ThemeOverrideDropDown != null && this.m_Themes != null)
		{
			if (this.m_ThemeOverrideDropDown.get_items().Length > 0 && this.m_ThemeOverrideDropDown.get_selectedIndex() > 0 && !string.IsNullOrEmpty(this.m_SelectedEnvironment) && this.m_Themes.ContainsKey(this.m_SelectedEnvironment) && this.m_Themes[this.m_SelectedEnvironment].Count >= this.m_ThemeOverrideDropDown.get_selectedIndex())
			{
				listingMetaData.mapThemeRef = this.m_Themes[this.m_SelectedEnvironment][this.m_ThemeOverrideDropDown.get_selectedIndex() - 1].mapThemeRef;
			}
			else if (this.m_ThemeOverrideDropDown.get_selectedIndex() == 0)
			{
				listingMetaData.mapThemeRef = null;
			}
		}
		if (base.showMissingThemeWarning)
		{
			ConfirmPanel.ShowModal("CONFIRM_MAPTHEME_MISSING", delegate(UIComponent comp, int ret)
			{
				if (ret == 1)
				{
					this.CheckForDisasterActivation();
				}
			});
		}
		else
		{
			this.CheckForDisasterActivation();
		}
	}

	private void CheckForDisasterActivation()
	{
		SavedBool hasSetDisastersOnOff = new SavedBool(Settings.hasSetDisastersOnOff, Settings.gameSettingsFile, false);
		if (!hasSetDisastersOnOff.get_value() && SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			ConfirmPanel.ShowModal("TEXTFORRANDOMDISASTERS", delegate(UIComponent c, int r)
			{
				if (r == 0 || r == 1)
				{
					hasSetDisastersOnOff.set_value(true);
					SavedBool savedBool = new SavedBool(Settings.randomDisastersEnabled, Settings.gameSettingsFile, DefaultSettings.randomDisastersEnabled, true);
					savedBool.set_value(r == 1);
					this.StartNewGameRoutine();
				}
			});
		}
		else
		{
			this.StartNewGameRoutine();
		}
	}

	private void StartNewGameRoutine()
	{
		SimulationMetaData simulationMetaData = new SimulationMetaData
		{
			m_CityName = this.m_MapName.get_text(),
			m_gameInstanceIdentifier = Guid.NewGuid().ToString(),
			m_invertTraffic = ((!base.Find<UICheckBox>("InvertTraffic").get_isChecked()) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True),
			m_disableAchievements = ((Singleton<PluginManager>.get_instance().get_enabledModCount() <= 0) ? SimulationMetaData.MetaBool.False : SimulationMetaData.MetaBool.True),
			m_startingDateTime = DateTime.Now,
			m_currentDateTime = DateTime.Now,
			m_newGameAppVersion = 172090642u,
			m_updateMode = SimulationManager.UpdateMode.NewGameFromMap
		};
		MapMetaData listingMetaData = base.GetListingMetaData(this.m_FileList.get_selectedIndex());
		simulationMetaData.m_MapName = listingMetaData.mapName;
		if (listingMetaData.mapThemeRef != null)
		{
			Package.Asset asset = PackageManager.FindAssetByName(listingMetaData.mapThemeRef);
			if (asset != null)
			{
				simulationMetaData.m_MapThemeMetaData = asset.Instantiate<MapThemeMetaData>();
				simulationMetaData.m_MapThemeMetaData.SetSelfRef(asset);
			}
		}
		Singleton<LoadingManager>.get_instance().LoadLevel(base.GetListingData(this.m_FileList.get_selectedIndex()), "Game", "InGame", simulationMetaData);
		UIView.get_library().Hide(base.GetType().Name, 1);
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 27)
			{
				this.OnClosed();
				p.Use();
			}
			else if (p.get_keycode() == 9)
			{
				UIComponent activeComponent = UIView.get_activeComponent();
				int num = Array.IndexOf<UIComponent>(this.m_TabFocusList, activeComponent);
				if (num != -1 && this.m_TabFocusList.Length > 0)
				{
					int num2 = 0;
					do
					{
						if (p.get_shift())
						{
							num = (num - 1) % this.m_TabFocusList.Length;
							if (num < 0)
							{
								num += this.m_TabFocusList.Length;
							}
						}
						else
						{
							num = (num + 1) % this.m_TabFocusList.Length;
						}
					}
					while (!this.m_TabFocusList[num].get_isEnabled() && num2 < this.m_TabFocusList.Length);
					this.m_TabFocusList[num].Focus();
				}
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnStartNewGame();
				p.Use();
			}
		}
	}

	public CityNameGroups m_CityNameGroups;

	public bool m_Virgin;

	public float m_OilExponent = 1f;

	public float m_OreExponent = 1f;

	public float m_ForestryExponent = 1f;

	public float m_FarmingExponent = 1f;

	public float m_WaterExponent = 1f;

	public float m_InterpolationTime = 0.7f;

	public EasingType m_InterpolationEasingType = 2;

	public UIComponent[] m_TabFocusList;

	private UIListBox m_FileList;

	private UITextureSprite m_SnapShot;

	private UIButton m_StartButton;

	private UILabel m_Theme;

	private UIProgressBar m_OilResources;

	private UIProgressBar m_OreResources;

	private UIProgressBar m_ForestryResources;

	private UIProgressBar m_FertilityResources;

	private UIProgressBar m_WaterResources;

	private UISprite m_OilNoResources;

	private UISprite m_OreNoResources;

	private UISprite m_ForestryNoResources;

	private UISprite m_FertilityNoResources;

	private UISprite m_WaterNoResources;

	private UISprite m_Highway;

	private UISprite m_NoHighway;

	private UISprite m_InHighway;

	private UISprite m_OutHighway;

	private UISprite m_Train;

	private UISprite m_NoTrain;

	private UISprite m_InTrain;

	private UISprite m_OutTrain;

	private UISprite m_Ship;

	private UISprite m_NoShip;

	private UISprite m_InShip;

	private UISprite m_OutShip;

	private UISprite m_Plane;

	private UISprite m_NoPlane;

	private UISprite m_InPlane;

	private UISprite m_OutPlane;

	private UILabel m_BuildableArea;

	private UITextField m_MapName;

	private UILabel m_Author;

	private static readonly string[] kGCScenarioMaps = new string[]
	{
		"Eden Valley",
		"Garden Rivers",
		"Lavender Lake"
	};

	private static readonly string[] kMTScenarioMaps = new string[]
	{
		"Arid Plains",
		"Regal Hills",
		"Seven Lakes"
	};

	private static readonly string[] kNDScenarioMaps = new string[]
	{
		"Prussian Peaks",
		"By The Dam",
		"Floodland",
		"The Archipelago",
		"The Dust Bowl"
	};

	private static readonly string[] kUnplayableNDScenarioMaps = new string[]
	{
		"By The Dam",
		"Floodland"
	};
}
