﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class EconomyPanel : ToolsModifierControl
{
	[DebuggerHidden]
	private IEnumerator TakeLoanSim(int index, int amount, int interest, int length)
	{
		EconomyPanel.<TakeLoanSim>c__Iterator0 <TakeLoanSim>c__Iterator = new EconomyPanel.<TakeLoanSim>c__Iterator0();
		<TakeLoanSim>c__Iterator.index = index;
		<TakeLoanSim>c__Iterator.amount = amount;
		<TakeLoanSim>c__Iterator.interest = interest;
		<TakeLoanSim>c__Iterator.length = length;
		<TakeLoanSim>c__Iterator.$this = this;
		return <TakeLoanSim>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator PayLoanNow(int index)
	{
		EconomyPanel.<PayLoanNow>c__Iterator1 <PayLoanNow>c__Iterator = new EconomyPanel.<PayLoanNow>c__Iterator1();
		<PayLoanNow>c__Iterator.index = index;
		<PayLoanNow>c__Iterator.$this = this;
		return <PayLoanNow>c__Iterator;
	}

	private void TakeLoan(int id)
	{
		if (Singleton<EconomyManager>.get_exists())
		{
			EconomyManager.Loan loan;
			EconomyManager.LoanInfo[] array;
			if (Singleton<EconomyManager>.get_instance().GetLoan(id, out loan))
			{
				base.StartCoroutine(this.PayLoanNow(id));
			}
			else if (Singleton<EconomyManager>.get_instance().GetLoanInfo(id, out array))
			{
				base.StartCoroutine(this.TakeLoanSim(id, array[0].m_amount * 100, array[0].m_interest * 100, array[0].m_length));
			}
		}
	}

	public void TakeLoan1()
	{
		this.TakeLoan(0);
	}

	public void TakeLoan2()
	{
		this.TakeLoan(1);
	}

	public void TakeLoan3()
	{
		this.TakeLoan(2);
	}

	private bool IsLoanUnlocked(int i)
	{
		if (i == 0)
		{
			return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Loans);
		}
		if (i == 1)
		{
			return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.SecondLoan);
		}
		return i == 2 && ToolsModifierControl.IsUnlocked(UnlockManager.Feature.ThirdLoan);
	}

	private void InitializeLoansTab()
	{
		this.m_LoansTab = base.Find("EconomyContainer").Find("Loans");
	}

	private string GetUnlockText(int i)
	{
		if (Singleton<UnlockManager>.get_exists() && i >= 0 && i < 3)
		{
			UnlockManager.Feature feature = UnlockManager.Feature.Loans;
			if (i == 0)
			{
				feature = UnlockManager.Feature.Loans;
			}
			if (i == 1)
			{
				feature = UnlockManager.Feature.SecondLoan;
			}
			if (i == 2)
			{
				feature = UnlockManager.Feature.ThirdLoan;
			}
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return string.Empty;
	}

	private void PopulateLoansTab()
	{
		for (int i = 0; i < 3; i++)
		{
			UIPanel uIPanel = base.Find<UIPanel>("LoanOffer" + i.ToString());
			uIPanel.set_isEnabled(this.IsLoanUnlocked(i));
			uIPanel.set_tooltip(this.GetUnlockText(i));
			UIButton uIButton = uIPanel.Find<UIButton>("Action");
			UILabel uILabel = uIPanel.Find<UILabel>("BankLabel");
			if (Singleton<EconomyManager>.get_exists())
			{
				string bankName = Singleton<EconomyManager>.get_instance().GetBankName(i);
				uIPanel.set_isVisible(!string.IsNullOrEmpty(bankName));
				if (!string.IsNullOrEmpty(bankName))
				{
					uILabel.set_text(Locale.Get("BANKNAME", bankName));
					EconomyManager.Loan loan;
					bool flag;
					if (Singleton<EconomyManager>.get_instance().GetLoan(i, out loan))
					{
						flag = true;
						uIPanel.set_color(this.m_TakenLoanColor);
						uIButton.set_text(Locale.Get("LOAN_PAY"));
						uIButton.set_isEnabled(Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.LoanAmount, loan.m_amountLeft) == loan.m_amountLeft);
					}
					else
					{
						flag = false;
						uIPanel.set_color(this.m_AvailableLoanColor);
						uIButton.set_text(Locale.Get("LOAN_TAKE"));
						uIButton.set_isEnabled(true);
					}
					EconomyManager.LoanInfo[] array;
					if (Singleton<EconomyManager>.get_instance().GetLoanInfo(i, out array))
					{
						float num = (float)array[0].m_amount + (float)(array[0].m_amount * array[0].m_interest) / 100f;
						float num2 = num / (float)array[0].m_length;
						UIComponent uIComponent = uIPanel.Find("OfferInfoDesc");
						UIComponent uIComponent2 = uIPanel.Find("OfferInfo");
						if (!flag)
						{
							uIComponent.Find<UILabel>("Info1").set_text(Locale.Get("LOAN_AMOUNT"));
							uIComponent2.Find<UILabel>("Info1").set_text(array[0].m_amount.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()));
							uIComponent.Find<UILabel>("Info2").set_text(Locale.Get("LOAN_PAYMENTPLAN"));
							uIComponent2.Find<UILabel>("Info2").set_text(StringUtils.SafeFormat(Locale.Get("LOAN_PAYMENTFORMAT"), new object[]
							{
								array[0].m_length,
								(array[0].m_length != 1) ? Locale.Get("DATETIME_WEEKS") : Locale.Get("DATETIME_WEEK")
							}));
							uIComponent.Find<UILabel>("Info3").set_text(Locale.Get("LOAN_INTEREST"));
							uIComponent2.Find<UILabel>("Info3").set_text(StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), array[0].m_interest));
							uIComponent.Find<UILabel>("Info4").set_text(Locale.Get("LOAN_WEEKLYCOST"));
							uIComponent2.Find<UILabel>("Info4").set_text(StringUtils.SafeFormat(num2.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()), new object[0]));
							uIComponent.Find<UILabel>("Info5").set_text(Locale.Get("LOAN_TOTAL"));
							uIComponent2.Find<UILabel>("Info5").set_text(StringUtils.SafeFormat(num.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()), new object[0]));
							uIComponent.Find<UILabel>("Info6").set_isVisible(false);
							uIComponent2.Find<UILabel>("Info6").set_isVisible(false);
						}
						else
						{
							float num3 = (float)(loan.m_interestRate / 100);
							float num4 = (float)loan.m_amountLeft / 100f;
							int num5 = Mathf.CeilToInt((float)loan.m_amountLeft / (float)loan.m_amountTaken * (float)loan.m_length);
							uIComponent.Find<UILabel>("Info1").set_text(Locale.Get("LOAN_PAYMENTLEFT"));
							uIComponent2.Find<UILabel>("Info1").set_text(num4.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()));
							uIComponent.Find<UILabel>("Info2").set_text(Locale.Get("LOAN_PAYMENTPLAN"));
							uIComponent2.Find<UILabel>("Info2").set_text(StringUtils.SafeFormat(Locale.Get("LOAN_PAYMENTFORMAT"), new object[]
							{
								loan.m_length,
								(loan.m_length != 1) ? Locale.Get("DATETIME_WEEKS") : Locale.Get("DATETIME_WEEK")
							}));
							uIComponent.Find<UILabel>("Info3").set_text(Locale.Get("LOAN_INTEREST"));
							uIComponent2.Find<UILabel>("Info3").set_text(StringUtils.SafeFormat(Locale.Get("VALUE_PERCENTAGE"), num3));
							uIComponent.Find<UILabel>("Info4").set_text(Locale.Get("LOAN_WEEKLYCOST"));
							uIComponent2.Find<UILabel>("Info4").set_text(StringUtils.SafeFormat(num2.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()), new object[0]));
							uIComponent.Find<UILabel>("Info5").set_text(Locale.Get("LOAN_PAYMENTTIMELEFT"));
							uIComponent2.Find<UILabel>("Info5").set_text(StringUtils.SafeFormat(Locale.Get("LOAN_PAYMENTFORMAT"), new object[]
							{
								num5,
								(num5 != 1) ? Locale.Get("DATETIME_WEEKS") : Locale.Get("DATETIME_WEEK")
							}));
							uIComponent.Find<UILabel>("Info6").set_isVisible(true);
							uIComponent2.Find<UILabel>("Info6").set_isVisible(true);
							uIComponent.Find<UILabel>("Info6").set_text(Locale.Get("LOAN_TOTAL"));
							uIComponent2.Find<UILabel>("Info6").set_text(StringUtils.SafeFormat(num.ToString(Settings.moneyFormat, LocaleManager.get_cultureInfo()), new object[0]));
						}
					}
				}
			}
		}
	}

	private void PopulateTaxesTab()
	{
		UIComponent uIComponent = base.get_component().Find("TaxesItemContainer");
		for (int i = 0; i < EconomyPanel.kZones.Length; i++)
		{
			UIComponent uIComponent2;
			if (uIComponent.get_childCount() > i)
			{
				uIComponent2 = uIComponent.get_components()[i];
			}
			else
			{
				GameObject asGameObject = UITemplateManager.GetAsGameObject(EconomyPanel.kTaxesItemTemplate);
				asGameObject.set_name(EconomyPanel.kZones[i].get_enumName());
				uIComponent2 = uIComponent.AttachUIComponent(asGameObject);
			}
			uIComponent2.set_isEnabled(ToolsModifierControl.IsUnlocked(EconomyPanel.kZones[i].get_enumValue()));
			uIComponent2.set_zOrder(i);
			UISprite uISprite = uIComponent2.Find<UISprite>("Icon");
			uISprite.set_spriteName("Zoning" + EconomyPanel.kZones[i].get_enumName());
			UISprite uISprite2 = uIComponent2.Find<UISprite>("Sprite");
			uISprite2.set_spriteName(EconomyPanel.kZones[i].get_enumName());
			uIComponent2.set_tooltip(EconomyPanel.kZones[i].GetLocalizedName());
			UISlider uISlider = uIComponent2.Find<UISlider>("Slider");
			BindProperty component = uISlider.get_gameObject().GetComponent<BindProperty>();
			component.set_enabled(false);
			component.get_dataSource().set_memberName(string.Concat(new string[]
			{
				"taxRate" + EconomyPanel.kZones[i].get_enumName()
			}));
			component.get_dataSource().set_component(this);
			component.set_enabled(true);
		}
	}

	private float GetTaxRate(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		if (Singleton<EconomyManager>.get_exists())
		{
			return (float)Singleton<EconomyManager>.get_instance().GetTaxRate(service, subService, level);
		}
		return (float)EconomyPanel.sDefaultTaxes;
	}

	private void SetTaxRate(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, float budget)
	{
		if (this.m_EnableAudio && this.m_TaxesSliderSound != null && Singleton<AudioManager>.get_exists())
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_TaxesSliderSound, 1f, true);
		}
		if (Singleton<EconomyManager>.get_exists())
		{
			Singleton<EconomyManager>.get_instance().SetTaxRate(service, subService, level, (int)budget);
		}
	}

	public float taxRateResidentialLow
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, ItemClass.Level.None, value);
		}
	}

	public float taxRateResidentialHigh
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, ItemClass.Level.None, value);
		}
	}

	public float taxRateCommercialLow
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.None, value);
		}
	}

	public float taxRateCommercialHigh
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.None, value);
		}
	}

	public float taxRateIndustrial
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Industrial, ItemClass.SubService.None, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Industrial, ItemClass.SubService.None, ItemClass.Level.None, value);
		}
	}

	public float taxRateOffice
	{
		get
		{
			return this.GetTaxRate(ItemClass.Service.Office, ItemClass.SubService.None, ItemClass.Level.None);
		}
		set
		{
			this.SetTaxRate(ItemClass.Service.Office, ItemClass.SubService.None, ItemClass.Level.None, value);
		}
	}

	private UIComponent AddEntry(UIComponent container, int i, string name, string localizedName, string spriteName, bool isUnlocked, string tooltipLocaleID)
	{
		UIComponent uIComponent;
		if (container.get_childCount() > i)
		{
			uIComponent = container.get_components()[i];
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(EconomyPanel.kBudgetItemTemplate);
			asGameObject.set_name(name);
			uIComponent = container.AttachUIComponent(asGameObject);
		}
		uIComponent.set_isEnabled(isUnlocked);
		uIComponent.set_zOrder(i);
		UISlicedSprite uISlicedSprite = uIComponent.Find<UISlicedSprite>("BackDivider");
		if (i % 2 == 0)
		{
			uISlicedSprite.set_color(this.m_BackDividerColor);
		}
		else
		{
			uISlicedSprite.set_color(this.m_BackDividerAltColor);
		}
		UISprite uISprite = uIComponent.Find<UISprite>("Icon");
		uISprite.set_spriteName((!isUnlocked) ? (spriteName + "Disabled") : spriteName);
		UITextureAtlas.SpriteInfo spriteInfo = uISprite.get_atlas().get_Item(uISprite.get_spriteName());
		if (spriteInfo != null)
		{
			uISprite.set_size(spriteInfo.get_pixelSize());
		}
		UISlider uISlider = uIComponent.Find<UISlider>("DaySlider");
		BindProperty component = uISlider.get_gameObject().GetComponent<BindProperty>();
		component.set_enabled(false);
		component.get_dataSource().set_memberName(string.Concat(new string[]
		{
			"budgetDay" + name
		}));
		component.get_dataSource().set_component(this);
		component.set_enabled(true);
		UISlider uISlider2 = uIComponent.Find<UISlider>("NightSlider");
		BindProperty component2 = uISlider2.get_gameObject().GetComponent<BindProperty>();
		component2.set_enabled(false);
		component2.get_dataSource().set_memberName(string.Concat(new string[]
		{
			"budgetNight" + name
		}));
		component2.get_dataSource().set_component(this);
		component2.set_enabled(true);
		UILabel uILabel = uIComponent.Find<UILabel>("Total");
		BindProperty component3 = uILabel.get_gameObject().GetComponent<BindProperty>();
		component3.set_enabled(false);
		component3.get_dataSource().set_memberName(string.Concat(new string[]
		{
			"expenses" + name + "Total"
		}));
		component3.get_dataSource().set_component(this);
		component3.set_enabled(true);
		uIComponent.set_tooltip(localizedName + "\n" + Locale.Get(tooltipLocaleID, name));
		return uIComponent;
	}

	private void PopulateBudgetTab()
	{
		bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
		bool flag2 = SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC);
		UIComponent container = base.get_component().Find("ServicesBudgetContainer");
		for (int i = 0; i < EconomyPanel.kServices.Length; i++)
		{
			if (EconomyPanel.kServices[i].get_enumValue() == ItemClass.Service.Water && flag)
			{
				this.AddEntry(container, i, EconomyPanel.kServices[i].get_enumName(), EconomyPanel.kServices[i].GetLocalizedName(), EconomyPanel.kServices[i].GetIconSpriteName(), ToolsModifierControl.IsUnlocked(EconomyPanel.kServices[i].get_enumValue()), "BUDGET_INFO_WW");
			}
			else if (EconomyPanel.kServices[i].get_enumValue() == ItemClass.Service.FireDepartment && flag2)
			{
				this.AddEntry(container, i, EconomyPanel.kServices[i].get_enumName(), EconomyPanel.kServices[i].GetLocalizedName(), EconomyPanel.kServices[i].GetIconSpriteName(), ToolsModifierControl.IsUnlocked(EconomyPanel.kServices[i].get_enumValue()), "BUDGET_INFO_ND");
			}
			else
			{
				this.AddEntry(container, i, EconomyPanel.kServices[i].get_enumName(), EconomyPanel.kServices[i].GetLocalizedName(), EconomyPanel.kServices[i].GetIconSpriteName(), ToolsModifierControl.IsUnlocked(EconomyPanel.kServices[i].get_enumValue()), "BUDGET_INFO");
			}
		}
		UIComponent container2 = base.get_component().Find("SubServicesBudgetContainer");
		int num = 0;
		if (flag)
		{
			this.AddEntry(container2, num, EconomyPanel.kRoadService[0].get_enumName(), EconomyPanel.kRoadService[0].GetLocalizedName(), EconomyPanel.kRoadService[0].GetIconSpriteName(), ToolsModifierControl.IsUnlocked(EconomyPanel.kRoadService[0].get_enumValue()), "BUDGET_INFO");
			num++;
		}
		for (int i = 0; i < EconomyPanel.kSubServices.Length; i++)
		{
			if (Singleton<TransportManager>.get_instance().TransportServiceLoaded(EconomyPanel.kSubServices[i].get_enumValue()))
			{
				this.AddEntry(container2, num, EconomyPanel.kSubServices[i].get_enumName(), EconomyPanel.kSubServices[i].GetLocalizedName(), EconomyPanel.kSubServices[i].GetIconSpriteName(), this.IsBudgetUnlocked(EconomyPanel.kSubServices[i].get_enumValue()), "BUDGET_INFO");
				num++;
			}
		}
	}

	public bool IsBudgetUnlocked(ItemClass.SubService subService)
	{
		if (subService == ItemClass.SubService.PublicTransportShip)
		{
			if (SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
			{
				return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Ferry);
			}
		}
		else if (subService == ItemClass.SubService.PublicTransportPlane && SteamHelper.IsDLCOwned(SteamHelper.DLC.InMotionDLC))
		{
			return ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Blimp);
		}
		return ToolsModifierControl.IsUnlocked(subService);
	}

	private float GetBudget(ItemClass.Service service, ItemClass.SubService subService, bool night)
	{
		if (Singleton<EconomyManager>.get_exists())
		{
			return (float)Singleton<EconomyManager>.get_instance().GetBudget(service, subService, night);
		}
		return (float)EconomyPanel.sDefaultBudget;
	}

	private void SetBudget(ItemClass.Service service, ItemClass.SubService subService, float budget, bool night)
	{
		if (this.m_EnableAudio && this.m_BudgetSliderSound != null && Singleton<AudioManager>.get_exists())
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_BudgetSliderSound, 1f, true);
		}
		if (Singleton<EconomyManager>.get_exists())
		{
			Singleton<EconomyManager>.get_instance().SetBudget(service, subService, (int)budget, night);
		}
	}

	public float budgetDayRoads
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Road, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Road, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightRoads
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Road, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Road, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayEducation
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Education, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Education, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightEducation
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Education, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Education, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayFireDepartment
	{
		get
		{
			return this.GetBudget(ItemClass.Service.FireDepartment, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.FireDepartment, ItemClass.SubService.None, value, false);
			this.SetBudget(ItemClass.Service.Disaster, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightFireDepartment
	{
		get
		{
			return this.GetBudget(ItemClass.Service.FireDepartment, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.FireDepartment, ItemClass.SubService.None, value, true);
			this.SetBudget(ItemClass.Service.Disaster, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayGarbage
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Garbage, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Garbage, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightGarbage
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Garbage, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Garbage, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayHealthcare
	{
		get
		{
			return this.GetBudget(ItemClass.Service.HealthCare, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.HealthCare, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightHealthcare
	{
		get
		{
			return this.GetBudget(ItemClass.Service.HealthCare, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.HealthCare, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayMonuments
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Monument, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Monument, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightMonuments
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Monument, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Monument, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayBeautification
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Beautification, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Beautification, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightBeautification
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Beautification, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Beautification, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayPublicTransport
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightPublicTransport
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayPolice
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PoliceDepartment, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PoliceDepartment, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightPolice
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PoliceDepartment, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PoliceDepartment, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayElectricity
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Electricity, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Electricity, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightElectricity
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Electricity, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Electricity, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayWaterAndSewage
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Water, ItemClass.SubService.None, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Water, ItemClass.SubService.None, value, false);
		}
	}

	public float budgetNightWaterAndSewage
	{
		get
		{
			return this.GetBudget(ItemClass.Service.Water, ItemClass.SubService.None, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.Water, ItemClass.SubService.None, value, true);
		}
	}

	public float budgetDayBus
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, value, false);
		}
	}

	public float budgetNightBus
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, value, true);
		}
	}

	public float budgetDayMetro
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, value, false);
		}
	}

	public float budgetNightMetro
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, value, true);
		}
	}

	public float budgetDayPlane
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, value, false);
		}
	}

	public float budgetNightPlane
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, value, true);
		}
	}

	public float budgetDayMonorail
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, value, false);
		}
	}

	public float budgetNightMonorail
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, value, true);
		}
	}

	public float budgetDayCableCar
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, value, false);
		}
	}

	public float budgetNightCableCar
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, value, true);
		}
	}

	public float budgetDayTrain
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, value, false);
		}
	}

	public float budgetNightTrain
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, value, true);
		}
	}

	public float budgetDayShip
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, value, false);
		}
	}

	public float budgetNightShip
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, value, true);
		}
	}

	public float budgetDayTaxi
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, value, false);
		}
	}

	public float budgetNightTaxi
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, value, true);
		}
	}

	public float budgetDayTram
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, false);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, value, false);
		}
	}

	public float budgetNightTram
	{
		get
		{
			return this.GetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, true);
		}
		set
		{
			this.SetBudget(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, value, true);
		}
	}

	public string expensesRoadsTotal
	{
		get
		{
			return this.budgetExpensesPolls[0].expensesString;
		}
	}

	public string expensesElectricityTotal
	{
		get
		{
			return this.budgetExpensesPolls[1].expensesString;
		}
	}

	public string expensesWaterAndSewageTotal
	{
		get
		{
			return this.budgetExpensesPolls[2].expensesString;
		}
	}

	public string expensesGarbageTotal
	{
		get
		{
			return this.budgetExpensesPolls[3].expensesString;
		}
	}

	public string expensesHealthcareTotal
	{
		get
		{
			return this.budgetExpensesPolls[4].expensesString;
		}
	}

	public string expensesFireDepartmentTotal
	{
		get
		{
			return this.budgetExpensesPolls[5].expensesString;
		}
	}

	public string expensesPoliceTotal
	{
		get
		{
			return this.budgetExpensesPolls[6].expensesString;
		}
	}

	public string expensesEducationTotal
	{
		get
		{
			return this.budgetExpensesPolls[7].expensesString;
		}
	}

	public string expensesMonumentsTotal
	{
		get
		{
			return this.budgetExpensesPolls[8].expensesString;
		}
	}

	public string expensesBeautificationTotal
	{
		get
		{
			return this.budgetExpensesPolls[9].expensesString;
		}
	}

	public string expensesBusTotal
	{
		get
		{
			return this.budgetExpensesPolls[10].expensesString;
		}
	}

	public string expensesMetroTotal
	{
		get
		{
			return this.budgetExpensesPolls[11].expensesString;
		}
	}

	public string expensesTrainTotal
	{
		get
		{
			return this.budgetExpensesPolls[12].expensesString;
		}
	}

	public string expensesShipTotal
	{
		get
		{
			return this.budgetExpensesPolls[13].expensesString;
		}
	}

	public string expensesPlaneTotal
	{
		get
		{
			return this.budgetExpensesPolls[14].expensesString;
		}
	}

	public string expensesTaxiTotal
	{
		get
		{
			return this.budgetExpensesPolls[15].expensesString;
		}
	}

	public string expensesTramTotal
	{
		get
		{
			return this.budgetExpensesPolls[16].expensesString;
		}
	}

	public string expensesMonorailTotal
	{
		get
		{
			return this.budgetExpensesPolls[17].expensesString;
		}
	}

	public string expensesCableCarTotal
	{
		get
		{
			return this.budgetExpensesPolls[18].expensesString;
		}
	}

	public string expensesLoansTotal
	{
		get
		{
			if (Singleton<EconomyManager>.get_exists())
			{
				this.m_ExpensesLoansTotal.Check(Singleton<EconomyManager>.get_instance().GetLoanExpenses());
			}
			return this.m_ExpensesLoansTotal.result;
		}
	}

	public string expensesPoliciesTotal
	{
		get
		{
			if (Singleton<EconomyManager>.get_exists())
			{
				this.m_ExpensesPoliciesTotal.Check(Singleton<EconomyManager>.get_instance().GetPolicyExpenses());
			}
			return this.m_ExpensesPoliciesTotal.result;
		}
	}

	private void OnTooltipResidentialTotal(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.residentialDetailIncomePolls.Length; i++)
		{
			this.residentialDetailIncomePolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.residentialDetailIncomePolls[i].Update(this.m_IncomeTooltipResidential);
		}
	}

	private void OnTooltipPublicTransportIncome(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.publicTransportDetailIncomePolls.Length; i++)
		{
			this.publicTransportDetailIncomePolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.publicTransportDetailIncomePolls[i].Update(this.m_IncomeTooltipPublicTransport);
		}
	}

	private void OnTooltipPublicTransportExpenses(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.publicTransportDetailExpensesPolls.Length; i++)
		{
			this.publicTransportDetailExpensesPolls[i].Update(this.m_IncomeTooltipPublicTransport);
		}
	}

	private void OnTooltipCommercialTotal(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.commercialDetailIncomePolls.Length; i++)
		{
			this.commercialDetailIncomePolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.commercialDetailIncomePolls[i].Update(this.m_IncomeTooltipCommercialTotal);
		}
	}

	private void OnTooltipIndustrialTotal(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.industrialDetailIncomePolls.Length; i++)
		{
			this.industrialDetailIncomePolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.industrialDetailIncomePolls[i].Update(this.m_IncomeTooltipIndustrial);
		}
	}

	private void OnTooltipOfficeTotal(UIComponent comp, UIMouseEventParameter p)
	{
		for (int i = 0; i < this.officeDetailIncomePolls.Length; i++)
		{
			this.officeDetailIncomePolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.officeDetailIncomePolls[i].Update(this.m_IncomeTooltipOffice);
		}
	}

	private void InitializePolls()
	{
		this.basicExpensesPolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Road, null, "RoadsTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Electricity, null, "ElectricityTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Water, null, "WaterTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Garbage, null, "GarbageTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.HealthCare, null, "HealthcareTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.FireDepartment, null, "FireDepartmentTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PoliceDepartment, null, "PoliceDepartmentTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Education, null, "EducationTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Monument, null, "MonumentsTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Beautification, null, "ParksTotal", ItemClass.Service.None)
		};
		this.basicIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.None, "IncomeTotal", "ExpensesTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, "IncomeResidentialTotal", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, "IncomeCommercialTotal", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, "IncomeIndustrialTotal", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Office, "IncomeOfficeTotal", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, "IncomePublicTransportTotal", "PublicTransportTotal", ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLow, "IncomeResidentialLow", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHigh, "IncomeResidentialHigh", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, "IncomeCommercialLow", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, "IncomeCommercialHigh", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, new ItemClass.SubService[]
			{
				ItemClass.SubService.CommercialLeisure,
				ItemClass.SubService.CommercialTourist
			}, "IncomeCommercialSpecializations", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, new ItemClass.SubService[]
			{
				ItemClass.SubService.IndustrialFarming,
				ItemClass.SubService.IndustrialForestry,
				ItemClass.SubService.IndustrialOre,
				ItemClass.SubService.IndustrialOil
			}, "IncomeIndustrialSpecializations", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Tourism, "IncomeCommercialTourist", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Citizen, "IncomeCommercialCitizen", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, "TotalOfResidentialTab", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, "TotalOfCommercialTab", null, ItemClass.Service.None),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, "TotalOfIndustrialTab", null, ItemClass.Service.Office),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.None, null, null, "TotalOfPublicTransportTab")
		};
		this.residentialDetailIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.Level1, "L1", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.Level2, "L2", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.Level3, "L3", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.Level4, "L4", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Residential, ItemClass.SubService.None, ItemClass.Level.Level5, "L5", null)
		};
		this.publicTransportDetailIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, "L1", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, "L2", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, "L3", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, "L4", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, "L5", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, "L6", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, "L7", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, "L8", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, "L9", null, string.Empty)
		};
		this.publicTransportDetailExpensesPolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, null, "L1", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, null, "L2", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, null, "L3", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, null, "L4", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, null, "L5", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, null, "L6", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, null, "L7", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, null, "L8", string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, null, "L9", string.Empty)
		};
		this.commercialDetailIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, new ItemClass.SubService[]
			{
				ItemClass.SubService.CommercialLow,
				ItemClass.SubService.CommercialHigh
			}, ItemClass.Level.Level1, "L1", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, new ItemClass.SubService[]
			{
				ItemClass.SubService.CommercialLow,
				ItemClass.SubService.CommercialHigh
			}, ItemClass.Level.Level2, "L2", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, new ItemClass.SubService[]
			{
				ItemClass.SubService.CommercialLow,
				ItemClass.SubService.CommercialHigh
			}, ItemClass.Level.Level3, "L3", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLeisure, ItemClass.Level.None, "L4", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, ItemClass.SubService.CommercialTourist, ItemClass.Level.None, "L5", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Commercial, ItemClass.SubService.CommercialEco, ItemClass.Level.None, "L6", null)
		};
		this.industrialDetailIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level1, "L1", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level2, "L2", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level3, "L3", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialFarming, "L4", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialForestry, "L5", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOil, "L6", null, string.Empty),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOre, "L7", null, string.Empty)
		};
		this.officeDetailIncomePolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Office, ItemClass.SubService.None, ItemClass.Level.Level1, "L1", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Office, ItemClass.SubService.None, ItemClass.Level.Level2, "L2", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Office, ItemClass.SubService.None, ItemClass.Level.Level3, "L3", null),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.Office, ItemClass.SubService.OfficeHightech, ItemClass.Level.None, "L4", null)
		};
		this.budgetExpensesPolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			this.basicExpensesPolls[0],
			this.basicExpensesPolls[1],
			this.basicExpensesPolls[2],
			this.basicExpensesPolls[3],
			this.basicExpensesPolls[4],
			this.basicExpensesPolls[5],
			this.basicExpensesPolls[6],
			this.basicExpensesPolls[7],
			this.basicExpensesPolls[8],
			this.basicExpensesPolls[9],
			this.publicTransportDetailExpensesPolls[0],
			this.publicTransportDetailExpensesPolls[1],
			this.publicTransportDetailExpensesPolls[2],
			this.publicTransportDetailExpensesPolls[3],
			this.publicTransportDetailExpensesPolls[4],
			this.publicTransportDetailExpensesPolls[5],
			this.publicTransportDetailExpensesPolls[6],
			this.publicTransportDetailExpensesPolls[7],
			this.publicTransportDetailExpensesPolls[8]
		};
		this.publicTransportTabPolls = new EconomyPanel.IncomeExpensesPoll[]
		{
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportBus, "BusIncome", "BusExpenses", "BusTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTram, "TramIncome", "TramExpenses", "TramTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMetro, "MetroIncome", "MetroExpenses", "MetroTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, "TrainIncome", "TrainExpenses", "TrainTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, "ShipIncome", "ShipExpenses", "ShipTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, "PlaneIncome", "PlaneExpenses", "PlaneTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTaxi, "TaxiIncome", "TaxiExpenses", "TaxiTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportMonorail, "MonorailIncome", "MonorailExpenses", "MonorailTotal"),
			new EconomyPanel.IncomeExpensesPoll(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportCableCar, "CableCarIncome", "CableCarExpenses", "CableCarTotal")
		};
		UIPanel container = base.Find<UIPanel>("ResidentialBarChartLowDensity");
		UIPanel container2 = base.Find<UIPanel>("ResidentialBarChartHighDensity");
		UIPanel container3 = base.Find<UIPanel>("ResidentialBarChartLowDensityEco");
		UIPanel container4 = base.Find<UIPanel>("ResidentialBarChartHighDensityEco");
		List<BarChartBar> list = new List<BarChartBar>();
		list.AddRange(new BarChartBar[]
		{
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialLow,
				ItemClass.SubService.ResidentialLowEco
			}, ItemClass.Level.Level1, container, this.m_lowDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialLow,
				ItemClass.SubService.ResidentialLowEco
			}, ItemClass.Level.Level2, container, this.m_lowDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialLow,
				ItemClass.SubService.ResidentialLowEco
			}, ItemClass.Level.Level3, container, this.m_lowDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialLow,
				ItemClass.SubService.ResidentialLowEco
			}, ItemClass.Level.Level4, container, this.m_lowDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialLow,
				ItemClass.SubService.ResidentialLowEco
			}, ItemClass.Level.Level5, container, this.m_lowDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialHigh,
				ItemClass.SubService.ResidentialHighEco
			}, ItemClass.Level.Level1, container2, this.m_highDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialHigh,
				ItemClass.SubService.ResidentialHighEco
			}, ItemClass.Level.Level2, container2, this.m_highDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialHigh,
				ItemClass.SubService.ResidentialHighEco
			}, ItemClass.Level.Level3, container2, this.m_highDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialHigh,
				ItemClass.SubService.ResidentialHighEco
			}, ItemClass.Level.Level4, container2, this.m_highDensityResidentialBarColor, false),
			new BarChartBar(ItemClass.Service.Residential, new ItemClass.SubService[]
			{
				ItemClass.SubService.ResidentialHigh,
				ItemClass.SubService.ResidentialHighEco
			}, ItemClass.Level.Level5, container2, this.m_highDensityResidentialBarColor, false)
		});
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			list.AddRange(new BarChartBar[]
			{
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, ItemClass.Level.Level1, container3, this.m_lowDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, ItemClass.Level.Level2, container3, this.m_lowDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, ItemClass.Level.Level3, container3, this.m_lowDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, ItemClass.Level.Level4, container3, this.m_lowDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialLowEco, ItemClass.Level.Level5, container3, this.m_lowDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, ItemClass.Level.Level1, container4, this.m_highDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, ItemClass.Level.Level2, container4, this.m_highDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, ItemClass.Level.Level3, container4, this.m_highDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, ItemClass.Level.Level4, container4, this.m_highDensityResidentialBarColorEco, true),
				new BarChartBar(ItemClass.Service.Residential, ItemClass.SubService.ResidentialHighEco, ItemClass.Level.Level5, container4, this.m_highDensityResidentialBarColorEco, true)
			});
		}
		else
		{
			base.Find<UIPanel>("ResidentialLegendPanel").Hide();
		}
		this.m_residentialBarChart = new BarChart(list.ToArray());
		UIPanel container5 = base.Find<UIPanel>("CommercialBarChartLowDensity");
		UIPanel container6 = base.Find<UIPanel>("CommercialBarChartHighDensity");
		UIPanel uIPanel = base.Find<UIPanel>("CommercialBarChartSpecializations");
		List<BarChartBar> list2 = new List<BarChartBar>();
		List<BarChartBar> list3 = new List<BarChartBar>();
		list2.AddRange(new BarChartBar[]
		{
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level1, container5, this.m_lowDensityCommercialBarColor, false),
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level2, container5, this.m_lowDensityCommercialBarColor, false),
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLow, ItemClass.Level.Level3, container5, this.m_lowDensityCommercialBarColor, false),
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level1, container6, this.m_highDensityCommercialBarColor, false),
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level2, container6, this.m_highDensityCommercialBarColor, false),
			new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialHigh, ItemClass.Level.Level3, container6, this.m_highDensityCommercialBarColor, false)
		});
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC))
		{
			list3.Add(new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialLeisure, ItemClass.Level.None, uIPanel, this.m_specializationsCommercialBarColor, false));
			list3.Add(new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialTourist, ItemClass.Level.None, uIPanel, this.m_specializationsCommercialBarColor, false));
		}
		else
		{
			base.Find<UILabel>("LabelCommercialLeisure").Hide();
			base.Find<UILabel>("LabelCommercialTourism").Hide();
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			list3.Add(new BarChartBar(ItemClass.Service.Commercial, ItemClass.SubService.CommercialEco, ItemClass.Level.None, uIPanel, this.m_specializationsCommercialBarColor, false));
		}
		else
		{
			base.Find<UILabel>("LabelCommercialOrganic").Hide();
		}
		base.Find<UIPanel>("CommercialBarChartPanel").set_relativePosition(new Vector2(18f, (float)(30 + (3 - list3.Count) * 12)));
		uIPanel.set_height((float)(30 * list3.Count + 1));
		if (list3.Count == 0)
		{
			uIPanel.Hide();
			base.Find<UIPanel>("CommercialLeftLabelsLower").Hide();
		}
		list2.AddRange(list3);
		this.m_commercialBarChart = new BarChart(list2.ToArray());
		UIPanel container7 = base.Find<UIPanel>("IndustryLevels");
		UIPanel container8 = base.Find<UIPanel>("IndustrySpecializations");
		UIPanel container9 = base.Find<UIPanel>("OfficeLevels");
		UIPanel uIPanel2 = base.Find<UIPanel>("OfficeSpecializations");
		List<BarChartBar> list4 = new List<BarChartBar>();
		list4.AddRange(new BarChartBar[]
		{
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level1, container7, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level2, container7, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialGeneric, ItemClass.Level.Level3, container7, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialFarming, ItemClass.Level.None, container8, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialForestry, ItemClass.Level.None, container8, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOre, ItemClass.Level.None, container8, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Industrial, ItemClass.SubService.IndustrialOil, ItemClass.Level.None, container8, this.m_industrialBarColor, false),
			new BarChartBar(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level1, container9, this.m_officeBarColor, false),
			new BarChartBar(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level2, container9, this.m_officeBarColor, false),
			new BarChartBar(ItemClass.Service.Office, ItemClass.SubService.OfficeGeneric, ItemClass.Level.Level3, container9, this.m_officeBarColor, false)
		});
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC))
		{
			list4.Add(new BarChartBar(ItemClass.Service.Office, ItemClass.SubService.OfficeHightech, ItemClass.Level.None, uIPanel2, this.m_officeBarColor, false));
		}
		else
		{
			base.Find<UILabel>("LabelITCluster").Hide();
			uIPanel2.Hide();
		}
		this.m_industrialBarChart = new BarChart(list4.ToArray());
		this.m_residentialIPC = new IncomePercentagePieChart(base.Find<UIRadialChart>("ResidentialIPC"), ItemClass.Service.Residential, "ECONOMYPANEL_IPC_RESIDENTIAL", true, ItemClass.Service.None);
		this.m_commercialIPC = new IncomePercentagePieChart(base.Find<UIRadialChart>("CommercialIPC"), ItemClass.Service.Commercial, "ECONOMYPANEL_IPC_COMMERCIAL", true, ItemClass.Service.None);
		this.m_industrialIPC = new IncomePercentagePieChart(base.Find<UIRadialChart>("IndustrialIPC"), ItemClass.Service.Industrial, "ECONOMYPANEL_IPC_WORK", true, ItemClass.Service.Office);
		this.m_pubTransIncomeIPC = new IncomePercentagePieChart(base.Find<UIRadialChart>("PublicTransportIPCIncome"), ItemClass.Service.PublicTransport, "ECONOMYPANEL_IPC_PUBLICTRANSPORTINCOME", true, ItemClass.Service.None);
		this.m_pubTransExpensesIPC = new IncomePercentagePieChart(base.Find<UIRadialChart>("PublicTransportIPCExpenses"), ItemClass.Service.PublicTransport, "ECONOMYPANEL_IPC_PUBLICTRANSPORTEXPENSES", false, ItemClass.Service.None);
	}

	private void InitializeIncomeExpenses()
	{
		this.InitializePolls();
		this.m_IncomeTooltipResidential = base.Find("IncomeTooltipResidential");
		this.m_IncomeTooltipOffice = base.Find("IncomeTooltipOffice");
		this.m_IncomeTooltipCommercialTotal = base.Find("IncomeTooltipCommercialTotal");
		this.m_IncomeTooltipIndustrial = base.Find("IncomeTooltipIndustrial");
		this.m_IncomeTooltipPublicTransport = base.Find("IncomeTooltipPublicTransport");
		this.m_IncomeTooltipResidential.Hide();
		this.m_IncomeTooltipOffice.Hide();
		this.m_IncomeTooltipPublicTransport.Hide();
		this.m_IncomeTooltipIndustrial.Hide();
		this.m_IncomeTooltipCommercialTotal.Hide();
		base.Find<UILabel>("IncomeResidentialTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipResidentialTotal));
		base.Find<UILabel>("IncomePublicTransportTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipPublicTransportIncome));
		base.Find<UILabel>("PublicTransportTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipPublicTransportExpenses));
		base.Find<UILabel>("IncomeCommercialTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipCommercialTotal));
		base.Find<UILabel>("IncomeIndustrialTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipIndustrialTotal));
		base.Find<UILabel>("IncomeOfficeTotal").add_eventTooltipHover(new MouseEventHandler(this.OnTooltipOfficeTotal));
		this.m_PoliciesExpenses = base.Find<UITextComponent>("PoliciesTotal");
		this.m_LoansExpenses = base.Find<UITextComponent>("LoansTotal");
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC))
		{
			UILabel uILabel = base.Find<UILabel>("WaterTotal");
			UISprite uISprite = uILabel.get_parent().Find<UISprite>("Icon");
			uILabel.set_tooltipLocaleID("EXPENSES_TT_WATER_WW");
			uISprite.set_tooltipLocaleID("EXPENSES_TT_WATER_WW");
		}
		if (SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC))
		{
			UILabel uILabel2 = base.Find<UILabel>("FireDepartmentTotal");
			UISprite uISprite2 = uILabel2.get_parent().Find<UISprite>("Icon");
			uILabel2.set_tooltipLocaleID("EXPENSES_TT_FIRE_ND");
			uISprite2.set_tooltipLocaleID("EXPENSES_TT_FIRE_ND");
		}
	}

	private void UpdateIncomeExpenses()
	{
		for (int i = 0; i < this.publicTransportDetailExpensesPolls.Length; i++)
		{
			this.publicTransportDetailExpensesPolls[i].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
		}
		for (int j = 0; j < this.basicIncomePolls.Length; j++)
		{
			this.basicIncomePolls[j].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.basicIncomePolls[j].Update(base.get_component());
		}
		for (int k = 0; k < this.basicExpensesPolls.Length; k++)
		{
			this.basicExpensesPolls[k].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.basicExpensesPolls[k].Update(base.get_component());
		}
		for (int l = 0; l < this.publicTransportTabPolls.Length; l++)
		{
			this.publicTransportTabPolls[l].Poll(Settings.moneyFormat, LocaleManager.get_cultureInfo());
			this.publicTransportTabPolls[l].Update(base.get_component());
		}
		this.m_PoliciesExpenses.set_text(this.expensesPoliciesTotal);
		this.m_LoansExpenses.set_text(this.expensesLoansTotal);
		this.m_residentialBarChart.Update();
		this.m_residentialIPC.Update();
		this.m_commercialBarChart.Update();
		this.m_commercialIPC.Update();
		this.m_industrialBarChart.Update();
		this.m_industrialIPC.Update();
		this.m_pubTransIncomeIPC.Update();
		this.m_pubTransExpensesIPC.Update();
	}

	private void Awake()
	{
		this.m_EnableAudio = false;
		this.m_MainPanel = base.Find<UITabContainer>("EconomyContainer");
		this.m_CloseButton = base.Find<UIButton>("Close");
		this.InitializeLoansTab();
		this.InitializeIncomeExpenses();
		base.get_component().Hide();
	}

	private void Start()
	{
		this.m_SavePosition = base.closeToolbarButton.get_absolutePosition();
	}

	private void RefreshPanel()
	{
		UITabstrip uITabstrip = base.Find<UITabstrip>("EconomyTabstrip");
		UIButton uIButton = uITabstrip.Find<UIButton>("Taxes");
		UIButton uIButton2 = uITabstrip.Find<UIButton>("Loans");
		UIButton uIButton3 = uITabstrip.Find<UIButton>("Budget");
		uIButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Taxes));
		uIButton2.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Loans));
		uIButton3.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Budget));
		uIButton.set_tooltip((!uIButton.get_isEnabled()) ? EconomyPanel.GetUnlockText(UnlockManager.Feature.Taxes) : string.Empty);
		uIButton2.set_tooltip((!uIButton2.get_isEnabled()) ? EconomyPanel.GetUnlockText(UnlockManager.Feature.Loans) : string.Empty);
		uIButton3.set_tooltip((!uIButton3.get_isEnabled()) ? EconomyPanel.GetUnlockText(UnlockManager.Feature.Budget) : string.Empty);
		if ((uITabstrip.get_selectedIndex() == 0 && !ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Taxes)) || (uITabstrip.get_selectedIndex() == 2 && !ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Loans)))
		{
			uITabstrip.set_selectedIndex(1);
		}
		this.PopulateBudgetTab();
		this.PopulateTaxesTab();
	}

	protected static string GetUnlockText(UnlockManager.Feature feature)
	{
		if (Singleton<UnlockManager>.get_exists())
		{
			MilestoneInfo milestoneInfo = Singleton<UnlockManager>.get_instance().m_properties.m_FeatureMilestones[(int)feature];
			if (!ToolsModifierControl.IsUnlocked(milestoneInfo))
			{
				return "<color #f4a755>" + milestoneInfo.GetLocalizedProgress().m_description + "</color>";
			}
		}
		return null;
	}

	private void OnEnable()
	{
		this.RefreshPanel();
		this.PopulateLoansTab();
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated += new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded += new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded += new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		}
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		if (Singleton<UnlockManager>.get_exists())
		{
			Singleton<UnlockManager>.get_instance().m_milestonesUpdated -= new Action(this.RefreshPanel);
		}
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
			Singleton<LoadingManager>.get_instance().m_levelPreLoaded -= new LoadingManager.LevelPreLoadedHandler(this.OnLevelUnloaded);
			Singleton<LoadingManager>.get_instance().m_levelPreUnloaded -= new LoadingManager.LevelPreUnloadedHandler(this.OnLevelUnloaded);
		}
	}

	private void OnLocaleChanged()
	{
		this.RefreshPanel();
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.m_EnableAudio = true;
	}

	private void OnLevelUnloaded()
	{
		this.m_EnableAudio = false;
	}

	public void SetParentButton(UIButton button)
	{
		base.Find<UIButton>("Taxes").GetComponent<TutorialUITag>().m_ParentOverride = button;
		base.Find<UIButton>("Loans").GetComponent<TutorialUITag>().m_ParentOverride = button;
		base.Find<UIButton>("Budget").GetComponent<TutorialUITag>().m_ParentOverride = button;
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	internal void Show(int tabIndex)
	{
		ToolsModifierControl.SetTool<IdleTool>();
		if (tabIndex != -1)
		{
			base.Find<UITabstrip>("EconomyTabstrip").set_selectedIndex(tabIndex);
		}
		if (!this.isVisible)
		{
			this.m_CloseButton.Hide();
			float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
			base.get_component().Show();
			ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
			{
				Vector3 relativePosition = base.get_component().get_relativePosition();
				relativePosition.y = val;
				base.get_component().set_relativePosition(relativePosition);
				base.closeToolbarButton.set_absolutePosition(this.m_CloseButton.get_absolutePosition());
			}, new AnimatedFloat(num + this.m_SafeMargin, num - base.get_component().get_size().y, this.m_ShowHideTime, this.m_ShowEasingType));
			if (this.m_EnableAudio && Singleton<AudioManager>.get_exists() && this.m_SwooshInSound != null)
			{
				Singleton<AudioManager>.get_instance().PlaySound(this.m_SwooshInSound, 1f);
			}
		}
		if (Singleton<EconomyManager>.get_exists())
		{
			GenericGuide guide = Singleton<EconomyManager>.get_instance().m_economyNotUsedGuide;
			if (guide != null && !guide.m_disabled)
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					guide.Disable();
				});
			}
		}
	}

	internal void Hide()
	{
		float num = base.get_component().get_parent().get_relativePosition().y + base.get_component().get_parent().get_size().y;
		ValueAnimator.Animate(base.GetType().ToString(), delegate(float val)
		{
			Vector3 relativePosition = base.get_component().get_relativePosition();
			relativePosition.y = val;
			base.get_component().set_relativePosition(relativePosition);
		}, new AnimatedFloat(num - base.get_component().get_size().y, num + this.m_SafeMargin, this.m_ShowHideTime, this.m_HideEasingType), delegate
		{
			base.get_component().Hide();
		});
		this.m_CloseButton.Show();
		base.closeToolbarButton.set_absolutePosition(this.m_SavePosition);
		if (base.get_component().get_isVisible() && this.m_EnableAudio && Singleton<AudioManager>.get_exists() && this.m_SwooshOutSound != null)
		{
			Singleton<AudioManager>.get_instance().PlaySound(this.m_SwooshOutSound, 1f);
		}
	}

	public void OnCloseButton()
	{
		base.CloseToolbar();
	}

	private void CheckLoaded()
	{
		if (Singleton<LoadingManager>.get_exists() && Singleton<LoadingManager>.get_instance().m_loadingComplete)
		{
			int num = 9;
			if (!Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Taxi))
			{
				this.m_IncomeTooltipPublicTransport.Find("Taxi").Hide();
				base.Find<UIPanel>("TransportTypeTaxi").Hide();
				num--;
			}
			if (!Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Tram))
			{
				this.m_IncomeTooltipPublicTransport.Find("Tram").Hide();
				base.Find<UIPanel>("TransportTypeTram").Hide();
				num--;
			}
			if (!Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.Monorail))
			{
				this.m_IncomeTooltipPublicTransport.Find("Monorail").Hide();
				base.Find<UIPanel>("TransportTypeMonorail").Hide();
				num--;
			}
			if (!Singleton<TransportManager>.get_instance().TransportTypeLoaded(TransportInfo.TransportType.CableCar))
			{
				this.m_IncomeTooltipPublicTransport.Find("CableCar").Hide();
				base.Find<UIPanel>("TransportTypeCableCar").Hide();
				num--;
			}
			this.m_IncomeTooltipPublicTransport.FitChildrenVertically(5f);
			if (num < 6)
			{
				base.Find<UISlicedSprite>("PublicTransportBackgroundSprite2").Hide();
				base.Find<UIPanel>("PublicTransportChartLabels2").Hide();
				base.Find<UISlicedSprite>("PublicTransportBackgroundSprite").set_relativePosition(new Vector2(155f, 53f));
				base.Find<UIPanel>("PublicTransportChartLabels").set_relativePosition(new Vector2(149f, 33f));
				base.Find<UIPanel>("PublicTransportLayoutContainer").set_relativePosition(new Vector2(115f, 60f));
			}
			else
			{
				base.Find<UISlicedSprite>("PublicTransportBackgroundSprite2").set_height((float)((num - 5) * 33));
			}
			if (!Singleton<DistrictManager>.get_instance().IsPolicyLoaded(DistrictPolicies.Policies.Leisure))
			{
				this.m_IncomeTooltipCommercialTotal.Find("AfterDarkDLC").Hide();
			}
			else
			{
				this.m_IncomeTooltipCommercialTotal.Find("AfterDarkDLC").Show();
			}
			this.m_IncomeTooltipCommercialTotal.Find("GreenCitiesDLCCommercial").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC));
			this.m_IncomeTooltipCommercialTotal.FitChildrenVertically(5f);
			this.m_IncomeTooltipOffice.Find("GreenCitiesDLCOffice").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.GreenCitiesDLC));
			this.m_IncomeTooltipOffice.FitChildrenVertically(5f);
			this.m_Initialized = true;
		}
	}

	private void Update()
	{
		if (!this.m_Initialized)
		{
			this.CheckLoaded();
		}
		if (base.get_component().get_isVisible() && Singleton<EconomyManager>.get_exists())
		{
			this.m_MainPanel.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Economy));
			this.UpdateIncomeExpenses();
			if (this.m_LoansTab.get_isVisible())
			{
				this.PopulateLoansTab();
			}
		}
	}

	private static readonly int sDefaultBudget = 50;

	private static readonly int sDefaultTaxes = 15;

	private static readonly string kBudgetItemTemplate = "BudgetItem";

	private static readonly string kTaxesItemTemplate = "TaxesItem";

	private static readonly PositionData<ItemClass.Service>[] kRoadService = Utils.GetOrderedEnumData<ItemClass.Service>("Budget2");

	private static readonly PositionData<ItemClass.Service>[] kServices = Utils.GetOrderedEnumData<ItemClass.Service>("Budget");

	private static readonly PositionData<ItemClass.SubService>[] kSubServices = Utils.GetOrderedEnumData<ItemClass.SubService>("Budget");

	private static readonly PositionData<ItemClass.Zone>[] kZones = Utils.GetOrderedEnumData<ItemClass.Zone>("Taxes");

	public Color m_BackDividerColor = Color.get_white();

	public Color m_BackDividerAltColor = Color.get_white();

	public Color m_TakenLoanColor = Color.get_white();

	public Color m_AvailableLoanColor = Color.get_white();

	public AudioClip m_SwooshInSound;

	public AudioClip m_SwooshOutSound;

	public AudioClip m_BudgetSliderSound;

	public AudioClip m_TaxesSliderSound;

	public float m_SafeMargin = 20f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private bool m_Initialized;

	private UITabContainer m_MainPanel;

	private UIComponent m_LoansTab;

	private bool m_EnableAudio;

	private UIComponent m_IncomeTooltipIndustrial;

	private UIComponent m_IncomeTooltipResidential;

	private UIComponent m_IncomeTooltipOffice;

	private UIComponent m_IncomeTooltipCommercialTotal;

	private UIComponent m_IncomeTooltipPublicTransport;

	private UITextComponent m_PoliciesExpenses;

	private UITextComponent m_LoansExpenses;

	private UIButton m_CloseButton;

	private Vector3 m_SavePosition;

	private UICurrencyWrapper m_ExpensesLoansTotal = new UICurrencyWrapper(9223372036854775807L);

	private UICurrencyWrapper m_ExpensesPoliciesTotal = new UICurrencyWrapper(9223372036854775807L);

	private EconomyPanel.IncomeExpensesPoll[] basicExpensesPolls;

	private EconomyPanel.IncomeExpensesPoll[] basicIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] residentialDetailIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] publicTransportDetailIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] publicTransportDetailExpensesPolls;

	private EconomyPanel.IncomeExpensesPoll[] commercialDetailIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] industrialDetailIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] officeDetailIncomePolls;

	private EconomyPanel.IncomeExpensesPoll[] budgetExpensesPolls;

	private EconomyPanel.IncomeExpensesPoll[] publicTransportTabPolls;

	public Color32 m_lowDensityResidentialBarColor;

	public Color32 m_highDensityResidentialBarColor;

	public Color32 m_lowDensityResidentialBarColorEco;

	public Color32 m_highDensityResidentialBarColorEco;

	public Color32 m_lowDensityCommercialBarColor;

	public Color32 m_highDensityCommercialBarColor;

	public Color32 m_specializationsCommercialBarColor;

	public Color32 m_industrialBarColor;

	public Color32 m_officeBarColor;

	private BarChart m_residentialBarChart;

	private BarChart m_commercialBarChart;

	private BarChart m_industrialBarChart;

	private IncomePercentagePieChart m_residentialIPC;

	private IncomePercentagePieChart m_commercialIPC;

	private IncomePercentagePieChart m_industrialIPC;

	private IncomePercentagePieChart m_pubTransIncomeIPC;

	private IncomePercentagePieChart m_pubTransExpensesIPC;

	private sealed class IncomeExpensesPoll
	{
		public IncomeExpensesPoll(ItemClass.Service service, string incomeFieldName, string expensesFieldName, ItemClass.Service secondService = ItemClass.Service.None)
		{
			this.m_Service = service;
			this.m_SecondService = secondService;
			this.m_SubService = new ItemClass.SubService[1];
			this.m_Level = ItemClass.Level.None;
			this.m_IncomeFieldName = incomeFieldName;
			this.m_ExpensesFieldName = expensesFieldName;
		}

		public IncomeExpensesPoll(ItemClass.Service service, ItemClass.SubService subService, string incomeFieldName, string expensesFieldName, string profitFieldName = "")
		{
			this.m_Service = service;
			this.m_SubService = new ItemClass.SubService[]
			{
				subService
			};
			this.m_Level = ItemClass.Level.None;
			this.m_IncomeFieldName = incomeFieldName;
			this.m_ExpensesFieldName = expensesFieldName;
			this.m_ProfitFieldName = profitFieldName;
		}

		public IncomeExpensesPoll(ItemClass.Service service, ItemClass.SubService[] subService, string incomeFieldName, string expensesFieldName)
		{
			this.m_Service = service;
			this.m_SubService = subService;
			this.m_Level = ItemClass.Level.None;
			this.m_IncomeFieldName = incomeFieldName;
			this.m_ExpensesFieldName = expensesFieldName;
		}

		public IncomeExpensesPoll(ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level, string incomeFieldName, string expensesFieldName)
		{
			this.m_Service = service;
			this.m_SubService = new ItemClass.SubService[]
			{
				subService
			};
			this.m_Level = level;
			this.m_IncomeFieldName = incomeFieldName;
			this.m_ExpensesFieldName = expensesFieldName;
		}

		public IncomeExpensesPoll(ItemClass.Service service, ItemClass.SubService[] subService, ItemClass.Level level, string incomeFieldName, string expensesFieldName)
		{
			this.m_Service = service;
			this.m_SubService = subService;
			this.m_Level = level;
			this.m_IncomeFieldName = incomeFieldName;
			this.m_ExpensesFieldName = expensesFieldName;
		}

		public string incomeString
		{
			get
			{
				return this.m_IncomeString;
			}
		}

		public string expensesString
		{
			get
			{
				return this.m_ExpensesString;
			}
		}

		public double income
		{
			get
			{
				return (double)this.m_Income;
			}
		}

		public double expenses
		{
			get
			{
				return (double)this.m_Expenses;
			}
		}

		public void Poll(string moneyFormat, CultureInfo moneyLocale)
		{
			long num = 0L;
			long num2 = 0L;
			if (Singleton<EconomyManager>.get_exists())
			{
				for (int i = 0; i < this.m_SubService.Length; i++)
				{
					long num3 = 0L;
					long num4 = 0L;
					Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(this.m_Service, this.m_SubService[i], this.m_Level, out num3, out num4);
					num += num3;
					num2 += num4;
					if (this.m_SecondService != ItemClass.Service.None)
					{
						Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(this.m_SecondService, this.m_SubService[i], this.m_Level, out num3, out num4);
						num += num3;
						num2 += num4;
					}
				}
				if (this.m_Service == ItemClass.Service.FireDepartment)
				{
					for (int j = 0; j < this.m_SubService.Length; j++)
					{
						long num5 = 0L;
						long num6 = 0L;
						Singleton<EconomyManager>.get_instance().GetIncomeAndExpenses(ItemClass.Service.Disaster, this.m_SubService[j], this.m_Level, out num5, out num6);
						num += num5;
						num2 += num6;
					}
				}
			}
			long num7 = num - num2;
			if (num != this.m_Income)
			{
				this.m_Income = num;
				this.m_IncomeString = ((double)this.m_Income / 100.0).ToString(moneyFormat, moneyLocale);
			}
			if (num2 != this.m_Expenses)
			{
				this.m_Expenses = num2;
				this.m_ExpensesString = ((double)this.m_Expenses / 100.0).ToString(moneyFormat, moneyLocale);
			}
			if (num7 != this.m_Profit)
			{
				this.m_Profit = num7;
				this.m_ProfitString = ((double)this.m_Profit / 100.0).ToString(moneyFormat, moneyLocale);
			}
		}

		public void Update(UIComponent root)
		{
			if (!this.m_Bound)
			{
				this.m_Bound = true;
				this.m_IncomeField = root.Find<UITextComponent>(this.m_IncomeFieldName);
				this.m_ExpensesField = root.Find<UITextComponent>(this.m_ExpensesFieldName);
				this.m_ProfitField = root.Find<UITextComponent>(this.m_ProfitFieldName);
			}
			else
			{
				if (this.m_IncomeField != null)
				{
					this.m_IncomeField.set_text(this.incomeString);
				}
				if (this.m_ExpensesField != null)
				{
					this.m_ExpensesField.set_text(this.expensesString);
				}
				if (this.m_ProfitField != null)
				{
					this.m_ProfitField.set_text(this.m_ProfitString);
				}
			}
		}

		private ItemClass.Service m_Service;

		private ItemClass.Service m_SecondService;

		private ItemClass.SubService[] m_SubService;

		private ItemClass.Level m_Level;

		private long m_Income = 9223372036854775807L;

		private long m_Expenses = 9223372036854775807L;

		private long m_Profit = 9223372036854775807L;

		private string m_IncomeString = "N/A";

		private string m_ExpensesString = "N/A";

		private string m_ProfitString = "N/A";

		private bool m_Bound;

		private string m_IncomeFieldName;

		private string m_ExpensesFieldName;

		private string m_ProfitFieldName;

		private UITextComponent m_IncomeField;

		private UITextComponent m_ExpensesField;

		private UITextComponent m_ProfitField;
	}
}
