﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class EconomyWrapper : IEconomy
{
	public EconomyWrapper(EconomyManager economyManager)
	{
		this.m_economyManager = economyManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	private void GetImplementations()
	{
		this.OnEconomyExtensionsReleased();
		this.m_EconomyExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IEconomyExtension>();
		this.OnEconomyExtensionsCreated();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnEconomyExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	private void OnEconomyExtensionsCreated()
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			try
			{
				this.m_EconomyExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_EconomyExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnEconomyExtensionsReleased()
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			try
			{
				this.m_EconomyExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_EconomyExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private EconomyResource Convert(EconomyManager.Resource resource)
	{
		EconomyResource result = 0;
		switch (resource)
		{
		case EconomyManager.Resource.Construction:
			return 1;
		case EconomyManager.Resource.Maintenance:
			return 2;
		case EconomyManager.Resource.LoanAmount:
			return 4;
		case EconomyManager.Resource.LoanPayment:
			return 8;
		case EconomyManager.Resource.PrivateIncome:
			return 16;
		case EconomyManager.Resource.CitizenIncome:
			return 32;
		case EconomyManager.Resource.TourismIncome:
			return 64;
		case EconomyManager.Resource.PublicIncome:
			return 128;
		case EconomyManager.Resource.RewardAmount:
			return 256;
		case EconomyManager.Resource.PolicyCost:
			return 512;
		case EconomyManager.Resource.BailoutAmount:
			return 1024;
		case EconomyManager.Resource.RefundAmount:
			return 2048;
		case EconomyManager.Resource.LandPrice:
			return 4096;
		case EconomyManager.Resource.Landscaping:
			return 8192;
		case EconomyManager.Resource.FeePayment:
			return 16384;
		default:
			return result;
		}
	}

	public long OnUpdateMoneyAmount(long internalCashAmount)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			internalCashAmount = this.m_EconomyExtensions[i].OnUpdateMoneyAmount(internalCashAmount);
		}
		return internalCashAmount;
	}

	public bool OnPeekResource(EconomyManager.Resource resource, ref int amount)
	{
		bool result = false;
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			IEconomyExtension economyExtension = this.m_EconomyExtensions[i];
			if (economyExtension.get_OverrideDefaultPeekResource())
			{
				result = true;
			}
			amount = economyExtension.OnPeekResource(this.Convert(resource), amount);
		}
		return result;
	}

	public void OnFetchResource(EconomyManager.Resource resource, ref int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			amount = this.m_EconomyExtensions[i].OnFetchResource(this.Convert(resource), amount, service, subService, level);
		}
	}

	public void OnAddResource(EconomyManager.Resource resource, ref int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			amount = this.m_EconomyExtensions[i].OnAddResource(this.Convert(resource), amount, service, subService, level);
		}
	}

	public void OnGetConstructionCost(ref int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			amount = this.m_EconomyExtensions[i].OnGetConstructionCost(amount, service, subService, level);
		}
	}

	public void OnGetMaintenanceCost(ref int amount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			amount = this.m_EconomyExtensions[i].OnGetMaintenanceCost(amount, service, subService, level);
		}
	}

	public void OnGetRelocationCost(int constructionCost, ref int relocationCost, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			relocationCost = this.m_EconomyExtensions[i].OnGetRelocationCost(constructionCost, relocationCost, service, subService, level);
		}
	}

	public void OnGetRefundAmount(int constructionCost, ref int refundAmount, ItemClass.Service service, ItemClass.SubService subService, ItemClass.Level level)
	{
		for (int i = 0; i < this.m_EconomyExtensions.Count; i++)
		{
			refundAmount = this.m_EconomyExtensions[i].OnGetRefundAmount(constructionCost, refundAmount, service, subService, level);
		}
	}

	public long currentMoneyAmount
	{
		get
		{
			return this.m_economyManager.LastCashAmount;
		}
	}

	public long internalMoneyAmount
	{
		get
		{
			return this.m_economyManager.InternalCashAmount;
		}
	}

	private EconomyManager m_economyManager;

	private List<IEconomyExtension> m_EconomyExtensions = new List<IEconomyExtension>();
}
