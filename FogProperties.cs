﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class FogProperties : MonoBehaviour
{
	public FogProperties()
	{
		Gradient gradient = new Gradient();
		gradient.set_colorKeys(new GradientColorKey[]
		{
			new GradientColorKey(new Color32(9, 8, 9, 255), 0.225f),
			new GradientColorKey(new Color32(79, 75, 68, 255), 0.25f),
			new GradientColorKey(new Color32(79, 75, 68, 255), 0.75f),
			new GradientColorKey(new Color32(9, 8, 9, 255), 0.775f)
		});
		gradient.set_alphaKeys(new GradientAlphaKey[]
		{
			new GradientAlphaKey(1f, 0f),
			new GradientAlphaKey(1f, 1f)
		});
		this.m_PollutionColorGradient = gradient;
		this.m_PollutionAmount = 0.68f;
		this.m_FogPatternChangeSpeed = 0.5f;
		this.m_ColorDecay = 0.2f;
		this.m_Scattering = 1f;
		this.m_FoggyFogDensity = 0.0001f;
		this.m_FoggyNoiseContribution = 1f;
		base..ctor();
	}

	private Material fogMaterial
	{
		get
		{
			if (this.m_Effect == null)
			{
				if (Camera.get_main() != null)
				{
					this.m_Effect = Camera.get_main().GetComponent<DayNightFogEffect>();
					if (this.m_Effect != null)
					{
						return this.m_Effect.material;
					}
				}
				return null;
			}
			return this.m_Effect.material;
		}
	}

	private void OnEnable()
	{
		this.ID_FogParams0 = Shader.PropertyToID("_FogParams0");
		this.ID_FogParams1 = Shader.PropertyToID("_FogParams1");
		this.ID_Fog3DNoiseTex = Shader.PropertyToID("_Fog3DNoiseTex");
		this.ID_FogSpeed = Shader.PropertyToID("_FogSpeed");
		this.ID_FogNoiseGainConstrastStepScale = Shader.PropertyToID("_FogNoiseGainConstrastStepScale");
		this.ID_FogPollutionColor = Shader.PropertyToID("_FogPollutionColor");
		this.ID_FogPollutionParams = Shader.PropertyToID("_FogPollutionParams");
		if (Application.get_isPlaying() && Camera.get_main() != null)
		{
			DayNightFogEffect component = Camera.get_main().GetComponent<DayNightFogEffect>();
			if (component != null)
			{
				component.set_enabled(true);
			}
			FogEffect component2 = Camera.get_main().GetComponent<FogEffect>();
			if (component2 != null)
			{
				component2.set_enabled(false);
			}
		}
	}

	private void Update()
	{
		if (this.fogMaterial != null)
		{
			float num = 0f;
			if (Singleton<WeatherManager>.get_exists())
			{
				num = Singleton<WeatherManager>.get_instance().m_currentFog;
			}
			bool flag = this.m_edgeFog;
			if (Singleton<ToolManager>.get_exists() && (Singleton<ToolManager>.get_instance().m_properties.m_mode & (ItemClass.Availability.MapEditor | ItemClass.Availability.AssetEditor | ItemClass.Availability.ScenarioEditor)) != ItemClass.Availability.None)
			{
				flag = false;
			}
			Vector4 vector;
			vector.x = ((!flag) ? 0f : 1f);
			vector.y = Mathf.Lerp(this.m_FogDensity, this.m_FoggyFogDensity, num);
			vector.z = 1f / Mathf.Lerp(this.m_FogStart, this.m_FoggyFogStart, num);
			vector.w = this.m_FogDistance;
			Vector4 vector2;
			vector2.x = 1f / this.m_FogHeight;
			vector2.y = this.m_ColorDecay;
			vector2.z = this.m_Scattering;
			vector2.w = this.m_EdgeFogDistance;
			Vector4 vector3;
			vector3.x = this.m_PollutionAmount;
			vector3.y = 0f;
			vector3.z = 1f / this.m_HorizonHeight;
			vector3.w = 1f - Mathf.Lerp(this.m_NoiseContribution, this.m_FoggyNoiseContribution, num);
			Shader.SetGlobalVector(this.ID_FogParams0, vector);
			Shader.SetGlobalVector(this.ID_FogParams1, vector2);
			Shader.SetGlobalColor(this.ID_FogPollutionColor, this.m_PollutionColorGradient.Evaluate(DayNightProperties.instance.normalizedTimeOfDay));
			Shader.SetGlobalVector(this.ID_FogPollutionParams, vector3);
			Shader.SetGlobalTexture(this.ID_Fog3DNoiseTex, this.m_3DNoiseTexture);
			Vector4 vector4 = Vector4.get_zero();
			if (Singleton<WeatherManager>.get_exists() && Singleton<SimulationManager>.get_exists())
			{
				float num2 = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta * 60f;
				float num3 = Singleton<WeatherManager>.get_instance().m_windDirection * 0.0174532924f;
				vector4.x = Mathf.Sin(num3);
				vector4.y = this.m_FogPatternChangeSpeed;
				vector4.z = Mathf.Cos(num3);
				vector4.w = 0f;
				vector4 *= this.m_WindSpeed;
				this.m_SpeedOffset += vector4 * num2;
			}
			Shader.SetGlobalVector(this.ID_FogSpeed, this.m_SpeedOffset);
			Shader.SetGlobalVector(this.ID_FogNoiseGainConstrastStepScale, new Vector4(this.m_NoiseGain, this.m_NoiseContrast, this.m_NoiseStepSize, this.m_NoiseScale));
		}
	}

	private int ID_Fog3DNoiseTex;

	private int ID_FogSpeed;

	private int ID_FogNoiseGainConstrastStepScale;

	private int ID_FogParams0;

	private int ID_FogParams1;

	private int ID_FogPollutionColor;

	private int ID_FogPollutionParams;

	public bool m_edgeFog = true;

	[Range(0f, 3800f)]
	public float m_EdgeFogDistance;

	[Range(0f, 5000f)]
	public float m_FogHeight = 5000f;

	[Range(0f, 5000f)]
	public float m_HorizonHeight = 800f;

	public Texture3D m_3DNoiseTexture;

	public float m_NoiseGain = 1f;

	public float m_NoiseContrast;

	[Range(0f, 0.01f)]
	public float m_FogDensity = 0.0001f;

	[Range(0f, 4800f)]
	public float m_FogStart;

	[Range(0f, 20000f)]
	public float m_FogDistance = 10f;

	public float m_NoiseStepSize = 4f;

	public float m_NoiseScale = 0.01f;

	[Range(0f, 1f)]
	public float m_NoiseContribution = 1f;

	public Gradient m_PollutionColorGradient;

	[Range(0f, 1f)]
	public float m_PollutionAmount;

	public float m_WindSpeed;

	public float m_FogPatternChangeSpeed;

	[Range(0.06f, 0.4f)]
	public float m_ColorDecay;

	[Range(0f, 1f)]
	public float m_Scattering;

	[Header("Foggy Settings"), Range(0f, 0.01f)]
	public float m_FoggyFogDensity;

	[Range(0f, 4800f)]
	public float m_FoggyFogStart;

	[Range(0f, 1f)]
	public float m_FoggyNoiseContribution;

	private Vector4 m_SpeedOffset;

	private DayNightFogEffect m_Effect;
}
