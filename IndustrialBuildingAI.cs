﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class IndustrialBuildingAI : PrivateBuildingAI
{
	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_info.m_class.m_service != ItemClass.Service.Industrial)
		{
			throw new PrefabException(this.m_info, "IndustrialBuildingAI used with other service type (" + this.m_info.m_class.m_service + ")");
		}
	}

	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.NaturalResources:
			if (this.ShowConsumption(buildingID, ref data))
			{
				return IndustrialBuildingAI.GetResourceColor(this.m_info.m_class.m_subService);
			}
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		case InfoManager.InfoMode.LandValue:
		case InfoManager.InfoMode.Districts:
			IL_19:
			if (infoMode != InfoManager.InfoMode.BuildingLevel)
			{
				return base.GetColor(buildingID, ref data, infoMode);
			}
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			if (this.m_info.m_class.m_subService == ItemClass.SubService.IndustrialGeneric)
			{
				return Color.Lerp(Singleton<InfoManager>.get_instance().m_properties.m_neutralColor, Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f, 0.333f + (float)this.m_info.m_class.m_level * 0.333f);
			}
			return Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6] * 0.5f;
		case InfoManager.InfoMode.Connections:
		{
			if (!this.ShowConsumption(buildingID, ref data))
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			InfoManager.SubInfoMode currentSubMode = Singleton<InfoManager>.get_instance().CurrentSubMode;
			if (currentSubMode == InfoManager.SubInfoMode.Default)
			{
				TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason(buildingID);
				if (incomingTransferReason != TransferManager.TransferReason.None && (data.m_tempImport != 0 || data.m_finalImport != 0))
				{
					return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)incomingTransferReason];
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			else
			{
				if (currentSubMode != InfoManager.SubInfoMode.WaterPower)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
				if (outgoingTransferReason != TransferManager.TransferReason.None && (data.m_tempExport != 0 || data.m_finalExport != 0))
				{
					return Singleton<TransferManager>.get_instance().m_properties.m_resourceColors[(int)outgoingTransferReason];
				}
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			break;
		}
		}
		goto IL_19;
	}

	public static Color GetResourceColor(ItemClass.SubService subService)
	{
		switch (subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[4];
		case ItemClass.SubService.IndustrialFarming:
			return Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[3];
		case ItemClass.SubService.IndustrialOil:
			return Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[2];
		case ItemClass.SubService.IndustrialOre:
			return Singleton<NaturalResourceManager>.get_instance().m_properties.m_resourceColors[0];
		default:
			return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
		}
	}

	public override void PlayAudio(AudioManager.ListenerInfo listenerInfo, ushort buildingID, ref Building data)
	{
		if (data.m_fireIntensity != 0 || !Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data);
			return;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		if (randomizer.Int32(5u) != 0)
		{
			return;
		}
		if ((data.m_flags & Building.Flags.Active) != Building.Flags.None)
		{
			base.PlayAudio(listenerInfo, buildingID, ref data, 0.25f);
		}
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		if (resource == NaturalResourceManager.Resource.Pollution)
		{
			return 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		if (resource == ImmaterialResourceManager.Resource.NoisePollution)
		{
			return 15;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.PrivateIncome)
		{
			int width = data.Width;
			int length = data.Length;
			int num = width * length;
			int num2 = (100 * num + 99) / 100;
			return num2 * 100;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		int num2 = (100 * num + 99) / 100;
		return -num2;
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int width = data.Width;
		int length = data.Length;
		int num = width * length;
		return (100 * num + 99) / 100;
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		return StringUtils.SafeFormat("{0}\n{1}: {2}\n{3}: {4}", new object[]
		{
			base.GetDebugString(buildingID, ref data),
			this.GetIncomingTransferReason(buildingID).ToString(),
			data.m_customBuffer1,
			this.GetOutgoingTransferReason().ToString(),
			data.m_customBuffer2
		});
	}

	public override string GetLevelUpInfo(ushort buildingID, ref Building data, out float progress)
	{
		if ((data.m_problems & Notification.Problem.FatalProblem) != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_IMPOSSIBLE");
		}
		if (this.m_info.m_class.m_subService != ItemClass.SubService.IndustrialGeneric)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_SPECIAL_INDUSTRY");
		}
		if (this.m_info.m_class.m_level == ItemClass.Level.Level3)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_WORKERS_HAPPY");
		}
		if (data.m_problems != Notification.Problem.None)
		{
			progress = 0f;
			return Locale.Get("LEVELUP_DISTRESS");
		}
		if (data.m_levelUpProgress == 0)
		{
			return base.GetLevelUpInfo(buildingID, ref data, out progress);
		}
		int num = (int)((data.m_levelUpProgress & 15) - 1);
		int num2 = (data.m_levelUpProgress >> 4) - 1;
		if (num <= num2)
		{
			progress = (float)num * 0.06666667f;
			return Locale.Get("LEVELUP_LOWTECH");
		}
		progress = (float)num2 * 0.06666667f;
		return Locale.Get("LEVELUP_SERVICES_NEEDED");
	}

	protected override string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return base.GetLocalizedStatusActive(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		int width = data.Width;
		int length = data.Length;
		int num = this.MaxIncomingLoadSize();
		int num2 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
		int consumptionDivider = this.GetConsumptionDivider();
		int num3 = Mathf.Max(num2 * 500 / consumptionDivider, num * 4);
		data.m_customBuffer1 = (ushort)Singleton<SimulationManager>.get_instance().m_randomizer.Int32(num3 - num, num3);
		DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
		if (specialization != DistrictPolicies.Specialization.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(data.m_position);
			District[] expr_9C_cp_0 = instance.m_districts.m_buffer;
			byte expr_9C_cp_1 = district;
			expr_9C_cp_0[(int)expr_9C_cp_1].m_specializationPoliciesEffect = (expr_9C_cp_0[(int)expr_9C_cp_1].m_specializationPoliciesEffect | specialization);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		if (version < 187u)
		{
			int width = data.Width;
			int length = data.Length;
			int num = this.MaxIncomingLoadSize();
			int num2 = this.MaxOutgoingLoadSize();
			int num3 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
			int consumptionDivider = this.GetConsumptionDivider();
			int num4 = Mathf.Max(num3 * 500 / consumptionDivider, num * 2);
			int num5 = Mathf.Max(num3 * 500 / consumptionDivider, num * 4);
			int num6 = Mathf.Max(num3 * 500, num2);
			int num7 = Mathf.Max(num3 * 500, num2 * 2);
			data.m_customBuffer1 = (ushort)((int)data.m_customBuffer1 + num5 - num4);
			data.m_customBuffer2 = (ushort)((int)data.m_customBuffer2 + num7 - num6);
		}
	}

	protected override void ManualActivation(ushort buildingID, ref Building buildingData)
	{
		Vector3 position = buildingData.m_position;
		position.y += this.m_info.m_size.y;
		Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.GainTaxes, position, 1.5f);
	}

	protected override void ManualDeactivation(ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			Singleton<NotificationManager>.get_instance().AddWaveEvent(buildingData.m_position, NotificationEvent.Type.Happy, ImmaterialResourceManager.Resource.Abandonment, (float)(-(float)buildingData.Width * buildingData.Length), 64f);
		}
		else
		{
			Vector3 position = buildingData.m_position;
			position.y += this.m_info.m_size.y;
			Singleton<NotificationManager>.get_instance().AddEvent(NotificationEvent.Type.LoseTaxes, position, 1.5f);
		}
	}

	public override void StartTransfer(ushort buildingID, ref Building data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == this.GetOutgoingTransferReason())
		{
			VehicleInfo randomVehicleInfo = Singleton<VehicleManager>.get_instance().GetRandomVehicleInfo(ref Singleton<SimulationManager>.get_instance().m_randomizer, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
			if (randomVehicleInfo != null)
			{
				Array16<Vehicle> vehicles = Singleton<VehicleManager>.get_instance().m_vehicles;
				ushort num;
				if (Singleton<VehicleManager>.get_instance().CreateVehicle(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, randomVehicleInfo, data.m_position, material, false, true))
				{
					randomVehicleInfo.m_vehicleAI.SetSource(num, ref vehicles.m_buffer[(int)num], buildingID);
					randomVehicleInfo.m_vehicleAI.StartTransfer(num, ref vehicles.m_buffer[(int)num], material, offer);
					ushort building = offer.Building;
					if (building != 0 && (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_flags & Building.Flags.IncomingOutgoing) != Building.Flags.None)
					{
						int amount;
						int num2;
						randomVehicleInfo.m_vehicleAI.GetSize(num, ref vehicles.m_buffer[(int)num], out amount, out num2);
						CommonBuildingAI.ExportResource(buildingID, ref data, material, amount);
					}
					data.m_outgoingProblemTimer = 0;
				}
			}
		}
		else
		{
			base.StartTransfer(buildingID, ref data, material, offer);
		}
	}

	public override void ModifyMaterialBuffer(ushort buildingID, ref Building data, TransferManager.TransferReason material, ref int amountDelta)
	{
		if (material == this.GetIncomingTransferReason(buildingID))
		{
			int width = data.Width;
			int length = data.Length;
			int num = this.MaxIncomingLoadSize();
			int num2 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
			int consumptionDivider = this.GetConsumptionDivider();
			int num3 = Mathf.Max(num2 * 500 / consumptionDivider, num * 4);
			int customBuffer = (int)data.m_customBuffer1;
			amountDelta = Mathf.Clamp(amountDelta, 0, num3 - customBuffer);
			data.m_customBuffer1 = (ushort)(customBuffer + amountDelta);
		}
		else if (material == this.GetOutgoingTransferReason())
		{
			int customBuffer2 = (int)data.m_customBuffer2;
			amountDelta = Mathf.Clamp(amountDelta, -customBuffer2, 0);
			data.m_customBuffer2 = (ushort)(customBuffer2 + amountDelta);
		}
		else
		{
			base.ModifyMaterialBuffer(buildingID, ref data, material, ref amountDelta);
		}
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason(buildingID);
		if (incomingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer(incomingTransferReason, offer);
		}
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
			offer2.Building = buildingID;
			Singleton<TransferManager>.get_instance().RemoveOutgoingOffer(outgoingTransferReason, offer2);
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		DistrictManager instance2 = Singleton<DistrictManager>.get_instance();
		byte district = instance2.GetDistrict(buildingData.m_position);
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			instance2.m_districts.m_buffer[(int)district].AddIndustrialData(buildingData.Width * buildingData.Length, (buildingData.m_flags & Building.Flags.Abandoned) != Building.Flags.None, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255, (buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255, this.m_info.m_class.m_subService);
		}
		if ((buildingData.m_levelUpProgress == 255 || (buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None) && buildingData.m_fireIntensity == 0 && instance.m_randomizer.Int32(10u) == 0)
		{
			DistrictPolicies.Specialization specializationPolicies = instance2.m_districts.m_buffer[(int)district].m_specializationPolicies;
			DistrictPolicies.Specialization specialization = this.SpecialPolicyNeeded();
			if (specialization != DistrictPolicies.Specialization.None)
			{
				if ((specializationPolicies & specialization) == DistrictPolicies.Specialization.None)
				{
					if (Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
					{
						buildingData.m_flags |= Building.Flags.Demolishing;
						instance.m_currentBuildIndex += 1u;
					}
				}
				else
				{
					District[] expr_175_cp_0 = instance2.m_districts.m_buffer;
					byte expr_175_cp_1 = district;
					expr_175_cp_0[(int)expr_175_cp_1].m_specializationPoliciesEffect = (expr_175_cp_0[(int)expr_175_cp_1].m_specializationPoliciesEffect | specialization);
				}
			}
			else if ((specializationPolicies & (DistrictPolicies.Specialization.Forest | DistrictPolicies.Specialization.Farming | DistrictPolicies.Specialization.Oil | DistrictPolicies.Specialization.Ore)) != DistrictPolicies.Specialization.None && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance.m_currentBuildIndex)
			{
				buildingData.m_flags |= Building.Flags.Demolishing;
				instance.m_currentBuildIndex += 1u;
			}
		}
		uint num = (instance.m_currentFrameIndex & 3840u) >> 8;
		if (num == 15u)
		{
			buildingData.m_finalImport = buildingData.m_tempImport;
			buildingData.m_finalExport = buildingData.m_tempExport;
			buildingData.m_tempImport = 0;
			buildingData.m_tempExport = 0;
		}
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
		District[] expr_52_cp_0 = instance.m_districts.m_buffer;
		byte expr_52_cp_1 = district;
		expr_52_cp_0[(int)expr_52_cp_1].m_servicePoliciesEffect = (expr_52_cp_0[(int)expr_52_cp_1].m_servicePoliciesEffect | (servicePolicies & (DistrictPolicies.Services.PowerSaving | DistrictPolicies.Services.WaterSaving | DistrictPolicies.Services.SmokeDetectors | DistrictPolicies.Services.Recycling | DistrictPolicies.Services.ExtraInsulation | DistrictPolicies.Services.NoElectricity | DistrictPolicies.Services.OnlyElectricity)));
		District[] expr_76_cp_0 = instance.m_districts.m_buffer;
		byte expr_76_cp_1 = district;
		expr_76_cp_0[(int)expr_76_cp_1].m_cityPlanningPoliciesEffect = (expr_76_cp_0[(int)expr_76_cp_1].m_cityPlanningPoliciesEffect | (cityPlanningPolicies & (DistrictPolicies.CityPlanning.IndustrySpace | DistrictPolicies.CityPlanning.LightningRods)));
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = base.HandleWorkers(buildingID, ref buildingData, ref behaviourData, ref num, ref num2, ref num3);
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num4 = 0;
		}
		if (Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			num4 = num4 + 1 >> 1;
		}
		TransferManager.TransferReason incomingTransferReason = this.GetIncomingTransferReason(buildingID);
		TransferManager.TransferReason outgoingTransferReason = this.GetOutgoingTransferReason();
		int width = buildingData.Width;
		int length = buildingData.Length;
		int num5 = this.MaxIncomingLoadSize();
		int num6 = this.MaxOutgoingLoadSize();
		int num7 = this.CalculateProductionCapacity(new Randomizer((int)buildingID), width, length);
		int consumptionDivider = this.GetConsumptionDivider();
		int num8 = Mathf.Max(num7 * 500 / consumptionDivider, num5 * 4);
		int num9 = num7 * 500;
		int num10 = Mathf.Max(num9, num6 * 2);
		if (num4 != 0)
		{
			int num11 = num10;
			if (incomingTransferReason != TransferManager.TransferReason.None)
			{
				num11 = Mathf.Min(num11, (int)buildingData.m_customBuffer1 * consumptionDivider);
			}
			if (outgoingTransferReason != TransferManager.TransferReason.None)
			{
				num11 = Mathf.Min(num11, num10 - (int)buildingData.m_customBuffer2);
			}
			num4 = Mathf.Max(0, Mathf.Min(num4, (num11 * 200 + num10 - 1) / num10));
			int num12 = (num7 * num4 + 9) / 10;
			num12 = Mathf.Max(0, Mathf.Min(num12, num11));
			if (incomingTransferReason != TransferManager.TransferReason.None)
			{
				buildingData.m_customBuffer1 -= (ushort)((num12 + consumptionDivider - 1) / consumptionDivider);
			}
			if (outgoingTransferReason != TransferManager.TransferReason.None)
			{
				if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.IndustrySpace) != DistrictPolicies.CityPlanning.None)
				{
					Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 38, this.m_info.m_class);
					buildingData.m_customBuffer2 = (ushort)Mathf.Min(num10, (int)buildingData.m_customBuffer2 + num12 * 2);
				}
				else
				{
					buildingData.m_customBuffer2 += (ushort)num12;
				}
				IndustrialBuildingAI.ProductType productType = IndustrialBuildingAI.GetProductType(outgoingTransferReason);
				if (productType != IndustrialBuildingAI.ProductType.None)
				{
					StatisticsManager instance2 = Singleton<StatisticsManager>.get_instance();
					StatisticBase statisticBase = instance2.Acquire<StatisticArray>(StatisticType.GoodsProduced);
					statisticBase.Acquire<StatisticInt32>((int)productType, 5).Add(num12);
				}
			}
			num4 = (num12 + 9) / 10;
		}
		int num13;
		int waterConsumption;
		int sewageAccumulation;
		int num14;
		int num15;
		this.GetConsumptionRates(new Randomizer((int)buildingID), num4, out num13, out waterConsumption, out sewageAccumulation, out num14, out num15);
		int heatingConsumption = 0;
		if (num13 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num13 * 3 + 8 >> 4);
				num15 = num15 * 95 / 100;
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num13 + 2 >> 2);
			}
		}
		if (num14 != 0 && (servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
		{
			num14 = Mathf.Max(1, num14 * 85 / 100);
			num15 = num15 * 95 / 100;
		}
		if (Singleton<SimulationManager>.get_instance().m_isNightTime)
		{
			num15 <<= 1;
		}
		if (num4 != 0)
		{
			int num16 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num13, ref heatingConsumption, ref waterConsumption, ref sewageAccumulation, ref num14, servicePolicies);
			num4 = (num4 * num16 + 99) / 100;
			if (num4 != 0)
			{
				if (num15 != 0)
				{
					Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.PrivateIncome, num15, this.m_info.m_class);
				}
				int num17;
				int num18;
				this.GetPollutionRates(num4, cityPlanningPolicies, out num17, out num18);
				if (num17 != 0)
				{
					if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.FilterIndustrialWaste) != DistrictPolicies.CityPlanning.None)
					{
						District[] expr_3BC_cp_0 = instance.m_districts.m_buffer;
						byte expr_3BC_cp_1 = district;
						expr_3BC_cp_0[(int)expr_3BC_cp_1].m_cityPlanningPoliciesEffect = (expr_3BC_cp_0[(int)expr_3BC_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.FilterIndustrialWaste);
						Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.PolicyCost, 13, this.m_info.m_class);
						if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(9u) == 0)
						{
							Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num17, num17, buildingData.m_position, 60f);
						}
					}
					else if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(3u) == 0)
					{
						Singleton<NaturalResourceManager>.get_instance().TryDumpResource(NaturalResourceManager.Resource.Pollution, num17, num17, buildingData.m_position, 60f);
					}
				}
				if (num18 != 0)
				{
					Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.NoisePollution, num18, buildingData.m_position, 60f);
				}
				if (num16 < 100)
				{
					buildingData.m_flags |= Building.Flags.RateReduced;
				}
				else
				{
					buildingData.m_flags &= ~Building.Flags.RateReduced;
				}
				buildingData.m_flags |= Building.Flags.Active;
			}
			else
			{
				buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
			}
		}
		else
		{
			num13 = 0;
			heatingConsumption = 0;
			waterConsumption = 0;
			sewageAccumulation = 0;
			num14 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.Sewage | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~(Building.Flags.RateReduced | Building.Flags.Active);
		}
		int num19 = 0;
		int wellbeing = 0;
		float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
		if (behaviourData.m_healthAccumulation != 0)
		{
			if (num != 0)
			{
				num19 = (behaviourData.m_healthAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Health, behaviourData.m_healthAccumulation, buildingData.m_position, radius);
		}
		if (behaviourData.m_wellbeingAccumulation != 0)
		{
			if (num != 0)
			{
				wellbeing = (behaviourData.m_wellbeingAccumulation + (num >> 1)) / num;
			}
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Wellbeing, behaviourData.m_wellbeingAccumulation, buildingData.m_position, radius);
		}
		int num20 = Citizen.GetHappiness(num19, wellbeing) * 15 / 100;
		if (num3 != 0)
		{
			num20 += num * 40 / num3;
		}
		if ((buildingData.m_problems & Notification.Problem.MajorProblem) == Notification.Problem.None)
		{
			num20 += 20;
		}
		if (buildingData.m_problems == Notification.Problem.None)
		{
			num20 += 25;
		}
		int taxRate = Singleton<EconomyManager>.get_instance().GetTaxRate(this.m_info.m_class);
		int num21 = (int)((ItemClass.Level)9 - this.m_info.m_class.m_level);
		int num22 = (int)((ItemClass.Level)11 - this.m_info.m_class.m_level);
		if (taxRate < num21)
		{
			num20 += num21 - taxRate;
		}
		if (taxRate > num22)
		{
			num20 -= taxRate - num22;
		}
		if (taxRate >= num22 + 4)
		{
			if (buildingData.m_taxProblemTimer != 0 || Singleton<SimulationManager>.get_instance().m_randomizer.Int32(32u) == 0)
			{
				int num23 = taxRate - num22 >> 2;
				buildingData.m_taxProblemTimer = (byte)Mathf.Min(255, (int)buildingData.m_taxProblemTimer + num23);
				if (buildingData.m_taxProblemTimer >= 96)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_taxProblemTimer >= 32)
				{
					buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
				}
			}
		}
		else
		{
			buildingData.m_taxProblemTimer = (byte)Mathf.Max(0, (int)(buildingData.m_taxProblemTimer - 1));
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TaxesTooHigh);
		}
		num20 = Mathf.Clamp(num20, 0, 100);
		buildingData.m_health = (byte)num19;
		buildingData.m_happiness = (byte)num20;
		buildingData.m_citizenCount = (byte)num;
		base.HandleDead(buildingID, ref buildingData, ref behaviourData, num2);
		int num24 = behaviourData.m_crimeAccumulation / 10;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num24 = num24 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num24, num);
		int num25 = (int)buildingData.m_crimeBuffer;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num, buildingData.m_position, radius);
			int num26 = behaviourData.m_educated0Count * 100 + behaviourData.m_educated1Count * 50 + behaviourData.m_educated2Count * 30;
			num26 = num26 / num + 100;
			buildingData.m_fireHazard = (byte)num26;
			num25 = (num25 + (num >> 1)) / num;
		}
		else
		{
			buildingData.m_fireHazard = 0;
			num25 = 0;
		}
		int num27 = 0;
		int num28 = 0;
		int num29 = 0;
		int num30 = 0;
		if (incomingTransferReason != TransferManager.TransferReason.None)
		{
			base.CalculateGuestVehicles(buildingID, ref buildingData, incomingTransferReason, ref num27, ref num28, ref num29, ref num30);
			buildingData.m_tempImport = (byte)Mathf.Clamp(num30, (int)buildingData.m_tempImport, 255);
		}
		int num31 = 0;
		int num32 = 0;
		int num33 = 0;
		int num34 = 0;
		if (outgoingTransferReason != TransferManager.TransferReason.None)
		{
			base.CalculateOwnVehicles(buildingID, ref buildingData, outgoingTransferReason, ref num31, ref num32, ref num33, ref num34);
			buildingData.m_tempExport = (byte)Mathf.Clamp(num34, (int)buildingData.m_tempExport, 255);
		}
		SimulationManager instance3 = Singleton<SimulationManager>.get_instance();
		uint num35 = (instance3.m_currentFrameIndex & 3840u) >> 8;
		if ((ulong)num35 == (ulong)((long)(buildingID & 15)) && this.m_info.m_class.m_subService == ItemClass.SubService.IndustrialGeneric && Singleton<ZoneManager>.get_instance().m_lastBuildIndex == instance3.m_currentBuildIndex && (buildingData.m_flags & Building.Flags.Upgrading) == Building.Flags.None)
		{
			this.CheckBuildingLevel(buildingID, ref buildingData, ref frameData, ref behaviourData, num);
		}
		if ((buildingData.m_flags & (Building.Flags.Completed | Building.Flags.Upgrading)) != Building.Flags.None)
		{
			Notification.Problem problem = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.NoResources | Notification.Problem.NoPlaceforGoods);
			if ((int)buildingData.m_customBuffer2 > num10 - (num9 >> 1))
			{
				buildingData.m_outgoingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_outgoingProblemTimer + 1));
				if (buildingData.m_outgoingProblemTimer >= 192)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods | Notification.Problem.MajorProblem);
				}
				else if (buildingData.m_outgoingProblemTimer >= 128)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoPlaceforGoods);
				}
			}
			else
			{
				buildingData.m_outgoingProblemTimer = 0;
			}
			if (buildingData.m_customBuffer1 == 0)
			{
				buildingData.m_incomingProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_incomingProblemTimer + 1));
				if (buildingData.m_incomingProblemTimer < 64)
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoResources);
				}
				else
				{
					problem = Notification.AddProblems(problem, Notification.Problem.NoResources | Notification.Problem.MajorProblem);
				}
			}
			else
			{
				buildingData.m_incomingProblemTimer = 0;
			}
			buildingData.m_problems = problem;
			instance.m_districts.m_buffer[(int)district].AddIndustrialData(ref behaviourData, num19, num20, num25, num3, num, Mathf.Max(0, num3 - num2), (int)this.m_info.m_class.m_level, num13, heatingConsumption, waterConsumption, sewageAccumulation, num14, num15, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255), (int)buildingData.m_finalImport, (int)buildingData.m_finalExport, this.m_info.m_class.m_subService);
			if (buildingData.m_fireIntensity == 0 && incomingTransferReason != TransferManager.TransferReason.None)
			{
				int num36 = num8 - (int)buildingData.m_customBuffer1 - num29;
				num36 -= num5 >> 1;
				if (num36 >= 0)
				{
					TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
					offer.Priority = num36 * 8 / num5;
					offer.Building = buildingID;
					offer.Position = buildingData.m_position;
					offer.Amount = 1;
					offer.Active = false;
					Singleton<TransferManager>.get_instance().AddIncomingOffer(incomingTransferReason, offer);
				}
			}
			if (buildingData.m_fireIntensity == 0 && outgoingTransferReason != TransferManager.TransferReason.None)
			{
				int num37 = Mathf.Max(1, num7 / 6);
				int customBuffer = (int)buildingData.m_customBuffer2;
				if (customBuffer >= num6 && num31 < num37)
				{
					TransferManager.TransferOffer offer2 = default(TransferManager.TransferOffer);
					offer2.Priority = customBuffer * 8 / num6;
					offer2.Building = buildingID;
					offer2.Position = buildingData.m_position;
					offer2.Amount = Mathf.Min(customBuffer / num6, num37 - num31);
					offer2.Active = true;
					Singleton<TransferManager>.get_instance().AddOutgoingOffer(outgoingTransferReason, offer2);
				}
			}
			base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
			base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		}
	}

	private void CheckBuildingLevel(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, ref Citizen.BehaviourData behaviour, int workerCount)
	{
		int num = behaviour.m_educated1Count + behaviour.m_educated2Count * 2 + behaviour.m_educated3Count * 3;
		int averageEducation;
		ItemClass.Level level;
		int num2;
		if (workerCount != 0)
		{
			averageEducation = (num * 100 + (workerCount >> 1)) / workerCount;
			num = (num * 20 + (workerCount >> 1)) / workerCount;
			if (num < 15)
			{
				level = ItemClass.Level.Level1;
				num2 = 1 + num;
			}
			else if (num < 30)
			{
				level = ItemClass.Level.Level2;
				num2 = 1 + (num - 15);
			}
			else
			{
				level = ItemClass.Level.Level3;
				num2 = 1;
			}
			if (level < this.m_info.m_class.m_level)
			{
				num2 = 1;
			}
			else if (level > this.m_info.m_class.m_level)
			{
				num2 = 15;
			}
		}
		else
		{
			level = ItemClass.Level.Level1;
			averageEducation = 0;
			num2 = 0;
		}
		int num3 = this.CalculateServiceValue(buildingID, ref buildingData);
		ItemClass.Level level2;
		int num4;
		if (num2 != 0)
		{
			if (num3 < 30)
			{
				level2 = ItemClass.Level.Level1;
				num4 = 1 + (num3 * 15 + 15) / 30;
			}
			else if (num3 < 60)
			{
				level2 = ItemClass.Level.Level2;
				num4 = 1 + ((num3 - 30) * 15 + 15) / 30;
			}
			else
			{
				level2 = ItemClass.Level.Level3;
				num4 = 1;
			}
			if (level2 < this.m_info.m_class.m_level)
			{
				num4 = 1;
			}
			else if (level2 > this.m_info.m_class.m_level)
			{
				num4 = 15;
			}
		}
		else
		{
			level2 = ItemClass.Level.Level1;
			num4 = 0;
		}
		bool flag = false;
		if (this.m_info.m_class.m_level == ItemClass.Level.Level2)
		{
			if (num3 < 20)
			{
				flag = true;
			}
		}
		else if (this.m_info.m_class.m_level == ItemClass.Level.Level3 && num3 < 40)
		{
			flag = true;
		}
		ItemClass.Level level3 = (ItemClass.Level)Mathf.Min((int)level, (int)level2);
		Singleton<BuildingManager>.get_instance().m_LevelUpWrapper.OnCalculateIndustrialLevelUp(ref level3, ref num2, ref num4, ref flag, averageEducation, num3, buildingID, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		if (flag)
		{
			buildingData.m_serviceProblemTimer = (byte)Mathf.Min(255, (int)(buildingData.m_serviceProblemTimer + 1));
			if (buildingData.m_serviceProblemTimer >= 8)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TooFewServices | Notification.Problem.MajorProblem);
			}
			else if (buildingData.m_serviceProblemTimer >= 4)
			{
				buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
			}
			else
			{
				buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
			}
		}
		else
		{
			buildingData.m_serviceProblemTimer = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TooFewServices);
		}
		if (level3 > this.m_info.m_class.m_level)
		{
			num2 = 0;
			num4 = 0;
			if (buildingData.m_problems == Notification.Problem.None && this.GetUpgradeInfo(buildingID, ref buildingData) != null && !Singleton<DisasterManager>.get_instance().IsEvacuating(buildingData.m_position))
			{
				frameData.m_constructState = 0;
				base.StartUpgrading(buildingID, ref buildingData);
			}
		}
		buildingData.m_levelUpProgress = (byte)(num2 | num4 << 4);
	}

	private int CalculateServiceValue(ushort buildingID, ref Building data)
	{
		ushort[] array;
		int num;
		Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResources(data.m_position, out array, out num);
		int resourceRate = (int)array[num + 7];
		int resourceRate2 = (int)array[num + 2];
		int resourceRate3 = (int)array[num];
		int resourceRate4 = (int)array[num + 6];
		int resourceRate5 = (int)array[num + 1];
		int resourceRate6 = (int)array[num + 13];
		int resourceRate7 = (int)array[num + 3];
		int resourceRate8 = (int)array[num + 4];
		int resourceRate9 = (int)array[num + 5];
		int resourceRate10 = (int)array[num + 19];
		int resourceRate11 = (int)array[num + 8];
		int resourceRate12 = (int)array[num + 18];
		int resourceRate13 = (int)array[num + 20];
		int resourceRate14 = (int)array[num + 21];
		int resourceRate15 = (int)array[num + 23];
		int num2 = 0;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate, 100, 500, 50, 100) / 3;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate2, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate3, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate4, 100, 500, 50, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate5, 100, 500, 50, 100) / 2;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate6, 100, 500, 50, 100) / 8;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate7, 100, 500, 50, 100) / 8;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate8, 100, 500, 50, 100) / 8;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate9, 100, 500, 50, 100) / 8;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate10, 100, 500, 50, 100);
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate13, 50, 100, 80, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate14, 100, 1000, 0, 100) / 5;
		num2 += ImmaterialResourceManager.CalculateResourceEffect(resourceRate15, 50, 100, 80, 100) / 5;
		num2 -= ImmaterialResourceManager.CalculateResourceEffect(resourceRate11, 100, 500, 50, 100) / 7;
		num2 -= ImmaterialResourceManager.CalculateResourceEffect(resourceRate12, 100, 500, 50, 100) / 7;
		byte resourceRate16;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out resourceRate16);
		return num2 - ImmaterialResourceManager.CalculateResourceEffect((int)resourceRate16, 50, 255, 50, 100) / 6;
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 255;
		fireTolerance = 10;
		return true;
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		switch (resource)
		{
		case ImmaterialResourceManager.Resource.HealthCare:
		case ImmaterialResourceManager.Resource.PoliceDepartment:
		case ImmaterialResourceManager.Resource.DeathCare:
		{
			int num;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num);
			int num2 = ImmaterialResourceManager.CalculateResourceEffect(num, 100, 500, 50, 100);
			int num3 = ImmaterialResourceManager.CalculateResourceEffect(num + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num3 - num2) / 250f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FireDepartment:
		{
			int num4;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num4);
			int num5 = ImmaterialResourceManager.CalculateResourceEffect(num4, 100, 500, 50, 100);
			int num6 = ImmaterialResourceManager.CalculateResourceEffect(num4 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num6 - num5) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.EducationElementary:
		case ImmaterialResourceManager.Resource.EducationHighSchool:
		case ImmaterialResourceManager.Resource.EducationUniversity:
		case ImmaterialResourceManager.Resource.Entertainment:
		{
			int num7;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num7);
			int num8 = ImmaterialResourceManager.CalculateResourceEffect(num7, 100, 500, 50, 100);
			int num9 = ImmaterialResourceManager.CalculateResourceEffect(num7 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num9 - num8) / 400f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.PublicTransport:
		{
			int num10;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num10);
			int num11 = ImmaterialResourceManager.CalculateResourceEffect(num10, 100, 500, 50, 100);
			int num12 = ImmaterialResourceManager.CalculateResourceEffect(num10 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num12 - num11) / 150f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.NoisePollution:
		{
			int num13;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num13);
			int num14 = ImmaterialResourceManager.CalculateResourceEffect(num13, 10, 100, 0, 100);
			int num15 = ImmaterialResourceManager.CalculateResourceEffect(num13 + Mathf.RoundToInt(amount), 10, 100, 0, 100);
			return Mathf.Clamp((float)(num15 - num14) / 350f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.Abandonment:
		{
			int num16;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num16);
			int num17 = ImmaterialResourceManager.CalculateResourceEffect(num16, 15, 50, 10, 20);
			int num18 = ImmaterialResourceManager.CalculateResourceEffect(num16 + Mathf.RoundToInt(amount), 15, 50, 10, 20);
			return Mathf.Clamp((float)(num18 - num17) / 350f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.CargoTransport:
		{
			int num19;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num19);
			int num20 = ImmaterialResourceManager.CalculateResourceEffect(num19, 100, 500, 50, 100);
			int num21 = ImmaterialResourceManager.CalculateResourceEffect(num19 + Mathf.RoundToInt(amount), 100, 500, 50, 100);
			return Mathf.Clamp((float)(num21 - num20) / 50f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.RadioCoverage:
		case ImmaterialResourceManager.Resource.DisasterCoverage:
		{
			int num22;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num22);
			int num23 = ImmaterialResourceManager.CalculateResourceEffect(num22, 50, 100, 20, 25);
			int num24 = ImmaterialResourceManager.CalculateResourceEffect(num22 + Mathf.RoundToInt(amount), 50, 100, 20, 25);
			return Mathf.Clamp((float)(num24 - num23) / 100f, -1f, 1f);
		}
		case ImmaterialResourceManager.Resource.FirewatchCoverage:
		{
			int num25;
			Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(resource, data.m_position, out num25);
			int num26 = ImmaterialResourceManager.CalculateResourceEffect(num25, 100, 1000, 0, 25);
			int num27 = ImmaterialResourceManager.CalculateResourceEffect(num25 + Mathf.RoundToInt(amount), 100, 1000, 0, 25);
			return Mathf.Clamp((float)(num27 - num26) / 100f, -1f, 1f);
		}
		}
		return base.GetEventImpact(buildingID, ref data, resource, amount);
	}

	public override float GetEventImpact(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource, float amount)
	{
		if ((data.m_flags & (Building.Flags.Abandoned | Building.Flags.Collapsed)) != Building.Flags.None)
		{
			return 0f;
		}
		if (resource != NaturalResourceManager.Resource.Pollution)
		{
			return base.GetEventImpact(buildingID, ref data, resource, amount);
		}
		byte b;
		Singleton<NaturalResourceManager>.get_instance().CheckPollution(data.m_position, out b);
		int num = ImmaterialResourceManager.CalculateResourceEffect((int)b, 50, 255, 50, 100);
		int num2 = ImmaterialResourceManager.CalculateResourceEffect((int)b + Mathf.RoundToInt(amount), 50, 255, 50, 100);
		return Mathf.Clamp((float)(num2 - num) / 300f, -1f, 1f);
	}

	private TransferManager.TransferReason GetIncomingTransferReason(ushort buildingID)
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return TransferManager.TransferReason.Logs;
		case ItemClass.SubService.IndustrialFarming:
			return TransferManager.TransferReason.Grain;
		case ItemClass.SubService.IndustrialOil:
			return TransferManager.TransferReason.Oil;
		case ItemClass.SubService.IndustrialOre:
			return TransferManager.TransferReason.Ore;
		default:
		{
			Randomizer randomizer;
			randomizer..ctor((int)buildingID);
			switch (randomizer.Int32(4u))
			{
			case 0:
				return TransferManager.TransferReason.Lumber;
			case 1:
				return TransferManager.TransferReason.Food;
			case 2:
				return TransferManager.TransferReason.Petrol;
			case 3:
				return TransferManager.TransferReason.Coal;
			default:
				return TransferManager.TransferReason.None;
			}
			break;
		}
		}
	}

	private TransferManager.TransferReason GetOutgoingTransferReason()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return TransferManager.TransferReason.Lumber;
		case ItemClass.SubService.IndustrialFarming:
			return TransferManager.TransferReason.Food;
		case ItemClass.SubService.IndustrialOil:
			return TransferManager.TransferReason.Petrol;
		case ItemClass.SubService.IndustrialOre:
			return TransferManager.TransferReason.Coal;
		default:
			return TransferManager.TransferReason.Goods;
		}
	}

	private int MaxIncomingLoadSize()
	{
		return 4000;
	}

	private int MaxOutgoingLoadSize()
	{
		return 8000;
	}

	private int GetConsumptionDivider()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return 1;
		case ItemClass.SubService.IndustrialFarming:
			return 1;
		case ItemClass.SubService.IndustrialOil:
			return 1;
		case ItemClass.SubService.IndustrialOre:
			return 1;
		default:
			return 4;
		}
	}

	private DistrictPolicies.Specialization SpecialPolicyNeeded()
	{
		switch (this.m_info.m_class.m_subService)
		{
		case ItemClass.SubService.IndustrialForestry:
			return DistrictPolicies.Specialization.Forest;
		case ItemClass.SubService.IndustrialFarming:
			return DistrictPolicies.Specialization.Farming;
		case ItemClass.SubService.IndustrialOil:
			return DistrictPolicies.Specialization.Oil;
		case ItemClass.SubService.IndustrialOre:
			return DistrictPolicies.Specialization.Ore;
		default:
			return DistrictPolicies.Specialization.None;
		}
	}

	public override int CalculateHomeCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override int CalculateVisitplaceCount(Randomizer r, int width, int length)
	{
		return 0;
	}

	public override void CalculateWorkplaceCount(Randomizer r, int width, int length, out int level0, out int level1, out int level2, out int level3)
	{
		ItemClass @class = this.m_info.m_class;
		int num = 0;
		level0 = 0;
		level1 = 0;
		level2 = 0;
		level3 = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 100;
				level0 = 100;
				level1 = 0;
				level2 = 0;
				level3 = 0;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 150;
				level0 = 20;
				level1 = 60;
				level2 = 20;
				level3 = 0;
			}
			else
			{
				num = 200;
				level0 = 5;
				level1 = 15;
				level2 = 30;
				level3 = 50;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			num = 100;
			level0 = 100;
			level1 = 0;
			level2 = 0;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			num = 100;
			level0 = 100;
			level1 = 0;
			level2 = 0;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			num = 150;
			level0 = 20;
			level1 = 60;
			level2 = 20;
			level3 = 0;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			num = 150;
			level0 = 20;
			level1 = 60;
			level2 = 20;
			level3 = 0;
		}
		if (num != 0)
		{
			num = Mathf.Max(200, width * length * num + r.Int32(100u)) / 100;
			int num2 = level0 + level1 + level2 + level3;
			if (num2 != 0)
			{
				level0 = (num * level0 + r.Int32((uint)num2)) / num2;
				num -= level0;
			}
			num2 = level1 + level2 + level3;
			if (num2 != 0)
			{
				level1 = (num * level1 + r.Int32((uint)num2)) / num2;
				num -= level1;
			}
			num2 = level2 + level3;
			if (num2 != 0)
			{
				level2 = (num * level2 + r.Int32((uint)num2)) / num2;
				num -= level2;
			}
			level3 = num;
		}
	}

	public override int CalculateProductionCapacity(Randomizer r, int width, int length)
	{
		ItemClass @class = this.m_info.m_class;
		int num;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			if (@class.m_level == ItemClass.Level.Level1)
			{
				num = 100;
			}
			else if (@class.m_level == ItemClass.Level.Level2)
			{
				num = 140;
			}
			else
			{
				num = 160;
			}
		}
		else
		{
			num = 100;
		}
		if (num != 0)
		{
			num = Mathf.Max(100, width * length * num + r.Int32(100u)) / 100;
		}
		return num;
	}

	public override void GetConsumptionRates(Randomizer r, int productionRate, out int electricityConsumption, out int waterConsumption, out int sewageAccumulation, out int garbageAccumulation, out int incomeAccumulation)
	{
		ItemClass @class = this.m_info.m_class;
		electricityConsumption = 0;
		waterConsumption = 0;
		sewageAccumulation = 0;
		garbageAccumulation = 0;
		incomeAccumulation = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			ItemClass.Level level = @class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						electricityConsumption = 250;
						waterConsumption = 160;
						sewageAccumulation = 160;
						garbageAccumulation = 100;
						incomeAccumulation = 240;
					}
				}
				else
				{
					electricityConsumption = 200;
					waterConsumption = 130;
					sewageAccumulation = 130;
					garbageAccumulation = 150;
					incomeAccumulation = 200;
				}
			}
			else
			{
				electricityConsumption = 150;
				waterConsumption = 100;
				sewageAccumulation = 100;
				garbageAccumulation = 200;
				incomeAccumulation = 160;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			electricityConsumption = 300;
			waterConsumption = 250;
			sewageAccumulation = 250;
			garbageAccumulation = 150;
			incomeAccumulation = 300;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			electricityConsumption = 350;
			waterConsumption = 200;
			sewageAccumulation = 200;
			garbageAccumulation = 200;
			incomeAccumulation = 360;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			electricityConsumption = 90;
			waterConsumption = 60;
			sewageAccumulation = 60;
			garbageAccumulation = 100;
			incomeAccumulation = 140;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			electricityConsumption = 110;
			waterConsumption = 350;
			sewageAccumulation = 350;
			garbageAccumulation = 150;
			incomeAccumulation = 180;
		}
		if (electricityConsumption != 0)
		{
			electricityConsumption = Mathf.Max(100, productionRate * electricityConsumption + r.Int32(100u)) / 100;
		}
		if (waterConsumption != 0)
		{
			int num = r.Int32(100u);
			waterConsumption = Mathf.Max(100, productionRate * waterConsumption + num) / 100;
			if (sewageAccumulation != 0)
			{
				sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + num) / 100;
			}
		}
		else if (sewageAccumulation != 0)
		{
			sewageAccumulation = Mathf.Max(100, productionRate * sewageAccumulation + r.Int32(100u)) / 100;
		}
		if (garbageAccumulation != 0)
		{
			garbageAccumulation = Mathf.Max(100, productionRate * garbageAccumulation + r.Int32(100u)) / 100;
		}
		if (incomeAccumulation != 0)
		{
			incomeAccumulation = productionRate * incomeAccumulation;
		}
	}

	public override void GetPollutionRates(int productionRate, DistrictPolicies.CityPlanning cityPlanningPolicies, out int groundPollution, out int noisePollution)
	{
		ItemClass @class = this.m_info.m_class;
		groundPollution = 0;
		noisePollution = 0;
		if (@class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			ItemClass.Level level = @class.m_level;
			if (level != ItemClass.Level.Level1)
			{
				if (level != ItemClass.Level.Level2)
				{
					if (level == ItemClass.Level.Level3)
					{
						groundPollution = 150;
						noisePollution = 150;
					}
				}
				else
				{
					groundPollution = 200;
					noisePollution = 200;
				}
			}
			else
			{
				groundPollution = 300;
				noisePollution = 300;
			}
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOre)
		{
			groundPollution = 400;
			noisePollution = 500;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialOil)
		{
			groundPollution = 500;
			noisePollution = 400;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialForestry)
		{
			groundPollution = 0;
			noisePollution = 200;
		}
		else if (@class.m_subService == ItemClass.SubService.IndustrialFarming)
		{
			groundPollution = 0;
			noisePollution = 200;
		}
		if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.FilterIndustrialWaste) != DistrictPolicies.CityPlanning.None)
		{
			groundPollution = groundPollution + 1 >> 1;
		}
		groundPollution = (productionRate * groundPollution + 99) / 100;
		noisePollution = (productionRate * noisePollution + 99) / 100;
	}

	public static IndustrialBuildingAI.ProductType GetProductType(TransferManager.TransferReason resource)
	{
		switch (resource)
		{
		case TransferManager.TransferReason.Oil:
			return IndustrialBuildingAI.ProductType.Oil;
		case TransferManager.TransferReason.Ore:
		case TransferManager.TransferReason.Coal:
			return IndustrialBuildingAI.ProductType.Ore;
		case TransferManager.TransferReason.Logs:
			return IndustrialBuildingAI.ProductType.Forestry;
		case TransferManager.TransferReason.Grain:
			return IndustrialBuildingAI.ProductType.Agricultural;
		case TransferManager.TransferReason.Goods:
			return IndustrialBuildingAI.ProductType.Goods;
		case TransferManager.TransferReason.PassengerTrain:
			IL_25:
			if (resource == TransferManager.TransferReason.Petrol)
			{
				return IndustrialBuildingAI.ProductType.Oil;
			}
			if (resource == TransferManager.TransferReason.Food)
			{
				return IndustrialBuildingAI.ProductType.Agricultural;
			}
			if (resource != TransferManager.TransferReason.Lumber)
			{
				return IndustrialBuildingAI.ProductType.None;
			}
			return IndustrialBuildingAI.ProductType.Forestry;
		}
		goto IL_25;
	}

	public override string GenerateName(ushort buildingID, InstanceID caller)
	{
		if (this.m_info.m_prefabDataIndex == -1)
		{
			return null;
		}
		Randomizer randomizer;
		randomizer..ctor((int)buildingID);
		string text = PrefabCollection<BuildingInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
		uint num = Locale.CountUnchecked("BUILDING_NAME", text);
		if (num != 0u)
		{
			return Locale.Get("BUILDING_NAME", text, randomizer.Int32(num));
		}
		if (this.m_info.m_class.m_subService == ItemClass.SubService.IndustrialGeneric)
		{
			text = this.m_info.m_class.m_level.ToString();
			num = Locale.Count("INDUSTRIAL_NAME", text);
			return Locale.Get("INDUSTRIAL_NAME", text, randomizer.Int32(num));
		}
		text = this.m_info.m_class.m_subService.ToString();
		num = Locale.Count("INDUSTRIAL_NAME", text);
		return Locale.Get("INDUSTRIAL_NAME", text, randomizer.Int32(num));
	}

	public enum ProductType
	{
		None = -1,
		Goods,
		Ore,
		Oil,
		Forestry,
		Agricultural,
		Count
	}
}
