﻿using System;
using UnityEngine;

public class ImportAssetTree : ImportAsset
{
	public ImportAssetTree(GameObject template, PreviewCamera camera) : base(template, camera)
	{
	}

	protected override AssetImporterTextureLoader.ResultType[] textureTypes
	{
		get
		{
			return new AssetImporterTextureLoader.ResultType[]
			{
				AssetImporterTextureLoader.ResultType.RGB,
				AssetImporterTextureLoader.ResultType.XYCA
			};
		}
	}

	protected override void InitializeObject()
	{
		TreeInfo component = this.m_Object.GetComponent<TreeInfo>();
		component.InitializePrefab();
		component.CheckReferences();
		component.m_prefabInitialized = true;
		this.m_Object.set_layer(LayerMask.NameToLayer("Trees"));
	}

	protected override void CreateInfo()
	{
		TreeInfo component = this.m_TemplateObject.GetComponent<TreeInfo>();
		TreeInfo treeInfo = ImportAsset.CopyComponent<TreeInfo>(component, this.m_Object);
		treeInfo.m_generatedInfo = ScriptableObject.CreateInstance<TreeInfoGen>();
		treeInfo.m_generatedInfo.set_name(this.m_Object.get_name() + " (GeneratedInfo)");
		treeInfo.m_Atlas = null;
		treeInfo.m_Thumbnail = string.Empty;
		treeInfo.CalculateGeneratedInfo();
	}

	protected override void CompressTextures()
	{
		Material material = this.m_Object.GetComponent<Renderer>().get_material();
		this.m_Tasks[0][0] = AssetImporterTextureLoader.CompressTexture(material, "_MainTex", false);
		this.m_Tasks[0][1] = AssetImporterTextureLoader.CompressTexture(material, "_XYCAMap", true);
		this.m_Tasks[0][2] = null;
	}
}
