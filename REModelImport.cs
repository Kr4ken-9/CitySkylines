﻿using System;
using System.Reflection;
using System.Threading;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class REModelImport : UICustomControl
{
	public event REModelImport.ModelChangedHandler EventModelChanged
	{
		add
		{
			REModelImport.ModelChangedHandler modelChangedHandler = this.EventModelChanged;
			REModelImport.ModelChangedHandler modelChangedHandler2;
			do
			{
				modelChangedHandler2 = modelChangedHandler;
				modelChangedHandler = Interlocked.CompareExchange<REModelImport.ModelChangedHandler>(ref this.EventModelChanged, (REModelImport.ModelChangedHandler)Delegate.Combine(modelChangedHandler2, value), modelChangedHandler);
			}
			while (modelChangedHandler != modelChangedHandler2);
		}
		remove
		{
			REModelImport.ModelChangedHandler modelChangedHandler = this.EventModelChanged;
			REModelImport.ModelChangedHandler modelChangedHandler2;
			do
			{
				modelChangedHandler2 = modelChangedHandler;
				modelChangedHandler = Interlocked.CompareExchange<REModelImport.ModelChangedHandler>(ref this.EventModelChanged, (REModelImport.ModelChangedHandler)Delegate.Remove(modelChangedHandler2, value), modelChangedHandler);
			}
			while (modelChangedHandler != modelChangedHandler2);
		}
	}

	private void OnEnable()
	{
		if (this.m_ImportButton != null)
		{
			this.m_ImportButton.add_eventClick(new MouseEventHandler(this.ImportModel));
		}
		if (this.m_Color != null)
		{
			this.m_Color.add_eventSelectedColorReleased(new PropertyChangedEventHandler<Color>(this.OnColor));
		}
		if (this.m_DeleteButton != null)
		{
			this.m_DeleteButton.add_eventClick(new MouseEventHandler(this.OnDeleteModel));
		}
	}

	private void OnDisable()
	{
		if (this.m_ImportButton != null)
		{
			this.m_ImportButton.remove_eventClick(new MouseEventHandler(this.ImportModel));
		}
		if (this.m_Color != null)
		{
			this.m_Color.remove_eventSelectedColorReleased(new PropertyChangedEventHandler<Color>(this.OnColor));
		}
		if (this.m_DeleteButton != null)
		{
			this.m_DeleteButton.remove_eventClick(new MouseEventHandler(this.OnDeleteModel));
		}
	}

	private void OnDeleteModel(UIComponent comp, UIMouseEventParameter param)
	{
		this.OnModelImported(new Mesh(), new Material(this.GetShader()), new Mesh(), new Material(this.GetShader()));
	}

	private void OnColor(UIComponent component, Color value)
	{
		Material material = AssetEditorRoadUtils.GetMaterial(this.m_Target);
		if (material != null && material.HasProperty(REModelImport.kColorProperty))
		{
			material.SetColor(REModelImport.kColorProperty, value);
		}
		if (this.EventModelChanged != null)
		{
			this.EventModelChanged();
		}
	}

	public void Initialize(string label, object target, bool showColorField)
	{
		if (this.m_ImportLabel != null)
		{
			this.m_ImportLabel.set_text(label);
		}
		this.m_Target = target;
		if (!showColorField)
		{
			UIPanel uIPanel = base.Find<UIPanel>("ColorPanel");
			if (uIPanel != null)
			{
				uIPanel.Hide();
			}
		}
		this.InitShaderMenu();
		this.RefreshButtonText();
		this.RefreshColorPicker();
	}

	private void RefreshColorPicker()
	{
		Material material = AssetEditorRoadUtils.GetMaterial(this.m_Target);
		if (material != null && material.HasProperty(REModelImport.kColorProperty))
		{
			this.m_Color.set_selectedColor(material.GetColor(REModelImport.kColorProperty));
		}
	}

	private static string UserToInternalShader(string shader)
	{
		if (shader == "Basic")
		{
			return REModelImport.RoadShader;
		}
		if (shader == "Bridge")
		{
			return REModelImport.RoadBridgeShader;
		}
		if (shader == "Rail")
		{
			return REModelImport.TrainBridgeShader;
		}
		if (shader == "Wire")
		{
			return REModelImport.ElectricityShader;
		}
		return REModelImport.RoadShader;
	}

	private static string InternalToUserShader(string shader)
	{
		if (shader == REModelImport.RoadShader)
		{
			return "Basic";
		}
		if (shader == REModelImport.RoadBridgeShader)
		{
			return "Bridge";
		}
		if (shader == REModelImport.TrainShader || shader == REModelImport.TrainBridgeShader)
		{
			return "Rail";
		}
		if (shader == REModelImport.ElectricityShader)
		{
			return "Wire";
		}
		return "Basic";
	}

	private void InitShaderMenu()
	{
		this.m_ShaderDropDown.set_items(new string[]
		{
			"Basic",
			"Bridge",
			"Rail",
			"Wire"
		});
		string text = REModelImport.InternalToUserShader(AssetEditorRoadUtils.GetShaderName(this.m_Target));
		((UIButton)this.m_ShaderDropDown.get_triggerButton()).set_text(text);
		this.m_ShaderDropDown.set_selectedValue(text);
		this.m_ShaderDropDown.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnShaderSelectionChanged));
	}

	private void OnShaderSelectionChanged(UIComponent comp, int value)
	{
		string text = this.m_ShaderDropDown.get_items()[value];
		((UIButton)((UIDropDown)comp).get_triggerButton()).set_text(text);
		this.RefreshShader();
		if (this.EventModelChanged != null)
		{
			this.EventModelChanged();
		}
	}

	private void RefreshButtonText()
	{
		if (this.m_ImportButton != null)
		{
			this.m_ImportButton.set_text(AssetEditorRoadUtils.GetMeshName(this.m_Target));
		}
	}

	public void ImportModel(UIComponent c = null, UIMouseEventParameter e = null)
	{
		UIComponent uIComponent = UIView.Find("ModelImportPanel");
		AssetImporterAssetImport assetImporterAssetImport = (!(uIComponent != null)) ? null : uIComponent.GetComponent<AssetImporterAssetImport>();
		if (assetImporterAssetImport != null)
		{
			assetImporterAssetImport.ImportModel(this.GetShader(), new AssetImporterAssetImport.ModelImportCallbackHandler(this.OnModelImported));
		}
	}

	private void OnModelImported(Mesh mesh, Material material, Mesh lodMesh, Material lodMaterial)
	{
		if (this.m_Target is NetInfo.Segment)
		{
			AssetEditorRoadUtils.ReleaseModel(this.m_Target as NetInfo.Segment);
		}
		else if (this.m_Target is NetInfo.Node)
		{
			AssetEditorRoadUtils.ReleaseModel(this.m_Target as NetInfo.Node);
		}
		this.SetField(AssetEditorRoadUtils.kMeshField, mesh);
		this.SetField(AssetEditorRoadUtils.kMaterialField, material);
		this.SetField(AssetEditorRoadUtils.kLodMeshField, lodMesh);
		this.SetField(AssetEditorRoadUtils.kLodMaterialField, lodMaterial);
		this.RefreshButtonText();
		this.RefreshColorPicker();
		if (this.EventModelChanged != null)
		{
			this.EventModelChanged();
		}
	}

	private void RefreshShader()
	{
		Shader shader = this.GetShader();
		if (shader != null)
		{
			Type type = this.m_Target.GetType();
			FieldInfo field = type.GetField(AssetEditorRoadUtils.kMaterialField, BindingFlags.Instance | BindingFlags.Public);
			object value = field.GetValue(this.m_Target);
			Material material = (value == null) ? null : ((Material)value);
			if (material != null && material.get_shader() != shader)
			{
				field.SetValue(this.m_Target, this.CopyMaterialWithShader(material, shader));
			}
			FieldInfo field2 = type.GetField(AssetEditorRoadUtils.kLodMaterialField, BindingFlags.Instance | BindingFlags.Public);
			object value2 = field2.GetValue(this.m_Target);
			Material material2 = (value2 == null) ? null : ((Material)value2);
			if (material2 != null)
			{
				field2.SetValue(this.m_Target, this.CopyMaterialWithShader(material2, shader));
			}
		}
	}

	private Material CopyMaterialWithShader(Material original, Shader shader)
	{
		Material material = new Material(shader);
		NetManager instance = Singleton<NetManager>.get_instance();
		Shader shader2 = Shader.Find(REModelImport.ElectricityShader);
		if (original.get_shader() == shader2 || shader == shader2)
		{
			return material;
		}
		if (original.HasProperty(instance.ID_MainTex) && material.HasProperty(instance.ID_MainTex))
		{
			material.SetTexture(instance.ID_MainTex, original.GetTexture(instance.ID_MainTex));
		}
		if (original.HasProperty(instance.ID_XYSMap) && material.HasProperty(instance.ID_XYSMap))
		{
			material.SetTexture(instance.ID_XYSMap, original.GetTexture(instance.ID_XYSMap));
		}
		if (original.HasProperty(instance.ID_APRMap) && material.HasProperty(instance.ID_APRMap))
		{
			material.SetTexture(instance.ID_APRMap, original.GetTexture(instance.ID_APRMap));
		}
		return material;
	}

	private void SetField(string fieldName, object value)
	{
		if (fieldName != null && value != null && this.m_Target != null)
		{
			Type type = this.m_Target.GetType();
			FieldInfo field = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.Public);
			if (field != null && field.FieldType == value.GetType())
			{
				field.SetValue(this.m_Target, value);
			}
		}
	}

	private Shader GetShader()
	{
		return Shader.Find(REModelImport.UserToInternalShader(this.m_ShaderDropDown.get_selectedValue()));
	}

	public UILabel m_ImportLabel;

	public UIButton m_ImportButton;

	public UILabel m_ShaderLabel;

	public UIButton m_DeleteButton;

	public UIDropDown m_ShaderDropDown;

	public UIColorField m_Color;

	public static readonly string kColorProperty = "_Color";

	public object m_Target;

	private static readonly string RoadShader = "Custom/Net/Road";

	private static readonly string RoadBridgeShader = "Custom/Net/RoadBridge";

	private static readonly string TrainShader = "Custom/Net/Train";

	private static readonly string TrainBridgeShader = "Custom/Net/TrainBridge";

	private static readonly string ElectricityShader = "Custom/Net/Electricity";

	public delegate void ModelChangedHandler();
}
