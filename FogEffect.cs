﻿using System;
using ColossalFramework;
using UnityEngine;

[ExecuteInEditMode]
public class FogEffect : MonoBehaviour
{
	private void OnEnable()
	{
		this.m_fogMaterial = new Material(this.m_fogShader);
		this.m_fogMaterial.set_hideFlags(52);
		this.ID_FrustumCornersWS = Shader.PropertyToID("_FrustumCornersWS");
		this.ID_CameraWS = Shader.PropertyToID("_CameraWS");
		this.ID_FogParams = Shader.PropertyToID("_FogParams");
		this.ID_3DNoiseTex = Shader.PropertyToID("_3DNoiseTex");
		this.ID_3DFogColor = Shader.PropertyToID("_3DFogColor");
		this.ID_Speed = Shader.PropertyToID("_Speed");
		this.ID_NoiseGainConstrastStepScale = Shader.PropertyToID("_NoiseGainConstrastStepScale");
		this.ID_PollutionColor = Shader.PropertyToID("_PollutionColor");
		this.ID_PollutionParams = Shader.PropertyToID("_PollutionParams");
		this.ID_InscatteringColor = Shader.PropertyToID("_InscatteringColor");
		this.ID_InscatteringParams = Shader.PropertyToID("_InscatteringParams");
		this.ID_SunDirection = Shader.PropertyToID("_SunDirection");
	}

	private void OnDisable()
	{
		if (this.m_fogMaterial)
		{
			Object.DestroyImmediate(this.m_fogMaterial);
			this.m_fogMaterial = null;
		}
	}

	private void ToggleVolumeFogPermutation(bool enable)
	{
		if (enable)
		{
			this.m_fogMaterial.EnableKeyword("USE_VOLUME_FOG");
		}
		else
		{
			this.m_fogMaterial.DisableKeyword("USE_VOLUME_FOG");
		}
	}

	[ImageEffectOpaque]
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Camera component = base.GetComponent<Camera>();
		Transform transform = base.get_transform();
		Matrix4x4 identity = Matrix4x4.get_identity();
		Color color;
		Color inscatteringColor;
		Vector4 vector;
		Transform sun;
		Vector4 vector2;
		Color color2;
		Vector4 vector3;
		Vector4 vector4;
		float edgeFogDistance;
		if (Singleton<RenderManager>.get_exists())
		{
			RenderProperties properties = Singleton<RenderManager>.get_instance().m_properties;
			if (properties == null)
			{
				return;
			}
			this.ToggleVolumeFogPermutation(properties.m_useVolumeFog);
			color = properties.m_volumeFogColor;
			inscatteringColor = properties.m_inscatteringColor;
			vector.x = properties.m_inscatteringExponent;
			vector.y = properties.m_inscatteringIntensity;
			vector.z = properties.m_inscatteringStartDistance;
			vector.w = 1f / properties.m_inscatteringTransitionSoftness;
			sun = properties.m_sun;
			vector2.x = ((!this.m_edgeFog) ? 0f : 1f);
			vector2.y = ((!this.m_edgeFog) ? this.m_3DFogAmount : properties.m_volumeFogDensity);
			vector2.z = 1f / properties.m_volumeFogStart;
			vector2.w = properties.m_volumeFogDistance;
			color2 = properties.m_pollutionFogColor;
			vector3.x = properties.m_pollutionFogIntensityWater;
			vector3.y = properties.m_pollutionFogIntensity;
			vector3.z = 0f;
			vector3.w = 0f;
			vector4 = transform.get_position();
			vector4.w = properties.m_fogHeight;
			edgeFogDistance = properties.m_edgeFogDistance;
		}
		else
		{
			this.ToggleVolumeFogPermutation(this.m_UseVolumeFog);
			color = this.m_3DFogColor;
			inscatteringColor = this.m_InscatteringColor;
			vector.x = this.m_InscateringExponent;
			vector.y = this.m_InscatteringIntensity;
			vector.z = this.m_InscatteringStartDistance;
			vector.w = 1f / this.m_InscatteringTransitionSoftness;
			vector2.x = ((!this.m_edgeFog) ? 0f : 1f);
			vector2.y = this.m_3DFogAmount;
			vector2.z = 1f / this.m_3DFogStart;
			vector2.w = this.m_3DFogDistance;
			sun = this.m_Sun;
			color2 = this.m_PollutionColor;
			vector3.x = this.m_PollutionIntensityWater;
			vector3.y = this.m_PollutionIntensity;
			vector3.z = 0f;
			vector3.w = 0f;
			vector4 = transform.get_position();
			vector4.w = this.m_FogHeight;
			edgeFogDistance = this.m_edgeFogDistance;
		}
		vector.x = Mathf.Max(0.01f, vector.x);
		float num = component.get_fieldOfView() * 0.5f;
		Vector3 vector5 = transform.get_right() * component.get_nearClipPlane() * Mathf.Tan(num * 0.0174532924f) * component.get_aspect();
		Vector3 vector6 = transform.get_up() * component.get_nearClipPlane() * Mathf.Tan(num * 0.0174532924f);
		Vector3 vector7 = transform.get_forward() * component.get_nearClipPlane() - vector5 + vector6;
		float num2 = vector7.get_magnitude() * component.get_farClipPlane() / component.get_nearClipPlane();
		vector7.Normalize();
		vector7 *= num2;
		Vector3 vector8 = transform.get_forward() * component.get_nearClipPlane() + vector5 + vector6;
		vector8.Normalize();
		vector8 *= num2;
		Vector3 vector9 = transform.get_forward() * component.get_nearClipPlane() + vector5 - vector6;
		vector9.Normalize();
		vector9 *= num2;
		Vector3 vector10 = transform.get_forward() * component.get_nearClipPlane() - vector5 - vector6;
		vector10.Normalize();
		vector10 *= num2;
		identity.SetRow(0, vector7);
		identity.SetRow(1, vector8);
		identity.SetRow(2, vector9);
		identity.SetRow(3, vector10);
		this.m_fogMaterial.SetMatrix(this.ID_FrustumCornersWS, identity);
		this.m_fogMaterial.SetVector(this.ID_CameraWS, vector4);
		this.m_fogMaterial.SetVector(this.ID_FogParams, vector2);
		this.m_fogMaterial.SetColor(this.ID_3DFogColor, color);
		this.m_fogMaterial.SetColor(this.ID_PollutionColor, color2);
		this.m_fogMaterial.SetVector(this.ID_PollutionParams, vector3);
		this.m_fogMaterial.SetColor(this.ID_InscatteringColor, inscatteringColor);
		this.m_fogMaterial.SetVector(this.ID_InscatteringParams, vector);
		this.m_fogMaterial.SetVector(this.ID_SunDirection, (!(sun != null)) ? Vector3.get_forward() : (-sun.get_forward()));
		this.m_fogMaterial.SetTexture(this.ID_3DNoiseTex, this.m_3DNoiseTexture);
		Vector4 vector11 = Vector4.get_zero();
		if (Singleton<WeatherManager>.get_exists() && Singleton<SimulationManager>.get_exists())
		{
			float num3 = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta * 60f;
			float num4 = Singleton<WeatherManager>.get_instance().m_windDirection * 0.0174532924f;
			vector11.x = Mathf.Sin(num4);
			vector11.y = this.m_FogPatternChangeSpeed;
			vector11.z = Mathf.Cos(num4);
			vector11.w = 0f;
			vector11 *= this.m_WindSpeed;
			this.m_SpeedOffset += vector11 * num3;
		}
		this.m_SpeedOffset.w = edgeFogDistance;
		this.m_fogMaterial.SetVector(this.ID_Speed, this.m_SpeedOffset);
		this.m_fogMaterial.SetVector(this.ID_NoiseGainConstrastStepScale, new Vector4(this.m_NoiseGain, this.m_NoiseContrast, this.m_3DNoiseStepSize, this.m_3DNoiseScale));
		Graphics.Blit(source, destination, this.m_fogMaterial);
	}

	public Shader m_fogShader;

	public bool m_edgeFog = true;

	[Range(0f, 3800f)]
	public float m_edgeFogDistance;

	public float m_FogHeight = 5000f;

	public bool m_UseVolumeFog;

	public Texture3D m_3DNoiseTexture;

	public float m_NoiseGain = 1f;

	public float m_NoiseContrast;

	public Color m_3DFogColor = Color.get_white();

	public float m_3DFogAmount = 1f;

	public float m_3DFogStart;

	public float m_3DFogDistance = 10f;

	public float m_3DNoiseStepSize = 4f;

	public float m_3DNoiseScale = 0.01f;

	public Color m_PollutionColor = Color.get_white();

	public float m_PollutionIntensityWater = 2f;

	public float m_PollutionIntensity = 1f;

	public float m_WindSpeed;

	public float m_FogPatternChangeSpeed = 0.5f;

	public Transform m_Sun;

	public Color m_InscatteringColor = Color.get_white();

	public float m_InscateringExponent = 15f;

	public float m_InscatteringIntensity = 2f;

	public float m_InscatteringStartDistance = 400f;

	public float m_InscatteringTransitionSoftness = 1f;

	private Material m_fogMaterial;

	private int ID_FrustumCornersWS;

	private int ID_CameraWS;

	private int ID_FogParams;

	private int ID_3DNoiseTex;

	private int ID_3DFogColor;

	private int ID_Speed;

	private int ID_NoiseGainConstrastStepScale;

	private int ID_InscatteringColor;

	private int ID_InscatteringParams;

	private int ID_SunDirection;

	private int ID_PollutionColor;

	private int ID_PollutionParams;

	private Vector4 m_SpeedOffset;
}
