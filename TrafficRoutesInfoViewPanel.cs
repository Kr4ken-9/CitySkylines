﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class TrafficRoutesInfoViewPanel : InfoViewPanel
{
	public bool showPedestrians
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showPedestrians;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showPedestrians = value;
		}
	}

	public bool showCyclists
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showCyclists;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showCyclists = value;
		}
	}

	public bool showPrivateVehicles
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showPrivateVehicles;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showPrivateVehicles = value;
		}
	}

	public bool showPublicTransport
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showPublicTransport;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showPublicTransport = value;
		}
	}

	public bool showTrucks
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showTrucks;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showTrucks = value;
		}
	}

	public bool showCityServiceVehicles
	{
		get
		{
			return Singleton<NetManager>.get_instance().PathVisualizer.showCityServiceVehicles;
		}
		set
		{
			Singleton<NetManager>.get_instance().PathVisualizer.showCityServiceVehicles = value;
		}
	}

	protected override void Start()
	{
		base.Start();
		this.showPedestrians = true;
		this.showCyclists = true;
		this.showPrivateVehicles = true;
		this.showPublicTransport = true;
		this.showCityServiceVehicles = true;
		this.showTrucks = true;
		this.m_Tabstrip.add_eventSelectedIndexChanged(delegate(UIComponent sender, int id)
		{
			if (Singleton<InfoManager>.get_exists())
			{
				if (id != (int)Singleton<InfoManager>.get_instance().NextSubMode)
				{
					ToolBase.OverrideInfoMode = true;
				}
				Singleton<InfoManager>.get_instance().SetCurrentMode(Singleton<InfoManager>.get_instance().NextMode, (InfoManager.SubInfoMode)id);
			}
		});
		TrafficRoutesInfoViewPanel.m_instance = this;
	}

	protected override void UpdatePanel()
	{
		if (Singleton<InfoManager>.get_exists())
		{
			this.m_Tabstrip.set_selectedIndex((int)Singleton<InfoManager>.get_instance().NextSubMode);
			if (this.m_isAssetEditor && this.m_Tabstrip.get_selectedIndex() != 1)
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.TrafficRoutes, InfoManager.SubInfoMode.WaterPower);
			}
			if (!this.m_initialized)
			{
				base.Find<UICheckBox>("CheckboxPedestrians").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[0]);
				base.Find<UICheckBox>("CheckboxCyclists").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[1]);
				base.Find<UICheckBox>("CheckboxPirateVehicles").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[2]);
				base.Find<UICheckBox>("CheckboxPublicTransport").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[3]);
				base.Find<UICheckBox>("CheckboxTrucks").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[4]);
				base.Find<UICheckBox>("CheckboxCityServiceVehicles").Find<UISprite>("Color").set_color(Singleton<InfoManager>.get_instance().m_properties.m_routeColors[5]);
				base.Find<UICheckBox>("CheckboxCyclists").set_isVisible(SteamHelper.IsDLCOwned(SteamHelper.DLC.AfterDarkDLC));
				this.m_initialized = true;
			}
		}
	}

	public static TrafficRoutesInfoViewPanel instance
	{
		get
		{
			return TrafficRoutesInfoViewPanel.m_instance;
		}
	}

	private void OnEnable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	private void OnDisable()
	{
		Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
	}

	protected override void OnLevelLoaded(SimulationManager.UpdateMode updateMode)
	{
		base.OnLevelLoaded(updateMode);
		this.m_isAssetEditor = (updateMode == SimulationManager.UpdateMode.NewAsset || updateMode == SimulationManager.UpdateMode.LoadAsset);
		base.Find<UIButton>("Routes").set_isEnabled(!this.m_isAssetEditor);
		base.Find<UIButton>("AdjustRoadFeature").set_isEnabled(!this.m_isAssetEditor);
	}

	private bool m_initialized;

	private bool m_isAssetEditor;

	private static TrafficRoutesInfoViewPanel m_instance;
}
