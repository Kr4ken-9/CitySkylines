﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public class AsyncAction : AsyncTaskBase
{
	public AsyncAction(Action action, string taskName = null) : base(taskName)
	{
		this.m_Action = action;
	}

	public override void Execute()
	{
		try
		{
			this.m_Progress = 0f;
			this.m_Action();
			this.m_Progress = 1f;
		}
		catch (Exception ex)
		{
			this.m_Progress = 100f;
			UIView.ForwardException(ex);
			CODebugBase<LogChannel>.Error(LogChannel.Core, "Execution error: " + ex.Message + "\n" + ex.StackTrace);
		}
	}

	private Action m_Action;
}
