﻿using System;
using ColossalFramework.Globalization;

public class AnimalAI : CitizenAI
{
	public override bool IsAnimal()
	{
		return true;
	}

	public override string GenerateName(ushort instanceID, bool useCitizen)
	{
		if (this.m_info.m_prefabDataIndex != -1)
		{
			string text = PrefabCollection<CitizenInfo>.PrefabName((uint)this.m_info.m_prefabDataIndex);
			return Locale.Get("ANIMAL_TITLE", text);
		}
		return null;
	}
}
