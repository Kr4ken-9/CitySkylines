﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using UnityEngine;

public class EffectCollection : MonoBehaviour
{
	private void Awake()
	{
		EffectCollection.InitializeEffects(this.m_effects);
	}

	private void OnDestroy()
	{
		EffectCollection.DestroyEffects(this.m_effects);
	}

	public static EffectInfo FindEffect(string name)
	{
		EffectInfo result;
		if (EffectCollection.m_dict.TryGetValue(name, out result))
		{
			return result;
		}
		CODebugBase<LogChannel>.Warn(LogChannel.Serialization, "Unknown effect: " + name);
		return null;
	}

	public static void InitializeEffects(EffectInfo[] effects)
	{
		for (int i = 0; i < effects.Length; i++)
		{
			EffectInfo effectInfo = effects[i];
			if (EffectCollection.m_dict.ContainsKey(effectInfo.get_name()))
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Duplicate effect name: " + effectInfo.get_name());
			}
			else
			{
				EffectCollection.m_dict.Add(effectInfo.get_name(), effectInfo);
			}
		}
	}

	public static void DestroyEffects(EffectInfo[] effects)
	{
		for (int i = 0; i < effects.Length; i++)
		{
			EffectInfo effectInfo = effects[i];
			EffectCollection.m_dict.Remove(effectInfo.get_name());
		}
	}

	public EffectInfo[] m_effects;

	private static Dictionary<string, EffectInfo> m_dict = new Dictionary<string, EffectInfo>();
}
