﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public static class BuildingDecoration
{
	public static void LoadDecorations(BuildingInfo source)
	{
		Building building = default(Building);
		building.m_position = new Vector3(0f, 60f, 0f);
		building.Width = source.m_cellWidth;
		building.Length = source.m_cellLength;
		BuildingDecoration.LoadPaths(source, 0, ref building, 0f);
		BuildingDecoration.LoadProps(source, 0, ref building);
	}

	public static void SaveDecorations(BuildingInfo target)
	{
		Building building = default(Building);
		building.m_position = new Vector3(0f, 60f, 0f);
		building.Width = target.m_cellWidth;
		building.Length = target.m_cellLength;
		BuildingDecoration.SavePaths(target, 0, ref building);
		BuildingDecoration.SaveProps(target, 0, ref building);
	}

	public static void ClearDecorations()
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 1; i < 36864; i++)
		{
			if (instance.m_segments.m_buffer[i].m_flags != NetSegment.Flags.None)
			{
				instance.ReleaseSegment((ushort)i, true);
			}
		}
		for (int j = 1; j < 32768; j++)
		{
			if (instance.m_nodes.m_buffer[j].m_flags != NetNode.Flags.None)
			{
				instance.ReleaseNode((ushort)j);
			}
		}
		PropManager instance2 = Singleton<PropManager>.get_instance();
		for (int k = 1; k < 65536; k++)
		{
			if (instance2.m_props.m_buffer[k].m_flags != 0)
			{
				instance2.ReleaseProp((ushort)k);
			}
		}
		TreeManager instance3 = Singleton<TreeManager>.get_instance();
		for (int l = 1; l < 262144; l++)
		{
			if (instance3.m_trees.m_buffer[l].m_flags != 0)
			{
				instance3.ReleaseTree((uint)l);
			}
		}
	}

	public static void LoadProps(BuildingInfo info, ushort buildingID, ref Building data)
	{
		Vector3 position = data.m_position;
		Quaternion quaternion = Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down());
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, quaternion, Vector3.get_one());
		PropManager instance = Singleton<PropManager>.get_instance();
		TreeManager instance2 = Singleton<TreeManager>.get_instance();
		if (info.m_props != null)
		{
			for (int i = 0; i < info.m_props.Length; i++)
			{
				PropInfo finalProp = info.m_props[i].m_finalProp;
				TreeInfo finalTree = info.m_props[i].m_finalTree;
				if (finalProp != null)
				{
					if (finalProp.m_prefabDataIndex != -1)
					{
						Vector3 vector = matrix4x.MultiplyPoint(info.m_props[i].m_position);
						float angle = data.m_angle + 0.0174532924f * info.m_props[i].m_angle;
						if (info.m_props[i].m_fixedHeight || !Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(vector)))
						{
							ushort num;
							if (instance.CreateProp(out num, ref Singleton<SimulationManager>.get_instance().m_randomizer, finalProp, vector, angle, true))
							{
								instance.m_props.m_buffer[(int)num].FixedHeight = info.m_props[i].m_fixedHeight;
							}
						}
					}
				}
				else if (finalTree != null && finalTree.m_prefabDataIndex != -1)
				{
					Vector3 vector2 = matrix4x.MultiplyPoint(info.m_props[i].m_position);
					if (info.m_props[i].m_fixedHeight || !Singleton<TerrainManager>.get_instance().HasWater(VectorUtils.XZ(vector2)))
					{
						uint num2;
						if (instance2.CreateTree(out num2, ref Singleton<SimulationManager>.get_instance().m_randomizer, finalTree, vector2, true))
						{
							instance2.m_trees.m_buffer[(int)((UIntPtr)num2)].FixedHeight = info.m_props[i].m_fixedHeight;
						}
					}
				}
			}
		}
	}

	public static void SaveProps(BuildingInfo info, ushort buildingID, ref Building data)
	{
		FastList<BuildingInfo.Prop> fastList = new FastList<BuildingInfo.Prop>();
		Vector3 position = data.m_position;
		Quaternion quaternion = Quaternion.AngleAxis(data.m_angle * 57.29578f, Vector3.get_down());
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(position, quaternion, Vector3.get_one());
		matrix4x = matrix4x.get_inverse();
		PropManager instance = Singleton<PropManager>.get_instance();
		for (int i = 0; i < 65536; i++)
		{
			if ((instance.m_props.m_buffer[i].m_flags & 67) == 1)
			{
				BuildingInfo.Prop prop = new BuildingInfo.Prop();
				prop.m_prop = instance.m_props.m_buffer[i].Info;
				prop.m_finalProp = prop.m_prop;
				prop.m_position = matrix4x.MultiplyPoint(instance.m_props.m_buffer[i].Position);
				prop.m_radAngle = instance.m_props.m_buffer[i].Angle - data.m_angle;
				prop.m_angle = 57.29578f * prop.m_radAngle;
				prop.m_fixedHeight = instance.m_props.m_buffer[i].FixedHeight;
				prop.m_probability = 100;
				fastList.Add(prop);
			}
		}
		TreeManager instance2 = Singleton<TreeManager>.get_instance();
		for (int j = 0; j < 262144; j++)
		{
			if ((instance2.m_trees.m_buffer[j].m_flags & 3) == 1 && instance2.m_trees.m_buffer[j].GrowState != 0)
			{
				BuildingInfo.Prop prop2 = new BuildingInfo.Prop();
				prop2.m_tree = instance2.m_trees.m_buffer[j].Info;
				prop2.m_finalTree = prop2.m_tree;
				prop2.m_position = matrix4x.MultiplyPoint(instance2.m_trees.m_buffer[j].Position);
				prop2.m_fixedHeight = instance2.m_trees.m_buffer[j].FixedHeight;
				prop2.m_probability = 100;
				fastList.Add(prop2);
			}
		}
		info.m_props = fastList.ToArray();
	}

	private static bool FindConnectNode(FastList<ushort> buffer, Vector3 pos, NetInfo info2, out NetTool.ControlPoint point)
	{
		point = default(NetTool.ControlPoint);
		NetManager instance = Singleton<NetManager>.get_instance();
		ItemClass.Service service = info2.m_class.m_service;
		ItemClass.SubService subService = info2.m_class.m_subService;
		ItemClass.Layer layer = info2.m_class.m_layer;
		ItemClass.Service service2 = ItemClass.Service.None;
		ItemClass.SubService subService2 = ItemClass.SubService.None;
		ItemClass.Layer layer2 = ItemClass.Layer.Default;
		if (info2.m_intersectClass != null)
		{
			service2 = info2.m_intersectClass.m_service;
			subService2 = info2.m_intersectClass.m_subService;
			layer2 = info2.m_intersectClass.m_layer;
		}
		if (info2.m_netAI.SupportUnderground() || info2.m_netAI.IsUnderground())
		{
			layer |= (ItemClass.Layer.Default | ItemClass.Layer.MetroTunnels);
			layer2 |= (ItemClass.Layer.Default | ItemClass.Layer.MetroTunnels);
		}
		for (int i = 0; i < buffer.m_size; i++)
		{
			ushort num = buffer.m_buffer[i];
			if (Vector3.SqrMagnitude(pos - instance.m_nodes.m_buffer[(int)num].m_position) < 0.001f)
			{
				NetInfo info3 = instance.m_nodes.m_buffer[(int)num].Info;
				ItemClass connectionClass = info3.GetConnectionClass();
				if (((service == ItemClass.Service.None || connectionClass.m_service == service) && (subService == ItemClass.SubService.None || connectionClass.m_subService == subService) && (layer == ItemClass.Layer.None || (connectionClass.m_layer & layer) != ItemClass.Layer.None)) || (info3.m_intersectClass != null && (service == ItemClass.Service.None || info3.m_intersectClass.m_service == service) && (subService == ItemClass.SubService.None || info3.m_intersectClass.m_subService == subService) && (layer == ItemClass.Layer.None || (info3.m_intersectClass.m_layer & layer) != ItemClass.Layer.None)) || (connectionClass.m_service == service2 && (subService2 == ItemClass.SubService.None || connectionClass.m_subService == subService2) && (layer2 == ItemClass.Layer.None || (connectionClass.m_layer & layer2) != ItemClass.Layer.None)))
				{
					point.m_position = instance.m_nodes.m_buffer[(int)num].m_position;
					point.m_direction = Vector3.get_zero();
					point.m_node = num;
					point.m_segment = 0;
					if (info3.m_netAI.IsUnderground())
					{
						point.m_elevation = (float)(-(float)instance.m_nodes.m_buffer[(int)num].m_elevation);
					}
					else
					{
						point.m_elevation = (float)instance.m_nodes.m_buffer[(int)num].m_elevation;
					}
					return true;
				}
			}
		}
		return false;
	}

	private static bool RequireFixedHeight(BuildingInfo buildingInfo, NetInfo info2, Vector3 pos)
	{
		if (info2.m_useFixedHeight)
		{
			return true;
		}
		ItemClass.Service service = info2.m_class.m_service;
		ItemClass.SubService subService = info2.m_class.m_subService;
		ItemClass.Layer layer = info2.m_class.m_layer;
		ItemClass.Service service2 = ItemClass.Service.None;
		ItemClass.SubService subService2 = ItemClass.SubService.None;
		ItemClass.Layer layer2 = ItemClass.Layer.Default;
		if (info2.m_intersectClass != null)
		{
			service2 = info2.m_intersectClass.m_service;
			subService2 = info2.m_intersectClass.m_subService;
			layer2 = info2.m_intersectClass.m_layer;
		}
		if (info2.m_netAI.SupportUnderground() || info2.m_netAI.IsUnderground())
		{
			layer |= (ItemClass.Layer.Default | ItemClass.Layer.MetroTunnels);
			layer2 |= (ItemClass.Layer.Default | ItemClass.Layer.MetroTunnels);
		}
		for (int i = 0; i < buildingInfo.m_paths.Length; i++)
		{
			BuildingInfo.PathInfo pathInfo = buildingInfo.m_paths[i];
			if (pathInfo.m_netInfo != null && pathInfo.m_netInfo.m_useFixedHeight && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
			{
				for (int j = 0; j < pathInfo.m_nodes.Length; j++)
				{
					if (Vector3.SqrMagnitude(pos - pathInfo.m_nodes[j]) < 0.001f)
					{
						NetInfo netInfo = pathInfo.m_netInfo;
						ItemClass connectionClass = netInfo.GetConnectionClass();
						if (((service == ItemClass.Service.None || connectionClass.m_service == service) && (subService == ItemClass.SubService.None || connectionClass.m_subService == subService) && (layer == ItemClass.Layer.None || (connectionClass.m_layer & layer) != ItemClass.Layer.None)) || (netInfo.m_intersectClass != null && (service == ItemClass.Service.None || netInfo.m_intersectClass.m_service == service) && (subService == ItemClass.SubService.None || netInfo.m_intersectClass.m_subService == subService) && (layer == ItemClass.Layer.None || (netInfo.m_intersectClass.m_layer & layer) != ItemClass.Layer.None)) || (netInfo.m_netAI.CanIntersect(info2) && connectionClass.m_service == service2 && (subService2 == ItemClass.SubService.None || connectionClass.m_subService == subService2) && (layer2 == ItemClass.Layer.None || (connectionClass.m_layer & layer2) != ItemClass.Layer.None)))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static void LoadPaths(BuildingInfo info, ushort buildingID, ref Building data, float elevation)
	{
		if (info.m_paths != null)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			instance.m_tempNodeBuffer.Clear();
			instance.m_tempSegmentBuffer.Clear();
			for (int i = 0; i < info.m_paths.Length; i++)
			{
				BuildingInfo.PathInfo pathInfo = info.m_paths[i];
				if (pathInfo.m_netInfo != null && pathInfo.m_nodes != null && pathInfo.m_nodes.Length != 0)
				{
					Vector3 vector = data.CalculatePosition(pathInfo.m_nodes[0]);
					bool flag = BuildingDecoration.RequireFixedHeight(info, pathInfo.m_netInfo, pathInfo.m_nodes[0]);
					if (!flag)
					{
						vector.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, vector, false, pathInfo.m_nodes[0].y + elevation);
					}
					Ray ray;
					ray..ctor(vector + new Vector3(0f, 8f, 0f), Vector3.get_down());
					NetTool.ControlPoint controlPoint;
					if (!BuildingDecoration.FindConnectNode(instance.m_tempNodeBuffer, vector, pathInfo.m_netInfo, out controlPoint))
					{
						if (NetTool.MakeControlPoint(ray, 16f, pathInfo.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo.m_nodes[0].y + elevation - pathInfo.m_netInfo.m_buildHeight, true, out controlPoint))
						{
							Vector3 vector2 = controlPoint.m_position - vector;
							if (!flag)
							{
								vector2.y = 0f;
							}
							float sqrMagnitude = vector2.get_sqrMagnitude();
							if (sqrMagnitude > pathInfo.m_maxSnapDistance * pathInfo.m_maxSnapDistance)
							{
								controlPoint.m_position = vector;
								controlPoint.m_elevation = 0f;
								controlPoint.m_node = 0;
								controlPoint.m_segment = 0;
							}
							else
							{
								controlPoint.m_position.y = vector.y;
							}
						}
						else
						{
							controlPoint.m_position = vector;
						}
					}
					ushort num;
					ushort num2;
					int num3;
					int num4;
					if (controlPoint.m_node != 0)
					{
						instance.m_tempNodeBuffer.Add(controlPoint.m_node);
					}
					else if (NetTool.CreateNode(pathInfo.m_netInfo, controlPoint, controlPoint, controlPoint, NetTool.m_nodePositionsSimulation, 0, false, false, false, false, pathInfo.m_invertSegments, false, 0, out num, out num2, out num3, out num4) == ToolBase.ToolErrors.None)
					{
						instance.m_tempNodeBuffer.Add(num);
						controlPoint.m_node = num;
						if (pathInfo.m_forbidLaneConnection != null && pathInfo.m_forbidLaneConnection.Length > 0 && pathInfo.m_forbidLaneConnection[0])
						{
							NetNode[] expr_272_cp_0 = instance.m_nodes.m_buffer;
							ushort expr_272_cp_1 = num;
							expr_272_cp_0[(int)expr_272_cp_1].m_flags = (expr_272_cp_0[(int)expr_272_cp_1].m_flags | NetNode.Flags.ForbidLaneConnection);
						}
						if (pathInfo.m_trafficLights != null && pathInfo.m_trafficLights.Length > 0)
						{
							BuildingDecoration.TrafficLightsToFlags(pathInfo.m_trafficLights[0], ref instance.m_nodes.m_buffer[(int)num].m_flags);
						}
					}
					for (int j = 1; j < pathInfo.m_nodes.Length; j++)
					{
						vector = data.CalculatePosition(pathInfo.m_nodes[j]);
						bool flag2 = BuildingDecoration.RequireFixedHeight(info, pathInfo.m_netInfo, pathInfo.m_nodes[j]);
						if (!flag2)
						{
							vector.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, vector, false, pathInfo.m_nodes[j].y + elevation);
						}
						ray..ctor(vector + new Vector3(0f, 8f, 0f), Vector3.get_down());
						NetTool.ControlPoint controlPoint2;
						if (!BuildingDecoration.FindConnectNode(instance.m_tempNodeBuffer, vector, pathInfo.m_netInfo, out controlPoint2))
						{
							if (NetTool.MakeControlPoint(ray, 16f, pathInfo.m_netInfo, true, NetNode.Flags.Untouchable, NetSegment.Flags.Untouchable, Building.Flags.All, pathInfo.m_nodes[j].y + elevation - pathInfo.m_netInfo.m_buildHeight, true, out controlPoint2))
							{
								Vector3 vector3 = controlPoint2.m_position - vector;
								if (!flag2)
								{
									vector3.y = 0f;
								}
								float sqrMagnitude2 = vector3.get_sqrMagnitude();
								if (sqrMagnitude2 > pathInfo.m_maxSnapDistance * pathInfo.m_maxSnapDistance)
								{
									controlPoint2.m_position = vector;
									controlPoint2.m_elevation = 0f;
									controlPoint2.m_node = 0;
									controlPoint2.m_segment = 0;
								}
								else
								{
									controlPoint2.m_position.y = vector.y;
								}
							}
							else
							{
								controlPoint2.m_position = vector;
							}
						}
						NetTool.ControlPoint middlePoint = controlPoint2;
						if (pathInfo.m_curveTargets != null && pathInfo.m_curveTargets.Length >= j)
						{
							middlePoint.m_position = data.CalculatePosition(pathInfo.m_curveTargets[j - 1]);
							if (!flag || !flag2)
							{
								middlePoint.m_position.y = NetSegment.SampleTerrainHeight(pathInfo.m_netInfo, middlePoint.m_position, false, pathInfo.m_curveTargets[j - 1].y + elevation);
							}
						}
						else
						{
							middlePoint.m_position = (controlPoint.m_position + controlPoint2.m_position) * 0.5f;
						}
						middlePoint.m_direction = VectorUtils.NormalizeXZ(middlePoint.m_position - controlPoint.m_position);
						controlPoint2.m_direction = VectorUtils.NormalizeXZ(controlPoint2.m_position - middlePoint.m_position);
						ushort num5;
						ushort num6;
						ushort num7;
						int num8;
						int num9;
						if (NetTool.CreateNode(pathInfo.m_netInfo, controlPoint, middlePoint, controlPoint2, NetTool.m_nodePositionsSimulation, 1, false, false, false, false, false, pathInfo.m_invertSegments, false, 0, out num5, out num6, out num7, out num8, out num9) == ToolBase.ToolErrors.None)
						{
							instance.m_tempNodeBuffer.Add(num6);
							instance.m_tempSegmentBuffer.Add(num7);
							controlPoint2.m_node = num6;
							if (pathInfo.m_forbidLaneConnection != null && pathInfo.m_forbidLaneConnection.Length > j && pathInfo.m_forbidLaneConnection[j])
							{
								NetNode[] expr_5BE_cp_0 = instance.m_nodes.m_buffer;
								ushort expr_5BE_cp_1 = num6;
								expr_5BE_cp_0[(int)expr_5BE_cp_1].m_flags = (expr_5BE_cp_0[(int)expr_5BE_cp_1].m_flags | NetNode.Flags.ForbidLaneConnection);
							}
							if (pathInfo.m_trafficLights != null && pathInfo.m_trafficLights.Length > j)
							{
								BuildingDecoration.TrafficLightsToFlags(pathInfo.m_trafficLights[j], ref instance.m_nodes.m_buffer[(int)num6].m_flags);
							}
							if (pathInfo.m_yieldSigns != null && pathInfo.m_yieldSigns.Length >= j * 2)
							{
								if (pathInfo.m_yieldSigns[j * 2 - 2])
								{
									NetSegment[] expr_64E_cp_0 = instance.m_segments.m_buffer;
									ushort expr_64E_cp_1 = num7;
									expr_64E_cp_0[(int)expr_64E_cp_1].m_flags = (expr_64E_cp_0[(int)expr_64E_cp_1].m_flags | NetSegment.Flags.YieldStart);
								}
								if (pathInfo.m_yieldSigns[j * 2 - 1])
								{
									NetSegment[] expr_683_cp_0 = instance.m_segments.m_buffer;
									ushort expr_683_cp_1 = num7;
									expr_683_cp_0[(int)expr_683_cp_1].m_flags = (expr_683_cp_0[(int)expr_683_cp_1].m_flags | NetSegment.Flags.YieldEnd);
								}
							}
						}
						controlPoint = controlPoint2;
						flag = flag2;
					}
				}
			}
			for (int k = 0; k < instance.m_tempNodeBuffer.m_size; k++)
			{
				ushort num10 = instance.m_tempNodeBuffer.m_buffer[k];
				if ((instance.m_nodes.m_buffer[(int)num10].m_flags & NetNode.Flags.Untouchable) == NetNode.Flags.None)
				{
					if (buildingID != 0)
					{
						if ((data.m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_nodes.m_buffer[(int)num10].Info.m_canDisable)
						{
							NetNode[] expr_746_cp_0 = instance.m_nodes.m_buffer;
							ushort expr_746_cp_1 = num10;
							expr_746_cp_0[(int)expr_746_cp_1].m_flags = (expr_746_cp_0[(int)expr_746_cp_1].m_flags | NetNode.Flags.Disabled);
						}
						NetNode[] expr_765_cp_0 = instance.m_nodes.m_buffer;
						ushort expr_765_cp_1 = num10;
						expr_765_cp_0[(int)expr_765_cp_1].m_flags = (expr_765_cp_0[(int)expr_765_cp_1].m_flags | NetNode.Flags.Untouchable);
						instance.UpdateNode(num10);
						instance.m_nodes.m_buffer[(int)num10].m_nextBuildingNode = data.m_netNode;
						data.m_netNode = num10;
					}
					else
					{
						instance.UpdateNode(num10);
					}
				}
			}
			for (int l = 0; l < instance.m_tempSegmentBuffer.m_size; l++)
			{
				ushort num11 = instance.m_tempSegmentBuffer.m_buffer[l];
				if ((instance.m_segments.m_buffer[(int)num11].m_flags & NetSegment.Flags.Untouchable) == NetSegment.Flags.None)
				{
					if (buildingID != 0)
					{
						NetSegment[] expr_817_cp_0 = instance.m_segments.m_buffer;
						ushort expr_817_cp_1 = num11;
						expr_817_cp_0[(int)expr_817_cp_1].m_flags = (expr_817_cp_0[(int)expr_817_cp_1].m_flags | NetSegment.Flags.Untouchable);
						instance.UpdateSegment(num11);
					}
					else
					{
						if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.AssetEditor) != ItemClass.Availability.None)
						{
							NetInfo info2 = instance.m_segments.m_buffer[(int)num11].Info;
							if ((info2.m_availableIn & ItemClass.Availability.AssetEditor) == ItemClass.Availability.None)
							{
								NetSegment[] expr_881_cp_0 = instance.m_segments.m_buffer;
								ushort expr_881_cp_1 = num11;
								expr_881_cp_0[(int)expr_881_cp_1].m_flags = (expr_881_cp_0[(int)expr_881_cp_1].m_flags | NetSegment.Flags.Untouchable);
							}
						}
						instance.UpdateSegment(num11);
					}
				}
			}
			instance.m_tempNodeBuffer.Clear();
			instance.m_tempSegmentBuffer.Clear();
		}
	}

	public static void SavePaths(BuildingInfo info, ushort buildingID, ref Building data)
	{
		FastList<BuildingInfo.PathInfo> fastList = new FastList<BuildingInfo.PathInfo>();
		List<ushort> list = new List<ushort>();
		List<ushort> list2 = new List<ushort>();
		for (ushort num = 1; num < 49152; num += 1)
		{
			if (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num].m_flags != Building.Flags.None)
			{
				list.AddRange(BuildingDecoration.GetBuildingSegments(ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)num]));
				list2.Add(num);
			}
		}
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = 0; i < 36864; i++)
		{
			if ((instance.m_segments.m_buffer[i].m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted)) == NetSegment.Flags.Created)
			{
				if (!list.Contains((ushort)i))
				{
					NetInfo info2 = instance.m_segments.m_buffer[i].Info;
					ushort startNode = instance.m_segments.m_buffer[i].m_startNode;
					ushort endNode = instance.m_segments.m_buffer[i].m_endNode;
					Vector3 position = instance.m_nodes.m_buffer[(int)startNode].m_position;
					Vector3 position2 = instance.m_nodes.m_buffer[(int)endNode].m_position;
					Vector3 startDirection = instance.m_segments.m_buffer[i].m_startDirection;
					Vector3 endDirection = instance.m_segments.m_buffer[i].m_endDirection;
					Vector3 vector;
					float num2;
					float num3;
					if (NetSegment.IsStraight(position, startDirection, position2, endDirection))
					{
						vector = (position + position2) * 0.5f;
					}
					else if (Line2.Intersect(VectorUtils.XZ(position), VectorUtils.XZ(position + startDirection), VectorUtils.XZ(position2), VectorUtils.XZ(position2 + endDirection), ref num2, ref num3))
					{
						float minNodeDistance = info2.GetMinNodeDistance();
						num2 = Mathf.Max(minNodeDistance, num2);
						num3 = Mathf.Max(minNodeDistance, num3);
						vector = (position + startDirection * num2 + position2 + endDirection * num3) * 0.5f;
					}
					else
					{
						vector = (position + position2) * 0.5f;
					}
					BuildingInfo.PathInfo pathInfo = new BuildingInfo.PathInfo();
					pathInfo.m_netInfo = info2;
					pathInfo.m_nodes = new Vector3[2];
					pathInfo.m_nodes[0] = position - data.m_position;
					pathInfo.m_nodes[0].z = -pathInfo.m_nodes[0].z;
					pathInfo.m_nodes[1] = position2 - data.m_position;
					pathInfo.m_nodes[1].z = -pathInfo.m_nodes[1].z;
					pathInfo.m_curveTargets = new Vector3[1];
					pathInfo.m_curveTargets[0] = vector - data.m_position;
					pathInfo.m_curveTargets[0].z = -pathInfo.m_curveTargets[0].z;
					pathInfo.m_forbidLaneConnection = new bool[2];
					pathInfo.m_forbidLaneConnection[0] = ((instance.m_nodes.m_buffer[(int)startNode].m_flags & NetNode.Flags.ForbidLaneConnection) != NetNode.Flags.None);
					pathInfo.m_forbidLaneConnection[1] = ((instance.m_nodes.m_buffer[(int)endNode].m_flags & NetNode.Flags.ForbidLaneConnection) != NetNode.Flags.None);
					pathInfo.m_trafficLights = new BuildingInfo.TrafficLights[2];
					pathInfo.m_trafficLights[0] = BuildingDecoration.FlagsToTrafficLights(instance.m_nodes.m_buffer[(int)startNode].m_flags);
					pathInfo.m_trafficLights[1] = BuildingDecoration.FlagsToTrafficLights(instance.m_nodes.m_buffer[(int)endNode].m_flags);
					pathInfo.m_yieldSigns = new bool[2];
					pathInfo.m_yieldSigns[0] = ((instance.m_segments.m_buffer[i].m_flags & NetSegment.Flags.YieldStart) != NetSegment.Flags.None);
					pathInfo.m_yieldSigns[1] = ((instance.m_segments.m_buffer[i].m_flags & NetSegment.Flags.YieldEnd) != NetSegment.Flags.None);
					if (info.m_placementMode == BuildingInfo.PlacementMode.Roadside && (position.z > (float)info.m_cellLength * 4f + 8f || position2.z > (float)info.m_cellLength * 4f + 8f))
					{
						pathInfo.m_maxSnapDistance = 7.5f;
					}
					else
					{
						pathInfo.m_maxSnapDistance = 0.1f;
					}
					pathInfo.m_invertSegments = ((instance.m_segments.m_buffer[i].m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None);
					fastList.Add(pathInfo);
				}
			}
		}
		for (int j = 0; j < 32768; j++)
		{
			if ((instance.m_nodes.m_buffer[j].m_flags & (NetNode.Flags.Created | NetNode.Flags.Deleted)) == NetNode.Flags.Created && instance.m_nodes.m_buffer[j].CountSegments() == 0)
			{
				bool flag = false;
				foreach (ushort current in list2)
				{
					if (Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)current].ContainsNode((ushort)j))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					NetInfo info3 = instance.m_nodes.m_buffer[j].Info;
					Vector3 position3 = instance.m_nodes.m_buffer[j].m_position;
					BuildingInfo.PathInfo pathInfo2 = new BuildingInfo.PathInfo();
					pathInfo2.m_netInfo = info3;
					pathInfo2.m_nodes = new Vector3[1];
					pathInfo2.m_nodes[0] = position3 - data.m_position;
					pathInfo2.m_nodes[0].z = -pathInfo2.m_nodes[0].z;
					pathInfo2.m_curveTargets = new Vector3[0];
					pathInfo2.m_forbidLaneConnection = new bool[1];
					pathInfo2.m_forbidLaneConnection[0] = ((instance.m_nodes.m_buffer[j].m_flags & NetNode.Flags.ForbidLaneConnection) != NetNode.Flags.None);
					pathInfo2.m_trafficLights = new BuildingInfo.TrafficLights[1];
					pathInfo2.m_trafficLights[0] = BuildingDecoration.FlagsToTrafficLights(instance.m_nodes.m_buffer[j].m_flags);
					pathInfo2.m_yieldSigns = new bool[0];
					pathInfo2.m_maxSnapDistance = 0.1f;
					pathInfo2.m_invertSegments = false;
					fastList.Add(pathInfo2);
				}
			}
		}
		info.m_paths = fastList.ToArray();
	}

	private static BuildingInfo.TrafficLights FlagsToTrafficLights(NetNode.Flags flags)
	{
		if ((flags & NetNode.Flags.CustomTrafficLights) == NetNode.Flags.None)
		{
			return BuildingInfo.TrafficLights.Default;
		}
		if ((flags & NetNode.Flags.TrafficLights) == NetNode.Flags.None)
		{
			return BuildingInfo.TrafficLights.ForceOff;
		}
		return BuildingInfo.TrafficLights.ForceOn;
	}

	private static void TrafficLightsToFlags(BuildingInfo.TrafficLights trafficLights, ref NetNode.Flags flags)
	{
		if (trafficLights != BuildingInfo.TrafficLights.Default)
		{
			if (trafficLights != BuildingInfo.TrafficLights.ForceOn)
			{
				if (trafficLights == BuildingInfo.TrafficLights.ForceOff)
				{
					flags = ((flags & ~NetNode.Flags.TrafficLights) | NetNode.Flags.CustomTrafficLights);
				}
			}
			else
			{
				flags |= (NetNode.Flags.TrafficLights | NetNode.Flags.CustomTrafficLights);
			}
		}
		else
		{
			flags &= (NetNode.Flags.Created | NetNode.Flags.Deleted | NetNode.Flags.Original | NetNode.Flags.Disabled | NetNode.Flags.End | NetNode.Flags.Middle | NetNode.Flags.Bend | NetNode.Flags.Junction | NetNode.Flags.Moveable | NetNode.Flags.Untouchable | NetNode.Flags.Outside | NetNode.Flags.Temporary | NetNode.Flags.Double | NetNode.Flags.Fixed | NetNode.Flags.OnGround | NetNode.Flags.Ambiguous | NetNode.Flags.Water | NetNode.Flags.Sewage | NetNode.Flags.ForbidLaneConnection | NetNode.Flags.Underground | NetNode.Flags.Transition | NetNode.Flags.LevelCrossing | NetNode.Flags.OneWayOut | NetNode.Flags.TrafficLights | NetNode.Flags.OneWayIn | NetNode.Flags.Heating | NetNode.Flags.Electricity | NetNode.Flags.Collapsed | NetNode.Flags.DisableOnlyMiddle | NetNode.Flags.AsymForward | NetNode.Flags.AsymBackward);
		}
	}

	public static ushort[] GetBuildingSegments(ref Building data)
	{
		List<ushort> list = new List<ushort>();
		if (data.m_netNode != 0)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = data.m_netNode;
			int num2 = 0;
			while (num != 0)
			{
				for (int i = 0; i < 8; i++)
				{
					ushort segment = instance.m_nodes.m_buffer[(int)num].GetSegment(i);
					if (segment != 0 && (instance.m_segments.m_buffer[(int)segment].m_flags & NetSegment.Flags.Untouchable) != NetSegment.Flags.None)
					{
						list.Add(segment);
					}
				}
				num = instance.m_nodes.m_buffer[(int)num].m_nextBuildingNode;
				if (++num2 > 32768)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
		}
		return list.ToArray();
	}

	public static void RenderBuildingMesh(RenderManager.CameraInfo cameraInfo, BuildingInfo info)
	{
		BuildingManager instance = Singleton<BuildingManager>.get_instance();
		Vector3 vector;
		vector..ctor(0f, 60f, 0f);
		Vector3 vector2 = vector;
		Quaternion identity = Quaternion.get_identity();
		Matrix4x4 matrix4x = default(Matrix4x4);
		matrix4x.SetTRS(vector2, identity, Vector3.get_one());
		Vector4 vector3;
		vector3..ctor(0f, 1000f, 0f, 0f);
		Vector4 colorLocation = RenderManager.GetColorLocation(0u);
		instance.m_materialBlock.Clear();
		instance.m_materialBlock.SetVector(instance.ID_BuildingState, vector3);
		instance.m_materialBlock.SetVector(instance.ID_ObjectIndex, colorLocation);
		if (info.m_requireHeightMap)
		{
			Texture texture;
			Vector4 vector4;
			Vector4 vector5;
			Singleton<TerrainManager>.get_instance().GetHeightMapping(vector, out texture, out vector4, out vector5);
			instance.m_materialBlock.SetTexture(instance.ID_HeightMap, texture);
			instance.m_materialBlock.SetVector(instance.ID_HeightMapping, vector4);
			instance.m_materialBlock.SetVector(instance.ID_SurfaceMapping, vector5);
		}
		if (info.m_requireWaterMap)
		{
			Texture texture2;
			Vector4 vector6;
			Vector4 vector7;
			Singleton<TerrainManager>.get_instance().GetWaterMapping(vector, out texture2, out vector6, out vector7);
			instance.m_materialBlock.SetTexture(instance.ID_WaterHeightMap, texture2);
			instance.m_materialBlock.SetVector(instance.ID_WaterHeightMapping, vector6);
			instance.m_materialBlock.SetVector(instance.ID_WaterSurfaceMapping, vector7);
		}
		float minLodDistance = info.m_minLodDistance;
		if (cameraInfo.CheckRenderDistance(vector2, minLodDistance))
		{
			if (info.m_mesh != null)
			{
				BuildingManager expr_165_cp_0 = instance;
				expr_165_cp_0.m_drawCallData.m_defaultCalls = expr_165_cp_0.m_drawCallData.m_defaultCalls + 1;
				Graphics.DrawMesh(info.m_mesh, matrix4x, info.m_material, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
			}
		}
		else if (info.m_lodMesh != null)
		{
			BuildingManager expr_1AF_cp_0 = instance;
			expr_1AF_cp_0.m_drawCallData.m_defaultCalls = expr_1AF_cp_0.m_drawCallData.m_defaultCalls + 1;
			Graphics.DrawMesh(info.m_lodMesh, matrix4x, info.m_lodMaterial, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
		}
		if (info.m_subMeshes != null)
		{
			for (int i = 0; i < info.m_subMeshes.Length; i++)
			{
				BuildingInfo.MeshInfo meshInfo = info.m_subMeshes[i];
				BuildingInfoBase subInfo = meshInfo.m_subInfo;
				minLodDistance = subInfo.m_minLodDistance;
				Matrix4x4 matrix4x2 = matrix4x * meshInfo.m_matrix;
				if (cameraInfo.CheckRenderDistance(vector2, minLodDistance))
				{
					if (subInfo.m_mesh != null)
					{
						BuildingManager expr_243_cp_0 = instance;
						expr_243_cp_0.m_drawCallData.m_defaultCalls = expr_243_cp_0.m_drawCallData.m_defaultCalls + 1;
						Graphics.DrawMesh(subInfo.m_mesh, matrix4x2, subInfo.m_material, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
					}
				}
				else if (subInfo.m_mesh != null)
				{
					BuildingManager expr_290_cp_0 = instance;
					expr_290_cp_0.m_drawCallData.m_defaultCalls = expr_290_cp_0.m_drawCallData.m_defaultCalls + 1;
					Graphics.DrawMesh(subInfo.m_lodMesh, matrix4x2, subInfo.m_lodMaterial, info.m_prefabDataLayer, null, 0, instance.m_materialBlock);
				}
			}
		}
	}

	public static void EnsureCellSurface(BuildingInfo info)
	{
		int num;
		int num2;
		float num3;
		info.m_buildingAI.GetDecorationArea(out num, out num2, out num3);
		int num4 = num * num2;
		int num5;
		if (info.m_cellSurfaces != null)
		{
			num5 = info.m_cellSurfaces.Length;
		}
		else
		{
			num5 = 0;
		}
		if (num5 != num4)
		{
			info.m_cellSurfaces = new TerrainModify.Surface[num4];
		}
		if (num4 != 0)
		{
			info.m_fullPavement = false;
			info.m_fullGravel = false;
			info.m_clipTerrain = false;
		}
	}
}
