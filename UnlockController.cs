﻿using System;
using ColossalFramework;
using UnityEngine;

public class UnlockController : MonoBehaviour
{
	private void Awake()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("UnlockController");
		Singleton<UnlockManager>.get_instance().InitializeProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	private void OnDestroy()
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.BeginLoading("UnlockController");
		Singleton<UnlockManager>.get_instance().DestroyProperties(this);
		Singleton<LoadingManager>.get_instance().m_loadingProfilerMain.EndLoading();
	}

	public MilestoneInfo[] m_progressionMilestones;

	public MilestoneInfo[] m_FeatureMilestones;

	public MilestoneInfo[] m_AreaMilestones;

	public MilestoneInfo[] m_ZoneMilestones;

	public MilestoneInfo[] m_ServiceMilestones;

	public MilestoneInfo[] m_SubServiceMilestones;

	public MilestoneInfo[] m_PolicyTypeMilestones;

	public MilestoneInfo[] m_SpecializationMilestones;

	public MilestoneInfo[] m_ServicePolicyMilestones;

	public MilestoneInfo[] m_TaxationPolicyMilestones;

	public MilestoneInfo[] m_CityPlanningPolicyMilestones;

	public MilestoneInfo[] m_SpecialPolicyMilestones;

	public MilestoneInfo[] m_EventPolicyMilestones;

	public MilestoneInfo[] m_InfoModeMilestones;

	public MilestoneInfo[] m_AchievementMilestones;

	public MilestoneInfo[] m_ScenarioMilestones;
}
