﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class RoadAI : RoadBaseAI
{
	public override void GetEffectRadius(out float radius, out bool capped, out Color color)
	{
		if (this.m_enableZoning)
		{
			radius = Mathf.Max(8f, this.m_info.m_halfWidth) + 32f;
			capped = true;
			if (Singleton<InfoManager>.get_instance().CurrentMode != InfoManager.InfoMode.None)
			{
				color = Singleton<ToolManager>.get_instance().m_properties.m_validColorInfo;
				color.a *= 0.5f;
			}
			else
			{
				color = Singleton<ToolManager>.get_instance().m_properties.m_validColor;
				color.a *= 0.5f;
			}
		}
		else
		{
			radius = 0f;
			capped = false;
			color..ctor(0f, 0f, 0f, 0f);
		}
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		if (elevation < -8f)
		{
			mode = InfoManager.InfoMode.Underground;
			subMode = InfoManager.SubInfoMode.Default;
		}
		else
		{
			mode = InfoManager.InfoMode.None;
			subMode = InfoManager.SubInfoMode.Default;
		}
	}

	public override void CreateSegment(ushort segmentID, ref NetSegment data)
	{
		base.CreateSegment(segmentID, ref data);
		if (this.m_enableZoning)
		{
			this.CreateZoneBlocks(segmentID, ref data);
		}
	}

	public override void GetElevationLimits(out int min, out int max)
	{
		min = ((this.m_tunnelInfo == null || (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) == ItemClass.Availability.None) ? 0 : -3);
		max = ((this.m_elevatedInfo == null && this.m_bridgeInfo == null) ? 0 : 5);
	}

	public override NetInfo GetInfo(float minElevation, float maxElevation, float length, bool incoming, bool outgoing, bool curved, bool enableDouble, ref ToolBase.ToolErrors errors)
	{
		if (incoming || outgoing)
		{
			int num;
			int num2;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, out num, out num2);
			if ((incoming && num >= 4) || (outgoing && num2 >= 4))
			{
				errors |= ToolBase.ToolErrors.TooManyConnections;
			}
		}
		if (maxElevation > 255f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		if ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) != ItemClass.Availability.None)
		{
			if (this.m_tunnelInfo != null && maxElevation < -8f)
			{
				return this.m_tunnelInfo;
			}
			if (this.m_slopeInfo != null && minElevation < -8f)
			{
				return this.m_slopeInfo;
			}
		}
		if (this.m_bridgeInfo != null && maxElevation > 25f && length > 45f && !curved && (enableDouble || !this.m_bridgeInfo.m_netAI.RequireDoubleSegments()))
		{
			return this.m_bridgeInfo;
		}
		if (this.m_elevatedInfo != null && maxElevation > 0.1f)
		{
			return this.m_elevatedInfo;
		}
		if (maxElevation > 8f)
		{
			errors |= ToolBase.ToolErrors.HeightTooHigh;
		}
		return this.m_info;
	}

	public override float GetNodeInfoPriority(ushort segmentID, ref NetSegment data)
	{
		return this.m_info.m_halfWidth + 1000f;
	}

	public override bool DisplayTempSegment()
	{
		return false;
	}

	public override bool SupportUnderground()
	{
		return this.m_tunnelInfo != null && (Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.GameAndAsset) != ItemClass.Availability.None;
	}

	public override void UpdateSegmentFlags(ushort segmentID, ref NetSegment data)
	{
		base.UpdateSegmentFlags(segmentID, ref data);
		NetSegment.Flags flags = data.m_flags & ~(NetSegment.Flags.StopRight | NetSegment.Flags.StopLeft | NetSegment.Flags.StopRight2 | NetSegment.Flags.StopLeft2);
		if (this.m_info.m_lanes != null)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			bool flag = (data.m_flags & NetSegment.Flags.Invert) != NetSegment.Flags.None;
			uint num = instance.m_segments.m_buffer[(int)segmentID].m_lanes;
			int num2 = 0;
			while (num2 < this.m_info.m_lanes.Length && num != 0u)
			{
				NetLane.Flags flags2 = (NetLane.Flags)instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_flags;
				if ((flags2 & NetLane.Flags.Stop) != NetLane.Flags.None)
				{
					if (this.m_info.m_lanes[num2].m_position < 0f != flag)
					{
						flags |= NetSegment.Flags.StopLeft;
					}
					else
					{
						flags |= NetSegment.Flags.StopRight;
					}
				}
				if ((flags2 & NetLane.Flags.Stop2) != NetLane.Flags.None)
				{
					if (this.m_info.m_lanes[num2].m_position < 0f != flag)
					{
						flags |= NetSegment.Flags.StopLeft2;
					}
					else
					{
						flags |= NetSegment.Flags.StopRight2;
					}
				}
				num = instance.m_lanes.m_buffer[(int)((UIntPtr)num)].m_nextLane;
				num2++;
			}
		}
		data.m_flags = flags;
	}

	public void CreateZoneBlocks(ushort segment, ref NetSegment data)
	{
		NetManager instance = Singleton<NetManager>.get_instance();
		Randomizer randomizer;
		randomizer..ctor((int)segment);
		Vector3 position = instance.m_nodes.m_buffer[(int)data.m_startNode].m_position;
		Vector3 position2 = instance.m_nodes.m_buffer[(int)data.m_endNode].m_position;
		Vector3 startDirection = data.m_startDirection;
		Vector3 endDirection = data.m_endDirection;
		float num = startDirection.x * endDirection.x + startDirection.z * endDirection.z;
		bool flag = !NetSegment.IsStraight(position, startDirection, position2, endDirection);
		float num2 = Mathf.Max(8f, this.m_info.m_halfWidth);
		float num3 = 32f;
		if (flag)
		{
			float num4 = VectorUtils.LengthXZ(position2 - position);
			bool flag2 = startDirection.x * endDirection.z - startDirection.z * endDirection.x > 0f;
			bool flag3 = num < -0.8f || num4 > 50f;
			if (flag2)
			{
				num2 = -num2;
				num3 = -num3;
			}
			Vector3 vector = position - new Vector3(startDirection.z, 0f, -startDirection.x) * num2;
			Vector3 vector2 = position2 + new Vector3(endDirection.z, 0f, -endDirection.x) * num2;
			Vector3 vector3;
			Vector3 vector4;
			NetSegment.CalculateMiddlePoints(vector, startDirection, vector2, endDirection, true, true, out vector3, out vector4);
			if (flag3)
			{
				float num5 = num * 0.025f + 0.04f;
				float num6 = num * 0.025f + 0.06f;
				if (num < -0.9f)
				{
					num6 = num5;
				}
				Bezier3 bezier;
				bezier..ctor(vector, vector3, vector4, vector2);
				vector = bezier.Position(num5);
				vector3 = bezier.Position(0.5f - num6);
				vector4 = bezier.Position(0.5f + num6);
				vector2 = bezier.Position(1f - num5);
			}
			else
			{
				Bezier3 bezier2;
				bezier2..ctor(vector, vector3, vector4, vector2);
				vector3 = bezier2.Position(0.86f);
				vector = bezier2.Position(0.14f);
			}
			float num7;
			Vector3 vector5 = VectorUtils.NormalizeXZ(vector3 - vector, ref num7);
			int num8 = Mathf.FloorToInt(num7 / 8f + 0.01f);
			float num9 = num7 * 0.5f + (float)(num8 - 8) * ((!flag2) ? -4f : 4f);
			if (num8 != 0)
			{
				float angle = (!flag2) ? Mathf.Atan2(vector5.x, -vector5.z) : Mathf.Atan2(-vector5.x, vector5.z);
				Vector3 position3 = vector + new Vector3(vector5.x * num9 - vector5.z * num3, 0f, vector5.z * num9 + vector5.x * num3);
				if (flag2)
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartRight, ref randomizer, position3, angle, num8, data.m_buildIndex);
				}
				else
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartLeft, ref randomizer, position3, angle, num8, data.m_buildIndex);
				}
			}
			if (flag3)
			{
				vector5 = VectorUtils.NormalizeXZ(vector2 - vector4, ref num7);
				num8 = Mathf.FloorToInt(num7 / 8f + 0.01f);
				num9 = num7 * 0.5f + (float)(num8 - 8) * ((!flag2) ? -4f : 4f);
				if (num8 != 0)
				{
					float angle2 = (!flag2) ? Mathf.Atan2(vector5.x, -vector5.z) : Mathf.Atan2(-vector5.x, vector5.z);
					Vector3 position4 = vector4 + new Vector3(vector5.x * num9 - vector5.z * num3, 0f, vector5.z * num9 + vector5.x * num3);
					if (flag2)
					{
						Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndRight, ref randomizer, position4, angle2, num8, data.m_buildIndex + 1u);
					}
					else
					{
						Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndLeft, ref randomizer, position4, angle2, num8, data.m_buildIndex + 1u);
					}
				}
			}
			Vector3 vector6 = position + new Vector3(startDirection.z, 0f, -startDirection.x) * num2;
			Vector3 vector7 = position2 - new Vector3(endDirection.z, 0f, -endDirection.x) * num2;
			Vector3 vector8;
			Vector3 vector9;
			NetSegment.CalculateMiddlePoints(vector6, startDirection, vector7, endDirection, true, true, out vector8, out vector9);
			Bezier3 bezier3;
			bezier3..ctor(vector6, vector8, vector9, vector7);
			Vector3 vector10 = bezier3.Position(0.5f);
			Vector3 vector11 = bezier3.Position(0.25f);
			vector11 = Line2.Offset(VectorUtils.XZ(vector6), VectorUtils.XZ(vector10), VectorUtils.XZ(vector11));
			Vector3 vector12 = bezier3.Position(0.75f);
			vector12 = Line2.Offset(VectorUtils.XZ(vector7), VectorUtils.XZ(vector10), VectorUtils.XZ(vector12));
			Vector3 vector13 = vector6;
			Vector3 vector14 = vector7;
			float num10;
			float num11;
			if (Line2.Intersect(VectorUtils.XZ(position), VectorUtils.XZ(vector6), VectorUtils.XZ(vector13 - vector11), VectorUtils.XZ(vector10 - vector11), ref num10, ref num11))
			{
				vector6 = position + (vector6 - position) * num10;
			}
			if (Line2.Intersect(VectorUtils.XZ(position2), VectorUtils.XZ(vector7), VectorUtils.XZ(vector14 - vector12), VectorUtils.XZ(vector10 - vector12), ref num10, ref num11))
			{
				vector7 = position2 + (vector7 - position2) * num10;
			}
			if (Line2.Intersect(VectorUtils.XZ(vector13 - vector11), VectorUtils.XZ(vector10 - vector11), VectorUtils.XZ(vector14 - vector12), VectorUtils.XZ(vector10 - vector12), ref num10, ref num11))
			{
				vector10 = vector13 - vector11 + (vector10 - vector13) * num10;
			}
			float num12;
			Vector3 vector15 = VectorUtils.NormalizeXZ(vector10 - vector6, ref num12);
			int num13 = Mathf.FloorToInt(num12 / 8f + 0.01f);
			float num14 = num12 * 0.5f + (float)(num13 - 8) * ((!flag2) ? 4f : -4f);
			if (num13 != 0)
			{
				float angle3 = (!flag2) ? Mathf.Atan2(-vector15.x, vector15.z) : Mathf.Atan2(vector15.x, -vector15.z);
				Vector3 position5 = vector6 + new Vector3(vector15.x * num14 + vector15.z * num3, 0f, vector15.z * num14 - vector15.x * num3);
				if (flag2)
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartLeft, ref randomizer, position5, angle3, num13, data.m_buildIndex);
				}
				else
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartRight, ref randomizer, position5, angle3, num13, data.m_buildIndex);
				}
			}
			vector15 = VectorUtils.NormalizeXZ(vector7 - vector10, ref num12);
			num13 = Mathf.FloorToInt(num12 / 8f + 0.01f);
			num14 = num12 * 0.5f + (float)(num13 - 8) * ((!flag2) ? 4f : -4f);
			if (num13 != 0)
			{
				float angle4 = (!flag2) ? Mathf.Atan2(-vector15.x, vector15.z) : Mathf.Atan2(vector15.x, -vector15.z);
				Vector3 position6 = vector10 + new Vector3(vector15.x * num14 + vector15.z * num3, 0f, vector15.z * num14 - vector15.x * num3);
				if (flag2)
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndLeft, ref randomizer, position6, angle4, num13, data.m_buildIndex + 1u);
				}
				else
				{
					Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndRight, ref randomizer, position6, angle4, num13, data.m_buildIndex + 1u);
				}
			}
		}
		else
		{
			num2 += num3;
			Vector2 vector16;
			vector16..ctor(position2.x - position.x, position2.z - position.z);
			float magnitude = vector16.get_magnitude();
			int num15 = Mathf.FloorToInt(magnitude / 8f + 0.1f);
			int num16 = (num15 <= 8) ? num15 : (num15 + 1 >> 1);
			int num17 = (num15 <= 8) ? 0 : (num15 >> 1);
			if (num16 > 0)
			{
				float num18 = Mathf.Atan2(startDirection.x, -startDirection.z);
				Vector3 position7 = position + new Vector3(startDirection.x * 32f - startDirection.z * num2, 0f, startDirection.z * 32f + startDirection.x * num2);
				Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartLeft, ref randomizer, position7, num18, num16, data.m_buildIndex);
				position7 = position + new Vector3(startDirection.x * (float)(num16 - 4) * 8f + startDirection.z * num2, 0f, startDirection.z * (float)(num16 - 4) * 8f - startDirection.x * num2);
				Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockStartRight, ref randomizer, position7, num18 + 3.14159274f, num16, data.m_buildIndex);
			}
			if (num17 > 0)
			{
				float num19 = magnitude - (float)num15 * 8f;
				float num20 = Mathf.Atan2(endDirection.x, -endDirection.z);
				Vector3 position8 = position2 + new Vector3(endDirection.x * (32f + num19) - endDirection.z * num2, 0f, endDirection.z * (32f + num19) + endDirection.x * num2);
				Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndLeft, ref randomizer, position8, num20, num17, data.m_buildIndex + 1u);
				position8 = position2 + new Vector3(endDirection.x * ((float)(num17 - 4) * 8f + num19) + endDirection.z * num2, 0f, endDirection.z * ((float)(num17 - 4) * 8f + num19) - endDirection.x * num2);
				Singleton<ZoneManager>.get_instance().CreateBlock(out data.m_blockEndRight, ref randomizer, position8, num20 + 3.14159274f, num17, data.m_buildIndex + 1u);
			}
		}
	}

	public override ToolBase.ToolErrors CheckBuildPosition(bool test, bool visualize, bool overlay, bool autofix, ref NetTool.ControlPoint startPoint, ref NetTool.ControlPoint middlePoint, ref NetTool.ControlPoint endPoint, out BuildingInfo ownerBuilding, out Vector3 ownerPosition, out Vector3 ownerDirection, out int productionRate)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(test, visualize, overlay, autofix, ref startPoint, ref middlePoint, ref endPoint, out ownerBuilding, out ownerPosition, out ownerDirection, out productionRate);
		if (test)
		{
			NetManager instance = Singleton<NetManager>.get_instance();
			ushort num = middlePoint.m_segment;
			if (startPoint.m_segment == num || endPoint.m_segment == num)
			{
				num = 0;
			}
			if (num != 0)
			{
				NetInfo info = instance.m_segments.m_buffer[(int)num].Info;
				if ((Singleton<NetManager>.get_instance().m_segments.m_buffer[(int)num].m_flags & NetSegment.Flags.Collapsed) == NetSegment.Flags.None)
				{
					if (this.m_elevatedInfo == info)
					{
						toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
					}
					if (this.m_bridgeInfo == info)
					{
						toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
					}
					if (this.m_tunnelInfo == info)
					{
						toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
					}
					if (this.m_slopeInfo == info)
					{
						toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
					}
				}
				if (info.m_netAI.IsUnderground() && !this.SupportUnderground())
				{
					toolErrors |= ToolBase.ToolErrors.CannotUpgrade;
				}
			}
			if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Tram) != VehicleInfo.VehicleType.None)
			{
				if (startPoint.m_node != 0)
				{
					for (int i = 0; i < 8; i++)
					{
						ushort segment = instance.m_nodes.m_buffer[(int)startPoint.m_node].GetSegment(i);
						if (segment != 0)
						{
							NetInfo info2 = instance.m_segments.m_buffer[(int)segment].Info;
							if ((info2.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
							{
								toolErrors |= ToolBase.ToolErrors.CannotCrossTrack;
							}
						}
					}
				}
				else if (startPoint.m_segment != 0)
				{
					NetInfo info3 = instance.m_segments.m_buffer[(int)startPoint.m_segment].Info;
					if ((info3.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
					{
						toolErrors |= ToolBase.ToolErrors.CannotCrossTrack;
					}
				}
				if (endPoint.m_node != 0)
				{
					for (int j = 0; j < 8; j++)
					{
						ushort segment2 = instance.m_nodes.m_buffer[(int)endPoint.m_node].GetSegment(j);
						if (segment2 != 0)
						{
							NetInfo info4 = instance.m_segments.m_buffer[(int)segment2].Info;
							if ((info4.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
							{
								toolErrors |= ToolBase.ToolErrors.CannotCrossTrack;
							}
						}
					}
				}
				else if (endPoint.m_segment != 0)
				{
					NetInfo info5 = instance.m_segments.m_buffer[(int)endPoint.m_segment].Info;
					if ((info5.m_vehicleTypes & VehicleInfo.VehicleType.Train) != VehicleInfo.VehicleType.None)
					{
						toolErrors |= ToolBase.ToolErrors.CannotCrossTrack;
					}
				}
			}
			if (this.m_enableZoning && !Singleton<ZoneManager>.get_instance().CheckLimits())
			{
				toolErrors |= ToolBase.ToolErrors.TooManyObjects;
			}
		}
		return toolErrors;
	}

	public override bool CanUpgradeTo(NetInfo info)
	{
		return this.m_elevatedInfo != info && this.m_bridgeInfo != info && this.m_tunnelInfo != info && this.m_slopeInfo != info && (!this.IsUnderground() || info.m_netAI.SupportUnderground()) && base.CanUpgradeTo(info);
	}

	public override string GenerateName(ushort segmentID, ref NetSegment data)
	{
		Randomizer randomizer;
		randomizer..ctor((int)data.m_nameSeed);
		string text = null;
		if ((this.m_info.m_vehicleTypes & VehicleInfo.VehicleType.Car) != VehicleInfo.VehicleType.None)
		{
			if (this.m_enableZoning)
			{
				if ((this.m_info.m_setVehicleFlags & Vehicle.Flags.OnGravel) != (Vehicle.Flags)0)
				{
					text = "ROAD_NAME_PATTERN";
				}
				else if (this.m_info.m_halfWidth >= 12f)
				{
					text = "AVENUE_NAME_PATTERN";
				}
				else
				{
					text = "STREET_NAME_PATTERN";
				}
			}
			else if (this.m_highwayRules)
			{
				if (this.m_info.m_hasForwardVehicleLanes && this.m_info.m_hasBackwardVehicleLanes)
				{
					text = "ROAD_NAME_PATTERN";
				}
				else if (this.m_info.m_forwardVehicleLaneCount >= 2 || this.m_info.m_backwardVehicleLaneCount >= 2)
				{
					text = "HIGHWAY_NAME_PATTERN";
				}
			}
		}
		if (text == null)
		{
			return string.Empty;
		}
		uint num = Locale.Count(text);
		string format = Locale.Get(text, randomizer.Int32(num));
		string arg = base.GenerateStreetName(ref randomizer);
		return StringUtils.SafeFormat(format, arg);
	}

	[CustomizableProperty("Enable Zoning", "Properties")]
	public bool m_enableZoning;

	public NetInfo m_elevatedInfo;

	public NetInfo m_bridgeInfo;

	public NetInfo m_slopeInfo;

	public NetInfo m_tunnelInfo;
}
