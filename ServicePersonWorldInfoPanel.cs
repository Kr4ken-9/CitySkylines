﻿using System;
using ColossalFramework;
using ColossalFramework.UI;

public sealed class ServicePersonWorldInfoPanel : LivingCreatureWorldInfoPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_Type = base.Find<UILabel>("Type");
		this.m_Status = base.Find<UILabel>("Status");
	}

	protected override void UpdateBindings()
	{
		base.UpdateBindings();
		if (Singleton<CitizenManager>.get_exists())
		{
			CitizenManager instance = Singleton<CitizenManager>.get_instance();
			if (this.m_InstanceID.Type == InstanceType.CitizenInstance && this.m_InstanceID.CitizenInstance != 0)
			{
				ushort citizenInstance = this.m_InstanceID.CitizenInstance;
				this.m_Type.set_text(Singleton<CitizenManager>.get_instance().GetDefaultInstanceName(citizenInstance));
				CitizenInfo info = instance.m_instances.m_buffer[(int)citizenInstance].Info;
				InstanceID instanceID;
				this.m_Status.set_text(info.m_citizenAI.GetLocalizedStatus(citizenInstance, ref instance.m_instances.m_buffer[(int)citizenInstance], out instanceID));
			}
		}
	}

	private UILabel m_Type;

	private UILabel m_Status;
}
