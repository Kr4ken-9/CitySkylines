﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class MultiEffect : EffectInfo
{
	public override void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_effects != null)
		{
			float num = 0f;
			if (this.m_duration != 0f)
			{
				if (this.m_useSimulationTime)
				{
					num = Singleton<SimulationManager>.get_instance().m_simulationTimer / this.m_duration;
				}
				else
				{
					num = Time.get_time() / this.m_duration;
				}
				num -= Mathf.Floor(num);
				num *= this.m_duration;
			}
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				MultiEffect.SubEffect subEffect = this.m_effects[i];
				if (subEffect.m_effect != null && subEffect.m_startTime <= num && subEffect.m_endTime >= num)
				{
					if (subEffect.m_probability < 1f)
					{
						if (this.m_fixedRandom)
						{
							Randomizer randomizer;
							randomizer..ctor(id.Index >> 1);
							if ((float)randomizer.Int32(1000u) * 0.001f > subEffect.m_probability)
							{
								goto IL_12B;
							}
						}
						else if (Random.get_value() > subEffect.m_probability)
						{
							goto IL_12B;
						}
					}
					subEffect.m_effect.RenderEffect(id, area, velocity, acceleration, magnitude, timeOffset, timeDelta, cameraInfo);
				}
				IL_12B:;
			}
		}
	}

	public override void PlayEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup)
	{
		if (this.m_effects != null)
		{
			float num = 0f;
			if (this.m_duration != 0f)
			{
				if (this.m_useSimulationTime)
				{
					num = Singleton<SimulationManager>.get_instance().m_simulationTimer / this.m_duration;
				}
				else
				{
					num = Time.get_time() / this.m_duration;
				}
				num -= Mathf.Floor(num);
				num *= this.m_duration;
			}
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				MultiEffect.SubEffect subEffect = this.m_effects[i];
				if (subEffect.m_effect != null && subEffect.m_startTime <= num && subEffect.m_endTime >= num && (subEffect.m_probability == 1f || Random.get_value() < subEffect.m_probability))
				{
					subEffect.m_effect.PlayEffect(id, area, velocity, acceleration, magnitude, listenerInfo, audioGroup);
				}
			}
		}
	}

	protected override void CreateEffect()
	{
		base.CreateEffect();
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_effects[i].m_effect.InitializeEffect();
				}
			}
		}
	}

	protected override void DestroyEffect()
	{
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_effects[i].m_effect.ReleaseEffect();
				}
			}
		}
		base.DestroyEffect();
	}

	public override bool CalculateGroupData(int layer, ref int vertexCount, ref int triangleCount, ref int objectCount, ref RenderGroup.VertexArrays vertexArrays)
	{
		bool result = false;
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null && this.m_effects[i].m_effect.CalculateGroupData(layer, ref vertexCount, ref triangleCount, ref objectCount, ref vertexArrays))
				{
					result = true;
				}
			}
		}
		return result;
	}

	public override void PopulateGroupData(int layer, InstanceID id, Vector3 pos, Vector3 dir, ref int vertexIndex, ref int triangleIndex, Vector3 groupPosition, RenderGroup.MeshData data, ref Vector3 min, ref Vector3 max, ref float maxRenderDistance, ref float maxInstanceDistance)
	{
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					this.m_effects[i].m_effect.PopulateGroupData(layer, id, pos, dir, ref vertexIndex, ref triangleIndex, groupPosition, data, ref min, ref max, ref maxRenderDistance, ref maxInstanceDistance);
				}
			}
		}
	}

	public override bool RequireRender()
	{
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null && this.m_effects[i].m_effect.RequireRender())
				{
					return true;
				}
			}
		}
		return false;
	}

	public override bool RequirePlay()
	{
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null && this.m_effects[i].m_effect.RequirePlay())
				{
					return true;
				}
			}
		}
		return false;
	}

	public override float RenderDistance()
	{
		float num = 0f;
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					num = Mathf.Max(num, this.m_effects[i].m_effect.RenderDistance());
				}
			}
		}
		return num;
	}

	public override float RenderDuration()
	{
		float num = 0f;
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					num = Mathf.Max(num, this.m_effects[i].m_effect.RenderDuration());
				}
			}
		}
		return num;
	}

	public override int GroupLayer()
	{
		if (this.m_effects != null)
		{
			for (int i = 0; i < this.m_effects.Length; i++)
			{
				if (this.m_effects[i].m_effect != null)
				{
					int num = this.m_effects[i].m_effect.GroupLayer();
					if (num != -1)
					{
						return num;
					}
				}
			}
		}
		return -1;
	}

	public MultiEffect.SubEffect[] m_effects;

	public float m_duration;

	public bool m_useSimulationTime;

	public bool m_fixedRandom;

	[Serializable]
	public struct SubEffect
	{
		public EffectInfo m_effect;

		public float m_probability;

		public float m_startTime;

		public float m_endTime;
	}
}
