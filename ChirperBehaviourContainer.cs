﻿using System;
using ColossalFramework;
using ICities;
using UnityEngine;

public class ChirperBehaviourContainer : MonoBehaviour
{
	private void OnEnable()
	{
		this.m_Wrapper = new ChirperWrapper(this);
		if (Singleton<MessageManager>.get_exists())
		{
			Singleton<MessageManager>.get_instance().m_newMessages += new MessageManager.NewMessageHandler(this.AddMessage);
			Singleton<MessageManager>.get_instance().m_messagesUpdated += new Action(this.SynchronizeMessages);
		}
	}

	private void Update()
	{
		this.m_Wrapper.OnChirperExtensionsUpdate();
	}

	private void AddMessage(IChirperMessage message)
	{
		this.m_Wrapper.OnChirperExtensionsNewMessage(message);
	}

	private void SynchronizeMessages()
	{
		this.m_Wrapper.OnChirperExtensionsMessagesUpdated();
	}

	private void OnDisable()
	{
		if (Singleton<MessageManager>.get_exists())
		{
			Singleton<MessageManager>.get_instance().m_newMessages -= new MessageManager.NewMessageHandler(this.AddMessage);
			Singleton<MessageManager>.get_instance().m_messagesUpdated -= new Action(this.SynchronizeMessages);
		}
		if (this.m_Wrapper != null)
		{
			this.m_Wrapper.Release();
			this.m_Wrapper = null;
		}
	}

	private ChirperWrapper m_Wrapper;
}
